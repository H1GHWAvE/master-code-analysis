#!/bin/bash

#remove old files
rm -rf ~/Desktop/master_thesis/sourcefiles/_originalAPKs/code-lvltest/java/
rm -rf ~/Desktop/master_thesis/sourcefiles/_originalAPKs/code-teamspeak/java/
rm -rf ~/Desktop/master_thesis/sourcefiles/_originalAPKs/code-luckypatcher/java/
rm -rf ~/Desktop/master_thesis/sourcefiles/_originalAPKs/code-lvltest/smali/
rm -rf ~/Desktop/master_thesis/sourcefiles/_originalAPKs/code-teamspeak/smali/
rm -rf ~/Desktop/master_thesis/sourcefiles/_originalAPKs/code-luckypatcher/smali/
rm -rf ~/Desktop/master_thesis/sourcefiles/_originalAPKs/code-lvltest/jasmin
rm -rf ~/Desktop/master_thesis/sourcefiles/_originalAPKs/code-teamspeak/jasmin
rm -rf ~/Desktop/master_thesis/sourcefiles/_originalAPKs/code-luckypatcher/jasmin
rm -rf ~/Desktop/master_thesis/sourcefiles/_originalAPKs/code-lvltest/jar/lvltest.jar
rm -rf ~/Desktop/master_thesis/sourcefiles/_originalAPKs/code-teamspeak/jar/teamspeak.jar
rm -rf ~/Desktop/master_thesis/sourcefiles/_originalAPKs/code-luckypatcher/jar/teamspeak.jar

#androguard
python ~/Desktop/master_thesis/sourcefiles/_tools/androdd.py -i ~/Desktop/master_thesis/sourcefiles/_originalAPKs/apk-lvltest/lvltest.apk -o ~/Desktop/master_thesis/sourcefiles/_originalAPKs/code-lvltest/java/
python ~/Desktop/master_thesis/sourcefiles/_tools/androdd.py -i ~/Desktop/master_thesis/sourcefiles/_originalAPKs/apk-teamspeak/teamspeak.apk -o ~/Desktop/master_thesis/sourcefiles/_originalAPKs/code-teamspeak/java/
python ~/Desktop/master_thesis/sourcefiles/_tools/androdd.py -i ~/Desktop/master_thesis/sourcefiles/_originalAPKs/apk-luckypatcher/luckypatcher.apk -o ~/Desktop/master_thesis/sourcefiles/_originalAPKs/code-luckypatcher/java/

#baksmali
java -jar ~/Desktop/master_thesis/sourcefiles/_tools/baksmali.jar -x ~/Desktop/master_thesis/sourcefiles/_originalAPKs/apk-lvltest/lvltest.apk -o ~/Desktop/master_thesis/sourcefiles/_originalAPKs/code-lvltest/smali/
java -jar ~/Desktop/master_thesis/sourcefiles/_tools/baksmali.jar -x ~/Desktop/master_thesis/sourcefiles/_originalAPKs/apk-teamspeak/teamspeak.apk -o ~/Desktop/master_thesis/sourcefiles/_originalAPKs/code-teamspeak/smali/
java -jar ~/Desktop/master_thesis/sourcefiles/_tools/baksmali.jar -x ~/Desktop/master_thesis/sourcefiles/_originalAPKs/apk-luckypatcher/luckypatcher.apk -o ~/Desktop/master_thesis/sourcefiles/_originalAPKs/code-luckypatcher/smali/

#jasmin
bash ~/Desktop/master_thesis/sourcefiles/_tools/_dex2jar-2.0/d2j-dex2jar.sh -f -o ~/Desktop/master_thesis/sourcefiles/_originalAPKs/code-lvltest/jar/lvltest.jar ~/Desktop/master_thesis/sourcefiles/_originalAPKs/apk-lvltest/lvltest.apk
bash ~/Desktop/master_thesis/sourcefiles/_tools/_dex2jar-2.0/d2j-dex2jar.sh -f -o ~/Desktop/master_thesis/sourcefiles/_originalAPKs/code-teamspeak/jar/teamspeak.jar ~/Desktop/master_thesis/sourcefiles/_originalAPKs/apk-teamspeak/teamspeak.apk
bash ~/Desktop/master_thesis/sourcefiles/_tools/_dex2jar-2.0/d2j-dex2jar.sh -f -o ~/Desktop/master_thesis/sourcefiles/_originalAPKs/code-teamspeak/jar/luckypatcher.jar ~/Desktop/master_thesis/sourcefiles/_originalAPKs/apk-luckypatcher/luckypatcher.apk

#jar
bash ~/Desktop/master_thesis/sourcefiles/_tools/_dex2jar-2.0/d2j-jar2jasmin.sh -f -o ~/Desktop/master_thesis/sourcefiles/_originalAPKs/code-lvltest/jasmin ~/Desktop/master_thesis/sourcefiles/_originalAPKs/code-lvltest/jar/lvltest.jar
bash ~/Desktop/master_thesis/sourcefiles/_tools/_dex2jar-2.0/d2j-jar2jasmin.sh -f -o ~/Desktop/master_thesis/sourcefiles/_originalAPKs/code-teamspeak/jasmin ~/Desktop/master_thesis/sourcefiles/_originalAPKs/code-teamspeak/jar/teamspeak.jar
bash ~/Desktop/master_thesis/sourcefiles/_tools/_dex2jar-2.0/d2j-jar2jasmin.sh -f -o ~/Desktop/master_thesis/sourcefiles/_originalAPKs/code-luckypatcher/jasmin ~/Desktop/master_thesis/sourcefiles/_originalAPKs/code-luckypatcher/jar/lvltest.jar
