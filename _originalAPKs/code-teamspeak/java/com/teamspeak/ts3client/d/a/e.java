package com.teamspeak.ts3client.d.a;
final class e implements android.view.View$OnClickListener {
    final synthetic com.teamspeak.ts3client.d.a.a a;

    e(com.teamspeak.ts3client.d.a.a p1)
    {
        this.a = p1;
        return;
    }

    public final void onClick(android.view.View p13)
    {
        java.util.Iterator v7 = com.teamspeak.ts3client.d.a.a.y().a.f.a.iterator();
        int v1_0 = 0;
        int v3_0 = 0;
        String v4_0 = 0;
        int v5 = 0;
        while (v7.hasNext()) {
            int v0_6 = ((com.teamspeak.ts3client.data.a) v7.next());
            if (v0_6.b != com.teamspeak.ts3client.d.a.a.a(this.a).b) {
                int v1_1;
                int v0_7;
                if (v5 == 0) {
                    v0_7 = v1_0;
                    v1_1 = v3_0;
                } else {
                    if ((v0_6.f == 0) || (v0_6.f == v4_0)) {
                        break;
                    }
                    v0_7 = (v1_0 + 1);
                    v1_1 = (v3_0 + v0_6.j().size());
                }
                v3_0 = v1_1;
                v1_0 = v0_7;
            } else {
                v4_0 = v0_6.f;
                v3_0 = (v0_6.j().size() + v3_0);
                v5 = 1;
            }
        }
        int v0_10 = new android.app.AlertDialog$Builder(this.a.i()).create();
        com.teamspeak.ts3client.data.d.q.a(v0_10);
        if (v3_0 <= 0) {
            if (v1_0 <= 0) {
                v0_10.setMessage(com.teamspeak.ts3client.data.e.a.a("dialog.channel.delete.warn3"));
            } else {
                v0_10.setMessage(com.teamspeak.ts3client.data.e.a.a("dialog.channel.delete.warn2"));
            }
        } else {
            v0_10.setMessage(com.teamspeak.ts3client.data.e.a.a("dialog.channel.delete.warn1"));
        }
        int v3_1 = new Object[1];
        v3_1[0] = com.teamspeak.ts3client.d.a.a.a(this.a).a;
        v0_10.setTitle(com.teamspeak.ts3client.data.e.a.a("dialog.channel.delete.dialog.text", v3_1));
        v0_10.setButton(-2, com.teamspeak.ts3client.data.e.a.a("button.cancel"), new com.teamspeak.ts3client.d.a.f(this));
        v0_10.setButton(-1, com.teamspeak.ts3client.data.e.a.a("button.delete"), new com.teamspeak.ts3client.d.a.g(this));
        v0_10.show();
        this.a.b();
        return;
    }
}
