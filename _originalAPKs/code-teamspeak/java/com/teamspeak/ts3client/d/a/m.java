package com.teamspeak.ts3client.d.a;
public final class m extends android.support.v4.app.ax implements com.teamspeak.ts3client.data.w {
    private android.widget.Spinner aA;
    private android.widget.CheckBox aB;
    private com.teamspeak.ts3client.Ts3Application aC;
    private com.teamspeak.ts3client.d.a.m aD;
    private String aE;
    private String aF;
    private int aG;
    private int aH;
    private int aI;
    private int aJ;
    private String aK;
    private android.widget.Button aL;
    private int aM;
    private int aN;
    private boolean aO;
    private boolean aP;
    private android.widget.ImageView aQ;
    private com.teamspeak.ts3client.customs.CustomCodecSettings aR;
    private com.teamspeak.ts3client.customs.a aS;
    private android.widget.TextView aT;
    private int aU;
    private int aV;
    private boolean aW;
    String at;
    boolean au;
    private com.teamspeak.ts3client.data.a av;
    private com.teamspeak.ts3client.customs.CustomCodecSettings aw;
    private com.teamspeak.ts3client.bookmark.CustomEditText ax;
    private android.widget.EditText ay;
    private android.widget.EditText az;

    public m(com.teamspeak.ts3client.data.a p4)
    {
        this.aE = "";
        this.aF = "";
        this.aG = 0;
        this.aH = 0;
        this.aI = 0;
        this.aJ = 0;
        this.at = "";
        this.aK = "";
        this.aM = 4;
        this.aN = 6;
        this.aO = 0;
        this.aP = 0;
        this.au = 0;
        this.aU = 0;
        this.aW = 1;
        if (p4 == null) {
            this.av = new com.teamspeak.ts3client.data.a();
            this.aO = 1;
            this.aP = 1;
        } else {
            this.av = p4;
        }
        this.au = 0;
        this.aD = this;
        return;
    }

    public m(com.teamspeak.ts3client.data.a p2, boolean p3)
    {
        this(p2);
        this.aO = 1;
        this.aP = p3;
        return;
    }

    private void A()
    {
        this.i().runOnUiThread(new com.teamspeak.ts3client.d.a.s(this));
        return;
    }

    static synthetic int a(com.teamspeak.ts3client.d.a.m p0, int p1)
    {
        p0.aG = p1;
        return p1;
    }

    static synthetic com.teamspeak.ts3client.d.a.m a(com.teamspeak.ts3client.d.a.m p1)
    {
        return p1.aD;
    }

    static synthetic String a(com.teamspeak.ts3client.d.a.m p0, String p1)
    {
        p0.aE = p1;
        return p1;
    }

    static synthetic int b(com.teamspeak.ts3client.d.a.m p0, int p1)
    {
        p0.aH = p1;
        return p1;
    }

    static synthetic android.widget.Spinner b(com.teamspeak.ts3client.d.a.m p1)
    {
        return p1.aA;
    }

    static synthetic String b(com.teamspeak.ts3client.d.a.m p0, String p1)
    {
        p0.aK = p1;
        return p1;
    }

    private void b(String p2)
    {
        this.au = 1;
        this.at = p2;
        return;
    }

    static synthetic int c(com.teamspeak.ts3client.d.a.m p0, int p1)
    {
        p0.aI = p1;
        return p1;
    }

    static synthetic String c(com.teamspeak.ts3client.d.a.m p0, String p1)
    {
        p0.aF = p1;
        return p1;
    }

    static synthetic boolean c(com.teamspeak.ts3client.d.a.m p1)
    {
        return p1.aW;
    }

    static synthetic int d(com.teamspeak.ts3client.d.a.m p0, int p1)
    {
        p0.aM = p1;
        return p1;
    }

    static synthetic android.widget.TextView d(com.teamspeak.ts3client.d.a.m p1)
    {
        return p1.aT;
    }

    static synthetic String d(com.teamspeak.ts3client.d.a.m p0, String p1)
    {
        p0.at = p1;
        return p1;
    }

    static synthetic int e(com.teamspeak.ts3client.d.a.m p0, int p1)
    {
        p0.aN = p1;
        return p1;
    }

    static synthetic com.teamspeak.ts3client.bookmark.CustomEditText e(com.teamspeak.ts3client.d.a.m p1)
    {
        return p1.ax;
    }

    static synthetic int f(com.teamspeak.ts3client.d.a.m p0, int p1)
    {
        p0.aJ = p1;
        return p1;
    }

    static synthetic android.widget.CheckBox f(com.teamspeak.ts3client.d.a.m p1)
    {
        return p1.aB;
    }

    static synthetic int g(com.teamspeak.ts3client.d.a.m p1)
    {
        return p1.aG;
    }

    static synthetic com.teamspeak.ts3client.Ts3Application h(com.teamspeak.ts3client.d.a.m p1)
    {
        return p1.aC;
    }

    static synthetic com.teamspeak.ts3client.data.a i(com.teamspeak.ts3client.d.a.m p1)
    {
        return p1.av;
    }

    static synthetic int j(com.teamspeak.ts3client.d.a.m p1)
    {
        return p1.aJ;
    }

    static synthetic int k(com.teamspeak.ts3client.d.a.m p1)
    {
        return p1.aU;
    }

    static synthetic android.widget.EditText l(com.teamspeak.ts3client.d.a.m p1)
    {
        return p1.ay;
    }

    static synthetic String m(com.teamspeak.ts3client.d.a.m p1)
    {
        return p1.aK;
    }

    static synthetic String n(com.teamspeak.ts3client.d.a.m p1)
    {
        return p1.aE;
    }

    static synthetic android.widget.EditText o(com.teamspeak.ts3client.d.a.m p1)
    {
        return p1.az;
    }

    static synthetic String p(com.teamspeak.ts3client.d.a.m p1)
    {
        return p1.aF;
    }

    static synthetic boolean q(com.teamspeak.ts3client.d.a.m p1)
    {
        return p1.au;
    }

    static synthetic String r(com.teamspeak.ts3client.d.a.m p1)
    {
        return p1.at;
    }

    static synthetic int s(com.teamspeak.ts3client.d.a.m p1)
    {
        return p1.aH;
    }

    static synthetic int t(com.teamspeak.ts3client.d.a.m p1)
    {
        return p1.aI;
    }

    static synthetic com.teamspeak.ts3client.customs.CustomCodecSettings u(com.teamspeak.ts3client.d.a.m p1)
    {
        return p1.aR;
    }

    static synthetic int v(com.teamspeak.ts3client.d.a.m p1)
    {
        return p1.aM;
    }

    static synthetic int w(com.teamspeak.ts3client.d.a.m p1)
    {
        return p1.aN;
    }

    static synthetic boolean x(com.teamspeak.ts3client.d.a.m p1)
    {
        return p1.aO;
    }

    private String y()
    {
        return this.at;
    }

    static synthetic boolean y(com.teamspeak.ts3client.d.a.m p1)
    {
        return p1.aP;
    }

    static synthetic int z(com.teamspeak.ts3client.d.a.m p1)
    {
        return p1.aV;
    }

    private void z()
    {
        if (!this.aO) {
            if (!this.aC.a.s.a(com.teamspeak.ts3client.jni.g.bf)) {
                this.ax.setEnabled(0);
            }
            if (!this.aC.a.s.a(com.teamspeak.ts3client.jni.g.bg)) {
                this.az.setEnabled(0);
            }
            if (!this.aC.a.s.a(com.teamspeak.ts3client.jni.g.bi)) {
                this.ay.setEnabled(0);
            }
            if (!this.aC.a.s.a(com.teamspeak.ts3client.jni.g.bh)) {
                this.aQ.setEnabled(0);
            }
            if (!this.aC.a.s.a(com.teamspeak.ts3client.jni.g.bb)) {
                this.aB.setEnabled(0);
            }
            if (!this.aC.a.s.a(com.teamspeak.ts3client.jni.g.bc)) {
                this.aS.a(2);
            }
            if (!this.aC.a.s.a(com.teamspeak.ts3client.jni.g.bd)) {
                this.aS.a(1);
            }
            if (!this.aC.a.s.a(com.teamspeak.ts3client.jni.g.be)) {
                this.aS.a(0);
            }
            if (!this.aC.a.s.a(com.teamspeak.ts3client.jni.g.bt)) {
                this.aW = 0;
                this.aT.setEnabled(0);
            }
        } else {
            if (!this.aC.a.s.a(com.teamspeak.ts3client.jni.g.aI)) {
                this.az.setEnabled(0);
            }
            if (!this.aC.a.s.a(com.teamspeak.ts3client.jni.g.aK)) {
                this.ay.setEnabled(0);
            }
            if (!this.aC.a.s.a(com.teamspeak.ts3client.jni.g.aJ)) {
                this.aQ.setEnabled(0);
            }
            if (!this.aC.a.s.a(com.teamspeak.ts3client.jni.g.aW)) {
                this.aB.setEnabled(0);
            }
            if (!this.aC.a.s.a(com.teamspeak.ts3client.jni.g.aE)) {
                this.aS.a(2);
                this.aA.setSelection(1);
            }
            if (!this.aC.a.s.a(com.teamspeak.ts3client.jni.g.aF)) {
                this.aS.a(1);
                this.aA.setSelection(0);
            }
            if (!this.aC.a.s.a(com.teamspeak.ts3client.jni.g.aG)) {
                this.aS.a(0);
            }
            this.aW = 1;
        }
        if (!this.aC.a.s.a.containsKey(Integer.valueOf(this.aC.a.u.a(com.teamspeak.ts3client.jni.g.aZ)))) {
            this.aU = -1;
        } else {
            this.aU = this.aC.a.s.b(this.aC.a.u.a(com.teamspeak.ts3client.jni.g.aZ));
            if (this.aU < -1) {
            }
        }
        this.aV = this.aC.a.a.a(this.aC.a.e, com.teamspeak.ts3client.jni.i.aB);
        if ((this.aU > 0) && (this.aV > this.aU)) {
            this.aV = this.aU;
        }
        this.aT.setText(new StringBuilder().append(this.aV).toString());
        switch (this.aU) {
            case -1:
                this.aW = 0;
                this.aT.setEnabled(0);
                this.aT.setText(new StringBuilder().append(this.aV).toString());
                break;
            case 0:
                this.aW = 0;
                this.aT.setEnabled(0);
                this.aT.setText("0");
                break;
        }
        return;
    }

    public final android.view.View a(android.view.LayoutInflater p12, android.view.ViewGroup p13)
    {
        this.aC = ((com.teamspeak.ts3client.Ts3Application) p12.getContext().getApplicationContext());
        android.widget.ScrollView v0_5 = ((android.widget.ScrollView) p12.inflate(2130903073, p13, 0));
        if (this.aO) {
            this.j.setTitle(com.teamspeak.ts3client.data.e.a.a("channeldialog.create.text"));
        } else {
            this.j.setTitle(this.av.a);
        }
        com.teamspeak.ts3client.data.e.a.a("channeldialog.editname", v0_5, 2131493039);
        com.teamspeak.ts3client.data.e.a.a("channeldialog.editpassword", v0_5, 2131493042);
        com.teamspeak.ts3client.data.e.a.a("channeldialog.edittopic", v0_5, 2131493045);
        com.teamspeak.ts3client.data.e.a.a("channeldialog.editdesc", v0_5, 2131493048);
        com.teamspeak.ts3client.data.e.a.a("channeldialog.edittype", v0_5, 2131493050);
        com.teamspeak.ts3client.data.e.a.a("channeldialog.editdefault", v0_5, 2131493053);
        com.teamspeak.ts3client.data.e.a.a("channeldialog.editdeletedelay", v0_5, 2131493056);
        this.ax = ((com.teamspeak.ts3client.bookmark.CustomEditText) v0_5.findViewById(2131493040));
        this.ay = ((android.widget.EditText) v0_5.findViewById(2131493043));
        this.az = ((android.widget.EditText) v0_5.findViewById(2131493046));
        this.aQ = ((android.widget.ImageView) v0_5.findViewById(2131493049));
        this.aR = ((com.teamspeak.ts3client.customs.CustomCodecSettings) v0_5.findViewById(2131493059));
        this.aQ.setOnClickListener(new com.teamspeak.ts3client.d.a.n(this));
        this.aA = ((android.widget.Spinner) v0_5.findViewById(2131493051));
        this.aS = com.teamspeak.ts3client.data.e.a.b("channeldialog.edittype.spinner", this.i(), 3);
        this.aA.setAdapter(this.aS);
        this.aB = ((android.widget.CheckBox) v0_5.findViewById(2131493054));
        this.aB.setOnCheckedChangeListener(new com.teamspeak.ts3client.d.a.o(this));
        this.aT = ((android.widget.TextView) v0_5.findViewById(2131493057));
        this.aA.setOnItemSelectedListener(new com.teamspeak.ts3client.d.a.p(this));
        this.aL = ((android.widget.Button) v0_5.findViewById(2131493060));
        this.aL.setText(com.teamspeak.ts3client.data.e.a.a("button.save"));
        this.aL.setOnClickListener(new com.teamspeak.ts3client.d.a.q(this));
        if (this.aO) {
            this.aR.a(new com.teamspeak.ts3client.data.d.ab(this.aM, this.aN), 1);
        }
        if (!this.aO) {
            this.aC.a.c.a(this);
            this.aC.a.a.ts3client_requestChannelDescription(this.aC.a.e, this.av.b, "ChannelEdit");
        }
        if (!this.aO) {
            if (!this.aC.a.s.a(com.teamspeak.ts3client.jni.g.bf)) {
                this.ax.setEnabled(0);
            }
            if (!this.aC.a.s.a(com.teamspeak.ts3client.jni.g.bg)) {
                this.az.setEnabled(0);
            }
            if (!this.aC.a.s.a(com.teamspeak.ts3client.jni.g.bi)) {
                this.ay.setEnabled(0);
            }
            if (!this.aC.a.s.a(com.teamspeak.ts3client.jni.g.bh)) {
                this.aQ.setEnabled(0);
            }
            if (!this.aC.a.s.a(com.teamspeak.ts3client.jni.g.bb)) {
                this.aB.setEnabled(0);
            }
            if (!this.aC.a.s.a(com.teamspeak.ts3client.jni.g.bc)) {
                this.aS.a(2);
            }
            if (!this.aC.a.s.a(com.teamspeak.ts3client.jni.g.bd)) {
                this.aS.a(1);
            }
            if (!this.aC.a.s.a(com.teamspeak.ts3client.jni.g.be)) {
                this.aS.a(0);
            }
            if (!this.aC.a.s.a(com.teamspeak.ts3client.jni.g.bt)) {
                this.aW = 0;
                this.aT.setEnabled(0);
            }
        } else {
            if (!this.aC.a.s.a(com.teamspeak.ts3client.jni.g.aI)) {
                this.az.setEnabled(0);
            }
            if (!this.aC.a.s.a(com.teamspeak.ts3client.jni.g.aK)) {
                this.ay.setEnabled(0);
            }
            if (!this.aC.a.s.a(com.teamspeak.ts3client.jni.g.aJ)) {
                this.aQ.setEnabled(0);
            }
            if (!this.aC.a.s.a(com.teamspeak.ts3client.jni.g.aW)) {
                this.aB.setEnabled(0);
            }
            if (!this.aC.a.s.a(com.teamspeak.ts3client.jni.g.aE)) {
                this.aS.a(2);
                this.aA.setSelection(1);
            }
            if (!this.aC.a.s.a(com.teamspeak.ts3client.jni.g.aF)) {
                this.aS.a(1);
                this.aA.setSelection(0);
            }
            if (!this.aC.a.s.a(com.teamspeak.ts3client.jni.g.aG)) {
                this.aS.a(0);
            }
            this.aW = 1;
        }
        if (!this.aC.a.s.a.containsKey(Integer.valueOf(this.aC.a.u.a(com.teamspeak.ts3client.jni.g.aZ)))) {
            this.aU = -1;
        } else {
            this.aU = this.aC.a.s.b(this.aC.a.u.a(com.teamspeak.ts3client.jni.g.aZ));
            if (this.aU < -1) {
            }
        }
        this.aV = this.aC.a.a.a(this.aC.a.e, com.teamspeak.ts3client.jni.i.aB);
        if ((this.aU > 0) && (this.aV > this.aU)) {
            this.aV = this.aU;
        }
        this.aT.setText(new StringBuilder().append(this.aV).toString());
        switch (this.aU) {
            case -1:
                this.aW = 0;
                this.aT.setEnabled(0);
                this.aT.setText(new StringBuilder().append(this.aV).toString());
                break;
            case 0:
                this.aW = 0;
                this.aT.setEnabled(0);
                this.aT.setText("0");
                break;
        }
        if (this.aT.isEnabled()) {
            this.aT.addTextChangedListener(new com.teamspeak.ts3client.d.a.r(this));
        }
        return v0_5;
    }

    public final void a(com.teamspeak.ts3client.jni.k p5)
    {
        if (!(p5 instanceof com.teamspeak.ts3client.jni.events.UpdateChannel)) {
            if (((p5 instanceof com.teamspeak.ts3client.jni.events.rare.ServerPermissionError)) && (((com.teamspeak.ts3client.jni.events.rare.ServerPermissionError) p5).a == this.aC.a.u.a(com.teamspeak.ts3client.jni.g.bK))) {
                this.A();
                this.aC.a.c.b(this);
            }
        } else {
            if (((com.teamspeak.ts3client.jni.events.UpdateChannel) p5).a == this.av.b) {
                this.A();
                this.aC.a.c.b(this);
            }
        }
        return;
    }

    public final void e()
    {
        super.e();
        com.teamspeak.ts3client.data.d.q.a(this);
        return;
    }

    public final void f()
    {
        super.f();
        com.teamspeak.ts3client.data.d.q.b(this);
        return;
    }
}
