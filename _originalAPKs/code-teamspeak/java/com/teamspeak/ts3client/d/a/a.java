package com.teamspeak.ts3client.d.a;
public final class a extends android.support.v4.app.ax implements com.teamspeak.ts3client.data.w {
    private static com.teamspeak.ts3client.Ts3Application at;
    private com.teamspeak.ts3client.data.a au;
    private boolean av;

    public a(com.teamspeak.ts3client.data.a p1, boolean p2)
    {
        this.au = p1;
        this.av = p2;
        return;
    }

    static synthetic com.teamspeak.ts3client.data.a a(com.teamspeak.ts3client.d.a.a p1)
    {
        return p1.au;
    }

    static synthetic com.teamspeak.ts3client.Ts3Application y()
    {
        return com.teamspeak.ts3client.d.a.a.at;
    }

    public final android.view.View a(android.view.LayoutInflater p7, android.view.ViewGroup p8)
    {
        android.widget.ScrollView v0_2 = ((com.teamspeak.ts3client.Ts3Application) p7.getContext().getApplicationContext());
        com.teamspeak.ts3client.d.a.a.at = v0_2;
        v0_2.a.c.a(this);
        android.widget.ScrollView v0_6 = new android.widget.ScrollView(this.i());
        android.widget.LinearLayout v1_2 = new android.widget.LinearLayout(this.i());
        v1_2.setOrientation(1);
        v1_2.setLayoutParams(new android.view.ViewGroup$LayoutParams(-1, -1));
        v0_6.addView(v1_2);
        this.j.setTitle(this.au.a);
        if (com.teamspeak.ts3client.d.a.a.at.a.g != this.au.b) {
            android.widget.Button v2_10 = new android.widget.Button(this.i());
            v2_10.setText(com.teamspeak.ts3client.data.e.a.a("dialog.channel.join.text"));
            v2_10.setOnClickListener(new com.teamspeak.ts3client.d.a.b(this));
            v1_2.addView(v2_10);
        }
        if (this.av) {
            android.widget.Button v2_13 = new android.widget.Button(this.i());
            v2_13.setText(com.teamspeak.ts3client.data.e.a.a("dialog.channel.info.text"));
            v2_13.setOnClickListener(new com.teamspeak.ts3client.d.a.c(this));
            v1_2.addView(v2_13);
        }
        android.widget.Button v2_15 = new android.widget.Button(this.i());
        v2_15.setText(com.teamspeak.ts3client.data.e.a.a("dialog.channel.edit.text"));
        v2_15.setOnClickListener(new com.teamspeak.ts3client.d.a.d(this));
        v1_2.addView(v2_15);
        if (!this.au.r) {
            android.widget.Button v2_19 = new android.widget.Button(this.i());
            v2_19.setText(com.teamspeak.ts3client.data.e.a.a("dialog.channel.delete.text"));
            v2_19.setOnClickListener(new com.teamspeak.ts3client.d.a.e(this));
            v1_2.addView(v2_19);
        }
        android.widget.Button v2_21 = new android.widget.Button(this.i());
        v2_21.setText(com.teamspeak.ts3client.data.e.a.a("dialog.channel.move.text"));
        v2_21.setOnClickListener(new com.teamspeak.ts3client.d.a.h(this));
        v1_2.addView(v2_21);
        if (com.teamspeak.ts3client.d.a.a.at.a.g != this.au.b) {
            android.widget.Button v2_27 = new android.widget.Button(this.i());
            if (!this.au.k) {
                v2_27.setText(com.teamspeak.ts3client.data.e.a.a("dialog.channel.subscribe.text1"));
            } else {
                v2_27.setText(com.teamspeak.ts3client.data.e.a.a("dialog.channel.subscribe.text2"));
            }
            v2_27.setOnClickListener(new com.teamspeak.ts3client.d.a.l(this));
            v1_2.addView(v2_27);
        }
        return v0_6;
    }

    public final void a(com.teamspeak.ts3client.jni.k p11)
    {
        if (((p11 instanceof com.teamspeak.ts3client.jni.events.ServerError)) && ((((com.teamspeak.ts3client.jni.events.ServerError) p11).b == 0) && (((com.teamspeak.ts3client.jni.events.ServerError) p11).c.equals("checkPW")))) {
            com.teamspeak.ts3client.d.a.a.at.a.a.ts3client_requestClientMove(com.teamspeak.ts3client.d.a.a.at.a.e, 0, this.au.b, ((String) com.teamspeak.ts3client.d.a.a.at.a.j.get(Long.valueOf(this.au.b))), new StringBuilder("join ").append(this.au.b).toString());
            com.teamspeak.ts3client.d.a.a.at.a.c.b(this);
            this.b();
        }
        return;
    }

    public final void e()
    {
        super.e();
        com.teamspeak.ts3client.data.d.q.a(this);
        return;
    }

    public final void f()
    {
        super.f();
        if (com.teamspeak.ts3client.d.a.a.at.a != null) {
            com.teamspeak.ts3client.d.a.a.at.a.c.b(this);
        }
        com.teamspeak.ts3client.data.d.q.b(this);
        return;
    }
}
