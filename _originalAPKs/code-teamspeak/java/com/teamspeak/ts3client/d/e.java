package com.teamspeak.ts3client.d;
public final class e extends android.support.v4.app.ax {
    private android.widget.CheckBox aA;
    private android.widget.CheckBox aB;
    private com.teamspeak.ts3client.data.c aC;
    private android.widget.RadioGroup aD;
    private android.widget.RadioButton aE;
    private android.widget.RadioButton aF;
    private android.widget.Button aG;
    private com.teamspeak.ts3client.c.a aH;
    private boolean aI;
    private android.widget.EditText at;
    private android.widget.Spinner au;
    private android.widget.Spinner av;
    private android.widget.CheckBox aw;
    private android.widget.CheckBox ax;
    private android.widget.CheckBox ay;
    private android.widget.CheckBox az;

    public e(com.teamspeak.ts3client.c.a p2)
    {
        this.aC = new com.teamspeak.ts3client.data.c(p2);
        return;
    }

    public e(com.teamspeak.ts3client.data.c p1)
    {
        this.aC = p1;
        return;
    }

    static synthetic boolean a(com.teamspeak.ts3client.d.e p1)
    {
        return p1.aI;
    }

    static synthetic boolean b(com.teamspeak.ts3client.d.e p1)
    {
        p1.aI = 0;
        return 0;
    }

    static synthetic android.widget.Spinner c(com.teamspeak.ts3client.d.e p1)
    {
        return p1.av;
    }

    static synthetic android.widget.CheckBox d(com.teamspeak.ts3client.d.e p1)
    {
        return p1.aw;
    }

    static synthetic android.widget.CheckBox e(com.teamspeak.ts3client.d.e p1)
    {
        return p1.ax;
    }

    static synthetic android.widget.CheckBox f(com.teamspeak.ts3client.d.e p1)
    {
        return p1.ay;
    }

    static synthetic android.widget.CheckBox g(com.teamspeak.ts3client.d.e p1)
    {
        return p1.az;
    }

    static synthetic android.widget.CheckBox h(com.teamspeak.ts3client.d.e p1)
    {
        return p1.aA;
    }

    static synthetic android.widget.CheckBox i(com.teamspeak.ts3client.d.e p1)
    {
        return p1.aB;
    }

    static synthetic android.widget.RadioButton j(com.teamspeak.ts3client.d.e p1)
    {
        return p1.aE;
    }

    static synthetic android.widget.RadioButton k(com.teamspeak.ts3client.d.e p1)
    {
        return p1.aF;
    }

    static synthetic android.widget.EditText l(com.teamspeak.ts3client.d.e p1)
    {
        return p1.at;
    }

    static synthetic com.teamspeak.ts3client.c.a m(com.teamspeak.ts3client.d.e p1)
    {
        return p1.aH;
    }

    static synthetic android.widget.Spinner n(com.teamspeak.ts3client.d.e p1)
    {
        return p1.au;
    }

    static synthetic com.teamspeak.ts3client.data.c o(com.teamspeak.ts3client.d.e p1)
    {
        return p1.aC;
    }

    public final android.view.View a(android.view.LayoutInflater p12, android.view.ViewGroup p13)
    {
        android.widget.Button v0_2 = ((com.teamspeak.ts3client.Ts3Application) p12.getContext().getApplicationContext());
        android.widget.ScrollView v1_2 = ((android.widget.ScrollView) p12.inflate(2130903089, p13, 0));
        String v2_0 = this.j;
        android.content.Context v4_1 = new Object[1];
        v4_1[0] = this.aC.a;
        v2_0.setTitle(com.teamspeak.ts3client.data.e.a.a("contact.settings.info", v4_1));
        this.at = ((android.widget.EditText) v1_2.findViewById(2131493220));
        this.at.setText(this.aC.a);
        this.au = ((android.widget.Spinner) v1_2.findViewById(2131493221));
        this.au.setAdapter(com.teamspeak.ts3client.data.e.a.a("contact.settings.status.array", v1_2.getContext(), 3));
        this.au.setOnItemSelectedListener(new com.teamspeak.ts3client.d.f(this));
        this.av = ((android.widget.Spinner) v1_2.findViewById(2131493223));
        this.av.setAdapter(com.teamspeak.ts3client.data.e.a.a("contact.settings.display.array", v1_2.getContext(), 3));
        this.av.setSelection(2);
        this.aw = ((android.widget.CheckBox) v1_2.findViewById(2131493225));
        this.ax = ((android.widget.CheckBox) v1_2.findViewById(2131493226));
        this.ay = ((android.widget.CheckBox) v1_2.findViewById(2131493227));
        this.az = ((android.widget.CheckBox) v1_2.findViewById(2131493228));
        this.aA = ((android.widget.CheckBox) v1_2.findViewById(2131493230));
        this.aB = ((android.widget.CheckBox) v1_2.findViewById(2131493229));
        this.aD = ((android.widget.RadioGroup) v1_2.findViewById(2131493232));
        this.aE = ((android.widget.RadioButton) v1_2.findViewById(2131493233));
        this.aF = ((android.widget.RadioButton) v1_2.findViewById(2131493234));
        this.aG = ((android.widget.Button) v1_2.findViewById(2131493235));
        this.aG.setOnClickListener(new com.teamspeak.ts3client.d.g(this, v0_2));
        com.teamspeak.ts3client.data.e.a.a("contact.settings.name", v1_2, 2131493219);
        com.teamspeak.ts3client.data.e.a.a("contact.settings.display", v1_2, 2131493222);
        com.teamspeak.ts3client.data.e.a.a("contact.settings.ignore", v1_2, 2131493224);
        com.teamspeak.ts3client.data.e.a.a("contact.settings.whisper", v1_2, 2131493231);
        com.teamspeak.ts3client.data.e.a.a("contact.settings.mute", v1_2, 2131493225);
        com.teamspeak.ts3client.data.e.a.a("contact.settings.ignorepublicchat", v1_2, 2131493226);
        com.teamspeak.ts3client.data.e.a.a("contact.settings.ignoreprivatechat", v1_2, 2131493227);
        com.teamspeak.ts3client.data.e.a.a("contact.settings.ignorepokes", v1_2, 2131493228);
        com.teamspeak.ts3client.data.e.a.a("contact.settings.hideavatar", v1_2, 2131493229);
        com.teamspeak.ts3client.data.e.a.a("contact.settings.hideaway", v1_2, 2131493230);
        com.teamspeak.ts3client.data.e.a.a("contact.settings.whisper.allow", v1_2, 2131493233);
        com.teamspeak.ts3client.data.e.a.a("contact.settings.whisper.deny", v1_2, 2131493234);
        this.aG.setText(com.teamspeak.ts3client.data.e.a.a("button.save"));
        return v1_2;
    }

    public final void c(android.os.Bundle p5)
    {
        int v0 = 1;
        super.c(p5);
        boolean v1_1 = com.teamspeak.ts3client.data.b.c.a.a(this.aC.b);
        if (v1_1) {
            this.aH = v1_1;
            this.aI = 1;
            this.at.setText(v1_1.a);
            this.au.setSelection(v1_1.e);
            this.av.setSelection(v1_1.d);
            this.aw.setChecked(v1_1.f);
            this.ax.setChecked(v1_1.g);
            this.ay.setChecked(v1_1.h);
            this.az.setChecked(v1_1.i);
            this.aA.setChecked(v1_1.j);
            this.aB.setChecked(v1_1.k);
            this.aE.setChecked(v1_1.l);
            if (v1_1.l) {
                v0 = 0;
            }
            this.aF.setChecked(v0);
        }
        return;
    }

    public final void e()
    {
        super.e();
        com.teamspeak.ts3client.data.d.q.a(this);
        return;
    }

    public final void f()
    {
        super.f();
        com.teamspeak.ts3client.data.d.q.b(this);
        return;
    }
}
