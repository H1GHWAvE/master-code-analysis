package com.teamspeak.ts3client.d.d;
final class f implements android.text.TextWatcher {
    final synthetic com.teamspeak.ts3client.d.d.d a;

    f(com.teamspeak.ts3client.d.d.d p1)
    {
        this.a = p1;
        return;
    }

    public final void afterTextChanged(android.text.Editable p3)
    {
        if ((p3.length() <= 0) || (com.teamspeak.ts3client.d.d.d.c(this.a).getText().length() <= 0)) {
            com.teamspeak.ts3client.d.d.d.b(this.a).setEnabled(0);
        } else {
            com.teamspeak.ts3client.d.d.d.b(this.a).setEnabled(1);
        }
        return;
    }

    public final void beforeTextChanged(CharSequence p1, int p2, int p3, int p4)
    {
        return;
    }

    public final void onTextChanged(CharSequence p1, int p2, int p3, int p4)
    {
        return;
    }
}
