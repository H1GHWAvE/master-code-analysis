package com.teamspeak.ts3client;
public final class b extends android.app.Dialog {
    private android.widget.ScrollView a;
    private android.widget.LinearLayout b;

    public b(android.content.Context p12)
    {
        this(p12, 2131165365);
        this.setTitle(com.teamspeak.ts3client.data.e.a.a("changelog.info"));
        com.teamspeak.ts3client.data.d.q.a(this);
        try {
            android.widget.ScrollView v0_5 = p12.getPackageManager().getPackageInfo(p12.getPackageName(), 0).versionCode;
        } catch (android.widget.ScrollView v0) {
            v0_5 = 0;
        } catch (android.widget.ScrollView v0) {
            v0_5 = 0;
        }
        this.a = new android.widget.ScrollView(p12);
        this.b = new android.widget.LinearLayout(p12);
        this.b.setOrientation(1);
        this.b.setPadding(5, 5, 5, 5);
        String v3_2 = p12.getResources().getStringArray(2131361793);
        int v4_1 = v3_2.length;
        int v2_8 = 0;
        while (v2_8 < v4_1) {
            String v5_0 = v3_2[v2_8];
            android.widget.TextView v6_1 = new android.widget.TextView(p12);
            v6_1.setPadding(0, 0, 0, 2);
            if (!v5_0.startsWith("->")) {
                if (!v5_0.startsWith("***")) {
                    v6_1.setTypeface(android.graphics.Typeface.DEFAULT, 0);
                    v6_1.setText(new StringBuilder("\u2022").append(v5_0).toString());
                } else {
                    v6_1.setTextSize(1102053376);
                    v6_1.setTypeface(android.graphics.Typeface.DEFAULT, 3);
                    v6_1.setText(v5_0.replace("***", ""));
                }
            } else {
                v6_1.setTextSize(1098907648);
                v6_1.setTypeface(android.graphics.Typeface.DEFAULT, 1);
                v6_1.setText(v5_0.replace("->", "\u2605"));
            }
            this.b.addView(v6_1);
            v2_8++;
        }
        android.widget.LinearLayout v1_2 = new android.widget.TextView(p12);
        v1_2.setText(new StringBuilder("\nPlay Store version: ").append(v0_5).toString());
        v1_2.setTypeface(0, 2);
        this.b.addView(v1_2);
        this.a.addView(this.b);
        this.setContentView(this.a);
        return;
    }
}
