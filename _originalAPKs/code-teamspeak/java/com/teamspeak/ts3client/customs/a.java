package com.teamspeak.ts3client.customs;
public final class a extends android.widget.ArrayAdapter implements android.widget.SpinnerAdapter {
    private java.util.Vector a;
    private android.content.Context b;
    private android.view.LayoutInflater c;
    private int d;
    private int e;

    private a(android.content.Context p2, int p3)
    {
        this(p2, p3);
        this.a = new java.util.Vector();
        this.d = 0;
        this.b = p2;
        this.c = ((android.view.LayoutInflater) p2.getSystemService("layout_inflater"));
        this.e = p3;
        return;
    }

    private a(android.content.Context p2, int p3, java.util.List p4)
    {
        this(p2, p3, p4);
        this.a = new java.util.Vector();
        this.d = 0;
        this.b = p2;
        this.c = ((android.view.LayoutInflater) p2.getSystemService("layout_inflater"));
        this.e = p3;
        return;
    }

    public a(android.content.Context p3, String[] p4)
    {
        this(p3, 17367049, p4);
        this.a = new java.util.Vector();
        this.d = 0;
        this.b = p3;
        this.c = ((android.view.LayoutInflater) p3.getSystemService("layout_inflater"));
        this.e = 17367049;
        return;
    }

    private android.view.View a(int p7, android.view.ViewGroup p8, int p9)
    {
        IllegalStateException v2_0 = this.c.inflate(p9, p8, 0);
        try {
            android.widget.TextView v3_0;
            if (this.d != 0) {
                v3_0 = ((android.widget.TextView) v2_0.findViewById(this.d));
            } else {
                v3_0 = ((android.widget.TextView) v2_0);
            }
        } catch (int v1_5) {
            android.util.Log.e("ArrayAdapter", "You must supply a resource ID for a TextView");
            throw new IllegalStateException("ArrayAdapter requires the resource ID to be a TextView", v1_5);
        }
        int v1_8 = ((String) this.getItem(p7));
        if (!(v1_8 instanceof CharSequence)) {
            v3_0.setText(v1_8.toString());
        } else {
            v3_0.setText(v1_8);
        }
        if (!this.isEnabled(p7)) {
            v3_0.setEnabled(0);
            v3_0.setBackgroundColor(-12303292);
        }
        return v2_0;
    }

    private void b(int p3)
    {
        this.a.removeElement(Integer.valueOf(p3));
        return;
    }

    public final void a(int p3)
    {
        this.a.addElement(Integer.valueOf(p3));
        return;
    }

    public final boolean areAllItemsEnabled()
    {
        return 0;
    }

    public final android.view.View getDropDownView(int p2, android.view.View p3, android.view.ViewGroup p4)
    {
        return this.a(p2, p4, this.e);
    }

    public final boolean isEnabled(int p3)
    {
        boolean v0_2;
        if (!this.a.contains(Integer.valueOf(p3))) {
            v0_2 = super.isEnabled(p3);
        } else {
            v0_2 = 0;
        }
        return v0_2;
    }
}
