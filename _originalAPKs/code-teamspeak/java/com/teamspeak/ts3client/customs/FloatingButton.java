package com.teamspeak.ts3client.customs;
public class FloatingButton extends android.view.View {
    android.os.Handler a;
    int b;
    public boolean c;
    private final android.graphics.Paint d;
    private final android.graphics.Paint e;
    private android.graphics.Bitmap f;
    private android.graphics.Paint g;
    private Runnable h;

    public FloatingButton(android.content.Context p3)
    {
        this(p3, 0);
        this.d = new android.graphics.Paint(1);
        this.e = new android.graphics.Paint(1);
        this.a = new android.os.Handler();
        this.b = 0;
        this.g = new android.graphics.Paint(1);
        this.h = new com.teamspeak.ts3client.customs.i(this);
        return;
    }

    public FloatingButton(android.content.Context p2, android.util.AttributeSet p3)
    {
        this(p2, p3, 0);
        return;
    }

    public FloatingButton(android.content.Context p9, android.util.AttributeSet p10, int p11)
    {
        this(p9, p10, p11);
        this.d = new android.graphics.Paint(1);
        this.e = new android.graphics.Paint(1);
        this.a = new android.os.Handler();
        this.b = 0;
        this.g = new android.graphics.Paint(1);
        this.h = new com.teamspeak.ts3client.customs.i(this);
        this.setLayoutParams(new android.view.ViewGroup$LayoutParams(15, 15));
        this.setClickable(1);
        this.d.setStyle(android.graphics.Paint$Style.FILL);
        this.d.setColor(this.getResources().getColor(2131427532));
        this.d.setShadowLayer(1092616192, 0, 1080033280, this.getResources().getColor(2131427482));
        this.a(0);
        if (android.os.Build$VERSION.SDK_INT >= 11) {
            this.setLayerType(1, 0);
        }
        return;
    }

    static synthetic android.graphics.Paint a(com.teamspeak.ts3client.customs.FloatingButton p1)
    {
        return p1.g;
    }

    private void a()
    {
        if (!this.c) {
            this.a(1);
            this.invalidate();
        }
        return;
    }

    private void b()
    {
        this.a(0);
        this.invalidate();
        return;
    }

    public final void a(boolean p7)
    {
        this.c = p7;
        this.g.setStyle(android.graphics.Paint$Style.FILL);
        this.g.setColor(this.getResources().getColor(2131427532));
        if (p7) {
            this.g.setShadowLayer(1084227584, 0, -1110651699, this.getResources().getColor(2131427535));
        } else {
            this.g.setShadowLayer(1084227584, 0, -1110651699, this.getResources().getColor(2131427534));
        }
        return;
    }

    protected void onDraw(android.graphics.Canvas p7)
    {
        p7.drawCircle(((float) (this.getWidth() / 2)), ((float) (this.getHeight() / 2)), ((float) (((double) this.getWidth()) / 2.6)), this.g);
        p7.drawCircle(((float) (this.getWidth() / 2)), ((float) (this.getHeight() / 2)), ((float) (((double) this.getWidth()) / 2.6)), this.d);
        if (this.f != null) {
            p7.drawBitmap(this.f, ((float) ((this.getWidth() - this.f.getWidth()) / 2)), ((float) ((this.getHeight() - this.f.getHeight()) / 2)), this.e);
        }
        if (this.c) {
            this.a.postDelayed(this.h, 40);
        }
        return;
    }

    protected void onMeasure(int p4, int p5)
    {
        this.setMeasuredDimension(Math.max(this.getSuggestedMinimumWidth(), android.view.View$MeasureSpec.getSize(p4)), Math.max(this.getSuggestedMinimumHeight(), android.view.View$MeasureSpec.getSize(p5)));
        return;
    }

    public boolean onTouchEvent(android.view.MotionEvent p3)
    {
        boolean v0_2;
        if (p3.getAction() != 1) {
            v0_2 = this.getResources().getColor(2131427533);
        } else {
            v0_2 = this.getResources().getColor(2131427532);
        }
        this.d.setColor(v0_2);
        this.invalidate();
        return super.onTouchEvent(p3);
    }

    public void setBitmap(android.graphics.Bitmap p1)
    {
        this.f = p1;
        this.invalidate();
        return;
    }

    public void setDrawable(android.graphics.drawable.Drawable p2)
    {
        this.f = ((android.graphics.drawable.BitmapDrawable) p2).getBitmap();
        this.invalidate();
        return;
    }
}
