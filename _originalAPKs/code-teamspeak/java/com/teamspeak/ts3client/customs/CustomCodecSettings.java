package com.teamspeak.ts3client.customs;
public class CustomCodecSettings extends android.widget.LinearLayout {
    private com.teamspeak.ts3client.data.d.ab a;
    private int b;
    private android.widget.SeekBar c;
    private android.widget.TextView d;
    private com.teamspeak.ts3client.Ts3Application e;
    private android.widget.TextView f;
    private android.widget.TextView g;
    private android.widget.Spinner h;
    private android.widget.Spinner i;
    private boolean j;
    private java.util.BitSet k;
    private int l;
    private com.teamspeak.ts3client.customs.a m;
    private com.teamspeak.ts3client.customs.a n;
    private boolean o;
    private boolean p;
    private boolean q;

    public CustomCodecSettings(android.content.Context p5)
    {
        this(p5);
        this.a = new com.teamspeak.ts3client.data.d.ab(0, 0);
        this.b = 0;
        this.j = 0;
        this.k = new java.util.BitSet();
        this.l = 0;
        this.e = ((com.teamspeak.ts3client.Ts3Application) p5.getApplicationContext());
        boolean v0_7 = android.view.LayoutInflater.from(p5).inflate(2130903072, 0, 0);
        this.a(v0_7);
        this.addView(v0_7);
        if (!v0_7.isInEditMode()) {
            this.b();
        }
        return;
    }

    public CustomCodecSettings(android.content.Context p5, android.util.AttributeSet p6)
    {
        this(p5, p6);
        this.a = new com.teamspeak.ts3client.data.d.ab(0, 0);
        this.b = 0;
        this.j = 0;
        this.k = new java.util.BitSet();
        this.l = 0;
        android.view.View v1_1 = android.view.LayoutInflater.from(p5).inflate(2130903072, 0, 0);
        if (!v1_1.isInEditMode()) {
            this.e = ((com.teamspeak.ts3client.Ts3Application) p5.getApplicationContext());
            this.a(v1_1);
        }
        this.addView(v1_1);
        if (!v1_1.isInEditMode()) {
            this.b();
        }
        return;
    }

    private void a()
    {
        if (this.o) {
            if (this.l < this.a.b) {
                this.a.b = this.l;
            }
            this.c.setProgress(this.a.b);
        }
        if (this.l == 0) {
            this.c.setEnabled(0);
        }
        if ((!this.k.get(com.teamspeak.ts3client.data.d.y.b)) || ((!this.q) && (!this.o))) {
            this.n.a(4);
            if (this.o) {
                if (this.a.a == 4) {
                    this.a.a = 2;
                }
                this.h.setSelection(2);
            }
        }
        if ((!this.k.get(com.teamspeak.ts3client.data.d.y.c)) || ((!this.q) && (!this.o))) {
            this.n.a(2);
            if (this.o) {
                if (this.a.a == 2) {
                    this.a.a = 1;
                }
                this.h.setSelection(1);
            }
        }
        if ((!this.k.get(com.teamspeak.ts3client.data.d.y.d)) || ((!this.q) && (!this.o))) {
            this.n.a(1);
            if (this.o) {
                if (this.a.a == 1) {
                    this.a.a = 0;
                }
                this.h.setSelection(0);
            }
        }
        if ((!this.k.get(com.teamspeak.ts3client.data.d.y.e)) || ((!this.q) && (!this.o))) {
            this.n.a(0);
            if (this.o) {
                if (this.a.a == 0) {
                    this.a.a = 5;
                }
                this.h.setSelection(5);
            }
        }
        if ((!this.k.get(com.teamspeak.ts3client.data.d.y.a)) || ((!this.q) && (!this.o))) {
            this.n.a(5);
            if (this.o) {
                if (this.a.a == 5) {
                    this.a.a = 4;
                }
                this.h.setSelection(4);
            }
        }
        if ((!this.k.get(com.teamspeak.ts3client.data.d.y.f)) || ((!this.q) && (!this.o))) {
            this.n.a(3);
            if (this.o) {
                if (this.a.a == 3) {
                    this.a.a = 2;
                }
                this.h.setSelection(2);
            }
        }
        if ((!this.k.get(com.teamspeak.ts3client.data.d.y.a)) && (!this.k.get(com.teamspeak.ts3client.data.d.y.f))) {
            this.m.a(2);
        }
        if ((!this.k.get(com.teamspeak.ts3client.data.d.y.e)) && ((!this.k.get(com.teamspeak.ts3client.data.d.y.d)) && ((!this.k.get(com.teamspeak.ts3client.data.d.y.c)) && (!this.k.get(com.teamspeak.ts3client.data.d.y.b))))) {
            this.m.a(0);
            this.m.a(1);
        }
        if ((!this.k.get(com.teamspeak.ts3client.data.d.y.a)) && (!this.k.get(com.teamspeak.ts3client.data.d.y.f))) {
            this.m.a(2);
            this.m.a(3);
        }
        if ((this.k.nextSetBit(0) == -1) || ((!this.q) && (!this.o))) {
            this.m.a(0);
            this.m.a(1);
            this.m.a(2);
            this.m.a(3);
        }
        if ((!this.o) && (!this.p)) {
            this.c.setEnabled(0);
        }
        return;
    }

    private void a(android.view.View p5)
    {
        com.teamspeak.ts3client.data.e.a.a("channeldialog.editpreset.text", p5, 2131493026);
        com.teamspeak.ts3client.data.e.a.a("channeldialog.editcodec.text", p5, 2131493029);
        com.teamspeak.ts3client.data.e.a.a("channeldialog.editquality.text", p5, 2131493032);
        this.c = ((android.widget.SeekBar) p5.findViewById(2131493033));
        this.d = ((android.widget.TextView) p5.findViewById(2131493035));
        this.f = ((android.widget.TextView) p5.findViewById(2131493036));
        this.g = ((android.widget.TextView) p5.findViewById(2131493034));
        this.h = ((android.widget.Spinner) p5.findViewById(2131493030));
        this.i = ((android.widget.Spinner) p5.findViewById(2131493027));
        this.m = com.teamspeak.ts3client.data.e.a.b("channeldialog.editpreset", this.getContext(), 4);
        this.i.setAdapter(this.m);
        this.n = com.teamspeak.ts3client.data.e.a.b("channeldialog.editcodec", this.getContext(), 6);
        this.h.setAdapter(this.n);
        this.c.setMax(10);
        this.c.setProgress(0);
        this.c.setFocusable(1);
        this.c.setFocusableInTouchMode(1);
        this.i.setOnItemSelectedListener(new com.teamspeak.ts3client.customs.b(this));
        this.h.setOnItemSelectedListener(new com.teamspeak.ts3client.customs.c(this));
        this.c.setOnSeekBarChangeListener(new com.teamspeak.ts3client.customs.d(this));
        this.getPerms();
        return;
    }

    static synthetic boolean a(com.teamspeak.ts3client.customs.CustomCodecSettings p1)
    {
        return p1.j;
    }

    static synthetic boolean a(com.teamspeak.ts3client.customs.CustomCodecSettings p0, boolean p1)
    {
        p0.j = p1;
        return p1;
    }

    static synthetic java.util.BitSet b(com.teamspeak.ts3client.customs.CustomCodecSettings p1)
    {
        return p1.k;
    }

    private void b()
    {
        android.widget.Spinner v0_2 = com.teamspeak.ts3client.data.d.y.a(this.a.a, this.a.b, this.k, this.l);
        this.j = 1;
        this.c.setProgress(this.a.b);
        this.h.setSelection(this.a.a);
        switch (com.teamspeak.ts3client.customs.e.a[v0_2.ordinal()]) {
            case 1:
                this.i.setSelection(0);
                break;
            case 2:
                this.i.setSelection(1);
                break;
            case 3:
                this.i.setSelection(2);
                break;
            case 4:
                this.i.setSelection(3);
                break;
            default:
                this.i.setSelection(3);
        }
        this.j = 0;
        return;
    }

    static synthetic int c(com.teamspeak.ts3client.customs.CustomCodecSettings p1)
    {
        return p1.l;
    }

    private void c()
    {
        long v0_2 = com.teamspeak.ts3client.data.d.y.a(this.h.getSelectedItemPosition(), this.c.getProgress());
        this.d.setText(new StringBuilder().append(new java.text.DecimalFormat("#.##").format(((double) ((((float) v0_2) / 1149239296) / 1090519040)))).append(" KiB/s").toString());
        this.setWarn(v0_2);
        return;
    }

    static synthetic android.widget.SeekBar d(com.teamspeak.ts3client.customs.CustomCodecSettings p1)
    {
        return p1.c;
    }

    static synthetic android.widget.Spinner e(com.teamspeak.ts3client.customs.CustomCodecSettings p1)
    {
        return p1.h;
    }

    static synthetic com.teamspeak.ts3client.data.d.ab f(com.teamspeak.ts3client.customs.CustomCodecSettings p1)
    {
        return p1.a;
    }

    static synthetic void g(com.teamspeak.ts3client.customs.CustomCodecSettings p8)
    {
        long v0_2 = com.teamspeak.ts3client.data.d.y.a(p8.h.getSelectedItemPosition(), p8.c.getProgress());
        p8.d.setText(new StringBuilder().append(new java.text.DecimalFormat("#.##").format(((double) ((((float) v0_2) / 1149239296) / 1090519040)))).append(" KiB/s").toString());
        p8.setWarn(v0_2);
        return;
    }

    private void getPerms()
    {
        if (this.e.a.s.a(this.e.a.u.a(com.teamspeak.ts3client.jni.g.aQ))) {
            this.k.set(0);
        }
        if (this.e.a.s.a(this.e.a.u.a(com.teamspeak.ts3client.jni.g.aP))) {
            this.k.set(1);
        }
        if (this.e.a.s.a(this.e.a.u.a(com.teamspeak.ts3client.jni.g.aN))) {
            this.k.set(2);
        }
        if (this.e.a.s.a(this.e.a.u.a(com.teamspeak.ts3client.jni.g.aM))) {
            this.k.set(3);
        }
        if (this.e.a.s.a(this.e.a.u.a(com.teamspeak.ts3client.jni.g.aL))) {
            this.k.set(4);
        }
        if (this.e.a.s.a(this.e.a.u.a(com.teamspeak.ts3client.jni.g.aO))) {
            this.k.set(5);
        }
        this.q = this.e.a.s.a(com.teamspeak.ts3client.jni.g.bj);
        this.p = this.e.a.s.a(com.teamspeak.ts3client.jni.g.bk);
        this.l = this.e.a.s.b(this.e.a.u.a(com.teamspeak.ts3client.jni.g.aR));
        return;
    }

    static synthetic android.widget.Spinner h(com.teamspeak.ts3client.customs.CustomCodecSettings p1)
    {
        return p1.i;
    }

    static synthetic boolean i(com.teamspeak.ts3client.customs.CustomCodecSettings p1)
    {
        return p1.o;
    }

    static synthetic int j(com.teamspeak.ts3client.customs.CustomCodecSettings p1)
    {
        return p1.b;
    }

    static synthetic android.widget.TextView k(com.teamspeak.ts3client.customs.CustomCodecSettings p1)
    {
        return p1.g;
    }

    private void setWarn(long p4)
    {
        this.f.setText("");
        if (p4 >= 29491) {
            this.f.setText(com.teamspeak.ts3client.data.e.a.a("codec.warning"));
        }
        return;
    }

    public final void a(com.teamspeak.ts3client.data.d.ab p9, boolean p10)
    {
        this.a = p9;
        if (p10) {
            this.a = com.teamspeak.ts3client.data.d.y.a(com.teamspeak.ts3client.data.d.aa.c, this.k, this.l);
        }
        this.b = p9.b;
        if (this.b < this.l) {
            this.b = this.l;
        }
        this.o = p10;
        if (this.o) {
            if (this.l < this.a.b) {
                this.a.b = this.l;
            }
            this.c.setProgress(this.a.b);
        }
        if (this.l == 0) {
            this.c.setEnabled(0);
        }
        if ((!this.k.get(com.teamspeak.ts3client.data.d.y.b)) || ((!this.q) && (!this.o))) {
            this.n.a(4);
            if (this.o) {
                if (this.a.a == 4) {
                    this.a.a = 2;
                }
                this.h.setSelection(2);
            }
        }
        if ((!this.k.get(com.teamspeak.ts3client.data.d.y.c)) || ((!this.q) && (!this.o))) {
            this.n.a(2);
            if (this.o) {
                if (this.a.a == 2) {
                    this.a.a = 1;
                }
                this.h.setSelection(1);
            }
        }
        if ((!this.k.get(com.teamspeak.ts3client.data.d.y.d)) || ((!this.q) && (!this.o))) {
            this.n.a(1);
            if (this.o) {
                if (this.a.a == 1) {
                    this.a.a = 0;
                }
                this.h.setSelection(0);
            }
        }
        if ((!this.k.get(com.teamspeak.ts3client.data.d.y.e)) || ((!this.q) && (!this.o))) {
            this.n.a(0);
            if (this.o) {
                if (this.a.a == 0) {
                    this.a.a = 5;
                }
                this.h.setSelection(5);
            }
        }
        if ((!this.k.get(com.teamspeak.ts3client.data.d.y.a)) || ((!this.q) && (!this.o))) {
            this.n.a(5);
            if (this.o) {
                if (this.a.a == 5) {
                    this.a.a = 4;
                }
                this.h.setSelection(4);
            }
        }
        if ((!this.k.get(com.teamspeak.ts3client.data.d.y.f)) || ((!this.q) && (!this.o))) {
            this.n.a(3);
            if (this.o) {
                if (this.a.a == 3) {
                    this.a.a = 2;
                }
                this.h.setSelection(2);
            }
        }
        if ((!this.k.get(com.teamspeak.ts3client.data.d.y.a)) && (!this.k.get(com.teamspeak.ts3client.data.d.y.f))) {
            this.m.a(2);
        }
        if ((!this.k.get(com.teamspeak.ts3client.data.d.y.e)) && ((!this.k.get(com.teamspeak.ts3client.data.d.y.d)) && ((!this.k.get(com.teamspeak.ts3client.data.d.y.c)) && (!this.k.get(com.teamspeak.ts3client.data.d.y.b))))) {
            this.m.a(0);
            this.m.a(1);
        }
        if ((!this.k.get(com.teamspeak.ts3client.data.d.y.a)) && (!this.k.get(com.teamspeak.ts3client.data.d.y.f))) {
            this.m.a(2);
            this.m.a(3);
        }
        if ((this.k.nextSetBit(0) == -1) || ((!this.q) && (!this.o))) {
            this.m.a(0);
            this.m.a(1);
            this.m.a(2);
            this.m.a(3);
        }
        if ((!this.o) && (!this.p)) {
            this.c.setEnabled(0);
        }
        this.b();
        return;
    }

    public com.teamspeak.ts3client.data.d.ab getSettings()
    {
        return this.a;
    }
}
