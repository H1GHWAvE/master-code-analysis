package com.teamspeak.ts3client.customs;
final class b implements android.widget.AdapterView$OnItemSelectedListener {
    final synthetic com.teamspeak.ts3client.customs.CustomCodecSettings a;

    b(com.teamspeak.ts3client.customs.CustomCodecSettings p1)
    {
        this.a = p1;
        return;
    }

    public final void onItemSelected(android.widget.AdapterView p4, android.view.View p5, int p6, long p7)
    {
        if (!com.teamspeak.ts3client.customs.CustomCodecSettings.a(this.a)) {
            com.teamspeak.ts3client.customs.CustomCodecSettings.a(this.a, 1);
            switch (p6) {
                case 0:
                    int v0_10 = com.teamspeak.ts3client.data.d.y.a(com.teamspeak.ts3client.data.d.aa.b, com.teamspeak.ts3client.customs.CustomCodecSettings.b(this.a), com.teamspeak.ts3client.customs.CustomCodecSettings.c(this.a));
                    com.teamspeak.ts3client.customs.CustomCodecSettings.d(this.a).setProgress(v0_10.b);
                    com.teamspeak.ts3client.customs.CustomCodecSettings.e(this.a).setSelection(v0_10.a);
                    com.teamspeak.ts3client.customs.CustomCodecSettings.f(this.a).a = v0_10.a;
                    com.teamspeak.ts3client.customs.CustomCodecSettings.f(this.a).b = v0_10.b;
                    break;
                case 1:
                    int v0_7 = com.teamspeak.ts3client.data.d.y.a(com.teamspeak.ts3client.data.d.aa.c, com.teamspeak.ts3client.customs.CustomCodecSettings.b(this.a), com.teamspeak.ts3client.customs.CustomCodecSettings.c(this.a));
                    com.teamspeak.ts3client.customs.CustomCodecSettings.d(this.a).setProgress(v0_7.b);
                    com.teamspeak.ts3client.customs.CustomCodecSettings.e(this.a).setSelection(v0_7.a);
                    com.teamspeak.ts3client.customs.CustomCodecSettings.f(this.a).a = v0_7.a;
                    com.teamspeak.ts3client.customs.CustomCodecSettings.f(this.a).b = v0_7.b;
                    break;
                case 2:
                    int v0_4 = com.teamspeak.ts3client.data.d.y.a(com.teamspeak.ts3client.data.d.aa.d, com.teamspeak.ts3client.customs.CustomCodecSettings.b(this.a), com.teamspeak.ts3client.customs.CustomCodecSettings.c(this.a));
                    com.teamspeak.ts3client.customs.CustomCodecSettings.d(this.a).setProgress(v0_4.b);
                    com.teamspeak.ts3client.customs.CustomCodecSettings.e(this.a).setSelection(v0_4.a);
                    com.teamspeak.ts3client.customs.CustomCodecSettings.f(this.a).a = v0_4.a;
                    com.teamspeak.ts3client.customs.CustomCodecSettings.f(this.a).b = v0_4.b;
                    break;
            }
            com.teamspeak.ts3client.customs.CustomCodecSettings.a(this.a, 0);
            com.teamspeak.ts3client.customs.CustomCodecSettings.g(this.a);
        }
        return;
    }

    public final void onNothingSelected(android.widget.AdapterView p1)
    {
        return;
    }
}
