package com.teamspeak.ts3client;
final class ah extends android.telephony.PhoneStateListener {
    boolean a;
    int b;
    final synthetic com.teamspeak.ts3client.t c;

    ah(com.teamspeak.ts3client.t p2)
    {
        this.c = p2;
        this.a = 0;
        this.b = 0;
        return;
    }

    private void a()
    {
        if (this.a) {
            if ((com.teamspeak.ts3client.t.b(this.c).a != null) && ((com.teamspeak.ts3client.t.b(this.c).a.k != null) && (com.teamspeak.ts3client.t.b(this.c).e.getBoolean("call_setaway", 1)))) {
                com.teamspeak.ts3client.t.b(this.c).a.a.a(com.teamspeak.ts3client.t.b(this.c).a.e, com.teamspeak.ts3client.jni.d.N, "");
                com.teamspeak.ts3client.t.b(this.c).a.a.a(com.teamspeak.ts3client.t.b(this.c).a.e, com.teamspeak.ts3client.jni.d.M, 0);
                com.teamspeak.ts3client.t.b(this.c).a.a.ts3client_flushClientSelfUpdates(com.teamspeak.ts3client.t.b(this.c).a.e, "");
                com.teamspeak.ts3client.t.b(this.c).a.v.s.clear(0);
                com.teamspeak.ts3client.t.b(this.c).a.v.s.clear(1);
                com.teamspeak.ts3client.t.b(this.c).a.v.s.clear(2);
                com.teamspeak.ts3client.t.b(this.c).a.v.t = "";
            }
            this.a = 0;
            com.teamspeak.ts3client.data.ab v0_46 = com.teamspeak.ts3client.t.b(this.c).a.k;
            if (v0_46.f) {
                v0_46.h.setBitmap(com.teamspeak.ts3client.data.d.t.a(2130837656, ((float) v0_46.d), ((float) v0_46.d)));
            } else {
                v0_46.c = 0;
                v0_46.a.g().a = 0;
                v0_46.a.a.a.a(v0_46.a.a.e, com.teamspeak.ts3client.jni.d.g, 0);
                v0_46.a.a.a.ts3client_flushClientSelfUpdates(v0_46.a.a.e, "");
                v0_46.h.setBitmap(com.teamspeak.ts3client.data.d.t.a(2130837654, ((float) v0_46.d), ((float) v0_46.d)));
                v0_46.a.a.v.s.set(1);
            }
            if (v0_46.e) {
                v0_46.g.setBitmap(com.teamspeak.ts3client.data.d.t.a(2130837636, ((float) v0_46.d), ((float) v0_46.d)));
            } else {
                v0_46.b = 0;
                v0_46.a.a.a.a(v0_46.a.a.e, com.teamspeak.ts3client.jni.d.f, 0);
                v0_46.a.a.a.ts3client_flushClientSelfUpdates(v0_46.a.a.e, "");
                v0_46.g.setBitmap(com.teamspeak.ts3client.data.d.t.a(2130837633, ((float) v0_46.d), ((float) v0_46.d)));
                v0_46.a.a.v.s.set(0);
            }
            new android.os.Handler().postDelayed(new com.teamspeak.ts3client.as(v0_46), 1000);
        }
        return;
    }

    private void b()
    {
        if ((!this.a) && (com.teamspeak.ts3client.t.b(this.c).a.k != null)) {
            com.teamspeak.ts3client.a.k v0_8 = com.teamspeak.ts3client.t.b(this.c).a.k;
            v0_8.e = v0_8.b;
            v0_8.f = v0_8.c;
            v0_8.a.g().a = 1;
            v0_8.g.setBitmap(com.teamspeak.ts3client.data.d.t.a(2130837636, ((float) v0_8.d), ((float) v0_8.d)));
            v0_8.h.setBitmap(com.teamspeak.ts3client.data.d.t.a(2130837656, ((float) v0_8.d), ((float) v0_8.d)));
            v0_8.c = 1;
            v0_8.a.a.a.a(v0_8.a.a.e, com.teamspeak.ts3client.jni.d.g, 1);
            v0_8.a.a.a.ts3client_flushClientSelfUpdates(v0_8.a.a.e, "");
            v0_8.b = 1;
            v0_8.a.a.a.a(v0_8.a.a.e, com.teamspeak.ts3client.jni.d.f, 1);
            v0_8.a.a.a.ts3client_flushClientSelfUpdates(v0_8.a.a.e, "");
            if (com.teamspeak.ts3client.t.b(this.c).e.getBoolean("call_setaway", 1)) {
                com.teamspeak.ts3client.t.b(this.c).a.a.a(com.teamspeak.ts3client.t.b(this.c).a.e, com.teamspeak.ts3client.jni.d.N, com.teamspeak.ts3client.t.b(this.c).e.getString("call_awaymessage", "On the Phone"));
                com.teamspeak.ts3client.t.b(this.c).a.a.a(com.teamspeak.ts3client.t.b(this.c).a.e, com.teamspeak.ts3client.jni.d.M, 1);
                com.teamspeak.ts3client.t.b(this.c).a.a.ts3client_flushClientSelfUpdates(com.teamspeak.ts3client.t.b(this.c).a.e, "");
                com.teamspeak.ts3client.t.b(this.c).a.v.s.set(0);
                com.teamspeak.ts3client.t.b(this.c).a.v.s.set(1);
                com.teamspeak.ts3client.t.b(this.c).a.v.s.set(2);
                com.teamspeak.ts3client.t.b(this.c).a.v.t = com.teamspeak.ts3client.t.b(this.c).e.getString("call_awaymessage", "On the Phone");
            }
            com.teamspeak.ts3client.t.b(this.c).a.b.a(Boolean.valueOf(0));
            this.a = 1;
            com.teamspeak.ts3client.t.b(this.c).a.b.g();
        }
        return;
    }

    public final void onCallStateChanged(int p8, String p9)
    {
        if (((com.teamspeak.ts3client.t.b(this.c).a.a.a(com.teamspeak.ts3client.t.b(this.c).a.e, com.teamspeak.ts3client.jni.d.M) != 1) || (this.a)) && (p8 != this.b)) {
            this.b = p8;
            com.teamspeak.ts3client.t.b(this.c).d.log(java.util.logging.Level.INFO, new StringBuilder("CallState: ").append(p8).toString());
            switch (p8) {
                case 0:
                    if (this.a) {
                        if ((com.teamspeak.ts3client.t.b(this.c).a != null) && ((com.teamspeak.ts3client.t.b(this.c).a.k != null) && (com.teamspeak.ts3client.t.b(this.c).e.getBoolean("call_setaway", 1)))) {
                            com.teamspeak.ts3client.t.b(this.c).a.a.a(com.teamspeak.ts3client.t.b(this.c).a.e, com.teamspeak.ts3client.jni.d.N, "");
                            com.teamspeak.ts3client.t.b(this.c).a.a.a(com.teamspeak.ts3client.t.b(this.c).a.e, com.teamspeak.ts3client.jni.d.M, 0);
                            com.teamspeak.ts3client.t.b(this.c).a.a.ts3client_flushClientSelfUpdates(com.teamspeak.ts3client.t.b(this.c).a.e, "");
                            com.teamspeak.ts3client.t.b(this.c).a.v.s.clear(0);
                            com.teamspeak.ts3client.t.b(this.c).a.v.s.clear(1);
                            com.teamspeak.ts3client.t.b(this.c).a.v.s.clear(2);
                            com.teamspeak.ts3client.t.b(this.c).a.v.t = "";
                        }
                        this.a = 0;
                        com.teamspeak.ts3client.data.ab v0_56 = com.teamspeak.ts3client.t.b(this.c).a.k;
                        if (v0_56.f) {
                            v0_56.h.setBitmap(com.teamspeak.ts3client.data.d.t.a(2130837656, ((float) v0_56.d), ((float) v0_56.d)));
                        } else {
                            v0_56.c = 0;
                            v0_56.a.g().a = 0;
                            v0_56.a.a.a.a(v0_56.a.a.e, com.teamspeak.ts3client.jni.d.g, 0);
                            v0_56.a.a.a.ts3client_flushClientSelfUpdates(v0_56.a.a.e, "");
                            v0_56.h.setBitmap(com.teamspeak.ts3client.data.d.t.a(2130837654, ((float) v0_56.d), ((float) v0_56.d)));
                            v0_56.a.a.v.s.set(1);
                        }
                        if (v0_56.e) {
                            v0_56.g.setBitmap(com.teamspeak.ts3client.data.d.t.a(2130837636, ((float) v0_56.d), ((float) v0_56.d)));
                        } else {
                            v0_56.b = 0;
                            v0_56.a.a.a.a(v0_56.a.a.e, com.teamspeak.ts3client.jni.d.f, 0);
                            v0_56.a.a.a.ts3client_flushClientSelfUpdates(v0_56.a.a.e, "");
                            v0_56.g.setBitmap(com.teamspeak.ts3client.data.d.t.a(2130837633, ((float) v0_56.d), ((float) v0_56.d)));
                            v0_56.a.a.v.s.set(0);
                        }
                        new android.os.Handler().postDelayed(new com.teamspeak.ts3client.as(v0_56), 1000);
                    }
                    break;
                case 1:
                    this.b();
                    break;
                case 2:
                    this.b();
                    break;
                default:
            }
        }
        return;
    }
}
