package com.teamspeak.ts3client.a;
public final class k {
    private static com.teamspeak.ts3client.a.k m;
    boolean a;
    short[] b;
    android.media.AudioRecord c;
    public com.teamspeak.ts3client.jni.Ts3Jni d;
    boolean e;
    android.media.AudioTrack f;
    public android.media.AudioManager g;
    com.teamspeak.ts3client.Ts3Application h;
    int i;
    int j;
    public boolean k;
    public boolean l;
    private Thread n;
    private int o;
    private int p;
    private int q;
    private Thread r;
    private int s;
    private int t;
    private int u;
    private boolean v;

    public k()
    {
        this.a = 0;
        this.e = 0;
        this.h = com.teamspeak.ts3client.Ts3Application.a();
        return;
    }

    private static synthetic android.media.AudioTrack a(com.teamspeak.ts3client.a.k p1)
    {
        return p1.f;
    }

    public static declared_synchronized com.teamspeak.ts3client.a.k a()
    {
        try {
            if (com.teamspeak.ts3client.a.k.m == null) {
                com.teamspeak.ts3client.a.k.m = new com.teamspeak.ts3client.a.k();
            }
        } catch (com.teamspeak.ts3client.a.k v0_3) {
            throw v0_3;
        }
        return com.teamspeak.ts3client.a.k.m;
    }

    public static java.util.Vector a(android.content.Context p14, int p15)
    {
        java.util.Vector v7_1 = new java.util.Vector();
        int v10 = (android.media.AudioTrack.getNativeOutputSampleRate(p15) * 2);
        int[] v11 = new int[5];
        v11 = {44100, 32000, 22050, 16000, 8000};
        p14.getApplicationContext();
        int v8 = 0;
        while (v8 < 5) {
            int v2 = v11[v8];
            if (v2 < v10) {
                try {
                    int v5 = android.media.AudioTrack.getMinBufferSize(v2, 4, 2);
                } catch (android.media.AudioTrack v0) {
                    android.util.Log.d("Audio", new StringBuilder("notSupportedplay: ").append(v2).toString());
                }
                if (v5 != -2) {
                    android.media.AudioTrack v0_7 = new android.media.AudioTrack(p15, v2, 4, 2, v5, 1);
                    if (v0_7.getState() == 1) {
                        v7_1.add(Integer.valueOf(v2));
                    }
                    v0_7.release();
                }
            }
            v8++;
        }
        android.media.AudioTrack v0_2;
        if (v7_1.size() > 0) {
            v0_2 = v7_1;
        } else {
            v7_1.add(Integer.valueOf(0));
            v0_2 = v7_1;
        }
        return v0_2;
    }

    public static java.util.Vector a(android.content.Context p12, int p13, int p14)
    {
        java.util.Vector v6_1 = new java.util.Vector();
        int v9 = (android.media.AudioTrack.getNativeOutputSampleRate(p13) * 2);
        int[] v10 = new int[5];
        v10 = {44100, 32000, 22050, 16000, 8000};
        p12.getApplicationContext();
        int v7 = 0;
        while (v7 < 5) {
            int v2 = v10[v7];
            if (v2 < v9) {
                try {
                    int v5 = android.media.AudioRecord.getMinBufferSize(v2, 16, 2);
                } catch (android.media.AudioRecord v0) {
                    android.util.Log.d("Audio", new StringBuilder("notSupportedrec: ").append(v2).toString());
                }
                if (v5 != -2) {
                    android.media.AudioRecord v0_7 = new android.media.AudioRecord(p14, v2, 16, 2, v5);
                    if (v0_7.getState() == 1) {
                        v6_1.add(Integer.valueOf(v2));
                    }
                    v0_7.release();
                }
            }
            v7++;
        }
        android.media.AudioRecord v0_2;
        if (v6_1.size() > 0) {
            v0_2 = v6_1;
        } else {
            v6_1.add(Integer.valueOf(0));
            v0_2 = v6_1;
        }
        return v0_2;
    }

    private void a(com.teamspeak.ts3client.jni.Ts3Jni p1)
    {
        this.d = p1;
        return;
    }

    private static synthetic com.teamspeak.ts3client.Ts3Application b(com.teamspeak.ts3client.a.k p1)
    {
        return p1.h;
    }

    private static synthetic boolean c(com.teamspeak.ts3client.a.k p1)
    {
        return p1.e;
    }

    private static synthetic com.teamspeak.ts3client.jni.Ts3Jni d(com.teamspeak.ts3client.a.k p1)
    {
        return p1.d;
    }

    private static synthetic int e(com.teamspeak.ts3client.a.k p1)
    {
        return p1.j;
    }

    private static synthetic android.media.AudioRecord f(com.teamspeak.ts3client.a.k p1)
    {
        return p1.c;
    }

    private static synthetic boolean g(com.teamspeak.ts3client.a.k p1)
    {
        return p1.a;
    }

    private void h()
    {
        this.k = 1;
        this.g.setMicrophoneMute(this.k);
        return;
    }

    private static synthetic short[] h(com.teamspeak.ts3client.a.k p1)
    {
        return p1.b;
    }

    private static synthetic int i(com.teamspeak.ts3client.a.k p1)
    {
        return p1.i;
    }

    private void i()
    {
        if (!this.l) {
            if (this.g != null) {
                this.g.stopBluetoothSco();
                this.g.setMicrophoneMute(0);
                this.g.setSpeakerphoneOn(0);
                this.g.setMode(0);
            }
            this.g = 0;
        }
        return;
    }

    private void j()
    {
        this.g.setStreamVolume(this.t, 15, 0);
        return;
    }

    public final void a(Boolean p3)
    {
        if (this.g != null) {
            this.g.setSpeakerphoneOn(p3.booleanValue());
        }
        return;
    }

    public final void a(boolean p5)
    {
        try {
            if (!p5) {
                this.g.adjustStreamVolume(this.t, -1, 1);
            } else {
                this.g.adjustStreamVolume(this.t, 1, 1);
            }
        } catch (Exception v0) {
        }
        return;
    }

    public final boolean a(int p11, int p12, int p13, int p14)
    {
        int v6 = 1;
        this.l = 1;
        if (this.c != null) {
            this.f();
            this.c.release();
        }
        if (this.f != null) {
            this.e();
            this.f.release();
        }
        this.i = p11;
        this.j = p12;
        this.t = p13;
        this.u = p14;
        this.p = 2;
        this.o = 16;
        this.q = android.media.AudioRecord.getMinBufferSize(p11, this.o, this.p);
        if (this.q != -2) {
            this.c = new android.media.AudioRecord(p14, p11, this.o, this.p, this.q);
            this.h.d.log(java.util.logging.Level.INFO, new StringBuilder("recSa:").append(p11).append(" PlaySa:").append(p12).append(" RecS:").append(p14).append(" PlayS:").append(p13).toString());
            android.media.AudioManager v0_14 = new short[((p11 / 100) * 2)];
            this.b = v0_14;
            this.s = android.media.AudioTrack.getMinBufferSize(p12, 4, this.p);
            this.f = new android.media.AudioTrack(p13, p12, 4, 2, this.s, 1);
            this.g = ((android.media.AudioManager) this.h.getSystemService("audio"));
            this.a(Boolean.valueOf(this.h.e.getBoolean("audio_handfree", 0)));
            if (p13 == 3) {
                this.g.setMode(2);
            }
        } else {
            v6 = 0;
        }
        return v6;
    }

    public final void b()
    {
        this.h.d.log(java.util.logging.Level.INFO, "Release Audio");
        if (this.c != null) {
            this.c.release();
        }
        this.c = 0;
        if (this.f != null) {
            this.f.release();
        }
        this.f = 0;
        this.l = 0;
        return;
    }

    public final void b(boolean p3)
    {
        this.v = p3;
        if (this.g != null) {
            if (!p3) {
                try {
                    this.g.stopBluetoothSco();
                    this.g.setBluetoothScoOn(0);
                } catch (Exception v0) {
                }
            } else {
                this.g.isBluetoothScoAvailableOffCall();
                this.g.setBluetoothScoOn(1);
                try {
                    this.g.startBluetoothSco();
                } catch (Exception v0) {
                }
            }
            com.teamspeak.ts3client.a.j.a().b();
        }
        return;
    }

    public final void c()
    {
        if (!this.e) {
            this.e = 1;
            this.r = new com.teamspeak.ts3client.a.l(this);
            this.r.start();
        }
        return;
    }

    public final void d()
    {
        if (!this.a) {
            this.a = 1;
            this.n = new com.teamspeak.ts3client.a.m(this);
            this.n.start();
        }
        return;
    }

    public final void e()
    {
        this.e = 0;
        if (this.r != null) {
            this.r.interrupt();
        }
        if (this.f != null) {
            try {
                this.f.stop();
                this.f.flush();
            } catch (String v0_6) {
                this.h.d.log(java.util.logging.Level.SEVERE, v0_6.toString());
            }
        }
        return;
    }

    public final void f()
    {
        this.a = 0;
        if (this.n != null) {
            this.n.interrupt();
        }
        if (this.c != null) {
            try {
                this.c.stop();
                this.c.setPositionNotificationPeriod(0);
            } catch (String v0_6) {
                this.h.d.log(java.util.logging.Level.SEVERE, v0_6.toString());
            }
        }
        return;
    }

    public final void g()
    {
        this.k = 0;
        if (this.g != null) {
            this.g.setMicrophoneMute(this.k);
        }
        return;
    }
}
