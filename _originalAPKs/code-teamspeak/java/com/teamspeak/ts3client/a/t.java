package com.teamspeak.ts3client.a;
public final class t implements com.teamspeak.ts3client.a.n {
    java.util.HashMap a;
    String b;
    android.media.SoundPool c;

    public t()
    {
        this.a = new java.util.HashMap();
        this.b = new StringBuilder().append(android.os.Environment.getExternalStorageDirectory()).append("/TS3/content/sound/default/").toString();
        return;
    }

    private static synthetic android.media.SoundPool a(com.teamspeak.ts3client.a.t p1)
    {
        return p1.c;
    }

    public final void a()
    {
        this.c.release();
        this.c = new android.media.SoundPool(4, 0, 0);
        if (android.os.Build$VERSION.SDK_INT < 11) {
            Void[] v1_2 = new Void[0];
            new com.teamspeak.ts3client.a.u(this).execute(v1_2);
        } else {
            Void[] v2_1 = new Void[0];
            new com.teamspeak.ts3client.a.u(this).executeOnExecutor(android.os.AsyncTask.THREAD_POOL_EXECUTOR, v2_1);
        }
        return;
    }

    public final void a(android.content.Context p4)
    {
        this.c = new android.media.SoundPool(4, 0, 0);
        if (android.os.Build$VERSION.SDK_INT < 11) {
            Void[] v1_2 = new Void[0];
            new com.teamspeak.ts3client.a.u(this).execute(v1_2);
        } else {
            Void[] v2_1 = new Void[0];
            new com.teamspeak.ts3client.a.u(this).executeOnExecutor(android.os.AsyncTask.THREAD_POOL_EXECUTOR, v2_1);
        }
        return;
    }

    public final void a(com.teamspeak.ts3client.jni.h p9, com.teamspeak.ts3client.a.o p10)
    {
        if ((!p9.toString().startsWith("CLIENT_")) || (p9.toString().startsWith("CLIENT_R"))) {
            if (this.a.containsKey(p9.toString())) {
                this.c.play(((Integer) this.a.get(p9.toString())).intValue(), 1050253722, 1050253722, 1, 0, 1065353216);
            }
        } else {
            switch (p10.b) {
                case 0:
                    if (this.a.containsKey(new StringBuilder("neutral_").append(p9.toString()).toString())) {
                        this.c.play(((Integer) this.a.get(new StringBuilder("neutral_").append(p9.toString()).toString())).intValue(), 1050253722, 1050253722, 1, 0, 1065353216);
                    }
                    break;
                case 1:
                    if (this.a.containsKey(new StringBuilder("blocked_").append(p9.toString()).toString())) {
                        this.c.play(((Integer) this.a.get(new StringBuilder("blocked_").append(p9.toString()).toString())).intValue(), 1050253722, 1050253722, 1, 0, 1065353216);
                    }
                    break;
                case 2:
                    if (this.a.containsKey(new StringBuilder("friend_").append(p9.toString()).toString())) {
                        this.c.play(((Integer) this.a.get(new StringBuilder("friend_").append(p9.toString()).toString())).intValue(), 1050253722, 1050253722, 1, 0, 1065353216);
                    }
                    break;
            }
        }
        return;
    }

    public final void a(String p1)
    {
        return;
    }

    public final void b()
    {
        this.c.release();
        return;
    }
}
