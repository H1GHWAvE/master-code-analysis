package com.teamspeak.ts3client.a;
public final class j {
    private static com.teamspeak.ts3client.a.j a;
    private java.util.BitSet b;

    private j()
    {
        this.b = new java.util.BitSet();
        return;
    }

    public static com.teamspeak.ts3client.a.j a()
    {
        if (com.teamspeak.ts3client.a.j.a == null) {
            com.teamspeak.ts3client.a.j.a = new com.teamspeak.ts3client.a.j();
        }
        return com.teamspeak.ts3client.a.j.a;
    }

    private java.util.BitSet c()
    {
        return this.b;
    }

    public final void a(int p2)
    {
        this.b.set(p2);
        this.b();
        return;
    }

    public final void b()
    {
        if (!this.b.get(1)) {
            com.teamspeak.ts3client.a.k.a().a(Boolean.valueOf(com.teamspeak.ts3client.Ts3Application.a().e.getBoolean("audio_handfree", 0)));
        } else {
            com.teamspeak.ts3client.a.k.a().a(Boolean.valueOf(0));
        }
        return;
    }

    public final void b(int p2)
    {
        this.b.clear(p2);
        this.b();
        return;
    }
}
