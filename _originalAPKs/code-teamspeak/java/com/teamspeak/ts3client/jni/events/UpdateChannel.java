package com.teamspeak.ts3client.jni.events;
public class UpdateChannel implements com.teamspeak.ts3client.jni.k {
    public long a;
    private long b;

    public UpdateChannel()
    {
        return;
    }

    private UpdateChannel(long p2, long p4)
    {
        this.b = p2;
        this.a = p4;
        com.teamspeak.ts3client.jni.l.a(this);
        return;
    }

    private long a()
    {
        return this.a;
    }

    private long b()
    {
        return this.b;
    }

    public String toString()
    {
        return new StringBuilder("UpdateChannel [serverConnectionHandlerID=").append(this.b).append(", channelID=").append(this.a).append("]").toString();
    }
}
