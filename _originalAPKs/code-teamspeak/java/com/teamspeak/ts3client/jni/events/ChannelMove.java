package com.teamspeak.ts3client.jni.events;
public class ChannelMove implements com.teamspeak.ts3client.jni.k {
    private long a;
    private long b;
    private long c;
    private int d;
    private String e;
    private String f;

    public ChannelMove()
    {
        return;
    }

    private ChannelMove(long p2, long p4, long p6, int p8, String p9, String p10)
    {
        this.a = p2;
        this.b = p4;
        this.c = p6;
        this.d = p8;
        this.e = p9;
        this.f = p10;
        com.teamspeak.ts3client.jni.l.a(this);
        return;
    }

    private String e()
    {
        return this.f;
    }

    private long f()
    {
        return this.a;
    }

    public final long a()
    {
        return this.b;
    }

    public final int b()
    {
        return this.d;
    }

    public final String c()
    {
        return this.e;
    }

    public final long d()
    {
        return this.c;
    }

    public String toString()
    {
        return new StringBuilder("ChannelMove [serverConnectionHandlerID=").append(this.a).append(", channelID=").append(this.b).append(", newChannelParentID=").append(this.c).append(", invokerID=").append(this.d).append(", invokerName=").append(this.e).append(", invokerUniqueIdentifier=").append(this.f).append("]").toString();
    }
}
