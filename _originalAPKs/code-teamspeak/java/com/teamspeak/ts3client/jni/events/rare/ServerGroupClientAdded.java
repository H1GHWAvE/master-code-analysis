package com.teamspeak.ts3client.jni.events.rare;
public class ServerGroupClientAdded implements com.teamspeak.ts3client.jni.k {
    private long a;
    private int b;
    private String c;
    private String d;
    private long e;
    private int f;
    private String g;
    private String h;

    public ServerGroupClientAdded()
    {
        return;
    }

    private ServerGroupClientAdded(long p1, int p3, String p4, String p5, long p6, int p8, String p9, String p10)
    {
        this.a = p1;
        this.b = p3;
        this.c = p4;
        this.d = p5;
        this.e = p6;
        this.f = p8;
        this.g = p9;
        this.h = p10;
        com.teamspeak.ts3client.jni.l.a(this);
        return;
    }

    private long e()
    {
        return this.a;
    }

    private String f()
    {
        return this.d;
    }

    private int g()
    {
        return this.f;
    }

    private String h()
    {
        return this.h;
    }

    public final int a()
    {
        return this.b;
    }

    public final String b()
    {
        return this.c;
    }

    public final long c()
    {
        return this.e;
    }

    public final String d()
    {
        return this.g;
    }

    public String toString()
    {
        return new StringBuilder("ServerGroupClientAdded [serverConnectionHandlerID=").append(this.a).append(", clientID=").append(this.b).append(", clientName=").append(this.c).append(", clientUniqueIdentity=").append(this.d).append(", serverGroupID=").append(this.e).append(", invokerClientID=").append(this.f).append(", invokerName=").append(this.g).append(", invokerUniqueIdentity=").append(this.h).append("]").toString();
    }
}
