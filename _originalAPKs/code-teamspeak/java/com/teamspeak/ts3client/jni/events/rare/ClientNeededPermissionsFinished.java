package com.teamspeak.ts3client.jni.events.rare;
public class ClientNeededPermissionsFinished implements com.teamspeak.ts3client.jni.k {
    public long a;

    public ClientNeededPermissionsFinished()
    {
        return;
    }

    private ClientNeededPermissionsFinished(long p2)
    {
        this.a = p2;
        com.teamspeak.ts3client.jni.l.a(this);
        return;
    }

    private long a()
    {
        return this.a;
    }

    public String toString()
    {
        return new StringBuilder("ClientNeededPermissionsFinished [serverConnectionHandlerID=").append(this.a).append("]").toString();
    }
}
