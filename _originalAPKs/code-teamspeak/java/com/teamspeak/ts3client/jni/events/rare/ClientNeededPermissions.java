package com.teamspeak.ts3client.jni.events.rare;
public class ClientNeededPermissions implements com.teamspeak.ts3client.jni.k {
    public int a;
    public int b;
    private long c;

    public ClientNeededPermissions()
    {
        return;
    }

    private ClientNeededPermissions(long p2, int p4, int p5)
    {
        this.c = p2;
        this.a = p4;
        this.b = p5;
        com.teamspeak.ts3client.jni.l.a(this);
        return;
    }

    private int b()
    {
        return this.b;
    }

    private long c()
    {
        return this.c;
    }

    public final int a()
    {
        return this.a;
    }

    public String toString()
    {
        return new StringBuilder("ClientNeededPermissions [serverConnectionHandlerID=").append(this.c).append(", permissionID=").append(this.a).append(", permissionValue=").append(this.b).append("]").toString();
    }
}
