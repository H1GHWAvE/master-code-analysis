package com.teamspeak.ts3client.jni.events.rare;
public class PermissionListFinished implements com.teamspeak.ts3client.jni.k {
    private long a;

    public PermissionListFinished()
    {
        return;
    }

    private PermissionListFinished(long p2)
    {
        this.a = p2;
        com.teamspeak.ts3client.jni.l.a(this);
        return;
    }

    private long a()
    {
        return this.a;
    }

    public String toString()
    {
        return new StringBuilder("PermissionListFinished [serverConnectionHandlerID=").append(this.a).append("]").toString();
    }
}
