package com.teamspeak.ts3client.jni.events.rare;
public class PermissionList implements com.teamspeak.ts3client.jni.k {
    public int a;
    public String b;
    private long c;
    private String d;

    public PermissionList()
    {
        return;
    }

    private PermissionList(long p2, int p4, String p5, String p6)
    {
        this.c = p2;
        this.a = p4;
        this.b = p5;
        this.d = p6;
        com.teamspeak.ts3client.jni.l.a(this);
        return;
    }

    private String a()
    {
        return this.d;
    }

    private int b()
    {
        return this.a;
    }

    private String c()
    {
        return this.b;
    }

    private long d()
    {
        return this.c;
    }

    public String toString()
    {
        return new StringBuilder("PermissionList [serverConnectionHandlerID=").append(this.c).append(", permissionID=").append(this.a).append(", permissionName=").append(this.b).append(", permissionDescription=").append(this.d).append("]").toString();
    }
}
