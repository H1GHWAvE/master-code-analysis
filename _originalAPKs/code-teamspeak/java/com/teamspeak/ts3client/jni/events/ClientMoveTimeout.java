package com.teamspeak.ts3client.jni.events;
public class ClientMoveTimeout implements com.teamspeak.ts3client.jni.k {
    private long a;
    private int b;
    private long c;
    private long d;
    private com.teamspeak.ts3client.jni.j e;
    private String f;

    public ClientMoveTimeout()
    {
        return;
    }

    private ClientMoveTimeout(long p3, int p5, long p6, long p8, int p10, String p11)
    {
        this.a = p3;
        this.b = p5;
        this.c = p6;
        this.d = p8;
        if (p10 == 0) {
            this.e = com.teamspeak.ts3client.jni.j.a;
        }
        if (p10 == 1) {
            this.e = com.teamspeak.ts3client.jni.j.b;
        }
        if (p10 == 2) {
            this.e = com.teamspeak.ts3client.jni.j.c;
        }
        this.f = p11;
        com.teamspeak.ts3client.jni.l.a(this);
        return;
    }

    private long c()
    {
        return this.d;
    }

    private long d()
    {
        return this.a;
    }

    private String e()
    {
        return this.f;
    }

    private com.teamspeak.ts3client.jni.j f()
    {
        return this.e;
    }

    public final int a()
    {
        return this.b;
    }

    public final long b()
    {
        return this.c;
    }

    public String toString()
    {
        return new StringBuilder("ClientMoveTimeout [serverConnectionHandlerID=").append(this.a).append(", clientID=").append(this.b).append(", oldChannelID=").append(this.c).append(", newChannelID=").append(this.d).append(", visibility=").append(this.e).append(", timeoutMessage=").append(this.f).append("]").toString();
    }
}
