package com.teamspeak.ts3client.jni.events;
public class ServerStop implements com.teamspeak.ts3client.jni.k {
    private long a;
    private String b;

    public ServerStop()
    {
        return;
    }

    private ServerStop(long p2, String p4)
    {
        this.a = p2;
        this.b = p4;
        com.teamspeak.ts3client.jni.l.a(this);
        return;
    }

    private long b()
    {
        return this.a;
    }

    public final String a()
    {
        return this.b;
    }

    public String toString()
    {
        return new StringBuilder("ServerStop [serverConnectionHandlerID=").append(this.a).append(", shutdownMessage=").append(this.b).append("]").toString();
    }
}
