package com.teamspeak.ts3client.jni;
public final enum class c extends java.lang.Enum {
    public static final enum com.teamspeak.ts3client.jni.c A;
    public static final enum com.teamspeak.ts3client.jni.c B;
    public static final enum com.teamspeak.ts3client.jni.c C;
    public static final enum com.teamspeak.ts3client.jni.c D;
    public static final enum com.teamspeak.ts3client.jni.c E;
    public static final enum com.teamspeak.ts3client.jni.c F;
    public static final enum com.teamspeak.ts3client.jni.c G;
    public static final enum com.teamspeak.ts3client.jni.c H;
    private static final synthetic com.teamspeak.ts3client.jni.c[] J;
    public static final enum com.teamspeak.ts3client.jni.c a;
    public static final enum com.teamspeak.ts3client.jni.c b;
    public static final enum com.teamspeak.ts3client.jni.c c;
    public static final enum com.teamspeak.ts3client.jni.c d;
    public static final enum com.teamspeak.ts3client.jni.c e;
    public static final enum com.teamspeak.ts3client.jni.c f;
    public static final enum com.teamspeak.ts3client.jni.c g;
    public static final enum com.teamspeak.ts3client.jni.c h;
    public static final enum com.teamspeak.ts3client.jni.c i;
    public static final enum com.teamspeak.ts3client.jni.c j;
    public static final enum com.teamspeak.ts3client.jni.c k;
    public static final enum com.teamspeak.ts3client.jni.c l;
    public static final enum com.teamspeak.ts3client.jni.c m;
    public static final enum com.teamspeak.ts3client.jni.c n;
    public static final enum com.teamspeak.ts3client.jni.c o;
    public static final enum com.teamspeak.ts3client.jni.c p;
    public static final enum com.teamspeak.ts3client.jni.c q;
    public static final enum com.teamspeak.ts3client.jni.c r;
    public static final enum com.teamspeak.ts3client.jni.c s;
    public static final enum com.teamspeak.ts3client.jni.c t;
    public static final enum com.teamspeak.ts3client.jni.c u;
    public static final enum com.teamspeak.ts3client.jni.c v;
    public static final enum com.teamspeak.ts3client.jni.c w;
    public static final enum com.teamspeak.ts3client.jni.c x;
    public static final enum com.teamspeak.ts3client.jni.c y;
    public static final enum com.teamspeak.ts3client.jni.c z;
    int I;

    static c()
    {
        com.teamspeak.ts3client.jni.c.a = new com.teamspeak.ts3client.jni.c("CHANNEL_NAME", 0, 0);
        com.teamspeak.ts3client.jni.c.b = new com.teamspeak.ts3client.jni.c("CHANNEL_TOPIC", 1, 1);
        com.teamspeak.ts3client.jni.c.c = new com.teamspeak.ts3client.jni.c("CHANNEL_DESCRIPTION", 2, 2);
        com.teamspeak.ts3client.jni.c.d = new com.teamspeak.ts3client.jni.c("CHANNEL_PASSWORD", 3, 3);
        com.teamspeak.ts3client.jni.c.e = new com.teamspeak.ts3client.jni.c("CHANNEL_CODEC", 4, 4);
        com.teamspeak.ts3client.jni.c.f = new com.teamspeak.ts3client.jni.c("CHANNEL_CODEC_QUALITY", 5, 5);
        com.teamspeak.ts3client.jni.c.g = new com.teamspeak.ts3client.jni.c("CHANNEL_MAXCLIENTS", 6, 6);
        com.teamspeak.ts3client.jni.c.h = new com.teamspeak.ts3client.jni.c("CHANNEL_MAXFAMILYCLIENTS", 7, 7);
        com.teamspeak.ts3client.jni.c.i = new com.teamspeak.ts3client.jni.c("CHANNEL_ORDER", 8, 8);
        com.teamspeak.ts3client.jni.c.j = new com.teamspeak.ts3client.jni.c("CHANNEL_FLAG_PERMANENT", 9, 9);
        com.teamspeak.ts3client.jni.c.k = new com.teamspeak.ts3client.jni.c("CHANNEL_FLAG_SEMI_PERMANENT", 10, 10);
        com.teamspeak.ts3client.jni.c.l = new com.teamspeak.ts3client.jni.c("CHANNEL_FLAG_DEFAULT", 11, 11);
        com.teamspeak.ts3client.jni.c.m = new com.teamspeak.ts3client.jni.c("CHANNEL_FLAG_PASSWORD", 12, 12);
        com.teamspeak.ts3client.jni.c.n = new com.teamspeak.ts3client.jni.c("CHANNEL_CODEC_LATENCY_FACTOR", 13, 13);
        com.teamspeak.ts3client.jni.c.o = new com.teamspeak.ts3client.jni.c("CHANNEL_CODEC_IS_UNENCRYPTED", 14, 14);
        com.teamspeak.ts3client.jni.c.p = new com.teamspeak.ts3client.jni.c("CHANNEL_SECURITY_SALT", 15, 15);
        com.teamspeak.ts3client.jni.c.q = new com.teamspeak.ts3client.jni.c("CHANNEL_DELETE_DELAY", 16, 16);
        com.teamspeak.ts3client.jni.c.r = new com.teamspeak.ts3client.jni.c("CHANNEL_DUMMY_2", 17, 17);
        com.teamspeak.ts3client.jni.c.s = new com.teamspeak.ts3client.jni.c("CHANNEL_DUMMY_3", 18, 18);
        com.teamspeak.ts3client.jni.c.t = new com.teamspeak.ts3client.jni.c("CHANNEL_DUMMY_4", 19, 19);
        com.teamspeak.ts3client.jni.c.u = new com.teamspeak.ts3client.jni.c("CHANNEL_DUMMY_5", 20, 20);
        com.teamspeak.ts3client.jni.c.v = new com.teamspeak.ts3client.jni.c("CHANNEL_DUMMY_6", 21, 21);
        com.teamspeak.ts3client.jni.c.w = new com.teamspeak.ts3client.jni.c("CHANNEL_DUMMY_7", 22, 22);
        com.teamspeak.ts3client.jni.c.x = new com.teamspeak.ts3client.jni.c("CHANNEL_FLAG_MAXCLIENTS_UNLIMITED", 23, 23);
        com.teamspeak.ts3client.jni.c.y = new com.teamspeak.ts3client.jni.c("CHANNEL_FLAG_MAXFAMILYCLIENTS_UNLIMITED", 24, 24);
        com.teamspeak.ts3client.jni.c.z = new com.teamspeak.ts3client.jni.c("CHANNEL_FLAG_MAXFAMILYCLIENTS_INHERITED", 25, 25);
        com.teamspeak.ts3client.jni.c.A = new com.teamspeak.ts3client.jni.c("CHANNEL_FLAG_ARE_SUBSCRIBED", 26, 26);
        com.teamspeak.ts3client.jni.c.B = new com.teamspeak.ts3client.jni.c("CHANNEL_FILEPATH", 27, 27);
        com.teamspeak.ts3client.jni.c.C = new com.teamspeak.ts3client.jni.c("CHANNEL_NEEDED_TALK_POWER", 28, 28);
        com.teamspeak.ts3client.jni.c.D = new com.teamspeak.ts3client.jni.c("CHANNEL_FORCED_SILENCE", 29, 29);
        com.teamspeak.ts3client.jni.c.E = new com.teamspeak.ts3client.jni.c("CHANNEL_NAME_PHONETIC", 30, 30);
        com.teamspeak.ts3client.jni.c.F = new com.teamspeak.ts3client.jni.c("CHANNEL_ICON_ID", 31, 31);
        com.teamspeak.ts3client.jni.c.G = new com.teamspeak.ts3client.jni.c("CHANNEL_FLAG_PRIVATE", 32, 32);
        com.teamspeak.ts3client.jni.c.H = new com.teamspeak.ts3client.jni.c("CHANNEL_ENDMARKER_RARE", 33, 33);
        com.teamspeak.ts3client.jni.c[] v0_69 = new com.teamspeak.ts3client.jni.c[34];
        v0_69[0] = com.teamspeak.ts3client.jni.c.a;
        v0_69[1] = com.teamspeak.ts3client.jni.c.b;
        v0_69[2] = com.teamspeak.ts3client.jni.c.c;
        v0_69[3] = com.teamspeak.ts3client.jni.c.d;
        v0_69[4] = com.teamspeak.ts3client.jni.c.e;
        v0_69[5] = com.teamspeak.ts3client.jni.c.f;
        v0_69[6] = com.teamspeak.ts3client.jni.c.g;
        v0_69[7] = com.teamspeak.ts3client.jni.c.h;
        v0_69[8] = com.teamspeak.ts3client.jni.c.i;
        v0_69[9] = com.teamspeak.ts3client.jni.c.j;
        v0_69[10] = com.teamspeak.ts3client.jni.c.k;
        v0_69[11] = com.teamspeak.ts3client.jni.c.l;
        v0_69[12] = com.teamspeak.ts3client.jni.c.m;
        v0_69[13] = com.teamspeak.ts3client.jni.c.n;
        v0_69[14] = com.teamspeak.ts3client.jni.c.o;
        v0_69[15] = com.teamspeak.ts3client.jni.c.p;
        v0_69[16] = com.teamspeak.ts3client.jni.c.q;
        v0_69[17] = com.teamspeak.ts3client.jni.c.r;
        v0_69[18] = com.teamspeak.ts3client.jni.c.s;
        v0_69[19] = com.teamspeak.ts3client.jni.c.t;
        v0_69[20] = com.teamspeak.ts3client.jni.c.u;
        v0_69[21] = com.teamspeak.ts3client.jni.c.v;
        v0_69[22] = com.teamspeak.ts3client.jni.c.w;
        v0_69[23] = com.teamspeak.ts3client.jni.c.x;
        v0_69[24] = com.teamspeak.ts3client.jni.c.y;
        v0_69[25] = com.teamspeak.ts3client.jni.c.z;
        v0_69[26] = com.teamspeak.ts3client.jni.c.A;
        v0_69[27] = com.teamspeak.ts3client.jni.c.B;
        v0_69[28] = com.teamspeak.ts3client.jni.c.C;
        v0_69[29] = com.teamspeak.ts3client.jni.c.D;
        v0_69[30] = com.teamspeak.ts3client.jni.c.E;
        v0_69[31] = com.teamspeak.ts3client.jni.c.F;
        v0_69[32] = com.teamspeak.ts3client.jni.c.G;
        v0_69[33] = com.teamspeak.ts3client.jni.c.H;
        com.teamspeak.ts3client.jni.c.J = v0_69;
        return;
    }

    private c(String p1, int p2, int p3)
    {
        this(p1, p2);
        this.I = p3;
        return;
    }

    private int a()
    {
        return this.I;
    }

    public static com.teamspeak.ts3client.jni.c valueOf(String p1)
    {
        return ((com.teamspeak.ts3client.jni.c) Enum.valueOf(com.teamspeak.ts3client.jni.c, p1));
    }

    public static com.teamspeak.ts3client.jni.c[] values()
    {
        return ((com.teamspeak.ts3client.jni.c[]) com.teamspeak.ts3client.jni.c.J.clone());
    }
}
