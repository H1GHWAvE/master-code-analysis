package com.teamspeak.ts3client.f;
final class h extends android.support.v4.app.ax {
    final synthetic com.teamspeak.ts3client.f.f at;

    private h(com.teamspeak.ts3client.f.f p1)
    {
        this.at = p1;
        return;
    }

    synthetic h(com.teamspeak.ts3client.f.f p1, byte p2)
    {
        this(p1);
        return;
    }

    public final android.view.View a(android.view.LayoutInflater p11, android.view.ViewGroup p12)
    {
        android.app.Dialog v0_2 = ((com.teamspeak.ts3client.Ts3Application) p11.getContext().getApplicationContext());
        String v1_1 = v0_2.e.getString(com.teamspeak.ts3client.f.f.a(this.at), com.teamspeak.ts3client.f.f.b(this.at));
        android.widget.RelativeLayout v2_3 = new android.widget.RelativeLayout(p11.getContext());
        v2_3.setLayoutParams(new android.widget.RelativeLayout$LayoutParams(-1, -2));
        com.teamspeak.ts3client.f.f.a(this.at, new android.widget.EditText(p11.getContext()));
        com.teamspeak.ts3client.f.f.c(this.at).setId(3);
        com.teamspeak.ts3client.f.f.c(this.at).setInputType(com.teamspeak.ts3client.f.f.d(this.at));
        com.teamspeak.ts3client.f.f.c(this.at).setText(v1_1);
        com.teamspeak.ts3client.f.f.c(this.at).setContentDescription("Input");
        String v1_5 = new android.widget.RelativeLayout$LayoutParams(-1, -2);
        v1_5.addRule(10);
        v2_3.addView(com.teamspeak.ts3client.f.f.c(this.at), v1_5);
        String v1_7 = new android.widget.RelativeLayout$LayoutParams(-1, -2);
        v1_7.addRule(3, com.teamspeak.ts3client.f.f.c(this.at).getId());
        android.widget.Button v3_20 = new android.widget.Button(p11.getContext());
        v3_20.setText(com.teamspeak.ts3client.data.e.a.a("button.save"));
        if (com.teamspeak.ts3client.f.f.c(this.at).getText().toString().equals("")) {
            v3_20.setEnabled(0);
        }
        if (com.teamspeak.ts3client.f.f.e(this.at) > 0) {
            com.teamspeak.ts3client.f.j v4_15 = com.teamspeak.ts3client.f.f.c(this.at);
            com.teamspeak.ts3client.f.i v5_3 = new android.text.InputFilter[1];
            v5_3[0] = new android.text.InputFilter$LengthFilter(com.teamspeak.ts3client.f.f.e(this.at));
            v4_15.setFilters(v5_3);
        }
        com.teamspeak.ts3client.f.f.c(this.at).addTextChangedListener(new com.teamspeak.ts3client.f.i(this, v3_20));
        v3_20.setOnClickListener(new com.teamspeak.ts3client.f.j(this, v0_2, v2_3));
        v2_3.addView(v3_20, v1_7);
        this.j.setTitle(com.teamspeak.ts3client.f.f.g(this.at));
        return v2_3;
    }
}
