package com.teamspeak.ts3client.f;
final class m extends android.support.v4.app.ax {
    final synthetic com.teamspeak.ts3client.f.k at;

    private m(com.teamspeak.ts3client.f.k p1)
    {
        this.at = p1;
        return;
    }

    synthetic m(com.teamspeak.ts3client.f.k p1, byte p2)
    {
        this(p1);
        return;
    }

    public final android.view.View a(android.view.LayoutInflater p12, android.view.ViewGroup p13)
    {
        android.app.Dialog v0_2 = ((com.teamspeak.ts3client.Ts3Application) p12.getContext().getApplicationContext());
        String v1_1 = v0_2.e.getInt(com.teamspeak.ts3client.f.k.a(this.at), 0);
        android.widget.RelativeLayout v2_3 = new android.widget.RelativeLayout(p12.getContext());
        v2_3.setLayoutParams(new android.widget.RelativeLayout$LayoutParams(-1, -2));
        com.teamspeak.ts3client.f.k.a(this.at, new android.widget.SeekBar(p12.getContext()));
        com.teamspeak.ts3client.f.k.b(this.at).setId(3);
        com.teamspeak.ts3client.f.k.b(this.at).setMax(120);
        com.teamspeak.ts3client.f.k.b(this.at).setProgress((v1_1 + 60));
        com.teamspeak.ts3client.f.k.b(this.at).setOnSeekBarChangeListener(new com.teamspeak.ts3client.f.n(this, v0_2));
        com.teamspeak.ts3client.f.k.b(this.at).setProgressDrawable(this.j().getDrawable(2130837676));
        com.teamspeak.ts3client.f.k.a(this.at, new android.widget.TextView(p12.getContext()));
        com.teamspeak.ts3client.f.k.c(this.at).setTextSize(1090519040);
        com.teamspeak.ts3client.f.k.c(this.at).setTypeface(0, 2);
        com.teamspeak.ts3client.f.k.c(this.at).setText(new StringBuilder().append((1056964608 * ((float) (com.teamspeak.ts3client.f.k.b(this.at).getProgress() - 60)))).append(" / 30").toString());
        com.teamspeak.ts3client.f.k.c(this.at).setPadding(0, 0, 10, 0);
        com.teamspeak.ts3client.f.k.c(this.at).setId(4);
        String v1_19 = new android.widget.RelativeLayout$LayoutParams(-1, -2);
        v1_19.addRule(10);
        v2_3.addView(com.teamspeak.ts3client.f.k.b(this.at), v1_19);
        String v1_21 = new android.widget.RelativeLayout$LayoutParams(-2, -2);
        v1_21.addRule(3, com.teamspeak.ts3client.f.k.b(this.at).getId());
        v1_21.addRule(11);
        v2_3.addView(com.teamspeak.ts3client.f.k.c(this.at), v1_21);
        String v1_23 = new android.widget.RelativeLayout$LayoutParams(-1, -2);
        v1_23.addRule(3, com.teamspeak.ts3client.f.k.c(this.at).getId());
        android.widget.Button v3_36 = new android.widget.Button(p12.getContext());
        v3_36.setText(com.teamspeak.ts3client.data.e.a.a("button.save"));
        v3_36.setOnClickListener(new com.teamspeak.ts3client.f.o(this, v0_2, v2_3));
        v2_3.addView(v3_36, v1_23);
        this.j.setTitle(com.teamspeak.ts3client.f.k.e(this.at));
        return v2_3;
    }

    public final void e()
    {
        super.e();
        return;
    }

    public final void f()
    {
        super.f();
        return;
    }

    public final void onDismiss(android.content.DialogInterface p1)
    {
        super.onDismiss(p1);
        return;
    }
}
