package com.teamspeak.ts3client.chat;
public final class a implements com.teamspeak.ts3client.data.d.s {
    String a;
    public boolean b;
    java.util.ArrayList c;
    int d;
    String e;
    com.teamspeak.ts3client.chat.h f;
    com.teamspeak.ts3client.data.c g;
    java.util.Date h;
    private boolean i;

    public a(String p2, String p3, com.teamspeak.ts3client.data.c p4)
    {
        this(p2, p3, p4, 0);
        return;
    }

    public a(String p3, String p4, com.teamspeak.ts3client.data.c p5, boolean p6)
    {
        this.b = 0;
        this.c = new java.util.ArrayList();
        this.d = 0;
        this.h = new java.util.Date();
        this.i = 0;
        this.e = p4;
        this.a = p3;
        this.g = p5;
        this.i = p6;
        if ((!p6) && (p5 != null)) {
            if (this.g.y == null) {
                this.a();
            }
            this.c();
        }
        return;
    }

    private static synthetic java.util.ArrayList a(com.teamspeak.ts3client.chat.a p1)
    {
        return p1.c;
    }

    private static synthetic java.util.Date a(com.teamspeak.ts3client.chat.a p0, java.util.Date p1)
    {
        p0.h = p1;
        return p1;
    }

    private void a(com.teamspeak.ts3client.chat.h p1)
    {
        this.f = p1;
        return;
    }

    private static synthetic void b(com.teamspeak.ts3client.chat.a p0)
    {
        p0.c();
        return;
    }

    private java.util.Date d()
    {
        return this.h;
    }

    private int e()
    {
        return this.c.size();
    }

    private java.util.ArrayList f()
    {
        return this.c;
    }

    private int g()
    {
        return this.d;
    }

    private String h()
    {
        return this.a;
    }

    private com.teamspeak.ts3client.data.c i()
    {
        return this.g;
    }

    private boolean j()
    {
        return this.b;
    }

    private void k()
    {
        this.b = 1;
        return;
    }

    private String l()
    {
        return this.e;
    }

    public final void a()
    {
        if ((this.g != null) && ((this.g.b() != null) && (!this.g.b().equals("")))) {
            this.g.a(this);
        }
        return;
    }

    public final void a(com.teamspeak.ts3client.chat.y p3)
    {
        if (com.teamspeak.ts3client.Ts3Application.a().c != null) {
            com.teamspeak.ts3client.Ts3Application.a().c.i().runOnUiThread(new com.teamspeak.ts3client.chat.b(this, p3));
        }
        return;
    }

    public final void a(String p1)
    {
        this.a = p1;
        this.c();
        return;
    }

    public final void b()
    {
        this.d = this.c.size();
        return;
    }

    final void c()
    {
        if (this.f != null) {
            this.f.a();
        }
        com.teamspeak.ts3client.chat.d.a().d();
        return;
    }

    public final void y()
    {
        if (this.f != null) {
            this.f.a();
        }
        return;
    }
}
