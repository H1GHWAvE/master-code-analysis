package com.teamspeak.ts3client.chat;
public class MListView extends android.widget.ListView {
    private com.teamspeak.ts3client.chat.x a;

    public MListView(android.content.Context p1)
    {
        this(p1);
        return;
    }

    public MListView(android.content.Context p1, android.util.AttributeSet p2)
    {
        this(p1, p2);
        return;
    }

    protected void onSizeChanged(int p2, int p3, int p4, int p5)
    {
        super.onSizeChanged(p2, p3, p4, p5);
        if (this.a != null) {
            this.a.a();
        }
        return;
    }

    void setOnSizeChangedListener(com.teamspeak.ts3client.chat.x p1)
    {
        this.a = p1;
        return;
    }
}
