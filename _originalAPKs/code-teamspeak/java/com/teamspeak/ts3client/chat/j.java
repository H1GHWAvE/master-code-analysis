package com.teamspeak.ts3client.chat;
public final class j extends android.support.v4.app.Fragment implements com.teamspeak.ts3client.data.d.s, com.teamspeak.ts3client.data.w {
    java.util.regex.Pattern a;
    private String at;
    private boolean au;
    private boolean av;
    private int aw;
    java.util.regex.Pattern b;
    android.database.DataSetObserver c;
    private com.teamspeak.ts3client.Ts3Application d;
    private com.teamspeak.ts3client.chat.MListView e;
    private com.teamspeak.ts3client.chat.h f;
    private com.teamspeak.ts3client.chat.a g;
    private android.widget.TextView h;
    private android.widget.EditText i;
    private android.widget.ImageButton j;
    private android.widget.ImageButton k;
    private android.view.View l;
    private android.view.View m;

    public j(com.teamspeak.ts3client.chat.a p3)
    {
        this.a = java.util.regex.Pattern.compile("(ts3server://[^\\s\"]{3,}|(?:(?:(?:http://|https://|ftp://)(?:\\w+:\\w+@)?)|www\\.|ftp\\.)[^ !\"#$%&\'()*+,/:;<=>?@\\\\\\[\\\\\\]^_`{|}~\u0008\t\n\u000c\r]{3,}(?::[0-9]+)?(?:/[^\\s\"]*)?)", 2);
        this.b = java.util.regex.Pattern.compile("((^|\\s)([A-Z0-9._%+-]+@[A-Z0-9\u00c4\u00d6\u00dc.-]+\\.[A-Z]{2,4})(\\s|$)?)", 2);
        this.c = new com.teamspeak.ts3client.chat.k(this);
        this.aw = 0;
        this.g = p3;
        return;
    }

    static synthetic int a(com.teamspeak.ts3client.chat.j p0, int p1)
    {
        p0.aw = p1;
        return p1;
    }

    static synthetic com.teamspeak.ts3client.chat.a a(com.teamspeak.ts3client.chat.j p1)
    {
        return p1.g;
    }

    private void a()
    {
        if (!this.i.getText().toString().equals("")) {
            String v0_5 = new StringBuffer(this.i.getText().toString().length());
            com.teamspeak.ts3client.jni.h v1_6 = this.a.matcher(this.i.getText().toString());
            while (v1_6.find()) {
                v1_6.appendReplacement(v0_5, java.util.regex.Matcher.quoteReplacement(new StringBuilder("[URL]").append(v1_6.group(1)).append("[/URL]").toString()));
            }
            v1_6.appendTail(v0_5);
            String v3_1 = new StringBuffer(v0_5.toString().length());
            String v0_8 = this.b.matcher(v0_5.toString().toString());
            while (v0_8.find()) {
                com.teamspeak.ts3client.jni.h v1_25 = v0_8.group(1);
                v0_8.appendReplacement(v3_1, java.util.regex.Matcher.quoteReplacement(new StringBuilder("[URL=mailto:").append(v1_25).append("]").append(v1_25).append("[/URL]").toString()));
            }
            v0_8.appendTail(v3_1);
            if (!this.g.e.equals("CHANNEL")) {
                if (!this.g.e.equals("SERVER")) {
                    this.at = v3_1.toString().trim();
                    this.a(1);
                } else {
                    this.d.a.a.ts3client_requestSendServerTextMsg(this.d.a.e, v3_1.toString().trim(), "Send Message to Server");
                    this.d.g().a(com.teamspeak.ts3client.jni.h.aP, new com.teamspeak.ts3client.a.o("", 0, "", this.d.a.m()));
                }
            } else {
                this.d.a.a.ts3client_requestSendChannelTextMsg(this.d.a.e, v3_1.toString().trim(), this.d.a.g, "Send Message to Channel");
                this.d.g().a(com.teamspeak.ts3client.jni.h.aN, new com.teamspeak.ts3client.a.o("", 0, "", this.d.a.m()));
            }
            this.h.setText(this.g.a);
            this.i.setText("");
        }
        return;
    }

    private void a(boolean p8)
    {
        this.d.a.c.a(this);
        this.d.a.a.ts3client_requestSendPrivateTextMsg(this.d.a.e, this.at, this.g.g.c, new StringBuilder("Send Message to Client ").append(this.g.g.c).toString());
        if (p8) {
            this.d.g().a(com.teamspeak.ts3client.jni.h.aL, new com.teamspeak.ts3client.a.o(this.g.a, 0, "", this.d.a.m()));
        }
        return;
    }

    static synthetic android.widget.TextView b(com.teamspeak.ts3client.chat.j p1)
    {
        return p1.h;
    }

    static synthetic com.teamspeak.ts3client.chat.MListView c(com.teamspeak.ts3client.chat.j p1)
    {
        return p1.e;
    }

    static synthetic com.teamspeak.ts3client.Ts3Application d(com.teamspeak.ts3client.chat.j p1)
    {
        return p1.d;
    }

    static synthetic void e(com.teamspeak.ts3client.chat.j p8)
    {
        if (!p8.i.getText().toString().equals("")) {
            String v0_5 = new StringBuffer(p8.i.getText().toString().length());
            com.teamspeak.ts3client.jni.h v1_6 = p8.a.matcher(p8.i.getText().toString());
            while (v1_6.find()) {
                v1_6.appendReplacement(v0_5, java.util.regex.Matcher.quoteReplacement(new StringBuilder("[URL]").append(v1_6.group(1)).append("[/URL]").toString()));
            }
            v1_6.appendTail(v0_5);
            String v3_1 = new StringBuffer(v0_5.toString().length());
            String v0_8 = p8.b.matcher(v0_5.toString().toString());
            while (v0_8.find()) {
                com.teamspeak.ts3client.jni.h v1_25 = v0_8.group(1);
                v0_8.appendReplacement(v3_1, java.util.regex.Matcher.quoteReplacement(new StringBuilder("[URL=mailto:").append(v1_25).append("]").append(v1_25).append("[/URL]").toString()));
            }
            v0_8.appendTail(v3_1);
            if (!p8.g.e.equals("CHANNEL")) {
                if (!p8.g.e.equals("SERVER")) {
                    p8.at = v3_1.toString().trim();
                    p8 = p8.a(1);
                } else {
                    p8.d.a.a.ts3client_requestSendServerTextMsg(p8.d.a.e, v3_1.toString().trim(), "Send Message to Server");
                    p8.d.g().a(com.teamspeak.ts3client.jni.h.aP, new com.teamspeak.ts3client.a.o("", 0, "", p8.d.a.m()));
                }
            } else {
                p8.d.a.a.ts3client_requestSendChannelTextMsg(p8.d.a.e, v3_1.toString().trim(), p8.d.a.g, "Send Message to Channel");
                p8.d.g().a(com.teamspeak.ts3client.jni.h.aN, new com.teamspeak.ts3client.a.o("", 0, "", p8.d.a.m()));
            }
            p8.h.setText(p8.g.a);
            p8.i.setText("");
        }
        return;
    }

    static synthetic android.widget.EditText f(com.teamspeak.ts3client.chat.j p1)
    {
        return p1.i;
    }

    static synthetic int g(com.teamspeak.ts3client.chat.j p1)
    {
        return p1.aw;
    }

    static synthetic com.teamspeak.ts3client.chat.h h(com.teamspeak.ts3client.chat.j p1)
    {
        return p1.f;
    }

    public final android.view.View a(android.view.LayoutInflater p5, android.view.ViewGroup p6)
    {
        this.d = ((com.teamspeak.ts3client.Ts3Application) this.i().getApplicationContext());
        this.d.c = this;
        this.n();
        android.view.View v1_1 = p5.inflate(2130903079, p6, 0);
        this.h = ((android.widget.TextView) v1_1.findViewById(2131493104));
        this.h.setText(this.g.a);
        this.i = ((android.widget.EditText) v1_1.findViewById(2131493107));
        this.i.addTextChangedListener(new com.teamspeak.ts3client.chat.n(this));
        this.i.setOnEditorActionListener(new com.teamspeak.ts3client.chat.o(this));
        this.j = ((android.widget.ImageButton) v1_1.findViewById(2131493109));
        this.k = ((android.widget.ImageButton) v1_1.findViewById(2131493108));
        this.k.setOnClickListener(new com.teamspeak.ts3client.chat.q(this));
        this.m = v1_1.findViewById(2131493110);
        this.m.setOnClickListener(new com.teamspeak.ts3client.chat.r(this));
        this.l = v1_1.findViewById(2131493111);
        this.l.setOnClickListener(new com.teamspeak.ts3client.chat.s(this));
        this.e = ((com.teamspeak.ts3client.chat.MListView) v1_1.findViewById(2131493105));
        if (this.f == null) {
            this.f = new com.teamspeak.ts3client.chat.h(this.i());
        }
        com.teamspeak.ts3client.chat.a v0_33 = this.f;
        com.teamspeak.ts3client.chat.w v2_13 = this.g;
        v0_33.b = v2_13;
        v2_13.f = v0_33;
        v0_33.a = v2_13.c;
        v0_33.a();
        this.e.setAdapter(this.f);
        this.e.setItemsCanFocus(1);
        this.e.setOnSizeChangedListener(new com.teamspeak.ts3client.chat.t(this));
        this.e.setOnScrollListener(new com.teamspeak.ts3client.chat.v(this));
        this.e.setTranscriptMode(1);
        this.j.setOnClickListener(new com.teamspeak.ts3client.chat.w(this));
        this.g.b();
        return v1_1;
    }

    public final void a(android.os.Bundle p1)
    {
        super.a(p1);
        return;
    }

    public final void a(android.view.Menu p1, android.view.MenuInflater p2)
    {
        p1.clear();
        super.a(p1, p2);
        return;
    }

    public final void a(com.teamspeak.ts3client.jni.k p10)
    {
        if (((p10 instanceof com.teamspeak.ts3client.jni.events.TextMessage)) && ((((com.teamspeak.ts3client.jni.events.TextMessage) p10).a == 1) && ((((com.teamspeak.ts3client.jni.events.TextMessage) p10).c == this.d.a.h) && (((com.teamspeak.ts3client.jni.events.TextMessage) p10).b == this.g.g.c)))) {
            this.g.a(new com.teamspeak.ts3client.chat.y(((com.teamspeak.ts3client.jni.events.TextMessage) p10).d, ((com.teamspeak.ts3client.jni.events.TextMessage) p10).e, ((com.teamspeak.ts3client.jni.events.TextMessage) p10).f, Boolean.valueOf(1)));
            this.d.a.c.b(this);
            this.au = 0;
        }
        if (!(p10 instanceof com.teamspeak.ts3client.jni.events.ServerError)) {
            if (((p10 instanceof com.teamspeak.ts3client.jni.events.ClientIDs)) && (((com.teamspeak.ts3client.jni.events.ClientIDs) p10).a.equals(this.g.g.b))) {
                this.g.g.c = ((com.teamspeak.ts3client.jni.events.ClientIDs) p10).b;
                this.av = 0;
            }
            if ((p10 instanceof com.teamspeak.ts3client.jni.events.ClientIDsFinished)) {
                this.a(0);
            }
        } else {
            if ((((com.teamspeak.ts3client.jni.events.ServerError) p10).a.equals("invalid clientID")) && (((com.teamspeak.ts3client.jni.events.ServerError) p10).c.equals(new StringBuilder("Send Message to Client ").append(this.g.g.c).toString()))) {
                if ((!this.au) || (this.av)) {
                    this.au = 1;
                    this.d.a.a.ts3client_requestClientIDs(this.d.a.e, this.g.g.b, "Request clientid");
                } else {
                    this.av = 1;
                    this.g.a(new com.teamspeak.ts3client.chat.y(0, "", "Chat partner disconnected out of view", Boolean.valueOf(0), Boolean.valueOf(1)));
                    this.d.a.c.b(this);
                    return;
                }
            }
            if ((!((com.teamspeak.ts3client.jni.events.ServerError) p10).a.equals("database empty result set")) || ((!((com.teamspeak.ts3client.jni.events.ServerError) p10).c.equals("Request clientid")) || (this.av))) {
            } else {
                this.av = 1;
                this.g.a(new com.teamspeak.ts3client.chat.y(0, "", "Chat partner disconnected out of view", Boolean.valueOf(0), Boolean.valueOf(1)));
                this.d.a.c.b(this);
            }
        }
        return;
    }

    protected final void b(String p8)
    {
        android.widget.EditText v0_2 = this.i.getText().toString();
        int v1_1 = this.i.getSelectionStart();
        android.widget.EditText v2_1 = this.i.getSelectionEnd();
        if (v1_1 == v2_1) {
            android.widget.EditText v0_5;
            if (this.i.getSelectionStart() != v0_2.length()) {
                v0_5 = new StringBuilder().append(v0_2.substring(0, v1_1)).append(p8).append(v0_2.substring(v1_1, v0_2.length())).toString();
            } else {
                v0_5 = new StringBuilder().append(v0_2).append(p8).toString();
            }
            this.i.setText(v0_5);
            this.i.setSelection((v1_1 + 3));
        } else {
            this.i.setText(new StringBuilder().append(v0_2.substring(0, v1_1)).append(p8.substring(0, 3)).append(v0_2.substring(v1_1, v2_1)).append(p8.substring(3, 7)).append(v0_2.substring(v2_1, v0_2.length())).toString());
            this.i.setSelection(((v2_1 + (v1_1 + 3)) - v1_1));
        }
        return;
    }

    public final void c(android.os.Bundle p1)
    {
        super.c(p1);
        return;
    }

    public final void d(android.os.Bundle p1)
    {
        return;
    }

    public final void s()
    {
        super.s();
        this.f.registerDataSetObserver(this.c);
        this.e.postDelayed(new com.teamspeak.ts3client.chat.m(this), 100);
        return;
    }

    public final void t()
    {
        super.t();
        ((android.view.inputmethod.InputMethodManager) this.d.getSystemService("input_method")).hideSoftInputFromWindow(this.i.getWindowToken(), 0);
        this.f.unregisterDataSetObserver(this.c);
        return;
    }

    public final void y()
    {
        return;
    }
}
