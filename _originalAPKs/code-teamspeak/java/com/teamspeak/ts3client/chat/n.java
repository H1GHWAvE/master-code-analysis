package com.teamspeak.ts3client.chat;
final class n implements android.text.TextWatcher {
    java.util.Date a;
    final synthetic com.teamspeak.ts3client.chat.j b;

    n(com.teamspeak.ts3client.chat.j p2)
    {
        this.b = p2;
        this.a = new java.util.Date();
        return;
    }

    public final void afterTextChanged(android.text.Editable p1)
    {
        return;
    }

    public final void beforeTextChanged(CharSequence p1, int p2, int p3, int p4)
    {
        return;
    }

    public final void onTextChanged(CharSequence p7, int p8, int p9, int p10)
    {
        if ((!com.teamspeak.ts3client.chat.j.a(this.b).e.equals("SERVER")) && (!com.teamspeak.ts3client.chat.j.a(this.b).e.equals("CHANNEL"))) {
            com.teamspeak.ts3client.jni.Ts3Jni v0_9 = new java.util.Date();
            if ((v0_9.getTime() - this.a.getTime()) > 3000) {
                this.a = v0_9;
                com.teamspeak.ts3client.chat.j.d(this.b).a.a.ts3client_clientChatComposing(com.teamspeak.ts3client.chat.j.d(this.b).a.e, com.teamspeak.ts3client.chat.j.a(this.b).g.c, "Composing");
            }
        }
        return;
    }
}
