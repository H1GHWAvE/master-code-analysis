package com.teamspeak.ts3client.data.b;
public final class f {
    private static final String b = "create table ident (ident_id integer primary key autoincrement, name text not null, identity text not null, nickname text not null, defaultid int not null);";
    private static final String c = "ident";
    private static final String d = "Teamspeak-Ident";
    private static final int e = 1;
    private static com.teamspeak.ts3client.data.b.e f;
    private static com.teamspeak.ts3client.data.b.f g;
    public android.database.sqlite.SQLiteDatabase a;

    static f()
    {
        com.teamspeak.ts3client.data.b.f.f = 0;
        return;
    }

    public f()
    {
        this.a = new com.teamspeak.ts3client.data.b.g(this, com.teamspeak.ts3client.Ts3Application.a()).getWritableDatabase();
        return;
    }

    public static declared_synchronized com.teamspeak.ts3client.data.b.f a()
    {
        try {
            com.teamspeak.ts3client.data.b.f v0_1;
            if (com.teamspeak.ts3client.data.b.f.g != null) {
                v0_1 = com.teamspeak.ts3client.data.b.f.g;
            } else {
                v0_1 = new com.teamspeak.ts3client.data.b.f();
                com.teamspeak.ts3client.data.b.f.g = v0_1;
            }
        } catch (com.teamspeak.ts3client.data.b.f v0_3) {
            throw v0_3;
        }
        return v0_1;
    }

    private void a(int p11)
    {
        com.teamspeak.ts3client.e.a v9_1 = new com.teamspeak.ts3client.e.a();
        try {
            android.database.Cursor v0_0 = this.a;
            long v2_1 = new String[5];
            v2_1[0] = "ident_id";
            v2_1[1] = "name";
            v2_1[2] = "identity";
            v2_1[3] = "nickname";
            v2_1[4] = "defaultid";
            android.database.Cursor v0_1 = v0_0.query("ident", v2_1, "defaultid=1", 0, 0, 0, 0, 0);
        } catch (android.database.Cursor v0) {
            return;
        }
        if (v0_1.moveToFirst()) {
            v9_1.b = v0_1.getString(v0_1.getColumnIndex("name"));
            v9_1.c = v0_1.getString(v0_1.getColumnIndex("identity"));
            v9_1.d = v0_1.getString(v0_1.getColumnIndex("nickname"));
            v9_1.e = 0;
            v9_1.a = v0_1.getInt(v0_1.getColumnIndex("ident_id"));
            this.a(((long) v9_1.a), v9_1);
        }
        v0_1.close();
        com.teamspeak.ts3client.e.a v9_3 = new com.teamspeak.ts3client.e.a();
        android.database.Cursor v0_2 = this.a;
        long v2_4 = new String[5];
        v2_4[0] = "ident_id";
        v2_4[1] = "name";
        v2_4[2] = "identity";
        v2_4[3] = "nickname";
        v2_4[4] = "defaultid";
        android.database.Cursor v0_3 = v0_2.query("ident", v2_4, new StringBuilder("ident_id=").append(p11).toString(), 0, 0, 0, 0, 0);
        if (v0_3.moveToFirst()) {
            v9_3.b = v0_3.getString(v0_3.getColumnIndex("name"));
            v9_3.c = v0_3.getString(v0_3.getColumnIndex("identity"));
            v9_3.d = v0_3.getString(v0_3.getColumnIndex("nickname"));
            v9_3.e = 1;
            v9_3.a = v0_3.getInt(v0_3.getColumnIndex("ident_id"));
            this.a(((long) v9_3.a), v9_3);
        }
        v0_3.close();
        return;
    }

    public static void a(com.teamspeak.ts3client.data.b.e p0)
    {
        com.teamspeak.ts3client.data.b.f.f = p0;
        return;
    }

    private void e()
    {
        this.a.close();
        return;
    }

    private static void f()
    {
        if (com.teamspeak.ts3client.data.b.f.f != null) {
            com.teamspeak.ts3client.data.b.f.f.a();
        }
        return;
    }

    private static void g()
    {
        com.teamspeak.ts3client.data.b.f.f = 0;
        return;
    }

    public final long a(com.teamspeak.ts3client.e.a p5)
    {
        long v0_4;
        android.content.ContentValues v1_1 = new android.content.ContentValues();
        v1_1.put("name", p5.b);
        v1_1.put("identity", p5.c);
        v1_1.put("nickname", p5.d);
        if (!p5.e) {
            v0_4 = 0;
        } else {
            v0_4 = 1;
        }
        v1_1.put("defaultid", Integer.valueOf(v0_4));
        try {
            long v0_7 = this.a.insert("ident", 0, v1_1);
        } catch (long v0_8) {
            v0_8.printStackTrace();
            v0_7 = -1;
        }
        return v0_7;
    }

    public final boolean a(long p14)
    {
        com.teamspeak.ts3client.e.a v11_1 = new com.teamspeak.ts3client.e.a();
        try {
            int v0_0 = this.a;
            long v2_1 = new String[5];
            v2_1[0] = "ident_id";
            v2_1[1] = "name";
            v2_1[2] = "identity";
            v2_1[3] = "nickname";
            v2_1[4] = "defaultid";
            int v0_1 = v0_0.query("ident", v2_1, new StringBuilder("ident_id=").append(p14).toString(), 0, 0, 0, 0, 0);
        } catch (int v0) {
            try {
                this.a.delete("ident", new StringBuilder("ident_id=").append(p14).toString(), 0);
                com.teamspeak.ts3client.data.b.f.f();
                int v0_6 = 1;
            } catch (int v0_7) {
                v0_7.printStackTrace();
                v0_6 = 0;
            }
            return v0_6;
        }
        if (v0_1.moveToFirst()) {
            v11_1.b = v0_1.getString(v0_1.getColumnIndex("name"));
            v11_1.c = v0_1.getString(v0_1.getColumnIndex("identity"));
            v11_1.d = v0_1.getString(v0_1.getColumnIndex("nickname"));
            if (v0_1.getInt(v0_1.getColumnIndex("defaultid")) != 0) {
                v11_1.e = 1;
            } else {
                v11_1.e = 0;
            }
            v11_1.a = v0_1.getInt(v0_1.getColumnIndex("ident_id"));
            this.a(((long) v11_1.a), v11_1);
        }
        v0_1.close();
        if (!v11_1.e) {
        } else {
            com.teamspeak.ts3client.e.a v11_3 = new com.teamspeak.ts3client.e.a();
            int v0_3 = this.a;
            long v2_4 = new String[5];
            v2_4[0] = "ident_id";
            v2_4[1] = "name";
            v2_4[2] = "identity";
            v2_4[3] = "nickname";
            v2_4[4] = "defaultid";
            int v0_4 = v0_3.query("ident", v2_4, "ident_id=1", 0, 0, 0, 0, 0);
            if (v0_4.moveToFirst()) {
                v11_3.b = v0_4.getString(v0_4.getColumnIndex("name"));
                v11_3.c = v0_4.getString(v0_4.getColumnIndex("identity"));
                v11_3.d = v0_4.getString(v0_4.getColumnIndex("nickname"));
                v11_3.e = 1;
                v11_3.a = v0_4.getInt(v0_4.getColumnIndex("ident_id"));
                this.a(((long) v11_3.a), v11_3);
            }
            v0_4.close();
        }
    }

    public final boolean a(long p8, com.teamspeak.ts3client.e.a p10)
    {
        Exception v0_4;
        int v1 = 1;
        android.content.ContentValues v3_1 = new android.content.ContentValues();
        v3_1.put("name", p10.b);
        v3_1.put("identity", p10.c);
        v3_1.put("nickname", p10.d);
        if (!p10.e) {
            v0_4 = 0;
        } else {
            v0_4 = 1;
        }
        v3_1.put("defaultid", Integer.valueOf(v0_4));
        try {
            this.a.update("ident", v3_1, new StringBuilder("ident_id=").append(p8).toString(), 0);
            com.teamspeak.ts3client.data.b.f.f();
        } catch (Exception v0_7) {
            v0_7.printStackTrace();
            v1 = 0;
        }
        return v1;
    }

    public final com.teamspeak.ts3client.e.a b(long p12)
    {
        com.teamspeak.ts3client.e.a v9_1 = new com.teamspeak.ts3client.e.a();
        try {
            com.teamspeak.ts3client.e.a v0_0 = this.a;
            String[] v2_1 = new String[5];
            v2_1[0] = "ident_id";
            v2_1[1] = "name";
            v2_1[2] = "identity";
            v2_1[3] = "nickname";
            v2_1[4] = "defaultid";
            com.teamspeak.ts3client.e.a v0_1 = v0_0.query("ident", v2_1, new StringBuilder("ident_id=").append(p12).toString(), 0, 0, 0, 0, 0);
        } catch (com.teamspeak.ts3client.e.a v0) {
            com.teamspeak.ts3client.e.a v0_2 = 0;
            return v0_2;
        }
        if (v0_1.moveToFirst()) {
            v9_1.b = v0_1.getString(v0_1.getColumnIndex("name"));
            v9_1.c = v0_1.getString(v0_1.getColumnIndex("identity"));
            v9_1.d = v0_1.getString(v0_1.getColumnIndex("nickname"));
            if (v0_1.getInt(v0_1.getColumnIndex("defaultid")) != 0) {
                v9_1.e = 1;
            } else {
                v9_1.e = 0;
            }
            v9_1.a = v0_1.getInt(v0_1.getColumnIndex("ident_id"));
        }
        v0_1.close();
        v0_2 = v9_1;
        return v0_2;
    }

    public final void b()
    {
        this.a = new com.teamspeak.ts3client.data.b.g(this, com.teamspeak.ts3client.Ts3Application.a()).a(com.teamspeak.ts3client.Ts3Application.a().getApplicationContext());
        return;
    }

    public final java.util.ArrayList c()
    {
        java.util.ArrayList v8_1 = new java.util.ArrayList();
        try {
            java.util.ArrayList v0_0 = this.a;
            int v2_1 = new String[5];
            v2_1[0] = "ident_id";
            v2_1[1] = "name";
            v2_1[2] = "identity";
            v2_1[3] = "nickname";
            v2_1[4] = "defaultid";
            java.util.ArrayList v0_1 = v0_0.query("ident", v2_1, 0, 0, 0, 0, 0);
            v0_1.moveToFirst();
        } catch (java.util.ArrayList v0_3) {
            v0_3.printStackTrace();
            java.util.ArrayList v0_2 = 0;
            return v0_2;
        }
        if (!v0_1.isAfterLast()) {
            do {
                boolean v1_3 = new com.teamspeak.ts3client.e.a();
                v1_3.b = v0_1.getString(v0_1.getColumnIndex("name"));
                v1_3.c = v0_1.getString(v0_1.getColumnIndex("identity"));
                v1_3.d = v0_1.getString(v0_1.getColumnIndex("nickname"));
                if (v0_1.getInt(v0_1.getColumnIndex("defaultid")) != 0) {
                    v1_3.e = 1;
                } else {
                    v1_3.e = 0;
                }
                v1_3.a = v0_1.getInt(v0_1.getColumnIndex("ident_id"));
                v8_1.add(v1_3);
            } while(v0_1.moveToNext());
        }
        v0_1.close();
        v0_2 = v8_1;
        return v0_2;
    }

    public final com.teamspeak.ts3client.e.a d()
    {
        com.teamspeak.ts3client.e.a v9_1 = new com.teamspeak.ts3client.e.a();
        try {
            com.teamspeak.ts3client.e.a v0_0 = this.a;
            String[] v2_1 = new String[5];
            v2_1[0] = "ident_id";
            v2_1[1] = "name";
            v2_1[2] = "identity";
            v2_1[3] = "nickname";
            v2_1[4] = "defaultid";
            com.teamspeak.ts3client.e.a v0_1 = v0_0.query("ident", v2_1, "defaultid=1", 0, 0, 0, 0, 0);
        } catch (com.teamspeak.ts3client.e.a v0) {
            com.teamspeak.ts3client.e.a v0_2 = 0;
            return v0_2;
        }
        if (v0_1.moveToFirst()) {
            v9_1.b = v0_1.getString(v0_1.getColumnIndex("name"));
            v9_1.c = v0_1.getString(v0_1.getColumnIndex("identity"));
            v9_1.d = v0_1.getString(v0_1.getColumnIndex("nickname"));
            if (v0_1.getInt(v0_1.getColumnIndex("defaultid")) != 0) {
                v9_1.e = 1;
            } else {
                v9_1.e = 0;
            }
            v9_1.a = v0_1.getInt(v0_1.getColumnIndex("ident_id"));
        }
        v0_1.close();
        v0_2 = v9_1;
        return v0_2;
    }
}
