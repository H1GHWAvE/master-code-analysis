package com.teamspeak.ts3client.data.g;
public final class b {
    public java.util.HashMap a;

    public b()
    {
        this.a = new java.util.HashMap();
        return;
    }

    private java.util.HashMap a()
    {
        return this.a;
    }

    private java.util.ArrayList b()
    {
        java.util.ArrayList v0_1 = new java.util.ArrayList();
        v0_1.addAll(this.a.values());
        java.util.Collections.sort(v0_1);
        return v0_1;
    }

    private void b(com.teamspeak.ts3client.data.g.a p5)
    {
        this.a.remove(Long.valueOf(p5.a));
        return;
    }

    public final void a(com.teamspeak.ts3client.data.g.a p5)
    {
        this.a.put(Long.valueOf(p5.a), p5);
        return;
    }

    public final boolean a(long p4)
    {
        return this.a.containsKey(Long.valueOf(p4));
    }

    public final com.teamspeak.ts3client.data.g.a b(long p4)
    {
        return ((com.teamspeak.ts3client.data.g.a) this.a.get(Long.valueOf(p4)));
    }
}
