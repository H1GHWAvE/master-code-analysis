package com.teamspeak.ts3client.data;
public final class g extends android.widget.BaseExpandableListAdapter {
    public java.util.List a;
    public com.teamspeak.ts3client.data.customExpandableListView b;
    public boolean c;
    public boolean d;
    private android.content.Context e;
    private com.teamspeak.ts3client.Ts3Application f;
    private android.view.LayoutInflater g;
    private int h;
    private int i;
    private android.support.v4.app.bi j;
    private int k;
    private java.util.regex.Pattern l;

    public g(android.content.Context p5, android.support.v4.app.bi p6)
    {
        this.a = new java.util.ArrayList();
        this.e = p5;
        this.f = ((com.teamspeak.ts3client.Ts3Application) p5.getApplicationContext());
        this.g = android.view.LayoutInflater.from(p5);
        this.h = ((int) android.util.TypedValue.applyDimension(1, ((float) this.f.e.getInt("channel_height", 25)), p5.getResources().getDisplayMetrics()));
        this.i = ((int) android.util.TypedValue.applyDimension(1, ((float) this.f.e.getInt("client_height", 25)), p5.getResources().getDisplayMetrics()));
        this.j = p6;
        this.k = new android.widget.TextView(p5).getTextColors().getDefaultColor();
        this.l = java.util.regex.Pattern.compile("^[\\W]*\\[(.*)spacer.*?\\](.*)");
        return;
    }

    private int a(float p3)
    {
        return ((int) ((this.e.getResources().getDisplayMetrics().density * p3) + 1056964608));
    }

    static synthetic android.support.v4.app.bi a(com.teamspeak.ts3client.data.g p1)
    {
        return p1.j;
    }

    private static android.view.View a(android.view.View p1)
    {
        p1.setVisibility(4);
        return p1;
    }

    private android.view.View a(android.view.View p6, com.teamspeak.ts3client.data.u p7)
    {
        p6.setPadding(this.a(-1044905984), 0, 0, 0);
        p6.setBackgroundColor(0);
        p7.b.setVisibility(4);
        p7.d.setImageBitmap(com.teamspeak.ts3client.data.d.t.a(2130837678, 1098907648, 1098907648));
        p7.c.setText(((com.teamspeak.ts3client.data.a) this.a.get(0)).a);
        p7.c.setGravity(19);
        try {
            if ((this.f.a.v.m != 0) && (this.f.a.r != null)) {
                android.widget.ImageView v0_18 = new android.widget.ImageView(this.e);
                v0_18.setBackgroundDrawable(this.f.a.r.a(this.f.a.v.m).a());
                v0_18.setMinimumHeight(20);
                v0_18.setMinimumWidth(20);
                p7.a.addView(v0_18);
            }
        } catch (android.widget.ImageView v0_19) {
            this.f.d.log(java.util.logging.Level.SEVERE, new StringBuilder("setServerEntry(): ").append(v0_19.toString()).toString());
        }
        p6.setOnTouchListener(new com.teamspeak.ts3client.data.o(this));
        return p6;
    }

    private java.util.List a()
    {
        return this.a;
    }

    private static void a(android.widget.TextView p12, String p13)
    {
        String v3 = p13.replace(".", "\u00b7");
        StringBuilder v4_1 = new StringBuilder();
        char[] v5 = v3.toCharArray();
        float v6 = p12.getPaint().measureText(v3);
        float v7 = ((float) p12.getWidth());
        int v0_3 = 1;
        int v1_1 = 0;
        while (((double) v0_3) < Math.floor(((double) (((v7 / v6) * ((float) v3.length())) + 1065353216)))) {
            v4_1.append(v5[v1_1]);
            v1_1++;
            if (v1_1 >= v5.length) {
                v1_1 = 0;
            }
            v0_3++;
        }
        p12.setText(v4_1.toString());
        return;
    }

    private void a(com.teamspeak.ts3client.data.customExpandableListView p1)
    {
        this.b = p1;
        return;
    }

    private void a(com.teamspeak.ts3client.data.a[] p9)
    {
        if (p9.length > 0) {
            this.a.clear();
            java.util.List v1_0 = p9.length;
            int v0_2 = 0;
            while (v0_2 < v1_0) {
                long v4_1 = p9[v0_2];
                if (v4_1 != 0) {
                    this.a.add(v4_1);
                }
                v0_2++;
            }
            int v0_4 = new com.teamspeak.ts3client.data.a("empty", 0, 0, 0);
            v0_4.q = 1;
            this.a.add(v0_4);
        }
        return;
    }

    static synthetic com.teamspeak.ts3client.data.customExpandableListView b(com.teamspeak.ts3client.data.g p1)
    {
        return p1.b;
    }

    private boolean b()
    {
        return this.c;
    }

    static synthetic com.teamspeak.ts3client.Ts3Application c(com.teamspeak.ts3client.data.g p1)
    {
        return p1.f;
    }

    private void c()
    {
        this.d = 1;
        return;
    }

    static synthetic boolean d(com.teamspeak.ts3client.data.g p1)
    {
        return p1.c;
    }

    static synthetic java.util.List e(com.teamspeak.ts3client.data.g p1)
    {
        return p1.a;
    }

    static synthetic android.content.Context f(com.teamspeak.ts3client.data.g p1)
    {
        return p1.e;
    }

    public final void a(boolean p2)
    {
        if ((this.d) && ((this.c) && (!p2))) {
            this.c = p2;
            this.d = 0;
            this.f.a.k.y();
        }
        this.c = p2;
        return;
    }

    public final Object getChild(int p3, int p4)
    {
        return this.f.a.d.b(((Integer) ((com.teamspeak.ts3client.data.a) this.a.get(p3)).k().get(p4)).intValue());
    }

    public final long getChildId(int p3, int p4)
    {
        return 0;
    }

    public final android.view.View getChildView(int p15, int p16, boolean p17, android.view.View p18, android.view.ViewGroup p19)
    {
        android.view.View v4_0;
        if (p18 != null) {
            v4_0 = p18;
        } else {
            v4_0 = this.g.inflate(2130903102, p19, 0);
            android.widget.LinearLayout v3_2 = new com.teamspeak.ts3client.data.t();
            v3_2.a = ((android.widget.ImageView) v4_0.findViewById(2131493248));
            v3_2.b = ((android.widget.TextView) v4_0.findViewById(2131493250));
            v3_2.c = ((android.widget.LinearLayout) v4_0.findViewById(2131493251));
            v4_0.setTag(v3_2);
        }
        try {
            com.teamspeak.ts3client.data.j v2_11 = ((com.teamspeak.ts3client.data.t) v4_0.getTag());
            v2_11.b.setTextColor(this.k);
            v2_11.c.removeAllViews();
            android.widget.LinearLayout v3_5 = v2_11;
        } catch (com.teamspeak.ts3client.data.j v2) {
            v3_5 = new com.teamspeak.ts3client.data.t();
            v3_5.a = ((android.widget.ImageView) v4_0.findViewById(2131493248));
            v3_5.b = ((android.widget.TextView) v4_0.findViewById(2131493250));
            v3_5.c = ((android.widget.LinearLayout) v4_0.findViewById(2131493251));
            v4_0.setTag(v3_5);
        }
        com.teamspeak.ts3client.data.j v2_154;
        if (((com.teamspeak.ts3client.data.a) this.a.get(p15)).o) {
            com.teamspeak.ts3client.data.c v5_2 = this.f.a.d.b(((Integer) ((com.teamspeak.ts3client.data.a) this.a.get(p15)).k().get(p16)).intValue());
            com.teamspeak.ts3client.data.j v2_35 = ((android.widget.AbsListView$LayoutParams) v4_0.getLayoutParams());
            v2_35.height = this.i;
            v4_0.setLayoutParams(v2_35);
            v4_0.setPadding((((((com.teamspeak.ts3client.data.a) this.a.get(p15)).f + 1) * this.a(1099956224)) + 2), 0, 0, 0);
            if (v5_2.w == null) {
                if (!v5_2.s) {
                    v3_5.b.setText(v5_2.a);
                } else {
                    com.teamspeak.ts3client.data.j v2_46 = v3_5.b;
                    int v7_2 = new Object[1];
                    v7_2[0] = v5_2.a;
                    v2_46.setText(com.teamspeak.ts3client.data.e.a.a("event.client.recording.record", v7_2));
                }
            } else {
                if (v5_2.s) {
                    switch (v5_2.w.d) {
                        case 0:
                            com.teamspeak.ts3client.data.j v2_52 = v3_5.b;
                            int v7_8 = new Object[1];
                            v7_8[0] = v5_2.w.a;
                            v2_52.setText(com.teamspeak.ts3client.data.e.a.a("event.client.recording.record", v7_8));
                            break;
                        case 1:
                            com.teamspeak.ts3client.data.j v2_51 = v3_5.b;
                            int v7_6 = new Object[1];
                            v7_6[0] = v5_2.w.a;
                            v2_51.setText(com.teamspeak.ts3client.data.e.a.a("event.client.recording.record", v7_6));
                            break;
                        case 2:
                            com.teamspeak.ts3client.data.j v2_50 = v3_5.b;
                            int v7_4 = new Object[1];
                            v7_4[0] = v5_2.a;
                            v2_50.setText(com.teamspeak.ts3client.data.e.a.a("event.client.recording.record", v7_4));
                            break;
                        default:
                    }
                } else {
                    switch (v5_2.w.d) {
                        case 0:
                            v3_5.b.setText(new StringBuilder("[").append(v5_2.w.a).append("] ").append(v5_2.a).toString());
                            break;
                        case 1:
                            v3_5.b.setText(v5_2.w.a);
                            break;
                        case 2:
                            v3_5.b.setText(v5_2.a);
                            break;
                    }
                }
            }
            if (v5_2.w != null) {
                switch (v5_2.w.e) {
                    case 0:
                        v3_5.b.setTextColor(this.k);
                        break;
                    case 1:
                        v3_5.b.setTextColor(-65536);
                        break;
                    case 2:
                        v3_5.b.setTextColor(-16711936);
                        break;
                }
            }
            if (v5_2.s) {
                v3_5.b.setTextColor(-65536);
            }
            if ((v5_2.c != this.f.a.h) && (!v5_2.s)) {
                v3_5.b.setTypeface(0, 0);
            } else {
                v3_5.b.setTypeface(0, 1);
            }
            if (!v5_2.l) {
                if (!v5_2.j) {
                    if (!v5_2.i) {
                        if (!v5_2.g) {
                            if (!v5_2.h) {
                                if (!v5_2.f) {
                                    if (v5_2.n) {
                                        v3_5.a.setImageBitmap(com.teamspeak.ts3client.data.d.t.a(2130837659, 1098907648, 1098907648));
                                    } else {
                                        v3_5.a.setImageBitmap(com.teamspeak.ts3client.data.d.t.a(2130837661, 1098907648, 1098907648));
                                    }
                                } else {
                                    v3_5.a.setImageBitmap(com.teamspeak.ts3client.data.d.t.a(2130837634, 1098907648, 1098907648));
                                }
                            } else {
                                v3_5.a.setImageBitmap(com.teamspeak.ts3client.data.d.t.a(2130837619, 1098907648, 1098907648));
                            }
                        } else {
                            v3_5.a.setImageBitmap(com.teamspeak.ts3client.data.d.t.a(2130837655, 1098907648, 1098907648));
                        }
                    } else {
                        v3_5.a.setImageBitmap(com.teamspeak.ts3client.data.d.t.a(2130837620, 1098907648, 1098907648));
                    }
                } else {
                    v3_5.a.setImageBitmap(com.teamspeak.ts3client.data.d.t.a(2130837658, 1098907648, 1098907648));
                    if (!v5_2.m.equals("")) {
                        if (v5_2.w == null) {
                            v3_5.b.setText(new StringBuilder().append(v5_2.a).append(" [").append(v5_2.m).append("]").toString());
                        } else {
                            if (!v5_2.w.j) {
                                switch (v5_2.w.d) {
                                    case 0:
                                        v3_5.b.setText(new StringBuilder().append(v5_2.w.a).append(" [").append(v5_2.m).append("]").toString());
                                        break;
                                    case 1:
                                        v3_5.b.setText(new StringBuilder().append(v5_2.w.a).append(" [").append(v5_2.m).append("]").toString());
                                        break;
                                    case 2:
                                        v3_5.b.setText(new StringBuilder().append(v5_2.a).append(" [").append(v5_2.m).append("]").toString());
                                        break;
                                    default:
                                }
                            } else {
                                switch (v5_2.w.d) {
                                    case 0:
                                        v3_5.b.setText(new StringBuilder("[").append(v5_2.w.a).append("] ").append(v5_2.a).toString());
                                        break;
                                    case 1:
                                        v3_5.b.setText(v5_2.w.a);
                                        break;
                                    case 2:
                                        v3_5.b.setText(v5_2.a);
                                        break;
                                    default:
                                }
                            }
                        }
                    }
                }
            } else {
                v3_5.a.setImageBitmap(com.teamspeak.ts3client.data.d.t.a(2130837635, 1098907648, 1098907648));
            }
            if (v5_2.A != 1) {
                if (v5_2.d == 1) {
                    if (v5_2.n) {
                        v3_5.a.setImageBitmap(com.teamspeak.ts3client.data.d.t.a(2130837660, 1098907648, 1098907648));
                    } else {
                        v3_5.a.setImageBitmap(com.teamspeak.ts3client.data.d.t.a(2130837662, 1098907648, 1098907648));
                    }
                }
            } else {
                v3_5.a.setImageBitmap(com.teamspeak.ts3client.data.d.t.a(2130837663, 1098907648, 1098907648));
            }
            if (v5_2.k) {
                com.teamspeak.ts3client.data.j v2_109 = new android.widget.ImageView(this.e);
                v2_109.setMinimumHeight(20);
                v2_109.setMinimumWidth(20);
                v2_109.setImageBitmap(com.teamspeak.ts3client.data.d.t.a(2130837599, 1098907648, 1098907648));
                v3_5.c.addView(v2_109);
            }
            if (v5_2.o < ((com.teamspeak.ts3client.data.a) this.a.get(p15)).h) {
                if (!v5_2.r) {
                    com.teamspeak.ts3client.data.j v2_116 = new android.widget.ImageView(this.e);
                    v2_116.setMinimumHeight(20);
                    v2_116.setMinimumWidth(20);
                    v2_116.setImageBitmap(com.teamspeak.ts3client.data.d.t.a(2130837634, 1098907648, 1098907648));
                    v3_5.c.addView(v2_116);
                } else {
                    com.teamspeak.ts3client.data.j v2_118 = new android.widget.ImageView(this.e);
                    v2_118.setMinimumHeight(20);
                    v2_118.setMinimumWidth(20);
                    v2_118.setImageBitmap(com.teamspeak.ts3client.data.d.t.a(2130837598, 1098907648, 1098907648));
                    v3_5.c.addView(v2_118);
                }
                if ((!v5_2.p.equals("0")) && (!v5_2.r)) {
                    com.teamspeak.ts3client.data.j v2_123 = new android.widget.ImageView(this.e);
                    v2_123.setMinimumHeight(20);
                    v2_123.setMinimumWidth(20);
                    v2_123.setImageBitmap(com.teamspeak.ts3client.data.d.t.a(2130837673, 1098907648, 1098907648));
                    v3_5.c.addView(v2_123);
                }
            }
            com.teamspeak.ts3client.data.j v2_125 = Integer.parseInt(v5_2.u);
            if ((this.f.a.p.a(((long) v2_125))) && (this.f.a.p.b(((long) v2_125)).d != 0)) {
                android.graphics.drawable.BitmapDrawable v6_130 = new android.widget.ImageView(this.e);
                v6_130.setBackgroundDrawable(this.f.a.r.a(this.f.a.p.b(((long) v2_125)).d).a());
                v6_130.setMinimumHeight(20);
                v6_130.setMinimumWidth(20);
                v3_5.c.addView(v6_130);
            }
            android.graphics.drawable.BitmapDrawable v6_132 = v5_2.t.split(",");
            int v7_58 = v6_132.length;
            com.teamspeak.ts3client.data.j v2_133 = 0;
            while (v2_133 < v7_58) {
                android.widget.LinearLayout v8_30 = Integer.parseInt(v6_132[v2_133]);
                if ((this.f.a.o.a(((long) v8_30))) && (this.f.a.o.b(((long) v8_30)).d != 0)) {
                    android.widget.ImageView v9_16 = new android.widget.ImageView(this.e);
                    v9_16.setMinimumHeight(20);
                    v9_16.setMinimumWidth(20);
                    v9_16.setBackgroundDrawable(this.f.a.r.a(this.f.a.o.b(((long) v8_30)).d).a());
                    v3_5.c.addView(v9_16);
                }
                v2_133++;
            }
            if (v5_2.z != 0) {
                com.teamspeak.ts3client.data.j v2_136 = new android.widget.ImageView(this.e);
                v2_136.setBackgroundDrawable(this.f.a.r.a(v5_2.z).a());
                v2_136.setMinimumHeight(20);
                v2_136.setMinimumWidth(20);
                v3_5.c.addView(v2_136);
            }
            if (this.f.e.getBoolean("show_flags", 0)) {
                com.teamspeak.ts3client.data.j v2_142 = this.f.b().a(v5_2.x.toLowerCase());
                if (v2_142 != null) {
                    android.graphics.drawable.BitmapDrawable v6_147 = new android.graphics.drawable.BitmapDrawable(this.f.getResources(), v2_142);
                    com.teamspeak.ts3client.data.j v2_144 = new android.widget.ImageView(this.e);
                    v2_144.setMinimumHeight(20);
                    v2_144.setMinimumWidth(20);
                    v2_144.setBackgroundDrawable(v6_147);
                    v3_5.c.addView(v2_144);
                }
            }
            if (this.f.e.getBoolean("longclick_client", 1)) {
                v4_0.setOnClickListener(new com.teamspeak.ts3client.data.i(this));
                v4_0.setOnTouchListener(new com.teamspeak.ts3client.data.j(this, v5_2));
                v2_154 = v4_0;
            } else {
                v4_0.setOnTouchListener(new com.teamspeak.ts3client.data.h(this, v5_2));
            }
        } else {
            v2_154 = v4_0;
        }
        return v2_154;
    }

    public final int getChildrenCount(int p3)
    {
        int v0_14;
        if (p3 != 0) {
            if (this.a.get(p3) != null) {
                if ((!((com.teamspeak.ts3client.data.a) this.a.get(p3)).p) && (((com.teamspeak.ts3client.data.a) this.a.get(p3)).o)) {
                    v0_14 = ((com.teamspeak.ts3client.data.a) this.a.get(p3)).k().size();
                } else {
                    v0_14 = 0;
                }
            } else {
                v0_14 = 0;
            }
        } else {
            v0_14 = 0;
        }
        return v0_14;
    }

    public final Object getGroup(int p2)
    {
        return this.a.get(p2);
    }

    public final int getGroupCount()
    {
        int v0_2;
        if (this.a != null) {
            v0_2 = this.a.size();
        } else {
            v0_2 = 0;
        }
        return v0_2;
    }

    public final long getGroupId(int p3)
    {
        return ((long) p3);
    }

    public final android.view.View getGroupView(int p9, boolean p10, android.view.View p11, android.view.ViewGroup p12)
    {
        if (p11 == null) {
            p11 = this.g.inflate(2130903101, p12, 0);
            android.graphics.drawable.Drawable v1_2 = new com.teamspeak.ts3client.data.u();
            v1_2.a = ((android.widget.LinearLayout) p11.findViewById(2131493247));
            v1_2.b = ((android.widget.ImageView) p11.findViewById(2131493243));
            v1_2.c = ((android.widget.TextView) p11.findViewById(2131493246));
            v1_2.d = ((android.widget.ImageView) p11.findViewById(2131493244));
            p11.setTag(v1_2);
        }
        if ((this.a != null) && (!((com.teamspeak.ts3client.data.a) this.a.get(p9)).p)) {
            Exception v0_19 = ((com.teamspeak.ts3client.data.u) p11.getTag());
            android.graphics.drawable.Drawable v1_4 = ((android.widget.AbsListView$LayoutParams) p11.getLayoutParams());
            v1_4.height = this.h;
            p11.setLayoutParams(v1_4);
            v0_19.a.removeAllViews();
            if (p9 != (this.a.size() - 1)) {
                if (p11.getVisibility() != 0) {
                    p11.setVisibility(0);
                }
                if (p9 != 0) {
                    p11.setPadding((((com.teamspeak.ts3client.data.a) this.a.get(p9)).f * this.a(1099956224)), 0, 0, 0);
                    if (!((com.teamspeak.ts3client.data.a) this.a.get(p9)).o) {
                        v0_19.b.setVisibility(0);
                        v0_19.b.setBackgroundResource(2130837612);
                    } else {
                        v0_19.b.setVisibility(0);
                        v0_19.b.setBackgroundResource(2130837611);
                    }
                    if (!((com.teamspeak.ts3client.data.a) this.a.get(p9)).q) {
                        v0_19.b.setVisibility(0);
                    } else {
                        v0_19.b.setVisibility(4);
                    }
                    android.widget.ImageView v2_7 = this.l.matcher(((com.teamspeak.ts3client.data.a) this.a.get(p9)).a);
                    if ((v2_7.find()) && (((com.teamspeak.ts3client.data.a) this.a.get(p9)).f == 0)) {
                        v0_19.c.setText(v2_7.group(2));
                        if (v2_7.group(1) != null) {
                            if (!v2_7.group(1).equals("r")) {
                                if (!v2_7.group(1).equals("c")) {
                                    if (v2_7.group(1).equals("*")) {
                                        com.teamspeak.ts3client.data.g.a(v0_19.c, v2_7.group(2));
                                    }
                                    v0_19.c.setGravity(19);
                                } else {
                                    v0_19.c.setGravity(17);
                                }
                            } else {
                                v0_19.c.setGravity(21);
                            }
                        }
                        if ((v2_7.group(2).equals("---")) || ((v2_7.group(2).equals("...")) || ((v2_7.group(2).equals("-.-")) || ((v2_7.group(2).equals("___")) || (v2_7.group(2).equals("-..")))))) {
                            if (!v2_7.group(2).equals("-.-")) {
                                if (!v2_7.group(2).equals("___")) {
                                    com.teamspeak.ts3client.data.g.a(v0_19.c, v2_7.group(2));
                                } else {
                                    com.teamspeak.ts3client.data.g.a(v0_19.c, "\u2500");
                                }
                            } else {
                                com.teamspeak.ts3client.data.g.a(v0_19.c, "-.");
                            }
                            v0_19.c.setGravity(19);
                        }
                        v0_19.d.setVisibility(8);
                    } else {
                        v0_19.d.setVisibility(0);
                        if (!((com.teamspeak.ts3client.data.a) this.a.get(p9)).j) {
                            if ((((com.teamspeak.ts3client.data.a) this.a.get(p9)).i == -1) || (((com.teamspeak.ts3client.data.a) this.a.get(p9)).i > ((com.teamspeak.ts3client.data.a) this.a.get(p9)).k().size())) {
                                if ((!((com.teamspeak.ts3client.data.a) this.a.get(p9)).m) || (((com.teamspeak.ts3client.data.a) this.a.get(p9)).n)) {
                                    if (!((com.teamspeak.ts3client.data.a) this.a.get(p9)).k) {
                                        v0_19.d.setImageBitmap(com.teamspeak.ts3client.data.d.t.a(2130837588, 1098907648, 1098907648));
                                    } else {
                                        v0_19.d.setImageBitmap(com.teamspeak.ts3client.data.d.t.a(2130837589, 1098907648, 1098907648));
                                    }
                                } else {
                                    if (!((com.teamspeak.ts3client.data.a) this.a.get(p9)).k) {
                                        v0_19.d.setImageBitmap(com.teamspeak.ts3client.data.d.t.a(2130837593, 1098907648, 1098907648));
                                    } else {
                                        v0_19.d.setImageBitmap(com.teamspeak.ts3client.data.d.t.a(2130837594, 1098907648, 1098907648));
                                    }
                                }
                            } else {
                                if (!((com.teamspeak.ts3client.data.a) this.a.get(p9)).k) {
                                    v0_19.d.setImageBitmap(com.teamspeak.ts3client.data.d.t.a(2130837591, 1098907648, 1098907648));
                                } else {
                                    v0_19.d.setImageBitmap(com.teamspeak.ts3client.data.d.t.a(2130837592, 1098907648, 1098907648));
                                }
                            }
                        } else {
                            v0_19.d.setImageBitmap(com.teamspeak.ts3client.data.d.t.a(2130837590, 1098907648, 1098907648));
                        }
                        v0_19.c.setText(((com.teamspeak.ts3client.data.a) this.a.get(p9)).a);
                        v0_19.c.setGravity(19);
                        if (((com.teamspeak.ts3client.data.a) this.a.get(p9)).r) {
                            android.graphics.drawable.Drawable v1_123 = new android.widget.ImageView(this.e);
                            v1_123.setMinimumHeight(20);
                            v1_123.setMinimumWidth(20);
                            v1_123.setImageBitmap(com.teamspeak.ts3client.data.d.t.a(2130837587, 1098907648, 1098907648));
                            v0_19.a.addView(v1_123);
                        }
                        if ((((com.teamspeak.ts3client.data.a) this.a.get(p9)).l == 3) || (((com.teamspeak.ts3client.data.a) this.a.get(p9)).l == 5)) {
                            android.graphics.drawable.Drawable v1_133 = new android.widget.ImageView(this.e);
                            v1_133.setMinimumHeight(20);
                            v1_133.setMinimumWidth(20);
                            v1_133.setImageBitmap(com.teamspeak.ts3client.data.d.t.a(2130837653, 1098907648, 1098907648));
                            v0_19.a.addView(v1_133);
                        }
                        if (((com.teamspeak.ts3client.data.a) this.a.get(p9)).h != 0) {
                            android.graphics.drawable.Drawable v1_139 = new android.widget.ImageView(this.e);
                            v1_139.setMinimumHeight(20);
                            v1_139.setMinimumWidth(20);
                            v1_139.setImageBitmap(com.teamspeak.ts3client.data.d.t.a(2130837651, 1098907648, 1098907648));
                            v0_19.a.addView(v1_139);
                        }
                        if (((com.teamspeak.ts3client.data.a) this.a.get(p9)).m) {
                            android.graphics.drawable.Drawable v1_145 = new android.widget.ImageView(this.e);
                            v1_145.setMinimumHeight(20);
                            v1_145.setMinimumWidth(20);
                            v1_145.setImageBitmap(com.teamspeak.ts3client.data.d.t.a(2130837670, 1098907648, 1098907648));
                            v0_19.a.addView(v1_145);
                        }
                        if (((com.teamspeak.ts3client.data.a) this.a.get(p9)).s != 0) {
                            android.widget.ImageView v2_50 = new android.widget.ImageView(this.e);
                            v2_50.setMinimumHeight(20);
                            v2_50.setMinimumWidth(20);
                            try {
                                v2_50.setBackgroundDrawable(this.f.a.r.a(((com.teamspeak.ts3client.data.a) this.a.get(p9)).s).a());
                                v0_19.a.addView(v2_50);
                            } catch (Exception v0) {
                            }
                        }
                    }
                    if (!((com.teamspeak.ts3client.data.a) this.a.get(p9)).o) {
                        this.b.collapseGroup(p9);
                    } else {
                        this.b.expandGroup(p9);
                    }
                    p11.setOnClickListener(new com.teamspeak.ts3client.data.l(this, p9));
                    p11.setOnTouchListener(new com.teamspeak.ts3client.data.m(this, p9));
                } else {
                    p11 = this.a(p11, v0_19);
                }
            } else {
                p11.setVisibility(4);
            }
        }
        return p11;
    }

    public final boolean hasStableIds()
    {
        return 0;
    }

    public final boolean isChildSelectable(int p2, int p3)
    {
        return 0;
    }
}
