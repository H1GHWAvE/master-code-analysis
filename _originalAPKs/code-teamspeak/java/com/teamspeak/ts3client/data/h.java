package com.teamspeak.ts3client.data;
final class h implements android.view.View$OnTouchListener {
    final synthetic com.teamspeak.ts3client.data.c a;
    final synthetic com.teamspeak.ts3client.data.g b;
    private int c;
    private int d;
    private int e;
    private android.view.View f;
    private int g;

    h(com.teamspeak.ts3client.data.g p2, com.teamspeak.ts3client.data.c p3)
    {
        this.b = p2;
        this.a = p3;
        this.c = 0;
        this.d = 0;
        this.e = 0;
        this.g = 0;
        return;
    }

    public final boolean onTouch(android.view.View p10, android.view.MotionEvent p11)
    {
        int v7 = 0;
        if (p11.getAction() == 0) {
            this.c = 0;
            this.g = p10.getPaddingLeft();
            this.d = ((int) p11.getX());
            this.e = ((int) p11.getX());
            this.f = p10;
        }
        if (p11.getAction() == 2) {
            this.e = ((int) p11.getX());
            this.c = (this.e - this.d);
        }
        if ((p11.getAction() != 1) && (p11.getAction() != 3)) {
            if (this.f != null) {
                if (this.c == 0) {
                    p10.setBackgroundColor(858678526);
                }
                if (this.c > 85) {
                    p10.setBackgroundColor(855638271);
                }
                if (this.c >= -85) {
                    p10.setPadding((this.c + this.g), 0, 0, 0);
                } else {
                    com.teamspeak.ts3client.data.g.b(this.b).onTouchEvent(android.view.MotionEvent.obtain(0, 0, 3, 0, 0, 0));
                    p10.setPadding(this.g, 0, 0, 0);
                    p10.setBackgroundColor(0);
                    com.teamspeak.ts3client.data.g.c(this.b).a.k.a(this.a);
                    return v7;
                }
            }
            v7 = 1;
        } else {
            if (this.c > 85) {
                new com.teamspeak.ts3client.d.b.a(this.a, 0).a(com.teamspeak.ts3client.data.g.a(this.b), "ClientActionDialog");
            }
            this.c = 0;
            this.d = 0;
            this.e = 0;
            p10.setPadding(this.g, 0, 0, 0);
            p10.setBackgroundColor(0);
        }
        return v7;
    }
}
