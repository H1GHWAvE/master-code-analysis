package com.teamspeak.ts3client.data;
public final class v implements android.content.SharedPreferences$OnSharedPreferenceChangeListener, com.teamspeak.ts3client.data.x {
    private static com.teamspeak.ts3client.data.v d;
    public java.util.BitSet a;
    public boolean b;
    public com.teamspeak.ts3client.data.y c;
    private java.util.BitSet e;
    private com.teamspeak.ts3client.Ts3Application f;
    private boolean g;

    public v()
    {
        this.a = new java.util.BitSet();
        this.e = new java.util.BitSet();
        this.b = 0;
        this.f = com.teamspeak.ts3client.Ts3Application.a();
        this.f.e.registerOnSharedPreferenceChangeListener(this);
        this.b = this.f.e.getBoolean("audio_ptt", 0);
        return;
    }

    public static declared_synchronized com.teamspeak.ts3client.data.v a()
    {
        try {
            if (com.teamspeak.ts3client.data.v.d == null) {
                com.teamspeak.ts3client.data.v.d = new com.teamspeak.ts3client.data.v();
            }
        } catch (com.teamspeak.ts3client.data.v v0_3) {
            throw v0_3;
        }
        return com.teamspeak.ts3client.data.v.d;
    }

    private void a(com.teamspeak.ts3client.data.y p1)
    {
        this.c = p1;
        return;
    }

    private void a(boolean p1)
    {
        this.b = p1;
        return;
    }

    private boolean b()
    {
        int v1 = 0;
        if (!this.e.isEmpty()) {
            int v0_2 = 1;
            boolean v2_1 = this.e.nextSetBit(0);
            while (v2_1) {
                if (this.a.get(v2_1)) {
                    v2_1 = this.e.nextSetBit((v2_1 + 1));
                } else {
                    v0_2 = 0;
                    break;
                }
            }
            if (this.f.a != null) {
                this.f.a.k.g(v0_2);
                if (this.g) {
                    v1 = v0_2;
                }
            }
        }
        return v1;
    }

    private void c()
    {
        this.c = 0;
        return;
    }

    private java.util.BitSet d()
    {
        return this.a;
    }

    private boolean e()
    {
        return this.b;
    }

    private java.util.BitSet f()
    {
        return this.e;
    }

    public final void a(java.util.BitSet p1, boolean p2)
    {
        this.e = p1;
        this.g = p2;
        return;
    }

    public final boolean a(android.view.KeyEvent p5)
    {
        int v1 = 0;
        if ((this.b) || (this.c != null)) {
            if (p5.getAction() != 0) {
                if (this.a.get(p5.getKeyCode())) {
                    this.a.clear(p5.getKeyCode());
                    if (this.c != null) {
                        this.c.p_();
                    }
                }
            } else {
                if (!this.a.get(p5.getKeyCode())) {
                    this.a.set(p5.getKeyCode());
                }
            }
            if ((!this.b) || (this.c != null)) {
                if (this.c != null) {
                    this.c.a();
                }
            } else {
                if (!this.e.isEmpty()) {
                    int v0_17 = 1;
                    boolean v2_5 = this.e.nextSetBit(0);
                    while (v2_5) {
                        if (this.a.get(v2_5)) {
                            v2_5 = this.e.nextSetBit((v2_5 + 1));
                        } else {
                            v0_17 = 0;
                            break;
                        }
                    }
                    if (this.f.a != null) {
                        this.f.a.k.g(v0_17);
                        if (this.g) {
                            v1 = v0_17;
                        }
                    }
                }
            }
        }
        return v1;
    }

    public final void onSharedPreferenceChanged(android.content.SharedPreferences p4, String p5)
    {
        if (p5.equals("audio_ptt")) {
            this.b = this.f.e.getBoolean("audio_ptt", 0);
        }
        return;
    }
}
