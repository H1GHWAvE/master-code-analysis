package com.teamspeak.ts3client.data.e;
public final class a {
    private static final String a = "LangSystem";
    private static java.util.HashMap b;
    private java.util.regex.Pattern c;
    private java.util.regex.Pattern d;
    private java.util.regex.Pattern e;

    public a(android.content.Context p8, String p9)
    {
        this.c = java.util.regex.Pattern.compile("(<entry>.+?</entry>)", 32);
        this.d = java.util.regex.Pattern.compile("<key>(.+)?</key>", 32);
        this.e = java.util.regex.Pattern.compile("<value>(.+)?</value>", 32);
        String v0_7 = com.teamspeak.ts3client.data.e.c.h.i;
        android.util.Log.d("defaultLang_tag", v0_7);
        String v2 = java.util.Locale.getDefault().getLanguage();
        com.teamspeak.ts3client.data.e.c[] v3 = com.teamspeak.ts3client.data.e.c.values();
        java.util.HashMap v1_3 = 0;
        while (v1_3 < v3.length) {
            com.teamspeak.ts3client.data.e.c v5 = v3[v1_3];
            if (!v2.equals(v5.i)) {
                v1_3++;
            } else {
                v0_7 = v5.i;
                break;
            }
        }
        com.teamspeak.ts3client.data.e.a.b = this.a(p8, v0_7);
        if (!p9.isEmpty()) {
            v0_7 = p9;
        }
        new java.util.HashMap();
        com.teamspeak.ts3client.data.e.a.b.putAll(this.a(p8, v0_7));
        return;
    }

    public static android.widget.SpinnerAdapter a(String p6, android.content.Context p7, int p8)
    {
        String[] v0_1;
        String[] v0_0 = 0;
        android.widget.ArrayAdapter v1_1 = new Object[1];
        v1_1[0] = 0;
        int v2_1 = com.teamspeak.ts3client.data.e.a.a(p6, v1_1);
        if (!v2_1.contains(",")) {
            com.teamspeak.ts3client.Ts3Application.a().d.log(java.util.logging.Level.WARNING, new StringBuilder("Broken LangEntry:").append(p6).toString());
            android.widget.ArrayAdapter v1_6 = new String[p8];
            while (v0_0 < p8) {
                v1_6[v0_0] = v2_1;
                v0_0++;
            }
            v0_1 = v1_6;
        } else {
            android.widget.ArrayAdapter v1_8 = v2_1.split("(?<!\\\\),");
            while (v0_0 < v1_8.length) {
                v1_8[v0_0] = v1_8[v0_0].replace("\\,", ",");
                v0_0++;
            }
            v0_1 = v1_8;
        }
        return new android.widget.ArrayAdapter(p7, 17367049, v0_1);
    }

    public static String a(String p3)
    {
        String v0_1 = new Object[1];
        v0_1[0] = 0;
        return com.teamspeak.ts3client.data.e.a.a(p3, v0_1);
    }

    public static varargs String a(String p4, Object[] p5)
    {
        String v0_6;
        if (!com.teamspeak.ts3client.data.e.a.b.containsKey(p4)) {
            v0_6 = new StringBuilder("<").append(p4).append(">").toString();
        } else {
            try {
                v0_6 = String.format(((String) com.teamspeak.ts3client.data.e.a.b.get(p4)), p5);
            } catch (String v0_10) {
                android.util.Log.e("LangSystem", new StringBuilder().append(p4).append(": ").append(v0_10.getMessage()).toString());
                v0_6 = new StringBuilder().append(((String) com.teamspeak.ts3client.data.e.a.b.get(p4))).append("(PARSING ERROR)").toString();
            }
        }
        return v0_6;
    }

    private java.util.HashMap a(android.content.Context p9, String p10)
    {
        java.util.HashMap v0_1 = new java.util.HashMap();
        java.util.regex.Matcher v1_1 = this.c.matcher(com.teamspeak.ts3client.data.e.a.b(p9, p10));
        while (v1_1.find()) {
            String v2_2 = v1_1.group();
            java.util.logging.Logger v3_1 = this.d.matcher(v2_2);
            if (v3_1.find()) {
                java.util.logging.Logger v3_2 = v3_1.group(1);
                if (v3_2 != null) {
                    String v4_2 = this.e.matcher(v2_2);
                    if (v4_2.find()) {
                        String v4_3 = v4_2.group(1);
                        if (v4_3 != null) {
                            v0_1.put(v3_2, v4_3.replace("\\n", "\n").replace("\\r", "\r"));
                        }
                    }
                    com.teamspeak.ts3client.Ts3Application.a().d.log(java.util.logging.Level.WARNING, new StringBuilder("Broken LangEntry:").append(v2_2).toString());
                }
            }
            com.teamspeak.ts3client.Ts3Application.a().d.log(java.util.logging.Level.WARNING, new StringBuilder("Broken LangEntry:").append(v2_2).toString());
        }
        return v0_1;
    }

    private static java.util.List a()
    {
        java.util.List v0_7;
        java.io.File v1_3 = new java.io.File(new StringBuilder().append(android.os.Environment.getExternalStorageDirectory()).append("/TS3/content/lang/").toString());
        if (!v1_3.exists()) {
            v0_7 = new String[0];
        } else {
            v0_7 = v1_3.list(new com.teamspeak.ts3client.data.e.b(v1_3));
        }
        return java.util.Arrays.asList(v0_7);
    }

    private static void a(String p3, android.support.v7.app.i p4, int p5)
    {
        android.widget.TextView v0_1 = new Object[1];
        v0_1[0] = 0;
        ((android.widget.TextView) p4.findViewById(p5)).setText(com.teamspeak.ts3client.data.e.a.a(p3, v0_1));
        return;
    }

    public static void a(String p3, android.view.View p4, int p5)
    {
        android.widget.TextView v0_1 = new Object[1];
        v0_1[0] = 0;
        ((android.widget.TextView) p4.findViewById(p5)).setText(com.teamspeak.ts3client.data.e.a.a(p3, v0_1));
        return;
    }

    public static void a(String p3, android.view.ViewGroup p4, int p5)
    {
        android.widget.TextView v0_1 = new Object[1];
        v0_1[0] = 0;
        ((android.widget.TextView) p4.findViewById(p5)).setText(com.teamspeak.ts3client.data.e.a.a(p3, v0_1));
        return;
    }

    public static void a(String p3, android.widget.TextView p4)
    {
        String v0_1 = new Object[1];
        v0_1[0] = 0;
        p4.setText(com.teamspeak.ts3client.data.e.a.a(p3, v0_1));
        return;
    }

    private static String[] a(String p6, int p7)
    {
        String[] v0_1;
        String[] v0_0 = 0;
        String[] v1_1 = new Object[1];
        v1_1[0] = 0;
        String v2_1 = com.teamspeak.ts3client.data.e.a.a(p6, v1_1);
        if (!v2_1.contains(",")) {
            com.teamspeak.ts3client.Ts3Application.a().d.log(java.util.logging.Level.WARNING, new StringBuilder("Broken LangEntry:").append(p6).toString());
            String[] v1_6 = new String[p7];
            while (v0_0 < p7) {
                v1_6[v0_0] = v2_1;
                v0_0++;
            }
            v0_1 = v1_6;
        } else {
            String[] v1_8 = v2_1.split("(?<!\\\\),");
            while (v0_0 < v1_8.length) {
                v1_8[v0_0] = v1_8[v0_0].replace("\\,", ",");
                v0_0++;
            }
            v0_1 = v1_8;
        }
        return v0_1;
    }

    public static com.teamspeak.ts3client.customs.a b(String p6, android.content.Context p7, int p8)
    {
        String[] v0_1;
        String[] v0_0 = 0;
        com.teamspeak.ts3client.customs.a v1_1 = new Object[1];
        v1_1[0] = 0;
        String v2_1 = com.teamspeak.ts3client.data.e.a.a(p6, v1_1);
        if (!v2_1.contains(",")) {
            com.teamspeak.ts3client.Ts3Application.a().d.log(java.util.logging.Level.WARNING, new StringBuilder("Broken LangEntry:").append(p6).toString());
            com.teamspeak.ts3client.customs.a v1_6 = new String[p8];
            while (v0_0 < p8) {
                v1_6[v0_0] = v2_1;
                v0_0++;
            }
            v0_1 = v1_6;
        } else {
            com.teamspeak.ts3client.customs.a v1_8 = v2_1.split("(?<!\\\\),");
            while (v0_0 < v1_8.length) {
                v1_8[v0_0] = v1_8[v0_0].replace("\\,", ",");
                v0_0++;
            }
            v0_1 = v1_8;
        }
        return new com.teamspeak.ts3client.customs.a(p7, v0_1);
    }

    private static String b(android.content.Context p8, String p9)
    {
        java.io.FileInputStream v1_0 = 0;
        String v0_0 = 0;
        try {
            StringBuilder v3_0 = com.teamspeak.ts3client.data.e.c.values();
            String v2_0 = 0;
        } catch (java.io.FileInputStream v1) {
            com.teamspeak.ts3client.Ts3Application.a().d.log(java.util.logging.Level.SEVERE, new StringBuilder("No Lang File for: ").append(p9).toString());
            String v0_4;
            if (v0_0 != null) {
                java.io.FileInputStream v1_19 = new StringBuffer();
                String v2_12 = new java.io.InputStreamReader(v0_0);
                StringBuilder v3_10 = new java.io.BufferedReader(v2_12);
                try {
                    String v0_2 = v3_10.readLine();
                } catch (String v0_3) {
                    v0_3.printStackTrace();
                    v0_4 = v1_19.toString();
                }
                while (v0_2 != null) {
                    v1_19.append(v0_2);
                    v0_2 = v3_10.readLine();
                }
                v3_10.close();
                v2_12.close();
            } else {
                v0_4 = "";
            }
            return v0_4;
        }
        while (v2_0 < v3_0.length) {
            String v5 = v3_0[v2_0];
            if (!p9.equals(v5.i)) {
                v2_0++;
            } else {
                v0_0 = p8.getAssets().open(new StringBuilder("lang/").append(v5.j).toString());
                v1_0 = 1;
                break;
            }
        }
        if (v1_0 != null) {
        } else {
            String v2_8 = new java.io.File(new StringBuilder().append(new StringBuilder().append(android.os.Environment.getExternalStorageDirectory()).append("/TS3/content/lang/").toString()).append(p9).toString());
            if ((!v2_8.exists()) || (!v2_8.isFile())) {
                v0_0 = p8.getAssets().open("lang_eng.xml");
            } else {
                v0_0 = new java.io.FileInputStream(v2_8);
            }
        }
    }
}
