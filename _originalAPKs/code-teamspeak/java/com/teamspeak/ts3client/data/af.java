package com.teamspeak.ts3client.data;
public final class af {
    private static com.teamspeak.ts3client.data.af f;
    public int a;
    public String b;
    boolean c;
    public com.teamspeak.ts3client.data.z d;
    public boolean e;

    private af()
    {
        this.a = 0;
        return;
    }

    private static synthetic int a(com.teamspeak.ts3client.data.af p0, int p1)
    {
        p0.a = p1;
        return p1;
    }

    public static declared_synchronized com.teamspeak.ts3client.data.af a()
    {
        try {
            if (com.teamspeak.ts3client.data.af.f == null) {
                com.teamspeak.ts3client.data.af.f = new com.teamspeak.ts3client.data.af();
            }
        } catch (com.teamspeak.ts3client.data.af v0_3) {
            throw v0_3;
        }
        return com.teamspeak.ts3client.data.af.f;
    }

    private void a(com.teamspeak.ts3client.data.z p1)
    {
        this.d = p1;
        return;
    }

    private static synthetic boolean a(com.teamspeak.ts3client.data.af p1)
    {
        return p1.c;
    }

    private static synthetic boolean a(com.teamspeak.ts3client.data.af p0, boolean p1)
    {
        p0.c = p1;
        return p1;
    }

    private static synthetic com.teamspeak.ts3client.data.z b(com.teamspeak.ts3client.data.af p1)
    {
        return p1.d;
    }

    private void b()
    {
        if (!this.e) {
            com.teamspeak.ts3client.data.z v0_1 = java.util.Calendar.getInstance();
            if ((com.teamspeak.ts3client.Ts3Application.a().e.getInt("LicenseAgreement", 0) != 0) && (com.teamspeak.ts3client.Ts3Application.a().e.getLong("la_lastcheck", 0) >= (v0_1.getTimeInMillis() - 86400000))) {
                android.util.Log.i("UpdateServerData", "skipped downloading new update info");
                this.d.o();
            } else {
                this.b = com.teamspeak.ts3client.jni.Ts3Jni.b().ts3client_android_getUpdaterURL();
                this.e = 1;
                com.teamspeak.ts3client.data.z v0_10 = new com.teamspeak.ts3client.data.ah(this, 0);
                String v1_6 = new String[1];
                v1_6[0] = this.b;
                v0_10.execute(v1_6);
            }
        }
        return;
    }

    private int c()
    {
        return this.a;
    }

    private static synthetic boolean c(com.teamspeak.ts3client.data.af p1)
    {
        p1.e = 0;
        return 0;
    }

    private boolean d()
    {
        return this.c;
    }
}
