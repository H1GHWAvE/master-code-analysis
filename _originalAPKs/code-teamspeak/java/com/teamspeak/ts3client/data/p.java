package com.teamspeak.ts3client.data;
final class p implements android.view.GestureDetector$OnGestureListener {
    final synthetic com.teamspeak.ts3client.data.o a;

    p(com.teamspeak.ts3client.data.o p1)
    {
        this.a = p1;
        return;
    }

    public final boolean onDown(android.view.MotionEvent p2)
    {
        return 0;
    }

    public final boolean onFling(android.view.MotionEvent p2, android.view.MotionEvent p3, float p4, float p5)
    {
        return 0;
    }

    public final void onLongPress(android.view.MotionEvent p10)
    {
        android.view.MotionEvent v0_0 = 1;
        android.widget.Button v1_2 = new android.app.AlertDialog$Builder(com.teamspeak.ts3client.data.g.f(this.a.b)).create();
        com.teamspeak.ts3client.data.d.q.a(v1_2);
        v1_2.setTitle(com.teamspeak.ts3client.data.e.a.a("dialog.virtualserver.edit.info"));
        boolean v3_0 = new Object[1];
        v3_0[0] = com.teamspeak.ts3client.data.g.c(this.a.b).a.m();
        v1_2.setMessage(com.teamspeak.ts3client.data.e.a.a("dialog.virtualserver.edit.text", v3_0));
        v1_2.setButton(-2, com.teamspeak.ts3client.data.e.a.a("button.serverinfo"), new com.teamspeak.ts3client.data.q(this));
        v1_2.setButton(-3, com.teamspeak.ts3client.data.e.a.a("button.edit"), new com.teamspeak.ts3client.data.r(this));
        v1_2.setButton(-1, com.teamspeak.ts3client.data.e.a.a("dialog.virtualserver.edit.buttonpw"), new com.teamspeak.ts3client.data.s(this));
        v1_2.setCancelable(1);
        v1_2.show();
        long v2_12 = v1_2.getButton(-1);
        android.widget.Button v1_3 = v1_2.getButton(-2);
        if ((!com.teamspeak.ts3client.data.g.c(this.a.b).a.s.a(com.teamspeak.ts3client.jni.g.au)) && (!com.teamspeak.ts3client.data.g.c(this.a.b).a.s.a(com.teamspeak.ts3client.jni.g.av))) {
            v0_0 = 0;
        }
        boolean v3_24 = com.teamspeak.ts3client.data.g.c(this.a.b).a.s.a(com.teamspeak.ts3client.jni.g.x);
        if ((v0_0 == null) || (v2_12 == 0)) {
            v2_12.setEnabled(0);
        }
        if ((!v3_24) || (v1_3 == null)) {
            v1_3.setEnabled(0);
        }
        com.teamspeak.ts3client.data.g.b(this.a.b).onTouchEvent(android.view.MotionEvent.obtain(0, 0, 3, 0, 0, 0));
        return;
    }

    public final boolean onScroll(android.view.MotionEvent p2, android.view.MotionEvent p3, float p4, float p5)
    {
        return 0;
    }

    public final void onShowPress(android.view.MotionEvent p1)
    {
        return;
    }

    public final boolean onSingleTapUp(android.view.MotionEvent p2)
    {
        return 0;
    }
}
