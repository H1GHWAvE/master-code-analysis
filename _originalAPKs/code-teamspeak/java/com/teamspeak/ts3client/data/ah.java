package com.teamspeak.ts3client.data;
public final class ah extends android.os.AsyncTask {
    final synthetic com.teamspeak.ts3client.data.af a;

    private ah(com.teamspeak.ts3client.data.af p1)
    {
        this.a = p1;
        return;
    }

    public synthetic ah(com.teamspeak.ts3client.data.af p1, byte p2)
    {
        this(p1);
        return;
    }

    private static varargs java.nio.ByteBuffer a(String[] p6)
    {
        java.nio.ByteBuffer v0_3 = ((java.net.HttpURLConnection) new java.net.URL(p6[0]).openConnection());
        v0_3.setReadTimeout(2500);
        java.nio.ByteBuffer v1_3 = v0_3.getInputStream();
        byte[] v2_1 = new java.io.ByteArrayOutputStream();
        int v3_1 = new byte[1024];
        while(true) {
            int v4_1 = v1_3.read(v3_1, 0, 1024);
            if (v4_1 == -1) {
                break;
            }
            v2_1.write(v3_1, 0, v4_1);
        }
        java.nio.ByteBuffer v1_5 = java.nio.ByteBuffer.allocateDirect(v0_3.getContentLength());
        v1_5.put(v2_1.toByteArray(), 0, v0_3.getContentLength());
        v0_3.disconnect();
        java.nio.ByteBuffer v0_4 = v1_5;
        return v0_4;
    }

    private void a(java.nio.ByteBuffer p7)
    {
        if (p7 == null) {
            android.util.Log.i("UpdateServerData", "Failed to download new update info");
            this.a.c = 0;
            this.a.d.o();
        } else {
            this.a.c = com.teamspeak.ts3client.jni.Ts3Jni.b().ts3client_android_parseProtobufDataResult(p7, p7.limit());
            if (!this.a.c) {
                android.util.Log.i("UpdateServerData", "Downloaded corrupt data, skipping...");
                this.a.c = 0;
                this.a.d.o();
            } else {
                this.a.a = com.teamspeak.ts3client.jni.Ts3Jni.b().ts3client_android_getLicenseAgreementVersion();
                if (this.a.d != null) {
                    com.teamspeak.ts3client.Ts3Application.a().e.edit().putLong("la_lastcheck", java.util.Calendar.getInstance().getTimeInMillis()).commit();
                    this.a.d.m();
                    android.util.Log.i("UpdateServerData", "Downloaded new update info");
                }
            }
        }
        this.a.e = 0;
        return;
    }

    protected final synthetic Object doInBackground(Object[] p2)
    {
        return com.teamspeak.ts3client.data.ah.a(((String[]) p2));
    }

    protected final synthetic void onPostExecute(Object p7)
    {
        if (((java.nio.ByteBuffer) p7) == null) {
            android.util.Log.i("UpdateServerData", "Failed to download new update info");
            this.a.c = 0;
            this.a.d.o();
        } else {
            this.a.c = com.teamspeak.ts3client.jni.Ts3Jni.b().ts3client_android_parseProtobufDataResult(((java.nio.ByteBuffer) p7), ((java.nio.ByteBuffer) p7).limit());
            if (!this.a.c) {
                android.util.Log.i("UpdateServerData", "Downloaded corrupt data, skipping...");
                this.a.c = 0;
                this.a.d.o();
            } else {
                this.a.a = com.teamspeak.ts3client.jni.Ts3Jni.b().ts3client_android_getLicenseAgreementVersion();
                if (this.a.d != null) {
                    com.teamspeak.ts3client.Ts3Application.a().e.edit().putLong("la_lastcheck", java.util.Calendar.getInstance().getTimeInMillis()).commit();
                    this.a.d.m();
                    android.util.Log.i("UpdateServerData", "Downloaded new update info");
                }
            }
        }
        this.a.e = 0;
        return;
    }
}
