package com.teamspeak.ts3client.data.d;
public final class f {
    public android.support.v4.n.j a;
    public com.teamspeak.ts3client.Ts3Application b;

    public f(com.teamspeak.ts3client.Ts3Application p1)
    {
        this.b = p1;
        return;
    }

    private void a(java.io.File p13)
    {
        if ((p13.exists()) && (p13.isDirectory())) {
            java.io.File[] v2 = p13.listFiles();
            int v3 = v2.length;
            int v0_2 = 0;
            while (v0_2 < v3) {
                android.graphics.Bitmap v4_0 = v2[v0_2];
                if ((v4_0.isFile()) && (v4_0.getName().endsWith(".png"))) {
                    String v5_4 = v4_0.getName().substring(0, v4_0.getName().lastIndexOf(46));
                    android.graphics.Bitmap v4_2 = android.graphics.BitmapFactory.decodeFile(v4_0.getAbsolutePath());
                    if (v4_2 != null) {
                        android.graphics.Bitmap v6_4 = android.graphics.Bitmap.createBitmap(16, 16, android.graphics.Bitmap$Config.ARGB_8888);
                        android.content.res.Resources v7_2 = new android.graphics.Canvas(v6_4);
                        v7_2.drawBitmap(v4_2, ((float) (8 - (v4_2.getWidth() / 2))), ((float) (8 - (v4_2.getHeight() / 2))), 0);
                        v7_2.save();
                        this.a(v5_4, new android.graphics.drawable.BitmapDrawable(this.b.getResources(), android.graphics.Bitmap.createScaledBitmap(v6_4, this.c(), this.c(), 1)).getBitmap());
                    }
                }
                v0_2++;
            }
        }
        return;
    }

    private void a(String p2, android.graphics.Bitmap p3)
    {
        if (this.a(p2) == null) {
            this.a.a(p2, p3);
        }
        return;
    }

    private void b()
    {
        int v1 = 0;
        this.a = new com.teamspeak.ts3client.data.d.g(this, ((((android.app.ActivityManager) this.b.getSystemService("activity")).getMemoryClass() * 1048576) / 10));
        android.content.res.AssetManager v2_4 = this.b.getAssets();
        try {
            String[] v3 = v2_4.list("countries");
            int v4 = v3.length;
        } catch (int v0) {
            return;
        }
        while (v1 < v4) {
            int v0_8 = v3[v1];
            try {
                android.graphics.Bitmap v5_5 = android.graphics.BitmapFactory.decodeStream(v2_4.open(new StringBuilder("countries/").append(v0_8).toString()));
                android.graphics.Bitmap v6_2 = android.graphics.Bitmap.createBitmap(16, 16, android.graphics.Bitmap$Config.ARGB_8888);
                android.content.res.Resources v7_2 = new android.graphics.Canvas(v6_2);
                v7_2.drawBitmap(v5_5, ((float) (8 - (v5_5.getWidth() / 2))), ((float) (8 - (v5_5.getHeight() / 2))), 0);
                v7_2.save();
                this.a(v0_8.substring(0, v0_8.lastIndexOf(46)), new android.graphics.drawable.BitmapDrawable(this.b.getResources(), android.graphics.Bitmap.createScaledBitmap(v6_2, this.c(), this.c(), 1)).getBitmap());
            } catch (int v0_10) {
                v0_10.printStackTrace();
            } catch (int v0_11) {
                throw v0_11;
            }
            v1++;
        }
        return;
    }

    private int c()
    {
        return ((int) ((this.b.getResources().getDisplayMetrics().density * 1098907648) + 1056964608));
    }

    public final android.graphics.Bitmap a(String p2)
    {
        return ((android.graphics.Bitmap) this.a.a(p2));
    }

    public final void a()
    {
        android.content.res.AssetManager v2 = this.b.getAssets();
        try {
            String[] v3 = v2.list("countries");
            int v4 = v3.length;
            int v1_2 = 0;
        } catch (int v0) {
            return;
        }
        while (v1_2 < v4) {
            int v0_1 = v3[v1_2];
            try {
                android.graphics.Bitmap v5_5 = android.graphics.BitmapFactory.decodeStream(v2.open(new StringBuilder("countries/").append(v0_1).toString()));
                android.graphics.Bitmap v6_2 = android.graphics.Bitmap.createBitmap(16, 16, android.graphics.Bitmap$Config.ARGB_8888);
                android.content.res.Resources v7_2 = new android.graphics.Canvas(v6_2);
                v7_2.drawBitmap(v5_5, ((float) (8 - (v5_5.getWidth() / 2))), ((float) (8 - (v5_5.getHeight() / 2))), 0);
                v7_2.save();
                this.a(v0_1.substring(0, v0_1.lastIndexOf(46)), new android.graphics.drawable.BitmapDrawable(this.b.getResources(), android.graphics.Bitmap.createScaledBitmap(v6_2, this.c(), this.c(), 1)).getBitmap());
            } catch (int v0_3) {
                v0_3.printStackTrace();
            } catch (int v0_4) {
                throw v0_4;
            }
            v1_2++;
        }
        return;
    }
}
