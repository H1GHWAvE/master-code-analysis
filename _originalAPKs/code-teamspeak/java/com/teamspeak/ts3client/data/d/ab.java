package com.teamspeak.ts3client.data.d;
public final class ab {
    public int a;
    public int b;

    public ab()
    {
        this.a = -1;
        this.b = -1;
        return;
    }

    public ab(int p2, int p3)
    {
        this.a = -1;
        this.b = -1;
        this.a = p2;
        this.b = p3;
        return;
    }

    private int a()
    {
        return this.a;
    }

    private void a(int p1)
    {
        this.a = p1;
        return;
    }

    private int b()
    {
        return this.b;
    }

    private void b(int p1)
    {
        this.b = p1;
        return;
    }

    public final String toString()
    {
        return new StringBuilder("CodecSettings [m_Codec=").append(this.a).append(", m_Quality=").append(this.b).append("]").toString();
    }
}
