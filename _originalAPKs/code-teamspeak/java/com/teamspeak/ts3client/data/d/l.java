package com.teamspeak.ts3client.data.d;
public final class l extends android.os.AsyncTask {
    private static final long b = 1048576;
    final synthetic com.teamspeak.ts3client.data.d.j a;
    private int c;
    private String d;
    private int e;
    private String f;
    private int g;

    public l(com.teamspeak.ts3client.data.d.j p2)
    {
        this.a = p2;
        this.d = "ContentUPDATER";
        this.e = 1;
        this.g = 100;
        return;
    }

    private varargs Boolean a()
    {
        try {
            byte[] v0_1 = new java.io.File(new StringBuilder().append(android.os.Environment.getExternalStorageDirectory()).append("/TS3/update/").toString());
        } catch (byte[] v0_26) {
            this.a.e.d.log(java.util.logging.Level.SEVERE, v0_26.toString());
            byte[] v0_28 = Boolean.valueOf(0);
            return v0_28;
        } catch (byte[] v0_29) {
            this.a.e.d.log(java.util.logging.Level.SEVERE, v0_29.toString());
            v0_28 = Boolean.valueOf(0);
            return v0_28;
        }
        if (!v0_1.exists()) {
            v0_1.mkdirs();
        }
        byte[] v0_3 = new java.net.URL(this.a.b);
        int v1_8 = v0_3.openConnection();
        v1_8.connect();
        this.c = v1_8.getContentLength();
        if (this.c <= 0) {
            this.c = this.a.h;
        }
        java.io.File v3_3 = new java.io.BufferedInputStream(v0_3.openStream());
        java.util.logging.Level v4_1 = new java.io.FileOutputStream(new StringBuilder().append(android.os.Environment.getExternalStorageDirectory()).append("/TS3/update/").append(this.a.d).append("_").append(this.a.c).append(".zip").toString());
        java.util.Enumeration v5_0 = new byte[1024];
        byte[] v0_15 = 0;
        while(true) {
            java.io.BufferedOutputStream v6_0 = v3_3.read(v5_0);
            if (v6_0 == -1) {
                break;
            }
            v0_15 += ((long) v6_0);
            java.io.BufferedInputStream v7_10 = new Integer[1];
            v7_10[0] = Integer.valueOf(((int) v0_15));
            this.publishProgress(v7_10);
            v4_1.write(v5_0, 0, v6_0);
        }
        v4_1.flush();
        v4_1.close();
        v3_3.close();
        java.io.File v3_5 = new java.io.File(new StringBuilder().append(android.os.Environment.getExternalStorageDirectory()).append("/TS3/update/").append(this.a.d).append("_").append(this.a.c).append(".zip").toString());
        try {
            this.e = 2;
            java.util.logging.Level v4_3 = new java.util.zip.ZipFile(v3_5);
            this.g = v4_3.size();
        } catch (byte[] v0_43) {
            this.a.e.d.log(java.util.logging.Level.SEVERE, v0_43.toString());
            v3_5.delete();
            v0_28 = Boolean.valueOf(0);
            return v0_28;
        }
        if (!this.a.a.exists()) {
            this.a.a.mkdirs();
        }
        java.util.Enumeration v5_1 = v4_3.entries();
        int v1_35 = 0;
        while (v5_1.hasMoreElements()) {
            byte[] v0_39 = ((java.util.zip.ZipEntry) v5_1.nextElement());
            java.io.BufferedOutputStream v6_2 = new java.io.File(this.a.a, v0_39.getName());
            v6_2.getParentFile().mkdirs();
            v1_35++;
            if (!v0_39.isDirectory()) {
                this.f = v6_2.getName();
                java.io.BufferedInputStream v7_6 = new Integer[1];
                v7_6[0] = Integer.valueOf(v1_35);
                this.publishProgress(v7_6);
                java.io.BufferedInputStream v7_8 = new java.io.BufferedInputStream(v4_3.getInputStream(v0_39));
                byte[] v0_42 = new byte[1024];
                java.io.BufferedOutputStream v6_4 = new java.io.BufferedOutputStream(new java.io.FileOutputStream(v6_2), 1024);
                while(true) {
                    int v8_5 = v7_8.read(v0_42, 0, 1024);
                    if (v8_5 == -1) {
                        break;
                    }
                    v6_4.write(v0_42, 0, v8_5);
                }
                v6_4.flush();
                v6_4.close();
                v7_8.close();
            }
        }
        v4_3.close();
        v3_5.delete();
        v0_28 = Boolean.valueOf(1);
        return v0_28;
    }

    private void a(Boolean p4)
    {
        super.onPostExecute(p4);
        if (p4.booleanValue()) {
            this.a.f.dismiss();
            this.a.e.e.edit().putInt(this.a.d, this.a.c).commit();
        } else {
            this.a.f.setMessage(com.teamspeak.ts3client.data.e.a.a("contentdownloader.error"));
            this.a.f.setCancelable(1);
        }
        return;
    }

    private varargs void a(Integer[] p13)
    {
        this.a.f.setMax(this.g);
        this.a.f.setProgress(p13[0].intValue());
        if (p13[0].intValue() != -1) {
            android.app.ProgressDialog v0_7 = this.a.f;
            String v1_5 = new StringBuilder("Downloaded ");
            String v3_0 = new Object[2];
            v3_0[0] = Double.valueOf((((double) p13[0].intValue()) / 1000000.0));
            v3_0[1] = "MB";
            String v1_7 = v1_5.append(String.format("%.2f %s", v3_0)).append(" of ");
            String v3_1 = new Object[2];
            v3_1[0] = Double.valueOf((((double) this.c) / 1000000.0));
            v3_1[1] = "MB";
            v0_7.setMessage(v1_7.append(String.format("%.2f %s", v3_1)).toString());
        } else {
            this.a.f.setMessage("Download Failed\nPlease try again.");
            this.a.f.setCancelable(1);
        }
        if (this.e == 2) {
            android.app.ProgressDialog v0_14 = this.a.f;
            Object[] v2_6 = new Object[1];
            v2_6[0] = this.f;
            v0_14.setMessage(com.teamspeak.ts3client.data.e.a.a("contentdownloader.extracting", v2_6));
        }
        return;
    }

    protected final synthetic Object doInBackground(Object[] p2)
    {
        return this.a();
    }

    protected final synthetic void onPostExecute(Object p4)
    {
        super.onPostExecute(((Boolean) p4));
        if (((Boolean) p4).booleanValue()) {
            this.a.f.dismiss();
            this.a.e.e.edit().putInt(this.a.d, this.a.c).commit();
        } else {
            this.a.f.setMessage(com.teamspeak.ts3client.data.e.a.a("contentdownloader.error"));
            this.a.f.setCancelable(1);
        }
        return;
    }

    protected final void onPreExecute()
    {
        this.a.f.setTitle(new StringBuilder("Update ").append(this.a.g).toString());
        this.a.f.setIcon(2130837622);
        this.a.f.setMax(100);
        this.a.f.setProgressStyle(0);
        this.a.f.setMessage(com.teamspeak.ts3client.data.e.a.a("contentdownloader.info"));
        this.a.f.setCancelable(0);
        this.a.f.setOnDismissListener(new com.teamspeak.ts3client.data.d.m(this));
        this.a.f.show();
        return;
    }

    protected final synthetic void onProgressUpdate(Object[] p13)
    {
        this.a.f.setMax(this.g);
        this.a.f.setProgress(((Integer[]) p13)[0].intValue());
        if (((Integer[]) p13)[0].intValue() != -1) {
            android.app.ProgressDialog v0_7 = this.a.f;
            String v1_5 = new StringBuilder("Downloaded ");
            String v3_0 = new Object[2];
            v3_0[0] = Double.valueOf((((double) ((Integer[]) p13)[0].intValue()) / 1000000.0));
            v3_0[1] = "MB";
            String v1_7 = v1_5.append(String.format("%.2f %s", v3_0)).append(" of ");
            String v3_1 = new Object[2];
            v3_1[0] = Double.valueOf((((double) this.c) / 1000000.0));
            v3_1[1] = "MB";
            v0_7.setMessage(v1_7.append(String.format("%.2f %s", v3_1)).toString());
        } else {
            this.a.f.setMessage("Download Failed\nPlease try again.");
            this.a.f.setCancelable(1);
        }
        if (this.e == 2) {
            android.app.ProgressDialog v0_14 = this.a.f;
            Object[] v2_6 = new Object[1];
            v2_6[0] = this.f;
            v0_14.setMessage(com.teamspeak.ts3client.data.e.a.a("contentdownloader.extracting", v2_6));
        }
        return;
    }
}
