package com.teamspeak.ts3client.data.d;
public final class v implements com.teamspeak.ts3client.data.w {
    java.util.HashMap a;
    java.util.HashMap b;
    public boolean c;
    private String d;
    private com.teamspeak.ts3client.Ts3Application e;
    private String f;
    private String g;

    public v(com.teamspeak.ts3client.Ts3Application p5)
    {
        this.d = new StringBuilder().append(android.os.Environment.getExternalStorageDirectory()).append("/TS3/cache/").toString();
        this.c = 0;
        this.e = p5;
        if (!this.a()) {
            this.a = new java.util.HashMap();
            this.b = new java.util.HashMap();
            p5.a.c.a(this);
            p5.a.a.ts3client_requestPermissionList(p5.a.e, "request ServerPermissionList");
        }
        return;
    }

    private String a(int p3)
    {
        String v0_2;
        if (!this.a.containsKey(Integer.valueOf(p3))) {
            v0_2 = "";
        } else {
            v0_2 = ((String) this.a.get(Integer.valueOf(p3)));
        }
        return v0_2;
    }

    private boolean a()
    {
        java.util.logging.Level v0_29;
        this.f = this.e.a.q;
        String v3_3 = this.e.a.a.b(this.e.a.e, com.teamspeak.ts3client.jni.i.e);
        if ((!this.f.isEmpty()) && (!v3_3.isEmpty())) {
            java.util.logging.Level v0_14 = new StringBuilder().append(this.d).append(this.e.a.q.replace("/", "")).append("/").toString();
            java.io.ObjectInputStream v4_8 = new java.io.File(v0_14);
            if (!v4_8.exists()) {
                v4_8.mkdirs();
            }
            java.io.ObjectInputStream v4_10 = new java.io.File(new StringBuilder().append(v0_14).append("perm.dat").toString());
            if (v4_10.exists()) {
                try {
                    java.io.FileInputStream v5_6 = new java.io.FileInputStream(v4_10);
                    java.io.ObjectInputStream v4_12 = new java.io.ObjectInputStream(v5_6);
                    this.g = ((String) v4_12.readObject());
                } catch (java.util.logging.Level v0) {
                    this.e.d.log(java.util.logging.Level.WARNING, "Failed to read new perm.dat, aborting load permissions");
                    v0_29 = 0;
                } catch (java.util.logging.Level v0) {
                    this.e.d.log(java.util.logging.Level.WARNING, "Failed to read new perm.dat, aborting load permissions");
                    v0_29 = 0;
                } catch (java.util.logging.Level v0) {
                    this.e.d.log(java.util.logging.Level.WARNING, "Failed to read new perm.dat, aborting load permissions");
                    v0_29 = 0;
                } catch (java.util.logging.Level v0) {
                    this.e.d.log(java.util.logging.Level.WARNING, "Failed to read new perm.dat, aborting load permissions");
                    v0_29 = 0;
                }
                if (v3_3.equals(this.g)) {
                    this.a = ((java.util.HashMap) v4_12.readObject());
                    this.b = ((java.util.HashMap) v4_12.readObject());
                    v4_12.close();
                    v5_6.close();
                    this.e.d.log(java.util.logging.Level.INFO, "Loaded Permissions from perm.dat");
                    this.c = 1;
                    v0_29 = 1;
                } else {
                    this.e.d.log(java.util.logging.Level.WARNING, "Found new ServerVersion, aborting load permissions");
                    v4_12.close();
                    v5_6.close();
                    v0_29 = 0;
                }
            } else {
                v0_29 = 0;
            }
        } else {
            this.e.d.log(java.util.logging.Level.WARNING, "Don\'t have a unique server id or version, aborting load permissions");
            v0_29 = 0;
        }
        return v0_29;
    }

    private void b()
    {
        this.f = this.e.a.q;
        this.g = this.e.a.a.b(this.e.a.e, com.teamspeak.ts3client.jni.i.e);
        if ((!this.a.isEmpty()) && (!this.b.isEmpty())) {
            if ((!this.f.isEmpty()) && (!this.g.isEmpty())) {
                java.util.logging.Logger v0_20 = new StringBuilder().append(this.d).append(this.e.a.q.replace("/", "")).append("/").toString();
                java.util.logging.Level v1_10 = new java.io.File(v0_20);
                if (!v1_10.exists()) {
                    v1_10.mkdirs();
                }
                try {
                    java.util.logging.Logger v0_25 = new java.io.FileOutputStream(new java.io.File(new StringBuilder().append(v0_20).append("perm.dat").toString()));
                    java.util.logging.Level v1_14 = new java.io.ObjectOutputStream(v0_25);
                    v1_14.writeObject(this.g);
                    v1_14.writeObject(this.a);
                    v1_14.writeObject(this.b);
                    v1_14.close();
                    v0_25.close();
                } catch (java.util.logging.Logger v0) {
                    this.e.d.log(java.util.logging.Level.WARNING, "Failed to write new perm.dat, aborting save permissions");
                } catch (java.util.logging.Logger v0) {
                    this.e.d.log(java.util.logging.Level.WARNING, "Failed to write new perm.dat, aborting save permissions");
                }
            } else {
                this.e.d.log(java.util.logging.Level.WARNING, "Don\'t have a unique server id or version, aborting load permissions");
            }
        }
        return;
    }

    private boolean c()
    {
        return this.c;
    }

    public final int a(com.teamspeak.ts3client.jni.g p3)
    {
        int v0_2;
        if (!this.b.containsKey(p3.iy)) {
            v0_2 = -1;
        } else {
            v0_2 = ((Integer) this.b.get(p3.iy)).intValue();
        }
        return v0_2;
    }

    public final int a(String p2)
    {
        int v0_2;
        if (!this.b.containsKey(p2)) {
            v0_2 = -1;
        } else {
            v0_2 = ((Integer) this.b.get(p2)).intValue();
        }
        return v0_2;
    }

    public final void a(com.teamspeak.ts3client.jni.k p5)
    {
        if ((p5 instanceof com.teamspeak.ts3client.jni.events.rare.PermissionList)) {
            this.a.put(Integer.valueOf(((com.teamspeak.ts3client.jni.events.rare.PermissionList) p5).a), ((com.teamspeak.ts3client.jni.events.rare.PermissionList) p5).b);
            this.b.put(((com.teamspeak.ts3client.jni.events.rare.PermissionList) p5).b, Integer.valueOf(((com.teamspeak.ts3client.jni.events.rare.PermissionList) p5).a));
        }
        if ((p5 instanceof com.teamspeak.ts3client.jni.events.rare.PermissionListFinished)) {
            this.e.a.c.b(this);
            this.f = this.e.a.q;
            this.g = this.e.a.a.b(this.e.a.e, com.teamspeak.ts3client.jni.i.e);
            if ((!this.a.isEmpty()) && (!this.b.isEmpty())) {
                if ((!this.f.isEmpty()) && (!this.g.isEmpty())) {
                    java.util.logging.Logger v0_29 = new StringBuilder().append(this.d).append(this.e.a.q.replace("/", "")).append("/").toString();
                    java.util.logging.Level v1_12 = new java.io.File(v0_29);
                    if (!v1_12.exists()) {
                        v1_12.mkdirs();
                    }
                    try {
                        java.util.logging.Logger v0_34 = new java.io.FileOutputStream(new java.io.File(new StringBuilder().append(v0_29).append("perm.dat").toString()));
                        java.util.logging.Level v1_16 = new java.io.ObjectOutputStream(v0_34);
                        v1_16.writeObject(this.g);
                        v1_16.writeObject(this.a);
                        v1_16.writeObject(this.b);
                        v1_16.close();
                        v0_34.close();
                    } catch (java.util.logging.Logger v0) {
                        this.e.d.log(java.util.logging.Level.WARNING, "Failed to write new perm.dat, aborting save permissions");
                    } catch (java.util.logging.Logger v0) {
                        this.e.d.log(java.util.logging.Level.WARNING, "Failed to write new perm.dat, aborting save permissions");
                    }
                } else {
                    this.e.d.log(java.util.logging.Level.WARNING, "Don\'t have a unique server id or version, aborting load permissions");
                }
            }
            this.c = 1;
        }
        return;
    }
}
