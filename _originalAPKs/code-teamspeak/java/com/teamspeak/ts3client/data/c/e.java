package com.teamspeak.ts3client.data.c;
public final class e implements com.teamspeak.ts3client.data.w {
    private static java.util.regex.Pattern c;
    java.util.concurrent.CountDownLatch a;
    android.graphics.drawable.Drawable b;
    private int d;
    private java.io.File e;
    private boolean f;
    private boolean g;
    private String h;
    private String i;

    static e()
    {
        com.teamspeak.ts3client.data.c.e.c = java.util.regex.Pattern.compile("ts3image://(.*)\\?channel=(\\d*)(?:&|&amp;)path=(.*)");
        return;
    }

    public e()
    {
        this.a = 0;
        this.b = 0;
        this.f = 0;
        this.g = 0;
        this.h = "";
        this.i = "";
        return;
    }

    public final android.graphics.drawable.Drawable a(String p13, java.io.File p14)
    {
        android.graphics.drawable.Drawable v0_37;
        int v9_0 = com.teamspeak.ts3client.data.c.e.c.matcher(p13);
        if (!v9_0.find()) {
            this.b = com.teamspeak.ts3client.Ts3Application.a().getResources().getDrawable(2130837580);
            this.b.setBounds(0, 0, (this.b.getIntrinsicWidth() + 0), (this.b.getIntrinsicHeight() + 0));
            v0_37 = this.b;
        } else {
            android.graphics.drawable.Drawable v0_5 = Long.valueOf(Long.parseLong(v9_0.group(2)));
            if (v9_0.group(3).length() <= 1) {
                this.i = new StringBuilder().append(v9_0.group(3)).append(v9_0.group(1)).toString();
            } else {
                this.i = new StringBuilder().append(v9_0.group(3)).append("/").append(v9_0.group(1)).toString();
            }
            String v6 = "";
            if (com.teamspeak.ts3client.Ts3Application.a().a.j.containsKey(v0_5)) {
                v6 = ((String) com.teamspeak.ts3client.Ts3Application.a().a.j.get(v0_5));
            }
            this.e = new java.io.File(new StringBuilder().append(p14).append("/").append(v9_0.group(1)).toString());
            if ((this.e.exists()) && (this.e.isFile())) {
                try {
                    this.b = android.graphics.drawable.Drawable.createFromPath(this.e.getAbsolutePath());
                    this.b.setBounds(0, 0, (this.b.getIntrinsicWidth() + 0), (this.b.getIntrinsicHeight() + 0));
                    com.teamspeak.ts3client.Ts3Application.a().a.c.a(this);
                    this.a = new java.util.concurrent.CountDownLatch(1);
                    this.h = this.i;
                    com.teamspeak.ts3client.Ts3Application.a().a.a.ts3client_requestFileInfo(com.teamspeak.ts3client.Ts3Application.a().a.e, Long.parseLong(v9_0.group(2)), v6, this.i, this.h);
                    try {
                        this.a.await();
                    } catch (android.graphics.drawable.Drawable v0) {
                        com.teamspeak.ts3client.Ts3Application.a().d.log(java.util.logging.Level.WARNING, new StringBuilder("Failed to load Image1: ").append(p13).toString());
                        this.b = com.teamspeak.ts3client.Ts3Application.a().getResources().getDrawable(2130837580);
                        this.b.setBounds(0, 0, (this.b.getIntrinsicWidth() + 0), (this.b.getIntrinsicHeight() + 0));
                        v0_37 = this.b;
                        return v0_37;
                    }
                    if (!this.f) {
                        if (this.g) {
                            this.b = com.teamspeak.ts3client.Ts3Application.a().getResources().getDrawable(2130837580);
                            this.b.setBounds(0, 0, (this.b.getIntrinsicWidth() + 0), (this.b.getIntrinsicHeight() + 0));
                            v0_37 = this.b;
                            return v0_37;
                        }
                    } else {
                        v0_37 = this.b;
                        return v0_37;
                    }
                } catch (android.graphics.drawable.Drawable v0) {
                    com.teamspeak.ts3client.Ts3Application.a().d.log(java.util.logging.Level.WARNING, new StringBuilder("Failed to load Image2: ").append(p13).toString());
                }
            }
            com.teamspeak.ts3client.Ts3Application.a().a.c.a(this);
            this.a = new java.util.concurrent.CountDownLatch(1);
            this.d = com.teamspeak.ts3client.Ts3Application.a().a.a.ts3client_requestFile(com.teamspeak.ts3client.Ts3Application.a().a.e, Long.parseLong(v9_0.group(2)), v6, this.i, 1, 0, p14.getAbsolutePath(), new StringBuilder("loadImage:").append(this.i).toString());
            try {
                this.a.await();
            } catch (android.graphics.drawable.Drawable v0) {
                com.teamspeak.ts3client.Ts3Application.a().d.log(java.util.logging.Level.WARNING, new StringBuilder("Failed to load Image3: ").append(p13).toString());
                this.b = com.teamspeak.ts3client.Ts3Application.a().getResources().getDrawable(2130837580);
                this.b.setBounds(0, 0, (this.b.getIntrinsicWidth() + 0), (this.b.getIntrinsicHeight() + 0));
                v0_37 = this.b;
            }
            if ((!this.e.exists()) || (!this.e.isFile())) {
            } else {
                try {
                    this.b = android.graphics.drawable.Drawable.createFromPath(this.e.getAbsolutePath());
                    this.b.setBounds(0, 0, (this.b.getIntrinsicWidth() + 0), (this.b.getIntrinsicHeight() + 0));
                    v0_37 = this.b;
                } catch (android.graphics.drawable.Drawable v0) {
                    com.teamspeak.ts3client.Ts3Application.a().d.log(java.util.logging.Level.WARNING, new StringBuilder("Failed to load Image4: ").append(p13).toString());
                    this.b = com.teamspeak.ts3client.Ts3Application.a().getResources().getDrawable(2130837580);
                    this.b.setBounds(0, 0, (this.b.getIntrinsicWidth() + 0), (this.b.getIntrinsicHeight() + 0));
                    v0_37 = this.b;
                }
            }
        }
        return v0_37;
    }

    public final void a(com.teamspeak.ts3client.jni.k p6)
    {
        if (((p6 instanceof com.teamspeak.ts3client.jni.events.rare.FileTransferStatus)) && (((com.teamspeak.ts3client.jni.events.rare.FileTransferStatus) p6).a == this.d)) {
            this.a.countDown();
            com.teamspeak.ts3client.Ts3Application.a().a.c.b(this);
        }
        if (((p6 instanceof com.teamspeak.ts3client.jni.events.rare.FileInfo)) && (((com.teamspeak.ts3client.jni.events.rare.FileInfo) p6).a.equals(this.h))) {
            if (((com.teamspeak.ts3client.jni.events.rare.FileInfo) p6).b != this.e.length()) {
                this.a.countDown();
                com.teamspeak.ts3client.Ts3Application.a().a.c.b(this);
            } else {
                this.a.countDown();
                this.f = 1;
                com.teamspeak.ts3client.Ts3Application.a().a.c.b(this);
            }
        }
        if (((p6 instanceof com.teamspeak.ts3client.jni.events.ServerError)) && (((com.teamspeak.ts3client.jni.events.ServerError) p6).c.equals(this.h))) {
            if ((((com.teamspeak.ts3client.jni.events.ServerError) p6).b == 2051) && (((com.teamspeak.ts3client.jni.events.ServerError) p6).c.equals(this.h))) {
                this.g = 1;
                this.a.countDown();
            }
            if ((((com.teamspeak.ts3client.jni.events.ServerError) p6).b == 2054) && (((com.teamspeak.ts3client.jni.events.ServerError) p6).c.equals(this.h))) {
                this.g = 1;
                this.a.countDown();
            }
            if ((((com.teamspeak.ts3client.jni.events.ServerError) p6).b == 781) && (((com.teamspeak.ts3client.jni.events.ServerError) p6).c.equals(this.h))) {
                this.g = 1;
                this.a.countDown();
            }
        }
        return;
    }
}
