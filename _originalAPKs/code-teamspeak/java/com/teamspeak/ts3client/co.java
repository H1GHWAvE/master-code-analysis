package com.teamspeak.ts3client;
final class co implements android.content.SharedPreferences$OnSharedPreferenceChangeListener {
    private com.teamspeak.ts3client.Ts3Application a;

    public co(com.teamspeak.ts3client.Ts3Application p1)
    {
        this.a = p1;
        return;
    }

    public final void onSharedPreferenceChanged(android.content.SharedPreferences p6, String p7)
    {
        if (p7.equals("screen_rotation")) {
            switch (p6.getInt("screen_rotation", 0)) {
                case 0:
                    this.a.f.setRequestedOrientation(-1);
                    break;
                case 1:
                    this.a.f.setRequestedOrientation(1);
                    break;
                case 2:
                    this.a.f.setRequestedOrientation(0);
                    break;
                default:
                    this.a.f.setRequestedOrientation(-1);
            }
        }
        if (p7.equals("android_overlay_setting")) {
            if (!p6.getBoolean("android_overlay_setting", 0)) {
                this.a.e.edit().putBoolean("android_overlay", 0).commit();
                this.a.e.edit().putBoolean("android_overlay_setting", 0).commit();
            } else {
                if (android.os.Build$VERSION.SDK_INT >= 23) {
                    if (android.provider.Settings.canDrawOverlays(this.a)) {
                        this.a.e.edit().putBoolean("android_overlay", 1).commit();
                    } else {
                        this.a.f.startActivityForResult(new android.content.Intent("android.settings.action.MANAGE_OVERLAY_PERMISSION", android.net.Uri.parse(new StringBuilder("package:").append(this.a.getPackageName()).toString())), 7474);
                    }
                } else {
                    this.a.e.edit().putBoolean("android_overlay", 1).commit();
                }
            }
        }
        return;
    }
}
