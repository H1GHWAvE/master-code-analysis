package com.teamspeak.ts3client.tsdns;
public final class f implements com.teamspeak.ts3client.tsdns.k {
    public static int a;
    public static int b;
    public static int c;
    public static int d;
    public static int e;
    public static int f;
    public static int g;
    public static int h;
    private boolean i;
    private String j;
    private com.teamspeak.ts3client.tsdns.i k;
    private int l;
    private java.util.ArrayList m;
    private java.util.BitSet n;
    private java.util.Vector o;
    private com.teamspeak.ts3client.tsdns.h p;
    private boolean q;
    private long r;
    private java.util.logging.Logger s;

    static f()
    {
        com.teamspeak.ts3client.tsdns.f.a = 0;
        com.teamspeak.ts3client.tsdns.f.b = 1;
        com.teamspeak.ts3client.tsdns.f.c = 2;
        com.teamspeak.ts3client.tsdns.f.d = 3;
        com.teamspeak.ts3client.tsdns.f.e = 0;
        com.teamspeak.ts3client.tsdns.f.f = 1;
        com.teamspeak.ts3client.tsdns.f.g = 2;
        com.teamspeak.ts3client.tsdns.f.h = 3;
        return;
    }

    public f(String p3, int p4, com.teamspeak.ts3client.tsdns.i p5, boolean p6, java.util.logging.Logger p7)
    {
        this.i = 0;
        this.j = "";
        this.m = new java.util.ArrayList();
        this.n = new java.util.BitSet();
        this.o = new java.util.Vector();
        this.r = 3500;
        this.j = p3.toLowerCase();
        this.l = p4;
        this.k = p5;
        this.s = p7;
        this.q = p6;
        if (p6) {
            this.r = 7000;
        }
        return;
    }

    static synthetic long a(com.teamspeak.ts3client.tsdns.f p2)
    {
        return p2.r;
    }

    static synthetic void a(com.teamspeak.ts3client.tsdns.f p0, com.teamspeak.ts3client.tsdns.h p1)
    {
        p0.b(p1);
        return;
    }

    private static void a(java.util.ArrayList p3)
    {
        java.util.Iterator v1 = p3.iterator();
        while (v1.hasNext()) {
            System.out.println(((String) v1.next()));
        }
        return;
    }

    private void b(com.teamspeak.ts3client.tsdns.h p5)
    {
        this.s.log(java.util.logging.Level.INFO, new StringBuilder("PUBLISH:").append(p5.d).append(" - ").append(p5.b).append(" - ").append(p5.a).append(" - ").append(p5.c).toString());
        this.k.a(p5);
        return;
    }

    static synthetic boolean b(com.teamspeak.ts3client.tsdns.f p1)
    {
        return p1.i;
    }

    static synthetic boolean c(com.teamspeak.ts3client.tsdns.f p1)
    {
        p1.i = 1;
        return 1;
    }

    static synthetic com.teamspeak.ts3client.tsdns.h d(com.teamspeak.ts3client.tsdns.f p1)
    {
        return p1.p;
    }

    static synthetic String e(com.teamspeak.ts3client.tsdns.f p1)
    {
        return p1.j;
    }

    static synthetic int f(com.teamspeak.ts3client.tsdns.f p1)
    {
        return p1.l;
    }

    public final void a()
    {
        if ((!this.j.contains(":")) && (!org.xbill.DNS.Address.isDottedQuad(this.j))) {
            String v1_4;
            this.m.add(this.j);
            int v3_2 = new java.util.ArrayList(java.util.Arrays.asList(this.j.split("\\.")));
            com.teamspeak.ts3client.tsdns.j v0_10 = ((v3_2.size() - com.teamspeak.ts3client.tsdns.f.d) - 1);
            if (v0_10 >= null) {
                v1_4 = v0_10;
            } else {
                v1_4 = 0;
            }
            while (v1_4 < (v3_2.size() - 1)) {
                int v4_3 = new StringBuilder();
                String v2_9 = v1_4;
                while (v2_9 < v3_2.size()) {
                    v4_3.append(((String) v3_2.get(v2_9)));
                    if (v2_9 < (v3_2.size() - 1)) {
                        v4_3.append(".");
                    }
                    v2_9++;
                }
                this.m.add(v4_3.toString());
                v1_4++;
            }
            Thread v13_1 = new Thread(new com.teamspeak.ts3client.tsdns.g(this));
            this.n.set(0);
            this.n.set(((com.teamspeak.ts3client.tsdns.f.d * 2) + 3));
            java.util.Collections.reverse(this.m);
            String v1_8 = this.m.iterator();
            com.teamspeak.ts3client.tsdns.j v0_19 = 0;
            while (v1_8.hasNext()) {
                v1_8.next();
                this.n.set((v0_19 + 1));
                this.n.set(((com.teamspeak.ts3client.tsdns.f.d + 2) + v0_19));
                v0_19++;
            }
            new com.teamspeak.ts3client.tsdns.j(this.j, this.j, this.l, com.teamspeak.ts3client.tsdns.f.h, 0, this).start();
            new com.teamspeak.ts3client.tsdns.j(this.j, this.j, this.l, com.teamspeak.ts3client.tsdns.f.e, ((com.teamspeak.ts3client.tsdns.f.d * 2) + 3), this).start();
            String v1_10 = this.m.iterator();
            com.teamspeak.ts3client.tsdns.j v0_25 = 0;
            while (v1_10.hasNext()) {
                new com.teamspeak.ts3client.tsdns.j(((String) v1_10.next()), this.j, this.l, com.teamspeak.ts3client.tsdns.f.f, ((com.teamspeak.ts3client.tsdns.f.d + 2) + v0_25), this).start();
                v0_25++;
            }
            java.util.Iterator v8_1 = this.m.iterator();
            int v7_1 = 0;
            while (v8_1.hasNext()) {
                new com.teamspeak.ts3client.tsdns.j(this.j, ((String) v8_1.next()), this.l, com.teamspeak.ts3client.tsdns.f.g, (v7_1 + 1), this).start();
                v7_1++;
            }
            v13_1.start();
        } else {
            this.b(new com.teamspeak.ts3client.tsdns.h(this.j, this.l, -1, com.teamspeak.ts3client.tsdns.f.a, -1, 1));
        }
        return;
    }

    public final declared_synchronized void a(com.teamspeak.ts3client.tsdns.h p8)
    {
        try {
            if (!this.i) {
                this.n.clear(p8.b);
                if (p8.d == com.teamspeak.ts3client.tsdns.f.a) {
                    if (this.p != null) {
                        if (this.p.b < p8.b) {
                            this.p = p8;
                        }
                    } else {
                        this.p = p8;
                    }
                }
                if (((this.n.nextSetBit(p8.b) == -1) && (p8.d == com.teamspeak.ts3client.tsdns.f.a)) || (this.n.isEmpty())) {
                    this.i = 1;
                    if (this.p != null) {
                        this.b(new com.teamspeak.ts3client.tsdns.h(this.p));
                    } else {
                        this.b(new com.teamspeak.ts3client.tsdns.h(this.j, this.l, -1, com.teamspeak.ts3client.tsdns.f.c, -1, 1));
                    }
                }
            }
        } catch (com.teamspeak.ts3client.tsdns.h v0_13) {
            throw v0_13;
        }
        return;
    }

    public final void a(Thread p2)
    {
        this.o.add(p2);
        return;
    }

    public final boolean b()
    {
        return this.q;
    }

    public final java.util.logging.Logger c()
    {
        return this.s;
    }
}
