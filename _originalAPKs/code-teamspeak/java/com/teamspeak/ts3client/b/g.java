package com.teamspeak.ts3client.b;
final class g implements android.view.View$OnLongClickListener {
    final synthetic com.teamspeak.ts3client.jni.events.rare.BanList a;
    final synthetic com.teamspeak.ts3client.b.f b;

    g(com.teamspeak.ts3client.b.f p1, com.teamspeak.ts3client.jni.events.rare.BanList p2)
    {
        this.b = p1;
        this.a = p2;
        return;
    }

    public final boolean onLongClick(android.view.View p8)
    {
        android.widget.Button v0_2 = new android.app.AlertDialog$Builder(this.b.getContext()).create();
        long v1_3 = new android.widget.TableLayout(this.b.b.i());
        com.teamspeak.ts3client.b.f.a(this.b, this.a, v1_3);
        v0_2.setView(v1_3);
        com.teamspeak.ts3client.data.d.q.a(v0_2);
        v0_2.setTitle(com.teamspeak.ts3client.data.e.a.a("banlist.dialog.info"));
        v0_2.setButton(-2, com.teamspeak.ts3client.data.e.a.a("button.cancel"), new com.teamspeak.ts3client.b.h(this, v0_2));
        v0_2.setButton(-1, com.teamspeak.ts3client.data.e.a.a("button.delete"), new com.teamspeak.ts3client.b.i(this, v0_2));
        v0_2.show();
        v0_2.setCancelable(1);
        android.widget.Button v0_3 = v0_2.getButton(-1);
        long v1_15 = com.teamspeak.ts3client.b.b.c(this.b.b).a.s.a(com.teamspeak.ts3client.jni.g.dc);
        if (!com.teamspeak.ts3client.b.b.c(this.b.b).a.s.a(com.teamspeak.ts3client.jni.g.dd)) {
            if (v1_15 != 0) {
                if (com.teamspeak.ts3client.b.b.c(this.b.b).a.z != this.a.f) {
                    v0_3.setEnabled(0);
                }
            } else {
                v0_3.setEnabled(0);
            }
        }
        return 0;
    }
}
