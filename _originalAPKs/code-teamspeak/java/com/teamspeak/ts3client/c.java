package com.teamspeak.ts3client;
public final class c extends android.support.v4.app.Fragment implements com.teamspeak.ts3client.data.d.c, com.teamspeak.ts3client.data.w {
    public static com.teamspeak.ts3client.c a;
    public static com.teamspeak.ts3client.data.a b;
    private android.widget.TableLayout at;
    private android.webkit.WebView au;
    private String av;
    private android.widget.RelativeLayout aw;
    private com.teamspeak.ts3client.customs.FloatingButton ax;
    private com.teamspeak.ts3client.Ts3Application c;
    private android.support.v4.app.bi d;
    private android.widget.TextView e;
    private android.widget.TextView f;
    private android.widget.TextView g;
    private android.widget.TextView h;
    private android.widget.TextView i;
    private android.widget.TextView j;
    private android.widget.TextView k;
    private android.widget.TableRow l;
    private android.widget.TableRow m;

    public c()
    {
        return;
    }

    static synthetic android.support.v4.app.bi a(com.teamspeak.ts3client.c p1)
    {
        return p1.d;
    }

    private static com.teamspeak.ts3client.c a(com.teamspeak.ts3client.data.a p1)
    {
        com.teamspeak.ts3client.c v0_3;
        if (com.teamspeak.ts3client.c.b != p1) {
            com.teamspeak.ts3client.c.a = new com.teamspeak.ts3client.c();
            com.teamspeak.ts3client.c.b = p1;
            v0_3 = com.teamspeak.ts3client.c.a;
        } else {
            v0_3 = com.teamspeak.ts3client.c.a;
        }
        return v0_3;
    }

    static synthetic com.teamspeak.ts3client.data.a a()
    {
        return com.teamspeak.ts3client.c.b;
    }

    static synthetic String a(com.teamspeak.ts3client.c p0, String p1)
    {
        p0.av = p1;
        return p1;
    }

    private void a(boolean p3)
    {
        if (this.l()) {
            this.i().runOnUiThread(new com.teamspeak.ts3client.g(this, p3));
        }
        return;
    }

    static synthetic android.webkit.WebView b(com.teamspeak.ts3client.c p1)
    {
        return p1.au;
    }

    static synthetic com.teamspeak.ts3client.c b()
    {
        return com.teamspeak.ts3client.c.a;
    }

    private void b(String p3)
    {
        if (this.l()) {
            this.i().runOnUiThread(new com.teamspeak.ts3client.f(this, p3));
        }
        return;
    }

    static synthetic com.teamspeak.ts3client.Ts3Application c(com.teamspeak.ts3client.c p1)
    {
        return p1.c;
    }

    static synthetic android.widget.TextView d(com.teamspeak.ts3client.c p1)
    {
        return p1.e;
    }

    static synthetic android.widget.TextView e(com.teamspeak.ts3client.c p1)
    {
        return p1.i;
    }

    static synthetic android.widget.TableRow f(com.teamspeak.ts3client.c p1)
    {
        return p1.m;
    }

    static synthetic android.widget.TableLayout g(com.teamspeak.ts3client.c p1)
    {
        return p1.at;
    }

    static synthetic android.widget.TextView h(com.teamspeak.ts3client.c p1)
    {
        return p1.f;
    }

    static synthetic android.widget.TextView i(com.teamspeak.ts3client.c p1)
    {
        return p1.g;
    }

    static synthetic android.widget.TextView j(com.teamspeak.ts3client.c p1)
    {
        return p1.h;
    }

    static synthetic android.widget.TableRow k(com.teamspeak.ts3client.c p1)
    {
        return p1.l;
    }

    static synthetic android.widget.TextView l(com.teamspeak.ts3client.c p1)
    {
        return p1.j;
    }

    static synthetic android.widget.TextView m(com.teamspeak.ts3client.c p1)
    {
        return p1.k;
    }

    static synthetic String n(com.teamspeak.ts3client.c p1)
    {
        return p1.av;
    }

    public final android.view.View a(android.view.LayoutInflater p7, android.view.ViewGroup p8)
    {
        android.view.View v1 = p7.inflate(2130903076, p8, 0);
        this.d = this.M;
        this.aw = ((android.widget.RelativeLayout) v1.findViewById(2131493071));
        this.at = ((android.widget.TableLayout) v1.findViewById(2131493073));
        this.e = ((android.widget.TextView) v1.findViewById(2131493076));
        this.f = ((android.widget.TextView) v1.findViewById(2131493082));
        this.g = ((android.widget.TextView) v1.findViewById(2131493085));
        this.l = ((android.widget.TableRow) v1.findViewById(2131493086));
        this.h = ((android.widget.TextView) v1.findViewById(2131493088));
        this.m = ((android.widget.TableRow) v1.findViewById(2131493077));
        this.i = ((android.widget.TextView) v1.findViewById(2131493079));
        this.j = ((android.widget.TextView) v1.findViewById(2131493091));
        this.k = ((android.widget.TextView) v1.findViewById(2131493094));
        this.ax = ((com.teamspeak.ts3client.customs.FloatingButton) v1.findViewById(2131493097));
        this.ax.setBitmap(com.teamspeak.ts3client.data.d.t.a(2130837621, 1102053376, 1102053376));
        this.au = ((android.webkit.WebView) v1.findViewById(2131493096));
        com.teamspeak.ts3client.data.e.a.a("channelinfo.name", v1, 2131493075);
        com.teamspeak.ts3client.data.e.a.a("channelinfo.topic", v1, 2131493078);
        com.teamspeak.ts3client.data.e.a.a("channelinfo.codec", v1, 2131493081);
        com.teamspeak.ts3client.data.e.a.a("channelinfo.codecquality", v1, 2131493084);
        com.teamspeak.ts3client.data.e.a.a("channelinfo.type", v1, 2131493087);
        com.teamspeak.ts3client.data.e.a.a("channelinfo.currentclients", v1, 2131493090);
        com.teamspeak.ts3client.data.e.a.a("channelinfo.subscription", v1, 2131493093);
        com.teamspeak.ts3client.data.e.a.a("channelinfo.description", v1, 2131493095);
        this.au.setBackgroundColor(0);
        this.au.getSettings().setJavaScriptEnabled(0);
        this.au.getSettings().setPluginState(android.webkit.WebSettings$PluginState.OFF);
        this.au.getSettings().setAllowFileAccess(1);
        this.au.getSettings().setBuiltInZoomControls(1);
        if (android.os.Build$VERSION.SDK_INT >= 11) {
            this.au.getSettings().setDisplayZoomControls(0);
        }
        this.au.setWebViewClient(new com.teamspeak.ts3client.d(this));
        this.ax.setOnClickListener(new com.teamspeak.ts3client.e(this));
        this.n();
        this.c.o.b(com.teamspeak.ts3client.data.e.a.a("dialog.channel.info.text"));
        v1.requestFocus();
        return v1;
    }

    public final void a(android.os.Bundle p2)
    {
        super.a(p2);
        this.c = ((com.teamspeak.ts3client.Ts3Application) this.i().getApplicationContext());
        return;
    }

    public final void a(android.view.Menu p1, android.view.MenuInflater p2)
    {
        p1.clear();
        super.a(p1, p2);
        return;
    }

    public final void a(com.teamspeak.ts3client.jni.k p5)
    {
        if (!(p5 instanceof com.teamspeak.ts3client.jni.events.UpdateChannel)) {
            if (((p5 instanceof com.teamspeak.ts3client.jni.events.rare.ServerPermissionError)) && (((com.teamspeak.ts3client.jni.events.rare.ServerPermissionError) p5).a == this.c.a.u.a(com.teamspeak.ts3client.jni.g.bK))) {
                com.teamspeak.ts3client.jni.l v0_4 = com.teamspeak.ts3client.data.e.a.a("channelinfo.desc.permerror");
                if (this.l()) {
                    this.i().runOnUiThread(new com.teamspeak.ts3client.f(this, v0_4));
                }
                this.c.a.c.b(this);
            }
        } else {
            if (((com.teamspeak.ts3client.jni.events.UpdateChannel) p5).a == com.teamspeak.ts3client.c.b.b) {
                this.a(1);
                this.c.a.c.b(this);
            }
        }
        return;
    }

    public final void a(String p7, String p8)
    {
        this.av = this.av.replace(p7, new StringBuilder("<img src=\"file://").append(p8).append("\">").toString());
        this.au.loadDataWithBaseURL("", this.av, "text/html", "utf-8", "about:blank");
        this.aw.requestFocus();
        return;
    }

    public final void c(android.os.Bundle p8)
    {
        super.c(p8);
        this.c.a.c.a(this);
        this.c.a.a.ts3client_requestChannelDescription(this.c.a.e, com.teamspeak.ts3client.c.b.b, "ChannelInfoFragment");
        this.c.c = this;
        this.a(0);
        return;
    }
}
