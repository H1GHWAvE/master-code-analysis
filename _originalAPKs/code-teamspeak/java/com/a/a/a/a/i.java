package com.a.a.a.a;
public abstract class i extends android.os.Binder implements com.a.a.a.a.h {
    static final int a = 1;
    private static final String b = "com.android.vending.licensing.ILicensingService";

    public i()
    {
        this.attachInterface(this, "com.android.vending.licensing.ILicensingService");
        return;
    }

    public static com.a.a.a.a.h a(android.os.IBinder p2)
    {
        com.a.a.a.a.h v0_3;
        if (p2 != null) {
            com.a.a.a.a.h v0_1 = p2.queryLocalInterface("com.android.vending.licensing.ILicensingService");
            if ((v0_1 == null) || (!(v0_1 instanceof com.a.a.a.a.h))) {
                v0_3 = new com.a.a.a.a.j(p2);
            } else {
                v0_3 = ((com.a.a.a.a.h) v0_1);
            }
        } else {
            v0_3 = 0;
        }
        return v0_3;
    }

    public android.os.IBinder asBinder()
    {
        return this;
    }

    public boolean onTransact(int p8, android.os.Parcel p9, android.os.Parcel p10, int p11)
    {
        com.a.a.a.a.e v0_1;
        switch (p8) {
            case 1:
                com.a.a.a.a.e v0_6;
                p9.enforceInterface("com.android.vending.licensing.ILicensingService");
                long v2 = p9.readLong();
                String v4 = p9.readString();
                android.os.IBinder v5 = p9.readStrongBinder();
                if (v5 != null) {
                    com.a.a.a.a.e v0_4 = v5.queryLocalInterface("com.android.vending.licensing.ILicenseResultListener");
                    if ((v0_4 == null) || (!(v0_4 instanceof com.a.a.a.a.e))) {
                        v0_6 = new com.a.a.a.a.g(v5);
                    } else {
                        v0_6 = ((com.a.a.a.a.e) v0_4);
                    }
                } else {
                    v0_6 = 0;
                }
                this.a(v2, v4, v0_6);
                v0_1 = 1;
                break;
            case 1598968902:
                p10.writeString("com.android.vending.licensing.ILicensingService");
                v0_1 = 1;
                break;
            default:
                v0_1 = super.onTransact(p8, p9, p10, p11);
        }
        return v0_1;
    }
}
