package com.a.a.a.a;
public final class k implements android.content.ServiceConnection {
    private static final String b = "LicenseChecker";
    private static final String c = "RSA";
    private static final int d = 10000;
    private static final java.security.SecureRandom e;
    private static final boolean f;
    public int a;
    private com.a.a.a.a.h g;
    private java.security.PublicKey h;
    private final android.content.Context i;
    private final com.a.a.a.a.s j;
    private android.os.Handler k;
    private final String l;
    private final String m;
    private final java.util.Set n;
    private final java.util.Queue o;

    static k()
    {
        com.a.a.a.a.k.e = new java.security.SecureRandom();
        return;
    }

    public k(android.content.Context p3, com.a.a.a.a.s p4, String p5)
    {
        this.n = new java.util.HashSet();
        this.o = new java.util.LinkedList();
        this.i = p3;
        this.j = p4;
        this.h = com.a.a.a.a.k.a(p5);
        this.l = this.i.getPackageName();
        this.m = com.a.a.a.a.k.a(p3, this.l);
        android.os.Looper v0_10 = new android.os.HandlerThread("background thread");
        v0_10.start();
        this.k = new android.os.Handler(v0_10.getLooper());
        return;
    }

    private static String a(android.content.Context p2, String p3)
    {
        try {
            String v0_3 = String.valueOf(p2.getPackageManager().getPackageInfo(p3, 0).versionCode);
        } catch (String v0) {
            android.util.Log.e("LicenseChecker", "Package not found. could not get version code.");
            v0_3 = "";
        }
        return v0_3;
    }

    private static java.security.PublicKey a(String p3)
    {
        try {
            return java.security.KeyFactory.getInstance("RSA").generatePublic(new java.security.spec.X509EncodedKeySpec(com.a.a.a.a.a.a.a(p3)));
        } catch (java.security.spec.InvalidKeySpecException v0_4) {
            throw new RuntimeException(v0_4);
        } catch (java.security.spec.InvalidKeySpecException v0_2) {
            android.util.Log.e("LicenseChecker", "Invalid key specification.");
            throw new IllegalArgumentException(v0_2);
        } catch (java.security.spec.InvalidKeySpecException v0_3) {
            android.util.Log.e("LicenseChecker", "Could not decode from Base64.");
            throw new IllegalArgumentException(v0_3);
        }
    }

    static synthetic java.util.Set a(com.a.a.a.a.k p1)
    {
        return p1.n;
    }

    private void a()
    {
        while(true) {
            com.a.a.a.a.p v0_2 = ((com.a.a.a.a.p) this.o.poll());
            if (v0_2 == null) {
                break;
            }
            try {
                android.util.Log.i("LicenseChecker", new StringBuilder("Calling checkLicense on service for ").append(v0_2.c).toString());
                this.g.a(((long) v0_2.b), v0_2.c, new com.a.a.a.a.l(this, v0_2));
                this.n.add(v0_2);
            } catch (android.os.RemoteException v1_3) {
                android.util.Log.w("LicenseChecker", "RemoteException in checkLicense call.", v1_3);
                this.b(v0_2);
            }
        }
        return;
    }

    static synthetic void a(com.a.a.a.a.k p0, com.a.a.a.a.p p1)
    {
        p0.b(p1);
        return;
    }

    private declared_synchronized void a(com.a.a.a.a.p p2)
    {
        try {
            this.n.remove(p2);
        } catch (Throwable v0_3) {
            throw v0_3;
        }
        if (this.n.isEmpty()) {
            this.c();
        }
        return;
    }

    static synthetic java.security.PublicKey b(com.a.a.a.a.k p1)
    {
        return p1.h;
    }

    private void b()
    {
        this.a = 0;
        this.a = (this.a + 16);
        return;
    }

    static synthetic void b(com.a.a.a.a.k p0, com.a.a.a.a.p p1)
    {
        p0.a(p1);
        return;
    }

    private declared_synchronized void b(com.a.a.a.a.p p4)
    {
        try {
            this.j.a(291, 0);
        } catch (com.a.a.a.a.o v0_5) {
            throw v0_5;
        }
        if (!this.j.a()) {
            p4.a.a(291);
        } else {
            p4.a.a();
        }
        return;
    }

    static synthetic android.os.Handler c(com.a.a.a.a.k p1)
    {
        return p1.k;
    }

    private void c()
    {
        if (this.g != null) {
            try {
                this.i.unbindService(this);
            } catch (int v0) {
                android.util.Log.e("LicenseChecker", "Unable to unbind from licensing service (already unbound)");
            }
            this.g = 0;
        }
        return;
    }

    private declared_synchronized void d()
    {
        try {
            this.c();
            this.k.getLooper().quit();
            return;
        } catch (Throwable v0_2) {
            throw v0_2;
        }
    }

    private static int e()
    {
        return com.a.a.a.a.k.e.nextInt();
    }

    public final declared_synchronized void a(com.a.a.a.a.o p8)
    {
        try {
            if (!this.j.a()) {
                com.a.a.a.a.a.b v0_3 = new com.a.a.a.a.p(this.j, new com.a.a.a.a.q(), p8, com.a.a.a.a.k.e.nextInt(), this.l, this.m);
                if (this.g != null) {
                    this.o.offer(v0_3);
                    this.a();
                } else {
                    android.util.Log.i("LicenseChecker", "Binding to licensing service.");
                    try {
                        String v1_5 = new android.content.Intent(new String(com.a.a.a.a.a.a.a("Y29tLmFuZHJvaWQudmVuZGluZy5saWNlbnNpbmcuSUxpY2Vuc2luZ1NlcnZpY2U=")));
                        v1_5.setPackage("com.android.vending");
                    } catch (com.a.a.a.a.a.b v0) {
                        p8.b(6);
                    } catch (com.a.a.a.a.a.b v0_4) {
                        v0_4.printStackTrace();
                    }
                    if (!this.i.bindService(v1_5, this, 1)) {
                        android.util.Log.e("LicenseChecker", "Could not bind to service.");
                        this.b(v0_3);
                    } else {
                        this.o.offer(v0_3);
                    }
                }
            } else {
                android.util.Log.i("LicenseChecker", "Using cached license response");
                p8.a();
                p8.a(256, this.j.b().a, this.j.b().b, this.j.b().c);
            }
        } catch (com.a.a.a.a.a.b v0_8) {
            throw v0_8;
        }
        return;
    }

    public final declared_synchronized void onServiceConnected(android.content.ComponentName p2, android.os.IBinder p3)
    {
        try {
            this.g = com.a.a.a.a.i.a(p3);
            this.a();
            return;
        } catch (Throwable v0_1) {
            throw v0_1;
        }
    }

    public final declared_synchronized void onServiceDisconnected(android.content.ComponentName p3)
    {
        try {
            android.util.Log.w("LicenseChecker", "Service unexpectedly disconnected.");
            this.g = 0;
            return;
        } catch (Throwable v0_2) {
            throw v0_2;
        }
    }
}
