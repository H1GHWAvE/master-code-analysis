package com.a.c;
public final class z extends com.a.c.w {
    public final com.a.c.b.ag a;

    public z()
    {
        this.a = new com.a.c.b.ag();
        return;
    }

    private static com.a.c.w a(Object p1)
    {
        com.a.c.ac v0_1;
        if (p1 != null) {
            v0_1 = new com.a.c.ac(p1);
        } else {
            v0_1 = com.a.c.y.a;
        }
        return v0_1;
    }

    private com.a.c.w a(String p2)
    {
        return ((com.a.c.w) this.a.remove(p2));
    }

    private void a(String p2, Boolean p3)
    {
        this.a(p2, com.a.c.z.a(p3));
        return;
    }

    private void a(String p2, Character p3)
    {
        this.a(p2, com.a.c.z.a(p3));
        return;
    }

    private void a(String p2, Number p3)
    {
        this.a(p2, com.a.c.z.a(p3));
        return;
    }

    private void a(String p2, String p3)
    {
        this.a(p2, com.a.c.z.a(p3));
        return;
    }

    private boolean b(String p2)
    {
        return this.a.containsKey(p2);
    }

    private com.a.c.w c(String p2)
    {
        return ((com.a.c.w) this.a.get(p2));
    }

    private com.a.c.ac d(String p2)
    {
        return ((com.a.c.ac) this.a.get(p2));
    }

    private com.a.c.t e(String p2)
    {
        return ((com.a.c.t) this.a.get(p2));
    }

    private com.a.c.z f(String p2)
    {
        return ((com.a.c.z) this.a.get(p2));
    }

    private com.a.c.z p()
    {
        com.a.c.z v2_1 = new com.a.c.z();
        java.util.Iterator v3 = this.a.entrySet().iterator();
        while (v3.hasNext()) {
            com.a.c.w v0_4 = ((java.util.Map$Entry) v3.next());
            v2_1.a(((String) v0_4.getKey()), ((com.a.c.w) v0_4.getValue()).m());
        }
        return v2_1;
    }

    private java.util.Set q()
    {
        return this.a.entrySet();
    }

    public final void a(String p2, com.a.c.w p3)
    {
        if (p3 == null) {
            p3 = com.a.c.y.a;
        }
        this.a.put(p2, p3);
        return;
    }

    public final boolean equals(Object p3)
    {
        if ((p3 != this) && ((!(p3 instanceof com.a.c.z)) || (!((com.a.c.z) p3).a.equals(this.a)))) {
            int v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        return v0_3;
    }

    public final int hashCode()
    {
        return this.a.hashCode();
    }

    final synthetic com.a.c.w m()
    {
        com.a.c.z v2_1 = new com.a.c.z();
        java.util.Iterator v3 = this.a.entrySet().iterator();
        while (v3.hasNext()) {
            com.a.c.w v0_4 = ((java.util.Map$Entry) v3.next());
            v2_1.a(((String) v0_4.getKey()), ((com.a.c.w) v0_4.getValue()).m());
        }
        return v2_1;
    }
}
