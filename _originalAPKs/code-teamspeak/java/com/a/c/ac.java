package com.a.c;
public final class ac extends com.a.c.w {
    private static final Class[] b;
    public Object a;

    static ac()
    {
        Class[] v0_1 = new Class[16];
        v0_1[0] = Integer.TYPE;
        v0_1[1] = Long.TYPE;
        v0_1[2] = Short.TYPE;
        v0_1[3] = Float.TYPE;
        v0_1[4] = Double.TYPE;
        v0_1[5] = Byte.TYPE;
        v0_1[6] = Boolean.TYPE;
        v0_1[7] = Character.TYPE;
        v0_1[8] = Integer;
        v0_1[9] = Long;
        v0_1[10] = Short;
        v0_1[11] = Float;
        v0_1[12] = Double;
        v0_1[13] = Byte;
        v0_1[14] = Boolean;
        v0_1[15] = Character;
        com.a.c.ac.b = v0_1;
        return;
    }

    public ac(Boolean p1)
    {
        this.a(p1);
        return;
    }

    private ac(Character p1)
    {
        this.a(p1);
        return;
    }

    public ac(Number p1)
    {
        this.a(p1);
        return;
    }

    ac(Object p1)
    {
        this.a(p1);
        return;
    }

    public ac(String p1)
    {
        this.a(p1);
        return;
    }

    private void a(Object p8)
    {
        int v0_0 = 0;
        if (!(p8 instanceof Character)) {
            if ((p8 instanceof Number)) {
                v0_0 = 1;
            } else {
                int v2_4;
                if (!(p8 instanceof String)) {
                    Class v3 = p8.getClass();
                    Class[] v4 = com.a.c.ac.b;
                    int v2_3 = 0;
                    while (v2_3 < v4.length) {
                        if (!v4[v2_3].isAssignableFrom(v3)) {
                            v2_3++;
                        } else {
                            v2_4 = 1;
                        }
                    }
                    v2_4 = 0;
                } else {
                    v2_4 = 1;
                }
                if (v2_4 != 0) {
                }
            }
            com.a.c.b.a.a(v0_0);
            this.a = p8;
        } else {
            this.a = String.valueOf(((Character) p8).charValue());
        }
        return;
    }

    private static boolean a(com.a.c.ac p3)
    {
        int v0_2;
        if (!(p3.a instanceof Number)) {
            v0_2 = 0;
        } else {
            int v0_4 = ((Number) p3.a);
            if ((!(v0_4 instanceof java.math.BigInteger)) && ((!(v0_4 instanceof Long)) && ((!(v0_4 instanceof Integer)) && ((!(v0_4 instanceof Short)) && (!(v0_4 instanceof Byte)))))) {
                v0_2 = 0;
            } else {
                v0_2 = 1;
            }
        }
        return v0_2;
    }

    private static boolean b(Object p7)
    {
        int v0 = 1;
        if (!(p7 instanceof String)) {
            Class v3 = p7.getClass();
            Class[] v4 = com.a.c.ac.b;
            int v2_1 = 0;
            while (v2_1 < v4.length) {
                if (!v4[v2_1].isAssignableFrom(v3)) {
                    v2_1++;
                }
            }
            v0 = 0;
        }
        return v0;
    }

    private com.a.c.ac p()
    {
        return this;
    }

    private boolean q()
    {
        return (this.a instanceof Boolean);
    }

    private boolean r()
    {
        return (this.a instanceof Number);
    }

    private boolean s()
    {
        return (this.a instanceof String);
    }

    public final Number a()
    {
        Number v0_3;
        if (!(this.a instanceof String)) {
            v0_3 = ((Number) this.a);
        } else {
            v0_3 = new com.a.c.b.v(((String) this.a));
        }
        return v0_3;
    }

    public final String b()
    {
        String v0_5;
        if (!(this.a instanceof Number)) {
            if (!(this.a instanceof Boolean)) {
                v0_5 = ((String) this.a);
            } else {
                v0_5 = ((Boolean) this.a).toString();
            }
        } else {
            v0_5 = this.a().toString();
        }
        return v0_5;
    }

    public final double c()
    {
        double v0_3;
        if (!(this.a instanceof Number)) {
            v0_3 = Double.parseDouble(this.b());
        } else {
            v0_3 = this.a().doubleValue();
        }
        return v0_3;
    }

    public final java.math.BigDecimal d()
    {
        java.math.BigDecimal v0_3;
        if (!(this.a instanceof java.math.BigDecimal)) {
            v0_3 = new java.math.BigDecimal(this.a.toString());
        } else {
            v0_3 = ((java.math.BigDecimal) this.a);
        }
        return v0_3;
    }

    public final java.math.BigInteger e()
    {
        java.math.BigInteger v0_3;
        if (!(this.a instanceof java.math.BigInteger)) {
            v0_3 = new java.math.BigInteger(this.a.toString());
        } else {
            v0_3 = ((java.math.BigInteger) this.a);
        }
        return v0_3;
    }

    public final boolean equals(Object p8)
    {
        Object v0_0 = 1;
        if (this != p8) {
            if ((p8 != null) && (this.getClass() == p8.getClass())) {
                if (this.a != null) {
                    if ((!com.a.c.ac.a(this)) || (!com.a.c.ac.a(((com.a.c.ac) p8)))) {
                        if ((!(this.a instanceof Number)) || (!(((com.a.c.ac) p8).a instanceof Number))) {
                            v0_0 = this.a.equals(((com.a.c.ac) p8).a);
                        } else {
                            boolean v2_9 = this.a().doubleValue();
                            double v4_1 = ((com.a.c.ac) p8).a().doubleValue();
                            if ((v2_9 != v4_1) && ((!Double.isNaN(v2_9)) || (!Double.isNaN(v4_1)))) {
                                v0_0 = 0;
                            }
                        }
                    } else {
                        if (this.a().longValue() != ((com.a.c.ac) p8).a().longValue()) {
                            v0_0 = 0;
                        }
                    }
                } else {
                    if (((com.a.c.ac) p8).a != null) {
                        v0_0 = 0;
                    }
                }
            } else {
                v0_0 = 0;
            }
        }
        return v0_0;
    }

    public final float f()
    {
        float v0_3;
        if (!(this.a instanceof Number)) {
            v0_3 = Float.parseFloat(this.b());
        } else {
            v0_3 = this.a().floatValue();
        }
        return v0_3;
    }

    public final long g()
    {
        long v0_3;
        if (!(this.a instanceof Number)) {
            v0_3 = Long.parseLong(this.b());
        } else {
            v0_3 = this.a().longValue();
        }
        return v0_3;
    }

    public final int h()
    {
        int v0_3;
        if (!(this.a instanceof Number)) {
            v0_3 = Integer.parseInt(this.b());
        } else {
            v0_3 = this.a().intValue();
        }
        return v0_3;
    }

    public final int hashCode()
    {
        int v0_5;
        if (this.a != null) {
            if (!com.a.c.ac.a(this)) {
                if (!(this.a instanceof Number)) {
                    v0_5 = this.a.hashCode();
                } else {
                    int v0_8 = Double.doubleToLongBits(this.a().doubleValue());
                    v0_5 = ((int) (v0_8 ^ (v0_8 >> 32)));
                }
            } else {
                int v0_11 = this.a().longValue();
                v0_5 = ((int) (v0_11 ^ (v0_11 >> 32)));
            }
        } else {
            v0_5 = 31;
        }
        return v0_5;
    }

    public final byte i()
    {
        byte v0_3;
        if (!(this.a instanceof Number)) {
            v0_3 = Byte.parseByte(this.b());
        } else {
            v0_3 = this.a().byteValue();
        }
        return v0_3;
    }

    public final char j()
    {
        return this.b().charAt(0);
    }

    public final short k()
    {
        short v0_3;
        if (!(this.a instanceof Number)) {
            v0_3 = Short.parseShort(this.b());
        } else {
            v0_3 = this.a().shortValue();
        }
        return v0_3;
    }

    public final boolean l()
    {
        boolean v0_3;
        if (!(this.a instanceof Boolean)) {
            v0_3 = Boolean.parseBoolean(this.b());
        } else {
            v0_3 = ((Boolean) this.a).booleanValue();
        }
        return v0_3;
    }

    final bridge synthetic com.a.c.w m()
    {
        return this;
    }

    final Boolean o()
    {
        return ((Boolean) this.a);
    }
}
