package com.a.c.b;
final class d implements java.io.Serializable, java.lang.reflect.ParameterizedType {
    private static final long d;
    private final reflect.Type a;
    private final reflect.Type b;
    private final reflect.Type[] c;

    public varargs d(reflect.Type p5, reflect.Type p6, reflect.Type[] p7)
    {
        int v1 = 0;
        if ((p6 instanceof Class)) {
            if ((!reflect.Modifier.isStatic(((Class) p6).getModifiers())) && (((Class) p6).getEnclosingClass() != null)) {
                reflect.Type[] v0_4 = 0;
            } else {
                v0_4 = 1;
            }
            if ((p5 == null) && (v0_4 == null)) {
                reflect.Type[] v0_5 = 0;
            } else {
                v0_5 = 1;
            }
            com.a.c.b.a.a(v0_5);
        }
        reflect.Type[] v0_6;
        if (p5 != null) {
            v0_6 = com.a.c.b.b.a(p5);
        } else {
            v0_6 = 0;
        }
        this.a = v0_6;
        this.b = com.a.c.b.b.a(p6);
        this.c = ((reflect.Type[]) p7.clone());
        while (v1 < this.c.length) {
            com.a.c.b.a.a(this.c[v1]);
            com.a.c.b.b.e(this.c[v1]);
            this.c[v1] = com.a.c.b.b.a(this.c[v1]);
            v1++;
        }
        return;
    }

    public final boolean equals(Object p2)
    {
        if ((!(p2 instanceof reflect.ParameterizedType)) || (!com.a.c.b.b.a(this, ((reflect.ParameterizedType) p2)))) {
            int v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    public final reflect.Type[] getActualTypeArguments()
    {
        return ((reflect.Type[]) this.c.clone());
    }

    public final reflect.Type getOwnerType()
    {
        return this.a;
    }

    public final reflect.Type getRawType()
    {
        return this.b;
    }

    public final int hashCode()
    {
        return ((java.util.Arrays.hashCode(this.c) ^ this.b.hashCode()) ^ com.a.c.b.b.a(this.a));
    }

    public final String toString()
    {
        String v0_13;
        StringBuilder v1_1 = new StringBuilder(((this.c.length + 1) * 30));
        v1_1.append(com.a.c.b.b.c(this.b));
        if (this.c.length != 0) {
            v1_1.append("<").append(com.a.c.b.b.c(this.c[0]));
            String v0_10 = 1;
            while (v0_10 < this.c.length) {
                v1_1.append(", ").append(com.a.c.b.b.c(this.c[v0_10]));
                v0_10++;
            }
            v0_13 = v1_1.append(">").toString();
        } else {
            v0_13 = v1_1.toString();
        }
        return v0_13;
    }
}
