package com.a.c.b;
public final class b {
    static final reflect.Type[] a;

    static b()
    {
        reflect.Type[] v0_1 = new reflect.Type[0];
        com.a.c.b.b.a = v0_1;
        return;
    }

    private b()
    {
        return;
    }

    static synthetic int a(Object p1)
    {
        int v0;
        if (p1 == null) {
            v0 = 0;
        } else {
            v0 = p1.hashCode();
        }
        return v0;
    }

    private static int a(Object[] p2, Object p3)
    {
        int v0_0 = 0;
        while (v0_0 < p2.length) {
            if (!p3.equals(p2[v0_0])) {
                v0_0++;
            } else {
                return v0_0;
            }
        }
        throw new java.util.NoSuchElementException();
    }

    private static Class a(reflect.TypeVariable p2)
    {
        int v0_1;
        int v0_0 = p2.getGenericDeclaration();
        if (!(v0_0 instanceof Class)) {
            v0_1 = 0;
        } else {
            v0_1 = ((Class) v0_0);
        }
        return v0_1;
    }

    private static varargs reflect.ParameterizedType a(reflect.Type p1, reflect.Type p2, reflect.Type[] p3)
    {
        return new com.a.c.b.d(p1, p2, p3);
    }

    public static reflect.Type a(reflect.Type p4)
    {
        reflect.WildcardType v0_4;
        if (!(p4 instanceof Class)) {
            if (!(p4 instanceof reflect.ParameterizedType)) {
                if (!(p4 instanceof reflect.GenericArrayType)) {
                    if (!(p4 instanceof reflect.WildcardType)) {
                        v0_4 = p4;
                    } else {
                        v0_4 = new com.a.c.b.e(((reflect.WildcardType) p4).getUpperBounds(), ((reflect.WildcardType) p4).getLowerBounds());
                    }
                } else {
                    v0_4 = new com.a.c.b.c(((reflect.GenericArrayType) p4).getGenericComponentType());
                }
            } else {
                v0_4 = new com.a.c.b.d(((reflect.ParameterizedType) p4).getOwnerType(), ((reflect.ParameterizedType) p4).getRawType(), ((reflect.ParameterizedType) p4).getActualTypeArguments());
            }
        } else {
            reflect.WildcardType v0_9;
            if (!((Class) p4).isArray()) {
                v0_9 = ((Class) p4);
            } else {
                v0_9 = new com.a.c.b.c(com.a.c.b.b.a(((Class) p4).getComponentType()));
            }
            v0_4 = ((reflect.Type) v0_9);
        }
        return v0_4;
    }

    public static reflect.Type a(reflect.Type p3, Class p4)
    {
        Class v0_1 = com.a.c.b.b.b(p3, p4, java.util.Collection);
        if ((v0_1 instanceof reflect.WildcardType)) {
            v0_1 = ((reflect.WildcardType) v0_1).getUpperBounds()[0];
        }
        Class v0_4;
        if (!(v0_1 instanceof reflect.ParameterizedType)) {
            v0_4 = Object;
        } else {
            v0_4 = ((reflect.ParameterizedType) v0_1).getActualTypeArguments()[0];
        }
        return v0_4;
    }

    private static reflect.Type a(reflect.Type p5, Class p6, Class p7)
    {
        reflect.Type v0_0 = p6;
        reflect.Type v1_0 = p5;
        while (p7 != v0_0) {
            if (p7.isInterface()) {
                boolean v2_0 = v0_0.getInterfaces();
                reflect.Type v1_2 = 0;
                while (v1_2 < v2_0.length) {
                    if (v2_0[v1_2] != p7) {
                        if (!p7.isAssignableFrom(v2_0[v1_2])) {
                            v1_2++;
                        } else {
                            reflect.Type v5_2 = v0_0.getGenericInterfaces()[v1_2];
                            v0_0 = v2_0[v1_2];
                            v1_0 = v5_2;
                        }
                    } else {
                        p7 = v0_0.getGenericInterfaces()[v1_2];
                    }
                    return p7;
                }
            }
            if (v0_0.isInterface()) {
                return p7;
            }
            while (v0_0 != Object) {
                reflect.Type v1_5 = v0_0.getSuperclass();
                if (v1_5 != p7) {
                    if (!p7.isAssignableFrom(v1_5)) {
                        v0_0 = v1_5;
                    } else {
                        reflect.Type v5_1 = v0_0.getGenericSuperclass();
                        v0_0 = v1_5;
                        v1_0 = v5_1;
                    }
                } else {
                    p7 = v0_0.getGenericSuperclass();
                    break;
                }
            }
            return p7;
        }
        p7 = v1_0;
        return p7;
    }

    public static reflect.Type a(reflect.Type p9, Class p10, reflect.Type p11)
    {
        int v3 = 0;
        com.a.c.b.d v0_0 = p11;
        while ((v0_0 instanceof reflect.TypeVariable)) {
            reflect.Type[] v2_9;
            reflect.Type[] v1_23 = ((reflect.TypeVariable) v0_0);
            com.a.c.b.d v0_4 = v1_23.getGenericDeclaration();
            if (!(v0_4 instanceof Class)) {
                v2_9 = 0;
            } else {
                v2_9 = ((Class) v0_4);
            }
            if (v2_9 == null) {
                v0_0 = v1_23;
            } else {
                com.a.c.b.d v0_7 = com.a.c.b.b.a(p9, p10, v2_9);
                if (!(v0_7 instanceof reflect.ParameterizedType)) {
                } else {
                    Class v5_4 = v2_9.getTypeParameters();
                    reflect.Type[] v2_10 = 0;
                    while (v2_10 < v5_4.length) {
                        if (!v1_23.equals(v5_4[v2_10])) {
                            v2_10++;
                        } else {
                            v0_0 = ((reflect.ParameterizedType) v0_7).getActualTypeArguments()[v2_10];
                        }
                    }
                    throw new java.util.NoSuchElementException();
                }
            }
            if (v0_0 == v1_23) {
            }
            return v0_0;
        }
        if ((!(v0_0 instanceof Class)) || (!((Class) v0_0).isArray())) {
            if (!(v0_0 instanceof reflect.GenericArrayType)) {
                if (!(v0_0 instanceof reflect.ParameterizedType)) {
                    if ((v0_0 instanceof reflect.WildcardType)) {
                        v0_0 = ((reflect.WildcardType) v0_0);
                        reflect.Type[] v1_8 = v0_0.getLowerBounds();
                        reflect.Type[] v2_0 = v0_0.getUpperBounds();
                        if (v1_8.length != 1) {
                            if (v2_0.length == 1) {
                                reflect.Type[] v1_11 = com.a.c.b.b.a(p9, p10, v2_0[0]);
                                if (v1_11 != v2_0[0]) {
                                    reflect.Type[] v2_2 = new reflect.Type[1];
                                    v2_2[0] = v1_11;
                                    v0_0 = new com.a.c.b.e(v2_2, com.a.c.b.b.a);
                                }
                            }
                        } else {
                            reflect.Type[] v2_4 = com.a.c.b.b.a(p9, p10, v1_8[0]);
                            if (v2_4 != v1_8[0]) {
                                reflect.Type[] v1_14 = new reflect.Type[1];
                                v1_14[0] = Object;
                                reflect.Type[] v4_1 = new reflect.Type[1];
                                v4_1[0] = v2_4;
                                v0_0 = new com.a.c.b.e(v1_14, v4_1);
                            }
                        }
                    }
                } else {
                    reflect.Type[] v1_16;
                    v0_0 = ((reflect.ParameterizedType) v0_0);
                    reflect.Type[] v1_15 = v0_0.getOwnerType();
                    Class v5_2 = com.a.c.b.b.a(p9, p10, v1_15);
                    if (v5_2 == v1_15) {
                        v1_16 = 0;
                    } else {
                        v1_16 = 1;
                    }
                    reflect.Type[] v2_5 = v0_0.getActualTypeArguments();
                    boolean v6_0 = v2_5.length;
                    while (v3 < v6_0) {
                        reflect.Type v7_1 = com.a.c.b.b.a(p9, p10, v2_5[v3]);
                        if (v7_1 != v2_5[v3]) {
                            if (v1_16 == null) {
                                v2_5 = ((reflect.Type[]) v2_5.clone());
                                v1_16 = 1;
                            }
                            v2_5[v3] = v7_1;
                        }
                        v3++;
                    }
                    if (v1_16 != null) {
                        v0_0 = new com.a.c.b.d(v5_2, v0_0.getRawType(), v2_5);
                    }
                }
            } else {
                v0_0 = ((reflect.GenericArrayType) v0_0);
                reflect.Type[] v1_20 = v0_0.getGenericComponentType();
                reflect.Type[] v2_6 = com.a.c.b.b.a(p9, p10, v1_20);
                if (v1_20 != v2_6) {
                    v0_0 = com.a.c.b.b.f(v2_6);
                }
            }
        } else {
            v0_0 = ((Class) v0_0);
            reflect.Type[] v1_21 = v0_0.getComponentType();
            reflect.Type[] v2_7 = com.a.c.b.b.a(p9, p10, v1_21);
            if (v1_21 != v2_7) {
                v0_0 = com.a.c.b.b.f(v2_7);
            }
        }
        return v0_0;
    }

    private static reflect.Type a(reflect.Type p4, Class p5, reflect.TypeVariable p6)
    {
        int v1_1;
        reflect.Type[] v0_0 = p6.getGenericDeclaration();
        if (!(v0_0 instanceof Class)) {
            v1_1 = 0;
        } else {
            v1_1 = ((Class) v0_0);
        }
        if (v1_1 != 0) {
            reflect.Type[] v0_3 = com.a.c.b.b.a(p4, p5, v1_1);
            if ((v0_3 instanceof reflect.ParameterizedType)) {
                reflect.TypeVariable[] v2_1 = v1_1.getTypeParameters();
                int v1_2 = 0;
                while (v1_2 < v2_1.length) {
                    if (!p6.equals(v2_1[v1_2])) {
                        v1_2++;
                    } else {
                        p6 = ((reflect.ParameterizedType) v0_3).getActualTypeArguments()[v1_2];
                    }
                }
                throw new java.util.NoSuchElementException();
            }
        }
        return p6;
    }

    private static boolean a(Object p1, Object p2)
    {
        if ((p1 != p2) && ((p1 == null) || (!p1.equals(p2)))) {
            int v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    public static boolean a(reflect.Type p6, reflect.Type p7)
    {
        String v1_0 = p7;
        int v0_0 = p6;
        while (v0_0 != v1_0) {
            if (!(v0_0 instanceof Class)) {
                if (!(v0_0 instanceof reflect.ParameterizedType)) {
                    if (!(v0_0 instanceof reflect.GenericArrayType)) {
                        if (!(v0_0 instanceof reflect.WildcardType)) {
                            if (!(v0_0 instanceof reflect.TypeVariable)) {
                                int v0_1 = 0;
                            } else {
                                if ((v1_0 instanceof reflect.TypeVariable)) {
                                    int v0_2 = ((reflect.TypeVariable) v0_0);
                                    String v1_1 = ((reflect.TypeVariable) v1_0);
                                    if ((v0_2.getGenericDeclaration() != v1_1.getGenericDeclaration()) || (!v0_2.getName().equals(v1_1.getName()))) {
                                        v0_1 = 0;
                                    } else {
                                        v0_1 = 1;
                                    }
                                } else {
                                    v0_1 = 0;
                                }
                            }
                        } else {
                            if ((v1_0 instanceof reflect.WildcardType)) {
                                int v0_5 = ((reflect.WildcardType) v0_0);
                                String v1_3 = ((reflect.WildcardType) v1_0);
                                if ((!java.util.Arrays.equals(v0_5.getUpperBounds(), v1_3.getUpperBounds())) || (!java.util.Arrays.equals(v0_5.getLowerBounds(), v1_3.getLowerBounds()))) {
                                    v0_1 = 0;
                                } else {
                                    v0_1 = 1;
                                }
                            } else {
                                v0_1 = 0;
                            }
                        }
                    } else {
                        if ((v1_0 instanceof reflect.GenericArrayType)) {
                            String v1_5 = ((reflect.GenericArrayType) v1_0);
                            v0_0 = ((reflect.GenericArrayType) v0_0).getGenericComponentType();
                            v1_0 = v1_5.getGenericComponentType();
                        } else {
                            v0_1 = 0;
                        }
                    }
                } else {
                    if ((v1_0 instanceof reflect.ParameterizedType)) {
                        reflect.GenericDeclaration v4_14;
                        int v0_9 = ((reflect.ParameterizedType) v0_0);
                        String v1_6 = ((reflect.ParameterizedType) v1_0);
                        reflect.GenericDeclaration v4_12 = v0_9.getOwnerType();
                        reflect.GenericDeclaration v5_2 = v1_6.getOwnerType();
                        if ((v4_12 != v5_2) && ((v4_12 == null) || (!v4_12.equals(v5_2)))) {
                            v4_14 = 0;
                        } else {
                            v4_14 = 1;
                        }
                        if ((v4_14 == null) || ((!v0_9.getRawType().equals(v1_6.getRawType())) || (!java.util.Arrays.equals(v0_9.getActualTypeArguments(), v1_6.getActualTypeArguments())))) {
                            v0_1 = 0;
                        } else {
                            v0_1 = 1;
                        }
                    } else {
                        v0_1 = 0;
                    }
                }
            } else {
                v0_1 = v0_0.equals(v1_0);
            }
            return v0_1;
        }
        v0_1 = 1;
        return v0_1;
    }

    private static int b(Object p1)
    {
        int v0;
        if (p1 == null) {
            v0 = 0;
        } else {
            v0 = p1.hashCode();
        }
        return v0;
    }

    public static Class b(reflect.Type p5)
    {
        String v0_0 = p5;
        while (!(v0_0 instanceof Class)) {
            if (!(v0_0 instanceof reflect.ParameterizedType)) {
                if (!(v0_0 instanceof reflect.GenericArrayType)) {
                    if (!(v0_0 instanceof reflect.TypeVariable)) {
                        if (!(v0_0 instanceof reflect.WildcardType)) {
                            String v1_6;
                            if (v0_0 != null) {
                                v1_6 = v0_0.getClass().getName();
                            } else {
                                v1_6 = "null";
                            }
                            throw new IllegalArgumentException(new StringBuilder("Expected a Class, ParameterizedType, or GenericArrayType, but <").append(v0_0).append("> is of type ").append(v1_6).toString());
                        } else {
                            v0_0 = ((reflect.WildcardType) v0_0).getUpperBounds()[0];
                        }
                    } else {
                        String v0_7 = Object;
                    }
                } else {
                    v0_7 = reflect.Array.newInstance(com.a.c.b.b.b(((reflect.GenericArrayType) v0_0).getGenericComponentType()), 0).getClass();
                }
            } else {
                String v0_13 = ((reflect.ParameterizedType) v0_0).getRawType();
                com.a.c.b.a.a((v0_13 instanceof Class));
                v0_7 = ((Class) v0_13);
            }
            return v0_7;
        }
        v0_7 = ((Class) v0_0);
        return v0_7;
    }

    private static reflect.Type b(reflect.Type p1, Class p2, Class p3)
    {
        com.a.c.b.a.a(p3.isAssignableFrom(p2));
        return com.a.c.b.b.a(p1, p2, com.a.c.b.b.a(p1, p2, p3));
    }

    public static reflect.Type[] b(reflect.Type p5, Class p6)
    {
        reflect.Type[] v0_3;
        if (p5 != java.util.Properties) {
            reflect.Type[] v0_2 = com.a.c.b.b.b(p5, p6, java.util.Map);
            if (!(v0_2 instanceof reflect.ParameterizedType)) {
                v0_3 = new reflect.Type[2];
                v0_3[0] = Object;
                v0_3[1] = Object;
            } else {
                v0_3 = ((reflect.ParameterizedType) v0_2).getActualTypeArguments();
            }
        } else {
            v0_3 = new reflect.Type[2];
            v0_3[0] = String;
            v0_3[1] = String;
        }
        return v0_3;
    }

    public static String c(reflect.Type p1)
    {
        String v0_1;
        if (!(p1 instanceof Class)) {
            v0_1 = p1.toString();
        } else {
            v0_1 = ((Class) p1).getName();
        }
        return v0_1;
    }

    public static reflect.Type d(reflect.Type p1)
    {
        Class v0_1;
        if (!(p1 instanceof reflect.GenericArrayType)) {
            v0_1 = ((Class) p1).getComponentType();
        } else {
            v0_1 = ((reflect.GenericArrayType) p1).getGenericComponentType();
        }
        return v0_1;
    }

    static synthetic void e(reflect.Type p1)
    {
        if (((p1 instanceof Class)) && (((Class) p1).isPrimitive())) {
            int v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        com.a.c.b.a.a(v0_2);
        return;
    }

    private static reflect.GenericArrayType f(reflect.Type p1)
    {
        return new com.a.c.b.c(p1);
    }

    private static reflect.WildcardType g(reflect.Type p3)
    {
        reflect.Type[] v1_1 = new reflect.Type[1];
        v1_1[0] = p3;
        return new com.a.c.b.e(v1_1, com.a.c.b.b.a);
    }

    private static reflect.WildcardType h(reflect.Type p5)
    {
        reflect.Type[] v1 = new reflect.Type[1];
        v1[0] = Object;
        reflect.Type[] v2_1 = new reflect.Type[1];
        v2_1[0] = p5;
        return new com.a.c.b.e(v1, v2_1);
    }

    private static void i(reflect.Type p1)
    {
        if (((p1 instanceof Class)) && (((Class) p1).isPrimitive())) {
            int v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        com.a.c.b.a.a(v0_2);
        return;
    }
}
