package com.a.c.b.a;
final class ad extends com.a.c.an {

    ad()
    {
        return;
    }

    private static void a(com.a.c.d.e p1, Character p2)
    {
        String v0;
        if (p2 != null) {
            v0 = String.valueOf(p2);
        } else {
            v0 = 0;
        }
        p1.b(v0);
        return;
    }

    private static Character b(com.a.c.d.a p4)
    {
        Character v0_3;
        if (p4.f() != com.a.c.d.d.i) {
            Character v0_1 = p4.i();
            if (v0_1.length() == 1) {
                v0_3 = Character.valueOf(v0_1.charAt(0));
            } else {
                throw new com.a.c.ag(new StringBuilder("Expecting character, got: ").append(v0_1).toString());
            }
        } else {
            p4.k();
            v0_3 = 0;
        }
        return v0_3;
    }

    public final synthetic Object a(com.a.c.d.a p5)
    {
        Character v0_3;
        if (p5.f() != com.a.c.d.d.i) {
            Character v0_1 = p5.i();
            if (v0_1.length() == 1) {
                v0_3 = Character.valueOf(v0_1.charAt(0));
            } else {
                throw new com.a.c.ag(new StringBuilder("Expecting character, got: ").append(v0_1).toString());
            }
        } else {
            p5.k();
            v0_3 = 0;
        }
        return v0_3;
    }

    public final synthetic void a(com.a.c.d.e p2, Object p3)
    {
        String v0;
        if (((Character) p3) != null) {
            v0 = String.valueOf(((Character) p3));
        } else {
            v0 = 0;
        }
        p2.b(v0);
        return;
    }
}
