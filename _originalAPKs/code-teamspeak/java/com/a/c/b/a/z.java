package com.a.c.b.a;
public final class z {
    public static final com.a.c.an A;
    public static final com.a.c.ap B;
    public static final com.a.c.an C;
    public static final com.a.c.ap D;
    public static final com.a.c.an E;
    public static final com.a.c.ap F;
    public static final com.a.c.an G;
    public static final com.a.c.ap H;
    public static final com.a.c.an I;
    public static final com.a.c.ap J;
    public static final com.a.c.ap K;
    public static final com.a.c.an L;
    public static final com.a.c.ap M;
    public static final com.a.c.an N;
    public static final com.a.c.ap O;
    public static final com.a.c.an P;
    public static final com.a.c.ap Q;
    public static final com.a.c.ap R;
    public static final com.a.c.an a;
    public static final com.a.c.ap b;
    public static final com.a.c.an c;
    public static final com.a.c.ap d;
    public static final com.a.c.an e;
    public static final com.a.c.an f;
    public static final com.a.c.ap g;
    public static final com.a.c.an h;
    public static final com.a.c.ap i;
    public static final com.a.c.an j;
    public static final com.a.c.ap k;
    public static final com.a.c.an l;
    public static final com.a.c.ap m;
    public static final com.a.c.an n;
    public static final com.a.c.an o;
    public static final com.a.c.an p;
    public static final com.a.c.an q;
    public static final com.a.c.ap r;
    public static final com.a.c.an s;
    public static final com.a.c.ap t;
    public static final com.a.c.an u;
    public static final com.a.c.an v;
    public static final com.a.c.an w;
    public static final com.a.c.ap x;
    public static final com.a.c.an y;
    public static final com.a.c.ap z;

    static z()
    {
        com.a.c.b.a.z.a = new com.a.c.b.a.aa();
        com.a.c.b.a.z.b = com.a.c.b.a.z.a(Class, com.a.c.b.a.z.a);
        com.a.c.b.a.z.c = new com.a.c.b.a.al();
        com.a.c.b.a.z.d = com.a.c.b.a.z.a(java.util.BitSet, com.a.c.b.a.z.c);
        com.a.c.b.a.z.e = new com.a.c.b.a.ax();
        com.a.c.b.a.z.f = new com.a.c.b.a.bb();
        com.a.c.b.a.z.g = com.a.c.b.a.z.a(Boolean.TYPE, Boolean, com.a.c.b.a.z.e);
        com.a.c.b.a.z.h = new com.a.c.b.a.bc();
        com.a.c.b.a.z.i = com.a.c.b.a.z.a(Byte.TYPE, Byte, com.a.c.b.a.z.h);
        com.a.c.b.a.z.j = new com.a.c.b.a.bd();
        com.a.c.b.a.z.k = com.a.c.b.a.z.a(Short.TYPE, Short, com.a.c.b.a.z.j);
        com.a.c.b.a.z.l = new com.a.c.b.a.be();
        com.a.c.b.a.z.m = com.a.c.b.a.z.a(Integer.TYPE, Integer, com.a.c.b.a.z.l);
        com.a.c.b.a.z.n = new com.a.c.b.a.bf();
        com.a.c.b.a.z.o = new com.a.c.b.a.bg();
        com.a.c.b.a.z.p = new com.a.c.b.a.ab();
        com.a.c.b.a.z.q = new com.a.c.b.a.ac();
        com.a.c.b.a.z.r = com.a.c.b.a.z.a(Number, com.a.c.b.a.z.q);
        com.a.c.b.a.z.s = new com.a.c.b.a.ad();
        com.a.c.b.a.z.t = com.a.c.b.a.z.a(Character.TYPE, Character, com.a.c.b.a.z.s);
        com.a.c.b.a.z.u = new com.a.c.b.a.ae();
        com.a.c.b.a.z.v = new com.a.c.b.a.af();
        com.a.c.b.a.z.w = new com.a.c.b.a.ag();
        com.a.c.b.a.z.x = com.a.c.b.a.z.a(String, com.a.c.b.a.z.u);
        com.a.c.b.a.z.y = new com.a.c.b.a.ah();
        com.a.c.b.a.z.z = com.a.c.b.a.z.a(StringBuilder, com.a.c.b.a.z.y);
        com.a.c.b.a.z.A = new com.a.c.b.a.ai();
        com.a.c.b.a.z.B = com.a.c.b.a.z.a(StringBuffer, com.a.c.b.a.z.A);
        com.a.c.b.a.z.C = new com.a.c.b.a.aj();
        com.a.c.b.a.z.D = com.a.c.b.a.z.a(java.net.URL, com.a.c.b.a.z.C);
        com.a.c.b.a.z.E = new com.a.c.b.a.ak();
        com.a.c.b.a.z.F = com.a.c.b.a.z.a(java.net.URI, com.a.c.b.a.z.E);
        com.a.c.b.a.z.G = new com.a.c.b.a.am();
        com.a.c.b.a.z.H = com.a.c.b.a.z.b(java.net.InetAddress, com.a.c.b.a.z.G);
        com.a.c.b.a.z.I = new com.a.c.b.a.an();
        com.a.c.b.a.z.J = com.a.c.b.a.z.a(java.util.UUID, com.a.c.b.a.z.I);
        com.a.c.b.a.z.K = new com.a.c.b.a.ao();
        com.a.c.b.a.z.L = new com.a.c.b.a.aq();
        com.a.c.b.a.z.M = new com.a.c.b.a.ay(java.util.Calendar, java.util.GregorianCalendar, com.a.c.b.a.z.L);
        com.a.c.b.a.z.N = new com.a.c.b.a.ar();
        com.a.c.b.a.z.O = com.a.c.b.a.z.a(java.util.Locale, com.a.c.b.a.z.N);
        com.a.c.b.a.z.P = new com.a.c.b.a.as();
        com.a.c.b.a.z.Q = com.a.c.b.a.z.b(com.a.c.w, com.a.c.b.a.z.P);
        com.a.c.b.a.z.R = new com.a.c.b.a.at();
        return;
    }

    private z()
    {
        return;
    }

    private static com.a.c.ap a()
    {
        return new com.a.c.b.a.at();
    }

    public static com.a.c.ap a(com.a.c.c.a p1, com.a.c.an p2)
    {
        return new com.a.c.b.a.au(p1, p2);
    }

    public static com.a.c.ap a(Class p1, com.a.c.an p2)
    {
        return new com.a.c.b.a.av(p1, p2);
    }

    public static com.a.c.ap a(Class p1, Class p2, com.a.c.an p3)
    {
        return new com.a.c.b.a.aw(p1, p2, p3);
    }

    public static com.a.c.ap b(Class p1, com.a.c.an p2)
    {
        return new com.a.c.b.a.az(p1, p2);
    }

    private static com.a.c.ap b(Class p1, Class p2, com.a.c.an p3)
    {
        return new com.a.c.b.a.ay(p1, p2, p3);
    }
}
