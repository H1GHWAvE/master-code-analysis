package com.a.c.b.a;
final class aa extends com.a.c.an {

    aa()
    {
        return;
    }

    private static void a(com.a.c.d.e p3, Class p4)
    {
        if (p4 != null) {
            throw new UnsupportedOperationException(new StringBuilder("Attempted to serialize java.lang.Class: ").append(p4.getName()).append(". Forgot to register a type adapter?").toString());
        } else {
            p3.f();
            return;
        }
    }

    private static Class b(com.a.c.d.a p2)
    {
        if (p2.f() != com.a.c.d.d.i) {
            throw new UnsupportedOperationException("Attempted to deserialize a java.lang.Class. Forgot to register a type adapter?");
        } else {
            p2.k();
            return 0;
        }
    }

    public final synthetic Object a(com.a.c.d.a p3)
    {
        if (p3.f() != com.a.c.d.d.i) {
            throw new UnsupportedOperationException("Attempted to deserialize a java.lang.Class. Forgot to register a type adapter?");
        } else {
            p3.k();
            return 0;
        }
    }

    public final synthetic void a(com.a.c.d.e p4, Object p5)
    {
        if (((Class) p5) != null) {
            throw new UnsupportedOperationException(new StringBuilder("Attempted to serialize java.lang.Class: ").append(((Class) p5).getName()).append(". Forgot to register a type adapter?").toString());
        } else {
            p4.f();
            return;
        }
    }
}
