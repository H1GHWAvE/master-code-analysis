package com.a.c.b.a;
public final class n extends com.a.c.an {
    public static final com.a.c.ap a;
    private final com.a.c.k b;

    static n()
    {
        com.a.c.b.a.n.a = new com.a.c.b.a.o();
        return;
    }

    private n(com.a.c.k p1)
    {
        this.b = p1;
        return;
    }

    synthetic n(com.a.c.k p1, byte p2)
    {
        this(p1);
        return;
    }

    public final Object a(com.a.c.d.a p4)
    {
        int v0_3;
        switch (com.a.c.b.a.p.a[p4.f().ordinal()]) {
            case 1:
                v0_3 = new java.util.ArrayList();
                p4.a();
                while (p4.e()) {
                    v0_3.add(this.a(p4));
                }
                p4.b();
                break;
            case 2:
                v0_3 = new com.a.c.b.ag();
                p4.c();
                while (p4.e()) {
                    v0_3.put(p4.h(), this.a(p4));
                }
                p4.d();
                break;
            case 3:
                v0_3 = p4.i();
                break;
            case 4:
                v0_3 = Double.valueOf(p4.l());
                break;
            case 5:
                v0_3 = Boolean.valueOf(p4.j());
                break;
            case 6:
                p4.k();
                v0_3 = 0;
                break;
            default:
                throw new IllegalStateException();
        }
        return v0_3;
    }

    public final void a(com.a.c.d.e p3, Object p4)
    {
        if (p4 != null) {
            com.a.c.an v0_1 = this.b.a(p4.getClass());
            if (!(v0_1 instanceof com.a.c.b.a.n)) {
                v0_1.a(p3, p4);
            } else {
                p3.d();
                p3.e();
            }
        } else {
            p3.f();
        }
        return;
    }
}
