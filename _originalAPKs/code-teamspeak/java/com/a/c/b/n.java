package com.a.c.b;
final class n implements com.a.c.b.ao {
    final synthetic reflect.Type a;
    final synthetic com.a.c.b.f b;

    n(com.a.c.b.f p1, reflect.Type p2)
    {
        this.b = p1;
        this.a = p2;
        return;
    }

    public final Object a()
    {
        if (!(this.a instanceof reflect.ParameterizedType)) {
            throw new com.a.c.x(new StringBuilder("Invalid EnumSet type: ").append(this.a.toString()).toString());
        } else {
            com.a.c.x v0_7 = ((reflect.ParameterizedType) this.a).getActualTypeArguments()[0];
            if (!(v0_7 instanceof Class)) {
                throw new com.a.c.x(new StringBuilder("Invalid EnumSet type: ").append(this.a.toString()).toString());
            } else {
                return java.util.EnumSet.noneOf(((Class) v0_7));
            }
        }
    }
}
