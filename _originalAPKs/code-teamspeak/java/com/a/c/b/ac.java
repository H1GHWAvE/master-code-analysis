package com.a.c.b;
final class ac extends java.util.AbstractSet {
    final synthetic com.a.c.b.w a;

    ac(com.a.c.b.w p1)
    {
        this.a = p1;
        return;
    }

    public final void clear()
    {
        this.a.clear();
        return;
    }

    public final boolean contains(Object p2)
    {
        return this.a.containsKey(p2);
    }

    public final java.util.Iterator iterator()
    {
        return new com.a.c.b.ad(this);
    }

    public final boolean remove(Object p2)
    {
        int v0_2;
        if (this.a.a(p2) == null) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    public final int size()
    {
        return this.a.d;
    }
}
