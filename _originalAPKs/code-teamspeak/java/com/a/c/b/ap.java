package com.a.c.b;
public final class ap {
    private static final java.util.Map a;
    private static final java.util.Map b;

    static ap()
    {
        java.util.Map v0_1 = new java.util.HashMap(16);
        java.util.HashMap v1_1 = new java.util.HashMap(16);
        com.a.c.b.ap.a(v0_1, v1_1, Boolean.TYPE, Boolean);
        com.a.c.b.ap.a(v0_1, v1_1, Byte.TYPE, Byte);
        com.a.c.b.ap.a(v0_1, v1_1, Character.TYPE, Character);
        com.a.c.b.ap.a(v0_1, v1_1, Double.TYPE, Double);
        com.a.c.b.ap.a(v0_1, v1_1, Float.TYPE, Float);
        com.a.c.b.ap.a(v0_1, v1_1, Integer.TYPE, Integer);
        com.a.c.b.ap.a(v0_1, v1_1, Long.TYPE, Long);
        com.a.c.b.ap.a(v0_1, v1_1, Short.TYPE, Short);
        com.a.c.b.ap.a(v0_1, v1_1, Void.TYPE, Void);
        com.a.c.b.ap.a = java.util.Collections.unmodifiableMap(v0_1);
        com.a.c.b.ap.b = java.util.Collections.unmodifiableMap(v1_1);
        return;
    }

    private ap()
    {
        return;
    }

    public static Class a(Class p2)
    {
        Class v0_2 = ((Class) com.a.c.b.ap.a.get(com.a.c.b.a.a(p2)));
        if (v0_2 != null) {
            p2 = v0_2;
        }
        return p2;
    }

    private static void a(java.util.Map p0, java.util.Map p1, Class p2, Class p3)
    {
        p0.put(p2, p3);
        p1.put(p3, p2);
        return;
    }

    public static boolean a(reflect.Type p1)
    {
        return com.a.c.b.ap.a.containsKey(p1);
    }

    private static Class b(Class p2)
    {
        Class v0_2 = ((Class) com.a.c.b.ap.b.get(com.a.c.b.a.a(p2)));
        if (v0_2 != null) {
            p2 = v0_2;
        }
        return p2;
    }

    private static boolean b(reflect.Type p2)
    {
        return com.a.c.b.ap.b.containsKey(com.a.c.b.a.a(p2));
    }
}
