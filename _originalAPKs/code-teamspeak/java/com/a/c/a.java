package com.a.c;
final class a implements com.a.c.ae, com.a.c.v {
    private final java.text.DateFormat a;
    private final java.text.DateFormat b;
    private final java.text.DateFormat c;

    a()
    {
        this(java.text.DateFormat.getDateTimeInstance(2, 2, java.util.Locale.US), java.text.DateFormat.getDateTimeInstance(2, 2));
        return;
    }

    private a(int p3)
    {
        this(java.text.DateFormat.getDateInstance(p3, java.util.Locale.US), java.text.DateFormat.getDateInstance(p3));
        return;
    }

    public a(int p3, int p4)
    {
        this(java.text.DateFormat.getDateTimeInstance(p3, p4, java.util.Locale.US), java.text.DateFormat.getDateTimeInstance(p3, p4));
        return;
    }

    a(String p3)
    {
        this(new java.text.SimpleDateFormat(p3, java.util.Locale.US), new java.text.SimpleDateFormat(p3));
        return;
    }

    private a(java.text.DateFormat p4, java.text.DateFormat p5)
    {
        this.a = p4;
        this.b = p5;
        this.c = new java.text.SimpleDateFormat("yyyy-MM-dd\'T\'HH:mm:ss\'Z\'", java.util.Locale.US);
        this.c.setTimeZone(java.util.TimeZone.getTimeZone("UTC"));
        return;
    }

    private com.a.c.w a(java.util.Date p4)
    {
        try {
            return new com.a.c.ac(this.a.format(p4));
        } catch (Throwable v0_2) {
            throw v0_2;
        }
    }

    private java.util.Date a(com.a.c.w p5)
    {
        try {
            java.text.ParseException v0_1 = this.b.parse(p5.b());
        } catch (java.text.ParseException v0) {
            try {
                v0_1 = this.a.parse(p5.b());
            } catch (java.text.ParseException v0) {
                v0_1 = this.c.parse(p5.b());
                return v0_1;
            }
            return v0_1;
        } catch (java.text.ParseException v0_5) {
            throw v0_5;
        }
        return v0_1;
    }

    private java.util.Date b(com.a.c.w p5, reflect.Type p6)
    {
        if ((p5 instanceof com.a.c.ac)) {
            IllegalArgumentException v0_1 = this.a(p5);
            if (p6 != java.util.Date) {
                if (p6 != java.sql.Timestamp) {
                    if (p6 != java.sql.Date) {
                        throw new IllegalArgumentException(new StringBuilder().append(this.getClass()).append(" cannot deserialize to ").append(p6).toString());
                    } else {
                        v0_1 = new java.sql.Date(v0_1.getTime());
                    }
                } else {
                    v0_1 = new java.sql.Timestamp(v0_1.getTime());
                }
            }
            return v0_1;
        } else {
            throw new com.a.c.aa("The date should be a string value");
        }
    }

    public final bridge synthetic com.a.c.w a(Object p2)
    {
        return this.a(((java.util.Date) p2));
    }

    public final synthetic Object a(com.a.c.w p5, reflect.Type p6)
    {
        if ((p5 instanceof com.a.c.ac)) {
            IllegalArgumentException v0_1 = this.a(p5);
            if (p6 != java.util.Date) {
                if (p6 != java.sql.Timestamp) {
                    if (p6 != java.sql.Date) {
                        throw new IllegalArgumentException(new StringBuilder().append(this.getClass()).append(" cannot deserialize to ").append(p6).toString());
                    } else {
                        v0_1 = new java.sql.Date(v0_1.getTime());
                    }
                } else {
                    v0_1 = new java.sql.Timestamp(v0_1.getTime());
                }
            }
            return v0_1;
        } else {
            throw new com.a.c.aa("The date should be a string value");
        }
    }

    public final String toString()
    {
        String v0_1 = new StringBuilder();
        v0_1.append(com.a.c.a.getSimpleName());
        v0_1.append(40).append(this.b.getClass().getSimpleName()).append(41);
        return v0_1.toString();
    }
}
