package com.a.c;
public abstract class w {

    public w()
    {
        return;
    }

    private boolean p()
    {
        return (this instanceof com.a.c.t);
    }

    private boolean q()
    {
        return (this instanceof com.a.c.z);
    }

    private boolean r()
    {
        return (this instanceof com.a.c.ac);
    }

    private boolean s()
    {
        return (this instanceof com.a.c.y);
    }

    private com.a.c.z t()
    {
        if (!(this instanceof com.a.c.z)) {
            throw new IllegalStateException(new StringBuilder("Not a JSON Object: ").append(this).toString());
        } else {
            return ((com.a.c.z) this);
        }
    }

    private com.a.c.t u()
    {
        if (!(this instanceof com.a.c.t)) {
            throw new IllegalStateException("This is not a JSON Array.");
        } else {
            return ((com.a.c.t) this);
        }
    }

    private com.a.c.y v()
    {
        if (!(this instanceof com.a.c.y)) {
            throw new IllegalStateException("This is not a JSON Null.");
        } else {
            return ((com.a.c.y) this);
        }
    }

    public Number a()
    {
        throw new UnsupportedOperationException(this.getClass().getSimpleName());
    }

    public String b()
    {
        throw new UnsupportedOperationException(this.getClass().getSimpleName());
    }

    public double c()
    {
        throw new UnsupportedOperationException(this.getClass().getSimpleName());
    }

    public java.math.BigDecimal d()
    {
        throw new UnsupportedOperationException(this.getClass().getSimpleName());
    }

    public java.math.BigInteger e()
    {
        throw new UnsupportedOperationException(this.getClass().getSimpleName());
    }

    public float f()
    {
        throw new UnsupportedOperationException(this.getClass().getSimpleName());
    }

    public long g()
    {
        throw new UnsupportedOperationException(this.getClass().getSimpleName());
    }

    public int h()
    {
        throw new UnsupportedOperationException(this.getClass().getSimpleName());
    }

    public byte i()
    {
        throw new UnsupportedOperationException(this.getClass().getSimpleName());
    }

    public char j()
    {
        throw new UnsupportedOperationException(this.getClass().getSimpleName());
    }

    public short k()
    {
        throw new UnsupportedOperationException(this.getClass().getSimpleName());
    }

    public boolean l()
    {
        throw new UnsupportedOperationException(this.getClass().getSimpleName());
    }

    abstract com.a.c.w m();

    public final com.a.c.ac n()
    {
        if (!(this instanceof com.a.c.ac)) {
            throw new IllegalStateException("This is not a JSON Primitive.");
        } else {
            return ((com.a.c.ac) this);
        }
    }

    Boolean o()
    {
        throw new UnsupportedOperationException(this.getClass().getSimpleName());
    }

    public String toString()
    {
        try {
            java.io.IOException v0_1 = new java.io.StringWriter();
            AssertionError v1_1 = new com.a.c.d.e(v0_1);
            v1_1.c = 1;
            com.a.c.b.aq.a(this, v1_1);
            return v0_1.toString();
        } catch (java.io.IOException v0_3) {
            throw new AssertionError(v0_3);
        }
    }
}
