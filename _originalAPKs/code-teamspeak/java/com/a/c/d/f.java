package com.a.c.d;
public final class f extends java.io.IOException {
    private static final long a = 1;

    public f(String p1)
    {
        this(p1);
        return;
    }

    private f(String p1, Throwable p2)
    {
        this(p1);
        this.initCause(p2);
        return;
    }

    private f(Throwable p1)
    {
        this.initCause(p1);
        return;
    }
}
