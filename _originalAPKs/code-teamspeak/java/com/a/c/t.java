package com.a.c;
public final class t extends com.a.c.w implements java.lang.Iterable {
    private final java.util.List a;

    public t()
    {
        this.a = new java.util.ArrayList();
        return;
    }

    private com.a.c.w a(int p2)
    {
        return ((com.a.c.w) this.a.remove(p2));
    }

    private com.a.c.w a(int p2, com.a.c.w p3)
    {
        return ((com.a.c.w) this.a.set(p2, p3));
    }

    private void a(com.a.c.t p3)
    {
        this.a.addAll(p3.a);
        return;
    }

    private com.a.c.w b(int p2)
    {
        return ((com.a.c.w) this.a.get(p2));
    }

    private boolean b(com.a.c.w p2)
    {
        return this.a.remove(p2);
    }

    private boolean c(com.a.c.w p2)
    {
        return this.a.contains(p2);
    }

    private com.a.c.t p()
    {
        com.a.c.t v1_1 = new com.a.c.t();
        java.util.Iterator v2 = this.a.iterator();
        while (v2.hasNext()) {
            v1_1.a(((com.a.c.w) v2.next()).m());
        }
        return v1_1;
    }

    private int q()
    {
        return this.a.size();
    }

    public final Number a()
    {
        if (this.a.size() != 1) {
            throw new IllegalStateException();
        } else {
            return ((com.a.c.w) this.a.get(0)).a();
        }
    }

    public final void a(com.a.c.w p2)
    {
        if (p2 == null) {
            p2 = com.a.c.y.a;
        }
        this.a.add(p2);
        return;
    }

    public final String b()
    {
        if (this.a.size() != 1) {
            throw new IllegalStateException();
        } else {
            return ((com.a.c.w) this.a.get(0)).b();
        }
    }

    public final double c()
    {
        if (this.a.size() != 1) {
            throw new IllegalStateException();
        } else {
            return ((com.a.c.w) this.a.get(0)).c();
        }
    }

    public final java.math.BigDecimal d()
    {
        if (this.a.size() != 1) {
            throw new IllegalStateException();
        } else {
            return ((com.a.c.w) this.a.get(0)).d();
        }
    }

    public final java.math.BigInteger e()
    {
        if (this.a.size() != 1) {
            throw new IllegalStateException();
        } else {
            return ((com.a.c.w) this.a.get(0)).e();
        }
    }

    public final boolean equals(Object p3)
    {
        if ((p3 != this) && ((!(p3 instanceof com.a.c.t)) || (!((com.a.c.t) p3).a.equals(this.a)))) {
            int v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        return v0_3;
    }

    public final float f()
    {
        if (this.a.size() != 1) {
            throw new IllegalStateException();
        } else {
            return ((com.a.c.w) this.a.get(0)).f();
        }
    }

    public final long g()
    {
        if (this.a.size() != 1) {
            throw new IllegalStateException();
        } else {
            return ((com.a.c.w) this.a.get(0)).g();
        }
    }

    public final int h()
    {
        if (this.a.size() != 1) {
            throw new IllegalStateException();
        } else {
            return ((com.a.c.w) this.a.get(0)).h();
        }
    }

    public final int hashCode()
    {
        return this.a.hashCode();
    }

    public final byte i()
    {
        if (this.a.size() != 1) {
            throw new IllegalStateException();
        } else {
            return ((com.a.c.w) this.a.get(0)).i();
        }
    }

    public final java.util.Iterator iterator()
    {
        return this.a.iterator();
    }

    public final char j()
    {
        if (this.a.size() != 1) {
            throw new IllegalStateException();
        } else {
            return ((com.a.c.w) this.a.get(0)).j();
        }
    }

    public final short k()
    {
        if (this.a.size() != 1) {
            throw new IllegalStateException();
        } else {
            return ((com.a.c.w) this.a.get(0)).k();
        }
    }

    public final boolean l()
    {
        if (this.a.size() != 1) {
            throw new IllegalStateException();
        } else {
            return ((com.a.c.w) this.a.get(0)).l();
        }
    }

    final synthetic com.a.c.w m()
    {
        com.a.c.t v1_1 = new com.a.c.t();
        java.util.Iterator v2 = this.a.iterator();
        while (v2.hasNext()) {
            v1_1.a(((com.a.c.w) v2.next()).m());
        }
        return v1_1;
    }
}
