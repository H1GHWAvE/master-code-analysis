package com.a.c;
final class ak extends com.a.c.an {
    private final com.a.c.ae a;
    private final com.a.c.v b;
    private final com.a.c.k c;
    private final com.a.c.c.a d;
    private final com.a.c.ap e;
    private com.a.c.an f;

    private ak(com.a.c.ae p1, com.a.c.v p2, com.a.c.k p3, com.a.c.c.a p4, com.a.c.ap p5)
    {
        this.a = p1;
        this.b = p2;
        this.c = p3;
        this.d = p4;
        this.e = p5;
        return;
    }

    synthetic ak(com.a.c.ae p1, com.a.c.v p2, com.a.c.k p3, com.a.c.c.a p4, com.a.c.ap p5, byte p6)
    {
        this(p1, p2, p3, p4, p5);
        return;
    }

    private com.a.c.an a()
    {
        com.a.c.an v0_0 = this.f;
        if (v0_0 == null) {
            v0_0 = this.c.a(this.e, this.d);
            this.f = v0_0;
        }
        return v0_0;
    }

    public static com.a.c.ap a(com.a.c.c.a p6, Object p7)
    {
        return new com.a.c.am(p7, p6, 0, 0, 0);
    }

    public static com.a.c.ap a(Class p6, Object p7)
    {
        return new com.a.c.am(p7, 0, 0, p6, 0);
    }

    public static com.a.c.ap b(com.a.c.c.a p6, Object p7)
    {
        int v3;
        if (p6.b != p6.a) {
            v3 = 0;
        } else {
            v3 = 1;
        }
        return new com.a.c.am(p7, p6, v3, 0, 0);
    }

    public final Object a(com.a.c.d.a p4)
    {
        Object v0_2;
        if (this.b != null) {
            Object v0_1 = com.a.c.b.aq.a(p4);
            if (!(v0_1 instanceof com.a.c.y)) {
                v0_2 = this.b.a(v0_1, this.d.b);
            } else {
                v0_2 = 0;
            }
        } else {
            v0_2 = this.a().a(p4);
        }
        return v0_2;
    }

    public final void a(com.a.c.d.e p2, Object p3)
    {
        if (this.a != null) {
            if (p3 != null) {
                com.a.c.b.aq.a(this.a.a(p3), p2);
            } else {
                p2.f();
            }
        } else {
            this.a().a(p2, p3);
        }
        return;
    }
}
