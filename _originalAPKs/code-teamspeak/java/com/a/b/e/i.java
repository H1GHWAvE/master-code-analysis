package com.a.b.e;
public final class i {
    private static final com.a.b.e.g a;

    static i()
    {
        com.a.b.e.i.a = new com.a.b.e.j();
        return;
    }

    private i()
    {
        return;
    }

    public static com.a.b.e.l a()
    {
        return new com.a.b.e.l(0);
    }

    private static com.a.b.e.p a(com.a.b.e.d p1)
    {
        return new com.a.b.e.k(p1);
    }

    private static com.a.b.e.p a(com.a.b.e.g p4)
    {
        com.a.b.e.k v4_2;
        com.a.b.b.cn.a(p4);
        if (!(p4 instanceof com.a.b.e.p)) {
            if (!(p4 instanceof com.a.b.e.d)) {
                String v0_6;
                String v0_4 = String.valueOf(p4.getClass().getName());
                if (v0_4.length() == 0) {
                    v0_6 = new String("Cannot create a UnicodeEscaper from: ");
                } else {
                    v0_6 = "Cannot create a UnicodeEscaper from: ".concat(v0_4);
                }
                throw new IllegalArgumentException(v0_6);
            } else {
                v4_2 = new com.a.b.e.k(((com.a.b.e.d) p4));
            }
        } else {
            v4_2 = ((com.a.b.e.p) p4);
        }
        return v4_2;
    }

    private static String a(com.a.b.e.d p1, char p2)
    {
        return com.a.b.e.i.a(p1.a(p2));
    }

    private static String a(com.a.b.e.p p1, int p2)
    {
        return com.a.b.e.i.a(p1.a(p2));
    }

    private static String a(char[] p1)
    {
        String v0_1;
        if (p1 != null) {
            v0_1 = new String(p1);
        } else {
            v0_1 = 0;
        }
        return v0_1;
    }

    private static com.a.b.e.g b()
    {
        return com.a.b.e.i.a;
    }
}
