package com.a.b.e;
public abstract class p extends com.a.b.e.g {
    private static final int a = 32;

    public p()
    {
        return;
    }

    private static char[] a(char[] p2, int p3, int p4)
    {
        char[] v0 = new char[p4];
        if (p3 > 0) {
            System.arraycopy(p2, 0, v0, 0, p3);
        }
        return v0;
    }

    private static int b(CharSequence p6, int p7, int p8)
    {
        com.a.b.b.cn.a(p6);
        if (p7 >= p8) {
            throw new IndexOutOfBoundsException("Index exceeds specified range");
        } else {
            String v1_1 = (p7 + 1);
            IllegalArgumentException v0_2 = p6.charAt(p7);
            if ((v0_2 >= 55296) && (v0_2 <= 57343)) {
                if (v0_2 > 56319) {
                    String v1_2 = (v1_1 - 1);
                    String v3_1 = String.valueOf(String.valueOf(p6));
                    throw new IllegalArgumentException(new StringBuilder((v3_1.length() + 88)).append("Unexpected low surrogate character \'").append(v0_2).append("\' with value ").append(v0_2).append(" at index ").append(v1_2).append(" in \'").append(v3_1).append("\'").toString());
                } else {
                    if (v1_1 != p8) {
                        String v2_5 = p6.charAt(v1_1);
                        if (!Character.isLowSurrogate(v2_5)) {
                            String v3_4 = String.valueOf(String.valueOf(p6));
                            throw new IllegalArgumentException(new StringBuilder((v3_4.length() + 89)).append("Expected low surrogate but got char \'").append(v2_5).append("\' with value ").append(v2_5).append(" at index ").append(v1_1).append(" in \'").append(v3_4).append("\'").toString());
                        } else {
                            v0_2 = Character.toCodePoint(v0_2, v2_5);
                        }
                    } else {
                        v0_2 = (- v0_2);
                    }
                }
            }
            return v0_2;
        }
    }

    public int a(CharSequence p3, int p4, int p5)
    {
        while (p4 < p5) {
            int v0_0 = com.a.b.e.p.b(p3, p4, p5);
            if ((v0_0 < 0) || (this.a(v0_0) != null)) {
                break;
            }
            int v0_2;
            if (!Character.isSupplementaryCodePoint(v0_0)) {
                v0_2 = 1;
            } else {
                v0_2 = 2;
            }
            p4 += v0_2;
        }
        return p4;
    }

    public String a(String p3)
    {
        com.a.b.b.cn.a(p3);
        int v0 = p3.length();
        int v1_1 = this.a(p3, 0, v0);
        if (v1_1 != v0) {
            p3 = this.a(p3, v1_1);
        }
        return p3;
    }

    public final String a(String p12, int p13)
    {
        int v5 = p12.length();
        char[] v3 = com.a.b.e.n.a();
        int v2_0 = 0;
        int v0_0 = 0;
        while (p13 < v5) {
            int v1_4 = com.a.b.e.p.b(p12, p13, v5);
            if (v1_4 >= 0) {
                int v1_6;
                int v6_1 = this.a(v1_4);
                if (!Character.isSupplementaryCodePoint(v1_4)) {
                    v1_6 = 1;
                } else {
                    v1_6 = 2;
                }
                int v2_1;
                int v0_1;
                int v1_7 = (v1_6 + p13);
                if (v6_1 == 0) {
                    v2_1 = v0_0;
                    v0_1 = v2_0;
                } else {
                    int v7 = (p13 - v2_0);
                    int v8_1 = ((v0_0 + v7) + v6_1.length);
                    if (v3.length < v8_1) {
                        v3 = com.a.b.e.p.a(v3, v0_0, ((v8_1 + (v5 - p13)) + 32));
                    }
                    if (v7 > 0) {
                        p12.getChars(v2_0, p13, v3, v0_0);
                        v0_0 += v7;
                    }
                    if (v6_1.length > 0) {
                        System.arraycopy(v6_1, 0, v3, v0_0, v6_1.length);
                        v0_0 += v6_1.length;
                    }
                    v2_1 = v0_0;
                    v0_1 = v1_7;
                }
                p13 = this.a(p12, v1_7, v5);
                v0_0 = v2_1;
                v2_0 = v0_1;
            } else {
                throw new IllegalArgumentException("Trailing high surrogate at end of input");
            }
        }
        int v1_0 = (v5 - v2_0);
        if (v1_0 > 0) {
            int v1_1 = (v1_0 + v0_0);
            if (v3.length < v1_1) {
                v3 = com.a.b.e.p.a(v3, v0_0, v1_1);
            }
            p12.getChars(v2_0, v5, v3, v0_0);
            v0_0 = v1_1;
        }
        return new String(v3, 0, v0_0);
    }

    public abstract char[] a();
}
