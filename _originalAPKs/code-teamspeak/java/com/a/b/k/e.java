package com.a.b.k;
public final class e {
    final java.net.Inet4Address a;
    private final java.net.Inet4Address b;
    private final int c;
    private final int d;

    public e(java.net.Inet4Address p8, java.net.Inet4Address p9, int p10, int p11)
    {
        java.net.Inet4Address v0_0;
        if ((p10 < 0) || (p10 > 65535)) {
            v0_0 = 0;
        } else {
            v0_0 = 1;
        }
        java.net.Inet4Address v0_1;
        Integer v4_0 = new Object[1];
        v4_0[0] = Integer.valueOf(p10);
        com.a.b.b.cn.a(v0_0, "port \'%s\' is out of range (0 <= port <= 0xffff)", v4_0);
        if ((p11 < 0) || (p11 > 65535)) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        Object[] v1_1 = new Object[1];
        v1_1[0] = Integer.valueOf(p11);
        com.a.b.b.cn.a(v0_1, "flags \'%s\' is out of range (0 <= flags <= 0xffff)", v1_1);
        this.b = ((java.net.Inet4Address) com.a.b.b.ca.a(p8, com.a.b.k.d.a()));
        this.a = ((java.net.Inet4Address) com.a.b.b.ca.a(p9, com.a.b.k.d.a()));
        this.c = p10;
        this.d = p11;
        return;
    }

    private java.net.Inet4Address a()
    {
        return this.b;
    }

    private java.net.Inet4Address b()
    {
        return this.a;
    }

    private int c()
    {
        return this.c;
    }

    private int d()
    {
        return this.d;
    }
}
