package com.a.b.m;
public final class h extends com.a.b.d.gs implements com.a.b.m.ad {
    private final com.a.b.d.jt a;

    private h(com.a.b.d.jt p1)
    {
        this.a = p1;
        return;
    }

    synthetic h(com.a.b.d.jt p1, byte p2)
    {
        this(p1);
        return;
    }

    private static com.a.b.m.h b()
    {
        return new com.a.b.m.h(com.a.b.d.jt.k());
    }

    private Object b(com.a.b.m.ae p2)
    {
        return this.a.get(p2);
    }

    private static com.a.b.m.j c()
    {
        return new com.a.b.m.j(0);
    }

    public final Object a(com.a.b.m.ae p2)
    {
        return this.b(p2.c());
    }

    public final Object a(com.a.b.m.ae p2, Object p3)
    {
        throw new UnsupportedOperationException();
    }

    public final Object a(Class p2)
    {
        return this.b(com.a.b.m.ae.a(p2));
    }

    public final Object a(Class p2, Object p3)
    {
        throw new UnsupportedOperationException();
    }

    protected final java.util.Map a()
    {
        return this.a;
    }

    protected final bridge synthetic Object k_()
    {
        return this.a;
    }
}
