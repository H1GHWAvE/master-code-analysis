package com.a.b.m;
public final class n extends com.a.b.d.gs implements com.a.b.m.ad {
    private final java.util.Map a;

    public n()
    {
        this.a = com.a.b.d.sz.c();
        return;
    }

    private static Object b()
    {
        throw new UnsupportedOperationException("Please use putInstance() instead.");
    }

    private Object b(com.a.b.m.ae p2)
    {
        return this.a.get(p2);
    }

    private Object b(com.a.b.m.ae p2, Object p3)
    {
        return this.a.put(p2, p3);
    }

    public final Object a(com.a.b.m.ae p2)
    {
        return this.b(p2.c());
    }

    public final Object a(com.a.b.m.ae p2, Object p3)
    {
        return this.b(p2.c(), p3);
    }

    public final Object a(Class p2)
    {
        return this.b(com.a.b.m.ae.a(p2));
    }

    public final Object a(Class p2, Object p3)
    {
        return this.b(com.a.b.m.ae.a(p2), p3);
    }

    protected final java.util.Map a()
    {
        return this.a;
    }

    public final java.util.Set entrySet()
    {
        return com.a.b.m.p.a(super.entrySet());
    }

    protected final bridge synthetic Object k_()
    {
        return this.a;
    }

    public final synthetic Object put(Object p3, Object p4)
    {
        throw new UnsupportedOperationException("Please use putInstance() instead.");
    }

    public final void putAll(java.util.Map p3)
    {
        throw new UnsupportedOperationException("Please use putInstance() instead.");
    }
}
