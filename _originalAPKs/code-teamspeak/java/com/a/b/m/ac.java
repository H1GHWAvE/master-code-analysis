package com.a.b.m;
final class ac {
    private final java.util.concurrent.atomic.AtomicInteger a;

    private ac()
    {
        this.a = new java.util.concurrent.atomic.AtomicInteger();
        return;
    }

    synthetic ac(byte p1)
    {
        return;
    }

    private reflect.Type[] a(reflect.Type[] p4)
    {
        reflect.Type[] v1 = new reflect.Type[p4.length];
        int v0_1 = 0;
        while (v0_1 < p4.length) {
            v1[v0_1] = this.a(p4[v0_1]);
            v0_1++;
        }
        return v1;
    }

    private reflect.Type b(reflect.Type p2)
    {
        reflect.Type v0;
        if (p2 != null) {
            v0 = this.a(p2);
        } else {
            v0 = 0;
        }
        return v0;
    }

    final reflect.Type a(reflect.Type p7)
    {
        com.a.b.b.cn.a(p7);
        if ((!(p7 instanceof Class)) && (!(p7 instanceof reflect.TypeVariable))) {
            if (!(p7 instanceof reflect.GenericArrayType)) {
                if (!(p7 instanceof reflect.ParameterizedType)) {
                    if (!(p7 instanceof reflect.WildcardType)) {
                        throw new AssertionError("must have been one of the known types");
                    } else {
                        if (((reflect.WildcardType) p7).getLowerBounds().length == 0) {
                            String v1_3 = ((reflect.WildcardType) p7).getUpperBounds();
                            int v2_1 = this.a.incrementAndGet();
                            String v1_6 = String.valueOf(String.valueOf(com.a.b.b.bv.a(38).a(v1_3)));
                            p7 = com.a.b.m.ay.a(com.a.b.m.ac, new StringBuilder((v1_6.length() + 33)).append("capture#").append(v2_1).append("-of ? extends ").append(v1_6).toString(), ((reflect.WildcardType) p7).getUpperBounds());
                        }
                    }
                } else {
                    String v1_9;
                    reflect.Type[] v0_10 = ((reflect.ParameterizedType) p7).getOwnerType();
                    if (v0_10 != null) {
                        v1_9 = this.a(v0_10);
                    } else {
                        v1_9 = 0;
                    }
                    reflect.Type[] v0_14 = ((Class) ((reflect.ParameterizedType) p7).getRawType());
                    String v3_6 = ((reflect.ParameterizedType) p7).getActualTypeArguments();
                    String v4_3 = new reflect.Type[v3_6.length];
                    int v2_6 = 0;
                    while (v2_6 < v3_6.length) {
                        v4_3[v2_6] = this.a(v3_6[v2_6]);
                        v2_6++;
                    }
                    p7 = com.a.b.m.ay.a(v1_9, v0_14, v4_3);
                }
            } else {
                p7 = com.a.b.m.ay.a(this.a(((reflect.GenericArrayType) p7).getGenericComponentType()));
            }
        }
        return p7;
    }
}
