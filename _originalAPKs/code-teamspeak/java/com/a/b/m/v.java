package com.a.b.m;
public abstract class v extends com.a.b.m.u {
    final reflect.TypeVariable a;

    protected v()
    {
        reflect.TypeVariable v0_0 = this.a();
        boolean v1 = (v0_0 instanceof reflect.TypeVariable);
        Object[] v3_1 = new Object[1];
        v3_1[0] = v0_0;
        com.a.b.b.cn.a(v1, "%s should be a type variable.", v3_1);
        this.a = ((reflect.TypeVariable) v0_0);
        return;
    }

    public final boolean equals(Object p3)
    {
        int v0_1;
        if (!(p3 instanceof com.a.b.m.v)) {
            v0_1 = 0;
        } else {
            v0_1 = this.a.equals(((com.a.b.m.v) p3).a);
        }
        return v0_1;
    }

    public final int hashCode()
    {
        return this.a.hashCode();
    }

    public String toString()
    {
        return this.a.toString();
    }
}
