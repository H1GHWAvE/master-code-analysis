package com.a.b.m;
final class ak extends com.a.b.m.aw {
    private static final long e;
    final synthetic com.a.b.m.ae a;
    private final transient com.a.b.m.aw c;
    private transient com.a.b.d.lo d;

    ak(com.a.b.m.ae p1, com.a.b.m.aw p2)
    {
        this.a = p1;
        this(p1);
        this.c = p2;
        return;
    }

    private Object f()
    {
        return this.a.b().e();
    }

    protected final java.util.Set a()
    {
        com.a.b.d.lo v0_0 = this.d;
        if (v0_0 == null) {
            v0_0 = com.a.b.d.lo.a(com.a.b.d.gd.a(this.c).a(com.a.b.m.at.b).c);
            this.d = v0_0;
        }
        return v0_0;
    }

    protected final synthetic java.util.Collection b()
    {
        return this.a();
    }

    public final com.a.b.m.aw c()
    {
        throw new UnsupportedOperationException("interfaces().classes() not supported.");
    }

    public final java.util.Set d()
    {
        return com.a.b.d.lo.a(com.a.b.d.gd.a(com.a.b.m.an.b.a(com.a.b.m.ae.f(this.a.a))).a(new com.a.b.m.al(this)).c);
    }

    public final com.a.b.m.aw e()
    {
        return this;
    }

    protected final synthetic Object k_()
    {
        return this.a();
    }
}
