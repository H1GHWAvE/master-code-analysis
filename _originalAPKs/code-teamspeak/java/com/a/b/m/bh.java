package com.a.b.m;
abstract enum class bh extends java.lang.Enum {
    public static final enum com.a.b.m.bh a;
    public static final enum com.a.b.m.bh b;
    public static final enum com.a.b.m.bh c;
    static final com.a.b.m.bh d;
    private static final synthetic com.a.b.m.bh[] e;

    static bh()
    {
        com.a.b.m.bh.a = new com.a.b.m.bi("JAVA6");
        com.a.b.m.bh.b = new com.a.b.m.bj("JAVA7");
        com.a.b.m.bh.c = new com.a.b.m.bk("JAVA8");
        com.a.b.m.bh v0_7 = new com.a.b.m.bh[3];
        v0_7[0] = com.a.b.m.bh.a;
        v0_7[1] = com.a.b.m.bh.b;
        v0_7[2] = com.a.b.m.bh.c;
        com.a.b.m.bh.e = v0_7;
        if (!reflect.AnnotatedElement.isAssignableFrom(reflect.TypeVariable)) {
            if (!(new com.a.b.m.bl().a() instanceof Class)) {
                com.a.b.m.bh.d = com.a.b.m.bh.a;
            } else {
                com.a.b.m.bh.d = com.a.b.m.bh.b;
            }
        } else {
            com.a.b.m.bh.d = com.a.b.m.bh.c;
        }
        return;
    }

    private bh(String p1, int p2)
    {
        this(p1, p2);
        return;
    }

    synthetic bh(String p1, int p2, byte p3)
    {
        this(p1, p2);
        return;
    }

    public static com.a.b.m.bh valueOf(String p1)
    {
        return ((com.a.b.m.bh) Enum.valueOf(com.a.b.m.bh, p1));
    }

    public static com.a.b.m.bh[] values()
    {
        return ((com.a.b.m.bh[]) com.a.b.m.bh.e.clone());
    }

    final com.a.b.d.jl a(reflect.Type[] p5)
    {
        com.a.b.d.jn v1 = com.a.b.d.jl.h();
        int v2 = p5.length;
        com.a.b.d.jl v0_0 = 0;
        while (v0_0 < v2) {
            v1.c(this.b(p5[v0_0]));
            v0_0++;
        }
        return v1.b();
    }

    abstract reflect.Type a();

    abstract reflect.Type b();

    String c(reflect.Type p2)
    {
        return com.a.b.m.ay.b(p2);
    }
}
