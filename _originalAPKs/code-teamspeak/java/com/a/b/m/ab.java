package com.a.b.m;
final class ab {
    private final reflect.TypeVariable a;

    ab(reflect.TypeVariable p2)
    {
        this.a = ((reflect.TypeVariable) com.a.b.b.cn.a(p2));
        return;
    }

    static Object a(reflect.Type p1)
    {
        int v0_1;
        if (!(p1 instanceof reflect.TypeVariable)) {
            v0_1 = 0;
        } else {
            v0_1 = new com.a.b.m.ab(((reflect.TypeVariable) p1));
        }
        return v0_1;
    }

    private boolean a(reflect.TypeVariable p3)
    {
        if ((!this.a.getGenericDeclaration().equals(p3.getGenericDeclaration())) || (!this.a.getName().equals(p3.getName()))) {
            int v0_6 = 0;
        } else {
            v0_6 = 1;
        }
        return v0_6;
    }

    final boolean b(reflect.Type p2)
    {
        int v0_1;
        if (!(p2 instanceof reflect.TypeVariable)) {
            v0_1 = 0;
        } else {
            v0_1 = this.a(((reflect.TypeVariable) p2));
        }
        return v0_1;
    }

    public final boolean equals(Object p2)
    {
        int v0_1;
        if (!(p2 instanceof com.a.b.m.ab)) {
            v0_1 = 0;
        } else {
            v0_1 = this.a(((com.a.b.m.ab) p2).a);
        }
        return v0_1;
    }

    public final int hashCode()
    {
        int v0_1 = new Object[2];
        v0_1[0] = this.a.getGenericDeclaration();
        v0_1[1] = this.a.getName();
        return java.util.Arrays.hashCode(v0_1);
    }

    public final String toString()
    {
        return this.a.toString();
    }
}
