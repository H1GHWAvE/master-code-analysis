package com.a.b.n.a;
final class ej extends com.a.b.n.a.ce implements com.a.b.n.a.dr {
    private final java.util.concurrent.ScheduledFuture a;

    public ej(com.a.b.n.a.dp p1, java.util.concurrent.ScheduledFuture p2)
    {
        this(p1);
        this.a = p2;
        return;
    }

    private int a(java.util.concurrent.Delayed p2)
    {
        return this.a.compareTo(p2);
    }

    public final boolean cancel(boolean p3)
    {
        boolean v0 = super.cancel(p3);
        if (v0) {
            this.a.cancel(p3);
        }
        return v0;
    }

    public final bridge synthetic int compareTo(Object p2)
    {
        return this.a.compareTo(((java.util.concurrent.Delayed) p2));
    }

    public final long getDelay(java.util.concurrent.TimeUnit p3)
    {
        return this.a.getDelay(p3);
    }
}
