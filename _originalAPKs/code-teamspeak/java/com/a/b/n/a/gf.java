package com.a.b.n.a;
final class gf extends com.a.b.n.a.gi {
    final java.util.concurrent.ConcurrentMap a;
    final com.a.b.b.dz b;
    final int c;

    gf(int p3, com.a.b.b.dz p4)
    {
        java.util.concurrent.ConcurrentMap v0_2;
        this(p3);
        if (this.d != -1) {
            v0_2 = (this.d + 1);
        } else {
            v0_2 = 2147483647;
        }
        this.c = v0_2;
        this.b = p4;
        this.a = new com.a.b.d.ql().b(com.a.b.d.sh.c).e();
        return;
    }

    public final int a()
    {
        return this.c;
    }

    public final Object a(int p4)
    {
        if (this.c != 2147483647) {
            com.a.b.b.cn.a(p4, this.c);
        }
        Object v0_3 = this.a.get(Integer.valueOf(p4));
        if (v0_3 == null) {
            Object v0_5 = this.b.a();
            v0_3 = com.a.b.b.ca.a(this.a.putIfAbsent(Integer.valueOf(p4), v0_5), v0_5);
        }
        return v0_3;
    }
}
