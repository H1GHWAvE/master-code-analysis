package com.a.b.n.a;
final class ed {

    ed()
    {
        return;
    }

    private java.util.concurrent.ExecutorService a(java.util.concurrent.ThreadPoolExecutor p4)
    {
        return this.a(p4, 120, java.util.concurrent.TimeUnit.SECONDS);
    }

    private java.util.concurrent.ScheduledExecutorService a(java.util.concurrent.ScheduledThreadPoolExecutor p4)
    {
        return this.a(p4, 120, java.util.concurrent.TimeUnit.SECONDS);
    }

    private static void a(Thread p1)
    {
        Runtime.getRuntime().addShutdownHook(p1);
        return;
    }

    final java.util.concurrent.ExecutorService a(java.util.concurrent.ThreadPoolExecutor p3, long p4, java.util.concurrent.TimeUnit p6)
    {
        com.a.b.n.a.dy.a(p3);
        java.util.concurrent.ExecutorService v0 = java.util.concurrent.Executors.unconfigurableExecutorService(p3);
        this.a(v0, p4, p6);
        return v0;
    }

    final java.util.concurrent.ScheduledExecutorService a(java.util.concurrent.ScheduledThreadPoolExecutor p3, long p4, java.util.concurrent.TimeUnit p6)
    {
        com.a.b.n.a.dy.a(p3);
        java.util.concurrent.ScheduledExecutorService v0 = java.util.concurrent.Executors.unconfigurableScheduledExecutorService(p3);
        this.a(v0, p4, p6);
        return v0;
    }

    final void a(java.util.concurrent.ExecutorService p9, long p10, java.util.concurrent.TimeUnit p12)
    {
        com.a.b.b.cn.a(p9);
        com.a.b.b.cn.a(p12);
        Thread v0_1 = String.valueOf(String.valueOf(p9));
        Runtime.getRuntime().addShutdownHook(com.a.b.n.a.dy.a(new StringBuilder((v0_1.length() + 24)).append("DelayedShutdownHook-for-").append(v0_1).toString(), new com.a.b.n.a.ee(this, p9, p10, p12)));
        return;
    }
}
