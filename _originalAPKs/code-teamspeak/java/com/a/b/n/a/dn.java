package com.a.b.n.a;
final class dn extends com.a.b.n.a.cb implements com.a.b.n.a.dp {
    private static final java.util.concurrent.ThreadFactory a;
    private static final java.util.concurrent.Executor b;
    private final java.util.concurrent.Executor c;
    private final com.a.b.n.a.bu d;
    private final java.util.concurrent.atomic.AtomicBoolean e;
    private final java.util.concurrent.Future f;

    static dn()
    {
        java.util.concurrent.ExecutorService v0_2 = new com.a.b.n.a.gl().a();
        Object[] v2_1 = new Object[1];
        v2_1[0] = Integer.valueOf(0);
        String.format("ListenableFutureAdapter-thread-%d", v2_1);
        v0_2.a = "ListenableFutureAdapter-thread-%d";
        java.util.concurrent.ExecutorService v0_3 = v0_2.b();
        com.a.b.n.a.dn.a = v0_3;
        com.a.b.n.a.dn.b = java.util.concurrent.Executors.newCachedThreadPool(v0_3);
        return;
    }

    dn(java.util.concurrent.Future p2)
    {
        this(p2, com.a.b.n.a.dn.b);
        return;
    }

    dn(java.util.concurrent.Future p3, java.util.concurrent.Executor p4)
    {
        this.d = new com.a.b.n.a.bu();
        this.e = new java.util.concurrent.atomic.AtomicBoolean(0);
        this.f = ((java.util.concurrent.Future) com.a.b.b.cn.a(p3));
        this.c = ((java.util.concurrent.Executor) com.a.b.b.cn.a(p4));
        return;
    }

    static synthetic java.util.concurrent.Future a(com.a.b.n.a.dn p1)
    {
        return p1.f;
    }

    static synthetic com.a.b.n.a.bu b(com.a.b.n.a.dn p1)
    {
        return p1.d;
    }

    public final void a(Runnable p4, java.util.concurrent.Executor p5)
    {
        this.d.a(p4, p5);
        if (this.e.compareAndSet(0, 1)) {
            if (!this.f.isDone()) {
                this.c.execute(new com.a.b.n.a.do(this));
            } else {
                this.d.a();
            }
        }
        return;
    }

    protected final java.util.concurrent.Future b()
    {
        return this.f;
    }

    protected final bridge synthetic Object k_()
    {
        return this.f;
    }
}
