package com.a.b.n.a;
public final class dw {
    final java.util.concurrent.locks.ReentrantLock a;
    private final boolean b;
    private com.a.b.n.a.dx c;

    public dw()
    {
        this(0);
        return;
    }

    private dw(byte p3)
    {
        this.c = 0;
        this.b = 0;
        this.a = new java.util.concurrent.locks.ReentrantLock(0);
        return;
    }

    private static synthetic java.util.concurrent.locks.ReentrantLock a(com.a.b.n.a.dw p1)
    {
        return p1.a;
    }

    private void a(com.a.b.n.a.dx p2, boolean p3)
    {
        if (p3) {
            this.k();
        }
        this.k(p2);
        try {
            do {
                p2.c.await();
            } while(!p2.a());
            this.l(p2);
            return;
        } catch (Throwable v0_2) {
            this.l(p2);
            throw v0_2;
        }
        if (p2.a()) {
        }
        this.l(p2);
        return;
    }

    private boolean a(long p10, java.util.concurrent.TimeUnit p12)
    {
        Throwable v0_0 = 1;
        long v2_0 = p12.toNanos(p10);
        java.util.concurrent.locks.ReentrantLock v4 = this.a;
        if ((this.b) || (!v4.tryLock())) {
            long v6_1 = (System.nanoTime() + v2_0);
            Throwable v1_2 = Thread.interrupted();
            try {
                while(true) {
                    v0_0 = v4.tryLock(v2_0, java.util.concurrent.TimeUnit.NANOSECONDS);
                    try {
                        v2_0 = (v6_1 - System.nanoTime());
                        v1_2 = v0_0;
                    } catch (Throwable v1_3) {
                        v1_2 = v0_0;
                        Throwable v0_1 = v1_3;
                        if (v1_2 != null) {
                            Thread.currentThread().interrupt();
                        }
                        throw v0_1;
                    }
                }
            } catch (Throwable v0_1) {
            } catch (Throwable v1) {
            }
            if (v1_2 != null) {
                Thread.currentThread().interrupt();
            }
        }
        return v0_0;
    }

    private boolean a(com.a.b.n.a.dx p3, long p4, boolean p6)
    {
        if (p6) {
            this.k();
        }
        this.k(p3);
        while (p4 >= 0) {
            try {
                p4 = p3.c.awaitNanos(p4);
            } catch (int v0_4) {
                this.l(p3);
                throw v0_4;
            }
            if (p3.a()) {
                this.l(p3);
                int v0_5 = 1;
            }
            return v0_5;
        }
        this.l(p3);
        v0_5 = 0;
        return v0_5;
    }

    private boolean a(Thread p2)
    {
        return this.a.hasQueuedThread(p2);
    }

    private void b()
    {
        this.a.lock();
        return;
    }

    private void b(com.a.b.n.a.dx p2, boolean p3)
    {
        if (p3) {
            this.k();
        }
        this.k(p2);
        try {
            do {
                p2.c.awaitUninterruptibly();
            } while(!p2.a());
            this.l(p2);
            return;
        } catch (Throwable v0_2) {
            this.l(p2);
            throw v0_2;
        }
        if (p2.a()) {
        }
        this.l(p2);
        return;
    }

    private boolean b(long p2, java.util.concurrent.TimeUnit p4)
    {
        return this.a.tryLock(p2, p4);
    }

    private void c()
    {
        this.a.lockInterruptibly();
        return;
    }

    private void c(com.a.b.n.a.dx p3)
    {
        if (p3.b == this) {
            Throwable v0_1 = this.a;
            boolean v1 = v0_1.isHeldByCurrentThread();
            v0_1.lockInterruptibly();
            try {
                if (!p3.a()) {
                    this.a(p3, v1);
                }
            } catch (Throwable v0_3) {
                this.a();
                throw v0_3;
            }
            return;
        } else {
            throw new IllegalMonitorStateException();
        }
    }

    private boolean c(com.a.b.n.a.dx p9, long p10, java.util.concurrent.TimeUnit p12)
    {
        Throwable v2 = 0;
        Throwable v0_0 = p12.toNanos(p10);
        if (p9.b == this) {
            java.util.concurrent.locks.ReentrantLock v3_1 = this.a;
            boolean v4 = v3_1.isHeldByCurrentThread();
            if ((!this.b) && (v3_1.tryLock())) {
                try {
                    if ((!p9.a()) && (!this.a(p9, v0_0, v4))) {
                        Throwable v0_5 = 0;
                    } else {
                        v0_5 = 1;
                    }
                } catch (Throwable v0_2) {
                    if (v4) {
                        v3_1.unlock();
                        throw v0_2;
                    } else {
                        try {
                            this.k();
                        } catch (Throwable v0_3) {
                            v3_1.unlock();
                            throw v0_3;
                        }
                    }
                }
                if (v0_5 == null) {
                    v3_1.unlock();
                }
                v2 = v0_5;
            } else {
                Throwable v0_1 = (v0_0 + System.nanoTime());
                if (v3_1.tryLock(p10, p12)) {
                    v0_0 = (v0_1 - System.nanoTime());
                }
            }
            return v2;
        } else {
            throw new IllegalMonitorStateException();
        }
    }

    private boolean d()
    {
        return this.a.tryLock();
    }

    private boolean d(com.a.b.n.a.dx p3)
    {
        if (p3.b == this) {
            java.util.concurrent.locks.ReentrantLock v0_1 = this.a;
            v0_1.lockInterruptibly();
            try {
                Throwable v1_0 = p3.a();
            } catch (Throwable v1_1) {
                v0_1.unlock();
                throw v1_1;
            }
            if (v1_0 == null) {
                v0_1.unlock();
            }
            return v1_0;
        } else {
            throw new IllegalMonitorStateException();
        }
    }

    private boolean d(com.a.b.n.a.dx p3, long p4, java.util.concurrent.TimeUnit p6)
    {
        if (p3.b == this) {
            Throwable v0_2;
            if (this.a(p4, p6)) {
                try {
                    v0_2 = p3.a();
                } catch (Throwable v0_3) {
                    this.a.unlock();
                    throw v0_3;
                }
                if (v0_2 == null) {
                    this.a.unlock();
                }
            } else {
                v0_2 = 0;
            }
            return v0_2;
        } else {
            throw new IllegalMonitorStateException();
        }
    }

    private boolean e()
    {
        return this.b;
    }

    private boolean e(com.a.b.n.a.dx p3)
    {
        if (p3.b == this) {
            Throwable v0_2;
            java.util.concurrent.locks.ReentrantLock v1 = this.a;
            if (v1.tryLock()) {
                try {
                    v0_2 = p3.a();
                } catch (Throwable v0_3) {
                    v1.unlock();
                    throw v0_3;
                }
                if (v0_2 == null) {
                    v1.unlock();
                }
            } else {
                v0_2 = 0;
            }
            return v0_2;
        } else {
            throw new IllegalMonitorStateException();
        }
    }

    private boolean e(com.a.b.n.a.dx p3, long p4, java.util.concurrent.TimeUnit p6)
    {
        if (p3.b == this) {
            Throwable v0_2;
            java.util.concurrent.locks.ReentrantLock v1 = this.a;
            if (v1.tryLock(p4, p6)) {
                try {
                    v0_2 = p3.a();
                } catch (Throwable v0_3) {
                    v1.unlock();
                    throw v0_3;
                }
                if (v0_2 == null) {
                    v1.unlock();
                }
            } else {
                v0_2 = 0;
            }
            return v0_2;
        } else {
            throw new IllegalMonitorStateException();
        }
    }

    private void f(com.a.b.n.a.dx p4)
    {
        boolean v0_1;
        if (p4.b != this) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        if ((v0_1 & this.a.isHeldByCurrentThread()) != 0) {
            if (!p4.a()) {
                this.a(p4, 1);
            }
            return;
        } else {
            throw new IllegalMonitorStateException();
        }
    }

    private boolean f()
    {
        return this.a.isLocked();
    }

    private boolean f(com.a.b.n.a.dx p7, long p8, java.util.concurrent.TimeUnit p10)
    {
        boolean v0_1;
        int v2 = 0;
        long v4 = p10.toNanos(p8);
        if (p7.b != this) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        if ((v0_1 & this.a.isHeldByCurrentThread()) != 0) {
            if ((p7.a()) || (this.a(p7, v4, 1))) {
                v2 = 1;
            }
            return v2;
        } else {
            throw new IllegalMonitorStateException();
        }
    }

    private void g(com.a.b.n.a.dx p4)
    {
        boolean v0_1;
        if (p4.b != this) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        if ((v0_1 & this.a.isHeldByCurrentThread()) != 0) {
            if (!p4.a()) {
                this.b(p4, 1);
            }
            return;
        } else {
            throw new IllegalMonitorStateException();
        }
    }

    private boolean g()
    {
        return this.a.isHeldByCurrentThread();
    }

    private int h()
    {
        return this.a.getHoldCount();
    }

    private boolean h(com.a.b.n.a.dx p2)
    {
        int v0_1;
        if (this.i(p2) <= 0) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    private int i()
    {
        return this.a.getQueueLength();
    }

    private int i(com.a.b.n.a.dx p3)
    {
        if (p3.b == this) {
            this.a.lock();
            try {
                Throwable v0_2 = p3.d;
                this.a.unlock();
                return v0_2;
            } catch (Throwable v0_3) {
                this.a.unlock();
                throw v0_3;
            }
        } else {
            throw new IllegalMonitorStateException();
        }
    }

    private boolean j()
    {
        return this.a.hasQueuedThreads();
    }

    private boolean j(com.a.b.n.a.dx p4)
    {
        try {
            return p4.a();
        } catch (Throwable v1) {
            RuntimeException v0_1 = this.c;
        }
    }

    private void k()
    {
        com.a.b.n.a.dx v0_0 = this.c;
        while (v0_0 != null) {
            if (!this.j(v0_0)) {
                v0_0 = v0_0.e;
            } else {
                v0_0.c.signal();
                break;
            }
        }
        return;
    }

    private void k(com.a.b.n.a.dx p3)
    {
        com.a.b.n.a.dx v0_0 = p3.d;
        p3.d = (v0_0 + 1);
        if (v0_0 == null) {
            p3.e = this.c;
            this.c = p3;
        }
        return;
    }

    private void l()
    {
        com.a.b.n.a.dx v0 = this.c;
        while (v0 != null) {
            v0.c.signalAll();
            v0 = v0.e;
        }
        return;
    }

    private void l(com.a.b.n.a.dx p6)
    {
        com.a.b.n.a.dx v0_1 = (p6.d - 1);
        p6.d = v0_1;
        if (v0_1 == null) {
            com.a.b.n.a.dx v2 = this.c;
            com.a.b.n.a.dx v0_2 = 0;
            while (v2 != p6) {
                com.a.b.n.a.dx v4 = v2;
                v2 = v2.e;
                v0_2 = v4;
            }
            if (v0_2 != null) {
                v0_2.e = v2.e;
            } else {
                this.c = v2.e;
            }
            v2.e = 0;
        }
        return;
    }

    public final void a()
    {
        java.util.concurrent.locks.ReentrantLock v1 = this.a;
        try {
            if (v1.getHoldCount() == 1) {
                this.k();
            }
        } catch (Throwable v0_1) {
            v1.unlock();
            throw v0_1;
        }
        v1.unlock();
        return;
    }

    public final void a(com.a.b.n.a.dx p3)
    {
        if (p3.b == this) {
            Throwable v0_1 = this.a;
            boolean v1 = v0_1.isHeldByCurrentThread();
            v0_1.lock();
            try {
                if (!p3.a()) {
                    this.b(p3, v1);
                }
            } catch (Throwable v0_3) {
                this.a();
                throw v0_3;
            }
            return;
        } else {
            throw new IllegalMonitorStateException();
        }
    }

    public final boolean a(com.a.b.n.a.dx p17, long p18, java.util.concurrent.TimeUnit p20)
    {
        Throwable v4_0 = p20.toNanos(p18);
        if (p17.b == this) {
            java.util.concurrent.locks.ReentrantLock v10 = this.a;
            long v12 = (System.nanoTime() + v4_0);
            boolean v7 = v10.isHeldByCurrentThread();
            int v6_2 = Thread.interrupted();
            try {
                if ((!this.b) && (v10.tryLock())) {
                    Thread v5_1 = v6_2;
                    Throwable v4_4 = v7;
                    int v6_3 = v4_0;
                    try {
                        while ((p17.a()) || (this.a(p17, v6_3, v4_4))) {
                            Throwable v4_7 = 1;
                            if (v4_7 == null) {
                                v10.unlock();
                            }
                            if (v5_1 != null) {
                                Thread.currentThread().interrupt();
                            }
                        }
                    } catch (Throwable v4) {
                        v5_1 = 1;
                        v4_4 = 0;
                        v6_3 = (v12 - System.nanoTime());
                    } catch (Throwable v4_3) {
                        v6_2 = v5_1;
                        if (v6_2 != 0) {
                            Thread.currentThread().interrupt();
                        }
                        throw v4_3;
                    } catch (Throwable v4_5) {
                        v10.unlock();
                        throw v4_5;
                    }
                    v4_7 = 0;
                } else {
                    int v8_3 = v4_0;
                    try {
                        while(true) {
                            Throwable v4_1 = v10.tryLock(v8_3, java.util.concurrent.TimeUnit.NANOSECONDS);
                            v8_3 = v4_0;
                        }
                        if (v6_2 != 0) {
                            Thread.currentThread().interrupt();
                        }
                        v4_7 = 0;
                    } catch (Thread v5) {
                        v6_2 = 1;
                        int v8_4 = v4_1;
                        v4_0 = (v12 - System.nanoTime());
                        if (v8_4 == 0) {
                        }
                    }
                    if (v4_1 != null) {
                        v8_4 = v4_1;
                    }
                }
            } catch (Throwable v4_3) {
            }
            return v4_7;
        } else {
            throw new IllegalMonitorStateException();
        }
    }

    public final boolean b(com.a.b.n.a.dx p3)
    {
        if (p3.b == this) {
            java.util.concurrent.locks.ReentrantLock v0_1 = this.a;
            v0_1.lock();
            try {
                Throwable v1_0 = p3.a();
            } catch (Throwable v1_1) {
                v0_1.unlock();
                throw v1_1;
            }
            if (v1_0 == null) {
                v0_1.unlock();
            }
            return v1_0;
        } else {
            throw new IllegalMonitorStateException();
        }
    }

    public final boolean b(com.a.b.n.a.dx p9, long p10, java.util.concurrent.TimeUnit p12)
    {
        int v0_1;
        Thread v1_0 = 1;
        long v4_0 = p12.toNanos(p10);
        if (p9.b != this) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        if ((v0_1 & this.a.isHeldByCurrentThread()) != 0) {
            if (!p9.a()) {
                long v6_1 = (System.nanoTime() + v4_0);
                Thread v3_2 = Thread.interrupted();
                int v0_4 = 1;
                try {
                    while(true) {
                        v1_0 = this.a(p9, v4_0, v0_4);
                        v4_0 = (v6_1 - System.nanoTime());
                        v3_2 = v1_0;
                        v0_4 = 0;
                    }
                    Thread.currentThread().interrupt();
                } catch (int v0_6) {
                    v1_0 = v3_2;
                    if (v1_0 != null) {
                        Thread.currentThread().interrupt();
                    }
                    throw v0_6;
                } catch (int v0) {
                    try {
                        if (!p9.a()) {
                        }
                    } catch (int v0_6) {
                    }
                }
                if (v3_2 != null) {
                    Thread.currentThread().interrupt();
                }
            }
            return v1_0;
        } else {
            throw new IllegalMonitorStateException();
        }
    }
}
