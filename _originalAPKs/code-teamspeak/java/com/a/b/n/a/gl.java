package com.a.b.n.a;
public final class gl {
    String a;
    java.util.concurrent.ThreadFactory b;
    private Boolean c;
    private Integer d;
    private Thread$UncaughtExceptionHandler e;

    public gl()
    {
        this.a = 0;
        this.c = 0;
        this.d = 0;
        this.e = 0;
        this.b = 0;
        return;
    }

    private com.a.b.n.a.gl a(int p9)
    {
        Integer v0_0;
        if (p9 <= 0) {
            v0_0 = 0;
        } else {
            v0_0 = 1;
        }
        Integer v0_1;
        Object[] v4_0 = new Object[2];
        v4_0[0] = Integer.valueOf(p9);
        v4_0[1] = Integer.valueOf(1);
        com.a.b.b.cn.a(v0_0, "Thread priority (%s) must be >= %s", v4_0);
        if (p9 > 10) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        Object[] v4_1 = new Object[2];
        v4_1[0] = Integer.valueOf(p9);
        v4_1[1] = Integer.valueOf(10);
        com.a.b.b.cn.a(v0_1, "Thread priority (%s) must be <= %s", v4_1);
        this.d = Integer.valueOf(p9);
        return this;
    }

    private com.a.b.n.a.gl a(String p4)
    {
        Object[] v0_1 = new Object[1];
        v0_1[0] = Integer.valueOf(0);
        String.format(p4, v0_1);
        this.a = p4;
        return this;
    }

    private com.a.b.n.a.gl a(Thread$UncaughtExceptionHandler p2)
    {
        this.e = ((Thread$UncaughtExceptionHandler) com.a.b.b.cn.a(p2));
        return this;
    }

    private com.a.b.n.a.gl a(java.util.concurrent.ThreadFactory p2)
    {
        this.b = ((java.util.concurrent.ThreadFactory) com.a.b.b.cn.a(p2));
        return this;
    }

    private static java.util.concurrent.ThreadFactory a(com.a.b.n.a.gl p10)
    {
        java.util.concurrent.ThreadFactory v1;
        String v2 = p10.a;
        Boolean v4 = p10.c;
        Integer v5 = p10.d;
        Thread$UncaughtExceptionHandler v6 = p10.e;
        if (p10.b == null) {
            v1 = java.util.concurrent.Executors.defaultThreadFactory();
        } else {
            v1 = p10.b;
        }
        int v3_0;
        if (v2 == null) {
            v3_0 = 0;
        } else {
            v3_0 = new java.util.concurrent.atomic.AtomicLong(0);
        }
        return new com.a.b.n.a.gm(v1, v2, v3_0, v4, v5, v6);
    }

    public final com.a.b.n.a.gl a()
    {
        this.c = Boolean.valueOf(1);
        return this;
    }

    public final java.util.concurrent.ThreadFactory b()
    {
        java.util.concurrent.ThreadFactory v1;
        String v2 = this.a;
        Boolean v4 = this.c;
        Integer v5 = this.d;
        Thread$UncaughtExceptionHandler v6 = this.e;
        if (this.b == null) {
            v1 = java.util.concurrent.Executors.defaultThreadFactory();
        } else {
            v1 = this.b;
        }
        int v3_0;
        if (v2 == null) {
            v3_0 = 0;
        } else {
            v3_0 = new java.util.concurrent.atomic.AtomicLong(0);
        }
        return new com.a.b.n.a.gm(v1, v2, v3_0, v4, v5, v6);
    }
}
