package com.a.b.n.a;
final class gj extends com.a.b.n.a.gi {
    final java.util.concurrent.atomic.AtomicReferenceArray a;
    final com.a.b.b.dz b;
    final int c;
    final ref.ReferenceQueue e;

    gj(int p3, com.a.b.b.dz p4)
    {
        java.util.concurrent.atomic.AtomicReferenceArray v0_4;
        this(p3);
        this.e = new ref.ReferenceQueue();
        if (this.d != -1) {
            v0_4 = (this.d + 1);
        } else {
            v0_4 = 2147483647;
        }
        this.c = v0_4;
        this.a = new java.util.concurrent.atomic.AtomicReferenceArray(this.c);
        this.b = p4;
        return;
    }

    private void b()
    {
        while(true) {
            com.a.b.n.a.gk v0_1 = this.e.poll();
            if (v0_1 == null) {
                break;
            }
            com.a.b.n.a.gk v0_2 = ((com.a.b.n.a.gk) v0_1);
            this.a.compareAndSet(v0_2.a, v0_2, 0);
        }
        return;
    }

    public final int a()
    {
        return this.c;
    }

    public final Object a(int p6)
    {
        if (this.c != 2147483647) {
            com.a.b.b.cn.a(p6, this.c);
        }
        java.util.concurrent.atomic.AtomicReferenceArray v1_1;
        java.util.concurrent.atomic.AtomicReferenceArray v0_4 = ((com.a.b.n.a.gk) this.a.get(p6));
        if (v0_4 != null) {
            v1_1 = v0_4.get();
        } else {
            v1_1 = 0;
        }
        java.util.concurrent.atomic.AtomicReferenceArray v0_8;
        if (v1_1 == null) {
            Object v2 = this.b.a();
            int v4_1 = new com.a.b.n.a.gk(v2, p6, this.e);
            while (!this.a.compareAndSet(p6, v0_4, v4_1)) {
                java.util.concurrent.atomic.AtomicReferenceArray v1_6;
                v0_4 = ((com.a.b.n.a.gk) this.a.get(p6));
                if (v0_4 != null) {
                    v1_6 = v0_4.get();
                } else {
                    v1_6 = 0;
                }
                if (v1_6 != null) {
                    v0_8 = v1_6;
                }
            }
            while(true) {
                java.util.concurrent.atomic.AtomicReferenceArray v0_7 = this.e.poll();
                if (v0_7 == null) {
                    break;
                }
                java.util.concurrent.atomic.AtomicReferenceArray v0_9 = ((com.a.b.n.a.gk) v0_7);
                this.a.compareAndSet(v0_9.a, v0_9, 0);
            }
            v0_8 = v2;
        } else {
            v0_8 = v1_1;
        }
        return v0_8;
    }
}
