package com.a.b.n.a;
public abstract class ca extends com.a.b.d.hg implements java.util.concurrent.ExecutorService {

    protected ca()
    {
        return;
    }

    protected abstract java.util.concurrent.ExecutorService a();

    public boolean awaitTermination(long p2, java.util.concurrent.TimeUnit p4)
    {
        return this.a().awaitTermination(p2, p4);
    }

    public void execute(Runnable p2)
    {
        this.a().execute(p2);
        return;
    }

    public java.util.List invokeAll(java.util.Collection p2)
    {
        return this.a().invokeAll(p2);
    }

    public java.util.List invokeAll(java.util.Collection p3, long p4, java.util.concurrent.TimeUnit p6)
    {
        return this.a().invokeAll(p3, p4, p6);
    }

    public Object invokeAny(java.util.Collection p2)
    {
        return this.a().invokeAny(p2);
    }

    public Object invokeAny(java.util.Collection p3, long p4, java.util.concurrent.TimeUnit p6)
    {
        return this.a().invokeAny(p3, p4, p6);
    }

    public boolean isShutdown()
    {
        return this.a().isShutdown();
    }

    public boolean isTerminated()
    {
        return this.a().isTerminated();
    }

    protected synthetic Object k_()
    {
        return this.a();
    }

    public void shutdown()
    {
        this.a().shutdown();
        return;
    }

    public java.util.List shutdownNow()
    {
        return this.a().shutdownNow();
    }

    public java.util.concurrent.Future submit(Runnable p2)
    {
        return this.a().submit(p2);
    }

    public java.util.concurrent.Future submit(Runnable p2, Object p3)
    {
        return this.a().submit(p2, p3);
    }

    public java.util.concurrent.Future submit(java.util.concurrent.Callable p2)
    {
        return this.a().submit(p2);
    }
}
