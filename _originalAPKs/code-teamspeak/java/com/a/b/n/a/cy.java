package com.a.b.n.a;
final class cy extends com.a.b.n.a.g {
    private volatile com.a.b.n.a.dp b;

    cy(com.a.b.n.a.dp p3, com.a.b.n.a.ch p4, java.util.concurrent.Executor p5)
    {
        this.b = p3;
        com.a.b.n.a.ci.a(this.b, new com.a.b.n.a.cz(this, p4), p5);
        return;
    }

    static synthetic com.a.b.n.a.dp a(com.a.b.n.a.cy p1)
    {
        return p1.b;
    }

    static synthetic com.a.b.n.a.dp a(com.a.b.n.a.cy p0, com.a.b.n.a.dp p1)
    {
        p0.b = p1;
        return p1;
    }

    public final boolean cancel(boolean p2)
    {
        int v0_1;
        if (!super.cancel(p2)) {
            v0_1 = 0;
        } else {
            this.b.cancel(p2);
            v0_1 = 1;
        }
        return v0_1;
    }
}
