package com.a.b.n.a;
final class fs implements java.lang.reflect.InvocationHandler {
    final synthetic Object a;
    final synthetic long b;
    final synthetic java.util.concurrent.TimeUnit c;
    final synthetic java.util.Set d;
    final synthetic com.a.b.n.a.fr e;

    fs(com.a.b.n.a.fr p2, Object p3, long p4, java.util.concurrent.TimeUnit p6, java.util.Set p7)
    {
        this.e = p2;
        this.a = p3;
        this.b = p4;
        this.c = p6;
        this.d = p7;
        return;
    }

    public final Object invoke(Object p7, reflect.Method p8, Object[] p9)
    {
        return this.e.a(new com.a.b.n.a.ft(this, p8, p9), this.b, this.c, this.d.contains(p8));
    }
}
