package com.a.b.n.a;
public abstract class o extends java.util.concurrent.AbstractExecutorService implements com.a.b.n.a.du {

    public o()
    {
        return;
    }

    private static com.a.b.n.a.dq b(Runnable p1, Object p2)
    {
        return com.a.b.n.a.dq.a(p1, p2);
    }

    private static com.a.b.n.a.dq b(java.util.concurrent.Callable p1)
    {
        return com.a.b.n.a.dq.a(p1);
    }

    public final com.a.b.n.a.dp a(Runnable p2)
    {
        return ((com.a.b.n.a.dp) super.submit(p2));
    }

    public final com.a.b.n.a.dp a(Runnable p2, Object p3)
    {
        return ((com.a.b.n.a.dp) super.submit(p2, p3));
    }

    public final com.a.b.n.a.dp a(java.util.concurrent.Callable p2)
    {
        return ((com.a.b.n.a.dp) super.submit(p2));
    }

    protected synthetic java.util.concurrent.RunnableFuture newTaskFor(Runnable p2, Object p3)
    {
        return com.a.b.n.a.dq.a(p2, p3);
    }

    protected synthetic java.util.concurrent.RunnableFuture newTaskFor(java.util.concurrent.Callable p2)
    {
        return com.a.b.n.a.dq.a(p2);
    }

    public synthetic java.util.concurrent.Future submit(Runnable p2)
    {
        return this.a(p2);
    }

    public synthetic java.util.concurrent.Future submit(Runnable p2, Object p3)
    {
        return this.a(p2, p3);
    }

    public synthetic java.util.concurrent.Future submit(java.util.concurrent.Callable p2)
    {
        return this.a(p2);
    }
}
