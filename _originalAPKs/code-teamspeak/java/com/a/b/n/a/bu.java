package com.a.b.n.a;
public final class bu {
    static final java.util.logging.Logger a;
    private com.a.b.n.a.bv b;
    private boolean c;

    static bu()
    {
        com.a.b.n.a.bu.a = java.util.logging.Logger.getLogger(com.a.b.n.a.bu.getName());
        return;
    }

    public bu()
    {
        return;
    }

    private static void b(Runnable p8, java.util.concurrent.Executor p9)
    {
        try {
            p9.execute(p8);
        } catch (RuntimeException v0) {
            String v3_1 = String.valueOf(String.valueOf(p8));
            String v4_1 = String.valueOf(String.valueOf(p9));
            com.a.b.n.a.bu.a.log(java.util.logging.Level.SEVERE, new StringBuilder(((v3_1.length() + 57) + v4_1.length())).append("RuntimeException while executing runnable ").append(v3_1).append(" with executor ").append(v4_1).toString(), v0);
        }
        return;
    }

    public final void a()
    {
        com.a.b.n.a.bv v0_0 = 0;
        if (!this.c) {
            this.c = 1;
            Runnable v1_2 = this.b;
            this.b = 0;
            while (v1_2 != null) {
                java.util.concurrent.Executor v2_2 = v1_2.c;
                v1_2.c = v0_0;
                v0_0 = v1_2;
                v1_2 = v2_2;
            }
            while (v0_0 != null) {
                com.a.b.n.a.bu.b(v0_0.a, v0_0.b);
                v0_0 = v0_0.c;
            }
        } else {
        }
        return;
    }

    public final void a(Runnable p3, java.util.concurrent.Executor p4)
    {
        com.a.b.b.cn.a(p3, "Runnable was null.");
        com.a.b.b.cn.a(p4, "Executor was null.");
        if (this.c) {
            com.a.b.n.a.bu.b(p3, p4);
        } else {
            this.b = new com.a.b.n.a.bv(p3, p4, this.b);
        }
        return;
    }
}
