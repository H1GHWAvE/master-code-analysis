package com.a.b.n.a;
public abstract enum class bm extends java.lang.Enum implements com.a.b.n.a.bq {
    public static final enum com.a.b.n.a.bm a;
    public static final enum com.a.b.n.a.bm b;
    public static final enum com.a.b.n.a.bm c;
    private static final synthetic com.a.b.n.a.bm[] d;

    static bm()
    {
        com.a.b.n.a.bm.a = new com.a.b.n.a.bn("THROW");
        com.a.b.n.a.bm.b = new com.a.b.n.a.bo("WARN");
        com.a.b.n.a.bm.c = new com.a.b.n.a.bp("DISABLED");
        com.a.b.n.a.bm[] v0_7 = new com.a.b.n.a.bm[3];
        v0_7[0] = com.a.b.n.a.bm.a;
        v0_7[1] = com.a.b.n.a.bm.b;
        v0_7[2] = com.a.b.n.a.bm.c;
        com.a.b.n.a.bm.d = v0_7;
        return;
    }

    private bm(String p1, int p2)
    {
        this(p1, p2);
        return;
    }

    synthetic bm(String p1, int p2, byte p3)
    {
        this(p1, p2);
        return;
    }

    public static com.a.b.n.a.bm valueOf(String p1)
    {
        return ((com.a.b.n.a.bm) Enum.valueOf(com.a.b.n.a.bm, p1));
    }

    public static com.a.b.n.a.bm[] values()
    {
        return ((com.a.b.n.a.bm[]) com.a.b.n.a.bm.d.clone());
    }
}
