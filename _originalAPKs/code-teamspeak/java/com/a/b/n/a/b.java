package com.a.b.n.a;
public abstract class b implements com.a.b.n.a.et {
    private static final java.util.logging.Logger a;
    private final com.a.b.n.a.et b;

    static b()
    {
        com.a.b.n.a.b.a = java.util.logging.Logger.getLogger(com.a.b.n.a.b.getName());
        return;
    }

    protected b()
    {
        this.b = new com.a.b.n.a.c(this);
        return;
    }

    protected static void a()
    {
        return;
    }

    protected static void c()
    {
        return;
    }

    protected static void d()
    {
        return;
    }

    static synthetic java.util.logging.Logger l()
    {
        return com.a.b.n.a.b.a;
    }

    private java.util.concurrent.Executor m()
    {
        return new com.a.b.n.a.f(this);
    }

    private String n()
    {
        return this.getClass().getSimpleName();
    }

    public final void a(long p2, java.util.concurrent.TimeUnit p4)
    {
        this.b.a(p2, p4);
        return;
    }

    public final void a(com.a.b.n.a.ev p2, java.util.concurrent.Executor p3)
    {
        this.b.a(p2, p3);
        return;
    }

    protected abstract void b();

    public final void b(long p2, java.util.concurrent.TimeUnit p4)
    {
        this.b.b(p2, p4);
        return;
    }

    public final boolean e()
    {
        return this.b.e();
    }

    public final com.a.b.n.a.ew f()
    {
        return this.b.f();
    }

    public final Throwable g()
    {
        return this.b.g();
    }

    public final com.a.b.n.a.et h()
    {
        this.b.h();
        return this;
    }

    public final com.a.b.n.a.et i()
    {
        this.b.i();
        return this;
    }

    public final void j()
    {
        this.b.j();
        return;
    }

    public final void k()
    {
        this.b.k();
        return;
    }

    public String toString()
    {
        String v0_3 = String.valueOf(String.valueOf(this.getClass().getSimpleName()));
        String v1_2 = String.valueOf(String.valueOf(this.f()));
        return new StringBuilder(((v0_3.length() + 3) + v1_2.length())).append(v0_3).append(" [").append(v1_2).append("]").toString();
    }
}
