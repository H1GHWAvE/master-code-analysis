package com.a.b.n.a;
abstract class fu extends com.a.b.n.a.el {
    double a;
    double b;
    double c;
    private long d;

    private fu(com.a.b.n.a.em p3)
    {
        this(p3);
        this.d = 0;
        return;
    }

    synthetic fu(com.a.b.n.a.em p1, byte p2)
    {
        this(p1);
        return;
    }

    private void a(long p10)
    {
        if (p10 > this.d) {
            this.a = Math.min(this.b, (this.a + (((double) (p10 - this.d)) / this.c)));
            this.d = p10;
        }
        return;
    }

    final double a()
    {
        return (((double) java.util.concurrent.TimeUnit.SECONDS.toMicros(1)) / this.c);
    }

    final long a(int p11, long p12)
    {
        this.a(p12);
        long v0 = this.d;
        double v2_1 = Math.min(((double) p11), this.a);
        this.d = ((((long) ((((double) p11) - v2_1) * this.c)) + this.b(this.a, v2_1)) + this.d);
        this.a = (this.a - v2_1);
        return v0;
    }

    abstract void a();

    final void a(double p6, long p8)
    {
        this.a(p8);
        double v0_3 = (((double) java.util.concurrent.TimeUnit.SECONDS.toMicros(1)) / p6);
        this.c = v0_3;
        this.a(p6, v0_3);
        return;
    }

    final long b()
    {
        return this.d;
    }

    abstract long b();
}
