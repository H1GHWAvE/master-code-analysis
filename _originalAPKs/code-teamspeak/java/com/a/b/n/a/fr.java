package com.a.b.n.a;
public final class fr implements com.a.b.n.a.gn {
    private final java.util.concurrent.ExecutorService a;

    public fr()
    {
        this(java.util.concurrent.Executors.newCachedThreadPool());
        return;
    }

    private fr(java.util.concurrent.ExecutorService p2)
    {
        this.a = ((java.util.concurrent.ExecutorService) com.a.b.b.cn.a(p2));
        return;
    }

    private static synthetic Exception a(Exception p1)
    {
        return com.a.b.n.a.fr.a(p1, 0);
    }

    static Exception a(Exception p4, boolean p5)
    {
        Error v1_0 = p4.getCause();
        if (v1_0 != null) {
            if (p5) {
                v1_0.setStackTrace(((StackTraceElement[]) com.a.b.d.yc.a(v1_0.getStackTrace(), p4.getStackTrace(), StackTraceElement)));
            }
            if (!(v1_0 instanceof Exception)) {
                if (!(v1_0 instanceof Error)) {
                    throw p4;
                } else {
                    throw ((Error) v1_0);
                }
            } else {
                throw ((Exception) v1_0);
            }
        } else {
            throw p4;
        }
    }

    private static Object a(Class p3, reflect.InvocationHandler p4)
    {
        Object v0_0 = p3.getClassLoader();
        Class[] v1_1 = new Class[1];
        v1_1[0] = p3;
        return p3.cast(reflect.Proxy.newProxyInstance(v0_0, v1_1, p4));
    }

    private static java.util.Set a(Class p11)
    {
        java.util.HashSet v3_1 = new java.util.HashSet();
        reflect.Method[] v4 = p11.getMethods();
        int v5 = v4.length;
        int v2 = 0;
        while (v2 < v5) {
            reflect.Method v6 = v4[v2];
            Class[] v7 = v6.getExceptionTypes();
            int v0_0 = 0;
            while (v0_0 < v7.length) {
                if (v7[v0_0] != InterruptedException) {
                    v0_0++;
                } else {
                    int v0_1 = 1;
                }
                if (v0_1 != 0) {
                    v3_1.add(v6);
                }
                v2++;
            }
            v0_1 = 0;
        }
        return v3_1;
    }

    private static boolean a(reflect.Method p6)
    {
        int v0 = 0;
        Class[] v2 = p6.getExceptionTypes();
        int v1 = 0;
        while (v1 < v2.length) {
            if (v2[v1] != InterruptedException) {
                v1++;
            } else {
                v0 = 1;
                break;
            }
        }
        return v0;
    }

    public final Object a(Object p12, Class p13, long p14, java.util.concurrent.TimeUnit p16)
    {
        int v0_2;
        com.a.b.b.cn.a(p12);
        com.a.b.b.cn.a(p13);
        com.a.b.b.cn.a(p16);
        if (p14 <= 0) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        Class[] v2_1 = new Object[1];
        v2_1[0] = Long.valueOf(p14);
        com.a.b.b.cn.a(v0_2, "bad timeout: %s", v2_1);
        com.a.b.b.cn.a(p13.isInterface(), "interfaceType must be an interface type");
        java.util.HashSet v7_1 = new java.util.HashSet();
        Class[] v2_2 = p13.getMethods();
        int v3_1 = v2_2.length;
        int v1_2 = 0;
        while (v1_2 < v3_1) {
            long v4_2 = v2_2[v1_2];
            Class[] v5 = v4_2.getExceptionTypes();
            int v0_8 = 0;
            while (v0_8 < v5.length) {
                if (v5[v0_8] != InterruptedException) {
                    v0_8++;
                } else {
                    int v0_9 = 1;
                }
                if (v0_9 != 0) {
                    v7_1.add(v4_2);
                }
                v1_2++;
            }
            v0_9 = 0;
        }
        int v1_4 = new com.a.b.n.a.fs(this, p12, p14, p16, v7_1);
        int v0_5 = p13.getClassLoader();
        Class[] v2_5 = new Class[1];
        v2_5[0] = p13;
        return p13.cast(reflect.Proxy.newProxyInstance(v0_5, v2_5, v1_4));
    }

    public final Object a(java.util.concurrent.Callable p7, long p8, java.util.concurrent.TimeUnit p10, boolean p11)
    {
        java.util.concurrent.TimeoutException v0_1;
        com.a.b.b.cn.a(p7);
        com.a.b.b.cn.a(p10);
        if (p8 <= 0) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        java.util.concurrent.TimeoutException v0_3;
        Object[] v4_1 = new Object[1];
        v4_1[0] = Long.valueOf(p8);
        com.a.b.b.cn.a(v0_1, "timeout must be positive: %s", v4_1);
        java.util.concurrent.Future v2_1 = this.a.submit(p7);
        try {
            if (!p11) {
                v0_3 = com.a.b.n.a.gs.a(v2_1, p8, p10);
            } else {
                try {
                    v0_3 = v2_1.get(p8, p10);
                } catch (java.util.concurrent.TimeoutException v0_4) {
                    v2_1.cancel(1);
                    throw v0_4;
                }
            }
        } catch (java.util.concurrent.TimeoutException v0_5) {
            v2_1.cancel(1);
            throw new com.a.b.n.a.gr(v0_5);
        } catch (java.util.concurrent.TimeoutException v0_6) {
            throw com.a.b.n.a.fr.a(v0_6, 1);
        }
        return v0_3;
    }
}
