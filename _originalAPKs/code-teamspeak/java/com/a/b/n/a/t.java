package com.a.b.n.a;
final class t implements java.lang.Runnable {
    final synthetic com.a.b.n.a.q a;

    t(com.a.b.n.a.q p1)
    {
        this.a = p1;
        return;
    }

    public final void run()
    {
        com.a.b.n.a.q.a(this.a).lock();
        try {
            com.a.b.n.a.p.b();
            com.a.b.n.a.q.a(this.a, this.a.a.d().a(com.a.b.n.a.p.a(this.a.a), com.a.b.n.a.q.b(this.a), com.a.b.n.a.q.c(this.a)));
            this.a.c();
            com.a.b.n.a.q.a(this.a).unlock();
            return;
        } catch (RuntimeException v0_9) {
            com.a.b.n.a.q.a(this.a).unlock();
            throw v0_9;
        } catch (RuntimeException v0_7) {
            this.a.a(v0_7);
            throw com.a.b.b.ei.a(v0_7);
        }
    }
}
