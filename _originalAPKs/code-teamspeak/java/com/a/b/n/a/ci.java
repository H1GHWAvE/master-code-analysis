package com.a.b.n.a;
public final class ci {
    private static final com.a.b.n.a.ap a;
    private static final com.a.b.d.yd b;

    static ci()
    {
        com.a.b.n.a.ci.a = new com.a.b.n.a.cn();
        com.a.b.n.a.ci.b = com.a.b.d.yd.d().a(new com.a.b.n.a.cq()).a();
        return;
    }

    private ci()
    {
        return;
    }

    private static com.a.b.n.a.ap a(com.a.b.b.bj p1)
    {
        return new com.a.b.n.a.cl(p1);
    }

    private static com.a.b.n.a.bc a(Exception p1)
    {
        com.a.b.b.cn.a(p1);
        return new com.a.b.n.a.dd(p1);
    }

    private static com.a.b.n.a.dp a()
    {
        return new com.a.b.n.a.dc();
    }

    private static com.a.b.n.a.dp a(com.a.b.d.jl p2, boolean p3, java.util.concurrent.Executor p4)
    {
        return new com.a.b.n.a.cu(p2, p3, p4, new com.a.b.n.a.cr());
    }

    public static com.a.b.n.a.dp a(com.a.b.n.a.dp p3)
    {
        com.a.b.n.a.cs v1_1 = new com.a.b.n.a.cs(com.a.b.n.a.ci.a, p3, 0);
        p3.a(v1_1, com.a.b.n.a.ef.a);
        return v1_1;
    }

    public static com.a.b.n.a.dp a(com.a.b.n.a.dp p3, com.a.b.b.bj p4)
    {
        com.a.b.b.cn.a(p4);
        com.a.b.n.a.cs v0_1 = new com.a.b.n.a.cs(com.a.b.n.a.ci.a(p4), p3, 0);
        p3.a(v0_1, com.a.b.n.a.ef.a);
        return v0_1;
    }

    private static com.a.b.n.a.dp a(com.a.b.n.a.dp p3, com.a.b.b.bj p4, java.util.concurrent.Executor p5)
    {
        com.a.b.b.cn.a(p4);
        com.a.b.n.a.cj v0_0 = com.a.b.n.a.ci.a(p4);
        com.a.b.b.cn.a(p5);
        com.a.b.n.a.cs v1_1 = new com.a.b.n.a.cs(v0_0, p3, 0);
        p3.a(new com.a.b.n.a.cj(p5, v1_1, v1_1), com.a.b.n.a.ef.a);
        return v1_1;
    }

    private static com.a.b.n.a.dp a(com.a.b.n.a.dp p2, com.a.b.n.a.ap p3)
    {
        com.a.b.n.a.cs v0_1 = new com.a.b.n.a.cs(p3, p2, 0);
        p2.a(v0_1, com.a.b.n.a.ef.a);
        return v0_1;
    }

    private static com.a.b.n.a.dp a(com.a.b.n.a.dp p3, com.a.b.n.a.ap p4, java.util.concurrent.Executor p5)
    {
        com.a.b.b.cn.a(p5);
        com.a.b.n.a.cs v0_1 = new com.a.b.n.a.cs(p4, p3, 0);
        p3.a(new com.a.b.n.a.cj(p5, v0_1, v0_1), com.a.b.n.a.ef.a);
        return v0_1;
    }

    private static com.a.b.n.a.dp a(com.a.b.n.a.dp p2, com.a.b.n.a.ch p3)
    {
        com.a.b.b.cn.a(p3);
        return new com.a.b.n.a.cy(p2, p3, com.a.b.n.a.ef.a);
    }

    private static com.a.b.n.a.dp a(com.a.b.n.a.dp p1, com.a.b.n.a.ch p2, java.util.concurrent.Executor p3)
    {
        com.a.b.b.cn.a(p2);
        return new com.a.b.n.a.cy(p1, p2, p3);
    }

    private static com.a.b.n.a.dp a(Iterable p3)
    {
        return com.a.b.n.a.ci.a(com.a.b.d.jl.a(p3), 1, com.a.b.n.a.ef.a);
    }

    public static com.a.b.n.a.dp a(Object p1)
    {
        return new com.a.b.n.a.dh(p1);
    }

    public static com.a.b.n.a.dp a(Throwable p1)
    {
        com.a.b.b.cn.a(p1);
        return new com.a.b.n.a.de(p1);
    }

    private static varargs com.a.b.n.a.dp a(com.a.b.n.a.dp[] p3)
    {
        return com.a.b.n.a.ci.a(com.a.b.d.jl.a(p3), 1, com.a.b.n.a.ef.a);
    }

    private static Exception a(Class p4, Throwable p5)
    {
        Throwable v1_1 = com.a.b.n.a.ci.b.a(java.util.Arrays.asList(p4.getConstructors())).iterator();
        while (v1_1.hasNext()) {
            IllegalArgumentException v0_9 = ((Exception) com.a.b.n.a.ci.a(((reflect.Constructor) v1_1.next()), p5));
            if (v0_9 != null) {
                if (v0_9.getCause() == null) {
                    v0_9.initCause(p5);
                }
                return v0_9;
            }
        }
        Throwable v1_3 = String.valueOf(String.valueOf(p4));
        throw new IllegalArgumentException(new StringBuilder((v1_3.length() + 82)).append("No appropriate constructor for exception of type ").append(v1_3).append(" in response to chained exception").toString(), p5);
    }

    private static Object a(reflect.Constructor p6, Throwable p7)
    {
        Class[] v2 = p6.getParameterTypes();
        Object[] v3 = new Object[v2.length];
        int v0_1 = 0;
        while (v0_1 < v2.length) {
            boolean v4_1 = v2[v0_1];
            if (!v4_1.equals(String)) {
                if (!v4_1.equals(Throwable)) {
                    int v0_2 = 0;
                    return v0_2;
                } else {
                    v3[v0_1] = p7;
                }
            } else {
                v3[v0_1] = p7.toString();
            }
            v0_1++;
        }
        try {
            v0_2 = p6.newInstance(v3);
        } catch (int v0) {
            v0_2 = 0;
        } catch (int v0) {
            v0_2 = 0;
        } catch (int v0) {
            v0_2 = 0;
        } catch (int v0) {
            v0_2 = 0;
        }
        return v0_2;
    }

    private static Object a(java.util.concurrent.Future p2)
    {
        com.a.b.b.cn.a(p2);
        try {
            return com.a.b.n.a.gs.a(p2);
        } catch (Error v0_1) {
            Error v0_2 = v0_1.getCause();
            if (!(v0_2 instanceof Error)) {
                throw new com.a.b.n.a.gq(v0_2);
            } else {
                throw new com.a.b.n.a.bt(((Error) v0_2));
            }
        }
    }

    private static Object a(java.util.concurrent.Future p5, long p6, java.util.concurrent.TimeUnit p8, Class p9)
    {
        AssertionError v0_2;
        com.a.b.b.cn.a(p5);
        com.a.b.b.cn.a(p8);
        if (RuntimeException.isAssignableFrom(p9)) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        Thread v1_1 = new Object[1];
        v1_1[0] = p9;
        com.a.b.b.cn.a(v0_2, "Futures.get exception type (%s) must not be a RuntimeException", v1_1);
        try {
            return p5.get(p6, p8);
        } catch (AssertionError v0_4) {
            com.a.b.n.a.ci.a(v0_4.getCause(), p9);
            throw new AssertionError();
        } catch (AssertionError v0_10) {
            Thread.currentThread().interrupt();
            throw com.a.b.n.a.ci.a(p9, v0_10);
        } catch (AssertionError v0_8) {
            throw com.a.b.n.a.ci.a(p9, v0_8);
        }
    }

    private static Object a(java.util.concurrent.Future p4, Class p5)
    {
        AssertionError v0_2;
        com.a.b.b.cn.a(p4);
        if (RuntimeException.isAssignableFrom(p5)) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        Thread v1_1 = new Object[1];
        v1_1[0] = p5;
        com.a.b.b.cn.a(v0_2, "Futures.get exception type (%s) must not be a RuntimeException", v1_1);
        try {
            return p4.get();
        } catch (AssertionError v0_4) {
            com.a.b.n.a.ci.a(v0_4.getCause(), p5);
            throw new AssertionError();
        } catch (AssertionError v0_8) {
            Thread.currentThread().interrupt();
            throw com.a.b.n.a.ci.a(p5, v0_8);
        }
    }

    private static Runnable a(com.a.b.n.a.g p1, Runnable p2, java.util.concurrent.Executor p3)
    {
        return new com.a.b.n.a.cj(p3, p2, p1);
    }

    private static java.util.List a(java.util.List p1)
    {
        return com.a.b.n.a.ci.b.a(p1);
    }

    private static java.util.concurrent.Future a(java.util.concurrent.Future p1, com.a.b.b.bj p2)
    {
        com.a.b.b.cn.a(p1);
        com.a.b.b.cn.a(p2);
        return new com.a.b.n.a.cm(p1, p2);
    }

    private static void a(com.a.b.n.a.dp p1, com.a.b.n.a.cg p2)
    {
        com.a.b.n.a.ci.a(p1, p2, com.a.b.n.a.ef.a);
        return;
    }

    public static void a(com.a.b.n.a.dp p1, com.a.b.n.a.cg p2, java.util.concurrent.Executor p3)
    {
        com.a.b.b.cn.a(p2);
        p1.a(new com.a.b.n.a.cp(p1, p2), p3);
        return;
    }

    private static void a(Throwable p1, Class p2)
    {
        if (!(p1 instanceof Error)) {
            if (!(p1 instanceof RuntimeException)) {
                throw com.a.b.n.a.ci.a(p2, p1);
            } else {
                throw new com.a.b.n.a.gq(p1);
            }
        } else {
            throw new com.a.b.n.a.bt(((Error) p1));
        }
    }

    private static com.a.b.n.a.bc b(com.a.b.n.a.dp p2, com.a.b.b.bj p3)
    {
        return new com.a.b.n.a.di(((com.a.b.n.a.dp) com.a.b.b.cn.a(p2)), p3);
    }

    private static com.a.b.n.a.bc b(Object p1)
    {
        return new com.a.b.n.a.dg(p1);
    }

    private static com.a.b.n.a.dp b(com.a.b.n.a.dp p1)
    {
        return new com.a.b.n.a.dj(p1);
    }

    private static com.a.b.n.a.dp b(Iterable p3)
    {
        return com.a.b.n.a.ci.a(com.a.b.d.jl.a(p3), 0, com.a.b.n.a.ef.a);
    }

    private static varargs com.a.b.n.a.dp b(com.a.b.n.a.dp[] p3)
    {
        return com.a.b.n.a.ci.a(com.a.b.d.jl.a(p3), 0, com.a.b.n.a.ef.a);
    }

    private static void b(Throwable p1)
    {
        if (!(p1 instanceof Error)) {
            throw new com.a.b.n.a.gq(p1);
        } else {
            throw new com.a.b.n.a.bt(((Error) p1));
        }
    }

    private static com.a.b.d.jl c(Iterable p7)
    {
        java.util.concurrent.ConcurrentLinkedQueue v1_1 = new java.util.concurrent.ConcurrentLinkedQueue();
        com.a.b.d.jn v2 = com.a.b.d.jl.h();
        com.a.b.n.a.eq v3_1 = new com.a.b.n.a.eq(com.a.b.n.a.ef.a);
        java.util.Iterator v4 = p7.iterator();
        while (v4.hasNext()) {
            com.a.b.d.jl v0_4 = ((com.a.b.n.a.dp) v4.next());
            com.a.b.n.a.aq v5 = com.a.b.n.a.aq.a();
            v1_1.add(v5);
            v0_4.a(new com.a.b.n.a.co(v1_1, v0_4), v3_1);
            v2.c(v5);
        }
        return v2.b();
    }
}
