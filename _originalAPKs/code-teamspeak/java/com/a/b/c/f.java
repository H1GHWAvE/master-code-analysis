package com.a.b.c;
public final class f {
    static final com.a.b.b.dz a = None;
    static final com.a.b.c.ai b = None;
    static final com.a.b.b.dz c = None;
    static final com.a.b.b.ej d = None;
    static final int e = 255;
    private static final int v = 16;
    private static final int w = 4;
    private static final int x;
    private static final int y;
    private static final java.util.logging.Logger z;
    boolean f;
    int g;
    int h;
    long i;
    long j;
    com.a.b.c.do k;
    com.a.b.c.bw l;
    com.a.b.c.bw m;
    long n;
    long o;
    long p;
    com.a.b.b.au q;
    com.a.b.b.au r;
    com.a.b.c.dg s;
    com.a.b.b.ej t;
    com.a.b.b.dz u;

    static f()
    {
        com.a.b.c.f.a = new com.a.b.b.eg(new com.a.b.c.g());
        com.a.b.c.f.b = new com.a.b.c.ai(0, 0, 0, 0, 0, 0);
        com.a.b.c.f.c = new com.a.b.c.h();
        com.a.b.c.f.d = new com.a.b.c.i();
        com.a.b.c.f.z = java.util.logging.Logger.getLogger(com.a.b.c.f.getName());
        return;
    }

    f()
    {
        this.f = 1;
        this.g = -1;
        this.h = -1;
        this.i = -1;
        this.j = -1;
        this.n = -1;
        this.o = -1;
        this.p = -1;
        this.u = com.a.b.c.f.a;
        return;
    }

    private com.a.b.b.ej a(boolean p2)
    {
        com.a.b.b.ej v0_1;
        if (this.t == null) {
            if (!p2) {
                v0_1 = com.a.b.c.f.d;
            } else {
                v0_1 = com.a.b.b.ej.b();
            }
        } else {
            v0_1 = this.t;
        }
        return v0_1;
    }

    public static com.a.b.c.f a()
    {
        return new com.a.b.c.f();
    }

    private com.a.b.c.f a(com.a.b.b.au p6)
    {
        com.a.b.b.au v0_1;
        if (this.q != null) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        Object[] v1_1 = new Object[1];
        v1_1[0] = this.q;
        com.a.b.b.cn.b(v0_1, "key equivalence was already set to %s", v1_1);
        this.q = ((com.a.b.b.au) com.a.b.b.cn.a(p6));
        return this;
    }

    private com.a.b.c.f a(com.a.b.b.ej p2)
    {
        com.a.b.b.ej v0_1;
        if (this.t != null) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        com.a.b.b.cn.b(v0_1);
        this.t = ((com.a.b.b.ej) com.a.b.b.cn.a(p2));
        return this;
    }

    private com.a.b.c.f a(com.a.b.c.dg p2)
    {
        com.a.b.c.dg v0_1;
        if (this.s != null) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        com.a.b.b.cn.b(v0_1);
        this.s = ((com.a.b.c.dg) com.a.b.b.cn.a(p2));
        return this;
    }

    private com.a.b.c.f a(com.a.b.c.do p9)
    {
        int v0_1;
        if (this.k != null) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        com.a.b.b.cn.b(v0_1);
        if (this.f) {
            int v0_4;
            if (this.i != -1) {
                v0_4 = 0;
            } else {
                v0_4 = 1;
            }
            Object[] v1_1 = new Object[1];
            v1_1[0] = Long.valueOf(this.i);
            com.a.b.b.cn.b(v0_4, "weigher can not be combined with maximum size", v1_1);
        }
        this.k = ((com.a.b.c.do) com.a.b.b.cn.a(p9));
        return this;
    }

    private static com.a.b.c.f a(com.a.b.c.l p12)
    {
        com.a.b.c.f v3_1 = new com.a.b.c.f();
        if (p12.a != null) {
            long v0_3;
            long v4_0 = p12.a.intValue();
            if (v3_1.g != -1) {
                v0_3 = 0;
            } else {
                v0_3 = 1;
            }
            long v0_4;
            java.util.concurrent.TimeUnit v6_0 = new Object[1];
            v6_0[0] = Integer.valueOf(v3_1.g);
            com.a.b.b.cn.b(v0_3, "initial capacity was already set to %s", v6_0);
            if (v4_0 < 0) {
                v0_4 = 0;
            } else {
                v0_4 = 1;
            }
            com.a.b.b.cn.a(v0_4);
            v3_1.g = v4_0;
        }
        if (p12.b != null) {
            v3_1.a(p12.b.longValue());
        }
        if (p12.c != null) {
            v3_1.b(p12.c.longValue());
        }
        if (p12.d != null) {
            v3_1.a(p12.d.intValue());
        }
        if (p12.e != null) {
            switch (com.a.b.c.m.a[p12.e.ordinal()]) {
                case 1:
                    v3_1.a(com.a.b.c.bw.c);
                    break;
                default:
                    throw new AssertionError();
            }
        }
        if (p12.f != null) {
            switch (com.a.b.c.m.a[p12.f.ordinal()]) {
                case 1:
                    v3_1.b(com.a.b.c.bw.c);
                    break;
                case 2:
                    v3_1.b(com.a.b.c.bw.b);
                    break;
                default:
                    throw new AssertionError();
            }
        }
        if ((p12.g != null) && (p12.g.booleanValue())) {
            v3_1.u = com.a.b.c.f.c;
        }
        if (p12.i != null) {
            v3_1.a(p12.h, p12.i);
        }
        if (p12.k != null) {
            v3_1.b(p12.j, p12.k);
        }
        if (p12.m != null) {
            long v0_31;
            long v4_9 = p12.l;
            java.util.concurrent.TimeUnit v6_1 = p12.m;
            com.a.b.b.cn.a(v6_1);
            if (v3_1.p != -1) {
                v0_31 = 0;
            } else {
                v0_31 = 1;
            }
            long v0_33;
            Object[] v8_1 = new Object[1];
            v8_1[0] = Long.valueOf(v3_1.p);
            com.a.b.b.cn.b(v0_31, "refresh was already set to %s ns", v8_1);
            if (v4_9 <= 0) {
                v0_33 = 0;
            } else {
                v0_33 = 1;
            }
            Object[] v8_4 = new Object[2];
            v8_4[0] = Long.valueOf(v4_9);
            v8_4[1] = v6_1;
            com.a.b.b.cn.a(v0_33, "duration must be positive: %s %s", v8_4);
            v3_1.p = v6_1.toNanos(v4_9);
        }
        v3_1.f = 0;
        return v3_1;
    }

    private static com.a.b.c.f a(String p12)
    {
        java.util.concurrent.TimeUnit v3_0 = com.a.b.c.l.a(p12);
        com.a.b.c.f v4_1 = new com.a.b.c.f();
        if (v3_0.a != null) {
            long v0_3;
            String v5_0 = v3_0.a.intValue();
            if (v4_1.g != -1) {
                v0_3 = 0;
            } else {
                v0_3 = 1;
            }
            long v0_4;
            Object[] v7 = new Object[1];
            v7[0] = Integer.valueOf(v4_1.g);
            com.a.b.b.cn.b(v0_3, "initial capacity was already set to %s", v7);
            if (v5_0 < null) {
                v0_4 = 0;
            } else {
                v0_4 = 1;
            }
            com.a.b.b.cn.a(v0_4);
            v4_1.g = v5_0;
        }
        if (v3_0.b != null) {
            v4_1.a(v3_0.b.longValue());
        }
        if (v3_0.c != null) {
            v4_1.b(v3_0.c.longValue());
        }
        if (v3_0.d != null) {
            v4_1.a(v3_0.d.intValue());
        }
        if (v3_0.e != null) {
            switch (com.a.b.c.m.a[v3_0.e.ordinal()]) {
                case 1:
                    v4_1.a(com.a.b.c.bw.c);
                    break;
                default:
                    throw new AssertionError();
            }
        }
        if (v3_0.f != null) {
            switch (com.a.b.c.m.a[v3_0.f.ordinal()]) {
                case 1:
                    v4_1.b(com.a.b.c.bw.c);
                    break;
                case 2:
                    v4_1.b(com.a.b.c.bw.b);
                    break;
                default:
                    throw new AssertionError();
            }
        }
        if ((v3_0.g != null) && (v3_0.g.booleanValue())) {
            v4_1.u = com.a.b.c.f.c;
        }
        if (v3_0.i != null) {
            v4_1.a(v3_0.h, v3_0.i);
        }
        if (v3_0.k != null) {
            v4_1.b(v3_0.j, v3_0.k);
        }
        if (v3_0.m != null) {
            long v0_31;
            long v6_6 = v3_0.l;
            java.util.concurrent.TimeUnit v3_1 = v3_0.m;
            com.a.b.b.cn.a(v3_1);
            if (v4_1.p != -1) {
                v0_31 = 0;
            } else {
                v0_31 = 1;
            }
            long v0_33;
            Object[] v8_3 = new Object[1];
            v8_3[0] = Long.valueOf(v4_1.p);
            com.a.b.b.cn.b(v0_31, "refresh was already set to %s ns", v8_3);
            if (v6_6 <= 0) {
                v0_33 = 0;
            } else {
                v0_33 = 1;
            }
            Object[] v8_6 = new Object[2];
            v8_6[0] = Long.valueOf(v6_6);
            v8_6[1] = v3_1;
            com.a.b.b.cn.a(v0_33, "duration must be positive: %s %s", v8_6);
            v4_1.p = v3_1.toNanos(v6_6);
        }
        v4_1.f = 0;
        return v4_1;
    }

    private com.a.b.c.f b(int p7)
    {
        int v0_1;
        int v1 = 1;
        if (this.g != -1) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        Object[] v4 = new Object[1];
        v4[0] = Integer.valueOf(this.g);
        com.a.b.b.cn.b(v0_1, "initial capacity was already set to %s", v4);
        if (p7 < 0) {
            v1 = 0;
        }
        com.a.b.b.cn.a(v1);
        this.g = p7;
        return this;
    }

    private com.a.b.c.f b(com.a.b.b.au p6)
    {
        com.a.b.b.au v0_1;
        if (this.r != null) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        Object[] v1_1 = new Object[1];
        v1_1[0] = this.r;
        com.a.b.b.cn.b(v0_1, "value equivalence was already set to %s", v1_1);
        this.r = ((com.a.b.b.au) com.a.b.b.cn.a(p6));
        return this;
    }

    private com.a.b.c.f c(long p10, java.util.concurrent.TimeUnit p12)
    {
        long v0_1;
        com.a.b.b.cn.a(p12);
        if (this.p != -1) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        long v0_3;
        Object[] v4_1 = new Object[1];
        v4_1[0] = Long.valueOf(this.p);
        com.a.b.b.cn.b(v0_1, "refresh was already set to %s ns", v4_1);
        if (p10 <= 0) {
            v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        Object[] v4_4 = new Object[2];
        v4_4[0] = Long.valueOf(p10);
        v4_4[1] = p12;
        com.a.b.b.cn.a(v0_3, "duration must be positive: %s %s", v4_4);
        this.p = p12.toNanos(p10);
        return this;
    }

    private com.a.b.c.f e()
    {
        this.f = 0;
        return this;
    }

    private com.a.b.b.au f()
    {
        return ((com.a.b.b.au) com.a.b.b.ca.a(this.q, this.b().a()));
    }

    private com.a.b.b.au g()
    {
        return ((com.a.b.b.au) com.a.b.b.ca.a(this.r, this.c().a()));
    }

    private int h()
    {
        int v0_1;
        if (this.g != -1) {
            v0_1 = this.g;
        } else {
            v0_1 = 16;
        }
        return v0_1;
    }

    private int i()
    {
        int v0_1;
        if (this.h != -1) {
            v0_1 = this.h;
        } else {
            v0_1 = 4;
        }
        return v0_1;
    }

    private long j()
    {
        long v0_0 = 0;
        if ((this.n != 0) && (this.o != 0)) {
            if (this.k != null) {
                v0_0 = this.j;
            } else {
                v0_0 = this.i;
            }
        }
        return v0_0;
    }

    private com.a.b.c.do k()
    {
        return ((com.a.b.c.do) com.a.b.b.ca.a(this.k, com.a.b.c.k.a));
    }

    private com.a.b.c.f l()
    {
        return this.a(com.a.b.c.bw.c);
    }

    private com.a.b.c.f m()
    {
        return this.b(com.a.b.c.bw.c);
    }

    private com.a.b.c.f n()
    {
        return this.b(com.a.b.c.bw.b);
    }

    private long o()
    {
        long v0_2;
        if (this.n != -1) {
            v0_2 = this.n;
        } else {
            v0_2 = 0;
        }
        return v0_2;
    }

    private long p()
    {
        long v0_2;
        if (this.o != -1) {
            v0_2 = this.o;
        } else {
            v0_2 = 0;
        }
        return v0_2;
    }

    private long q()
    {
        long v0_2;
        if (this.p != -1) {
            v0_2 = this.p;
        } else {
            v0_2 = 0;
        }
        return v0_2;
    }

    private com.a.b.c.dg r()
    {
        return ((com.a.b.c.dg) com.a.b.b.ca.a(this.s, com.a.b.c.j.a));
    }

    private com.a.b.c.f s()
    {
        this.u = com.a.b.c.f.c;
        return this;
    }

    private boolean t()
    {
        int v0_1;
        if (this.u != com.a.b.c.f.c) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    private com.a.b.b.dz u()
    {
        return this.u;
    }

    private com.a.b.c.e v()
    {
        com.a.b.c.bo v0_2;
        this.d();
        if (this.p != -1) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        com.a.b.b.cn.b(v0_2, "refreshAfterWrite requires a LoadingCache");
        return new com.a.b.c.bo(this);
    }

    private void w()
    {
        int v0_2;
        if (this.p != -1) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        com.a.b.b.cn.b(v0_2, "refreshAfterWrite requires a LoadingCache");
        return;
    }

    public final com.a.b.c.an a(com.a.b.c.ab p2)
    {
        this.d();
        return new com.a.b.c.bn(this, p2);
    }

    public final com.a.b.c.f a(int p7)
    {
        int v0_1;
        int v1 = 1;
        if (this.h != -1) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        Object[] v4 = new Object[1];
        v4[0] = Integer.valueOf(this.h);
        com.a.b.b.cn.b(v0_1, "concurrency level was already set to %s", v4);
        if (p7 <= 0) {
            v1 = 0;
        }
        com.a.b.b.cn.a(v1);
        this.h = p7;
        return this;
    }

    public final com.a.b.c.f a(long p12)
    {
        String v0_1;
        int v1 = 1;
        if (this.i != -1) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        String v0_3;
        long v4_1 = new Object[1];
        v4_1[0] = Long.valueOf(this.i);
        com.a.b.b.cn.b(v0_1, "maximum size was already set to %s", v4_1);
        if (this.j != -1) {
            v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        String v0_5;
        long v4_3 = new Object[1];
        v4_3[0] = Long.valueOf(this.j);
        com.a.b.b.cn.b(v0_3, "maximum weight was already set to %s", v4_3);
        if (this.k != null) {
            v0_5 = 0;
        } else {
            v0_5 = 1;
        }
        com.a.b.b.cn.b(v0_5, "maximum size can not be combined with weigher");
        if (p12 < 0) {
            v1 = 0;
        }
        com.a.b.b.cn.a(v1, "maximum size must not be negative");
        this.i = p12;
        return this;
    }

    public final com.a.b.c.f a(long p10, java.util.concurrent.TimeUnit p12)
    {
        long v0_1;
        if (this.n != -1) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        long v0_3;
        Object[] v4_1 = new Object[1];
        v4_1[0] = Long.valueOf(this.n);
        com.a.b.b.cn.b(v0_1, "expireAfterWrite was already set to %s ns", v4_1);
        if (p10 < 0) {
            v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        Object[] v4_4 = new Object[2];
        v4_4[0] = Long.valueOf(p10);
        v4_4[1] = p12;
        com.a.b.b.cn.a(v0_3, "duration cannot be negative: %s %s", v4_4);
        this.n = p12.toNanos(p10);
        return this;
    }

    public final com.a.b.c.f a(com.a.b.c.bw p6)
    {
        com.a.b.c.bw v0_1;
        if (this.l != null) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        Object[] v1_1 = new Object[1];
        v1_1[0] = this.l;
        com.a.b.b.cn.b(v0_1, "Key strength was already set to %s", v1_1);
        this.l = ((com.a.b.c.bw) com.a.b.b.cn.a(p6));
        return this;
    }

    final com.a.b.c.bw b()
    {
        return ((com.a.b.c.bw) com.a.b.b.ca.a(this.l, com.a.b.c.bw.a));
    }

    public final com.a.b.c.f b(long p12)
    {
        String v0_1;
        int v1 = 1;
        if (this.j != -1) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        String v0_3;
        long v4_1 = new Object[1];
        v4_1[0] = Long.valueOf(this.j);
        com.a.b.b.cn.b(v0_1, "maximum weight was already set to %s", v4_1);
        if (this.i != -1) {
            v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        long v4_3 = new Object[1];
        v4_3[0] = Long.valueOf(this.i);
        com.a.b.b.cn.b(v0_3, "maximum size was already set to %s", v4_3);
        this.j = p12;
        if (p12 < 0) {
            v1 = 0;
        }
        com.a.b.b.cn.a(v1, "maximum weight must not be negative");
        return this;
    }

    public final com.a.b.c.f b(long p10, java.util.concurrent.TimeUnit p12)
    {
        long v0_1;
        if (this.o != -1) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        long v0_3;
        Object[] v4_1 = new Object[1];
        v4_1[0] = Long.valueOf(this.o);
        com.a.b.b.cn.b(v0_1, "expireAfterAccess was already set to %s ns", v4_1);
        if (p10 < 0) {
            v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        Object[] v4_4 = new Object[2];
        v4_4[0] = Long.valueOf(p10);
        v4_4[1] = p12;
        com.a.b.b.cn.a(v0_3, "duration cannot be negative: %s %s", v4_4);
        this.o = p12.toNanos(p10);
        return this;
    }

    final com.a.b.c.f b(com.a.b.c.bw p6)
    {
        com.a.b.c.bw v0_1;
        if (this.m != null) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        Object[] v1_1 = new Object[1];
        v1_1[0] = this.m;
        com.a.b.b.cn.b(v0_1, "Value strength was already set to %s", v1_1);
        this.m = ((com.a.b.c.bw) com.a.b.b.cn.a(p6));
        return this;
    }

    final com.a.b.c.bw c()
    {
        return ((com.a.b.c.bw) com.a.b.b.ca.a(this.m, com.a.b.c.bw.a));
    }

    final void d()
    {
        java.util.logging.Logger v0_0 = 1;
        if (this.k != null) {
            if (!this.f) {
                if (this.j == -1) {
                    com.a.b.c.f.z.log(java.util.logging.Level.WARNING, "ignoring weigher specified without maximumWeight");
                }
            } else {
                if (this.j == -1) {
                    v0_0 = 0;
                }
                com.a.b.b.cn.b(v0_0, "weigher requires maximumWeight");
            }
        } else {
            if (this.j != -1) {
                v0_0 = 0;
            }
            com.a.b.b.cn.b(v0_0, "maximumWeight requires weigher");
        }
        return;
    }

    public final String toString()
    {
        String v0_0 = com.a.b.b.ca.a(this);
        if (this.g != -1) {
            v0_0.a("initialCapacity", this.g);
        }
        if (this.h != -1) {
            v0_0.a("concurrencyLevel", this.h);
        }
        if (this.i != -1) {
            v0_0.a("maximumSize", this.i);
        }
        if (this.j != -1) {
            v0_0.a("maximumWeight", this.j);
        }
        if (this.n != -1) {
            v0_0.a("expireAfterWrite", new StringBuilder(22).append(this.n).append("ns").toString());
        }
        if (this.o != -1) {
            v0_0.a("expireAfterAccess", new StringBuilder(22).append(this.o).append("ns").toString());
        }
        if (this.l != null) {
            v0_0.a("keyStrength", com.a.b.b.e.a(this.l.toString()));
        }
        if (this.m != null) {
            v0_0.a("valueStrength", com.a.b.b.e.a(this.m.toString()));
        }
        if (this.q != null) {
            v0_0.a("keyEquivalence");
        }
        if (this.r != null) {
            v0_0.a("valueEquivalence");
        }
        if (this.s != null) {
            v0_0.a("removalListener");
        }
        return v0_0.toString();
    }
}
