package com.a.b.c;
final class at extends java.util.AbstractQueue {
    final com.a.b.c.bs a;

    at()
    {
        this.a = new com.a.b.c.au(this);
        return;
    }

    private com.a.b.c.bs a()
    {
        int v0_1 = this.a.f();
        if (v0_1 == this.a) {
            v0_1 = 0;
        }
        return v0_1;
    }

    private boolean a(com.a.b.c.bs p3)
    {
        com.a.b.c.ao.a(p3.g(), p3.f());
        com.a.b.c.ao.a(this.a.g(), p3);
        com.a.b.c.ao.a(p3, this.a);
        return 1;
    }

    private com.a.b.c.bs b()
    {
        int v0_1 = this.a.f();
        if (v0_1 != this.a) {
            this.remove(v0_1);
        } else {
            v0_1 = 0;
        }
        return v0_1;
    }

    public final void clear()
    {
        com.a.b.c.bs v0_1 = this.a.f();
        while (v0_1 != this.a) {
            com.a.b.c.bs v1_3 = v0_1.f();
            com.a.b.c.ao.a(v0_1);
            v0_1 = v1_3;
        }
        this.a.a(this.a);
        this.a.b(this.a);
        return;
    }

    public final boolean contains(Object p3)
    {
        int v0_1;
        if (((com.a.b.c.bs) p3).f() == com.a.b.c.br.a) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    public final boolean isEmpty()
    {
        int v0_2;
        if (this.a.f() != this.a) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    public final java.util.Iterator iterator()
    {
        return new com.a.b.c.av(this, this.a());
    }

    public final synthetic boolean offer(Object p3)
    {
        com.a.b.c.ao.a(((com.a.b.c.bs) p3).g(), ((com.a.b.c.bs) p3).f());
        com.a.b.c.ao.a(this.a.g(), ((com.a.b.c.bs) p3));
        com.a.b.c.ao.a(((com.a.b.c.bs) p3), this.a);
        return 1;
    }

    public final synthetic Object peek()
    {
        return this.a();
    }

    public final synthetic Object poll()
    {
        int v0_1 = this.a.f();
        if (v0_1 != this.a) {
            this.remove(v0_1);
        } else {
            v0_1 = 0;
        }
        return v0_1;
    }

    public final boolean remove(Object p3)
    {
        int v0_2;
        int v0_0 = ((com.a.b.c.bs) p3).g();
        com.a.b.c.bs v1 = ((com.a.b.c.bs) p3).f();
        com.a.b.c.ao.a(v0_0, v1);
        com.a.b.c.ao.a(((com.a.b.c.bs) p3));
        if (v1 == com.a.b.c.br.a) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    public final int size()
    {
        int v1 = 0;
        com.a.b.c.bs v0_1 = this.a.f();
        while (v0_1 != this.a) {
            v1++;
            v0_1 = v0_1.f();
        }
        return v1;
    }
}
