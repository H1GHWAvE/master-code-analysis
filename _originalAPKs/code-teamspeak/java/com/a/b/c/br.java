package com.a.b.c;
final enum class br extends java.lang.Enum implements com.a.b.c.bs {
    public static final enum com.a.b.c.br a;
    private static final synthetic com.a.b.c.br[] b;

    static br()
    {
        com.a.b.c.br.a = new com.a.b.c.br("INSTANCE");
        com.a.b.c.br[] v0_3 = new com.a.b.c.br[1];
        v0_3[0] = com.a.b.c.br.a;
        com.a.b.c.br.b = v0_3;
        return;
    }

    private br(String p2)
    {
        this(p2, 0);
        return;
    }

    public static com.a.b.c.br valueOf(String p1)
    {
        return ((com.a.b.c.br) Enum.valueOf(com.a.b.c.br, p1));
    }

    public static com.a.b.c.br[] values()
    {
        return ((com.a.b.c.br[]) com.a.b.c.br.b.clone());
    }

    public final com.a.b.c.cg a()
    {
        return 0;
    }

    public final void a(long p1)
    {
        return;
    }

    public final void a(com.a.b.c.bs p1)
    {
        return;
    }

    public final void a(com.a.b.c.cg p1)
    {
        return;
    }

    public final com.a.b.c.bs b()
    {
        return 0;
    }

    public final void b(long p1)
    {
        return;
    }

    public final void b(com.a.b.c.bs p1)
    {
        return;
    }

    public final int c()
    {
        return 0;
    }

    public final void c(com.a.b.c.bs p1)
    {
        return;
    }

    public final Object d()
    {
        return 0;
    }

    public final void d(com.a.b.c.bs p1)
    {
        return;
    }

    public final long e()
    {
        return 0;
    }

    public final com.a.b.c.bs f()
    {
        return this;
    }

    public final com.a.b.c.bs g()
    {
        return this;
    }

    public final long h()
    {
        return 0;
    }

    public final com.a.b.c.bs i()
    {
        return this;
    }

    public final com.a.b.c.bs j()
    {
        return this;
    }
}
