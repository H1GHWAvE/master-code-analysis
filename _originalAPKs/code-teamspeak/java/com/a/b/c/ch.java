package com.a.b.c;
final class ch extends java.util.AbstractCollection {
    final synthetic com.a.b.c.ao a;
    private final java.util.concurrent.ConcurrentMap b;

    ch(com.a.b.c.ao p1, java.util.concurrent.ConcurrentMap p2)
    {
        this.a = p1;
        this.b = p2;
        return;
    }

    public final void clear()
    {
        this.b.clear();
        return;
    }

    public final boolean contains(Object p2)
    {
        return this.b.containsValue(p2);
    }

    public final boolean isEmpty()
    {
        return this.b.isEmpty();
    }

    public final java.util.Iterator iterator()
    {
        return new com.a.b.c.cf(this.a);
    }

    public final int size()
    {
        return this.b.size();
    }
}
