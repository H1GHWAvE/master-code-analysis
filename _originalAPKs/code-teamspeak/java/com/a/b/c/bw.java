package com.a.b.c;
public abstract enum class bw extends java.lang.Enum {
    public static final enum com.a.b.c.bw a;
    public static final enum com.a.b.c.bw b;
    public static final enum com.a.b.c.bw c;
    private static final synthetic com.a.b.c.bw[] d;

    static bw()
    {
        com.a.b.c.bw.a = new com.a.b.c.bx("STRONG");
        com.a.b.c.bw.b = new com.a.b.c.by("SOFT");
        com.a.b.c.bw.c = new com.a.b.c.bz("WEAK");
        com.a.b.c.bw[] v0_7 = new com.a.b.c.bw[3];
        v0_7[0] = com.a.b.c.bw.a;
        v0_7[1] = com.a.b.c.bw.b;
        v0_7[2] = com.a.b.c.bw.c;
        com.a.b.c.bw.d = v0_7;
        return;
    }

    private bw(String p1, int p2)
    {
        this(p1, p2);
        return;
    }

    synthetic bw(String p1, int p2, byte p3)
    {
        this(p1, p2);
        return;
    }

    public static com.a.b.c.bw valueOf(String p1)
    {
        return ((com.a.b.c.bw) Enum.valueOf(com.a.b.c.bw, p1));
    }

    public static com.a.b.c.bw[] values()
    {
        return ((com.a.b.c.bw[]) com.a.b.c.bw.d.clone());
    }

    abstract com.a.b.b.au a();

    abstract com.a.b.c.cg a();
}
