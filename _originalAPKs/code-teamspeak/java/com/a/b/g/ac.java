package com.a.b.g;
public final class ac extends java.io.OutputStream {
    final com.a.b.g.bn a;

    public ac(com.a.b.g.bn p2)
    {
        this.a = ((com.a.b.g.bn) com.a.b.b.cn.a(p2));
        return;
    }

    public final String toString()
    {
        String v0_2 = String.valueOf(String.valueOf(this.a));
        return new StringBuilder((v0_2.length() + 24)).append("Funnels.asOutputStream(").append(v0_2).append(")").toString();
    }

    public final void write(int p3)
    {
        this.a.c(((byte) p3));
        return;
    }

    public final void write(byte[] p2)
    {
        this.a.c(p2);
        return;
    }

    public final void write(byte[] p2, int p3, int p4)
    {
        this.a.c(p2, p3, p4);
        return;
    }
}
