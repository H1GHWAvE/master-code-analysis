package com.a.b.g;
final class bf extends com.a.b.g.h implements java.io.Serializable {
    private final java.security.MessageDigest a;
    private final int b;
    private final boolean c;
    private final String d;

    bf(String p8, int p9, String p10)
    {
        boolean v0_5;
        this.d = ((String) com.a.b.b.cn.a(p10));
        this.a = com.a.b.g.bf.a(p8);
        int v3 = this.a.getDigestLength();
        if ((p9 < 4) || (p9 > v3)) {
            v0_5 = 0;
        } else {
            v0_5 = 1;
        }
        Object[] v5_1 = new Object[2];
        v5_1[0] = Integer.valueOf(p9);
        v5_1[1] = Integer.valueOf(v3);
        com.a.b.b.cn.a(v0_5, "bytes (%s) must be >= 4 and < %s", v5_1);
        this.b = p9;
        this.c = this.c();
        return;
    }

    bf(String p2, String p3)
    {
        this.a = com.a.b.g.bf.a(p2);
        this.b = this.a.getDigestLength();
        this.d = ((String) com.a.b.b.cn.a(p3));
        this.c = this.c();
        return;
    }

    private static java.security.MessageDigest a(String p2)
    {
        try {
            return java.security.MessageDigest.getInstance(p2);
        } catch (java.security.NoSuchAlgorithmException v0_1) {
            throw new AssertionError(v0_1);
        }
    }

    private boolean c()
    {
        try {
            this.a.clone();
            int v0_1 = 1;
        } catch (int v0) {
            v0_1 = 0;
        }
        return v0_1;
    }

    private Object d()
    {
        return new com.a.b.g.bi(this.a.getAlgorithm(), this.b, this.d, 0);
    }

    public final com.a.b.g.al a()
    {
        CloneNotSupportedException v0_4;
        if (!this.c) {
            v0_4 = new com.a.b.g.bh(com.a.b.g.bf.a(this.a.getAlgorithm()), this.b, 0);
        } else {
            try {
                v0_4 = new com.a.b.g.bh(((java.security.MessageDigest) this.a.clone()), this.b, 0);
            } catch (CloneNotSupportedException v0) {
            }
        }
        return v0_4;
    }

    public final int b()
    {
        return (this.b * 8);
    }

    public final String toString()
    {
        return this.d;
    }
}
