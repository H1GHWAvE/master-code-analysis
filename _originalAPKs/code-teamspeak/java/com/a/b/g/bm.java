package com.a.b.g;
final class bm extends com.a.b.g.i {
    private static final int a = 4;
    private int b;
    private int c;

    bm(int p2)
    {
        this(4);
        this.b = p2;
        this.c = 0;
        return;
    }

    protected final void a(java.nio.ByteBuffer p3)
    {
        this.b = com.a.b.g.bl.a(this.b, com.a.b.g.bl.c(p3.getInt()));
        this.c = (this.c + 4);
        return;
    }

    public final com.a.b.g.ag b()
    {
        return com.a.b.g.bl.b(this.b, this.c);
    }

    protected final void b(java.nio.ByteBuffer p4)
    {
        int v0_0 = 0;
        this.c = (this.c + p4.remaining());
        int v1_2 = 0;
        while (p4.hasRemaining()) {
            v1_2 ^= ((p4.get() & 255) << v0_0);
            v0_0 += 8;
        }
        this.b = (this.b ^ com.a.b.g.bl.c(v1_2));
        return;
    }
}
