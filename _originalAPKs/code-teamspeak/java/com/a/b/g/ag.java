package com.a.b.g;
public abstract class ag {
    private static final char[] a;

    static ag()
    {
        com.a.b.g.ag.a = "0123456789abcdef".toCharArray();
        return;
    }

    ag()
    {
        return;
    }

    private static int a(char p3)
    {
        if ((p3 < 48) || (p3 > 57)) {
            if ((p3 < 97) || (p3 > 102)) {
                throw new IllegalArgumentException(new StringBuilder(32).append("Illegal hexadecimal character: ").append(p3).toString());
            } else {
                int v0_7 = ((p3 - 97) + 10);
            }
        } else {
            v0_7 = (p3 - 48);
        }
        return v0_7;
    }

    public static com.a.b.g.ag a(int p1)
    {
        return new com.a.b.g.ai(p1);
    }

    public static com.a.b.g.ag a(long p2)
    {
        return new com.a.b.g.aj(p2);
    }

    private static com.a.b.g.ag a(String p5)
    {
        com.a.b.g.ag v0_1;
        int v2 = 0;
        if (p5.length() < 2) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        com.a.b.g.ag v0_4;
        int v4_0 = new Object[1];
        v4_0[0] = p5;
        com.a.b.b.cn.a(v0_1, "input string (%s) must have at least 2 characters", v4_0);
        if ((p5.length() % 2) != 0) {
            v0_4 = 0;
        } else {
            v0_4 = 1;
        }
        byte v1_1 = new Object[1];
        v1_1[0] = p5;
        com.a.b.b.cn.a(v0_4, "input string (%s) must have an even number of characters", v1_1);
        com.a.b.g.ag v0_7 = new byte[(p5.length() / 2)];
        while (v2 < p5.length()) {
            v0_7[(v2 / 2)] = ((byte) ((com.a.b.g.ag.a(p5.charAt(v2)) << 4) + com.a.b.g.ag.a(p5.charAt((v2 + 1)))));
            v2 += 2;
        }
        return com.a.b.g.ag.a(v0_7);
    }

    static com.a.b.g.ag a(byte[] p1)
    {
        return new com.a.b.g.ah(p1);
    }

    private int b(byte[] p4, int p5, int p6)
    {
        int v0_1 = new int[2];
        v0_1[0] = p6;
        v0_1[1] = (this.a() / 8);
        int v0_2 = com.a.b.l.q.a(v0_1);
        com.a.b.b.cn.a(p5, (p5 + v0_2), p4.length);
        this.a(p4, p5, v0_2);
        return v0_2;
    }

    private static com.a.b.g.ag b(byte[] p2)
    {
        com.a.b.g.ag v0_1;
        if (p2.length <= 0) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        com.a.b.b.cn.a(v0_1, "A HashCode must contain at least 1 byte.");
        return com.a.b.g.ag.a(((byte[]) p2.clone()));
    }

    public abstract int a();

    abstract void a();

    abstract boolean a();

    public abstract int b();

    public abstract long c();

    public abstract long d();

    public abstract byte[] e();

    public final boolean equals(Object p4)
    {
        int v0 = 0;
        if (((p4 instanceof com.a.b.g.ag)) && ((this.a() == ((com.a.b.g.ag) p4).a()) && (this.a(((com.a.b.g.ag) p4))))) {
            v0 = 1;
        }
        return v0;
    }

    byte[] f()
    {
        return this.e();
    }

    public final int hashCode()
    {
        int v1_1;
        if (this.a() < 32) {
            byte[] v2 = this.e();
            v1_1 = (v2[0] & 255);
            int v0_3 = 1;
            while (v0_3 < v2.length) {
                v1_1 |= ((v2[v0_3] & 255) << (v0_3 * 8));
                v0_3++;
            }
        } else {
            v1_1 = this.b();
        }
        return v1_1;
    }

    public final String toString()
    {
        byte[] v1 = this.e();
        StringBuilder v2_1 = new StringBuilder((v1.length * 2));
        int v3 = v1.length;
        String v0_2 = 0;
        while (v0_2 < v3) {
            char v4_0 = v1[v0_2];
            v2_1.append(com.a.b.g.ag.a[((v4_0 >> 4) & 15)]).append(com.a.b.g.ag.a[(v4_0 & 15)]);
            v0_2++;
        }
        return v2_1.toString();
    }
}
