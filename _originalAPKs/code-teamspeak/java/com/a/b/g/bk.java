package com.a.b.g;
final class bk extends com.a.b.g.i {
    private static final int a = 16;
    private static final long b = 9782798678568883157;
    private static final long c = 5545529020109919103;
    private long d;
    private long e;
    private int f;

    bk(int p3)
    {
        this(16);
        this.d = ((long) p3);
        this.e = ((long) p3);
        this.f = 0;
        return;
    }

    private void a(long p8, long p10)
    {
        this.d = (this.d ^ com.a.b.g.bk.d(p8));
        this.d = Long.rotateLeft(this.d, 27);
        this.d = (this.d + this.e);
        this.d = ((this.d * 5) + 1390208809);
        this.e = (this.e ^ com.a.b.g.bk.e(p10));
        this.e = Long.rotateLeft(this.e, 31);
        this.e = (this.e + this.d);
        this.e = ((this.e * 5) + 944331445);
        return;
    }

    private static long c(long p6)
    {
        long v0_2 = (((p6 >> 33) ^ p6) * -1.9406492979739223e+305);
        long v0_4 = ((v0_2 ^ (v0_2 >> 33)) * -2.902039044684214e+23);
        return (v0_4 ^ (v0_4 >> 33));
    }

    private static long d(long p4)
    {
        return (Long.rotateLeft((-2.8811287363897357e-271 * p4), 31) * 5.573325460219186e+62);
    }

    private static long e(long p4)
    {
        return (Long.rotateLeft((5.573325460219186e+62 * p4), 33) * -2.8811287363897357e-271);
    }

    protected final void a(java.nio.ByteBuffer p9)
    {
        int v0_0 = p9.getLong();
        long v2_0 = p9.getLong();
        this.d = (com.a.b.g.bk.d(v0_0) ^ this.d);
        this.d = Long.rotateLeft(this.d, 27);
        this.d = (this.d + this.e);
        this.d = ((this.d * 5) + 1390208809);
        this.e = (this.e ^ com.a.b.g.bk.e(v2_0));
        this.e = Long.rotateLeft(this.e, 31);
        this.e = (this.e + this.d);
        this.e = ((this.e * 5) + 944331445);
        this.f = (this.f + 16);
        return;
    }

    public final com.a.b.g.ag b()
    {
        this.d = (this.d ^ ((long) this.f));
        this.e = (this.e ^ ((long) this.f));
        this.d = (this.d + this.e);
        this.e = (this.e + this.d);
        this.d = com.a.b.g.bk.c(this.d);
        this.e = com.a.b.g.bk.c(this.e);
        this.d = (this.d + this.e);
        this.e = (this.e + this.d);
        com.a.b.g.ag v0_17 = new byte[16];
        return com.a.b.g.ag.a(java.nio.ByteBuffer.wrap(v0_17).order(java.nio.ByteOrder.LITTLE_ENDIAN).putLong(this.d).putLong(this.e).array());
    }

    protected final void b(java.nio.ByteBuffer p14)
    {
        long v0_21;
        long v0_22;
        long v0_10;
        long v0_13;
        long v0_23;
        long v0_24;
        long v0_9;
        long v0_12;
        long v0_25;
        long v2_1;
        long v0_8;
        long v0_11;
        long v0_20;
        long v0_14;
        this.f = (this.f + p14.remaining());
        switch (p14.remaining()) {
            case 1:
                v0_25 = 0;
                v2_1 = (v0_25 ^ ((long) (p14.get(0) & 255)));
                v0_14 = 0;
                break;
            case 2:
                v0_24 = 0;
                v0_25 = (v0_24 ^ (((long) (p14.get(1) & 255)) << 8));
                break;
            case 3:
                v0_23 = 0;
                v0_24 = (v0_23 ^ (((long) (p14.get(2) & 255)) << 16));
                break;
            case 4:
                v0_22 = 0;
                v0_23 = (v0_22 ^ (((long) (p14.get(3) & 255)) << 24));
                break;
            case 5:
                v0_21 = 0;
                v0_22 = (v0_21 ^ (((long) (p14.get(4) & 255)) << 32));
                break;
            case 6:
                v0_20 = 0;
                v0_21 = (v0_20 ^ (((long) (p14.get(5) & 255)) << 40));
                break;
            case 7:
                v0_20 = ((((long) (p14.get(6) & 255)) << 48) ^ 0);
                break;
            case 8:
                v0_14 = 0;
                v2_1 = (0 ^ p14.getLong());
                break;
            case 9:
                v0_13 = 0;
                v0_14 = (v0_13 ^ ((long) (p14.get(8) & 255)));
                break;
            case 10:
                v0_12 = 0;
                v0_13 = (v0_12 ^ (((long) (p14.get(9) & 255)) << 8));
                break;
            case 11:
                v0_11 = 0;
                v0_12 = (v0_11 ^ (((long) (p14.get(10) & 255)) << 16));
                break;
            case 12:
                v0_10 = 0;
                v0_11 = (v0_10 ^ (((long) (p14.get(11) & 255)) << 24));
                break;
            case 13:
                v0_9 = 0;
                v0_10 = (v0_9 ^ (((long) (p14.get(12) & 255)) << 32));
                break;
            case 14:
                v0_8 = 0;
                v0_9 = (v0_8 ^ (((long) (p14.get(13) & 255)) << 40));
                break;
            case 15:
                v0_8 = ((((long) (p14.get(14) & 255)) << 48) ^ 0);
                break;
            default:
                throw new AssertionError("Should never get here.");
        }
        this.d = (com.a.b.g.bk.d(v2_1) ^ this.d);
        this.e = (com.a.b.g.bk.e(v0_14) ^ this.e);
        return;
    }
}
