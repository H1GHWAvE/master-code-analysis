package com.a.b.l;
final enum class t extends java.lang.Enum implements java.util.Comparator {
    public static final enum com.a.b.l.t a;
    private static final synthetic com.a.b.l.t[] b;

    static t()
    {
        com.a.b.l.t.a = new com.a.b.l.t("INSTANCE");
        com.a.b.l.t[] v0_3 = new com.a.b.l.t[1];
        v0_3[0] = com.a.b.l.t.a;
        com.a.b.l.t.b = v0_3;
        return;
    }

    private t(String p2)
    {
        this(p2, 0);
        return;
    }

    private static int a(int[] p4, int[] p5)
    {
        int v2 = Math.min(p4.length, p5.length);
        int v1_1 = 0;
        while (v1_1 < v2) {
            int v0_3 = com.a.b.l.q.a(p4[v1_1], p5[v1_1]);
            if (v0_3 == 0) {
                v1_1++;
            }
            return v0_3;
        }
        v0_3 = (p4.length - p5.length);
        return v0_3;
    }

    public static com.a.b.l.t valueOf(String p1)
    {
        return ((com.a.b.l.t) Enum.valueOf(com.a.b.l.t, p1));
    }

    public static com.a.b.l.t[] values()
    {
        return ((com.a.b.l.t[]) com.a.b.l.t.b.clone());
    }

    public final synthetic int compare(Object p5, Object p6)
    {
        int v2 = Math.min(((int[]) p5).length, ((int[]) p6).length);
        int v1_1 = 0;
        while (v1_1 < v2) {
            int v0_3 = com.a.b.l.q.a(((int[]) p5)[v1_1], ((int[]) p6)[v1_1]);
            if (v0_3 == 0) {
                v1_1++;
            }
            return v0_3;
        }
        v0_3 = (((int[]) p5).length - ((int[]) p6).length);
        return v0_3;
    }
}
