package com.a.b.l;
public final class i {
    public static final int a = 8;
    static final java.util.regex.Pattern b;

    static i()
    {
        java.util.regex.Pattern v0_2 = String.valueOf("(?:\\d++(?:\\.\\d*+)?|\\.\\d++)").concat("(?:[eE][+-]?\\d++)?[fFdD]?");
        String v1_3 = String.valueOf(String.valueOf("(?:\\p{XDigit}++(?:\\.\\p{XDigit}*+)?|\\.\\p{XDigit}++)"));
        String v1_6 = new StringBuilder((v1_3.length() + 25)).append("0[xX]").append(v1_3).append("[pP][+-]?\\d++[fFdD]?").toString();
        java.util.regex.Pattern v0_4 = String.valueOf(String.valueOf(v0_2));
        String v1_8 = String.valueOf(String.valueOf(v1_6));
        com.a.b.l.i.b = java.util.regex.Pattern.compile(new StringBuilder(((v0_4.length() + 23) + v1_8.length())).append("[+-]?(?:NaN|Infinity|").append(v0_4).append("|").append(v1_8).append(")").toString());
        return;
    }

    private i()
    {
        return;
    }

    private static varargs double a(double[] p6)
    {
        int v0_1;
        int v1 = 1;
        if (p6.length <= 0) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        com.a.b.b.cn.a(v0_1);
        double v2_1 = p6[0];
        while (v1 < p6.length) {
            v2_1 = Math.min(v2_1, p6[v1]);
            v1++;
        }
        return v2_1;
    }

    public static int a(double p2)
    {
        return Double.valueOf(p2).hashCode();
    }

    private static int a(double p2, double p4)
    {
        return Double.compare(p2, p4);
    }

    static synthetic int a(double[] p1, double p2, int p4, int p5)
    {
        return com.a.b.l.i.c(p1, p2, p4, p5);
    }

    private static int a(double[] p8, double[] p9)
    {
        int v1 = 0;
        com.a.b.b.cn.a(p8, "array");
        com.a.b.b.cn.a(p9, "target");
        if (p9.length != 0) {
            int v0_3 = 0;
            while (v0_3 < ((p8.length - p9.length) + 1)) {
                int v2_3 = 0;
                while (v2_3 < p9.length) {
                    if (p8[(v0_3 + v2_3)] != p9[v2_3]) {
                        v0_3++;
                    } else {
                        v2_3++;
                    }
                }
                v1 = v0_3;
            }
            v1 = -1;
        }
        return v1;
    }

    private static com.a.b.b.ak a()
    {
        return com.a.b.l.k.a;
    }

    private static Double a(String p2)
    {
        NumberFormatException v0_4;
        if (!com.a.b.l.i.b.matcher(p2).matches()) {
            v0_4 = 0;
        } else {
            try {
                v0_4 = Double.valueOf(Double.parseDouble(p2));
            } catch (NumberFormatException v0) {
            }
        }
        return v0_4;
    }

    private static varargs String a(String p6, double[] p7)
    {
        String v0_5;
        com.a.b.b.cn.a(p6);
        if (p7.length != 0) {
            StringBuilder v1_1 = new StringBuilder((p7.length * 12));
            v1_1.append(p7[0]);
            String v0_4 = 1;
            while (v0_4 < p7.length) {
                v1_1.append(p6).append(p7[v0_4]);
                v0_4++;
            }
            v0_5 = v1_1.toString();
        } else {
            v0_5 = "";
        }
        return v0_5;
    }

    private static boolean a(double[] p7, double p8)
    {
        int v0 = 0;
        int v1 = 0;
        while (v1 < p7.length) {
            if (p7[v1] != p8) {
                v1++;
            } else {
                v0 = 1;
                break;
            }
        }
        return v0;
    }

    private static double[] a(java.util.Collection p8)
    {
        double[] v0_1;
        int v2 = 0;
        if (!(p8 instanceof com.a.b.l.j)) {
            Object[] v3_0 = p8.toArray();
            int v4_0 = v3_0.length;
            double[] v1_0 = new double[v4_0];
            while (v2 < v4_0) {
                v1_0[v2] = ((Number) com.a.b.b.cn.a(v3_0[v2])).doubleValue();
                v2++;
            }
            v0_1 = v1_0;
        } else {
            double[] v1_1 = ((com.a.b.l.j) p8).size();
            v0_1 = new double[v1_1];
            System.arraycopy(((com.a.b.l.j) p8).a, ((com.a.b.l.j) p8).b, v0_1, 0, v1_1);
        }
        return v0_1;
    }

    private static double[] a(double[] p3, int p4)
    {
        double[] v0 = new double[p4];
        System.arraycopy(p3, 0, v0, 0, Math.min(p3.length, p4));
        return v0;
    }

    private static double[] a(double[] p6, int p7, int p8)
    {
        double[] v0_0;
        if (p7 < 0) {
            v0_0 = 0;
        } else {
            v0_0 = 1;
        }
        double[] v0_1;
        Integer v4_0 = new Object[1];
        v4_0[0] = Integer.valueOf(p7);
        com.a.b.b.cn.a(v0_0, "Invalid minLength: %s", v4_0);
        if (p8 < 0) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        int v1_1 = new Object[1];
        v1_1[0] = Integer.valueOf(p8);
        com.a.b.b.cn.a(v0_1, "Invalid padding: %s", v1_1);
        if (p6.length < p7) {
            int v1_2 = (p7 + p8);
            double[] v0_3 = new double[v1_2];
            System.arraycopy(p6, 0, v0_3, 0, Math.min(p6.length, v1_2));
            p6 = v0_3;
        }
        return p6;
    }

    private static varargs double[] a(double[][] p7)
    {
        int v0_0 = 0;
        int v2_0 = 0;
        while (v0_0 < p7.length) {
            v2_0 += p7[v0_0].length;
            v0_0++;
        }
        double[] v3_1 = new double[v2_0];
        int v4_0 = p7.length;
        int v0_1 = 0;
        int v2_1 = 0;
        while (v2_1 < v4_0) {
            int v5_0 = p7[v2_1];
            System.arraycopy(v5_0, 0, v3_1, v0_1, v5_0.length);
            v0_1 += v5_0.length;
            v2_1++;
        }
        return v3_1;
    }

    private static varargs double b(double[] p6)
    {
        int v0_1;
        int v1 = 1;
        if (p6.length <= 0) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        com.a.b.b.cn.a(v0_1);
        double v2_1 = p6[0];
        while (v1 < p6.length) {
            v2_1 = Math.max(v2_1, p6[v1]);
            v1++;
        }
        return v2_1;
    }

    private static int b(double[] p3, double p4)
    {
        return com.a.b.l.i.c(p3, p4, 0, p3.length);
    }

    static synthetic int b(double[] p1, double p2, int p4, int p5)
    {
        return com.a.b.l.i.d(p1, p2, p4, p5);
    }

    private static java.util.Comparator b()
    {
        return com.a.b.l.l.a;
    }

    private static boolean b(double p6)
    {
        int v2_2;
        int v0_0 = 1;
        if (-inf >= p6) {
            v2_2 = 0;
        } else {
            v2_2 = 1;
        }
        if (p6 >= inf) {
            v0_0 = 0;
        }
        return (v0_0 & v2_2);
    }

    private static int c(double[] p3, double p4)
    {
        return com.a.b.l.i.d(p3, p4, 0, p3.length);
    }

    private static int c(double[] p5, double p6, int p8, int p9)
    {
        int v0 = p8;
        while (v0 < p9) {
            if (p5[v0] != p6) {
                v0++;
            }
            return v0;
        }
        v0 = -1;
        return v0;
    }

    private static varargs java.util.List c(double[] p1)
    {
        com.a.b.l.j v0_2;
        if (p1.length != 0) {
            v0_2 = new com.a.b.l.j(p1);
        } else {
            v0_2 = java.util.Collections.emptyList();
        }
        return v0_2;
    }

    private static java.util.regex.Pattern c()
    {
        java.util.regex.Pattern v0_2 = String.valueOf("(?:\\d++(?:\\.\\d*+)?|\\.\\d++)").concat("(?:[eE][+-]?\\d++)?[fFdD]?");
        String v1_3 = String.valueOf(String.valueOf("(?:\\p{XDigit}++(?:\\.\\p{XDigit}*+)?|\\.\\p{XDigit}++)"));
        String v1_6 = new StringBuilder((v1_3.length() + 25)).append("0[xX]").append(v1_3).append("[pP][+-]?\\d++[fFdD]?").toString();
        java.util.regex.Pattern v0_4 = String.valueOf(String.valueOf(v0_2));
        String v1_8 = String.valueOf(String.valueOf(v1_6));
        return java.util.regex.Pattern.compile(new StringBuilder(((v0_4.length() + 23) + v1_8.length())).append("[+-]?(?:NaN|Infinity|").append(v0_4).append("|").append(v1_8).append(")").toString());
    }

    private static int d(double[] p5, double p6, int p8, int p9)
    {
        int v0 = (p9 - 1);
        while (v0 >= p8) {
            if (p5[v0] != p6) {
                v0--;
            }
            return v0;
        }
        v0 = -1;
        return v0;
    }
}
