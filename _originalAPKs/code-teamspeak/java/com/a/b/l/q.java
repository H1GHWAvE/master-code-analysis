package com.a.b.l;
public final class q {
    public static final int a = 4;
    public static final int b = 1073741824;
    private static final byte[] c;

    static q()
    {
        int v0 = 0;
        byte[] v1_1 = new byte[128];
        com.a.b.l.q.c = v1_1;
        java.util.Arrays.fill(v1_1, -1);
        byte[] v1_2 = 0;
        while (v1_2 <= 9) {
            com.a.b.l.q.c[(v1_2 + 48)] = ((byte) v1_2);
            v1_2++;
        }
        while (v0 <= 26) {
            com.a.b.l.q.c[(v0 + 65)] = ((byte) (v0 + 10));
            com.a.b.l.q.c[(v0 + 97)] = ((byte) (v0 + 10));
            v0++;
        }
        return;
    }

    private q()
    {
        return;
    }

    public static int a(byte p2, byte p3, byte p4, byte p5)
    {
        return ((((p2 << 24) | ((p3 & 255) << 16)) | ((p4 & 255) << 8)) | (p5 & 255));
    }

    private static int a(char p1)
    {
        int v0_1;
        if (p1 >= 128) {
            v0_1 = -1;
        } else {
            v0_1 = com.a.b.l.q.c[p1];
        }
        return v0_1;
    }

    public static int a(int p0)
    {
        return p0;
    }

    public static int a(int p1, int p2)
    {
        int v0;
        if (p1 >= p2) {
            if (p1 <= p2) {
                v0 = 0;
            } else {
                v0 = 1;
            }
        } else {
            v0 = -1;
        }
        return v0;
    }

    public static int a(long p4)
    {
        if (((long) ((int) p4)) == p4) {
            return ((int) p4);
        } else {
            throw new IllegalArgumentException(new StringBuilder(34).append("Out of range: ").append(p4).toString());
        }
    }

    private static int a(byte[] p8)
    {
        int v0_1;
        if (p8.length < 4) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        Object[] v4 = new Object[2];
        v4[0] = Integer.valueOf(p8.length);
        v4[1] = Integer.valueOf(4);
        com.a.b.b.cn.a(v0_1, "array too small: %s < %s", v4);
        return com.a.b.l.q.a(p8[0], p8[1], p8[2], p8[3]);
    }

    public static varargs int a(int[] p4)
    {
        com.a.b.b.cn.a(1);
        int v0_1 = p4[0];
        int v1 = 1;
        while (v1 < 2) {
            if (p4[1] < v0_1) {
                v0_1 = p4[1];
            }
            v1++;
        }
        return v0_1;
    }

    static synthetic int a(int[] p1, int p2, int p3, int p4)
    {
        return com.a.b.l.q.c(p1, p2, p3, p4);
    }

    private static int a(int[] p5, int[] p6)
    {
        int v1 = 0;
        com.a.b.b.cn.a(p5, "array");
        com.a.b.b.cn.a(p6, "target");
        if (p6.length != 0) {
            int v0_3 = 0;
            while (v0_3 < ((p5.length - p6.length) + 1)) {
                int v2_3 = 0;
                while (v2_3 < p6.length) {
                    if (p5[(v0_3 + v2_3)] != p6[v2_3]) {
                        v0_3++;
                    } else {
                        v2_3++;
                    }
                }
                v1 = v0_3;
            }
            v1 = -1;
        }
        return v1;
    }

    private static com.a.b.b.ak a()
    {
        return com.a.b.l.s.a;
    }

    private static Integer a(String p8)
    {
        int v0_9;
        if (!((String) com.a.b.b.cn.a(p8)).isEmpty()) {
            int v4_1;
            if (p8.charAt(0) != 45) {
                v4_1 = 0;
            } else {
                v4_1 = 1;
            }
            int v0_4;
            if (v4_1 == 0) {
                v0_4 = 0;
            } else {
                v0_4 = 1;
            }
            if (v0_4 != p8.length()) {
                int v1_2 = (v0_4 + 1);
                int v0_6 = com.a.b.l.q.a(p8.charAt(v0_4));
                if ((v0_6 >= 0) && (v0_6 < 10)) {
                    int v0_7 = (- v0_6);
                    while (v1_2 < p8.length()) {
                        int v2_2 = (v1_2 + 1);
                        int v1_4 = com.a.b.l.q.a(p8.charAt(v1_2));
                        if ((v1_4 >= 0) && ((v1_4 < 10) && (v0_7 >= -214748364))) {
                            int v0_10 = (v0_7 * 10);
                            if (v0_10 >= (-2147483648 + v1_4)) {
                                v0_7 = (v0_10 - v1_4);
                                v1_2 = v2_2;
                            } else {
                                v0_9 = 0;
                            }
                        } else {
                            v0_9 = 0;
                        }
                    }
                    if (v4_1 == 0) {
                        if (v0_7 != -2147483648) {
                            v0_9 = Integer.valueOf((- v0_7));
                        } else {
                            v0_9 = 0;
                        }
                    } else {
                        v0_9 = Integer.valueOf(v0_7);
                    }
                } else {
                    v0_9 = 0;
                }
            } else {
                v0_9 = 0;
            }
        } else {
            v0_9 = 0;
        }
        return v0_9;
    }

    private static varargs String a(String p4, int[] p5)
    {
        String v0_6;
        com.a.b.b.cn.a(p4);
        if (p5.length != 0) {
            StringBuilder v1_1 = new StringBuilder((p5.length * 5));
            v1_1.append(p5[0]);
            String v0_5 = 1;
            while (v0_5 < p5.length) {
                v1_1.append(p4).append(p5[v0_5]);
                v0_5++;
            }
            v0_6 = v1_1.toString();
        } else {
            v0_6 = "";
        }
        return v0_6;
    }

    private static boolean a(int[] p4, int p5)
    {
        int v0 = 0;
        int v1 = 0;
        while (v1 < p4.length) {
            if (p4[v1] != p5) {
                v1++;
            } else {
                v0 = 1;
                break;
            }
        }
        return v0;
    }

    private static int[] a(java.util.Collection p5)
    {
        int[] v0_1;
        int v2 = 0;
        if (!(p5 instanceof com.a.b.l.r)) {
            Object[] v3_0 = p5.toArray();
            int v4_0 = v3_0.length;
            int[] v1_0 = new int[v4_0];
            while (v2 < v4_0) {
                v1_0[v2] = ((Number) com.a.b.b.cn.a(v3_0[v2])).intValue();
                v2++;
            }
            v0_1 = v1_0;
        } else {
            int[] v1_1 = ((com.a.b.l.r) p5).size();
            v0_1 = new int[v1_1];
            System.arraycopy(((com.a.b.l.r) p5).a, ((com.a.b.l.r) p5).b, v0_1, 0, v1_1);
        }
        return v0_1;
    }

    private static int[] a(int[] p6, int p7, int p8)
    {
        int[] v0_0;
        if (p7 < 0) {
            v0_0 = 0;
        } else {
            v0_0 = 1;
        }
        int[] v0_1;
        Integer v4_0 = new Object[1];
        v4_0[0] = Integer.valueOf(p7);
        com.a.b.b.cn.a(v0_0, "Invalid minLength: %s", v4_0);
        if (p8 < 0) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        int v1_1 = new Object[1];
        v1_1[0] = Integer.valueOf(p8);
        com.a.b.b.cn.a(v0_1, "Invalid padding: %s", v1_1);
        if (p6.length < p7) {
            int v1_2 = (p7 + p8);
            int[] v0_3 = new int[v1_2];
            System.arraycopy(p6, 0, v0_3, 0, Math.min(p6.length, v1_2));
            p6 = v0_3;
        }
        return p6;
    }

    private static varargs int[] a(int[][] p7)
    {
        int v0_0 = 0;
        int v2_0 = 0;
        while (v0_0 < p7.length) {
            v2_0 += p7[v0_0].length;
            v0_0++;
        }
        int[] v3_1 = new int[v2_0];
        int v4_0 = p7.length;
        int v0_1 = 0;
        int v2_1 = 0;
        while (v2_1 < v4_0) {
            int v5_0 = p7[v2_1];
            System.arraycopy(v5_0, 0, v3_1, v0_1, v5_0.length);
            v0_1 += v5_0.length;
            v2_1++;
        }
        return v3_1;
    }

    public static int b(long p2)
    {
        int v0_4;
        if (p2 <= 2147483647) {
            if (p2 >= -2147483648) {
                v0_4 = ((int) p2);
            } else {
                v0_4 = -2147483648;
            }
        } else {
            v0_4 = 2147483647;
        }
        return v0_4;
    }

    private static varargs int b(int[] p3)
    {
        int v0_1;
        int v1 = 1;
        if (p3.length <= 0) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        com.a.b.b.cn.a(v0_1);
        int v0_2 = p3[0];
        while (v1 < p3.length) {
            if (p3[v1] > v0_2) {
                v0_2 = p3[v1];
            }
            v1++;
        }
        return v0_2;
    }

    private static int b(int[] p2, int p3)
    {
        return com.a.b.l.q.c(p2, p3, 0, p2.length);
    }

    static synthetic int b(int[] p1, int p2, int p3, int p4)
    {
        return com.a.b.l.q.d(p1, p2, p3, p4);
    }

    private static Integer b(String p8)
    {
        int v0_9;
        if (!((String) com.a.b.b.cn.a(p8)).isEmpty()) {
            int v4_1;
            if (p8.charAt(0) != 45) {
                v4_1 = 0;
            } else {
                v4_1 = 1;
            }
            int v0_4;
            if (v4_1 == 0) {
                v0_4 = 0;
            } else {
                v0_4 = 1;
            }
            if (v0_4 != p8.length()) {
                int v1_2 = (v0_4 + 1);
                int v0_6 = com.a.b.l.q.a(p8.charAt(v0_4));
                if ((v0_6 >= 0) && (v0_6 < 10)) {
                    int v0_7 = (- v0_6);
                    while (v1_2 < p8.length()) {
                        int v2_2 = (v1_2 + 1);
                        int v1_4 = com.a.b.l.q.a(p8.charAt(v1_2));
                        if ((v1_4 >= 0) && ((v1_4 < 10) && (v0_7 >= -214748364))) {
                            int v0_10 = (v0_7 * 10);
                            if (v0_10 >= (-2147483648 + v1_4)) {
                                v0_7 = (v0_10 - v1_4);
                                v1_2 = v2_2;
                            } else {
                                v0_9 = 0;
                            }
                        } else {
                            v0_9 = 0;
                        }
                    }
                    if (v4_1 == 0) {
                        if (v0_7 != -2147483648) {
                            v0_9 = Integer.valueOf((- v0_7));
                        } else {
                            v0_9 = 0;
                        }
                    } else {
                        v0_9 = Integer.valueOf(v0_7);
                    }
                } else {
                    v0_9 = 0;
                }
            } else {
                v0_9 = 0;
            }
        } else {
            v0_9 = 0;
        }
        return v0_9;
    }

    private static java.util.Comparator b()
    {
        return com.a.b.l.t.a;
    }

    public static byte[] b(int p3)
    {
        byte[] v0_1 = new byte[4];
        v0_1[0] = ((byte) (p3 >> 24));
        v0_1[1] = ((byte) (p3 >> 16));
        v0_1[2] = ((byte) (p3 >> 8));
        v0_1[3] = ((byte) p3);
        return v0_1;
    }

    private static int c(int[] p2, int p3)
    {
        return com.a.b.l.q.d(p2, p3, 0, p2.length);
    }

    private static int c(int[] p2, int p3, int p4, int p5)
    {
        int v0 = p4;
        while (v0 < p5) {
            if (p2[v0] != p3) {
                v0++;
            }
            return v0;
        }
        v0 = -1;
        return v0;
    }

    private static varargs java.util.List c(int[] p1)
    {
        com.a.b.l.r v0_2;
        if (p1.length != 0) {
            v0_2 = new com.a.b.l.r(p1);
        } else {
            v0_2 = java.util.Collections.emptyList();
        }
        return v0_2;
    }

    private static int d(int[] p2, int p3, int p4, int p5)
    {
        int v0 = (p5 - 1);
        while (v0 >= p4) {
            if (p2[v0] != p3) {
                v0--;
            }
            return v0;
        }
        v0 = -1;
        return v0;
    }

    private static int[] d(int[] p3, int p4)
    {
        int[] v0 = new int[p4];
        System.arraycopy(p3, 0, v0, 0, Math.min(p3.length, p4));
        return v0;
    }
}
