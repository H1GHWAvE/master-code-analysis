package com.a.b.l;
final enum class aq extends java.lang.Enum implements java.util.Comparator {
    public static final enum com.a.b.l.aq a;
    private static final synthetic com.a.b.l.aq[] b;

    static aq()
    {
        com.a.b.l.aq.a = new com.a.b.l.aq("INSTANCE");
        com.a.b.l.aq[] v0_3 = new com.a.b.l.aq[1];
        v0_3[0] = com.a.b.l.aq.a;
        com.a.b.l.aq.b = v0_3;
        return;
    }

    private aq(String p2)
    {
        this(p2, 0);
        return;
    }

    private static int a(long[] p6, long[] p7)
    {
        int v1_1 = Math.min(p6.length, p7.length);
        int v0_1 = 0;
        while (v0_1 < v1_1) {
            if (p6[v0_1] == p7[v0_1]) {
                v0_1++;
            } else {
                int v0_3 = com.a.b.l.ap.a(p6[v0_1], p7[v0_1]);
            }
            return v0_3;
        }
        v0_3 = (p6.length - p7.length);
        return v0_3;
    }

    public static com.a.b.l.aq valueOf(String p1)
    {
        return ((com.a.b.l.aq) Enum.valueOf(com.a.b.l.aq, p1));
    }

    public static com.a.b.l.aq[] values()
    {
        return ((com.a.b.l.aq[]) com.a.b.l.aq.b.clone());
    }

    public final synthetic int compare(Object p7, Object p8)
    {
        int v1_1 = Math.min(((long[]) p7).length, ((long[]) p8).length);
        int v0_1 = 0;
        while (v0_1 < v1_1) {
            if (((long[]) p7)[v0_1] == ((long[]) p8)[v0_1]) {
                v0_1++;
            } else {
                int v0_3 = com.a.b.l.ap.a(((long[]) p7)[v0_1], ((long[]) p8)[v0_1]);
            }
            return v0_3;
        }
        v0_3 = (((long[]) p7).length - ((long[]) p8).length);
        return v0_3;
    }
}
