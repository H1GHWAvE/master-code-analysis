package com.a.b.l;
public final class u {
    public static final int a = 8;
    public static final long b = 4611686018427387904;

    private u()
    {
        return;
    }

    private static int a(long p2)
    {
        return ((int) ((p2 >> 32) ^ p2));
    }

    public static int a(long p2, long p4)
    {
        int v0_2;
        if (p2 >= p4) {
            if (p2 <= p4) {
                v0_2 = 0;
            } else {
                v0_2 = 1;
            }
        } else {
            v0_2 = -1;
        }
        return v0_2;
    }

    static int a(long[] p5, long p6, int p8, int p9)
    {
        int v0 = (p9 - 1);
        while (v0 >= p8) {
            if (p5[v0] != p6) {
                v0--;
            }
            return v0;
        }
        v0 = -1;
        return v0;
    }

    private static int a(long[] p8, long[] p9)
    {
        int v1 = 0;
        com.a.b.b.cn.a(p8, "array");
        com.a.b.b.cn.a(p9, "target");
        if (p9.length != 0) {
            int v0_3 = 0;
            while (v0_3 < ((p8.length - p9.length) + 1)) {
                int v2_3 = 0;
                while (v2_3 < p9.length) {
                    if (p8[(v0_3 + v2_3)] != p9[v2_3]) {
                        v0_3++;
                    } else {
                        v2_3++;
                    }
                }
                v1 = v0_3;
            }
            v1 = -1;
        }
        return v1;
    }

    public static long a(byte p8, byte p9, byte p10, byte p11, byte p12, byte p13, byte p14, byte p15)
    {
        return (((((((((((long) p8) & 255) << 56) | ((((long) p9) & 255) << 48)) | ((((long) p10) & 255) << 40)) | ((((long) p11) & 255) << 32)) | ((((long) p12) & 255) << 24)) | ((((long) p13) & 255) << 16)) | ((((long) p14) & 255) << 8)) | (((long) p15) & 255));
    }

    private static long a(byte[] p8)
    {
        long v0_1;
        if (p8.length < 8) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        byte v4_0 = new Object[2];
        v4_0[0] = Integer.valueOf(p8.length);
        v4_0[1] = Integer.valueOf(8);
        com.a.b.b.cn.a(v0_1, "array too small: %s < %s", v4_0);
        return com.a.b.l.u.a(p8[0], p8[1], p8[2], p8[3], p8[4], p8[5], p8[6], p8[7]);
    }

    private static varargs long a(long[] p6)
    {
        long v0_1;
        int v1 = 1;
        if (p6.length <= 0) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        com.a.b.b.cn.a(v0_1);
        long v2_1 = p6[0];
        while (v1 < p6.length) {
            if (p6[v1] < v2_1) {
                v2_1 = p6[v1];
            }
            v1++;
        }
        return v2_1;
    }

    private static com.a.b.b.ak a()
    {
        return com.a.b.l.x.a;
    }

    private static Long a(String p12)
    {
        long v0_10;
        if (!((String) com.a.b.b.cn.a(p12)).isEmpty()) {
            int v5;
            if (p12.charAt(0) != 45) {
                v5 = 0;
            } else {
                v5 = 1;
            }
            long v0_4;
            if (v5 == 0) {
                v0_4 = 0;
            } else {
                v0_4 = 1;
            }
            if (v0_4 != p12.length()) {
                int v2_1 = (v0_4 + 1);
                long v0_6 = (p12.charAt(v0_4) - 48);
                if ((v0_6 >= 0) && (v0_6 <= 9)) {
                    long v0_8 = ((long) (- v0_6));
                    while (v2_1 < p12.length()) {
                        int v3_2 = (v2_1 + 1);
                        int v2_4 = (p12.charAt(v2_1) - 48);
                        if ((v2_4 >= 0) && ((v2_4 <= 9) && (v0_8 >= -8.390303882365713e+246))) {
                            long v0_11 = (v0_8 * 10);
                            if (v0_11 >= (((long) v2_4) + -0.0)) {
                                v0_8 = (v0_11 - ((long) v2_4));
                                v2_1 = v3_2;
                            } else {
                                v0_10 = 0;
                            }
                        } else {
                            v0_10 = 0;
                        }
                    }
                    if (v5 == 0) {
                        if (v0_8 != -0.0) {
                            v0_10 = Long.valueOf((- v0_8));
                        } else {
                            v0_10 = 0;
                        }
                    } else {
                        v0_10 = Long.valueOf(v0_8);
                    }
                } else {
                    v0_10 = 0;
                }
            } else {
                v0_10 = 0;
            }
        } else {
            v0_10 = 0;
        }
        return v0_10;
    }

    private static varargs String a(String p6, long[] p7)
    {
        String v0_5;
        com.a.b.b.cn.a(p6);
        if (p7.length != 0) {
            StringBuilder v1_1 = new StringBuilder((p7.length * 10));
            v1_1.append(p7[0]);
            String v0_4 = 1;
            while (v0_4 < p7.length) {
                v1_1.append(p6).append(p7[v0_4]);
                v0_4++;
            }
            v0_5 = v1_1.toString();
        } else {
            v0_5 = "";
        }
        return v0_5;
    }

    private static boolean a(long[] p7, long p8)
    {
        int v0 = 0;
        int v1 = 0;
        while (v1 < p7.length) {
            if (p7[v1] != p8) {
                v1++;
            } else {
                v0 = 1;
                break;
            }
        }
        return v0;
    }

    private static long[] a(java.util.Collection p8)
    {
        long[] v0_1;
        int v2 = 0;
        if (!(p8 instanceof com.a.b.l.w)) {
            Object[] v3_0 = p8.toArray();
            int v4_0 = v3_0.length;
            long[] v1_0 = new long[v4_0];
            while (v2 < v4_0) {
                v1_0[v2] = ((Number) com.a.b.b.cn.a(v3_0[v2])).longValue();
                v2++;
            }
            v0_1 = v1_0;
        } else {
            long[] v1_1 = ((com.a.b.l.w) p8).size();
            v0_1 = new long[v1_1];
            System.arraycopy(((com.a.b.l.w) p8).a, ((com.a.b.l.w) p8).b, v0_1, 0, v1_1);
        }
        return v0_1;
    }

    private static long[] a(long[] p3, int p4)
    {
        long[] v0 = new long[p4];
        System.arraycopy(p3, 0, v0, 0, Math.min(p3.length, p4));
        return v0;
    }

    private static long[] a(long[] p6, int p7, int p8)
    {
        long[] v0_0;
        if (p7 < 0) {
            v0_0 = 0;
        } else {
            v0_0 = 1;
        }
        long[] v0_1;
        Integer v4_0 = new Object[1];
        v4_0[0] = Integer.valueOf(p7);
        com.a.b.b.cn.a(v0_0, "Invalid minLength: %s", v4_0);
        if (p8 < 0) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        int v1_1 = new Object[1];
        v1_1[0] = Integer.valueOf(p8);
        com.a.b.b.cn.a(v0_1, "Invalid padding: %s", v1_1);
        if (p6.length < p7) {
            int v1_2 = (p7 + p8);
            long[] v0_3 = new long[v1_2];
            System.arraycopy(p6, 0, v0_3, 0, Math.min(p6.length, v1_2));
            p6 = v0_3;
        }
        return p6;
    }

    private static varargs long[] a(long[][] p7)
    {
        int v0_0 = 0;
        int v2_0 = 0;
        while (v0_0 < p7.length) {
            v2_0 += p7[v0_0].length;
            v0_0++;
        }
        long[] v3_1 = new long[v2_0];
        int v4_0 = p7.length;
        int v0_1 = 0;
        int v2_1 = 0;
        while (v2_1 < v4_0) {
            int v5_0 = p7[v2_1];
            System.arraycopy(v5_0, 0, v3_1, v0_1, v5_0.length);
            v0_1 += v5_0.length;
            v2_1++;
        }
        return v3_1;
    }

    private static int b(long[] p3, long p4)
    {
        return com.a.b.l.u.c(p3, p4, 0, p3.length);
    }

    static synthetic int b(long[] p1, long p2, int p4, int p5)
    {
        return com.a.b.l.u.c(p1, p2, p4, p5);
    }

    private static varargs long b(long[] p6)
    {
        long v0_1;
        int v1 = 1;
        if (p6.length <= 0) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        com.a.b.b.cn.a(v0_1);
        long v2_1 = p6[0];
        while (v1 < p6.length) {
            if (p6[v1] > v2_1) {
                v2_1 = p6[v1];
            }
            v1++;
        }
        return v2_1;
    }

    private static java.util.Comparator b()
    {
        return com.a.b.l.v.a;
    }

    private static byte[] b(long p6)
    {
        byte[] v1 = new byte[8];
        int v0 = 7;
        while (v0 >= 0) {
            v1[v0] = ((byte) ((int) (255 & p6)));
            p6 >>= 8;
            v0--;
        }
        return v1;
    }

    private static int c(long[] p3, long p4)
    {
        return com.a.b.l.u.a(p3, p4, 0, p3.length);
    }

    private static int c(long[] p5, long p6, int p8, int p9)
    {
        int v0 = p8;
        while (v0 < p9) {
            if (p5[v0] != p6) {
                v0++;
            }
            return v0;
        }
        v0 = -1;
        return v0;
    }

    private static varargs java.util.List c(long[] p1)
    {
        com.a.b.l.w v0_2;
        if (p1.length != 0) {
            v0_2 = new com.a.b.l.w(p1);
        } else {
            v0_2 = java.util.Collections.emptyList();
        }
        return v0_2;
    }

    private static synthetic int d(long[] p1, long p2, int p4, int p5)
    {
        return com.a.b.l.u.a(p1, p2, p4, p5);
    }
}
