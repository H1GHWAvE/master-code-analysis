package com.a.b.l;
final enum class v extends java.lang.Enum implements java.util.Comparator {
    public static final enum com.a.b.l.v a;
    private static final synthetic com.a.b.l.v[] b;

    static v()
    {
        com.a.b.l.v.a = new com.a.b.l.v("INSTANCE");
        com.a.b.l.v[] v0_3 = new com.a.b.l.v[1];
        v0_3[0] = com.a.b.l.v.a;
        com.a.b.l.v.b = v0_3;
        return;
    }

    private v(String p2)
    {
        this(p2, 0);
        return;
    }

    private static int a(long[] p8, long[] p9)
    {
        int v2 = Math.min(p8.length, p9.length);
        int v1_1 = 0;
        while (v1_1 < v2) {
            int v0_3 = com.a.b.l.u.a(p8[v1_1], p9[v1_1]);
            if (v0_3 == 0) {
                v1_1++;
            }
            return v0_3;
        }
        v0_3 = (p8.length - p9.length);
        return v0_3;
    }

    public static com.a.b.l.v valueOf(String p1)
    {
        return ((com.a.b.l.v) Enum.valueOf(com.a.b.l.v, p1));
    }

    public static com.a.b.l.v[] values()
    {
        return ((com.a.b.l.v[]) com.a.b.l.v.b.clone());
    }

    public final synthetic int compare(Object p9, Object p10)
    {
        int v2 = Math.min(((long[]) p9).length, ((long[]) p10).length);
        int v1_1 = 0;
        while (v1_1 < v2) {
            int v0_3 = com.a.b.l.u.a(((long[]) p9)[v1_1], ((long[]) p10)[v1_1]);
            if (v0_3 == 0) {
                v1_1++;
            }
            return v0_3;
        }
        v0_3 = (((long[]) p9).length - ((long[]) p10).length);
        return v0_3;
    }
}
