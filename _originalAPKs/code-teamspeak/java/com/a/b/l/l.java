package com.a.b.l;
final enum class l extends java.lang.Enum implements java.util.Comparator {
    public static final enum com.a.b.l.l a;
    private static final synthetic com.a.b.l.l[] b;

    static l()
    {
        com.a.b.l.l.a = new com.a.b.l.l("INSTANCE");
        com.a.b.l.l[] v0_3 = new com.a.b.l.l[1];
        v0_3[0] = com.a.b.l.l.a;
        com.a.b.l.l.b = v0_3;
        return;
    }

    private l(String p2)
    {
        this(p2, 0);
        return;
    }

    private static int a(double[] p8, double[] p9)
    {
        int v2 = Math.min(p8.length, p9.length);
        int v1_1 = 0;
        while (v1_1 < v2) {
            int v0_3 = Double.compare(p8[v1_1], p9[v1_1]);
            if (v0_3 == 0) {
                v1_1++;
            }
            return v0_3;
        }
        v0_3 = (p8.length - p9.length);
        return v0_3;
    }

    public static com.a.b.l.l valueOf(String p1)
    {
        return ((com.a.b.l.l) Enum.valueOf(com.a.b.l.l, p1));
    }

    public static com.a.b.l.l[] values()
    {
        return ((com.a.b.l.l[]) com.a.b.l.l.b.clone());
    }

    public final synthetic int compare(Object p9, Object p10)
    {
        int v2 = Math.min(((double[]) p9).length, ((double[]) p10).length);
        int v1_1 = 0;
        while (v1_1 < v2) {
            int v0_3 = Double.compare(((double[]) p9)[v1_1], ((double[]) p10)[v1_1]);
            if (v0_3 == 0) {
                v1_1++;
            }
            return v0_3;
        }
        v0_3 = (((double[]) p9).length - ((double[]) p10).length);
        return v0_3;
    }
}
