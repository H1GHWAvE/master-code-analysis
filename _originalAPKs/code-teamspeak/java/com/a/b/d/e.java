package com.a.b.d;
final class e extends com.a.b.d.gw {
    final synthetic java.util.Map$Entry a;
    final synthetic com.a.b.d.d b;

    e(com.a.b.d.d p1, java.util.Map$Entry p2)
    {
        this.b = p1;
        this.a = p2;
        return;
    }

    protected final java.util.Map$Entry a()
    {
        return this.a;
    }

    protected final bridge synthetic Object k_()
    {
        return this.a;
    }

    public final Object setValue(Object p5)
    {
        com.a.b.b.cn.b(this.b.c.contains(this), "entry no longer in map");
        if (!com.a.b.b.ce.a(p5, this.getValue())) {
            Object v0_9;
            if (this.b.c.b.containsValue(p5)) {
                v0_9 = 0;
            } else {
                v0_9 = 1;
            }
            com.a.b.d.a v1_1 = new Object[1];
            v1_1[0] = p5;
            com.a.b.b.cn.a(v0_9, "value already present: %s", v1_1);
            Object v0_11 = this.a.setValue(p5);
            com.a.b.b.cn.b(com.a.b.b.ce.a(p5, this.b.c.b.get(this.getKey())), "entry no longer in map");
            com.a.b.d.a.a(this.b.c.b, this.getKey(), v0_11, p5);
            p5 = v0_11;
        }
        return p5;
    }
}
