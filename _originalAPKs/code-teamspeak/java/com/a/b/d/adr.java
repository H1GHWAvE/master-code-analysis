package com.a.b.d;
 class adr extends com.a.b.d.adj implements com.a.b.d.aac {
    private static final long i;
    transient java.util.Set f;

    adr(com.a.b.d.aac p1)
    {
        this(p1);
        return;
    }

    synthetic com.a.b.d.vi a()
    {
        return this.c();
    }

    public java.util.Set a(Object p4)
    {
        try {
            return com.a.b.d.acu.a(this.c().a(p4), this.h);
        } catch (Throwable v0_3) {
            throw v0_3;
        }
    }

    public java.util.Set a(Object p3, Iterable p4)
    {
        try {
            return this.c().a(p3, p4);
        } catch (Throwable v0_2) {
            throw v0_2;
        }
    }

    public synthetic java.util.Collection b(Object p2, Iterable p3)
    {
        return this.a(p2, p3);
    }

    public java.util.Set b(Object p3)
    {
        try {
            return this.c().b(p3);
        } catch (Throwable v0_2) {
            throw v0_2;
        }
    }

    com.a.b.d.aac c()
    {
        return ((com.a.b.d.aac) super.a());
    }

    public synthetic java.util.Collection c(Object p2)
    {
        return this.a(p2);
    }

    synthetic Object d()
    {
        return this.c();
    }

    public synthetic java.util.Collection d(Object p2)
    {
        return this.b(p2);
    }

    public final synthetic java.util.Collection k()
    {
        return this.u();
    }

    public final java.util.Set u()
    {
        try {
            if (this.f == null) {
                this.f = com.a.b.d.acu.a(this.c().u(), this.h);
            }
        } catch (java.util.Set v0_5) {
            throw v0_5;
        }
        return this.f;
    }
}
