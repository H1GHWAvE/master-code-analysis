package com.a.b.d;
public final class bk extends com.a.b.d.m {
    private static final int b = 3;
    private static final long c;
    transient int a;

    private bk()
    {
        this(new java.util.HashMap());
        this.a = 3;
        return;
    }

    private bk(int p2, int p3)
    {
        this(com.a.b.d.sz.a(p2));
        com.a.b.d.cl.a(p3, "expectedValuesPerKey");
        this.a = p3;
        return;
    }

    private bk(com.a.b.d.vi p3)
    {
        int v0_2;
        int v1 = p3.p().size();
        if (!(p3 instanceof com.a.b.d.bk)) {
            v0_2 = 3;
        } else {
            v0_2 = ((com.a.b.d.bk) p3).a;
        }
        this(v1, v0_2);
        this.a(p3);
        return;
    }

    private static com.a.b.d.bk a(int p1, int p2)
    {
        return new com.a.b.d.bk(p1, p2);
    }

    private void a(java.io.ObjectInputStream p3)
    {
        p3.defaultReadObject();
        this.a = p3.readInt();
        int v0_1 = p3.readInt();
        this.a(com.a.b.d.sz.a(v0_1));
        com.a.b.d.zz.a(this, p3, v0_1);
        return;
    }

    private void a(java.io.ObjectOutputStream p2)
    {
        p2.defaultWriteObject();
        p2.writeInt(this.a);
        com.a.b.d.zz.a(this, p2);
        return;
    }

    private static com.a.b.d.bk b(com.a.b.d.vi p1)
    {
        return new com.a.b.d.bk(p1);
    }

    private static com.a.b.d.bk t()
    {
        return new com.a.b.d.bk();
    }

    private void u()
    {
        java.util.Iterator v1 = this.e().values().iterator();
        while (v1.hasNext()) {
            ((java.util.ArrayList) ((java.util.Collection) v1.next())).trimToSize();
        }
        return;
    }

    final java.util.List a()
    {
        return new java.util.ArrayList(this.a);
    }

    public final bridge synthetic java.util.List a(Object p2)
    {
        return super.a(p2);
    }

    public final bridge synthetic java.util.List a(Object p2, Iterable p3)
    {
        return super.a(p2, p3);
    }

    public final bridge synthetic boolean a(com.a.b.d.vi p2)
    {
        return super.a(p2);
    }

    public final bridge synthetic boolean a(Object p2, Object p3)
    {
        return super.a(p2, p3);
    }

    public final bridge synthetic java.util.List b(Object p2)
    {
        return super.b(p2);
    }

    public final bridge synthetic java.util.Map b()
    {
        return super.b();
    }

    public final bridge synthetic boolean b(Object p2, Object p3)
    {
        return super.b(p2, p3);
    }

    final synthetic java.util.Collection c()
    {
        return this.a();
    }

    public final bridge synthetic boolean c(Object p2, Iterable p3)
    {
        return super.c(p2, p3);
    }

    public final bridge synthetic boolean c(Object p2, Object p3)
    {
        return super.c(p2, p3);
    }

    public final bridge synthetic boolean equals(Object p2)
    {
        return super.equals(p2);
    }

    public final bridge synthetic int f()
    {
        return super.f();
    }

    public final bridge synthetic boolean f(Object p2)
    {
        return super.f(p2);
    }

    public final bridge synthetic void g()
    {
        super.g();
        return;
    }

    public final bridge synthetic boolean g(Object p2)
    {
        return super.g(p2);
    }

    public final bridge synthetic int hashCode()
    {
        return super.hashCode();
    }

    public final bridge synthetic java.util.Collection i()
    {
        return super.i();
    }

    public final bridge synthetic java.util.Collection k()
    {
        return super.k();
    }

    public final bridge synthetic boolean n()
    {
        return super.n();
    }

    public final bridge synthetic java.util.Set p()
    {
        return super.p();
    }

    public final bridge synthetic com.a.b.d.xc q()
    {
        return super.q();
    }

    public final bridge synthetic String toString()
    {
        return super.toString();
    }
}
