package com.a.b.d;
final class oa implements com.a.b.d.yi {
    private final java.util.Iterator a;
    private boolean b;
    private Object c;

    public oa(java.util.Iterator p2)
    {
        this.a = ((java.util.Iterator) com.a.b.b.cn.a(p2));
        return;
    }

    public final Object a()
    {
        if (!this.b) {
            this.c = this.a.next();
            this.b = 1;
        }
        return this.c;
    }

    public final boolean hasNext()
    {
        if ((!this.b) && (!this.a.hasNext())) {
            int v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        return v0_3;
    }

    public final Object next()
    {
        Object v0_1;
        if (this.b) {
            v0_1 = this.c;
            this.b = 0;
            this.c = 0;
        } else {
            v0_1 = this.a.next();
        }
        return v0_1;
    }

    public final void remove()
    {
        java.util.Iterator v0_1;
        if (this.b) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        com.a.b.b.cn.b(v0_1, "Can\'t remove after you\'ve peeked at next");
        this.a.remove();
        return;
    }
}
