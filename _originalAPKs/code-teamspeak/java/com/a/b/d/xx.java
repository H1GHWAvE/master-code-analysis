package com.a.b.d;
public final class xx extends com.a.b.d.qb implements com.a.b.d.ck {
    private static final com.a.b.d.pn a;
    private static final long c;

    static xx()
    {
        com.a.b.d.xx.a = new com.a.b.d.xy();
        return;
    }

    private xx(java.util.Map p2)
    {
        this(p2, com.a.b.d.xx.a);
        return;
    }

    private static com.a.b.d.xx a(java.util.Map p1)
    {
        return new com.a.b.d.xx(p1);
    }

    private static com.a.b.d.xx b()
    {
        return new com.a.b.d.xx(new java.util.HashMap());
    }

    static synthetic Object b(Class p1, Object p2)
    {
        return com.a.b.d.xx.c(p1, p2);
    }

    private static Object c(Class p1, Object p2)
    {
        return com.a.b.l.z.a(p1).cast(p2);
    }

    public final Object a(Class p2)
    {
        return com.a.b.d.xx.c(p2, this.get(p2));
    }

    public final Object a(Class p2, Object p3)
    {
        return com.a.b.d.xx.c(p2, this.put(p2, p3));
    }

    public final bridge synthetic java.util.Set entrySet()
    {
        return super.entrySet();
    }

    public final bridge synthetic void putAll(java.util.Map p1)
    {
        super.putAll(p1);
        return;
    }
}
