package com.a.b.d;
public final class vc extends java.util.AbstractQueue {
    private static final int g = 1431655765;
    private static final int h = 2863311530;
    private static final int i = 11;
    final int a;
    Object[] b;
    private final com.a.b.d.vf c;
    private final com.a.b.d.vf d;
    private int e;
    private int f;

    private vc(com.a.b.d.ve p3, int p4)
    {
        Object[] v0_1 = com.a.b.d.yd.a(p3.a);
        this.c = new com.a.b.d.vf(this, v0_1);
        this.d = new com.a.b.d.vf(this, v0_1.a());
        this.c.b = this.d;
        this.d.b = this.c;
        this.a = p3.c;
        Object[] v0_6 = new Object[p4];
        this.b = v0_6;
        return;
    }

    synthetic vc(com.a.b.d.ve p1, int p2, byte p3)
    {
        this(p1, p2);
        return;
    }

    private static int a(int p1, int p2)
    {
        return (Math.min((p1 - 1), p2) + 1);
    }

    static int a(int p1, int p2, Iterable p3)
    {
        if (p1 == -1) {
            p1 = 11;
        }
        if ((p3 instanceof java.util.Collection)) {
            p1 = Math.max(p1, ((java.util.Collection) p3).size());
        }
        return com.a.b.d.vc.a(p1, p2);
    }

    private static com.a.b.d.vc a()
    {
        return new com.a.b.d.ve(com.a.b.d.yd.d(), 0).a(java.util.Collections.emptySet());
    }

    private static com.a.b.d.vc a(Iterable p3)
    {
        return new com.a.b.d.ve(com.a.b.d.yd.d(), 0).a(p3);
    }

    private static com.a.b.d.ve a(java.util.Comparator p2)
    {
        return new com.a.b.d.ve(p2, 0);
    }

    private com.a.b.d.vg a(int p7, Object p8)
    {
        com.a.b.d.vf v4 = this.f(p7);
        com.a.b.d.vf v3_0 = p7;
        while(true) {
            com.a.b.d.vg v0_4;
            com.a.b.d.vg v0_1 = ((v3_0 * 2) + 1);
            if (v0_1 >= null) {
                v0_4 = v4.b(((v0_1 * 2) + 1), 4);
            } else {
                v0_4 = -1;
            }
            if (v0_4 <= null) {
                break;
            }
            v4.c.b[v3_0] = v4.c.b[v0_4];
            v3_0 = v0_4;
        }
        com.a.b.d.vg v0_6;
        com.a.b.d.vg v0_5 = v4.a(v3_0, p8);
        if (v0_5 != v3_0) {
            if (v0_5 >= p7) {
                v0_6 = 0;
            } else {
                v0_6 = new com.a.b.d.vg(p8, this.b[p7]);
            }
        } else {
            int v2_4;
            com.a.b.d.vg v0_10 = v4.b(((v3_0 * 2) + 1), 2);
            if ((v0_10 <= null) || (v4.a.compare(v4.c.b[v0_10], p8) >= 0)) {
                v2_4 = v4.b(v3_0, p8);
            } else {
                v4.c.b[v3_0] = v4.c.b[v0_10];
                v4.c.b[v0_10] = p8;
                v2_4 = v0_10;
            }
            if (v2_4 != v3_0) {
                com.a.b.d.vg v0_14;
                if (v2_4 >= p7) {
                    v0_14 = v4.c.b[((p7 - 1) / 2)];
                } else {
                    v0_14 = v4.c.b[p7];
                }
                if (v4.b.a(v2_4, p8) < p7) {
                    v0_6 = new com.a.b.d.vg(p8, v0_14);
                    return v0_6;
                }
            }
            v0_6 = 0;
        }
        return v0_6;
    }

    static synthetic Object[] a(com.a.b.d.vc p1)
    {
        return p1.b;
    }

    private int b()
    {
        int v0 = 1;
        switch (this.e) {
            case 1:
                v0 = 0;
            case 2:
                break;
            default:
                if (this.d.a(1, 2) > 0) {
                    v0 = 2;
                } else {
                }
        }
        return v0;
    }

    static synthetic int b(com.a.b.d.vc p1)
    {
        return p1.e;
    }

    private static com.a.b.d.ve b(int p3)
    {
        int v0 = 0;
        com.a.b.d.ve v1_1 = new com.a.b.d.ve(com.a.b.d.yd.d(), 0);
        if (p3 >= 0) {
            v0 = 1;
        }
        com.a.b.b.cn.a(v0);
        v1_1.b = p3;
        return v1_1;
    }

    static synthetic int c(com.a.b.d.vc p1)
    {
        return p1.f;
    }

    private static com.a.b.d.ve c(int p3)
    {
        int v0 = 0;
        com.a.b.d.ve v1_1 = new com.a.b.d.ve(com.a.b.d.yd.d(), 0);
        if (p3 > 0) {
            v0 = 1;
        }
        com.a.b.b.cn.a(v0);
        v1_1.c = p3;
        return v1_1;
    }

    private Object c()
    {
        return this.poll();
    }

    private Object d()
    {
        return this.remove();
    }

    private Object d(int p2)
    {
        return this.b[p2];
    }

    private Object e()
    {
        return this.peek();
    }

    private Object e(int p2)
    {
        Object v0_1 = this.b[p2];
        this.a(p2);
        return v0_1;
    }

    private com.a.b.d.vf f(int p6)
    {
        com.a.b.d.vf v0_0;
        int v1 = 1;
        int v3_0 = (p6 + 1);
        if (v3_0 <= 0) {
            v0_0 = 0;
        } else {
            v0_0 = 1;
        }
        com.a.b.b.cn.b(v0_0, "negative index");
        if ((1431655765 & v3_0) <= (v3_0 & -1431655766)) {
            v1 = 0;
        }
        com.a.b.d.vf v0_3;
        if (v1 == 0) {
            v0_3 = this.d;
        } else {
            v0_3 = this.c;
        }
        return v0_3;
    }

    private Object f()
    {
        Object v0_2;
        if (!this.isEmpty()) {
            v0_2 = this.e(this.b());
        } else {
            v0_2 = 0;
        }
        return v0_2;
    }

    private Object g()
    {
        if (!this.isEmpty()) {
            return this.e(this.b());
        } else {
            throw new java.util.NoSuchElementException();
        }
    }

    private static boolean g(int p5)
    {
        int v0_0;
        int v1 = 1;
        int v3_0 = (p5 + 1);
        if (v3_0 <= 0) {
            v0_0 = 0;
        } else {
            v0_0 = 1;
        }
        com.a.b.b.cn.b(v0_0, "negative index");
        if ((1431655765 & v3_0) <= (v3_0 & -1431655766)) {
            v1 = 0;
        }
        return v1;
    }

    private Object h()
    {
        Object v0_2;
        if (!this.isEmpty()) {
            v0_2 = this.b[this.b()];
        } else {
            v0_2 = 0;
        }
        return v0_2;
    }

    private boolean i()
    {
        int v1 = 1;
        int v0 = 1;
        while (v0 < this.e) {
            int v3_3;
            int v3_1 = this.f(v0);
            if ((((v0 * 2) + 1) >= v3_1.c.e) || (v3_1.a(v0, ((v0 * 2) + 1)) <= 0)) {
                if ((((v0 * 2) + 2) >= v3_1.c.e) || (v3_1.a(v0, ((v0 * 2) + 2)) <= 0)) {
                    if ((v0 <= 0) || (v3_1.a(v0, ((v0 - 1) / 2)) <= 0)) {
                        if ((v0 <= 2) || (v3_1.a(((((v0 - 1) / 2) - 1) / 2), v0) <= 0)) {
                            v3_3 = 1;
                        } else {
                            v3_3 = 0;
                        }
                    } else {
                        v3_3 = 0;
                    }
                } else {
                    v3_3 = 0;
                }
            } else {
                v3_3 = 0;
            }
            if (v3_3 != 0) {
                v0++;
            } else {
                v1 = 0;
                break;
            }
        }
        return v1;
    }

    private java.util.Comparator j()
    {
        return this.c.a;
    }

    private int k()
    {
        return this.b.length;
    }

    private void l()
    {
        if (this.e > this.b.length) {
            Object[] v0_4;
            Object[] v0_2 = this.b.length;
            if (v0_2 >= 64) {
                v0_4 = com.a.b.j.g.b((v0_2 / 2), 3);
            } else {
                v0_4 = ((v0_2 + 1) * 2);
            }
            Object[] v0_7 = new Object[com.a.b.d.vc.a(v0_4, this.a)];
            System.arraycopy(this.b, 0, v0_7, 0, this.b.length);
            this.b = v0_7;
        }
        return;
    }

    private int m()
    {
        int v0_3;
        int v0_1 = this.b.length;
        if (v0_1 >= 64) {
            v0_3 = com.a.b.j.g.b((v0_1 / 2), 3);
        } else {
            v0_3 = ((v0_1 + 1) * 2);
        }
        return com.a.b.d.vc.a(v0_3, this.a);
    }

    final com.a.b.d.vg a(int p10)
    {
        com.a.b.d.vg v0_16;
        com.a.b.b.cn.b(p10, this.e);
        this.f = (this.f + 1);
        this.e = (this.e - 1);
        if (this.e != p10) {
            com.a.b.d.vg v0_14;
            Object v5 = this.b[this.e];
            com.a.b.d.vg v1_1 = this.f(this.e);
            int v3_0 = ((v1_1.c.e - 1) / 2);
            if (v3_0 == 0) {
                v0_14 = v1_1.c.e;
            } else {
                v0_14 = ((((v3_0 - 1) / 2) * 2) + 2);
                if ((v0_14 == v3_0) || (((v0_14 * 2) + 1) < v1_1.c.e)) {
                } else {
                    int v3_5 = v1_1.c.b[v0_14];
                    if (v1_1.a.compare(v3_5, v5) >= 0) {
                    } else {
                        v1_1.c.b[v0_14] = v5;
                        v1_1.c.b[v1_1.c.e] = v3_5;
                    }
                }
            }
            Object v6 = this.b[this.e];
            this.b[this.e] = 0;
            com.a.b.d.vf v7 = this.f(p10);
            com.a.b.d.vf v4_8 = p10;
            while(true) {
                com.a.b.d.vg v1_10;
                com.a.b.d.vg v1_7 = ((v4_8 * 2) + 1);
                if (v1_7 >= null) {
                    v1_10 = v7.b(((v1_7 * 2) + 1), 4);
                } else {
                    v1_10 = -1;
                }
                if (v1_10 <= null) {
                    break;
                }
                v7.c.b[v4_8] = v7.c.b[v1_10];
                v4_8 = v1_10;
            }
            com.a.b.d.vg v1_12;
            com.a.b.d.vg v1_11 = v7.a(v4_8, v6);
            if (v1_11 != v4_8) {
                if (v1_11 >= p10) {
                    v1_12 = 0;
                } else {
                    v1_12 = new com.a.b.d.vg(v6, this.b[p10]);
                }
            } else {
                int v3_12;
                com.a.b.d.vg v1_17 = v7.b(((v4_8 * 2) + 1), 2);
                if ((v1_17 <= null) || (v7.a.compare(v7.c.b[v1_17], v6) >= 0)) {
                    v3_12 = v7.b(v4_8, v6);
                } else {
                    v7.c.b[v4_8] = v7.c.b[v1_17];
                    v7.c.b[v1_17] = v6;
                    v3_12 = v1_17;
                }
                if (v3_12 != v4_8) {
                    com.a.b.d.vg v1_21;
                    if (v3_12 >= p10) {
                        v1_21 = v7.c.b[((p10 - 1) / 2)];
                    } else {
                        v1_21 = v7.c.b[p10];
                    }
                    if (v7.b.a(v3_12, v6) < p10) {
                        v1_12 = new com.a.b.d.vg(v6, v1_21);
                        if (v0_14 >= p10) {
                            v0_16 = v1_12;
                        } else {
                            if (v1_12 != null) {
                                v0_16 = new com.a.b.d.vg(v5, v1_12.b);
                            } else {
                                v0_16 = new com.a.b.d.vg(v5, v6);
                            }
                        }
                        return v0_16;
                    }
                }
                v1_12 = 0;
            }
        } else {
            this.b[this.e] = 0;
            v0_16 = 0;
        }
        return v0_16;
    }

    public final boolean add(Object p2)
    {
        this.offer(p2);
        return 1;
    }

    public final boolean addAll(java.util.Collection p4)
    {
        int v0_0 = 0;
        java.util.Iterator v1 = p4.iterator();
        while (v1.hasNext()) {
            this.offer(v1.next());
            v0_0 = 1;
        }
        return v0_0;
    }

    public final void clear()
    {
        int v0 = 0;
        while (v0 < this.e) {
            this.b[v0] = 0;
            v0++;
        }
        this.e = 0;
        return;
    }

    public final java.util.Iterator iterator()
    {
        return new com.a.b.d.vh(this, 0);
    }

    public final boolean offer(Object p6)
    {
        com.a.b.b.cn.a(p6);
        this.f = (this.f + 1);
        int v1_0 = this.e;
        this.e = (v1_0 + 1);
        if (this.e > this.b.length) {
            int v0_7;
            int v0_5 = this.b.length;
            if (v0_5 >= 64) {
                v0_7 = com.a.b.j.g.b((v0_5 / 2), 3);
            } else {
                v0_7 = ((v0_5 + 1) * 2);
            }
            int v0_10 = new Object[com.a.b.d.vc.a(v0_7, this.a)];
            System.arraycopy(this.b, 0, v0_10, 0, this.b.length);
            this.b = v0_10;
        }
        int v0_11 = this.f(v1_0);
        Object[] v2_6 = v0_11.b(v1_0, p6);
        if (v2_6 != v1_0) {
            v0_11 = v0_11.b;
            v1_0 = v2_6;
        }
        int v0_16;
        v0_11.a(v1_0, p6);
        if (this.e <= this.a) {
            v0_16 = 1;
        } else {
            int v0_15;
            if (!this.isEmpty()) {
                v0_15 = this.e(this.b());
            } else {
                v0_15 = 0;
            }
            if (v0_15 == p6) {
                v0_16 = 0;
            }
        }
        return v0_16;
    }

    public final Object peek()
    {
        Object v0_2;
        if (!this.isEmpty()) {
            v0_2 = this.b[0];
        } else {
            v0_2 = 0;
        }
        return v0_2;
    }

    public final Object poll()
    {
        Object v0_2;
        if (!this.isEmpty()) {
            v0_2 = this.e(0);
        } else {
            v0_2 = 0;
        }
        return v0_2;
    }

    public final int size()
    {
        return this.e;
    }

    public final Object[] toArray()
    {
        Object[] v0_1 = new Object[this.e];
        System.arraycopy(this.b, 0, v0_1, 0, this.e);
        return v0_1;
    }
}
