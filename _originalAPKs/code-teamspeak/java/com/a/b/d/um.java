package com.a.b.d;
final class um extends com.a.b.d.av {
    private final java.util.NavigableSet a;
    private final com.a.b.b.bj b;

    um(java.util.NavigableSet p2, com.a.b.b.bj p3)
    {
        this.a = ((java.util.NavigableSet) com.a.b.b.cn.a(p2));
        this.b = ((com.a.b.b.bj) com.a.b.b.cn.a(p3));
        return;
    }

    final java.util.Iterator a()
    {
        return com.a.b.d.sz.a(this.a, this.b);
    }

    final java.util.Iterator b()
    {
        return this.descendingMap().entrySet().iterator();
    }

    public final void clear()
    {
        this.a.clear();
        return;
    }

    public final java.util.Comparator comparator()
    {
        return this.a.comparator();
    }

    public final java.util.NavigableMap descendingMap()
    {
        return com.a.b.d.sz.a(this.a.descendingSet(), this.b);
    }

    public final Object get(Object p2)
    {
        int v0_2;
        if (!com.a.b.d.cm.a(this.a, p2)) {
            v0_2 = 0;
        } else {
            v0_2 = this.b.e(p2);
        }
        return v0_2;
    }

    public final java.util.NavigableMap headMap(Object p3, boolean p4)
    {
        return com.a.b.d.sz.a(this.a.headSet(p3, p4), this.b);
    }

    public final java.util.NavigableSet navigableKeySet()
    {
        return com.a.b.d.sz.a(this.a);
    }

    public final int size()
    {
        return this.a.size();
    }

    public final java.util.NavigableMap subMap(Object p3, boolean p4, Object p5, boolean p6)
    {
        return com.a.b.d.sz.a(this.a.subSet(p3, p4, p5, p6), this.b);
    }

    public final java.util.NavigableMap tailMap(Object p3, boolean p4)
    {
        return com.a.b.d.sz.a(this.a.tailSet(p3, p4), this.b);
    }
}
