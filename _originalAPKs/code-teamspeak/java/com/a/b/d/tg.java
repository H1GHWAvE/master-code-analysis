package com.a.b.d;
final class tg extends com.a.b.d.he {
    final synthetic java.util.NavigableSet a;

    tg(java.util.NavigableSet p1)
    {
        this.a = p1;
        return;
    }

    protected final bridge synthetic java.util.Set a()
    {
        return this.a;
    }

    public final boolean add(Object p2)
    {
        throw new UnsupportedOperationException();
    }

    public final boolean addAll(java.util.Collection p2)
    {
        throw new UnsupportedOperationException();
    }

    protected final bridge synthetic java.util.Collection b()
    {
        return this.a;
    }

    protected final bridge synthetic java.util.SortedSet c()
    {
        return this.a;
    }

    protected final java.util.NavigableSet d()
    {
        return this.a;
    }

    public final java.util.NavigableSet descendingSet()
    {
        return com.a.b.d.sz.a(super.descendingSet());
    }

    public final java.util.NavigableSet headSet(Object p2, boolean p3)
    {
        return com.a.b.d.sz.a(super.headSet(p2, p3));
    }

    public final java.util.SortedSet headSet(Object p2)
    {
        return com.a.b.d.sz.a(super.headSet(p2));
    }

    protected final bridge synthetic Object k_()
    {
        return this.a;
    }

    public final java.util.NavigableSet subSet(Object p2, boolean p3, Object p4, boolean p5)
    {
        return com.a.b.d.sz.a(super.subSet(p2, p3, p4, p5));
    }

    public final java.util.SortedSet subSet(Object p2, Object p3)
    {
        return com.a.b.d.sz.a(super.subSet(p2, p3));
    }

    public final java.util.NavigableSet tailSet(Object p2, boolean p3)
    {
        return com.a.b.d.sz.a(super.tailSet(p2, p3));
    }

    public final java.util.SortedSet tailSet(Object p2)
    {
        return com.a.b.d.sz.a(super.tailSet(p2));
    }
}
