package com.a.b.d;
final class afg implements com.a.b.d.yq {
    final com.a.b.d.yl a;
    final synthetic com.a.b.d.afb b;

    afg(com.a.b.d.afb p1, com.a.b.d.yl p2)
    {
        this.b = p1;
        this.a = p2;
        return;
    }

    private static synthetic com.a.b.d.yl a(com.a.b.d.afg p1)
    {
        return p1.a;
    }

    public final com.a.b.d.yl a()
    {
        java.util.NavigableMap v1_9;
        com.a.b.d.yl v0_2 = com.a.b.d.afb.a(this.b).floorEntry(this.a.b);
        if ((v0_2 == null) || (((com.a.b.d.aff) v0_2.getValue()).a.c.a(this.a.b) <= 0)) {
            com.a.b.d.yl v0_11 = ((com.a.b.d.dw) com.a.b.d.afb.a(this.b).ceilingKey(this.a.b));
            if ((v0_11 != null) && (v0_11.a(this.a.c) < 0)) {
                v1_9 = v0_11;
            } else {
                throw new java.util.NoSuchElementException();
            }
        } else {
            v1_9 = this.a.b;
        }
        java.util.Map$Entry v2_2 = com.a.b.d.afb.a(this.b).lowerEntry(this.a.c);
        if (v2_2 != null) {
            com.a.b.d.yl v0_26;
            if (((com.a.b.d.aff) v2_2.getValue()).a.c.a(this.a.c) < 0) {
                v0_26 = ((com.a.b.d.aff) v2_2.getValue()).a.c;
            } else {
                v0_26 = this.a.c;
            }
            return com.a.b.d.yl.a(v1_9, v0_26);
        } else {
            throw new java.util.NoSuchElementException();
        }
    }

    public final Object a(Comparable p2)
    {
        Object v0_4;
        if (!this.a.c(p2)) {
            v0_4 = 0;
        } else {
            Object v0_3 = this.b.b(p2);
            if (v0_3 == null) {
            } else {
                v0_4 = v0_3.getValue();
            }
        }
        return v0_4;
    }

    public final void a(com.a.b.d.yl p3)
    {
        if (p3.b(this.a)) {
            this.b.a(p3.c(this.a));
        }
        return;
    }

    public final void a(com.a.b.d.yl p6, Object p7)
    {
        com.a.b.d.afb v0_1 = this.a.a(p6);
        Object[] v2_1 = new Object[2];
        v2_1[0] = p6;
        v2_1[1] = this.a;
        com.a.b.b.cn.a(v0_1, "Cannot put range %s into a subRangeMap(%s)", v2_1);
        this.b.a(p6, p7);
        return;
    }

    public final void a(com.a.b.d.yq p6)
    {
        if (!p6.d().isEmpty()) {
            Object v0_2 = p6.a();
            com.a.b.d.yl v1_1 = this.a.a(v0_2);
            java.util.Iterator v3_1 = new Object[2];
            v3_1[0] = v0_2;
            v3_1[1] = this.a;
            com.a.b.b.cn.a(v1_1, "Cannot putAll rangeMap with span %s into a subRangeMap(%s)", v3_1);
            com.a.b.d.afb v2_1 = this.b;
            java.util.Iterator v3_2 = p6.d().entrySet().iterator();
            while (v3_2.hasNext()) {
                Object v0_8 = ((java.util.Map$Entry) v3_2.next());
                v2_1.a(((com.a.b.d.yl) v0_8.getKey()), v0_8.getValue());
            }
        }
        return;
    }

    public final java.util.Map$Entry b(Comparable p4)
    {
        java.util.Map$Entry v0_3;
        if (!this.a.c(p4)) {
            v0_3 = 0;
        } else {
            Object v1_0 = this.b.b(p4);
            if (v1_0 == null) {
            } else {
                v0_3 = com.a.b.d.sz.a(((com.a.b.d.yl) v1_0.getKey()).c(this.a), v1_0.getValue());
            }
        }
        return v0_3;
    }

    public final void b()
    {
        this.b.a(this.a);
        return;
    }

    public final com.a.b.d.yq c(com.a.b.d.yl p4)
    {
        com.a.b.d.afg v0_2;
        if (p4.b(this.a)) {
            v0_2 = this.b;
            com.a.b.d.yl v2 = p4.c(this.a);
            if (!v2.equals(com.a.b.d.yl.c())) {
                v0_2 = new com.a.b.d.afg(v0_2, v2);
            }
        } else {
            v0_2 = com.a.b.d.afb.e();
        }
        return v0_2;
    }

    public final java.util.Map d()
    {
        return new com.a.b.d.afh(this);
    }

    public final boolean equals(Object p3)
    {
        int v0_1;
        if (!(p3 instanceof com.a.b.d.yq)) {
            v0_1 = 0;
        } else {
            v0_1 = this.d().equals(((com.a.b.d.yq) p3).d());
        }
        return v0_1;
    }

    public final int hashCode()
    {
        return this.d().hashCode();
    }

    public final String toString()
    {
        return this.d().toString();
    }
}
