package com.a.b.d;
public abstract class mi extends com.a.b.d.bf {
    private static final com.a.b.d.mi a;

    static mi()
    {
        com.a.b.d.mi.a = new com.a.b.d.abt(com.a.b.d.jl.d(), com.a.b.d.lo.h(), com.a.b.d.lo.h());
        return;
    }

    mi()
    {
        return;
    }

    static com.a.b.d.adw b(Object p3, Object p4, Object p5)
    {
        return com.a.b.d.adx.a(com.a.b.b.cn.a(p3), com.a.b.b.cn.a(p4), com.a.b.b.cn.a(p5));
    }

    private static com.a.b.d.mi b(com.a.b.d.adv p6)
    {
        com.a.b.d.zr v6_2;
        if (!(p6 instanceof com.a.b.d.mi)) {
            switch (p6.k()) {
                case 0:
                    v6_2 = com.a.b.d.mi.a;
                    break;
                case 1:
                    com.a.b.d.lo v0_4 = ((com.a.b.d.adw) com.a.b.d.mq.b(p6.e()));
                    v6_2 = new com.a.b.d.aax(v0_4.a(), v0_4.b(), v0_4.c());
                    break;
                default:
                    Object v1_1 = com.a.b.d.lo.i();
                    Object v2_1 = p6.e().iterator();
                    while (v2_1.hasNext()) {
                        com.a.b.d.lo v0_10 = ((com.a.b.d.adw) v2_1.next());
                        v1_1.c(com.a.b.d.mi.b(v0_10.a(), v0_10.b(), v0_10.c()));
                    }
                    v6_2 = com.a.b.d.zr.a(v1_1.b(), 0, 0);
            }
        } else {
            v6_2 = ((com.a.b.d.mi) p6);
        }
        return v6_2;
    }

    private static com.a.b.d.mi c(Object p1, Object p2, Object p3)
    {
        return new com.a.b.d.aax(p1, p2, p3);
    }

    private com.a.b.d.jt g(Object p3)
    {
        com.a.b.b.cn.a(p3);
        return ((com.a.b.d.jt) com.a.b.b.ca.a(((com.a.b.d.jt) this.o().get(p3)), com.a.b.d.jt.k()));
    }

    public static com.a.b.d.mi p()
    {
        return com.a.b.d.mi.a;
    }

    private static com.a.b.d.mj s()
    {
        return new com.a.b.d.mj();
    }

    private com.a.b.d.lo t()
    {
        return ((com.a.b.d.lo) super.e());
    }

    private static com.a.b.d.agi u()
    {
        throw new AssertionError("should never be called");
    }

    private com.a.b.d.iz v()
    {
        return ((com.a.b.d.iz) super.h());
    }

    private com.a.b.d.lo w()
    {
        return this.n().g();
    }

    private com.a.b.d.lo x()
    {
        return this.o().g();
    }

    public final Object a(Object p2, Object p3, Object p4)
    {
        throw new UnsupportedOperationException();
    }

    public final synthetic java.util.Set a()
    {
        return this.o().g();
    }

    public final void a(com.a.b.d.adv p2)
    {
        throw new UnsupportedOperationException();
    }

    public final bridge synthetic boolean a(Object p2)
    {
        return super.a(p2);
    }

    public final boolean a(Object p2, Object p3)
    {
        int v0_1;
        if (this.b(p2, p3) == null) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    public bridge synthetic Object b(Object p2, Object p3)
    {
        return super.b(p2, p3);
    }

    public final synthetic java.util.Set b()
    {
        return this.n().g();
    }

    public final bridge synthetic boolean b(Object p2)
    {
        return super.b(p2);
    }

    public final Object c(Object p2, Object p3)
    {
        throw new UnsupportedOperationException();
    }

    public final bridge synthetic boolean c()
    {
        return super.c();
    }

    public final boolean c(Object p2)
    {
        return ((com.a.b.d.iz) super.h()).contains(p2);
    }

    public synthetic java.util.Map d(Object p2)
    {
        return this.f(p2);
    }

    public final void d()
    {
        throw new UnsupportedOperationException();
    }

    public final synthetic java.util.Map e(Object p3)
    {
        com.a.b.b.cn.a(p3);
        return ((com.a.b.d.jt) com.a.b.b.ca.a(((com.a.b.d.jt) this.o().get(p3)), com.a.b.d.jt.k()));
    }

    public final bridge synthetic java.util.Set e()
    {
        return ((com.a.b.d.lo) super.e());
    }

    public bridge synthetic boolean equals(Object p2)
    {
        return super.equals(p2);
    }

    public com.a.b.d.jt f(Object p3)
    {
        com.a.b.b.cn.a(p3);
        return ((com.a.b.d.jt) com.a.b.b.ca.a(((com.a.b.d.jt) this.n().get(p3)), com.a.b.d.jt.k()));
    }

    synthetic java.util.Set f()
    {
        return this.q();
    }

    final synthetic java.util.Iterator g()
    {
        throw new AssertionError("should never be called");
    }

    public final bridge synthetic java.util.Collection h()
    {
        return ((com.a.b.d.iz) super.h());
    }

    public bridge synthetic int hashCode()
    {
        return super.hashCode();
    }

    synthetic java.util.Collection i()
    {
        return this.r();
    }

    public synthetic java.util.Map l()
    {
        return this.n();
    }

    public synthetic java.util.Map m()
    {
        return this.o();
    }

    final java.util.Iterator m_()
    {
        throw new AssertionError("should never be called");
    }

    public abstract com.a.b.d.jt n();

    public abstract com.a.b.d.jt o();

    abstract com.a.b.d.lo q();

    abstract com.a.b.d.iz r();

    public bridge synthetic String toString()
    {
        return super.toString();
    }
}
