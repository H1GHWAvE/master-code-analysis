package com.a.b.d;
public final class fd extends com.a.b.d.a {
    private static final long d;
    private transient Class b;
    private transient Class c;

    private fd(Class p3, Class p4)
    {
        this(com.a.b.d.agm.a(new java.util.EnumMap(p3)), com.a.b.d.agm.a(new java.util.EnumMap(p4)));
        this.b = p3;
        this.c = p4;
        return;
    }

    private static com.a.b.d.fd a(Class p1, Class p2)
    {
        return new com.a.b.d.fd(p1, p2);
    }

    static Class a(java.util.Map p1)
    {
        Class v0_8;
        if (!(p1 instanceof com.a.b.d.fd)) {
            if (!(p1 instanceof com.a.b.d.fe)) {
                Class v0_3;
                if (p1.isEmpty()) {
                    v0_3 = 0;
                } else {
                    v0_3 = 1;
                }
                com.a.b.b.cn.a(v0_3);
                v0_8 = ((Enum) p1.keySet().iterator().next()).getDeclaringClass();
            } else {
                v0_8 = ((com.a.b.d.fe) p1).b;
            }
        } else {
            v0_8 = ((com.a.b.d.fd) p1).b;
        }
        return v0_8;
    }

    private static Enum a(Enum p1)
    {
        return ((Enum) com.a.b.b.cn.a(p1));
    }

    private void a(java.io.ObjectInputStream p4)
    {
        p4.defaultReadObject();
        this.b = ((Class) p4.readObject());
        this.c = ((Class) p4.readObject());
        this.a(com.a.b.d.agm.a(new java.util.EnumMap(this.b)), com.a.b.d.agm.a(new java.util.EnumMap(this.c)));
        com.a.b.d.zz.a(this, p4);
        return;
    }

    private void a(java.io.ObjectOutputStream p2)
    {
        p2.defaultWriteObject();
        p2.writeObject(this.b);
        p2.writeObject(this.c);
        com.a.b.d.zz.a(this, p2);
        return;
    }

    private static com.a.b.d.fd b(java.util.Map p3)
    {
        Class v0_7;
        Class v1 = com.a.b.d.fd.a(p3);
        if (!(p3 instanceof com.a.b.d.fd)) {
            Class v0_2;
            if (p3.isEmpty()) {
                v0_2 = 0;
            } else {
                v0_2 = 1;
            }
            com.a.b.b.cn.a(v0_2);
            v0_7 = ((Enum) p3.values().iterator().next()).getDeclaringClass();
        } else {
            v0_7 = ((com.a.b.d.fd) p3).c;
        }
        com.a.b.d.fd v2_1 = new com.a.b.d.fd(v1, v0_7);
        v2_1.putAll(p3);
        return v2_1;
    }

    private static Enum b(Enum p1)
    {
        return ((Enum) com.a.b.b.cn.a(p1));
    }

    private static Class c(java.util.Map p1)
    {
        Class v0_7;
        if (!(p1 instanceof com.a.b.d.fd)) {
            Class v0_2;
            if (p1.isEmpty()) {
                v0_2 = 0;
            } else {
                v0_2 = 1;
            }
            com.a.b.b.cn.a(v0_2);
            v0_7 = ((Enum) p1.values().iterator().next()).getDeclaringClass();
        } else {
            v0_7 = ((com.a.b.d.fd) p1).c;
        }
        return v0_7;
    }

    private Class d()
    {
        return this.b;
    }

    private Class e()
    {
        return this.c;
    }

    final bridge synthetic Object a(Object p2)
    {
        return ((Enum) com.a.b.b.cn.a(((Enum) p2)));
    }

    public final bridge synthetic com.a.b.d.bw b()
    {
        return super.b();
    }

    final synthetic Object b(Object p2)
    {
        return ((Enum) com.a.b.b.cn.a(((Enum) p2)));
    }

    public final bridge synthetic void clear()
    {
        super.clear();
        return;
    }

    public final bridge synthetic boolean containsValue(Object p2)
    {
        return super.containsValue(p2);
    }

    public final bridge synthetic java.util.Set entrySet()
    {
        return super.entrySet();
    }

    public final bridge synthetic java.util.Set j_()
    {
        return super.j_();
    }

    public final bridge synthetic java.util.Set keySet()
    {
        return super.keySet();
    }

    public final bridge synthetic void putAll(java.util.Map p1)
    {
        super.putAll(p1);
        return;
    }
}
