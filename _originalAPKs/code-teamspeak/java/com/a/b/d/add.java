package com.a.b.d;
 class add extends com.a.b.d.adn implements java.util.Collection {
    private static final long a;

    private add(java.util.Collection p1, Object p2)
    {
        this(p1, p2);
        return;
    }

    synthetic add(java.util.Collection p1, Object p2, byte p3)
    {
        this(p1, p2);
        return;
    }

    public boolean add(Object p3)
    {
        try {
            return this.b().add(p3);
        } catch (Throwable v0_2) {
            throw v0_2;
        }
    }

    public boolean addAll(java.util.Collection p3)
    {
        try {
            return this.b().addAll(p3);
        } catch (Throwable v0_2) {
            throw v0_2;
        }
    }

    java.util.Collection b()
    {
        return ((java.util.Collection) super.d());
    }

    public void clear()
    {
        try {
            this.b().clear();
            return;
        } catch (Throwable v0_1) {
            throw v0_1;
        }
    }

    public boolean contains(Object p3)
    {
        try {
            return this.b().contains(p3);
        } catch (Throwable v0_2) {
            throw v0_2;
        }
    }

    public boolean containsAll(java.util.Collection p3)
    {
        try {
            return this.b().containsAll(p3);
        } catch (Throwable v0_2) {
            throw v0_2;
        }
    }

    synthetic Object d()
    {
        return this.b();
    }

    public boolean isEmpty()
    {
        try {
            return this.b().isEmpty();
        } catch (Throwable v0_2) {
            throw v0_2;
        }
    }

    public java.util.Iterator iterator()
    {
        return this.b().iterator();
    }

    public boolean remove(Object p3)
    {
        try {
            return this.b().remove(p3);
        } catch (Throwable v0_2) {
            throw v0_2;
        }
    }

    public boolean removeAll(java.util.Collection p3)
    {
        try {
            return this.b().removeAll(p3);
        } catch (Throwable v0_2) {
            throw v0_2;
        }
    }

    public boolean retainAll(java.util.Collection p3)
    {
        try {
            return this.b().retainAll(p3);
        } catch (Throwable v0_2) {
            throw v0_2;
        }
    }

    public int size()
    {
        try {
            return this.b().size();
        } catch (Throwable v0_2) {
            throw v0_2;
        }
    }

    public Object[] toArray()
    {
        try {
            return this.b().toArray();
        } catch (Throwable v0_2) {
            throw v0_2;
        }
    }

    public Object[] toArray(Object[] p3)
    {
        try {
            return this.b().toArray(p3);
        } catch (Throwable v0_2) {
            throw v0_2;
        }
    }
}
