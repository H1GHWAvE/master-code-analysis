package com.a.b.d;
final class hs implements java.io.Serializable {
    final java.util.Comparator a;
    final boolean b;
    final Object c;
    final com.a.b.d.ce d;
    final boolean e;
    final Object f;
    final com.a.b.d.ce g;
    private transient com.a.b.d.hs h;

    hs(java.util.Comparator p7, boolean p8, Object p9, com.a.b.d.ce p10, boolean p11, Object p12, com.a.b.d.ce p13)
    {
        int v1 = 1;
        this.a = ((java.util.Comparator) com.a.b.b.cn.a(p7));
        this.b = p8;
        this.e = p11;
        this.c = p9;
        this.d = ((com.a.b.d.ce) com.a.b.b.cn.a(p10));
        this.f = p12;
        this.g = ((com.a.b.d.ce) com.a.b.b.cn.a(p13));
        if (p8) {
            p7.compare(p9, p9);
        }
        if (p11) {
            p7.compare(p12, p12);
        }
        if ((p8) && (p11)) {
            int v0_6;
            com.a.b.d.ce v3_0 = p7.compare(p9, p12);
            if (v3_0 > null) {
                v0_6 = 0;
            } else {
                v0_6 = 1;
            }
            Object[] v5_1 = new Object[2];
            v5_1[0] = p9;
            v5_1[1] = p12;
            com.a.b.b.cn.a(v0_6, "lowerEndpoint (%s) > upperEndpoint (%s)", v5_1);
            if (v3_0 == null) {
                int v0_8;
                if (p10 == com.a.b.d.ce.a) {
                    v0_8 = 0;
                } else {
                    v0_8 = 1;
                }
                if (p13 == com.a.b.d.ce.a) {
                    v1 = 0;
                }
                com.a.b.b.cn.a((v0_8 | v1));
            }
        }
        return;
    }

    private static com.a.b.d.hs a(com.a.b.d.yl p8)
    {
        int v3;
        if (!p8.d()) {
            v3 = 0;
        } else {
            v3 = p8.b.c();
        }
        com.a.b.d.ce v4;
        if (!p8.d()) {
            v4 = com.a.b.d.ce.a;
        } else {
            v4 = p8.b.a();
        }
        com.a.b.d.dw v6;
        if (!p8.e()) {
            v6 = 0;
        } else {
            v6 = p8.c.c();
        }
        com.a.b.d.ce v7;
        if (!p8.e()) {
            v7 = com.a.b.d.ce.a;
        } else {
            v7 = p8.c.b();
        }
        return new com.a.b.d.hs(com.a.b.d.yd.d(), p8.d(), v3, v4, p8.e(), v6, v7);
    }

    static com.a.b.d.hs a(java.util.Comparator p8)
    {
        return new com.a.b.d.hs(p8, 0, 0, com.a.b.d.ce.a, 0, 0, com.a.b.d.ce.a);
    }

    private static com.a.b.d.hs a(java.util.Comparator p8, Object p9, com.a.b.d.ce p10)
    {
        return new com.a.b.d.hs(p8, 1, p9, p10, 0, 0, com.a.b.d.ce.a);
    }

    private static com.a.b.d.hs a(java.util.Comparator p8, Object p9, com.a.b.d.ce p10, Object p11, com.a.b.d.ce p12)
    {
        return new com.a.b.d.hs(p8, 1, p9, p10, 1, p11, p12);
    }

    private java.util.Comparator a()
    {
        return this.a;
    }

    private static com.a.b.d.hs b(java.util.Comparator p8, Object p9, com.a.b.d.ce p10)
    {
        return new com.a.b.d.hs(p8, 0, 0, com.a.b.d.ce.a, 1, p9, p10);
    }

    private boolean b()
    {
        return this.b;
    }

    private boolean c()
    {
        return this.e;
    }

    private boolean d()
    {
        if (((!this.e) || (!this.a(this.f))) && ((!this.b) || (!this.b(this.c)))) {
            int v0_6 = 0;
        } else {
            v0_6 = 1;
        }
        return v0_6;
    }

    private com.a.b.d.hs e()
    {
        com.a.b.d.hs v0_0 = this.h;
        if (v0_0 == null) {
            v0_0 = new com.a.b.d.hs(com.a.b.d.yd.a(this.a).a(), this.e, this.f, this.g, this.b, this.c, this.d);
            v0_0.h = this;
            this.h = v0_0;
        }
        return v0_0;
    }

    private Object f()
    {
        return this.c;
    }

    private com.a.b.d.ce g()
    {
        return this.d;
    }

    private Object h()
    {
        return this.f;
    }

    private com.a.b.d.ce i()
    {
        return this.g;
    }

    final com.a.b.d.hs a(com.a.b.d.hs p11)
    {
        com.a.b.d.ce v0_3;
        java.util.Comparator v1_2;
        com.a.b.d.ce v2_1;
        com.a.b.b.cn.a(p11);
        com.a.b.b.cn.a(this.a.equals(p11.a));
        com.a.b.d.ce v0_2 = this.b;
        com.a.b.d.ce v2_0 = this.c;
        java.util.Comparator v1_1 = this.d;
        if (this.b) {
            if (p11.b) {
                com.a.b.d.ce v3_3 = this.a.compare(this.c, p11.c);
                if ((v3_3 < null) || ((v3_3 == null) && (p11.d == com.a.b.d.ce.a))) {
                    v1_2 = p11.c;
                    v2_1 = v0_2;
                    v0_3 = p11.d;
                    com.a.b.d.ce v5_3;
                    com.a.b.d.ce v3_5 = this.e;
                    Object v6 = this.f;
                    com.a.b.d.ce v7 = this.g;
                    if (this.e) {
                        if (p11.e) {
                            com.a.b.d.ce v4_5 = this.a.compare(this.f, p11.f);
                            if ((v4_5 > null) || ((v4_5 == null) && (p11.g == com.a.b.d.ce.a))) {
                                v6 = p11.f;
                                v7 = p11.g;
                                v5_3 = v3_5;
                                if ((v2_1 == null) || (v5_3 == null)) {
                                    com.a.b.d.ce v4_7 = v0_3;
                                    com.a.b.d.ce v3_10 = v1_2;
                                } else {
                                    com.a.b.d.ce v3_7 = this.a.compare(v1_2, v6);
                                    if ((v3_7 <= null) && ((v3_7 != null) || ((v0_3 != com.a.b.d.ce.a) || (v7 != com.a.b.d.ce.a)))) {
                                    } else {
                                        v7 = com.a.b.d.ce.b;
                                        v4_7 = com.a.b.d.ce.a;
                                        v3_10 = v6;
                                    }
                                }
                                return new com.a.b.d.hs(this.a, v2_1, v3_10, v4_7, v5_3, v6, v7);
                            }
                        }
                        v5_3 = v3_5;
                    } else {
                        v3_5 = p11.e;
                    }
                }
            }
            v1_2 = v2_0;
            v2_1 = v0_2;
            v0_3 = v1_1;
        } else {
            v0_2 = p11.b;
        }
    }

    final boolean a(Object p7)
    {
        int v1 = 1;
        int v2 = 0;
        if (this.b) {
            int v3_1;
            int v0_2 = this.a.compare(p7, this.c);
            if (v0_2 >= 0) {
                v3_1 = 0;
            } else {
                v3_1 = 1;
            }
            int v0_3;
            if (v0_2 != 0) {
                v0_3 = 0;
            } else {
                v0_3 = 1;
            }
            if (this.d != com.a.b.d.ce.a) {
                v1 = 0;
            }
            v2 = (v3_1 | (v0_3 & v1));
        }
        return v2;
    }

    final boolean b(Object p7)
    {
        int v1 = 1;
        int v2 = 0;
        if (this.e) {
            int v3_1;
            int v0_2 = this.a.compare(p7, this.f);
            if (v0_2 <= 0) {
                v3_1 = 0;
            } else {
                v3_1 = 1;
            }
            int v0_3;
            if (v0_2 != 0) {
                v0_3 = 0;
            } else {
                v0_3 = 1;
            }
            if (this.g != com.a.b.d.ce.a) {
                v1 = 0;
            }
            v2 = (v3_1 | (v0_3 & v1));
        }
        return v2;
    }

    final boolean c(Object p2)
    {
        if ((this.a(p2)) || (this.b(p2))) {
            int v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    public final boolean equals(Object p4)
    {
        int v0 = 0;
        if (((p4 instanceof com.a.b.d.hs)) && ((this.a.equals(((com.a.b.d.hs) p4).a)) && ((this.b == ((com.a.b.d.hs) p4).b) && ((this.e == ((com.a.b.d.hs) p4).e) && ((this.d.equals(((com.a.b.d.hs) p4).d)) && ((this.g.equals(((com.a.b.d.hs) p4).g)) && ((com.a.b.b.ce.a(this.c, ((com.a.b.d.hs) p4).c)) && (com.a.b.b.ce.a(this.f, ((com.a.b.d.hs) p4).f))))))))) {
            v0 = 1;
        }
        return v0;
    }

    public final int hashCode()
    {
        int v0_1 = new Object[5];
        v0_1[0] = this.a;
        v0_1[1] = this.c;
        v0_1[2] = this.d;
        v0_1[3] = this.f;
        v0_1[4] = this.g;
        return java.util.Arrays.hashCode(v0_1);
    }

    public final String toString()
    {
        String v0_4;
        StringBuilder v1_2 = new StringBuilder().append(this.a).append(":");
        if (this.d != com.a.b.d.ce.b) {
            v0_4 = 40;
        } else {
            v0_4 = 91;
        }
        String v0_6;
        StringBuilder v1_3 = v1_2.append(v0_4);
        if (!this.b) {
            v0_6 = "-\u221e";
        } else {
            v0_6 = this.c;
        }
        String v0_9;
        StringBuilder v1_5 = v1_3.append(v0_6).append(44);
        if (!this.e) {
            v0_9 = "\u221e";
        } else {
            v0_9 = this.f;
        }
        String v0_11;
        StringBuilder v1_6 = v1_5.append(v0_9);
        if (this.g != com.a.b.d.ce.b) {
            v0_11 = 41;
        } else {
            v0_11 = 93;
        }
        return v1_6.append(v0_11).toString();
    }
}
