package com.a.b.d;
final class acu {

    private acu()
    {
        return;
    }

    private static com.a.b.d.aac a(com.a.b.d.aac p1)
    {
        if ((!(p1 instanceof com.a.b.d.adr)) && (!(p1 instanceof com.a.b.d.lr))) {
            p1 = new com.a.b.d.adr(p1);
        }
        return p1;
    }

    private static com.a.b.d.abs a(com.a.b.d.abs p1)
    {
        if (!(p1 instanceof com.a.b.d.adu)) {
            p1 = new com.a.b.d.adu(p1);
        }
        return p1;
    }

    private static com.a.b.d.bw a(com.a.b.d.bw p1)
    {
        if ((!(p1 instanceof com.a.b.d.adc)) && (!(p1 instanceof com.a.b.d.it))) {
            p1 = new com.a.b.d.adc(p1);
        }
        return p1;
    }

    private static com.a.b.d.ou a(com.a.b.d.ou p1)
    {
        if ((!(p1 instanceof com.a.b.d.adh)) && (!(p1 instanceof com.a.b.d.jr))) {
            p1 = new com.a.b.d.adh(p1);
        }
        return p1;
    }

    private static com.a.b.d.vi a(com.a.b.d.vi p1)
    {
        if ((!(p1 instanceof com.a.b.d.adj)) && (!(p1 instanceof com.a.b.d.kk))) {
            p1 = new com.a.b.d.adj(p1);
        }
        return p1;
    }

    private static com.a.b.d.xc a(com.a.b.d.xc p1, Object p2)
    {
        if ((!(p1 instanceof com.a.b.d.adk)) && (!(p1 instanceof com.a.b.d.ku))) {
            p1 = new com.a.b.d.adk(p1, p2);
        }
        return p1;
    }

    static java.util.Collection a(java.util.Collection p2, Object p3)
    {
        return new com.a.b.d.add(p2, p3, 0);
    }

    private static java.util.Deque a(java.util.Deque p1)
    {
        return new com.a.b.d.ade(p1);
    }

    static java.util.List a(java.util.List p1, Object p2)
    {
        com.a.b.d.adg v0_2;
        if (!(p1 instanceof java.util.RandomAccess)) {
            v0_2 = new com.a.b.d.adg(p1, p2);
        } else {
            v0_2 = new com.a.b.d.adp(p1, p2);
        }
        return v0_2;
    }

    static synthetic java.util.Map$Entry a(java.util.Map$Entry p1, Object p2)
    {
        com.a.b.d.adf v0_1;
        if (p1 != null) {
            v0_1 = new com.a.b.d.adf(p1, p2);
        } else {
            v0_1 = 0;
        }
        return v0_1;
    }

    private static java.util.Map a(java.util.Map p1, Object p2)
    {
        return new com.a.b.d.adi(p1, p2);
    }

    private static java.util.NavigableMap a(java.util.NavigableMap p1)
    {
        return com.a.b.d.acu.a(p1, 0);
    }

    static java.util.NavigableMap a(java.util.NavigableMap p1, Object p2)
    {
        return new com.a.b.d.adl(p1, p2);
    }

    private static java.util.NavigableSet a(java.util.NavigableSet p1)
    {
        return com.a.b.d.acu.a(p1, 0);
    }

    static java.util.NavigableSet a(java.util.NavigableSet p1, Object p2)
    {
        return new com.a.b.d.adm(p1, p2);
    }

    private static java.util.Queue a(java.util.Queue p1)
    {
        if (!(p1 instanceof com.a.b.d.ado)) {
            p1 = new com.a.b.d.ado(p1);
        }
        return p1;
    }

    static java.util.Set a(java.util.Set p1, Object p2)
    {
        return new com.a.b.d.adq(p1, p2);
    }

    static java.util.SortedMap a(java.util.SortedMap p1, Object p2)
    {
        return new com.a.b.d.ads(p1, p2);
    }

    static java.util.SortedSet a(java.util.SortedSet p1, Object p2)
    {
        return new com.a.b.d.adt(p1, p2);
    }

    static synthetic java.util.Collection b(java.util.Collection p1, Object p2)
    {
        java.util.Collection v0_3;
        if (!(p1 instanceof java.util.SortedSet)) {
            if (!(p1 instanceof java.util.Set)) {
                if (!(p1 instanceof java.util.List)) {
                    v0_3 = com.a.b.d.acu.a(p1, p2);
                } else {
                    v0_3 = com.a.b.d.acu.a(((java.util.List) p1), p2);
                }
            } else {
                v0_3 = com.a.b.d.acu.a(((java.util.Set) p1), p2);
            }
        } else {
            v0_3 = com.a.b.d.acu.a(((java.util.SortedSet) p1), p2);
        }
        return v0_3;
    }

    private static synthetic java.util.List b(java.util.List p1, Object p2)
    {
        return com.a.b.d.acu.a(p1, p2);
    }

    private static java.util.Map$Entry b(java.util.Map$Entry p1, Object p2)
    {
        com.a.b.d.adf v0_1;
        if (p1 != null) {
            v0_1 = new com.a.b.d.adf(p1, p2);
        } else {
            v0_1 = 0;
        }
        return v0_1;
    }

    static synthetic java.util.Set b(java.util.Set p1, Object p2)
    {
        java.util.Set v0_1;
        if (!(p1 instanceof java.util.SortedSet)) {
            v0_1 = com.a.b.d.acu.a(p1, p2);
        } else {
            v0_1 = com.a.b.d.acu.a(((java.util.SortedSet) p1), p2);
        }
        return v0_1;
    }

    private static synthetic java.util.SortedSet b(java.util.SortedSet p1, Object p2)
    {
        return com.a.b.d.acu.a(p1, p2);
    }

    private static java.util.Collection c(java.util.Collection p1, Object p2)
    {
        java.util.Collection v0_3;
        if (!(p1 instanceof java.util.SortedSet)) {
            if (!(p1 instanceof java.util.Set)) {
                if (!(p1 instanceof java.util.List)) {
                    v0_3 = com.a.b.d.acu.a(p1, p2);
                } else {
                    v0_3 = com.a.b.d.acu.a(((java.util.List) p1), p2);
                }
            } else {
                v0_3 = com.a.b.d.acu.a(((java.util.Set) p1), p2);
            }
        } else {
            v0_3 = com.a.b.d.acu.a(((java.util.SortedSet) p1), p2);
        }
        return v0_3;
    }

    private static java.util.Set c(java.util.Set p1, Object p2)
    {
        java.util.Set v0_1;
        if (!(p1 instanceof java.util.SortedSet)) {
            v0_1 = com.a.b.d.acu.a(p1, p2);
        } else {
            v0_1 = com.a.b.d.acu.a(((java.util.SortedSet) p1), p2);
        }
        return v0_1;
    }

    private static synthetic java.util.Collection d(java.util.Collection p1, Object p2)
    {
        return com.a.b.d.acu.a(p1, p2);
    }
}
