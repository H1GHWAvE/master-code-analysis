package com.a.b.d;
final class adm extends com.a.b.d.adt implements java.util.NavigableSet {
    private static final long b;
    transient java.util.NavigableSet a;

    adm(java.util.NavigableSet p1, Object p2)
    {
        this(p1, p2);
        return;
    }

    private java.util.NavigableSet e()
    {
        return ((java.util.NavigableSet) super.a());
    }

    final bridge synthetic java.util.SortedSet a()
    {
        return ((java.util.NavigableSet) super.a());
    }

    final synthetic java.util.Collection b()
    {
        return ((java.util.NavigableSet) super.a());
    }

    final synthetic java.util.Set c()
    {
        return ((java.util.NavigableSet) super.a());
    }

    public final Object ceiling(Object p3)
    {
        try {
            return ((java.util.NavigableSet) super.a()).ceiling(p3);
        } catch (Throwable v0_3) {
            throw v0_3;
        }
    }

    final synthetic Object d()
    {
        return ((java.util.NavigableSet) super.a());
    }

    public final java.util.Iterator descendingIterator()
    {
        return ((java.util.NavigableSet) super.a()).descendingIterator();
    }

    public final java.util.NavigableSet descendingSet()
    {
        try {
            java.util.NavigableSet v0_1;
            if (this.a != null) {
                v0_1 = this.a;
            } else {
                v0_1 = com.a.b.d.acu.a(((java.util.NavigableSet) super.a()).descendingSet(), this.h);
                this.a = v0_1;
            }
        } catch (java.util.NavigableSet v0_5) {
            throw v0_5;
        }
        return v0_1;
    }

    public final Object floor(Object p3)
    {
        try {
            return ((java.util.NavigableSet) super.a()).floor(p3);
        } catch (Throwable v0_3) {
            throw v0_3;
        }
    }

    public final java.util.NavigableSet headSet(Object p4, boolean p5)
    {
        try {
            return com.a.b.d.acu.a(((java.util.NavigableSet) super.a()).headSet(p4, p5), this.h);
        } catch (Throwable v0_4) {
            throw v0_4;
        }
    }

    public final java.util.SortedSet headSet(Object p2)
    {
        return this.headSet(p2, 0);
    }

    public final Object higher(Object p3)
    {
        try {
            return ((java.util.NavigableSet) super.a()).higher(p3);
        } catch (Throwable v0_3) {
            throw v0_3;
        }
    }

    public final Object lower(Object p3)
    {
        try {
            return ((java.util.NavigableSet) super.a()).lower(p3);
        } catch (Throwable v0_3) {
            throw v0_3;
        }
    }

    public final Object pollFirst()
    {
        try {
            return ((java.util.NavigableSet) super.a()).pollFirst();
        } catch (Throwable v0_3) {
            throw v0_3;
        }
    }

    public final Object pollLast()
    {
        try {
            return ((java.util.NavigableSet) super.a()).pollLast();
        } catch (Throwable v0_3) {
            throw v0_3;
        }
    }

    public final java.util.NavigableSet subSet(Object p4, boolean p5, Object p6, boolean p7)
    {
        try {
            return com.a.b.d.acu.a(((java.util.NavigableSet) super.a()).subSet(p4, p5, p6, p7), this.h);
        } catch (Throwable v0_4) {
            throw v0_4;
        }
    }

    public final java.util.SortedSet subSet(Object p3, Object p4)
    {
        return this.subSet(p3, 1, p4, 0);
    }

    public final java.util.NavigableSet tailSet(Object p4, boolean p5)
    {
        try {
            return com.a.b.d.acu.a(((java.util.NavigableSet) super.a()).tailSet(p4, p5), this.h);
        } catch (Throwable v0_4) {
            throw v0_4;
        }
    }

    public final java.util.SortedSet tailSet(Object p2)
    {
        return this.tailSet(p2, 1);
    }
}
