package com.a.b.d;
public abstract class hq extends com.a.b.d.hj implements com.a.b.d.abs {

    protected hq()
    {
        return;
    }

    protected final synthetic com.a.b.d.aac a()
    {
        return this.d();
    }

    public final synthetic java.util.Set a(Object p2)
    {
        return this.h(p2);
    }

    public final synthetic java.util.Set a(Object p2, Iterable p3)
    {
        return this.d(p2, p3);
    }

    public final synthetic java.util.Collection b(Object p2, Iterable p3)
    {
        return this.d(p2, p3);
    }

    public final synthetic java.util.Set b(Object p2)
    {
        return this.i(p2);
    }

    protected final synthetic com.a.b.d.vi c()
    {
        return this.d();
    }

    public final synthetic java.util.Collection c(Object p2)
    {
        return this.h(p2);
    }

    protected abstract com.a.b.d.abs d();

    public final synthetic java.util.Collection d(Object p2)
    {
        return this.i(p2);
    }

    public final java.util.SortedSet d(Object p2, Iterable p3)
    {
        return this.d().d(p2, p3);
    }

    public final java.util.Comparator d_()
    {
        return this.d().d_();
    }

    public final java.util.SortedSet h(Object p2)
    {
        return this.d().h(p2);
    }

    public final java.util.SortedSet i(Object p2)
    {
        return this.d().i(p2);
    }

    protected final synthetic Object k_()
    {
        return this.d();
    }
}
