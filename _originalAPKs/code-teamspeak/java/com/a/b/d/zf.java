package com.a.b.d;
final class zf extends com.a.b.d.jt {
    private static final double d = 4608083138725491507;
    private static final long e;
    private final transient com.a.b.d.ka[] a;
    private final transient com.a.b.d.ka[] b;
    private final transient int c;

    zf(int p7, com.a.b.d.kb[] p8)
    {
        com.a.b.d.zi v0_0 = new com.a.b.d.ka[p7];
        this.a = v0_0;
        com.a.b.d.zi v0_2 = com.a.b.d.iq.a(p7, 1.2);
        com.a.b.d.zi v1_0 = new com.a.b.d.ka[v0_2];
        this.b = v1_0;
        this.c = (v0_2 - 1);
        int v2 = 0;
        while (v2 < p7) {
            com.a.b.d.zi v0_5 = p8[v2];
            Object v3 = v0_5.getKey();
            int v4_1 = (this.c & com.a.b.d.iq.a(v3.hashCode()));
            com.a.b.d.ka v5 = this.b[v4_1];
            if (v5 != null) {
                v0_5 = new com.a.b.d.zi(v0_5, v5);
            }
            this.b[v4_1] = v0_5;
            this.a[v2] = v0_5;
            com.a.b.d.zf.a(v3, v0_5, v5);
            v2++;
        }
        return;
    }

    varargs zf(com.a.b.d.kb[] p2)
    {
        this(p2.length, p2);
        return;
    }

    zf(java.util.Map$Entry[] p8)
    {
        int v2 = p8.length;
        int v0_0 = new com.a.b.d.ka[v2];
        this.a = v0_0;
        int v0_2 = com.a.b.d.iq.a(v2, 1.2);
        int v1_0 = new com.a.b.d.ka[v0_2];
        this.b = v1_0;
        this.c = (v0_2 - 1);
        int v1_1 = 0;
        while (v1_1 < v2) {
            int v0_10;
            int v0_5 = p8[v1_1];
            Object v3 = v0_5.getKey();
            com.a.b.d.ka[] v4_0 = v0_5.getValue();
            com.a.b.d.cl.a(v3, v4_0);
            int v5_1 = (this.c & com.a.b.d.iq.a(v3.hashCode()));
            com.a.b.d.ka v6 = this.b[v5_1];
            if (v6 != null) {
                v0_10 = new com.a.b.d.zi(v3, v4_0, v6);
            } else {
                v0_10 = new com.a.b.d.kb(v3, v4_0);
            }
            this.b[v5_1] = v0_10;
            this.a[v1_1] = v0_10;
            com.a.b.d.zf.a(v3, v0_10, v6);
            v1_1++;
        }
        return;
    }

    private static void a(Object p2, com.a.b.d.ka p3, com.a.b.d.ka p4)
    {
        while (p4 != null) {
            int v0_2;
            if (p2.equals(p4.getKey())) {
                v0_2 = 0;
            } else {
                v0_2 = 1;
            }
            com.a.b.d.zf.a(v0_2, "key", p3, p4);
            p4 = p4.a();
        }
        return;
    }

    private static com.a.b.d.ka[] a(int p1)
    {
        com.a.b.d.ka[] v0 = new com.a.b.d.ka[p1];
        return v0;
    }

    static synthetic com.a.b.d.ka[] a(com.a.b.d.zf p1)
    {
        return p1.a;
    }

    final com.a.b.d.lo d()
    {
        return new com.a.b.d.zh(this, 0);
    }

    public final Object get(Object p4)
    {
        Object v0 = 0;
        if (p4 != null) {
            com.a.b.d.ka v1_3 = this.b[(com.a.b.d.iq.a(p4.hashCode()) & this.c)];
            while (v1_3 != null) {
                if (!p4.equals(v1_3.getKey())) {
                    v1_3 = v1_3.a();
                } else {
                    v0 = v1_3.getValue();
                    break;
                }
            }
        }
        return v0;
    }

    final boolean i_()
    {
        return 0;
    }

    public final int size()
    {
        return this.a.length;
    }
}
