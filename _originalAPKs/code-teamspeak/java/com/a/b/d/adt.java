package com.a.b.d;
 class adt extends com.a.b.d.adq implements java.util.SortedSet {
    private static final long a;

    adt(java.util.SortedSet p1, Object p2)
    {
        this(p1, p2);
        return;
    }

    java.util.SortedSet a()
    {
        return ((java.util.SortedSet) super.c());
    }

    synthetic java.util.Collection b()
    {
        return this.a();
    }

    synthetic java.util.Set c()
    {
        return this.a();
    }

    public java.util.Comparator comparator()
    {
        try {
            return this.a().comparator();
        } catch (Throwable v0_2) {
            throw v0_2;
        }
    }

    synthetic Object d()
    {
        return this.a();
    }

    public Object first()
    {
        try {
            return this.a().first();
        } catch (Throwable v0_2) {
            throw v0_2;
        }
    }

    public java.util.SortedSet headSet(Object p4)
    {
        try {
            return com.a.b.d.acu.a(this.a().headSet(p4), this.h);
        } catch (Throwable v0_3) {
            throw v0_3;
        }
    }

    public Object last()
    {
        try {
            return this.a().last();
        } catch (Throwable v0_2) {
            throw v0_2;
        }
    }

    public java.util.SortedSet subSet(Object p4, Object p5)
    {
        try {
            return com.a.b.d.acu.a(this.a().subSet(p4, p5), this.h);
        } catch (Throwable v0_3) {
            throw v0_3;
        }
    }

    public java.util.SortedSet tailSet(Object p4)
    {
        try {
            return com.a.b.d.acu.a(this.a().tailSet(p4), this.h);
        } catch (Throwable v0_3) {
            throw v0_3;
        }
    }
}
