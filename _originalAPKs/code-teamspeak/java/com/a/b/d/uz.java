package com.a.b.d;
final class uz extends com.a.b.d.hk implements java.io.Serializable, java.util.NavigableMap {
    private final java.util.NavigableMap a;
    private transient com.a.b.d.uz b;

    uz(java.util.NavigableMap p1)
    {
        this.a = p1;
        return;
    }

    private uz(java.util.NavigableMap p1, com.a.b.d.uz p2)
    {
        this.a = p1;
        this.b = p2;
        return;
    }

    protected final synthetic java.util.Map a()
    {
        return java.util.Collections.unmodifiableSortedMap(this.a);
    }

    protected final java.util.SortedMap c()
    {
        return java.util.Collections.unmodifiableSortedMap(this.a);
    }

    public final java.util.Map$Entry ceilingEntry(Object p2)
    {
        return com.a.b.d.sz.d(this.a.ceilingEntry(p2));
    }

    public final Object ceilingKey(Object p2)
    {
        return this.a.ceilingKey(p2);
    }

    public final java.util.NavigableSet descendingKeySet()
    {
        return com.a.b.d.aad.a(this.a.descendingKeySet());
    }

    public final java.util.NavigableMap descendingMap()
    {
        com.a.b.d.uz v0_0 = this.b;
        if (v0_0 == null) {
            v0_0 = new com.a.b.d.uz(this.a.descendingMap(), this);
            this.b = v0_0;
        }
        return v0_0;
    }

    public final java.util.Map$Entry firstEntry()
    {
        return com.a.b.d.sz.d(this.a.firstEntry());
    }

    public final java.util.Map$Entry floorEntry(Object p2)
    {
        return com.a.b.d.sz.d(this.a.floorEntry(p2));
    }

    public final Object floorKey(Object p2)
    {
        return this.a.floorKey(p2);
    }

    public final java.util.NavigableMap headMap(Object p2, boolean p3)
    {
        return com.a.b.d.sz.a(this.a.headMap(p2, p3));
    }

    public final java.util.SortedMap headMap(Object p2)
    {
        return this.headMap(p2, 0);
    }

    public final java.util.Map$Entry higherEntry(Object p2)
    {
        return com.a.b.d.sz.d(this.a.higherEntry(p2));
    }

    public final Object higherKey(Object p2)
    {
        return this.a.higherKey(p2);
    }

    protected final synthetic Object k_()
    {
        return java.util.Collections.unmodifiableSortedMap(this.a);
    }

    public final java.util.Set keySet()
    {
        return this.navigableKeySet();
    }

    public final java.util.Map$Entry lastEntry()
    {
        return com.a.b.d.sz.d(this.a.lastEntry());
    }

    public final java.util.Map$Entry lowerEntry(Object p2)
    {
        return com.a.b.d.sz.d(this.a.lowerEntry(p2));
    }

    public final Object lowerKey(Object p2)
    {
        return this.a.lowerKey(p2);
    }

    public final java.util.NavigableSet navigableKeySet()
    {
        return com.a.b.d.aad.a(this.a.navigableKeySet());
    }

    public final java.util.Map$Entry pollFirstEntry()
    {
        throw new UnsupportedOperationException();
    }

    public final java.util.Map$Entry pollLastEntry()
    {
        throw new UnsupportedOperationException();
    }

    public final java.util.NavigableMap subMap(Object p2, boolean p3, Object p4, boolean p5)
    {
        return com.a.b.d.sz.a(this.a.subMap(p2, p3, p4, p5));
    }

    public final java.util.SortedMap subMap(Object p3, Object p4)
    {
        return this.subMap(p3, 1, p4, 0);
    }

    public final java.util.NavigableMap tailMap(Object p2, boolean p3)
    {
        return com.a.b.d.sz.a(this.a.tailMap(p2, p3));
    }

    public final java.util.SortedMap tailMap(Object p2)
    {
        return this.tailMap(p2, 1);
    }
}
