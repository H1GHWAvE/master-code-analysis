package com.a.b.d;
final class zs implements java.util.Comparator {
    final synthetic java.util.Comparator a;
    final synthetic java.util.Comparator b;

    zs(java.util.Comparator p1, java.util.Comparator p2)
    {
        this.a = p1;
        this.b = p2;
        return;
    }

    private int a(com.a.b.d.adw p5, com.a.b.d.adw p6)
    {
        int v0_2;
        if (this.a != null) {
            v0_2 = this.a.compare(p5.a(), p6.a());
        } else {
            v0_2 = 0;
        }
        if (v0_2 == 0) {
            if (this.b != null) {
                v0_2 = this.b.compare(p5.b(), p6.b());
            } else {
                v0_2 = 0;
            }
        }
        return v0_2;
    }

    public final synthetic int compare(Object p5, Object p6)
    {
        int v0_2;
        if (this.a != null) {
            v0_2 = this.a.compare(((com.a.b.d.adw) p5).a(), ((com.a.b.d.adw) p6).a());
        } else {
            v0_2 = 0;
        }
        if (v0_2 == 0) {
            if (this.b != null) {
                v0_2 = this.b.compare(((com.a.b.d.adw) p5).b(), ((com.a.b.d.adw) p6).b());
            } else {
                v0_2 = 0;
            }
        }
        return v0_2;
    }
}
