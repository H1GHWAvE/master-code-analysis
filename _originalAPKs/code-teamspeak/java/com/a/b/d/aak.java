package com.a.b.d;
final class aak extends com.a.b.d.aam implements java.util.NavigableSet {

    aak(java.util.NavigableSet p1, com.a.b.b.co p2)
    {
        this(p1, p2);
        return;
    }

    private java.util.NavigableSet a()
    {
        return ((java.util.NavigableSet) this.a);
    }

    public final Object ceiling(Object p2)
    {
        return com.a.b.d.mq.f(this.tailSet(p2, 1));
    }

    public final java.util.Iterator descendingIterator()
    {
        return com.a.b.d.nj.b(((java.util.NavigableSet) this.a).descendingIterator(), this.b);
    }

    public final java.util.NavigableSet descendingSet()
    {
        return com.a.b.d.aad.a(((java.util.NavigableSet) this.a).descendingSet(), this.b);
    }

    public final Object floor(Object p3)
    {
        return com.a.b.d.nj.d(this.headSet(p3, 1).descendingIterator(), 0);
    }

    public final java.util.NavigableSet headSet(Object p3, boolean p4)
    {
        return com.a.b.d.aad.a(((java.util.NavigableSet) this.a).headSet(p3, p4), this.b);
    }

    public final Object higher(Object p2)
    {
        return com.a.b.d.mq.f(this.tailSet(p2, 0));
    }

    public final Object last()
    {
        return this.descendingIterator().next();
    }

    public final Object lower(Object p3)
    {
        return com.a.b.d.nj.d(this.headSet(p3, 0).descendingIterator(), 0);
    }

    public final Object pollFirst()
    {
        return com.a.b.d.mq.b(((java.util.NavigableSet) this.a), this.b);
    }

    public final Object pollLast()
    {
        return com.a.b.d.mq.b(((java.util.NavigableSet) this.a).descendingSet(), this.b);
    }

    public final java.util.NavigableSet subSet(Object p3, boolean p4, Object p5, boolean p6)
    {
        return com.a.b.d.aad.a(((java.util.NavigableSet) this.a).subSet(p3, p4, p5, p6), this.b);
    }

    public final java.util.NavigableSet tailSet(Object p3, boolean p4)
    {
        return com.a.b.d.aad.a(((java.util.NavigableSet) this.a).tailSet(p3, p4), this.b);
    }
}
