package com.a.b.d;
final class c extends com.a.b.d.hi {
    final java.util.Set a;
    final synthetic com.a.b.d.a b;

    private c(com.a.b.d.a p2)
    {
        this.b = p2;
        this.a = com.a.b.d.a.a(this.b).entrySet();
        return;
    }

    synthetic c(com.a.b.d.a p1, byte p2)
    {
        this(p1);
        return;
    }

    protected final java.util.Set a()
    {
        return this.a;
    }

    protected final bridge synthetic java.util.Collection b()
    {
        return this.a;
    }

    public final void clear()
    {
        this.b.clear();
        return;
    }

    public final boolean contains(Object p2)
    {
        return com.a.b.d.sz.a(this.a, p2);
    }

    public final boolean containsAll(java.util.Collection p2)
    {
        return com.a.b.d.cm.a(this, p2);
    }

    public final java.util.Iterator iterator()
    {
        return new com.a.b.d.d(this, this.a.iterator());
    }

    protected final bridge synthetic Object k_()
    {
        return this.a;
    }

    public final boolean remove(Object p3)
    {
        int v0_6;
        if (this.a.contains(p3)) {
            com.a.b.d.a.a(this.b.a).remove(((java.util.Map$Entry) p3).getValue());
            this.a.remove(((java.util.Map$Entry) p3));
            v0_6 = 1;
        } else {
            v0_6 = 0;
        }
        return v0_6;
    }

    public final boolean removeAll(java.util.Collection p2)
    {
        return this.b(p2);
    }

    public final boolean retainAll(java.util.Collection p2)
    {
        return this.c(p2);
    }

    public final Object[] toArray()
    {
        return this.p();
    }

    public final Object[] toArray(Object[] p2)
    {
        return com.a.b.d.yc.a(this, p2);
    }
}
