package com.a.b.d;
final class jg extends com.a.b.d.agi {
    final synthetic com.a.b.d.jf a;
    private final java.util.Iterator b;

    jg(com.a.b.d.jf p2)
    {
        this.a = p2;
        this.b = com.a.b.d.jd.a(this.a.a).entrySet().iterator();
        return;
    }

    private java.util.Map$Entry a()
    {
        java.util.Map$Entry v0_2 = ((java.util.Map$Entry) this.b.next());
        return com.a.b.d.sz.a(v0_2.getKey(), v0_2.getValue());
    }

    public final boolean hasNext()
    {
        return this.b.hasNext();
    }

    public final synthetic Object next()
    {
        java.util.Map$Entry v0_2 = ((java.util.Map$Entry) this.b.next());
        return com.a.b.d.sz.a(v0_2.getKey(), v0_2.getValue());
    }
}
