package com.a.b.d;
final class ec extends com.a.b.d.zr {
    private final com.a.b.d.jt a;
    private final com.a.b.d.jt b;
    private final com.a.b.d.jt c;
    private final com.a.b.d.jt d;
    private final int[] e;
    private final int[] f;
    private final Object[][] g;
    private final int[] h;
    private final int[] i;

    ec(com.a.b.d.jl p13, com.a.b.d.lo p14, com.a.b.d.lo p15)
    {
        this.g = ((Object[][]) ((Object[][]) reflect.Array.newInstance(Object, new int[] {p14.size(), p15.size()}))));
        this.a = com.a.b.d.ec.a(p14);
        this.b = com.a.b.d.ec.a(p15);
        int v0_9 = new int[this.a.size()];
        this.e = v0_9;
        int v0_12 = new int[this.b.size()];
        this.f = v0_12;
        int[] v3 = new int[p13.size()];
        int[] v4 = new int[p13.size()];
        int v2 = 0;
        while (v2 < p13.size()) {
            int v1_13;
            int v0_22 = ((com.a.b.d.adw) p13.get(v2));
            int v5_0 = v0_22.a();
            Object v6 = v0_22.b();
            int v7 = ((Integer) this.a.get(v5_0)).intValue();
            int v8 = ((Integer) this.b.get(v6)).intValue();
            if (this.g[v7][v8] != null) {
                v1_13 = 0;
            } else {
                v1_13 = 1;
            }
            Object[] v10_1 = new Object[2];
            v10_1[0] = v5_0;
            v10_1[1] = v6;
            com.a.b.b.cn.a(v1_13, "duplicate key: (%s, %s)", v10_1);
            this.g[v7][v8] = v0_22.c();
            int v0_24 = this.e;
            v0_24[v7] = (v0_24[v7] + 1);
            int v0_25 = this.f;
            v0_25[v8] = (v0_25[v8] + 1);
            v3[v2] = v7;
            v4[v2] = v8;
            v2++;
        }
        this.h = v3;
        this.i = v4;
        this.c = new com.a.b.d.ek(this, 0);
        this.d = new com.a.b.d.ef(this, 0);
        return;
    }

    private static com.a.b.d.jt a(com.a.b.d.lo p5)
    {
        com.a.b.d.ju v1 = com.a.b.d.jt.l();
        com.a.b.d.jt v0_0 = 0;
        java.util.Iterator v2 = p5.iterator();
        while (v2.hasNext()) {
            v1.a(v2.next(), Integer.valueOf(v0_0));
            v0_0++;
        }
        return v1.a();
    }

    static synthetic int[] a(com.a.b.d.ec p1)
    {
        return p1.e;
    }

    static synthetic com.a.b.d.jt b(com.a.b.d.ec p1)
    {
        return p1.b;
    }

    static synthetic Object[][] c(com.a.b.d.ec p1)
    {
        return p1.g;
    }

    static synthetic int[] d(com.a.b.d.ec p1)
    {
        return p1.f;
    }

    static synthetic com.a.b.d.jt e(com.a.b.d.ec p1)
    {
        return p1.a;
    }

    final com.a.b.d.adw a(int p6)
    {
        com.a.b.d.adw v0_1 = this.h[p6];
        int v1_1 = this.i[p6];
        return com.a.b.d.ec.b(this.o().g().f().get(v0_1), this.n().g().f().get(v1_1), this.g[v0_1][v1_1]);
    }

    final Object b(int p3)
    {
        return this.g[this.h[p3]][this.i[p3]];
    }

    public final Object b(Object p4, Object p5)
    {
        Object v0_5;
        Object v0_2 = ((Integer) this.a.get(p4));
        int v1_2 = ((Integer) this.b.get(p5));
        if ((v0_2 != null) && (v1_2 != 0)) {
            v0_5 = this.g[v0_2.intValue()][v1_2.intValue()];
        } else {
            v0_5 = 0;
        }
        return v0_5;
    }

    public final int k()
    {
        return this.h.length;
    }

    public final bridge synthetic java.util.Map l()
    {
        return this.d;
    }

    public final bridge synthetic java.util.Map m()
    {
        return this.c;
    }

    public final com.a.b.d.jt n()
    {
        return this.d;
    }

    public final com.a.b.d.jt o()
    {
        return this.c;
    }
}
