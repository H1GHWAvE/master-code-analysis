package com.a.b.d;
public final class wu extends com.a.b.d.wv implements com.a.b.d.ou {

    public wu(com.a.b.d.ou p1, com.a.b.d.tv p2)
    {
        this(p1, p2);
        return;
    }

    private java.util.List b(Object p2, java.util.Collection p3)
    {
        return com.a.b.d.ov.a(((java.util.List) p3), com.a.b.d.sz.a(this.b, p2));
    }

    final synthetic java.util.Collection a(Object p2, java.util.Collection p3)
    {
        return this.b(p2, p3);
    }

    public final java.util.List a(Object p2)
    {
        return this.b(p2, this.a.c(p2));
    }

    public final java.util.List a(Object p2, Iterable p3)
    {
        throw new UnsupportedOperationException();
    }

    public final synthetic java.util.Collection b(Object p2, Iterable p3)
    {
        return this.a(p2, p3);
    }

    public final java.util.List b(Object p2)
    {
        return this.b(p2, this.a.d(p2));
    }

    public final synthetic java.util.Collection c(Object p2)
    {
        return this.a(p2);
    }

    public final synthetic java.util.Collection d(Object p2)
    {
        return this.b(p2);
    }
}
