package com.a.b.d;
final class dh extends com.a.b.d.hi {
    final synthetic java.util.Set a;
    final synthetic com.a.b.d.dg b;

    dh(com.a.b.d.dg p1, java.util.Set p2)
    {
        this.b = p1;
        this.a = p2;
        return;
    }

    protected final java.util.Set a()
    {
        return this.a;
    }

    protected final bridge synthetic java.util.Collection b()
    {
        return this.a;
    }

    public final boolean contains(Object p2)
    {
        if ((p2 == null) || (!com.a.b.d.cm.a(this.a, p2))) {
            int v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    public final boolean containsAll(java.util.Collection p2)
    {
        return com.a.b.d.cm.a(this, p2);
    }

    protected final bridge synthetic Object k_()
    {
        return this.a;
    }

    public final boolean remove(Object p2)
    {
        if ((p2 == null) || (!com.a.b.d.cm.b(this.a, p2))) {
            int v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    public final boolean removeAll(java.util.Collection p2)
    {
        return this.b(p2);
    }
}
