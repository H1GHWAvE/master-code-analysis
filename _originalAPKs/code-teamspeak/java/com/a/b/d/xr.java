package com.a.b.d;
abstract class xr extends com.a.b.d.aan {

    xr()
    {
        return;
    }

    abstract com.a.b.d.xc a();

    public void clear()
    {
        this.a().clear();
        return;
    }

    public boolean contains(Object p4)
    {
        int v0 = 0;
        if (((p4 instanceof com.a.b.d.xd)) && ((((com.a.b.d.xd) p4).b() > 0) && (this.a().a(((com.a.b.d.xd) p4).a()) == ((com.a.b.d.xd) p4).b()))) {
            v0 = 1;
        }
        return v0;
    }

    public boolean remove(Object p5)
    {
        boolean v0 = 0;
        if ((p5 instanceof com.a.b.d.xd)) {
            Object v1_1 = ((com.a.b.d.xd) p5).a();
            int v2 = ((com.a.b.d.xd) p5).b();
            if (v2 != 0) {
                v0 = this.a().a(v1_1, v2, 0);
            }
        }
        return v0;
    }
}
