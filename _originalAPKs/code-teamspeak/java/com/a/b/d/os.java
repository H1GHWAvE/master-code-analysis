package com.a.b.d;
final class os implements java.util.ListIterator {
    int a;
    com.a.b.d.or b;
    com.a.b.d.or c;
    com.a.b.d.or d;
    int e;
    final synthetic com.a.b.d.oj f;

    os(com.a.b.d.oj p3, int p4)
    {
        this.f = p3;
        this.e = com.a.b.d.oj.a(this.f);
        int v1 = p3.f();
        com.a.b.b.cn.b(p4, v1);
        if (p4 < (v1 / 2)) {
            this.b = com.a.b.d.oj.c(p3);
            while(true) {
                int v0_4 = (p4 - 1);
                if (p4 <= 0) {
                    break;
                }
                this.b();
                p4 = v0_4;
            }
        } else {
            this.d = com.a.b.d.oj.b(p3);
            this.a = v1;
            while(true) {
                int v0_6 = (p4 + 1);
                if (p4 >= v1) {
                    break;
                }
                this.c();
                p4 = v0_6;
            }
        }
        this.c = 0;
        return;
    }

    private void a()
    {
        if (com.a.b.d.oj.a(this.f) == this.e) {
            return;
        } else {
            throw new java.util.ConcurrentModificationException();
        }
    }

    private void a(Object p2)
    {
        com.a.b.d.or v0_1;
        if (this.c == null) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        com.a.b.b.cn.b(v0_1);
        this.c.b = p2;
        return;
    }

    private com.a.b.d.or b()
    {
        this.a();
        com.a.b.d.oj.e(this.b);
        com.a.b.d.or v0_1 = this.b;
        this.c = v0_1;
        this.d = v0_1;
        this.b = this.b.c;
        this.a = (this.a + 1);
        return this.c;
    }

    private com.a.b.d.or c()
    {
        this.a();
        com.a.b.d.oj.e(this.d);
        com.a.b.d.or v0_1 = this.d;
        this.c = v0_1;
        this.b = v0_1;
        this.d = this.d.d;
        this.a = (this.a - 1);
        return this.c;
    }

    private static void d()
    {
        throw new UnsupportedOperationException();
    }

    private static void e()
    {
        throw new UnsupportedOperationException();
    }

    public final synthetic void add(Object p2)
    {
        throw new UnsupportedOperationException();
    }

    public final boolean hasNext()
    {
        int v0_1;
        this.a();
        if (this.b == null) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    public final boolean hasPrevious()
    {
        int v0_1;
        this.a();
        if (this.d == null) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    public final synthetic Object next()
    {
        return this.b();
    }

    public final int nextIndex()
    {
        return this.a;
    }

    public final synthetic Object previous()
    {
        return this.c();
    }

    public final int previousIndex()
    {
        return (this.a - 1);
    }

    public final void remove()
    {
        int v0_1;
        this.a();
        if (this.c == null) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        com.a.b.b.cn.b(v0_1, "no calls to next() since the last call to remove()");
        if (this.c == this.b) {
            this.b = this.c.c;
        } else {
            this.d = this.c.d;
            this.a = (this.a - 1);
        }
        com.a.b.d.oj.a(this.f, this.c);
        this.c = 0;
        this.e = com.a.b.d.oj.a(this.f);
        return;
    }

    public final synthetic void set(Object p2)
    {
        throw new UnsupportedOperationException();
    }
}
