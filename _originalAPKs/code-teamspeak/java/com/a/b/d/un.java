package com.a.b.d;
 class un extends com.a.b.d.up implements java.util.NavigableSet {

    un(java.util.NavigableMap p1)
    {
        this(p1);
        return;
    }

    private java.util.NavigableMap c()
    {
        return ((java.util.NavigableMap) this.d);
    }

    final bridge synthetic java.util.SortedMap a()
    {
        return ((java.util.NavigableMap) this.d);
    }

    final bridge synthetic java.util.Map b()
    {
        return ((java.util.NavigableMap) this.d);
    }

    public Object ceiling(Object p2)
    {
        return ((java.util.NavigableMap) this.d).ceilingKey(p2);
    }

    public java.util.Iterator descendingIterator()
    {
        return this.descendingSet().iterator();
    }

    public java.util.NavigableSet descendingSet()
    {
        return ((java.util.NavigableMap) this.d).descendingKeySet();
    }

    public Object floor(Object p2)
    {
        return ((java.util.NavigableMap) this.d).floorKey(p2);
    }

    public java.util.NavigableSet headSet(Object p2, boolean p3)
    {
        return ((java.util.NavigableMap) this.d).headMap(p2, p3).navigableKeySet();
    }

    public java.util.SortedSet headSet(Object p2)
    {
        return this.headSet(p2, 0);
    }

    public Object higher(Object p2)
    {
        return ((java.util.NavigableMap) this.d).higherKey(p2);
    }

    public Object lower(Object p2)
    {
        return ((java.util.NavigableMap) this.d).lowerKey(p2);
    }

    public Object pollFirst()
    {
        return com.a.b.d.sz.b(((java.util.NavigableMap) this.d).pollFirstEntry());
    }

    public Object pollLast()
    {
        return com.a.b.d.sz.b(((java.util.NavigableMap) this.d).pollLastEntry());
    }

    public java.util.NavigableSet subSet(Object p2, boolean p3, Object p4, boolean p5)
    {
        return ((java.util.NavigableMap) this.d).subMap(p2, p3, p4, p5).navigableKeySet();
    }

    public java.util.SortedSet subSet(Object p3, Object p4)
    {
        return this.subSet(p3, 1, p4, 0);
    }

    public java.util.NavigableSet tailSet(Object p2, boolean p3)
    {
        return ((java.util.NavigableMap) this.d).tailMap(p2, p3).navigableKeySet();
    }

    public java.util.SortedSet tailSet(Object p2)
    {
        return this.tailSet(p2, 1);
    }
}
