package com.a.b.d;
 class wv extends com.a.b.d.an {
    final com.a.b.d.vi a;
    final com.a.b.d.tv b;

    wv(com.a.b.d.vi p2, com.a.b.d.tv p3)
    {
        this.a = ((com.a.b.d.vi) com.a.b.b.cn.a(p2));
        this.b = ((com.a.b.d.tv) com.a.b.b.cn.a(p3));
        return;
    }

    java.util.Collection a(Object p3, java.util.Collection p4)
    {
        java.util.Collection v0_2;
        java.util.Collection v0_1 = com.a.b.d.sz.a(this.b, p3);
        if (!(p4 instanceof java.util.List)) {
            v0_2 = com.a.b.d.cm.a(p4, v0_1);
        } else {
            v0_2 = com.a.b.d.ov.a(((java.util.List) p4), v0_1);
        }
        return v0_2;
    }

    public final boolean a(com.a.b.d.vi p2)
    {
        throw new UnsupportedOperationException();
    }

    public final boolean a(Object p2, Object p3)
    {
        throw new UnsupportedOperationException();
    }

    public java.util.Collection b(Object p2, Iterable p3)
    {
        throw new UnsupportedOperationException();
    }

    public java.util.Collection c(Object p2)
    {
        return this.a(p2, this.a.c(p2));
    }

    public final boolean c(Object p2, Iterable p3)
    {
        throw new UnsupportedOperationException();
    }

    public final boolean c(Object p2, Object p3)
    {
        return this.c(p2).remove(p3);
    }

    public java.util.Collection d(Object p2)
    {
        return this.a(p2, this.a.d(p2));
    }

    public final int f()
    {
        return this.a.f();
    }

    public final boolean f(Object p2)
    {
        return this.a.f(p2);
    }

    public final void g()
    {
        this.a.g();
        return;
    }

    final java.util.Iterator l()
    {
        return com.a.b.d.nj.a(this.a.k().iterator(), com.a.b.d.sz.b(this.b));
    }

    final java.util.Map m()
    {
        return com.a.b.d.sz.a(this.a.b(), new com.a.b.d.ww(this));
    }

    public final boolean n()
    {
        return this.a.n();
    }

    public final java.util.Set p()
    {
        return this.a.p();
    }

    public final com.a.b.d.xc q()
    {
        return this.a.q();
    }

    final java.util.Collection s()
    {
        return com.a.b.d.cm.a(this.a.k(), com.a.b.d.sz.a(this.b));
    }
}
