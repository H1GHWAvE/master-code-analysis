package com.a.b.d;
public final class we {

    private we()
    {
        return;
    }

    private static com.a.b.d.aac a(com.a.b.d.aac p1)
    {
        if ((!(p1 instanceof com.a.b.d.adr)) && (!(p1 instanceof com.a.b.d.lr))) {
            p1 = new com.a.b.d.adr(p1);
        }
        return p1;
    }

    public static com.a.b.d.aac a(com.a.b.d.aac p3, com.a.b.b.co p4)
    {
        com.a.b.d.fy v0_3;
        if (!(p3 instanceof com.a.b.d.fy)) {
            if (!(p3 instanceof com.a.b.d.gc)) {
                v0_3 = new com.a.b.d.fy(p3, p4);
            } else {
                v0_3 = com.a.b.d.we.a(((com.a.b.d.gc) p3), com.a.b.d.sz.a(p4));
            }
        } else {
            v0_3 = new com.a.b.d.fy(((com.a.b.d.aac) ((com.a.b.d.fy) p3).a), com.a.b.b.cp.a(((com.a.b.d.fy) p3).b, p4));
        }
        return v0_3;
    }

    private static com.a.b.d.aac a(com.a.b.d.gc p3, com.a.b.b.co p4)
    {
        return new com.a.b.d.fs(p3.d(), com.a.b.b.cp.a(p3.c(), p4));
    }

    private static com.a.b.d.aac a(com.a.b.d.lr p1)
    {
        return ((com.a.b.d.aac) com.a.b.b.cn.a(p1));
    }

    private static com.a.b.d.aac a(java.util.Map p1)
    {
        return new com.a.b.d.wr(p1);
    }

    private static com.a.b.d.abs a(com.a.b.d.abs p1)
    {
        if (!(p1 instanceof com.a.b.d.adu)) {
            p1 = new com.a.b.d.adu(p1);
        }
        return p1;
    }

    private static com.a.b.d.jr a(Iterable p4, com.a.b.b.bj p5)
    {
        com.a.b.d.jr v0_0 = p4.iterator();
        com.a.b.b.cn.a(p5);
        com.a.b.d.js v1 = com.a.b.d.jr.c();
        while (v0_0.hasNext()) {
            Object v2_1 = v0_0.next();
            com.a.b.b.cn.a(v2_1, v0_0);
            v1.a(p5.e(v2_1), v2_1);
        }
        return v1.a();
    }

    private static com.a.b.d.jr a(java.util.Iterator p3, com.a.b.b.bj p4)
    {
        com.a.b.b.cn.a(p4);
        com.a.b.d.jr v0_0 = com.a.b.d.jr.c();
        while (p3.hasNext()) {
            Object v1_1 = p3.next();
            com.a.b.b.cn.a(v1_1, p3);
            v0_0.a(p4.e(v1_1), v1_1);
        }
        return v0_0.a();
    }

    private static com.a.b.d.ou a(com.a.b.d.jr p1)
    {
        return ((com.a.b.d.ou) com.a.b.b.cn.a(p1));
    }

    private static com.a.b.d.ou a(com.a.b.d.ou p1)
    {
        if ((!(p1 instanceof com.a.b.d.adh)) && (!(p1 instanceof com.a.b.d.jr))) {
            p1 = new com.a.b.d.adh(p1);
        }
        return p1;
    }

    private static com.a.b.d.ou a(com.a.b.d.ou p2, com.a.b.b.bj p3)
    {
        com.a.b.b.cn.a(p3);
        return new com.a.b.d.wu(p2, com.a.b.d.sz.a(p3));
    }

    private static com.a.b.d.ou a(com.a.b.d.ou p3, com.a.b.b.co p4)
    {
        com.a.b.d.ft v0_2;
        if (!(p3 instanceof com.a.b.d.ft)) {
            v0_2 = new com.a.b.d.ft(p3, p4);
        } else {
            v0_2 = new com.a.b.d.ft(((com.a.b.d.ft) p3).d(), com.a.b.b.cp.a(((com.a.b.d.ft) p3).b, p4));
        }
        return v0_2;
    }

    private static com.a.b.d.ou a(com.a.b.d.ou p1, com.a.b.d.tv p2)
    {
        return new com.a.b.d.wu(p1, p2);
    }

    public static com.a.b.d.ou a(java.util.Map p1, com.a.b.b.dz p2)
    {
        return new com.a.b.d.wi(p1, p2);
    }

    private static com.a.b.d.vi a(com.a.b.d.ga p3, com.a.b.b.co p4)
    {
        return new com.a.b.d.fi(p3.a(), com.a.b.b.cp.a(p3.c(), p4));
    }

    private static com.a.b.d.vi a(com.a.b.d.kk p1)
    {
        return ((com.a.b.d.vi) com.a.b.b.cn.a(p1));
    }

    private static com.a.b.d.vi a(com.a.b.d.vi p1)
    {
        if ((!(p1 instanceof com.a.b.d.adj)) && (!(p1 instanceof com.a.b.d.kk))) {
            p1 = new com.a.b.d.adj(p1);
        }
        return p1;
    }

    private static com.a.b.d.vi a(com.a.b.d.vi p2, com.a.b.b.bj p3)
    {
        com.a.b.b.cn.a(p3);
        return new com.a.b.d.wv(p2, com.a.b.d.sz.a(p3));
    }

    private static com.a.b.d.vi a(com.a.b.d.vi p3, com.a.b.b.co p4)
    {
        com.a.b.d.fu v0_5;
        if (!(p3 instanceof com.a.b.d.aac)) {
            if (!(p3 instanceof com.a.b.d.ou)) {
                if (!(p3 instanceof com.a.b.d.fu)) {
                    if (!(p3 instanceof com.a.b.d.ga)) {
                        v0_5 = new com.a.b.d.fu(p3, p4);
                    } else {
                        v0_5 = com.a.b.d.we.a(((com.a.b.d.ga) p3), com.a.b.d.sz.a(p4));
                    }
                } else {
                    v0_5 = new com.a.b.d.fu(((com.a.b.d.fu) p3).a, com.a.b.b.cp.a(((com.a.b.d.fu) p3).b, p4));
                }
            } else {
                if (!(((com.a.b.d.ou) p3) instanceof com.a.b.d.ft)) {
                    v0_5 = new com.a.b.d.ft(((com.a.b.d.ou) p3), p4);
                } else {
                    v0_5 = new com.a.b.d.ft(((com.a.b.d.ft) ((com.a.b.d.ou) p3)).d(), com.a.b.b.cp.a(((com.a.b.d.ft) ((com.a.b.d.ou) p3)).b, p4));
                }
            }
        } else {
            v0_5 = com.a.b.d.we.a(((com.a.b.d.aac) p3), p4);
        }
        return v0_5;
    }

    private static com.a.b.d.vi a(com.a.b.d.vi p1, com.a.b.d.tv p2)
    {
        return new com.a.b.d.wv(p1, p2);
    }

    private static com.a.b.d.vi a(com.a.b.d.vi p3, com.a.b.d.vi p4)
    {
        com.a.b.b.cn.a(p4);
        java.util.Iterator v1 = p3.k().iterator();
        while (v1.hasNext()) {
            Object v0_3 = ((java.util.Map$Entry) v1.next());
            p4.a(v0_3.getValue(), v0_3.getKey());
        }
        return p4;
    }

    static synthetic java.util.Collection a(java.util.Collection p1)
    {
        java.util.Collection v0_3;
        if (!(p1 instanceof java.util.SortedSet)) {
            if (!(p1 instanceof java.util.Set)) {
                if (!(p1 instanceof java.util.List)) {
                    v0_3 = java.util.Collections.unmodifiableCollection(p1);
                } else {
                    v0_3 = java.util.Collections.unmodifiableList(((java.util.List) p1));
                }
            } else {
                v0_3 = java.util.Collections.unmodifiableSet(((java.util.Set) p1));
            }
        } else {
            v0_3 = java.util.Collections.unmodifiableSortedSet(((java.util.SortedSet) p1));
        }
        return v0_3;
    }

    private static boolean a(com.a.b.d.vi p2, Object p3)
    {
        int v0_1;
        if (p3 != p2) {
            if (!(p3 instanceof com.a.b.d.vi)) {
                v0_1 = 0;
            } else {
                v0_1 = p2.b().equals(((com.a.b.d.vi) p3).b());
            }
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    private static com.a.b.d.aac b(com.a.b.d.aac p1)
    {
        if ((!(p1 instanceof com.a.b.d.xa)) && (!(p1 instanceof com.a.b.d.lr))) {
            p1 = new com.a.b.d.xa(p1);
        }
        return p1;
    }

    private static com.a.b.d.aac b(com.a.b.d.aac p1, com.a.b.b.co p2)
    {
        return com.a.b.d.we.c(p1, com.a.b.d.sz.b(p2));
    }

    public static com.a.b.d.aac b(java.util.Map p1, com.a.b.b.dz p2)
    {
        return new com.a.b.d.wk(p1, p2);
    }

    private static com.a.b.d.abs b(com.a.b.d.abs p1)
    {
        if (!(p1 instanceof com.a.b.d.xb)) {
            p1 = new com.a.b.d.xb(p1);
        }
        return p1;
    }

    private static com.a.b.d.ou b(com.a.b.d.ou p1)
    {
        if ((!(p1 instanceof com.a.b.d.wx)) && (!(p1 instanceof com.a.b.d.jr))) {
            p1 = new com.a.b.d.wx(p1);
        }
        return p1;
    }

    private static com.a.b.d.vi b(com.a.b.d.vi p1)
    {
        if ((!(p1 instanceof com.a.b.d.wy)) && (!(p1 instanceof com.a.b.d.kk))) {
            p1 = new com.a.b.d.wy(p1);
        }
        return p1;
    }

    private static com.a.b.d.vi b(com.a.b.d.vi p3, com.a.b.b.co p4)
    {
        com.a.b.d.fi v0_4;
        com.a.b.b.co v2 = com.a.b.d.sz.b(p4);
        com.a.b.b.cn.a(v2);
        if (!(p3 instanceof com.a.b.d.aac)) {
            if (!(p3 instanceof com.a.b.d.ga)) {
                v0_4 = new com.a.b.d.fi(((com.a.b.d.vi) com.a.b.b.cn.a(p3)), v2);
            } else {
                v0_4 = com.a.b.d.we.a(((com.a.b.d.ga) p3), v2);
            }
        } else {
            v0_4 = com.a.b.d.we.c(((com.a.b.d.aac) p3), v2);
        }
        return v0_4;
    }

    private static java.util.Collection b(java.util.Collection p1)
    {
        java.util.Collection v0_3;
        if (!(p1 instanceof java.util.SortedSet)) {
            if (!(p1 instanceof java.util.Set)) {
                if (!(p1 instanceof java.util.List)) {
                    v0_3 = java.util.Collections.unmodifiableCollection(p1);
                } else {
                    v0_3 = java.util.Collections.unmodifiableList(((java.util.List) p1));
                }
            } else {
                v0_3 = java.util.Collections.unmodifiableSet(((java.util.Set) p1));
            }
        } else {
            v0_3 = java.util.Collections.unmodifiableSortedSet(((java.util.SortedSet) p1));
        }
        return v0_3;
    }

    private static com.a.b.d.aac c(com.a.b.d.aac p2, com.a.b.b.co p3)
    {
        com.a.b.d.fs v0_3;
        com.a.b.b.cn.a(p3);
        if (!(p2 instanceof com.a.b.d.gc)) {
            v0_3 = new com.a.b.d.fs(((com.a.b.d.aac) com.a.b.b.cn.a(p2)), p3);
        } else {
            v0_3 = com.a.b.d.we.a(((com.a.b.d.gc) p2), p3);
        }
        return v0_3;
    }

    private static com.a.b.d.vi c(com.a.b.d.vi p2, com.a.b.b.co p3)
    {
        com.a.b.d.fi v0_4;
        com.a.b.b.cn.a(p3);
        if (!(p2 instanceof com.a.b.d.aac)) {
            if (!(p2 instanceof com.a.b.d.ga)) {
                v0_4 = new com.a.b.d.fi(((com.a.b.d.vi) com.a.b.b.cn.a(p2)), p3);
            } else {
                v0_4 = com.a.b.d.we.a(((com.a.b.d.ga) p2), p3);
            }
        } else {
            v0_4 = com.a.b.d.we.c(((com.a.b.d.aac) p2), p3);
        }
        return v0_4;
    }

    private static com.a.b.d.vi c(java.util.Map p1, com.a.b.b.dz p2)
    {
        return new com.a.b.d.wj(p1, p2);
    }

    private static java.util.Collection c(java.util.Collection p2)
    {
        com.a.b.d.uw v0_2;
        if (!(p2 instanceof java.util.Set)) {
            v0_2 = new com.a.b.d.uw(java.util.Collections.unmodifiableCollection(p2));
        } else {
            v0_2 = com.a.b.d.sz.a(((java.util.Set) p2));
        }
        return v0_2;
    }

    private static java.util.Map c(com.a.b.d.aac p1)
    {
        return p1.b();
    }

    private static java.util.Map c(com.a.b.d.abs p1)
    {
        return p1.b();
    }

    private static java.util.Map c(com.a.b.d.ou p1)
    {
        return p1.b();
    }

    private static java.util.Map c(com.a.b.d.vi p1)
    {
        return p1.b();
    }

    private static com.a.b.d.abs d(java.util.Map p1, com.a.b.b.dz p2)
    {
        return new com.a.b.d.wl(p1, p2);
    }

    private static synthetic java.util.Collection d(java.util.Collection p2)
    {
        com.a.b.d.uw v0_2;
        if (!(p2 instanceof java.util.Set)) {
            v0_2 = new com.a.b.d.uw(java.util.Collections.unmodifiableCollection(p2));
        } else {
            v0_2 = com.a.b.d.sz.a(((java.util.Set) p2));
        }
        return v0_2;
    }
}
