package com.a.b.d;
 class aa extends com.a.b.d.u implements java.util.SortedSet {
    final synthetic com.a.b.d.n c;

    aa(com.a.b.d.n p1, java.util.SortedMap p2)
    {
        this.c = p1;
        this(p1, p2);
        return;
    }

    java.util.SortedMap a()
    {
        return ((java.util.SortedMap) super.b());
    }

    public java.util.Comparator comparator()
    {
        return this.a().comparator();
    }

    public Object first()
    {
        return this.a().firstKey();
    }

    public java.util.SortedSet headSet(Object p4)
    {
        return new com.a.b.d.aa(this.c, this.a().headMap(p4));
    }

    public Object last()
    {
        return this.a().lastKey();
    }

    public java.util.SortedSet subSet(Object p4, Object p5)
    {
        return new com.a.b.d.aa(this.c, this.a().subMap(p4, p5));
    }

    public java.util.SortedSet tailSet(Object p4)
    {
        return new com.a.b.d.aa(this.c, this.a().tailMap(p4));
    }
}
