package com.a.b.d;
final class ys extends com.a.b.d.du {
    private static final long g;
    private final com.a.b.d.yl f;

    ys(com.a.b.d.yl p1, com.a.b.d.ep p2)
    {
        this(p2);
        this.f = p1;
        return;
    }

    private com.a.b.d.du a(com.a.b.d.yl p3)
    {
        com.a.b.d.et v0_3;
        if (!this.f.b(p3)) {
            v0_3 = new com.a.b.d.et(this.a);
        } else {
            v0_3 = com.a.b.d.du.a(this.f.c(p3), this.a);
        }
        return v0_3;
    }

    static synthetic boolean a(Comparable p1, Comparable p2)
    {
        if ((p2 == null) || (com.a.b.d.yl.b(p1, p2) != 0)) {
            int v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    private static boolean b(Comparable p1, Comparable p2)
    {
        if ((p2 == null) || (com.a.b.d.yl.b(p1, p2) != 0)) {
            int v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    public final com.a.b.d.du a(com.a.b.d.du p5)
    {
        com.a.b.b.cn.a(p5);
        com.a.b.b.cn.a(this.a.equals(p5.a));
        if (!p5.isEmpty()) {
            com.a.b.d.ep v0_5 = ((Comparable) com.a.b.d.yd.d().b(this.k(), p5.first()));
            com.a.b.d.ep v1_4 = ((Comparable) com.a.b.d.yd.d().a(this.m(), p5.last()));
            if (v0_5.compareTo(v1_4) >= 0) {
                p5 = new com.a.b.d.et(this.a);
            } else {
                p5 = com.a.b.d.du.a(com.a.b.d.yl.a(v0_5, v1_4), this.a);
            }
        }
        return p5;
    }

    final com.a.b.d.du a(Comparable p2, boolean p3)
    {
        return this.a(com.a.b.d.yl.a(p2, com.a.b.d.ce.a(p3)));
    }

    final com.a.b.d.du a(Comparable p3, boolean p4, Comparable p5, boolean p6)
    {
        if ((p3.compareTo(p5) != 0) || ((p4) || (p6))) {
            com.a.b.d.et v0_3 = this.a(com.a.b.d.yl.a(p3, com.a.b.d.ce.a(p4), p5, com.a.b.d.ce.a(p6)));
        } else {
            v0_3 = new com.a.b.d.et(this.a);
        }
        return v0_3;
    }

    final synthetic com.a.b.d.me a(Object p2, boolean p3)
    {
        return this.b(((Comparable) p2), p3);
    }

    final bridge synthetic com.a.b.d.me a(Object p2, boolean p3, Object p4, boolean p5)
    {
        return this.a(((Comparable) p2), p3, ((Comparable) p4), p5);
    }

    public final com.a.b.d.yl a(com.a.b.d.ce p4, com.a.b.d.ce p5)
    {
        return com.a.b.d.yl.a(this.f.b.a(p4, this.a), this.f.c.b(p5, this.a));
    }

    final com.a.b.d.du b(Comparable p2, boolean p3)
    {
        return this.a(com.a.b.d.yl.b(p2, com.a.b.d.ce.a(p3)));
    }

    final synthetic com.a.b.d.me b(Object p2, boolean p3)
    {
        return this.a(((Comparable) p2), p3);
    }

    final int c(Object p3)
    {
        int v0_1;
        if (!this.contains(p3)) {
            v0_1 = -1;
        } else {
            v0_1 = ((int) this.a.a(this.k(), ((Comparable) p3)));
        }
        return v0_1;
    }

    public final com.a.b.d.agi c()
    {
        return new com.a.b.d.yt(this, this.k());
    }

    public final boolean contains(Object p3)
    {
        boolean v0 = 0;
        if (p3 != null) {
            try {
                v0 = this.f.c(((Comparable) p3));
            } catch (ClassCastException v1) {
            }
        }
        return v0;
    }

    public final boolean containsAll(java.util.Collection p2)
    {
        return com.a.b.d.cm.a(this, p2);
    }

    public final com.a.b.d.agi d()
    {
        return new com.a.b.d.yu(this, this.m());
    }

    public final synthetic java.util.Iterator descendingIterator()
    {
        return this.d();
    }

    public final boolean equals(Object p5)
    {
        int v0_3;
        if (p5 != this) {
            if ((!(p5 instanceof com.a.b.d.ys)) || (!this.a.equals(((com.a.b.d.ys) p5).a))) {
                v0_3 = super.equals(p5);
            } else {
                if ((!this.k().equals(((com.a.b.d.ys) p5).k())) || (!this.m().equals(((com.a.b.d.ys) p5).m()))) {
                    v0_3 = 0;
                } else {
                    v0_3 = 1;
                }
            }
        } else {
            v0_3 = 1;
        }
        return v0_3;
    }

    public final com.a.b.d.yl f_()
    {
        return com.a.b.d.yl.a(this.f.b.a(com.a.b.d.ce.b, this.a), this.f.c.b(com.a.b.d.ce.b, this.a));
    }

    public final synthetic Object first()
    {
        return this.k();
    }

    final Object g()
    {
        return new com.a.b.d.yv(this.f, this.a, 0);
    }

    final boolean h_()
    {
        return 0;
    }

    public final int hashCode()
    {
        return com.a.b.d.aad.a(this);
    }

    public final boolean isEmpty()
    {
        return 0;
    }

    public final synthetic java.util.Iterator iterator()
    {
        return this.c();
    }

    public final Comparable k()
    {
        return this.f.b.a(this.a);
    }

    public final synthetic Object last()
    {
        return this.m();
    }

    public final Comparable m()
    {
        return this.f.c.b(this.a);
    }

    public final int size()
    {
        int v0_3;
        int v0_1 = this.a.a(this.k(), this.m());
        if (v0_1 < 2147483647) {
            v0_3 = (((int) v0_1) + 1);
        } else {
            v0_3 = 2147483647;
        }
        return v0_3;
    }
}
