package com.a.b.d;
final class pi extends java.util.AbstractList implements java.io.Serializable, java.util.RandomAccess {
    private static final long c;
    final java.util.List a;
    final com.a.b.b.bj b;

    pi(java.util.List p2, com.a.b.b.bj p3)
    {
        this.a = ((java.util.List) com.a.b.b.cn.a(p2));
        this.b = ((com.a.b.b.bj) com.a.b.b.cn.a(p3));
        return;
    }

    public final void clear()
    {
        this.a.clear();
        return;
    }

    public final Object get(int p3)
    {
        return this.b.e(this.a.get(p3));
    }

    public final boolean isEmpty()
    {
        return this.a.isEmpty();
    }

    public final java.util.Iterator iterator()
    {
        return this.listIterator();
    }

    public final java.util.ListIterator listIterator(int p3)
    {
        return new com.a.b.d.pj(this, this.a.listIterator(p3));
    }

    public final Object remove(int p3)
    {
        return this.b.e(this.a.remove(p3));
    }

    public final int size()
    {
        return this.a.size();
    }
}
