package com.a.b.d;
public class lb implements com.a.b.d.yq {
    static final com.a.b.d.lb a;
    private final com.a.b.d.jl b;
    private final com.a.b.d.jl c;

    static lb()
    {
        com.a.b.d.lb.a = new com.a.b.d.lb(com.a.b.d.jl.d(), com.a.b.d.jl.d());
        return;
    }

    lb(com.a.b.d.jl p1, com.a.b.d.jl p2)
    {
        this.b = p1;
        this.c = p2;
        return;
    }

    static synthetic com.a.b.d.jl a(com.a.b.d.lb p1)
    {
        return p1.b;
    }

    private static com.a.b.d.lb b(com.a.b.d.yl p3, Object p4)
    {
        return new com.a.b.d.lb(com.a.b.d.jl.a(p3), com.a.b.d.jl.a(p4));
    }

    private static com.a.b.d.lb b(com.a.b.d.yq p5)
    {
        com.a.b.d.lb v5_2;
        if (!(p5 instanceof com.a.b.d.lb)) {
            com.a.b.d.jl v0_1 = p5.d();
            com.a.b.d.jl v1_1 = new com.a.b.d.jn(v0_1.size());
            com.a.b.d.jn v2_2 = new com.a.b.d.jn(v0_1.size());
            java.util.Iterator v3_1 = v0_1.entrySet().iterator();
            while (v3_1.hasNext()) {
                com.a.b.d.jl v0_6 = ((java.util.Map$Entry) v3_1.next());
                v1_1.c(v0_6.getKey());
                v2_2.c(v0_6.getValue());
            }
            v5_2 = new com.a.b.d.lb(v1_1.b(), v2_2.b());
        } else {
            v5_2 = ((com.a.b.d.lb) p5);
        }
        return v5_2;
    }

    private static com.a.b.d.lb e()
    {
        return com.a.b.d.lb.a;
    }

    private static com.a.b.d.le f()
    {
        return new com.a.b.d.le();
    }

    public final com.a.b.d.yl a()
    {
        if (!this.b.isEmpty()) {
            return com.a.b.d.yl.a(((com.a.b.d.yl) this.b.get(0)).b, ((com.a.b.d.yl) this.b.get((this.b.size() - 1))).c);
        } else {
            throw new java.util.NoSuchElementException();
        }
    }

    public final Object a(Comparable p7)
    {
        int v0_6;
        int v2_1 = com.a.b.d.aba.a(this.b, com.a.b.d.yl.a(), com.a.b.d.dw.b(p7), com.a.b.d.abg.a, com.a.b.d.abc.a);
        if (v2_1 != -1) {
            if (!((com.a.b.d.yl) this.b.get(v2_1)).c(p7)) {
                v0_6 = 0;
            } else {
                v0_6 = this.c.get(v2_1);
            }
        } else {
            v0_6 = 0;
        }
        return v0_6;
    }

    public final void a(com.a.b.d.yl p2)
    {
        throw new UnsupportedOperationException();
    }

    public final void a(com.a.b.d.yl p2, Object p3)
    {
        throw new UnsupportedOperationException();
    }

    public final void a(com.a.b.d.yq p2)
    {
        throw new UnsupportedOperationException();
    }

    public com.a.b.d.lb b(com.a.b.d.yl p7)
    {
        com.a.b.d.ld v0_11;
        if (!((com.a.b.d.yl) com.a.b.b.cn.a(p7)).f()) {
            if ((!this.b.isEmpty()) && (!p7.a(this.a()))) {
                com.a.b.d.lb v1_1 = com.a.b.d.aba.a(this.b, com.a.b.d.yl.b(), p7.b, com.a.b.d.abg.d, com.a.b.d.abc.b);
                com.a.b.d.jl v3_2 = com.a.b.d.aba.a(this.b, com.a.b.d.yl.a(), p7.c, com.a.b.d.abg.a, com.a.b.d.abc.b);
                if (v1_1 < v3_2) {
                    v0_11 = new com.a.b.d.ld(this, new com.a.b.d.lc(this, (v3_2 - v1_1), v1_1, p7), this.c.a(v1_1, v3_2), p7, this);
                } else {
                    v0_11 = com.a.b.d.lb.a;
                }
            } else {
                v0_11 = this;
            }
        } else {
            v0_11 = com.a.b.d.lb.a;
        }
        return v0_11;
    }

    public final java.util.Map$Entry b(Comparable p7)
    {
        Object v0_5;
        int v2_1 = com.a.b.d.aba.a(this.b, com.a.b.d.yl.a(), com.a.b.d.dw.b(p7), com.a.b.d.abg.a, com.a.b.d.abc.a);
        if (v2_1 != -1) {
            Object v0_4 = ((com.a.b.d.yl) this.b.get(v2_1));
            if (!v0_4.c(p7)) {
                v0_5 = 0;
            } else {
                v0_5 = com.a.b.d.sz.a(v0_4, this.c.get(v2_1));
            }
        } else {
            v0_5 = 0;
        }
        return v0_5;
    }

    public final void b()
    {
        throw new UnsupportedOperationException();
    }

    public final com.a.b.d.jt c()
    {
        com.a.b.d.zl v0_4;
        if (!this.b.isEmpty()) {
            v0_4 = new com.a.b.d.zl(new com.a.b.d.zq(this.b, com.a.b.d.yl.a), this.c);
        } else {
            v0_4 = com.a.b.d.jt.k();
        }
        return v0_4;
    }

    public synthetic com.a.b.d.yq c(com.a.b.d.yl p2)
    {
        return this.b(p2);
    }

    public synthetic java.util.Map d()
    {
        return this.c();
    }

    public boolean equals(Object p3)
    {
        int v0_1;
        if (!(p3 instanceof com.a.b.d.yq)) {
            v0_1 = 0;
        } else {
            v0_1 = this.c().equals(((com.a.b.d.yq) p3).d());
        }
        return v0_1;
    }

    public int hashCode()
    {
        return this.c().hashCode();
    }

    public String toString()
    {
        return this.c().toString();
    }
}
