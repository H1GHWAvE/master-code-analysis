package com.a.b.d;
public final class aad {

    private aad()
    {
        return;
    }

    static int a(java.util.Set p4)
    {
        java.util.Iterator v3 = p4.iterator();
        int v0_0 = 0;
        while (v3.hasNext()) {
            int v2_2;
            int v2_1 = v3.next();
            if (v2_1 == 0) {
                v2_2 = 0;
            } else {
                v2_2 = v2_1.hashCode();
            }
            v0_0 = (((v0_0 + v2_2) ^ -1) ^ -1);
        }
        return v0_0;
    }

    public static com.a.b.d.aaq a(java.util.Set p2, java.util.Set p3)
    {
        com.a.b.b.cn.a(p2, "set1");
        com.a.b.b.cn.a(p3, "set2");
        return new com.a.b.d.aae(p2, com.a.b.d.aad.c(p3, p2), p3);
    }

    private static varargs com.a.b.d.lo a(Enum p1, Enum[] p2)
    {
        return com.a.b.d.ji.a(java.util.EnumSet.of(p1, p2));
    }

    private static com.a.b.d.lo a(Iterable p2)
    {
        com.a.b.d.lo v2_1;
        if (!(p2 instanceof com.a.b.d.ji)) {
            if (!(p2 instanceof java.util.Collection)) {
                java.util.Iterator v1 = p2.iterator();
                if (!v1.hasNext()) {
                    v2_1 = com.a.b.d.lo.h();
                } else {
                    java.util.EnumSet v0_5 = java.util.EnumSet.of(((Enum) v1.next()));
                    com.a.b.d.nj.a(v0_5, v1);
                    v2_1 = com.a.b.d.ji.a(v0_5);
                }
            } else {
                if (!((java.util.Collection) p2).isEmpty()) {
                    v2_1 = com.a.b.d.ji.a(java.util.EnumSet.copyOf(((java.util.Collection) p2)));
                } else {
                    v2_1 = com.a.b.d.lo.h();
                }
            }
        } else {
            v2_1 = ((com.a.b.d.ji) p2);
        }
        return v2_1;
    }

    private static java.util.EnumSet a(Iterable p1, Class p2)
    {
        java.util.EnumSet v0 = java.util.EnumSet.noneOf(p2);
        com.a.b.d.mq.a(v0, p1);
        return v0;
    }

    private static java.util.EnumSet a(java.util.Collection p2)
    {
        java.util.EnumSet v0_7;
        if (!(p2 instanceof java.util.EnumSet)) {
            java.util.EnumSet v0_2;
            if (p2.isEmpty()) {
                v0_2 = 0;
            } else {
                v0_2 = 1;
            }
            com.a.b.b.cn.a(v0_2, "collection is empty; use the other version of this method");
            v0_7 = com.a.b.d.aad.b(p2, ((Enum) p2.iterator().next()).getDeclaringClass());
        } else {
            v0_7 = java.util.EnumSet.complementOf(((java.util.EnumSet) p2));
        }
        return v0_7;
    }

    private static java.util.EnumSet a(java.util.Collection p1, Class p2)
    {
        java.util.EnumSet v0_1;
        com.a.b.b.cn.a(p1);
        if (!(p1 instanceof java.util.EnumSet)) {
            v0_1 = com.a.b.d.aad.b(p1, p2);
        } else {
            v0_1 = java.util.EnumSet.complementOf(((java.util.EnumSet) p1));
        }
        return v0_1;
    }

    private static java.util.HashSet a()
    {
        return new java.util.HashSet();
    }

    public static java.util.HashSet a(int p2)
    {
        return new java.util.HashSet(com.a.b.d.sz.b(p2));
    }

    private static java.util.HashSet a(java.util.Iterator p1)
    {
        java.util.HashSet v0_1 = new java.util.HashSet();
        com.a.b.d.nj.a(v0_1, p1);
        return v0_1;
    }

    private static varargs java.util.HashSet a(Object[] p1)
    {
        java.util.HashSet v0_1 = com.a.b.d.aad.a(p1.length);
        java.util.Collections.addAll(v0_1, p1);
        return v0_1;
    }

    public static java.util.NavigableSet a(java.util.NavigableSet p1)
    {
        if ((!(p1 instanceof com.a.b.d.me)) && (!(p1 instanceof com.a.b.d.aat))) {
            p1 = new com.a.b.d.aat(p1);
        }
        return p1;
    }

    public static java.util.NavigableSet a(java.util.NavigableSet p3, com.a.b.b.co p4)
    {
        com.a.b.d.aak v0_3;
        if (!(p3 instanceof com.a.b.d.aal)) {
            v0_3 = new com.a.b.d.aak(((java.util.NavigableSet) com.a.b.b.cn.a(p3)), ((com.a.b.b.co) com.a.b.b.cn.a(p4)));
        } else {
            v0_3 = new com.a.b.d.aak(((java.util.NavigableSet) ((com.a.b.d.aal) p3).a), com.a.b.b.cp.a(((com.a.b.d.aal) p3).b, p4));
        }
        return v0_3;
    }

    private static java.util.Set a(java.util.List p1)
    {
        return com.a.b.d.aah.a(p1);
    }

    private static java.util.Set a(java.util.Map p1)
    {
        return java.util.Collections.newSetFromMap(p1);
    }

    public static java.util.Set a(java.util.Set p3, com.a.b.b.co p4)
    {
        com.a.b.d.aam v0_4;
        if (!(p3 instanceof java.util.SortedSet)) {
            if (!(p3 instanceof com.a.b.d.aal)) {
                v0_4 = new com.a.b.d.aal(((java.util.Set) com.a.b.b.cn.a(p3)), ((com.a.b.b.co) com.a.b.b.cn.a(p4)));
            } else {
                v0_4 = new com.a.b.d.aal(((java.util.Set) ((com.a.b.d.aal) p3).a), com.a.b.b.cp.a(((com.a.b.d.aal) p3).b, p4));
            }
        } else {
            if (!(((java.util.SortedSet) p3) instanceof java.util.NavigableSet)) {
                if (!(((java.util.SortedSet) p3) instanceof com.a.b.d.aal)) {
                    v0_4 = new com.a.b.d.aam(((java.util.SortedSet) com.a.b.b.cn.a(((java.util.SortedSet) p3))), ((com.a.b.b.co) com.a.b.b.cn.a(p4)));
                } else {
                    v0_4 = new com.a.b.d.aam(((java.util.SortedSet) ((com.a.b.d.aal) ((java.util.SortedSet) p3)).a), com.a.b.b.cp.a(((com.a.b.d.aal) ((java.util.SortedSet) p3)).b, p4));
                }
            } else {
                v0_4 = com.a.b.d.aad.a(((java.util.NavigableSet) ((java.util.SortedSet) p3)), p4);
            }
        }
        return v0_4;
    }

    private static varargs java.util.Set a(java.util.Set[] p1)
    {
        return com.a.b.d.aah.a(java.util.Arrays.asList(p1));
    }

    private static java.util.SortedSet a(java.util.SortedSet p3, com.a.b.b.co p4)
    {
        com.a.b.d.aam v0_4;
        if (!(p3 instanceof java.util.NavigableSet)) {
            if (!(p3 instanceof com.a.b.d.aal)) {
                v0_4 = new com.a.b.d.aam(((java.util.SortedSet) com.a.b.b.cn.a(p3)), ((com.a.b.b.co) com.a.b.b.cn.a(p4)));
            } else {
                v0_4 = new com.a.b.d.aam(((java.util.SortedSet) ((com.a.b.d.aal) p3).a), com.a.b.b.cp.a(((com.a.b.d.aal) p3).b, p4));
            }
        } else {
            v0_4 = com.a.b.d.aad.a(((java.util.NavigableSet) p3), p4);
        }
        return v0_4;
    }

    private static java.util.TreeSet a(java.util.Comparator p2)
    {
        return new java.util.TreeSet(((java.util.Comparator) com.a.b.b.cn.a(p2)));
    }

    static boolean a(java.util.Set p4, Object p5)
    {
        int v0 = 1;
        if (p4 != p5) {
            if (!(p5 instanceof java.util.Set)) {
                v0 = 0;
            } else {
                try {
                    if ((p4.size() != ((java.util.Set) p5).size()) || (!p4.containsAll(((java.util.Set) p5)))) {
                        v0 = 0;
                    }
                } catch (int v0) {
                    v0 = 0;
                } catch (int v0) {
                    v0 = 0;
                }
            }
        }
        return v0;
    }

    static boolean a(java.util.Set p2, java.util.Collection p3)
    {
        com.a.b.b.cn.a(p3);
        if ((p3 instanceof com.a.b.d.xc)) {
            p3 = ((com.a.b.d.xc) p3).n_();
        }
        if ((!(p3 instanceof java.util.Set)) || (p3.size() <= p2.size())) {
            boolean v0_4 = com.a.b.d.aad.a(p2, p3.iterator());
        } else {
            v0_4 = com.a.b.d.nj.a(p2.iterator(), p3);
        }
        return v0_4;
    }

    static boolean a(java.util.Set p2, java.util.Iterator p3)
    {
        int v0 = 0;
        while (p3.hasNext()) {
            v0 |= p2.remove(p3.next());
        }
        return v0;
    }

    public static com.a.b.d.aaq b(java.util.Set p2, java.util.Set p3)
    {
        com.a.b.b.cn.a(p2, "set1");
        com.a.b.b.cn.a(p3, "set2");
        return new com.a.b.d.aaf(p2, com.a.b.b.cp.a(p3), p3);
    }

    private static java.util.EnumSet b(java.util.Collection p1, Class p2)
    {
        java.util.EnumSet v0 = java.util.EnumSet.allOf(p2);
        v0.removeAll(p1);
        return v0;
    }

    private static java.util.HashSet b(Iterable p2)
    {
        java.util.HashSet v0_2;
        if (!(p2 instanceof java.util.Collection)) {
            java.util.Iterator v1_0 = p2.iterator();
            v0_2 = new java.util.HashSet();
            com.a.b.d.nj.a(v0_2, v1_0);
        } else {
            v0_2 = new java.util.HashSet(com.a.b.d.cm.a(p2));
        }
        return v0_2;
    }

    private static java.util.LinkedHashSet b(int p2)
    {
        return new java.util.LinkedHashSet(com.a.b.d.sz.b(p2));
    }

    private static java.util.NavigableSet b(java.util.NavigableSet p1)
    {
        return com.a.b.d.acu.a(p1, 0);
    }

    private static java.util.Set b()
    {
        return java.util.Collections.newSetFromMap(new java.util.concurrent.ConcurrentHashMap());
    }

    private static java.util.Set b(java.util.Set p1)
    {
        return new com.a.b.d.aao(p1);
    }

    private static java.util.SortedSet b(java.util.SortedSet p3, com.a.b.b.co p4)
    {
        com.a.b.d.aam v0_3;
        if (!(p3 instanceof com.a.b.d.aal)) {
            v0_3 = new com.a.b.d.aam(((java.util.SortedSet) com.a.b.b.cn.a(p3)), ((com.a.b.b.co) com.a.b.b.cn.a(p4)));
        } else {
            v0_3 = new com.a.b.d.aam(((java.util.SortedSet) ((com.a.b.d.aal) p3).a), com.a.b.b.cp.a(((com.a.b.d.aal) p3).b, p4));
        }
        return v0_3;
    }

    private static com.a.b.d.aaq c(java.util.Set p2, java.util.Set p3)
    {
        com.a.b.b.cn.a(p2, "set1");
        com.a.b.b.cn.a(p3, "set2");
        return new com.a.b.d.aag(p2, com.a.b.b.cp.a(com.a.b.b.cp.a(p3)), p3);
    }

    private static java.util.LinkedHashSet c()
    {
        return new java.util.LinkedHashSet();
    }

    private static java.util.Set c(Iterable p1)
    {
        java.util.Set v0_2 = java.util.Collections.newSetFromMap(new java.util.concurrent.ConcurrentHashMap());
        com.a.b.d.mq.a(v0_2, p1);
        return v0_2;
    }

    private static com.a.b.d.aaq d(java.util.Set p2, java.util.Set p3)
    {
        com.a.b.b.cn.a(p2, "set1");
        com.a.b.b.cn.a(p3, "set2");
        return com.a.b.d.aad.c(com.a.b.d.aad.a(p2, p3), com.a.b.d.aad.b(p2, p3));
    }

    private static java.util.LinkedHashSet d(Iterable p2)
    {
        java.util.LinkedHashSet v0_2;
        if (!(p2 instanceof java.util.Collection)) {
            v0_2 = new java.util.LinkedHashSet();
            com.a.b.d.mq.a(v0_2, p2);
        } else {
            v0_2 = new java.util.LinkedHashSet(com.a.b.d.cm.a(p2));
        }
        return v0_2;
    }

    private static java.util.TreeSet d()
    {
        return new java.util.TreeSet();
    }

    private static java.util.Set e()
    {
        return java.util.Collections.newSetFromMap(com.a.b.d.sz.f());
    }

    private static java.util.TreeSet e(Iterable p1)
    {
        java.util.TreeSet v0_1 = new java.util.TreeSet();
        com.a.b.d.mq.a(v0_1, p1);
        return v0_1;
    }

    private static java.util.concurrent.CopyOnWriteArraySet f()
    {
        return new java.util.concurrent.CopyOnWriteArraySet();
    }

    private static java.util.concurrent.CopyOnWriteArraySet f(Iterable p2)
    {
        java.util.ArrayList v0_1;
        if (!(p2 instanceof java.util.Collection)) {
            v0_1 = com.a.b.d.ov.a(p2);
        } else {
            v0_1 = com.a.b.d.cm.a(p2);
        }
        return new java.util.concurrent.CopyOnWriteArraySet(v0_1);
    }
}
