package com.a.b.d;
final class afj extends com.a.b.d.tu {
    final synthetic com.a.b.d.afh a;

    afj(com.a.b.d.afh p1)
    {
        this.a = p1;
        return;
    }

    final java.util.Map a()
    {
        return this.a;
    }

    public final boolean isEmpty()
    {
        int v0_2;
        if (this.iterator().hasNext()) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    public final java.util.Iterator iterator()
    {
        com.a.b.d.afk v0_14;
        if (!this.a.a.a.f()) {
            v0_14 = new com.a.b.d.afk(this, com.a.b.d.afb.a(this.a.a.b).tailMap(((com.a.b.d.dw) com.a.b.b.ca.a(com.a.b.d.afb.a(this.a.a.b).floorKey(this.a.a.a.b), this.a.a.a.b)), 1).values().iterator());
        } else {
            v0_14 = com.a.b.d.nj.a();
        }
        return v0_14;
    }

    public final boolean retainAll(java.util.Collection p3)
    {
        return com.a.b.d.afh.a(this.a, com.a.b.b.cp.a(com.a.b.b.cp.a(p3)));
    }

    public final int size()
    {
        return com.a.b.d.nj.b(this.iterator());
    }
}
