package com.a.b.d;
final class aau extends com.a.b.d.it {
    final transient Object a;
    final transient Object b;
    transient com.a.b.d.it c;

    aau(Object p1, Object p2)
    {
        com.a.b.d.cl.a(p1, p2);
        this.a = p1;
        this.b = p2;
        return;
    }

    private aau(Object p1, Object p2, com.a.b.d.it p3)
    {
        this.a = p1;
        this.b = p2;
        this.c = p3;
        return;
    }

    private aau(java.util.Map$Entry p3)
    {
        this(p3.getKey(), p3.getValue());
        return;
    }

    public final com.a.b.d.it a()
    {
        com.a.b.d.aau v0_0 = this.c;
        if (v0_0 == null) {
            v0_0 = new com.a.b.d.aau(this.b, this.a, this);
            this.c = v0_0;
        }
        return v0_0;
    }

    public final synthetic com.a.b.d.bw b()
    {
        return this.a();
    }

    final com.a.b.d.lo c()
    {
        return com.a.b.d.lo.d(this.a);
    }

    public final boolean containsKey(Object p2)
    {
        return this.a.equals(p2);
    }

    public final boolean containsValue(Object p2)
    {
        return this.b.equals(p2);
    }

    final com.a.b.d.lo d()
    {
        return com.a.b.d.lo.d(com.a.b.d.sz.a(this.a, this.b));
    }

    public final Object get(Object p2)
    {
        int v0_2;
        if (!this.a.equals(p2)) {
            v0_2 = 0;
        } else {
            v0_2 = this.b;
        }
        return v0_2;
    }

    final boolean i_()
    {
        return 0;
    }

    public final int size()
    {
        return 1;
    }
}
