package com.a.b.d;
final class mw extends com.a.b.d.gd {
    final synthetic Iterable a;

    mw(Iterable p1)
    {
        this.a = p1;
        return;
    }

    public final java.util.Iterator iterator()
    {
        return com.a.b.d.nj.g(this.a.iterator());
    }

    public final String toString()
    {
        return "Iterables.consumingIterable(...)";
    }
}
