package com.a.b.d;
final class wt implements java.util.Iterator {
    int a;
    final synthetic com.a.b.d.ws b;

    wt(com.a.b.d.ws p1)
    {
        this.b = p1;
        return;
    }

    public final boolean hasNext()
    {
        if ((this.a != 0) || (!this.b.b.a.containsKey(this.b.a))) {
            int v0_5 = 0;
        } else {
            v0_5 = 1;
        }
        return v0_5;
    }

    public final Object next()
    {
        if (this.hasNext()) {
            this.a = (this.a + 1);
            return this.b.b.a.get(this.b.a);
        } else {
            throw new java.util.NoSuchElementException();
        }
    }

    public final void remove()
    {
        int v0_0 = 1;
        if (this.a != 1) {
            v0_0 = 0;
        }
        com.a.b.b.cn.b(v0_0, "no calls to next() since the last call to remove()");
        this.a = -1;
        this.b.b.a.remove(this.b.a);
        return;
    }
}
