package com.a.b.d;
final class r extends com.a.b.d.tu {
    final synthetic com.a.b.d.q a;

    r(com.a.b.d.q p1)
    {
        this.a = p1;
        return;
    }

    final java.util.Map a()
    {
        return this.a;
    }

    public final boolean contains(Object p2)
    {
        return com.a.b.d.cm.a(this.a.a.entrySet(), p2);
    }

    public final java.util.Iterator iterator()
    {
        return new com.a.b.d.s(this.a);
    }

    public final boolean remove(Object p3)
    {
        int v0_3;
        if (this.contains(p3)) {
            com.a.b.d.n.a(this.a.b, ((java.util.Map$Entry) p3).getKey());
            v0_3 = 1;
        } else {
            v0_3 = 0;
        }
        return v0_3;
    }
}
