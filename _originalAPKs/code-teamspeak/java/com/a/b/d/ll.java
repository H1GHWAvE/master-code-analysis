package com.a.b.d;
public final class ll {
    private final com.a.b.d.yr a;

    public ll()
    {
        this.a = com.a.b.d.afm.c();
        return;
    }

    private com.a.b.d.lf a()
    {
        return com.a.b.d.lf.d(this.a);
    }

    private com.a.b.d.ll a(com.a.b.d.yl p8)
    {
        if (!p8.f()) {
            if (this.a.f().c(p8)) {
                this.a.a(p8);
                return this;
            } else {
                java.util.Iterator v4 = this.a.g().iterator();
                while (v4.hasNext()) {
                    StringBuilder v1_4;
                    AssertionError v0_11 = ((com.a.b.d.yl) v4.next());
                    if ((v0_11.b(p8)) && (!v0_11.c(p8).f())) {
                        v1_4 = 0;
                    } else {
                        v1_4 = 1;
                    }
                    Object[] v6_1 = new Object[2];
                    v6_1[0] = v0_11;
                    v6_1[1] = p8;
                    com.a.b.b.cn.a(v1_4, "Ranges may not overlap, but received %s and %s", v6_1);
                }
                throw new AssertionError("should have thrown an IAE above");
            }
        } else {
            StringBuilder v1_6 = String.valueOf(String.valueOf(p8));
            throw new IllegalArgumentException(new StringBuilder((v1_6.length() + 33)).append("range must not be empty, but was ").append(v1_6).toString());
        }
    }

    private com.a.b.d.ll a(com.a.b.d.yr p9)
    {
        String v1_0 = p9.g().iterator();
        while (v1_0.hasNext()) {
            AssertionError v0_3 = ((com.a.b.d.yl) v1_0.next());
            if (!v0_3.f()) {
                if (this.a.f().c(v0_3)) {
                    this.a.a(v0_3);
                } else {
                    java.util.Iterator v5 = this.a.g().iterator();
                    while (v5.hasNext()) {
                        String v2_8;
                        String v1_6 = ((com.a.b.d.yl) v5.next());
                        if ((v1_6.b(v0_3)) && (!v1_6.c(v0_3).f())) {
                            v2_8 = 0;
                        } else {
                            v2_8 = 1;
                        }
                        Object[] v7_1 = new Object[2];
                        v7_1[0] = v1_6;
                        v7_1[1] = v0_3;
                        com.a.b.b.cn.a(v2_8, "Ranges may not overlap, but received %s and %s", v7_1);
                    }
                    throw new AssertionError("should have thrown an IAE above");
                }
            } else {
                AssertionError v0_7 = String.valueOf(String.valueOf(v0_3));
                throw new IllegalArgumentException(new StringBuilder((v0_7.length() + 33)).append("range must not be empty, but was ").append(v0_7).toString());
            }
        }
        return this;
    }
}
