package com.a.b.d;
 class uc extends com.a.b.d.uk {
    final synthetic com.a.b.d.ty a;

    uc(com.a.b.d.ty p1)
    {
        this.a = p1;
        this(p1);
        return;
    }

    private boolean a(com.a.b.b.co p4)
    {
        return com.a.b.d.mq.a(this.a.a.entrySet(), com.a.b.b.cp.a(this.a.b, com.a.b.d.sz.a(p4)));
    }

    public boolean remove(Object p2)
    {
        int v0_2;
        if (!this.a.containsKey(p2)) {
            v0_2 = 0;
        } else {
            this.a.a.remove(p2);
            v0_2 = 1;
        }
        return v0_2;
    }

    public boolean removeAll(java.util.Collection p2)
    {
        return this.a(com.a.b.b.cp.a(p2));
    }

    public boolean retainAll(java.util.Collection p2)
    {
        return this.a(com.a.b.b.cp.a(com.a.b.b.cp.a(p2)));
    }

    public Object[] toArray()
    {
        return com.a.b.d.ov.a(this.iterator()).toArray();
    }

    public Object[] toArray(Object[] p2)
    {
        return com.a.b.d.ov.a(this.iterator()).toArray(p2);
    }
}
