package com.a.b.d;
final class ns implements java.util.Iterator {
    java.util.Iterator a;
    java.util.Iterator b;
    final synthetic Iterable c;

    ns(Iterable p2)
    {
        this.c = p2;
        this.a = com.a.b.d.nj.a();
        return;
    }

    public final boolean hasNext()
    {
        if (!this.a.hasNext()) {
            this.a = this.c.iterator();
        }
        return this.a.hasNext();
    }

    public final Object next()
    {
        if (this.hasNext()) {
            this.b = this.a;
            return this.a.next();
        } else {
            throw new java.util.NoSuchElementException();
        }
    }

    public final void remove()
    {
        int v0_1;
        if (this.b == null) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        com.a.b.b.cn.b(v0_1, "no calls to next() since the last call to remove()");
        this.b.remove();
        this.b = 0;
        return;
    }
}
