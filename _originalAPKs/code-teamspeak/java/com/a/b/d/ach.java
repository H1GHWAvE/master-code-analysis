package com.a.b.d;
final class ach extends com.a.b.d.act {
    final synthetic com.a.b.d.abx a;

    private ach(com.a.b.d.abx p2)
    {
        this.a = p2;
        this(p2, 0);
        return;
    }

    synthetic ach(com.a.b.d.abx p1, byte p2)
    {
        this(p1);
        return;
    }

    public final boolean contains(Object p2)
    {
        return this.a.b(p2);
    }

    public final java.util.Iterator iterator()
    {
        return this.a.o();
    }

    public final boolean remove(Object p5)
    {
        int v0_0 = 0;
        if (p5 != null) {
            java.util.Iterator v2 = this.a.a.values().iterator();
            int v1_3 = 0;
            while (v2.hasNext()) {
                int v0_3 = ((java.util.Map) v2.next());
                if (v0_3.keySet().remove(p5)) {
                    v1_3 = 1;
                    if (v0_3.isEmpty()) {
                        v2.remove();
                    }
                }
            }
            v0_0 = v1_3;
        }
        return v0_0;
    }

    public final boolean removeAll(java.util.Collection p5)
    {
        com.a.b.b.cn.a(p5);
        java.util.Iterator v2 = this.a.a.values().iterator();
        int v1_3 = 0;
        while (v2.hasNext()) {
            int v0_3 = ((java.util.Map) v2.next());
            if (com.a.b.d.nj.a(v0_3.keySet().iterator(), p5)) {
                v1_3 = 1;
                if (v0_3.isEmpty()) {
                    v2.remove();
                }
            }
        }
        return v1_3;
    }

    public final boolean retainAll(java.util.Collection p5)
    {
        com.a.b.b.cn.a(p5);
        java.util.Iterator v2 = this.a.a.values().iterator();
        int v1_3 = 0;
        while (v2.hasNext()) {
            int v0_3 = ((java.util.Map) v2.next());
            if (v0_3.keySet().retainAll(p5)) {
                v1_3 = 1;
                if (v0_3.isEmpty()) {
                    v2.remove();
                }
            }
        }
        return v1_3;
    }

    public final int size()
    {
        return com.a.b.d.nj.b(this.iterator());
    }
}
