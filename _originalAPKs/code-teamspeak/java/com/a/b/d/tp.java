package com.a.b.d;
abstract class tp extends com.a.b.d.gs implements java.util.NavigableMap {
    private transient java.util.Comparator a;
    private transient java.util.Set b;
    private transient java.util.NavigableSet c;

    tp()
    {
        return;
    }

    private static com.a.b.d.yd a(java.util.Comparator p1)
    {
        return com.a.b.d.yd.a(p1).a();
    }

    private java.util.Set d()
    {
        return new com.a.b.d.tq(this);
    }

    protected final java.util.Map a()
    {
        return this.b();
    }

    abstract java.util.NavigableMap b();

    abstract java.util.Iterator c();

    public java.util.Map$Entry ceilingEntry(Object p2)
    {
        return this.b().floorEntry(p2);
    }

    public Object ceilingKey(Object p2)
    {
        return this.b().floorKey(p2);
    }

    public java.util.Comparator comparator()
    {
        com.a.b.d.yd v0_0 = this.a;
        if (v0_0 == null) {
            com.a.b.d.yd v0_2 = this.b().comparator();
            if (v0_2 == null) {
                v0_2 = com.a.b.d.yd.d();
            }
            v0_0 = com.a.b.d.yd.a(v0_2).a();
            this.a = v0_0;
        }
        return v0_0;
    }

    public java.util.NavigableSet descendingKeySet()
    {
        return this.b().navigableKeySet();
    }

    public java.util.NavigableMap descendingMap()
    {
        return this.b();
    }

    public java.util.Set entrySet()
    {
        com.a.b.d.tq v0_0 = this.b;
        if (v0_0 == null) {
            v0_0 = new com.a.b.d.tq(this);
            this.b = v0_0;
        }
        return v0_0;
    }

    public java.util.Map$Entry firstEntry()
    {
        return this.b().lastEntry();
    }

    public Object firstKey()
    {
        return this.b().lastKey();
    }

    public java.util.Map$Entry floorEntry(Object p2)
    {
        return this.b().ceilingEntry(p2);
    }

    public Object floorKey(Object p2)
    {
        return this.b().ceilingKey(p2);
    }

    public java.util.NavigableMap headMap(Object p2, boolean p3)
    {
        return this.b().tailMap(p2, p3).descendingMap();
    }

    public java.util.SortedMap headMap(Object p2)
    {
        return this.headMap(p2, 0);
    }

    public java.util.Map$Entry higherEntry(Object p2)
    {
        return this.b().lowerEntry(p2);
    }

    public Object higherKey(Object p2)
    {
        return this.b().lowerKey(p2);
    }

    protected final synthetic Object k_()
    {
        return this.b();
    }

    public java.util.Set keySet()
    {
        return this.navigableKeySet();
    }

    public java.util.Map$Entry lastEntry()
    {
        return this.b().firstEntry();
    }

    public Object lastKey()
    {
        return this.b().firstKey();
    }

    public java.util.Map$Entry lowerEntry(Object p2)
    {
        return this.b().higherEntry(p2);
    }

    public Object lowerKey(Object p2)
    {
        return this.b().higherKey(p2);
    }

    public java.util.NavigableSet navigableKeySet()
    {
        com.a.b.d.un v0_0 = this.c;
        if (v0_0 == null) {
            v0_0 = new com.a.b.d.un(this);
            this.c = v0_0;
        }
        return v0_0;
    }

    public java.util.Map$Entry pollFirstEntry()
    {
        return this.b().pollLastEntry();
    }

    public java.util.Map$Entry pollLastEntry()
    {
        return this.b().pollFirstEntry();
    }

    public java.util.NavigableMap subMap(Object p2, boolean p3, Object p4, boolean p5)
    {
        return this.b().subMap(p4, p5, p2, p3).descendingMap();
    }

    public java.util.SortedMap subMap(Object p3, Object p4)
    {
        return this.subMap(p3, 1, p4, 0);
    }

    public java.util.NavigableMap tailMap(Object p2, boolean p3)
    {
        return this.b().headMap(p2, p3).descendingMap();
    }

    public java.util.SortedMap tailMap(Object p2)
    {
        return this.tailMap(p2, 1);
    }

    public String toString()
    {
        return com.a.b.d.sz.a(this);
    }

    public java.util.Collection values()
    {
        return new com.a.b.d.vb(this);
    }
}
