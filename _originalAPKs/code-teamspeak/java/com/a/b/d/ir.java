package com.a.b.d;
abstract class ir extends com.a.b.d.jl {

    ir()
    {
        return;
    }

    private static void i()
    {
        throw new java.io.InvalidObjectException("Use SerializedForm");
    }

    abstract com.a.b.d.iz b();

    public boolean contains(Object p2)
    {
        return this.b().contains(p2);
    }

    final Object g()
    {
        return new com.a.b.d.is(this.b());
    }

    final boolean h_()
    {
        return this.b().h_();
    }

    public boolean isEmpty()
    {
        return this.b().isEmpty();
    }

    public int size()
    {
        return this.b().size();
    }
}
