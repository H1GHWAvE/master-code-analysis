package com.a.b.d;
final class afw extends com.a.b.d.afm {
    final synthetic com.a.b.d.afm b;
    private final com.a.b.d.yl c;

    afw(com.a.b.d.afm p5, com.a.b.d.yl p6)
    {
        this.b = p5;
        this(new com.a.b.d.afx(com.a.b.d.yl.c(), p6, p5.a, 0), 0);
        this.c = p6;
        return;
    }

    public final void a(com.a.b.d.yl p6)
    {
        boolean v0_1 = this.c.a(p6);
        Object[] v2_1 = new Object[2];
        v2_1[0] = p6;
        v2_1[1] = this.c;
        com.a.b.b.cn.a(v0_1, "Cannot add range %s to subRangeSet(%s)", v2_1);
        super.a(p6);
        return;
    }

    public final boolean a(Comparable p2)
    {
        if ((!this.c.c(p2)) || (!this.b.a(p2))) {
            int v0_4 = 0;
        } else {
            v0_4 = 1;
        }
        return v0_4;
    }

    public final com.a.b.d.yl b(Comparable p3)
    {
        com.a.b.d.yl v0_0 = 0;
        if (this.c.c(p3)) {
            com.a.b.d.yl v1_3 = this.b.b(p3);
            if (v1_3 != null) {
                v0_0 = v1_3.c(this.c);
            }
        }
        return v0_0;
    }

    public final void b()
    {
        this.b.b(this.c);
        return;
    }

    public final void b(com.a.b.d.yl p3)
    {
        if (p3.b(this.c)) {
            this.b.b(p3.c(this.c));
        }
        return;
    }

    public final boolean c(com.a.b.d.yl p4)
    {
        if ((this.c.f()) || (!this.c.a(p4))) {
            int v0_4 = 0;
        } else {
            int v0_10;
            int v0_5 = this.b;
            com.a.b.b.cn.a(p4);
            com.a.b.d.yl v2_1 = v0_5.a.floorEntry(p4.b);
            if ((v2_1 == null) || (!((com.a.b.d.yl) v2_1.getValue()).a(p4))) {
                v0_10 = 0;
            } else {
                v0_10 = ((com.a.b.d.yl) v2_1.getValue());
            }
            if ((v0_10 == 0) || (v0_10.c(this.c).f())) {
                v0_4 = 0;
            } else {
                v0_4 = 1;
            }
        }
        return v0_4;
    }

    public final com.a.b.d.yr e(com.a.b.d.yl p3)
    {
        if (!p3.a(this.c)) {
            if (!p3.b(this.c)) {
                this = com.a.b.d.lf.c();
            } else {
                this = new com.a.b.d.afw(this, this.c.c(p3));
            }
        }
        return this;
    }
}
