package com.a.b.d;
final class ui extends com.a.b.d.vb {
    java.util.Map a;
    com.a.b.b.co b;

    ui(java.util.Map p1, java.util.Map p2, com.a.b.b.co p3)
    {
        this(p1);
        this.a = p2;
        this.b = p3;
        return;
    }

    private boolean a(com.a.b.b.co p4)
    {
        return com.a.b.d.mq.a(this.a.entrySet(), com.a.b.b.cp.a(this.b, com.a.b.d.sz.b(p4)));
    }

    public final boolean remove(Object p4)
    {
        int v0_3;
        if (com.a.b.d.mq.b(this.a.entrySet(), com.a.b.b.cp.a(this.b, com.a.b.d.sz.b(com.a.b.b.cp.a(p4)))) == null) {
            v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        return v0_3;
    }

    public final boolean removeAll(java.util.Collection p2)
    {
        return this.a(com.a.b.b.cp.a(p2));
    }

    public final boolean retainAll(java.util.Collection p2)
    {
        return this.a(com.a.b.b.cp.a(com.a.b.b.cp.a(p2)));
    }

    public final Object[] toArray()
    {
        return com.a.b.d.ov.a(this.iterator()).toArray();
    }

    public final Object[] toArray(Object[] p2)
    {
        return com.a.b.d.ov.a(this.iterator()).toArray(p2);
    }
}
