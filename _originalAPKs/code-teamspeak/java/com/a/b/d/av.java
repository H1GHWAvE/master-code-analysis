package com.a.b.d;
abstract class av extends java.util.AbstractMap implements java.util.NavigableMap {

    av()
    {
        return;
    }

    abstract java.util.Iterator a();

    abstract java.util.Iterator b();

    public java.util.Map$Entry ceilingEntry(Object p2)
    {
        return this.tailMap(p2, 1).firstEntry();
    }

    public Object ceilingKey(Object p2)
    {
        return com.a.b.d.sz.b(this.ceilingEntry(p2));
    }

    public java.util.NavigableSet descendingKeySet()
    {
        return this.descendingMap().navigableKeySet();
    }

    public java.util.NavigableMap descendingMap()
    {
        return new com.a.b.d.ax(this, 0);
    }

    public java.util.Set entrySet()
    {
        return new com.a.b.d.aw(this);
    }

    public java.util.Map$Entry firstEntry()
    {
        return ((java.util.Map$Entry) com.a.b.d.nj.d(this.a(), 0));
    }

    public Object firstKey()
    {
        Object v0_0 = this.firstEntry();
        if (v0_0 != null) {
            return v0_0.getKey();
        } else {
            throw new java.util.NoSuchElementException();
        }
    }

    public java.util.Map$Entry floorEntry(Object p2)
    {
        return this.headMap(p2, 1).lastEntry();
    }

    public Object floorKey(Object p2)
    {
        return com.a.b.d.sz.b(this.floorEntry(p2));
    }

    public abstract Object get();

    public java.util.SortedMap headMap(Object p2)
    {
        return this.headMap(p2, 0);
    }

    public java.util.Map$Entry higherEntry(Object p2)
    {
        return this.tailMap(p2, 0).firstEntry();
    }

    public Object higherKey(Object p2)
    {
        return com.a.b.d.sz.b(this.higherEntry(p2));
    }

    public java.util.Set keySet()
    {
        return this.navigableKeySet();
    }

    public java.util.Map$Entry lastEntry()
    {
        return ((java.util.Map$Entry) com.a.b.d.nj.d(this.b(), 0));
    }

    public Object lastKey()
    {
        Object v0_0 = this.lastEntry();
        if (v0_0 != null) {
            return v0_0.getKey();
        } else {
            throw new java.util.NoSuchElementException();
        }
    }

    public java.util.Map$Entry lowerEntry(Object p2)
    {
        return this.headMap(p2, 0).lastEntry();
    }

    public Object lowerKey(Object p2)
    {
        return com.a.b.d.sz.b(this.lowerEntry(p2));
    }

    public java.util.NavigableSet navigableKeySet()
    {
        return new com.a.b.d.un(this);
    }

    public java.util.Map$Entry pollFirstEntry()
    {
        return ((java.util.Map$Entry) com.a.b.d.nj.h(this.a()));
    }

    public java.util.Map$Entry pollLastEntry()
    {
        return ((java.util.Map$Entry) com.a.b.d.nj.h(this.b()));
    }

    public abstract int size();

    public java.util.SortedMap subMap(Object p3, Object p4)
    {
        return this.subMap(p3, 1, p4, 0);
    }

    public java.util.SortedMap tailMap(Object p2)
    {
        return this.tailMap(p2, 1);
    }
}
