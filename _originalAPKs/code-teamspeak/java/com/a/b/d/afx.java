package com.a.b.d;
final class afx extends com.a.b.d.av {
    private final com.a.b.d.yl a;
    private final com.a.b.d.yl b;
    private final java.util.NavigableMap c;
    private final java.util.NavigableMap d;

    private afx(com.a.b.d.yl p2, com.a.b.d.yl p3, java.util.NavigableMap p4)
    {
        this.a = ((com.a.b.d.yl) com.a.b.b.cn.a(p2));
        this.b = ((com.a.b.d.yl) com.a.b.b.cn.a(p3));
        this.c = ((java.util.NavigableMap) com.a.b.b.cn.a(p4));
        this.d = new com.a.b.d.aft(p4);
        return;
    }

    synthetic afx(com.a.b.d.yl p1, com.a.b.d.yl p2, java.util.NavigableMap p3, byte p4)
    {
        this(p1, p2, p3);
        return;
    }

    static synthetic com.a.b.d.yl a(com.a.b.d.afx p1)
    {
        return p1.b;
    }

    private com.a.b.d.yl a(Object p5)
    {
        try {
            com.a.b.d.yl v0_15;
            if (!(p5 instanceof com.a.b.d.dw)) {
                v0_15 = 0;
            } else {
                if ((this.a.c(((com.a.b.d.dw) p5))) && ((((com.a.b.d.dw) p5).a(this.b.b) >= 0) && (((com.a.b.d.dw) p5).a(this.b.c) < 0))) {
                    if (!((com.a.b.d.dw) p5).equals(this.b.b)) {
                        com.a.b.d.yl v0_14 = ((com.a.b.d.yl) this.c.get(((com.a.b.d.dw) p5)));
                        if (v0_14 == null) {
                        } else {
                            v0_15 = v0_14.c(this.b);
                        }
                    } else {
                        com.a.b.d.yl v0_19 = ((com.a.b.d.yl) com.a.b.d.sz.c(this.c.floorEntry(((com.a.b.d.dw) p5))));
                        if ((v0_19 == null) || (v0_19.c.a(this.b.b) <= 0)) {
                        } else {
                            v0_15 = v0_19.c(this.b);
                        }
                    }
                } else {
                    v0_15 = 0;
                }
            }
        } catch (com.a.b.d.yl v0) {
            v0_15 = 0;
        }
        return v0_15;
    }

    private java.util.NavigableMap a(com.a.b.d.dw p2, boolean p3)
    {
        return this.a(com.a.b.d.yl.a(p2, com.a.b.d.ce.a(p3)));
    }

    private java.util.NavigableMap a(com.a.b.d.dw p3, boolean p4, com.a.b.d.dw p5, boolean p6)
    {
        return this.a(com.a.b.d.yl.a(p3, com.a.b.d.ce.a(p4), p5, com.a.b.d.ce.a(p6)));
    }

    private java.util.NavigableMap a(com.a.b.d.yl p5)
    {
        com.a.b.d.afx v0_3;
        if (p5.b(this.a)) {
            v0_3 = new com.a.b.d.afx(this.a.c(p5), this.b, this.c);
        } else {
            v0_3 = com.a.b.d.lw.m();
        }
        return v0_3;
    }

    static synthetic com.a.b.d.yl b(com.a.b.d.afx p1)
    {
        return p1.a;
    }

    private java.util.NavigableMap b(com.a.b.d.dw p2, boolean p3)
    {
        return this.a(com.a.b.d.yl.b(p2, com.a.b.d.ce.a(p3)));
    }

    final java.util.Iterator a()
    {
        com.a.b.d.afy v0_18;
        if (!this.b.f()) {
            if (!this.a.c.a(this.b.b)) {
                com.a.b.d.afy v0_10;
                com.a.b.d.afy v0_11;
                com.a.b.d.afy v1_4;
                java.util.Iterator v2_4;
                com.a.b.d.afy v1_5;
                if (!this.a.b.a(this.b.b)) {
                    v1_4 = this.c;
                    v0_10 = this.a.b.c();
                    if (this.a.b.a() != com.a.b.d.ce.b) {
                        v2_4 = v1_4;
                        v1_5 = v0_10;
                        v0_11 = 0;
                    } else {
                        v2_4 = v1_4;
                        v1_5 = v0_10;
                        v0_11 = 1;
                    }
                } else {
                    v1_4 = this.d;
                    v0_10 = this.b.b;
                }
                v0_18 = new com.a.b.d.afy(this, v2_4.tailMap(v1_5, v0_11).values().iterator(), ((com.a.b.d.dw) com.a.b.d.yd.d().a(this.a.c, com.a.b.d.dw.b(this.b.c))));
            } else {
                v0_18 = com.a.b.d.nj.a();
            }
        } else {
            v0_18 = com.a.b.d.nj.a();
        }
        return v0_18;
    }

    final java.util.Iterator b()
    {
        com.a.b.d.afz v0_11;
        if (!this.b.f()) {
            com.a.b.d.afz v0_6;
            com.a.b.d.afz v0_4 = ((com.a.b.d.dw) com.a.b.d.yd.d().a(this.a.c, com.a.b.d.dw.b(this.b.c)));
            java.util.Iterator v1_2 = this.c;
            Comparable v2_3 = v0_4.c();
            if (v0_4.b() != com.a.b.d.ce.b) {
                v0_6 = 0;
            } else {
                v0_6 = 1;
            }
            v0_11 = new com.a.b.d.afz(this, v1_2.headMap(v2_3, v0_6).descendingMap().values().iterator());
        } else {
            v0_11 = com.a.b.d.nj.a();
        }
        return v0_11;
    }

    public final java.util.Comparator comparator()
    {
        return com.a.b.d.yd.d();
    }

    public final boolean containsKey(Object p2)
    {
        int v0_1;
        if (this.a(p2) == null) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    public final synthetic Object get(Object p2)
    {
        return this.a(p2);
    }

    public final synthetic java.util.NavigableMap headMap(Object p2, boolean p3)
    {
        return this.a(com.a.b.d.yl.a(((com.a.b.d.dw) p2), com.a.b.d.ce.a(p3)));
    }

    public final int size()
    {
        return com.a.b.d.nj.b(this.a());
    }

    public final synthetic java.util.NavigableMap subMap(Object p3, boolean p4, Object p5, boolean p6)
    {
        return this.a(com.a.b.d.yl.a(((com.a.b.d.dw) p3), com.a.b.d.ce.a(p4), ((com.a.b.d.dw) p5), com.a.b.d.ce.a(p6)));
    }

    public final synthetic java.util.NavigableMap tailMap(Object p2, boolean p3)
    {
        return this.a(com.a.b.d.yl.b(((com.a.b.d.dw) p2), com.a.b.d.ce.a(p3)));
    }
}
