package com.a.b.d;
final class v implements java.util.Iterator {
    java.util.Map$Entry a;
    final synthetic java.util.Iterator b;
    final synthetic com.a.b.d.u c;

    v(com.a.b.d.u p1, java.util.Iterator p2)
    {
        this.c = p1;
        this.b = p2;
        return;
    }

    public final boolean hasNext()
    {
        return this.b.hasNext();
    }

    public final Object next()
    {
        this.a = ((java.util.Map$Entry) this.b.next());
        return this.a.getKey();
    }

    public final void remove()
    {
        java.util.Collection v0_1;
        if (this.a == null) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        com.a.b.b.cn.b(v0_1, "no calls to next() since the last call to remove()");
        java.util.Collection v0_4 = ((java.util.Collection) this.a.getValue());
        this.b.remove();
        com.a.b.d.n.b(this.c.a, v0_4.size());
        v0_4.clear();
        return;
    }
}
