package com.a.b.d;
final class jd extends com.a.b.d.jt {
    private final transient java.util.EnumMap a;

    private jd(java.util.EnumMap p2)
    {
        int v0_1;
        this.a = p2;
        if (p2.isEmpty()) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        com.a.b.b.cn.a(v0_1);
        return;
    }

    synthetic jd(java.util.EnumMap p1, byte p2)
    {
        this(p1);
        return;
    }

    static com.a.b.d.jt a(java.util.EnumMap p2)
    {
        com.a.b.d.it v0_5;
        switch (p2.size()) {
            case 0:
                v0_5 = com.a.b.d.it.i();
                break;
            case 1:
                com.a.b.d.it v0_3 = ((java.util.Map$Entry) com.a.b.d.mq.b(p2.entrySet()));
                v0_5 = com.a.b.d.it.b(v0_3.getKey(), v0_3.getValue());
                break;
            default:
                v0_5 = new com.a.b.d.jd(p2);
        }
        return v0_5;
    }

    static synthetic java.util.EnumMap a(com.a.b.d.jd p1)
    {
        return p1.a;
    }

    final com.a.b.d.lo c()
    {
        return new com.a.b.d.je(this);
    }

    public final boolean containsKey(Object p2)
    {
        return this.a.containsKey(p2);
    }

    final com.a.b.d.lo d()
    {
        return new com.a.b.d.jf(this);
    }

    public final Object get(Object p2)
    {
        return this.a.get(p2);
    }

    final boolean i_()
    {
        return 0;
    }

    final Object j()
    {
        return new com.a.b.d.jh(this.a);
    }

    public final int size()
    {
        return this.a.size();
    }
}
