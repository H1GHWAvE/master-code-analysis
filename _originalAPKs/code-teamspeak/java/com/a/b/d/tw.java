package com.a.b.d;
final class tw extends com.a.b.d.ty implements com.a.b.d.bw {
    private final com.a.b.d.bw d;

    tw(com.a.b.d.bw p4, com.a.b.b.co p5)
    {
        this(p4, p5);
        this.d = new com.a.b.d.tw(p4.b(), new com.a.b.d.tx(p5), this);
        return;
    }

    private tw(com.a.b.d.bw p1, com.a.b.b.co p2, com.a.b.d.bw p3)
    {
        this(p1, p2);
        this.d = p3;
        return;
    }

    private static com.a.b.b.co a(com.a.b.b.co p1)
    {
        return new com.a.b.d.tx(p1);
    }

    private com.a.b.d.bw d()
    {
        return ((com.a.b.d.bw) this.a);
    }

    public final Object a(Object p2, Object p3)
    {
        com.a.b.b.cn.a(this.b(p2, p3));
        return ((com.a.b.d.bw) this.a).a(p2, p3);
    }

    public final com.a.b.d.bw b()
    {
        return this.d;
    }

    public final java.util.Set j_()
    {
        return this.d.keySet();
    }

    public final synthetic java.util.Collection values()
    {
        return this.j_();
    }
}
