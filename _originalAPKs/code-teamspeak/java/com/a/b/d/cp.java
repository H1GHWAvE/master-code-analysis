package com.a.b.d;
final class cp extends java.util.AbstractCollection {
    final com.a.b.d.jl a;
    final java.util.Comparator b;
    final int c;

    cp(Iterable p2, java.util.Comparator p3)
    {
        this.a = com.a.b.d.yd.a(p3).b(p2);
        this.b = p3;
        this.c = com.a.b.d.cp.a(this.a, p3);
        return;
    }

    private static int a(java.util.List p8, java.util.Comparator p9)
    {
        int v0_0 = 1;
        long v2_0 = 1;
        int v1 = 1;
        while (v1 < p8.size()) {
            if (p9.compare(p8.get((v1 - 1)), p8.get(v1)) < 0) {
                v2_0 *= com.a.b.j.i.a(v1, v0_0);
                v0_0 = 0;
                if (!com.a.b.d.cm.a(v2_0)) {
                    int v0_3 = 2147483647;
                    return v0_3;
                }
            }
            v1++;
            v0_0++;
        }
        int v0_2 = (com.a.b.j.i.a(v1, v0_0) * v2_0);
        if (com.a.b.d.cm.a(v0_2)) {
            v0_3 = ((int) v0_2);
        } else {
            v0_3 = 2147483647;
        }
        return v0_3;
    }

    public final boolean contains(Object p2)
    {
        int v0_1;
        if (!(p2 instanceof java.util.List)) {
            v0_1 = 0;
        } else {
            v0_1 = com.a.b.d.cm.a(this.a, ((java.util.List) p2));
        }
        return v0_1;
    }

    public final boolean isEmpty()
    {
        return 0;
    }

    public final java.util.Iterator iterator()
    {
        return new com.a.b.d.cq(this.a, this.b);
    }

    public final int size()
    {
        return this.c;
    }

    public final String toString()
    {
        String v0_2 = String.valueOf(String.valueOf(this.a));
        return new StringBuilder((v0_2.length() + 30)).append("orderedPermutationCollection(").append(v0_2).append(")").toString();
    }
}
