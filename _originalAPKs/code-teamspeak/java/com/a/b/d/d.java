package com.a.b.d;
final class d implements java.util.Iterator {
    java.util.Map$Entry a;
    final synthetic java.util.Iterator b;
    final synthetic com.a.b.d.c c;

    d(com.a.b.d.c p1, java.util.Iterator p2)
    {
        this.c = p1;
        this.b = p2;
        return;
    }

    private java.util.Map$Entry a()
    {
        this.a = ((java.util.Map$Entry) this.b.next());
        return new com.a.b.d.e(this, this.a);
    }

    public final boolean hasNext()
    {
        return this.b.hasNext();
    }

    public final synthetic Object next()
    {
        this.a = ((java.util.Map$Entry) this.b.next());
        return new com.a.b.d.e(this, this.a);
    }

    public final void remove()
    {
        Object v0_1;
        if (this.a == null) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        com.a.b.b.cn.b(v0_1, "no calls to next() since the last call to remove()");
        Object v0_3 = this.a.getValue();
        this.b.remove();
        com.a.b.d.a.b(this.c.b, v0_3);
        return;
    }
}
