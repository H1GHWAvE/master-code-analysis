package com.a.b.d;
 class ac implements java.util.Iterator {
    final java.util.Iterator a;
    final java.util.Collection b;
    final synthetic com.a.b.d.ab c;

    ac(com.a.b.d.ab p2)
    {
        this.c = p2;
        this.b = this.c.c;
        this.a = com.a.b.d.n.b(p2.c);
        return;
    }

    ac(com.a.b.d.ab p2, java.util.Iterator p3)
    {
        this.c = p2;
        this.b = this.c.c;
        this.a = p3;
        return;
    }

    private java.util.Iterator b()
    {
        this.a();
        return this.a;
    }

    final void a()
    {
        this.c.a();
        if (this.c.c == this.b) {
            return;
        } else {
            throw new java.util.ConcurrentModificationException();
        }
    }

    public boolean hasNext()
    {
        this.a();
        return this.a.hasNext();
    }

    public Object next()
    {
        this.a();
        return this.a.next();
    }

    public void remove()
    {
        this.a.remove();
        com.a.b.d.n.b(this.c.f);
        this.c.b();
        return;
    }
}
