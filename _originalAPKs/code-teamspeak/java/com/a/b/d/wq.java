package com.a.b.d;
final class wq extends com.a.b.d.xr {
    final synthetic com.a.b.d.wn a;

    wq(com.a.b.d.wn p1)
    {
        this.a = p1;
        return;
    }

    final com.a.b.d.xc a()
    {
        return this.a;
    }

    public final boolean contains(Object p4)
    {
        int v0_1;
        if (!(p4 instanceof com.a.b.d.xd)) {
            v0_1 = 0;
        } else {
            int v0_6 = ((java.util.Collection) this.a.b.b().get(((com.a.b.d.xd) p4).a()));
            if ((v0_6 == 0) || (v0_6.size() != ((com.a.b.d.xd) p4).b())) {
                v0_1 = 0;
            } else {
                v0_1 = 1;
            }
        }
        return v0_1;
    }

    public final boolean isEmpty()
    {
        return this.a.b.n();
    }

    public final java.util.Iterator iterator()
    {
        return this.a.b();
    }

    public final boolean remove(Object p4)
    {
        int v0_6;
        if (!(p4 instanceof com.a.b.d.xd)) {
            v0_6 = 0;
        } else {
            int v0_5 = ((java.util.Collection) this.a.b.b().get(((com.a.b.d.xd) p4).a()));
            if ((v0_5 == 0) || (v0_5.size() != ((com.a.b.d.xd) p4).b())) {
            } else {
                v0_5.clear();
                v0_6 = 1;
            }
        }
        return v0_6;
    }

    public final int size()
    {
        return this.a.c();
    }
}
