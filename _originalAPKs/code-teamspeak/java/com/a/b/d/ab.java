package com.a.b.d;
 class ab extends java.util.AbstractCollection {
    final Object b;
    java.util.Collection c;
    final com.a.b.d.ab d;
    final java.util.Collection e;
    final synthetic com.a.b.d.n f;

    ab(com.a.b.d.n p2, Object p3, java.util.Collection p4, com.a.b.d.ab p5)
    {
        java.util.Collection v0;
        this.f = p2;
        this.b = p3;
        this.c = p4;
        this.d = p5;
        if (p5 != null) {
            v0 = p5.c;
        } else {
            v0 = 0;
        }
        this.e = v0;
        return;
    }

    private Object d()
    {
        return this.b;
    }

    private java.util.Collection e()
    {
        return this.c;
    }

    private com.a.b.d.ab f()
    {
        return this.d;
    }

    final void a()
    {
        if (this.d == null) {
            if (this.c.isEmpty()) {
                java.util.Collection v0_6 = ((java.util.Collection) com.a.b.d.n.a(this.f).get(this.b));
                if (v0_6 != null) {
                    this.c = v0_6;
                }
            }
        } else {
            this.d.a();
            if (this.d.c != this.e) {
                throw new java.util.ConcurrentModificationException();
            }
        }
        return;
    }

    public boolean add(Object p4)
    {
        this.a();
        boolean v0_1 = this.c.isEmpty();
        boolean v1_1 = this.c.add(p4);
        if (v1_1) {
            com.a.b.d.n.c(this.f);
            if (v0_1) {
                this.c();
            }
        }
        return v1_1;
    }

    public boolean addAll(java.util.Collection p5)
    {
        boolean v0_2;
        if (!p5.isEmpty()) {
            int v1 = this.size();
            v0_2 = this.c.addAll(p5);
            if (v0_2) {
                com.a.b.d.n.a(this.f, (this.c.size() - v1));
                if (v1 == 0) {
                    this.c();
                }
            }
        } else {
            v0_2 = 0;
        }
        return v0_2;
    }

    final void b()
    {
        while (this.d != null) {
            this = this.d;
        }
        if (this.c.isEmpty()) {
            com.a.b.d.n.a(this.f).remove(this.b);
        }
        return;
    }

    final void c()
    {
        while (this.d != null) {
            this = this.d;
        }
        com.a.b.d.n.a(this.f).put(this.b, this.c);
        return;
    }

    public void clear()
    {
        int v0 = this.size();
        if (v0 != 0) {
            this.c.clear();
            com.a.b.d.n.b(this.f, v0);
            this.b();
        }
        return;
    }

    public boolean contains(Object p2)
    {
        this.a();
        return this.c.contains(p2);
    }

    public boolean containsAll(java.util.Collection p2)
    {
        this.a();
        return this.c.containsAll(p2);
    }

    public boolean equals(Object p2)
    {
        boolean v0_1;
        if (p2 != this) {
            this.a();
            v0_1 = this.c.equals(p2);
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    public int hashCode()
    {
        this.a();
        return this.c.hashCode();
    }

    public java.util.Iterator iterator()
    {
        this.a();
        return new com.a.b.d.ac(this);
    }

    public boolean remove(Object p3)
    {
        this.a();
        boolean v0_1 = this.c.remove(p3);
        if (v0_1) {
            com.a.b.d.n.b(this.f);
            this.b();
        }
        return v0_1;
    }

    public boolean removeAll(java.util.Collection p5)
    {
        boolean v0_2;
        if (!p5.isEmpty()) {
            int v1_0 = this.size();
            v0_2 = this.c.removeAll(p5);
            if (v0_2) {
                com.a.b.d.n.a(this.f, (this.c.size() - v1_0));
                this.b();
            }
        } else {
            v0_2 = 0;
        }
        return v0_2;
    }

    public boolean retainAll(java.util.Collection p5)
    {
        com.a.b.b.cn.a(p5);
        int v0_0 = this.size();
        boolean v1_1 = this.c.retainAll(p5);
        if (v1_1) {
            com.a.b.d.n.a(this.f, (this.c.size() - v0_0));
            this.b();
        }
        return v1_1;
    }

    public int size()
    {
        this.a();
        return this.c.size();
    }

    public String toString()
    {
        this.a();
        return this.c.toString();
    }
}
