package com.a.b.d;
public final class iy {
    final com.a.b.d.ju a;

    public iy()
    {
        this.a = com.a.b.d.jt.l();
        return;
    }

    private com.a.b.d.iw a()
    {
        return new com.a.b.d.iw(this.a.a(), 0);
    }

    private com.a.b.d.iy a(Class p2, Object p3)
    {
        this.a.a(p2, p3);
        return this;
    }

    private com.a.b.d.iy a(java.util.Map p6)
    {
        java.util.Iterator v2 = p6.entrySet().iterator();
        while (v2.hasNext()) {
            Object v0_3 = ((java.util.Map$Entry) v2.next());
            Class v1_1 = ((Class) v0_3.getKey());
            this.a.a(v1_1, com.a.b.l.z.a(v1_1).cast(v0_3.getValue()));
        }
        return this;
    }

    private static Object b(Class p1, Object p2)
    {
        return com.a.b.l.z.a(p1).cast(p2);
    }
}
