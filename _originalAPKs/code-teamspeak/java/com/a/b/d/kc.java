package com.a.b.d;
abstract class kc extends com.a.b.d.lo {

    kc()
    {
        return;
    }

    abstract com.a.b.d.jt b();

    public boolean contains(Object p4)
    {
        int v0 = 0;
        if ((p4 instanceof java.util.Map$Entry)) {
            boolean v1_2 = this.b().get(((java.util.Map$Entry) p4).getKey());
            if ((v1_2) && (v1_2.equals(((java.util.Map$Entry) p4).getValue()))) {
                v0 = 1;
            }
        }
        return v0;
    }

    final Object g()
    {
        return new com.a.b.d.kd(this.b());
    }

    final boolean h_()
    {
        return this.b().i_();
    }

    public int size()
    {
        return this.b().size();
    }
}
