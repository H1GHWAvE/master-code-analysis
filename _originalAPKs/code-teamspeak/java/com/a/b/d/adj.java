package com.a.b.d;
 class adj extends com.a.b.d.adn implements com.a.b.d.vi {
    private static final long f;
    transient java.util.Set a;
    transient java.util.Collection b;
    transient java.util.Collection c;
    transient java.util.Map d;
    transient com.a.b.d.xc e;

    adj(com.a.b.d.vi p2)
    {
        this(p2, 0);
        return;
    }

    com.a.b.d.vi a()
    {
        return ((com.a.b.d.vi) super.d());
    }

    public final boolean a(com.a.b.d.vi p3)
    {
        try {
            return this.a().a(p3);
        } catch (Throwable v0_2) {
            throw v0_2;
        }
    }

    public final boolean a(Object p3, Object p4)
    {
        try {
            return this.a().a(p3, p4);
        } catch (Throwable v0_2) {
            throw v0_2;
        }
    }

    public java.util.Collection b(Object p3, Iterable p4)
    {
        try {
            return this.a().b(p3, p4);
        } catch (Throwable v0_2) {
            throw v0_2;
        }
    }

    public final java.util.Map b()
    {
        try {
            if (this.d == null) {
                this.d = new com.a.b.d.acw(this.a().b(), this.h);
            }
        } catch (java.util.Map v0_4) {
            throw v0_4;
        }
        return this.d;
    }

    public final boolean b(Object p3, Object p4)
    {
        try {
            return this.a().b(p3, p4);
        } catch (Throwable v0_2) {
            throw v0_2;
        }
    }

    public java.util.Collection c(Object p4)
    {
        try {
            return com.a.b.d.acu.b(this.a().c(p4), this.h);
        } catch (Throwable v0_3) {
            throw v0_3;
        }
    }

    public final boolean c(Object p3, Iterable p4)
    {
        try {
            return this.a().c(p3, p4);
        } catch (Throwable v0_2) {
            throw v0_2;
        }
    }

    public final boolean c(Object p3, Object p4)
    {
        try {
            return this.a().c(p3, p4);
        } catch (Throwable v0_2) {
            throw v0_2;
        }
    }

    synthetic Object d()
    {
        return this.a();
    }

    public java.util.Collection d(Object p3)
    {
        try {
            return this.a().d(p3);
        } catch (Throwable v0_2) {
            throw v0_2;
        }
    }

    public boolean equals(Object p3)
    {
        Throwable v0_1;
        if (p3 != this) {
            try {
                v0_1 = this.a().equals(p3);
            } catch (Throwable v0_2) {
                throw v0_2;
            }
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    public final int f()
    {
        try {
            return this.a().f();
        } catch (Throwable v0_2) {
            throw v0_2;
        }
    }

    public final boolean f(Object p3)
    {
        try {
            return this.a().f(p3);
        } catch (Throwable v0_2) {
            throw v0_2;
        }
    }

    public final void g()
    {
        try {
            this.a().g();
            return;
        } catch (Throwable v0_1) {
            throw v0_1;
        }
    }

    public final boolean g(Object p3)
    {
        try {
            return this.a().g(p3);
        } catch (Throwable v0_2) {
            throw v0_2;
        }
    }

    public int hashCode()
    {
        try {
            return this.a().hashCode();
        } catch (Throwable v0_2) {
            throw v0_2;
        }
    }

    public final java.util.Collection i()
    {
        try {
            if (this.b == null) {
                this.b = com.a.b.d.acu.a(this.a().i(), this.h);
            }
        } catch (java.util.Collection v0_5) {
            throw v0_5;
        }
        return this.b;
    }

    public java.util.Collection k()
    {
        try {
            if (this.c == null) {
                this.c = com.a.b.d.acu.b(this.a().k(), this.h);
            }
        } catch (java.util.Collection v0_5) {
            throw v0_5;
        }
        return this.c;
    }

    public final boolean n()
    {
        try {
            return this.a().n();
        } catch (Throwable v0_2) {
            throw v0_2;
        }
    }

    public final java.util.Set p()
    {
        try {
            if (this.a == null) {
                this.a = com.a.b.d.acu.b(this.a().p(), this.h);
            }
        } catch (java.util.Set v0_5) {
            throw v0_5;
        }
        return this.a;
    }

    public final com.a.b.d.xc q()
    {
        try {
            if (this.e == null) {
                com.a.b.d.adk v0_5;
                com.a.b.d.xc v1 = this.a().q();
                if ((!(v1 instanceof com.a.b.d.adk)) && (!(v1 instanceof com.a.b.d.ku))) {
                    v0_5 = new com.a.b.d.adk(v1, this.h);
                } else {
                    v0_5 = v1;
                }
                this.e = v0_5;
            }
        } catch (com.a.b.d.adk v0_7) {
            throw v0_7;
        }
        return this.e;
    }
}
