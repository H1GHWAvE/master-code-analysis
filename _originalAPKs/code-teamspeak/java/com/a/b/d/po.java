package com.a.b.d;
public final class po {

    private po()
    {
        return;
    }

    private static com.a.b.d.aac a(com.a.b.d.aac p1, com.a.b.d.pn p2)
    {
        return new com.a.b.d.qf(p1, p2);
    }

    private static com.a.b.d.abs a(com.a.b.d.abs p1, com.a.b.d.pn p2)
    {
        return new com.a.b.d.qg(p1, p2);
    }

    private static com.a.b.d.bw a(com.a.b.d.bw p2, com.a.b.d.pn p3)
    {
        return new com.a.b.d.pw(p2, 0, p3);
    }

    private static com.a.b.d.ou a(com.a.b.d.ou p1, com.a.b.d.pn p2)
    {
        return new com.a.b.d.qa(p1, p2);
    }

    private static com.a.b.d.pn a()
    {
        return com.a.b.d.qi.a;
    }

    private static com.a.b.d.vi a(com.a.b.d.vi p1, com.a.b.d.pn p2)
    {
        return new com.a.b.d.qc(p1, p2);
    }

    static synthetic java.util.Collection a(Object p3, Iterable p4, com.a.b.d.pn p5)
    {
        java.util.ArrayList v0 = com.a.b.d.ov.a(p4);
        java.util.Iterator v1 = v0.iterator();
        while (v1.hasNext()) {
            p5.a(p3, v1.next());
        }
        return v0;
    }

    private static java.util.Collection a(java.util.Collection p1, com.a.b.d.pn p2)
    {
        com.a.b.d.px v0_2;
        if (!(p1 instanceof java.util.Set)) {
            v0_2 = new com.a.b.d.px(p1, p2);
        } else {
            v0_2 = com.a.b.d.po.a(((java.util.Set) p1), p2);
        }
        return v0_2;
    }

    private static java.util.Map$Entry a(java.util.Map$Entry p1, com.a.b.d.pn p2)
    {
        com.a.b.b.cn.a(p1);
        com.a.b.b.cn.a(p2);
        return new com.a.b.d.pp(p1, p2);
    }

    private static java.util.Map a(java.util.Map p1, com.a.b.d.pn p2)
    {
        return new com.a.b.d.qb(p1, p2);
    }

    static java.util.Set a(java.util.Set p1, com.a.b.d.pn p2)
    {
        return new com.a.b.d.pz(p1, p2);
    }

    private static java.util.Collection b(Object p3, Iterable p4, com.a.b.d.pn p5)
    {
        java.util.ArrayList v0 = com.a.b.d.ov.a(p4);
        java.util.Iterator v1 = v0.iterator();
        while (v1.hasNext()) {
            p5.a(p3, v1.next());
        }
        return v0;
    }

    private static synthetic java.util.Collection b(java.util.Collection p1, com.a.b.d.pn p2)
    {
        com.a.b.d.px v0_2;
        if (!(p1 instanceof java.util.Set)) {
            v0_2 = new com.a.b.d.px(p1, p2);
        } else {
            v0_2 = com.a.b.d.po.a(((java.util.Set) p1), p2);
        }
        return v0_2;
    }

    private static java.util.Map$Entry b(java.util.Map$Entry p1, com.a.b.d.pn p2)
    {
        com.a.b.b.cn.a(p1);
        com.a.b.b.cn.a(p2);
        return new com.a.b.d.pq(p1, p2);
    }

    private static java.util.Map b(java.util.Map p4, com.a.b.d.pn p5)
    {
        java.util.LinkedHashMap v1_1 = new java.util.LinkedHashMap(p4);
        java.util.Iterator v2 = v1_1.entrySet().iterator();
        while (v2.hasNext()) {
            Object v0_3 = ((java.util.Map$Entry) v2.next());
            p5.a(v0_3.getKey(), v0_3.getValue());
        }
        return v1_1;
    }

    private static java.util.Set b(java.util.Set p1, com.a.b.d.pn p2)
    {
        return new com.a.b.d.ps(p1, p2);
    }

    private static synthetic java.util.Map$Entry c(java.util.Map$Entry p1, com.a.b.d.pn p2)
    {
        com.a.b.b.cn.a(p1);
        com.a.b.b.cn.a(p2);
        return new com.a.b.d.pp(p1, p2);
    }

    private static synthetic java.util.Map c(java.util.Map p4, com.a.b.d.pn p5)
    {
        java.util.LinkedHashMap v1_1 = new java.util.LinkedHashMap(p4);
        java.util.Iterator v2 = v1_1.entrySet().iterator();
        while (v2.hasNext()) {
            Object v0_3 = ((java.util.Map$Entry) v2.next());
            p5.a(v0_3.getKey(), v0_3.getValue());
        }
        return v1_1;
    }

    private static synthetic java.util.Set c(java.util.Set p1, com.a.b.d.pn p2)
    {
        return com.a.b.d.po.a(p1, p2);
    }

    private static synthetic java.util.Map$Entry d(java.util.Map$Entry p1, com.a.b.d.pn p2)
    {
        com.a.b.b.cn.a(p1);
        com.a.b.b.cn.a(p2);
        return new com.a.b.d.pq(p1, p2);
    }

    private static synthetic java.util.Set d(java.util.Set p1, com.a.b.d.pn p2)
    {
        return new com.a.b.d.ps(p1, p2);
    }
}
