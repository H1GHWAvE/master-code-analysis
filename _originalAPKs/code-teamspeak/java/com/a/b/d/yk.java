package com.a.b.d;
public final class yk {

    private yk()
    {
        return;
    }

    private static int a(java.util.concurrent.BlockingQueue p7, java.util.Collection p8, int p9, long p10, java.util.concurrent.TimeUnit p12)
    {
        com.a.b.b.cn.a(p8);
        long v2_1 = (p12.toNanos(p10) + System.nanoTime());
        int v0_1 = 0;
        while (v0_1 < p9) {
            v0_1 += p7.drainTo(p8, (p9 - v0_1));
            if (v0_1 < p9) {
                Object v1_3 = p7.poll((v2_1 - System.nanoTime()), java.util.concurrent.TimeUnit.NANOSECONDS);
                if (v1_3 == null) {
                    break;
                }
                p8.add(v1_3);
                v0_1++;
            }
        }
        return v0_1;
    }

    private static java.util.ArrayDeque a()
    {
        return new java.util.ArrayDeque();
    }

    private static java.util.ArrayDeque a(Iterable p2)
    {
        java.util.ArrayDeque v0_2;
        if (!(p2 instanceof java.util.Collection)) {
            v0_2 = new java.util.ArrayDeque();
            com.a.b.d.mq.a(v0_2, p2);
        } else {
            v0_2 = new java.util.ArrayDeque(com.a.b.d.cm.a(p2));
        }
        return v0_2;
    }

    private static java.util.Deque a(java.util.Deque p1)
    {
        return new com.a.b.d.ade(p1);
    }

    private static java.util.Queue a(java.util.Queue p1)
    {
        if (!(p1 instanceof com.a.b.d.ado)) {
            p1 = new com.a.b.d.ado(p1);
        }
        return p1;
    }

    private static java.util.concurrent.ArrayBlockingQueue a(int p1)
    {
        return new java.util.concurrent.ArrayBlockingQueue(p1);
    }

    private static int b(java.util.concurrent.BlockingQueue p7, java.util.Collection p8, int p9, long p10, java.util.concurrent.TimeUnit p12)
    {
        int v1_0 = 0;
        com.a.b.b.cn.a(p8);
        long v2_1 = (System.nanoTime() + p12.toNanos(p10));
        int v0_0 = 0;
        while (v0_0 < p9) {
            try {
                v0_0 += p7.drainTo(p8, (p9 - v0_0));
            } catch (int v0_1) {
                if (v1_0 != 0) {
                    Thread.currentThread().interrupt();
                }
                throw v0_1;
            }
            if (v0_0 < p9) {
            }
            try {
                while(true) {
                    Object v4_5 = p7.poll((v2_1 - System.nanoTime()), java.util.concurrent.TimeUnit.NANOSECONDS);
                    v1_0 = 1;
                }
            } catch (int v1) {
            }
            if (v4_5 == null) {
                break;
            }
            p8.add(v4_5);
            v0_0++;
        }
        if (v1_0 != 0) {
            Thread.currentThread().interrupt();
        }
        return v0_0;
    }

    private static java.util.concurrent.ConcurrentLinkedQueue b()
    {
        return new java.util.concurrent.ConcurrentLinkedQueue();
    }

    private static java.util.concurrent.ConcurrentLinkedQueue b(Iterable p2)
    {
        java.util.concurrent.ConcurrentLinkedQueue v0_2;
        if (!(p2 instanceof java.util.Collection)) {
            v0_2 = new java.util.concurrent.ConcurrentLinkedQueue();
            com.a.b.d.mq.a(v0_2, p2);
        } else {
            v0_2 = new java.util.concurrent.ConcurrentLinkedQueue(com.a.b.d.cm.a(p2));
        }
        return v0_2;
    }

    private static java.util.concurrent.LinkedBlockingDeque b(int p1)
    {
        return new java.util.concurrent.LinkedBlockingDeque(p1);
    }

    private static java.util.concurrent.LinkedBlockingDeque c()
    {
        return new java.util.concurrent.LinkedBlockingDeque();
    }

    private static java.util.concurrent.LinkedBlockingDeque c(Iterable p2)
    {
        java.util.concurrent.LinkedBlockingDeque v0_2;
        if (!(p2 instanceof java.util.Collection)) {
            v0_2 = new java.util.concurrent.LinkedBlockingDeque();
            com.a.b.d.mq.a(v0_2, p2);
        } else {
            v0_2 = new java.util.concurrent.LinkedBlockingDeque(com.a.b.d.cm.a(p2));
        }
        return v0_2;
    }

    private static java.util.concurrent.LinkedBlockingQueue c(int p1)
    {
        return new java.util.concurrent.LinkedBlockingQueue(p1);
    }

    private static java.util.concurrent.LinkedBlockingQueue d()
    {
        return new java.util.concurrent.LinkedBlockingQueue();
    }

    private static java.util.concurrent.LinkedBlockingQueue d(Iterable p2)
    {
        java.util.concurrent.LinkedBlockingQueue v0_2;
        if (!(p2 instanceof java.util.Collection)) {
            v0_2 = new java.util.concurrent.LinkedBlockingQueue();
            com.a.b.d.mq.a(v0_2, p2);
        } else {
            v0_2 = new java.util.concurrent.LinkedBlockingQueue(com.a.b.d.cm.a(p2));
        }
        return v0_2;
    }

    private static java.util.concurrent.PriorityBlockingQueue e()
    {
        return new java.util.concurrent.PriorityBlockingQueue();
    }

    private static java.util.concurrent.PriorityBlockingQueue e(Iterable p2)
    {
        java.util.concurrent.PriorityBlockingQueue v0_2;
        if (!(p2 instanceof java.util.Collection)) {
            v0_2 = new java.util.concurrent.PriorityBlockingQueue();
            com.a.b.d.mq.a(v0_2, p2);
        } else {
            v0_2 = new java.util.concurrent.PriorityBlockingQueue(com.a.b.d.cm.a(p2));
        }
        return v0_2;
    }

    private static java.util.PriorityQueue f()
    {
        return new java.util.PriorityQueue();
    }

    private static java.util.PriorityQueue f(Iterable p2)
    {
        java.util.PriorityQueue v0_2;
        if (!(p2 instanceof java.util.Collection)) {
            v0_2 = new java.util.PriorityQueue();
            com.a.b.d.mq.a(v0_2, p2);
        } else {
            v0_2 = new java.util.PriorityQueue(com.a.b.d.cm.a(p2));
        }
        return v0_2;
    }

    private static java.util.concurrent.SynchronousQueue g()
    {
        return new java.util.concurrent.SynchronousQueue();
    }
}
