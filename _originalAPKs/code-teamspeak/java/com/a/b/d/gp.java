package com.a.b.d;
public abstract class gp extends com.a.b.d.gh implements java.util.List {

    protected gp()
    {
        return;
    }

    private java.util.List a(int p2, int p3)
    {
        java.util.List v0_2;
        if (!(this instanceof java.util.RandomAccess)) {
            v0_2 = new com.a.b.d.ox(this);
        } else {
            v0_2 = new com.a.b.d.ow(this);
        }
        return v0_2.subList(p2, p3);
    }

    private java.util.ListIterator a(int p2)
    {
        return new com.a.b.d.oy(this).listIterator(p2);
    }

    private boolean a(int p5, Iterable p6)
    {
        int v0_0 = 0;
        java.util.ListIterator v1 = this.listIterator(p5);
        java.util.Iterator v2 = p6.iterator();
        while (v2.hasNext()) {
            v1.add(v2.next());
            v0_0 = 1;
        }
        return v0_0;
    }

    private boolean a(Object p2)
    {
        this.add(this.size(), p2);
        return 1;
    }

    private java.util.Iterator c()
    {
        return this.listIterator();
    }

    private int d(Object p2)
    {
        return com.a.b.d.ov.b(this, p2);
    }

    private java.util.ListIterator d()
    {
        return this.listIterator(0);
    }

    private int e()
    {
        int v0_0 = 1;
        java.util.Iterator v1 = this.iterator();
        while (v1.hasNext()) {
            int v0_1;
            Object v2_1 = v1.next();
            int v3 = (v0_0 * 31);
            if (v2_1 != null) {
                v0_1 = v2_1.hashCode();
            } else {
                v0_1 = 0;
            }
            v0_0 = (((v0_1 + v3) ^ -1) ^ -1);
        }
        return v0_0;
    }

    private int e(Object p2)
    {
        return com.a.b.d.ov.c(this, p2);
    }

    private boolean f(Object p2)
    {
        return com.a.b.d.ov.a(this, p2);
    }

    protected abstract java.util.List a();

    public void add(int p2, Object p3)
    {
        this.a().add(p2, p3);
        return;
    }

    public boolean addAll(int p2, java.util.Collection p3)
    {
        return this.a().addAll(p2, p3);
    }

    protected synthetic java.util.Collection b()
    {
        return this.a();
    }

    public boolean equals(Object p2)
    {
        if ((p2 != this) && (!this.a().equals(p2))) {
            int v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    public Object get(int p2)
    {
        return this.a().get(p2);
    }

    public int hashCode()
    {
        return this.a().hashCode();
    }

    public int indexOf(Object p2)
    {
        return this.a().indexOf(p2);
    }

    protected synthetic Object k_()
    {
        return this.a();
    }

    public int lastIndexOf(Object p2)
    {
        return this.a().lastIndexOf(p2);
    }

    public java.util.ListIterator listIterator()
    {
        return this.a().listIterator();
    }

    public java.util.ListIterator listIterator(int p2)
    {
        return this.a().listIterator(p2);
    }

    public Object remove(int p2)
    {
        return this.a().remove(p2);
    }

    public Object set(int p2, Object p3)
    {
        return this.a().set(p2, p3);
    }

    public java.util.List subList(int p2, int p3)
    {
        return this.a().subList(p2, p3);
    }
}
