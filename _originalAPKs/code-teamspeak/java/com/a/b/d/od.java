package com.a.b.d;
final class od implements java.util.Iterator {
    com.a.b.d.oe a;
    com.a.b.d.oe b;
    final synthetic com.a.b.d.oc c;

    od(com.a.b.d.oc p2)
    {
        this.c = p2;
        this.a = com.a.b.d.oc.a(this.c).h;
        return;
    }

    private java.util.Map$Entry a()
    {
        if (this.hasNext()) {
            com.a.b.d.oe v0_1 = this.a;
            this.b = v0_1;
            this.a = this.a.h;
            return v0_1;
        } else {
            throw new java.util.NoSuchElementException();
        }
    }

    public final boolean hasNext()
    {
        int v0_1;
        if (this.a == com.a.b.d.oc.a(this.c)) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    public final synthetic Object next()
    {
        if (this.hasNext()) {
            com.a.b.d.oe v0_1 = this.a;
            this.b = v0_1;
            this.a = this.a.h;
            return v0_1;
        } else {
            throw new java.util.NoSuchElementException();
        }
    }

    public final void remove()
    {
        int v0_1;
        if (this.b == null) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        com.a.b.b.cn.b(v0_1, "no calls to next() since the last call to remove()");
        this.c.c(this.b.getKey(), this.b.getValue());
        this.b = 0;
        return;
    }
}
