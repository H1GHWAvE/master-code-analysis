package com.a.b.d;
final class fa extends com.a.b.d.lw {
    private final transient com.a.b.d.me a;

    fa(java.util.Comparator p2)
    {
        this.a = com.a.b.d.me.a(p2);
        return;
    }

    private fa(java.util.Comparator p2, com.a.b.d.lw p3)
    {
        this(p3);
        this.a = com.a.b.d.me.a(p2);
        return;
    }

    public final com.a.b.d.lw a(Object p1, boolean p2)
    {
        com.a.b.b.cn.a(p1);
        return this;
    }

    public final com.a.b.d.me a()
    {
        return this.a;
    }

    public final com.a.b.d.lw b(Object p1, boolean p2)
    {
        com.a.b.b.cn.a(p1);
        return this;
    }

    final com.a.b.d.lo d()
    {
        throw new AssertionError("should never be called");
    }

    public final com.a.b.d.lo e()
    {
        return com.a.b.d.lo.h();
    }

    public final synthetic java.util.Set entrySet()
    {
        return com.a.b.d.lo.h();
    }

    public final com.a.b.d.lr f()
    {
        return com.a.b.d.lr.a();
    }

    public final bridge synthetic com.a.b.d.lo g()
    {
        return this.a;
    }

    public final Object get(Object p2)
    {
        return 0;
    }

    public final com.a.b.d.iz h()
    {
        return com.a.b.d.jl.d();
    }

    public final synthetic java.util.NavigableMap headMap(Object p1, boolean p2)
    {
        com.a.b.b.cn.a(p1);
        return this;
    }

    final com.a.b.d.lw i()
    {
        return new com.a.b.d.fa(com.a.b.d.yd.a(this.comparator()).a(), this);
    }

    final boolean i_()
    {
        return 0;
    }

    public final boolean isEmpty()
    {
        return 1;
    }

    public final bridge synthetic java.util.Set keySet()
    {
        return this.a;
    }

    public final int size()
    {
        return 0;
    }

    public final synthetic java.util.NavigableMap tailMap(Object p1, boolean p2)
    {
        com.a.b.b.cn.a(p1);
        return this;
    }

    public final String toString()
    {
        return "{}";
    }

    public final synthetic java.util.Collection values()
    {
        return com.a.b.d.jl.d();
    }
}
