package com.a.b.d;
final class ya extends com.a.b.d.yd implements java.io.Serializable {
    private static final long b;
    final com.a.b.d.yd a;

    ya(com.a.b.d.yd p1)
    {
        this.a = p1;
        return;
    }

    public final com.a.b.d.yd a()
    {
        return this.a.a().c();
    }

    public final com.a.b.d.yd b()
    {
        return this;
    }

    public final com.a.b.d.yd c()
    {
        return this.a.c();
    }

    public final int compare(Object p2, Object p3)
    {
        int v0_1;
        if (p2 != p3) {
            if (p2 != null) {
                if (p3 != null) {
                    v0_1 = this.a.compare(p2, p3);
                } else {
                    v0_1 = 1;
                }
            } else {
                v0_1 = -1;
            }
        } else {
            v0_1 = 0;
        }
        return v0_1;
    }

    public final boolean equals(Object p3)
    {
        int v0_1;
        if (p3 != this) {
            if (!(p3 instanceof com.a.b.d.ya)) {
                v0_1 = 0;
            } else {
                v0_1 = this.a.equals(((com.a.b.d.ya) p3).a);
            }
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    public final int hashCode()
    {
        return (this.a.hashCode() ^ 957692532);
    }

    public final String toString()
    {
        String v0_2 = String.valueOf(String.valueOf(this.a));
        return new StringBuilder((v0_2.length() + 13)).append(v0_2).append(".nullsFirst()").toString();
    }
}
