package com.a.b.d;
final class lu extends com.a.b.d.lo {
    private final transient com.a.b.d.lr a;

    lu(com.a.b.d.lr p1)
    {
        this.a = p1;
        return;
    }

    public final com.a.b.d.agi c()
    {
        return this.a.w();
    }

    public final boolean contains(Object p4)
    {
        int v0_1;
        if (!(p4 instanceof java.util.Map$Entry)) {
            v0_1 = 0;
        } else {
            v0_1 = this.a.b(((java.util.Map$Entry) p4).getKey(), ((java.util.Map$Entry) p4).getValue());
        }
        return v0_1;
    }

    final boolean h_()
    {
        return 0;
    }

    public final synthetic java.util.Iterator iterator()
    {
        return this.a.w();
    }

    public final int size()
    {
        return this.a.f();
    }
}
