package com.a.b.d;
abstract class ai extends com.a.b.d.as implements java.io.Serializable {
    private static final long c = 16195977368011011642;
    transient java.util.Map a;
    private transient long b;

    protected ai(java.util.Map p3)
    {
        this.a = ((java.util.Map) com.a.b.b.cn.a(p3));
        this.b = ((long) super.size());
        return;
    }

    private static int a(com.a.b.d.dv p1, int p2)
    {
        int v0;
        if (p1 != null) {
            v0 = p1.b(p2);
        } else {
            v0 = 0;
        }
        return v0;
    }

    static synthetic long a(com.a.b.d.ai p3, long p4)
    {
        long v0_1 = (p3.b - p4);
        p3.b = v0_1;
        return v0_1;
    }

    static synthetic java.util.Map a(com.a.b.d.ai p1)
    {
        return p1.a;
    }

    private void a(java.util.Map p1)
    {
        this.a = p1;
        return;
    }

    static synthetic long b(com.a.b.d.ai p4)
    {
        long v0 = p4.b;
        p4.b = (v0 - 1);
        return v0;
    }

    private static void g()
    {
        throw new java.io.InvalidObjectException("Stream data required");
    }

    public int a(Object p2)
    {
        int v0_3;
        int v0_2 = ((com.a.b.d.dv) com.a.b.d.sz.a(this.a, p2));
        if (v0_2 != 0) {
            v0_3 = v0_2.a;
        } else {
            v0_3 = 0;
        }
        return v0_3;
    }

    public int a(Object p11, int p12)
    {
        long v2 = 0;
        if (p12 != 0) {
            long v0_0;
            if (p12 <= 0) {
                v0_0 = 0;
            } else {
                v0_0 = 1;
            }
            long v4_0 = new Object[1];
            v4_0[0] = Integer.valueOf(p12);
            com.a.b.b.cn.a(v0_0, "occurrences cannot be negative: %s", v4_0);
            long v0_3 = ((com.a.b.d.dv) this.a.get(p11));
            if (v0_3 != 0) {
                int v3_2;
                long v4_1 = v0_3.a;
                Long v6_1 = (((long) v4_1) + ((long) p12));
                if (v6_1 > 2147483647) {
                    v3_2 = 0;
                } else {
                    v3_2 = 1;
                }
                int v1_1 = new Object[1];
                v1_1[0] = Long.valueOf(v6_1);
                com.a.b.b.cn.a(v3_2, "too many occurrences: %s", v1_1);
                v0_3.a = (v0_3.a + p12);
                v2 = v4_1;
            } else {
                this.a.put(p11, new com.a.b.d.dv(p12));
            }
            this.b = (this.b + ((long) p12));
        } else {
            v2 = this.a(p11);
        }
        return v2;
    }

    public java.util.Set a()
    {
        return super.a();
    }

    public int b(Object p7, int p8)
    {
        int v2_0 = 0;
        if (p8 != 0) {
            com.a.b.d.dv v0_0;
            if (p8 <= 0) {
                v0_0 = 0;
            } else {
                v0_0 = 1;
            }
            int v1_1 = new Object[1];
            v1_1[0] = Integer.valueOf(p8);
            com.a.b.b.cn.a(v0_0, "occurrences cannot be negative: %s", v1_1);
            com.a.b.d.dv v0_3 = ((com.a.b.d.dv) this.a.get(p7));
            if (v0_3 != null) {
                int v1_2 = v0_3.a;
                if (v1_2 <= p8) {
                    this.a.remove(p7);
                    p8 = v1_2;
                }
                v0_3.a((- p8));
                this.b = (this.b - ((long) p8));
                v2_0 = v1_2;
            }
        } else {
            v2_0 = this.a(p7);
        }
        return v2_0;
    }

    final java.util.Iterator b()
    {
        return new com.a.b.d.aj(this, this.a.entrySet().iterator());
    }

    final int c()
    {
        return this.a.size();
    }

    public int c(Object p7, int p8)
    {
        int v0_5;
        com.a.b.d.cl.a(p8, "count");
        if (p8 != 0) {
            int v0_3 = ((com.a.b.d.dv) this.a.get(p7));
            int v1_0 = com.a.b.d.ai.a(v0_3, p8);
            if (v0_3 == 0) {
                this.a.put(p7, new com.a.b.d.dv(p8));
            }
            v0_5 = v1_0;
        } else {
            v0_5 = com.a.b.d.ai.a(((com.a.b.d.dv) this.a.remove(p7)), p8);
        }
        this.b = (this.b + ((long) (p8 - v0_5)));
        return v0_5;
    }

    public void clear()
    {
        java.util.Iterator v1 = this.a.values().iterator();
        while (v1.hasNext()) {
            ((com.a.b.d.dv) v1.next()).a = 0;
        }
        this.a.clear();
        this.b = 0;
        return;
    }

    public java.util.Iterator iterator()
    {
        return new com.a.b.d.al(this);
    }

    public int size()
    {
        return com.a.b.l.q.b(this.b);
    }
}
