package com.a.b.d;
final class ut extends com.a.b.d.uu implements java.util.NavigableMap {

    ut(java.util.NavigableMap p1, com.a.b.d.tv p2)
    {
        this(p1, p2);
        return;
    }

    private java.util.Map$Entry a(java.util.Map$Entry p2)
    {
        java.util.Map$Entry v0_1;
        if (p2 != null) {
            v0_1 = com.a.b.d.sz.a(this.b, p2);
        } else {
            v0_1 = 0;
        }
        return v0_1;
    }

    private java.util.NavigableMap a(Object p2)
    {
        return this.headMap(p2, 0);
    }

    private java.util.NavigableMap a(Object p3, Object p4)
    {
        return this.subMap(p3, 1, p4, 0);
    }

    private java.util.NavigableMap b(Object p2)
    {
        return this.tailMap(p2, 1);
    }

    private java.util.NavigableMap d()
    {
        return ((java.util.NavigableMap) super.c());
    }

    protected final bridge synthetic java.util.SortedMap c()
    {
        return ((java.util.NavigableMap) super.c());
    }

    public final java.util.Map$Entry ceilingEntry(Object p2)
    {
        return this.a(((java.util.NavigableMap) super.c()).ceilingEntry(p2));
    }

    public final Object ceilingKey(Object p2)
    {
        return ((java.util.NavigableMap) super.c()).ceilingKey(p2);
    }

    public final java.util.NavigableSet descendingKeySet()
    {
        return ((java.util.NavigableMap) super.c()).descendingKeySet();
    }

    public final java.util.NavigableMap descendingMap()
    {
        return com.a.b.d.sz.a(((java.util.NavigableMap) super.c()).descendingMap(), this.b);
    }

    public final java.util.Map$Entry firstEntry()
    {
        return this.a(((java.util.NavigableMap) super.c()).firstEntry());
    }

    public final java.util.Map$Entry floorEntry(Object p2)
    {
        return this.a(((java.util.NavigableMap) super.c()).floorEntry(p2));
    }

    public final Object floorKey(Object p2)
    {
        return ((java.util.NavigableMap) super.c()).floorKey(p2);
    }

    public final java.util.NavigableMap headMap(Object p3, boolean p4)
    {
        return com.a.b.d.sz.a(((java.util.NavigableMap) super.c()).headMap(p3, p4), this.b);
    }

    public final bridge synthetic java.util.SortedMap headMap(Object p2)
    {
        return this.headMap(p2, 0);
    }

    public final java.util.Map$Entry higherEntry(Object p2)
    {
        return this.a(((java.util.NavigableMap) super.c()).higherEntry(p2));
    }

    public final Object higherKey(Object p2)
    {
        return ((java.util.NavigableMap) super.c()).higherKey(p2);
    }

    public final java.util.Map$Entry lastEntry()
    {
        return this.a(((java.util.NavigableMap) super.c()).lastEntry());
    }

    public final java.util.Map$Entry lowerEntry(Object p2)
    {
        return this.a(((java.util.NavigableMap) super.c()).lowerEntry(p2));
    }

    public final Object lowerKey(Object p2)
    {
        return ((java.util.NavigableMap) super.c()).lowerKey(p2);
    }

    public final java.util.NavigableSet navigableKeySet()
    {
        return ((java.util.NavigableMap) super.c()).navigableKeySet();
    }

    public final java.util.Map$Entry pollFirstEntry()
    {
        return this.a(((java.util.NavigableMap) super.c()).pollFirstEntry());
    }

    public final java.util.Map$Entry pollLastEntry()
    {
        return this.a(((java.util.NavigableMap) super.c()).pollLastEntry());
    }

    public final java.util.NavigableMap subMap(Object p3, boolean p4, Object p5, boolean p6)
    {
        return com.a.b.d.sz.a(((java.util.NavigableMap) super.c()).subMap(p3, p4, p5, p6), this.b);
    }

    public final bridge synthetic java.util.SortedMap subMap(Object p3, Object p4)
    {
        return this.subMap(p3, 1, p4, 0);
    }

    public final java.util.NavigableMap tailMap(Object p3, boolean p4)
    {
        return com.a.b.d.sz.a(((java.util.NavigableMap) super.c()).tailMap(p3, p4), this.b);
    }

    public final bridge synthetic java.util.SortedMap tailMap(Object p2)
    {
        return this.tailMap(p2, 1);
    }
}
