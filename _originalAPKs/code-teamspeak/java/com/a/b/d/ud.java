package com.a.b.d;
final class ud extends com.a.b.d.av {
    private final java.util.NavigableMap a;
    private final com.a.b.b.co b;
    private final java.util.Map c;

    ud(java.util.NavigableMap p2, com.a.b.b.co p3)
    {
        this.a = ((java.util.NavigableMap) com.a.b.b.cn.a(p2));
        this.b = p3;
        this.c = new com.a.b.d.ty(p2, p3);
        return;
    }

    static synthetic com.a.b.b.co a(com.a.b.d.ud p1)
    {
        return p1.b;
    }

    static synthetic java.util.NavigableMap b(com.a.b.d.ud p1)
    {
        return p1.a;
    }

    final java.util.Iterator a()
    {
        return com.a.b.d.nj.b(this.a.entrySet().iterator(), this.b);
    }

    final java.util.Iterator b()
    {
        return com.a.b.d.nj.b(this.a.descendingMap().entrySet().iterator(), this.b);
    }

    public final void clear()
    {
        this.c.clear();
        return;
    }

    public final java.util.Comparator comparator()
    {
        return this.a.comparator();
    }

    public final boolean containsKey(Object p2)
    {
        return this.c.containsKey(p2);
    }

    public final java.util.NavigableMap descendingMap()
    {
        return com.a.b.d.sz.a(this.a.descendingMap(), this.b);
    }

    public final java.util.Set entrySet()
    {
        return this.c.entrySet();
    }

    public final Object get(Object p2)
    {
        return this.c.get(p2);
    }

    public final java.util.NavigableMap headMap(Object p3, boolean p4)
    {
        return com.a.b.d.sz.a(this.a.headMap(p3, p4), this.b);
    }

    public final boolean isEmpty()
    {
        int v0_3;
        if (com.a.b.d.mq.d(this.a.entrySet(), this.b)) {
            v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        return v0_3;
    }

    public final java.util.NavigableSet navigableKeySet()
    {
        return new com.a.b.d.ue(this, this);
    }

    public final java.util.Map$Entry pollFirstEntry()
    {
        return ((java.util.Map$Entry) com.a.b.d.mq.b(this.a.entrySet(), this.b));
    }

    public final java.util.Map$Entry pollLastEntry()
    {
        return ((java.util.Map$Entry) com.a.b.d.mq.b(this.a.descendingMap().entrySet(), this.b));
    }

    public final Object put(Object p2, Object p3)
    {
        return this.c.put(p2, p3);
    }

    public final void putAll(java.util.Map p2)
    {
        this.c.putAll(p2);
        return;
    }

    public final Object remove(Object p2)
    {
        return this.c.remove(p2);
    }

    public final int size()
    {
        return this.c.size();
    }

    public final java.util.NavigableMap subMap(Object p3, boolean p4, Object p5, boolean p6)
    {
        return com.a.b.d.sz.a(this.a.subMap(p3, p4, p5, p6), this.b);
    }

    public final java.util.NavigableMap tailMap(Object p3, boolean p4)
    {
        return com.a.b.d.sz.a(this.a.tailMap(p3, p4), this.b);
    }

    public final java.util.Collection values()
    {
        return new com.a.b.d.ui(this, this.a, this.b);
    }
}
