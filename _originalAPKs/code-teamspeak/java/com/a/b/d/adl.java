package com.a.b.d;
final class adl extends com.a.b.d.ads implements java.util.NavigableMap {
    private static final long i;
    transient java.util.NavigableSet a;
    transient java.util.NavigableMap b;
    transient java.util.NavigableSet f;

    adl(java.util.NavigableMap p1, Object p2)
    {
        this(p1, p2);
        return;
    }

    private java.util.NavigableMap c()
    {
        return ((java.util.NavigableMap) super.b());
    }

    final synthetic java.util.Map a()
    {
        return ((java.util.NavigableMap) super.b());
    }

    final bridge synthetic java.util.SortedMap b()
    {
        return ((java.util.NavigableMap) super.b());
    }

    public final java.util.Map$Entry ceilingEntry(Object p4)
    {
        try {
            return com.a.b.d.acu.a(((java.util.NavigableMap) super.b()).ceilingEntry(p4), this.h);
        } catch (Throwable v0_4) {
            throw v0_4;
        }
    }

    public final Object ceilingKey(Object p3)
    {
        try {
            return ((java.util.NavigableMap) super.b()).ceilingKey(p3);
        } catch (Throwable v0_3) {
            throw v0_3;
        }
    }

    final synthetic Object d()
    {
        return ((java.util.NavigableMap) super.b());
    }

    public final java.util.NavigableSet descendingKeySet()
    {
        try {
            java.util.NavigableSet v0_1;
            if (this.a != null) {
                v0_1 = this.a;
            } else {
                v0_1 = com.a.b.d.acu.a(((java.util.NavigableMap) super.b()).descendingKeySet(), this.h);
                this.a = v0_1;
            }
        } catch (java.util.NavigableSet v0_5) {
            throw v0_5;
        }
        return v0_1;
    }

    public final java.util.NavigableMap descendingMap()
    {
        try {
            java.util.NavigableMap v0_1;
            if (this.b != null) {
                v0_1 = this.b;
            } else {
                v0_1 = com.a.b.d.acu.a(((java.util.NavigableMap) super.b()).descendingMap(), this.h);
                this.b = v0_1;
            }
        } catch (java.util.NavigableMap v0_5) {
            throw v0_5;
        }
        return v0_1;
    }

    public final java.util.Map$Entry firstEntry()
    {
        try {
            return com.a.b.d.acu.a(((java.util.NavigableMap) super.b()).firstEntry(), this.h);
        } catch (Throwable v0_4) {
            throw v0_4;
        }
    }

    public final java.util.Map$Entry floorEntry(Object p4)
    {
        try {
            return com.a.b.d.acu.a(((java.util.NavigableMap) super.b()).floorEntry(p4), this.h);
        } catch (Throwable v0_4) {
            throw v0_4;
        }
    }

    public final Object floorKey(Object p3)
    {
        try {
            return ((java.util.NavigableMap) super.b()).floorKey(p3);
        } catch (Throwable v0_3) {
            throw v0_3;
        }
    }

    public final java.util.NavigableMap headMap(Object p4, boolean p5)
    {
        try {
            return com.a.b.d.acu.a(((java.util.NavigableMap) super.b()).headMap(p4, p5), this.h);
        } catch (Throwable v0_4) {
            throw v0_4;
        }
    }

    public final java.util.SortedMap headMap(Object p2)
    {
        return this.headMap(p2, 0);
    }

    public final java.util.Map$Entry higherEntry(Object p4)
    {
        try {
            return com.a.b.d.acu.a(((java.util.NavigableMap) super.b()).higherEntry(p4), this.h);
        } catch (Throwable v0_4) {
            throw v0_4;
        }
    }

    public final Object higherKey(Object p3)
    {
        try {
            return ((java.util.NavigableMap) super.b()).higherKey(p3);
        } catch (Throwable v0_3) {
            throw v0_3;
        }
    }

    public final java.util.Set keySet()
    {
        return this.navigableKeySet();
    }

    public final java.util.Map$Entry lastEntry()
    {
        try {
            return com.a.b.d.acu.a(((java.util.NavigableMap) super.b()).lastEntry(), this.h);
        } catch (Throwable v0_4) {
            throw v0_4;
        }
    }

    public final java.util.Map$Entry lowerEntry(Object p4)
    {
        try {
            return com.a.b.d.acu.a(((java.util.NavigableMap) super.b()).lowerEntry(p4), this.h);
        } catch (Throwable v0_4) {
            throw v0_4;
        }
    }

    public final Object lowerKey(Object p3)
    {
        try {
            return ((java.util.NavigableMap) super.b()).lowerKey(p3);
        } catch (Throwable v0_3) {
            throw v0_3;
        }
    }

    public final java.util.NavigableSet navigableKeySet()
    {
        try {
            java.util.NavigableSet v0_1;
            if (this.f != null) {
                v0_1 = this.f;
            } else {
                v0_1 = com.a.b.d.acu.a(((java.util.NavigableMap) super.b()).navigableKeySet(), this.h);
                this.f = v0_1;
            }
        } catch (java.util.NavigableSet v0_5) {
            throw v0_5;
        }
        return v0_1;
    }

    public final java.util.Map$Entry pollFirstEntry()
    {
        try {
            return com.a.b.d.acu.a(((java.util.NavigableMap) super.b()).pollFirstEntry(), this.h);
        } catch (Throwable v0_4) {
            throw v0_4;
        }
    }

    public final java.util.Map$Entry pollLastEntry()
    {
        try {
            return com.a.b.d.acu.a(((java.util.NavigableMap) super.b()).pollLastEntry(), this.h);
        } catch (Throwable v0_4) {
            throw v0_4;
        }
    }

    public final java.util.NavigableMap subMap(Object p4, boolean p5, Object p6, boolean p7)
    {
        try {
            return com.a.b.d.acu.a(((java.util.NavigableMap) super.b()).subMap(p4, p5, p6, p7), this.h);
        } catch (Throwable v0_4) {
            throw v0_4;
        }
    }

    public final java.util.SortedMap subMap(Object p3, Object p4)
    {
        return this.subMap(p3, 1, p4, 0);
    }

    public final java.util.NavigableMap tailMap(Object p4, boolean p5)
    {
        try {
            return com.a.b.d.acu.a(((java.util.NavigableMap) super.b()).tailMap(p4, p5), this.h);
        } catch (Throwable v0_4) {
            throw v0_4;
        }
    }

    public final java.util.SortedMap tailMap(Object p2)
    {
        return this.tailMap(p2, 1);
    }
}
