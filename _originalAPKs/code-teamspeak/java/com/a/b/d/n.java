package com.a.b.d;
abstract class n extends com.a.b.d.an implements java.io.Serializable {
    private static final long c = 2447537837011683357;
    private transient java.util.Map a;
    private transient int b;

    protected n(java.util.Map p2)
    {
        com.a.b.b.cn.a(p2.isEmpty());
        this.a = p2;
        return;
    }

    static synthetic int a(com.a.b.d.n p1, int p2)
    {
        int v0_1 = (p1.b + p2);
        p1.b = v0_1;
        return v0_1;
    }

    static synthetic int a(com.a.b.d.n p2, Object p3)
    {
        int v0_2 = ((java.util.Collection) com.a.b.d.sz.c(p2.a, p3));
        int v1 = 0;
        if (v0_2 != 0) {
            v1 = v0_2.size();
            v0_2.clear();
            p2.b = (p2.b - v1);
        }
        return v1;
    }

    static synthetic java.util.List a(com.a.b.d.n p1, Object p2, java.util.List p3, com.a.b.d.ab p4)
    {
        return p1.a(p2, p3, p4);
    }

    private java.util.List a(Object p2, java.util.List p3, com.a.b.d.ab p4)
    {
        com.a.b.d.ad v0_2;
        if (!(p3 instanceof java.util.RandomAccess)) {
            v0_2 = new com.a.b.d.ad(this, p2, p3, p4);
        } else {
            v0_2 = new com.a.b.d.y(this, p2, p3, p4);
        }
        return v0_2;
    }

    static synthetic java.util.Map a(com.a.b.d.n p1)
    {
        return p1.a;
    }

    static synthetic int b(com.a.b.d.n p2)
    {
        int v0 = p2.b;
        p2.b = (v0 - 1);
        return v0;
    }

    static synthetic int b(com.a.b.d.n p1, int p2)
    {
        int v0_1 = (p1.b - p2);
        p1.b = v0_1;
        return v0_1;
    }

    static synthetic java.util.Iterator b(java.util.Collection p1)
    {
        java.util.Iterator v0_1;
        if (!(p1 instanceof java.util.List)) {
            v0_1 = p1.iterator();
        } else {
            v0_1 = ((java.util.List) p1).listIterator();
        }
        return v0_1;
    }

    static synthetic int c(com.a.b.d.n p2)
    {
        int v0 = p2.b;
        p2.b = (v0 + 1);
        return v0;
    }

    private static java.util.Iterator c(java.util.Collection p1)
    {
        java.util.Iterator v0_1;
        if (!(p1 instanceof java.util.List)) {
            v0_1 = p1.iterator();
        } else {
            v0_1 = ((java.util.List) p1).listIterator();
        }
        return v0_1;
    }

    private java.util.Collection j(Object p3)
    {
        java.util.Collection v0_2 = ((java.util.Collection) this.a.get(p3));
        if (v0_2 == null) {
            v0_2 = this.e(p3);
            this.a.put(p3, v0_2);
        }
        return v0_2;
    }

    private int k(Object p3)
    {
        int v0_2 = ((java.util.Collection) com.a.b.d.sz.c(this.a, p3));
        int v1 = 0;
        if (v0_2 != 0) {
            v1 = v0_2.size();
            v0_2.clear();
            this.b = (this.b - v1);
        }
        return v1;
    }

    java.util.Collection a(Object p3, java.util.Collection p4)
    {
        com.a.b.d.ab v0_4;
        if (!(p4 instanceof java.util.SortedSet)) {
            if (!(p4 instanceof java.util.Set)) {
                if (!(p4 instanceof java.util.List)) {
                    v0_4 = new com.a.b.d.ab(this, p3, p4, 0);
                } else {
                    v0_4 = this.a(p3, ((java.util.List) p4), 0);
                }
            } else {
                v0_4 = new com.a.b.d.ag(this, p3, ((java.util.Set) p4));
            }
        } else {
            v0_4 = new com.a.b.d.ah(this, p3, ((java.util.SortedSet) p4), 0);
        }
        return v0_4;
    }

    java.util.Collection a(java.util.Collection p2)
    {
        java.util.Collection v0_3;
        if (!(p2 instanceof java.util.SortedSet)) {
            if (!(p2 instanceof java.util.Set)) {
                if (!(p2 instanceof java.util.List)) {
                    v0_3 = java.util.Collections.unmodifiableCollection(p2);
                } else {
                    v0_3 = java.util.Collections.unmodifiableList(((java.util.List) p2));
                }
            } else {
                v0_3 = java.util.Collections.unmodifiableSet(((java.util.Set) p2));
            }
        } else {
            v0_3 = java.util.Collections.unmodifiableSortedSet(((java.util.SortedSet) p2));
        }
        return v0_3;
    }

    final void a(java.util.Map p5)
    {
        this.a = p5;
        this.b = 0;
        java.util.Iterator v3 = p5.values().iterator();
        while (v3.hasNext()) {
            int v1_1;
            int v0_3 = ((java.util.Collection) v3.next());
            if (v0_3.isEmpty()) {
                v1_1 = 0;
            } else {
                v1_1 = 1;
            }
            com.a.b.b.cn.a(v1_1);
            this.b = (v0_3.size() + this.b);
        }
        return;
    }

    public boolean a(Object p4, Object p5)
    {
        int v0_4;
        int v0_2 = ((java.util.Collection) this.a.get(p4));
        if (v0_2 != 0) {
            if (!v0_2.add(p5)) {
                v0_4 = 0;
            } else {
                this.b = (this.b + 1);
                v0_4 = 1;
            }
        } else {
            int v0_7 = this.e(p4);
            if (!v0_7.add(p5)) {
                throw new AssertionError("New Collection violated the Collection spec");
            } else {
                this.b = (this.b + 1);
                this.a.put(p4, v0_7);
                v0_4 = 1;
            }
        }
        return v0_4;
    }

    public java.util.Collection b(Object p6, Iterable p7)
    {
        java.util.Collection v0_4;
        java.util.Iterator v1 = p7.iterator();
        if (v1.hasNext()) {
            java.util.Collection v0_3 = ((java.util.Collection) this.a.get(p6));
            if (v0_3 == null) {
                v0_3 = this.e(p6);
                this.a.put(p6, v0_3);
            }
            java.util.Collection v2_1 = this.c();
            v2_1.addAll(v0_3);
            this.b = (this.b - v0_3.size());
            v0_3.clear();
            while (v1.hasNext()) {
                if (v0_3.add(v1.next())) {
                    this.b = (this.b + 1);
                }
            }
            v0_4 = this.a(v2_1);
        } else {
            v0_4 = this.d(p6);
        }
        return v0_4;
    }

    abstract java.util.Collection c();

    public java.util.Collection c(Object p2)
    {
        java.util.Collection v0_2 = ((java.util.Collection) this.a.get(p2));
        if (v0_2 == null) {
            v0_2 = this.e(p2);
        }
        return this.a(p2, v0_2);
    }

    java.util.Collection d()
    {
        return this.a(this.c());
    }

    public java.util.Collection d(Object p5)
    {
        java.util.Collection v0_3;
        java.util.Collection v0_2 = ((java.util.Collection) this.a.remove(p5));
        if (v0_2 != null) {
            java.util.Collection v1 = this.c();
            v1.addAll(v0_2);
            this.b = (this.b - v0_2.size());
            v0_2.clear();
            v0_3 = this.a(v1);
        } else {
            v0_3 = this.d();
        }
        return v0_3;
    }

    java.util.Collection e(Object p2)
    {
        return this.c();
    }

    java.util.Map e()
    {
        return this.a;
    }

    public int f()
    {
        return this.b;
    }

    public boolean f(Object p2)
    {
        return this.a.containsKey(p2);
    }

    public void g()
    {
        java.util.Iterator v1 = this.a.values().iterator();
        while (v1.hasNext()) {
            ((java.util.Collection) v1.next()).clear();
        }
        this.a.clear();
        this.b = 0;
        return;
    }

    java.util.Set h()
    {
        com.a.b.d.u v0_3;
        if (!(this.a instanceof java.util.SortedMap)) {
            v0_3 = new com.a.b.d.u(this, this.a);
        } else {
            v0_3 = new com.a.b.d.aa(this, ((java.util.SortedMap) this.a));
        }
        return v0_3;
    }

    public java.util.Collection i()
    {
        return super.i();
    }

    java.util.Iterator j()
    {
        return new com.a.b.d.o(this);
    }

    public java.util.Collection k()
    {
        return super.k();
    }

    java.util.Iterator l()
    {
        return new com.a.b.d.p(this);
    }

    java.util.Map m()
    {
        com.a.b.d.q v0_3;
        if (!(this.a instanceof java.util.SortedMap)) {
            v0_3 = new com.a.b.d.q(this, this.a);
        } else {
            v0_3 = new com.a.b.d.z(this, ((java.util.SortedMap) this.a));
        }
        return v0_3;
    }
}
