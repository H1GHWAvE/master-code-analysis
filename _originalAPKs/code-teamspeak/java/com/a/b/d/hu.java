package com.a.b.d;
final enum class hu extends java.lang.Enum implements com.a.b.d.qw {
    public static final enum com.a.b.d.hu a;
    private static final synthetic com.a.b.d.hu[] b;

    static hu()
    {
        com.a.b.d.hu.a = new com.a.b.d.hu("INSTANCE");
        com.a.b.d.hu[] v0_3 = new com.a.b.d.hu[1];
        v0_3[0] = com.a.b.d.hu.a;
        com.a.b.d.hu.b = v0_3;
        return;
    }

    private hu(String p2)
    {
        this(p2, 0);
        return;
    }

    public static com.a.b.d.hu valueOf(String p1)
    {
        return ((com.a.b.d.hu) Enum.valueOf(com.a.b.d.hu, p1));
    }

    public static com.a.b.d.hu[] values()
    {
        return ((com.a.b.d.hu[]) com.a.b.d.hu.b.clone());
    }

    public final void a()
    {
        return;
    }
}
