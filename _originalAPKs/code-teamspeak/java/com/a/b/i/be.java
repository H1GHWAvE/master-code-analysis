package com.a.b.i;
final class be extends com.a.b.d.aga {

    be()
    {
        return;
    }

    private static Iterable a(java.io.File p1)
    {
        java.util.List v0_2;
        if (!p1.isDirectory()) {
            v0_2 = java.util.Collections.emptyList();
        } else {
            java.util.List v0_1 = p1.listFiles();
            if (v0_1 == null) {
            } else {
                v0_2 = java.util.Collections.unmodifiableList(java.util.Arrays.asList(v0_1));
            }
        }
        return v0_2;
    }

    public final synthetic Iterable a(Object p2)
    {
        java.util.List v0_2;
        if (!((java.io.File) p2).isDirectory()) {
            v0_2 = java.util.Collections.emptyList();
        } else {
            java.util.List v0_1 = ((java.io.File) p2).listFiles();
            if (v0_1 == null) {
            } else {
                v0_2 = java.util.Collections.unmodifiableList(java.util.Arrays.asList(v0_1));
            }
        }
        return v0_2;
    }

    public final String toString()
    {
        return "Files.fileTreeTraverser()";
    }
}
