package com.a.b.i;
final class y extends com.a.b.i.s {
    final synthetic com.a.b.i.s a;
    private final long b;
    private final long c;

    private y(com.a.b.i.s p9, long p10, long p12)
    {
        int v0_1;
        this.a = p9;
        if (p10 < 0) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        int v0_3;
        Long v4_0 = new Object[1];
        v4_0[0] = Long.valueOf(p10);
        com.a.b.b.cn.a(v0_1, "offset (%s) may not be negative", v4_0);
        if (p12 < 0) {
            v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        Object[] v1_1 = new Object[1];
        v1_1[0] = Long.valueOf(p12);
        com.a.b.b.cn.a(v0_3, "length (%s) may not be negative", v1_1);
        this.b = p10;
        this.c = p12;
        return;
    }

    synthetic y(com.a.b.i.s p1, long p2, long p4, byte p6)
    {
        this(p1, p2, p4);
        return;
    }

    private java.io.InputStream a(java.io.InputStream p5)
    {
        if (this.b > 0) {
            try {
                com.a.b.i.z.b(p5, this.b);
            } catch (Throwable v0_3) {
                com.a.b.i.ar v1 = com.a.b.i.ar.a();
                v1.a(p5);
                try {
                    throw v1.a(v0_3);
                } catch (Throwable v0_5) {
                    v1.close();
                    throw v0_5;
                }
            }
        }
        return com.a.b.i.z.a(p5, this.c);
    }

    public final com.a.b.i.s a(long p10, long p12)
    {
        com.a.b.i.s v0_1;
        if (p10 < 0) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        com.a.b.i.s v0_3;
        long v4_0 = new Object[1];
        v4_0[0] = Long.valueOf(p10);
        com.a.b.b.cn.a(v0_1, "offset (%s) may not be negative", v4_0);
        if (p12 < 0) {
            v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        Object[] v1_1 = new Object[1];
        v1_1[0] = Long.valueOf(p12);
        com.a.b.b.cn.a(v0_3, "length (%s) may not be negative", v1_1);
        return this.a.a((this.b + p10), Math.min(p12, (this.c - p10)));
    }

    public final java.io.InputStream a()
    {
        return this.a(this.a.a());
    }

    public final java.io.InputStream b()
    {
        return this.a(this.a.b());
    }

    public final boolean c()
    {
        if ((this.c != 0) && (!super.c())) {
            int v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        return v0_3;
    }

    public final String toString()
    {
        String v0_3 = String.valueOf(String.valueOf(this.a.toString()));
        return new StringBuilder((v0_3.length() + 50)).append(v0_3).append(".slice(").append(this.b).append(", ").append(this.c).append(")").toString();
    }
}
