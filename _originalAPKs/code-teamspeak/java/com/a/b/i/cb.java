package com.a.b.i;
public final class cb extends java.io.FilterInputStream implements java.io.DataInput {

    private cb(java.io.InputStream p2)
    {
        this(((java.io.InputStream) com.a.b.b.cn.a(p2)));
        return;
    }

    private byte a()
    {
        byte v0_1 = this.in.read();
        if (-1 != v0_1) {
            return ((byte) v0_1);
        } else {
            throw new java.io.EOFException();
        }
    }

    public final boolean readBoolean()
    {
        int v0_1;
        if (this.readUnsignedByte() == 0) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    public final byte readByte()
    {
        return ((byte) this.readUnsignedByte());
    }

    public final char readChar()
    {
        return ((char) this.readUnsignedShort());
    }

    public final double readDouble()
    {
        return Double.longBitsToDouble(this.readLong());
    }

    public final float readFloat()
    {
        return Float.intBitsToFloat(this.readInt());
    }

    public final void readFully(byte[] p1)
    {
        com.a.b.i.z.a(this, p1);
        return;
    }

    public final void readFully(byte[] p1, int p2, int p3)
    {
        com.a.b.i.z.a(this, p1, p2, p3);
        return;
    }

    public final int readInt()
    {
        return com.a.b.l.q.a(this.a(), this.a(), this.a(), this.a());
    }

    public final String readLine()
    {
        throw new UnsupportedOperationException("readLine is not supported");
    }

    public final long readLong()
    {
        return com.a.b.l.u.a(this.a(), this.a(), this.a(), this.a(), this.a(), this.a(), this.a(), this.a());
    }

    public final short readShort()
    {
        return ((short) this.readUnsignedShort());
    }

    public final String readUTF()
    {
        return new java.io.DataInputStream(this.in).readUTF();
    }

    public final int readUnsignedByte()
    {
        java.io.EOFException v0_1 = this.in.read();
        if (v0_1 >= null) {
            return v0_1;
        } else {
            throw new java.io.EOFException();
        }
    }

    public final int readUnsignedShort()
    {
        return com.a.b.l.q.a(0, 0, this.a(), this.a());
    }

    public final int skipBytes(int p5)
    {
        return ((int) this.in.skip(((long) p5)));
    }
}
