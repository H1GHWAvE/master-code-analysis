package com.a.b.i;
final class bo extends java.io.InputStream {
    final synthetic com.a.b.i.bs a;

    bo(com.a.b.i.bs p1)
    {
        this.a = p1;
        return;
    }

    public final void close()
    {
        this.a.b();
        return;
    }

    public final int read()
    {
        return this.a.a();
    }

    public final int read(byte[] p5, int p6, int p7)
    {
        com.a.b.b.cn.a(p5);
        com.a.b.b.cn.a(p6, (p6 + p7), p5.length);
        if (p7 != 0) {
            int v1_1 = this.read();
            if (v1_1 != -1) {
                p5[p6] = ((byte) v1_1);
                int v1_3 = 1;
                while (v1_3 < p7) {
                    byte v2_1 = this.read();
                    if (v2_1 != -1) {
                        p5[(p6 + v1_3)] = ((byte) v2_1);
                        v1_3++;
                    } else {
                        p7 = v1_3;
                        break;
                    }
                }
            } else {
                p7 = -1;
            }
        } else {
            p7 = 0;
        }
        return p7;
    }
}
