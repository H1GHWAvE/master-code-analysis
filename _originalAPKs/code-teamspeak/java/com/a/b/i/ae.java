package com.a.b.i;
final class ae extends java.io.FilterInputStream {
    private long a;
    private long b;

    ae(java.io.InputStream p3, long p4)
    {
        int v0_3;
        this(p3);
        this.b = -1;
        com.a.b.b.cn.a(p3);
        if (p4 < 0) {
            v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        com.a.b.b.cn.a(v0_3, "limit must be non-negative");
        this.a = p4;
        return;
    }

    public final int available()
    {
        return ((int) Math.min(((long) this.in.available()), this.a));
    }

    public final declared_synchronized void mark(int p3)
    {
        try {
            this.in.mark(p3);
            this.b = this.a;
            return;
        } catch (Throwable v0_2) {
            throw v0_2;
        }
    }

    public final int read()
    {
        int v0 = -1;
        if (this.a != 0) {
            int v1_2 = this.in.read();
            if (v1_2 != -1) {
                this.a = (this.a - 1);
            }
            v0 = v1_2;
        }
        return v0;
    }

    public final int read(byte[] p7, int p8, int p9)
    {
        int v0 = -1;
        if (this.a != 0) {
            int v1_2 = this.in.read(p7, p8, ((int) Math.min(((long) p9), this.a)));
            if (v1_2 != -1) {
                this.a = (this.a - ((long) v1_2));
            }
            v0 = v1_2;
        }
        return v0;
    }

    public final declared_synchronized void reset()
    {
        try {
            if (this.in.markSupported()) {
                if (this.b != -1) {
                    this.in.reset();
                    this.a = this.b;
                    return;
                } else {
                    throw new java.io.IOException("Mark not set");
                }
            } else {
                throw new java.io.IOException("Mark not supported");
            }
        } catch (long v0_10) {
            throw v0_10;
        }
    }

    public final long skip(long p6)
    {
        long v0_2 = this.in.skip(Math.min(p6, this.a));
        this.a = (this.a - v0_2);
        return v0_2;
    }
}
