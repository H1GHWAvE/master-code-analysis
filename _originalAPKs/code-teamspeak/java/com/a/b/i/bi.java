package com.a.b.i;
final enum class bi extends com.a.b.i.bh {

    bi(String p2)
    {
        this(p2, 0, 0);
        return;
    }

    private static boolean a(java.io.File p1)
    {
        return p1.isDirectory();
    }

    public final synthetic boolean a(Object p2)
    {
        return ((java.io.File) p2).isDirectory();
    }

    public final String toString()
    {
        return "Files.isDirectory()";
    }
}
