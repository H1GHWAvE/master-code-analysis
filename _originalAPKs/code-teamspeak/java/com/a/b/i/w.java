package com.a.b.i;
final class w extends com.a.b.i.s {
    private final Iterable a;

    w(Iterable p2)
    {
        this.a = ((Iterable) com.a.b.b.cn.a(p2));
        return;
    }

    public final java.io.InputStream a()
    {
        return new com.a.b.i.cd(this.a.iterator());
    }

    public final boolean c()
    {
        java.util.Iterator v1 = this.a.iterator();
        while (v1.hasNext()) {
            if (!((com.a.b.i.s) v1.next()).c()) {
                int v0_2 = 0;
            }
            return v0_2;
        }
        v0_2 = 1;
        return v0_2;
    }

    public final long d()
    {
        java.util.Iterator v4 = this.a.iterator();
        long v2_1 = 0;
        while (v4.hasNext()) {
            v2_1 = (((com.a.b.i.s) v4.next()).d() + v2_1);
        }
        return v2_1;
    }

    public final String toString()
    {
        String v0_2 = String.valueOf(String.valueOf(this.a));
        return new StringBuilder((v0_2.length() + 19)).append("ByteSource.concat(").append(v0_2).append(")").toString();
    }
}
