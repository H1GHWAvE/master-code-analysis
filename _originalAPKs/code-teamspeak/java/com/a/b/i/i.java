package com.a.b.i;
final class i extends com.a.b.i.b {
    private final com.a.b.i.b a;
    private final String b;
    private final int c;
    private final com.a.b.b.m d;

    i(com.a.b.i.b p6, String p7, int p8)
    {
        com.a.b.b.m v0_4;
        this.a = ((com.a.b.i.b) com.a.b.b.cn.a(p6));
        this.b = ((String) com.a.b.b.cn.a(p7));
        this.c = p8;
        if (p8 <= 0) {
            v0_4 = 0;
        } else {
            v0_4 = 1;
        }
        Object[] v1_1 = new Object[1];
        v1_1[0] = Integer.valueOf(p8);
        com.a.b.b.cn.a(v0_4, "Cannot add a separator after every %s chars", v1_1);
        this.d = com.a.b.b.m.a(p7).b();
        return;
    }

    final int a(int p6)
    {
        int v0_1 = this.a.a(p6);
        return (v0_1 + (this.b.length() * com.a.b.j.g.a(Math.max(0, (v0_1 - 1)), this.c, java.math.RoundingMode.FLOOR)));
    }

    final com.a.b.b.m a()
    {
        return this.a.a();
    }

    public final com.a.b.i.b a(char p4)
    {
        return this.a.a(p4).a(this.b, this.c);
    }

    public final com.a.b.i.b a(String p3, int p4)
    {
        throw new UnsupportedOperationException("Already have a separator");
    }

    final com.a.b.i.bs a(com.a.b.i.bu p4)
    {
        com.a.b.i.bs v0_0 = this.a;
        com.a.b.b.m v1 = this.d;
        com.a.b.b.cn.a(p4);
        com.a.b.b.cn.a(v1);
        return v0_0.a(new com.a.b.i.e(p4, v1));
    }

    final com.a.b.i.bt a(com.a.b.i.bv p5)
    {
        com.a.b.i.bt v0_0;
        com.a.b.i.b v1 = this.a;
        String v2 = this.b;
        int v3 = this.c;
        com.a.b.b.cn.a(p5);
        com.a.b.b.cn.a(v2);
        if (v3 <= 0) {
            v0_0 = 0;
        } else {
            v0_0 = 1;
        }
        com.a.b.b.cn.a(v0_0);
        return v1.a(new com.a.b.i.f(v3, v2, p5));
    }

    final int b(int p2)
    {
        return this.a.b(p2);
    }

    public final com.a.b.i.b b()
    {
        return this.a.b().a(this.b, this.c);
    }

    public final com.a.b.i.b c()
    {
        return this.a.c().a(this.b, this.c);
    }

    public final com.a.b.i.b d()
    {
        return this.a.d().a(this.b, this.c);
    }

    public final String toString()
    {
        String v0_3 = String.valueOf(String.valueOf(this.a.toString()));
        String v1_2 = String.valueOf(String.valueOf(this.b));
        return new StringBuilder(((v0_3.length() + 31) + v1_2.length())).append(v0_3).append(".withSeparator(\"").append(v1_2).append("\", ").append(this.c).append(")").toString();
    }
}
