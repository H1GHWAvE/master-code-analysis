package com.a.b.b;
final class cx implements com.a.b.b.co, java.io.Serializable {
    private static final long b;
    private final Class a;

    private cx(Class p2)
    {
        this.a = ((Class) com.a.b.b.cn.a(p2));
        return;
    }

    synthetic cx(Class p1, byte p2)
    {
        this(p1);
        return;
    }

    public final boolean a(Object p2)
    {
        return this.a.isInstance(p2);
    }

    public final boolean equals(Object p4)
    {
        int v0 = 0;
        if (((p4 instanceof com.a.b.b.cx)) && (this.a == ((com.a.b.b.cx) p4).a)) {
            v0 = 1;
        }
        return v0;
    }

    public final int hashCode()
    {
        return this.a.hashCode();
    }

    public final String toString()
    {
        String v0_3 = String.valueOf(String.valueOf(this.a.getName()));
        return new StringBuilder((v0_3.length() + 23)).append("Predicates.instanceOf(").append(v0_3).append(")").toString();
    }
}
