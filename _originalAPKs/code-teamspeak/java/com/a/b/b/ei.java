package com.a.b.b;
public final class ei {

    private ei()
    {
        return;
    }

    public static RuntimeException a(Throwable p1)
    {
        com.a.b.b.ei.b(((Throwable) com.a.b.b.cn.a(p1)));
        throw new RuntimeException(p1);
    }

    public static void a(Throwable p1, Class p2)
    {
        if ((p1 == null) || (!p2.isInstance(p1))) {
            return;
        } else {
            throw ((Throwable) p2.cast(p1));
        }
    }

    private static void a(Throwable p0, Class p1, Class p2)
    {
        com.a.b.b.cn.a(p2);
        com.a.b.b.ei.a(p0, p1);
        com.a.b.b.ei.b(p0, p2);
        return;
    }

    private static void b(Throwable p1)
    {
        com.a.b.b.ei.a(p1, Error);
        com.a.b.b.ei.a(p1, RuntimeException);
        return;
    }

    public static void b(Throwable p0, Class p1)
    {
        com.a.b.b.ei.a(p0, p1);
        com.a.b.b.ei.b(p0);
        return;
    }

    private static Throwable c(Throwable p1)
    {
        while(true) {
            Throwable v0 = p1.getCause();
            if (v0 == null) {
                break;
            }
            p1 = v0;
        }
        return p1;
    }

    private static java.util.List d(Throwable p2)
    {
        com.a.b.b.cn.a(p2);
        java.util.List v0_1 = new java.util.ArrayList(4);
        while (p2 != null) {
            v0_1.add(p2);
            p2 = p2.getCause();
        }
        return java.util.Collections.unmodifiableList(v0_1);
    }

    private static String e(Throwable p2)
    {
        String v0_1 = new java.io.StringWriter();
        p2.printStackTrace(new java.io.PrintWriter(v0_1));
        return v0_1.toString();
    }
}
