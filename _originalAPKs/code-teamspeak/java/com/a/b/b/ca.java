package com.a.b.b;
public final class ca {

    private ca()
    {
        return;
    }

    public static com.a.b.b.cc a(Object p3)
    {
        return new com.a.b.b.cc(com.a.b.b.ca.a(p3.getClass()), 0);
    }

    private static com.a.b.b.cc a(String p2)
    {
        return new com.a.b.b.cc(p2, 0);
    }

    public static Object a(Object p0, Object p1)
    {
        if (p0 == null) {
            p0 = com.a.b.b.cn.a(p1);
        }
        return p0;
    }

    public static String a(Class p3)
    {
        String v1_1 = p3.getName().replaceAll("\\$[0-9]+", "\\$");
        String v0_2 = v1_1.lastIndexOf(36);
        if (v0_2 == -1) {
            v0_2 = v1_1.lastIndexOf(46);
        }
        return v1_1.substring((v0_2 + 1));
    }

    private static com.a.b.b.cc b(Class p3)
    {
        return new com.a.b.b.cc(com.a.b.b.ca.a(p3), 0);
    }
}
