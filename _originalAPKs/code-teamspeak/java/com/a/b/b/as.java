package com.a.b.b;
public final class as {
    private static final java.util.Map a;

    static as()
    {
        com.a.b.b.as.a = new java.util.WeakHashMap();
        return;
    }

    private as()
    {
        return;
    }

    private static com.a.b.b.ci a(Class p1, String p2)
    {
        com.a.b.b.ci v0_5;
        com.a.b.b.cn.a(p1);
        com.a.b.b.cn.a(p2);
        com.a.b.b.ci v0_2 = ((ref.WeakReference) com.a.b.b.as.a(p1).get(p2));
        if (v0_2 != null) {
            v0_5 = com.a.b.b.ci.b(p1.cast(v0_2.get()));
        } else {
            v0_5 = com.a.b.b.ci.f();
        }
        return v0_5;
    }

    private static reflect.Field a(Enum p2)
    {
        try {
            return p2.getDeclaringClass().getDeclaredField(p2.name());
        } catch (NoSuchFieldException v0_2) {
            throw new AssertionError(v0_2);
        }
    }

    static java.util.Map a(Class p6)
    {
        try {
            java.util.HashMap v0_2 = ((java.util.Map) com.a.b.b.as.a.get(p6));
        } catch (java.util.HashMap v0_8) {
            throw v0_8;
        }
        if (v0_2 == null) {
            java.util.HashMap v1_1 = new java.util.HashMap();
            java.util.Iterator v3 = java.util.EnumSet.allOf(p6).iterator();
            while (v3.hasNext()) {
                java.util.HashMap v0_7 = ((Enum) v3.next());
                v1_1.put(v0_7.name(), new ref.WeakReference(v0_7));
            }
            com.a.b.b.as.a.put(p6, v1_1);
            v0_2 = v1_1;
        }
        return v0_2;
    }

    private static java.util.Map b(Class p5)
    {
        java.util.HashMap v1_1 = new java.util.HashMap();
        java.util.Iterator v2 = java.util.EnumSet.allOf(p5).iterator();
        while (v2.hasNext()) {
            java.util.Map v0_4 = ((Enum) v2.next());
            v1_1.put(v0_4.name(), new ref.WeakReference(v0_4));
        }
        com.a.b.b.as.a.put(p5, v1_1);
        return v1_1;
    }

    private static com.a.b.b.ak c(Class p1)
    {
        return new com.a.b.b.at(p1);
    }
}
