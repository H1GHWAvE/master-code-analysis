package com.a.b.b;
 class cv implements com.a.b.b.co, java.io.Serializable {
    private static final long b;
    final java.util.regex.Pattern a;

    cv(java.util.regex.Pattern p2)
    {
        this.a = ((java.util.regex.Pattern) com.a.b.b.cn.a(p2));
        return;
    }

    private boolean a(CharSequence p2)
    {
        return this.a.matcher(p2).find();
    }

    public final synthetic boolean a(Object p2)
    {
        return this.a.matcher(((CharSequence) p2)).find();
    }

    public boolean equals(Object p4)
    {
        int v0 = 0;
        if (((p4 instanceof com.a.b.b.cv)) && ((com.a.b.b.ce.a(this.a.pattern(), ((com.a.b.b.cv) p4).a.pattern())) && (com.a.b.b.ce.a(Integer.valueOf(this.a.flags()), Integer.valueOf(((com.a.b.b.cv) p4).a.flags()))))) {
            v0 = 1;
        }
        return v0;
    }

    public int hashCode()
    {
        int v0_1 = new Object[2];
        v0_1[0] = this.a.pattern();
        v0_1[1] = Integer.valueOf(this.a.flags());
        return java.util.Arrays.hashCode(v0_1);
    }

    public String toString()
    {
        String v0_8 = String.valueOf(String.valueOf(new com.a.b.b.cg(com.a.b.b.ca.a(this.a.getClass()), 0).a("pattern", this.a.pattern()).a("pattern.flags", String.valueOf(this.a.flags())).toString()));
        return new StringBuilder((v0_8.length() + 21)).append("Predicates.contains(").append(v0_8).append(")").toString();
    }
}
