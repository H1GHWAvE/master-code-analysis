package com.a.b.b;
final class ct implements com.a.b.b.co, java.io.Serializable {
    private static final long c;
    final com.a.b.b.co a;
    final com.a.b.b.bj b;

    private ct(com.a.b.b.co p2, com.a.b.b.bj p3)
    {
        this.a = ((com.a.b.b.co) com.a.b.b.cn.a(p2));
        this.b = ((com.a.b.b.bj) com.a.b.b.cn.a(p3));
        return;
    }

    synthetic ct(com.a.b.b.co p1, com.a.b.b.bj p2, byte p3)
    {
        this(p1, p2);
        return;
    }

    public final boolean a(Object p3)
    {
        return this.a.a(this.b.e(p3));
    }

    public final boolean equals(Object p4)
    {
        int v0 = 0;
        if (((p4 instanceof com.a.b.b.ct)) && ((this.b.equals(((com.a.b.b.ct) p4).b)) && (this.a.equals(((com.a.b.b.ct) p4).a)))) {
            v0 = 1;
        }
        return v0;
    }

    public final int hashCode()
    {
        return (this.b.hashCode() ^ this.a.hashCode());
    }

    public final String toString()
    {
        String v0_3 = String.valueOf(String.valueOf(this.a.toString()));
        String v1_3 = String.valueOf(String.valueOf(this.b.toString()));
        return new StringBuilder(((v0_3.length() + 2) + v1_3.length())).append(v0_3).append("(").append(v1_3).append(")").toString();
    }
}
