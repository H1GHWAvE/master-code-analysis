package com.a.b.b;
final class ab extends com.a.b.b.ae {
    final synthetic char s;

    ab(String p1, char p2)
    {
        this.s = p2;
        this(p1);
        return;
    }

    public final com.a.b.b.m a()
    {
        return com.a.b.b.ab.b(this.s);
    }

    public final com.a.b.b.m a(com.a.b.b.m p2)
    {
        if (!p2.c(this.s)) {
            this = com.a.b.b.ab.m;
        }
        return this;
    }

    public final String a(CharSequence p3, char p4)
    {
        return p3.toString().replace(this.s, p4);
    }

    final void a(java.util.BitSet p2)
    {
        p2.set(this.s);
        return;
    }

    public final com.a.b.b.m b(com.a.b.b.m p2)
    {
        if (!p2.c(this.s)) {
            p2 = super.b(p2);
        }
        return p2;
    }

    public final boolean c(char p2)
    {
        int v0_1;
        if (p2 != this.s) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }
}
