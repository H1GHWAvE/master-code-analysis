package com.a.b.b;
final class cr implements com.a.b.b.co, java.io.Serializable {
    private static final long b;
    private final java.util.List a;

    private cr(java.util.List p1)
    {
        this.a = p1;
        return;
    }

    synthetic cr(java.util.List p1, byte p2)
    {
        this(p1);
        return;
    }

    public final boolean a(Object p4)
    {
        int v2 = 0;
        int v1 = 0;
        while (v1 < this.a.size()) {
            if (((com.a.b.b.co) this.a.get(v1)).a(p4)) {
                v1++;
            }
            return v2;
        }
        v2 = 1;
        return v2;
    }

    public final boolean equals(Object p3)
    {
        int v0_1;
        if (!(p3 instanceof com.a.b.b.cr)) {
            v0_1 = 0;
        } else {
            v0_1 = this.a.equals(((com.a.b.b.cr) p3).a);
        }
        return v0_1;
    }

    public final int hashCode()
    {
        return (this.a.hashCode() + 306654252);
    }

    public final String toString()
    {
        String v0_3 = String.valueOf(String.valueOf(com.a.b.b.cp.b().a(this.a)));
        return new StringBuilder((v0_3.length() + 16)).append("Predicates.and(").append(v0_3).append(")").toString();
    }
}
