package com.a.b.b;
public final class eg implements com.a.b.b.dz, java.io.Serializable {
    private static final long b;
    final Object a;

    public eg(Object p1)
    {
        this.a = p1;
        return;
    }

    public final Object a()
    {
        return this.a;
    }

    public final boolean equals(Object p3)
    {
        int v0_1;
        if (!(p3 instanceof com.a.b.b.eg)) {
            v0_1 = 0;
        } else {
            v0_1 = com.a.b.b.ce.a(this.a, ((com.a.b.b.eg) p3).a);
        }
        return v0_1;
    }

    public final int hashCode()
    {
        int v0_1 = new Object[1];
        v0_1[0] = this.a;
        return java.util.Arrays.hashCode(v0_1);
    }

    public final String toString()
    {
        String v0_2 = String.valueOf(String.valueOf(this.a));
        return new StringBuilder((v0_2.length() + 22)).append("Suppliers.ofInstance(").append(v0_2).append(")").toString();
    }
}
