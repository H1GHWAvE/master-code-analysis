package com.a.b.b;
public class bc implements java.io.Closeable {
    private static final java.util.logging.Logger d = None;
    private static final String e = "com.google.common.base.internal.Finalizer";
    private static final reflect.Method f;
    final ref.ReferenceQueue a;
    final ref.PhantomReference b;
    final boolean c;

    static bc()
    {
        int v0_0 = 0;
        com.a.b.b.bc.d = java.util.logging.Logger.getLogger(com.a.b.b.bc.getName());
        com.a.b.b.bf[] v1_3 = new com.a.b.b.bf[3];
        v1_3[0] = new com.a.b.b.bg();
        v1_3[1] = new com.a.b.b.bd();
        v1_3[2] = new com.a.b.b.be();
        while (v0_0 < 3) {
            Class v2_5 = v1_3[v0_0].a();
            if (v2_5 == null) {
                v0_0++;
            } else {
                com.a.b.b.bc.f = com.a.b.b.bc.a(v2_5);
                return;
            }
        }
        throw new AssertionError();
    }

    public bc()
    {
        AssertionError v0_0 = 1;
        this.a = new ref.ReferenceQueue();
        this.b = new ref.PhantomReference(this, this.a);
        try {
            String v4_1 = new Object[3];
            v4_1[0] = com.a.b.b.bb;
            v4_1[1] = this.a;
            v4_1[2] = this.b;
            com.a.b.b.bc.f.invoke(0, v4_1);
        } catch (AssertionError v0_2) {
            throw new AssertionError(v0_2);
        } catch (AssertionError v0_1) {
            com.a.b.b.bc.d.log(java.util.logging.Level.INFO, "Failed to start reference finalizer thread. Reference cleanup will only occur when new references are created.", v0_1);
            v0_0 = 0;
        }
        this.c = v0_0;
        return;
    }

    private static varargs Class a(com.a.b.b.bf[] p2)
    {
        int v0_0 = 0;
        while (v0_0 < 3) {
            Class v1_2 = p2[v0_0].a();
            if (v1_2 == null) {
                v0_0++;
            } else {
                return v1_2;
            }
        }
        throw new AssertionError();
    }

    private static reflect.Method a(Class p4)
    {
        try {
            AssertionError v1_1 = new Class[3];
            v1_1[0] = Class;
            v1_1[1] = ref.ReferenceQueue;
            v1_1[2] = ref.PhantomReference;
            return p4.getMethod("startFinalizer", v1_1);
        } catch (NoSuchMethodException v0_2) {
            throw new AssertionError(v0_2);
        }
    }

    static synthetic java.util.logging.Logger b()
    {
        return com.a.b.b.bc.d;
    }

    final void a()
    {
        if (!this.c) {
            while(true) {
                ref.Reference v0_2 = this.a.poll();
                if (v0_2 == null) {
                    break;
                }
                v0_2.clear();
            }
        }
        return;
    }

    public void close()
    {
        this.b.enqueue();
        this.a();
        return;
    }
}
