package com.a.b.b;
public final class dw {
    public boolean a;
    private final com.a.b.b.ej b;
    private long c;
    private long d;

    public dw()
    {
        this(com.a.b.b.ej.b());
        return;
    }

    private dw(com.a.b.b.ej p2)
    {
        this.b = ((com.a.b.b.ej) com.a.b.b.cn.a(p2, "ticker"));
        return;
    }

    public static com.a.b.b.dw a()
    {
        return new com.a.b.b.dw().b();
    }

    private static com.a.b.b.dw a(com.a.b.b.ej p1)
    {
        return new com.a.b.b.dw(p1);
    }

    private static java.util.concurrent.TimeUnit a(long p4)
    {
        java.util.concurrent.TimeUnit v0_18;
        if (java.util.concurrent.TimeUnit.DAYS.convert(p4, java.util.concurrent.TimeUnit.NANOSECONDS) <= 0) {
            if (java.util.concurrent.TimeUnit.HOURS.convert(p4, java.util.concurrent.TimeUnit.NANOSECONDS) <= 0) {
                if (java.util.concurrent.TimeUnit.MINUTES.convert(p4, java.util.concurrent.TimeUnit.NANOSECONDS) <= 0) {
                    if (java.util.concurrent.TimeUnit.SECONDS.convert(p4, java.util.concurrent.TimeUnit.NANOSECONDS) <= 0) {
                        if (java.util.concurrent.TimeUnit.MILLISECONDS.convert(p4, java.util.concurrent.TimeUnit.NANOSECONDS) <= 0) {
                            if (java.util.concurrent.TimeUnit.MICROSECONDS.convert(p4, java.util.concurrent.TimeUnit.NANOSECONDS) <= 0) {
                                v0_18 = java.util.concurrent.TimeUnit.NANOSECONDS;
                            } else {
                                v0_18 = java.util.concurrent.TimeUnit.MICROSECONDS;
                            }
                        } else {
                            v0_18 = java.util.concurrent.TimeUnit.MILLISECONDS;
                        }
                    } else {
                        v0_18 = java.util.concurrent.TimeUnit.SECONDS;
                    }
                } else {
                    v0_18 = java.util.concurrent.TimeUnit.MINUTES;
                }
            } else {
                v0_18 = java.util.concurrent.TimeUnit.HOURS;
            }
        } else {
            v0_18 = java.util.concurrent.TimeUnit.DAYS;
        }
        return v0_18;
    }

    private static com.a.b.b.dw b(com.a.b.b.ej p1)
    {
        return new com.a.b.b.dw(p1).b();
    }

    private static String b(java.util.concurrent.TimeUnit p2)
    {
        String v0_2;
        switch (com.a.b.b.dx.a[p2.ordinal()]) {
            case 1:
                v0_2 = "ns";
                break;
            case 2:
                v0_2 = "\u03bcs";
                break;
            case 3:
                v0_2 = "ms";
                break;
            case 4:
                v0_2 = "s";
                break;
            case 5:
                v0_2 = "min";
                break;
            case 6:
                v0_2 = "h";
                break;
            case 7:
                v0_2 = "d";
                break;
            default:
                throw new AssertionError();
        }
        return v0_2;
    }

    private static com.a.b.b.dw d()
    {
        return new com.a.b.b.dw();
    }

    private boolean e()
    {
        return this.a;
    }

    private com.a.b.b.dw f()
    {
        this.c = 0;
        this.a = 0;
        return this;
    }

    private long g()
    {
        long v0_1;
        if (!this.a) {
            v0_1 = this.c;
        } else {
            v0_1 = ((this.b.a() - this.d) + this.c);
        }
        return v0_1;
    }

    public final long a(java.util.concurrent.TimeUnit p4)
    {
        return p4.convert(this.g(), java.util.concurrent.TimeUnit.NANOSECONDS);
    }

    public final com.a.b.b.dw b()
    {
        long v0_1;
        if (this.a) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        com.a.b.b.cn.b(v0_1, "This stopwatch is already running.");
        this.a = 1;
        this.d = this.b.a();
        return this;
    }

    public final com.a.b.b.dw c()
    {
        long v0_1 = this.b.a();
        com.a.b.b.cn.b(this.a, "This stopwatch is already stopped.");
        this.a = 0;
        this.c = ((v0_1 - this.d) + this.c);
        return this;
    }

    public final String toString()
    {
        java.util.concurrent.TimeUnit v0_18;
        int v2_0 = this.g();
        if (java.util.concurrent.TimeUnit.DAYS.convert(v2_0, java.util.concurrent.TimeUnit.NANOSECONDS) <= 0) {
            if (java.util.concurrent.TimeUnit.HOURS.convert(v2_0, java.util.concurrent.TimeUnit.NANOSECONDS) <= 0) {
                if (java.util.concurrent.TimeUnit.MINUTES.convert(v2_0, java.util.concurrent.TimeUnit.NANOSECONDS) <= 0) {
                    if (java.util.concurrent.TimeUnit.SECONDS.convert(v2_0, java.util.concurrent.TimeUnit.NANOSECONDS) <= 0) {
                        if (java.util.concurrent.TimeUnit.MILLISECONDS.convert(v2_0, java.util.concurrent.TimeUnit.NANOSECONDS) <= 0) {
                            if (java.util.concurrent.TimeUnit.MICROSECONDS.convert(v2_0, java.util.concurrent.TimeUnit.NANOSECONDS) <= 0) {
                                v0_18 = java.util.concurrent.TimeUnit.NANOSECONDS;
                            } else {
                                v0_18 = java.util.concurrent.TimeUnit.MICROSECONDS;
                            }
                        } else {
                            v0_18 = java.util.concurrent.TimeUnit.MILLISECONDS;
                        }
                    } else {
                        v0_18 = java.util.concurrent.TimeUnit.SECONDS;
                    }
                } else {
                    v0_18 = java.util.concurrent.TimeUnit.MINUTES;
                }
            } else {
                v0_18 = java.util.concurrent.TimeUnit.HOURS;
            }
        } else {
            v0_18 = java.util.concurrent.TimeUnit.DAYS;
        }
        java.util.concurrent.TimeUnit v0_21;
        Object[] v4_5 = new Object[2];
        v4_5[0] = Double.valueOf((((double) v2_0) / ((double) java.util.concurrent.TimeUnit.NANOSECONDS.convert(1, v0_18))));
        switch (com.a.b.b.dx.a[v0_18.ordinal()]) {
            case 1:
                v0_21 = "ns";
                break;
            case 2:
                v0_21 = "\u03bcs";
                break;
            case 3:
                v0_21 = "ms";
                break;
            case 4:
                v0_21 = "s";
                break;
            case 5:
                v0_21 = "min";
                break;
            case 6:
                v0_21 = "h";
                break;
            case 7:
                v0_21 = "d";
                break;
            default:
                throw new AssertionError();
        }
        v4_5[1] = v0_21;
        return String.format("%.4g %s", v4_5);
    }
}
