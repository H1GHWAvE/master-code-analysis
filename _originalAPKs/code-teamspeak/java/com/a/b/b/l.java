package com.a.b.b;
final class l extends com.a.b.b.ak implements java.io.Serializable {
    private static final long c;
    private final com.a.b.b.f a;
    private final com.a.b.b.f b;

    l(com.a.b.b.f p2, com.a.b.b.f p3)
    {
        this.a = ((com.a.b.b.f) com.a.b.b.cn.a(p2));
        this.b = ((com.a.b.b.f) com.a.b.b.cn.a(p3));
        return;
    }

    private String a(String p3)
    {
        String v0_1;
        if (p3 != null) {
            v0_1 = this.a.a(this.b, p3);
        } else {
            v0_1 = 0;
        }
        return v0_1;
    }

    private String b(String p3)
    {
        String v0_1;
        if (p3 != null) {
            v0_1 = this.b.a(this.a, p3);
        } else {
            v0_1 = 0;
        }
        return v0_1;
    }

    protected final bridge synthetic Object a(Object p3)
    {
        String v0_1;
        if (((String) p3) != null) {
            v0_1 = this.b.a(this.a, ((String) p3));
        } else {
            v0_1 = 0;
        }
        return v0_1;
    }

    protected final synthetic Object b(Object p3)
    {
        String v0_1;
        if (((String) p3) != null) {
            v0_1 = this.a.a(this.b, ((String) p3));
        } else {
            v0_1 = 0;
        }
        return v0_1;
    }

    public final boolean equals(Object p4)
    {
        int v0 = 0;
        if (((p4 instanceof com.a.b.b.l)) && ((this.a.equals(((com.a.b.b.l) p4).a)) && (this.b.equals(((com.a.b.b.l) p4).b)))) {
            v0 = 1;
        }
        return v0;
    }

    public final int hashCode()
    {
        return (this.a.hashCode() ^ this.b.hashCode());
    }

    public final String toString()
    {
        String v0_2 = String.valueOf(String.valueOf(this.a));
        String v1_2 = String.valueOf(String.valueOf(this.b));
        return new StringBuilder(((v0_2.length() + 14) + v1_2.length())).append(v0_2).append(".converterTo(").append(v1_2).append(")").toString();
    }
}
