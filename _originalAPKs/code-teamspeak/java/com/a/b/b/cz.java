package com.a.b.b;
final class cz implements com.a.b.b.co, java.io.Serializable {
    private static final long b;
    final com.a.b.b.co a;

    cz(com.a.b.b.co p2)
    {
        this.a = ((com.a.b.b.co) com.a.b.b.cn.a(p2));
        return;
    }

    public final boolean a(Object p2)
    {
        int v0_2;
        if (this.a.a(p2)) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    public final boolean equals(Object p3)
    {
        int v0_1;
        if (!(p3 instanceof com.a.b.b.cz)) {
            v0_1 = 0;
        } else {
            v0_1 = this.a.equals(((com.a.b.b.cz) p3).a);
        }
        return v0_1;
    }

    public final int hashCode()
    {
        return (this.a.hashCode() ^ -1);
    }

    public final String toString()
    {
        String v0_3 = String.valueOf(String.valueOf(this.a.toString()));
        return new StringBuilder((v0_3.length() + 16)).append("Predicates.not(").append(v0_3).append(")").toString();
    }
}
