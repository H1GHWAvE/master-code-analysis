package com.a.b.b;
final class bw extends com.a.b.b.bv {
    final synthetic String b;
    final synthetic com.a.b.b.bv c;

    bw(com.a.b.b.bv p2, com.a.b.b.bv p3, String p4)
    {
        this.c = p2;
        this.b = p4;
        this(p3, 0);
        return;
    }

    public final com.a.b.b.bv a()
    {
        throw new UnsupportedOperationException("already specified useForNull");
    }

    final CharSequence a(Object p2)
    {
        CharSequence v0_1;
        if (p2 != null) {
            v0_1 = this.c.a(p2);
        } else {
            v0_1 = this.b;
        }
        return v0_1;
    }

    public final com.a.b.b.bv b(String p3)
    {
        throw new UnsupportedOperationException("already specified useForNull");
    }
}
