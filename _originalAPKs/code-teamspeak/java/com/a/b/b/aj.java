package com.a.b.b;
public final class aj {
    public static final java.nio.charset.Charset a;
    public static final java.nio.charset.Charset b;
    public static final java.nio.charset.Charset c;
    public static final java.nio.charset.Charset d;
    public static final java.nio.charset.Charset e;
    public static final java.nio.charset.Charset f;

    static aj()
    {
        com.a.b.b.aj.a = java.nio.charset.Charset.forName("US-ASCII");
        com.a.b.b.aj.b = java.nio.charset.Charset.forName("ISO-8859-1");
        com.a.b.b.aj.c = java.nio.charset.Charset.forName("UTF-8");
        com.a.b.b.aj.d = java.nio.charset.Charset.forName("UTF-16BE");
        com.a.b.b.aj.e = java.nio.charset.Charset.forName("UTF-16LE");
        com.a.b.b.aj.f = java.nio.charset.Charset.forName("UTF-16");
        return;
    }

    private aj()
    {
        return;
    }
}
