package com.a.b.b;
final class bt implements com.a.b.b.bj, java.io.Serializable {
    private static final long b;
    private final com.a.b.b.dz a;

    private bt(com.a.b.b.dz p2)
    {
        this.a = ((com.a.b.b.dz) com.a.b.b.cn.a(p2));
        return;
    }

    synthetic bt(com.a.b.b.dz p1, byte p2)
    {
        this(p1);
        return;
    }

    public final Object e(Object p2)
    {
        return this.a.a();
    }

    public final boolean equals(Object p3)
    {
        int v0_1;
        if (!(p3 instanceof com.a.b.b.bt)) {
            v0_1 = 0;
        } else {
            v0_1 = this.a.equals(((com.a.b.b.bt) p3).a);
        }
        return v0_1;
    }

    public final int hashCode()
    {
        return this.a.hashCode();
    }

    public final String toString()
    {
        String v0_2 = String.valueOf(String.valueOf(this.a));
        return new StringBuilder((v0_2.length() + 13)).append("forSupplier(").append(v0_2).append(")").toString();
    }
}
