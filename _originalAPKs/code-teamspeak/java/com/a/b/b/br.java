package com.a.b.b;
final enum class br extends java.lang.Enum implements com.a.b.b.bj {
    public static final enum com.a.b.b.br a;
    private static final synthetic com.a.b.b.br[] b;

    static br()
    {
        com.a.b.b.br.a = new com.a.b.b.br("INSTANCE");
        com.a.b.b.br[] v0_3 = new com.a.b.b.br[1];
        v0_3[0] = com.a.b.b.br.a;
        com.a.b.b.br.b = v0_3;
        return;
    }

    private br(String p2)
    {
        this(p2, 0);
        return;
    }

    public static com.a.b.b.br valueOf(String p1)
    {
        return ((com.a.b.b.br) Enum.valueOf(com.a.b.b.br, p1));
    }

    public static com.a.b.b.br[] values()
    {
        return ((com.a.b.b.br[]) com.a.b.b.br.b.clone());
    }

    public final Object e(Object p1)
    {
        return p1;
    }

    public final String toString()
    {
        return "identity";
    }
}
