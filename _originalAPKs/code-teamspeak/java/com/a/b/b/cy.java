package com.a.b.b;
final class cy implements com.a.b.b.co, java.io.Serializable {
    private static final long b;
    private final Object a;

    private cy(Object p1)
    {
        this.a = p1;
        return;
    }

    synthetic cy(Object p1, byte p2)
    {
        this(p1);
        return;
    }

    public final boolean a(Object p2)
    {
        return this.a.equals(p2);
    }

    public final boolean equals(Object p3)
    {
        int v0_1;
        if (!(p3 instanceof com.a.b.b.cy)) {
            v0_1 = 0;
        } else {
            v0_1 = this.a.equals(((com.a.b.b.cy) p3).a);
        }
        return v0_1;
    }

    public final int hashCode()
    {
        return this.a.hashCode();
    }

    public final String toString()
    {
        String v0_2 = String.valueOf(String.valueOf(this.a));
        return new StringBuilder((v0_2.length() + 20)).append("Predicates.equalTo(").append(v0_2).append(")").toString();
    }
}
