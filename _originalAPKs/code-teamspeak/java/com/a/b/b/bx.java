package com.a.b.b;
final class bx extends com.a.b.b.bv {
    final synthetic com.a.b.b.bv b;

    bx(com.a.b.b.bv p2, com.a.b.b.bv p3)
    {
        this.b = p2;
        this(p3, 0);
        return;
    }

    public final Appendable a(Appendable p3, java.util.Iterator p4)
    {
        com.a.b.b.cn.a(p3, "appendable");
        com.a.b.b.cn.a(p4, "parts");
        while (p4.hasNext()) {
            CharSequence v0_3 = p4.next();
            if (v0_3 != null) {
                p3.append(this.b.a(v0_3));
                break;
            }
        }
        while (p4.hasNext()) {
            CharSequence v0_6 = p4.next();
            if (v0_6 != null) {
                p3.append(this.b.a);
                p3.append(this.b.a(v0_6));
            }
        }
        return p3;
    }

    public final com.a.b.b.bv b(String p3)
    {
        throw new UnsupportedOperationException("already specified skipNulls");
    }

    public final com.a.b.b.bz c(String p3)
    {
        throw new UnsupportedOperationException("can\'t use .skipNulls() with maps");
    }
}
