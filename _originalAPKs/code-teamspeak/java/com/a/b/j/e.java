package com.a.b.j;
final class e {
    private long a;
    private double b;

    private e()
    {
        this.a = 0;
        this.b = 0;
        return;
    }

    synthetic e(byte p1)
    {
        return;
    }

    final double a()
    {
        double v0_2;
        if (this.a <= 0) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        com.a.b.b.cn.a(v0_2, "Cannot take mean of 0 values");
        return this.b;
    }

    final void a(double p8)
    {
        com.a.b.b.cn.a(com.a.b.j.f.b(p8));
        this.a = (this.a + 1);
        this.b = (this.b + ((p8 - this.b) / ((double) this.a)));
        return;
    }
}
