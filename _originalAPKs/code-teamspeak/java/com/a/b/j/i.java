package com.a.b.j;
public final class i {
    static final long a = 13043817825332782212;
    static final byte[] b = None;
    static final long[] c = None;
    static final long[] d = None;
    static final long e = 3037000499;
    static final long[] f;
    static final int[] g;
    static final int[] h;

    static i()
    {
        int[] v0_1 = new byte[64];
        v0_1 = {19, 18, 18, 18, 18, 17, 17, 17, 16, 16, 16, 15, 15, 15, 15, 14, 14, 14, 13, 13, 13, 12, 12, 12, 12, 11, 11, 11, 10, 10, 10, 9, 9, 9, 9, 8, 8, 8, 7, 7, 7, 6, 6, 6, 6, 5, 5, 5, 4, 4, 4, 3, 3, 3, 3, 2, 2, 2, 1, 1, 1, 0, 0, 0};
        com.a.b.j.i.b = v0_1;
        int[] v0_2 = new long[19];
        v0_2 = {1, 0, 0, 0, 0, 0, 0, 0, 10, 0, 0, 0, 0, 0, 0, 0, 100, 0, 0};
        com.a.b.j.i.c = v0_2;
        int[] v0_3 = new long[19];
        v0_3 = {3, 0, 0, 0, 0, 0, 0, 0, 31, 0, 0, 0, 0, 0, 0, 0, 60, 1, 0};
        com.a.b.j.i.d = v0_3;
        int[] v0_5 = new long[21];
        v0_5 = {1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0};
        com.a.b.j.i.f = v0_5;
        int[] v0_7 = new int[34];
        v0_7 = {2147483647, 2147483647, 2147483647, 3810779, 121977, 16175, 4337, 1733, 887, 534, 361, 265, 206, 169, 143, 125, 111, 101, 94, 88, 83, 79, 76, 74, 72, 70, 69, 68, 67, 67, 66, 66, 66, 66};
        com.a.b.j.i.g = v0_7;
        int[] v0_9 = new int[31];
        v0_9 = {2147483647, 2147483647, 2147483647, 2642246, 86251, 11724, 3218, 1313, 684, 419, 287, 214, 169, 139, 119, 105, 95, 87, 81, 76, 73, 70, 68, 66, 64, 63, 62, 62, 61, 61, 61};
        com.a.b.j.i.h = v0_9;
        return;
    }

    private i()
    {
        return;
    }

    private static int a(long p4, long p6)
    {
        return ((int) ((((p4 - p6) ^ -1) ^ -1) >> 63));
    }

    public static int a(long p4, java.math.RoundingMode p6)
    {
        int v0_5;
        com.a.b.j.k.a("x", p4);
        switch (com.a.b.j.j.a[p6.ordinal()]) {
            case 1:
                com.a.b.j.k.a(com.a.b.j.i.a(p4));
            case 2:
            case 3:
                v0_5 = (63 - Long.numberOfLeadingZeros(p4));
                break;
            case 4:
            case 5:
                v0_5 = (64 - Long.numberOfLeadingZeros((p4 - 1)));
                break;
            case 6:
            case 7:
            case 8:
                int v0_3 = Long.numberOfLeadingZeros(p4);
                v0_5 = ((63 - v0_3) + com.a.b.j.i.a((-2.734104117489491e-53 >> v0_3), p4));
                break;
            default:
                throw new AssertionError("impossible");
        }
        return v0_5;
    }

    private static long a(int p2)
    {
        double v0_3;
        com.a.b.j.k.b("n", p2);
        if (p2 >= com.a.b.j.i.f.length) {
            v0_3 = nan;
        } else {
            v0_3 = com.a.b.j.i.f[p2];
        }
        return v0_3;
    }

    public static long a(int p14, int p15)
    {
        long v0_2;
        long v4_0 = 1;
        int v3 = 2;
        com.a.b.j.k.b("n", p14);
        com.a.b.j.k.b("k", p15);
        if (p15 > p14) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        int v7_0 = new Object[2];
        v7_0[0] = Integer.valueOf(p15);
        v7_0[1] = Integer.valueOf(p14);
        com.a.b.b.cn.a(v0_2, "k (%s) > n (%s)", v7_0);
        if (p15 > (p14 >> 1)) {
            p15 = (p14 - p15);
        }
        long v0_4;
        switch (p15) {
            case 0:
                v0_4 = 1;
                break;
            case 1:
                v0_4 = ((long) p14);
                break;
            default:
                if (p14 >= com.a.b.j.i.f.length) {
                    if ((p15 < com.a.b.j.i.g.length) && (p14 <= com.a.b.j.i.g[p15])) {
                        if ((p15 >= com.a.b.j.i.h.length) || (p14 > com.a.b.j.i.h[p15])) {
                            int v6_1 = com.a.b.j.i.a(((long) p14), java.math.RoundingMode.CEILING);
                            int v7_1 = 2;
                            int v10 = v6_1;
                            int v11 = (p14 - 1);
                            long v2_4 = ((long) p14);
                            long v0_17 = 1;
                            while (v7_1 <= p15) {
                                long v0_19;
                                long v2_6;
                                long v4_2;
                                long v8_2;
                                if ((v10 + v6_1) >= 63) {
                                    v8_2 = com.a.b.j.i.a(v0_17, v2_4, v4_0);
                                    v4_2 = ((long) v11);
                                    v2_6 = ((long) v7_1);
                                    v0_19 = v6_1;
                                } else {
                                    v2_6 = (v4_0 * ((long) v7_1));
                                    v4_2 = (((long) v11) * v2_4);
                                    v8_2 = v0_17;
                                    v0_19 = (v10 + v6_1);
                                }
                                v10 = v0_19;
                                v11--;
                                v7_1++;
                                v0_17 = v8_2;
                                v4_0 = v2_6;
                                v2_4 = v4_2;
                            }
                            v0_4 = com.a.b.j.i.a(v0_17, v2_4, v4_0);
                        } else {
                            long v2_9 = (p14 - 1);
                            v0_4 = ((long) p14);
                            while (v3 <= p15) {
                                v0_4 = ((v0_4 * ((long) v2_9)) / ((long) v3));
                                v2_9--;
                                v3++;
                            }
                        }
                    } else {
                        v0_4 = nan;
                    }
                } else {
                    v0_4 = (com.a.b.j.i.f[p14] / (com.a.b.j.i.f[p15] * com.a.b.j.i.f[(p14 - p15)]));
                }
        }
        return v0_4;
    }

    private static long a(long p8, int p10)
    {
        long v2 = 1;
        com.a.b.j.k.b("exponent", p10);
        if ((-2 > p8) || (p8 > 2)) {
            long v6_1 = 1;
            long v4_5 = p8;
            while(true) {
                switch (p10) {
                    case 0:
                        v2 = v6_1;
                        break;
                    case 1:
                        v2 = (v6_1 * v4_5);
                        break;
                    default:
                        long v0_2;
                        if ((p10 & 1) != 0) {
                            v0_2 = v4_5;
                        } else {
                            v0_2 = 1;
                        }
                        v6_1 *= v0_2;
                        v4_5 *= v4_5;
                        p10 >>= 1;
                }
            }
        } else {
            switch (((int) p8)) {
                case -2:
                    if (p10 >= 64) {
                        v2 = 0;
                    } else {
                        if ((p10 & 1) != 0) {
                            v2 = (- (1 << p10));
                        } else {
                            v2 = (1 << p10);
                        }
                    }
                    break;
                case -1:
                    if ((p10 & 1) != 0) {
                        v2 = -1;
                    }
                    break;
                case 0:
                    if (p10 != 0) {
                        v2 = 0;
                    }
                case 1:
                    break;
                case 2:
                    if (p10 >= 64) {
                        v2 = 0;
                    } else {
                        v2 = (1 << p10);
                    }
                    break;
                default:
                    throw new AssertionError();
            }
        }
        return v2;
    }

    private static long a(long p8, long p10, long p12)
    {
        long v0_11;
        if (p8 != 1) {
            long v0_8;
            com.a.b.j.k.b("a", p8);
            com.a.b.j.k.b("b", p12);
            if (p8 != 0) {
                if (p12 != 0) {
                    int v4 = Long.numberOfTrailingZeros(p8);
                    long v2_1 = (p8 >> v4);
                    int v5 = Long.numberOfTrailingZeros(p12);
                    long v0_6 = (p12 >> v5);
                    while (v2_1 != v0_6) {
                        long v2_2 = (v2_1 - v0_6);
                        int v6_3 = ((v2_2 >> 63) & v2_2);
                        long v2_4 = ((v2_2 - v6_3) - v6_3);
                        v0_6 += v6_3;
                        v2_1 = (v2_4 >> Long.numberOfTrailingZeros(v2_4));
                    }
                    v0_8 = (v2_1 << Math.min(v4, v5));
                } else {
                    v0_8 = p8;
                }
            } else {
                v0_8 = p12;
            }
            v0_11 = ((p10 / (p12 / v0_8)) * (p8 / v0_8));
        } else {
            v0_11 = (p10 / p12);
        }
        return v0_11;
    }

    public static boolean a(long p8)
    {
        int v2_1;
        int v0_0 = 1;
        if (p8 <= 0) {
            v2_1 = 0;
        } else {
            v2_1 = 1;
        }
        if (((p8 - 1) & p8) != 0) {
            v0_0 = 0;
        }
        return (v0_0 & v2_1);
    }

    private static int b(long p4)
    {
        int v0_1 = com.a.b.j.i.b[Long.numberOfLeadingZeros(p4)];
        return (v0_1 - com.a.b.j.i.a(p4, com.a.b.j.i.c[v0_1]));
    }

    private static int b(long p6, int p8)
    {
        if (((long) p8) > 0) {
            long v0_1 = (p6 % ((long) p8));
            if (v0_1 < 0) {
                v0_1 += ((long) p8);
            }
            return ((int) v0_1);
        } else {
            throw new ArithmeticException("Modulus must be positive");
        }
    }

    public static int b(long p6, java.math.RoundingMode p8)
    {
        int v0_8;
        com.a.b.j.k.a("x", p6);
        int v0_2 = com.a.b.j.i.b[Long.numberOfLeadingZeros(p6)];
        int v1_3 = (v0_2 - com.a.b.j.i.a(p6, com.a.b.j.i.c[v0_2]));
        long v2_1 = com.a.b.j.i.c[v1_3];
        switch (com.a.b.j.j.a[p8.ordinal()]) {
            case 1:
                int v0_11;
                if (p6 != v2_1) {
                    v0_11 = 0;
                } else {
                    v0_11 = 1;
                }
                com.a.b.j.k.a(v0_11);
            case 2:
            case 3:
                v0_8 = v1_3;
                break;
            case 4:
            case 5:
                v0_8 = (com.a.b.j.i.a(v2_1, p6) + v1_3);
                break;
            case 6:
            case 7:
            case 8:
                v0_8 = (com.a.b.j.i.a(com.a.b.j.i.d[v1_3], p6) + v1_3);
                break;
            default:
                throw new AssertionError();
        }
        return v0_8;
    }

    private static long b(long p4, long p6)
    {
        if (p6 > 0) {
            long v0_1 = (p4 % p6);
            if (v0_1 < 0) {
                v0_1 += p6;
            }
            return v0_1;
        } else {
            throw new ArithmeticException("Modulus must be positive");
        }
    }

    private static long c(long p10, int p12)
    {
        int v7;
        long v0_0 = 1;
        long v2_0 = 1;
        com.a.b.j.k.b("exponent", p12);
        if (p10 < -2) {
            v7 = 0;
        } else {
            v7 = 1;
        }
        int v6_4;
        if (p10 > 2) {
            v6_4 = 0;
        } else {
            v6_4 = 1;
        }
        long v0_1;
        if ((v6_4 & v7) == 0) {
            while(true) {
                switch (p12) {
                    case 0:
                        v0_1 = v2_0;
                        break;
                    case 1:
                        v0_1 = com.a.b.j.i.f(v2_0, p10);
                        break;
                    default:
                        long v4_2;
                        if ((p12 & 1) == 0) {
                            v4_2 = v2_0;
                        } else {
                            v4_2 = com.a.b.j.i.f(v2_0, p10);
                        }
                        p12 >>= 1;
                        if (p12 <= 0) {
                            v2_0 = v4_2;
                        } else {
                            long v2_4;
                            if (p10 > 1.500477613e-314) {
                                v2_4 = 0;
                            } else {
                                v2_4 = 1;
                            }
                            com.a.b.j.k.c(v2_4);
                            p10 *= p10;
                            v2_0 = v4_2;
                        }
                }
            }
        } else {
            switch (((int) p10)) {
                case -2:
                    if (p12 >= 64) {
                        v0_0 = 0;
                    }
                    com.a.b.j.k.c(v0_0);
                    if ((p12 & 1) != 0) {
                        v0_1 = (-1 << p12);
                    } else {
                        v0_1 = (1 << p12);
                    }
                    break;
                case -1:
                    if ((p12 & 1) != 0) {
                        v0_1 = -1;
                    } else {
                        v0_1 = 1;
                    }
                    break;
                case 0:
                    if (p12 != 0) {
                        v0_1 = 0;
                    } else {
                        v0_1 = 1;
                    }
                    break;
                case 1:
                    v0_1 = 1;
                    break;
                case 2:
                    if (p12 >= 63) {
                        v0_0 = 0;
                    }
                    com.a.b.j.k.c(v0_0);
                    v0_1 = (1 << p12);
                    break;
                default:
                    throw new AssertionError();
            }
        }
        return v0_1;
    }

    private static long c(long p8, long p10)
    {
        com.a.b.j.k.b("a", p8);
        com.a.b.j.k.b("b", p10);
        if (p8 != 0) {
            if (p10 != 0) {
                int v4 = Long.numberOfTrailingZeros(p8);
                long v2_1 = (p8 >> v4);
                int v5 = Long.numberOfTrailingZeros(p10);
                int v0_4 = (p10 >> v5);
                while (v2_1 != v0_4) {
                    long v2_2 = (v2_1 - v0_4);
                    int v6_3 = ((v2_2 >> 63) & v2_2);
                    long v2_4 = ((v2_2 - v6_3) - v6_3);
                    v0_4 += v6_3;
                    v2_1 = (v2_4 >> Long.numberOfTrailingZeros(v2_4));
                }
                p10 = (v2_1 << Math.min(v4, v5));
            } else {
                p10 = p8;
            }
        }
        return p10;
    }

    public static long c(long p10, java.math.RoundingMode p12)
    {
        long v2_4;
        int v0_0 = 1;
        com.a.b.j.k.b("x", p10);
        if (((long) ((int) p10)) != p10) {
            v2_4 = 0;
        } else {
            v2_4 = 1;
        }
        int v0_3;
        if (v2_4 == 0) {
            long v2_7 = ((long) Math.sqrt(((double) p10)));
            long v4_0 = (v2_7 * v2_7);
            switch (com.a.b.j.j.a[p12.ordinal()]) {
                case 1:
                    if (v4_0 != p10) {
                        v0_0 = 0;
                    }
                    com.a.b.j.k.a(v0_0);
                    v0_3 = v2_7;
                    break;
                case 2:
                case 3:
                    if (p10 >= v4_0) {
                        v0_3 = v2_7;
                    } else {
                        v0_3 = (v2_7 - 1);
                    }
                    break;
                case 4:
                case 5:
                    if (p10 <= v4_0) {
                        v0_3 = v2_7;
                    } else {
                        v0_3 = (v2_7 + 1);
                    }
                    break;
                case 6:
                case 7:
                case 8:
                    if (p10 >= v4_0) {
                        v0_0 = 0;
                    }
                    int v0_2 = (v2_7 - ((long) v0_0));
                    v0_3 = (v0_2 + ((long) com.a.b.j.i.a(((v0_2 * v0_2) + v0_2), p10)));
                    break;
                default:
                    throw new AssertionError();
            }
        } else {
            v0_3 = ((long) com.a.b.j.g.b(((int) p10), p12));
        }
        return v0_3;
    }

    private static boolean c(long p2)
    {
        int v0_3;
        if (((long) ((int) p2)) != p2) {
            v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        return v0_3;
    }

    private static long d(long p10, long p12)
    {
        int v2_2;
        int v0_0 = 1;
        long v4 = (p10 + p12);
        if ((p10 ^ p12) >= 0) {
            v2_2 = 0;
        } else {
            v2_2 = 1;
        }
        if ((p10 ^ v4) < 0) {
            v0_0 = 0;
        }
        com.a.b.j.k.c((v0_0 | v2_2));
        return v4;
    }

    public static long d(long p10, java.math.RoundingMode p12)
    {
        int v0_12;
        com.a.b.b.cn.a(p12);
        long v2 = (p10 / 64);
        int v0_3 = (p10 - (64 * v2));
        if (v0_3 != 0) {
            int v0_7;
            int v5 = (((int) ((64 ^ p10) >> 63)) | 1);
            switch (com.a.b.j.j.a[p12.ordinal()]) {
                case 1:
                    int v0_11;
                    if (v0_3 != 0) {
                        v0_11 = 0;
                    } else {
                        v0_11 = 1;
                    }
                    com.a.b.j.k.a(v0_11);
                case 2:
                    v0_7 = 0;
                    break;
                case 3:
                    if (v5 >= 0) {
                        v0_7 = 0;
                    } else {
                        v0_7 = 1;
                    }
                    break;
                case 4:
                    v0_7 = 1;
                    break;
                case 5:
                    if (v5 <= 0) {
                        v0_7 = 0;
                    } else {
                        v0_7 = 1;
                    }
                    break;
                case 6:
                case 7:
                case 8:
                    int v0_4 = Math.abs(v0_3);
                    int v0_5 = (v0_4 - (Math.abs(64) - v0_4));
                    if (v0_5 != 0) {
                        if (v0_5 <= 0) {
                            v0_7 = 0;
                        } else {
                            v0_7 = 1;
                        }
                    } else {
                        int v0_9;
                        if (p12 != java.math.RoundingMode.HALF_UP) {
                            v0_9 = 0;
                        } else {
                            v0_9 = 1;
                        }
                        int v4_9;
                        if (p12 != java.math.RoundingMode.HALF_EVEN) {
                            v4_9 = 0;
                        } else {
                            v4_9 = 1;
                        }
                        int v1_4;
                        if ((1 & v2) == 0) {
                            v1_4 = 0;
                        } else {
                            v1_4 = 1;
                        }
                        v0_7 = (v0_9 | (v1_4 & v4_9));
                    }
                    break;
                default:
                    throw new AssertionError();
            }
            if (v0_7 == 0) {
                v0_12 = v2;
            } else {
                v0_12 = (((long) v5) + v2);
            }
        } else {
            v0_12 = v2;
        }
        return v0_12;
    }

    private static long e(long p10, long p12)
    {
        int v2_2;
        int v0_0 = 1;
        long v4 = (p10 - p12);
        if ((p10 ^ p12) < 0) {
            v2_2 = 0;
        } else {
            v2_2 = 1;
        }
        if ((p10 ^ v4) < 0) {
            v0_0 = 0;
        }
        com.a.b.j.k.c((v0_0 | v2_2));
        return v4;
    }

    private static long f(long p10, long p12)
    {
        long v0_11;
        int v2 = 0;
        long v0_3 = (((Long.numberOfLeadingZeros(p10) + Long.numberOfLeadingZeros((p10 ^ -1))) + Long.numberOfLeadingZeros(p12)) + Long.numberOfLeadingZeros((p12 ^ -1)));
        if (v0_3 <= 65) {
            long v0_4;
            if (v0_3 < 64) {
                v0_4 = 0;
            } else {
                v0_4 = 1;
            }
            int v3_5;
            com.a.b.j.k.c(v0_4);
            if (p10 < 0) {
                v3_5 = 0;
            } else {
                v3_5 = 1;
            }
            long v0_7;
            if (p12 == -0.0) {
                v0_7 = 0;
            } else {
                v0_7 = 1;
            }
            com.a.b.j.k.c((v0_7 | v3_5));
            long v4_3 = (p10 * p12);
            if ((p10 == 0) || ((v4_3 / p10) == p12)) {
                v2 = 1;
            }
            com.a.b.j.k.c(v2);
            v0_11 = v4_3;
        } else {
            v0_11 = (p10 * p12);
        }
        return v0_11;
    }

    private static long g(long p6, long p8)
    {
        return ((p6 & p8) + ((p6 ^ p8) >> 1));
    }
}
