package javax.annotation.meta;
public interface annotation TypeQualifier implements java.lang.annotation.Annotation {

    public abstract Class applicableTo();
}
