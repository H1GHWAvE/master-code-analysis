package android.support.v4.i;
public final class a {

    public a()
    {
        return;
    }

    private static varargs android.os.AsyncTask a(android.os.AsyncTask p2, Object[] p3)
    {
        if (android.os.Build$VERSION.SDK_INT < 11) {
            p2.execute(p3);
        } else {
            p2.executeOnExecutor(android.os.AsyncTask.THREAD_POOL_EXECUTOR, p3);
        }
        return p2;
    }
}
