package android.support.v4.e.a;
 class r extends android.graphics.drawable.Drawable implements android.graphics.drawable.Drawable$Callback, android.support.v4.e.a.q {
    static final android.graphics.PorterDuff$Mode a;
    android.graphics.drawable.Drawable b;
    private android.content.res.ColorStateList c;
    private android.graphics.PorterDuff$Mode d;
    private int e;
    private android.graphics.PorterDuff$Mode f;
    private boolean g;

    static r()
    {
        android.support.v4.e.a.r.a = android.graphics.PorterDuff$Mode.SRC_IN;
        return;
    }

    r(android.graphics.drawable.Drawable p2)
    {
        this.d = android.support.v4.e.a.r.a;
        this.a(p2);
        return;
    }

    private boolean a(int[] p6)
    {
        int v0 = 1;
        if ((this.c == null) || (this.d == null)) {
            this.g = 0;
            this.clearColorFilter();
            v0 = 0;
        } else {
            int v2_3 = this.c.getColorForState(p6, this.c.getDefaultColor());
            android.graphics.PorterDuff$Mode v3_2 = this.d;
            if ((this.g) && ((v2_3 == this.e) && (v3_2 == this.f))) {
            } else {
                this.setColorFilter(v2_3, v3_2);
                this.e = v2_3;
                this.f = v3_2;
                this.g = 1;
            }
        }
        return v0;
    }

    public final android.graphics.drawable.Drawable a()
    {
        return this.b;
    }

    public final void a(android.graphics.drawable.Drawable p3)
    {
        if (this.b != null) {
            this.b.setCallback(0);
        }
        this.b = p3;
        if (p3 != null) {
            p3.setCallback(this);
        }
        this.invalidateSelf();
        return;
    }

    public void draw(android.graphics.Canvas p2)
    {
        this.b.draw(p2);
        return;
    }

    public int getChangingConfigurations()
    {
        return this.b.getChangingConfigurations();
    }

    public android.graphics.drawable.Drawable getCurrent()
    {
        return this.b.getCurrent();
    }

    public int getIntrinsicHeight()
    {
        return this.b.getIntrinsicHeight();
    }

    public int getIntrinsicWidth()
    {
        return this.b.getIntrinsicWidth();
    }

    public int getMinimumHeight()
    {
        return this.b.getMinimumHeight();
    }

    public int getMinimumWidth()
    {
        return this.b.getMinimumWidth();
    }

    public int getOpacity()
    {
        return this.b.getOpacity();
    }

    public boolean getPadding(android.graphics.Rect p2)
    {
        return this.b.getPadding(p2);
    }

    public int[] getState()
    {
        return this.b.getState();
    }

    public android.graphics.Region getTransparentRegion()
    {
        return this.b.getTransparentRegion();
    }

    public void invalidateDrawable(android.graphics.drawable.Drawable p1)
    {
        this.invalidateSelf();
        return;
    }

    public boolean isStateful()
    {
        if (((this.c == null) || (!this.c.isStateful())) && (!this.b.isStateful())) {
            int v0_5 = 0;
        } else {
            v0_5 = 1;
        }
        return v0_5;
    }

    public android.graphics.drawable.Drawable mutate()
    {
        android.graphics.drawable.Drawable v0 = this.b;
        android.graphics.drawable.Drawable v1 = v0.mutate();
        if (v1 != v0) {
            this.a(v1);
        }
        return this;
    }

    protected void onBoundsChange(android.graphics.Rect p2)
    {
        this.b.setBounds(p2);
        return;
    }

    protected boolean onLevelChange(int p2)
    {
        return this.b.setLevel(p2);
    }

    public void scheduleDrawable(android.graphics.drawable.Drawable p2, Runnable p3, long p4)
    {
        this.scheduleSelf(p3, p4);
        return;
    }

    public void setAlpha(int p2)
    {
        this.b.setAlpha(p2);
        return;
    }

    public void setChangingConfigurations(int p2)
    {
        this.b.setChangingConfigurations(p2);
        return;
    }

    public void setColorFilter(android.graphics.ColorFilter p2)
    {
        this.b.setColorFilter(p2);
        return;
    }

    public void setDither(boolean p2)
    {
        this.b.setDither(p2);
        return;
    }

    public void setFilterBitmap(boolean p2)
    {
        this.b.setFilterBitmap(p2);
        return;
    }

    public boolean setState(int[] p3)
    {
        int v0_2;
        int v0_1 = this.b.setState(p3);
        if ((!this.a(p3)) && (v0_1 == 0)) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    public void setTint(int p2)
    {
        this.setTintList(android.content.res.ColorStateList.valueOf(p2));
        return;
    }

    public void setTintList(android.content.res.ColorStateList p2)
    {
        this.c = p2;
        this.a(this.getState());
        return;
    }

    public void setTintMode(android.graphics.PorterDuff$Mode p2)
    {
        this.d = p2;
        this.a(this.getState());
        return;
    }

    public boolean setVisible(boolean p2, boolean p3)
    {
        if ((!super.setVisible(p2, p3)) && (!this.b.setVisible(p2, p3))) {
            int v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        return v0_3;
    }

    public void unscheduleDrawable(android.graphics.drawable.Drawable p1, Runnable p2)
    {
        this.unscheduleSelf(p2);
        return;
    }
}
