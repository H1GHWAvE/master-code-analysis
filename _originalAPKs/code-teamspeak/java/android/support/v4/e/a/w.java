package android.support.v4.e.a;
final class w extends android.support.v4.e.a.v {

    protected w(android.content.res.Resources p1, android.graphics.Bitmap p2)
    {
        this(p1, p2);
        return;
    }

    final void a(int p7, int p8, int p9, android.graphics.Rect p10, android.graphics.Rect p11)
    {
        android.view.Gravity.apply(p7, p8, p9, p10, p11, 0);
        return;
    }

    public final void a(boolean p2)
    {
        if (this.a != null) {
            this.a.setHasMipMap(p2);
            this.invalidateSelf();
        }
        return;
    }

    public final boolean a()
    {
        if ((this.a == null) || (!this.a.hasMipMap())) {
            int v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        return v0_3;
    }

    public final void getOutline(android.graphics.Outline p3)
    {
        this.b();
        p3.setRoundRect(this.c, this.b);
        return;
    }
}
