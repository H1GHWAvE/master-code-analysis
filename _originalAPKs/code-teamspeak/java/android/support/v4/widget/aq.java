package android.support.v4.widget;
final class aq {

    aq()
    {
        return;
    }

    private static Object a(android.content.Context p1)
    {
        return new android.widget.EdgeEffect(p1);
    }

    private static void a(Object p0, int p1, int p2)
    {
        ((android.widget.EdgeEffect) p0).setSize(p1, p2);
        return;
    }

    private static boolean a(Object p1)
    {
        return ((android.widget.EdgeEffect) p1).isFinished();
    }

    public static boolean a(Object p1, float p2)
    {
        ((android.widget.EdgeEffect) p1).onPull(p2);
        return 1;
    }

    private static boolean a(Object p1, int p2)
    {
        ((android.widget.EdgeEffect) p1).onAbsorb(p2);
        return 1;
    }

    private static boolean a(Object p1, android.graphics.Canvas p2)
    {
        return ((android.widget.EdgeEffect) p1).draw(p2);
    }

    private static void b(Object p0)
    {
        ((android.widget.EdgeEffect) p0).finish();
        return;
    }

    private static boolean c(Object p1)
    {
        ((android.widget.EdgeEffect) p1).onRelease();
        return ((android.widget.EdgeEffect) p1).isFinished();
    }
}
