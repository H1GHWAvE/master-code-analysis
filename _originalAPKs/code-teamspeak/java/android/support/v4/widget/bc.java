package android.support.v4.widget;
final class bc extends android.view.animation.Animation {
    final synthetic android.support.v4.widget.bg a;
    final synthetic android.support.v4.widget.bb b;

    bc(android.support.v4.widget.bb p1, android.support.v4.widget.bg p2)
    {
        this.b = p1;
        this.a = p2;
        return;
    }

    public final void applyTransformation(float p10, android.view.animation.Transformation p11)
    {
        if (!this.b.d) {
            float v0_3 = android.support.v4.widget.bb.a(this.a);
            android.support.v4.widget.bb v1_1 = this.a.l;
            float v2_1 = this.a.k;
            int v3_1 = this.a.m;
            android.support.v4.widget.bb.b(p10, this.a);
            if (p10 <= 1056964608) {
                this.a.a((v2_1 + (android.support.v4.widget.bb.a().getInterpolation((p10 / 1056964608)) * (1061997773 - v0_3))));
            }
            if (p10 > 1056964608) {
                this.a.b((((1061997773 - v0_3) * android.support.v4.widget.bb.a().getInterpolation(((p10 - 1056964608) / 1056964608))) + v1_1));
            }
            this.a.c(((1048576000 * p10) + v3_1));
            this.b.c(((1129840640 * p10) + (1149698048 * (android.support.v4.widget.bb.a(this.b) / 1084227584))));
        } else {
            android.support.v4.widget.bb.a(p10, this.a);
        }
        return;
    }
}
