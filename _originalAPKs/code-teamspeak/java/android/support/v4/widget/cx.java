package android.support.v4.widget;
public final class cx extends android.support.v4.widget.bz {
    protected int[] l;
    protected int[] m;
    String[] n;
    private int o;
    private android.support.v4.widget.cy p;
    private android.support.v4.widget.cz q;

    private cx(android.content.Context p2, int p3, android.database.Cursor p4, String[] p5, int[] p6)
    {
        this(p2, p3, p4);
        this.o = -1;
        this.m = p6;
        this.n = p5;
        this.a(p5);
        return;
    }

    private cx(android.content.Context p2, int p3, android.database.Cursor p4, String[] p5, int[] p6, int p7)
    {
        this(p2, p3, p4, p7);
        this.o = -1;
        this.m = p6;
        this.n = p5;
        this.a(p5);
        return;
    }

    private void a(int p1)
    {
        this.o = p1;
        return;
    }

    private void a(android.database.Cursor p2, String[] p3, int[] p4)
    {
        this.n = p3;
        this.m = p4;
        super.a(p2);
        this.a(this.n);
        return;
    }

    private void a(android.support.v4.widget.cy p1)
    {
        this.p = p1;
        return;
    }

    private void a(android.support.v4.widget.cz p1)
    {
        this.q = p1;
        return;
    }

    private static void a(android.widget.ImageView p1, String p2)
    {
        try {
            p1.setImageResource(Integer.parseInt(p2));
        } catch (android.net.Uri v0) {
            p1.setImageURI(android.net.Uri.parse(p2));
        }
        return;
    }

    private static void a(android.widget.TextView p0, String p1)
    {
        p0.setText(p1);
        return;
    }

    private void a(String[] p6)
    {
        if (this.c == null) {
            this.l = 0;
        } else {
            int v1 = p6.length;
            if ((this.l == null) || (this.l.length != v1)) {
                int v0_5 = new int[v1];
                this.l = v0_5;
            }
            int v0_6 = 0;
            while (v0_6 < v1) {
                this.l[v0_6] = this.c.getColumnIndexOrThrow(p6[v0_6]);
                v0_6++;
            }
        }
        return;
    }

    private android.support.v4.widget.cz c()
    {
        return this.q;
    }

    private int d()
    {
        return this.o;
    }

    private android.support.v4.widget.cy e()
    {
        return this.p;
    }

    public final void a(android.view.View p10, android.database.Cursor p11)
    {
        android.support.v4.widget.cz v4 = this.q;
        int v5 = this.m.length;
        int[] v6 = this.l;
        int[] v7 = this.m;
        int v3 = 0;
        while (v3 < v5) {
            String v0_2 = p10.findViewById(v7[v3]);
            if (v0_2 != null) {
                android.net.Uri v1_0;
                if (v4 == null) {
                    v1_0 = 0;
                } else {
                    v1_0 = v4.a();
                }
                if (v1_0 == null) {
                    android.net.Uri v1_2 = p11.getString(v6[v3]);
                    if (v1_2 == null) {
                        v1_2 = "";
                    }
                    if (!(v0_2 instanceof android.widget.TextView)) {
                        if (!(v0_2 instanceof android.widget.ImageView)) {
                            throw new IllegalStateException(new StringBuilder().append(v0_2.getClass().getName()).append(" is not a  view that can be bounds by this SimpleCursorAdapter").toString());
                        } else {
                            String v0_8 = ((android.widget.ImageView) v0_2);
                            try {
                                v0_8.setImageResource(Integer.parseInt(v1_2));
                            } catch (NumberFormatException v8) {
                                v0_8.setImageURI(android.net.Uri.parse(v1_2));
                            }
                        }
                    } else {
                        ((android.widget.TextView) v0_2).setText(v1_2);
                    }
                }
            }
            v3++;
        }
        return;
    }

    public final android.database.Cursor b(android.database.Cursor p3)
    {
        android.database.Cursor v0 = super.b(p3);
        this.a(this.n);
        return v0;
    }

    public final CharSequence c(android.database.Cursor p2)
    {
        CharSequence v0_2;
        if (this.p == null) {
            if (this.o < 0) {
                v0_2 = super.c(p2);
            } else {
                v0_2 = p2.getString(this.o);
            }
        } else {
            v0_2 = this.p.a();
        }
        return v0_2;
    }
}
