package android.support.v4.widget;
final class dc implements java.lang.Runnable {
    final android.view.View a;
    final synthetic android.support.v4.widget.SlidingPaneLayout b;

    dc(android.support.v4.widget.SlidingPaneLayout p1, android.view.View p2)
    {
        this.b = p1;
        this.a = p2;
        return;
    }

    public final void run()
    {
        if (this.a.getParent() == this.b) {
            android.support.v4.view.cx.a(this.a, 0, 0);
            android.support.v4.widget.SlidingPaneLayout.a(this.b, this.a);
        }
        android.support.v4.widget.SlidingPaneLayout.g(this.b).remove(this);
        return;
    }
}
