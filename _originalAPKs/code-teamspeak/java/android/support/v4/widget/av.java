package android.support.v4.widget;
public final class av {
    static final android.support.v4.widget.ay a;

    static av()
    {
        if (android.os.Build$VERSION.SDK_INT < 19) {
            android.support.v4.widget.av.a = new android.support.v4.widget.aw();
        } else {
            android.support.v4.widget.av.a = new android.support.v4.widget.ax();
        }
        return;
    }

    private av()
    {
        return;
    }

    private static android.view.View$OnTouchListener a(Object p1, android.view.View p2)
    {
        return android.support.v4.widget.av.a.a(p1, p2);
    }
}
