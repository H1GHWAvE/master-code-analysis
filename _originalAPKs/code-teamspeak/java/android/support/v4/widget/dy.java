package android.support.v4.widget;
public final class dy {
    static final android.support.v4.widget.ec a;

    static dy()
    {
        android.support.v4.widget.dz v0_0 = android.os.Build$VERSION.SDK_INT;
        if (v0_0 < 18) {
            if (v0_0 < 17) {
                android.support.v4.widget.dy.a = new android.support.v4.widget.dz();
            } else {
                android.support.v4.widget.dy.a = new android.support.v4.widget.ea();
            }
        } else {
            android.support.v4.widget.dy.a = new android.support.v4.widget.eb();
        }
        return;
    }

    private dy()
    {
        return;
    }

    private static void a(android.widget.TextView p6, int p7, int p8, int p9, int p10)
    {
        android.support.v4.widget.dy.a.a(p6, p7, p8, p9, p10);
        return;
    }

    public static void a(android.widget.TextView p1, android.graphics.drawable.Drawable p2)
    {
        android.support.v4.widget.dy.a.a(p1, p2);
        return;
    }

    private static void a(android.widget.TextView p6, android.graphics.drawable.Drawable p7, android.graphics.drawable.Drawable p8, android.graphics.drawable.Drawable p9, android.graphics.drawable.Drawable p10)
    {
        android.support.v4.widget.dy.a.a(p6, p7, p8, p9, p10);
        return;
    }
}
