package android.support.v4.widget;
 class i implements android.support.v4.widget.j {

    i()
    {
        return;
    }

    public android.graphics.drawable.Drawable a(android.widget.CompoundButton p2)
    {
        return android.support.v4.widget.m.a(p2);
    }

    public void a(android.widget.CompoundButton p2, android.content.res.ColorStateList p3)
    {
        if ((p2 instanceof android.support.v4.widget.ef)) {
            ((android.support.v4.widget.ef) p2).setSupportButtonTintList(p3);
        }
        return;
    }

    public void a(android.widget.CompoundButton p2, android.graphics.PorterDuff$Mode p3)
    {
        if ((p2 instanceof android.support.v4.widget.ef)) {
            ((android.support.v4.widget.ef) p2).setSupportButtonTintMode(p3);
        }
        return;
    }

    public android.content.res.ColorStateList b(android.widget.CompoundButton p2)
    {
        int v0_1;
        if (!(p2 instanceof android.support.v4.widget.ef)) {
            v0_1 = 0;
        } else {
            v0_1 = ((android.support.v4.widget.ef) p2).getSupportButtonTintList();
        }
        return v0_1;
    }

    public android.graphics.PorterDuff$Mode c(android.widget.CompoundButton p2)
    {
        int v0_1;
        if (!(p2 instanceof android.support.v4.widget.ef)) {
            v0_1 = 0;
        } else {
            v0_1 = ((android.support.v4.widget.ef) p2).getSupportButtonTintMode();
        }
        return v0_1;
    }
}
