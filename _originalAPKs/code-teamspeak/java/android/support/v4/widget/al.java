package android.support.v4.widget;
public final class al {
    private static final android.support.v4.widget.ao b;
    private Object a;

    static al()
    {
        if (android.os.Build$VERSION.SDK_INT < 21) {
            if (android.os.Build$VERSION.SDK_INT < 14) {
                android.support.v4.widget.al.b = new android.support.v4.widget.am();
            } else {
                android.support.v4.widget.al.b = new android.support.v4.widget.an();
            }
        } else {
            android.support.v4.widget.al.b = new android.support.v4.widget.ap();
        }
        return;
    }

    public al(android.content.Context p2)
    {
        this.a = android.support.v4.widget.al.b.a(p2);
        return;
    }

    public final void a(int p3, int p4)
    {
        android.support.v4.widget.al.b.a(this.a, p3, p4);
        return;
    }

    public final boolean a()
    {
        return android.support.v4.widget.al.b.a(this.a);
    }

    public final boolean a(float p3)
    {
        return android.support.v4.widget.al.b.a(this.a, p3);
    }

    public final boolean a(float p3, float p4)
    {
        return android.support.v4.widget.al.b.a(this.a, p3, p4);
    }

    public final boolean a(int p3)
    {
        return android.support.v4.widget.al.b.a(this.a, p3);
    }

    public final boolean a(android.graphics.Canvas p3)
    {
        return android.support.v4.widget.al.b.a(this.a, p3);
    }

    public final void b()
    {
        android.support.v4.widget.al.b.b(this.a);
        return;
    }

    public final boolean c()
    {
        return android.support.v4.widget.al.b.c(this.a);
    }
}
