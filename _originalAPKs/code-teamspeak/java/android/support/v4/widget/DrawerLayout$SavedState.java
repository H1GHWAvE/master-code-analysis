package android.support.v4.widget;
public class DrawerLayout$SavedState extends android.view.View$BaseSavedState {
    public static final android.os.Parcelable$Creator CREATOR;
    int a;
    int b;
    int c;

    static DrawerLayout$SavedState()
    {
        android.support.v4.widget.DrawerLayout$SavedState.CREATOR = new android.support.v4.widget.ae();
        return;
    }

    public DrawerLayout$SavedState(android.os.Parcel p2)
    {
        this(p2);
        this.a = 0;
        this.b = 0;
        this.c = 0;
        this.a = p2.readInt();
        return;
    }

    public DrawerLayout$SavedState(android.os.Parcelable p2)
    {
        this(p2);
        this.a = 0;
        this.b = 0;
        this.c = 0;
        return;
    }

    public void writeToParcel(android.os.Parcel p2, int p3)
    {
        super.writeToParcel(p2, p3);
        p2.writeInt(this.a);
        return;
    }
}
