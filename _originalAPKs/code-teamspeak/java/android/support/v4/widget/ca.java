package android.support.v4.widget;
public final class ca {
    static final int c = 16;
    private static final String d = "ScrollerCompat";
    Object a;
    android.support.v4.widget.cb b;

    private ca(int p2, android.content.Context p3, android.view.animation.Interpolator p4)
    {
        if (p2 < 14) {
            if (p2 < 9) {
                this.b = new android.support.v4.widget.cc();
            } else {
                this.b = new android.support.v4.widget.cd();
            }
        } else {
            this.b = new android.support.v4.widget.ce();
        }
        this.a = this.b.a(p3, p4);
        return;
    }

    ca(android.content.Context p2, android.view.animation.Interpolator p3)
    {
        this(android.os.Build$VERSION.SDK_INT, p2, p3);
        return;
    }

    private static android.support.v4.widget.ca a(android.content.Context p1)
    {
        return android.support.v4.widget.ca.a(p1, 0);
    }

    public static android.support.v4.widget.ca a(android.content.Context p1, android.view.animation.Interpolator p2)
    {
        return new android.support.v4.widget.ca(p1, p2);
    }

    private void a(int p3, int p4, int p5)
    {
        this.b.a(this.a, p3, p4, p5);
        return;
    }

    private void a(int p8, int p9, int p10, int p11, int p12)
    {
        this.b.a(this.a, p8, p9, p10, p11, p12);
        return;
    }

    private void b(int p3, int p4, int p5)
    {
        this.b.b(this.a, p3, p4, p5);
        return;
    }

    private void b(int p8, int p9, int p10, int p11, int p12)
    {
        this.b.b(this.a, p8, p9, p10, p11, p12);
        return;
    }

    private void c(int p3, int p4, int p5)
    {
        this.b.c(this.a, p3, p4, p5);
        return;
    }

    private int h()
    {
        return this.b.h(this.a);
    }

    private boolean i()
    {
        return this.b.g(this.a);
    }

    public final void a(int p11, int p12, int p13, int p14, int p15, int p16, int p17, int p18)
    {
        this.b.a(this.a, p11, p12, p13, p14, p15, p16, p17, p18);
        return;
    }

    public final boolean a()
    {
        return this.b.a(this.a);
    }

    public final int b()
    {
        return this.b.b(this.a);
    }

    public final int c()
    {
        return this.b.c(this.a);
    }

    public final int d()
    {
        return this.b.i(this.a);
    }

    public final float e()
    {
        return this.b.d(this.a);
    }

    public final boolean f()
    {
        return this.b.e(this.a);
    }

    public final void g()
    {
        this.b.f(this.a);
        return;
    }
}
