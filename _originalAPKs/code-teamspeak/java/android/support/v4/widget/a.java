package android.support.v4.widget;
public abstract class a implements android.view.View$OnTouchListener {
    private static final int A = 315;
    private static final int B = 1575;
    private static final float C = 2139095039;
    private static final float D = 1045220557;
    private static final float E = 16256;
    private static final int F = 0;
    private static final int G = 500;
    private static final int H = 500;
    public static final float a = 0;
    public static final float b = 2139095039;
    public static final float c = 0;
    public static final int d = 0;
    public static final int e = 1;
    public static final int f = 2;
    private static final int g = 0;
    private static final int h = 1;
    private static final int z = 1;
    private final android.support.v4.widget.c i;
    private final android.view.animation.Interpolator j;
    private final android.view.View k;
    private Runnable l;
    private float[] m;
    private float[] n;
    private int o;
    private int p;
    private float[] q;
    private float[] r;
    private float[] s;
    private boolean t;
    private boolean u;
    private boolean v;
    private boolean w;
    private boolean x;
    private boolean y;

    static a()
    {
        android.support.v4.widget.a.F = android.view.ViewConfiguration.getTapTimeout();
        return;
    }

    public a(android.view.View p9)
    {
        this.i = new android.support.v4.widget.c();
        this.j = new android.view.animation.AccelerateInterpolator();
        android.support.v4.widget.c v0_4 = new float[2];
        v0_4 = {0, 0};
        this.m = v0_4;
        android.support.v4.widget.c v0_5 = new float[2];
        v0_5 = {2139095039, 2139095039};
        this.n = v0_5;
        android.support.v4.widget.c v0_6 = new float[2];
        v0_6 = {0, 0};
        this.q = v0_6;
        android.support.v4.widget.c v0_7 = new float[2];
        v0_7 = {0, 0};
        this.r = v0_7;
        android.support.v4.widget.c v0_8 = new float[2];
        v0_8 = {2139095039, 2139095039};
        this.s = v0_8;
        this.k = p9;
        android.support.v4.widget.c v0_10 = android.content.res.Resources.getSystem().getDisplayMetrics();
        android.support.v4.widget.c v0_14 = ((int) ((v0_10.density * 1134395392) + 1056964608));
        int v1_5 = ((float) ((int) ((1153753088 * v0_10.density) + 1056964608)));
        this.s[0] = (v1_5 / 1148846080);
        this.s[1] = (v1_5 / 1148846080);
        android.support.v4.widget.c v0_15 = ((float) v0_14);
        this.r[0] = (v0_15 / 1148846080);
        this.r[1] = (v0_15 / 1148846080);
        this.o = 1;
        this.n[0] = 2139095039;
        this.n[1] = 2139095039;
        this.m[0] = 1045220557;
        this.m[1] = 1045220557;
        this.q[0] = 981668463;
        this.q[1] = 981668463;
        this.p = android.support.v4.widget.a.F;
        this.i.a = 500;
        this.i.b = 500;
        return;
    }

    static synthetic float a(float p2)
    {
        return android.support.v4.widget.a.a(p2, 0, 1065353216);
    }

    private static float a(float p1, float p2, float p3)
    {
        if (p1 <= p3) {
            if (p1 >= p2) {
                p3 = p1;
            } else {
                p3 = p2;
            }
        }
        return p3;
    }

    private float a(float p5, float p6, float p7, float p8)
    {
        float v0_2;
        float v0_0 = 0;
        int v1_1 = android.support.v4.widget.a.a((p5 * p6), 0, p7);
        int v1_3 = (this.c((p6 - p8), v1_1) - this.c(p8, v1_1));
        if (v1_3 >= 0) {
            if (v1_3 > 0) {
                v0_2 = this.j.getInterpolation(v1_3);
                v0_0 = android.support.v4.widget.a.a(v0_2, -1082130432, 1065353216);
            }
        } else {
            v0_2 = (- this.j.getInterpolation((- v1_3)));
        }
        return v0_0;
    }

    private float a(int p6, float p7, float p8, float p9)
    {
        float v0_6;
        float v0_7;
        float v0_3 = android.support.v4.widget.a.a((this.m[p6] * p8), 0, this.n[p6]);
        float v0_5 = (this.c((p8 - p7), v0_3) - this.c(p7, v0_3));
        if (v0_5 >= 0) {
            if (v0_5 <= 0) {
                v0_6 = 0;
            } else {
                v0_7 = this.j.getInterpolation(v0_5);
                v0_6 = android.support.v4.widget.a.a(v0_7, -1082130432, 1065353216);
            }
        } else {
            v0_7 = (- this.j.getInterpolation((- v0_5)));
        }
        float v0_13;
        if (v0_6 != 0) {
            float v3_3 = this.r[p6];
            float v4_1 = this.s[p6];
            float v2_11 = (this.q[p6] * p9);
            if (v0_6 <= 0) {
                v0_13 = (- android.support.v4.widget.a.a(((- v0_6) * v2_11), v3_3, v4_1));
            } else {
                v0_13 = android.support.v4.widget.a.a((v0_6 * v2_11), v3_3, v4_1);
            }
        } else {
            v0_13 = 0;
        }
        return v0_13;
    }

    static synthetic int a(int p0, int p1)
    {
        if (p0 <= p1) {
            if (p0 >= 0) {
                p1 = p0;
            } else {
                p1 = 0;
            }
        }
        return p1;
    }

    private static int a(int p0, int p1, int p2)
    {
        if (p0 <= p2) {
            if (p0 >= p1) {
                p2 = p0;
            } else {
                p2 = p1;
            }
        }
        return p2;
    }

    private android.support.v4.widget.a a(float p5, float p6)
    {
        this.s[0] = (p5 / 1148846080);
        this.s[1] = (p6 / 1148846080);
        return this;
    }

    static synthetic boolean a(android.support.v4.widget.a p1)
    {
        return p1.w;
    }

    private android.support.v4.widget.a b(float p5, float p6)
    {
        this.r[0] = (p5 / 1148846080);
        this.r[1] = (p6 / 1148846080);
        return this;
    }

    private android.support.v4.widget.a b(boolean p1)
    {
        this.y = p1;
        return this;
    }

    private boolean b()
    {
        return this.x;
    }

    static synthetic boolean b(android.support.v4.widget.a p1)
    {
        return p1.u;
    }

    private float c(float p5, float p6)
    {
        float v0_0 = 0;
        if (p6 != 0) {
            switch (this.o) {
                case 0:
                case 1:
                    if (p5 < p6) {
                        if (p5 < 0) {
                            if ((this.w) && (this.o == 1)) {
                                v0_0 = 1065353216;
                            }
                        } else {
                            v0_0 = (1065353216 - (p5 / p6));
                        }
                    }
                    break;
                case 2:
                    if (p5 < 0) {
                        v0_0 = (p5 / (- p6));
                    }
                    break;
                default:
            }
        }
        return v0_0;
    }

    private android.support.v4.widget.a c(int p1)
    {
        this.p = p1;
        return this;
    }

    private boolean c()
    {
        return this.y;
    }

    static synthetic boolean c(android.support.v4.widget.a p1)
    {
        p1.u = 0;
        return 0;
    }

    private android.support.v4.widget.a d()
    {
        this.q[0] = 981668463;
        this.q[1] = 981668463;
        return this;
    }

    static synthetic android.support.v4.widget.c d(android.support.v4.widget.a p1)
    {
        return p1.i;
    }

    private android.support.v4.widget.a e()
    {
        this.o = 1;
        return this;
    }

    static synthetic boolean e(android.support.v4.widget.a p1)
    {
        return p1.j();
    }

    private android.support.v4.widget.a f()
    {
        this.m[0] = 1045220557;
        this.m[1] = 1045220557;
        return this;
    }

    static synthetic boolean f(android.support.v4.widget.a p1)
    {
        p1.w = 0;
        return 0;
    }

    private android.support.v4.widget.a g()
    {
        this.n[0] = 2139095039;
        this.n[1] = 2139095039;
        return this;
    }

    static synthetic boolean g(android.support.v4.widget.a p1)
    {
        return p1.v;
    }

    private android.support.v4.widget.a h()
    {
        this.i.a = 500;
        return this;
    }

    static synthetic boolean h(android.support.v4.widget.a p1)
    {
        p1.v = 0;
        return 0;
    }

    private android.support.v4.widget.a i()
    {
        this.i.b = 500;
        return this;
    }

    static synthetic void i(android.support.v4.widget.a p8)
    {
        android.view.MotionEvent v0_0 = android.os.SystemClock.uptimeMillis();
        android.view.MotionEvent v0_1 = android.view.MotionEvent.obtain(v0_0, v0_0, 3, 0, 0, 0);
        p8.k.onTouchEvent(v0_1);
        v0_1.recycle();
        return;
    }

    static synthetic android.view.View j(android.support.v4.widget.a p1)
    {
        return p1.k;
    }

    private boolean j()
    {
        int v0_5;
        int v0_0 = this.i;
        boolean v1_2 = ((int) (v0_0.d / Math.abs(v0_0.d)));
        if ((v1_2) && (this.b(v1_2))) {
            v0_5 = 1;
        } else {
            // Both branches of the condition point to the same code.
            // if (((int) (v0_0.c / Math.abs(v0_0.c))) == 0) {
                v0_5 = 0;
            // }
        }
        return v0_5;
    }

    private void k()
    {
        if (this.l == null) {
            this.l = new android.support.v4.widget.d(this, 0);
        }
        this.w = 1;
        this.u = 1;
        if ((this.t) || (this.p <= 0)) {
            this.l.run();
        } else {
            android.support.v4.view.cx.a(this.k, this.l, ((long) this.p));
        }
        this.t = 1;
        return;
    }

    private void l()
    {
        if (!this.u) {
            android.support.v4.widget.c v3 = this.i;
            long v4 = android.view.animation.AnimationUtils.currentAnimationTimeMillis();
            int v2 = ((int) (v4 - v3.e));
            int v0_1 = v3.b;
            if (v2 <= v0_1) {
                if (v2 >= 0) {
                    v0_1 = v2;
                } else {
                    v0_1 = 0;
                }
            }
            v3.k = v0_1;
            v3.j = v3.a(v4);
            v3.i = v4;
        } else {
            this.w = 0;
        }
        return;
    }

    private void m()
    {
        android.view.MotionEvent v0_0 = android.os.SystemClock.uptimeMillis();
        android.view.MotionEvent v0_1 = android.view.MotionEvent.obtain(v0_0, v0_0, 3, 0, 0, 0);
        this.k.onTouchEvent(v0_1);
        v0_1.recycle();
        return;
    }

    public final android.support.v4.widget.a a(boolean p2)
    {
        if ((this.x) && (!p2)) {
            this.l();
        }
        this.x = p2;
        return this;
    }

    public abstract void a();

    public abstract boolean a();

    public abstract boolean b();

    public boolean onTouch(android.view.View p7, android.view.MotionEvent p8)
    {
        int v0 = 0;
        if (this.x) {
            switch (android.support.v4.view.bk.a(p8)) {
                case 0:
                    this.v = 1;
                    this.t = 0;
                    android.view.View v2_3 = this.a(0, p8.getX(), ((float) p7.getWidth()), ((float) this.k.getWidth()));
                    Runnable v3_3 = this.a(1, p8.getY(), ((float) p7.getHeight()), ((float) this.k.getHeight()));
                    long v4_5 = this.i;
                    v4_5.c = v2_3;
                    v4_5.d = v3_3;
                    if ((this.w) || (!this.j())) {
                    } else {
                        if (this.l == null) {
                            this.l = new android.support.v4.widget.d(this, 0);
                        }
                        this.w = 1;
                        this.u = 1;
                        if ((this.t) || (this.p <= 0)) {
                            this.l.run();
                        } else {
                            android.support.v4.view.cx.a(this.k, this.l, ((long) this.p));
                        }
                        this.t = 1;
                    }
                    break;
                case 1:
                case 3:
                    this.l();
                    break;
                default:
                    if (!this.y) {
                    } else {
                        if (!this.w) {
                        } else {
                            v0 = 1;
                        }
                    }
            }
            if ((this.y) && (this.w)) {
            }
        }
        return v0;
    }
}
