package android.support.v4.widget;
final class ag extends android.support.v4.widget.ej {
    final int a;
    android.support.v4.widget.eg b;
    final synthetic android.support.v4.widget.DrawerLayout c;
    private final Runnable d;

    public ag(android.support.v4.widget.DrawerLayout p2, int p3)
    {
        this.c = p2;
        this.d = new android.support.v4.widget.ah(this);
        this.a = p3;
        return;
    }

    private static synthetic void a(android.support.v4.widget.ag p10)
    {
        android.support.v4.widget.eg v3_0;
        int v7 = 0;
        android.view.View v2_0 = p10.b.t;
        if (p10.a != 3) {
            v3_0 = 0;
        } else {
            v3_0 = 1;
        }
        int v1_2;
        android.view.View v2_1;
        if (v3_0 == null) {
            v2_1 = p10.c.a(5);
            v1_2 = (p10.c.getWidth() - v2_0);
        } else {
            android.view.MotionEvent v0_7;
            int v1_3 = p10.c.a(3);
            if (v1_3 == 0) {
                v0_7 = 0;
            } else {
                v0_7 = (- v1_3.getWidth());
            }
            v2_1 = v1_3;
            v1_2 = (v0_7 + v2_0);
        }
        if (((v2_1 != null) && (((v3_0 != null) && (v2_1.getLeft() < v1_2)) || ((v3_0 == null) && (v2_1.getLeft() > v1_2)))) && (p10.c.a(v2_1) == 0)) {
            android.view.MotionEvent v0_15 = ((android.support.v4.widget.ad) v2_1.getLayoutParams());
            p10.b.a(v2_1, v1_2, v2_1.getTop());
            v0_15.c = 1;
            p10.c.invalidate();
            p10.b();
            android.support.v4.widget.DrawerLayout v9 = p10.c;
            if (!v9.j) {
                android.view.MotionEvent v0_18 = android.os.SystemClock.uptimeMillis();
                android.view.MotionEvent v0_19 = android.view.MotionEvent.obtain(v0_18, v0_18, 3, 0, 0, 0);
                int v1_4 = v9.getChildCount();
                while (v7 < v1_4) {
                    v9.getChildAt(v7).dispatchTouchEvent(v0_19);
                    v7++;
                }
                v0_19.recycle();
                v9.j = 1;
            }
        }
        return;
    }

    private void a(android.support.v4.widget.eg p1)
    {
        this.b = p1;
        return;
    }

    private void e()
    {
        android.support.v4.widget.eg v3_0;
        int v7 = 0;
        android.view.View v2_0 = this.b.t;
        if (this.a != 3) {
            v3_0 = 0;
        } else {
            v3_0 = 1;
        }
        int v1_2;
        android.view.View v2_1;
        if (v3_0 == null) {
            v2_1 = this.c.a(5);
            v1_2 = (this.c.getWidth() - v2_0);
        } else {
            android.view.MotionEvent v0_7;
            int v1_3 = this.c.a(3);
            if (v1_3 == 0) {
                v0_7 = 0;
            } else {
                v0_7 = (- v1_3.getWidth());
            }
            v2_1 = v1_3;
            v1_2 = (v0_7 + v2_0);
        }
        if (((v2_1 != null) && (((v3_0 != null) && (v2_1.getLeft() < v1_2)) || ((v3_0 == null) && (v2_1.getLeft() > v1_2)))) && (this.c.a(v2_1) == 0)) {
            android.view.MotionEvent v0_15 = ((android.support.v4.widget.ad) v2_1.getLayoutParams());
            this.b.a(v2_1, v1_2, v2_1.getTop());
            v0_15.c = 1;
            this.c.invalidate();
            this.b();
            android.support.v4.widget.DrawerLayout v9 = this.c;
            if (!v9.j) {
                android.view.MotionEvent v0_18 = android.os.SystemClock.uptimeMillis();
                android.view.MotionEvent v0_19 = android.view.MotionEvent.obtain(v0_18, v0_18, 3, 0, 0, 0);
                int v1_4 = v9.getChildCount();
                while (v7 < v1_4) {
                    v9.getChildAt(v7).dispatchTouchEvent(v0_19);
                    v7++;
                }
                v0_19.recycle();
                v9.j = 1;
            }
        }
        return;
    }

    public final int a(android.view.View p3, int p4)
    {
        int v0_5;
        if (!this.c.a(p3, 3)) {
            int v0_3 = this.c.getWidth();
            v0_5 = Math.max((v0_3 - p3.getWidth()), Math.min(p4, v0_3));
        } else {
            v0_5 = Math.max((- p3.getWidth()), Math.min(p4, 0));
        }
        return v0_5;
    }

    public final void a()
    {
        this.c.removeCallbacks(this.d);
        return;
    }

    public final void a(int p10)
    {
        int v1_3;
        android.support.v4.widget.DrawerLayout v4 = this.c;
        android.view.View v5 = this.b.v;
        int v1_2 = v4.g.m;
        float v6_1 = v4.h.m;
        if ((v1_2 != 1) && (v6_1 != 1)) {
            if ((v1_2 != 2) && (v6_1 != 2)) {
                v1_3 = 0;
            } else {
                v1_3 = 2;
            }
        } else {
            v1_3 = 1;
        }
        if ((v5 != null) && (p10 == 0)) {
            boolean v0_2 = ((android.support.v4.widget.ad) v5.getLayoutParams());
            if (v0_2.b != 0) {
                if (v0_2.b == 1065353216) {
                    boolean v0_6 = ((android.support.v4.widget.ad) v5.getLayoutParams());
                    if (!v0_6.d) {
                        v0_6.d = 1;
                        if (v4.k != null) {
                            v4.k.a();
                        }
                        v4.a(v5, 1);
                        if (v4.hasWindowFocus()) {
                            v4.sendAccessibilityEvent(32);
                        }
                        v5.requestFocus();
                    }
                }
            } else {
                boolean v0_11 = ((android.support.v4.widget.ad) v5.getLayoutParams());
                if (v0_11.d) {
                    v0_11.d = 0;
                    if (v4.k != null) {
                        v4.k.b();
                    }
                    v4.a(v5, 0);
                    if (v4.hasWindowFocus()) {
                        boolean v0_15 = v4.getRootView();
                        if (v0_15) {
                            v0_15.sendAccessibilityEvent(32);
                        }
                    }
                }
            }
        }
        if (v1_3 != v4.i) {
            v4.i = v1_3;
        }
        return;
    }

    public final void a(int p3, int p4)
    {
        android.view.View v0_2;
        if ((p3 & 1) != 1) {
            v0_2 = this.c.a(5);
        } else {
            v0_2 = this.c.a(3);
        }
        if ((v0_2 != null) && (this.c.a(v0_2) == 0)) {
            this.b.a(v0_2, p4);
        }
        return;
    }

    public final void a(android.view.View p7, float p8)
    {
        android.support.v4.widget.DrawerLayout v0_3;
        float v1_0 = android.support.v4.widget.DrawerLayout.b(p7);
        int v2_0 = p7.getWidth();
        if (!this.c.a(p7, 3)) {
            v0_3 = this.c.getWidth();
            if ((p8 < 0) || ((p8 == 0) && (v1_0 > 1056964608))) {
                v0_3 -= v2_0;
            }
        } else {
            if ((p8 <= 0) && ((p8 != 0) || (v1_0 <= 1056964608))) {
                v0_3 = (- v2_0);
            } else {
                v0_3 = 0;
            }
        }
        this.b.a(v0_3, p7.getTop());
        this.c.invalidate();
        return;
    }

    public final boolean a(android.view.View p3)
    {
        if ((!android.support.v4.widget.DrawerLayout.d(p3)) || ((!this.c.a(p3, this.a)) || (this.c.a(p3) != 0))) {
            int v0_5 = 0;
        } else {
            v0_5 = 1;
        }
        return v0_5;
    }

    public final int b(android.view.View p2)
    {
        int v0_1;
        if (!android.support.v4.widget.DrawerLayout.d(p2)) {
            v0_1 = 0;
        } else {
            v0_1 = p2.getWidth();
        }
        return v0_1;
    }

    final void b()
    {
        android.view.View v0_0 = 3;
        if (this.a == 3) {
            v0_0 = 5;
        }
        android.view.View v0_1 = this.c.a(v0_0);
        if (v0_1 != null) {
            this.c.e(v0_1);
        }
        return;
    }

    public final void b(android.view.View p4, int p5)
    {
        android.support.v4.widget.DrawerLayout v0_2;
        android.support.v4.widget.DrawerLayout v0_0 = p4.getWidth();
        if (!this.c.a(p4, 3)) {
            v0_2 = (((float) (this.c.getWidth() - p5)) / ((float) v0_0));
        } else {
            v0_2 = (((float) (v0_0 + p5)) / ((float) v0_0));
        }
        android.support.v4.widget.DrawerLayout v0_5;
        this.c.a(p4, v0_2);
        if (v0_2 != 0) {
            v0_5 = 0;
        } else {
            v0_5 = 4;
        }
        p4.setVisibility(v0_5);
        this.c.invalidate();
        return;
    }

    public final int c(android.view.View p2)
    {
        return p2.getTop();
    }

    public final void c()
    {
        this.c.postDelayed(this.d, 160);
        return;
    }

    public final void d(android.view.View p3)
    {
        ((android.support.v4.widget.ad) p3.getLayoutParams()).c = 0;
        this.b();
        return;
    }

    public final boolean d()
    {
        return 0;
    }
}
