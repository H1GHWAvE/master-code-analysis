package android.support.v4.widget;
final class cq {

    cq()
    {
        return;
    }

    private static android.view.View a(android.content.Context p1)
    {
        return new android.widget.SearchView(p1);
    }

    private static CharSequence a(android.view.View p1)
    {
        return ((android.widget.SearchView) p1).getQuery();
    }

    private static Object a(android.support.v4.widget.ct p1)
    {
        return new android.support.v4.widget.cs(p1);
    }

    private static Object a(android.support.v4.widget.cu p1)
    {
        return new android.support.v4.widget.cr(p1);
    }

    private static void a(android.view.View p0, int p1)
    {
        ((android.widget.SearchView) p0).setMaxWidth(p1);
        return;
    }

    private static void a(android.view.View p2, android.content.ComponentName p3)
    {
        ((android.widget.SearchView) p2).setSearchableInfo(((android.app.SearchManager) ((android.widget.SearchView) p2).getContext().getSystemService("search")).getSearchableInfo(p3));
        return;
    }

    private static void a(android.view.View p0, CharSequence p1)
    {
        ((android.widget.SearchView) p0).setQueryHint(p1);
        return;
    }

    private static void a(android.view.View p0, CharSequence p1, boolean p2)
    {
        ((android.widget.SearchView) p0).setQuery(p1, p2);
        return;
    }

    private static void a(android.view.View p0, boolean p1)
    {
        ((android.widget.SearchView) p0).setIconified(p1);
        return;
    }

    private static void a(Object p0, Object p1)
    {
        ((android.widget.SearchView) p0).setOnQueryTextListener(((android.widget.SearchView$OnQueryTextListener) p1));
        return;
    }

    private static void b(android.view.View p0, boolean p1)
    {
        ((android.widget.SearchView) p0).setSubmitButtonEnabled(p1);
        return;
    }

    private static void b(Object p0, Object p1)
    {
        ((android.widget.SearchView) p0).setOnCloseListener(((android.widget.SearchView$OnCloseListener) p1));
        return;
    }

    private static boolean b(android.view.View p1)
    {
        return ((android.widget.SearchView) p1).isIconified();
    }

    private static void c(android.view.View p0, boolean p1)
    {
        ((android.widget.SearchView) p0).setQueryRefinementEnabled(p1);
        return;
    }

    private static boolean c(android.view.View p1)
    {
        return ((android.widget.SearchView) p1).isSubmitButtonEnabled();
    }

    private static boolean d(android.view.View p1)
    {
        return ((android.widget.SearchView) p1).isQueryRefinementEnabled();
    }
}
