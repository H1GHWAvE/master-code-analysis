package android.support.v4.widget;
final class bi implements android.os.Parcelable$Creator {

    bi()
    {
        return;
    }

    private static android.support.v4.widget.NestedScrollView$SavedState a(android.os.Parcel p1)
    {
        return new android.support.v4.widget.NestedScrollView$SavedState(p1);
    }

    private static android.support.v4.widget.NestedScrollView$SavedState[] a(int p1)
    {
        android.support.v4.widget.NestedScrollView$SavedState[] v0 = new android.support.v4.widget.NestedScrollView$SavedState[p1];
        return v0;
    }

    public final synthetic Object createFromParcel(android.os.Parcel p2)
    {
        return new android.support.v4.widget.NestedScrollView$SavedState(p2);
    }

    public final bridge synthetic Object[] newArray(int p2)
    {
        android.support.v4.widget.NestedScrollView$SavedState[] v0 = new android.support.v4.widget.NestedScrollView$SavedState[p2];
        return v0;
    }
}
