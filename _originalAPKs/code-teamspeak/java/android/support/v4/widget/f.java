package android.support.v4.widget;
final class f extends android.graphics.drawable.shapes.OvalShape {
    final synthetic android.support.v4.widget.e a;
    private android.graphics.RadialGradient b;
    private android.graphics.Paint c;
    private int d;

    public f(android.support.v4.widget.e p8, int p9, int p10)
    {
        this.a = p8;
        this.c = new android.graphics.Paint();
        android.support.v4.widget.e.a(p8, p9);
        this.d = p10;
        int[] v4_1 = new int[2];
        v4_1 = {1023410176, 0};
        this.b = new android.graphics.RadialGradient(((float) (this.d / 2)), ((float) (this.d / 2)), ((float) android.support.v4.widget.e.a(p8)), v4_1, 0, android.graphics.Shader$TileMode.CLAMP);
        this.c.setShader(this.b);
        return;
    }

    public final void draw(android.graphics.Canvas p7, android.graphics.Paint p8)
    {
        float v0_1 = this.a.getWidth();
        float v1_1 = this.a.getHeight();
        p7.drawCircle(((float) (v0_1 / 2)), ((float) (v1_1 / 2)), ((float) ((this.d / 2) + android.support.v4.widget.e.a(this.a))), this.c);
        p7.drawCircle(((float) (v0_1 / 2)), ((float) (v1_1 / 2)), ((float) (this.d / 2)), p8);
        return;
    }
}
