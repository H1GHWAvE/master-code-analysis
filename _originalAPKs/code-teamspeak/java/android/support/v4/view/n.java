package android.support.v4.view;
public abstract class n {
    private static final String c = "ActionProvider(support)";
    public android.support.v4.view.o a;
    public android.support.v4.view.p b;
    private final android.content.Context d;

    public n(android.content.Context p1)
    {
        this.d = p1;
        return;
    }

    private void a(android.support.v4.view.o p1)
    {
        this.a = p1;
        return;
    }

    private android.content.Context g()
    {
        return this.d;
    }

    private void h()
    {
        this.b = 0;
        this.a = 0;
        return;
    }

    public abstract android.view.View a();

    public android.view.View a(android.view.MenuItem p2)
    {
        return this.a();
    }

    public void a(android.support.v4.view.p p4)
    {
        if (this.b != null) {
            android.util.Log.w("ActionProvider(support)", new StringBuilder("setVisibilityListener: Setting a new ActionProvider.VisibilityListener when one is already set. Are you reusing this ").append(this.getClass().getSimpleName()).append(" instance while it is still in use somewhere else?").toString());
        }
        this.b = p4;
        return;
    }

    public void a(android.view.SubMenu p1)
    {
        return;
    }

    public final void a(boolean p2)
    {
        if (this.a != null) {
            this.a.b(p2);
        }
        return;
    }

    public boolean b()
    {
        return 0;
    }

    public boolean c()
    {
        return 1;
    }

    public void d()
    {
        if ((this.b != null) && (this.b())) {
            android.support.v4.view.p v0_2 = this.b;
            this.c();
            v0_2.a();
        }
        return;
    }

    public boolean e()
    {
        return 0;
    }

    public boolean f()
    {
        return 0;
    }
}
