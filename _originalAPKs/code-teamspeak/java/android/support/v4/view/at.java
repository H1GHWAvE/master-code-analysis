package android.support.v4.view;
public final class at {
    static final android.support.v4.view.au a;

    static at()
    {
        if (android.os.Build$VERSION.SDK_INT < 17) {
            android.support.v4.view.at.a = new android.support.v4.view.av();
        } else {
            android.support.v4.view.at.a = new android.support.v4.view.aw();
        }
        return;
    }

    public at()
    {
        return;
    }

    public static int a(android.view.ViewGroup$MarginLayoutParams p1)
    {
        return android.support.v4.view.at.a.a(p1);
    }

    private static void a(android.view.ViewGroup$MarginLayoutParams p1, int p2)
    {
        android.support.v4.view.at.a.a(p1, p2);
        return;
    }

    public static int b(android.view.ViewGroup$MarginLayoutParams p1)
    {
        return android.support.v4.view.at.a.b(p1);
    }

    private static void b(android.view.ViewGroup$MarginLayoutParams p1, int p2)
    {
        android.support.v4.view.at.a.b(p1, p2);
        return;
    }

    private static void c(android.view.ViewGroup$MarginLayoutParams p1, int p2)
    {
        android.support.v4.view.at.a.c(p1, p2);
        return;
    }

    private static boolean c(android.view.ViewGroup$MarginLayoutParams p1)
    {
        return android.support.v4.view.at.a.c(p1);
    }

    private static int d(android.view.ViewGroup$MarginLayoutParams p1)
    {
        return android.support.v4.view.at.a.d(p1);
    }

    private static void d(android.view.ViewGroup$MarginLayoutParams p1, int p2)
    {
        android.support.v4.view.at.a.d(p1, p2);
        return;
    }
}
