package android.support.v4.view;
 class dg extends android.support.v4.view.df {

    dg()
    {
        return;
    }

    public final boolean Z(android.view.View p2)
    {
        return p2.isLaidOut();
    }

    public final boolean ab(android.view.View p2)
    {
        return p2.isAttachedToWindow();
    }

    public final void d(android.view.View p1, int p2)
    {
        p1.setImportantForAccessibility(p2);
        return;
    }

    public final void g(android.view.View p1, int p2)
    {
        p1.setAccessibilityLiveRegion(p2);
        return;
    }

    public final int q(android.view.View p2)
    {
        return p2.getAccessibilityLiveRegion();
    }
}
