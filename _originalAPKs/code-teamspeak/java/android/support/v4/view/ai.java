package android.support.v4.view;
public final class ai {
    static final android.support.v4.view.aj a;

    static ai()
    {
        android.support.v4.view.ak v0_0 = android.os.Build$VERSION.SDK_INT;
        if (v0_0 < 21) {
            if (v0_0 < 11) {
                android.support.v4.view.ai.a = new android.support.v4.view.ak();
            } else {
                android.support.v4.view.ai.a = new android.support.v4.view.al();
            }
        } else {
            android.support.v4.view.ai.a = new android.support.v4.view.am();
        }
        return;
    }

    private ai()
    {
        return;
    }

    public static void a(android.view.LayoutInflater p1, android.support.v4.view.as p2)
    {
        android.support.v4.view.ai.a.a(p1, p2);
        return;
    }
}
