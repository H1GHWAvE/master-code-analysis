package android.support.v4.view;
final class t extends android.os.Handler {
    final synthetic android.support.v4.view.s a;

    t(android.support.v4.view.s p1)
    {
        this.a = p1;
        return;
    }

    private t(android.support.v4.view.s p2, android.os.Handler p3)
    {
        this.a = p2;
        this(p3.getLooper());
        return;
    }

    public final void handleMessage(android.os.Message p4)
    {
        switch (p4.what) {
            case 1:
                android.support.v4.view.s.b(this.a).onShowPress(android.support.v4.view.s.a(this.a));
                break;
            case 2:
                android.support.v4.view.s.c(this.a);
                break;
            case 3:
                if (android.support.v4.view.s.d(this.a) == null) {
                } else {
                    if (android.support.v4.view.s.e(this.a)) {
                        android.support.v4.view.s.f(this.a);
                    } else {
                        android.support.v4.view.s.d(this.a).onSingleTapConfirmed(android.support.v4.view.s.a(this.a));
                    }
                }
                break;
            default:
                throw new RuntimeException(new StringBuilder("Unknown message ").append(p4).toString());
        }
        return;
    }
}
