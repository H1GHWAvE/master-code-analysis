package android.support.v4.view;
final class dm {

    dm()
    {
        return;
    }

    private static float a(android.view.View p1)
    {
        return p1.getAlpha();
    }

    private static int a(int p1, int p2)
    {
        return android.view.View.combineMeasuredStates(p1, p2);
    }

    private static int a(int p1, int p2, int p3)
    {
        return android.view.View.resolveSizeAndState(p1, p2, p3);
    }

    private static long a()
    {
        return android.animation.ValueAnimator.getFrameDelay();
    }

    private static void a(android.view.View p0, float p1)
    {
        p0.setTranslationX(p1);
        return;
    }

    private static void a(android.view.View p0, int p1, android.graphics.Paint p2)
    {
        p0.setLayerType(p1, p2);
        return;
    }

    private static void a(android.view.View p0, boolean p1)
    {
        p0.setSaveFromParentEnabled(p1);
        return;
    }

    private static int b(android.view.View p1)
    {
        return p1.getLayerType();
    }

    private static void b(android.view.View p0, float p1)
    {
        p0.setTranslationY(p1);
        return;
    }

    private static void b(android.view.View p0, boolean p1)
    {
        p0.setActivated(p1);
        return;
    }

    private static int c(android.view.View p1)
    {
        return p1.getMeasuredWidthAndState();
    }

    private static void c(android.view.View p0, float p1)
    {
        p0.setAlpha(p1);
        return;
    }

    private static int d(android.view.View p1)
    {
        return p1.getMeasuredHeightAndState();
    }

    private static void d(android.view.View p0, float p1)
    {
        p0.setX(p1);
        return;
    }

    private static int e(android.view.View p1)
    {
        return p1.getMeasuredState();
    }

    private static void e(android.view.View p0, float p1)
    {
        p0.setY(p1);
        return;
    }

    private static float f(android.view.View p1)
    {
        return p1.getTranslationX();
    }

    private static void f(android.view.View p0, float p1)
    {
        p0.setRotation(p1);
        return;
    }

    private static float g(android.view.View p1)
    {
        return p1.getTranslationY();
    }

    private static void g(android.view.View p0, float p1)
    {
        p0.setRotationX(p1);
        return;
    }

    private static float h(android.view.View p1)
    {
        return p1.getX();
    }

    private static void h(android.view.View p0, float p1)
    {
        p0.setRotationY(p1);
        return;
    }

    private static float i(android.view.View p1)
    {
        return p1.getY();
    }

    private static void i(android.view.View p0, float p1)
    {
        p0.setScaleX(p1);
        return;
    }

    private static float j(android.view.View p1)
    {
        return p1.getRotation();
    }

    private static void j(android.view.View p0, float p1)
    {
        p0.setScaleY(p1);
        return;
    }

    private static float k(android.view.View p1)
    {
        return p1.getRotationX();
    }

    private static void k(android.view.View p0, float p1)
    {
        p0.setPivotX(p1);
        return;
    }

    private static float l(android.view.View p1)
    {
        return p1.getRotationY();
    }

    private static void l(android.view.View p0, float p1)
    {
        p0.setPivotY(p1);
        return;
    }

    private static float m(android.view.View p1)
    {
        return p1.getScaleX();
    }

    private static float n(android.view.View p1)
    {
        return p1.getScaleY();
    }

    private static float o(android.view.View p1)
    {
        return p1.getPivotX();
    }

    private static float p(android.view.View p1)
    {
        return p1.getPivotY();
    }

    private static void q(android.view.View p0)
    {
        p0.jumpDrawablesToCurrentState();
        return;
    }
}
