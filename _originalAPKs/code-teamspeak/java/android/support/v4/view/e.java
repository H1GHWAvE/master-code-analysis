package android.support.v4.view;
final class e extends android.support.v4.view.b {

    e()
    {
        return;
    }

    public final android.support.v4.view.a.aq a(Object p3, android.view.View p4)
    {
        int v0_0;
        android.view.accessibility.AccessibilityNodeProvider v1 = ((android.view.View$AccessibilityDelegate) p3).getAccessibilityNodeProvider(p4);
        if (v1 == null) {
            v0_0 = 0;
        } else {
            v0_0 = new android.support.v4.view.a.aq(v1);
        }
        return v0_0;
    }

    public final Object a(android.support.v4.view.a p3)
    {
        return new android.support.v4.view.l(new android.support.v4.view.f(this, p3));
    }

    public final boolean a(Object p2, android.view.View p3, int p4, android.os.Bundle p5)
    {
        return ((android.view.View$AccessibilityDelegate) p2).performAccessibilityAction(p3, p4, p5);
    }
}
