package android.support.v4.view;
final class dh extends android.support.v4.view.dg {

    dh()
    {
        return;
    }

    public final String K(android.view.View p2)
    {
        return p2.getTransitionName();
    }

    public final void M(android.view.View p1)
    {
        p1.requestApplyInsets();
        return;
    }

    public final float N(android.view.View p2)
    {
        return p2.getElevation();
    }

    public final float O(android.view.View p2)
    {
        return p2.getTranslationZ();
    }

    public final boolean U(android.view.View p2)
    {
        return p2.isNestedScrollingEnabled();
    }

    public final android.content.res.ColorStateList V(android.view.View p2)
    {
        return p2.getBackgroundTintList();
    }

    public final android.graphics.PorterDuff$Mode W(android.view.View p2)
    {
        return p2.getBackgroundTintMode();
    }

    public final void X(android.view.View p1)
    {
        p1.stopNestedScroll();
        return;
    }

    public final boolean Y(android.view.View p2)
    {
        return p2.hasNestedScrollingParent();
    }

    public final android.support.v4.view.gh a(android.view.View p3, android.support.v4.view.gh p4)
    {
        if ((p4 instanceof android.support.v4.view.gi)) {
            android.view.WindowInsets v0_3 = ((android.support.v4.view.gi) p4).a;
            android.view.WindowInsets v1 = p3.onApplyWindowInsets(v0_3);
            if (v1 != v0_3) {
                p4 = new android.support.v4.view.gi(v1);
            }
        }
        return p4;
    }

    public final void a(android.view.View p1, android.content.res.ColorStateList p2)
    {
        p1.setBackgroundTintList(p2);
        return;
    }

    public final void a(android.view.View p1, android.graphics.PorterDuff$Mode p2)
    {
        p1.setBackgroundTintMode(p2);
        return;
    }

    public final void a(android.view.View p2, android.support.v4.view.bx p3)
    {
        p2.setOnApplyWindowInsetsListener(new android.support.v4.view.dt(p3));
        return;
    }

    public final void a(android.view.View p1, String p2)
    {
        p1.setTransitionName(p2);
        return;
    }

    public final boolean a(android.view.View p2, float p3, float p4)
    {
        return p2.dispatchNestedPreFling(p3, p4);
    }

    public final boolean a(android.view.View p2, float p3, float p4, boolean p5)
    {
        return p2.dispatchNestedFling(p3, p4, p5);
    }

    public final boolean a(android.view.View p2, int p3, int p4, int p5, int p6, int[] p7)
    {
        return p2.dispatchNestedScroll(p3, p4, p5, p6, p7);
    }

    public final boolean a(android.view.View p2, int p3, int p4, int[] p5, int[] p6)
    {
        return p2.dispatchNestedPreScroll(p3, p4, p5, p6);
    }

    public final float aa(android.view.View p2)
    {
        return p2.getZ();
    }

    public final android.support.v4.view.gh b(android.view.View p3, android.support.v4.view.gh p4)
    {
        if ((p4 instanceof android.support.v4.view.gi)) {
            android.view.WindowInsets v0_3 = ((android.support.v4.view.gi) p4).a;
            android.view.WindowInsets v1 = p3.dispatchApplyWindowInsets(v0_3);
            if (v1 != v0_3) {
                p4 = new android.support.v4.view.gi(v1);
            }
        }
        return p4;
    }

    public final void d(android.view.View p1, boolean p2)
    {
        p1.setNestedScrollingEnabled(p2);
        return;
    }

    public final boolean f(android.view.View p2)
    {
        return p2.isImportantForAccessibility();
    }

    public final boolean h(android.view.View p2, int p3)
    {
        return p2.startNestedScroll(p3);
    }

    public final void m(android.view.View p1, float p2)
    {
        p1.setElevation(p2);
        return;
    }

    public final void n(android.view.View p1, float p2)
    {
        p1.setTranslationZ(p2);
        return;
    }
}
