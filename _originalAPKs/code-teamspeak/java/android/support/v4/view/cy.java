package android.support.v4.view;
 class cy implements android.support.v4.view.di {
    java.util.WeakHashMap a;
    private reflect.Method b;
    private reflect.Method c;
    private boolean d;

    cy()
    {
        this.a = 0;
        return;
    }

    private static boolean a(android.support.v4.view.cq p5, int p6)
    {
        int v0 = 0;
        int v2 = p5.b();
        int v3_1 = (p5.a() - p5.c());
        if (v3_1 != 0) {
            if (p6 >= 0) {
                if (v2 < (v3_1 - 1)) {
                    v0 = 1;
                }
            } else {
                if (v2 > 0) {
                    v0 = 1;
                }
            }
        }
        return v0;
    }

    private void b()
    {
        try {
            String v2_1 = new Class[0];
            this.b = android.view.View.getDeclaredMethod("dispatchStartTemporaryDetach", v2_1);
            String v2_3 = new Class[0];
            this.c = android.view.View.getDeclaredMethod("dispatchFinishTemporaryDetach", v2_3);
        } catch (int v0_4) {
            android.util.Log.e("ViewCompat", "Couldn\'t find method", v0_4);
        }
        this.d = 1;
        return;
    }

    private static boolean b(android.support.v4.view.cq p5, int p6)
    {
        int v0 = 0;
        int v2 = p5.e();
        int v3_1 = (p5.d() - p5.f());
        if (v3_1 != 0) {
            if (p6 >= 0) {
                if (v2 < (v3_1 - 1)) {
                    v0 = 1;
                }
            } else {
                if (v2 > 0) {
                    v0 = 1;
                }
            }
        }
        return v0;
    }

    public float A(android.view.View p2)
    {
        return 0;
    }

    public float B(android.view.View p2)
    {
        return 0;
    }

    public float C(android.view.View p2)
    {
        return 0;
    }

    public float D(android.view.View p2)
    {
        return 0;
    }

    public float E(android.view.View p2)
    {
        return 0;
    }

    public int F(android.view.View p2)
    {
        return android.support.v4.view.dj.a(p2);
    }

    public int G(android.view.View p2)
    {
        return android.support.v4.view.dj.b(p2);
    }

    public android.support.v4.view.fk H(android.view.View p2)
    {
        return new android.support.v4.view.fk(p2);
    }

    public float I(android.view.View p2)
    {
        return 0;
    }

    public float J(android.view.View p2)
    {
        return 0;
    }

    public String K(android.view.View p2)
    {
        return 0;
    }

    public int L(android.view.View p2)
    {
        return 0;
    }

    public void M(android.view.View p1)
    {
        return;
    }

    public float N(android.view.View p2)
    {
        return 0;
    }

    public float O(android.view.View p2)
    {
        return 0;
    }

    public android.graphics.Rect P(android.view.View p2)
    {
        return 0;
    }

    public boolean Q(android.view.View p2)
    {
        return 0;
    }

    public void R(android.view.View p1)
    {
        return;
    }

    public void S(android.view.View p1)
    {
        return;
    }

    public boolean T(android.view.View p2)
    {
        return 0;
    }

    public boolean U(android.view.View p2)
    {
        int v0_1;
        if (!(p2 instanceof android.support.v4.view.bt)) {
            v0_1 = 0;
        } else {
            v0_1 = ((android.support.v4.view.bt) p2).isNestedScrollingEnabled();
        }
        return v0_1;
    }

    public android.content.res.ColorStateList V(android.view.View p2)
    {
        int v0_1;
        if (!(p2 instanceof android.support.v4.view.cr)) {
            v0_1 = 0;
        } else {
            v0_1 = ((android.support.v4.view.cr) p2).getSupportBackgroundTintList();
        }
        return v0_1;
    }

    public android.graphics.PorterDuff$Mode W(android.view.View p2)
    {
        int v0_1;
        if (!(p2 instanceof android.support.v4.view.cr)) {
            v0_1 = 0;
        } else {
            v0_1 = ((android.support.v4.view.cr) p2).getSupportBackgroundTintMode();
        }
        return v0_1;
    }

    public void X(android.view.View p2)
    {
        if ((p2 instanceof android.support.v4.view.bt)) {
            ((android.support.v4.view.bt) p2).stopNestedScroll();
        }
        return;
    }

    public boolean Y(android.view.View p2)
    {
        int v0_1;
        if (!(p2 instanceof android.support.v4.view.bt)) {
            v0_1 = 0;
        } else {
            v0_1 = ((android.support.v4.view.bt) p2).hasNestedScrollingParent();
        }
        return v0_1;
    }

    public boolean Z(android.view.View p2)
    {
        if ((p2.getWidth() <= 0) || (p2.getHeight() <= 0)) {
            int v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    public int a(int p2, int p3)
    {
        return (p2 | p3);
    }

    public int a(int p2, int p3, int p4)
    {
        return android.view.View.resolveSize(p2, p3);
    }

    public int a(android.view.View p2)
    {
        return 2;
    }

    long a()
    {
        return 10;
    }

    public android.support.v4.view.gh a(android.view.View p1, android.support.v4.view.gh p2)
    {
        return p2;
    }

    public void a(android.view.View p1, float p2)
    {
        return;
    }

    public void a(android.view.View p1, int p2, int p3, int p4, int p5)
    {
        p1.invalidate(p2, p3, p4, p5);
        return;
    }

    public void a(android.view.View p1, int p2, android.graphics.Paint p3)
    {
        return;
    }

    public void a(android.view.View p2, android.content.res.ColorStateList p3)
    {
        if ((p2 instanceof android.support.v4.view.cr)) {
            ((android.support.v4.view.cr) p2).setSupportBackgroundTintList(p3);
        }
        return;
    }

    public void a(android.view.View p1, android.graphics.Paint p2)
    {
        return;
    }

    public void a(android.view.View p2, android.graphics.PorterDuff$Mode p3)
    {
        if ((p2 instanceof android.support.v4.view.cr)) {
            ((android.support.v4.view.cr) p2).setSupportBackgroundTintMode(p3);
        }
        return;
    }

    public void a(android.view.View p1, android.graphics.Rect p2)
    {
        return;
    }

    public void a(android.view.View p1, android.support.v4.view.a.q p2)
    {
        return;
    }

    public void a(android.view.View p1, android.support.v4.view.a p2)
    {
        return;
    }

    public void a(android.view.View p1, android.support.v4.view.bx p2)
    {
        return;
    }

    public void a(android.view.View p1, android.view.accessibility.AccessibilityEvent p2)
    {
        return;
    }

    public void a(android.view.View p3, Runnable p4)
    {
        p3.postDelayed(p4, this.a());
        return;
    }

    public void a(android.view.View p4, Runnable p5, long p6)
    {
        p4.postDelayed(p5, (this.a() + p6));
        return;
    }

    public void a(android.view.View p1, String p2)
    {
        return;
    }

    public void a(android.view.View p1, boolean p2)
    {
        return;
    }

    public void a(android.view.ViewGroup p1)
    {
        return;
    }

    public boolean a(android.view.View p2, float p3, float p4)
    {
        int v0_1;
        if (!(p2 instanceof android.support.v4.view.bt)) {
            v0_1 = 0;
        } else {
            v0_1 = ((android.support.v4.view.bt) p2).dispatchNestedPreFling(p3, p4);
        }
        return v0_1;
    }

    public boolean a(android.view.View p2, float p3, float p4, boolean p5)
    {
        int v0_1;
        if (!(p2 instanceof android.support.v4.view.bt)) {
            v0_1 = 0;
        } else {
            v0_1 = ((android.support.v4.view.bt) p2).dispatchNestedFling(p3, p4, p5);
        }
        return v0_1;
    }

    public boolean a(android.view.View p6, int p7)
    {
        int v0 = 1;
        if (!(p6 instanceof android.support.v4.view.cq)) {
            v0 = 0;
        } else {
            int v2_2;
            int v2_1 = ((android.support.v4.view.cq) p6).b();
            int v3_1 = (((android.support.v4.view.cq) p6).a() - ((android.support.v4.view.cq) p6).c());
            if (v3_1 == 0) {
                v2_2 = 0;
            } else {
                if (p7 >= 0) {
                    if (v2_1 >= (v3_1 - 1)) {
                    } else {
                        v2_2 = 1;
                    }
                } else {
                    if (v2_1 <= 0) {
                        v2_2 = 0;
                    } else {
                        v2_2 = 1;
                    }
                }
            }
            if (v2_2 == 0) {
            }
        }
        return v0;
    }

    public boolean a(android.view.View p7, int p8, int p9, int p10, int p11, int[] p12)
    {
        int v0_1;
        if (!(p7 instanceof android.support.v4.view.bt)) {
            v0_1 = 0;
        } else {
            v0_1 = ((android.support.v4.view.bt) p7).dispatchNestedScroll(p8, p9, p10, p11, p12);
        }
        return v0_1;
    }

    public boolean a(android.view.View p2, int p3, int p4, int[] p5, int[] p6)
    {
        int v0_1;
        if (!(p2 instanceof android.support.v4.view.bt)) {
            v0_1 = 0;
        } else {
            v0_1 = ((android.support.v4.view.bt) p2).dispatchNestedPreScroll(p3, p4, p5, p6);
        }
        return v0_1;
    }

    public boolean a(android.view.View p2, int p3, android.os.Bundle p4)
    {
        return 0;
    }

    public float aa(android.view.View p3)
    {
        return (this.O(p3) + this.N(p3));
    }

    public boolean ab(android.view.View p2)
    {
        int v0_1;
        if (p2.getWindowToken() == null) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    public android.support.v4.view.gh b(android.view.View p1, android.support.v4.view.gh p2)
    {
        return p2;
    }

    public void b(android.view.View p1, float p2)
    {
        return;
    }

    public void b(android.view.View p1, int p2, int p3, int p4, int p5)
    {
        p1.setPadding(p2, p3, p4, p5);
        return;
    }

    public void b(android.view.View p1, android.view.accessibility.AccessibilityEvent p2)
    {
        return;
    }

    public void b(android.view.View p1, boolean p2)
    {
        return;
    }

    public boolean b(android.view.View p2)
    {
        return 0;
    }

    public boolean b(android.view.View p6, int p7)
    {
        int v0 = 1;
        if (!(p6 instanceof android.support.v4.view.cq)) {
            v0 = 0;
        } else {
            int v2_2;
            int v2_1 = ((android.support.v4.view.cq) p6).e();
            int v3_1 = (((android.support.v4.view.cq) p6).d() - ((android.support.v4.view.cq) p6).f());
            if (v3_1 == 0) {
                v2_2 = 0;
            } else {
                if (p7 >= 0) {
                    if (v2_1 >= (v3_1 - 1)) {
                    } else {
                        v2_2 = 1;
                    }
                } else {
                    if (v2_1 <= 0) {
                        v2_2 = 0;
                    } else {
                        v2_2 = 1;
                    }
                }
            }
            if (v2_2 == 0) {
            }
        }
        return v0;
    }

    public void c(android.view.View p1, float p2)
    {
        return;
    }

    public void c(android.view.View p1, int p2)
    {
        return;
    }

    public void c(android.view.View p1, boolean p2)
    {
        return;
    }

    public boolean c(android.view.View p2)
    {
        return 0;
    }

    public void d(android.view.View p1)
    {
        p1.invalidate();
        return;
    }

    public void d(android.view.View p1, float p2)
    {
        return;
    }

    public void d(android.view.View p1, int p2)
    {
        return;
    }

    public void d(android.view.View p2, boolean p3)
    {
        if ((p2 instanceof android.support.v4.view.bt)) {
            ((android.support.v4.view.bt) p2).setNestedScrollingEnabled(p3);
        }
        return;
    }

    public int e(android.view.View p2)
    {
        return 0;
    }

    public void e(android.view.View p1, float p2)
    {
        return;
    }

    public void e(android.view.View p1, int p2)
    {
        return;
    }

    public void f(android.view.View p1, float p2)
    {
        return;
    }

    public void f(android.view.View p1, int p2)
    {
        return;
    }

    public boolean f(android.view.View p2)
    {
        return 1;
    }

    public android.support.v4.view.a.aq g(android.view.View p2)
    {
        return 0;
    }

    public void g(android.view.View p1, float p2)
    {
        return;
    }

    public void g(android.view.View p1, int p2)
    {
        return;
    }

    public float h(android.view.View p2)
    {
        return 1065353216;
    }

    public void h(android.view.View p1, float p2)
    {
        return;
    }

    public boolean h(android.view.View p2, int p3)
    {
        int v0_1;
        if (!(p2 instanceof android.support.v4.view.bt)) {
            v0_1 = 0;
        } else {
            v0_1 = ((android.support.v4.view.bt) p2).startNestedScroll(p3);
        }
        return v0_1;
    }

    public int i(android.view.View p2)
    {
        return 0;
    }

    public void i(android.view.View p1, float p2)
    {
        return;
    }

    public int j(android.view.View p2)
    {
        return 0;
    }

    public void j(android.view.View p1, float p2)
    {
        return;
    }

    public int k(android.view.View p2)
    {
        return 0;
    }

    public void k(android.view.View p1, float p2)
    {
        return;
    }

    public android.view.ViewParent l(android.view.View p2)
    {
        return p2.getParent();
    }

    public void l(android.view.View p1, float p2)
    {
        return;
    }

    public void m(android.view.View p1, float p2)
    {
        return;
    }

    public boolean m(android.view.View p4)
    {
        int v0 = 0;
        int v1_0 = p4.getBackground();
        if ((v1_0 != 0) && (v1_0.getOpacity() == -1)) {
            v0 = 1;
        }
        return v0;
    }

    public int n(android.view.View p2)
    {
        return p2.getMeasuredWidth();
    }

    public void n(android.view.View p1, float p2)
    {
        return;
    }

    public int o(android.view.View p2)
    {
        return p2.getMeasuredHeight();
    }

    public int p(android.view.View p2)
    {
        return 0;
    }

    public int q(android.view.View p2)
    {
        return 0;
    }

    public int r(android.view.View p2)
    {
        return p2.getPaddingLeft();
    }

    public int s(android.view.View p2)
    {
        return p2.getPaddingRight();
    }

    public final void t(android.view.View p4)
    {
        if (!this.d) {
            this.b();
        }
        if (this.b == null) {
            p4.onStartTemporaryDetach();
        } else {
            try {
                String v1_1 = new Object[0];
                this.b.invoke(p4, v1_1);
            } catch (Exception v0_3) {
                android.util.Log.d("ViewCompat", "Error calling dispatchStartTemporaryDetach", v0_3);
            }
        }
        return;
    }

    public final void u(android.view.View p4)
    {
        if (!this.d) {
            this.b();
        }
        if (this.c == null) {
            p4.onFinishTemporaryDetach();
        } else {
            try {
                String v1_1 = new Object[0];
                this.c.invoke(p4, v1_1);
            } catch (Exception v0_3) {
                android.util.Log.d("ViewCompat", "Error calling dispatchFinishTemporaryDetach", v0_3);
            }
        }
        return;
    }

    public boolean v(android.view.View p2)
    {
        return 1;
    }

    public float w(android.view.View p2)
    {
        return 0;
    }

    public float x(android.view.View p2)
    {
        return 0;
    }

    public float y(android.view.View p2)
    {
        return 0;
    }

    public float z(android.view.View p2)
    {
        return 0;
    }
}
