package android.support.v4.view;
public final class bu {
    public boolean a;
    private final android.view.View b;
    private android.view.ViewParent c;
    private int[] d;

    public bu(android.view.View p1)
    {
        this.b = p1;
        return;
    }

    private boolean c()
    {
        return this.a;
    }

    private void d()
    {
        android.support.v4.view.cx.A(this.b);
        return;
    }

    private void e()
    {
        android.support.v4.view.cx.A(this.b);
        return;
    }

    public final void a(boolean p2)
    {
        if (this.a) {
            android.support.v4.view.cx.A(this.b);
        }
        this.a = p2;
        return;
    }

    public final boolean a()
    {
        int v0_1;
        if (this.c == null) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    public final boolean a(float p3, float p4)
    {
        if ((!this.a) || (this.c == null)) {
            boolean v0_2 = 0;
        } else {
            v0_2 = android.support.v4.view.fb.a(this.c, this.b, p3, p4);
        }
        return v0_2;
    }

    public final boolean a(float p3, float p4, boolean p5)
    {
        if ((!this.a) || (this.c == null)) {
            boolean v0_2 = 0;
        } else {
            v0_2 = android.support.v4.view.fb.a(this.c, this.b, p3, p4, p5);
        }
        return v0_2;
    }

    public final boolean a(int p5)
    {
        android.view.View v0_4;
        if (!this.a()) {
            if (this.a) {
                android.view.ViewParent v1 = this.b.getParent();
                android.view.View v0_3 = this.b;
                while (v1 != null) {
                    if (!android.support.v4.view.fb.a(v1, v0_3, this.b, p5)) {
                        if ((v1 instanceof android.view.View)) {
                            v0_3 = ((android.view.View) v1);
                        }
                        v1 = v1.getParent();
                    } else {
                        this.c = v1;
                        android.support.v4.view.fb.b(v1, v0_3, this.b, p5);
                        v0_4 = 1;
                    }
                    return v0_4;
                }
            }
            v0_4 = 0;
        } else {
            v0_4 = 1;
        }
        return v0_4;
    }

    public final boolean a(int p11, int p12, int p13, int p14, int[] p15)
    {
        int v7 = 0;
        if ((this.a) && (this.c != null)) {
            if ((p11 == 0) && ((p12 == 0) && ((p13 == 0) && (p14 == 0)))) {
                if (p15 != null) {
                    p15[0] = 0;
                    p15[1] = 0;
                }
            } else {
                int v8;
                int v6;
                if (p15 == null) {
                    v6 = 0;
                    v8 = 0;
                } else {
                    this.b.getLocationInWindow(p15);
                    v6 = p15[1];
                    v8 = p15[0];
                }
                android.support.v4.view.fb.a(this.c, this.b, p11, p12, p13, p14);
                if (p15 != null) {
                    this.b.getLocationInWindow(p15);
                    p15[0] = (p15[0] - v8);
                    p15[1] = (p15[1] - v6);
                }
                v7 = 1;
            }
        }
        return v7;
    }

    public final boolean a(int p7, int p8, int[] p9, int[] p10)
    {
        int v0 = 0;
        if ((this.a) && (this.c != null)) {
            if ((p7 == 0) && (p8 == 0)) {
                if (p10 != null) {
                    p10[0] = 0;
                    p10[1] = 0;
                }
            } else {
                int v2_2;
                int v3_0;
                if (p10 == null) {
                    v2_2 = 0;
                    v3_0 = 0;
                } else {
                    this.b.getLocationInWindow(p10);
                    v3_0 = p10[0];
                    v2_2 = p10[1];
                }
                if (p9 == null) {
                    if (this.d == null) {
                        int v4_2 = new int[2];
                        this.d = v4_2;
                    }
                    p9 = this.d;
                }
                p9[0] = 0;
                p9[1] = 0;
                android.support.v4.view.fb.a(this.c, this.b, p7, p8, p9);
                if (p10 != null) {
                    this.b.getLocationInWindow(p10);
                    p10[0] = (p10[0] - v3_0);
                    p10[1] = (p10[1] - v2_2);
                }
                if ((p9[0] != 0) || (p9[1] != 0)) {
                    v0 = 1;
                }
            }
        }
        return v0;
    }

    public final void b()
    {
        if (this.c != null) {
            android.support.v4.view.fb.a(this.c, this.b);
            this.c = 0;
        }
        return;
    }
}
