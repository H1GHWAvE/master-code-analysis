package android.support.v4.view;
final class bc extends android.support.v4.view.bb {

    bc()
    {
        return;
    }

    public final android.view.MenuItem a(android.view.MenuItem p2, android.support.v4.view.bf p3)
    {
        android.view.MenuItem v0_2;
        if (p3 != null) {
            v0_2 = android.support.v4.view.bh.a(p2, new android.support.v4.view.bd(this, p3));
        } else {
            v0_2 = android.support.v4.view.bh.a(p2, 0);
        }
        return v0_2;
    }

    public final boolean b(android.view.MenuItem p2)
    {
        return p2.expandActionView();
    }

    public final boolean c(android.view.MenuItem p2)
    {
        return p2.collapseActionView();
    }

    public final boolean d(android.view.MenuItem p2)
    {
        return p2.isActionViewExpanded();
    }
}
