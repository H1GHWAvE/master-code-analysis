package android.support.v4.view.a;
public final class bd {
    public static final android.support.v4.view.a.bg a;
    public final Object b;

    static bd()
    {
        if (android.os.Build$VERSION.SDK_INT < 16) {
            if (android.os.Build$VERSION.SDK_INT < 15) {
                if (android.os.Build$VERSION.SDK_INT < 14) {
                    android.support.v4.view.a.bd.a = new android.support.v4.view.a.bi();
                } else {
                    android.support.v4.view.a.bd.a = new android.support.v4.view.a.be();
                }
            } else {
                android.support.v4.view.a.bd.a = new android.support.v4.view.a.bf();
            }
        } else {
            android.support.v4.view.a.bd.a = new android.support.v4.view.a.bh();
        }
        return;
    }

    public bd(Object p1)
    {
        this.b = p1;
        return;
    }

    public static android.support.v4.view.a.bd a()
    {
        return new android.support.v4.view.a.bd(android.support.v4.view.a.bd.a.a());
    }

    private static android.support.v4.view.a.bd a(android.support.v4.view.a.bd p3)
    {
        return new android.support.v4.view.a.bd(android.support.v4.view.a.bd.a.a(p3.b));
    }

    private void a(int p3)
    {
        android.support.v4.view.a.bd.a.d(this.b, p3);
        return;
    }

    private void a(android.os.Parcelable p3)
    {
        android.support.v4.view.a.bd.a.a(this.b, p3);
        return;
    }

    private void a(android.view.View p3)
    {
        android.support.v4.view.a.bd.a.a(this.b, p3);
        return;
    }

    private void a(android.view.View p3, int p4)
    {
        android.support.v4.view.a.bd.a.a(this.b, p3, p4);
        return;
    }

    private void a(CharSequence p3)
    {
        android.support.v4.view.a.bd.a.b(this.b, p3);
        return;
    }

    private Object b()
    {
        return this.b;
    }

    private void b(int p3)
    {
        android.support.v4.view.a.bd.a.b(this.b, p3);
        return;
    }

    private void b(CharSequence p3)
    {
        android.support.v4.view.a.bd.a.a(this.b, p3);
        return;
    }

    private void b(boolean p3)
    {
        android.support.v4.view.a.bd.a.a(this.b, p3);
        return;
    }

    private android.support.v4.view.a.q c()
    {
        return android.support.v4.view.a.bd.a.m(this.b);
    }

    private void c(int p3)
    {
        android.support.v4.view.a.bd.a.c(this.b, p3);
        return;
    }

    private void c(CharSequence p3)
    {
        android.support.v4.view.a.bd.a.c(this.b, p3);
        return;
    }

    private void c(boolean p3)
    {
        android.support.v4.view.a.bd.a.b(this.b, p3);
        return;
    }

    private int d()
    {
        return android.support.v4.view.a.bd.a.p(this.b);
    }

    private void d(int p3)
    {
        android.support.v4.view.a.bd.a.h(this.b, p3);
        return;
    }

    private void d(boolean p3)
    {
        android.support.v4.view.a.bd.a.d(this.b, p3);
        return;
    }

    private void e(int p3)
    {
        android.support.v4.view.a.bd.a.f(this.b, p3);
        return;
    }

    private void e(boolean p3)
    {
        android.support.v4.view.a.bd.a.c(this.b, p3);
        return;
    }

    private boolean e()
    {
        return android.support.v4.view.a.bd.a.q(this.b);
    }

    private void f(int p3)
    {
        android.support.v4.view.a.bd.a.g(this.b, p3);
        return;
    }

    private boolean f()
    {
        return android.support.v4.view.a.bd.a.r(this.b);
    }

    private void g(int p3)
    {
        android.support.v4.view.a.bd.a.i(this.b, p3);
        return;
    }

    private boolean g()
    {
        return android.support.v4.view.a.bd.a.t(this.b);
    }

    private void h(int p3)
    {
        android.support.v4.view.a.bd.a.j(this.b, p3);
        return;
    }

    private boolean h()
    {
        return android.support.v4.view.a.bd.a.s(this.b);
    }

    private void i(int p3)
    {
        android.support.v4.view.a.bd.a.a(this.b, p3);
        return;
    }

    private boolean i()
    {
        return android.support.v4.view.a.bd.a.u(this.b);
    }

    private int j()
    {
        return android.support.v4.view.a.bd.a.h(this.b);
    }

    private void j(int p3)
    {
        android.support.v4.view.a.bd.a.e(this.b, p3);
        return;
    }

    private int k()
    {
        return android.support.v4.view.a.bd.a.f(this.b);
    }

    private int l()
    {
        return android.support.v4.view.a.bd.a.g(this.b);
    }

    private int m()
    {
        return android.support.v4.view.a.bd.a.o(this.b);
    }

    private int n()
    {
        return android.support.v4.view.a.bd.a.k(this.b);
    }

    private int o()
    {
        return android.support.v4.view.a.bd.a.l(this.b);
    }

    private int p()
    {
        return android.support.v4.view.a.bd.a.w(this.b);
    }

    private int q()
    {
        return android.support.v4.view.a.bd.a.x(this.b);
    }

    private int r()
    {
        return android.support.v4.view.a.bd.a.b(this.b);
    }

    private int s()
    {
        return android.support.v4.view.a.bd.a.j(this.b);
    }

    private CharSequence t()
    {
        return android.support.v4.view.a.bd.a.d(this.b);
    }

    private java.util.List u()
    {
        return android.support.v4.view.a.bd.a.n(this.b);
    }

    private CharSequence v()
    {
        return android.support.v4.view.a.bd.a.c(this.b);
    }

    private CharSequence w()
    {
        return android.support.v4.view.a.bd.a.e(this.b);
    }

    private android.os.Parcelable x()
    {
        return android.support.v4.view.a.bd.a.i(this.b);
    }

    private void y()
    {
        android.support.v4.view.a.bd.a.v(this.b);
        return;
    }

    public final void a(boolean p3)
    {
        android.support.v4.view.a.bd.a.e(this.b, p3);
        return;
    }

    public final boolean equals(Object p5)
    {
        int v0 = 1;
        if (this != p5) {
            if (p5 != null) {
                if (this.getClass() == p5.getClass()) {
                    if (this.b != null) {
                        if (!this.b.equals(((android.support.v4.view.a.bd) p5).b)) {
                            v0 = 0;
                        }
                    } else {
                        if (((android.support.v4.view.a.bd) p5).b != null) {
                            v0 = 0;
                        }
                    }
                } else {
                    v0 = 0;
                }
            } else {
                v0 = 0;
            }
        }
        return v0;
    }

    public final int hashCode()
    {
        int v0_2;
        if (this.b != null) {
            v0_2 = this.b.hashCode();
        } else {
            v0_2 = 0;
        }
        return v0_2;
    }
}
