package android.support.v4.view.a;
final class u extends android.support.v4.view.a.t {

    u()
    {
        return;
    }

    public final void b(Object p1, android.view.View p2)
    {
        ((android.view.accessibility.AccessibilityNodeInfo) p1).setTraversalBefore(p2);
        return;
    }

    public final void b(Object p1, android.view.View p2, int p3)
    {
        ((android.view.accessibility.AccessibilityNodeInfo) p1).setTraversalBefore(p2, p3);
        return;
    }

    public final void c(Object p1, android.view.View p2)
    {
        ((android.view.accessibility.AccessibilityNodeInfo) p1).setTraversalAfter(p2);
        return;
    }

    public final void c(Object p1, android.view.View p2, int p3)
    {
        ((android.view.accessibility.AccessibilityNodeInfo) p1).setTraversalAfter(p2, p3);
        return;
    }

    public final Object h(Object p2)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo) p2).getTraversalBefore();
    }

    public final Object i(Object p2)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo) p2).getTraversalAfter();
    }
}
