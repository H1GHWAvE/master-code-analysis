package android.support.v4.view.a;
public final class q {
    public static final String A = "ACTION_ARGUMENT_EXTEND_SELECTION_BOOLEAN";
    public static final String B = "ACTION_ARGUMENT_SELECTION_START_INT";
    public static final String C = "ACTION_ARGUMENT_SELECTION_END_INT";
    public static final String D = "ACTION_ARGUMENT_SET_TEXT_CHARSEQUENCE";
    public static final int E = 1;
    public static final int F = 2;
    public static final int G = 1;
    public static final int H = 2;
    public static final int I = 4;
    public static final int J = 8;
    public static final int K = 16;
    public static final android.support.v4.view.a.w a = None;
    public static final int c = 1;
    public static final int d = 2;
    public static final int e = 4;
    public static final int f = 8;
    public static final int g = 16;
    public static final int h = 32;
    public static final int i = 64;
    public static final int j = 128;
    public static final int k = 256;
    public static final int l = 512;
    public static final int m = 1024;
    public static final int n = 2048;
    public static final int o = 4096;
    public static final int p = 8192;
    public static final int q = 16384;
    public static final int r = 32768;
    public static final int s = 65536;
    public static final int t = 131072;
    public static final int u = 262144;
    public static final int v = 524288;
    public static final int w = 1048576;
    public static final int x = 2097152;
    public static final String y = "ACTION_ARGUMENT_MOVEMENT_GRANULARITY_INT";
    public static final String z = "ACTION_ARGUMENT_HTML_ELEMENT_STRING";
    public final Object b;

    static q()
    {
        if (android.os.Build$VERSION.SDK_INT < 22) {
            if (android.os.Build$VERSION.SDK_INT < 21) {
                if (android.os.Build$VERSION.SDK_INT < 19) {
                    if (android.os.Build$VERSION.SDK_INT < 18) {
                        if (android.os.Build$VERSION.SDK_INT < 17) {
                            if (android.os.Build$VERSION.SDK_INT < 16) {
                                if (android.os.Build$VERSION.SDK_INT < 14) {
                                    android.support.v4.view.a.q.a = new android.support.v4.view.a.ab();
                                } else {
                                    android.support.v4.view.a.q.a = new android.support.v4.view.a.v();
                                }
                            } else {
                                android.support.v4.view.a.q.a = new android.support.v4.view.a.x();
                            }
                        } else {
                            android.support.v4.view.a.q.a = new android.support.v4.view.a.y();
                        }
                    } else {
                        android.support.v4.view.a.q.a = new android.support.v4.view.a.z();
                    }
                } else {
                    android.support.v4.view.a.q.a = new android.support.v4.view.a.aa();
                }
            } else {
                android.support.v4.view.a.q.a = new android.support.v4.view.a.t();
            }
        } else {
            android.support.v4.view.a.q.a = new android.support.v4.view.a.u();
        }
        return;
    }

    public q(Object p1)
    {
        this.b = p1;
        return;
    }

    private int A()
    {
        return android.support.v4.view.a.q.a.H(this.b);
    }

    private android.support.v4.view.a.ac B()
    {
        android.support.v4.view.a.ac v0_2;
        Object v1_1 = android.support.v4.view.a.q.a.I(this.b);
        if (v1_1 != null) {
            v0_2 = new android.support.v4.view.a.ac(v1_1, 0);
        } else {
            v0_2 = 0;
        }
        return v0_2;
    }

    private android.support.v4.view.a.ad C()
    {
        android.support.v4.view.a.ad v0_2;
        Object v1_1 = android.support.v4.view.a.q.a.J(this.b);
        if (v1_1 != null) {
            v0_2 = new android.support.v4.view.a.ad(v1_1, 0);
        } else {
            v0_2 = 0;
        }
        return v0_2;
    }

    private android.support.v4.view.a.ae D()
    {
        android.support.v4.view.a.ae v0_2;
        Object v1_1 = android.support.v4.view.a.q.a.K(this.b);
        if (v1_1 != null) {
            v0_2 = new android.support.v4.view.a.ae(v1_1, 0);
        } else {
            v0_2 = 0;
        }
        return v0_2;
    }

    private java.util.List E()
    {
        java.util.List v0_1;
        java.util.List v3 = android.support.v4.view.a.q.a.a(this.b);
        if (v3 == null) {
            v0_1 = java.util.Collections.emptyList();
        } else {
            v0_1 = new java.util.ArrayList();
            int v4 = v3.size();
            int v1_1 = 0;
            while (v1_1 < v4) {
                v0_1.add(new android.support.v4.view.a.s(v3.get(v1_1), 0));
                v1_1++;
            }
        }
        return v0_1;
    }

    private void F()
    {
        android.support.v4.view.a.q.a.T(this.b);
        return;
    }

    private boolean G()
    {
        return android.support.v4.view.a.q.a.U(this.b);
    }

    private CharSequence H()
    {
        return android.support.v4.view.a.q.a.e(this.b);
    }

    private android.support.v4.view.a.q I()
    {
        return android.support.v4.view.a.q.a(android.support.v4.view.a.q.a.V(this.b));
    }

    private android.support.v4.view.a.q J()
    {
        return android.support.v4.view.a.q.a(android.support.v4.view.a.q.a.W(this.b));
    }

    private boolean K()
    {
        return android.support.v4.view.a.q.a.X(this.b);
    }

    private android.os.Bundle L()
    {
        return android.support.v4.view.a.q.a.Y(this.b);
    }

    private int M()
    {
        return android.support.v4.view.a.q.a.Z(this.b);
    }

    private int N()
    {
        return android.support.v4.view.a.q.a.f(this.b);
    }

    private int O()
    {
        return android.support.v4.view.a.q.a.aa(this.b);
    }

    private int P()
    {
        return android.support.v4.view.a.q.a.ab(this.b);
    }

    private android.support.v4.view.a.q Q()
    {
        return android.support.v4.view.a.q.a(android.support.v4.view.a.q.a.h(this.b));
    }

    private android.support.v4.view.a.q R()
    {
        return android.support.v4.view.a.q.a(android.support.v4.view.a.q.a.i(this.b));
    }

    private android.support.v4.view.a.bm S()
    {
        return android.support.v4.view.a.bm.a(android.support.v4.view.a.q.a.g(this.b));
    }

    private boolean T()
    {
        return android.support.v4.view.a.q.a.ac(this.b);
    }

    private boolean U()
    {
        return android.support.v4.view.a.q.a.ad(this.b);
    }

    private boolean V()
    {
        return android.support.v4.view.a.q.a.ae(this.b);
    }

    private boolean W()
    {
        return android.support.v4.view.a.q.a.af(this.b);
    }

    public static android.support.v4.view.a.q a()
    {
        return android.support.v4.view.a.q.a(android.support.v4.view.a.q.a.a());
    }

    public static android.support.v4.view.a.q a(android.support.v4.view.a.q p2)
    {
        return android.support.v4.view.a.q.a(android.support.v4.view.a.q.a.j(p2.b));
    }

    public static android.support.v4.view.a.q a(android.view.View p1)
    {
        return android.support.v4.view.a.q.a(android.support.v4.view.a.q.a.a(p1));
    }

    private static android.support.v4.view.a.q a(android.view.View p1, int p2)
    {
        return android.support.v4.view.a.q.a(android.support.v4.view.a.q.a.a(p1, p2));
    }

    static android.support.v4.view.a.q a(Object p1)
    {
        int v0_0;
        if (p1 == null) {
            v0_0 = 0;
        } else {
            v0_0 = new android.support.v4.view.a.q(p1);
        }
        return v0_0;
    }

    private java.util.List a(String p7)
    {
        java.util.ArrayList v1_1 = new java.util.ArrayList();
        java.util.List v2_1 = android.support.v4.view.a.q.a.a(this.b, p7);
        int v3 = v2_1.size();
        int v0_1 = 0;
        while (v0_1 < v3) {
            v1_1.add(new android.support.v4.view.a.q(v2_1.get(v0_1)));
            v0_1++;
        }
        return v1_1;
    }

    private void a(int p3, int p4)
    {
        android.support.v4.view.a.q.a.a(this.b, p3, p4);
        return;
    }

    private void a(android.support.v4.view.a.ae p4)
    {
        android.support.v4.view.a.q.a.e(this.b, p4.d);
        return;
    }

    private boolean a(int p3, android.os.Bundle p4)
    {
        return android.support.v4.view.a.q.a.a(this.b, p3, p4);
    }

    private android.support.v4.view.a.q b(int p3)
    {
        return android.support.v4.view.a.q.a(android.support.v4.view.a.q.a.e(this.b, p3));
    }

    private void b(android.support.v4.view.a.s p4)
    {
        android.support.v4.view.a.q.a.a(this.b, android.support.v4.view.a.s.a(p4));
        return;
    }

    private void b(android.view.View p3, int p4)
    {
        android.support.v4.view.a.q.a.d(this.b, p3, p4);
        return;
    }

    private void b(Object p4)
    {
        android.support.v4.view.a.q.a.c(this.b, ((android.support.v4.view.a.ac) p4).d);
        return;
    }

    private void b(String p3)
    {
        android.support.v4.view.a.q.a.b(this.b, p3);
        return;
    }

    private android.support.v4.view.a.q c(int p3)
    {
        return android.support.v4.view.a.q.a(android.support.v4.view.a.q.a.f(this.b, p3));
    }

    private java.util.List c(String p5)
    {
        java.util.List v0_1;
        java.util.Iterator v1_1 = android.support.v4.view.a.q.a.c(this.b, p5);
        if (v1_1 == null) {
            v0_1 = java.util.Collections.emptyList();
        } else {
            v0_1 = new java.util.ArrayList();
            java.util.Iterator v1_2 = v1_1.iterator();
            while (v1_2.hasNext()) {
                v0_1.add(new android.support.v4.view.a.q(v1_2.next()));
            }
        }
        return v0_1;
    }

    private void c(android.view.View p3, int p4)
    {
        android.support.v4.view.a.q.a.e(this.b, p3, p4);
        return;
    }

    private void c(Object p4)
    {
        android.support.v4.view.a.q.a.d(this.b, ((android.support.v4.view.a.ad) p4).a);
        return;
    }

    private android.support.v4.view.a.q d(int p3)
    {
        return android.support.v4.view.a.q.a(android.support.v4.view.a.q.a.c(this.b, p3));
    }

    private void d(CharSequence p3)
    {
        android.support.v4.view.a.q.a.e(this.b, p3);
        return;
    }

    private boolean d(android.view.View p3, int p4)
    {
        return android.support.v4.view.a.q.a.a(this.b, p3, p4);
    }

    private void e(android.view.View p3, int p4)
    {
        android.support.v4.view.a.q.a.f(this.b, p3, p4);
        return;
    }

    private void e(CharSequence p3)
    {
        android.support.v4.view.a.q.a.a(this.b, p3);
        return;
    }

    private boolean e(int p3)
    {
        return android.support.v4.view.a.q.a.d(this.b, p3);
    }

    private boolean e(android.view.View p3)
    {
        return android.support.v4.view.a.q.a.a(this.b, p3);
    }

    private void f(int p3)
    {
        android.support.v4.view.a.q.a.g(this.b, p3);
        return;
    }

    private void f(android.view.View p3)
    {
        android.support.v4.view.a.q.a.g(this.b, p3);
        return;
    }

    private void f(android.view.View p3, int p4)
    {
        android.support.v4.view.a.q.a.g(this.b, p3, p4);
        return;
    }

    private void g(int p3)
    {
        android.support.v4.view.a.q.a.h(this.b, p3);
        return;
    }

    private void g(android.view.View p3)
    {
        android.support.v4.view.a.q.a.h(this.b, p3);
        return;
    }

    private void g(android.view.View p3, int p4)
    {
        android.support.v4.view.a.q.a.h(this.b, p3, p4);
        return;
    }

    private void h(int p3)
    {
        android.support.v4.view.a.q.a.i(this.b, p3);
        return;
    }

    private void h(android.view.View p3)
    {
        android.support.v4.view.a.q.a.b(this.b, p3);
        return;
    }

    private void h(android.view.View p3, int p4)
    {
        android.support.v4.view.a.q.a.b(this.b, p3, p4);
        return;
    }

    private void i(int p3)
    {
        android.support.v4.view.a.q.a.a(this.b, p3);
        return;
    }

    private void i(android.view.View p3)
    {
        android.support.v4.view.a.q.a.c(this.b, p3);
        return;
    }

    private void i(android.view.View p3, int p4)
    {
        android.support.v4.view.a.q.a.c(this.b, p3, p4);
        return;
    }

    private static String j(int p1)
    {
        String v0;
        switch (p1) {
            case 1:
                v0 = "ACTION_FOCUS";
                break;
            case 2:
                v0 = "ACTION_CLEAR_FOCUS";
                break;
            case 4:
                v0 = "ACTION_SELECT";
                break;
            case 8:
                v0 = "ACTION_CLEAR_SELECTION";
                break;
            case 16:
                v0 = "ACTION_CLICK";
                break;
            case 32:
                v0 = "ACTION_LONG_CLICK";
                break;
            case 64:
                v0 = "ACTION_ACCESSIBILITY_FOCUS";
                break;
            case 128:
                v0 = "ACTION_CLEAR_ACCESSIBILITY_FOCUS";
                break;
            case 256:
                v0 = "ACTION_NEXT_AT_MOVEMENT_GRANULARITY";
                break;
            case 512:
                v0 = "ACTION_PREVIOUS_AT_MOVEMENT_GRANULARITY";
                break;
            case 1024:
                v0 = "ACTION_NEXT_HTML_ELEMENT";
                break;
            case 2048:
                v0 = "ACTION_PREVIOUS_HTML_ELEMENT";
                break;
            case 4096:
                v0 = "ACTION_SCROLL_FORWARD";
                break;
            case 8192:
                v0 = "ACTION_SCROLL_BACKWARD";
                break;
            case 16384:
                v0 = "ACTION_COPY";
                break;
            case 32768:
                v0 = "ACTION_PASTE";
                break;
            case 65536:
                v0 = "ACTION_CUT";
                break;
            case 131072:
                v0 = "ACTION_SET_SELECTION";
                break;
            default:
                v0 = "ACTION_UNKNOWN";
        }
        return v0;
    }

    private void j(boolean p3)
    {
        android.support.v4.view.a.q.a.a(this.b, p3);
        return;
    }

    private void k(boolean p3)
    {
        android.support.v4.view.a.q.a.b(this.b, p3);
        return;
    }

    private void l(boolean p3)
    {
        android.support.v4.view.a.q.a.h(this.b, p3);
        return;
    }

    private void m(boolean p3)
    {
        android.support.v4.view.a.q.a.m(this.b, p3);
        return;
    }

    private void n(boolean p3)
    {
        android.support.v4.view.a.q.a.n(this.b, p3);
        return;
    }

    private void o(boolean p3)
    {
        android.support.v4.view.a.q.a.o(this.b, p3);
        return;
    }

    static synthetic android.support.v4.view.a.w p()
    {
        return android.support.v4.view.a.q.a;
    }

    private void p(boolean p3)
    {
        android.support.v4.view.a.q.a.p(this.b, p3);
        return;
    }

    private Object q()
    {
        return this.b;
    }

    private int r()
    {
        return android.support.v4.view.a.q.a.r(this.b);
    }

    private int s()
    {
        return android.support.v4.view.a.q.a.l(this.b);
    }

    private int t()
    {
        return android.support.v4.view.a.q.a.D(this.b);
    }

    private android.support.v4.view.a.q u()
    {
        return android.support.v4.view.a.q.a(android.support.v4.view.a.q.a.p(this.b));
    }

    private boolean v()
    {
        return android.support.v4.view.a.q.a.s(this.b);
    }

    private boolean w()
    {
        return android.support.v4.view.a.q.a.t(this.b);
    }

    private boolean x()
    {
        return android.support.v4.view.a.q.a.z(this.b);
    }

    private boolean y()
    {
        return android.support.v4.view.a.q.a.A(this.b);
    }

    private String z()
    {
        return android.support.v4.view.a.q.a.G(this.b);
    }

    public final void a(int p3)
    {
        android.support.v4.view.a.q.a.b(this.b, p3);
        return;
    }

    public final void a(android.graphics.Rect p3)
    {
        android.support.v4.view.a.q.a.a(this.b, p3);
        return;
    }

    public final void a(CharSequence p3)
    {
        android.support.v4.view.a.q.a.d(this.b, p3);
        return;
    }

    public final void a(boolean p3)
    {
        android.support.v4.view.a.q.a.e(this.b, p3);
        return;
    }

    public final boolean a(android.support.v4.view.a.s p4)
    {
        return android.support.v4.view.a.q.a.b(this.b, android.support.v4.view.a.s.a(p4));
    }

    public final int b()
    {
        return android.support.v4.view.a.q.a.k(this.b);
    }

    public final void b(android.graphics.Rect p3)
    {
        android.support.v4.view.a.q.a.c(this.b, p3);
        return;
    }

    public final void b(android.view.View p3)
    {
        android.support.v4.view.a.q.a.f(this.b, p3);
        return;
    }

    public final void b(CharSequence p3)
    {
        android.support.v4.view.a.q.a.b(this.b, p3);
        return;
    }

    public final void b(boolean p3)
    {
        android.support.v4.view.a.q.a.f(this.b, p3);
        return;
    }

    public final void c(android.graphics.Rect p3)
    {
        android.support.v4.view.a.q.a.b(this.b, p3);
        return;
    }

    public final void c(android.view.View p3)
    {
        android.support.v4.view.a.q.a.d(this.b, p3);
        return;
    }

    public final void c(CharSequence p3)
    {
        android.support.v4.view.a.q.a.c(this.b, p3);
        return;
    }

    public final void c(boolean p3)
    {
        android.support.v4.view.a.q.a.k(this.b, p3);
        return;
    }

    public final boolean c()
    {
        return android.support.v4.view.a.q.a.w(this.b);
    }

    public final void d(android.graphics.Rect p3)
    {
        android.support.v4.view.a.q.a.d(this.b, p3);
        return;
    }

    public final void d(android.view.View p3)
    {
        android.support.v4.view.a.q.a.e(this.b, p3);
        return;
    }

    public final void d(boolean p3)
    {
        android.support.v4.view.a.q.a.l(this.b, p3);
        return;
    }

    public final boolean d()
    {
        return android.support.v4.view.a.q.a.x(this.b);
    }

    public final void e(boolean p3)
    {
        android.support.v4.view.a.q.a.j(this.b, p3);
        return;
    }

    public final boolean e()
    {
        return android.support.v4.view.a.q.a.E(this.b);
    }

    public final boolean equals(Object p5)
    {
        int v0 = 1;
        if (this != p5) {
            if (p5 != null) {
                if (this.getClass() == p5.getClass()) {
                    if (this.b != null) {
                        if (!this.b.equals(((android.support.v4.view.a.q) p5).b)) {
                            v0 = 0;
                        }
                    } else {
                        if (((android.support.v4.view.a.q) p5).b != null) {
                            v0 = 0;
                        }
                    }
                } else {
                    v0 = 0;
                }
            } else {
                v0 = 0;
            }
        }
        return v0;
    }

    public final void f(boolean p3)
    {
        android.support.v4.view.a.q.a.c(this.b, p3);
        return;
    }

    public final boolean f()
    {
        return android.support.v4.view.a.q.a.F(this.b);
    }

    public final void g(boolean p3)
    {
        android.support.v4.view.a.q.a.g(this.b, p3);
        return;
    }

    public final boolean g()
    {
        return android.support.v4.view.a.q.a.B(this.b);
    }

    public final void h(boolean p3)
    {
        android.support.v4.view.a.q.a.d(this.b, p3);
        return;
    }

    public final boolean h()
    {
        return android.support.v4.view.a.q.a.u(this.b);
    }

    public final int hashCode()
    {
        int v0_2;
        if (this.b != null) {
            v0_2 = this.b.hashCode();
        } else {
            v0_2 = 0;
        }
        return v0_2;
    }

    public final void i(boolean p3)
    {
        android.support.v4.view.a.q.a.i(this.b, p3);
        return;
    }

    public final boolean i()
    {
        return android.support.v4.view.a.q.a.y(this.b);
    }

    public final boolean j()
    {
        return android.support.v4.view.a.q.a.v(this.b);
    }

    public final CharSequence k()
    {
        return android.support.v4.view.a.q.a.o(this.b);
    }

    public final CharSequence l()
    {
        return android.support.v4.view.a.q.a.m(this.b);
    }

    public final CharSequence m()
    {
        return android.support.v4.view.a.q.a.q(this.b);
    }

    public final CharSequence n()
    {
        return android.support.v4.view.a.q.a.n(this.b);
    }

    public final void o()
    {
        android.support.v4.view.a.q.a.C(this.b);
        return;
    }

    public final String toString()
    {
        StringBuilder v2_1 = new StringBuilder();
        v2_1.append(super.toString());
        int v0_2 = new android.graphics.Rect();
        this.a(v0_2);
        v2_1.append(new StringBuilder("; boundsInParent: ").append(v0_2).toString());
        this.c(v0_2);
        v2_1.append(new StringBuilder("; boundsInScreen: ").append(v0_2).toString());
        v2_1.append("; packageName: ").append(this.k());
        v2_1.append("; className: ").append(this.l());
        v2_1.append("; text: ").append(this.m());
        v2_1.append("; contentDescription: ").append(this.n());
        v2_1.append("; viewId: ").append(android.support.v4.view.a.q.a.G(this.b));
        v2_1.append("; checkable: ").append(android.support.v4.view.a.q.a.s(this.b));
        v2_1.append("; checked: ").append(android.support.v4.view.a.q.a.t(this.b));
        v2_1.append("; focusable: ").append(this.c());
        v2_1.append("; focused: ").append(this.d());
        v2_1.append("; selected: ").append(this.g());
        v2_1.append("; clickable: ").append(this.h());
        v2_1.append("; longClickable: ").append(this.i());
        v2_1.append("; enabled: ").append(this.j());
        v2_1.append("; password: ").append(android.support.v4.view.a.q.a.z(this.b));
        v2_1.append(new StringBuilder("; scrollable: ").append(android.support.v4.view.a.q.a.A(this.b)).toString());
        v2_1.append("; [");
        int v0_38 = this.b();
        while (v0_38 != 0) {
            int v0_41;
            int v3_8 = (1 << Integer.numberOfTrailingZeros(v0_38));
            int v1_29 = ((v3_8 ^ -1) & v0_38);
            switch (v3_8) {
                case 1:
                    v0_41 = "ACTION_FOCUS";
                    break;
                case 2:
                    v0_41 = "ACTION_CLEAR_FOCUS";
                    break;
                case 4:
                    v0_41 = "ACTION_SELECT";
                    break;
                case 8:
                    v0_41 = "ACTION_CLEAR_SELECTION";
                    break;
                case 16:
                    v0_41 = "ACTION_CLICK";
                    break;
                case 32:
                    v0_41 = "ACTION_LONG_CLICK";
                    break;
                case 64:
                    v0_41 = "ACTION_ACCESSIBILITY_FOCUS";
                    break;
                case 128:
                    v0_41 = "ACTION_CLEAR_ACCESSIBILITY_FOCUS";
                    break;
                case 256:
                    v0_41 = "ACTION_NEXT_AT_MOVEMENT_GRANULARITY";
                    break;
                case 512:
                    v0_41 = "ACTION_PREVIOUS_AT_MOVEMENT_GRANULARITY";
                    break;
                case 1024:
                    v0_41 = "ACTION_NEXT_HTML_ELEMENT";
                    break;
                case 2048:
                    v0_41 = "ACTION_PREVIOUS_HTML_ELEMENT";
                    break;
                case 4096:
                    v0_41 = "ACTION_SCROLL_FORWARD";
                    break;
                case 8192:
                    v0_41 = "ACTION_SCROLL_BACKWARD";
                    break;
                case 16384:
                    v0_41 = "ACTION_COPY";
                    break;
                case 32768:
                    v0_41 = "ACTION_PASTE";
                    break;
                case 65536:
                    v0_41 = "ACTION_CUT";
                    break;
                case 131072:
                    v0_41 = "ACTION_SET_SELECTION";
                    break;
                default:
                    v0_41 = "ACTION_UNKNOWN";
            }
            v2_1.append(v0_41);
            if (v1_29 != 0) {
                v2_1.append(", ");
            }
            v0_38 = v1_29;
        }
        v2_1.append("]");
        return v2_1.toString();
    }
}
