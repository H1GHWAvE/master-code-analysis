package android.support.v4.view.a;
public final class a {
    public static final int a = 128;
    public static final int b = 256;
    public static final int c = 512;
    public static final int d = 1024;
    public static final int e = 2048;
    public static final int f = 4096;
    public static final int g = 8192;
    public static final int h = 16384;
    public static final int i = 32768;
    public static final int j = 65536;
    public static final int k = 131072;
    public static final int l = 262144;
    public static final int m = 524288;
    public static final int n = 1048576;
    public static final int o = 2097152;
    public static final int p = 0;
    public static final int q = 1;
    public static final int r = 2;
    public static final int s = 4;
    public static final int t = 255;
    private static final android.support.v4.view.a.e u;

    static a()
    {
        if (android.os.Build$VERSION.SDK_INT < 19) {
            if (android.os.Build$VERSION.SDK_INT < 14) {
                android.support.v4.view.a.a.u = new android.support.v4.view.a.d();
            } else {
                android.support.v4.view.a.a.u = new android.support.v4.view.a.b();
            }
        } else {
            android.support.v4.view.a.a.u = new android.support.v4.view.a.c();
        }
        return;
    }

    private a()
    {
        return;
    }

    public static android.support.v4.view.a.bd a(android.view.accessibility.AccessibilityEvent p1)
    {
        return new android.support.v4.view.a.bd(p1);
    }

    private static android.support.v4.view.a.bd a(android.view.accessibility.AccessibilityEvent p2, int p3)
    {
        return new android.support.v4.view.a.bd(android.support.v4.view.a.a.u.a(p2, p3));
    }

    private static void a(android.view.accessibility.AccessibilityEvent p2, android.support.v4.view.a.bd p3)
    {
        android.support.v4.view.a.a.u.a(p2, p3.b);
        return;
    }

    private static int b(android.view.accessibility.AccessibilityEvent p1)
    {
        return android.support.v4.view.a.a.u.a(p1);
    }

    private static void b(android.view.accessibility.AccessibilityEvent p1, int p2)
    {
        android.support.v4.view.a.a.u.b(p1, p2);
        return;
    }

    private static int c(android.view.accessibility.AccessibilityEvent p1)
    {
        return android.support.v4.view.a.a.u.b(p1);
    }
}
