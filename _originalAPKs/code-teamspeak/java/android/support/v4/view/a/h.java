package android.support.v4.view.a;
public final class h {
    private static final android.support.v4.view.a.l a;

    static h()
    {
        if (android.os.Build$VERSION.SDK_INT < 14) {
            android.support.v4.view.a.h.a = new android.support.v4.view.a.k();
        } else {
            android.support.v4.view.a.h.a = new android.support.v4.view.a.i();
        }
        return;
    }

    public h()
    {
        return;
    }

    static synthetic android.support.v4.view.a.l a()
    {
        return android.support.v4.view.a.h.a;
    }

    private static java.util.List a(android.view.accessibility.AccessibilityManager p1, int p2)
    {
        return android.support.v4.view.a.h.a.a(p1, p2);
    }

    public static boolean a(android.view.accessibility.AccessibilityManager p1)
    {
        return android.support.v4.view.a.h.a.b(p1);
    }

    private static boolean a(android.view.accessibility.AccessibilityManager p1, android.support.v4.view.a.m p2)
    {
        return android.support.v4.view.a.h.a.a(p1, p2);
    }

    private static java.util.List b(android.view.accessibility.AccessibilityManager p1)
    {
        return android.support.v4.view.a.h.a.a(p1);
    }

    private static boolean b(android.view.accessibility.AccessibilityManager p1, android.support.v4.view.a.m p2)
    {
        return android.support.v4.view.a.h.a.b(p1, p2);
    }
}
