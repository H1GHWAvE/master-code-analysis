package android.support.v4.view.a;
final class bj {

    bj()
    {
        return;
    }

    private static Object a()
    {
        return android.view.accessibility.AccessibilityRecord.obtain();
    }

    private static Object a(Object p1)
    {
        return android.view.accessibility.AccessibilityRecord.obtain(((android.view.accessibility.AccessibilityRecord) p1));
    }

    private static void a(Object p0, int p1)
    {
        ((android.view.accessibility.AccessibilityRecord) p0).setAddedCount(p1);
        return;
    }

    private static void a(Object p0, android.os.Parcelable p1)
    {
        ((android.view.accessibility.AccessibilityRecord) p0).setParcelableData(p1);
        return;
    }

    private static void a(Object p0, android.view.View p1)
    {
        ((android.view.accessibility.AccessibilityRecord) p0).setSource(p1);
        return;
    }

    private static void a(Object p0, CharSequence p1)
    {
        ((android.view.accessibility.AccessibilityRecord) p0).setBeforeText(p1);
        return;
    }

    private static void a(Object p0, boolean p1)
    {
        ((android.view.accessibility.AccessibilityRecord) p0).setChecked(p1);
        return;
    }

    private static int b(Object p1)
    {
        return ((android.view.accessibility.AccessibilityRecord) p1).getAddedCount();
    }

    private static void b(Object p0, int p1)
    {
        ((android.view.accessibility.AccessibilityRecord) p0).setCurrentItemIndex(p1);
        return;
    }

    private static void b(Object p0, CharSequence p1)
    {
        ((android.view.accessibility.AccessibilityRecord) p0).setClassName(p1);
        return;
    }

    private static void b(Object p0, boolean p1)
    {
        ((android.view.accessibility.AccessibilityRecord) p0).setEnabled(p1);
        return;
    }

    private static CharSequence c(Object p1)
    {
        return ((android.view.accessibility.AccessibilityRecord) p1).getBeforeText();
    }

    private static void c(Object p0, int p1)
    {
        ((android.view.accessibility.AccessibilityRecord) p0).setFromIndex(p1);
        return;
    }

    private static void c(Object p0, CharSequence p1)
    {
        ((android.view.accessibility.AccessibilityRecord) p0).setContentDescription(p1);
        return;
    }

    private static void c(Object p0, boolean p1)
    {
        ((android.view.accessibility.AccessibilityRecord) p0).setFullScreen(p1);
        return;
    }

    private static CharSequence d(Object p1)
    {
        return ((android.view.accessibility.AccessibilityRecord) p1).getClassName();
    }

    private static void d(Object p0, int p1)
    {
        ((android.view.accessibility.AccessibilityRecord) p0).setItemCount(p1);
        return;
    }

    private static void d(Object p0, boolean p1)
    {
        ((android.view.accessibility.AccessibilityRecord) p0).setPassword(p1);
        return;
    }

    private static CharSequence e(Object p1)
    {
        return ((android.view.accessibility.AccessibilityRecord) p1).getContentDescription();
    }

    private static void e(Object p0, int p1)
    {
        ((android.view.accessibility.AccessibilityRecord) p0).setRemovedCount(p1);
        return;
    }

    private static void e(Object p0, boolean p1)
    {
        ((android.view.accessibility.AccessibilityRecord) p0).setScrollable(p1);
        return;
    }

    private static int f(Object p1)
    {
        return ((android.view.accessibility.AccessibilityRecord) p1).getCurrentItemIndex();
    }

    private static void f(Object p0, int p1)
    {
        ((android.view.accessibility.AccessibilityRecord) p0).setScrollX(p1);
        return;
    }

    private static int g(Object p1)
    {
        return ((android.view.accessibility.AccessibilityRecord) p1).getFromIndex();
    }

    private static void g(Object p0, int p1)
    {
        ((android.view.accessibility.AccessibilityRecord) p0).setScrollY(p1);
        return;
    }

    private static int h(Object p1)
    {
        return ((android.view.accessibility.AccessibilityRecord) p1).getItemCount();
    }

    private static void h(Object p0, int p1)
    {
        ((android.view.accessibility.AccessibilityRecord) p0).setToIndex(p1);
        return;
    }

    private static android.os.Parcelable i(Object p1)
    {
        return ((android.view.accessibility.AccessibilityRecord) p1).getParcelableData();
    }

    private static int j(Object p1)
    {
        return ((android.view.accessibility.AccessibilityRecord) p1).getRemovedCount();
    }

    private static int k(Object p1)
    {
        return ((android.view.accessibility.AccessibilityRecord) p1).getScrollX();
    }

    private static int l(Object p1)
    {
        return ((android.view.accessibility.AccessibilityRecord) p1).getScrollY();
    }

    private static Object m(Object p1)
    {
        return ((android.view.accessibility.AccessibilityRecord) p1).getSource();
    }

    private static java.util.List n(Object p1)
    {
        return ((android.view.accessibility.AccessibilityRecord) p1).getText();
    }

    private static int o(Object p1)
    {
        return ((android.view.accessibility.AccessibilityRecord) p1).getToIndex();
    }

    private static int p(Object p1)
    {
        return ((android.view.accessibility.AccessibilityRecord) p1).getWindowId();
    }

    private static boolean q(Object p1)
    {
        return ((android.view.accessibility.AccessibilityRecord) p1).isChecked();
    }

    private static boolean r(Object p1)
    {
        return ((android.view.accessibility.AccessibilityRecord) p1).isEnabled();
    }

    private static boolean s(Object p1)
    {
        return ((android.view.accessibility.AccessibilityRecord) p1).isFullScreen();
    }

    private static boolean t(Object p1)
    {
        return ((android.view.accessibility.AccessibilityRecord) p1).isPassword();
    }

    private static boolean u(Object p1)
    {
        return ((android.view.accessibility.AccessibilityRecord) p1).isScrollable();
    }

    private static void v(Object p0)
    {
        ((android.view.accessibility.AccessibilityRecord) p0).recycle();
        return;
    }
}
