package android.support.v4.view.b;
final class h implements android.view.animation.Interpolator {
    private static final float a = 990057071;
    private final float[] b;
    private final float[] c;

    public h(float p4, float p5)
    {
        android.graphics.Path v0_1 = new android.graphics.Path();
        v0_1.moveTo(0, 0);
        v0_1.quadTo(p4, p5, 1065353216, 1065353216);
        this(v0_1);
        return;
    }

    public h(float p8, float p9, float p10, float p11)
    {
        android.graphics.Path v0_1 = new android.graphics.Path();
        v0_1.moveTo(0, 0);
        v0_1.cubicTo(p8, p9, p10, p11, 1065353216, 1065353216);
        this(v0_1);
        return;
    }

    public h(android.graphics.Path p9)
    {
        android.graphics.PathMeasure v2_1 = new android.graphics.PathMeasure(p9, 0);
        float v3 = v2_1.getLength();
        int v4 = (((int) (v3 / 990057071)) + 1);
        int v0_3 = new float[v4];
        this.b = v0_3;
        int v0_4 = new float[v4];
        this.c = v0_4;
        float[] v5 = new float[2];
        int v0_6 = 0;
        while (v0_6 < v4) {
            v2_1.getPosTan(((((float) v0_6) * v3) / ((float) (v4 - 1))), v5, 0);
            this.b[v0_6] = v5[0];
            this.c[v0_6] = v5[1];
            v0_6++;
        }
        return;
    }

    private static android.graphics.Path a(float p3, float p4)
    {
        android.graphics.Path v0_1 = new android.graphics.Path();
        v0_1.moveTo(0, 0);
        v0_1.quadTo(p3, p4, 1065353216, 1065353216);
        return v0_1;
    }

    private static android.graphics.Path a(float p7, float p8, float p9, float p10)
    {
        android.graphics.Path v0_1 = new android.graphics.Path();
        v0_1.moveTo(0, 0);
        v0_1.cubicTo(p7, p8, p9, p10, 1065353216, 1065353216);
        return v0_1;
    }

    public final float getInterpolation(float p6)
    {
        float v0_0 = 0;
        if (p6 > 0) {
            if (p6 < 1065353216) {
                float v1_3 = (this.b.length - 1);
                float[] v3_0 = 0;
                while ((v1_3 - v3_0) > 1) {
                    float v2_10 = ((v3_0 + v1_3) / 2);
                    if (p6 >= this.b[v2_10]) {
                        v3_0 = v2_10;
                    } else {
                        v1_3 = v2_10;
                    }
                }
                float v2_6 = (this.b[v1_3] - this.b[v3_0]);
                if (v2_6 != 0) {
                    float v2_8 = this.c[v3_0];
                    v0_0 = ((((p6 - this.b[v3_0]) / v2_6) * (this.c[v1_3] - v2_8)) + v2_8);
                } else {
                    v0_0 = this.c[v3_0];
                }
            } else {
                v0_0 = 1065353216;
            }
        }
        return v0_0;
    }
}
