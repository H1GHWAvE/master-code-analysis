package android.support.v4.view;
public final class ec {
    public static final int a = 0;
    public static final int b = 1;
    static final android.support.v4.view.ef c;

    static ec()
    {
        android.support.v4.view.ei v0_0 = android.os.Build$VERSION.SDK_INT;
        if (v0_0 < 21) {
            if (v0_0 < 18) {
                if (v0_0 < 14) {
                    if (v0_0 < 11) {
                        android.support.v4.view.ec.c = new android.support.v4.view.ei();
                    } else {
                        android.support.v4.view.ec.c = new android.support.v4.view.ed();
                    }
                } else {
                    android.support.v4.view.ec.c = new android.support.v4.view.ee();
                }
            } else {
                android.support.v4.view.ec.c = new android.support.v4.view.eg();
            }
        } else {
            android.support.v4.view.ec.c = new android.support.v4.view.eh();
        }
        return;
    }

    private ec()
    {
        return;
    }

    public static void a(android.view.ViewGroup p1)
    {
        android.support.v4.view.ec.c.a(p1);
        return;
    }

    private static void a(android.view.ViewGroup p1, int p2)
    {
        android.support.v4.view.ec.c.a(p1, p2);
        return;
    }

    private static void a(android.view.ViewGroup p1, boolean p2)
    {
        android.support.v4.view.ec.c.a(p1, p2);
        return;
    }

    private static boolean a(android.view.ViewGroup p1, android.view.View p2, android.view.accessibility.AccessibilityEvent p3)
    {
        return android.support.v4.view.ec.c.a(p1, p2, p3);
    }

    private static int b(android.view.ViewGroup p1)
    {
        return android.support.v4.view.ec.c.b(p1);
    }

    private static boolean c(android.view.ViewGroup p1)
    {
        return android.support.v4.view.ec.c.c(p1);
    }

    private static int d(android.view.ViewGroup p1)
    {
        return android.support.v4.view.ec.c.d(p1);
    }
}
