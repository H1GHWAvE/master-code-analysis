package android.support.v4.view;
final class ds {

    ds()
    {
        return;
    }

    private static android.support.v4.view.gh a(android.view.View p2, android.support.v4.view.gh p3)
    {
        if ((p3 instanceof android.support.v4.view.gi)) {
            android.view.WindowInsets v0_3 = ((android.support.v4.view.gi) p3).a;
            android.view.WindowInsets v1 = p2.onApplyWindowInsets(v0_3);
            if (v1 != v0_3) {
                p3 = new android.support.v4.view.gi(v1);
            }
        }
        return p3;
    }

    private static String a(android.view.View p1)
    {
        return p1.getTransitionName();
    }

    private static void a(android.view.View p0, float p1)
    {
        p0.setElevation(p1);
        return;
    }

    private static void a(android.view.View p0, android.content.res.ColorStateList p1)
    {
        p0.setBackgroundTintList(p1);
        return;
    }

    private static void a(android.view.View p0, android.graphics.PorterDuff$Mode p1)
    {
        p0.setBackgroundTintMode(p1);
        return;
    }

    private static void a(android.view.View p1, android.support.v4.view.bx p2)
    {
        p1.setOnApplyWindowInsetsListener(new android.support.v4.view.dt(p2));
        return;
    }

    private static void a(android.view.View p0, String p1)
    {
        p0.setTransitionName(p1);
        return;
    }

    private static void a(android.view.View p0, boolean p1)
    {
        p0.setNestedScrollingEnabled(p1);
        return;
    }

    private static boolean a(android.view.View p1, float p2, float p3)
    {
        return p1.dispatchNestedPreFling(p2, p3);
    }

    private static boolean a(android.view.View p1, float p2, float p3, boolean p4)
    {
        return p1.dispatchNestedFling(p2, p3, p4);
    }

    private static boolean a(android.view.View p1, int p2)
    {
        return p1.startNestedScroll(p2);
    }

    private static boolean a(android.view.View p1, int p2, int p3, int p4, int p5, int[] p6)
    {
        return p1.dispatchNestedScroll(p2, p3, p4, p5, p6);
    }

    private static boolean a(android.view.View p1, int p2, int p3, int[] p4, int[] p5)
    {
        return p1.dispatchNestedPreScroll(p2, p3, p4, p5);
    }

    private static android.support.v4.view.gh b(android.view.View p2, android.support.v4.view.gh p3)
    {
        if ((p3 instanceof android.support.v4.view.gi)) {
            android.view.WindowInsets v0_3 = ((android.support.v4.view.gi) p3).a;
            android.view.WindowInsets v1 = p2.dispatchApplyWindowInsets(v0_3);
            if (v1 != v0_3) {
                p3 = new android.support.v4.view.gi(v1);
            }
        }
        return p3;
    }

    private static void b(android.view.View p0)
    {
        p0.requestApplyInsets();
        return;
    }

    private static void b(android.view.View p0, float p1)
    {
        p0.setTranslationZ(p1);
        return;
    }

    private static float c(android.view.View p1)
    {
        return p1.getElevation();
    }

    private static float d(android.view.View p1)
    {
        return p1.getTranslationZ();
    }

    private static boolean e(android.view.View p1)
    {
        return p1.isImportantForAccessibility();
    }

    private static android.content.res.ColorStateList f(android.view.View p1)
    {
        return p1.getBackgroundTintList();
    }

    private static android.graphics.PorterDuff$Mode g(android.view.View p1)
    {
        return p1.getBackgroundTintMode();
    }

    private static boolean h(android.view.View p1)
    {
        return p1.isNestedScrollingEnabled();
    }

    private static void i(android.view.View p0)
    {
        p0.stopNestedScroll();
        return;
    }

    private static boolean j(android.view.View p1)
    {
        return p1.hasNestedScrollingParent();
    }

    private static float k(android.view.View p1)
    {
        return p1.getZ();
    }
}
