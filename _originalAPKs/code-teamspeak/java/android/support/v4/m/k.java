package android.support.v4.m;
final class k {
    private static final String a = "ICUCompatIcs";
    private static reflect.Method b;
    private static reflect.Method c;

    static k()
    {
        try {
            reflect.Method v0_1 = Class.forName("libcore.icu.ICU");
        } catch (reflect.Method v0_3) {
            android.support.v4.m.k.b = 0;
            android.support.v4.m.k.c = 0;
            android.util.Log.w("ICUCompatIcs", v0_3);
            return;
        }
        if (v0_1 == null) {
            return;
        } else {
            Class[] v2_1 = new Class[1];
            v2_1[0] = String;
            android.support.v4.m.k.b = v0_1.getMethod("getScript", v2_1);
            Class[] v2_3 = new Class[1];
            v2_3[0] = String;
            android.support.v4.m.k.c = v0_1.getMethod("addLikelySubtags", v2_3);
            return;
        }
    }

    k()
    {
        return;
    }

    private static String a(String p4)
    {
        try {
            int v0_4;
            if (android.support.v4.m.k.b == null) {
                v0_4 = 0;
            } else {
                int v0_2 = new Object[1];
                v0_2[0] = p4;
                v0_4 = ((String) android.support.v4.m.k.b.invoke(0, v0_2));
            }
        } catch (int v0_5) {
            android.util.Log.w("ICUCompatIcs", v0_5);
        } catch (int v0_6) {
            android.util.Log.w("ICUCompatIcs", v0_6);
        }
        return v0_4;
    }

    public static String a(java.util.Locale p1)
    {
        int v0_1;
        int v0_0 = android.support.v4.m.k.b(p1);
        if (v0_0 == 0) {
            v0_1 = 0;
        } else {
            v0_1 = android.support.v4.m.k.a(v0_0);
        }
        return v0_1;
    }

    private static String b(java.util.Locale p4)
    {
        String v1 = p4.toString();
        try {
            String v0_4;
            if (android.support.v4.m.k.c == null) {
                v0_4 = v1;
            } else {
                String v0_2 = new Object[1];
                v0_2[0] = v1;
                v0_4 = ((String) android.support.v4.m.k.c.invoke(0, v0_2));
            }
        } catch (String v0_6) {
            android.util.Log.w("ICUCompatIcs", v0_6);
        } catch (String v0_5) {
            android.util.Log.w("ICUCompatIcs", v0_5);
        }
        return v0_4;
    }
}
