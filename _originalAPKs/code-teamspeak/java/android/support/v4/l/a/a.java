package android.support.v4.l.a;
final class a {
    private static final String a = "android.support.v4.speech.tts";

    a()
    {
        return;
    }

    private static android.speech.tts.TextToSpeech a(android.content.Context p2, android.speech.tts.TextToSpeech$OnInitListener p3, String p4)
    {
        android.speech.tts.TextToSpeech v0_2;
        if (android.os.Build$VERSION.SDK_INT >= 14) {
            v0_2 = new android.speech.tts.TextToSpeech(p2, p3, p4);
        } else {
            if (p4 != null) {
                android.util.Log.w("android.support.v4.speech.tts", "Can\'t specify tts engine on this device");
                v0_2 = new android.speech.tts.TextToSpeech(p2, p3);
            } else {
                v0_2 = new android.speech.tts.TextToSpeech(p2, p3);
            }
        }
        return v0_2;
    }
}
