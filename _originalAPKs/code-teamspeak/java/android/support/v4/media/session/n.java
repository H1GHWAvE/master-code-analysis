package android.support.v4.media.session;
 class n implements android.support.v4.media.session.m {
    protected final Object a;

    public n(android.content.Context p2, android.support.v4.media.session.MediaSessionCompat$Token p3)
    {
        this.a = android.support.v4.media.session.v.a(p2, p3.a);
        if (this.a != null) {
            return;
        } else {
            throw new android.os.RemoteException();
        }
    }

    public n(android.content.Context p2, android.support.v4.media.session.MediaSessionCompat p3)
    {
        this.a = android.support.v4.media.session.v.a(p2, p3.a().a);
        return;
    }

    public android.support.v4.media.session.r a()
    {
        int v0_2;
        android.media.session.MediaController$TransportControls v1 = ((android.media.session.MediaController) this.a).getTransportControls();
        if (v1 == null) {
            v0_2 = 0;
        } else {
            v0_2 = new android.support.v4.media.session.s(v1);
        }
        return v0_2;
    }

    public final void a(int p2, int p3)
    {
        ((android.media.session.MediaController) this.a).setVolumeTo(p2, p3);
        return;
    }

    public final void a(android.support.v4.media.session.i p3)
    {
        ((android.media.session.MediaController) this.a).unregisterCallback(((android.media.session.MediaController$Callback) android.support.v4.media.session.i.c(p3)));
        return;
    }

    public final void a(android.support.v4.media.session.i p3, android.os.Handler p4)
    {
        ((android.media.session.MediaController) this.a).registerCallback(((android.media.session.MediaController$Callback) android.support.v4.media.session.i.c(p3)), p4);
        return;
    }

    public final void a(String p2, android.os.Bundle p3, android.os.ResultReceiver p4)
    {
        ((android.media.session.MediaController) this.a).sendCommand(p2, p3, p4);
        return;
    }

    public final boolean a(android.view.KeyEvent p2)
    {
        return ((android.media.session.MediaController) this.a).dispatchMediaButtonEvent(p2);
    }

    public final android.support.v4.media.session.PlaybackStateCompat b()
    {
        int v0_3;
        int v0_2 = ((android.media.session.MediaController) this.a).getPlaybackState();
        if (v0_2 == 0) {
            v0_3 = 0;
        } else {
            v0_3 = android.support.v4.media.session.PlaybackStateCompat.a(v0_2);
        }
        return v0_3;
    }

    public final void b(int p2, int p3)
    {
        ((android.media.session.MediaController) this.a).adjustVolume(p2, p3);
        return;
    }

    public final android.support.v4.media.MediaMetadataCompat c()
    {
        int v0_3;
        int v0_2 = ((android.media.session.MediaController) this.a).getMetadata();
        if (v0_2 == 0) {
            v0_3 = 0;
        } else {
            v0_3 = android.support.v4.media.MediaMetadataCompat.a(v0_2);
        }
        return v0_3;
    }

    public final java.util.List d()
    {
        android.support.v4.media.session.MediaSessionCompat$QueueItem v2_1;
        android.support.v4.media.session.MediaSessionCompat$QueueItem v2_0 = ((android.media.session.MediaController) this.a).getQueue();
        if (v2_0 != null) {
            v2_1 = new java.util.ArrayList(v2_0);
        } else {
            v2_1 = 0;
        }
        java.util.ArrayList v0_5;
        if (v2_1 != null) {
            v0_5 = new java.util.ArrayList();
            java.util.Iterator v1_1 = v2_1.iterator();
            while (v1_1.hasNext()) {
                v0_5.add(android.support.v4.media.session.MediaSessionCompat$QueueItem.a(v1_1.next()));
            }
        } else {
            v0_5 = 0;
        }
        return v0_5;
    }

    public final CharSequence e()
    {
        return ((android.media.session.MediaController) this.a).getQueueTitle();
    }

    public final android.os.Bundle f()
    {
        return ((android.media.session.MediaController) this.a).getExtras();
    }

    public final int g()
    {
        return ((android.media.session.MediaController) this.a).getRatingType();
    }

    public final long h()
    {
        return ((android.media.session.MediaController) this.a).getFlags();
    }

    public final android.support.v4.media.session.q i()
    {
        int v0_2;
        int v5_0 = ((android.media.session.MediaController) this.a).getPlaybackInfo();
        if (v5_0 == 0) {
            v0_2 = 0;
        } else {
            int v2_4;
            int v1_2 = ((android.media.session.MediaController$PlaybackInfo) v5_0).getPlaybackType();
            int v2_2 = ((android.media.session.MediaController$PlaybackInfo) v5_0).getAudioAttributes();
            if ((v2_2.getFlags() & 1) != 1) {
                if ((v2_2.getFlags() & 4) != 4) {
                    switch (v2_2.getUsage()) {
                        case 1:
                        case 11:
                        case 12:
                        case 14:
                            v2_4 = 3;
                            break;
                        case 2:
                            v2_4 = 0;
                            break;
                        case 3:
                            v2_4 = 8;
                            break;
                        case 4:
                            v2_4 = 4;
                            break;
                        case 5:
                        case 7:
                        case 8:
                        case 9:
                        case 10:
                            v2_4 = 5;
                            break;
                        case 6:
                            v2_4 = 2;
                            break;
                        case 13:
                            v2_4 = 1;
                            break;
                        default:
                            v2_4 = 3;
                    }
                } else {
                    v2_4 = 6;
                }
            } else {
                v2_4 = 7;
            }
            v0_2 = new android.support.v4.media.session.q(v1_2, v2_4, ((android.media.session.MediaController$PlaybackInfo) v5_0).getVolumeControl(), ((android.media.session.MediaController$PlaybackInfo) v5_0).getMaxVolume(), ((android.media.session.MediaController$PlaybackInfo) v5_0).getCurrentVolume());
        }
        return v0_2;
    }

    public final android.app.PendingIntent j()
    {
        return ((android.media.session.MediaController) this.a).getSessionActivity();
    }

    public final String k()
    {
        return ((android.media.session.MediaController) this.a).getPackageName();
    }

    public final Object l()
    {
        return this.a;
    }
}
