package android.support.v4.media.session;
public final class MediaSessionCompat$Token implements android.os.Parcelable {
    public static final android.os.Parcelable$Creator CREATOR;
    public final Object a;

    static MediaSessionCompat$Token()
    {
        android.support.v4.media.session.MediaSessionCompat$Token.CREATOR = new android.support.v4.media.session.as();
        return;
    }

    MediaSessionCompat$Token(Object p1)
    {
        this.a = p1;
        return;
    }

    private static android.support.v4.media.session.MediaSessionCompat$Token a(Object p2)
    {
        if ((p2 != null) && (android.os.Build$VERSION.SDK_INT >= 21)) {
            if (!(p2 instanceof android.media.session.MediaSession$Token)) {
                throw new IllegalArgumentException("token is not a valid MediaSession.Token object");
            } else {
                IllegalArgumentException v0_4 = new android.support.v4.media.session.MediaSessionCompat$Token(p2);
            }
        } else {
            v0_4 = 0;
        }
        return v0_4;
    }

    private Object a()
    {
        return this.a;
    }

    public final int describeContents()
    {
        return 0;
    }

    public final void writeToParcel(android.os.Parcel p3, int p4)
    {
        if (android.os.Build$VERSION.SDK_INT < 21) {
            p3.writeStrongBinder(((android.os.IBinder) this.a));
        } else {
            p3.writeParcelable(((android.os.Parcelable) this.a), p4);
        }
        return;
    }
}
