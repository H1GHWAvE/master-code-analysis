package android.support.v4.media.session;
final class ah implements android.support.v4.media.session.ag {
    private final Object a;
    private final android.support.v4.media.session.MediaSessionCompat$Token b;
    private android.app.PendingIntent c;

    public ah(android.content.Context p3, String p4)
    {
        this.a = new android.media.session.MediaSession(p3, p4);
        this.b = new android.support.v4.media.session.MediaSessionCompat$Token(((android.media.session.MediaSession) this.a).getSessionToken());
        return;
    }

    public ah(Object p3)
    {
        if (!(p3 instanceof android.media.session.MediaSession)) {
            throw new IllegalArgumentException("mediaSession is not a valid MediaSession object");
        } else {
            this.a = p3;
            this.b = new android.support.v4.media.session.MediaSessionCompat$Token(((android.media.session.MediaSession) this.a).getSessionToken());
            return;
        }
    }

    public final void a(int p2)
    {
        ((android.media.session.MediaSession) this.a).setFlags(p2);
        return;
    }

    public final void a(android.app.PendingIntent p2)
    {
        ((android.media.session.MediaSession) this.a).setSessionActivity(p2);
        return;
    }

    public final void a(android.os.Bundle p2)
    {
        ((android.media.session.MediaSession) this.a).setExtras(p2);
        return;
    }

    public final void a(android.support.v4.media.MediaMetadataCompat p9)
    {
        android.media.MediaMetadata$Builder v1_1;
        Object v4 = this.a;
        if ((p9.D == null) && (android.os.Build$VERSION.SDK_INT >= 21)) {
            android.media.MediaMetadata$Builder v2_1 = new android.media.MediaMetadata$Builder();
            java.util.Iterator v5 = p9.C.keySet().iterator();
            while (v5.hasNext()) {
                android.media.MediaMetadata v0_7 = ((String) v5.next());
                android.media.MediaMetadata$Builder v1_4 = ((Integer) android.support.v4.media.MediaMetadataCompat.B.get(v0_7));
                if (v1_4 != null) {
                    switch (v1_4.intValue()) {
                        case 0:
                            ((android.media.MediaMetadata$Builder) v2_1).putLong(v0_7, p9.b(v0_7));
                            break;
                        case 1:
                            ((android.media.MediaMetadata$Builder) v2_1).putText(v0_7, p9.a(v0_7));
                            break;
                        case 2:
                            ((android.media.MediaMetadata$Builder) v2_1).putBitmap(v0_7, p9.d(v0_7));
                            break;
                        case 3:
                            ((android.media.MediaMetadata$Builder) v2_1).putRating(v0_7, ((android.media.Rating) p9.c(v0_7).a()));
                            break;
                        default:
                    }
                }
            }
            p9.D = ((android.media.MediaMetadata$Builder) v2_1).build();
            v1_1 = p9.D;
        } else {
            v1_1 = p9.D;
        }
        ((android.media.session.MediaSession) v4).setMetadata(((android.media.MediaMetadata) v1_1));
        return;
    }

    public final void a(android.support.v4.media.ae p3)
    {
        ((android.media.session.MediaSession) this.a).setPlaybackToRemote(((android.media.VolumeProvider) p3.a()));
        return;
    }

    public final void a(android.support.v4.media.session.PlaybackStateCompat p20)
    {
        Object v3_5;
        Object v18 = this.a;
        if ((p20.L == null) && (android.os.Build$VERSION.SDK_INT >= 21)) {
            java.util.ArrayList v14_0 = 0;
            if (p20.I != null) {
                v14_0 = new java.util.ArrayList(p20.I.size());
                Object v3_1 = p20.I.iterator();
                while (v3_1.hasNext()) {
                    Object v2_12;
                    Object v2_11 = ((android.support.v4.media.session.PlaybackStateCompat$CustomAction) v3_1.next());
                    if ((v2_11.e == null) && (android.os.Build$VERSION.SDK_INT >= 21)) {
                        android.os.Bundle v7 = v2_11.d;
                        android.media.session.PlaybackState$CustomAction$Builder v8_3 = new android.media.session.PlaybackState$CustomAction$Builder(v2_11.a, v2_11.b, v2_11.c);
                        v8_3.setExtras(v7);
                        v2_11.e = v8_3.build();
                        v2_12 = v2_11.e;
                    } else {
                        v2_12 = v2_11.e;
                    }
                    v14_0.add(v2_12);
                }
            }
            if (android.os.Build$VERSION.SDK_INT < 22) {
                p20.L = android.support.v4.media.session.bp.a(p20.B, p20.C, p20.D, p20.E, p20.F, p20.G, p20.H, v14_0, p20.J);
            } else {
                p20.L = android.support.v4.media.session.br.a(p20.B, p20.C, p20.D, p20.E, p20.F, p20.G, p20.H, v14_0, p20.J, p20.K);
            }
            v3_5 = p20.L;
        } else {
            v3_5 = p20.L;
        }
        ((android.media.session.MediaSession) v18).setPlaybackState(((android.media.session.PlaybackState) v3_5));
        return;
    }

    public final void a(android.support.v4.media.session.ad p3, android.os.Handler p4)
    {
        ((android.media.session.MediaSession) this.a).setCallback(((android.media.session.MediaSession$Callback) p3.a), p4);
        return;
    }

    public final void a(CharSequence p2)
    {
        ((android.media.session.MediaSession) this.a).setQueueTitle(p2);
        return;
    }

    public final void a(String p2, android.os.Bundle p3)
    {
        ((android.media.session.MediaSession) this.a).sendSessionEvent(p2, p3);
        return;
    }

    public final void a(java.util.List p8)
    {
        Object v0_0 = 0;
        if (p8 != null) {
            java.util.ArrayList v2_1 = new java.util.ArrayList();
            java.util.Iterator v3 = p8.iterator();
            while (v3.hasNext()) {
                Object v0_4;
                Object v0_3 = ((android.support.v4.media.session.MediaSessionCompat$QueueItem) v3.next());
                if ((v0_3.d == null) && (android.os.Build$VERSION.SDK_INT >= 21)) {
                    v0_3.d = new android.media.session.MediaSession$QueueItem(((android.media.MediaDescription) v0_3.b.a()), v0_3.c);
                    v0_4 = v0_3.d;
                } else {
                    v0_4 = v0_3.d;
                }
                v2_1.add(v0_4);
            }
            v0_0 = v2_1;
        }
        android.support.v4.media.session.az.a(this.a, v0_0);
        return;
    }

    public final void a(boolean p2)
    {
        ((android.media.session.MediaSession) this.a).setActive(p2);
        return;
    }

    public final boolean a()
    {
        return ((android.media.session.MediaSession) this.a).isActive();
    }

    public final void b()
    {
        ((android.media.session.MediaSession) this.a).release();
        return;
    }

    public final void b(int p3)
    {
        android.media.session.MediaSession v0_0 = this.a;
        android.media.AudioAttributes v1_1 = new android.media.AudioAttributes$Builder();
        v1_1.setLegacyStreamType(p3);
        ((android.media.session.MediaSession) v0_0).setPlaybackToLocal(v1_1.build());
        return;
    }

    public final void b(android.app.PendingIntent p2)
    {
        this.c = p2;
        ((android.media.session.MediaSession) this.a).setMediaButtonReceiver(p2);
        return;
    }

    public final android.support.v4.media.session.MediaSessionCompat$Token c()
    {
        return this.b;
    }

    public final void c(int p3)
    {
        if (android.os.Build$VERSION.SDK_INT >= 22) {
            ((android.media.session.MediaSession) this.a).setRatingType(p3);
        }
        return;
    }

    public final Object d()
    {
        return this.a;
    }

    public final Object e()
    {
        return 0;
    }
}
