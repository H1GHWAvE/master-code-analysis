package android.support.v4.media.session;
public final class at {
    private static final String A = "android.media.metadata.ALBUM";
    private static final String B = "android.media.metadata.AUTHOR";
    private static final String C = "android.media.metadata.WRITER";
    private static final String D = "android.media.metadata.COMPOSER";
    private static final String E = "android.media.metadata.COMPILATION";
    private static final String F = "android.media.metadata.DATE";
    private static final String G = "android.media.metadata.GENRE";
    private static final String H = "android.media.metadata.TRACK_NUMBER";
    private static final String I = "android.media.metadata.DISC_NUMBER";
    private static final String J = "android.media.metadata.ALBUM_ARTIST";
    static final int a = 0;
    static final int b = 0;
    static final int c = 1;
    static final int d = 2;
    static final int e = 3;
    static final int f = 4;
    static final int g = 5;
    static final int h = 6;
    static final int i = 7;
    static final int j = 8;
    static final int k = 9;
    static final int l = 10;
    static final int m = 11;
    private static final long n = 1;
    private static final long o = 2;
    private static final long p = 4;
    private static final long q = 8;
    private static final long r = 16;
    private static final long s = 32;
    private static final long t = 64;
    private static final long u = 512;
    private static final String v = "android.media.metadata.ART";
    private static final String w = "android.media.metadata.ALBUM_ART";
    private static final String x = "android.media.metadata.TITLE";
    private static final String y = "android.media.metadata.ARTIST";
    private static final String z = "android.media.metadata.DURATION";

    public at()
    {
        return;
    }

    static int a(int p1)
    {
        int v0;
        switch (p1) {
            case 0:
                v0 = 0;
                break;
            case 1:
                v0 = 1;
                break;
            case 2:
                v0 = 2;
                break;
            case 3:
                v0 = 3;
                break;
            case 4:
                v0 = 4;
                break;
            case 5:
                v0 = 5;
                break;
            case 6:
            case 8:
                v0 = 8;
                break;
            case 7:
                v0 = 9;
                break;
            case 9:
                v0 = 7;
                break;
            case 10:
            case 11:
                v0 = 6;
                break;
            default:
                v0 = -1;
        }
        return v0;
    }

    static int a(long p6)
    {
        int v0 = 0;
        if ((1 & p6) != 0) {
            v0 = 32;
        }
        if ((2 & p6) != 0) {
            v0 |= 16;
        }
        if ((4 & p6) != 0) {
            v0 |= 4;
        }
        if ((8 & p6) != 0) {
            v0 |= 2;
        }
        if ((16 & p6) != 0) {
            v0 |= 1;
        }
        if ((32 & p6) != 0) {
            v0 |= 128;
        }
        if ((64 & p6) != 0) {
            v0 |= 64;
        }
        if ((512 & p6) != 0) {
            v0 |= 8;
        }
        return v0;
    }

    private static Object a(android.app.PendingIntent p1)
    {
        return new android.media.RemoteControlClient(p1);
    }

    public static void a(android.content.Context p1, Object p2)
    {
        ((android.media.AudioManager) p1.getSystemService("audio")).unregisterRemoteControlClient(((android.media.RemoteControlClient) p2));
        return;
    }

    static void a(android.os.Bundle p4, android.media.RemoteControlClient$MetadataEditor p5)
    {
        if (p4 != null) {
            if (!p4.containsKey("android.media.metadata.ART")) {
                if (p4.containsKey("android.media.metadata.ALBUM_ART")) {
                    p5.putBitmap(100, ((android.graphics.Bitmap) p4.getParcelable("android.media.metadata.ALBUM_ART")));
                }
            } else {
                p5.putBitmap(100, ((android.graphics.Bitmap) p4.getParcelable("android.media.metadata.ART")));
            }
            if (p4.containsKey("android.media.metadata.ALBUM")) {
                p5.putString(1, p4.getString("android.media.metadata.ALBUM"));
            }
            if (p4.containsKey("android.media.metadata.ALBUM_ARTIST")) {
                p5.putString(13, p4.getString("android.media.metadata.ALBUM_ARTIST"));
            }
            if (p4.containsKey("android.media.metadata.ARTIST")) {
                p5.putString(2, p4.getString("android.media.metadata.ARTIST"));
            }
            if (p4.containsKey("android.media.metadata.AUTHOR")) {
                p5.putString(3, p4.getString("android.media.metadata.AUTHOR"));
            }
            if (p4.containsKey("android.media.metadata.COMPILATION")) {
                p5.putString(15, p4.getString("android.media.metadata.COMPILATION"));
            }
            if (p4.containsKey("android.media.metadata.COMPOSER")) {
                p5.putString(4, p4.getString("android.media.metadata.COMPOSER"));
            }
            if (p4.containsKey("android.media.metadata.DATE")) {
                p5.putString(5, p4.getString("android.media.metadata.DATE"));
            }
            if (p4.containsKey("android.media.metadata.DISC_NUMBER")) {
                p5.putLong(14, p4.getLong("android.media.metadata.DISC_NUMBER"));
            }
            if (p4.containsKey("android.media.metadata.DURATION")) {
                p5.putLong(9, p4.getLong("android.media.metadata.DURATION"));
            }
            if (p4.containsKey("android.media.metadata.GENRE")) {
                p5.putString(6, p4.getString("android.media.metadata.GENRE"));
            }
            if (p4.containsKey("android.media.metadata.TITLE")) {
                p5.putString(7, p4.getString("android.media.metadata.TITLE"));
            }
            if (p4.containsKey("android.media.metadata.TRACK_NUMBER")) {
                p5.putLong(0, p4.getLong("android.media.metadata.TRACK_NUMBER"));
            }
            if (p4.containsKey("android.media.metadata.WRITER")) {
                p5.putString(11, p4.getString("android.media.metadata.WRITER"));
            }
        }
        return;
    }

    public static void a(Object p1, int p2)
    {
        ((android.media.RemoteControlClient) p1).setPlaybackState(android.support.v4.media.session.at.a(p2));
        return;
    }

    public static void a(Object p1, long p2)
    {
        ((android.media.RemoteControlClient) p1).setTransportControlFlags(android.support.v4.media.session.at.a(p2));
        return;
    }

    private static void a(Object p1, android.os.Bundle p2)
    {
        android.media.RemoteControlClient$MetadataEditor v0_1 = ((android.media.RemoteControlClient) p1).editMetadata(1);
        android.support.v4.media.session.at.a(p2, v0_1);
        v0_1.apply();
        return;
    }

    private static void b(android.content.Context p1, Object p2)
    {
        ((android.media.AudioManager) p1.getSystemService("audio")).registerRemoteControlClient(((android.media.RemoteControlClient) p2));
        return;
    }
}
