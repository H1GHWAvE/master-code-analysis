package android.support.v4.media.session;
final class u extends android.support.v4.media.session.r {
    private android.support.v4.media.session.d a;

    public u(android.support.v4.media.session.d p1)
    {
        this.a = p1;
        return;
    }

    public final void a()
    {
        try {
            this.a.g();
        } catch (String v0_1) {
            android.util.Log.e("MediaControllerCompat", new StringBuilder("Dead object in play. ").append(v0_1).toString());
        }
        return;
    }

    public final void a(long p6)
    {
        try {
            this.a.a(p6);
        } catch (String v0_1) {
            android.util.Log.e("MediaControllerCompat", new StringBuilder("Dead object in skipToQueueItem. ").append(v0_1).toString());
        }
        return;
    }

    public final void a(android.net.Uri p5, android.os.Bundle p6)
    {
        try {
            this.a.a(p5, p6);
        } catch (String v0_1) {
            android.util.Log.e("MediaControllerCompat", new StringBuilder("Dead object in playFromUri. ").append(v0_1).toString());
        }
        return;
    }

    public final void a(android.support.v4.media.RatingCompat p5)
    {
        try {
            this.a.a(p5);
        } catch (String v0_1) {
            android.util.Log.e("MediaControllerCompat", new StringBuilder("Dead object in setRating. ").append(v0_1).toString());
        }
        return;
    }

    public final void a(android.support.v4.media.session.PlaybackStateCompat$CustomAction p5, android.os.Bundle p6)
    {
        try {
            this.a.c(p5.a, p6);
        } catch (String v0_1) {
            android.util.Log.e("MediaControllerCompat", new StringBuilder("Dead object in sendCustomAction. ").append(v0_1).toString());
        }
        return;
    }

    public final void a(String p5, android.os.Bundle p6)
    {
        try {
            this.a.a(p5, p6);
        } catch (String v0_1) {
            android.util.Log.e("MediaControllerCompat", new StringBuilder("Dead object in playFromMediaId. ").append(v0_1).toString());
        }
        return;
    }

    public final void b()
    {
        try {
            this.a.h();
        } catch (String v0_1) {
            android.util.Log.e("MediaControllerCompat", new StringBuilder("Dead object in pause. ").append(v0_1).toString());
        }
        return;
    }

    public final void b(long p6)
    {
        try {
            this.a.b(p6);
        } catch (String v0_1) {
            android.util.Log.e("MediaControllerCompat", new StringBuilder("Dead object in seekTo. ").append(v0_1).toString());
        }
        return;
    }

    public final void b(String p5, android.os.Bundle p6)
    {
        try {
            this.a.b(p5, p6);
        } catch (String v0_1) {
            android.util.Log.e("MediaControllerCompat", new StringBuilder("Dead object in playFromSearch. ").append(v0_1).toString());
        }
        return;
    }

    public final void c()
    {
        try {
            this.a.i();
        } catch (String v0_1) {
            android.util.Log.e("MediaControllerCompat", new StringBuilder("Dead object in stop. ").append(v0_1).toString());
        }
        return;
    }

    public final void c(String p5, android.os.Bundle p6)
    {
        try {
            this.a.c(p5, p6);
        } catch (String v0_1) {
            android.util.Log.e("MediaControllerCompat", new StringBuilder("Dead object in sendCustomAction. ").append(v0_1).toString());
        }
        return;
    }

    public final void d()
    {
        try {
            this.a.l();
        } catch (String v0_1) {
            android.util.Log.e("MediaControllerCompat", new StringBuilder("Dead object in fastForward. ").append(v0_1).toString());
        }
        return;
    }

    public final void e()
    {
        try {
            this.a.j();
        } catch (String v0_1) {
            android.util.Log.e("MediaControllerCompat", new StringBuilder("Dead object in skipToNext. ").append(v0_1).toString());
        }
        return;
    }

    public final void f()
    {
        try {
            this.a.m();
        } catch (String v0_1) {
            android.util.Log.e("MediaControllerCompat", new StringBuilder("Dead object in rewind. ").append(v0_1).toString());
        }
        return;
    }

    public final void g()
    {
        try {
            this.a.k();
        } catch (String v0_1) {
            android.util.Log.e("MediaControllerCompat", new StringBuilder("Dead object in skipToPrevious. ").append(v0_1).toString());
        }
        return;
    }
}
