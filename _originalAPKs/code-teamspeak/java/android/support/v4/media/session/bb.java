package android.support.v4.media.session;
 class bb extends android.media.session.MediaSession$Callback {
    protected final android.support.v4.media.session.ba a;

    public bb(android.support.v4.media.session.ba p1)
    {
        this.a = p1;
        return;
    }

    public void onCommand(String p1, android.os.Bundle p2, android.os.ResultReceiver p3)
    {
        return;
    }

    public void onCustomAction(String p1, android.os.Bundle p2)
    {
        return;
    }

    public void onFastForward()
    {
        return;
    }

    public boolean onMediaButtonEvent(android.content.Intent p2)
    {
        int v0_1;
        if (!super.onMediaButtonEvent(p2)) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    public void onPause()
    {
        return;
    }

    public void onPlay()
    {
        return;
    }

    public void onPlayFromMediaId(String p1, android.os.Bundle p2)
    {
        return;
    }

    public void onPlayFromSearch(String p1, android.os.Bundle p2)
    {
        return;
    }

    public void onRewind()
    {
        return;
    }

    public void onSeekTo(long p1)
    {
        return;
    }

    public void onSetRating(android.media.Rating p2)
    {
        this.a.a(p2);
        return;
    }

    public void onSkipToNext()
    {
        return;
    }

    public void onSkipToPrevious()
    {
        return;
    }

    public void onSkipToQueueItem(long p1)
    {
        return;
    }

    public void onStop()
    {
        return;
    }
}
