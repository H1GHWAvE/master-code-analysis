package android.support.v4.media.session;
final class o extends android.support.v4.media.session.n {

    private o(android.content.Context p1, android.support.v4.media.session.MediaSessionCompat$Token p2)
    {
        this(p1, p2);
        return;
    }

    public o(android.content.Context p1, android.support.v4.media.session.MediaSessionCompat p2)
    {
        this(p1, p2);
        return;
    }

    public final android.support.v4.media.session.r a()
    {
        int v0_2;
        android.media.session.MediaController$TransportControls v1 = ((android.media.session.MediaController) this.a).getTransportControls();
        if (v1 == null) {
            v0_2 = 0;
        } else {
            v0_2 = new android.support.v4.media.session.t(v1);
        }
        return v0_2;
    }
}
