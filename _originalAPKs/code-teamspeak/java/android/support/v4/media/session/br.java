package android.support.v4.media.session;
final class br {

    br()
    {
        return;
    }

    private static android.os.Bundle a(Object p1)
    {
        return ((android.media.session.PlaybackState) p1).getExtras();
    }

    public static Object a(int p9, long p10, long p12, float p14, long p15, CharSequence p17, long p18, java.util.List p20, long p21, android.os.Bundle p23)
    {
        android.media.session.PlaybackState v2_1 = new android.media.session.PlaybackState$Builder();
        v2_1.setState(p9, p10, p14, p18);
        v2_1.setBufferedPosition(p12);
        v2_1.setActions(p15);
        v2_1.setErrorMessage(p17);
        java.util.Iterator v4_1 = p20.iterator();
        while (v4_1.hasNext()) {
            v2_1.addCustomAction(((android.media.session.PlaybackState$CustomAction) v4_1.next()));
        }
        v2_1.setActiveQueueItemId(p21);
        v2_1.setExtras(p23);
        return v2_1.build();
    }
}
