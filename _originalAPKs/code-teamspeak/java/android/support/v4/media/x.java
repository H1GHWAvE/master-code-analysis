package android.support.v4.media;
final class x implements android.media.RemoteControlClient$OnGetPlaybackPositionListener, android.media.RemoteControlClient$OnPlaybackPositionUpdateListener {
    final android.content.Context a;
    final android.media.AudioManager b;
    final android.view.View c;
    final android.support.v4.media.w d;
    final String e;
    final android.content.IntentFilter f;
    final android.content.Intent g;
    final android.view.ViewTreeObserver$OnWindowAttachListener h;
    final android.view.ViewTreeObserver$OnWindowFocusChangeListener i;
    final android.content.BroadcastReceiver j;
    android.media.AudioManager$OnAudioFocusChangeListener k;
    android.app.PendingIntent l;
    android.media.RemoteControlClient m;
    boolean n;
    int o;
    boolean p;

    public x(android.content.Context p3, android.media.AudioManager p4, android.view.View p5, android.support.v4.media.w p6)
    {
        this.h = new android.support.v4.media.y(this);
        this.i = new android.support.v4.media.z(this);
        this.j = new android.support.v4.media.aa(this);
        this.k = new android.support.v4.media.ab(this);
        this.o = 0;
        this.a = p3;
        this.b = p4;
        this.c = p5;
        this.d = p6;
        this.e = new StringBuilder().append(p3.getPackageName()).append(":transport:").append(System.identityHashCode(this)).toString();
        this.g = new android.content.Intent(this.e);
        this.g.setPackage(p3.getPackageName());
        this.f = new android.content.IntentFilter();
        this.f.addAction(this.e);
        this.c.getViewTreeObserver().addOnWindowAttachListener(this.h);
        this.c.getViewTreeObserver().addOnWindowFocusChangeListener(this.i);
        return;
    }

    private void a(boolean p5, long p6, int p8)
    {
        if (this.m != null) {
            int v1;
            if (!p5) {
                v1 = 1;
            } else {
                v1 = 3;
            }
            android.media.RemoteControlClient v0_3;
            if (!p5) {
                v0_3 = 0;
            } else {
                v0_3 = 1065353216;
            }
            this.m.setPlaybackState(v1, p6, v0_3);
            this.m.setTransportControlFlags(p8);
        }
        return;
    }

    private Object e()
    {
        return this.m;
    }

    private void f()
    {
        this.d();
        this.c.getViewTreeObserver().removeOnWindowAttachListener(this.h);
        this.c.getViewTreeObserver().removeOnWindowFocusChangeListener(this.i);
        return;
    }

    private void g()
    {
        this.a.registerReceiver(this.j, this.f);
        this.l = android.app.PendingIntent.getBroadcast(this.a, 0, this.g, 268435456);
        this.m = new android.media.RemoteControlClient(this.l);
        this.m.setOnGetPlaybackPositionListener(this);
        this.m.setPlaybackPositionUpdateListener(this);
        return;
    }

    private void h()
    {
        if (!this.n) {
            this.n = 1;
            this.b.registerMediaButtonEventReceiver(this.l);
            this.b.registerRemoteControlClient(this.m);
            if (this.o == 3) {
                this.a();
            }
        }
        return;
    }

    private void i()
    {
        if (this.o != 3) {
            this.o = 3;
            this.m.setPlaybackState(3);
        }
        if (this.n) {
            this.a();
        }
        return;
    }

    private void j()
    {
        if (this.o == 3) {
            this.o = 2;
            this.m.setPlaybackState(2);
        }
        this.b();
        return;
    }

    private void k()
    {
        if (this.o != 1) {
            this.o = 1;
            this.m.setPlaybackState(1);
        }
        this.b();
        return;
    }

    final void a()
    {
        if (!this.p) {
            this.p = 1;
            this.b.requestAudioFocus(this.k, 3, 1);
        }
        return;
    }

    final void b()
    {
        if (this.p) {
            this.p = 0;
            this.b.abandonAudioFocus(this.k);
        }
        return;
    }

    final void c()
    {
        this.b();
        if (this.n) {
            this.n = 0;
            this.b.unregisterRemoteControlClient(this.m);
            this.b.unregisterMediaButtonEventReceiver(this.l);
        }
        return;
    }

    final void d()
    {
        this.c();
        if (this.l != null) {
            this.a.unregisterReceiver(this.j);
            this.l.cancel();
            this.l = 0;
            this.m = 0;
        }
        return;
    }

    public final long onGetPlaybackPosition()
    {
        return this.d.a();
    }

    public final void onPlaybackPositionUpdate(long p1)
    {
        return;
    }
}
