package android.support.v4.media;
final class a implements android.os.Parcelable$Creator {

    a()
    {
        return;
    }

    private static android.support.v4.media.MediaDescriptionCompat a(android.os.Parcel p2)
    {
        android.support.v4.media.MediaDescriptionCompat v0_3;
        if (android.os.Build$VERSION.SDK_INT >= 21) {
            v0_3 = android.support.v4.media.MediaDescriptionCompat.a(android.media.MediaDescription.CREATOR.createFromParcel(p2));
        } else {
            v0_3 = new android.support.v4.media.MediaDescriptionCompat(p2, 0);
        }
        return v0_3;
    }

    private static android.support.v4.media.MediaDescriptionCompat[] a(int p1)
    {
        android.support.v4.media.MediaDescriptionCompat[] v0 = new android.support.v4.media.MediaDescriptionCompat[p1];
        return v0;
    }

    public final synthetic Object createFromParcel(android.os.Parcel p3)
    {
        android.support.v4.media.MediaDescriptionCompat v0_3;
        if (android.os.Build$VERSION.SDK_INT >= 21) {
            v0_3 = android.support.v4.media.MediaDescriptionCompat.a(android.media.MediaDescription.CREATOR.createFromParcel(p3));
        } else {
            v0_3 = new android.support.v4.media.MediaDescriptionCompat(p3, 0);
        }
        return v0_3;
    }

    public final bridge synthetic Object[] newArray(int p2)
    {
        android.support.v4.media.MediaDescriptionCompat[] v0 = new android.support.v4.media.MediaDescriptionCompat[p2];
        return v0;
    }
}
