package android.support.v4.media;
final class r {

    r()
    {
        return;
    }

    private static Object a(float p1)
    {
        return android.media.Rating.newPercentageRating(p1);
    }

    private static Object a(int p1)
    {
        return android.media.Rating.newUnratedRating(p1);
    }

    private static Object a(int p1, float p2)
    {
        return android.media.Rating.newStarRating(p1, p2);
    }

    private static Object a(boolean p1)
    {
        return android.media.Rating.newHeartRating(p1);
    }

    private static boolean a(Object p1)
    {
        return ((android.media.Rating) p1).isRated();
    }

    private static int b(Object p1)
    {
        return ((android.media.Rating) p1).getRatingStyle();
    }

    private static Object b(boolean p1)
    {
        return android.media.Rating.newThumbRating(p1);
    }

    private static boolean c(Object p1)
    {
        return ((android.media.Rating) p1).hasHeart();
    }

    private static boolean d(Object p1)
    {
        return ((android.media.Rating) p1).isThumbUp();
    }

    private static float e(Object p1)
    {
        return ((android.media.Rating) p1).getStarRating();
    }

    private static float f(Object p1)
    {
        return ((android.media.Rating) p1).getPercentRating();
    }
}
