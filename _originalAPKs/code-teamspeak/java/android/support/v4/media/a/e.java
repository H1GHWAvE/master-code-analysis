package android.support.v4.media.a;
public final class e {

    public e()
    {
        return;
    }

    private static CharSequence a(Object p1, android.content.Context p2)
    {
        return ((android.media.MediaRouter$RouteCategory) p1).getName(p2);
    }

    private static java.util.List a(Object p1)
    {
        java.util.ArrayList v0_1 = new java.util.ArrayList();
        ((android.media.MediaRouter$RouteCategory) p1).getRoutes(v0_1);
        return v0_1;
    }

    private static int b(Object p1)
    {
        return ((android.media.MediaRouter$RouteCategory) p1).getSupportedTypes();
    }

    private static boolean c(Object p1)
    {
        return ((android.media.MediaRouter$RouteCategory) p1).isGroupable();
    }
}
