package android.support.v4.media.a;
public final class h {
    private reflect.Method a;

    public h()
    {
        if ((android.os.Build$VERSION.SDK_INT >= 16) && (android.os.Build$VERSION.SDK_INT <= 17)) {
            try {
                Class[] v2_1 = new Class[2];
                v2_1[0] = Integer.TYPE;
                v2_1[1] = android.media.MediaRouter$RouteInfo;
                this.a = android.media.MediaRouter.getMethod("selectRouteInt", v2_1);
            } catch (NoSuchMethodException v0) {
            }
            return;
        } else {
            throw new UnsupportedOperationException();
        }
    }

    private void a(Object p5, int p6, Object p7)
    {
        if ((((android.media.MediaRouter$RouteInfo) p7).getSupportedTypes() & 8388608) != 0) {
            ((android.media.MediaRouter) p5).selectRoute(p6, ((android.media.MediaRouter$RouteInfo) p7));
        } else {
            if (this.a == null) {
                android.util.Log.w("MediaRouterJellybean", "Cannot programmatically select non-user route because the platform is missing the selectRouteInt() method.  Media routing may not work.");
            } else {
                try {
                    reflect.InvocationTargetException v0_4 = this.a;
                    String v1_3 = new Object[2];
                    v1_3[0] = Integer.valueOf(p6);
                    v1_3[1] = ((android.media.MediaRouter$RouteInfo) p7);
                    v0_4.invoke(((android.media.MediaRouter) p5), v1_3);
                } catch (reflect.InvocationTargetException v0_6) {
                    android.util.Log.w("MediaRouterJellybean", "Cannot programmatically select non-user route.  Media routing may not work.", v0_6);
                } catch (reflect.InvocationTargetException v0_5) {
                    android.util.Log.w("MediaRouterJellybean", "Cannot programmatically select non-user route.  Media routing may not work.", v0_5);
                }
            }
        }
        return;
    }
}
