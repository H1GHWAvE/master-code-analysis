package android.support.v4.media;
public abstract class ae {
    public static final int a = 0;
    public static final int b = 1;
    public static final int c = 2;
    public final int d;
    public final int e;
    public int f;
    public android.support.v4.media.ag g;
    private Object h;

    private ae(int p1, int p2, int p3)
    {
        this.d = p1;
        this.e = p2;
        this.f = p3;
        return;
    }

    private void a(int p2)
    {
        this.f = p2;
        android.support.v4.media.ag v0_0 = this.a();
        if (v0_0 != null) {
            ((android.media.VolumeProvider) v0_0).setCurrentVolume(p2);
        }
        if (this.g != null) {
            this.g.a(this);
        }
        return;
    }

    private void a(android.support.v4.media.ag p1)
    {
        this.g = p1;
        return;
    }

    private int b()
    {
        return this.f;
    }

    private int c()
    {
        return this.d;
    }

    private int d()
    {
        return this.e;
    }

    private static void e()
    {
        return;
    }

    private static void f()
    {
        return;
    }

    public final Object a()
    {
        if ((this.h == null) && (android.os.Build$VERSION.SDK_INT >= 21)) {
            this.h = new android.support.v4.media.aj(this.d, this.e, this.f, new android.support.v4.media.af(this));
            Object v0_3 = this.h;
        } else {
            v0_3 = this.h;
        }
        return v0_3;
    }
}
