package android.support.v4.media;
public class d {

    public d()
    {
        return;
    }

    private static Object a()
    {
        return new android.media.MediaDescription$Builder();
    }

    private static Object a(Object p1)
    {
        return ((android.media.MediaDescription$Builder) p1).build();
    }

    private static void a(Object p0, android.graphics.Bitmap p1)
    {
        ((android.media.MediaDescription$Builder) p0).setIconBitmap(p1);
        return;
    }

    private static void a(Object p0, android.net.Uri p1)
    {
        ((android.media.MediaDescription$Builder) p0).setIconUri(p1);
        return;
    }

    private static void a(Object p0, android.os.Bundle p1)
    {
        ((android.media.MediaDescription$Builder) p0).setExtras(p1);
        return;
    }

    private static void a(Object p0, CharSequence p1)
    {
        ((android.media.MediaDescription$Builder) p0).setTitle(p1);
        return;
    }

    private static void a(Object p0, String p1)
    {
        ((android.media.MediaDescription$Builder) p0).setMediaId(p1);
        return;
    }

    private static void b(Object p0, CharSequence p1)
    {
        ((android.media.MediaDescription$Builder) p0).setSubtitle(p1);
        return;
    }

    private static void c(Object p0, CharSequence p1)
    {
        ((android.media.MediaDescription$Builder) p0).setDescription(p1);
        return;
    }
}
