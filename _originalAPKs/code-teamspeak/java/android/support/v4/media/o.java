package android.support.v4.media;
final class o implements android.os.Parcelable$Creator {

    o()
    {
        return;
    }

    private static android.support.v4.media.RatingCompat a(android.os.Parcel p4)
    {
        return new android.support.v4.media.RatingCompat(p4.readInt(), p4.readFloat(), 0);
    }

    private static android.support.v4.media.RatingCompat[] a(int p1)
    {
        android.support.v4.media.RatingCompat[] v0 = new android.support.v4.media.RatingCompat[p1];
        return v0;
    }

    public final synthetic Object createFromParcel(android.os.Parcel p5)
    {
        return new android.support.v4.media.RatingCompat(p5.readInt(), p5.readFloat(), 0);
    }

    public final bridge synthetic Object[] newArray(int p2)
    {
        android.support.v4.media.RatingCompat[] v0 = new android.support.v4.media.RatingCompat[p2];
        return v0;
    }
}
