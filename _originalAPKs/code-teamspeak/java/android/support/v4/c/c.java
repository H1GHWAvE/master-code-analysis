package android.support.v4.c;
public final class c {
    private static final android.support.v4.c.d a;

    static c()
    {
        if (android.os.Build$VERSION.SDK_INT < 16) {
            android.support.v4.c.c.a = new android.support.v4.c.e();
        } else {
            android.support.v4.c.c.a = new android.support.v4.c.f();
        }
        return;
    }

    private c()
    {
        return;
    }

    public static android.database.Cursor a(android.content.ContentResolver p8, android.net.Uri p9, String[] p10, String p11, String[] p12, String p13, android.support.v4.i.c p14)
    {
        return android.support.v4.c.c.a.a(p8, p9, p10, p11, p12, p13, p14);
    }
}
