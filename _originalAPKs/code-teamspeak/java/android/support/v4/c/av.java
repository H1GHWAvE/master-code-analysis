package android.support.v4.c;
public final class av {
    private static android.support.v4.c.av a;
    private final android.support.v4.c.ay b;

    private av()
    {
        if (android.os.Build$VERSION.SDK_INT < 9) {
            this.b = new android.support.v4.c.ax(0);
        } else {
            this.b = new android.support.v4.c.aw(0);
        }
        return;
    }

    private static android.support.v4.c.av a()
    {
        if (android.support.v4.c.av.a == null) {
            android.support.v4.c.av.a = new android.support.v4.c.av();
        }
        return android.support.v4.c.av.a;
    }

    private void a(android.content.SharedPreferences$Editor p2)
    {
        this.b.a(p2);
        return;
    }
}
