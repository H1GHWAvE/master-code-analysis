package android.support.v4.c;
public final class q extends android.content.ContentProvider {
    private static final String[] a = None;
    private static final String b = "android.support.FILE_PROVIDER_PATHS";
    private static final String c = "root-path";
    private static final String d = "files-path";
    private static final String e = "cache-path";
    private static final String f = "external-path";
    private static final String g = "name";
    private static final String h = "path";
    private static final java.io.File i;
    private static java.util.HashMap j;
    private android.support.v4.c.r k;

    static q()
    {
        java.util.HashMap v0_1 = new String[2];
        v0_1[0] = "_display_name";
        v0_1[1] = "_size";
        android.support.v4.c.q.a = v0_1;
        android.support.v4.c.q.i = new java.io.File("/");
        android.support.v4.c.q.j = new java.util.HashMap();
        return;
    }

    public q()
    {
        return;
    }

    private static int a(String p3)
    {
        IllegalArgumentException v0_14;
        if (!"r".equals(p3)) {
            if ((!"w".equals(p3)) && (!"wt".equals(p3))) {
                if (!"wa".equals(p3)) {
                    if (!"rw".equals(p3)) {
                        if (!"rwt".equals(p3)) {
                            throw new IllegalArgumentException(new StringBuilder("Invalid mode: ").append(p3).toString());
                        } else {
                            v0_14 = 1006632960;
                        }
                    } else {
                        v0_14 = 939524096;
                    }
                } else {
                    v0_14 = 704643072;
                }
            } else {
                v0_14 = 738197504;
            }
        } else {
            v0_14 = 268435456;
        }
        return v0_14;
    }

    private static android.net.Uri a(android.content.Context p1, String p2, java.io.File p3)
    {
        return android.support.v4.c.q.a(p1, p2).a(p3);
    }

    private static android.support.v4.c.r a(android.content.Context p10, String p11)
    {
        try {
            java.io.IOException v0_2 = ((android.support.v4.c.r) android.support.v4.c.q.j.get(p11));
        } catch (java.io.IOException v0_11) {
            throw v0_11;
        }
        if (v0_2 == null) {
            try {
                v0_2 = new android.support.v4.c.s(p11);
                StringBuilder v4_2 = p10.getPackageManager().resolveContentProvider(p11, 128).loadXmlMetaData(p10.getPackageManager(), "android.support.FILE_PROVIDER_PATHS");
            } catch (java.io.IOException v0_9) {
                throw new IllegalArgumentException("Failed to parse android.support.FILE_PROVIDER_PATHS meta-data", v0_9);
            } catch (java.io.IOException v0_10) {
                throw new IllegalArgumentException("Failed to parse android.support.FILE_PROVIDER_PATHS meta-data", v0_10);
            }
            if (v4_2 == null) {
                throw new IllegalArgumentException("Missing android.support.FILE_PROVIDER_PATHS meta-data");
            }
            while(true) {
                String v1_2 = v4_2.next();
                if (v1_2 == 1) {
                    break;
                }
                if (v1_2 == 2) {
                    String v1_6;
                    String v1_4 = v4_2.getName();
                    String v5_3 = v4_2.getAttributeValue(0, "name");
                    java.util.HashMap v6_2 = v4_2.getAttributeValue(0, "path");
                    if (!"root-path".equals(v1_4)) {
                        if (!"files-path".equals(v1_4)) {
                            if (!"cache-path".equals(v1_4)) {
                                if (!"external-path".equals(v1_4)) {
                                    v1_6 = 0;
                                } else {
                                    String v1_7 = android.os.Environment.getExternalStorageDirectory();
                                    String[] v7_9 = new String[1];
                                    v7_9[0] = v6_2;
                                    v1_6 = android.support.v4.c.q.a(v1_7, v7_9);
                                }
                            } else {
                                String v1_8 = p10.getCacheDir();
                                String[] v7_11 = new String[1];
                                v7_11[0] = v6_2;
                                v1_6 = android.support.v4.c.q.a(v1_8, v7_11);
                            }
                        } else {
                            String v1_9 = p10.getFilesDir();
                            String[] v7_13 = new String[1];
                            v7_13[0] = v6_2;
                            v1_6 = android.support.v4.c.q.a(v1_9, v7_13);
                        }
                    } else {
                        String[] v7_15 = new String[1];
                        v7_15[0] = v6_2;
                        v1_6 = android.support.v4.c.q.a(android.support.v4.c.q.i, v7_15);
                    }
                    if (v1_6 != null) {
                        if (!android.text.TextUtils.isEmpty(v5_3)) {
                            String v1_11 = v1_6.getCanonicalFile();
                            v0_2.a.put(v5_3, v1_11);
                        } else {
                            throw new IllegalArgumentException("Name must not be empty");
                        }
                    }
                }
            }
            android.support.v4.c.q.j.put(p11, v0_2);
        }
        return v0_2;
    }

    private static varargs java.io.File a(java.io.File p5, String[] p6)
    {
        int v2 = 0;
        java.io.File v1_0 = p5;
        while (v2 <= 0) {
            java.io.File v0_0;
            String v4 = p6[0];
            if (v4 == null) {
                v0_0 = v1_0;
            } else {
                v0_0 = new java.io.File(v1_0, v4);
            }
            v2++;
            v1_0 = v0_0;
        }
        return v1_0;
    }

    private static Object[] a(Object[] p2, int p3)
    {
        Object[] v0 = new Object[p3];
        System.arraycopy(p2, 0, v0, 0, p3);
        return v0;
    }

    private static String[] a(String[] p2, int p3)
    {
        String[] v0 = new String[p3];
        System.arraycopy(p2, 0, v0, 0, p3);
        return v0;
    }

    private static android.support.v4.c.r b(android.content.Context p9, String p10)
    {
        IllegalArgumentException v2_1 = new android.support.v4.c.s(p10);
        StringBuilder v3_2 = p9.getPackageManager().resolveContentProvider(p10, 128).loadXmlMetaData(p9.getPackageManager(), "android.support.FILE_PROVIDER_PATHS");
        if (v3_2 == null) {
            throw new IllegalArgumentException("Missing android.support.FILE_PROVIDER_PATHS meta-data");
        }
        while(true) {
            String v0_2 = v3_2.next();
            if (v0_2 == 1) {
                break;
            }
            if (v0_2 == 2) {
                String v0_5;
                String v0_3 = v3_2.getName();
                String v4_3 = v3_2.getAttributeValue(0, "name");
                java.util.HashMap v5_1 = v3_2.getAttributeValue(0, "path");
                if (!"root-path".equals(v0_3)) {
                    if (!"files-path".equals(v0_3)) {
                        if (!"cache-path".equals(v0_3)) {
                            if (!"external-path".equals(v0_3)) {
                                v0_5 = 0;
                            } else {
                                String v0_6 = android.os.Environment.getExternalStorageDirectory();
                                String[] v6_7 = new String[1];
                                v6_7[0] = v5_1;
                                v0_5 = android.support.v4.c.q.a(v0_6, v6_7);
                            }
                        } else {
                            String v0_7 = p9.getCacheDir();
                            String[] v6_8 = new String[1];
                            v6_8[0] = v5_1;
                            v0_5 = android.support.v4.c.q.a(v0_7, v6_8);
                        }
                    } else {
                        String v0_8 = p9.getFilesDir();
                        String[] v6_9 = new String[1];
                        v6_9[0] = v5_1;
                        v0_5 = android.support.v4.c.q.a(v0_8, v6_9);
                    }
                } else {
                    String[] v6_10 = new String[1];
                    v6_10[0] = v5_1;
                    v0_5 = android.support.v4.c.q.a(android.support.v4.c.q.i, v6_10);
                }
                if (v0_5 != null) {
                    if (!android.text.TextUtils.isEmpty(v4_3)) {
                        try {
                            String v0_10 = v0_5.getCanonicalFile();
                            v2_1.a.put(v4_3, v0_10);
                        } catch (java.io.IOException v1_1) {
                            throw new IllegalArgumentException(new StringBuilder("Failed to resolve canonical path for ").append(v0_10).toString(), v1_1);
                        }
                    } else {
                        throw new IllegalArgumentException("Name must not be empty");
                    }
                }
            }
        }
        return v2_1;
    }

    public final void attachInfo(android.content.Context p3, android.content.pm.ProviderInfo p4)
    {
        super.attachInfo(p3, p4);
        if (!p4.exported) {
            if (p4.grantUriPermissions) {
                this.k = android.support.v4.c.q.a(p3, p4.authority);
                return;
            } else {
                throw new SecurityException("Provider must grant uri permissions");
            }
        } else {
            throw new SecurityException("Provider must not be exported");
        }
    }

    public final int delete(android.net.Uri p2, String p3, String[] p4)
    {
        int v0_3;
        if (!this.k.a(p2).delete()) {
            v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        return v0_3;
    }

    public final String getType(android.net.Uri p4)
    {
        String v0_4;
        String v0_1 = this.k.a(p4);
        android.webkit.MimeTypeMap v1_1 = v0_1.getName().lastIndexOf(46);
        if (v1_1 < null) {
            v0_4 = "application/octet-stream";
        } else {
            v0_4 = android.webkit.MimeTypeMap.getSingleton().getMimeTypeFromExtension(v0_1.getName().substring((v1_1 + 1)));
            if (v0_4 == null) {
            }
        }
        return v0_4;
    }

    public final android.net.Uri insert(android.net.Uri p3, android.content.ContentValues p4)
    {
        throw new UnsupportedOperationException("No external inserts");
    }

    public final boolean onCreate()
    {
        return 1;
    }

    public final android.os.ParcelFileDescriptor openFile(android.net.Uri p4, String p5)
    {
        IllegalArgumentException v0_15;
        String v1_0 = this.k.a(p4);
        if (!"r".equals(p5)) {
            if ((!"w".equals(p5)) && (!"wt".equals(p5))) {
                if (!"wa".equals(p5)) {
                    if (!"rw".equals(p5)) {
                        if (!"rwt".equals(p5)) {
                            throw new IllegalArgumentException(new StringBuilder("Invalid mode: ").append(p5).toString());
                        } else {
                            v0_15 = 1006632960;
                        }
                    } else {
                        v0_15 = 939524096;
                    }
                } else {
                    v0_15 = 704643072;
                }
            } else {
                v0_15 = 738197504;
            }
        } else {
            v0_15 = 268435456;
        }
        return android.os.ParcelFileDescriptor.open(v1_0, v0_15);
    }

    public final android.database.Cursor query(android.net.Uri p11, String[] p12, String p13, String[] p14, String p15)
    {
        java.io.File v4 = this.k.a(p11);
        if (p12 == null) {
            p12 = android.support.v4.c.q.a;
        }
        String[] v5 = new String[p12.length];
        Object[] v6 = new Object[p12.length];
        int v7 = p12.length;
        int v2_0 = 0;
        boolean v1_0 = 0;
        while (v2_0 < v7) {
            boolean v0_6;
            boolean v0_4 = p12[v2_0];
            if (!"_display_name".equals(v0_4)) {
                if (!"_size".equals(v0_4)) {
                    v0_6 = v1_0;
                } else {
                    v5[v1_0] = "_size";
                    v0_6 = (v1_0 + 1);
                    v6[v1_0] = Long.valueOf(v4.length());
                }
            } else {
                v5[v1_0] = "_display_name";
                v0_6 = (v1_0 + 1);
                v6[v1_0] = v4.getName();
            }
            v2_0++;
            v1_0 = v0_6;
        }
        boolean v0_3 = new String[v1_0];
        System.arraycopy(v5, 0, v0_3, 0, v1_0);
        int v2_1 = new Object[v1_0];
        System.arraycopy(v6, 0, v2_1, 0, v1_0);
        boolean v1_2 = new android.database.MatrixCursor(v0_3, 1);
        v1_2.addRow(v2_1);
        return v1_2;
    }

    public final int update(android.net.Uri p3, android.content.ContentValues p4, String p5, String[] p6)
    {
        throw new UnsupportedOperationException("No external updates");
    }
}
