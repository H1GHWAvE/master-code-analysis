package android.support.v4.f.b;
final class j extends android.hardware.fingerprint.FingerprintManager$AuthenticationCallback {
    final synthetic android.support.v4.f.b.k a;

    j(android.support.v4.f.b.k p1)
    {
        this.a = p1;
        return;
    }

    public final void onAuthenticationError(int p1, CharSequence p2)
    {
        return;
    }

    public final void onAuthenticationFailed()
    {
        return;
    }

    public final void onAuthenticationHelp(int p1, CharSequence p2)
    {
        return;
    }

    public final void onAuthenticationSucceeded(android.hardware.fingerprint.FingerprintManager$AuthenticationResult p5)
    {
        android.support.v4.f.b.m v0_3;
        android.support.v4.f.b.k v1 = this.a;
        javax.crypto.Mac v3_0 = p5.getCryptoObject();
        if (v3_0 == null) {
            v0_3 = 0;
        } else {
            if (v3_0.getCipher() == null) {
                if (v3_0.getSignature() == null) {
                    if (v3_0.getMac() == null) {
                    } else {
                        v0_3 = new android.support.v4.f.b.m(v3_0.getMac());
                    }
                } else {
                    v0_3 = new android.support.v4.f.b.m(v3_0.getSignature());
                }
            } else {
                v0_3 = new android.support.v4.f.b.m(v3_0.getCipher());
            }
        }
        v1.a(new android.support.v4.f.b.l(v0_3));
        return;
    }
}
