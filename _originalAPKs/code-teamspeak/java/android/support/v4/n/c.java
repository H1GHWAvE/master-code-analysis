package android.support.v4.n;
public final class c {
    private final java.io.File a;
    private final java.io.File b;

    private c(java.io.File p4)
    {
        this.a = p4;
        this.b = new java.io.File(new StringBuilder().append(p4.getPath()).append(".bak").toString());
        return;
    }

    private java.io.File a()
    {
        return this.a;
    }

    private void a(java.io.FileOutputStream p4)
    {
        if (p4 != null) {
            android.support.v4.n.c.c(p4);
            try {
                p4.close();
                this.b.delete();
            } catch (java.io.IOException v0_1) {
                android.util.Log.w("AtomicFile", "finishWrite: Got exception:", v0_1);
            }
        }
        return;
    }

    private void b()
    {
        this.a.delete();
        this.b.delete();
        return;
    }

    private void b(java.io.FileOutputStream p4)
    {
        if (p4 != null) {
            android.support.v4.n.c.c(p4);
            try {
                p4.close();
                this.a.delete();
                this.b.renameTo(this.a);
            } catch (java.io.IOException v0_2) {
                android.util.Log.w("AtomicFile", "failWrite: Got exception:", v0_2);
            }
        }
        return;
    }

    private java.io.FileOutputStream c()
    {
        if (this.a.exists()) {
            if (this.b.exists()) {
                this.a.delete();
            } else {
                if (!this.a.renameTo(this.b)) {
                    android.util.Log.w("AtomicFile", new StringBuilder("Couldn\'t rename file ").append(this.a).append(" to backup file ").append(this.b).toString());
                }
            }
        }
        try {
            java.io.IOException v0_9 = new java.io.FileOutputStream(this.a);
        } catch (java.io.IOException v0) {
            if (this.a.getParentFile().mkdir()) {
                try {
                    v0_9 = new java.io.FileOutputStream(this.a);
                } catch (java.io.IOException v0) {
                    throw new java.io.IOException(new StringBuilder("Couldn\'t create ").append(this.a).toString());
                }
            } else {
                throw new java.io.IOException(new StringBuilder("Couldn\'t create directory ").append(this.a).toString());
            }
        }
        return v0_9;
    }

    private static boolean c(java.io.FileOutputStream p1)
    {
        if (p1 == null) {
            int v0_1 = 1;
        } else {
            try {
                p1.getFD().sync();
            } catch (int v0) {
                v0_1 = 0;
            }
        }
        return v0_1;
    }

    private java.io.FileInputStream d()
    {
        if (this.b.exists()) {
            this.a.delete();
            this.b.renameTo(this.a);
        }
        return new java.io.FileInputStream(this.a);
    }

    private byte[] e()
    {
        int v0_0 = 0;
        if (this.b.exists()) {
            this.a.delete();
            this.b.renameTo(this.a);
        }
        java.io.FileInputStream v3_1 = new java.io.FileInputStream(this.a);
        try {
            byte[] v1_6 = new byte[v3_1.available()];
        } catch (int v0_4) {
            v3_1.close();
            throw v0_4;
        }
        while(true) {
            int v2_3 = v3_1.read(v1_6, v0_0, (v1_6.length - v0_0));
            if (v2_3 <= 0) {
                break;
            }
            int v2_4 = (v2_3 + v0_0);
            int v0_2;
            int v0_1 = v3_1.available();
            if (v0_1 <= (v1_6.length - v2_4)) {
                v0_2 = v1_6;
            } else {
                v0_2 = new byte[(v0_1 + v2_4)];
                System.arraycopy(v1_6, 0, v0_2, 0, v2_4);
            }
            v1_6 = v0_2;
            v0_0 = v2_4;
        }
        v3_1.close();
        return v1_6;
    }
}
