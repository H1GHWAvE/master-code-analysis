package android.support.v4.n;
public final class f {
    static final int[] a;
    static final long[] b;
    static final Object[] c;

    static f()
    {
        Object[] v0_0 = new int[0];
        android.support.v4.n.f.a = v0_0;
        Object[] v0_1 = new long[0];
        android.support.v4.n.f.b = v0_1;
        Object[] v0_2 = new Object[0];
        android.support.v4.n.f.c = v0_2;
        return;
    }

    f()
    {
        return;
    }

    public static int a(int p1)
    {
        return (android.support.v4.n.f.c((p1 * 4)) / 4);
    }

    public static int a(int[] p4, int p5, int p6)
    {
        int v2 = (p5 - 1);
        int v1_1 = 0;
        while (v1_1 <= v2) {
            int v0_1 = ((v1_1 + v2) >> 1);
            int v3 = p4[v0_1];
            if (v3 >= p6) {
                if (v3 > p6) {
                    v2 = (v0_1 - 1);
                }
            } else {
                v1_1 = (v0_1 + 1);
            }
            return v0_1;
        }
        v0_1 = (v1_1 ^ -1);
        return v0_1;
    }

    static int a(long[] p6, int p7, long p8)
    {
        int v2_0 = (p7 - 1);
        int v1_1 = 0;
        while (v1_1 <= v2_0) {
            int v0_1 = ((v1_1 + v2_0) >> 1);
            long v4 = p6[v0_1];
            if (v4 >= p8) {
                if (v4 > p8) {
                    v2_0 = (v0_1 - 1);
                }
            } else {
                v1_1 = (v0_1 + 1);
            }
            return v0_1;
        }
        v0_1 = (v1_1 ^ -1);
        return v0_1;
    }

    public static boolean a(Object p1, Object p2)
    {
        if ((p1 != p2) && ((p1 == null) || (!p1.equals(p2)))) {
            int v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    public static int b(int p1)
    {
        return (android.support.v4.n.f.c((p1 * 8)) / 8);
    }

    private static int c(int p3)
    {
        int v0_0 = 4;
        while (v0_0 < 32) {
            if (p3 > ((1 << v0_0) - 12)) {
                v0_0++;
            } else {
                p3 = ((1 << v0_0) - 12);
                break;
            }
        }
        return p3;
    }
}
