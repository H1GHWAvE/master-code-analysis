package android.support.v4.n;
public class a extends android.support.v4.n.v implements java.util.Map {
    android.support.v4.n.k a;

    public a()
    {
        return;
    }

    private a(int p1)
    {
        this(p1);
        return;
    }

    private a(android.support.v4.n.v p1)
    {
        this(p1);
        return;
    }

    private android.support.v4.n.k a()
    {
        if (this.a == null) {
            this.a = new android.support.v4.n.b(this);
        }
        return this.a;
    }

    private boolean a(java.util.Collection p2)
    {
        return android.support.v4.n.k.a(this, p2);
    }

    private boolean b(java.util.Collection p2)
    {
        return android.support.v4.n.k.b(this, p2);
    }

    private boolean c(java.util.Collection p2)
    {
        return android.support.v4.n.k.c(this, p2);
    }

    public java.util.Set entrySet()
    {
        android.support.v4.n.m v0_0 = this.a();
        if (v0_0.b == null) {
            v0_0.b = new android.support.v4.n.m(v0_0);
        }
        return v0_0.b;
    }

    public java.util.Set keySet()
    {
        android.support.v4.n.n v0_0 = this.a();
        if (v0_0.c == null) {
            v0_0.c = new android.support.v4.n.n(v0_0);
        }
        return v0_0.c;
    }

    public void putAll(java.util.Map p4)
    {
        this.a((this.h + p4.size()));
        java.util.Iterator v1_1 = p4.entrySet().iterator();
        while (v1_1.hasNext()) {
            Object v0_5 = ((java.util.Map$Entry) v1_1.next());
            this.put(v0_5.getKey(), v0_5.getValue());
        }
        return;
    }

    public java.util.Collection values()
    {
        android.support.v4.n.p v0_0 = this.a();
        if (v0_0.d == null) {
            v0_0.d = new android.support.v4.n.p(v0_0);
        }
        return v0_0.d;
    }
}
