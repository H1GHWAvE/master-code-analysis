package android.support.v4.n;
public class j {
    private final java.util.LinkedHashMap a;
    private int b;
    private int c;
    private int d;
    private int e;
    private int f;
    private int g;
    private int h;

    public j(int p5)
    {
        if (p5 > 0) {
            this.c = p5;
            this.a = new java.util.LinkedHashMap(0, 1061158912, 1);
            return;
        } else {
            throw new IllegalArgumentException("maxSize <= 0");
        }
    }

    private static void a()
    {
        return;
    }

    private void a(int p3)
    {
        if (p3 > 0) {
            this.c = p3;
            this.b(p3);
            return;
        } else {
            throw new IllegalArgumentException("maxSize <= 0");
        }
    }

    private int b(Object p4, Object p5)
    {
        IllegalStateException v0_0 = this.b(p5);
        if (v0_0 >= null) {
            return v0_0;
        } else {
            throw new IllegalStateException(new StringBuilder("Negative size: ").append(p4).append("=").append(p5).toString());
        }
    }

    private static Object b()
    {
        return 0;
    }

    private void b(int p4)
    {
        try {
            while ((this.b >= 0) && ((!this.a.isEmpty()) || (this.b == 0))) {
                if ((this.b > p4) && (!this.a.isEmpty())) {
                    int v0_11 = ((java.util.Map$Entry) this.a.entrySet().iterator().next());
                    Object v1_0 = v0_11.getKey();
                    int v0_12 = v0_11.getValue();
                    this.a.remove(v1_0);
                    this.b = (this.b - this.b(v1_0, v0_12));
                    this.f = (this.f + 1);
                } else {
                    return;
                }
            }
        } catch (int v0_19) {
            throw v0_19;
        }
        throw new IllegalStateException(new StringBuilder().append(this.getClass().getName()).append(".sizeOf() is reporting inconsistent results!").toString());
    }

    private Object c(Object p4)
    {
        if (p4 != null) {
            try {
                Throwable v0_1 = this.a.remove(p4);
            } catch (Throwable v0_2) {
                throw v0_2;
            }
            if (v0_1 != null) {
                this.b = (this.b - this.b(p4, v0_1));
            }
            return v0_1;
        } else {
            throw new NullPointerException("key == null");
        }
    }

    private void c()
    {
        this.b(-1);
        return;
    }

    private declared_synchronized int d()
    {
        try {
            return this.b;
        } catch (Throwable v0_1) {
            throw v0_1;
        }
    }

    private declared_synchronized int e()
    {
        try {
            return this.c;
        } catch (Throwable v0_1) {
            throw v0_1;
        }
    }

    private declared_synchronized int f()
    {
        try {
            return this.g;
        } catch (Throwable v0_1) {
            throw v0_1;
        }
    }

    private declared_synchronized int g()
    {
        try {
            return this.h;
        } catch (Throwable v0_1) {
            throw v0_1;
        }
    }

    private declared_synchronized int h()
    {
        try {
            return this.e;
        } catch (Throwable v0_1) {
            throw v0_1;
        }
    }

    private declared_synchronized int i()
    {
        try {
            return this.d;
        } catch (Throwable v0_1) {
            throw v0_1;
        }
    }

    private declared_synchronized int j()
    {
        try {
            return this.f;
        } catch (Throwable v0_1) {
            throw v0_1;
        }
    }

    private declared_synchronized java.util.Map k()
    {
        try {
            return new java.util.LinkedHashMap(this.a);
        } catch (Throwable v0_2) {
            throw v0_2;
        }
    }

    public final Object a(Object p3)
    {
        if (p3 != null) {
            try {
                int v0_1 = this.a.get(p3);
            } catch (int v0_4) {
                throw v0_4;
            }
            if (v0_1 == 0) {
                this.h = (this.h + 1);
                v0_1 = 0;
            } else {
                this.g = (this.g + 1);
            }
            return v0_1;
        } else {
            throw new NullPointerException("key == null");
        }
    }

    public final Object a(Object p4, Object p5)
    {
        if ((p4 != null) && (p5 != null)) {
            this.d = (this.d + 1);
            this.b = (this.b + this.b(p4, p5));
            Throwable v0_5 = this.a.put(p4, p5);
            if (v0_5 != null) {
                this.b = (this.b - this.b(p4, v0_5));
            }
            this.b(this.c);
            return v0_5;
        } else {
            throw new NullPointerException("key == null || value == null");
        }
    }

    public int b(Object p2)
    {
        return 1;
    }

    public final declared_synchronized String toString()
    {
        String v0_0 = 0;
        try {
            String v1_1 = (this.g + this.h);
        } catch (String v0_3) {
            throw v0_3;
        }
        if (v1_1 != null) {
            v0_0 = ((this.g * 100) / v1_1);
        }
        Object[] v2_2 = new Object[4];
        v2_2[0] = Integer.valueOf(this.c);
        v2_2[1] = Integer.valueOf(this.g);
        v2_2[2] = Integer.valueOf(this.h);
        v2_2[3] = Integer.valueOf(v0_0);
        return String.format("LruCache[maxSize=%d,hits=%d,misses=%d,hitRate=%d%%]", v2_2);
    }
}
