package android.support.v4.n;
public final class q {
    public final Object a;
    public final Object b;

    private q(Object p1, Object p2)
    {
        this.a = p1;
        this.b = p2;
        return;
    }

    private static boolean a(Object p1, Object p2)
    {
        if ((p1 != p2) && ((p1 == null) || (!p1.equals(p2)))) {
            int v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    private static android.support.v4.n.q b(Object p1, Object p2)
    {
        return new android.support.v4.n.q(p1, p2);
    }

    public final boolean equals(Object p4)
    {
        int v0 = 0;
        if (((p4 instanceof android.support.v4.n.q)) && ((android.support.v4.n.q.a(((android.support.v4.n.q) p4).a, this.a)) && (android.support.v4.n.q.a(((android.support.v4.n.q) p4).b, this.b)))) {
            v0 = 1;
        }
        return v0;
    }

    public final int hashCode()
    {
        int v0_2;
        int v1_0 = 0;
        if (this.a != null) {
            v0_2 = this.a.hashCode();
        } else {
            v0_2 = 0;
        }
        if (this.b != null) {
            v1_0 = this.b.hashCode();
        }
        return (v0_2 ^ v1_0);
    }
}
