package android.support.v4.app;
final class fi implements android.content.ServiceConnection, android.os.Handler$Callback {
    private static final int b = 0;
    private static final int c = 1;
    private static final int d = 2;
    private static final int e = 3;
    private static final String f = "binder";
    final android.os.Handler a;
    private final android.content.Context g;
    private final android.os.HandlerThread h;
    private final java.util.Map i;
    private java.util.Set j;

    public fi(android.content.Context p3)
    {
        this.i = new java.util.HashMap();
        this.j = new java.util.HashSet();
        this.g = p3;
        this.h = new android.os.HandlerThread("NotificationManagerCompat");
        this.h.start();
        this.a = new android.os.Handler(this.h.getLooper(), this);
        return;
    }

    private void a()
    {
        java.util.Iterator v1_0 = android.support.v4.app.fa.a(this.g);
        if (!v1_0.equals(this.j)) {
            this.j = v1_0;
            android.support.v4.app.fj v0_5 = this.g.getPackageManager().queryIntentServices(new android.content.Intent().setAction("android.support.BIND_NOTIFICATION_SIDE_CHANNEL"), 4);
            java.util.HashSet v2_4 = new java.util.HashSet();
            String v3_2 = v0_5.iterator();
            while (v3_2.hasNext()) {
                android.support.v4.app.fj v0_18 = ((android.content.pm.ResolveInfo) v3_2.next());
                if (v1_0.contains(v0_18.serviceInfo.packageName)) {
                    String v4_14 = new android.content.ComponentName(v0_18.serviceInfo.packageName, v0_18.serviceInfo.name);
                    if (v0_18.serviceInfo.permission == null) {
                        v2_4.add(v4_14);
                    } else {
                        android.util.Log.w("NotifManCompat", new StringBuilder("Permission present on component ").append(v4_14).append(", not adding listener record.").toString());
                    }
                }
            }
            java.util.Iterator v1_1 = v2_4.iterator();
            while (v1_1.hasNext()) {
                android.support.v4.app.fj v0_16 = ((android.content.ComponentName) v1_1.next());
                if (!this.i.containsKey(v0_16)) {
                    if (android.util.Log.isLoggable("NotifManCompat", 3)) {
                        android.util.Log.d("NotifManCompat", new StringBuilder("Adding listener record for ").append(v0_16).toString());
                    }
                    this.i.put(v0_16, new android.support.v4.app.fj(v0_16));
                }
            }
            java.util.Iterator v1_2 = this.i.entrySet().iterator();
            while (v1_2.hasNext()) {
                android.support.v4.app.fj v0_12 = ((java.util.Map$Entry) v1_2.next());
                if (!v2_4.contains(v0_12.getKey())) {
                    if (android.util.Log.isLoggable("NotifManCompat", 3)) {
                        android.util.Log.d("NotifManCompat", new StringBuilder("Removing listener record for ").append(v0_12.getKey()).toString());
                    }
                    this.b(((android.support.v4.app.fj) v0_12.getValue()));
                    v1_2.remove();
                }
            }
        }
        return;
    }

    private void a(android.content.ComponentName p2)
    {
        android.support.v4.app.fj v0_2 = ((android.support.v4.app.fj) this.i.get(p2));
        if (v0_2 != null) {
            this.b(v0_2);
        }
        return;
    }

    private void a(android.content.ComponentName p3, android.os.IBinder p4)
    {
        android.support.v4.app.fj v0_2 = ((android.support.v4.app.fj) this.i.get(p3));
        if (v0_2 != null) {
            v0_2.c = android.support.v4.app.cm.a(p4);
            v0_2.e = 0;
            this.d(v0_2);
        }
        return;
    }

    private void a(android.support.v4.app.fk p3)
    {
        this.a.obtainMessage(0, p3).sendToTarget();
        return;
    }

    private boolean a(android.support.v4.app.fj p4)
    {
        boolean v0_9;
        if (!p4.b) {
            p4.b = this.g.bindService(new android.content.Intent("android.support.BIND_NOTIFICATION_SIDE_CHANNEL").setComponent(p4.a), this, android.support.v4.app.fa.a());
            if (!p4.b) {
                android.util.Log.w("NotifManCompat", new StringBuilder("Unable to bind to listener ").append(p4.a).toString());
                this.g.unbindService(this);
            } else {
                p4.e = 0;
            }
            v0_9 = p4.b;
        } else {
            v0_9 = 1;
        }
        return v0_9;
    }

    private void b(android.content.ComponentName p2)
    {
        android.support.v4.app.fj v0_2 = ((android.support.v4.app.fj) this.i.get(p2));
        if (v0_2 != null) {
            this.d(v0_2);
        }
        return;
    }

    private void b(android.support.v4.app.fj p2)
    {
        if (p2.b) {
            this.g.unbindService(this);
            p2.b = 0;
        }
        p2.c = 0;
        return;
    }

    private void b(android.support.v4.app.fk p9)
    {
        java.util.Iterator v1_0 = android.support.v4.app.fa.a(this.g);
        if (!v1_0.equals(this.j)) {
            this.j = v1_0;
            android.support.v4.app.fj v0_5 = this.g.getPackageManager().queryIntentServices(new android.content.Intent().setAction("android.support.BIND_NOTIFICATION_SIDE_CHANNEL"), 4);
            java.util.LinkedList v2_4 = new java.util.HashSet();
            String v3_2 = v0_5.iterator();
            while (v3_2.hasNext()) {
                android.support.v4.app.fj v0_23 = ((android.content.pm.ResolveInfo) v3_2.next());
                if (v1_0.contains(v0_23.serviceInfo.packageName)) {
                    String v4_14 = new android.content.ComponentName(v0_23.serviceInfo.packageName, v0_23.serviceInfo.name);
                    if (v0_23.serviceInfo.permission == null) {
                        v2_4.add(v4_14);
                    } else {
                        android.util.Log.w("NotifManCompat", new StringBuilder("Permission present on component ").append(v4_14).append(", not adding listener record.").toString());
                    }
                }
            }
            java.util.Iterator v1_1 = v2_4.iterator();
            while (v1_1.hasNext()) {
                android.support.v4.app.fj v0_21 = ((android.content.ComponentName) v1_1.next());
                if (!this.i.containsKey(v0_21)) {
                    if (android.util.Log.isLoggable("NotifManCompat", 3)) {
                        android.util.Log.d("NotifManCompat", new StringBuilder("Adding listener record for ").append(v0_21).toString());
                    }
                    this.i.put(v0_21, new android.support.v4.app.fj(v0_21));
                }
            }
            java.util.Iterator v1_2 = this.i.entrySet().iterator();
            while (v1_2.hasNext()) {
                android.support.v4.app.fj v0_17 = ((java.util.Map$Entry) v1_2.next());
                if (!v2_4.contains(v0_17.getKey())) {
                    if (android.util.Log.isLoggable("NotifManCompat", 3)) {
                        android.util.Log.d("NotifManCompat", new StringBuilder("Removing listener record for ").append(v0_17.getKey()).toString());
                    }
                    this.b(((android.support.v4.app.fj) v0_17.getValue()));
                    v1_2.remove();
                }
            }
        }
        java.util.Iterator v1_3 = this.i.values().iterator();
        while (v1_3.hasNext()) {
            android.support.v4.app.fj v0_15 = ((android.support.v4.app.fj) v1_3.next());
            v0_15.d.add(p9);
            this.d(v0_15);
        }
        return;
    }

    private void c(android.support.v4.app.fj p7)
    {
        if (!this.a.hasMessages(3, p7.a)) {
            p7.e = (p7.e + 1);
            if (p7.e <= 6) {
                int v0_7 = ((1 << (p7.e - 1)) * 1000);
                if (android.util.Log.isLoggable("NotifManCompat", 3)) {
                    android.util.Log.d("NotifManCompat", new StringBuilder("Scheduling retry for ").append(v0_7).append(" ms").toString());
                }
                this.a.sendMessageDelayed(this.a.obtainMessage(3, p7.a), ((long) v0_7));
            } else {
                android.util.Log.w("NotifManCompat", new StringBuilder("Giving up on delivering ").append(p7.d.size()).append(" tasks to ").append(p7.a).append(" after ").append(p7.e).append(" retries").toString());
                p7.d.clear();
            }
        }
        return;
    }

    private void d(android.support.v4.app.fj p6)
    {
        if (android.util.Log.isLoggable("NotifManCompat", 3)) {
            android.util.Log.d("NotifManCompat", new StringBuilder("Processing component ").append(p6.a).append(", ").append(p6.d.size()).append(" queued tasks").toString());
        }
        if (!p6.d.isEmpty()) {
            String v0_14;
            if (!p6.b) {
                p6.b = this.g.bindService(new android.content.Intent("android.support.BIND_NOTIFICATION_SIDE_CHANNEL").setComponent(p6.a), this, android.support.v4.app.fa.a());
                if (!p6.b) {
                    android.util.Log.w("NotifManCompat", new StringBuilder("Unable to bind to listener ").append(p6.a).toString());
                    this.g.unbindService(this);
                } else {
                    p6.e = 0;
                }
                v0_14 = p6.b;
            } else {
                v0_14 = 1;
            }
            if ((v0_14 == null) || (p6.c == null)) {
                this.c(p6);
                return;
            }
            while(true) {
                String v0_18 = ((android.support.v4.app.fk) p6.d.peek());
                if (v0_18 == null) {
                    break;
                }
                try {
                    if (android.util.Log.isLoggable("NotifManCompat", 3)) {
                        android.util.Log.d("NotifManCompat", new StringBuilder("Sending task ").append(v0_18).toString());
                    }
                    v0_18.a(p6.c);
                    p6.d.remove();
                } catch (String v0_19) {
                    android.util.Log.w("NotifManCompat", new StringBuilder("RemoteException communicating with ").append(p6.a).toString(), v0_19);
                    break;
                } catch (String v0) {
                    if (!android.util.Log.isLoggable("NotifManCompat", 3)) {
                        break;
                    }
                    android.util.Log.d("NotifManCompat", new StringBuilder("Remote service has died: ").append(p6.a).toString());
                    break;
                }
            }
            if (p6.d.isEmpty()) {
                return;
            } else {
                this.c(p6);
                return;
            }
        }
        return;
    }

    public final boolean handleMessage(android.os.Message p11)
    {
        int v0_5;
        switch (p11.what) {
            case 0:
                int v0_16 = ((android.support.v4.app.fk) p11.obj);
                java.util.Iterator v3_2 = android.support.v4.app.fa.a(this.g);
                if (!v3_2.equals(this.j)) {
                    this.j = v3_2;
                    android.support.v4.app.fj v1_8 = this.g.getPackageManager().queryIntentServices(new android.content.Intent().setAction("android.support.BIND_NOTIFICATION_SIDE_CHANNEL"), 4);
                    java.util.LinkedList v4_5 = new java.util.HashSet();
                    String v5_2 = v1_8.iterator();
                    while (v5_2.hasNext()) {
                        android.support.v4.app.fj v1_26 = ((android.content.pm.ResolveInfo) v5_2.next());
                        if (v3_2.contains(v1_26.serviceInfo.packageName)) {
                            String v6_14 = new android.content.ComponentName(v1_26.serviceInfo.packageName, v1_26.serviceInfo.name);
                            if (v1_26.serviceInfo.permission == null) {
                                v4_5.add(v6_14);
                            } else {
                                android.util.Log.w("NotifManCompat", new StringBuilder("Permission present on component ").append(v6_14).append(", not adding listener record.").toString());
                            }
                        }
                    }
                    java.util.Iterator v3_3 = v4_5.iterator();
                    while (v3_3.hasNext()) {
                        android.support.v4.app.fj v1_24 = ((android.content.ComponentName) v3_3.next());
                        if (!this.i.containsKey(v1_24)) {
                            if (android.util.Log.isLoggable("NotifManCompat", 3)) {
                                android.util.Log.d("NotifManCompat", new StringBuilder("Adding listener record for ").append(v1_24).toString());
                            }
                            this.i.put(v1_24, new android.support.v4.app.fj(v1_24));
                        }
                    }
                    java.util.Iterator v3_4 = this.i.entrySet().iterator();
                    while (v3_4.hasNext()) {
                        android.support.v4.app.fj v1_20 = ((java.util.Map$Entry) v3_4.next());
                        if (!v4_5.contains(v1_20.getKey())) {
                            if (android.util.Log.isLoggable("NotifManCompat", 3)) {
                                android.util.Log.d("NotifManCompat", new StringBuilder("Removing listener record for ").append(v1_20.getKey()).toString());
                            }
                            this.b(((android.support.v4.app.fj) v1_20.getValue()));
                            v3_4.remove();
                        }
                    }
                }
                java.util.Iterator v3_5 = this.i.values().iterator();
                while (v3_5.hasNext()) {
                    android.support.v4.app.fj v1_18 = ((android.support.v4.app.fj) v3_5.next());
                    v1_18.d.add(v0_16);
                    this.d(v1_18);
                }
                v0_5 = 1;
                break;
            case 1:
                int v0_11 = ((android.support.v4.app.fh) p11.obj);
                int v0_14 = ((android.support.v4.app.fj) this.i.get(v0_11.a));
                if (v0_14 != 0) {
                    v0_14.c = android.support.v4.app.cm.a(v0_11.b);
                    v0_14.e = 0;
                    this.d(v0_14);
                }
                v0_5 = 1;
                break;
            case 2:
                int v0_9 = ((android.support.v4.app.fj) this.i.get(((android.content.ComponentName) p11.obj)));
                if (v0_9 != 0) {
                    this.b(v0_9);
                }
                v0_5 = 1;
                break;
            case 3:
                int v0_4 = ((android.support.v4.app.fj) this.i.get(((android.content.ComponentName) p11.obj)));
                if (v0_4 != 0) {
                    this.d(v0_4);
                }
                v0_5 = 1;
                break;
            default:
                v0_5 = 0;
        }
        return v0_5;
    }

    public final void onServiceConnected(android.content.ComponentName p4, android.os.IBinder p5)
    {
        if (android.util.Log.isLoggable("NotifManCompat", 3)) {
            android.util.Log.d("NotifManCompat", new StringBuilder("Connected to service ").append(p4).toString());
        }
        this.a.obtainMessage(1, new android.support.v4.app.fh(p4, p5)).sendToTarget();
        return;
    }

    public final void onServiceDisconnected(android.content.ComponentName p4)
    {
        if (android.util.Log.isLoggable("NotifManCompat", 3)) {
            android.util.Log.d("NotifManCompat", new StringBuilder("Disconnected from service ").append(p4).toString());
        }
        this.a.obtainMessage(2, p4).sendToTarget();
        return;
    }
}
