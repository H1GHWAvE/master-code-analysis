package android.support.v4.app;
 class br implements android.view.animation.Animation$AnimationListener {
    private boolean a;
    private android.view.View b;

    public br(android.view.View p2, android.view.animation.Animation p3)
    {
        this.a = 0;
        if ((p2 != null) && (p3 != null)) {
            this.b = p2;
        }
        return;
    }

    static synthetic android.view.View a(android.support.v4.app.br p1)
    {
        return p1.b;
    }

    public void onAnimationEnd(android.view.animation.Animation p3)
    {
        if (this.a) {
            this.b.post(new android.support.v4.app.bt(this));
        }
        return;
    }

    public void onAnimationRepeat(android.view.animation.Animation p1)
    {
        return;
    }

    public void onAnimationStart(android.view.animation.Animation p3)
    {
        this.a = android.support.v4.app.bl.a(this.b, p3);
        if (this.a) {
            this.b.post(new android.support.v4.app.bs(this));
        }
        return;
    }
}
