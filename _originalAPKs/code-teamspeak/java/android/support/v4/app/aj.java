package android.support.v4.app;
public final class aj {

    public aj()
    {
        return;
    }

    private static int a(android.content.Context p1, String p2, int p3, String p4)
    {
        return ((android.app.AppOpsManager) p1.getSystemService(android.app.AppOpsManager)).noteOp(p2, p3, p4);
    }

    private static int a(android.content.Context p1, String p2, String p3)
    {
        return ((android.app.AppOpsManager) p1.getSystemService(android.app.AppOpsManager)).noteProxyOp(p2, p3);
    }

    private static String a(String p1)
    {
        return android.app.AppOpsManager.permissionToOp(p1);
    }
}
