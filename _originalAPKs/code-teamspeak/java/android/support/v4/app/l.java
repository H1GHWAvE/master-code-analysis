package android.support.v4.app;
final class l {
    private static final String a = "ActionBarDrawerToggleImplJellybeanMR2";
    private static final int[] b;

    static l()
    {
        int[] v0_1 = new int[1];
        v0_1[0] = 16843531;
        android.support.v4.app.l.b = v0_1;
        return;
    }

    l()
    {
        return;
    }

    public static android.graphics.drawable.Drawable a(android.app.Activity p4)
    {
        android.content.res.TypedArray v0_0 = p4.getActionBar();
        if (v0_0 != null) {
            p4 = v0_0.getThemedContext();
        }
        android.content.res.TypedArray v0_2 = p4.obtainStyledAttributes(0, android.support.v4.app.l.b, 16843470, 0);
        android.graphics.drawable.Drawable v1_1 = v0_2.getDrawable(0);
        v0_2.recycle();
        return v1_1;
    }

    public static Object a(Object p1, android.app.Activity p2, int p3)
    {
        android.app.ActionBar v0 = p2.getActionBar();
        if (v0 != null) {
            v0.setHomeActionContentDescription(p3);
        }
        return p1;
    }

    public static Object a(Object p1, android.app.Activity p2, android.graphics.drawable.Drawable p3, int p4)
    {
        android.app.ActionBar v0 = p2.getActionBar();
        if (v0 != null) {
            v0.setHomeAsUpIndicator(p3);
            v0.setHomeActionContentDescription(p4);
        }
        return p1;
    }
}
