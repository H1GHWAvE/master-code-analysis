package android.support.v4.app;
public class Fragment implements android.content.ComponentCallbacks, android.view.View$OnCreateContextMenuListener {
    private static final android.support.v4.n.v a = None;
    static final Object n = None;
    static final int o = 0;
    static final int p = 1;
    static final int q = 2;
    static final int r = 3;
    static final int s = 4;
    static final int t = 5;
    String A;
    public android.os.Bundle B;
    android.support.v4.app.Fragment C;
    int D;
    int E;
    boolean F;
    boolean G;
    boolean H;
    boolean I;
    boolean J;
    boolean K;
    int L;
    public android.support.v4.app.bl M;
    public android.support.v4.app.bh N;
    android.support.v4.app.bl O;
    android.support.v4.app.Fragment P;
    int Q;
    int R;
    String S;
    boolean T;
    boolean U;
    boolean V;
    boolean W;
    boolean X;
    boolean Y;
    boolean Z;
    int aa;
    android.view.ViewGroup ab;
    public android.view.View ac;
    android.view.View ad;
    boolean ae;
    boolean af;
    android.support.v4.app.ct ag;
    boolean ah;
    boolean ai;
    Object aj;
    Object ak;
    Object al;
    Object am;
    Object an;
    Object ao;
    Boolean ap;
    Boolean aq;
    android.support.v4.app.gj ar;
    android.support.v4.app.gj as;
    int u;
    android.view.View v;
    int w;
    android.os.Bundle x;
    android.util.SparseArray y;
    int z;

    static Fragment()
    {
        android.support.v4.app.Fragment.a = new android.support.v4.n.v();
        android.support.v4.app.Fragment.n = new Object();
        return;
    }

    public Fragment()
    {
        this.u = 0;
        this.z = -1;
        this.D = -1;
        this.Y = 1;
        this.af = 1;
        this.aj = 0;
        this.ak = android.support.v4.app.Fragment.n;
        this.al = 0;
        this.am = android.support.v4.app.Fragment.n;
        this.an = 0;
        this.ao = android.support.v4.app.Fragment.n;
        this.ar = 0;
        this.as = 0;
        return;
    }

    private android.support.v4.app.Fragment A()
    {
        return this.C;
    }

    private int B()
    {
        return this.E;
    }

    private Object C()
    {
        Object v0_2;
        if (this.N != null) {
            v0_2 = this.N.h();
        } else {
            v0_2 = 0;
        }
        return v0_2;
    }

    private android.support.v4.app.bi D()
    {
        return this.M;
    }

    private android.support.v4.app.bi E()
    {
        if (this.O == null) {
            this.w();
            if (this.u < 5) {
                if (this.u < 4) {
                    if (this.u < 2) {
                        if (this.u > 0) {
                            this.O.n();
                        }
                    } else {
                        this.O.o();
                    }
                } else {
                    this.O.p();
                }
            } else {
                this.O.q();
            }
        }
        return this.O;
    }

    private android.support.v4.app.Fragment F()
    {
        return this.P;
    }

    private boolean G()
    {
        return this.U;
    }

    private boolean H()
    {
        return this.G;
    }

    private boolean I()
    {
        return this.J;
    }

    private boolean J()
    {
        return this.H;
    }

    private boolean K()
    {
        return this.T;
    }

    private boolean L()
    {
        return this.X;
    }

    private boolean M()
    {
        return this.Y;
    }

    private boolean N()
    {
        return this.V;
    }

    private boolean O()
    {
        return this.af;
    }

    private android.support.v4.app.cr P()
    {
        android.support.v4.app.ct v0_4;
        if (this.ag == null) {
            if (this.N != null) {
                this.ai = 1;
                this.ag = this.N.a(this.A, this.ah, 1);
                v0_4 = this.ag;
            } else {
                throw new IllegalStateException(new StringBuilder("Fragment ").append(this).append(" not attached to Activity").toString());
            }
        } else {
            v0_4 = this.ag;
        }
        return v0_4;
    }

    private void Q()
    {
        this.Z = 1;
        return;
    }

    private void R()
    {
        android.app.Activity v0_3;
        this.Z = 1;
        if (this.N != null) {
            v0_3 = this.N.b;
        } else {
            v0_3 = 0;
        }
        if (v0_3 != null) {
            this.Z = 0;
            this.a(v0_3);
        }
        return;
    }

    private android.view.View S()
    {
        return this.ac;
    }

    private void T()
    {
        this.Z = 1;
        return;
    }

    private void U()
    {
        this.z = -1;
        this.A = 0;
        this.F = 0;
        this.G = 0;
        this.H = 0;
        this.I = 0;
        this.J = 0;
        this.K = 0;
        this.L = 0;
        this.M = 0;
        this.O = 0;
        this.N = 0;
        this.Q = 0;
        this.R = 0;
        this.S = 0;
        this.T = 0;
        this.U = 0;
        this.W = 0;
        this.ag = 0;
        this.ah = 0;
        this.ai = 0;
        return;
    }

    private static void V()
    {
        return;
    }

    private static void W()
    {
        return;
    }

    private static boolean X()
    {
        return 0;
    }

    private Object Y()
    {
        return this.aj;
    }

    private Object Z()
    {
        Object v0_1;
        if (this.ak != android.support.v4.app.Fragment.n) {
            v0_1 = this.ak;
        } else {
            v0_1 = this.aj;
        }
        return v0_1;
    }

    public static android.support.v4.app.Fragment a(android.content.Context p1, String p2)
    {
        return android.support.v4.app.Fragment.a(p1, p2, 0);
    }

    public static android.support.v4.app.Fragment a(android.content.Context p4, String p5, android.os.Bundle p6)
    {
        try {
            android.support.v4.app.Fragment v0_2 = ((Class) android.support.v4.app.Fragment.a.get(p5));
        } catch (android.support.v4.app.Fragment v0_6) {
            throw new android.support.v4.app.az(new StringBuilder("Unable to instantiate fragment ").append(p5).append(": make sure class name exists, is public, and has an empty constructor that is public").toString(), v0_6);
        } catch (android.support.v4.app.Fragment v0_8) {
            throw new android.support.v4.app.az(new StringBuilder("Unable to instantiate fragment ").append(p5).append(": make sure class name exists, is public, and has an empty constructor that is public").toString(), v0_8);
        } catch (android.support.v4.app.Fragment v0_7) {
            throw new android.support.v4.app.az(new StringBuilder("Unable to instantiate fragment ").append(p5).append(": make sure class name exists, is public, and has an empty constructor that is public").toString(), v0_7);
        }
        if (v0_2 == null) {
            v0_2 = p4.getClassLoader().loadClass(p5);
            android.support.v4.app.Fragment.a.put(p5, v0_2);
        }
        android.support.v4.app.Fragment v0_5 = ((android.support.v4.app.Fragment) v0_2.newInstance());
        if (p6 != null) {
            p6.setClassLoader(v0_5.getClass().getClassLoader());
            v0_5.B = p6;
        }
        return v0_5;
    }

    private CharSequence a(int p2)
    {
        return this.j().getText(p2);
    }

    private varargs String a(int p2, Object[] p3)
    {
        return this.j().getString(p2, p3);
    }

    private void a()
    {
        if (this.y != null) {
            this.ad.restoreHierarchyState(this.y);
            this.y = 0;
        }
        this.Z = 0;
        this.Z = 1;
        if (this.Z) {
            return;
        } else {
            throw new android.support.v4.app.gk(new StringBuilder("Fragment ").append(this).append(" did not call through to super.onViewStateRestored()").toString());
        }
    }

    private void a(android.content.Intent p4)
    {
        if (this.N != null) {
            this.N.a(this, p4, -1);
            return;
        } else {
            throw new IllegalStateException(new StringBuilder("Fragment ").append(this).append(" not attached to Activity").toString());
        }
    }

    private void a(android.content.Intent p4, int p5)
    {
        if (this.N != null) {
            this.N.a(this, p4, p5);
            return;
        } else {
            throw new IllegalStateException(new StringBuilder("Fragment ").append(this).append(" not attached to Activity").toString());
        }
    }

    private void a(android.content.res.Configuration p2)
    {
        this.onConfigurationChanged(p2);
        if (this.O != null) {
            this.O.a(p2);
        }
        return;
    }

    private void a(android.support.v4.app.Fragment$SavedState p3)
    {
        if (this.z < 0) {
            if ((p3 == null) || (p3.a == null)) {
                android.os.Bundle v0_2 = 0;
            } else {
                v0_2 = p3.a;
            }
            this.x = v0_2;
            return;
        } else {
            throw new IllegalStateException("Fragment already active");
        }
    }

    private void a(android.support.v4.app.Fragment p1, int p2)
    {
        this.C = p1;
        this.E = p2;
        return;
    }

    private void a(android.support.v4.app.gj p1)
    {
        this.ar = p1;
        return;
    }

    private void a(android.view.View p1)
    {
        p1.setOnCreateContextMenuListener(this);
        return;
    }

    private void a(Object p1)
    {
        this.aj = p1;
        return;
    }

    private void a(String p4, java.io.FileDescriptor p5, java.io.PrintWriter p6, String[] p7)
    {
        p6.print(p4);
        p6.print("mFragmentId=#");
        p6.print(Integer.toHexString(this.Q));
        p6.print(" mContainerId=#");
        p6.print(Integer.toHexString(this.R));
        p6.print(" mTag=");
        p6.println(this.S);
        p6.print(p4);
        p6.print("mState=");
        p6.print(this.u);
        p6.print(" mIndex=");
        p6.print(this.z);
        p6.print(" mWho=");
        p6.print(this.A);
        p6.print(" mBackStackNesting=");
        p6.println(this.L);
        p6.print(p4);
        p6.print("mAdded=");
        p6.print(this.F);
        p6.print(" mRemoving=");
        p6.print(this.G);
        p6.print(" mResumed=");
        p6.print(this.H);
        p6.print(" mFromLayout=");
        p6.print(this.I);
        p6.print(" mInLayout=");
        p6.println(this.J);
        p6.print(p4);
        p6.print("mHidden=");
        p6.print(this.T);
        p6.print(" mDetached=");
        p6.print(this.U);
        p6.print(" mMenuVisible=");
        p6.print(this.Y);
        p6.print(" mHasMenu=");
        p6.println(this.X);
        p6.print(p4);
        p6.print("mRetainInstance=");
        p6.print(this.V);
        p6.print(" mRetaining=");
        p6.print(this.W);
        p6.print(" mUserVisibleHint=");
        p6.println(this.af);
        if (this.M != null) {
            p6.print(p4);
            p6.print("mFragmentManager=");
            p6.println(this.M);
        }
        if (this.N != null) {
            p6.print(p4);
            p6.print("mHost=");
            p6.println(this.N);
        }
        if (this.P != null) {
            p6.print(p4);
            p6.print("mParentFragment=");
            p6.println(this.P);
        }
        if (this.B != null) {
            p6.print(p4);
            p6.print("mArguments=");
            p6.println(this.B);
        }
        if (this.x != null) {
            p6.print(p4);
            p6.print("mSavedFragmentState=");
            p6.println(this.x);
        }
        if (this.y != null) {
            p6.print(p4);
            p6.print("mSavedViewState=");
            p6.println(this.y);
        }
        if (this.C != null) {
            p6.print(p4);
            p6.print("mTarget=");
            p6.print(this.C);
            p6.print(" mTargetRequestCode=");
            p6.println(this.E);
        }
        if (this.aa != 0) {
            p6.print(p4);
            p6.print("mNextAnim=");
            p6.println(this.aa);
        }
        if (this.ab != null) {
            p6.print(p4);
            p6.print("mContainer=");
            p6.println(this.ab);
        }
        if (this.ac != null) {
            p6.print(p4);
            p6.print("mView=");
            p6.println(this.ac);
        }
        if (this.ad != null) {
            p6.print(p4);
            p6.print("mInnerView=");
            p6.println(this.ac);
        }
        if (this.v != null) {
            p6.print(p4);
            p6.print("mAnimatingAway=");
            p6.println(this.v);
            p6.print(p4);
            p6.print("mStateAfterAnimating=");
            p6.println(this.w);
        }
        if (this.ag != null) {
            p6.print(p4);
            p6.println("Loader Manager:");
            this.ag.a(new StringBuilder().append(p4).append("  ").toString(), p5, p6, p7);
        }
        if (this.O != null) {
            p6.print(p4);
            p6.println(new StringBuilder("Child ").append(this.O).append(":").toString());
            this.O.a(new StringBuilder().append(p4).append("  ").toString(), p5, p6, p7);
        }
        return;
    }

    private void a(boolean p3)
    {
        if ((!p3) || (this.P == null)) {
            this.V = p3;
            return;
        } else {
            throw new IllegalStateException("Can\'t retain fragements that are nested in other fragments");
        }
    }

    private void a(String[] p4, int p5)
    {
        if (this.N != null) {
            this.N.a(this, p4, p5);
            return;
        } else {
            throw new IllegalStateException(new StringBuilder("Fragment ").append(this).append(" not attached to Activity").toString());
        }
    }

    private boolean a(android.view.Menu p3)
    {
        int v0 = 0;
        if (!this.T) {
            if ((this.X) && (this.Y)) {
                v0 = 1;
            }
            if (this.O != null) {
                v0 |= this.O.a(p3);
            }
        }
        return v0;
    }

    private Object aa()
    {
        return this.al;
    }

    private Object ab()
    {
        Object v0_1;
        if (this.am != android.support.v4.app.Fragment.n) {
            v0_1 = this.am;
        } else {
            v0_1 = this.al;
        }
        return v0_1;
    }

    private Object ac()
    {
        return this.an;
    }

    private Object ad()
    {
        Object v0_1;
        if (this.ao != android.support.v4.app.Fragment.n) {
            v0_1 = this.ao;
        } else {
            v0_1 = this.an;
        }
        return v0_1;
    }

    private boolean ae()
    {
        boolean v0_2;
        if (this.aq != null) {
            v0_2 = this.aq.booleanValue();
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    private boolean af()
    {
        boolean v0_2;
        if (this.ap != null) {
            v0_2 = this.ap.booleanValue();
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    private void ag()
    {
        if (this.O != null) {
            this.O.z = 0;
            this.O.k();
        }
        this.Z = 0;
        this.e();
        if (this.Z) {
            if (this.O != null) {
                this.O.p();
            }
            if (this.ag != null) {
                this.ag.f();
            }
            return;
        } else {
            throw new android.support.v4.app.gk(new StringBuilder("Fragment ").append(this).append(" did not call through to super.onStart()").toString());
        }
    }

    private void ah()
    {
        if (this.O != null) {
            this.O.z = 0;
            this.O.k();
        }
        this.Z = 0;
        this.s();
        if (this.Z) {
            if (this.O != null) {
                this.O.q();
                this.O.k();
            }
            return;
        } else {
            throw new android.support.v4.app.gk(new StringBuilder("Fragment ").append(this).append(" did not call through to super.onResume()").toString());
        }
    }

    private void ai()
    {
        this.onLowMemory();
        if (this.O != null) {
            this.O.t();
        }
        return;
    }

    private void aj()
    {
        if (this.O != null) {
            this.O.c(4);
        }
        this.Z = 0;
        this.t();
        if (this.Z) {
            return;
        } else {
            throw new android.support.v4.app.gk(new StringBuilder("Fragment ").append(this).append(" did not call through to super.onPause()").toString());
        }
    }

    private void ak()
    {
        if (this.O != null) {
            this.O.r();
        }
        this.Z = 0;
        this.f();
        if (this.Z) {
            return;
        } else {
            throw new android.support.v4.app.gk(new StringBuilder("Fragment ").append(this).append(" did not call through to super.onStop()").toString());
        }
    }

    private void al()
    {
        if (this.O != null) {
            this.O.c(1);
        }
        this.Z = 0;
        this.g();
        if (this.Z) {
            if (this.ag != null) {
                this.ag.e();
            }
            return;
        } else {
            throw new android.support.v4.app.gk(new StringBuilder("Fragment ").append(this).append(" did not call through to super.onDestroyView()").toString());
        }
    }

    private void am()
    {
        if (this.O != null) {
            this.O.s();
        }
        this.Z = 0;
        this.u();
        if (this.Z) {
            return;
        } else {
            throw new android.support.v4.app.gk(new StringBuilder("Fragment ").append(this).append(" did not call through to super.onDestroy()").toString());
        }
    }

    private String b(int p2)
    {
        return this.j().getString(p2);
    }

    private void b(android.support.v4.app.gj p1)
    {
        this.as = p1;
        return;
    }

    private void b(android.view.Menu p2)
    {
        if ((!this.T) && (this.O != null)) {
            this.O.b(p2);
        }
        return;
    }

    private static void b(android.view.View p1)
    {
        p1.setOnCreateContextMenuListener(0);
        return;
    }

    private void b(Object p1)
    {
        this.ak = p1;
        return;
    }

    private boolean b()
    {
        int v0_1;
        if (this.L <= 0) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    static boolean b(android.content.Context p2, String p3)
    {
        try {
            boolean v0_2 = ((Class) android.support.v4.app.Fragment.a.get(p3));
        } catch (boolean v0) {
            boolean v0_4 = 0;
            return v0_4;
        }
        if (!v0_2) {
            v0_2 = p2.getClassLoader().loadClass(p3);
            android.support.v4.app.Fragment.a.put(p3, v0_2);
        }
        v0_4 = android.support.v4.app.Fragment.isAssignableFrom(v0_2);
        return v0_4;
    }

    private boolean b(android.view.Menu p3, android.view.MenuInflater p4)
    {
        int v0 = 0;
        if (!this.T) {
            if ((this.X) && (this.Y)) {
                v0 = 1;
                this.a(p3, p4);
            }
            if (this.O != null) {
                v0 |= this.O.a(p3, p4);
            }
        }
        return v0;
    }

    private boolean b(android.view.MenuItem p3)
    {
        int v0 = 1;
        if ((this.T) || (((!this.X) || ((!this.Y) || (!this.a(p3)))) && ((this.O == null) || (!this.O.a(p3))))) {
            v0 = 0;
        }
        return v0;
    }

    private boolean b(String p2)
    {
        int v0_1;
        if (this.N == null) {
            v0_1 = 0;
        } else {
            v0_1 = this.N.a(p2);
        }
        return v0_1;
    }

    private void c(Object p1)
    {
        this.al = p1;
        return;
    }

    private boolean c(android.view.MenuItem p2)
    {
        if ((this.T) || ((this.O == null) || (!this.O.b(p2)))) {
            int v0_4 = 0;
        } else {
            v0_4 = 1;
        }
        return v0_4;
    }

    private int d()
    {
        return this.Q;
    }

    private void d(Object p1)
    {
        this.am = p1;
        return;
    }

    private void d(boolean p2)
    {
        this.aq = Boolean.valueOf(p2);
        return;
    }

    private void e(Object p1)
    {
        this.an = p1;
        return;
    }

    private void e(boolean p2)
    {
        this.ap = Boolean.valueOf(p2);
        return;
    }

    private void f(Object p1)
    {
        this.ao = p1;
        return;
    }

    private void g(android.os.Bundle p4)
    {
        if (this.O != null) {
            this.O.z = 0;
        }
        this.Z = 0;
        this.a(p4);
        if (this.Z) {
            if (p4 != null) {
                android.support.v4.app.bl v0_4 = p4.getParcelable("android:support:fragments");
                if (v0_4 != null) {
                    if (this.O == null) {
                        this.w();
                    }
                    this.O.a(v0_4, 0);
                    this.O.n();
                }
            }
            return;
        } else {
            throw new android.support.v4.app.gk(new StringBuilder("Fragment ").append(this).append(" did not call through to super.onCreate()").toString());
        }
    }

    private void h(android.os.Bundle p4)
    {
        if (this.O != null) {
            this.O.z = 0;
        }
        this.Z = 0;
        this.c(p4);
        if (this.Z) {
            if (this.O != null) {
                this.O.o();
            }
            return;
        } else {
            throw new android.support.v4.app.gk(new StringBuilder("Fragment ").append(this).append(" did not call through to super.onActivityCreated()").toString());
        }
    }

    public static void m()
    {
        return;
    }

    public static void o()
    {
        return;
    }

    public static void p()
    {
        return;
    }

    public static android.view.animation.Animation r()
    {
        return 0;
    }

    public static void v()
    {
        return;
    }

    private String y()
    {
        return this.S;
    }

    private android.os.Bundle z()
    {
        return this.B;
    }

    final android.support.v4.app.Fragment a(String p5)
    {
        if (!p5.equals(this.A)) {
            if (this.O == null) {
                this = 0;
            } else {
                android.support.v4.app.bl v3 = this.O;
                if ((v3.l != null) && (p5 != null)) {
                    int v2 = (v3.l.size() - 1);
                    while (v2 >= 0) {
                        int v0_9 = ((android.support.v4.app.Fragment) v3.l.get(v2));
                        if (v0_9 != 0) {
                            this = v0_9.a(p5);
                            if (this != null) {
                                return this;
                            }
                        }
                        v2--;
                    }
                }
                this = 0;
            }
        }
        return this;
    }

    public android.view.View a(android.view.LayoutInflater p2, android.view.ViewGroup p3)
    {
        return 0;
    }

    final void a(int p3, android.support.v4.app.Fragment p4)
    {
        this.z = p3;
        if (p4 == null) {
            this.A = new StringBuilder("android:fragment:").append(this.z).toString();
        } else {
            this.A = new StringBuilder().append(p4.A).append(":").append(this.z).toString();
        }
        return;
    }

    public void a(android.app.Activity p2)
    {
        this.Z = 1;
        return;
    }

    public void a(android.os.Bundle p2)
    {
        this.Z = 1;
        return;
    }

    public void a(android.view.Menu p1, android.view.MenuInflater p2)
    {
        return;
    }

    public void a(android.view.View p1, android.os.Bundle p2)
    {
        return;
    }

    public boolean a(android.view.MenuItem p2)
    {
        return 0;
    }

    public android.view.LayoutInflater b(android.os.Bundle p4)
    {
        android.view.LayoutInflater v0_1 = this.N.c();
        if (this.O == null) {
            this.w();
            if (this.u < 5) {
                if (this.u < 4) {
                    if (this.u < 2) {
                        if (this.u > 0) {
                            this.O.n();
                        }
                    } else {
                        this.O.o();
                    }
                } else {
                    this.O.p();
                }
            } else {
                this.O.q();
            }
        }
        android.support.v4.view.ai.a(v0_1, this.O);
        return v0_1;
    }

    final android.view.View b(android.view.LayoutInflater p3, android.view.ViewGroup p4)
    {
        if (this.O != null) {
            this.O.z = 0;
        }
        return this.a(p3, p4);
    }

    public final void b(boolean p2)
    {
        if (this.Y != p2) {
            this.Y = p2;
            if ((this.X) && ((this.k()) && (!this.T))) {
                this.N.d();
            }
        }
        return;
    }

    public void c()
    {
        this.Z = 1;
        return;
    }

    public void c(android.os.Bundle p2)
    {
        this.Z = 1;
        return;
    }

    public final void c(boolean p3)
    {
        if ((!this.af) && ((p3) && (this.u < 4))) {
            this.M.b(this);
        }
        android.support.v4.app.bl v0_3;
        this.af = p3;
        if (p3) {
            v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        this.ae = v0_3;
        return;
    }

    public void d(android.os.Bundle p1)
    {
        return;
    }

    public void e()
    {
        this.Z = 1;
        if (!this.ah) {
            this.ah = 1;
            if (!this.ai) {
                this.ai = 1;
                this.ag = this.N.a(this.A, this.ah, 0);
            }
            if (this.ag != null) {
                this.ag.b();
            }
        }
        return;
    }

    public final void e(android.os.Bundle p3)
    {
        if (this.z < 0) {
            this.B = p3;
            return;
        } else {
            throw new IllegalStateException("Fragment already active");
        }
    }

    public final boolean equals(Object p2)
    {
        return super.equals(p2);
    }

    public void f()
    {
        this.Z = 1;
        return;
    }

    final void f(android.os.Bundle p3)
    {
        this.d(p3);
        if (this.O != null) {
            android.os.Parcelable v0_2 = this.O.m();
            if (v0_2 != null) {
                p3.putParcelable("android:support:fragments", v0_2);
            }
        }
        return;
    }

    public void g()
    {
        this.Z = 1;
        return;
    }

    public final android.content.Context h()
    {
        android.content.Context v0_2;
        if (this.N != null) {
            v0_2 = this.N.c;
        } else {
            v0_2 = 0;
        }
        return v0_2;
    }

    public final int hashCode()
    {
        return super.hashCode();
    }

    public final android.support.v4.app.bb i()
    {
        android.support.v4.app.bb v0_3;
        if (this.N != null) {
            v0_3 = ((android.support.v4.app.bb) this.N.b);
        } else {
            v0_3 = 0;
        }
        return v0_3;
    }

    public final android.content.res.Resources j()
    {
        if (this.N != null) {
            return this.N.c.getResources();
        } else {
            throw new IllegalStateException(new StringBuilder("Fragment ").append(this).append(" not attached to Activity").toString());
        }
    }

    public final boolean k()
    {
        if ((this.N == null) || (!this.F)) {
            int v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    public final boolean l()
    {
        if ((!this.k()) || ((this.T) || ((this.ac == null) || ((this.ac.getWindowToken() == null) || (this.ac.getVisibility() != 0))))) {
            int v0_7 = 0;
        } else {
            v0_7 = 1;
        }
        return v0_7;
    }

    public final void n()
    {
        if (this.X != 1) {
            this.X = 1;
            if ((this.k()) && (!this.T)) {
                this.N.d();
            }
        }
        return;
    }

    public void onConfigurationChanged(android.content.res.Configuration p2)
    {
        this.Z = 1;
        return;
    }

    public void onCreateContextMenu(android.view.ContextMenu p2, android.view.View p3, android.view.ContextMenu$ContextMenuInfo p4)
    {
        this.i().onCreateContextMenu(p2, p3, p4);
        return;
    }

    public void onLowMemory()
    {
        this.Z = 1;
        return;
    }

    public final void q()
    {
        int v0_2;
        this.Z = 1;
        if (this.N != null) {
            v0_2 = this.N.b;
        } else {
            v0_2 = 0;
        }
        if (v0_2 != 0) {
            this.Z = 0;
            this.Z = 1;
        }
        return;
    }

    public void s()
    {
        this.Z = 1;
        return;
    }

    public void t()
    {
        this.Z = 1;
        return;
    }

    public String toString()
    {
        String v0_1 = new StringBuilder(128);
        android.support.v4.n.g.a(this, v0_1);
        if (this.z >= 0) {
            v0_1.append(" #");
            v0_1.append(this.z);
        }
        if (this.Q != 0) {
            v0_1.append(" id=0x");
            v0_1.append(Integer.toHexString(this.Q));
        }
        if (this.S != null) {
            v0_1.append(" ");
            v0_1.append(this.S);
        }
        v0_1.append(125);
        return v0_1.toString();
    }

    public void u()
    {
        this.Z = 1;
        if (!this.ai) {
            this.ai = 1;
            this.ag = this.N.a(this.A, this.ah, 0);
        }
        if (this.ag != null) {
            this.ag.g();
        }
        return;
    }

    final void w()
    {
        this.O = new android.support.v4.app.bl();
        this.O.a(this.N, new android.support.v4.app.ay(this), this);
        return;
    }

    final void x()
    {
        if (this.O != null) {
            this.O.c(2);
        }
        if (this.ah) {
            this.ah = 0;
            if (!this.ai) {
                this.ai = 1;
                this.ag = this.N.a(this.A, this.ah, 0);
            }
            if (this.ag != null) {
                if (this.W) {
                    this.ag.d();
                } else {
                    this.ag.c();
                }
            }
        }
        return;
    }
}
