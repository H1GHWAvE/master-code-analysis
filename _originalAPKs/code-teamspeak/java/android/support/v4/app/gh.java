package android.support.v4.app;
final class gh {
    private static final String a = ".sharecompat_";

    gh()
    {
        return;
    }

    private static void a(android.view.MenuItem p3, android.app.Activity p4, android.content.Intent p5)
    {
        android.widget.ShareActionProvider v0_1;
        android.widget.ShareActionProvider v0_0 = p3.getActionProvider();
        if ((v0_0 instanceof android.widget.ShareActionProvider)) {
            v0_1 = ((android.widget.ShareActionProvider) v0_0);
        } else {
            v0_1 = new android.widget.ShareActionProvider(p4);
        }
        v0_1.setShareHistoryFileName(new StringBuilder(".sharecompat_").append(p4.getClass().getName()).toString());
        v0_1.setShareIntent(p5);
        p3.setActionProvider(v0_1);
        return;
    }
}
