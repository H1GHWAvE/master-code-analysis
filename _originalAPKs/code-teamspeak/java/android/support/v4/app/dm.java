package android.support.v4.app;
public class dm {
    private static final int D = 5120;
    android.app.Notification A;
    public android.app.Notification B;
    public java.util.ArrayList C;
    public android.content.Context a;
    public CharSequence b;
    public CharSequence c;
    public android.app.PendingIntent d;
    android.app.PendingIntent e;
    android.widget.RemoteViews f;
    public android.graphics.Bitmap g;
    public CharSequence h;
    public int i;
    int j;
    boolean k;
    public boolean l;
    public android.support.v4.app.ed m;
    public CharSequence n;
    int o;
    int p;
    boolean q;
    String r;
    boolean s;
    String t;
    public java.util.ArrayList u;
    boolean v;
    String w;
    android.os.Bundle x;
    int y;
    int z;

    public dm(android.content.Context p6)
    {
        this.k = 1;
        this.u = new java.util.ArrayList();
        this.v = 0;
        this.y = 0;
        this.z = 0;
        this.B = new android.app.Notification();
        this.a = p6;
        this.B.when = System.currentTimeMillis();
        this.B.audioStreamType = -1;
        this.j = 0;
        this.C = new java.util.ArrayList();
        return;
    }

    private android.support.v4.app.dm a(int p2, int p3)
    {
        this.B.icon = p2;
        this.B.iconLevel = p3;
        return this;
    }

    private android.support.v4.app.dm a(int p6, int p7, int p8)
    {
        int v0_7;
        int v1 = 1;
        this.B.ledARGB = p6;
        this.B.ledOnMS = p7;
        this.B.ledOffMS = p8;
        if ((this.B.ledOnMS == 0) || (this.B.ledOffMS == 0)) {
            v0_7 = 0;
        } else {
            v0_7 = 1;
        }
        if (v0_7 == 0) {
            v1 = 0;
        }
        this.B.flags = ((this.B.flags & -2) | v1);
        return this;
    }

    private android.support.v4.app.dm a(int p1, int p2, boolean p3)
    {
        this.o = p1;
        this.p = p2;
        this.q = p3;
        return this;
    }

    private android.support.v4.app.dm a(int p3, CharSequence p4, android.app.PendingIntent p5)
    {
        this.u.add(new android.support.v4.app.df(p3, p4, p5));
        return this;
    }

    private android.support.v4.app.dm a(android.app.Notification p1)
    {
        this.A = p1;
        return this;
    }

    private android.support.v4.app.dm a(android.app.PendingIntent p2, boolean p3)
    {
        this.e = p2;
        this.a(128, p3);
        return this;
    }

    private android.support.v4.app.dm a(android.net.Uri p3)
    {
        this.B.sound = p3;
        this.B.audioStreamType = -1;
        return this;
    }

    private android.support.v4.app.dm a(android.net.Uri p2, int p3)
    {
        this.B.sound = p2;
        this.B.audioStreamType = p3;
        return this;
    }

    private android.support.v4.app.dm a(android.os.Bundle p2)
    {
        if (p2 != null) {
            if (this.x != null) {
                this.x.putAll(p2);
            } else {
                this.x = new android.os.Bundle(p2);
            }
        }
        return this;
    }

    private android.support.v4.app.dm a(android.support.v4.app.df p2)
    {
        this.u.add(p2);
        return this;
    }

    private android.support.v4.app.dm a(android.support.v4.app.ds p1)
    {
        p1.a(this);
        return this;
    }

    private android.support.v4.app.dm a(android.support.v4.app.ed p2)
    {
        if (this.m != p2) {
            this.m = p2;
            if (this.m != null) {
                this.m.a(this);
            }
        }
        return this;
    }

    private android.support.v4.app.dm a(android.widget.RemoteViews p2)
    {
        this.B.contentView = p2;
        return this;
    }

    private android.support.v4.app.dm a(CharSequence p3, android.widget.RemoteViews p4)
    {
        this.B.tickerText = android.support.v4.app.dm.d(p3);
        this.f = p4;
        return this;
    }

    private android.support.v4.app.dm a(String p1)
    {
        this.w = p1;
        return this;
    }

    private android.support.v4.app.dm a(boolean p1)
    {
        this.k = p1;
        return this;
    }

    private android.support.v4.app.dm a(long[] p2)
    {
        this.B.vibrate = p2;
        return this;
    }

    private android.support.v4.app.dm b(int p1)
    {
        this.i = p1;
        return this;
    }

    private android.support.v4.app.dm b(android.app.PendingIntent p2)
    {
        this.B.deleteIntent = p2;
        return this;
    }

    private android.support.v4.app.dm b(android.os.Bundle p1)
    {
        this.x = p1;
        return this;
    }

    private android.support.v4.app.dm b(String p2)
    {
        this.C.add(p2);
        return this;
    }

    private android.support.v4.app.dm b(boolean p1)
    {
        this.l = p1;
        return this;
    }

    private android.support.v4.app.dm c(int p3)
    {
        this.B.defaults = p3;
        if ((p3 & 4) != 0) {
            android.app.Notification v0_2 = this.B;
            v0_2.flags = (v0_2.flags | 1);
        }
        return this;
    }

    private android.support.v4.app.dm c(String p1)
    {
        this.r = p1;
        return this;
    }

    private android.support.v4.app.dm c(boolean p2)
    {
        this.a(8, p2);
        return this;
    }

    private android.support.v4.app.dm d(int p1)
    {
        this.j = p1;
        return this;
    }

    private android.support.v4.app.dm d(String p1)
    {
        this.t = p1;
        return this;
    }

    private android.support.v4.app.dm d(boolean p1)
    {
        this.v = p1;
        return this;
    }

    protected static CharSequence d(CharSequence p2)
    {
        if ((p2 != null) && (p2.length() > 5120)) {
            p2 = p2.subSequence(0, 5120);
        }
        return p2;
    }

    private android.support.v4.app.dm e()
    {
        this.a(2, 1);
        return this;
    }

    private android.support.v4.app.dm e(int p1)
    {
        this.y = p1;
        return this;
    }

    private android.support.v4.app.dm e(CharSequence p2)
    {
        this.n = android.support.v4.app.dm.d(p2);
        return this;
    }

    private android.support.v4.app.dm e(boolean p1)
    {
        this.s = p1;
        return this;
    }

    private android.app.Notification f()
    {
        return this.c();
    }

    private android.support.v4.app.dm f(int p1)
    {
        this.z = p1;
        return this;
    }

    private android.support.v4.app.dm f(CharSequence p2)
    {
        this.h = android.support.v4.app.dm.d(p2);
        return this;
    }

    public final android.support.v4.app.dm a()
    {
        this.a(16, 0);
        return this;
    }

    public final android.support.v4.app.dm a(int p2)
    {
        this.B.icon = p2;
        return this;
    }

    public final android.support.v4.app.dm a(long p2)
    {
        this.B.when = p2;
        return this;
    }

    public final android.support.v4.app.dm a(android.app.PendingIntent p1)
    {
        this.d = p1;
        return this;
    }

    public final android.support.v4.app.dm a(android.graphics.Bitmap p1)
    {
        this.g = p1;
        return this;
    }

    public final android.support.v4.app.dm a(CharSequence p2)
    {
        this.b = android.support.v4.app.dm.d(p2);
        return this;
    }

    public final void a(int p4, boolean p5)
    {
        if (!p5) {
            android.app.Notification v0_0 = this.B;
            v0_0.flags = (v0_0.flags & (p4 ^ -1));
        } else {
            android.app.Notification v0_1 = this.B;
            v0_1.flags = (v0_1.flags | p4);
        }
        return;
    }

    public final android.os.Bundle b()
    {
        if (this.x == null) {
            this.x = new android.os.Bundle();
        }
        return this.x;
    }

    public final android.support.v4.app.dm b(CharSequence p2)
    {
        this.c = android.support.v4.app.dm.d(p2);
        return this;
    }

    public final android.app.Notification c()
    {
        return android.support.v4.app.dd.a().a(this, this.d());
    }

    public final android.support.v4.app.dm c(CharSequence p3)
    {
        this.B.tickerText = android.support.v4.app.dm.d(p3);
        return this;
    }

    public android.support.v4.app.dn d()
    {
        return new android.support.v4.app.dn();
    }
}
