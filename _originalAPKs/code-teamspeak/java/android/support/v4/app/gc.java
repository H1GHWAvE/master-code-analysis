package android.support.v4.app;
public final class gc {
    private static final String a = "IntentReader";
    private android.app.Activity b;
    private android.content.Intent c;
    private String d;
    private android.content.ComponentName e;
    private java.util.ArrayList f;

    private gc(android.app.Activity p2)
    {
        this.b = p2;
        this.c = p2.getIntent();
        this.d = android.support.v4.app.ga.a(p2);
        this.e = android.support.v4.app.ga.b(p2);
        return;
    }

    private android.net.Uri a(int p5)
    {
        if ((this.f == null) && (this.c())) {
            this.f = this.c.getParcelableArrayListExtra("android.intent.extra.STREAM");
        }
        int v0_21;
        if (this.f == null) {
            if (p5 != 0) {
                String v2_1 = new StringBuilder("Stream items available: ");
                if ((this.f == null) && (this.c())) {
                    this.f = this.c.getParcelableArrayListExtra("android.intent.extra.STREAM");
                }
                int v0_13;
                if (this.f == null) {
                    if (!this.c.hasExtra("android.intent.extra.STREAM")) {
                        v0_13 = 0;
                    } else {
                        v0_13 = 1;
                    }
                } else {
                    v0_13 = this.f.size();
                }
                throw new IndexOutOfBoundsException(v2_1.append(v0_13).append(" index requested: ").append(p5).toString());
            } else {
                v0_21 = ((android.net.Uri) this.c.getParcelableExtra("android.intent.extra.STREAM"));
            }
        } else {
            v0_21 = ((android.net.Uri) this.f.get(p5));
        }
        return v0_21;
    }

    private static android.support.v4.app.gc a(android.app.Activity p1)
    {
        return new android.support.v4.app.gc(p1);
    }

    private boolean a()
    {
        int v0_3;
        int v0_1 = this.c.getAction();
        if ((!"android.intent.action.SEND".equals(v0_1)) && (!"android.intent.action.SEND_MULTIPLE".equals(v0_1))) {
            v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        return v0_3;
    }

    private boolean b()
    {
        return "android.intent.action.SEND".equals(this.c.getAction());
    }

    private boolean c()
    {
        return "android.intent.action.SEND_MULTIPLE".equals(this.c.getAction());
    }

    private String d()
    {
        return this.c.getType();
    }

    private CharSequence e()
    {
        return this.c.getCharSequenceExtra("android.intent.extra.TEXT");
    }

    private String f()
    {
        String v0_3;
        android.support.v4.app.gd v1_1 = this.c.getStringExtra("android.intent.extra.HTML_TEXT");
        if (v1_1 != null) {
            v0_3 = v1_1;
        } else {
            String v0_2 = this.c.getCharSequenceExtra("android.intent.extra.TEXT");
            if (!(v0_2 instanceof android.text.Spanned)) {
                if (v0_2 == null) {
                } else {
                    v0_3 = android.support.v4.app.ga.a().a(v0_2);
                }
            } else {
                v0_3 = android.text.Html.toHtml(((android.text.Spanned) v0_2));
            }
        }
        return v0_3;
    }

    private android.net.Uri g()
    {
        return ((android.net.Uri) this.c.getParcelableExtra("android.intent.extra.STREAM"));
    }

    private int h()
    {
        if ((this.f == null) && (this.c())) {
            this.f = this.c.getParcelableArrayListExtra("android.intent.extra.STREAM");
        }
        int v0_7;
        if (this.f == null) {
            if (!this.c.hasExtra("android.intent.extra.STREAM")) {
                v0_7 = 0;
            } else {
                v0_7 = 1;
            }
        } else {
            v0_7 = this.f.size();
        }
        return v0_7;
    }

    private String[] i()
    {
        return this.c.getStringArrayExtra("android.intent.extra.EMAIL");
    }

    private String[] j()
    {
        return this.c.getStringArrayExtra("android.intent.extra.CC");
    }

    private String[] k()
    {
        return this.c.getStringArrayExtra("android.intent.extra.BCC");
    }

    private String l()
    {
        return this.c.getStringExtra("android.intent.extra.SUBJECT");
    }

    private String m()
    {
        return this.d;
    }

    private android.content.ComponentName n()
    {
        return this.e;
    }

    private android.graphics.drawable.Drawable o()
    {
        android.graphics.drawable.Drawable v0 = 0;
        if (this.e != null) {
            try {
                v0 = this.b.getPackageManager().getActivityIcon(this.e);
            } catch (android.content.pm.PackageManager$NameNotFoundException v1_3) {
                android.util.Log.e("IntentReader", "Could not retrieve icon for calling activity", v1_3);
            }
        }
        return v0;
    }

    private android.graphics.drawable.Drawable p()
    {
        android.graphics.drawable.Drawable v0 = 0;
        if (this.d != null) {
            try {
                v0 = this.b.getPackageManager().getApplicationIcon(this.d);
            } catch (android.content.pm.PackageManager$NameNotFoundException v1_3) {
                android.util.Log.e("IntentReader", "Could not retrieve icon for calling application", v1_3);
            }
        }
        return v0;
    }

    private CharSequence q()
    {
        CharSequence v0 = 0;
        if (this.d != null) {
            android.content.pm.PackageManager$NameNotFoundException v1_2 = this.b.getPackageManager();
            try {
                v0 = v1_2.getApplicationLabel(v1_2.getApplicationInfo(this.d, 0));
            } catch (android.content.pm.PackageManager$NameNotFoundException v1_3) {
                android.util.Log.e("IntentReader", "Could not retrieve label for calling application", v1_3);
            }
        }
        return v0;
    }
}
