package android.support.v4.app;
final class s extends android.app.SharedElementCallback {
    private android.support.v4.app.r a;

    public s(android.support.v4.app.r p1)
    {
        this.a = p1;
        return;
    }

    public final android.os.Parcelable onCaptureSharedElementSnapshot(android.view.View p2, android.graphics.Matrix p3, android.graphics.RectF p4)
    {
        return this.a.a(p2, p3, p4);
    }

    public final android.view.View onCreateSnapshotView(android.content.Context p2, android.os.Parcelable p3)
    {
        return this.a.a(p2, p3);
    }

    public final void onMapSharedElements(java.util.List p1, java.util.Map p2)
    {
        return;
    }

    public final void onRejectSharedElements(java.util.List p1)
    {
        return;
    }

    public final void onSharedElementEnd(java.util.List p1, java.util.List p2, java.util.List p3)
    {
        return;
    }

    public final void onSharedElementStart(java.util.List p1, java.util.List p2, java.util.List p3)
    {
        return;
    }
}
