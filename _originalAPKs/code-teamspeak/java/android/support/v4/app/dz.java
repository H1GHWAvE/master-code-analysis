package android.support.v4.app;
final class dz extends android.support.v4.app.dx {

    dz()
    {
        return;
    }

    public final android.app.Notification a(android.support.v4.app.dm p15, android.support.v4.app.dn p16)
    {
        android.app.Notification v0_9;
        android.app.Notification v1 = p15.B;
        android.app.Notification$Builder v2_0 = p15.b;
        CharSequence v3 = p15.c;
        CharSequence v4 = p15.h;
        int v6 = p15.i;
        android.app.PendingIntent v7 = p15.d;
        android.app.PendingIntent v8 = p15.e;
        android.graphics.Bitmap v9 = p15.g;
        int v5_4 = new android.app.Notification$Builder(p15.a).setWhen(v1.when).setSmallIcon(v1.icon, v1.iconLevel).setContent(v1.contentView).setTicker(v1.tickerText, p15.f).setSound(v1.sound, v1.audioStreamType).setVibrate(v1.vibrate).setLights(v1.ledARGB, v1.ledOnMS, v1.ledOffMS);
        if ((v1.flags & 2) == 0) {
            v0_9 = 0;
        } else {
            v0_9 = 1;
        }
        android.app.Notification v0_12;
        int v5_5 = v5_4.setOngoing(v0_9);
        if ((v1.flags & 8) == 0) {
            v0_12 = 0;
        } else {
            v0_12 = 1;
        }
        android.app.Notification v0_15;
        int v5_6 = v5_5.setOnlyAlertOnce(v0_12);
        if ((v1.flags & 16) == 0) {
            v0_15 = 0;
        } else {
            v0_15 = 1;
        }
        android.app.Notification v0_24;
        android.app.Notification$Builder v2_2 = v5_6.setAutoCancel(v0_15).setDefaults(v1.defaults).setContentTitle(v2_0).setContentText(v3).setContentInfo(v4).setContentIntent(v7).setDeleteIntent(v1.deleteIntent);
        if ((v1.flags & 128) == 0) {
            v0_24 = 0;
        } else {
            v0_24 = 1;
        }
        return v2_2.setFullScreenIntent(v8, v0_24).setLargeIcon(v9).setNumber(v6).getNotification();
    }
}
