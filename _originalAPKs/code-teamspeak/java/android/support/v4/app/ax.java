package android.support.v4.app;
public class ax extends android.support.v4.app.Fragment implements android.content.DialogInterface$OnCancelListener, android.content.DialogInterface$OnDismissListener {
    public static final int a = 0;
    private static final String at = "android:savedDialogState";
    private static final String au = "android:style";
    private static final String av = "android:theme";
    private static final String aw = "android:cancelable";
    private static final String ax = "android:showsDialog";
    private static final String ay = "android:backStackId";
    public static final int b = 1;
    public static final int c = 2;
    public static final int d = 3;
    int e;
    public int f;
    boolean g;
    boolean h;
    int i;
    public android.app.Dialog j;
    boolean k;
    boolean l;
    boolean m;

    public ax()
    {
        this.e = 0;
        this.f = 0;
        this.g = 1;
        this.h = 1;
        this.i = -1;
        return;
    }

    private int A()
    {
        return this.f;
    }

    private boolean B()
    {
        return this.g;
    }

    private boolean C()
    {
        return this.h;
    }

    private void d(boolean p3)
    {
        if (!this.l) {
            this.l = 1;
            this.m = 0;
            if (this.j != null) {
                this.j.dismiss();
                this.j = 0;
            }
            this.k = 1;
            if (this.i < 0) {
                android.support.v4.app.cd v0_7 = this.M.a();
                v0_7.b(this);
                if (!p3) {
                    v0_7.i();
                } else {
                    v0_7.j();
                }
            } else {
                this.M.b(this.i);
                this.i = -1;
            }
        }
        return;
    }

    private void e(boolean p1)
    {
        this.h = p1;
        return;
    }

    private void y()
    {
        this.d(1);
        return;
    }

    private android.app.Dialog z()
    {
        return this.j;
    }

    public final int a(android.support.v4.app.cd p3, String p4)
    {
        this.l = 0;
        this.m = 1;
        p3.a(this, p4);
        this.k = 0;
        this.i = p3.i();
        return this.i;
    }

    public final void a(android.app.Activity p2)
    {
        super.a(p2);
        if (!this.m) {
            this.l = 0;
        }
        return;
    }

    public void a(android.app.Dialog p3, int p4)
    {
        switch (p4) {
            case 1:
            case 2:
                p3.requestWindowFeature(1);
                break;
            case 3:
                p3.getWindow().addFlags(24);
                break;
        }
        return;
    }

    public void a(android.os.Bundle p4)
    {
        int v0_1;
        super.a(p4);
        if (this.R != 0) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        this.h = v0_1;
        if (p4 != null) {
            this.e = p4.getInt("android:style", 0);
            this.f = p4.getInt("android:theme", 0);
            this.g = p4.getBoolean("android:cancelable", 1);
            this.h = p4.getBoolean("android:showsDialog", this.h);
            this.i = p4.getInt("android:backStackId", -1);
        }
        return;
    }

    public void a(android.support.v4.app.bi p2, String p3)
    {
        this.l = 0;
        this.m = 1;
        android.support.v4.app.cd v0_2 = p2.a();
        v0_2.a(this, p3);
        v0_2.i();
        return;
    }

    public final void a(boolean p2)
    {
        this.g = p2;
        if (this.j != null) {
            this.j.setCancelable(p2);
        }
        return;
    }

    public final android.view.LayoutInflater b(android.os.Bundle p3)
    {
        android.view.LayoutInflater v0_6;
        if (this.h) {
            this.j = this.d();
            if (this.j == null) {
                v0_6 = ((android.view.LayoutInflater) this.N.c.getSystemService("layout_inflater"));
            } else {
                this.a(this.j, this.e);
                v0_6 = ((android.view.LayoutInflater) this.j.getContext().getSystemService("layout_inflater"));
            }
        } else {
            v0_6 = super.b(p3);
        }
        return v0_6;
    }

    public void b()
    {
        this.d(0);
        return;
    }

    public final void c()
    {
        super.c();
        if ((!this.m) && (!this.l)) {
            this.l = 1;
        }
        return;
    }

    public void c(android.os.Bundle p3)
    {
        super.c(p3);
        if (this.h) {
            android.os.Bundle v0_1 = this.ac;
            if (v0_1 != null) {
                if (v0_1.getParent() == null) {
                    this.j.setContentView(v0_1);
                } else {
                    throw new IllegalStateException("DialogFragment can not be attached to a container view");
                }
            }
            this.j.setOwnerActivity(this.i());
            this.j.setCancelable(this.g);
            this.j.setOnCancelListener(this);
            this.j.setOnDismissListener(this);
            if (p3 != null) {
                android.os.Bundle v0_7 = p3.getBundle("android:savedDialogState");
                if (v0_7 != null) {
                    this.j.onRestoreInstanceState(v0_7);
                }
            }
        }
        return;
    }

    public android.app.Dialog d()
    {
        return new android.app.Dialog(this.i(), this.f);
    }

    public final void d(android.os.Bundle p3)
    {
        super.d(p3);
        if (this.j != null) {
            String v0_2 = this.j.onSaveInstanceState();
            if (v0_2 != null) {
                p3.putBundle("android:savedDialogState", v0_2);
            }
        }
        if (this.e != 0) {
            p3.putInt("android:style", this.e);
        }
        if (this.f != 0) {
            p3.putInt("android:theme", this.f);
        }
        if (!this.g) {
            p3.putBoolean("android:cancelable", this.g);
        }
        if (!this.h) {
            p3.putBoolean("android:showsDialog", this.h);
        }
        if (this.i != -1) {
            p3.putInt("android:backStackId", this.i);
        }
        return;
    }

    public void e()
    {
        super.e();
        if (this.j != null) {
            this.k = 0;
            this.j.show();
        }
        return;
    }

    public void f()
    {
        super.f();
        if (this.j != null) {
            this.j.hide();
        }
        return;
    }

    public final void g()
    {
        super.g();
        if (this.j != null) {
            this.k = 1;
            this.j.dismiss();
            this.j = 0;
        }
        return;
    }

    public void onCancel(android.content.DialogInterface p1)
    {
        return;
    }

    public void onDismiss(android.content.DialogInterface p2)
    {
        if (!this.k) {
            this.d(1);
        }
        return;
    }

    public final void q_()
    {
        this.e = 0;
        if ((this.e == 2) || (this.e == 3)) {
            this.f = 16973913;
        }
        this.f = 2131165365;
        return;
    }
}
