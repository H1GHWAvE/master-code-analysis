package android.support.v4.app;
public final class y {

    private y()
    {
        return;
    }

    private static boolean a(android.app.ActivityManager p2)
    {
        int v0_1;
        if (android.os.Build$VERSION.SDK_INT < 19) {
            v0_1 = 0;
        } else {
            v0_1 = p2.isLowRamDevice();
        }
        return v0_1;
    }
}
