package android.support.v4.app;
public class Fragment$SavedState implements android.os.Parcelable {
    public static final android.os.Parcelable$Creator CREATOR;
    final android.os.Bundle a;

    static Fragment$SavedState()
    {
        android.support.v4.app.Fragment$SavedState.CREATOR = new android.support.v4.app.ba();
        return;
    }

    Fragment$SavedState(android.os.Bundle p1)
    {
        this.a = p1;
        return;
    }

    Fragment$SavedState(android.os.Parcel p2)
    {
        this.a = p2.readBundle();
        return;
    }

    public int describeContents()
    {
        return 0;
    }

    public void writeToParcel(android.os.Parcel p2, int p3)
    {
        p2.writeBundle(this.a);
        return;
    }
}
