package android.support.v4.app;
public final class at {

    public at()
    {
        return;
    }

    private static android.os.IBinder a(android.os.Bundle p2, String p3)
    {
        android.os.IBinder v0_1;
        if (android.os.Build$VERSION.SDK_INT < 18) {
            v0_1 = android.support.v4.app.au.a(p2, p3);
        } else {
            v0_1 = p2.getBinder(p3);
        }
        return v0_1;
    }

    private static void a(android.os.Bundle p6, String p7, android.os.IBinder p8)
    {
        if (android.os.Build$VERSION.SDK_INT < 18) {
            if (!android.support.v4.app.au.b) {
                try {
                    String v2_1 = new Class[2];
                    v2_1[0] = String;
                    v2_1[1] = android.os.IBinder;
                    int v0_3 = android.os.Bundle.getMethod("putIBinder", v2_1);
                    android.support.v4.app.au.a = v0_3;
                    v0_3.setAccessible(1);
                } catch (int v0_4) {
                    android.util.Log.i("BundleCompatDonut", "Failed to retrieve putIBinder method", v0_4);
                }
                android.support.v4.app.au.b = 1;
            }
            if (android.support.v4.app.au.a != null) {
                try {
                    String v1_5 = new Object[2];
                    v1_5[0] = p7;
                    v1_5[1] = p8;
                    android.support.v4.app.au.a.invoke(p6, v1_5);
                } catch (int v0_7) {
                    android.util.Log.i("BundleCompatDonut", "Failed to invoke putIBinder via reflection", v0_7);
                    android.support.v4.app.au.a = 0;
                } catch (int v0_7) {
                } catch (int v0_7) {
                }
            }
        } else {
            p6.putBinder(p7, p8);
        }
        return;
    }
}
