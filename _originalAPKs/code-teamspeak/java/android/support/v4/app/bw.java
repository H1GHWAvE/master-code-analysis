package android.support.v4.app;
public abstract class bw extends android.support.v4.view.by {
    private static final String c = "FragmentPagerAdapter";
    private static final boolean d;
    private final android.support.v4.app.bi e;
    private android.support.v4.app.cd f;
    private android.support.v4.app.Fragment g;

    private bw(android.support.v4.app.bi p2)
    {
        this.f = 0;
        this.g = 0;
        this.e = p2;
        return;
    }

    private static long a(int p2)
    {
        return ((long) p2);
    }

    private static String a(int p3, long p4)
    {
        return new StringBuilder("android:switcher:").append(p3).append(":").append(p4).toString();
    }

    public abstract android.support.v4.app.Fragment a();

    public final Object a(android.view.ViewGroup p8, int p9)
    {
        if (this.f == null) {
            this.f = this.e.a();
        }
        android.support.v4.app.Fragment v0_5 = this.e.a(android.support.v4.app.bw.a(p8.getId(), ((long) p9)));
        if (v0_5 == null) {
            v0_5 = this.a();
            this.f.a(p8.getId(), v0_5, android.support.v4.app.bw.a(p8.getId(), ((long) p9)));
        } else {
            this.f.f(v0_5);
        }
        if (v0_5 != this.g) {
            v0_5.b(0);
            v0_5.c(0);
        }
        return v0_5;
    }

    public final void a(int p2, Object p3)
    {
        if (this.f == null) {
            this.f = this.e.a();
        }
        this.f.e(((android.support.v4.app.Fragment) p3));
        return;
    }

    public final void a(android.os.Parcelable p1, ClassLoader p2)
    {
        return;
    }

    public final void a(Object p4)
    {
        if (((android.support.v4.app.Fragment) p4) != this.g) {
            if (this.g != null) {
                this.g.b(0);
                this.g.c(0);
            }
            if (((android.support.v4.app.Fragment) p4) != null) {
                ((android.support.v4.app.Fragment) p4).b(1);
                ((android.support.v4.app.Fragment) p4).c(1);
            }
            this.g = ((android.support.v4.app.Fragment) p4);
        }
        return;
    }

    public final boolean a(android.view.View p2, Object p3)
    {
        int v0_1;
        if (((android.support.v4.app.Fragment) p3).ac != p2) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    public final void b()
    {
        return;
    }

    public final void c()
    {
        if (this.f != null) {
            this.f.j();
            this.f = 0;
            this.e.b();
        }
        return;
    }

    public final android.os.Parcelable d()
    {
        return 0;
    }
}
