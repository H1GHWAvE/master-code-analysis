package android.support.v4.app;
final class BackStackState implements android.os.Parcelable {
    public static final android.os.Parcelable$Creator CREATOR;
    final int[] a;
    final int b;
    final int c;
    final String d;
    final int e;
    final int f;
    final CharSequence g;
    final int h;
    final CharSequence i;
    final java.util.ArrayList j;
    final java.util.ArrayList k;

    static BackStackState()
    {
        android.support.v4.app.BackStackState.CREATOR = new android.support.v4.app.aq();
        return;
    }

    public BackStackState(android.os.Parcel p2)
    {
        this.a = p2.createIntArray();
        this.b = p2.readInt();
        this.c = p2.readInt();
        this.d = p2.readString();
        this.e = p2.readInt();
        this.f = p2.readInt();
        this.g = ((CharSequence) android.text.TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(p2));
        this.h = p2.readInt();
        this.i = ((CharSequence) android.text.TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(p2));
        this.j = p2.createStringArrayList();
        this.k = p2.createStringArrayList();
        return;
    }

    public BackStackState(android.support.v4.app.ak p9)
    {
        int v1_0 = p9.l;
        int v0_1 = 0;
        while (v1_0 != 0) {
            if (v1_0.i != null) {
                v0_1 += v1_0.i.size();
            }
            v1_0 = v1_0.a;
        }
        int v0_3 = new int[(v0_1 + (p9.n * 7))];
        this.a = v0_3;
        if (p9.u) {
            android.support.v4.app.ao v5 = p9.l;
            int v0_6 = 0;
            while (v5 != null) {
                int v0_18;
                int v2_0 = (v0_6 + 1);
                this.a[v0_6] = v5.c;
                int v4_1 = (v2_0 + 1);
                if (v5.d == null) {
                    v0_18 = -1;
                } else {
                    v0_18 = v5.d.z;
                }
                this.a[v2_0] = v0_18;
                int v1_5 = (v4_1 + 1);
                this.a[v4_1] = v5.e;
                int v2_2 = (v1_5 + 1);
                this.a[v1_5] = v5.f;
                int v1_6 = (v2_2 + 1);
                this.a[v2_2] = v5.g;
                int v2_3 = (v1_6 + 1);
                this.a[v1_6] = v5.h;
                if (v5.i == null) {
                    v0_6 = (v2_3 + 1);
                    this.a[v2_3] = 0;
                } else {
                    int v6 = v5.i.size();
                    int v1_8 = (v2_3 + 1);
                    this.a[v2_3] = v6;
                    int v2_4 = 0;
                    while (v2_4 < v6) {
                        int v4_5 = (v1_8 + 1);
                        this.a[v1_8] = ((android.support.v4.app.Fragment) v5.i.get(v2_4)).z;
                        v2_4++;
                        v1_8 = v4_5;
                    }
                    v0_6 = v1_8;
                }
                v5 = v5.a;
            }
            this.b = p9.s;
            this.c = p9.t;
            this.d = p9.w;
            this.e = p9.y;
            this.f = p9.z;
            this.g = p9.A;
            this.h = p9.B;
            this.i = p9.C;
            this.j = p9.D;
            this.k = p9.E;
            return;
        } else {
            throw new IllegalStateException("Not on back stack");
        }
    }

    public final android.support.v4.app.ak a(android.support.v4.app.bl p12)
    {
        android.support.v4.app.ak v6_1 = new android.support.v4.app.ak(p12);
        int v1 = 0;
        int v0_0 = 0;
        while (v0_0 < this.a.length) {
            android.support.v4.app.ao v7_1 = new android.support.v4.app.ao();
            int v4_0 = (v0_0 + 1);
            v7_1.c = this.a[v0_0];
            if (android.support.v4.app.bl.b) {
                android.util.Log.v("FragmentManager", new StringBuilder("Instantiate ").append(v6_1).append(" op #").append(v1).append(" base fragment #").append(this.a[v4_0]).toString());
            }
            int v3_11 = (v4_0 + 1);
            int v0_15 = this.a[v4_0];
            if (v0_15 < 0) {
                v7_1.d = 0;
            } else {
                v7_1.d = ((android.support.v4.app.Fragment) p12.l.get(v0_15));
            }
            int v4_2 = (v3_11 + 1);
            v7_1.e = this.a[v3_11];
            int v3_12 = (v4_2 + 1);
            v7_1.f = this.a[v4_2];
            int v4_3 = (v3_12 + 1);
            v7_1.g = this.a[v3_12];
            int v5_5 = (v4_3 + 1);
            v7_1.h = this.a[v4_3];
            int v3_13 = (v5_5 + 1);
            int v8 = this.a[v5_5];
            if (v8 > 0) {
                v7_1.i = new java.util.ArrayList(v8);
                int v4_4 = 0;
                while (v4_4 < v8) {
                    if (android.support.v4.app.bl.b) {
                        android.util.Log.v("FragmentManager", new StringBuilder("Instantiate ").append(v6_1).append(" set remove fragment #").append(this.a[v3_13]).toString());
                    }
                    int v5_12 = (v3_13 + 1);
                    v7_1.i.add(((android.support.v4.app.Fragment) p12.l.get(this.a[v3_13])));
                    v4_4++;
                    v3_13 = v5_12;
                }
            }
            v6_1.a(v7_1);
            v1++;
            v0_0 = v3_13;
        }
        v6_1.s = this.b;
        v6_1.t = this.c;
        v6_1.w = this.d;
        v6_1.y = this.e;
        v6_1.u = 1;
        v6_1.z = this.f;
        v6_1.A = this.g;
        v6_1.B = this.h;
        v6_1.C = this.i;
        v6_1.D = this.j;
        v6_1.E = this.k;
        v6_1.e(1);
        return v6_1;
    }

    public final int describeContents()
    {
        return 0;
    }

    public final void writeToParcel(android.os.Parcel p3, int p4)
    {
        p3.writeIntArray(this.a);
        p3.writeInt(this.b);
        p3.writeInt(this.c);
        p3.writeString(this.d);
        p3.writeInt(this.e);
        p3.writeInt(this.f);
        android.text.TextUtils.writeToParcel(this.g, p3, 0);
        p3.writeInt(this.h);
        android.text.TextUtils.writeToParcel(this.i, p3, 0);
        p3.writeStringList(this.j);
        p3.writeStringList(this.k);
        return;
    }
}
