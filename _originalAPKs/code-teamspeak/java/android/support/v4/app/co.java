package android.support.v4.app;
public final class co extends android.support.v4.app.Fragment {
    static final int a = 16711681;
    static final int b = 16711682;
    static final int c = 16711683;
    private final android.widget.AdapterView$OnItemClickListener at;
    android.widget.ListAdapter d;
    android.widget.ListView e;
    android.view.View f;
    android.widget.TextView g;
    android.view.View h;
    android.view.View i;
    CharSequence j;
    boolean k;
    private final android.os.Handler l;
    private final Runnable m;

    public co()
    {
        this.l = new android.os.Handler();
        this.m = new android.support.v4.app.cp(this);
        this.at = new android.support.v4.app.cq(this);
        return;
    }

    private void A()
    {
        int v3 = 0;
        if (this.e == null) {
            android.os.IBinder v0_1 = this.ac;
            if (v0_1 != null) {
                if (!(v0_1 instanceof android.widget.ListView)) {
                    this.g = ((android.widget.TextView) v0_1.findViewById(16711681));
                    if (this.g != null) {
                        this.g.setVisibility(8);
                    } else {
                        this.f = v0_1.findViewById(16908292);
                    }
                    this.h = v0_1.findViewById(16711682);
                    this.i = v0_1.findViewById(16711683);
                    android.os.IBinder v0_2 = v0_1.findViewById(16908298);
                    if ((v0_2 instanceof android.widget.ListView)) {
                        this.e = ((android.widget.ListView) v0_2);
                        if (this.f == null) {
                            if (this.j != null) {
                                this.g.setText(this.j);
                                this.e.setEmptyView(this.g);
                            }
                        } else {
                            this.e.setEmptyView(this.f);
                        }
                    } else {
                        if (v0_2 != null) {
                            throw new RuntimeException("Content has view with id attribute \'android.R.id.list\' that is not a ListView class");
                        } else {
                            throw new RuntimeException("Your content must have a ListView whose id attribute is \'android.R.id.list\'");
                        }
                    }
                } else {
                    this.e = ((android.widget.ListView) v0_1);
                }
                this.k = 1;
                this.e.setOnItemClickListener(this.at);
                if (this.d == null) {
                    if (this.h != null) {
                        this.a(0, 0);
                    }
                } else {
                    android.os.IBinder v0_19;
                    android.widget.TextView v1_20 = this.d;
                    this.d = 0;
                    if (this.d == null) {
                        v0_19 = 0;
                    } else {
                        v0_19 = 1;
                    }
                    this.d = v1_20;
                    if (this.e != null) {
                        this.e.setAdapter(v1_20);
                        if ((!this.k) && (v0_19 == null)) {
                            if (this.ac.getWindowToken() != null) {
                                v3 = 1;
                            }
                            this.a(1, v3);
                        }
                    }
                }
                this.l.post(this.m);
            } else {
                throw new IllegalStateException("Content view not yet created");
            }
        }
        return;
    }

    public static void a()
    {
        return;
    }

    private void a(int p2)
    {
        this.A();
        this.e.setSelection(p2);
        return;
    }

    private void a(android.widget.ListAdapter p5)
    {
        android.os.IBinder v0_1;
        int v2 = 0;
        if (this.d == null) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        this.d = p5;
        if (this.e != null) {
            this.e.setAdapter(p5);
            if ((!this.k) && (v0_1 == null)) {
                if (this.ac.getWindowToken() != null) {
                    v2 = 1;
                }
                this.a(1, v2);
            }
        }
        return;
    }

    private void a(CharSequence p3)
    {
        this.A();
        if (this.g != null) {
            this.g.setText(p3);
            if (this.j == null) {
                this.e.setEmptyView(this.g);
            }
            this.j = p3;
            return;
        } else {
            throw new IllegalStateException("Can\'t be used with a custom content view");
        }
    }

    private void a(boolean p2)
    {
        this.a(p2, 1);
        return;
    }

    private void a(boolean p7, boolean p8)
    {
        this.A();
        if (this.h != null) {
            if (this.k != p7) {
                this.k = p7;
                if (!p7) {
                    if (!p8) {
                        this.h.clearAnimation();
                        this.i.clearAnimation();
                    } else {
                        this.h.startAnimation(android.view.animation.AnimationUtils.loadAnimation(this.i(), 17432576));
                        this.i.startAnimation(android.view.animation.AnimationUtils.loadAnimation(this.i(), 17432577));
                    }
                    this.h.setVisibility(0);
                    this.i.setVisibility(8);
                } else {
                    if (!p8) {
                        this.h.clearAnimation();
                        this.i.clearAnimation();
                    } else {
                        this.h.startAnimation(android.view.animation.AnimationUtils.loadAnimation(this.i(), 17432577));
                        this.i.startAnimation(android.view.animation.AnimationUtils.loadAnimation(this.i(), 17432576));
                    }
                    this.h.setVisibility(8);
                    this.i.setVisibility(0);
                }
            }
            return;
        } else {
            throw new IllegalStateException("Can\'t be used with a custom content view");
        }
    }

    private int b()
    {
        this.A();
        return this.e.getSelectedItemPosition();
    }

    private long d()
    {
        this.A();
        return this.e.getSelectedItemId();
    }

    private void d(boolean p2)
    {
        this.a(p2, 0);
        return;
    }

    private android.widget.ListView y()
    {
        this.A();
        return this.e;
    }

    private android.widget.ListAdapter z()
    {
        return this.d;
    }

    public final android.view.View a(android.view.LayoutInflater p10, android.view.ViewGroup p11)
    {
        android.widget.FrameLayout$LayoutParams v0_0 = this.i();
        android.widget.FrameLayout v1_1 = new android.widget.FrameLayout(v0_0);
        android.widget.FrameLayout v2_1 = new android.widget.LinearLayout(v0_0);
        v2_1.setId(16711682);
        v2_1.setOrientation(1);
        v2_1.setVisibility(8);
        v2_1.setGravity(17);
        v2_1.addView(new android.widget.ProgressBar(v0_0, 0, 16842874), new android.widget.FrameLayout$LayoutParams(-2, -2));
        v1_1.addView(v2_1, new android.widget.FrameLayout$LayoutParams(-1, -1));
        android.widget.FrameLayout v2_3 = new android.widget.FrameLayout(v0_0);
        v2_3.setId(16711683);
        android.widget.FrameLayout$LayoutParams v0_3 = new android.widget.TextView(this.i());
        v0_3.setId(16711681);
        v0_3.setGravity(17);
        v2_3.addView(v0_3, new android.widget.FrameLayout$LayoutParams(-1, -1));
        android.widget.FrameLayout$LayoutParams v0_5 = new android.widget.ListView(this.i());
        v0_5.setId(16908298);
        v0_5.setDrawSelectorOnTop(0);
        v2_3.addView(v0_5, new android.widget.FrameLayout$LayoutParams(-1, -1));
        v1_1.addView(v2_3, new android.widget.FrameLayout$LayoutParams(-1, -1));
        v1_1.setLayoutParams(new android.widget.FrameLayout$LayoutParams(-1, -1));
        return v1_1;
    }

    public final void a(android.view.View p1, android.os.Bundle p2)
    {
        super.a(p1, p2);
        this.A();
        return;
    }

    public final void g()
    {
        this.l.removeCallbacks(this.m);
        this.e = 0;
        this.k = 0;
        this.i = 0;
        this.h = 0;
        this.f = 0;
        this.g = 0;
        super.g();
        return;
    }
}
