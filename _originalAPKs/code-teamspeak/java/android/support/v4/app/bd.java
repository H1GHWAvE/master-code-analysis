package android.support.v4.app;
final class bd extends android.support.v4.app.bh {
    final synthetic android.support.v4.app.bb a;

    public bd(android.support.v4.app.bb p1)
    {
        this.a = p1;
        this(p1);
        return;
    }

    private android.support.v4.app.bb j()
    {
        return this.a;
    }

    public final android.view.View a(int p2)
    {
        return this.a.findViewById(p2);
    }

    public final void a(android.support.v4.app.Fragment p2, android.content.Intent p3, int p4)
    {
        this.a.a(p2, p3, p4);
        return;
    }

    public final void a(android.support.v4.app.Fragment p2, String[] p3, int p4)
    {
        android.support.v4.app.bb.a(this.a, p2, p3, p4);
        return;
    }

    public final void a(String p3, java.io.PrintWriter p4, String[] p5)
    {
        this.a.dump(p3, 0, p4, p5);
        return;
    }

    public final boolean a()
    {
        int v0_3;
        int v0_1 = this.a.getWindow();
        if ((v0_1 == 0) || (v0_1.peekDecorView() == null)) {
            v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        return v0_3;
    }

    public final boolean a(String p4)
    {
        int v0_1;
        if (android.os.Build$VERSION.SDK_INT < 23) {
            v0_1 = 0;
        } else {
            v0_1 = this.a.shouldShowRequestPermissionRationale(p4);
        }
        return v0_1;
    }

    public final boolean b()
    {
        int v0_2;
        if (this.a.isFinishing()) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    public final android.view.LayoutInflater c()
    {
        return this.a.getLayoutInflater().cloneInContext(this.a);
    }

    public final void d()
    {
        this.a.b();
        return;
    }

    public final boolean e()
    {
        int v0_2;
        if (this.a.getWindow() == null) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    public final int f()
    {
        int v0_3;
        int v0_1 = this.a.getWindow();
        if (v0_1 != 0) {
            v0_3 = v0_1.getAttributes().windowAnimations;
        } else {
            v0_3 = 0;
        }
        return v0_3;
    }

    public final void g()
    {
        return;
    }

    public final bridge synthetic Object h()
    {
        return this.a;
    }
}
