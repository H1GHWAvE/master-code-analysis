package android.support.v4.app;
final class am implements android.view.ViewTreeObserver$OnPreDrawListener {
    final synthetic android.view.View a;
    final synthetic Object b;
    final synthetic java.util.ArrayList c;
    final synthetic android.support.v4.app.ap d;
    final synthetic boolean e;
    final synthetic android.support.v4.app.Fragment f;
    final synthetic android.support.v4.app.Fragment g;
    final synthetic android.support.v4.app.ak h;

    am(android.support.v4.app.ak p1, android.view.View p2, Object p3, java.util.ArrayList p4, android.support.v4.app.ap p5, boolean p6, android.support.v4.app.Fragment p7, android.support.v4.app.Fragment p8)
    {
        this.h = p1;
        this.a = p2;
        this.b = p3;
        this.c = p4;
        this.d = p5;
        this.e = p6;
        this.f = p7;
        this.g = p8;
        return;
    }

    public final boolean onPreDraw()
    {
        this.a.getViewTreeObserver().removeOnPreDrawListener(this);
        if (this.b != null) {
            android.support.v4.app.ce.a(this.b, this.c);
            this.c.clear();
            int v0_6 = android.support.v4.app.ak.a(this.h, this.d, this.e, this.f);
            android.support.v4.app.ce.a(this.b, this.d.d, v0_6, this.c);
            android.support.v4.app.ak.a(this.h, v0_6, this.d);
            android.support.v4.app.ak.a(this.f, this.g, this.e, v0_6);
        }
        return 1;
    }
}
