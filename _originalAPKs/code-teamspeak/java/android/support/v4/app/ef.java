package android.support.v4.app;
final class ef {

    ef()
    {
        return;
    }

    static android.app.Notification$Action a(android.support.v4.app.ek p5)
    {
        android.app.Notification$Action$Builder v1_2 = new android.app.Notification$Action$Builder(p5.a(), p5.b(), p5.c()).addExtras(p5.d());
        int v0_2 = p5.e();
        if (v0_2 != 0) {
            android.app.RemoteInput[] v2_1 = android.support.v4.app.fu.a(v0_2);
            int v3_1 = v2_1.length;
            int v0_3 = 0;
            while (v0_3 < v3_1) {
                v1_2.addRemoteInput(v2_1[v0_3]);
                v0_3++;
            }
        }
        return v1_2.build();
    }

    static android.support.v4.app.ek a(android.app.Notification$Action p9, android.support.v4.app.el p10, android.support.v4.app.fx p11)
    {
        android.support.v4.app.fw[] v5_0;
        android.app.RemoteInput[] v8 = p9.getRemoteInputs();
        if (v8 != null) {
            android.support.v4.app.fw[] v7 = p11.a(v8.length);
            int v6 = 0;
            while (v6 < v8.length) {
                int v0_3 = v8[v6];
                v7[v6] = p11.a(v0_3.getResultKey(), v0_3.getLabel(), v0_3.getChoices(), v0_3.getAllowFreeFormInput(), v0_3.getExtras());
                v6++;
            }
            v5_0 = v7;
        } else {
            v5_0 = 0;
        }
        return p10.a(p9.icon, p9.title, p9.actionIntent, p9.getExtras(), v5_0);
    }

    private static android.support.v4.app.ek a(android.app.Notification p1, int p2, android.support.v4.app.el p3, android.support.v4.app.fx p4)
    {
        return android.support.v4.app.ef.a(p1.actions[p2], p3, p4);
    }

    private static java.util.ArrayList a(android.support.v4.app.ek[] p4)
    {
        java.util.ArrayList v0_1;
        if (p4 != null) {
            v0_1 = new java.util.ArrayList(p4.length);
            int v2 = p4.length;
            int v1_1 = 0;
            while (v1_1 < v2) {
                v0_1.add(android.support.v4.app.ef.a(p4[v1_1]));
                v1_1++;
            }
        } else {
            v0_1 = 0;
        }
        return v0_1;
    }

    public static void a(android.app.Notification$Builder p5, android.support.v4.app.ek p6)
    {
        android.app.Notification$Action$Builder v1_1 = new android.app.Notification$Action$Builder(p6.a(), p6.b(), p6.c());
        if (p6.e() != null) {
            android.app.RemoteInput[] v2_1 = android.support.v4.app.fu.a(p6.e());
            int v3_1 = v2_1.length;
            int v0_3 = 0;
            while (v0_3 < v3_1) {
                v1_1.addRemoteInput(v2_1[v0_3]);
                v0_3++;
            }
        }
        if (p6.d() != null) {
            v1_1.addExtras(p6.d());
        }
        p5.addAction(v1_1.build());
        return;
    }

    private static boolean a(android.app.Notification p1)
    {
        int v0_2;
        if ((p1.flags & 256) == 0) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    private static android.support.v4.app.ek[] a(java.util.ArrayList p3, android.support.v4.app.el p4, android.support.v4.app.fx p5)
    {
        android.support.v4.app.ek[] v0_3;
        if (p3 != null) {
            android.support.v4.app.ek[] v2 = p4.a(p3.size());
            int v1 = 0;
            while (v1 < v2.length) {
                v2[v1] = android.support.v4.app.ef.a(((android.app.Notification$Action) p3.get(v1)), p4, p5);
                v1++;
            }
            v0_3 = v2;
        } else {
            v0_3 = 0;
        }
        return v0_3;
    }

    private static String b(android.app.Notification p1)
    {
        return p1.getGroup();
    }

    private static boolean c(android.app.Notification p1)
    {
        int v0_2;
        if ((p1.flags & 512) == 0) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    private static String d(android.app.Notification p1)
    {
        return p1.getSortKey();
    }
}
