package android.support.v4.app;
final class eh {
    public static final String a = "call";
    public static final String b = "msg";
    public static final String c = "email";
    public static final String d = "event";
    public static final String e = "promo";
    public static final String f = "alarm";
    public static final String g = "progress";
    public static final String h = "social";
    public static final String i = "err";
    public static final String j = "transport";
    public static final String k = "sys";
    public static final String l = "service";
    public static final String m = "recommendation";
    public static final String n = "status";
    private static final String o = "author";
    private static final String p = "text";
    private static final String q = "messages";
    private static final String r = "remote_input";
    private static final String s = "on_reply";
    private static final String t = "on_read";
    private static final String u = "participants";
    private static final String v = "timestamp";

    eh()
    {
        return;
    }

    static android.app.RemoteInput a(android.support.v4.app.fw p2)
    {
        return new android.app.RemoteInput$Builder(p2.a()).setLabel(p2.b()).setChoices(p2.c()).setAllowFreeFormInput(p2.d()).addExtras(p2.e()).build();
    }

    private static android.os.Bundle a(android.support.v4.app.em p7)
    {
        android.os.Bundle v0_0 = 0;
        String[] v1_0 = 0;
        if (p7 != null) {
            android.os.Bundle v2_1 = new android.os.Bundle();
            if ((p7.d() != null) && (p7.d().length > 1)) {
                v0_0 = p7.d()[0];
            }
            android.app.RemoteInput$Builder v3_5 = new android.os.Parcelable[p7.a().length];
            while (v1_0 < v3_5.length) {
                long v4_8 = new android.os.Bundle();
                v4_8.putString("text", p7.a()[v1_0]);
                v4_8.putString("author", v0_0);
                v3_5[v1_0] = v4_8;
                v1_0++;
            }
            v2_1.putParcelableArray("messages", v3_5);
            android.os.Bundle v0_3 = p7.g();
            if (v0_3 != null) {
                v2_1.putParcelable("remote_input", new android.app.RemoteInput$Builder(v0_3.a()).setLabel(v0_3.b()).setChoices(v0_3.c()).setAllowFreeFormInput(v0_3.d()).addExtras(v0_3.e()).build());
            }
            v2_1.putParcelable("on_reply", p7.b());
            v2_1.putParcelable("on_read", p7.c());
            v2_1.putStringArray("participants", p7.d());
            v2_1.putLong("timestamp", p7.f());
            v0_0 = v2_1;
        }
        return v0_0;
    }

    private static android.support.v4.app.em a(android.os.Bundle p12, android.support.v4.app.en p13, android.support.v4.app.fx p14)
    {
        boolean v2_0 = 0;
        android.support.v4.app.em v4_0 = 0;
        if (p12 != null) {
            String[] v10;
            long v6_0 = p12.getParcelableArray("messages");
            if (v6_0 == 0) {
                v10 = 0;
            } else {
                android.app.PendingIntent v3_0 = new String[v6_0.length];
                String[] v1_0 = 0;
                while (v1_0 < v3_0.length) {
                    if ((v6_0[v1_0] instanceof android.os.Bundle)) {
                        v3_0[v1_0] = ((android.os.Bundle) v6_0[v1_0]).getString("text");
                        if (v3_0[v1_0] != null) {
                            v1_0++;
                        }
                    }
                    if (!v2_0) {
                        return v4_0;
                    } else {
                        v10 = v3_0;
                    }
                }
                v2_0 = 1;
            }
            android.app.PendingIntent v8_1 = ((android.app.PendingIntent) p12.getParcelable("on_read"));
            android.app.PendingIntent v9_1 = ((android.app.PendingIntent) p12.getParcelable("on_reply"));
            android.support.v4.app.en v0_16 = ((android.app.RemoteInput) p12.getParcelable("remote_input"));
            String[] v11 = p12.getStringArray("participants");
            if ((v11 != null) && (v11.length == 1)) {
                boolean v2_1;
                if (v0_16 == null) {
                    v2_1 = 0;
                } else {
                    v2_1 = p14.a(v0_16.getResultKey(), v0_16.getLabel(), v0_16.getChoices(), v0_16.getAllowFreeFormInput(), v0_16.getExtras());
                }
                v4_0 = p13.a(v10, v2_1, v9_1, v8_1, v11, p12.getLong("timestamp"));
            }
        }
        return v4_0;
    }

    private static android.support.v4.app.fw a(android.app.RemoteInput p6, android.support.v4.app.fx p7)
    {
        return p7.a(p6.getResultKey(), p6.getLabel(), p6.getChoices(), p6.getAllowFreeFormInput(), p6.getExtras());
    }

    private static String a(android.app.Notification p1)
    {
        return p1.category;
    }
}
