package android.support.design.widget;
abstract class al {
    static final int b = 200;
    static final int[] c;
    static final int[] d;
    static final int[] e;
    final android.view.View f;
    final android.support.design.widget.as g;

    static al()
    {
        int[] v0_0 = new int[2];
        v0_0 = {16842919, 16842910};
        android.support.design.widget.al.c = v0_0;
        int[] v0_1 = new int[2];
        v0_1 = {16842908, 16842910};
        android.support.design.widget.al.d = v0_1;
        int[] v0_3 = new int[0];
        android.support.design.widget.al.e = v0_3;
        return;
    }

    al(android.view.View p1, android.support.design.widget.as p2)
    {
        this.f = p1;
        this.g = p2;
        return;
    }

    final android.graphics.drawable.Drawable a(int p8, android.content.res.ColorStateList p9)
    {
        int v0_1 = this.f.getResources();
        android.support.design.widget.j v1 = this.d();
        android.graphics.Paint v2_1 = v0_1.getColor(android.support.design.f.design_fab_stroke_top_outer_color);
        int v3_1 = v0_1.getColor(android.support.design.f.design_fab_stroke_top_inner_color);
        int v4_1 = v0_1.getColor(android.support.design.f.design_fab_stroke_end_inner_color);
        int v0_2 = v0_1.getColor(android.support.design.f.design_fab_stroke_end_outer_color);
        v1.e = v2_1;
        v1.f = v3_1;
        v1.g = v4_1;
        v1.h = v0_2;
        if (v1.d != ((float) p8)) {
            v1.d = ((float) p8);
            v1.a.setStrokeWidth((((float) p8) * 1068149139));
            v1.j = 1;
            v1.invalidateSelf();
        }
        v1.i = p9.getDefaultColor();
        v1.j = 1;
        v1.invalidateSelf();
        return v1;
    }

    abstract void a();

    abstract void a();

    abstract void a();

    abstract void a();

    abstract void a();

    abstract void a();

    abstract void a();

    abstract void b();

    abstract void b();

    abstract void c();

    android.support.design.widget.j d()
    {
        return new android.support.design.widget.j();
    }
}
