package android.support.design.widget;
final class de {
    private static final ThreadLocal a;
    private static final ThreadLocal b;
    private static final android.graphics.Matrix c;

    static de()
    {
        android.support.design.widget.de.a = new ThreadLocal();
        android.support.design.widget.de.b = new ThreadLocal();
        android.support.design.widget.de.c = new android.graphics.Matrix();
        return;
    }

    de()
    {
        return;
    }

    public static void a(android.view.ViewGroup p5, android.view.View p6, android.graphics.Rect p7)
    {
        int v1_1;
        int v0_2 = ((android.graphics.Matrix) android.support.design.widget.de.a.get());
        if (v0_2 != 0) {
            v0_2.set(android.support.design.widget.de.c);
            v1_1 = v0_2;
        } else {
            int v0_4 = new android.graphics.Matrix();
            android.support.design.widget.de.a.set(v0_4);
            v1_1 = v0_4;
        }
        android.support.design.widget.de.a(p5, p6, v1_1);
        int v0_7 = ((android.graphics.RectF) android.support.design.widget.de.b.get());
        if (v0_7 == 0) {
            v0_7 = new android.graphics.RectF();
        }
        v0_7.set(p7);
        v1_1.mapRect(v0_7);
        p7.set(((int) (v0_7.left + 1056964608)), ((int) (v0_7.top + 1056964608)), ((int) (v0_7.right + 1056964608)), ((int) (v0_7.bottom + 1056964608)));
        return;
    }

    private static void a(android.view.ViewParent p2, android.view.View p3, android.graphics.Matrix p4)
    {
        android.graphics.Matrix v0_0 = p3.getParent();
        if (((v0_0 instanceof android.view.View)) && (v0_0 != p2)) {
            android.graphics.Matrix v0_1 = ((android.view.View) v0_0);
            android.support.design.widget.de.a(p2, v0_1, p4);
            p4.preTranslate(((float) (- v0_1.getScrollX())), ((float) (- v0_1.getScrollY())));
        }
        p4.preTranslate(((float) p3.getLeft()), ((float) p3.getTop()));
        if (!p3.getMatrix().isIdentity()) {
            p4.preConcat(p3.getMatrix());
        }
        return;
    }
}
