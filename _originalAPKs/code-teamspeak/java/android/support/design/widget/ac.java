package android.support.design.widget;
final class ac implements android.support.design.widget.as {
    final synthetic android.support.design.widget.FloatingActionButton a;

    ac(android.support.design.widget.FloatingActionButton p1)
    {
        this.a = p1;
        return;
    }

    public final float a()
    {
        return (((float) this.a.getSizeDimension()) / 1073741824);
    }

    public final void a(int p6, int p7, int p8, int p9)
    {
        android.support.design.widget.FloatingActionButton.a(this.a).set(p6, p7, p8, p9);
        this.a.setPadding((android.support.design.widget.FloatingActionButton.b(this.a) + p6), (android.support.design.widget.FloatingActionButton.b(this.a) + p7), (android.support.design.widget.FloatingActionButton.b(this.a) + p8), (android.support.design.widget.FloatingActionButton.b(this.a) + p9));
        return;
    }

    public final void a(android.graphics.drawable.Drawable p2)
    {
        android.support.design.widget.FloatingActionButton.a(this.a, p2);
        return;
    }
}
