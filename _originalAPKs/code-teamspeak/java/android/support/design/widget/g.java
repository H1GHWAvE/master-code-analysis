package android.support.design.widget;
public final class g extends android.widget.LinearLayout$LayoutParams {
    public static final int a = 1;
    public static final int b = 2;
    public static final int c = 4;
    public static final int d = 8;
    static final int e = 5;
    int f;
    android.view.animation.Interpolator g;

    public g()
    {
        this(-1, -2);
        this.f = 1;
        return;
    }

    private g(int p2, int p3, float p4)
    {
        this(p2, p3, p4);
        this.f = 1;
        return;
    }

    public g(android.content.Context p4, android.util.AttributeSet p5)
    {
        this(p4, p5);
        this.f = 1;
        android.content.res.TypedArray v0_2 = p4.obtainStyledAttributes(p5, android.support.design.n.AppBarLayout_LayoutParams);
        this.f = v0_2.getInt(android.support.design.n.AppBarLayout_LayoutParams_layout_scrollFlags, 0);
        if (v0_2.hasValue(android.support.design.n.AppBarLayout_LayoutParams_layout_scrollInterpolator)) {
            this.g = android.view.animation.AnimationUtils.loadInterpolator(p4, v0_2.getResourceId(android.support.design.n.AppBarLayout_LayoutParams_layout_scrollInterpolator, 0));
        }
        v0_2.recycle();
        return;
    }

    private g(android.support.design.widget.g p2)
    {
        this(p2);
        this.f = 1;
        this.f = p2.f;
        this.g = p2.g;
        return;
    }

    public g(android.view.ViewGroup$LayoutParams p2)
    {
        this(p2);
        this.f = 1;
        return;
    }

    public g(android.view.ViewGroup$MarginLayoutParams p2)
    {
        this(p2);
        this.f = 1;
        return;
    }

    public g(android.widget.LinearLayout$LayoutParams p2)
    {
        this(p2);
        this.f = 1;
        return;
    }

    private int a()
    {
        return this.f;
    }

    private void a(int p1)
    {
        this.f = p1;
        return;
    }

    private void a(android.view.animation.Interpolator p1)
    {
        this.g = p1;
        return;
    }

    private android.view.animation.Interpolator b()
    {
        return this.g;
    }
}
