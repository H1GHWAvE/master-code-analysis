package android.support.design.widget;
public final class m extends android.widget.FrameLayout {
    private static final int a = 600;
    private boolean b;
    private int c;
    private android.support.v7.widget.Toolbar d;
    private android.view.View e;
    private int f;
    private int g;
    private int h;
    private int i;
    private final android.graphics.Rect j;
    private final android.support.design.widget.l k;
    private boolean l;
    private android.graphics.drawable.Drawable m;
    private android.graphics.drawable.Drawable n;
    private int o;
    private boolean p;
    private android.support.design.widget.ck q;
    private android.support.design.widget.i r;
    private int s;
    private android.support.v4.view.gh t;

    private m(android.content.Context p2)
    {
        this(p2, 0);
        return;
    }

    private m(android.content.Context p2, byte p3)
    {
        this(p2, 0);
        return;
    }

    private m(android.content.Context p7, char p8)
    {
        android.support.design.widget.n v0_11;
        this(p7, 0, 0);
        this.b = 1;
        this.j = new android.graphics.Rect();
        this.k = new android.support.design.widget.l(this);
        this.k.a(android.support.design.widget.a.c);
        android.content.res.TypedArray v3_2 = p7.obtainStyledAttributes(0, android.support.design.n.CollapsingToolbarLayout, 0, android.support.design.m.Widget_Design_CollapsingToolbar);
        this.k.c(v3_2.getInt(android.support.design.n.CollapsingToolbarLayout_expandedTitleGravity, 8388691));
        this.k.d(v3_2.getInt(android.support.design.n.CollapsingToolbarLayout_collapsedTitleGravity, 8388627));
        android.support.design.widget.n v0_9 = v3_2.getDimensionPixelSize(android.support.design.n.CollapsingToolbarLayout_expandedTitleMargin, 0);
        this.i = v0_9;
        this.h = v0_9;
        this.g = v0_9;
        this.f = v0_9;
        if (android.support.v4.view.cx.f(this) != 1) {
            v0_11 = 0;
        } else {
            v0_11 = 1;
        }
        if (v3_2.hasValue(android.support.design.n.CollapsingToolbarLayout_expandedTitleMarginStart)) {
            int v4_8 = v3_2.getDimensionPixelSize(android.support.design.n.CollapsingToolbarLayout_expandedTitleMarginStart, 0);
            if (v0_11 == null) {
                this.f = v4_8;
            } else {
                this.h = v4_8;
            }
        }
        if (v3_2.hasValue(android.support.design.n.CollapsingToolbarLayout_expandedTitleMarginEnd)) {
            int v4_12 = v3_2.getDimensionPixelSize(android.support.design.n.CollapsingToolbarLayout_expandedTitleMarginEnd, 0);
            if (v0_11 == null) {
                this.h = v4_12;
            } else {
                this.f = v4_12;
            }
        }
        if (v3_2.hasValue(android.support.design.n.CollapsingToolbarLayout_expandedTitleMarginTop)) {
            this.g = v3_2.getDimensionPixelSize(android.support.design.n.CollapsingToolbarLayout_expandedTitleMarginTop, 0);
        }
        if (v3_2.hasValue(android.support.design.n.CollapsingToolbarLayout_expandedTitleMarginBottom)) {
            this.i = v3_2.getDimensionPixelSize(android.support.design.n.CollapsingToolbarLayout_expandedTitleMarginBottom, 0);
        }
        this.l = v3_2.getBoolean(android.support.design.n.CollapsingToolbarLayout_titleEnabled, 1);
        this.setTitle(v3_2.getText(android.support.design.n.CollapsingToolbarLayout_title));
        this.k.f(android.support.design.m.TextAppearance_Design_CollapsingToolbar_Expanded);
        this.k.e(android.support.design.m.TextAppearance_AppCompat_Widget_ActionBar_Title);
        if (v3_2.hasValue(android.support.design.n.CollapsingToolbarLayout_expandedTitleTextAppearance)) {
            this.k.f(v3_2.getResourceId(android.support.design.n.CollapsingToolbarLayout_expandedTitleTextAppearance, 0));
        }
        if (v3_2.hasValue(android.support.design.n.CollapsingToolbarLayout_collapsedTitleTextAppearance)) {
            this.k.e(v3_2.getResourceId(android.support.design.n.CollapsingToolbarLayout_collapsedTitleTextAppearance, 0));
        }
        this.setContentScrim(v3_2.getDrawable(android.support.design.n.CollapsingToolbarLayout_contentScrim));
        this.setStatusBarScrim(v3_2.getDrawable(android.support.design.n.CollapsingToolbarLayout_statusBarScrim));
        this.c = v3_2.getResourceId(android.support.design.n.CollapsingToolbarLayout_toolbarId, -1);
        v3_2.recycle();
        this.setWillNotDraw(0);
        android.support.v4.view.cx.a(this, new android.support.design.widget.n(this));
        return;
    }

    static synthetic android.support.design.widget.dg a(android.view.View p1)
    {
        return android.support.design.widget.m.b(p1);
    }

    static synthetic android.support.v4.view.gh a(android.support.design.widget.m p1)
    {
        return p1.t;
    }

    static synthetic android.support.v4.view.gh a(android.support.design.widget.m p0, android.support.v4.view.gh p1)
    {
        p0.t = p1;
        return p1;
    }

    private static android.widget.FrameLayout$LayoutParams a(android.view.ViewGroup$LayoutParams p1)
    {
        return new android.support.design.widget.p(p1);
    }

    private void a()
    {
        if (this.b) {
            int v5 = this.getChildCount();
            int v3 = 0;
            android.support.v7.widget.Toolbar v1_0 = 0;
            while (v3 < v5) {
                android.support.v7.widget.Toolbar v0_3;
                android.support.v7.widget.Toolbar v0_2 = this.getChildAt(v3);
                if (!(v0_2 instanceof android.support.v7.widget.Toolbar)) {
                    v0_3 = v1_0;
                } else {
                    if (this.c == -1) {
                        android.support.v7.widget.Toolbar v0_1 = ((android.support.v7.widget.Toolbar) v0_2);
                        if (v0_1 != null) {
                            v1_0 = v0_1;
                        }
                        this.d = v1_0;
                        this.b();
                        this.b = 0;
                        return;
                    } else {
                        if (this.c != v0_2.getId()) {
                            if (v1_0 != null) {
                            } else {
                                v0_3 = ((android.support.v7.widget.Toolbar) v0_2);
                            }
                        } else {
                            v0_1 = ((android.support.v7.widget.Toolbar) v0_2);
                        }
                    }
                }
                v3++;
                v1_0 = v0_3;
            }
            v0_1 = 0;
        }
        return;
    }

    private void a(int p3)
    {
        this.a();
        if (this.q != null) {
            if (this.q.a.b()) {
                this.q.a.e();
            }
        } else {
            this.q = android.support.design.widget.dh.a();
            this.q.a(600);
            this.q.a(android.support.design.widget.a.b);
            this.q.a(new android.support.design.widget.o(this));
        }
        this.q.a(this.o, p3);
        this.q.a.a();
        return;
    }

    static synthetic void a(android.support.design.widget.m p0, int p1)
    {
        p0.setScrimAlpha(p1);
        return;
    }

    static synthetic int b(android.support.design.widget.m p0, int p1)
    {
        p0.s = p1;
        return p1;
    }

    static synthetic android.graphics.drawable.Drawable b(android.support.design.widget.m p1)
    {
        return p1.m;
    }

    private static android.support.design.widget.dg b(android.view.View p2)
    {
        android.support.design.widget.dg v0_2 = ((android.support.design.widget.dg) p2.getTag(android.support.design.i.view_offset_helper));
        if (v0_2 == null) {
            v0_2 = new android.support.design.widget.dg(p2);
            p2.setTag(android.support.design.i.view_offset_helper, v0_2);
        }
        return v0_2;
    }

    private void b()
    {
        if ((!this.l) && (this.e != null)) {
            android.support.v7.widget.Toolbar v0_3 = this.e.getParent();
            if ((v0_3 instanceof android.view.ViewGroup)) {
                ((android.view.ViewGroup) v0_3).removeView(this.e);
            }
        }
        if ((this.l) && (this.d != null)) {
            if (this.e == null) {
                this.e = new android.view.View(this.getContext());
            }
            if (this.e.getParent() == null) {
                this.d.addView(this.e, -1, -1);
            }
        }
        return;
    }

    static synthetic android.graphics.drawable.Drawable c(android.support.design.widget.m p1)
    {
        return p1.n;
    }

    private boolean c()
    {
        return this.l;
    }

    private void d()
    {
        if (!this.p) {
            if ((!android.support.v4.view.cx.B(this)) || (this.isInEditMode())) {
                this.setScrimAlpha(255);
            } else {
                this.a(255);
            }
            this.p = 1;
        }
        return;
    }

    static synthetic void d(android.support.design.widget.m p2)
    {
        if (!p2.p) {
            if ((!android.support.v4.view.cx.B(p2)) || (p2.isInEditMode())) {
                void v2_1 = p2.setScrimAlpha(255);
            } else {
                v2_1 = p2.a(255);
            }
            v2_1.p = 1;
        }
        return;
    }

    private void e()
    {
        if (this.p) {
            if ((!android.support.v4.view.cx.B(this)) || (this.isInEditMode())) {
                this.setScrimAlpha(0);
            } else {
                this.a(0);
            }
            this.p = 0;
        }
        return;
    }

    static synthetic void e(android.support.design.widget.m p2)
    {
        if (p2.p) {
            if ((!android.support.v4.view.cx.B(p2)) || (p2.isInEditMode())) {
                void v2_1 = p2.setScrimAlpha(0);
            } else {
                v2_1 = p2.a(0);
            }
            v2_1.p = 0;
        }
        return;
    }

    static synthetic android.support.design.widget.l f(android.support.design.widget.m p1)
    {
        return p1.k;
    }

    private android.support.design.widget.p f()
    {
        return new android.support.design.widget.p(super.generateDefaultLayoutParams());
    }

    private void setScrimAlpha(int p2)
    {
        if (p2 != this.o) {
            if ((this.m != null) && (this.d != null)) {
                android.support.v4.view.cx.b(this.d);
            }
            this.o = p2;
            android.support.v4.view.cx.b(this);
        }
        return;
    }

    protected final boolean checkLayoutParams(android.view.ViewGroup$LayoutParams p2)
    {
        return (p2 instanceof android.support.design.widget.p);
    }

    public final void draw(android.graphics.Canvas p7)
    {
        super.draw(p7);
        this.a();
        if ((this.d == null) && ((this.m != null) && (this.o > 0))) {
            this.m.mutate().setAlpha(this.o);
            this.m.draw(p7);
        }
        if (this.l) {
            this.k.a(p7);
        }
        if ((this.n != null) && (this.o > 0)) {
            android.graphics.drawable.Drawable v0_11;
            if (this.t == null) {
                v0_11 = 0;
            } else {
                v0_11 = this.t.b();
            }
            if (v0_11 > null) {
                this.n.setBounds(0, (- this.s), this.getWidth(), (v0_11 - this.s));
                this.n.mutate().setAlpha(this.o);
                this.n.draw(p7);
            }
        }
        return;
    }

    protected final boolean drawChild(android.graphics.Canvas p4, android.view.View p5, long p6)
    {
        this.a();
        if ((p5 == this.d) && ((this.m != null) && (this.o > 0))) {
            this.m.mutate().setAlpha(this.o);
            this.m.draw(p4);
        }
        return super.drawChild(p4, p5, p6);
    }

    protected final synthetic android.view.ViewGroup$LayoutParams generateDefaultLayoutParams()
    {
        return this.f();
    }

    protected final synthetic android.widget.FrameLayout$LayoutParams generateDefaultLayoutParams()
    {
        return this.f();
    }

    public final bridge synthetic android.view.ViewGroup$LayoutParams generateLayoutParams(android.util.AttributeSet p2)
    {
        return this.generateLayoutParams(p2);
    }

    protected final synthetic android.view.ViewGroup$LayoutParams generateLayoutParams(android.view.ViewGroup$LayoutParams p2)
    {
        return new android.support.design.widget.p(p2);
    }

    public final android.widget.FrameLayout$LayoutParams generateLayoutParams(android.util.AttributeSet p3)
    {
        return new android.support.design.widget.p(this.getContext(), p3);
    }

    public final int getCollapsedTitleGravity()
    {
        return this.k.c;
    }

    public final android.graphics.drawable.Drawable getContentScrim()
    {
        return this.m;
    }

    public final int getExpandedTitleGravity()
    {
        return this.k.b;
    }

    final int getScrimTriggerOffset()
    {
        return (android.support.v4.view.cx.o(this) * 2);
    }

    public final android.graphics.drawable.Drawable getStatusBarScrim()
    {
        return this.n;
    }

    public final CharSequence getTitle()
    {
        int v0_1;
        if (!this.l) {
            v0_1 = 0;
        } else {
            v0_1 = this.k.g;
        }
        return v0_1;
    }

    protected final void onAttachedToWindow()
    {
        super.onAttachedToWindow();
        java.util.List v0_0 = this.getParent();
        if ((v0_0 instanceof android.support.design.widget.AppBarLayout)) {
            if (this.r == null) {
                this.r = new android.support.design.widget.q(this, 0);
            }
            java.util.List v0_1 = ((android.support.design.widget.AppBarLayout) v0_0);
            android.support.design.widget.i v1_4 = this.r;
            if ((v1_4 != null) && (!v0_1.c.contains(v1_4))) {
                v0_1.c.add(v1_4);
            }
        }
        return;
    }

    protected final void onDetachedFromWindow()
    {
        java.util.List v0_0 = this.getParent();
        if ((this.r != null) && ((v0_0 instanceof android.support.design.widget.AppBarLayout))) {
            android.support.design.widget.i v1_2 = this.r;
            if (v1_2 != null) {
                ((android.support.design.widget.AppBarLayout) v0_0).c.remove(v1_2);
            }
        }
        super.onDetachedFromWindow();
        return;
    }

    protected final void onLayout(boolean p7, int p8, int p9, int p10, int p11)
    {
        this = super.onLayout(p7, p8, p9, p10, p11);
        android.support.design.widget.l v0_0 = 0;
        CharSequence v1_0 = this.getChildCount();
        while (v0_0 < v1_0) {
            int v2_6 = this.getChildAt(v0_0);
            if ((this.t != null) && (!android.support.v4.view.cx.u(v2_6))) {
                int v3_8 = this.t.b();
                if (v2_6.getTop() < v3_8) {
                    v2_6.offsetTopAndBottom(v3_8);
                }
            }
            android.support.design.widget.m.b(v2_6).a();
            v0_0++;
        }
        if ((this.l) && (this.e != null)) {
            android.support.design.widget.cz.a(this, this.e, this.j);
            this.k.b(this.j.left, (p11 - this.j.height()), this.j.right, p11);
            this.k.a(this.f, (this.j.bottom + this.g), ((p10 - p8) - this.h), ((p11 - p9) - this.i));
            this.k.a();
        }
        if (this.d != null) {
            if ((this.l) && (android.text.TextUtils.isEmpty(this.k.g))) {
                this.k.a(this.d.getTitle());
            }
            this.setMinimumHeight(this.d.getHeight());
        }
        return;
    }

    protected final void onMeasure(int p1, int p2)
    {
        this.a();
        super.onMeasure(p1, p2);
        return;
    }

    protected final void onSizeChanged(int p3, int p4, int p5, int p6)
    {
        super.onSizeChanged(p3, p4, p5, p6);
        if (this.m != null) {
            this.m.setBounds(0, 0, p3, p4);
        }
        return;
    }

    public final void setCollapsedTitleGravity(int p2)
    {
        this.k.c(p2);
        return;
    }

    public final void setCollapsedTitleTextAppearance(int p2)
    {
        this.k.e(p2);
        return;
    }

    public final void setCollapsedTitleTextColor(int p2)
    {
        this.k.a(p2);
        return;
    }

    public final void setContentScrim(android.graphics.drawable.Drawable p4)
    {
        if (this.m != p4) {
            if (this.m != null) {
                this.m.setCallback(0);
            }
            this.m = p4;
            p4.setBounds(0, 0, this.getWidth(), this.getHeight());
            p4.setCallback(this);
            p4.mutate().setAlpha(this.o);
            android.support.v4.view.cx.b(this);
        }
        return;
    }

    public final void setContentScrimColor(int p2)
    {
        this.setContentScrim(new android.graphics.drawable.ColorDrawable(p2));
        return;
    }

    public final void setContentScrimResource(int p2)
    {
        this.setContentScrim(android.support.v4.c.h.a(this.getContext(), p2));
        return;
    }

    public final void setExpandedTitleColor(int p2)
    {
        this.k.b(p2);
        return;
    }

    public final void setExpandedTitleGravity(int p2)
    {
        this.k.c(p2);
        return;
    }

    public final void setExpandedTitleTextAppearance(int p2)
    {
        this.k.f(p2);
        return;
    }

    public final void setStatusBarScrim(android.graphics.drawable.Drawable p3)
    {
        if (this.n != p3) {
            if (this.n != null) {
                this.n.setCallback(0);
            }
            this.n = p3;
            p3.setCallback(this);
            p3.mutate().setAlpha(this.o);
            android.support.v4.view.cx.b(this);
        }
        return;
    }

    public final void setStatusBarScrimColor(int p2)
    {
        this.setStatusBarScrim(new android.graphics.drawable.ColorDrawable(p2));
        return;
    }

    public final void setStatusBarScrimResource(int p2)
    {
        this.setStatusBarScrim(android.support.v4.c.h.a(this.getContext(), p2));
        return;
    }

    public final void setTitle(CharSequence p2)
    {
        this.k.a(p2);
        return;
    }

    public final void setTitleEnabled(boolean p2)
    {
        if (p2 != this.l) {
            this.l = p2;
            this.b();
            this.requestLayout();
        }
        return;
    }
}
