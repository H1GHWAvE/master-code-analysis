package android.support.design.widget;
final class l {
    private static final boolean j;
    private static final boolean k;
    private static final android.graphics.Paint l;
    private boolean A;
    private android.graphics.Bitmap B;
    private android.graphics.Paint C;
    private float D;
    private float E;
    private float F;
    private float G;
    private boolean H;
    private android.view.animation.Interpolator I;
    float a;
    int b;
    int c;
    float d;
    float e;
    int f;
    CharSequence g;
    final android.text.TextPaint h;
    android.view.animation.Interpolator i;
    private final android.view.View m;
    private boolean n;
    private final android.graphics.Rect o;
    private final android.graphics.Rect p;
    private final android.graphics.RectF q;
    private int r;
    private float s;
    private float t;
    private float u;
    private float v;
    private float w;
    private float x;
    private CharSequence y;
    private boolean z;

    static l()
    {
        int v0_1;
        if (android.os.Build$VERSION.SDK_INT >= 18) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        android.support.design.widget.l.j = v0_1;
        android.support.design.widget.l.l = 0;
        return;
    }

    public l(android.view.View p3)
    {
        this.b = 16;
        this.c = 16;
        this.d = 1097859072;
        this.e = 1097859072;
        this.m = p3;
        this.h = new android.text.TextPaint();
        this.h.setAntiAlias(1);
        this.p = new android.graphics.Rect();
        this.o = new android.graphics.Rect();
        this.q = new android.graphics.RectF();
        return;
    }

    private static float a(float p1, float p2, float p3, android.view.animation.Interpolator p4)
    {
        if (p4 != null) {
            p3 = p4.getInterpolation(p3);
        }
        return android.support.design.widget.a.a(p1, p2, p3);
    }

    private static int a(int p5, int p6, float p7)
    {
        int v0_1 = (1065353216 - p7);
        return android.graphics.Color.argb(((int) ((((float) android.graphics.Color.alpha(p5)) * v0_1) + (((float) android.graphics.Color.alpha(p6)) * p7))), ((int) ((((float) android.graphics.Color.red(p5)) * v0_1) + (((float) android.graphics.Color.red(p6)) * p7))), ((int) ((((float) android.graphics.Color.green(p5)) * v0_1) + (((float) android.graphics.Color.green(p6)) * p7))), ((int) ((v0_1 * ((float) android.graphics.Color.blue(p5))) + (((float) android.graphics.Color.blue(p6)) * p7))));
    }

    private static boolean a(float p2, float p3)
    {
        int v0_3;
        if (Math.abs((p2 - p3)) >= 981668463) {
            v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        return v0_3;
    }

    private static boolean a(android.graphics.Rect p1, int p2, int p3, int p4, int p5)
    {
        if ((p1.left != p2) || ((p1.top != p3) || ((p1.right != p4) || (p1.bottom != p5)))) {
            int v0_4 = 0;
        } else {
            v0_4 = 1;
        }
        return v0_4;
    }

    private void b()
    {
        if ((this.p.width() <= 0) || ((this.p.height() <= 0) || ((this.o.width() <= 0) || (this.o.height() <= 0)))) {
            int v0_8 = 0;
        } else {
            v0_8 = 1;
        }
        this.n = v0_8;
        return;
    }

    private void b(float p2)
    {
        if (this.d != p2) {
            this.d = p2;
            this.a();
        }
        return;
    }

    private void b(android.view.animation.Interpolator p1)
    {
        this.i = p1;
        this.a();
        return;
    }

    private boolean b(CharSequence p4)
    {
        boolean v0_0 = 1;
        if (android.support.v4.view.cx.f(this.m) != 1) {
            v0_0 = 0;
        }
        boolean v0_1;
        if (!v0_0) {
            v0_1 = android.support.v4.m.m.c;
        } else {
            v0_1 = android.support.v4.m.m.d;
        }
        return v0_1.a(p4, 0, p4.length());
    }

    private int c()
    {
        return this.b;
    }

    private void c(float p2)
    {
        if (this.e != p2) {
            this.e = p2;
            this.a();
        }
        return;
    }

    private int d()
    {
        return this.c;
    }

    private void d(float p5)
    {
        this.q.left = android.support.design.widget.l.a(((float) this.o.left), ((float) this.p.left), p5, this.i);
        this.q.top = android.support.design.widget.l.a(this.s, this.t, p5, this.i);
        this.q.right = android.support.design.widget.l.a(((float) this.o.right), ((float) this.p.right), p5, this.i);
        this.q.bottom = android.support.design.widget.l.a(((float) this.o.bottom), ((float) this.p.bottom), p5, this.i);
        return;
    }

    private android.graphics.Typeface e()
    {
        return this.h.getTypeface();
    }

    private void e(float p9)
    {
        int v1_0 = 1;
        if (this.g != null) {
            float v5_4;
            int v3_1;
            if (!android.support.design.widget.l.a(p9, this.e)) {
                int v3_0 = ((float) this.o.width());
                android.graphics.Paint v0_5 = this.d;
                if (!android.support.design.widget.l.a(p9, this.d)) {
                    this.F = (p9 / this.d);
                    v5_4 = v3_0;
                    v3_1 = v0_5;
                } else {
                    this.F = 1065353216;
                    v5_4 = v3_0;
                    v3_1 = v0_5;
                }
            } else {
                int v3_2 = ((float) this.p.width());
                android.graphics.Paint v0_8 = this.e;
                this.F = 1065353216;
                v5_4 = v3_2;
                v3_1 = v0_8;
            }
            android.graphics.Paint v0_10;
            if (v5_4 <= 0) {
                v0_10 = 0;
            } else {
                if ((this.G == v3_1) && (!this.H)) {
                    v0_10 = 0;
                } else {
                    v0_10 = 1;
                }
                this.G = v3_1;
                this.H = 0;
            }
            if ((this.y == null) || (v0_10 != null)) {
                this.h.setTextSize(this.G);
                android.graphics.Paint v0_16 = android.text.TextUtils.ellipsize(this.g, this.h, v5_4, android.text.TextUtils$TruncateAt.END);
                if ((this.y == null) || (!this.y.equals(v0_16))) {
                    this.y = v0_16;
                }
                android.graphics.Paint v0_19;
                int v3_9 = this.y;
                if (android.support.v4.view.cx.f(this.m) != 1) {
                    v0_19 = 0;
                } else {
                    v0_19 = 1;
                }
                android.graphics.Paint v0_20;
                if (v0_19 == null) {
                    v0_20 = android.support.v4.m.m.c;
                } else {
                    v0_20 = android.support.v4.m.m.d;
                }
                this.z = v0_20.a(v3_9, 0, v3_9.length());
            }
            if ((!android.support.design.widget.l.j) || (this.F == 1065353216)) {
                v1_0 = 0;
            }
            this.A = v1_0;
            if ((this.A) && ((this.B == null) && ((!this.o.isEmpty()) && (!android.text.TextUtils.isEmpty(this.y))))) {
                this.h.setTextSize(this.d);
                this.h.setColor(this.r);
                this.D = this.h.ascent();
                this.E = this.h.descent();
                android.graphics.Paint v0_39 = Math.round(this.h.measureText(this.y, 0, this.y.length()));
                float v5_6 = Math.round((this.E - this.D));
                if ((v0_39 > null) || (v5_6 > 0)) {
                    this.B = android.graphics.Bitmap.createBitmap(v0_39, v5_6, android.graphics.Bitmap$Config.ARGB_8888);
                    new android.graphics.Canvas(this.B).drawText(this.y, 0, this.y.length(), 0, (((float) v5_6) - this.h.descent()), this.h);
                    if (this.C == null) {
                        this.C = new android.graphics.Paint(3);
                    }
                }
            }
            android.support.v4.view.cx.b(this.m);
        }
        return;
    }

    private float f()
    {
        return this.a;
    }

    private float g()
    {
        return this.e;
    }

    private float h()
    {
        return this.d;
    }

    private void i()
    {
        int v1_0 = 1;
        float v7 = this.a;
        this.q.left = android.support.design.widget.l.a(((float) this.o.left), ((float) this.p.left), v7, this.i);
        this.q.top = android.support.design.widget.l.a(this.s, this.t, v7, this.i);
        this.q.right = android.support.design.widget.l.a(((float) this.o.right), ((float) this.p.right), v7, this.i);
        this.q.bottom = android.support.design.widget.l.a(((float) this.o.bottom), ((float) this.p.bottom), v7, this.i);
        this.w = android.support.design.widget.l.a(this.u, this.v, v7, this.i);
        this.x = android.support.design.widget.l.a(this.s, this.t, v7, this.i);
        float v5_13 = android.support.design.widget.l.a(this.d, this.e, v7, this.I);
        if (this.g != null) {
            float v5_15;
            int v3_18;
            if (!android.support.design.widget.l.a(v5_13, this.e)) {
                int v3_17 = ((float) this.o.width());
                android.graphics.Paint v0_14 = this.d;
                if (!android.support.design.widget.l.a(v5_13, this.d)) {
                    this.F = (v5_13 / this.d);
                    v5_15 = v3_17;
                    v3_18 = v0_14;
                } else {
                    this.F = 1065353216;
                    v5_15 = v3_17;
                    v3_18 = v0_14;
                }
            } else {
                int v3_19 = ((float) this.p.width());
                android.graphics.Paint v0_17 = this.e;
                this.F = 1065353216;
                v5_15 = v3_19;
                v3_18 = v0_17;
            }
            android.graphics.Paint v0_19;
            if (v5_15 <= 0) {
                v0_19 = 0;
            } else {
                if ((this.G == v3_18) && (!this.H)) {
                    v0_19 = 0;
                } else {
                    v0_19 = 1;
                }
                this.G = v3_18;
                this.H = 0;
            }
            if ((this.y == null) || (v0_19 != null)) {
                this.h.setTextSize(this.G);
                android.graphics.Paint v0_25 = android.text.TextUtils.ellipsize(this.g, this.h, v5_15, android.text.TextUtils$TruncateAt.END);
                if ((this.y == null) || (!this.y.equals(v0_25))) {
                    this.y = v0_25;
                }
                android.graphics.Paint v0_28;
                int v3_26 = this.y;
                if (android.support.v4.view.cx.f(this.m) != 1) {
                    v0_28 = 0;
                } else {
                    v0_28 = 1;
                }
                android.graphics.Paint v0_29;
                if (v0_28 == null) {
                    v0_29 = android.support.v4.m.m.c;
                } else {
                    v0_29 = android.support.v4.m.m.d;
                }
                this.z = v0_29.a(v3_26, 0, v3_26.length());
            }
            if ((!android.support.design.widget.l.j) || (this.F == 1065353216)) {
                v1_0 = 0;
            }
            this.A = v1_0;
            if ((this.A) && ((this.B == null) && ((!this.o.isEmpty()) && (!android.text.TextUtils.isEmpty(this.y))))) {
                this.h.setTextSize(this.d);
                this.h.setColor(this.r);
                this.D = this.h.ascent();
                this.E = this.h.descent();
                android.graphics.Paint v0_48 = Math.round(this.h.measureText(this.y, 0, this.y.length()));
                float v5_17 = Math.round((this.E - this.D));
                if ((v0_48 > null) || (v5_17 > 0)) {
                    this.B = android.graphics.Bitmap.createBitmap(v0_48, v5_17, android.graphics.Bitmap$Config.ARGB_8888);
                    new android.graphics.Canvas(this.B).drawText(this.y, 0, this.y.length(), 0, (((float) v5_17) - this.h.descent()), this.h);
                    if (this.C == null) {
                        this.C = new android.graphics.Paint(3);
                    }
                }
            }
            android.support.v4.view.cx.b(this.m);
        }
        if (this.f == this.r) {
            this.h.setColor(this.f);
        } else {
            int v1_12 = this.r;
            int v2_1 = this.f;
            int v3_32 = (1065353216 - v7);
            this.h.setColor(android.graphics.Color.argb(((int) ((((float) android.graphics.Color.alpha(v1_12)) * v3_32) + (((float) android.graphics.Color.alpha(v2_1)) * v7))), ((int) ((((float) android.graphics.Color.red(v1_12)) * v3_32) + (((float) android.graphics.Color.red(v2_1)) * v7))), ((int) ((((float) android.graphics.Color.green(v1_12)) * v3_32) + (((float) android.graphics.Color.green(v2_1)) * v7))), ((int) ((((float) android.graphics.Color.blue(v1_12)) * v3_32) + (((float) android.graphics.Color.blue(v2_1)) * v7)))));
        }
        android.support.v4.view.cx.b(this.m);
        return;
    }

    private void j()
    {
        float v0_2;
        float v3_0 = 1;
        float v1_0 = 0;
        this.h.setTextSize(this.e);
        if (this.y == null) {
            v0_2 = 0;
        } else {
            v0_2 = this.h.measureText(this.y, 0, this.y.length());
        }
        float v2_3;
        if (!this.z) {
            v2_3 = 0;
        } else {
            v2_3 = 1;
        }
        float v2_4 = android.support.v4.view.v.a(this.c, v2_3);
        switch ((v2_4 & 112)) {
            case 48:
                this.t = (((float) this.p.top) - this.h.ascent());
                break;
            case 80:
                this.t = ((float) this.p.bottom);
                break;
            default:
                this.t = ((((this.h.descent() - this.h.ascent()) / 1073741824) - this.h.descent()) + ((float) this.p.centerY()));
        }
        switch ((v2_4 & 7)) {
            case 1:
                this.v = (((float) this.p.centerX()) - (v0_2 / 1073741824));
                break;
            case 5:
                this.v = (((float) this.p.right) - v0_2);
                break;
            default:
                this.v = ((float) this.p.left);
        }
        this.h.setTextSize(this.d);
        if (this.y != null) {
            v1_0 = this.h.measureText(this.y, 0, this.y.length());
        }
        if (!this.z) {
            v3_0 = 0;
        }
        float v0_14 = android.support.v4.view.v.a(this.b, v3_0);
        switch ((v0_14 & 112)) {
            case 48:
                this.s = (((float) this.o.top) - this.h.ascent());
                break;
            case 80:
                this.s = ((float) this.o.bottom);
                break;
            default:
                this.s = ((((this.h.descent() - this.h.ascent()) / 1073741824) - this.h.descent()) + ((float) this.o.centerY()));
        }
        switch ((v0_14 & 7)) {
            case 1:
                this.u = (((float) this.o.centerX()) - (v1_0 / 1073741824));
                break;
            case 5:
                this.u = (((float) this.o.right) - v1_0);
                break;
            default:
                this.u = ((float) this.o.left);
        }
        this.m();
        return;
    }

    private void k()
    {
        if ((this.B == null) && ((!this.o.isEmpty()) && (!android.text.TextUtils.isEmpty(this.y)))) {
            this.h.setTextSize(this.d);
            this.h.setColor(this.r);
            this.D = this.h.ascent();
            this.E = this.h.descent();
            android.graphics.Paint v0_13 = Math.round(this.h.measureText(this.y, 0, this.y.length()));
            float v5_0 = Math.round((this.E - this.D));
            if ((v0_13 > null) || (v5_0 > 0)) {
                this.B = android.graphics.Bitmap.createBitmap(v0_13, v5_0, android.graphics.Bitmap$Config.ARGB_8888);
                new android.graphics.Canvas(this.B).drawText(this.y, 0, this.y.length(), 0, (((float) v5_0) - this.h.descent()), this.h);
                if (this.C == null) {
                    this.C = new android.graphics.Paint(3);
                }
            }
        }
        return;
    }

    private CharSequence l()
    {
        return this.g;
    }

    private void m()
    {
        if (this.B != null) {
            this.B.recycle();
            this.B = 0;
        }
        return;
    }

    private int n()
    {
        return this.r;
    }

    private int o()
    {
        return this.f;
    }

    public final void a()
    {
        float v3_0 = 1;
        float v1_0 = 0;
        if ((this.m.getHeight() > 0) && (this.m.getWidth() > 0)) {
            float v0_6;
            this.h.setTextSize(this.e);
            if (this.y == null) {
                v0_6 = 0;
            } else {
                v0_6 = this.h.measureText(this.y, 0, this.y.length());
            }
            float v2_3;
            if (!this.z) {
                v2_3 = 0;
            } else {
                v2_3 = 1;
            }
            float v2_4 = android.support.v4.view.v.a(this.c, v2_3);
            switch ((v2_4 & 112)) {
                case 48:
                    this.t = (((float) this.p.top) - this.h.ascent());
                    break;
                case 80:
                    this.t = ((float) this.p.bottom);
                    break;
                default:
                    this.t = ((((this.h.descent() - this.h.ascent()) / 1073741824) - this.h.descent()) + ((float) this.p.centerY()));
            }
            switch ((v2_4 & 7)) {
                case 1:
                    this.v = (((float) this.p.centerX()) - (v0_6 / 1073741824));
                    break;
                case 5:
                    this.v = (((float) this.p.right) - v0_6);
                    break;
                default:
                    this.v = ((float) this.p.left);
            }
            this.h.setTextSize(this.d);
            if (this.y != null) {
                v1_0 = this.h.measureText(this.y, 0, this.y.length());
            }
            if (!this.z) {
                v3_0 = 0;
            }
            float v0_18 = android.support.v4.view.v.a(this.b, v3_0);
            switch ((v0_18 & 112)) {
                case 48:
                    this.s = (((float) this.o.top) - this.h.ascent());
                    break;
                case 80:
                    this.s = ((float) this.o.bottom);
                    break;
                default:
                    this.s = ((((this.h.descent() - this.h.ascent()) / 1073741824) - this.h.descent()) + ((float) this.o.centerY()));
            }
            switch ((v0_18 & 7)) {
                case 1:
                    this.u = (((float) this.o.centerX()) - (v1_0 / 1073741824));
                    break;
                case 5:
                    this.u = (((float) this.o.right) - v1_0);
                    break;
                default:
                    this.u = ((float) this.o.left);
            }
            this.m();
            this.i();
        }
        return;
    }

    final void a(float p4)
    {
        if (p4 >= 0) {
            if (p4 > 1065353216) {
                p4 = 1065353216;
            }
        } else {
            p4 = 0;
        }
        if (p4 != this.a) {
            this.a = p4;
            this.i();
        }
        return;
    }

    final void a(int p2)
    {
        if (this.f != p2) {
            this.f = p2;
            this.a();
        }
        return;
    }

    final void a(int p2, int p3, int p4, int p5)
    {
        if (!android.support.design.widget.l.a(this.o, p2, p3, p4, p5)) {
            this.o.set(p2, p3, p4, p5);
            this.H = 1;
            this.b();
        }
        return;
    }

    public final void a(android.graphics.Canvas p9)
    {
        int v7 = p9.save();
        if ((this.y != null) && (this.n)) {
            android.graphics.Canvas v0_4;
            float v4 = this.w;
            float v5 = this.x;
            if ((!this.A) || (this.B == null)) {
                v0_4 = 0;
            } else {
                v0_4 = 1;
            }
            CharSequence v1_2;
            this.h.setTextSize(this.G);
            if (v0_4 == null) {
                this.h.ascent();
                v1_2 = 0;
                this.h.descent();
            } else {
                v1_2 = (this.D * this.F);
            }
            if (v0_4 != null) {
                v5 += v1_2;
            }
            if (this.F != 1065353216) {
                p9.scale(this.F, this.F, v4, v5);
            }
            if (v0_4 == null) {
                p9.drawText(this.y, 0, this.y.length(), v4, v5, this.h);
            } else {
                p9.drawBitmap(this.B, v4, v5, this.C);
            }
        }
        p9.restoreToCount(v7);
        return;
    }

    final void a(android.graphics.Typeface p2)
    {
        if (p2 == null) {
            p2 = android.graphics.Typeface.DEFAULT;
        }
        if (this.h.getTypeface() != p2) {
            this.h.setTypeface(p2);
            this.a();
        }
        return;
    }

    final void a(android.view.animation.Interpolator p1)
    {
        this.I = p1;
        this.a();
        return;
    }

    final void a(CharSequence p2)
    {
        if ((p2 == null) || (!p2.equals(this.g))) {
            this.g = p2;
            this.y = 0;
            this.m();
            this.a();
        }
        return;
    }

    final void b(int p2)
    {
        if (this.r != p2) {
            this.r = p2;
            this.a();
        }
        return;
    }

    final void b(int p2, int p3, int p4, int p5)
    {
        if (!android.support.design.widget.l.a(this.p, p2, p3, p4, p5)) {
            this.p.set(p2, p3, p4, p5);
            this.H = 1;
            this.b();
        }
        return;
    }

    final void c(int p2)
    {
        if (this.b != p2) {
            this.b = p2;
            this.a();
        }
        return;
    }

    final void d(int p2)
    {
        if (this.c != p2) {
            this.c = p2;
            this.a();
        }
        return;
    }

    final void e(int p4)
    {
        android.content.res.TypedArray v0_2 = this.m.getContext().obtainStyledAttributes(p4, android.support.design.n.TextAppearance);
        if (v0_2.hasValue(android.support.design.n.TextAppearance_android_textColor)) {
            this.f = v0_2.getColor(android.support.design.n.TextAppearance_android_textColor, this.f);
        }
        if (v0_2.hasValue(android.support.design.n.TextAppearance_android_textSize)) {
            this.e = ((float) v0_2.getDimensionPixelSize(android.support.design.n.TextAppearance_android_textSize, ((int) this.e)));
        }
        v0_2.recycle();
        this.a();
        return;
    }

    final void f(int p4)
    {
        android.content.res.TypedArray v0_2 = this.m.getContext().obtainStyledAttributes(p4, android.support.design.n.TextAppearance);
        if (v0_2.hasValue(android.support.design.n.TextAppearance_android_textColor)) {
            this.r = v0_2.getColor(android.support.design.n.TextAppearance_android_textColor, this.r);
        }
        if (v0_2.hasValue(android.support.design.n.TextAppearance_android_textSize)) {
            this.d = ((float) v0_2.getDimensionPixelSize(android.support.design.n.TextAppearance_android_textSize, ((int) this.d)));
        }
        v0_2.recycle();
        this.a();
        return;
    }
}
