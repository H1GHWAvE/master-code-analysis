package android.support.design.widget;
 class ai extends android.support.design.widget.ad {
    private boolean h;

    ai(android.view.View p1, android.support.design.widget.as p2)
    {
        this(p1, p2);
        return;
    }

    static synthetic boolean a(android.support.design.widget.ai p0, boolean p1)
    {
        p0.h = p1;
        return p1;
    }

    final void b()
    {
        if ((!this.h) && (this.f.getVisibility() == 0)) {
            if ((android.support.v4.view.cx.B(this.f)) && (!this.f.isInEditMode())) {
                this.f.animate().scaleX(0).scaleY(0).alpha(0).setDuration(200).setInterpolator(android.support.design.widget.a.b).setListener(new android.support.design.widget.aj(this));
            } else {
                this.f.setVisibility(8);
            }
        }
        return;
    }

    final void c()
    {
        if (this.f.getVisibility() != 0) {
            if ((!android.support.v4.view.cx.B(this.f)) || (this.f.isInEditMode())) {
                this.f.setVisibility(0);
                this.f.setAlpha(1065353216);
                this.f.setScaleY(1065353216);
                this.f.setScaleX(1065353216);
            } else {
                this.f.setAlpha(0);
                this.f.setScaleY(0);
                this.f.setScaleX(0);
                this.f.animate().scaleX(1065353216).scaleY(1065353216).alpha(1065353216).setDuration(200).setInterpolator(android.support.design.widget.a.b).setListener(new android.support.design.widget.ak(this));
            }
        }
        return;
    }
}
