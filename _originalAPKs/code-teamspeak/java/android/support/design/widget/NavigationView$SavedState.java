package android.support.design.widget;
public class NavigationView$SavedState extends android.view.View$BaseSavedState {
    public static final android.os.Parcelable$Creator CREATOR;
    public android.os.Bundle a;

    static NavigationView$SavedState()
    {
        android.support.design.widget.NavigationView$SavedState.CREATOR = new android.support.design.widget.aq();
        return;
    }

    public NavigationView$SavedState(android.os.Parcel p2)
    {
        this(p2);
        this.a = p2.readBundle();
        return;
    }

    public NavigationView$SavedState(android.os.Parcelable p1)
    {
        this(p1);
        return;
    }

    public void writeToParcel(android.os.Parcel p2, int p3)
    {
        super.writeToParcel(p2, p3);
        p2.writeBundle(this.a);
        return;
    }
}
