package android.support.design.widget;
public final class AppBarLayout extends android.widget.LinearLayout {
    private static final int d = 0;
    private static final int e = 1;
    private static final int f = 2;
    private static final int g = 4;
    private static final int h = 255;
    boolean a;
    int b;
    final java.util.List c;
    private int i;
    private int j;
    private int k;
    private float l;
    private android.support.v4.view.gh m;

    private AppBarLayout(android.content.Context p2)
    {
        this(p2, 0);
        return;
    }

    private AppBarLayout(android.content.Context p5, byte p6)
    {
        this(p5, 0);
        this.i = -1;
        this.j = -1;
        this.k = -1;
        this.b = 0;
        this.setOrientation(1);
        android.support.design.widget.c v0_3 = p5.obtainStyledAttributes(0, android.support.design.n.AppBarLayout, 0, android.support.design.m.Widget_Design_AppBarLayout);
        this.l = ((float) v0_3.getDimensionPixelSize(android.support.design.n.AppBarLayout_elevation, 0));
        this.setBackgroundDrawable(v0_3.getDrawable(android.support.design.n.AppBarLayout_android_background));
        if (v0_3.hasValue(android.support.design.n.AppBarLayout_expanded)) {
            this.setExpanded(v0_3.getBoolean(android.support.design.n.AppBarLayout_expanded, 0));
        }
        v0_3.recycle();
        android.support.design.widget.dh.a(this);
        this.c = new java.util.ArrayList();
        android.support.v4.view.cx.f(this, this.l);
        android.support.v4.view.cx.a(this, new android.support.design.widget.c(this));
        return;
    }

    private static android.support.design.widget.g a()
    {
        return new android.support.design.widget.g();
    }

    private android.support.design.widget.g a(android.util.AttributeSet p3)
    {
        return new android.support.design.widget.g(this.getContext(), p3);
    }

    private static android.support.design.widget.g a(android.view.ViewGroup$LayoutParams p1)
    {
        android.support.design.widget.g v0_3;
        if (!(p1 instanceof android.widget.LinearLayout$LayoutParams)) {
            if (!(p1 instanceof android.view.ViewGroup$MarginLayoutParams)) {
                v0_3 = new android.support.design.widget.g(p1);
            } else {
                v0_3 = new android.support.design.widget.g(((android.view.ViewGroup$MarginLayoutParams) p1));
            }
        } else {
            v0_3 = new android.support.design.widget.g(((android.widget.LinearLayout$LayoutParams) p1));
        }
        return v0_3;
    }

    static synthetic java.util.List a(android.support.design.widget.AppBarLayout p1)
    {
        return p1.c;
    }

    static synthetic void a(android.support.design.widget.AppBarLayout p0, android.support.v4.view.gh p1)
    {
        p0.setWindowInsets(p1);
        return;
    }

    private void a(android.support.design.widget.i p2)
    {
        if ((p2 != null) && (!this.c.contains(p2))) {
            this.c.add(p2);
        }
        return;
    }

    private void a(boolean p3, boolean p4)
    {
        int v1;
        if (!p3) {
            v1 = 2;
        } else {
            v1 = 1;
        }
        int v0_2;
        if (!p4) {
            v0_2 = 0;
        } else {
            v0_2 = 4;
        }
        this.b = (v0_2 | v1);
        this.requestLayout();
        return;
    }

    private void b(android.support.design.widget.i p2)
    {
        if (p2 != null) {
            this.c.remove(p2);
        }
        return;
    }

    private boolean b()
    {
        return this.a;
    }

    private boolean c()
    {
        int v0_1;
        if (this.getTotalScrollRange() == 0) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    private void d()
    {
        this.b = 0;
        return;
    }

    private void setWindowInsets(android.support.v4.view.gh p4)
    {
        this.i = -1;
        this.m = p4;
        int v0_1 = 0;
        int v1 = this.getChildCount();
        while (v0_1 < v1) {
            p4 = android.support.v4.view.cx.b(this.getChildAt(v0_1), p4);
            if (p4.g()) {
                break;
            }
            v0_1++;
        }
        return;
    }

    protected final boolean checkLayoutParams(android.view.ViewGroup$LayoutParams p2)
    {
        return (p2 instanceof android.support.design.widget.g);
    }

    protected final synthetic android.view.ViewGroup$LayoutParams generateDefaultLayoutParams()
    {
        return new android.support.design.widget.g();
    }

    protected final synthetic android.widget.LinearLayout$LayoutParams generateDefaultLayoutParams()
    {
        return new android.support.design.widget.g();
    }

    public final synthetic android.view.ViewGroup$LayoutParams generateLayoutParams(android.util.AttributeSet p2)
    {
        return this.a(p2);
    }

    protected final synthetic android.view.ViewGroup$LayoutParams generateLayoutParams(android.view.ViewGroup$LayoutParams p2)
    {
        return android.support.design.widget.AppBarLayout.a(p2);
    }

    public final synthetic android.widget.LinearLayout$LayoutParams generateLayoutParams(android.util.AttributeSet p2)
    {
        return this.a(p2);
    }

    protected final synthetic android.widget.LinearLayout$LayoutParams generateLayoutParams(android.view.ViewGroup$LayoutParams p2)
    {
        return android.support.design.widget.AppBarLayout.a(p2);
    }

    final int getDownNestedPreScrollRange()
    {
        int v2_0;
        if (this.j == -1) {
            v2_0 = 0;
            int v3 = (this.getChildCount() - 1);
            while (v3 >= 0) {
                int v1_2;
                android.view.View v4 = this.getChildAt(v3);
                int v0_4 = ((android.support.design.widget.g) v4.getLayoutParams());
                if (!android.support.v4.view.cx.B(v4)) {
                    v1_2 = v4.getMeasuredHeight();
                } else {
                    v1_2 = v4.getHeight();
                }
                int v0_5;
                int v5 = v0_4.f;
                if ((v5 & 5) != 5) {
                    if (v2_0 > 0) {
                        break;
                    }
                    v0_5 = v2_0;
                } else {
                    int v0_8 = ((v0_4.bottomMargin + v0_4.topMargin) + v2_0);
                    if ((v5 & 8) == 0) {
                        v0_5 = (v0_8 + v1_2);
                    } else {
                        v0_5 = (v0_8 + android.support.v4.view.cx.o(v4));
                    }
                }
                v3--;
                v2_0 = v0_5;
            }
            this.j = v2_0;
        } else {
            v2_0 = this.j;
        }
        return v2_0;
    }

    final int getDownNestedScrollRange()
    {
        int v0_4;
        if (this.k == -1) {
            int v4 = this.getChildCount();
            int v3 = 0;
            int v2_1 = 0;
            while (v3 < v4) {
                int v1_2;
                android.view.View v5 = this.getChildAt(v3);
                int v0_2 = ((android.support.design.widget.g) v5.getLayoutParams());
                if (!android.support.v4.view.cx.B(v5)) {
                    v1_2 = v5.getMeasuredHeight();
                } else {
                    v1_2 = v5.getHeight();
                }
                int v0_3 = v0_2.f;
                if ((v0_3 & 1) == 0) {
                    break;
                }
                v2_1 += (v1_2 + (v0_2.topMargin + v0_2.bottomMargin));
                if ((v0_3 & 2) == 0) {
                    v3++;
                } else {
                    v0_4 = (v2_1 - android.support.v4.view.cx.o(v5));
                    return v0_4;
                }
            }
            this.k = v2_1;
            v0_4 = v2_1;
        } else {
            v0_4 = this.k;
        }
        return v0_4;
    }

    final int getMinimumHeightForVisibleOverlappingContent()
    {
        int v0_1;
        int v1_0 = 0;
        if (this.m == null) {
            v0_1 = 0;
        } else {
            v0_1 = this.m.b();
        }
        int v2_0 = android.support.v4.view.cx.o(this);
        if (v2_0 == 0) {
            int v2_1 = this.getChildCount();
            if (v2_1 > 0) {
                v1_0 = ((android.support.v4.view.cx.o(this.getChildAt((v2_1 - 1))) * 2) + v0_1);
            }
        } else {
            v1_0 = ((v2_0 * 2) + v0_1);
        }
        return v1_0;
    }

    final int getPendingAction()
    {
        return this.b;
    }

    public final float getTargetElevation()
    {
        return this.l;
    }

    public final int getTotalScrollRange()
    {
        int v0_9;
        if (this.i == -1) {
            int v5 = this.getChildCount();
            int v4 = 0;
            int v2 = 0;
            while (v4 < v5) {
                int v1_2;
                android.view.View v6 = this.getChildAt(v4);
                int v0_2 = ((android.support.design.widget.g) v6.getLayoutParams());
                if (!android.support.v4.view.cx.B(v6)) {
                    v1_2 = v6.getMeasuredHeight();
                } else {
                    v1_2 = v6.getHeight();
                }
                int v7 = v0_2.f;
                if ((v7 & 1) == 0) {
                    break;
                }
                v2 += (v0_2.bottomMargin + (v1_2 + v0_2.topMargin));
                if ((v7 & 2) == 0) {
                    v4++;
                } else {
                    int v0_3 = (v2 - android.support.v4.view.cx.o(v6));
                }
                int v1_5;
                if (this.m == null) {
                    v1_5 = 0;
                } else {
                    v1_5 = this.m.b();
                }
                v0_9 = (v0_3 - v1_5);
                this.i = v0_9;
            }
            v0_3 = v2;
        } else {
            v0_9 = this.i;
        }
        return v0_9;
    }

    final int getUpNestedPreScrollRange()
    {
        return this.getTotalScrollRange();
    }

    protected final void onLayout(boolean p4, int p5, int p6, int p7, int p8)
    {
        this = super.onLayout(p4, p5, p6, p7, p8);
        this.i = -1;
        this.j = -1;
        this.j = -1;
        this.a = 0;
        int v2 = this.getChildCount();
        int v1_1 = 0;
        while (v1_1 < v2) {
            if (((android.support.design.widget.g) this.getChildAt(v1_1).getLayoutParams()).g == null) {
                v1_1++;
            } else {
                this.a = 1;
                break;
            }
        }
        return;
    }

    public final void setExpanded(boolean p4)
    {
        int v1;
        boolean v2 = android.support.v4.view.cx.B(this);
        if (!p4) {
            v1 = 2;
        } else {
            v1 = 1;
        }
        int v0_2;
        if (!v2) {
            v0_2 = 0;
        } else {
            v0_2 = 4;
        }
        this.b = (v0_2 | v1);
        this.requestLayout();
        return;
    }

    public final void setOrientation(int p3)
    {
        if (p3 == 1) {
            super.setOrientation(p3);
            return;
        } else {
            throw new IllegalArgumentException("AppBarLayout is always vertical and does not support horizontal orientation");
        }
    }

    public final void setTargetElevation(float p1)
    {
        this.l = p1;
        return;
    }
}
