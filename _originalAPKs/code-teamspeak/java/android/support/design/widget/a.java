package android.support.design.widget;
final class a {
    static final android.view.animation.Interpolator a;
    static final android.view.animation.Interpolator b;
    static final android.view.animation.Interpolator c;

    static a()
    {
        android.support.design.widget.a.a = new android.view.animation.LinearInterpolator();
        android.support.design.widget.a.b = new android.support.v4.view.b.b();
        android.support.design.widget.a.c = new android.view.animation.DecelerateInterpolator();
        return;
    }

    a()
    {
        return;
    }

    static float a(float p1, float p2, float p3)
    {
        return (((p2 - p1) * p3) + p1);
    }

    static int a(int p1, int p2, float p3)
    {
        return (Math.round((((float) (p2 - p1)) * p3)) + p1);
    }
}
