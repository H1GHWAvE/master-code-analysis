package android.support.design.widget;
public final class Snackbar {
    public static final int a = 254;
    public static final int b = 255;
    public static final int c = 0;
    private static final int f = 250;
    private static final int g = 180;
    private static final android.os.Handler h = None;
    private static final int i = 0;
    private static final int j = 1;
    final android.view.ViewGroup d;
    final android.support.design.widget.Snackbar$SnackbarLayout e;
    private final android.content.Context k;
    private int l;
    private android.support.design.widget.bd m;
    private final android.support.design.widget.bj n;

    static Snackbar()
    {
        android.support.design.widget.Snackbar.h = new android.os.Handler(android.os.Looper.getMainLooper(), new android.support.design.widget.at());
        return;
    }

    private Snackbar(android.view.ViewGroup p5)
    {
        this.n = new android.support.design.widget.av(this);
        this.d = p5;
        this.k = p5.getContext();
        this.e = ((android.support.design.widget.Snackbar$SnackbarLayout) android.view.LayoutInflater.from(this.k).inflate(android.support.design.k.design_layout_snackbar, this.d, 0));
        return;
    }

    private android.support.design.widget.Snackbar a(int p2)
    {
        this.e.getActionView().setTextColor(p2);
        return this;
    }

    private android.support.design.widget.Snackbar a(int p4, android.view.View$OnClickListener p5)
    {
        android.support.design.widget.au v0_1 = this.k.getText(p4);
        android.widget.Button v1_1 = this.e.getActionView();
        if ((!android.text.TextUtils.isEmpty(v0_1)) && (p5 != null)) {
            v1_1.setVisibility(0);
            v1_1.setText(v0_1);
            v1_1.setOnClickListener(new android.support.design.widget.au(this, p5));
        } else {
            v1_1.setVisibility(8);
            v1_1.setOnClickListener(0);
        }
        return this;
    }

    private android.support.design.widget.Snackbar a(android.content.res.ColorStateList p2)
    {
        this.e.getActionView().setTextColor(p2);
        return this;
    }

    private android.support.design.widget.Snackbar a(android.support.design.widget.bd p1)
    {
        this.m = p1;
        return this;
    }

    private static android.support.design.widget.Snackbar a(android.view.View p3, int p4, int p5)
    {
        CharSequence v0_1 = p3.getResources().getText(p4);
        android.support.design.widget.Snackbar v1_1 = new android.support.design.widget.Snackbar(android.support.design.widget.Snackbar.a(p3));
        v1_1.a(v0_1);
        v1_1.l = p5;
        return v1_1;
    }

    private static android.support.design.widget.Snackbar a(android.view.View p2, CharSequence p3, int p4)
    {
        android.support.design.widget.Snackbar v0_1 = new android.support.design.widget.Snackbar(android.support.design.widget.Snackbar.a(p2));
        v0_1.a(p3);
        v0_1.l = p4;
        return v0_1;
    }

    private android.support.design.widget.Snackbar a(CharSequence p2)
    {
        this.e.getMessageView().setText(p2);
        return this;
    }

    private android.support.design.widget.Snackbar a(CharSequence p3, android.view.View$OnClickListener p4)
    {
        android.widget.Button v0_1 = this.e.getActionView();
        if ((!android.text.TextUtils.isEmpty(p3)) && (p4 != null)) {
            v0_1.setVisibility(0);
            v0_1.setText(p3);
            v0_1.setOnClickListener(new android.support.design.widget.au(this, p4));
        } else {
            v0_1.setVisibility(8);
            v0_1.setOnClickListener(0);
        }
        return this;
    }

    static synthetic android.support.design.widget.bj a(android.support.design.widget.Snackbar p1)
    {
        return p1.n;
    }

    private static android.view.ViewGroup a(android.view.View p4)
    {
        android.view.ViewGroup v1_0 = 0;
        android.view.ViewGroup v0_0 = p4;
        while (!(v0_0 instanceof android.support.design.widget.CoordinatorLayout)) {
            if ((v0_0 instanceof android.widget.FrameLayout)) {
                if (v0_0.getId() != 16908290) {
                    v1_0 = ((android.view.ViewGroup) v0_0);
                } else {
                    android.view.ViewGroup v0_2 = ((android.view.ViewGroup) v0_0);
                    return v0_2;
                }
            }
            if (v0_0 != null) {
                android.view.ViewGroup v0_1 = v0_0.getParent();
                if (!(v0_1 instanceof android.view.View)) {
                    v0_0 = 0;
                } else {
                    v0_0 = ((android.view.View) v0_1);
                }
            }
            if (v0_0 == null) {
                v0_2 = v1_0;
            }
            return v0_2;
        }
        v0_2 = ((android.view.ViewGroup) v0_0);
        return v0_2;
    }

    static synthetic void a(android.support.design.widget.Snackbar p0, int p1)
    {
        p0.d(p1);
        return;
    }

    private android.support.design.widget.Snackbar b(int p2)
    {
        return this.a(this.k.getText(p2));
    }

    static synthetic void b(android.support.design.widget.Snackbar p0)
    {
        p0.a();
        return;
    }

    static synthetic android.os.Handler c()
    {
        return android.support.design.widget.Snackbar.h;
    }

    static synthetic android.support.design.widget.Snackbar$SnackbarLayout c(android.support.design.widget.Snackbar p1)
    {
        return p1.e;
    }

    private android.support.design.widget.Snackbar c(int p1)
    {
        this.l = p1;
        return this;
    }

    private int d()
    {
        return this.l;
    }

    static synthetic android.support.design.widget.bd d(android.support.design.widget.Snackbar p1)
    {
        return p1.m;
    }

    private void d(int p5)
    {
        android.support.design.widget.bk v0_0 = android.support.design.widget.bh.a();
        boolean v1_0 = this.n;
        try {
            if (!v0_0.d(v1_0)) {
                if (v0_0.e(v1_0)) {
                    android.support.design.widget.bh.a(v0_0.d, p5);
                }
            } else {
                android.support.design.widget.bh.a(v0_0.c, p5);
            }
        } catch (android.support.design.widget.bk v0_3) {
            throw v0_3;
        }
        return;
    }

    private android.view.View e()
    {
        return this.e;
    }

    private void e(int p5)
    {
        if (android.os.Build$VERSION.SDK_INT < 14) {
            android.view.animation.Animation v0_3 = android.view.animation.AnimationUtils.loadAnimation(this.e.getContext(), android.support.design.c.design_snackbar_out);
            v0_3.setInterpolator(android.support.design.widget.a.b);
            v0_3.setDuration(250);
            v0_3.setAnimationListener(new android.support.design.widget.bb(this, p5));
            this.e.startAnimation(v0_3);
        } else {
            android.support.v4.view.cx.p(this.e).b(((float) this.e.getHeight())).a(android.support.design.widget.a.b).a(250).a(new android.support.design.widget.ba(this, p5)).b();
        }
        return;
    }

    static synthetic void e(android.support.design.widget.Snackbar p0)
    {
        p0.b();
        return;
    }

    private void f()
    {
        Throwable v0_0 = android.support.design.widget.bh.a();
        int v1_0 = this.l;
        int v2_0 = this.n;
        try {
            if (!v0_0.d(v2_0)) {
                if (!v0_0.e(v2_0)) {
                    v0_0.d = new android.support.design.widget.bk(v1_0, v2_0);
                } else {
                    v0_0.d.b = v1_0;
                }
                if ((v0_0.c == null) || (!android.support.design.widget.bh.a(v0_0.c, 4))) {
                    v0_0.c = 0;
                    v0_0.b();
                } else {
                }
            } else {
                v0_0.c.b = v1_0;
                v0_0.b.removeCallbacksAndMessages(v0_0.c);
                v0_0.a(v0_0.c);
            }
        } catch (Throwable v0_1) {
            throw v0_1;
        }
        return;
    }

    private void f(int p7)
    {
        if (this.e.getVisibility() != 0) {
            this.b();
        } else {
            android.support.design.widget.Snackbar$SnackbarLayout v0_6;
            android.support.design.widget.Snackbar$SnackbarLayout v0_3 = this.e.getLayoutParams();
            if (!(v0_3 instanceof android.support.design.widget.w)) {
                v0_6 = 0;
            } else {
                android.support.design.widget.Snackbar$SnackbarLayout v0_5 = ((android.support.design.widget.w) v0_3).a;
                if (!(v0_5 instanceof android.support.design.widget.SwipeDismissBehavior)) {
                } else {
                    android.support.design.widget.Snackbar$SnackbarLayout v0_8;
                    android.support.design.widget.Snackbar$SnackbarLayout v0_7 = ((android.support.design.widget.SwipeDismissBehavior) v0_5);
                    if (v0_7.h == null) {
                        v0_8 = 0;
                    } else {
                        v0_8 = v0_7.h.m;
                    }
                    if (v0_8 == null) {
                        v0_6 = 0;
                    } else {
                        v0_6 = 1;
                    }
                }
            }
            if (v0_6 == null) {
                if (android.os.Build$VERSION.SDK_INT < 14) {
                    android.support.design.widget.Snackbar$SnackbarLayout v0_13 = android.view.animation.AnimationUtils.loadAnimation(this.e.getContext(), android.support.design.c.design_snackbar_out);
                    v0_13.setInterpolator(android.support.design.widget.a.b);
                    v0_13.setDuration(250);
                    v0_13.setAnimationListener(new android.support.design.widget.bb(this, p7));
                    this.e.startAnimation(v0_13);
                } else {
                    android.support.v4.view.cx.p(this.e).b(((float) this.e.getHeight())).a(android.support.design.widget.a.b).a(250).a(new android.support.design.widget.ba(this, p7)).b();
                }
            }
        }
        return;
    }

    private void g()
    {
        this.d(3);
        return;
    }

    private boolean h()
    {
        return this.e.isShown();
    }

    private void i()
    {
        if (this.e.getParent() == null) {
            android.support.design.widget.Snackbar$SnackbarLayout v0_3 = this.e.getLayoutParams();
            if ((v0_3 instanceof android.support.design.widget.w)) {
                android.support.design.widget.ax v1_2 = new android.support.design.widget.bc(this);
                v1_2.k = android.support.design.widget.SwipeDismissBehavior.a(1036831949);
                v1_2.l = android.support.design.widget.SwipeDismissBehavior.a(1058642330);
                v1_2.j = 0;
                v1_2.i = new android.support.design.widget.aw(this);
                ((android.support.design.widget.w) v0_3).a(v1_2);
            }
            this.d.addView(this.e);
        }
        if (!android.support.v4.view.cx.B(this.e)) {
            this.e.setOnLayoutChangeListener(new android.support.design.widget.ax(this));
        } else {
            this.a();
        }
        return;
    }

    private boolean j()
    {
        int v0_4;
        int v0_1 = this.e.getLayoutParams();
        if (!(v0_1 instanceof android.support.design.widget.w)) {
            v0_4 = 0;
        } else {
            int v0_3 = ((android.support.design.widget.w) v0_1).a;
            if (!(v0_3 instanceof android.support.design.widget.SwipeDismissBehavior)) {
            } else {
                int v0_6;
                int v0_5 = ((android.support.design.widget.SwipeDismissBehavior) v0_3);
                if (v0_5.h == null) {
                    v0_6 = 0;
                } else {
                    v0_6 = v0_5.h.m;
                }
                if (v0_6 == 0) {
                    v0_4 = 0;
                } else {
                    v0_4 = 1;
                }
            }
        }
        return v0_4;
    }

    final void a()
    {
        if (android.os.Build$VERSION.SDK_INT < 14) {
            android.view.animation.Animation v0_3 = android.view.animation.AnimationUtils.loadAnimation(this.e.getContext(), android.support.design.c.design_snackbar_in);
            v0_3.setInterpolator(android.support.design.widget.a.b);
            v0_3.setDuration(250);
            v0_3.setAnimationListener(new android.support.design.widget.az(this));
            this.e.startAnimation(v0_3);
        } else {
            android.support.v4.view.cx.b(this.e, ((float) this.e.getHeight()));
            android.support.v4.view.cx.p(this.e).b(0).a(android.support.design.widget.a.b).a(250).a(new android.support.design.widget.ay(this)).b();
        }
        return;
    }

    final void b()
    {
        this.d.removeView(this.e);
        Throwable v0_1 = android.support.design.widget.bh.a();
        try {
            if (v0_1.d(this.n)) {
                v0_1.c = 0;
                if (v0_1.d != null) {
                    v0_1.b();
                }
            }
        } catch (Throwable v0_2) {
            throw v0_2;
        }
        return;
    }
}
