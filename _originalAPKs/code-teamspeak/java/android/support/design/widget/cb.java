package android.support.design.widget;
public final class cb implements android.support.v4.view.ev {
    private final ref.WeakReference a;
    private int b;
    private int c;

    public cb(android.support.design.widget.br p2)
    {
        this.a = new ref.WeakReference(p2);
        return;
    }

    public final void a(int p2)
    {
        this.b = this.c;
        this.c = p2;
        return;
    }

    public final void a(int p5, float p6)
    {
        int v1 = 1;
        android.support.design.widget.br v0_2 = ((android.support.design.widget.br) this.a.get());
        if (v0_2 != null) {
            if ((this.c != 1) && ((this.c != 2) || (this.b != 1))) {
                v1 = 0;
            }
            v0_2.a(p5, p6, v1);
        }
        return;
    }

    public final void b(int p4)
    {
        android.support.design.widget.br v0_2 = ((android.support.design.widget.br) this.a.get());
        if (v0_2 != null) {
            int v1_1;
            android.support.design.widget.bz v2 = v0_2.a(p4);
            if (this.c != 0) {
                v1_1 = 0;
            } else {
                v1_1 = 1;
            }
            v0_2.a(v2, v1_1);
        }
        return;
    }
}
