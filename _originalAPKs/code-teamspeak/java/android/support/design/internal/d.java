package android.support.design.internal;
final class d {
    final android.support.v7.internal.view.menu.m a;
    final int b;
    final int c;

    private d(android.support.v7.internal.view.menu.m p1, int p2, int p3)
    {
        this.a = p1;
        this.b = p2;
        this.c = p3;
        return;
    }

    public static android.support.design.internal.d a(int p2, int p3)
    {
        return new android.support.design.internal.d(0, p2, p3);
    }

    public static android.support.design.internal.d a(android.support.v7.internal.view.menu.m p2)
    {
        return new android.support.design.internal.d(p2, 0, 0);
    }

    private boolean a()
    {
        int v0_1;
        if (this.a != null) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    private int b()
    {
        return this.b;
    }

    private int c()
    {
        return this.c;
    }

    private android.support.v7.internal.view.menu.m d()
    {
        return this.a;
    }

    private boolean e()
    {
        if ((this.a == null) || ((this.a.hasSubMenu()) || (!this.a.isEnabled()))) {
            int v0_5 = 0;
        } else {
            v0_5 = 1;
        }
        return v0_5;
    }
}
