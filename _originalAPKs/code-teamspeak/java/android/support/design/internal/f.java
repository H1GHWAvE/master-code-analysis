package android.support.design.internal;
public class f extends android.widget.FrameLayout {
    private android.graphics.drawable.Drawable a;
    private android.graphics.Rect b;
    private android.graphics.Rect c;

    private f(android.content.Context p2)
    {
        this(p2, 0);
        return;
    }

    private f(android.content.Context p2, byte p3)
    {
        this(p2, 0);
        return;
    }

    public f(android.content.Context p5, char p6)
    {
        this(p5, 0, 0);
        this.c = new android.graphics.Rect();
        android.support.design.internal.g v0_3 = p5.obtainStyledAttributes(0, android.support.design.n.ScrimInsetsFrameLayout, 0, android.support.design.m.Widget_Design_ScrimInsetsFrameLayout);
        this.a = v0_3.getDrawable(android.support.design.n.ScrimInsetsFrameLayout_insetForeground);
        v0_3.recycle();
        this.setWillNotDraw(1);
        android.support.v4.view.cx.a(this, new android.support.design.internal.g(this));
        return;
    }

    static synthetic android.graphics.Rect a(android.support.design.internal.f p1)
    {
        return p1.b;
    }

    static synthetic android.graphics.Rect a(android.support.design.internal.f p0, android.graphics.Rect p1)
    {
        p0.b = p1;
        return p1;
    }

    static synthetic android.graphics.drawable.Drawable b(android.support.design.internal.f p1)
    {
        return p1.a;
    }

    public void draw(android.graphics.Canvas p9)
    {
        super.draw(p9);
        android.graphics.drawable.Drawable v0_0 = this.getWidth();
        android.graphics.Rect v1_0 = this.getHeight();
        if ((this.b != null) && (this.a != null)) {
            int v2_2 = p9.save();
            p9.translate(((float) this.getScrollX()), ((float) this.getScrollY()));
            this.c.set(0, 0, v0_0, this.b.top);
            this.a.setBounds(this.c);
            this.a.draw(p9);
            this.c.set(0, (v1_0 - this.b.bottom), v0_0, v1_0);
            this.a.setBounds(this.c);
            this.a.draw(p9);
            this.c.set(0, this.b.top, this.b.left, (v1_0 - this.b.bottom));
            this.a.setBounds(this.c);
            this.a.draw(p9);
            this.c.set((v0_0 - this.b.right), this.b.top, v0_0, (v1_0 - this.b.bottom));
            this.a.setBounds(this.c);
            this.a.draw(p9);
            p9.restoreToCount(v2_2);
        }
        return;
    }

    protected void onAttachedToWindow()
    {
        super.onAttachedToWindow();
        if (this.a != null) {
            this.a.setCallback(this);
        }
        return;
    }

    protected void onDetachedFromWindow()
    {
        super.onDetachedFromWindow();
        if (this.a != null) {
            this.a.setCallback(0);
        }
        return;
    }
}
