package android.support.design.internal;
public class NavigationMenuView extends android.widget.ListView implements android.support.v7.internal.view.menu.z {

    public NavigationMenuView(android.content.Context p2)
    {
        this(p2, 0);
        return;
    }

    public NavigationMenuView(android.content.Context p2, android.util.AttributeSet p3)
    {
        this(p2, p3, 0);
        return;
    }

    public NavigationMenuView(android.content.Context p1, android.util.AttributeSet p2, int p3)
    {
        this(p1, p2, p3);
        return;
    }

    public final void a(android.support.v7.internal.view.menu.i p1)
    {
        return;
    }

    public int getWindowAnimations()
    {
        return 0;
    }
}
