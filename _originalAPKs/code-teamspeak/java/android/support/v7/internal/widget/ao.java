package android.support.v7.internal.widget;
final class ao implements android.view.View$OnClickListener {
    final synthetic android.support.v7.internal.widget.al a;

    private ao(android.support.v7.internal.widget.al p1)
    {
        this.a = p1;
        return;
    }

    synthetic ao(android.support.v7.internal.widget.al p1, byte p2)
    {
        this(p1);
        return;
    }

    public final void onClick(android.view.View p6)
    {
        ((android.support.v7.internal.widget.ap) p6).a.f();
        int v3 = android.support.v7.internal.widget.al.a(this.a).getChildCount();
        int v2 = 0;
        while (v2 < v3) {
            int v0_7;
            android.view.View v4 = android.support.v7.internal.widget.al.a(this.a).getChildAt(v2);
            if (v4 != p6) {
                v0_7 = 0;
            } else {
                v0_7 = 1;
            }
            v4.setSelected(v0_7);
            v2++;
        }
        return;
    }
}
