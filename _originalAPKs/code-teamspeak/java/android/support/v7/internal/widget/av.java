package android.support.v7.internal.widget;
public final class av {
    public static final boolean a = False;
    private static final String b = "TintManager";
    private static final boolean c;
    private static final android.graphics.PorterDuff$Mode d;
    private static final java.util.WeakHashMap e;
    private static final android.support.v7.internal.widget.aw f;
    private static final int[] g;
    private static final int[] h;
    private static final int[] i;
    private static final int[] j;
    private static final int[] k;
    private static final int[] l;
    private final ref.WeakReference m;
    private android.util.SparseArray n;
    private android.content.res.ColorStateList o;

    static av()
    {
        int[] v0_1;
        if (android.os.Build$VERSION.SDK_INT >= 21) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        android.support.v7.internal.widget.av.a = v0_1;
        android.support.v7.internal.widget.av.d = android.graphics.PorterDuff$Mode.SRC_IN;
        android.support.v7.internal.widget.av.e = new java.util.WeakHashMap();
        android.support.v7.internal.widget.av.f = new android.support.v7.internal.widget.aw();
        int[] v0_7 = new int[3];
        v0_7[0] = android.support.v7.a.h.abc_textfield_search_default_mtrl_alpha;
        v0_7[1] = android.support.v7.a.h.abc_textfield_default_mtrl_alpha;
        v0_7[2] = android.support.v7.a.h.abc_ab_share_pack_mtrl_alpha;
        android.support.v7.internal.widget.av.g = v0_7;
        int[] v0_9 = new int[12];
        v0_9[0] = android.support.v7.a.h.abc_ic_ab_back_mtrl_am_alpha;
        v0_9[1] = android.support.v7.a.h.abc_ic_go_search_api_mtrl_alpha;
        v0_9[2] = android.support.v7.a.h.abc_ic_search_api_mtrl_alpha;
        v0_9[3] = android.support.v7.a.h.abc_ic_commit_search_api_mtrl_alpha;
        v0_9[4] = android.support.v7.a.h.abc_ic_clear_mtrl_alpha;
        v0_9[5] = android.support.v7.a.h.abc_ic_menu_share_mtrl_alpha;
        v0_9[6] = android.support.v7.a.h.abc_ic_menu_copy_mtrl_am_alpha;
        v0_9[7] = android.support.v7.a.h.abc_ic_menu_cut_mtrl_alpha;
        v0_9[8] = android.support.v7.a.h.abc_ic_menu_selectall_mtrl_alpha;
        v0_9[9] = android.support.v7.a.h.abc_ic_menu_paste_mtrl_am_alpha;
        v0_9[10] = android.support.v7.a.h.abc_ic_menu_moreoverflow_mtrl_alpha;
        v0_9[11] = android.support.v7.a.h.abc_ic_voice_search_api_mtrl_alpha;
        android.support.v7.internal.widget.av.h = v0_9;
        int[] v0_10 = new int[4];
        v0_10[0] = android.support.v7.a.h.abc_textfield_activated_mtrl_alpha;
        v0_10[1] = android.support.v7.a.h.abc_textfield_search_activated_mtrl_alpha;
        v0_10[2] = android.support.v7.a.h.abc_cab_background_top_mtrl_alpha;
        v0_10[3] = android.support.v7.a.h.abc_text_cursor_material;
        android.support.v7.internal.widget.av.i = v0_10;
        int[] v0_11 = new int[3];
        v0_11[0] = android.support.v7.a.h.abc_popup_background_mtrl_mult;
        v0_11[1] = android.support.v7.a.h.abc_cab_background_internal_bg;
        v0_11[2] = android.support.v7.a.h.abc_menu_hardkey_panel_mtrl_mult;
        android.support.v7.internal.widget.av.j = v0_11;
        int[] v0_13 = new int[10];
        v0_13[0] = android.support.v7.a.h.abc_edit_text_material;
        v0_13[1] = android.support.v7.a.h.abc_tab_indicator_material;
        v0_13[2] = android.support.v7.a.h.abc_textfield_search_material;
        v0_13[3] = android.support.v7.a.h.abc_spinner_mtrl_am_alpha;
        v0_13[4] = android.support.v7.a.h.abc_spinner_textfield_background_material;
        v0_13[5] = android.support.v7.a.h.abc_ratingbar_full_material;
        v0_13[6] = android.support.v7.a.h.abc_switch_track_mtrl_alpha;
        v0_13[7] = android.support.v7.a.h.abc_switch_thumb_material;
        v0_13[8] = android.support.v7.a.h.abc_btn_default_mtrl_shape;
        v0_13[9] = android.support.v7.a.h.abc_btn_borderless_material;
        android.support.v7.internal.widget.av.k = v0_13;
        int[] v0_14 = new int[2];
        v0_14[0] = android.support.v7.a.h.abc_btn_check_material;
        v0_14[1] = android.support.v7.a.h.abc_btn_radio_material;
        android.support.v7.internal.widget.av.l = v0_14;
        return;
    }

    private av(android.content.Context p2)
    {
        this.m = new ref.WeakReference(p2);
        return;
    }

    private static android.graphics.PorterDuffColorFilter a(int p3, android.graphics.PorterDuff$Mode p4)
    {
        android.graphics.PorterDuffColorFilter v0_2 = ((android.graphics.PorterDuffColorFilter) android.support.v7.internal.widget.av.f.a(Integer.valueOf(android.support.v7.internal.widget.aw.a(p3, p4))));
        if (v0_2 == null) {
            v0_2 = new android.graphics.PorterDuffColorFilter(p3, p4);
            android.support.v7.internal.widget.av.f.a(Integer.valueOf(android.support.v7.internal.widget.aw.a(p3, p4)), v0_2);
        }
        return v0_2;
    }

    private static android.graphics.PorterDuffColorFilter a(android.content.res.ColorStateList p1, android.graphics.PorterDuff$Mode p2, int[] p3)
    {
        if ((p1 != null) && (p2 != null)) {
            android.graphics.PorterDuffColorFilter v0_2 = android.support.v7.internal.widget.av.a(p1.getColorForState(p3, 0), p2);
        } else {
            v0_2 = 0;
        }
        return v0_2;
    }

    public static android.graphics.drawable.Drawable a(android.content.Context p2, int p3)
    {
        if ((!android.support.v7.internal.widget.av.a(android.support.v7.internal.widget.av.h, p3)) && ((!android.support.v7.internal.widget.av.a(android.support.v7.internal.widget.av.g, p3)) && ((!android.support.v7.internal.widget.av.a(android.support.v7.internal.widget.av.i, p3)) && ((!android.support.v7.internal.widget.av.a(android.support.v7.internal.widget.av.k, p3)) && ((!android.support.v7.internal.widget.av.a(android.support.v7.internal.widget.av.j, p3)) && ((!android.support.v7.internal.widget.av.a(android.support.v7.internal.widget.av.l, p3)) && (p3 != android.support.v7.a.h.abc_cab_background_top_material))))))) {
            int v0_13 = 0;
        } else {
            v0_13 = 1;
        }
        int v0_14;
        if (v0_13 == 0) {
            v0_14 = android.support.v4.c.h.a(p2, p3);
        } else {
            v0_14 = android.support.v7.internal.widget.av.a(p2).a(p3, 0);
        }
        return v0_14;
    }

    public static android.support.v7.internal.widget.av a(android.content.Context p2)
    {
        android.support.v7.internal.widget.av v0_2 = ((android.support.v7.internal.widget.av) android.support.v7.internal.widget.av.e.get(p2));
        if (v0_2 == null) {
            v0_2 = new android.support.v7.internal.widget.av(p2);
            android.support.v7.internal.widget.av.e.put(p2, v0_2);
        }
        return v0_2;
    }

    public static void a(android.view.View p5, android.support.v7.internal.widget.au p6)
    {
        android.graphics.PorterDuffColorFilter v1_0 = 0;
        android.graphics.drawable.Drawable v3 = p5.getBackground();
        if ((!p6.d) && (!p6.c)) {
            v3.clearColorFilter();
        } else {
            int v0_3;
            if (!p6.d) {
                v0_3 = 0;
            } else {
                v0_3 = p6.a;
            }
            android.graphics.PorterDuff$Mode v2_1;
            if (!p6.c) {
                v2_1 = android.support.v7.internal.widget.av.d;
            } else {
                v2_1 = p6.b;
            }
            int[] v4 = p5.getDrawableState();
            if ((v0_3 != 0) && (v2_1 != null)) {
                v1_0 = android.support.v7.internal.widget.av.a(v0_3.getColorForState(v4, 0), v2_1);
            }
            v3.setColorFilter(v1_0);
        }
        if (android.os.Build$VERSION.SDK_INT <= 10) {
            p5.invalidate();
        }
        return;
    }

    private static boolean a(int[] p4, int p5)
    {
        int v0 = 0;
        int v1 = 0;
        while (v1 < p4.length) {
            if (p4[v1] != p5) {
                v1++;
            } else {
                v0 = 1;
                break;
            }
        }
        return v0;
    }

    private android.content.res.ColorStateList b(android.content.Context p11)
    {
        if (this.o == null) {
            android.content.res.ColorStateList v0_2 = android.support.v7.internal.widget.ar.a(p11, android.support.v7.a.d.colorControlNormal);
            int v1_1 = android.support.v7.internal.widget.ar.a(p11, android.support.v7.a.d.colorControlActivated);
            int[][] v2_1 = new int[][7];
            int[] v3_1 = new int[7];
            v2_1[0] = android.support.v7.internal.widget.ar.a;
            v3_1[0] = android.support.v7.internal.widget.ar.c(p11, android.support.v7.a.d.colorControlNormal);
            v2_1[1] = android.support.v7.internal.widget.ar.b;
            v3_1[1] = v1_1;
            v2_1[2] = android.support.v7.internal.widget.ar.c;
            v3_1[2] = v1_1;
            v2_1[3] = android.support.v7.internal.widget.ar.d;
            v3_1[3] = v1_1;
            v2_1[4] = android.support.v7.internal.widget.ar.e;
            v3_1[4] = v1_1;
            v2_1[5] = android.support.v7.internal.widget.ar.f;
            v3_1[5] = v1_1;
            v2_1[6] = android.support.v7.internal.widget.ar.h;
            v3_1[6] = v0_2;
            this.o = new android.content.res.ColorStateList(v2_1, v3_1);
        }
        return this.o;
    }

    private static android.content.res.ColorStateList b(android.content.Context p9, int p10)
    {
        int[][] v0 = new int[][4];
        int[] v1_1 = new int[4];
        android.content.res.ColorStateList v2_0 = android.support.v7.internal.widget.ar.a(p9, p10);
        int[] v3_1 = android.support.v7.internal.widget.ar.a(p9, android.support.v7.a.d.colorControlHighlight);
        v0[0] = android.support.v7.internal.widget.ar.a;
        v1_1[0] = android.support.v7.internal.widget.ar.c(p9, android.support.v7.a.d.colorButtonNormal);
        v0[1] = android.support.v7.internal.widget.ar.d;
        v1_1[1] = android.support.v4.e.j.a(v3_1, v2_0);
        v0[2] = android.support.v7.internal.widget.ar.b;
        v1_1[2] = android.support.v4.e.j.a(v3_1, v2_0);
        v0[3] = android.support.v7.internal.widget.ar.h;
        v1_1[3] = v2_0;
        return new android.content.res.ColorStateList(v0, v1_1);
    }

    private android.graphics.drawable.Drawable b(int p2)
    {
        return this.a(p2, 0);
    }

    private static android.content.res.ColorStateList c(android.content.Context p6)
    {
        int[][] v0 = new int[][3];
        int[] v1_1 = new int[3];
        v0[0] = android.support.v7.internal.widget.ar.a;
        v1_1[0] = android.support.v7.internal.widget.ar.c(p6, android.support.v7.a.d.colorControlNormal);
        v0[1] = android.support.v7.internal.widget.ar.e;
        v1_1[1] = android.support.v7.internal.widget.ar.a(p6, android.support.v7.a.d.colorControlActivated);
        v0[2] = android.support.v7.internal.widget.ar.h;
        v1_1[2] = android.support.v7.internal.widget.ar.a(p6, android.support.v7.a.d.colorControlNormal);
        return new android.content.res.ColorStateList(v0, v1_1);
    }

    private static boolean c(int p1)
    {
        if ((!android.support.v7.internal.widget.av.a(android.support.v7.internal.widget.av.h, p1)) && ((!android.support.v7.internal.widget.av.a(android.support.v7.internal.widget.av.g, p1)) && ((!android.support.v7.internal.widget.av.a(android.support.v7.internal.widget.av.i, p1)) && ((!android.support.v7.internal.widget.av.a(android.support.v7.internal.widget.av.k, p1)) && ((!android.support.v7.internal.widget.av.a(android.support.v7.internal.widget.av.j, p1)) && ((!android.support.v7.internal.widget.av.a(android.support.v7.internal.widget.av.l, p1)) && (p1 != android.support.v7.a.h.abc_cab_background_top_material))))))) {
            int v0_13 = 0;
        } else {
            v0_13 = 1;
        }
        return v0_13;
    }

    private static android.content.res.ColorStateList d(android.content.Context p8)
    {
        int[][] v0 = new int[][3];
        int[] v1_1 = new int[3];
        v0[0] = android.support.v7.internal.widget.ar.a;
        v1_1[0] = android.support.v7.internal.widget.ar.a(p8, 16842800, 1036831949);
        v0[1] = android.support.v7.internal.widget.ar.e;
        v1_1[1] = android.support.v7.internal.widget.ar.a(p8, android.support.v7.a.d.colorControlActivated, 1050253722);
        v0[2] = android.support.v7.internal.widget.ar.h;
        v1_1[2] = android.support.v7.internal.widget.ar.a(p8, 16842800, 1050253722);
        return new android.content.res.ColorStateList(v0, v1_1);
    }

    private static android.graphics.PorterDuff$Mode d(int p2)
    {
        android.graphics.PorterDuff$Mode v0 = 0;
        if (p2 == android.support.v7.a.h.abc_switch_thumb_material) {
            v0 = android.graphics.PorterDuff$Mode.MULTIPLY;
        }
        return v0;
    }

    private static android.content.res.ColorStateList e(android.content.Context p7)
    {
        int[][] v0 = new int[][3];
        int[] v1_1 = new int[3];
        android.content.res.ColorStateList v2_1 = android.support.v7.internal.widget.ar.b(p7, android.support.v7.a.d.colorSwitchThumbNormal);
        if ((v2_1 == null) || (!v2_1.isStateful())) {
            v0[0] = android.support.v7.internal.widget.ar.a;
            v1_1[0] = android.support.v7.internal.widget.ar.c(p7, android.support.v7.a.d.colorSwitchThumbNormal);
            v0[1] = android.support.v7.internal.widget.ar.e;
            v1_1[1] = android.support.v7.internal.widget.ar.a(p7, android.support.v7.a.d.colorControlActivated);
            v0[2] = android.support.v7.internal.widget.ar.h;
            v1_1[2] = android.support.v7.internal.widget.ar.a(p7, android.support.v7.a.d.colorSwitchThumbNormal);
        } else {
            v0[0] = android.support.v7.internal.widget.ar.a;
            v1_1[0] = v2_1.getColorForState(v0[0], 0);
            v0[1] = android.support.v7.internal.widget.ar.e;
            v1_1[1] = android.support.v7.internal.widget.ar.a(p7, android.support.v7.a.d.colorControlActivated);
            v0[2] = android.support.v7.internal.widget.ar.h;
            v1_1[2] = v2_1.getDefaultColor();
        }
        return new android.content.res.ColorStateList(v0, v1_1);
    }

    private static android.content.res.ColorStateList f(android.content.Context p6)
    {
        int[][] v0 = new int[][3];
        int[] v1_1 = new int[3];
        v0[0] = android.support.v7.internal.widget.ar.a;
        v1_1[0] = android.support.v7.internal.widget.ar.c(p6, android.support.v7.a.d.colorControlNormal);
        v0[1] = android.support.v7.internal.widget.ar.g;
        v1_1[1] = android.support.v7.internal.widget.ar.a(p6, android.support.v7.a.d.colorControlNormal);
        v0[2] = android.support.v7.internal.widget.ar.h;
        v1_1[2] = android.support.v7.internal.widget.ar.a(p6, android.support.v7.a.d.colorControlActivated);
        return new android.content.res.ColorStateList(v0, v1_1);
    }

    private static android.content.res.ColorStateList g(android.content.Context p1)
    {
        return android.support.v7.internal.widget.av.b(p1, android.support.v7.a.d.colorButtonNormal);
    }

    private static android.content.res.ColorStateList h(android.content.Context p1)
    {
        return android.support.v7.internal.widget.av.b(p1, android.support.v7.a.d.colorAccent);
    }

    private static android.content.res.ColorStateList i(android.content.Context p6)
    {
        int[][] v0 = new int[][3];
        int[] v1_1 = new int[3];
        v0[0] = android.support.v7.internal.widget.ar.a;
        v1_1[0] = android.support.v7.internal.widget.ar.c(p6, android.support.v7.a.d.colorControlNormal);
        v0[1] = android.support.v7.internal.widget.ar.g;
        v1_1[1] = android.support.v7.internal.widget.ar.a(p6, android.support.v7.a.d.colorControlNormal);
        v0[2] = android.support.v7.internal.widget.ar.h;
        v1_1[2] = android.support.v7.internal.widget.ar.a(p6, android.support.v7.a.d.colorControlActivated);
        return new android.content.res.ColorStateList(v0, v1_1);
    }

    public final android.content.res.ColorStateList a(int p11)
    {
        int[][] v0_3;
        int[][] v0_2 = ((android.content.Context) this.m.get());
        if (v0_2 != null) {
            int[][] v1_1;
            if (this.n == null) {
                v1_1 = 0;
            } else {
                v1_1 = ((android.content.res.ColorStateList) this.n.get(p11));
            }
            if (v1_1 != null) {
                v0_3 = v1_1;
            } else {
                if (p11 != android.support.v7.a.h.abc_edit_text_material) {
                    if (p11 != android.support.v7.a.h.abc_switch_track_mtrl_alpha) {
                        if (p11 != android.support.v7.a.h.abc_switch_thumb_material) {
                            if ((p11 != android.support.v7.a.h.abc_btn_default_mtrl_shape) && (p11 != android.support.v7.a.h.abc_btn_borderless_material)) {
                                if (p11 != android.support.v7.a.h.abc_btn_colored_material) {
                                    if ((p11 != android.support.v7.a.h.abc_spinner_mtrl_am_alpha) && (p11 != android.support.v7.a.h.abc_spinner_textfield_background_material)) {
                                        if (!android.support.v7.internal.widget.av.a(android.support.v7.internal.widget.av.h, p11)) {
                                            if (!android.support.v7.internal.widget.av.a(android.support.v7.internal.widget.av.k, p11)) {
                                                if (!android.support.v7.internal.widget.av.a(android.support.v7.internal.widget.av.l, p11)) {
                                                    v0_3 = v1_1;
                                                } else {
                                                    int[][] v1_4 = new int[][3];
                                                    int[] v2_14 = new int[3];
                                                    v1_4[0] = android.support.v7.internal.widget.ar.a;
                                                    v2_14[0] = android.support.v7.internal.widget.ar.c(v0_2, android.support.v7.a.d.colorControlNormal);
                                                    v1_4[1] = android.support.v7.internal.widget.ar.e;
                                                    v2_14[1] = android.support.v7.internal.widget.ar.a(v0_2, android.support.v7.a.d.colorControlActivated);
                                                    v1_4[2] = android.support.v7.internal.widget.ar.h;
                                                    v2_14[2] = android.support.v7.internal.widget.ar.a(v0_2, android.support.v7.a.d.colorControlNormal);
                                                    v0_3 = new android.content.res.ColorStateList(v1_4, v2_14);
                                                }
                                            } else {
                                                if (this.o == null) {
                                                    int[][] v1_7 = android.support.v7.internal.widget.ar.a(v0_2, android.support.v7.a.d.colorControlNormal);
                                                    int[] v2_16 = android.support.v7.internal.widget.ar.a(v0_2, android.support.v7.a.d.colorControlActivated);
                                                    int v3_9 = new int[][7];
                                                    int[] v4_1 = new int[7];
                                                    v3_9[0] = android.support.v7.internal.widget.ar.a;
                                                    v4_1[0] = android.support.v7.internal.widget.ar.c(v0_2, android.support.v7.a.d.colorControlNormal);
                                                    v3_9[1] = android.support.v7.internal.widget.ar.b;
                                                    v4_1[1] = v2_16;
                                                    v3_9[2] = android.support.v7.internal.widget.ar.c;
                                                    v4_1[2] = v2_16;
                                                    v3_9[3] = android.support.v7.internal.widget.ar.d;
                                                    v4_1[3] = v2_16;
                                                    v3_9[4] = android.support.v7.internal.widget.ar.e;
                                                    v4_1[4] = v2_16;
                                                    v3_9[5] = android.support.v7.internal.widget.ar.f;
                                                    v4_1[5] = v2_16;
                                                    v3_9[6] = android.support.v7.internal.widget.ar.h;
                                                    v4_1[6] = v1_7;
                                                    this.o = new android.content.res.ColorStateList(v3_9, v4_1);
                                                }
                                                v0_3 = this.o;
                                            }
                                        } else {
                                            v0_3 = android.support.v7.internal.widget.ar.b(v0_2, android.support.v7.a.d.colorControlNormal);
                                        }
                                    } else {
                                        int[][] v1_9 = new int[][3];
                                        int[] v2_18 = new int[3];
                                        v1_9[0] = android.support.v7.internal.widget.ar.a;
                                        v2_18[0] = android.support.v7.internal.widget.ar.c(v0_2, android.support.v7.a.d.colorControlNormal);
                                        v1_9[1] = android.support.v7.internal.widget.ar.g;
                                        v2_18[1] = android.support.v7.internal.widget.ar.a(v0_2, android.support.v7.a.d.colorControlNormal);
                                        v1_9[2] = android.support.v7.internal.widget.ar.h;
                                        v2_18[2] = android.support.v7.internal.widget.ar.a(v0_2, android.support.v7.a.d.colorControlActivated);
                                        v0_3 = new android.content.res.ColorStateList(v1_9, v2_18);
                                    }
                                } else {
                                    v0_3 = android.support.v7.internal.widget.av.b(v0_2, android.support.v7.a.d.colorAccent);
                                }
                            } else {
                                v0_3 = android.support.v7.internal.widget.av.b(v0_2, android.support.v7.a.d.colorButtonNormal);
                            }
                        } else {
                            int[][] v1_12 = new int[][3];
                            int[] v2_19 = new int[3];
                            int v3_19 = android.support.v7.internal.widget.ar.b(v0_2, android.support.v7.a.d.colorSwitchThumbNormal);
                            if ((v3_19 == 0) || (!v3_19.isStateful())) {
                                v1_12[0] = android.support.v7.internal.widget.ar.a;
                                v2_19[0] = android.support.v7.internal.widget.ar.c(v0_2, android.support.v7.a.d.colorSwitchThumbNormal);
                                v1_12[1] = android.support.v7.internal.widget.ar.e;
                                v2_19[1] = android.support.v7.internal.widget.ar.a(v0_2, android.support.v7.a.d.colorControlActivated);
                                v1_12[2] = android.support.v7.internal.widget.ar.h;
                                v2_19[2] = android.support.v7.internal.widget.ar.a(v0_2, android.support.v7.a.d.colorSwitchThumbNormal);
                            } else {
                                v1_12[0] = android.support.v7.internal.widget.ar.a;
                                v2_19[0] = v3_19.getColorForState(v1_12[0], 0);
                                v1_12[1] = android.support.v7.internal.widget.ar.e;
                                v2_19[1] = android.support.v7.internal.widget.ar.a(v0_2, android.support.v7.a.d.colorControlActivated);
                                v1_12[2] = android.support.v7.internal.widget.ar.h;
                                v2_19[2] = v3_19.getDefaultColor();
                            }
                            v0_3 = new android.content.res.ColorStateList(v1_12, v2_19);
                        }
                    } else {
                        int[][] v1_13 = new int[][3];
                        int[] v2_20 = new int[3];
                        v1_13[0] = android.support.v7.internal.widget.ar.a;
                        v2_20[0] = android.support.v7.internal.widget.ar.a(v0_2, 16842800, 1036831949);
                        v1_13[1] = android.support.v7.internal.widget.ar.e;
                        v2_20[1] = android.support.v7.internal.widget.ar.a(v0_2, android.support.v7.a.d.colorControlActivated, 1050253722);
                        v1_13[2] = android.support.v7.internal.widget.ar.h;
                        v2_20[2] = android.support.v7.internal.widget.ar.a(v0_2, 16842800, 1050253722);
                        v0_3 = new android.content.res.ColorStateList(v1_13, v2_20);
                    }
                } else {
                    int[][] v1_14 = new int[][3];
                    int[] v2_21 = new int[3];
                    v1_14[0] = android.support.v7.internal.widget.ar.a;
                    v2_21[0] = android.support.v7.internal.widget.ar.c(v0_2, android.support.v7.a.d.colorControlNormal);
                    v1_14[1] = android.support.v7.internal.widget.ar.g;
                    v2_21[1] = android.support.v7.internal.widget.ar.a(v0_2, android.support.v7.a.d.colorControlNormal);
                    v1_14[2] = android.support.v7.internal.widget.ar.h;
                    v2_21[2] = android.support.v7.internal.widget.ar.a(v0_2, android.support.v7.a.d.colorControlActivated);
                    v0_3 = new android.content.res.ColorStateList(v1_14, v2_21);
                }
                if (v0_3 != null) {
                    if (this.n == null) {
                        this.n = new android.util.SparseArray();
                    }
                    this.n.append(p11, v0_3);
                }
            }
        } else {
            v0_3 = 0;
        }
        return v0_3;
    }

    public final android.graphics.drawable.Drawable a(int p6, boolean p7)
    {
        android.graphics.drawable.LayerDrawable v1_0 = 0;
        android.graphics.drawable.LayerDrawable v0_2 = ((android.content.Context) this.m.get());
        if (v0_2 != null) {
            android.graphics.drawable.LayerDrawable v0_3 = android.support.v4.c.h.a(v0_2, p6);
            if (v0_3 != null) {
                if (android.os.Build$VERSION.SDK_INT >= 8) {
                    v0_3 = v0_3.mutate();
                }
                boolean v2_1 = this.a(p6);
                if (!v2_1) {
                    if (p6 != android.support.v7.a.h.abc_cab_background_top_material) {
                        if ((!this.a(p6, v0_3)) && (p7)) {
                            v0_3 = 0;
                        }
                    } else {
                        android.graphics.drawable.LayerDrawable v0_5 = new android.graphics.drawable.Drawable[2];
                        v0_5[0] = this.a(android.support.v7.a.h.abc_cab_background_internal_bg, 0);
                        v0_5[1] = this.a(android.support.v7.a.h.abc_cab_background_top_mtrl_alpha, 0);
                        v1_0 = new android.graphics.drawable.LayerDrawable(v0_5);
                        return v1_0;
                    }
                } else {
                    v0_3 = android.support.v4.e.a.a.c(v0_3);
                    android.support.v4.e.a.a.a(v0_3, v2_1);
                    if (p6 == android.support.v7.a.h.abc_switch_thumb_material) {
                        v1_0 = android.graphics.PorterDuff$Mode.MULTIPLY;
                    }
                    if (v1_0 != null) {
                        android.support.v4.e.a.a.a(v0_3, v1_0);
                    }
                }
            }
            v1_0 = v0_3;
        }
        return v1_0;
    }

    public final boolean a(int p9, android.graphics.drawable.Drawable p10)
    {
        int v0_3;
        int v0_2 = ((android.content.Context) this.m.get());
        if (v0_2 != 0) {
            int v5_0;
            int v3_7;
            int v7;
            int v6_1;
            int v6_0 = android.support.v7.internal.widget.av.d;
            if (!android.support.v7.internal.widget.av.a(android.support.v7.internal.widget.av.g, p9)) {
                if (!android.support.v7.internal.widget.av.a(android.support.v7.internal.widget.av.i, p9)) {
                    if (!android.support.v7.internal.widget.av.a(android.support.v7.internal.widget.av.j, p9)) {
                        if (p9 != android.support.v7.a.h.abc_list_divider_mtrl_alpha) {
                            v3_7 = -1;
                            v5_0 = 0;
                            v7 = v6_0;
                            v6_1 = 0;
                        } else {
                            v5_0 = 16842800;
                            v3_7 = Math.round(1109603123);
                            v7 = v6_0;
                            v6_1 = 1;
                        }
                    } else {
                        v6_1 = 1;
                        v7 = android.graphics.PorterDuff$Mode.MULTIPLY;
                        v5_0 = 16842801;
                        v3_7 = -1;
                    }
                } else {
                    v5_0 = android.support.v7.a.d.colorControlActivated;
                    v7 = v6_0;
                    v6_1 = 1;
                    v3_7 = -1;
                }
            } else {
                v5_0 = android.support.v7.a.d.colorControlNormal;
                v7 = v6_0;
                v6_1 = 1;
                v3_7 = -1;
            }
            if (v6_1 == 0) {
                v0_3 = 0;
            } else {
                p10.setColorFilter(android.support.v7.internal.widget.av.a(android.support.v7.internal.widget.ar.a(v0_2, v5_0), v7));
                if (v3_7 != -1) {
                    p10.setAlpha(v3_7);
                }
                v0_3 = 1;
            }
        } else {
            v0_3 = 0;
        }
        return v0_3;
    }
}
