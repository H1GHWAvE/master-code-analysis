package android.support.v7.internal.widget;
public final class ar {
    static final int[] a;
    static final int[] b;
    static final int[] c;
    static final int[] d;
    static final int[] e;
    static final int[] f;
    static final int[] g;
    static final int[] h;
    private static final ThreadLocal i;
    private static final int[] j;

    static ar()
    {
        android.support.v7.internal.widget.ar.i = new ThreadLocal();
        int[] v0_2 = new int[1];
        v0_2[0] = -16842910;
        android.support.v7.internal.widget.ar.a = v0_2;
        int[] v0_3 = new int[1];
        v0_3[0] = 16842908;
        android.support.v7.internal.widget.ar.b = v0_3;
        int[] v0_4 = new int[1];
        v0_4[0] = 16843518;
        android.support.v7.internal.widget.ar.c = v0_4;
        int[] v0_5 = new int[1];
        v0_5[0] = 16842919;
        android.support.v7.internal.widget.ar.d = v0_5;
        int[] v0_6 = new int[1];
        v0_6[0] = 16842912;
        android.support.v7.internal.widget.ar.e = v0_6;
        int[] v0_7 = new int[1];
        v0_7[0] = 16842913;
        android.support.v7.internal.widget.ar.f = v0_7;
        int[] v0_9 = new int[2];
        v0_9 = {-16842919, -16842908};
        android.support.v7.internal.widget.ar.g = v0_9;
        int[] v0_10 = new int[0];
        android.support.v7.internal.widget.ar.h = v0_10;
        int[] v0_11 = new int[1];
        android.support.v7.internal.widget.ar.j = v0_11;
        return;
    }

    public ar()
    {
        return;
    }

    public static int a(android.content.Context p3, int p4)
    {
        android.support.v7.internal.widget.ar.j[0] = p4;
        android.content.res.TypedArray v0_2 = p3.obtainStyledAttributes(0, android.support.v7.internal.widget.ar.j);
        try {
            Throwable v1_3 = v0_2.getColor(0, 0);
            v0_2.recycle();
            return v1_3;
        } catch (Throwable v1_4) {
            v0_2.recycle();
            throw v1_4;
        }
    }

    static int a(android.content.Context p2, int p3, float p4)
    {
        int v0_0 = android.support.v7.internal.widget.ar.a(p2, p3);
        return android.support.v4.e.j.b(v0_0, Math.round((((float) android.graphics.Color.alpha(v0_0)) * p4)));
    }

    public static android.content.res.ColorStateList a(int p5, int p6)
    {
        int[][] v0 = new int[][2];
        int[] v1_1 = new int[2];
        v0[0] = android.support.v7.internal.widget.ar.a;
        v1_1[0] = p6;
        v0[1] = android.support.v7.internal.widget.ar.h;
        v1_1[1] = p5;
        return new android.content.res.ColorStateList(v0, v1_1);
    }

    private static android.util.TypedValue a()
    {
        android.util.TypedValue v0_2 = ((android.util.TypedValue) android.support.v7.internal.widget.ar.i.get());
        if (v0_2 == null) {
            v0_2 = new android.util.TypedValue();
            android.support.v7.internal.widget.ar.i.set(v0_2);
        }
        return v0_2;
    }

    public static android.content.res.ColorStateList b(android.content.Context p2, int p3)
    {
        android.support.v7.internal.widget.ar.j[0] = p3;
        android.content.res.TypedArray v0_2 = p2.obtainStyledAttributes(0, android.support.v7.internal.widget.ar.j);
        try {
            Throwable v1_3 = v0_2.getColorStateList(0);
            v0_2.recycle();
            return v1_3;
        } catch (Throwable v1_4) {
            v0_2.recycle();
            throw v1_4;
        }
    }

    public static int c(android.content.Context p4, int p5)
    {
        int v0_6;
        int v0_0 = android.support.v7.internal.widget.ar.b(p4, p5);
        if ((v0_0 == 0) || (!v0_0.isStateful())) {
            int v0_3 = ((android.util.TypedValue) android.support.v7.internal.widget.ar.i.get());
            if (v0_3 == 0) {
                v0_3 = new android.util.TypedValue();
                android.support.v7.internal.widget.ar.i.set(v0_3);
            }
            p4.getTheme().resolveAttribute(16842803, v0_3, 1);
            v0_6 = android.support.v7.internal.widget.ar.a(p4, p5, v0_3.getFloat());
        } else {
            v0_6 = v0_0.getColorForState(android.support.v7.internal.widget.ar.a, v0_0.getDefaultColor());
        }
        return v0_6;
    }
}
