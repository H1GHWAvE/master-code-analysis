package android.support.v7.internal.widget;
final class aw extends android.support.v4.n.j {

    public aw()
    {
        this(6);
        return;
    }

    static int a(int p2, android.graphics.PorterDuff$Mode p3)
    {
        return (((p2 + 31) * 31) + p3.hashCode());
    }

    private android.graphics.PorterDuffColorFilter a(int p2, android.graphics.PorterDuff$Mode p3, android.graphics.PorterDuffColorFilter p4)
    {
        return ((android.graphics.PorterDuffColorFilter) this.a(Integer.valueOf(android.support.v7.internal.widget.aw.a(p2, p3)), p4));
    }

    private android.graphics.PorterDuffColorFilter b(int p2, android.graphics.PorterDuff$Mode p3)
    {
        return ((android.graphics.PorterDuffColorFilter) this.a(Integer.valueOf(android.support.v7.internal.widget.aw.a(p2, p3))));
    }
}
