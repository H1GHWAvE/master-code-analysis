package android.support.v7.internal.widget;
final class z implements android.view.View$OnClickListener, android.view.View$OnLongClickListener, android.widget.AdapterView$OnItemClickListener, android.widget.PopupWindow$OnDismissListener {
    final synthetic android.support.v7.internal.widget.ActivityChooserView a;

    private z(android.support.v7.internal.widget.ActivityChooserView p1)
    {
        this.a = p1;
        return;
    }

    synthetic z(android.support.v7.internal.widget.ActivityChooserView p1, byte p2)
    {
        this(p1);
        return;
    }

    private void a()
    {
        if (android.support.v7.internal.widget.ActivityChooserView.h(this.a) != null) {
            android.support.v7.internal.widget.ActivityChooserView.h(this.a).onDismiss();
        }
        return;
    }

    public final void onClick(android.view.View p3)
    {
        if (p3 != android.support.v7.internal.widget.ActivityChooserView.e(this.a)) {
            if (p3 != android.support.v7.internal.widget.ActivityChooserView.f(this.a)) {
                throw new IllegalArgumentException();
            } else {
                android.support.v7.internal.widget.ActivityChooserView.a(this.a, 0);
                android.support.v7.internal.widget.ActivityChooserView.a(this.a, android.support.v7.internal.widget.ActivityChooserView.g(this.a));
            }
        } else {
            this.a.b();
            IllegalArgumentException v0_14 = android.support.v7.internal.widget.ActivityChooserView.a(this.a).c.b(android.support.v7.internal.widget.ActivityChooserView.a(this.a).c.a(android.support.v7.internal.widget.ActivityChooserView.a(this.a).c.b()));
            if (v0_14 != null) {
                v0_14.addFlags(524288);
                this.a.getContext().startActivity(v0_14);
            }
        }
        return;
    }

    public final void onDismiss()
    {
        if (android.support.v7.internal.widget.ActivityChooserView.h(this.a) != null) {
            android.support.v7.internal.widget.ActivityChooserView.h(this.a).onDismiss();
        }
        if (this.a.a != null) {
            this.a.a.a(0);
        }
        return;
    }

    public final void onItemClick(android.widget.AdapterView p9, android.view.View p10, int p11, long p12)
    {
        switch (((android.support.v7.internal.widget.y) p9.getAdapter()).getItemViewType(p11)) {
            case 0:
                this.a.b();
                if (!android.support.v7.internal.widget.ActivityChooserView.d(this.a)) {
                    if (!android.support.v7.internal.widget.ActivityChooserView.a(this.a).d) {
                        p11++;
                    }
                    android.support.v7.internal.widget.r v0_13 = android.support.v7.internal.widget.ActivityChooserView.a(this.a).c.b(p11);
                    if (v0_13 == null) {
                    } else {
                        v0_13.addFlags(524288);
                        this.a.getContext().startActivity(v0_13);
                    }
                } else {
                    if (p11 <= 0) {
                    } else {
                        android.support.v7.internal.widget.l v2 = android.support.v7.internal.widget.ActivityChooserView.a(this.a).c;
                        try {
                            int v1_7;
                            v2.d();
                            android.support.v7.internal.widget.r v0_18 = ((android.support.v7.internal.widget.o) v2.d.get(p11));
                            int v1_6 = ((android.support.v7.internal.widget.o) v2.d.get(0));
                        } catch (android.support.v7.internal.widget.r v0_24) {
                            throw v0_24;
                        }
                        if (v1_6 == 0) {
                            v1_7 = 1065353216;
                        } else {
                            v1_7 = ((v1_6.b - v0_18.b) + 1084227584);
                        }
                        v2.a(new android.support.v7.internal.widget.r(new android.content.ComponentName(v0_18.a.activityInfo.packageName, v0_18.a.activityInfo.name), System.currentTimeMillis(), v1_7));
                    }
                }
                break;
            case 1:
                android.support.v7.internal.widget.ActivityChooserView.a(this.a, 2147483647);
                break;
            default:
                throw new IllegalArgumentException();
        }
        return;
    }

    public final boolean onLongClick(android.view.View p4)
    {
        if (p4 != android.support.v7.internal.widget.ActivityChooserView.e(this.a)) {
            throw new IllegalArgumentException();
        } else {
            if (android.support.v7.internal.widget.ActivityChooserView.a(this.a).getCount() > 0) {
                android.support.v7.internal.widget.ActivityChooserView.a(this.a, 1);
                android.support.v7.internal.widget.ActivityChooserView.a(this.a, android.support.v7.internal.widget.ActivityChooserView.g(this.a));
            }
            return 1;
        }
    }
}
