package android.support.v7.internal.widget;
public final class ay implements android.support.v7.internal.widget.ad {
    private static final String e = "ToolbarWidgetWrapper";
    private static final int f = 3;
    private static final long g = 200;
    android.support.v7.widget.Toolbar a;
    CharSequence b;
    android.view.Window$Callback c;
    boolean d;
    private int h;
    private android.view.View i;
    private android.widget.Spinner j;
    private android.view.View k;
    private android.graphics.drawable.Drawable l;
    private android.graphics.drawable.Drawable m;
    private android.graphics.drawable.Drawable n;
    private boolean o;
    private CharSequence p;
    private CharSequence q;
    private android.support.v7.widget.ActionMenuPresenter r;
    private int s;
    private final android.support.v7.internal.widget.av t;
    private int u;
    private android.graphics.drawable.Drawable v;

    public ay(android.support.v7.widget.Toolbar p3, boolean p4)
    {
        this(p3, p4, android.support.v7.a.l.abc_action_bar_up_description, android.support.v7.a.h.abc_ic_ab_back_mtrl_am_alpha);
        return;
    }

    private ay(android.support.v7.widget.Toolbar p7, boolean p8, int p9, int p10)
    {
        android.support.v7.internal.widget.av v0_3;
        this.s = 0;
        this.u = 0;
        this.a = p7;
        this.b = p7.getTitle();
        this.p = p7.getSubtitle();
        if (this.b == null) {
            v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        this.o = v0_3;
        this.n = p7.getNavigationIcon();
        if (!p8) {
            android.support.v7.internal.widget.av v0_5 = 11;
            if (this.a.getNavigationIcon() != null) {
                v0_5 = 15;
            }
            this.h = v0_5;
            this.t = android.support.v7.internal.widget.av.a(p7.getContext());
        } else {
            android.support.v7.internal.widget.av v0_9 = android.support.v7.internal.widget.ax.a(p7.getContext(), 0, android.support.v7.a.n.ActionBar, android.support.v7.a.d.actionBarStyle);
            android.content.res.TypedArray v2_4 = v0_9.c(android.support.v7.a.n.ActionBar_title);
            if (!android.text.TextUtils.isEmpty(v2_4)) {
                this.b(v2_4);
            }
            android.content.res.TypedArray v2_6 = v0_9.c(android.support.v7.a.n.ActionBar_subtitle);
            if (!android.text.TextUtils.isEmpty(v2_6)) {
                this.c(v2_6);
            }
            android.content.res.TypedArray v2_8 = v0_9.a(android.support.v7.a.n.ActionBar_logo);
            if (v2_8 != null) {
                this.b(v2_8);
            }
            android.content.res.TypedArray v2_10 = v0_9.a(android.support.v7.a.n.ActionBar_icon);
            if ((this.n == null) && (v2_10 != null)) {
                this.a(v2_10);
            }
            android.content.res.TypedArray v2_12 = v0_9.a(android.support.v7.a.n.ActionBar_homeAsUpIndicator);
            if (v2_12 != null) {
                this.c(v2_12);
            }
            this.c(v0_9.a(android.support.v7.a.n.ActionBar_displayOptions, 0));
            android.content.res.TypedArray v2_16 = v0_9.e(android.support.v7.a.n.ActionBar_customNavigationLayout, 0);
            if (v2_16 != null) {
                this.a(android.view.LayoutInflater.from(this.a.getContext()).inflate(v2_16, this.a, 0));
                this.c((this.h | 16));
            }
            android.content.res.TypedArray v2_21 = v0_9.d(android.support.v7.a.n.ActionBar_height, 0);
            if (v2_21 > null) {
                android.support.v7.widget.Toolbar v3_8 = this.a.getLayoutParams();
                v3_8.height = v2_21;
                this.a.setLayoutParams(v3_8);
            }
            android.content.res.TypedArray v2_24 = v0_9.b(android.support.v7.a.n.ActionBar_contentInsetStart, -1);
            android.support.v7.widget.Toolbar v3_10 = v0_9.b(android.support.v7.a.n.ActionBar_contentInsetEnd, -1);
            if ((v2_24 >= null) || (v3_10 >= null)) {
                this.a.i.a(Math.max(v2_24, 0), Math.max(v3_10, 0));
            }
            android.content.res.TypedArray v2_27 = v0_9.e(android.support.v7.a.n.ActionBar_titleTextStyle, 0);
            if (v2_27 != null) {
                android.support.v7.widget.Toolbar v3_12 = this.a;
                android.content.Context v4_5 = this.a.getContext();
                v3_12.g = v2_27;
                if (v3_12.b != null) {
                    v3_12.b.setTextAppearance(v4_5, v2_27);
                }
            }
            android.content.res.TypedArray v2_29 = v0_9.e(android.support.v7.a.n.ActionBar_subtitleTextStyle, 0);
            if (v2_29 != null) {
                android.support.v7.widget.Toolbar v3_14 = this.a;
                android.content.Context v4_7 = this.a.getContext();
                v3_14.h = v2_29;
                if (v3_14.c != null) {
                    v3_14.c.setTextAppearance(v4_7, v2_29);
                }
            }
            android.content.res.TypedArray v2_31 = v0_9.e(android.support.v7.a.n.ActionBar_popupTheme, 0);
            if (v2_31 != null) {
                this.a.setPopupTheme(v2_31);
            }
            v0_9.a.recycle();
            this.t = v0_9.a();
        }
        if (p9 != this.u) {
            this.u = p9;
            if (android.text.TextUtils.isEmpty(this.a.getNavigationContentDescription())) {
                this.h(this.u);
            }
        }
        this.q = this.a.getNavigationContentDescription();
        android.support.v7.internal.widget.av v0_19 = this.t.a(p10, 0);
        if (this.v != v0_19) {
            this.v = v0_19;
            this.G();
        }
        this.a.setNavigationOnClickListener(new android.support.v7.internal.widget.az(this));
        return;
    }

    private int C()
    {
        int v0 = 11;
        if (this.a.getNavigationIcon() != null) {
            v0 = 15;
        }
        return v0;
    }

    private void D()
    {
        android.graphics.drawable.Drawable v0_0 = 0;
        if ((this.h & 2) != 0) {
            if ((this.h & 1) == 0) {
                v0_0 = this.l;
            } else {
                if (this.m == null) {
                    v0_0 = this.l;
                } else {
                    v0_0 = this.m;
                }
            }
        }
        this.a.setLogo(v0_0);
        return;
    }

    private void E()
    {
        if (this.j == null) {
            this.j = new android.support.v7.widget.aa(this.a.getContext(), 0, android.support.v7.a.d.actionDropDownStyle);
            this.j.setLayoutParams(new android.support.v7.widget.ck(-2, 8388627));
        }
        return;
    }

    private void F()
    {
        if ((this.h & 4) != 0) {
            if (!android.text.TextUtils.isEmpty(this.q)) {
                this.a.setNavigationContentDescription(this.q);
            } else {
                this.a.setNavigationContentDescription(this.u);
            }
        }
        return;
    }

    private void G()
    {
        if ((this.h & 4) != 0) {
            android.graphics.drawable.Drawable v0_3;
            if (this.n == null) {
                v0_3 = this.v;
            } else {
                v0_3 = this.n;
            }
            this.a.setNavigationIcon(v0_3);
        }
        return;
    }

    private static synthetic android.support.v7.widget.Toolbar a(android.support.v7.internal.widget.ay p1)
    {
        return p1.a;
    }

    private static synthetic CharSequence b(android.support.v7.internal.widget.ay p1)
    {
        return p1.b;
    }

    private static synthetic android.view.Window$Callback c(android.support.v7.internal.widget.ay p1)
    {
        return p1.c;
    }

    private static synthetic boolean d(android.support.v7.internal.widget.ay p1)
    {
        return p1.d;
    }

    private void e(CharSequence p2)
    {
        this.b = p2;
        if ((this.h & 8) != 0) {
            this.a.setTitle(p2);
        }
        return;
    }

    public final int A()
    {
        return this.a.getVisibility();
    }

    public final android.view.Menu B()
    {
        return this.a.getMenu();
    }

    public final android.support.v4.view.fk a(int p3, long p4)
    {
        int v0_1;
        if (p3 != 8) {
            if (p3 != 0) {
                v0_1 = 0;
            } else {
                v0_1 = android.support.v4.view.cx.p(this.a).a(1065353216);
                v0_1.a(p4);
                v0_1.a(new android.support.v7.internal.widget.bb(this));
            }
        } else {
            v0_1 = android.support.v4.view.cx.p(this.a).a(0);
            v0_1.a(p4);
            v0_1.a(new android.support.v7.internal.widget.ba(this));
        }
        return v0_1;
    }

    public final android.view.ViewGroup a()
    {
        return this.a;
    }

    public final void a(int p3)
    {
        int v0_0;
        if (p3 == 0) {
            v0_0 = 0;
        } else {
            v0_0 = this.t.a(p3, 0);
        }
        this.a(v0_0);
        return;
    }

    public final void a(android.graphics.drawable.Drawable p1)
    {
        this.l = p1;
        this.D();
        return;
    }

    public final void a(android.support.v7.internal.view.menu.y p2, android.support.v7.internal.view.menu.j p3)
    {
        android.support.v7.widget.Toolbar v0 = this.a;
        v0.m = p2;
        v0.n = p3;
        return;
    }

    public final void a(android.support.v7.internal.widget.al p5)
    {
        if ((this.i != null) && (this.i.getParent() == this.a)) {
            this.a.removeView(this.i);
        }
        this.i = p5;
        if ((p5 != null) && (this.s == 2)) {
            this.a.addView(this.i, 0);
            int v0_8 = ((android.support.v7.widget.ck) this.i.getLayoutParams());
            v0_8.width = -2;
            v0_8.height = -2;
            v0_8.a = 8388691;
            p5.setAllowCollapse(1);
        }
        return;
    }

    public final void a(android.util.SparseArray p2)
    {
        this.a.saveHierarchyState(p2);
        return;
    }

    public final void a(android.view.Menu p7, android.support.v7.internal.view.menu.y p8)
    {
        if (this.r == null) {
            this.r = new android.support.v7.widget.ActionMenuPresenter(this.a.getContext());
            this.r.h = android.support.v7.a.i.action_menu_presenter;
        }
        this.r.f = p8;
        android.support.v7.widget.Toolbar v0_5 = this.a;
        android.support.v7.widget.ActionMenuPresenter v1_3 = this.r;
        if ((((android.support.v7.internal.view.menu.i) p7) != null) || (v0_5.a != null)) {
            v0_5.d();
            android.support.v7.widget.ActionMenuView v2_2 = v0_5.a.c;
            if (v2_2 != ((android.support.v7.internal.view.menu.i) p7)) {
                if (v2_2 != null) {
                    v2_2.b(v0_5.k);
                    v2_2.b(v0_5.l);
                }
                if (v0_5.l == null) {
                    v0_5.l = new android.support.v7.widget.cj(v0_5, 0);
                }
                v1_3.o = 1;
                if (((android.support.v7.internal.view.menu.i) p7) == null) {
                    v1_3.a(v0_5.e, 0);
                    v0_5.l.a(v0_5.e, 0);
                    v1_3.a(1);
                    v0_5.l.a(1);
                } else {
                    ((android.support.v7.internal.view.menu.i) p7).a(v1_3, v0_5.e);
                    ((android.support.v7.internal.view.menu.i) p7).a(v0_5.l, v0_5.e);
                }
                v0_5.a.setPopupTheme(v0_5.f);
                v0_5.a.setPresenter(v1_3);
                v0_5.k = v1_3;
            }
        }
        return;
    }

    public final void a(android.view.View p3)
    {
        if ((this.k != null) && ((this.h & 16) != 0)) {
            this.a.removeView(this.k);
        }
        this.k = p3;
        if ((p3 != null) && ((this.h & 16) != 0)) {
            this.a.addView(this.k);
        }
        return;
    }

    public final void a(android.view.Window$Callback p1)
    {
        this.c = p1;
        return;
    }

    public final void a(android.widget.SpinnerAdapter p2, android.widget.AdapterView$OnItemSelectedListener p3)
    {
        this.E();
        this.j.setAdapter(p2);
        this.j.setOnItemSelectedListener(p3);
        return;
    }

    public final void a(CharSequence p2)
    {
        if (!this.o) {
            this.e(p2);
        }
        return;
    }

    public final void a(boolean p2)
    {
        this.a.setCollapsible(p2);
        return;
    }

    public final android.content.Context b()
    {
        return this.a.getContext();
    }

    public final void b(int p3)
    {
        int v0_0;
        if (p3 == 0) {
            v0_0 = 0;
        } else {
            v0_0 = this.t.a(p3, 0);
        }
        this.b(v0_0);
        return;
    }

    public final void b(android.graphics.drawable.Drawable p1)
    {
        this.m = p1;
        this.D();
        return;
    }

    public final void b(android.util.SparseArray p2)
    {
        this.a.restoreHierarchyState(p2);
        return;
    }

    public final void b(CharSequence p2)
    {
        this.o = 1;
        this.e(p2);
        return;
    }

    public final void c(int p4)
    {
        android.support.v7.widget.Toolbar v0_1 = (this.h ^ p4);
        this.h = p4;
        if (v0_1 != null) {
            if ((v0_1 & 4) != 0) {
                if ((p4 & 4) == 0) {
                    this.a.setNavigationIcon(0);
                } else {
                    this.G();
                    this.F();
                }
            }
            if ((v0_1 & 3) != 0) {
                this.D();
            }
            if ((v0_1 & 8) != 0) {
                if ((p4 & 8) == 0) {
                    this.a.setTitle(0);
                    this.a.setSubtitle(0);
                } else {
                    this.a.setTitle(this.b);
                    this.a.setSubtitle(this.p);
                }
            }
            if (((v0_1 & 16) != 0) && (this.k != null)) {
                if ((p4 & 16) == 0) {
                    this.a.removeView(this.k);
                } else {
                    this.a.addView(this.k);
                }
            }
        }
        return;
    }

    public final void c(android.graphics.drawable.Drawable p1)
    {
        this.n = p1;
        this.G();
        return;
    }

    public final void c(CharSequence p2)
    {
        this.p = p2;
        if ((this.h & 8) != 0) {
            this.a.setSubtitle(p2);
        }
        return;
    }

    public final boolean c()
    {
        int v0_3;
        int v0_0 = this.a;
        if ((v0_0.l == null) || (v0_0.l.b == null)) {
            v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        return v0_3;
    }

    public final void d()
    {
        this.a.c();
        return;
    }

    public final void d(int p5)
    {
        android.support.v7.widget.Toolbar v0_0 = this.s;
        if (p5 != v0_0) {
            switch (v0_0) {
                case 1:
                    if ((this.j == null) || (this.j.getParent() != this.a)) {
                    } else {
                        this.a.removeView(this.j);
                    }
                    break;
                case 2:
                    if ((this.i == null) || (this.i.getParent() != this.a)) {
                    } else {
                        this.a.removeView(this.i);
                    }
                    break;
            }
            this.s = p5;
            switch (p5) {
                case 0:
                    break;
                case 1:
                    this.E();
                    this.a.addView(this.j, 0);
                    break;
                case 2:
                    if (this.i != null) {
                        this.a.addView(this.i, 0);
                        android.support.v7.widget.Toolbar v0_13 = ((android.support.v7.widget.ck) this.i.getLayoutParams());
                        v0_13.width = -2;
                        v0_13.height = -2;
                        v0_13.a = 8388691;
                    }
                    break;
                default:
                    throw new IllegalArgumentException(new StringBuilder("Invalid navigation mode ").append(p5).toString());
            }
        }
        return;
    }

    public final void d(android.graphics.drawable.Drawable p2)
    {
        if (this.v != p2) {
            this.v = p2;
            this.G();
        }
        return;
    }

    public final void d(CharSequence p1)
    {
        this.q = p1;
        this.F();
        return;
    }

    public final CharSequence e()
    {
        return this.a.getTitle();
    }

    public final void e(int p3)
    {
        if (this.j != null) {
            this.j.setSelection(p3);
            return;
        } else {
            throw new IllegalStateException("Can\'t set dropdown selected position without an adapter");
        }
    }

    public final void e(android.graphics.drawable.Drawable p2)
    {
        this.a.setBackgroundDrawable(p2);
        return;
    }

    public final CharSequence f()
    {
        return this.a.getSubtitle();
    }

    public final void f(int p3)
    {
        android.support.v4.view.fk v0_1 = this.a(p3, 200);
        if (v0_1 != null) {
            v0_1.b();
        }
        return;
    }

    public final void g()
    {
        android.util.Log.i("ToolbarWidgetWrapper", "Progress display unsupported");
        return;
    }

    public final void g(int p3)
    {
        int v0_0;
        if (p3 == 0) {
            v0_0 = 0;
        } else {
            v0_0 = this.t.a(p3, 0);
        }
        this.c(v0_0);
        return;
    }

    public final void h()
    {
        android.util.Log.i("ToolbarWidgetWrapper", "Progress display unsupported");
        return;
    }

    public final void h(int p2)
    {
        String v0_2;
        if (p2 != 0) {
            v0_2 = this.a.getContext().getString(p2);
        } else {
            v0_2 = 0;
        }
        this.d(v0_2);
        return;
    }

    public final void i(int p2)
    {
        if (p2 != this.u) {
            this.u = p2;
            if (android.text.TextUtils.isEmpty(this.a.getNavigationContentDescription())) {
                this.h(this.u);
            }
        }
        return;
    }

    public final boolean i()
    {
        int v0_1;
        if (this.l == null) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    public final void j(int p2)
    {
        this.a.setVisibility(p2);
        return;
    }

    public final boolean j()
    {
        int v0_1;
        if (this.m == null) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    public final boolean k()
    {
        int v0_3;
        int v0_0 = this.a;
        if ((v0_0.getVisibility() != 0) || ((v0_0.a == null) || (!v0_0.a.d))) {
            v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        return v0_3;
    }

    public final boolean l()
    {
        return this.a.a();
    }

    public final boolean m()
    {
        int v0 = 1;
        int v2_0 = this.a;
        if (v2_0.a == null) {
            v0 = 0;
        } else {
            int v2_4;
            int v2_1 = v2_0.a;
            if ((v2_1.e == null) || (!v2_1.e.j())) {
                v2_4 = 0;
            } else {
                v2_4 = 1;
            }
            if (v2_4 == 0) {
            }
        }
        return v0;
    }

    public final boolean n()
    {
        return this.a.b();
    }

    public final boolean o()
    {
        int v0 = 1;
        int v2_0 = this.a;
        if (v2_0.a == null) {
            v0 = 0;
        } else {
            int v2_4;
            int v2_1 = v2_0.a;
            if ((v2_1.e == null) || (!v2_1.e.f())) {
                v2_4 = 0;
            } else {
                v2_4 = 1;
            }
            if (v2_4 == 0) {
            }
        }
        return v0;
    }

    public final void p()
    {
        this.d = 1;
        return;
    }

    public final void q()
    {
        android.support.v7.widget.ActionMenuView v0_0 = this.a;
        if (v0_0.a != null) {
            v0_0.a.b();
        }
        return;
    }

    public final int r()
    {
        return this.h;
    }

    public final boolean s()
    {
        int v0_1;
        if (this.i == null) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    public final boolean t()
    {
        int v0 = 0;
        int v1_0 = this.a;
        if (v1_0.b != null) {
            android.text.Layout v2_1 = v1_0.b.getLayout();
            if (v2_1 != null) {
                int v3 = v2_1.getLineCount();
                int v1_2 = 0;
                while (v1_2 < v3) {
                    if (v2_1.getEllipsisCount(v1_2) <= 0) {
                        v1_2++;
                    } else {
                        v0 = 1;
                        break;
                    }
                }
            }
        }
        return v0;
    }

    public final void u()
    {
        return;
    }

    public final int v()
    {
        return this.s;
    }

    public final int w()
    {
        int v0_1;
        if (this.j == null) {
            v0_1 = 0;
        } else {
            v0_1 = this.j.getSelectedItemPosition();
        }
        return v0_1;
    }

    public final int x()
    {
        int v0_1;
        if (this.j == null) {
            v0_1 = 0;
        } else {
            v0_1 = this.j.getCount();
        }
        return v0_1;
    }

    public final android.view.View y()
    {
        return this.k;
    }

    public final int z()
    {
        return this.a.getHeight();
    }
}
