package android.support.v7.internal.view.menu;
final class b extends android.support.v7.widget.as {
    final synthetic android.support.v7.internal.view.menu.ActionMenuItemView a;

    public b(android.support.v7.internal.view.menu.ActionMenuItemView p1)
    {
        this.a = p1;
        this(p1);
        return;
    }

    public final android.support.v7.widget.an a()
    {
        int v0_2;
        if (android.support.v7.internal.view.menu.ActionMenuItemView.a(this.a) == null) {
            v0_2 = 0;
        } else {
            v0_2 = android.support.v7.internal.view.menu.ActionMenuItemView.a(this.a).a();
        }
        return v0_2;
    }

    protected final boolean b()
    {
        int v0 = 0;
        if ((android.support.v7.internal.view.menu.ActionMenuItemView.b(this.a) != null) && (android.support.v7.internal.view.menu.ActionMenuItemView.b(this.a).a(android.support.v7.internal.view.menu.ActionMenuItemView.c(this.a)))) {
            boolean v1_5 = this.a();
            if ((v1_5) && (v1_5.c.isShowing())) {
                v0 = 1;
            }
        }
        return v0;
    }
}
