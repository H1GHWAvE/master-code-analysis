package android.support.v7.internal.view.menu;
final class q extends android.widget.FrameLayout implements android.support.v7.c.c {
    final android.view.CollapsibleActionView a;

    q(android.view.View p2)
    {
        this(p2.getContext());
        this.a = ((android.view.CollapsibleActionView) p2);
        this.addView(p2);
        return;
    }

    private android.view.View c()
    {
        return ((android.view.View) this.a);
    }

    public final void a()
    {
        this.a.onActionViewExpanded();
        return;
    }

    public final void b()
    {
        this.a.onActionViewCollapsed();
        return;
    }
}
