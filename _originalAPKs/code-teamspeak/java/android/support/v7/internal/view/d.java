package android.support.v7.internal.view;
public final class d extends android.view.ActionMode {
    final android.content.Context a;
    final android.support.v7.c.a b;

    public d(android.content.Context p1, android.support.v7.c.a p2)
    {
        this.a = p1;
        this.b = p2;
        return;
    }

    public final void finish()
    {
        this.b.c();
        return;
    }

    public final android.view.View getCustomView()
    {
        return this.b.i();
    }

    public final android.view.Menu getMenu()
    {
        return android.support.v7.internal.view.menu.ab.a(this.a, ((android.support.v4.g.a.a) this.b.b()));
    }

    public final android.view.MenuInflater getMenuInflater()
    {
        return this.b.a();
    }

    public final CharSequence getSubtitle()
    {
        return this.b.g();
    }

    public final Object getTag()
    {
        return this.b.b;
    }

    public final CharSequence getTitle()
    {
        return this.b.f();
    }

    public final boolean getTitleOptionalHint()
    {
        return this.b.c;
    }

    public final void invalidate()
    {
        this.b.d();
        return;
    }

    public final boolean isTitleOptional()
    {
        return this.b.h();
    }

    public final void setCustomView(android.view.View p2)
    {
        this.b.a(p2);
        return;
    }

    public final void setSubtitle(int p2)
    {
        this.b.b(p2);
        return;
    }

    public final void setSubtitle(CharSequence p2)
    {
        this.b.a(p2);
        return;
    }

    public final void setTag(Object p2)
    {
        this.b.b = p2;
        return;
    }

    public final void setTitle(int p2)
    {
        this.b.a(p2);
        return;
    }

    public final void setTitle(CharSequence p2)
    {
        this.b.b(p2);
        return;
    }

    public final void setTitleOptionalHint(boolean p2)
    {
        this.b.a(p2);
        return;
    }
}
