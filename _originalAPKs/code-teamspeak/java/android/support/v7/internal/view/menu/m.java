package android.support.v7.internal.view.menu;
public final class m implements android.support.v4.g.a.b {
    private static final int F = 1;
    private static final int G = 2;
    private static final int H = 4;
    private static final int I = 8;
    private static final int J = 16;
    private static final int K = 32;
    static final int j = 0;
    static String l = "None";
    static String m = "None";
    static String n = "None";
    static String o = "None";
    private static final String p = "MenuItemImpl";
    private static final int q = 3;
    private int A;
    private android.support.v7.internal.view.menu.ad B;
    private Runnable C;
    private android.view.MenuItem$OnMenuItemClickListener D;
    private int E;
    private android.view.View L;
    private android.support.v4.view.bf M;
    private boolean N;
    final int f;
    android.support.v7.internal.view.menu.i g;
    int h;
    public android.support.v4.view.n i;
    android.view.ContextMenu$ContextMenuInfo k;
    private final int r;
    private final int s;
    private final int t;
    private CharSequence u;
    private CharSequence v;
    private android.content.Intent w;
    private char x;
    private char y;
    private android.graphics.drawable.Drawable z;

    m(android.support.v7.internal.view.menu.i p3, int p4, int p5, int p6, int p7, CharSequence p8, int p9)
    {
        this.A = 0;
        this.E = 16;
        this.h = 0;
        this.N = 0;
        this.g = p3;
        this.r = p5;
        this.s = p4;
        this.t = p6;
        this.f = p7;
        this.u = p8;
        this.h = p9;
        return;
    }

    private android.support.v4.g.a.b a(int p4)
    {
        android.view.View v0_1 = this.g.e;
        this.a(android.view.LayoutInflater.from(v0_1).inflate(p4, new android.widget.LinearLayout(v0_1), 0));
        return this;
    }

    private android.support.v4.g.a.b a(android.view.View p3)
    {
        this.L = p3;
        this.i = 0;
        if ((p3 != null) && ((p3.getId() == -1) && (this.r > 0))) {
            p3.setId(this.r);
        }
        this.g.g();
        return this;
    }

    static synthetic android.support.v7.internal.view.menu.i a(android.support.v7.internal.view.menu.m p1)
    {
        return p1.g;
    }

    private android.view.MenuItem a(Runnable p1)
    {
        this.C = p1;
        return this;
    }

    private void a(android.view.ContextMenu$ContextMenuInfo p1)
    {
        this.k = p1;
        return;
    }

    private android.support.v4.g.a.b b(int p1)
    {
        this.setShowAsAction(p1);
        return this;
    }

    private int j()
    {
        return this.f;
    }

    private Runnable k()
    {
        return this.C;
    }

    private String l()
    {
        String v0_4;
        String v0_0 = this.c();
        if (v0_0 != null) {
            StringBuilder v1_1 = new StringBuilder(android.support.v7.internal.view.menu.m.l);
            switch (v0_0) {
                case 8:
                    v1_1.append(android.support.v7.internal.view.menu.m.n);
                    break;
                case 10:
                    v1_1.append(android.support.v7.internal.view.menu.m.m);
                    break;
                case 32:
                    v1_1.append(android.support.v7.internal.view.menu.m.o);
                    break;
                default:
                    v1_1.append(v0_0);
            }
            v0_4 = v1_1.toString();
        } else {
            v0_4 = "";
        }
        return v0_4;
    }

    private void m()
    {
        this.g.g();
        return;
    }

    private boolean n()
    {
        return this.g.m;
    }

    private boolean o()
    {
        int v0_2;
        if ((this.h & 4) != 4) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    public final android.support.v4.g.a.b a(android.support.v4.view.bf p1)
    {
        this.M = p1;
        return this;
    }

    public final android.support.v4.g.a.b a(android.support.v4.view.n p3)
    {
        if (this.i != null) {
            android.support.v4.view.n v0_1 = this.i;
            v0_1.b = 0;
            v0_1.a = 0;
        }
        this.L = 0;
        this.i = p3;
        this.g.c(1);
        if (this.i != null) {
            this.i.a(new android.support.v7.internal.view.menu.n(this));
        }
        return this;
    }

    public final android.support.v4.view.n a()
    {
        return this.i;
    }

    final CharSequence a(android.support.v7.internal.view.menu.aa p2)
    {
        if ((p2 == null) || (!p2.a())) {
            CharSequence v0_1 = this.getTitle();
        } else {
            v0_1 = this.getTitleCondensed();
        }
        return v0_1;
    }

    public final void a(android.support.v7.internal.view.menu.ad p2)
    {
        this.B = p2;
        p2.setHeaderTitle(this.getTitle());
        return;
    }

    public final void a(boolean p3)
    {
        int v0_1;
        if (!p3) {
            v0_1 = 0;
        } else {
            v0_1 = 4;
        }
        this.E = (v0_1 | (this.E & -5));
        return;
    }

    final void b(boolean p5)
    {
        android.support.v7.internal.view.menu.i v0_1;
        int v2 = this.E;
        if (!p5) {
            v0_1 = 0;
        } else {
            v0_1 = 2;
        }
        this.E = (v0_1 | (this.E & -3));
        if (v2 != this.E) {
            this.g.c(0);
        }
        return;
    }

    public final boolean b()
    {
        int v0 = 1;
        if (((this.D == null) || (!this.D.onMenuItemClick(this))) && (!this.g.a(this.g.k(), this))) {
            if (this.C == null) {
                if (this.w == null) {
                    if ((this.i == null) || (!this.i.e())) {
                        v0 = 0;
                    }
                } else {
                    try {
                        this.g.e.startActivity(this.w);
                    } catch (boolean v1_9) {
                        android.util.Log.e("MenuItemImpl", "Can\'t find activity to handle intent; ignoring", v1_9);
                    }
                }
            } else {
                this.C.run();
            }
        }
        return v0;
    }

    final char c()
    {
        char v0_2;
        if (!this.g.b()) {
            v0_2 = this.x;
        } else {
            v0_2 = this.y;
        }
        return v0_2;
    }

    final boolean c(boolean p5)
    {
        int v0_1;
        int v1 = 0;
        int v2 = this.E;
        if (!p5) {
            v0_1 = 8;
        } else {
            v0_1 = 0;
        }
        this.E = (v0_1 | (this.E & -9));
        if (v2 != this.E) {
            v1 = 1;
        }
        return v1;
    }

    public final boolean collapseActionView()
    {
        boolean v0_0 = 0;
        if ((this.h & 8) != 0) {
            if (this.L != null) {
                if ((this.M == null) || (this.M.b(this))) {
                    v0_0 = this.g.b(this);
                }
            } else {
                v0_0 = 1;
            }
        }
        return v0_0;
    }

    public final void d(boolean p2)
    {
        if (!p2) {
            this.E = (this.E & -33);
        } else {
            this.E = (this.E | 32);
        }
        return;
    }

    final boolean d()
    {
        if ((!this.g.c()) || (this.c() == 0)) {
            int v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        return v0_3;
    }

    public final void e(boolean p3)
    {
        this.N = p3;
        this.g.c(0);
        return;
    }

    public final boolean e()
    {
        int v0_2;
        if ((this.E & 4) == 0) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    public final boolean expandActionView()
    {
        boolean v0_0 = 0;
        if ((this.i()) && ((this.M == null) || (this.M.a(this)))) {
            v0_0 = this.g.a(this);
        }
        return v0_0;
    }

    public final boolean f()
    {
        int v0_2;
        if ((this.E & 32) != 32) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    public final boolean g()
    {
        int v0 = 1;
        if ((this.h & 1) != 1) {
            v0 = 0;
        }
        return v0;
    }

    public final android.view.ActionProvider getActionProvider()
    {
        throw new UnsupportedOperationException("This is not supported, use MenuItemCompat.getActionProvider()");
    }

    public final android.view.View getActionView()
    {
        int v0_2;
        if (this.L == null) {
            if (this.i == null) {
                v0_2 = 0;
            } else {
                this.L = this.i.a(this);
                v0_2 = this.L;
            }
        } else {
            v0_2 = this.L;
        }
        return v0_2;
    }

    public final char getAlphabeticShortcut()
    {
        return this.y;
    }

    public final int getGroupId()
    {
        return this.s;
    }

    public final android.graphics.drawable.Drawable getIcon()
    {
        int v0_2;
        if (this.z == null) {
            if (this.A == 0) {
                v0_2 = 0;
            } else {
                v0_2 = android.support.v7.internal.widget.av.a(this.g.e, this.A);
                this.A = 0;
                this.z = v0_2;
            }
        } else {
            v0_2 = this.z;
        }
        return v0_2;
    }

    public final android.content.Intent getIntent()
    {
        return this.w;
    }

    public final int getItemId()
    {
        return this.r;
    }

    public final android.view.ContextMenu$ContextMenuInfo getMenuInfo()
    {
        return this.k;
    }

    public final char getNumericShortcut()
    {
        return this.x;
    }

    public final int getOrder()
    {
        return this.t;
    }

    public final android.view.SubMenu getSubMenu()
    {
        return this.B;
    }

    public final CharSequence getTitle()
    {
        return this.u;
    }

    public final CharSequence getTitleCondensed()
    {
        String v0_1;
        if (this.v == null) {
            v0_1 = this.u;
        } else {
            v0_1 = this.v;
        }
        if ((android.os.Build$VERSION.SDK_INT < 18) && ((v0_1 != null) && (!(v0_1 instanceof String)))) {
            v0_1 = v0_1.toString();
        }
        return v0_1;
    }

    public final boolean h()
    {
        int v0_2;
        if ((this.h & 2) != 2) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    public final boolean hasSubMenu()
    {
        int v0_1;
        if (this.B == null) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    public final boolean i()
    {
        int v0 = 0;
        if ((this.h & 8) != 0) {
            if ((this.L == null) && (this.i != null)) {
                this.L = this.i.a(this);
            }
            if (this.L != null) {
                v0 = 1;
            }
        }
        return v0;
    }

    public final boolean isActionViewExpanded()
    {
        return this.N;
    }

    public final boolean isCheckable()
    {
        int v0 = 1;
        if ((this.E & 1) != 1) {
            v0 = 0;
        }
        return v0;
    }

    public final boolean isChecked()
    {
        int v0_2;
        if ((this.E & 2) != 2) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    public final boolean isEnabled()
    {
        int v0_2;
        if ((this.E & 16) == 0) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    public final boolean isVisible()
    {
        int v0 = 1;
        if ((this.i == null) || (!this.i.b())) {
            if ((this.E & 8) != 0) {
                v0 = 0;
            }
        } else {
            if (((this.E & 8) != 0) || (!this.i.c())) {
                v0 = 0;
            }
        }
        return v0;
    }

    public final android.view.MenuItem setActionProvider(android.view.ActionProvider p3)
    {
        throw new UnsupportedOperationException("This is not supported, use MenuItemCompat.setActionProvider()");
    }

    public final synthetic android.view.MenuItem setActionView(int p4)
    {
        android.view.View v0_1 = this.g.e;
        this.a(android.view.LayoutInflater.from(v0_1).inflate(p4, new android.widget.LinearLayout(v0_1), 0));
        return this;
    }

    public final synthetic android.view.MenuItem setActionView(android.view.View p2)
    {
        return this.a(p2);
    }

    public final android.view.MenuItem setAlphabeticShortcut(char p3)
    {
        if (this.y != p3) {
            this.y = Character.toLowerCase(p3);
            this.g.c(0);
        }
        return this;
    }

    public final android.view.MenuItem setCheckable(boolean p5)
    {
        android.support.v7.internal.view.menu.i v0_1;
        int v2 = this.E;
        if (!p5) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        this.E = (v0_1 | (this.E & -2));
        if (v2 != this.E) {
            this.g.c(0);
        }
        return this;
    }

    public final android.view.MenuItem setChecked(boolean p8)
    {
        if ((this.E & 4) == 0) {
            this.b(p8);
        } else {
            android.support.v7.internal.view.menu.i v4 = this.g;
            int v5 = this.getGroupId();
            int v6 = v4.g.size();
            int v3 = 0;
            while (v3 < v6) {
                int v0_5 = ((android.support.v7.internal.view.menu.m) v4.g.get(v3));
                if ((v0_5.getGroupId() == v5) && ((v0_5.e()) && (v0_5.isCheckable()))) {
                    int v1_3;
                    if (v0_5 != this) {
                        v1_3 = 0;
                    } else {
                        v1_3 = 1;
                    }
                    v0_5.b(v1_3);
                }
                v3++;
            }
        }
        return this;
    }

    public final android.view.MenuItem setEnabled(boolean p3)
    {
        if (!p3) {
            this.E = (this.E & -17);
        } else {
            this.E = (this.E | 16);
        }
        this.g.c(0);
        return this;
    }

    public final android.view.MenuItem setIcon(int p3)
    {
        this.z = 0;
        this.A = p3;
        this.g.c(0);
        return this;
    }

    public final android.view.MenuItem setIcon(android.graphics.drawable.Drawable p3)
    {
        this.A = 0;
        this.z = p3;
        this.g.c(0);
        return this;
    }

    public final android.view.MenuItem setIntent(android.content.Intent p1)
    {
        this.w = p1;
        return this;
    }

    public final android.view.MenuItem setNumericShortcut(char p3)
    {
        if (this.x != p3) {
            this.x = p3;
            this.g.c(0);
        }
        return this;
    }

    public final android.view.MenuItem setOnActionExpandListener(android.view.MenuItem$OnActionExpandListener p3)
    {
        throw new UnsupportedOperationException("This is not supported, use MenuItemCompat.setOnActionExpandListener()");
    }

    public final android.view.MenuItem setOnMenuItemClickListener(android.view.MenuItem$OnMenuItemClickListener p1)
    {
        this.D = p1;
        return this;
    }

    public final android.view.MenuItem setShortcut(char p3, char p4)
    {
        this.x = p3;
        this.y = Character.toLowerCase(p4);
        this.g.c(0);
        return this;
    }

    public final void setShowAsAction(int p3)
    {
        switch ((p3 & 3)) {
            case 0:
            case 1:
            case 2:
                this.h = p3;
                this.g.g();
                return;
            default:
                throw new IllegalArgumentException("SHOW_AS_ACTION_ALWAYS, SHOW_AS_ACTION_IF_ROOM, and SHOW_AS_ACTION_NEVER are mutually exclusive.");
        }
    }

    public final synthetic android.view.MenuItem setShowAsActionFlags(int p1)
    {
        this.setShowAsAction(p1);
        return this;
    }

    public final android.view.MenuItem setTitle(int p2)
    {
        return this.setTitle(this.g.e.getString(p2));
    }

    public final android.view.MenuItem setTitle(CharSequence p3)
    {
        this.u = p3;
        this.g.c(0);
        if (this.B != null) {
            this.B.setHeaderTitle(p3);
        }
        return this;
    }

    public final android.view.MenuItem setTitleCondensed(CharSequence p3)
    {
        this.v = p3;
        this.g.c(0);
        return this;
    }

    public final android.view.MenuItem setVisible(boolean p2)
    {
        if (this.c(p2)) {
            this.g.f();
        }
        return this;
    }

    public final String toString()
    {
        return this.u.toString();
    }
}
