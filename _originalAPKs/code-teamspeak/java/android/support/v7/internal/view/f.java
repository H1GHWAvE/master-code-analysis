package android.support.v7.internal.view;
public final class f extends android.view.MenuInflater {
    private static final String a = "SupportMenuInflater";
    private static final String b = "menu";
    private static final String c = "group";
    private static final String d = "item";
    private static final int e;
    private static final Class[] f;
    private static final Class[] g;
    private final Object[] h;
    private final Object[] i;
    private android.content.Context j;
    private Object k;

    static f()
    {
        Class[] v0_1 = new Class[1];
        v0_1[0] = android.content.Context;
        android.support.v7.internal.view.f.f = v0_1;
        android.support.v7.internal.view.f.g = v0_1;
        return;
    }

    public f(android.content.Context p3)
    {
        this(p3);
        this.j = p3;
        Object[] v0_1 = new Object[1];
        v0_1[0] = p3;
        this.h = v0_1;
        this.i = this.h;
        return;
    }

    static synthetic android.content.Context a(android.support.v7.internal.view.f p1)
    {
        return p1.j;
    }

    private static Object a(Object p2)
    {
        android.content.Context v0_0 = p2;
        while ((!(v0_0 instanceof android.app.Activity)) && ((v0_0 instanceof android.content.ContextWrapper))) {
            v0_0 = ((android.content.ContextWrapper) v0_0).getBaseContext();
        }
        return v0_0;
    }

    private void a(org.xmlpull.v1.XmlPullParser p11, android.util.AttributeSet p12, android.view.Menu p13)
    {
        android.support.v7.internal.view.h v4_1 = new android.support.v7.internal.view.h(this, p13);
        String v0_0 = p11.getEventType();
        String v3_0 = 0;
        String v2_0 = 0;
        while (v0_0 != 2) {
            v0_0 = p11.next();
            if (v0_0 == 1) {
            }
            String v1_6 = 0;
            while (v1_6 == null) {
                String v2_3;
                String v1_7;
                String v0_6;
                switch (v0_0) {
                    case 1:
                        throw new RuntimeException("Unexpected end of document");
                        break;
                    case 2:
                        if (v3_0 != null) {
                            v0_6 = v1_6;
                            v1_7 = v2_0;
                            v2_3 = v3_0;
                        } else {
                            String v0_15 = p11.getName();
                            if (!v0_15.equals("group")) {
                                if (!v0_15.equals("item")) {
                                    if (!v0_15.equals("menu")) {
                                        v2_3 = 1;
                                        v1_7 = v0_15;
                                        v0_6 = v1_6;
                                    } else {
                                        this.a(p11, p12, v4_1.b());
                                        v0_6 = v1_6;
                                        v1_7 = v2_0;
                                        v2_3 = v3_0;
                                    }
                                } else {
                                    int v5_14 = v4_1.z.j.obtainStyledAttributes(p12, android.support.v7.a.n.MenuItem);
                                    v4_1.i = v5_14.getResourceId(android.support.v7.a.n.MenuItem_android_id, 0);
                                    v4_1.j = ((v5_14.getInt(android.support.v7.a.n.MenuItem_android_menuCategory, v4_1.c) & -65536) | (v5_14.getInt(android.support.v7.a.n.MenuItem_android_orderInCategory, v4_1.d) & 65535));
                                    v4_1.k = v5_14.getText(android.support.v7.a.n.MenuItem_android_title);
                                    v4_1.l = v5_14.getText(android.support.v7.a.n.MenuItem_android_titleCondensed);
                                    v4_1.m = v5_14.getResourceId(android.support.v7.a.n.MenuItem_android_icon, 0);
                                    v4_1.n = android.support.v7.internal.view.h.a(v5_14.getString(android.support.v7.a.n.MenuItem_android_alphabeticShortcut));
                                    v4_1.o = android.support.v7.internal.view.h.a(v5_14.getString(android.support.v7.a.n.MenuItem_android_numericShortcut));
                                    if (!v5_14.hasValue(android.support.v7.a.n.MenuItem_android_checkable)) {
                                        v4_1.p = v4_1.e;
                                    } else {
                                        String v0_42;
                                        if (!v5_14.getBoolean(android.support.v7.a.n.MenuItem_android_checkable, 0)) {
                                            v0_42 = 0;
                                        } else {
                                            v0_42 = 1;
                                        }
                                        v4_1.p = v0_42;
                                    }
                                    String v0_60;
                                    v4_1.q = v5_14.getBoolean(android.support.v7.a.n.MenuItem_android_checked, 0);
                                    v4_1.r = v5_14.getBoolean(android.support.v7.a.n.MenuItem_android_visible, v4_1.f);
                                    v4_1.s = v5_14.getBoolean(android.support.v7.a.n.MenuItem_android_enabled, v4_1.g);
                                    v4_1.t = v5_14.getInt(android.support.v7.a.n.MenuItem_showAsAction, -1);
                                    v4_1.x = v5_14.getString(android.support.v7.a.n.MenuItem_android_onClick);
                                    v4_1.u = v5_14.getResourceId(android.support.v7.a.n.MenuItem_actionLayout, 0);
                                    v4_1.v = v5_14.getString(android.support.v7.a.n.MenuItem_actionViewClass);
                                    v4_1.w = v5_14.getString(android.support.v7.a.n.MenuItem_actionProviderClass);
                                    if (v4_1.w == null) {
                                        v0_60 = 0;
                                    } else {
                                        v0_60 = 1;
                                    }
                                    if ((v0_60 == null) || ((v4_1.u != 0) || (v4_1.v != null))) {
                                        if (v0_60 != null) {
                                            android.util.Log.w("SupportMenuInflater", "Ignoring attribute \'actionProviderClass\'. Action view already specified.");
                                        }
                                        v4_1.y = 0;
                                    } else {
                                        v4_1.y = ((android.support.v4.view.n) v4_1.a(v4_1.w, android.support.v7.internal.view.f.g, v4_1.z.i));
                                    }
                                    v5_14.recycle();
                                    v4_1.h = 0;
                                    v0_6 = v1_6;
                                    v1_7 = v2_0;
                                    v2_3 = v3_0;
                                }
                            } else {
                                String v0_69 = v4_1.z.j.obtainStyledAttributes(p12, android.support.v7.a.n.MenuGroup);
                                v4_1.b = v0_69.getResourceId(android.support.v7.a.n.MenuGroup_android_id, 0);
                                v4_1.c = v0_69.getInt(android.support.v7.a.n.MenuGroup_android_menuCategory, 0);
                                v4_1.d = v0_69.getInt(android.support.v7.a.n.MenuGroup_android_orderInCategory, 0);
                                v4_1.e = v0_69.getInt(android.support.v7.a.n.MenuGroup_android_checkableBehavior, 0);
                                v4_1.f = v0_69.getBoolean(android.support.v7.a.n.MenuGroup_android_visible, 1);
                                v4_1.g = v0_69.getBoolean(android.support.v7.a.n.MenuGroup_android_enabled, 1);
                                v0_69.recycle();
                                v0_6 = v1_6;
                                v1_7 = v2_0;
                                v2_3 = v3_0;
                            }
                        }
                        break;
                    case 3:
                        String v0_4 = p11.getName();
                        if ((v3_0 == null) || (!v0_4.equals(v2_0))) {
                            if (!v0_4.equals("group")) {
                                if (!v0_4.equals("item")) {
                                    if (!v0_4.equals("menu")) {
                                    } else {
                                        v0_6 = 1;
                                        v1_7 = v2_0;
                                        v2_3 = v3_0;
                                    }
                                } else {
                                    if (v4_1.h) {
                                    } else {
                                        if ((v4_1.y == null) || (!v4_1.y.f())) {
                                            v4_1.h = 1;
                                            v4_1.a(v4_1.a.add(v4_1.b, v4_1.i, v4_1.j, v4_1.k));
                                            v0_6 = v1_6;
                                            v1_7 = v2_0;
                                            v2_3 = v3_0;
                                        } else {
                                            v4_1.b();
                                            v0_6 = v1_6;
                                            v1_7 = v2_0;
                                            v2_3 = v3_0;
                                        }
                                    }
                                }
                            } else {
                                v4_1.a();
                                v0_6 = v1_6;
                                v1_7 = v2_0;
                                v2_3 = v3_0;
                            }
                        } else {
                            v2_3 = 0;
                            v1_7 = 0;
                            v0_6 = v1_6;
                        }
                        break;
                    default:
                }
                v0_0 = p11.next();
                v3_0 = v2_3;
                v2_0 = v1_7;
                v1_6 = v0_6;
            }
            return;
        }
        String v0_1 = p11.getName();
        if (!v0_1.equals("menu")) {
            throw new RuntimeException(new StringBuilder("Expecting menu, got ").append(v0_1).toString());
        } else {
            v0_0 = p11.next();
        }
    }

    static synthetic Class[] a()
    {
        return android.support.v7.internal.view.f.g;
    }

    static synthetic Class[] b()
    {
        return android.support.v7.internal.view.f.f;
    }

    static synthetic Object[] b(android.support.v7.internal.view.f p1)
    {
        return p1.i;
    }

    private Object c()
    {
        if (this.k == null) {
            this.k = android.support.v7.internal.view.f.a(this.j);
        }
        return this.k;
    }

    static synthetic Object c(android.support.v7.internal.view.f p1)
    {
        if (p1.k == null) {
            p1.k = android.support.v7.internal.view.f.a(p1.j);
        }
        return p1.k;
    }

    static synthetic Object[] d(android.support.v7.internal.view.f p1)
    {
        return p1.h;
    }

    public final void inflate(int p5, android.view.Menu p6)
    {
        if ((p6 instanceof android.support.v4.g.a.a)) {
            try {
                android.content.res.XmlResourceParser v1 = this.j.getResources().getLayout(p5);
                this.a(v1, android.util.Xml.asAttributeSet(v1), p6);
            } catch (java.io.IOException v0_5) {
                throw new android.view.InflateException("Error inflating menu XML", v0_5);
            } catch (java.io.IOException v0_6) {
                if (v1 != null) {
                    v1.close();
                }
                throw v0_6;
            } catch (java.io.IOException v0_4) {
                throw new android.view.InflateException("Error inflating menu XML", v0_4);
            }
            if (v1 != null) {
                v1.close();
            }
        } else {
            super.inflate(p5, p6);
        }
        return;
    }
}
