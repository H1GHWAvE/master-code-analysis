package android.support.v7.internal.view.menu;
final class r extends android.support.v7.internal.view.menu.f implements android.support.v4.view.bf {
    final synthetic android.support.v7.internal.view.menu.o a;

    r(android.support.v7.internal.view.menu.o p1, android.view.MenuItem$OnActionExpandListener p2)
    {
        this.a = p1;
        this(p2);
        return;
    }

    public final boolean a(android.view.MenuItem p3)
    {
        return ((android.view.MenuItem$OnActionExpandListener) this.d).onMenuItemActionExpand(this.a.a(p3));
    }

    public final boolean b(android.view.MenuItem p3)
    {
        return ((android.view.MenuItem$OnActionExpandListener) this.d).onMenuItemActionCollapse(this.a.a(p3));
    }
}
