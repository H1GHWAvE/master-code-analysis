package android.support.v7.internal.view.menu;
public final class a implements android.support.v4.g.a.b {
    private static final int s = 0;
    private static final int u = 1;
    private static final int v = 2;
    private static final int w = 4;
    private static final int x = 8;
    private static final int y = 16;
    private final int f;
    private final int g;
    private final int h;
    private final int i;
    private CharSequence j;
    private CharSequence k;
    private android.content.Intent l;
    private char m;
    private char n;
    private android.graphics.drawable.Drawable o;
    private int p;
    private android.content.Context q;
    private android.view.MenuItem$OnMenuItemClickListener r;
    private int t;

    public a(android.content.Context p3, CharSequence p4)
    {
        this.p = 0;
        this.t = 16;
        this.q = p3;
        this.f = 16908332;
        this.g = 0;
        this.h = 0;
        this.i = 0;
        this.j = p4;
        return;
    }

    private android.support.v4.g.a.b a(int p1)
    {
        this.setShowAsAction(p1);
        return this;
    }

    private android.support.v7.internal.view.menu.a a(boolean p3)
    {
        int v0_1;
        if (!p3) {
            v0_1 = 0;
        } else {
            v0_1 = 4;
        }
        this.t = (v0_1 | (this.t & -5));
        return this;
    }

    private boolean b()
    {
        int v0 = 1;
        if ((this.r == null) || (!this.r.onMenuItemClick(this))) {
            if (this.l == null) {
                v0 = 0;
            } else {
                this.q.startActivity(this.l);
            }
        }
        return v0;
    }

    private static android.support.v4.g.a.b c()
    {
        throw new UnsupportedOperationException();
    }

    private static android.support.v4.g.a.b d()
    {
        throw new UnsupportedOperationException();
    }

    public final android.support.v4.g.a.b a(android.support.v4.view.bf p1)
    {
        return this;
    }

    public final android.support.v4.g.a.b a(android.support.v4.view.n p2)
    {
        throw new UnsupportedOperationException();
    }

    public final android.support.v4.view.n a()
    {
        return 0;
    }

    public final boolean collapseActionView()
    {
        return 0;
    }

    public final boolean expandActionView()
    {
        return 0;
    }

    public final android.view.ActionProvider getActionProvider()
    {
        throw new UnsupportedOperationException();
    }

    public final android.view.View getActionView()
    {
        return 0;
    }

    public final char getAlphabeticShortcut()
    {
        return this.n;
    }

    public final int getGroupId()
    {
        return this.g;
    }

    public final android.graphics.drawable.Drawable getIcon()
    {
        return this.o;
    }

    public final android.content.Intent getIntent()
    {
        return this.l;
    }

    public final int getItemId()
    {
        return this.f;
    }

    public final android.view.ContextMenu$ContextMenuInfo getMenuInfo()
    {
        return 0;
    }

    public final char getNumericShortcut()
    {
        return this.m;
    }

    public final int getOrder()
    {
        return this.i;
    }

    public final android.view.SubMenu getSubMenu()
    {
        return 0;
    }

    public final CharSequence getTitle()
    {
        return this.j;
    }

    public final CharSequence getTitleCondensed()
    {
        CharSequence v0_1;
        if (this.k == null) {
            v0_1 = this.j;
        } else {
            v0_1 = this.k;
        }
        return v0_1;
    }

    public final boolean hasSubMenu()
    {
        return 0;
    }

    public final boolean isActionViewExpanded()
    {
        return 0;
    }

    public final boolean isCheckable()
    {
        int v0_2;
        if ((this.t & 1) == 0) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    public final boolean isChecked()
    {
        int v0_2;
        if ((this.t & 2) == 0) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    public final boolean isEnabled()
    {
        int v0_2;
        if ((this.t & 16) == 0) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    public final boolean isVisible()
    {
        int v0_2;
        if ((this.t & 8) != 0) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    public final android.view.MenuItem setActionProvider(android.view.ActionProvider p2)
    {
        throw new UnsupportedOperationException();
    }

    public final synthetic android.view.MenuItem setActionView(int p2)
    {
        throw new UnsupportedOperationException();
    }

    public final synthetic android.view.MenuItem setActionView(android.view.View p2)
    {
        throw new UnsupportedOperationException();
    }

    public final android.view.MenuItem setAlphabeticShortcut(char p1)
    {
        this.n = p1;
        return this;
    }

    public final android.view.MenuItem setCheckable(boolean p3)
    {
        int v0_1;
        if (!p3) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        this.t = (v0_1 | (this.t & -2));
        return this;
    }

    public final android.view.MenuItem setChecked(boolean p3)
    {
        int v0_1;
        if (!p3) {
            v0_1 = 0;
        } else {
            v0_1 = 2;
        }
        this.t = (v0_1 | (this.t & -3));
        return this;
    }

    public final android.view.MenuItem setEnabled(boolean p3)
    {
        int v0_1;
        if (!p3) {
            v0_1 = 0;
        } else {
            v0_1 = 16;
        }
        this.t = (v0_1 | (this.t & -17));
        return this;
    }

    public final android.view.MenuItem setIcon(int p2)
    {
        this.p = p2;
        this.o = android.support.v4.c.h.a(this.q, p2);
        return this;
    }

    public final android.view.MenuItem setIcon(android.graphics.drawable.Drawable p2)
    {
        this.o = p2;
        this.p = 0;
        return this;
    }

    public final android.view.MenuItem setIntent(android.content.Intent p1)
    {
        this.l = p1;
        return this;
    }

    public final android.view.MenuItem setNumericShortcut(char p1)
    {
        this.m = p1;
        return this;
    }

    public final android.view.MenuItem setOnActionExpandListener(android.view.MenuItem$OnActionExpandListener p2)
    {
        throw new UnsupportedOperationException();
    }

    public final android.view.MenuItem setOnMenuItemClickListener(android.view.MenuItem$OnMenuItemClickListener p1)
    {
        this.r = p1;
        return this;
    }

    public final android.view.MenuItem setShortcut(char p1, char p2)
    {
        this.m = p1;
        this.n = p2;
        return this;
    }

    public final void setShowAsAction(int p1)
    {
        return;
    }

    public final synthetic android.view.MenuItem setShowAsActionFlags(int p1)
    {
        this.setShowAsAction(p1);
        return this;
    }

    public final android.view.MenuItem setTitle(int p2)
    {
        this.j = this.q.getResources().getString(p2);
        return this;
    }

    public final android.view.MenuItem setTitle(CharSequence p1)
    {
        this.j = p1;
        return this;
    }

    public final android.view.MenuItem setTitleCondensed(CharSequence p1)
    {
        this.k = p1;
        return this;
    }

    public final android.view.MenuItem setVisible(boolean p3)
    {
        int v0_1;
        if (!p3) {
            v0_1 = 8;
        } else {
            v0_1 = 0;
        }
        this.t = (v0_1 | (this.t & 8));
        return this;
    }
}
