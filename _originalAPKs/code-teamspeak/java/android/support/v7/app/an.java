package android.support.v7.app;
 class an extends android.support.v7.internal.view.k {
    final synthetic android.support.v7.app.ak a;

    an(android.support.v7.app.ak p1, android.view.Window$Callback p2)
    {
        this.a = p1;
        this(p2);
        return;
    }

    public boolean dispatchKeyEvent(android.view.KeyEvent p2)
    {
        if ((!this.a.a(p2)) && (!super.dispatchKeyEvent(p2))) {
            int v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        return v0_3;
    }

    public boolean dispatchKeyShortcutEvent(android.view.KeyEvent p3)
    {
        if ((!super.dispatchKeyShortcutEvent(p3)) && (!this.a.a(p3.getKeyCode(), p3))) {
            int v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        return v0_3;
    }

    public void onContentChanged()
    {
        return;
    }

    public boolean onCreatePanelMenu(int p2, android.view.Menu p3)
    {
        if ((p2 != 0) || ((p3 instanceof android.support.v7.internal.view.menu.i))) {
            int v0_1 = super.onCreatePanelMenu(p2, p3);
        } else {
            v0_1 = 0;
        }
        return v0_1;
    }

    public boolean onMenuOpened(int p2, android.view.Menu p3)
    {
        if ((!super.onMenuOpened(p2, p3)) && (!this.a.e(p2))) {
            int v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        return v0_3;
    }

    public void onPanelClosed(int p2, android.view.Menu p3)
    {
        super.onPanelClosed(p2, p3);
        this.a.d(p2);
        return;
    }

    public boolean onPreparePanel(int p4, android.view.View p5, android.view.Menu p6)
    {
        int v2;
        if (!(p6 instanceof android.support.v7.internal.view.menu.i)) {
            v2 = 0;
        } else {
            v2 = ((android.support.v7.internal.view.menu.i) p6);
        }
        if ((p4 != 0) || (v2 != 0)) {
            if (v2 != 0) {
                v2.o = 1;
            }
            boolean v0_5 = super.onPreparePanel(p4, p5, p6);
            if (v2 != 0) {
                v2.o = 0;
            }
        } else {
            v0_5 = 0;
        }
        return v0_5;
    }
}
