package android.support.v7.app;
final class s implements android.support.v7.app.l {
    final android.support.v7.widget.Toolbar a;
    final android.graphics.drawable.Drawable b;
    final CharSequence c;

    s(android.support.v7.widget.Toolbar p2)
    {
        this.a = p2;
        this.b = p2.getNavigationIcon();
        this.c = p2.getNavigationContentDescription();
        return;
    }

    public final android.graphics.drawable.Drawable a()
    {
        return this.b;
    }

    public final void a(int p3)
    {
        if (p3 != 0) {
            this.a.setNavigationContentDescription(p3);
        } else {
            this.a.setNavigationContentDescription(this.c);
        }
        return;
    }

    public final void a(android.graphics.drawable.Drawable p2, int p3)
    {
        this.a.setNavigationIcon(p2);
        this.a(p3);
        return;
    }

    public final android.content.Context b()
    {
        return this.a.getContext();
    }

    public final boolean c()
    {
        return 1;
    }
}
