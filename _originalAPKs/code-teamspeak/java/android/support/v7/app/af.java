package android.support.v7.app;
public final class af extends android.support.v7.app.bf implements android.content.DialogInterface {
    static final int a = 0;
    static final int b = 1;
    private android.support.v7.app.v c;

    private af(android.content.Context p3)
    {
        this(p3, android.support.v7.app.af.a(p3, 0), 0);
        return;
    }

    private af(android.content.Context p2, int p3)
    {
        this(p2, p3, 0);
        return;
    }

    af(android.content.Context p4, int p5, byte p6)
    {
        this(p4, android.support.v7.app.af.a(p4, p5));
        this.c = new android.support.v7.app.v(this.getContext(), this, this.getWindow());
        return;
    }

    private af(android.content.Context p3, boolean p4, android.content.DialogInterface$OnCancelListener p5)
    {
        this(p3, android.support.v7.app.af.a(p3, 0));
        this.setCancelable(p4);
        this.setOnCancelListener(p5);
        this.c = new android.support.v7.app.v(p3, this, this.getWindow());
        return;
    }

    static int a(android.content.Context p4, int p5)
    {
        if (p5 < 16777216) {
            android.util.TypedValue v0_2 = new android.util.TypedValue();
            p4.getTheme().resolveAttribute(android.support.v7.a.d.alertDialogTheme, v0_2, 1);
            p5 = v0_2.resourceId;
        }
        return p5;
    }

    static synthetic android.support.v7.app.v a(android.support.v7.app.af p1)
    {
        return p1.c;
    }

    private android.widget.Button a(int p2)
    {
        android.widget.Button v0_1;
        android.widget.Button v0_0 = this.c;
        switch (p2) {
            case -3:
                v0_1 = v0_0.t;
                break;
            case -2:
                v0_1 = v0_0.q;
                break;
            case -1:
                v0_1 = v0_0.n;
                break;
            default:
                v0_1 = 0;
        }
        return v0_1;
    }

    private void a(int p3, CharSequence p4, android.content.DialogInterface$OnClickListener p5)
    {
        this.c.a(p3, p4, p5, 0);
        return;
    }

    private void a(int p3, CharSequence p4, android.os.Message p5)
    {
        this.c.a(p3, p4, 0, p5);
        return;
    }

    private void a(android.graphics.drawable.Drawable p2)
    {
        this.c.a(p2);
        return;
    }

    private void a(android.view.View p2)
    {
        this.c.C = p2;
        return;
    }

    private void a(android.view.View p7, int p8, int p9, int p10, int p11)
    {
        this.c.a(p7, p8, p9, p10, p11);
        return;
    }

    private void a(CharSequence p2)
    {
        this.c.b(p2);
        return;
    }

    private android.widget.ListView b()
    {
        return this.c.f;
    }

    private void b(int p2)
    {
        this.c.L = p2;
        return;
    }

    private void b(android.view.View p2)
    {
        this.c.b(p2);
        return;
    }

    private void c(int p2)
    {
        this.c.a(p2);
        return;
    }

    private void d(int p4)
    {
        int v0_1 = new android.util.TypedValue();
        this.getContext().getTheme().resolveAttribute(p4, v0_1, 1);
        this.c.a(v0_1.resourceId);
        return;
    }

    protected final void onCreate(android.os.Bundle p14)
    {
        android.content.res.TypedArray v0_3;
        int v2_0 = 0;
        super.onCreate(p14);
        android.support.v7.app.v v5 = this.c;
        v5.b.a();
        if ((v5.G == 0) || (v5.L != 1)) {
            v0_3 = v5.F;
        } else {
            v0_3 = v5.G;
        }
        v5.b.setContentView(v0_3);
        android.content.res.TypedArray v0_6 = ((android.view.ViewGroup) v5.c.findViewById(android.support.v7.a.i.contentPanel));
        v5.w = ((android.widget.ScrollView) v5.c.findViewById(android.support.v7.a.i.scrollView));
        v5.w.setFocusable(0);
        v5.B = ((android.widget.TextView) v5.c.findViewById(16908299));
        if (v5.B != null) {
            if (v5.e == null) {
                v5.B.setVisibility(8);
                v5.w.removeView(v5.B);
                if (v5.f == null) {
                    v0_6.setVisibility(8);
                } else {
                    android.content.res.TypedArray v0_9 = ((android.view.ViewGroup) v5.w.getParent());
                    int v1_15 = v0_9.indexOfChild(v5.w);
                    v0_9.removeViewAt(v1_15);
                    v0_9.addView(v5.f, v1_15, new android.view.ViewGroup$LayoutParams(-1, -1));
                }
            } else {
                v5.B.setText(v5.e);
            }
        }
        int v1_20;
        v5.n = ((android.widget.Button) v5.c.findViewById(16908313));
        v5.n.setOnClickListener(v5.N);
        if (!android.text.TextUtils.isEmpty(v5.o)) {
            v5.n.setText(v5.o);
            v5.n.setVisibility(0);
            v1_20 = 1;
        } else {
            v5.n.setVisibility(8);
            v1_20 = 0;
        }
        v5.q = ((android.widget.Button) v5.c.findViewById(16908314));
        v5.q.setOnClickListener(v5.N);
        if (!android.text.TextUtils.isEmpty(v5.r)) {
            v5.q.setText(v5.r);
            v5.q.setVisibility(0);
            v1_20 |= 2;
        } else {
            v5.q.setVisibility(8);
        }
        v5.t = ((android.widget.Button) v5.c.findViewById(16908315));
        v5.t.setOnClickListener(v5.N);
        if (!android.text.TextUtils.isEmpty(v5.u)) {
            v5.t.setText(v5.u);
            v5.t.setVisibility(0);
            v1_20 |= 4;
        } else {
            v5.t.setVisibility(8);
        }
        android.content.res.TypedArray v0_41;
        android.content.res.TypedArray v0_38 = v5.a;
        int v4_11 = new android.util.TypedValue();
        v0_38.getTheme().resolveAttribute(android.support.v7.a.d.alertDialogCenterButtons, v4_11, 1);
        if (v4_11.data == 0) {
            v0_41 = 0;
        } else {
            v0_41 = 1;
        }
        if (v0_41 != null) {
            if (v1_20 != 1) {
                if (v1_20 != 2) {
                    if (v1_20 == 4) {
                        android.support.v7.app.v.a(v5.t);
                    }
                } else {
                    android.support.v7.app.v.a(v5.q);
                }
            } else {
                android.support.v7.app.v.a(v5.n);
            }
        }
        int v4_12;
        if (v1_20 == 0) {
            v4_12 = 0;
        } else {
            v4_12 = 1;
        }
        android.content.res.TypedArray v0_49 = ((android.view.ViewGroup) v5.c.findViewById(android.support.v7.a.i.topPanel));
        android.support.v7.internal.widget.ax v6_4 = android.support.v7.internal.widget.ax.a(v5.a, 0, android.support.v7.a.n.AlertDialog, android.support.v7.a.d.alertDialogStyle);
        if (v5.C == null) {
            int v1_29;
            v5.z = ((android.widget.ImageView) v5.c.findViewById(16908294));
            if (android.text.TextUtils.isEmpty(v5.d)) {
                v1_29 = 0;
            } else {
                v1_29 = 1;
            }
            if (v1_29 == 0) {
                v5.c.findViewById(android.support.v7.a.i.title_template).setVisibility(8);
                v5.z.setVisibility(8);
                v0_49.setVisibility(8);
            } else {
                v5.A = ((android.widget.TextView) v5.c.findViewById(android.support.v7.a.i.alertTitle));
                v5.A.setText(v5.d);
                if (v5.x == 0) {
                    if (v5.y == null) {
                        v5.A.setPadding(v5.z.getPaddingLeft(), v5.z.getPaddingTop(), v5.z.getPaddingRight(), v5.z.getPaddingBottom());
                        v5.z.setVisibility(8);
                    } else {
                        v5.z.setImageDrawable(v5.y);
                    }
                } else {
                    v5.z.setImageResource(v5.x);
                }
            }
        } else {
            v0_49.addView(v5.C, 0, new android.view.ViewGroup$LayoutParams(-1, -2));
            v5.c.findViewById(android.support.v7.a.i.title_template).setVisibility(8);
        }
        android.content.res.TypedArray v0_63 = v5.c.findViewById(android.support.v7.a.i.buttonPanel);
        if (v4_12 == 0) {
            v0_63.setVisibility(8);
            android.content.res.TypedArray v0_65 = v5.c.findViewById(android.support.v7.a.i.textSpacerNoButtons);
            if (v0_65 != null) {
                v0_65.setVisibility(0);
            }
        }
        int v4_13;
        android.content.res.TypedArray v0_68 = ((android.widget.FrameLayout) v5.c.findViewById(android.support.v7.a.i.customPanel));
        if (v5.g == null) {
            if (v5.h == 0) {
                v4_13 = 0;
            } else {
                v4_13 = android.view.LayoutInflater.from(v5.a).inflate(v5.h, v0_68, 0);
            }
        } else {
            v4_13 = v5.g;
        }
        if (v4_13 != 0) {
            v2_0 = 1;
        }
        if ((v2_0 == 0) || (!android.support.v7.app.v.a(v4_13))) {
            v5.c.setFlags(131072, 131072);
        }
        if (v2_0 == 0) {
            v0_68.setVisibility(8);
        } else {
            int v1_56 = ((android.widget.FrameLayout) v5.c.findViewById(android.support.v7.a.i.custom));
            v1_56.addView(v4_13, new android.view.ViewGroup$LayoutParams(-1, -1));
            if (v5.m) {
                v1_56.setPadding(v5.i, v5.j, v5.k, v5.l);
            }
            if (v5.f != null) {
                ((android.widget.LinearLayout$LayoutParams) v0_68.getLayoutParams()).weight = 0;
            }
        }
        android.content.res.TypedArray v0_71 = v5.f;
        if ((v0_71 != null) && (v5.D != null)) {
            v0_71.setAdapter(v5.D);
            int v1_61 = v5.E;
            if (v1_61 >= 0) {
                v0_71.setItemChecked(v1_61, 1);
                v0_71.setSelection(v1_61);
            }
        }
        v6_4.a.recycle();
        return;
    }

    public final boolean onKeyDown(int p4, android.view.KeyEvent p5)
    {
        int v1_3;
        boolean v0 = 1;
        int v1_0 = this.c;
        if ((v1_0.w == null) || (!v1_0.w.executeKeyEvent(p5))) {
            v1_3 = 0;
        } else {
            v1_3 = 1;
        }
        if (v1_3 == 0) {
            v0 = super.onKeyDown(p4, p5);
        }
        return v0;
    }

    public final boolean onKeyUp(int p4, android.view.KeyEvent p5)
    {
        int v1_3;
        boolean v0 = 1;
        int v1_0 = this.c;
        if ((v1_0.w == null) || (!v1_0.w.executeKeyEvent(p5))) {
            v1_3 = 0;
        } else {
            v1_3 = 1;
        }
        if (v1_3 == 0) {
            v0 = super.onKeyUp(p4, p5);
        }
        return v0;
    }

    public final void setTitle(CharSequence p2)
    {
        super.setTitle(p2);
        this.c.a(p2);
        return;
    }
}
