package android.support.v7.app;
public final class bh extends android.support.v4.app.dd {

    public bh()
    {
        return;
    }

    static synthetic void a(android.app.Notification p18, android.support.v4.app.dm p19)
    {
        if ((p19.m instanceof android.support.v7.app.bn)) {
            int v12_4;
            int v12_1 = ((android.support.v7.app.bn) p19.m);
            int v2_3 = p19.a;
            int v3_0 = p19.b;
            int v4_0 = p19.c;
            android.widget.RemoteViews v5_0 = p19.h;
            int v6_0 = p19.i;
            android.graphics.Bitmap v7 = p19.g;
            CharSequence v8 = p19.n;
            boolean v9 = p19.l;
            long v10_1 = p19.B.when;
            java.util.ArrayList v14 = p19.u;
            boolean v15 = v12_1.c;
            android.app.PendingIntent v16 = v12_1.h;
            int v17 = Math.min(v14.size(), 5);
            if (v17 > 3) {
                v12_4 = android.support.v7.a.k.notification_template_big_media;
            } else {
                v12_4 = android.support.v7.a.k.notification_template_big_media_narrow;
            }
            android.widget.RemoteViews v5_1 = android.support.v7.internal.a.d.a(v2_3, v3_0, v4_0, v5_0, v6_0, v7, v8, v9, v10_1, v12_4, 0);
            v5_1.removeAllViews(android.support.v7.a.i.media_actions);
            if (v17 > 0) {
                int v4_1 = 0;
                while (v4_1 < v17) {
                    v5_1.addView(android.support.v7.a.i.media_actions, android.support.v7.internal.a.d.a(v2_3, ((android.support.v4.app.ek) v14.get(v4_1))));
                    v4_1++;
                }
            }
            if (!v15) {
                v5_1.setViewVisibility(android.support.v7.a.i.cancel_action, 8);
            } else {
                v5_1.setViewVisibility(android.support.v7.a.i.cancel_action, 0);
                v5_1.setInt(android.support.v7.a.i.cancel_action, "setAlpha", v2_3.getResources().getInteger(android.support.v7.a.j.cancel_button_image_alpha));
                v5_1.setOnClickPendingIntent(android.support.v7.a.i.cancel_action, v16);
            }
            p18.bigContentView = v5_1;
            if (v15) {
                p18.flags = (p18.flags | 2);
            }
        }
        return;
    }

    static synthetic void a(android.support.v4.app.dc p18, android.support.v4.app.dm p19)
    {
        if ((p19.m instanceof android.support.v7.app.bn)) {
            Object[] v4_2;
            int v12_1 = ((android.support.v7.app.bn) p19.m);
            android.app.Notification$Builder v2_3 = p19.a;
            java.util.ArrayList v14 = p19.u;
            int[] v15 = v12_1.a;
            boolean v16 = v12_1.c;
            android.app.PendingIntent v17 = v12_1.h;
            Integer v6_1 = android.support.v7.internal.a.d.a(v2_3, p19.b, p19.c, p19.h, p19.i, p19.g, p19.n, p19.l, p19.B.when, android.support.v7.a.k.notification_template_media, 1);
            int v7_1 = v14.size();
            if (v15 != null) {
                v4_2 = Math.min(v15.length, 3);
            } else {
                v4_2 = 0;
            }
            v6_1.removeAllViews(android.support.v7.a.i.media_actions);
            if (v4_2 > null) {
                int v5_1 = 0;
                while (v5_1 < v4_2) {
                    if (v5_1 < v7_1) {
                        v6_1.addView(android.support.v7.a.i.media_actions, android.support.v7.internal.a.d.a(v2_3, ((android.support.v4.app.ek) v14.get(v15[v5_1]))));
                        v5_1++;
                    } else {
                        Object[] v4_7 = new Object[2];
                        v4_7[0] = Integer.valueOf(v5_1);
                        v4_7[1] = Integer.valueOf((v7_1 - 1));
                        throw new IllegalArgumentException(String.format("setShowActionsInCompactView: action %d out of bounds (max %d)", v4_7));
                    }
                    return;
                }
            }
            if (!v16) {
                v6_1.setViewVisibility(android.support.v7.a.i.end_padder, 0);
                v6_1.setViewVisibility(android.support.v7.a.i.cancel_action, 8);
            } else {
                v6_1.setViewVisibility(android.support.v7.a.i.end_padder, 8);
                v6_1.setViewVisibility(android.support.v7.a.i.cancel_action, 0);
                v6_1.setOnClickPendingIntent(android.support.v7.a.i.cancel_action, v17);
                v6_1.setInt(android.support.v7.a.i.cancel_action, "setAlpha", v2_3.getResources().getInteger(android.support.v7.a.j.cancel_button_image_alpha));
            }
            p18.a().setContent(v6_1);
            if (v16) {
                p18.a().setOngoing(1);
            }
        }
        return;
    }

    private static void b(android.app.Notification p18, android.support.v4.app.dm p19)
    {
        if ((p19.m instanceof android.support.v7.app.bn)) {
            int v12_4;
            int v12_1 = ((android.support.v7.app.bn) p19.m);
            int v2_3 = p19.a;
            int v3_0 = p19.b;
            int v4_0 = p19.c;
            android.widget.RemoteViews v5_0 = p19.h;
            int v6_0 = p19.i;
            android.graphics.Bitmap v7 = p19.g;
            CharSequence v8 = p19.n;
            boolean v9 = p19.l;
            long v10_1 = p19.B.when;
            java.util.ArrayList v14 = p19.u;
            boolean v15 = v12_1.c;
            android.app.PendingIntent v16 = v12_1.h;
            int v17 = Math.min(v14.size(), 5);
            if (v17 > 3) {
                v12_4 = android.support.v7.a.k.notification_template_big_media;
            } else {
                v12_4 = android.support.v7.a.k.notification_template_big_media_narrow;
            }
            android.widget.RemoteViews v5_1 = android.support.v7.internal.a.d.a(v2_3, v3_0, v4_0, v5_0, v6_0, v7, v8, v9, v10_1, v12_4, 0);
            v5_1.removeAllViews(android.support.v7.a.i.media_actions);
            if (v17 > 0) {
                int v4_1 = 0;
                while (v4_1 < v17) {
                    v5_1.addView(android.support.v7.a.i.media_actions, android.support.v7.internal.a.d.a(v2_3, ((android.support.v4.app.ek) v14.get(v4_1))));
                    v4_1++;
                }
            }
            if (!v15) {
                v5_1.setViewVisibility(android.support.v7.a.i.cancel_action, 8);
            } else {
                v5_1.setViewVisibility(android.support.v7.a.i.cancel_action, 0);
                v5_1.setInt(android.support.v7.a.i.cancel_action, "setAlpha", v2_3.getResources().getInteger(android.support.v7.a.j.cancel_button_image_alpha));
                v5_1.setOnClickPendingIntent(android.support.v7.a.i.cancel_action, v16);
            }
            p18.bigContentView = v5_1;
            if (v15) {
                p18.flags = (p18.flags | 2);
            }
        }
        return;
    }

    private static void b(android.support.v4.app.dc p18, android.support.v4.app.dm p19)
    {
        if ((p19.m instanceof android.support.v7.app.bn)) {
            Object[] v4_2;
            int v12_1 = ((android.support.v7.app.bn) p19.m);
            android.app.Notification$Builder v2_3 = p19.a;
            java.util.ArrayList v14 = p19.u;
            int[] v15 = v12_1.a;
            boolean v16 = v12_1.c;
            android.app.PendingIntent v17 = v12_1.h;
            Integer v6_1 = android.support.v7.internal.a.d.a(v2_3, p19.b, p19.c, p19.h, p19.i, p19.g, p19.n, p19.l, p19.B.when, android.support.v7.a.k.notification_template_media, 1);
            int v7_1 = v14.size();
            if (v15 != null) {
                v4_2 = Math.min(v15.length, 3);
            } else {
                v4_2 = 0;
            }
            v6_1.removeAllViews(android.support.v7.a.i.media_actions);
            if (v4_2 > null) {
                int v5_1 = 0;
                while (v5_1 < v4_2) {
                    if (v5_1 < v7_1) {
                        v6_1.addView(android.support.v7.a.i.media_actions, android.support.v7.internal.a.d.a(v2_3, ((android.support.v4.app.ek) v14.get(v15[v5_1]))));
                        v5_1++;
                    } else {
                        Object[] v4_7 = new Object[2];
                        v4_7[0] = Integer.valueOf(v5_1);
                        v4_7[1] = Integer.valueOf((v7_1 - 1));
                        throw new IllegalArgumentException(String.format("setShowActionsInCompactView: action %d out of bounds (max %d)", v4_7));
                    }
                    return;
                }
            }
            if (!v16) {
                v6_1.setViewVisibility(android.support.v7.a.i.end_padder, 0);
                v6_1.setViewVisibility(android.support.v7.a.i.cancel_action, 8);
            } else {
                v6_1.setViewVisibility(android.support.v7.a.i.end_padder, 8);
                v6_1.setViewVisibility(android.support.v7.a.i.cancel_action, 0);
                v6_1.setOnClickPendingIntent(android.support.v7.a.i.cancel_action, v17);
                v6_1.setInt(android.support.v7.a.i.cancel_action, "setAlpha", v2_3.getResources().getInteger(android.support.v7.a.j.cancel_button_image_alpha));
            }
            p18.a().setContent(v6_1);
            if (v16) {
                p18.a().setOngoing(1);
            }
        }
        return;
    }

    static synthetic void b(android.support.v4.app.dc p4, android.support.v4.app.ed p5)
    {
        if ((p5 instanceof android.support.v7.app.bn)) {
            android.media.session.MediaSession$Token v0_2;
            int[] v1 = ((android.support.v7.app.bn) p5).a;
            if (((android.support.v7.app.bn) p5).b == null) {
                v0_2 = 0;
            } else {
                v0_2 = ((android.support.v7.app.bn) p5).b.a;
            }
            android.app.Notification$MediaStyle v2_1 = new android.app.Notification$MediaStyle(p4.a());
            if (v1 != null) {
                v2_1.setShowActionsInCompactView(v1);
            }
            if (v0_2 != null) {
                v2_1.setMediaSession(((android.media.session.MediaSession$Token) v0_2));
            }
        }
        return;
    }

    private static void c(android.support.v4.app.dc p4, android.support.v4.app.ed p5)
    {
        if ((p5 instanceof android.support.v7.app.bn)) {
            android.media.session.MediaSession$Token v0_2;
            int[] v1 = ((android.support.v7.app.bn) p5).a;
            if (((android.support.v7.app.bn) p5).b == null) {
                v0_2 = 0;
            } else {
                v0_2 = ((android.support.v7.app.bn) p5).b.a;
            }
            android.app.Notification$MediaStyle v2_1 = new android.app.Notification$MediaStyle(p4.a());
            if (v1 != null) {
                v2_1.setShowActionsInCompactView(v1);
            }
            if (v0_2 != null) {
                v2_1.setMediaSession(((android.media.session.MediaSession$Token) v0_2));
            }
        }
        return;
    }
}
