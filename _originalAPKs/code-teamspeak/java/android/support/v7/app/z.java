package android.support.v7.app;
final class z extends android.widget.CursorAdapter {
    final synthetic android.widget.ListView a;
    final synthetic android.support.v7.app.v b;
    final synthetic android.support.v7.app.x c;
    private final int d;
    private final int e;

    z(android.support.v7.app.x p3, android.content.Context p4, android.database.Cursor p5, android.widget.ListView p6, android.support.v7.app.v p7)
    {
        this.c = p3;
        this.a = p6;
        this.b = p7;
        this(p4, p5, 0);
        int v0_1 = this.getCursor();
        this.d = v0_1.getColumnIndexOrThrow(this.c.I);
        this.e = v0_1.getColumnIndexOrThrow(this.c.J);
        return;
    }

    public final void bindView(android.view.View p5, android.content.Context p6, android.database.Cursor p7)
    {
        int v0_5;
        ((android.widget.CheckedTextView) p5.findViewById(16908308)).setText(p7.getString(this.d));
        android.widget.ListView v2_2 = this.a;
        int v3 = p7.getPosition();
        if (p7.getInt(this.e) != 1) {
            v0_5 = 0;
        } else {
            v0_5 = 1;
        }
        v2_2.setItemChecked(v3, v0_5);
        return;
    }

    public final android.view.View newView(android.content.Context p4, android.database.Cursor p5, android.view.ViewGroup p6)
    {
        return this.c.b.inflate(this.b.I, p6, 0);
    }
}
