package android.support.v7.widget;
public final class r extends android.widget.Button implements android.support.v4.view.cr {
    private final android.support.v7.internal.widget.av a;
    private final android.support.v7.widget.q b;
    private final android.support.v7.widget.ah c;

    private r(android.content.Context p2)
    {
        this(p2, 0);
        return;
    }

    public r(android.content.Context p2, android.util.AttributeSet p3)
    {
        this(p2, p3, android.support.v7.a.d.buttonStyle);
        return;
    }

    private r(android.content.Context p3, android.util.AttributeSet p4, int p5)
    {
        this(p3, p4, p5);
        this.a = android.support.v7.internal.widget.av.a(this.getContext());
        this.b = new android.support.v7.widget.q(this, this.a);
        this.b.a(p4, p5);
        this.c = new android.support.v7.widget.ah(this);
        this.c.a(p4, p5);
        return;
    }

    protected final void drawableStateChanged()
    {
        super.drawableStateChanged();
        if (this.b != null) {
            this.b.c();
        }
        return;
    }

    public final android.content.res.ColorStateList getSupportBackgroundTintList()
    {
        int v0_1;
        if (this.b == null) {
            v0_1 = 0;
        } else {
            v0_1 = this.b.a();
        }
        return v0_1;
    }

    public final android.graphics.PorterDuff$Mode getSupportBackgroundTintMode()
    {
        int v0_1;
        if (this.b == null) {
            v0_1 = 0;
        } else {
            v0_1 = this.b.b();
        }
        return v0_1;
    }

    public final void onInitializeAccessibilityEvent(android.view.accessibility.AccessibilityEvent p2)
    {
        super.onInitializeAccessibilityEvent(p2);
        p2.setClassName(android.widget.Button.getName());
        return;
    }

    public final void onInitializeAccessibilityNodeInfo(android.view.accessibility.AccessibilityNodeInfo p2)
    {
        super.onInitializeAccessibilityNodeInfo(p2);
        p2.setClassName(android.widget.Button.getName());
        return;
    }

    public final void setBackgroundDrawable(android.graphics.drawable.Drawable p3)
    {
        super.setBackgroundDrawable(p3);
        if (this.b != null) {
            this.b.b(0);
        }
        return;
    }

    public final void setBackgroundResource(int p2)
    {
        super.setBackgroundResource(p2);
        if (this.b != null) {
            this.b.a(p2);
        }
        return;
    }

    public final void setSupportAllCaps(boolean p2)
    {
        if (this.c != null) {
            this.c.a(p2);
        }
        return;
    }

    public final void setSupportBackgroundTintList(android.content.res.ColorStateList p2)
    {
        if (this.b != null) {
            this.b.a(p2);
        }
        return;
    }

    public final void setSupportBackgroundTintMode(android.graphics.PorterDuff$Mode p2)
    {
        if (this.b != null) {
            this.b.a(p2);
        }
        return;
    }

    public final void setTextAppearance(android.content.Context p2, int p3)
    {
        super.setTextAppearance(p2, p3);
        if (this.c != null) {
            this.c.a(p2, p3);
        }
        return;
    }
}
