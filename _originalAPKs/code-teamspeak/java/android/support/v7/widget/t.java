package android.support.v7.widget;
public final class t extends android.widget.CheckedTextView {
    private static final int[] a;
    private android.support.v7.internal.widget.av b;

    static t()
    {
        int[] v0_1 = new int[1];
        v0_1[0] = 16843016;
        android.support.v7.widget.t.a = v0_1;
        return;
    }

    private t(android.content.Context p2)
    {
        this(p2, 0);
        return;
    }

    public t(android.content.Context p2, android.util.AttributeSet p3)
    {
        this(p2, p3, 0);
        return;
    }

    private t(android.content.Context p4, android.util.AttributeSet p5, byte p6)
    {
        this(p4, p5, 16843720);
        if (android.support.v7.internal.widget.av.a) {
            android.support.v7.internal.widget.av v0_2 = android.support.v7.internal.widget.ax.a(this.getContext(), p5, android.support.v7.widget.t.a, 16843720);
            this.setCheckMarkDrawable(v0_2.a(0));
            v0_2.a.recycle();
            this.b = v0_2.a();
        }
        return;
    }

    public final void setCheckMarkDrawable(int p3)
    {
        if (this.b == null) {
            super.setCheckMarkDrawable(p3);
        } else {
            this.setCheckMarkDrawable(this.b.a(p3, 0));
        }
        return;
    }
}
