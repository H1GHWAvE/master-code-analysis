package android.support.v7.widget;
final class ax implements android.widget.AbsListView$OnScrollListener {
    final synthetic android.support.v7.widget.an a;

    private ax(android.support.v7.widget.an p1)
    {
        this.a = p1;
        return;
    }

    synthetic ax(android.support.v7.widget.an p1, byte p2)
    {
        this(p1);
        return;
    }

    public final void onScroll(android.widget.AbsListView p1, int p2, int p3, int p4)
    {
        return;
    }

    public final void onScrollStateChanged(android.widget.AbsListView p3, int p4)
    {
        if ((p4 == 1) && ((!this.a.g()) && (android.support.v7.widget.an.b(this.a).getContentView() != null))) {
            android.support.v7.widget.an.d(this.a).removeCallbacks(android.support.v7.widget.an.c(this.a));
            android.support.v7.widget.an.c(this.a).run();
        }
        return;
    }
}
