package android.support.v7.widget;
final class ay implements android.view.View$OnTouchListener {
    final synthetic android.support.v7.widget.an a;

    private ay(android.support.v7.widget.an p1)
    {
        this.a = p1;
        return;
    }

    synthetic ay(android.support.v7.widget.an p1, byte p2)
    {
        this(p1);
        return;
    }

    public final boolean onTouch(android.view.View p5, android.view.MotionEvent p6)
    {
        android.os.Handler v0_0 = p6.getAction();
        android.support.v7.widget.az v1_1 = ((int) p6.getX());
        long v2_1 = ((int) p6.getY());
        if ((v0_0 != null) || ((android.support.v7.widget.an.b(this.a) == null) || ((!android.support.v7.widget.an.b(this.a).isShowing()) || ((v1_1 < null) || ((v1_1 >= android.support.v7.widget.an.b(this.a).getWidth()) || ((v2_1 < 0) || (v2_1 >= android.support.v7.widget.an.b(this.a).getHeight()))))))) {
            if (v0_0 == 1) {
                android.support.v7.widget.an.d(this.a).removeCallbacks(android.support.v7.widget.an.c(this.a));
            }
        } else {
            android.support.v7.widget.an.d(this.a).postDelayed(android.support.v7.widget.an.c(this.a), 250);
        }
        return 0;
    }
}
