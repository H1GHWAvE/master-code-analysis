package android.support.v7.widget;
final class ar extends android.support.v7.internal.widget.ah {
    private boolean h;
    private boolean i;
    private boolean j;
    private android.support.v4.view.fk k;
    private android.support.v4.widget.ba l;

    public ar(android.content.Context p2, boolean p3)
    {
        this(p2, android.support.v7.a.d.dropDownListViewStyle);
        this.i = p3;
        this.setCacheColorHint(0);
        return;
    }

    private void a(android.view.View p3, int p4)
    {
        this.performItemClick(p3, p4, this.getItemIdAtPosition(p4));
        return;
    }

    private void a(android.view.View p2, int p3, float p4, float p5)
    {
        this.j = 1;
        this.setPressed(1);
        this.layoutChildren();
        this.setSelection(p3);
        this.a(p3, p2, p4, p5);
        this.setSelectorEnabled(0);
        this.refreshDrawableState();
        return;
    }

    static synthetic boolean a(android.support.v7.widget.ar p0, boolean p1)
    {
        p0.h = p1;
        return p1;
    }

    private void b()
    {
        this.j = 0;
        this.setPressed(0);
        this.drawableStateChanged();
        if (this.k != null) {
            this.k.a();
            this.k = 0;
        }
        return;
    }

    protected final boolean a()
    {
        if ((!this.j) && (!super.a())) {
            int v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    public final boolean a(android.view.MotionEvent p9, int p10)
    {
        int v3_1;
        android.support.v4.widget.ba v0_1;
        android.support.v4.widget.ba v0_0;
        int v3_0 = android.support.v4.view.bk.a(p9);
        switch (v3_0) {
            case 1:
                v0_1 = 0;
                long v4_0 = p9.findPointerIndex(p10);
                if (v4_0 >= 0) {
                    float v5_1 = ((int) p9.getX(v4_0));
                    long v4_2 = ((int) p9.getY(v4_0));
                    int v6 = this.pointToPosition(v5_1, v4_2);
                    if (v6 != -1) {
                        android.support.v4.widget.ba v0_4 = this.getChildAt((v6 - this.getFirstVisiblePosition()));
                        float v5_2 = ((float) v5_1);
                        long v4_3 = ((float) v4_2);
                        this.j = 1;
                        this.setPressed(1);
                        this.layoutChildren();
                        this.setSelection(v6);
                        this.a(v6, v0_4, v5_2, v4_3);
                        this.setSelectorEnabled(0);
                        this.refreshDrawableState();
                        if (v3_0 != 1) {
                            v0_0 = 0;
                            v3_1 = 1;
                        } else {
                            this.performItemClick(v0_4, v6, this.getItemIdAtPosition(v6));
                        }
                    } else {
                        v3_1 = v0_1;
                        v0_0 = 1;
                    }
                } else {
                    v0_0 = 0;
                    v3_1 = 0;
                }
                break;
            case 2:
                v0_1 = 1;
                break;
            case 3:
                v0_0 = 0;
                v3_1 = 0;
                break;
            default:
        }
        if ((v3_1 == 0) || (v0_0 != null)) {
            this.j = 0;
            this.setPressed(0);
            this.drawableStateChanged();
            if (this.k != null) {
                this.k.a();
                this.k = 0;
            }
        }
        if (v3_1 == 0) {
            if (this.l != null) {
                this.l.a(0);
            }
        } else {
            if (this.l == null) {
                this.l = new android.support.v4.widget.ba(this);
            }
            this.l.a(1);
            this.l.onTouch(this, p9);
        }
        return v3_1;
    }

    public final boolean hasFocus()
    {
        if ((!this.i) && (!super.hasFocus())) {
            int v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    public final boolean hasWindowFocus()
    {
        if ((!this.i) && (!super.hasWindowFocus())) {
            int v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    public final boolean isFocused()
    {
        if ((!this.i) && (!super.isFocused())) {
            int v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    public final boolean isInTouchMode()
    {
        if (((!this.i) || (!this.h)) && (!super.isInTouchMode())) {
            int v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        return v0_3;
    }
}
