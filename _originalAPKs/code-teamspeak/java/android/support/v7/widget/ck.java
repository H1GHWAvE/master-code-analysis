package android.support.v7.widget;
public final class ck extends android.support.v7.app.c {
    static final int b = 0;
    static final int c = 1;
    static final int d = 2;
    int e;

    public ck()
    {
        this(-2);
        this.e = 0;
        this.a = 8388627;
        return;
    }

    private ck(int p2)
    {
        this(-1, p2);
        return;
    }

    public ck(int p2, int p3)
    {
        this(p2);
        this.e = 0;
        this.a = p3;
        return;
    }

    public ck(android.content.Context p2, android.util.AttributeSet p3)
    {
        this(p2, p3);
        this.e = 0;
        return;
    }

    public ck(android.support.v7.app.c p2)
    {
        this(p2);
        this.e = 0;
        return;
    }

    public ck(android.support.v7.widget.ck p2)
    {
        this(p2);
        this.e = 0;
        this.e = p2.e;
        return;
    }

    public ck(android.view.ViewGroup$LayoutParams p2)
    {
        this(p2);
        this.e = 0;
        return;
    }

    public ck(android.view.ViewGroup$MarginLayoutParams p2)
    {
        this(p2);
        this.e = 0;
        this.leftMargin = p2.leftMargin;
        this.topMargin = p2.topMargin;
        this.rightMargin = p2.rightMargin;
        this.bottomMargin = p2.bottomMargin;
        return;
    }

    private void a(android.view.ViewGroup$MarginLayoutParams p2)
    {
        this.leftMargin = p2.leftMargin;
        this.topMargin = p2.topMargin;
        this.rightMargin = p2.rightMargin;
        this.bottomMargin = p2.bottomMargin;
        return;
    }
}
