package android.support.v7.widget;
final class i implements android.os.Parcelable$Creator {

    i()
    {
        return;
    }

    private static android.support.v7.widget.ActionMenuPresenter$SavedState a(android.os.Parcel p1)
    {
        return new android.support.v7.widget.ActionMenuPresenter$SavedState(p1);
    }

    private static android.support.v7.widget.ActionMenuPresenter$SavedState[] a(int p1)
    {
        android.support.v7.widget.ActionMenuPresenter$SavedState[] v0 = new android.support.v7.widget.ActionMenuPresenter$SavedState[p1];
        return v0;
    }

    public final synthetic Object createFromParcel(android.os.Parcel p2)
    {
        return new android.support.v7.widget.ActionMenuPresenter$SavedState(p2);
    }

    public final bridge synthetic Object[] newArray(int p2)
    {
        android.support.v7.widget.ActionMenuPresenter$SavedState[] v0 = new android.support.v7.widget.ActionMenuPresenter$SavedState[p2];
        return v0;
    }
}
