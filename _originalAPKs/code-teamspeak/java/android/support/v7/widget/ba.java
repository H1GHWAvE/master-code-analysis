package android.support.v7.widget;
public final class ba implements android.support.v7.internal.view.menu.j, android.support.v7.internal.view.menu.y {
    android.support.v7.internal.view.menu.v a;
    private android.content.Context b;
    private android.support.v7.internal.view.menu.i c;
    private android.view.View d;
    private android.support.v7.widget.bd e;
    private android.support.v7.widget.bc f;
    private android.view.View$OnTouchListener g;

    private ba(android.content.Context p2, android.view.View p3)
    {
        this(p2, p3, 0);
        return;
    }

    private ba(android.content.Context p2, android.view.View p3, byte p4)
    {
        this(p2, p3, android.support.v7.a.d.popupMenuStyle);
        return;
    }

    private ba(android.content.Context p8, android.view.View p9, int p10)
    {
        this.b = p8;
        this.c = new android.support.v7.internal.view.menu.i(p8);
        this.c.a(this);
        this.d = p9;
        this.a = new android.support.v7.internal.view.menu.v(p8, this.c, p9, 0, p10, 0);
        this.a.f = 0;
        this.a.d = this;
        return;
    }

    private int a()
    {
        return this.a.f;
    }

    private static synthetic android.support.v7.internal.view.menu.v a(android.support.v7.widget.ba p1)
    {
        return p1.a;
    }

    private void a(int p2)
    {
        this.a.f = p2;
        return;
    }

    private void a(android.support.v7.widget.bc p1)
    {
        this.f = p1;
        return;
    }

    private void a(android.support.v7.widget.bd p1)
    {
        this.e = p1;
        return;
    }

    private android.view.View$OnTouchListener b()
    {
        if (this.g == null) {
            this.g = new android.support.v7.widget.bb(this, this.d);
        }
        return this.g;
    }

    private void b(int p3)
    {
        new android.support.v7.internal.view.f(this.b).inflate(p3, this.c);
        return;
    }

    private android.view.Menu c()
    {
        return this.c;
    }

    private android.view.MenuInflater d()
    {
        return new android.support.v7.internal.view.f(this.b);
    }

    private void e()
    {
        this.a.d();
        return;
    }

    private void f()
    {
        this.a.f();
        return;
    }

    private static void g()
    {
        return;
    }

    public final void a(android.support.v7.internal.view.menu.i p1)
    {
        return;
    }

    public final void a(android.support.v7.internal.view.menu.i p1, boolean p2)
    {
        return;
    }

    public final boolean a(android.support.v7.internal.view.menu.i p2, android.view.MenuItem p3)
    {
        int v0_1;
        if (this.e == null) {
            v0_1 = 0;
        } else {
            v0_1 = this.e.a();
        }
        return v0_1;
    }

    public final boolean a_(android.support.v7.internal.view.menu.i p5)
    {
        int v0 = 1;
        if (p5 != null) {
            if (p5.hasVisibleItems()) {
                new android.support.v7.internal.view.menu.v(this.b, p5, this.d).d();
            }
        } else {
            v0 = 0;
        }
        return v0;
    }
}
