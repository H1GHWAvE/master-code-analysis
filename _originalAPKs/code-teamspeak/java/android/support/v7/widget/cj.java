package android.support.v7.widget;
public final class cj implements android.support.v7.internal.view.menu.x {
    android.support.v7.internal.view.menu.i a;
    public android.support.v7.internal.view.menu.m b;
    final synthetic android.support.v7.widget.Toolbar c;

    private cj(android.support.v7.widget.Toolbar p1)
    {
        this.c = p1;
        return;
    }

    public synthetic cj(android.support.v7.widget.Toolbar p1, byte p2)
    {
        this(p1);
        return;
    }

    public final android.support.v7.internal.view.menu.z a(android.view.ViewGroup p2)
    {
        return 0;
    }

    public final void a(android.content.Context p3, android.support.v7.internal.view.menu.i p4)
    {
        if ((this.a != null) && (this.b != null)) {
            this.a.b(this.b);
        }
        this.a = p4;
        return;
    }

    public final void a(android.os.Parcelable p1)
    {
        return;
    }

    public final void a(android.support.v7.internal.view.menu.i p1, boolean p2)
    {
        return;
    }

    public final void a(android.support.v7.internal.view.menu.y p1)
    {
        return;
    }

    public final void a(boolean p6)
    {
        int v0_0 = 0;
        if (this.b != null) {
            if (this.a != null) {
                int v2 = this.a.size();
                int v1_3 = 0;
                while (v1_3 < v2) {
                    if (this.a.getItem(v1_3) != this.b) {
                        v1_3++;
                    } else {
                        v0_0 = 1;
                        break;
                    }
                }
            }
            if (v0_0 == 0) {
                this.b(this.b);
            }
        }
        return;
    }

    public final boolean a()
    {
        return 0;
    }

    public final boolean a(android.support.v7.internal.view.menu.ad p2)
    {
        return 0;
    }

    public final boolean a(android.support.v7.internal.view.menu.m p7)
    {
        android.support.v7.widget.Toolbar.b(this.c);
        if (android.support.v7.widget.Toolbar.c(this.c).getParent() != this.c) {
            this.c.addView(android.support.v7.widget.Toolbar.c(this.c));
        }
        this.c.d = p7.getActionView();
        this.b = p7;
        if (this.c.d.getParent() != this.c) {
            java.util.ArrayList v0_9 = android.support.v7.widget.Toolbar.e();
            v0_9.a = (8388611 | (android.support.v7.widget.Toolbar.d(this.c) & 112));
            v0_9.e = 2;
            this.c.d.setLayoutParams(v0_9);
            this.c.addView(this.c.d);
        }
        android.support.v7.widget.Toolbar v2_3 = this.c;
        int v1_11 = (v2_3.getChildCount() - 1);
        while (v1_11 >= 0) {
            android.view.View v3 = v2_3.getChildAt(v1_11);
            if ((((android.support.v7.widget.ck) v3.getLayoutParams()).e != 2) && (v3 != v2_3.a)) {
                v2_3.removeViewAt(v1_11);
                v2_3.j.add(v3);
            }
            v1_11--;
        }
        this.c.requestLayout();
        p7.e(1);
        if ((this.c.d instanceof android.support.v7.c.c)) {
            ((android.support.v7.c.c) this.c.d).a();
        }
        return 1;
    }

    public final int b()
    {
        return 0;
    }

    public final boolean b(android.support.v7.internal.view.menu.m p5)
    {
        if ((this.c.d instanceof android.support.v7.c.c)) {
            ((android.support.v7.c.c) this.c.d).b();
        }
        this.c.removeView(this.c.d);
        this.c.removeView(android.support.v7.widget.Toolbar.c(this.c));
        this.c.d = 0;
        android.support.v7.widget.Toolbar v2 = this.c;
        int v1_4 = (v2.j.size() - 1);
        while (v1_4 >= 0) {
            v2.addView(((android.view.View) v2.j.get(v1_4)));
            v1_4--;
        }
        v2.j.clear();
        this.b = 0;
        this.c.requestLayout();
        p5.e(0);
        return 1;
    }

    public final android.os.Parcelable c()
    {
        return 0;
    }
}
