package android.support.v7.widget;
final class b extends android.support.v7.internal.view.menu.v {
    final synthetic android.support.v7.widget.ActionMenuPresenter g;
    private android.support.v7.internal.view.menu.ad h;

    public b(android.support.v7.widget.ActionMenuPresenter p7, android.content.Context p8, android.support.v7.internal.view.menu.ad p9)
    {
        int v4 = 0;
        this.g = p7;
        this(p8, p9, 0, 0, android.support.v7.a.d.actionOverflowMenuStyle);
        this.h = p9;
        if (!((android.support.v7.internal.view.menu.m) p9.getItem()).f()) {
            int v0_5;
            if (p7.i != null) {
                v0_5 = p7.i;
            } else {
                v0_5 = ((android.view.View) p7.g);
            }
            this.b = v0_5;
        }
        this.d = p7.s;
        int v1_1 = p9.size();
        int v0_8 = 0;
        while (v0_8 < v1_1) {
            android.graphics.drawable.Drawable v2_1 = p9.getItem(v0_8);
            if ((!v2_1.isVisible()) || (v2_1.getIcon() == null)) {
                v0_8++;
            } else {
                v4 = 1;
                break;
            }
        }
        this.e = v4;
        return;
    }

    public final void onDismiss()
    {
        super.onDismiss();
        this.g.q = 0;
        this.g.t = 0;
        return;
    }
}
