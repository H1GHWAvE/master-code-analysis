package net.hockeyapp.android;
public final class z {

    public z()
    {
        return;
    }

    private static void a(android.content.Context p2)
    {
        net.hockeyapp.android.z.a("hockeyapp_crash_dialog_title", 0, p2);
        net.hockeyapp.android.z.a("hockeyapp_crash_dialog_message", 1, p2);
        net.hockeyapp.android.z.a("hockeyapp_crash_dialog_negative_button", 2, p2);
        net.hockeyapp.android.z.a("hockeyapp_crash_dialog_neutral_button", 3, p2);
        net.hockeyapp.android.z.a("hockeyapp_crash_dialog_positive_button", 4, p2);
        net.hockeyapp.android.z.a("hockeyapp_download_failed_dialog_title", 256, p2);
        net.hockeyapp.android.z.a("hockeyapp_download_failed_dialog_message", 257, p2);
        net.hockeyapp.android.z.a("hockeyapp_download_failed_dialog_negative_button", 258, p2);
        net.hockeyapp.android.z.a("hockeyapp_download_failed_dialog_positive_button", 259, p2);
        net.hockeyapp.android.z.a("hockeyapp_update_mandatory_toast", 512, p2);
        net.hockeyapp.android.z.a("hockeyapp_update_dialog_title", 513, p2);
        net.hockeyapp.android.z.a("hockeyapp_update_dialog_message", 514, p2);
        net.hockeyapp.android.z.a("hockeyapp_update_dialog_negative_button", 515, p2);
        net.hockeyapp.android.z.a("hockeyapp_update_dialog_positive_button", 516, p2);
        net.hockeyapp.android.z.a("hockeyapp_expiry_info_title", 768, p2);
        net.hockeyapp.android.z.a("hockeyapp_expiry_info_text", 769, p2);
        net.hockeyapp.android.z.a("hockeyapp_feedback_failed_title", 1024, p2);
        net.hockeyapp.android.z.a("hockeyapp_feedback_failed_text", 1025, p2);
        net.hockeyapp.android.z.a("hockeyapp_feedback_name_hint", 1026, p2);
        net.hockeyapp.android.z.a("hockeyapp_feedback_email_hint", 1027, p2);
        net.hockeyapp.android.z.a("hockeyapp_feedback_subject_hint", 1028, p2);
        net.hockeyapp.android.z.a("hockeyapp_feedback_message_hint", 1029, p2);
        net.hockeyapp.android.z.a("hockeyapp_feedback_last_updated_text", 1030, p2);
        net.hockeyapp.android.z.a("hockeyapp_feedback_attachment_button_text", 1031, p2);
        net.hockeyapp.android.z.a("hockeyapp_feedback_send_button_text", 1032, p2);
        net.hockeyapp.android.z.a("hockeyapp_feedback_response_button_text", 1033, p2);
        net.hockeyapp.android.z.a("hockeyapp_feedback_refresh_button_text", 1034, p2);
        net.hockeyapp.android.z.a("hockeyapp_feedback_title", 1035, p2);
        net.hockeyapp.android.z.a("hockeyapp_feedback_send_generic_error", 1036, p2);
        net.hockeyapp.android.z.a("hockeyapp_feedback_send_network_error", 1037, p2);
        net.hockeyapp.android.z.a("hockeyapp_feedback_validate_subject_error", 1038, p2);
        net.hockeyapp.android.z.a("hockeyapp_feedback_validate_email_error", 1039, p2);
        net.hockeyapp.android.z.a("hockeyapp_feedback_validate_email_empty", 1042, p2);
        net.hockeyapp.android.z.a("hockeyapp_feedback_validate_name_error", 1041, p2);
        net.hockeyapp.android.z.a("hockeyapp_feedback_validate_text_error", 1043, p2);
        net.hockeyapp.android.z.a("hockeyapp_feedback_generic_error", 1040, p2);
        net.hockeyapp.android.z.a("hockeyapp_feedback_attach_file", 1044, p2);
        net.hockeyapp.android.z.a("hockeyapp_feedback_attach_picture", 1045, p2);
        net.hockeyapp.android.z.a("hockeyapp_feedback_select_file", 1046, p2);
        net.hockeyapp.android.z.a("hockeyapp_feedback_select_picture", 1047, p2);
        net.hockeyapp.android.z.a("hockeyapp_feedback_max_attachments_allowed", 1048, p2);
        net.hockeyapp.android.z.a("hockeyapp_login_headline_text", 1280, p2);
        net.hockeyapp.android.z.a("hockeyapp_login_missing_credentials_toast", 1281, p2);
        net.hockeyapp.android.z.a("hockeyapp_login_email_hint", 1282, p2);
        net.hockeyapp.android.z.a("hockeyapp_login_password_hint", 1283, p2);
        net.hockeyapp.android.z.a("hockeyapp_login_login_button_text", 1284, p2);
        net.hockeyapp.android.z.a("hockeyapp_paint_indicator_toast", 1536, p2);
        net.hockeyapp.android.z.a("hockeyapp_paint_menu_save", 1537, p2);
        net.hockeyapp.android.z.a("hockeyapp_paint_menu_undo", 1538, p2);
        net.hockeyapp.android.z.a("hockeyapp_paint_menu_clear", 1539, p2);
        net.hockeyapp.android.z.a("hockeyapp_paint_dialog_message", 1540, p2);
        net.hockeyapp.android.z.a("hockeyapp_paint_dialog_negative_button", 1541, p2);
        net.hockeyapp.android.z.a("hockeyapp_paint_dialog_positive_button", 1542, p2);
        net.hockeyapp.android.z.a("hockeyapp_permission_update_title", 1792, p2);
        net.hockeyapp.android.z.a("hockeyapp_permission_update_message", 1793, p2);
        net.hockeyapp.android.z.a("hockeyapp_permission_dialog_negative_button", 1794, p2);
        net.hockeyapp.android.z.a("hockeyapp_permission_dialog_positive_button", 1795, p2);
        net.hockeyapp.android.z.a("hockeyapp_dialog_positive_button", 2048, p2);
        net.hockeyapp.android.z.a("hockeyapp_dialog_negative_button", 2049, p2);
        net.hockeyapp.android.z.a("hockeyapp_dialog_error_title", 2050, p2);
        net.hockeyapp.android.z.a("hockeyapp_dialog_error_message", 2051, p2);
        return;
    }

    private static void a(String p3, int p4, android.content.Context p5)
    {
        String v0_1 = p5.getResources().getIdentifier(p3, "string", p5.getPackageName());
        if (v0_1 != null) {
            String v0_2 = p5.getString(v0_1);
            if (!android.text.TextUtils.isEmpty(v0_2)) {
                net.hockeyapp.android.aj.a(p4, v0_2);
            }
        }
        return;
    }
}
