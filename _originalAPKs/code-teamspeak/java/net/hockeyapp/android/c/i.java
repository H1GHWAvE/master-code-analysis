package net.hockeyapp.android.c;
public final enum class i extends java.lang.Enum {
    public static final enum net.hockeyapp.android.c.i a;
    public static final enum net.hockeyapp.android.c.i b;
    public static final enum net.hockeyapp.android.c.i c;
    private static final synthetic net.hockeyapp.android.c.i[] e;
    private final int d;

    static i()
    {
        net.hockeyapp.android.c.i.a = new net.hockeyapp.android.c.i("DONT_SHOW", 0, 0);
        net.hockeyapp.android.c.i.b = new net.hockeyapp.android.c.i("OPTIONAL", 1, 1);
        net.hockeyapp.android.c.i.c = new net.hockeyapp.android.c.i("REQUIRED", 2, 2);
        net.hockeyapp.android.c.i[] v0_7 = new net.hockeyapp.android.c.i[3];
        v0_7[0] = net.hockeyapp.android.c.i.a;
        v0_7[1] = net.hockeyapp.android.c.i.b;
        v0_7[2] = net.hockeyapp.android.c.i.c;
        net.hockeyapp.android.c.i.e = v0_7;
        return;
    }

    private i(String p1, int p2, int p3)
    {
        this(p1, p2);
        this.d = p3;
        return;
    }

    private int a()
    {
        return this.d;
    }

    public static net.hockeyapp.android.c.i valueOf(String p1)
    {
        return ((net.hockeyapp.android.c.i) Enum.valueOf(net.hockeyapp.android.c.i, p1));
    }

    public static net.hockeyapp.android.c.i[] values()
    {
        return ((net.hockeyapp.android.c.i[]) net.hockeyapp.android.c.i.e.clone());
    }
}
