package net.hockeyapp.android.c;
public class e implements java.io.Serializable {
    private static final long g = 5059651319640956830;
    public int a;
    public int b;
    public String c;
    public String d;
    public String e;
    public String f;

    public e()
    {
        return;
    }

    private void a(int p1)
    {
        this.a = p1;
        return;
    }

    private void a(String p1)
    {
        this.c = p1;
        return;
    }

    private int b()
    {
        return this.a;
    }

    private void b(int p1)
    {
        this.b = p1;
        return;
    }

    private void b(String p1)
    {
        this.d = p1;
        return;
    }

    private int c()
    {
        return this.b;
    }

    private void c(String p1)
    {
        this.e = p1;
        return;
    }

    private String d()
    {
        return this.c;
    }

    private void d(String p1)
    {
        this.f = p1;
        return;
    }

    private String e()
    {
        return this.d;
    }

    private String f()
    {
        return this.e;
    }

    private String g()
    {
        return this.f;
    }

    private boolean h()
    {
        int v0 = 1;
        int v2_0 = net.hockeyapp.android.a.a();
        if ((!v2_0.exists()) || (!v2_0.isDirectory())) {
            v0 = 0;
        } else {
            int v2_1 = v2_0.listFiles(new net.hockeyapp.android.c.f(this));
            if ((v2_1 == 0) || (v2_1.length != 1)) {
                v0 = 0;
            }
        }
        return v0;
    }

    public final String a()
    {
        return new StringBuilder().append(this.b).append(this.a).toString();
    }

    public String toString()
    {
        return new StringBuilder("\n").append(net.hockeyapp.android.c.e.getSimpleName()).append("\nid         ").append(this.a).append("\nmessage id ").append(this.b).append("\nfilename   ").append(this.c).append("\nurl        ").append(this.d).append("\ncreatedAt  ").append(this.e).append("\nupdatedAt  ").append(this.f).toString();
    }
}
