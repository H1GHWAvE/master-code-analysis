package net.hockeyapp.android;
public class al extends android.app.Activity implements android.view.View$OnClickListener, net.hockeyapp.android.as, net.hockeyapp.android.ax {
    protected net.hockeyapp.android.d.l a;
    protected net.hockeyapp.android.e.y b;
    private final int c;
    private net.hockeyapp.android.c.c d;
    private android.content.Context e;

    public al()
    {
        this.c = 0;
        return;
    }

    static synthetic net.hockeyapp.android.c.c a(net.hockeyapp.android.al p1)
    {
        p1.d = 0;
        return 0;
    }

    private void a(String p3)
    {
        this.a = new net.hockeyapp.android.d.l(this, p3, new net.hockeyapp.android.ao(this));
        net.hockeyapp.android.e.a.a(this.a);
        return;
    }

    private void a(String p2, net.hockeyapp.android.b.a p3)
    {
        this.a = new net.hockeyapp.android.d.l(this, p2, p3);
        return;
    }

    private static boolean a(android.content.Context p1)
    {
        int v0_2;
        if (p1.checkCallingOrSelfPermission("android.permission.WRITE_EXTERNAL_STORAGE") != 0) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    private void e()
    {
        ((android.widget.TextView) this.findViewById(4098)).setText(this.h());
        android.webkit.WebView v0_5 = ((android.widget.TextView) this.findViewById(4099));
        String v2_3 = new StringBuilder("Version ").append(this.b.a()).toString();
        String v3_0 = this.b.b();
        String v1_5 = "Unknown size";
        String v4_1 = this.b.c();
        if (v4_1 < 0) {
            net.hockeyapp.android.e.a.a(new net.hockeyapp.android.d.o(this, this.getIntent().getStringExtra("url"), new net.hockeyapp.android.am(this, v0_5, v2_3, v3_0)));
        } else {
            String v1_7 = new StringBuilder();
            Object[] v7 = new Object[1];
            v7[0] = Float.valueOf((((float) v4_1) / 1233125376));
            v1_5 = v1_7.append(String.format("%.2f", v7)).append(" MB").toString();
        }
        v0_5.setText(new StringBuilder().append(v2_3).append("\n").append(v3_0).append(" - ").append(v1_5).toString());
        ((android.widget.Button) this.findViewById(4100)).setOnClickListener(this);
        android.webkit.WebView v0_11 = ((android.webkit.WebView) this.findViewById(4101));
        v0_11.clearCache(1);
        v0_11.destroyDrawingCache();
        v0_11.loadDataWithBaseURL("https://sdk.hockeyapp.net/", this.b.d(), "text/html", "utf-8", 0);
        return;
    }

    private String f()
    {
        return this.b.d();
    }

    private android.view.ViewGroup g()
    {
        return new net.hockeyapp.android.f.m(this);
    }

    private String h()
    {
        try {
            String v0_0 = this.getPackageManager();
            String v0_2 = v0_0.getApplicationLabel(v0_0.getApplicationInfo(this.getPackageName(), 0)).toString();
        } catch (String v0) {
            v0_2 = "";
        }
        return v0_2;
    }

    private boolean i()
    {
        android.provider.Settings$SettingNotFoundException v0 = 1;
        try {
            if ((android.os.Build$VERSION.SDK_INT < 17) || (android.os.Build$VERSION.SDK_INT >= 21)) {
                if (android.provider.Settings$Secure.getInt(this.getContentResolver(), "install_non_market_apps") != 1) {
                    v0 = 0;
                }
            } else {
                if (android.provider.Settings$Global.getInt(this.getContentResolver(), "install_non_market_apps") != 1) {
                    v0 = 0;
                }
            }
        } catch (android.provider.Settings$SettingNotFoundException v1) {
        }
        return v0;
    }

    protected final void a()
    {
        this.a = new net.hockeyapp.android.d.l(this, this.getIntent().getStringExtra("url"), new net.hockeyapp.android.ao(this));
        net.hockeyapp.android.e.a.a(this.a);
        return;
    }

    public final void b()
    {
        this.findViewById(4100).setEnabled(1);
        return;
    }

    protected final void c()
    {
        net.hockeyapp.android.aq v0_2;
        if (this.e.checkCallingOrSelfPermission("android.permission.WRITE_EXTERNAL_STORAGE") != 0) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        if (v0_2 != null) {
            if (this.i()) {
                this.a();
            } else {
                this.d = new net.hockeyapp.android.c.c();
                this.d.a = "The installation from unknown sources is not enabled. Please check the device settings.";
                this.runOnUiThread(new net.hockeyapp.android.aq(this));
            }
        } else {
            if (android.os.Build$VERSION.SDK_INT < 23) {
                this.d = new net.hockeyapp.android.c.c();
                this.d.a = "The permission to access the external storage permission is not set. Please contact the developer.";
                this.runOnUiThread(new net.hockeyapp.android.ap(this));
            } else {
                net.hockeyapp.android.aq v0_15 = new String[1];
                v0_15[0] = "android.permission.WRITE_EXTERNAL_STORAGE";
                this.requestPermissions(v0_15, 1);
            }
        }
        return;
    }

    public final synthetic android.view.View d()
    {
        return this.g();
    }

    public int getCurrentVersionCode()
    {
        try {
            int v0 = this.getPackageManager().getPackageInfo(this.getPackageName(), 128).versionCode;
        } catch (android.content.pm.PackageManager$NameNotFoundException v1) {
        }
        return v0;
    }

    public void onClick(android.view.View p2)
    {
        this.c();
        p2.setEnabled(0);
        return;
    }

    public void onCreate(android.os.Bundle p11)
    {
        super.onCreate(p11);
        this.setTitle("App Update");
        this.setContentView(this.g());
        this.e = this;
        this.b = new net.hockeyapp.android.e.y(this, this.getIntent().getStringExtra("json"), this);
        ((android.widget.TextView) this.findViewById(4098)).setText(this.h());
        net.hockeyapp.android.d.l v0_9 = ((android.widget.TextView) this.findViewById(4099));
        String v2_4 = new StringBuilder("Version ").append(this.b.a()).toString();
        String v3_0 = this.b.b();
        String v1_7 = "Unknown size";
        String v4_1 = this.b.c();
        if (v4_1 < 0) {
            net.hockeyapp.android.e.a.a(new net.hockeyapp.android.d.o(this, this.getIntent().getStringExtra("url"), new net.hockeyapp.android.am(this, v0_9, v2_4, v3_0)));
        } else {
            String v1_9 = new StringBuilder();
            Object[] v7 = new Object[1];
            v7[0] = Float.valueOf((((float) v4_1) / 1233125376));
            v1_7 = v1_9.append(String.format("%.2f", v7)).append(" MB").toString();
        }
        v0_9.setText(new StringBuilder().append(v2_4).append("\n").append(v3_0).append(" - ").append(v1_7).toString());
        ((android.widget.Button) this.findViewById(4100)).setOnClickListener(this);
        net.hockeyapp.android.d.l v0_15 = ((android.webkit.WebView) this.findViewById(4101));
        v0_15.clearCache(1);
        v0_15.destroyDrawingCache();
        v0_15.loadDataWithBaseURL("https://sdk.hockeyapp.net/", this.b.d(), "text/html", "utf-8", 0);
        this.a = ((net.hockeyapp.android.d.l) this.getLastNonConfigurationInstance());
        if (this.a != null) {
            this.a.a(this);
        }
        return;
    }

    protected android.app.Dialog onCreateDialog(int p2)
    {
        return this.onCreateDialog(p2, 0);
    }

    protected android.app.Dialog onCreateDialog(int p4, android.os.Bundle p5)
    {
        android.app.AlertDialog v0_7;
        switch (p4) {
            case 0:
                v0_7 = new android.app.AlertDialog$Builder(this).setMessage("An error has occured").setCancelable(0).setTitle("Error").setIcon(17301543).setPositiveButton("OK", new net.hockeyapp.android.ar(this)).create();
                break;
            default:
                v0_7 = 0;
        }
        return v0_7;
    }

    protected void onPrepareDialog(int p2, android.app.Dialog p3)
    {
        switch (p2) {
            case 0:
                if (this.d == null) {
                    ((android.app.AlertDialog) p3).setMessage("An unknown error has occured.");
                } else {
                    ((android.app.AlertDialog) p3).setMessage(this.d.a);
                }
                break;
        }
        return;
    }

    public void onRequestPermissionsResult(int p4, String[] p5, int[] p6)
    {
        this.b();
        if ((p5.length != 0) && ((p6.length != 0) && (p4 == 1))) {
            if (p6[0] != 0) {
                android.util.Log.w("HockeyApp", "User denied write permission, can\'t continue with updater task.");
                if (net.hockeyapp.android.ay.a() == null) {
                    new android.app.AlertDialog$Builder(this.e).setTitle(net.hockeyapp.android.aj.a(1792)).setMessage(net.hockeyapp.android.aj.a(1793)).setNegativeButton(net.hockeyapp.android.aj.a(1794), 0).setPositiveButton(net.hockeyapp.android.aj.a(1795), new net.hockeyapp.android.an(this, this)).create().show();
                }
            } else {
                this.c();
            }
        }
        return;
    }

    public Object onRetainNonConfigurationInstance()
    {
        if (this.a != null) {
            this.a.a();
        }
        return this.a;
    }
}
