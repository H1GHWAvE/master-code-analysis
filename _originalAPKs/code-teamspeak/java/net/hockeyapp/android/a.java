package net.hockeyapp.android;
public final class a {
    public static String a = "None";
    public static String b = "None";
    public static String c = "None";
    public static String d = "None";
    public static String e = "None";
    public static String f = "None";
    public static String g = "None";
    public static String h = "None";
    public static final String i = "HockeyApp";
    public static final String j = "https://sdk.hockeyapp.net/";
    public static final String k = "HockeySDK";
    public static final String l = "3.6.0";
    public static final int m = 1;

    static a()
    {
        net.hockeyapp.android.a.a = 0;
        net.hockeyapp.android.a.b = 0;
        net.hockeyapp.android.a.c = 0;
        net.hockeyapp.android.a.d = 0;
        net.hockeyapp.android.a.e = 0;
        net.hockeyapp.android.a.f = 0;
        net.hockeyapp.android.a.g = 0;
        net.hockeyapp.android.a.h = 0;
        return;
    }

    public a()
    {
        return;
    }

    private static int a(android.content.Context p4, android.content.pm.PackageManager p5)
    {
        int v0 = 0;
        try {
            Exception v1_2 = p5.getApplicationInfo(p4.getPackageName(), 128).metaData;
        } catch (Exception v1_3) {
            android.util.Log.e("HockeyApp", "Exception thrown when accessing the application info:");
            v1_3.printStackTrace();
            return v0;
        }
        if (v1_2 == null) {
            return v0;
        } else {
            v0 = v1_2.getInt("buildNumber", 0);
            return v0;
        }
    }

    public static java.io.File a()
    {
        java.io.File v1_1 = new java.io.File(new StringBuilder().append(android.os.Environment.getExternalStorageDirectory().getAbsolutePath()).append("/HockeyApp").toString());
        v1_1.mkdirs();
        return v1_1;
    }

    private static String a(byte[] p6)
    {
        String v1_0 = "0123456789ABCDEF".toCharArray();
        String v2_0 = new char[(p6.length * 2)];
        String v0_3 = 0;
        while (v0_3 < p6.length) {
            char v3_2 = (p6[v0_3] & 255);
            v2_0[(v0_3 * 2)] = v1_0[(v3_2 >> 4)];
            v2_0[((v0_3 * 2) + 1)] = v1_0[(v3_2 & 15)];
            v0_3++;
        }
        return new String(v2_0).replaceAll("(\\w{8})(\\w{4})(\\w{4})(\\w{4})(\\w{12})", "$1-$2-$3-$4-$5");
    }

    public static void a(android.content.Context p4)
    {
        net.hockeyapp.android.a.e = android.os.Build$VERSION.RELEASE;
        net.hockeyapp.android.a.f = android.os.Build.MODEL;
        net.hockeyapp.android.a.g = android.os.Build.MANUFACTURER;
        if (p4 != null) {
            try {
                Throwable v0_3 = p4.getFilesDir();
            } catch (Throwable v0_5) {
                android.util.Log.e("HockeyApp", "Exception thrown when accessing the files dir:");
                v0_5.printStackTrace();
            }
            if (v0_3 != null) {
                net.hockeyapp.android.a.a = v0_3.getAbsolutePath();
            }
        }
        if (p4 != null) {
            try {
                Throwable v0_6 = p4.getPackageManager();
                java.security.MessageDigest v1_2 = v0_6.getPackageInfo(p4.getPackageName(), 0);
                net.hockeyapp.android.a.d = v1_2.packageName;
                net.hockeyapp.android.a.b = new StringBuilder().append(v1_2.versionCode).toString();
                net.hockeyapp.android.a.c = v1_2.versionName;
                Throwable v0_7 = net.hockeyapp.android.a.a(p4, v0_6);
            } catch (Throwable v0_9) {
                android.util.Log.e("HockeyApp", "Exception thrown when accessing the package info:");
                v0_9.printStackTrace();
            }
            if ((v0_7 != null) && (v0_7 > v1_2.versionCode)) {
                net.hockeyapp.android.a.b = String.valueOf(v0_7);
            }
        }
        Throwable v0_11 = android.provider.Settings$Secure.getString(p4.getContentResolver(), "android_id");
        if ((net.hockeyapp.android.a.d != null) && (v0_11 != null)) {
            Throwable v0_15 = new StringBuilder().append(net.hockeyapp.android.a.d).append(":").append(v0_11).append(":").append(net.hockeyapp.android.a.b()).toString();
            try {
                java.security.MessageDigest v1_14 = java.security.MessageDigest.getInstance("SHA-1");
                Throwable v0_16 = v0_15.getBytes("UTF-8");
                v1_14.update(v0_16, 0, v0_16.length);
                net.hockeyapp.android.a.h = net.hockeyapp.android.a.a(v1_14.digest());
            } catch (Throwable v0) {
            }
        }
        return;
    }

    private static String b()
    {
        StringBuilder v1_13 = new StringBuilder("HA").append((android.os.Build.BOARD.length() % 10)).append((android.os.Build.BRAND.length() % 10)).append((android.os.Build.CPU_ABI.length() % 10)).append((android.os.Build.PRODUCT.length() % 10)).toString();
        String v0_6 = "";
        if (android.os.Build$VERSION.SDK_INT >= 9) {
            try {
                v0_6 = android.os.Build.getField("SERIAL").get(0).toString();
            } catch (Throwable v2) {
            }
        }
        return new StringBuilder().append(v1_13).append(":").append(v0_6).toString();
    }

    private static void b(android.content.Context p3)
    {
        if (p3 != null) {
            try {
                String v0_0 = p3.getFilesDir();
            } catch (String v0_2) {
                android.util.Log.e("HockeyApp", "Exception thrown when accessing the files dir:");
                v0_2.printStackTrace();
            }
            if (v0_0 != null) {
                net.hockeyapp.android.a.a = v0_0.getAbsolutePath();
            }
        }
        return;
    }

    private static void c(android.content.Context p4)
    {
        if (p4 != null) {
            try {
                String v0_0 = p4.getPackageManager();
                int v1_1 = v0_0.getPackageInfo(p4.getPackageName(), 0);
                net.hockeyapp.android.a.d = v1_1.packageName;
                net.hockeyapp.android.a.b = new StringBuilder().append(v1_1.versionCode).toString();
                net.hockeyapp.android.a.c = v1_1.versionName;
                String v0_1 = net.hockeyapp.android.a.a(p4, v0_0);
            } catch (String v0_3) {
                android.util.Log.e("HockeyApp", "Exception thrown when accessing the package info:");
                v0_3.printStackTrace();
            }
            if ((v0_1 != null) && (v0_1 > v1_1.versionCode)) {
                net.hockeyapp.android.a.b = String.valueOf(v0_1);
            }
        }
        return;
    }

    private static void d(android.content.Context p4)
    {
        Throwable v0_1 = android.provider.Settings$Secure.getString(p4.getContentResolver(), "android_id");
        if ((net.hockeyapp.android.a.d != null) && (v0_1 != null)) {
            Throwable v0_5 = new StringBuilder().append(net.hockeyapp.android.a.d).append(":").append(v0_1).append(":").append(net.hockeyapp.android.a.b()).toString();
            try {
                java.security.MessageDigest v1_9 = java.security.MessageDigest.getInstance("SHA-1");
                Throwable v0_6 = v0_5.getBytes("UTF-8");
                v1_9.update(v0_6, 0, v0_6.length);
                net.hockeyapp.android.a.h = net.hockeyapp.android.a.a(v1_9.digest());
            } catch (Throwable v0) {
            }
        }
        return;
    }
}
