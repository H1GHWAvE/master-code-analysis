package net.hockeyapp.android.f;
public final class k extends android.widget.ImageView {
    private static final float b = 16512;
    public java.util.Stack a;
    private android.graphics.Path c;
    private android.graphics.Paint d;
    private float e;
    private float f;

    public k(android.content.Context p5, android.net.Uri p6, int p7, int p8)
    {
        this(p5);
        this.c = new android.graphics.Path();
        this.a = new java.util.Stack();
        this.d = new android.graphics.Paint();
        this.d.setAntiAlias(1);
        this.d.setDither(1);
        this.d.setColor(-65536);
        this.d.setStyle(android.graphics.Paint$Style.STROKE);
        this.d.setStrokeJoin(android.graphics.Paint$Join.ROUND);
        this.d.setStrokeCap(android.graphics.Paint$Cap.ROUND);
        this.d.setStrokeWidth(1094713344);
        net.hockeyapp.android.f.l v0_14 = new net.hockeyapp.android.f.l(this);
        Object[] v1_6 = new Object[4];
        v1_6[0] = p5;
        v1_6[1] = p6;
        v1_6[2] = Integer.valueOf(p7);
        v1_6[3] = Integer.valueOf(p8);
        v0_14.execute(v1_6);
        return;
    }

    public static int a(android.content.ContentResolver p4, android.net.Uri p5)
    {
        int v0 = 1;
        java.io.IOException v1_1 = new android.graphics.BitmapFactory$Options();
        v1_1.inJustDecodeBounds = 1;
        try {
            android.graphics.BitmapFactory.decodeStream(p4.openInputStream(p5), 0, v1_1);
        } catch (java.io.IOException v1_6) {
            android.util.Log.e("HockeyApp", "Unable to determine necessary screen orientation.", v1_6);
            return v0;
        }
        if ((((float) v1_1.outWidth) / ((float) v1_1.outHeight)) <= 1065353216) {
            return v0;
        } else {
            v0 = 0;
            return v0;
        }
    }

    private static int a(android.graphics.BitmapFactory$Options p4, int p5, int p6)
    {
        int v1_0 = p4.outHeight;
        int v2_0 = p4.outWidth;
        int v0 = 1;
        if ((v1_0 > p6) || (v2_0 > p5)) {
            while ((((v1_0 / 2) / v0) > p6) && (((v2_0 / 2) / v0) > p5)) {
                v0 *= 2;
            }
        }
        return v0;
    }

    static synthetic android.graphics.Bitmap a(android.content.ContentResolver p3, android.net.Uri p4, int p5, int p6)
    {
        android.graphics.Bitmap v0_1 = new android.graphics.BitmapFactory$Options();
        v0_1.inJustDecodeBounds = 1;
        android.graphics.BitmapFactory.decodeStream(p3.openInputStream(p4), 0, v0_1);
        v0_1.inSampleSize = net.hockeyapp.android.f.k.a(v0_1, p5, p6);
        v0_1.inJustDecodeBounds = 0;
        return android.graphics.BitmapFactory.decodeStream(p3.openInputStream(p4), 0, v0_1);
    }

    private void a()
    {
        this.a.clear();
        this.invalidate();
        return;
    }

    private void a(float p2, float p3)
    {
        this.c.reset();
        this.c.moveTo(p2, p3);
        this.e = p2;
        this.f = p3;
        return;
    }

    private static android.graphics.Bitmap b(android.content.ContentResolver p3, android.net.Uri p4, int p5, int p6)
    {
        android.graphics.Bitmap v0_1 = new android.graphics.BitmapFactory$Options();
        v0_1.inJustDecodeBounds = 1;
        android.graphics.BitmapFactory.decodeStream(p3.openInputStream(p4), 0, v0_1);
        v0_1.inSampleSize = net.hockeyapp.android.f.k.a(v0_1, p5, p6);
        v0_1.inJustDecodeBounds = 0;
        return android.graphics.BitmapFactory.decodeStream(p3.openInputStream(p4), 0, v0_1);
    }

    private void b()
    {
        if (!this.a.empty()) {
            this.a.pop();
            this.invalidate();
        }
        return;
    }

    private void b(float p7, float p8)
    {
        android.graphics.Path v0_2 = Math.abs((p7 - this.e));
        float v1_2 = Math.abs((p8 - this.f));
        if ((v0_2 >= 1082130432) || (v1_2 >= 1082130432)) {
            this.c.quadTo(this.e, this.f, ((this.e + p7) / 1073741824), ((this.f + p8) / 1073741824));
            this.e = p7;
            this.f = p8;
        }
        return;
    }

    private boolean c()
    {
        return this.a.empty();
    }

    private void d()
    {
        this.c.lineTo(this.e, this.f);
        this.a.push(this.c);
        this.c = new android.graphics.Path();
        return;
    }

    protected final void onDraw(android.graphics.Canvas p4)
    {
        super.onDraw(p4);
        android.graphics.Paint v1_0 = this.a.iterator();
        while (v1_0.hasNext()) {
            p4.drawPath(((android.graphics.Path) v1_0.next()), this.d);
        }
        p4.drawPath(this.c, this.d);
        return;
    }

    public final boolean onTouchEvent(android.view.MotionEvent p9)
    {
        android.graphics.Path v0_0 = p9.getX();
        android.graphics.Path v1_0 = p9.getY();
        switch (p9.getAction()) {
            case 0:
                this.c.reset();
                this.c.moveTo(v0_0, v1_0);
                this.e = v0_0;
                this.f = v1_0;
                this.invalidate();
                break;
            case 1:
                this.c.lineTo(this.e, this.f);
                this.a.push(this.c);
                this.c = new android.graphics.Path();
                this.invalidate();
                break;
            case 2:
                android.graphics.Path v2_3 = Math.abs((v0_0 - this.e));
                float v3_2 = Math.abs((v1_0 - this.f));
                if ((v2_3 >= 1082130432) || (v3_2 >= 1082130432)) {
                    this.c.quadTo(this.e, this.f, ((this.e + v0_0) / 1073741824), ((this.f + v1_0) / 1073741824));
                    this.e = v0_0;
                    this.f = v1_0;
                }
                this.invalidate();
                break;
        }
        return 1;
    }
}
