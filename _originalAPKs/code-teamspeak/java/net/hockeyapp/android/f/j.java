package net.hockeyapp.android.f;
public final class j extends android.widget.LinearLayout {
    public static final int a = 12289;
    public static final int b = 12290;
    public static final int c = 12291;
    public static final int d = 12292;
    public static final int e = 12293;
    private android.widget.LinearLayout f;

    public j(android.content.Context p2)
    {
        this(p2, 0);
        return;
    }

    private j(android.content.Context p9, byte p10)
    {
        this(p9);
        android.widget.Button v0_1 = new android.widget.LinearLayout$LayoutParams(-1, -2);
        this.setBackgroundColor(-1);
        this.setLayoutParams(v0_1);
        this.f = new android.widget.LinearLayout(p9);
        this.f.setId(12289);
        android.widget.Button v0_6 = new android.widget.LinearLayout$LayoutParams(-1, -1);
        android.widget.LinearLayout v1_3 = ((int) android.util.TypedValue.applyDimension(1, 1101004800, this.getResources().getDisplayMetrics()));
        v0_6.gravity = 49;
        this.f.setLayoutParams(v0_6);
        this.f.setPadding(v1_3, v1_3, v1_3, v1_3);
        this.f.setOrientation(1);
        this.addView(this.f);
        android.widget.Button v0_11 = new android.widget.TextView(p9);
        v0_11.setId(12290);
        android.widget.LinearLayout v1_6 = new android.widget.LinearLayout$LayoutParams(-1, -2);
        v1_6.setMargins(0, 0, 0, ((int) android.util.TypedValue.applyDimension(1, 1106247680, this.getResources().getDisplayMetrics())));
        v0_11.setLayoutParams(v1_6);
        v0_11.setText(net.hockeyapp.android.aj.a(1280));
        v0_11.setTextColor(-7829368);
        v0_11.setTextSize(2, 1099956224);
        v0_11.setTypeface(0, 0);
        this.f.addView(v0_11);
        android.widget.Button v0_13 = new android.widget.EditText(p9);
        v0_13.setId(12291);
        android.widget.LinearLayout v1_15 = new android.widget.LinearLayout$LayoutParams(-1, -2);
        v1_15.setMargins(0, 0, 0, ((int) android.util.TypedValue.applyDimension(1, 1106247680, this.getResources().getDisplayMetrics())));
        v0_13.setLayoutParams(v1_15);
        v0_13.setHint(net.hockeyapp.android.aj.a(1282));
        v0_13.setImeOptions(5);
        v0_13.setInputType(33);
        v0_13.setTextColor(-7829368);
        v0_13.setTextSize(2, 1097859072);
        v0_13.setTypeface(0, 0);
        v0_13.setHintTextColor(-3355444);
        net.hockeyapp.android.f.j.a(p9, v0_13);
        this.f.addView(v0_13);
        android.widget.Button v0_15 = new android.widget.EditText(p9);
        v0_15.setId(12292);
        android.widget.LinearLayout v1_27 = new android.widget.LinearLayout$LayoutParams(-1, -2);
        v1_27.setMargins(0, 0, 0, ((int) android.util.TypedValue.applyDimension(1, 1106247680, this.getResources().getDisplayMetrics())));
        v0_15.setLayoutParams(v1_27);
        v0_15.setHint(net.hockeyapp.android.aj.a(1283));
        v0_15.setImeOptions(5);
        v0_15.setInputType(128);
        v0_15.setTransformationMethod(android.text.method.PasswordTransformationMethod.getInstance());
        v0_15.setTextColor(-7829368);
        v0_15.setTextSize(2, 1097859072);
        v0_15.setTypeface(0, 0);
        v0_15.setHintTextColor(-3355444);
        net.hockeyapp.android.f.j.a(p9, v0_15);
        this.f.addView(v0_15);
        android.widget.Button v0_17 = new android.widget.Button(p9);
        v0_17.setId(12293);
        android.widget.LinearLayout v1_40 = new android.widget.LinearLayout$LayoutParams(-1, -2);
        v1_40.setMargins(0, 0, 0, ((int) android.util.TypedValue.applyDimension(1, 1106247680, this.getResources().getDisplayMetrics())));
        v0_17.setLayoutParams(v1_40);
        v0_17.setBackgroundDrawable(this.getButtonSelector());
        v0_17.setText(net.hockeyapp.android.aj.a(1284));
        v0_17.setTextColor(-1);
        v0_17.setTextSize(2, 1097859072);
        this.f.addView(v0_17);
        return;
    }

    private void a()
    {
        android.widget.LinearLayout$LayoutParams v0_1 = new android.widget.LinearLayout$LayoutParams(-1, -2);
        this.setBackgroundColor(-1);
        this.setLayoutParams(v0_1);
        return;
    }

    private void a(android.content.Context p5)
    {
        this.f = new android.widget.LinearLayout(p5);
        this.f.setId(12289);
        android.widget.LinearLayout v0_4 = new android.widget.LinearLayout$LayoutParams(-1, -1);
        int v1_3 = ((int) android.util.TypedValue.applyDimension(1, 1101004800, this.getResources().getDisplayMetrics()));
        v0_4.gravity = 49;
        this.f.setLayoutParams(v0_4);
        this.f.setPadding(v1_3, v1_3, v1_3, v1_3);
        this.f.setOrientation(1);
        this.addView(this.f);
        return;
    }

    private static void a(android.content.Context p8, android.widget.EditText p9)
    {
        if (android.os.Build$VERSION.SDK_INT < 11) {
            android.graphics.drawable.LayerDrawable v0_5 = ((int) (p8.getResources().getDisplayMetrics().density * 1092616192));
            android.graphics.drawable.ShapeDrawable v1_3 = new android.graphics.drawable.ShapeDrawable(new android.graphics.drawable.shapes.RectShape());
            int v2_2 = v1_3.getPaint();
            v2_2.setColor(-1);
            v2_2.setStyle(android.graphics.Paint$Style.FILL_AND_STROKE);
            v2_2.setStrokeWidth(1065353216);
            v1_3.setPadding(v0_5, v0_5, v0_5, v0_5);
            android.graphics.drawable.LayerDrawable v0_9 = ((int) (((double) p8.getResources().getDisplayMetrics().density) * 1.5));
            int v2_6 = new android.graphics.drawable.ShapeDrawable(new android.graphics.drawable.shapes.RectShape());
            android.graphics.drawable.Drawable[] v3_4 = v2_6.getPaint();
            v3_4.setColor(-12303292);
            v3_4.setStyle(android.graphics.Paint$Style.FILL_AND_STROKE);
            v3_4.setStrokeWidth(1065353216);
            v2_6.setPadding(0, 0, 0, v0_9);
            android.graphics.drawable.Drawable[] v3_6 = new android.graphics.drawable.Drawable[2];
            v3_6[0] = v2_6;
            v3_6[1] = v1_3;
            p9.setBackgroundDrawable(new android.graphics.drawable.LayerDrawable(v3_6));
        }
        return;
    }

    private void b(android.content.Context p7)
    {
        android.widget.TextView v0_1 = new android.widget.TextView(p7);
        v0_1.setId(12290);
        android.widget.LinearLayout v1_2 = new android.widget.LinearLayout$LayoutParams(-1, -2);
        v1_2.setMargins(0, 0, 0, ((int) android.util.TypedValue.applyDimension(1, 1106247680, this.getResources().getDisplayMetrics())));
        v0_1.setLayoutParams(v1_2);
        v0_1.setText(net.hockeyapp.android.aj.a(1280));
        v0_1.setTextColor(-7829368);
        v0_1.setTextSize(2, 1099956224);
        v0_1.setTypeface(0, 0);
        this.f.addView(v0_1);
        return;
    }

    private void c(android.content.Context p7)
    {
        android.widget.EditText v0_1 = new android.widget.EditText(p7);
        v0_1.setId(12291);
        android.widget.LinearLayout v1_2 = new android.widget.LinearLayout$LayoutParams(-1, -2);
        v1_2.setMargins(0, 0, 0, ((int) android.util.TypedValue.applyDimension(1, 1106247680, this.getResources().getDisplayMetrics())));
        v0_1.setLayoutParams(v1_2);
        v0_1.setHint(net.hockeyapp.android.aj.a(1282));
        v0_1.setImeOptions(5);
        v0_1.setInputType(33);
        v0_1.setTextColor(-7829368);
        v0_1.setTextSize(2, 1097859072);
        v0_1.setTypeface(0, 0);
        v0_1.setHintTextColor(-3355444);
        net.hockeyapp.android.f.j.a(p7, v0_1);
        this.f.addView(v0_1);
        return;
    }

    private void d(android.content.Context p7)
    {
        android.widget.EditText v0_1 = new android.widget.EditText(p7);
        v0_1.setId(12292);
        android.widget.LinearLayout v1_2 = new android.widget.LinearLayout$LayoutParams(-1, -2);
        v1_2.setMargins(0, 0, 0, ((int) android.util.TypedValue.applyDimension(1, 1106247680, this.getResources().getDisplayMetrics())));
        v0_1.setLayoutParams(v1_2);
        v0_1.setHint(net.hockeyapp.android.aj.a(1283));
        v0_1.setImeOptions(5);
        v0_1.setInputType(128);
        v0_1.setTransformationMethod(android.text.method.PasswordTransformationMethod.getInstance());
        v0_1.setTextColor(-7829368);
        v0_1.setTextSize(2, 1097859072);
        v0_1.setTypeface(0, 0);
        v0_1.setHintTextColor(-3355444);
        net.hockeyapp.android.f.j.a(p7, v0_1);
        this.f.addView(v0_1);
        return;
    }

    private void e(android.content.Context p8)
    {
        android.widget.Button v0_1 = new android.widget.Button(p8);
        v0_1.setId(12293);
        android.widget.LinearLayout v1_2 = new android.widget.LinearLayout$LayoutParams(-1, -2);
        v1_2.setMargins(0, 0, 0, ((int) android.util.TypedValue.applyDimension(1, 1106247680, this.getResources().getDisplayMetrics())));
        v0_1.setLayoutParams(v1_2);
        v0_1.setBackgroundDrawable(this.getButtonSelector());
        v0_1.setText(net.hockeyapp.android.aj.a(1284));
        v0_1.setTextColor(-1);
        v0_1.setTextSize(2, 1097859072);
        this.f.addView(v0_1);
        return;
    }

    private static android.graphics.drawable.Drawable f(android.content.Context p8)
    {
        android.graphics.drawable.LayerDrawable v0_4 = ((int) (p8.getResources().getDisplayMetrics().density * 1092616192));
        android.graphics.drawable.ShapeDrawable v1_2 = new android.graphics.drawable.ShapeDrawable(new android.graphics.drawable.shapes.RectShape());
        int v2_2 = v1_2.getPaint();
        v2_2.setColor(-1);
        v2_2.setStyle(android.graphics.Paint$Style.FILL_AND_STROKE);
        v2_2.setStrokeWidth(1065353216);
        v1_2.setPadding(v0_4, v0_4, v0_4, v0_4);
        android.graphics.drawable.LayerDrawable v0_8 = ((int) (((double) p8.getResources().getDisplayMetrics().density) * 1.5));
        int v2_6 = new android.graphics.drawable.ShapeDrawable(new android.graphics.drawable.shapes.RectShape());
        android.graphics.drawable.Drawable[] v3_4 = v2_6.getPaint();
        v3_4.setColor(-12303292);
        v3_4.setStyle(android.graphics.Paint$Style.FILL_AND_STROKE);
        v3_4.setStrokeWidth(1065353216);
        v2_6.setPadding(0, 0, 0, v0_8);
        android.graphics.drawable.Drawable[] v3_6 = new android.graphics.drawable.Drawable[2];
        v3_6[0] = v2_6;
        v3_6[1] = v1_2;
        return new android.graphics.drawable.LayerDrawable(v3_6);
    }

    private android.graphics.drawable.Drawable getButtonSelector()
    {
        android.graphics.drawable.StateListDrawable v0_1 = new android.graphics.drawable.StateListDrawable();
        int[] v1_0 = new int[1];
        v1_0[0] = -16842919;
        v0_1.addState(v1_0, new android.graphics.drawable.ColorDrawable(-16777216));
        int[] v1_2 = new int[2];
        v1_2 = {-16842919, 16842908};
        v0_1.addState(v1_2, new android.graphics.drawable.ColorDrawable(-12303292));
        int[] v1_3 = new int[1];
        v1_3[0] = 16842919;
        v0_1.addState(v1_3, new android.graphics.drawable.ColorDrawable(-7829368));
        return v0_1;
    }
}
