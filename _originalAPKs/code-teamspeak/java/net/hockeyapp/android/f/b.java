package net.hockeyapp.android.f;
public final class b extends android.widget.FrameLayout {
    private static final int e = 3;
    private static final int f = 2;
    final android.view.ViewGroup a;
    public final String b;
    public android.widget.TextView c;
    public int d;
    private final android.content.Context g;
    private final net.hockeyapp.android.c.e h;
    private final android.net.Uri i;
    private android.widget.ImageView j;
    private int k;
    private int l;
    private int m;
    private int n;
    private int o;

    public b(android.content.Context p3, android.view.ViewGroup p4, android.net.Uri p5)
    {
        this(p3);
        this.g = p3;
        this.a = p4;
        this.h = 0;
        this.i = p5;
        this.b = p5.getLastPathSegment();
        this.a(20);
        this.a(p3, 1);
        this.c.setText(this.b);
        Void[] v1_2 = new Void[0];
        new net.hockeyapp.android.f.c(this).execute(v1_2);
        return;
    }

    public b(android.content.Context p5, android.view.ViewGroup p6, net.hockeyapp.android.c.e p7)
    {
        this(p5);
        this.g = p5;
        this.a = p6;
        this.h = p7;
        this.i = android.net.Uri.fromFile(new java.io.File(net.hockeyapp.android.a.a(), p7.a()));
        this.b = p7.c;
        this.a(30);
        this.a(p5, 0);
        this.d = 0;
        this.c.setText("Loading...");
        this.a(0);
        return;
    }

    static synthetic android.graphics.Bitmap a(net.hockeyapp.android.f.b p1)
    {
        return p1.c();
    }

    private android.graphics.drawable.Drawable a(String p5)
    {
        return this.getResources().getDrawable(this.getResources().getIdentifier(p5, "drawable", "android"));
    }

    private void a()
    {
        this.a.removeView(this);
        return;
    }

    private void a(int p5)
    {
        int v0_1 = this.getResources().getDisplayMetrics();
        this.o = Math.round(android.util.TypedValue.applyDimension(1, 1092616192, v0_1));
        int v1_5 = Math.round(android.util.TypedValue.applyDimension(1, ((float) p5), v0_1));
        int v0_2 = v0_1.widthPixels;
        int v0_4 = ((v0_2 - (v1_5 * 2)) - (this.o * 1));
        this.k = (((v0_2 - (v1_5 * 2)) - (this.o * 2)) / 3);
        this.m = (v0_4 / 2);
        this.l = (this.k * 2);
        this.n = this.m;
        return;
    }

    private void a(android.content.Context p10, boolean p11)
    {
        this.setLayoutParams(new android.widget.FrameLayout$LayoutParams(-2, -2, 80));
        this.setPadding(0, this.o, 0, 0);
        this.j = new android.widget.ImageView(p10);
        android.widget.LinearLayout v0_6 = new android.widget.LinearLayout(p10);
        v0_6.setLayoutParams(new android.widget.FrameLayout$LayoutParams(-1, -2, 80));
        v0_6.setGravity(3);
        v0_6.setOrientation(1);
        v0_6.setBackgroundColor(android.graphics.Color.parseColor("#80262626"));
        this.c = new android.widget.TextView(p10);
        this.c.setLayoutParams(new android.widget.FrameLayout$LayoutParams(-1, -2, 17));
        this.c.setGravity(17);
        this.c.setTextColor(android.graphics.Color.parseColor("#FFFFFF"));
        this.c.setSingleLine();
        this.c.setEllipsize(android.text.TextUtils$TruncateAt.MIDDLE);
        if (p11) {
            android.widget.ImageView v1_13 = new android.widget.ImageButton(p10);
            v1_13.setLayoutParams(new android.widget.FrameLayout$LayoutParams(-1, -2, 80));
            v1_13.setAdjustViewBounds(1);
            v1_13.setImageDrawable(this.a("ic_menu_delete"));
            v1_13.setBackgroundResource(0);
            v1_13.setOnClickListener(new net.hockeyapp.android.f.d(this));
            v0_6.addView(v1_13);
        }
        v0_6.addView(this.c);
        this.addView(this.j);
        this.addView(v0_6);
        return;
    }

    private void a(android.graphics.Bitmap p4, int p5)
    {
        this.c.setText(this.b);
        this.d = p5;
        if (p4 != null) {
            this.a(p4, 1);
        } else {
            this.a(1);
        }
        return;
    }

    static synthetic void a(net.hockeyapp.android.f.b p1, android.graphics.Bitmap p2)
    {
        p1.a(p2, 0);
        return;
    }

    private void b()
    {
        this.c.setText("Error");
        return;
    }

    static synthetic void b(net.hockeyapp.android.f.b p1)
    {
        p1.a(0);
        return;
    }

    private android.graphics.Bitmap c()
    {
        android.graphics.Bitmap v0 = 0;
        try {
            int v2_1;
            this.d = net.hockeyapp.android.e.m.a(this.g, this.i);
        } catch (java.io.InputStream v1) {
            return v0;
        }
        if (this.d != 1) {
            v2_1 = this.k;
        } else {
            v2_1 = this.m;
        }
        java.io.InputStream v1_6;
        if (this.d != 1) {
            v1_6 = this.l;
        } else {
            v1_6 = this.n;
        }
        android.content.Context v3_1 = this.g;
        android.net.Uri v4 = this.i;
        android.graphics.BitmapFactory$Options v5_1 = new android.graphics.BitmapFactory$Options();
        v5_1.inJustDecodeBounds = 1;
        android.graphics.BitmapFactory.decodeStream(v3_1.getContentResolver().openInputStream(v4), 0, v5_1);
        v5_1.inSampleSize = net.hockeyapp.android.e.m.a(v5_1, v2_1, v1_6);
        v5_1.inJustDecodeBounds = 0;
        v0 = android.graphics.BitmapFactory.decodeStream(v3_1.getContentResolver().openInputStream(v4), 0, v5_1);
        return v0;
    }

    static synthetic android.net.Uri c(net.hockeyapp.android.f.b p1)
    {
        return p1.i;
    }

    static synthetic android.content.Context d(net.hockeyapp.android.f.b p1)
    {
        return p1.g;
    }

    public final void a(android.graphics.Bitmap p7, boolean p8)
    {
        android.widget.ImageView v0_1;
        if (this.d != 1) {
            v0_1 = this.k;
        } else {
            v0_1 = this.m;
        }
        net.hockeyapp.android.f.e v1_1;
        if (this.d != 1) {
            v1_1 = this.l;
        } else {
            v1_1 = this.n;
        }
        this.c.setMaxWidth(v0_1);
        this.c.setMinWidth(v0_1);
        this.j.setLayoutParams(new android.widget.FrameLayout$LayoutParams(-2, -2));
        this.j.setAdjustViewBounds(1);
        this.j.setMinimumWidth(v0_1);
        this.j.setMaxWidth(v0_1);
        this.j.setMaxHeight(v1_1);
        this.j.setScaleType(android.widget.ImageView$ScaleType.CENTER_INSIDE);
        this.j.setImageBitmap(p7);
        this.j.setOnClickListener(new net.hockeyapp.android.f.e(this, p8));
        return;
    }

    public final void a(boolean p4)
    {
        this.c.setMaxWidth(this.k);
        this.c.setMinWidth(this.k);
        this.j.setLayoutParams(new android.widget.FrameLayout$LayoutParams(-2, -2));
        this.j.setAdjustViewBounds(0);
        this.j.setBackgroundColor(android.graphics.Color.parseColor("#eeeeee"));
        this.j.setMinimumHeight(((int) (((float) this.k) * 1067030938)));
        this.j.setMinimumWidth(this.k);
        this.j.setScaleType(android.widget.ImageView$ScaleType.FIT_CENTER);
        this.j.setImageDrawable(this.a("ic_menu_attachment"));
        this.j.setOnClickListener(new net.hockeyapp.android.f.f(this, p4));
        return;
    }

    public final net.hockeyapp.android.c.e getAttachment()
    {
        return this.h;
    }

    public final android.net.Uri getAttachmentUri()
    {
        return this.i;
    }

    public final int getEffectiveMaxHeight()
    {
        int v0_1;
        if (this.d != 1) {
            v0_1 = this.l;
        } else {
            v0_1 = this.n;
        }
        return v0_1;
    }

    public final int getGap()
    {
        return this.o;
    }

    public final int getMaxHeightLandscape()
    {
        return this.n;
    }

    public final int getMaxHeightPortrait()
    {
        return this.l;
    }

    public final int getWidthLandscape()
    {
        return this.m;
    }

    public final int getWidthPortrait()
    {
        return this.k;
    }
}
