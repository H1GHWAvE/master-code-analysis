package net.hockeyapp.android.f;
public final class g extends android.widget.RelativeLayout {

    private g(android.content.Context p2)
    {
        this(p2, "");
        return;
    }

    public g(android.content.Context p5, String p6)
    {
        this(p5);
        android.widget.TextView v0_1 = new android.widget.RelativeLayout$LayoutParams(-1, -1);
        this.setBackgroundColor(-1);
        this.setLayoutParams(v0_1);
        int v1_3 = new android.widget.RelativeLayout$LayoutParams(-1, ((int) android.util.TypedValue.applyDimension(1, 1077936128, this.getResources().getDisplayMetrics())));
        v1_3.addRule(10, -1);
        android.widget.TextView v0_7 = new android.widget.ImageView(p5);
        v0_7.setLayoutParams(v1_3);
        v0_7.setBackgroundDrawable(net.hockeyapp.android.e.aa.a());
        this.addView(v0_7);
        android.widget.TextView v0_10 = ((int) android.util.TypedValue.applyDimension(1, 1101004800, this.getResources().getDisplayMetrics()));
        int v1_8 = new android.widget.RelativeLayout$LayoutParams(-1, -2);
        v1_8.addRule(13, -1);
        v1_8.setMargins(v0_10, v0_10, v0_10, v0_10);
        android.widget.TextView v0_12 = new android.widget.TextView(p5);
        v0_12.setGravity(17);
        v0_12.setLayoutParams(v1_8);
        v0_12.setText(p6);
        v0_12.setTextColor(-16777216);
        this.addView(v0_12);
        return;
    }

    private void a()
    {
        android.widget.RelativeLayout$LayoutParams v0_1 = new android.widget.RelativeLayout$LayoutParams(-1, -1);
        this.setBackgroundColor(-1);
        this.setLayoutParams(v0_1);
        return;
    }

    private void a(android.content.Context p5)
    {
        android.graphics.drawable.Drawable v1_2 = new android.widget.RelativeLayout$LayoutParams(-1, ((int) android.util.TypedValue.applyDimension(1, 1077936128, this.getResources().getDisplayMetrics())));
        v1_2.addRule(10, -1);
        android.widget.ImageView v0_5 = new android.widget.ImageView(p5);
        v0_5.setLayoutParams(v1_2);
        v0_5.setBackgroundDrawable(net.hockeyapp.android.e.aa.a());
        this.addView(v0_5);
        return;
    }

    private void a(android.content.Context p5, String p6)
    {
        android.widget.TextView v0_2 = ((int) android.util.TypedValue.applyDimension(1, 1101004800, this.getResources().getDisplayMetrics()));
        int v1_2 = new android.widget.RelativeLayout$LayoutParams(-1, -2);
        v1_2.addRule(13, -1);
        v1_2.setMargins(v0_2, v0_2, v0_2, v0_2);
        android.widget.TextView v0_4 = new android.widget.TextView(p5);
        v0_4.setGravity(17);
        v0_4.setLayoutParams(v1_2);
        v0_4.setText(p6);
        v0_4.setTextColor(-16777216);
        this.addView(v0_4);
        return;
    }
}
