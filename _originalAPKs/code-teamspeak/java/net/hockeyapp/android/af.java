package net.hockeyapp.android;
public class af extends android.app.Activity {
    private static final int a = 1;
    private static final int b = 2;
    private static final int c = 3;
    private net.hockeyapp.android.f.k d;
    private String e;

    public af()
    {
        return;
    }

    private String a(android.net.Uri p7, String p8)
    {
        String v3 = 0;
        String[] v2 = new String[1];
        v2[0] = "_data";
        android.database.Cursor v1_2 = this.getApplicationContext().getContentResolver().query(p7, v2, 0, 0, 0);
        if (v1_2 != null) {
            try {
                if (v1_2.moveToFirst()) {
                    v3 = v1_2.getString(0);
                }
            } catch (java.io.File v0_5) {
                v1_2.close();
                throw v0_5;
            }
            v1_2.close();
        }
        if (v3 != null) {
            p8 = new java.io.File(v3).getName();
        }
        return p8;
    }

    private void a()
    {
        net.hockeyapp.android.ah v3_1 = new java.io.File(this.getCacheDir(), "HockeyApp");
        v3_1.mkdir();
        String v2_4 = new java.io.File(v3_1, new StringBuilder().append(this.e).append(".jpg").toString());
        android.content.Intent v0_7 = 1;
        while (v2_4.exists()) {
            v2_4 = new java.io.File(v3_1, new StringBuilder().append(this.e).append("_").append(v0_7).append(".jpg").toString());
            v0_7++;
        }
        this.d.setDrawingCacheEnabled(1);
        net.hockeyapp.android.ah v3_3 = new net.hockeyapp.android.ah(this, this.d.getDrawingCache());
        android.content.Intent v0_11 = new java.io.File[1];
        v0_11[0] = v2_4;
        v3_3.execute(v0_11);
        android.content.Intent v0_13 = new android.content.Intent();
        v0_13.putExtra("imageUri", android.net.Uri.fromFile(v2_4));
        if (this.getParent() != null) {
            this.getParent().setResult(-1, v0_13);
        } else {
            this.setResult(-1, v0_13);
        }
        this.finish();
        return;
    }

    public void onCreate(android.os.Bundle p10)
    {
        int v1_7;
        super.onCreate(p10);
        android.widget.Toast v0_3 = ((android.net.Uri) this.getIntent().getExtras().getParcelable("imageUri"));
        this.e = this.a(v0_3, v0_3.getLastPathSegment());
        int v4 = this.getResources().getDisplayMetrics().widthPixels;
        int v5 = this.getResources().getDisplayMetrics().heightPixels;
        if (v4 <= v5) {
            v1_7 = 1;
        } else {
            v1_7 = 0;
        }
        int v6_1 = net.hockeyapp.android.f.k.a(this.getContentResolver(), v0_3);
        this.setRequestedOrientation(v6_1);
        if (v1_7 == v6_1) {
            this.d = new net.hockeyapp.android.f.k(this, v0_3, v4, v5);
            android.widget.Toast v0_5 = new android.widget.LinearLayout(this);
            v0_5.setLayoutParams(new android.widget.LinearLayout$LayoutParams(-1, -1));
            v0_5.setGravity(17);
            v0_5.setOrientation(1);
            int v1_13 = new android.widget.LinearLayout(this);
            v1_13.setLayoutParams(new android.widget.LinearLayout$LayoutParams(-1, -1));
            v1_13.setGravity(17);
            v1_13.setOrientation(0);
            v0_5.addView(v1_13);
            v1_13.addView(this.d);
            this.setContentView(v0_5);
            android.widget.Toast.makeText(this, net.hockeyapp.android.aj.a(1536), 1000).show();
        } else {
            android.util.Log.d("HockeyApp", "Image loading skipped because activity will be destroyed for orientation change.");
        }
        return;
    }

    public boolean onCreateOptionsMenu(android.view.Menu p5)
    {
        super.onCreateOptionsMenu(p5);
        p5.add(0, 1, 0, net.hockeyapp.android.aj.a(1537));
        p5.add(0, 2, 0, net.hockeyapp.android.aj.a(1538));
        p5.add(0, 3, 0, net.hockeyapp.android.aj.a(1539));
        return 1;
    }

    public boolean onKeyDown(int p4, android.view.KeyEvent p5)
    {
        if ((p4 != 4) || (this.d.a.empty())) {
            int v0_4 = super.onKeyDown(p4, p5);
        } else {
            int v0_6 = new net.hockeyapp.android.ag(this);
            new android.app.AlertDialog$Builder(this).setMessage(net.hockeyapp.android.aj.a(1540)).setPositiveButton(net.hockeyapp.android.aj.a(1542), v0_6).setNegativeButton(net.hockeyapp.android.aj.a(1541), v0_6).show();
            v0_4 = 1;
        }
        return v0_4;
    }

    public boolean onOptionsItemSelected(android.view.MenuItem p8)
    {
        boolean v1 = 1;
        switch (p8.getItemId()) {
            case 1:
                String v3_1 = new java.io.File(this.getCacheDir(), "HockeyApp");
                v3_1.mkdir();
                android.app.Activity v2_8 = new java.io.File(v3_1, new StringBuilder().append(this.e).append(".jpg").toString());
                android.content.Intent v0_10 = 1;
                while (v2_8.exists()) {
                    v2_8 = new java.io.File(v3_1, new StringBuilder().append(this.e).append("_").append(v0_10).append(".jpg").toString());
                    v0_10++;
                }
                this.d.setDrawingCacheEnabled(1);
                String v3_3 = new net.hockeyapp.android.ah(this, this.d.getDrawingCache());
                android.content.Intent v0_14 = new java.io.File[1];
                v0_14[0] = v2_8;
                v3_3.execute(v0_14);
                android.content.Intent v0_16 = new android.content.Intent();
                v0_16.putExtra("imageUri", android.net.Uri.fromFile(v2_8));
                if (this.getParent() != null) {
                    this.getParent().setResult(-1, v0_16);
                } else {
                    this.setResult(-1, v0_16);
                }
                this.finish();
                break;
            case 2:
                android.content.Intent v0_2 = this.d;
                if (v0_2.a.empty()) {
                } else {
                    v0_2.a.pop();
                    v0_2.invalidate();
                }
                break;
            case 3:
                android.content.Intent v0_1 = this.d;
                v0_1.a.clear();
                v0_1.invalidate();
                break;
            default:
                v1 = super.onOptionsItemSelected(p8);
        }
        return v1;
    }

    public boolean onPrepareOptionsMenu(android.view.Menu p2)
    {
        super.onPrepareOptionsMenu(p2);
        return 1;
    }
}
