package net.hockeyapp.android;
public final class ac {
    public static final int a = 0;
    public static final int b = 1;
    public static final int c = 2;
    public static final int d = 3;
    static final String e = "net.hockeyapp.android.EXIT";
    static Class f;
    static net.hockeyapp.android.ae g;
    private static String h;
    private static String i;
    private static android.os.Handler j;
    private static String k;
    private static int l;

    static ac()
    {
        net.hockeyapp.android.ac.h = 0;
        net.hockeyapp.android.ac.i = 0;
        net.hockeyapp.android.ac.j = 0;
        net.hockeyapp.android.ac.k = 0;
        return;
    }

    public ac()
    {
        return;
    }

    private static String a(int p3)
    {
        String v0_0 = "";
        if (p3 != 2) {
            if (p3 != 1) {
                if (p3 == 3) {
                    v0_0 = "validate";
                }
            } else {
                v0_0 = "check";
            }
        } else {
            v0_0 = "authorize";
        }
        return new StringBuilder().append(net.hockeyapp.android.ac.k).append("api/3/apps/").append(net.hockeyapp.android.ac.h).append("/identity/").append(v0_0).toString();
    }

    private static void a(android.app.Activity p8, android.content.Intent p9)
    {
        String v0_0 = 1;
        if ((p9 == null) || (!p9.getBooleanExtra("net.hockeyapp.android.EXIT", 0))) {
            if ((p8 != null) && ((net.hockeyapp.android.ac.l != 0) && (net.hockeyapp.android.ac.l != 3))) {
                String v1_5 = p8.getSharedPreferences("net.hockeyapp.android.login", 0);
                if (v1_5.getInt("mode", -1) != net.hockeyapp.android.ac.l) {
                    net.hockeyapp.android.e.n.a(v1_5.edit().remove("auid").remove("iuid").putInt("mode", net.hockeyapp.android.ac.l));
                }
                android.os.Handler v2_8;
                String v3_5 = v1_5.getString("auid", 0);
                String v7_1 = v1_5.getString("iuid", 0);
                if ((v3_5 != null) || (v7_1 != null)) {
                    v2_8 = 0;
                } else {
                    v2_8 = 1;
                }
                if ((v3_5 != null) || (net.hockeyapp.android.ac.l != 2)) {
                    String v1_7 = 0;
                } else {
                    v1_7 = 1;
                }
                if ((v7_1 != null) || (net.hockeyapp.android.ac.l != 1)) {
                    v0_0 = 0;
                }
                if ((v2_8 == null) && ((v1_7 == null) && (v0_0 == null))) {
                    java.util.HashMap v5_4 = new java.util.HashMap();
                    if (v3_5 == null) {
                        if (v7_1 != null) {
                            v5_4.put("type", "iuid");
                            v5_4.put("id", v7_1);
                        }
                    } else {
                        v5_4.put("type", "auid");
                        v5_4.put("id", v3_5);
                    }
                    String v0_6 = new net.hockeyapp.android.d.p(p8, net.hockeyapp.android.ac.j, net.hockeyapp.android.ac.a(3), 3, v5_4);
                    v0_6.d = 0;
                    net.hockeyapp.android.e.a.a(v0_6);
                } else {
                    net.hockeyapp.android.ac.b(p8);
                }
            }
        } else {
            p8.finish();
        }
        return;
    }

    static synthetic void a(android.content.Context p0)
    {
        net.hockeyapp.android.ac.b(p0);
        return;
    }

    private static void a(android.content.Context p2, String p3, String p4, int p5)
    {
        if (p2 != null) {
            net.hockeyapp.android.ac.h = net.hockeyapp.android.e.w.c(p3);
            net.hockeyapp.android.ac.i = p4;
            net.hockeyapp.android.ac.k = "https://sdk.hockeyapp.net/";
            net.hockeyapp.android.ac.l = p5;
            net.hockeyapp.android.ac.f = 0;
            if (net.hockeyapp.android.ac.j == null) {
                net.hockeyapp.android.ac.j = new net.hockeyapp.android.ad(p2);
            }
            net.hockeyapp.android.a.a(p2);
        }
        return;
    }

    private static void a(android.content.Context p2, String p3, String p4, int p5, net.hockeyapp.android.ae p6)
    {
        net.hockeyapp.android.ac.g = p6;
        if (p2 != null) {
            net.hockeyapp.android.ac.h = net.hockeyapp.android.e.w.c(p3);
            net.hockeyapp.android.ac.i = p4;
            net.hockeyapp.android.ac.k = "https://sdk.hockeyapp.net/";
            net.hockeyapp.android.ac.l = p5;
            net.hockeyapp.android.ac.f = 0;
            if (net.hockeyapp.android.ac.j == null) {
                net.hockeyapp.android.ac.j = new net.hockeyapp.android.ad(p2);
            }
            net.hockeyapp.android.a.a(p2);
        }
        return;
    }

    private static void a(android.content.Context p1, String p2, String p3, String p4, int p5, Class p6)
    {
        if (p1 != null) {
            net.hockeyapp.android.ac.h = net.hockeyapp.android.e.w.c(p2);
            net.hockeyapp.android.ac.i = p3;
            net.hockeyapp.android.ac.k = p4;
            net.hockeyapp.android.ac.l = p5;
            net.hockeyapp.android.ac.f = p6;
            if (net.hockeyapp.android.ac.j == null) {
                net.hockeyapp.android.ac.j = new net.hockeyapp.android.ad(p1);
            }
            net.hockeyapp.android.a.a(p1);
        }
        return;
    }

    private static void b(android.content.Context p3)
    {
        android.content.Intent v0_1 = new android.content.Intent();
        v0_1.setFlags(1342177280);
        v0_1.setClass(p3, net.hockeyapp.android.aa);
        v0_1.putExtra("url", net.hockeyapp.android.ac.a(net.hockeyapp.android.ac.l));
        v0_1.putExtra("mode", net.hockeyapp.android.ac.l);
        v0_1.putExtra("secret", net.hockeyapp.android.ac.i);
        p3.startActivity(v0_1);
        return;
    }
}
