package net.hockeyapp.android.e;
public final class w {
    public static final String a = "net.hockeyapp.android.prefs_feedback_token";
    public static final String b = "net.hockeyapp.android.prefs_key_feedback_token";
    public static final String c = "net.hockeyapp.android.prefs_name_email";
    public static final String d = "net.hockeyapp.android.prefs_key_name_email";
    public static final String e = "[0-9a-f]+";
    public static final int f = 32;
    public static final String g = "HockeyApp";
    private static final java.util.regex.Pattern h;

    static w()
    {
        net.hockeyapp.android.e.w.h = java.util.regex.Pattern.compile("[0-9a-f]+", 2);
        return;
    }

    public w()
    {
        return;
    }

    public static android.app.Notification a(android.content.Context p3, android.app.PendingIntent p4, String p5, String p6, int p7)
    {
        if ((android.os.Build$VERSION.SDK_INT < 11) || (!net.hockeyapp.android.e.w.d("android.app.Notification.Builder"))) {
            android.app.Notification v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        android.app.Notification v0_4;
        if (v0_3 == null) {
            v0_4 = net.hockeyapp.android.e.w.b(p3, p4, p5, p6, p7);
        } else {
            android.app.Notification v0_10 = new android.app.Notification$Builder(p3).setContentTitle(p5).setContentText(p6).setContentIntent(p4).setSmallIcon(p7);
            if (android.os.Build$VERSION.SDK_INT >= 16) {
                v0_4 = v0_10.build();
            } else {
                v0_4 = v0_10.getNotification();
            }
        }
        return v0_4;
    }

    public static Boolean a()
    {
        try {
            if ((android.os.Build$VERSION.SDK_INT < 11) || (!net.hockeyapp.android.e.w.d("android.app.Fragment"))) {
                Boolean v0_3 = 0;
                Boolean v0_4 = Boolean.valueOf(v0_3);
            } else {
                v0_3 = 1;
            }
        } catch (Boolean v0) {
            v0_4 = Boolean.valueOf(0);
        }
        return v0_4;
    }

    public static Boolean a(ref.WeakReference p4)
    {
        Boolean v0_2;
        Boolean v0_1 = ((android.app.Activity) p4.get());
        if (v0_1 == null) {
            v0_2 = Boolean.valueOf(0);
        } else {
            Boolean v0_7;
            Boolean v0_4 = v0_1.getResources().getConfiguration();
            if (((v0_4.screenLayout & 15) != 3) && ((v0_4.screenLayout & 15) != 4)) {
                v0_7 = 0;
            } else {
                v0_7 = 1;
            }
            v0_2 = Boolean.valueOf(v0_7);
        }
        return v0_2;
    }

    public static String a(String p1)
    {
        try {
            String v0_1 = java.net.URLEncoder.encode(p1, "UTF-8");
        } catch (String v0_2) {
            v0_2.printStackTrace();
            v0_1 = "";
        }
        return v0_1;
    }

    private static String a(java.util.Map p5)
    {
        java.util.ArrayList v2_1 = new java.util.ArrayList();
        java.util.Iterator v3 = p5.keySet().iterator();
        while (v3.hasNext()) {
            String v0_5 = ((String) v3.next());
            v2_1.add(new StringBuilder().append(java.net.URLEncoder.encode(v0_5, "UTF-8")).append("=").append(java.net.URLEncoder.encode(((String) p5.get(v0_5)), "UTF-8")).toString());
        }
        return android.text.TextUtils.join("&", v2_1);
    }

    private static android.app.Notification b(android.content.Context p6, android.app.PendingIntent p7, String p8, String p9, int p10)
    {
        android.app.Notification v0_1 = new android.app.Notification(p10, "", System.currentTimeMillis());
        try {
            Exception v1_1 = v0_1.getClass();
            int v3_1 = new Class[4];
            v3_1[0] = android.content.Context;
            v3_1[1] = CharSequence;
            v3_1[2] = CharSequence;
            v3_1[3] = android.app.PendingIntent;
            Exception v1_2 = v1_1.getMethod("setLatestEventInfo", v3_1);
            Object[] v2_3 = new Object[4];
            v2_3[0] = p6;
            v2_3[1] = p8;
            v2_3[2] = p9;
            v2_3[3] = p7;
            v1_2.invoke(v0_1, v2_3);
        } catch (Exception v1) {
        }
        return v0_1;
    }

    private static boolean b()
    {
        if ((android.os.Build$VERSION.SDK_INT < 11) || (!net.hockeyapp.android.e.w.d("android.app.Notification.Builder"))) {
            int v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        return v0_3;
    }

    public static final boolean b(String p4)
    {
        int v0 = 1;
        if (android.os.Build$VERSION.SDK_INT < 8) {
            if (android.text.TextUtils.isEmpty(p4)) {
                v0 = 0;
            }
        } else {
            if ((android.text.TextUtils.isEmpty(p4)) || (!android.util.Patterns.EMAIL_ADDRESS.matcher(p4).matches())) {
                v0 = 0;
            }
        }
        return v0;
    }

    private static android.app.Notification c(android.content.Context p3, android.app.PendingIntent p4, String p5, String p6, int p7)
    {
        android.app.Notification v0_6;
        android.app.Notification v0_5 = new android.app.Notification$Builder(p3).setContentTitle(p5).setContentText(p6).setContentIntent(p4).setSmallIcon(p7);
        if (android.os.Build$VERSION.SDK_INT >= 16) {
            v0_6 = v0_5.build();
        } else {
            v0_6 = v0_5.getNotification();
        }
        return v0_6;
    }

    public static String c(String p4)
    {
        if (p4 != null) {
            IllegalArgumentException v0_0 = p4.trim();
            String v1_1 = net.hockeyapp.android.e.w.h.matcher(v0_0);
            if (v0_0.length() == 32) {
                if (v1_1.matches()) {
                    return v0_0;
                } else {
                    throw new IllegalArgumentException("App ID must match regex pattern /[0-9a-f]+/i");
                }
            } else {
                throw new IllegalArgumentException("App ID length must be 32 characters.");
            }
        } else {
            throw new IllegalArgumentException("App ID must not be null.");
        }
    }

    private static boolean d(String p2)
    {
        int v0 = 0;
        try {
            if (Class.forName(p2) != null) {
                v0 = 1;
            }
        } catch (ClassNotFoundException v1) {
        }
        return v0;
    }
}
