package net.hockeyapp.android.e;
public class b {
    public static final int a = 0;
    public static final int b = 1;
    public static final int c = 2;
    public static final int d = 4;
    public static final int e = 8;
    static final synthetic boolean f;

    static b()
    {
        int v0_2;
        if (net.hockeyapp.android.e.b.desiredAssertionStatus()) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        net.hockeyapp.android.e.b.f = v0_2;
        return;
    }

    private b()
    {
        return;
    }

    public static String a(byte[] p4)
    {
        try {
            return new String(net.hockeyapp.android.e.b.b(p4, 0, p4.length, 2), "US-ASCII");
        } catch (java.io.UnsupportedEncodingException v0_2) {
            throw new AssertionError(v0_2);
        }
    }

    private static String a(byte[] p3, int p4, int p5, int p6)
    {
        try {
            return new String(net.hockeyapp.android.e.b.b(p3, p4, p5, p6), "US-ASCII");
        } catch (java.io.UnsupportedEncodingException v0_2) {
            throw new AssertionError(v0_2);
        }
    }

    private static byte[] a(String p5, int p6)
    {
        byte[] v0_0 = p5.getBytes();
        byte[] v1_0 = v0_0.length;
        byte[] v3_2 = new byte[((v1_0 * 3) / 4)];
        int v2_1 = new net.hockeyapp.android.e.d(p6, v3_2);
        if (v2_1.a(v0_0, 0, v1_0)) {
            byte[] v0_4;
            if (v2_1.b != v2_1.a.length) {
                v0_4 = new byte[v2_1.b];
                System.arraycopy(v2_1.a, 0, v0_4, 0, v2_1.b);
            } else {
                v0_4 = v2_1.a;
            }
            return v0_4;
        } else {
            throw new IllegalArgumentException("bad base-64");
        }
    }

    private static byte[] a(byte[] p4, int p5)
    {
        byte[] v0_0 = p4.length;
        byte[] v2_2 = new byte[((v0_0 * 3) / 4)];
        int v1_1 = new net.hockeyapp.android.e.d(p5, v2_2);
        if (v1_1.a(p4, 0, v0_0)) {
            byte[] v0_4;
            if (v1_1.b != v1_1.a.length) {
                v0_4 = new byte[v1_1.b];
                System.arraycopy(v1_1.a, 0, v0_4, 0, v1_1.b);
            } else {
                v0_4 = v1_1.a;
            }
            return v0_4;
        } else {
            throw new IllegalArgumentException("bad base-64");
        }
    }

    private static byte[] a(byte[] p4, int p5, int p6)
    {
        byte[] v0_2 = new byte[((p5 * 3) / 4)];
        int v1_1 = new net.hockeyapp.android.e.d(p6, v0_2);
        if (v1_1.a(p4, 0, p5)) {
            byte[] v0_6;
            if (v1_1.b != v1_1.a.length) {
                v0_6 = new byte[v1_1.b];
                System.arraycopy(v1_1.a, 0, v0_6, 0, v1_1.b);
            } else {
                v0_6 = v1_1.a;
            }
            return v0_6;
        } else {
            throw new IllegalArgumentException("bad base-64");
        }
    }

    private static byte[] b(byte[] p2, int p3)
    {
        return net.hockeyapp.android.e.b.b(p2, 0, p2.length, p3);
    }

    private static byte[] b(byte[] p4, int p5, int p6, int p7)
    {
        net.hockeyapp.android.e.e v2_1 = new net.hockeyapp.android.e.e(p7);
        int v0_1 = ((p6 / 3) * 4);
        if (!v2_1.e) {
            switch ((p6 % 3)) {
                case 0:
                    break;
                case 1:
                    v0_1 += 2;
                    break;
                case 2:
                    v0_1 += 3;
                    break;
                default:
            }
        } else {
            if ((p6 % 3) > 0) {
                v0_1 += 4;
            }
        }
        if ((v2_1.f) && (p6 > 0)) {
            int v1_7;
            if (!v2_1.g) {
                v1_7 = 1;
            } else {
                v1_7 = 2;
            }
            v0_1 += (v1_7 * (((p6 - 1) / 57) + 1));
        }
        int v1_9 = new byte[v0_1];
        v2_1.a = v1_9;
        v2_1.a(p4, p5, p6);
        if ((net.hockeyapp.android.e.b.f) || (v2_1.b == v0_1)) {
            return v2_1.a;
        } else {
            throw new AssertionError();
        }
    }
}
