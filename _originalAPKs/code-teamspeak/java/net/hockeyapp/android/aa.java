package net.hockeyapp.android;
public class aa extends android.app.Activity implements android.view.View$OnClickListener {
    private String a;
    private String b;
    private int c;
    private net.hockeyapp.android.d.p d;
    private android.os.Handler e;

    public aa()
    {
        return;
    }

    private static String a(String p7)
    {
        try {
            int v0_1 = java.security.MessageDigest.getInstance("MD5");
            v0_1.update(p7.getBytes());
            byte[] v2 = v0_1.digest();
            StringBuilder v3_1 = new StringBuilder();
            int v4 = v2.length;
            int v1_1 = 0;
        } catch (int v0_9) {
            v0_9.printStackTrace();
            int v0_3 = "";
            return v0_3;
        }
        while (v1_1 < v4) {
            int v0_6 = Integer.toHexString((v2[v1_1] & 255));
            while (v0_6.length() < 2) {
                v0_6 = new StringBuilder("0").append(v0_6).toString();
            }
            v3_1.append(v0_6);
            v1_1++;
        }
        v0_3 = v3_1.toString();
        return v0_3;
    }

    private void a()
    {
        if (this.c == 1) {
            ((android.widget.EditText) this.findViewById(12292)).setVisibility(4);
        }
        ((android.widget.Button) this.findViewById(12293)).setOnClickListener(this);
        return;
    }

    private void b()
    {
        this.e = new net.hockeyapp.android.ab(this);
        return;
    }

    private void c()
    {
        android.widget.Toast v0_9;
        int v1_0 = 1;
        String v3_0 = ((android.widget.EditText) this.findViewById(12291)).getText().toString();
        android.widget.Toast v0_8 = ((android.widget.EditText) this.findViewById(12292)).getText().toString();
        java.util.HashMap v5_1 = new java.util.HashMap();
        if (this.c != 1) {
            if (this.c != 2) {
                v0_9 = 0;
            } else {
                if ((android.text.TextUtils.isEmpty(v3_0)) || (android.text.TextUtils.isEmpty(v0_8))) {
                    v1_0 = 0;
                }
                v5_1.put("email", v3_0);
                v5_1.put("password", v0_8);
                v0_9 = v1_0;
            }
        } else {
            if (android.text.TextUtils.isEmpty(v3_0)) {
                v0_9 = 0;
            } else {
                v0_9 = 1;
            }
            v5_1.put("email", v3_0);
            v5_1.put("authcode", net.hockeyapp.android.aa.a(new StringBuilder().append(this.b).append(v3_0).toString()));
        }
        if (v0_9 == null) {
            android.widget.Toast.makeText(this, net.hockeyapp.android.aj.a(1281), 1000).show();
        } else {
            this.d = new net.hockeyapp.android.d.p(this, this.e, this.a, this.c, v5_1);
            net.hockeyapp.android.e.a.a(this.d);
        }
        return;
    }

    public void onClick(android.view.View p8)
    {
        int v1_0 = 1;
        switch (p8.getId()) {
            case 12293:
                android.widget.Toast v0_10;
                String v3_0 = ((android.widget.EditText) this.findViewById(12291)).getText().toString();
                android.widget.Toast v0_9 = ((android.widget.EditText) this.findViewById(12292)).getText().toString();
                java.util.HashMap v5_1 = new java.util.HashMap();
                if (this.c != 1) {
                    if (this.c != 2) {
                        v0_10 = 0;
                    } else {
                        if ((android.text.TextUtils.isEmpty(v3_0)) || (android.text.TextUtils.isEmpty(v0_9))) {
                            v1_0 = 0;
                        }
                        v5_1.put("email", v3_0);
                        v5_1.put("password", v0_9);
                        v0_10 = v1_0;
                    }
                } else {
                    if (android.text.TextUtils.isEmpty(v3_0)) {
                        v0_10 = 0;
                    } else {
                        v0_10 = 1;
                    }
                    v5_1.put("email", v3_0);
                    v5_1.put("authcode", net.hockeyapp.android.aa.a(new StringBuilder().append(this.b).append(v3_0).toString()));
                }
                if (v0_10 == null) {
                    android.widget.Toast.makeText(this, net.hockeyapp.android.aj.a(1281), 1000).show();
                } else {
                    this.d = new net.hockeyapp.android.d.p(this, this.e, this.a, this.c, v5_1);
                    net.hockeyapp.android.e.a.a(this.d);
                }
                break;
        }
        return;
    }

    protected void onCreate(android.os.Bundle p3)
    {
        super.onCreate(p3);
        this.setContentView(new net.hockeyapp.android.f.j(this));
        net.hockeyapp.android.d.p v0_3 = this.getIntent().getExtras();
        if (v0_3 != null) {
            this.a = v0_3.getString("url");
            this.b = v0_3.getString("secret");
            this.c = v0_3.getInt("mode");
        }
        if (this.c == 1) {
            ((android.widget.EditText) this.findViewById(12292)).setVisibility(4);
        }
        ((android.widget.Button) this.findViewById(12293)).setOnClickListener(this);
        this.e = new net.hockeyapp.android.ab(this);
        net.hockeyapp.android.d.p v0_14 = this.getLastNonConfigurationInstance();
        if (v0_14 != null) {
            this.d = ((net.hockeyapp.android.d.p) v0_14);
            net.hockeyapp.android.d.p v0_16 = this.d;
            android.os.Handler v1_7 = this.e;
            v0_16.a = this;
            v0_16.b = v1_7;
        }
        return;
    }

    public boolean onKeyDown(int p4, android.view.KeyEvent p5)
    {
        boolean v0 = 1;
        if ((p4 != 4) || (net.hockeyapp.android.ac.g != null)) {
            v0 = super.onKeyDown(p4, p5);
        } else {
            android.content.Intent v1_3 = new android.content.Intent(this, net.hockeyapp.android.ac.f);
            v1_3.setFlags(67108864);
            v1_3.putExtra("net.hockeyapp.android.EXIT", 1);
            this.startActivity(v1_3);
        }
        return v0;
    }

    public Object onRetainNonConfigurationInstance()
    {
        if (this.d != null) {
            net.hockeyapp.android.d.p v0_1 = this.d;
            v0_1.a = 0;
            v0_1.b = 0;
            v0_1.c = 0;
        }
        return this.d;
    }
}
