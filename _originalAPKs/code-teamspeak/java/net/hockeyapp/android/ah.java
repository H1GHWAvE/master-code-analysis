package net.hockeyapp.android;
final class ah extends android.os.AsyncTask {
    final synthetic android.graphics.Bitmap a;
    final synthetic net.hockeyapp.android.af b;

    ah(net.hockeyapp.android.af p1, android.graphics.Bitmap p2)
    {
        this.b = p1;
        this.a = p2;
        return;
    }

    private varargs Void a(java.io.File[] p5)
    {
        try {
            int v0_1 = new java.io.FileOutputStream(p5[0]);
            this.a.compress(android.graphics.Bitmap$CompressFormat.JPEG, 100, v0_1);
            v0_1.close();
        } catch (int v0_2) {
            v0_2.printStackTrace();
            android.util.Log.e("HockeyApp", "Could not save image.", v0_2);
        }
        return 0;
    }

    protected final synthetic Object doInBackground(Object[] p2)
    {
        return this.a(((java.io.File[]) p2));
    }
}
