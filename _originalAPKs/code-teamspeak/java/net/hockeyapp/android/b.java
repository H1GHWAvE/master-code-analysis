package net.hockeyapp.android;
public final class b {
    private static String a = "None";
    private static String b = "None";
    private static boolean c = False;
    private static final String d = "always_send_crash_reports";

    static b()
    {
        net.hockeyapp.android.b.a = 0;
        net.hockeyapp.android.b.b = 0;
        net.hockeyapp.android.b.c = 0;
        return;
    }

    public b()
    {
        return;
    }

    private static int a(ref.WeakReference p6)
    {
        int v0_1;
        int v2 = 0;
        String[] v3 = net.hockeyapp.android.b.c();
        int v1 = 0;
        if ((v3 == null) || (v3.length <= 0)) {
            v0_1 = 0;
        } else {
            try {
                int v0_4;
                int v0_3 = ((android.content.Context) p6.get());
            } catch (int v0) {
                if (v1 != 0) {
                    v0_1 = 2;
                    while (v2 < v3.length) {
                        if (v1.contains(v3[v2])) {
                            v2++;
                        }
                    }
                    return v0_1;
                }
                v0_1 = 1;
            }
            if (v0_3 == 0) {
                v0_4 = 0;
            } else {
                v0_4 = java.util.Arrays.asList(v0_3.getSharedPreferences("HockeySDK", 0).getString("ConfirmedFilenames", "").split("\\|"));
            }
            v1 = v0_4;
        }
        return v0_1;
    }

    private static String a(String[] p3, String p4)
    {
        StringBuffer v1_1 = new StringBuffer();
        int v0_0 = 0;
        while (v0_0 < p3.length) {
            v1_1.append(p3[v0_0]);
            if (v0_0 < (p3.length - 1)) {
                v1_1.append(p4);
            }
            v0_0++;
        }
        return v1_1.toString();
    }

    public static void a(android.content.Context p2, String p3)
    {
        net.hockeyapp.android.b.a(p2, "https://sdk.hockeyapp.net/", p3, 0);
        return;
    }

    private static void a(android.content.Context p8, String p9, String p10, net.hockeyapp.android.i p11)
    {
        net.hockeyapp.android.b.a(p8, p9, p10, p11, 0);
        boolean v3_0 = Boolean.valueOf(0);
        ref.WeakReference v4_1 = new ref.WeakReference(p8);
        android.app.AlertDialog v0_0 = net.hockeyapp.android.b.a(v4_1);
        if (v0_0 != 1) {
            if (v0_0 != 2) {
                net.hockeyapp.android.b.a(p11, v3_0.booleanValue());
            } else {
                net.hockeyapp.android.b.c(v4_1, p11, v3_0.booleanValue());
            }
        } else {
            android.app.AlertDialog v0_4;
            if ((p8 instanceof android.app.Activity)) {
                v0_4 = 0;
            } else {
                v0_4 = 1;
            }
            android.app.AlertDialog v0_8 = Boolean.valueOf((Boolean.valueOf(v0_4).booleanValue() | android.preference.PreferenceManager.getDefaultSharedPreferences(p8).getBoolean("always_send_crash_reports", 0)));
            if (p11 != null) {
                v0_8 = Boolean.valueOf((Boolean.valueOf((v0_8.booleanValue() | 0)).booleanValue() | 0));
            }
            if (v0_8.booleanValue()) {
                net.hockeyapp.android.b.c(v4_1, p11, v3_0.booleanValue());
            } else {
                boolean v3_1 = v3_0.booleanValue();
                android.app.AlertDialog v0_17 = ((android.content.Context) v4_1.get());
                if (v0_17 != null) {
                    android.app.AlertDialog$Builder v5_3 = new android.app.AlertDialog$Builder(v0_17);
                    v5_3.setTitle(net.hockeyapp.android.aj.a(p11, 0));
                    v5_3.setMessage(net.hockeyapp.android.aj.a(p11, 1));
                    v5_3.setNegativeButton(net.hockeyapp.android.aj.a(p11, 2), new net.hockeyapp.android.c(p11, v4_1, v3_1));
                    v5_3.setNeutralButton(net.hockeyapp.android.aj.a(p11, 3), new net.hockeyapp.android.d(p11, v4_1, v3_1));
                    v5_3.setPositiveButton(net.hockeyapp.android.aj.a(p11, 4), new net.hockeyapp.android.e(p11, v4_1, v3_1));
                    v5_3.create().show();
                }
            }
        }
        return;
    }

    private static void a(android.content.Context p2, String p3, String p4, net.hockeyapp.android.i p5, boolean p6)
    {
        if (p2 != null) {
            net.hockeyapp.android.b.b = p3;
            net.hockeyapp.android.b.a = net.hockeyapp.android.e.w.c(p4);
            net.hockeyapp.android.a.a(p2);
            if (net.hockeyapp.android.b.a == null) {
                net.hockeyapp.android.b.a = net.hockeyapp.android.a.d;
            }
            if (p6) {
                boolean v0_4 = Boolean.valueOf(0);
                new ref.WeakReference(p2);
                net.hockeyapp.android.b.a(p5, v0_4.booleanValue());
            }
        }
        return;
    }

    private static void a(android.content.Context p1, String p2, net.hockeyapp.android.i p3)
    {
        net.hockeyapp.android.b.a(p1, "https://sdk.hockeyapp.net/", p2, p3);
        return;
    }

    private static void a(android.content.Context p8, net.hockeyapp.android.i p9)
    {
        boolean v3_0 = Boolean.valueOf(0);
        ref.WeakReference v4_1 = new ref.WeakReference(p8);
        android.app.AlertDialog v0_0 = net.hockeyapp.android.b.a(v4_1);
        if (v0_0 != 1) {
            if (v0_0 != 2) {
                net.hockeyapp.android.b.a(p9, v3_0.booleanValue());
            } else {
                net.hockeyapp.android.b.c(v4_1, p9, v3_0.booleanValue());
            }
        } else {
            android.app.AlertDialog v0_4;
            if ((p8 instanceof android.app.Activity)) {
                v0_4 = 0;
            } else {
                v0_4 = 1;
            }
            android.app.AlertDialog v0_8 = Boolean.valueOf((Boolean.valueOf(v0_4).booleanValue() | android.preference.PreferenceManager.getDefaultSharedPreferences(p8).getBoolean("always_send_crash_reports", 0)));
            if (p9 != null) {
                v0_8 = Boolean.valueOf((Boolean.valueOf((v0_8.booleanValue() | 0)).booleanValue() | 0));
            }
            if (v0_8.booleanValue()) {
                net.hockeyapp.android.b.c(v4_1, p9, v3_0.booleanValue());
            } else {
                boolean v3_1 = v3_0.booleanValue();
                android.app.AlertDialog v0_17 = ((android.content.Context) v4_1.get());
                if (v0_17 != null) {
                    android.app.AlertDialog$Builder v5_3 = new android.app.AlertDialog$Builder(v0_17);
                    v5_3.setTitle(net.hockeyapp.android.aj.a(p9, 0));
                    v5_3.setMessage(net.hockeyapp.android.aj.a(p9, 1));
                    v5_3.setNegativeButton(net.hockeyapp.android.aj.a(p9, 2), new net.hockeyapp.android.c(p9, v4_1, v3_1));
                    v5_3.setNeutralButton(net.hockeyapp.android.aj.a(p9, 3), new net.hockeyapp.android.d(p9, v4_1, v3_1));
                    v5_3.setPositiveButton(net.hockeyapp.android.aj.a(p9, 4), new net.hockeyapp.android.e(p9, v4_1, v3_1));
                    v5_3.create().show();
                }
            }
        }
        return;
    }

    private static void a(ref.WeakReference p3, String p4)
    {
        if (p3 != null) {
            android.content.SharedPreferences$Editor v0_1 = ((android.content.Context) p3.get());
            if (v0_1 != null) {
                android.content.SharedPreferences$Editor v0_3 = v0_1.getSharedPreferences("HockeySDK", 0).edit();
                v0_3.remove(new StringBuilder("RETRY_COUNT: ").append(p4).toString());
                v0_3.commit();
            }
        }
        return;
    }

    private static void a(ref.WeakReference p5, String p6, int p7)
    {
        if ((p7 != -1) && (p5 != null)) {
            int v0_2 = ((android.content.Context) p5.get());
            if (v0_2 != 0) {
                int v0_3 = v0_2.getSharedPreferences("HockeySDK", 0);
                android.content.SharedPreferences$Editor v1_1 = v0_3.edit();
                int v0_4 = v0_3.getInt(new StringBuilder("RETRY_COUNT: ").append(p6).toString(), 0);
                if (v0_4 < p7) {
                    v1_1.putInt(new StringBuilder("RETRY_COUNT: ").append(p6).toString(), (v0_4 + 1));
                    v1_1.commit();
                } else {
                    net.hockeyapp.android.b.b(p5, p6);
                    net.hockeyapp.android.b.a(p5, p6);
                }
            }
        }
        return;
    }

    private static void a(ref.WeakReference p1, net.hockeyapp.android.i p2)
    {
        net.hockeyapp.android.b.a(p1, p2, 0);
        return;
    }

    public static void a(ref.WeakReference p13, net.hockeyapp.android.i p14, net.hockeyapp.android.c.b p15)
    {
        String[] v6 = net.hockeyapp.android.b.c();
        String v1_0 = Boolean.valueOf(0);
        if ((v6 != null) && (v6.length > 0)) {
            android.util.Log.d("HockeyApp", new StringBuilder("Found ").append(v6.length).append(" stacktrace(s).").toString());
            Boolean v2_5 = v1_0;
            String v1_1 = 0;
            while (v1_1 < v6.length) {
                String v3_3 = 0;
                try {
                    String v7_0 = v6[v1_1];
                    String v8_0 = net.hockeyapp.android.b.c(p13, v7_0);
                } catch (int v0_16) {
                    int v0_17 = 0;
                    String v3_4 = v0_16;
                    if (v0_17 != 0) {
                        v0_17.disconnect();
                    }
                    if (!v2_5.booleanValue()) {
                        android.util.Log.d("HockeyApp", "Transmission failed, will retry on next register() call");
                        if (p14 != null) {
                            String v1_2 = v6[v1_1];
                            if (p13 != null) {
                                int v0_40 = ((android.content.Context) p13.get());
                                if (v0_40 != 0) {
                                    int v0_41 = v0_40.getSharedPreferences("HockeySDK", 0);
                                    Boolean v2_8 = v0_41.edit();
                                    int v0_42 = v0_41.getInt(new StringBuilder("RETRY_COUNT: ").append(v1_2).toString(), 0);
                                    if (v0_42 <= 0) {
                                        v2_8.putInt(new StringBuilder("RETRY_COUNT: ").append(v1_2).toString(), (v0_42 + 1));
                                        v2_8.commit();
                                    } else {
                                        net.hockeyapp.android.b.b(p13, v1_2);
                                        net.hockeyapp.android.b.a(p13, v1_2);
                                    }
                                }
                            }
                        }
                    } else {
                        android.util.Log.d("HockeyApp", "Transmission succeeded");
                        net.hockeyapp.android.b.b(p13, v6[v1_1]);
                        if (p14 != null) {
                            net.hockeyapp.android.b.a(p13, v6[v1_1]);
                        }
                    }
                    throw v3_4;
                } catch (int v0_25) {
                    v0_25.printStackTrace();
                    if (v3_3 != null) {
                        v3_3.disconnect();
                    }
                    if (!v2_5.booleanValue()) {
                        android.util.Log.d("HockeyApp", "Transmission failed, will retry on next register() call");
                        if (p14 == null) {
                            v1_1++;
                        } else {
                            String v3_6 = v6[v1_1];
                            if (p13 == null) {
                            } else {
                                int v0_29 = ((android.content.Context) p13.get());
                                if (v0_29 == 0) {
                                } else {
                                    int v0_30 = v0_29.getSharedPreferences("HockeySDK", 0);
                                    int v4_22 = v0_30.edit();
                                    int v0_31 = v0_30.getInt(new StringBuilder("RETRY_COUNT: ").append(v3_6).toString(), 0);
                                    if (v0_31 <= 0) {
                                        v4_22.putInt(new StringBuilder("RETRY_COUNT: ").append(v3_6).toString(), (v0_31 + 1));
                                        v4_22.commit();
                                    } else {
                                        net.hockeyapp.android.b.b(p13, v3_6);
                                        net.hockeyapp.android.b.a(p13, v3_6);
                                    }
                                }
                            }
                        }
                    } else {
                        android.util.Log.d("HockeyApp", "Transmission succeeded");
                        net.hockeyapp.android.b.b(p13, v6[v1_1]);
                        if (p14 == null) {
                        } else {
                            int v0_35 = v6[v1_1];
                            net.hockeyapp.android.b.a(p13, v0_35);
                        }
                    }
                    v2_5 = Boolean.valueOf(int v0_24);
                    if (v3_3 != null) {
                        v3_3.disconnect();
                    }
                    if (!v2_5.booleanValue()) {
                        android.util.Log.d("HockeyApp", "Transmission failed, will retry on next register() call");
                        if (p14 == null) {
                        } else {
                            v3_6 = v6[v1_1];
                            if (p13 == null) {
                            } else {
                                int v0_50 = ((android.content.Context) p13.get());
                                if (v0_50 == 0) {
                                } else {
                                    int v0_51 = v0_50.getSharedPreferences("HockeySDK", 0);
                                    v4_22 = v0_51.edit();
                                    int v0_52 = v0_51.getInt(new StringBuilder("RETRY_COUNT: ").append(v3_6).toString(), 0);
                                    if (v0_52 <= 0) {
                                        v4_22.putInt(new StringBuilder("RETRY_COUNT: ").append(v3_6).toString(), (v0_52 + 1));
                                    } else {
                                        net.hockeyapp.android.b.b(p13, v3_6);
                                    }
                                }
                            }
                        }
                    } else {
                        android.util.Log.d("HockeyApp", "Transmission succeeded");
                        net.hockeyapp.android.b.b(p13, v6[v1_1]);
                        if (p14 == null) {
                        } else {
                            v0_35 = v6[v1_1];
                        }
                    }
                } catch (int v0_36) {
                    v0_17 = v3_3;
                    v3_4 = v0_36;
                }
                if (v8_0.length() > 0) {
                    StringBuilder v5_2;
                    android.util.Log.d("HockeyApp", new StringBuilder("Transmitting crash data: \n").append(v8_0).toString());
                    int v0_9 = net.hockeyapp.android.b.c(p13, v7_0.replace(".stacktrace", ".user"));
                    int v4_7 = net.hockeyapp.android.b.c(p13, v7_0.replace(".stacktrace", ".contact"));
                    if (p15 == null) {
                        v5_2 = v0_9;
                    } else {
                        v5_2 = p15.c;
                        if ((v5_2 == null) || (v5_2.length() <= 0)) {
                            v5_2 = v0_9;
                        }
                        int v0_10 = p15.b;
                        if ((v0_10 != 0) && (v0_10.length() > 0)) {
                            v4_7 = v0_10;
                        }
                    }
                    int v0_13;
                    String v7_1 = net.hockeyapp.android.b.c(p13, v7_0.replace(".stacktrace", ".description"));
                    if (p15 == null) {
                        v0_13 = "";
                    } else {
                        v0_13 = p15.a;
                    }
                    if ((v7_1 != null) && (v7_1.length() > 0)) {
                        if ((v0_13 == 0) || (v0_13.length() <= 0)) {
                            String v9_6 = new Object[1];
                            v9_6[0] = v7_1;
                            v0_13 = String.format("Log:\n%s", v9_6);
                        } else {
                            Object[] v10_2 = new Object[2];
                            v10_2[0] = v0_13;
                            v10_2[1] = v7_1;
                            v0_13 = String.format("%s\n\nLog:\n%s", v10_2);
                        }
                    }
                    String v7_3 = new java.util.HashMap();
                    v7_3.put("raw", v8_0);
                    v7_3.put("userID", v5_2);
                    v7_3.put("contact", v4_7);
                    v7_3.put("description", v0_13);
                    v7_3.put("sdk", "HockeySDK");
                    v7_3.put("sdk_version", "3.6.0");
                    int v0_21 = new net.hockeyapp.android.e.l(new StringBuilder().append(net.hockeyapp.android.b.b).append("api/2/apps/").append(net.hockeyapp.android.b.a).append("/crashes/").toString());
                    v0_21.b = "POST";
                    v3_3 = v0_21.a(v7_3).a();
                    int v0_23 = v3_3.getResponseCode();
                    if ((v0_23 != 202) && (v0_23 != 201)) {
                        v0_24 = 0;
                    } else {
                        v0_24 = 1;
                    }
                }
            }
        }
        return;
    }

    private static void a(ref.WeakReference p3, net.hockeyapp.android.i p4, boolean p5)
    {
        android.app.AlertDialog v0_1 = ((android.content.Context) p3.get());
        if (v0_1 != null) {
            android.app.AlertDialog$Builder v1_1 = new android.app.AlertDialog$Builder(v0_1);
            v1_1.setTitle(net.hockeyapp.android.aj.a(p4, 0));
            v1_1.setMessage(net.hockeyapp.android.aj.a(p4, 1));
            v1_1.setNegativeButton(net.hockeyapp.android.aj.a(p4, 2), new net.hockeyapp.android.c(p4, p3, p5));
            v1_1.setNeutralButton(net.hockeyapp.android.aj.a(p4, 3), new net.hockeyapp.android.d(p4, p3, p5));
            v1_1.setPositiveButton(net.hockeyapp.android.aj.a(p4, 4), new net.hockeyapp.android.e(p4, p3, p5));
            v1_1.create().show();
        }
        return;
    }

    private static void a(net.hockeyapp.android.i p4, boolean p5)
    {
        if ((net.hockeyapp.android.a.b == null) || (net.hockeyapp.android.a.d == null)) {
            android.util.Log.d("HockeyApp", "Exception handler not set because version or package is null.");
        } else {
            net.hockeyapp.android.j v0_3 = Thread.getDefaultUncaughtExceptionHandler();
            if (v0_3 != null) {
                android.util.Log.d("HockeyApp", new StringBuilder("Current handler class = ").append(v0_3.getClass().getName()).toString());
            }
            if (!(v0_3 instanceof net.hockeyapp.android.j)) {
                Thread.setDefaultUncaughtExceptionHandler(new net.hockeyapp.android.j(v0_3, p4, p5));
            } else {
                ((net.hockeyapp.android.j) v0_3).a = p4;
            }
        }
        return;
    }

    static synthetic boolean a()
    {
        net.hockeyapp.android.b.c = 0;
        return 0;
    }

    public static boolean a(net.hockeyapp.android.c.a p6, net.hockeyapp.android.i p7, ref.WeakReference p8, boolean p9)
    {
        int v1_0 = 0;
        switch (net.hockeyapp.android.h.a[p6.ordinal()]) {
            case 1:
                String[] v3_1 = net.hockeyapp.android.b.c();
                if ((v3_1 != null) && (v3_1.length > 0)) {
                    android.util.Log.d("HockeyApp", new StringBuilder("Found ").append(v3_1.length).append(" stacktrace(s).").toString());
                    while (v1_0 < v3_1.length) {
                        if (p8 != null) {
                            try {
                                android.util.Log.d("HockeyApp", new StringBuilder("Delete stacktrace ").append(v3_1[v1_0]).append(".").toString());
                                net.hockeyapp.android.b.b(p8, v3_1[v1_0]);
                                Exception v0_13 = ((android.content.Context) p8.get());
                            } catch (Exception v0_14) {
                                v0_14.printStackTrace();
                            }
                            if (v0_13 != null) {
                                v0_13.deleteFile(v3_1[v1_0]);
                            }
                        }
                        v1_0++;
                    }
                }
                net.hockeyapp.android.b.a(p7, p9);
                v1_0 = 1;
                break;
            case 2:
                Exception v0_2 = 0;
                if (p8 != null) {
                    v0_2 = ((android.content.Context) p8.get());
                }
                if (v0_2 == null) {
                } else {
                    android.preference.PreferenceManager.getDefaultSharedPreferences(v0_2).edit().putBoolean("always_send_crash_reports", 1).commit();
                    net.hockeyapp.android.b.c(p8, p7, p9);
                    v1_0 = 1;
                }
                break;
            case 3:
                net.hockeyapp.android.b.c(p8, p7, p9);
                v1_0 = 1;
                break;
        }
        return v1_0;
    }

    private static String b()
    {
        return new StringBuilder().append(net.hockeyapp.android.b.b).append("api/2/apps/").append(net.hockeyapp.android.b.a).append("/crashes/").toString();
    }

    private static void b(android.content.Context p1, String p2, String p3, net.hockeyapp.android.i p4)
    {
        net.hockeyapp.android.b.a(p1, p2, p3, p4, 1);
        return;
    }

    private static void b(android.content.Context p2, String p3, net.hockeyapp.android.i p4)
    {
        net.hockeyapp.android.b.a(p2, "https://sdk.hockeyapp.net/", p3, p4, 1);
        return;
    }

    private static void b(ref.WeakReference p5)
    {
        String[] v2 = net.hockeyapp.android.b.c();
        if ((v2 != null) && (v2.length > 0)) {
            android.util.Log.d("HockeyApp", new StringBuilder("Found ").append(v2.length).append(" stacktrace(s).").toString());
            int v1_5 = 0;
            while (v1_5 < v2.length) {
                if (p5 != null) {
                    try {
                        android.util.Log.d("HockeyApp", new StringBuilder("Delete stacktrace ").append(v2[v1_5]).append(".").toString());
                        net.hockeyapp.android.b.b(p5, v2[v1_5]);
                        Exception v0_7 = ((android.content.Context) p5.get());
                    } catch (Exception v0_8) {
                        v0_8.printStackTrace();
                    }
                    if (v0_7 != null) {
                        v0_7.deleteFile(v2[v1_5]);
                    }
                }
                v1_5++;
            }
        }
        return;
    }

    private static void b(ref.WeakReference p3, String p4)
    {
        if (p3 != null) {
            android.content.Context v0_1 = ((android.content.Context) p3.get());
            if (v0_1 != null) {
                v0_1.deleteFile(p4);
                v0_1.deleteFile(p4.replace(".stacktrace", ".user"));
                v0_1.deleteFile(p4.replace(".stacktrace", ".contact"));
                v0_1.deleteFile(p4.replace(".stacktrace", ".description"));
            }
        }
        return;
    }

    private static void b(ref.WeakReference p0, net.hockeyapp.android.i p1, boolean p2)
    {
        net.hockeyapp.android.b.c(p0, p1, p2);
        return;
    }

    private static String c(ref.WeakReference p5, String p6)
    {
        java.io.IOException v0_2;
        java.io.IOException v1 = 0;
        if (p5 == null) {
            v0_2 = 0;
        } else {
            java.io.IOException v0_1 = ((android.content.Context) p5.get());
            if (v0_1 == null) {
            } else {
                StringBuilder v3_1 = new StringBuilder();
                try {
                    int v2_1 = new java.io.BufferedReader(new java.io.InputStreamReader(v0_1.openFileInput(p6)));
                    try {
                        while(true) {
                            java.io.IOException v0_6 = v2_1.readLine();
                            v3_1.append(v0_6);
                            v3_1.append(System.getProperty("line.separator"));
                        }
                        if (v2_1 == 0) {
                            v0_2 = v3_1.toString();
                        } else {
                            v2_1.close();
                        }
                    } catch (java.io.IOException v0) {
                        v1 = v2_1;
                        if (v1 == null) {
                        } else {
                            v1.close();
                        }
                    } catch (java.io.IOException v0_4) {
                        v0_4.printStackTrace();
                        if (v2_1 == 0) {
                        } else {
                            v2_1.close();
                        }
                    }
                    if (v0_6 == null) {
                    }
                } catch (java.io.IOException v0) {
                } catch (java.io.IOException v0_4) {
                    v2_1 = 0;
                } catch (java.io.IOException v0_5) {
                    v2_1 = 0;
                    if (v2_1 != 0) {
                        try {
                            v2_1.close();
                        } catch (java.io.IOException v1) {
                        }
                    }
                    throw v0_5;
                } catch (java.io.IOException v0_5) {
                } catch (java.io.IOException v0) {
                }
            }
        }
        return v0_2;
    }

    private static void c(ref.WeakReference p2)
    {
        if (p2 != null) {
            android.content.SharedPreferences$Editor v0_1 = ((android.content.Context) p2.get());
            if (v0_1 != null) {
                android.preference.PreferenceManager.getDefaultSharedPreferences(v0_1).edit().remove("always_send_crash_reports").commit();
            }
        }
        return;
    }

    private static void c(ref.WeakReference p7, net.hockeyapp.android.i p8, boolean p9)
    {
        if (p7 != null) {
            int v0_1 = ((android.content.Context) p7.get());
            if (v0_1 != 0) {
                try {
                    String[] v2 = net.hockeyapp.android.b.c();
                    android.content.SharedPreferences$Editor v3_1 = v0_1.getSharedPreferences("HockeySDK", 0).edit();
                    StringBuffer v6_1 = new StringBuffer();
                    int v0_3 = 0;
                } catch (int v0) {
                }
                while (v0_3 < v2.length) {
                    v6_1.append(v2[v0_3]);
                    if (v0_3 < (v2.length - 1)) {
                        v6_1.append("|");
                    }
                    v0_3++;
                }
                v3_1.putString("ConfirmedFilenames", v6_1.toString());
                net.hockeyapp.android.e.n.a(v3_1);
            }
        }
        net.hockeyapp.android.b.a(p8, p9);
        if (!net.hockeyapp.android.b.c) {
            net.hockeyapp.android.b.c = 1;
            new net.hockeyapp.android.f(p7, p8).start();
        }
        return;
    }

    private static String[] c()
    {
        String[] v0_2;
        if (net.hockeyapp.android.a.a == null) {
            android.util.Log.d("HockeyApp", "Can\'t search for exception as file path is null.");
            v0_2 = 0;
        } else {
            android.util.Log.d("HockeyApp", new StringBuilder("Looking for exceptions in: ").append(net.hockeyapp.android.a.a).toString());
            String[] v0_5 = new java.io.File(new StringBuilder().append(net.hockeyapp.android.a.a).append("/").toString());
            if ((v0_5.mkdir()) || (v0_5.exists())) {
                v0_2 = v0_5.list(new net.hockeyapp.android.g());
            } else {
                v0_2 = new String[0];
            }
        }
        return v0_2;
    }

    private static void d(ref.WeakReference p7)
    {
        if (p7 != null) {
            int v0_1 = ((android.content.Context) p7.get());
            if (v0_1 != 0) {
                try {
                    String[] v2 = net.hockeyapp.android.b.c();
                    android.content.SharedPreferences$Editor v3_1 = v0_1.getSharedPreferences("HockeySDK", 0).edit();
                    StringBuffer v6_1 = new StringBuffer();
                    int v0_3 = 0;
                } catch (int v0) {
                }
                while (v0_3 < v2.length) {
                    v6_1.append(v2[v0_3]);
                    if (v0_3 < (v2.length - 1)) {
                        v6_1.append("|");
                    }
                    v0_3++;
                }
                v3_1.putString("ConfirmedFilenames", v6_1.toString());
                net.hockeyapp.android.e.n.a(v3_1);
            }
        }
        return;
    }
}
