package net.hockeyapp.android.d;
final class f extends android.os.AsyncTask {
    private final net.hockeyapp.android.d.e a;
    private final android.os.Handler b;
    private java.io.File c;
    private android.graphics.Bitmap d;
    private int e;

    public f(net.hockeyapp.android.d.e p2, android.os.Handler p3)
    {
        this.a = p2;
        this.b = p3;
        this.c = net.hockeyapp.android.a.a();
        this.d = 0;
        this.e = 0;
        return;
    }

    private varargs Boolean a()
    {
        String v0_0 = 0;
        String v2_1 = this.a.a;
        int v3_0 = net.hockeyapp.android.a.a();
        if ((v3_0.exists()) && (v3_0.isDirectory())) {
            int v3_1 = v3_0.listFiles(new net.hockeyapp.android.c.f(v2_1));
            if ((v3_1 != 0) && (v3_1.length == 1)) {
                v0_0 = 1;
            }
        }
        String v0_4;
        if (v0_0 == null) {
            android.util.Log.e("HockeyApp", "Downloading...");
            String v0_3 = this.a(v2_1.d, v2_1.a());
            if (v0_3 != null) {
                this.c();
            }
            v0_4 = Boolean.valueOf(v0_3);
        } else {
            android.util.Log.e("HockeyApp", "Cached...");
            this.c();
            v0_4 = Boolean.valueOf(1);
        }
        return v0_4;
    }

    private static java.net.URLConnection a(java.net.URL p3)
    {
        java.net.HttpURLConnection v0_1 = ((java.net.HttpURLConnection) p3.openConnection());
        v0_1.addRequestProperty("User-Agent", "HockeySDK/Android");
        v0_1.setInstanceFollowRedirects(1);
        if (android.os.Build$VERSION.SDK_INT <= 9) {
            v0_1.setRequestProperty("connection", "close");
        }
        return v0_1;
    }

    private void a(Boolean p8)
    {
        android.widget.TextView v0_0 = 1;
        String v2_1 = this.a.b;
        this.a.c = p8.booleanValue();
        if (!p8.booleanValue()) {
            if (this.a.d <= 0) {
                v0_0 = 0;
            }
            if (v0_0 == null) {
                v2_1.c.setText("Error");
            }
        } else {
            int v3_4 = this.d;
            int v4_1 = this.e;
            v2_1.c.setText(v2_1.b);
            v2_1.d = v4_1;
            if (v3_4 != 0) {
                v2_1.a(v3_4, 1);
            } else {
                v2_1.a(1);
            }
        }
        this.b.sendEmptyMessage(0);
        return;
    }

    private boolean a(String p17, String p18)
    {
        try {
            int v2_3 = ((java.net.HttpURLConnection) new java.net.URL(p17).openConnection());
            v2_3.addRequestProperty("User-Agent", "HockeySDK/Android");
            v2_3.setInstanceFollowRedirects(1);
        } catch (int v2_9) {
            v2_9.printStackTrace();
            int v2_8 = 0;
            return v2_8;
        }
        if (android.os.Build$VERSION.SDK_INT <= 9) {
            v2_3.setRequestProperty("connection", "close");
        }
        v2_3.connect();
        long v4_3 = v2_3.getContentLength();
        java.io.File v3_5 = v2_3.getHeaderField("Status");
        if ((v3_5 == null) || (v3_5.startsWith("200"))) {
            java.io.File v3_8 = new java.io.File(this.c, p18);
            java.io.BufferedInputStream v5_3 = new java.io.BufferedInputStream(v2_3.getInputStream());
            java.io.FileOutputStream v6_1 = new java.io.FileOutputStream(v3_8);
            byte[] v7 = new byte[1024];
            int v2_6 = 0;
            while(true) {
                int v8 = v5_3.read(v7);
                if (v8 == -1) {
                    break;
                }
                v2_6 += ((long) v8);
                int v9_2 = new Integer[1];
                v9_2[0] = Integer.valueOf(((int) ((100 * v2_6) / ((long) v4_3))));
                this.publishProgress(v9_2);
                v6_1.write(v7, 0, v8);
            }
            v6_1.flush();
            v6_1.close();
            v5_3.close();
            if (v2_6 <= 0) {
                v2_8 = 0;
                return v2_8;
            } else {
                v2_8 = 1;
                return v2_8;
            }
        } else {
            v2_8 = 0;
            return v2_8;
        }
    }

    private static varargs void b()
    {
        return;
    }

    private void c()
    {
        try {
            int v1_1;
            android.graphics.BitmapFactory$Options v2_0 = this.a.a.a();
            java.io.File v3_0 = this.a.b;
            this.e = net.hockeyapp.android.e.m.a(new java.io.File(this.c, v2_0));
        } catch (android.graphics.Bitmap v0_15) {
            v0_15.printStackTrace();
            this.d = 0;
            return;
        }
        if (this.e != 1) {
            v1_1 = v3_0.getWidthPortrait();
        } else {
            v1_1 = v3_0.getWidthLandscape();
        }
        android.graphics.Bitmap v0_10;
        if (this.e != 1) {
            v0_10 = v3_0.getMaxHeightPortrait();
        } else {
            v0_10 = v3_0.getMaxHeightLandscape();
        }
        java.io.File v3_2 = new java.io.File(this.c, v2_0);
        android.graphics.BitmapFactory$Options v2_2 = new android.graphics.BitmapFactory$Options();
        v2_2.inJustDecodeBounds = 1;
        android.graphics.BitmapFactory.decodeFile(v3_2.getAbsolutePath(), v2_2);
        v2_2.inSampleSize = net.hockeyapp.android.e.m.a(v2_2, v1_1, v0_10);
        v2_2.inJustDecodeBounds = 0;
        this.d = android.graphics.BitmapFactory.decodeFile(v3_2.getAbsolutePath(), v2_2);
        return;
    }

    protected final synthetic Object doInBackground(Object[] p2)
    {
        return this.a();
    }

    protected final synthetic void onPostExecute(Object p8)
    {
        android.widget.TextView v0_0 = 1;
        String v2_1 = this.a.b;
        this.a.c = ((Boolean) p8).booleanValue();
        if (!((Boolean) p8).booleanValue()) {
            if (this.a.d <= 0) {
                v0_0 = 0;
            }
            if (v0_0 == null) {
                v2_1.c.setText("Error");
            }
        } else {
            int v3_4 = this.d;
            int v4_1 = this.e;
            v2_1.c.setText(v2_1.b);
            v2_1.d = v4_1;
            if (v3_4 != 0) {
                v2_1.a(v3_4, 1);
            } else {
                v2_1.a(1);
            }
        }
        this.b.sendEmptyMessage(0);
        return;
    }

    protected final void onPreExecute()
    {
        return;
    }

    protected final bridge synthetic void onProgressUpdate(Object[] p1)
    {
        return;
    }
}
