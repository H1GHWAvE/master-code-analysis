package net.hockeyapp.android;
public final class t {
    private static final int a = 1;
    private static final int b = 1;
    private static final String c = "net.hockeyapp.android.SCREENSHOT";
    private static android.content.BroadcastReceiver d;
    private static android.app.Activity e;
    private static boolean f;
    private static String g;
    private static String h;
    private static net.hockeyapp.android.c.i i;
    private static net.hockeyapp.android.c.i j;
    private static net.hockeyapp.android.y k;

    static t()
    {
        net.hockeyapp.android.t.d = 0;
        net.hockeyapp.android.t.f = 0;
        net.hockeyapp.android.t.g = 0;
        net.hockeyapp.android.t.h = 0;
        net.hockeyapp.android.t.k = 0;
        return;
    }

    public t()
    {
        return;
    }

    public static net.hockeyapp.android.y a()
    {
        return net.hockeyapp.android.t.k;
    }

    private static void a(android.app.Activity p7)
    {
        net.hockeyapp.android.t.e = p7;
        if (!net.hockeyapp.android.t.f) {
            net.hockeyapp.android.t.f = 1;
            android.content.BroadcastReceiver v1_3 = net.hockeyapp.android.t.e.getResources().getIdentifier("ic_menu_camera", "drawable", "android");
            android.content.IntentFilter v2_2 = new android.content.Intent();
            v2_2.setAction("net.hockeyapp.android.SCREENSHOT");
            ((android.app.NotificationManager) net.hockeyapp.android.t.e.getSystemService("notification")).notify(1, net.hockeyapp.android.e.w.a(net.hockeyapp.android.t.e, android.app.PendingIntent.getBroadcast(net.hockeyapp.android.t.e, 1, v2_2, 1073741824), "HockeyApp Feedback", "Take a screenshot for your feedback.", v1_3));
            if (net.hockeyapp.android.t.d == null) {
                net.hockeyapp.android.t.d = new net.hockeyapp.android.w();
            }
            net.hockeyapp.android.t.e.registerReceiver(net.hockeyapp.android.t.d, new android.content.IntentFilter("net.hockeyapp.android.SCREENSHOT"));
        }
        return;
    }

    public static void a(android.content.Context p9)
    {
        android.widget.Toast v0_2 = net.hockeyapp.android.t.e.getWindow().getDecorView();
        v0_2.setDrawingCacheEnabled(1);
        android.app.Activity v3_0 = v0_2.getDrawingCache();
        String v4 = net.hockeyapp.android.t.e.getLocalClassName();
        java.io.File v5 = net.hockeyapp.android.a.a();
        java.io.File v2_5 = new java.io.File(v5, new StringBuilder().append(v4).append(".jpg").toString());
        android.widget.Toast v0_6 = 1;
        while (v2_5.exists()) {
            v2_5 = new java.io.File(v5, new StringBuilder().append(v4).append("_").append(v0_6).append(".jpg").toString());
            v0_6++;
        }
        android.widget.Toast v0_8 = new net.hockeyapp.android.v(v3_0, p9);
        int v1_1 = new java.io.File[1];
        v1_1[0] = v2_5;
        v0_8.execute(v1_1);
        android.widget.Toast v0_10 = new net.hockeyapp.android.x(v2_5.getAbsolutePath(), 0);
        int v1_4 = new android.media.MediaScannerConnection(net.hockeyapp.android.t.e, v0_10);
        v0_10.a = v1_4;
        v1_4.connect();
        android.widget.Toast.makeText(p9, new StringBuilder("Screenshot \'").append(v2_5.getName()).append("\' is available in gallery.").toString(), 2000).show();
        return;
    }

    private static void a(android.content.Context p2, String p3)
    {
        if (p2 != null) {
            net.hockeyapp.android.t.g = net.hockeyapp.android.e.w.c(p3);
            net.hockeyapp.android.t.h = "https://sdk.hockeyapp.net/";
            net.hockeyapp.android.t.k = 0;
            net.hockeyapp.android.a.a(p2);
        }
        return;
    }

    private static void a(android.content.Context p1, String p2, String p3, net.hockeyapp.android.y p4)
    {
        if (p1 != null) {
            net.hockeyapp.android.t.g = net.hockeyapp.android.e.w.c(p3);
            net.hockeyapp.android.t.h = p2;
            net.hockeyapp.android.t.k = p4;
            net.hockeyapp.android.a.a(p1);
        }
        return;
    }

    private static varargs void a(android.content.Context p3, android.net.Uri[] p4)
    {
        if (p3 != null) {
            String v0_0 = 0;
            if (net.hockeyapp.android.t.k != null) {
                v0_0 = net.hockeyapp.android.FeedbackActivity;
            }
            if (v0_0 == null) {
                v0_0 = net.hockeyapp.android.FeedbackActivity;
            }
            android.content.Intent v1_2 = new android.content.Intent();
            v1_2.setFlags(268435456);
            v1_2.setClass(p3, v0_0);
            v1_2.putExtra("url", net.hockeyapp.android.t.f());
            v1_2.putExtra("initialAttachments", p4);
            p3.startActivity(v1_2);
        }
        return;
    }

    private static void a(net.hockeyapp.android.c.i p0)
    {
        net.hockeyapp.android.t.i = p0;
        return;
    }

    public static net.hockeyapp.android.c.i b()
    {
        return net.hockeyapp.android.t.i;
    }

    private static void b(android.app.Activity p2)
    {
        if ((net.hockeyapp.android.t.e != null) && (net.hockeyapp.android.t.e == p2)) {
            net.hockeyapp.android.t.f = 0;
            net.hockeyapp.android.t.e.unregisterReceiver(net.hockeyapp.android.t.d);
            ((android.app.NotificationManager) net.hockeyapp.android.t.e.getSystemService("notification")).cancel(1);
            net.hockeyapp.android.t.e = 0;
        }
        return;
    }

    private static void b(android.content.Context p13)
    {
        String v8 = net.hockeyapp.android.e.p.a.a(p13);
        if (v8 != null) {
            int v11 = p13.getSharedPreferences("net.hockeyapp.android.feedback", 0).getInt("idLastMessageSend", -1);
            net.hockeyapp.android.d.r v0_4 = new net.hockeyapp.android.d.r(p13, net.hockeyapp.android.t.f(), 0, 0, 0, 0, 0, v8, new net.hockeyapp.android.u(p13), 1);
            v0_4.a = 0;
            v0_4.b = v11;
            net.hockeyapp.android.e.a.a(v0_4);
        }
        return;
    }

    private static void b(android.content.Context p2, String p3)
    {
        if (p2 != null) {
            net.hockeyapp.android.t.g = net.hockeyapp.android.e.w.c(p3);
            net.hockeyapp.android.t.h = "https://sdk.hockeyapp.net/";
            net.hockeyapp.android.t.k = 0;
            net.hockeyapp.android.a.a(p2);
        }
        return;
    }

    private static varargs void b(android.content.Context p3, android.net.Uri[] p4)
    {
        if (p3 != null) {
            String v0_0 = 0;
            if (net.hockeyapp.android.t.k != null) {
                v0_0 = net.hockeyapp.android.FeedbackActivity;
            }
            if (v0_0 == null) {
                v0_0 = net.hockeyapp.android.FeedbackActivity;
            }
            android.content.Intent v1_2 = new android.content.Intent();
            v1_2.setFlags(268435456);
            v1_2.setClass(p3, v0_0);
            v1_2.putExtra("url", net.hockeyapp.android.t.f());
            v1_2.putExtra("initialAttachments", p4);
            p3.startActivity(v1_2);
        }
        return;
    }

    private static void b(net.hockeyapp.android.c.i p0)
    {
        net.hockeyapp.android.t.j = p0;
        return;
    }

    public static net.hockeyapp.android.c.i c()
    {
        return net.hockeyapp.android.t.j;
    }

    static synthetic String d()
    {
        return net.hockeyapp.android.t.f();
    }

    private static void e()
    {
        net.hockeyapp.android.t.k = 0;
        return;
    }

    private static String f()
    {
        return new StringBuilder().append(net.hockeyapp.android.t.h).append("api/2/apps/").append(net.hockeyapp.android.t.g).append("/feedback/").toString();
    }

    private static void g()
    {
        net.hockeyapp.android.t.f = 1;
        android.content.BroadcastReceiver v1_3 = net.hockeyapp.android.t.e.getResources().getIdentifier("ic_menu_camera", "drawable", "android");
        android.content.IntentFilter v2_2 = new android.content.Intent();
        v2_2.setAction("net.hockeyapp.android.SCREENSHOT");
        ((android.app.NotificationManager) net.hockeyapp.android.t.e.getSystemService("notification")).notify(1, net.hockeyapp.android.e.w.a(net.hockeyapp.android.t.e, android.app.PendingIntent.getBroadcast(net.hockeyapp.android.t.e, 1, v2_2, 1073741824), "HockeyApp Feedback", "Take a screenshot for your feedback.", v1_3));
        if (net.hockeyapp.android.t.d == null) {
            net.hockeyapp.android.t.d = new net.hockeyapp.android.w();
        }
        net.hockeyapp.android.t.e.registerReceiver(net.hockeyapp.android.t.d, new android.content.IntentFilter("net.hockeyapp.android.SCREENSHOT"));
        return;
    }

    private static void h()
    {
        net.hockeyapp.android.t.f = 0;
        net.hockeyapp.android.t.e.unregisterReceiver(net.hockeyapp.android.t.d);
        ((android.app.NotificationManager) net.hockeyapp.android.t.e.getSystemService("notification")).cancel(1);
        return;
    }
}
