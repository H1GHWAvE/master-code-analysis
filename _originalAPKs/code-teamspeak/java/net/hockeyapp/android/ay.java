package net.hockeyapp.android;
public final class ay {
    private static net.hockeyapp.android.d.g a;
    private static net.hockeyapp.android.az b;

    static ay()
    {
        net.hockeyapp.android.ay.a = 0;
        net.hockeyapp.android.ay.b = 0;
        return;
    }

    public ay()
    {
        return;
    }

    public static net.hockeyapp.android.az a()
    {
        return net.hockeyapp.android.ay.b;
    }

    private static void a(android.app.Activity p3, String p4)
    {
        net.hockeyapp.android.ay.a(p3, "https://sdk.hockeyapp.net/", p4, 0, 1);
        return;
    }

    private static void a(android.app.Activity p1, String p2, String p3, net.hockeyapp.android.az p4)
    {
        net.hockeyapp.android.ay.a(p1, p2, p3, p4, 1);
        return;
    }

    private static void a(android.app.Activity p6, String p7, String p8, net.hockeyapp.android.az p9, boolean p10)
    {
        String v3 = net.hockeyapp.android.e.w.c(p8);
        net.hockeyapp.android.ay.b = p9;
        ref.WeakReference v1_1 = new ref.WeakReference(p6);
        if (!net.hockeyapp.android.e.w.a().booleanValue()) {
            if (!net.hockeyapp.android.ay.a(v1_1)) {
                if ((net.hockeyapp.android.ay.a != null) && (net.hockeyapp.android.ay.a.getStatus() != android.os.AsyncTask$Status.FINISHED)) {
                    net.hockeyapp.android.ay.a.a(v1_1);
                } else {
                    net.hockeyapp.android.d.g v0_13 = new net.hockeyapp.android.d.h(v1_1, p7, v3, p9, p10);
                    net.hockeyapp.android.ay.a = v0_13;
                    net.hockeyapp.android.e.a.a(v0_13);
                }
            }
        } else {
            net.hockeyapp.android.d.g v0_4;
            net.hockeyapp.android.d.g v0_3 = ((android.app.Activity) v1_1.get());
            if (v0_3 == null) {
                v0_4 = 0;
            } else {
                if (v0_3.getFragmentManager().findFragmentByTag("hockey_update_dialog") == null) {
                    v0_4 = 0;
                } else {
                    v0_4 = 1;
                }
            }
            if (v0_4 == null) {
            }
        }
        return;
    }

    private static void a(android.app.Activity p2, String p3, net.hockeyapp.android.az p4)
    {
        net.hockeyapp.android.ay.a(p2, "https://sdk.hockeyapp.net/", p3, p4, 1);
        return;
    }

    private static void a(android.app.Activity p2, String p3, boolean p4)
    {
        net.hockeyapp.android.ay.a(p2, "https://sdk.hockeyapp.net/", p3, 0, p4);
        return;
    }

    private static void a(android.content.Context p4, String p5, String p6, net.hockeyapp.android.az p7)
    {
        net.hockeyapp.android.d.g v0_0 = net.hockeyapp.android.e.w.c(p6);
        net.hockeyapp.android.ay.b = p7;
        ref.WeakReference v1_1 = new ref.WeakReference(p4);
        if (!net.hockeyapp.android.ay.a(v1_1)) {
            if ((net.hockeyapp.android.ay.a != null) && (net.hockeyapp.android.ay.a.getStatus() != android.os.AsyncTask$Status.FINISHED)) {
                net.hockeyapp.android.ay.a.a(v1_1);
            } else {
                net.hockeyapp.android.d.g v2_5 = new net.hockeyapp.android.d.g(v1_1, p5, v0_0, p7);
                net.hockeyapp.android.ay.a = v2_5;
                net.hockeyapp.android.e.a.a(v2_5);
            }
        }
        return;
    }

    private static void a(android.content.Context p5, String p6, net.hockeyapp.android.az p7)
    {
        String v1 = net.hockeyapp.android.e.w.c(p6);
        net.hockeyapp.android.ay.b = p7;
        ref.WeakReference v2_1 = new ref.WeakReference(p5);
        if (!net.hockeyapp.android.ay.a(v2_1)) {
            if ((net.hockeyapp.android.ay.a != null) && (net.hockeyapp.android.ay.a.getStatus() != android.os.AsyncTask$Status.FINISHED)) {
                net.hockeyapp.android.ay.a.a(v2_1);
            } else {
                net.hockeyapp.android.d.g v3_5 = new net.hockeyapp.android.d.g(v2_1, "https://sdk.hockeyapp.net/", v1, p7);
                net.hockeyapp.android.ay.a = v3_5;
                net.hockeyapp.android.e.a.a(v3_5);
            }
        }
        return;
    }

    private static void a(ref.WeakReference p2, String p3, String p4, net.hockeyapp.android.az p5)
    {
        if ((net.hockeyapp.android.ay.a != null) && (net.hockeyapp.android.ay.a.getStatus() != android.os.AsyncTask$Status.FINISHED)) {
            net.hockeyapp.android.ay.a.a(p2);
        } else {
            net.hockeyapp.android.d.g v0_5 = new net.hockeyapp.android.d.g(p2, p3, p4, p5);
            net.hockeyapp.android.ay.a = v0_5;
            net.hockeyapp.android.e.a.a(v0_5);
        }
        return;
    }

    private static void a(ref.WeakReference p6, String p7, String p8, net.hockeyapp.android.az p9, boolean p10)
    {
        if ((net.hockeyapp.android.ay.a != null) && (net.hockeyapp.android.ay.a.getStatus() != android.os.AsyncTask$Status.FINISHED)) {
            net.hockeyapp.android.ay.a.a(p6);
        } else {
            net.hockeyapp.android.d.g v0_5 = new net.hockeyapp.android.d.h(p6, p7, p8, p9, p10);
            net.hockeyapp.android.ay.a = v0_5;
            net.hockeyapp.android.e.a.a(v0_5);
        }
        return;
    }

    private static boolean a(ref.WeakReference p3)
    {
        int v1 = 0;
        int v0_1 = ((android.content.Context) p3.get());
        if (v0_1 != 0) {
            try {
                int v0_5;
                if (android.text.TextUtils.isEmpty(v0_1.getPackageManager().getInstallerPackageName(v0_1.getPackageName()))) {
                    v0_5 = 0;
                } else {
                    v0_5 = 1;
                }
            } catch (int v0) {
            }
            v1 = v0_5;
        }
        return v1;
    }

    private static void b()
    {
        if (net.hockeyapp.android.ay.a != null) {
            net.hockeyapp.android.ay.a.cancel(1);
            net.hockeyapp.android.ay.a.a();
            net.hockeyapp.android.ay.a = 0;
        }
        net.hockeyapp.android.ay.b = 0;
        return;
    }

    private static void b(android.app.Activity p3, String p4)
    {
        net.hockeyapp.android.ay.a(p3, "https://sdk.hockeyapp.net/", p4, 0, 1);
        return;
    }

    private static void b(ref.WeakReference p3)
    {
        if (p3 != null) {
            android.app.Activity v0_1 = ((android.app.Activity) p3.get());
            if (v0_1 != null) {
                v0_1.finish();
                android.content.Intent v1_1 = new android.content.Intent(v0_1, net.hockeyapp.android.k);
                v1_1.addFlags(335544320);
                v0_1.startActivity(v1_1);
            }
        }
        return;
    }

    private static boolean c()
    {
        return 0;
    }

    private static boolean c(ref.WeakReference p3)
    {
        int v0_2;
        int v0_1 = ((android.app.Activity) p3.get());
        if (v0_1 == 0) {
            v0_2 = 0;
        } else {
            if (v0_1.getFragmentManager().findFragmentByTag("hockey_update_dialog") == null) {
                v0_2 = 0;
            } else {
                v0_2 = 1;
            }
        }
        return v0_2;
    }

    private static boolean d()
    {
        return 0;
    }
}
