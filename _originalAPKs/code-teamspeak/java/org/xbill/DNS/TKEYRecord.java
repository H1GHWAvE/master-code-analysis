package org.xbill.DNS;
public class TKEYRecord extends org.xbill.DNS.Record {
    public static final int DELETE = 5;
    public static final int DIFFIEHELLMAN = 2;
    public static final int GSSAPI = 3;
    public static final int RESOLVERASSIGNED = 4;
    public static final int SERVERASSIGNED = 1;
    private static final long serialVersionUID = 8828458121926391756;
    private org.xbill.DNS.Name alg;
    private int error;
    private byte[] key;
    private int mode;
    private byte[] other;
    private java.util.Date timeExpire;
    private java.util.Date timeInception;

    TKEYRecord()
    {
        return;
    }

    public TKEYRecord(org.xbill.DNS.Name p10, int p11, long p12, org.xbill.DNS.Name p14, java.util.Date p15, java.util.Date p16, int p17, int p18, byte[] p19, byte[] p20)
    {
        this(p10, 249, p11, p12);
        this.alg = org.xbill.DNS.TKEYRecord.checkName("alg", p14);
        this.timeInception = p15;
        this.timeExpire = p16;
        this.mode = org.xbill.DNS.TKEYRecord.checkU16("mode", p17);
        this.error = org.xbill.DNS.TKEYRecord.checkU16("error", p18);
        this.key = p19;
        this.other = p20;
        return;
    }

    public org.xbill.DNS.Name getAlgorithm()
    {
        return this.alg;
    }

    public int getError()
    {
        return this.error;
    }

    public byte[] getKey()
    {
        return this.key;
    }

    public int getMode()
    {
        return this.mode;
    }

    org.xbill.DNS.Record getObject()
    {
        return new org.xbill.DNS.TKEYRecord();
    }

    public byte[] getOther()
    {
        return this.other;
    }

    public java.util.Date getTimeExpire()
    {
        return this.timeExpire;
    }

    public java.util.Date getTimeInception()
    {
        return this.timeInception;
    }

    protected String modeString()
    {
        String v0_1;
        switch (this.mode) {
            case 1:
                v0_1 = "SERVERASSIGNED";
                break;
            case 2:
                v0_1 = "DIFFIEHELLMAN";
                break;
            case 3:
                v0_1 = "GSSAPI";
                break;
            case 4:
                v0_1 = "RESOLVERASSIGNED";
                break;
            case 5:
                v0_1 = "DELETE";
                break;
            default:
                v0_1 = Integer.toString(this.mode);
        }
        return v0_1;
    }

    void rdataFromString(org.xbill.DNS.Tokenizer p2, org.xbill.DNS.Name p3)
    {
        throw p2.exception("no text format defined for TKEY");
    }

    void rrFromWire(org.xbill.DNS.DNSInput p7)
    {
        this.alg = new org.xbill.DNS.Name(p7);
        this.timeInception = new java.util.Date((p7.readU32() * 1000));
        this.timeExpire = new java.util.Date((p7.readU32() * 1000));
        this.mode = p7.readU16();
        this.error = p7.readU16();
        byte[] v0_8 = p7.readU16();
        if (v0_8 <= null) {
            this.key = 0;
        } else {
            this.key = p7.readByteArray(v0_8);
        }
        byte[] v0_10 = p7.readU16();
        if (v0_10 <= null) {
            this.other = 0;
        } else {
            this.other = p7.readByteArray(v0_10);
        }
        return;
    }

    String rrToString()
    {
        String v0_1 = new StringBuffer();
        v0_1.append(this.alg);
        v0_1.append(" ");
        if (org.xbill.DNS.Options.check("multiline")) {
            v0_1.append("(\n\t");
        }
        v0_1.append(org.xbill.DNS.FormattedTime.format(this.timeInception));
        v0_1.append(" ");
        v0_1.append(org.xbill.DNS.FormattedTime.format(this.timeExpire));
        v0_1.append(" ");
        v0_1.append(this.modeString());
        v0_1.append(" ");
        v0_1.append(org.xbill.DNS.Rcode.TSIGstring(this.error));
        if (!org.xbill.DNS.Options.check("multiline")) {
            v0_1.append(" ");
            if (this.key != null) {
                v0_1.append(org.xbill.DNS.utils.base64.toString(this.key));
                v0_1.append(" ");
            }
            if (this.other != null) {
                v0_1.append(org.xbill.DNS.utils.base64.toString(this.other));
            }
        } else {
            v0_1.append("\n");
            if (this.key != null) {
                v0_1.append(org.xbill.DNS.utils.base64.formatString(this.key, 64, "\t", 0));
                v0_1.append("\n");
            }
            if (this.other != null) {
                v0_1.append(org.xbill.DNS.utils.base64.formatString(this.other, 64, "\t", 0));
            }
            v0_1.append(" )");
        }
        return v0_1.toString();
    }

    void rrToWire(org.xbill.DNS.DNSOutput p7, org.xbill.DNS.Compression p8, boolean p9)
    {
        this.alg.toWire(p7, 0, p9);
        p7.writeU32((this.timeInception.getTime() / 1000));
        p7.writeU32((this.timeExpire.getTime() / 1000));
        p7.writeU16(this.mode);
        p7.writeU16(this.error);
        if (this.key == null) {
            p7.writeU16(0);
        } else {
            p7.writeU16(this.key.length);
            p7.writeByteArray(this.key);
        }
        if (this.other == null) {
            p7.writeU16(0);
        } else {
            p7.writeU16(this.other.length);
            p7.writeByteArray(this.other);
        }
        return;
    }
}
