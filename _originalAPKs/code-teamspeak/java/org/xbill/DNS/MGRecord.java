package org.xbill.DNS;
public class MGRecord extends org.xbill.DNS.SingleNameBase {
    private static final long serialVersionUID = 14466688522845907034;

    MGRecord()
    {
        return;
    }

    public MGRecord(org.xbill.DNS.Name p10, int p11, long p12, org.xbill.DNS.Name p14)
    {
        this(p10, 8, p11, p12, p14, "mailbox");
        return;
    }

    public org.xbill.DNS.Name getMailbox()
    {
        return this.getSingleName();
    }

    org.xbill.DNS.Record getObject()
    {
        return new org.xbill.DNS.MGRecord();
    }
}
