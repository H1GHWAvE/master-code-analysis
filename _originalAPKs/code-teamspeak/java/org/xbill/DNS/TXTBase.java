package org.xbill.DNS;
abstract class TXTBase extends org.xbill.DNS.Record {
    private static final long serialVersionUID = 14127233566463245685;
    protected java.util.List strings;

    protected TXTBase()
    {
        return;
    }

    protected TXTBase(org.xbill.DNS.Name p1, int p2, int p3, long p4)
    {
        this(p1, p2, p3, p4);
        return;
    }

    protected TXTBase(org.xbill.DNS.Name p9, int p10, int p11, long p12, String p14)
    {
        this(p9, p10, p11, p12, java.util.Collections.singletonList(p14));
        return;
    }

    protected TXTBase(org.xbill.DNS.Name p5, int p6, int p7, long p8, java.util.List p10)
    {
        org.xbill.DNS.TXTBase v4_1 = this(p5, p6, p7, p8);
        if (p10 != null) {
            v4_1.strings = new java.util.ArrayList(p10.size());
            IllegalArgumentException v1_1 = p10.iterator();
            try {
                while (v1_1.hasNext()) {
                    v4_1.strings.add(org.xbill.DNS.TXTBase.byteArrayFromString(((String) v1_1.next())));
                }
            } catch (byte[] v0_3) {
                throw new IllegalArgumentException(v0_3.getMessage());
            }
            return;
        } else {
            throw new IllegalArgumentException("strings must not be null");
        }
    }

    public java.util.List getStrings()
    {
        java.util.ArrayList v3_1 = new java.util.ArrayList(this.strings.size());
        int v1 = 0;
        while (v1 < this.strings.size()) {
            v3_1.add(org.xbill.DNS.TXTBase.byteArrayToString(((byte[]) ((byte[]) this.strings.get(v1))), 0));
            v1++;
        }
        return v3_1;
    }

    public java.util.List getStringsAsByteArrays()
    {
        return this.strings;
    }

    void rdataFromString(org.xbill.DNS.Tokenizer p3, org.xbill.DNS.Name p4)
    {
        this.strings = new java.util.ArrayList(2);
        while(true) {
            org.xbill.DNS.TextParseException v0_2 = p3.get();
            if (!v0_2.isString()) {
                break;
            }
            try {
                this.strings.add(org.xbill.DNS.TXTBase.byteArrayFromString(v0_2.value));
            } catch (org.xbill.DNS.TextParseException v0_5) {
                throw p3.exception(v0_5.getMessage());
            }
        }
        p3.unget();
        return;
    }

    void rrFromWire(org.xbill.DNS.DNSInput p3)
    {
        this.strings = new java.util.ArrayList(2);
        while (p3.remaining() > 0) {
            this.strings.add(p3.readCountedString());
        }
        return;
    }

    String rrToString()
    {
        StringBuffer v1_1 = new StringBuffer();
        java.util.Iterator v2 = this.strings.iterator();
        while (v2.hasNext()) {
            v1_1.append(org.xbill.DNS.TXTBase.byteArrayToString(((byte[]) ((byte[]) v2.next())), 1));
            if (v2.hasNext()) {
                v1_1.append(" ");
            }
        }
        return v1_1.toString();
    }

    void rrToWire(org.xbill.DNS.DNSOutput p3, org.xbill.DNS.Compression p4, boolean p5)
    {
        java.util.Iterator v1 = this.strings.iterator();
        while (v1.hasNext()) {
            p3.writeCountedString(((byte[]) ((byte[]) v1.next())));
        }
        return;
    }
}
