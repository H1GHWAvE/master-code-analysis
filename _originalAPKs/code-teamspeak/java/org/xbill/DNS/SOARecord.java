package org.xbill.DNS;
public class SOARecord extends org.xbill.DNS.Record {
    private static final long serialVersionUID = 1049740098229303931;
    private org.xbill.DNS.Name admin;
    private long expire;
    private org.xbill.DNS.Name host;
    private long minimum;
    private long refresh;
    private long retry;
    private long serial;

    SOARecord()
    {
        return;
    }

    public SOARecord(org.xbill.DNS.Name p10, int p11, long p12, org.xbill.DNS.Name p14, org.xbill.DNS.Name p15, long p16, long p18, long p20, long p22, long p24)
    {
        this(p10, 6, p11, p12);
        this.host = org.xbill.DNS.SOARecord.checkName("host", p14);
        this.admin = org.xbill.DNS.SOARecord.checkName("admin", p15);
        this.serial = org.xbill.DNS.SOARecord.checkU32("serial", p16);
        this.refresh = org.xbill.DNS.SOARecord.checkU32("refresh", p18);
        this.retry = org.xbill.DNS.SOARecord.checkU32("retry", p20);
        this.expire = org.xbill.DNS.SOARecord.checkU32("expire", p22);
        this.minimum = org.xbill.DNS.SOARecord.checkU32("minimum", p24);
        return;
    }

    public org.xbill.DNS.Name getAdmin()
    {
        return this.admin;
    }

    public long getExpire()
    {
        return this.expire;
    }

    public org.xbill.DNS.Name getHost()
    {
        return this.host;
    }

    public long getMinimum()
    {
        return this.minimum;
    }

    org.xbill.DNS.Record getObject()
    {
        return new org.xbill.DNS.SOARecord();
    }

    public long getRefresh()
    {
        return this.refresh;
    }

    public long getRetry()
    {
        return this.retry;
    }

    public long getSerial()
    {
        return this.serial;
    }

    void rdataFromString(org.xbill.DNS.Tokenizer p3, org.xbill.DNS.Name p4)
    {
        this.host = p3.getName(p4);
        this.admin = p3.getName(p4);
        this.serial = p3.getUInt32();
        this.refresh = p3.getTTLLike();
        this.retry = p3.getTTLLike();
        this.expire = p3.getTTLLike();
        this.minimum = p3.getTTLLike();
        return;
    }

    void rrFromWire(org.xbill.DNS.DNSInput p3)
    {
        this.host = new org.xbill.DNS.Name(p3);
        this.admin = new org.xbill.DNS.Name(p3);
        this.serial = p3.readU32();
        this.refresh = p3.readU32();
        this.retry = p3.readU32();
        this.expire = p3.readU32();
        this.minimum = p3.readU32();
        return;
    }

    String rrToString()
    {
        String v0_1 = new StringBuffer();
        v0_1.append(this.host);
        v0_1.append(" ");
        v0_1.append(this.admin);
        if (!org.xbill.DNS.Options.check("multiline")) {
            v0_1.append(" ");
            v0_1.append(this.serial);
            v0_1.append(" ");
            v0_1.append(this.refresh);
            v0_1.append(" ");
            v0_1.append(this.retry);
            v0_1.append(" ");
            v0_1.append(this.expire);
            v0_1.append(" ");
            v0_1.append(this.minimum);
        } else {
            v0_1.append(" (\n\t\t\t\t\t");
            v0_1.append(this.serial);
            v0_1.append("\t; serial\n\t\t\t\t\t");
            v0_1.append(this.refresh);
            v0_1.append("\t; refresh\n\t\t\t\t\t");
            v0_1.append(this.retry);
            v0_1.append("\t; retry\n\t\t\t\t\t");
            v0_1.append(this.expire);
            v0_1.append("\t; expire\n\t\t\t\t\t");
            v0_1.append(this.minimum);
            v0_1.append(" )\t; minimum");
        }
        return v0_1.toString();
    }

    void rrToWire(org.xbill.DNS.DNSOutput p3, org.xbill.DNS.Compression p4, boolean p5)
    {
        this.host.toWire(p3, p4, p5);
        this.admin.toWire(p3, p4, p5);
        p3.writeU32(this.serial);
        p3.writeU32(this.refresh);
        p3.writeU32(this.retry);
        p3.writeU32(this.expire);
        p3.writeU32(this.minimum);
        return;
    }
}
