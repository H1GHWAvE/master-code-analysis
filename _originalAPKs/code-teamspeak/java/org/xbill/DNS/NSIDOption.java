package org.xbill.DNS;
public class NSIDOption extends org.xbill.DNS.GenericEDNSOption {
    private static final long serialVersionUID = 74739759292589056;

    NSIDOption()
    {
        this(3);
        return;
    }

    public NSIDOption(byte[] p2)
    {
        this(3, p2);
        return;
    }
}
