package org.xbill.DNS;
public class UNKRecord extends org.xbill.DNS.Record {
    private static final long serialVersionUID = 14253160762114924701;
    private byte[] data;

    UNKRecord()
    {
        return;
    }

    public byte[] getData()
    {
        return this.data;
    }

    org.xbill.DNS.Record getObject()
    {
        return new org.xbill.DNS.UNKRecord();
    }

    void rdataFromString(org.xbill.DNS.Tokenizer p2, org.xbill.DNS.Name p3)
    {
        throw p2.exception("invalid unknown RR encoding");
    }

    void rrFromWire(org.xbill.DNS.DNSInput p2)
    {
        this.data = p2.readByteArray();
        return;
    }

    String rrToString()
    {
        return org.xbill.DNS.UNKRecord.unknownToString(this.data);
    }

    void rrToWire(org.xbill.DNS.DNSOutput p2, org.xbill.DNS.Compression p3, boolean p4)
    {
        p2.writeByteArray(this.data);
        return;
    }
}
