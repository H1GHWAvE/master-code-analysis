package org.xbill.DNS.utils;
public class base32 {
    private String alphabet;
    private boolean lowercase;
    private boolean padding;

    public base32(String p1, boolean p2, boolean p3)
    {
        this.alphabet = p1;
        this.padding = p2;
        this.lowercase = p3;
        return;
    }

    private static int blockLenToPadding(int p1)
    {
        int v0;
        switch (p1) {
            case 1:
                v0 = 6;
                break;
            case 2:
                v0 = 4;
                break;
            case 3:
                v0 = 3;
                break;
            case 4:
                v0 = 1;
                break;
            case 5:
                v0 = 0;
                break;
            default:
                v0 = -1;
        }
        return v0;
    }

    private static int paddingToBlockLen(int p1)
    {
        int v0;
        switch (p1) {
            case 0:
                v0 = 5;
                break;
            case 1:
                v0 = 4;
                break;
            case 2:
            case 5:
            default:
                v0 = -1;
                break;
            case 3:
                v0 = 3;
                break;
            case 4:
                v0 = 2;
                break;
            case 6:
                v0 = 1;
                break;
        }
        return v0;
    }

    public byte[] fromString(String p11)
    {
        java.io.ByteArrayOutputStream v3_1 = new java.io.ByteArrayOutputStream();
        java.io.IOException v1_0 = p11.getBytes();
        int v0_0 = 0;
        while (v0_0 < v1_0.length) {
            int v2_4 = ((char) v1_0[v0_0]);
            if (!Character.isWhitespace(v2_4)) {
                v3_1.write(((byte) Character.toUpperCase(v2_4)));
            }
            v0_0++;
        }
        int v0_8;
        if (!this.padding) {
            while ((v3_1.size() % 8) != 0) {
                v3_1.write(61);
            }
            byte[] v4_0 = v3_1.toByteArray();
            v3_1.reset();
            java.io.DataOutputStream v5_1 = new java.io.DataOutputStream(v3_1);
            int v0_7 = 0;
            while (v0_7 < (v4_0.length / 8)) {
                byte v6_0 = new short[8];
                int[] v7 = new int[5];
                int v2_1 = 8;
                java.io.IOException v1_5 = 0;
                while ((v1_5 < 8) && (((char) v4_0[((v0_7 * 8) + v1_5)]) != 61)) {
                    v6_0[v1_5] = ((short) this.alphabet.indexOf(v4_0[((v0_7 * 8) + v1_5)]));
                    if (v6_0[v1_5] >= 0) {
                        v2_1--;
                        v1_5++;
                    } else {
                        v0_8 = 0;
                    }
                }
                int v2_2 = org.xbill.DNS.utils.base32.paddingToBlockLen(v2_1);
                if (v2_2 >= 0) {
                    v7[0] = ((v6_0[0] << 3) | (v6_0[1] >> 2));
                    v7[1] = ((((v6_0[1] & 3) << 6) | (v6_0[2] << 1)) | (v6_0[3] >> 4));
                    v7[2] = (((v6_0[3] & 15) << 4) | ((v6_0[4] >> 1) & 15));
                    v7[3] = (((v6_0[4] << 7) | (v6_0[5] << 2)) | (v6_0[6] >> 3));
                    v7[4] = (v6_0[7] | ((v6_0[6] & 7) << 5));
                    java.io.IOException v1_11 = 0;
                    while (v1_11 < v2_2) {
                        try {
                            v5_1.writeByte(((byte) (v7[v1_11] & 255)));
                            v1_11++;
                        } catch (java.io.IOException v1) {
                            break;
                        }
                    }
                    v0_7++;
                } else {
                    v0_8 = 0;
                }
            }
            v0_8 = v3_1.toByteArray();
        } else {
            if ((v3_1.size() % 8) == 0) {
            } else {
                v0_8 = 0;
            }
        }
        return v0_8;
    }

    public String toString(byte[] p14)
    {
        java.io.ByteArrayOutputStream v5_1 = new java.io.ByteArrayOutputStream();
        int v0_0 = 0;
        while (v0_0 < ((p14.length + 4) / 5)) {
            boolean v6_0 = new short[5];
            int[] v7 = new int[8];
            int v3_0 = 0;
            int v2_4 = 5;
            while (v3_0 < 5) {
                if (((v0_0 * 5) + v3_0) >= p14.length) {
                    v6_0[v3_0] = 0;
                    v2_4--;
                } else {
                    v6_0[v3_0] = ((short) (p14[((v0_0 * 5) + v3_0)] & 255));
                }
                v3_0++;
            }
            short v8_0 = org.xbill.DNS.utils.base32.blockLenToPadding(v2_4);
            v7[0] = ((byte) ((v6_0[0] >> 3) & 31));
            v7[1] = ((byte) (((v6_0[0] & 7) << 2) | ((v6_0[1] >> 6) & 3)));
            v7[2] = ((byte) ((v6_0[1] >> 1) & 31));
            v7[3] = ((byte) (((v6_0[1] & 1) << 4) | ((v6_0[2] >> 4) & 15)));
            v7[4] = ((byte) (((v6_0[2] & 15) << 1) | ((v6_0[3] >> 7) & 1)));
            v7[5] = ((byte) ((v6_0[3] >> 2) & 31));
            v7[6] = ((byte) (((v6_0[3] & 3) << 3) | ((v6_0[4] >> 5) & 7)));
            v7[7] = ((byte) (v6_0[4] & 31));
            int v3_21 = 0;
            while (v3_21 < (8 - v8_0)) {
                int v2_34 = this.alphabet.charAt(v7[v3_21]);
                if (this.lowercase) {
                    v2_34 = Character.toLowerCase(v2_34);
                }
                v5_1.write(v2_34);
                v3_21++;
            }
            if (this.padding) {
                int v2_32 = (8 - v8_0);
                while (v2_32 < 8) {
                    v5_1.write(61);
                    v2_32++;
                }
            }
            v0_0++;
        }
        return new String(v5_1.toByteArray());
    }
}
