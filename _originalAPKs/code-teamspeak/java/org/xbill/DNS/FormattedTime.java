package org.xbill.DNS;
final class FormattedTime {
    private static java.text.NumberFormat w2;
    private static java.text.NumberFormat w4;

    static FormattedTime()
    {
        java.text.NumberFormat v0_1 = new java.text.DecimalFormat();
        org.xbill.DNS.FormattedTime.w2 = v0_1;
        v0_1.setMinimumIntegerDigits(2);
        java.text.NumberFormat v0_3 = new java.text.DecimalFormat();
        org.xbill.DNS.FormattedTime.w4 = v0_3;
        v0_3.setMinimumIntegerDigits(4);
        org.xbill.DNS.FormattedTime.w4.setGroupingUsed(0);
        return;
    }

    private FormattedTime()
    {
        return;
    }

    public static String format(java.util.Date p6)
    {
        String v0_1 = new java.util.GregorianCalendar(java.util.TimeZone.getTimeZone("UTC"));
        StringBuffer v1_3 = new StringBuffer();
        v0_1.setTime(p6);
        v1_3.append(org.xbill.DNS.FormattedTime.w4.format(((long) v0_1.get(1))));
        v1_3.append(org.xbill.DNS.FormattedTime.w2.format(((long) (v0_1.get(2) + 1))));
        v1_3.append(org.xbill.DNS.FormattedTime.w2.format(((long) v0_1.get(5))));
        v1_3.append(org.xbill.DNS.FormattedTime.w2.format(((long) v0_1.get(11))));
        v1_3.append(org.xbill.DNS.FormattedTime.w2.format(((long) v0_1.get(12))));
        v1_3.append(org.xbill.DNS.FormattedTime.w2.format(((long) v0_1.get(13))));
        return v1_3.toString();
    }

    public static java.util.Date parse(String p8)
    {
        if (p8.length() == 14) {
            org.xbill.DNS.TextParseException v0_2 = new java.util.GregorianCalendar(java.util.TimeZone.getTimeZone("UTC"));
            v0_2.clear();
            try {
                v0_2.set(Integer.parseInt(p8.substring(0, 4)), (Integer.parseInt(p8.substring(4, 6)) - 1), Integer.parseInt(p8.substring(6, 8)), Integer.parseInt(p8.substring(8, 10)), Integer.parseInt(p8.substring(10, 12)), Integer.parseInt(p8.substring(12, 14)));
                return v0_2.getTime();
            } catch (org.xbill.DNS.TextParseException v0) {
                throw new org.xbill.DNS.TextParseException(new StringBuffer("Invalid time encoding: ").append(p8).toString());
            }
        } else {
            throw new org.xbill.DNS.TextParseException(new StringBuffer("Invalid time encoding: ").append(p8).toString());
        }
    }
}
