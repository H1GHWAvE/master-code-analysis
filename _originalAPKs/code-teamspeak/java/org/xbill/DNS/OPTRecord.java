package org.xbill.DNS;
public class OPTRecord extends org.xbill.DNS.Record {
    private static final long serialVersionUID = 12192222178900183678;
    private java.util.List options;

    OPTRecord()
    {
        return;
    }

    public OPTRecord(int p7, int p8, int p9)
    {
        this(p7, p8, p9, 0, 0);
        return;
    }

    public OPTRecord(int p7, int p8, int p9, int p10)
    {
        this(p7, p8, p9, p10, 0);
        return;
    }

    public OPTRecord(int p7, int p8, int p9, int p10, java.util.List p11)
    {
        this(org.xbill.DNS.Name.root, 41, p7, 0);
        org.xbill.DNS.OPTRecord.checkU16("payloadSize", p7);
        org.xbill.DNS.OPTRecord.checkU8("xrcode", p8);
        org.xbill.DNS.OPTRecord.checkU8("version", p9);
        org.xbill.DNS.OPTRecord.checkU16("flags", p10);
        this.ttl = (((((long) p8) << 24) + (((long) p9) << 16)) + ((long) p10));
        if (p11 != null) {
            this.options = new java.util.ArrayList(p11);
        }
        return;
    }

    public boolean equals(Object p5)
    {
        if ((!super.equals(p5)) || (this.ttl != ((org.xbill.DNS.OPTRecord) p5).ttl)) {
            int v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        return v0_3;
    }

    public int getExtendedRcode()
    {
        return ((int) (this.ttl >> 24));
    }

    public int getFlags()
    {
        return ((int) (this.ttl & 65535));
    }

    org.xbill.DNS.Record getObject()
    {
        return new org.xbill.DNS.OPTRecord();
    }

    public java.util.List getOptions()
    {
        java.util.List v0_2;
        if (this.options != null) {
            v0_2 = java.util.Collections.unmodifiableList(this.options);
        } else {
            v0_2 = java.util.Collections.EMPTY_LIST;
        }
        return v0_2;
    }

    public java.util.List getOptions(int p5)
    {
        java.util.ArrayList v1_0;
        if (this.options != null) {
            v1_0 = java.util.Collections.EMPTY_LIST;
            java.util.Iterator v2 = this.options.iterator();
            while (v2.hasNext()) {
                org.xbill.DNS.EDNSOption v0_4 = ((org.xbill.DNS.EDNSOption) v2.next());
                if (v0_4.getCode() == p5) {
                    if (v1_0 == java.util.Collections.EMPTY_LIST) {
                        v1_0 = new java.util.ArrayList();
                    }
                    v1_0.add(v0_4);
                }
            }
        } else {
            v1_0 = java.util.Collections.EMPTY_LIST;
        }
        return v1_0;
    }

    public int getPayloadSize()
    {
        return this.dclass;
    }

    public int getVersion()
    {
        return ((int) ((this.ttl >> 16) & 255));
    }

    void rdataFromString(org.xbill.DNS.Tokenizer p2, org.xbill.DNS.Name p3)
    {
        throw p2.exception("no text format defined for OPT");
    }

    void rrFromWire(org.xbill.DNS.DNSInput p3)
    {
        if (p3.remaining() > 0) {
            this.options = new java.util.ArrayList();
        }
        while (p3.remaining() > 0) {
            this.options.add(org.xbill.DNS.EDNSOption.fromWire(p3));
        }
        return;
    }

    String rrToString()
    {
        String v0_1 = new StringBuffer();
        if (this.options != null) {
            v0_1.append(this.options);
            v0_1.append(" ");
        }
        v0_1.append(" ; payload ");
        v0_1.append(this.getPayloadSize());
        v0_1.append(", xrcode ");
        v0_1.append(this.getExtendedRcode());
        v0_1.append(", version ");
        v0_1.append(this.getVersion());
        v0_1.append(", flags ");
        v0_1.append(this.getFlags());
        return v0_1.toString();
    }

    void rrToWire(org.xbill.DNS.DNSOutput p3, org.xbill.DNS.Compression p4, boolean p5)
    {
        if (this.options != null) {
            java.util.Iterator v1 = this.options.iterator();
            while (v1.hasNext()) {
                ((org.xbill.DNS.EDNSOption) v1.next()).toWire(p3);
            }
        }
        return;
    }
}
