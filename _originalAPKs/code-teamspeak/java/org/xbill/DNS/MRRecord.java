package org.xbill.DNS;
public class MRRecord extends org.xbill.DNS.SingleNameBase {
    private static final long serialVersionUID = 12828804979499624083;

    MRRecord()
    {
        return;
    }

    public MRRecord(org.xbill.DNS.Name p10, int p11, long p12, org.xbill.DNS.Name p14)
    {
        this(p10, 9, p11, p12, p14, "new name");
        return;
    }

    public org.xbill.DNS.Name getNewName()
    {
        return this.getSingleName();
    }

    org.xbill.DNS.Record getObject()
    {
        return new org.xbill.DNS.MRRecord();
    }
}
