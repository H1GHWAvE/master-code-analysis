package org.xbill.DNS;
public class NSECRecord extends org.xbill.DNS.Record {
    private static final long serialVersionUID = 13281678304893286231;
    private org.xbill.DNS.Name next;
    private org.xbill.DNS.TypeBitmap types;

    NSECRecord()
    {
        return;
    }

    public NSECRecord(org.xbill.DNS.Name p8, int p9, long p10, org.xbill.DNS.Name p12, int[] p13)
    {
        this(p8, 47, p9, p10);
        this.next = org.xbill.DNS.NSECRecord.checkName("next", p12);
        org.xbill.DNS.TypeBitmap v0_3 = 0;
        while (v0_3 < p13.length) {
            org.xbill.DNS.Type.check(p13[v0_3]);
            v0_3++;
        }
        this.types = new org.xbill.DNS.TypeBitmap(p13);
        return;
    }

    public org.xbill.DNS.Name getNext()
    {
        return this.next;
    }

    org.xbill.DNS.Record getObject()
    {
        return new org.xbill.DNS.NSECRecord();
    }

    public int[] getTypes()
    {
        return this.types.toArray();
    }

    public boolean hasType(int p2)
    {
        return this.types.contains(p2);
    }

    void rdataFromString(org.xbill.DNS.Tokenizer p2, org.xbill.DNS.Name p3)
    {
        this.next = p2.getName(p3);
        this.types = new org.xbill.DNS.TypeBitmap(p2);
        return;
    }

    void rrFromWire(org.xbill.DNS.DNSInput p2)
    {
        this.next = new org.xbill.DNS.Name(p2);
        this.types = new org.xbill.DNS.TypeBitmap(p2);
        return;
    }

    String rrToString()
    {
        String v0_1 = new StringBuffer();
        v0_1.append(this.next);
        if (!this.types.empty()) {
            v0_1.append(32);
            v0_1.append(this.types.toString());
        }
        return v0_1.toString();
    }

    void rrToWire(org.xbill.DNS.DNSOutput p4, org.xbill.DNS.Compression p5, boolean p6)
    {
        this.next.toWire(p4, 0, 0);
        this.types.toWire(p4);
        return;
    }
}
