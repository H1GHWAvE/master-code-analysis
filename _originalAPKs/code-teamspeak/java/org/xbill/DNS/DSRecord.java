package org.xbill.DNS;
public class DSRecord extends org.xbill.DNS.Record {
    public static final int GOST3411_DIGEST_ID = 3;
    public static final int SHA1_DIGEST_ID = 1;
    public static final int SHA256_DIGEST_ID = 2;
    public static final int SHA384_DIGEST_ID = 4;
    private static final long serialVersionUID = 9444924744009470123;
    private int alg;
    private byte[] digest;
    private int digestid;
    private int footprint;

    DSRecord()
    {
        return;
    }

    public DSRecord(org.xbill.DNS.Name p8, int p9, long p10, int p12, int p13, int p14, byte[] p15)
    {
        this(p8, 43, p9, p10);
        this.footprint = org.xbill.DNS.DSRecord.checkU16("footprint", p12);
        this.alg = org.xbill.DNS.DSRecord.checkU8("alg", p13);
        this.digestid = org.xbill.DNS.DSRecord.checkU8("digestid", p14);
        this.digest = p15;
        return;
    }

    public DSRecord(org.xbill.DNS.Name p14, int p15, long p16, int p18, org.xbill.DNS.DNSKEYRecord p19)
    {
        this(p14, p15, p16, p19.getFootprint(), p19.getAlgorithm(), p18, org.xbill.DNS.DNSSEC.generateDSDigest(p19, p18));
        return;
    }

    public int getAlgorithm()
    {
        return this.alg;
    }

    public byte[] getDigest()
    {
        return this.digest;
    }

    public int getDigestID()
    {
        return this.digestid;
    }

    public int getFootprint()
    {
        return this.footprint;
    }

    org.xbill.DNS.Record getObject()
    {
        return new org.xbill.DNS.DSRecord();
    }

    void rdataFromString(org.xbill.DNS.Tokenizer p2, org.xbill.DNS.Name p3)
    {
        this.footprint = p2.getUInt16();
        this.alg = p2.getUInt8();
        this.digestid = p2.getUInt8();
        this.digest = p2.getHex();
        return;
    }

    void rrFromWire(org.xbill.DNS.DNSInput p2)
    {
        this.footprint = p2.readU16();
        this.alg = p2.readU8();
        this.digestid = p2.readU8();
        this.digest = p2.readByteArray();
        return;
    }

    String rrToString()
    {
        String v0_1 = new StringBuffer();
        v0_1.append(this.footprint);
        v0_1.append(" ");
        v0_1.append(this.alg);
        v0_1.append(" ");
        v0_1.append(this.digestid);
        if (this.digest != null) {
            v0_1.append(" ");
            v0_1.append(org.xbill.DNS.utils.base16.toString(this.digest));
        }
        return v0_1.toString();
    }

    void rrToWire(org.xbill.DNS.DNSOutput p2, org.xbill.DNS.Compression p3, boolean p4)
    {
        p2.writeU16(this.footprint);
        p2.writeU8(this.alg);
        p2.writeU8(this.digestid);
        if (this.digest != null) {
            p2.writeByteArray(this.digest);
        }
        return;
    }
}
