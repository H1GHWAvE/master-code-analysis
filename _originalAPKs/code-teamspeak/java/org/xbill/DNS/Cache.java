package org.xbill.DNS;
public class Cache {
    private static final int defaultMaxEntries = 50000;
    private org.xbill.DNS.Cache$CacheMap data;
    private int dclass;
    private int maxcache;
    private int maxncache;

    public Cache()
    {
        this(1);
        return;
    }

    public Cache(int p3)
    {
        this.maxncache = -1;
        this.maxcache = -1;
        this.dclass = p3;
        this.data = new org.xbill.DNS.Cache$CacheMap(50000);
        return;
    }

    public Cache(String p4)
    {
        this.maxncache = -1;
        this.maxcache = -1;
        this.data = new org.xbill.DNS.Cache$CacheMap(50000);
        org.xbill.DNS.Master v0_4 = new org.xbill.DNS.Master(p4);
        while(true) {
            org.xbill.DNS.Record v1_1 = v0_4.nextRecord();
            if (v1_1 == null) {
                break;
            }
            this.addRecord(v1_1, 0, v0_4);
        }
        return;
    }

    static int access$000(long p2, long p4)
    {
        return org.xbill.DNS.Cache.limitExpire(p2, p4);
    }

    private declared_synchronized void addElement(org.xbill.DNS.Name p5, org.xbill.DNS.Cache$Element p6)
    {
        try {
            org.xbill.DNS.Cache$CacheMap v0_1 = this.data.get(p5);
        } catch (org.xbill.DNS.Cache$CacheMap v0_7) {
            throw v0_7;
        }
        if (v0_1 != null) {
            int v3 = p6.getType();
            if (!(v0_1 instanceof java.util.List)) {
                org.xbill.DNS.Cache$CacheMap v0_2 = ((org.xbill.DNS.Cache$Element) v0_1);
                if (v0_2.getType() != v3) {
                    int v1_3 = new java.util.LinkedList();
                    v1_3.add(v0_2);
                    v1_3.add(p6);
                    this.data.put(p5, v1_3);
                } else {
                    this.data.put(p5, p6);
                }
            } else {
                org.xbill.DNS.Cache$CacheMap v0_5 = ((java.util.List) v0_1);
                int v2 = 0;
                while (v2 < v0_5.size()) {
                    if (((org.xbill.DNS.Cache$Element) v0_5.get(v2)).getType() != v3) {
                        v2++;
                    } else {
                        v0_5.set(v2, p6);
                    }
                }
                v0_5.add(p6);
            }
        } else {
            this.data.put(p5, p6);
        }
        return;
    }

    private declared_synchronized org.xbill.DNS.Cache$Element[] allElements(Object p3)
    {
        try {
            org.xbill.DNS.Cache$Element[] v0_2;
            if (!(p3 instanceof java.util.List)) {
                v0_2 = new org.xbill.DNS.Cache$Element[1];
                v0_2[0] = ((org.xbill.DNS.Cache$Element) p3);
            } else {
                org.xbill.DNS.Cache$Element[] v0_4 = new org.xbill.DNS.Cache$Element[((java.util.List) p3).size()];
                v0_2 = ((org.xbill.DNS.Cache$Element[]) ((org.xbill.DNS.Cache$Element[]) ((java.util.List) p3).toArray(v0_4)));
            }
        } catch (org.xbill.DNS.Cache$Element[] v0_7) {
            throw v0_7;
        }
        return v0_2;
    }

    private declared_synchronized Object exactName(org.xbill.DNS.Name p2)
    {
        try {
            return this.data.get(p2);
        } catch (Throwable v0_2) {
            throw v0_2;
        }
    }

    private declared_synchronized org.xbill.DNS.Cache$Element findElement(org.xbill.DNS.Name p2, int p3, int p4)
    {
        try {
            org.xbill.DNS.Cache$Element v0_1;
            org.xbill.DNS.Cache$Element v0_0 = this.exactName(p2);
        } catch (org.xbill.DNS.Cache$Element v0_2) {
            throw v0_2;
        }
        if (v0_0 != null) {
            v0_1 = this.oneElement(p2, v0_0, p3, p4);
        } else {
            v0_1 = 0;
        }
        return v0_1;
    }

    private org.xbill.DNS.RRset[] findRecords(org.xbill.DNS.Name p3, int p4, int p5)
    {
        int v0_1;
        int v0_0 = this.lookupRecords(p3, p4, p5);
        if (!v0_0.isSuccessful()) {
            v0_1 = 0;
        } else {
            v0_1 = v0_0.answers();
        }
        return v0_1;
    }

    private final int getCred(int p5, boolean p6)
    {
        IllegalArgumentException v0_0 = 4;
        if (p5 != 1) {
            if (p5 != 2) {
                if (p5 != 3) {
                    throw new IllegalArgumentException("getCred: invalid section");
                } else {
                    v0_0 = 1;
                }
            } else {
                if (!p6) {
                    v0_0 = 3;
                }
            }
        } else {
            if (!p6) {
                v0_0 = 3;
            }
        }
        return v0_0;
    }

    private static int limitExpire(long p6, long p8)
    {
        if ((p8 >= 0) && (p8 < p6)) {
            p6 = p8;
        }
        int v0_5;
        int v0_4 = ((System.currentTimeMillis() / 1000) + p6);
        if ((v0_4 >= 0) && (v0_4 <= 2147483647)) {
            v0_5 = ((int) v0_4);
        } else {
            v0_5 = 2147483647;
        }
        return v0_5;
    }

    private static void markAdditional(org.xbill.DNS.RRset p2, java.util.Set p3)
    {
        if (p2.first().getAdditionalName() != null) {
            java.util.Iterator v1 = p2.rrs();
            while (v1.hasNext()) {
                org.xbill.DNS.Name v0_5 = ((org.xbill.DNS.Record) v1.next()).getAdditionalName();
                if (v0_5 != null) {
                    p3.add(v0_5);
                }
            }
        }
        return;
    }

    private declared_synchronized org.xbill.DNS.Cache$Element oneElement(org.xbill.DNS.Name p5, Object p6, int p7, int p8)
    {
        try {
            if (p7 != 255) {
                String v0_3;
                if (!(p6 instanceof java.util.List)) {
                    if (((org.xbill.DNS.Cache$Element) p6).getType() != p7) {
                        v0_3 = 0;
                    } else {
                        v0_3 = ((org.xbill.DNS.Cache$Element) p6);
                    }
                } else {
                    int v2_0 = 0;
                    while (v2_0 < ((java.util.List) p6).size()) {
                        v0_3 = ((org.xbill.DNS.Cache$Element) ((java.util.List) p6).get(v2_0));
                        if (v0_3.getType() != p7) {
                            v2_0++;
                        }
                    }
                    v0_3 = 0;
                }
                if (v0_3 != null) {
                    if (!v0_3.expired()) {
                        if (v0_3.compareCredibility(p8) < 0) {
                            v0_3 = 0;
                        }
                    } else {
                        this.removeElement(p5, p7);
                        v0_3 = 0;
                    }
                } else {
                    v0_3 = 0;
                }
                return v0_3;
            } else {
                throw new IllegalArgumentException("oneElement(ANY)");
            }
        } catch (String v0_10) {
            throw v0_10;
        }
    }

    private declared_synchronized void removeElement(org.xbill.DNS.Name p4, int p5)
    {
        try {
            org.xbill.DNS.Cache$CacheMap v0_1 = this.data.get(p4);
        } catch (org.xbill.DNS.Cache$CacheMap v0_7) {
            throw v0_7;
        }
        if (v0_1 != null) {
            if (!(v0_1 instanceof java.util.List)) {
                if (((org.xbill.DNS.Cache$Element) v0_1).getType() == p5) {
                    this.data.remove(p4);
                }
            } else {
                org.xbill.DNS.Cache$CacheMap v0_5 = ((java.util.List) v0_1);
                int v2 = 0;
                while (v2 < v0_5.size()) {
                    if (((org.xbill.DNS.Cache$Element) v0_5.get(v2)).getType() != p5) {
                        v2++;
                    } else {
                        v0_5.remove(v2);
                        if (v0_5.size() != 0) {
                            break;
                        }
                        this.data.remove(p4);
                        break;
                    }
                }
            }
        }
        return;
    }

    private declared_synchronized void removeName(org.xbill.DNS.Name p2)
    {
        try {
            this.data.remove(p2);
            return;
        } catch (Throwable v0_1) {
            throw v0_1;
        }
    }

    public org.xbill.DNS.SetResponse addMessage(org.xbill.DNS.Message p19)
    {
        org.xbill.DNS.SetResponse v2_4;
        boolean v8 = p19.getHeader().getFlag(5);
        int v1_1 = p19.getQuestion();
        int v9 = p19.getHeader().getRcode();
        int v3_0 = 0;
        boolean v10 = org.xbill.DNS.Options.check("verbosecache");
        if (((v9 == 0) || (v9 == 3)) && (v1_1 != 0)) {
            int v6_0 = v1_1.getName();
            int v7 = v1_1.getType();
            org.xbill.DNS.RRset[] v11_0 = v1_1.getDClass();
            java.util.HashSet v12_1 = new java.util.HashSet();
            boolean v13_0 = p19.getSectionRRsets(1);
            int v5_0 = 0;
            int v4_2 = v6_0;
            int v1_4 = 0;
            while (v5_0 < v13_0.length) {
                if (v13_0[v5_0].getDClass() != v11_0) {
                    v2_4 = v3_0;
                    v3_0 = v4_2;
                } else {
                    org.xbill.DNS.SetResponse v2_8 = v13_0[v5_0].getType();
                    boolean v14_1 = v13_0[v5_0].getName();
                    int v15_1 = this.getCred(1, v8);
                    if (((v2_8 != v7) && (v7 != 255)) || (!v14_1.equals(v4_2))) {
                        if ((v2_8 != 5) || (!v14_1.equals(v4_2))) {
                            if ((v2_8 != 39) || (!v4_2.subdomain(v14_1))) {
                            } else {
                                org.xbill.DNS.SetResponse v2_11;
                                this.addRRset(v13_0[v5_0], v15_1);
                                if (v4_2 != v6_0) {
                                    v2_11 = v1_4;
                                } else {
                                    v2_11 = new org.xbill.DNS.SetResponse(5, v13_0[v5_0]);
                                }
                                try {
                                    v2_4 = v3_0;
                                    v3_0 = v4_2.fromDNAME(((org.xbill.DNS.DNAMERecord) v13_0[v5_0].first()));
                                    v1_4 = v2_11;
                                } catch (int v1) {
                                    org.xbill.DNS.RRset[] v11_1 = p19.getSectionRRsets(2);
                                    int v1_13 = 0;
                                    int v5_2 = 0;
                                    int v6_2 = 0;
                                    while (v6_2 < v11_1.length) {
                                        if ((v11_1[v6_2].getType() != 6) || (!v4_2.subdomain(v11_1[v6_2].getName()))) {
                                            if ((v11_1[v6_2].getType() == 2) && (v4_2.subdomain(v11_1[v6_2].getName()))) {
                                                v1_13 = v11_1[v6_2];
                                            }
                                        } else {
                                            v5_2 = v11_1[v6_2];
                                        }
                                        v6_2++;
                                    }
                                    if (v3_0 != 0) {
                                        if ((v9 == 0) && (v1_13 != 0)) {
                                            this.addRRset(v1_13, this.getCred(2, v8));
                                            org.xbill.DNS.Cache.markAdditional(v1_13, v12_1);
                                        }
                                    } else {
                                        int v3_4;
                                        if (v9 != 3) {
                                            v3_4 = v7;
                                        } else {
                                            v3_4 = 0;
                                        }
                                        if ((v9 != 3) && ((v5_2 == 0) && (v1_13 != 0))) {
                                            this.addRRset(v1_13, this.getCred(2, v8));
                                            org.xbill.DNS.Cache.markAdditional(v1_13, v12_1);
                                            if (v2_4 == null) {
                                                v2_4 = new org.xbill.DNS.SetResponse(3, v1_13);
                                            }
                                        } else {
                                            int v6_4 = this.getCred(2, v8);
                                            int v1_15 = 0;
                                            if (v5_2 != 0) {
                                                v1_15 = ((org.xbill.DNS.SOARecord) v5_2.first());
                                            }
                                            this.addNegative(v4_2, v3_4, v1_15, v6_4);
                                            if (v2_4 == null) {
                                                int v1_18;
                                                if (v9 != 3) {
                                                    v1_18 = 2;
                                                } else {
                                                    v1_18 = 1;
                                                }
                                                v2_4 = org.xbill.DNS.SetResponse.ofType(v1_18);
                                            }
                                        }
                                    }
                                    int v3_8 = p19.getSectionRRsets(3);
                                    int v1_20 = 0;
                                    while (v1_20 < v3_8.length) {
                                        int v4_6 = v3_8[v1_20].getType();
                                        if (((v4_6 == 1) || ((v4_6 == 28) || (v4_6 == 38))) && (v12_1.contains(v3_8[v1_20].getName()))) {
                                            this.addRRset(v3_8[v1_20], this.getCred(3, v8));
                                        }
                                        v1_20++;
                                    }
                                    if (!v10) {
                                        return v2_4;
                                    } else {
                                        System.out.println(new StringBuffer("addMessage: ").append(v2_4).toString());
                                        return v2_4;
                                    }
                                }
                            }
                        } else {
                            org.xbill.DNS.SetResponse v2_15;
                            this.addRRset(v13_0[v5_0], v15_1);
                            if (v4_2 != v6_0) {
                                v2_15 = v1_4;
                            } else {
                                v2_15 = new org.xbill.DNS.SetResponse(4, v13_0[v5_0]);
                            }
                            v2_4 = v3_0;
                            v3_0 = ((org.xbill.DNS.CNAMERecord) v13_0[v5_0].first()).getTarget();
                            v1_4 = v2_15;
                        }
                    } else {
                        this.addRRset(v13_0[v5_0], v15_1);
                        v2_4 = 1;
                        if (v4_2 == v6_0) {
                            if (v1_4 == 0) {
                                v1_4 = new org.xbill.DNS.SetResponse(6);
                            }
                            v1_4.addRRset(v13_0[v5_0]);
                        }
                        org.xbill.DNS.Cache.markAdditional(v13_0[v5_0], v12_1);
                        v3_0 = v4_2;
                    }
                }
                v5_0++;
                v4_2 = v3_0;
                v3_0 = v2_4;
            }
            v2_4 = v1_4;
        } else {
            v2_4 = 0;
        }
        return v2_4;
    }

    public declared_synchronized void addNegative(org.xbill.DNS.Name p9, int p10, org.xbill.DNS.SOARecord p11, int p12)
    {
        try {
            org.xbill.DNS.Name v2_0;
            if (p11 == null) {
                v2_0 = 0;
            } else {
                v2_0 = p11.getTTL();
            }
        } catch (int v0_5) {
            throw v0_5;
        }
        int v0_2 = this.findElement(p9, p10, 0);
        if (v2_0 != 0) {
            if ((v0_2 != 0) && (v0_2.compareCredibility(p12) <= 0)) {
                v0_2 = 0;
            }
            if (v0_2 == 0) {
                this.addElement(p9, new org.xbill.DNS.Cache$NegativeElement(p9, p10, p11, p12, ((long) this.maxncache)));
            }
        } else {
            if ((v0_2 != 0) && (v0_2.compareCredibility(p12) <= 0)) {
                this.removeElement(p9, p10);
            }
        }
        return;
    }

    public declared_synchronized void addRRset(org.xbill.DNS.RRset p9, int p10)
    {
        try {
            long v2_0 = p9.getTTL();
            org.xbill.DNS.Name v1 = p9.getName();
            int v4 = p9.getType();
            org.xbill.DNS.Cache$CacheRRset v0_1 = this.findElement(v1, v4, 0);
        } catch (org.xbill.DNS.Cache$CacheRRset v0_6) {
            throw v0_6;
        }
        if (v2_0 != 0) {
            if ((v0_1 != null) && (v0_1.compareCredibility(p10) <= 0)) {
                v0_1 = 0;
            }
            if (v0_1 == null) {
                org.xbill.DNS.Cache$CacheRRset v9_1;
                if (!(p9 instanceof org.xbill.DNS.Cache$CacheRRset)) {
                    v9_1 = new org.xbill.DNS.Cache$CacheRRset(p9, p10, ((long) this.maxcache));
                } else {
                    v9_1 = ((org.xbill.DNS.Cache$CacheRRset) p9);
                }
                this.addElement(v1, v9_1);
            }
        } else {
            if ((v0_1 != null) && (v0_1.compareCredibility(p10) <= 0)) {
                this.removeElement(v1, v4);
            }
        }
        return;
    }

    public declared_synchronized void addRecord(org.xbill.DNS.Record p5, int p6, Object p7)
    {
        try {
            org.xbill.DNS.Cache$CacheRRset v0_0 = p5.getName();
            boolean v1_0 = p5.getRRsetType();
        } catch (org.xbill.DNS.Cache$CacheRRset v0_2) {
            throw v0_2;
        }
        if (org.xbill.DNS.Type.isRR(v1_0)) {
            org.xbill.DNS.Cache$CacheRRset v0_1 = this.findElement(v0_0, v1_0, p6);
            if (v0_1 != null) {
                if ((v0_1.compareCredibility(p6) == 0) && ((v0_1 instanceof org.xbill.DNS.Cache$CacheRRset))) {
                    ((org.xbill.DNS.Cache$CacheRRset) v0_1).addRR(p5);
                }
            } else {
                this.addRRset(new org.xbill.DNS.Cache$CacheRRset(p5, p6, ((long) this.maxcache)), p6);
            }
        }
        return;
    }

    public declared_synchronized void clearCache()
    {
        try {
            this.data.clear();
            return;
        } catch (Throwable v0_1) {
            throw v0_1;
        }
    }

    public org.xbill.DNS.RRset[] findAnyRecords(org.xbill.DNS.Name p2, int p3)
    {
        return this.findRecords(p2, p3, 2);
    }

    public org.xbill.DNS.RRset[] findRecords(org.xbill.DNS.Name p2, int p3)
    {
        return this.findRecords(p2, p3, 3);
    }

    public void flushName(org.xbill.DNS.Name p1)
    {
        this.removeName(p1);
        return;
    }

    public void flushSet(org.xbill.DNS.Name p1, int p2)
    {
        this.removeElement(p1, p2);
        return;
    }

    public int getDClass()
    {
        return this.dclass;
    }

    public int getMaxCache()
    {
        return this.maxcache;
    }

    public int getMaxEntries()
    {
        return this.data.getMaxSize();
    }

    public int getMaxNCache()
    {
        return this.maxncache;
    }

    public int getSize()
    {
        return this.data.size();
    }

    protected declared_synchronized org.xbill.DNS.SetResponse lookup(org.xbill.DNS.Name p12, int p13, int p14)
    {
        try {
            int v7 = p12.labels();
            int v6 = v7;
        } catch (org.xbill.DNS.SetResponse v0_35) {
            throw v0_35;
        }
        while (v6 > 0) {
            org.xbill.DNS.SetResponse v1_0;
            if (v6 != 1) {
                v1_0 = 0;
            } else {
                v1_0 = 1;
            }
            int v5;
            if (v6 != v7) {
                v5 = 0;
            } else {
                v5 = 1;
            }
            org.xbill.DNS.Name v4;
            if (v1_0 == null) {
                if (v5 == 0) {
                    v4 = new org.xbill.DNS.Name(p12, (v7 - v6));
                } else {
                    v4 = p12;
                }
            } else {
                v4 = org.xbill.DNS.Name.root;
            }
            Object v8 = this.data.get(v4);
            if (v8 != null) {
                if ((v5 == 0) || (p13 != 255)) {
                    if (v5 == 0) {
                        org.xbill.DNS.SetResponse v0_13 = this.oneElement(v4, v8, 39, p14);
                        if ((v0_13 != null) && ((v0_13 instanceof org.xbill.DNS.Cache$CacheRRset))) {
                            org.xbill.DNS.SetResponse v0_1 = new org.xbill.DNS.SetResponse(5, ((org.xbill.DNS.Cache$CacheRRset) v0_13));
                            return v0_1;
                        }
                    } else {
                        org.xbill.DNS.SetResponse v0_15 = this.oneElement(v4, v8, p13, p14);
                        if ((v0_15 == null) || (!(v0_15 instanceof org.xbill.DNS.Cache$CacheRRset))) {
                            if (v0_15 == null) {
                                org.xbill.DNS.SetResponse v0_17 = this.oneElement(v4, v8, 5, p14);
                                if ((v0_17 != null) && ((v0_17 instanceof org.xbill.DNS.Cache$CacheRRset))) {
                                    v0_1 = new org.xbill.DNS.SetResponse(4, ((org.xbill.DNS.Cache$CacheRRset) v0_17));
                                    return v0_1;
                                }
                            } else {
                                v0_1 = new org.xbill.DNS.SetResponse(2);
                                return v0_1;
                            }
                        } else {
                            org.xbill.DNS.SetResponse v1_11 = new org.xbill.DNS.SetResponse(6);
                            v1_11.addRRset(((org.xbill.DNS.Cache$CacheRRset) v0_15));
                            v0_1 = v1_11;
                            return v0_1;
                        }
                    }
                } else {
                    int v2_4 = new org.xbill.DNS.SetResponse(6);
                    org.xbill.DNS.Cache$Element[] v9 = this.allElements(v8);
                    org.xbill.DNS.SetResponse v1_12 = 0;
                    int v3 = 0;
                    while (v3 < v9.length) {
                        org.xbill.DNS.SetResponse v0_32;
                        org.xbill.DNS.SetResponse v0_31 = v9[v3];
                        if (!v0_31.expired()) {
                            if ((!(v0_31 instanceof org.xbill.DNS.Cache$CacheRRset)) || (v0_31.compareCredibility(p14) < 0)) {
                                v0_32 = v1_12;
                            } else {
                                v2_4.addRRset(((org.xbill.DNS.Cache$CacheRRset) v0_31));
                                v0_32 = (v1_12 + 1);
                            }
                        } else {
                            this.removeElement(v4, v0_31.getType());
                            v0_32 = v1_12;
                        }
                        v3++;
                        v1_12 = v0_32;
                    }
                    if (v1_12 > null) {
                        v0_1 = v2_4;
                        return v0_1;
                    }
                }
                org.xbill.DNS.SetResponse v0_25 = this.oneElement(v4, v8, 2, p14);
                if ((v0_25 == null) || (!(v0_25 instanceof org.xbill.DNS.Cache$CacheRRset))) {
                    if ((v5 != 0) && (this.oneElement(v4, v8, 0, p14) != null)) {
                        v0_1 = org.xbill.DNS.SetResponse.ofType(1);
                        return v0_1;
                    }
                } else {
                    v0_1 = new org.xbill.DNS.SetResponse(3, ((org.xbill.DNS.Cache$CacheRRset) v0_25));
                    return v0_1;
                }
            }
            v6--;
        }
        v0_1 = org.xbill.DNS.SetResponse.ofType(0);
        return v0_1;
    }

    public org.xbill.DNS.SetResponse lookupRecords(org.xbill.DNS.Name p2, int p3, int p4)
    {
        return this.lookup(p2, p3, p4);
    }

    public void setMaxCache(int p1)
    {
        this.maxcache = p1;
        return;
    }

    public void setMaxEntries(int p2)
    {
        this.data.setMaxSize(p2);
        return;
    }

    public void setMaxNCache(int p1)
    {
        this.maxncache = p1;
        return;
    }

    public String toString()
    {
        StringBuffer v1_1 = new StringBuffer();
        java.util.Iterator v2 = this.data.values().iterator();
        while (v2.hasNext()) {
            org.xbill.DNS.Cache$Element[] v3 = this.allElements(v2.next());
            int v0_5 = 0;
            while (v0_5 < v3.length) {
                v1_1.append(v3[v0_5]);
                v1_1.append("\n");
                v0_5++;
            }
        }
        return v1_1.toString();
    }
}
