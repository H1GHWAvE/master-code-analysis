package org.xbill.DNS;
public class SRVRecord extends org.xbill.DNS.Record {
    private static final long serialVersionUID = 14560283941322029564;
    private int port;
    private int priority;
    private org.xbill.DNS.Name target;
    private int weight;

    SRVRecord()
    {
        return;
    }

    public SRVRecord(org.xbill.DNS.Name p8, int p9, long p10, int p12, int p13, int p14, org.xbill.DNS.Name p15)
    {
        this(p8, 33, p9, p10);
        this.priority = org.xbill.DNS.SRVRecord.checkU16("priority", p12);
        this.weight = org.xbill.DNS.SRVRecord.checkU16("weight", p13);
        this.port = org.xbill.DNS.SRVRecord.checkU16("port", p14);
        this.target = org.xbill.DNS.SRVRecord.checkName("target", p15);
        return;
    }

    public org.xbill.DNS.Name getAdditionalName()
    {
        return this.target;
    }

    org.xbill.DNS.Record getObject()
    {
        return new org.xbill.DNS.SRVRecord();
    }

    public int getPort()
    {
        return this.port;
    }

    public int getPriority()
    {
        return this.priority;
    }

    public org.xbill.DNS.Name getTarget()
    {
        return this.target;
    }

    public int getWeight()
    {
        return this.weight;
    }

    void rdataFromString(org.xbill.DNS.Tokenizer p2, org.xbill.DNS.Name p3)
    {
        this.priority = p2.getUInt16();
        this.weight = p2.getUInt16();
        this.port = p2.getUInt16();
        this.target = p2.getName(p3);
        return;
    }

    void rrFromWire(org.xbill.DNS.DNSInput p2)
    {
        this.priority = p2.readU16();
        this.weight = p2.readU16();
        this.port = p2.readU16();
        this.target = new org.xbill.DNS.Name(p2);
        return;
    }

    String rrToString()
    {
        String v0_1 = new StringBuffer();
        v0_1.append(new StringBuffer().append(this.priority).append(" ").toString());
        v0_1.append(new StringBuffer().append(this.weight).append(" ").toString());
        v0_1.append(new StringBuffer().append(this.port).append(" ").toString());
        v0_1.append(this.target);
        return v0_1.toString();
    }

    void rrToWire(org.xbill.DNS.DNSOutput p3, org.xbill.DNS.Compression p4, boolean p5)
    {
        p3.writeU16(this.priority);
        p3.writeU16(this.weight);
        p3.writeU16(this.port);
        this.target.toWire(p3, 0, p5);
        return;
    }
}
