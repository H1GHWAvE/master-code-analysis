package org.xbill.DNS;
public class DNSSEC$Algorithm {
    public static final int DH = 2;
    public static final int DSA = 3;
    public static final int DSA_NSEC3_SHA1 = 6;
    public static final int ECC_GOST = 12;
    public static final int ECDSAP256SHA256 = 13;
    public static final int ECDSAP384SHA384 = 14;
    public static final int INDIRECT = 252;
    public static final int PRIVATEDNS = 253;
    public static final int PRIVATEOID = 254;
    public static final int RSAMD5 = 1;
    public static final int RSASHA1 = 5;
    public static final int RSASHA256 = 8;
    public static final int RSASHA512 = 10;
    public static final int RSA_NSEC3_SHA1 = 7;
    private static org.xbill.DNS.Mnemonic algs;

    static DNSSEC$Algorithm()
    {
        org.xbill.DNS.Mnemonic v0_1 = new org.xbill.DNS.Mnemonic("DNSSEC algorithm", 2);
        org.xbill.DNS.DNSSEC$Algorithm.algs = v0_1;
        v0_1.setMaximum(255);
        org.xbill.DNS.DNSSEC$Algorithm.algs.setNumericAllowed(1);
        org.xbill.DNS.DNSSEC$Algorithm.algs.add(1, "RSAMD5");
        org.xbill.DNS.DNSSEC$Algorithm.algs.add(2, "DH");
        org.xbill.DNS.DNSSEC$Algorithm.algs.add(3, "DSA");
        org.xbill.DNS.DNSSEC$Algorithm.algs.add(5, "RSASHA1");
        org.xbill.DNS.DNSSEC$Algorithm.algs.add(6, "DSA-NSEC3-SHA1");
        org.xbill.DNS.DNSSEC$Algorithm.algs.add(7, "RSA-NSEC3-SHA1");
        org.xbill.DNS.DNSSEC$Algorithm.algs.add(8, "RSASHA256");
        org.xbill.DNS.DNSSEC$Algorithm.algs.add(10, "RSASHA512");
        org.xbill.DNS.DNSSEC$Algorithm.algs.add(12, "ECC-GOST");
        org.xbill.DNS.DNSSEC$Algorithm.algs.add(13, "ECDSAP256SHA256");
        org.xbill.DNS.DNSSEC$Algorithm.algs.add(14, "ECDSAP384SHA384");
        org.xbill.DNS.DNSSEC$Algorithm.algs.add(252, "INDIRECT");
        org.xbill.DNS.DNSSEC$Algorithm.algs.add(253, "PRIVATEDNS");
        org.xbill.DNS.DNSSEC$Algorithm.algs.add(254, "PRIVATEOID");
        return;
    }

    private DNSSEC$Algorithm()
    {
        return;
    }

    public static String string(int p1)
    {
        return org.xbill.DNS.DNSSEC$Algorithm.algs.getText(p1);
    }

    public static int value(String p1)
    {
        return org.xbill.DNS.DNSSEC$Algorithm.algs.getValue(p1);
    }
}
