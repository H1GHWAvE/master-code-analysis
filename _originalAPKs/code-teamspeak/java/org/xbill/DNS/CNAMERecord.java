package org.xbill.DNS;
public class CNAMERecord extends org.xbill.DNS.SingleCompressedNameBase {
    private static final long serialVersionUID = 14426370186817013036;

    CNAMERecord()
    {
        return;
    }

    public CNAMERecord(org.xbill.DNS.Name p10, int p11, long p12, org.xbill.DNS.Name p14)
    {
        this(p10, 5, p11, p12, p14, "alias");
        return;
    }

    public org.xbill.DNS.Name getAlias()
    {
        return this.getSingleName();
    }

    org.xbill.DNS.Record getObject()
    {
        return new org.xbill.DNS.CNAMERecord();
    }

    public org.xbill.DNS.Name getTarget()
    {
        return this.getSingleName();
    }
}
