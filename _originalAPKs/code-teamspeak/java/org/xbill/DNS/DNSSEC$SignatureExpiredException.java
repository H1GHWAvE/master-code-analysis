package org.xbill.DNS;
public class DNSSEC$SignatureExpiredException extends org.xbill.DNS.DNSSEC$DNSSECException {
    private java.util.Date now;
    private java.util.Date when;

    DNSSEC$SignatureExpiredException(java.util.Date p2, java.util.Date p3)
    {
        this("signature expired");
        this.when = p2;
        this.now = p3;
        return;
    }

    public java.util.Date getExpiration()
    {
        return this.when;
    }

    public java.util.Date getVerifyTime()
    {
        return this.now;
    }
}
