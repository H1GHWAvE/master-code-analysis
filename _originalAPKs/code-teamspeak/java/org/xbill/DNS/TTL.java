package org.xbill.DNS;
public final class TTL {
    public static final long MAX_VALUE = 2147483647;

    private TTL()
    {
        return;
    }

    static void check(long p2)
    {
        if ((p2 >= 0) && (p2 <= 2147483647)) {
            return;
        } else {
            throw new org.xbill.DNS.InvalidTTLException(p2);
        }
    }

    public static String format(long p14)
    {
        org.xbill.DNS.TTL.check(p14);
        String v0_1 = new StringBuffer();
        String v2_1 = (p14 % 60);
        long v4_1 = (p14 / 60);
        long v6_1 = (v4_1 % 60);
        long v4_2 = (v4_1 / 60);
        long v8_2 = (v4_2 % 24);
        long v4_3 = (v4_2 / 24);
        long v10_2 = (v4_3 % 7);
        long v4_4 = (v4_3 / 7);
        if (v4_4 > 0) {
            v0_1.append(new StringBuffer().append(v4_4).append("W").toString());
        }
        if (v10_2 > 0) {
            v0_1.append(new StringBuffer().append(v10_2).append("D").toString());
        }
        if (v8_2 > 0) {
            v0_1.append(new StringBuffer().append(v8_2).append("H").toString());
        }
        if (v6_1 > 0) {
            v0_1.append(new StringBuffer().append(v6_1).append("M").toString());
        }
        if ((v2_1 > 0) || ((v4_4 == 0) && ((v10_2 == 0) && ((v8_2 == 0) && (v6_1 == 0))))) {
            v0_1.append(new StringBuffer().append(v2_1).append("S").toString());
        }
        return v0_1.toString();
    }

    public static long parse(String p10, boolean p11)
    {
        if ((p10 != null) && ((p10.length() != 0) && (Character.isDigit(p10.charAt(0))))) {
            long v4 = 0;
            long v2_0 = 0;
            long v0_4 = 0;
            while (v0_4 < p10.length()) {
                long v1_1 = p10.charAt(v0_4);
                if (!Character.isDigit(v1_1)) {
                    switch (Character.toUpperCase(v1_1)) {
                        case 68:
                            v4 *= 24;
                        case 72:
                            v4 *= 60;
                        case 77:
                            v4 *= 60;
                        case 83:
                            break;
                        case 87:
                            v4 *= 7;
                            break;
                        default:
                            throw new NumberFormatException();
                    }
                    v2_0 += v4;
                    v4 = 0;
                    if (v2_0 > 2.1219957905e-314) {
                        throw new NumberFormatException();
                    }
                } else {
                    long v6_8 = ((10 * v4) + ((long) Character.getNumericValue(v1_1)));
                    if (v6_8 >= v4) {
                        v4 = v6_8;
                    } else {
                        throw new NumberFormatException();
                    }
                }
                v0_4++;
            }
            long v0_7;
            if (v2_0 != 0) {
                v0_7 = v2_0;
            } else {
                v0_7 = v4;
            }
            if (v0_7 <= 2.1219957905e-314) {
                if ((v0_7 > 2147483647) && (p11)) {
                    v0_7 = 2147483647;
                }
                return v0_7;
            } else {
                throw new NumberFormatException();
            }
        } else {
            throw new NumberFormatException();
        }
    }

    public static long parseTTL(String p2)
    {
        return org.xbill.DNS.TTL.parse(p2, 1);
    }
}
