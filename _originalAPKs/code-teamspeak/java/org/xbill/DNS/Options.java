package org.xbill.DNS;
public final class Options {
    private static java.util.Map table;

    static Options()
    {
        try {
            org.xbill.DNS.Options.refresh();
        } catch (SecurityException v0) {
        }
        return;
    }

    private Options()
    {
        return;
    }

    public static boolean check(String p3)
    {
        int v0 = 0;
        if ((org.xbill.DNS.Options.table != null) && (org.xbill.DNS.Options.table.get(p3.toLowerCase()) != null)) {
            v0 = 1;
        }
        return v0;
    }

    public static void clear()
    {
        org.xbill.DNS.Options.table = 0;
        return;
    }

    public static int intValue(String p1)
    {
        NumberFormatException v0_1;
        NumberFormatException v0_0 = org.xbill.DNS.Options.value(p1);
        if (v0_0 == null) {
            v0_1 = -1;
        } else {
            try {
                v0_1 = Integer.parseInt(v0_0);
            } catch (NumberFormatException v0) {
            }
            if (v0_1 <= null) {
            }
        }
        return v0_1;
    }

    public static void refresh()
    {
        String v0_1 = System.getProperty("dnsjava.options");
        if (v0_1 != null) {
            java.util.StringTokenizer v1_1 = new java.util.StringTokenizer(v0_1, ",");
            while (v1_1.hasMoreTokens()) {
                String v0_3 = v1_1.nextToken();
                int v2_2 = v0_3.indexOf(61);
                if (v2_2 != -1) {
                    org.xbill.DNS.Options.set(v0_3.substring(0, v2_2), v0_3.substring((v2_2 + 1)));
                } else {
                    org.xbill.DNS.Options.set(v0_3);
                }
            }
        }
        return;
    }

    public static void set(String p3)
    {
        if (org.xbill.DNS.Options.table == null) {
            org.xbill.DNS.Options.table = new java.util.HashMap();
        }
        org.xbill.DNS.Options.table.put(p3.toLowerCase(), "true");
        return;
    }

    public static void set(String p3, String p4)
    {
        if (org.xbill.DNS.Options.table == null) {
            org.xbill.DNS.Options.table = new java.util.HashMap();
        }
        org.xbill.DNS.Options.table.put(p3.toLowerCase(), p4.toLowerCase());
        return;
    }

    public static void unset(String p2)
    {
        if (org.xbill.DNS.Options.table != null) {
            org.xbill.DNS.Options.table.remove(p2.toLowerCase());
        }
        return;
    }

    public static String value(String p2)
    {
        String v0_3;
        if (org.xbill.DNS.Options.table != null) {
            v0_3 = ((String) org.xbill.DNS.Options.table.get(p2.toLowerCase()));
        } else {
            v0_3 = 0;
        }
        return v0_3;
    }
}
