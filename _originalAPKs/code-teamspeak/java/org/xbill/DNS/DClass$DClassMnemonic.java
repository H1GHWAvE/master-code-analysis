package org.xbill.DNS;
 class DClass$DClassMnemonic extends org.xbill.DNS.Mnemonic {

    public DClass$DClassMnemonic()
    {
        this("DClass", 2);
        this.setPrefix("CLASS");
        return;
    }

    public void check(int p1)
    {
        org.xbill.DNS.DClass.check(p1);
        return;
    }
}
