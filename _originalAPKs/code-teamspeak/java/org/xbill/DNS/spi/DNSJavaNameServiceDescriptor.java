package org.xbill.DNS.spi;
public class DNSJavaNameServiceDescriptor implements sun.net.spi.nameservice.NameServiceDescriptor {
    static Class class$sun$net$spi$nameservice$NameService;
    private static sun.net.spi.nameservice.NameService nameService;

    static DNSJavaNameServiceDescriptor()
    {
        sun.net.spi.nameservice.NameService v0_1;
        if (org.xbill.DNS.spi.DNSJavaNameServiceDescriptor.class$sun$net$spi$nameservice$NameService != null) {
            v0_1 = org.xbill.DNS.spi.DNSJavaNameServiceDescriptor.class$sun$net$spi$nameservice$NameService;
        } else {
            v0_1 = org.xbill.DNS.spi.DNSJavaNameServiceDescriptor.class$("sun.net.spi.nameservice.NameService");
            org.xbill.DNS.spi.DNSJavaNameServiceDescriptor.class$sun$net$spi$nameservice$NameService = v0_1;
        }
        sun.net.spi.nameservice.NameService v0_5;
        ClassLoader v1 = v0_1.getClassLoader();
        Class[] v2 = new Class[1];
        if (org.xbill.DNS.spi.DNSJavaNameServiceDescriptor.class$sun$net$spi$nameservice$NameService != null) {
            v0_5 = org.xbill.DNS.spi.DNSJavaNameServiceDescriptor.class$sun$net$spi$nameservice$NameService;
        } else {
            v0_5 = org.xbill.DNS.spi.DNSJavaNameServiceDescriptor.class$("sun.net.spi.nameservice.NameService");
            org.xbill.DNS.spi.DNSJavaNameServiceDescriptor.class$sun$net$spi$nameservice$NameService = v0_5;
        }
        v2[0] = v0_5;
        org.xbill.DNS.spi.DNSJavaNameServiceDescriptor.nameService = ((sun.net.spi.nameservice.NameService) reflect.Proxy.newProxyInstance(v1, v2, new org.xbill.DNS.spi.DNSJavaNameService()));
        return;
    }

    public DNSJavaNameServiceDescriptor()
    {
        return;
    }

    static Class class$(String p2)
    {
        try {
            return Class.forName(p2);
        } catch (Throwable v0_1) {
            throw new NoClassDefFoundError().initCause(v0_1);
        }
    }

    public sun.net.spi.nameservice.NameService createNameService()
    {
        return org.xbill.DNS.spi.DNSJavaNameServiceDescriptor.nameService;
    }

    public String getProviderName()
    {
        return "dnsjava";
    }

    public String getType()
    {
        return "dns";
    }
}
