package org.xbill.DNS;
public class LOCRecord extends org.xbill.DNS.Record {
    private static final long serialVersionUID = 9058224788126750409;
    private static java.text.NumberFormat w2;
    private static java.text.NumberFormat w3;
    private long altitude;
    private long hPrecision;
    private long latitude;
    private long longitude;
    private long size;
    private long vPrecision;

    static LOCRecord()
    {
        java.text.DecimalFormat v0_1 = new java.text.DecimalFormat();
        org.xbill.DNS.LOCRecord.w2 = v0_1;
        v0_1.setMinimumIntegerDigits(2);
        java.text.DecimalFormat v0_3 = new java.text.DecimalFormat();
        org.xbill.DNS.LOCRecord.w3 = v0_3;
        v0_3.setMinimumIntegerDigits(3);
        return;
    }

    LOCRecord()
    {
        return;
    }

    public LOCRecord(org.xbill.DNS.Name p8, int p9, long p10, double p12, double p14, double p16, double p18, double p20, double p22)
    {
        this(p8, 29, p9, p10);
        this.latitude = ((long) (((3600.0 * p12) * 1000.0) + 2147483648.0));
        this.longitude = ((long) (((3600.0 * p14) * 1000.0) + 2147483648.0));
        this.altitude = ((long) ((100000.0 + p16) * 100.0));
        this.size = ((long) (100.0 * p18));
        this.hPrecision = ((long) (100.0 * p20));
        this.vPrecision = ((long) (100.0 * p22));
        return;
    }

    private long parseDouble(org.xbill.DNS.Tokenizer p5, String p6, boolean p7, long p8, long p10, long p12)
    {
        org.xbill.DNS.TextParseException v0_0 = p5.get();
        if (!v0_0.isEOL()) {
            org.xbill.DNS.TextParseException v0_1 = v0_0.value;
            if ((v0_1.length() > 1) && (v0_1.charAt((v0_1.length() - 1)) == 109)) {
                v0_1 = v0_1.substring(0, (v0_1.length() - 1));
            }
            try {
                p12 = ((long) (this.parseFixedPoint(v0_1) * 100.0));
            } catch (org.xbill.DNS.TextParseException v0) {
                throw p5.exception(new StringBuffer("Invalid LOC ").append(p6).toString());
            }
            if ((p12 < p8) || (p12 > p10)) {
                throw p5.exception(new StringBuffer("Invalid LOC ").append(p6).toString());
            }
        } else {
            if (!p7) {
                p5.unget();
            } else {
                throw p5.exception(new StringBuffer("Invalid LOC ").append(p6).toString());
            }
        }
        return p12;
    }

    private double parseFixedPoint(String p10)
    {
        double v0_14;
        if (!p10.matches("^-?\\d+$")) {
            if (!p10.matches("^-?\\d+\\.\\d*$")) {
                throw new NumberFormatException();
            } else {
                double v2_0 = p10.split("\\.");
                double v4 = ((double) Integer.parseInt(v2_0[0]));
                double v0_12 = ((double) Integer.parseInt(v2_0[1]));
                if (v4 < 0) {
                    v0_12 *= -1.0;
                }
                v0_14 = ((v0_12 / Math.pow(10.0, ((double) v2_0[1].length()))) + v4);
            }
        } else {
            v0_14 = ((double) Integer.parseInt(p10));
        }
        return v0_14;
    }

    private static long parseLOCformat(int p6)
    {
        long v0_1 = ((long) (p6 >> 4));
        int v2 = (p6 & 15);
        if ((v0_1 > 9) || (v2 > 9)) {
            throw new org.xbill.DNS.WireParseException("Invalid LOC Encoding");
        }
        while (v2 > 0) {
            v0_1 *= 10;
            v2--;
        }
        return v0_1;
    }

    private long parsePosition(org.xbill.DNS.Tokenizer p19, String p20)
    {
        boolean v6 = p20.equals("latitude");
        double v4_0 = 0;
        int v7 = p19.getUInt16();
        if ((v7 <= 180) && ((v7 <= 90) || (!v6))) {
            double v2_3 = p19.getString();
            try {
                int v3_0 = Integer.parseInt(v2_3);
            } catch (org.xbill.DNS.TextParseException v8) {
                if (v2_3.length() == 1) {
                    double v2_6;
                    double v4_3 = ((long) ((v4_0 + ((double) (60 * (((long) v3_0) + (60 * ((long) v7)))))) * 1000.0));
                    double v2_5 = Character.toUpperCase(v2_3.charAt(0));
                    if (((!v6) || (v2_5 != 83)) && ((v6) || (v2_5 != 87))) {
                        if (((!v6) || (v2_5 == 78)) && ((v6) || (v2_5 == 69))) {
                            v2_6 = v4_3;
                        } else {
                            throw p19.exception(new StringBuffer("Invalid LOC ").append(p20).toString());
                        }
                    } else {
                        v2_6 = (- v4_3);
                    }
                    return (v2_6 + 1.0609978955e-314);
                } else {
                    throw p19.exception(new StringBuffer("Invalid LOC ").append(p20).toString());
                }
            }
            if ((v3_0 >= 0) && (v3_0 <= 59)) {
                v2_3 = p19.getString();
                v4_0 = this.parseFixedPoint(v2_3);
                if ((v4_0 >= 0) && (v4_0 < 60.0)) {
                    v2_3 = p19.getString();
                } else {
                    throw p19.exception(new StringBuffer("Invalid LOC ").append(p20).append(" seconds").toString());
                }
            } else {
                throw p19.exception(new StringBuffer("Invalid LOC ").append(p20).append(" minutes").toString());
            }
        } else {
            throw p19.exception(new StringBuffer("Invalid LOC ").append(p20).append(" degrees").toString());
        }
    }

    private String positionToString(long p12, char p14, char p15)
    {
        StringBuffer v2_1 = new StringBuffer();
        String v0_1 = (p12 - 1.0609978955e-314);
        if (v0_1 >= 0) {
            p15 = p14;
        } else {
            v0_1 = (- v0_1);
        }
        v2_1.append((v0_1 / 3600000));
        String v0_2 = (v0_1 % 3600000);
        v2_1.append(" ");
        v2_1.append((v0_2 / 60000));
        long v4_3 = (v0_2 % 60000);
        v2_1.append(" ");
        this.renderFixedPoint(v2_1, org.xbill.DNS.LOCRecord.w3, v4_3, 1000);
        v2_1.append(" ");
        v2_1.append(p15);
        return v2_1.toString();
    }

    private void renderFixedPoint(StringBuffer p6, java.text.NumberFormat p7, long p8, long p10)
    {
        p6.append((p8 / p10));
        String v0_1 = (p8 % p10);
        if (v0_1 != 0) {
            p6.append(".");
            p6.append(p7.format(v0_1));
        }
        return;
    }

    private int toLOCformat(long p6)
    {
        int v0_0 = 0;
        while (p6 > 9) {
            v0_0 = ((byte) (v0_0 + 1));
            p6 /= 10;
        }
        return ((int) (((long) v0_0) + (p6 << 4)));
    }

    public double getAltitude()
    {
        return (((double) (this.altitude - 10000000)) / 100.0);
    }

    public double getHPrecision()
    {
        return (((double) this.hPrecision) / 100.0);
    }

    public double getLatitude()
    {
        return (((double) (this.latitude - 1.0609978955e-314)) / 3600000.0);
    }

    public double getLongitude()
    {
        return (((double) (this.longitude - 1.0609978955e-314)) / 3600000.0);
    }

    org.xbill.DNS.Record getObject()
    {
        return new org.xbill.DNS.LOCRecord();
    }

    public double getSize()
    {
        return (((double) this.size) / 100.0);
    }

    public double getVPrecision()
    {
        return (((double) this.vPrecision) / 100.0);
    }

    void rdataFromString(org.xbill.DNS.Tokenizer p11, org.xbill.DNS.Name p12)
    {
        this.latitude = this.parsePosition(p11, "latitude");
        this.longitude = this.parsePosition(p11, "longitude");
        this.altitude = (this.parseDouble(p11, "altitude", 1, -10000000, 2.117055134e-314, 0) + 10000000);
        this.size = this.parseDouble(p11, "size", 0, 0, 4.4465908126e-314, 100);
        this.hPrecision = this.parseDouble(p11, "horizontal precision", 0, 0, 4.4465908126e-314, 1000000);
        this.vPrecision = this.parseDouble(p11, "vertical precision", 0, 0, 4.4465908126e-314, 1000);
        return;
    }

    void rrFromWire(org.xbill.DNS.DNSInput p3)
    {
        if (p3.readU8() == 0) {
            this.size = org.xbill.DNS.LOCRecord.parseLOCformat(p3.readU8());
            this.hPrecision = org.xbill.DNS.LOCRecord.parseLOCformat(p3.readU8());
            this.vPrecision = org.xbill.DNS.LOCRecord.parseLOCformat(p3.readU8());
            this.latitude = p3.readU32();
            this.longitude = p3.readU32();
            this.altitude = p3.readU32();
            return;
        } else {
            throw new org.xbill.DNS.WireParseException("Invalid LOC version");
        }
    }

    String rrToString()
    {
        StringBuffer v2_1 = new StringBuffer();
        v2_1.append(this.positionToString(this.latitude, 78, 83));
        v2_1.append(" ");
        v2_1.append(this.positionToString(this.longitude, 69, 87));
        v2_1.append(" ");
        this.renderFixedPoint(v2_1, org.xbill.DNS.LOCRecord.w2, (this.altitude - 10000000), 100);
        v2_1.append("m ");
        this.renderFixedPoint(v2_1, org.xbill.DNS.LOCRecord.w2, this.size, 100);
        v2_1.append("m ");
        this.renderFixedPoint(v2_1, org.xbill.DNS.LOCRecord.w2, this.hPrecision, 100);
        v2_1.append("m ");
        this.renderFixedPoint(v2_1, org.xbill.DNS.LOCRecord.w2, this.vPrecision, 100);
        v2_1.append("m");
        return v2_1.toString();
    }

    void rrToWire(org.xbill.DNS.DNSOutput p3, org.xbill.DNS.Compression p4, boolean p5)
    {
        p3.writeU8(0);
        p3.writeU8(this.toLOCformat(this.size));
        p3.writeU8(this.toLOCformat(this.hPrecision));
        p3.writeU8(this.toLOCformat(this.vPrecision));
        p3.writeU32(this.latitude);
        p3.writeU32(this.longitude);
        p3.writeU32(this.altitude);
        return;
    }
}
