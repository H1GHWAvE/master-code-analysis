package org.xbill.DNS;
public class ResolverConfig {
    static Class class$java$lang$String;
    static Class class$org$xbill$DNS$ResolverConfig;
    private static org.xbill.DNS.ResolverConfig currentConfig;
    private int ndots;
    private org.xbill.DNS.Name[] searchlist;
    private String[] servers;

    static ResolverConfig()
    {
        org.xbill.DNS.ResolverConfig.refresh();
        return;
    }

    public ResolverConfig()
    {
        this.servers = 0;
        this.searchlist = 0;
        this.ndots = -1;
        if ((!this.findProperty()) && ((!this.findSunJVM()) && ((this.servers == null) || (this.searchlist == null)))) {
            int v0_6 = System.getProperty("os.name");
            String v1_1 = System.getProperty("java.vendor");
            if (v0_6.indexOf("Windows") == -1) {
                if (v0_6.indexOf("NetWare") == -1) {
                    if (v1_1.indexOf("Android") == -1) {
                        this.findUnix();
                    } else {
                        this.findAndroid();
                    }
                } else {
                    this.findNetware();
                }
            } else {
                if ((v0_6.indexOf("95") == -1) && ((v0_6.indexOf("98") == -1) && (v0_6.indexOf("ME") == -1))) {
                    this.findNT();
                } else {
                    this.find95();
                }
            }
        }
        return;
    }

    private void addSearch(String p4, java.util.List p5)
    {
        if (org.xbill.DNS.Options.check("verbose")) {
            System.out.println(new StringBuffer("adding search ").append(p4).toString());
        }
        try {
            org.xbill.DNS.TextParseException v0_4 = org.xbill.DNS.Name.fromString(p4, org.xbill.DNS.Name.root);
        } catch (org.xbill.DNS.TextParseException v0) {
            return;
        }
        if (!p5.contains(v0_4)) {
            p5.add(v0_4);
            return;
        } else {
            return;
        }
    }

    private void addServer(String p4, java.util.List p5)
    {
        if (!p5.contains(p4)) {
            if (org.xbill.DNS.Options.check("verbose")) {
                System.out.println(new StringBuffer("adding server ").append(p4).toString());
            }
            p5.add(p4);
        }
        return;
    }

    static Class class$(String p2)
    {
        try {
            return Class.forName(p2);
        } catch (Throwable v0_1) {
            throw new NoClassDefFoundError().initCause(v0_1);
        }
    }

    private void configureFromLists(java.util.List p3, java.util.List p4)
    {
        if ((this.servers == null) && (p3.size() > 0)) {
            org.xbill.DNS.Name[] v0_2 = new String[0];
            this.servers = ((String[]) ((String[]) p3.toArray(v0_2)));
        }
        if ((this.searchlist == null) && (p4.size() > 0)) {
            org.xbill.DNS.Name[] v0_8 = new org.xbill.DNS.Name[0];
            this.searchlist = ((org.xbill.DNS.Name[]) ((org.xbill.DNS.Name[]) p4.toArray(v0_8)));
        }
        return;
    }

    private void configureNdots(int p2)
    {
        if ((this.ndots < 0) && (p2 > 0)) {
            this.ndots = p2;
        }
        return;
    }

    private void find95()
    {
        try {
            Runtime.getRuntime().exec(new StringBuffer("winipcfg /all /batch ").append("winipcfg.out").toString()).waitFor();
            this.findWin(new java.io.FileInputStream(new java.io.File("winipcfg.out")));
            new java.io.File("winipcfg.out").delete();
        } catch (Exception v0) {
        }
        return;
    }

    private void findAndroid()
    {
        java.util.ArrayList v2_1 = new java.util.ArrayList();
        java.util.ArrayList v3_1 = new java.util.ArrayList();
        try {
            int v1_3;
            reflect.Method v4_0 = Class.forName("android.os.SystemProperties");
            boolean v6_0 = new Class[1];
        } catch (int v0) {
            this.configureFromLists(v2_1, v3_1);
            return;
        }
        if (org.xbill.DNS.ResolverConfig.class$java$lang$String != null) {
            v1_3 = org.xbill.DNS.ResolverConfig.class$java$lang$String;
        } else {
            v1_3 = org.xbill.DNS.ResolverConfig.class$("java.lang.String");
            org.xbill.DNS.ResolverConfig.class$java$lang$String = v1_3;
        }
        v6_0[0] = v1_3;
        reflect.Method v4_1 = v4_0.getMethod("get", v6_0);
        String[] v5_1 = new String[4];
        v5_1[0] = "net.dns1";
        v5_1[1] = "net.dns2";
        v5_1[2] = "net.dns3";
        v5_1[3] = "net.dns4";
        int v1_10 = 0;
        while (v1_10 < 4) {
            int v0_2 = new Object[1];
            v0_2[0] = v5_1[v1_10];
            int v0_4 = ((String) v4_1.invoke(0, v0_2));
            if (((v0_4 != 0) && ((v0_4.matches("^\\d+(\\.\\d+){3}$")) || (v0_4.matches("^[0-9a-f]+(:[0-9a-f]*)+:[0-9a-f]+$")))) && (!v2_1.contains(v0_4))) {
                v2_1.add(v0_4);
            }
            v1_10++;
        }
        this.configureFromLists(v2_1, v3_1);
        return;
    }

    private void findNT()
    {
        try {
            Exception v0_1 = Runtime.getRuntime().exec("ipconfig /all");
            this.findWin(v0_1.getInputStream());
            v0_1.destroy();
        } catch (Exception v0) {
        }
        return;
    }

    private void findNetware()
    {
        this.findResolvConf("sys:/etc/resolv.cfg");
        return;
    }

    private boolean findProperty()
    {
        int v0 = 0;
        org.xbill.DNS.Name[] v1_1 = new java.util.ArrayList(0);
        java.util.ArrayList v2_1 = new java.util.ArrayList(0);
        String v3_1 = System.getProperty("dns.server");
        if (v3_1 != null) {
            java.util.StringTokenizer v4_1 = new java.util.StringTokenizer(v3_1, ",");
            while (v4_1.hasMoreTokens()) {
                this.addServer(v4_1.nextToken(), v1_1);
            }
        }
        String v3_4 = System.getProperty("dns.search");
        if (v3_4 != null) {
            java.util.StringTokenizer v4_3 = new java.util.StringTokenizer(v3_4, ",");
            while (v4_3.hasMoreTokens()) {
                this.addSearch(v4_3.nextToken(), v2_1);
            }
        }
        this.configureFromLists(v1_1, v2_1);
        if ((this.servers != null) && (this.searchlist != null)) {
            v0 = 1;
        }
        return v0;
    }

    private void findResolvConf(String p8)
    {
        try {
            java.io.IOException v2_1 = new java.io.BufferedReader(new java.io.InputStreamReader(new java.io.FileInputStream(p8)));
            java.util.ArrayList v1_3 = new java.util.ArrayList(0);
            java.util.ArrayList v3_1 = new java.util.ArrayList(0);
            int v0_2 = -1;
        } catch (int v0) {
            return;
        }
        try {
            do {
                String v4_1 = v2_1.readLine();
            } while(!java.util.StringTokenizer v5_12.hasMoreTokens());
            this.addSearch(v5_12.nextToken(), v3_1);
            while (v5_12.hasMoreTokens()) {
            }
        } catch (java.io.IOException v2) {
            this.configureFromLists(v1_3, v3_1);
            this.configureNdots(v0_2);
            return;
        }
        if (v4_1 == null) {
            v2_1.close();
        } else {
            if (!v4_1.startsWith("nameserver")) {
                if (!v4_1.startsWith("domain")) {
                    if (!v4_1.startsWith("search")) {
                        if (!v4_1.startsWith("options")) {
                        } else {
                            java.util.StringTokenizer v5_9 = new java.util.StringTokenizer(v4_1);
                            v5_9.nextToken();
                            while (v5_9.hasMoreTokens()) {
                                String v4_3 = v5_9.nextToken();
                                if (v4_3.startsWith("ndots:")) {
                                    v0_2 = this.parseNdots(v4_3);
                                }
                            }
                        }
                    } else {
                        if (!v3_1.isEmpty()) {
                            v3_1.clear();
                        }
                        v5_12 = new java.util.StringTokenizer(v4_1);
                        v5_12.nextToken();
                    }
                } else {
                    java.util.StringTokenizer v5_14 = new java.util.StringTokenizer(v4_1);
                    v5_14.nextToken();
                    if ((!v5_14.hasMoreTokens()) || (!v3_1.isEmpty())) {
                    } else {
                        this.addSearch(v5_14.nextToken(), v3_1);
                    }
                }
            } else {
                java.util.StringTokenizer v5_16 = new java.util.StringTokenizer(v4_1);
                v5_16.nextToken();
                this.addServer(v5_16.nextToken(), v1_3);
            }
        }
    }

    private boolean findSunJVM()
    {
        java.util.ArrayList v3_1 = new java.util.ArrayList(0);
        java.util.ArrayList v4_1 = new java.util.ArrayList(0);
        try {
            String v0_9;
            java.util.Iterator v1_0 = new Class[0];
            int v5_0 = new Object[0];
            Class v6 = Class.forName("sun.net.dns.ResolverConfiguration");
            Object v7_1 = v6.getDeclaredMethod("open", v1_0).invoke(0, v5_0);
            String v0_8 = ((java.util.List) v6.getMethod("nameservers", v1_0).invoke(v7_1, v5_0));
            java.util.Iterator v1_3 = ((java.util.List) v6.getMethod("searchlist", v1_0).invoke(v7_1, v5_0));
        } catch (String v0) {
            v0_9 = 0;
            return v0_9;
        }
        if (v0_8.size() != 0) {
            if (v0_8.size() > 0) {
                java.util.Iterator v2_2 = v0_8.iterator();
                while (v2_2.hasNext()) {
                    this.addServer(((String) v2_2.next()), v3_1);
                }
            }
            if (v1_3.size() > 0) {
                java.util.Iterator v1_4 = v1_3.iterator();
                while (v1_4.hasNext()) {
                    this.addSearch(((String) v1_4.next()), v4_1);
                }
            }
            this.configureFromLists(v3_1, v4_1);
            v0_9 = 1;
            return v0_9;
        } else {
            v0_9 = 0;
            return v0_9;
        }
    }

    private void findUnix()
    {
        this.findResolvConf("/etc/resolv.conf");
        return;
    }

    private void findWin(java.io.InputStream p5)
    {
        java.io.IOException v0_2 = Integer.getInteger("org.xbill.DNS.windows.parse.buffer", 8192).intValue();
        java.io.BufferedInputStream v1_2 = new java.io.BufferedInputStream(p5, v0_2);
        v1_2.mark(v0_2);
        this.findWin(v1_2, 0);
        if (this.servers == null) {
            try {
                v1_2.reset();
                this.findWin(v1_2, new java.util.Locale("", ""));
            } catch (java.io.IOException v0) {
            }
        }
        return;
    }

    private void findWin(java.io.InputStream p17, java.util.Locale p18)
    {
        int v1_1;
        if (org.xbill.DNS.ResolverConfig.class$org$xbill$DNS$ResolverConfig != null) {
            v1_1 = org.xbill.DNS.ResolverConfig.class$org$xbill$DNS$ResolverConfig;
        } else {
            v1_1 = org.xbill.DNS.ResolverConfig.class$("org.xbill.DNS.ResolverConfig");
            org.xbill.DNS.ResolverConfig.class$org$xbill$DNS$ResolverConfig = v1_1;
        }
        int v1_8;
        int v1_7 = new StringBuffer().append(v1_1.getPackage().getName()).append(".windows.DNSServer").toString();
        if (p18 == null) {
            v1_8 = java.util.ResourceBundle.getBundle(v1_7);
        } else {
            v1_8 = java.util.ResourceBundle.getBundle(v1_7, p18);
        }
        String v4 = v1_8.getString("host_name");
        String v5 = v1_8.getString("primary_dns_suffix");
        String v6 = v1_8.getString("dns_suffix");
        String v7 = v1_8.getString("dns_servers");
        java.io.BufferedReader v8_1 = new java.io.BufferedReader(new java.io.InputStreamReader(p17));
        try {
            java.util.ArrayList v9_1 = new java.util.ArrayList();
            java.util.ArrayList v10_1 = new java.util.ArrayList();
            int v3_1 = 0;
            int v1_12 = 0;
        } catch (int v1) {
            return;
        }
        do {
            boolean v11_0 = v8_1.readLine();
            if (!v11_0) {
                this.configureFromLists(v9_1, v10_1);
                return;
            } else {
                int v12_1 = new java.util.StringTokenizer(v11_0);
                if (v12_1.hasMoreTokens()) {
                    int v1_13;
                    int v3_2;
                    String v2_8 = v12_1.nextToken();
                    if (v11_0.indexOf(":") == -1) {
                        v1_13 = v3_1;
                        v3_2 = v1_12;
                    } else {
                        v1_13 = 0;
                        v3_2 = 0;
                    }
                    if (v11_0.indexOf(v4) == -1) {
                        if (v11_0.indexOf(v5) == -1) {
                            if ((v3_2 == 0) && (v11_0.indexOf(v6) == -1)) {
                                if ((v1_13 != 0) || (v11_0.indexOf(v7) != -1)) {
                                    while (v12_1.hasMoreTokens()) {
                                        v2_8 = v12_1.nextToken();
                                    }
                                    if (v2_8.equals(":")) {
                                        break;
                                    }
                                    this.addServer(v2_8, v9_1);
                                    v1_13 = 1;
                                }
                                v3_1 = v1_13;
                                v1_12 = v3_2;
                            }
                            while (v12_1.hasMoreTokens()) {
                                v2_8 = v12_1.nextToken();
                            }
                            if (v2_8.equals(":")) {
                                break;
                            }
                            this.addSearch(v2_8, v10_1);
                            v3_1 = v1_13;
                            v1_12 = 1;
                        }
                        while (v12_1.hasMoreTokens()) {
                            v2_8 = v12_1.nextToken();
                        }
                        if (v2_8.equals(":")) {
                            break;
                        }
                        this.addSearch(v2_8, v10_1);
                        v3_1 = v1_13;
                        v1_12 = 1;
                    } else {
                        while (v12_1.hasMoreTokens()) {
                            v2_8 = v12_1.nextToken();
                        }
                    }
                } else {
                    v3_1 = 0;
                    v1_12 = 0;
                }
            }
        } while(org.xbill.DNS.Name.fromString(v2_8, 0).labels() == 1);
        v3_1 = v1_13;
        v1_12 = v3_2;
    }

    public static declared_synchronized org.xbill.DNS.ResolverConfig getCurrentConfig()
    {
        try {
            return org.xbill.DNS.ResolverConfig.currentConfig;
        } catch (Throwable v1_1) {
            throw v1_1;
        }
    }

    private int parseNdots(String p6)
    {
        String v1_0 = p6.substring(6);
        try {
            int v0_1 = Integer.parseInt(v1_0);
        } catch (int v0) {
            v0_1 = -1;
            return v0_1;
        }
        if (v0_1 < 0) {
        } else {
            if (!org.xbill.DNS.Options.check("verbose")) {
                return v0_1;
            } else {
                System.out.println(new StringBuffer("setting ndots ").append(v1_0).toString());
                return v0_1;
            }
        }
    }

    public static void refresh()
    {
        Throwable v1_1 = new org.xbill.DNS.ResolverConfig();
        if (org.xbill.DNS.ResolverConfig.class$org$xbill$DNS$ResolverConfig == null) {
            org.xbill.DNS.ResolverConfig.class$org$xbill$DNS$ResolverConfig = org.xbill.DNS.ResolverConfig.class$("org.xbill.DNS.ResolverConfig");
        }
        org.xbill.DNS.ResolverConfig.currentConfig = v1_1;
        return;
    }

    public int ndots()
    {
        int v0_1;
        if (this.ndots >= 0) {
            v0_1 = this.ndots;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    public org.xbill.DNS.Name[] searchPath()
    {
        return this.searchlist;
    }

    public String server()
    {
        String v0_2;
        if (this.servers != null) {
            v0_2 = this.servers[0];
        } else {
            v0_2 = 0;
        }
        return v0_2;
    }

    public String[] servers()
    {
        return this.servers;
    }
}
