package org.xbill.DNS;
public class PXRecord extends org.xbill.DNS.Record {
    private static final long serialVersionUID = 1811540008806660667;
    private org.xbill.DNS.Name map822;
    private org.xbill.DNS.Name mapX400;
    private int preference;

    PXRecord()
    {
        return;
    }

    public PXRecord(org.xbill.DNS.Name p8, int p9, long p10, int p12, org.xbill.DNS.Name p13, org.xbill.DNS.Name p14)
    {
        this(p8, 26, p9, p10);
        this.preference = org.xbill.DNS.PXRecord.checkU16("preference", p12);
        this.map822 = org.xbill.DNS.PXRecord.checkName("map822", p13);
        this.mapX400 = org.xbill.DNS.PXRecord.checkName("mapX400", p14);
        return;
    }

    public org.xbill.DNS.Name getMap822()
    {
        return this.map822;
    }

    public org.xbill.DNS.Name getMapX400()
    {
        return this.mapX400;
    }

    org.xbill.DNS.Record getObject()
    {
        return new org.xbill.DNS.PXRecord();
    }

    public int getPreference()
    {
        return this.preference;
    }

    void rdataFromString(org.xbill.DNS.Tokenizer p2, org.xbill.DNS.Name p3)
    {
        this.preference = p2.getUInt16();
        this.map822 = p2.getName(p3);
        this.mapX400 = p2.getName(p3);
        return;
    }

    void rrFromWire(org.xbill.DNS.DNSInput p2)
    {
        this.preference = p2.readU16();
        this.map822 = new org.xbill.DNS.Name(p2);
        this.mapX400 = new org.xbill.DNS.Name(p2);
        return;
    }

    String rrToString()
    {
        String v0_1 = new StringBuffer();
        v0_1.append(this.preference);
        v0_1.append(" ");
        v0_1.append(this.map822);
        v0_1.append(" ");
        v0_1.append(this.mapX400);
        return v0_1.toString();
    }

    void rrToWire(org.xbill.DNS.DNSOutput p3, org.xbill.DNS.Compression p4, boolean p5)
    {
        p3.writeU16(this.preference);
        this.map822.toWire(p3, 0, p5);
        this.mapX400.toWire(p3, 0, p5);
        return;
    }
}
