package org.xbill.DNS;
public class InvalidTTLException extends java.lang.IllegalArgumentException {

    public InvalidTTLException(long p4)
    {
        this(new StringBuffer("Invalid DNS TTL: ").append(p4).toString());
        return;
    }
}
