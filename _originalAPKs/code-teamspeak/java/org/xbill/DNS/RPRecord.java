package org.xbill.DNS;
public class RPRecord extends org.xbill.DNS.Record {
    private static final long serialVersionUID = 8124584364211337460;
    private org.xbill.DNS.Name mailbox;
    private org.xbill.DNS.Name textDomain;

    RPRecord()
    {
        return;
    }

    public RPRecord(org.xbill.DNS.Name p8, int p9, long p10, org.xbill.DNS.Name p12, org.xbill.DNS.Name p13)
    {
        this(p8, 17, p9, p10);
        this.mailbox = org.xbill.DNS.RPRecord.checkName("mailbox", p12);
        this.textDomain = org.xbill.DNS.RPRecord.checkName("textDomain", p13);
        return;
    }

    public org.xbill.DNS.Name getMailbox()
    {
        return this.mailbox;
    }

    org.xbill.DNS.Record getObject()
    {
        return new org.xbill.DNS.RPRecord();
    }

    public org.xbill.DNS.Name getTextDomain()
    {
        return this.textDomain;
    }

    void rdataFromString(org.xbill.DNS.Tokenizer p2, org.xbill.DNS.Name p3)
    {
        this.mailbox = p2.getName(p3);
        this.textDomain = p2.getName(p3);
        return;
    }

    void rrFromWire(org.xbill.DNS.DNSInput p2)
    {
        this.mailbox = new org.xbill.DNS.Name(p2);
        this.textDomain = new org.xbill.DNS.Name(p2);
        return;
    }

    String rrToString()
    {
        String v0_1 = new StringBuffer();
        v0_1.append(this.mailbox);
        v0_1.append(" ");
        v0_1.append(this.textDomain);
        return v0_1.toString();
    }

    void rrToWire(org.xbill.DNS.DNSOutput p3, org.xbill.DNS.Compression p4, boolean p5)
    {
        this.mailbox.toWire(p3, 0, p5);
        this.textDomain.toWire(p3, 0, p5);
        return;
    }
}
