package org.xbill.DNS;
abstract class SingleNameBase extends org.xbill.DNS.Record {
    private static final long serialVersionUID = 262879934209243;
    protected org.xbill.DNS.Name singleName;

    protected SingleNameBase()
    {
        return;
    }

    protected SingleNameBase(org.xbill.DNS.Name p1, int p2, int p3, long p4)
    {
        this(p1, p2, p3, p4);
        return;
    }

    protected SingleNameBase(org.xbill.DNS.Name p3, int p4, int p5, long p6, org.xbill.DNS.Name p8, String p9)
    {
        this(p3, p4, p5, p6).singleName = org.xbill.DNS.SingleNameBase.checkName(p9, p8);
        return;
    }

    protected org.xbill.DNS.Name getSingleName()
    {
        return this.singleName;
    }

    void rdataFromString(org.xbill.DNS.Tokenizer p2, org.xbill.DNS.Name p3)
    {
        this.singleName = p2.getName(p3);
        return;
    }

    void rrFromWire(org.xbill.DNS.DNSInput p2)
    {
        this.singleName = new org.xbill.DNS.Name(p2);
        return;
    }

    String rrToString()
    {
        return this.singleName.toString();
    }

    void rrToWire(org.xbill.DNS.DNSOutput p3, org.xbill.DNS.Compression p4, boolean p5)
    {
        this.singleName.toWire(p3, 0, p5);
        return;
    }
}
