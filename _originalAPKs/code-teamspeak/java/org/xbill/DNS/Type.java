package org.xbill.DNS;
public final class Type {
    public static final int A = 1;
    public static final int A6 = 38;
    public static final int AAAA = 28;
    public static final int AFSDB = 18;
    public static final int ANY = 255;
    public static final int APL = 42;
    public static final int ATMA = 34;
    public static final int AXFR = 252;
    public static final int CERT = 37;
    public static final int CNAME = 5;
    public static final int DHCID = 49;
    public static final int DLV = 32769;
    public static final int DNAME = 39;
    public static final int DNSKEY = 48;
    public static final int DS = 43;
    public static final int EID = 31;
    public static final int GPOS = 27;
    public static final int HINFO = 13;
    public static final int IPSECKEY = 45;
    public static final int ISDN = 20;
    public static final int IXFR = 251;
    public static final int KEY = 25;
    public static final int KX = 36;
    public static final int LOC = 29;
    public static final int MAILA = 254;
    public static final int MAILB = 253;
    public static final int MB = 7;
    public static final int MD = 3;
    public static final int MF = 4;
    public static final int MG = 8;
    public static final int MINFO = 14;
    public static final int MR = 9;
    public static final int MX = 15;
    public static final int NAPTR = 35;
    public static final int NIMLOC = 32;
    public static final int NS = 2;
    public static final int NSAP = 22;
    public static final int NSAP_PTR = 23;
    public static final int NSEC = 47;
    public static final int NSEC3 = 50;
    public static final int NSEC3PARAM = 51;
    public static final int NULL = 10;
    public static final int NXT = 30;
    public static final int OPT = 41;
    public static final int PTR = 12;
    public static final int PX = 26;
    public static final int RP = 17;
    public static final int RRSIG = 46;
    public static final int RT = 21;
    public static final int SIG = 24;
    public static final int SOA = 6;
    public static final int SPF = 99;
    public static final int SRV = 33;
    public static final int SSHFP = 44;
    public static final int TKEY = 249;
    public static final int TLSA = 52;
    public static final int TSIG = 250;
    public static final int TXT = 16;
    public static final int URI = 256;
    public static final int WKS = 11;
    public static final int X25 = 19;
    private static org.xbill.DNS.Type$TypeMnemonic types;

    static Type()
    {
        org.xbill.DNS.Type$TypeMnemonic v0_1 = new org.xbill.DNS.Type$TypeMnemonic();
        org.xbill.DNS.Type.types = v0_1;
        v0_1.add(1, "A", new org.xbill.DNS.ARecord());
        org.xbill.DNS.Type.types.add(2, "NS", new org.xbill.DNS.NSRecord());
        org.xbill.DNS.Type.types.add(3, "MD", new org.xbill.DNS.MDRecord());
        org.xbill.DNS.Type.types.add(4, "MF", new org.xbill.DNS.MFRecord());
        org.xbill.DNS.Type.types.add(5, "CNAME", new org.xbill.DNS.CNAMERecord());
        org.xbill.DNS.Type.types.add(6, "SOA", new org.xbill.DNS.SOARecord());
        org.xbill.DNS.Type.types.add(7, "MB", new org.xbill.DNS.MBRecord());
        org.xbill.DNS.Type.types.add(8, "MG", new org.xbill.DNS.MGRecord());
        org.xbill.DNS.Type.types.add(9, "MR", new org.xbill.DNS.MRRecord());
        org.xbill.DNS.Type.types.add(10, "NULL", new org.xbill.DNS.NULLRecord());
        org.xbill.DNS.Type.types.add(11, "WKS", new org.xbill.DNS.WKSRecord());
        org.xbill.DNS.Type.types.add(12, "PTR", new org.xbill.DNS.PTRRecord());
        org.xbill.DNS.Type.types.add(13, "HINFO", new org.xbill.DNS.HINFORecord());
        org.xbill.DNS.Type.types.add(14, "MINFO", new org.xbill.DNS.MINFORecord());
        org.xbill.DNS.Type.types.add(15, "MX", new org.xbill.DNS.MXRecord());
        org.xbill.DNS.Type.types.add(16, "TXT", new org.xbill.DNS.TXTRecord());
        org.xbill.DNS.Type.types.add(17, "RP", new org.xbill.DNS.RPRecord());
        org.xbill.DNS.Type.types.add(18, "AFSDB", new org.xbill.DNS.AFSDBRecord());
        org.xbill.DNS.Type.types.add(19, "X25", new org.xbill.DNS.X25Record());
        org.xbill.DNS.Type.types.add(20, "ISDN", new org.xbill.DNS.ISDNRecord());
        org.xbill.DNS.Type.types.add(21, "RT", new org.xbill.DNS.RTRecord());
        org.xbill.DNS.Type.types.add(22, "NSAP", new org.xbill.DNS.NSAPRecord());
        org.xbill.DNS.Type.types.add(23, "NSAP-PTR", new org.xbill.DNS.NSAP_PTRRecord());
        org.xbill.DNS.Type.types.add(24, "SIG", new org.xbill.DNS.SIGRecord());
        org.xbill.DNS.Type.types.add(25, "KEY", new org.xbill.DNS.KEYRecord());
        org.xbill.DNS.Type.types.add(26, "PX", new org.xbill.DNS.PXRecord());
        org.xbill.DNS.Type.types.add(27, "GPOS", new org.xbill.DNS.GPOSRecord());
        org.xbill.DNS.Type.types.add(28, "AAAA", new org.xbill.DNS.AAAARecord());
        org.xbill.DNS.Type.types.add(29, "LOC", new org.xbill.DNS.LOCRecord());
        org.xbill.DNS.Type.types.add(30, "NXT", new org.xbill.DNS.NXTRecord());
        org.xbill.DNS.Type.types.add(31, "EID");
        org.xbill.DNS.Type.types.add(32, "NIMLOC");
        org.xbill.DNS.Type.types.add(33, "SRV", new org.xbill.DNS.SRVRecord());
        org.xbill.DNS.Type.types.add(34, "ATMA");
        org.xbill.DNS.Type.types.add(35, "NAPTR", new org.xbill.DNS.NAPTRRecord());
        org.xbill.DNS.Type.types.add(36, "KX", new org.xbill.DNS.KXRecord());
        org.xbill.DNS.Type.types.add(37, "CERT", new org.xbill.DNS.CERTRecord());
        org.xbill.DNS.Type.types.add(38, "A6", new org.xbill.DNS.A6Record());
        org.xbill.DNS.Type.types.add(39, "DNAME", new org.xbill.DNS.DNAMERecord());
        org.xbill.DNS.Type.types.add(41, "OPT", new org.xbill.DNS.OPTRecord());
        org.xbill.DNS.Type.types.add(42, "APL", new org.xbill.DNS.APLRecord());
        org.xbill.DNS.Type.types.add(43, "DS", new org.xbill.DNS.DSRecord());
        org.xbill.DNS.Type.types.add(44, "SSHFP", new org.xbill.DNS.SSHFPRecord());
        org.xbill.DNS.Type.types.add(45, "IPSECKEY", new org.xbill.DNS.IPSECKEYRecord());
        org.xbill.DNS.Type.types.add(46, "RRSIG", new org.xbill.DNS.RRSIGRecord());
        org.xbill.DNS.Type.types.add(47, "NSEC", new org.xbill.DNS.NSECRecord());
        org.xbill.DNS.Type.types.add(48, "DNSKEY", new org.xbill.DNS.DNSKEYRecord());
        org.xbill.DNS.Type.types.add(49, "DHCID", new org.xbill.DNS.DHCIDRecord());
        org.xbill.DNS.Type.types.add(50, "NSEC3", new org.xbill.DNS.NSEC3Record());
        org.xbill.DNS.Type.types.add(51, "NSEC3PARAM", new org.xbill.DNS.NSEC3PARAMRecord());
        org.xbill.DNS.Type.types.add(52, "TLSA", new org.xbill.DNS.TLSARecord());
        org.xbill.DNS.Type.types.add(99, "SPF", new org.xbill.DNS.SPFRecord());
        org.xbill.DNS.Type.types.add(249, "TKEY", new org.xbill.DNS.TKEYRecord());
        org.xbill.DNS.Type.types.add(250, "TSIG", new org.xbill.DNS.TSIGRecord());
        org.xbill.DNS.Type.types.add(251, "IXFR");
        org.xbill.DNS.Type.types.add(252, "AXFR");
        org.xbill.DNS.Type.types.add(253, "MAILB");
        org.xbill.DNS.Type.types.add(254, "MAILA");
        org.xbill.DNS.Type.types.add(255, "ANY");
        org.xbill.DNS.Type.types.add(256, "URI", new org.xbill.DNS.URIRecord());
        org.xbill.DNS.Type.types.add(32769, "DLV", new org.xbill.DNS.DLVRecord());
        return;
    }

    private Type()
    {
        return;
    }

    public static void check(int p1)
    {
        if ((p1 >= 0) && (p1 <= 65535)) {
            return;
        } else {
            throw new org.xbill.DNS.InvalidTypeException(p1);
        }
    }

    static org.xbill.DNS.Record getProto(int p1)
    {
        return org.xbill.DNS.Type.types.getProto(p1);
    }

    public static boolean isRR(int p1)
    {
        int v0;
        switch (p1) {
            case 41:
            case 249:
            case 250:
            case 251:
            case 252:
            case 253:
            case 254:
            case 255:
                v0 = 0;
                break;
            default:
                v0 = 1;
        }
        return v0;
    }

    public static String string(int p1)
    {
        return org.xbill.DNS.Type.types.getText(p1);
    }

    public static int value(String p1)
    {
        return org.xbill.DNS.Type.value(p1, 0);
    }

    public static int value(String p3, boolean p4)
    {
        int v0_1 = org.xbill.DNS.Type.types.getValue(p3);
        if ((v0_1 == -1) && (p4)) {
            v0_1 = org.xbill.DNS.Type.types.getValue(new StringBuffer("TYPE").append(p3).toString());
        }
        return v0_1;
    }
}
