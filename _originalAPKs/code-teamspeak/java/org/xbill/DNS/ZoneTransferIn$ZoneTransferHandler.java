package org.xbill.DNS;
public interface ZoneTransferIn$ZoneTransferHandler {

    public abstract void handleRecord();

    public abstract void startAXFR();

    public abstract void startIXFR();

    public abstract void startIXFRAdds();

    public abstract void startIXFRDeletes();
}
