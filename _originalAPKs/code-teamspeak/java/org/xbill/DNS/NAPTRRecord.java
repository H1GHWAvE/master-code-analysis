package org.xbill.DNS;
public class NAPTRRecord extends org.xbill.DNS.Record {
    private static final long serialVersionUID = 5191232392044947002;
    private byte[] flags;
    private int order;
    private int preference;
    private byte[] regexp;
    private org.xbill.DNS.Name replacement;
    private byte[] service;

    NAPTRRecord()
    {
        return;
    }

    public NAPTRRecord(org.xbill.DNS.Name p10, int p11, long p12, int p14, int p15, String p16, String p17, String p18, org.xbill.DNS.Name p19)
    {
        this(p10, 35, p11, p12);
        this.order = org.xbill.DNS.NAPTRRecord.checkU16("order", p14);
        this.preference = org.xbill.DNS.NAPTRRecord.checkU16("preference", p15);
        try {
            this.flags = org.xbill.DNS.NAPTRRecord.byteArrayFromString(p16);
            this.service = org.xbill.DNS.NAPTRRecord.byteArrayFromString(p17);
            this.regexp = org.xbill.DNS.NAPTRRecord.byteArrayFromString(p18);
            this.replacement = org.xbill.DNS.NAPTRRecord.checkName("replacement", p19);
            return;
        } catch (String v2_10) {
            throw new IllegalArgumentException(v2_10.getMessage());
        }
    }

    public org.xbill.DNS.Name getAdditionalName()
    {
        return this.replacement;
    }

    public String getFlags()
    {
        return org.xbill.DNS.NAPTRRecord.byteArrayToString(this.flags, 0);
    }

    org.xbill.DNS.Record getObject()
    {
        return new org.xbill.DNS.NAPTRRecord();
    }

    public int getOrder()
    {
        return this.order;
    }

    public int getPreference()
    {
        return this.preference;
    }

    public String getRegexp()
    {
        return org.xbill.DNS.NAPTRRecord.byteArrayToString(this.regexp, 0);
    }

    public org.xbill.DNS.Name getReplacement()
    {
        return this.replacement;
    }

    public String getService()
    {
        return org.xbill.DNS.NAPTRRecord.byteArrayToString(this.service, 0);
    }

    void rdataFromString(org.xbill.DNS.Tokenizer p2, org.xbill.DNS.Name p3)
    {
        this.order = p2.getUInt16();
        this.preference = p2.getUInt16();
        try {
            this.flags = org.xbill.DNS.NAPTRRecord.byteArrayFromString(p2.getString());
            this.service = org.xbill.DNS.NAPTRRecord.byteArrayFromString(p2.getString());
            this.regexp = org.xbill.DNS.NAPTRRecord.byteArrayFromString(p2.getString());
            this.replacement = p2.getName(p3);
            return;
        } catch (org.xbill.DNS.TextParseException v0_9) {
            throw p2.exception(v0_9.getMessage());
        }
    }

    void rrFromWire(org.xbill.DNS.DNSInput p2)
    {
        this.order = p2.readU16();
        this.preference = p2.readU16();
        this.flags = p2.readCountedString();
        this.service = p2.readCountedString();
        this.regexp = p2.readCountedString();
        this.replacement = new org.xbill.DNS.Name(p2);
        return;
    }

    String rrToString()
    {
        String v0_1 = new StringBuffer();
        v0_1.append(this.order);
        v0_1.append(" ");
        v0_1.append(this.preference);
        v0_1.append(" ");
        v0_1.append(org.xbill.DNS.NAPTRRecord.byteArrayToString(this.flags, 1));
        v0_1.append(" ");
        v0_1.append(org.xbill.DNS.NAPTRRecord.byteArrayToString(this.service, 1));
        v0_1.append(" ");
        v0_1.append(org.xbill.DNS.NAPTRRecord.byteArrayToString(this.regexp, 1));
        v0_1.append(" ");
        v0_1.append(this.replacement);
        return v0_1.toString();
    }

    void rrToWire(org.xbill.DNS.DNSOutput p3, org.xbill.DNS.Compression p4, boolean p5)
    {
        p3.writeU16(this.order);
        p3.writeU16(this.preference);
        p3.writeCountedString(this.flags);
        p3.writeCountedString(this.service);
        p3.writeCountedString(this.regexp);
        this.replacement.toWire(p3, 0, p5);
        return;
    }
}
