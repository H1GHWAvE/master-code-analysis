.bytecode 50.0
.class public synchronized org/xbill/DNS/NXTRecord
.super org/xbill/DNS/Record

.field private static final 'serialVersionUID' J = -8851454400765507520L


.field private 'bitmap' Ljava/util/BitSet;

.field private 'next' Lorg/xbill/DNS/Name;

.method <init>()V
aload 0
invokespecial org/xbill/DNS/Record/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public <init>(Lorg/xbill/DNS/Name;IJLorg/xbill/DNS/Name;Ljava/util/BitSet;)V
aload 0
aload 1
bipush 30
iload 2
lload 3
invokespecial org/xbill/DNS/Record/<init>(Lorg/xbill/DNS/Name;IIJ)V
aload 0
ldc "next"
aload 5
invokestatic org/xbill/DNS/NXTRecord/checkName(Ljava/lang/String;Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/Name;
putfield org/xbill/DNS/NXTRecord/next Lorg/xbill/DNS/Name;
aload 0
aload 6
putfield org/xbill/DNS/NXTRecord/bitmap Ljava/util/BitSet;
return
.limit locals 7
.limit stack 6
.end method

.method public getBitmap()Ljava/util/BitSet;
aload 0
getfield org/xbill/DNS/NXTRecord/bitmap Ljava/util/BitSet;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getNext()Lorg/xbill/DNS/Name;
aload 0
getfield org/xbill/DNS/NXTRecord/next Lorg/xbill/DNS/Name;
areturn
.limit locals 1
.limit stack 1
.end method

.method getObject()Lorg/xbill/DNS/Record;
new org/xbill/DNS/NXTRecord
dup
invokespecial org/xbill/DNS/NXTRecord/<init>()V
areturn
.limit locals 1
.limit stack 2
.end method

.method rdataFromString(Lorg/xbill/DNS/Tokenizer;Lorg/xbill/DNS/Name;)V
aload 0
aload 1
aload 2
invokevirtual org/xbill/DNS/Tokenizer/getName(Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/Name;
putfield org/xbill/DNS/NXTRecord/next Lorg/xbill/DNS/Name;
aload 0
new java/util/BitSet
dup
invokespecial java/util/BitSet/<init>()V
putfield org/xbill/DNS/NXTRecord/bitmap Ljava/util/BitSet;
L0:
aload 1
invokevirtual org/xbill/DNS/Tokenizer/get()Lorg/xbill/DNS/Tokenizer$Token;
astore 2
aload 2
invokevirtual org/xbill/DNS/Tokenizer$Token/isString()Z
ifeq L1
aload 2
getfield org/xbill/DNS/Tokenizer$Token/value Ljava/lang/String;
iconst_1
invokestatic org/xbill/DNS/Type/value(Ljava/lang/String;Z)I
istore 3
iload 3
ifle L2
iload 3
sipush 128
if_icmple L3
L2:
aload 1
new java/lang/StringBuffer
dup
ldc "Invalid type: "
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
aload 2
getfield org/xbill/DNS/Tokenizer$Token/value Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual org/xbill/DNS/Tokenizer/exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;
athrow
L3:
aload 0
getfield org/xbill/DNS/NXTRecord/bitmap Ljava/util/BitSet;
iload 3
invokevirtual java/util/BitSet/set(I)V
goto L0
L1:
aload 1
invokevirtual org/xbill/DNS/Tokenizer/unget()V
return
.limit locals 4
.limit stack 4
.end method

.method rrFromWire(Lorg/xbill/DNS/DNSInput;)V
aload 0
new org/xbill/DNS/Name
dup
aload 1
invokespecial org/xbill/DNS/Name/<init>(Lorg/xbill/DNS/DNSInput;)V
putfield org/xbill/DNS/NXTRecord/next Lorg/xbill/DNS/Name;
aload 0
new java/util/BitSet
dup
invokespecial java/util/BitSet/<init>()V
putfield org/xbill/DNS/NXTRecord/bitmap Ljava/util/BitSet;
aload 1
invokevirtual org/xbill/DNS/DNSInput/remaining()I
istore 4
iconst_0
istore 2
L0:
iload 2
iload 4
if_icmpge L1
aload 1
invokevirtual org/xbill/DNS/DNSInput/readU8()I
istore 5
iconst_0
istore 3
L2:
iload 3
bipush 8
if_icmpge L3
iconst_1
bipush 7
iload 3
isub
ishl
iload 5
iand
ifeq L4
aload 0
getfield org/xbill/DNS/NXTRecord/bitmap Ljava/util/BitSet;
iload 2
bipush 8
imul
iload 3
iadd
invokevirtual java/util/BitSet/set(I)V
L4:
iload 3
iconst_1
iadd
istore 3
goto L2
L3:
iload 2
iconst_1
iadd
istore 2
goto L0
L1:
return
.limit locals 6
.limit stack 4
.end method

.method rrToString()Ljava/lang/String;
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
astore 3
aload 3
aload 0
getfield org/xbill/DNS/NXTRecord/next Lorg/xbill/DNS/Name;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/Object;)Ljava/lang/StringBuffer;
pop
aload 0
getfield org/xbill/DNS/NXTRecord/bitmap Ljava/util/BitSet;
invokevirtual java/util/BitSet/length()I
istore 2
iconst_0
istore 1
L0:
iload 1
iload 2
if_icmpge L1
aload 0
getfield org/xbill/DNS/NXTRecord/bitmap Ljava/util/BitSet;
iload 1
invokevirtual java/util/BitSet/get(I)Z
ifeq L2
aload 3
ldc " "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 3
iload 1
invokestatic org/xbill/DNS/Type/string(I)Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
L2:
iload 1
iconst_1
iadd
i2s
istore 1
goto L0
L1:
aload 3
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
areturn
.limit locals 4
.limit stack 2
.end method

.method rrToWire(Lorg/xbill/DNS/DNSOutput;Lorg/xbill/DNS/Compression;Z)V
aload 0
getfield org/xbill/DNS/NXTRecord/next Lorg/xbill/DNS/Name;
aload 1
aconst_null
iload 3
invokevirtual org/xbill/DNS/Name/toWire(Lorg/xbill/DNS/DNSOutput;Lorg/xbill/DNS/Compression;Z)V
aload 0
getfield org/xbill/DNS/NXTRecord/bitmap Ljava/util/BitSet;
invokevirtual java/util/BitSet/length()I
istore 7
iconst_0
istore 4
iconst_0
istore 5
L0:
iload 5
iload 7
if_icmpge L1
aload 0
getfield org/xbill/DNS/NXTRecord/bitmap Ljava/util/BitSet;
iload 5
invokevirtual java/util/BitSet/get(I)Z
ifeq L2
iconst_1
bipush 7
iload 5
bipush 8
irem
isub
ishl
istore 6
L3:
iload 6
iload 4
ior
istore 6
iload 5
bipush 8
irem
bipush 7
if_icmpeq L4
iload 6
istore 4
iload 5
iload 7
iconst_1
isub
if_icmpne L5
L4:
aload 1
iload 6
invokevirtual org/xbill/DNS/DNSOutput/writeU8(I)V
iconst_0
istore 4
L5:
iload 5
iconst_1
iadd
istore 5
goto L0
L2:
iconst_0
istore 6
goto L3
L1:
return
.limit locals 8
.limit stack 4
.end method
