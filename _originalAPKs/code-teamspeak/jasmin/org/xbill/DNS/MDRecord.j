.bytecode 50.0
.class public synchronized org/xbill/DNS/MDRecord
.super org/xbill/DNS/SingleNameBase

.field private static final 'serialVersionUID' J = 5268878603762942202L


.method <init>()V
aload 0
invokespecial org/xbill/DNS/SingleNameBase/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public <init>(Lorg/xbill/DNS/Name;IJLorg/xbill/DNS/Name;)V
aload 0
aload 1
iconst_3
iload 2
lload 3
aload 5
ldc "mail agent"
invokespecial org/xbill/DNS/SingleNameBase/<init>(Lorg/xbill/DNS/Name;IIJLorg/xbill/DNS/Name;Ljava/lang/String;)V
return
.limit locals 6
.limit stack 8
.end method

.method public getAdditionalName()Lorg/xbill/DNS/Name;
aload 0
invokevirtual org/xbill/DNS/MDRecord/getSingleName()Lorg/xbill/DNS/Name;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getMailAgent()Lorg/xbill/DNS/Name;
aload 0
invokevirtual org/xbill/DNS/MDRecord/getSingleName()Lorg/xbill/DNS/Name;
areturn
.limit locals 1
.limit stack 1
.end method

.method getObject()Lorg/xbill/DNS/Record;
new org/xbill/DNS/MDRecord
dup
invokespecial org/xbill/DNS/MDRecord/<init>()V
areturn
.limit locals 1
.limit stack 2
.end method
