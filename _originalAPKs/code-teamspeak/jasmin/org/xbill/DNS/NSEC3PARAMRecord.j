.bytecode 50.0
.class public synchronized org/xbill/DNS/NSEC3PARAMRecord
.super org/xbill/DNS/Record

.field private static final 'serialVersionUID' J = -8689038598776316533L


.field private 'flags' I

.field private 'hashAlg' I

.field private 'iterations' I

.field private 'salt' [B

.method <init>()V
aload 0
invokespecial org/xbill/DNS/Record/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public <init>(Lorg/xbill/DNS/Name;IJIII[B)V
aload 0
aload 1
bipush 51
iload 2
lload 3
invokespecial org/xbill/DNS/Record/<init>(Lorg/xbill/DNS/Name;IIJ)V
aload 0
ldc "hashAlg"
iload 5
invokestatic org/xbill/DNS/NSEC3PARAMRecord/checkU8(Ljava/lang/String;I)I
putfield org/xbill/DNS/NSEC3PARAMRecord/hashAlg I
aload 0
ldc "flags"
iload 6
invokestatic org/xbill/DNS/NSEC3PARAMRecord/checkU8(Ljava/lang/String;I)I
putfield org/xbill/DNS/NSEC3PARAMRecord/flags I
aload 0
ldc "iterations"
iload 7
invokestatic org/xbill/DNS/NSEC3PARAMRecord/checkU16(Ljava/lang/String;I)I
putfield org/xbill/DNS/NSEC3PARAMRecord/iterations I
aload 8
ifnull L0
aload 8
arraylength
sipush 255
if_icmple L1
new java/lang/IllegalArgumentException
dup
ldc "Invalid salt length"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L1:
aload 8
arraylength
ifle L0
aload 0
aload 8
arraylength
newarray byte
putfield org/xbill/DNS/NSEC3PARAMRecord/salt [B
aload 8
iconst_0
aload 0
getfield org/xbill/DNS/NSEC3PARAMRecord/salt [B
iconst_0
aload 8
arraylength
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
L0:
return
.limit locals 9
.limit stack 6
.end method

.method public getFlags()I
aload 0
getfield org/xbill/DNS/NSEC3PARAMRecord/flags I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getHashAlgorithm()I
aload 0
getfield org/xbill/DNS/NSEC3PARAMRecord/hashAlg I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getIterations()I
aload 0
getfield org/xbill/DNS/NSEC3PARAMRecord/iterations I
ireturn
.limit locals 1
.limit stack 1
.end method

.method getObject()Lorg/xbill/DNS/Record;
new org/xbill/DNS/NSEC3PARAMRecord
dup
invokespecial org/xbill/DNS/NSEC3PARAMRecord/<init>()V
areturn
.limit locals 1
.limit stack 2
.end method

.method public getSalt()[B
aload 0
getfield org/xbill/DNS/NSEC3PARAMRecord/salt [B
areturn
.limit locals 1
.limit stack 1
.end method

.method public hashName(Lorg/xbill/DNS/Name;)[B
aload 1
aload 0
getfield org/xbill/DNS/NSEC3PARAMRecord/hashAlg I
aload 0
getfield org/xbill/DNS/NSEC3PARAMRecord/iterations I
aload 0
getfield org/xbill/DNS/NSEC3PARAMRecord/salt [B
invokestatic org/xbill/DNS/NSEC3Record/hashName(Lorg/xbill/DNS/Name;II[B)[B
areturn
.limit locals 2
.limit stack 4
.end method

.method rdataFromString(Lorg/xbill/DNS/Tokenizer;Lorg/xbill/DNS/Name;)V
aload 0
aload 1
invokevirtual org/xbill/DNS/Tokenizer/getUInt8()I
putfield org/xbill/DNS/NSEC3PARAMRecord/hashAlg I
aload 0
aload 1
invokevirtual org/xbill/DNS/Tokenizer/getUInt8()I
putfield org/xbill/DNS/NSEC3PARAMRecord/flags I
aload 0
aload 1
invokevirtual org/xbill/DNS/Tokenizer/getUInt16()I
putfield org/xbill/DNS/NSEC3PARAMRecord/iterations I
aload 1
invokevirtual org/xbill/DNS/Tokenizer/getString()Ljava/lang/String;
ldc "-"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L0
aload 0
aconst_null
putfield org/xbill/DNS/NSEC3PARAMRecord/salt [B
L1:
return
L0:
aload 1
invokevirtual org/xbill/DNS/Tokenizer/unget()V
aload 0
aload 1
invokevirtual org/xbill/DNS/Tokenizer/getHexString()[B
putfield org/xbill/DNS/NSEC3PARAMRecord/salt [B
aload 0
getfield org/xbill/DNS/NSEC3PARAMRecord/salt [B
arraylength
sipush 255
if_icmple L1
aload 1
ldc "salt value too long"
invokevirtual org/xbill/DNS/Tokenizer/exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;
athrow
.limit locals 3
.limit stack 2
.end method

.method rrFromWire(Lorg/xbill/DNS/DNSInput;)V
aload 0
aload 1
invokevirtual org/xbill/DNS/DNSInput/readU8()I
putfield org/xbill/DNS/NSEC3PARAMRecord/hashAlg I
aload 0
aload 1
invokevirtual org/xbill/DNS/DNSInput/readU8()I
putfield org/xbill/DNS/NSEC3PARAMRecord/flags I
aload 0
aload 1
invokevirtual org/xbill/DNS/DNSInput/readU16()I
putfield org/xbill/DNS/NSEC3PARAMRecord/iterations I
aload 1
invokevirtual org/xbill/DNS/DNSInput/readU8()I
istore 2
iload 2
ifle L0
aload 0
aload 1
iload 2
invokevirtual org/xbill/DNS/DNSInput/readByteArray(I)[B
putfield org/xbill/DNS/NSEC3PARAMRecord/salt [B
return
L0:
aload 0
aconst_null
putfield org/xbill/DNS/NSEC3PARAMRecord/salt [B
return
.limit locals 3
.limit stack 3
.end method

.method rrToString()Ljava/lang/String;
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
astore 1
aload 1
aload 0
getfield org/xbill/DNS/NSEC3PARAMRecord/hashAlg I
invokevirtual java/lang/StringBuffer/append(I)Ljava/lang/StringBuffer;
pop
aload 1
bipush 32
invokevirtual java/lang/StringBuffer/append(C)Ljava/lang/StringBuffer;
pop
aload 1
aload 0
getfield org/xbill/DNS/NSEC3PARAMRecord/flags I
invokevirtual java/lang/StringBuffer/append(I)Ljava/lang/StringBuffer;
pop
aload 1
bipush 32
invokevirtual java/lang/StringBuffer/append(C)Ljava/lang/StringBuffer;
pop
aload 1
aload 0
getfield org/xbill/DNS/NSEC3PARAMRecord/iterations I
invokevirtual java/lang/StringBuffer/append(I)Ljava/lang/StringBuffer;
pop
aload 1
bipush 32
invokevirtual java/lang/StringBuffer/append(C)Ljava/lang/StringBuffer;
pop
aload 0
getfield org/xbill/DNS/NSEC3PARAMRecord/salt [B
ifnonnull L0
aload 1
bipush 45
invokevirtual java/lang/StringBuffer/append(C)Ljava/lang/StringBuffer;
pop
L1:
aload 1
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
areturn
L0:
aload 1
aload 0
getfield org/xbill/DNS/NSEC3PARAMRecord/salt [B
invokestatic org/xbill/DNS/utils/base16/toString([B)Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
goto L1
.limit locals 2
.limit stack 2
.end method

.method rrToWire(Lorg/xbill/DNS/DNSOutput;Lorg/xbill/DNS/Compression;Z)V
aload 1
aload 0
getfield org/xbill/DNS/NSEC3PARAMRecord/hashAlg I
invokevirtual org/xbill/DNS/DNSOutput/writeU8(I)V
aload 1
aload 0
getfield org/xbill/DNS/NSEC3PARAMRecord/flags I
invokevirtual org/xbill/DNS/DNSOutput/writeU8(I)V
aload 1
aload 0
getfield org/xbill/DNS/NSEC3PARAMRecord/iterations I
invokevirtual org/xbill/DNS/DNSOutput/writeU16(I)V
aload 0
getfield org/xbill/DNS/NSEC3PARAMRecord/salt [B
ifnull L0
aload 1
aload 0
getfield org/xbill/DNS/NSEC3PARAMRecord/salt [B
arraylength
invokevirtual org/xbill/DNS/DNSOutput/writeU8(I)V
aload 1
aload 0
getfield org/xbill/DNS/NSEC3PARAMRecord/salt [B
invokevirtual org/xbill/DNS/DNSOutput/writeByteArray([B)V
return
L0:
aload 1
iconst_0
invokevirtual org/xbill/DNS/DNSOutput/writeU8(I)V
return
.limit locals 4
.limit stack 2
.end method
