.bytecode 50.0
.class public synchronized org/xbill/DNS/InvalidTypeException
.super java/lang/IllegalArgumentException

.method public <init>(I)V
aload 0
new java/lang/StringBuffer
dup
ldc "Invalid DNS type: "
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
iload 1
invokevirtual java/lang/StringBuffer/append(I)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
return
.limit locals 2
.limit stack 4
.end method
