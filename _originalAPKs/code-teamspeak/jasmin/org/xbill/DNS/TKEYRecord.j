.bytecode 50.0
.class public synchronized org/xbill/DNS/TKEYRecord
.super org/xbill/DNS/Record

.field public static final 'DELETE' I = 5


.field public static final 'DIFFIEHELLMAN' I = 2


.field public static final 'GSSAPI' I = 3


.field public static final 'RESOLVERASSIGNED' I = 4


.field public static final 'SERVERASSIGNED' I = 1


.field private static final 'serialVersionUID' J = 8828458121926391756L


.field private 'alg' Lorg/xbill/DNS/Name;

.field private 'error' I

.field private 'key' [B

.field private 'mode' I

.field private 'other' [B

.field private 'timeExpire' Ljava/util/Date;

.field private 'timeInception' Ljava/util/Date;

.method <init>()V
aload 0
invokespecial org/xbill/DNS/Record/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public <init>(Lorg/xbill/DNS/Name;IJLorg/xbill/DNS/Name;Ljava/util/Date;Ljava/util/Date;II[B[B)V
aload 0
aload 1
sipush 249
iload 2
lload 3
invokespecial org/xbill/DNS/Record/<init>(Lorg/xbill/DNS/Name;IIJ)V
aload 0
ldc "alg"
aload 5
invokestatic org/xbill/DNS/TKEYRecord/checkName(Ljava/lang/String;Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/Name;
putfield org/xbill/DNS/TKEYRecord/alg Lorg/xbill/DNS/Name;
aload 0
aload 6
putfield org/xbill/DNS/TKEYRecord/timeInception Ljava/util/Date;
aload 0
aload 7
putfield org/xbill/DNS/TKEYRecord/timeExpire Ljava/util/Date;
aload 0
ldc "mode"
iload 8
invokestatic org/xbill/DNS/TKEYRecord/checkU16(Ljava/lang/String;I)I
putfield org/xbill/DNS/TKEYRecord/mode I
aload 0
ldc "error"
iload 9
invokestatic org/xbill/DNS/TKEYRecord/checkU16(Ljava/lang/String;I)I
putfield org/xbill/DNS/TKEYRecord/error I
aload 0
aload 10
putfield org/xbill/DNS/TKEYRecord/key [B
aload 0
aload 11
putfield org/xbill/DNS/TKEYRecord/other [B
return
.limit locals 12
.limit stack 6
.end method

.method public getAlgorithm()Lorg/xbill/DNS/Name;
aload 0
getfield org/xbill/DNS/TKEYRecord/alg Lorg/xbill/DNS/Name;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getError()I
aload 0
getfield org/xbill/DNS/TKEYRecord/error I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getKey()[B
aload 0
getfield org/xbill/DNS/TKEYRecord/key [B
areturn
.limit locals 1
.limit stack 1
.end method

.method public getMode()I
aload 0
getfield org/xbill/DNS/TKEYRecord/mode I
ireturn
.limit locals 1
.limit stack 1
.end method

.method getObject()Lorg/xbill/DNS/Record;
new org/xbill/DNS/TKEYRecord
dup
invokespecial org/xbill/DNS/TKEYRecord/<init>()V
areturn
.limit locals 1
.limit stack 2
.end method

.method public getOther()[B
aload 0
getfield org/xbill/DNS/TKEYRecord/other [B
areturn
.limit locals 1
.limit stack 1
.end method

.method public getTimeExpire()Ljava/util/Date;
aload 0
getfield org/xbill/DNS/TKEYRecord/timeExpire Ljava/util/Date;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getTimeInception()Ljava/util/Date;
aload 0
getfield org/xbill/DNS/TKEYRecord/timeInception Ljava/util/Date;
areturn
.limit locals 1
.limit stack 1
.end method

.method protected modeString()Ljava/lang/String;
aload 0
getfield org/xbill/DNS/TKEYRecord/mode I
tableswitch 1
L0
L1
L2
L3
L4
default : L5
L5:
aload 0
getfield org/xbill/DNS/TKEYRecord/mode I
invokestatic java/lang/Integer/toString(I)Ljava/lang/String;
areturn
L0:
ldc "SERVERASSIGNED"
areturn
L1:
ldc "DIFFIEHELLMAN"
areturn
L2:
ldc "GSSAPI"
areturn
L3:
ldc "RESOLVERASSIGNED"
areturn
L4:
ldc "DELETE"
areturn
.limit locals 1
.limit stack 1
.end method

.method rdataFromString(Lorg/xbill/DNS/Tokenizer;Lorg/xbill/DNS/Name;)V
aload 1
ldc "no text format defined for TKEY"
invokevirtual org/xbill/DNS/Tokenizer/exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;
athrow
.limit locals 3
.limit stack 2
.end method

.method rrFromWire(Lorg/xbill/DNS/DNSInput;)V
aload 0
new org/xbill/DNS/Name
dup
aload 1
invokespecial org/xbill/DNS/Name/<init>(Lorg/xbill/DNS/DNSInput;)V
putfield org/xbill/DNS/TKEYRecord/alg Lorg/xbill/DNS/Name;
aload 0
new java/util/Date
dup
aload 1
invokevirtual org/xbill/DNS/DNSInput/readU32()J
ldc2_w 1000L
lmul
invokespecial java/util/Date/<init>(J)V
putfield org/xbill/DNS/TKEYRecord/timeInception Ljava/util/Date;
aload 0
new java/util/Date
dup
aload 1
invokevirtual org/xbill/DNS/DNSInput/readU32()J
ldc2_w 1000L
lmul
invokespecial java/util/Date/<init>(J)V
putfield org/xbill/DNS/TKEYRecord/timeExpire Ljava/util/Date;
aload 0
aload 1
invokevirtual org/xbill/DNS/DNSInput/readU16()I
putfield org/xbill/DNS/TKEYRecord/mode I
aload 0
aload 1
invokevirtual org/xbill/DNS/DNSInput/readU16()I
putfield org/xbill/DNS/TKEYRecord/error I
aload 1
invokevirtual org/xbill/DNS/DNSInput/readU16()I
istore 2
iload 2
ifle L0
aload 0
aload 1
iload 2
invokevirtual org/xbill/DNS/DNSInput/readByteArray(I)[B
putfield org/xbill/DNS/TKEYRecord/key [B
L1:
aload 1
invokevirtual org/xbill/DNS/DNSInput/readU16()I
istore 2
iload 2
ifle L2
aload 0
aload 1
iload 2
invokevirtual org/xbill/DNS/DNSInput/readByteArray(I)[B
putfield org/xbill/DNS/TKEYRecord/other [B
return
L0:
aload 0
aconst_null
putfield org/xbill/DNS/TKEYRecord/key [B
goto L1
L2:
aload 0
aconst_null
putfield org/xbill/DNS/TKEYRecord/other [B
return
.limit locals 3
.limit stack 7
.end method

.method rrToString()Ljava/lang/String;
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
astore 1
aload 1
aload 0
getfield org/xbill/DNS/TKEYRecord/alg Lorg/xbill/DNS/Name;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/Object;)Ljava/lang/StringBuffer;
pop
aload 1
ldc " "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
ldc "multiline"
invokestatic org/xbill/DNS/Options/check(Ljava/lang/String;)Z
ifeq L0
aload 1
ldc "(\n\u0009"
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
L0:
aload 1
aload 0
getfield org/xbill/DNS/TKEYRecord/timeInception Ljava/util/Date;
invokestatic org/xbill/DNS/FormattedTime/format(Ljava/util/Date;)Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
ldc " "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
aload 0
getfield org/xbill/DNS/TKEYRecord/timeExpire Ljava/util/Date;
invokestatic org/xbill/DNS/FormattedTime/format(Ljava/util/Date;)Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
ldc " "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
aload 0
invokevirtual org/xbill/DNS/TKEYRecord/modeString()Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
ldc " "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
aload 0
getfield org/xbill/DNS/TKEYRecord/error I
invokestatic org/xbill/DNS/Rcode/TSIGstring(I)Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
ldc "multiline"
invokestatic org/xbill/DNS/Options/check(Ljava/lang/String;)Z
ifeq L1
aload 1
ldc "\n"
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 0
getfield org/xbill/DNS/TKEYRecord/key [B
ifnull L2
aload 1
aload 0
getfield org/xbill/DNS/TKEYRecord/key [B
bipush 64
ldc "\u0009"
iconst_0
invokestatic org/xbill/DNS/utils/base64/formatString([BILjava/lang/String;Z)Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
ldc "\n"
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
L2:
aload 0
getfield org/xbill/DNS/TKEYRecord/other [B
ifnull L3
aload 1
aload 0
getfield org/xbill/DNS/TKEYRecord/other [B
bipush 64
ldc "\u0009"
iconst_0
invokestatic org/xbill/DNS/utils/base64/formatString([BILjava/lang/String;Z)Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
L3:
aload 1
ldc " )"
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
L4:
aload 1
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
areturn
L1:
aload 1
ldc " "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 0
getfield org/xbill/DNS/TKEYRecord/key [B
ifnull L5
aload 1
aload 0
getfield org/xbill/DNS/TKEYRecord/key [B
invokestatic org/xbill/DNS/utils/base64/toString([B)Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
ldc " "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
L5:
aload 0
getfield org/xbill/DNS/TKEYRecord/other [B
ifnull L4
aload 1
aload 0
getfield org/xbill/DNS/TKEYRecord/other [B
invokestatic org/xbill/DNS/utils/base64/toString([B)Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
goto L4
.limit locals 2
.limit stack 5
.end method

.method rrToWire(Lorg/xbill/DNS/DNSOutput;Lorg/xbill/DNS/Compression;Z)V
aload 0
getfield org/xbill/DNS/TKEYRecord/alg Lorg/xbill/DNS/Name;
aload 1
aconst_null
iload 3
invokevirtual org/xbill/DNS/Name/toWire(Lorg/xbill/DNS/DNSOutput;Lorg/xbill/DNS/Compression;Z)V
aload 1
aload 0
getfield org/xbill/DNS/TKEYRecord/timeInception Ljava/util/Date;
invokevirtual java/util/Date/getTime()J
ldc2_w 1000L
ldiv
invokevirtual org/xbill/DNS/DNSOutput/writeU32(J)V
aload 1
aload 0
getfield org/xbill/DNS/TKEYRecord/timeExpire Ljava/util/Date;
invokevirtual java/util/Date/getTime()J
ldc2_w 1000L
ldiv
invokevirtual org/xbill/DNS/DNSOutput/writeU32(J)V
aload 1
aload 0
getfield org/xbill/DNS/TKEYRecord/mode I
invokevirtual org/xbill/DNS/DNSOutput/writeU16(I)V
aload 1
aload 0
getfield org/xbill/DNS/TKEYRecord/error I
invokevirtual org/xbill/DNS/DNSOutput/writeU16(I)V
aload 0
getfield org/xbill/DNS/TKEYRecord/key [B
ifnull L0
aload 1
aload 0
getfield org/xbill/DNS/TKEYRecord/key [B
arraylength
invokevirtual org/xbill/DNS/DNSOutput/writeU16(I)V
aload 1
aload 0
getfield org/xbill/DNS/TKEYRecord/key [B
invokevirtual org/xbill/DNS/DNSOutput/writeByteArray([B)V
L1:
aload 0
getfield org/xbill/DNS/TKEYRecord/other [B
ifnull L2
aload 1
aload 0
getfield org/xbill/DNS/TKEYRecord/other [B
arraylength
invokevirtual org/xbill/DNS/DNSOutput/writeU16(I)V
aload 1
aload 0
getfield org/xbill/DNS/TKEYRecord/other [B
invokevirtual org/xbill/DNS/DNSOutput/writeByteArray([B)V
return
L0:
aload 1
iconst_0
invokevirtual org/xbill/DNS/DNSOutput/writeU16(I)V
goto L1
L2:
aload 1
iconst_0
invokevirtual org/xbill/DNS/DNSOutput/writeU16(I)V
return
.limit locals 4
.limit stack 5
.end method
