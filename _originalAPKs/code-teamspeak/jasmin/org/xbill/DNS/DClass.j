.bytecode 50.0
.class public final synchronized org/xbill/DNS/DClass
.super java/lang/Object

.field public static final 'ANY' I = 255


.field public static final 'CH' I = 3


.field public static final 'CHAOS' I = 3


.field public static final 'HESIOD' I = 4


.field public static final 'HS' I = 4


.field public static final 'IN' I = 1


.field public static final 'NONE' I = 254


.field private static 'classes' Lorg/xbill/DNS/Mnemonic;

.method static <clinit>()V
new org/xbill/DNS/DClass$DClassMnemonic
dup
invokespecial org/xbill/DNS/DClass$DClassMnemonic/<init>()V
astore 0
aload 0
putstatic org/xbill/DNS/DClass/classes Lorg/xbill/DNS/Mnemonic;
aload 0
iconst_1
ldc "IN"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/DClass/classes Lorg/xbill/DNS/Mnemonic;
iconst_3
ldc "CH"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/DClass/classes Lorg/xbill/DNS/Mnemonic;
iconst_3
ldc "CHAOS"
invokevirtual org/xbill/DNS/Mnemonic/addAlias(ILjava/lang/String;)V
getstatic org/xbill/DNS/DClass/classes Lorg/xbill/DNS/Mnemonic;
iconst_4
ldc "HS"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/DClass/classes Lorg/xbill/DNS/Mnemonic;
iconst_4
ldc "HESIOD"
invokevirtual org/xbill/DNS/Mnemonic/addAlias(ILjava/lang/String;)V
getstatic org/xbill/DNS/DClass/classes Lorg/xbill/DNS/Mnemonic;
sipush 254
ldc "NONE"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/DClass/classes Lorg/xbill/DNS/Mnemonic;
sipush 255
ldc "ANY"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
return
.limit locals 1
.limit stack 3
.end method

.method private <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static check(I)V
iload 0
iflt L0
iload 0
ldc_w 65535
if_icmple L1
L0:
new org/xbill/DNS/InvalidDClassException
dup
iload 0
invokespecial org/xbill/DNS/InvalidDClassException/<init>(I)V
athrow
L1:
return
.limit locals 1
.limit stack 3
.end method

.method public static string(I)Ljava/lang/String;
getstatic org/xbill/DNS/DClass/classes Lorg/xbill/DNS/Mnemonic;
iload 0
invokevirtual org/xbill/DNS/Mnemonic/getText(I)Ljava/lang/String;
areturn
.limit locals 1
.limit stack 2
.end method

.method public static value(Ljava/lang/String;)I
getstatic org/xbill/DNS/DClass/classes Lorg/xbill/DNS/Mnemonic;
aload 0
invokevirtual org/xbill/DNS/Mnemonic/getValue(Ljava/lang/String;)I
ireturn
.limit locals 1
.limit stack 2
.end method
