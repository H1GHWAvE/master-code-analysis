.bytecode 50.0
.class public synchronized org/xbill/DNS/SetResponse
.super java/lang/Object

.field static final 'CNAME' I = 4


.field static final 'DELEGATION' I = 3


.field static final 'DNAME' I = 5


.field static final 'NXDOMAIN' I = 1


.field static final 'NXRRSET' I = 2


.field static final 'SUCCESSFUL' I = 6


.field static final 'UNKNOWN' I = 0


.field private static final 'nxdomain' Lorg/xbill/DNS/SetResponse;

.field private static final 'nxrrset' Lorg/xbill/DNS/SetResponse;

.field private static final 'unknown' Lorg/xbill/DNS/SetResponse;

.field private 'data' Ljava/lang/Object;

.field private 'type' I

.method static <clinit>()V
new org/xbill/DNS/SetResponse
dup
iconst_0
invokespecial org/xbill/DNS/SetResponse/<init>(I)V
putstatic org/xbill/DNS/SetResponse/unknown Lorg/xbill/DNS/SetResponse;
new org/xbill/DNS/SetResponse
dup
iconst_1
invokespecial org/xbill/DNS/SetResponse/<init>(I)V
putstatic org/xbill/DNS/SetResponse/nxdomain Lorg/xbill/DNS/SetResponse;
new org/xbill/DNS/SetResponse
dup
iconst_2
invokespecial org/xbill/DNS/SetResponse/<init>(I)V
putstatic org/xbill/DNS/SetResponse/nxrrset Lorg/xbill/DNS/SetResponse;
return
.limit locals 0
.limit stack 3
.end method

.method private <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method <init>(I)V
aload 0
invokespecial java/lang/Object/<init>()V
iload 1
iflt L0
iload 1
bipush 6
if_icmple L1
L0:
new java/lang/IllegalArgumentException
dup
ldc "invalid type"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L1:
aload 0
iload 1
putfield org/xbill/DNS/SetResponse/type I
aload 0
aconst_null
putfield org/xbill/DNS/SetResponse/data Ljava/lang/Object;
return
.limit locals 2
.limit stack 3
.end method

.method <init>(ILorg/xbill/DNS/RRset;)V
aload 0
invokespecial java/lang/Object/<init>()V
iload 1
iflt L0
iload 1
bipush 6
if_icmple L1
L0:
new java/lang/IllegalArgumentException
dup
ldc "invalid type"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L1:
aload 0
iload 1
putfield org/xbill/DNS/SetResponse/type I
aload 0
aload 2
putfield org/xbill/DNS/SetResponse/data Ljava/lang/Object;
return
.limit locals 3
.limit stack 3
.end method

.method static ofType(I)Lorg/xbill/DNS/SetResponse;
iload 0
tableswitch 0
L0
L1
L2
L3
L3
L3
L3
default : L4
L4:
new java/lang/IllegalArgumentException
dup
ldc "invalid type"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
getstatic org/xbill/DNS/SetResponse/unknown Lorg/xbill/DNS/SetResponse;
areturn
L1:
getstatic org/xbill/DNS/SetResponse/nxdomain Lorg/xbill/DNS/SetResponse;
areturn
L2:
getstatic org/xbill/DNS/SetResponse/nxrrset Lorg/xbill/DNS/SetResponse;
areturn
L3:
new org/xbill/DNS/SetResponse
dup
invokespecial org/xbill/DNS/SetResponse/<init>()V
astore 1
aload 1
iload 0
putfield org/xbill/DNS/SetResponse/type I
aload 1
aconst_null
putfield org/xbill/DNS/SetResponse/data Ljava/lang/Object;
aload 1
areturn
.limit locals 2
.limit stack 3
.end method

.method addRRset(Lorg/xbill/DNS/RRset;)V
aload 0
getfield org/xbill/DNS/SetResponse/data Ljava/lang/Object;
ifnonnull L0
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield org/xbill/DNS/SetResponse/data Ljava/lang/Object;
L0:
aload 0
getfield org/xbill/DNS/SetResponse/data Ljava/lang/Object;
checkcast java/util/List
aload 1
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
return
.limit locals 2
.limit stack 3
.end method

.method public answers()[Lorg/xbill/DNS/RRset;
aload 0
getfield org/xbill/DNS/SetResponse/type I
bipush 6
if_icmpeq L0
aconst_null
areturn
L0:
aload 0
getfield org/xbill/DNS/SetResponse/data Ljava/lang/Object;
checkcast java/util/List
astore 1
aload 1
aload 1
invokeinterface java/util/List/size()I 0
anewarray org/xbill/DNS/RRset
invokeinterface java/util/List/toArray([Ljava/lang/Object;)[Ljava/lang/Object; 1
checkcast [Lorg/xbill/DNS/RRset;
checkcast [Lorg/xbill/DNS/RRset;
areturn
.limit locals 2
.limit stack 2
.end method

.method public getCNAME()Lorg/xbill/DNS/CNAMERecord;
aload 0
getfield org/xbill/DNS/SetResponse/data Ljava/lang/Object;
checkcast org/xbill/DNS/RRset
invokevirtual org/xbill/DNS/RRset/first()Lorg/xbill/DNS/Record;
checkcast org/xbill/DNS/CNAMERecord
areturn
.limit locals 1
.limit stack 1
.end method

.method public getDNAME()Lorg/xbill/DNS/DNAMERecord;
aload 0
getfield org/xbill/DNS/SetResponse/data Ljava/lang/Object;
checkcast org/xbill/DNS/RRset
invokevirtual org/xbill/DNS/RRset/first()Lorg/xbill/DNS/Record;
checkcast org/xbill/DNS/DNAMERecord
areturn
.limit locals 1
.limit stack 1
.end method

.method public getNS()Lorg/xbill/DNS/RRset;
aload 0
getfield org/xbill/DNS/SetResponse/data Ljava/lang/Object;
checkcast org/xbill/DNS/RRset
areturn
.limit locals 1
.limit stack 1
.end method

.method public isCNAME()Z
aload 0
getfield org/xbill/DNS/SetResponse/type I
iconst_4
if_icmpne L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method public isDNAME()Z
aload 0
getfield org/xbill/DNS/SetResponse/type I
iconst_5
if_icmpne L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method public isDelegation()Z
aload 0
getfield org/xbill/DNS/SetResponse/type I
iconst_3
if_icmpne L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method public isNXDOMAIN()Z
aload 0
getfield org/xbill/DNS/SetResponse/type I
iconst_1
if_icmpne L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method public isNXRRSET()Z
aload 0
getfield org/xbill/DNS/SetResponse/type I
iconst_2
if_icmpne L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method public isSuccessful()Z
aload 0
getfield org/xbill/DNS/SetResponse/type I
bipush 6
if_icmpne L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method public isUnknown()Z
aload 0
getfield org/xbill/DNS/SetResponse/type I
ifne L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public toString()Ljava/lang/String;
aload 0
getfield org/xbill/DNS/SetResponse/type I
tableswitch 0
L0
L1
L2
L3
L4
L5
L6
default : L7
L7:
new java/lang/IllegalStateException
dup
invokespecial java/lang/IllegalStateException/<init>()V
athrow
L0:
ldc "unknown"
areturn
L1:
ldc "NXDOMAIN"
areturn
L2:
ldc "NXRRSET"
areturn
L3:
new java/lang/StringBuffer
dup
ldc "delegation: "
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
aload 0
getfield org/xbill/DNS/SetResponse/data Ljava/lang/Object;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/Object;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
areturn
L4:
new java/lang/StringBuffer
dup
ldc "CNAME: "
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
aload 0
getfield org/xbill/DNS/SetResponse/data Ljava/lang/Object;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/Object;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
areturn
L5:
new java/lang/StringBuffer
dup
ldc "DNAME: "
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
aload 0
getfield org/xbill/DNS/SetResponse/data Ljava/lang/Object;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/Object;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
areturn
L6:
ldc "successful"
areturn
.limit locals 1
.limit stack 3
.end method
