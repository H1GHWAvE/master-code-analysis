.bytecode 50.0
.class synchronized abstract org/xbill/DNS/SingleNameBase
.super org/xbill/DNS/Record

.field private static final 'serialVersionUID' J = -18595042501413L


.field protected 'singleName' Lorg/xbill/DNS/Name;

.method protected <init>()V
aload 0
invokespecial org/xbill/DNS/Record/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method protected <init>(Lorg/xbill/DNS/Name;IIJ)V
aload 0
aload 1
iload 2
iload 3
lload 4
invokespecial org/xbill/DNS/Record/<init>(Lorg/xbill/DNS/Name;IIJ)V
return
.limit locals 6
.limit stack 6
.end method

.method protected <init>(Lorg/xbill/DNS/Name;IIJLorg/xbill/DNS/Name;Ljava/lang/String;)V
aload 0
aload 1
iload 2
iload 3
lload 4
invokespecial org/xbill/DNS/Record/<init>(Lorg/xbill/DNS/Name;IIJ)V
aload 0
aload 7
aload 6
invokestatic org/xbill/DNS/SingleNameBase/checkName(Ljava/lang/String;Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/Name;
putfield org/xbill/DNS/SingleNameBase/singleName Lorg/xbill/DNS/Name;
return
.limit locals 8
.limit stack 6
.end method

.method protected getSingleName()Lorg/xbill/DNS/Name;
aload 0
getfield org/xbill/DNS/SingleNameBase/singleName Lorg/xbill/DNS/Name;
areturn
.limit locals 1
.limit stack 1
.end method

.method rdataFromString(Lorg/xbill/DNS/Tokenizer;Lorg/xbill/DNS/Name;)V
aload 0
aload 1
aload 2
invokevirtual org/xbill/DNS/Tokenizer/getName(Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/Name;
putfield org/xbill/DNS/SingleNameBase/singleName Lorg/xbill/DNS/Name;
return
.limit locals 3
.limit stack 3
.end method

.method rrFromWire(Lorg/xbill/DNS/DNSInput;)V
aload 0
new org/xbill/DNS/Name
dup
aload 1
invokespecial org/xbill/DNS/Name/<init>(Lorg/xbill/DNS/DNSInput;)V
putfield org/xbill/DNS/SingleNameBase/singleName Lorg/xbill/DNS/Name;
return
.limit locals 2
.limit stack 4
.end method

.method rrToString()Ljava/lang/String;
aload 0
getfield org/xbill/DNS/SingleNameBase/singleName Lorg/xbill/DNS/Name;
invokevirtual org/xbill/DNS/Name/toString()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method rrToWire(Lorg/xbill/DNS/DNSOutput;Lorg/xbill/DNS/Compression;Z)V
aload 0
getfield org/xbill/DNS/SingleNameBase/singleName Lorg/xbill/DNS/Name;
aload 1
aconst_null
iload 3
invokevirtual org/xbill/DNS/Name/toWire(Lorg/xbill/DNS/DNSOutput;Lorg/xbill/DNS/Compression;Z)V
return
.limit locals 4
.limit stack 4
.end method
