.bytecode 50.0
.class public synchronized org/xbill/DNS/CERTRecord$CertificateType
.super java/lang/Object

.field public static final 'ACPKIX' I = 7


.field public static final 'IACPKIX' I = 8


.field public static final 'IPGP' I = 6


.field public static final 'IPKIX' I = 4


.field public static final 'ISPKI' I = 5


.field public static final 'OID' I = 254


.field public static final 'PGP' I = 3


.field public static final 'PKIX' I = 1


.field public static final 'SPKI' I = 2


.field public static final 'URI' I = 253


.field private static 'types' Lorg/xbill/DNS/Mnemonic;

.method static <clinit>()V
new org/xbill/DNS/Mnemonic
dup
ldc "Certificate type"
iconst_2
invokespecial org/xbill/DNS/Mnemonic/<init>(Ljava/lang/String;I)V
astore 0
aload 0
putstatic org/xbill/DNS/CERTRecord$CertificateType/types Lorg/xbill/DNS/Mnemonic;
aload 0
ldc_w 65535
invokevirtual org/xbill/DNS/Mnemonic/setMaximum(I)V
getstatic org/xbill/DNS/CERTRecord$CertificateType/types Lorg/xbill/DNS/Mnemonic;
iconst_1
invokevirtual org/xbill/DNS/Mnemonic/setNumericAllowed(Z)V
getstatic org/xbill/DNS/CERTRecord$CertificateType/types Lorg/xbill/DNS/Mnemonic;
iconst_1
ldc "PKIX"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/CERTRecord$CertificateType/types Lorg/xbill/DNS/Mnemonic;
iconst_2
ldc "SPKI"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/CERTRecord$CertificateType/types Lorg/xbill/DNS/Mnemonic;
iconst_3
ldc "PGP"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/CERTRecord$CertificateType/types Lorg/xbill/DNS/Mnemonic;
iconst_1
ldc "IPKIX"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/CERTRecord$CertificateType/types Lorg/xbill/DNS/Mnemonic;
iconst_2
ldc "ISPKI"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/CERTRecord$CertificateType/types Lorg/xbill/DNS/Mnemonic;
iconst_3
ldc "IPGP"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/CERTRecord$CertificateType/types Lorg/xbill/DNS/Mnemonic;
iconst_3
ldc "ACPKIX"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/CERTRecord$CertificateType/types Lorg/xbill/DNS/Mnemonic;
iconst_3
ldc "IACPKIX"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/CERTRecord$CertificateType/types Lorg/xbill/DNS/Mnemonic;
sipush 253
ldc "URI"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/CERTRecord$CertificateType/types Lorg/xbill/DNS/Mnemonic;
sipush 254
ldc "OID"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
return
.limit locals 1
.limit stack 4
.end method

.method private <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static string(I)Ljava/lang/String;
getstatic org/xbill/DNS/CERTRecord$CertificateType/types Lorg/xbill/DNS/Mnemonic;
iload 0
invokevirtual org/xbill/DNS/Mnemonic/getText(I)Ljava/lang/String;
areturn
.limit locals 1
.limit stack 2
.end method

.method public static value(Ljava/lang/String;)I
getstatic org/xbill/DNS/CERTRecord$CertificateType/types Lorg/xbill/DNS/Mnemonic;
aload 0
invokevirtual org/xbill/DNS/Mnemonic/getValue(Ljava/lang/String;)I
ireturn
.limit locals 1
.limit stack 2
.end method
