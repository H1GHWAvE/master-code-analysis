.bytecode 50.0
.class public synchronized org/xbill/DNS/SRVRecord
.super org/xbill/DNS/Record

.field private static final 'serialVersionUID' J = -3886460132387522052L


.field private 'port' I

.field private 'priority' I

.field private 'target' Lorg/xbill/DNS/Name;

.field private 'weight' I

.method <init>()V
aload 0
invokespecial org/xbill/DNS/Record/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public <init>(Lorg/xbill/DNS/Name;IJIIILorg/xbill/DNS/Name;)V
aload 0
aload 1
bipush 33
iload 2
lload 3
invokespecial org/xbill/DNS/Record/<init>(Lorg/xbill/DNS/Name;IIJ)V
aload 0
ldc "priority"
iload 5
invokestatic org/xbill/DNS/SRVRecord/checkU16(Ljava/lang/String;I)I
putfield org/xbill/DNS/SRVRecord/priority I
aload 0
ldc "weight"
iload 6
invokestatic org/xbill/DNS/SRVRecord/checkU16(Ljava/lang/String;I)I
putfield org/xbill/DNS/SRVRecord/weight I
aload 0
ldc "port"
iload 7
invokestatic org/xbill/DNS/SRVRecord/checkU16(Ljava/lang/String;I)I
putfield org/xbill/DNS/SRVRecord/port I
aload 0
ldc "target"
aload 8
invokestatic org/xbill/DNS/SRVRecord/checkName(Ljava/lang/String;Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/Name;
putfield org/xbill/DNS/SRVRecord/target Lorg/xbill/DNS/Name;
return
.limit locals 9
.limit stack 6
.end method

.method public getAdditionalName()Lorg/xbill/DNS/Name;
aload 0
getfield org/xbill/DNS/SRVRecord/target Lorg/xbill/DNS/Name;
areturn
.limit locals 1
.limit stack 1
.end method

.method getObject()Lorg/xbill/DNS/Record;
new org/xbill/DNS/SRVRecord
dup
invokespecial org/xbill/DNS/SRVRecord/<init>()V
areturn
.limit locals 1
.limit stack 2
.end method

.method public getPort()I
aload 0
getfield org/xbill/DNS/SRVRecord/port I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getPriority()I
aload 0
getfield org/xbill/DNS/SRVRecord/priority I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getTarget()Lorg/xbill/DNS/Name;
aload 0
getfield org/xbill/DNS/SRVRecord/target Lorg/xbill/DNS/Name;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getWeight()I
aload 0
getfield org/xbill/DNS/SRVRecord/weight I
ireturn
.limit locals 1
.limit stack 1
.end method

.method rdataFromString(Lorg/xbill/DNS/Tokenizer;Lorg/xbill/DNS/Name;)V
aload 0
aload 1
invokevirtual org/xbill/DNS/Tokenizer/getUInt16()I
putfield org/xbill/DNS/SRVRecord/priority I
aload 0
aload 1
invokevirtual org/xbill/DNS/Tokenizer/getUInt16()I
putfield org/xbill/DNS/SRVRecord/weight I
aload 0
aload 1
invokevirtual org/xbill/DNS/Tokenizer/getUInt16()I
putfield org/xbill/DNS/SRVRecord/port I
aload 0
aload 1
aload 2
invokevirtual org/xbill/DNS/Tokenizer/getName(Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/Name;
putfield org/xbill/DNS/SRVRecord/target Lorg/xbill/DNS/Name;
return
.limit locals 3
.limit stack 3
.end method

.method rrFromWire(Lorg/xbill/DNS/DNSInput;)V
aload 0
aload 1
invokevirtual org/xbill/DNS/DNSInput/readU16()I
putfield org/xbill/DNS/SRVRecord/priority I
aload 0
aload 1
invokevirtual org/xbill/DNS/DNSInput/readU16()I
putfield org/xbill/DNS/SRVRecord/weight I
aload 0
aload 1
invokevirtual org/xbill/DNS/DNSInput/readU16()I
putfield org/xbill/DNS/SRVRecord/port I
aload 0
new org/xbill/DNS/Name
dup
aload 1
invokespecial org/xbill/DNS/Name/<init>(Lorg/xbill/DNS/DNSInput;)V
putfield org/xbill/DNS/SRVRecord/target Lorg/xbill/DNS/Name;
return
.limit locals 2
.limit stack 4
.end method

.method rrToString()Ljava/lang/String;
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
astore 1
aload 1
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
aload 0
getfield org/xbill/DNS/SRVRecord/priority I
invokevirtual java/lang/StringBuffer/append(I)Ljava/lang/StringBuffer;
ldc " "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
aload 0
getfield org/xbill/DNS/SRVRecord/weight I
invokevirtual java/lang/StringBuffer/append(I)Ljava/lang/StringBuffer;
ldc " "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
aload 0
getfield org/xbill/DNS/SRVRecord/port I
invokevirtual java/lang/StringBuffer/append(I)Ljava/lang/StringBuffer;
ldc " "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
aload 0
getfield org/xbill/DNS/SRVRecord/target Lorg/xbill/DNS/Name;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/Object;)Ljava/lang/StringBuffer;
pop
aload 1
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
areturn
.limit locals 2
.limit stack 3
.end method

.method rrToWire(Lorg/xbill/DNS/DNSOutput;Lorg/xbill/DNS/Compression;Z)V
aload 1
aload 0
getfield org/xbill/DNS/SRVRecord/priority I
invokevirtual org/xbill/DNS/DNSOutput/writeU16(I)V
aload 1
aload 0
getfield org/xbill/DNS/SRVRecord/weight I
invokevirtual org/xbill/DNS/DNSOutput/writeU16(I)V
aload 1
aload 0
getfield org/xbill/DNS/SRVRecord/port I
invokevirtual org/xbill/DNS/DNSOutput/writeU16(I)V
aload 0
getfield org/xbill/DNS/SRVRecord/target Lorg/xbill/DNS/Name;
aload 1
aconst_null
iload 3
invokevirtual org/xbill/DNS/Name/toWire(Lorg/xbill/DNS/DNSOutput;Lorg/xbill/DNS/Compression;Z)V
return
.limit locals 4
.limit stack 4
.end method
