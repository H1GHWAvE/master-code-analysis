.bytecode 50.0
.class public synchronized org/xbill/DNS/DNSOutput
.super java/lang/Object

.field private 'array' [B

.field private 'pos' I

.field private 'saved_pos' I

.method public <init>()V
aload 0
bipush 32
invokespecial org/xbill/DNS/DNSOutput/<init>(I)V
return
.limit locals 1
.limit stack 2
.end method

.method public <init>(I)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
iload 1
newarray byte
putfield org/xbill/DNS/DNSOutput/array [B
aload 0
iconst_0
putfield org/xbill/DNS/DNSOutput/pos I
aload 0
iconst_m1
putfield org/xbill/DNS/DNSOutput/saved_pos I
return
.limit locals 2
.limit stack 2
.end method

.method private check(JI)V
lload 1
lconst_0
lcmp
iflt L0
lload 1
lconst_1
iload 3
lshl
lcmp
ifle L1
L0:
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
lload 1
invokevirtual java/lang/StringBuffer/append(J)Ljava/lang/StringBuffer;
ldc " out of range for "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
iload 3
invokevirtual java/lang/StringBuffer/append(I)Ljava/lang/StringBuffer;
ldc " bit value"
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L1:
return
.limit locals 4
.limit stack 5
.end method

.method private need(I)V
aload 0
getfield org/xbill/DNS/DNSOutput/array [B
arraylength
aload 0
getfield org/xbill/DNS/DNSOutput/pos I
isub
iload 1
if_icmplt L0
return
L0:
aload 0
getfield org/xbill/DNS/DNSOutput/array [B
arraylength
iconst_2
imul
istore 3
iload 3
istore 2
iload 3
aload 0
getfield org/xbill/DNS/DNSOutput/pos I
iload 1
iadd
if_icmpge L1
aload 0
getfield org/xbill/DNS/DNSOutput/pos I
iload 1
iadd
istore 2
L1:
iload 2
newarray byte
astore 4
aload 0
getfield org/xbill/DNS/DNSOutput/array [B
iconst_0
aload 4
iconst_0
aload 0
getfield org/xbill/DNS/DNSOutput/pos I
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 0
aload 4
putfield org/xbill/DNS/DNSOutput/array [B
return
.limit locals 5
.limit stack 5
.end method

.method public current()I
aload 0
getfield org/xbill/DNS/DNSOutput/pos I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public jump(I)V
iload 1
aload 0
getfield org/xbill/DNS/DNSOutput/pos I
if_icmple L0
new java/lang/IllegalArgumentException
dup
ldc "cannot jump past end of data"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
iload 1
putfield org/xbill/DNS/DNSOutput/pos I
return
.limit locals 2
.limit stack 3
.end method

.method public restore()V
aload 0
getfield org/xbill/DNS/DNSOutput/saved_pos I
ifge L0
new java/lang/IllegalStateException
dup
ldc "no previous state"
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
aload 0
getfield org/xbill/DNS/DNSOutput/saved_pos I
putfield org/xbill/DNS/DNSOutput/pos I
aload 0
iconst_m1
putfield org/xbill/DNS/DNSOutput/saved_pos I
return
.limit locals 1
.limit stack 3
.end method

.method public save()V
aload 0
aload 0
getfield org/xbill/DNS/DNSOutput/pos I
putfield org/xbill/DNS/DNSOutput/saved_pos I
return
.limit locals 1
.limit stack 2
.end method

.method public toByteArray()[B
aload 0
getfield org/xbill/DNS/DNSOutput/pos I
newarray byte
astore 1
aload 0
getfield org/xbill/DNS/DNSOutput/array [B
iconst_0
aload 1
iconst_0
aload 0
getfield org/xbill/DNS/DNSOutput/pos I
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 1
areturn
.limit locals 2
.limit stack 5
.end method

.method public writeByteArray([B)V
aload 0
aload 1
iconst_0
aload 1
arraylength
invokevirtual org/xbill/DNS/DNSOutput/writeByteArray([BII)V
return
.limit locals 2
.limit stack 4
.end method

.method public writeByteArray([BII)V
aload 0
iload 3
invokespecial org/xbill/DNS/DNSOutput/need(I)V
aload 1
iload 2
aload 0
getfield org/xbill/DNS/DNSOutput/array [B
aload 0
getfield org/xbill/DNS/DNSOutput/pos I
iload 3
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 0
aload 0
getfield org/xbill/DNS/DNSOutput/pos I
iload 3
iadd
putfield org/xbill/DNS/DNSOutput/pos I
return
.limit locals 4
.limit stack 5
.end method

.method public writeCountedString([B)V
aload 1
arraylength
sipush 255
if_icmple L0
new java/lang/IllegalArgumentException
dup
ldc "Invalid counted string"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
aload 1
arraylength
iconst_1
iadd
invokespecial org/xbill/DNS/DNSOutput/need(I)V
aload 0
getfield org/xbill/DNS/DNSOutput/array [B
astore 3
aload 0
getfield org/xbill/DNS/DNSOutput/pos I
istore 2
aload 0
iload 2
iconst_1
iadd
putfield org/xbill/DNS/DNSOutput/pos I
aload 3
iload 2
aload 1
arraylength
sipush 255
iand
i2b
bastore
aload 0
aload 1
iconst_0
aload 1
arraylength
invokevirtual org/xbill/DNS/DNSOutput/writeByteArray([BII)V
return
.limit locals 4
.limit stack 4
.end method

.method public writeU16(I)V
aload 0
iload 1
i2l
bipush 16
invokespecial org/xbill/DNS/DNSOutput/check(JI)V
aload 0
iconst_2
invokespecial org/xbill/DNS/DNSOutput/need(I)V
aload 0
getfield org/xbill/DNS/DNSOutput/array [B
astore 3
aload 0
getfield org/xbill/DNS/DNSOutput/pos I
istore 2
aload 0
iload 2
iconst_1
iadd
putfield org/xbill/DNS/DNSOutput/pos I
aload 3
iload 2
iload 1
bipush 8
iushr
sipush 255
iand
i2b
bastore
aload 0
getfield org/xbill/DNS/DNSOutput/array [B
astore 3
aload 0
getfield org/xbill/DNS/DNSOutput/pos I
istore 2
aload 0
iload 2
iconst_1
iadd
putfield org/xbill/DNS/DNSOutput/pos I
aload 3
iload 2
iload 1
sipush 255
iand
i2b
bastore
return
.limit locals 4
.limit stack 4
.end method

.method public writeU16At(II)V
aload 0
iload 1
i2l
bipush 16
invokespecial org/xbill/DNS/DNSOutput/check(JI)V
iload 2
aload 0
getfield org/xbill/DNS/DNSOutput/pos I
iconst_2
isub
if_icmple L0
new java/lang/IllegalArgumentException
dup
ldc "cannot write past end of data"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
getfield org/xbill/DNS/DNSOutput/array [B
iload 2
iload 1
bipush 8
iushr
sipush 255
iand
i2b
bastore
aload 0
getfield org/xbill/DNS/DNSOutput/array [B
iload 2
iconst_1
iadd
iload 1
sipush 255
iand
i2b
bastore
return
.limit locals 3
.limit stack 4
.end method

.method public writeU32(J)V
aload 0
lload 1
bipush 32
invokespecial org/xbill/DNS/DNSOutput/check(JI)V
aload 0
iconst_4
invokespecial org/xbill/DNS/DNSOutput/need(I)V
aload 0
getfield org/xbill/DNS/DNSOutput/array [B
astore 4
aload 0
getfield org/xbill/DNS/DNSOutput/pos I
istore 3
aload 0
iload 3
iconst_1
iadd
putfield org/xbill/DNS/DNSOutput/pos I
aload 4
iload 3
lload 1
bipush 24
lushr
ldc2_w 255L
land
l2i
i2b
bastore
aload 0
getfield org/xbill/DNS/DNSOutput/array [B
astore 4
aload 0
getfield org/xbill/DNS/DNSOutput/pos I
istore 3
aload 0
iload 3
iconst_1
iadd
putfield org/xbill/DNS/DNSOutput/pos I
aload 4
iload 3
lload 1
bipush 16
lushr
ldc2_w 255L
land
l2i
i2b
bastore
aload 0
getfield org/xbill/DNS/DNSOutput/array [B
astore 4
aload 0
getfield org/xbill/DNS/DNSOutput/pos I
istore 3
aload 0
iload 3
iconst_1
iadd
putfield org/xbill/DNS/DNSOutput/pos I
aload 4
iload 3
lload 1
bipush 8
lushr
ldc2_w 255L
land
l2i
i2b
bastore
aload 0
getfield org/xbill/DNS/DNSOutput/array [B
astore 4
aload 0
getfield org/xbill/DNS/DNSOutput/pos I
istore 3
aload 0
iload 3
iconst_1
iadd
putfield org/xbill/DNS/DNSOutput/pos I
aload 4
iload 3
lload 1
ldc2_w 255L
land
l2i
i2b
bastore
return
.limit locals 5
.limit stack 6
.end method

.method public writeU8(I)V
aload 0
iload 1
i2l
bipush 8
invokespecial org/xbill/DNS/DNSOutput/check(JI)V
aload 0
iconst_1
invokespecial org/xbill/DNS/DNSOutput/need(I)V
aload 0
getfield org/xbill/DNS/DNSOutput/array [B
astore 3
aload 0
getfield org/xbill/DNS/DNSOutput/pos I
istore 2
aload 0
iload 2
iconst_1
iadd
putfield org/xbill/DNS/DNSOutput/pos I
aload 3
iload 2
iload 1
sipush 255
iand
i2b
bastore
return
.limit locals 4
.limit stack 4
.end method
