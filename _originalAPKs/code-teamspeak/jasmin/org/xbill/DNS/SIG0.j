.bytecode 50.0
.class public synchronized org/xbill/DNS/SIG0
.super java/lang/Object

.field private static final 'VALIDITY' S = 300


.method private <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static signMessage(Lorg/xbill/DNS/Message;Lorg/xbill/DNS/KEYRecord;Ljava/security/PrivateKey;Lorg/xbill/DNS/SIGRecord;)V
ldc "sig0validity"
invokestatic org/xbill/DNS/Options/intValue(Ljava/lang/String;)I
istore 5
iload 5
istore 4
iload 5
ifge L0
sipush 300
istore 4
L0:
invokestatic java/lang/System/currentTimeMillis()J
lstore 6
aload 0
aload 0
aload 3
aload 1
aload 2
new java/util/Date
dup
lload 6
invokespecial java/util/Date/<init>(J)V
new java/util/Date
dup
iload 4
sipush 1000
imul
i2l
lload 6
ladd
invokespecial java/util/Date/<init>(J)V
invokestatic org/xbill/DNS/DNSSEC/signMessage(Lorg/xbill/DNS/Message;Lorg/xbill/DNS/SIGRecord;Lorg/xbill/DNS/KEYRecord;Ljava/security/PrivateKey;Ljava/util/Date;Ljava/util/Date;)Lorg/xbill/DNS/SIGRecord;
iconst_3
invokevirtual org/xbill/DNS/Message/addRecord(Lorg/xbill/DNS/Record;I)V
return
.limit locals 8
.limit stack 12
.end method

.method public static verifyMessage(Lorg/xbill/DNS/Message;[BLorg/xbill/DNS/KEYRecord;Lorg/xbill/DNS/SIGRecord;)V
aload 0
iconst_3
invokevirtual org/xbill/DNS/Message/getSectionArray(I)[Lorg/xbill/DNS/Record;
astore 5
iconst_0
istore 4
L0:
iload 4
aload 5
arraylength
if_icmpge L1
aload 5
iload 4
aaload
invokevirtual org/xbill/DNS/Record/getType()I
bipush 24
if_icmpne L2
aload 5
iload 4
aaload
checkcast org/xbill/DNS/SIGRecord
invokevirtual org/xbill/DNS/SIGRecord/getTypeCovered()I
ifne L2
aload 5
iload 4
aaload
checkcast org/xbill/DNS/SIGRecord
astore 5
L3:
aload 0
aload 1
aload 5
aload 3
aload 2
invokestatic org/xbill/DNS/DNSSEC/verifyMessage(Lorg/xbill/DNS/Message;[BLorg/xbill/DNS/SIGRecord;Lorg/xbill/DNS/SIGRecord;Lorg/xbill/DNS/KEYRecord;)V
return
L2:
iload 4
iconst_1
iadd
istore 4
goto L0
L1:
aconst_null
astore 5
goto L3
.limit locals 6
.limit stack 5
.end method
