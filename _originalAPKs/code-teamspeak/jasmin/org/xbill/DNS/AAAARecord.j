.bytecode 50.0
.class public synchronized org/xbill/DNS/AAAARecord
.super org/xbill/DNS/Record

.field private static final 'serialVersionUID' J = -4588601512069748050L


.field private 'address' [B

.method <init>()V
aload 0
invokespecial org/xbill/DNS/Record/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public <init>(Lorg/xbill/DNS/Name;IJLjava/net/InetAddress;)V
aload 0
aload 1
bipush 28
iload 2
lload 3
invokespecial org/xbill/DNS/Record/<init>(Lorg/xbill/DNS/Name;IIJ)V
aload 5
invokestatic org/xbill/DNS/Address/familyOf(Ljava/net/InetAddress;)I
iconst_2
if_icmpeq L0
new java/lang/IllegalArgumentException
dup
ldc "invalid IPv6 address"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
aload 5
invokevirtual java/net/InetAddress/getAddress()[B
putfield org/xbill/DNS/AAAARecord/address [B
return
.limit locals 6
.limit stack 6
.end method

.method public getAddress()Ljava/net/InetAddress;
.catch java/net/UnknownHostException from L0 to L1 using L2
.catch java/net/UnknownHostException from L1 to L3 using L2
L0:
aload 0
getfield org/xbill/DNS/AAAARecord/name Lorg/xbill/DNS/Name;
ifnonnull L1
aload 0
getfield org/xbill/DNS/AAAARecord/address [B
invokestatic java/net/InetAddress/getByAddress([B)Ljava/net/InetAddress;
areturn
L1:
aload 0
getfield org/xbill/DNS/AAAARecord/name Lorg/xbill/DNS/Name;
invokevirtual org/xbill/DNS/Name/toString()Ljava/lang/String;
aload 0
getfield org/xbill/DNS/AAAARecord/address [B
invokestatic java/net/InetAddress/getByAddress(Ljava/lang/String;[B)Ljava/net/InetAddress;
astore 1
L3:
aload 1
areturn
L2:
astore 1
aconst_null
areturn
.limit locals 2
.limit stack 2
.end method

.method getObject()Lorg/xbill/DNS/Record;
new org/xbill/DNS/AAAARecord
dup
invokespecial org/xbill/DNS/AAAARecord/<init>()V
areturn
.limit locals 1
.limit stack 2
.end method

.method rdataFromString(Lorg/xbill/DNS/Tokenizer;Lorg/xbill/DNS/Name;)V
aload 0
aload 1
iconst_2
invokevirtual org/xbill/DNS/Tokenizer/getAddressBytes(I)[B
putfield org/xbill/DNS/AAAARecord/address [B
return
.limit locals 3
.limit stack 3
.end method

.method rrFromWire(Lorg/xbill/DNS/DNSInput;)V
aload 0
aload 1
bipush 16
invokevirtual org/xbill/DNS/DNSInput/readByteArray(I)[B
putfield org/xbill/DNS/AAAARecord/address [B
return
.limit locals 2
.limit stack 3
.end method

.method rrToString()Ljava/lang/String;
.catch java/net/UnknownHostException from L0 to L1 using L2
L0:
aconst_null
aload 0
getfield org/xbill/DNS/AAAARecord/address [B
invokestatic java/net/InetAddress/getByAddress(Ljava/lang/String;[B)Ljava/net/InetAddress;
astore 5
L1:
aload 5
invokevirtual java/net/InetAddress/getAddress()[B
arraylength
iconst_4
if_icmpne L3
new java/lang/StringBuffer
dup
ldc "0:0:0:0:0:ffff:"
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
astore 5
aload 0
getfield org/xbill/DNS/AAAARecord/address [B
bipush 12
baload
istore 1
aload 0
getfield org/xbill/DNS/AAAARecord/address [B
bipush 13
baload
istore 2
aload 0
getfield org/xbill/DNS/AAAARecord/address [B
bipush 14
baload
istore 3
aload 0
getfield org/xbill/DNS/AAAARecord/address [B
bipush 15
baload
istore 4
aload 5
iload 1
sipush 255
iand
bipush 8
ishl
iload 2
sipush 255
iand
iadd
invokestatic java/lang/Integer/toHexString(I)Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 5
bipush 58
invokevirtual java/lang/StringBuffer/append(C)Ljava/lang/StringBuffer;
pop
aload 5
iload 3
sipush 255
iand
bipush 8
ishl
iload 4
sipush 255
iand
iadd
invokestatic java/lang/Integer/toHexString(I)Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 5
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
areturn
L3:
aload 5
invokevirtual java/net/InetAddress/getHostAddress()Ljava/lang/String;
areturn
L2:
astore 5
aconst_null
areturn
.limit locals 6
.limit stack 4
.end method

.method rrToWire(Lorg/xbill/DNS/DNSOutput;Lorg/xbill/DNS/Compression;Z)V
aload 1
aload 0
getfield org/xbill/DNS/AAAARecord/address [B
invokevirtual org/xbill/DNS/DNSOutput/writeByteArray([B)V
return
.limit locals 4
.limit stack 2
.end method
