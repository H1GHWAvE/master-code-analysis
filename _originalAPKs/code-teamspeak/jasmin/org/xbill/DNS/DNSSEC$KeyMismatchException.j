.bytecode 50.0
.class public synchronized org/xbill/DNS/DNSSEC$KeyMismatchException
.super org/xbill/DNS/DNSSEC$DNSSECException

.field private 'key' Lorg/xbill/DNS/KEYBase;

.field private 'sig' Lorg/xbill/DNS/SIGBase;

.method <init>(Lorg/xbill/DNS/KEYBase;Lorg/xbill/DNS/SIGBase;)V
aload 0
new java/lang/StringBuffer
dup
ldc "key "
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
aload 1
invokevirtual org/xbill/DNS/KEYBase/getName()Lorg/xbill/DNS/Name;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/Object;)Ljava/lang/StringBuffer;
ldc "/"
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
aload 1
invokevirtual org/xbill/DNS/KEYBase/getAlgorithm()I
invokestatic org/xbill/DNS/DNSSEC$Algorithm/string(I)Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
ldc "/"
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
aload 1
invokevirtual org/xbill/DNS/KEYBase/getFootprint()I
invokevirtual java/lang/StringBuffer/append(I)Ljava/lang/StringBuffer;
ldc " does not match signature "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
aload 2
invokevirtual org/xbill/DNS/SIGBase/getSigner()Lorg/xbill/DNS/Name;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/Object;)Ljava/lang/StringBuffer;
ldc "/"
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
aload 2
invokevirtual org/xbill/DNS/SIGBase/getAlgorithm()I
invokestatic org/xbill/DNS/DNSSEC$Algorithm/string(I)Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
ldc "/"
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
aload 2
invokevirtual org/xbill/DNS/SIGBase/getFootprint()I
invokevirtual java/lang/StringBuffer/append(I)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokespecial org/xbill/DNS/DNSSEC$DNSSECException/<init>(Ljava/lang/String;)V
return
.limit locals 3
.limit stack 4
.end method
