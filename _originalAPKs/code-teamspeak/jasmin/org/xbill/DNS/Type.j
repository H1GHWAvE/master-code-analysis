.bytecode 50.0
.class public final synchronized org/xbill/DNS/Type
.super java/lang/Object

.field public static final 'A' I = 1


.field public static final 'A6' I = 38


.field public static final 'AAAA' I = 28


.field public static final 'AFSDB' I = 18


.field public static final 'ANY' I = 255


.field public static final 'APL' I = 42


.field public static final 'ATMA' I = 34


.field public static final 'AXFR' I = 252


.field public static final 'CERT' I = 37


.field public static final 'CNAME' I = 5


.field public static final 'DHCID' I = 49


.field public static final 'DLV' I = 32769


.field public static final 'DNAME' I = 39


.field public static final 'DNSKEY' I = 48


.field public static final 'DS' I = 43


.field public static final 'EID' I = 31


.field public static final 'GPOS' I = 27


.field public static final 'HINFO' I = 13


.field public static final 'IPSECKEY' I = 45


.field public static final 'ISDN' I = 20


.field public static final 'IXFR' I = 251


.field public static final 'KEY' I = 25


.field public static final 'KX' I = 36


.field public static final 'LOC' I = 29


.field public static final 'MAILA' I = 254


.field public static final 'MAILB' I = 253


.field public static final 'MB' I = 7


.field public static final 'MD' I = 3


.field public static final 'MF' I = 4


.field public static final 'MG' I = 8


.field public static final 'MINFO' I = 14


.field public static final 'MR' I = 9


.field public static final 'MX' I = 15


.field public static final 'NAPTR' I = 35


.field public static final 'NIMLOC' I = 32


.field public static final 'NS' I = 2


.field public static final 'NSAP' I = 22


.field public static final 'NSAP_PTR' I = 23


.field public static final 'NSEC' I = 47


.field public static final 'NSEC3' I = 50


.field public static final 'NSEC3PARAM' I = 51


.field public static final 'NULL' I = 10


.field public static final 'NXT' I = 30


.field public static final 'OPT' I = 41


.field public static final 'PTR' I = 12


.field public static final 'PX' I = 26


.field public static final 'RP' I = 17


.field public static final 'RRSIG' I = 46


.field public static final 'RT' I = 21


.field public static final 'SIG' I = 24


.field public static final 'SOA' I = 6


.field public static final 'SPF' I = 99


.field public static final 'SRV' I = 33


.field public static final 'SSHFP' I = 44


.field public static final 'TKEY' I = 249


.field public static final 'TLSA' I = 52


.field public static final 'TSIG' I = 250


.field public static final 'TXT' I = 16


.field public static final 'URI' I = 256


.field public static final 'WKS' I = 11


.field public static final 'X25' I = 19


.field private static 'types' Lorg/xbill/DNS/Type$TypeMnemonic;

.method static <clinit>()V
new org/xbill/DNS/Type$TypeMnemonic
dup
invokespecial org/xbill/DNS/Type$TypeMnemonic/<init>()V
astore 0
aload 0
putstatic org/xbill/DNS/Type/types Lorg/xbill/DNS/Type$TypeMnemonic;
aload 0
iconst_1
ldc "A"
new org/xbill/DNS/ARecord
dup
invokespecial org/xbill/DNS/ARecord/<init>()V
invokevirtual org/xbill/DNS/Type$TypeMnemonic/add(ILjava/lang/String;Lorg/xbill/DNS/Record;)V
getstatic org/xbill/DNS/Type/types Lorg/xbill/DNS/Type$TypeMnemonic;
iconst_2
ldc "NS"
new org/xbill/DNS/NSRecord
dup
invokespecial org/xbill/DNS/NSRecord/<init>()V
invokevirtual org/xbill/DNS/Type$TypeMnemonic/add(ILjava/lang/String;Lorg/xbill/DNS/Record;)V
getstatic org/xbill/DNS/Type/types Lorg/xbill/DNS/Type$TypeMnemonic;
iconst_3
ldc "MD"
new org/xbill/DNS/MDRecord
dup
invokespecial org/xbill/DNS/MDRecord/<init>()V
invokevirtual org/xbill/DNS/Type$TypeMnemonic/add(ILjava/lang/String;Lorg/xbill/DNS/Record;)V
getstatic org/xbill/DNS/Type/types Lorg/xbill/DNS/Type$TypeMnemonic;
iconst_4
ldc "MF"
new org/xbill/DNS/MFRecord
dup
invokespecial org/xbill/DNS/MFRecord/<init>()V
invokevirtual org/xbill/DNS/Type$TypeMnemonic/add(ILjava/lang/String;Lorg/xbill/DNS/Record;)V
getstatic org/xbill/DNS/Type/types Lorg/xbill/DNS/Type$TypeMnemonic;
iconst_5
ldc "CNAME"
new org/xbill/DNS/CNAMERecord
dup
invokespecial org/xbill/DNS/CNAMERecord/<init>()V
invokevirtual org/xbill/DNS/Type$TypeMnemonic/add(ILjava/lang/String;Lorg/xbill/DNS/Record;)V
getstatic org/xbill/DNS/Type/types Lorg/xbill/DNS/Type$TypeMnemonic;
bipush 6
ldc "SOA"
new org/xbill/DNS/SOARecord
dup
invokespecial org/xbill/DNS/SOARecord/<init>()V
invokevirtual org/xbill/DNS/Type$TypeMnemonic/add(ILjava/lang/String;Lorg/xbill/DNS/Record;)V
getstatic org/xbill/DNS/Type/types Lorg/xbill/DNS/Type$TypeMnemonic;
bipush 7
ldc "MB"
new org/xbill/DNS/MBRecord
dup
invokespecial org/xbill/DNS/MBRecord/<init>()V
invokevirtual org/xbill/DNS/Type$TypeMnemonic/add(ILjava/lang/String;Lorg/xbill/DNS/Record;)V
getstatic org/xbill/DNS/Type/types Lorg/xbill/DNS/Type$TypeMnemonic;
bipush 8
ldc "MG"
new org/xbill/DNS/MGRecord
dup
invokespecial org/xbill/DNS/MGRecord/<init>()V
invokevirtual org/xbill/DNS/Type$TypeMnemonic/add(ILjava/lang/String;Lorg/xbill/DNS/Record;)V
getstatic org/xbill/DNS/Type/types Lorg/xbill/DNS/Type$TypeMnemonic;
bipush 9
ldc "MR"
new org/xbill/DNS/MRRecord
dup
invokespecial org/xbill/DNS/MRRecord/<init>()V
invokevirtual org/xbill/DNS/Type$TypeMnemonic/add(ILjava/lang/String;Lorg/xbill/DNS/Record;)V
getstatic org/xbill/DNS/Type/types Lorg/xbill/DNS/Type$TypeMnemonic;
bipush 10
ldc "NULL"
new org/xbill/DNS/NULLRecord
dup
invokespecial org/xbill/DNS/NULLRecord/<init>()V
invokevirtual org/xbill/DNS/Type$TypeMnemonic/add(ILjava/lang/String;Lorg/xbill/DNS/Record;)V
getstatic org/xbill/DNS/Type/types Lorg/xbill/DNS/Type$TypeMnemonic;
bipush 11
ldc "WKS"
new org/xbill/DNS/WKSRecord
dup
invokespecial org/xbill/DNS/WKSRecord/<init>()V
invokevirtual org/xbill/DNS/Type$TypeMnemonic/add(ILjava/lang/String;Lorg/xbill/DNS/Record;)V
getstatic org/xbill/DNS/Type/types Lorg/xbill/DNS/Type$TypeMnemonic;
bipush 12
ldc "PTR"
new org/xbill/DNS/PTRRecord
dup
invokespecial org/xbill/DNS/PTRRecord/<init>()V
invokevirtual org/xbill/DNS/Type$TypeMnemonic/add(ILjava/lang/String;Lorg/xbill/DNS/Record;)V
getstatic org/xbill/DNS/Type/types Lorg/xbill/DNS/Type$TypeMnemonic;
bipush 13
ldc "HINFO"
new org/xbill/DNS/HINFORecord
dup
invokespecial org/xbill/DNS/HINFORecord/<init>()V
invokevirtual org/xbill/DNS/Type$TypeMnemonic/add(ILjava/lang/String;Lorg/xbill/DNS/Record;)V
getstatic org/xbill/DNS/Type/types Lorg/xbill/DNS/Type$TypeMnemonic;
bipush 14
ldc "MINFO"
new org/xbill/DNS/MINFORecord
dup
invokespecial org/xbill/DNS/MINFORecord/<init>()V
invokevirtual org/xbill/DNS/Type$TypeMnemonic/add(ILjava/lang/String;Lorg/xbill/DNS/Record;)V
getstatic org/xbill/DNS/Type/types Lorg/xbill/DNS/Type$TypeMnemonic;
bipush 15
ldc "MX"
new org/xbill/DNS/MXRecord
dup
invokespecial org/xbill/DNS/MXRecord/<init>()V
invokevirtual org/xbill/DNS/Type$TypeMnemonic/add(ILjava/lang/String;Lorg/xbill/DNS/Record;)V
getstatic org/xbill/DNS/Type/types Lorg/xbill/DNS/Type$TypeMnemonic;
bipush 16
ldc "TXT"
new org/xbill/DNS/TXTRecord
dup
invokespecial org/xbill/DNS/TXTRecord/<init>()V
invokevirtual org/xbill/DNS/Type$TypeMnemonic/add(ILjava/lang/String;Lorg/xbill/DNS/Record;)V
getstatic org/xbill/DNS/Type/types Lorg/xbill/DNS/Type$TypeMnemonic;
bipush 17
ldc "RP"
new org/xbill/DNS/RPRecord
dup
invokespecial org/xbill/DNS/RPRecord/<init>()V
invokevirtual org/xbill/DNS/Type$TypeMnemonic/add(ILjava/lang/String;Lorg/xbill/DNS/Record;)V
getstatic org/xbill/DNS/Type/types Lorg/xbill/DNS/Type$TypeMnemonic;
bipush 18
ldc "AFSDB"
new org/xbill/DNS/AFSDBRecord
dup
invokespecial org/xbill/DNS/AFSDBRecord/<init>()V
invokevirtual org/xbill/DNS/Type$TypeMnemonic/add(ILjava/lang/String;Lorg/xbill/DNS/Record;)V
getstatic org/xbill/DNS/Type/types Lorg/xbill/DNS/Type$TypeMnemonic;
bipush 19
ldc "X25"
new org/xbill/DNS/X25Record
dup
invokespecial org/xbill/DNS/X25Record/<init>()V
invokevirtual org/xbill/DNS/Type$TypeMnemonic/add(ILjava/lang/String;Lorg/xbill/DNS/Record;)V
getstatic org/xbill/DNS/Type/types Lorg/xbill/DNS/Type$TypeMnemonic;
bipush 20
ldc "ISDN"
new org/xbill/DNS/ISDNRecord
dup
invokespecial org/xbill/DNS/ISDNRecord/<init>()V
invokevirtual org/xbill/DNS/Type$TypeMnemonic/add(ILjava/lang/String;Lorg/xbill/DNS/Record;)V
getstatic org/xbill/DNS/Type/types Lorg/xbill/DNS/Type$TypeMnemonic;
bipush 21
ldc "RT"
new org/xbill/DNS/RTRecord
dup
invokespecial org/xbill/DNS/RTRecord/<init>()V
invokevirtual org/xbill/DNS/Type$TypeMnemonic/add(ILjava/lang/String;Lorg/xbill/DNS/Record;)V
getstatic org/xbill/DNS/Type/types Lorg/xbill/DNS/Type$TypeMnemonic;
bipush 22
ldc "NSAP"
new org/xbill/DNS/NSAPRecord
dup
invokespecial org/xbill/DNS/NSAPRecord/<init>()V
invokevirtual org/xbill/DNS/Type$TypeMnemonic/add(ILjava/lang/String;Lorg/xbill/DNS/Record;)V
getstatic org/xbill/DNS/Type/types Lorg/xbill/DNS/Type$TypeMnemonic;
bipush 23
ldc "NSAP-PTR"
new org/xbill/DNS/NSAP_PTRRecord
dup
invokespecial org/xbill/DNS/NSAP_PTRRecord/<init>()V
invokevirtual org/xbill/DNS/Type$TypeMnemonic/add(ILjava/lang/String;Lorg/xbill/DNS/Record;)V
getstatic org/xbill/DNS/Type/types Lorg/xbill/DNS/Type$TypeMnemonic;
bipush 24
ldc "SIG"
new org/xbill/DNS/SIGRecord
dup
invokespecial org/xbill/DNS/SIGRecord/<init>()V
invokevirtual org/xbill/DNS/Type$TypeMnemonic/add(ILjava/lang/String;Lorg/xbill/DNS/Record;)V
getstatic org/xbill/DNS/Type/types Lorg/xbill/DNS/Type$TypeMnemonic;
bipush 25
ldc "KEY"
new org/xbill/DNS/KEYRecord
dup
invokespecial org/xbill/DNS/KEYRecord/<init>()V
invokevirtual org/xbill/DNS/Type$TypeMnemonic/add(ILjava/lang/String;Lorg/xbill/DNS/Record;)V
getstatic org/xbill/DNS/Type/types Lorg/xbill/DNS/Type$TypeMnemonic;
bipush 26
ldc "PX"
new org/xbill/DNS/PXRecord
dup
invokespecial org/xbill/DNS/PXRecord/<init>()V
invokevirtual org/xbill/DNS/Type$TypeMnemonic/add(ILjava/lang/String;Lorg/xbill/DNS/Record;)V
getstatic org/xbill/DNS/Type/types Lorg/xbill/DNS/Type$TypeMnemonic;
bipush 27
ldc "GPOS"
new org/xbill/DNS/GPOSRecord
dup
invokespecial org/xbill/DNS/GPOSRecord/<init>()V
invokevirtual org/xbill/DNS/Type$TypeMnemonic/add(ILjava/lang/String;Lorg/xbill/DNS/Record;)V
getstatic org/xbill/DNS/Type/types Lorg/xbill/DNS/Type$TypeMnemonic;
bipush 28
ldc "AAAA"
new org/xbill/DNS/AAAARecord
dup
invokespecial org/xbill/DNS/AAAARecord/<init>()V
invokevirtual org/xbill/DNS/Type$TypeMnemonic/add(ILjava/lang/String;Lorg/xbill/DNS/Record;)V
getstatic org/xbill/DNS/Type/types Lorg/xbill/DNS/Type$TypeMnemonic;
bipush 29
ldc "LOC"
new org/xbill/DNS/LOCRecord
dup
invokespecial org/xbill/DNS/LOCRecord/<init>()V
invokevirtual org/xbill/DNS/Type$TypeMnemonic/add(ILjava/lang/String;Lorg/xbill/DNS/Record;)V
getstatic org/xbill/DNS/Type/types Lorg/xbill/DNS/Type$TypeMnemonic;
bipush 30
ldc "NXT"
new org/xbill/DNS/NXTRecord
dup
invokespecial org/xbill/DNS/NXTRecord/<init>()V
invokevirtual org/xbill/DNS/Type$TypeMnemonic/add(ILjava/lang/String;Lorg/xbill/DNS/Record;)V
getstatic org/xbill/DNS/Type/types Lorg/xbill/DNS/Type$TypeMnemonic;
bipush 31
ldc "EID"
invokevirtual org/xbill/DNS/Type$TypeMnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/Type/types Lorg/xbill/DNS/Type$TypeMnemonic;
bipush 32
ldc "NIMLOC"
invokevirtual org/xbill/DNS/Type$TypeMnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/Type/types Lorg/xbill/DNS/Type$TypeMnemonic;
bipush 33
ldc "SRV"
new org/xbill/DNS/SRVRecord
dup
invokespecial org/xbill/DNS/SRVRecord/<init>()V
invokevirtual org/xbill/DNS/Type$TypeMnemonic/add(ILjava/lang/String;Lorg/xbill/DNS/Record;)V
getstatic org/xbill/DNS/Type/types Lorg/xbill/DNS/Type$TypeMnemonic;
bipush 34
ldc "ATMA"
invokevirtual org/xbill/DNS/Type$TypeMnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/Type/types Lorg/xbill/DNS/Type$TypeMnemonic;
bipush 35
ldc "NAPTR"
new org/xbill/DNS/NAPTRRecord
dup
invokespecial org/xbill/DNS/NAPTRRecord/<init>()V
invokevirtual org/xbill/DNS/Type$TypeMnemonic/add(ILjava/lang/String;Lorg/xbill/DNS/Record;)V
getstatic org/xbill/DNS/Type/types Lorg/xbill/DNS/Type$TypeMnemonic;
bipush 36
ldc "KX"
new org/xbill/DNS/KXRecord
dup
invokespecial org/xbill/DNS/KXRecord/<init>()V
invokevirtual org/xbill/DNS/Type$TypeMnemonic/add(ILjava/lang/String;Lorg/xbill/DNS/Record;)V
getstatic org/xbill/DNS/Type/types Lorg/xbill/DNS/Type$TypeMnemonic;
bipush 37
ldc "CERT"
new org/xbill/DNS/CERTRecord
dup
invokespecial org/xbill/DNS/CERTRecord/<init>()V
invokevirtual org/xbill/DNS/Type$TypeMnemonic/add(ILjava/lang/String;Lorg/xbill/DNS/Record;)V
getstatic org/xbill/DNS/Type/types Lorg/xbill/DNS/Type$TypeMnemonic;
bipush 38
ldc "A6"
new org/xbill/DNS/A6Record
dup
invokespecial org/xbill/DNS/A6Record/<init>()V
invokevirtual org/xbill/DNS/Type$TypeMnemonic/add(ILjava/lang/String;Lorg/xbill/DNS/Record;)V
getstatic org/xbill/DNS/Type/types Lorg/xbill/DNS/Type$TypeMnemonic;
bipush 39
ldc "DNAME"
new org/xbill/DNS/DNAMERecord
dup
invokespecial org/xbill/DNS/DNAMERecord/<init>()V
invokevirtual org/xbill/DNS/Type$TypeMnemonic/add(ILjava/lang/String;Lorg/xbill/DNS/Record;)V
getstatic org/xbill/DNS/Type/types Lorg/xbill/DNS/Type$TypeMnemonic;
bipush 41
ldc "OPT"
new org/xbill/DNS/OPTRecord
dup
invokespecial org/xbill/DNS/OPTRecord/<init>()V
invokevirtual org/xbill/DNS/Type$TypeMnemonic/add(ILjava/lang/String;Lorg/xbill/DNS/Record;)V
getstatic org/xbill/DNS/Type/types Lorg/xbill/DNS/Type$TypeMnemonic;
bipush 42
ldc "APL"
new org/xbill/DNS/APLRecord
dup
invokespecial org/xbill/DNS/APLRecord/<init>()V
invokevirtual org/xbill/DNS/Type$TypeMnemonic/add(ILjava/lang/String;Lorg/xbill/DNS/Record;)V
getstatic org/xbill/DNS/Type/types Lorg/xbill/DNS/Type$TypeMnemonic;
bipush 43
ldc "DS"
new org/xbill/DNS/DSRecord
dup
invokespecial org/xbill/DNS/DSRecord/<init>()V
invokevirtual org/xbill/DNS/Type$TypeMnemonic/add(ILjava/lang/String;Lorg/xbill/DNS/Record;)V
getstatic org/xbill/DNS/Type/types Lorg/xbill/DNS/Type$TypeMnemonic;
bipush 44
ldc "SSHFP"
new org/xbill/DNS/SSHFPRecord
dup
invokespecial org/xbill/DNS/SSHFPRecord/<init>()V
invokevirtual org/xbill/DNS/Type$TypeMnemonic/add(ILjava/lang/String;Lorg/xbill/DNS/Record;)V
getstatic org/xbill/DNS/Type/types Lorg/xbill/DNS/Type$TypeMnemonic;
bipush 45
ldc "IPSECKEY"
new org/xbill/DNS/IPSECKEYRecord
dup
invokespecial org/xbill/DNS/IPSECKEYRecord/<init>()V
invokevirtual org/xbill/DNS/Type$TypeMnemonic/add(ILjava/lang/String;Lorg/xbill/DNS/Record;)V
getstatic org/xbill/DNS/Type/types Lorg/xbill/DNS/Type$TypeMnemonic;
bipush 46
ldc "RRSIG"
new org/xbill/DNS/RRSIGRecord
dup
invokespecial org/xbill/DNS/RRSIGRecord/<init>()V
invokevirtual org/xbill/DNS/Type$TypeMnemonic/add(ILjava/lang/String;Lorg/xbill/DNS/Record;)V
getstatic org/xbill/DNS/Type/types Lorg/xbill/DNS/Type$TypeMnemonic;
bipush 47
ldc "NSEC"
new org/xbill/DNS/NSECRecord
dup
invokespecial org/xbill/DNS/NSECRecord/<init>()V
invokevirtual org/xbill/DNS/Type$TypeMnemonic/add(ILjava/lang/String;Lorg/xbill/DNS/Record;)V
getstatic org/xbill/DNS/Type/types Lorg/xbill/DNS/Type$TypeMnemonic;
bipush 48
ldc "DNSKEY"
new org/xbill/DNS/DNSKEYRecord
dup
invokespecial org/xbill/DNS/DNSKEYRecord/<init>()V
invokevirtual org/xbill/DNS/Type$TypeMnemonic/add(ILjava/lang/String;Lorg/xbill/DNS/Record;)V
getstatic org/xbill/DNS/Type/types Lorg/xbill/DNS/Type$TypeMnemonic;
bipush 49
ldc "DHCID"
new org/xbill/DNS/DHCIDRecord
dup
invokespecial org/xbill/DNS/DHCIDRecord/<init>()V
invokevirtual org/xbill/DNS/Type$TypeMnemonic/add(ILjava/lang/String;Lorg/xbill/DNS/Record;)V
getstatic org/xbill/DNS/Type/types Lorg/xbill/DNS/Type$TypeMnemonic;
bipush 50
ldc "NSEC3"
new org/xbill/DNS/NSEC3Record
dup
invokespecial org/xbill/DNS/NSEC3Record/<init>()V
invokevirtual org/xbill/DNS/Type$TypeMnemonic/add(ILjava/lang/String;Lorg/xbill/DNS/Record;)V
getstatic org/xbill/DNS/Type/types Lorg/xbill/DNS/Type$TypeMnemonic;
bipush 51
ldc "NSEC3PARAM"
new org/xbill/DNS/NSEC3PARAMRecord
dup
invokespecial org/xbill/DNS/NSEC3PARAMRecord/<init>()V
invokevirtual org/xbill/DNS/Type$TypeMnemonic/add(ILjava/lang/String;Lorg/xbill/DNS/Record;)V
getstatic org/xbill/DNS/Type/types Lorg/xbill/DNS/Type$TypeMnemonic;
bipush 52
ldc "TLSA"
new org/xbill/DNS/TLSARecord
dup
invokespecial org/xbill/DNS/TLSARecord/<init>()V
invokevirtual org/xbill/DNS/Type$TypeMnemonic/add(ILjava/lang/String;Lorg/xbill/DNS/Record;)V
getstatic org/xbill/DNS/Type/types Lorg/xbill/DNS/Type$TypeMnemonic;
bipush 99
ldc "SPF"
new org/xbill/DNS/SPFRecord
dup
invokespecial org/xbill/DNS/SPFRecord/<init>()V
invokevirtual org/xbill/DNS/Type$TypeMnemonic/add(ILjava/lang/String;Lorg/xbill/DNS/Record;)V
getstatic org/xbill/DNS/Type/types Lorg/xbill/DNS/Type$TypeMnemonic;
sipush 249
ldc "TKEY"
new org/xbill/DNS/TKEYRecord
dup
invokespecial org/xbill/DNS/TKEYRecord/<init>()V
invokevirtual org/xbill/DNS/Type$TypeMnemonic/add(ILjava/lang/String;Lorg/xbill/DNS/Record;)V
getstatic org/xbill/DNS/Type/types Lorg/xbill/DNS/Type$TypeMnemonic;
sipush 250
ldc "TSIG"
new org/xbill/DNS/TSIGRecord
dup
invokespecial org/xbill/DNS/TSIGRecord/<init>()V
invokevirtual org/xbill/DNS/Type$TypeMnemonic/add(ILjava/lang/String;Lorg/xbill/DNS/Record;)V
getstatic org/xbill/DNS/Type/types Lorg/xbill/DNS/Type$TypeMnemonic;
sipush 251
ldc "IXFR"
invokevirtual org/xbill/DNS/Type$TypeMnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/Type/types Lorg/xbill/DNS/Type$TypeMnemonic;
sipush 252
ldc "AXFR"
invokevirtual org/xbill/DNS/Type$TypeMnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/Type/types Lorg/xbill/DNS/Type$TypeMnemonic;
sipush 253
ldc "MAILB"
invokevirtual org/xbill/DNS/Type$TypeMnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/Type/types Lorg/xbill/DNS/Type$TypeMnemonic;
sipush 254
ldc "MAILA"
invokevirtual org/xbill/DNS/Type$TypeMnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/Type/types Lorg/xbill/DNS/Type$TypeMnemonic;
sipush 255
ldc "ANY"
invokevirtual org/xbill/DNS/Type$TypeMnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/Type/types Lorg/xbill/DNS/Type$TypeMnemonic;
sipush 256
ldc "URI"
new org/xbill/DNS/URIRecord
dup
invokespecial org/xbill/DNS/URIRecord/<init>()V
invokevirtual org/xbill/DNS/Type$TypeMnemonic/add(ILjava/lang/String;Lorg/xbill/DNS/Record;)V
getstatic org/xbill/DNS/Type/types Lorg/xbill/DNS/Type$TypeMnemonic;
ldc_w 32769
ldc "DLV"
new org/xbill/DNS/DLVRecord
dup
invokespecial org/xbill/DNS/DLVRecord/<init>()V
invokevirtual org/xbill/DNS/Type$TypeMnemonic/add(ILjava/lang/String;Lorg/xbill/DNS/Record;)V
return
.limit locals 1
.limit stack 5
.end method

.method private <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static check(I)V
iload 0
iflt L0
iload 0
ldc_w 65535
if_icmple L1
L0:
new org/xbill/DNS/InvalidTypeException
dup
iload 0
invokespecial org/xbill/DNS/InvalidTypeException/<init>(I)V
athrow
L1:
return
.limit locals 1
.limit stack 3
.end method

.method static getProto(I)Lorg/xbill/DNS/Record;
getstatic org/xbill/DNS/Type/types Lorg/xbill/DNS/Type$TypeMnemonic;
iload 0
invokevirtual org/xbill/DNS/Type$TypeMnemonic/getProto(I)Lorg/xbill/DNS/Record;
areturn
.limit locals 1
.limit stack 2
.end method

.method public static isRR(I)Z
iload 0
lookupswitch
41 : L0
249 : L0
250 : L0
251 : L0
252 : L0
253 : L0
254 : L0
255 : L0
default : L1
L1:
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public static string(I)Ljava/lang/String;
getstatic org/xbill/DNS/Type/types Lorg/xbill/DNS/Type$TypeMnemonic;
iload 0
invokevirtual org/xbill/DNS/Type$TypeMnemonic/getText(I)Ljava/lang/String;
areturn
.limit locals 1
.limit stack 2
.end method

.method public static value(Ljava/lang/String;)I
aload 0
iconst_0
invokestatic org/xbill/DNS/Type/value(Ljava/lang/String;Z)I
ireturn
.limit locals 1
.limit stack 2
.end method

.method public static value(Ljava/lang/String;Z)I
getstatic org/xbill/DNS/Type/types Lorg/xbill/DNS/Type$TypeMnemonic;
aload 0
invokevirtual org/xbill/DNS/Type$TypeMnemonic/getValue(Ljava/lang/String;)I
istore 3
iload 3
istore 2
iload 3
iconst_m1
if_icmpne L0
iload 3
istore 2
iload 1
ifeq L0
getstatic org/xbill/DNS/Type/types Lorg/xbill/DNS/Type$TypeMnemonic;
new java/lang/StringBuffer
dup
ldc "TYPE"
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
aload 0
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual org/xbill/DNS/Type$TypeMnemonic/getValue(Ljava/lang/String;)I
istore 2
L0:
iload 2
ireturn
.limit locals 4
.limit stack 4
.end method
