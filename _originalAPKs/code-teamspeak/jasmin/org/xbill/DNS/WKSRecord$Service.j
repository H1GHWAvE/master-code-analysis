.bytecode 50.0
.class public synchronized org/xbill/DNS/WKSRecord$Service
.super java/lang/Object

.field public static final 'AUTH' I = 113


.field public static final 'BL_IDM' I = 142


.field public static final 'BOOTPC' I = 68


.field public static final 'BOOTPS' I = 67


.field public static final 'CHARGEN' I = 19


.field public static final 'CISCO_FNA' I = 130


.field public static final 'CISCO_SYS' I = 132


.field public static final 'CISCO_TNA' I = 131


.field public static final 'CSNET_NS' I = 105


.field public static final 'DAYTIME' I = 13


.field public static final 'DCP' I = 93


.field public static final 'DISCARD' I = 9


.field public static final 'DOMAIN' I = 53


.field public static final 'DSP' I = 33


.field public static final 'ECHO' I = 7


.field public static final 'EMFIS_CNTL' I = 141


.field public static final 'EMFIS_DATA' I = 140


.field public static final 'ERPC' I = 121


.field public static final 'FINGER' I = 79


.field public static final 'FTP' I = 21


.field public static final 'FTP_DATA' I = 20


.field public static final 'GRAPHICS' I = 41


.field public static final 'HOSTNAME' I = 101


.field public static final 'HOSTS2_NS' I = 81


.field public static final 'INGRES_NET' I = 134


.field public static final 'ISI_GL' I = 55


.field public static final 'ISO_TSAP' I = 102


.field public static final 'LA_MAINT' I = 51


.field public static final 'LINK' I = 245


.field public static final 'LOCUS_CON' I = 127


.field public static final 'LOCUS_MAP' I = 125


.field public static final 'LOC_SRV' I = 135


.field public static final 'LOGIN' I = 49


.field public static final 'METAGRAM' I = 99


.field public static final 'MIT_DOV' I = 91


.field public static final 'MPM' I = 45


.field public static final 'MPM_FLAGS' I = 44


.field public static final 'MPM_SND' I = 46


.field public static final 'MSG_AUTH' I = 31


.field public static final 'MSG_ICP' I = 29


.field public static final 'NAMESERVER' I = 42


.field public static final 'NETBIOS_DGM' I = 138


.field public static final 'NETBIOS_NS' I = 137


.field public static final 'NETBIOS_SSN' I = 139


.field public static final 'NETRJS_1' I = 71


.field public static final 'NETRJS_2' I = 72


.field public static final 'NETRJS_3' I = 73


.field public static final 'NETRJS_4' I = 74


.field public static final 'NICNAME' I = 43


.field public static final 'NI_FTP' I = 47


.field public static final 'NI_MAIL' I = 61


.field public static final 'NNTP' I = 119


.field public static final 'NSW_FE' I = 27


.field public static final 'NTP' I = 123


.field public static final 'POP_2' I = 109


.field public static final 'PROFILE' I = 136


.field public static final 'PWDGEN' I = 129


.field public static final 'QUOTE' I = 17


.field public static final 'RJE' I = 5


.field public static final 'RLP' I = 39


.field public static final 'RTELNET' I = 107


.field public static final 'SFTP' I = 115


.field public static final 'SMTP' I = 25


.field public static final 'STATSRV' I = 133


.field public static final 'SUNRPC' I = 111


.field public static final 'SUPDUP' I = 95


.field public static final 'SUR_MEAS' I = 243


.field public static final 'SU_MIT_TG' I = 89


.field public static final 'SWIFT_RVF' I = 97


.field public static final 'TACACS_DS' I = 65


.field public static final 'TACNEWS' I = 98


.field public static final 'TELNET' I = 23


.field public static final 'TFTP' I = 69


.field public static final 'TIME' I = 37


.field public static final 'USERS' I = 11


.field public static final 'UUCP_PATH' I = 117


.field public static final 'VIA_FTP' I = 63


.field public static final 'X400' I = 103


.field public static final 'X400_SND' I = 104


.field private static 'services' Lorg/xbill/DNS/Mnemonic;

.method static <clinit>()V
new org/xbill/DNS/Mnemonic
dup
ldc "TCP/UDP service"
iconst_3
invokespecial org/xbill/DNS/Mnemonic/<init>(Ljava/lang/String;I)V
astore 0
aload 0
putstatic org/xbill/DNS/WKSRecord$Service/services Lorg/xbill/DNS/Mnemonic;
aload 0
ldc_w 65535
invokevirtual org/xbill/DNS/Mnemonic/setMaximum(I)V
getstatic org/xbill/DNS/WKSRecord$Service/services Lorg/xbill/DNS/Mnemonic;
iconst_1
invokevirtual org/xbill/DNS/Mnemonic/setNumericAllowed(Z)V
getstatic org/xbill/DNS/WKSRecord$Service/services Lorg/xbill/DNS/Mnemonic;
iconst_5
ldc "rje"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Service/services Lorg/xbill/DNS/Mnemonic;
bipush 7
ldc "echo"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Service/services Lorg/xbill/DNS/Mnemonic;
bipush 9
ldc "discard"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Service/services Lorg/xbill/DNS/Mnemonic;
bipush 11
ldc "users"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Service/services Lorg/xbill/DNS/Mnemonic;
bipush 13
ldc "daytime"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Service/services Lorg/xbill/DNS/Mnemonic;
bipush 17
ldc "quote"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Service/services Lorg/xbill/DNS/Mnemonic;
bipush 19
ldc "chargen"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Service/services Lorg/xbill/DNS/Mnemonic;
bipush 20
ldc "ftp-data"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Service/services Lorg/xbill/DNS/Mnemonic;
bipush 21
ldc "ftp"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Service/services Lorg/xbill/DNS/Mnemonic;
bipush 23
ldc "telnet"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Service/services Lorg/xbill/DNS/Mnemonic;
bipush 25
ldc "smtp"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Service/services Lorg/xbill/DNS/Mnemonic;
bipush 27
ldc "nsw-fe"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Service/services Lorg/xbill/DNS/Mnemonic;
bipush 29
ldc "msg-icp"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Service/services Lorg/xbill/DNS/Mnemonic;
bipush 31
ldc "msg-auth"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Service/services Lorg/xbill/DNS/Mnemonic;
bipush 33
ldc "dsp"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Service/services Lorg/xbill/DNS/Mnemonic;
bipush 37
ldc "time"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Service/services Lorg/xbill/DNS/Mnemonic;
bipush 39
ldc "rlp"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Service/services Lorg/xbill/DNS/Mnemonic;
bipush 41
ldc "graphics"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Service/services Lorg/xbill/DNS/Mnemonic;
bipush 42
ldc "nameserver"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Service/services Lorg/xbill/DNS/Mnemonic;
bipush 43
ldc "nicname"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Service/services Lorg/xbill/DNS/Mnemonic;
bipush 44
ldc "mpm-flags"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Service/services Lorg/xbill/DNS/Mnemonic;
bipush 45
ldc "mpm"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Service/services Lorg/xbill/DNS/Mnemonic;
bipush 46
ldc "mpm-snd"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Service/services Lorg/xbill/DNS/Mnemonic;
bipush 47
ldc "ni-ftp"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Service/services Lorg/xbill/DNS/Mnemonic;
bipush 49
ldc "login"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Service/services Lorg/xbill/DNS/Mnemonic;
bipush 51
ldc "la-maint"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Service/services Lorg/xbill/DNS/Mnemonic;
bipush 53
ldc "domain"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Service/services Lorg/xbill/DNS/Mnemonic;
bipush 55
ldc "isi-gl"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Service/services Lorg/xbill/DNS/Mnemonic;
bipush 61
ldc "ni-mail"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Service/services Lorg/xbill/DNS/Mnemonic;
bipush 63
ldc "via-ftp"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Service/services Lorg/xbill/DNS/Mnemonic;
bipush 65
ldc "tacacs-ds"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Service/services Lorg/xbill/DNS/Mnemonic;
bipush 67
ldc "bootps"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Service/services Lorg/xbill/DNS/Mnemonic;
bipush 68
ldc "bootpc"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Service/services Lorg/xbill/DNS/Mnemonic;
bipush 69
ldc "tftp"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Service/services Lorg/xbill/DNS/Mnemonic;
bipush 71
ldc "netrjs-1"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Service/services Lorg/xbill/DNS/Mnemonic;
bipush 72
ldc "netrjs-2"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Service/services Lorg/xbill/DNS/Mnemonic;
bipush 73
ldc "netrjs-3"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Service/services Lorg/xbill/DNS/Mnemonic;
bipush 74
ldc "netrjs-4"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Service/services Lorg/xbill/DNS/Mnemonic;
bipush 79
ldc "finger"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Service/services Lorg/xbill/DNS/Mnemonic;
bipush 81
ldc "hosts2-ns"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Service/services Lorg/xbill/DNS/Mnemonic;
bipush 89
ldc "su-mit-tg"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Service/services Lorg/xbill/DNS/Mnemonic;
bipush 91
ldc "mit-dov"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Service/services Lorg/xbill/DNS/Mnemonic;
bipush 93
ldc "dcp"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Service/services Lorg/xbill/DNS/Mnemonic;
bipush 95
ldc "supdup"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Service/services Lorg/xbill/DNS/Mnemonic;
bipush 97
ldc "swift-rvf"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Service/services Lorg/xbill/DNS/Mnemonic;
bipush 98
ldc "tacnews"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Service/services Lorg/xbill/DNS/Mnemonic;
bipush 99
ldc "metagram"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Service/services Lorg/xbill/DNS/Mnemonic;
bipush 101
ldc "hostname"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Service/services Lorg/xbill/DNS/Mnemonic;
bipush 102
ldc "iso-tsap"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Service/services Lorg/xbill/DNS/Mnemonic;
bipush 103
ldc "x400"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Service/services Lorg/xbill/DNS/Mnemonic;
bipush 104
ldc "x400-snd"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Service/services Lorg/xbill/DNS/Mnemonic;
bipush 105
ldc "csnet-ns"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Service/services Lorg/xbill/DNS/Mnemonic;
bipush 107
ldc "rtelnet"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Service/services Lorg/xbill/DNS/Mnemonic;
bipush 109
ldc "pop-2"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Service/services Lorg/xbill/DNS/Mnemonic;
bipush 111
ldc "sunrpc"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Service/services Lorg/xbill/DNS/Mnemonic;
bipush 113
ldc "auth"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Service/services Lorg/xbill/DNS/Mnemonic;
bipush 115
ldc "sftp"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Service/services Lorg/xbill/DNS/Mnemonic;
bipush 117
ldc "uucp-path"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Service/services Lorg/xbill/DNS/Mnemonic;
bipush 119
ldc "nntp"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Service/services Lorg/xbill/DNS/Mnemonic;
bipush 121
ldc "erpc"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Service/services Lorg/xbill/DNS/Mnemonic;
bipush 123
ldc "ntp"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Service/services Lorg/xbill/DNS/Mnemonic;
bipush 125
ldc "locus-map"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Service/services Lorg/xbill/DNS/Mnemonic;
bipush 127
ldc "locus-con"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Service/services Lorg/xbill/DNS/Mnemonic;
sipush 129
ldc "pwdgen"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Service/services Lorg/xbill/DNS/Mnemonic;
sipush 130
ldc "cisco-fna"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Service/services Lorg/xbill/DNS/Mnemonic;
sipush 131
ldc "cisco-tna"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Service/services Lorg/xbill/DNS/Mnemonic;
sipush 132
ldc "cisco-sys"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Service/services Lorg/xbill/DNS/Mnemonic;
sipush 133
ldc "statsrv"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Service/services Lorg/xbill/DNS/Mnemonic;
sipush 134
ldc "ingres-net"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Service/services Lorg/xbill/DNS/Mnemonic;
sipush 135
ldc "loc-srv"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Service/services Lorg/xbill/DNS/Mnemonic;
sipush 136
ldc "profile"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Service/services Lorg/xbill/DNS/Mnemonic;
sipush 137
ldc "netbios-ns"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Service/services Lorg/xbill/DNS/Mnemonic;
sipush 138
ldc "netbios-dgm"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Service/services Lorg/xbill/DNS/Mnemonic;
sipush 139
ldc "netbios-ssn"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Service/services Lorg/xbill/DNS/Mnemonic;
sipush 140
ldc "emfis-data"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Service/services Lorg/xbill/DNS/Mnemonic;
sipush 141
ldc "emfis-cntl"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Service/services Lorg/xbill/DNS/Mnemonic;
sipush 142
ldc "bl-idm"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Service/services Lorg/xbill/DNS/Mnemonic;
sipush 243
ldc "sur-meas"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/WKSRecord$Service/services Lorg/xbill/DNS/Mnemonic;
sipush 245
ldc "link"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
return
.limit locals 1
.limit stack 4
.end method

.method private <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static string(I)Ljava/lang/String;
getstatic org/xbill/DNS/WKSRecord$Service/services Lorg/xbill/DNS/Mnemonic;
iload 0
invokevirtual org/xbill/DNS/Mnemonic/getText(I)Ljava/lang/String;
areturn
.limit locals 1
.limit stack 2
.end method

.method public static value(Ljava/lang/String;)I
getstatic org/xbill/DNS/WKSRecord$Service/services Lorg/xbill/DNS/Mnemonic;
aload 0
invokevirtual org/xbill/DNS/Mnemonic/getValue(Ljava/lang/String;)I
ireturn
.limit locals 1
.limit stack 2
.end method
