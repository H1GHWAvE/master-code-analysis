.bytecode 50.0
.class public synchronized org/xbill/DNS/APLRecord
.super org/xbill/DNS/Record

.field private static final 'serialVersionUID' J = -1348173791712935864L


.field private 'elements' Ljava/util/List;

.method <init>()V
aload 0
invokespecial org/xbill/DNS/Record/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public <init>(Lorg/xbill/DNS/Name;IJLjava/util/List;)V
aload 0
aload 1
bipush 42
iload 2
lload 3
invokespecial org/xbill/DNS/Record/<init>(Lorg/xbill/DNS/Name;IIJ)V
aload 0
new java/util/ArrayList
dup
aload 5
invokeinterface java/util/List/size()I 0
invokespecial java/util/ArrayList/<init>(I)V
putfield org/xbill/DNS/APLRecord/elements Ljava/util/List;
aload 5
invokeinterface java/util/List/iterator()Ljava/util/Iterator; 0
astore 1
L0:
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
astore 5
aload 5
instanceof org/xbill/DNS/APLRecord$Element
ifne L2
new java/lang/IllegalArgumentException
dup
ldc "illegal element"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L2:
aload 5
checkcast org/xbill/DNS/APLRecord$Element
astore 5
aload 5
getfield org/xbill/DNS/APLRecord$Element/family I
iconst_1
if_icmpeq L3
aload 5
getfield org/xbill/DNS/APLRecord$Element/family I
iconst_2
if_icmpeq L3
new java/lang/IllegalArgumentException
dup
ldc "unknown family"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L3:
aload 0
getfield org/xbill/DNS/APLRecord/elements Ljava/util/List;
aload 5
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
goto L0
L1:
return
.limit locals 6
.limit stack 6
.end method

.method static access$000(II)Z
iload 0
iload 1
invokestatic org/xbill/DNS/APLRecord/validatePrefixLength(II)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method private static addressLength([B)I
aload 0
arraylength
iconst_1
isub
istore 1
L0:
iload 1
iflt L1
aload 0
iload 1
baload
ifeq L2
iload 1
iconst_1
iadd
ireturn
L2:
iload 1
iconst_1
isub
istore 1
goto L0
L1:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method private static parseAddress([BI)[B
aload 0
arraylength
iload 1
if_icmple L0
new org/xbill/DNS/WireParseException
dup
ldc "invalid address length"
invokespecial org/xbill/DNS/WireParseException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
arraylength
iload 1
if_icmpne L1
aload 0
areturn
L1:
iload 1
newarray byte
astore 2
aload 0
iconst_0
aload 2
iconst_0
aload 0
arraylength
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 2
areturn
.limit locals 3
.limit stack 5
.end method

.method private static validatePrefixLength(II)Z
iload 1
iflt L0
iload 1
sipush 256
if_icmplt L1
L0:
iconst_0
ireturn
L1:
iload 0
iconst_1
if_icmpne L2
iload 1
bipush 32
if_icmpgt L0
L2:
iload 0
iconst_2
if_icmpne L3
iload 1
sipush 128
if_icmpgt L0
L3:
iconst_1
ireturn
.limit locals 2
.limit stack 2
.end method

.method public getElements()Ljava/util/List;
aload 0
getfield org/xbill/DNS/APLRecord/elements Ljava/util/List;
areturn
.limit locals 1
.limit stack 1
.end method

.method getObject()Lorg/xbill/DNS/Record;
new org/xbill/DNS/APLRecord
dup
invokespecial org/xbill/DNS/APLRecord/<init>()V
areturn
.limit locals 1
.limit stack 2
.end method

.method rdataFromString(Lorg/xbill/DNS/Tokenizer;Lorg/xbill/DNS/Name;)V
.catch java/lang/NumberFormatException from L0 to L1 using L2
.catch java/lang/NumberFormatException from L3 to L4 using L5
aload 0
new java/util/ArrayList
dup
iconst_1
invokespecial java/util/ArrayList/<init>(I)V
putfield org/xbill/DNS/APLRecord/elements Ljava/util/List;
L6:
aload 1
invokevirtual org/xbill/DNS/Tokenizer/get()Lorg/xbill/DNS/Tokenizer$Token;
astore 2
aload 2
invokevirtual org/xbill/DNS/Tokenizer$Token/isString()Z
ifeq L7
aload 2
getfield org/xbill/DNS/Tokenizer$Token/value Ljava/lang/String;
astore 8
aload 8
ldc "!"
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifeq L8
iconst_1
istore 3
iconst_1
istore 6
L9:
aload 8
bipush 58
iload 3
invokevirtual java/lang/String/indexOf(II)I
istore 4
iload 4
ifge L10
aload 1
ldc "invalid address prefix element"
invokevirtual org/xbill/DNS/Tokenizer/exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;
athrow
L10:
aload 8
bipush 47
iload 4
invokevirtual java/lang/String/indexOf(II)I
istore 5
iload 5
ifge L11
aload 1
ldc "invalid address prefix element"
invokevirtual org/xbill/DNS/Tokenizer/exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;
athrow
L11:
aload 8
iload 3
iload 4
invokevirtual java/lang/String/substring(II)Ljava/lang/String;
astore 7
aload 8
iload 4
iconst_1
iadd
iload 5
invokevirtual java/lang/String/substring(II)Ljava/lang/String;
astore 2
aload 8
iload 5
iconst_1
iadd
invokevirtual java/lang/String/substring(I)Ljava/lang/String;
astore 8
L0:
aload 7
invokestatic java/lang/Integer/parseInt(Ljava/lang/String;)I
istore 3
L1:
iload 3
iconst_1
if_icmpeq L3
iload 3
iconst_2
if_icmpeq L3
aload 1
ldc "unknown family"
invokevirtual org/xbill/DNS/Tokenizer/exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;
athrow
L2:
astore 2
aload 1
ldc "invalid family"
invokevirtual org/xbill/DNS/Tokenizer/exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;
athrow
L3:
aload 8
invokestatic java/lang/Integer/parseInt(Ljava/lang/String;)I
istore 4
L4:
iload 3
iload 4
invokestatic org/xbill/DNS/APLRecord/validatePrefixLength(II)Z
ifne L12
aload 1
ldc "invalid prefix length"
invokevirtual org/xbill/DNS/Tokenizer/exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;
athrow
L5:
astore 2
aload 1
ldc "invalid prefix length"
invokevirtual org/xbill/DNS/Tokenizer/exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;
athrow
L12:
aload 2
iload 3
invokestatic org/xbill/DNS/Address/toByteArray(Ljava/lang/String;I)[B
astore 7
aload 7
ifnonnull L13
aload 1
new java/lang/StringBuffer
dup
ldc "invalid IP address "
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
aload 2
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual org/xbill/DNS/Tokenizer/exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;
athrow
L13:
aload 7
invokestatic java/net/InetAddress/getByAddress([B)Ljava/net/InetAddress;
astore 2
aload 0
getfield org/xbill/DNS/APLRecord/elements Ljava/util/List;
new org/xbill/DNS/APLRecord$Element
dup
iload 6
aload 2
iload 4
invokespecial org/xbill/DNS/APLRecord$Element/<init>(ZLjava/net/InetAddress;I)V
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
goto L6
L7:
aload 1
invokevirtual org/xbill/DNS/Tokenizer/unget()V
return
L8:
iconst_0
istore 3
iconst_0
istore 6
goto L9
.limit locals 9
.limit stack 6
.end method

.method rrFromWire(Lorg/xbill/DNS/DNSInput;)V
aload 0
new java/util/ArrayList
dup
iconst_1
invokespecial java/util/ArrayList/<init>(I)V
putfield org/xbill/DNS/APLRecord/elements Ljava/util/List;
L0:
aload 1
invokevirtual org/xbill/DNS/DNSInput/remaining()I
ifeq L1
aload 1
invokevirtual org/xbill/DNS/DNSInput/readU16()I
istore 2
aload 1
invokevirtual org/xbill/DNS/DNSInput/readU8()I
istore 3
aload 1
invokevirtual org/xbill/DNS/DNSInput/readU8()I
istore 4
iload 4
sipush 128
iand
ifeq L2
iconst_1
istore 5
L3:
aload 1
iload 4
sipush -129
iand
invokevirtual org/xbill/DNS/DNSInput/readByteArray(I)[B
astore 6
iload 2
iload 3
invokestatic org/xbill/DNS/APLRecord/validatePrefixLength(II)Z
ifne L4
new org/xbill/DNS/WireParseException
dup
ldc "invalid prefix length"
invokespecial org/xbill/DNS/WireParseException/<init>(Ljava/lang/String;)V
athrow
L2:
iconst_0
istore 5
goto L3
L4:
iload 2
iconst_1
if_icmpeq L5
iload 2
iconst_2
if_icmpne L6
L5:
new org/xbill/DNS/APLRecord$Element
dup
iload 5
aload 6
iload 2
invokestatic org/xbill/DNS/Address/addressLength(I)I
invokestatic org/xbill/DNS/APLRecord/parseAddress([BI)[B
invokestatic java/net/InetAddress/getByAddress([B)Ljava/net/InetAddress;
iload 3
invokespecial org/xbill/DNS/APLRecord$Element/<init>(ZLjava/net/InetAddress;I)V
astore 6
L7:
aload 0
getfield org/xbill/DNS/APLRecord/elements Ljava/util/List;
aload 6
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
goto L0
L6:
new org/xbill/DNS/APLRecord$Element
dup
iload 2
iload 5
aload 6
iload 3
aconst_null
invokespecial org/xbill/DNS/APLRecord$Element/<init>(IZLjava/lang/Object;ILorg/xbill/DNS/APLRecord$1;)V
astore 6
goto L7
L1:
return
.limit locals 7
.limit stack 7
.end method

.method rrToString()Ljava/lang/String;
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
astore 1
aload 0
getfield org/xbill/DNS/APLRecord/elements Ljava/util/List;
invokeinterface java/util/List/iterator()Ljava/util/Iterator; 0
astore 2
L0:
aload 2
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 1
aload 2
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast org/xbill/DNS/APLRecord$Element
invokevirtual java/lang/StringBuffer/append(Ljava/lang/Object;)Ljava/lang/StringBuffer;
pop
aload 2
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L0
aload 1
ldc " "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
goto L0
L1:
aload 1
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
areturn
.limit locals 3
.limit stack 2
.end method

.method rrToWire(Lorg/xbill/DNS/DNSOutput;Lorg/xbill/DNS/Compression;Z)V
aload 0
getfield org/xbill/DNS/APLRecord/elements Ljava/util/List;
invokeinterface java/util/List/iterator()Ljava/util/Iterator; 0
astore 6
L0:
aload 6
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 6
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast org/xbill/DNS/APLRecord$Element
astore 7
aload 7
getfield org/xbill/DNS/APLRecord$Element/family I
iconst_1
if_icmpeq L2
aload 7
getfield org/xbill/DNS/APLRecord$Element/family I
iconst_2
if_icmpne L3
L2:
aload 7
getfield org/xbill/DNS/APLRecord$Element/address Ljava/lang/Object;
checkcast java/net/InetAddress
invokevirtual java/net/InetAddress/getAddress()[B
astore 2
aload 2
invokestatic org/xbill/DNS/APLRecord/addressLength([B)I
istore 4
L4:
aload 7
getfield org/xbill/DNS/APLRecord$Element/negative Z
ifeq L5
iload 4
sipush 128
ior
istore 5
L6:
aload 1
aload 7
getfield org/xbill/DNS/APLRecord$Element/family I
invokevirtual org/xbill/DNS/DNSOutput/writeU16(I)V
aload 1
aload 7
getfield org/xbill/DNS/APLRecord$Element/prefixLength I
invokevirtual org/xbill/DNS/DNSOutput/writeU8(I)V
aload 1
iload 5
invokevirtual org/xbill/DNS/DNSOutput/writeU8(I)V
aload 1
aload 2
iconst_0
iload 4
invokevirtual org/xbill/DNS/DNSOutput/writeByteArray([BII)V
goto L0
L3:
aload 7
getfield org/xbill/DNS/APLRecord$Element/address Ljava/lang/Object;
checkcast [B
checkcast [B
astore 2
aload 2
arraylength
istore 4
goto L4
L1:
return
L5:
iload 4
istore 5
goto L6
.limit locals 8
.limit stack 4
.end method
