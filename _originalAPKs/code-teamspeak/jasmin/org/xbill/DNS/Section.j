.bytecode 50.0
.class public final synchronized org/xbill/DNS/Section
.super java/lang/Object

.field public static final 'ADDITIONAL' I = 3


.field public static final 'ANSWER' I = 1


.field public static final 'AUTHORITY' I = 2


.field public static final 'PREREQ' I = 1


.field public static final 'QUESTION' I = 0


.field public static final 'UPDATE' I = 2


.field public static final 'ZONE' I = 0


.field private static 'longSections' [Ljava/lang/String;

.field private static 'sections' Lorg/xbill/DNS/Mnemonic;

.field private static 'updateSections' [Ljava/lang/String;

.method static <clinit>()V
new org/xbill/DNS/Mnemonic
dup
ldc "Message Section"
iconst_3
invokespecial org/xbill/DNS/Mnemonic/<init>(Ljava/lang/String;I)V
putstatic org/xbill/DNS/Section/sections Lorg/xbill/DNS/Mnemonic;
iconst_4
anewarray java/lang/String
putstatic org/xbill/DNS/Section/longSections [Ljava/lang/String;
iconst_4
anewarray java/lang/String
putstatic org/xbill/DNS/Section/updateSections [Ljava/lang/String;
getstatic org/xbill/DNS/Section/sections Lorg/xbill/DNS/Mnemonic;
iconst_3
invokevirtual org/xbill/DNS/Mnemonic/setMaximum(I)V
getstatic org/xbill/DNS/Section/sections Lorg/xbill/DNS/Mnemonic;
iconst_1
invokevirtual org/xbill/DNS/Mnemonic/setNumericAllowed(Z)V
getstatic org/xbill/DNS/Section/sections Lorg/xbill/DNS/Mnemonic;
iconst_0
ldc "qd"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/Section/sections Lorg/xbill/DNS/Mnemonic;
iconst_1
ldc "an"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/Section/sections Lorg/xbill/DNS/Mnemonic;
iconst_2
ldc "au"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/Section/sections Lorg/xbill/DNS/Mnemonic;
iconst_3
ldc "ad"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/Section/longSections [Ljava/lang/String;
iconst_0
ldc "QUESTIONS"
aastore
getstatic org/xbill/DNS/Section/longSections [Ljava/lang/String;
iconst_1
ldc "ANSWERS"
aastore
getstatic org/xbill/DNS/Section/longSections [Ljava/lang/String;
iconst_2
ldc "AUTHORITY RECORDS"
aastore
getstatic org/xbill/DNS/Section/longSections [Ljava/lang/String;
iconst_3
ldc "ADDITIONAL RECORDS"
aastore
getstatic org/xbill/DNS/Section/updateSections [Ljava/lang/String;
iconst_0
ldc "ZONE"
aastore
getstatic org/xbill/DNS/Section/updateSections [Ljava/lang/String;
iconst_1
ldc "PREREQUISITES"
aastore
getstatic org/xbill/DNS/Section/updateSections [Ljava/lang/String;
iconst_2
ldc "UPDATE RECORDS"
aastore
getstatic org/xbill/DNS/Section/updateSections [Ljava/lang/String;
iconst_3
ldc "ADDITIONAL RECORDS"
aastore
return
.limit locals 0
.limit stack 4
.end method

.method private <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static longString(I)Ljava/lang/String;
getstatic org/xbill/DNS/Section/sections Lorg/xbill/DNS/Mnemonic;
iload 0
invokevirtual org/xbill/DNS/Mnemonic/check(I)V
getstatic org/xbill/DNS/Section/longSections [Ljava/lang/String;
iload 0
aaload
areturn
.limit locals 1
.limit stack 2
.end method

.method public static string(I)Ljava/lang/String;
getstatic org/xbill/DNS/Section/sections Lorg/xbill/DNS/Mnemonic;
iload 0
invokevirtual org/xbill/DNS/Mnemonic/getText(I)Ljava/lang/String;
areturn
.limit locals 1
.limit stack 2
.end method

.method public static updString(I)Ljava/lang/String;
getstatic org/xbill/DNS/Section/sections Lorg/xbill/DNS/Mnemonic;
iload 0
invokevirtual org/xbill/DNS/Mnemonic/check(I)V
getstatic org/xbill/DNS/Section/updateSections [Ljava/lang/String;
iload 0
aaload
areturn
.limit locals 1
.limit stack 2
.end method

.method public static value(Ljava/lang/String;)I
getstatic org/xbill/DNS/Section/sections Lorg/xbill/DNS/Mnemonic;
aload 0
invokevirtual org/xbill/DNS/Mnemonic/getValue(Ljava/lang/String;)I
ireturn
.limit locals 1
.limit stack 2
.end method
