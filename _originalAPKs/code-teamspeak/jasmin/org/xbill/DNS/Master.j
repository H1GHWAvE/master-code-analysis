.bytecode 50.0
.class public synchronized org/xbill/DNS/Master
.super java/lang/Object

.field private 'currentDClass' I

.field private 'currentTTL' J

.field private 'currentType' I

.field private 'defaultTTL' J

.field private 'file' Ljava/io/File;

.field private 'generator' Lorg/xbill/DNS/Generator;

.field private 'generators' Ljava/util/List;

.field private 'included' Lorg/xbill/DNS/Master;

.field private 'last' Lorg/xbill/DNS/Record;

.field private 'needSOATTL' Z

.field private 'noExpandGenerate' Z

.field private 'origin' Lorg/xbill/DNS/Name;

.field private 'st' Lorg/xbill/DNS/Tokenizer;

.method <init>(Ljava/io/File;Lorg/xbill/DNS/Name;J)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aconst_null
putfield org/xbill/DNS/Master/last Lorg/xbill/DNS/Record;
aload 0
aconst_null
putfield org/xbill/DNS/Master/included Lorg/xbill/DNS/Master;
aload 2
ifnull L0
aload 2
invokevirtual org/xbill/DNS/Name/isAbsolute()Z
ifne L0
new org/xbill/DNS/RelativeNameException
dup
aload 2
invokespecial org/xbill/DNS/RelativeNameException/<init>(Lorg/xbill/DNS/Name;)V
athrow
L0:
aload 0
aload 1
putfield org/xbill/DNS/Master/file Ljava/io/File;
aload 0
new org/xbill/DNS/Tokenizer
dup
aload 1
invokespecial org/xbill/DNS/Tokenizer/<init>(Ljava/io/File;)V
putfield org/xbill/DNS/Master/st Lorg/xbill/DNS/Tokenizer;
aload 0
aload 2
putfield org/xbill/DNS/Master/origin Lorg/xbill/DNS/Name;
aload 0
lload 3
putfield org/xbill/DNS/Master/defaultTTL J
return
.limit locals 5
.limit stack 4
.end method

.method public <init>(Ljava/io/InputStream;)V
aload 0
aload 1
aconst_null
ldc2_w -1L
invokespecial org/xbill/DNS/Master/<init>(Ljava/io/InputStream;Lorg/xbill/DNS/Name;J)V
return
.limit locals 2
.limit stack 5
.end method

.method public <init>(Ljava/io/InputStream;Lorg/xbill/DNS/Name;)V
aload 0
aload 1
aload 2
ldc2_w -1L
invokespecial org/xbill/DNS/Master/<init>(Ljava/io/InputStream;Lorg/xbill/DNS/Name;J)V
return
.limit locals 3
.limit stack 5
.end method

.method public <init>(Ljava/io/InputStream;Lorg/xbill/DNS/Name;J)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aconst_null
putfield org/xbill/DNS/Master/last Lorg/xbill/DNS/Record;
aload 0
aconst_null
putfield org/xbill/DNS/Master/included Lorg/xbill/DNS/Master;
aload 2
ifnull L0
aload 2
invokevirtual org/xbill/DNS/Name/isAbsolute()Z
ifne L0
new org/xbill/DNS/RelativeNameException
dup
aload 2
invokespecial org/xbill/DNS/RelativeNameException/<init>(Lorg/xbill/DNS/Name;)V
athrow
L0:
aload 0
new org/xbill/DNS/Tokenizer
dup
aload 1
invokespecial org/xbill/DNS/Tokenizer/<init>(Ljava/io/InputStream;)V
putfield org/xbill/DNS/Master/st Lorg/xbill/DNS/Tokenizer;
aload 0
aload 2
putfield org/xbill/DNS/Master/origin Lorg/xbill/DNS/Name;
aload 0
lload 3
putfield org/xbill/DNS/Master/defaultTTL J
return
.limit locals 5
.limit stack 4
.end method

.method public <init>(Ljava/lang/String;)V
aload 0
new java/io/File
dup
aload 1
invokespecial java/io/File/<init>(Ljava/lang/String;)V
aconst_null
ldc2_w -1L
invokespecial org/xbill/DNS/Master/<init>(Ljava/io/File;Lorg/xbill/DNS/Name;J)V
return
.limit locals 2
.limit stack 5
.end method

.method public <init>(Ljava/lang/String;Lorg/xbill/DNS/Name;)V
aload 0
new java/io/File
dup
aload 1
invokespecial java/io/File/<init>(Ljava/lang/String;)V
aload 2
ldc2_w -1L
invokespecial org/xbill/DNS/Master/<init>(Ljava/io/File;Lorg/xbill/DNS/Name;J)V
return
.limit locals 3
.limit stack 5
.end method

.method public <init>(Ljava/lang/String;Lorg/xbill/DNS/Name;J)V
aload 0
new java/io/File
dup
aload 1
invokespecial java/io/File/<init>(Ljava/lang/String;)V
aload 2
lload 3
invokespecial org/xbill/DNS/Master/<init>(Ljava/io/File;Lorg/xbill/DNS/Name;J)V
return
.limit locals 5
.limit stack 5
.end method

.method private endGenerate()V
aload 0
getfield org/xbill/DNS/Master/st Lorg/xbill/DNS/Tokenizer;
invokevirtual org/xbill/DNS/Tokenizer/getEOL()V
aload 0
aconst_null
putfield org/xbill/DNS/Master/generator Lorg/xbill/DNS/Generator;
return
.limit locals 1
.limit stack 2
.end method

.method private nextGenerated()Lorg/xbill/DNS/Record;
.catch org/xbill/DNS/Tokenizer$TokenizerException from L0 to L1 using L2
.catch org/xbill/DNS/TextParseException from L0 to L1 using L3
L0:
aload 0
getfield org/xbill/DNS/Master/generator Lorg/xbill/DNS/Generator;
invokevirtual org/xbill/DNS/Generator/nextRecord()Lorg/xbill/DNS/Record;
astore 1
L1:
aload 1
areturn
L2:
astore 1
aload 0
getfield org/xbill/DNS/Master/st Lorg/xbill/DNS/Tokenizer;
new java/lang/StringBuffer
dup
ldc "Parsing $GENERATE: "
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
aload 1
invokevirtual org/xbill/DNS/Tokenizer$TokenizerException/getBaseMessage()Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual org/xbill/DNS/Tokenizer/exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;
athrow
L3:
astore 1
aload 0
getfield org/xbill/DNS/Master/st Lorg/xbill/DNS/Tokenizer;
new java/lang/StringBuffer
dup
ldc "Parsing $GENERATE: "
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
aload 1
invokevirtual org/xbill/DNS/TextParseException/getMessage()Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual org/xbill/DNS/Tokenizer/exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;
athrow
.limit locals 2
.limit stack 4
.end method

.method private parseName(Ljava/lang/String;Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/Name;
.catch org/xbill/DNS/TextParseException from L0 to L1 using L2
L0:
aload 1
aload 2
invokestatic org/xbill/DNS/Name/fromString(Ljava/lang/String;Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/Name;
astore 1
L1:
aload 1
areturn
L2:
astore 1
aload 0
getfield org/xbill/DNS/Master/st Lorg/xbill/DNS/Tokenizer;
aload 1
invokevirtual org/xbill/DNS/TextParseException/getMessage()Ljava/lang/String;
invokevirtual org/xbill/DNS/Tokenizer/exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;
athrow
.limit locals 3
.limit stack 2
.end method

.method private parseTTLClassAndType()V
.catch java/lang/NumberFormatException from L0 to L1 using L2
iconst_0
istore 1
aload 0
getfield org/xbill/DNS/Master/st Lorg/xbill/DNS/Tokenizer;
invokevirtual org/xbill/DNS/Tokenizer/getString()Ljava/lang/String;
astore 3
aload 3
invokestatic org/xbill/DNS/DClass/value(Ljava/lang/String;)I
istore 2
aload 0
iload 2
putfield org/xbill/DNS/Master/currentDClass I
iload 2
iflt L3
aload 0
getfield org/xbill/DNS/Master/st Lorg/xbill/DNS/Tokenizer;
invokevirtual org/xbill/DNS/Tokenizer/getString()Ljava/lang/String;
astore 3
iconst_1
istore 1
L3:
aload 0
ldc2_w -1L
putfield org/xbill/DNS/Master/currentTTL J
L0:
aload 0
aload 3
invokestatic org/xbill/DNS/TTL/parseTTL(Ljava/lang/String;)J
putfield org/xbill/DNS/Master/currentTTL J
aload 0
getfield org/xbill/DNS/Master/st Lorg/xbill/DNS/Tokenizer;
invokevirtual org/xbill/DNS/Tokenizer/getString()Ljava/lang/String;
astore 4
L1:
iload 1
ifne L4
aload 4
invokestatic org/xbill/DNS/DClass/value(Ljava/lang/String;)I
istore 1
aload 0
iload 1
putfield org/xbill/DNS/Master/currentDClass I
iload 1
iflt L5
aload 0
getfield org/xbill/DNS/Master/st Lorg/xbill/DNS/Tokenizer;
invokevirtual org/xbill/DNS/Tokenizer/getString()Ljava/lang/String;
astore 3
L6:
aload 3
invokestatic org/xbill/DNS/Type/value(Ljava/lang/String;)I
istore 1
aload 0
iload 1
putfield org/xbill/DNS/Master/currentType I
iload 1
ifge L7
aload 0
getfield org/xbill/DNS/Master/st Lorg/xbill/DNS/Tokenizer;
new java/lang/StringBuffer
dup
ldc "Invalid type '"
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
aload 3
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
ldc "'"
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual org/xbill/DNS/Tokenizer/exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;
athrow
L2:
astore 4
aload 0
getfield org/xbill/DNS/Master/defaultTTL J
lconst_0
lcmp
iflt L8
aload 0
aload 0
getfield org/xbill/DNS/Master/defaultTTL J
putfield org/xbill/DNS/Master/currentTTL J
aload 3
astore 4
goto L1
L8:
aload 3
astore 4
aload 0
getfield org/xbill/DNS/Master/last Lorg/xbill/DNS/Record;
ifnull L1
aload 0
aload 0
getfield org/xbill/DNS/Master/last Lorg/xbill/DNS/Record;
invokevirtual org/xbill/DNS/Record/getTTL()J
putfield org/xbill/DNS/Master/currentTTL J
aload 3
astore 4
goto L1
L5:
aload 0
iconst_1
putfield org/xbill/DNS/Master/currentDClass I
L4:
aload 4
astore 3
goto L6
L7:
aload 0
getfield org/xbill/DNS/Master/currentTTL J
lconst_0
lcmp
ifge L9
aload 0
getfield org/xbill/DNS/Master/currentType I
bipush 6
if_icmpeq L10
aload 0
getfield org/xbill/DNS/Master/st Lorg/xbill/DNS/Tokenizer;
ldc "missing TTL"
invokevirtual org/xbill/DNS/Tokenizer/exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;
athrow
L10:
aload 0
iconst_1
putfield org/xbill/DNS/Master/needSOATTL Z
aload 0
lconst_0
putfield org/xbill/DNS/Master/currentTTL J
L9:
return
.limit locals 5
.limit stack 4
.end method

.method private parseUInt32(Ljava/lang/String;)J
.catch java/lang/NumberFormatException from L0 to L1 using L2
aload 1
iconst_0
invokevirtual java/lang/String/charAt(I)C
invokestatic java/lang/Character/isDigit(C)Z
ifne L0
L3:
ldc2_w -1L
lreturn
L0:
aload 1
invokestatic java/lang/Long/parseLong(Ljava/lang/String;)J
lstore 2
L1:
lload 2
lconst_0
lcmp
iflt L3
lload 2
ldc2_w 4294967295L
lcmp
ifgt L3
lload 2
lreturn
L2:
astore 1
ldc2_w -1L
lreturn
.limit locals 4
.limit stack 4
.end method

.method private startGenerate()V
aload 0
getfield org/xbill/DNS/Master/st Lorg/xbill/DNS/Tokenizer;
invokevirtual org/xbill/DNS/Tokenizer/getIdentifier()Ljava/lang/String;
astore 11
aload 11
ldc "-"
invokevirtual java/lang/String/indexOf(Ljava/lang/String;)I
istore 1
iload 1
ifge L0
aload 0
getfield org/xbill/DNS/Master/st Lorg/xbill/DNS/Tokenizer;
new java/lang/StringBuffer
dup
ldc "Invalid $GENERATE range specifier: "
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
aload 11
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual org/xbill/DNS/Tokenizer/exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;
athrow
L0:
aload 11
iconst_0
iload 1
invokevirtual java/lang/String/substring(II)Ljava/lang/String;
astore 12
aload 11
iload 1
iconst_1
iadd
invokevirtual java/lang/String/substring(I)Ljava/lang/String;
astore 10
aconst_null
astore 9
aload 10
ldc "/"
invokevirtual java/lang/String/indexOf(Ljava/lang/String;)I
istore 1
aload 10
astore 8
iload 1
iflt L1
aload 10
iload 1
iconst_1
iadd
invokevirtual java/lang/String/substring(I)Ljava/lang/String;
astore 9
aload 10
iconst_0
iload 1
invokevirtual java/lang/String/substring(II)Ljava/lang/String;
astore 8
L1:
aload 0
aload 12
invokespecial org/xbill/DNS/Master/parseUInt32(Ljava/lang/String;)J
lstore 4
aload 0
aload 8
invokespecial org/xbill/DNS/Master/parseUInt32(Ljava/lang/String;)J
lstore 6
aload 9
ifnull L2
aload 0
aload 9
invokespecial org/xbill/DNS/Master/parseUInt32(Ljava/lang/String;)J
lstore 2
L3:
lload 4
lconst_0
lcmp
iflt L4
lload 6
lconst_0
lcmp
iflt L4
lload 4
lload 6
lcmp
ifgt L4
lload 2
lconst_0
lcmp
ifgt L5
L4:
aload 0
getfield org/xbill/DNS/Master/st Lorg/xbill/DNS/Tokenizer;
new java/lang/StringBuffer
dup
ldc "Invalid $GENERATE range specifier: "
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
aload 11
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual org/xbill/DNS/Tokenizer/exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;
athrow
L2:
lconst_1
lstore 2
goto L3
L5:
aload 0
getfield org/xbill/DNS/Master/st Lorg/xbill/DNS/Tokenizer;
invokevirtual org/xbill/DNS/Tokenizer/getIdentifier()Ljava/lang/String;
astore 8
aload 0
invokespecial org/xbill/DNS/Master/parseTTLClassAndType()V
aload 0
getfield org/xbill/DNS/Master/currentType I
invokestatic org/xbill/DNS/Generator/supportedType(I)Z
ifne L6
aload 0
getfield org/xbill/DNS/Master/st Lorg/xbill/DNS/Tokenizer;
new java/lang/StringBuffer
dup
ldc "$GENERATE does not support "
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
aload 0
getfield org/xbill/DNS/Master/currentType I
invokestatic org/xbill/DNS/Type/string(I)Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
ldc " records"
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual org/xbill/DNS/Tokenizer/exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;
athrow
L6:
aload 0
getfield org/xbill/DNS/Master/st Lorg/xbill/DNS/Tokenizer;
invokevirtual org/xbill/DNS/Tokenizer/getIdentifier()Ljava/lang/String;
astore 9
aload 0
getfield org/xbill/DNS/Master/st Lorg/xbill/DNS/Tokenizer;
invokevirtual org/xbill/DNS/Tokenizer/getEOL()V
aload 0
getfield org/xbill/DNS/Master/st Lorg/xbill/DNS/Tokenizer;
invokevirtual org/xbill/DNS/Tokenizer/unget()V
aload 0
new org/xbill/DNS/Generator
dup
lload 4
lload 6
lload 2
aload 8
aload 0
getfield org/xbill/DNS/Master/currentType I
aload 0
getfield org/xbill/DNS/Master/currentDClass I
aload 0
getfield org/xbill/DNS/Master/currentTTL J
aload 9
aload 0
getfield org/xbill/DNS/Master/origin Lorg/xbill/DNS/Name;
invokespecial org/xbill/DNS/Generator/<init>(JJJLjava/lang/String;IIJLjava/lang/String;Lorg/xbill/DNS/Name;)V
putfield org/xbill/DNS/Master/generator Lorg/xbill/DNS/Generator;
aload 0
getfield org/xbill/DNS/Master/generators Ljava/util/List;
ifnonnull L7
aload 0
new java/util/ArrayList
dup
iconst_1
invokespecial java/util/ArrayList/<init>(I)V
putfield org/xbill/DNS/Master/generators Ljava/util/List;
L7:
aload 0
getfield org/xbill/DNS/Master/generators Ljava/util/List;
aload 0
getfield org/xbill/DNS/Master/generator Lorg/xbill/DNS/Generator;
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
return
.limit locals 13
.limit stack 16
.end method

.method public _nextRecord()Lorg/xbill/DNS/Record;
aload 0
getfield org/xbill/DNS/Master/included Lorg/xbill/DNS/Master;
ifnull L0
aload 0
getfield org/xbill/DNS/Master/included Lorg/xbill/DNS/Master;
invokevirtual org/xbill/DNS/Master/nextRecord()Lorg/xbill/DNS/Record;
astore 3
aload 3
ifnull L1
L2:
aload 3
areturn
L1:
aload 0
aconst_null
putfield org/xbill/DNS/Master/included Lorg/xbill/DNS/Master;
L0:
aload 0
getfield org/xbill/DNS/Master/generator Lorg/xbill/DNS/Generator;
ifnull L3
aload 0
invokespecial org/xbill/DNS/Master/nextGenerated()Lorg/xbill/DNS/Record;
astore 4
aload 4
astore 3
aload 4
ifnonnull L2
L4:
aload 0
invokespecial org/xbill/DNS/Master/endGenerate()V
L3:
aload 0
getfield org/xbill/DNS/Master/st Lorg/xbill/DNS/Tokenizer;
iconst_1
iconst_0
invokevirtual org/xbill/DNS/Tokenizer/get(ZZ)Lorg/xbill/DNS/Tokenizer$Token;
astore 3
aload 3
getfield org/xbill/DNS/Tokenizer$Token/type I
iconst_2
if_icmpne L5
aload 0
getfield org/xbill/DNS/Master/st Lorg/xbill/DNS/Tokenizer;
invokevirtual org/xbill/DNS/Tokenizer/get()Lorg/xbill/DNS/Tokenizer$Token;
astore 3
aload 3
getfield org/xbill/DNS/Tokenizer$Token/type I
iconst_1
if_icmpeq L3
aload 3
getfield org/xbill/DNS/Tokenizer$Token/type I
ifne L6
aconst_null
areturn
L6:
aload 0
getfield org/xbill/DNS/Master/st Lorg/xbill/DNS/Tokenizer;
invokevirtual org/xbill/DNS/Tokenizer/unget()V
aload 0
getfield org/xbill/DNS/Master/last Lorg/xbill/DNS/Record;
ifnonnull L7
aload 0
getfield org/xbill/DNS/Master/st Lorg/xbill/DNS/Tokenizer;
ldc "no owner"
invokevirtual org/xbill/DNS/Tokenizer/exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;
athrow
L7:
aload 0
getfield org/xbill/DNS/Master/last Lorg/xbill/DNS/Record;
invokevirtual org/xbill/DNS/Record/getName()Lorg/xbill/DNS/Name;
astore 3
L8:
aload 0
invokespecial org/xbill/DNS/Master/parseTTLClassAndType()V
aload 0
aload 3
aload 0
getfield org/xbill/DNS/Master/currentType I
aload 0
getfield org/xbill/DNS/Master/currentDClass I
aload 0
getfield org/xbill/DNS/Master/currentTTL J
aload 0
getfield org/xbill/DNS/Master/st Lorg/xbill/DNS/Tokenizer;
aload 0
getfield org/xbill/DNS/Master/origin Lorg/xbill/DNS/Name;
invokestatic org/xbill/DNS/Record/fromString(Lorg/xbill/DNS/Name;IIJLorg/xbill/DNS/Tokenizer;Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/Record;
putfield org/xbill/DNS/Master/last Lorg/xbill/DNS/Record;
aload 0
getfield org/xbill/DNS/Master/needSOATTL Z
ifeq L9
aload 0
getfield org/xbill/DNS/Master/last Lorg/xbill/DNS/Record;
checkcast org/xbill/DNS/SOARecord
invokevirtual org/xbill/DNS/SOARecord/getMinimum()J
lstore 1
aload 0
getfield org/xbill/DNS/Master/last Lorg/xbill/DNS/Record;
lload 1
invokevirtual org/xbill/DNS/Record/setTTL(J)V
aload 0
lload 1
putfield org/xbill/DNS/Master/defaultTTL J
aload 0
iconst_0
putfield org/xbill/DNS/Master/needSOATTL Z
L9:
aload 0
getfield org/xbill/DNS/Master/last Lorg/xbill/DNS/Record;
areturn
L5:
aload 3
getfield org/xbill/DNS/Tokenizer$Token/type I
iconst_1
if_icmpeq L3
aload 3
getfield org/xbill/DNS/Tokenizer$Token/type I
ifne L10
aconst_null
areturn
L10:
aload 3
getfield org/xbill/DNS/Tokenizer$Token/value Ljava/lang/String;
iconst_0
invokevirtual java/lang/String/charAt(I)C
bipush 36
if_icmpne L11
aload 3
getfield org/xbill/DNS/Tokenizer$Token/value Ljava/lang/String;
astore 3
aload 3
ldc "$ORIGIN"
invokevirtual java/lang/String/equalsIgnoreCase(Ljava/lang/String;)Z
ifeq L12
aload 0
aload 0
getfield org/xbill/DNS/Master/st Lorg/xbill/DNS/Tokenizer;
getstatic org/xbill/DNS/Name/root Lorg/xbill/DNS/Name;
invokevirtual org/xbill/DNS/Tokenizer/getName(Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/Name;
putfield org/xbill/DNS/Master/origin Lorg/xbill/DNS/Name;
aload 0
getfield org/xbill/DNS/Master/st Lorg/xbill/DNS/Tokenizer;
invokevirtual org/xbill/DNS/Tokenizer/getEOL()V
goto L3
L12:
aload 3
ldc "$TTL"
invokevirtual java/lang/String/equalsIgnoreCase(Ljava/lang/String;)Z
ifeq L13
aload 0
aload 0
getfield org/xbill/DNS/Master/st Lorg/xbill/DNS/Tokenizer;
invokevirtual org/xbill/DNS/Tokenizer/getTTL()J
putfield org/xbill/DNS/Master/defaultTTL J
aload 0
getfield org/xbill/DNS/Master/st Lorg/xbill/DNS/Tokenizer;
invokevirtual org/xbill/DNS/Tokenizer/getEOL()V
goto L3
L13:
aload 3
ldc "$INCLUDE"
invokevirtual java/lang/String/equalsIgnoreCase(Ljava/lang/String;)Z
ifeq L14
aload 0
getfield org/xbill/DNS/Master/st Lorg/xbill/DNS/Tokenizer;
invokevirtual org/xbill/DNS/Tokenizer/getString()Ljava/lang/String;
astore 3
aload 0
getfield org/xbill/DNS/Master/file Ljava/io/File;
ifnull L15
new java/io/File
dup
aload 0
getfield org/xbill/DNS/Master/file Ljava/io/File;
invokevirtual java/io/File/getParent()Ljava/lang/String;
aload 3
invokespecial java/io/File/<init>(Ljava/lang/String;Ljava/lang/String;)V
astore 3
L16:
aload 0
getfield org/xbill/DNS/Master/origin Lorg/xbill/DNS/Name;
astore 4
aload 0
getfield org/xbill/DNS/Master/st Lorg/xbill/DNS/Tokenizer;
invokevirtual org/xbill/DNS/Tokenizer/get()Lorg/xbill/DNS/Tokenizer$Token;
astore 5
aload 5
invokevirtual org/xbill/DNS/Tokenizer$Token/isString()Z
ifeq L17
aload 0
aload 5
getfield org/xbill/DNS/Tokenizer$Token/value Ljava/lang/String;
getstatic org/xbill/DNS/Name/root Lorg/xbill/DNS/Name;
invokespecial org/xbill/DNS/Master/parseName(Ljava/lang/String;Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/Name;
astore 4
aload 0
getfield org/xbill/DNS/Master/st Lorg/xbill/DNS/Tokenizer;
invokevirtual org/xbill/DNS/Tokenizer/getEOL()V
L17:
aload 0
new org/xbill/DNS/Master
dup
aload 3
aload 4
aload 0
getfield org/xbill/DNS/Master/defaultTTL J
invokespecial org/xbill/DNS/Master/<init>(Ljava/io/File;Lorg/xbill/DNS/Name;J)V
putfield org/xbill/DNS/Master/included Lorg/xbill/DNS/Master;
aload 0
invokevirtual org/xbill/DNS/Master/nextRecord()Lorg/xbill/DNS/Record;
areturn
L15:
new java/io/File
dup
aload 3
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 3
goto L16
L14:
aload 3
ldc "$GENERATE"
invokevirtual java/lang/String/equalsIgnoreCase(Ljava/lang/String;)Z
ifeq L18
aload 0
getfield org/xbill/DNS/Master/generator Lorg/xbill/DNS/Generator;
ifnull L19
new java/lang/IllegalStateException
dup
ldc "cannot nest $GENERATE"
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L19:
aload 0
invokespecial org/xbill/DNS/Master/startGenerate()V
aload 0
getfield org/xbill/DNS/Master/noExpandGenerate Z
ifne L4
aload 0
invokespecial org/xbill/DNS/Master/nextGenerated()Lorg/xbill/DNS/Record;
areturn
L18:
aload 0
getfield org/xbill/DNS/Master/st Lorg/xbill/DNS/Tokenizer;
new java/lang/StringBuffer
dup
ldc "Invalid directive: "
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
aload 3
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual org/xbill/DNS/Tokenizer/exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;
athrow
L11:
aload 0
aload 3
getfield org/xbill/DNS/Tokenizer$Token/value Ljava/lang/String;
aload 0
getfield org/xbill/DNS/Master/origin Lorg/xbill/DNS/Name;
invokespecial org/xbill/DNS/Master/parseName(Ljava/lang/String;Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/Name;
astore 4
aload 4
astore 3
aload 0
getfield org/xbill/DNS/Master/last Lorg/xbill/DNS/Record;
ifnull L8
aload 4
astore 3
aload 4
aload 0
getfield org/xbill/DNS/Master/last Lorg/xbill/DNS/Record;
invokevirtual org/xbill/DNS/Record/getName()Lorg/xbill/DNS/Name;
invokevirtual org/xbill/DNS/Name/equals(Ljava/lang/Object;)Z
ifeq L8
aload 0
getfield org/xbill/DNS/Master/last Lorg/xbill/DNS/Record;
invokevirtual org/xbill/DNS/Record/getName()Lorg/xbill/DNS/Name;
astore 3
goto L8
.limit locals 6
.limit stack 8
.end method

.method public expandGenerate(Z)V
iload 1
ifne L0
iconst_1
istore 1
L1:
aload 0
iload 1
putfield org/xbill/DNS/Master/noExpandGenerate Z
return
L0:
iconst_0
istore 1
goto L1
.limit locals 2
.limit stack 2
.end method

.method protected finalize()V
aload 0
getfield org/xbill/DNS/Master/st Lorg/xbill/DNS/Tokenizer;
ifnull L0
aload 0
getfield org/xbill/DNS/Master/st Lorg/xbill/DNS/Tokenizer;
invokevirtual org/xbill/DNS/Tokenizer/close()V
L0:
return
.limit locals 1
.limit stack 1
.end method

.method public generators()Ljava/util/Iterator;
aload 0
getfield org/xbill/DNS/Master/generators Ljava/util/List;
ifnull L0
aload 0
getfield org/xbill/DNS/Master/generators Ljava/util/List;
invokestatic java/util/Collections/unmodifiableList(Ljava/util/List;)Ljava/util/List;
invokeinterface java/util/List/iterator()Ljava/util/Iterator; 0
areturn
L0:
getstatic java/util/Collections/EMPTY_LIST Ljava/util/List;
invokeinterface java/util/List/iterator()Ljava/util/Iterator; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method public nextRecord()Lorg/xbill/DNS/Record;
.catch all from L0 to L1 using L2
L0:
aload 0
invokevirtual org/xbill/DNS/Master/_nextRecord()Lorg/xbill/DNS/Record;
astore 1
L1:
aload 1
ifnonnull L3
aload 0
getfield org/xbill/DNS/Master/st Lorg/xbill/DNS/Tokenizer;
invokevirtual org/xbill/DNS/Tokenizer/close()V
L3:
aload 1
areturn
L2:
astore 1
aload 0
getfield org/xbill/DNS/Master/st Lorg/xbill/DNS/Tokenizer;
invokevirtual org/xbill/DNS/Tokenizer/close()V
aload 1
athrow
.limit locals 2
.limit stack 1
.end method
