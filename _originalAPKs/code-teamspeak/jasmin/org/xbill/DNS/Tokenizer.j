.bytecode 50.0
.class public synchronized org/xbill/DNS/Tokenizer
.super java/lang/Object

.field public static final 'COMMENT' I = 5


.field public static final 'EOF' I = 0


.field public static final 'EOL' I = 1


.field public static final 'IDENTIFIER' I = 3


.field public static final 'QUOTED_STRING' I = 4


.field public static final 'WHITESPACE' I = 2


.field private static 'delim' Ljava/lang/String;

.field private static 'quotes' Ljava/lang/String;

.field private 'current' Lorg/xbill/DNS/Tokenizer$Token;

.field private 'delimiters' Ljava/lang/String;

.field private 'filename' Ljava/lang/String;

.field private 'is' Ljava/io/PushbackInputStream;

.field private 'line' I

.field private 'multiline' I

.field private 'quoting' Z

.field private 'sb' Ljava/lang/StringBuffer;

.field private 'ungottenToken' Z

.field private 'wantClose' Z

.method static <clinit>()V
ldc " \u0009\n;()\""
putstatic org/xbill/DNS/Tokenizer/delim Ljava/lang/String;
ldc "\""
putstatic org/xbill/DNS/Tokenizer/quotes Ljava/lang/String;
return
.limit locals 0
.limit stack 1
.end method

.method public <init>(Ljava/io/File;)V
aload 0
new java/io/FileInputStream
dup
aload 1
invokespecial java/io/FileInputStream/<init>(Ljava/io/File;)V
invokespecial org/xbill/DNS/Tokenizer/<init>(Ljava/io/InputStream;)V
aload 0
iconst_1
putfield org/xbill/DNS/Tokenizer/wantClose Z
aload 0
aload 1
invokevirtual java/io/File/getName()Ljava/lang/String;
putfield org/xbill/DNS/Tokenizer/filename Ljava/lang/String;
return
.limit locals 2
.limit stack 4
.end method

.method public <init>(Ljava/io/InputStream;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 1
astore 2
aload 1
instanceof java/io/BufferedInputStream
ifne L0
new java/io/BufferedInputStream
dup
aload 1
invokespecial java/io/BufferedInputStream/<init>(Ljava/io/InputStream;)V
astore 2
L0:
aload 0
new java/io/PushbackInputStream
dup
aload 2
iconst_2
invokespecial java/io/PushbackInputStream/<init>(Ljava/io/InputStream;I)V
putfield org/xbill/DNS/Tokenizer/is Ljava/io/PushbackInputStream;
aload 0
iconst_0
putfield org/xbill/DNS/Tokenizer/ungottenToken Z
aload 0
iconst_0
putfield org/xbill/DNS/Tokenizer/multiline I
aload 0
iconst_0
putfield org/xbill/DNS/Tokenizer/quoting Z
aload 0
getstatic org/xbill/DNS/Tokenizer/delim Ljava/lang/String;
putfield org/xbill/DNS/Tokenizer/delimiters Ljava/lang/String;
aload 0
new org/xbill/DNS/Tokenizer$Token
dup
aconst_null
invokespecial org/xbill/DNS/Tokenizer$Token/<init>(Lorg/xbill/DNS/Tokenizer$1;)V
putfield org/xbill/DNS/Tokenizer/current Lorg/xbill/DNS/Tokenizer$Token;
aload 0
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
putfield org/xbill/DNS/Tokenizer/sb Ljava/lang/StringBuffer;
aload 0
ldc "<none>"
putfield org/xbill/DNS/Tokenizer/filename Ljava/lang/String;
aload 0
iconst_1
putfield org/xbill/DNS/Tokenizer/line I
return
.limit locals 3
.limit stack 5
.end method

.method public <init>(Ljava/lang/String;)V
aload 0
new java/io/ByteArrayInputStream
dup
aload 1
invokevirtual java/lang/String/getBytes()[B
invokespecial java/io/ByteArrayInputStream/<init>([B)V
invokespecial org/xbill/DNS/Tokenizer/<init>(Ljava/io/InputStream;)V
return
.limit locals 2
.limit stack 4
.end method

.method private _getIdentifier(Ljava/lang/String;)Ljava/lang/String;
aload 0
invokevirtual org/xbill/DNS/Tokenizer/get()Lorg/xbill/DNS/Tokenizer$Token;
astore 2
aload 2
getfield org/xbill/DNS/Tokenizer$Token/type I
iconst_3
if_icmpeq L0
aload 0
new java/lang/StringBuffer
dup
ldc "expected "
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual org/xbill/DNS/Tokenizer/exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;
athrow
L0:
aload 2
getfield org/xbill/DNS/Tokenizer$Token/value Ljava/lang/String;
areturn
.limit locals 3
.limit stack 4
.end method

.method private checkUnbalancedParens()V
aload 0
getfield org/xbill/DNS/Tokenizer/multiline I
ifle L0
aload 0
ldc "unbalanced parentheses"
invokevirtual org/xbill/DNS/Tokenizer/exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;
athrow
L0:
return
.limit locals 1
.limit stack 2
.end method

.method private getChar()I
aload 0
getfield org/xbill/DNS/Tokenizer/is Ljava/io/PushbackInputStream;
invokevirtual java/io/PushbackInputStream/read()I
istore 2
iload 2
istore 1
iload 2
bipush 13
if_icmpne L0
aload 0
getfield org/xbill/DNS/Tokenizer/is Ljava/io/PushbackInputStream;
invokevirtual java/io/PushbackInputStream/read()I
istore 1
iload 1
bipush 10
if_icmpeq L1
aload 0
getfield org/xbill/DNS/Tokenizer/is Ljava/io/PushbackInputStream;
iload 1
invokevirtual java/io/PushbackInputStream/unread(I)V
L1:
bipush 10
istore 1
L0:
iload 1
bipush 10
if_icmpne L2
aload 0
aload 0
getfield org/xbill/DNS/Tokenizer/line I
iconst_1
iadd
putfield org/xbill/DNS/Tokenizer/line I
L2:
iload 1
ireturn
.limit locals 3
.limit stack 3
.end method

.method private remainingStrings()Ljava/lang/String;
aconst_null
astore 1
L0:
aload 0
invokevirtual org/xbill/DNS/Tokenizer/get()Lorg/xbill/DNS/Tokenizer$Token;
astore 3
aload 3
invokevirtual org/xbill/DNS/Tokenizer$Token/isString()Z
ifeq L1
aload 1
astore 2
aload 1
ifnonnull L2
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
astore 2
L2:
aload 2
aload 3
getfield org/xbill/DNS/Tokenizer$Token/value Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 2
astore 1
goto L0
L1:
aload 0
invokevirtual org/xbill/DNS/Tokenizer/unget()V
aload 1
ifnonnull L3
aconst_null
areturn
L3:
aload 1
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
areturn
.limit locals 4
.limit stack 2
.end method

.method private skipWhitespace()I
iconst_0
istore 1
L0:
aload 0
invokespecial org/xbill/DNS/Tokenizer/getChar()I
istore 2
iload 2
bipush 32
if_icmpeq L1
iload 2
bipush 9
if_icmpeq L1
iload 2
bipush 10
if_icmpne L2
aload 0
getfield org/xbill/DNS/Tokenizer/multiline I
ifgt L1
L2:
aload 0
iload 2
invokespecial org/xbill/DNS/Tokenizer/ungetChar(I)V
iload 1
ireturn
L1:
iload 1
iconst_1
iadd
istore 1
goto L0
.limit locals 3
.limit stack 2
.end method

.method private ungetChar(I)V
iload 1
iconst_m1
if_icmpne L0
L1:
return
L0:
aload 0
getfield org/xbill/DNS/Tokenizer/is Ljava/io/PushbackInputStream;
iload 1
invokevirtual java/io/PushbackInputStream/unread(I)V
iload 1
bipush 10
if_icmpne L1
aload 0
aload 0
getfield org/xbill/DNS/Tokenizer/line I
iconst_1
isub
putfield org/xbill/DNS/Tokenizer/line I
return
.limit locals 2
.limit stack 3
.end method

.method public close()V
.catch java/io/IOException from L0 to L1 using L2
aload 0
getfield org/xbill/DNS/Tokenizer/wantClose Z
ifeq L1
L0:
aload 0
getfield org/xbill/DNS/Tokenizer/is Ljava/io/PushbackInputStream;
invokevirtual java/io/PushbackInputStream/close()V
L1:
return
L2:
astore 1
return
.limit locals 2
.limit stack 1
.end method

.method public exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;
new org/xbill/DNS/Tokenizer$TokenizerException
dup
aload 0
getfield org/xbill/DNS/Tokenizer/filename Ljava/lang/String;
aload 0
getfield org/xbill/DNS/Tokenizer/line I
aload 1
invokespecial org/xbill/DNS/Tokenizer$TokenizerException/<init>(Ljava/lang/String;ILjava/lang/String;)V
areturn
.limit locals 2
.limit stack 5
.end method

.method protected finalize()V
aload 0
invokevirtual org/xbill/DNS/Tokenizer/close()V
return
.limit locals 1
.limit stack 1
.end method

.method public get()Lorg/xbill/DNS/Tokenizer$Token;
aload 0
iconst_0
iconst_0
invokevirtual org/xbill/DNS/Tokenizer/get(ZZ)Lorg/xbill/DNS/Tokenizer$Token;
areturn
.limit locals 1
.limit stack 3
.end method

.method public get(ZZ)Lorg/xbill/DNS/Tokenizer$Token;
aload 0
getfield org/xbill/DNS/Tokenizer/ungottenToken Z
ifeq L0
aload 0
iconst_0
putfield org/xbill/DNS/Tokenizer/ungottenToken Z
aload 0
getfield org/xbill/DNS/Tokenizer/current Lorg/xbill/DNS/Tokenizer$Token;
getfield org/xbill/DNS/Tokenizer$Token/type I
iconst_2
if_icmpne L1
iload 1
ifeq L0
aload 0
getfield org/xbill/DNS/Tokenizer/current Lorg/xbill/DNS/Tokenizer$Token;
areturn
L1:
aload 0
getfield org/xbill/DNS/Tokenizer/current Lorg/xbill/DNS/Tokenizer$Token;
getfield org/xbill/DNS/Tokenizer$Token/type I
iconst_5
if_icmpne L2
iload 2
ifeq L0
aload 0
getfield org/xbill/DNS/Tokenizer/current Lorg/xbill/DNS/Tokenizer$Token;
areturn
L2:
aload 0
getfield org/xbill/DNS/Tokenizer/current Lorg/xbill/DNS/Tokenizer$Token;
getfield org/xbill/DNS/Tokenizer$Token/type I
iconst_1
if_icmpne L3
aload 0
aload 0
getfield org/xbill/DNS/Tokenizer/line I
iconst_1
iadd
putfield org/xbill/DNS/Tokenizer/line I
L3:
aload 0
getfield org/xbill/DNS/Tokenizer/current Lorg/xbill/DNS/Tokenizer$Token;
areturn
L0:
aload 0
invokespecial org/xbill/DNS/Tokenizer/skipWhitespace()I
ifle L4
iload 1
ifeq L4
aload 0
getfield org/xbill/DNS/Tokenizer/current Lorg/xbill/DNS/Tokenizer$Token;
iconst_2
aconst_null
invokestatic org/xbill/DNS/Tokenizer$Token/access$100(Lorg/xbill/DNS/Tokenizer$Token;ILjava/lang/StringBuffer;)Lorg/xbill/DNS/Tokenizer$Token;
areturn
L4:
iconst_3
istore 3
aload 0
getfield org/xbill/DNS/Tokenizer/sb Ljava/lang/StringBuffer;
iconst_0
invokevirtual java/lang/StringBuffer/setLength(I)V
L5:
aload 0
invokespecial org/xbill/DNS/Tokenizer/getChar()I
istore 5
iload 5
iconst_m1
if_icmpeq L6
aload 0
getfield org/xbill/DNS/Tokenizer/delimiters Ljava/lang/String;
iload 5
invokevirtual java/lang/String/indexOf(I)I
iconst_m1
if_icmpeq L7
L6:
iload 5
iconst_m1
if_icmpne L8
aload 0
getfield org/xbill/DNS/Tokenizer/quoting Z
ifeq L9
aload 0
ldc "EOF in quoted string"
invokevirtual org/xbill/DNS/Tokenizer/exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;
athrow
L9:
aload 0
getfield org/xbill/DNS/Tokenizer/sb Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/length()I
ifne L10
aload 0
getfield org/xbill/DNS/Tokenizer/current Lorg/xbill/DNS/Tokenizer$Token;
iconst_0
aconst_null
invokestatic org/xbill/DNS/Tokenizer$Token/access$100(Lorg/xbill/DNS/Tokenizer$Token;ILjava/lang/StringBuffer;)Lorg/xbill/DNS/Tokenizer$Token;
areturn
L10:
aload 0
getfield org/xbill/DNS/Tokenizer/current Lorg/xbill/DNS/Tokenizer$Token;
iload 3
aload 0
getfield org/xbill/DNS/Tokenizer/sb Ljava/lang/StringBuffer;
invokestatic org/xbill/DNS/Tokenizer$Token/access$100(Lorg/xbill/DNS/Tokenizer$Token;ILjava/lang/StringBuffer;)Lorg/xbill/DNS/Tokenizer$Token;
areturn
L8:
aload 0
getfield org/xbill/DNS/Tokenizer/sb Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/length()I
ifne L11
iload 3
iconst_4
if_icmpeq L11
iload 5
bipush 40
if_icmpne L12
aload 0
aload 0
getfield org/xbill/DNS/Tokenizer/multiline I
iconst_1
iadd
putfield org/xbill/DNS/Tokenizer/multiline I
aload 0
invokespecial org/xbill/DNS/Tokenizer/skipWhitespace()I
pop
goto L5
L12:
iload 5
bipush 41
if_icmpne L13
aload 0
getfield org/xbill/DNS/Tokenizer/multiline I
ifgt L14
aload 0
ldc "invalid close parenthesis"
invokevirtual org/xbill/DNS/Tokenizer/exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;
athrow
L14:
aload 0
aload 0
getfield org/xbill/DNS/Tokenizer/multiline I
iconst_1
isub
putfield org/xbill/DNS/Tokenizer/multiline I
aload 0
invokespecial org/xbill/DNS/Tokenizer/skipWhitespace()I
pop
goto L5
L13:
iload 5
bipush 34
if_icmpne L15
aload 0
getfield org/xbill/DNS/Tokenizer/quoting Z
ifne L16
aload 0
iconst_1
putfield org/xbill/DNS/Tokenizer/quoting Z
aload 0
getstatic org/xbill/DNS/Tokenizer/quotes Ljava/lang/String;
putfield org/xbill/DNS/Tokenizer/delimiters Ljava/lang/String;
iconst_4
istore 3
goto L5
L16:
aload 0
iconst_0
putfield org/xbill/DNS/Tokenizer/quoting Z
aload 0
getstatic org/xbill/DNS/Tokenizer/delim Ljava/lang/String;
putfield org/xbill/DNS/Tokenizer/delimiters Ljava/lang/String;
aload 0
invokespecial org/xbill/DNS/Tokenizer/skipWhitespace()I
pop
goto L5
L15:
iload 5
bipush 10
if_icmpne L17
aload 0
getfield org/xbill/DNS/Tokenizer/current Lorg/xbill/DNS/Tokenizer$Token;
iconst_1
aconst_null
invokestatic org/xbill/DNS/Tokenizer$Token/access$100(Lorg/xbill/DNS/Tokenizer$Token;ILjava/lang/StringBuffer;)Lorg/xbill/DNS/Tokenizer$Token;
areturn
L17:
iload 5
bipush 59
if_icmpne L18
L19:
aload 0
invokespecial org/xbill/DNS/Tokenizer/getChar()I
istore 4
iload 4
bipush 10
if_icmpeq L20
iload 4
iconst_m1
if_icmpeq L20
aload 0
getfield org/xbill/DNS/Tokenizer/sb Ljava/lang/StringBuffer;
iload 4
i2c
invokevirtual java/lang/StringBuffer/append(C)Ljava/lang/StringBuffer;
pop
goto L19
L20:
iload 2
ifeq L21
aload 0
iload 4
invokespecial org/xbill/DNS/Tokenizer/ungetChar(I)V
aload 0
getfield org/xbill/DNS/Tokenizer/current Lorg/xbill/DNS/Tokenizer$Token;
iconst_5
aload 0
getfield org/xbill/DNS/Tokenizer/sb Ljava/lang/StringBuffer;
invokestatic org/xbill/DNS/Tokenizer$Token/access$100(Lorg/xbill/DNS/Tokenizer$Token;ILjava/lang/StringBuffer;)Lorg/xbill/DNS/Tokenizer$Token;
areturn
L21:
iload 4
iconst_m1
if_icmpne L22
iload 3
iconst_4
if_icmpeq L22
aload 0
invokespecial org/xbill/DNS/Tokenizer/checkUnbalancedParens()V
aload 0
getfield org/xbill/DNS/Tokenizer/current Lorg/xbill/DNS/Tokenizer$Token;
iconst_0
aconst_null
invokestatic org/xbill/DNS/Tokenizer$Token/access$100(Lorg/xbill/DNS/Tokenizer$Token;ILjava/lang/StringBuffer;)Lorg/xbill/DNS/Tokenizer$Token;
areturn
L22:
aload 0
getfield org/xbill/DNS/Tokenizer/multiline I
ifle L23
aload 0
invokespecial org/xbill/DNS/Tokenizer/skipWhitespace()I
pop
aload 0
getfield org/xbill/DNS/Tokenizer/sb Ljava/lang/StringBuffer;
iconst_0
invokevirtual java/lang/StringBuffer/setLength(I)V
goto L5
L23:
aload 0
getfield org/xbill/DNS/Tokenizer/current Lorg/xbill/DNS/Tokenizer$Token;
iconst_1
aconst_null
invokestatic org/xbill/DNS/Tokenizer$Token/access$100(Lorg/xbill/DNS/Tokenizer$Token;ILjava/lang/StringBuffer;)Lorg/xbill/DNS/Tokenizer$Token;
areturn
L18:
new java/lang/IllegalStateException
dup
invokespecial java/lang/IllegalStateException/<init>()V
athrow
L11:
aload 0
iload 5
invokespecial org/xbill/DNS/Tokenizer/ungetChar(I)V
aload 0
getfield org/xbill/DNS/Tokenizer/sb Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/length()I
ifne L24
iload 3
iconst_4
if_icmpeq L24
aload 0
invokespecial org/xbill/DNS/Tokenizer/checkUnbalancedParens()V
aload 0
getfield org/xbill/DNS/Tokenizer/current Lorg/xbill/DNS/Tokenizer$Token;
iconst_0
aconst_null
invokestatic org/xbill/DNS/Tokenizer$Token/access$100(Lorg/xbill/DNS/Tokenizer$Token;ILjava/lang/StringBuffer;)Lorg/xbill/DNS/Tokenizer$Token;
areturn
L7:
iload 5
bipush 92
if_icmpne L25
aload 0
invokespecial org/xbill/DNS/Tokenizer/getChar()I
istore 4
iload 4
iconst_m1
if_icmpne L26
aload 0
ldc "unterminated escape sequence"
invokevirtual org/xbill/DNS/Tokenizer/exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;
athrow
L26:
aload 0
getfield org/xbill/DNS/Tokenizer/sb Ljava/lang/StringBuffer;
bipush 92
invokevirtual java/lang/StringBuffer/append(C)Ljava/lang/StringBuffer;
pop
L27:
aload 0
getfield org/xbill/DNS/Tokenizer/sb Ljava/lang/StringBuffer;
iload 4
i2c
invokevirtual java/lang/StringBuffer/append(C)Ljava/lang/StringBuffer;
pop
goto L5
L25:
iload 5
istore 4
aload 0
getfield org/xbill/DNS/Tokenizer/quoting Z
ifeq L27
iload 5
istore 4
iload 5
bipush 10
if_icmpne L27
aload 0
ldc "newline in quoted string"
invokevirtual org/xbill/DNS/Tokenizer/exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;
athrow
L24:
aload 0
getfield org/xbill/DNS/Tokenizer/current Lorg/xbill/DNS/Tokenizer$Token;
iload 3
aload 0
getfield org/xbill/DNS/Tokenizer/sb Ljava/lang/StringBuffer;
invokestatic org/xbill/DNS/Tokenizer$Token/access$100(Lorg/xbill/DNS/Tokenizer$Token;ILjava/lang/StringBuffer;)Lorg/xbill/DNS/Tokenizer$Token;
areturn
.limit locals 6
.limit stack 3
.end method

.method public getAddress(I)Ljava/net/InetAddress;
.catch java/net/UnknownHostException from L0 to L1 using L2
aload 0
ldc "an address"
invokespecial org/xbill/DNS/Tokenizer/_getIdentifier(Ljava/lang/String;)Ljava/lang/String;
astore 2
L0:
aload 2
iload 1
invokestatic org/xbill/DNS/Address/getByAddress(Ljava/lang/String;I)Ljava/net/InetAddress;
astore 2
L1:
aload 2
areturn
L2:
astore 2
aload 0
aload 2
invokevirtual java/net/UnknownHostException/getMessage()Ljava/lang/String;
invokevirtual org/xbill/DNS/Tokenizer/exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;
athrow
.limit locals 3
.limit stack 2
.end method

.method public getAddressBytes(I)[B
aload 0
ldc "an address"
invokespecial org/xbill/DNS/Tokenizer/_getIdentifier(Ljava/lang/String;)Ljava/lang/String;
astore 2
aload 2
iload 1
invokestatic org/xbill/DNS/Address/toByteArray(Ljava/lang/String;I)[B
astore 3
aload 3
ifnonnull L0
aload 0
new java/lang/StringBuffer
dup
ldc "Invalid address: "
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
aload 2
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual org/xbill/DNS/Tokenizer/exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;
athrow
L0:
aload 3
areturn
.limit locals 4
.limit stack 4
.end method

.method public getBase32String(Lorg/xbill/DNS/utils/base32;)[B
aload 1
aload 0
ldc "a base32 string"
invokespecial org/xbill/DNS/Tokenizer/_getIdentifier(Ljava/lang/String;)Ljava/lang/String;
invokevirtual org/xbill/DNS/utils/base32/fromString(Ljava/lang/String;)[B
astore 1
aload 1
ifnonnull L0
aload 0
ldc "invalid base32 encoding"
invokevirtual org/xbill/DNS/Tokenizer/exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;
athrow
L0:
aload 1
areturn
.limit locals 2
.limit stack 3
.end method

.method public getBase64()[B
aload 0
iconst_0
invokevirtual org/xbill/DNS/Tokenizer/getBase64(Z)[B
areturn
.limit locals 1
.limit stack 2
.end method

.method public getBase64(Z)[B
aload 0
invokespecial org/xbill/DNS/Tokenizer/remainingStrings()Ljava/lang/String;
astore 2
aload 2
ifnonnull L0
iload 1
ifeq L1
aload 0
ldc "expected base64 encoded string"
invokevirtual org/xbill/DNS/Tokenizer/exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;
athrow
L1:
aconst_null
astore 2
L2:
aload 2
areturn
L0:
aload 2
invokestatic org/xbill/DNS/utils/base64/fromString(Ljava/lang/String;)[B
astore 3
aload 3
astore 2
aload 3
ifnonnull L2
aload 0
ldc "invalid base64 encoding"
invokevirtual org/xbill/DNS/Tokenizer/exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;
athrow
.limit locals 4
.limit stack 2
.end method

.method public getEOL()V
aload 0
invokevirtual org/xbill/DNS/Tokenizer/get()Lorg/xbill/DNS/Tokenizer$Token;
astore 1
aload 1
getfield org/xbill/DNS/Tokenizer$Token/type I
iconst_1
if_icmpeq L0
aload 1
getfield org/xbill/DNS/Tokenizer$Token/type I
ifeq L0
aload 0
ldc "expected EOL or EOF"
invokevirtual org/xbill/DNS/Tokenizer/exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;
athrow
L0:
return
.limit locals 2
.limit stack 2
.end method

.method public getHex()[B
aload 0
iconst_0
invokevirtual org/xbill/DNS/Tokenizer/getHex(Z)[B
areturn
.limit locals 1
.limit stack 2
.end method

.method public getHex(Z)[B
aload 0
invokespecial org/xbill/DNS/Tokenizer/remainingStrings()Ljava/lang/String;
astore 2
aload 2
ifnonnull L0
iload 1
ifeq L1
aload 0
ldc "expected hex encoded string"
invokevirtual org/xbill/DNS/Tokenizer/exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;
athrow
L1:
aconst_null
astore 2
L2:
aload 2
areturn
L0:
aload 2
invokestatic org/xbill/DNS/utils/base16/fromString(Ljava/lang/String;)[B
astore 3
aload 3
astore 2
aload 3
ifnonnull L2
aload 0
ldc "invalid hex encoding"
invokevirtual org/xbill/DNS/Tokenizer/exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;
athrow
.limit locals 4
.limit stack 2
.end method

.method public getHexString()[B
aload 0
ldc "a hex string"
invokespecial org/xbill/DNS/Tokenizer/_getIdentifier(Ljava/lang/String;)Ljava/lang/String;
invokestatic org/xbill/DNS/utils/base16/fromString(Ljava/lang/String;)[B
astore 1
aload 1
ifnonnull L0
aload 0
ldc "invalid hex encoding"
invokevirtual org/xbill/DNS/Tokenizer/exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;
athrow
L0:
aload 1
areturn
.limit locals 2
.limit stack 2
.end method

.method public getIdentifier()Ljava/lang/String;
aload 0
ldc "an identifier"
invokespecial org/xbill/DNS/Tokenizer/_getIdentifier(Ljava/lang/String;)Ljava/lang/String;
areturn
.limit locals 1
.limit stack 2
.end method

.method public getLong()J
.catch java/lang/NumberFormatException from L0 to L1 using L2
aload 0
ldc "an integer"
invokespecial org/xbill/DNS/Tokenizer/_getIdentifier(Ljava/lang/String;)Ljava/lang/String;
astore 3
aload 3
iconst_0
invokevirtual java/lang/String/charAt(I)C
invokestatic java/lang/Character/isDigit(C)Z
ifne L0
aload 0
ldc "expected an integer"
invokevirtual org/xbill/DNS/Tokenizer/exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;
athrow
L0:
aload 3
invokestatic java/lang/Long/parseLong(Ljava/lang/String;)J
lstore 1
L1:
lload 1
lreturn
L2:
astore 3
aload 0
ldc "expected an integer"
invokevirtual org/xbill/DNS/Tokenizer/exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;
athrow
.limit locals 4
.limit stack 2
.end method

.method public getName(Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/Name;
.catch org/xbill/DNS/TextParseException from L0 to L1 using L1
aload 0
ldc "a name"
invokespecial org/xbill/DNS/Tokenizer/_getIdentifier(Ljava/lang/String;)Ljava/lang/String;
astore 2
L0:
aload 2
aload 1
invokestatic org/xbill/DNS/Name/fromString(Ljava/lang/String;Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/Name;
astore 1
aload 1
invokevirtual org/xbill/DNS/Name/isAbsolute()Z
ifne L2
new org/xbill/DNS/RelativeNameException
dup
aload 1
invokespecial org/xbill/DNS/RelativeNameException/<init>(Lorg/xbill/DNS/Name;)V
athrow
L1:
astore 1
aload 0
aload 1
invokevirtual org/xbill/DNS/TextParseException/getMessage()Ljava/lang/String;
invokevirtual org/xbill/DNS/Tokenizer/exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;
athrow
L2:
aload 1
areturn
.limit locals 3
.limit stack 3
.end method

.method public getString()Ljava/lang/String;
aload 0
invokevirtual org/xbill/DNS/Tokenizer/get()Lorg/xbill/DNS/Tokenizer$Token;
astore 1
aload 1
invokevirtual org/xbill/DNS/Tokenizer$Token/isString()Z
ifne L0
aload 0
ldc "expected a string"
invokevirtual org/xbill/DNS/Tokenizer/exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;
athrow
L0:
aload 1
getfield org/xbill/DNS/Tokenizer$Token/value Ljava/lang/String;
areturn
.limit locals 2
.limit stack 2
.end method

.method public getTTL()J
.catch java/lang/NumberFormatException from L0 to L1 using L2
aload 0
ldc "a TTL value"
invokespecial org/xbill/DNS/Tokenizer/_getIdentifier(Ljava/lang/String;)Ljava/lang/String;
astore 3
L0:
aload 3
invokestatic org/xbill/DNS/TTL/parseTTL(Ljava/lang/String;)J
lstore 1
L1:
lload 1
lreturn
L2:
astore 3
aload 0
ldc "expected a TTL value"
invokevirtual org/xbill/DNS/Tokenizer/exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;
athrow
.limit locals 4
.limit stack 2
.end method

.method public getTTLLike()J
.catch java/lang/NumberFormatException from L0 to L1 using L2
aload 0
ldc "a TTL-like value"
invokespecial org/xbill/DNS/Tokenizer/_getIdentifier(Ljava/lang/String;)Ljava/lang/String;
astore 3
L0:
aload 3
iconst_0
invokestatic org/xbill/DNS/TTL/parse(Ljava/lang/String;Z)J
lstore 1
L1:
lload 1
lreturn
L2:
astore 3
aload 0
ldc "expected a TTL-like value"
invokevirtual org/xbill/DNS/Tokenizer/exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;
athrow
.limit locals 4
.limit stack 2
.end method

.method public getUInt16()I
aload 0
invokevirtual org/xbill/DNS/Tokenizer/getLong()J
lstore 1
lload 1
lconst_0
lcmp
iflt L0
lload 1
ldc2_w 65535L
lcmp
ifle L1
L0:
aload 0
ldc "expected an 16 bit unsigned integer"
invokevirtual org/xbill/DNS/Tokenizer/exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;
athrow
L1:
lload 1
l2i
ireturn
.limit locals 3
.limit stack 4
.end method

.method public getUInt32()J
aload 0
invokevirtual org/xbill/DNS/Tokenizer/getLong()J
lstore 1
lload 1
lconst_0
lcmp
iflt L0
lload 1
ldc2_w 4294967295L
lcmp
ifle L1
L0:
aload 0
ldc "expected an 32 bit unsigned integer"
invokevirtual org/xbill/DNS/Tokenizer/exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;
athrow
L1:
lload 1
lreturn
.limit locals 3
.limit stack 4
.end method

.method public getUInt8()I
aload 0
invokevirtual org/xbill/DNS/Tokenizer/getLong()J
lstore 1
lload 1
lconst_0
lcmp
iflt L0
lload 1
ldc2_w 255L
lcmp
ifle L1
L0:
aload 0
ldc "expected an 8 bit unsigned integer"
invokevirtual org/xbill/DNS/Tokenizer/exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;
athrow
L1:
lload 1
l2i
ireturn
.limit locals 3
.limit stack 4
.end method

.method public unget()V
aload 0
getfield org/xbill/DNS/Tokenizer/ungottenToken Z
ifeq L0
new java/lang/IllegalStateException
dup
ldc "Cannot unget multiple tokens"
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
getfield org/xbill/DNS/Tokenizer/current Lorg/xbill/DNS/Tokenizer$Token;
getfield org/xbill/DNS/Tokenizer$Token/type I
iconst_1
if_icmpne L1
aload 0
aload 0
getfield org/xbill/DNS/Tokenizer/line I
iconst_1
isub
putfield org/xbill/DNS/Tokenizer/line I
L1:
aload 0
iconst_1
putfield org/xbill/DNS/Tokenizer/ungottenToken Z
return
.limit locals 1
.limit stack 3
.end method
