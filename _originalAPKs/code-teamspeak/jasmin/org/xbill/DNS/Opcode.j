.bytecode 50.0
.class public final synchronized org/xbill/DNS/Opcode
.super java/lang/Object

.field public static final 'IQUERY' I = 1


.field public static final 'NOTIFY' I = 4


.field public static final 'QUERY' I = 0


.field public static final 'STATUS' I = 2


.field public static final 'UPDATE' I = 5


.field private static 'opcodes' Lorg/xbill/DNS/Mnemonic;

.method static <clinit>()V
new org/xbill/DNS/Mnemonic
dup
ldc "DNS Opcode"
iconst_2
invokespecial org/xbill/DNS/Mnemonic/<init>(Ljava/lang/String;I)V
astore 0
aload 0
putstatic org/xbill/DNS/Opcode/opcodes Lorg/xbill/DNS/Mnemonic;
aload 0
bipush 15
invokevirtual org/xbill/DNS/Mnemonic/setMaximum(I)V
getstatic org/xbill/DNS/Opcode/opcodes Lorg/xbill/DNS/Mnemonic;
ldc "RESERVED"
invokevirtual org/xbill/DNS/Mnemonic/setPrefix(Ljava/lang/String;)V
getstatic org/xbill/DNS/Opcode/opcodes Lorg/xbill/DNS/Mnemonic;
iconst_1
invokevirtual org/xbill/DNS/Mnemonic/setNumericAllowed(Z)V
getstatic org/xbill/DNS/Opcode/opcodes Lorg/xbill/DNS/Mnemonic;
iconst_0
ldc "QUERY"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/Opcode/opcodes Lorg/xbill/DNS/Mnemonic;
iconst_1
ldc "IQUERY"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/Opcode/opcodes Lorg/xbill/DNS/Mnemonic;
iconst_2
ldc "STATUS"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/Opcode/opcodes Lorg/xbill/DNS/Mnemonic;
iconst_4
ldc "NOTIFY"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/Opcode/opcodes Lorg/xbill/DNS/Mnemonic;
iconst_5
ldc "UPDATE"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
return
.limit locals 1
.limit stack 4
.end method

.method private <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static string(I)Ljava/lang/String;
getstatic org/xbill/DNS/Opcode/opcodes Lorg/xbill/DNS/Mnemonic;
iload 0
invokevirtual org/xbill/DNS/Mnemonic/getText(I)Ljava/lang/String;
areturn
.limit locals 1
.limit stack 2
.end method

.method public static value(Ljava/lang/String;)I
getstatic org/xbill/DNS/Opcode/opcodes Lorg/xbill/DNS/Mnemonic;
aload 0
invokevirtual org/xbill/DNS/Mnemonic/getValue(Ljava/lang/String;)I
ireturn
.limit locals 1
.limit stack 2
.end method
