.bytecode 50.0
.class public synchronized org/xbill/DNS/OPTRecord
.super org/xbill/DNS/Record

.field private static final 'serialVersionUID' J = -6254521894809367938L


.field private 'options' Ljava/util/List;

.method <init>()V
aload 0
invokespecial org/xbill/DNS/Record/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public <init>(III)V
aload 0
iload 1
iload 2
iload 3
iconst_0
aconst_null
invokespecial org/xbill/DNS/OPTRecord/<init>(IIIILjava/util/List;)V
return
.limit locals 4
.limit stack 6
.end method

.method public <init>(IIII)V
aload 0
iload 1
iload 2
iload 3
iload 4
aconst_null
invokespecial org/xbill/DNS/OPTRecord/<init>(IIIILjava/util/List;)V
return
.limit locals 5
.limit stack 6
.end method

.method public <init>(IIIILjava/util/List;)V
aload 0
getstatic org/xbill/DNS/Name/root Lorg/xbill/DNS/Name;
bipush 41
iload 1
lconst_0
invokespecial org/xbill/DNS/Record/<init>(Lorg/xbill/DNS/Name;IIJ)V
ldc "payloadSize"
iload 1
invokestatic org/xbill/DNS/OPTRecord/checkU16(Ljava/lang/String;I)I
pop
ldc "xrcode"
iload 2
invokestatic org/xbill/DNS/OPTRecord/checkU8(Ljava/lang/String;I)I
pop
ldc "version"
iload 3
invokestatic org/xbill/DNS/OPTRecord/checkU8(Ljava/lang/String;I)I
pop
ldc "flags"
iload 4
invokestatic org/xbill/DNS/OPTRecord/checkU16(Ljava/lang/String;I)I
pop
aload 0
iload 2
i2l
bipush 24
lshl
iload 3
i2l
bipush 16
lshl
ladd
iload 4
i2l
ladd
putfield org/xbill/DNS/OPTRecord/ttl J
aload 5
ifnull L0
aload 0
new java/util/ArrayList
dup
aload 5
invokespecial java/util/ArrayList/<init>(Ljava/util/Collection;)V
putfield org/xbill/DNS/OPTRecord/options Ljava/util/List;
L0:
return
.limit locals 6
.limit stack 6
.end method

.method public equals(Ljava/lang/Object;)Z
aload 0
aload 1
invokespecial org/xbill/DNS/Record/equals(Ljava/lang/Object;)Z
ifeq L0
aload 0
getfield org/xbill/DNS/OPTRecord/ttl J
aload 1
checkcast org/xbill/DNS/OPTRecord
getfield org/xbill/DNS/OPTRecord/ttl J
lcmp
ifne L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 4
.end method

.method public getExtendedRcode()I
aload 0
getfield org/xbill/DNS/OPTRecord/ttl J
bipush 24
lushr
l2i
ireturn
.limit locals 1
.limit stack 3
.end method

.method public getFlags()I
aload 0
getfield org/xbill/DNS/OPTRecord/ttl J
ldc2_w 65535L
land
l2i
ireturn
.limit locals 1
.limit stack 4
.end method

.method getObject()Lorg/xbill/DNS/Record;
new org/xbill/DNS/OPTRecord
dup
invokespecial org/xbill/DNS/OPTRecord/<init>()V
areturn
.limit locals 1
.limit stack 2
.end method

.method public getOptions()Ljava/util/List;
aload 0
getfield org/xbill/DNS/OPTRecord/options Ljava/util/List;
ifnonnull L0
getstatic java/util/Collections/EMPTY_LIST Ljava/util/List;
areturn
L0:
aload 0
getfield org/xbill/DNS/OPTRecord/options Ljava/util/List;
invokestatic java/util/Collections/unmodifiableList(Ljava/util/List;)Ljava/util/List;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getOptions(I)Ljava/util/List;
aload 0
getfield org/xbill/DNS/OPTRecord/options Ljava/util/List;
ifnonnull L0
getstatic java/util/Collections/EMPTY_LIST Ljava/util/List;
astore 3
L1:
aload 3
areturn
L0:
getstatic java/util/Collections/EMPTY_LIST Ljava/util/List;
astore 2
aload 0
getfield org/xbill/DNS/OPTRecord/options Ljava/util/List;
invokeinterface java/util/List/iterator()Ljava/util/Iterator; 0
astore 4
L2:
aload 2
astore 3
aload 4
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 4
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast org/xbill/DNS/EDNSOption
astore 5
aload 5
invokevirtual org/xbill/DNS/EDNSOption/getCode()I
iload 1
if_icmpne L2
aload 2
astore 3
aload 2
getstatic java/util/Collections/EMPTY_LIST Ljava/util/List;
if_acmpne L3
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 3
L3:
aload 3
aload 5
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
aload 3
astore 2
goto L2
.limit locals 6
.limit stack 2
.end method

.method public getPayloadSize()I
aload 0
getfield org/xbill/DNS/OPTRecord/dclass I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getVersion()I
aload 0
getfield org/xbill/DNS/OPTRecord/ttl J
bipush 16
lushr
ldc2_w 255L
land
l2i
ireturn
.limit locals 1
.limit stack 4
.end method

.method rdataFromString(Lorg/xbill/DNS/Tokenizer;Lorg/xbill/DNS/Name;)V
aload 1
ldc "no text format defined for OPT"
invokevirtual org/xbill/DNS/Tokenizer/exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;
athrow
.limit locals 3
.limit stack 2
.end method

.method rrFromWire(Lorg/xbill/DNS/DNSInput;)V
aload 1
invokevirtual org/xbill/DNS/DNSInput/remaining()I
ifle L0
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield org/xbill/DNS/OPTRecord/options Ljava/util/List;
L0:
aload 1
invokevirtual org/xbill/DNS/DNSInput/remaining()I
ifle L1
aload 1
invokestatic org/xbill/DNS/EDNSOption/fromWire(Lorg/xbill/DNS/DNSInput;)Lorg/xbill/DNS/EDNSOption;
astore 2
aload 0
getfield org/xbill/DNS/OPTRecord/options Ljava/util/List;
aload 2
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
goto L0
L1:
return
.limit locals 3
.limit stack 3
.end method

.method rrToString()Ljava/lang/String;
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
astore 1
aload 0
getfield org/xbill/DNS/OPTRecord/options Ljava/util/List;
ifnull L0
aload 1
aload 0
getfield org/xbill/DNS/OPTRecord/options Ljava/util/List;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/Object;)Ljava/lang/StringBuffer;
pop
aload 1
ldc " "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
L0:
aload 1
ldc " ; payload "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
aload 0
invokevirtual org/xbill/DNS/OPTRecord/getPayloadSize()I
invokevirtual java/lang/StringBuffer/append(I)Ljava/lang/StringBuffer;
pop
aload 1
ldc ", xrcode "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
aload 0
invokevirtual org/xbill/DNS/OPTRecord/getExtendedRcode()I
invokevirtual java/lang/StringBuffer/append(I)Ljava/lang/StringBuffer;
pop
aload 1
ldc ", version "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
aload 0
invokevirtual org/xbill/DNS/OPTRecord/getVersion()I
invokevirtual java/lang/StringBuffer/append(I)Ljava/lang/StringBuffer;
pop
aload 1
ldc ", flags "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
aload 0
invokevirtual org/xbill/DNS/OPTRecord/getFlags()I
invokevirtual java/lang/StringBuffer/append(I)Ljava/lang/StringBuffer;
pop
aload 1
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
areturn
.limit locals 2
.limit stack 2
.end method

.method rrToWire(Lorg/xbill/DNS/DNSOutput;Lorg/xbill/DNS/Compression;Z)V
aload 0
getfield org/xbill/DNS/OPTRecord/options Ljava/util/List;
ifnonnull L0
L1:
return
L0:
aload 0
getfield org/xbill/DNS/OPTRecord/options Ljava/util/List;
invokeinterface java/util/List/iterator()Ljava/util/Iterator; 0
astore 2
L2:
aload 2
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 2
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast org/xbill/DNS/EDNSOption
aload 1
invokevirtual org/xbill/DNS/EDNSOption/toWire(Lorg/xbill/DNS/DNSOutput;)V
goto L2
.limit locals 4
.limit stack 2
.end method
