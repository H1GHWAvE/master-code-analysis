.bytecode 50.0
.class public synchronized org/xbill/DNS/TSIG
.super java/lang/Object

.field public static final 'FUDGE' S = 300


.field public static final 'HMAC' Lorg/xbill/DNS/Name;

.field public static final 'HMAC_MD5' Lorg/xbill/DNS/Name;

.field private static final 'HMAC_MD5_STR' Ljava/lang/String; = "HMAC-MD5.SIG-ALG.REG.INT."

.field public static final 'HMAC_SHA1' Lorg/xbill/DNS/Name;

.field private static final 'HMAC_SHA1_STR' Ljava/lang/String; = "hmac-sha1."

.field public static final 'HMAC_SHA224' Lorg/xbill/DNS/Name;

.field private static final 'HMAC_SHA224_STR' Ljava/lang/String; = "hmac-sha224."

.field public static final 'HMAC_SHA256' Lorg/xbill/DNS/Name;

.field private static final 'HMAC_SHA256_STR' Ljava/lang/String; = "hmac-sha256."

.field public static final 'HMAC_SHA384' Lorg/xbill/DNS/Name;

.field private static final 'HMAC_SHA384_STR' Ljava/lang/String; = "hmac-sha384."

.field public static final 'HMAC_SHA512' Lorg/xbill/DNS/Name;

.field private static final 'HMAC_SHA512_STR' Ljava/lang/String; = "hmac-sha512."

.field private 'alg' Lorg/xbill/DNS/Name;

.field private 'digest' Ljava/lang/String;

.field private 'digestBlockLength' I

.field private 'key' [B

.field private 'name' Lorg/xbill/DNS/Name;

.method static <clinit>()V
ldc "HMAC-MD5.SIG-ALG.REG.INT."
invokestatic org/xbill/DNS/Name/fromConstantString(Ljava/lang/String;)Lorg/xbill/DNS/Name;
astore 0
aload 0
putstatic org/xbill/DNS/TSIG/HMAC_MD5 Lorg/xbill/DNS/Name;
aload 0
putstatic org/xbill/DNS/TSIG/HMAC Lorg/xbill/DNS/Name;
ldc "hmac-sha1."
invokestatic org/xbill/DNS/Name/fromConstantString(Ljava/lang/String;)Lorg/xbill/DNS/Name;
putstatic org/xbill/DNS/TSIG/HMAC_SHA1 Lorg/xbill/DNS/Name;
ldc "hmac-sha224."
invokestatic org/xbill/DNS/Name/fromConstantString(Ljava/lang/String;)Lorg/xbill/DNS/Name;
putstatic org/xbill/DNS/TSIG/HMAC_SHA224 Lorg/xbill/DNS/Name;
ldc "hmac-sha256."
invokestatic org/xbill/DNS/Name/fromConstantString(Ljava/lang/String;)Lorg/xbill/DNS/Name;
putstatic org/xbill/DNS/TSIG/HMAC_SHA256 Lorg/xbill/DNS/Name;
ldc "hmac-sha384."
invokestatic org/xbill/DNS/Name/fromConstantString(Ljava/lang/String;)Lorg/xbill/DNS/Name;
putstatic org/xbill/DNS/TSIG/HMAC_SHA384 Lorg/xbill/DNS/Name;
ldc "hmac-sha512."
invokestatic org/xbill/DNS/Name/fromConstantString(Ljava/lang/String;)Lorg/xbill/DNS/Name;
putstatic org/xbill/DNS/TSIG/HMAC_SHA512 Lorg/xbill/DNS/Name;
return
.limit locals 1
.limit stack 1
.end method

.method public <init>(Ljava/lang/String;Ljava/lang/String;)V
aload 0
getstatic org/xbill/DNS/TSIG/HMAC_MD5 Lorg/xbill/DNS/Name;
aload 1
aload 2
invokespecial org/xbill/DNS/TSIG/<init>(Lorg/xbill/DNS/Name;Ljava/lang/String;Ljava/lang/String;)V
return
.limit locals 3
.limit stack 4
.end method

.method public <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
aload 0
getstatic org/xbill/DNS/TSIG/HMAC_MD5 Lorg/xbill/DNS/Name;
aload 2
aload 3
invokespecial org/xbill/DNS/TSIG/<init>(Lorg/xbill/DNS/Name;Ljava/lang/String;Ljava/lang/String;)V
aload 1
ldc "hmac-md5"
invokevirtual java/lang/String/equalsIgnoreCase(Ljava/lang/String;)Z
ifeq L0
aload 0
getstatic org/xbill/DNS/TSIG/HMAC_MD5 Lorg/xbill/DNS/Name;
putfield org/xbill/DNS/TSIG/alg Lorg/xbill/DNS/Name;
L1:
aload 0
invokespecial org/xbill/DNS/TSIG/getDigest()V
return
L0:
aload 1
ldc "hmac-sha1"
invokevirtual java/lang/String/equalsIgnoreCase(Ljava/lang/String;)Z
ifeq L2
aload 0
getstatic org/xbill/DNS/TSIG/HMAC_SHA1 Lorg/xbill/DNS/Name;
putfield org/xbill/DNS/TSIG/alg Lorg/xbill/DNS/Name;
goto L1
L2:
aload 1
ldc "hmac-sha224"
invokevirtual java/lang/String/equalsIgnoreCase(Ljava/lang/String;)Z
ifeq L3
aload 0
getstatic org/xbill/DNS/TSIG/HMAC_SHA224 Lorg/xbill/DNS/Name;
putfield org/xbill/DNS/TSIG/alg Lorg/xbill/DNS/Name;
goto L1
L3:
aload 1
ldc "hmac-sha256"
invokevirtual java/lang/String/equalsIgnoreCase(Ljava/lang/String;)Z
ifeq L4
aload 0
getstatic org/xbill/DNS/TSIG/HMAC_SHA256 Lorg/xbill/DNS/Name;
putfield org/xbill/DNS/TSIG/alg Lorg/xbill/DNS/Name;
goto L1
L4:
aload 1
ldc "hmac-sha384"
invokevirtual java/lang/String/equalsIgnoreCase(Ljava/lang/String;)Z
ifeq L5
aload 0
getstatic org/xbill/DNS/TSIG/HMAC_SHA384 Lorg/xbill/DNS/Name;
putfield org/xbill/DNS/TSIG/alg Lorg/xbill/DNS/Name;
goto L1
L5:
aload 1
ldc "hmac-sha512"
invokevirtual java/lang/String/equalsIgnoreCase(Ljava/lang/String;)Z
ifeq L6
aload 0
getstatic org/xbill/DNS/TSIG/HMAC_SHA512 Lorg/xbill/DNS/Name;
putfield org/xbill/DNS/TSIG/alg Lorg/xbill/DNS/Name;
goto L1
L6:
new java/lang/IllegalArgumentException
dup
ldc "Invalid TSIG algorithm"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
.limit locals 4
.limit stack 4
.end method

.method public <init>(Lorg/xbill/DNS/Name;Ljava/lang/String;Ljava/lang/String;)V
.catch org/xbill/DNS/TextParseException from L0 to L1 using L2
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 3
invokestatic org/xbill/DNS/utils/base64/fromString(Ljava/lang/String;)[B
putfield org/xbill/DNS/TSIG/key [B
aload 0
getfield org/xbill/DNS/TSIG/key [B
ifnonnull L0
new java/lang/IllegalArgumentException
dup
ldc "Invalid TSIG key string"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
aload 2
getstatic org/xbill/DNS/Name/root Lorg/xbill/DNS/Name;
invokestatic org/xbill/DNS/Name/fromString(Ljava/lang/String;Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/Name;
putfield org/xbill/DNS/TSIG/name Lorg/xbill/DNS/Name;
L1:
aload 0
aload 1
putfield org/xbill/DNS/TSIG/alg Lorg/xbill/DNS/Name;
aload 0
invokespecial org/xbill/DNS/TSIG/getDigest()V
return
L2:
astore 1
new java/lang/IllegalArgumentException
dup
ldc "Invalid TSIG key name"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
.limit locals 4
.limit stack 3
.end method

.method public <init>(Lorg/xbill/DNS/Name;Lorg/xbill/DNS/Name;[B)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 2
putfield org/xbill/DNS/TSIG/name Lorg/xbill/DNS/Name;
aload 0
aload 1
putfield org/xbill/DNS/TSIG/alg Lorg/xbill/DNS/Name;
aload 0
aload 3
putfield org/xbill/DNS/TSIG/key [B
aload 0
invokespecial org/xbill/DNS/TSIG/getDigest()V
return
.limit locals 4
.limit stack 2
.end method

.method public <init>(Lorg/xbill/DNS/Name;[B)V
aload 0
getstatic org/xbill/DNS/TSIG/HMAC_MD5 Lorg/xbill/DNS/Name;
aload 1
aload 2
invokespecial org/xbill/DNS/TSIG/<init>(Lorg/xbill/DNS/Name;Lorg/xbill/DNS/Name;[B)V
return
.limit locals 3
.limit stack 4
.end method

.method static access$000(Lorg/xbill/DNS/TSIG;)Ljava/lang/String;
aload 0
getfield org/xbill/DNS/TSIG/digest Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method static access$100(Lorg/xbill/DNS/TSIG;)I
aload 0
getfield org/xbill/DNS/TSIG/digestBlockLength I
ireturn
.limit locals 1
.limit stack 1
.end method

.method static access$200(Lorg/xbill/DNS/TSIG;)[B
aload 0
getfield org/xbill/DNS/TSIG/key [B
areturn
.limit locals 1
.limit stack 1
.end method

.method static access$300(Lorg/xbill/DNS/TSIG;)Lorg/xbill/DNS/Name;
aload 0
getfield org/xbill/DNS/TSIG/name Lorg/xbill/DNS/Name;
areturn
.limit locals 1
.limit stack 1
.end method

.method static access$400(Lorg/xbill/DNS/TSIG;)Lorg/xbill/DNS/Name;
aload 0
getfield org/xbill/DNS/TSIG/alg Lorg/xbill/DNS/Name;
areturn
.limit locals 1
.limit stack 1
.end method

.method public static fromString(Ljava/lang/String;)Lorg/xbill/DNS/TSIG;
.catch java/lang/IllegalArgumentException from L0 to L1 using L2
aload 0
ldc "[:/]"
iconst_3
invokevirtual java/lang/String/split(Ljava/lang/String;I)[Ljava/lang/String;
astore 2
aload 2
arraylength
iconst_2
if_icmpge L3
new java/lang/IllegalArgumentException
dup
ldc "Invalid TSIG key specification"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L3:
aload 2
astore 1
aload 2
arraylength
iconst_3
if_icmpne L4
L0:
new org/xbill/DNS/TSIG
dup
aload 2
iconst_0
aaload
aload 2
iconst_1
aaload
aload 2
iconst_2
aaload
invokespecial org/xbill/DNS/TSIG/<init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
astore 1
L1:
aload 1
areturn
L2:
astore 1
aload 0
ldc "[:/]"
iconst_2
invokevirtual java/lang/String/split(Ljava/lang/String;I)[Ljava/lang/String;
astore 1
L4:
new org/xbill/DNS/TSIG
dup
getstatic org/xbill/DNS/TSIG/HMAC_MD5 Lorg/xbill/DNS/Name;
aload 1
iconst_0
aaload
aload 1
iconst_1
aaload
invokespecial org/xbill/DNS/TSIG/<init>(Lorg/xbill/DNS/Name;Ljava/lang/String;Ljava/lang/String;)V
areturn
.limit locals 3
.limit stack 6
.end method

.method private getDigest()V
aload 0
getfield org/xbill/DNS/TSIG/alg Lorg/xbill/DNS/Name;
getstatic org/xbill/DNS/TSIG/HMAC_MD5 Lorg/xbill/DNS/Name;
invokevirtual org/xbill/DNS/Name/equals(Ljava/lang/Object;)Z
ifeq L0
aload 0
ldc "md5"
putfield org/xbill/DNS/TSIG/digest Ljava/lang/String;
aload 0
bipush 64
putfield org/xbill/DNS/TSIG/digestBlockLength I
return
L0:
aload 0
getfield org/xbill/DNS/TSIG/alg Lorg/xbill/DNS/Name;
getstatic org/xbill/DNS/TSIG/HMAC_SHA1 Lorg/xbill/DNS/Name;
invokevirtual org/xbill/DNS/Name/equals(Ljava/lang/Object;)Z
ifeq L1
aload 0
ldc "sha-1"
putfield org/xbill/DNS/TSIG/digest Ljava/lang/String;
aload 0
bipush 64
putfield org/xbill/DNS/TSIG/digestBlockLength I
return
L1:
aload 0
getfield org/xbill/DNS/TSIG/alg Lorg/xbill/DNS/Name;
getstatic org/xbill/DNS/TSIG/HMAC_SHA224 Lorg/xbill/DNS/Name;
invokevirtual org/xbill/DNS/Name/equals(Ljava/lang/Object;)Z
ifeq L2
aload 0
ldc "sha-224"
putfield org/xbill/DNS/TSIG/digest Ljava/lang/String;
aload 0
bipush 64
putfield org/xbill/DNS/TSIG/digestBlockLength I
return
L2:
aload 0
getfield org/xbill/DNS/TSIG/alg Lorg/xbill/DNS/Name;
getstatic org/xbill/DNS/TSIG/HMAC_SHA256 Lorg/xbill/DNS/Name;
invokevirtual org/xbill/DNS/Name/equals(Ljava/lang/Object;)Z
ifeq L3
aload 0
ldc "sha-256"
putfield org/xbill/DNS/TSIG/digest Ljava/lang/String;
aload 0
bipush 64
putfield org/xbill/DNS/TSIG/digestBlockLength I
return
L3:
aload 0
getfield org/xbill/DNS/TSIG/alg Lorg/xbill/DNS/Name;
getstatic org/xbill/DNS/TSIG/HMAC_SHA512 Lorg/xbill/DNS/Name;
invokevirtual org/xbill/DNS/Name/equals(Ljava/lang/Object;)Z
ifeq L4
aload 0
ldc "sha-512"
putfield org/xbill/DNS/TSIG/digest Ljava/lang/String;
aload 0
sipush 128
putfield org/xbill/DNS/TSIG/digestBlockLength I
return
L4:
aload 0
getfield org/xbill/DNS/TSIG/alg Lorg/xbill/DNS/Name;
getstatic org/xbill/DNS/TSIG/HMAC_SHA384 Lorg/xbill/DNS/Name;
invokevirtual org/xbill/DNS/Name/equals(Ljava/lang/Object;)Z
ifeq L5
aload 0
ldc "sha-384"
putfield org/xbill/DNS/TSIG/digest Ljava/lang/String;
aload 0
sipush 128
putfield org/xbill/DNS/TSIG/digestBlockLength I
return
L5:
new java/lang/IllegalArgumentException
dup
ldc "Invalid algorithm"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
.limit locals 1
.limit stack 3
.end method

.method public apply(Lorg/xbill/DNS/Message;ILorg/xbill/DNS/TSIGRecord;)V
aload 1
aload 0
aload 1
aload 1
invokevirtual org/xbill/DNS/Message/toWire()[B
iload 2
aload 3
invokevirtual org/xbill/DNS/TSIG/generate(Lorg/xbill/DNS/Message;[BILorg/xbill/DNS/TSIGRecord;)Lorg/xbill/DNS/TSIGRecord;
iconst_3
invokevirtual org/xbill/DNS/Message/addRecord(Lorg/xbill/DNS/Record;I)V
aload 1
iconst_3
putfield org/xbill/DNS/Message/tsigState I
return
.limit locals 4
.limit stack 6
.end method

.method public apply(Lorg/xbill/DNS/Message;Lorg/xbill/DNS/TSIGRecord;)V
aload 0
aload 1
iconst_0
aload 2
invokevirtual org/xbill/DNS/TSIG/apply(Lorg/xbill/DNS/Message;ILorg/xbill/DNS/TSIGRecord;)V
return
.limit locals 3
.limit stack 4
.end method

.method public applyStream(Lorg/xbill/DNS/Message;Lorg/xbill/DNS/TSIGRecord;Z)V
iload 3
ifeq L0
aload 0
aload 1
aload 2
invokevirtual org/xbill/DNS/TSIG/apply(Lorg/xbill/DNS/Message;Lorg/xbill/DNS/TSIGRecord;)V
return
L0:
new java/util/Date
dup
invokespecial java/util/Date/<init>()V
astore 8
new org/xbill/DNS/utils/HMAC
dup
aload 0
getfield org/xbill/DNS/TSIG/digest Ljava/lang/String;
aload 0
getfield org/xbill/DNS/TSIG/digestBlockLength I
aload 0
getfield org/xbill/DNS/TSIG/key [B
invokespecial org/xbill/DNS/utils/HMAC/<init>(Ljava/lang/String;I[B)V
astore 9
ldc "tsigfudge"
invokestatic org/xbill/DNS/Options/intValue(Ljava/lang/String;)I
istore 5
iload 5
iflt L1
iload 5
istore 4
iload 5
sipush 32767
if_icmple L2
L1:
sipush 300
istore 4
L2:
new org/xbill/DNS/DNSOutput
dup
invokespecial org/xbill/DNS/DNSOutput/<init>()V
astore 10
aload 10
aload 2
invokevirtual org/xbill/DNS/TSIGRecord/getSignature()[B
arraylength
invokevirtual org/xbill/DNS/DNSOutput/writeU16(I)V
aload 9
aload 10
invokevirtual org/xbill/DNS/DNSOutput/toByteArray()[B
invokevirtual org/xbill/DNS/utils/HMAC/update([B)V
aload 9
aload 2
invokevirtual org/xbill/DNS/TSIGRecord/getSignature()[B
invokevirtual org/xbill/DNS/utils/HMAC/update([B)V
aload 9
aload 1
invokevirtual org/xbill/DNS/Message/toWire()[B
invokevirtual org/xbill/DNS/utils/HMAC/update([B)V
new org/xbill/DNS/DNSOutput
dup
invokespecial org/xbill/DNS/DNSOutput/<init>()V
astore 2
aload 8
invokevirtual java/util/Date/getTime()J
ldc2_w 1000L
ldiv
lstore 6
aload 2
lload 6
bipush 32
lshr
l2i
invokevirtual org/xbill/DNS/DNSOutput/writeU16(I)V
aload 2
lload 6
ldc2_w 4294967295L
land
invokevirtual org/xbill/DNS/DNSOutput/writeU32(J)V
aload 2
iload 4
invokevirtual org/xbill/DNS/DNSOutput/writeU16(I)V
aload 9
aload 2
invokevirtual org/xbill/DNS/DNSOutput/toByteArray()[B
invokevirtual org/xbill/DNS/utils/HMAC/update([B)V
aload 9
invokevirtual org/xbill/DNS/utils/HMAC/sign()[B
astore 2
aload 1
new org/xbill/DNS/TSIGRecord
dup
aload 0
getfield org/xbill/DNS/TSIG/name Lorg/xbill/DNS/Name;
sipush 255
lconst_0
aload 0
getfield org/xbill/DNS/TSIG/alg Lorg/xbill/DNS/Name;
aload 8
iload 4
aload 2
aload 1
invokevirtual org/xbill/DNS/Message/getHeader()Lorg/xbill/DNS/Header;
invokevirtual org/xbill/DNS/Header/getID()I
iconst_0
aconst_null
invokespecial org/xbill/DNS/TSIGRecord/<init>(Lorg/xbill/DNS/Name;IJLorg/xbill/DNS/Name;Ljava/util/Date;I[BII[B)V
iconst_3
invokevirtual org/xbill/DNS/Message/addRecord(Lorg/xbill/DNS/Record;I)V
aload 1
iconst_3
putfield org/xbill/DNS/Message/tsigState I
return
.limit locals 11
.limit stack 14
.end method

.method public generate(Lorg/xbill/DNS/Message;[BILorg/xbill/DNS/TSIGRecord;)Lorg/xbill/DNS/TSIGRecord;
iload 3
bipush 18
if_icmpeq L0
new java/util/Date
dup
invokespecial java/util/Date/<init>()V
astore 9
L1:
aconst_null
astore 10
iload 3
ifeq L2
iload 3
bipush 18
if_icmpne L3
L2:
new org/xbill/DNS/utils/HMAC
dup
aload 0
getfield org/xbill/DNS/TSIG/digest Ljava/lang/String;
aload 0
getfield org/xbill/DNS/TSIG/digestBlockLength I
aload 0
getfield org/xbill/DNS/TSIG/key [B
invokespecial org/xbill/DNS/utils/HMAC/<init>(Ljava/lang/String;I[B)V
astore 10
L3:
ldc "tsigfudge"
invokestatic org/xbill/DNS/Options/intValue(Ljava/lang/String;)I
istore 6
iload 6
iflt L4
iload 6
istore 5
iload 6
sipush 32767
if_icmple L5
L4:
sipush 300
istore 5
L5:
aload 4
ifnull L6
new org/xbill/DNS/DNSOutput
dup
invokespecial org/xbill/DNS/DNSOutput/<init>()V
astore 11
aload 11
aload 4
invokevirtual org/xbill/DNS/TSIGRecord/getSignature()[B
arraylength
invokevirtual org/xbill/DNS/DNSOutput/writeU16(I)V
aload 10
ifnull L6
aload 10
aload 11
invokevirtual org/xbill/DNS/DNSOutput/toByteArray()[B
invokevirtual org/xbill/DNS/utils/HMAC/update([B)V
aload 10
aload 4
invokevirtual org/xbill/DNS/TSIGRecord/getSignature()[B
invokevirtual org/xbill/DNS/utils/HMAC/update([B)V
L6:
aload 10
ifnull L7
aload 10
aload 2
invokevirtual org/xbill/DNS/utils/HMAC/update([B)V
L7:
new org/xbill/DNS/DNSOutput
dup
invokespecial org/xbill/DNS/DNSOutput/<init>()V
astore 2
aload 0
getfield org/xbill/DNS/TSIG/name Lorg/xbill/DNS/Name;
aload 2
invokevirtual org/xbill/DNS/Name/toWireCanonical(Lorg/xbill/DNS/DNSOutput;)V
aload 2
sipush 255
invokevirtual org/xbill/DNS/DNSOutput/writeU16(I)V
aload 2
lconst_0
invokevirtual org/xbill/DNS/DNSOutput/writeU32(J)V
aload 0
getfield org/xbill/DNS/TSIG/alg Lorg/xbill/DNS/Name;
aload 2
invokevirtual org/xbill/DNS/Name/toWireCanonical(Lorg/xbill/DNS/DNSOutput;)V
aload 9
invokevirtual java/util/Date/getTime()J
ldc2_w 1000L
ldiv
lstore 7
aload 2
lload 7
bipush 32
lshr
l2i
invokevirtual org/xbill/DNS/DNSOutput/writeU16(I)V
aload 2
lload 7
ldc2_w 4294967295L
land
invokevirtual org/xbill/DNS/DNSOutput/writeU32(J)V
aload 2
iload 5
invokevirtual org/xbill/DNS/DNSOutput/writeU16(I)V
aload 2
iload 3
invokevirtual org/xbill/DNS/DNSOutput/writeU16(I)V
aload 2
iconst_0
invokevirtual org/xbill/DNS/DNSOutput/writeU16(I)V
aload 10
ifnull L8
aload 10
aload 2
invokevirtual org/xbill/DNS/DNSOutput/toByteArray()[B
invokevirtual org/xbill/DNS/utils/HMAC/update([B)V
L8:
aload 10
ifnull L9
aload 10
invokevirtual org/xbill/DNS/utils/HMAC/sign()[B
astore 2
L10:
aconst_null
astore 4
iload 3
bipush 18
if_icmpne L11
new org/xbill/DNS/DNSOutput
dup
invokespecial org/xbill/DNS/DNSOutput/<init>()V
astore 4
new java/util/Date
dup
invokespecial java/util/Date/<init>()V
invokevirtual java/util/Date/getTime()J
ldc2_w 1000L
ldiv
lstore 7
aload 4
lload 7
bipush 32
lshr
l2i
invokevirtual org/xbill/DNS/DNSOutput/writeU16(I)V
aload 4
lload 7
ldc2_w 4294967295L
land
invokevirtual org/xbill/DNS/DNSOutput/writeU32(J)V
aload 4
invokevirtual org/xbill/DNS/DNSOutput/toByteArray()[B
astore 4
L11:
new org/xbill/DNS/TSIGRecord
dup
aload 0
getfield org/xbill/DNS/TSIG/name Lorg/xbill/DNS/Name;
sipush 255
lconst_0
aload 0
getfield org/xbill/DNS/TSIG/alg Lorg/xbill/DNS/Name;
aload 9
iload 5
aload 2
aload 1
invokevirtual org/xbill/DNS/Message/getHeader()Lorg/xbill/DNS/Header;
invokevirtual org/xbill/DNS/Header/getID()I
iload 3
aload 4
invokespecial org/xbill/DNS/TSIGRecord/<init>(Lorg/xbill/DNS/Name;IJLorg/xbill/DNS/Name;Ljava/util/Date;I[BII[B)V
areturn
L0:
aload 4
invokevirtual org/xbill/DNS/TSIGRecord/getTimeSigned()Ljava/util/Date;
astore 9
goto L1
L9:
iconst_0
newarray byte
astore 2
goto L10
.limit locals 12
.limit stack 13
.end method

.method public recordLength()I
aload 0
getfield org/xbill/DNS/TSIG/name Lorg/xbill/DNS/Name;
invokevirtual org/xbill/DNS/Name/length()S
bipush 10
iadd
aload 0
getfield org/xbill/DNS/TSIG/alg Lorg/xbill/DNS/Name;
invokevirtual org/xbill/DNS/Name/length()S
iadd
bipush 8
iadd
bipush 18
iadd
iconst_4
iadd
bipush 8
iadd
ireturn
.limit locals 1
.limit stack 2
.end method

.method public verify(Lorg/xbill/DNS/Message;[BILorg/xbill/DNS/TSIGRecord;)B
aload 1
iconst_4
putfield org/xbill/DNS/Message/tsigState I
aload 1
invokevirtual org/xbill/DNS/Message/getTSIG()Lorg/xbill/DNS/TSIGRecord;
astore 13
new org/xbill/DNS/utils/HMAC
dup
aload 0
getfield org/xbill/DNS/TSIG/digest Ljava/lang/String;
aload 0
getfield org/xbill/DNS/TSIG/digestBlockLength I
aload 0
getfield org/xbill/DNS/TSIG/key [B
invokespecial org/xbill/DNS/utils/HMAC/<init>(Ljava/lang/String;I[B)V
astore 12
aload 13
ifnonnull L0
iconst_1
ireturn
L0:
aload 13
invokevirtual org/xbill/DNS/TSIGRecord/getName()Lorg/xbill/DNS/Name;
aload 0
getfield org/xbill/DNS/TSIG/name Lorg/xbill/DNS/Name;
invokevirtual org/xbill/DNS/Name/equals(Ljava/lang/Object;)Z
ifeq L1
aload 13
invokevirtual org/xbill/DNS/TSIGRecord/getAlgorithm()Lorg/xbill/DNS/Name;
aload 0
getfield org/xbill/DNS/TSIG/alg Lorg/xbill/DNS/Name;
invokevirtual org/xbill/DNS/Name/equals(Ljava/lang/Object;)Z
ifne L2
L1:
ldc "verbose"
invokestatic org/xbill/DNS/Options/check(Ljava/lang/String;)Z
ifeq L3
getstatic java/lang/System/err Ljava/io/PrintStream;
ldc "BADKEY failure"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L3:
bipush 17
ireturn
L2:
invokestatic java/lang/System/currentTimeMillis()J
lstore 6
aload 13
invokevirtual org/xbill/DNS/TSIGRecord/getTimeSigned()Ljava/util/Date;
invokevirtual java/util/Date/getTime()J
lstore 8
aload 13
invokevirtual org/xbill/DNS/TSIGRecord/getFudge()I
i2l
lstore 10
lload 6
lload 8
lsub
invokestatic java/lang/Math/abs(J)J
ldc2_w 1000L
lload 10
lmul
lcmp
ifle L4
ldc "verbose"
invokestatic org/xbill/DNS/Options/check(Ljava/lang/String;)Z
ifeq L5
getstatic java/lang/System/err Ljava/io/PrintStream;
ldc "BADTIME failure"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L5:
bipush 18
ireturn
L4:
aload 4
ifnull L6
aload 13
invokevirtual org/xbill/DNS/TSIGRecord/getError()I
bipush 17
if_icmpeq L6
aload 13
invokevirtual org/xbill/DNS/TSIGRecord/getError()I
bipush 16
if_icmpeq L6
new org/xbill/DNS/DNSOutput
dup
invokespecial org/xbill/DNS/DNSOutput/<init>()V
astore 14
aload 14
aload 4
invokevirtual org/xbill/DNS/TSIGRecord/getSignature()[B
arraylength
invokevirtual org/xbill/DNS/DNSOutput/writeU16(I)V
aload 12
aload 14
invokevirtual org/xbill/DNS/DNSOutput/toByteArray()[B
invokevirtual org/xbill/DNS/utils/HMAC/update([B)V
aload 12
aload 4
invokevirtual org/xbill/DNS/TSIGRecord/getSignature()[B
invokevirtual org/xbill/DNS/utils/HMAC/update([B)V
L6:
aload 1
invokevirtual org/xbill/DNS/Message/getHeader()Lorg/xbill/DNS/Header;
iconst_3
invokevirtual org/xbill/DNS/Header/decCount(I)V
aload 1
invokevirtual org/xbill/DNS/Message/getHeader()Lorg/xbill/DNS/Header;
invokevirtual org/xbill/DNS/Header/toWire()[B
astore 4
aload 1
invokevirtual org/xbill/DNS/Message/getHeader()Lorg/xbill/DNS/Header;
iconst_3
invokevirtual org/xbill/DNS/Header/incCount(I)V
aload 12
aload 4
invokevirtual org/xbill/DNS/utils/HMAC/update([B)V
aload 1
getfield org/xbill/DNS/Message/tsigstart I
istore 3
aload 4
arraylength
istore 5
aload 12
aload 2
aload 4
arraylength
iload 3
iload 5
isub
invokevirtual org/xbill/DNS/utils/HMAC/update([BII)V
new org/xbill/DNS/DNSOutput
dup
invokespecial org/xbill/DNS/DNSOutput/<init>()V
astore 2
aload 13
invokevirtual org/xbill/DNS/TSIGRecord/getName()Lorg/xbill/DNS/Name;
aload 2
invokevirtual org/xbill/DNS/Name/toWireCanonical(Lorg/xbill/DNS/DNSOutput;)V
aload 2
aload 13
getfield org/xbill/DNS/TSIGRecord/dclass I
invokevirtual org/xbill/DNS/DNSOutput/writeU16(I)V
aload 2
aload 13
getfield org/xbill/DNS/TSIGRecord/ttl J
invokevirtual org/xbill/DNS/DNSOutput/writeU32(J)V
aload 13
invokevirtual org/xbill/DNS/TSIGRecord/getAlgorithm()Lorg/xbill/DNS/Name;
aload 2
invokevirtual org/xbill/DNS/Name/toWireCanonical(Lorg/xbill/DNS/DNSOutput;)V
aload 13
invokevirtual org/xbill/DNS/TSIGRecord/getTimeSigned()Ljava/util/Date;
invokevirtual java/util/Date/getTime()J
ldc2_w 1000L
ldiv
lstore 6
aload 2
lload 6
bipush 32
lshr
l2i
invokevirtual org/xbill/DNS/DNSOutput/writeU16(I)V
aload 2
lload 6
ldc2_w 4294967295L
land
invokevirtual org/xbill/DNS/DNSOutput/writeU32(J)V
aload 2
aload 13
invokevirtual org/xbill/DNS/TSIGRecord/getFudge()I
invokevirtual org/xbill/DNS/DNSOutput/writeU16(I)V
aload 2
aload 13
invokevirtual org/xbill/DNS/TSIGRecord/getError()I
invokevirtual org/xbill/DNS/DNSOutput/writeU16(I)V
aload 13
invokevirtual org/xbill/DNS/TSIGRecord/getOther()[B
ifnull L7
aload 2
aload 13
invokevirtual org/xbill/DNS/TSIGRecord/getOther()[B
arraylength
invokevirtual org/xbill/DNS/DNSOutput/writeU16(I)V
aload 2
aload 13
invokevirtual org/xbill/DNS/TSIGRecord/getOther()[B
invokevirtual org/xbill/DNS/DNSOutput/writeByteArray([B)V
L8:
aload 12
aload 2
invokevirtual org/xbill/DNS/DNSOutput/toByteArray()[B
invokevirtual org/xbill/DNS/utils/HMAC/update([B)V
aload 13
invokevirtual org/xbill/DNS/TSIGRecord/getSignature()[B
astore 2
aload 12
invokevirtual org/xbill/DNS/utils/HMAC/digestLength()I
istore 5
aload 0
getfield org/xbill/DNS/TSIG/digest Ljava/lang/String;
ldc "md5"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L9
bipush 10
istore 3
L10:
aload 2
arraylength
iload 5
if_icmple L11
ldc "verbose"
invokestatic org/xbill/DNS/Options/check(Ljava/lang/String;)Z
ifeq L12
getstatic java/lang/System/err Ljava/io/PrintStream;
ldc "BADSIG: signature too long"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L12:
bipush 16
ireturn
L7:
aload 2
iconst_0
invokevirtual org/xbill/DNS/DNSOutput/writeU16(I)V
goto L8
L9:
iload 5
iconst_2
idiv
istore 3
goto L10
L11:
aload 2
arraylength
iload 3
if_icmpge L13
ldc "verbose"
invokestatic org/xbill/DNS/Options/check(Ljava/lang/String;)Z
ifeq L14
getstatic java/lang/System/err Ljava/io/PrintStream;
ldc "BADSIG: signature too short"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L14:
bipush 16
ireturn
L13:
aload 12
aload 2
iconst_1
invokevirtual org/xbill/DNS/utils/HMAC/verify([BZ)Z
ifne L15
ldc "verbose"
invokestatic org/xbill/DNS/Options/check(Ljava/lang/String;)Z
ifeq L16
getstatic java/lang/System/err Ljava/io/PrintStream;
ldc "BADSIG: signature verification"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L16:
bipush 16
ireturn
L15:
aload 1
iconst_1
putfield org/xbill/DNS/Message/tsigState I
iconst_0
ireturn
.limit locals 15
.limit stack 6
.end method

.method public verify(Lorg/xbill/DNS/Message;[BLorg/xbill/DNS/TSIGRecord;)I
aload 0
aload 1
aload 2
aload 2
arraylength
aload 3
invokevirtual org/xbill/DNS/TSIG/verify(Lorg/xbill/DNS/Message;[BILorg/xbill/DNS/TSIGRecord;)B
ireturn
.limit locals 4
.limit stack 5
.end method
