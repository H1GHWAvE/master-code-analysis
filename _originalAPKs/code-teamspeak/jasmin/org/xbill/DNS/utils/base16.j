.bytecode 50.0
.class public synchronized org/xbill/DNS/utils/base16
.super java/lang/Object

.field private static final 'Base16' Ljava/lang/String; = "0123456789ABCDEF"

.method private <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static fromString(Ljava/lang/String;)[B
.catch java/io/IOException from L0 to L1 using L2
iconst_0
istore 2
new java/io/ByteArrayOutputStream
dup
invokespecial java/io/ByteArrayOutputStream/<init>()V
astore 4
aload 0
invokevirtual java/lang/String/getBytes()[B
astore 0
iconst_0
istore 1
L3:
iload 1
aload 0
arraylength
if_icmpge L4
aload 0
iload 1
baload
i2c
invokestatic java/lang/Character/isWhitespace(C)Z
ifne L5
aload 4
aload 0
iload 1
baload
invokevirtual java/io/ByteArrayOutputStream/write(I)V
L5:
iload 1
iconst_1
iadd
istore 1
goto L3
L4:
aload 4
invokevirtual java/io/ByteArrayOutputStream/toByteArray()[B
astore 0
aload 0
arraylength
iconst_2
irem
ifeq L6
aconst_null
areturn
L6:
aload 4
invokevirtual java/io/ByteArrayOutputStream/reset()V
new java/io/DataOutputStream
dup
aload 4
invokespecial java/io/DataOutputStream/<init>(Ljava/io/OutputStream;)V
astore 5
iload 2
istore 1
L7:
iload 1
aload 0
arraylength
if_icmpge L8
ldc "0123456789ABCDEF"
aload 0
iload 1
baload
i2c
invokestatic java/lang/Character/toUpperCase(C)C
invokevirtual java/lang/String/indexOf(I)I
i2b
istore 2
ldc "0123456789ABCDEF"
aload 0
iload 1
iconst_1
iadd
baload
i2c
invokestatic java/lang/Character/toUpperCase(C)C
invokevirtual java/lang/String/indexOf(I)I
i2b
istore 3
L0:
aload 5
iload 2
iconst_4
ishl
iload 3
iadd
invokevirtual java/io/DataOutputStream/writeByte(I)V
L1:
iload 1
iconst_2
iadd
istore 1
goto L7
L8:
aload 4
invokevirtual java/io/ByteArrayOutputStream/toByteArray()[B
areturn
L2:
astore 6
goto L1
.limit locals 7
.limit stack 4
.end method

.method public static toString([B)Ljava/lang/String;
new java/io/ByteArrayOutputStream
dup
invokespecial java/io/ByteArrayOutputStream/<init>()V
astore 4
iconst_0
istore 1
L0:
iload 1
aload 0
arraylength
if_icmpge L1
aload 0
iload 1
baload
sipush 255
iand
i2s
istore 3
iload 3
iconst_4
ishr
i2b
istore 2
iload 3
bipush 15
iand
i2b
istore 3
aload 4
ldc "0123456789ABCDEF"
iload 2
invokevirtual java/lang/String/charAt(I)C
invokevirtual java/io/ByteArrayOutputStream/write(I)V
aload 4
ldc "0123456789ABCDEF"
iload 3
invokevirtual java/lang/String/charAt(I)C
invokevirtual java/io/ByteArrayOutputStream/write(I)V
iload 1
iconst_1
iadd
istore 1
goto L0
L1:
new java/lang/String
dup
aload 4
invokevirtual java/io/ByteArrayOutputStream/toByteArray()[B
invokespecial java/lang/String/<init>([B)V
areturn
.limit locals 5
.limit stack 3
.end method
