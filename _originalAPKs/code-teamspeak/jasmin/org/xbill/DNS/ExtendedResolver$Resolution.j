.bytecode 50.0
.class synchronized org/xbill/DNS/ExtendedResolver$Resolution
.super java/lang/Object
.implements org/xbill/DNS/ResolverListener

.field 'done' Z

.field 'inprogress' [Ljava/lang/Object;

.field 'listener' Lorg/xbill/DNS/ResolverListener;

.field 'outstanding' I

.field 'query' Lorg/xbill/DNS/Message;

.field 'resolvers' [Lorg/xbill/DNS/Resolver;

.field 'response' Lorg/xbill/DNS/Message;

.field 'retries' I

.field 'sent' [I

.field 'thrown' Ljava/lang/Throwable;

.method public <init>(Lorg/xbill/DNS/ExtendedResolver;Lorg/xbill/DNS/Message;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 1
invokestatic org/xbill/DNS/ExtendedResolver/access$000(Lorg/xbill/DNS/ExtendedResolver;)Ljava/util/List;
astore 6
aload 0
aload 6
aload 6
invokeinterface java/util/List/size()I 0
anewarray org/xbill/DNS/Resolver
invokeinterface java/util/List/toArray([Ljava/lang/Object;)[Ljava/lang/Object; 1
checkcast [Lorg/xbill/DNS/Resolver;
checkcast [Lorg/xbill/DNS/Resolver;
putfield org/xbill/DNS/ExtendedResolver$Resolution/resolvers [Lorg/xbill/DNS/Resolver;
aload 1
invokestatic org/xbill/DNS/ExtendedResolver/access$100(Lorg/xbill/DNS/ExtendedResolver;)Z
ifeq L0
aload 0
getfield org/xbill/DNS/ExtendedResolver$Resolution/resolvers [Lorg/xbill/DNS/Resolver;
arraylength
istore 4
aload 1
invokestatic org/xbill/DNS/ExtendedResolver/access$208(Lorg/xbill/DNS/ExtendedResolver;)I
iload 4
irem
istore 5
aload 1
invokestatic org/xbill/DNS/ExtendedResolver/access$200(Lorg/xbill/DNS/ExtendedResolver;)I
iload 4
if_icmple L1
aload 1
iload 4
invokestatic org/xbill/DNS/ExtendedResolver/access$244(Lorg/xbill/DNS/ExtendedResolver;I)I
pop
L1:
iload 5
ifle L0
iload 4
anewarray org/xbill/DNS/Resolver
astore 6
iconst_0
istore 3
L2:
iload 3
iload 4
if_icmpge L3
aload 6
iload 3
aload 0
getfield org/xbill/DNS/ExtendedResolver$Resolution/resolvers [Lorg/xbill/DNS/Resolver;
iload 3
iload 5
iadd
iload 4
irem
aaload
aastore
iload 3
iconst_1
iadd
istore 3
goto L2
L3:
aload 0
aload 6
putfield org/xbill/DNS/ExtendedResolver$Resolution/resolvers [Lorg/xbill/DNS/Resolver;
L0:
aload 0
aload 0
getfield org/xbill/DNS/ExtendedResolver$Resolution/resolvers [Lorg/xbill/DNS/Resolver;
arraylength
newarray int
putfield org/xbill/DNS/ExtendedResolver$Resolution/sent [I
aload 0
aload 0
getfield org/xbill/DNS/ExtendedResolver$Resolution/resolvers [Lorg/xbill/DNS/Resolver;
arraylength
anewarray java/lang/Object
putfield org/xbill/DNS/ExtendedResolver$Resolution/inprogress [Ljava/lang/Object;
aload 0
aload 1
invokestatic org/xbill/DNS/ExtendedResolver/access$300(Lorg/xbill/DNS/ExtendedResolver;)I
putfield org/xbill/DNS/ExtendedResolver$Resolution/retries I
aload 0
aload 2
putfield org/xbill/DNS/ExtendedResolver$Resolution/query Lorg/xbill/DNS/Message;
return
.limit locals 7
.limit stack 5
.end method

.method public handleException(Ljava/lang/Object;Ljava/lang/Exception;)V
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch all from L5 to L6 using L2
.catch all from L7 to L8 using L2
.catch all from L9 to L10 using L2
.catch all from L10 to L11 using L2
.catch all from L11 to L12 using L2
.catch all from L12 to L13 using L2
.catch all from L13 to L14 using L2
.catch all from L15 to L16 using L2
.catch all from L16 to L17 using L2
.catch all from L18 to L19 using L2
.catch all from L19 to L20 using L2
.catch all from L21 to L22 using L2
.catch all from L23 to L24 using L2
.catch all from L25 to L26 using L2
iconst_1
istore 4
ldc "verbose"
invokestatic org/xbill/DNS/Options/check(Ljava/lang/String;)Z
ifeq L27
getstatic java/lang/System/err Ljava/io/PrintStream;
new java/lang/StringBuffer
dup
ldc "ExtendedResolver: got "
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
aload 2
invokevirtual java/lang/StringBuffer/append(Ljava/lang/Object;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L27:
aload 0
monitorenter
L0:
aload 0
aload 0
getfield org/xbill/DNS/ExtendedResolver$Resolution/outstanding I
iconst_1
isub
putfield org/xbill/DNS/ExtendedResolver$Resolution/outstanding I
aload 0
getfield org/xbill/DNS/ExtendedResolver$Resolution/done Z
ifeq L28
aload 0
monitorexit
L1:
return
L3:
iload 3
aload 0
getfield org/xbill/DNS/ExtendedResolver$Resolution/inprogress [Ljava/lang/Object;
arraylength
if_icmpge L5
aload 0
getfield org/xbill/DNS/ExtendedResolver$Resolution/inprogress [Ljava/lang/Object;
iload 3
aaload
aload 1
if_acmpeq L5
L4:
iload 3
iconst_1
iadd
istore 3
goto L3
L5:
iload 3
aload 0
getfield org/xbill/DNS/ExtendedResolver$Resolution/inprogress [Ljava/lang/Object;
arraylength
if_icmpne L9
aload 0
monitorexit
L6:
return
L2:
astore 1
L7:
aload 0
monitorexit
L8:
aload 1
athrow
L9:
aload 0
getfield org/xbill/DNS/ExtendedResolver$Resolution/sent [I
iload 3
iaload
iconst_1
if_icmpne L29
iload 3
aload 0
getfield org/xbill/DNS/ExtendedResolver$Resolution/resolvers [Lorg/xbill/DNS/Resolver;
arraylength
iconst_1
isub
if_icmpge L29
L10:
aload 2
instanceof java/io/InterruptedIOException
ifeq L15
aload 0
getfield org/xbill/DNS/ExtendedResolver$Resolution/sent [I
iload 3
iaload
aload 0
getfield org/xbill/DNS/ExtendedResolver$Resolution/retries I
if_icmpge L11
aload 0
iload 3
invokevirtual org/xbill/DNS/ExtendedResolver$Resolution/send(I)V
L11:
aload 0
getfield org/xbill/DNS/ExtendedResolver$Resolution/thrown Ljava/lang/Throwable;
ifnonnull L13
L12:
aload 0
aload 2
putfield org/xbill/DNS/ExtendedResolver$Resolution/thrown Ljava/lang/Throwable;
L13:
aload 0
getfield org/xbill/DNS/ExtendedResolver$Resolution/done Z
ifeq L30
aload 0
monitorexit
L14:
return
L15:
aload 2
instanceof java/net/SocketException
ifeq L12
aload 0
getfield org/xbill/DNS/ExtendedResolver$Resolution/thrown Ljava/lang/Throwable;
ifnull L16
aload 0
getfield org/xbill/DNS/ExtendedResolver$Resolution/thrown Ljava/lang/Throwable;
instanceof java/io/InterruptedIOException
ifeq L13
L16:
aload 0
aload 2
putfield org/xbill/DNS/ExtendedResolver$Resolution/thrown Ljava/lang/Throwable;
L17:
goto L13
L30:
iload 4
ifeq L19
L18:
aload 0
iload 3
iconst_1
iadd
invokevirtual org/xbill/DNS/ExtendedResolver$Resolution/send(I)V
L19:
aload 0
getfield org/xbill/DNS/ExtendedResolver$Resolution/done Z
ifeq L21
aload 0
monitorexit
L20:
return
L21:
aload 0
getfield org/xbill/DNS/ExtendedResolver$Resolution/outstanding I
ifne L23
aload 0
iconst_1
putfield org/xbill/DNS/ExtendedResolver$Resolution/done Z
aload 0
getfield org/xbill/DNS/ExtendedResolver$Resolution/listener Lorg/xbill/DNS/ResolverListener;
ifnonnull L23
aload 0
invokevirtual java/lang/Object/notifyAll()V
aload 0
monitorexit
L22:
return
L23:
aload 0
getfield org/xbill/DNS/ExtendedResolver$Resolution/done Z
ifne L25
aload 0
monitorexit
L24:
return
L25:
aload 0
monitorexit
L26:
aload 0
getfield org/xbill/DNS/ExtendedResolver$Resolution/thrown Ljava/lang/Throwable;
instanceof java/lang/Exception
ifne L31
aload 0
new java/lang/RuntimeException
dup
aload 0
getfield org/xbill/DNS/ExtendedResolver$Resolution/thrown Ljava/lang/Throwable;
invokevirtual java/lang/Throwable/getMessage()Ljava/lang/String;
invokespecial java/lang/RuntimeException/<init>(Ljava/lang/String;)V
putfield org/xbill/DNS/ExtendedResolver$Resolution/thrown Ljava/lang/Throwable;
L31:
aload 0
getfield org/xbill/DNS/ExtendedResolver$Resolution/listener Lorg/xbill/DNS/ResolverListener;
aload 0
aload 0
getfield org/xbill/DNS/ExtendedResolver$Resolution/thrown Ljava/lang/Throwable;
checkcast java/lang/Exception
invokeinterface org/xbill/DNS/ResolverListener/handleException(Ljava/lang/Object;Ljava/lang/Exception;)V 2
return
L29:
iconst_0
istore 4
goto L10
L28:
iconst_0
istore 3
goto L3
.limit locals 5
.limit stack 4
.end method

.method public receiveMessage(Ljava/lang/Object;Lorg/xbill/DNS/Message;)V
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch all from L5 to L6 using L2
.catch all from L7 to L8 using L2
ldc "verbose"
invokestatic org/xbill/DNS/Options/check(Ljava/lang/String;)Z
ifeq L9
getstatic java/lang/System/err Ljava/io/PrintStream;
ldc "ExtendedResolver: received message"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L9:
aload 0
monitorenter
L0:
aload 0
getfield org/xbill/DNS/ExtendedResolver$Resolution/done Z
ifeq L3
aload 0
monitorexit
L1:
return
L3:
aload 0
aload 2
putfield org/xbill/DNS/ExtendedResolver$Resolution/response Lorg/xbill/DNS/Message;
aload 0
iconst_1
putfield org/xbill/DNS/ExtendedResolver$Resolution/done Z
aload 0
getfield org/xbill/DNS/ExtendedResolver$Resolution/listener Lorg/xbill/DNS/ResolverListener;
ifnonnull L7
aload 0
invokevirtual java/lang/Object/notifyAll()V
aload 0
monitorexit
L4:
return
L2:
astore 1
L5:
aload 0
monitorexit
L6:
aload 1
athrow
L7:
aload 0
monitorexit
L8:
aload 0
getfield org/xbill/DNS/ExtendedResolver$Resolution/listener Lorg/xbill/DNS/ResolverListener;
aload 0
aload 0
getfield org/xbill/DNS/ExtendedResolver$Resolution/response Lorg/xbill/DNS/Message;
invokeinterface org/xbill/DNS/ResolverListener/receiveMessage(Ljava/lang/Object;Lorg/xbill/DNS/Message;)V 2
return
.limit locals 3
.limit stack 3
.end method

.method public send(I)V
.catch java/lang/Throwable from L0 to L1 using L2
.catch all from L3 to L4 using L5
.catch all from L6 to L7 using L5
.catch all from L8 to L9 using L5
aload 0
getfield org/xbill/DNS/ExtendedResolver$Resolution/sent [I
astore 2
aload 2
iload 1
aload 2
iload 1
iaload
iconst_1
iadd
iastore
aload 0
aload 0
getfield org/xbill/DNS/ExtendedResolver$Resolution/outstanding I
iconst_1
iadd
putfield org/xbill/DNS/ExtendedResolver$Resolution/outstanding I
L0:
aload 0
getfield org/xbill/DNS/ExtendedResolver$Resolution/inprogress [Ljava/lang/Object;
iload 1
aload 0
getfield org/xbill/DNS/ExtendedResolver$Resolution/resolvers [Lorg/xbill/DNS/Resolver;
iload 1
aaload
aload 0
getfield org/xbill/DNS/ExtendedResolver$Resolution/query Lorg/xbill/DNS/Message;
aload 0
invokeinterface org/xbill/DNS/Resolver/sendAsync(Lorg/xbill/DNS/Message;Lorg/xbill/DNS/ResolverListener;)Ljava/lang/Object; 2
aastore
L1:
return
L2:
astore 2
aload 0
monitorenter
L3:
aload 0
aload 2
putfield org/xbill/DNS/ExtendedResolver$Resolution/thrown Ljava/lang/Throwable;
aload 0
iconst_1
putfield org/xbill/DNS/ExtendedResolver$Resolution/done Z
aload 0
getfield org/xbill/DNS/ExtendedResolver$Resolution/listener Lorg/xbill/DNS/ResolverListener;
ifnonnull L8
aload 0
invokevirtual java/lang/Object/notifyAll()V
aload 0
monitorexit
L4:
return
L5:
astore 2
L6:
aload 0
monitorexit
L7:
aload 2
athrow
L8:
aload 0
monitorexit
L9:
return
.limit locals 3
.limit stack 5
.end method

.method public start()Lorg/xbill/DNS/Message;
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L2
.catch all from L5 to L6 using L7
.catch java/lang/InterruptedException from L8 to L9 using L10
.catch all from L8 to L9 using L7
.catch all from L11 to L12 using L7
.catch all from L13 to L14 using L7
L0:
aload 0
getfield org/xbill/DNS/ExtendedResolver$Resolution/sent [I
astore 2
L1:
aload 2
iconst_0
aload 2
iconst_0
iaload
iconst_1
iadd
iastore
L3:
aload 0
aload 0
getfield org/xbill/DNS/ExtendedResolver$Resolution/outstanding I
iconst_1
iadd
putfield org/xbill/DNS/ExtendedResolver$Resolution/outstanding I
aload 0
getfield org/xbill/DNS/ExtendedResolver$Resolution/inprogress [Ljava/lang/Object;
iconst_0
new java/lang/Object
dup
invokespecial java/lang/Object/<init>()V
aastore
aload 0
getfield org/xbill/DNS/ExtendedResolver$Resolution/resolvers [Lorg/xbill/DNS/Resolver;
iconst_0
aaload
aload 0
getfield org/xbill/DNS/ExtendedResolver$Resolution/query Lorg/xbill/DNS/Message;
invokeinterface org/xbill/DNS/Resolver/send(Lorg/xbill/DNS/Message;)Lorg/xbill/DNS/Message; 1
astore 2
L4:
aload 2
areturn
L2:
astore 2
aload 0
aload 0
getfield org/xbill/DNS/ExtendedResolver$Resolution/inprogress [Ljava/lang/Object;
iconst_0
aaload
aload 2
invokevirtual org/xbill/DNS/ExtendedResolver$Resolution/handleException(Ljava/lang/Object;Ljava/lang/Exception;)V
aload 0
monitorenter
L5:
aload 0
getfield org/xbill/DNS/ExtendedResolver$Resolution/done Z
istore 1
L6:
iload 1
ifne L11
L8:
aload 0
invokevirtual java/lang/Object/wait()V
L9:
goto L5
L10:
astore 2
goto L5
L11:
aload 0
monitorexit
L12:
aload 0
getfield org/xbill/DNS/ExtendedResolver$Resolution/response Lorg/xbill/DNS/Message;
ifnull L15
aload 0
getfield org/xbill/DNS/ExtendedResolver$Resolution/response Lorg/xbill/DNS/Message;
areturn
L7:
astore 2
L13:
aload 0
monitorexit
L14:
aload 2
athrow
L15:
aload 0
getfield org/xbill/DNS/ExtendedResolver$Resolution/thrown Ljava/lang/Throwable;
instanceof java/io/IOException
ifeq L16
aload 0
getfield org/xbill/DNS/ExtendedResolver$Resolution/thrown Ljava/lang/Throwable;
checkcast java/io/IOException
athrow
L16:
aload 0
getfield org/xbill/DNS/ExtendedResolver$Resolution/thrown Ljava/lang/Throwable;
instanceof java/lang/RuntimeException
ifeq L17
aload 0
getfield org/xbill/DNS/ExtendedResolver$Resolution/thrown Ljava/lang/Throwable;
checkcast java/lang/RuntimeException
athrow
L17:
aload 0
getfield org/xbill/DNS/ExtendedResolver$Resolution/thrown Ljava/lang/Throwable;
instanceof java/lang/Error
ifeq L18
aload 0
getfield org/xbill/DNS/ExtendedResolver$Resolution/thrown Ljava/lang/Throwable;
checkcast java/lang/Error
athrow
L18:
new java/lang/IllegalStateException
dup
ldc "ExtendedResolver failure"
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
.limit locals 3
.limit stack 4
.end method

.method public startAsync(Lorg/xbill/DNS/ResolverListener;)V
aload 0
aload 1
putfield org/xbill/DNS/ExtendedResolver$Resolution/listener Lorg/xbill/DNS/ResolverListener;
aload 0
iconst_0
invokevirtual org/xbill/DNS/ExtendedResolver$Resolution/send(I)V
return
.limit locals 2
.limit stack 2
.end method
