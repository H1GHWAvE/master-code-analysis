.bytecode 50.0
.class public synchronized org/xbill/DNS/RPRecord
.super org/xbill/DNS/Record

.field private static final 'serialVersionUID' J = 8124584364211337460L


.field private 'mailbox' Lorg/xbill/DNS/Name;

.field private 'textDomain' Lorg/xbill/DNS/Name;

.method <init>()V
aload 0
invokespecial org/xbill/DNS/Record/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public <init>(Lorg/xbill/DNS/Name;IJLorg/xbill/DNS/Name;Lorg/xbill/DNS/Name;)V
aload 0
aload 1
bipush 17
iload 2
lload 3
invokespecial org/xbill/DNS/Record/<init>(Lorg/xbill/DNS/Name;IIJ)V
aload 0
ldc "mailbox"
aload 5
invokestatic org/xbill/DNS/RPRecord/checkName(Ljava/lang/String;Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/Name;
putfield org/xbill/DNS/RPRecord/mailbox Lorg/xbill/DNS/Name;
aload 0
ldc "textDomain"
aload 6
invokestatic org/xbill/DNS/RPRecord/checkName(Ljava/lang/String;Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/Name;
putfield org/xbill/DNS/RPRecord/textDomain Lorg/xbill/DNS/Name;
return
.limit locals 7
.limit stack 6
.end method

.method public getMailbox()Lorg/xbill/DNS/Name;
aload 0
getfield org/xbill/DNS/RPRecord/mailbox Lorg/xbill/DNS/Name;
areturn
.limit locals 1
.limit stack 1
.end method

.method getObject()Lorg/xbill/DNS/Record;
new org/xbill/DNS/RPRecord
dup
invokespecial org/xbill/DNS/RPRecord/<init>()V
areturn
.limit locals 1
.limit stack 2
.end method

.method public getTextDomain()Lorg/xbill/DNS/Name;
aload 0
getfield org/xbill/DNS/RPRecord/textDomain Lorg/xbill/DNS/Name;
areturn
.limit locals 1
.limit stack 1
.end method

.method rdataFromString(Lorg/xbill/DNS/Tokenizer;Lorg/xbill/DNS/Name;)V
aload 0
aload 1
aload 2
invokevirtual org/xbill/DNS/Tokenizer/getName(Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/Name;
putfield org/xbill/DNS/RPRecord/mailbox Lorg/xbill/DNS/Name;
aload 0
aload 1
aload 2
invokevirtual org/xbill/DNS/Tokenizer/getName(Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/Name;
putfield org/xbill/DNS/RPRecord/textDomain Lorg/xbill/DNS/Name;
return
.limit locals 3
.limit stack 3
.end method

.method rrFromWire(Lorg/xbill/DNS/DNSInput;)V
aload 0
new org/xbill/DNS/Name
dup
aload 1
invokespecial org/xbill/DNS/Name/<init>(Lorg/xbill/DNS/DNSInput;)V
putfield org/xbill/DNS/RPRecord/mailbox Lorg/xbill/DNS/Name;
aload 0
new org/xbill/DNS/Name
dup
aload 1
invokespecial org/xbill/DNS/Name/<init>(Lorg/xbill/DNS/DNSInput;)V
putfield org/xbill/DNS/RPRecord/textDomain Lorg/xbill/DNS/Name;
return
.limit locals 2
.limit stack 4
.end method

.method rrToString()Ljava/lang/String;
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
astore 1
aload 1
aload 0
getfield org/xbill/DNS/RPRecord/mailbox Lorg/xbill/DNS/Name;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/Object;)Ljava/lang/StringBuffer;
pop
aload 1
ldc " "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
aload 0
getfield org/xbill/DNS/RPRecord/textDomain Lorg/xbill/DNS/Name;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/Object;)Ljava/lang/StringBuffer;
pop
aload 1
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
areturn
.limit locals 2
.limit stack 2
.end method

.method rrToWire(Lorg/xbill/DNS/DNSOutput;Lorg/xbill/DNS/Compression;Z)V
aload 0
getfield org/xbill/DNS/RPRecord/mailbox Lorg/xbill/DNS/Name;
aload 1
aconst_null
iload 3
invokevirtual org/xbill/DNS/Name/toWire(Lorg/xbill/DNS/DNSOutput;Lorg/xbill/DNS/Compression;Z)V
aload 0
getfield org/xbill/DNS/RPRecord/textDomain Lorg/xbill/DNS/Name;
aload 1
aconst_null
iload 3
invokevirtual org/xbill/DNS/Name/toWire(Lorg/xbill/DNS/DNSOutput;Lorg/xbill/DNS/Compression;Z)V
return
.limit locals 4
.limit stack 4
.end method
