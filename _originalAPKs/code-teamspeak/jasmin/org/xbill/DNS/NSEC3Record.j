.bytecode 50.0
.class public synchronized org/xbill/DNS/NSEC3Record
.super org/xbill/DNS/Record

.field public static final 'SHA1_DIGEST_ID' I = 1


.field private static final 'b32' Lorg/xbill/DNS/utils/base32;

.field private static final 'serialVersionUID' J = -7123504635968932855L


.field private 'flags' I

.field private 'hashAlg' I

.field private 'iterations' I

.field private 'next' [B

.field private 'salt' [B

.field private 'types' Lorg/xbill/DNS/TypeBitmap;

.method static <clinit>()V
new org/xbill/DNS/utils/base32
dup
ldc "0123456789ABCDEFGHIJKLMNOPQRSTUV="
iconst_0
iconst_0
invokespecial org/xbill/DNS/utils/base32/<init>(Ljava/lang/String;ZZ)V
putstatic org/xbill/DNS/NSEC3Record/b32 Lorg/xbill/DNS/utils/base32;
return
.limit locals 0
.limit stack 5
.end method

.method <init>()V
aload 0
invokespecial org/xbill/DNS/Record/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public <init>(Lorg/xbill/DNS/Name;IJIII[B[B[I)V
aload 0
aload 1
bipush 50
iload 2
lload 3
invokespecial org/xbill/DNS/Record/<init>(Lorg/xbill/DNS/Name;IIJ)V
aload 0
ldc "hashAlg"
iload 5
invokestatic org/xbill/DNS/NSEC3Record/checkU8(Ljava/lang/String;I)I
putfield org/xbill/DNS/NSEC3Record/hashAlg I
aload 0
ldc "flags"
iload 6
invokestatic org/xbill/DNS/NSEC3Record/checkU8(Ljava/lang/String;I)I
putfield org/xbill/DNS/NSEC3Record/flags I
aload 0
ldc "iterations"
iload 7
invokestatic org/xbill/DNS/NSEC3Record/checkU16(Ljava/lang/String;I)I
putfield org/xbill/DNS/NSEC3Record/iterations I
aload 8
ifnull L0
aload 8
arraylength
sipush 255
if_icmple L1
new java/lang/IllegalArgumentException
dup
ldc "Invalid salt"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L1:
aload 8
arraylength
ifle L0
aload 0
aload 8
arraylength
newarray byte
putfield org/xbill/DNS/NSEC3Record/salt [B
aload 8
iconst_0
aload 0
getfield org/xbill/DNS/NSEC3Record/salt [B
iconst_0
aload 8
arraylength
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
L0:
aload 9
arraylength
sipush 255
if_icmple L2
new java/lang/IllegalArgumentException
dup
ldc "Invalid next hash"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L2:
aload 0
aload 9
arraylength
newarray byte
putfield org/xbill/DNS/NSEC3Record/next [B
aload 9
iconst_0
aload 0
getfield org/xbill/DNS/NSEC3Record/next [B
iconst_0
aload 9
arraylength
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 0
new org/xbill/DNS/TypeBitmap
dup
aload 10
invokespecial org/xbill/DNS/TypeBitmap/<init>([I)V
putfield org/xbill/DNS/NSEC3Record/types Lorg/xbill/DNS/TypeBitmap;
return
.limit locals 11
.limit stack 6
.end method

.method static hashName(Lorg/xbill/DNS/Name;II[B)[B
iload 1
tableswitch 1
L0
default : L1
L1:
new java/security/NoSuchAlgorithmException
dup
new java/lang/StringBuffer
dup
ldc "Unknown NSEC3 algorithmidentifier: "
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
iload 1
invokevirtual java/lang/StringBuffer/append(I)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokespecial java/security/NoSuchAlgorithmException/<init>(Ljava/lang/String;)V
athrow
L0:
ldc "sha-1"
invokestatic java/security/MessageDigest/getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
astore 5
aconst_null
astore 4
iconst_0
istore 1
L2:
iload 1
iload 2
if_icmpgt L3
aload 5
invokevirtual java/security/MessageDigest/reset()V
iload 1
ifne L4
aload 5
aload 0
invokevirtual org/xbill/DNS/Name/toWireCanonical()[B
invokevirtual java/security/MessageDigest/update([B)V
L5:
aload 3
ifnull L6
aload 5
aload 3
invokevirtual java/security/MessageDigest/update([B)V
L6:
aload 5
invokevirtual java/security/MessageDigest/digest()[B
astore 4
iload 1
iconst_1
iadd
istore 1
goto L2
L4:
aload 5
aload 4
invokevirtual java/security/MessageDigest/update([B)V
goto L5
L3:
aload 4
areturn
.limit locals 6
.limit stack 5
.end method

.method public getFlags()I
aload 0
getfield org/xbill/DNS/NSEC3Record/flags I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getHashAlgorithm()I
aload 0
getfield org/xbill/DNS/NSEC3Record/hashAlg I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getIterations()I
aload 0
getfield org/xbill/DNS/NSEC3Record/iterations I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getNext()[B
aload 0
getfield org/xbill/DNS/NSEC3Record/next [B
areturn
.limit locals 1
.limit stack 1
.end method

.method getObject()Lorg/xbill/DNS/Record;
new org/xbill/DNS/NSEC3Record
dup
invokespecial org/xbill/DNS/NSEC3Record/<init>()V
areturn
.limit locals 1
.limit stack 2
.end method

.method public getSalt()[B
aload 0
getfield org/xbill/DNS/NSEC3Record/salt [B
areturn
.limit locals 1
.limit stack 1
.end method

.method public getTypes()[I
aload 0
getfield org/xbill/DNS/NSEC3Record/types Lorg/xbill/DNS/TypeBitmap;
invokevirtual org/xbill/DNS/TypeBitmap/toArray()[I
areturn
.limit locals 1
.limit stack 1
.end method

.method public hasType(I)Z
aload 0
getfield org/xbill/DNS/NSEC3Record/types Lorg/xbill/DNS/TypeBitmap;
iload 1
invokevirtual org/xbill/DNS/TypeBitmap/contains(I)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public hashName(Lorg/xbill/DNS/Name;)[B
aload 1
aload 0
getfield org/xbill/DNS/NSEC3Record/hashAlg I
aload 0
getfield org/xbill/DNS/NSEC3Record/iterations I
aload 0
getfield org/xbill/DNS/NSEC3Record/salt [B
invokestatic org/xbill/DNS/NSEC3Record/hashName(Lorg/xbill/DNS/Name;II[B)[B
areturn
.limit locals 2
.limit stack 4
.end method

.method rdataFromString(Lorg/xbill/DNS/Tokenizer;Lorg/xbill/DNS/Name;)V
aload 0
aload 1
invokevirtual org/xbill/DNS/Tokenizer/getUInt8()I
putfield org/xbill/DNS/NSEC3Record/hashAlg I
aload 0
aload 1
invokevirtual org/xbill/DNS/Tokenizer/getUInt8()I
putfield org/xbill/DNS/NSEC3Record/flags I
aload 0
aload 1
invokevirtual org/xbill/DNS/Tokenizer/getUInt16()I
putfield org/xbill/DNS/NSEC3Record/iterations I
aload 1
invokevirtual org/xbill/DNS/Tokenizer/getString()Ljava/lang/String;
ldc "-"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L0
aload 0
aconst_null
putfield org/xbill/DNS/NSEC3Record/salt [B
L1:
aload 0
aload 1
getstatic org/xbill/DNS/NSEC3Record/b32 Lorg/xbill/DNS/utils/base32;
invokevirtual org/xbill/DNS/Tokenizer/getBase32String(Lorg/xbill/DNS/utils/base32;)[B
putfield org/xbill/DNS/NSEC3Record/next [B
aload 0
new org/xbill/DNS/TypeBitmap
dup
aload 1
invokespecial org/xbill/DNS/TypeBitmap/<init>(Lorg/xbill/DNS/Tokenizer;)V
putfield org/xbill/DNS/NSEC3Record/types Lorg/xbill/DNS/TypeBitmap;
return
L0:
aload 1
invokevirtual org/xbill/DNS/Tokenizer/unget()V
aload 0
aload 1
invokevirtual org/xbill/DNS/Tokenizer/getHexString()[B
putfield org/xbill/DNS/NSEC3Record/salt [B
aload 0
getfield org/xbill/DNS/NSEC3Record/salt [B
arraylength
sipush 255
if_icmple L1
aload 1
ldc "salt value too long"
invokevirtual org/xbill/DNS/Tokenizer/exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;
athrow
.limit locals 3
.limit stack 4
.end method

.method rrFromWire(Lorg/xbill/DNS/DNSInput;)V
aload 0
aload 1
invokevirtual org/xbill/DNS/DNSInput/readU8()I
putfield org/xbill/DNS/NSEC3Record/hashAlg I
aload 0
aload 1
invokevirtual org/xbill/DNS/DNSInput/readU8()I
putfield org/xbill/DNS/NSEC3Record/flags I
aload 0
aload 1
invokevirtual org/xbill/DNS/DNSInput/readU16()I
putfield org/xbill/DNS/NSEC3Record/iterations I
aload 1
invokevirtual org/xbill/DNS/DNSInput/readU8()I
istore 2
iload 2
ifle L0
aload 0
aload 1
iload 2
invokevirtual org/xbill/DNS/DNSInput/readByteArray(I)[B
putfield org/xbill/DNS/NSEC3Record/salt [B
L1:
aload 0
aload 1
aload 1
invokevirtual org/xbill/DNS/DNSInput/readU8()I
invokevirtual org/xbill/DNS/DNSInput/readByteArray(I)[B
putfield org/xbill/DNS/NSEC3Record/next [B
aload 0
new org/xbill/DNS/TypeBitmap
dup
aload 1
invokespecial org/xbill/DNS/TypeBitmap/<init>(Lorg/xbill/DNS/DNSInput;)V
putfield org/xbill/DNS/NSEC3Record/types Lorg/xbill/DNS/TypeBitmap;
return
L0:
aload 0
aconst_null
putfield org/xbill/DNS/NSEC3Record/salt [B
goto L1
.limit locals 3
.limit stack 4
.end method

.method rrToString()Ljava/lang/String;
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
astore 1
aload 1
aload 0
getfield org/xbill/DNS/NSEC3Record/hashAlg I
invokevirtual java/lang/StringBuffer/append(I)Ljava/lang/StringBuffer;
pop
aload 1
bipush 32
invokevirtual java/lang/StringBuffer/append(C)Ljava/lang/StringBuffer;
pop
aload 1
aload 0
getfield org/xbill/DNS/NSEC3Record/flags I
invokevirtual java/lang/StringBuffer/append(I)Ljava/lang/StringBuffer;
pop
aload 1
bipush 32
invokevirtual java/lang/StringBuffer/append(C)Ljava/lang/StringBuffer;
pop
aload 1
aload 0
getfield org/xbill/DNS/NSEC3Record/iterations I
invokevirtual java/lang/StringBuffer/append(I)Ljava/lang/StringBuffer;
pop
aload 1
bipush 32
invokevirtual java/lang/StringBuffer/append(C)Ljava/lang/StringBuffer;
pop
aload 0
getfield org/xbill/DNS/NSEC3Record/salt [B
ifnonnull L0
aload 1
bipush 45
invokevirtual java/lang/StringBuffer/append(C)Ljava/lang/StringBuffer;
pop
L1:
aload 1
bipush 32
invokevirtual java/lang/StringBuffer/append(C)Ljava/lang/StringBuffer;
pop
aload 1
getstatic org/xbill/DNS/NSEC3Record/b32 Lorg/xbill/DNS/utils/base32;
aload 0
getfield org/xbill/DNS/NSEC3Record/next [B
invokevirtual org/xbill/DNS/utils/base32/toString([B)Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 0
getfield org/xbill/DNS/NSEC3Record/types Lorg/xbill/DNS/TypeBitmap;
invokevirtual org/xbill/DNS/TypeBitmap/empty()Z
ifne L2
aload 1
bipush 32
invokevirtual java/lang/StringBuffer/append(C)Ljava/lang/StringBuffer;
pop
aload 1
aload 0
getfield org/xbill/DNS/NSEC3Record/types Lorg/xbill/DNS/TypeBitmap;
invokevirtual org/xbill/DNS/TypeBitmap/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
L2:
aload 1
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
areturn
L0:
aload 1
aload 0
getfield org/xbill/DNS/NSEC3Record/salt [B
invokestatic org/xbill/DNS/utils/base16/toString([B)Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
goto L1
.limit locals 2
.limit stack 3
.end method

.method rrToWire(Lorg/xbill/DNS/DNSOutput;Lorg/xbill/DNS/Compression;Z)V
aload 1
aload 0
getfield org/xbill/DNS/NSEC3Record/hashAlg I
invokevirtual org/xbill/DNS/DNSOutput/writeU8(I)V
aload 1
aload 0
getfield org/xbill/DNS/NSEC3Record/flags I
invokevirtual org/xbill/DNS/DNSOutput/writeU8(I)V
aload 1
aload 0
getfield org/xbill/DNS/NSEC3Record/iterations I
invokevirtual org/xbill/DNS/DNSOutput/writeU16(I)V
aload 0
getfield org/xbill/DNS/NSEC3Record/salt [B
ifnull L0
aload 1
aload 0
getfield org/xbill/DNS/NSEC3Record/salt [B
arraylength
invokevirtual org/xbill/DNS/DNSOutput/writeU8(I)V
aload 1
aload 0
getfield org/xbill/DNS/NSEC3Record/salt [B
invokevirtual org/xbill/DNS/DNSOutput/writeByteArray([B)V
L1:
aload 1
aload 0
getfield org/xbill/DNS/NSEC3Record/next [B
arraylength
invokevirtual org/xbill/DNS/DNSOutput/writeU8(I)V
aload 1
aload 0
getfield org/xbill/DNS/NSEC3Record/next [B
invokevirtual org/xbill/DNS/DNSOutput/writeByteArray([B)V
aload 0
getfield org/xbill/DNS/NSEC3Record/types Lorg/xbill/DNS/TypeBitmap;
aload 1
invokevirtual org/xbill/DNS/TypeBitmap/toWire(Lorg/xbill/DNS/DNSOutput;)V
return
L0:
aload 1
iconst_0
invokevirtual org/xbill/DNS/DNSOutput/writeU8(I)V
goto L1
.limit locals 4
.limit stack 2
.end method
