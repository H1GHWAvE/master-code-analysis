.bytecode 50.0
.class synchronized abstract org/xbill/DNS/TXTBase
.super org/xbill/DNS/Record

.field private static final 'serialVersionUID' J = -4319510507246305931L


.field protected 'strings' Ljava/util/List;

.method protected <init>()V
aload 0
invokespecial org/xbill/DNS/Record/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method protected <init>(Lorg/xbill/DNS/Name;IIJ)V
aload 0
aload 1
iload 2
iload 3
lload 4
invokespecial org/xbill/DNS/Record/<init>(Lorg/xbill/DNS/Name;IIJ)V
return
.limit locals 6
.limit stack 6
.end method

.method protected <init>(Lorg/xbill/DNS/Name;IIJLjava/lang/String;)V
aload 0
aload 1
iload 2
iload 3
lload 4
aload 6
invokestatic java/util/Collections/singletonList(Ljava/lang/Object;)Ljava/util/List;
invokespecial org/xbill/DNS/TXTBase/<init>(Lorg/xbill/DNS/Name;IIJLjava/util/List;)V
return
.limit locals 7
.limit stack 7
.end method

.method protected <init>(Lorg/xbill/DNS/Name;IIJLjava/util/List;)V
.catch org/xbill/DNS/TextParseException from L0 to L1 using L2
aload 0
aload 1
iload 2
iload 3
lload 4
invokespecial org/xbill/DNS/Record/<init>(Lorg/xbill/DNS/Name;IIJ)V
aload 6
ifnonnull L3
new java/lang/IllegalArgumentException
dup
ldc "strings must not be null"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L3:
aload 0
new java/util/ArrayList
dup
aload 6
invokeinterface java/util/List/size()I 0
invokespecial java/util/ArrayList/<init>(I)V
putfield org/xbill/DNS/TXTBase/strings Ljava/util/List;
aload 6
invokeinterface java/util/List/iterator()Ljava/util/Iterator; 0
astore 1
L0:
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L4
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/lang/String
astore 6
aload 0
getfield org/xbill/DNS/TXTBase/strings Ljava/util/List;
aload 6
invokestatic org/xbill/DNS/TXTBase/byteArrayFromString(Ljava/lang/String;)[B
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
L1:
goto L0
L2:
astore 1
new java/lang/IllegalArgumentException
dup
aload 1
invokevirtual org/xbill/DNS/TextParseException/getMessage()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L4:
return
.limit locals 7
.limit stack 6
.end method

.method public getStrings()Ljava/util/List;
new java/util/ArrayList
dup
aload 0
getfield org/xbill/DNS/TXTBase/strings Ljava/util/List;
invokeinterface java/util/List/size()I 0
invokespecial java/util/ArrayList/<init>(I)V
astore 2
iconst_0
istore 1
L0:
iload 1
aload 0
getfield org/xbill/DNS/TXTBase/strings Ljava/util/List;
invokeinterface java/util/List/size()I 0
if_icmpge L1
aload 2
aload 0
getfield org/xbill/DNS/TXTBase/strings Ljava/util/List;
iload 1
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast [B
checkcast [B
iconst_0
invokestatic org/xbill/DNS/TXTBase/byteArrayToString([BZ)Ljava/lang/String;
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
iload 1
iconst_1
iadd
istore 1
goto L0
L1:
aload 2
areturn
.limit locals 3
.limit stack 3
.end method

.method public getStringsAsByteArrays()Ljava/util/List;
aload 0
getfield org/xbill/DNS/TXTBase/strings Ljava/util/List;
areturn
.limit locals 1
.limit stack 1
.end method

.method rdataFromString(Lorg/xbill/DNS/Tokenizer;Lorg/xbill/DNS/Name;)V
.catch org/xbill/DNS/TextParseException from L0 to L1 using L2
aload 0
new java/util/ArrayList
dup
iconst_2
invokespecial java/util/ArrayList/<init>(I)V
putfield org/xbill/DNS/TXTBase/strings Ljava/util/List;
L3:
aload 1
invokevirtual org/xbill/DNS/Tokenizer/get()Lorg/xbill/DNS/Tokenizer$Token;
astore 2
aload 2
invokevirtual org/xbill/DNS/Tokenizer$Token/isString()Z
ifeq L4
L0:
aload 0
getfield org/xbill/DNS/TXTBase/strings Ljava/util/List;
aload 2
getfield org/xbill/DNS/Tokenizer$Token/value Ljava/lang/String;
invokestatic org/xbill/DNS/TXTBase/byteArrayFromString(Ljava/lang/String;)[B
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
L1:
goto L3
L2:
astore 2
aload 1
aload 2
invokevirtual org/xbill/DNS/TextParseException/getMessage()Ljava/lang/String;
invokevirtual org/xbill/DNS/Tokenizer/exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;
athrow
L4:
aload 1
invokevirtual org/xbill/DNS/Tokenizer/unget()V
return
.limit locals 3
.limit stack 4
.end method

.method rrFromWire(Lorg/xbill/DNS/DNSInput;)V
aload 0
new java/util/ArrayList
dup
iconst_2
invokespecial java/util/ArrayList/<init>(I)V
putfield org/xbill/DNS/TXTBase/strings Ljava/util/List;
L0:
aload 1
invokevirtual org/xbill/DNS/DNSInput/remaining()I
ifle L1
aload 1
invokevirtual org/xbill/DNS/DNSInput/readCountedString()[B
astore 2
aload 0
getfield org/xbill/DNS/TXTBase/strings Ljava/util/List;
aload 2
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
goto L0
L1:
return
.limit locals 3
.limit stack 4
.end method

.method rrToString()Ljava/lang/String;
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
astore 1
aload 0
getfield org/xbill/DNS/TXTBase/strings Ljava/util/List;
invokeinterface java/util/List/iterator()Ljava/util/Iterator; 0
astore 2
L0:
aload 2
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 1
aload 2
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast [B
checkcast [B
iconst_1
invokestatic org/xbill/DNS/TXTBase/byteArrayToString([BZ)Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 2
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L0
aload 1
ldc " "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
goto L0
L1:
aload 1
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
areturn
.limit locals 3
.limit stack 3
.end method

.method rrToWire(Lorg/xbill/DNS/DNSOutput;Lorg/xbill/DNS/Compression;Z)V
aload 0
getfield org/xbill/DNS/TXTBase/strings Ljava/util/List;
invokeinterface java/util/List/iterator()Ljava/util/Iterator; 0
astore 2
L0:
aload 2
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 1
aload 2
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast [B
checkcast [B
invokevirtual org/xbill/DNS/DNSOutput/writeCountedString([B)V
goto L0
L1:
return
.limit locals 4
.limit stack 2
.end method
