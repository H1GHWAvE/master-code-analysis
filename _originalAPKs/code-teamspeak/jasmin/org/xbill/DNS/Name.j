.bytecode 50.0
.class public synchronized org/xbill/DNS/Name
.super java/lang/Object
.implements java/io/Serializable
.implements java/lang/Comparable

.field private static final 'LABEL_COMPRESSION' I = 192


.field private static final 'LABEL_MASK' I = 192


.field private static final 'LABEL_NORMAL' I = 0


.field private static final 'MAXLABEL' I = 63


.field private static final 'MAXLABELS' I = 128


.field private static final 'MAXNAME' I = 255


.field private static final 'MAXOFFSETS' I = 7


.field private static final 'byteFormat' Ljava/text/DecimalFormat;

.field public static final 'empty' Lorg/xbill/DNS/Name;

.field private static final 'emptyLabel' [B

.field private static final 'lowercase' [B

.field public static final 'root' Lorg/xbill/DNS/Name;

.field private static final 'serialVersionUID' J = -7257019940971525644L


.field private static final 'wild' Lorg/xbill/DNS/Name;

.field private static final 'wildLabel' [B

.field private 'hashcode' I

.field private 'name' [B

.field private 'offsets' J

.method static <clinit>()V
iconst_1
newarray byte
dup
iconst_0
iconst_0
bastore
putstatic org/xbill/DNS/Name/emptyLabel [B
iconst_2
newarray byte
dup
iconst_0
ldc_w 1
bastore
dup
iconst_1
ldc_w 42
bastore
putstatic org/xbill/DNS/Name/wildLabel [B
new java/text/DecimalFormat
dup
invokespecial java/text/DecimalFormat/<init>()V
putstatic org/xbill/DNS/Name/byteFormat Ljava/text/DecimalFormat;
sipush 256
newarray byte
putstatic org/xbill/DNS/Name/lowercase [B
getstatic org/xbill/DNS/Name/byteFormat Ljava/text/DecimalFormat;
iconst_3
invokevirtual java/text/DecimalFormat/setMinimumIntegerDigits(I)V
iconst_0
istore 0
L0:
iload 0
getstatic org/xbill/DNS/Name/lowercase [B
arraylength
if_icmpge L1
iload 0
bipush 65
if_icmplt L2
iload 0
bipush 90
if_icmple L3
L2:
getstatic org/xbill/DNS/Name/lowercase [B
iload 0
iload 0
i2b
bastore
L4:
iload 0
iconst_1
iadd
istore 0
goto L0
L3:
getstatic org/xbill/DNS/Name/lowercase [B
iload 0
iload 0
bipush 65
isub
bipush 97
iadd
i2b
bastore
goto L4
L1:
new org/xbill/DNS/Name
dup
invokespecial org/xbill/DNS/Name/<init>()V
astore 1
aload 1
putstatic org/xbill/DNS/Name/root Lorg/xbill/DNS/Name;
aload 1
getstatic org/xbill/DNS/Name/emptyLabel [B
iconst_0
iconst_1
invokespecial org/xbill/DNS/Name/appendSafe([BII)V
new org/xbill/DNS/Name
dup
invokespecial org/xbill/DNS/Name/<init>()V
astore 1
aload 1
putstatic org/xbill/DNS/Name/empty Lorg/xbill/DNS/Name;
aload 1
iconst_0
newarray byte
putfield org/xbill/DNS/Name/name [B
new org/xbill/DNS/Name
dup
invokespecial org/xbill/DNS/Name/<init>()V
astore 1
aload 1
putstatic org/xbill/DNS/Name/wild Lorg/xbill/DNS/Name;
aload 1
getstatic org/xbill/DNS/Name/wildLabel [B
iconst_0
iconst_1
invokespecial org/xbill/DNS/Name/appendSafe([BII)V
return
.limit locals 2
.limit stack 4
.end method

.method private <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public <init>(Ljava/lang/String;)V
aload 0
aload 1
aconst_null
invokespecial org/xbill/DNS/Name/<init>(Ljava/lang/String;Lorg/xbill/DNS/Name;)V
return
.limit locals 2
.limit stack 3
.end method

.method public <init>(Ljava/lang/String;Lorg/xbill/DNS/Name;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 1
ldc ""
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L0
aload 1
ldc "empty name"
invokestatic org/xbill/DNS/Name/parseException(Ljava/lang/String;Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;
athrow
L0:
aload 1
ldc "@"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L1
aload 2
ifnonnull L2
getstatic org/xbill/DNS/Name/empty Lorg/xbill/DNS/Name;
aload 0
invokestatic org/xbill/DNS/Name/copy(Lorg/xbill/DNS/Name;Lorg/xbill/DNS/Name;)V
L3:
return
L2:
aload 2
aload 0
invokestatic org/xbill/DNS/Name/copy(Lorg/xbill/DNS/Name;Lorg/xbill/DNS/Name;)V
return
L1:
aload 1
ldc "."
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L4
getstatic org/xbill/DNS/Name/root Lorg/xbill/DNS/Name;
aload 0
invokestatic org/xbill/DNS/Name/copy(Lorg/xbill/DNS/Name;Lorg/xbill/DNS/Name;)V
return
L4:
iconst_m1
istore 7
iconst_1
istore 9
bipush 64
newarray byte
astore 13
iconst_0
istore 10
iconst_0
istore 5
iconst_0
istore 4
iconst_0
istore 8
L5:
iload 8
aload 1
invokevirtual java/lang/String/length()I
if_icmpge L6
aload 1
iload 8
invokevirtual java/lang/String/charAt(I)C
i2b
istore 3
iload 10
ifeq L7
iload 3
bipush 48
if_icmplt L8
iload 3
bipush 57
if_icmpgt L8
iload 5
iconst_3
if_icmpge L8
iload 5
iconst_1
iadd
istore 11
iload 4
bipush 10
imul
iload 3
bipush 48
isub
iadd
istore 12
iload 12
sipush 255
if_icmple L9
aload 1
ldc "bad escape"
invokestatic org/xbill/DNS/Name/parseException(Ljava/lang/String;Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;
athrow
L9:
iload 12
istore 4
iload 11
istore 5
iload 9
istore 6
iload 11
iconst_3
if_icmplt L10
iload 12
i2b
istore 3
iload 11
istore 5
iload 12
istore 4
L11:
iload 9
bipush 63
if_icmple L12
aload 1
ldc "label too long"
invokestatic org/xbill/DNS/Name/parseException(Ljava/lang/String;Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;
athrow
L8:
iload 5
ifle L13
iload 5
iconst_3
if_icmpge L13
aload 1
ldc "bad escape"
invokestatic org/xbill/DNS/Name/parseException(Ljava/lang/String;Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;
athrow
L12:
aload 13
iload 9
iload 3
bastore
iconst_0
istore 10
iload 9
istore 7
iload 9
iconst_1
iadd
istore 6
L10:
iload 8
iconst_1
iadd
istore 8
iload 6
istore 9
goto L5
L7:
iload 3
bipush 92
if_icmpne L14
iconst_1
istore 10
iconst_0
istore 5
iconst_0
istore 4
iload 9
istore 6
goto L10
L14:
iload 3
bipush 46
if_icmpne L15
iload 7
iconst_m1
if_icmpne L16
aload 1
ldc "invalid empty label"
invokestatic org/xbill/DNS/Name/parseException(Ljava/lang/String;Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;
athrow
L16:
aload 13
iconst_0
iload 9
iconst_1
isub
i2b
bastore
aload 0
aload 1
aload 13
iconst_0
iconst_1
invokespecial org/xbill/DNS/Name/appendFromString(Ljava/lang/String;[BII)V
iconst_m1
istore 7
iconst_1
istore 6
goto L10
L15:
iload 7
iconst_m1
if_icmpne L17
iload 8
istore 6
L18:
iload 9
bipush 63
if_icmple L19
aload 1
ldc "label too long"
invokestatic org/xbill/DNS/Name/parseException(Ljava/lang/String;Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;
athrow
L19:
aload 13
iload 9
iload 3
bastore
iload 9
iconst_1
iadd
istore 9
iload 6
istore 7
iload 9
istore 6
goto L10
L6:
iload 5
ifle L20
iload 5
iconst_3
if_icmpge L20
aload 1
ldc "bad escape"
invokestatic org/xbill/DNS/Name/parseException(Ljava/lang/String;Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;
athrow
L20:
iload 10
ifeq L21
aload 1
ldc "bad escape"
invokestatic org/xbill/DNS/Name/parseException(Ljava/lang/String;Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;
athrow
L21:
iload 7
iconst_m1
if_icmpne L22
aload 0
aload 1
getstatic org/xbill/DNS/Name/emptyLabel [B
iconst_0
iconst_1
invokespecial org/xbill/DNS/Name/appendFromString(Ljava/lang/String;[BII)V
iconst_1
istore 4
L23:
aload 2
ifnull L3
iload 4
ifne L3
aload 0
aload 1
aload 2
getfield org/xbill/DNS/Name/name [B
aload 2
iconst_0
invokespecial org/xbill/DNS/Name/offset(I)I
aload 2
invokespecial org/xbill/DNS/Name/getlabels()I
invokespecial org/xbill/DNS/Name/appendFromString(Ljava/lang/String;[BII)V
return
L22:
aload 13
iconst_0
iload 9
iconst_1
isub
i2b
bastore
aload 0
aload 1
aload 13
iconst_0
iconst_1
invokespecial org/xbill/DNS/Name/appendFromString(Ljava/lang/String;[BII)V
iconst_0
istore 4
goto L23
L17:
iload 7
istore 6
goto L18
L13:
goto L11
.limit locals 14
.limit stack 5
.end method

.method public <init>(Lorg/xbill/DNS/DNSInput;)V
aload 0
invokespecial java/lang/Object/<init>()V
bipush 64
newarray byte
astore 6
iconst_0
istore 3
iconst_0
istore 2
L0:
iload 2
ifne L1
aload 1
invokevirtual org/xbill/DNS/DNSInput/readU8()I
istore 4
iload 4
sipush 192
iand
lookupswitch
0 : L2
192 : L3
default : L4
L4:
new org/xbill/DNS/WireParseException
dup
ldc "bad label type"
invokespecial org/xbill/DNS/WireParseException/<init>(Ljava/lang/String;)V
athrow
L2:
aload 0
invokespecial org/xbill/DNS/Name/getlabels()I
sipush 128
if_icmplt L5
new org/xbill/DNS/WireParseException
dup
ldc "too many labels"
invokespecial org/xbill/DNS/WireParseException/<init>(Ljava/lang/String;)V
athrow
L5:
iload 4
ifne L6
aload 0
getstatic org/xbill/DNS/Name/emptyLabel [B
iconst_0
iconst_1
invokespecial org/xbill/DNS/Name/append([BII)V
iconst_1
istore 2
goto L0
L6:
aload 6
iconst_0
iload 4
i2b
bastore
aload 1
aload 6
iconst_1
iload 4
invokevirtual org/xbill/DNS/DNSInput/readByteArray([BII)V
aload 0
aload 6
iconst_0
iconst_1
invokespecial org/xbill/DNS/Name/append([BII)V
goto L0
L3:
iload 4
sipush -193
iand
bipush 8
ishl
aload 1
invokevirtual org/xbill/DNS/DNSInput/readU8()I
iadd
istore 5
ldc "verbosecompression"
invokestatic org/xbill/DNS/Options/check(Ljava/lang/String;)Z
ifeq L7
getstatic java/lang/System/err Ljava/io/PrintStream;
new java/lang/StringBuffer
dup
ldc "currently "
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
aload 1
invokevirtual org/xbill/DNS/DNSInput/current()I
invokevirtual java/lang/StringBuffer/append(I)Ljava/lang/StringBuffer;
ldc ", pointer to "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
iload 5
invokevirtual java/lang/StringBuffer/append(I)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L7:
iload 5
aload 1
invokevirtual org/xbill/DNS/DNSInput/current()I
iconst_2
isub
if_icmplt L8
new org/xbill/DNS/WireParseException
dup
ldc "bad compression"
invokespecial org/xbill/DNS/WireParseException/<init>(Ljava/lang/String;)V
athrow
L8:
iload 3
istore 4
iload 3
ifne L9
aload 1
invokevirtual org/xbill/DNS/DNSInput/save()V
iconst_1
istore 4
L9:
aload 1
iload 5
invokevirtual org/xbill/DNS/DNSInput/jump(I)V
iload 4
istore 3
ldc "verbosecompression"
invokestatic org/xbill/DNS/Options/check(Ljava/lang/String;)Z
ifeq L0
getstatic java/lang/System/err Ljava/io/PrintStream;
new java/lang/StringBuffer
dup
ldc "current name '"
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
aload 0
invokevirtual java/lang/StringBuffer/append(Ljava/lang/Object;)Ljava/lang/StringBuffer;
ldc "', seeking to "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
iload 5
invokevirtual java/lang/StringBuffer/append(I)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
iload 4
istore 3
goto L0
L1:
iload 3
ifeq L10
aload 1
invokevirtual org/xbill/DNS/DNSInput/restore()V
L10:
return
.limit locals 7
.limit stack 4
.end method

.method public <init>(Lorg/xbill/DNS/Name;I)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 1
invokevirtual org/xbill/DNS/Name/labels()I
istore 4
iload 2
iload 4
if_icmple L0
new java/lang/IllegalArgumentException
dup
ldc "attempted to remove too many labels"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
aload 1
getfield org/xbill/DNS/Name/name [B
putfield org/xbill/DNS/Name/name [B
aload 0
iload 4
iload 2
isub
invokespecial org/xbill/DNS/Name/setlabels(I)V
iconst_0
istore 3
L1:
iload 3
bipush 7
if_icmpge L2
iload 3
iload 4
iload 2
isub
if_icmpge L2
aload 0
iload 3
aload 1
iload 3
iload 2
iadd
invokespecial org/xbill/DNS/Name/offset(I)I
invokespecial org/xbill/DNS/Name/setoffset(II)V
iload 3
iconst_1
iadd
istore 3
goto L1
L2:
return
.limit locals 5
.limit stack 5
.end method

.method public <init>([B)V
aload 0
new org/xbill/DNS/DNSInput
dup
aload 1
invokespecial org/xbill/DNS/DNSInput/<init>([B)V
invokespecial org/xbill/DNS/Name/<init>(Lorg/xbill/DNS/DNSInput;)V
return
.limit locals 2
.limit stack 4
.end method

.method private final append([BII)V
iconst_0
istore 8
aload 0
getfield org/xbill/DNS/Name/name [B
ifnonnull L0
iconst_0
istore 4
L1:
iload 2
istore 7
iconst_0
istore 6
iconst_0
istore 5
L2:
iload 6
iload 3
if_icmpge L3
aload 1
iload 7
baload
istore 9
iload 9
bipush 63
if_icmple L4
new java/lang/IllegalStateException
dup
ldc "invalid label"
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
getfield org/xbill/DNS/Name/name [B
arraylength
aload 0
iconst_0
invokespecial org/xbill/DNS/Name/offset(I)I
isub
istore 4
goto L1
L4:
iload 9
iconst_1
iadd
istore 9
iload 7
iload 9
iadd
istore 7
iload 5
iload 9
iadd
istore 5
iload 6
iconst_1
iadd
istore 6
goto L2
L3:
iload 4
iload 5
iadd
istore 9
iload 9
sipush 255
if_icmple L5
new org/xbill/DNS/NameTooLongException
dup
invokespecial org/xbill/DNS/NameTooLongException/<init>()V
athrow
L5:
aload 0
invokespecial org/xbill/DNS/Name/getlabels()I
istore 6
iload 6
iload 3
iadd
istore 7
iload 7
sipush 128
if_icmple L6
new java/lang/IllegalStateException
dup
ldc "too many labels"
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L6:
iload 9
newarray byte
astore 10
iload 4
ifeq L7
aload 0
getfield org/xbill/DNS/Name/name [B
aload 0
iconst_0
invokespecial org/xbill/DNS/Name/offset(I)I
aload 10
iconst_0
iload 4
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
L7:
aload 1
iload 2
aload 10
iload 4
iload 5
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 0
aload 10
putfield org/xbill/DNS/Name/name [B
iload 8
istore 2
L8:
iload 2
iload 3
if_icmpge L9
aload 0
iload 6
iload 2
iadd
iload 4
invokespecial org/xbill/DNS/Name/setoffset(II)V
iload 4
aload 10
iload 4
baload
iconst_1
iadd
iadd
istore 4
iload 2
iconst_1
iadd
istore 2
goto L8
L9:
aload 0
iload 7
invokespecial org/xbill/DNS/Name/setlabels(I)V
return
.limit locals 11
.limit stack 5
.end method

.method private final appendFromString(Ljava/lang/String;[BII)V
.catch org/xbill/DNS/NameTooLongException from L0 to L1 using L2
L0:
aload 0
aload 2
iload 3
iload 4
invokespecial org/xbill/DNS/Name/append([BII)V
L1:
return
L2:
astore 2
aload 1
ldc "Name too long"
invokestatic org/xbill/DNS/Name/parseException(Ljava/lang/String;Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;
athrow
.limit locals 5
.limit stack 4
.end method

.method private final appendSafe([BII)V
.catch org/xbill/DNS/NameTooLongException from L0 to L1 using L2
L0:
aload 0
aload 1
iload 2
iload 3
invokespecial org/xbill/DNS/Name/append([BII)V
L1:
return
L2:
astore 1
return
.limit locals 4
.limit stack 4
.end method

.method private byteString([BI)Ljava/lang/String;
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
astore 6
iload 2
iconst_1
iadd
istore 3
aload 1
iload 2
baload
istore 4
iload 3
istore 2
L0:
iload 2
iload 3
iload 4
iadd
if_icmpge L1
aload 1
iload 2
baload
sipush 255
iand
istore 5
iload 5
bipush 32
if_icmple L2
iload 5
bipush 127
if_icmplt L3
L2:
aload 6
bipush 92
invokevirtual java/lang/StringBuffer/append(C)Ljava/lang/StringBuffer;
pop
aload 6
getstatic org/xbill/DNS/Name/byteFormat Ljava/text/DecimalFormat;
iload 5
i2l
invokevirtual java/text/DecimalFormat/format(J)Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
L4:
iload 2
iconst_1
iadd
istore 2
goto L0
L3:
iload 5
bipush 34
if_icmpeq L5
iload 5
bipush 40
if_icmpeq L5
iload 5
bipush 41
if_icmpeq L5
iload 5
bipush 46
if_icmpeq L5
iload 5
bipush 59
if_icmpeq L5
iload 5
bipush 92
if_icmpeq L5
iload 5
bipush 64
if_icmpeq L5
iload 5
bipush 36
if_icmpne L6
L5:
aload 6
bipush 92
invokevirtual java/lang/StringBuffer/append(C)Ljava/lang/StringBuffer;
pop
aload 6
iload 5
i2c
invokevirtual java/lang/StringBuffer/append(C)Ljava/lang/StringBuffer;
pop
goto L4
L6:
aload 6
iload 5
i2c
invokevirtual java/lang/StringBuffer/append(C)Ljava/lang/StringBuffer;
pop
goto L4
L1:
aload 6
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
areturn
.limit locals 7
.limit stack 4
.end method

.method public static concatenate(Lorg/xbill/DNS/Name;Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/Name;
aload 0
invokevirtual org/xbill/DNS/Name/isAbsolute()Z
ifeq L0
aload 0
areturn
L0:
new org/xbill/DNS/Name
dup
invokespecial org/xbill/DNS/Name/<init>()V
astore 2
aload 0
aload 2
invokestatic org/xbill/DNS/Name/copy(Lorg/xbill/DNS/Name;Lorg/xbill/DNS/Name;)V
aload 2
aload 1
getfield org/xbill/DNS/Name/name [B
aload 1
iconst_0
invokespecial org/xbill/DNS/Name/offset(I)I
aload 1
invokespecial org/xbill/DNS/Name/getlabels()I
invokespecial org/xbill/DNS/Name/append([BII)V
aload 2
areturn
.limit locals 3
.limit stack 4
.end method

.method private static final copy(Lorg/xbill/DNS/Name;Lorg/xbill/DNS/Name;)V
iconst_0
istore 2
aload 0
iconst_0
invokespecial org/xbill/DNS/Name/offset(I)I
ifne L0
aload 1
aload 0
getfield org/xbill/DNS/Name/name [B
putfield org/xbill/DNS/Name/name [B
aload 1
aload 0
getfield org/xbill/DNS/Name/offsets J
putfield org/xbill/DNS/Name/offsets J
return
L0:
aload 0
iconst_0
invokespecial org/xbill/DNS/Name/offset(I)I
istore 3
aload 0
getfield org/xbill/DNS/Name/name [B
arraylength
iload 3
isub
istore 5
aload 0
invokevirtual org/xbill/DNS/Name/labels()I
istore 4
aload 1
iload 5
newarray byte
putfield org/xbill/DNS/Name/name [B
aload 0
getfield org/xbill/DNS/Name/name [B
iload 3
aload 1
getfield org/xbill/DNS/Name/name [B
iconst_0
iload 5
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
L1:
iload 2
iload 4
if_icmpge L2
iload 2
bipush 7
if_icmpge L2
aload 1
iload 2
aload 0
iload 2
invokespecial org/xbill/DNS/Name/offset(I)I
iload 3
isub
invokespecial org/xbill/DNS/Name/setoffset(II)V
iload 2
iconst_1
iadd
istore 2
goto L1
L2:
aload 1
iload 4
invokespecial org/xbill/DNS/Name/setlabels(I)V
return
.limit locals 6
.limit stack 5
.end method

.method private final equals([BI)Z
aload 0
invokevirtual org/xbill/DNS/Name/labels()I
istore 6
aload 0
iconst_0
invokespecial org/xbill/DNS/Name/offset(I)I
istore 5
iconst_0
istore 4
iload 2
istore 3
iload 5
istore 2
L0:
iload 4
iload 6
if_icmpge L1
aload 0
getfield org/xbill/DNS/Name/name [B
iload 2
baload
aload 1
iload 3
baload
if_icmpeq L2
L3:
iconst_0
ireturn
L2:
aload 0
getfield org/xbill/DNS/Name/name [B
iload 2
baload
istore 7
iload 7
bipush 63
if_icmple L4
new java/lang/IllegalStateException
dup
ldc "invalid label"
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L4:
iload 3
iconst_1
iadd
istore 3
iload 2
iconst_1
iadd
istore 2
iconst_0
istore 5
L5:
iload 5
iload 7
if_icmpge L6
getstatic org/xbill/DNS/Name/lowercase [B
aload 0
getfield org/xbill/DNS/Name/name [B
iload 2
baload
sipush 255
iand
baload
getstatic org/xbill/DNS/Name/lowercase [B
aload 1
iload 3
baload
sipush 255
iand
baload
if_icmpne L3
iload 5
iconst_1
iadd
istore 5
iload 3
iconst_1
iadd
istore 3
iload 2
iconst_1
iadd
istore 2
goto L5
L6:
iload 4
iconst_1
iadd
istore 4
goto L0
L1:
iconst_1
ireturn
.limit locals 8
.limit stack 4
.end method

.method public static fromConstantString(Ljava/lang/String;)Lorg/xbill/DNS/Name;
.catch org/xbill/DNS/TextParseException from L0 to L1 using L2
L0:
aload 0
aconst_null
invokestatic org/xbill/DNS/Name/fromString(Ljava/lang/String;Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/Name;
astore 1
L1:
aload 1
areturn
L2:
astore 1
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuffer
dup
ldc "Invalid name '"
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
aload 0
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
ldc "'"
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
.limit locals 2
.limit stack 5
.end method

.method public static fromString(Ljava/lang/String;)Lorg/xbill/DNS/Name;
aload 0
aconst_null
invokestatic org/xbill/DNS/Name/fromString(Ljava/lang/String;Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/Name;
areturn
.limit locals 1
.limit stack 2
.end method

.method public static fromString(Ljava/lang/String;Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/Name;
aload 0
ldc "@"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L0
aload 1
ifnull L0
aload 1
areturn
L0:
aload 0
ldc "."
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L1
getstatic org/xbill/DNS/Name/root Lorg/xbill/DNS/Name;
areturn
L1:
new org/xbill/DNS/Name
dup
aload 0
aload 1
invokespecial org/xbill/DNS/Name/<init>(Ljava/lang/String;Lorg/xbill/DNS/Name;)V
areturn
.limit locals 2
.limit stack 4
.end method

.method private final getlabels()I
aload 0
getfield org/xbill/DNS/Name/offsets J
ldc2_w 255L
land
l2i
ireturn
.limit locals 1
.limit stack 4
.end method

.method private final offset(I)I
iload 1
ifne L0
aload 0
invokespecial org/xbill/DNS/Name/getlabels()I
ifne L0
iconst_0
istore 4
L1:
iload 4
ireturn
L0:
iload 1
iflt L2
iload 1
aload 0
invokespecial org/xbill/DNS/Name/getlabels()I
if_icmplt L3
L2:
new java/lang/IllegalArgumentException
dup
ldc "label out of range"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L3:
iload 1
bipush 7
if_icmpge L4
aload 0
getfield org/xbill/DNS/Name/offsets J
bipush 7
iload 1
isub
bipush 8
imul
lushr
l2i
sipush 255
iand
ireturn
L4:
aload 0
bipush 6
invokespecial org/xbill/DNS/Name/offset(I)I
istore 2
bipush 6
istore 3
L5:
iload 2
istore 4
iload 3
iload 1
if_icmpge L1
aload 0
getfield org/xbill/DNS/Name/name [B
iload 2
baload
istore 4
iload 3
iconst_1
iadd
istore 3
iload 4
iconst_1
iadd
iload 2
iadd
istore 2
goto L5
.limit locals 5
.limit stack 4
.end method

.method private static parseException(Ljava/lang/String;Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;
new org/xbill/DNS/TextParseException
dup
new java/lang/StringBuffer
dup
ldc "'"
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
aload 0
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
ldc "': "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
aload 1
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokespecial org/xbill/DNS/TextParseException/<init>(Ljava/lang/String;)V
areturn
.limit locals 2
.limit stack 5
.end method

.method private final setlabels(I)V
aload 0
aload 0
getfield org/xbill/DNS/Name/offsets J
ldc2_w -256L
land
putfield org/xbill/DNS/Name/offsets J
aload 0
aload 0
getfield org/xbill/DNS/Name/offsets J
iload 1
i2l
lor
putfield org/xbill/DNS/Name/offsets J
return
.limit locals 2
.limit stack 5
.end method

.method private final setoffset(II)V
iload 1
bipush 7
if_icmplt L0
return
L0:
bipush 7
iload 1
isub
bipush 8
imul
istore 1
aload 0
aload 0
getfield org/xbill/DNS/Name/offsets J
ldc2_w 255L
iload 1
lshl
ldc2_w -1L
lxor
land
putfield org/xbill/DNS/Name/offsets J
aload 0
getfield org/xbill/DNS/Name/offsets J
lstore 3
aload 0
iload 2
i2l
iload 1
lshl
lload 3
lor
putfield org/xbill/DNS/Name/offsets J
return
.limit locals 5
.limit stack 7
.end method

.method public canonicalize()Lorg/xbill/DNS/Name;
iconst_0
istore 2
iconst_0
istore 1
L0:
iload 1
aload 0
getfield org/xbill/DNS/Name/name [B
arraylength
if_icmpge L1
getstatic org/xbill/DNS/Name/lowercase [B
aload 0
getfield org/xbill/DNS/Name/name [B
iload 1
baload
sipush 255
iand
baload
aload 0
getfield org/xbill/DNS/Name/name [B
iload 1
baload
if_icmpeq L2
iconst_0
istore 1
L3:
iload 1
ifeq L4
aload 0
areturn
L2:
iload 1
iconst_1
iadd
istore 1
goto L0
L4:
new org/xbill/DNS/Name
dup
invokespecial org/xbill/DNS/Name/<init>()V
astore 3
aload 3
aload 0
getfield org/xbill/DNS/Name/name [B
aload 0
iconst_0
invokespecial org/xbill/DNS/Name/offset(I)I
aload 0
invokespecial org/xbill/DNS/Name/getlabels()I
invokespecial org/xbill/DNS/Name/appendSafe([BII)V
iload 2
istore 1
L5:
iload 1
aload 3
getfield org/xbill/DNS/Name/name [B
arraylength
if_icmpge L6
aload 3
getfield org/xbill/DNS/Name/name [B
iload 1
getstatic org/xbill/DNS/Name/lowercase [B
aload 3
getfield org/xbill/DNS/Name/name [B
iload 1
baload
sipush 255
iand
baload
bastore
iload 1
iconst_1
iadd
istore 1
goto L5
L6:
aload 3
areturn
L1:
iconst_1
istore 1
goto L3
.limit locals 4
.limit stack 5
.end method

.method public compareTo(Ljava/lang/Object;)I
aload 1
checkcast org/xbill/DNS/Name
astore 1
aload 0
aload 1
if_acmpne L0
iconst_0
ireturn
L0:
aload 0
invokevirtual org/xbill/DNS/Name/labels()I
istore 5
aload 1
invokevirtual org/xbill/DNS/Name/labels()I
istore 6
iload 5
iload 6
if_icmple L1
iload 6
istore 2
L2:
iconst_1
istore 3
L3:
iload 3
iload 2
if_icmpgt L4
aload 0
iload 5
iload 3
isub
invokespecial org/xbill/DNS/Name/offset(I)I
istore 7
aload 1
iload 6
iload 3
isub
invokespecial org/xbill/DNS/Name/offset(I)I
istore 8
aload 0
getfield org/xbill/DNS/Name/name [B
iload 7
baload
istore 9
aload 1
getfield org/xbill/DNS/Name/name [B
iload 8
baload
istore 10
iconst_0
istore 4
L5:
iload 4
iload 9
if_icmpge L6
iload 4
iload 10
if_icmpge L6
getstatic org/xbill/DNS/Name/lowercase [B
aload 0
getfield org/xbill/DNS/Name/name [B
iload 4
iload 7
iadd
iconst_1
iadd
baload
sipush 255
iand
baload
getstatic org/xbill/DNS/Name/lowercase [B
aload 1
getfield org/xbill/DNS/Name/name [B
iload 4
iload 8
iadd
iconst_1
iadd
baload
sipush 255
iand
baload
isub
istore 11
iload 11
ifeq L7
iload 11
ireturn
L1:
iload 5
istore 2
goto L2
L7:
iload 4
iconst_1
iadd
istore 4
goto L5
L6:
iload 9
iload 10
if_icmpeq L8
iload 9
iload 10
isub
ireturn
L8:
iload 3
iconst_1
iadd
istore 3
goto L3
L4:
iload 5
iload 6
isub
ireturn
.limit locals 12
.limit stack 5
.end method

.method public equals(Ljava/lang/Object;)Z
iconst_0
istore 3
aload 1
aload 0
if_acmpne L0
iconst_1
istore 2
L1:
iload 2
ireturn
L0:
iload 3
istore 2
aload 1
ifnull L1
iload 3
istore 2
aload 1
instanceof org/xbill/DNS/Name
ifeq L1
aload 1
checkcast org/xbill/DNS/Name
astore 1
aload 1
getfield org/xbill/DNS/Name/hashcode I
ifne L2
aload 1
invokevirtual org/xbill/DNS/Name/hashCode()I
pop
L2:
aload 0
getfield org/xbill/DNS/Name/hashcode I
ifne L3
aload 0
invokevirtual org/xbill/DNS/Name/hashCode()I
pop
L3:
iload 3
istore 2
aload 1
getfield org/xbill/DNS/Name/hashcode I
aload 0
getfield org/xbill/DNS/Name/hashcode I
if_icmpne L1
iload 3
istore 2
aload 1
invokevirtual org/xbill/DNS/Name/labels()I
aload 0
invokevirtual org/xbill/DNS/Name/labels()I
if_icmpne L1
aload 0
aload 1
getfield org/xbill/DNS/Name/name [B
aload 1
iconst_0
invokespecial org/xbill/DNS/Name/offset(I)I
invokespecial org/xbill/DNS/Name/equals([BI)Z
ireturn
.limit locals 4
.limit stack 4
.end method

.method public fromDNAME(Lorg/xbill/DNS/DNAMERecord;)Lorg/xbill/DNS/Name;
iconst_0
istore 3
aload 1
invokevirtual org/xbill/DNS/DNAMERecord/getName()Lorg/xbill/DNS/Name;
astore 8
aload 1
invokevirtual org/xbill/DNS/DNAMERecord/getTarget()Lorg/xbill/DNS/Name;
astore 1
aload 0
aload 8
invokevirtual org/xbill/DNS/Name/subdomain(Lorg/xbill/DNS/Name;)Z
ifne L0
aconst_null
astore 1
L1:
aload 1
areturn
L0:
aload 0
invokevirtual org/xbill/DNS/Name/labels()I
aload 8
invokevirtual org/xbill/DNS/Name/labels()I
isub
istore 4
aload 0
invokevirtual org/xbill/DNS/Name/length()S
aload 8
invokevirtual org/xbill/DNS/Name/length()S
isub
istore 2
aload 0
iconst_0
invokespecial org/xbill/DNS/Name/offset(I)I
istore 6
aload 1
invokevirtual org/xbill/DNS/Name/labels()I
istore 5
aload 1
invokevirtual org/xbill/DNS/Name/length()S
istore 7
iload 2
iload 7
iadd
sipush 255
if_icmple L2
new org/xbill/DNS/NameTooLongException
dup
invokespecial org/xbill/DNS/NameTooLongException/<init>()V
athrow
L2:
new org/xbill/DNS/Name
dup
invokespecial org/xbill/DNS/Name/<init>()V
astore 8
aload 8
iload 4
iload 5
iadd
invokespecial org/xbill/DNS/Name/setlabels(I)V
aload 8
iload 2
iload 7
iadd
newarray byte
putfield org/xbill/DNS/Name/name [B
aload 0
getfield org/xbill/DNS/Name/name [B
iload 6
aload 8
getfield org/xbill/DNS/Name/name [B
iconst_0
iload 2
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 1
getfield org/xbill/DNS/Name/name [B
iconst_0
aload 8
getfield org/xbill/DNS/Name/name [B
iload 2
iload 7
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
iconst_0
istore 2
L3:
aload 8
astore 1
iload 2
bipush 7
if_icmpge L1
aload 8
astore 1
iload 2
iload 4
iload 5
iadd
if_icmpge L1
aload 8
iload 2
iload 3
invokespecial org/xbill/DNS/Name/setoffset(II)V
iload 3
aload 8
getfield org/xbill/DNS/Name/name [B
iload 3
baload
iconst_1
iadd
iadd
istore 3
iload 2
iconst_1
iadd
istore 2
goto L3
.limit locals 9
.limit stack 5
.end method

.method public getLabel(I)[B
aload 0
iload 1
invokespecial org/xbill/DNS/Name/offset(I)I
istore 1
aload 0
getfield org/xbill/DNS/Name/name [B
iload 1
baload
iconst_1
iadd
i2b
istore 2
iload 2
newarray byte
astore 3
aload 0
getfield org/xbill/DNS/Name/name [B
iload 1
aload 3
iconst_0
iload 2
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 3
areturn
.limit locals 4
.limit stack 5
.end method

.method public getLabelString(I)Ljava/lang/String;
aload 0
iload 1
invokespecial org/xbill/DNS/Name/offset(I)I
istore 1
aload 0
aload 0
getfield org/xbill/DNS/Name/name [B
iload 1
invokespecial org/xbill/DNS/Name/byteString([BI)Ljava/lang/String;
areturn
.limit locals 2
.limit stack 3
.end method

.method public hashCode()I
iconst_0
istore 2
aload 0
getfield org/xbill/DNS/Name/hashcode I
ifeq L0
aload 0
getfield org/xbill/DNS/Name/hashcode I
ireturn
L0:
aload 0
iconst_0
invokespecial org/xbill/DNS/Name/offset(I)I
istore 1
L1:
iload 1
aload 0
getfield org/xbill/DNS/Name/name [B
arraylength
if_icmpge L2
iload 2
iload 2
iconst_3
ishl
getstatic org/xbill/DNS/Name/lowercase [B
aload 0
getfield org/xbill/DNS/Name/name [B
iload 1
baload
sipush 255
iand
baload
iadd
iadd
istore 2
iload 1
iconst_1
iadd
istore 1
goto L1
L2:
aload 0
iload 2
putfield org/xbill/DNS/Name/hashcode I
aload 0
getfield org/xbill/DNS/Name/hashcode I
ireturn
.limit locals 3
.limit stack 5
.end method

.method public isAbsolute()Z
aload 0
invokevirtual org/xbill/DNS/Name/labels()I
istore 1
iload 1
ifne L0
L1:
iconst_0
ireturn
L0:
aload 0
getfield org/xbill/DNS/Name/name [B
aload 0
iload 1
iconst_1
isub
invokespecial org/xbill/DNS/Name/offset(I)I
baload
ifne L1
iconst_1
ireturn
.limit locals 2
.limit stack 4
.end method

.method public isWild()Z
aload 0
invokevirtual org/xbill/DNS/Name/labels()I
ifne L0
L1:
iconst_0
ireturn
L0:
aload 0
getfield org/xbill/DNS/Name/name [B
iconst_0
baload
iconst_1
if_icmpne L1
aload 0
getfield org/xbill/DNS/Name/name [B
iconst_1
baload
bipush 42
if_icmpne L1
iconst_1
ireturn
.limit locals 1
.limit stack 2
.end method

.method public labels()I
aload 0
invokespecial org/xbill/DNS/Name/getlabels()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public length()S
aload 0
invokespecial org/xbill/DNS/Name/getlabels()I
ifne L0
iconst_0
ireturn
L0:
aload 0
getfield org/xbill/DNS/Name/name [B
arraylength
aload 0
iconst_0
invokespecial org/xbill/DNS/Name/offset(I)I
isub
i2s
ireturn
.limit locals 1
.limit stack 3
.end method

.method public relativize(Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/Name;
aload 1
ifnull L0
aload 0
aload 1
invokevirtual org/xbill/DNS/Name/subdomain(Lorg/xbill/DNS/Name;)Z
ifne L1
L0:
aload 0
areturn
L1:
new org/xbill/DNS/Name
dup
invokespecial org/xbill/DNS/Name/<init>()V
astore 3
aload 0
aload 3
invokestatic org/xbill/DNS/Name/copy(Lorg/xbill/DNS/Name;Lorg/xbill/DNS/Name;)V
aload 0
invokevirtual org/xbill/DNS/Name/length()S
aload 1
invokevirtual org/xbill/DNS/Name/length()S
isub
istore 2
aload 3
aload 3
invokevirtual org/xbill/DNS/Name/labels()I
aload 1
invokevirtual org/xbill/DNS/Name/labels()I
isub
invokespecial org/xbill/DNS/Name/setlabels(I)V
aload 3
iload 2
newarray byte
putfield org/xbill/DNS/Name/name [B
aload 0
getfield org/xbill/DNS/Name/name [B
aload 0
iconst_0
invokespecial org/xbill/DNS/Name/offset(I)I
aload 3
getfield org/xbill/DNS/Name/name [B
iconst_0
iload 2
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 3
areturn
.limit locals 4
.limit stack 5
.end method

.method public subdomain(Lorg/xbill/DNS/Name;)Z
aload 0
invokevirtual org/xbill/DNS/Name/labels()I
istore 2
aload 1
invokevirtual org/xbill/DNS/Name/labels()I
istore 3
iload 3
iload 2
if_icmple L0
iconst_0
ireturn
L0:
iload 3
iload 2
if_icmpne L1
aload 0
aload 1
invokevirtual org/xbill/DNS/Name/equals(Ljava/lang/Object;)Z
ireturn
L1:
aload 1
aload 0
getfield org/xbill/DNS/Name/name [B
aload 0
iload 2
iload 3
isub
invokespecial org/xbill/DNS/Name/offset(I)I
invokespecial org/xbill/DNS/Name/equals([BI)Z
ireturn
.limit locals 4
.limit stack 5
.end method

.method public toString()Ljava/lang/String;
aload 0
iconst_0
invokevirtual org/xbill/DNS/Name/toString(Z)Ljava/lang/String;
areturn
.limit locals 1
.limit stack 2
.end method

.method public toString(Z)Ljava/lang/String;
iconst_0
istore 2
aload 0
invokevirtual org/xbill/DNS/Name/labels()I
istore 4
iload 4
ifne L0
ldc "@"
areturn
L0:
iload 4
iconst_1
if_icmpne L1
aload 0
getfield org/xbill/DNS/Name/name [B
aload 0
iconst_0
invokespecial org/xbill/DNS/Name/offset(I)I
baload
ifne L1
ldc "."
areturn
L1:
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
astore 6
aload 0
iconst_0
invokespecial org/xbill/DNS/Name/offset(I)I
istore 3
L2:
iload 2
iload 4
if_icmpge L3
aload 0
getfield org/xbill/DNS/Name/name [B
iload 3
baload
istore 5
iload 5
bipush 63
if_icmple L4
new java/lang/IllegalStateException
dup
ldc "invalid label"
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L4:
iload 5
ifne L5
iload 1
ifne L3
aload 6
bipush 46
invokevirtual java/lang/StringBuffer/append(C)Ljava/lang/StringBuffer;
pop
L3:
aload 6
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
areturn
L5:
iload 2
ifle L6
aload 6
bipush 46
invokevirtual java/lang/StringBuffer/append(C)Ljava/lang/StringBuffer;
pop
L6:
aload 6
aload 0
aload 0
getfield org/xbill/DNS/Name/name [B
iload 3
invokespecial org/xbill/DNS/Name/byteString([BI)Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
iload 3
iload 5
iconst_1
iadd
iadd
istore 3
iload 2
iconst_1
iadd
istore 2
goto L2
.limit locals 7
.limit stack 4
.end method

.method public toWire(Lorg/xbill/DNS/DNSOutput;Lorg/xbill/DNS/Compression;)V
aload 0
invokevirtual org/xbill/DNS/Name/isAbsolute()Z
ifne L0
new java/lang/IllegalArgumentException
dup
ldc "toWire() called on non-absolute name"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
invokevirtual org/xbill/DNS/Name/labels()I
istore 5
iconst_0
istore 3
L1:
iload 3
iload 5
iconst_1
isub
if_icmpge L2
iload 3
ifne L3
aload 0
astore 6
L4:
iconst_m1
istore 4
aload 2
ifnull L5
aload 2
aload 6
invokevirtual org/xbill/DNS/Compression/get(Lorg/xbill/DNS/Name;)I
istore 4
L5:
iload 4
iflt L6
aload 1
iload 4
ldc_w 49152
ior
invokevirtual org/xbill/DNS/DNSOutput/writeU16(I)V
return
L3:
new org/xbill/DNS/Name
dup
aload 0
iload 3
invokespecial org/xbill/DNS/Name/<init>(Lorg/xbill/DNS/Name;I)V
astore 6
goto L4
L6:
aload 2
ifnull L7
aload 2
aload 1
invokevirtual org/xbill/DNS/DNSOutput/current()I
aload 6
invokevirtual org/xbill/DNS/Compression/add(ILorg/xbill/DNS/Name;)V
L7:
aload 0
iload 3
invokespecial org/xbill/DNS/Name/offset(I)I
istore 4
aload 1
aload 0
getfield org/xbill/DNS/Name/name [B
iload 4
aload 0
getfield org/xbill/DNS/Name/name [B
iload 4
baload
iconst_1
iadd
invokevirtual org/xbill/DNS/DNSOutput/writeByteArray([BII)V
iload 3
iconst_1
iadd
istore 3
goto L1
L2:
aload 1
iconst_0
invokevirtual org/xbill/DNS/DNSOutput/writeU8(I)V
return
.limit locals 7
.limit stack 5
.end method

.method public toWire(Lorg/xbill/DNS/DNSOutput;Lorg/xbill/DNS/Compression;Z)V
iload 3
ifeq L0
aload 0
aload 1
invokevirtual org/xbill/DNS/Name/toWireCanonical(Lorg/xbill/DNS/DNSOutput;)V
return
L0:
aload 0
aload 1
aload 2
invokevirtual org/xbill/DNS/Name/toWire(Lorg/xbill/DNS/DNSOutput;Lorg/xbill/DNS/Compression;)V
return
.limit locals 4
.limit stack 3
.end method

.method public toWire()[B
new org/xbill/DNS/DNSOutput
dup
invokespecial org/xbill/DNS/DNSOutput/<init>()V
astore 1
aload 0
aload 1
aconst_null
invokevirtual org/xbill/DNS/Name/toWire(Lorg/xbill/DNS/DNSOutput;Lorg/xbill/DNS/Compression;)V
aload 1
invokevirtual org/xbill/DNS/DNSOutput/toByteArray()[B
areturn
.limit locals 2
.limit stack 3
.end method

.method public toWireCanonical(Lorg/xbill/DNS/DNSOutput;)V
aload 1
aload 0
invokevirtual org/xbill/DNS/Name/toWireCanonical()[B
invokevirtual org/xbill/DNS/DNSOutput/writeByteArray([B)V
return
.limit locals 2
.limit stack 2
.end method

.method public toWireCanonical()[B
aload 0
invokevirtual org/xbill/DNS/Name/labels()I
istore 5
iload 5
ifne L0
iconst_0
newarray byte
astore 7
L1:
aload 7
areturn
L0:
aload 0
getfield org/xbill/DNS/Name/name [B
arraylength
aload 0
iconst_0
invokespecial org/xbill/DNS/Name/offset(I)I
isub
newarray byte
astore 8
aload 0
iconst_0
invokespecial org/xbill/DNS/Name/offset(I)I
istore 1
iconst_0
istore 2
iconst_0
istore 3
L2:
aload 8
astore 7
iload 3
iload 5
if_icmpge L1
aload 0
getfield org/xbill/DNS/Name/name [B
iload 1
baload
istore 6
iload 6
bipush 63
if_icmple L3
new java/lang/IllegalStateException
dup
ldc "invalid label"
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L3:
aload 8
iload 2
aload 0
getfield org/xbill/DNS/Name/name [B
iload 1
baload
bastore
iload 1
iconst_1
iadd
istore 1
iload 2
iconst_1
iadd
istore 2
iconst_0
istore 4
L4:
iload 4
iload 6
if_icmpge L5
aload 8
iload 2
getstatic org/xbill/DNS/Name/lowercase [B
aload 0
getfield org/xbill/DNS/Name/name [B
iload 1
baload
sipush 255
iand
baload
bastore
iload 4
iconst_1
iadd
istore 4
iload 2
iconst_1
iadd
istore 2
iload 1
iconst_1
iadd
istore 1
goto L4
L5:
iload 3
iconst_1
iadd
istore 3
goto L2
.limit locals 9
.limit stack 5
.end method

.method public wild(I)Lorg/xbill/DNS/Name;
.catch org/xbill/DNS/NameTooLongException from L0 to L1 using L2
iload 1
ifgt L0
new java/lang/IllegalArgumentException
dup
ldc "must replace 1 or more labels"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
new org/xbill/DNS/Name
dup
invokespecial org/xbill/DNS/Name/<init>()V
astore 2
getstatic org/xbill/DNS/Name/wild Lorg/xbill/DNS/Name;
aload 2
invokestatic org/xbill/DNS/Name/copy(Lorg/xbill/DNS/Name;Lorg/xbill/DNS/Name;)V
aload 2
aload 0
getfield org/xbill/DNS/Name/name [B
aload 0
iload 1
invokespecial org/xbill/DNS/Name/offset(I)I
aload 0
invokespecial org/xbill/DNS/Name/getlabels()I
iload 1
isub
invokespecial org/xbill/DNS/Name/append([BII)V
L1:
aload 2
areturn
L2:
astore 2
new java/lang/IllegalStateException
dup
ldc "Name.wild: concatenate failed"
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
.limit locals 3
.limit stack 5
.end method
