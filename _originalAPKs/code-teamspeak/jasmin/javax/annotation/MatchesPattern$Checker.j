.bytecode 50.0
.class public final synchronized javax/annotation/MatchesPattern$Checker
.super java/lang/Object
.implements javax/annotation/meta/TypeQualifierValidator

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static forConstantValue(Ljavax/annotation/MatchesPattern;Ljava/lang/Object;)Ljavax/annotation/meta/When;
aload 0
invokeinterface javax/annotation/MatchesPattern/value()Ljava/lang/String; 0
aload 0
invokeinterface javax/annotation/MatchesPattern/flags()I 0
invokestatic java/util/regex/Pattern/compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;
aload 1
checkcast java/lang/String
invokevirtual java/util/regex/Pattern/matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;
invokevirtual java/util/regex/Matcher/matches()Z
ifeq L0
getstatic javax/annotation/meta/When/ALWAYS Ljavax/annotation/meta/When;
areturn
L0:
getstatic javax/annotation/meta/When/NEVER Ljavax/annotation/meta/When;
areturn
.limit locals 2
.limit stack 2
.end method

.method public final synthetic forConstantValue(Ljava/lang/annotation/Annotation;Ljava/lang/Object;)Ljavax/annotation/meta/When;
aload 1
checkcast javax/annotation/MatchesPattern
astore 1
aload 1
invokeinterface javax/annotation/MatchesPattern/value()Ljava/lang/String; 0
aload 1
invokeinterface javax/annotation/MatchesPattern/flags()I 0
invokestatic java/util/regex/Pattern/compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;
aload 2
checkcast java/lang/String
invokevirtual java/util/regex/Pattern/matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;
invokevirtual java/util/regex/Matcher/matches()Z
ifeq L0
getstatic javax/annotation/meta/When/ALWAYS Ljavax/annotation/meta/When;
areturn
L0:
getstatic javax/annotation/meta/When/NEVER Ljavax/annotation/meta/When;
areturn
.limit locals 3
.limit stack 2
.end method
