.bytecode 50.0
.class public abstract interface annotation javax/annotation/Syntax
.super java/lang/Object
.implements java/lang/annotation/Annotation
.annotation visible Ljava/lang/annotation/Documented;
.end annotation
.annotation visible Ljava/lang/annotation/Retention;
value e Ljava/lang/annotation/RetentionPolicy; = "RUNTIME"
.end annotation
.annotation visible Ljavax/annotation/meta/TypeQualifier;
applicableTo c = Ljava/lang/String;
.end annotation

.method public abstract value()Ljava/lang/String;
.end method

.method public abstract when()Ljavax/annotation/meta/When;
.annotation default
e Ljavax/annotation/meta/When; = "ALWAYS"
.end annotation
.end method
