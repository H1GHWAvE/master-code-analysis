.bytecode 50.0
.class public synchronized com/teamspeak/ts3client/Ts3Application
.super android/app/Application

.field private static 'p' Lcom/teamspeak/ts3client/Ts3Application;

.field public 'a' Lcom/teamspeak/ts3client/data/e;

.field public 'b' Landroid/os/PowerManager$WakeLock;

.field public 'c' Landroid/support/v4/app/Fragment;

.field public 'd' Ljava/util/logging/Logger;

.field public 'e' Landroid/content/SharedPreferences;

.field public 'f' Lcom/teamspeak/ts3client/StartGUIFragment;

.field 'g' Z

.field public 'h' Landroid/net/ConnectivityManager;

.field 'i' Ljava/util/Vector;

.field 'j' Ljava/util/Vector;

.field public 'k' Landroid/content/Intent;

.field public 'l' Landroid/app/NotificationManager;

.field public 'm' Landroid/os/PowerManager$WakeLock;

.field public 'n' Landroid/net/wifi/WifiManager$WifiLock;

.field public 'o' Landroid/support/v7/app/a;

.field private 'q' Z

.field private 'r' Lcom/teamspeak/ts3client/data/d/f;

.field private 's' Lcom/teamspeak/ts3client/a/p;

.method public <init>()V
aload 0
invokespecial android/app/Application/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static a()Lcom/teamspeak/ts3client/Ts3Application;
getstatic com/teamspeak/ts3client/Ts3Application/p Lcom/teamspeak/ts3client/Ts3Application;
areturn
.limit locals 0
.limit stack 1
.end method

.method private a(Landroid/app/NotificationManager;)V
aload 0
aload 1
putfield com/teamspeak/ts3client/Ts3Application/l Landroid/app/NotificationManager;
return
.limit locals 2
.limit stack 2
.end method

.method private a(Landroid/content/Intent;)V
aload 0
aload 1
putfield com/teamspeak/ts3client/Ts3Application/k Landroid/content/Intent;
return
.limit locals 2
.limit stack 2
.end method

.method private a(Landroid/content/SharedPreferences;)V
aload 0
aload 1
putfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
return
.limit locals 2
.limit stack 2
.end method

.method private a(Landroid/net/ConnectivityManager;)V
aload 0
aload 1
putfield com/teamspeak/ts3client/Ts3Application/h Landroid/net/ConnectivityManager;
return
.limit locals 2
.limit stack 2
.end method

.method private a(Landroid/net/wifi/WifiManager$WifiLock;)V
aload 0
aload 1
putfield com/teamspeak/ts3client/Ts3Application/n Landroid/net/wifi/WifiManager$WifiLock;
return
.limit locals 2
.limit stack 2
.end method

.method private a(Landroid/os/PowerManager$WakeLock;)V
aload 0
aload 1
putfield com/teamspeak/ts3client/Ts3Application/b Landroid/os/PowerManager$WakeLock;
return
.limit locals 2
.limit stack 2
.end method

.method private a(Landroid/support/v4/app/Fragment;)V
aload 0
aload 1
putfield com/teamspeak/ts3client/Ts3Application/c Landroid/support/v4/app/Fragment;
return
.limit locals 2
.limit stack 2
.end method

.method private a(Landroid/support/v7/app/a;)V
aload 0
aload 1
putfield com/teamspeak/ts3client/Ts3Application/o Landroid/support/v7/app/a;
return
.limit locals 2
.limit stack 2
.end method

.method private a(Lcom/teamspeak/ts3client/StartGUIFragment;)V
aload 0
aload 1
putfield com/teamspeak/ts3client/Ts3Application/f Lcom/teamspeak/ts3client/StartGUIFragment;
return
.limit locals 2
.limit stack 2
.end method

.method private a(Lcom/teamspeak/ts3client/data/e;)V
aload 0
aload 1
putfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
return
.limit locals 2
.limit stack 2
.end method

.method private a(Ljava/util/Vector;)V
aload 0
aload 1
putfield com/teamspeak/ts3client/Ts3Application/j Ljava/util/Vector;
return
.limit locals 2
.limit stack 2
.end method

.method private a(Ljava/util/logging/Logger;)V
aload 0
aload 1
putfield com/teamspeak/ts3client/Ts3Application/d Ljava/util/logging/Logger;
return
.limit locals 2
.limit stack 2
.end method

.method private a(Z)V
aload 0
iload 1
putfield com/teamspeak/ts3client/Ts3Application/q Z
return
.limit locals 2
.limit stack 2
.end method

.method private b(Landroid/os/PowerManager$WakeLock;)V
aload 0
aload 1
putfield com/teamspeak/ts3client/Ts3Application/m Landroid/os/PowerManager$WakeLock;
return
.limit locals 2
.limit stack 2
.end method

.method private b(Ljava/util/Vector;)V
aload 0
aload 1
putfield com/teamspeak/ts3client/Ts3Application/i Ljava/util/Vector;
return
.limit locals 2
.limit stack 2
.end method

.method private k()V
aload 0
iconst_0
putfield com/teamspeak/ts3client/Ts3Application/g Z
return
.limit locals 1
.limit stack 2
.end method

.method private l()V
aload 0
iconst_1
putfield com/teamspeak/ts3client/Ts3Application/g Z
return
.limit locals 1
.limit stack 2
.end method

.method private m()Landroid/net/ConnectivityManager;
aload 0
getfield com/teamspeak/ts3client/Ts3Application/h Landroid/net/ConnectivityManager;
areturn
.limit locals 1
.limit stack 1
.end method

.method private n()Lcom/teamspeak/ts3client/StartGUIFragment;
aload 0
getfield com/teamspeak/ts3client/Ts3Application/f Lcom/teamspeak/ts3client/StartGUIFragment;
areturn
.limit locals 1
.limit stack 1
.end method

.method private o()Landroid/os/PowerManager$WakeLock;
aload 0
getfield com/teamspeak/ts3client/Ts3Application/b Landroid/os/PowerManager$WakeLock;
areturn
.limit locals 1
.limit stack 1
.end method

.method private p()Landroid/os/PowerManager$WakeLock;
aload 0
getfield com/teamspeak/ts3client/Ts3Application/m Landroid/os/PowerManager$WakeLock;
areturn
.limit locals 1
.limit stack 1
.end method

.method private q()Landroid/net/wifi/WifiManager$WifiLock;
aload 0
getfield com/teamspeak/ts3client/Ts3Application/n Landroid/net/wifi/WifiManager$WifiLock;
areturn
.limit locals 1
.limit stack 1
.end method

.method private r()Z
aload 0
getfield com/teamspeak/ts3client/Ts3Application/g Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private s()Z
aload 0
getfield com/teamspeak/ts3client/Ts3Application/q Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private static t()Z
iconst_0
ireturn
.limit locals 0
.limit stack 1
.end method

.method private u()Ljava/util/Vector;
aload 0
getfield com/teamspeak/ts3client/Ts3Application/j Ljava/util/Vector;
areturn
.limit locals 1
.limit stack 1
.end method

.method private v()Ljava/util/Vector;
aload 0
getfield com/teamspeak/ts3client/Ts3Application/i Ljava/util/Vector;
areturn
.limit locals 1
.limit stack 1
.end method

.method private w()Landroid/content/Intent;
aload 0
getfield com/teamspeak/ts3client/Ts3Application/k Landroid/content/Intent;
areturn
.limit locals 1
.limit stack 1
.end method

.method private x()Landroid/support/v7/app/a;
aload 0
getfield com/teamspeak/ts3client/Ts3Application/o Landroid/support/v7/app/a;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final b()Lcom/teamspeak/ts3client/data/d/f;
aload 0
getfield com/teamspeak/ts3client/Ts3Application/r Lcom/teamspeak/ts3client/data/d/f;
ifnonnull L0
aload 0
new com/teamspeak/ts3client/data/d/f
dup
aload 0
invokespecial com/teamspeak/ts3client/data/d/f/<init>(Lcom/teamspeak/ts3client/Ts3Application;)V
putfield com/teamspeak/ts3client/Ts3Application/r Lcom/teamspeak/ts3client/data/d/f;
L0:
aload 0
getfield com/teamspeak/ts3client/Ts3Application/r Lcom/teamspeak/ts3client/data/d/f;
areturn
.limit locals 1
.limit stack 4
.end method

.method public final c()Lcom/teamspeak/ts3client/data/e;
aload 0
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final d()Landroid/support/v4/app/Fragment;
aload 0
getfield com/teamspeak/ts3client/Ts3Application/c Landroid/support/v4/app/Fragment;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final e()Ljava/util/logging/Logger;
aload 0
getfield com/teamspeak/ts3client/Ts3Application/d Ljava/util/logging/Logger;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final f()Landroid/content/SharedPreferences;
aload 0
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final g()Lcom/teamspeak/ts3client/a/p;
aload 0
getfield com/teamspeak/ts3client/Ts3Application/s Lcom/teamspeak/ts3client/a/p;
ifnonnull L0
aload 0
new com/teamspeak/ts3client/a/p
dup
aload 0
invokespecial com/teamspeak/ts3client/a/p/<init>(Landroid/content/Context;)V
putfield com/teamspeak/ts3client/Ts3Application/s Lcom/teamspeak/ts3client/a/p;
L0:
aload 0
getfield com/teamspeak/ts3client/Ts3Application/s Lcom/teamspeak/ts3client/a/p;
areturn
.limit locals 1
.limit stack 4
.end method

.method public final h()I
aload 0
getfield com/teamspeak/ts3client/Ts3Application/j Ljava/util/Vector;
iconst_0
invokevirtual java/util/Vector/get(I)Ljava/lang/Object;
checkcast java/lang/Integer
invokevirtual java/lang/Integer/intValue()I
ireturn
.limit locals 1
.limit stack 2
.end method

.method public final i()I
aload 0
getfield com/teamspeak/ts3client/Ts3Application/i Ljava/util/Vector;
iconst_0
invokevirtual java/util/Vector/get(I)Ljava/lang/Object;
checkcast java/lang/Integer
invokevirtual java/lang/Integer/intValue()I
ireturn
.limit locals 1
.limit stack 2
.end method

.method public final j()Landroid/app/NotificationManager;
aload 0
getfield com/teamspeak/ts3client/Ts3Application/l Landroid/app/NotificationManager;
areturn
.limit locals 1
.limit stack 1
.end method

.method public onCreate()V
aload 0
putstatic com/teamspeak/ts3client/Ts3Application/p Lcom/teamspeak/ts3client/Ts3Application;
aload 0
invokespecial android/app/Application/onCreate()V
return
.limit locals 1
.limit stack 1
.end method
