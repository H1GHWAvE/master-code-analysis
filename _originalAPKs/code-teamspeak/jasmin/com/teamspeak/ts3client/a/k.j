.bytecode 50.0
.class public final synchronized com/teamspeak/ts3client/a/k
.super java/lang/Object

.field private static 'm' Lcom/teamspeak/ts3client/a/k;

.field 'a' Z

.field 'b' [S

.field 'c' Landroid/media/AudioRecord;

.field public 'd' Lcom/teamspeak/ts3client/jni/Ts3Jni;

.field 'e' Z

.field 'f' Landroid/media/AudioTrack;

.field public 'g' Landroid/media/AudioManager;

.field 'h' Lcom/teamspeak/ts3client/Ts3Application;

.field 'i' I

.field 'j' I

.field public 'k' Z

.field public 'l' Z

.field private 'n' Ljava/lang/Thread;

.field private 'o' I

.field private 'p' I

.field private 'q' I

.field private 'r' Ljava/lang/Thread;

.field private 's' I

.field private 't' I

.field private 'u' I

.field private 'v' Z

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
iconst_0
putfield com/teamspeak/ts3client/a/k/a Z
aload 0
iconst_0
putfield com/teamspeak/ts3client/a/k/e Z
aload 0
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
putfield com/teamspeak/ts3client/a/k/h Lcom/teamspeak/ts3client/Ts3Application;
return
.limit locals 1
.limit stack 2
.end method

.method private static synthetic a(Lcom/teamspeak/ts3client/a/k;)Landroid/media/AudioTrack;
aload 0
getfield com/teamspeak/ts3client/a/k/f Landroid/media/AudioTrack;
areturn
.limit locals 1
.limit stack 1
.end method

.method public static a()Lcom/teamspeak/ts3client/a/k;
.catch all from L0 to L1 using L2
.catch all from L1 to L3 using L2
ldc com/teamspeak/ts3client/a/k
monitorenter
L0:
getstatic com/teamspeak/ts3client/a/k/m Lcom/teamspeak/ts3client/a/k;
ifnonnull L1
new com/teamspeak/ts3client/a/k
dup
invokespecial com/teamspeak/ts3client/a/k/<init>()V
putstatic com/teamspeak/ts3client/a/k/m Lcom/teamspeak/ts3client/a/k;
L1:
getstatic com/teamspeak/ts3client/a/k/m Lcom/teamspeak/ts3client/a/k;
astore 0
L3:
ldc com/teamspeak/ts3client/a/k
monitorexit
aload 0
areturn
L2:
astore 0
ldc com/teamspeak/ts3client/a/k
monitorexit
aload 0
athrow
.limit locals 1
.limit stack 2
.end method

.method public static a(Landroid/content/Context;I)Ljava/util/Vector;
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L2
.catch java/lang/Exception from L4 to L5 using L2
new java/util/Vector
dup
invokespecial java/util/Vector/<init>()V
astore 6
iload 1
invokestatic android/media/AudioTrack/getNativeOutputSampleRate(I)I
istore 3
aload 0
invokevirtual android/content/Context/getApplicationContext()Landroid/content/Context;
pop
iconst_0
istore 2
L6:
iload 2
iconst_5
if_icmpge L7
iconst_5
newarray int
dup
iconst_0
ldc_w 44100
iastore
dup
iconst_1
sipush 32000
iastore
dup
iconst_2
sipush 22050
iastore
dup
iconst_3
sipush 16000
iastore
dup
iconst_4
sipush 8000
iastore
iload 2
iaload
istore 4
iload 4
iload 3
iconst_2
imul
if_icmpge L5
L0:
iload 4
iconst_4
iconst_2
invokestatic android/media/AudioTrack/getMinBufferSize(III)I
istore 5
L1:
iload 5
bipush -2
if_icmpeq L5
L3:
new android/media/AudioTrack
dup
iload 1
iload 4
iconst_4
iconst_2
iload 5
iconst_1
invokespecial android/media/AudioTrack/<init>(IIIIII)V
astore 0
aload 0
invokevirtual android/media/AudioTrack/getState()I
iconst_1
if_icmpne L4
aload 6
iload 4
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual java/util/Vector/add(Ljava/lang/Object;)Z
pop
L4:
aload 0
invokevirtual android/media/AudioTrack/release()V
L5:
iload 2
iconst_1
iadd
istore 2
goto L6
L2:
astore 0
ldc "Audio"
new java/lang/StringBuilder
dup
ldc "notSupportedplay: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
iload 4
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/d(Ljava/lang/String;Ljava/lang/String;)I
pop
goto L5
L7:
aload 6
invokevirtual java/util/Vector/size()I
ifgt L8
aload 6
iconst_0
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual java/util/Vector/add(Ljava/lang/Object;)Z
pop
aload 6
areturn
L8:
aload 6
areturn
.limit locals 7
.limit stack 8
.end method

.method public static a(Landroid/content/Context;II)Ljava/util/Vector;
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L2
.catch java/lang/Exception from L4 to L5 using L2
new java/util/Vector
dup
invokespecial java/util/Vector/<init>()V
astore 6
iload 1
invokestatic android/media/AudioTrack/getNativeOutputSampleRate(I)I
istore 3
aload 0
invokevirtual android/content/Context/getApplicationContext()Landroid/content/Context;
pop
iconst_0
istore 1
L6:
iload 1
iconst_5
if_icmpge L7
iconst_5
newarray int
dup
iconst_0
ldc_w 44100
iastore
dup
iconst_1
sipush 32000
iastore
dup
iconst_2
sipush 22050
iastore
dup
iconst_3
sipush 16000
iastore
dup
iconst_4
sipush 8000
iastore
iload 1
iaload
istore 4
iload 4
iload 3
iconst_2
imul
if_icmpge L5
L0:
iload 4
bipush 16
iconst_2
invokestatic android/media/AudioRecord/getMinBufferSize(III)I
istore 5
L1:
iload 5
bipush -2
if_icmpeq L5
L3:
new android/media/AudioRecord
dup
iload 2
iload 4
bipush 16
iconst_2
iload 5
invokespecial android/media/AudioRecord/<init>(IIIII)V
astore 0
aload 0
invokevirtual android/media/AudioRecord/getState()I
iconst_1
if_icmpne L4
aload 6
iload 4
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual java/util/Vector/add(Ljava/lang/Object;)Z
pop
L4:
aload 0
invokevirtual android/media/AudioRecord/release()V
L5:
iload 1
iconst_1
iadd
istore 1
goto L6
L2:
astore 0
ldc "Audio"
new java/lang/StringBuilder
dup
ldc "notSupportedrec: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
iload 4
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/d(Ljava/lang/String;Ljava/lang/String;)I
pop
goto L5
L7:
aload 6
invokevirtual java/util/Vector/size()I
ifgt L8
aload 6
iconst_0
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual java/util/Vector/add(Ljava/lang/Object;)Z
pop
aload 6
areturn
L8:
aload 6
areturn
.limit locals 7
.limit stack 7
.end method

.method private a(Lcom/teamspeak/ts3client/jni/Ts3Jni;)V
aload 0
aload 1
putfield com/teamspeak/ts3client/a/k/d Lcom/teamspeak/ts3client/jni/Ts3Jni;
return
.limit locals 2
.limit stack 2
.end method

.method private static synthetic b(Lcom/teamspeak/ts3client/a/k;)Lcom/teamspeak/ts3client/Ts3Application;
aload 0
getfield com/teamspeak/ts3client/a/k/h Lcom/teamspeak/ts3client/Ts3Application;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static synthetic c(Lcom/teamspeak/ts3client/a/k;)Z
aload 0
getfield com/teamspeak/ts3client/a/k/e Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private static synthetic d(Lcom/teamspeak/ts3client/a/k;)Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/a/k/d Lcom/teamspeak/ts3client/jni/Ts3Jni;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static synthetic e(Lcom/teamspeak/ts3client/a/k;)I
aload 0
getfield com/teamspeak/ts3client/a/k/j I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private static synthetic f(Lcom/teamspeak/ts3client/a/k;)Landroid/media/AudioRecord;
aload 0
getfield com/teamspeak/ts3client/a/k/c Landroid/media/AudioRecord;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static synthetic g(Lcom/teamspeak/ts3client/a/k;)Z
aload 0
getfield com/teamspeak/ts3client/a/k/a Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private h()V
aload 0
iconst_1
putfield com/teamspeak/ts3client/a/k/k Z
aload 0
getfield com/teamspeak/ts3client/a/k/g Landroid/media/AudioManager;
aload 0
getfield com/teamspeak/ts3client/a/k/k Z
invokevirtual android/media/AudioManager/setMicrophoneMute(Z)V
return
.limit locals 1
.limit stack 2
.end method

.method private static synthetic h(Lcom/teamspeak/ts3client/a/k;)[S
aload 0
getfield com/teamspeak/ts3client/a/k/b [S
areturn
.limit locals 1
.limit stack 1
.end method

.method private static synthetic i(Lcom/teamspeak/ts3client/a/k;)I
aload 0
getfield com/teamspeak/ts3client/a/k/i I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private i()V
aload 0
getfield com/teamspeak/ts3client/a/k/l Z
ifeq L0
return
L0:
aload 0
getfield com/teamspeak/ts3client/a/k/g Landroid/media/AudioManager;
ifnull L1
aload 0
getfield com/teamspeak/ts3client/a/k/g Landroid/media/AudioManager;
invokevirtual android/media/AudioManager/stopBluetoothSco()V
aload 0
getfield com/teamspeak/ts3client/a/k/g Landroid/media/AudioManager;
iconst_0
invokevirtual android/media/AudioManager/setMicrophoneMute(Z)V
aload 0
getfield com/teamspeak/ts3client/a/k/g Landroid/media/AudioManager;
iconst_0
invokevirtual android/media/AudioManager/setSpeakerphoneOn(Z)V
aload 0
getfield com/teamspeak/ts3client/a/k/g Landroid/media/AudioManager;
iconst_0
invokevirtual android/media/AudioManager/setMode(I)V
L1:
aload 0
aconst_null
putfield com/teamspeak/ts3client/a/k/g Landroid/media/AudioManager;
return
.limit locals 1
.limit stack 2
.end method

.method private j()V
aload 0
getfield com/teamspeak/ts3client/a/k/g Landroid/media/AudioManager;
aload 0
getfield com/teamspeak/ts3client/a/k/t I
bipush 15
iconst_0
invokevirtual android/media/AudioManager/setStreamVolume(III)V
return
.limit locals 1
.limit stack 4
.end method

.method public final a(Ljava/lang/Boolean;)V
aload 0
getfield com/teamspeak/ts3client/a/k/g Landroid/media/AudioManager;
ifnull L0
aload 0
getfield com/teamspeak/ts3client/a/k/g Landroid/media/AudioManager;
aload 1
invokevirtual java/lang/Boolean/booleanValue()Z
invokevirtual android/media/AudioManager/setSpeakerphoneOn(Z)V
L0:
return
.limit locals 2
.limit stack 2
.end method

.method public final a(Z)V
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L2
iload 1
ifeq L3
L0:
aload 0
getfield com/teamspeak/ts3client/a/k/g Landroid/media/AudioManager;
aload 0
getfield com/teamspeak/ts3client/a/k/t I
iconst_1
iconst_1
invokevirtual android/media/AudioManager/adjustStreamVolume(III)V
L1:
return
L3:
aload 0
getfield com/teamspeak/ts3client/a/k/g Landroid/media/AudioManager;
aload 0
getfield com/teamspeak/ts3client/a/k/t I
iconst_m1
iconst_1
invokevirtual android/media/AudioManager/adjustStreamVolume(III)V
L4:
return
L2:
astore 2
return
.limit locals 3
.limit stack 4
.end method

.method public final a(IIII)Z
iconst_1
istore 5
aload 0
iconst_1
putfield com/teamspeak/ts3client/a/k/l Z
aload 0
getfield com/teamspeak/ts3client/a/k/c Landroid/media/AudioRecord;
ifnull L0
aload 0
invokevirtual com/teamspeak/ts3client/a/k/f()V
aload 0
getfield com/teamspeak/ts3client/a/k/c Landroid/media/AudioRecord;
invokevirtual android/media/AudioRecord/release()V
L0:
aload 0
getfield com/teamspeak/ts3client/a/k/f Landroid/media/AudioTrack;
ifnull L1
aload 0
invokevirtual com/teamspeak/ts3client/a/k/e()V
aload 0
getfield com/teamspeak/ts3client/a/k/f Landroid/media/AudioTrack;
invokevirtual android/media/AudioTrack/release()V
L1:
aload 0
iload 1
putfield com/teamspeak/ts3client/a/k/i I
aload 0
iload 2
putfield com/teamspeak/ts3client/a/k/j I
aload 0
iload 3
putfield com/teamspeak/ts3client/a/k/t I
aload 0
iload 4
putfield com/teamspeak/ts3client/a/k/u I
aload 0
iconst_2
putfield com/teamspeak/ts3client/a/k/p I
aload 0
bipush 16
putfield com/teamspeak/ts3client/a/k/o I
aload 0
iload 1
aload 0
getfield com/teamspeak/ts3client/a/k/o I
aload 0
getfield com/teamspeak/ts3client/a/k/p I
invokestatic android/media/AudioRecord/getMinBufferSize(III)I
putfield com/teamspeak/ts3client/a/k/q I
aload 0
getfield com/teamspeak/ts3client/a/k/q I
bipush -2
if_icmpne L2
iconst_0
istore 5
L3:
iload 5
ireturn
L2:
aload 0
new android/media/AudioRecord
dup
iload 4
iload 1
aload 0
getfield com/teamspeak/ts3client/a/k/o I
aload 0
getfield com/teamspeak/ts3client/a/k/p I
aload 0
getfield com/teamspeak/ts3client/a/k/q I
invokespecial android/media/AudioRecord/<init>(IIIII)V
putfield com/teamspeak/ts3client/a/k/c Landroid/media/AudioRecord;
aload 0
getfield com/teamspeak/ts3client/a/k/h Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/d Ljava/util/logging/Logger;
getstatic java/util/logging/Level/INFO Ljava/util/logging/Level;
new java/lang/StringBuilder
dup
ldc "recSa:"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
iload 1
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc " PlaySa:"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 2
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc " RecS:"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 4
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc " PlayS:"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 3
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;)V
aload 0
iload 1
bipush 100
idiv
iconst_2
imul
newarray short
putfield com/teamspeak/ts3client/a/k/b [S
aload 0
iload 2
iconst_4
aload 0
getfield com/teamspeak/ts3client/a/k/p I
invokestatic android/media/AudioTrack/getMinBufferSize(III)I
putfield com/teamspeak/ts3client/a/k/s I
aload 0
new android/media/AudioTrack
dup
iload 3
iload 2
iconst_4
iconst_2
aload 0
getfield com/teamspeak/ts3client/a/k/s I
iconst_1
invokespecial android/media/AudioTrack/<init>(IIIIII)V
putfield com/teamspeak/ts3client/a/k/f Landroid/media/AudioTrack;
aload 0
aload 0
getfield com/teamspeak/ts3client/a/k/h Lcom/teamspeak/ts3client/Ts3Application;
ldc "audio"
invokevirtual com/teamspeak/ts3client/Ts3Application/getSystemService(Ljava/lang/String;)Ljava/lang/Object;
checkcast android/media/AudioManager
putfield com/teamspeak/ts3client/a/k/g Landroid/media/AudioManager;
aload 0
aload 0
getfield com/teamspeak/ts3client/a/k/h Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
ldc "audio_handfree"
iconst_0
invokeinterface android/content/SharedPreferences/getBoolean(Ljava/lang/String;Z)Z 2
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual com/teamspeak/ts3client/a/k/a(Ljava/lang/Boolean;)V
iload 3
iconst_3
if_icmpne L3
aload 0
getfield com/teamspeak/ts3client/a/k/g Landroid/media/AudioManager;
iconst_2
invokevirtual android/media/AudioManager/setMode(I)V
iconst_1
ireturn
.limit locals 6
.limit stack 9
.end method

.method public final b()V
aload 0
getfield com/teamspeak/ts3client/a/k/h Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/d Ljava/util/logging/Logger;
getstatic java/util/logging/Level/INFO Ljava/util/logging/Level;
ldc "Release Audio"
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;)V
aload 0
getfield com/teamspeak/ts3client/a/k/c Landroid/media/AudioRecord;
ifnull L0
aload 0
getfield com/teamspeak/ts3client/a/k/c Landroid/media/AudioRecord;
invokevirtual android/media/AudioRecord/release()V
L0:
aload 0
aconst_null
putfield com/teamspeak/ts3client/a/k/c Landroid/media/AudioRecord;
aload 0
getfield com/teamspeak/ts3client/a/k/f Landroid/media/AudioTrack;
ifnull L1
aload 0
getfield com/teamspeak/ts3client/a/k/f Landroid/media/AudioTrack;
invokevirtual android/media/AudioTrack/release()V
L1:
aload 0
aconst_null
putfield com/teamspeak/ts3client/a/k/f Landroid/media/AudioTrack;
aload 0
iconst_0
putfield com/teamspeak/ts3client/a/k/l Z
return
.limit locals 1
.limit stack 3
.end method

.method public final b(Z)V
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L5
aload 0
iload 1
putfield com/teamspeak/ts3client/a/k/v Z
aload 0
getfield com/teamspeak/ts3client/a/k/g Landroid/media/AudioManager;
ifnonnull L6
return
L6:
iload 1
ifeq L3
aload 0
getfield com/teamspeak/ts3client/a/k/g Landroid/media/AudioManager;
invokevirtual android/media/AudioManager/isBluetoothScoAvailableOffCall()Z
pop
aload 0
getfield com/teamspeak/ts3client/a/k/g Landroid/media/AudioManager;
iconst_1
invokevirtual android/media/AudioManager/setBluetoothScoOn(Z)V
L0:
aload 0
getfield com/teamspeak/ts3client/a/k/g Landroid/media/AudioManager;
invokevirtual android/media/AudioManager/startBluetoothSco()V
L1:
invokestatic com/teamspeak/ts3client/a/j/a()Lcom/teamspeak/ts3client/a/j;
invokevirtual com/teamspeak/ts3client/a/j/b()V
return
L3:
aload 0
getfield com/teamspeak/ts3client/a/k/g Landroid/media/AudioManager;
invokevirtual android/media/AudioManager/stopBluetoothSco()V
aload 0
getfield com/teamspeak/ts3client/a/k/g Landroid/media/AudioManager;
iconst_0
invokevirtual android/media/AudioManager/setBluetoothScoOn(Z)V
L4:
goto L1
L5:
astore 2
goto L1
L2:
astore 2
goto L1
.limit locals 3
.limit stack 2
.end method

.method public final c()V
aload 0
getfield com/teamspeak/ts3client/a/k/e Z
ifeq L0
return
L0:
aload 0
iconst_1
putfield com/teamspeak/ts3client/a/k/e Z
aload 0
new com/teamspeak/ts3client/a/l
dup
aload 0
invokespecial com/teamspeak/ts3client/a/l/<init>(Lcom/teamspeak/ts3client/a/k;)V
putfield com/teamspeak/ts3client/a/k/r Ljava/lang/Thread;
aload 0
getfield com/teamspeak/ts3client/a/k/r Ljava/lang/Thread;
invokevirtual java/lang/Thread/start()V
return
.limit locals 1
.limit stack 4
.end method

.method public final d()V
aload 0
getfield com/teamspeak/ts3client/a/k/a Z
ifeq L0
return
L0:
aload 0
iconst_1
putfield com/teamspeak/ts3client/a/k/a Z
aload 0
new com/teamspeak/ts3client/a/m
dup
aload 0
invokespecial com/teamspeak/ts3client/a/m/<init>(Lcom/teamspeak/ts3client/a/k;)V
putfield com/teamspeak/ts3client/a/k/n Ljava/lang/Thread;
aload 0
getfield com/teamspeak/ts3client/a/k/n Ljava/lang/Thread;
invokevirtual java/lang/Thread/start()V
return
.limit locals 1
.limit stack 4
.end method

.method public final e()V
.catch java/lang/IllegalStateException from L0 to L1 using L2
aload 0
iconst_0
putfield com/teamspeak/ts3client/a/k/e Z
aload 0
getfield com/teamspeak/ts3client/a/k/r Ljava/lang/Thread;
ifnull L3
aload 0
getfield com/teamspeak/ts3client/a/k/r Ljava/lang/Thread;
invokevirtual java/lang/Thread/interrupt()V
L3:
aload 0
getfield com/teamspeak/ts3client/a/k/f Landroid/media/AudioTrack;
ifnonnull L0
return
L0:
aload 0
getfield com/teamspeak/ts3client/a/k/f Landroid/media/AudioTrack;
invokevirtual android/media/AudioTrack/stop()V
aload 0
getfield com/teamspeak/ts3client/a/k/f Landroid/media/AudioTrack;
invokevirtual android/media/AudioTrack/flush()V
L1:
return
L2:
astore 1
aload 0
getfield com/teamspeak/ts3client/a/k/h Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/d Ljava/util/logging/Logger;
getstatic java/util/logging/Level/SEVERE Ljava/util/logging/Level;
aload 1
invokevirtual java/lang/IllegalStateException/toString()Ljava/lang/String;
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;)V
return
.limit locals 2
.limit stack 3
.end method

.method public final f()V
.catch java/lang/IllegalStateException from L0 to L1 using L2
aload 0
iconst_0
putfield com/teamspeak/ts3client/a/k/a Z
aload 0
getfield com/teamspeak/ts3client/a/k/n Ljava/lang/Thread;
ifnull L3
aload 0
getfield com/teamspeak/ts3client/a/k/n Ljava/lang/Thread;
invokevirtual java/lang/Thread/interrupt()V
L3:
aload 0
getfield com/teamspeak/ts3client/a/k/c Landroid/media/AudioRecord;
ifnull L1
L0:
aload 0
getfield com/teamspeak/ts3client/a/k/c Landroid/media/AudioRecord;
invokevirtual android/media/AudioRecord/stop()V
aload 0
getfield com/teamspeak/ts3client/a/k/c Landroid/media/AudioRecord;
iconst_0
invokevirtual android/media/AudioRecord/setPositionNotificationPeriod(I)I
pop
L1:
return
L2:
astore 1
aload 0
getfield com/teamspeak/ts3client/a/k/h Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/d Ljava/util/logging/Logger;
getstatic java/util/logging/Level/SEVERE Ljava/util/logging/Level;
aload 1
invokevirtual java/lang/IllegalStateException/toString()Ljava/lang/String;
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;)V
return
.limit locals 2
.limit stack 3
.end method

.method public final g()V
aload 0
iconst_0
putfield com/teamspeak/ts3client/a/k/k Z
aload 0
getfield com/teamspeak/ts3client/a/k/g Landroid/media/AudioManager;
ifnull L0
aload 0
getfield com/teamspeak/ts3client/a/k/g Landroid/media/AudioManager;
aload 0
getfield com/teamspeak/ts3client/a/k/k Z
invokevirtual android/media/AudioManager/setMicrophoneMute(Z)V
L0:
return
.limit locals 1
.limit stack 2
.end method
