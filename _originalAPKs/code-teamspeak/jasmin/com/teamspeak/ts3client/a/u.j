.bytecode 50.0
.class public final synchronized com/teamspeak/ts3client/a/u
.super android/os/AsyncTask

.field final synthetic 'a' Lcom/teamspeak/ts3client/a/t;

.method public <init>(Lcom/teamspeak/ts3client/a/t;)V
aload 0
aload 1
putfield com/teamspeak/ts3client/a/u/a Lcom/teamspeak/ts3client/a/t;
aload 0
invokespecial android/os/AsyncTask/<init>()V
return
.limit locals 2
.limit stack 2
.end method

.method private transient a()Ljava/lang/Void;
.catch java/io/FileNotFoundException from L0 to L1 using L2
.catch java/io/IOException from L0 to L1 using L3
.catch java/io/FileNotFoundException from L1 to L4 using L2
.catch java/io/IOException from L1 to L4 using L3
.catch java/io/FileNotFoundException from L5 to L6 using L2
.catch java/io/IOException from L5 to L6 using L3
.catch java/io/FileNotFoundException from L7 to L8 using L2
.catch java/io/IOException from L7 to L8 using L3
.catch java/io/FileNotFoundException from L9 to L10 using L2
.catch java/io/IOException from L9 to L10 using L3
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/getAssets()Landroid/content/res/AssetManager;
astore 1
L0:
aload 1
ldc "sound/female/settings.ini"
invokevirtual android/content/res/AssetManager/open(Ljava/lang/String;)Ljava/io/InputStream;
astore 2
new java/io/DataInputStream
dup
aload 2
invokespecial java/io/DataInputStream/<init>(Ljava/io/InputStream;)V
astore 3
new java/io/BufferedReader
dup
new java/io/InputStreamReader
dup
aload 3
invokespecial java/io/InputStreamReader/<init>(Ljava/io/InputStream;)V
invokespecial java/io/BufferedReader/<init>(Ljava/io/Reader;)V
astore 4
L1:
aload 4
invokevirtual java/io/BufferedReader/readLine()Ljava/lang/String;
astore 5
L4:
aload 5
ifnull L9
L5:
aload 5
ldc "(.*= play\\(\".*\"\\))"
invokevirtual java/lang/String/matches(Ljava/lang/String;)Z
ifeq L1
aload 5
ldc "="
invokevirtual java/lang/String/split(Ljava/lang/String;)[Ljava/lang/String;
astore 5
aload 5
iconst_0
aload 5
iconst_0
aaload
invokevirtual java/lang/String/trim()Ljava/lang/String;
aastore
aload 5
iconst_1
aaload
ldc ""
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L1
aload 5
iconst_1
aload 5
iconst_1
aaload
bipush 7
invokevirtual java/lang/String/substring(I)Ljava/lang/String;
aastore
aload 5
iconst_1
aload 5
iconst_1
aaload
iconst_0
aload 5
iconst_1
aaload
invokevirtual java/lang/String/length()I
iconst_2
isub
invokevirtual java/lang/String/substring(II)Ljava/lang/String;
aastore
aload 5
iconst_1
aaload
ldc "${clientType}"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L7
aload 5
iconst_1
aaload
ldc "${clientType}"
ldc "neutral"
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
astore 6
aload 1
new java/lang/StringBuilder
dup
ldc "sound/female/"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 6
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual android/content/res/AssetManager/openFd(Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;
astore 6
aload 0
getfield com/teamspeak/ts3client/a/u/a Lcom/teamspeak/ts3client/a/t;
getfield com/teamspeak/ts3client/a/t/a Ljava/util/HashMap;
new java/lang/StringBuilder
dup
ldc "neutral_"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 5
iconst_0
aaload
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/a/u/a Lcom/teamspeak/ts3client/a/t;
getfield com/teamspeak/ts3client/a/t/c Landroid/media/SoundPool;
aload 6
iconst_1
invokevirtual android/media/SoundPool/load(Landroid/content/res/AssetFileDescriptor;I)I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 6
invokevirtual android/content/res/AssetFileDescriptor/close()V
aload 5
iconst_1
aaload
ldc "${clientType}"
ldc "blocked_user"
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
astore 6
aload 1
new java/lang/StringBuilder
dup
ldc "sound/female/"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 6
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual android/content/res/AssetManager/openFd(Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;
astore 6
aload 0
getfield com/teamspeak/ts3client/a/u/a Lcom/teamspeak/ts3client/a/t;
getfield com/teamspeak/ts3client/a/t/a Ljava/util/HashMap;
new java/lang/StringBuilder
dup
ldc "blocked_"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 5
iconst_0
aaload
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/a/u/a Lcom/teamspeak/ts3client/a/t;
getfield com/teamspeak/ts3client/a/t/c Landroid/media/SoundPool;
aload 6
iconst_1
invokevirtual android/media/SoundPool/load(Landroid/content/res/AssetFileDescriptor;I)I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 6
invokevirtual android/content/res/AssetFileDescriptor/close()V
aload 5
iconst_1
aaload
ldc "${clientType}"
ldc "friend"
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
astore 6
aload 1
new java/lang/StringBuilder
dup
ldc "sound/female/"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 6
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual android/content/res/AssetManager/openFd(Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;
astore 6
aload 0
getfield com/teamspeak/ts3client/a/u/a Lcom/teamspeak/ts3client/a/t;
getfield com/teamspeak/ts3client/a/t/a Ljava/util/HashMap;
new java/lang/StringBuilder
dup
ldc "friend_"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 5
iconst_0
aaload
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/a/u/a Lcom/teamspeak/ts3client/a/t;
getfield com/teamspeak/ts3client/a/t/c Landroid/media/SoundPool;
aload 6
iconst_1
invokevirtual android/media/SoundPool/load(Landroid/content/res/AssetFileDescriptor;I)I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 6
invokevirtual android/content/res/AssetFileDescriptor/close()V
L6:
goto L1
L2:
astore 1
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/d Ljava/util/logging/Logger;
getstatic java/util/logging/Level/SEVERE Ljava/util/logging/Level;
aload 1
invokevirtual java/io/FileNotFoundException/getMessage()Ljava/lang/String;
invokevirtual java/lang/String/toString()Ljava/lang/String;
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;)V
L11:
aconst_null
areturn
L7:
aload 1
new java/lang/StringBuilder
dup
ldc "sound/female/"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 5
iconst_1
aaload
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual android/content/res/AssetManager/openFd(Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;
astore 6
aload 0
getfield com/teamspeak/ts3client/a/u/a Lcom/teamspeak/ts3client/a/t;
getfield com/teamspeak/ts3client/a/t/a Ljava/util/HashMap;
aload 5
iconst_0
aaload
aload 0
getfield com/teamspeak/ts3client/a/u/a Lcom/teamspeak/ts3client/a/t;
getfield com/teamspeak/ts3client/a/t/c Landroid/media/SoundPool;
aload 6
iconst_1
invokevirtual android/media/SoundPool/load(Landroid/content/res/AssetFileDescriptor;I)I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 6
invokevirtual android/content/res/AssetFileDescriptor/close()V
L8:
goto L1
L3:
astore 1
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/d Ljava/util/logging/Logger;
getstatic java/util/logging/Level/SEVERE Ljava/util/logging/Level;
aload 1
invokevirtual java/io/IOException/getMessage()Ljava/lang/String;
invokevirtual java/lang/String/toString()Ljava/lang/String;
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;)V
goto L11
L9:
aload 4
invokevirtual java/io/BufferedReader/close()V
aload 3
invokevirtual java/io/DataInputStream/close()V
aload 2
invokevirtual java/io/InputStream/close()V
L10:
goto L11
.limit locals 7
.limit stack 6
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
aload 0
invokespecial com/teamspeak/ts3client/a/u/a()Ljava/lang/Void;
areturn
.limit locals 2
.limit stack 1
.end method
