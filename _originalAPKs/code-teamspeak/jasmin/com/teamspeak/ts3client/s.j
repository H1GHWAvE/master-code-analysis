.bytecode 50.0
.class final synchronized com/teamspeak/ts3client/s
.super java/lang/Object
.implements android/content/SharedPreferences$OnSharedPreferenceChangeListener

.field private 'a' Lcom/teamspeak/ts3client/Ts3Application;

.method public <init>(Lcom/teamspeak/ts3client/Ts3Application;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
putfield com/teamspeak/ts3client/s/a Lcom/teamspeak/ts3client/Ts3Application;
return
.limit locals 2
.limit stack 2
.end method

.method public final onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
aload 2
ldc "audio_ptt"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L0
aload 0
getfield com/teamspeak/ts3client/s/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
ldc "audio_ptt"
iconst_0
invokeinterface android/content/SharedPreferences/getBoolean(Ljava/lang/String;Z)Z 2
istore 4
aload 0
getfield com/teamspeak/ts3client/s/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/d Ljava/util/logging/Logger;
getstatic java/util/logging/Level/INFO Ljava/util/logging/Level;
new java/lang/StringBuilder
dup
ldc "Setting PTT: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
iload 4
invokevirtual java/lang/StringBuilder/append(Z)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;)V
iload 4
ifeq L1
aload 0
getfield com/teamspeak/ts3client/s/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/s/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
getstatic com/teamspeak/ts3client/jni/d/k Lcom/teamspeak/ts3client/jni/d;
iconst_1
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/a(JLcom/teamspeak/ts3client/jni/d;I)I
pop
aload 0
getfield com/teamspeak/ts3client/s/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/s/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
ldc "vad"
ldc "false"
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_setPreProcessorConfigValue(JLjava/lang/String;Ljava/lang/String;)I
pop
aload 0
getfield com/teamspeak/ts3client/s/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/s/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
ldc "voiceactivation_level"
ldc "-50"
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_setPreProcessorConfigValue(JLjava/lang/String;Ljava/lang/String;)I
pop
aload 0
getfield com/teamspeak/ts3client/s/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/k Lcom/teamspeak/ts3client/t;
invokevirtual com/teamspeak/ts3client/t/H()V
L2:
aload 0
getfield com/teamspeak/ts3client/s/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/l Lcom/teamspeak/ts3client/ConnectionBackground;
invokestatic com/teamspeak/ts3client/ConnectionBackground/f(Lcom/teamspeak/ts3client/ConnectionBackground;)V
L0:
aload 2
ldc "voiceactivation_level"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L3
aload 0
getfield com/teamspeak/ts3client/s/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
ldc "voiceactivation_level"
bipush 10
invokeinterface android/content/SharedPreferences/getInt(Ljava/lang/String;I)I 2
istore 3
aload 0
getfield com/teamspeak/ts3client/s/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/d Ljava/util/logging/Logger;
getstatic java/util/logging/Level/INFO Ljava/util/logging/Level;
new java/lang/StringBuilder
dup
ldc "Setting voiceactivation_level: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
iload 3
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;)V
aload 0
getfield com/teamspeak/ts3client/s/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/s/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
ldc "voiceactivation_level"
iload 3
invokestatic java/lang/String/valueOf(I)Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_setPreProcessorConfigValue(JLjava/lang/String;Ljava/lang/String;)I
pop
L3:
aload 2
ldc "audio_bt"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L4
aload 0
getfield com/teamspeak/ts3client/s/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
ldc "audio_bt"
iconst_0
invokeinterface android/content/SharedPreferences/getBoolean(Ljava/lang/String;Z)Z 2
istore 4
aload 0
getfield com/teamspeak/ts3client/s/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/d Ljava/util/logging/Logger;
getstatic java/util/logging/Level/INFO Ljava/util/logging/Level;
new java/lang/StringBuilder
dup
ldc "Setting BT:"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
iload 4
invokevirtual java/lang/StringBuilder/append(Z)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;)V
iload 4
ifeq L5
aload 0
getfield com/teamspeak/ts3client/s/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/b Lcom/teamspeak/ts3client/a/k;
iconst_1
invokevirtual com/teamspeak/ts3client/a/k/b(Z)V
L4:
aload 2
ldc "audio_use_tts"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
pop
aload 2
ldc "call_setaway"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
pop
aload 2
ldc "call_awaymessage"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
pop
aload 2
ldc "show_squeryclient"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L6
aload 0
getfield com/teamspeak/ts3client/s/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/k Lcom/teamspeak/ts3client/t;
invokevirtual com/teamspeak/ts3client/t/z()V
L6:
aload 2
ldc "audio_handfree"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L7
aload 0
getfield com/teamspeak/ts3client/s/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
ldc "audio_handfree"
iconst_0
invokeinterface android/content/SharedPreferences/getBoolean(Ljava/lang/String;Z)Z 2
ifeq L8
invokestatic com/teamspeak/ts3client/a/j/a()Lcom/teamspeak/ts3client/a/j;
iconst_0
invokevirtual com/teamspeak/ts3client/a/j/a(I)V
L7:
aload 2
ldc "use_proximity"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L9
aload 0
getfield com/teamspeak/ts3client/s/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
ldc "use_proximity"
iconst_1
invokeinterface android/content/SharedPreferences/getBoolean(Ljava/lang/String;Z)Z 2
istore 4
aload 0
getfield com/teamspeak/ts3client/s/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/m Landroid/os/PowerManager$WakeLock;
ifnull L9
aload 0
getfield com/teamspeak/ts3client/s/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/f Lcom/teamspeak/ts3client/StartGUIFragment;
invokevirtual com/teamspeak/ts3client/StartGUIFragment/j()V
aload 0
getfield com/teamspeak/ts3client/s/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/m Landroid/os/PowerManager$WakeLock;
invokevirtual android/os/PowerManager$WakeLock/isHeld()Z
ifeq L10
aload 0
getfield com/teamspeak/ts3client/s/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/m Landroid/os/PowerManager$WakeLock;
invokevirtual android/os/PowerManager$WakeLock/release()V
L10:
iload 4
ifeq L9
aload 0
getfield com/teamspeak/ts3client/s/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/f Lcom/teamspeak/ts3client/StartGUIFragment;
invokevirtual com/teamspeak/ts3client/StartGUIFragment/k()V
L9:
aload 2
ldc "whisper"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L11
aload 0
getfield com/teamspeak/ts3client/s/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/w Lcom/a/b/d/bw;
invokeinterface com/a/b/d/bw/keySet()Ljava/util/Set; 0
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
astore 1
L12:
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L13
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/lang/Integer
invokevirtual java/lang/Integer/intValue()I
istore 3
aload 0
getfield com/teamspeak/ts3client/s/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/s/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
iload 3
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_removeFromAllowedWhispersFrom(JI)I
pop
goto L12
L1:
aload 0
getfield com/teamspeak/ts3client/s/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/b Lcom/teamspeak/ts3client/a/k;
invokevirtual com/teamspeak/ts3client/a/k/d()V
aload 0
getfield com/teamspeak/ts3client/s/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/s/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
getstatic com/teamspeak/ts3client/jni/d/k Lcom/teamspeak/ts3client/jni/d;
iconst_0
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/a(JLcom/teamspeak/ts3client/jni/d;I)I
pop
aload 0
getfield com/teamspeak/ts3client/s/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/s/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
ldc "vad"
ldc "true"
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_setPreProcessorConfigValue(JLjava/lang/String;Ljava/lang/String;)I
pop
aload 0
getfield com/teamspeak/ts3client/s/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
ldc "voiceactivation_level"
bipush 10
invokeinterface android/content/SharedPreferences/getInt(Ljava/lang/String;I)I 2
istore 3
aload 0
getfield com/teamspeak/ts3client/s/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/s/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
ldc "voiceactivation_level"
iload 3
invokestatic java/lang/String/valueOf(I)Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_setPreProcessorConfigValue(JLjava/lang/String;Ljava/lang/String;)I
pop
goto L2
L5:
aload 0
getfield com/teamspeak/ts3client/s/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/b Lcom/teamspeak/ts3client/a/k;
iconst_0
invokevirtual com/teamspeak/ts3client/a/k/b(Z)V
goto L4
L8:
invokestatic com/teamspeak/ts3client/a/j/a()Lcom/teamspeak/ts3client/a/j;
iconst_0
invokevirtual com/teamspeak/ts3client/a/j/b(I)V
goto L7
L13:
aload 0
getfield com/teamspeak/ts3client/s/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/w Lcom/a/b/d/bw;
invokeinterface com/a/b/d/bw/clear()V 0
L11:
aload 2
ldc "talk_notification"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L14
aload 0
getfield com/teamspeak/ts3client/s/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
ldc "talk_notification"
iconst_0
invokeinterface android/content/SharedPreferences/getBoolean(Ljava/lang/String;Z)Z 2
invokestatic com/teamspeak/ts3client/ConnectionBackground/b(Z)Z
pop
invokestatic com/teamspeak/ts3client/ConnectionBackground/e()Z
invokestatic com/teamspeak/ts3client/ConnectionBackground/a(Z)V
L14:
return
.limit locals 5
.limit stack 5
.end method
