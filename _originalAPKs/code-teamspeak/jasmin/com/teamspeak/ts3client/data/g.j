.bytecode 50.0
.class public final synchronized com/teamspeak/ts3client/data/g
.super android/widget/BaseExpandableListAdapter

.field public 'a' Ljava/util/List;

.field public 'b' Lcom/teamspeak/ts3client/data/customExpandableListView;

.field public 'c' Z

.field public 'd' Z

.field private 'e' Landroid/content/Context;

.field private 'f' Lcom/teamspeak/ts3client/Ts3Application;

.field private 'g' Landroid/view/LayoutInflater;

.field private 'h' I

.field private 'i' I

.field private 'j' Landroid/support/v4/app/bi;

.field private 'k' I

.field private 'l' Ljava/util/regex/Pattern;

.method public <init>(Landroid/content/Context;Landroid/support/v4/app/bi;)V
aload 0
invokespecial android/widget/BaseExpandableListAdapter/<init>()V
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield com/teamspeak/ts3client/data/g/a Ljava/util/List;
aload 0
aload 1
putfield com/teamspeak/ts3client/data/g/e Landroid/content/Context;
aload 0
aload 1
invokevirtual android/content/Context/getApplicationContext()Landroid/content/Context;
checkcast com/teamspeak/ts3client/Ts3Application
putfield com/teamspeak/ts3client/data/g/f Lcom/teamspeak/ts3client/Ts3Application;
aload 0
aload 1
invokestatic android/view/LayoutInflater/from(Landroid/content/Context;)Landroid/view/LayoutInflater;
putfield com/teamspeak/ts3client/data/g/g Landroid/view/LayoutInflater;
aload 0
iconst_1
aload 0
getfield com/teamspeak/ts3client/data/g/f Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
ldc "channel_height"
bipush 25
invokeinterface android/content/SharedPreferences/getInt(Ljava/lang/String;I)I 2
i2f
aload 1
invokevirtual android/content/Context/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
invokestatic android/util/TypedValue/applyDimension(IFLandroid/util/DisplayMetrics;)F
f2i
putfield com/teamspeak/ts3client/data/g/h I
aload 0
iconst_1
aload 0
getfield com/teamspeak/ts3client/data/g/f Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
ldc "client_height"
bipush 25
invokeinterface android/content/SharedPreferences/getInt(Ljava/lang/String;I)I 2
i2f
aload 1
invokevirtual android/content/Context/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
invokestatic android/util/TypedValue/applyDimension(IFLandroid/util/DisplayMetrics;)F
f2i
putfield com/teamspeak/ts3client/data/g/i I
aload 0
aload 2
putfield com/teamspeak/ts3client/data/g/j Landroid/support/v4/app/bi;
aload 0
new android/widget/TextView
dup
aload 1
invokespecial android/widget/TextView/<init>(Landroid/content/Context;)V
invokevirtual android/widget/TextView/getTextColors()Landroid/content/res/ColorStateList;
invokevirtual android/content/res/ColorStateList/getDefaultColor()I
putfield com/teamspeak/ts3client/data/g/k I
aload 0
ldc "^[\\W]*\\[(.*)spacer.*?\\](.*)"
invokestatic java/util/regex/Pattern/compile(Ljava/lang/String;)Ljava/util/regex/Pattern;
putfield com/teamspeak/ts3client/data/g/l Ljava/util/regex/Pattern;
return
.limit locals 3
.limit stack 5
.end method

.method private a(F)I
aload 0
getfield com/teamspeak/ts3client/data/g/e Landroid/content/Context;
invokevirtual android/content/Context/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
getfield android/util/DisplayMetrics/density F
fload 1
fmul
ldc_w 0.5F
fadd
f2i
ireturn
.limit locals 2
.limit stack 2
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/data/g;)Landroid/support/v4/app/bi;
aload 0
getfield com/teamspeak/ts3client/data/g/j Landroid/support/v4/app/bi;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static a(Landroid/view/View;)Landroid/view/View;
aload 0
iconst_4
invokevirtual android/view/View/setVisibility(I)V
aload 0
areturn
.limit locals 1
.limit stack 2
.end method

.method private a(Landroid/view/View;Lcom/teamspeak/ts3client/data/u;)Landroid/view/View;
.catch java/lang/Exception from L0 to L1 using L2
aload 1
aload 0
ldc_w -23.0F
invokespecial com/teamspeak/ts3client/data/g/a(F)I
iconst_0
iconst_0
iconst_0
invokevirtual android/view/View/setPadding(IIII)V
aload 1
iconst_0
invokevirtual android/view/View/setBackgroundColor(I)V
aload 2
getfield com/teamspeak/ts3client/data/u/b Landroid/widget/ImageView;
iconst_4
invokevirtual android/widget/ImageView/setVisibility(I)V
aload 2
getfield com/teamspeak/ts3client/data/u/d Landroid/widget/ImageView;
ldc_w 2130837678
ldc_w 16.0F
ldc_w 16.0F
invokestatic com/teamspeak/ts3client/data/d/t/a(IFF)Landroid/graphics/Bitmap;
invokevirtual android/widget/ImageView/setImageBitmap(Landroid/graphics/Bitmap;)V
aload 2
getfield com/teamspeak/ts3client/data/u/c Landroid/widget/TextView;
aload 0
getfield com/teamspeak/ts3client/data/g/a Ljava/util/List;
iconst_0
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast com/teamspeak/ts3client/data/a
getfield com/teamspeak/ts3client/data/a/a Ljava/lang/String;
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
aload 2
getfield com/teamspeak/ts3client/data/u/c Landroid/widget/TextView;
bipush 19
invokevirtual android/widget/TextView/setGravity(I)V
L0:
aload 0
getfield com/teamspeak/ts3client/data/g/f Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/v Lcom/teamspeak/ts3client/data/ab;
getfield com/teamspeak/ts3client/data/ab/m J
lconst_0
lcmp
ifeq L1
aload 0
getfield com/teamspeak/ts3client/data/g/f Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/r Lcom/teamspeak/ts3client/data/c/d;
ifnull L1
new android/widget/ImageView
dup
aload 0
getfield com/teamspeak/ts3client/data/g/e Landroid/content/Context;
invokespecial android/widget/ImageView/<init>(Landroid/content/Context;)V
astore 3
aload 3
aload 0
getfield com/teamspeak/ts3client/data/g/f Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/r Lcom/teamspeak/ts3client/data/c/d;
aload 0
getfield com/teamspeak/ts3client/data/g/f Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/v Lcom/teamspeak/ts3client/data/ab;
getfield com/teamspeak/ts3client/data/ab/m J
invokevirtual com/teamspeak/ts3client/data/c/d/a(J)Lcom/teamspeak/ts3client/data/c/c;
invokevirtual com/teamspeak/ts3client/data/c/c/a()Landroid/graphics/drawable/Drawable;
invokevirtual android/widget/ImageView/setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
aload 3
bipush 20
invokevirtual android/widget/ImageView/setMinimumHeight(I)V
aload 3
bipush 20
invokevirtual android/widget/ImageView/setMinimumWidth(I)V
aload 2
getfield com/teamspeak/ts3client/data/u/a Landroid/widget/LinearLayout;
aload 3
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
L1:
aload 1
new com/teamspeak/ts3client/data/o
dup
aload 0
invokespecial com/teamspeak/ts3client/data/o/<init>(Lcom/teamspeak/ts3client/data/g;)V
invokevirtual android/view/View/setOnTouchListener(Landroid/view/View$OnTouchListener;)V
aload 1
areturn
L2:
astore 2
aload 0
getfield com/teamspeak/ts3client/data/g/f Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/d Ljava/util/logging/Logger;
getstatic java/util/logging/Level/SEVERE Ljava/util/logging/Level;
new java/lang/StringBuilder
dup
ldc "setServerEntry(): "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 2
invokevirtual java/lang/Exception/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;)V
goto L1
.limit locals 4
.limit stack 5
.end method

.method private a()Ljava/util/List;
aload 0
getfield com/teamspeak/ts3client/data/g/a Ljava/util/List;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static a(Landroid/widget/TextView;Ljava/lang/String;)V
aload 1
ldc "."
ldc "\u00b7"
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
astore 1
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
astore 7
aload 1
invokevirtual java/lang/String/toCharArray()[C
astore 8
aload 0
invokevirtual android/widget/TextView/getPaint()Landroid/text/TextPaint;
aload 1
invokevirtual android/graphics/Paint/measureText(Ljava/lang/String;)F
fstore 2
aload 0
invokevirtual android/widget/TextView/getWidth()I
i2f
fstore 3
iconst_1
istore 5
iconst_0
istore 4
L0:
iload 5
i2d
fload 3
fload 2
fdiv
aload 1
invokevirtual java/lang/String/length()I
i2f
fmul
fconst_1
fadd
f2d
invokestatic java/lang/Math/floor(D)D
dcmpg
ifge L1
aload 7
aload 8
iload 4
caload
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
pop
iload 4
iconst_1
iadd
istore 6
iload 6
istore 4
iload 6
aload 8
arraylength
if_icmplt L2
iconst_0
istore 4
L2:
iload 5
iconst_1
iadd
istore 5
goto L0
L1:
aload 0
aload 7
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
return
.limit locals 9
.limit stack 4
.end method

.method private a(Lcom/teamspeak/ts3client/data/customExpandableListView;)V
aload 0
aload 1
putfield com/teamspeak/ts3client/data/g/b Lcom/teamspeak/ts3client/data/customExpandableListView;
return
.limit locals 2
.limit stack 2
.end method

.method private a([Lcom/teamspeak/ts3client/data/a;)V
aload 1
arraylength
ifgt L0
return
L0:
aload 0
getfield com/teamspeak/ts3client/data/g/a Ljava/util/List;
invokeinterface java/util/List/clear()V 0
aload 1
arraylength
istore 3
iconst_0
istore 2
L1:
iload 2
iload 3
if_icmpge L2
aload 1
iload 2
aaload
astore 4
aload 4
ifnull L3
aload 0
getfield com/teamspeak/ts3client/data/g/a Ljava/util/List;
aload 4
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
L3:
iload 2
iconst_1
iadd
istore 2
goto L1
L2:
new com/teamspeak/ts3client/data/a
dup
ldc "empty"
lconst_0
lconst_0
lconst_0
invokespecial com/teamspeak/ts3client/data/a/<init>(Ljava/lang/String;JJJ)V
astore 1
aload 1
iconst_1
putfield com/teamspeak/ts3client/data/a/q Z
aload 0
getfield com/teamspeak/ts3client/data/g/a Ljava/util/List;
aload 1
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
return
.limit locals 5
.limit stack 9
.end method

.method static synthetic b(Lcom/teamspeak/ts3client/data/g;)Lcom/teamspeak/ts3client/data/customExpandableListView;
aload 0
getfield com/teamspeak/ts3client/data/g/b Lcom/teamspeak/ts3client/data/customExpandableListView;
areturn
.limit locals 1
.limit stack 1
.end method

.method private b()Z
aload 0
getfield com/teamspeak/ts3client/data/g/c Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic c(Lcom/teamspeak/ts3client/data/g;)Lcom/teamspeak/ts3client/Ts3Application;
aload 0
getfield com/teamspeak/ts3client/data/g/f Lcom/teamspeak/ts3client/Ts3Application;
areturn
.limit locals 1
.limit stack 1
.end method

.method private c()V
aload 0
iconst_1
putfield com/teamspeak/ts3client/data/g/d Z
return
.limit locals 1
.limit stack 2
.end method

.method static synthetic d(Lcom/teamspeak/ts3client/data/g;)Z
aload 0
getfield com/teamspeak/ts3client/data/g/c Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic e(Lcom/teamspeak/ts3client/data/g;)Ljava/util/List;
aload 0
getfield com/teamspeak/ts3client/data/g/a Ljava/util/List;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic f(Lcom/teamspeak/ts3client/data/g;)Landroid/content/Context;
aload 0
getfield com/teamspeak/ts3client/data/g/e Landroid/content/Context;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final a(Z)V
aload 0
getfield com/teamspeak/ts3client/data/g/d Z
ifeq L0
aload 0
getfield com/teamspeak/ts3client/data/g/c Z
ifeq L0
iload 1
ifne L0
aload 0
iload 1
putfield com/teamspeak/ts3client/data/g/c Z
aload 0
iconst_0
putfield com/teamspeak/ts3client/data/g/d Z
aload 0
getfield com/teamspeak/ts3client/data/g/f Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/k Lcom/teamspeak/ts3client/t;
invokevirtual com/teamspeak/ts3client/t/y()V
L0:
aload 0
iload 1
putfield com/teamspeak/ts3client/data/g/c Z
return
.limit locals 2
.limit stack 2
.end method

.method public final getChild(II)Ljava/lang/Object;
aload 0
getfield com/teamspeak/ts3client/data/g/f Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/d Lcom/teamspeak/ts3client/data/d;
aload 0
getfield com/teamspeak/ts3client/data/g/a Ljava/util/List;
iload 1
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast com/teamspeak/ts3client/data/a
invokevirtual com/teamspeak/ts3client/data/a/k()Ljava/util/ArrayList;
iload 2
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast java/lang/Integer
invokevirtual java/lang/Integer/intValue()I
invokevirtual com/teamspeak/ts3client/data/d/b(I)Lcom/teamspeak/ts3client/data/c;
areturn
.limit locals 3
.limit stack 3
.end method

.method public final getChildId(II)J
lconst_0
lreturn
.limit locals 3
.limit stack 2
.end method

.method public final getChildView(IIZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L5
.catch java/lang/Exception from L4 to L6 using L5
.catch java/lang/Exception from L6 to L7 using L5
.catch java/lang/Exception from L7 to L8 using L5
.catch java/lang/Exception from L8 to L9 using L5
.catch java/lang/Exception from L9 to L10 using L5
.catch java/lang/Exception from L10 to L11 using L5
.catch java/lang/Exception from L11 to L12 using L5
.catch java/lang/Exception from L12 to L13 using L5
.catch java/lang/Exception from L13 to L14 using L5
.catch java/lang/Exception from L14 to L15 using L5
.catch java/lang/Exception from L15 to L16 using L5
.catch java/lang/Exception from L17 to L18 using L5
.catch java/lang/Exception from L19 to L20 using L5
.catch java/lang/Exception from L21 to L22 using L5
.catch java/lang/Exception from L23 to L24 using L5
.catch java/lang/Exception from L25 to L26 using L5
.catch java/lang/Exception from L26 to L27 using L5
.catch java/lang/Exception from L28 to L29 using L5
.catch java/lang/Exception from L30 to L31 using L5
.catch java/lang/Exception from L32 to L33 using L5
.catch java/lang/Exception from L34 to L35 using L5
.catch java/lang/Exception from L36 to L37 using L5
.catch java/lang/Exception from L38 to L39 using L5
.catch java/lang/Exception from L40 to L41 using L5
.catch java/lang/Exception from L42 to L43 using L5
.catch java/lang/Exception from L44 to L45 using L5
.catch java/lang/Exception from L45 to L46 using L5
.catch java/lang/Exception from L47 to L48 using L5
.catch java/lang/Exception from L49 to L50 using L5
.catch java/lang/Exception from L51 to L52 using L5
.catch java/lang/Exception from L52 to L53 using L5
.catch java/lang/Exception from L54 to L55 using L5
.catch java/lang/Exception from L56 to L57 using L5
.catch java/lang/Exception from L58 to L59 using L5
.catch java/lang/Exception from L60 to L61 using L5
.catch java/lang/Exception from L62 to L63 using L5
.catch java/lang/Exception from L64 to L65 using L5
.catch java/lang/Exception from L66 to L67 using L5
.catch java/lang/Exception from L68 to L69 using L5
.catch java/lang/Exception from L70 to L71 using L5
.catch java/lang/Exception from L72 to L73 using L5
.catch java/lang/Exception from L74 to L75 using L5
.catch java/lang/Exception from L76 to L77 using L5
.catch java/lang/Exception from L78 to L79 using L5
.catch java/lang/Exception from L79 to L80 using L5
.catch java/lang/Exception from L81 to L82 using L5
.catch java/lang/Exception from L82 to L83 using L5
.catch java/lang/Exception from L84 to L85 using L5
aload 4
ifnonnull L86
aload 0
getfield com/teamspeak/ts3client/data/g/g Landroid/view/LayoutInflater;
ldc_w 2130903102
aload 5
iconst_0
invokevirtual android/view/LayoutInflater/inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;
astore 4
new com/teamspeak/ts3client/data/t
dup
invokespecial com/teamspeak/ts3client/data/t/<init>()V
astore 5
aload 5
aload 4
ldc_w 2131493248
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/ImageView
putfield com/teamspeak/ts3client/data/t/a Landroid/widget/ImageView;
aload 5
aload 4
ldc_w 2131493250
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/TextView
putfield com/teamspeak/ts3client/data/t/b Landroid/widget/TextView;
aload 5
aload 4
ldc_w 2131493251
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/LinearLayout
putfield com/teamspeak/ts3client/data/t/c Landroid/widget/LinearLayout;
aload 4
aload 5
invokevirtual android/view/View/setTag(Ljava/lang/Object;)V
L0:
aload 4
invokevirtual android/view/View/getTag()Ljava/lang/Object;
checkcast com/teamspeak/ts3client/data/t
astore 5
aload 5
getfield com/teamspeak/ts3client/data/t/b Landroid/widget/TextView;
aload 0
getfield com/teamspeak/ts3client/data/g/k I
invokevirtual android/widget/TextView/setTextColor(I)V
aload 5
getfield com/teamspeak/ts3client/data/t/c Landroid/widget/LinearLayout;
invokevirtual android/widget/LinearLayout/removeAllViews()V
L1:
aload 0
getfield com/teamspeak/ts3client/data/g/a Ljava/util/List;
iload 1
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast com/teamspeak/ts3client/data/a
getfield com/teamspeak/ts3client/data/a/o Z
ifne L3
aload 4
areturn
L2:
astore 5
new com/teamspeak/ts3client/data/t
dup
invokespecial com/teamspeak/ts3client/data/t/<init>()V
astore 5
aload 5
aload 4
ldc_w 2131493248
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/ImageView
putfield com/teamspeak/ts3client/data/t/a Landroid/widget/ImageView;
aload 5
aload 4
ldc_w 2131493250
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/TextView
putfield com/teamspeak/ts3client/data/t/b Landroid/widget/TextView;
aload 5
aload 4
ldc_w 2131493251
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/LinearLayout
putfield com/teamspeak/ts3client/data/t/c Landroid/widget/LinearLayout;
aload 4
aload 5
invokevirtual android/view/View/setTag(Ljava/lang/Object;)V
goto L1
L3:
aload 0
getfield com/teamspeak/ts3client/data/g/f Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/d Lcom/teamspeak/ts3client/data/d;
aload 0
getfield com/teamspeak/ts3client/data/g/a Ljava/util/List;
iload 1
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast com/teamspeak/ts3client/data/a
invokevirtual com/teamspeak/ts3client/data/a/k()Ljava/util/ArrayList;
iload 2
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast java/lang/Integer
invokevirtual java/lang/Integer/intValue()I
invokevirtual com/teamspeak/ts3client/data/d/b(I)Lcom/teamspeak/ts3client/data/c;
astore 7
aload 4
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/widget/AbsListView$LayoutParams
astore 8
aload 8
aload 0
getfield com/teamspeak/ts3client/data/g/i I
putfield android/widget/AbsListView$LayoutParams/height I
aload 4
aload 8
invokevirtual android/view/View/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 4
aload 0
getfield com/teamspeak/ts3client/data/g/a Ljava/util/List;
iload 1
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast com/teamspeak/ts3client/data/a
getfield com/teamspeak/ts3client/data/a/f I
iconst_1
iadd
aload 0
ldc_w 18.0F
invokespecial com/teamspeak/ts3client/data/g/a(F)I
imul
iconst_2
iadd
iconst_0
iconst_0
iconst_0
invokevirtual android/view/View/setPadding(IIII)V
aload 7
getfield com/teamspeak/ts3client/data/c/w Lcom/teamspeak/ts3client/c/a;
ifnull L32
aload 7
getfield com/teamspeak/ts3client/data/c/s Z
ifne L25
aload 7
getfield com/teamspeak/ts3client/data/c/w Lcom/teamspeak/ts3client/c/a;
getfield com/teamspeak/ts3client/c/a/d I
tableswitch 0
L19
L21
L23
default : L87
L4:
aload 7
getfield com/teamspeak/ts3client/data/c/w Lcom/teamspeak/ts3client/c/a;
ifnull L6
aload 7
getfield com/teamspeak/ts3client/data/c/w Lcom/teamspeak/ts3client/c/a;
getfield com/teamspeak/ts3client/c/a/e I
tableswitch 0
L36
L38
L40
default : L88
L6:
aload 7
getfield com/teamspeak/ts3client/data/c/s Z
ifeq L7
aload 5
getfield com/teamspeak/ts3client/data/t/b Landroid/widget/TextView;
ldc_w -65536
invokevirtual android/widget/TextView/setTextColor(I)V
L7:
aload 7
getfield com/teamspeak/ts3client/data/c/c I
aload 0
getfield com/teamspeak/ts3client/data/g/f Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/h I
if_icmpeq L8
aload 7
getfield com/teamspeak/ts3client/data/c/s Z
ifeq L42
L8:
aload 5
getfield com/teamspeak/ts3client/data/t/b Landroid/widget/TextView;
aconst_null
iconst_1
invokevirtual android/widget/TextView/setTypeface(Landroid/graphics/Typeface;I)V
L9:
aload 7
getfield com/teamspeak/ts3client/data/c/l Z
ifeq L44
aload 5
getfield com/teamspeak/ts3client/data/t/a Landroid/widget/ImageView;
ldc_w 2130837635
ldc_w 16.0F
ldc_w 16.0F
invokestatic com/teamspeak/ts3client/data/d/t/a(IFF)Landroid/graphics/Bitmap;
invokevirtual android/widget/ImageView/setImageBitmap(Landroid/graphics/Bitmap;)V
L10:
aload 7
getfield com/teamspeak/ts3client/data/c/A I
iconst_1
if_icmpne L72
aload 5
getfield com/teamspeak/ts3client/data/t/a Landroid/widget/ImageView;
ldc_w 2130837663
ldc_w 16.0F
ldc_w 16.0F
invokestatic com/teamspeak/ts3client/data/d/t/a(IFF)Landroid/graphics/Bitmap;
invokevirtual android/widget/ImageView/setImageBitmap(Landroid/graphics/Bitmap;)V
L11:
aload 7
getfield com/teamspeak/ts3client/data/c/k Z
ifeq L12
new android/widget/ImageView
dup
aload 0
getfield com/teamspeak/ts3client/data/g/e Landroid/content/Context;
invokespecial android/widget/ImageView/<init>(Landroid/content/Context;)V
astore 8
aload 8
bipush 20
invokevirtual android/widget/ImageView/setMinimumHeight(I)V
aload 8
bipush 20
invokevirtual android/widget/ImageView/setMinimumWidth(I)V
aload 8
ldc_w 2130837599
ldc_w 16.0F
ldc_w 16.0F
invokestatic com/teamspeak/ts3client/data/d/t/a(IFF)Landroid/graphics/Bitmap;
invokevirtual android/widget/ImageView/setImageBitmap(Landroid/graphics/Bitmap;)V
aload 5
getfield com/teamspeak/ts3client/data/t/c Landroid/widget/LinearLayout;
aload 8
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
L12:
aload 7
getfield com/teamspeak/ts3client/data/c/o I
aload 0
getfield com/teamspeak/ts3client/data/g/a Ljava/util/List;
iload 1
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast com/teamspeak/ts3client/data/a
getfield com/teamspeak/ts3client/data/a/h I
if_icmpge L14
aload 7
getfield com/teamspeak/ts3client/data/c/r Z
ifeq L76
new android/widget/ImageView
dup
aload 0
getfield com/teamspeak/ts3client/data/g/e Landroid/content/Context;
invokespecial android/widget/ImageView/<init>(Landroid/content/Context;)V
astore 8
aload 8
bipush 20
invokevirtual android/widget/ImageView/setMinimumHeight(I)V
aload 8
bipush 20
invokevirtual android/widget/ImageView/setMinimumWidth(I)V
aload 8
ldc_w 2130837598
ldc_w 16.0F
ldc_w 16.0F
invokestatic com/teamspeak/ts3client/data/d/t/a(IFF)Landroid/graphics/Bitmap;
invokevirtual android/widget/ImageView/setImageBitmap(Landroid/graphics/Bitmap;)V
aload 5
getfield com/teamspeak/ts3client/data/t/c Landroid/widget/LinearLayout;
aload 8
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
L13:
aload 7
getfield com/teamspeak/ts3client/data/c/p Ljava/lang/String;
ldc "0"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L14
aload 7
getfield com/teamspeak/ts3client/data/c/r Z
ifne L14
new android/widget/ImageView
dup
aload 0
getfield com/teamspeak/ts3client/data/g/e Landroid/content/Context;
invokespecial android/widget/ImageView/<init>(Landroid/content/Context;)V
astore 8
aload 8
bipush 20
invokevirtual android/widget/ImageView/setMinimumHeight(I)V
aload 8
bipush 20
invokevirtual android/widget/ImageView/setMinimumWidth(I)V
aload 8
ldc_w 2130837673
ldc_w 16.0F
ldc_w 16.0F
invokestatic com/teamspeak/ts3client/data/d/t/a(IFF)Landroid/graphics/Bitmap;
invokevirtual android/widget/ImageView/setImageBitmap(Landroid/graphics/Bitmap;)V
aload 5
getfield com/teamspeak/ts3client/data/t/c Landroid/widget/LinearLayout;
aload 8
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
L14:
aload 7
getfield com/teamspeak/ts3client/data/c/u Ljava/lang/String;
invokestatic java/lang/Integer/parseInt(Ljava/lang/String;)I
istore 1
aload 0
getfield com/teamspeak/ts3client/data/g/f Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/p Lcom/teamspeak/ts3client/data/a/b;
iload 1
i2l
invokevirtual com/teamspeak/ts3client/data/a/b/a(J)Z
ifeq L15
aload 0
getfield com/teamspeak/ts3client/data/g/f Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/p Lcom/teamspeak/ts3client/data/a/b;
iload 1
i2l
invokevirtual com/teamspeak/ts3client/data/a/b/b(J)Lcom/teamspeak/ts3client/data/a/a;
getfield com/teamspeak/ts3client/data/a/a/d J
lconst_0
lcmp
ifeq L15
new android/widget/ImageView
dup
aload 0
getfield com/teamspeak/ts3client/data/g/e Landroid/content/Context;
invokespecial android/widget/ImageView/<init>(Landroid/content/Context;)V
astore 8
aload 8
aload 0
getfield com/teamspeak/ts3client/data/g/f Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/r Lcom/teamspeak/ts3client/data/c/d;
aload 0
getfield com/teamspeak/ts3client/data/g/f Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/p Lcom/teamspeak/ts3client/data/a/b;
iload 1
i2l
invokevirtual com/teamspeak/ts3client/data/a/b/b(J)Lcom/teamspeak/ts3client/data/a/a;
getfield com/teamspeak/ts3client/data/a/a/d J
invokevirtual com/teamspeak/ts3client/data/c/d/a(J)Lcom/teamspeak/ts3client/data/c/c;
invokevirtual com/teamspeak/ts3client/data/c/c/a()Landroid/graphics/drawable/Drawable;
invokevirtual android/widget/ImageView/setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
aload 8
bipush 20
invokevirtual android/widget/ImageView/setMinimumHeight(I)V
aload 8
bipush 20
invokevirtual android/widget/ImageView/setMinimumWidth(I)V
aload 5
getfield com/teamspeak/ts3client/data/t/c Landroid/widget/LinearLayout;
aload 8
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
L15:
aload 7
getfield com/teamspeak/ts3client/data/c/t Ljava/lang/String;
ldc ","
invokevirtual java/lang/String/split(Ljava/lang/String;)[Ljava/lang/String;
astore 8
aload 8
arraylength
istore 2
L16:
iconst_0
istore 1
L89:
iload 1
iload 2
if_icmpge L78
L17:
aload 8
iload 1
aaload
invokestatic java/lang/Integer/parseInt(Ljava/lang/String;)I
istore 6
aload 0
getfield com/teamspeak/ts3client/data/g/f Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/o Lcom/teamspeak/ts3client/data/g/b;
iload 6
i2l
invokevirtual com/teamspeak/ts3client/data/g/b/a(J)Z
ifeq L90
aload 0
getfield com/teamspeak/ts3client/data/g/f Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/o Lcom/teamspeak/ts3client/data/g/b;
iload 6
i2l
invokevirtual com/teamspeak/ts3client/data/g/b/b(J)Lcom/teamspeak/ts3client/data/g/a;
getfield com/teamspeak/ts3client/data/g/a/d J
lconst_0
lcmp
ifeq L90
new android/widget/ImageView
dup
aload 0
getfield com/teamspeak/ts3client/data/g/e Landroid/content/Context;
invokespecial android/widget/ImageView/<init>(Landroid/content/Context;)V
astore 9
aload 9
bipush 20
invokevirtual android/widget/ImageView/setMinimumHeight(I)V
aload 9
bipush 20
invokevirtual android/widget/ImageView/setMinimumWidth(I)V
aload 9
aload 0
getfield com/teamspeak/ts3client/data/g/f Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/r Lcom/teamspeak/ts3client/data/c/d;
aload 0
getfield com/teamspeak/ts3client/data/g/f Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/o Lcom/teamspeak/ts3client/data/g/b;
iload 6
i2l
invokevirtual com/teamspeak/ts3client/data/g/b/b(J)Lcom/teamspeak/ts3client/data/g/a;
getfield com/teamspeak/ts3client/data/g/a/d J
invokevirtual com/teamspeak/ts3client/data/c/d/a(J)Lcom/teamspeak/ts3client/data/c/c;
invokevirtual com/teamspeak/ts3client/data/c/c/a()Landroid/graphics/drawable/Drawable;
invokevirtual android/widget/ImageView/setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
aload 5
getfield com/teamspeak/ts3client/data/t/c Landroid/widget/LinearLayout;
aload 9
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
L18:
goto L90
L19:
aload 5
getfield com/teamspeak/ts3client/data/t/b Landroid/widget/TextView;
new java/lang/StringBuilder
dup
ldc "["
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 7
getfield com/teamspeak/ts3client/data/c/w Lcom/teamspeak/ts3client/c/a;
getfield com/teamspeak/ts3client/c/a/a Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "] "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 7
getfield com/teamspeak/ts3client/data/c/a Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
L20:
goto L4
L21:
aload 5
getfield com/teamspeak/ts3client/data/t/b Landroid/widget/TextView;
aload 7
getfield com/teamspeak/ts3client/data/c/w Lcom/teamspeak/ts3client/c/a;
getfield com/teamspeak/ts3client/c/a/a Ljava/lang/String;
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
L22:
goto L4
L23:
aload 5
getfield com/teamspeak/ts3client/data/t/b Landroid/widget/TextView;
aload 7
getfield com/teamspeak/ts3client/data/c/a Ljava/lang/String;
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
L24:
goto L4
L25:
aload 7
getfield com/teamspeak/ts3client/data/c/w Lcom/teamspeak/ts3client/c/a;
getfield com/teamspeak/ts3client/c/a/d I
tableswitch 0
L26
L28
L30
default : L91
L26:
aload 5
getfield com/teamspeak/ts3client/data/t/b Landroid/widget/TextView;
ldc "event.client.recording.record"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 7
getfield com/teamspeak/ts3client/data/c/w Lcom/teamspeak/ts3client/c/a;
getfield com/teamspeak/ts3client/c/a/a Ljava/lang/String;
aastore
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
L27:
goto L4
L28:
aload 5
getfield com/teamspeak/ts3client/data/t/b Landroid/widget/TextView;
ldc "event.client.recording.record"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 7
getfield com/teamspeak/ts3client/data/c/w Lcom/teamspeak/ts3client/c/a;
getfield com/teamspeak/ts3client/c/a/a Ljava/lang/String;
aastore
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
L29:
goto L4
L30:
aload 5
getfield com/teamspeak/ts3client/data/t/b Landroid/widget/TextView;
ldc "event.client.recording.record"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 7
getfield com/teamspeak/ts3client/data/c/a Ljava/lang/String;
aastore
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
L31:
goto L4
L32:
aload 7
getfield com/teamspeak/ts3client/data/c/s Z
ifeq L34
aload 5
getfield com/teamspeak/ts3client/data/t/b Landroid/widget/TextView;
ldc "event.client.recording.record"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 7
getfield com/teamspeak/ts3client/data/c/a Ljava/lang/String;
aastore
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
L33:
goto L4
L34:
aload 5
getfield com/teamspeak/ts3client/data/t/b Landroid/widget/TextView;
aload 7
getfield com/teamspeak/ts3client/data/c/a Ljava/lang/String;
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
L35:
goto L4
L36:
aload 5
getfield com/teamspeak/ts3client/data/t/b Landroid/widget/TextView;
aload 0
getfield com/teamspeak/ts3client/data/g/k I
invokevirtual android/widget/TextView/setTextColor(I)V
L37:
goto L6
L38:
aload 5
getfield com/teamspeak/ts3client/data/t/b Landroid/widget/TextView;
ldc_w -65536
invokevirtual android/widget/TextView/setTextColor(I)V
L39:
goto L6
L40:
aload 5
getfield com/teamspeak/ts3client/data/t/b Landroid/widget/TextView;
ldc_w -16711936
invokevirtual android/widget/TextView/setTextColor(I)V
L41:
goto L6
L42:
aload 5
getfield com/teamspeak/ts3client/data/t/b Landroid/widget/TextView;
aconst_null
iconst_0
invokevirtual android/widget/TextView/setTypeface(Landroid/graphics/Typeface;I)V
L43:
goto L9
L44:
aload 7
getfield com/teamspeak/ts3client/data/c/j Z
ifeq L60
aload 5
getfield com/teamspeak/ts3client/data/t/a Landroid/widget/ImageView;
ldc_w 2130837658
ldc_w 16.0F
ldc_w 16.0F
invokestatic com/teamspeak/ts3client/data/d/t/a(IFF)Landroid/graphics/Bitmap;
invokevirtual android/widget/ImageView/setImageBitmap(Landroid/graphics/Bitmap;)V
aload 7
getfield com/teamspeak/ts3client/data/c/m Ljava/lang/String;
ldc ""
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L10
aload 7
getfield com/teamspeak/ts3client/data/c/w Lcom/teamspeak/ts3client/c/a;
ifnull L58
aload 7
getfield com/teamspeak/ts3client/data/c/w Lcom/teamspeak/ts3client/c/a;
getfield com/teamspeak/ts3client/c/a/j Z
ifeq L51
aload 7
getfield com/teamspeak/ts3client/data/c/w Lcom/teamspeak/ts3client/c/a;
getfield com/teamspeak/ts3client/c/a/d I
tableswitch 0
L45
L47
L49
default : L92
L45:
aload 5
getfield com/teamspeak/ts3client/data/t/b Landroid/widget/TextView;
new java/lang/StringBuilder
dup
ldc "["
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 7
getfield com/teamspeak/ts3client/data/c/w Lcom/teamspeak/ts3client/c/a;
getfield com/teamspeak/ts3client/c/a/a Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "] "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 7
getfield com/teamspeak/ts3client/data/c/a Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
L46:
goto L10
L47:
aload 5
getfield com/teamspeak/ts3client/data/t/b Landroid/widget/TextView;
aload 7
getfield com/teamspeak/ts3client/data/c/w Lcom/teamspeak/ts3client/c/a;
getfield com/teamspeak/ts3client/c/a/a Ljava/lang/String;
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
L48:
goto L10
L49:
aload 5
getfield com/teamspeak/ts3client/data/t/b Landroid/widget/TextView;
aload 7
getfield com/teamspeak/ts3client/data/c/a Ljava/lang/String;
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
L50:
goto L10
L51:
aload 7
getfield com/teamspeak/ts3client/data/c/w Lcom/teamspeak/ts3client/c/a;
getfield com/teamspeak/ts3client/c/a/d I
tableswitch 0
L52
L54
L56
default : L93
L52:
aload 5
getfield com/teamspeak/ts3client/data/t/b Landroid/widget/TextView;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 7
getfield com/teamspeak/ts3client/data/c/w Lcom/teamspeak/ts3client/c/a;
getfield com/teamspeak/ts3client/c/a/a Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " ["
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 7
getfield com/teamspeak/ts3client/data/c/m Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "]"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
L53:
goto L10
L54:
aload 5
getfield com/teamspeak/ts3client/data/t/b Landroid/widget/TextView;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 7
getfield com/teamspeak/ts3client/data/c/w Lcom/teamspeak/ts3client/c/a;
getfield com/teamspeak/ts3client/c/a/a Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " ["
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 7
getfield com/teamspeak/ts3client/data/c/m Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "]"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
L55:
goto L10
L56:
aload 5
getfield com/teamspeak/ts3client/data/t/b Landroid/widget/TextView;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 7
getfield com/teamspeak/ts3client/data/c/a Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " ["
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 7
getfield com/teamspeak/ts3client/data/c/m Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "]"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
L57:
goto L10
L58:
aload 5
getfield com/teamspeak/ts3client/data/t/b Landroid/widget/TextView;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 7
getfield com/teamspeak/ts3client/data/c/a Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " ["
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 7
getfield com/teamspeak/ts3client/data/c/m Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "]"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
L59:
goto L10
L60:
aload 7
getfield com/teamspeak/ts3client/data/c/i Z
ifeq L62
aload 5
getfield com/teamspeak/ts3client/data/t/a Landroid/widget/ImageView;
ldc_w 2130837620
ldc_w 16.0F
ldc_w 16.0F
invokestatic com/teamspeak/ts3client/data/d/t/a(IFF)Landroid/graphics/Bitmap;
invokevirtual android/widget/ImageView/setImageBitmap(Landroid/graphics/Bitmap;)V
L61:
goto L10
L62:
aload 7
getfield com/teamspeak/ts3client/data/c/g Z
ifeq L64
aload 5
getfield com/teamspeak/ts3client/data/t/a Landroid/widget/ImageView;
ldc_w 2130837655
ldc_w 16.0F
ldc_w 16.0F
invokestatic com/teamspeak/ts3client/data/d/t/a(IFF)Landroid/graphics/Bitmap;
invokevirtual android/widget/ImageView/setImageBitmap(Landroid/graphics/Bitmap;)V
L63:
goto L10
L64:
aload 7
getfield com/teamspeak/ts3client/data/c/h Z
ifeq L66
aload 5
getfield com/teamspeak/ts3client/data/t/a Landroid/widget/ImageView;
ldc_w 2130837619
ldc_w 16.0F
ldc_w 16.0F
invokestatic com/teamspeak/ts3client/data/d/t/a(IFF)Landroid/graphics/Bitmap;
invokevirtual android/widget/ImageView/setImageBitmap(Landroid/graphics/Bitmap;)V
L65:
goto L10
L66:
aload 7
getfield com/teamspeak/ts3client/data/c/f Z
ifeq L68
aload 5
getfield com/teamspeak/ts3client/data/t/a Landroid/widget/ImageView;
ldc_w 2130837634
ldc_w 16.0F
ldc_w 16.0F
invokestatic com/teamspeak/ts3client/data/d/t/a(IFF)Landroid/graphics/Bitmap;
invokevirtual android/widget/ImageView/setImageBitmap(Landroid/graphics/Bitmap;)V
L67:
goto L10
L68:
aload 7
getfield com/teamspeak/ts3client/data/c/n Z
ifne L70
aload 5
getfield com/teamspeak/ts3client/data/t/a Landroid/widget/ImageView;
ldc_w 2130837661
ldc_w 16.0F
ldc_w 16.0F
invokestatic com/teamspeak/ts3client/data/d/t/a(IFF)Landroid/graphics/Bitmap;
invokevirtual android/widget/ImageView/setImageBitmap(Landroid/graphics/Bitmap;)V
L69:
goto L10
L70:
aload 5
getfield com/teamspeak/ts3client/data/t/a Landroid/widget/ImageView;
ldc_w 2130837659
ldc_w 16.0F
ldc_w 16.0F
invokestatic com/teamspeak/ts3client/data/d/t/a(IFF)Landroid/graphics/Bitmap;
invokevirtual android/widget/ImageView/setImageBitmap(Landroid/graphics/Bitmap;)V
L71:
goto L10
L72:
aload 7
getfield com/teamspeak/ts3client/data/c/d I
iconst_1
if_icmpne L11
aload 7
getfield com/teamspeak/ts3client/data/c/n Z
ifne L74
aload 5
getfield com/teamspeak/ts3client/data/t/a Landroid/widget/ImageView;
ldc_w 2130837662
ldc_w 16.0F
ldc_w 16.0F
invokestatic com/teamspeak/ts3client/data/d/t/a(IFF)Landroid/graphics/Bitmap;
invokevirtual android/widget/ImageView/setImageBitmap(Landroid/graphics/Bitmap;)V
L73:
goto L11
L74:
aload 5
getfield com/teamspeak/ts3client/data/t/a Landroid/widget/ImageView;
ldc_w 2130837660
ldc_w 16.0F
ldc_w 16.0F
invokestatic com/teamspeak/ts3client/data/d/t/a(IFF)Landroid/graphics/Bitmap;
invokevirtual android/widget/ImageView/setImageBitmap(Landroid/graphics/Bitmap;)V
L75:
goto L11
L76:
new android/widget/ImageView
dup
aload 0
getfield com/teamspeak/ts3client/data/g/e Landroid/content/Context;
invokespecial android/widget/ImageView/<init>(Landroid/content/Context;)V
astore 8
aload 8
bipush 20
invokevirtual android/widget/ImageView/setMinimumHeight(I)V
aload 8
bipush 20
invokevirtual android/widget/ImageView/setMinimumWidth(I)V
aload 8
ldc_w 2130837634
ldc_w 16.0F
ldc_w 16.0F
invokestatic com/teamspeak/ts3client/data/d/t/a(IFF)Landroid/graphics/Bitmap;
invokevirtual android/widget/ImageView/setImageBitmap(Landroid/graphics/Bitmap;)V
aload 5
getfield com/teamspeak/ts3client/data/t/c Landroid/widget/LinearLayout;
aload 8
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
L77:
goto L13
L78:
aload 7
getfield com/teamspeak/ts3client/data/c/z J
lconst_0
lcmp
ifeq L79
new android/widget/ImageView
dup
aload 0
getfield com/teamspeak/ts3client/data/g/e Landroid/content/Context;
invokespecial android/widget/ImageView/<init>(Landroid/content/Context;)V
astore 8
aload 8
aload 0
getfield com/teamspeak/ts3client/data/g/f Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/r Lcom/teamspeak/ts3client/data/c/d;
aload 7
getfield com/teamspeak/ts3client/data/c/z J
invokevirtual com/teamspeak/ts3client/data/c/d/a(J)Lcom/teamspeak/ts3client/data/c/c;
invokevirtual com/teamspeak/ts3client/data/c/c/a()Landroid/graphics/drawable/Drawable;
invokevirtual android/widget/ImageView/setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
aload 8
bipush 20
invokevirtual android/widget/ImageView/setMinimumHeight(I)V
aload 8
bipush 20
invokevirtual android/widget/ImageView/setMinimumWidth(I)V
aload 5
getfield com/teamspeak/ts3client/data/t/c Landroid/widget/LinearLayout;
aload 8
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
L79:
aload 0
getfield com/teamspeak/ts3client/data/g/f Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
ldc "show_flags"
iconst_0
invokeinterface android/content/SharedPreferences/getBoolean(Ljava/lang/String;Z)Z 2
ifeq L82
aload 0
getfield com/teamspeak/ts3client/data/g/f Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/b()Lcom/teamspeak/ts3client/data/d/f;
aload 7
getfield com/teamspeak/ts3client/data/c/x Ljava/lang/String;
invokevirtual java/lang/String/toLowerCase()Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/data/d/f/a(Ljava/lang/String;)Landroid/graphics/Bitmap;
astore 8
L80:
aload 8
ifnull L82
L81:
new android/graphics/drawable/BitmapDrawable
dup
aload 0
getfield com/teamspeak/ts3client/data/g/f Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/getResources()Landroid/content/res/Resources;
aload 8
invokespecial android/graphics/drawable/BitmapDrawable/<init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V
astore 8
new android/widget/ImageView
dup
aload 0
getfield com/teamspeak/ts3client/data/g/e Landroid/content/Context;
invokespecial android/widget/ImageView/<init>(Landroid/content/Context;)V
astore 9
aload 9
bipush 20
invokevirtual android/widget/ImageView/setMinimumHeight(I)V
aload 9
bipush 20
invokevirtual android/widget/ImageView/setMinimumWidth(I)V
aload 9
aload 8
invokevirtual android/widget/ImageView/setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
aload 5
getfield com/teamspeak/ts3client/data/t/c Landroid/widget/LinearLayout;
aload 9
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
L82:
aload 0
getfield com/teamspeak/ts3client/data/g/f Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
ldc "longclick_client"
iconst_1
invokeinterface android/content/SharedPreferences/getBoolean(Ljava/lang/String;Z)Z 2
ifne L84
aload 4
new com/teamspeak/ts3client/data/h
dup
aload 0
aload 7
invokespecial com/teamspeak/ts3client/data/h/<init>(Lcom/teamspeak/ts3client/data/g;Lcom/teamspeak/ts3client/data/c;)V
invokevirtual android/view/View/setOnTouchListener(Landroid/view/View$OnTouchListener;)V
L83:
goto L94
L84:
aload 4
new com/teamspeak/ts3client/data/i
dup
aload 0
invokespecial com/teamspeak/ts3client/data/i/<init>(Lcom/teamspeak/ts3client/data/g;)V
invokevirtual android/view/View/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 4
new com/teamspeak/ts3client/data/j
dup
aload 0
aload 7
invokespecial com/teamspeak/ts3client/data/j/<init>(Lcom/teamspeak/ts3client/data/g;Lcom/teamspeak/ts3client/data/c;)V
invokevirtual android/view/View/setOnTouchListener(Landroid/view/View$OnTouchListener;)V
L85:
goto L94
L86:
goto L0
L87:
goto L4
L88:
goto L6
L90:
iload 1
iconst_1
iadd
istore 1
goto L89
L5:
astore 5
L94:
aload 4
areturn
L91:
goto L4
L92:
goto L10
L93:
goto L10
.limit locals 10
.limit stack 6
.end method

.method public final getChildrenCount(I)I
iload 1
ifne L0
iconst_0
ireturn
L0:
aload 0
getfield com/teamspeak/ts3client/data/g/a Ljava/util/List;
iload 1
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
ifnonnull L1
iconst_0
ireturn
L1:
aload 0
getfield com/teamspeak/ts3client/data/g/a Ljava/util/List;
iload 1
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast com/teamspeak/ts3client/data/a
getfield com/teamspeak/ts3client/data/a/p Z
ifne L2
aload 0
getfield com/teamspeak/ts3client/data/g/a Ljava/util/List;
iload 1
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast com/teamspeak/ts3client/data/a
getfield com/teamspeak/ts3client/data/a/o Z
ifne L3
L2:
iconst_0
ireturn
L3:
aload 0
getfield com/teamspeak/ts3client/data/g/a Ljava/util/List;
iload 1
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast com/teamspeak/ts3client/data/a
invokevirtual com/teamspeak/ts3client/data/a/k()Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final getGroup(I)Ljava/lang/Object;
aload 0
getfield com/teamspeak/ts3client/data/g/a Ljava/util/List;
iload 1
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
areturn
.limit locals 2
.limit stack 2
.end method

.method public final getGroupCount()I
aload 0
getfield com/teamspeak/ts3client/data/g/a Ljava/util/List;
ifnonnull L0
iconst_0
ireturn
L0:
aload 0
getfield com/teamspeak/ts3client/data/g/a Ljava/util/List;
invokeinterface java/util/List/size()I 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final getGroupId(I)J
iload 1
i2l
lreturn
.limit locals 2
.limit stack 2
.end method

.method public final getGroupView(IZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.catch java/lang/Exception from L0 to L1 using L2
aload 3
astore 5
aload 3
ifnonnull L3
aload 0
getfield com/teamspeak/ts3client/data/g/g Landroid/view/LayoutInflater;
ldc_w 2130903101
aload 4
iconst_0
invokevirtual android/view/LayoutInflater/inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;
astore 5
new com/teamspeak/ts3client/data/u
dup
invokespecial com/teamspeak/ts3client/data/u/<init>()V
astore 3
aload 3
aload 5
ldc_w 2131493247
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/LinearLayout
putfield com/teamspeak/ts3client/data/u/a Landroid/widget/LinearLayout;
aload 3
aload 5
ldc_w 2131493243
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/ImageView
putfield com/teamspeak/ts3client/data/u/b Landroid/widget/ImageView;
aload 3
aload 5
ldc_w 2131493246
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/TextView
putfield com/teamspeak/ts3client/data/u/c Landroid/widget/TextView;
aload 3
aload 5
ldc_w 2131493244
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/ImageView
putfield com/teamspeak/ts3client/data/u/d Landroid/widget/ImageView;
aload 5
aload 3
invokevirtual android/view/View/setTag(Ljava/lang/Object;)V
L3:
aload 0
getfield com/teamspeak/ts3client/data/g/a Ljava/util/List;
ifnonnull L4
L5:
aload 5
areturn
L4:
aload 0
getfield com/teamspeak/ts3client/data/g/a Ljava/util/List;
iload 1
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast com/teamspeak/ts3client/data/a
getfield com/teamspeak/ts3client/data/a/p Z
ifne L5
aload 5
invokevirtual android/view/View/getTag()Ljava/lang/Object;
checkcast com/teamspeak/ts3client/data/u
astore 3
aload 5
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/widget/AbsListView$LayoutParams
astore 4
aload 4
aload 0
getfield com/teamspeak/ts3client/data/g/h I
putfield android/widget/AbsListView$LayoutParams/height I
aload 5
aload 4
invokevirtual android/view/View/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 3
getfield com/teamspeak/ts3client/data/u/a Landroid/widget/LinearLayout;
invokevirtual android/widget/LinearLayout/removeAllViews()V
iload 1
aload 0
getfield com/teamspeak/ts3client/data/g/a Ljava/util/List;
invokeinterface java/util/List/size()I 0
iconst_1
isub
if_icmpne L6
aload 5
iconst_4
invokevirtual android/view/View/setVisibility(I)V
aload 5
areturn
L6:
aload 5
invokevirtual android/view/View/getVisibility()I
ifeq L7
aload 5
iconst_0
invokevirtual android/view/View/setVisibility(I)V
L7:
iload 1
ifne L8
aload 0
aload 5
aload 3
invokespecial com/teamspeak/ts3client/data/g/a(Landroid/view/View;Lcom/teamspeak/ts3client/data/u;)Landroid/view/View;
areturn
L8:
aload 5
aload 0
getfield com/teamspeak/ts3client/data/g/a Ljava/util/List;
iload 1
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast com/teamspeak/ts3client/data/a
getfield com/teamspeak/ts3client/data/a/f I
aload 0
ldc_w 18.0F
invokespecial com/teamspeak/ts3client/data/g/a(F)I
imul
iconst_0
iconst_0
iconst_0
invokevirtual android/view/View/setPadding(IIII)V
aload 0
getfield com/teamspeak/ts3client/data/g/a Ljava/util/List;
iload 1
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast com/teamspeak/ts3client/data/a
getfield com/teamspeak/ts3client/data/a/o Z
ifeq L9
aload 3
getfield com/teamspeak/ts3client/data/u/b Landroid/widget/ImageView;
iconst_0
invokevirtual android/widget/ImageView/setVisibility(I)V
aload 3
getfield com/teamspeak/ts3client/data/u/b Landroid/widget/ImageView;
ldc_w 2130837611
invokevirtual android/widget/ImageView/setBackgroundResource(I)V
L10:
aload 0
getfield com/teamspeak/ts3client/data/g/a Ljava/util/List;
iload 1
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast com/teamspeak/ts3client/data/a
getfield com/teamspeak/ts3client/data/a/q Z
ifeq L11
aload 3
getfield com/teamspeak/ts3client/data/u/b Landroid/widget/ImageView;
iconst_4
invokevirtual android/widget/ImageView/setVisibility(I)V
L12:
aload 0
getfield com/teamspeak/ts3client/data/g/l Ljava/util/regex/Pattern;
aload 0
getfield com/teamspeak/ts3client/data/g/a Ljava/util/List;
iload 1
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast com/teamspeak/ts3client/data/a
getfield com/teamspeak/ts3client/data/a/a Ljava/lang/String;
invokevirtual java/util/regex/Pattern/matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;
astore 4
aload 4
invokevirtual java/util/regex/Matcher/find()Z
ifeq L13
aload 0
getfield com/teamspeak/ts3client/data/g/a Ljava/util/List;
iload 1
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast com/teamspeak/ts3client/data/a
getfield com/teamspeak/ts3client/data/a/f I
ifeq L14
L13:
aload 3
getfield com/teamspeak/ts3client/data/u/d Landroid/widget/ImageView;
iconst_0
invokevirtual android/widget/ImageView/setVisibility(I)V
aload 0
getfield com/teamspeak/ts3client/data/g/a Ljava/util/List;
iload 1
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast com/teamspeak/ts3client/data/a
getfield com/teamspeak/ts3client/data/a/j Z
ifeq L15
aload 3
getfield com/teamspeak/ts3client/data/u/d Landroid/widget/ImageView;
ldc_w 2130837590
ldc_w 16.0F
ldc_w 16.0F
invokestatic com/teamspeak/ts3client/data/d/t/a(IFF)Landroid/graphics/Bitmap;
invokevirtual android/widget/ImageView/setImageBitmap(Landroid/graphics/Bitmap;)V
L16:
aload 3
getfield com/teamspeak/ts3client/data/u/c Landroid/widget/TextView;
aload 0
getfield com/teamspeak/ts3client/data/g/a Ljava/util/List;
iload 1
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast com/teamspeak/ts3client/data/a
getfield com/teamspeak/ts3client/data/a/a Ljava/lang/String;
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
aload 3
getfield com/teamspeak/ts3client/data/u/c Landroid/widget/TextView;
bipush 19
invokevirtual android/widget/TextView/setGravity(I)V
aload 0
getfield com/teamspeak/ts3client/data/g/a Ljava/util/List;
iload 1
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast com/teamspeak/ts3client/data/a
getfield com/teamspeak/ts3client/data/a/r Z
ifeq L17
new android/widget/ImageView
dup
aload 0
getfield com/teamspeak/ts3client/data/g/e Landroid/content/Context;
invokespecial android/widget/ImageView/<init>(Landroid/content/Context;)V
astore 4
aload 4
bipush 20
invokevirtual android/widget/ImageView/setMinimumHeight(I)V
aload 4
bipush 20
invokevirtual android/widget/ImageView/setMinimumWidth(I)V
aload 4
ldc_w 2130837587
ldc_w 16.0F
ldc_w 16.0F
invokestatic com/teamspeak/ts3client/data/d/t/a(IFF)Landroid/graphics/Bitmap;
invokevirtual android/widget/ImageView/setImageBitmap(Landroid/graphics/Bitmap;)V
aload 3
getfield com/teamspeak/ts3client/data/u/a Landroid/widget/LinearLayout;
aload 4
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
L17:
aload 0
getfield com/teamspeak/ts3client/data/g/a Ljava/util/List;
iload 1
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast com/teamspeak/ts3client/data/a
getfield com/teamspeak/ts3client/data/a/l I
iconst_3
if_icmpeq L18
aload 0
getfield com/teamspeak/ts3client/data/g/a Ljava/util/List;
iload 1
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast com/teamspeak/ts3client/data/a
getfield com/teamspeak/ts3client/data/a/l I
iconst_5
if_icmpne L19
L18:
new android/widget/ImageView
dup
aload 0
getfield com/teamspeak/ts3client/data/g/e Landroid/content/Context;
invokespecial android/widget/ImageView/<init>(Landroid/content/Context;)V
astore 4
aload 4
bipush 20
invokevirtual android/widget/ImageView/setMinimumHeight(I)V
aload 4
bipush 20
invokevirtual android/widget/ImageView/setMinimumWidth(I)V
aload 4
ldc_w 2130837653
ldc_w 16.0F
ldc_w 16.0F
invokestatic com/teamspeak/ts3client/data/d/t/a(IFF)Landroid/graphics/Bitmap;
invokevirtual android/widget/ImageView/setImageBitmap(Landroid/graphics/Bitmap;)V
aload 3
getfield com/teamspeak/ts3client/data/u/a Landroid/widget/LinearLayout;
aload 4
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
L19:
aload 0
getfield com/teamspeak/ts3client/data/g/a Ljava/util/List;
iload 1
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast com/teamspeak/ts3client/data/a
getfield com/teamspeak/ts3client/data/a/h I
ifeq L20
new android/widget/ImageView
dup
aload 0
getfield com/teamspeak/ts3client/data/g/e Landroid/content/Context;
invokespecial android/widget/ImageView/<init>(Landroid/content/Context;)V
astore 4
aload 4
bipush 20
invokevirtual android/widget/ImageView/setMinimumHeight(I)V
aload 4
bipush 20
invokevirtual android/widget/ImageView/setMinimumWidth(I)V
aload 4
ldc_w 2130837651
ldc_w 16.0F
ldc_w 16.0F
invokestatic com/teamspeak/ts3client/data/d/t/a(IFF)Landroid/graphics/Bitmap;
invokevirtual android/widget/ImageView/setImageBitmap(Landroid/graphics/Bitmap;)V
aload 3
getfield com/teamspeak/ts3client/data/u/a Landroid/widget/LinearLayout;
aload 4
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
L20:
aload 0
getfield com/teamspeak/ts3client/data/g/a Ljava/util/List;
iload 1
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast com/teamspeak/ts3client/data/a
getfield com/teamspeak/ts3client/data/a/m Z
ifeq L21
new android/widget/ImageView
dup
aload 0
getfield com/teamspeak/ts3client/data/g/e Landroid/content/Context;
invokespecial android/widget/ImageView/<init>(Landroid/content/Context;)V
astore 4
aload 4
bipush 20
invokevirtual android/widget/ImageView/setMinimumHeight(I)V
aload 4
bipush 20
invokevirtual android/widget/ImageView/setMinimumWidth(I)V
aload 4
ldc_w 2130837670
ldc_w 16.0F
ldc_w 16.0F
invokestatic com/teamspeak/ts3client/data/d/t/a(IFF)Landroid/graphics/Bitmap;
invokevirtual android/widget/ImageView/setImageBitmap(Landroid/graphics/Bitmap;)V
aload 3
getfield com/teamspeak/ts3client/data/u/a Landroid/widget/LinearLayout;
aload 4
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
L21:
aload 0
getfield com/teamspeak/ts3client/data/g/a Ljava/util/List;
iload 1
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast com/teamspeak/ts3client/data/a
getfield com/teamspeak/ts3client/data/a/s J
lconst_0
lcmp
ifeq L1
new android/widget/ImageView
dup
aload 0
getfield com/teamspeak/ts3client/data/g/e Landroid/content/Context;
invokespecial android/widget/ImageView/<init>(Landroid/content/Context;)V
astore 4
aload 4
bipush 20
invokevirtual android/widget/ImageView/setMinimumHeight(I)V
aload 4
bipush 20
invokevirtual android/widget/ImageView/setMinimumWidth(I)V
L0:
aload 4
aload 0
getfield com/teamspeak/ts3client/data/g/f Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/r Lcom/teamspeak/ts3client/data/c/d;
aload 0
getfield com/teamspeak/ts3client/data/g/a Ljava/util/List;
iload 1
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast com/teamspeak/ts3client/data/a
getfield com/teamspeak/ts3client/data/a/s J
invokevirtual com/teamspeak/ts3client/data/c/d/a(J)Lcom/teamspeak/ts3client/data/c/c;
invokevirtual com/teamspeak/ts3client/data/c/c/a()Landroid/graphics/drawable/Drawable;
invokevirtual android/widget/ImageView/setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
aload 3
getfield com/teamspeak/ts3client/data/u/a Landroid/widget/LinearLayout;
aload 4
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
L1:
aload 0
getfield com/teamspeak/ts3client/data/g/a Ljava/util/List;
iload 1
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast com/teamspeak/ts3client/data/a
getfield com/teamspeak/ts3client/data/a/o Z
ifeq L22
aload 0
getfield com/teamspeak/ts3client/data/g/b Lcom/teamspeak/ts3client/data/customExpandableListView;
iload 1
invokevirtual com/teamspeak/ts3client/data/customExpandableListView/expandGroup(I)Z
pop
L23:
aload 5
new com/teamspeak/ts3client/data/l
dup
aload 0
iload 1
invokespecial com/teamspeak/ts3client/data/l/<init>(Lcom/teamspeak/ts3client/data/g;I)V
invokevirtual android/view/View/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 5
new com/teamspeak/ts3client/data/m
dup
aload 0
iload 1
invokespecial com/teamspeak/ts3client/data/m/<init>(Lcom/teamspeak/ts3client/data/g;I)V
invokevirtual android/view/View/setOnTouchListener(Landroid/view/View$OnTouchListener;)V
aload 5
areturn
L9:
aload 3
getfield com/teamspeak/ts3client/data/u/b Landroid/widget/ImageView;
iconst_0
invokevirtual android/widget/ImageView/setVisibility(I)V
aload 3
getfield com/teamspeak/ts3client/data/u/b Landroid/widget/ImageView;
ldc_w 2130837612
invokevirtual android/widget/ImageView/setBackgroundResource(I)V
goto L10
L11:
aload 3
getfield com/teamspeak/ts3client/data/u/b Landroid/widget/ImageView;
iconst_0
invokevirtual android/widget/ImageView/setVisibility(I)V
goto L12
L15:
aload 0
getfield com/teamspeak/ts3client/data/g/a Ljava/util/List;
iload 1
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast com/teamspeak/ts3client/data/a
getfield com/teamspeak/ts3client/data/a/i I
iconst_m1
if_icmpeq L24
aload 0
getfield com/teamspeak/ts3client/data/g/a Ljava/util/List;
iload 1
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast com/teamspeak/ts3client/data/a
getfield com/teamspeak/ts3client/data/a/i I
aload 0
getfield com/teamspeak/ts3client/data/g/a Ljava/util/List;
iload 1
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast com/teamspeak/ts3client/data/a
invokevirtual com/teamspeak/ts3client/data/a/k()Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
if_icmpgt L24
aload 0
getfield com/teamspeak/ts3client/data/g/a Ljava/util/List;
iload 1
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast com/teamspeak/ts3client/data/a
getfield com/teamspeak/ts3client/data/a/k Z
ifeq L25
aload 3
getfield com/teamspeak/ts3client/data/u/d Landroid/widget/ImageView;
ldc_w 2130837592
ldc_w 16.0F
ldc_w 16.0F
invokestatic com/teamspeak/ts3client/data/d/t/a(IFF)Landroid/graphics/Bitmap;
invokevirtual android/widget/ImageView/setImageBitmap(Landroid/graphics/Bitmap;)V
goto L16
L25:
aload 3
getfield com/teamspeak/ts3client/data/u/d Landroid/widget/ImageView;
ldc_w 2130837591
ldc_w 16.0F
ldc_w 16.0F
invokestatic com/teamspeak/ts3client/data/d/t/a(IFF)Landroid/graphics/Bitmap;
invokevirtual android/widget/ImageView/setImageBitmap(Landroid/graphics/Bitmap;)V
goto L16
L24:
aload 0
getfield com/teamspeak/ts3client/data/g/a Ljava/util/List;
iload 1
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast com/teamspeak/ts3client/data/a
getfield com/teamspeak/ts3client/data/a/m Z
ifeq L26
aload 0
getfield com/teamspeak/ts3client/data/g/a Ljava/util/List;
iload 1
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast com/teamspeak/ts3client/data/a
getfield com/teamspeak/ts3client/data/a/n Z
ifne L26
aload 0
getfield com/teamspeak/ts3client/data/g/a Ljava/util/List;
iload 1
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast com/teamspeak/ts3client/data/a
getfield com/teamspeak/ts3client/data/a/k Z
ifeq L27
aload 3
getfield com/teamspeak/ts3client/data/u/d Landroid/widget/ImageView;
ldc_w 2130837594
ldc_w 16.0F
ldc_w 16.0F
invokestatic com/teamspeak/ts3client/data/d/t/a(IFF)Landroid/graphics/Bitmap;
invokevirtual android/widget/ImageView/setImageBitmap(Landroid/graphics/Bitmap;)V
goto L16
L27:
aload 3
getfield com/teamspeak/ts3client/data/u/d Landroid/widget/ImageView;
ldc_w 2130837593
ldc_w 16.0F
ldc_w 16.0F
invokestatic com/teamspeak/ts3client/data/d/t/a(IFF)Landroid/graphics/Bitmap;
invokevirtual android/widget/ImageView/setImageBitmap(Landroid/graphics/Bitmap;)V
goto L16
L26:
aload 0
getfield com/teamspeak/ts3client/data/g/a Ljava/util/List;
iload 1
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast com/teamspeak/ts3client/data/a
getfield com/teamspeak/ts3client/data/a/k Z
ifeq L28
aload 3
getfield com/teamspeak/ts3client/data/u/d Landroid/widget/ImageView;
ldc_w 2130837589
ldc_w 16.0F
ldc_w 16.0F
invokestatic com/teamspeak/ts3client/data/d/t/a(IFF)Landroid/graphics/Bitmap;
invokevirtual android/widget/ImageView/setImageBitmap(Landroid/graphics/Bitmap;)V
goto L16
L28:
aload 3
getfield com/teamspeak/ts3client/data/u/d Landroid/widget/ImageView;
ldc_w 2130837588
ldc_w 16.0F
ldc_w 16.0F
invokestatic com/teamspeak/ts3client/data/d/t/a(IFF)Landroid/graphics/Bitmap;
invokevirtual android/widget/ImageView/setImageBitmap(Landroid/graphics/Bitmap;)V
goto L16
L14:
aload 3
getfield com/teamspeak/ts3client/data/u/c Landroid/widget/TextView;
aload 4
iconst_2
invokevirtual java/util/regex/Matcher/group(I)Ljava/lang/String;
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
aload 4
iconst_1
invokevirtual java/util/regex/Matcher/group(I)Ljava/lang/String;
ifnull L29
aload 4
iconst_1
invokevirtual java/util/regex/Matcher/group(I)Ljava/lang/String;
ldc "r"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L30
aload 3
getfield com/teamspeak/ts3client/data/u/c Landroid/widget/TextView;
bipush 21
invokevirtual android/widget/TextView/setGravity(I)V
L29:
aload 4
iconst_2
invokevirtual java/util/regex/Matcher/group(I)Ljava/lang/String;
ldc "---"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L31
aload 4
iconst_2
invokevirtual java/util/regex/Matcher/group(I)Ljava/lang/String;
ldc "..."
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L31
aload 4
iconst_2
invokevirtual java/util/regex/Matcher/group(I)Ljava/lang/String;
ldc "-.-"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L31
aload 4
iconst_2
invokevirtual java/util/regex/Matcher/group(I)Ljava/lang/String;
ldc "___"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L31
aload 4
iconst_2
invokevirtual java/util/regex/Matcher/group(I)Ljava/lang/String;
ldc "-.."
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L32
L31:
aload 4
iconst_2
invokevirtual java/util/regex/Matcher/group(I)Ljava/lang/String;
ldc "-.-"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L33
aload 3
getfield com/teamspeak/ts3client/data/u/c Landroid/widget/TextView;
ldc "-."
invokestatic com/teamspeak/ts3client/data/g/a(Landroid/widget/TextView;Ljava/lang/String;)V
L34:
aload 3
getfield com/teamspeak/ts3client/data/u/c Landroid/widget/TextView;
bipush 19
invokevirtual android/widget/TextView/setGravity(I)V
L32:
aload 3
getfield com/teamspeak/ts3client/data/u/d Landroid/widget/ImageView;
bipush 8
invokevirtual android/widget/ImageView/setVisibility(I)V
goto L1
L30:
aload 4
iconst_1
invokevirtual java/util/regex/Matcher/group(I)Ljava/lang/String;
ldc "c"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L35
aload 3
getfield com/teamspeak/ts3client/data/u/c Landroid/widget/TextView;
bipush 17
invokevirtual android/widget/TextView/setGravity(I)V
goto L29
L35:
aload 4
iconst_1
invokevirtual java/util/regex/Matcher/group(I)Ljava/lang/String;
ldc "*"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L36
aload 3
getfield com/teamspeak/ts3client/data/u/c Landroid/widget/TextView;
aload 4
iconst_2
invokevirtual java/util/regex/Matcher/group(I)Ljava/lang/String;
invokestatic com/teamspeak/ts3client/data/g/a(Landroid/widget/TextView;Ljava/lang/String;)V
L36:
aload 3
getfield com/teamspeak/ts3client/data/u/c Landroid/widget/TextView;
bipush 19
invokevirtual android/widget/TextView/setGravity(I)V
goto L29
L33:
aload 4
iconst_2
invokevirtual java/util/regex/Matcher/group(I)Ljava/lang/String;
ldc "___"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L37
aload 3
getfield com/teamspeak/ts3client/data/u/c Landroid/widget/TextView;
ldc "\u2500"
invokestatic com/teamspeak/ts3client/data/g/a(Landroid/widget/TextView;Ljava/lang/String;)V
goto L34
L37:
aload 3
getfield com/teamspeak/ts3client/data/u/c Landroid/widget/TextView;
aload 4
iconst_2
invokevirtual java/util/regex/Matcher/group(I)Ljava/lang/String;
invokestatic com/teamspeak/ts3client/data/g/a(Landroid/widget/TextView;Ljava/lang/String;)V
goto L34
L22:
aload 0
getfield com/teamspeak/ts3client/data/g/b Lcom/teamspeak/ts3client/data/customExpandableListView;
iload 1
invokevirtual com/teamspeak/ts3client/data/customExpandableListView/collapseGroup(I)Z
pop
goto L23
L2:
astore 3
goto L1
.limit locals 6
.limit stack 5
.end method

.method public final hasStableIds()Z
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final isChildSelectable(II)Z
iconst_0
ireturn
.limit locals 3
.limit stack 1
.end method
