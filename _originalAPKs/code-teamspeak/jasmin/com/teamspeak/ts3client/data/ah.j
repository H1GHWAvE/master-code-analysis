.bytecode 50.0
.class public final synchronized com/teamspeak/ts3client/data/ah
.super android/os/AsyncTask

.field final synthetic 'a' Lcom/teamspeak/ts3client/data/af;

.method private <init>(Lcom/teamspeak/ts3client/data/af;)V
aload 0
aload 1
putfield com/teamspeak/ts3client/data/ah/a Lcom/teamspeak/ts3client/data/af;
aload 0
invokespecial android/os/AsyncTask/<init>()V
return
.limit locals 2
.limit stack 2
.end method

.method public synthetic <init>(Lcom/teamspeak/ts3client/data/af;B)V
aload 0
aload 1
invokespecial com/teamspeak/ts3client/data/ah/<init>(Lcom/teamspeak/ts3client/data/af;)V
return
.limit locals 3
.limit stack 2
.end method

.method private static transient a([Ljava/lang/String;)Ljava/nio/ByteBuffer;
.catch java/io/IOException from L0 to L1 using L2
.catch java/io/IOException from L1 to L3 using L2
.catch java/io/IOException from L4 to L5 using L2
.catch java/io/IOException from L6 to L7 using L2
L0:
new java/net/URL
dup
aload 0
iconst_0
aaload
invokespecial java/net/URL/<init>(Ljava/lang/String;)V
invokevirtual java/net/URL/openConnection()Ljava/net/URLConnection;
checkcast java/net/HttpURLConnection
astore 0
aload 0
sipush 2500
invokevirtual java/net/HttpURLConnection/setReadTimeout(I)V
aload 0
invokevirtual java/net/HttpURLConnection/getInputStream()Ljava/io/InputStream;
astore 3
new java/io/ByteArrayOutputStream
dup
invokespecial java/io/ByteArrayOutputStream/<init>()V
astore 2
sipush 1024
newarray byte
astore 4
L1:
aload 3
aload 4
iconst_0
sipush 1024
invokevirtual java/io/InputStream/read([BII)I
istore 1
L3:
iload 1
iconst_m1
if_icmpeq L6
L4:
aload 2
aload 4
iconst_0
iload 1
invokevirtual java/io/ByteArrayOutputStream/write([BII)V
L5:
goto L1
L6:
aload 0
invokevirtual java/net/HttpURLConnection/getContentLength()I
invokestatic java/nio/ByteBuffer/allocateDirect(I)Ljava/nio/ByteBuffer;
astore 3
aload 3
aload 2
invokevirtual java/io/ByteArrayOutputStream/toByteArray()[B
iconst_0
aload 0
invokevirtual java/net/HttpURLConnection/getContentLength()I
invokevirtual java/nio/ByteBuffer/put([BII)Ljava/nio/ByteBuffer;
pop
aload 0
invokevirtual java/net/HttpURLConnection/disconnect()V
L7:
aload 3
areturn
L2:
astore 0
aconst_null
areturn
.limit locals 5
.limit stack 4
.end method

.method private a(Ljava/nio/ByteBuffer;)V
aload 1
ifnull L0
aload 0
getfield com/teamspeak/ts3client/data/ah/a Lcom/teamspeak/ts3client/data/af;
invokestatic com/teamspeak/ts3client/jni/Ts3Jni/b()Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 1
aload 1
invokevirtual java/nio/ByteBuffer/limit()I
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_android_parseProtobufDataResult(Ljava/nio/ByteBuffer;I)Z
putfield com/teamspeak/ts3client/data/af/c Z
aload 0
getfield com/teamspeak/ts3client/data/ah/a Lcom/teamspeak/ts3client/data/af;
getfield com/teamspeak/ts3client/data/af/c Z
ifeq L1
aload 0
getfield com/teamspeak/ts3client/data/ah/a Lcom/teamspeak/ts3client/data/af;
invokestatic com/teamspeak/ts3client/jni/Ts3Jni/b()Lcom/teamspeak/ts3client/jni/Ts3Jni;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_android_getLicenseAgreementVersion()I
putfield com/teamspeak/ts3client/data/af/a I
aload 0
getfield com/teamspeak/ts3client/data/ah/a Lcom/teamspeak/ts3client/data/af;
getfield com/teamspeak/ts3client/data/af/d Lcom/teamspeak/ts3client/data/z;
ifnull L2
invokestatic java/util/Calendar/getInstance()Ljava/util/Calendar;
astore 1
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
invokeinterface android/content/SharedPreferences/edit()Landroid/content/SharedPreferences$Editor; 0
ldc "la_lastcheck"
aload 1
invokevirtual java/util/Calendar/getTimeInMillis()J
invokeinterface android/content/SharedPreferences$Editor/putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor; 3
invokeinterface android/content/SharedPreferences$Editor/commit()Z 0
pop
aload 0
getfield com/teamspeak/ts3client/data/ah/a Lcom/teamspeak/ts3client/data/af;
getfield com/teamspeak/ts3client/data/af/d Lcom/teamspeak/ts3client/data/z;
invokeinterface com/teamspeak/ts3client/data/z/m()V 0
ldc "UpdateServerData"
ldc "Downloaded new update info"
invokestatic android/util/Log/i(Ljava/lang/String;Ljava/lang/String;)I
pop
L2:
aload 0
getfield com/teamspeak/ts3client/data/ah/a Lcom/teamspeak/ts3client/data/af;
iconst_0
putfield com/teamspeak/ts3client/data/af/e Z
return
L1:
ldc "UpdateServerData"
ldc "Downloaded corrupt data, skipping..."
invokestatic android/util/Log/i(Ljava/lang/String;Ljava/lang/String;)I
pop
aload 0
getfield com/teamspeak/ts3client/data/ah/a Lcom/teamspeak/ts3client/data/af;
iconst_0
putfield com/teamspeak/ts3client/data/af/c Z
aload 0
getfield com/teamspeak/ts3client/data/ah/a Lcom/teamspeak/ts3client/data/af;
getfield com/teamspeak/ts3client/data/af/d Lcom/teamspeak/ts3client/data/z;
invokeinterface com/teamspeak/ts3client/data/z/o()V 0
goto L2
L0:
ldc "UpdateServerData"
ldc "Failed to download new update info"
invokestatic android/util/Log/i(Ljava/lang/String;Ljava/lang/String;)I
pop
aload 0
getfield com/teamspeak/ts3client/data/ah/a Lcom/teamspeak/ts3client/data/af;
iconst_0
putfield com/teamspeak/ts3client/data/af/c Z
aload 0
getfield com/teamspeak/ts3client/data/ah/a Lcom/teamspeak/ts3client/data/af;
getfield com/teamspeak/ts3client/data/af/d Lcom/teamspeak/ts3client/data/z;
invokeinterface com/teamspeak/ts3client/data/z/o()V 0
goto L2
.limit locals 2
.limit stack 4
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
aload 1
checkcast [Ljava/lang/String;
invokestatic com/teamspeak/ts3client/data/ah/a([Ljava/lang/String;)Ljava/nio/ByteBuffer;
areturn
.limit locals 2
.limit stack 1
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
aload 1
checkcast java/nio/ByteBuffer
astore 1
aload 1
ifnull L0
aload 0
getfield com/teamspeak/ts3client/data/ah/a Lcom/teamspeak/ts3client/data/af;
invokestatic com/teamspeak/ts3client/jni/Ts3Jni/b()Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 1
aload 1
invokevirtual java/nio/ByteBuffer/limit()I
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_android_parseProtobufDataResult(Ljava/nio/ByteBuffer;I)Z
putfield com/teamspeak/ts3client/data/af/c Z
aload 0
getfield com/teamspeak/ts3client/data/ah/a Lcom/teamspeak/ts3client/data/af;
getfield com/teamspeak/ts3client/data/af/c Z
ifeq L1
aload 0
getfield com/teamspeak/ts3client/data/ah/a Lcom/teamspeak/ts3client/data/af;
invokestatic com/teamspeak/ts3client/jni/Ts3Jni/b()Lcom/teamspeak/ts3client/jni/Ts3Jni;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_android_getLicenseAgreementVersion()I
putfield com/teamspeak/ts3client/data/af/a I
aload 0
getfield com/teamspeak/ts3client/data/ah/a Lcom/teamspeak/ts3client/data/af;
getfield com/teamspeak/ts3client/data/af/d Lcom/teamspeak/ts3client/data/z;
ifnull L2
invokestatic java/util/Calendar/getInstance()Ljava/util/Calendar;
astore 1
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
invokeinterface android/content/SharedPreferences/edit()Landroid/content/SharedPreferences$Editor; 0
ldc "la_lastcheck"
aload 1
invokevirtual java/util/Calendar/getTimeInMillis()J
invokeinterface android/content/SharedPreferences$Editor/putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor; 3
invokeinterface android/content/SharedPreferences$Editor/commit()Z 0
pop
aload 0
getfield com/teamspeak/ts3client/data/ah/a Lcom/teamspeak/ts3client/data/af;
getfield com/teamspeak/ts3client/data/af/d Lcom/teamspeak/ts3client/data/z;
invokeinterface com/teamspeak/ts3client/data/z/m()V 0
ldc "UpdateServerData"
ldc "Downloaded new update info"
invokestatic android/util/Log/i(Ljava/lang/String;Ljava/lang/String;)I
pop
L2:
aload 0
getfield com/teamspeak/ts3client/data/ah/a Lcom/teamspeak/ts3client/data/af;
iconst_0
putfield com/teamspeak/ts3client/data/af/e Z
return
L1:
ldc "UpdateServerData"
ldc "Downloaded corrupt data, skipping..."
invokestatic android/util/Log/i(Ljava/lang/String;Ljava/lang/String;)I
pop
aload 0
getfield com/teamspeak/ts3client/data/ah/a Lcom/teamspeak/ts3client/data/af;
iconst_0
putfield com/teamspeak/ts3client/data/af/c Z
aload 0
getfield com/teamspeak/ts3client/data/ah/a Lcom/teamspeak/ts3client/data/af;
getfield com/teamspeak/ts3client/data/af/d Lcom/teamspeak/ts3client/data/z;
invokeinterface com/teamspeak/ts3client/data/z/o()V 0
goto L2
L0:
ldc "UpdateServerData"
ldc "Failed to download new update info"
invokestatic android/util/Log/i(Ljava/lang/String;Ljava/lang/String;)I
pop
aload 0
getfield com/teamspeak/ts3client/data/ah/a Lcom/teamspeak/ts3client/data/af;
iconst_0
putfield com/teamspeak/ts3client/data/af/c Z
aload 0
getfield com/teamspeak/ts3client/data/ah/a Lcom/teamspeak/ts3client/data/af;
getfield com/teamspeak/ts3client/data/af/d Lcom/teamspeak/ts3client/data/z;
invokeinterface com/teamspeak/ts3client/data/z/o()V 0
goto L2
.limit locals 2
.limit stack 4
.end method
