.bytecode 50.0
.class public final synchronized com/teamspeak/ts3client/data/b/a
.super java/lang/Object

.field public static 'a' Lcom/teamspeak/ts3client/data/b/a;

.field private static final 'b' Ljava/lang/String; = "create table server (server_id integer primary key autoincrement, label text not null, address text not null, serverpassword blob null, nickname text not null, defaultchannel text not null, defaultchannelpassword text not null, ident int not null, subscribeall text not null,subscriptionlist int not null);"

.field private static final 'c' Ljava/lang/String; = "server"

.field private static final 'd' Ljava/lang/String; = "Teamspeak-Bookmark"

.field private static final 'e' I = 5


.field private 'f' Landroid/database/sqlite/SQLiteDatabase;

.method public <init>(Landroid/content/Context;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
new com/teamspeak/ts3client/data/b/b
dup
aload 0
aload 1
invokespecial com/teamspeak/ts3client/data/b/b/<init>(Lcom/teamspeak/ts3client/data/b/a;Landroid/content/Context;)V
invokevirtual com/teamspeak/ts3client/data/b/b/getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;
putfield com/teamspeak/ts3client/data/b/a/f Landroid/database/sqlite/SQLiteDatabase;
aload 0
putstatic com/teamspeak/ts3client/data/b/a/a Lcom/teamspeak/ts3client/data/b/a;
return
.limit locals 2
.limit stack 5
.end method

.method private static a([B)Ljava/lang/String;
.catch java/security/NoSuchAlgorithmException from L0 to L1 using L2
.catch javax/crypto/NoSuchPaddingException from L0 to L1 using L3
.catch javax/crypto/IllegalBlockSizeException from L0 to L1 using L4
.catch javax/crypto/BadPaddingException from L0 to L1 using L5
.catch java/security/InvalidKeyException from L0 to L1 using L6
.catch java/security/InvalidAlgorithmParameterException from L0 to L1 using L7
aload 0
ifnonnull L8
ldc ""
areturn
L8:
new javax/crypto/spec/IvParameterSpec
dup
ldc "mnd783bnsd03bs72"
invokevirtual java/lang/String/getBytes()[B
invokespecial javax/crypto/spec/IvParameterSpec/<init>([B)V
astore 1
new javax/crypto/spec/SecretKeySpec
dup
ldc "abd732ns0Sg37s3S"
invokevirtual java/lang/String/getBytes()[B
ldc "AES"
invokespecial javax/crypto/spec/SecretKeySpec/<init>([BLjava/lang/String;)V
astore 2
L0:
ldc "AES/CBC/PKCS5Padding"
invokestatic javax/crypto/Cipher/getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;
astore 3
aload 3
iconst_2
aload 2
aload 1
invokevirtual javax/crypto/Cipher/init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V
new java/lang/String
dup
aload 3
aload 0
invokevirtual javax/crypto/Cipher/doFinal([B)[B
invokespecial java/lang/String/<init>([B)V
astore 0
L1:
aload 0
areturn
L2:
astore 0
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/d Ljava/util/logging/Logger;
getstatic java/util/logging/Level/SEVERE Ljava/util/logging/Level;
aload 0
invokevirtual java/security/NoSuchAlgorithmException/getMessage()Ljava/lang/String;
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;)V
L9:
ldc ""
areturn
L3:
astore 0
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/d Ljava/util/logging/Logger;
getstatic java/util/logging/Level/SEVERE Ljava/util/logging/Level;
aload 0
invokevirtual javax/crypto/NoSuchPaddingException/getMessage()Ljava/lang/String;
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;)V
goto L9
L4:
astore 0
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/d Ljava/util/logging/Logger;
getstatic java/util/logging/Level/SEVERE Ljava/util/logging/Level;
aload 0
invokevirtual javax/crypto/IllegalBlockSizeException/getMessage()Ljava/lang/String;
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;)V
goto L9
L5:
astore 0
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/d Ljava/util/logging/Logger;
getstatic java/util/logging/Level/SEVERE Ljava/util/logging/Level;
aload 0
invokevirtual javax/crypto/BadPaddingException/getMessage()Ljava/lang/String;
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;)V
goto L9
L6:
astore 0
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/d Ljava/util/logging/Logger;
getstatic java/util/logging/Level/SEVERE Ljava/util/logging/Level;
aload 0
invokevirtual java/security/InvalidKeyException/getMessage()Ljava/lang/String;
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;)V
goto L9
L7:
astore 0
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/d Ljava/util/logging/Logger;
getstatic java/util/logging/Level/SEVERE Ljava/util/logging/Level;
aload 0
invokevirtual java/security/InvalidAlgorithmParameterException/getMessage()Ljava/lang/String;
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;)V
goto L9
.limit locals 4
.limit stack 4
.end method

.method static a(Ljava/lang/String;)[B
.catch java/security/NoSuchAlgorithmException from L0 to L1 using L2
.catch javax/crypto/NoSuchPaddingException from L0 to L1 using L3
.catch java/security/InvalidKeyException from L0 to L1 using L4
.catch javax/crypto/IllegalBlockSizeException from L0 to L1 using L5
.catch javax/crypto/BadPaddingException from L0 to L1 using L6
.catch java/security/InvalidAlgorithmParameterException from L0 to L1 using L7
aload 0
ldc ""
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L8
aconst_null
areturn
L8:
new javax/crypto/spec/IvParameterSpec
dup
ldc "mnd783bnsd03bs72"
invokevirtual java/lang/String/getBytes()[B
invokespecial javax/crypto/spec/IvParameterSpec/<init>([B)V
astore 1
new javax/crypto/spec/SecretKeySpec
dup
ldc "abd732ns0Sg37s3S"
invokevirtual java/lang/String/getBytes()[B
ldc "AES"
invokespecial javax/crypto/spec/SecretKeySpec/<init>([BLjava/lang/String;)V
astore 2
L0:
ldc "AES/CBC/PKCS5Padding"
invokestatic javax/crypto/Cipher/getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;
astore 3
aload 3
iconst_1
aload 2
aload 1
invokevirtual javax/crypto/Cipher/init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V
aload 3
aload 0
invokevirtual java/lang/String/getBytes()[B
invokevirtual javax/crypto/Cipher/doFinal([B)[B
astore 0
L1:
aload 0
areturn
L2:
astore 0
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/d Ljava/util/logging/Logger;
getstatic java/util/logging/Level/SEVERE Ljava/util/logging/Level;
aload 0
invokevirtual java/security/NoSuchAlgorithmException/getMessage()Ljava/lang/String;
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;)V
aconst_null
areturn
L3:
astore 0
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/d Ljava/util/logging/Logger;
getstatic java/util/logging/Level/SEVERE Ljava/util/logging/Level;
aload 0
invokevirtual javax/crypto/NoSuchPaddingException/getMessage()Ljava/lang/String;
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;)V
aconst_null
areturn
L4:
astore 0
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/d Ljava/util/logging/Logger;
getstatic java/util/logging/Level/SEVERE Ljava/util/logging/Level;
aload 0
invokevirtual java/security/InvalidKeyException/getMessage()Ljava/lang/String;
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;)V
aconst_null
areturn
L5:
astore 0
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/d Ljava/util/logging/Logger;
getstatic java/util/logging/Level/SEVERE Ljava/util/logging/Level;
aload 0
invokevirtual javax/crypto/IllegalBlockSizeException/getMessage()Ljava/lang/String;
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;)V
aconst_null
areturn
L6:
astore 0
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/d Ljava/util/logging/Logger;
getstatic java/util/logging/Level/SEVERE Ljava/util/logging/Level;
aload 0
invokevirtual javax/crypto/BadPaddingException/getMessage()Ljava/lang/String;
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;)V
aconst_null
areturn
L7:
astore 0
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/d Ljava/util/logging/Logger;
getstatic java/util/logging/Level/SEVERE Ljava/util/logging/Level;
aload 0
invokevirtual java/security/InvalidAlgorithmParameterException/getMessage()Ljava/lang/String;
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;)V
aconst_null
areturn
.limit locals 4
.limit stack 4
.end method

.method private b(J)Lcom/teamspeak/ts3client/data/ab;
.catch android/database/SQLException from L0 to L1 using L2
.catch android/database/SQLException from L3 to L4 using L2
.catch android/database/SQLException from L4 to L5 using L2
new com/teamspeak/ts3client/data/ab
dup
invokespecial com/teamspeak/ts3client/data/ab/<init>()V
astore 4
L0:
aload 0
getfield com/teamspeak/ts3client/data/b/a/f Landroid/database/sqlite/SQLiteDatabase;
astore 5
new java/lang/StringBuilder
dup
ldc "server_id="
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
lload 1
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 6
aload 5
ldc "server"
bipush 10
anewarray java/lang/String
dup
iconst_0
ldc "server_id"
aastore
dup
iconst_1
ldc "label"
aastore
dup
iconst_2
ldc "address"
aastore
dup
iconst_3
ldc "serverpassword"
aastore
dup
iconst_4
ldc "nickname"
aastore
dup
iconst_5
ldc "defaultchannel"
aastore
dup
bipush 6
ldc "defaultchannelpassword"
aastore
dup
bipush 7
ldc "ident"
aastore
dup
bipush 8
ldc "subscribeall"
aastore
dup
bipush 9
ldc "subscriptionlist"
aastore
aload 6
aconst_null
aconst_null
aconst_null
aconst_null
aconst_null
invokevirtual android/database/sqlite/SQLiteDatabase/query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
astore 5
aload 5
invokeinterface android/database/Cursor/moveToFirst()Z 0
ifeq L4
aload 4
aload 5
aload 5
ldc "label"
invokeinterface android/database/Cursor/getColumnIndex(Ljava/lang/String;)I 1
invokeinterface android/database/Cursor/getString(I)Ljava/lang/String; 1
putfield com/teamspeak/ts3client/data/ab/a Ljava/lang/String;
aload 4
aload 5
aload 5
ldc "address"
invokeinterface android/database/Cursor/getColumnIndex(Ljava/lang/String;)I 1
invokeinterface android/database/Cursor/getString(I)Ljava/lang/String; 1
ldc "(.*://)+"
ldc ""
invokevirtual java/lang/String/replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/data/ab/a(Ljava/lang/String;)V
aload 4
new java/lang/String
dup
aload 5
aload 5
ldc "serverpassword"
invokeinterface android/database/Cursor/getColumnIndex(Ljava/lang/String;)I 1
invokeinterface android/database/Cursor/getBlob(I)[B 1
invokestatic com/teamspeak/ts3client/data/b/a/a([B)Ljava/lang/String;
invokespecial java/lang/String/<init>(Ljava/lang/String;)V
putfield com/teamspeak/ts3client/data/ab/e Ljava/lang/String;
aload 4
aload 5
aload 5
ldc "nickname"
invokeinterface android/database/Cursor/getColumnIndex(Ljava/lang/String;)I 1
invokeinterface android/database/Cursor/getString(I)Ljava/lang/String; 1
putfield com/teamspeak/ts3client/data/ab/f Ljava/lang/String;
aload 4
aload 5
aload 5
ldc "defaultchannel"
invokeinterface android/database/Cursor/getColumnIndex(Ljava/lang/String;)I 1
invokeinterface android/database/Cursor/getString(I)Ljava/lang/String; 1
putfield com/teamspeak/ts3client/data/ab/g Ljava/lang/String;
aload 4
aload 5
aload 5
ldc "defaultchannelpassword"
invokeinterface android/database/Cursor/getColumnIndex(Ljava/lang/String;)I 1
invokeinterface android/database/Cursor/getString(I)Ljava/lang/String; 1
putfield com/teamspeak/ts3client/data/ab/h Ljava/lang/String;
aload 4
aload 5
aload 5
ldc "server_id"
invokeinterface android/database/Cursor/getColumnIndex(Ljava/lang/String;)I 1
invokeinterface android/database/Cursor/getLong(I)J 1
putfield com/teamspeak/ts3client/data/ab/l J
aload 4
aload 5
aload 5
ldc "ident"
invokeinterface android/database/Cursor/getColumnIndex(Ljava/lang/String;)I 1
invokeinterface android/database/Cursor/getInt(I)I 1
putfield com/teamspeak/ts3client/data/ab/d I
aload 5
aload 5
ldc "subscribeall"
invokeinterface android/database/Cursor/getColumnIndex(Ljava/lang/String;)I 1
invokeinterface android/database/Cursor/getInt(I)I 1
ifne L6
L1:
iconst_0
istore 3
L3:
aload 4
iload 3
putfield com/teamspeak/ts3client/data/ab/j Z
aload 4
aload 5
aload 5
ldc "subscriptionlist"
invokeinterface android/database/Cursor/getColumnIndex(Ljava/lang/String;)I 1
invokeinterface android/database/Cursor/getString(I)Ljava/lang/String; 1
invokevirtual com/teamspeak/ts3client/data/ab/b(Ljava/lang/String;)V
L4:
aload 5
invokeinterface android/database/Cursor/close()V 0
L5:
aload 4
areturn
L6:
iconst_1
istore 3
goto L3
L2:
astore 4
aconst_null
areturn
.limit locals 7
.limit stack 9
.end method

.method private static b()Lcom/teamspeak/ts3client/data/b/a;
getstatic com/teamspeak/ts3client/data/b/a/a Lcom/teamspeak/ts3client/data/b/a;
areturn
.limit locals 0
.limit stack 1
.end method

.method private static synthetic b(Ljava/lang/String;)[B
aload 0
invokestatic com/teamspeak/ts3client/data/b/a/a(Ljava/lang/String;)[B
areturn
.limit locals 1
.limit stack 1
.end method

.method private c()V
aload 0
getfield com/teamspeak/ts3client/data/b/a/f Landroid/database/sqlite/SQLiteDatabase;
invokevirtual android/database/sqlite/SQLiteDatabase/close()V
return
.limit locals 1
.limit stack 1
.end method

.method public final a(Lcom/teamspeak/ts3client/data/ab;)J
.catch java/lang/Exception from L0 to L1 using L2
new android/content/ContentValues
dup
invokespecial android/content/ContentValues/<init>()V
astore 5
aload 5
ldc "label"
aload 1
getfield com/teamspeak/ts3client/data/ab/a Ljava/lang/String;
invokevirtual android/content/ContentValues/put(Ljava/lang/String;Ljava/lang/String;)V
aload 5
ldc "address"
aload 1
getfield com/teamspeak/ts3client/data/ab/c Ljava/lang/String;
ldc "(.*://)+"
ldc ""
invokevirtual java/lang/String/replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/content/ContentValues/put(Ljava/lang/String;Ljava/lang/String;)V
aload 5
ldc "serverpassword"
aload 1
getfield com/teamspeak/ts3client/data/ab/e Ljava/lang/String;
invokestatic com/teamspeak/ts3client/data/b/a/a(Ljava/lang/String;)[B
invokevirtual android/content/ContentValues/put(Ljava/lang/String;[B)V
aload 5
ldc "nickname"
aload 1
getfield com/teamspeak/ts3client/data/ab/f Ljava/lang/String;
invokevirtual android/content/ContentValues/put(Ljava/lang/String;Ljava/lang/String;)V
aload 5
ldc "defaultchannel"
aload 1
getfield com/teamspeak/ts3client/data/ab/g Ljava/lang/String;
invokevirtual android/content/ContentValues/put(Ljava/lang/String;Ljava/lang/String;)V
aload 5
ldc "defaultchannelpassword"
aload 1
getfield com/teamspeak/ts3client/data/ab/h Ljava/lang/String;
invokevirtual android/content/ContentValues/put(Ljava/lang/String;Ljava/lang/String;)V
aload 5
ldc "ident"
aload 1
getfield com/teamspeak/ts3client/data/ab/d I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual android/content/ContentValues/put(Ljava/lang/String;Ljava/lang/Integer;)V
aload 1
getfield com/teamspeak/ts3client/data/ab/j Z
ifeq L3
iconst_1
istore 2
L4:
aload 5
ldc "subscribeall"
iload 2
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual android/content/ContentValues/put(Ljava/lang/String;Ljava/lang/Integer;)V
aload 5
ldc "subscriptionlist"
aload 1
invokevirtual com/teamspeak/ts3client/data/ab/h()Ljava/lang/String;
invokevirtual android/content/ContentValues/put(Ljava/lang/String;Ljava/lang/String;)V
L0:
aload 0
getfield com/teamspeak/ts3client/data/b/a/f Landroid/database/sqlite/SQLiteDatabase;
ldc "server"
aconst_null
aload 5
invokevirtual android/database/sqlite/SQLiteDatabase/insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
lstore 3
L1:
lload 3
lreturn
L3:
iconst_0
istore 2
goto L4
L2:
astore 1
aload 1
invokevirtual java/lang/Exception/printStackTrace()V
ldc2_w -1L
lreturn
.limit locals 6
.limit stack 5
.end method

.method public final a()Ljava/util/ArrayList;
.catch android/database/SQLException from L0 to L1 using L2
.catch android/database/SQLException from L1 to L3 using L2
.catch android/database/SQLException from L4 to L5 using L2
.catch android/database/SQLException from L5 to L6 using L2
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 2
L0:
aload 0
getfield com/teamspeak/ts3client/data/b/a/f Landroid/database/sqlite/SQLiteDatabase;
ldc "server"
bipush 10
anewarray java/lang/String
dup
iconst_0
ldc "server_id"
aastore
dup
iconst_1
ldc "label"
aastore
dup
iconst_2
ldc "address"
aastore
dup
iconst_3
ldc "serverpassword"
aastore
dup
iconst_4
ldc "nickname"
aastore
dup
iconst_5
ldc "defaultchannel"
aastore
dup
bipush 6
ldc "defaultchannelpassword"
aastore
dup
bipush 7
ldc "ident"
aastore
dup
bipush 8
ldc "subscribeall"
aastore
dup
bipush 9
ldc "subscriptionlist"
aastore
aconst_null
aconst_null
aconst_null
aconst_null
aconst_null
invokevirtual android/database/sqlite/SQLiteDatabase/query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
astore 3
aload 3
invokeinterface android/database/Cursor/moveToFirst()Z 0
pop
aload 3
invokeinterface android/database/Cursor/isAfterLast()Z 0
ifne L5
L1:
new com/teamspeak/ts3client/data/ab
dup
invokespecial com/teamspeak/ts3client/data/ab/<init>()V
astore 4
aload 4
aload 3
aload 3
ldc "label"
invokeinterface android/database/Cursor/getColumnIndex(Ljava/lang/String;)I 1
invokeinterface android/database/Cursor/getString(I)Ljava/lang/String; 1
putfield com/teamspeak/ts3client/data/ab/a Ljava/lang/String;
aload 4
aload 3
aload 3
ldc "address"
invokeinterface android/database/Cursor/getColumnIndex(Ljava/lang/String;)I 1
invokeinterface android/database/Cursor/getString(I)Ljava/lang/String; 1
ldc "(.*://)+"
ldc ""
invokevirtual java/lang/String/replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/data/ab/a(Ljava/lang/String;)V
aload 4
new java/lang/String
dup
aload 3
aload 3
ldc "serverpassword"
invokeinterface android/database/Cursor/getColumnIndex(Ljava/lang/String;)I 1
invokeinterface android/database/Cursor/getBlob(I)[B 1
invokestatic com/teamspeak/ts3client/data/b/a/a([B)Ljava/lang/String;
invokespecial java/lang/String/<init>(Ljava/lang/String;)V
putfield com/teamspeak/ts3client/data/ab/e Ljava/lang/String;
aload 4
aload 3
aload 3
ldc "nickname"
invokeinterface android/database/Cursor/getColumnIndex(Ljava/lang/String;)I 1
invokeinterface android/database/Cursor/getString(I)Ljava/lang/String; 1
putfield com/teamspeak/ts3client/data/ab/f Ljava/lang/String;
aload 4
aload 3
aload 3
ldc "defaultchannel"
invokeinterface android/database/Cursor/getColumnIndex(Ljava/lang/String;)I 1
invokeinterface android/database/Cursor/getString(I)Ljava/lang/String; 1
putfield com/teamspeak/ts3client/data/ab/g Ljava/lang/String;
aload 4
aload 3
aload 3
ldc "defaultchannelpassword"
invokeinterface android/database/Cursor/getColumnIndex(Ljava/lang/String;)I 1
invokeinterface android/database/Cursor/getString(I)Ljava/lang/String; 1
putfield com/teamspeak/ts3client/data/ab/h Ljava/lang/String;
aload 4
aload 3
aload 3
ldc "server_id"
invokeinterface android/database/Cursor/getColumnIndex(Ljava/lang/String;)I 1
invokeinterface android/database/Cursor/getLong(I)J 1
putfield com/teamspeak/ts3client/data/ab/l J
aload 4
aload 3
aload 3
ldc "ident"
invokeinterface android/database/Cursor/getColumnIndex(Ljava/lang/String;)I 1
invokeinterface android/database/Cursor/getInt(I)I 1
putfield com/teamspeak/ts3client/data/ab/d I
aload 3
aload 3
ldc "subscribeall"
invokeinterface android/database/Cursor/getColumnIndex(Ljava/lang/String;)I 1
invokeinterface android/database/Cursor/getInt(I)I 1
ifne L7
L3:
iconst_0
istore 1
L4:
aload 4
iload 1
putfield com/teamspeak/ts3client/data/ab/j Z
aload 4
aload 3
aload 3
ldc "subscriptionlist"
invokeinterface android/database/Cursor/getColumnIndex(Ljava/lang/String;)I 1
invokeinterface android/database/Cursor/getString(I)Ljava/lang/String; 1
invokevirtual com/teamspeak/ts3client/data/ab/b(Ljava/lang/String;)V
aload 2
aload 4
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 3
invokeinterface android/database/Cursor/moveToNext()Z 0
ifne L1
L5:
aload 3
invokeinterface android/database/Cursor/close()V 0
L6:
aload 2
areturn
L7:
iconst_1
istore 1
goto L4
L2:
astore 2
aload 2
invokevirtual android/database/SQLException/printStackTrace()V
aconst_null
areturn
.limit locals 5
.limit stack 8
.end method

.method public final a(J)Z
.catch java/lang/Exception from L0 to L1 using L2
L0:
aload 0
getfield com/teamspeak/ts3client/data/b/a/f Landroid/database/sqlite/SQLiteDatabase;
ldc "server"
new java/lang/StringBuilder
dup
ldc "server_id="
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
lload 1
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aconst_null
invokevirtual android/database/sqlite/SQLiteDatabase/delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
pop
L1:
iconst_1
ireturn
L2:
astore 3
aload 3
invokevirtual java/lang/Exception/printStackTrace()V
iconst_0
ireturn
.limit locals 4
.limit stack 5
.end method

.method public final a(JLcom/teamspeak/ts3client/data/ab;)Z
.catch java/lang/Exception from L0 to L1 using L2
new android/content/ContentValues
dup
invokespecial android/content/ContentValues/<init>()V
astore 5
aload 5
ldc "label"
aload 3
getfield com/teamspeak/ts3client/data/ab/a Ljava/lang/String;
invokevirtual android/content/ContentValues/put(Ljava/lang/String;Ljava/lang/String;)V
aload 5
ldc "address"
aload 3
getfield com/teamspeak/ts3client/data/ab/c Ljava/lang/String;
ldc "(.*://)+"
ldc ""
invokevirtual java/lang/String/replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/content/ContentValues/put(Ljava/lang/String;Ljava/lang/String;)V
aload 5
ldc "serverpassword"
aload 3
getfield com/teamspeak/ts3client/data/ab/e Ljava/lang/String;
invokestatic com/teamspeak/ts3client/data/b/a/a(Ljava/lang/String;)[B
invokevirtual android/content/ContentValues/put(Ljava/lang/String;[B)V
aload 5
ldc "nickname"
aload 3
getfield com/teamspeak/ts3client/data/ab/f Ljava/lang/String;
invokevirtual android/content/ContentValues/put(Ljava/lang/String;Ljava/lang/String;)V
aload 5
ldc "defaultchannel"
aload 3
getfield com/teamspeak/ts3client/data/ab/g Ljava/lang/String;
invokevirtual android/content/ContentValues/put(Ljava/lang/String;Ljava/lang/String;)V
aload 5
ldc "defaultchannelpassword"
aload 3
getfield com/teamspeak/ts3client/data/ab/h Ljava/lang/String;
invokevirtual android/content/ContentValues/put(Ljava/lang/String;Ljava/lang/String;)V
aload 5
ldc "ident"
aload 3
getfield com/teamspeak/ts3client/data/ab/d I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual android/content/ContentValues/put(Ljava/lang/String;Ljava/lang/Integer;)V
aload 3
getfield com/teamspeak/ts3client/data/ab/j Z
ifeq L3
iconst_1
istore 4
L4:
aload 5
ldc "subscribeall"
iload 4
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual android/content/ContentValues/put(Ljava/lang/String;Ljava/lang/Integer;)V
aload 5
ldc "subscriptionlist"
aload 3
invokevirtual com/teamspeak/ts3client/data/ab/h()Ljava/lang/String;
invokevirtual android/content/ContentValues/put(Ljava/lang/String;Ljava/lang/String;)V
L0:
aload 0
getfield com/teamspeak/ts3client/data/b/a/f Landroid/database/sqlite/SQLiteDatabase;
ldc "server"
aload 5
new java/lang/StringBuilder
dup
ldc "server_id="
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
lload 1
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aconst_null
invokevirtual android/database/sqlite/SQLiteDatabase/update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
pop
L1:
iconst_1
ireturn
L3:
iconst_0
istore 4
goto L4
L2:
astore 3
aload 3
invokevirtual java/lang/Exception/printStackTrace()V
iconst_0
ireturn
.limit locals 6
.limit stack 6
.end method
