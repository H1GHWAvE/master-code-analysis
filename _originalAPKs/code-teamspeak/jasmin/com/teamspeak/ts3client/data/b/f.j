.bytecode 50.0
.class public final synchronized com/teamspeak/ts3client/data/b/f
.super java/lang/Object

.field private static final 'b' Ljava/lang/String; = "create table ident (ident_id integer primary key autoincrement, name text not null, identity text not null, nickname text not null, defaultid int not null);"

.field private static final 'c' Ljava/lang/String; = "ident"

.field private static final 'd' Ljava/lang/String; = "Teamspeak-Ident"

.field private static final 'e' I = 1


.field private static 'f' Lcom/teamspeak/ts3client/data/b/e;

.field private static 'g' Lcom/teamspeak/ts3client/data/b/f;

.field public 'a' Landroid/database/sqlite/SQLiteDatabase;

.method static <clinit>()V
aconst_null
putstatic com/teamspeak/ts3client/data/b/f/f Lcom/teamspeak/ts3client/data/b/e;
return
.limit locals 0
.limit stack 1
.end method

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
new com/teamspeak/ts3client/data/b/g
dup
aload 0
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
invokespecial com/teamspeak/ts3client/data/b/g/<init>(Lcom/teamspeak/ts3client/data/b/f;Landroid/content/Context;)V
invokevirtual com/teamspeak/ts3client/data/b/g/getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;
putfield com/teamspeak/ts3client/data/b/f/a Landroid/database/sqlite/SQLiteDatabase;
return
.limit locals 1
.limit stack 5
.end method

.method public static a()Lcom/teamspeak/ts3client/data/b/f;
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
ldc com/teamspeak/ts3client/data/b/f
monitorenter
L0:
getstatic com/teamspeak/ts3client/data/b/f/g Lcom/teamspeak/ts3client/data/b/f;
ifnonnull L3
new com/teamspeak/ts3client/data/b/f
dup
invokespecial com/teamspeak/ts3client/data/b/f/<init>()V
astore 0
aload 0
putstatic com/teamspeak/ts3client/data/b/f/g Lcom/teamspeak/ts3client/data/b/f;
L1:
ldc com/teamspeak/ts3client/data/b/f
monitorexit
aload 0
areturn
L3:
getstatic com/teamspeak/ts3client/data/b/f/g Lcom/teamspeak/ts3client/data/b/f;
astore 0
L4:
goto L1
L2:
astore 0
ldc com/teamspeak/ts3client/data/b/f
monitorexit
aload 0
athrow
.limit locals 1
.limit stack 2
.end method

.method private a(I)V
.catch android/database/SQLException from L0 to L1 using L2
.catch android/database/SQLException from L1 to L3 using L2
.catch android/database/SQLException from L3 to L4 using L2
new com/teamspeak/ts3client/e/a
dup
invokespecial com/teamspeak/ts3client/e/a/<init>()V
astore 2
L0:
aload 0
getfield com/teamspeak/ts3client/data/b/f/a Landroid/database/sqlite/SQLiteDatabase;
ldc "ident"
iconst_5
anewarray java/lang/String
dup
iconst_0
ldc "ident_id"
aastore
dup
iconst_1
ldc "name"
aastore
dup
iconst_2
ldc "identity"
aastore
dup
iconst_3
ldc "nickname"
aastore
dup
iconst_4
ldc "defaultid"
aastore
ldc "defaultid=1"
aconst_null
aconst_null
aconst_null
aconst_null
aconst_null
invokevirtual android/database/sqlite/SQLiteDatabase/query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
astore 3
aload 3
invokeinterface android/database/Cursor/moveToFirst()Z 0
ifeq L1
aload 2
aload 3
aload 3
ldc "name"
invokeinterface android/database/Cursor/getColumnIndex(Ljava/lang/String;)I 1
invokeinterface android/database/Cursor/getString(I)Ljava/lang/String; 1
putfield com/teamspeak/ts3client/e/a/b Ljava/lang/String;
aload 2
aload 3
aload 3
ldc "identity"
invokeinterface android/database/Cursor/getColumnIndex(Ljava/lang/String;)I 1
invokeinterface android/database/Cursor/getString(I)Ljava/lang/String; 1
putfield com/teamspeak/ts3client/e/a/c Ljava/lang/String;
aload 2
aload 3
aload 3
ldc "nickname"
invokeinterface android/database/Cursor/getColumnIndex(Ljava/lang/String;)I 1
invokeinterface android/database/Cursor/getString(I)Ljava/lang/String; 1
putfield com/teamspeak/ts3client/e/a/d Ljava/lang/String;
aload 2
iconst_0
putfield com/teamspeak/ts3client/e/a/e Z
aload 2
aload 3
aload 3
ldc "ident_id"
invokeinterface android/database/Cursor/getColumnIndex(Ljava/lang/String;)I 1
invokeinterface android/database/Cursor/getInt(I)I 1
putfield com/teamspeak/ts3client/e/a/a I
aload 0
aload 2
getfield com/teamspeak/ts3client/e/a/a I
i2l
aload 2
invokevirtual com/teamspeak/ts3client/data/b/f/a(JLcom/teamspeak/ts3client/e/a;)Z
pop
L1:
aload 3
invokeinterface android/database/Cursor/close()V 0
new com/teamspeak/ts3client/e/a
dup
invokespecial com/teamspeak/ts3client/e/a/<init>()V
astore 2
aload 0
getfield com/teamspeak/ts3client/data/b/f/a Landroid/database/sqlite/SQLiteDatabase;
astore 3
new java/lang/StringBuilder
dup
ldc "ident_id="
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
iload 1
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 4
aload 3
ldc "ident"
iconst_5
anewarray java/lang/String
dup
iconst_0
ldc "ident_id"
aastore
dup
iconst_1
ldc "name"
aastore
dup
iconst_2
ldc "identity"
aastore
dup
iconst_3
ldc "nickname"
aastore
dup
iconst_4
ldc "defaultid"
aastore
aload 4
aconst_null
aconst_null
aconst_null
aconst_null
aconst_null
invokevirtual android/database/sqlite/SQLiteDatabase/query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
astore 3
aload 3
invokeinterface android/database/Cursor/moveToFirst()Z 0
ifeq L3
aload 2
aload 3
aload 3
ldc "name"
invokeinterface android/database/Cursor/getColumnIndex(Ljava/lang/String;)I 1
invokeinterface android/database/Cursor/getString(I)Ljava/lang/String; 1
putfield com/teamspeak/ts3client/e/a/b Ljava/lang/String;
aload 2
aload 3
aload 3
ldc "identity"
invokeinterface android/database/Cursor/getColumnIndex(Ljava/lang/String;)I 1
invokeinterface android/database/Cursor/getString(I)Ljava/lang/String; 1
putfield com/teamspeak/ts3client/e/a/c Ljava/lang/String;
aload 2
aload 3
aload 3
ldc "nickname"
invokeinterface android/database/Cursor/getColumnIndex(Ljava/lang/String;)I 1
invokeinterface android/database/Cursor/getString(I)Ljava/lang/String; 1
putfield com/teamspeak/ts3client/e/a/d Ljava/lang/String;
aload 2
iconst_1
putfield com/teamspeak/ts3client/e/a/e Z
aload 2
aload 3
aload 3
ldc "ident_id"
invokeinterface android/database/Cursor/getColumnIndex(Ljava/lang/String;)I 1
invokeinterface android/database/Cursor/getInt(I)I 1
putfield com/teamspeak/ts3client/e/a/a I
aload 0
aload 2
getfield com/teamspeak/ts3client/e/a/a I
i2l
aload 2
invokevirtual com/teamspeak/ts3client/data/b/f/a(JLcom/teamspeak/ts3client/e/a;)Z
pop
L3:
aload 3
invokeinterface android/database/Cursor/close()V 0
L4:
return
L2:
astore 2
return
.limit locals 5
.limit stack 9
.end method

.method public static a(Lcom/teamspeak/ts3client/data/b/e;)V
aload 0
putstatic com/teamspeak/ts3client/data/b/f/f Lcom/teamspeak/ts3client/data/b/e;
return
.limit locals 1
.limit stack 1
.end method

.method private e()V
aload 0
getfield com/teamspeak/ts3client/data/b/f/a Landroid/database/sqlite/SQLiteDatabase;
invokevirtual android/database/sqlite/SQLiteDatabase/close()V
return
.limit locals 1
.limit stack 1
.end method

.method private static f()V
getstatic com/teamspeak/ts3client/data/b/f/f Lcom/teamspeak/ts3client/data/b/e;
ifnull L0
getstatic com/teamspeak/ts3client/data/b/f/f Lcom/teamspeak/ts3client/data/b/e;
invokeinterface com/teamspeak/ts3client/data/b/e/a()V 0
L0:
return
.limit locals 0
.limit stack 1
.end method

.method private static g()V
aconst_null
putstatic com/teamspeak/ts3client/data/b/f/f Lcom/teamspeak/ts3client/data/b/e;
return
.limit locals 0
.limit stack 1
.end method

.method public final a(Lcom/teamspeak/ts3client/e/a;)J
.catch java/lang/Exception from L0 to L1 using L2
new android/content/ContentValues
dup
invokespecial android/content/ContentValues/<init>()V
astore 5
aload 5
ldc "name"
aload 1
getfield com/teamspeak/ts3client/e/a/b Ljava/lang/String;
invokevirtual android/content/ContentValues/put(Ljava/lang/String;Ljava/lang/String;)V
aload 5
ldc "identity"
aload 1
getfield com/teamspeak/ts3client/e/a/c Ljava/lang/String;
invokevirtual android/content/ContentValues/put(Ljava/lang/String;Ljava/lang/String;)V
aload 5
ldc "nickname"
aload 1
getfield com/teamspeak/ts3client/e/a/d Ljava/lang/String;
invokevirtual android/content/ContentValues/put(Ljava/lang/String;Ljava/lang/String;)V
aload 1
getfield com/teamspeak/ts3client/e/a/e Z
ifeq L3
iconst_1
istore 2
L4:
aload 5
ldc "defaultid"
iload 2
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual android/content/ContentValues/put(Ljava/lang/String;Ljava/lang/Integer;)V
L0:
aload 0
getfield com/teamspeak/ts3client/data/b/f/a Landroid/database/sqlite/SQLiteDatabase;
ldc "ident"
aconst_null
aload 5
invokevirtual android/database/sqlite/SQLiteDatabase/insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
lstore 3
L1:
lload 3
lreturn
L3:
iconst_0
istore 2
goto L4
L2:
astore 1
aload 1
invokevirtual java/lang/Exception/printStackTrace()V
ldc2_w -1L
lreturn
.limit locals 6
.limit stack 4
.end method

.method public final a(J)Z
.catch android/database/SQLException from L0 to L1 using L2
.catch android/database/SQLException from L1 to L3 using L2
.catch android/database/SQLException from L3 to L4 using L2
.catch android/database/SQLException from L4 to L5 using L2
.catch java/lang/Exception from L5 to L6 using L7
.catch android/database/SQLException from L8 to L9 using L2
new com/teamspeak/ts3client/e/a
dup
invokespecial com/teamspeak/ts3client/e/a/<init>()V
astore 3
L0:
aload 0
getfield com/teamspeak/ts3client/data/b/f/a Landroid/database/sqlite/SQLiteDatabase;
astore 4
new java/lang/StringBuilder
dup
ldc "ident_id="
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
lload 1
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 5
aload 4
ldc "ident"
iconst_5
anewarray java/lang/String
dup
iconst_0
ldc "ident_id"
aastore
dup
iconst_1
ldc "name"
aastore
dup
iconst_2
ldc "identity"
aastore
dup
iconst_3
ldc "nickname"
aastore
dup
iconst_4
ldc "defaultid"
aastore
aload 5
aconst_null
aconst_null
aconst_null
aconst_null
aconst_null
invokevirtual android/database/sqlite/SQLiteDatabase/query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
astore 4
aload 4
invokeinterface android/database/Cursor/moveToFirst()Z 0
ifeq L3
aload 3
aload 4
aload 4
ldc "name"
invokeinterface android/database/Cursor/getColumnIndex(Ljava/lang/String;)I 1
invokeinterface android/database/Cursor/getString(I)Ljava/lang/String; 1
putfield com/teamspeak/ts3client/e/a/b Ljava/lang/String;
aload 3
aload 4
aload 4
ldc "identity"
invokeinterface android/database/Cursor/getColumnIndex(Ljava/lang/String;)I 1
invokeinterface android/database/Cursor/getString(I)Ljava/lang/String; 1
putfield com/teamspeak/ts3client/e/a/c Ljava/lang/String;
aload 3
aload 4
aload 4
ldc "nickname"
invokeinterface android/database/Cursor/getColumnIndex(Ljava/lang/String;)I 1
invokeinterface android/database/Cursor/getString(I)Ljava/lang/String; 1
putfield com/teamspeak/ts3client/e/a/d Ljava/lang/String;
aload 4
aload 4
ldc "defaultid"
invokeinterface android/database/Cursor/getColumnIndex(Ljava/lang/String;)I 1
invokeinterface android/database/Cursor/getInt(I)I 1
ifne L8
aload 3
iconst_0
putfield com/teamspeak/ts3client/e/a/e Z
L1:
aload 3
aload 4
aload 4
ldc "ident_id"
invokeinterface android/database/Cursor/getColumnIndex(Ljava/lang/String;)I 1
invokeinterface android/database/Cursor/getInt(I)I 1
putfield com/teamspeak/ts3client/e/a/a I
aload 0
aload 3
getfield com/teamspeak/ts3client/e/a/a I
i2l
aload 3
invokevirtual com/teamspeak/ts3client/data/b/f/a(JLcom/teamspeak/ts3client/e/a;)Z
pop
L3:
aload 4
invokeinterface android/database/Cursor/close()V 0
aload 3
getfield com/teamspeak/ts3client/e/a/e Z
ifeq L5
new com/teamspeak/ts3client/e/a
dup
invokespecial com/teamspeak/ts3client/e/a/<init>()V
astore 3
aload 0
getfield com/teamspeak/ts3client/data/b/f/a Landroid/database/sqlite/SQLiteDatabase;
ldc "ident"
iconst_5
anewarray java/lang/String
dup
iconst_0
ldc "ident_id"
aastore
dup
iconst_1
ldc "name"
aastore
dup
iconst_2
ldc "identity"
aastore
dup
iconst_3
ldc "nickname"
aastore
dup
iconst_4
ldc "defaultid"
aastore
ldc "ident_id=1"
aconst_null
aconst_null
aconst_null
aconst_null
aconst_null
invokevirtual android/database/sqlite/SQLiteDatabase/query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
astore 4
aload 4
invokeinterface android/database/Cursor/moveToFirst()Z 0
ifeq L4
aload 3
aload 4
aload 4
ldc "name"
invokeinterface android/database/Cursor/getColumnIndex(Ljava/lang/String;)I 1
invokeinterface android/database/Cursor/getString(I)Ljava/lang/String; 1
putfield com/teamspeak/ts3client/e/a/b Ljava/lang/String;
aload 3
aload 4
aload 4
ldc "identity"
invokeinterface android/database/Cursor/getColumnIndex(Ljava/lang/String;)I 1
invokeinterface android/database/Cursor/getString(I)Ljava/lang/String; 1
putfield com/teamspeak/ts3client/e/a/c Ljava/lang/String;
aload 3
aload 4
aload 4
ldc "nickname"
invokeinterface android/database/Cursor/getColumnIndex(Ljava/lang/String;)I 1
invokeinterface android/database/Cursor/getString(I)Ljava/lang/String; 1
putfield com/teamspeak/ts3client/e/a/d Ljava/lang/String;
aload 3
iconst_1
putfield com/teamspeak/ts3client/e/a/e Z
aload 3
aload 4
aload 4
ldc "ident_id"
invokeinterface android/database/Cursor/getColumnIndex(Ljava/lang/String;)I 1
invokeinterface android/database/Cursor/getInt(I)I 1
putfield com/teamspeak/ts3client/e/a/a I
aload 0
aload 3
getfield com/teamspeak/ts3client/e/a/a I
i2l
aload 3
invokevirtual com/teamspeak/ts3client/data/b/f/a(JLcom/teamspeak/ts3client/e/a;)Z
pop
L4:
aload 4
invokeinterface android/database/Cursor/close()V 0
L5:
aload 0
getfield com/teamspeak/ts3client/data/b/f/a Landroid/database/sqlite/SQLiteDatabase;
ldc "ident"
new java/lang/StringBuilder
dup
ldc "ident_id="
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
lload 1
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aconst_null
invokevirtual android/database/sqlite/SQLiteDatabase/delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
pop
invokestatic com/teamspeak/ts3client/data/b/f/f()V
L6:
iconst_1
ireturn
L8:
aload 3
iconst_1
putfield com/teamspeak/ts3client/e/a/e Z
L9:
goto L1
L2:
astore 3
goto L5
L7:
astore 3
aload 3
invokevirtual java/lang/Exception/printStackTrace()V
iconst_0
ireturn
.limit locals 6
.limit stack 9
.end method

.method public final a(JLcom/teamspeak/ts3client/e/a;)Z
.catch java/lang/Exception from L0 to L1 using L2
new android/content/ContentValues
dup
invokespecial android/content/ContentValues/<init>()V
astore 5
aload 5
ldc "name"
aload 3
getfield com/teamspeak/ts3client/e/a/b Ljava/lang/String;
invokevirtual android/content/ContentValues/put(Ljava/lang/String;Ljava/lang/String;)V
aload 5
ldc "identity"
aload 3
getfield com/teamspeak/ts3client/e/a/c Ljava/lang/String;
invokevirtual android/content/ContentValues/put(Ljava/lang/String;Ljava/lang/String;)V
aload 5
ldc "nickname"
aload 3
getfield com/teamspeak/ts3client/e/a/d Ljava/lang/String;
invokevirtual android/content/ContentValues/put(Ljava/lang/String;Ljava/lang/String;)V
aload 3
getfield com/teamspeak/ts3client/e/a/e Z
ifeq L3
iconst_1
istore 4
L4:
aload 5
ldc "defaultid"
iload 4
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual android/content/ContentValues/put(Ljava/lang/String;Ljava/lang/Integer;)V
L0:
aload 0
getfield com/teamspeak/ts3client/data/b/f/a Landroid/database/sqlite/SQLiteDatabase;
ldc "ident"
aload 5
new java/lang/StringBuilder
dup
ldc "ident_id="
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
lload 1
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aconst_null
invokevirtual android/database/sqlite/SQLiteDatabase/update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
pop
invokestatic com/teamspeak/ts3client/data/b/f/f()V
L1:
iconst_1
ireturn
L3:
iconst_0
istore 4
goto L4
L2:
astore 3
aload 3
invokevirtual java/lang/Exception/printStackTrace()V
iconst_0
ireturn
.limit locals 6
.limit stack 6
.end method

.method public final b(J)Lcom/teamspeak/ts3client/e/a;
.catch android/database/SQLException from L0 to L1 using L2
.catch android/database/SQLException from L1 to L3 using L2
.catch android/database/SQLException from L3 to L4 using L2
.catch android/database/SQLException from L5 to L6 using L2
new com/teamspeak/ts3client/e/a
dup
invokespecial com/teamspeak/ts3client/e/a/<init>()V
astore 3
L0:
aload 0
getfield com/teamspeak/ts3client/data/b/f/a Landroid/database/sqlite/SQLiteDatabase;
astore 4
new java/lang/StringBuilder
dup
ldc "ident_id="
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
lload 1
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 5
aload 4
ldc "ident"
iconst_5
anewarray java/lang/String
dup
iconst_0
ldc "ident_id"
aastore
dup
iconst_1
ldc "name"
aastore
dup
iconst_2
ldc "identity"
aastore
dup
iconst_3
ldc "nickname"
aastore
dup
iconst_4
ldc "defaultid"
aastore
aload 5
aconst_null
aconst_null
aconst_null
aconst_null
aconst_null
invokevirtual android/database/sqlite/SQLiteDatabase/query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
astore 4
aload 4
invokeinterface android/database/Cursor/moveToFirst()Z 0
ifeq L3
aload 3
aload 4
aload 4
ldc "name"
invokeinterface android/database/Cursor/getColumnIndex(Ljava/lang/String;)I 1
invokeinterface android/database/Cursor/getString(I)Ljava/lang/String; 1
putfield com/teamspeak/ts3client/e/a/b Ljava/lang/String;
aload 3
aload 4
aload 4
ldc "identity"
invokeinterface android/database/Cursor/getColumnIndex(Ljava/lang/String;)I 1
invokeinterface android/database/Cursor/getString(I)Ljava/lang/String; 1
putfield com/teamspeak/ts3client/e/a/c Ljava/lang/String;
aload 3
aload 4
aload 4
ldc "nickname"
invokeinterface android/database/Cursor/getColumnIndex(Ljava/lang/String;)I 1
invokeinterface android/database/Cursor/getString(I)Ljava/lang/String; 1
putfield com/teamspeak/ts3client/e/a/d Ljava/lang/String;
aload 4
aload 4
ldc "defaultid"
invokeinterface android/database/Cursor/getColumnIndex(Ljava/lang/String;)I 1
invokeinterface android/database/Cursor/getInt(I)I 1
ifne L5
aload 3
iconst_0
putfield com/teamspeak/ts3client/e/a/e Z
L1:
aload 3
aload 4
aload 4
ldc "ident_id"
invokeinterface android/database/Cursor/getColumnIndex(Ljava/lang/String;)I 1
invokeinterface android/database/Cursor/getInt(I)I 1
putfield com/teamspeak/ts3client/e/a/a I
L3:
aload 4
invokeinterface android/database/Cursor/close()V 0
L4:
aload 3
areturn
L5:
aload 3
iconst_1
putfield com/teamspeak/ts3client/e/a/e Z
L6:
goto L1
L2:
astore 3
aconst_null
areturn
.limit locals 6
.limit stack 9
.end method

.method public final b()V
aload 0
new com/teamspeak/ts3client/data/b/g
dup
aload 0
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
invokespecial com/teamspeak/ts3client/data/b/g/<init>(Lcom/teamspeak/ts3client/data/b/f;Landroid/content/Context;)V
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/getApplicationContext()Landroid/content/Context;
invokevirtual com/teamspeak/ts3client/data/b/g/a(Landroid/content/Context;)Landroid/database/sqlite/SQLiteDatabase;
putfield com/teamspeak/ts3client/data/b/f/a Landroid/database/sqlite/SQLiteDatabase;
return
.limit locals 1
.limit stack 5
.end method

.method public final c()Ljava/util/ArrayList;
.catch android/database/SQLException from L0 to L1 using L2
.catch android/database/SQLException from L1 to L3 using L2
.catch android/database/SQLException from L3 to L4 using L2
.catch android/database/SQLException from L4 to L5 using L2
.catch android/database/SQLException from L6 to L7 using L2
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 1
L0:
aload 0
getfield com/teamspeak/ts3client/data/b/f/a Landroid/database/sqlite/SQLiteDatabase;
ldc "ident"
iconst_5
anewarray java/lang/String
dup
iconst_0
ldc "ident_id"
aastore
dup
iconst_1
ldc "name"
aastore
dup
iconst_2
ldc "identity"
aastore
dup
iconst_3
ldc "nickname"
aastore
dup
iconst_4
ldc "defaultid"
aastore
aconst_null
aconst_null
aconst_null
aconst_null
aconst_null
invokevirtual android/database/sqlite/SQLiteDatabase/query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
astore 2
aload 2
invokeinterface android/database/Cursor/moveToFirst()Z 0
pop
aload 2
invokeinterface android/database/Cursor/isAfterLast()Z 0
ifne L4
L1:
new com/teamspeak/ts3client/e/a
dup
invokespecial com/teamspeak/ts3client/e/a/<init>()V
astore 3
aload 3
aload 2
aload 2
ldc "name"
invokeinterface android/database/Cursor/getColumnIndex(Ljava/lang/String;)I 1
invokeinterface android/database/Cursor/getString(I)Ljava/lang/String; 1
putfield com/teamspeak/ts3client/e/a/b Ljava/lang/String;
aload 3
aload 2
aload 2
ldc "identity"
invokeinterface android/database/Cursor/getColumnIndex(Ljava/lang/String;)I 1
invokeinterface android/database/Cursor/getString(I)Ljava/lang/String; 1
putfield com/teamspeak/ts3client/e/a/c Ljava/lang/String;
aload 3
aload 2
aload 2
ldc "nickname"
invokeinterface android/database/Cursor/getColumnIndex(Ljava/lang/String;)I 1
invokeinterface android/database/Cursor/getString(I)Ljava/lang/String; 1
putfield com/teamspeak/ts3client/e/a/d Ljava/lang/String;
aload 2
aload 2
ldc "defaultid"
invokeinterface android/database/Cursor/getColumnIndex(Ljava/lang/String;)I 1
invokeinterface android/database/Cursor/getInt(I)I 1
ifne L6
aload 3
iconst_0
putfield com/teamspeak/ts3client/e/a/e Z
L3:
aload 3
aload 2
aload 2
ldc "ident_id"
invokeinterface android/database/Cursor/getColumnIndex(Ljava/lang/String;)I 1
invokeinterface android/database/Cursor/getInt(I)I 1
putfield com/teamspeak/ts3client/e/a/a I
aload 1
aload 3
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 2
invokeinterface android/database/Cursor/moveToNext()Z 0
ifne L1
L4:
aload 2
invokeinterface android/database/Cursor/close()V 0
L5:
aload 1
areturn
L6:
aload 3
iconst_1
putfield com/teamspeak/ts3client/e/a/e Z
L7:
goto L3
L2:
astore 1
aload 1
invokevirtual android/database/SQLException/printStackTrace()V
aconst_null
areturn
.limit locals 4
.limit stack 8
.end method

.method public final d()Lcom/teamspeak/ts3client/e/a;
.catch android/database/SQLException from L0 to L1 using L2
.catch android/database/SQLException from L1 to L3 using L2
.catch android/database/SQLException from L3 to L4 using L2
.catch android/database/SQLException from L5 to L6 using L2
new com/teamspeak/ts3client/e/a
dup
invokespecial com/teamspeak/ts3client/e/a/<init>()V
astore 1
L0:
aload 0
getfield com/teamspeak/ts3client/data/b/f/a Landroid/database/sqlite/SQLiteDatabase;
ldc "ident"
iconst_5
anewarray java/lang/String
dup
iconst_0
ldc "ident_id"
aastore
dup
iconst_1
ldc "name"
aastore
dup
iconst_2
ldc "identity"
aastore
dup
iconst_3
ldc "nickname"
aastore
dup
iconst_4
ldc "defaultid"
aastore
ldc "defaultid=1"
aconst_null
aconst_null
aconst_null
aconst_null
aconst_null
invokevirtual android/database/sqlite/SQLiteDatabase/query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
astore 2
aload 2
invokeinterface android/database/Cursor/moveToFirst()Z 0
ifeq L3
aload 1
aload 2
aload 2
ldc "name"
invokeinterface android/database/Cursor/getColumnIndex(Ljava/lang/String;)I 1
invokeinterface android/database/Cursor/getString(I)Ljava/lang/String; 1
putfield com/teamspeak/ts3client/e/a/b Ljava/lang/String;
aload 1
aload 2
aload 2
ldc "identity"
invokeinterface android/database/Cursor/getColumnIndex(Ljava/lang/String;)I 1
invokeinterface android/database/Cursor/getString(I)Ljava/lang/String; 1
putfield com/teamspeak/ts3client/e/a/c Ljava/lang/String;
aload 1
aload 2
aload 2
ldc "nickname"
invokeinterface android/database/Cursor/getColumnIndex(Ljava/lang/String;)I 1
invokeinterface android/database/Cursor/getString(I)Ljava/lang/String; 1
putfield com/teamspeak/ts3client/e/a/d Ljava/lang/String;
aload 2
aload 2
ldc "defaultid"
invokeinterface android/database/Cursor/getColumnIndex(Ljava/lang/String;)I 1
invokeinterface android/database/Cursor/getInt(I)I 1
ifne L5
aload 1
iconst_0
putfield com/teamspeak/ts3client/e/a/e Z
L1:
aload 1
aload 2
aload 2
ldc "ident_id"
invokeinterface android/database/Cursor/getColumnIndex(Ljava/lang/String;)I 1
invokeinterface android/database/Cursor/getInt(I)I 1
putfield com/teamspeak/ts3client/e/a/a I
L3:
aload 2
invokeinterface android/database/Cursor/close()V 0
L4:
aload 1
areturn
L5:
aload 1
iconst_1
putfield com/teamspeak/ts3client/e/a/e Z
L6:
goto L1
L2:
astore 1
aconst_null
areturn
.limit locals 3
.limit stack 9
.end method
