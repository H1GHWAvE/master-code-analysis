.bytecode 50.0
.class final synchronized com/teamspeak/ts3client/data/b/g
.super android/database/sqlite/SQLiteOpenHelper

.field final synthetic 'a' Lcom/teamspeak/ts3client/data/b/f;

.method public <init>(Lcom/teamspeak/ts3client/data/b/f;Landroid/content/Context;)V
aload 0
aload 1
putfield com/teamspeak/ts3client/data/b/g/a Lcom/teamspeak/ts3client/data/b/f;
aload 0
aload 2
ldc "Teamspeak-Ident"
aconst_null
iconst_1
invokespecial android/database/sqlite/SQLiteOpenHelper/<init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V
return
.limit locals 3
.limit stack 5
.end method

.method public final a(Landroid/content/Context;)Landroid/database/sqlite/SQLiteDatabase;
aload 1
ldc "Teamspeak-Ident"
invokevirtual android/content/Context/getDatabasePath(Ljava/lang/String;)Ljava/io/File;
invokevirtual java/io/File/delete()Z
pop
aload 0
invokespecial android/database/sqlite/SQLiteOpenHelper/getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;
areturn
.limit locals 2
.limit stack 2
.end method

.method public final onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
.catch java/lang/Exception from L0 to L1 using L2
aload 1
ldc "create table ident (ident_id integer primary key autoincrement, name text not null, identity text not null, nickname text not null, defaultid int not null);"
invokevirtual android/database/sqlite/SQLiteDatabase/execSQL(Ljava/lang/String;)V
invokestatic com/teamspeak/ts3client/jni/Ts3Jni/b()Lcom/teamspeak/ts3client/jni/Ts3Jni;
astore 2
aload 2
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_startInit()I
pop
invokestatic com/teamspeak/ts3client/jni/Ts3Jni/a()I
iconst_5
if_icmpeq L1
aload 2
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_createIdentity()Ljava/lang/String;
astore 2
new android/content/ContentValues
dup
invokespecial android/content/ContentValues/<init>()V
astore 3
aload 3
ldc "name"
ldc "Android Default"
invokevirtual android/content/ContentValues/put(Ljava/lang/String;Ljava/lang/String;)V
aload 3
ldc "identity"
aload 2
invokevirtual android/content/ContentValues/put(Ljava/lang/String;Ljava/lang/String;)V
aload 3
ldc "nickname"
ldc "TeamSpeakAndroidUser"
invokevirtual android/content/ContentValues/put(Ljava/lang/String;Ljava/lang/String;)V
aload 3
ldc "defaultid"
ldc "1"
invokevirtual android/content/ContentValues/put(Ljava/lang/String;Ljava/lang/String;)V
L0:
aload 1
ldc "ident"
aconst_null
aload 3
invokevirtual android/database/sqlite/SQLiteDatabase/insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
pop2
L1:
return
L2:
astore 1
return
.limit locals 4
.limit stack 4
.end method

.method public final onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
return
.limit locals 4
.limit stack 0
.end method
