.bytecode 50.0
.class public final synchronized com/teamspeak/ts3client/data/c/c
.super java/lang/Object
.implements com/teamspeak/ts3client/data/w

.field static final 'a' Landroid/graphics/drawable/Drawable;

.field private 'b' Ljava/io/File;

.field private 'c' J

.field private 'd' Landroid/graphics/drawable/Drawable;

.field private 'e' Lcom/teamspeak/ts3client/Ts3Application;

.field private 'f' Ljava/util/ArrayList;

.method static <clinit>()V
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/getResources()Landroid/content/res/Resources;
ldc_w 2130837609
invokevirtual android/content/res/Resources/getDrawable(I)Landroid/graphics/drawable/Drawable;
putstatic com/teamspeak/ts3client/data/c/c/a Landroid/graphics/drawable/Drawable;
return
.limit locals 0
.limit stack 2
.end method

.method public <init>(JLjava/io/File;Lcom/teamspeak/ts3client/Ts3Application;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield com/teamspeak/ts3client/data/c/c/f Ljava/util/ArrayList;
aload 0
lload 1
putfield com/teamspeak/ts3client/data/c/c/c J
aload 0
aload 3
putfield com/teamspeak/ts3client/data/c/c/b Ljava/io/File;
aload 0
aload 4
putfield com/teamspeak/ts3client/data/c/c/e Lcom/teamspeak/ts3client/Ts3Application;
lload 1
lconst_0
lcmp
ifne L0
L1:
return
L0:
lload 1
ldc2_w 999L
lcmp
ifle L1
new java/io/File
dup
aload 3
new java/lang/StringBuilder
dup
ldc "icon_"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
lload 1
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
ldc ".gif"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/io/File;Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifne L1
aload 4
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/c Lcom/teamspeak/ts3client/jni/l;
aload 0
invokevirtual com/teamspeak/ts3client/jni/l/a(Lcom/teamspeak/ts3client/data/w;)V
aload 4
getfield com/teamspeak/ts3client/Ts3Application/d Ljava/util/logging/Logger;
getstatic java/util/logging/Level/INFO Ljava/util/logging/Level;
new java/lang/StringBuilder
dup
ldc "Loading Icon "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
lload 1
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
ldc " from Server"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;)V
aload 0
getfield com/teamspeak/ts3client/data/c/c/f Ljava/util/ArrayList;
aload 4
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 4
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
lconst_0
ldc ""
new java/lang/StringBuilder
dup
ldc "/icon_"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
lload 1
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
iconst_1
iconst_0
aload 0
getfield com/teamspeak/ts3client/data/c/c/b Ljava/io/File;
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
new java/lang/StringBuilder
dup
ldc "icon_"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
lload 1
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_requestFile(JJLjava/lang/String;Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
return
.limit locals 5
.limit stack 14
.end method

.method private b()J
aload 0
getfield com/teamspeak/ts3client/data/c/c/c J
lreturn
.limit locals 1
.limit stack 2
.end method

.method private c()I
aload 0
getfield com/teamspeak/ts3client/data/c/c/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
getfield android/util/DisplayMetrics/density F
ldc_w 18.0F
fmul
ldc_w 0.5F
fadd
f2i
ireturn
.limit locals 1
.limit stack 2
.end method

.method public final a()Landroid/graphics/drawable/Drawable;
aload 0
getfield com/teamspeak/ts3client/data/c/c/c J
lconst_0
lcmp
ifne L0
getstatic com/teamspeak/ts3client/data/c/c/a Landroid/graphics/drawable/Drawable;
areturn
L0:
aload 0
getfield com/teamspeak/ts3client/data/c/c/d Landroid/graphics/drawable/Drawable;
ifnull L1
aload 0
getfield com/teamspeak/ts3client/data/c/c/d Landroid/graphics/drawable/Drawable;
getstatic com/teamspeak/ts3client/data/c/c/a Landroid/graphics/drawable/Drawable;
if_acmpeq L1
aload 0
getfield com/teamspeak/ts3client/data/c/c/d Landroid/graphics/drawable/Drawable;
areturn
L1:
aload 0
getfield com/teamspeak/ts3client/data/c/c/c J
ldc2_w 999L
lcmp
ifgt L2
aload 0
getfield com/teamspeak/ts3client/data/c/c/c J
ldc2_w 100L
lcmp
ifne L3
aload 0
getfield com/teamspeak/ts3client/data/c/c/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/getResources()Landroid/content/res/Resources;
ldc_w 2130837613
invokevirtual android/content/res/Resources/getDrawable(I)Landroid/graphics/drawable/Drawable;
astore 1
L4:
aload 0
getfield com/teamspeak/ts3client/data/c/c/c J
ldc2_w 200L
lcmp
ifne L5
aload 0
getfield com/teamspeak/ts3client/data/c/c/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/getResources()Landroid/content/res/Resources;
ldc_w 2130837614
invokevirtual android/content/res/Resources/getDrawable(I)Landroid/graphics/drawable/Drawable;
astore 1
L5:
aload 0
getfield com/teamspeak/ts3client/data/c/c/c J
ldc2_w 300L
lcmp
ifne L6
aload 0
getfield com/teamspeak/ts3client/data/c/c/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/getResources()Landroid/content/res/Resources;
ldc_w 2130837615
invokevirtual android/content/res/Resources/getDrawable(I)Landroid/graphics/drawable/Drawable;
astore 1
L6:
aload 0
getfield com/teamspeak/ts3client/data/c/c/c J
ldc2_w 500L
lcmp
ifne L7
aload 0
getfield com/teamspeak/ts3client/data/c/c/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/getResources()Landroid/content/res/Resources;
ldc_w 2130837616
invokevirtual android/content/res/Resources/getDrawable(I)Landroid/graphics/drawable/Drawable;
astore 1
L7:
aload 0
getfield com/teamspeak/ts3client/data/c/c/c J
ldc2_w 600L
lcmp
ifne L8
aload 0
getfield com/teamspeak/ts3client/data/c/c/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/getResources()Landroid/content/res/Resources;
ldc_w 2130837617
invokevirtual android/content/res/Resources/getDrawable(I)Landroid/graphics/drawable/Drawable;
astore 1
L8:
aload 1
ifnonnull L9
aload 0
aload 0
getfield com/teamspeak/ts3client/data/c/c/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/getResources()Landroid/content/res/Resources;
ldc_w 2130837609
invokevirtual android/content/res/Resources/getDrawable(I)Landroid/graphics/drawable/Drawable;
putfield com/teamspeak/ts3client/data/c/c/d Landroid/graphics/drawable/Drawable;
L9:
aload 0
new android/graphics/drawable/BitmapDrawable
dup
aload 0
getfield com/teamspeak/ts3client/data/c/c/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/getResources()Landroid/content/res/Resources;
aload 1
checkcast android/graphics/drawable/BitmapDrawable
invokevirtual android/graphics/drawable/BitmapDrawable/getBitmap()Landroid/graphics/Bitmap;
aload 0
invokespecial com/teamspeak/ts3client/data/c/c/c()I
aload 0
invokespecial com/teamspeak/ts3client/data/c/c/c()I
iconst_1
invokestatic android/graphics/Bitmap/createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;
invokespecial android/graphics/drawable/BitmapDrawable/<init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V
putfield com/teamspeak/ts3client/data/c/c/d Landroid/graphics/drawable/Drawable;
aload 0
getfield com/teamspeak/ts3client/data/c/c/d Landroid/graphics/drawable/Drawable;
areturn
L2:
new java/io/File
dup
aload 0
getfield com/teamspeak/ts3client/data/c/c/b Ljava/io/File;
new java/lang/StringBuilder
dup
ldc "icon_"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
getfield com/teamspeak/ts3client/data/c/c/c J
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
ldc ".gif"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/io/File;Ljava/lang/String;)V
astore 1
aload 1
invokevirtual java/io/File/exists()Z
ifeq L10
aload 1
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
invokestatic android/graphics/drawable/Drawable/createFromPath(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
checkcast android/graphics/drawable/BitmapDrawable
invokevirtual android/graphics/drawable/BitmapDrawable/getBitmap()Landroid/graphics/Bitmap;
astore 1
aload 1
invokevirtual android/graphics/Bitmap/getHeight()I
bipush 16
if_icmpne L11
aload 1
invokevirtual android/graphics/Bitmap/getWidth()I
bipush 16
if_icmpne L11
L12:
aload 0
new android/graphics/drawable/BitmapDrawable
dup
aload 0
getfield com/teamspeak/ts3client/data/c/c/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/getResources()Landroid/content/res/Resources;
aload 1
aload 0
invokespecial com/teamspeak/ts3client/data/c/c/c()I
aload 0
invokespecial com/teamspeak/ts3client/data/c/c/c()I
iconst_1
invokestatic android/graphics/Bitmap/createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;
invokespecial android/graphics/drawable/BitmapDrawable/<init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V
putfield com/teamspeak/ts3client/data/c/c/d Landroid/graphics/drawable/Drawable;
L13:
aload 0
getfield com/teamspeak/ts3client/data/c/c/d Landroid/graphics/drawable/Drawable;
areturn
L11:
bipush 16
bipush 16
getstatic android/graphics/Bitmap$Config/ARGB_8888 Landroid/graphics/Bitmap$Config;
invokestatic android/graphics/Bitmap/createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
astore 2
new android/graphics/Canvas
dup
aload 2
invokespecial android/graphics/Canvas/<init>(Landroid/graphics/Bitmap;)V
astore 3
aload 3
aload 1
bipush 8
aload 1
invokevirtual android/graphics/Bitmap/getWidth()I
iconst_2
idiv
isub
i2f
bipush 8
aload 1
invokevirtual android/graphics/Bitmap/getHeight()I
iconst_2
idiv
isub
i2f
aconst_null
invokevirtual android/graphics/Canvas/drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V
aload 3
invokevirtual android/graphics/Canvas/save()I
pop
aload 2
astore 1
goto L12
L10:
aload 0
getstatic com/teamspeak/ts3client/data/c/c/a Landroid/graphics/drawable/Drawable;
putfield com/teamspeak/ts3client/data/c/c/d Landroid/graphics/drawable/Drawable;
goto L13
L3:
aconst_null
astore 1
goto L4
.limit locals 4
.limit stack 8
.end method

.method public final a(Lcom/teamspeak/ts3client/jni/k;)V
aload 1
instanceof com/teamspeak/ts3client/jni/events/rare/FileTransferStatus
ifeq L0
aload 1
checkcast com/teamspeak/ts3client/jni/events/rare/FileTransferStatus
getfield com/teamspeak/ts3client/jni/events/rare/FileTransferStatus/b J
ldc2_w 2065L
lcmp
ifne L0
aload 0
getfield com/teamspeak/ts3client/data/c/c/f Ljava/util/ArrayList;
aload 1
checkcast com/teamspeak/ts3client/jni/events/rare/FileTransferStatus
getfield com/teamspeak/ts3client/jni/events/rare/FileTransferStatus/a I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual java/util/ArrayList/contains(Ljava/lang/Object;)Z
ifeq L0
new java/io/File
dup
aload 0
getfield com/teamspeak/ts3client/data/c/c/b Ljava/io/File;
new java/lang/StringBuilder
dup
ldc "icon_"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
getfield com/teamspeak/ts3client/data/c/c/c J
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/io/File;Ljava/lang/String;)V
new java/io/File
dup
aload 0
getfield com/teamspeak/ts3client/data/c/c/b Ljava/io/File;
new java/lang/StringBuilder
dup
ldc "icon_"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
getfield com/teamspeak/ts3client/data/c/c/c J
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
ldc ".gif"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/io/File;Ljava/lang/String;)V
invokevirtual java/io/File/renameTo(Ljava/io/File;)Z
pop
aload 0
getfield com/teamspeak/ts3client/data/c/c/f Ljava/util/ArrayList;
aload 0
getfield com/teamspeak/ts3client/data/c/c/f Ljava/util/ArrayList;
aload 1
checkcast com/teamspeak/ts3client/jni/events/rare/FileTransferStatus
getfield com/teamspeak/ts3client/jni/events/rare/FileTransferStatus/a I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual java/util/ArrayList/indexOf(Ljava/lang/Object;)I
invokevirtual java/util/ArrayList/remove(I)Ljava/lang/Object;
pop
aload 0
getfield com/teamspeak/ts3client/data/c/c/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/k Lcom/teamspeak/ts3client/t;
invokevirtual com/teamspeak/ts3client/t/z()V
aload 0
getfield com/teamspeak/ts3client/data/c/c/f Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
ifne L0
aload 0
getfield com/teamspeak/ts3client/data/c/c/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/c Lcom/teamspeak/ts3client/jni/l;
aload 0
invokevirtual com/teamspeak/ts3client/jni/l/b(Lcom/teamspeak/ts3client/data/w;)V
L0:
return
.limit locals 2
.limit stack 7
.end method
