.bytecode 50.0
.class public final synchronized com/teamspeak/ts3client/data/d/f
.super java/lang/Object

.field public 'a' Landroid/support/v4/n/j;

.field public 'b' Lcom/teamspeak/ts3client/Ts3Application;

.method public <init>(Lcom/teamspeak/ts3client/Ts3Application;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
putfield com/teamspeak/ts3client/data/d/f/b Lcom/teamspeak/ts3client/Ts3Application;
return
.limit locals 2
.limit stack 2
.end method

.method private a(Ljava/io/File;)V
aload 1
invokevirtual java/io/File/exists()Z
ifeq L0
aload 1
invokevirtual java/io/File/isDirectory()Z
ifne L1
L0:
return
L1:
aload 1
invokevirtual java/io/File/listFiles()[Ljava/io/File;
astore 1
aload 1
arraylength
istore 3
iconst_0
istore 2
L2:
iload 2
iload 3
if_icmpge L0
aload 1
iload 2
aaload
astore 5
aload 5
invokevirtual java/io/File/isFile()Z
ifeq L3
aload 5
invokevirtual java/io/File/getName()Ljava/lang/String;
ldc ".png"
invokevirtual java/lang/String/endsWith(Ljava/lang/String;)Z
ifeq L3
aload 5
invokevirtual java/io/File/getName()Ljava/lang/String;
iconst_0
aload 5
invokevirtual java/io/File/getName()Ljava/lang/String;
bipush 46
invokevirtual java/lang/String/lastIndexOf(I)I
invokevirtual java/lang/String/substring(II)Ljava/lang/String;
astore 4
aload 5
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
invokestatic android/graphics/BitmapFactory/decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;
astore 5
aload 5
ifnull L3
bipush 16
bipush 16
getstatic android/graphics/Bitmap$Config/ARGB_8888 Landroid/graphics/Bitmap$Config;
invokestatic android/graphics/Bitmap/createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
astore 6
new android/graphics/Canvas
dup
aload 6
invokespecial android/graphics/Canvas/<init>(Landroid/graphics/Bitmap;)V
astore 7
aload 7
aload 5
bipush 8
aload 5
invokevirtual android/graphics/Bitmap/getWidth()I
iconst_2
idiv
isub
i2f
bipush 8
aload 5
invokevirtual android/graphics/Bitmap/getHeight()I
iconst_2
idiv
isub
i2f
aconst_null
invokevirtual android/graphics/Canvas/drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V
aload 7
invokevirtual android/graphics/Canvas/save()I
pop
aload 0
aload 4
new android/graphics/drawable/BitmapDrawable
dup
aload 0
getfield com/teamspeak/ts3client/data/d/f/b Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/getResources()Landroid/content/res/Resources;
aload 6
aload 0
invokespecial com/teamspeak/ts3client/data/d/f/c()I
aload 0
invokespecial com/teamspeak/ts3client/data/d/f/c()I
iconst_1
invokestatic android/graphics/Bitmap/createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;
invokespecial android/graphics/drawable/BitmapDrawable/<init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V
invokevirtual android/graphics/drawable/BitmapDrawable/getBitmap()Landroid/graphics/Bitmap;
invokespecial com/teamspeak/ts3client/data/d/f/a(Ljava/lang/String;Landroid/graphics/Bitmap;)V
L3:
iload 2
iconst_1
iadd
istore 2
goto L2
.limit locals 8
.limit stack 9
.end method

.method private a(Ljava/lang/String;Landroid/graphics/Bitmap;)V
aload 0
aload 1
invokevirtual com/teamspeak/ts3client/data/d/f/a(Ljava/lang/String;)Landroid/graphics/Bitmap;
ifnonnull L0
aload 0
getfield com/teamspeak/ts3client/data/d/f/a Landroid/support/v4/n/j;
aload 1
aload 2
invokevirtual android/support/v4/n/j/a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
L0:
return
.limit locals 3
.limit stack 3
.end method

.method private b()V
.catch java/io/IOException from L0 to L1 using L2
.catch java/io/IOException from L3 to L4 using L5
.catch all from L3 to L4 using L6
.catch all from L7 to L8 using L6
iconst_0
istore 1
aload 0
new com/teamspeak/ts3client/data/d/g
dup
aload 0
aload 0
getfield com/teamspeak/ts3client/data/d/f/b Lcom/teamspeak/ts3client/Ts3Application;
ldc "activity"
invokevirtual com/teamspeak/ts3client/Ts3Application/getSystemService(Ljava/lang/String;)Ljava/lang/Object;
checkcast android/app/ActivityManager
invokevirtual android/app/ActivityManager/getMemoryClass()I
ldc_w 1048576
imul
bipush 10
idiv
invokespecial com/teamspeak/ts3client/data/d/g/<init>(Lcom/teamspeak/ts3client/data/d/f;I)V
putfield com/teamspeak/ts3client/data/d/f/a Landroid/support/v4/n/j;
aload 0
getfield com/teamspeak/ts3client/data/d/f/b Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/getAssets()Landroid/content/res/AssetManager;
astore 3
L0:
aload 3
ldc "countries"
invokevirtual android/content/res/AssetManager/list(Ljava/lang/String;)[Ljava/lang/String;
astore 4
L1:
aload 4
arraylength
istore 2
L9:
iload 1
iload 2
if_icmpge L10
aload 4
iload 1
aaload
astore 5
L3:
aload 3
new java/lang/StringBuilder
dup
ldc "countries/"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 5
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual android/content/res/AssetManager/open(Ljava/lang/String;)Ljava/io/InputStream;
invokestatic android/graphics/BitmapFactory/decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;
astore 6
bipush 16
bipush 16
getstatic android/graphics/Bitmap$Config/ARGB_8888 Landroid/graphics/Bitmap$Config;
invokestatic android/graphics/Bitmap/createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
astore 7
new android/graphics/Canvas
dup
aload 7
invokespecial android/graphics/Canvas/<init>(Landroid/graphics/Bitmap;)V
astore 8
aload 8
aload 6
bipush 8
aload 6
invokevirtual android/graphics/Bitmap/getWidth()I
iconst_2
idiv
isub
i2f
bipush 8
aload 6
invokevirtual android/graphics/Bitmap/getHeight()I
iconst_2
idiv
isub
i2f
aconst_null
invokevirtual android/graphics/Canvas/drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V
aload 8
invokevirtual android/graphics/Canvas/save()I
pop
aload 0
aload 5
iconst_0
aload 5
bipush 46
invokevirtual java/lang/String/lastIndexOf(I)I
invokevirtual java/lang/String/substring(II)Ljava/lang/String;
new android/graphics/drawable/BitmapDrawable
dup
aload 0
getfield com/teamspeak/ts3client/data/d/f/b Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/getResources()Landroid/content/res/Resources;
aload 7
aload 0
invokespecial com/teamspeak/ts3client/data/d/f/c()I
aload 0
invokespecial com/teamspeak/ts3client/data/d/f/c()I
iconst_1
invokestatic android/graphics/Bitmap/createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;
invokespecial android/graphics/drawable/BitmapDrawable/<init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V
invokevirtual android/graphics/drawable/BitmapDrawable/getBitmap()Landroid/graphics/Bitmap;
invokespecial com/teamspeak/ts3client/data/d/f/a(Ljava/lang/String;Landroid/graphics/Bitmap;)V
L4:
iload 1
iconst_1
iadd
istore 1
goto L9
L5:
astore 5
L7:
aload 5
invokevirtual java/io/IOException/printStackTrace()V
L8:
goto L4
L6:
astore 3
aload 3
athrow
L2:
astore 3
L10:
return
.limit locals 9
.limit stack 9
.end method

.method private c()I
aload 0
getfield com/teamspeak/ts3client/data/d/f/b Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
getfield android/util/DisplayMetrics/density F
ldc_w 16.0F
fmul
ldc_w 0.5F
fadd
f2i
ireturn
.limit locals 1
.limit stack 2
.end method

.method public final a(Ljava/lang/String;)Landroid/graphics/Bitmap;
aload 0
getfield com/teamspeak/ts3client/data/d/f/a Landroid/support/v4/n/j;
aload 1
invokevirtual android/support/v4/n/j/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast android/graphics/Bitmap
areturn
.limit locals 2
.limit stack 2
.end method

.method public final a()V
.catch java/io/IOException from L0 to L1 using L2
.catch java/io/IOException from L3 to L4 using L5
.catch all from L3 to L4 using L6
.catch all from L7 to L8 using L6
aload 0
getfield com/teamspeak/ts3client/data/d/f/b Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/getAssets()Landroid/content/res/AssetManager;
astore 3
L0:
aload 3
ldc "countries"
invokevirtual android/content/res/AssetManager/list(Ljava/lang/String;)[Ljava/lang/String;
astore 4
L1:
aload 4
arraylength
istore 2
iconst_0
istore 1
L9:
iload 1
iload 2
if_icmpge L10
aload 4
iload 1
aaload
astore 5
L3:
aload 3
new java/lang/StringBuilder
dup
ldc "countries/"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 5
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual android/content/res/AssetManager/open(Ljava/lang/String;)Ljava/io/InputStream;
invokestatic android/graphics/BitmapFactory/decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;
astore 6
bipush 16
bipush 16
getstatic android/graphics/Bitmap$Config/ARGB_8888 Landroid/graphics/Bitmap$Config;
invokestatic android/graphics/Bitmap/createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
astore 7
new android/graphics/Canvas
dup
aload 7
invokespecial android/graphics/Canvas/<init>(Landroid/graphics/Bitmap;)V
astore 8
aload 8
aload 6
bipush 8
aload 6
invokevirtual android/graphics/Bitmap/getWidth()I
iconst_2
idiv
isub
i2f
bipush 8
aload 6
invokevirtual android/graphics/Bitmap/getHeight()I
iconst_2
idiv
isub
i2f
aconst_null
invokevirtual android/graphics/Canvas/drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V
aload 8
invokevirtual android/graphics/Canvas/save()I
pop
aload 0
aload 5
iconst_0
aload 5
bipush 46
invokevirtual java/lang/String/lastIndexOf(I)I
invokevirtual java/lang/String/substring(II)Ljava/lang/String;
new android/graphics/drawable/BitmapDrawable
dup
aload 0
getfield com/teamspeak/ts3client/data/d/f/b Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/getResources()Landroid/content/res/Resources;
aload 7
aload 0
invokespecial com/teamspeak/ts3client/data/d/f/c()I
aload 0
invokespecial com/teamspeak/ts3client/data/d/f/c()I
iconst_1
invokestatic android/graphics/Bitmap/createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;
invokespecial android/graphics/drawable/BitmapDrawable/<init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V
invokevirtual android/graphics/drawable/BitmapDrawable/getBitmap()Landroid/graphics/Bitmap;
invokespecial com/teamspeak/ts3client/data/d/f/a(Ljava/lang/String;Landroid/graphics/Bitmap;)V
L4:
iload 1
iconst_1
iadd
istore 1
goto L9
L5:
astore 5
L7:
aload 5
invokevirtual java/io/IOException/printStackTrace()V
L8:
goto L4
L6:
astore 3
aload 3
athrow
L2:
astore 3
L10:
return
.limit locals 9
.limit stack 9
.end method
