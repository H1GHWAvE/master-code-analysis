.bytecode 50.0
.class public final synchronized com/teamspeak/ts3client/data/d/i
.super android/os/AsyncTask

.field final synthetic 'a' Lcom/teamspeak/ts3client/data/d/h;

.method public <init>(Lcom/teamspeak/ts3client/data/d/h;)V
aload 0
aload 1
putfield com/teamspeak/ts3client/data/d/i/a Lcom/teamspeak/ts3client/data/d/h;
aload 0
invokespecial android/os/AsyncTask/<init>()V
return
.limit locals 2
.limit stack 2
.end method

.method private transient a()Ljava/lang/Boolean;
.catch java/net/SocketException from L0 to L1 using L2
.catch java/net/UnknownHostException from L0 to L1 using L3
.catch java/io/IOException from L0 to L1 using L4
iconst_1
newarray byte
astore 2
L0:
new java/net/DatagramSocket
dup
invokespecial java/net/DatagramSocket/<init>()V
astore 1
aload 1
sipush 10000
invokevirtual java/net/DatagramSocket/setSoTimeout(I)V
getstatic com/teamspeak/ts3client/data/d/h/b Ljava/lang/String;
invokestatic java/net/InetAddress/getByName(Ljava/lang/String;)Ljava/net/InetAddress;
astore 3
new java/lang/StringBuilder
dup
ldc "ip4:"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
getfield com/teamspeak/ts3client/data/d/i/a Lcom/teamspeak/ts3client/data/d/h;
invokestatic com/teamspeak/ts3client/data/d/h/a(Lcom/teamspeak/ts3client/data/d/h;)Ljava/lang/String;
invokestatic java/net/InetAddress/getByName(Ljava/lang/String;)Ljava/net/InetAddress;
invokevirtual java/net/InetAddress/getHostAddress()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/lang/String/getBytes()[B
astore 4
aload 1
new java/net/DatagramPacket
dup
aload 4
aload 4
arraylength
aload 3
getstatic com/teamspeak/ts3client/data/d/h/a I
invokespecial java/net/DatagramPacket/<init>([BILjava/net/InetAddress;I)V
invokevirtual java/net/DatagramSocket/send(Ljava/net/DatagramPacket;)V
new java/net/DatagramPacket
dup
aload 2
iconst_1
invokespecial java/net/DatagramPacket/<init>([BI)V
astore 2
aload 1
aload 2
invokevirtual java/net/DatagramSocket/receive(Ljava/net/DatagramPacket;)V
new java/lang/String
dup
aload 2
invokevirtual java/net/DatagramPacket/getData()[B
iconst_0
aload 2
invokevirtual java/net/DatagramPacket/getLength()I
invokespecial java/lang/String/<init>([BII)V
astore 2
aload 1
invokevirtual java/net/DatagramSocket/close()V
aload 2
ldc "0"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L5
L1:
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
areturn
L2:
astore 1
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
areturn
L3:
astore 1
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
areturn
L4:
astore 1
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
areturn
L5:
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
areturn
.limit locals 5
.limit stack 7
.end method

.method private a(Ljava/lang/Boolean;)V
aload 0
aload 1
invokespecial android/os/AsyncTask/onPostExecute(Ljava/lang/Object;)V
aload 1
invokevirtual java/lang/Boolean/booleanValue()Z
ifne L0
aload 0
getfield com/teamspeak/ts3client/data/d/i/a Lcom/teamspeak/ts3client/data/d/h;
invokestatic com/teamspeak/ts3client/data/d/h/b(Lcom/teamspeak/ts3client/data/d/h;)Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
ifnull L0
aload 0
getfield com/teamspeak/ts3client/data/d/i/a Lcom/teamspeak/ts3client/data/d/h;
invokestatic com/teamspeak/ts3client/data/d/h/b(Lcom/teamspeak/ts3client/data/d/h;)Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/d Ljava/util/logging/Logger;
getstatic java/util/logging/Level/INFO Ljava/util/logging/Level;
ldc "Server is blacklisted!"
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;)V
aload 0
getfield com/teamspeak/ts3client/data/d/i/a Lcom/teamspeak/ts3client/data/d/h;
invokestatic com/teamspeak/ts3client/data/d/h/b(Lcom/teamspeak/ts3client/data/d/h;)Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/k Lcom/teamspeak/ts3client/t;
ldc "blacklist.warning"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
ldc "blacklist.text"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
ldc "button.exit"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/t/a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;)V
aload 0
getfield com/teamspeak/ts3client/data/d/i/a Lcom/teamspeak/ts3client/data/d/h;
invokestatic com/teamspeak/ts3client/data/d/h/b(Lcom/teamspeak/ts3client/data/d/h;)Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/k Lcom/teamspeak/ts3client/t;
invokevirtual com/teamspeak/ts3client/t/a()V
L0:
return
.limit locals 2
.limit stack 7
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
aload 0
invokespecial com/teamspeak/ts3client/data/d/i/a()Ljava/lang/Boolean;
areturn
.limit locals 2
.limit stack 1
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
aload 1
checkcast java/lang/Boolean
astore 1
aload 0
aload 1
invokespecial android/os/AsyncTask/onPostExecute(Ljava/lang/Object;)V
aload 1
invokevirtual java/lang/Boolean/booleanValue()Z
ifne L0
aload 0
getfield com/teamspeak/ts3client/data/d/i/a Lcom/teamspeak/ts3client/data/d/h;
invokestatic com/teamspeak/ts3client/data/d/h/b(Lcom/teamspeak/ts3client/data/d/h;)Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
ifnull L0
aload 0
getfield com/teamspeak/ts3client/data/d/i/a Lcom/teamspeak/ts3client/data/d/h;
invokestatic com/teamspeak/ts3client/data/d/h/b(Lcom/teamspeak/ts3client/data/d/h;)Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/d Ljava/util/logging/Logger;
getstatic java/util/logging/Level/INFO Ljava/util/logging/Level;
ldc "Server is blacklisted!"
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;)V
aload 0
getfield com/teamspeak/ts3client/data/d/i/a Lcom/teamspeak/ts3client/data/d/h;
invokestatic com/teamspeak/ts3client/data/d/h/b(Lcom/teamspeak/ts3client/data/d/h;)Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/k Lcom/teamspeak/ts3client/t;
ldc "blacklist.warning"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
ldc "blacklist.text"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
ldc "button.exit"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/t/a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;)V
aload 0
getfield com/teamspeak/ts3client/data/d/i/a Lcom/teamspeak/ts3client/data/d/h;
invokestatic com/teamspeak/ts3client/data/d/h/b(Lcom/teamspeak/ts3client/data/d/h;)Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/k Lcom/teamspeak/ts3client/t;
invokevirtual com/teamspeak/ts3client/t/a()V
L0:
return
.limit locals 2
.limit stack 7
.end method
