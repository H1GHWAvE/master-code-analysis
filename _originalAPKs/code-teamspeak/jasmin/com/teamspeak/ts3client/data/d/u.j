.bytecode 50.0
.class public final synchronized com/teamspeak/ts3client/data/d/u
.super java/lang/Object

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static a(Landroid/content/Context;)Z
aload 0
ldc "connectivity"
invokevirtual android/content/Context/getSystemService(Ljava/lang/String;)Ljava/lang/Object;
checkcast android/net/ConnectivityManager
astore 0
aload 0
iconst_1
invokevirtual android/net/ConnectivityManager/getNetworkInfo(I)Landroid/net/NetworkInfo;
astore 1
aload 1
ifnull L0
aload 1
invokevirtual android/net/NetworkInfo/isConnected()Z
ifeq L0
iconst_1
ireturn
L0:
aload 0
iconst_0
invokevirtual android/net/ConnectivityManager/getNetworkInfo(I)Landroid/net/NetworkInfo;
astore 1
aload 1
ifnull L1
aload 1
invokevirtual android/net/NetworkInfo/isConnected()Z
ifeq L1
iconst_1
ireturn
L1:
aload 0
invokevirtual android/net/ConnectivityManager/getActiveNetworkInfo()Landroid/net/NetworkInfo;
astore 0
aload 0
ifnull L2
aload 0
invokevirtual android/net/NetworkInfo/isConnected()Z
ifeq L2
iconst_1
ireturn
L2:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method public static b(Landroid/content/Context;)Z
aload 0
ldc "connectivity"
invokevirtual android/content/Context/getSystemService(Ljava/lang/String;)Ljava/lang/Object;
checkcast android/net/ConnectivityManager
astore 0
aload 0
iconst_1
invokevirtual android/net/ConnectivityManager/getNetworkInfo(I)Landroid/net/NetworkInfo;
ifnull L0
aload 0
iconst_1
invokevirtual android/net/ConnectivityManager/getNetworkInfo(I)Landroid/net/NetworkInfo;
invokevirtual android/net/NetworkInfo/isConnected()Z
ifeq L0
iconst_0
ireturn
L0:
aload 0
iconst_0
invokevirtual android/net/ConnectivityManager/getNetworkInfo(I)Landroid/net/NetworkInfo;
ifnonnull L1
iconst_0
ireturn
L1:
aload 0
iconst_0
invokevirtual android/net/ConnectivityManager/getNetworkInfo(I)Landroid/net/NetworkInfo;
invokevirtual android/net/NetworkInfo/getType()I
ifne L2
iconst_1
ireturn
L2:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method private static c(Landroid/content/Context;)Landroid/net/NetworkInfo;
aload 0
ldc "connectivity"
invokevirtual android/content/Context/getSystemService(Ljava/lang/String;)Ljava/lang/Object;
checkcast android/net/ConnectivityManager
invokevirtual android/net/ConnectivityManager/getActiveNetworkInfo()Landroid/net/NetworkInfo;
areturn
.limit locals 1
.limit stack 2
.end method

.method private static d(Landroid/content/Context;)I
aload 0
ldc "connectivity"
invokevirtual android/content/Context/getSystemService(Ljava/lang/String;)Ljava/lang/Object;
checkcast android/net/ConnectivityManager
astore 0
aload 0
invokevirtual android/net/ConnectivityManager/getActiveNetworkInfo()Landroid/net/NetworkInfo;
ifnonnull L0
iconst_m1
ireturn
L0:
aload 0
invokevirtual android/net/ConnectivityManager/getActiveNetworkInfo()Landroid/net/NetworkInfo;
invokevirtual android/net/NetworkInfo/getType()I
ireturn
.limit locals 1
.limit stack 2
.end method

.method private static e(Landroid/content/Context;)Z
aload 0
ldc "connectivity"
invokevirtual android/content/Context/getSystemService(Ljava/lang/String;)Ljava/lang/Object;
checkcast android/net/ConnectivityManager
astore 0
aload 0
iconst_1
invokevirtual android/net/ConnectivityManager/getNetworkInfo(I)Landroid/net/NetworkInfo;
ifnonnull L0
iconst_0
ireturn
L0:
aload 0
iconst_1
invokevirtual android/net/ConnectivityManager/getNetworkInfo(I)Landroid/net/NetworkInfo;
invokevirtual android/net/NetworkInfo/getType()I
iconst_1
if_icmpne L1
iconst_1
ireturn
L1:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method private static f(Landroid/content/Context;)Z
aload 0
ldc "connectivity"
invokevirtual android/content/Context/getSystemService(Ljava/lang/String;)Ljava/lang/Object;
checkcast android/net/ConnectivityManager
astore 0
aload 0
invokevirtual android/net/ConnectivityManager/getActiveNetworkInfo()Landroid/net/NetworkInfo;
ifnonnull L0
iconst_0
ireturn
L0:
aload 0
invokevirtual android/net/ConnectivityManager/getActiveNetworkInfo()Landroid/net/NetworkInfo;
invokevirtual android/net/NetworkInfo/isAvailable()Z
ireturn
.limit locals 1
.limit stack 2
.end method

.method private static g(Landroid/content/Context;)Z
.annotation invisible Landroid/annotation/SuppressLint;
value [s = "InlinedApi" 
.end annotation
getstatic android/os/Build$VERSION/SDK_INT I
bipush 17
if_icmplt L0
aload 0
invokevirtual android/content/Context/getContentResolver()Landroid/content/ContentResolver;
ldc "airplane_mode_on"
iconst_0
invokestatic android/provider/Settings$System/getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I
ifeq L1
L2:
iconst_1
ireturn
L1:
iconst_0
ireturn
L0:
aload 0
invokevirtual android/content/Context/getContentResolver()Landroid/content/ContentResolver;
ldc "airplane_mode_on"
iconst_0
invokestatic android/provider/Settings$System/getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I
ifne L2
iconst_0
ireturn
.limit locals 1
.limit stack 3
.end method
