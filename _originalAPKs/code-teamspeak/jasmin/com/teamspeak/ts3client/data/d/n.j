.bytecode 50.0
.class public final synchronized com/teamspeak/ts3client/data/d/n
.super android/text/method/LinkMovementMethod

.field private static 'a' Lcom/teamspeak/ts3client/data/d/n;

.method static <clinit>()V
new com/teamspeak/ts3client/data/d/n
dup
invokespecial com/teamspeak/ts3client/data/d/n/<init>()V
putstatic com/teamspeak/ts3client/data/d/n/a Lcom/teamspeak/ts3client/data/d/n;
return
.limit locals 0
.limit stack 2
.end method

.method public <init>()V
aload 0
invokespecial android/text/method/LinkMovementMethod/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static a()Landroid/text/method/MovementMethod;
getstatic com/teamspeak/ts3client/data/d/n/a Lcom/teamspeak/ts3client/data/d/n;
areturn
.limit locals 0
.limit stack 1
.end method

.method private a(Landroid/text/style/URLSpan;)V
new com/teamspeak/ts3client/data/ab
dup
invokespecial com/teamspeak/ts3client/data/ab/<init>()V
astore 4
aload 1
invokevirtual android/text/style/URLSpan/getURL()Ljava/lang/String;
invokestatic android/net/Uri/parse(Ljava/lang/String;)Landroid/net/Uri;
astore 1
aload 1
ifnull L0
aload 1
invokevirtual android/net/Uri/getQuery()Ljava/lang/String;
ifnonnull L1
aload 4
aload 1
invokevirtual android/net/Uri/getHost()Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/data/ab/a(Ljava/lang/String;)V
L2:
new android/app/AlertDialog$Builder
dup
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/c Landroid/support/v4/app/Fragment;
invokevirtual android/support/v4/app/Fragment/i()Landroid/support/v4/app/bb;
invokespecial android/app/AlertDialog$Builder/<init>(Landroid/content/Context;)V
invokevirtual android/app/AlertDialog$Builder/create()Landroid/app/AlertDialog;
astore 5
aload 5
ldc "TS3 URL"
invokevirtual android/app/AlertDialog/setTitle(Ljava/lang/CharSequence;)V
aload 5
new java/lang/StringBuilder
dup
ldc "Found the following Host:"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 4
getfield com/teamspeak/ts3client/data/ab/c Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual android/app/AlertDialog/setMessage(Ljava/lang/CharSequence;)V
aload 5
bipush -3
ldc "Bookmark"
new com/teamspeak/ts3client/data/d/o
dup
aload 0
aload 1
aload 5
invokespecial com/teamspeak/ts3client/data/d/o/<init>(Lcom/teamspeak/ts3client/data/d/n;Landroid/net/Uri;Landroid/app/AlertDialog;)V
invokevirtual android/app/AlertDialog/setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V
aload 5
bipush -2
ldc "Cancel"
new com/teamspeak/ts3client/data/d/p
dup
aload 0
aload 5
invokespecial com/teamspeak/ts3client/data/d/p/<init>(Lcom/teamspeak/ts3client/data/d/n;Landroid/app/AlertDialog;)V
invokevirtual android/app/AlertDialog/setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V
aload 5
iconst_0
invokevirtual android/app/AlertDialog/setCancelable(Z)V
aload 5
invokevirtual android/app/AlertDialog/show()V
L0:
return
L1:
new java/util/HashMap
dup
invokespecial java/util/HashMap/<init>()V
astore 5
aload 1
invokevirtual android/net/Uri/getQuery()Ljava/lang/String;
ldc "&"
invokevirtual java/lang/String/split(Ljava/lang/String;)[Ljava/lang/String;
astore 6
aload 6
arraylength
istore 3
iconst_0
istore 2
L3:
iload 2
iload 3
if_icmpge L4
aload 6
iload 2
aaload
ldc "="
invokevirtual java/lang/String/split(Ljava/lang/String;)[Ljava/lang/String;
astore 7
aload 7
arraylength
iconst_1
if_icmple L5
aload 5
aload 7
iconst_0
aaload
aload 7
iconst_1
aaload
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
L6:
iload 2
iconst_1
iadd
istore 2
goto L3
L5:
aload 5
aload 7
iconst_0
aaload
ldc ""
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
goto L6
L4:
aload 4
aload 1
invokevirtual android/net/Uri/getHost()Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/data/ab/a(Ljava/lang/String;)V
aload 5
ldc "port"
invokevirtual java/util/HashMap/containsKey(Ljava/lang/Object;)Z
ifeq L7
aload 4
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 4
getfield com/teamspeak/ts3client/data/ab/c Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ":"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 5
ldc "port"
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/String
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/data/ab/a(Ljava/lang/String;)V
L7:
aload 5
ldc "nickname"
invokevirtual java/util/HashMap/containsKey(Ljava/lang/Object;)Z
ifeq L8
aload 4
aload 5
ldc "nickname"
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/String
putfield com/teamspeak/ts3client/data/ab/f Ljava/lang/String;
L8:
aload 5
ldc "password"
invokevirtual java/util/HashMap/containsKey(Ljava/lang/Object;)Z
ifeq L9
aload 4
aload 5
ldc "password"
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/String
putfield com/teamspeak/ts3client/data/ab/e Ljava/lang/String;
L9:
aload 5
ldc "channel"
invokevirtual java/util/HashMap/containsKey(Ljava/lang/Object;)Z
ifeq L10
aload 4
aload 5
ldc "channel"
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/String
putfield com/teamspeak/ts3client/data/ab/g Ljava/lang/String;
L10:
aload 5
ldc "channelpassword"
invokevirtual java/util/HashMap/containsKey(Ljava/lang/Object;)Z
ifeq L11
aload 4
aload 5
ldc "channelpassword"
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/String
putfield com/teamspeak/ts3client/data/ab/h Ljava/lang/String;
L11:
aload 5
ldc "addbookmark"
invokevirtual java/util/HashMap/containsKey(Ljava/lang/Object;)Z
ifeq L12
aload 4
aload 5
ldc "addbookmark"
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/String
putfield com/teamspeak/ts3client/data/ab/a Ljava/lang/String;
L12:
aload 5
ldc "token"
invokevirtual java/util/HashMap/containsKey(Ljava/lang/Object;)Z
ifeq L2
aload 4
aload 5
ldc "token"
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/String
putfield com/teamspeak/ts3client/data/ab/i Ljava/lang/String;
goto L2
.limit locals 8
.limit stack 8
.end method

.method public final onTouchEvent(Landroid/widget/TextView;Landroid/text/Spannable;Landroid/view/MotionEvent;)Z
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L2
.catch android/content/ActivityNotFoundException from L5 to L6 using L7
.catch java/lang/Exception from L8 to L9 using L10
aload 3
invokevirtual android/view/MotionEvent/getAction()I
istore 4
iload 4
iconst_1
if_icmpeq L11
iload 4
ifne L12
L11:
aload 3
invokevirtual android/view/MotionEvent/getX()F
f2i
istore 5
aload 3
invokevirtual android/view/MotionEvent/getY()F
f2i
istore 6
aload 1
invokevirtual android/widget/TextView/getTotalPaddingLeft()I
istore 7
aload 1
invokevirtual android/widget/TextView/getTotalPaddingTop()I
istore 8
aload 1
invokevirtual android/widget/TextView/getScrollX()I
istore 9
aload 1
invokevirtual android/widget/TextView/getScrollY()I
istore 10
aload 1
invokevirtual android/widget/TextView/getLayout()Landroid/text/Layout;
astore 3
aload 3
aload 3
iload 6
iload 8
isub
iload 10
iadd
invokevirtual android/text/Layout/getLineForVertical(I)I
iload 5
iload 7
isub
iload 9
iadd
i2f
invokevirtual android/text/Layout/getOffsetForHorizontal(IF)I
istore 5
aload 2
iload 5
iload 5
ldc android/text/style/ClickableSpan
invokeinterface android/text/Spannable/getSpans(IILjava/lang/Class;)[Ljava/lang/Object; 3
checkcast [Landroid/text/style/ClickableSpan;
astore 3
aload 3
arraylength
ifeq L13
iload 4
iconst_1
if_icmpne L14
aload 3
iconst_0
aaload
instanceof android/text/style/URLSpan
ifeq L15
aload 3
iconst_0
aaload
checkcast android/text/style/URLSpan
astore 2
aload 2
invokevirtual android/text/style/URLSpan/getURL()Ljava/lang/String;
ldc "client://"
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifne L16
aload 2
invokevirtual android/text/style/URLSpan/getURL()Ljava/lang/String;
ldc "ts3file://"
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifne L16
aload 2
invokevirtual android/text/style/URLSpan/getURL()Ljava/lang/String;
ldc "ts3server://"
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifeq L17
new com/teamspeak/ts3client/data/ab
dup
invokespecial com/teamspeak/ts3client/data/ab/<init>()V
astore 1
aload 2
invokevirtual android/text/style/URLSpan/getURL()Ljava/lang/String;
invokestatic android/net/Uri/parse(Ljava/lang/String;)Landroid/net/Uri;
astore 2
aload 2
ifnull L16
aload 2
invokevirtual android/net/Uri/getQuery()Ljava/lang/String;
ifnonnull L18
aload 1
aload 2
invokevirtual android/net/Uri/getHost()Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/data/ab/a(Ljava/lang/String;)V
L19:
new android/app/AlertDialog$Builder
dup
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/c Landroid/support/v4/app/Fragment;
invokevirtual android/support/v4/app/Fragment/i()Landroid/support/v4/app/bb;
invokespecial android/app/AlertDialog$Builder/<init>(Landroid/content/Context;)V
invokevirtual android/app/AlertDialog$Builder/create()Landroid/app/AlertDialog;
astore 3
aload 3
ldc "TS3 URL"
invokevirtual android/app/AlertDialog/setTitle(Ljava/lang/CharSequence;)V
aload 3
new java/lang/StringBuilder
dup
ldc "Found the following Host:"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
getfield com/teamspeak/ts3client/data/ab/c Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual android/app/AlertDialog/setMessage(Ljava/lang/CharSequence;)V
aload 3
bipush -3
ldc "Bookmark"
new com/teamspeak/ts3client/data/d/o
dup
aload 0
aload 2
aload 3
invokespecial com/teamspeak/ts3client/data/d/o/<init>(Lcom/teamspeak/ts3client/data/d/n;Landroid/net/Uri;Landroid/app/AlertDialog;)V
invokevirtual android/app/AlertDialog/setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V
aload 3
bipush -2
ldc "Cancel"
new com/teamspeak/ts3client/data/d/p
dup
aload 0
aload 3
invokespecial com/teamspeak/ts3client/data/d/p/<init>(Lcom/teamspeak/ts3client/data/d/n;Landroid/app/AlertDialog;)V
invokevirtual android/app/AlertDialog/setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V
aload 3
iconst_0
invokevirtual android/app/AlertDialog/setCancelable(Z)V
aload 3
invokevirtual android/app/AlertDialog/show()V
L16:
iconst_1
ireturn
L18:
new java/util/HashMap
dup
invokespecial java/util/HashMap/<init>()V
astore 3
aload 2
invokevirtual android/net/Uri/getQuery()Ljava/lang/String;
ldc "&"
invokevirtual java/lang/String/split(Ljava/lang/String;)[Ljava/lang/String;
astore 13
aload 13
arraylength
istore 5
iconst_0
istore 4
L20:
iload 4
iload 5
if_icmpge L21
aload 13
iload 4
aaload
ldc "="
invokevirtual java/lang/String/split(Ljava/lang/String;)[Ljava/lang/String;
astore 14
aload 14
arraylength
iconst_1
if_icmple L22
aload 3
aload 14
iconst_0
aaload
aload 14
iconst_1
aaload
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
L23:
iload 4
iconst_1
iadd
istore 4
goto L20
L22:
aload 3
aload 14
iconst_0
aaload
ldc ""
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
goto L23
L21:
aload 1
aload 2
invokevirtual android/net/Uri/getHost()Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/data/ab/a(Ljava/lang/String;)V
aload 3
ldc "port"
invokevirtual java/util/HashMap/containsKey(Ljava/lang/Object;)Z
ifeq L24
aload 1
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 1
getfield com/teamspeak/ts3client/data/ab/c Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ":"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
ldc "port"
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/String
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/data/ab/a(Ljava/lang/String;)V
L24:
aload 3
ldc "nickname"
invokevirtual java/util/HashMap/containsKey(Ljava/lang/Object;)Z
ifeq L25
aload 1
aload 3
ldc "nickname"
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/String
putfield com/teamspeak/ts3client/data/ab/f Ljava/lang/String;
L25:
aload 3
ldc "password"
invokevirtual java/util/HashMap/containsKey(Ljava/lang/Object;)Z
ifeq L26
aload 1
aload 3
ldc "password"
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/String
putfield com/teamspeak/ts3client/data/ab/e Ljava/lang/String;
L26:
aload 3
ldc "channel"
invokevirtual java/util/HashMap/containsKey(Ljava/lang/Object;)Z
ifeq L27
aload 1
aload 3
ldc "channel"
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/String
putfield com/teamspeak/ts3client/data/ab/g Ljava/lang/String;
L27:
aload 3
ldc "channelpassword"
invokevirtual java/util/HashMap/containsKey(Ljava/lang/Object;)Z
ifeq L28
aload 1
aload 3
ldc "channelpassword"
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/String
putfield com/teamspeak/ts3client/data/ab/h Ljava/lang/String;
L28:
aload 3
ldc "addbookmark"
invokevirtual java/util/HashMap/containsKey(Ljava/lang/Object;)Z
ifeq L29
aload 1
aload 3
ldc "addbookmark"
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/String
putfield com/teamspeak/ts3client/data/ab/a Ljava/lang/String;
L29:
aload 3
ldc "token"
invokevirtual java/util/HashMap/containsKey(Ljava/lang/Object;)Z
ifeq L19
aload 1
aload 3
ldc "token"
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/String
putfield com/teamspeak/ts3client/data/ab/i Ljava/lang/String;
goto L19
L17:
aload 2
invokevirtual android/text/style/URLSpan/getURL()Ljava/lang/String;
ldc "channelid://"
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifeq L30
L0:
aload 2
invokevirtual android/text/style/URLSpan/getURL()Ljava/lang/String;
bipush 12
invokevirtual java/lang/String/substring(I)Ljava/lang/String;
invokestatic java/lang/Long/parseLong(Ljava/lang/String;)J
lstore 11
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
astore 1
L1:
aload 1
ifnull L4
L3:
aload 1
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
lload 11
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/d(Ljava/lang/Long;)Z
ifeq L4
new com/teamspeak/ts3client/d/a/a
dup
aload 1
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
lload 11
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
iconst_0
invokespecial com/teamspeak/ts3client/d/a/a/<init>(Lcom/teamspeak/ts3client/data/a;Z)V
aload 1
getfield com/teamspeak/ts3client/data/e/k Lcom/teamspeak/ts3client/t;
getfield android/support/v4/app/Fragment/M Landroid/support/v4/app/bl;
ldc "ChannelActionDialog"
invokevirtual com/teamspeak/ts3client/d/a/a/a(Landroid/support/v4/app/bi;Ljava/lang/String;)V
L4:
iconst_1
ireturn
L30:
aload 2
invokevirtual android/text/style/URLSpan/getURL()Ljava/lang/String;
astore 3
aload 3
astore 2
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 3
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_bbcode_shouldPrependHTTP(Ljava/lang/String;)Z
ifeq L5
new java/lang/StringBuilder
dup
ldc "http://"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 2
L5:
aload 2
invokestatic android/net/Uri/parse(Ljava/lang/String;)Landroid/net/Uri;
astore 2
aload 1
invokevirtual android/widget/TextView/getContext()Landroid/content/Context;
astore 1
new android/content/Intent
dup
ldc "android.intent.action.VIEW"
aload 2
invokespecial android/content/Intent/<init>(Ljava/lang/String;Landroid/net/Uri;)V
astore 2
aload 2
ldc "com.android.browser.application_id"
aload 1
invokevirtual android/content/Context/getPackageName()Ljava/lang/String;
invokevirtual android/content/Intent/putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
pop
aload 1
aload 2
invokevirtual android/content/Context/startActivity(Landroid/content/Intent;)V
L6:
goto L16
L7:
astore 1
goto L16
L15:
aload 3
iconst_0
aaload
aload 1
invokevirtual android/text/style/ClickableSpan/onClick(Landroid/view/View;)V
L14:
iconst_1
ireturn
L13:
iload 4
ifne L14
L8:
aload 2
aload 2
aload 3
iconst_0
aaload
invokeinterface android/text/Spannable/getSpanStart(Ljava/lang/Object;)I 1
aload 2
aload 3
iconst_0
aaload
invokeinterface android/text/Spannable/getSpanEnd(Ljava/lang/Object;)I 1
invokestatic android/text/Selection/setSelection(Landroid/text/Spannable;II)V
L9:
goto L14
L10:
astore 1
goto L14
L12:
aload 2
invokestatic android/text/Selection/removeSelection(Landroid/text/Spannable;)V
aload 0
aload 1
aload 2
aload 3
invokespecial android/text/method/LinkMovementMethod/onTouchEvent(Landroid/widget/TextView;Landroid/text/Spannable;Landroid/view/MotionEvent;)Z
ireturn
L2:
astore 1
goto L16
.limit locals 15
.limit stack 8
.end method
