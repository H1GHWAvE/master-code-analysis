.bytecode 50.0
.class public final synchronized com/teamspeak/ts3client/data/e
.super java/lang/Object
.annotation invisible Landroid/annotation/SuppressLint;
value [s = "UseSparseArrays" 
.end annotation

.field private static 'A' Ljava/util/regex/Pattern;

.field private 'B' Lcom/teamspeak/ts3client/data/b;

.field private 'C' Ljava/lang/String;

.field private 'D' I

.field private 'E' I

.field public 'a' Lcom/teamspeak/ts3client/jni/Ts3Jni;

.field public 'b' Lcom/teamspeak/ts3client/a/k;

.field public 'c' Lcom/teamspeak/ts3client/jni/l;

.field public 'd' Lcom/teamspeak/ts3client/data/d;

.field public 'e' J

.field public 'f' Lcom/teamspeak/ts3client/data/g;

.field public 'g' J

.field public 'h' I

.field public 'i' Ljava/util/HashMap;

.field public 'j' Ljava/util/HashMap;

.field public 'k' Lcom/teamspeak/ts3client/t;

.field public 'l' Lcom/teamspeak/ts3client/ConnectionBackground;

.field public 'm' Lcom/teamspeak/ts3client/Ts3Application;

.field public 'n' I

.field public 'o' Lcom/teamspeak/ts3client/data/g/b;

.field public 'p' Lcom/teamspeak/ts3client/data/a/b;

.field public 'q' Ljava/lang/String;

.field public 'r' Lcom/teamspeak/ts3client/data/c/d;

.field public 's' Lcom/teamspeak/ts3client/data/f/a;

.field public 't' Ljava/lang/String;

.field public 'u' Lcom/teamspeak/ts3client/data/d/v;

.field public 'v' Lcom/teamspeak/ts3client/data/ab;

.field public 'w' Lcom/a/b/d/bw;

.field public 'x' I

.field public 'y' I

.field public 'z' J

.method static <clinit>()V
ldc "(((\\\\/)*[^/])*)"
invokestatic java/util/regex/Pattern/compile(Ljava/lang/String;)Ljava/util/regex/Pattern;
putstatic com/teamspeak/ts3client/data/e/A Ljava/util/regex/Pattern;
return
.limit locals 0
.limit stack 1
.end method

.method private <init>(Landroid/content/Context;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
lconst_0
putfield com/teamspeak/ts3client/data/e/g J
aload 0
iconst_0
putfield com/teamspeak/ts3client/data/e/h I
aload 0
new java/util/HashMap
dup
invokespecial java/util/HashMap/<init>()V
putfield com/teamspeak/ts3client/data/e/j Ljava/util/HashMap;
aload 0
aconst_null
putfield com/teamspeak/ts3client/data/e/k Lcom/teamspeak/ts3client/t;
aload 0
iconst_0
putfield com/teamspeak/ts3client/data/e/n I
aload 0
invokestatic com/a/b/d/hy/a()Lcom/a/b/d/hy;
putfield com/teamspeak/ts3client/data/e/w Lcom/a/b/d/bw;
aload 0
aload 1
invokevirtual android/content/Context/getApplicationContext()Landroid/content/Context;
checkcast com/teamspeak/ts3client/Ts3Application
putfield com/teamspeak/ts3client/data/e/m Lcom/teamspeak/ts3client/Ts3Application;
aload 0
invokestatic com/teamspeak/ts3client/jni/Ts3Jni/b()Lcom/teamspeak/ts3client/jni/Ts3Jni;
putfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
invokestatic com/teamspeak/ts3client/a/k/a()Lcom/teamspeak/ts3client/a/k;
putfield com/teamspeak/ts3client/data/e/b Lcom/teamspeak/ts3client/a/k;
aload 0
getfield com/teamspeak/ts3client/data/e/b Lcom/teamspeak/ts3client/a/k;
aload 0
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
putfield com/teamspeak/ts3client/a/k/d Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
invokespecial com/teamspeak/ts3client/data/e/E()V
aload 0
invokestatic com/teamspeak/ts3client/jni/l/a()Lcom/teamspeak/ts3client/jni/l;
putfield com/teamspeak/ts3client/data/e/c Lcom/teamspeak/ts3client/jni/l;
aload 0
new com/teamspeak/ts3client/data/b
dup
invokespecial com/teamspeak/ts3client/data/b/<init>()V
putfield com/teamspeak/ts3client/data/e/B Lcom/teamspeak/ts3client/data/b;
aload 0
new com/teamspeak/ts3client/data/d
dup
invokespecial com/teamspeak/ts3client/data/d/<init>()V
putfield com/teamspeak/ts3client/data/e/d Lcom/teamspeak/ts3client/data/d;
aload 0
new com/teamspeak/ts3client/data/g/b
dup
invokespecial com/teamspeak/ts3client/data/g/b/<init>()V
putfield com/teamspeak/ts3client/data/e/o Lcom/teamspeak/ts3client/data/g/b;
aload 0
new com/teamspeak/ts3client/data/a/b
dup
invokespecial com/teamspeak/ts3client/data/a/b/<init>()V
putfield com/teamspeak/ts3client/data/e/p Lcom/teamspeak/ts3client/data/a/b;
aload 0
new java/util/HashMap
dup
invokespecial java/util/HashMap/<init>()V
putfield com/teamspeak/ts3client/data/e/i Ljava/util/HashMap;
return
.limit locals 2
.limit stack 3
.end method

.method public <init>(Ljava/lang/String;Landroid/content/Context;Lcom/teamspeak/ts3client/data/ab;)V
aload 0
aload 2
invokespecial com/teamspeak/ts3client/data/e/<init>(Landroid/content/Context;)V
aload 0
aload 1
putfield com/teamspeak/ts3client/data/e/C Ljava/lang/String;
aload 0
aload 3
putfield com/teamspeak/ts3client/data/e/v Lcom/teamspeak/ts3client/data/ab;
return
.limit locals 4
.limit stack 2
.end method

.method private A()Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/data/e/q Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method private B()I
aload 0
getfield com/teamspeak/ts3client/data/e/m Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/d Ljava/util/logging/Logger;
getstatic java/util/logging/Level/INFO Ljava/util/logging/Level;
ldc "Loading lib"
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;)V
invokestatic com/teamspeak/ts3client/jni/Ts3Jni/a()I
istore 1
iload 1
ifle L0
iload 1
ireturn
L0:
aload 0
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/data/e/y I
aload 0
getfield com/teamspeak/ts3client/data/e/x I
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_prepareAudioDevice(II)I
pop
aload 0
new com/teamspeak/ts3client/data/f/a
dup
aload 0
getfield com/teamspeak/ts3client/data/e/m Lcom/teamspeak/ts3client/Ts3Application;
invokespecial com/teamspeak/ts3client/data/f/a/<init>(Lcom/teamspeak/ts3client/Ts3Application;)V
putfield com/teamspeak/ts3client/data/e/s Lcom/teamspeak/ts3client/data/f/a;
aload 0
aload 0
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_spawnNewServerConnectionHandler()J
putfield com/teamspeak/ts3client/data/e/e J
iload 1
ireturn
.limit locals 2
.limit stack 4
.end method

.method private C()V
aload 0
getfield com/teamspeak/ts3client/data/e/b Lcom/teamspeak/ts3client/a/k;
invokevirtual com/teamspeak/ts3client/a/k/b()V
new com/teamspeak/ts3client/data/f
dup
aload 0
invokespecial com/teamspeak/ts3client/data/f/<init>(Lcom/teamspeak/ts3client/data/e;)V
astore 1
new android/os/Handler
dup
invokespecial android/os/Handler/<init>()V
aload 1
ldc2_w 3000L
invokevirtual android/os/Handler/postDelayed(Ljava/lang/Runnable;J)Z
pop
return
.limit locals 2
.limit stack 4
.end method

.method private D()V
aload 0
getfield com/teamspeak/ts3client/data/e/m Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/d Ljava/util/logging/Logger;
getstatic java/util/logging/Level/INFO Ljava/util/logging/Level;
ldc "Stop AUDIO"
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;)V
aload 0
getfield com/teamspeak/ts3client/data/e/b Lcom/teamspeak/ts3client/a/k;
invokevirtual com/teamspeak/ts3client/a/k/f()V
aload 0
getfield com/teamspeak/ts3client/data/e/b Lcom/teamspeak/ts3client/a/k;
invokevirtual com/teamspeak/ts3client/a/k/e()V
return
.limit locals 1
.limit stack 3
.end method

.method private E()V
aload 0
aload 0
getfield com/teamspeak/ts3client/data/e/m Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
ldc "playbackStream"
iconst_0
invokeinterface android/content/SharedPreferences/getInt(Ljava/lang/String;I)I 2
putfield com/teamspeak/ts3client/data/e/D I
aload 0
aload 0
getfield com/teamspeak/ts3client/data/e/m Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
ldc "recordStream"
iconst_0
invokeinterface android/content/SharedPreferences/getInt(Ljava/lang/String;I)I 2
putfield com/teamspeak/ts3client/data/e/E I
aload 0
aload 0
getfield com/teamspeak/ts3client/data/e/m Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
ldc "samplePlay"
aload 0
getfield com/teamspeak/ts3client/data/e/m Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/i()I
invokeinterface android/content/SharedPreferences/getInt(Ljava/lang/String;I)I 2
putfield com/teamspeak/ts3client/data/e/x I
aload 0
aload 0
getfield com/teamspeak/ts3client/data/e/m Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
ldc "sampleRec"
aload 0
getfield com/teamspeak/ts3client/data/e/m Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/h()I
invokeinterface android/content/SharedPreferences/getInt(Ljava/lang/String;I)I 2
putfield com/teamspeak/ts3client/data/e/y I
aload 0
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/data/e/y I
aload 0
getfield com/teamspeak/ts3client/data/e/x I
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_prepareAudioDevice(II)I
pop
aload 0
getfield com/teamspeak/ts3client/data/e/b Lcom/teamspeak/ts3client/a/k;
aload 0
getfield com/teamspeak/ts3client/data/e/y I
aload 0
getfield com/teamspeak/ts3client/data/e/x I
aload 0
getfield com/teamspeak/ts3client/data/e/D I
aload 0
getfield com/teamspeak/ts3client/data/e/E I
invokevirtual com/teamspeak/ts3client/a/k/a(IIII)Z
pop
return
.limit locals 1
.limit stack 5
.end method

.method private F()J
aload 0
getfield com/teamspeak/ts3client/data/e/z J
lreturn
.limit locals 1
.limit stack 2
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/data/e;)Lcom/teamspeak/ts3client/a/k;
aload 0
getfield com/teamspeak/ts3client/data/e/b Lcom/teamspeak/ts3client/a/k;
areturn
.limit locals 1
.limit stack 1
.end method

.method private a(I)V
aload 0
iload 1
putfield com/teamspeak/ts3client/data/e/n I
aload 0
getfield com/teamspeak/ts3client/data/e/n I
sipush 519
if_icmpne L0
invokestatic com/teamspeak/ts3client/data/b/f/a()Lcom/teamspeak/ts3client/data/b/f;
invokevirtual com/teamspeak/ts3client/data/b/f/b()V
aload 0
getfield com/teamspeak/ts3client/data/e/m Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/k Lcom/teamspeak/ts3client/t;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
getfield com/teamspeak/ts3client/data/e/m Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/getApplicationContext()Landroid/content/Context;
ldc_w 2131034152
invokevirtual android/content/Context/getString(I)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/data/e/m Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/n I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/data/e/m Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/getApplicationContext()Landroid/content/Context;
ldc_w 2131034151
invokevirtual android/content/Context/getString(I)Ljava/lang/String;
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
aload 0
getfield com/teamspeak/ts3client/data/e/m Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/getApplicationContext()Landroid/content/Context;
ldc_w 2131034142
invokevirtual android/content/Context/getString(I)Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/t/a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;)V
L0:
aload 0
getfield com/teamspeak/ts3client/data/e/m Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/d Ljava/util/logging/Logger;
getstatic java/util/logging/Level/INFO Ljava/util/logging/Level;
new java/lang/StringBuilder
dup
ldc "ClientLib Connect_return:"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
iload 1
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;)V
return
.limit locals 2
.limit stack 8
.end method

.method private a(Lcom/teamspeak/ts3client/ConnectionBackground;)V
aload 0
aload 1
putfield com/teamspeak/ts3client/data/e/l Lcom/teamspeak/ts3client/ConnectionBackground;
return
.limit locals 2
.limit stack 2
.end method

.method private a(Lcom/teamspeak/ts3client/data/g;)V
aload 0
aload 1
putfield com/teamspeak/ts3client/data/e/f Lcom/teamspeak/ts3client/data/g;
return
.limit locals 2
.limit stack 2
.end method

.method private a(Lcom/teamspeak/ts3client/t;)V
aload 0
aload 1
putfield com/teamspeak/ts3client/data/e/k Lcom/teamspeak/ts3client/t;
return
.limit locals 2
.limit stack 2
.end method

.method private b(I)V
aload 0
iload 1
putfield com/teamspeak/ts3client/data/e/h I
return
.limit locals 2
.limit stack 2
.end method

.method private b(J)V
aload 0
lload 1
putfield com/teamspeak/ts3client/data/e/z J
return
.limit locals 3
.limit stack 3
.end method

.method private c(Ljava/lang/String;)V
invokestatic com/teamspeak/ts3client/data/d/q/a()V
invokestatic com/teamspeak/ts3client/data/v/a()Lcom/teamspeak/ts3client/data/v;
iconst_0
putfield com/teamspeak/ts3client/data/v/b Z
aload 0
getfield com/teamspeak/ts3client/data/e/m Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/f Lcom/teamspeak/ts3client/StartGUIFragment;
getfield com/teamspeak/ts3client/StartGUIFragment/o Lcom/teamspeak/ts3client/bookmark/e;
getfield com/teamspeak/ts3client/bookmark/e/b Lcom/teamspeak/ts3client/data/b/a;
aload 0
getfield com/teamspeak/ts3client/data/e/v Lcom/teamspeak/ts3client/data/ab;
getfield com/teamspeak/ts3client/data/ab/l J
aload 0
getfield com/teamspeak/ts3client/data/e/v Lcom/teamspeak/ts3client/data/ab;
invokevirtual com/teamspeak/ts3client/data/b/a/a(JLcom/teamspeak/ts3client/data/ab;)Z
pop
aload 0
getfield com/teamspeak/ts3client/data/e/m Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/d Ljava/util/logging/Logger;
getstatic java/util/logging/Level/INFO Ljava/util/logging/Level;
ldc "Disconnecting from Server"
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;)V
aload 0
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/data/e/e J
aload 1
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_stopConnection(JLjava/lang/String;)I
pop
aload 0
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/data/e/e J
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_closeCaptureDevice(J)I
pop
aload 0
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/data/e/e J
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_closePlaybackDevice(J)I
pop
aload 0
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/data/e/e J
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_destroyServerConnectionHandler(J)I
pop
aload 0
getfield com/teamspeak/ts3client/data/e/m Lcom/teamspeak/ts3client/Ts3Application;
aload 0
getfield com/teamspeak/ts3client/data/e/m Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/k Landroid/content/Intent;
invokevirtual com/teamspeak/ts3client/Ts3Application/stopService(Landroid/content/Intent;)Z
pop
aload 0
getfield com/teamspeak/ts3client/data/e/b Lcom/teamspeak/ts3client/a/k;
invokevirtual com/teamspeak/ts3client/a/k/b()V
new com/teamspeak/ts3client/data/f
dup
aload 0
invokespecial com/teamspeak/ts3client/data/f/<init>(Lcom/teamspeak/ts3client/data/e;)V
astore 1
new android/os/Handler
dup
invokespecial android/os/Handler/<init>()V
aload 1
ldc2_w 3000L
invokevirtual android/os/Handler/postDelayed(Ljava/lang/Runnable;J)Z
pop
aload 0
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
ldc ""
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_unregisterCustomDevice(Ljava/lang/String;)I
pop
aload 0
aconst_null
putfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/data/e/m Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/getBaseContext()Landroid/content/Context;
invokestatic com/teamspeak/ts3client/chat/d/a(Landroid/content/Context;)Lcom/teamspeak/ts3client/chat/d;
astore 1
aload 1
aconst_null
putfield com/teamspeak/ts3client/chat/d/c Lcom/teamspeak/ts3client/chat/a;
aload 1
aconst_null
putfield com/teamspeak/ts3client/chat/d/d Lcom/teamspeak/ts3client/chat/a;
aload 1
getfield com/teamspeak/ts3client/chat/d/a Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/clear()V
aload 1
getfield com/teamspeak/ts3client/chat/d/b Ljava/util/HashMap;
invokevirtual java/util/HashMap/clear()V
aload 1
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield com/teamspeak/ts3client/chat/d/a Ljava/util/ArrayList;
aload 1
new java/util/HashMap
dup
invokespecial java/util/HashMap/<init>()V
putfield com/teamspeak/ts3client/chat/d/b Ljava/util/HashMap;
aload 0
getfield com/teamspeak/ts3client/data/e/m Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/d Ljava/util/logging/Logger;
getstatic java/util/logging/Level/INFO Ljava/util/logging/Level;
ldc "ClientLib closed"
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;)V
return
.limit locals 2
.limit stack 4
.end method

.method private d(Ljava/lang/String;)V
aload 0
aload 1
putfield com/teamspeak/ts3client/data/e/t Ljava/lang/String;
return
.limit locals 2
.limit stack 2
.end method

.method private s()Lcom/teamspeak/ts3client/a/k;
aload 0
getfield com/teamspeak/ts3client/data/e/b Lcom/teamspeak/ts3client/a/k;
areturn
.limit locals 1
.limit stack 1
.end method

.method private t()Lcom/teamspeak/ts3client/data/g;
aload 0
getfield com/teamspeak/ts3client/data/e/f Lcom/teamspeak/ts3client/data/g;
areturn
.limit locals 1
.limit stack 1
.end method

.method private u()Lcom/teamspeak/ts3client/data/f/a;
aload 0
getfield com/teamspeak/ts3client/data/e/s Lcom/teamspeak/ts3client/data/f/a;
areturn
.limit locals 1
.limit stack 1
.end method

.method private v()I
aload 0
getfield com/teamspeak/ts3client/data/e/n I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private w()Lcom/teamspeak/ts3client/ConnectionBackground;
aload 0
getfield com/teamspeak/ts3client/data/e/l Lcom/teamspeak/ts3client/ConnectionBackground;
areturn
.limit locals 1
.limit stack 1
.end method

.method private x()Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/data/e/C Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method private y()Lcom/teamspeak/ts3client/jni/l;
aload 0
getfield com/teamspeak/ts3client/data/e/c Lcom/teamspeak/ts3client/jni/l;
areturn
.limit locals 1
.limit stack 1
.end method

.method private z()Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/data/e/t Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final a()Lcom/a/b/d/bw;
aload 0
getfield com/teamspeak/ts3client/data/e/w Lcom/a/b/d/bw;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final a(J)V
aload 0
lload 1
putfield com/teamspeak/ts3client/data/e/g J
return
.limit locals 3
.limit stack 3
.end method

.method public final a(Lcom/teamspeak/ts3client/data/c/d;)V
aload 0
aload 1
putfield com/teamspeak/ts3client/data/e/r Lcom/teamspeak/ts3client/data/c/d;
return
.limit locals 2
.limit stack 2
.end method

.method public final a(Lcom/teamspeak/ts3client/data/d/v;)V
aload 0
aload 1
putfield com/teamspeak/ts3client/data/e/u Lcom/teamspeak/ts3client/data/d/v;
return
.limit locals 2
.limit stack 2
.end method

.method public final a(Ljava/lang/String;)V
aload 0
getfield com/teamspeak/ts3client/data/e/v Lcom/teamspeak/ts3client/data/ab;
aload 1
putfield com/teamspeak/ts3client/data/ab/b Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/data/e/m Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/k Lcom/teamspeak/ts3client/t;
astore 1
aload 1
invokevirtual com/teamspeak/ts3client/t/i()Landroid/support/v4/app/bb;
new com/teamspeak/ts3client/ap
dup
aload 1
invokespecial com/teamspeak/ts3client/ap/<init>(Lcom/teamspeak/ts3client/t;)V
invokevirtual android/support/v4/app/bb/runOnUiThread(Ljava/lang/Runnable;)V
return
.limit locals 2
.limit stack 4
.end method

.method public final a(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.catch java/lang/Exception from L0 to L1 using L2
aload 0
getfield com/teamspeak/ts3client/data/e/m Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
aload 1
putfield com/teamspeak/ts3client/data/e/t Ljava/lang/String;
aload 7
ldc ""
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L3
aload 0
getfield com/teamspeak/ts3client/data/e/m Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/data/e/e J
getstatic com/teamspeak/ts3client/jni/d/ad Lcom/teamspeak/ts3client/jni/d;
aload 7
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/a(JLcom/teamspeak/ts3client/jni/d;Ljava/lang/String;)I
pop
L3:
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 7
iconst_0
istore 9
aload 4
ldc "/"
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifeq L4
aload 7
aload 4
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
iconst_1
istore 8
L5:
aload 7
aload 7
invokevirtual java/util/ArrayList/size()I
anewarray java/lang/String
invokevirtual java/util/ArrayList/toArray([Ljava/lang/Object;)[Ljava/lang/Object;
checkcast [Ljava/lang/String;
astore 11
aload 0
getfield com/teamspeak/ts3client/data/e/m Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/getContentResolver()Landroid/content/ContentResolver;
ldc "android_id"
invokestatic android/provider/Settings$Secure/getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;
astore 7
aload 7
astore 4
aload 7
ifnonnull L6
ldc ""
astore 4
L6:
aload 0
getfield com/teamspeak/ts3client/data/e/m Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
astore 7
aload 0
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/data/e/e J
aload 0
getfield com/teamspeak/ts3client/data/e/C Ljava/lang/String;
aload 1
iload 2
aload 3
aload 11
iload 8
aload 5
aload 6
aload 4
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_startConnectionEx(JLjava/lang/String;Ljava/lang/String;ILjava/lang/String;[Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
istore 2
aload 7
iload 2
putfield com/teamspeak/ts3client/data/e/n I
aload 7
getfield com/teamspeak/ts3client/data/e/n I
sipush 519
if_icmpne L7
invokestatic com/teamspeak/ts3client/data/b/f/a()Lcom/teamspeak/ts3client/data/b/f;
invokevirtual com/teamspeak/ts3client/data/b/f/b()V
aload 7
getfield com/teamspeak/ts3client/data/e/m Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/k Lcom/teamspeak/ts3client/t;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 7
getfield com/teamspeak/ts3client/data/e/m Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/getApplicationContext()Landroid/content/Context;
ldc_w 2131034152
invokevirtual android/content/Context/getString(I)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 7
getfield com/teamspeak/ts3client/data/e/m Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/n I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aload 7
getfield com/teamspeak/ts3client/data/e/m Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/getApplicationContext()Landroid/content/Context;
ldc_w 2131034151
invokevirtual android/content/Context/getString(I)Ljava/lang/String;
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
aload 7
getfield com/teamspeak/ts3client/data/e/m Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/getApplicationContext()Landroid/content/Context;
ldc_w 2131034142
invokevirtual android/content/Context/getString(I)Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/t/a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;)V
L7:
aload 7
getfield com/teamspeak/ts3client/data/e/m Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/d Ljava/util/logging/Logger;
getstatic java/util/logging/Level/INFO Ljava/util/logging/Level;
new java/lang/StringBuilder
dup
ldc "ClientLib Connect_return:"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
iload 2
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;)V
aload 0
getfield com/teamspeak/ts3client/data/e/m Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/getBaseContext()Landroid/content/Context;
invokestatic com/teamspeak/ts3client/chat/d/a(Landroid/content/Context;)Lcom/teamspeak/ts3client/chat/d;
astore 1
aload 1
new com/teamspeak/ts3client/chat/a
dup
ldc "Server Tab"
ldc "SERVER"
aconst_null
invokespecial com/teamspeak/ts3client/chat/a/<init>(Ljava/lang/String;Ljava/lang/String;Lcom/teamspeak/ts3client/data/c;)V
putfield com/teamspeak/ts3client/chat/d/c Lcom/teamspeak/ts3client/chat/a;
aload 1
getfield com/teamspeak/ts3client/chat/d/c Lcom/teamspeak/ts3client/chat/a;
iconst_1
putfield com/teamspeak/ts3client/chat/a/b Z
aload 1
new com/teamspeak/ts3client/chat/a
dup
ldc "Channel Tab"
ldc "CHANNEL"
aconst_null
invokespecial com/teamspeak/ts3client/chat/a/<init>(Ljava/lang/String;Ljava/lang/String;Lcom/teamspeak/ts3client/data/c;)V
putfield com/teamspeak/ts3client/chat/d/d Lcom/teamspeak/ts3client/chat/a;
aload 1
getfield com/teamspeak/ts3client/chat/d/d Lcom/teamspeak/ts3client/chat/a;
iconst_1
putfield com/teamspeak/ts3client/chat/a/b Z
aload 1
aload 1
getfield com/teamspeak/ts3client/chat/d/c Lcom/teamspeak/ts3client/chat/a;
invokevirtual com/teamspeak/ts3client/chat/d/a(Lcom/teamspeak/ts3client/chat/a;)V
aload 1
aload 1
getfield com/teamspeak/ts3client/chat/d/d Lcom/teamspeak/ts3client/chat/a;
invokevirtual com/teamspeak/ts3client/chat/d/a(Lcom/teamspeak/ts3client/chat/a;)V
aload 0
getfield com/teamspeak/ts3client/data/e/m Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/k Lcom/teamspeak/ts3client/t;
getfield android/support/v4/app/Fragment/M Landroid/support/v4/app/bl;
invokestatic com/teamspeak/ts3client/chat/d/a(Landroid/support/v4/app/bi;)V
aload 0
getfield com/teamspeak/ts3client/data/e/m Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
ldc "ptt_key_setting"
ldc ""
invokeinterface android/content/SharedPreferences/getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String; 2
astore 3
aload 3
invokevirtual java/lang/String/isEmpty()Z
ifne L8
new java/util/BitSet
dup
invokespecial java/util/BitSet/<init>()V
astore 1
aload 3
ldc ","
invokevirtual java/lang/String/split(Ljava/lang/String;)[Ljava/lang/String;
astore 3
aload 3
arraylength
istore 8
iconst_0
istore 2
L9:
iload 2
iload 8
if_icmpge L10
aload 3
iload 2
aaload
astore 4
L0:
aload 1
aload 4
invokestatic java/lang/Integer/parseInt(Ljava/lang/String;)I
invokevirtual java/util/BitSet/set(I)V
L1:
iload 2
iconst_1
iadd
istore 2
goto L9
L4:
getstatic com/teamspeak/ts3client/data/e/A Ljava/util/regex/Pattern;
aload 4
invokevirtual java/util/regex/Pattern/matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;
astore 4
L11:
iload 9
istore 8
aload 4
invokevirtual java/util/regex/Matcher/find()Z
ifeq L5
aload 4
iconst_1
invokevirtual java/util/regex/Matcher/group(I)Ljava/lang/String;
astore 11
aload 11
ldc ""
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L11
aload 7
aload 11
ldc "\\/"
ldc "/"
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
goto L11
L10:
aload 0
getfield com/teamspeak/ts3client/data/e/m Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
ldc "ptt_key_setting_intercept"
iconst_0
invokeinterface android/content/SharedPreferences/getBoolean(Ljava/lang/String;Z)Z 2
istore 10
invokestatic com/teamspeak/ts3client/data/v/a()Lcom/teamspeak/ts3client/data/v;
aload 1
iload 10
invokevirtual com/teamspeak/ts3client/data/v/a(Ljava/util/BitSet;Z)V
invokestatic com/teamspeak/ts3client/data/v/a()Lcom/teamspeak/ts3client/data/v;
iconst_1
putfield com/teamspeak/ts3client/data/v/b Z
L8:
return
L2:
astore 4
goto L1
.limit locals 12
.limit stack 12
.end method

.method public final b()Lcom/teamspeak/ts3client/data/a/b;
aload 0
getfield com/teamspeak/ts3client/data/e/p Lcom/teamspeak/ts3client/data/a/b;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final b(Ljava/lang/String;)V
aload 0
aload 1
putfield com/teamspeak/ts3client/data/e/q Ljava/lang/String;
return
.limit locals 2
.limit stack 2
.end method

.method public final c()Lcom/teamspeak/ts3client/data/b;
.catch all from L0 to L1 using L2
aload 0
monitorenter
L0:
aload 0
getfield com/teamspeak/ts3client/data/e/B Lcom/teamspeak/ts3client/data/b;
astore 1
L1:
aload 0
monitorexit
aload 1
areturn
L2:
astore 1
aload 0
monitorexit
aload 1
athrow
.limit locals 2
.limit stack 1
.end method

.method public final d()Ljava/util/HashMap;
aload 0
getfield com/teamspeak/ts3client/data/e/i Ljava/util/HashMap;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final e()Lcom/teamspeak/ts3client/t;
aload 0
getfield com/teamspeak/ts3client/data/e/k Lcom/teamspeak/ts3client/t;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final f()J
aload 0
getfield com/teamspeak/ts3client/data/e/g J
lreturn
.limit locals 1
.limit stack 2
.end method

.method public final g()Lcom/teamspeak/ts3client/data/c/d;
aload 0
getfield com/teamspeak/ts3client/data/e/r Lcom/teamspeak/ts3client/data/c/d;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final h()Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final i()I
aload 0
getfield com/teamspeak/ts3client/data/e/h I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final j()Lcom/teamspeak/ts3client/data/d/v;
aload 0
getfield com/teamspeak/ts3client/data/e/u Lcom/teamspeak/ts3client/data/d/v;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final k()J
aload 0
getfield com/teamspeak/ts3client/data/e/e J
lreturn
.limit locals 1
.limit stack 2
.end method

.method public final l()Lcom/teamspeak/ts3client/data/g/b;
aload 0
getfield com/teamspeak/ts3client/data/e/o Lcom/teamspeak/ts3client/data/g/b;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final m()Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/data/e/v Lcom/teamspeak/ts3client/data/ab;
getfield com/teamspeak/ts3client/data/ab/b Ljava/lang/String;
ldc ""
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L0
aload 0
getfield com/teamspeak/ts3client/data/e/v Lcom/teamspeak/ts3client/data/ab;
getfield com/teamspeak/ts3client/data/ab/a Ljava/lang/String;
areturn
L0:
aload 0
getfield com/teamspeak/ts3client/data/e/v Lcom/teamspeak/ts3client/data/ab;
getfield com/teamspeak/ts3client/data/ab/b Ljava/lang/String;
areturn
.limit locals 1
.limit stack 2
.end method

.method public final n()V
aload 0
getfield com/teamspeak/ts3client/data/e/m Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/d Ljava/util/logging/Logger;
getstatic java/util/logging/Level/INFO Ljava/util/logging/Level;
ldc "Start AUDIO"
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;)V
invokestatic com/teamspeak/ts3client/a/j/a()Lcom/teamspeak/ts3client/a/j;
pop
aload 0
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/data/e/e J
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_activateCaptureDevice(J)I
pop
aload 0
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/data/e/e J
ldc ""
ldc ""
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_openCaptureDevice(JLjava/lang/String;Ljava/lang/String;)I
pop
aload 0
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/data/e/e J
ldc ""
ldc ""
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_openPlaybackDevice(JLjava/lang/String;Ljava/lang/String;)I
pop
aload 0
getfield com/teamspeak/ts3client/data/e/m Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/data/e/m Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
ldc "agc"
ldc "true"
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_setPreProcessorConfigValue(JLjava/lang/String;Ljava/lang/String;)I
pop
aload 0
getfield com/teamspeak/ts3client/data/e/b Lcom/teamspeak/ts3client/a/k;
invokevirtual com/teamspeak/ts3client/a/k/g()V
aload 0
getfield com/teamspeak/ts3client/data/e/b Lcom/teamspeak/ts3client/a/k;
invokevirtual com/teamspeak/ts3client/a/k/c()V
aload 0
getfield com/teamspeak/ts3client/data/e/m Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
ldc "voiceactivation_level"
bipush 10
invokeinterface android/content/SharedPreferences/getInt(Ljava/lang/String;I)I 2
istore 1
aload 0
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/data/e/e J
ldc "voiceactivation_level"
iload 1
invokestatic java/lang/String/valueOf(I)Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_setPreProcessorConfigValue(JLjava/lang/String;Ljava/lang/String;)I
pop
aload 0
getfield com/teamspeak/ts3client/data/e/m Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
ldc "audio_ptt"
iconst_0
invokeinterface android/content/SharedPreferences/getBoolean(Ljava/lang/String;Z)Z 2
istore 2
aload 0
getfield com/teamspeak/ts3client/data/e/m Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/d Ljava/util/logging/Logger;
getstatic java/util/logging/Level/INFO Ljava/util/logging/Level;
new java/lang/StringBuilder
dup
ldc "Setting PTT: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
iload 2
invokevirtual java/lang/StringBuilder/append(Z)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;)V
iload 2
ifeq L0
aload 0
getfield com/teamspeak/ts3client/data/e/m Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/data/e/m Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
getstatic com/teamspeak/ts3client/jni/d/k Lcom/teamspeak/ts3client/jni/d;
iconst_1
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/a(JLcom/teamspeak/ts3client/jni/d;I)I
pop
aload 0
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/data/e/e J
ldc "vad"
ldc "false"
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_setPreProcessorConfigValue(JLjava/lang/String;Ljava/lang/String;)I
pop
aload 0
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/data/e/e J
ldc "voiceactivation_level"
ldc "-50"
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_setPreProcessorConfigValue(JLjava/lang/String;Ljava/lang/String;)I
pop
L1:
aload 0
getfield com/teamspeak/ts3client/data/e/m Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
ldc "audio_bt"
iconst_0
invokeinterface android/content/SharedPreferences/getBoolean(Ljava/lang/String;Z)Z 2
istore 2
aload 0
getfield com/teamspeak/ts3client/data/e/m Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/d Ljava/util/logging/Logger;
getstatic java/util/logging/Level/INFO Ljava/util/logging/Level;
new java/lang/StringBuilder
dup
ldc "Setting BT:"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
iload 2
invokevirtual java/lang/StringBuilder/append(Z)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;)V
iload 2
ifeq L2
aload 0
getfield com/teamspeak/ts3client/data/e/b Lcom/teamspeak/ts3client/a/k;
iconst_1
invokevirtual com/teamspeak/ts3client/a/k/b(Z)V
L2:
aload 0
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/data/e/e J
ldc "volume_modifier"
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc_w 0.5F
aload 0
getfield com/teamspeak/ts3client/data/e/m Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
ldc "volume_modifier"
iconst_0
invokeinterface android/content/SharedPreferences/getInt(Ljava/lang/String;I)I 2
i2f
fmul
invokevirtual java/lang/StringBuilder/append(F)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_setPlaybackConfigValue(JLjava/lang/String;Ljava/lang/String;)I
pop
return
L0:
aload 0
getfield com/teamspeak/ts3client/data/e/b Lcom/teamspeak/ts3client/a/k;
invokevirtual com/teamspeak/ts3client/a/k/d()V
goto L1
.limit locals 3
.limit stack 9
.end method

.method public final o()Ljava/util/HashMap;
aload 0
getfield com/teamspeak/ts3client/data/e/j Ljava/util/HashMap;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final p()Lcom/teamspeak/ts3client/data/ab;
aload 0
getfield com/teamspeak/ts3client/data/e/v Lcom/teamspeak/ts3client/data/ab;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final q()Lcom/teamspeak/ts3client/data/d;
aload 0
getfield com/teamspeak/ts3client/data/e/d Lcom/teamspeak/ts3client/data/d;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final r()V
aload 0
invokespecial com/teamspeak/ts3client/data/e/E()V
aload 0
invokevirtual com/teamspeak/ts3client/data/e/n()V
return
.limit locals 1
.limit stack 1
.end method
