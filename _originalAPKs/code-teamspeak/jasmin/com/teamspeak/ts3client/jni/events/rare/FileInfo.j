.bytecode 50.0
.class public synchronized com/teamspeak/ts3client/jni/events/rare/FileInfo
.super java/lang/Object
.implements com/teamspeak/ts3client/jni/k

.field public 'a' Ljava/lang/String;

.field public 'b' J

.field private 'c' J

.field private 'd' J

.field private 'e' J

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private <init>(JJLjava/lang/String;JJ)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
lload 1
putfield com/teamspeak/ts3client/jni/events/rare/FileInfo/c J
aload 0
lload 3
putfield com/teamspeak/ts3client/jni/events/rare/FileInfo/d J
aload 0
aload 5
putfield com/teamspeak/ts3client/jni/events/rare/FileInfo/a Ljava/lang/String;
aload 0
lload 6
putfield com/teamspeak/ts3client/jni/events/rare/FileInfo/b J
aload 0
lload 8
putfield com/teamspeak/ts3client/jni/events/rare/FileInfo/e J
aload 0
invokestatic com/teamspeak/ts3client/jni/l/a(Lcom/teamspeak/ts3client/jni/k;)V
return
.limit locals 10
.limit stack 3
.end method

.method private a()J
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/FileInfo/c J
lreturn
.limit locals 1
.limit stack 2
.end method

.method private b()J
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/FileInfo/d J
lreturn
.limit locals 1
.limit stack 2
.end method

.method private c()Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/FileInfo/a Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method private d()J
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/FileInfo/b J
lreturn
.limit locals 1
.limit stack 2
.end method

.method private e()J
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/FileInfo/e J
lreturn
.limit locals 1
.limit stack 2
.end method

.method public toString()Ljava/lang/String;
new java/lang/StringBuilder
dup
ldc "FileInfo [serverConnectionHandlerID="
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/FileInfo/c J
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
ldc ", channelID="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/FileInfo/d J
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
ldc ", name="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/FileInfo/a Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ", size="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/FileInfo/b J
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
ldc ", datetime="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/FileInfo/e J
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
ldc "]"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 3
.end method
