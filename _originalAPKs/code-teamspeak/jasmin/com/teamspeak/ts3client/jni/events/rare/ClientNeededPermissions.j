.bytecode 50.0
.class public synchronized com/teamspeak/ts3client/jni/events/rare/ClientNeededPermissions
.super java/lang/Object
.implements com/teamspeak/ts3client/jni/k

.field public 'a' I

.field public 'b' I

.field private 'c' J

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private <init>(JII)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
lload 1
putfield com/teamspeak/ts3client/jni/events/rare/ClientNeededPermissions/c J
aload 0
iload 3
putfield com/teamspeak/ts3client/jni/events/rare/ClientNeededPermissions/a I
aload 0
iload 4
putfield com/teamspeak/ts3client/jni/events/rare/ClientNeededPermissions/b I
aload 0
invokestatic com/teamspeak/ts3client/jni/l/a(Lcom/teamspeak/ts3client/jni/k;)V
return
.limit locals 5
.limit stack 3
.end method

.method private b()I
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ClientNeededPermissions/b I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private c()J
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ClientNeededPermissions/c J
lreturn
.limit locals 1
.limit stack 2
.end method

.method public final a()I
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ClientNeededPermissions/a I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public toString()Ljava/lang/String;
new java/lang/StringBuilder
dup
ldc "ClientNeededPermissions [serverConnectionHandlerID="
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ClientNeededPermissions/c J
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
ldc ", permissionID="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ClientNeededPermissions/a I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc ", permissionValue="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ClientNeededPermissions/b I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc "]"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 3
.end method
