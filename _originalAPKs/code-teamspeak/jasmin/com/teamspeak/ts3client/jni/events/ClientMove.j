.bytecode 50.0
.class public synchronized com/teamspeak/ts3client/jni/events/ClientMove
.super java/lang/Object
.implements com/teamspeak/ts3client/jni/k

.field private 'a' J

.field private 'b' I

.field private 'c' J

.field private 'd' J

.field private 'e' Lcom/teamspeak/ts3client/jni/j;

.field private 'f' Ljava/lang/String;

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private <init>(JIJJILjava/lang/String;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
lload 1
putfield com/teamspeak/ts3client/jni/events/ClientMove/a J
aload 0
iload 3
putfield com/teamspeak/ts3client/jni/events/ClientMove/b I
aload 0
lload 4
putfield com/teamspeak/ts3client/jni/events/ClientMove/c J
aload 0
lload 6
putfield com/teamspeak/ts3client/jni/events/ClientMove/d J
iload 8
ifne L0
aload 0
getstatic com/teamspeak/ts3client/jni/j/a Lcom/teamspeak/ts3client/jni/j;
putfield com/teamspeak/ts3client/jni/events/ClientMove/e Lcom/teamspeak/ts3client/jni/j;
L0:
iload 8
iconst_1
if_icmpne L1
aload 0
getstatic com/teamspeak/ts3client/jni/j/b Lcom/teamspeak/ts3client/jni/j;
putfield com/teamspeak/ts3client/jni/events/ClientMove/e Lcom/teamspeak/ts3client/jni/j;
L1:
iload 8
iconst_2
if_icmpne L2
aload 0
getstatic com/teamspeak/ts3client/jni/j/c Lcom/teamspeak/ts3client/jni/j;
putfield com/teamspeak/ts3client/jni/events/ClientMove/e Lcom/teamspeak/ts3client/jni/j;
L2:
aload 0
aload 9
putfield com/teamspeak/ts3client/jni/events/ClientMove/f Ljava/lang/String;
aload 0
invokestatic com/teamspeak/ts3client/jni/l/a(Lcom/teamspeak/ts3client/jni/k;)V
return
.limit locals 10
.limit stack 3
.end method

.method public final a()I
aload 0
getfield com/teamspeak/ts3client/jni/events/ClientMove/b I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final b()Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/jni/events/ClientMove/f Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final c()J
aload 0
getfield com/teamspeak/ts3client/jni/events/ClientMove/d J
lreturn
.limit locals 1
.limit stack 2
.end method

.method public final d()J
aload 0
getfield com/teamspeak/ts3client/jni/events/ClientMove/c J
lreturn
.limit locals 1
.limit stack 2
.end method

.method public final e()J
aload 0
getfield com/teamspeak/ts3client/jni/events/ClientMove/a J
lreturn
.limit locals 1
.limit stack 2
.end method

.method public final f()Lcom/teamspeak/ts3client/jni/j;
aload 0
getfield com/teamspeak/ts3client/jni/events/ClientMove/e Lcom/teamspeak/ts3client/jni/j;
areturn
.limit locals 1
.limit stack 1
.end method

.method public toString()Ljava/lang/String;
new java/lang/StringBuilder
dup
ldc "ClientMove [serverConnectionHandlerID="
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
getfield com/teamspeak/ts3client/jni/events/ClientMove/a J
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
ldc ", clientID="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/jni/events/ClientMove/b I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc ", oldChannelID="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/jni/events/ClientMove/c J
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
ldc ", newChannelID="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/jni/events/ClientMove/d J
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
ldc ", visibility="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/jni/events/ClientMove/e Lcom/teamspeak/ts3client/jni/j;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc ", moveMessage="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/jni/events/ClientMove/f Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "]"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 3
.end method
