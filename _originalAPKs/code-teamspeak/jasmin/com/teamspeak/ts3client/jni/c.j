.bytecode 50.0
.class public final synchronized enum com/teamspeak/ts3client/jni/c
.super java/lang/Enum

.field public static final enum 'A' Lcom/teamspeak/ts3client/jni/c;

.field public static final enum 'B' Lcom/teamspeak/ts3client/jni/c;

.field public static final enum 'C' Lcom/teamspeak/ts3client/jni/c;

.field public static final enum 'D' Lcom/teamspeak/ts3client/jni/c;

.field public static final enum 'E' Lcom/teamspeak/ts3client/jni/c;

.field public static final enum 'F' Lcom/teamspeak/ts3client/jni/c;

.field public static final enum 'G' Lcom/teamspeak/ts3client/jni/c;

.field public static final enum 'H' Lcom/teamspeak/ts3client/jni/c;

.field private static final synthetic 'J' [Lcom/teamspeak/ts3client/jni/c;

.field public static final enum 'a' Lcom/teamspeak/ts3client/jni/c;

.field public static final enum 'b' Lcom/teamspeak/ts3client/jni/c;

.field public static final enum 'c' Lcom/teamspeak/ts3client/jni/c;

.field public static final enum 'd' Lcom/teamspeak/ts3client/jni/c;

.field public static final enum 'e' Lcom/teamspeak/ts3client/jni/c;

.field public static final enum 'f' Lcom/teamspeak/ts3client/jni/c;

.field public static final enum 'g' Lcom/teamspeak/ts3client/jni/c;

.field public static final enum 'h' Lcom/teamspeak/ts3client/jni/c;

.field public static final enum 'i' Lcom/teamspeak/ts3client/jni/c;

.field public static final enum 'j' Lcom/teamspeak/ts3client/jni/c;

.field public static final enum 'k' Lcom/teamspeak/ts3client/jni/c;

.field public static final enum 'l' Lcom/teamspeak/ts3client/jni/c;

.field public static final enum 'm' Lcom/teamspeak/ts3client/jni/c;

.field public static final enum 'n' Lcom/teamspeak/ts3client/jni/c;

.field public static final enum 'o' Lcom/teamspeak/ts3client/jni/c;

.field public static final enum 'p' Lcom/teamspeak/ts3client/jni/c;

.field public static final enum 'q' Lcom/teamspeak/ts3client/jni/c;

.field public static final enum 'r' Lcom/teamspeak/ts3client/jni/c;

.field public static final enum 's' Lcom/teamspeak/ts3client/jni/c;

.field public static final enum 't' Lcom/teamspeak/ts3client/jni/c;

.field public static final enum 'u' Lcom/teamspeak/ts3client/jni/c;

.field public static final enum 'v' Lcom/teamspeak/ts3client/jni/c;

.field public static final enum 'w' Lcom/teamspeak/ts3client/jni/c;

.field public static final enum 'x' Lcom/teamspeak/ts3client/jni/c;

.field public static final enum 'y' Lcom/teamspeak/ts3client/jni/c;

.field public static final enum 'z' Lcom/teamspeak/ts3client/jni/c;

.field 'I' I

.method static <clinit>()V
new com/teamspeak/ts3client/jni/c
dup
ldc "CHANNEL_NAME"
iconst_0
iconst_0
invokespecial com/teamspeak/ts3client/jni/c/<init>(Ljava/lang/String;II)V
putstatic com/teamspeak/ts3client/jni/c/a Lcom/teamspeak/ts3client/jni/c;
new com/teamspeak/ts3client/jni/c
dup
ldc "CHANNEL_TOPIC"
iconst_1
iconst_1
invokespecial com/teamspeak/ts3client/jni/c/<init>(Ljava/lang/String;II)V
putstatic com/teamspeak/ts3client/jni/c/b Lcom/teamspeak/ts3client/jni/c;
new com/teamspeak/ts3client/jni/c
dup
ldc "CHANNEL_DESCRIPTION"
iconst_2
iconst_2
invokespecial com/teamspeak/ts3client/jni/c/<init>(Ljava/lang/String;II)V
putstatic com/teamspeak/ts3client/jni/c/c Lcom/teamspeak/ts3client/jni/c;
new com/teamspeak/ts3client/jni/c
dup
ldc "CHANNEL_PASSWORD"
iconst_3
iconst_3
invokespecial com/teamspeak/ts3client/jni/c/<init>(Ljava/lang/String;II)V
putstatic com/teamspeak/ts3client/jni/c/d Lcom/teamspeak/ts3client/jni/c;
new com/teamspeak/ts3client/jni/c
dup
ldc "CHANNEL_CODEC"
iconst_4
iconst_4
invokespecial com/teamspeak/ts3client/jni/c/<init>(Ljava/lang/String;II)V
putstatic com/teamspeak/ts3client/jni/c/e Lcom/teamspeak/ts3client/jni/c;
new com/teamspeak/ts3client/jni/c
dup
ldc "CHANNEL_CODEC_QUALITY"
iconst_5
iconst_5
invokespecial com/teamspeak/ts3client/jni/c/<init>(Ljava/lang/String;II)V
putstatic com/teamspeak/ts3client/jni/c/f Lcom/teamspeak/ts3client/jni/c;
new com/teamspeak/ts3client/jni/c
dup
ldc "CHANNEL_MAXCLIENTS"
bipush 6
bipush 6
invokespecial com/teamspeak/ts3client/jni/c/<init>(Ljava/lang/String;II)V
putstatic com/teamspeak/ts3client/jni/c/g Lcom/teamspeak/ts3client/jni/c;
new com/teamspeak/ts3client/jni/c
dup
ldc "CHANNEL_MAXFAMILYCLIENTS"
bipush 7
bipush 7
invokespecial com/teamspeak/ts3client/jni/c/<init>(Ljava/lang/String;II)V
putstatic com/teamspeak/ts3client/jni/c/h Lcom/teamspeak/ts3client/jni/c;
new com/teamspeak/ts3client/jni/c
dup
ldc "CHANNEL_ORDER"
bipush 8
bipush 8
invokespecial com/teamspeak/ts3client/jni/c/<init>(Ljava/lang/String;II)V
putstatic com/teamspeak/ts3client/jni/c/i Lcom/teamspeak/ts3client/jni/c;
new com/teamspeak/ts3client/jni/c
dup
ldc "CHANNEL_FLAG_PERMANENT"
bipush 9
bipush 9
invokespecial com/teamspeak/ts3client/jni/c/<init>(Ljava/lang/String;II)V
putstatic com/teamspeak/ts3client/jni/c/j Lcom/teamspeak/ts3client/jni/c;
new com/teamspeak/ts3client/jni/c
dup
ldc "CHANNEL_FLAG_SEMI_PERMANENT"
bipush 10
bipush 10
invokespecial com/teamspeak/ts3client/jni/c/<init>(Ljava/lang/String;II)V
putstatic com/teamspeak/ts3client/jni/c/k Lcom/teamspeak/ts3client/jni/c;
new com/teamspeak/ts3client/jni/c
dup
ldc "CHANNEL_FLAG_DEFAULT"
bipush 11
bipush 11
invokespecial com/teamspeak/ts3client/jni/c/<init>(Ljava/lang/String;II)V
putstatic com/teamspeak/ts3client/jni/c/l Lcom/teamspeak/ts3client/jni/c;
new com/teamspeak/ts3client/jni/c
dup
ldc "CHANNEL_FLAG_PASSWORD"
bipush 12
bipush 12
invokespecial com/teamspeak/ts3client/jni/c/<init>(Ljava/lang/String;II)V
putstatic com/teamspeak/ts3client/jni/c/m Lcom/teamspeak/ts3client/jni/c;
new com/teamspeak/ts3client/jni/c
dup
ldc "CHANNEL_CODEC_LATENCY_FACTOR"
bipush 13
bipush 13
invokespecial com/teamspeak/ts3client/jni/c/<init>(Ljava/lang/String;II)V
putstatic com/teamspeak/ts3client/jni/c/n Lcom/teamspeak/ts3client/jni/c;
new com/teamspeak/ts3client/jni/c
dup
ldc "CHANNEL_CODEC_IS_UNENCRYPTED"
bipush 14
bipush 14
invokespecial com/teamspeak/ts3client/jni/c/<init>(Ljava/lang/String;II)V
putstatic com/teamspeak/ts3client/jni/c/o Lcom/teamspeak/ts3client/jni/c;
new com/teamspeak/ts3client/jni/c
dup
ldc "CHANNEL_SECURITY_SALT"
bipush 15
bipush 15
invokespecial com/teamspeak/ts3client/jni/c/<init>(Ljava/lang/String;II)V
putstatic com/teamspeak/ts3client/jni/c/p Lcom/teamspeak/ts3client/jni/c;
new com/teamspeak/ts3client/jni/c
dup
ldc "CHANNEL_DELETE_DELAY"
bipush 16
bipush 16
invokespecial com/teamspeak/ts3client/jni/c/<init>(Ljava/lang/String;II)V
putstatic com/teamspeak/ts3client/jni/c/q Lcom/teamspeak/ts3client/jni/c;
new com/teamspeak/ts3client/jni/c
dup
ldc "CHANNEL_DUMMY_2"
bipush 17
bipush 17
invokespecial com/teamspeak/ts3client/jni/c/<init>(Ljava/lang/String;II)V
putstatic com/teamspeak/ts3client/jni/c/r Lcom/teamspeak/ts3client/jni/c;
new com/teamspeak/ts3client/jni/c
dup
ldc "CHANNEL_DUMMY_3"
bipush 18
bipush 18
invokespecial com/teamspeak/ts3client/jni/c/<init>(Ljava/lang/String;II)V
putstatic com/teamspeak/ts3client/jni/c/s Lcom/teamspeak/ts3client/jni/c;
new com/teamspeak/ts3client/jni/c
dup
ldc "CHANNEL_DUMMY_4"
bipush 19
bipush 19
invokespecial com/teamspeak/ts3client/jni/c/<init>(Ljava/lang/String;II)V
putstatic com/teamspeak/ts3client/jni/c/t Lcom/teamspeak/ts3client/jni/c;
new com/teamspeak/ts3client/jni/c
dup
ldc "CHANNEL_DUMMY_5"
bipush 20
bipush 20
invokespecial com/teamspeak/ts3client/jni/c/<init>(Ljava/lang/String;II)V
putstatic com/teamspeak/ts3client/jni/c/u Lcom/teamspeak/ts3client/jni/c;
new com/teamspeak/ts3client/jni/c
dup
ldc "CHANNEL_DUMMY_6"
bipush 21
bipush 21
invokespecial com/teamspeak/ts3client/jni/c/<init>(Ljava/lang/String;II)V
putstatic com/teamspeak/ts3client/jni/c/v Lcom/teamspeak/ts3client/jni/c;
new com/teamspeak/ts3client/jni/c
dup
ldc "CHANNEL_DUMMY_7"
bipush 22
bipush 22
invokespecial com/teamspeak/ts3client/jni/c/<init>(Ljava/lang/String;II)V
putstatic com/teamspeak/ts3client/jni/c/w Lcom/teamspeak/ts3client/jni/c;
new com/teamspeak/ts3client/jni/c
dup
ldc "CHANNEL_FLAG_MAXCLIENTS_UNLIMITED"
bipush 23
bipush 23
invokespecial com/teamspeak/ts3client/jni/c/<init>(Ljava/lang/String;II)V
putstatic com/teamspeak/ts3client/jni/c/x Lcom/teamspeak/ts3client/jni/c;
new com/teamspeak/ts3client/jni/c
dup
ldc "CHANNEL_FLAG_MAXFAMILYCLIENTS_UNLIMITED"
bipush 24
bipush 24
invokespecial com/teamspeak/ts3client/jni/c/<init>(Ljava/lang/String;II)V
putstatic com/teamspeak/ts3client/jni/c/y Lcom/teamspeak/ts3client/jni/c;
new com/teamspeak/ts3client/jni/c
dup
ldc "CHANNEL_FLAG_MAXFAMILYCLIENTS_INHERITED"
bipush 25
bipush 25
invokespecial com/teamspeak/ts3client/jni/c/<init>(Ljava/lang/String;II)V
putstatic com/teamspeak/ts3client/jni/c/z Lcom/teamspeak/ts3client/jni/c;
new com/teamspeak/ts3client/jni/c
dup
ldc "CHANNEL_FLAG_ARE_SUBSCRIBED"
bipush 26
bipush 26
invokespecial com/teamspeak/ts3client/jni/c/<init>(Ljava/lang/String;II)V
putstatic com/teamspeak/ts3client/jni/c/A Lcom/teamspeak/ts3client/jni/c;
new com/teamspeak/ts3client/jni/c
dup
ldc "CHANNEL_FILEPATH"
bipush 27
bipush 27
invokespecial com/teamspeak/ts3client/jni/c/<init>(Ljava/lang/String;II)V
putstatic com/teamspeak/ts3client/jni/c/B Lcom/teamspeak/ts3client/jni/c;
new com/teamspeak/ts3client/jni/c
dup
ldc "CHANNEL_NEEDED_TALK_POWER"
bipush 28
bipush 28
invokespecial com/teamspeak/ts3client/jni/c/<init>(Ljava/lang/String;II)V
putstatic com/teamspeak/ts3client/jni/c/C Lcom/teamspeak/ts3client/jni/c;
new com/teamspeak/ts3client/jni/c
dup
ldc "CHANNEL_FORCED_SILENCE"
bipush 29
bipush 29
invokespecial com/teamspeak/ts3client/jni/c/<init>(Ljava/lang/String;II)V
putstatic com/teamspeak/ts3client/jni/c/D Lcom/teamspeak/ts3client/jni/c;
new com/teamspeak/ts3client/jni/c
dup
ldc "CHANNEL_NAME_PHONETIC"
bipush 30
bipush 30
invokespecial com/teamspeak/ts3client/jni/c/<init>(Ljava/lang/String;II)V
putstatic com/teamspeak/ts3client/jni/c/E Lcom/teamspeak/ts3client/jni/c;
new com/teamspeak/ts3client/jni/c
dup
ldc "CHANNEL_ICON_ID"
bipush 31
bipush 31
invokespecial com/teamspeak/ts3client/jni/c/<init>(Ljava/lang/String;II)V
putstatic com/teamspeak/ts3client/jni/c/F Lcom/teamspeak/ts3client/jni/c;
new com/teamspeak/ts3client/jni/c
dup
ldc "CHANNEL_FLAG_PRIVATE"
bipush 32
bipush 32
invokespecial com/teamspeak/ts3client/jni/c/<init>(Ljava/lang/String;II)V
putstatic com/teamspeak/ts3client/jni/c/G Lcom/teamspeak/ts3client/jni/c;
new com/teamspeak/ts3client/jni/c
dup
ldc "CHANNEL_ENDMARKER_RARE"
bipush 33
bipush 33
invokespecial com/teamspeak/ts3client/jni/c/<init>(Ljava/lang/String;II)V
putstatic com/teamspeak/ts3client/jni/c/H Lcom/teamspeak/ts3client/jni/c;
bipush 34
anewarray com/teamspeak/ts3client/jni/c
dup
iconst_0
getstatic com/teamspeak/ts3client/jni/c/a Lcom/teamspeak/ts3client/jni/c;
aastore
dup
iconst_1
getstatic com/teamspeak/ts3client/jni/c/b Lcom/teamspeak/ts3client/jni/c;
aastore
dup
iconst_2
getstatic com/teamspeak/ts3client/jni/c/c Lcom/teamspeak/ts3client/jni/c;
aastore
dup
iconst_3
getstatic com/teamspeak/ts3client/jni/c/d Lcom/teamspeak/ts3client/jni/c;
aastore
dup
iconst_4
getstatic com/teamspeak/ts3client/jni/c/e Lcom/teamspeak/ts3client/jni/c;
aastore
dup
iconst_5
getstatic com/teamspeak/ts3client/jni/c/f Lcom/teamspeak/ts3client/jni/c;
aastore
dup
bipush 6
getstatic com/teamspeak/ts3client/jni/c/g Lcom/teamspeak/ts3client/jni/c;
aastore
dup
bipush 7
getstatic com/teamspeak/ts3client/jni/c/h Lcom/teamspeak/ts3client/jni/c;
aastore
dup
bipush 8
getstatic com/teamspeak/ts3client/jni/c/i Lcom/teamspeak/ts3client/jni/c;
aastore
dup
bipush 9
getstatic com/teamspeak/ts3client/jni/c/j Lcom/teamspeak/ts3client/jni/c;
aastore
dup
bipush 10
getstatic com/teamspeak/ts3client/jni/c/k Lcom/teamspeak/ts3client/jni/c;
aastore
dup
bipush 11
getstatic com/teamspeak/ts3client/jni/c/l Lcom/teamspeak/ts3client/jni/c;
aastore
dup
bipush 12
getstatic com/teamspeak/ts3client/jni/c/m Lcom/teamspeak/ts3client/jni/c;
aastore
dup
bipush 13
getstatic com/teamspeak/ts3client/jni/c/n Lcom/teamspeak/ts3client/jni/c;
aastore
dup
bipush 14
getstatic com/teamspeak/ts3client/jni/c/o Lcom/teamspeak/ts3client/jni/c;
aastore
dup
bipush 15
getstatic com/teamspeak/ts3client/jni/c/p Lcom/teamspeak/ts3client/jni/c;
aastore
dup
bipush 16
getstatic com/teamspeak/ts3client/jni/c/q Lcom/teamspeak/ts3client/jni/c;
aastore
dup
bipush 17
getstatic com/teamspeak/ts3client/jni/c/r Lcom/teamspeak/ts3client/jni/c;
aastore
dup
bipush 18
getstatic com/teamspeak/ts3client/jni/c/s Lcom/teamspeak/ts3client/jni/c;
aastore
dup
bipush 19
getstatic com/teamspeak/ts3client/jni/c/t Lcom/teamspeak/ts3client/jni/c;
aastore
dup
bipush 20
getstatic com/teamspeak/ts3client/jni/c/u Lcom/teamspeak/ts3client/jni/c;
aastore
dup
bipush 21
getstatic com/teamspeak/ts3client/jni/c/v Lcom/teamspeak/ts3client/jni/c;
aastore
dup
bipush 22
getstatic com/teamspeak/ts3client/jni/c/w Lcom/teamspeak/ts3client/jni/c;
aastore
dup
bipush 23
getstatic com/teamspeak/ts3client/jni/c/x Lcom/teamspeak/ts3client/jni/c;
aastore
dup
bipush 24
getstatic com/teamspeak/ts3client/jni/c/y Lcom/teamspeak/ts3client/jni/c;
aastore
dup
bipush 25
getstatic com/teamspeak/ts3client/jni/c/z Lcom/teamspeak/ts3client/jni/c;
aastore
dup
bipush 26
getstatic com/teamspeak/ts3client/jni/c/A Lcom/teamspeak/ts3client/jni/c;
aastore
dup
bipush 27
getstatic com/teamspeak/ts3client/jni/c/B Lcom/teamspeak/ts3client/jni/c;
aastore
dup
bipush 28
getstatic com/teamspeak/ts3client/jni/c/C Lcom/teamspeak/ts3client/jni/c;
aastore
dup
bipush 29
getstatic com/teamspeak/ts3client/jni/c/D Lcom/teamspeak/ts3client/jni/c;
aastore
dup
bipush 30
getstatic com/teamspeak/ts3client/jni/c/E Lcom/teamspeak/ts3client/jni/c;
aastore
dup
bipush 31
getstatic com/teamspeak/ts3client/jni/c/F Lcom/teamspeak/ts3client/jni/c;
aastore
dup
bipush 32
getstatic com/teamspeak/ts3client/jni/c/G Lcom/teamspeak/ts3client/jni/c;
aastore
dup
bipush 33
getstatic com/teamspeak/ts3client/jni/c/H Lcom/teamspeak/ts3client/jni/c;
aastore
putstatic com/teamspeak/ts3client/jni/c/J [Lcom/teamspeak/ts3client/jni/c;
return
.limit locals 0
.limit stack 5
.end method

.method private <init>(Ljava/lang/String;II)V
aload 0
aload 1
iload 2
invokespecial java/lang/Enum/<init>(Ljava/lang/String;I)V
aload 0
iload 3
putfield com/teamspeak/ts3client/jni/c/I I
return
.limit locals 4
.limit stack 3
.end method

.method private a()I
aload 0
getfield com/teamspeak/ts3client/jni/c/I I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/teamspeak/ts3client/jni/c;
ldc com/teamspeak/ts3client/jni/c
aload 0
invokestatic java/lang/Enum/valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;
checkcast com/teamspeak/ts3client/jni/c
areturn
.limit locals 1
.limit stack 2
.end method

.method public static values()[Lcom/teamspeak/ts3client/jni/c;
getstatic com/teamspeak/ts3client/jni/c/J [Lcom/teamspeak/ts3client/jni/c;
invokevirtual [Lcom/teamspeak/ts3client/jni/c;/clone()Ljava/lang/Object;
checkcast [Lcom/teamspeak/ts3client/jni/c;
areturn
.limit locals 0
.limit stack 1
.end method
