.bytecode 50.0
.class public synchronized com/teamspeak/ts3client/StartGUIFragment
.super android/support/v7/app/i
.implements android/hardware/SensorEventListener
.implements android/support/v4/app/bk
.implements com/teamspeak/ts3client/d/l
.implements com/teamspeak/ts3client/data/z

.field public static final 'm' Ljava/lang/String; = "preferences"

.field private static final 't' Ljava/lang/String; = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAo0t0xR6Ch8m7hhrWgMBjBBdSaOA/wFTSk27DMKnqsYrw/AgxJjK07KyxMnRYojLsX34Cy3TCmPbcK3V9mKCjwlakgBYTObiVKRnnF1wu20WmsOb0wjbRbhYGDbhmAwc7Sy8sXNehiuz/O+PTSHcwWvHL20zum2cmHDmCAjREb1ZlA4myNN/tY9DYNMkQjOpMqgOCIQbyXdJLinOzwrgeX7x2IXMOJ7bbxWEmuyRXviOijp/1hQfpweo6slwfuc9iQDPBQl77M9dObMUrr+/23pxEE2i+U8WsePwTIw4uxkyEjew3ZRkVmFTtTqdp77tcOOFJ2hn4KkhLkM0hSaLLRQIDAQAB"

.field private static final 'u' [B

.field private static final 'v' Ljava/lang/String; = "Axdk3D"

.field private static final 'w' I = 7474


.field private static 'x' Ljava/lang/String;

.field private 'A' Lcom/teamspeak/ts3client/data/b/c;

.field private 'B' Landroid/app/AlertDialog;

.field private 'C' Z

.field private 'D' I

.field private 'E' Lcom/teamspeak/ts3client/co;

.field private 'F' Landroid/os/PowerManager$WakeLock;

.field private 'G' Landroid/hardware/SensorManager;

.field private 'H' Landroid/hardware/Sensor;

.field private 'I' I

.field private 'J' Landroid/os/Handler;

.field private 'K' Lcom/teamspeak/ts3client/cm;

.field private 'L' Lcom/a/a/a/a/k;

.field private 'M' Landroid/view/View;

.field private 'N' Ljava/lang/String;

.field private 'O' Lcom/teamspeak/ts3client/d/c/a;

.field private 'P' Lcom/teamspeak/ts3client/d/t;

.field public 'n' Lcom/teamspeak/ts3client/Ts3Application;

.field public 'o' Lcom/teamspeak/ts3client/bookmark/e;

.field 'p' Z

.field public 'q' Lcom/teamspeak/ts3client/b;

.field 'r' Z

.field 's' Z

.field private 'y' Landroid/os/Bundle;

.field private 'z' Lcom/teamspeak/ts3client/data/b/f;

.method static <clinit>()V
bipush 20
newarray byte
dup
iconst_0
ldc_w 111
bastore
dup
iconst_1
ldc_w -46
bastore
dup
iconst_2
ldc_w 92
bastore
dup
iconst_3
ldc_w 23
bastore
dup
iconst_4
ldc_w -111
bastore
dup
iconst_5
ldc_w 95
bastore
dup
bipush 6
ldc_w 16
bastore
dup
bipush 7
ldc_w 23
bastore
dup
bipush 8
ldc_w -105
bastore
dup
bipush 9
ldc_w 71
bastore
dup
bipush 10
ldc_w -62
bastore
dup
bipush 11
ldc_w 78
bastore
dup
bipush 12
ldc_w 35
bastore
dup
bipush 13
ldc_w 47
bastore
dup
bipush 14
ldc_w -121
bastore
dup
bipush 15
ldc_w -12
bastore
dup
bipush 16
ldc_w -108
bastore
dup
bipush 17
ldc_w 12
bastore
dup
bipush 18
ldc_w -35
bastore
dup
bipush 19
ldc_w -46
bastore
putstatic com/teamspeak/ts3client/StartGUIFragment/u [B
aconst_null
putstatic com/teamspeak/ts3client/StartGUIFragment/x Ljava/lang/String;
return
.limit locals 0
.limit stack 4
.end method

.method public <init>()V
aload 0
invokespecial android/support/v7/app/i/<init>()V
aload 0
iconst_1
putfield com/teamspeak/ts3client/StartGUIFragment/C Z
aload 0
iconst_0
putfield com/teamspeak/ts3client/StartGUIFragment/p Z
aload 0
iconst_2
putfield com/teamspeak/ts3client/StartGUIFragment/D I
aload 0
ldc "734ea6e2d3c02045caa058dd1a7bb1b8"
putfield com/teamspeak/ts3client/StartGUIFragment/N Ljava/lang/String;
return
.limit locals 1
.limit stack 2
.end method

.method private A()V
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/getApplicationContext()Landroid/content/Context;
ldc "wifi"
invokevirtual android/content/Context/getSystemService(Ljava/lang/String;)Ljava/lang/Object;
checkcast android/net/wifi/WifiManager
iconst_3
ldc "Ts3WifiLOCK"
invokevirtual android/net/wifi/WifiManager/createWifiLock(ILjava/lang/String;)Landroid/net/wifi/WifiManager$WifiLock;
astore 1
aload 1
invokevirtual android/net/wifi/WifiManager$WifiLock/acquire()V
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
aload 1
putfield com/teamspeak/ts3client/Ts3Application/n Landroid/net/wifi/WifiManager$WifiLock;
return
.limit locals 2
.limit stack 3
.end method

.method private B()V
aload 0
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/N Ljava/lang/String;
invokestatic net/hockeyapp/android/b/a(Landroid/content/Context;Ljava/lang/String;)V
return
.limit locals 1
.limit stack 2
.end method

.method private C()V
aload 0
iconst_1
putfield com/teamspeak/ts3client/StartGUIFragment/r Z
return
.limit locals 1
.limit stack 2
.end method

.method private D()V
.catch java/io/IOException from L0 to L1 using L2
.catch java/io/IOException from L1 to L3 using L2
.catch java/io/IOException from L4 to L5 using L2
.catch java/io/IOException from L6 to L7 using L2
.catch java/io/IOException from L8 to L9 using L2
iconst_0
istore 1
ldc "com.teamspeak.ts"
invokestatic java/util/logging/Logger/getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;
astore 5
L0:
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
invokestatic android/os/Environment/getExternalStorageDirectory()Ljava/io/File;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc "/TS3/logs/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 6
aload 6
invokevirtual java/io/File/exists()Z
ifne L1
aload 6
invokevirtual java/io/File/mkdirs()Z
pop
L1:
aload 6
invokevirtual java/io/File/listFiles()[Ljava/io/File;
astore 6
invokestatic java/lang/System/currentTimeMillis()J
lstore 3
L3:
aload 6
ifnull L8
L4:
aload 6
arraylength
istore 2
L5:
iload 1
iload 2
if_icmpge L8
aload 6
iload 1
aaload
astore 7
L6:
aload 7
invokevirtual java/io/File/lastModified()J
lload 3
ldc2_w 604800000L
lsub
lcmp
ifge L10
aload 7
invokevirtual java/io/File/delete()Z
pop
L7:
goto L10
L8:
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
ldc "create_debuglog"
iconst_0
invokeinterface android/content/SharedPreferences/getBoolean(Ljava/lang/String;Z)Z 2
ifeq L9
new java/util/logging/FileHandler
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
invokestatic android/os/Environment/getExternalStorageDirectory()Ljava/io/File;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc "/TS3/logs/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
new java/text/SimpleDateFormat
dup
ldc "yyyy_MM_dd_HH_mm_ss"
invokespecial java/text/SimpleDateFormat/<init>(Ljava/lang/String;)V
new java/util/Date
dup
invokespecial java/util/Date/<init>()V
invokevirtual java/text/SimpleDateFormat/format(Ljava/util/Date;)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ".log"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/util/logging/FileHandler/<init>(Ljava/lang/String;)V
astore 6
aload 6
new java/util/logging/SimpleFormatter
dup
invokespecial java/util/logging/SimpleFormatter/<init>()V
invokevirtual java/util/logging/FileHandler/setFormatter(Ljava/util/logging/Formatter;)V
aload 5
aload 6
invokevirtual java/util/logging/Logger/addHandler(Ljava/util/logging/Handler;)V
L9:
aload 5
getstatic java/util/logging/Level/ALL Ljava/util/logging/Level;
invokevirtual java/util/logging/Logger/setLevel(Ljava/util/logging/Level;)V
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
aload 5
putfield com/teamspeak/ts3client/Ts3Application/d Ljava/util/logging/Logger;
return
L2:
astore 6
goto L9
L10:
iload 1
iconst_1
iadd
istore 1
goto L5
.limit locals 8
.limit stack 6
.end method

.method private E()V
.catch java/io/IOException from L0 to L1 using L2
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
invokestatic android/os/Environment/getExternalStorageDirectory()Ljava/io/File;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc "/TS3/content/lang"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 1
aload 1
invokevirtual java/io/File/exists()Z
ifne L3
aload 1
invokevirtual java/io/File/mkdirs()Z
pop
L3:
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
invokestatic android/os/Environment/getExternalStorageDirectory()Ljava/io/File;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc "/TS3"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 1
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 1
invokevirtual java/io/File/getAbsoluteFile()Ljava/io/File;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc "/Update.apk"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 1
aload 1
invokevirtual java/io/File/exists()Z
ifeq L4
aload 1
invokevirtual java/io/File/isFile()Z
ifeq L4
aload 1
invokevirtual java/io/File/delete()Z
pop
L4:
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
invokestatic android/os/Environment/getExternalStorageDirectory()Ljava/io/File;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc "/TS3/cache"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 1
aload 1
invokevirtual java/io/File/exists()Z
ifne L5
aload 1
invokevirtual java/io/File/mkdirs()Z
pop
L5:
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 1
invokevirtual java/io/File/getAbsoluteFile()Ljava/io/File;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc "/.nomedia"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 1
aload 1
invokevirtual java/io/File/exists()Z
ifne L1
L0:
aload 1
invokevirtual java/io/File/createNewFile()Z
pop
L1:
return
L2:
astore 1
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/d Ljava/util/logging/Logger;
getstatic java/util/logging/Level/SEVERE Ljava/util/logging/Level;
ldc "Could not create .nomedia File"
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;)V
return
.limit locals 2
.limit stack 4
.end method

.method private F()V
aload 0
invokevirtual com/teamspeak/ts3client/StartGUIFragment/c()Landroid/support/v4/app/bi;
invokevirtual android/support/v4/app/bi/f()I
ifle L0
aload 0
invokevirtual com/teamspeak/ts3client/StartGUIFragment/c()Landroid/support/v4/app/bi;
invokevirtual android/support/v4/app/bi/e()V
L0:
aload 0
invokevirtual com/teamspeak/ts3client/StartGUIFragment/c()Landroid/support/v4/app/bi;
invokevirtual android/support/v4/app/bi/a()Landroid/support/v4/app/cd;
astore 1
aload 1
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/c Landroid/support/v4/app/Fragment;
invokevirtual android/support/v4/app/cd/b(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/cd;
pop
aload 1
aload 0
invokevirtual com/teamspeak/ts3client/StartGUIFragment/c()Landroid/support/v4/app/bi;
ldc "Bookmark"
invokevirtual android/support/v4/app/bi/a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;
invokevirtual android/support/v4/app/cd/f(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/cd;
pop
aload 1
sipush 8194
invokevirtual android/support/v4/app/cd/a(I)Landroid/support/v4/app/cd;
pop
aload 1
invokevirtual android/support/v4/app/cd/i()I
pop
aload 0
invokevirtual com/teamspeak/ts3client/StartGUIFragment/l()V
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/k Lcom/teamspeak/ts3client/t;
astore 1
aload 1
invokevirtual com/teamspeak/ts3client/t/B()V
aload 1
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
aload 1
putfield com/teamspeak/ts3client/Ts3Application/c Landroid/support/v4/app/Fragment;
return
.limit locals 2
.limit stack 3
.end method

.method private G()Lcom/teamspeak/ts3client/b;
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/q Lcom/teamspeak/ts3client/b;
areturn
.limit locals 1
.limit stack 1
.end method

.method private H()V
.catch android/content/pm/PackageManager$NameNotFoundException from L0 to L1 using L2
.catch java/lang/NullPointerException from L0 to L1 using L3
iconst_0
istore 1
aload 0
ldc "preferences"
iconst_0
invokevirtual com/teamspeak/ts3client/StartGUIFragment/getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;
astore 4
aload 4
ldc "version"
iconst_m1
invokeinterface android/content/SharedPreferences/getInt(Ljava/lang/String;I)I 2
istore 3
L0:
aload 0
invokevirtual com/teamspeak/ts3client/StartGUIFragment/getPackageManager()Landroid/content/pm/PackageManager;
aload 0
invokevirtual com/teamspeak/ts3client/StartGUIFragment/getPackageName()Ljava/lang/String;
iconst_0
invokevirtual android/content/pm/PackageManager/getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
getfield android/content/pm/PackageInfo/versionCode I
istore 2
L1:
iload 2
istore 1
L4:
iload 3
iload 1
if_icmpge L5
aload 4
invokeinterface android/content/SharedPreferences/edit()Landroid/content/SharedPreferences$Editor; 0
astore 4
aload 4
ldc "version"
iload 1
invokeinterface android/content/SharedPreferences$Editor/putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor; 2
pop
aload 4
invokeinterface android/content/SharedPreferences$Editor/commit()Z 0
pop
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/B Landroid/app/AlertDialog;
ifnonnull L5
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/q Lcom/teamspeak/ts3client/b;
invokevirtual com/teamspeak/ts3client/b/show()V
L5:
return
L3:
astore 5
goto L4
L2:
astore 5
goto L4
.limit locals 6
.limit stack 3
.end method

.method private I()V
aload 0
new android/app/AlertDialog$Builder
dup
aload 0
invokespecial android/app/AlertDialog$Builder/<init>(Landroid/content/Context;)V
invokevirtual android/app/AlertDialog$Builder/create()Landroid/app/AlertDialog;
putfield com/teamspeak/ts3client/StartGUIFragment/B Landroid/app/AlertDialog;
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/B Landroid/app/AlertDialog;
ldc "Permission missing"
invokevirtual android/app/AlertDialog/setTitle(Ljava/lang/CharSequence;)V
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/B Landroid/app/AlertDialog;
ldc "We are missing a required permission!"
invokevirtual android/app/AlertDialog/setMessage(Ljava/lang/CharSequence;)V
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/B Landroid/app/AlertDialog;
bipush -2
ldc "button.exit"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
new com/teamspeak/ts3client/cb
dup
aload 0
invokespecial com/teamspeak/ts3client/cb/<init>(Lcom/teamspeak/ts3client/StartGUIFragment;)V
invokevirtual android/app/AlertDialog/setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/B Landroid/app/AlertDialog;
iconst_0
invokevirtual android/app/AlertDialog/setCancelable(Z)V
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/B Landroid/app/AlertDialog;
invokevirtual android/app/AlertDialog/show()V
return
.limit locals 1
.limit stack 6
.end method

.method private J()V
getstatic android/os/Build$VERSION/SDK_INT I
bipush 23
if_icmpge L0
aload 0
invokespecial com/teamspeak/ts3client/StartGUIFragment/w()V
return
L0:
aload 0
ldc "android.permission.RECORD_AUDIO"
invokestatic android/support/v4/c/h/a(Landroid/content/Context;Ljava/lang/String;)I
ifeq L1
aload 0
iconst_1
anewarray java/lang/String
dup
iconst_0
ldc "android.permission.RECORD_AUDIO"
aastore
iconst_1
invokestatic android/support/v4/app/m/a(Landroid/app/Activity;[Ljava/lang/String;I)V
return
L1:
aload 0
invokespecial com/teamspeak/ts3client/StartGUIFragment/w()V
aload 0
invokespecial com/teamspeak/ts3client/StartGUIFragment/L()V
return
.limit locals 1
.limit stack 5
.end method

.method private K()V
aload 0
ldc "android.permission.RECORD_AUDIO"
invokestatic android/support/v4/c/h/a(Landroid/content/Context;Ljava/lang/String;)I
ifeq L0
aload 0
iconst_1
anewarray java/lang/String
dup
iconst_0
ldc "android.permission.RECORD_AUDIO"
aastore
iconst_1
invokestatic android/support/v4/app/m/a(Landroid/app/Activity;[Ljava/lang/String;I)V
return
L0:
aload 0
invokespecial com/teamspeak/ts3client/StartGUIFragment/w()V
aload 0
invokespecial com/teamspeak/ts3client/StartGUIFragment/L()V
return
.limit locals 1
.limit stack 5
.end method

.method private L()V
aload 0
ldc "android.permission.READ_EXTERNAL_STORAGE"
invokestatic android/support/v4/c/h/a(Landroid/content/Context;Ljava/lang/String;)I
ifeq L0
aload 0
iconst_1
anewarray java/lang/String
dup
iconst_0
ldc "android.permission.READ_EXTERNAL_STORAGE"
aastore
iconst_2
invokestatic android/support/v4/app/m/a(Landroid/app/Activity;[Ljava/lang/String;I)V
return
L0:
aload 0
invokespecial com/teamspeak/ts3client/StartGUIFragment/M()V
return
.limit locals 1
.limit stack 5
.end method

.method private M()V
aload 0
ldc "android.permission.READ_EXTERNAL_STORAGE"
invokestatic android/support/v4/c/h/a(Landroid/content/Context;Ljava/lang/String;)I
ifeq L0
aload 0
iconst_1
anewarray java/lang/String
dup
iconst_0
ldc "android.permission.READ_PHONE_STATE"
aastore
iconst_3
invokestatic android/support/v4/app/m/a(Landroid/app/Activity;[Ljava/lang/String;I)V
L0:
return
.limit locals 1
.limit stack 5
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/StartGUIFragment;I)I
aload 0
iload 1
putfield com/teamspeak/ts3client/StartGUIFragment/D I
iload 1
ireturn
.limit locals 2
.limit stack 2
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/StartGUIFragment;)Landroid/app/AlertDialog;
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/B Landroid/app/AlertDialog;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static a(Landroid/content/Context;)Ljava/lang/String;
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch all from L4 to L5 using L2
ldc com/teamspeak/ts3client/StartGUIFragment
monitorenter
L0:
getstatic com/teamspeak/ts3client/StartGUIFragment/x Ljava/lang/String;
ifnonnull L4
aload 0
ldc "Axdk3D"
iconst_0
invokevirtual android/content/Context/getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;
astore 0
aload 0
ldc "Axdk3D"
aconst_null
invokeinterface android/content/SharedPreferences/getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String; 2
astore 1
aload 1
putstatic com/teamspeak/ts3client/StartGUIFragment/x Ljava/lang/String;
L1:
aload 1
ifnonnull L4
L3:
invokestatic java/util/UUID/randomUUID()Ljava/util/UUID;
invokevirtual java/util/UUID/toString()Ljava/lang/String;
putstatic com/teamspeak/ts3client/StartGUIFragment/x Ljava/lang/String;
aload 0
invokeinterface android/content/SharedPreferences/edit()Landroid/content/SharedPreferences$Editor; 0
astore 0
aload 0
ldc "Axdk3D"
getstatic com/teamspeak/ts3client/StartGUIFragment/x Ljava/lang/String;
invokeinterface android/content/SharedPreferences$Editor/putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor; 2
pop
aload 0
invokeinterface android/content/SharedPreferences$Editor/commit()Z 0
pop
L4:
getstatic com/teamspeak/ts3client/StartGUIFragment/x Ljava/lang/String;
astore 0
L5:
ldc com/teamspeak/ts3client/StartGUIFragment
monitorexit
aload 0
areturn
L2:
astore 0
ldc com/teamspeak/ts3client/StartGUIFragment
monitorexit
aload 0
athrow
.limit locals 2
.limit stack 3
.end method

.method private a(Landroid/net/Uri;)V
.catch java/io/UnsupportedEncodingException from L0 to L1 using L2
.catch java/io/UnsupportedEncodingException from L3 to L4 using L2
.catch java/io/UnsupportedEncodingException from L5 to L6 using L2
.catch java/io/UnsupportedEncodingException from L7 to L8 using L2
.catch java/io/UnsupportedEncodingException from L8 to L9 using L2
.catch java/io/UnsupportedEncodingException from L9 to L10 using L2
.catch java/io/UnsupportedEncodingException from L10 to L11 using L2
.catch java/io/UnsupportedEncodingException from L11 to L12 using L2
.catch java/io/UnsupportedEncodingException from L12 to L13 using L2
.catch java/io/UnsupportedEncodingException from L13 to L14 using L2
.catch java/io/UnsupportedEncodingException from L14 to L15 using L2
.catch java/io/UnsupportedEncodingException from L15 to L16 using L2
.catch java/io/UnsupportedEncodingException from L17 to L18 using L2
iconst_0
istore 2
aload 1
ifnull L19
L0:
aload 1
invokevirtual android/net/Uri/getQuery()Ljava/lang/String;
ldc "UTF-8"
invokestatic java/net/URLDecoder/decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
astore 5
new java/util/HashMap
dup
invokespecial java/util/HashMap/<init>()V
astore 4
aload 5
ldc "&"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L7
aload 5
ldc "&"
invokevirtual java/lang/String/split(Ljava/lang/String;)[Ljava/lang/String;
astore 5
aload 5
arraylength
istore 3
L1:
iload 2
iload 3
if_icmpge L8
L3:
aload 5
iload 2
aaload
ldc "="
invokevirtual java/lang/String/split(Ljava/lang/String;)[Ljava/lang/String;
astore 6
aload 6
arraylength
iconst_1
if_icmple L5
aload 4
aload 6
iconst_0
aaload
aload 6
iconst_1
aaload
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
L4:
goto L20
L5:
aload 4
aload 6
iconst_0
aaload
ldc ""
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
L6:
goto L20
L2:
astore 1
aload 1
invokevirtual java/io/UnsupportedEncodingException/printStackTrace()V
L19:
return
L7:
aload 5
ldc "="
invokevirtual java/lang/String/split(Ljava/lang/String;)[Ljava/lang/String;
astore 5
aload 5
arraylength
iconst_1
if_icmple L17
aload 4
aload 5
iconst_0
aaload
aload 5
iconst_1
aaload
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
L8:
new com/teamspeak/ts3client/data/ab
dup
invokespecial com/teamspeak/ts3client/data/ab/<init>()V
astore 5
aload 5
aload 1
invokevirtual android/net/Uri/getHost()Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/data/ab/a(Ljava/lang/String;)V
aload 4
ldc "port"
invokevirtual java/util/HashMap/containsKey(Ljava/lang/Object;)Z
ifeq L9
aload 5
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 5
getfield com/teamspeak/ts3client/data/ab/c Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ":"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 4
ldc "port"
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/String
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/data/ab/a(Ljava/lang/String;)V
L9:
aload 4
ldc "nickname"
invokevirtual java/util/HashMap/containsKey(Ljava/lang/Object;)Z
ifeq L10
aload 5
aload 4
ldc "nickname"
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/String
putfield com/teamspeak/ts3client/data/ab/f Ljava/lang/String;
L10:
aload 4
ldc "password"
invokevirtual java/util/HashMap/containsKey(Ljava/lang/Object;)Z
ifeq L11
aload 5
aload 4
ldc "password"
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/String
putfield com/teamspeak/ts3client/data/ab/e Ljava/lang/String;
L11:
aload 4
ldc "channel"
invokevirtual java/util/HashMap/containsKey(Ljava/lang/Object;)Z
ifeq L12
aload 5
aload 4
ldc "channel"
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/String
putfield com/teamspeak/ts3client/data/ab/g Ljava/lang/String;
L12:
aload 4
ldc "channelpassword"
invokevirtual java/util/HashMap/containsKey(Ljava/lang/Object;)Z
ifeq L13
aload 5
aload 4
ldc "channelpassword"
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/String
putfield com/teamspeak/ts3client/data/ab/h Ljava/lang/String;
L13:
aload 4
ldc "addbookmark"
invokevirtual java/util/HashMap/containsKey(Ljava/lang/Object;)Z
ifeq L14
aload 5
aload 4
ldc "addbookmark"
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/String
putfield com/teamspeak/ts3client/data/ab/a Ljava/lang/String;
L14:
aload 4
ldc "token"
invokevirtual java/util/HashMap/containsKey(Ljava/lang/Object;)Z
ifeq L15
aload 5
aload 4
ldc "token"
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/String
putfield com/teamspeak/ts3client/data/ab/i Ljava/lang/String;
L15:
aload 5
invokestatic com/teamspeak/ts3client/data/b/f/a()Lcom/teamspeak/ts3client/data/b/f;
invokevirtual com/teamspeak/ts3client/data/b/f/d()Lcom/teamspeak/ts3client/e/a;
getfield com/teamspeak/ts3client/e/a/a I
putfield com/teamspeak/ts3client/data/ab/d I
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/o Lcom/teamspeak/ts3client/bookmark/e;
aload 5
invokevirtual com/teamspeak/ts3client/bookmark/e/a(Lcom/teamspeak/ts3client/data/ab;)V
L16:
return
L17:
aload 4
aload 5
iconst_0
aaload
ldc ""
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
L18:
goto L8
L20:
iload 2
iconst_1
iadd
istore 2
goto L1
.limit locals 7
.limit stack 4
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/StartGUIFragment;Landroid/net/Uri;)V
.catch java/io/UnsupportedEncodingException from L0 to L1 using L2
.catch java/io/UnsupportedEncodingException from L3 to L4 using L2
.catch java/io/UnsupportedEncodingException from L5 to L6 using L2
.catch java/io/UnsupportedEncodingException from L7 to L8 using L2
.catch java/io/UnsupportedEncodingException from L8 to L9 using L2
.catch java/io/UnsupportedEncodingException from L9 to L10 using L2
.catch java/io/UnsupportedEncodingException from L10 to L11 using L2
.catch java/io/UnsupportedEncodingException from L11 to L12 using L2
.catch java/io/UnsupportedEncodingException from L12 to L13 using L2
.catch java/io/UnsupportedEncodingException from L13 to L14 using L2
.catch java/io/UnsupportedEncodingException from L14 to L15 using L2
.catch java/io/UnsupportedEncodingException from L15 to L16 using L2
.catch java/io/UnsupportedEncodingException from L17 to L18 using L2
iconst_0
istore 2
aload 1
ifnull L19
L0:
aload 1
invokevirtual android/net/Uri/getQuery()Ljava/lang/String;
ldc "UTF-8"
invokestatic java/net/URLDecoder/decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
astore 5
new java/util/HashMap
dup
invokespecial java/util/HashMap/<init>()V
astore 4
aload 5
ldc "&"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L7
aload 5
ldc "&"
invokevirtual java/lang/String/split(Ljava/lang/String;)[Ljava/lang/String;
astore 5
aload 5
arraylength
istore 3
L1:
iload 2
iload 3
if_icmpge L8
L3:
aload 5
iload 2
aaload
ldc "="
invokevirtual java/lang/String/split(Ljava/lang/String;)[Ljava/lang/String;
astore 6
aload 6
arraylength
iconst_1
if_icmple L5
aload 4
aload 6
iconst_0
aaload
aload 6
iconst_1
aaload
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
L4:
goto L20
L5:
aload 4
aload 6
iconst_0
aaload
ldc ""
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
L6:
goto L20
L2:
astore 0
aload 0
invokevirtual java/io/UnsupportedEncodingException/printStackTrace()V
L19:
return
L7:
aload 5
ldc "="
invokevirtual java/lang/String/split(Ljava/lang/String;)[Ljava/lang/String;
astore 5
aload 5
arraylength
iconst_1
if_icmple L17
aload 4
aload 5
iconst_0
aaload
aload 5
iconst_1
aaload
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
L8:
new com/teamspeak/ts3client/data/ab
dup
invokespecial com/teamspeak/ts3client/data/ab/<init>()V
astore 5
aload 5
aload 1
invokevirtual android/net/Uri/getHost()Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/data/ab/a(Ljava/lang/String;)V
aload 4
ldc "port"
invokevirtual java/util/HashMap/containsKey(Ljava/lang/Object;)Z
ifeq L9
aload 5
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 5
getfield com/teamspeak/ts3client/data/ab/c Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ":"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 4
ldc "port"
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/String
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/data/ab/a(Ljava/lang/String;)V
L9:
aload 4
ldc "nickname"
invokevirtual java/util/HashMap/containsKey(Ljava/lang/Object;)Z
ifeq L10
aload 5
aload 4
ldc "nickname"
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/String
putfield com/teamspeak/ts3client/data/ab/f Ljava/lang/String;
L10:
aload 4
ldc "password"
invokevirtual java/util/HashMap/containsKey(Ljava/lang/Object;)Z
ifeq L11
aload 5
aload 4
ldc "password"
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/String
putfield com/teamspeak/ts3client/data/ab/e Ljava/lang/String;
L11:
aload 4
ldc "channel"
invokevirtual java/util/HashMap/containsKey(Ljava/lang/Object;)Z
ifeq L12
aload 5
aload 4
ldc "channel"
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/String
putfield com/teamspeak/ts3client/data/ab/g Ljava/lang/String;
L12:
aload 4
ldc "channelpassword"
invokevirtual java/util/HashMap/containsKey(Ljava/lang/Object;)Z
ifeq L13
aload 5
aload 4
ldc "channelpassword"
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/String
putfield com/teamspeak/ts3client/data/ab/h Ljava/lang/String;
L13:
aload 4
ldc "addbookmark"
invokevirtual java/util/HashMap/containsKey(Ljava/lang/Object;)Z
ifeq L14
aload 5
aload 4
ldc "addbookmark"
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/String
putfield com/teamspeak/ts3client/data/ab/a Ljava/lang/String;
L14:
aload 4
ldc "token"
invokevirtual java/util/HashMap/containsKey(Ljava/lang/Object;)Z
ifeq L15
aload 5
aload 4
ldc "token"
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/String
putfield com/teamspeak/ts3client/data/ab/i Ljava/lang/String;
L15:
aload 5
invokestatic com/teamspeak/ts3client/data/b/f/a()Lcom/teamspeak/ts3client/data/b/f;
invokevirtual com/teamspeak/ts3client/data/b/f/d()Lcom/teamspeak/ts3client/e/a;
getfield com/teamspeak/ts3client/e/a/a I
putfield com/teamspeak/ts3client/data/ab/d I
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/o Lcom/teamspeak/ts3client/bookmark/e;
aload 5
invokevirtual com/teamspeak/ts3client/bookmark/e/a(Lcom/teamspeak/ts3client/data/ab;)V
L16:
return
L17:
aload 4
aload 5
iconst_0
aaload
ldc ""
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
L18:
goto L8
L20:
iload 2
iconst_1
iadd
istore 2
goto L1
.limit locals 7
.limit stack 4
.end method

.method private a(Lcom/teamspeak/ts3client/data/ab;)V
aload 1
getfield com/teamspeak/ts3client/data/ab/n I
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
ldc "reconnect.limit"
ldc "15"
invokeinterface android/content/SharedPreferences/getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String; 2
invokestatic java/lang/Integer/parseInt(Ljava/lang/String;)I
iconst_1
isub
if_icmple L0
new android/content/Intent
dup
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
ldc com/teamspeak/ts3client/StartGUIFragment
invokespecial android/content/Intent/<init>(Landroid/content/Context;Ljava/lang/Class;)V
astore 1
aload 1
ldc "android.intent.action.MAIN"
invokevirtual android/content/Intent/setAction(Ljava/lang/String;)Landroid/content/Intent;
pop
aload 1
ldc "android.intent.category.LAUNCHER"
invokevirtual android/content/Intent/addCategory(Ljava/lang/String;)Landroid/content/Intent;
pop
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
iconst_0
aload 1
iconst_0
invokestatic android/app/PendingIntent/getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;
astore 1
new android/support/v4/app/dm
dup
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/getApplicationContext()Landroid/content/Context;
invokespecial android/support/v4/app/dm/<init>(Landroid/content/Context;)V
astore 2
aload 2
ldc_w 2130837622
invokevirtual android/support/v4/app/dm/a(I)Landroid/support/v4/app/dm;
ldc "connection.reconnect"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/support/v4/app/dm/c(Ljava/lang/CharSequence;)Landroid/support/v4/app/dm;
invokestatic java/lang/System/currentTimeMillis()J
invokevirtual android/support/v4/app/dm/a(J)Landroid/support/v4/app/dm;
invokevirtual android/support/v4/app/dm/a()Landroid/support/v4/app/dm;
ldc "connection.reconnect"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/support/v4/app/dm/a(Ljava/lang/CharSequence;)Landroid/support/v4/app/dm;
ldc "connection.reconnect.text"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
ldc "reconnect.limit"
ldc "15"
invokeinterface android/content/SharedPreferences/getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String; 2
invokestatic java/lang/Integer/parseInt(Ljava/lang/String;)I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokevirtual android/support/v4/app/dm/b(Ljava/lang/CharSequence;)Landroid/support/v4/app/dm;
aload 1
putfield android/support/v4/app/dm/d Landroid/app/PendingIntent;
aload 2
invokevirtual android/support/v4/app/dm/c()Landroid/app/Notification;
astore 1
aload 1
aload 1
getfield android/app/Notification/flags I
bipush 20
ior
putfield android/app/Notification/flags I
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/l Landroid/app/NotificationManager;
aconst_null
bipush 101
aload 1
invokevirtual android/app/NotificationManager/notify(Ljava/lang/String;ILandroid/app/Notification;)V
aload 0
iconst_0
putfield com/teamspeak/ts3client/StartGUIFragment/p Z
aload 0
iconst_1
putfield com/teamspeak/ts3client/StartGUIFragment/s Z
return
L0:
new android/content/Intent
dup
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
ldc com/teamspeak/ts3client/StartGUIFragment
invokespecial android/content/Intent/<init>(Landroid/content/Context;Ljava/lang/Class;)V
astore 2
aload 2
ldc "android.intent.action.MAIN"
invokevirtual android/content/Intent/setAction(Ljava/lang/String;)Landroid/content/Intent;
pop
aload 2
ldc "android.intent.category.LAUNCHER"
invokevirtual android/content/Intent/addCategory(Ljava/lang/String;)Landroid/content/Intent;
pop
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
iconst_0
aload 2
iconst_0
invokestatic android/app/PendingIntent/getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;
astore 2
new android/support/v4/app/dm
dup
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/getApplicationContext()Landroid/content/Context;
invokespecial android/support/v4/app/dm/<init>(Landroid/content/Context;)V
astore 3
aload 3
ldc_w 2130837622
invokevirtual android/support/v4/app/dm/a(I)Landroid/support/v4/app/dm;
ldc "connection.reconnect.retry"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/support/v4/app/dm/c(Ljava/lang/CharSequence;)Landroid/support/v4/app/dm;
invokestatic java/lang/System/currentTimeMillis()J
invokevirtual android/support/v4/app/dm/a(J)Landroid/support/v4/app/dm;
invokevirtual android/support/v4/app/dm/a()Landroid/support/v4/app/dm;
ldc "connection.reconnect.retry"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/support/v4/app/dm/a(Ljava/lang/CharSequence;)Landroid/support/v4/app/dm;
ldc "connection.reconnect.retry"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/support/v4/app/dm/b(Ljava/lang/CharSequence;)Landroid/support/v4/app/dm;
aload 2
putfield android/support/v4/app/dm/d Landroid/app/PendingIntent;
aload 3
invokevirtual android/support/v4/app/dm/c()Landroid/app/Notification;
astore 2
aload 2
aload 2
getfield android/app/Notification/flags I
bipush 20
ior
putfield android/app/Notification/flags I
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/l Landroid/app/NotificationManager;
aconst_null
bipush 101
aload 2
invokevirtual android/app/NotificationManager/notify(Ljava/lang/String;ILandroid/app/Notification;)V
aload 1
aload 1
getfield com/teamspeak/ts3client/data/ab/n I
iconst_1
iadd
putfield com/teamspeak/ts3client/data/ab/n I
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/o Lcom/teamspeak/ts3client/bookmark/e;
aload 1
invokevirtual com/teamspeak/ts3client/bookmark/e/a(Lcom/teamspeak/ts3client/data/ab;)V
return
.limit locals 4
.limit stack 8
.end method

.method static synthetic b(Lcom/teamspeak/ts3client/StartGUIFragment;)Lcom/teamspeak/ts3client/cm;
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/K Lcom/teamspeak/ts3client/cm;
areturn
.limit locals 1
.limit stack 1
.end method

.method private b(I)V
aload 0
new android/app/AlertDialog$Builder
dup
aload 0
invokespecial android/app/AlertDialog$Builder/<init>(Landroid/content/Context;)V
invokevirtual android/app/AlertDialog$Builder/create()Landroid/app/AlertDialog;
putfield com/teamspeak/ts3client/StartGUIFragment/B Landroid/app/AlertDialog;
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/B Landroid/app/AlertDialog;
ldc "critical.payment"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/app/AlertDialog/setTitle(Ljava/lang/CharSequence;)V
iload 1
sipush 291
if_icmpne L0
new android/text/SpannableString
dup
ldc "critical.payment.network"
iconst_1
anewarray java/lang/Object
dup
iconst_0
iload 1
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokespecial android/text/SpannableString/<init>(Ljava/lang/CharSequence;)V
astore 2
L1:
new android/text/SpannableString
dup
ldc "\nhttp://play.google.com/store/apps/details?id=com.teamspeak.ts3client"
invokespecial android/text/SpannableString/<init>(Ljava/lang/CharSequence;)V
astore 3
aload 3
bipush 15
invokestatic android/text/util/Linkify/addLinks(Landroid/text/Spannable;I)Z
pop
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/B Landroid/app/AlertDialog;
iconst_2
anewarray java/lang/CharSequence
dup
iconst_0
aload 2
aastore
dup
iconst_1
aload 3
aastore
invokestatic android/text/TextUtils/concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
checkcast android/text/Spanned
invokevirtual android/app/AlertDialog/setMessage(Ljava/lang/CharSequence;)V
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/B Landroid/app/AlertDialog;
bipush -2
ldc "button.exit"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
new com/teamspeak/ts3client/bu
dup
aload 0
invokespecial com/teamspeak/ts3client/bu/<init>(Lcom/teamspeak/ts3client/StartGUIFragment;)V
invokevirtual android/app/AlertDialog/setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/B Landroid/app/AlertDialog;
bipush -3
ldc "button.retry"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
new com/teamspeak/ts3client/cc
dup
aload 0
invokespecial com/teamspeak/ts3client/cc/<init>(Lcom/teamspeak/ts3client/StartGUIFragment;)V
invokevirtual android/app/AlertDialog/setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/B Landroid/app/AlertDialog;
iconst_0
invokevirtual android/app/AlertDialog/setCancelable(Z)V
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/B Landroid/app/AlertDialog;
invokevirtual android/app/AlertDialog/show()V
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/B Landroid/app/AlertDialog;
ldc_w 16908299
invokevirtual android/app/AlertDialog/findViewById(I)Landroid/view/View;
checkcast android/widget/TextView
invokestatic android/text/method/LinkMovementMethod/getInstance()Landroid/text/method/MovementMethod;
invokevirtual android/widget/TextView/setMovementMethod(Landroid/text/method/MovementMethod;)V
return
L0:
new android/text/SpannableString
dup
ldc "critical.payment.other"
iconst_1
anewarray java/lang/Object
dup
iconst_0
iload 1
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokespecial android/text/SpannableString/<init>(Ljava/lang/CharSequence;)V
astore 2
goto L1
.limit locals 4
.limit stack 7
.end method

.method private b(Landroid/net/Uri;)V
.catch java/io/UnsupportedEncodingException from L0 to L1 using L2
.catch java/io/UnsupportedEncodingException from L3 to L4 using L2
.catch java/io/UnsupportedEncodingException from L5 to L6 using L2
.catch java/io/UnsupportedEncodingException from L7 to L8 using L2
.catch java/io/UnsupportedEncodingException from L8 to L9 using L2
.catch java/io/UnsupportedEncodingException from L9 to L10 using L2
.catch java/io/UnsupportedEncodingException from L10 to L11 using L2
.catch java/io/UnsupportedEncodingException from L11 to L12 using L2
.catch java/io/UnsupportedEncodingException from L12 to L13 using L2
.catch java/io/UnsupportedEncodingException from L13 to L14 using L2
.catch java/io/UnsupportedEncodingException from L14 to L15 using L2
.catch java/io/UnsupportedEncodingException from L15 to L16 using L2
.catch java/io/UnsupportedEncodingException from L17 to L18 using L2
iconst_0
istore 2
new com/teamspeak/ts3client/data/ab
dup
invokespecial com/teamspeak/ts3client/data/ab/<init>()V
astore 4
aload 1
ifnull L19
L0:
new java/util/HashMap
dup
invokespecial java/util/HashMap/<init>()V
astore 5
aload 1
invokevirtual android/net/Uri/getQuery()Ljava/lang/String;
ifnull L8
aload 1
invokevirtual android/net/Uri/getQuery()Ljava/lang/String;
ldc "UTF-8"
invokestatic java/net/URLDecoder/decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
astore 6
aload 6
ldc "&"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L7
aload 6
ldc "&"
invokevirtual java/lang/String/split(Ljava/lang/String;)[Ljava/lang/String;
astore 6
aload 6
arraylength
istore 3
L1:
iload 2
iload 3
if_icmpge L8
L3:
aload 6
iload 2
aaload
ldc "="
invokevirtual java/lang/String/split(Ljava/lang/String;)[Ljava/lang/String;
astore 7
aload 7
arraylength
iconst_1
if_icmple L5
aload 5
aload 7
iconst_0
aaload
aload 7
iconst_1
aaload
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
L4:
goto L20
L5:
aload 5
aload 7
iconst_0
aaload
ldc ""
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
L6:
goto L20
L2:
astore 1
aload 1
invokevirtual java/io/UnsupportedEncodingException/printStackTrace()V
L19:
return
L7:
aload 6
ldc "="
invokevirtual java/lang/String/split(Ljava/lang/String;)[Ljava/lang/String;
astore 6
aload 6
arraylength
iconst_1
if_icmple L17
aload 5
aload 6
iconst_0
aaload
aload 6
iconst_1
aaload
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
L8:
aload 4
aload 1
invokevirtual android/net/Uri/getHost()Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/data/ab/a(Ljava/lang/String;)V
aload 5
ldc "port"
invokevirtual java/util/HashMap/containsKey(Ljava/lang/Object;)Z
ifeq L9
aload 4
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 4
getfield com/teamspeak/ts3client/data/ab/c Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ":"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 5
ldc "port"
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/String
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/data/ab/a(Ljava/lang/String;)V
L9:
aload 5
ldc "nickname"
invokevirtual java/util/HashMap/containsKey(Ljava/lang/Object;)Z
ifeq L10
aload 4
aload 5
ldc "nickname"
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/String
putfield com/teamspeak/ts3client/data/ab/f Ljava/lang/String;
L10:
aload 5
ldc "password"
invokevirtual java/util/HashMap/containsKey(Ljava/lang/Object;)Z
ifeq L11
aload 4
aload 5
ldc "password"
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/String
putfield com/teamspeak/ts3client/data/ab/e Ljava/lang/String;
L11:
aload 5
ldc "channel"
invokevirtual java/util/HashMap/containsKey(Ljava/lang/Object;)Z
ifeq L12
aload 4
aload 5
ldc "channel"
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/String
putfield com/teamspeak/ts3client/data/ab/g Ljava/lang/String;
L12:
aload 5
ldc "channelpassword"
invokevirtual java/util/HashMap/containsKey(Ljava/lang/Object;)Z
ifeq L13
aload 4
aload 5
ldc "channelpassword"
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/String
putfield com/teamspeak/ts3client/data/ab/h Ljava/lang/String;
L13:
aload 5
ldc "addbookmark"
invokevirtual java/util/HashMap/containsKey(Ljava/lang/Object;)Z
ifeq L14
aload 4
aload 5
ldc "addbookmark"
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/String
putfield com/teamspeak/ts3client/data/ab/a Ljava/lang/String;
L14:
aload 5
ldc "token"
invokevirtual java/util/HashMap/containsKey(Ljava/lang/Object;)Z
ifeq L15
aload 4
aload 5
ldc "token"
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/String
putfield com/teamspeak/ts3client/data/ab/i Ljava/lang/String;
L15:
new com/teamspeak/ts3client/bookmark/a
dup
getstatic com/teamspeak/ts3client/data/b/a/a Lcom/teamspeak/ts3client/data/b/a;
aload 4
invokespecial com/teamspeak/ts3client/bookmark/a/<init>(Lcom/teamspeak/ts3client/data/b/a;Lcom/teamspeak/ts3client/data/ab;)V
aload 0
invokevirtual com/teamspeak/ts3client/StartGUIFragment/c()Landroid/support/v4/app/bi;
ldc "AddBookmark"
invokevirtual com/teamspeak/ts3client/bookmark/a/a(Landroid/support/v4/app/bi;Ljava/lang/String;)V
L16:
return
L17:
aload 5
aload 6
iconst_0
aaload
ldc ""
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
L18:
goto L8
L20:
iload 2
iconst_1
iadd
istore 2
goto L1
.limit locals 8
.limit stack 4
.end method

.method static synthetic b(Lcom/teamspeak/ts3client/StartGUIFragment;I)V
aload 0
new android/app/AlertDialog$Builder
dup
aload 0
invokespecial android/app/AlertDialog$Builder/<init>(Landroid/content/Context;)V
invokevirtual android/app/AlertDialog$Builder/create()Landroid/app/AlertDialog;
putfield com/teamspeak/ts3client/StartGUIFragment/B Landroid/app/AlertDialog;
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/B Landroid/app/AlertDialog;
ldc "critical.payment"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/app/AlertDialog/setTitle(Ljava/lang/CharSequence;)V
iload 1
sipush 291
if_icmpne L0
new android/text/SpannableString
dup
ldc "critical.payment.network"
iconst_1
anewarray java/lang/Object
dup
iconst_0
iload 1
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokespecial android/text/SpannableString/<init>(Ljava/lang/CharSequence;)V
astore 2
L1:
new android/text/SpannableString
dup
ldc "\nhttp://play.google.com/store/apps/details?id=com.teamspeak.ts3client"
invokespecial android/text/SpannableString/<init>(Ljava/lang/CharSequence;)V
astore 3
aload 3
bipush 15
invokestatic android/text/util/Linkify/addLinks(Landroid/text/Spannable;I)Z
pop
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/B Landroid/app/AlertDialog;
iconst_2
anewarray java/lang/CharSequence
dup
iconst_0
aload 2
aastore
dup
iconst_1
aload 3
aastore
invokestatic android/text/TextUtils/concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
checkcast android/text/Spanned
invokevirtual android/app/AlertDialog/setMessage(Ljava/lang/CharSequence;)V
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/B Landroid/app/AlertDialog;
bipush -2
ldc "button.exit"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
new com/teamspeak/ts3client/bu
dup
aload 0
invokespecial com/teamspeak/ts3client/bu/<init>(Lcom/teamspeak/ts3client/StartGUIFragment;)V
invokevirtual android/app/AlertDialog/setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/B Landroid/app/AlertDialog;
bipush -3
ldc "button.retry"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
new com/teamspeak/ts3client/cc
dup
aload 0
invokespecial com/teamspeak/ts3client/cc/<init>(Lcom/teamspeak/ts3client/StartGUIFragment;)V
invokevirtual android/app/AlertDialog/setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/B Landroid/app/AlertDialog;
iconst_0
invokevirtual android/app/AlertDialog/setCancelable(Z)V
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/B Landroid/app/AlertDialog;
invokevirtual android/app/AlertDialog/show()V
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/B Landroid/app/AlertDialog;
ldc_w 16908299
invokevirtual android/app/AlertDialog/findViewById(I)Landroid/view/View;
checkcast android/widget/TextView
invokestatic android/text/method/LinkMovementMethod/getInstance()Landroid/text/method/MovementMethod;
invokevirtual android/widget/TextView/setMovementMethod(Landroid/text/method/MovementMethod;)V
return
L0:
new android/text/SpannableString
dup
ldc "critical.payment.other"
iconst_1
anewarray java/lang/Object
dup
iconst_0
iload 1
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokespecial android/text/SpannableString/<init>(Ljava/lang/CharSequence;)V
astore 2
goto L1
.limit locals 4
.limit stack 7
.end method

.method static synthetic b(Lcom/teamspeak/ts3client/StartGUIFragment;Landroid/net/Uri;)V
aload 0
aload 1
invokespecial com/teamspeak/ts3client/StartGUIFragment/b(Landroid/net/Uri;)V
return
.limit locals 2
.limit stack 2
.end method

.method static synthetic c(Lcom/teamspeak/ts3client/StartGUIFragment;)Lcom/a/a/a/a/k;
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/L Lcom/a/a/a/a/k;
areturn
.limit locals 1
.limit stack 1
.end method

.method private c(Landroid/net/Uri;)V
aload 0
aload 1
invokespecial com/teamspeak/ts3client/StartGUIFragment/b(Landroid/net/Uri;)V
return
.limit locals 2
.limit stack 2
.end method

.method static synthetic d(Lcom/teamspeak/ts3client/StartGUIFragment;)Lcom/teamspeak/ts3client/Ts3Application;
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic e(Lcom/teamspeak/ts3client/StartGUIFragment;)V
aload 0
invokespecial com/teamspeak/ts3client/StartGUIFragment/r()V
return
.limit locals 1
.limit stack 1
.end method

.method static synthetic f(Lcom/teamspeak/ts3client/StartGUIFragment;)Landroid/view/View;
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/M Landroid/view/View;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic g(Lcom/teamspeak/ts3client/StartGUIFragment;)V
getstatic android/os/Build$VERSION/SDK_INT I
bipush 23
if_icmpge L0
aload 0
invokespecial com/teamspeak/ts3client/StartGUIFragment/w()V
return
L0:
aload 0
ldc "android.permission.RECORD_AUDIO"
invokestatic android/support/v4/c/h/a(Landroid/content/Context;Ljava/lang/String;)I
ifeq L1
aload 0
iconst_1
anewarray java/lang/String
dup
iconst_0
ldc "android.permission.RECORD_AUDIO"
aastore
iconst_1
invokestatic android/support/v4/app/m/a(Landroid/app/Activity;[Ljava/lang/String;I)V
return
L1:
aload 0
invokespecial com/teamspeak/ts3client/StartGUIFragment/w()V
aload 0
invokespecial com/teamspeak/ts3client/StartGUIFragment/L()V
return
.limit locals 1
.limit stack 5
.end method

.method static synthetic h(Lcom/teamspeak/ts3client/StartGUIFragment;)V
.catch android/content/pm/PackageManager$NameNotFoundException from L0 to L1 using L2
.catch java/lang/NullPointerException from L0 to L1 using L3
iconst_0
istore 1
aload 0
ldc "preferences"
iconst_0
invokevirtual com/teamspeak/ts3client/StartGUIFragment/getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;
astore 4
aload 4
ldc "version"
iconst_m1
invokeinterface android/content/SharedPreferences/getInt(Ljava/lang/String;I)I 2
istore 3
L0:
aload 0
invokevirtual com/teamspeak/ts3client/StartGUIFragment/getPackageManager()Landroid/content/pm/PackageManager;
aload 0
invokevirtual com/teamspeak/ts3client/StartGUIFragment/getPackageName()Ljava/lang/String;
iconst_0
invokevirtual android/content/pm/PackageManager/getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
getfield android/content/pm/PackageInfo/versionCode I
istore 2
L1:
iload 2
istore 1
L4:
iload 3
iload 1
if_icmpge L5
aload 4
invokeinterface android/content/SharedPreferences/edit()Landroid/content/SharedPreferences$Editor; 0
astore 4
aload 4
ldc "version"
iload 1
invokeinterface android/content/SharedPreferences$Editor/putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor; 2
pop
aload 4
invokeinterface android/content/SharedPreferences$Editor/commit()Z 0
pop
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/B Landroid/app/AlertDialog;
ifnonnull L5
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/q Lcom/teamspeak/ts3client/b;
invokevirtual com/teamspeak/ts3client/b/show()V
L5:
return
L3:
astore 5
goto L4
L2:
astore 5
goto L4
.limit locals 6
.limit stack 3
.end method

.method static synthetic i(Lcom/teamspeak/ts3client/StartGUIFragment;)V
aload 0
ldc_w 2131493316
invokevirtual com/teamspeak/ts3client/StartGUIFragment/findViewById(I)Landroid/view/View;
astore 1
aload 0
ldc_w 2130968590
invokestatic android/view/animation/AnimationUtils/loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;
astore 2
aload 2
ldc2_w 2000L
invokevirtual android/view/animation/Animation/setDuration(J)V
aload 2
new com/teamspeak/ts3client/ci
dup
aload 0
aload 2
invokespecial com/teamspeak/ts3client/ci/<init>(Lcom/teamspeak/ts3client/StartGUIFragment;Landroid/view/animation/Animation;)V
invokevirtual android/view/animation/Animation/setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V
aload 0
new com/teamspeak/ts3client/ck
dup
aload 0
aload 1
aload 2
invokespecial com/teamspeak/ts3client/ck/<init>(Lcom/teamspeak/ts3client/StartGUIFragment;Landroid/view/View;Landroid/view/animation/Animation;)V
invokevirtual com/teamspeak/ts3client/StartGUIFragment/runOnUiThread(Ljava/lang/Runnable;)V
aload 1
new com/teamspeak/ts3client/cl
dup
aload 0
invokespecial com/teamspeak/ts3client/cl/<init>(Lcom/teamspeak/ts3client/StartGUIFragment;)V
invokevirtual android/view/View/setOnTouchListener(Landroid/view/View$OnTouchListener;)V
return
.limit locals 3
.limit stack 6
.end method

.method static synthetic j(Lcom/teamspeak/ts3client/StartGUIFragment;)I
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/D I
ireturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic k(Lcom/teamspeak/ts3client/StartGUIFragment;)Landroid/os/Handler;
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/J Landroid/os/Handler;
areturn
.limit locals 1
.limit stack 1
.end method

.method private p()Lcom/teamspeak/ts3client/data/b/a;
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/o Lcom/teamspeak/ts3client/bookmark/e;
getfield com/teamspeak/ts3client/bookmark/e/b Lcom/teamspeak/ts3client/data/b/a;
areturn
.limit locals 1
.limit stack 1
.end method

.method private q()V
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/P Lcom/teamspeak/ts3client/d/t;
ifnull L0
return
L0:
invokestatic com/teamspeak/ts3client/data/af/a()Lcom/teamspeak/ts3client/data/af;
getfield com/teamspeak/ts3client/data/af/a I
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
ldc "LicenseAgreement"
iconst_0
invokeinterface android/content/SharedPreferences/getInt(Ljava/lang/String;I)I 2
if_icmpeq L1
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
ifnonnull L1
aload 0
new com/teamspeak/ts3client/d/t
dup
aload 0
invokespecial com/teamspeak/ts3client/d/t/<init>(Landroid/content/Context;)V
putfield com/teamspeak/ts3client/StartGUIFragment/P Lcom/teamspeak/ts3client/d/t;
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/P Lcom/teamspeak/ts3client/d/t;
iconst_0
invokevirtual com/teamspeak/ts3client/d/t/setCancelable(Z)V
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/P Lcom/teamspeak/ts3client/d/t;
invokestatic com/teamspeak/ts3client/data/af/a()Lcom/teamspeak/ts3client/data/af;
getfield com/teamspeak/ts3client/data/af/a I
putfield com/teamspeak/ts3client/d/t/c I
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/P Lcom/teamspeak/ts3client/d/t;
new com/teamspeak/ts3client/ce
dup
aload 0
invokespecial com/teamspeak/ts3client/ce/<init>(Lcom/teamspeak/ts3client/StartGUIFragment;)V
putfield com/teamspeak/ts3client/d/t/a Lcom/teamspeak/ts3client/d/k;
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/P Lcom/teamspeak/ts3client/d/t;
new com/teamspeak/ts3client/cf
dup
aload 0
invokespecial com/teamspeak/ts3client/cf/<init>(Lcom/teamspeak/ts3client/StartGUIFragment;)V
putfield com/teamspeak/ts3client/d/t/b Lcom/teamspeak/ts3client/d/k;
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/P Lcom/teamspeak/ts3client/d/t;
new com/teamspeak/ts3client/ch
dup
aload 0
invokespecial com/teamspeak/ts3client/ch/<init>(Lcom/teamspeak/ts3client/StartGUIFragment;)V
putfield com/teamspeak/ts3client/d/t/d Lcom/teamspeak/ts3client/d/k;
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/P Lcom/teamspeak/ts3client/d/t;
astore 1
aload 1
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
ldc "lang_tag"
invokestatic java/util/Locale/getDefault()Ljava/util/Locale;
invokevirtual java/util/Locale/getLanguage()Ljava/lang/String;
invokeinterface android/content/SharedPreferences/getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String; 2
putfield com/teamspeak/ts3client/d/t/f Ljava/lang/String;
aload 1
aload 0
putfield com/teamspeak/ts3client/d/t/e Lcom/teamspeak/ts3client/d/l;
aload 1
invokevirtual com/teamspeak/ts3client/d/t/b()V
return
L1:
aload 0
invokespecial com/teamspeak/ts3client/StartGUIFragment/r()V
return
.limit locals 2
.limit stack 4
.end method

.method private r()V
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual com/teamspeak/ts3client/StartGUIFragment/getContentResolver()Landroid/content/ContentResolver;
ldc "android_id"
invokestatic android/provider/Settings$Secure/getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual com/teamspeak/ts3client/StartGUIFragment/getBaseContext()Landroid/content/Context;
invokestatic com/teamspeak/ts3client/StartGUIFragment/a(Landroid/content/Context;)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 1
aload 0
new com/teamspeak/ts3client/cm
dup
aload 0
iconst_0
invokespecial com/teamspeak/ts3client/cm/<init>(Lcom/teamspeak/ts3client/StartGUIFragment;B)V
putfield com/teamspeak/ts3client/StartGUIFragment/K Lcom/teamspeak/ts3client/cm;
aload 0
new com/a/a/a/a/k
dup
aload 0
new com/a/a/a/a/v
dup
aload 0
new com/a/a/a/a/a
dup
getstatic com/teamspeak/ts3client/StartGUIFragment/u [B
aload 0
invokevirtual com/teamspeak/ts3client/StartGUIFragment/getPackageName()Ljava/lang/String;
aload 1
invokespecial com/a/a/a/a/a/<init>([BLjava/lang/String;Ljava/lang/String;)V
invokespecial com/a/a/a/a/v/<init>(Landroid/content/Context;Lcom/a/a/a/a/r;)V
ldc "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAo0t0xR6Ch8m7hhrWgMBjBBdSaOA/wFTSk27DMKnqsYrw/AgxJjK07KyxMnRYojLsX34Cy3TCmPbcK3V9mKCjwlakgBYTObiVKRnnF1wu20WmsOb0wjbRbhYGDbhmAwc7Sy8sXNehiuz/O+PTSHcwWvHL20zum2cmHDmCAjREb1ZlA4myNN/tY9DYNMkQjOpMqgOCIQbyXdJLinOzwrgeX7x2IXMOJ7bbxWEmuyRXviOijp/1hQfpweo6slwfuc9iQDPBQl77M9dObMUrr+/23pxEE2i+U8WsePwTIw4uxkyEjew3ZRkVmFTtTqdp77tcOOFJ2hn4KkhLkM0hSaLLRQIDAQAB"
invokespecial com/a/a/a/a/k/<init>(Landroid/content/Context;Lcom/a/a/a/a/s;Ljava/lang/String;)V
putfield com/teamspeak/ts3client/StartGUIFragment/L Lcom/a/a/a/a/k;
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/L Lcom/a/a/a/a/k;
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/K Lcom/teamspeak/ts3client/cm;
invokevirtual com/a/a/a/a/k/a(Lcom/a/a/a/a/o;)V
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/L Lcom/a/a/a/a/k;
astore 1
aload 1
iconst_0
putfield com/a/a/a/a/k/a I
aload 1
aload 1
getfield com/a/a/a/a/k/a I
bipush 16
iadd
putfield com/a/a/a/a/k/a I
return
.limit locals 2
.limit stack 12
.end method

.method private s()V
aload 0
ldc_w 2131493316
invokevirtual com/teamspeak/ts3client/StartGUIFragment/findViewById(I)Landroid/view/View;
astore 1
aload 0
ldc_w 2130968590
invokestatic android/view/animation/AnimationUtils/loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;
astore 2
aload 2
ldc2_w 2000L
invokevirtual android/view/animation/Animation/setDuration(J)V
aload 2
new com/teamspeak/ts3client/ci
dup
aload 0
aload 2
invokespecial com/teamspeak/ts3client/ci/<init>(Lcom/teamspeak/ts3client/StartGUIFragment;Landroid/view/animation/Animation;)V
invokevirtual android/view/animation/Animation/setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V
aload 0
new com/teamspeak/ts3client/ck
dup
aload 0
aload 1
aload 2
invokespecial com/teamspeak/ts3client/ck/<init>(Lcom/teamspeak/ts3client/StartGUIFragment;Landroid/view/View;Landroid/view/animation/Animation;)V
invokevirtual com/teamspeak/ts3client/StartGUIFragment/runOnUiThread(Ljava/lang/Runnable;)V
aload 1
new com/teamspeak/ts3client/cl
dup
aload 0
invokespecial com/teamspeak/ts3client/cl/<init>(Lcom/teamspeak/ts3client/StartGUIFragment;)V
invokevirtual android/view/View/setOnTouchListener(Landroid/view/View$OnTouchListener;)V
return
.limit locals 3
.limit stack 6
.end method

.method private t()V
aload 0
invokevirtual com/teamspeak/ts3client/StartGUIFragment/getIntent()Landroid/content/Intent;
ldc "UrlStart"
invokevirtual android/content/Intent/hasExtra(Ljava/lang/String;)Z
ifeq L0
aload 0
invokevirtual com/teamspeak/ts3client/StartGUIFragment/getIntent()Landroid/content/Intent;
invokevirtual android/content/Intent/getExtras()Landroid/os/Bundle;
ldc "uri"
invokevirtual android/os/Bundle/get(Ljava/lang/String;)Ljava/lang/Object;
checkcast android/net/Uri
astore 1
new android/app/AlertDialog$Builder
dup
aload 0
invokespecial android/app/AlertDialog$Builder/<init>(Landroid/content/Context;)V
invokevirtual android/app/AlertDialog$Builder/create()Landroid/app/AlertDialog;
astore 2
aload 2
ldc "gui.extern.info"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/app/AlertDialog/setTitle(Ljava/lang/CharSequence;)V
aload 2
ldc "gui.extern.text"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
invokevirtual com/teamspeak/ts3client/StartGUIFragment/getIntent()Landroid/content/Intent;
invokevirtual android/content/Intent/getExtras()Landroid/os/Bundle;
ldc "address"
invokevirtual android/os/Bundle/get(Ljava/lang/String;)Ljava/lang/Object;
aastore
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokevirtual android/app/AlertDialog/setMessage(Ljava/lang/CharSequence;)V
aload 2
iconst_m1
ldc "gui.extern.button1"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
new com/teamspeak/ts3client/bv
dup
aload 0
aload 1
aload 2
invokespecial com/teamspeak/ts3client/bv/<init>(Lcom/teamspeak/ts3client/StartGUIFragment;Landroid/net/Uri;Landroid/app/AlertDialog;)V
invokevirtual android/app/AlertDialog/setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V
aload 2
bipush -3
ldc "gui.extern.button2"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
new com/teamspeak/ts3client/bw
dup
aload 0
aload 1
aload 2
invokespecial com/teamspeak/ts3client/bw/<init>(Lcom/teamspeak/ts3client/StartGUIFragment;Landroid/net/Uri;Landroid/app/AlertDialog;)V
invokevirtual android/app/AlertDialog/setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V
aload 2
bipush -2
ldc "button.cancel"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
new com/teamspeak/ts3client/bx
dup
aload 0
aload 2
invokespecial com/teamspeak/ts3client/bx/<init>(Lcom/teamspeak/ts3client/StartGUIFragment;Landroid/app/AlertDialog;)V
invokevirtual android/app/AlertDialog/setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V
aload 2
iconst_0
invokevirtual android/app/AlertDialog/setCancelable(Z)V
aload 2
invokevirtual android/app/AlertDialog/show()V
L0:
return
.limit locals 3
.limit stack 8
.end method

.method private u()Z
.catch java/lang/UnsatisfiedLinkError from L0 to L1 using L2
L0:
invokestatic com/teamspeak/ts3client/jni/Ts3Jni/b()Lcom/teamspeak/ts3client/jni/Ts3Jni;
pop
L1:
iconst_1
ireturn
L2:
astore 1
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/d Ljava/util/logging/Logger;
getstatic java/util/logging/Level/SEVERE Ljava/util/logging/Level;
ldc "No libs found, missing CPU support!"
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;)V
iconst_0
ireturn
.limit locals 2
.limit stack 3
.end method

.method private v()V
.catch java/lang/Exception from L0 to L1 using L2
new com/teamspeak/ts3client/data/e/a
dup
aload 0
invokevirtual com/teamspeak/ts3client/StartGUIFragment/getApplicationContext()Landroid/content/Context;
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
ldc "lang_tag"
ldc ""
invokeinterface android/content/SharedPreferences/getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String; 2
invokespecial com/teamspeak/ts3client/data/e/a/<init>(Landroid/content/Context;Ljava/lang/String;)V
pop
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
ldc "sampleRec"
iconst_0
invokeinterface android/content/SharedPreferences/getInt(Ljava/lang/String;I)I 2
ldc_w 44100
if_icmple L3
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
invokeinterface android/content/SharedPreferences/edit()Landroid/content/SharedPreferences$Editor; 0
ldc "samplePlay"
invokeinterface android/content/SharedPreferences$Editor/remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor; 1
invokeinterface android/content/SharedPreferences$Editor/commit()Z 0
pop
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
invokeinterface android/content/SharedPreferences/edit()Landroid/content/SharedPreferences$Editor; 0
ldc "sampleRec"
invokeinterface android/content/SharedPreferences$Editor/remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor; 1
invokeinterface android/content/SharedPreferences$Editor/commit()Z 0
pop
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/d Ljava/util/logging/Logger;
getstatic java/util/logging/Level/INFO Ljava/util/logging/Level;
ldc "Cleared sample rates higher than 44100"
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;)V
L3:
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
ldc "samplePlay"
iconst_m1
invokeinterface android/content/SharedPreferences/getInt(Ljava/lang/String;I)I 2
ifne L4
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
invokeinterface android/content/SharedPreferences/edit()Landroid/content/SharedPreferences$Editor; 0
ldc "samplePlay"
invokeinterface android/content/SharedPreferences$Editor/remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor; 1
invokeinterface android/content/SharedPreferences$Editor/commit()Z 0
pop
L4:
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
ldc "sampleRec"
iconst_m1
invokeinterface android/content/SharedPreferences/getInt(Ljava/lang/String;I)I 2
ifne L5
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
invokeinterface android/content/SharedPreferences/edit()Landroid/content/SharedPreferences$Editor; 0
ldc "sampleRec"
invokeinterface android/content/SharedPreferences$Editor/remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor; 1
invokeinterface android/content/SharedPreferences$Editor/commit()Z 0
pop
L5:
aload 0
new com/teamspeak/ts3client/b
dup
aload 0
invokespecial com/teamspeak/ts3client/b/<init>(Landroid/content/Context;)V
putfield com/teamspeak/ts3client/StartGUIFragment/q Lcom/teamspeak/ts3client/b;
aload 0
invokestatic com/teamspeak/ts3client/data/b/f/a()Lcom/teamspeak/ts3client/data/b/f;
putfield com/teamspeak/ts3client/StartGUIFragment/z Lcom/teamspeak/ts3client/data/b/f;
aload 0
new com/teamspeak/ts3client/data/b/c
dup
aload 0
invokespecial com/teamspeak/ts3client/data/b/c/<init>(Landroid/content/Context;)V
putfield com/teamspeak/ts3client/StartGUIFragment/A Lcom/teamspeak/ts3client/data/b/c;
aload 0
new com/teamspeak/ts3client/bookmark/e
dup
invokespecial com/teamspeak/ts3client/bookmark/e/<init>()V
putfield com/teamspeak/ts3client/StartGUIFragment/o Lcom/teamspeak/ts3client/bookmark/e;
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/o Lcom/teamspeak/ts3client/bookmark/e;
aload 0
invokevirtual com/teamspeak/ts3client/StartGUIFragment/getIntent()Landroid/content/Intent;
invokevirtual android/content/Intent/getExtras()Landroid/os/Bundle;
invokevirtual com/teamspeak/ts3client/bookmark/e/e(Landroid/os/Bundle;)V
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
ldc "notification"
invokevirtual com/teamspeak/ts3client/Ts3Application/getSystemService(Ljava/lang/String;)Ljava/lang/Object;
checkcast android/app/NotificationManager
putfield com/teamspeak/ts3client/Ts3Application/l Landroid/app/NotificationManager;
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/l Landroid/app/NotificationManager;
invokevirtual android/app/NotificationManager/cancelAll()V
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
aload 0
ldc "connectivity"
invokevirtual com/teamspeak/ts3client/StartGUIFragment/getSystemService(Ljava/lang/String;)Ljava/lang/Object;
checkcast android/net/ConnectivityManager
putfield com/teamspeak/ts3client/Ts3Application/h Landroid/net/ConnectivityManager;
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/b()Lcom/teamspeak/ts3client/data/d/f;
astore 1
aload 1
new com/teamspeak/ts3client/data/d/g
dup
aload 1
aload 1
getfield com/teamspeak/ts3client/data/d/f/b Lcom/teamspeak/ts3client/Ts3Application;
ldc "activity"
invokevirtual com/teamspeak/ts3client/Ts3Application/getSystemService(Ljava/lang/String;)Ljava/lang/Object;
checkcast android/app/ActivityManager
invokevirtual android/app/ActivityManager/getMemoryClass()I
ldc_w 1048576
imul
bipush 10
idiv
invokespecial com/teamspeak/ts3client/data/d/g/<init>(Lcom/teamspeak/ts3client/data/d/f;I)V
putfield com/teamspeak/ts3client/data/d/f/a Landroid/support/v4/n/j;
aload 1
invokevirtual com/teamspeak/ts3client/data/d/f/a()V
L0:
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/g()Lcom/teamspeak/ts3client/a/p;
pop
L1:
return
L2:
astore 1
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
ldc "sound_pack"
iconst_0
invokeinterface android/content/SharedPreferences/getInt(Ljava/lang/String;I)I 2
iconst_1
if_icmpne L1
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
invokeinterface android/content/SharedPreferences/edit()Landroid/content/SharedPreferences$Editor; 0
ldc "sound_pack"
iconst_0
invokeinterface android/content/SharedPreferences$Editor/putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor; 2
invokeinterface android/content/SharedPreferences$Editor/commit()Z 0
pop
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/g()Lcom/teamspeak/ts3client/a/p;
pop
new android/app/AlertDialog$Builder
dup
aload 0
invokespecial android/app/AlertDialog$Builder/<init>(Landroid/content/Context;)V
invokevirtual android/app/AlertDialog$Builder/create()Landroid/app/AlertDialog;
astore 1
aload 1
ldc "critical.tts"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/app/AlertDialog/setTitle(Ljava/lang/CharSequence;)V
aload 1
ldc "critical.tts.text"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/app/AlertDialog/setMessage(Ljava/lang/CharSequence;)V
aload 1
bipush -3
ldc "button.ok"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
new com/teamspeak/ts3client/by
dup
aload 0
aload 1
invokespecial com/teamspeak/ts3client/by/<init>(Lcom/teamspeak/ts3client/StartGUIFragment;Landroid/app/AlertDialog;)V
invokevirtual android/app/AlertDialog/setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V
aload 1
iconst_0
invokevirtual android/app/AlertDialog/setCancelable(Z)V
aload 1
invokevirtual android/app/AlertDialog/show()V
return
.limit locals 2
.limit stack 7
.end method

.method private w()V
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
aload 0
invokevirtual com/teamspeak/ts3client/StartGUIFragment/getApplication()Landroid/app/Application;
iconst_0
iconst_0
invokestatic com/teamspeak/ts3client/a/k/a(Landroid/content/Context;II)Ljava/util/Vector;
putfield com/teamspeak/ts3client/Ts3Application/j Ljava/util/Vector;
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/d Ljava/util/logging/Logger;
getstatic java/util/logging/Level/INFO Ljava/util/logging/Level;
new java/lang/StringBuilder
dup
ldc "Found Audio RECSampleRate:"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/h()I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;)V
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
aload 0
invokevirtual com/teamspeak/ts3client/StartGUIFragment/getApplication()Landroid/app/Application;
iconst_0
invokestatic com/teamspeak/ts3client/a/k/a(Landroid/content/Context;I)Ljava/util/Vector;
putfield com/teamspeak/ts3client/Ts3Application/i Ljava/util/Vector;
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/d Ljava/util/logging/Logger;
getstatic java/util/logging/Level/INFO Ljava/util/logging/Level;
new java/lang/StringBuilder
dup
ldc "Found Audio PLAYSampleRate:"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/i()I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;)V
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/h()I
ifeq L0
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/i()I
ifne L1
L0:
aload 0
new android/app/AlertDialog$Builder
dup
aload 0
invokespecial android/app/AlertDialog$Builder/<init>(Landroid/content/Context;)V
invokevirtual android/app/AlertDialog$Builder/create()Landroid/app/AlertDialog;
putfield com/teamspeak/ts3client/StartGUIFragment/B Landroid/app/AlertDialog;
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/B Landroid/app/AlertDialog;
ldc "critical.audio"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/app/AlertDialog/setTitle(Ljava/lang/CharSequence;)V
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/B Landroid/app/AlertDialog;
ldc "critical.audio.text"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/app/AlertDialog/setMessage(Ljava/lang/CharSequence;)V
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/B Landroid/app/AlertDialog;
bipush -2
ldc "button.exit"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
new com/teamspeak/ts3client/bz
dup
aload 0
invokespecial com/teamspeak/ts3client/bz/<init>(Lcom/teamspeak/ts3client/StartGUIFragment;)V
invokevirtual android/app/AlertDialog/setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/B Landroid/app/AlertDialog;
iconst_0
invokevirtual android/app/AlertDialog/setCancelable(Z)V
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/B Landroid/app/AlertDialog;
invokevirtual android/app/AlertDialog/show()V
L1:
return
.limit locals 1
.limit stack 6
.end method

.method private x()V
aload 0
ldc_w 2131493319
invokevirtual com/teamspeak/ts3client/StartGUIFragment/findViewById(I)Landroid/view/View;
ifnull L0
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/o Lcom/teamspeak/ts3client/bookmark/e;
putfield com/teamspeak/ts3client/Ts3Application/c Landroid/support/v4/app/Fragment;
aload 0
invokevirtual com/teamspeak/ts3client/StartGUIFragment/c()Landroid/support/v4/app/bi;
invokevirtual android/support/v4/app/bi/a()Landroid/support/v4/app/cd;
ldc_w 2131493319
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/o Lcom/teamspeak/ts3client/bookmark/e;
ldc "Bookmark"
invokevirtual android/support/v4/app/cd/a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/cd;
invokevirtual android/support/v4/app/cd/i()I
pop
L0:
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/d Ljava/util/logging/Logger;
getstatic java/util/logging/Level/INFO Ljava/util/logging/Level;
ldc "Finished loading GUI"
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;)V
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
ldc "screen_rotation"
iconst_0
invokeinterface android/content/SharedPreferences/getInt(Ljava/lang/String;I)I 2
tableswitch 0
L1
L2
L3
default : L4
L4:
aload 0
iconst_m1
invokevirtual com/teamspeak/ts3client/StartGUIFragment/setRequestedOrientation(I)V
return
L1:
aload 0
iconst_m1
invokevirtual com/teamspeak/ts3client/StartGUIFragment/setRequestedOrientation(I)V
return
L2:
aload 0
iconst_1
invokevirtual com/teamspeak/ts3client/StartGUIFragment/setRequestedOrientation(I)V
return
L3:
aload 0
iconst_0
invokevirtual com/teamspeak/ts3client/StartGUIFragment/setRequestedOrientation(I)V
return
.limit locals 1
.limit stack 4
.end method

.method private y()V
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/getApplicationContext()Landroid/content/Context;
ldc "power"
invokevirtual android/content/Context/getSystemService(Ljava/lang/String;)Ljava/lang/Object;
checkcast android/os/PowerManager
iconst_1
ldc "Ts3WakeLOCK"
invokevirtual android/os/PowerManager/newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;
astore 1
aload 1
invokevirtual android/os/PowerManager$WakeLock/acquire()V
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
aload 1
putfield com/teamspeak/ts3client/Ts3Application/b Landroid/os/PowerManager$WakeLock;
return
.limit locals 2
.limit stack 3
.end method

.method private z()V
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/getApplicationContext()Landroid/content/Context;
ldc "power"
invokevirtual android/content/Context/getSystemService(Ljava/lang/String;)Ljava/lang/Object;
checkcast android/os/PowerManager
bipush 32
ldc "Ts3WakeLOCKSensor"
invokevirtual android/os/PowerManager/newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;
astore 1
aload 1
iconst_0
invokevirtual android/os/PowerManager$WakeLock/setReferenceCounted(Z)V
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
aload 1
putfield com/teamspeak/ts3client/Ts3Application/m Landroid/os/PowerManager$WakeLock;
return
.limit locals 2
.limit stack 3
.end method

.method public final a()V
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/o Lcom/teamspeak/ts3client/bookmark/e;
invokevirtual com/teamspeak/ts3client/bookmark/e/a()V
return
.limit locals 1
.limit stack 1
.end method

.method public final h()Z
aload 0
invokevirtual com/teamspeak/ts3client/StartGUIFragment/finish()V
iconst_1
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final j()V
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/G Landroid/hardware/SensorManager;
aload 0
invokevirtual android/hardware/SensorManager/unregisterListener(Landroid/hardware/SensorEventListener;)V
return
.limit locals 1
.limit stack 2
.end method

.method public final k()V
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/G Landroid/hardware/SensorManager;
aload 0
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/H Landroid/hardware/Sensor;
iconst_3
invokevirtual android/hardware/SensorManager/registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z
pop
return
.limit locals 1
.limit stack 4
.end method

.method public final l()V
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/o Lcom/teamspeak/ts3client/bookmark/e;
putfield com/teamspeak/ts3client/Ts3Application/c Landroid/support/v4/app/Fragment;
aload 0
invokevirtual com/teamspeak/ts3client/StartGUIFragment/c()Landroid/support/v4/app/bi;
invokevirtual android/support/v4/app/bi/a()Landroid/support/v4/app/cd;
astore 1
aload 1
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/c Landroid/support/v4/app/Fragment;
invokevirtual android/support/v4/app/cd/e(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/cd;
pop
aload 1
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/k Lcom/teamspeak/ts3client/t;
ldc "Connection"
invokevirtual android/support/v4/app/cd/b(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/cd;
pop
aload 1
sipush 4097
invokevirtual android/support/v4/app/cd/a(I)Landroid/support/v4/app/cd;
pop
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/C Z
ifeq L0
aload 1
invokevirtual android/support/v4/app/cd/i()I
pop
aload 0
iconst_0
putfield com/teamspeak/ts3client/StartGUIFragment/p Z
return
L0:
aload 1
invokevirtual android/support/v4/app/cd/j()I
pop
aload 0
iconst_1
putfield com/teamspeak/ts3client/StartGUIFragment/p Z
return
.limit locals 2
.limit stack 3
.end method

.method public final m()V
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/P Lcom/teamspeak/ts3client/d/t;
ifnonnull L0
invokestatic com/teamspeak/ts3client/data/af/a()Lcom/teamspeak/ts3client/data/af;
getfield com/teamspeak/ts3client/data/af/a I
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
ldc "LicenseAgreement"
iconst_0
invokeinterface android/content/SharedPreferences/getInt(Ljava/lang/String;I)I 2
if_icmpeq L1
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
ifnonnull L1
aload 0
new com/teamspeak/ts3client/d/t
dup
aload 0
invokespecial com/teamspeak/ts3client/d/t/<init>(Landroid/content/Context;)V
putfield com/teamspeak/ts3client/StartGUIFragment/P Lcom/teamspeak/ts3client/d/t;
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/P Lcom/teamspeak/ts3client/d/t;
iconst_0
invokevirtual com/teamspeak/ts3client/d/t/setCancelable(Z)V
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/P Lcom/teamspeak/ts3client/d/t;
invokestatic com/teamspeak/ts3client/data/af/a()Lcom/teamspeak/ts3client/data/af;
getfield com/teamspeak/ts3client/data/af/a I
putfield com/teamspeak/ts3client/d/t/c I
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/P Lcom/teamspeak/ts3client/d/t;
new com/teamspeak/ts3client/ce
dup
aload 0
invokespecial com/teamspeak/ts3client/ce/<init>(Lcom/teamspeak/ts3client/StartGUIFragment;)V
putfield com/teamspeak/ts3client/d/t/a Lcom/teamspeak/ts3client/d/k;
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/P Lcom/teamspeak/ts3client/d/t;
new com/teamspeak/ts3client/cf
dup
aload 0
invokespecial com/teamspeak/ts3client/cf/<init>(Lcom/teamspeak/ts3client/StartGUIFragment;)V
putfield com/teamspeak/ts3client/d/t/b Lcom/teamspeak/ts3client/d/k;
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/P Lcom/teamspeak/ts3client/d/t;
new com/teamspeak/ts3client/ch
dup
aload 0
invokespecial com/teamspeak/ts3client/ch/<init>(Lcom/teamspeak/ts3client/StartGUIFragment;)V
putfield com/teamspeak/ts3client/d/t/d Lcom/teamspeak/ts3client/d/k;
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/P Lcom/teamspeak/ts3client/d/t;
astore 1
aload 1
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
ldc "lang_tag"
invokestatic java/util/Locale/getDefault()Ljava/util/Locale;
invokevirtual java/util/Locale/getLanguage()Ljava/lang/String;
invokeinterface android/content/SharedPreferences/getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String; 2
putfield com/teamspeak/ts3client/d/t/f Ljava/lang/String;
aload 1
aload 0
putfield com/teamspeak/ts3client/d/t/e Lcom/teamspeak/ts3client/d/l;
aload 1
invokevirtual com/teamspeak/ts3client/d/t/b()V
L0:
return
L1:
aload 0
invokespecial com/teamspeak/ts3client/StartGUIFragment/r()V
return
.limit locals 2
.limit stack 4
.end method

.method public final n()V
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/P Lcom/teamspeak/ts3client/d/t;
ifnull L0
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/P Lcom/teamspeak/ts3client/d/t;
invokevirtual com/teamspeak/ts3client/d/t/show()V
L0:
return
.limit locals 1
.limit stack 1
.end method

.method public final o()V
aload 0
invokespecial com/teamspeak/ts3client/StartGUIFragment/r()V
return
.limit locals 1
.limit stack 1
.end method

.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
return
.limit locals 3
.limit stack 0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
iload 1
lookupswitch
666 : L0
7474 : L1
default : L2
L2:
return
L0:
iload 2
iconst_m1
if_icmpne L2
iconst_2
invokestatic java/lang/System/exit(I)V
return
L1:
getstatic android/os/Build$VERSION/SDK_INT I
bipush 23
if_icmplt L2
aload 0
invokestatic android/provider/Settings/canDrawOverlays(Landroid/content/Context;)Z
ifeq L3
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
invokeinterface android/content/SharedPreferences/edit()Landroid/content/SharedPreferences$Editor; 0
ldc "android_overlay"
iconst_1
invokeinterface android/content/SharedPreferences$Editor/putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor; 2
invokeinterface android/content/SharedPreferences$Editor/commit()Z 0
pop
return
L3:
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
invokeinterface android/content/SharedPreferences/edit()Landroid/content/SharedPreferences$Editor; 0
ldc "android_overlay"
iconst_0
invokeinterface android/content/SharedPreferences$Editor/putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor; 2
invokeinterface android/content/SharedPreferences$Editor/commit()Z 0
pop
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
invokeinterface android/content/SharedPreferences/edit()Landroid/content/SharedPreferences$Editor; 0
ldc "android_overlay_setting"
iconst_0
invokeinterface android/content/SharedPreferences$Editor/putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor; 2
invokeinterface android/content/SharedPreferences$Editor/commit()Z 0
pop
return
.limit locals 4
.limit stack 3
.end method

.method public onBackPressed()V
aload 0
invokespecial android/support/v7/app/i/onBackPressed()V
return
.limit locals 1
.limit stack 1
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
aload 0
aload 1
invokespecial android/support/v7/app/i/onConfigurationChanged(Landroid/content/res/Configuration;)V
return
.limit locals 2
.limit stack 2
.end method

.method public onCreate(Landroid/os/Bundle;)V
.annotation invisible Landroid/annotation/SuppressLint;
value [s = "NewApi" 
.end annotation
.catch java/io/IOException from L0 to L1 using L2
.catch java/io/IOException from L1 to L3 using L2
.catch java/io/IOException from L4 to L5 using L2
.catch java/io/IOException from L6 to L7 using L2
.catch java/io/IOException from L8 to L9 using L2
.catch java/io/IOException from L10 to L11 using L12
.catch java/lang/Exception from L13 to L14 using L15
aload 0
aload 1
invokespecial android/support/v7/app/i/onCreate(Landroid/os/Bundle;)V
aload 0
aload 0
invokevirtual com/teamspeak/ts3client/StartGUIFragment/getApplicationContext()Landroid/content/Context;
checkcast com/teamspeak/ts3client/Ts3Application
putfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
aload 0
ldc "TS3Settings"
iconst_0
invokevirtual com/teamspeak/ts3client/StartGUIFragment/getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;
putfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/O Lcom/teamspeak/ts3client/d/c/a;
ifnonnull L16
aload 0
new com/teamspeak/ts3client/d/c/a
dup
invokespecial com/teamspeak/ts3client/d/c/a/<init>()V
putfield com/teamspeak/ts3client/StartGUIFragment/O Lcom/teamspeak/ts3client/d/c/a;
L16:
aload 0
ldc_w 2130903124
invokevirtual com/teamspeak/ts3client/StartGUIFragment/setContentView(I)V
aload 0
ldc_w 2131493318
invokevirtual com/teamspeak/ts3client/StartGUIFragment/findViewById(I)Landroid/view/View;
checkcast android/support/v7/widget/Toolbar
astore 7
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
ldc "enable_fullscreen"
iconst_1
invokeinterface android/content/SharedPreferences/getBoolean(Ljava/lang/String;Z)Z 2
ifeq L17
aload 0
invokevirtual com/teamspeak/ts3client/StartGUIFragment/getWindow()Landroid/view/Window;
sipush 1024
sipush 1024
invokevirtual android/view/Window/setFlags(II)V
L17:
aload 7
ifnull L18
aload 0
invokevirtual android/support/v7/app/ah/i()Landroid/support/v7/app/aj;
aload 7
invokevirtual android/support/v7/app/aj/a(Landroid/support/v7/widget/Toolbar;)V
L18:
getstatic android/os/Build$VERSION/SDK_INT I
bipush 21
if_icmplt L19
aload 0
invokevirtual com/teamspeak/ts3client/StartGUIFragment/getWindow()Landroid/view/Window;
ldc_w -2147483648
invokevirtual android/view/Window/addFlags(I)V
L19:
aload 0
aload 1
putfield com/teamspeak/ts3client/StartGUIFragment/y Landroid/os/Bundle;
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
aload 0
invokevirtual android/support/v7/app/ah/i()Landroid/support/v7/app/aj;
invokevirtual android/support/v7/app/aj/a()Landroid/support/v7/app/a;
putfield com/teamspeak/ts3client/Ts3Application/o Landroid/support/v7/app/a;
aload 0
invokevirtual android/support/v7/app/ah/i()Landroid/support/v7/app/aj;
invokevirtual android/support/v7/app/aj/a()Landroid/support/v7/app/a;
pop
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/o Landroid/support/v7/app/a;
ldc "TeamSpeak"
invokevirtual android/support/v7/app/a/a(Ljava/lang/CharSequence;)V
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/o Landroid/support/v7/app/a;
invokevirtual android/support/v7/app/a/n()V
ldc "com.teamspeak.ts"
invokestatic java/util/logging/Logger/getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;
astore 1
L0:
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
invokestatic android/os/Environment/getExternalStorageDirectory()Ljava/io/File;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc "/TS3/logs/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 7
aload 7
invokevirtual java/io/File/exists()Z
ifne L1
aload 7
invokevirtual java/io/File/mkdirs()Z
pop
L1:
aload 7
invokevirtual java/io/File/listFiles()[Ljava/io/File;
astore 7
invokestatic java/lang/System/currentTimeMillis()J
lstore 4
L3:
aload 7
ifnull L8
L4:
aload 7
arraylength
istore 3
L5:
iconst_0
istore 2
L20:
iload 2
iload 3
if_icmpge L8
aload 7
iload 2
aaload
astore 8
L6:
aload 8
invokevirtual java/io/File/lastModified()J
lload 4
ldc2_w 604800000L
lsub
lcmp
ifge L21
aload 8
invokevirtual java/io/File/delete()Z
pop
L7:
goto L21
L8:
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
ldc "create_debuglog"
iconst_0
invokeinterface android/content/SharedPreferences/getBoolean(Ljava/lang/String;Z)Z 2
ifeq L9
new java/util/logging/FileHandler
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
invokestatic android/os/Environment/getExternalStorageDirectory()Ljava/io/File;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc "/TS3/logs/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
new java/text/SimpleDateFormat
dup
ldc "yyyy_MM_dd_HH_mm_ss"
invokespecial java/text/SimpleDateFormat/<init>(Ljava/lang/String;)V
new java/util/Date
dup
invokespecial java/util/Date/<init>()V
invokevirtual java/text/SimpleDateFormat/format(Ljava/util/Date;)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ".log"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/util/logging/FileHandler/<init>(Ljava/lang/String;)V
astore 7
aload 7
new java/util/logging/SimpleFormatter
dup
invokespecial java/util/logging/SimpleFormatter/<init>()V
invokevirtual java/util/logging/FileHandler/setFormatter(Ljava/util/logging/Formatter;)V
aload 1
aload 7
invokevirtual java/util/logging/Logger/addHandler(Ljava/util/logging/Handler;)V
L9:
aload 1
getstatic java/util/logging/Level/ALL Ljava/util/logging/Level;
invokevirtual java/util/logging/Logger/setLevel(Ljava/util/logging/Level;)V
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
aload 1
putfield com/teamspeak/ts3client/Ts3Application/d Ljava/util/logging/Logger;
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
invokestatic android/os/Environment/getExternalStorageDirectory()Ljava/io/File;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc "/TS3/content/lang"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 1
aload 1
invokevirtual java/io/File/exists()Z
ifne L22
aload 1
invokevirtual java/io/File/mkdirs()Z
pop
L22:
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
invokestatic android/os/Environment/getExternalStorageDirectory()Ljava/io/File;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc "/TS3"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 1
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 1
invokevirtual java/io/File/getAbsoluteFile()Ljava/io/File;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc "/Update.apk"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 1
aload 1
invokevirtual java/io/File/exists()Z
ifeq L23
aload 1
invokevirtual java/io/File/isFile()Z
ifeq L23
aload 1
invokevirtual java/io/File/delete()Z
pop
L23:
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
invokestatic android/os/Environment/getExternalStorageDirectory()Ljava/io/File;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc "/TS3/cache"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 1
aload 1
invokevirtual java/io/File/exists()Z
ifne L24
aload 1
invokevirtual java/io/File/mkdirs()Z
pop
L24:
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 1
invokevirtual java/io/File/getAbsoluteFile()Ljava/io/File;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc "/.nomedia"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 1
aload 1
invokevirtual java/io/File/exists()Z
ifne L11
L10:
aload 1
invokevirtual java/io/File/createNewFile()Z
pop
L11:
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/d Ljava/util/logging/Logger;
getstatic java/util/logging/Level/INFO Ljava/util/logging/Level;
ldc "Start"
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;)V
aload 0
invokespecial com/teamspeak/ts3client/StartGUIFragment/u()Z
istore 6
aload 0
aload 0
invokevirtual com/teamspeak/ts3client/StartGUIFragment/getLayoutInflater()Landroid/view/LayoutInflater;
ldc_w 2130903123
aconst_null
invokevirtual android/view/LayoutInflater/inflate(ILandroid/view/ViewGroup;)Landroid/view/View;
putfield com/teamspeak/ts3client/StartGUIFragment/M Landroid/view/View;
aload 0
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/M Landroid/view/View;
new android/view/ViewGroup$LayoutParams
dup
iconst_m1
iconst_m1
invokespecial android/view/ViewGroup$LayoutParams/<init>(II)V
invokevirtual com/teamspeak/ts3client/StartGUIFragment/addContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
iload 6
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/lang/Boolean/booleanValue()Z
ifne L25
aload 0
new android/app/AlertDialog$Builder
dup
aload 0
invokespecial android/app/AlertDialog$Builder/<init>(Landroid/content/Context;)V
invokevirtual android/app/AlertDialog$Builder/create()Landroid/app/AlertDialog;
putfield com/teamspeak/ts3client/StartGUIFragment/B Landroid/app/AlertDialog;
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/B Landroid/app/AlertDialog;
ldc "Not supported"
invokevirtual android/app/AlertDialog/setTitle(Ljava/lang/CharSequence;)V
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/B Landroid/app/AlertDialog;
ldc "The device you are using seems to be an ARMv6 device, but TeamSpeak3 requires at least an ARMv7 device.\nIf you think this is an error on our side please visit our forum @ http://forum.teamspeak.com"
invokevirtual android/app/AlertDialog/setMessage(Ljava/lang/CharSequence;)V
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/B Landroid/app/AlertDialog;
bipush -2
ldc "EXIT"
new com/teamspeak/ts3client/cd
dup
aload 0
invokespecial com/teamspeak/ts3client/cd/<init>(Lcom/teamspeak/ts3client/StartGUIFragment;)V
invokevirtual android/app/AlertDialog/setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/B Landroid/app/AlertDialog;
iconst_0
invokevirtual android/app/AlertDialog/setCancelable(Z)V
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/B Landroid/app/AlertDialog;
invokevirtual android/app/AlertDialog/show()V
return
L12:
astore 1
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/d Ljava/util/logging/Logger;
getstatic java/util/logging/Level/SEVERE Ljava/util/logging/Level;
ldc "Could not create .nomedia File"
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;)V
goto L11
L25:
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
aload 0
putfield com/teamspeak/ts3client/Ts3Application/f Lcom/teamspeak/ts3client/StartGUIFragment;
new com/teamspeak/ts3client/data/e/a
dup
aload 0
invokevirtual com/teamspeak/ts3client/StartGUIFragment/getApplicationContext()Landroid/content/Context;
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
ldc "lang_tag"
ldc ""
invokeinterface android/content/SharedPreferences/getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String; 2
invokespecial com/teamspeak/ts3client/data/e/a/<init>(Landroid/content/Context;Ljava/lang/String;)V
pop
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
ldc "sampleRec"
iconst_0
invokeinterface android/content/SharedPreferences/getInt(Ljava/lang/String;I)I 2
ldc_w 44100
if_icmple L26
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
invokeinterface android/content/SharedPreferences/edit()Landroid/content/SharedPreferences$Editor; 0
ldc "samplePlay"
invokeinterface android/content/SharedPreferences$Editor/remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor; 1
invokeinterface android/content/SharedPreferences$Editor/commit()Z 0
pop
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
invokeinterface android/content/SharedPreferences/edit()Landroid/content/SharedPreferences$Editor; 0
ldc "sampleRec"
invokeinterface android/content/SharedPreferences$Editor/remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor; 1
invokeinterface android/content/SharedPreferences$Editor/commit()Z 0
pop
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/d Ljava/util/logging/Logger;
getstatic java/util/logging/Level/INFO Ljava/util/logging/Level;
ldc "Cleared sample rates higher than 44100"
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;)V
L26:
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
ldc "samplePlay"
iconst_m1
invokeinterface android/content/SharedPreferences/getInt(Ljava/lang/String;I)I 2
ifne L27
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
invokeinterface android/content/SharedPreferences/edit()Landroid/content/SharedPreferences$Editor; 0
ldc "samplePlay"
invokeinterface android/content/SharedPreferences$Editor/remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor; 1
invokeinterface android/content/SharedPreferences$Editor/commit()Z 0
pop
L27:
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
ldc "sampleRec"
iconst_m1
invokeinterface android/content/SharedPreferences/getInt(Ljava/lang/String;I)I 2
ifne L28
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
invokeinterface android/content/SharedPreferences/edit()Landroid/content/SharedPreferences$Editor; 0
ldc "sampleRec"
invokeinterface android/content/SharedPreferences$Editor/remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor; 1
invokeinterface android/content/SharedPreferences$Editor/commit()Z 0
pop
L28:
aload 0
new com/teamspeak/ts3client/b
dup
aload 0
invokespecial com/teamspeak/ts3client/b/<init>(Landroid/content/Context;)V
putfield com/teamspeak/ts3client/StartGUIFragment/q Lcom/teamspeak/ts3client/b;
aload 0
invokestatic com/teamspeak/ts3client/data/b/f/a()Lcom/teamspeak/ts3client/data/b/f;
putfield com/teamspeak/ts3client/StartGUIFragment/z Lcom/teamspeak/ts3client/data/b/f;
aload 0
new com/teamspeak/ts3client/data/b/c
dup
aload 0
invokespecial com/teamspeak/ts3client/data/b/c/<init>(Landroid/content/Context;)V
putfield com/teamspeak/ts3client/StartGUIFragment/A Lcom/teamspeak/ts3client/data/b/c;
aload 0
new com/teamspeak/ts3client/bookmark/e
dup
invokespecial com/teamspeak/ts3client/bookmark/e/<init>()V
putfield com/teamspeak/ts3client/StartGUIFragment/o Lcom/teamspeak/ts3client/bookmark/e;
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/o Lcom/teamspeak/ts3client/bookmark/e;
aload 0
invokevirtual com/teamspeak/ts3client/StartGUIFragment/getIntent()Landroid/content/Intent;
invokevirtual android/content/Intent/getExtras()Landroid/os/Bundle;
invokevirtual com/teamspeak/ts3client/bookmark/e/e(Landroid/os/Bundle;)V
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
ldc "notification"
invokevirtual com/teamspeak/ts3client/Ts3Application/getSystemService(Ljava/lang/String;)Ljava/lang/Object;
checkcast android/app/NotificationManager
putfield com/teamspeak/ts3client/Ts3Application/l Landroid/app/NotificationManager;
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/l Landroid/app/NotificationManager;
invokevirtual android/app/NotificationManager/cancelAll()V
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
aload 0
ldc "connectivity"
invokevirtual com/teamspeak/ts3client/StartGUIFragment/getSystemService(Ljava/lang/String;)Ljava/lang/Object;
checkcast android/net/ConnectivityManager
putfield com/teamspeak/ts3client/Ts3Application/h Landroid/net/ConnectivityManager;
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/b()Lcom/teamspeak/ts3client/data/d/f;
astore 1
aload 1
new com/teamspeak/ts3client/data/d/g
dup
aload 1
aload 1
getfield com/teamspeak/ts3client/data/d/f/b Lcom/teamspeak/ts3client/Ts3Application;
ldc "activity"
invokevirtual com/teamspeak/ts3client/Ts3Application/getSystemService(Ljava/lang/String;)Ljava/lang/Object;
checkcast android/app/ActivityManager
invokevirtual android/app/ActivityManager/getMemoryClass()I
ldc_w 1048576
imul
bipush 10
idiv
invokespecial com/teamspeak/ts3client/data/d/g/<init>(Lcom/teamspeak/ts3client/data/d/f;I)V
putfield com/teamspeak/ts3client/data/d/f/a Landroid/support/v4/n/j;
aload 1
invokevirtual com/teamspeak/ts3client/data/d/f/a()V
L13:
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/g()Lcom/teamspeak/ts3client/a/p;
pop
L14:
aload 0
ldc_w 2131493319
invokevirtual com/teamspeak/ts3client/StartGUIFragment/findViewById(I)Landroid/view/View;
ifnull L29
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/o Lcom/teamspeak/ts3client/bookmark/e;
putfield com/teamspeak/ts3client/Ts3Application/c Landroid/support/v4/app/Fragment;
aload 0
invokevirtual com/teamspeak/ts3client/StartGUIFragment/c()Landroid/support/v4/app/bi;
invokevirtual android/support/v4/app/bi/a()Landroid/support/v4/app/cd;
ldc_w 2131493319
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/o Lcom/teamspeak/ts3client/bookmark/e;
ldc "Bookmark"
invokevirtual android/support/v4/app/cd/a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/cd;
invokevirtual android/support/v4/app/cd/i()I
pop
L29:
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/d Ljava/util/logging/Logger;
getstatic java/util/logging/Level/INFO Ljava/util/logging/Level;
ldc "Finished loading GUI"
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;)V
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
ldc "screen_rotation"
iconst_0
invokeinterface android/content/SharedPreferences/getInt(Ljava/lang/String;I)I 2
tableswitch 0
L30
L31
L32
default : L33
L33:
aload 0
iconst_m1
invokevirtual com/teamspeak/ts3client/StartGUIFragment/setRequestedOrientation(I)V
L34:
invokestatic com/teamspeak/ts3client/data/af/a()Lcom/teamspeak/ts3client/data/af;
aload 0
putfield com/teamspeak/ts3client/data/af/d Lcom/teamspeak/ts3client/data/z;
aload 0
aload 0
ldc "sensor"
invokevirtual com/teamspeak/ts3client/StartGUIFragment/getSystemService(Ljava/lang/String;)Ljava/lang/Object;
checkcast android/hardware/SensorManager
putfield com/teamspeak/ts3client/StartGUIFragment/G Landroid/hardware/SensorManager;
aload 0
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/G Landroid/hardware/SensorManager;
iconst_1
invokevirtual android/hardware/SensorManager/getDefaultSensor(I)Landroid/hardware/Sensor;
putfield com/teamspeak/ts3client/StartGUIFragment/H Landroid/hardware/Sensor;
aload 0
new android/os/Handler
dup
invokespecial android/os/Handler/<init>()V
putfield com/teamspeak/ts3client/StartGUIFragment/J Landroid/os/Handler;
aload 0
ldc "preferences"
iconst_0
invokevirtual com/teamspeak/ts3client/StartGUIFragment/getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;
ldc "version"
iconst_m1
invokeinterface android/content/SharedPreferences/getInt(Ljava/lang/String;I)I 2
istore 2
iload 2
bipush 12
if_icmpge L35
aload 0
ldc "com.android.vending.licensing.ServerManagedPolicy"
iconst_0
invokevirtual com/teamspeak/ts3client/StartGUIFragment/getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;
invokeinterface android/content/SharedPreferences/edit()Landroid/content/SharedPreferences$Editor; 0
invokeinterface android/content/SharedPreferences$Editor/clear()Landroid/content/SharedPreferences$Editor; 0
invokeinterface android/content/SharedPreferences$Editor/commit()Z 0
pop
L35:
iload 2
bipush 57
if_icmpge L36
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
invokeinterface android/content/SharedPreferences/edit()Landroid/content/SharedPreferences$Editor; 0
ldc "samplePlay"
invokeinterface android/content/SharedPreferences$Editor/remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor; 1
invokeinterface android/content/SharedPreferences$Editor/commit()Z 0
pop
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
invokeinterface android/content/SharedPreferences/edit()Landroid/content/SharedPreferences$Editor; 0
ldc "sampleRec"
invokeinterface android/content/SharedPreferences$Editor/remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor; 1
invokeinterface android/content/SharedPreferences$Editor/commit()Z 0
pop
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/d Ljava/util/logging/Logger;
getstatic java/util/logging/Level/INFO Ljava/util/logging/Level;
ldc "Cleared pre 57 audio settings setting"
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;)V
L36:
iload 2
bipush 70
if_icmpge L37
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
invokeinterface android/content/SharedPreferences/edit()Landroid/content/SharedPreferences$Editor; 0
ldc "android_overlay"
invokeinterface android/content/SharedPreferences$Editor/remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor; 1
invokeinterface android/content/SharedPreferences$Editor/commit()Z 0
pop
L37:
aload 0
new com/teamspeak/ts3client/co
dup
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
invokespecial com/teamspeak/ts3client/co/<init>(Lcom/teamspeak/ts3client/Ts3Application;)V
putfield com/teamspeak/ts3client/StartGUIFragment/E Lcom/teamspeak/ts3client/co;
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/E Lcom/teamspeak/ts3client/co;
invokeinterface android/content/SharedPreferences/registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V 1
return
L15:
astore 1
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
ldc "sound_pack"
iconst_0
invokeinterface android/content/SharedPreferences/getInt(Ljava/lang/String;I)I 2
iconst_1
if_icmpne L14
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
invokeinterface android/content/SharedPreferences/edit()Landroid/content/SharedPreferences$Editor; 0
ldc "sound_pack"
iconst_0
invokeinterface android/content/SharedPreferences$Editor/putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor; 2
invokeinterface android/content/SharedPreferences$Editor/commit()Z 0
pop
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/g()Lcom/teamspeak/ts3client/a/p;
pop
new android/app/AlertDialog$Builder
dup
aload 0
invokespecial android/app/AlertDialog$Builder/<init>(Landroid/content/Context;)V
invokevirtual android/app/AlertDialog$Builder/create()Landroid/app/AlertDialog;
astore 1
aload 1
ldc "critical.tts"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/app/AlertDialog/setTitle(Ljava/lang/CharSequence;)V
aload 1
ldc "critical.tts.text"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/app/AlertDialog/setMessage(Ljava/lang/CharSequence;)V
aload 1
bipush -3
ldc "button.ok"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
new com/teamspeak/ts3client/by
dup
aload 0
aload 1
invokespecial com/teamspeak/ts3client/by/<init>(Lcom/teamspeak/ts3client/StartGUIFragment;Landroid/app/AlertDialog;)V
invokevirtual android/app/AlertDialog/setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V
aload 1
iconst_0
invokevirtual android/app/AlertDialog/setCancelable(Z)V
aload 1
invokevirtual android/app/AlertDialog/show()V
goto L14
L30:
aload 0
iconst_m1
invokevirtual com/teamspeak/ts3client/StartGUIFragment/setRequestedOrientation(I)V
goto L34
L31:
aload 0
iconst_1
invokevirtual com/teamspeak/ts3client/StartGUIFragment/setRequestedOrientation(I)V
goto L34
L32:
aload 0
iconst_0
invokevirtual com/teamspeak/ts3client/StartGUIFragment/setRequestedOrientation(I)V
goto L34
L2:
astore 7
goto L9
L21:
iload 2
iconst_1
iadd
istore 2
goto L20
.limit locals 9
.limit stack 7
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
aload 1
invokeinterface android/view/Menu/clear()V 0
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/O Lcom/teamspeak/ts3client/d/c/a;
aload 1
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/d/c/a/a(Landroid/view/Menu;Lcom/teamspeak/ts3client/Ts3Application;)V
iconst_1
ireturn
.limit locals 2
.limit stack 3
.end method

.method protected onDestroy()V
aload 0
invokespecial android/support/v7/app/i/onDestroy()V
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/k Landroid/content/Intent;
ifnull L0
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/k Landroid/content/Intent;
invokevirtual com/teamspeak/ts3client/Ts3Application/stopService(Landroid/content/Intent;)Z
pop
L0:
invokestatic com/teamspeak/ts3client/jni/Ts3Jni/c()V
return
.limit locals 1
.limit stack 2
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
invokestatic com/teamspeak/ts3client/data/v/a()Lcom/teamspeak/ts3client/data/v;
aload 2
invokevirtual com/teamspeak/ts3client/data/v/a(Landroid/view/KeyEvent;)Z
ifeq L0
L1:
iconst_1
ireturn
L0:
iload 1
lookupswitch
4 : L2
24 : L3
25 : L4
default : L5
L5:
aload 0
iload 1
aload 2
invokespecial android/support/v7/app/i/onKeyDown(ILandroid/view/KeyEvent;)Z
ireturn
L3:
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
ifnull L6
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/b Lcom/teamspeak/ts3client/a/k;
iconst_1
invokevirtual com/teamspeak/ts3client/a/k/a(Z)V
iconst_1
ireturn
L6:
iconst_0
ireturn
L4:
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
ifnull L7
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/b Lcom/teamspeak/ts3client/a/k;
iconst_0
invokevirtual com/teamspeak/ts3client/a/k/a(Z)V
iconst_1
ireturn
L7:
iconst_0
ireturn
L2:
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
ifnull L8
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/c Landroid/support/v4/app/Fragment;
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/k Lcom/teamspeak/ts3client/t;
if_acmpne L9
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/l Lcom/teamspeak/ts3client/ConnectionBackground;
getfield com/teamspeak/ts3client/ConnectionBackground/g Z
ifeq L9
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/k Lcom/teamspeak/ts3client/t;
astore 2
new android/app/AlertDialog$Builder
dup
aload 2
invokevirtual com/teamspeak/ts3client/t/i()Landroid/support/v4/app/bb;
invokespecial android/app/AlertDialog$Builder/<init>(Landroid/content/Context;)V
invokevirtual android/app/AlertDialog$Builder/create()Landroid/app/AlertDialog;
astore 3
aload 3
invokestatic com/teamspeak/ts3client/data/d/q/a(Landroid/app/Dialog;)V
aload 3
ldc "disconnect.info"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/app/AlertDialog/setTitle(Ljava/lang/CharSequence;)V
aload 3
ldc "disconnect.text"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/app/AlertDialog/setMessage(Ljava/lang/CharSequence;)V
aload 3
iconst_m1
ldc "disconnect.button"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
new com/teamspeak/ts3client/az
dup
aload 2
invokespecial com/teamspeak/ts3client/az/<init>(Lcom/teamspeak/ts3client/t;)V
invokevirtual android/app/AlertDialog/setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V
aload 3
bipush -2
ldc "button.cancel"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
new com/teamspeak/ts3client/ba
dup
aload 2
aload 3
invokespecial com/teamspeak/ts3client/ba/<init>(Lcom/teamspeak/ts3client/t;Landroid/app/AlertDialog;)V
invokevirtual android/app/AlertDialog/setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V
aload 3
iconst_0
invokevirtual android/app/AlertDialog/setCancelable(Z)V
aload 3
invokevirtual android/app/AlertDialog/show()V
iconst_1
ireturn
L9:
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/c Landroid/support/v4/app/Fragment;
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/k Lcom/teamspeak/ts3client/t;
if_acmpne L10
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/l Lcom/teamspeak/ts3client/ConnectionBackground;
getfield com/teamspeak/ts3client/ConnectionBackground/g Z
ifne L10
aload 0
iload 1
aload 2
invokespecial android/support/v7/app/i/onKeyDown(ILandroid/view/KeyEvent;)Z
ireturn
L10:
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/c Landroid/support/v4/app/Fragment;
instanceof com/teamspeak/ts3client/bookmark/a
ifeq L11
aload 0
iload 1
aload 2
invokespecial android/support/v7/app/i/onKeyDown(ILandroid/view/KeyEvent;)Z
ireturn
L11:
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/c Landroid/support/v4/app/Fragment;
instanceof com/teamspeak/ts3client/h
ifeq L12
aload 0
iload 1
aload 2
invokespecial android/support/v7/app/i/onKeyDown(ILandroid/view/KeyEvent;)Z
ireturn
L12:
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/c Landroid/support/v4/app/Fragment;
instanceof com/teamspeak/ts3client/c
ifeq L13
aload 0
iload 1
aload 2
invokespecial android/support/v7/app/i/onKeyDown(ILandroid/view/KeyEvent;)Z
ireturn
L13:
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/c Landroid/support/v4/app/Fragment;
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/o Lcom/teamspeak/ts3client/bookmark/e;
if_acmpne L14
aload 0
iload 1
aload 2
invokespecial android/support/v7/app/i/onKeyDown(ILandroid/view/KeyEvent;)Z
ireturn
L14:
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/c Landroid/support/v4/app/Fragment;
instanceof com/teamspeak/ts3client/f/as
ifeq L15
aload 0
iload 1
aload 2
invokespecial android/support/v7/app/i/onKeyDown(ILandroid/view/KeyEvent;)Z
ireturn
L15:
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/c Landroid/support/v4/app/Fragment;
instanceof com/teamspeak/ts3client/c/b
ifeq L16
aload 0
iload 1
aload 2
invokespecial android/support/v7/app/i/onKeyDown(ILandroid/view/KeyEvent;)Z
ireturn
L16:
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/c Landroid/support/v4/app/Fragment;
instanceof com/teamspeak/ts3client/e/b
ifeq L17
aload 0
iload 1
aload 2
invokespecial android/support/v7/app/i/onKeyDown(ILandroid/view/KeyEvent;)Z
ireturn
L17:
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/c Landroid/support/v4/app/Fragment;
instanceof com/teamspeak/ts3client/d/d/i
ifeq L18
aload 0
iload 1
aload 2
invokespecial android/support/v7/app/i/onKeyDown(ILandroid/view/KeyEvent;)Z
ireturn
L18:
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/c Landroid/support/v4/app/Fragment;
instanceof com/teamspeak/ts3client/chat/c
ifeq L19
aload 0
iload 1
aload 2
invokespecial android/support/v7/app/i/onKeyDown(ILandroid/view/KeyEvent;)Z
ireturn
L19:
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/c Landroid/support/v4/app/Fragment;
instanceof com/teamspeak/ts3client/chat/j
ifeq L20
aload 0
iload 1
aload 2
invokespecial android/support/v7/app/i/onKeyDown(ILandroid/view/KeyEvent;)Z
ireturn
L20:
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/c Landroid/support/v4/app/Fragment;
instanceof com/teamspeak/ts3client/b/b
ifeq L1
aload 0
iload 1
aload 2
invokespecial android/support/v7/app/i/onKeyDown(ILandroid/view/KeyEvent;)Z
ireturn
L8:
aload 0
iload 1
aload 2
invokespecial android/support/v7/app/i/onKeyDown(ILandroid/view/KeyEvent;)Z
ireturn
.limit locals 4
.limit stack 7
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
invokestatic com/teamspeak/ts3client/data/v/a()Lcom/teamspeak/ts3client/data/v;
aload 2
invokevirtual com/teamspeak/ts3client/data/v/a(Landroid/view/KeyEvent;)Z
ifeq L0
iconst_1
ireturn
L0:
aload 0
iload 1
aload 2
invokespecial android/support/v7/app/i/onKeyUp(ILandroid/view/KeyEvent;)Z
ireturn
.limit locals 3
.limit stack 3
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
aload 0
aload 1
invokespecial android/support/v7/app/i/onNewIntent(Landroid/content/Intent;)V
aload 1
ldc "bookmark"
invokevirtual android/content/Intent/hasExtra(Ljava/lang/String;)Z
ifeq L0
aload 0
aload 1
invokevirtual android/content/Intent/getExtras()Landroid/os/Bundle;
ldc "uri"
invokevirtual android/os/Bundle/get(Ljava/lang/String;)Ljava/lang/Object;
checkcast android/net/Uri
invokespecial com/teamspeak/ts3client/StartGUIFragment/b(Landroid/net/Uri;)V
L0:
return
.limit locals 2
.limit stack 3
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/O Lcom/teamspeak/ts3client/d/c/a;
aload 1
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/d/c/a/a(Landroid/view/MenuItem;Lcom/teamspeak/ts3client/Ts3Application;)Z
ireturn
.limit locals 2
.limit stack 3
.end method

.method protected onPause()V
.catch java/lang/NullPointerException from L0 to L1 using L2
aload 0
invokespecial android/support/v7/app/i/onPause()V
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
iconst_0
putfield com/teamspeak/ts3client/Ts3Application/g Z
L0:
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/l Lcom/teamspeak/ts3client/ConnectionBackground;
astore 1
invokestatic com/teamspeak/ts3client/ConnectionBackground/a()Landroid/app/Notification;
ifnull L1
new android/os/Handler
dup
invokespecial android/os/Handler/<init>()V
new com/teamspeak/ts3client/ca
dup
aload 0
invokespecial com/teamspeak/ts3client/ca/<init>(Lcom/teamspeak/ts3client/StartGUIFragment;)V
ldc2_w 500L
invokevirtual android/os/Handler/postDelayed(Ljava/lang/Runnable;J)Z
pop
L1:
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
ifnull L3
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/k Lcom/teamspeak/ts3client/t;
invokevirtual com/teamspeak/ts3client/t/A()V
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/l Lcom/teamspeak/ts3client/ConnectionBackground;
invokevirtual com/teamspeak/ts3client/ConnectionBackground/c()V
L3:
aload 0
iconst_0
putfield com/teamspeak/ts3client/StartGUIFragment/C Z
return
L2:
astore 1
goto L1
.limit locals 2
.limit stack 4
.end method

.method public onRequestPermissionsResult(I[Ljava/lang/String;[I)V
iload 1
tableswitch 1
L0
L1
L2
default : L3
L3:
return
L0:
aload 3
arraylength
ifle L4
aload 3
iconst_0
iaload
ifne L4
aload 0
invokespecial com/teamspeak/ts3client/StartGUIFragment/w()V
aload 0
invokespecial com/teamspeak/ts3client/StartGUIFragment/L()V
L1:
aload 3
arraylength
ifle L5
aload 3
iconst_0
iaload
ifne L5
aload 0
invokespecial com/teamspeak/ts3client/StartGUIFragment/M()V
L2:
aload 3
arraylength
ifle L6
aload 3
iconst_0
iaload
ifeq L3
L6:
aload 0
invokespecial com/teamspeak/ts3client/StartGUIFragment/I()V
return
L4:
aload 0
invokespecial com/teamspeak/ts3client/StartGUIFragment/I()V
goto L1
L5:
aload 0
invokespecial com/teamspeak/ts3client/StartGUIFragment/I()V
goto L2
.limit locals 4
.limit stack 2
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
aload 0
aload 1
invokespecial android/support/v7/app/i/onRestoreInstanceState(Landroid/os/Bundle;)V
return
.limit locals 2
.limit stack 2
.end method

.method protected onResume()V
aload 0
invokespecial android/support/v7/app/i/onResume()V
aload 0
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/N Ljava/lang/String;
invokestatic net/hockeyapp/android/b/a(Landroid/content/Context;Ljava/lang/String;)V
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
iconst_1
putfield com/teamspeak/ts3client/Ts3Application/g Z
new android/content/IntentFilter
dup
invokespecial android/content/IntentFilter/<init>()V
pop
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
ldc "use_proximity"
iconst_1
invokeinterface android/content/SharedPreferences/getBoolean(Ljava/lang/String;Z)Z 2
pop
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/r Z
ifeq L0
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/g Z
ifeq L1
aload 0
invokevirtual com/teamspeak/ts3client/StartGUIFragment/c()Landroid/support/v4/app/bi;
invokevirtual android/support/v4/app/bi/f()I
ifle L2
aload 0
invokevirtual com/teamspeak/ts3client/StartGUIFragment/c()Landroid/support/v4/app/bi;
invokevirtual android/support/v4/app/bi/e()V
L2:
aload 0
invokevirtual com/teamspeak/ts3client/StartGUIFragment/c()Landroid/support/v4/app/bi;
invokevirtual android/support/v4/app/bi/a()Landroid/support/v4/app/cd;
astore 1
aload 1
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/c Landroid/support/v4/app/Fragment;
invokevirtual android/support/v4/app/cd/b(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/cd;
pop
aload 1
aload 0
invokevirtual com/teamspeak/ts3client/StartGUIFragment/c()Landroid/support/v4/app/bi;
ldc "Bookmark"
invokevirtual android/support/v4/app/bi/a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;
invokevirtual android/support/v4/app/cd/f(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/cd;
pop
aload 1
sipush 8194
invokevirtual android/support/v4/app/cd/a(I)Landroid/support/v4/app/cd;
pop
aload 1
invokevirtual android/support/v4/app/cd/i()I
pop
L1:
aload 0
iconst_0
putfield com/teamspeak/ts3client/StartGUIFragment/r Z
L0:
aload 0
iconst_1
putfield com/teamspeak/ts3client/StartGUIFragment/C Z
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/p Z
ifeq L3
aload 0
invokevirtual com/teamspeak/ts3client/StartGUIFragment/c()Landroid/support/v4/app/bi;
invokevirtual android/support/v4/app/bi/f()I
ifle L4
aload 0
invokevirtual com/teamspeak/ts3client/StartGUIFragment/c()Landroid/support/v4/app/bi;
invokevirtual android/support/v4/app/bi/e()V
L4:
aload 0
invokevirtual com/teamspeak/ts3client/StartGUIFragment/c()Landroid/support/v4/app/bi;
invokevirtual android/support/v4/app/bi/a()Landroid/support/v4/app/cd;
astore 1
aload 1
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/c Landroid/support/v4/app/Fragment;
invokevirtual android/support/v4/app/cd/b(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/cd;
pop
aload 1
aload 0
invokevirtual com/teamspeak/ts3client/StartGUIFragment/c()Landroid/support/v4/app/bi;
ldc "Bookmark"
invokevirtual android/support/v4/app/bi/a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;
invokevirtual android/support/v4/app/cd/f(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/cd;
pop
aload 1
sipush 8194
invokevirtual android/support/v4/app/cd/a(I)Landroid/support/v4/app/cd;
pop
aload 1
invokevirtual android/support/v4/app/cd/i()I
pop
aload 0
invokevirtual com/teamspeak/ts3client/StartGUIFragment/l()V
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/k Lcom/teamspeak/ts3client/t;
invokevirtual com/teamspeak/ts3client/t/I()V
L3:
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/s Z
ifeq L5
aload 0
invokevirtual com/teamspeak/ts3client/StartGUIFragment/c()Landroid/support/v4/app/bi;
invokevirtual android/support/v4/app/bi/f()I
ifle L6
aload 0
invokevirtual com/teamspeak/ts3client/StartGUIFragment/c()Landroid/support/v4/app/bi;
invokevirtual android/support/v4/app/bi/e()V
L6:
aload 0
invokevirtual com/teamspeak/ts3client/StartGUIFragment/c()Landroid/support/v4/app/bi;
invokevirtual android/support/v4/app/bi/a()Landroid/support/v4/app/cd;
astore 1
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/c Landroid/support/v4/app/Fragment;
instanceof com/teamspeak/ts3client/bookmark/e
ifne L7
aload 1
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/c Landroid/support/v4/app/Fragment;
invokevirtual android/support/v4/app/cd/b(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/cd;
pop
L7:
aload 1
aload 0
invokevirtual com/teamspeak/ts3client/StartGUIFragment/c()Landroid/support/v4/app/bi;
ldc "Bookmark"
invokevirtual android/support/v4/app/bi/a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;
invokevirtual android/support/v4/app/cd/f(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/cd;
pop
aload 1
sipush 8194
invokevirtual android/support/v4/app/cd/a(I)Landroid/support/v4/app/cd;
pop
aload 1
invokevirtual android/support/v4/app/cd/i()I
pop
aload 0
iconst_0
putfield com/teamspeak/ts3client/StartGUIFragment/s Z
L5:
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
ifnull L8
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/l Lcom/teamspeak/ts3client/ConnectionBackground;
invokevirtual com/teamspeak/ts3client/ConnectionBackground/d()V
L8:
invokestatic com/teamspeak/ts3client/data/af/a()Lcom/teamspeak/ts3client/data/af;
astore 1
aload 1
getfield com/teamspeak/ts3client/data/af/e Z
ifne L9
invokestatic java/util/Calendar/getInstance()Ljava/util/Calendar;
astore 2
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
ldc "LicenseAgreement"
iconst_0
invokeinterface android/content/SharedPreferences/getInt(Ljava/lang/String;I)I 2
ifeq L10
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
ldc "la_lastcheck"
lconst_0
invokeinterface android/content/SharedPreferences/getLong(Ljava/lang/String;J)J 3
aload 2
invokevirtual java/util/Calendar/getTimeInMillis()J
ldc2_w 86400000L
lsub
lcmp
ifge L11
L10:
aload 1
invokestatic com/teamspeak/ts3client/jni/Ts3Jni/b()Lcom/teamspeak/ts3client/jni/Ts3Jni;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_android_getUpdaterURL()Ljava/lang/String;
putfield com/teamspeak/ts3client/data/af/b Ljava/lang/String;
aload 1
iconst_1
putfield com/teamspeak/ts3client/data/af/e Z
new com/teamspeak/ts3client/data/ah
dup
aload 1
iconst_0
invokespecial com/teamspeak/ts3client/data/ah/<init>(Lcom/teamspeak/ts3client/data/af;B)V
iconst_1
anewarray java/lang/String
dup
iconst_0
aload 1
getfield com/teamspeak/ts3client/data/af/b Ljava/lang/String;
aastore
invokevirtual com/teamspeak/ts3client/data/ah/execute([Ljava/lang/Object;)Landroid/os/AsyncTask;
pop
L9:
return
L11:
ldc "UpdateServerData"
ldc "skipped downloading new update info"
invokestatic android/util/Log/i(Ljava/lang/String;Ljava/lang/String;)I
pop
aload 1
getfield com/teamspeak/ts3client/data/af/d Lcom/teamspeak/ts3client/data/z;
invokeinterface com/teamspeak/ts3client/data/z/o()V 0
return
.limit locals 3
.limit stack 6
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
return
.limit locals 2
.limit stack 0
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
aload 1
getfield android/hardware/SensorEvent/values [F
iconst_0
faload
fstore 2
aload 1
getfield android/hardware/SensorEvent/values [F
iconst_1
faload
fstore 3
aload 1
getfield android/hardware/SensorEvent/values [F
iconst_2
faload
fstore 4
fload 2
fload 2
fmul
fload 3
fload 3
fmul
fadd
f2d
invokestatic java/lang/Math/sqrt(D)D
fload 4
f2d
invokestatic java/lang/Math/atan2(DD)D
ldc2_w 180.0D
dmul
ldc2_w 3.141592653589793D
ddiv
ldc2_w 65.0D
dcmpl
ifle L0
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/I I
iconst_1
if_icmpne L1
L2:
return
L1:
aload 0
iconst_1
putfield com/teamspeak/ts3client/StartGUIFragment/I I
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/m Landroid/os/PowerManager$WakeLock;
invokevirtual android/os/PowerManager$WakeLock/acquire()V
return
L0:
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/I I
ifeq L2
aload 0
iconst_0
putfield com/teamspeak/ts3client/StartGUIFragment/I I
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/m Landroid/os/PowerManager$WakeLock;
invokevirtual android/os/PowerManager$WakeLock/release()V
return
.limit locals 5
.limit stack 4
.end method

.method public sendNewBookmark(Landroid/view/View;)V
aload 0
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/d Ljava/util/logging/Logger;
getstatic java/util/logging/Level/INFO Ljava/util/logging/Level;
ldc "NewBookmark"
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;)V
new com/teamspeak/ts3client/bookmark/a
dup
getstatic com/teamspeak/ts3client/data/b/a/a Lcom/teamspeak/ts3client/data/b/a;
invokespecial com/teamspeak/ts3client/bookmark/a/<init>(Lcom/teamspeak/ts3client/data/b/a;)V
aload 0
invokevirtual com/teamspeak/ts3client/StartGUIFragment/c()Landroid/support/v4/app/bi;
ldc "AddBookmark"
invokevirtual com/teamspeak/ts3client/bookmark/a/a(Landroid/support/v4/app/bi;Ljava/lang/String;)V
return
.limit locals 2
.limit stack 3
.end method
