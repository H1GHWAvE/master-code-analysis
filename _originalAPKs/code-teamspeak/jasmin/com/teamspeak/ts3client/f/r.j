.bytecode 50.0
.class final synchronized com/teamspeak/ts3client/f/r
.super android/support/v4/app/ax
.implements com/teamspeak/ts3client/data/y

.field final 'at' Ljava/util/BitSet;

.field 'au' Landroid/view/KeyCharacterMap;

.field 'av' Z

.field final synthetic 'aw' Lcom/teamspeak/ts3client/f/p;

.field private 'ax' Landroid/widget/TextView;

.field private 'ay' Landroid/widget/CheckBox;

.method private <init>(Lcom/teamspeak/ts3client/f/p;)V
aload 0
aload 1
putfield com/teamspeak/ts3client/f/r/aw Lcom/teamspeak/ts3client/f/p;
aload 0
invokespecial android/support/v4/app/ax/<init>()V
aload 0
new java/util/BitSet
dup
invokespecial java/util/BitSet/<init>()V
putfield com/teamspeak/ts3client/f/r/at Ljava/util/BitSet;
aload 0
iconst_3
invokestatic android/view/KeyCharacterMap/load(I)Landroid/view/KeyCharacterMap;
putfield com/teamspeak/ts3client/f/r/au Landroid/view/KeyCharacterMap;
aload 0
iconst_1
putfield com/teamspeak/ts3client/f/r/av Z
return
.limit locals 2
.limit stack 3
.end method

.method synthetic <init>(Lcom/teamspeak/ts3client/f/p;B)V
aload 0
aload 1
invokespecial com/teamspeak/ts3client/f/r/<init>(Lcom/teamspeak/ts3client/f/p;)V
return
.limit locals 3
.limit stack 2
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/f/r;)Landroid/widget/CheckBox;
aload 0
getfield com/teamspeak/ts3client/f/r/ay Landroid/widget/CheckBox;
areturn
.limit locals 1
.limit stack 1
.end method

.method private a(Ljava/util/BitSet;)V
aload 1
iconst_0
invokevirtual java/util/BitSet/nextSetBit(I)I
istore 3
ldc ""
astore 4
L0:
iload 3
iflt L1
aload 0
getfield com/teamspeak/ts3client/f/r/au Landroid/view/KeyCharacterMap;
iload 3
invokevirtual android/view/KeyCharacterMap/getDisplayLabel(I)C
istore 2
iload 2
ifne L2
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " <"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 3
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc ">"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 4
L3:
aload 1
iload 3
iconst_1
iadd
invokevirtual java/util/BitSet/nextSetBit(I)I
istore 3
goto L0
L2:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 2
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 4
goto L3
L1:
aload 0
getfield com/teamspeak/ts3client/f/r/ax Landroid/widget/TextView;
aload 4
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
return
.limit locals 5
.limit stack 3
.end method

.method static synthetic b(Lcom/teamspeak/ts3client/f/r;)Landroid/widget/TextView;
aload 0
getfield com/teamspeak/ts3client/f/r/ax Landroid/widget/TextView;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
.catch java/lang/Exception from L0 to L1 using L2
aload 1
invokevirtual android/view/LayoutInflater/getContext()Landroid/content/Context;
invokevirtual android/content/Context/getApplicationContext()Landroid/content/Context;
checkcast com/teamspeak/ts3client/Ts3Application
astore 2
aload 2
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
aload 0
getfield com/teamspeak/ts3client/f/r/aw Lcom/teamspeak/ts3client/f/p;
invokestatic com/teamspeak/ts3client/f/p/a(Lcom/teamspeak/ts3client/f/p;)Ljava/lang/String;
ldc ""
invokeinterface android/content/SharedPreferences/getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String; 2
astore 6
aload 2
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
getfield com/teamspeak/ts3client/f/r/aw Lcom/teamspeak/ts3client/f/p;
invokestatic com/teamspeak/ts3client/f/p/a(Lcom/teamspeak/ts3client/f/p;)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "_intercept"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
iconst_0
invokeinterface android/content/SharedPreferences/getBoolean(Ljava/lang/String;Z)Z 2
istore 5
aload 6
invokevirtual java/lang/String/isEmpty()Z
ifne L3
aload 6
ldc ","
invokevirtual java/lang/String/split(Ljava/lang/String;)[Ljava/lang/String;
astore 6
aload 6
arraylength
istore 4
iconst_0
istore 3
L4:
iload 3
iload 4
if_icmpge L3
aload 6
iload 3
aaload
astore 7
L0:
aload 0
getfield com/teamspeak/ts3client/f/r/at Ljava/util/BitSet;
aload 7
invokestatic java/lang/Integer/parseInt(Ljava/lang/String;)I
invokevirtual java/util/BitSet/set(I)V
L1:
iload 3
iconst_1
iadd
istore 3
goto L4
L3:
new android/widget/RelativeLayout
dup
aload 1
invokevirtual android/view/LayoutInflater/getContext()Landroid/content/Context;
invokespecial android/widget/RelativeLayout/<init>(Landroid/content/Context;)V
astore 6
aload 6
new android/widget/RelativeLayout$LayoutParams
dup
iconst_m1
bipush -2
invokespecial android/widget/RelativeLayout$LayoutParams/<init>(II)V
invokevirtual android/widget/RelativeLayout/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
new android/widget/RelativeLayout$LayoutParams
dup
iconst_m1
bipush -2
invokespecial android/widget/RelativeLayout$LayoutParams/<init>(II)V
astore 8
aload 8
bipush 10
invokevirtual android/widget/RelativeLayout$LayoutParams/addRule(I)V
new android/widget/TextView
dup
aload 0
invokevirtual com/teamspeak/ts3client/f/r/h()Landroid/content/Context;
invokespecial android/widget/TextView/<init>(Landroid/content/Context;)V
astore 7
aload 7
iconst_3
invokevirtual android/widget/TextView/setId(I)V
aload 7
aload 0
getfield com/teamspeak/ts3client/f/r/aw Lcom/teamspeak/ts3client/f/p;
invokestatic com/teamspeak/ts3client/f/p/b(Lcom/teamspeak/ts3client/f/p;)Ljava/lang/String;
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
aload 6
aload 7
aload 8
invokevirtual android/widget/RelativeLayout/addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
new android/widget/RelativeLayout$LayoutParams
dup
iconst_m1
bipush -2
invokespecial android/widget/RelativeLayout$LayoutParams/<init>(II)V
astore 8
aload 8
iconst_3
aload 7
invokevirtual android/widget/TextView/getId()I
invokevirtual android/widget/RelativeLayout$LayoutParams/addRule(II)V
aload 0
new android/widget/TextView
dup
aload 1
invokevirtual android/view/LayoutInflater/getContext()Landroid/content/Context;
invokespecial android/widget/TextView/<init>(Landroid/content/Context;)V
putfield com/teamspeak/ts3client/f/r/ax Landroid/widget/TextView;
aload 0
getfield com/teamspeak/ts3client/f/r/ax Landroid/widget/TextView;
iconst_4
invokevirtual android/widget/TextView/setId(I)V
aload 0
getfield com/teamspeak/ts3client/f/r/ax Landroid/widget/TextView;
ldc "Key"
invokevirtual android/widget/TextView/setContentDescription(Ljava/lang/CharSequence;)V
aload 0
getfield com/teamspeak/ts3client/f/r/ax Landroid/widget/TextView;
ldc_w -65536
invokevirtual android/widget/TextView/setTextColor(I)V
aload 0
getfield com/teamspeak/ts3client/f/r/ax Landroid/widget/TextView;
aconst_null
iconst_1
invokevirtual android/widget/TextView/setTypeface(Landroid/graphics/Typeface;I)V
aload 0
aload 0
getfield com/teamspeak/ts3client/f/r/at Ljava/util/BitSet;
invokespecial com/teamspeak/ts3client/f/r/a(Ljava/util/BitSet;)V
aload 6
aload 0
getfield com/teamspeak/ts3client/f/r/ax Landroid/widget/TextView;
aload 8
invokevirtual android/widget/RelativeLayout/addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
new android/widget/RelativeLayout$LayoutParams
dup
iconst_m1
bipush -2
invokespecial android/widget/RelativeLayout$LayoutParams/<init>(II)V
astore 7
aload 7
iconst_3
aload 0
getfield com/teamspeak/ts3client/f/r/ax Landroid/widget/TextView;
invokevirtual android/widget/TextView/getId()I
invokevirtual android/widget/RelativeLayout$LayoutParams/addRule(II)V
aload 0
new android/widget/CheckBox
dup
aload 1
invokevirtual android/view/LayoutInflater/getContext()Landroid/content/Context;
invokespecial android/widget/CheckBox/<init>(Landroid/content/Context;)V
putfield com/teamspeak/ts3client/f/r/ay Landroid/widget/CheckBox;
aload 0
getfield com/teamspeak/ts3client/f/r/ay Landroid/widget/CheckBox;
aload 0
getfield com/teamspeak/ts3client/f/r/aw Lcom/teamspeak/ts3client/f/p;
invokestatic com/teamspeak/ts3client/f/p/c(Lcom/teamspeak/ts3client/f/p;)Ljava/lang/String;
invokevirtual android/widget/CheckBox/setText(Ljava/lang/CharSequence;)V
aload 0
getfield com/teamspeak/ts3client/f/r/ay Landroid/widget/CheckBox;
iconst_5
invokevirtual android/widget/CheckBox/setId(I)V
aload 0
getfield com/teamspeak/ts3client/f/r/ay Landroid/widget/CheckBox;
iload 5
invokevirtual android/widget/CheckBox/setChecked(Z)V
aload 6
aload 0
getfield com/teamspeak/ts3client/f/r/ay Landroid/widget/CheckBox;
aload 7
invokevirtual android/widget/RelativeLayout/addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
new android/widget/LinearLayout
dup
aload 0
invokevirtual com/teamspeak/ts3client/f/r/h()Landroid/content/Context;
invokespecial android/widget/LinearLayout/<init>(Landroid/content/Context;)V
astore 7
new android/widget/LinearLayout$LayoutParams
dup
iconst_0
bipush -2
ldc_w 0.5F
invokespecial android/widget/LinearLayout$LayoutParams/<init>(IIF)V
astore 8
new android/widget/RelativeLayout$LayoutParams
dup
iconst_m1
bipush -2
invokespecial android/widget/RelativeLayout$LayoutParams/<init>(II)V
astore 9
aload 9
iconst_3
aload 0
getfield com/teamspeak/ts3client/f/r/ay Landroid/widget/CheckBox;
invokevirtual android/widget/CheckBox/getId()I
invokevirtual android/widget/RelativeLayout$LayoutParams/addRule(II)V
new android/widget/Button
dup
aload 1
invokevirtual android/view/LayoutInflater/getContext()Landroid/content/Context;
invokespecial android/widget/Button/<init>(Landroid/content/Context;)V
astore 10
aload 10
ldc "button.save"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/widget/Button/setText(Ljava/lang/CharSequence;)V
aload 10
new com/teamspeak/ts3client/f/s
dup
aload 0
aload 2
aload 6
invokespecial com/teamspeak/ts3client/f/s/<init>(Lcom/teamspeak/ts3client/f/r;Lcom/teamspeak/ts3client/Ts3Application;Landroid/widget/RelativeLayout;)V
invokevirtual android/widget/Button/setOnClickListener(Landroid/view/View$OnClickListener;)V
new android/widget/Button
dup
aload 1
invokevirtual android/view/LayoutInflater/getContext()Landroid/content/Context;
invokespecial android/widget/Button/<init>(Landroid/content/Context;)V
astore 1
aload 1
ldc "button.clear"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/widget/Button/setText(Ljava/lang/CharSequence;)V
aload 1
new com/teamspeak/ts3client/f/t
dup
aload 0
invokespecial com/teamspeak/ts3client/f/t/<init>(Lcom/teamspeak/ts3client/f/r;)V
invokevirtual android/widget/Button/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 1
aload 8
invokevirtual android/widget/Button/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 10
aload 8
invokevirtual android/widget/Button/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 7
aload 1
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
aload 7
aload 10
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
aload 6
aload 7
aload 9
invokevirtual android/widget/RelativeLayout/addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
aload 0
getfield android/support/v4/app/ax/j Landroid/app/Dialog;
aload 0
getfield com/teamspeak/ts3client/f/r/aw Lcom/teamspeak/ts3client/f/p;
invokestatic com/teamspeak/ts3client/f/p/e(Lcom/teamspeak/ts3client/f/p;)Ljava/lang/String;
invokevirtual android/app/Dialog/setTitle(Ljava/lang/CharSequence;)V
aload 0
iconst_0
invokevirtual com/teamspeak/ts3client/f/r/a(Z)V
invokestatic com/teamspeak/ts3client/data/v/a()Lcom/teamspeak/ts3client/data/v;
aload 0
putfield com/teamspeak/ts3client/data/v/c Lcom/teamspeak/ts3client/data/y;
aload 0
getfield android/support/v4/app/ax/j Landroid/app/Dialog;
new com/teamspeak/ts3client/f/u
dup
aload 0
invokespecial com/teamspeak/ts3client/f/u/<init>(Lcom/teamspeak/ts3client/f/r;)V
invokevirtual android/app/Dialog/setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V
aload 6
areturn
L2:
astore 7
goto L1
.limit locals 11
.limit stack 6
.end method

.method public final a()V
aload 0
getfield com/teamspeak/ts3client/f/r/av Z
ifeq L0
aload 0
getfield com/teamspeak/ts3client/f/r/at Ljava/util/BitSet;
invokevirtual java/util/BitSet/clear()V
aload 0
getfield com/teamspeak/ts3client/f/r/at Ljava/util/BitSet;
invokestatic com/teamspeak/ts3client/data/v/a()Lcom/teamspeak/ts3client/data/v;
getfield com/teamspeak/ts3client/data/v/a Ljava/util/BitSet;
invokevirtual java/util/BitSet/or(Ljava/util/BitSet;)V
aload 0
aload 0
getfield com/teamspeak/ts3client/f/r/at Ljava/util/BitSet;
invokespecial com/teamspeak/ts3client/f/r/a(Ljava/util/BitSet;)V
L0:
invokestatic com/teamspeak/ts3client/data/v/a()Lcom/teamspeak/ts3client/data/v;
getfield com/teamspeak/ts3client/data/v/a Ljava/util/BitSet;
invokevirtual java/util/BitSet/isEmpty()Z
ifeq L1
aload 0
iconst_1
putfield com/teamspeak/ts3client/f/r/av Z
L1:
return
.limit locals 1
.limit stack 2
.end method

.method public final p_()V
aload 0
iconst_0
putfield com/teamspeak/ts3client/f/r/av Z
return
.limit locals 1
.limit stack 2
.end method
