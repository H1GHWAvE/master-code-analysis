.bytecode 50.0
.class public final synchronized com/teamspeak/ts3client/f/k
.super android/widget/RelativeLayout
.implements android/view/View$OnClickListener

.field private 'a' Landroid/widget/SeekBar;

.field private 'b' Landroid/widget/TextView;

.field private 'c' Ljava/lang/String;

.field private 'd' Lcom/teamspeak/ts3client/f/m;

.field private 'e' Landroid/support/v4/app/bi;

.field private 'f' Ljava/lang/String;

.field private 'g' Ljava/lang/String;

.method public <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/support/v4/app/bi;)V
aload 0
aload 1
invokespecial android/widget/RelativeLayout/<init>(Landroid/content/Context;)V
aload 0
new android/widget/RelativeLayout$LayoutParams
dup
iconst_m1
bipush -2
invokespecial android/widget/RelativeLayout$LayoutParams/<init>(II)V
invokevirtual com/teamspeak/ts3client/f/k/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 0
iconst_0
bipush 18
iconst_0
bipush 18
invokevirtual com/teamspeak/ts3client/f/k/setPadding(IIII)V
aload 0
aload 5
putfield com/teamspeak/ts3client/f/k/e Landroid/support/v4/app/bi;
aload 0
aload 3
putfield com/teamspeak/ts3client/f/k/c Ljava/lang/String;
aload 0
aload 4
putfield com/teamspeak/ts3client/f/k/g Ljava/lang/String;
aload 0
aload 2
putfield com/teamspeak/ts3client/f/k/f Ljava/lang/String;
aload 0
ldc_w 2130837686
invokevirtual com/teamspeak/ts3client/f/k/setBackgroundResource(I)V
new android/widget/TextView
dup
aload 1
invokespecial android/widget/TextView/<init>(Landroid/content/Context;)V
astore 2
new android/widget/TextView
dup
aload 1
invokespecial android/widget/TextView/<init>(Landroid/content/Context;)V
astore 1
aload 2
aload 0
getfield com/teamspeak/ts3client/f/k/c Ljava/lang/String;
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
aload 2
ldc_w 20.0F
invokevirtual android/widget/TextView/setTextSize(F)V
aload 2
aconst_null
iconst_1
invokevirtual android/widget/TextView/setTypeface(Landroid/graphics/Typeface;I)V
aload 2
iconst_1
invokevirtual android/widget/TextView/setId(I)V
aload 1
aload 0
getfield com/teamspeak/ts3client/f/k/g Ljava/lang/String;
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
aload 1
ldc_w 10.0F
invokevirtual android/widget/TextView/setTextSize(F)V
aload 1
aconst_null
iconst_2
invokevirtual android/widget/TextView/setTypeface(Landroid/graphics/Typeface;I)V
aload 1
iconst_2
invokevirtual android/widget/TextView/setId(I)V
new android/widget/RelativeLayout$LayoutParams
dup
bipush -2
bipush -2
invokespecial android/widget/RelativeLayout$LayoutParams/<init>(II)V
astore 3
aload 3
bipush 10
invokevirtual android/widget/RelativeLayout$LayoutParams/addRule(I)V
aload 0
aload 2
aload 3
invokevirtual com/teamspeak/ts3client/f/k/addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
new android/widget/RelativeLayout$LayoutParams
dup
bipush -2
bipush -2
invokespecial android/widget/RelativeLayout$LayoutParams/<init>(II)V
astore 3
aload 3
iconst_3
aload 2
invokevirtual android/widget/TextView/getId()I
invokevirtual android/widget/RelativeLayout$LayoutParams/addRule(II)V
aload 0
aload 1
aload 3
invokevirtual com/teamspeak/ts3client/f/k/addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
aload 0
iconst_1
invokevirtual com/teamspeak/ts3client/f/k/setClickable(Z)V
aload 0
aload 0
invokevirtual com/teamspeak/ts3client/f/k/setOnClickListener(Landroid/view/View$OnClickListener;)V
return
.limit locals 6
.limit stack 5
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/f/k;Landroid/widget/SeekBar;)Landroid/widget/SeekBar;
aload 0
aload 1
putfield com/teamspeak/ts3client/f/k/a Landroid/widget/SeekBar;
aload 1
areturn
.limit locals 2
.limit stack 2
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/f/k;Landroid/widget/TextView;)Landroid/widget/TextView;
aload 0
aload 1
putfield com/teamspeak/ts3client/f/k/b Landroid/widget/TextView;
aload 1
areturn
.limit locals 2
.limit stack 2
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/f/k;)Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/f/k/f Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic b(Lcom/teamspeak/ts3client/f/k;)Landroid/widget/SeekBar;
aload 0
getfield com/teamspeak/ts3client/f/k/a Landroid/widget/SeekBar;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic c(Lcom/teamspeak/ts3client/f/k;)Landroid/widget/TextView;
aload 0
getfield com/teamspeak/ts3client/f/k/b Landroid/widget/TextView;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic d(Lcom/teamspeak/ts3client/f/k;)Lcom/teamspeak/ts3client/f/m;
aload 0
getfield com/teamspeak/ts3client/f/k/d Lcom/teamspeak/ts3client/f/m;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic e(Lcom/teamspeak/ts3client/f/k;)Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/f/k/c Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final onClick(Landroid/view/View;)V
aload 0
new com/teamspeak/ts3client/f/m
dup
aload 0
iconst_0
invokespecial com/teamspeak/ts3client/f/m/<init>(Lcom/teamspeak/ts3client/f/k;B)V
putfield com/teamspeak/ts3client/f/k/d Lcom/teamspeak/ts3client/f/m;
aload 0
getfield com/teamspeak/ts3client/f/k/d Lcom/teamspeak/ts3client/f/m;
aload 0
getfield com/teamspeak/ts3client/f/k/e Landroid/support/v4/app/bi;
aload 0
getfield com/teamspeak/ts3client/f/k/c Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/f/m/a(Landroid/support/v4/app/bi;Ljava/lang/String;)V
aload 1
iconst_1
invokevirtual android/view/View/performHapticFeedback(I)Z
pop
return
.limit locals 2
.limit stack 5
.end method
