.bytecode 50.0
.class public final synchronized com/teamspeak/ts3client/t
.super android/support/v4/app/Fragment

.field private static 'i' Lcom/teamspeak/ts3client/customs/FloatingButton;

.field public 'a' Lcom/teamspeak/ts3client/Ts3Application;

.field private 'aA' Z

.field private 'aB' Lcom/teamspeak/ts3client/bf;

.field private 'aC' Z

.field private 'aD' Landroid/content/BroadcastReceiver;

.field private 'aE' Lcom/teamspeak/ts3client/d/c/d;

.field private 'aF' Z

.field private 'aG' Lcom/teamspeak/ts3client/customs/FloatingButton;

.field private 'aH' Landroid/telephony/PhoneStateListener;

.field private 'aI' Lcom/teamspeak/ts3client/customs/FloatingButton;

.field private 'at' I

.field private 'au' Z

.field private 'av' Lcom/teamspeak/ts3client/chat/c;

.field private 'aw' Z

.field private 'ax' Z

.field private 'ay' Z

.field private 'az' Z

.field 'b' Z

.field 'c' Z

.field 'd' I

.field 'e' Z

.field 'f' Z

.field 'g' Lcom/teamspeak/ts3client/customs/FloatingButton;

.field 'h' Lcom/teamspeak/ts3client/customs/FloatingButton;

.field private 'j' Lcom/teamspeak/ts3client/data/customExpandableListView;

.field private 'k' Lcom/teamspeak/ts3client/d/h;

.field private 'l' Lcom/teamspeak/ts3client/data/g;

.field private 'm' Landroid/telephony/TelephonyManager;

.method public <init>()V
aload 0
invokespecial android/support/v4/app/Fragment/<init>()V
aload 0
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
putfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
aload 0
iconst_0
putfield com/teamspeak/ts3client/t/b Z
aload 0
iconst_0
putfield com/teamspeak/ts3client/t/c Z
aload 0
iconst_2
putfield com/teamspeak/ts3client/t/at I
aload 0
iconst_0
putfield com/teamspeak/ts3client/t/aw Z
aload 0
iconst_0
putfield com/teamspeak/ts3client/t/ax Z
aload 0
bipush 22
putfield com/teamspeak/ts3client/t/d I
aload 0
new com/teamspeak/ts3client/u
dup
aload 0
invokespecial com/teamspeak/ts3client/u/<init>(Lcom/teamspeak/ts3client/t;)V
putfield com/teamspeak/ts3client/t/aD Landroid/content/BroadcastReceiver;
aload 0
iconst_0
putfield com/teamspeak/ts3client/t/aF Z
aload 0
new com/teamspeak/ts3client/ah
dup
aload 0
invokespecial com/teamspeak/ts3client/ah/<init>(Lcom/teamspeak/ts3client/t;)V
putfield com/teamspeak/ts3client/t/aH Landroid/telephony/PhoneStateListener;
return
.limit locals 1
.limit stack 4
.end method

.method static synthetic J()Lcom/teamspeak/ts3client/customs/FloatingButton;
getstatic com/teamspeak/ts3client/t/i Lcom/teamspeak/ts3client/customs/FloatingButton;
areturn
.limit locals 0
.limit stack 1
.end method

.method private K()V
aload 0
getfield com/teamspeak/ts3client/t/f Z
ifne L0
aload 0
iconst_0
putfield com/teamspeak/ts3client/t/c Z
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/g()Lcom/teamspeak/ts3client/a/p;
iconst_0
putfield com/teamspeak/ts3client/a/p/a Z
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
getstatic com/teamspeak/ts3client/jni/d/g Lcom/teamspeak/ts3client/jni/d;
iconst_0
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/a(JLcom/teamspeak/ts3client/jni/d;I)I
pop
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
ldc ""
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_flushClientSelfUpdates(JLjava/lang/String;)I
pop
aload 0
getfield com/teamspeak/ts3client/t/h Lcom/teamspeak/ts3client/customs/FloatingButton;
ldc_w 2130837654
aload 0
getfield com/teamspeak/ts3client/t/d I
i2f
aload 0
getfield com/teamspeak/ts3client/t/d I
i2f
invokestatic com/teamspeak/ts3client/data/d/t/a(IFF)Landroid/graphics/Bitmap;
invokevirtual com/teamspeak/ts3client/customs/FloatingButton/setBitmap(Landroid/graphics/Bitmap;)V
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/v Lcom/teamspeak/ts3client/data/ab;
getfield com/teamspeak/ts3client/data/ab/s Ljava/util/BitSet;
iconst_1
invokevirtual java/util/BitSet/set(I)V
L1:
aload 0
getfield com/teamspeak/ts3client/t/e Z
ifne L2
aload 0
iconst_0
putfield com/teamspeak/ts3client/t/b Z
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
getstatic com/teamspeak/ts3client/jni/d/f Lcom/teamspeak/ts3client/jni/d;
iconst_0
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/a(JLcom/teamspeak/ts3client/jni/d;I)I
pop
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
ldc ""
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_flushClientSelfUpdates(JLjava/lang/String;)I
pop
aload 0
getfield com/teamspeak/ts3client/t/g Lcom/teamspeak/ts3client/customs/FloatingButton;
ldc_w 2130837633
aload 0
getfield com/teamspeak/ts3client/t/d I
i2f
aload 0
getfield com/teamspeak/ts3client/t/d I
i2f
invokestatic com/teamspeak/ts3client/data/d/t/a(IFF)Landroid/graphics/Bitmap;
invokevirtual com/teamspeak/ts3client/customs/FloatingButton/setBitmap(Landroid/graphics/Bitmap;)V
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/v Lcom/teamspeak/ts3client/data/ab;
getfield com/teamspeak/ts3client/data/ab/s Ljava/util/BitSet;
iconst_0
invokevirtual java/util/BitSet/set(I)V
L3:
new android/os/Handler
dup
invokespecial android/os/Handler/<init>()V
new com/teamspeak/ts3client/as
dup
aload 0
invokespecial com/teamspeak/ts3client/as/<init>(Lcom/teamspeak/ts3client/t;)V
ldc2_w 1000L
invokevirtual android/os/Handler/postDelayed(Ljava/lang/Runnable;J)Z
pop
return
L0:
aload 0
getfield com/teamspeak/ts3client/t/h Lcom/teamspeak/ts3client/customs/FloatingButton;
ldc_w 2130837656
aload 0
getfield com/teamspeak/ts3client/t/d I
i2f
aload 0
getfield com/teamspeak/ts3client/t/d I
i2f
invokestatic com/teamspeak/ts3client/data/d/t/a(IFF)Landroid/graphics/Bitmap;
invokevirtual com/teamspeak/ts3client/customs/FloatingButton/setBitmap(Landroid/graphics/Bitmap;)V
goto L1
L2:
aload 0
getfield com/teamspeak/ts3client/t/g Lcom/teamspeak/ts3client/customs/FloatingButton;
ldc_w 2130837636
aload 0
getfield com/teamspeak/ts3client/t/d I
i2f
aload 0
getfield com/teamspeak/ts3client/t/d I
i2f
invokestatic com/teamspeak/ts3client/data/d/t/a(IFF)Landroid/graphics/Bitmap;
invokevirtual com/teamspeak/ts3client/customs/FloatingButton/setBitmap(Landroid/graphics/Bitmap;)V
goto L3
.limit locals 1
.limit stack 5
.end method

.method private L()V
aload 0
aload 0
getfield com/teamspeak/ts3client/t/b Z
putfield com/teamspeak/ts3client/t/e Z
aload 0
aload 0
getfield com/teamspeak/ts3client/t/c Z
putfield com/teamspeak/ts3client/t/f Z
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/g()Lcom/teamspeak/ts3client/a/p;
iconst_1
putfield com/teamspeak/ts3client/a/p/a Z
aload 0
getfield com/teamspeak/ts3client/t/g Lcom/teamspeak/ts3client/customs/FloatingButton;
ldc_w 2130837636
aload 0
getfield com/teamspeak/ts3client/t/d I
i2f
aload 0
getfield com/teamspeak/ts3client/t/d I
i2f
invokestatic com/teamspeak/ts3client/data/d/t/a(IFF)Landroid/graphics/Bitmap;
invokevirtual com/teamspeak/ts3client/customs/FloatingButton/setBitmap(Landroid/graphics/Bitmap;)V
aload 0
getfield com/teamspeak/ts3client/t/h Lcom/teamspeak/ts3client/customs/FloatingButton;
ldc_w 2130837656
aload 0
getfield com/teamspeak/ts3client/t/d I
i2f
aload 0
getfield com/teamspeak/ts3client/t/d I
i2f
invokestatic com/teamspeak/ts3client/data/d/t/a(IFF)Landroid/graphics/Bitmap;
invokevirtual com/teamspeak/ts3client/customs/FloatingButton/setBitmap(Landroid/graphics/Bitmap;)V
aload 0
iconst_1
putfield com/teamspeak/ts3client/t/c Z
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
getstatic com/teamspeak/ts3client/jni/d/g Lcom/teamspeak/ts3client/jni/d;
iconst_1
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/a(JLcom/teamspeak/ts3client/jni/d;I)I
pop
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
ldc ""
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_flushClientSelfUpdates(JLjava/lang/String;)I
pop
aload 0
iconst_1
putfield com/teamspeak/ts3client/t/b Z
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
getstatic com/teamspeak/ts3client/jni/d/f Lcom/teamspeak/ts3client/jni/d;
iconst_1
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/a(JLcom/teamspeak/ts3client/jni/d;I)I
pop
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
ldc ""
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_flushClientSelfUpdates(JLjava/lang/String;)I
pop
return
.limit locals 1
.limit stack 5
.end method

.method private M()V
new android/app/AlertDialog$Builder
dup
aload 0
invokevirtual com/teamspeak/ts3client/t/i()Landroid/support/v4/app/bb;
invokespecial android/app/AlertDialog$Builder/<init>(Landroid/content/Context;)V
invokevirtual android/app/AlertDialog$Builder/create()Landroid/app/AlertDialog;
astore 1
aload 1
invokestatic com/teamspeak/ts3client/data/d/q/a(Landroid/app/Dialog;)V
aload 1
ldc "disconnect.info"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/app/AlertDialog/setTitle(Ljava/lang/CharSequence;)V
aload 1
ldc "disconnect.text"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/app/AlertDialog/setMessage(Ljava/lang/CharSequence;)V
aload 1
iconst_m1
ldc "disconnect.button"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
new com/teamspeak/ts3client/az
dup
aload 0
invokespecial com/teamspeak/ts3client/az/<init>(Lcom/teamspeak/ts3client/t;)V
invokevirtual android/app/AlertDialog/setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V
aload 1
bipush -2
ldc "button.cancel"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
new com/teamspeak/ts3client/ba
dup
aload 0
aload 1
invokespecial com/teamspeak/ts3client/ba/<init>(Lcom/teamspeak/ts3client/t;Landroid/app/AlertDialog;)V
invokevirtual android/app/AlertDialog/setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V
aload 1
iconst_0
invokevirtual android/app/AlertDialog/setCancelable(Z)V
aload 1
invokevirtual android/app/AlertDialog/show()V
return
.limit locals 2
.limit stack 7
.end method

.method private N()V
aload 0
iconst_0
invokevirtual com/teamspeak/ts3client/t/d(Z)V
return
.limit locals 1
.limit stack 2
.end method

.method private O()Z
aload 0
getfield com/teamspeak/ts3client/t/au Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private P()V
aload 0
invokevirtual com/teamspeak/ts3client/t/i()Landroid/support/v4/app/bb;
new com/teamspeak/ts3client/ai
dup
aload 0
invokespecial com/teamspeak/ts3client/ai/<init>(Lcom/teamspeak/ts3client/t;)V
invokevirtual android/support/v4/app/bb/runOnUiThread(Ljava/lang/Runnable;)V
return
.limit locals 1
.limit stack 4
.end method

.method private Q()V
aload 0
getfield com/teamspeak/ts3client/t/aG Lcom/teamspeak/ts3client/customs/FloatingButton;
astore 1
aload 1
getfield com/teamspeak/ts3client/customs/FloatingButton/c Z
ifne L0
aload 1
iconst_1
invokevirtual com/teamspeak/ts3client/customs/FloatingButton/a(Z)V
aload 1
invokevirtual com/teamspeak/ts3client/customs/FloatingButton/invalidate()V
L0:
return
.limit locals 2
.limit stack 2
.end method

.method private R()V
aload 0
invokevirtual com/teamspeak/ts3client/t/i()Landroid/support/v4/app/bb;
new com/teamspeak/ts3client/ap
dup
aload 0
invokespecial com/teamspeak/ts3client/ap/<init>(Lcom/teamspeak/ts3client/t;)V
invokevirtual android/support/v4/app/bb/runOnUiThread(Ljava/lang/Runnable;)V
return
.limit locals 1
.limit stack 4
.end method

.method private S()V
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/v Lcom/teamspeak/ts3client/data/ab;
getfield com/teamspeak/ts3client/data/ab/j Z
ifne L0
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/l Lcom/teamspeak/ts3client/ConnectionBackground;
iconst_1
putfield com/teamspeak/ts3client/ConnectionBackground/h Z
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
ldc "SubscribeAll"
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_requestChannelSubscribeAll(JLjava/lang/String;)I
pop
L1:
return
L0:
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/l Lcom/teamspeak/ts3client/ConnectionBackground;
iconst_1
putfield com/teamspeak/ts3client/ConnectionBackground/h Z
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
ldc "UnsubscribeAll"
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_requestChannelUnsubscribeAll(JLjava/lang/String;)I
pop
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/v Lcom/teamspeak/ts3client/data/ab;
invokevirtual com/teamspeak/ts3client/data/ab/e()[J
ifnull L1
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/v Lcom/teamspeak/ts3client/data/ab;
invokevirtual com/teamspeak/ts3client/data/ab/e()[J
ldc "Subscribe after UnsubscribeAll"
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_requestChannelSubscribe(J[JLjava/lang/String;)I
pop
return
.limit locals 1
.limit stack 5
.end method

.method private T()Z
aload 0
getfield com/teamspeak/ts3client/t/ax Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private static a(F)I
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
getfield android/util/DisplayMetrics/density F
fload 0
fmul
ldc_w 0.5F
fadd
f2i
ireturn
.limit locals 1
.limit stack 2
.end method

.method private a(Landroid/text/Spanned;Ljava/lang/String;Ljava/lang/String;)V
aload 0
invokevirtual com/teamspeak/ts3client/t/i()Landroid/support/v4/app/bb;
new com/teamspeak/ts3client/ao
dup
aload 0
aload 1
aload 2
aload 3
invokespecial com/teamspeak/ts3client/ao/<init>(Lcom/teamspeak/ts3client/t;Landroid/text/Spanned;Ljava/lang/String;Ljava/lang/String;)V
invokevirtual android/support/v4/app/bb/runOnUiThread(Ljava/lang/Runnable;)V
return
.limit locals 4
.limit stack 7
.end method

.method private a([Lcom/teamspeak/ts3client/data/a;)V
aload 0
invokevirtual com/teamspeak/ts3client/t/l()Z
ifeq L0
aload 0
invokevirtual com/teamspeak/ts3client/t/i()Landroid/support/v4/app/bb;
new com/teamspeak/ts3client/v
dup
aload 0
aload 1
invokespecial com/teamspeak/ts3client/v/<init>(Lcom/teamspeak/ts3client/t;[Lcom/teamspeak/ts3client/data/a;)V
invokevirtual android/support/v4/app/bb/runOnUiThread(Ljava/lang/Runnable;)V
L0:
return
.limit locals 2
.limit stack 5
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/t;)Z
aload 0
getfield com/teamspeak/ts3client/t/aC Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/t;Z)Z
aload 0
iload 1
putfield com/teamspeak/ts3client/t/aC Z
iload 1
ireturn
.limit locals 2
.limit stack 2
.end method

.method static synthetic b(Lcom/teamspeak/ts3client/t;)Lcom/teamspeak/ts3client/Ts3Application;
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
areturn
.limit locals 1
.limit stack 1
.end method

.method private b(Lcom/teamspeak/ts3client/data/a;)V
aload 0
invokevirtual com/teamspeak/ts3client/t/l()Z
ifeq L0
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/c Landroid/support/v4/app/Fragment;
instanceof com/teamspeak/ts3client/c
ifeq L1
L0:
return
L1:
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/d Ljava/util/logging/Logger;
getstatic java/util/logging/Level/INFO Ljava/util/logging/Level;
new java/lang/StringBuilder
dup
ldc "Channelinfo: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;)V
getstatic com/teamspeak/ts3client/c/b Lcom/teamspeak/ts3client/data/a;
aload 1
if_acmpne L2
getstatic com/teamspeak/ts3client/c/a Lcom/teamspeak/ts3client/c;
astore 1
L3:
aload 0
getfield android/support/v4/app/Fragment/M Landroid/support/v4/app/bl;
invokevirtual android/support/v4/app/bi/a()Landroid/support/v4/app/cd;
astore 2
aload 2
aload 1
ldc "ChannelInfo"
invokevirtual android/support/v4/app/cd/b(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/cd;
pop
aload 2
invokevirtual android/support/v4/app/cd/f()Landroid/support/v4/app/cd;
pop
aload 2
sipush 4097
invokevirtual android/support/v4/app/cd/a(I)Landroid/support/v4/app/cd;
pop
aload 2
invokevirtual android/support/v4/app/cd/i()I
pop
return
L2:
new com/teamspeak/ts3client/c
dup
invokespecial com/teamspeak/ts3client/c/<init>()V
putstatic com/teamspeak/ts3client/c/a Lcom/teamspeak/ts3client/c;
aload 1
putstatic com/teamspeak/ts3client/c/b Lcom/teamspeak/ts3client/data/a;
getstatic com/teamspeak/ts3client/c/a Lcom/teamspeak/ts3client/c;
astore 1
goto L3
.limit locals 3
.limit stack 5
.end method

.method static synthetic c(Lcom/teamspeak/ts3client/t;)Lcom/teamspeak/ts3client/d/h;
aload 0
getfield com/teamspeak/ts3client/t/k Lcom/teamspeak/ts3client/d/h;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic d(Lcom/teamspeak/ts3client/t;)Lcom/teamspeak/ts3client/data/g;
aload 0
getfield com/teamspeak/ts3client/t/l Lcom/teamspeak/ts3client/data/g;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic e(Lcom/teamspeak/ts3client/t;)Lcom/teamspeak/ts3client/data/customExpandableListView;
aload 0
getfield com/teamspeak/ts3client/t/j Lcom/teamspeak/ts3client/data/customExpandableListView;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic f(Lcom/teamspeak/ts3client/t;)V
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/v Lcom/teamspeak/ts3client/data/ab;
getfield com/teamspeak/ts3client/data/ab/j Z
ifne L0
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/l Lcom/teamspeak/ts3client/ConnectionBackground;
iconst_1
putfield com/teamspeak/ts3client/ConnectionBackground/h Z
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
ldc "SubscribeAll"
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_requestChannelSubscribeAll(JLjava/lang/String;)I
pop
L1:
return
L0:
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/l Lcom/teamspeak/ts3client/ConnectionBackground;
iconst_1
putfield com/teamspeak/ts3client/ConnectionBackground/h Z
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
ldc "UnsubscribeAll"
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_requestChannelUnsubscribeAll(JLjava/lang/String;)I
pop
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/v Lcom/teamspeak/ts3client/data/ab;
invokevirtual com/teamspeak/ts3client/data/ab/e()[J
ifnull L1
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/v Lcom/teamspeak/ts3client/data/ab;
invokevirtual com/teamspeak/ts3client/data/ab/e()[J
ldc "Subscribe after UnsubscribeAll"
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_requestChannelSubscribe(J[JLjava/lang/String;)I
pop
return
.limit locals 1
.limit stack 5
.end method

.method static synthetic g(Lcom/teamspeak/ts3client/t;)Lcom/teamspeak/ts3client/chat/c;
aload 0
getfield com/teamspeak/ts3client/t/av Lcom/teamspeak/ts3client/chat/c;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic h(Lcom/teamspeak/ts3client/t;)V
aload 0
getfield com/teamspeak/ts3client/t/aG Lcom/teamspeak/ts3client/customs/FloatingButton;
astore 0
aload 0
getfield com/teamspeak/ts3client/customs/FloatingButton/c Z
ifne L0
aload 0
iconst_1
invokevirtual com/teamspeak/ts3client/customs/FloatingButton/a(Z)V
aload 0
invokevirtual com/teamspeak/ts3client/customs/FloatingButton/invalidate()V
L0:
return
.limit locals 1
.limit stack 2
.end method

.method static synthetic i(Lcom/teamspeak/ts3client/t;)Z
aload 0
getfield com/teamspeak/ts3client/t/ay Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic j(Lcom/teamspeak/ts3client/t;)Z
aload 0
iconst_0
putfield com/teamspeak/ts3client/t/b Z
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method static synthetic k(Lcom/teamspeak/ts3client/t;)I
aload 0
getfield com/teamspeak/ts3client/t/d I
ireturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic l(Lcom/teamspeak/ts3client/t;)Lcom/teamspeak/ts3client/customs/FloatingButton;
aload 0
getfield com/teamspeak/ts3client/t/aI Lcom/teamspeak/ts3client/customs/FloatingButton;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final A()V
aload 0
getfield com/teamspeak/ts3client/t/au Z
ifeq L0
aload 0
getfield com/teamspeak/ts3client/t/az Z
ifeq L0
aload 0
iconst_0
invokevirtual com/teamspeak/ts3client/t/g(Z)V
L0:
return
.limit locals 1
.limit stack 2
.end method

.method final B()V
aload 0
getfield com/teamspeak/ts3client/t/aw Z
ifeq L0
return
L0:
aload 0
aload 0
invokevirtual com/teamspeak/ts3client/t/i()Landroid/support/v4/app/bb;
ldc_w 2131493216
invokevirtual android/support/v4/app/bb/findViewById(I)Landroid/view/View;
checkcast com/teamspeak/ts3client/customs/FloatingButton
putfield com/teamspeak/ts3client/t/aI Lcom/teamspeak/ts3client/customs/FloatingButton;
aload 0
getfield com/teamspeak/ts3client/t/aI Lcom/teamspeak/ts3client/customs/FloatingButton;
ldc_w 2130837645
aload 0
getfield com/teamspeak/ts3client/t/d I
i2f
aload 0
getfield com/teamspeak/ts3client/t/d I
i2f
invokestatic com/teamspeak/ts3client/data/d/t/a(IFF)Landroid/graphics/Bitmap;
invokevirtual com/teamspeak/ts3client/customs/FloatingButton/setBitmap(Landroid/graphics/Bitmap;)V
aload 0
getfield com/teamspeak/ts3client/t/aI Lcom/teamspeak/ts3client/customs/FloatingButton;
new com/teamspeak/ts3client/y
dup
aload 0
invokespecial com/teamspeak/ts3client/y/<init>(Lcom/teamspeak/ts3client/t;)V
invokevirtual com/teamspeak/ts3client/customs/FloatingButton/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 0
aload 0
invokevirtual com/teamspeak/ts3client/t/i()Landroid/support/v4/app/bb;
ldc_w 2131493213
invokevirtual android/support/v4/app/bb/findViewById(I)Landroid/view/View;
checkcast com/teamspeak/ts3client/customs/FloatingButton
putfield com/teamspeak/ts3client/t/aG Lcom/teamspeak/ts3client/customs/FloatingButton;
aload 0
getfield com/teamspeak/ts3client/t/aG Lcom/teamspeak/ts3client/customs/FloatingButton;
ldc_w 2130837640
aload 0
getfield com/teamspeak/ts3client/t/d I
i2f
aload 0
getfield com/teamspeak/ts3client/t/d I
i2f
invokestatic com/teamspeak/ts3client/data/d/t/a(IFF)Landroid/graphics/Bitmap;
invokevirtual com/teamspeak/ts3client/customs/FloatingButton/setBitmap(Landroid/graphics/Bitmap;)V
aload 0
getfield com/teamspeak/ts3client/t/aG Lcom/teamspeak/ts3client/customs/FloatingButton;
new com/teamspeak/ts3client/z
dup
aload 0
invokespecial com/teamspeak/ts3client/z/<init>(Lcom/teamspeak/ts3client/t;)V
invokevirtual com/teamspeak/ts3client/customs/FloatingButton/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 0
aload 0
invokevirtual com/teamspeak/ts3client/t/i()Landroid/support/v4/app/bb;
ldc_w 2131493214
invokevirtual android/support/v4/app/bb/findViewById(I)Landroid/view/View;
checkcast com/teamspeak/ts3client/customs/FloatingButton
putfield com/teamspeak/ts3client/t/g Lcom/teamspeak/ts3client/customs/FloatingButton;
aload 0
getfield com/teamspeak/ts3client/t/g Lcom/teamspeak/ts3client/customs/FloatingButton;
ldc_w 2130837633
aload 0
getfield com/teamspeak/ts3client/t/d I
i2f
aload 0
getfield com/teamspeak/ts3client/t/d I
i2f
invokestatic com/teamspeak/ts3client/data/d/t/a(IFF)Landroid/graphics/Bitmap;
invokevirtual com/teamspeak/ts3client/customs/FloatingButton/setBitmap(Landroid/graphics/Bitmap;)V
aload 0
getfield com/teamspeak/ts3client/t/g Lcom/teamspeak/ts3client/customs/FloatingButton;
new com/teamspeak/ts3client/aa
dup
aload 0
invokespecial com/teamspeak/ts3client/aa/<init>(Lcom/teamspeak/ts3client/t;)V
invokevirtual com/teamspeak/ts3client/customs/FloatingButton/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 0
getfield com/teamspeak/ts3client/t/ay Z
ifeq L1
aload 0
invokevirtual com/teamspeak/ts3client/t/i()Landroid/support/v4/app/bb;
new com/teamspeak/ts3client/ac
dup
aload 0
invokespecial com/teamspeak/ts3client/ac/<init>(Lcom/teamspeak/ts3client/t;)V
invokevirtual android/support/v4/app/bb/runOnUiThread(Ljava/lang/Runnable;)V
L1:
aload 0
aload 0
invokevirtual com/teamspeak/ts3client/t/i()Landroid/support/v4/app/bb;
ldc_w 2131493215
invokevirtual android/support/v4/app/bb/findViewById(I)Landroid/view/View;
checkcast com/teamspeak/ts3client/customs/FloatingButton
putfield com/teamspeak/ts3client/t/h Lcom/teamspeak/ts3client/customs/FloatingButton;
aload 0
getfield com/teamspeak/ts3client/t/h Lcom/teamspeak/ts3client/customs/FloatingButton;
ldc_w 2130837654
aload 0
getfield com/teamspeak/ts3client/t/d I
i2f
aload 0
getfield com/teamspeak/ts3client/t/d I
i2f
invokestatic com/teamspeak/ts3client/data/d/t/a(IFF)Landroid/graphics/Bitmap;
invokevirtual com/teamspeak/ts3client/customs/FloatingButton/setBitmap(Landroid/graphics/Bitmap;)V
aload 0
getfield com/teamspeak/ts3client/t/h Lcom/teamspeak/ts3client/customs/FloatingButton;
new com/teamspeak/ts3client/ad
dup
aload 0
invokespecial com/teamspeak/ts3client/ad/<init>(Lcom/teamspeak/ts3client/t;)V
invokevirtual com/teamspeak/ts3client/customs/FloatingButton/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 0
invokevirtual com/teamspeak/ts3client/t/i()Landroid/support/v4/app/bb;
ldc_w 2131493217
invokevirtual android/support/v4/app/bb/findViewById(I)Landroid/view/View;
checkcast com/teamspeak/ts3client/customs/FloatingButton
astore 1
aload 1
putstatic com/teamspeak/ts3client/t/i Lcom/teamspeak/ts3client/customs/FloatingButton;
aload 1
ldc_w 2130837644
aload 0
getfield com/teamspeak/ts3client/t/d I
i2f
aload 0
getfield com/teamspeak/ts3client/t/d I
i2f
invokestatic com/teamspeak/ts3client/data/d/t/a(IFF)Landroid/graphics/Bitmap;
invokevirtual com/teamspeak/ts3client/customs/FloatingButton/setBitmap(Landroid/graphics/Bitmap;)V
getstatic com/teamspeak/ts3client/t/i Lcom/teamspeak/ts3client/customs/FloatingButton;
new com/teamspeak/ts3client/af
dup
aload 0
invokespecial com/teamspeak/ts3client/af/<init>(Lcom/teamspeak/ts3client/t;)V
invokevirtual com/teamspeak/ts3client/customs/FloatingButton/setOnTouchListener(Landroid/view/View$OnTouchListener;)V
aload 0
getfield com/teamspeak/ts3client/t/h Lcom/teamspeak/ts3client/customs/FloatingButton;
ldc_w 2130837654
aload 0
getfield com/teamspeak/ts3client/t/d I
i2f
aload 0
getfield com/teamspeak/ts3client/t/d I
i2f
invokestatic com/teamspeak/ts3client/data/d/t/a(IFF)Landroid/graphics/Bitmap;
invokevirtual com/teamspeak/ts3client/customs/FloatingButton/setBitmap(Landroid/graphics/Bitmap;)V
aload 0
getfield com/teamspeak/ts3client/t/g Lcom/teamspeak/ts3client/customs/FloatingButton;
ldc_w 2130837633
aload 0
getfield com/teamspeak/ts3client/t/d I
i2f
aload 0
getfield com/teamspeak/ts3client/t/d I
i2f
invokestatic com/teamspeak/ts3client/data/d/t/a(IFF)Landroid/graphics/Bitmap;
invokevirtual com/teamspeak/ts3client/customs/FloatingButton/setBitmap(Landroid/graphics/Bitmap;)V
aload 0
getfield com/teamspeak/ts3client/t/b Z
ifeq L2
aload 0
getfield com/teamspeak/ts3client/t/g Lcom/teamspeak/ts3client/customs/FloatingButton;
ldc_w 2130837636
aload 0
getfield com/teamspeak/ts3client/t/d I
i2f
aload 0
getfield com/teamspeak/ts3client/t/d I
i2f
invokestatic com/teamspeak/ts3client/data/d/t/a(IFF)Landroid/graphics/Bitmap;
invokevirtual com/teamspeak/ts3client/customs/FloatingButton/setBitmap(Landroid/graphics/Bitmap;)V
L2:
aload 0
getfield com/teamspeak/ts3client/t/c Z
ifeq L3
aload 0
getfield com/teamspeak/ts3client/t/h Lcom/teamspeak/ts3client/customs/FloatingButton;
ldc_w 2130837656
aload 0
getfield com/teamspeak/ts3client/t/d I
i2f
aload 0
getfield com/teamspeak/ts3client/t/d I
i2f
invokestatic com/teamspeak/ts3client/data/d/t/a(IFF)Landroid/graphics/Bitmap;
invokevirtual com/teamspeak/ts3client/customs/FloatingButton/setBitmap(Landroid/graphics/Bitmap;)V
L3:
aload 0
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
ldc "audio_ptt"
iconst_0
invokeinterface android/content/SharedPreferences/getBoolean(Ljava/lang/String;Z)Z 2
putfield com/teamspeak/ts3client/t/az Z
aload 0
getfield com/teamspeak/ts3client/t/az Z
ifne L4
aload 0
invokevirtual com/teamspeak/ts3client/t/C()V
L4:
aload 0
aload 0
invokevirtual com/teamspeak/ts3client/t/i()Landroid/support/v4/app/bb;
ldc_w 2131493212
invokevirtual android/support/v4/app/bb/findViewById(I)Landroid/view/View;
checkcast com/teamspeak/ts3client/data/customExpandableListView
putfield com/teamspeak/ts3client/t/j Lcom/teamspeak/ts3client/data/customExpandableListView;
aload 0
getfield com/teamspeak/ts3client/t/j Lcom/teamspeak/ts3client/data/customExpandableListView;
new com/teamspeak/ts3client/ag
dup
aload 0
invokespecial com/teamspeak/ts3client/ag/<init>(Lcom/teamspeak/ts3client/t;)V
invokevirtual com/teamspeak/ts3client/data/customExpandableListView/setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V
aload 0
getfield com/teamspeak/ts3client/t/l Lcom/teamspeak/ts3client/data/g;
aload 0
getfield com/teamspeak/ts3client/t/j Lcom/teamspeak/ts3client/data/customExpandableListView;
putfield com/teamspeak/ts3client/data/g/b Lcom/teamspeak/ts3client/data/customExpandableListView;
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
ldc "trenn_linie"
iconst_0
invokeinterface android/content/SharedPreferences/getBoolean(Ljava/lang/String;Z)Z 2
ifne L5
aload 0
getfield com/teamspeak/ts3client/t/j Lcom/teamspeak/ts3client/data/customExpandableListView;
iconst_0
invokevirtual com/teamspeak/ts3client/data/customExpandableListView/setDividerHeight(I)V
L5:
aload 0
getfield com/teamspeak/ts3client/t/j Lcom/teamspeak/ts3client/data/customExpandableListView;
iconst_0
invokevirtual com/teamspeak/ts3client/data/customExpandableListView/setAlwaysDrawnWithCacheEnabled(Z)V
aload 0
getfield com/teamspeak/ts3client/t/j Lcom/teamspeak/ts3client/data/customExpandableListView;
aload 0
getfield com/teamspeak/ts3client/t/l Lcom/teamspeak/ts3client/data/g;
invokevirtual com/teamspeak/ts3client/data/customExpandableListView/setAdapter(Landroid/widget/ExpandableListAdapter;)V
aload 0
getfield com/teamspeak/ts3client/t/j Lcom/teamspeak/ts3client/data/customExpandableListView;
aconst_null
invokevirtual com/teamspeak/ts3client/data/customExpandableListView/setGroupIndicator(Landroid/graphics/drawable/Drawable;)V
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/l Lcom/teamspeak/ts3client/ConnectionBackground;
invokevirtual com/teamspeak/ts3client/ConnectionBackground/b()V
return
.limit locals 2
.limit stack 4
.end method

.method public final C()V
aload 0
getfield com/teamspeak/ts3client/t/ax Z
ifeq L0
return
L0:
aload 0
invokevirtual com/teamspeak/ts3client/t/i()Landroid/support/v4/app/bb;
new com/teamspeak/ts3client/aj
dup
aload 0
invokespecial com/teamspeak/ts3client/aj/<init>(Lcom/teamspeak/ts3client/t;)V
invokevirtual android/support/v4/app/bb/runOnUiThread(Ljava/lang/Runnable;)V
return
.limit locals 1
.limit stack 4
.end method

.method public final D()V
aload 0
invokevirtual com/teamspeak/ts3client/t/i()Landroid/support/v4/app/bb;
new com/teamspeak/ts3client/al
dup
aload 0
invokespecial com/teamspeak/ts3client/al/<init>(Lcom/teamspeak/ts3client/t;)V
invokevirtual android/support/v4/app/bb/runOnUiThread(Ljava/lang/Runnable;)V
return
.limit locals 1
.limit stack 4
.end method

.method public final E()V
.catch java/lang/Exception from L0 to L1 using L2
L0:
aload 0
invokevirtual com/teamspeak/ts3client/t/i()Landroid/support/v4/app/bb;
new com/teamspeak/ts3client/aq
dup
aload 0
invokespecial com/teamspeak/ts3client/aq/<init>(Lcom/teamspeak/ts3client/t;)V
invokevirtual android/support/v4/app/bb/runOnUiThread(Ljava/lang/Runnable;)V
L1:
return
L2:
astore 1
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/d Ljava/util/logging/Logger;
getstatic java/util/logging/Level/SEVERE Ljava/util/logging/Level;
ldc "Error: Dialog Exception"
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;)V
return
.limit locals 2
.limit stack 4
.end method

.method public final F()V
aload 0
getfield com/teamspeak/ts3client/t/b Z
ifne L0
aload 0
iconst_1
putfield com/teamspeak/ts3client/t/b Z
aload 0
getfield com/teamspeak/ts3client/t/g Lcom/teamspeak/ts3client/customs/FloatingButton;
ldc_w 2130837636
aload 0
getfield com/teamspeak/ts3client/t/d I
i2f
aload 0
getfield com/teamspeak/ts3client/t/d I
i2f
invokestatic com/teamspeak/ts3client/data/d/t/a(IFF)Landroid/graphics/Bitmap;
invokevirtual com/teamspeak/ts3client/customs/FloatingButton/setBitmap(Landroid/graphics/Bitmap;)V
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
getstatic com/teamspeak/ts3client/jni/d/f Lcom/teamspeak/ts3client/jni/d;
iconst_1
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/a(JLcom/teamspeak/ts3client/jni/d;I)I
pop
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
ldc "InputMute"
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_flushClientSelfUpdates(JLjava/lang/String;)I
pop
return
L0:
aload 0
iconst_0
putfield com/teamspeak/ts3client/t/b Z
aload 0
getfield com/teamspeak/ts3client/t/g Lcom/teamspeak/ts3client/customs/FloatingButton;
ldc_w 2130837633
aload 0
getfield com/teamspeak/ts3client/t/d I
i2f
aload 0
getfield com/teamspeak/ts3client/t/d I
i2f
invokestatic com/teamspeak/ts3client/data/d/t/a(IFF)Landroid/graphics/Bitmap;
invokevirtual com/teamspeak/ts3client/customs/FloatingButton/setBitmap(Landroid/graphics/Bitmap;)V
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
getstatic com/teamspeak/ts3client/jni/d/f Lcom/teamspeak/ts3client/jni/d;
iconst_0
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/a(JLcom/teamspeak/ts3client/jni/d;I)I
pop
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
ldc "InputUnMute"
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_flushClientSelfUpdates(JLjava/lang/String;)I
pop
return
.limit locals 1
.limit stack 5
.end method

.method public final G()V
aload 0
getfield com/teamspeak/ts3client/t/c Z
ifne L0
aload 0
iconst_1
putfield com/teamspeak/ts3client/t/c Z
aload 0
getfield com/teamspeak/ts3client/t/h Lcom/teamspeak/ts3client/customs/FloatingButton;
ldc_w 2130837656
aload 0
getfield com/teamspeak/ts3client/t/d I
i2f
aload 0
getfield com/teamspeak/ts3client/t/d I
i2f
invokestatic com/teamspeak/ts3client/data/d/t/a(IFF)Landroid/graphics/Bitmap;
invokevirtual com/teamspeak/ts3client/customs/FloatingButton/setBitmap(Landroid/graphics/Bitmap;)V
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
getstatic com/teamspeak/ts3client/jni/d/g Lcom/teamspeak/ts3client/jni/d;
iconst_1
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/a(JLcom/teamspeak/ts3client/jni/d;I)I
pop
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
ldc ""
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_flushClientSelfUpdates(JLjava/lang/String;)I
pop
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/g()Lcom/teamspeak/ts3client/a/p;
getstatic com/teamspeak/ts3client/jni/h/aT Lcom/teamspeak/ts3client/jni/h;
aconst_null
invokevirtual com/teamspeak/ts3client/a/p/a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/g()Lcom/teamspeak/ts3client/a/p;
iconst_1
putfield com/teamspeak/ts3client/a/p/a Z
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/v Lcom/teamspeak/ts3client/data/ab;
getfield com/teamspeak/ts3client/data/ab/s Ljava/util/BitSet;
iconst_1
invokevirtual java/util/BitSet/set(I)V
return
L0:
aload 0
iconst_0
putfield com/teamspeak/ts3client/t/c Z
aload 0
getfield com/teamspeak/ts3client/t/h Lcom/teamspeak/ts3client/customs/FloatingButton;
ldc_w 2130837654
aload 0
getfield com/teamspeak/ts3client/t/d I
i2f
aload 0
getfield com/teamspeak/ts3client/t/d I
i2f
invokestatic com/teamspeak/ts3client/data/d/t/a(IFF)Landroid/graphics/Bitmap;
invokevirtual com/teamspeak/ts3client/customs/FloatingButton/setBitmap(Landroid/graphics/Bitmap;)V
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
getstatic com/teamspeak/ts3client/jni/d/g Lcom/teamspeak/ts3client/jni/d;
iconst_0
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/a(JLcom/teamspeak/ts3client/jni/d;I)I
pop
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
ldc ""
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_flushClientSelfUpdates(JLjava/lang/String;)I
pop
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/g()Lcom/teamspeak/ts3client/a/p;
iconst_0
putfield com/teamspeak/ts3client/a/p/a Z
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/g()Lcom/teamspeak/ts3client/a/p;
getstatic com/teamspeak/ts3client/jni/h/aU Lcom/teamspeak/ts3client/jni/h;
aconst_null
invokevirtual com/teamspeak/ts3client/a/p/a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/v Lcom/teamspeak/ts3client/data/ab;
getfield com/teamspeak/ts3client/data/ab/s Ljava/util/BitSet;
iconst_1
invokevirtual java/util/BitSet/clear(I)V
return
.limit locals 1
.limit stack 5
.end method

.method public final H()V
aload 0
iconst_0
putfield com/teamspeak/ts3client/t/aA Z
aload 0
iconst_0
putfield com/teamspeak/ts3client/t/ax Z
return
.limit locals 1
.limit stack 2
.end method

.method public final I()V
aload 0
invokevirtual com/teamspeak/ts3client/t/B()V
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
aload 0
putfield com/teamspeak/ts3client/Ts3Application/c Landroid/support/v4/app/Fragment;
return
.limit locals 1
.limit stack 2
.end method

.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
aload 0
getfield com/teamspeak/ts3client/t/aE Lcom/teamspeak/ts3client/d/c/d;
ifnonnull L0
aload 0
new com/teamspeak/ts3client/d/c/d
dup
invokespecial com/teamspeak/ts3client/d/c/d/<init>()V
putfield com/teamspeak/ts3client/t/aE Lcom/teamspeak/ts3client/d/c/d;
L0:
aload 0
getfield com/teamspeak/ts3client/t/aw Z
ifeq L1
aload 1
ldc_w 2130903088
aload 2
iconst_0
invokevirtual android/view/LayoutInflater/inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;
areturn
L1:
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/o Landroid/support/v7/app/a;
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/v Lcom/teamspeak/ts3client/data/ab;
getfield com/teamspeak/ts3client/data/ab/a Ljava/lang/String;
invokevirtual android/support/v7/app/a/a(Ljava/lang/CharSequence;)V
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/o Landroid/support/v7/app/a;
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/m()Ljava/lang/String;
invokevirtual android/support/v7/app/a/b(Ljava/lang/CharSequence;)V
aload 0
invokevirtual com/teamspeak/ts3client/t/n()V
aload 0
invokevirtual com/teamspeak/ts3client/t/i()Landroid/support/v4/app/bb;
invokevirtual android/support/v4/app/bb/getWindow()Landroid/view/Window;
ldc_w 32768
invokevirtual android/view/Window/addFlags(I)V
aload 0
invokevirtual com/teamspeak/ts3client/t/i()Landroid/support/v4/app/bb;
invokevirtual android/support/v4/app/bb/getWindow()Landroid/view/Window;
ldc_w 524288
invokevirtual android/view/Window/addFlags(I)V
aload 0
invokevirtual com/teamspeak/ts3client/t/i()Landroid/support/v4/app/bb;
invokevirtual android/support/v4/app/bb/getWindow()Landroid/view/Window;
ldc_w 4194304
invokevirtual android/view/Window/addFlags(I)V
aload 0
invokevirtual com/teamspeak/ts3client/t/i()Landroid/support/v4/app/bb;
invokevirtual android/support/v4/app/bb/getWindow()Landroid/view/Window;
sipush 128
invokevirtual android/view/Window/addFlags(I)V
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
aload 0
putfield com/teamspeak/ts3client/Ts3Application/c Landroid/support/v4/app/Fragment;
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
new com/teamspeak/ts3client/data/g
dup
aload 0
invokevirtual com/teamspeak/ts3client/t/i()Landroid/support/v4/app/bb;
aload 0
getfield android/support/v4/app/Fragment/M Landroid/support/v4/app/bl;
invokespecial com/teamspeak/ts3client/data/g/<init>(Landroid/content/Context;Landroid/support/v4/app/bi;)V
putfield com/teamspeak/ts3client/data/e/f Lcom/teamspeak/ts3client/data/g;
aload 0
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/f Lcom/teamspeak/ts3client/data/g;
putfield com/teamspeak/ts3client/t/l Lcom/teamspeak/ts3client/data/g;
aload 1
ldc_w 2130903088
aload 2
iconst_0
invokevirtual android/view/LayoutInflater/inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;
astore 1
aload 0
getfield com/teamspeak/ts3client/t/j Lcom/teamspeak/ts3client/data/customExpandableListView;
ifnonnull L2
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/n I
ifeq L3
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/k Lcom/teamspeak/ts3client/t;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual com/teamspeak/ts3client/t/i()Landroid/support/v4/app/bb;
ldc_w 2131034152
invokevirtual android/support/v4/app/bb/getString(I)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/n I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aload 0
invokevirtual com/teamspeak/ts3client/t/i()Landroid/support/v4/app/bb;
ldc_w 2131034151
invokevirtual android/support/v4/app/bb/getString(I)Ljava/lang/String;
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
aload 0
invokevirtual com/teamspeak/ts3client/t/i()Landroid/support/v4/app/bb;
ldc_w 2131034142
invokevirtual android/support/v4/app/bb/getString(I)Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/t/a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;)V
aload 1
areturn
L3:
aload 0
new com/teamspeak/ts3client/d/h
dup
aload 0
invokevirtual com/teamspeak/ts3client/t/i()Landroid/support/v4/app/bb;
ldc "connectiondialog.step0"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
ldc "connectiondialog.info"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
ldc "button.cancel"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
iconst_0
invokespecial com/teamspeak/ts3client/d/h/<init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;B)V
putfield com/teamspeak/ts3client/t/k Lcom/teamspeak/ts3client/d/h;
aload 0
invokevirtual com/teamspeak/ts3client/t/i()Landroid/support/v4/app/bb;
new com/teamspeak/ts3client/am
dup
aload 0
invokespecial com/teamspeak/ts3client/am/<init>(Lcom/teamspeak/ts3client/t;)V
invokevirtual android/support/v4/app/bb/runOnUiThread(Ljava/lang/Runnable;)V
L2:
aload 0
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
ldc "phone"
invokevirtual com/teamspeak/ts3client/Ts3Application/getSystemService(Ljava/lang/String;)Ljava/lang/Object;
checkcast android/telephony/TelephonyManager
putfield com/teamspeak/ts3client/t/m Landroid/telephony/TelephonyManager;
aload 0
getfield com/teamspeak/ts3client/t/m Landroid/telephony/TelephonyManager;
aload 0
getfield com/teamspeak/ts3client/t/aH Landroid/telephony/PhoneStateListener;
bipush 32
invokevirtual android/telephony/TelephonyManager/listen(Landroid/telephony/PhoneStateListener;I)V
aload 0
new com/teamspeak/ts3client/bf
dup
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
invokespecial com/teamspeak/ts3client/bf/<init>(Lcom/teamspeak/ts3client/Ts3Application;)V
putfield com/teamspeak/ts3client/t/aB Lcom/teamspeak/ts3client/bf;
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
aload 0
getfield com/teamspeak/ts3client/t/aB Lcom/teamspeak/ts3client/bf;
invokeinterface android/content/SharedPreferences/registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V 1
aload 1
areturn
.limit locals 3
.limit stack 8
.end method

.method public final a()V
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/c Landroid/support/v4/app/Fragment;
ifnull L0
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/c Landroid/support/v4/app/Fragment;
invokevirtual android/support/v4/app/Fragment/i()Landroid/support/v4/app/bb;
new com/teamspeak/ts3client/ay
dup
aload 0
invokespecial com/teamspeak/ts3client/ay/<init>(Lcom/teamspeak/ts3client/t;)V
invokevirtual android/support/v4/app/bb/runOnUiThread(Ljava/lang/Runnable;)V
L0:
return
.limit locals 1
.limit stack 4
.end method

.method public final a(Landroid/os/Bundle;)V
aload 0
aload 1
invokespecial android/support/v4/app/Fragment/a(Landroid/os/Bundle;)V
aload 0
new com/teamspeak/ts3client/chat/c
dup
invokespecial com/teamspeak/ts3client/chat/c/<init>()V
putfield com/teamspeak/ts3client/t/av Lcom/teamspeak/ts3client/chat/c;
return
.limit locals 2
.limit stack 3
.end method

.method public final a(Landroid/view/Menu;Landroid/view/MenuInflater;)V
aload 1
invokeinterface android/view/Menu/clear()V 0
aload 0
getfield com/teamspeak/ts3client/t/aE Lcom/teamspeak/ts3client/d/c/d;
aload 1
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/d/c/d/a(Landroid/view/Menu;Lcom/teamspeak/ts3client/Ts3Application;)V
aload 0
aload 1
aload 2
invokespecial android/support/v4/app/Fragment/a(Landroid/view/Menu;Landroid/view/MenuInflater;)V
return
.limit locals 3
.limit stack 3
.end method

.method public final a(Lcom/teamspeak/ts3client/data/a;)V
aload 0
aload 1
iconst_0
invokevirtual com/teamspeak/ts3client/t/a(Lcom/teamspeak/ts3client/data/a;I)V
return
.limit locals 2
.limit stack 3
.end method

.method public final a(Lcom/teamspeak/ts3client/data/a;I)V
aload 0
invokevirtual com/teamspeak/ts3client/t/i()Landroid/support/v4/app/bb;
new com/teamspeak/ts3client/at
dup
aload 0
aload 1
iload 2
invokespecial com/teamspeak/ts3client/at/<init>(Lcom/teamspeak/ts3client/t;Lcom/teamspeak/ts3client/data/a;I)V
invokevirtual android/support/v4/app/bb/runOnUiThread(Ljava/lang/Runnable;)V
return
.limit locals 3
.limit stack 6
.end method

.method public final a(Lcom/teamspeak/ts3client/data/c;)V
aload 0
invokevirtual com/teamspeak/ts3client/t/l()Z
ifeq L0
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/c Landroid/support/v4/app/Fragment;
instanceof com/teamspeak/ts3client/h
ifeq L1
L0:
return
L1:
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/d Ljava/util/logging/Logger;
getstatic java/util/logging/Level/INFO Ljava/util/logging/Level;
new java/lang/StringBuilder
dup
ldc "Clientinfo: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;)V
aload 1
invokestatic com/teamspeak/ts3client/h/a(Lcom/teamspeak/ts3client/data/c;)Lcom/teamspeak/ts3client/h;
astore 1
aload 0
getfield android/support/v4/app/Fragment/M Landroid/support/v4/app/bl;
invokevirtual android/support/v4/app/bi/a()Landroid/support/v4/app/cd;
astore 2
aload 2
aload 1
ldc "ClientInfo"
invokevirtual android/support/v4/app/cd/b(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/cd;
pop
aload 2
invokevirtual android/support/v4/app/cd/f()Landroid/support/v4/app/cd;
pop
aload 2
sipush 4097
invokevirtual android/support/v4/app/cd/a(I)Landroid/support/v4/app/cd;
pop
aload 2
invokevirtual android/support/v4/app/cd/i()I
pop
return
.limit locals 3
.limit stack 5
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Boolean;)V
aload 0
invokevirtual com/teamspeak/ts3client/t/l()Z
ifeq L0
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/c Landroid/support/v4/app/Fragment;
invokevirtual android/support/v4/app/Fragment/i()Landroid/support/v4/app/bb;
new com/teamspeak/ts3client/w
dup
aload 0
aload 2
aload 1
invokespecial com/teamspeak/ts3client/w/<init>(Lcom/teamspeak/ts3client/t;Ljava/lang/Boolean;Ljava/lang/String;)V
invokevirtual android/support/v4/app/bb/runOnUiThread(Ljava/lang/Runnable;)V
L0:
return
.limit locals 3
.limit stack 6
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;)V
.catch java/lang/Exception from L0 to L1 using L2
L0:
aload 0
invokevirtual com/teamspeak/ts3client/t/i()Landroid/support/v4/app/bb;
new com/teamspeak/ts3client/ar
dup
aload 0
aload 1
aload 2
aload 6
aload 5
aload 4
aload 3
invokespecial com/teamspeak/ts3client/ar/<init>(Lcom/teamspeak/ts3client/t;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;)V
invokevirtual android/support/v4/app/bb/runOnUiThread(Ljava/lang/Runnable;)V
L1:
return
L2:
astore 1
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/d Ljava/util/logging/Logger;
getstatic java/util/logging/Level/SEVERE Ljava/util/logging/Level;
ldc "Error: Dialog Exception"
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;)V
return
.limit locals 7
.limit stack 10
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Lcom/teamspeak/ts3client/jni/h;)V
aload 0
aload 1
aload 2
aload 3
aload 4
aload 5
aload 6
invokevirtual com/teamspeak/ts3client/t/a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;)V
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/g()Lcom/teamspeak/ts3client/a/p;
aload 7
new com/teamspeak/ts3client/a/o
dup
ldc ""
iconst_0
ldc ""
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/m()Ljava/lang/String;
invokespecial com/teamspeak/ts3client/a/o/<init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
invokevirtual com/teamspeak/ts3client/a/p/a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V
return
.limit locals 8
.limit stack 8
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
new android/content/Intent
dup
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
ldc com/teamspeak/ts3client/StartGUIFragment
invokespecial android/content/Intent/<init>(Landroid/content/Context;Ljava/lang/Class;)V
astore 4
aload 4
ldc "android.intent.action.MAIN"
invokevirtual android/content/Intent/setAction(Ljava/lang/String;)Landroid/content/Intent;
pop
aload 4
ldc "android.intent.category.LAUNCHER"
invokevirtual android/content/Intent/addCategory(Ljava/lang/String;)Landroid/content/Intent;
pop
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
iconst_0
aload 4
iconst_0
invokestatic android/app/PendingIntent/getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;
astore 4
new android/support/v4/app/dm
dup
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/getApplicationContext()Landroid/content/Context;
invokespecial android/support/v4/app/dm/<init>(Landroid/content/Context;)V
astore 5
aload 5
ldc_w 2130837622
invokevirtual android/support/v4/app/dm/a(I)Landroid/support/v4/app/dm;
aload 1
invokevirtual android/support/v4/app/dm/c(Ljava/lang/CharSequence;)Landroid/support/v4/app/dm;
invokestatic java/lang/System/currentTimeMillis()J
invokevirtual android/support/v4/app/dm/a(J)Landroid/support/v4/app/dm;
invokevirtual android/support/v4/app/dm/a()Landroid/support/v4/app/dm;
aload 2
invokevirtual android/support/v4/app/dm/a(Ljava/lang/CharSequence;)Landroid/support/v4/app/dm;
aload 3
invokevirtual android/support/v4/app/dm/b(Ljava/lang/CharSequence;)Landroid/support/v4/app/dm;
aload 4
putfield android/support/v4/app/dm/d Landroid/app/PendingIntent;
aload 5
invokevirtual android/support/v4/app/dm/c()Landroid/app/Notification;
astore 1
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/l Landroid/app/NotificationManager;
aconst_null
bipush 100
aload 1
invokevirtual android/app/NotificationManager/notify(Ljava/lang/String;ILandroid/app/Notification;)V
return
.limit locals 6
.limit stack 4
.end method

.method public final a(Z)V
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/c Landroid/support/v4/app/Fragment;
invokevirtual android/support/v4/app/Fragment/i()Landroid/support/v4/app/bb;
new com/teamspeak/ts3client/bb
dup
aload 0
iload 1
invokespecial com/teamspeak/ts3client/bb/<init>(Lcom/teamspeak/ts3client/t;Z)V
invokevirtual android/support/v4/app/bb/runOnUiThread(Ljava/lang/Runnable;)V
return
.limit locals 2
.limit stack 5
.end method

.method public final a(Landroid/view/MenuItem;)Z
aload 0
getfield com/teamspeak/ts3client/t/aE Lcom/teamspeak/ts3client/d/c/d;
aload 1
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/d/c/d/a(Landroid/view/MenuItem;Lcom/teamspeak/ts3client/Ts3Application;)Z
ireturn
.limit locals 2
.limit stack 3
.end method

.method public final b()Z
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
getstatic com/teamspeak/ts3client/jni/i/x Lcom/teamspeak/ts3client/jni/i;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/a(JLcom/teamspeak/ts3client/jni/i;)I
istore 1
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
getstatic com/teamspeak/ts3client/jni/i/w Lcom/teamspeak/ts3client/jni/i;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/b(JLcom/teamspeak/ts3client/jni/i;)Ljava/lang/String;
astore 2
iload 1
tableswitch 1
L0
L1
L2
default : L3
L3:
iconst_0
ireturn
L0:
aload 2
invokestatic com/teamspeak/ts3client/chat/d/b(Ljava/lang/String;)V
goto L3
L1:
aload 2
ldc ""
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L3
aload 0
aload 2
invokestatic com/teamspeak/ts3client/data/d/a/a(Ljava/lang/String;)Landroid/text/Spanned;
ldc "messages.hostmessage"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
ldc "button.close"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokespecial com/teamspeak/ts3client/t/a(Landroid/text/Spanned;Ljava/lang/String;Ljava/lang/String;)V
goto L3
L2:
aload 0
aload 2
invokestatic com/teamspeak/ts3client/data/d/a/a(Ljava/lang/String;)Landroid/text/Spanned;
ldc "messages.hostmessage"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
ldc "button.exit"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokespecial com/teamspeak/ts3client/t/a(Landroid/text/Spanned;Ljava/lang/String;Ljava/lang/String;)V
iconst_1
ireturn
.limit locals 3
.limit stack 4
.end method

.method public final c(Landroid/os/Bundle;)V
aload 0
aload 1
invokespecial android/support/v4/app/Fragment/c(Landroid/os/Bundle;)V
aload 0
invokevirtual com/teamspeak/ts3client/t/B()V
return
.limit locals 2
.limit stack 2
.end method

.method public final d()V
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/c Landroid/support/v4/app/Fragment;
ifnull L0
aload 0
invokevirtual com/teamspeak/ts3client/t/l()Z
ifeq L0
aload 0
invokevirtual com/teamspeak/ts3client/t/i()Landroid/support/v4/app/bb;
new com/teamspeak/ts3client/bd
dup
aload 0
invokespecial com/teamspeak/ts3client/bd/<init>(Lcom/teamspeak/ts3client/t;)V
invokevirtual android/support/v4/app/bb/runOnUiThread(Ljava/lang/Runnable;)V
L0:
return
.limit locals 1
.limit stack 4
.end method

.method public final d(Landroid/os/Bundle;)V
return
.limit locals 2
.limit stack 0
.end method

.method public final d(Z)V
.catch java/lang/IllegalArgumentException from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L5
.catch java/lang/Exception from L6 to L7 using L8
.catch java/lang/Exception from L9 to L10 using L11
aload 0
iconst_1
putfield com/teamspeak/ts3client/t/aF Z
L0:
aload 0
invokevirtual com/teamspeak/ts3client/t/i()Landroid/support/v4/app/bb;
ifnull L1
aload 0
getfield com/teamspeak/ts3client/t/aD Landroid/content/BroadcastReceiver;
ifnull L1
aload 0
invokevirtual com/teamspeak/ts3client/t/i()Landroid/support/v4/app/bb;
aload 0
getfield com/teamspeak/ts3client/t/aD Landroid/content/BroadcastReceiver;
invokevirtual android/support/v4/app/bb/unregisterReceiver(Landroid/content/BroadcastReceiver;)V
L1:
aload 0
getfield com/teamspeak/ts3client/t/k Lcom/teamspeak/ts3client/d/h;
invokevirtual com/teamspeak/ts3client/d/h/l()Z
ifeq L12
aload 0
getfield com/teamspeak/ts3client/t/k Lcom/teamspeak/ts3client/d/h;
invokevirtual com/teamspeak/ts3client/d/h/b()V
L12:
aload 0
iconst_1
putfield com/teamspeak/ts3client/t/aw Z
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/g Z
ifeq L13
aload 0
getfield android/support/v4/app/Fragment/M Landroid/support/v4/app/bl;
invokevirtual android/support/v4/app/bi/f()I
ifle L14
aload 0
getfield android/support/v4/app/Fragment/M Landroid/support/v4/app/bl;
invokevirtual android/support/v4/app/bi/g()Landroid/support/v4/app/bj;
invokeinterface android/support/v4/app/bj/a()I 0
istore 2
aload 0
getfield android/support/v4/app/Fragment/M Landroid/support/v4/app/bl;
iload 2
invokevirtual android/support/v4/app/bi/b(I)V
aload 0
getfield android/support/v4/app/Fragment/M Landroid/support/v4/app/bl;
invokevirtual android/support/v4/app/bi/a()Landroid/support/v4/app/cd;
astore 3
aload 3
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/c Landroid/support/v4/app/Fragment;
invokevirtual android/support/v4/app/cd/b(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/cd;
pop
aload 3
sipush 8194
invokevirtual android/support/v4/app/cd/a(I)Landroid/support/v4/app/cd;
pop
aload 3
invokevirtual android/support/v4/app/cd/i()I
pop
L14:
aload 0
getfield android/support/v4/app/Fragment/M Landroid/support/v4/app/bl;
invokevirtual android/support/v4/app/bi/a()Landroid/support/v4/app/cd;
astore 3
L3:
aload 3
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/k Lcom/teamspeak/ts3client/t;
invokevirtual android/support/v4/app/cd/b(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/cd;
pop
L4:
aload 3
aload 0
getfield android/support/v4/app/Fragment/M Landroid/support/v4/app/bl;
ldc "Bookmark"
invokevirtual android/support/v4/app/bi/a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;
invokevirtual android/support/v4/app/cd/f(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/cd;
pop
aload 3
sipush 8194
invokevirtual android/support/v4/app/cd/a(I)Landroid/support/v4/app/cd;
pop
aload 3
invokevirtual android/support/v4/app/cd/i()I
pop
L15:
aload 0
invokevirtual com/teamspeak/ts3client/t/i()Landroid/support/v4/app/bb;
invokevirtual android/support/v4/app/bb/getWindow()Landroid/view/Window;
ldc_w 32768
invokevirtual android/view/Window/clearFlags(I)V
aload 0
invokevirtual com/teamspeak/ts3client/t/i()Landroid/support/v4/app/bb;
invokevirtual android/support/v4/app/bb/getWindow()Landroid/view/Window;
ldc_w 524288
invokevirtual android/view/Window/clearFlags(I)V
aload 0
invokevirtual com/teamspeak/ts3client/t/i()Landroid/support/v4/app/bb;
invokevirtual android/support/v4/app/bb/getWindow()Landroid/view/Window;
ldc_w 4194304
invokevirtual android/view/Window/clearFlags(I)V
aload 0
invokevirtual com/teamspeak/ts3client/t/i()Landroid/support/v4/app/bb;
invokevirtual android/support/v4/app/bb/getWindow()Landroid/view/Window;
sipush 128
invokevirtual android/view/Window/clearFlags(I)V
aload 0
getfield com/teamspeak/ts3client/t/m Landroid/telephony/TelephonyManager;
ifnull L16
aload 0
getfield com/teamspeak/ts3client/t/m Landroid/telephony/TelephonyManager;
aload 0
getfield com/teamspeak/ts3client/t/aH Landroid/telephony/PhoneStateListener;
iconst_0
invokevirtual android/telephony/TelephonyManager/listen(Landroid/telephony/PhoneStateListener;I)V
L16:
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
aload 0
getfield com/teamspeak/ts3client/t/aB Lcom/teamspeak/ts3client/bf;
invokeinterface android/content/SharedPreferences/unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V 1
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/l Landroid/app/NotificationManager;
ifnull L6
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/l Landroid/app/NotificationManager;
iconst_1
invokevirtual android/app/NotificationManager/cancel(I)V
L6:
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
astore 3
aload 3
getfield com/teamspeak/ts3client/data/e/m Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/d Ljava/util/logging/Logger;
getstatic java/util/logging/Level/INFO Ljava/util/logging/Level;
ldc "Stop AUDIO"
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;)V
aload 3
getfield com/teamspeak/ts3client/data/e/b Lcom/teamspeak/ts3client/a/k;
invokevirtual com/teamspeak/ts3client/a/k/f()V
aload 3
getfield com/teamspeak/ts3client/data/e/b Lcom/teamspeak/ts3client/a/k;
invokevirtual com/teamspeak/ts3client/a/k/e()V
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
astore 3
invokestatic com/teamspeak/ts3client/data/d/q/a()V
invokestatic com/teamspeak/ts3client/data/v/a()Lcom/teamspeak/ts3client/data/v;
iconst_0
putfield com/teamspeak/ts3client/data/v/b Z
aload 3
getfield com/teamspeak/ts3client/data/e/m Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/f Lcom/teamspeak/ts3client/StartGUIFragment;
getfield com/teamspeak/ts3client/StartGUIFragment/o Lcom/teamspeak/ts3client/bookmark/e;
getfield com/teamspeak/ts3client/bookmark/e/b Lcom/teamspeak/ts3client/data/b/a;
aload 3
getfield com/teamspeak/ts3client/data/e/v Lcom/teamspeak/ts3client/data/ab;
getfield com/teamspeak/ts3client/data/ab/l J
aload 3
getfield com/teamspeak/ts3client/data/e/v Lcom/teamspeak/ts3client/data/ab;
invokevirtual com/teamspeak/ts3client/data/b/a/a(JLcom/teamspeak/ts3client/data/ab;)Z
pop
aload 3
getfield com/teamspeak/ts3client/data/e/m Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/d Ljava/util/logging/Logger;
getstatic java/util/logging/Level/INFO Ljava/util/logging/Level;
ldc "Disconnecting from Server"
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;)V
aload 3
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 3
getfield com/teamspeak/ts3client/data/e/e J
ldc "....TS3 Android...."
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_stopConnection(JLjava/lang/String;)I
pop
aload 3
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 3
getfield com/teamspeak/ts3client/data/e/e J
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_closeCaptureDevice(J)I
pop
aload 3
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 3
getfield com/teamspeak/ts3client/data/e/e J
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_closePlaybackDevice(J)I
pop
aload 3
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 3
getfield com/teamspeak/ts3client/data/e/e J
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_destroyServerConnectionHandler(J)I
pop
aload 3
getfield com/teamspeak/ts3client/data/e/m Lcom/teamspeak/ts3client/Ts3Application;
aload 3
getfield com/teamspeak/ts3client/data/e/m Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/k Landroid/content/Intent;
invokevirtual com/teamspeak/ts3client/Ts3Application/stopService(Landroid/content/Intent;)Z
pop
aload 3
getfield com/teamspeak/ts3client/data/e/b Lcom/teamspeak/ts3client/a/k;
invokevirtual com/teamspeak/ts3client/a/k/b()V
new com/teamspeak/ts3client/data/f
dup
aload 3
invokespecial com/teamspeak/ts3client/data/f/<init>(Lcom/teamspeak/ts3client/data/e;)V
astore 4
new android/os/Handler
dup
invokespecial android/os/Handler/<init>()V
aload 4
ldc2_w 3000L
invokevirtual android/os/Handler/postDelayed(Ljava/lang/Runnable;J)Z
pop
aload 3
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
ldc ""
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_unregisterCustomDevice(Ljava/lang/String;)I
pop
aload 3
aconst_null
putfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 3
getfield com/teamspeak/ts3client/data/e/m Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/getBaseContext()Landroid/content/Context;
invokestatic com/teamspeak/ts3client/chat/d/a(Landroid/content/Context;)Lcom/teamspeak/ts3client/chat/d;
astore 4
aload 4
aconst_null
putfield com/teamspeak/ts3client/chat/d/c Lcom/teamspeak/ts3client/chat/a;
aload 4
aconst_null
putfield com/teamspeak/ts3client/chat/d/d Lcom/teamspeak/ts3client/chat/a;
aload 4
getfield com/teamspeak/ts3client/chat/d/a Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/clear()V
aload 4
getfield com/teamspeak/ts3client/chat/d/b Ljava/util/HashMap;
invokevirtual java/util/HashMap/clear()V
aload 4
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield com/teamspeak/ts3client/chat/d/a Ljava/util/ArrayList;
aload 4
new java/util/HashMap
dup
invokespecial java/util/HashMap/<init>()V
putfield com/teamspeak/ts3client/chat/d/b Ljava/util/HashMap;
aload 3
getfield com/teamspeak/ts3client/data/e/m Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/d Ljava/util/logging/Logger;
getstatic java/util/logging/Level/INFO Ljava/util/logging/Level;
ldc "ClientLib closed"
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;)V
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/d Ljava/util/logging/Logger;
getstatic java/util/logging/Level/INFO Ljava/util/logging/Level;
ldc "Connect status: STATUS_DISCONNECTED"
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;)V
L7:
getstatic com/teamspeak/ts3client/data/b/c/a Lcom/teamspeak/ts3client/data/b/c;
new java/util/Vector
dup
invokespecial java/util/Vector/<init>()V
putfield com/teamspeak/ts3client/data/b/c/b Ljava/util/Vector;
L9:
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/l Lcom/teamspeak/ts3client/ConnectionBackground;
astore 3
aload 3
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/c Lcom/teamspeak/ts3client/jni/l;
aload 3
invokevirtual com/teamspeak/ts3client/jni/l/b(Lcom/teamspeak/ts3client/data/w;)V
aload 3
getfield com/teamspeak/ts3client/ConnectionBackground/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
aload 3
getfield com/teamspeak/ts3client/ConnectionBackground/f Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;
invokeinterface android/content/SharedPreferences/unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V 1
L10:
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/b Landroid/os/PowerManager$WakeLock;
ifnull L17
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/b Landroid/os/PowerManager$WakeLock;
invokevirtual android/os/PowerManager$WakeLock/isHeld()Z
ifeq L17
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/b Landroid/os/PowerManager$WakeLock;
invokevirtual android/os/PowerManager$WakeLock/release()V
L17:
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/m Landroid/os/PowerManager$WakeLock;
ifnull L18
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/m Landroid/os/PowerManager$WakeLock;
invokevirtual android/os/PowerManager$WakeLock/isHeld()Z
ifeq L18
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/m Landroid/os/PowerManager$WakeLock;
invokevirtual android/os/PowerManager$WakeLock/release()V
L18:
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/n Landroid/net/wifi/WifiManager$WifiLock;
ifnull L19
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/n Landroid/net/wifi/WifiManager$WifiLock;
invokevirtual android/net/wifi/WifiManager$WifiLock/isHeld()Z
ifeq L19
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/n Landroid/net/wifi/WifiManager$WifiLock;
invokevirtual android/net/wifi/WifiManager$WifiLock/release()V
L19:
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
ifnull L20
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/v Lcom/teamspeak/ts3client/data/ab;
astore 3
L21:
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/f Lcom/teamspeak/ts3client/StartGUIFragment;
invokevirtual com/teamspeak/ts3client/StartGUIFragment/j()V
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
ifnull L22
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
aconst_null
putfield com/teamspeak/ts3client/data/e/k Lcom/teamspeak/ts3client/t;
L22:
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
aconst_null
putfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
aload 0
putfield com/teamspeak/ts3client/Ts3Application/c Landroid/support/v4/app/Fragment;
iload 1
ifeq L23
aload 3
ifnull L23
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/f Lcom/teamspeak/ts3client/StartGUIFragment;
astore 4
aload 3
getfield com/teamspeak/ts3client/data/ab/n I
aload 4
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
ldc "reconnect.limit"
ldc "15"
invokeinterface android/content/SharedPreferences/getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String; 2
invokestatic java/lang/Integer/parseInt(Ljava/lang/String;)I
iconst_1
isub
if_icmple L24
new android/content/Intent
dup
aload 4
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
ldc com/teamspeak/ts3client/StartGUIFragment
invokespecial android/content/Intent/<init>(Landroid/content/Context;Ljava/lang/Class;)V
astore 3
aload 3
ldc "android.intent.action.MAIN"
invokevirtual android/content/Intent/setAction(Ljava/lang/String;)Landroid/content/Intent;
pop
aload 3
ldc "android.intent.category.LAUNCHER"
invokevirtual android/content/Intent/addCategory(Ljava/lang/String;)Landroid/content/Intent;
pop
aload 4
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
iconst_0
aload 3
iconst_0
invokestatic android/app/PendingIntent/getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;
astore 3
new android/support/v4/app/dm
dup
aload 4
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/getApplicationContext()Landroid/content/Context;
invokespecial android/support/v4/app/dm/<init>(Landroid/content/Context;)V
astore 5
aload 5
ldc_w 2130837622
invokevirtual android/support/v4/app/dm/a(I)Landroid/support/v4/app/dm;
ldc "connection.reconnect"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/support/v4/app/dm/c(Ljava/lang/CharSequence;)Landroid/support/v4/app/dm;
invokestatic java/lang/System/currentTimeMillis()J
invokevirtual android/support/v4/app/dm/a(J)Landroid/support/v4/app/dm;
invokevirtual android/support/v4/app/dm/a()Landroid/support/v4/app/dm;
ldc "connection.reconnect"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/support/v4/app/dm/a(Ljava/lang/CharSequence;)Landroid/support/v4/app/dm;
ldc "connection.reconnect.text"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 4
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
ldc "reconnect.limit"
ldc "15"
invokeinterface android/content/SharedPreferences/getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String; 2
invokestatic java/lang/Integer/parseInt(Ljava/lang/String;)I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokevirtual android/support/v4/app/dm/b(Ljava/lang/CharSequence;)Landroid/support/v4/app/dm;
aload 3
putfield android/support/v4/app/dm/d Landroid/app/PendingIntent;
aload 5
invokevirtual android/support/v4/app/dm/c()Landroid/app/Notification;
astore 3
aload 3
aload 3
getfield android/app/Notification/flags I
bipush 20
ior
putfield android/app/Notification/flags I
aload 4
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/l Landroid/app/NotificationManager;
aconst_null
bipush 101
aload 3
invokevirtual android/app/NotificationManager/notify(Ljava/lang/String;ILandroid/app/Notification;)V
aload 4
iconst_0
putfield com/teamspeak/ts3client/StartGUIFragment/p Z
aload 4
iconst_1
putfield com/teamspeak/ts3client/StartGUIFragment/s Z
L23:
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/o Landroid/support/v7/app/a;
ldc "TeamSpeak"
invokevirtual android/support/v7/app/a/a(Ljava/lang/CharSequence;)V
return
L2:
astore 3
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/d Ljava/util/logging/Logger;
getstatic java/util/logging/Level/WARNING Ljava/util/logging/Level;
ldc "java.lang.IllegalArgumentException: Receiver not registered"
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;)V
goto L1
L13:
iload 1
ifne L15
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/f Lcom/teamspeak/ts3client/StartGUIFragment;
iconst_1
putfield com/teamspeak/ts3client/StartGUIFragment/r Z
goto L15
L24:
new android/content/Intent
dup
aload 4
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
ldc com/teamspeak/ts3client/StartGUIFragment
invokespecial android/content/Intent/<init>(Landroid/content/Context;Ljava/lang/Class;)V
astore 5
aload 5
ldc "android.intent.action.MAIN"
invokevirtual android/content/Intent/setAction(Ljava/lang/String;)Landroid/content/Intent;
pop
aload 5
ldc "android.intent.category.LAUNCHER"
invokevirtual android/content/Intent/addCategory(Ljava/lang/String;)Landroid/content/Intent;
pop
aload 4
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
iconst_0
aload 5
iconst_0
invokestatic android/app/PendingIntent/getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;
astore 5
new android/support/v4/app/dm
dup
aload 4
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/getApplicationContext()Landroid/content/Context;
invokespecial android/support/v4/app/dm/<init>(Landroid/content/Context;)V
astore 6
aload 6
ldc_w 2130837622
invokevirtual android/support/v4/app/dm/a(I)Landroid/support/v4/app/dm;
ldc "connection.reconnect.retry"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/support/v4/app/dm/c(Ljava/lang/CharSequence;)Landroid/support/v4/app/dm;
invokestatic java/lang/System/currentTimeMillis()J
invokevirtual android/support/v4/app/dm/a(J)Landroid/support/v4/app/dm;
invokevirtual android/support/v4/app/dm/a()Landroid/support/v4/app/dm;
ldc "connection.reconnect.retry"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/support/v4/app/dm/a(Ljava/lang/CharSequence;)Landroid/support/v4/app/dm;
ldc "connection.reconnect.retry"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/support/v4/app/dm/b(Ljava/lang/CharSequence;)Landroid/support/v4/app/dm;
aload 5
putfield android/support/v4/app/dm/d Landroid/app/PendingIntent;
aload 6
invokevirtual android/support/v4/app/dm/c()Landroid/app/Notification;
astore 5
aload 5
aload 5
getfield android/app/Notification/flags I
bipush 20
ior
putfield android/app/Notification/flags I
aload 4
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/l Landroid/app/NotificationManager;
aconst_null
bipush 101
aload 5
invokevirtual android/app/NotificationManager/notify(Ljava/lang/String;ILandroid/app/Notification;)V
aload 3
aload 3
getfield com/teamspeak/ts3client/data/ab/n I
iconst_1
iadd
putfield com/teamspeak/ts3client/data/ab/n I
aload 4
getfield com/teamspeak/ts3client/StartGUIFragment/o Lcom/teamspeak/ts3client/bookmark/e;
aload 3
invokevirtual com/teamspeak/ts3client/bookmark/e/a(Lcom/teamspeak/ts3client/data/ab;)V
goto L23
L11:
astore 3
goto L10
L8:
astore 3
goto L7
L5:
astore 4
goto L4
L20:
aconst_null
astore 3
goto L21
.limit locals 7
.limit stack 8
.end method

.method public final e()V
aload 0
invokespecial android/support/v4/app/Fragment/e()V
new android/content/IntentFilter
dup
invokespecial android/content/IntentFilter/<init>()V
astore 1
aload 1
ldc "android.intent.action.HEADSET_PLUG"
invokevirtual android/content/IntentFilter/addAction(Ljava/lang/String;)V
aload 1
ldc "android.intent.action.NEW_OUTGOING_CALL"
invokevirtual android/content/IntentFilter/addAction(Ljava/lang/String;)V
aload 0
invokevirtual com/teamspeak/ts3client/t/i()Landroid/support/v4/app/bb;
aload 0
getfield com/teamspeak/ts3client/t/aD Landroid/content/BroadcastReceiver;
aload 1
invokevirtual android/support/v4/app/bb/registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
pop
return
.limit locals 2
.limit stack 3
.end method

.method public final e(Z)V
iload 1
ifeq L0
aload 0
invokevirtual com/teamspeak/ts3client/t/i()Landroid/support/v4/app/bb;
new com/teamspeak/ts3client/ak
dup
aload 0
invokespecial com/teamspeak/ts3client/ak/<init>(Lcom/teamspeak/ts3client/t;)V
invokevirtual android/support/v4/app/bb/runOnUiThread(Ljava/lang/Runnable;)V
aload 0
iconst_1
putfield com/teamspeak/ts3client/t/ay Z
return
L0:
aload 0
iconst_0
putfield com/teamspeak/ts3client/t/ay Z
return
.limit locals 2
.limit stack 4
.end method

.method public final f(Z)V
aload 0
invokevirtual com/teamspeak/ts3client/t/i()Landroid/support/v4/app/bb;
new com/teamspeak/ts3client/au
dup
aload 0
iload 1
invokespecial com/teamspeak/ts3client/au/<init>(Lcom/teamspeak/ts3client/t;Z)V
invokevirtual android/support/v4/app/bb/runOnUiThread(Ljava/lang/Runnable;)V
return
.limit locals 2
.limit stack 5
.end method

.method public final g(Z)V
iload 1
ifeq L0
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
getstatic com/teamspeak/ts3client/jni/d/k Lcom/teamspeak/ts3client/jni/d;
iconst_0
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/a(JLcom/teamspeak/ts3client/jni/d;I)I
pop
aload 0
iconst_1
putfield com/teamspeak/ts3client/t/au Z
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/b Lcom/teamspeak/ts3client/a/k;
invokevirtual com/teamspeak/ts3client/a/k/d()V
return
L0:
aload 0
getfield com/teamspeak/ts3client/t/aA Z
ifeq L1
aload 0
iconst_0
putfield com/teamspeak/ts3client/t/au Z
return
L1:
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
getstatic com/teamspeak/ts3client/jni/d/k Lcom/teamspeak/ts3client/jni/d;
iconst_1
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/a(JLcom/teamspeak/ts3client/jni/d;I)I
pop
aload 0
iconst_0
putfield com/teamspeak/ts3client/t/au Z
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/b Lcom/teamspeak/ts3client/a/k;
invokevirtual com/teamspeak/ts3client/a/k/f()V
return
.limit locals 2
.limit stack 5
.end method

.method public final h(Z)V
aload 0
invokevirtual com/teamspeak/ts3client/t/i()Landroid/support/v4/app/bb;
new com/teamspeak/ts3client/ax
dup
aload 0
iload 1
invokespecial com/teamspeak/ts3client/ax/<init>(Lcom/teamspeak/ts3client/t;Z)V
invokevirtual android/support/v4/app/bb/runOnUiThread(Ljava/lang/Runnable;)V
return
.limit locals 2
.limit stack 5
.end method

.method public final i(Z)V
iload 1
ifne L0
aload 0
getfield com/teamspeak/ts3client/t/au Z
ifeq L0
aload 0
iconst_1
putfield com/teamspeak/ts3client/t/aA Z
L0:
aload 0
iload 1
putfield com/teamspeak/ts3client/t/ax Z
return
.limit locals 2
.limit stack 2
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
aload 0
aload 1
invokespecial android/support/v4/app/Fragment/onConfigurationChanged(Landroid/content/res/Configuration;)V
aload 0
invokevirtual com/teamspeak/ts3client/t/A()V
return
.limit locals 2
.limit stack 2
.end method

.method public final s()V
aload 0
invokespecial android/support/v4/app/Fragment/s()V
aload 0
getfield com/teamspeak/ts3client/t/j Lcom/teamspeak/ts3client/data/customExpandableListView;
ifnull L0
aload 0
getfield com/teamspeak/ts3client/t/aw Z
iconst_1
if_icmpeq L0
aload 0
invokevirtual com/teamspeak/ts3client/t/i()Landroid/support/v4/app/bb;
new com/teamspeak/ts3client/an
dup
aload 0
invokespecial com/teamspeak/ts3client/an/<init>(Lcom/teamspeak/ts3client/t;)V
invokevirtual android/support/v4/app/bb/runOnUiThread(Ljava/lang/Runnable;)V
L0:
return
.limit locals 1
.limit stack 4
.end method

.method public final t()V
aload 0
invokespecial android/support/v4/app/Fragment/t()V
return
.limit locals 1
.limit stack 1
.end method

.method public final u()V
aload 0
invokespecial android/support/v4/app/Fragment/u()V
aload 0
getfield com/teamspeak/ts3client/t/aF Z
ifne L0
aload 0
getfield com/teamspeak/ts3client/t/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/l Lcom/teamspeak/ts3client/ConnectionBackground;
invokevirtual com/teamspeak/ts3client/ConnectionBackground/d()V
aload 0
invokevirtual com/teamspeak/ts3client/t/i()Landroid/support/v4/app/bb;
aload 0
getfield com/teamspeak/ts3client/t/aD Landroid/content/BroadcastReceiver;
invokevirtual android/support/v4/app/bb/unregisterReceiver(Landroid/content/BroadcastReceiver;)V
aload 0
iconst_0
invokevirtual com/teamspeak/ts3client/t/d(Z)V
L0:
return
.limit locals 1
.limit stack 2
.end method

.method public final y()V
aload 0
invokevirtual com/teamspeak/ts3client/t/l()Z
ifeq L0
aload 0
invokevirtual com/teamspeak/ts3client/t/i()Landroid/support/v4/app/bb;
new com/teamspeak/ts3client/be
dup
aload 0
invokespecial com/teamspeak/ts3client/be/<init>(Lcom/teamspeak/ts3client/t;)V
invokevirtual android/support/v4/app/bb/runOnUiThread(Ljava/lang/Runnable;)V
L0:
return
.limit locals 1
.limit stack 4
.end method

.method public final z()V
aload 0
invokevirtual com/teamspeak/ts3client/t/l()Z
ifeq L0
aload 0
invokevirtual com/teamspeak/ts3client/t/i()Landroid/support/v4/app/bb;
new com/teamspeak/ts3client/x
dup
aload 0
invokespecial com/teamspeak/ts3client/x/<init>(Lcom/teamspeak/ts3client/t;)V
invokevirtual android/support/v4/app/bb/runOnUiThread(Ljava/lang/Runnable;)V
L0:
return
.limit locals 1
.limit stack 4
.end method
