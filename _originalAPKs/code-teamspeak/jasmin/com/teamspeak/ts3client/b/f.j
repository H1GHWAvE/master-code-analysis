.bytecode 50.0
.class public final synchronized com/teamspeak/ts3client/b/f
.super android/widget/ArrayAdapter

.field 'a' Lcom/teamspeak/ts3client/b/j;

.field final synthetic 'b' Lcom/teamspeak/ts3client/b/b;

.field private 'c' Landroid/view/LayoutInflater;

.method public <init>(Lcom/teamspeak/ts3client/b/b;Landroid/content/Context;)V
aload 0
aload 1
putfield com/teamspeak/ts3client/b/f/b Lcom/teamspeak/ts3client/b/b;
aload 0
aload 2
ldc_w 17367043
invokespecial android/widget/ArrayAdapter/<init>(Landroid/content/Context;I)V
aload 0
aload 2
invokestatic android/view/LayoutInflater/from(Landroid/content/Context;)Landroid/view/LayoutInflater;
putfield com/teamspeak/ts3client/b/f/c Landroid/view/LayoutInflater;
return
.limit locals 3
.limit stack 3
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)Landroid/widget/TableRow;
new android/widget/TableRow
dup
aload 0
invokevirtual com/teamspeak/ts3client/b/f/getContext()Landroid/content/Context;
invokespecial android/widget/TableRow/<init>(Landroid/content/Context;)V
astore 3
new android/widget/TextView
dup
aload 0
invokevirtual com/teamspeak/ts3client/b/f/getContext()Landroid/content/Context;
invokespecial android/widget/TextView/<init>(Landroid/content/Context;)V
astore 4
aload 4
aload 1
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
aload 4
iconst_0
iconst_0
iconst_5
iconst_0
invokevirtual android/widget/TextView/setPadding(IIII)V
aload 4
aload 0
invokevirtual com/teamspeak/ts3client/b/f/getContext()Landroid/content/Context;
ldc_w 2131165314
invokevirtual android/widget/TextView/setTextAppearance(Landroid/content/Context;I)V
new android/widget/TextView
dup
aload 0
invokevirtual com/teamspeak/ts3client/b/f/getContext()Landroid/content/Context;
invokespecial android/widget/TextView/<init>(Landroid/content/Context;)V
astore 1
aload 1
aload 2
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
aload 3
aload 4
invokevirtual android/widget/TableRow/addView(Landroid/view/View;)V
aload 3
aload 1
invokevirtual android/widget/TableRow/addView(Landroid/view/View;)V
aload 3
areturn
.limit locals 5
.limit stack 5
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/b/f;Lcom/teamspeak/ts3client/jni/events/rare/BanList;Landroid/widget/TableLayout;)V
aload 0
aload 1
aload 2
invokespecial com/teamspeak/ts3client/b/f/a(Lcom/teamspeak/ts3client/jni/events/rare/BanList;Landroid/widget/TableLayout;)V
return
.limit locals 3
.limit stack 3
.end method

.method private a(Lcom/teamspeak/ts3client/jni/events/rare/BanList;Landroid/widget/TableLayout;)V
aload 1
getfield com/teamspeak/ts3client/jni/events/rare/BanList/c Ljava/lang/String;
ldc ""
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L0
aload 2
aload 0
ldc "Name"
aload 1
getfield com/teamspeak/ts3client/jni/events/rare/BanList/c Ljava/lang/String;
invokespecial com/teamspeak/ts3client/b/f/a(Ljava/lang/String;Ljava/lang/String;)Landroid/widget/TableRow;
invokevirtual android/widget/TableLayout/addView(Landroid/view/View;)V
L0:
aload 1
getfield com/teamspeak/ts3client/jni/events/rare/BanList/b Ljava/lang/String;
ldc ""
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L1
aload 2
aload 0
ldc "IP"
aload 1
getfield com/teamspeak/ts3client/jni/events/rare/BanList/b Ljava/lang/String;
invokespecial com/teamspeak/ts3client/b/f/a(Ljava/lang/String;Ljava/lang/String;)Landroid/widget/TableRow;
invokevirtual android/widget/TableLayout/addView(Landroid/view/View;)V
L1:
aload 1
getfield com/teamspeak/ts3client/jni/events/rare/BanList/d Ljava/lang/String;
ldc ""
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L2
aload 2
aload 0
ldc "Uid"
aload 1
getfield com/teamspeak/ts3client/jni/events/rare/BanList/d Ljava/lang/String;
invokespecial com/teamspeak/ts3client/b/f/a(Ljava/lang/String;Ljava/lang/String;)Landroid/widget/TableRow;
invokevirtual android/widget/TableLayout/addView(Landroid/view/View;)V
L2:
aload 1
getfield com/teamspeak/ts3client/jni/events/rare/BanList/h Ljava/lang/String;
ldc ""
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L3
aload 2
aload 0
ldc "Nick"
aload 1
getfield com/teamspeak/ts3client/jni/events/rare/BanList/h Ljava/lang/String;
invokespecial com/teamspeak/ts3client/b/f/a(Ljava/lang/String;Ljava/lang/String;)Landroid/widget/TableRow;
invokevirtual android/widget/TableLayout/addView(Landroid/view/View;)V
L3:
aload 2
aload 0
ldc "banlist.creator"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
aload 1
getfield com/teamspeak/ts3client/jni/events/rare/BanList/e Ljava/lang/String;
invokespecial com/teamspeak/ts3client/b/f/a(Ljava/lang/String;Ljava/lang/String;)Landroid/widget/TableRow;
invokevirtual android/widget/TableLayout/addView(Landroid/view/View;)V
return
.limit locals 3
.limit stack 4
.end method

.method public final getFilter()Landroid/widget/Filter;
aload 0
getfield com/teamspeak/ts3client/b/f/a Lcom/teamspeak/ts3client/b/j;
ifnonnull L0
aload 0
new com/teamspeak/ts3client/b/j
dup
aload 0
invokespecial com/teamspeak/ts3client/b/j/<init>(Lcom/teamspeak/ts3client/b/f;)V
putfield com/teamspeak/ts3client/b/f/a Lcom/teamspeak/ts3client/b/j;
L0:
aload 0
getfield com/teamspeak/ts3client/b/f/a Lcom/teamspeak/ts3client/b/j;
areturn
.limit locals 1
.limit stack 4
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
aload 0
getfield com/teamspeak/ts3client/b/f/c Landroid/view/LayoutInflater;
ldc_w 2130903067
aconst_null
invokevirtual android/view/LayoutInflater/inflate(ILandroid/view/ViewGroup;)Landroid/view/View;
astore 2
aload 2
ldc_w 2131492999
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/TableLayout
astore 3
aload 2
ldc_w 2131492998
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/TextView
astore 4
aload 2
ldc_w 2131493000
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/TextView
astore 5
aload 0
iload 1
invokevirtual com/teamspeak/ts3client/b/f/getItem(I)Ljava/lang/Object;
checkcast com/teamspeak/ts3client/jni/events/rare/BanList
astore 6
aload 0
aload 6
aload 3
invokespecial com/teamspeak/ts3client/b/f/a(Lcom/teamspeak/ts3client/jni/events/rare/BanList;Landroid/widget/TableLayout;)V
aload 4
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "banlist.reason"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 6
getfield com/teamspeak/ts3client/jni/events/rare/BanList/g Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
aload 4
aload 0
invokevirtual com/teamspeak/ts3client/b/f/getContext()Landroid/content/Context;
ldc_w 2131165315
invokevirtual android/widget/TextView/setTextAppearance(Landroid/content/Context;I)V
aload 5
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "banlist.created"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 6
getfield com/teamspeak/ts3client/jni/events/rare/BanList/k Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "banlist.expires"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 6
getfield com/teamspeak/ts3client/jni/events/rare/BanList/j Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
aload 5
aload 0
invokevirtual com/teamspeak/ts3client/b/f/getContext()Landroid/content/Context;
ldc_w 2131165313
invokevirtual android/widget/TextView/setTextAppearance(Landroid/content/Context;I)V
aload 2
new com/teamspeak/ts3client/b/g
dup
aload 0
aload 6
invokespecial com/teamspeak/ts3client/b/g/<init>(Lcom/teamspeak/ts3client/b/f;Lcom/teamspeak/ts3client/jni/events/rare/BanList;)V
invokevirtual android/view/View/setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V
aload 2
areturn
.limit locals 7
.limit stack 5
.end method
