.bytecode 50.0
.class public synchronized com/teamspeak/ts3client/OpenBrowserIntent
.super android/app/Activity

.field 'a' Z

.method public <init>()V
aload 0
invokespecial android/app/Activity/<init>()V
aload 0
iconst_0
putfield com/teamspeak/ts3client/OpenBrowserIntent/a Z
return
.limit locals 1
.limit stack 2
.end method

.method private a(Landroid/net/Uri;)V
.catch java/io/UnsupportedEncodingException from L0 to L1 using L2
.catch java/io/UnsupportedEncodingException from L3 to L4 using L2
.catch java/io/UnsupportedEncodingException from L5 to L6 using L2
.catch java/io/UnsupportedEncodingException from L7 to L8 using L2
.catch java/io/UnsupportedEncodingException from L8 to L9 using L2
.catch java/io/UnsupportedEncodingException from L9 to L10 using L2
.catch java/io/UnsupportedEncodingException from L10 to L11 using L2
.catch java/io/UnsupportedEncodingException from L11 to L12 using L2
.catch java/io/UnsupportedEncodingException from L12 to L13 using L2
.catch java/io/UnsupportedEncodingException from L13 to L14 using L2
.catch java/io/UnsupportedEncodingException from L14 to L15 using L2
.catch java/io/UnsupportedEncodingException from L16 to L17 using L2
aload 0
ldc "activity"
invokevirtual com/teamspeak/ts3client/OpenBrowserIntent/getSystemService(Ljava/lang/String;)Ljava/lang/Object;
checkcast android/app/ActivityManager
ldc_w 2147483647
invokevirtual android/app/ActivityManager/getRunningServices(I)Ljava/util/List;
invokeinterface java/util/List/iterator()Ljava/util/Iterator; 0
astore 4
L18:
aload 4
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L19
ldc "com.teamspeak.ts3client.ConnectionBackground"
aload 4
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast android/app/ActivityManager$RunningServiceInfo
getfield android/app/ActivityManager$RunningServiceInfo/service Landroid/content/ComponentName;
invokevirtual android/content/ComponentName/getClassName()Ljava/lang/String;
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L18
aload 0
iconst_1
putfield com/teamspeak/ts3client/OpenBrowserIntent/a Z
goto L18
L19:
new com/teamspeak/ts3client/data/ab
dup
invokespecial com/teamspeak/ts3client/data/ab/<init>()V
astore 4
aload 1
ifnull L20
aload 1
invokevirtual android/net/Uri/getQuery()Ljava/lang/String;
ifnonnull L0
aload 4
aload 1
invokevirtual android/net/Uri/getHost()Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/data/ab/a(Ljava/lang/String;)V
L21:
new android/content/Intent
dup
aload 0
invokevirtual com/teamspeak/ts3client/OpenBrowserIntent/getApplicationContext()Landroid/content/Context;
ldc com/teamspeak/ts3client/StartGUIFragment
invokespecial android/content/Intent/<init>(Landroid/content/Context;Ljava/lang/Class;)V
astore 5
aload 5
ldc "UrlStart"
iconst_1
invokevirtual android/content/Intent/putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
pop
aload 5
ldc "uri"
aload 1
invokevirtual android/content/Intent/putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;
pop
aload 5
ldc "address"
aload 4
getfield com/teamspeak/ts3client/data/ab/c Ljava/lang/String;
invokevirtual android/content/Intent/putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
pop
aload 0
aload 5
invokevirtual com/teamspeak/ts3client/OpenBrowserIntent/startActivity(Landroid/content/Intent;)V
aload 0
invokevirtual com/teamspeak/ts3client/OpenBrowserIntent/finish()V
L20:
return
L0:
aload 1
invokevirtual android/net/Uri/getQuery()Ljava/lang/String;
ldc "UTF-8"
invokestatic java/net/URLDecoder/decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
astore 6
new java/util/HashMap
dup
invokespecial java/util/HashMap/<init>()V
astore 5
aload 6
ldc "&"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L7
aload 6
ldc "&"
invokevirtual java/lang/String/split(Ljava/lang/String;)[Ljava/lang/String;
astore 6
aload 6
arraylength
istore 3
L1:
iconst_0
istore 2
L22:
iload 2
iload 3
if_icmpge L8
L3:
aload 6
iload 2
aaload
ldc "="
invokevirtual java/lang/String/split(Ljava/lang/String;)[Ljava/lang/String;
astore 7
aload 7
arraylength
iconst_1
if_icmple L5
aload 5
aload 7
iconst_0
aaload
aload 7
iconst_1
aaload
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
L4:
goto L23
L5:
aload 5
aload 7
iconst_0
aaload
ldc ""
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
L6:
goto L23
L2:
astore 5
aload 5
invokevirtual java/io/UnsupportedEncodingException/printStackTrace()V
goto L21
L7:
aload 6
ldc "="
invokevirtual java/lang/String/split(Ljava/lang/String;)[Ljava/lang/String;
astore 6
aload 6
arraylength
iconst_1
if_icmple L16
aload 5
aload 6
iconst_0
aaload
aload 6
iconst_1
aaload
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
L8:
aload 4
aload 1
invokevirtual android/net/Uri/getHost()Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/data/ab/a(Ljava/lang/String;)V
aload 5
ldc "port"
invokevirtual java/util/HashMap/containsKey(Ljava/lang/Object;)Z
ifeq L9
aload 4
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 4
getfield com/teamspeak/ts3client/data/ab/c Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ":"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 5
ldc "port"
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/String
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/data/ab/a(Ljava/lang/String;)V
L9:
aload 5
ldc "nickname"
invokevirtual java/util/HashMap/containsKey(Ljava/lang/Object;)Z
ifeq L10
aload 4
aload 5
ldc "nickname"
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/String
putfield com/teamspeak/ts3client/data/ab/f Ljava/lang/String;
L10:
aload 5
ldc "password"
invokevirtual java/util/HashMap/containsKey(Ljava/lang/Object;)Z
ifeq L11
aload 4
aload 5
ldc "password"
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/String
putfield com/teamspeak/ts3client/data/ab/e Ljava/lang/String;
L11:
aload 5
ldc "channel"
invokevirtual java/util/HashMap/containsKey(Ljava/lang/Object;)Z
ifeq L12
aload 4
aload 5
ldc "channel"
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/String
putfield com/teamspeak/ts3client/data/ab/g Ljava/lang/String;
L12:
aload 5
ldc "channelpassword"
invokevirtual java/util/HashMap/containsKey(Ljava/lang/Object;)Z
ifeq L13
aload 4
aload 5
ldc "channelpassword"
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/String
putfield com/teamspeak/ts3client/data/ab/h Ljava/lang/String;
L13:
aload 5
ldc "addbookmark"
invokevirtual java/util/HashMap/containsKey(Ljava/lang/Object;)Z
ifeq L14
aload 4
aload 5
ldc "addbookmark"
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/String
putfield com/teamspeak/ts3client/data/ab/a Ljava/lang/String;
L14:
aload 5
ldc "token"
invokevirtual java/util/HashMap/containsKey(Ljava/lang/Object;)Z
ifeq L21
aload 4
aload 5
ldc "token"
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/String
putfield com/teamspeak/ts3client/data/ab/i Ljava/lang/String;
L15:
goto L21
L16:
aload 5
aload 6
iconst_0
aaload
ldc ""
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
L17:
goto L8
L23:
iload 2
iconst_1
iadd
istore 2
goto L22
.limit locals 8
.limit stack 4
.end method

.method protected onCreate(Landroid/os/Bundle;)V
.catch java/io/UnsupportedEncodingException from L0 to L1 using L2
.catch java/io/UnsupportedEncodingException from L3 to L4 using L2
.catch java/io/UnsupportedEncodingException from L5 to L6 using L2
.catch java/io/UnsupportedEncodingException from L7 to L8 using L2
.catch java/io/UnsupportedEncodingException from L8 to L9 using L2
.catch java/io/UnsupportedEncodingException from L9 to L10 using L2
.catch java/io/UnsupportedEncodingException from L10 to L11 using L2
.catch java/io/UnsupportedEncodingException from L11 to L12 using L2
.catch java/io/UnsupportedEncodingException from L12 to L13 using L2
.catch java/io/UnsupportedEncodingException from L13 to L14 using L2
.catch java/io/UnsupportedEncodingException from L14 to L15 using L2
.catch java/io/UnsupportedEncodingException from L16 to L17 using L2
aload 0
aload 1
invokespecial android/app/Activity/onCreate(Landroid/os/Bundle;)V
aload 0
invokevirtual com/teamspeak/ts3client/OpenBrowserIntent/getIntent()Landroid/content/Intent;
invokevirtual android/content/Intent/getData()Landroid/net/Uri;
astore 1
aload 1
ifnull L18
aload 0
ldc "activity"
invokevirtual com/teamspeak/ts3client/OpenBrowserIntent/getSystemService(Ljava/lang/String;)Ljava/lang/Object;
checkcast android/app/ActivityManager
ldc_w 2147483647
invokevirtual android/app/ActivityManager/getRunningServices(I)Ljava/util/List;
invokeinterface java/util/List/iterator()Ljava/util/Iterator; 0
astore 4
L19:
aload 4
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L20
ldc "com.teamspeak.ts3client.ConnectionBackground"
aload 4
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast android/app/ActivityManager$RunningServiceInfo
getfield android/app/ActivityManager$RunningServiceInfo/service Landroid/content/ComponentName;
invokevirtual android/content/ComponentName/getClassName()Ljava/lang/String;
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L19
aload 0
iconst_1
putfield com/teamspeak/ts3client/OpenBrowserIntent/a Z
goto L19
L20:
new com/teamspeak/ts3client/data/ab
dup
invokespecial com/teamspeak/ts3client/data/ab/<init>()V
astore 4
aload 1
ifnull L18
aload 1
invokevirtual android/net/Uri/getQuery()Ljava/lang/String;
ifnonnull L0
aload 4
aload 1
invokevirtual android/net/Uri/getHost()Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/data/ab/a(Ljava/lang/String;)V
L21:
new android/content/Intent
dup
aload 0
invokevirtual com/teamspeak/ts3client/OpenBrowserIntent/getApplicationContext()Landroid/content/Context;
ldc com/teamspeak/ts3client/StartGUIFragment
invokespecial android/content/Intent/<init>(Landroid/content/Context;Ljava/lang/Class;)V
astore 5
aload 5
ldc "UrlStart"
iconst_1
invokevirtual android/content/Intent/putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
pop
aload 5
ldc "uri"
aload 1
invokevirtual android/content/Intent/putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;
pop
aload 5
ldc "address"
aload 4
getfield com/teamspeak/ts3client/data/ab/c Ljava/lang/String;
invokevirtual android/content/Intent/putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
pop
aload 0
aload 5
invokevirtual com/teamspeak/ts3client/OpenBrowserIntent/startActivity(Landroid/content/Intent;)V
aload 0
invokevirtual com/teamspeak/ts3client/OpenBrowserIntent/finish()V
L18:
return
L0:
aload 1
invokevirtual android/net/Uri/getQuery()Ljava/lang/String;
ldc "UTF-8"
invokestatic java/net/URLDecoder/decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
astore 6
new java/util/HashMap
dup
invokespecial java/util/HashMap/<init>()V
astore 5
aload 6
ldc "&"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L7
aload 6
ldc "&"
invokevirtual java/lang/String/split(Ljava/lang/String;)[Ljava/lang/String;
astore 6
aload 6
arraylength
istore 3
L1:
iconst_0
istore 2
L22:
iload 2
iload 3
if_icmpge L8
L3:
aload 6
iload 2
aaload
ldc "="
invokevirtual java/lang/String/split(Ljava/lang/String;)[Ljava/lang/String;
astore 7
aload 7
arraylength
iconst_1
if_icmple L5
aload 5
aload 7
iconst_0
aaload
aload 7
iconst_1
aaload
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
L4:
goto L23
L5:
aload 5
aload 7
iconst_0
aaload
ldc ""
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
L6:
goto L23
L2:
astore 5
aload 5
invokevirtual java/io/UnsupportedEncodingException/printStackTrace()V
goto L21
L7:
aload 6
ldc "="
invokevirtual java/lang/String/split(Ljava/lang/String;)[Ljava/lang/String;
astore 6
aload 6
arraylength
iconst_1
if_icmple L16
aload 5
aload 6
iconst_0
aaload
aload 6
iconst_1
aaload
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
L8:
aload 4
aload 1
invokevirtual android/net/Uri/getHost()Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/data/ab/a(Ljava/lang/String;)V
aload 5
ldc "port"
invokevirtual java/util/HashMap/containsKey(Ljava/lang/Object;)Z
ifeq L9
aload 4
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 4
getfield com/teamspeak/ts3client/data/ab/c Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ":"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 5
ldc "port"
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/String
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/data/ab/a(Ljava/lang/String;)V
L9:
aload 5
ldc "nickname"
invokevirtual java/util/HashMap/containsKey(Ljava/lang/Object;)Z
ifeq L10
aload 4
aload 5
ldc "nickname"
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/String
putfield com/teamspeak/ts3client/data/ab/f Ljava/lang/String;
L10:
aload 5
ldc "password"
invokevirtual java/util/HashMap/containsKey(Ljava/lang/Object;)Z
ifeq L11
aload 4
aload 5
ldc "password"
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/String
putfield com/teamspeak/ts3client/data/ab/e Ljava/lang/String;
L11:
aload 5
ldc "channel"
invokevirtual java/util/HashMap/containsKey(Ljava/lang/Object;)Z
ifeq L12
aload 4
aload 5
ldc "channel"
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/String
putfield com/teamspeak/ts3client/data/ab/g Ljava/lang/String;
L12:
aload 5
ldc "channelpassword"
invokevirtual java/util/HashMap/containsKey(Ljava/lang/Object;)Z
ifeq L13
aload 4
aload 5
ldc "channelpassword"
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/String
putfield com/teamspeak/ts3client/data/ab/h Ljava/lang/String;
L13:
aload 5
ldc "addbookmark"
invokevirtual java/util/HashMap/containsKey(Ljava/lang/Object;)Z
ifeq L14
aload 4
aload 5
ldc "addbookmark"
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/String
putfield com/teamspeak/ts3client/data/ab/a Ljava/lang/String;
L14:
aload 5
ldc "token"
invokevirtual java/util/HashMap/containsKey(Ljava/lang/Object;)Z
ifeq L21
aload 4
aload 5
ldc "token"
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/String
putfield com/teamspeak/ts3client/data/ab/i Ljava/lang/String;
L15:
goto L21
L16:
aload 5
aload 6
iconst_0
aaload
ldc ""
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
L17:
goto L8
L23:
iload 2
iconst_1
iadd
istore 2
goto L22
.limit locals 8
.limit stack 4
.end method
