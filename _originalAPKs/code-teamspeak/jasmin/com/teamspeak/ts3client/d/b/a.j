.bytecode 50.0
.class public final synchronized com/teamspeak/ts3client/d/b/a
.super android/support/v4/app/ax

.field private 'aA' Landroid/widget/Button;

.field private 'aB' Landroid/widget/Button;

.field private 'aC' Landroid/widget/Button;

.field private 'aD' Landroid/widget/LinearLayout;

.field private 'aE' Z

.field private 'aF' Z

.field private 'at' Lcom/teamspeak/ts3client/data/c;

.field private 'au' Landroid/widget/Button;

.field private 'av' Landroid/widget/Button;

.field private 'aw' Landroid/widget/Button;

.field private 'ax' Landroid/widget/Button;

.field private 'ay' Landroid/widget/Button;

.field private 'az' Landroid/widget/Button;

.method public <init>(Lcom/teamspeak/ts3client/data/c;Z)V
aload 0
invokespecial android/support/v4/app/ax/<init>()V
aload 0
iconst_0
putfield com/teamspeak/ts3client/d/b/a/aF Z
aload 0
aload 1
putfield com/teamspeak/ts3client/d/b/a/at Lcom/teamspeak/ts3client/data/c;
aload 1
getfield com/teamspeak/ts3client/data/c/c I
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/h I
if_icmpne L0
aload 0
iconst_1
putfield com/teamspeak/ts3client/d/b/a/aF Z
L0:
aload 0
iload 2
putfield com/teamspeak/ts3client/d/b/a/aE Z
return
.limit locals 3
.limit stack 2
.end method

.method private a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Lcom/teamspeak/ts3client/Ts3Application;)Landroid/view/View;
aload 1
ldc_w 2130903084
aload 2
iconst_0
invokevirtual android/view/LayoutInflater/inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;
checkcast android/widget/LinearLayout
astore 1
aload 0
getfield android/support/v4/app/ax/j Landroid/app/Dialog;
aload 0
getfield com/teamspeak/ts3client/d/b/a/at Lcom/teamspeak/ts3client/data/c;
getfield com/teamspeak/ts3client/data/c/a Ljava/lang/String;
invokevirtual android/app/Dialog/setTitle(Ljava/lang/CharSequence;)V
aload 0
aload 1
ldc_w 2131493175
invokevirtual android/widget/LinearLayout/findViewById(I)Landroid/view/View;
checkcast android/widget/Button
putfield com/teamspeak/ts3client/d/b/a/aA Landroid/widget/Button;
ldc "dialog.client.contact.text"
aload 1
ldc_w 2131493175
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;Landroid/view/ViewGroup;I)V
aload 0
getfield com/teamspeak/ts3client/d/b/a/aA Landroid/widget/Button;
new com/teamspeak/ts3client/d/b/y
dup
aload 0
invokespecial com/teamspeak/ts3client/d/b/y/<init>(Lcom/teamspeak/ts3client/d/b/a;)V
invokevirtual android/widget/Button/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 0
aload 1
ldc_w 2131493185
invokevirtual android/widget/LinearLayout/findViewById(I)Landroid/view/View;
checkcast android/widget/Button
putfield com/teamspeak/ts3client/d/b/a/au Landroid/widget/Button;
ldc "dialog.client.kickserver.text"
aload 1
ldc_w 2131493185
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;Landroid/view/ViewGroup;I)V
aload 0
getfield com/teamspeak/ts3client/d/b/a/au Landroid/widget/Button;
new com/teamspeak/ts3client/d/b/z
dup
aload 0
invokespecial com/teamspeak/ts3client/d/b/z/<init>(Lcom/teamspeak/ts3client/d/b/a;)V
invokevirtual android/widget/Button/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 0
aload 1
ldc_w 2131493184
invokevirtual android/widget/LinearLayout/findViewById(I)Landroid/view/View;
checkcast android/widget/Button
putfield com/teamspeak/ts3client/d/b/a/av Landroid/widget/Button;
ldc "dialog.client.kickchannel.text"
aload 1
ldc_w 2131493184
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;Landroid/view/ViewGroup;I)V
aload 0
getfield com/teamspeak/ts3client/d/b/a/av Landroid/widget/Button;
new com/teamspeak/ts3client/d/b/aa
dup
aload 0
invokespecial com/teamspeak/ts3client/d/b/aa/<init>(Lcom/teamspeak/ts3client/d/b/a;)V
invokevirtual android/widget/Button/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 0
aload 1
ldc_w 2131493176
invokevirtual android/widget/LinearLayout/findViewById(I)Landroid/view/View;
checkcast android/widget/Button
putfield com/teamspeak/ts3client/d/b/a/aw Landroid/widget/Button;
ldc "dialog.client.poke.text"
aload 1
ldc_w 2131493176
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;Landroid/view/ViewGroup;I)V
aload 0
getfield com/teamspeak/ts3client/d/b/a/aw Landroid/widget/Button;
new com/teamspeak/ts3client/d/b/c
dup
aload 0
invokespecial com/teamspeak/ts3client/d/b/c/<init>(Lcom/teamspeak/ts3client/d/b/a;)V
invokevirtual android/widget/Button/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 0
aload 1
ldc_w 2131493182
invokevirtual android/widget/LinearLayout/findViewById(I)Landroid/view/View;
checkcast android/widget/Button
putfield com/teamspeak/ts3client/d/b/a/aB Landroid/widget/Button;
ldc "dialog.client.priorityspeaker.text"
aload 1
ldc_w 2131493182
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;Landroid/view/ViewGroup;I)V
aload 0
getfield com/teamspeak/ts3client/d/b/a/aB Landroid/widget/Button;
new com/teamspeak/ts3client/d/b/d
dup
aload 0
aload 3
invokespecial com/teamspeak/ts3client/d/b/d/<init>(Lcom/teamspeak/ts3client/d/b/a;Lcom/teamspeak/ts3client/Ts3Application;)V
invokevirtual android/widget/Button/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 0
aload 1
ldc_w 2131493181
invokevirtual android/widget/LinearLayout/findViewById(I)Landroid/view/View;
checkcast android/widget/Button
putfield com/teamspeak/ts3client/d/b/a/aC Landroid/widget/Button;
ldc "dialog.client.volumemodifier.text"
aload 1
ldc_w 2131493181
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;Landroid/view/ViewGroup;I)V
aload 0
getfield com/teamspeak/ts3client/d/b/a/aC Landroid/widget/Button;
new com/teamspeak/ts3client/d/b/e
dup
aload 0
aload 3
invokespecial com/teamspeak/ts3client/d/b/e/<init>(Lcom/teamspeak/ts3client/d/b/a;Lcom/teamspeak/ts3client/Ts3Application;)V
invokevirtual android/widget/Button/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 0
aload 1
ldc_w 2131493178
invokevirtual android/widget/LinearLayout/findViewById(I)Landroid/view/View;
checkcast android/widget/Button
putfield com/teamspeak/ts3client/d/b/a/ax Landroid/widget/Button;
ldc "dialog.client.complain.text"
aload 1
ldc_w 2131493178
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;Landroid/view/ViewGroup;I)V
aload 0
getfield com/teamspeak/ts3client/d/b/a/ax Landroid/widget/Button;
new com/teamspeak/ts3client/d/b/h
dup
aload 0
invokespecial com/teamspeak/ts3client/d/b/h/<init>(Lcom/teamspeak/ts3client/d/b/a;)V
invokevirtual android/widget/Button/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 0
aload 1
ldc_w 2131493183
invokevirtual android/widget/LinearLayout/findViewById(I)Landroid/view/View;
checkcast android/widget/Button
putfield com/teamspeak/ts3client/d/b/a/ay Landroid/widget/Button;
ldc "dialog.client.ban.text"
aload 1
ldc_w 2131493183
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;Landroid/view/ViewGroup;I)V
aload 0
getfield com/teamspeak/ts3client/d/b/a/ay Landroid/widget/Button;
new com/teamspeak/ts3client/d/b/i
dup
aload 0
invokespecial com/teamspeak/ts3client/d/b/i/<init>(Lcom/teamspeak/ts3client/d/b/a;)V
invokevirtual android/widget/Button/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 0
aload 1
ldc_w 2131493179
invokevirtual android/widget/LinearLayout/findViewById(I)Landroid/view/View;
checkcast android/widget/Button
putfield com/teamspeak/ts3client/d/b/a/az Landroid/widget/Button;
ldc "dialog.client.mute.text1"
aload 1
ldc_w 2131493179
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;Landroid/view/ViewGroup;I)V
aload 3
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 3
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
aload 0
getfield com/teamspeak/ts3client/d/b/a/at Lcom/teamspeak/ts3client/data/c;
getfield com/teamspeak/ts3client/data/c/c I
getstatic com/teamspeak/ts3client/jni/d/q Lcom/teamspeak/ts3client/jni/d;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/a(JILcom/teamspeak/ts3client/jni/d;)I
iconst_1
if_icmpne L0
ldc "dialog.client.mute.text2"
aload 1
ldc_w 2131493179
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;Landroid/view/ViewGroup;I)V
L0:
aload 0
getfield com/teamspeak/ts3client/d/b/a/az Landroid/widget/Button;
new com/teamspeak/ts3client/d/b/j
dup
aload 0
aload 3
invokespecial com/teamspeak/ts3client/d/b/j/<init>(Lcom/teamspeak/ts3client/d/b/a;Lcom/teamspeak/ts3client/Ts3Application;)V
invokevirtual android/widget/Button/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 0
aload 1
ldc_w 2131493182
invokevirtual android/widget/LinearLayout/findViewById(I)Landroid/view/View;
checkcast android/widget/Button
putfield com/teamspeak/ts3client/d/b/a/aB Landroid/widget/Button;
ldc "dialog.client.priorityspeaker.text"
aload 1
ldc_w 2131493182
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;Landroid/view/ViewGroup;I)V
aload 0
getfield com/teamspeak/ts3client/d/b/a/aB Landroid/widget/Button;
new com/teamspeak/ts3client/d/b/k
dup
aload 0
aload 3
invokespecial com/teamspeak/ts3client/d/b/k/<init>(Lcom/teamspeak/ts3client/d/b/a;Lcom/teamspeak/ts3client/Ts3Application;)V
invokevirtual android/widget/Button/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 0
aload 1
ldc_w 2131493180
invokevirtual android/widget/LinearLayout/findViewById(I)Landroid/view/View;
checkcast android/widget/LinearLayout
putfield com/teamspeak/ts3client/d/b/a/aD Landroid/widget/LinearLayout;
aload 0
getfield com/teamspeak/ts3client/d/b/a/aE Z
ifeq L1
new android/widget/Button
dup
aload 0
invokevirtual com/teamspeak/ts3client/d/b/a/i()Landroid/support/v4/app/bb;
invokespecial android/widget/Button/<init>(Landroid/content/Context;)V
astore 2
aload 2
ldc "dialog.client.info.text"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/widget/Button/setText(Ljava/lang/CharSequence;)V
aload 2
new com/teamspeak/ts3client/d/b/l
dup
aload 0
aload 3
invokespecial com/teamspeak/ts3client/d/b/l/<init>(Lcom/teamspeak/ts3client/d/b/a;Lcom/teamspeak/ts3client/Ts3Application;)V
invokevirtual android/widget/Button/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 0
getfield com/teamspeak/ts3client/d/b/a/aD Landroid/widget/LinearLayout;
aload 2
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
L1:
new android/widget/Button
dup
aload 1
invokevirtual android/widget/LinearLayout/getContext()Landroid/content/Context;
invokespecial android/widget/Button/<init>(Landroid/content/Context;)V
astore 2
aload 2
ldc "dialog.client.move.text"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/widget/Button/setText(Ljava/lang/CharSequence;)V
aload 2
new com/teamspeak/ts3client/d/b/m
dup
aload 0
aload 3
invokespecial com/teamspeak/ts3client/d/b/m/<init>(Lcom/teamspeak/ts3client/d/b/a;Lcom/teamspeak/ts3client/Ts3Application;)V
invokevirtual android/widget/Button/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 0
getfield com/teamspeak/ts3client/d/b/a/aD Landroid/widget/LinearLayout;
aload 2
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
new android/widget/Button
dup
aload 1
invokevirtual android/widget/LinearLayout/getContext()Landroid/content/Context;
invokespecial android/widget/Button/<init>(Landroid/content/Context;)V
astore 2
aload 2
ldc "dialog.client.chat.text"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/widget/Button/setText(Ljava/lang/CharSequence;)V
aload 2
new com/teamspeak/ts3client/d/b/o
dup
aload 0
aload 3
invokespecial com/teamspeak/ts3client/d/b/o/<init>(Lcom/teamspeak/ts3client/d/b/a;Lcom/teamspeak/ts3client/Ts3Application;)V
invokevirtual android/widget/Button/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 0
getfield com/teamspeak/ts3client/d/b/a/aD Landroid/widget/LinearLayout;
aload 2
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
aload 3
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/i Ljava/util/HashMap;
aload 0
getfield com/teamspeak/ts3client/d/b/a/at Lcom/teamspeak/ts3client/data/c;
getfield com/teamspeak/ts3client/data/c/c I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
ifnull L2
aload 3
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/s Lcom/teamspeak/ts3client/data/f/a;
getstatic com/teamspeak/ts3client/jni/g/do Lcom/teamspeak/ts3client/jni/g;
invokevirtual com/teamspeak/ts3client/data/f/a/a(Lcom/teamspeak/ts3client/jni/g;)Z
istore 4
aload 3
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 3
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/i Ljava/util/HashMap;
aload 0
getfield com/teamspeak/ts3client/d/b/a/at Lcom/teamspeak/ts3client/data/c;
getfield com/teamspeak/ts3client/data/c/c I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/Long
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
astore 2
aload 0
getfield com/teamspeak/ts3client/d/b/a/at Lcom/teamspeak/ts3client/data/c;
getfield com/teamspeak/ts3client/data/c/p Ljava/lang/String;
ldc "0"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L3
aload 2
getfield com/teamspeak/ts3client/data/a/h I
ifle L4
aload 0
getfield com/teamspeak/ts3client/d/b/a/at Lcom/teamspeak/ts3client/data/c;
getfield com/teamspeak/ts3client/data/c/o I
aload 2
getfield com/teamspeak/ts3client/data/a/h I
if_icmpge L4
aload 0
getfield com/teamspeak/ts3client/d/b/a/at Lcom/teamspeak/ts3client/data/c;
getfield com/teamspeak/ts3client/data/c/r Z
ifne L4
L3:
new android/widget/Button
dup
aload 1
invokevirtual android/widget/LinearLayout/getContext()Landroid/content/Context;
invokespecial android/widget/Button/<init>(Landroid/content/Context;)V
astore 2
aload 2
ldc "dialog.client.talkpower.text1"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/widget/Button/setText(Ljava/lang/CharSequence;)V
aload 2
new com/teamspeak/ts3client/d/b/q
dup
aload 0
aload 3
invokespecial com/teamspeak/ts3client/d/b/q/<init>(Lcom/teamspeak/ts3client/d/b/a;Lcom/teamspeak/ts3client/Ts3Application;)V
invokevirtual android/widget/Button/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 2
iload 4
invokevirtual android/widget/Button/setEnabled(Z)V
aload 0
getfield com/teamspeak/ts3client/d/b/a/aD Landroid/widget/LinearLayout;
aload 2
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
L5:
new android/widget/Button
dup
aload 1
invokevirtual android/widget/LinearLayout/getContext()Landroid/content/Context;
invokespecial android/widget/Button/<init>(Landroid/content/Context;)V
astore 2
aload 2
ldc "dialog.client.group.text"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/widget/Button/setText(Ljava/lang/CharSequence;)V
aload 2
new com/teamspeak/ts3client/d/b/s
dup
aload 0
invokespecial com/teamspeak/ts3client/d/b/s/<init>(Lcom/teamspeak/ts3client/d/b/a;)V
invokevirtual android/widget/Button/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 0
getfield com/teamspeak/ts3client/d/b/a/aD Landroid/widget/LinearLayout;
aload 2
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
new android/widget/Button
dup
aload 1
invokevirtual android/widget/LinearLayout/getContext()Landroid/content/Context;
invokespecial android/widget/Button/<init>(Landroid/content/Context;)V
astore 2
aload 2
ldc "dialog.client.cinfo.text"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/widget/Button/setText(Ljava/lang/CharSequence;)V
aload 2
new com/teamspeak/ts3client/d/b/t
dup
aload 0
invokespecial com/teamspeak/ts3client/d/b/t/<init>(Lcom/teamspeak/ts3client/d/b/a;)V
invokevirtual android/widget/Button/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 0
getfield com/teamspeak/ts3client/d/b/a/aD Landroid/widget/LinearLayout;
aload 2
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
aload 0
getfield com/teamspeak/ts3client/d/b/a/at Lcom/teamspeak/ts3client/data/c;
getfield com/teamspeak/ts3client/data/c/v I
ifeq L2
aload 0
getfield com/teamspeak/ts3client/d/b/a/ax Landroid/widget/Button;
iconst_0
invokevirtual android/widget/Button/setEnabled(Z)V
aload 0
getfield com/teamspeak/ts3client/d/b/a/ay Landroid/widget/Button;
iconst_0
invokevirtual android/widget/Button/setEnabled(Z)V
aload 0
getfield com/teamspeak/ts3client/d/b/a/aA Landroid/widget/Button;
iconst_0
invokevirtual android/widget/Button/setEnabled(Z)V
aload 0
getfield com/teamspeak/ts3client/d/b/a/av Landroid/widget/Button;
iconst_0
invokevirtual android/widget/Button/setEnabled(Z)V
aload 0
getfield com/teamspeak/ts3client/d/b/a/aD Landroid/widget/LinearLayout;
iconst_0
invokevirtual android/widget/LinearLayout/setEnabled(Z)V
aload 0
getfield com/teamspeak/ts3client/d/b/a/az Landroid/widget/Button;
iconst_0
invokevirtual android/widget/Button/setEnabled(Z)V
aload 0
getfield com/teamspeak/ts3client/d/b/a/aw Landroid/widget/Button;
iconst_0
invokevirtual android/widget/Button/setEnabled(Z)V
aload 0
getfield com/teamspeak/ts3client/d/b/a/aB Landroid/widget/Button;
iconst_0
invokevirtual android/widget/Button/setEnabled(Z)V
aload 0
getfield com/teamspeak/ts3client/d/b/a/au Landroid/widget/Button;
iconst_0
invokevirtual android/widget/Button/setEnabled(Z)V
L2:
aload 1
areturn
L4:
aload 0
getfield com/teamspeak/ts3client/d/b/a/at Lcom/teamspeak/ts3client/data/c;
getfield com/teamspeak/ts3client/data/c/r Z
ifeq L5
new android/widget/Button
dup
aload 1
invokevirtual android/widget/LinearLayout/getContext()Landroid/content/Context;
invokespecial android/widget/Button/<init>(Landroid/content/Context;)V
astore 2
aload 2
ldc "dialog.client.talkpower.text2"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/widget/Button/setText(Ljava/lang/CharSequence;)V
aload 2
new com/teamspeak/ts3client/d/b/r
dup
aload 0
aload 3
invokespecial com/teamspeak/ts3client/d/b/r/<init>(Lcom/teamspeak/ts3client/d/b/a;Lcom/teamspeak/ts3client/Ts3Application;)V
invokevirtual android/widget/Button/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 2
iload 4
invokevirtual android/widget/Button/setEnabled(Z)V
aload 0
getfield com/teamspeak/ts3client/d/b/a/aD Landroid/widget/LinearLayout;
aload 2
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
goto L5
.limit locals 5
.limit stack 5
.end method

.method private a(Lcom/teamspeak/ts3client/Ts3Application;)Landroid/view/View;
new android/widget/LinearLayout
dup
aload 0
invokevirtual com/teamspeak/ts3client/d/b/a/i()Landroid/support/v4/app/bb;
invokespecial android/widget/LinearLayout/<init>(Landroid/content/Context;)V
astore 3
aload 0
getfield android/support/v4/app/ax/j Landroid/app/Dialog;
aload 0
getfield com/teamspeak/ts3client/d/b/a/at Lcom/teamspeak/ts3client/data/c;
getfield com/teamspeak/ts3client/data/c/a Ljava/lang/String;
invokevirtual android/app/Dialog/setTitle(Ljava/lang/CharSequence;)V
aload 3
iconst_1
invokevirtual android/widget/LinearLayout/setOrientation(I)V
aload 3
new android/view/ViewGroup$LayoutParams
dup
iconst_m1
bipush -2
invokespecial android/view/ViewGroup$LayoutParams/<init>(II)V
invokevirtual android/widget/LinearLayout/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
new android/widget/Button
dup
aload 3
invokevirtual android/widget/LinearLayout/getContext()Landroid/content/Context;
invokespecial android/widget/Button/<init>(Landroid/content/Context;)V
astore 4
aload 4
ldc "dialog.client.priorityspeaker.text"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/widget/Button/setText(Ljava/lang/CharSequence;)V
aload 4
new com/teamspeak/ts3client/d/b/b
dup
aload 0
aload 1
invokespecial com/teamspeak/ts3client/d/b/b/<init>(Lcom/teamspeak/ts3client/d/b/a;Lcom/teamspeak/ts3client/Ts3Application;)V
invokevirtual android/widget/Button/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 3
aload 4
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
aload 0
getfield com/teamspeak/ts3client/d/b/a/aE Z
ifeq L0
new android/widget/Button
dup
aload 0
invokevirtual com/teamspeak/ts3client/d/b/a/i()Landroid/support/v4/app/bb;
invokespecial android/widget/Button/<init>(Landroid/content/Context;)V
astore 4
aload 4
ldc "dialog.client.info.text"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/widget/Button/setText(Ljava/lang/CharSequence;)V
aload 4
new com/teamspeak/ts3client/d/b/p
dup
aload 0
aload 1
invokespecial com/teamspeak/ts3client/d/b/p/<init>(Lcom/teamspeak/ts3client/d/b/a;Lcom/teamspeak/ts3client/Ts3Application;)V
invokevirtual android/widget/Button/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 3
aload 4
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
L0:
aload 1
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/s Lcom/teamspeak/ts3client/data/f/a;
getstatic com/teamspeak/ts3client/jni/g/do Lcom/teamspeak/ts3client/jni/g;
invokevirtual com/teamspeak/ts3client/data/f/a/a(Lcom/teamspeak/ts3client/jni/g;)Z
istore 2
aload 1
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/i Ljava/util/HashMap;
aload 0
getfield com/teamspeak/ts3client/d/b/a/at Lcom/teamspeak/ts3client/data/c;
getfield com/teamspeak/ts3client/data/c/c I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
ifnull L1
aload 1
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 1
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/i Ljava/util/HashMap;
aload 0
getfield com/teamspeak/ts3client/d/b/a/at Lcom/teamspeak/ts3client/data/c;
getfield com/teamspeak/ts3client/data/c/c I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/Long
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
astore 4
aload 0
getfield com/teamspeak/ts3client/d/b/a/at Lcom/teamspeak/ts3client/data/c;
getfield com/teamspeak/ts3client/data/c/p Ljava/lang/String;
ldc "0"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L2
aload 4
getfield com/teamspeak/ts3client/data/a/h I
ifle L3
aload 0
getfield com/teamspeak/ts3client/d/b/a/at Lcom/teamspeak/ts3client/data/c;
getfield com/teamspeak/ts3client/data/c/o I
aload 4
getfield com/teamspeak/ts3client/data/a/h I
if_icmpge L3
aload 0
getfield com/teamspeak/ts3client/d/b/a/at Lcom/teamspeak/ts3client/data/c;
getfield com/teamspeak/ts3client/data/c/r Z
ifne L3
L2:
new android/widget/Button
dup
aload 3
invokevirtual android/widget/LinearLayout/getContext()Landroid/content/Context;
invokespecial android/widget/Button/<init>(Landroid/content/Context;)V
astore 4
aload 4
ldc "dialog.client.talkpower.text1"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/widget/Button/setText(Ljava/lang/CharSequence;)V
aload 4
new com/teamspeak/ts3client/d/b/u
dup
aload 0
aload 1
invokespecial com/teamspeak/ts3client/d/b/u/<init>(Lcom/teamspeak/ts3client/d/b/a;Lcom/teamspeak/ts3client/Ts3Application;)V
invokevirtual android/widget/Button/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 4
iload 2
invokevirtual android/widget/Button/setEnabled(Z)V
aload 3
aload 4
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
L1:
new android/widget/Button
dup
aload 3
invokevirtual android/widget/LinearLayout/getContext()Landroid/content/Context;
invokespecial android/widget/Button/<init>(Landroid/content/Context;)V
astore 1
aload 1
ldc "dialog.client.group.text"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/widget/Button/setText(Ljava/lang/CharSequence;)V
aload 1
new com/teamspeak/ts3client/d/b/w
dup
aload 0
invokespecial com/teamspeak/ts3client/d/b/w/<init>(Lcom/teamspeak/ts3client/d/b/a;)V
invokevirtual android/widget/Button/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 3
aload 1
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
new android/widget/Button
dup
aload 3
invokevirtual android/widget/LinearLayout/getContext()Landroid/content/Context;
invokespecial android/widget/Button/<init>(Landroid/content/Context;)V
astore 1
aload 1
ldc "dialog.client.cinfo.text"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/widget/Button/setText(Ljava/lang/CharSequence;)V
aload 1
new com/teamspeak/ts3client/d/b/x
dup
aload 0
invokespecial com/teamspeak/ts3client/d/b/x/<init>(Lcom/teamspeak/ts3client/d/b/a;)V
invokevirtual android/widget/Button/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 3
aload 1
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
aload 3
areturn
L3:
aload 0
getfield com/teamspeak/ts3client/d/b/a/at Lcom/teamspeak/ts3client/data/c;
getfield com/teamspeak/ts3client/data/c/r Z
ifeq L1
new android/widget/Button
dup
aload 3
invokevirtual android/widget/LinearLayout/getContext()Landroid/content/Context;
invokespecial android/widget/Button/<init>(Landroid/content/Context;)V
astore 4
aload 4
ldc "dialog.client.talkpower.text2"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/widget/Button/setText(Ljava/lang/CharSequence;)V
aload 4
new com/teamspeak/ts3client/d/b/v
dup
aload 0
aload 1
invokespecial com/teamspeak/ts3client/d/b/v/<init>(Lcom/teamspeak/ts3client/d/b/a;Lcom/teamspeak/ts3client/Ts3Application;)V
invokevirtual android/widget/Button/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 4
iload 2
invokevirtual android/widget/Button/setEnabled(Z)V
aload 3
aload 4
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
goto L1
.limit locals 5
.limit stack 5
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/d/b/a;)Lcom/teamspeak/ts3client/data/c;
aload 0
getfield com/teamspeak/ts3client/d/b/a/at Lcom/teamspeak/ts3client/data/c;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
aload 1
invokevirtual android/view/LayoutInflater/getContext()Landroid/content/Context;
invokevirtual android/content/Context/getApplicationContext()Landroid/content/Context;
checkcast com/teamspeak/ts3client/Ts3Application
astore 4
aload 0
getfield com/teamspeak/ts3client/d/b/a/aF Z
ifne L0
aload 1
ldc_w 2130903084
aload 2
iconst_0
invokevirtual android/view/LayoutInflater/inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;
checkcast android/widget/LinearLayout
astore 1
aload 0
getfield android/support/v4/app/ax/j Landroid/app/Dialog;
aload 0
getfield com/teamspeak/ts3client/d/b/a/at Lcom/teamspeak/ts3client/data/c;
getfield com/teamspeak/ts3client/data/c/a Ljava/lang/String;
invokevirtual android/app/Dialog/setTitle(Ljava/lang/CharSequence;)V
aload 0
aload 1
ldc_w 2131493175
invokevirtual android/widget/LinearLayout/findViewById(I)Landroid/view/View;
checkcast android/widget/Button
putfield com/teamspeak/ts3client/d/b/a/aA Landroid/widget/Button;
ldc "dialog.client.contact.text"
aload 1
ldc_w 2131493175
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;Landroid/view/ViewGroup;I)V
aload 0
getfield com/teamspeak/ts3client/d/b/a/aA Landroid/widget/Button;
new com/teamspeak/ts3client/d/b/y
dup
aload 0
invokespecial com/teamspeak/ts3client/d/b/y/<init>(Lcom/teamspeak/ts3client/d/b/a;)V
invokevirtual android/widget/Button/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 0
aload 1
ldc_w 2131493185
invokevirtual android/widget/LinearLayout/findViewById(I)Landroid/view/View;
checkcast android/widget/Button
putfield com/teamspeak/ts3client/d/b/a/au Landroid/widget/Button;
ldc "dialog.client.kickserver.text"
aload 1
ldc_w 2131493185
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;Landroid/view/ViewGroup;I)V
aload 0
getfield com/teamspeak/ts3client/d/b/a/au Landroid/widget/Button;
new com/teamspeak/ts3client/d/b/z
dup
aload 0
invokespecial com/teamspeak/ts3client/d/b/z/<init>(Lcom/teamspeak/ts3client/d/b/a;)V
invokevirtual android/widget/Button/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 0
aload 1
ldc_w 2131493184
invokevirtual android/widget/LinearLayout/findViewById(I)Landroid/view/View;
checkcast android/widget/Button
putfield com/teamspeak/ts3client/d/b/a/av Landroid/widget/Button;
ldc "dialog.client.kickchannel.text"
aload 1
ldc_w 2131493184
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;Landroid/view/ViewGroup;I)V
aload 0
getfield com/teamspeak/ts3client/d/b/a/av Landroid/widget/Button;
new com/teamspeak/ts3client/d/b/aa
dup
aload 0
invokespecial com/teamspeak/ts3client/d/b/aa/<init>(Lcom/teamspeak/ts3client/d/b/a;)V
invokevirtual android/widget/Button/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 0
aload 1
ldc_w 2131493176
invokevirtual android/widget/LinearLayout/findViewById(I)Landroid/view/View;
checkcast android/widget/Button
putfield com/teamspeak/ts3client/d/b/a/aw Landroid/widget/Button;
ldc "dialog.client.poke.text"
aload 1
ldc_w 2131493176
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;Landroid/view/ViewGroup;I)V
aload 0
getfield com/teamspeak/ts3client/d/b/a/aw Landroid/widget/Button;
new com/teamspeak/ts3client/d/b/c
dup
aload 0
invokespecial com/teamspeak/ts3client/d/b/c/<init>(Lcom/teamspeak/ts3client/d/b/a;)V
invokevirtual android/widget/Button/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 0
aload 1
ldc_w 2131493182
invokevirtual android/widget/LinearLayout/findViewById(I)Landroid/view/View;
checkcast android/widget/Button
putfield com/teamspeak/ts3client/d/b/a/aB Landroid/widget/Button;
ldc "dialog.client.priorityspeaker.text"
aload 1
ldc_w 2131493182
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;Landroid/view/ViewGroup;I)V
aload 0
getfield com/teamspeak/ts3client/d/b/a/aB Landroid/widget/Button;
new com/teamspeak/ts3client/d/b/d
dup
aload 0
aload 4
invokespecial com/teamspeak/ts3client/d/b/d/<init>(Lcom/teamspeak/ts3client/d/b/a;Lcom/teamspeak/ts3client/Ts3Application;)V
invokevirtual android/widget/Button/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 0
aload 1
ldc_w 2131493181
invokevirtual android/widget/LinearLayout/findViewById(I)Landroid/view/View;
checkcast android/widget/Button
putfield com/teamspeak/ts3client/d/b/a/aC Landroid/widget/Button;
ldc "dialog.client.volumemodifier.text"
aload 1
ldc_w 2131493181
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;Landroid/view/ViewGroup;I)V
aload 0
getfield com/teamspeak/ts3client/d/b/a/aC Landroid/widget/Button;
new com/teamspeak/ts3client/d/b/e
dup
aload 0
aload 4
invokespecial com/teamspeak/ts3client/d/b/e/<init>(Lcom/teamspeak/ts3client/d/b/a;Lcom/teamspeak/ts3client/Ts3Application;)V
invokevirtual android/widget/Button/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 0
aload 1
ldc_w 2131493178
invokevirtual android/widget/LinearLayout/findViewById(I)Landroid/view/View;
checkcast android/widget/Button
putfield com/teamspeak/ts3client/d/b/a/ax Landroid/widget/Button;
ldc "dialog.client.complain.text"
aload 1
ldc_w 2131493178
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;Landroid/view/ViewGroup;I)V
aload 0
getfield com/teamspeak/ts3client/d/b/a/ax Landroid/widget/Button;
new com/teamspeak/ts3client/d/b/h
dup
aload 0
invokespecial com/teamspeak/ts3client/d/b/h/<init>(Lcom/teamspeak/ts3client/d/b/a;)V
invokevirtual android/widget/Button/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 0
aload 1
ldc_w 2131493183
invokevirtual android/widget/LinearLayout/findViewById(I)Landroid/view/View;
checkcast android/widget/Button
putfield com/teamspeak/ts3client/d/b/a/ay Landroid/widget/Button;
ldc "dialog.client.ban.text"
aload 1
ldc_w 2131493183
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;Landroid/view/ViewGroup;I)V
aload 0
getfield com/teamspeak/ts3client/d/b/a/ay Landroid/widget/Button;
new com/teamspeak/ts3client/d/b/i
dup
aload 0
invokespecial com/teamspeak/ts3client/d/b/i/<init>(Lcom/teamspeak/ts3client/d/b/a;)V
invokevirtual android/widget/Button/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 0
aload 1
ldc_w 2131493179
invokevirtual android/widget/LinearLayout/findViewById(I)Landroid/view/View;
checkcast android/widget/Button
putfield com/teamspeak/ts3client/d/b/a/az Landroid/widget/Button;
ldc "dialog.client.mute.text1"
aload 1
ldc_w 2131493179
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;Landroid/view/ViewGroup;I)V
aload 4
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 4
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
aload 0
getfield com/teamspeak/ts3client/d/b/a/at Lcom/teamspeak/ts3client/data/c;
getfield com/teamspeak/ts3client/data/c/c I
getstatic com/teamspeak/ts3client/jni/d/q Lcom/teamspeak/ts3client/jni/d;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/a(JILcom/teamspeak/ts3client/jni/d;)I
iconst_1
if_icmpne L1
ldc "dialog.client.mute.text2"
aload 1
ldc_w 2131493179
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;Landroid/view/ViewGroup;I)V
L1:
aload 0
getfield com/teamspeak/ts3client/d/b/a/az Landroid/widget/Button;
new com/teamspeak/ts3client/d/b/j
dup
aload 0
aload 4
invokespecial com/teamspeak/ts3client/d/b/j/<init>(Lcom/teamspeak/ts3client/d/b/a;Lcom/teamspeak/ts3client/Ts3Application;)V
invokevirtual android/widget/Button/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 0
aload 1
ldc_w 2131493182
invokevirtual android/widget/LinearLayout/findViewById(I)Landroid/view/View;
checkcast android/widget/Button
putfield com/teamspeak/ts3client/d/b/a/aB Landroid/widget/Button;
ldc "dialog.client.priorityspeaker.text"
aload 1
ldc_w 2131493182
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;Landroid/view/ViewGroup;I)V
aload 0
getfield com/teamspeak/ts3client/d/b/a/aB Landroid/widget/Button;
new com/teamspeak/ts3client/d/b/k
dup
aload 0
aload 4
invokespecial com/teamspeak/ts3client/d/b/k/<init>(Lcom/teamspeak/ts3client/d/b/a;Lcom/teamspeak/ts3client/Ts3Application;)V
invokevirtual android/widget/Button/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 0
aload 1
ldc_w 2131493180
invokevirtual android/widget/LinearLayout/findViewById(I)Landroid/view/View;
checkcast android/widget/LinearLayout
putfield com/teamspeak/ts3client/d/b/a/aD Landroid/widget/LinearLayout;
aload 0
getfield com/teamspeak/ts3client/d/b/a/aE Z
ifeq L2
new android/widget/Button
dup
aload 0
invokevirtual com/teamspeak/ts3client/d/b/a/i()Landroid/support/v4/app/bb;
invokespecial android/widget/Button/<init>(Landroid/content/Context;)V
astore 2
aload 2
ldc "dialog.client.info.text"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/widget/Button/setText(Ljava/lang/CharSequence;)V
aload 2
new com/teamspeak/ts3client/d/b/l
dup
aload 0
aload 4
invokespecial com/teamspeak/ts3client/d/b/l/<init>(Lcom/teamspeak/ts3client/d/b/a;Lcom/teamspeak/ts3client/Ts3Application;)V
invokevirtual android/widget/Button/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 0
getfield com/teamspeak/ts3client/d/b/a/aD Landroid/widget/LinearLayout;
aload 2
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
L2:
new android/widget/Button
dup
aload 1
invokevirtual android/widget/LinearLayout/getContext()Landroid/content/Context;
invokespecial android/widget/Button/<init>(Landroid/content/Context;)V
astore 2
aload 2
ldc "dialog.client.move.text"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/widget/Button/setText(Ljava/lang/CharSequence;)V
aload 2
new com/teamspeak/ts3client/d/b/m
dup
aload 0
aload 4
invokespecial com/teamspeak/ts3client/d/b/m/<init>(Lcom/teamspeak/ts3client/d/b/a;Lcom/teamspeak/ts3client/Ts3Application;)V
invokevirtual android/widget/Button/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 0
getfield com/teamspeak/ts3client/d/b/a/aD Landroid/widget/LinearLayout;
aload 2
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
new android/widget/Button
dup
aload 1
invokevirtual android/widget/LinearLayout/getContext()Landroid/content/Context;
invokespecial android/widget/Button/<init>(Landroid/content/Context;)V
astore 2
aload 2
ldc "dialog.client.chat.text"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/widget/Button/setText(Ljava/lang/CharSequence;)V
aload 2
new com/teamspeak/ts3client/d/b/o
dup
aload 0
aload 4
invokespecial com/teamspeak/ts3client/d/b/o/<init>(Lcom/teamspeak/ts3client/d/b/a;Lcom/teamspeak/ts3client/Ts3Application;)V
invokevirtual android/widget/Button/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 0
getfield com/teamspeak/ts3client/d/b/a/aD Landroid/widget/LinearLayout;
aload 2
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
aload 4
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/i Ljava/util/HashMap;
aload 0
getfield com/teamspeak/ts3client/d/b/a/at Lcom/teamspeak/ts3client/data/c;
getfield com/teamspeak/ts3client/data/c/c I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
ifnull L3
aload 4
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/s Lcom/teamspeak/ts3client/data/f/a;
getstatic com/teamspeak/ts3client/jni/g/do Lcom/teamspeak/ts3client/jni/g;
invokevirtual com/teamspeak/ts3client/data/f/a/a(Lcom/teamspeak/ts3client/jni/g;)Z
istore 3
aload 4
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 4
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/i Ljava/util/HashMap;
aload 0
getfield com/teamspeak/ts3client/d/b/a/at Lcom/teamspeak/ts3client/data/c;
getfield com/teamspeak/ts3client/data/c/c I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/Long
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
astore 2
aload 0
getfield com/teamspeak/ts3client/d/b/a/at Lcom/teamspeak/ts3client/data/c;
getfield com/teamspeak/ts3client/data/c/p Ljava/lang/String;
ldc "0"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L4
aload 2
getfield com/teamspeak/ts3client/data/a/h I
ifle L5
aload 0
getfield com/teamspeak/ts3client/d/b/a/at Lcom/teamspeak/ts3client/data/c;
getfield com/teamspeak/ts3client/data/c/o I
aload 2
getfield com/teamspeak/ts3client/data/a/h I
if_icmpge L5
aload 0
getfield com/teamspeak/ts3client/d/b/a/at Lcom/teamspeak/ts3client/data/c;
getfield com/teamspeak/ts3client/data/c/r Z
ifne L5
L4:
new android/widget/Button
dup
aload 1
invokevirtual android/widget/LinearLayout/getContext()Landroid/content/Context;
invokespecial android/widget/Button/<init>(Landroid/content/Context;)V
astore 2
aload 2
ldc "dialog.client.talkpower.text1"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/widget/Button/setText(Ljava/lang/CharSequence;)V
aload 2
new com/teamspeak/ts3client/d/b/q
dup
aload 0
aload 4
invokespecial com/teamspeak/ts3client/d/b/q/<init>(Lcom/teamspeak/ts3client/d/b/a;Lcom/teamspeak/ts3client/Ts3Application;)V
invokevirtual android/widget/Button/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 2
iload 3
invokevirtual android/widget/Button/setEnabled(Z)V
aload 0
getfield com/teamspeak/ts3client/d/b/a/aD Landroid/widget/LinearLayout;
aload 2
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
L6:
new android/widget/Button
dup
aload 1
invokevirtual android/widget/LinearLayout/getContext()Landroid/content/Context;
invokespecial android/widget/Button/<init>(Landroid/content/Context;)V
astore 2
aload 2
ldc "dialog.client.group.text"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/widget/Button/setText(Ljava/lang/CharSequence;)V
aload 2
new com/teamspeak/ts3client/d/b/s
dup
aload 0
invokespecial com/teamspeak/ts3client/d/b/s/<init>(Lcom/teamspeak/ts3client/d/b/a;)V
invokevirtual android/widget/Button/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 0
getfield com/teamspeak/ts3client/d/b/a/aD Landroid/widget/LinearLayout;
aload 2
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
new android/widget/Button
dup
aload 1
invokevirtual android/widget/LinearLayout/getContext()Landroid/content/Context;
invokespecial android/widget/Button/<init>(Landroid/content/Context;)V
astore 2
aload 2
ldc "dialog.client.cinfo.text"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/widget/Button/setText(Ljava/lang/CharSequence;)V
aload 2
new com/teamspeak/ts3client/d/b/t
dup
aload 0
invokespecial com/teamspeak/ts3client/d/b/t/<init>(Lcom/teamspeak/ts3client/d/b/a;)V
invokevirtual android/widget/Button/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 0
getfield com/teamspeak/ts3client/d/b/a/aD Landroid/widget/LinearLayout;
aload 2
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
aload 0
getfield com/teamspeak/ts3client/d/b/a/at Lcom/teamspeak/ts3client/data/c;
getfield com/teamspeak/ts3client/data/c/v I
ifeq L3
aload 0
getfield com/teamspeak/ts3client/d/b/a/ax Landroid/widget/Button;
iconst_0
invokevirtual android/widget/Button/setEnabled(Z)V
aload 0
getfield com/teamspeak/ts3client/d/b/a/ay Landroid/widget/Button;
iconst_0
invokevirtual android/widget/Button/setEnabled(Z)V
aload 0
getfield com/teamspeak/ts3client/d/b/a/aA Landroid/widget/Button;
iconst_0
invokevirtual android/widget/Button/setEnabled(Z)V
aload 0
getfield com/teamspeak/ts3client/d/b/a/av Landroid/widget/Button;
iconst_0
invokevirtual android/widget/Button/setEnabled(Z)V
aload 0
getfield com/teamspeak/ts3client/d/b/a/aD Landroid/widget/LinearLayout;
iconst_0
invokevirtual android/widget/LinearLayout/setEnabled(Z)V
aload 0
getfield com/teamspeak/ts3client/d/b/a/az Landroid/widget/Button;
iconst_0
invokevirtual android/widget/Button/setEnabled(Z)V
aload 0
getfield com/teamspeak/ts3client/d/b/a/aw Landroid/widget/Button;
iconst_0
invokevirtual android/widget/Button/setEnabled(Z)V
aload 0
getfield com/teamspeak/ts3client/d/b/a/aB Landroid/widget/Button;
iconst_0
invokevirtual android/widget/Button/setEnabled(Z)V
aload 0
getfield com/teamspeak/ts3client/d/b/a/au Landroid/widget/Button;
iconst_0
invokevirtual android/widget/Button/setEnabled(Z)V
L3:
aload 1
areturn
L5:
aload 0
getfield com/teamspeak/ts3client/d/b/a/at Lcom/teamspeak/ts3client/data/c;
getfield com/teamspeak/ts3client/data/c/r Z
ifeq L6
new android/widget/Button
dup
aload 1
invokevirtual android/widget/LinearLayout/getContext()Landroid/content/Context;
invokespecial android/widget/Button/<init>(Landroid/content/Context;)V
astore 2
aload 2
ldc "dialog.client.talkpower.text2"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/widget/Button/setText(Ljava/lang/CharSequence;)V
aload 2
new com/teamspeak/ts3client/d/b/r
dup
aload 0
aload 4
invokespecial com/teamspeak/ts3client/d/b/r/<init>(Lcom/teamspeak/ts3client/d/b/a;Lcom/teamspeak/ts3client/Ts3Application;)V
invokevirtual android/widget/Button/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 2
iload 3
invokevirtual android/widget/Button/setEnabled(Z)V
aload 0
getfield com/teamspeak/ts3client/d/b/a/aD Landroid/widget/LinearLayout;
aload 2
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
goto L6
L0:
new android/widget/LinearLayout
dup
aload 0
invokevirtual com/teamspeak/ts3client/d/b/a/i()Landroid/support/v4/app/bb;
invokespecial android/widget/LinearLayout/<init>(Landroid/content/Context;)V
astore 1
aload 0
getfield android/support/v4/app/ax/j Landroid/app/Dialog;
aload 0
getfield com/teamspeak/ts3client/d/b/a/at Lcom/teamspeak/ts3client/data/c;
getfield com/teamspeak/ts3client/data/c/a Ljava/lang/String;
invokevirtual android/app/Dialog/setTitle(Ljava/lang/CharSequence;)V
aload 1
iconst_1
invokevirtual android/widget/LinearLayout/setOrientation(I)V
aload 1
new android/view/ViewGroup$LayoutParams
dup
iconst_m1
bipush -2
invokespecial android/view/ViewGroup$LayoutParams/<init>(II)V
invokevirtual android/widget/LinearLayout/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
new android/widget/Button
dup
aload 1
invokevirtual android/widget/LinearLayout/getContext()Landroid/content/Context;
invokespecial android/widget/Button/<init>(Landroid/content/Context;)V
astore 2
aload 2
ldc "dialog.client.priorityspeaker.text"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/widget/Button/setText(Ljava/lang/CharSequence;)V
aload 2
new com/teamspeak/ts3client/d/b/b
dup
aload 0
aload 4
invokespecial com/teamspeak/ts3client/d/b/b/<init>(Lcom/teamspeak/ts3client/d/b/a;Lcom/teamspeak/ts3client/Ts3Application;)V
invokevirtual android/widget/Button/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 1
aload 2
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
aload 0
getfield com/teamspeak/ts3client/d/b/a/aE Z
ifeq L7
new android/widget/Button
dup
aload 0
invokevirtual com/teamspeak/ts3client/d/b/a/i()Landroid/support/v4/app/bb;
invokespecial android/widget/Button/<init>(Landroid/content/Context;)V
astore 2
aload 2
ldc "dialog.client.info.text"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/widget/Button/setText(Ljava/lang/CharSequence;)V
aload 2
new com/teamspeak/ts3client/d/b/p
dup
aload 0
aload 4
invokespecial com/teamspeak/ts3client/d/b/p/<init>(Lcom/teamspeak/ts3client/d/b/a;Lcom/teamspeak/ts3client/Ts3Application;)V
invokevirtual android/widget/Button/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 1
aload 2
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
L7:
aload 4
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/s Lcom/teamspeak/ts3client/data/f/a;
getstatic com/teamspeak/ts3client/jni/g/do Lcom/teamspeak/ts3client/jni/g;
invokevirtual com/teamspeak/ts3client/data/f/a/a(Lcom/teamspeak/ts3client/jni/g;)Z
istore 3
aload 4
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/i Ljava/util/HashMap;
aload 0
getfield com/teamspeak/ts3client/d/b/a/at Lcom/teamspeak/ts3client/data/c;
getfield com/teamspeak/ts3client/data/c/c I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
ifnull L8
aload 4
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 4
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/i Ljava/util/HashMap;
aload 0
getfield com/teamspeak/ts3client/d/b/a/at Lcom/teamspeak/ts3client/data/c;
getfield com/teamspeak/ts3client/data/c/c I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/Long
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
astore 2
aload 0
getfield com/teamspeak/ts3client/d/b/a/at Lcom/teamspeak/ts3client/data/c;
getfield com/teamspeak/ts3client/data/c/p Ljava/lang/String;
ldc "0"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L9
aload 2
getfield com/teamspeak/ts3client/data/a/h I
ifle L10
aload 0
getfield com/teamspeak/ts3client/d/b/a/at Lcom/teamspeak/ts3client/data/c;
getfield com/teamspeak/ts3client/data/c/o I
aload 2
getfield com/teamspeak/ts3client/data/a/h I
if_icmpge L10
aload 0
getfield com/teamspeak/ts3client/d/b/a/at Lcom/teamspeak/ts3client/data/c;
getfield com/teamspeak/ts3client/data/c/r Z
ifne L10
L9:
new android/widget/Button
dup
aload 1
invokevirtual android/widget/LinearLayout/getContext()Landroid/content/Context;
invokespecial android/widget/Button/<init>(Landroid/content/Context;)V
astore 2
aload 2
ldc "dialog.client.talkpower.text1"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/widget/Button/setText(Ljava/lang/CharSequence;)V
aload 2
new com/teamspeak/ts3client/d/b/u
dup
aload 0
aload 4
invokespecial com/teamspeak/ts3client/d/b/u/<init>(Lcom/teamspeak/ts3client/d/b/a;Lcom/teamspeak/ts3client/Ts3Application;)V
invokevirtual android/widget/Button/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 2
iload 3
invokevirtual android/widget/Button/setEnabled(Z)V
aload 1
aload 2
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
L8:
new android/widget/Button
dup
aload 1
invokevirtual android/widget/LinearLayout/getContext()Landroid/content/Context;
invokespecial android/widget/Button/<init>(Landroid/content/Context;)V
astore 2
aload 2
ldc "dialog.client.group.text"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/widget/Button/setText(Ljava/lang/CharSequence;)V
aload 2
new com/teamspeak/ts3client/d/b/w
dup
aload 0
invokespecial com/teamspeak/ts3client/d/b/w/<init>(Lcom/teamspeak/ts3client/d/b/a;)V
invokevirtual android/widget/Button/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 1
aload 2
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
new android/widget/Button
dup
aload 1
invokevirtual android/widget/LinearLayout/getContext()Landroid/content/Context;
invokespecial android/widget/Button/<init>(Landroid/content/Context;)V
astore 2
aload 2
ldc "dialog.client.cinfo.text"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/widget/Button/setText(Ljava/lang/CharSequence;)V
aload 2
new com/teamspeak/ts3client/d/b/x
dup
aload 0
invokespecial com/teamspeak/ts3client/d/b/x/<init>(Lcom/teamspeak/ts3client/d/b/a;)V
invokevirtual android/widget/Button/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 1
aload 2
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
aload 1
areturn
L10:
aload 0
getfield com/teamspeak/ts3client/d/b/a/at Lcom/teamspeak/ts3client/data/c;
getfield com/teamspeak/ts3client/data/c/r Z
ifeq L8
new android/widget/Button
dup
aload 1
invokevirtual android/widget/LinearLayout/getContext()Landroid/content/Context;
invokespecial android/widget/Button/<init>(Landroid/content/Context;)V
astore 2
aload 2
ldc "dialog.client.talkpower.text2"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/widget/Button/setText(Ljava/lang/CharSequence;)V
aload 2
new com/teamspeak/ts3client/d/b/v
dup
aload 0
aload 4
invokespecial com/teamspeak/ts3client/d/b/v/<init>(Lcom/teamspeak/ts3client/d/b/a;Lcom/teamspeak/ts3client/Ts3Application;)V
invokevirtual android/widget/Button/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 2
iload 3
invokevirtual android/widget/Button/setEnabled(Z)V
aload 1
aload 2
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
goto L8
.limit locals 5
.limit stack 5
.end method

.method public final e()V
aload 0
invokespecial android/support/v4/app/ax/e()V
aload 0
new java/lang/StringBuilder
dup
ldc "cid_"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
getfield com/teamspeak/ts3client/d/b/a/at Lcom/teamspeak/ts3client/data/c;
getfield com/teamspeak/ts3client/data/c/c I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/teamspeak/ts3client/data/d/q/a(Landroid/support/v4/app/ax;Ljava/lang/String;)V
return
.limit locals 1
.limit stack 4
.end method

.method public final f()V
aload 0
invokespecial android/support/v4/app/ax/f()V
aload 0
invokestatic com/teamspeak/ts3client/data/d/q/b(Landroid/support/v4/app/ax;)V
return
.limit locals 1
.limit stack 1
.end method
