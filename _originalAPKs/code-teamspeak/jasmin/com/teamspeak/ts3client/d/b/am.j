.bytecode 50.0
.class public final synchronized com/teamspeak/ts3client/d/b/am
.super java/lang/Object
.implements android/view/View$OnClickListener

.field 'a' Ljava/util/ArrayList;

.field 'b' Landroid/widget/CheckBox;

.field 'c' Z

.field final synthetic 'd' Lcom/teamspeak/ts3client/d/b/aj;

.method public <init>(Lcom/teamspeak/ts3client/d/b/aj;Z)V
aload 0
aload 1
putfield com/teamspeak/ts3client/d/b/am/d Lcom/teamspeak/ts3client/d/b/aj;
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield com/teamspeak/ts3client/d/b/am/a Ljava/util/ArrayList;
aload 0
iconst_0
putfield com/teamspeak/ts3client/d/b/am/c Z
aload 0
iload 2
putfield com/teamspeak/ts3client/d/b/am/c Z
return
.limit locals 3
.limit stack 3
.end method

.method private a(Landroid/widget/CheckBox;)V
aload 0
getfield com/teamspeak/ts3client/d/b/am/a Ljava/util/ArrayList;
aload 1
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 1
aload 0
invokevirtual android/widget/CheckBox/setOnClickListener(Landroid/view/View$OnClickListener;)V
return
.limit locals 2
.limit stack 2
.end method

.method private b(Landroid/widget/CheckBox;)V
aload 0
aload 1
putfield com/teamspeak/ts3client/d/b/am/b Landroid/widget/CheckBox;
return
.limit locals 2
.limit stack 2
.end method

.method public final onClick(Landroid/view/View;)V
aload 1
checkcast android/widget/CompoundButton
invokevirtual android/widget/CompoundButton/isChecked()Z
istore 3
aload 0
getfield com/teamspeak/ts3client/d/b/am/c Z
ifeq L0
aload 0
getfield com/teamspeak/ts3client/d/b/am/d Lcom/teamspeak/ts3client/d/b/aj;
invokestatic com/teamspeak/ts3client/d/b/aj/b(Lcom/teamspeak/ts3client/d/b/aj;)Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/c()Lcom/teamspeak/ts3client/data/b;
aload 0
getfield com/teamspeak/ts3client/d/b/am/d Lcom/teamspeak/ts3client/d/b/aj;
invokestatic com/teamspeak/ts3client/d/b/aj/b(Lcom/teamspeak/ts3client/d/b/aj;)Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/i Ljava/util/HashMap;
aload 0
getfield com/teamspeak/ts3client/d/b/am/d Lcom/teamspeak/ts3client/d/b/aj;
invokestatic com/teamspeak/ts3client/d/b/aj/a(Lcom/teamspeak/ts3client/d/b/aj;)Lcom/teamspeak/ts3client/data/c;
getfield com/teamspeak/ts3client/data/c/c I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/Long
invokevirtual com/teamspeak/ts3client/data/b/a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
astore 12
aload 0
getfield com/teamspeak/ts3client/d/b/am/a Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 13
L1:
aload 13
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L2
aload 13
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast android/widget/CheckBox
iconst_0
invokevirtual android/widget/CheckBox/setChecked(Z)V
goto L1
L2:
iload 3
ifeq L3
aload 1
checkcast android/widget/CompoundButton
iconst_1
invokevirtual android/widget/CompoundButton/setChecked(Z)V
aload 0
getfield com/teamspeak/ts3client/d/b/am/d Lcom/teamspeak/ts3client/d/b/aj;
invokestatic com/teamspeak/ts3client/d/b/aj/b(Lcom/teamspeak/ts3client/d/b/aj;)Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
astore 13
aload 0
getfield com/teamspeak/ts3client/d/b/am/d Lcom/teamspeak/ts3client/d/b/aj;
invokestatic com/teamspeak/ts3client/d/b/aj/b(Lcom/teamspeak/ts3client/d/b/aj;)Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
lstore 4
aload 1
invokevirtual android/view/View/getTag()Ljava/lang/Object;
invokevirtual java/lang/Object/toString()Ljava/lang/String;
invokestatic java/lang/Long/parseLong(Ljava/lang/String;)J
lstore 6
aload 12
getfield com/teamspeak/ts3client/data/a/b J
lstore 8
aload 0
getfield com/teamspeak/ts3client/d/b/am/d Lcom/teamspeak/ts3client/d/b/aj;
invokestatic com/teamspeak/ts3client/d/b/aj/c(Lcom/teamspeak/ts3client/d/b/aj;)J
lstore 10
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
getfield com/teamspeak/ts3client/d/b/am/d Lcom/teamspeak/ts3client/d/b/aj;
invokestatic com/teamspeak/ts3client/d/b/aj/c(Lcom/teamspeak/ts3client/d/b/aj;)J
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
ldc " set to channelgroup "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual android/view/View/getTag()Ljava/lang/Object;
invokevirtual java/lang/Object/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 1
aload 13
lload 4
iconst_1
newarray long
dup
iconst_0
lload 6
lastore
iconst_1
newarray long
dup
iconst_0
lload 8
lastore
iconst_1
newarray long
dup
iconst_0
lload 10
lastore
iconst_1
aload 1
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_requestSetClientChannelGroup(J[J[J[JILjava/lang/String;)I
pop
return
L3:
aload 0
getfield com/teamspeak/ts3client/d/b/am/d Lcom/teamspeak/ts3client/d/b/aj;
invokestatic com/teamspeak/ts3client/d/b/aj/b(Lcom/teamspeak/ts3client/d/b/aj;)Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
astore 1
aload 0
getfield com/teamspeak/ts3client/d/b/am/d Lcom/teamspeak/ts3client/d/b/aj;
invokestatic com/teamspeak/ts3client/d/b/aj/b(Lcom/teamspeak/ts3client/d/b/aj;)Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
lstore 4
aload 0
getfield com/teamspeak/ts3client/d/b/am/b Landroid/widget/CheckBox;
invokevirtual android/widget/CheckBox/getTag()Ljava/lang/Object;
invokevirtual java/lang/Object/toString()Ljava/lang/String;
invokestatic java/lang/Long/parseLong(Ljava/lang/String;)J
lstore 6
aload 12
getfield com/teamspeak/ts3client/data/a/b J
lstore 8
aload 0
getfield com/teamspeak/ts3client/d/b/am/d Lcom/teamspeak/ts3client/d/b/aj;
invokestatic com/teamspeak/ts3client/d/b/aj/c(Lcom/teamspeak/ts3client/d/b/aj;)J
lstore 10
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
getfield com/teamspeak/ts3client/d/b/am/d Lcom/teamspeak/ts3client/d/b/aj;
invokestatic com/teamspeak/ts3client/d/b/aj/c(Lcom/teamspeak/ts3client/d/b/aj;)J
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
ldc " set to channelgroup "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/d/b/am/b Landroid/widget/CheckBox;
invokevirtual android/widget/CheckBox/getTag()Ljava/lang/Object;
invokevirtual java/lang/Object/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 12
aload 1
lload 4
iconst_1
newarray long
dup
iconst_0
lload 6
lastore
iconst_1
newarray long
dup
iconst_0
lload 8
lastore
iconst_1
newarray long
dup
iconst_0
lload 10
lastore
iconst_1
aload 12
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_requestSetClientChannelGroup(J[J[J[JILjava/lang/String;)I
pop
aload 0
getfield com/teamspeak/ts3client/d/b/am/b Landroid/widget/CheckBox;
iconst_1
invokevirtual android/widget/CheckBox/setChecked(Z)V
return
L0:
aload 1
invokevirtual android/view/View/getTag()Ljava/lang/Object;
aload 0
getfield com/teamspeak/ts3client/d/b/am/b Landroid/widget/CheckBox;
invokevirtual android/widget/CheckBox/getTag()Ljava/lang/Object;
invokevirtual java/lang/Object/equals(Ljava/lang/Object;)Z
ifne L4
aload 0
getfield com/teamspeak/ts3client/d/b/am/b Landroid/widget/CheckBox;
iconst_0
invokevirtual android/widget/CheckBox/setChecked(Z)V
aload 0
getfield com/teamspeak/ts3client/d/b/am/a Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 12
iconst_0
istore 2
L5:
aload 12
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L6
aload 12
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast android/widget/CheckBox
astore 13
aload 13
invokevirtual android/widget/CheckBox/isChecked()Z
ifeq L7
aload 0
getfield com/teamspeak/ts3client/d/b/am/d Lcom/teamspeak/ts3client/d/b/aj;
invokestatic com/teamspeak/ts3client/d/b/aj/d(Lcom/teamspeak/ts3client/d/b/aj;)I
aload 0
getfield com/teamspeak/ts3client/d/b/am/d Lcom/teamspeak/ts3client/d/b/aj;
invokestatic com/teamspeak/ts3client/d/b/aj/b(Lcom/teamspeak/ts3client/d/b/aj;)Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/o Lcom/teamspeak/ts3client/data/g/b;
aload 13
invokevirtual android/widget/CheckBox/getTag()Ljava/lang/Object;
invokevirtual java/lang/Object/toString()Ljava/lang/String;
invokestatic java/lang/Long/parseLong(Ljava/lang/String;)J
invokevirtual com/teamspeak/ts3client/data/g/b/b(J)Lcom/teamspeak/ts3client/data/g/a;
getfield com/teamspeak/ts3client/data/g/a/f I
if_icmpge L8
aload 13
iconst_0
invokevirtual android/widget/CheckBox/setEnabled(Z)V
iconst_1
istore 2
goto L5
L8:
aload 13
iconst_1
invokevirtual android/widget/CheckBox/setEnabled(Z)V
iconst_1
istore 2
L9:
goto L5
L6:
iload 2
ifne L4
aload 0
getfield com/teamspeak/ts3client/d/b/am/b Landroid/widget/CheckBox;
iconst_1
invokevirtual android/widget/CheckBox/setChecked(Z)V
L4:
iload 3
ifeq L10
aload 0
getfield com/teamspeak/ts3client/d/b/am/d Lcom/teamspeak/ts3client/d/b/aj;
invokestatic com/teamspeak/ts3client/d/b/aj/b(Lcom/teamspeak/ts3client/d/b/aj;)Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/d/b/am/d Lcom/teamspeak/ts3client/d/b/aj;
invokestatic com/teamspeak/ts3client/d/b/aj/b(Lcom/teamspeak/ts3client/d/b/aj;)Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
aload 1
invokevirtual android/view/View/getTag()Ljava/lang/Object;
invokevirtual java/lang/Object/toString()Ljava/lang/String;
invokestatic java/lang/Long/parseLong(Ljava/lang/String;)J
aload 0
getfield com/teamspeak/ts3client/d/b/am/d Lcom/teamspeak/ts3client/d/b/aj;
invokestatic com/teamspeak/ts3client/d/b/aj/c(Lcom/teamspeak/ts3client/d/b/aj;)J
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
getfield com/teamspeak/ts3client/d/b/am/d Lcom/teamspeak/ts3client/d/b/aj;
invokestatic com/teamspeak/ts3client/d/b/aj/c(Lcom/teamspeak/ts3client/d/b/aj;)J
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
ldc " added in sgroup "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual android/view/View/getTag()Ljava/lang/Object;
invokevirtual java/lang/Object/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_requestServerGroupAddClient(JJJLjava/lang/String;)I
pop
return
L10:
aload 0
getfield com/teamspeak/ts3client/d/b/am/d Lcom/teamspeak/ts3client/d/b/aj;
invokestatic com/teamspeak/ts3client/d/b/aj/b(Lcom/teamspeak/ts3client/d/b/aj;)Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/d/b/am/d Lcom/teamspeak/ts3client/d/b/aj;
invokestatic com/teamspeak/ts3client/d/b/aj/b(Lcom/teamspeak/ts3client/d/b/aj;)Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
aload 1
invokevirtual android/view/View/getTag()Ljava/lang/Object;
invokevirtual java/lang/Object/toString()Ljava/lang/String;
invokestatic java/lang/Long/parseLong(Ljava/lang/String;)J
aload 0
getfield com/teamspeak/ts3client/d/b/am/d Lcom/teamspeak/ts3client/d/b/aj;
invokestatic com/teamspeak/ts3client/d/b/aj/c(Lcom/teamspeak/ts3client/d/b/aj;)J
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
getfield com/teamspeak/ts3client/d/b/am/d Lcom/teamspeak/ts3client/d/b/aj;
invokestatic com/teamspeak/ts3client/d/b/aj/c(Lcom/teamspeak/ts3client/d/b/aj;)J
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
ldc " added in sgroup "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual android/view/View/getTag()Ljava/lang/Object;
invokevirtual java/lang/Object/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_requestServerGroupDelClient(JJJLjava/lang/String;)I
pop
return
L7:
goto L9
.limit locals 14
.limit stack 10
.end method
