.bytecode 50.0
.class public final synchronized com/teamspeak/ts3client/d/e
.super android/support/v4/app/ax

.field private 'aA' Landroid/widget/CheckBox;

.field private 'aB' Landroid/widget/CheckBox;

.field private 'aC' Lcom/teamspeak/ts3client/data/c;

.field private 'aD' Landroid/widget/RadioGroup;

.field private 'aE' Landroid/widget/RadioButton;

.field private 'aF' Landroid/widget/RadioButton;

.field private 'aG' Landroid/widget/Button;

.field private 'aH' Lcom/teamspeak/ts3client/c/a;

.field private 'aI' Z

.field private 'at' Landroid/widget/EditText;

.field private 'au' Landroid/widget/Spinner;

.field private 'av' Landroid/widget/Spinner;

.field private 'aw' Landroid/widget/CheckBox;

.field private 'ax' Landroid/widget/CheckBox;

.field private 'ay' Landroid/widget/CheckBox;

.field private 'az' Landroid/widget/CheckBox;

.method public <init>(Lcom/teamspeak/ts3client/c/a;)V
aload 0
invokespecial android/support/v4/app/ax/<init>()V
aload 0
new com/teamspeak/ts3client/data/c
dup
aload 1
invokespecial com/teamspeak/ts3client/data/c/<init>(Lcom/teamspeak/ts3client/c/a;)V
putfield com/teamspeak/ts3client/d/e/aC Lcom/teamspeak/ts3client/data/c;
return
.limit locals 2
.limit stack 4
.end method

.method public <init>(Lcom/teamspeak/ts3client/data/c;)V
aload 0
invokespecial android/support/v4/app/ax/<init>()V
aload 0
aload 1
putfield com/teamspeak/ts3client/d/e/aC Lcom/teamspeak/ts3client/data/c;
return
.limit locals 2
.limit stack 2
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/d/e;)Z
aload 0
getfield com/teamspeak/ts3client/d/e/aI Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic b(Lcom/teamspeak/ts3client/d/e;)Z
aload 0
iconst_0
putfield com/teamspeak/ts3client/d/e/aI Z
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method static synthetic c(Lcom/teamspeak/ts3client/d/e;)Landroid/widget/Spinner;
aload 0
getfield com/teamspeak/ts3client/d/e/av Landroid/widget/Spinner;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic d(Lcom/teamspeak/ts3client/d/e;)Landroid/widget/CheckBox;
aload 0
getfield com/teamspeak/ts3client/d/e/aw Landroid/widget/CheckBox;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic e(Lcom/teamspeak/ts3client/d/e;)Landroid/widget/CheckBox;
aload 0
getfield com/teamspeak/ts3client/d/e/ax Landroid/widget/CheckBox;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic f(Lcom/teamspeak/ts3client/d/e;)Landroid/widget/CheckBox;
aload 0
getfield com/teamspeak/ts3client/d/e/ay Landroid/widget/CheckBox;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic g(Lcom/teamspeak/ts3client/d/e;)Landroid/widget/CheckBox;
aload 0
getfield com/teamspeak/ts3client/d/e/az Landroid/widget/CheckBox;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic h(Lcom/teamspeak/ts3client/d/e;)Landroid/widget/CheckBox;
aload 0
getfield com/teamspeak/ts3client/d/e/aA Landroid/widget/CheckBox;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic i(Lcom/teamspeak/ts3client/d/e;)Landroid/widget/CheckBox;
aload 0
getfield com/teamspeak/ts3client/d/e/aB Landroid/widget/CheckBox;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic j(Lcom/teamspeak/ts3client/d/e;)Landroid/widget/RadioButton;
aload 0
getfield com/teamspeak/ts3client/d/e/aE Landroid/widget/RadioButton;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic k(Lcom/teamspeak/ts3client/d/e;)Landroid/widget/RadioButton;
aload 0
getfield com/teamspeak/ts3client/d/e/aF Landroid/widget/RadioButton;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic l(Lcom/teamspeak/ts3client/d/e;)Landroid/widget/EditText;
aload 0
getfield com/teamspeak/ts3client/d/e/at Landroid/widget/EditText;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic m(Lcom/teamspeak/ts3client/d/e;)Lcom/teamspeak/ts3client/c/a;
aload 0
getfield com/teamspeak/ts3client/d/e/aH Lcom/teamspeak/ts3client/c/a;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic n(Lcom/teamspeak/ts3client/d/e;)Landroid/widget/Spinner;
aload 0
getfield com/teamspeak/ts3client/d/e/au Landroid/widget/Spinner;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic o(Lcom/teamspeak/ts3client/d/e;)Lcom/teamspeak/ts3client/data/c;
aload 0
getfield com/teamspeak/ts3client/d/e/aC Lcom/teamspeak/ts3client/data/c;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
aload 1
invokevirtual android/view/LayoutInflater/getContext()Landroid/content/Context;
invokevirtual android/content/Context/getApplicationContext()Landroid/content/Context;
checkcast com/teamspeak/ts3client/Ts3Application
astore 3
aload 1
ldc_w 2130903089
aload 2
iconst_0
invokevirtual android/view/LayoutInflater/inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;
checkcast android/widget/ScrollView
astore 1
aload 0
getfield android/support/v4/app/ax/j Landroid/app/Dialog;
ldc "contact.settings.info"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
getfield com/teamspeak/ts3client/d/e/aC Lcom/teamspeak/ts3client/data/c;
getfield com/teamspeak/ts3client/data/c/a Ljava/lang/String;
aastore
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokevirtual android/app/Dialog/setTitle(Ljava/lang/CharSequence;)V
aload 0
aload 1
ldc_w 2131493220
invokevirtual android/widget/ScrollView/findViewById(I)Landroid/view/View;
checkcast android/widget/EditText
putfield com/teamspeak/ts3client/d/e/at Landroid/widget/EditText;
aload 0
getfield com/teamspeak/ts3client/d/e/at Landroid/widget/EditText;
aload 0
getfield com/teamspeak/ts3client/d/e/aC Lcom/teamspeak/ts3client/data/c;
getfield com/teamspeak/ts3client/data/c/a Ljava/lang/String;
invokevirtual android/widget/EditText/setText(Ljava/lang/CharSequence;)V
aload 0
aload 1
ldc_w 2131493221
invokevirtual android/widget/ScrollView/findViewById(I)Landroid/view/View;
checkcast android/widget/Spinner
putfield com/teamspeak/ts3client/d/e/au Landroid/widget/Spinner;
aload 0
getfield com/teamspeak/ts3client/d/e/au Landroid/widget/Spinner;
ldc "contact.settings.status.array"
aload 1
invokevirtual android/widget/ScrollView/getContext()Landroid/content/Context;
iconst_3
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;Landroid/content/Context;I)Landroid/widget/SpinnerAdapter;
invokevirtual android/widget/Spinner/setAdapter(Landroid/widget/SpinnerAdapter;)V
aload 0
getfield com/teamspeak/ts3client/d/e/au Landroid/widget/Spinner;
new com/teamspeak/ts3client/d/f
dup
aload 0
invokespecial com/teamspeak/ts3client/d/f/<init>(Lcom/teamspeak/ts3client/d/e;)V
invokevirtual android/widget/Spinner/setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V
aload 0
aload 1
ldc_w 2131493223
invokevirtual android/widget/ScrollView/findViewById(I)Landroid/view/View;
checkcast android/widget/Spinner
putfield com/teamspeak/ts3client/d/e/av Landroid/widget/Spinner;
aload 0
getfield com/teamspeak/ts3client/d/e/av Landroid/widget/Spinner;
ldc "contact.settings.display.array"
aload 1
invokevirtual android/widget/ScrollView/getContext()Landroid/content/Context;
iconst_3
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;Landroid/content/Context;I)Landroid/widget/SpinnerAdapter;
invokevirtual android/widget/Spinner/setAdapter(Landroid/widget/SpinnerAdapter;)V
aload 0
getfield com/teamspeak/ts3client/d/e/av Landroid/widget/Spinner;
iconst_2
invokevirtual android/widget/Spinner/setSelection(I)V
aload 0
aload 1
ldc_w 2131493225
invokevirtual android/widget/ScrollView/findViewById(I)Landroid/view/View;
checkcast android/widget/CheckBox
putfield com/teamspeak/ts3client/d/e/aw Landroid/widget/CheckBox;
aload 0
aload 1
ldc_w 2131493226
invokevirtual android/widget/ScrollView/findViewById(I)Landroid/view/View;
checkcast android/widget/CheckBox
putfield com/teamspeak/ts3client/d/e/ax Landroid/widget/CheckBox;
aload 0
aload 1
ldc_w 2131493227
invokevirtual android/widget/ScrollView/findViewById(I)Landroid/view/View;
checkcast android/widget/CheckBox
putfield com/teamspeak/ts3client/d/e/ay Landroid/widget/CheckBox;
aload 0
aload 1
ldc_w 2131493228
invokevirtual android/widget/ScrollView/findViewById(I)Landroid/view/View;
checkcast android/widget/CheckBox
putfield com/teamspeak/ts3client/d/e/az Landroid/widget/CheckBox;
aload 0
aload 1
ldc_w 2131493230
invokevirtual android/widget/ScrollView/findViewById(I)Landroid/view/View;
checkcast android/widget/CheckBox
putfield com/teamspeak/ts3client/d/e/aA Landroid/widget/CheckBox;
aload 0
aload 1
ldc_w 2131493229
invokevirtual android/widget/ScrollView/findViewById(I)Landroid/view/View;
checkcast android/widget/CheckBox
putfield com/teamspeak/ts3client/d/e/aB Landroid/widget/CheckBox;
aload 0
aload 1
ldc_w 2131493232
invokevirtual android/widget/ScrollView/findViewById(I)Landroid/view/View;
checkcast android/widget/RadioGroup
putfield com/teamspeak/ts3client/d/e/aD Landroid/widget/RadioGroup;
aload 0
aload 1
ldc_w 2131493233
invokevirtual android/widget/ScrollView/findViewById(I)Landroid/view/View;
checkcast android/widget/RadioButton
putfield com/teamspeak/ts3client/d/e/aE Landroid/widget/RadioButton;
aload 0
aload 1
ldc_w 2131493234
invokevirtual android/widget/ScrollView/findViewById(I)Landroid/view/View;
checkcast android/widget/RadioButton
putfield com/teamspeak/ts3client/d/e/aF Landroid/widget/RadioButton;
aload 0
aload 1
ldc_w 2131493235
invokevirtual android/widget/ScrollView/findViewById(I)Landroid/view/View;
checkcast android/widget/Button
putfield com/teamspeak/ts3client/d/e/aG Landroid/widget/Button;
aload 0
getfield com/teamspeak/ts3client/d/e/aG Landroid/widget/Button;
new com/teamspeak/ts3client/d/g
dup
aload 0
aload 3
invokespecial com/teamspeak/ts3client/d/g/<init>(Lcom/teamspeak/ts3client/d/e;Lcom/teamspeak/ts3client/Ts3Application;)V
invokevirtual android/widget/Button/setOnClickListener(Landroid/view/View$OnClickListener;)V
ldc "contact.settings.name"
aload 1
ldc_w 2131493219
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;Landroid/view/ViewGroup;I)V
ldc "contact.settings.display"
aload 1
ldc_w 2131493222
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;Landroid/view/ViewGroup;I)V
ldc "contact.settings.ignore"
aload 1
ldc_w 2131493224
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;Landroid/view/ViewGroup;I)V
ldc "contact.settings.whisper"
aload 1
ldc_w 2131493231
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;Landroid/view/ViewGroup;I)V
ldc "contact.settings.mute"
aload 1
ldc_w 2131493225
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;Landroid/view/ViewGroup;I)V
ldc "contact.settings.ignorepublicchat"
aload 1
ldc_w 2131493226
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;Landroid/view/ViewGroup;I)V
ldc "contact.settings.ignoreprivatechat"
aload 1
ldc_w 2131493227
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;Landroid/view/ViewGroup;I)V
ldc "contact.settings.ignorepokes"
aload 1
ldc_w 2131493228
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;Landroid/view/ViewGroup;I)V
ldc "contact.settings.hideavatar"
aload 1
ldc_w 2131493229
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;Landroid/view/ViewGroup;I)V
ldc "contact.settings.hideaway"
aload 1
ldc_w 2131493230
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;Landroid/view/ViewGroup;I)V
ldc "contact.settings.whisper.allow"
aload 1
ldc_w 2131493233
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;Landroid/view/ViewGroup;I)V
ldc "contact.settings.whisper.deny"
aload 1
ldc_w 2131493234
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;Landroid/view/ViewGroup;I)V
aload 0
getfield com/teamspeak/ts3client/d/e/aG Landroid/widget/Button;
ldc "button.save"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/widget/Button/setText(Ljava/lang/CharSequence;)V
aload 1
areturn
.limit locals 4
.limit stack 6
.end method

.method public final c(Landroid/os/Bundle;)V
iconst_1
istore 2
aload 0
aload 1
invokespecial android/support/v4/app/ax/c(Landroid/os/Bundle;)V
getstatic com/teamspeak/ts3client/data/b/c/a Lcom/teamspeak/ts3client/data/b/c;
aload 0
getfield com/teamspeak/ts3client/d/e/aC Lcom/teamspeak/ts3client/data/c;
getfield com/teamspeak/ts3client/data/c/b Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/data/b/c/a(Ljava/lang/String;)Lcom/teamspeak/ts3client/c/a;
astore 1
aload 1
ifnull L0
aload 0
aload 1
putfield com/teamspeak/ts3client/d/e/aH Lcom/teamspeak/ts3client/c/a;
aload 0
iconst_1
putfield com/teamspeak/ts3client/d/e/aI Z
aload 0
getfield com/teamspeak/ts3client/d/e/at Landroid/widget/EditText;
aload 1
getfield com/teamspeak/ts3client/c/a/a Ljava/lang/String;
invokevirtual android/widget/EditText/setText(Ljava/lang/CharSequence;)V
aload 0
getfield com/teamspeak/ts3client/d/e/au Landroid/widget/Spinner;
aload 1
getfield com/teamspeak/ts3client/c/a/e I
invokevirtual android/widget/Spinner/setSelection(I)V
aload 0
getfield com/teamspeak/ts3client/d/e/av Landroid/widget/Spinner;
aload 1
getfield com/teamspeak/ts3client/c/a/d I
invokevirtual android/widget/Spinner/setSelection(I)V
aload 0
getfield com/teamspeak/ts3client/d/e/aw Landroid/widget/CheckBox;
aload 1
getfield com/teamspeak/ts3client/c/a/f Z
invokevirtual android/widget/CheckBox/setChecked(Z)V
aload 0
getfield com/teamspeak/ts3client/d/e/ax Landroid/widget/CheckBox;
aload 1
getfield com/teamspeak/ts3client/c/a/g Z
invokevirtual android/widget/CheckBox/setChecked(Z)V
aload 0
getfield com/teamspeak/ts3client/d/e/ay Landroid/widget/CheckBox;
aload 1
getfield com/teamspeak/ts3client/c/a/h Z
invokevirtual android/widget/CheckBox/setChecked(Z)V
aload 0
getfield com/teamspeak/ts3client/d/e/az Landroid/widget/CheckBox;
aload 1
getfield com/teamspeak/ts3client/c/a/i Z
invokevirtual android/widget/CheckBox/setChecked(Z)V
aload 0
getfield com/teamspeak/ts3client/d/e/aA Landroid/widget/CheckBox;
aload 1
getfield com/teamspeak/ts3client/c/a/j Z
invokevirtual android/widget/CheckBox/setChecked(Z)V
aload 0
getfield com/teamspeak/ts3client/d/e/aB Landroid/widget/CheckBox;
aload 1
getfield com/teamspeak/ts3client/c/a/k Z
invokevirtual android/widget/CheckBox/setChecked(Z)V
aload 0
getfield com/teamspeak/ts3client/d/e/aE Landroid/widget/RadioButton;
aload 1
getfield com/teamspeak/ts3client/c/a/l Z
invokevirtual android/widget/RadioButton/setChecked(Z)V
aload 0
getfield com/teamspeak/ts3client/d/e/aF Landroid/widget/RadioButton;
astore 3
aload 1
getfield com/teamspeak/ts3client/c/a/l Z
ifne L1
L2:
aload 3
iload 2
invokevirtual android/widget/RadioButton/setChecked(Z)V
L0:
return
L1:
iconst_0
istore 2
goto L2
.limit locals 4
.limit stack 2
.end method

.method public final e()V
aload 0
invokespecial android/support/v4/app/ax/e()V
aload 0
invokestatic com/teamspeak/ts3client/data/d/q/a(Landroid/support/v4/app/ax;)V
return
.limit locals 1
.limit stack 1
.end method

.method public final f()V
aload 0
invokespecial android/support/v4/app/ax/f()V
aload 0
invokestatic com/teamspeak/ts3client/data/d/q/b(Landroid/support/v4/app/ax;)V
return
.limit locals 1
.limit stack 1
.end method
