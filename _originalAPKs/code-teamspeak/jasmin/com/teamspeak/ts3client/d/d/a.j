.bytecode 50.0
.class public final synchronized com/teamspeak/ts3client/d/d/a
.super android/widget/BaseAdapter

.field 'a' Ljava/util/ArrayList;

.field private 'b' Landroid/content/Context;

.field private 'c' Landroid/view/LayoutInflater;

.field private 'd' Landroid/support/v4/app/bi;

.method public <init>(Landroid/content/Context;Landroid/support/v4/app/bi;)V
aload 0
invokespecial android/widget/BaseAdapter/<init>()V
aload 0
aload 1
putfield com/teamspeak/ts3client/d/d/a/b Landroid/content/Context;
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield com/teamspeak/ts3client/d/d/a/a Ljava/util/ArrayList;
aload 0
aload 1
invokestatic android/view/LayoutInflater/from(Landroid/content/Context;)Landroid/view/LayoutInflater;
putfield com/teamspeak/ts3client/d/d/a/c Landroid/view/LayoutInflater;
aload 0
aload 2
putfield com/teamspeak/ts3client/d/d/a/d Landroid/support/v4/app/bi;
return
.limit locals 3
.limit stack 3
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/d/d/a;)Ljava/util/ArrayList;
aload 0
getfield com/teamspeak/ts3client/d/d/a/a Ljava/util/ArrayList;
areturn
.limit locals 1
.limit stack 1
.end method

.method private a()V
aload 0
getfield com/teamspeak/ts3client/d/d/a/a Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/clear()V
return
.limit locals 1
.limit stack 1
.end method

.method private a(Lcom/teamspeak/ts3client/d/d/c;)V
aload 0
getfield com/teamspeak/ts3client/d/d/a/a Ljava/util/ArrayList;
aload 1
invokevirtual java/util/ArrayList/contains(Ljava/lang/Object;)Z
ifne L0
aload 0
getfield com/teamspeak/ts3client/d/d/a/a Ljava/util/ArrayList;
aload 1
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L0:
return
.limit locals 2
.limit stack 2
.end method

.method static synthetic b(Lcom/teamspeak/ts3client/d/d/a;)Landroid/support/v4/app/bi;
aload 0
getfield com/teamspeak/ts3client/d/d/a/d Landroid/support/v4/app/bi;
areturn
.limit locals 1
.limit stack 1
.end method

.method private b(Lcom/teamspeak/ts3client/d/d/c;)V
aload 0
getfield com/teamspeak/ts3client/d/d/a/a Ljava/util/ArrayList;
aload 1
invokevirtual java/util/ArrayList/contains(Ljava/lang/Object;)Z
ifne L0
aload 0
getfield com/teamspeak/ts3client/d/d/a/a Ljava/util/ArrayList;
aload 1
invokevirtual java/util/ArrayList/remove(Ljava/lang/Object;)Z
pop
L0:
return
.limit locals 2
.limit stack 2
.end method

.method public final getCount()I
aload 0
getfield com/teamspeak/ts3client/d/d/a/a Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final getItem(I)Ljava/lang/Object;
aload 0
getfield com/teamspeak/ts3client/d/d/a/a Ljava/util/ArrayList;
iload 1
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
areturn
.limit locals 2
.limit stack 2
.end method

.method public final getItemId(I)J
lconst_0
lreturn
.limit locals 2
.limit stack 2
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
aload 0
getfield com/teamspeak/ts3client/d/d/a/c Landroid/view/LayoutInflater;
ldc_w 2130903129
aconst_null
invokevirtual android/view/LayoutInflater/inflate(ILandroid/view/ViewGroup;)Landroid/view/View;
astore 2
aload 2
ldc_w 2131493351
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/TextView
astore 4
aload 2
ldc_w 2131493352
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/TextView
astore 3
aload 4
ldc "temppass.entry.text"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
getfield com/teamspeak/ts3client/d/d/a/a Ljava/util/ArrayList;
iload 1
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/teamspeak/ts3client/d/d/c
getfield com/teamspeak/ts3client/d/d/c/c Ljava/lang/String;
aastore
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
aload 0
getfield com/teamspeak/ts3client/d/d/a/a Ljava/util/ArrayList;
iload 1
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/teamspeak/ts3client/d/d/c
astore 4
aload 3
ldc "temppass.entry.until"
iconst_1
anewarray java/lang/Object
dup
iconst_0
new java/lang/StringBuilder
dup
ldc " "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
invokestatic java/text/DateFormat/getDateTimeInstance()Ljava/text/DateFormat;
new java/util/Date
dup
aload 4
getfield com/teamspeak/ts3client/d/d/c/e J
ldc2_w 1000L
lmul
invokespecial java/util/Date/<init>(J)V
invokevirtual java/text/DateFormat/format(Ljava/util/Date;)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
aload 2
new com/teamspeak/ts3client/d/d/b
dup
aload 0
iload 1
invokespecial com/teamspeak/ts3client/d/d/b/<init>(Lcom/teamspeak/ts3client/d/d/a;I)V
invokevirtual android/view/View/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 2
areturn
.limit locals 5
.limit stack 13
.end method
