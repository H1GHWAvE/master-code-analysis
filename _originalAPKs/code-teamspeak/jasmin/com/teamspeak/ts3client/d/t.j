.bytecode 50.0
.class public final synchronized com/teamspeak/ts3client/d/t
.super android/app/Dialog
.implements com/teamspeak/ts3client/customs/f
.implements com/teamspeak/ts3client/d/j

.field public 'a' Lcom/teamspeak/ts3client/d/k;

.field public 'b' Lcom/teamspeak/ts3client/d/k;

.field public 'c' I

.field public 'd' Lcom/teamspeak/ts3client/d/k;

.field public 'e' Lcom/teamspeak/ts3client/d/l;

.field public 'f' Ljava/lang/String;

.field private 'g' Lcom/teamspeak/ts3client/customs/CustomLicenseAgreementWebView;

.field private 'h' Ljava/lang/String;

.field private 'i' Landroid/widget/Button;

.field private 'j' Ljava/lang/String;

.method public <init>(Landroid/content/Context;)V
aload 0
aload 1
ldc_w 2131165364
invokespecial android/app/Dialog/<init>(Landroid/content/Context;I)V
aload 0
aload 1
invokespecial com/teamspeak/ts3client/d/t/a(Landroid/content/Context;)V
return
.limit locals 2
.limit stack 3
.end method

.method private <init>(Landroid/content/Context;B)V
aload 0
aload 1
invokespecial android/app/Dialog/<init>(Landroid/content/Context;)V
aload 0
aload 1
invokespecial com/teamspeak/ts3client/d/t/a(Landroid/content/Context;)V
return
.limit locals 3
.limit stack 2
.end method

.method private <init>(Landroid/content/Context;ZLandroid/content/DialogInterface$OnCancelListener;)V
aload 0
aload 1
iload 2
aload 3
invokespecial android/app/Dialog/<init>(Landroid/content/Context;ZLandroid/content/DialogInterface$OnCancelListener;)V
aload 0
aload 1
invokespecial com/teamspeak/ts3client/d/t/a(Landroid/content/Context;)V
return
.limit locals 4
.limit stack 4
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/d/t;)Lcom/teamspeak/ts3client/d/k;
aload 0
getfield com/teamspeak/ts3client/d/t/b Lcom/teamspeak/ts3client/d/k;
areturn
.limit locals 1
.limit stack 1
.end method

.method private a(I)V
aload 0
iload 1
putfield com/teamspeak/ts3client/d/t/c I
return
.limit locals 2
.limit stack 2
.end method

.method private a(Landroid/content/Context;)V
aload 0
iconst_0
invokevirtual com/teamspeak/ts3client/d/t/setCancelable(Z)V
aload 0
ldc "License Agreement"
invokevirtual com/teamspeak/ts3client/d/t/setTitle(Ljava/lang/CharSequence;)V
aload 1
invokestatic android/view/LayoutInflater/from(Landroid/content/Context;)Landroid/view/LayoutInflater;
ldc_w 2130903107
aconst_null
iconst_0
invokevirtual android/view/LayoutInflater/inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;
astore 1
aload 0
aload 1
ldc_w 2131493262
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast com/teamspeak/ts3client/customs/CustomLicenseAgreementWebView
putfield com/teamspeak/ts3client/d/t/g Lcom/teamspeak/ts3client/customs/CustomLicenseAgreementWebView;
aload 0
getfield com/teamspeak/ts3client/d/t/g Lcom/teamspeak/ts3client/customs/CustomLicenseAgreementWebView;
new com/teamspeak/ts3client/d/u
dup
aload 0
invokespecial com/teamspeak/ts3client/d/u/<init>(Lcom/teamspeak/ts3client/d/t;)V
invokevirtual com/teamspeak/ts3client/customs/CustomLicenseAgreementWebView/setWebViewClient(Landroid/webkit/WebViewClient;)V
aload 1
ldc_w 2131493259
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/Button
astore 2
aload 2
ldc "button.reject"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/widget/Button/setText(Ljava/lang/CharSequence;)V
aload 2
new com/teamspeak/ts3client/d/v
dup
aload 0
invokespecial com/teamspeak/ts3client/d/v/<init>(Lcom/teamspeak/ts3client/d/t;)V
invokevirtual android/widget/Button/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 1
ldc_w 2131493260
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/Button
astore 2
aload 2
ldc "button.browser"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/widget/Button/setText(Ljava/lang/CharSequence;)V
aload 2
new com/teamspeak/ts3client/d/w
dup
aload 0
invokespecial com/teamspeak/ts3client/d/w/<init>(Lcom/teamspeak/ts3client/d/t;)V
invokevirtual android/widget/Button/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 0
aload 1
ldc_w 2131493261
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/Button
putfield com/teamspeak/ts3client/d/t/i Landroid/widget/Button;
aload 0
getfield com/teamspeak/ts3client/d/t/i Landroid/widget/Button;
ldc "button.accept"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/widget/Button/setText(Ljava/lang/CharSequence;)V
aload 0
getfield com/teamspeak/ts3client/d/t/i Landroid/widget/Button;
new com/teamspeak/ts3client/d/x
dup
aload 0
invokespecial com/teamspeak/ts3client/d/x/<init>(Lcom/teamspeak/ts3client/d/t;)V
invokevirtual android/widget/Button/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 0
getfield com/teamspeak/ts3client/d/t/g Lcom/teamspeak/ts3client/customs/CustomLicenseAgreementWebView;
aload 0
invokevirtual com/teamspeak/ts3client/customs/CustomLicenseAgreementWebView/setOnBottomReachedListener(Lcom/teamspeak/ts3client/customs/f;)V
aload 0
aload 1
new android/view/ViewGroup$LayoutParams
dup
iconst_m1
iconst_m1
invokespecial android/view/ViewGroup$LayoutParams/<init>(II)V
invokevirtual com/teamspeak/ts3client/d/t/addContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
return
.limit locals 3
.limit stack 6
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/d/t;Ljava/lang/String;)V
aload 0
aload 1
putfield com/teamspeak/ts3client/d/t/h Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/d/t/g Lcom/teamspeak/ts3client/customs/CustomLicenseAgreementWebView;
aload 1
ldc "text/html; charset=UTF-8"
aconst_null
invokevirtual com/teamspeak/ts3client/customs/CustomLicenseAgreementWebView/loadData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
aload 0
getfield com/teamspeak/ts3client/d/t/e Lcom/teamspeak/ts3client/d/l;
invokeinterface com/teamspeak/ts3client/d/l/n()V 0
return
.limit locals 2
.limit stack 4
.end method

.method private a(Ljava/lang/String;)V
aload 0
aload 1
putfield com/teamspeak/ts3client/d/t/h Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/d/t/g Lcom/teamspeak/ts3client/customs/CustomLicenseAgreementWebView;
aload 1
ldc "text/html; charset=UTF-8"
aconst_null
invokevirtual com/teamspeak/ts3client/customs/CustomLicenseAgreementWebView/loadData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
aload 0
getfield com/teamspeak/ts3client/d/t/e Lcom/teamspeak/ts3client/d/l;
invokeinterface com/teamspeak/ts3client/d/l/n()V 0
return
.limit locals 2
.limit stack 4
.end method

.method static synthetic b(Lcom/teamspeak/ts3client/d/t;)Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/d/t/j Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic b(Lcom/teamspeak/ts3client/d/t;Ljava/lang/String;)Ljava/lang/String;
aload 0
aload 1
putfield com/teamspeak/ts3client/d/t/f Ljava/lang/String;
aload 1
areturn
.limit locals 2
.limit stack 2
.end method

.method private b(Ljava/lang/String;)V
aload 0
aload 1
putfield com/teamspeak/ts3client/d/t/j Ljava/lang/String;
return
.limit locals 2
.limit stack 2
.end method

.method static synthetic c(Lcom/teamspeak/ts3client/d/t;)Lcom/teamspeak/ts3client/d/k;
aload 0
getfield com/teamspeak/ts3client/d/t/a Lcom/teamspeak/ts3client/d/k;
areturn
.limit locals 1
.limit stack 1
.end method

.method private c()Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/d/t/j Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic d(Lcom/teamspeak/ts3client/d/t;)I
aload 0
getfield com/teamspeak/ts3client/d/t/c I
ireturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic e(Lcom/teamspeak/ts3client/d/t;)Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/d/t/f Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic f(Lcom/teamspeak/ts3client/d/t;)Lcom/teamspeak/ts3client/d/k;
aload 0
getfield com/teamspeak/ts3client/d/t/d Lcom/teamspeak/ts3client/d/k;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic g(Lcom/teamspeak/ts3client/d/t;)V
aload 0
invokevirtual com/teamspeak/ts3client/d/t/b()V
return
.limit locals 1
.limit stack 1
.end method

.method public final a()V
aload 0
getfield com/teamspeak/ts3client/d/t/i Landroid/widget/Button;
ifnull L0
aload 0
getfield com/teamspeak/ts3client/d/t/i Landroid/widget/Button;
iconst_1
invokevirtual android/widget/Button/setEnabled(Z)V
L0:
return
.limit locals 1
.limit stack 2
.end method

.method public final a(Lcom/teamspeak/ts3client/d/k;)V
aload 0
aload 1
putfield com/teamspeak/ts3client/d/t/a Lcom/teamspeak/ts3client/d/k;
return
.limit locals 2
.limit stack 2
.end method

.method public final a(Lcom/teamspeak/ts3client/d/l;)V
aload 0
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
ldc "lang_tag"
invokestatic java/util/Locale/getDefault()Ljava/util/Locale;
invokevirtual java/util/Locale/getLanguage()Ljava/lang/String;
invokeinterface android/content/SharedPreferences/getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String; 2
putfield com/teamspeak/ts3client/d/t/f Ljava/lang/String;
aload 0
aload 1
putfield com/teamspeak/ts3client/d/t/e Lcom/teamspeak/ts3client/d/l;
aload 0
invokevirtual com/teamspeak/ts3client/d/t/b()V
return
.limit locals 2
.limit stack 4
.end method

.method public final b()V
aload 0
new java/lang/StringBuilder
dup
ldc "http://la.teamspeak.com/"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
getfield com/teamspeak/ts3client/d/t/c I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/d/t/f Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/la.html"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
putfield com/teamspeak/ts3client/d/t/j Ljava/lang/String;
new com/teamspeak/ts3client/d/y
dup
aload 0
iconst_0
invokespecial com/teamspeak/ts3client/d/y/<init>(Lcom/teamspeak/ts3client/d/t;B)V
iconst_1
anewarray java/lang/String
dup
iconst_0
aload 0
getfield com/teamspeak/ts3client/d/t/j Ljava/lang/String;
aastore
invokevirtual com/teamspeak/ts3client/d/y/execute([Ljava/lang/Object;)Landroid/os/AsyncTask;
pop
return
.limit locals 1
.limit stack 5
.end method

.method public final b(Lcom/teamspeak/ts3client/d/k;)V
aload 0
aload 1
putfield com/teamspeak/ts3client/d/t/b Lcom/teamspeak/ts3client/d/k;
return
.limit locals 2
.limit stack 2
.end method

.method public final c(Lcom/teamspeak/ts3client/d/k;)V
aload 0
aload 1
putfield com/teamspeak/ts3client/d/t/d Lcom/teamspeak/ts3client/d/k;
return
.limit locals 2
.limit stack 2
.end method
