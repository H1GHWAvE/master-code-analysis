.bytecode 50.0
.class public final synchronized com/a/a/a/a/a
.super java/lang/Object
.implements com/a/a/a/a/r

.field private static final 'a' Ljava/lang/String; = "UTF-8"

.field private static final 'b' Ljava/lang/String; = "PBEWITHSHAAND256BITAES-CBC-BC"

.field private static final 'c' Ljava/lang/String; = "AES/CBC/PKCS5Padding"

.field private static final 'd' [B

.field private static final 'e' Ljava/lang/String; = "com.android.vending.licensing.AESObfuscator-1|"

.field private 'f' Ljavax/crypto/Cipher;

.field private 'g' Ljavax/crypto/Cipher;

.method static <clinit>()V
bipush 16
newarray byte
dup
iconst_0
ldc_w 16
bastore
dup
iconst_1
ldc_w 74
bastore
dup
iconst_2
ldc_w 71
bastore
dup
iconst_3
ldc_w -80
bastore
dup
iconst_4
ldc_w 32
bastore
dup
iconst_5
ldc_w 101
bastore
dup
bipush 6
ldc_w -47
bastore
dup
bipush 7
ldc_w 72
bastore
dup
bipush 8
ldc_w 117
bastore
dup
bipush 9
ldc_w -14
bastore
dup
bipush 10
ldc_w 0
bastore
dup
bipush 11
ldc_w -29
bastore
dup
bipush 12
ldc_w 70
bastore
dup
bipush 13
ldc_w 65
bastore
dup
bipush 14
ldc_w -12
bastore
dup
bipush 15
ldc_w 74
bastore
putstatic com/a/a/a/a/a/d [B
return
.limit locals 0
.limit stack 4
.end method

.method public <init>([BLjava/lang/String;Ljava/lang/String;)V
.catch java/security/GeneralSecurityException from L0 to L1 using L2
aload 0
invokespecial java/lang/Object/<init>()V
L0:
new javax/crypto/spec/SecretKeySpec
dup
ldc "PBEWITHSHAAND256BITAES-CBC-BC"
invokestatic javax/crypto/SecretKeyFactory/getInstance(Ljava/lang/String;)Ljavax/crypto/SecretKeyFactory;
new javax/crypto/spec/PBEKeySpec
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/lang/String/toCharArray()[C
aload 1
sipush 1024
sipush 256
invokespecial javax/crypto/spec/PBEKeySpec/<init>([C[BII)V
invokevirtual javax/crypto/SecretKeyFactory/generateSecret(Ljava/security/spec/KeySpec;)Ljavax/crypto/SecretKey;
invokeinterface javax/crypto/SecretKey/getEncoded()[B 0
ldc "AES"
invokespecial javax/crypto/spec/SecretKeySpec/<init>([BLjava/lang/String;)V
astore 1
aload 0
ldc "AES/CBC/PKCS5Padding"
invokestatic javax/crypto/Cipher/getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;
putfield com/a/a/a/a/a/f Ljavax/crypto/Cipher;
aload 0
getfield com/a/a/a/a/a/f Ljavax/crypto/Cipher;
iconst_1
aload 1
new javax/crypto/spec/IvParameterSpec
dup
getstatic com/a/a/a/a/a/d [B
invokespecial javax/crypto/spec/IvParameterSpec/<init>([B)V
invokevirtual javax/crypto/Cipher/init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V
aload 0
ldc "AES/CBC/PKCS5Padding"
invokestatic javax/crypto/Cipher/getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;
putfield com/a/a/a/a/a/g Ljavax/crypto/Cipher;
aload 0
getfield com/a/a/a/a/a/g Ljavax/crypto/Cipher;
iconst_2
aload 1
new javax/crypto/spec/IvParameterSpec
dup
getstatic com/a/a/a/a/a/d [B
invokespecial javax/crypto/spec/IvParameterSpec/<init>([B)V
invokevirtual javax/crypto/Cipher/init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V
L1:
return
L2:
astore 1
new java/lang/RuntimeException
dup
ldc "Invalid environment"
aload 1
invokespecial java/lang/RuntimeException/<init>(Ljava/lang/String;Ljava/lang/Throwable;)V
athrow
.limit locals 4
.limit stack 9
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
.catch java/io/UnsupportedEncodingException from L0 to L1 using L2
.catch java/security/GeneralSecurityException from L0 to L1 using L3
aload 1
ifnonnull L0
aconst_null
areturn
L0:
aload 0
getfield com/a/a/a/a/a/f Ljavax/crypto/Cipher;
new java/lang/StringBuilder
dup
ldc "com.android.vending.licensing.AESObfuscator-1|"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
ldc "UTF-8"
invokevirtual java/lang/String/getBytes(Ljava/lang/String;)[B
invokevirtual javax/crypto/Cipher/doFinal([B)[B
invokestatic com/a/a/a/a/a/a/a([B)Ljava/lang/String;
astore 1
L1:
aload 1
areturn
L2:
astore 1
new java/lang/RuntimeException
dup
ldc "Invalid environment"
aload 1
invokespecial java/lang/RuntimeException/<init>(Ljava/lang/String;Ljava/lang/Throwable;)V
athrow
L3:
astore 1
new java/lang/RuntimeException
dup
ldc "Invalid environment"
aload 1
invokespecial java/lang/RuntimeException/<init>(Ljava/lang/String;Ljava/lang/Throwable;)V
athrow
.limit locals 3
.limit stack 4
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
.catch com/a/a/a/a/a/b from L0 to L1 using L1
.catch javax/crypto/IllegalBlockSizeException from L0 to L1 using L2
.catch javax/crypto/BadPaddingException from L0 to L1 using L3
.catch java/io/UnsupportedEncodingException from L0 to L1 using L4
.catch com/a/a/a/a/a/b from L5 to L6 using L1
.catch javax/crypto/IllegalBlockSizeException from L5 to L6 using L2
.catch javax/crypto/BadPaddingException from L5 to L6 using L3
.catch java/io/UnsupportedEncodingException from L5 to L6 using L4
aload 1
ifnonnull L0
aconst_null
areturn
L0:
new java/lang/String
dup
aload 0
getfield com/a/a/a/a/a/g Ljavax/crypto/Cipher;
aload 1
invokestatic com/a/a/a/a/a/a/a(Ljava/lang/String;)[B
invokevirtual javax/crypto/Cipher/doFinal([B)[B
ldc "UTF-8"
invokespecial java/lang/String/<init>([BLjava/lang/String;)V
astore 3
aload 3
new java/lang/StringBuilder
dup
ldc "com.android.vending.licensing.AESObfuscator-1|"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/lang/String/indexOf(Ljava/lang/String;)I
ifeq L5
new com/a/a/a/a/y
dup
new java/lang/StringBuilder
dup
ldc "Header not found (invalid data or key):"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial com/a/a/a/a/y/<init>(Ljava/lang/String;)V
athrow
L1:
astore 2
new com/a/a/a/a/y
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 2
invokevirtual com/a/a/a/a/a/b/getMessage()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ":"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial com/a/a/a/a/y/<init>(Ljava/lang/String;)V
athrow
L5:
aload 3
aload 2
invokevirtual java/lang/String/length()I
bipush 46
iadd
aload 3
invokevirtual java/lang/String/length()I
invokevirtual java/lang/String/substring(II)Ljava/lang/String;
astore 2
L6:
aload 2
areturn
L2:
astore 2
new com/a/a/a/a/y
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 2
invokevirtual javax/crypto/IllegalBlockSizeException/getMessage()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ":"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial com/a/a/a/a/y/<init>(Ljava/lang/String;)V
athrow
L3:
astore 2
new com/a/a/a/a/y
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 2
invokevirtual javax/crypto/BadPaddingException/getMessage()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ":"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial com/a/a/a/a/y/<init>(Ljava/lang/String;)V
athrow
L4:
astore 1
new java/lang/RuntimeException
dup
ldc "Invalid environment"
aload 1
invokespecial java/lang/RuntimeException/<init>(Ljava/lang/String;Ljava/lang/Throwable;)V
athrow
.limit locals 4
.limit stack 5
.end method
