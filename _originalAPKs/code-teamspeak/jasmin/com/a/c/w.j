.bytecode 50.0
.class public synchronized abstract com/a/c/w
.super java/lang/Object

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private p()Z
aload 0
instanceof com/a/c/t
ireturn
.limit locals 1
.limit stack 1
.end method

.method private q()Z
aload 0
instanceof com/a/c/z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private r()Z
aload 0
instanceof com/a/c/ac
ireturn
.limit locals 1
.limit stack 1
.end method

.method private s()Z
aload 0
instanceof com/a/c/y
ireturn
.limit locals 1
.limit stack 1
.end method

.method private t()Lcom/a/c/z;
aload 0
instanceof com/a/c/z
ifeq L0
aload 0
checkcast com/a/c/z
areturn
L0:
new java/lang/IllegalStateException
dup
new java/lang/StringBuilder
dup
ldc "Not a JSON Object: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
.limit locals 1
.limit stack 5
.end method

.method private u()Lcom/a/c/t;
aload 0
instanceof com/a/c/t
ifeq L0
aload 0
checkcast com/a/c/t
areturn
L0:
new java/lang/IllegalStateException
dup
ldc "This is not a JSON Array."
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
.limit locals 1
.limit stack 3
.end method

.method private v()Lcom/a/c/y;
aload 0
instanceof com/a/c/y
ifeq L0
aload 0
checkcast com/a/c/y
areturn
L0:
new java/lang/IllegalStateException
dup
ldc "This is not a JSON Null."
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
.limit locals 1
.limit stack 3
.end method

.method public a()Ljava/lang/Number;
new java/lang/UnsupportedOperationException
dup
aload 0
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
invokevirtual java/lang/Class/getSimpleName()Ljava/lang/String;
invokespecial java/lang/UnsupportedOperationException/<init>(Ljava/lang/String;)V
athrow
.limit locals 1
.limit stack 3
.end method

.method public b()Ljava/lang/String;
new java/lang/UnsupportedOperationException
dup
aload 0
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
invokevirtual java/lang/Class/getSimpleName()Ljava/lang/String;
invokespecial java/lang/UnsupportedOperationException/<init>(Ljava/lang/String;)V
athrow
.limit locals 1
.limit stack 3
.end method

.method public c()D
new java/lang/UnsupportedOperationException
dup
aload 0
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
invokevirtual java/lang/Class/getSimpleName()Ljava/lang/String;
invokespecial java/lang/UnsupportedOperationException/<init>(Ljava/lang/String;)V
athrow
.limit locals 1
.limit stack 3
.end method

.method public d()Ljava/math/BigDecimal;
new java/lang/UnsupportedOperationException
dup
aload 0
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
invokevirtual java/lang/Class/getSimpleName()Ljava/lang/String;
invokespecial java/lang/UnsupportedOperationException/<init>(Ljava/lang/String;)V
athrow
.limit locals 1
.limit stack 3
.end method

.method public e()Ljava/math/BigInteger;
new java/lang/UnsupportedOperationException
dup
aload 0
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
invokevirtual java/lang/Class/getSimpleName()Ljava/lang/String;
invokespecial java/lang/UnsupportedOperationException/<init>(Ljava/lang/String;)V
athrow
.limit locals 1
.limit stack 3
.end method

.method public f()F
new java/lang/UnsupportedOperationException
dup
aload 0
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
invokevirtual java/lang/Class/getSimpleName()Ljava/lang/String;
invokespecial java/lang/UnsupportedOperationException/<init>(Ljava/lang/String;)V
athrow
.limit locals 1
.limit stack 3
.end method

.method public g()J
new java/lang/UnsupportedOperationException
dup
aload 0
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
invokevirtual java/lang/Class/getSimpleName()Ljava/lang/String;
invokespecial java/lang/UnsupportedOperationException/<init>(Ljava/lang/String;)V
athrow
.limit locals 1
.limit stack 3
.end method

.method public h()I
new java/lang/UnsupportedOperationException
dup
aload 0
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
invokevirtual java/lang/Class/getSimpleName()Ljava/lang/String;
invokespecial java/lang/UnsupportedOperationException/<init>(Ljava/lang/String;)V
athrow
.limit locals 1
.limit stack 3
.end method

.method public i()B
new java/lang/UnsupportedOperationException
dup
aload 0
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
invokevirtual java/lang/Class/getSimpleName()Ljava/lang/String;
invokespecial java/lang/UnsupportedOperationException/<init>(Ljava/lang/String;)V
athrow
.limit locals 1
.limit stack 3
.end method

.method public j()C
new java/lang/UnsupportedOperationException
dup
aload 0
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
invokevirtual java/lang/Class/getSimpleName()Ljava/lang/String;
invokespecial java/lang/UnsupportedOperationException/<init>(Ljava/lang/String;)V
athrow
.limit locals 1
.limit stack 3
.end method

.method public k()S
new java/lang/UnsupportedOperationException
dup
aload 0
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
invokevirtual java/lang/Class/getSimpleName()Ljava/lang/String;
invokespecial java/lang/UnsupportedOperationException/<init>(Ljava/lang/String;)V
athrow
.limit locals 1
.limit stack 3
.end method

.method public l()Z
new java/lang/UnsupportedOperationException
dup
aload 0
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
invokevirtual java/lang/Class/getSimpleName()Ljava/lang/String;
invokespecial java/lang/UnsupportedOperationException/<init>(Ljava/lang/String;)V
athrow
.limit locals 1
.limit stack 3
.end method

.method abstract m()Lcom/a/c/w;
.end method

.method public final n()Lcom/a/c/ac;
aload 0
instanceof com/a/c/ac
ifeq L0
aload 0
checkcast com/a/c/ac
areturn
L0:
new java/lang/IllegalStateException
dup
ldc "This is not a JSON Primitive."
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
.limit locals 1
.limit stack 3
.end method

.method o()Ljava/lang/Boolean;
new java/lang/UnsupportedOperationException
dup
aload 0
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
invokevirtual java/lang/Class/getSimpleName()Ljava/lang/String;
invokespecial java/lang/UnsupportedOperationException/<init>(Ljava/lang/String;)V
athrow
.limit locals 1
.limit stack 3
.end method

.method public toString()Ljava/lang/String;
.catch java/io/IOException from L0 to L1 using L2
L0:
new java/io/StringWriter
dup
invokespecial java/io/StringWriter/<init>()V
astore 1
new com/a/c/d/e
dup
aload 1
invokespecial com/a/c/d/e/<init>(Ljava/io/Writer;)V
astore 2
aload 2
iconst_1
putfield com/a/c/d/e/c Z
aload 0
aload 2
invokestatic com/a/c/b/aq/a(Lcom/a/c/w;Lcom/a/c/d/e;)V
aload 1
invokevirtual java/io/StringWriter/toString()Ljava/lang/String;
astore 1
L1:
aload 1
areturn
L2:
astore 1
new java/lang/AssertionError
dup
aload 1
invokespecial java/lang/AssertionError/<init>(Ljava/lang/Object;)V
athrow
.limit locals 3
.limit stack 3
.end method
