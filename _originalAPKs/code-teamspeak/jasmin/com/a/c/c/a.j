.bytecode 50.0
.class public final synchronized com/a/c/c/a
.super java/lang/Object

.field public final 'a' Ljava/lang/Class;

.field public final 'b' Ljava/lang/reflect/Type;

.field final 'c' I

.method protected <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
invokevirtual java/lang/Class/getGenericSuperclass()Ljava/lang/reflect/Type;
astore 1
aload 1
instanceof java/lang/Class
ifeq L0
new java/lang/RuntimeException
dup
ldc "Missing type parameter."
invokespecial java/lang/RuntimeException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
aload 1
checkcast java/lang/reflect/ParameterizedType
invokeinterface java/lang/reflect/ParameterizedType/getActualTypeArguments()[Ljava/lang/reflect/Type; 0
iconst_0
aaload
invokestatic com/a/c/b/b/a(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;
putfield com/a/c/c/a/b Ljava/lang/reflect/Type;
aload 0
aload 0
getfield com/a/c/c/a/b Ljava/lang/reflect/Type;
invokestatic com/a/c/b/b/b(Ljava/lang/reflect/Type;)Ljava/lang/Class;
putfield com/a/c/c/a/a Ljava/lang/Class;
aload 0
aload 0
getfield com/a/c/c/a/b Ljava/lang/reflect/Type;
invokevirtual java/lang/Object/hashCode()I
putfield com/a/c/c/a/c I
return
.limit locals 2
.limit stack 3
.end method

.method private <init>(Ljava/lang/reflect/Type;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
invokestatic com/a/c/b/a/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/reflect/Type
invokestatic com/a/c/b/b/a(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;
putfield com/a/c/c/a/b Ljava/lang/reflect/Type;
aload 0
aload 0
getfield com/a/c/c/a/b Ljava/lang/reflect/Type;
invokestatic com/a/c/b/b/b(Ljava/lang/reflect/Type;)Ljava/lang/Class;
putfield com/a/c/c/a/a Ljava/lang/Class;
aload 0
aload 0
getfield com/a/c/c/a/b Ljava/lang/reflect/Type;
invokevirtual java/lang/Object/hashCode()I
putfield com/a/c/c/a/c I
return
.limit locals 2
.limit stack 2
.end method

.method public static a(Ljava/lang/Class;)Lcom/a/c/c/a;
new com/a/c/c/a
dup
aload 0
invokespecial com/a/c/c/a/<init>(Ljava/lang/reflect/Type;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method public static a(Ljava/lang/reflect/Type;)Lcom/a/c/c/a;
new com/a/c/c/a
dup
aload 0
invokespecial com/a/c/c/a/<init>(Ljava/lang/reflect/Type;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private static transient a(Ljava/lang/reflect/Type;[Ljava/lang/Class;)Ljava/lang/AssertionError;
new java/lang/StringBuilder
dup
ldc "Unexpected type. Expected one of: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
astore 3
iconst_0
istore 2
L0:
iload 2
iconst_3
if_icmpge L1
aload 3
aload 1
iload 2
aaload
invokevirtual java/lang/Class/getName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ", "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
iload 2
iconst_1
iadd
istore 2
goto L0
L1:
aload 3
ldc "but got: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
invokevirtual java/lang/Class/getName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ", for type token: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/Object/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
bipush 46
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
pop
new java/lang/AssertionError
dup
aload 3
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/AssertionError/<init>(Ljava/lang/Object;)V
areturn
.limit locals 4
.limit stack 3
.end method

.method private a()Ljava/lang/Class;
aload 0
getfield com/a/c/c/a/a Ljava/lang/Class;
areturn
.limit locals 1
.limit stack 1
.end method

.method private a(Lcom/a/c/c/a;)Z
.annotation visible Ljava/lang/Deprecated;
.end annotation
aload 0
aload 1
getfield com/a/c/c/a/b Ljava/lang/reflect/Type;
invokespecial com/a/c/c/a/b(Ljava/lang/reflect/Type;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method private static a(Ljava/lang/reflect/ParameterizedType;Ljava/lang/reflect/ParameterizedType;Ljava/util/Map;)Z
aload 0
invokeinterface java/lang/reflect/ParameterizedType/getRawType()Ljava/lang/reflect/Type; 0
aload 1
invokeinterface java/lang/reflect/ParameterizedType/getRawType()Ljava/lang/reflect/Type; 0
invokevirtual java/lang/Object/equals(Ljava/lang/Object;)Z
ifeq L0
aload 0
invokeinterface java/lang/reflect/ParameterizedType/getActualTypeArguments()[Ljava/lang/reflect/Type; 0
astore 0
aload 1
invokeinterface java/lang/reflect/ParameterizedType/getActualTypeArguments()[Ljava/lang/reflect/Type; 0
astore 1
iconst_0
istore 3
L1:
iload 3
aload 0
arraylength
if_icmpge L2
aload 0
iload 3
aaload
astore 5
aload 1
iload 3
aaload
astore 6
aload 6
aload 5
invokevirtual java/lang/Object/equals(Ljava/lang/Object;)Z
ifne L3
aload 5
instanceof java/lang/reflect/TypeVariable
ifeq L4
aload 6
aload 2
aload 5
checkcast java/lang/reflect/TypeVariable
invokeinterface java/lang/reflect/TypeVariable/getName()Ljava/lang/String; 0
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
invokevirtual java/lang/Object/equals(Ljava/lang/Object;)Z
ifeq L4
L3:
iconst_1
istore 4
L5:
iload 4
ifne L6
L0:
iconst_0
ireturn
L4:
iconst_0
istore 4
goto L5
L6:
iload 3
iconst_1
iadd
istore 3
goto L1
L2:
iconst_1
ireturn
.limit locals 7
.limit stack 3
.end method

.method private static a(Ljava/lang/reflect/Type;Ljava/lang/reflect/GenericArrayType;)Z
aload 1
invokeinterface java/lang/reflect/GenericArrayType/getGenericComponentType()Ljava/lang/reflect/Type; 0
astore 2
aload 2
instanceof java/lang/reflect/ParameterizedType
ifeq L0
aload 0
instanceof java/lang/reflect/GenericArrayType
ifeq L1
aload 0
checkcast java/lang/reflect/GenericArrayType
invokeinterface java/lang/reflect/GenericArrayType/getGenericComponentType()Ljava/lang/reflect/Type; 0
astore 1
L2:
aload 1
aload 2
checkcast java/lang/reflect/ParameterizedType
new java/util/HashMap
dup
invokespecial java/util/HashMap/<init>()V
invokestatic com/a/c/c/a/a(Ljava/lang/reflect/Type;Ljava/lang/reflect/ParameterizedType;Ljava/util/Map;)Z
ireturn
L1:
aload 0
astore 1
aload 0
instanceof java/lang/Class
ifeq L2
aload 0
checkcast java/lang/Class
astore 0
L3:
aload 0
astore 1
aload 0
invokevirtual java/lang/Class/isArray()Z
ifeq L2
aload 0
invokevirtual java/lang/Class/getComponentType()Ljava/lang/Class;
astore 0
goto L3
L0:
iconst_1
ireturn
.limit locals 3
.limit stack 4
.end method

.method private static a(Ljava/lang/reflect/Type;Ljava/lang/reflect/ParameterizedType;Ljava/util/Map;)Z
L0:
aload 0
ifnonnull L1
iconst_0
ireturn
L1:
aload 1
aload 0
invokevirtual java/lang/Object/equals(Ljava/lang/Object;)Z
ifeq L2
iconst_1
ireturn
L2:
aload 0
invokestatic com/a/c/b/b/b(Ljava/lang/reflect/Type;)Ljava/lang/Class;
astore 6
aload 0
instanceof java/lang/reflect/ParameterizedType
ifeq L3
aload 0
checkcast java/lang/reflect/ParameterizedType
astore 0
L4:
aload 0
ifnull L5
aload 0
invokeinterface java/lang/reflect/ParameterizedType/getActualTypeArguments()[Ljava/lang/reflect/Type; 0
astore 7
aload 6
invokevirtual java/lang/Class/getTypeParameters()[Ljava/lang/reflect/TypeVariable;
astore 8
iconst_0
istore 3
L6:
iload 3
aload 7
arraylength
if_icmpge L7
aload 7
iload 3
aaload
astore 5
aload 8
iload 3
aaload
astore 9
L8:
aload 5
instanceof java/lang/reflect/TypeVariable
ifeq L9
aload 2
aload 5
checkcast java/lang/reflect/TypeVariable
invokeinterface java/lang/reflect/TypeVariable/getName()Ljava/lang/String; 0
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast java/lang/reflect/Type
astore 5
goto L8
L9:
aload 2
aload 9
invokeinterface java/lang/reflect/TypeVariable/getName()Ljava/lang/String; 0
aload 5
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
iload 3
iconst_1
iadd
istore 3
goto L6
L7:
aload 0
invokeinterface java/lang/reflect/ParameterizedType/getRawType()Ljava/lang/reflect/Type; 0
aload 1
invokeinterface java/lang/reflect/ParameterizedType/getRawType()Ljava/lang/reflect/Type; 0
invokevirtual java/lang/Object/equals(Ljava/lang/Object;)Z
ifeq L10
aload 0
invokeinterface java/lang/reflect/ParameterizedType/getActualTypeArguments()[Ljava/lang/reflect/Type; 0
astore 0
aload 1
invokeinterface java/lang/reflect/ParameterizedType/getActualTypeArguments()[Ljava/lang/reflect/Type; 0
astore 5
iconst_0
istore 3
L11:
iload 3
aload 0
arraylength
if_icmpge L12
aload 0
iload 3
aaload
astore 7
aload 5
iload 3
aaload
astore 8
aload 8
aload 7
invokevirtual java/lang/Object/equals(Ljava/lang/Object;)Z
ifne L13
aload 7
instanceof java/lang/reflect/TypeVariable
ifeq L14
aload 8
aload 2
aload 7
checkcast java/lang/reflect/TypeVariable
invokeinterface java/lang/reflect/TypeVariable/getName()Ljava/lang/String; 0
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
invokevirtual java/lang/Object/equals(Ljava/lang/Object;)Z
ifeq L14
L13:
iconst_1
istore 4
L15:
iload 4
ifeq L10
iload 3
iconst_1
iadd
istore 3
goto L11
L14:
iconst_0
istore 4
goto L15
L12:
iconst_1
istore 3
L16:
iload 3
ifeq L5
iconst_1
ireturn
L10:
iconst_0
istore 3
goto L16
L5:
aload 6
invokevirtual java/lang/Class/getGenericInterfaces()[Ljava/lang/reflect/Type;
astore 0
aload 0
arraylength
istore 4
iconst_0
istore 3
L17:
iload 3
iload 4
if_icmpge L18
aload 0
iload 3
aaload
aload 1
new java/util/HashMap
dup
aload 2
invokespecial java/util/HashMap/<init>(Ljava/util/Map;)V
invokestatic com/a/c/c/a/a(Ljava/lang/reflect/Type;Ljava/lang/reflect/ParameterizedType;Ljava/util/Map;)Z
ifeq L19
iconst_1
ireturn
L19:
iload 3
iconst_1
iadd
istore 3
goto L17
L18:
aload 6
invokevirtual java/lang/Class/getGenericSuperclass()Ljava/lang/reflect/Type;
astore 0
new java/util/HashMap
dup
aload 2
invokespecial java/util/HashMap/<init>(Ljava/util/Map;)V
astore 2
goto L0
L3:
aconst_null
astore 0
goto L4
.limit locals 10
.limit stack 5
.end method

.method private static a(Ljava/lang/reflect/Type;Ljava/lang/reflect/Type;Ljava/util/Map;)Z
aload 1
aload 0
invokevirtual java/lang/Object/equals(Ljava/lang/Object;)Z
ifne L0
aload 0
instanceof java/lang/reflect/TypeVariable
ifeq L1
aload 1
aload 2
aload 0
checkcast java/lang/reflect/TypeVariable
invokeinterface java/lang/reflect/TypeVariable/getName()Ljava/lang/String; 0
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
invokevirtual java/lang/Object/equals(Ljava/lang/Object;)Z
ifeq L1
L0:
iconst_1
ireturn
L1:
iconst_0
ireturn
.limit locals 3
.limit stack 3
.end method

.method private b()Ljava/lang/reflect/Type;
aload 0
getfield com/a/c/c/a/b Ljava/lang/reflect/Type;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static b(Ljava/lang/Class;)Ljava/lang/reflect/Type;
aload 0
invokevirtual java/lang/Class/getGenericSuperclass()Ljava/lang/reflect/Type;
astore 0
aload 0
instanceof java/lang/Class
ifeq L0
new java/lang/RuntimeException
dup
ldc "Missing type parameter."
invokespecial java/lang/RuntimeException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
checkcast java/lang/reflect/ParameterizedType
invokeinterface java/lang/reflect/ParameterizedType/getActualTypeArguments()[Ljava/lang/reflect/Type; 0
iconst_0
aaload
invokestatic com/a/c/b/b/a(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;
areturn
.limit locals 1
.limit stack 3
.end method

.method private b(Ljava/lang/reflect/Type;)Z
.annotation visible Ljava/lang/Deprecated;
.end annotation
aload 1
ifnonnull L0
iconst_0
ireturn
L0:
aload 0
getfield com/a/c/c/a/b Ljava/lang/reflect/Type;
aload 1
invokevirtual java/lang/Object/equals(Ljava/lang/Object;)Z
ifeq L1
iconst_1
ireturn
L1:
aload 0
getfield com/a/c/c/a/b Ljava/lang/reflect/Type;
instanceof java/lang/Class
ifeq L2
aload 0
getfield com/a/c/c/a/a Ljava/lang/Class;
aload 1
invokestatic com/a/c/b/b/b(Ljava/lang/reflect/Type;)Ljava/lang/Class;
invokevirtual java/lang/Class/isAssignableFrom(Ljava/lang/Class;)Z
ireturn
L2:
aload 0
getfield com/a/c/c/a/b Ljava/lang/reflect/Type;
instanceof java/lang/reflect/ParameterizedType
ifeq L3
aload 1
aload 0
getfield com/a/c/c/a/b Ljava/lang/reflect/Type;
checkcast java/lang/reflect/ParameterizedType
new java/util/HashMap
dup
invokespecial java/util/HashMap/<init>()V
invokestatic com/a/c/c/a/a(Ljava/lang/reflect/Type;Ljava/lang/reflect/ParameterizedType;Ljava/util/Map;)Z
ireturn
L3:
aload 0
getfield com/a/c/c/a/b Ljava/lang/reflect/Type;
instanceof java/lang/reflect/GenericArrayType
ifeq L4
aload 0
getfield com/a/c/c/a/a Ljava/lang/Class;
aload 1
invokestatic com/a/c/b/b/b(Ljava/lang/reflect/Type;)Ljava/lang/Class;
invokevirtual java/lang/Class/isAssignableFrom(Ljava/lang/Class;)Z
ifeq L5
aload 1
aload 0
getfield com/a/c/c/a/b Ljava/lang/reflect/Type;
checkcast java/lang/reflect/GenericArrayType
invokestatic com/a/c/c/a/a(Ljava/lang/reflect/Type;Ljava/lang/reflect/GenericArrayType;)Z
ifeq L5
iconst_1
ireturn
L5:
iconst_0
ireturn
L4:
aload 0
getfield com/a/c/c/a/b Ljava/lang/reflect/Type;
iconst_3
anewarray java/lang/Class
dup
iconst_0
ldc java/lang/Class
aastore
dup
iconst_1
ldc java/lang/reflect/ParameterizedType
aastore
dup
iconst_2
ldc java/lang/reflect/GenericArrayType
aastore
invokestatic com/a/c/c/a/a(Ljava/lang/reflect/Type;[Ljava/lang/Class;)Ljava/lang/AssertionError;
athrow
.limit locals 2
.limit stack 5
.end method

.method private c(Ljava/lang/Class;)Z
.annotation visible Ljava/lang/Deprecated;
.end annotation
aload 0
aload 1
invokespecial com/a/c/c/a/b(Ljava/lang/reflect/Type;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final equals(Ljava/lang/Object;)Z
aload 1
instanceof com/a/c/c/a
ifeq L0
aload 0
getfield com/a/c/c/a/b Ljava/lang/reflect/Type;
aload 1
checkcast com/a/c/c/a
getfield com/a/c/c/a/b Ljava/lang/reflect/Type;
invokestatic com/a/c/b/b/a(Ljava/lang/reflect/Type;Ljava/lang/reflect/Type;)Z
ifeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final hashCode()I
aload 0
getfield com/a/c/c/a/c I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final toString()Ljava/lang/String;
aload 0
getfield com/a/c/c/a/b Ljava/lang/reflect/Type;
invokestatic com/a/c/b/b/c(Ljava/lang/reflect/Type;)Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method
