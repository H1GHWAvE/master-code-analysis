.bytecode 50.0
.class public final synchronized com/a/c/b/b
.super java/lang/Object

.field static final 'a' [Ljava/lang/reflect/Type;

.method static <clinit>()V
iconst_0
anewarray java/lang/reflect/Type
putstatic com/a/c/b/b/a [Ljava/lang/reflect/Type;
return
.limit locals 0
.limit stack 1
.end method

.method private <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method static synthetic a(Ljava/lang/Object;)I
aload 0
ifnull L0
aload 0
invokevirtual java/lang/Object/hashCode()I
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method private static a([Ljava/lang/Object;Ljava/lang/Object;)I
iconst_0
istore 2
L0:
iload 2
aload 0
arraylength
if_icmpge L1
aload 1
aload 0
iload 2
aaload
invokevirtual java/lang/Object/equals(Ljava/lang/Object;)Z
ifeq L2
iload 2
ireturn
L2:
iload 2
iconst_1
iadd
istore 2
goto L0
L1:
new java/util/NoSuchElementException
dup
invokespecial java/util/NoSuchElementException/<init>()V
athrow
.limit locals 3
.limit stack 3
.end method

.method private static a(Ljava/lang/reflect/TypeVariable;)Ljava/lang/Class;
aload 0
invokeinterface java/lang/reflect/TypeVariable/getGenericDeclaration()Ljava/lang/reflect/GenericDeclaration; 0
astore 0
aload 0
instanceof java/lang/Class
ifeq L0
aload 0
checkcast java/lang/Class
areturn
L0:
aconst_null
areturn
.limit locals 1
.limit stack 1
.end method

.method private static transient a(Ljava/lang/reflect/Type;Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;
new com/a/c/b/d
dup
aload 0
aload 1
aload 2
invokespecial com/a/c/b/d/<init>(Ljava/lang/reflect/Type;Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)V
areturn
.limit locals 3
.limit stack 5
.end method

.method public static a(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;
aload 0
instanceof java/lang/Class
ifeq L0
aload 0
checkcast java/lang/Class
astore 0
aload 0
invokevirtual java/lang/Class/isArray()Z
ifeq L1
new com/a/c/b/c
dup
aload 0
invokevirtual java/lang/Class/getComponentType()Ljava/lang/Class;
invokestatic com/a/c/b/b/a(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;
invokespecial com/a/c/b/c/<init>(Ljava/lang/reflect/Type;)V
astore 0
L2:
aload 0
checkcast java/lang/reflect/Type
areturn
L1:
goto L2
L0:
aload 0
instanceof java/lang/reflect/ParameterizedType
ifeq L3
aload 0
checkcast java/lang/reflect/ParameterizedType
astore 0
new com/a/c/b/d
dup
aload 0
invokeinterface java/lang/reflect/ParameterizedType/getOwnerType()Ljava/lang/reflect/Type; 0
aload 0
invokeinterface java/lang/reflect/ParameterizedType/getRawType()Ljava/lang/reflect/Type; 0
aload 0
invokeinterface java/lang/reflect/ParameterizedType/getActualTypeArguments()[Ljava/lang/reflect/Type; 0
invokespecial com/a/c/b/d/<init>(Ljava/lang/reflect/Type;Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)V
areturn
L3:
aload 0
instanceof java/lang/reflect/GenericArrayType
ifeq L4
new com/a/c/b/c
dup
aload 0
checkcast java/lang/reflect/GenericArrayType
invokeinterface java/lang/reflect/GenericArrayType/getGenericComponentType()Ljava/lang/reflect/Type; 0
invokespecial com/a/c/b/c/<init>(Ljava/lang/reflect/Type;)V
areturn
L4:
aload 0
instanceof java/lang/reflect/WildcardType
ifeq L5
aload 0
checkcast java/lang/reflect/WildcardType
astore 0
new com/a/c/b/e
dup
aload 0
invokeinterface java/lang/reflect/WildcardType/getUpperBounds()[Ljava/lang/reflect/Type; 0
aload 0
invokeinterface java/lang/reflect/WildcardType/getLowerBounds()[Ljava/lang/reflect/Type; 0
invokespecial com/a/c/b/e/<init>([Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)V
areturn
L5:
aload 0
areturn
.limit locals 1
.limit stack 5
.end method

.method public static a(Ljava/lang/reflect/Type;Ljava/lang/Class;)Ljava/lang/reflect/Type;
aload 0
aload 1
ldc java/util/Collection
invokestatic com/a/c/b/b/b(Ljava/lang/reflect/Type;Ljava/lang/Class;Ljava/lang/Class;)Ljava/lang/reflect/Type;
astore 1
aload 1
astore 0
aload 1
instanceof java/lang/reflect/WildcardType
ifeq L0
aload 1
checkcast java/lang/reflect/WildcardType
invokeinterface java/lang/reflect/WildcardType/getUpperBounds()[Ljava/lang/reflect/Type; 0
iconst_0
aaload
astore 0
L0:
aload 0
instanceof java/lang/reflect/ParameterizedType
ifeq L1
aload 0
checkcast java/lang/reflect/ParameterizedType
invokeinterface java/lang/reflect/ParameterizedType/getActualTypeArguments()[Ljava/lang/reflect/Type; 0
iconst_0
aaload
areturn
L1:
ldc java/lang/Object
areturn
.limit locals 2
.limit stack 3
.end method

.method private static a(Ljava/lang/reflect/Type;Ljava/lang/Class;Ljava/lang/Class;)Ljava/lang/reflect/Type;
aload 1
astore 5
aload 0
astore 1
aload 5
astore 0
L0:
aload 2
aload 0
if_acmpne L1
L2:
aload 1
areturn
L1:
aload 2
invokevirtual java/lang/Class/isInterface()Z
ifeq L3
aload 0
invokevirtual java/lang/Class/getInterfaces()[Ljava/lang/Class;
astore 5
iconst_0
istore 3
aload 5
arraylength
istore 4
L4:
iload 3
iload 4
if_icmpge L3
aload 5
iload 3
aaload
aload 2
if_acmpne L5
aload 0
invokevirtual java/lang/Class/getGenericInterfaces()[Ljava/lang/reflect/Type;
iload 3
aaload
areturn
L5:
aload 2
aload 5
iload 3
aaload
invokevirtual java/lang/Class/isAssignableFrom(Ljava/lang/Class;)Z
ifeq L6
aload 0
invokevirtual java/lang/Class/getGenericInterfaces()[Ljava/lang/reflect/Type;
iload 3
aaload
astore 1
aload 5
iload 3
aaload
astore 0
goto L0
L6:
iload 3
iconst_1
iadd
istore 3
goto L4
L3:
aload 2
astore 1
aload 0
invokevirtual java/lang/Class/isInterface()Z
ifne L2
L7:
aload 2
astore 1
aload 0
ldc java/lang/Object
if_acmpeq L2
aload 0
invokevirtual java/lang/Class/getSuperclass()Ljava/lang/Class;
astore 1
aload 1
aload 2
if_acmpne L8
aload 0
invokevirtual java/lang/Class/getGenericSuperclass()Ljava/lang/reflect/Type;
areturn
L8:
aload 2
aload 1
invokevirtual java/lang/Class/isAssignableFrom(Ljava/lang/Class;)Z
ifeq L9
aload 0
invokevirtual java/lang/Class/getGenericSuperclass()Ljava/lang/reflect/Type;
astore 5
aload 1
astore 0
aload 5
astore 1
goto L0
L9:
aload 1
astore 0
goto L7
.limit locals 6
.limit stack 3
.end method

.method public static a(Ljava/lang/reflect/Type;Ljava/lang/Class;Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;
iconst_0
istore 5
aload 2
astore 7
L0:
aload 7
instanceof java/lang/reflect/TypeVariable
ifeq L1
aload 7
checkcast java/lang/reflect/TypeVariable
astore 8
aload 8
invokeinterface java/lang/reflect/TypeVariable/getGenericDeclaration()Ljava/lang/reflect/GenericDeclaration; 0
astore 2
aload 2
instanceof java/lang/Class
ifeq L2
aload 2
checkcast java/lang/Class
astore 2
L3:
aload 2
ifnull L4
aload 0
aload 1
aload 2
invokestatic com/a/c/b/b/a(Ljava/lang/reflect/Type;Ljava/lang/Class;Ljava/lang/Class;)Ljava/lang/reflect/Type;
astore 7
aload 7
instanceof java/lang/reflect/ParameterizedType
ifeq L4
aload 2
invokevirtual java/lang/Class/getTypeParameters()[Ljava/lang/reflect/TypeVariable;
astore 2
iconst_0
istore 3
L5:
iload 3
aload 2
arraylength
if_icmpge L6
aload 8
aload 2
iload 3
aaload
invokevirtual java/lang/Object/equals(Ljava/lang/Object;)Z
ifeq L7
aload 7
checkcast java/lang/reflect/ParameterizedType
invokeinterface java/lang/reflect/ParameterizedType/getActualTypeArguments()[Ljava/lang/reflect/Type; 0
iload 3
aaload
astore 2
L8:
aload 2
astore 7
aload 2
aload 8
if_acmpne L0
L9:
aload 2
areturn
L2:
aconst_null
astore 2
goto L3
L7:
iload 3
iconst_1
iadd
istore 3
goto L5
L6:
new java/util/NoSuchElementException
dup
invokespecial java/util/NoSuchElementException/<init>()V
athrow
L4:
aload 8
astore 2
goto L8
L1:
aload 7
instanceof java/lang/Class
ifeq L10
aload 7
checkcast java/lang/Class
invokevirtual java/lang/Class/isArray()Z
ifeq L10
aload 7
checkcast java/lang/Class
astore 2
aload 2
invokevirtual java/lang/Class/getComponentType()Ljava/lang/Class;
astore 7
aload 0
aload 1
aload 7
invokestatic com/a/c/b/b/a(Ljava/lang/reflect/Type;Ljava/lang/Class;Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;
astore 0
aload 7
aload 0
if_acmpeq L9
aload 0
invokestatic com/a/c/b/b/f(Ljava/lang/reflect/Type;)Ljava/lang/reflect/GenericArrayType;
areturn
L10:
aload 7
instanceof java/lang/reflect/GenericArrayType
ifeq L11
aload 7
checkcast java/lang/reflect/GenericArrayType
astore 2
aload 2
invokeinterface java/lang/reflect/GenericArrayType/getGenericComponentType()Ljava/lang/reflect/Type; 0
astore 7
aload 0
aload 1
aload 7
invokestatic com/a/c/b/b/a(Ljava/lang/reflect/Type;Ljava/lang/Class;Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;
astore 0
aload 7
aload 0
if_acmpeq L9
aload 0
invokestatic com/a/c/b/b/f(Ljava/lang/reflect/Type;)Ljava/lang/reflect/GenericArrayType;
areturn
L11:
aload 7
instanceof java/lang/reflect/ParameterizedType
ifeq L12
aload 7
checkcast java/lang/reflect/ParameterizedType
astore 8
aload 8
invokeinterface java/lang/reflect/ParameterizedType/getOwnerType()Ljava/lang/reflect/Type; 0
astore 2
aload 0
aload 1
aload 2
invokestatic com/a/c/b/b/a(Ljava/lang/reflect/Type;Ljava/lang/Class;Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;
astore 9
aload 9
aload 2
if_acmpeq L13
iconst_1
istore 3
L14:
aload 8
invokeinterface java/lang/reflect/ParameterizedType/getActualTypeArguments()[Ljava/lang/reflect/Type; 0
astore 7
aload 7
arraylength
istore 6
L15:
iload 5
iload 6
if_icmpge L16
aload 0
aload 1
aload 7
iload 5
aaload
invokestatic com/a/c/b/b/a(Ljava/lang/reflect/Type;Ljava/lang/Class;Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;
astore 10
iload 3
istore 4
aload 7
astore 2
aload 10
aload 7
iload 5
aaload
if_acmpeq L17
iload 3
istore 4
aload 7
astore 2
iload 3
ifne L18
aload 7
invokevirtual [Ljava/lang/reflect/Type;/clone()Ljava/lang/Object;
checkcast [Ljava/lang/reflect/Type;
astore 2
iconst_1
istore 4
L18:
aload 2
iload 5
aload 10
aastore
L17:
iload 5
iconst_1
iadd
istore 5
iload 4
istore 3
aload 2
astore 7
goto L15
L13:
iconst_0
istore 3
goto L14
L16:
aload 8
astore 2
iload 3
ifeq L9
new com/a/c/b/d
dup
aload 9
aload 8
invokeinterface java/lang/reflect/ParameterizedType/getRawType()Ljava/lang/reflect/Type; 0
aload 7
invokespecial com/a/c/b/d/<init>(Ljava/lang/reflect/Type;Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)V
areturn
L12:
aload 7
astore 2
aload 7
instanceof java/lang/reflect/WildcardType
ifeq L9
aload 7
checkcast java/lang/reflect/WildcardType
astore 7
aload 7
invokeinterface java/lang/reflect/WildcardType/getLowerBounds()[Ljava/lang/reflect/Type; 0
astore 8
aload 7
invokeinterface java/lang/reflect/WildcardType/getUpperBounds()[Ljava/lang/reflect/Type; 0
astore 9
aload 8
arraylength
iconst_1
if_icmpne L19
aload 0
aload 1
aload 8
iconst_0
aaload
invokestatic com/a/c/b/b/a(Ljava/lang/reflect/Type;Ljava/lang/Class;Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;
astore 0
aload 7
astore 2
aload 0
aload 8
iconst_0
aaload
if_acmpeq L9
new com/a/c/b/e
dup
iconst_1
anewarray java/lang/reflect/Type
dup
iconst_0
ldc java/lang/Object
aastore
iconst_1
anewarray java/lang/reflect/Type
dup
iconst_0
aload 0
aastore
invokespecial com/a/c/b/e/<init>([Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)V
areturn
L19:
aload 7
astore 2
aload 9
arraylength
iconst_1
if_icmpne L9
aload 0
aload 1
aload 9
iconst_0
aaload
invokestatic com/a/c/b/b/a(Ljava/lang/reflect/Type;Ljava/lang/Class;Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;
astore 0
aload 7
astore 2
aload 0
aload 9
iconst_0
aaload
if_acmpeq L9
getstatic com/a/c/b/b/a [Ljava/lang/reflect/Type;
astore 1
new com/a/c/b/e
dup
iconst_1
anewarray java/lang/reflect/Type
dup
iconst_0
aload 0
aastore
aload 1
invokespecial com/a/c/b/e/<init>([Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)V
areturn
.limit locals 11
.limit stack 7
.end method

.method private static a(Ljava/lang/reflect/Type;Ljava/lang/Class;Ljava/lang/reflect/TypeVariable;)Ljava/lang/reflect/Type;
aload 2
invokeinterface java/lang/reflect/TypeVariable/getGenericDeclaration()Ljava/lang/reflect/GenericDeclaration; 0
astore 4
aload 4
instanceof java/lang/Class
ifeq L0
aload 4
checkcast java/lang/Class
astore 4
L1:
aload 4
ifnonnull L2
L3:
aload 2
areturn
L0:
aconst_null
astore 4
goto L1
L2:
aload 0
aload 1
aload 4
invokestatic com/a/c/b/b/a(Ljava/lang/reflect/Type;Ljava/lang/Class;Ljava/lang/Class;)Ljava/lang/reflect/Type;
astore 0
aload 0
instanceof java/lang/reflect/ParameterizedType
ifeq L3
aload 4
invokevirtual java/lang/Class/getTypeParameters()[Ljava/lang/reflect/TypeVariable;
astore 1
iconst_0
istore 3
L4:
iload 3
aload 1
arraylength
if_icmpge L5
aload 2
aload 1
iload 3
aaload
invokevirtual java/lang/Object/equals(Ljava/lang/Object;)Z
ifeq L6
aload 0
checkcast java/lang/reflect/ParameterizedType
invokeinterface java/lang/reflect/ParameterizedType/getActualTypeArguments()[Ljava/lang/reflect/Type; 0
iload 3
aaload
areturn
L6:
iload 3
iconst_1
iadd
istore 3
goto L4
L5:
new java/util/NoSuchElementException
dup
invokespecial java/util/NoSuchElementException/<init>()V
athrow
.limit locals 5
.limit stack 3
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/Object;)Z
aload 0
aload 1
if_acmpeq L0
aload 0
ifnull L1
aload 0
aload 1
invokevirtual java/lang/Object/equals(Ljava/lang/Object;)Z
ifeq L1
L0:
iconst_1
ireturn
L1:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method public static a(Ljava/lang/reflect/Type;Ljava/lang/reflect/Type;)Z
aload 0
astore 3
aload 1
astore 0
L0:
aload 3
aload 0
if_acmpne L1
iconst_1
ireturn
L1:
aload 3
instanceof java/lang/Class
ifeq L2
aload 3
aload 0
invokevirtual java/lang/Object/equals(Ljava/lang/Object;)Z
ireturn
L2:
aload 3
instanceof java/lang/reflect/ParameterizedType
ifeq L3
aload 0
instanceof java/lang/reflect/ParameterizedType
ifne L4
iconst_0
ireturn
L4:
aload 3
checkcast java/lang/reflect/ParameterizedType
astore 1
aload 0
checkcast java/lang/reflect/ParameterizedType
astore 0
aload 1
invokeinterface java/lang/reflect/ParameterizedType/getOwnerType()Ljava/lang/reflect/Type; 0
astore 3
aload 0
invokeinterface java/lang/reflect/ParameterizedType/getOwnerType()Ljava/lang/reflect/Type; 0
astore 4
aload 3
aload 4
if_acmpeq L5
aload 3
ifnull L6
aload 3
aload 4
invokevirtual java/lang/Object/equals(Ljava/lang/Object;)Z
ifeq L6
L5:
iconst_1
istore 2
L7:
iload 2
ifeq L8
aload 1
invokeinterface java/lang/reflect/ParameterizedType/getRawType()Ljava/lang/reflect/Type; 0
aload 0
invokeinterface java/lang/reflect/ParameterizedType/getRawType()Ljava/lang/reflect/Type; 0
invokevirtual java/lang/Object/equals(Ljava/lang/Object;)Z
ifeq L8
aload 1
invokeinterface java/lang/reflect/ParameterizedType/getActualTypeArguments()[Ljava/lang/reflect/Type; 0
aload 0
invokeinterface java/lang/reflect/ParameterizedType/getActualTypeArguments()[Ljava/lang/reflect/Type; 0
invokestatic java/util/Arrays/equals([Ljava/lang/Object;[Ljava/lang/Object;)Z
ifeq L8
iconst_1
ireturn
L6:
iconst_0
istore 2
goto L7
L8:
iconst_0
ireturn
L3:
aload 3
instanceof java/lang/reflect/GenericArrayType
ifeq L9
aload 0
instanceof java/lang/reflect/GenericArrayType
ifne L10
iconst_0
ireturn
L10:
aload 3
checkcast java/lang/reflect/GenericArrayType
astore 1
aload 0
checkcast java/lang/reflect/GenericArrayType
astore 0
aload 1
invokeinterface java/lang/reflect/GenericArrayType/getGenericComponentType()Ljava/lang/reflect/Type; 0
astore 3
aload 0
invokeinterface java/lang/reflect/GenericArrayType/getGenericComponentType()Ljava/lang/reflect/Type; 0
astore 0
goto L0
L9:
aload 3
instanceof java/lang/reflect/WildcardType
ifeq L11
aload 0
instanceof java/lang/reflect/WildcardType
ifne L12
iconst_0
ireturn
L12:
aload 3
checkcast java/lang/reflect/WildcardType
astore 1
aload 0
checkcast java/lang/reflect/WildcardType
astore 0
aload 1
invokeinterface java/lang/reflect/WildcardType/getUpperBounds()[Ljava/lang/reflect/Type; 0
aload 0
invokeinterface java/lang/reflect/WildcardType/getUpperBounds()[Ljava/lang/reflect/Type; 0
invokestatic java/util/Arrays/equals([Ljava/lang/Object;[Ljava/lang/Object;)Z
ifeq L13
aload 1
invokeinterface java/lang/reflect/WildcardType/getLowerBounds()[Ljava/lang/reflect/Type; 0
aload 0
invokeinterface java/lang/reflect/WildcardType/getLowerBounds()[Ljava/lang/reflect/Type; 0
invokestatic java/util/Arrays/equals([Ljava/lang/Object;[Ljava/lang/Object;)Z
ifeq L13
iconst_1
ireturn
L13:
iconst_0
ireturn
L11:
aload 3
instanceof java/lang/reflect/TypeVariable
ifeq L14
aload 0
instanceof java/lang/reflect/TypeVariable
ifne L15
iconst_0
ireturn
L15:
aload 3
checkcast java/lang/reflect/TypeVariable
astore 1
aload 0
checkcast java/lang/reflect/TypeVariable
astore 0
aload 1
invokeinterface java/lang/reflect/TypeVariable/getGenericDeclaration()Ljava/lang/reflect/GenericDeclaration; 0
aload 0
invokeinterface java/lang/reflect/TypeVariable/getGenericDeclaration()Ljava/lang/reflect/GenericDeclaration; 0
if_acmpne L16
aload 1
invokeinterface java/lang/reflect/TypeVariable/getName()Ljava/lang/String; 0
aload 0
invokeinterface java/lang/reflect/TypeVariable/getName()Ljava/lang/String; 0
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L16
iconst_1
ireturn
L16:
iconst_0
ireturn
L14:
iconst_0
ireturn
.limit locals 5
.limit stack 2
.end method

.method private static b(Ljava/lang/Object;)I
aload 0
ifnull L0
aload 0
invokevirtual java/lang/Object/hashCode()I
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public static b(Ljava/lang/reflect/Type;)Ljava/lang/Class;
L0:
aload 0
instanceof java/lang/Class
ifeq L1
aload 0
checkcast java/lang/Class
areturn
L1:
aload 0
instanceof java/lang/reflect/ParameterizedType
ifeq L2
aload 0
checkcast java/lang/reflect/ParameterizedType
invokeinterface java/lang/reflect/ParameterizedType/getRawType()Ljava/lang/reflect/Type; 0
astore 0
aload 0
instanceof java/lang/Class
invokestatic com/a/c/b/a/a(Z)V
aload 0
checkcast java/lang/Class
areturn
L2:
aload 0
instanceof java/lang/reflect/GenericArrayType
ifeq L3
aload 0
checkcast java/lang/reflect/GenericArrayType
invokeinterface java/lang/reflect/GenericArrayType/getGenericComponentType()Ljava/lang/reflect/Type; 0
invokestatic com/a/c/b/b/b(Ljava/lang/reflect/Type;)Ljava/lang/Class;
iconst_0
invokestatic java/lang/reflect/Array/newInstance(Ljava/lang/Class;I)Ljava/lang/Object;
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
areturn
L3:
aload 0
instanceof java/lang/reflect/TypeVariable
ifeq L4
ldc java/lang/Object
areturn
L4:
aload 0
instanceof java/lang/reflect/WildcardType
ifeq L5
aload 0
checkcast java/lang/reflect/WildcardType
invokeinterface java/lang/reflect/WildcardType/getUpperBounds()[Ljava/lang/reflect/Type; 0
iconst_0
aaload
astore 0
goto L0
L5:
aload 0
ifnonnull L6
ldc "null"
astore 1
L7:
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
ldc "Expected a Class, ParameterizedType, or GenericArrayType, but <"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc "> is of type "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L6:
aload 0
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
invokevirtual java/lang/Class/getName()Ljava/lang/String;
astore 1
goto L7
.limit locals 2
.limit stack 5
.end method

.method private static b(Ljava/lang/reflect/Type;Ljava/lang/Class;Ljava/lang/Class;)Ljava/lang/reflect/Type;
aload 2
aload 1
invokevirtual java/lang/Class/isAssignableFrom(Ljava/lang/Class;)Z
invokestatic com/a/c/b/a/a(Z)V
aload 0
aload 1
aload 0
aload 1
aload 2
invokestatic com/a/c/b/b/a(Ljava/lang/reflect/Type;Ljava/lang/Class;Ljava/lang/Class;)Ljava/lang/reflect/Type;
invokestatic com/a/c/b/b/a(Ljava/lang/reflect/Type;Ljava/lang/Class;Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;
areturn
.limit locals 3
.limit stack 5
.end method

.method public static b(Ljava/lang/reflect/Type;Ljava/lang/Class;)[Ljava/lang/reflect/Type;
aload 0
ldc java/util/Properties
if_acmpne L0
iconst_2
anewarray java/lang/reflect/Type
dup
iconst_0
ldc java/lang/String
aastore
dup
iconst_1
ldc java/lang/String
aastore
areturn
L0:
aload 0
aload 1
ldc java/util/Map
invokestatic com/a/c/b/b/b(Ljava/lang/reflect/Type;Ljava/lang/Class;Ljava/lang/Class;)Ljava/lang/reflect/Type;
astore 0
aload 0
instanceof java/lang/reflect/ParameterizedType
ifeq L1
aload 0
checkcast java/lang/reflect/ParameterizedType
invokeinterface java/lang/reflect/ParameterizedType/getActualTypeArguments()[Ljava/lang/reflect/Type; 0
areturn
L1:
iconst_2
anewarray java/lang/reflect/Type
dup
iconst_0
ldc java/lang/Object
aastore
dup
iconst_1
ldc java/lang/Object
aastore
areturn
.limit locals 2
.limit stack 4
.end method

.method public static c(Ljava/lang/reflect/Type;)Ljava/lang/String;
aload 0
instanceof java/lang/Class
ifeq L0
aload 0
checkcast java/lang/Class
invokevirtual java/lang/Class/getName()Ljava/lang/String;
areturn
L0:
aload 0
invokevirtual java/lang/Object/toString()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method public static d(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;
aload 0
instanceof java/lang/reflect/GenericArrayType
ifeq L0
aload 0
checkcast java/lang/reflect/GenericArrayType
invokeinterface java/lang/reflect/GenericArrayType/getGenericComponentType()Ljava/lang/reflect/Type; 0
areturn
L0:
aload 0
checkcast java/lang/Class
invokevirtual java/lang/Class/getComponentType()Ljava/lang/Class;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic e(Ljava/lang/reflect/Type;)V
aload 0
instanceof java/lang/Class
ifeq L0
aload 0
checkcast java/lang/Class
invokevirtual java/lang/Class/isPrimitive()Z
ifne L1
L0:
iconst_1
istore 1
L2:
iload 1
invokestatic com/a/c/b/a/a(Z)V
return
L1:
iconst_0
istore 1
goto L2
.limit locals 2
.limit stack 1
.end method

.method private static f(Ljava/lang/reflect/Type;)Ljava/lang/reflect/GenericArrayType;
new com/a/c/b/c
dup
aload 0
invokespecial com/a/c/b/c/<init>(Ljava/lang/reflect/Type;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private static g(Ljava/lang/reflect/Type;)Ljava/lang/reflect/WildcardType;
getstatic com/a/c/b/b/a [Ljava/lang/reflect/Type;
astore 1
new com/a/c/b/e
dup
iconst_1
anewarray java/lang/reflect/Type
dup
iconst_0
aload 0
aastore
aload 1
invokespecial com/a/c/b/e/<init>([Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)V
areturn
.limit locals 2
.limit stack 6
.end method

.method private static h(Ljava/lang/reflect/Type;)Ljava/lang/reflect/WildcardType;
new com/a/c/b/e
dup
iconst_1
anewarray java/lang/reflect/Type
dup
iconst_0
ldc java/lang/Object
aastore
iconst_1
anewarray java/lang/reflect/Type
dup
iconst_0
aload 0
aastore
invokespecial com/a/c/b/e/<init>([Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)V
areturn
.limit locals 1
.limit stack 7
.end method

.method private static i(Ljava/lang/reflect/Type;)V
aload 0
instanceof java/lang/Class
ifeq L0
aload 0
checkcast java/lang/Class
invokevirtual java/lang/Class/isPrimitive()Z
ifne L1
L0:
iconst_1
istore 1
L2:
iload 1
invokestatic com/a/c/b/a/a(Z)V
return
L1:
iconst_0
istore 1
goto L2
.limit locals 2
.limit stack 1
.end method
