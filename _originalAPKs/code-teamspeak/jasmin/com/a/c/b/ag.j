.bytecode 50.0
.class public final synchronized com/a/c/b/ag
.super java/util/AbstractMap
.implements java/io/Serializable

.field static final synthetic 'f' Z

.field private static final 'g' Ljava/util/Comparator;

.field 'a' Ljava/util/Comparator;

.field 'b' Lcom/a/c/b/an;

.field 'c' I

.field 'd' I

.field final 'e' Lcom/a/c/b/an;

.field private 'h' Lcom/a/c/b/ai;

.field private 'i' Lcom/a/c/b/ak;

.method static <clinit>()V
ldc com/a/c/b/ag
invokevirtual java/lang/Class/desiredAssertionStatus()Z
ifne L0
iconst_1
istore 0
L1:
iload 0
putstatic com/a/c/b/ag/f Z
new com/a/c/b/ah
dup
invokespecial com/a/c/b/ah/<init>()V
putstatic com/a/c/b/ag/g Ljava/util/Comparator;
return
L0:
iconst_0
istore 0
goto L1
.limit locals 1
.limit stack 2
.end method

.method public <init>()V
aload 0
getstatic com/a/c/b/ag/g Ljava/util/Comparator;
invokespecial com/a/c/b/ag/<init>(Ljava/util/Comparator;)V
return
.limit locals 1
.limit stack 2
.end method

.method private <init>(Ljava/util/Comparator;)V
aload 0
invokespecial java/util/AbstractMap/<init>()V
aload 0
iconst_0
putfield com/a/c/b/ag/c I
aload 0
iconst_0
putfield com/a/c/b/ag/d I
aload 0
new com/a/c/b/an
dup
invokespecial com/a/c/b/an/<init>()V
putfield com/a/c/b/ag/e Lcom/a/c/b/an;
aload 1
ifnull L0
L1:
aload 0
aload 1
putfield com/a/c/b/ag/a Ljava/util/Comparator;
return
L0:
getstatic com/a/c/b/ag/g Ljava/util/Comparator;
astore 1
goto L1
.limit locals 2
.limit stack 3
.end method

.method private a(Ljava/lang/Object;Z)Lcom/a/c/b/an;
aconst_null
astore 7
aload 0
getfield com/a/c/b/ag/a Ljava/util/Comparator;
astore 8
aload 0
getfield com/a/c/b/ag/b Lcom/a/c/b/an;
astore 4
aload 4
ifnull L0
aload 8
getstatic com/a/c/b/ag/g Ljava/util/Comparator;
if_acmpne L1
aload 1
checkcast java/lang/Comparable
astore 6
L2:
aload 6
ifnull L3
aload 6
aload 4
getfield com/a/c/b/an/f Ljava/lang/Object;
invokeinterface java/lang/Comparable/compareTo(Ljava/lang/Object;)I 1
istore 3
L4:
iload 3
ifne L5
aload 4
astore 5
L6:
aload 5
areturn
L1:
aconst_null
astore 6
goto L2
L3:
aload 8
aload 1
aload 4
getfield com/a/c/b/an/f Ljava/lang/Object;
invokeinterface java/util/Comparator/compare(Ljava/lang/Object;Ljava/lang/Object;)I 2
istore 3
goto L4
L5:
iload 3
ifge L7
aload 4
getfield com/a/c/b/an/b Lcom/a/c/b/an;
astore 5
L8:
aload 5
ifnull L9
aload 5
astore 4
goto L2
L7:
aload 4
getfield com/a/c/b/an/c Lcom/a/c/b/an;
astore 5
goto L8
L9:
aload 7
astore 5
iload 2
ifeq L6
aload 0
getfield com/a/c/b/ag/e Lcom/a/c/b/an;
astore 5
aload 4
ifnonnull L10
aload 8
getstatic com/a/c/b/ag/g Ljava/util/Comparator;
if_acmpne L11
aload 1
instanceof java/lang/Comparable
ifne L11
new java/lang/ClassCastException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 1
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
invokevirtual java/lang/Class/getName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " is not Comparable"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/ClassCastException/<init>(Ljava/lang/String;)V
athrow
L11:
new com/a/c/b/an
dup
aload 4
aload 1
aload 5
aload 5
getfield com/a/c/b/an/e Lcom/a/c/b/an;
invokespecial com/a/c/b/an/<init>(Lcom/a/c/b/an;Ljava/lang/Object;Lcom/a/c/b/an;Lcom/a/c/b/an;)V
astore 1
aload 0
aload 1
putfield com/a/c/b/ag/b Lcom/a/c/b/an;
L12:
aload 0
aload 0
getfield com/a/c/b/ag/c I
iconst_1
iadd
putfield com/a/c/b/ag/c I
aload 0
aload 0
getfield com/a/c/b/ag/d I
iconst_1
iadd
putfield com/a/c/b/ag/d I
aload 1
areturn
L10:
new com/a/c/b/an
dup
aload 4
aload 1
aload 5
aload 5
getfield com/a/c/b/an/e Lcom/a/c/b/an;
invokespecial com/a/c/b/an/<init>(Lcom/a/c/b/an;Ljava/lang/Object;Lcom/a/c/b/an;Lcom/a/c/b/an;)V
astore 1
iload 3
ifge L13
aload 4
aload 1
putfield com/a/c/b/an/b Lcom/a/c/b/an;
L14:
aload 0
aload 4
iconst_1
invokespecial com/a/c/b/ag/b(Lcom/a/c/b/an;Z)V
goto L12
L13:
aload 4
aload 1
putfield com/a/c/b/an/c Lcom/a/c/b/an;
goto L14
L0:
iconst_0
istore 3
goto L9
.limit locals 9
.limit stack 6
.end method

.method private a()Ljava/lang/Object;
new java/util/LinkedHashMap
dup
aload 0
invokespecial java/util/LinkedHashMap/<init>(Ljava/util/Map;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private a(Lcom/a/c/b/an;)V
iconst_0
istore 4
aload 1
getfield com/a/c/b/an/b Lcom/a/c/b/an;
astore 5
aload 1
getfield com/a/c/b/an/c Lcom/a/c/b/an;
astore 6
aload 6
getfield com/a/c/b/an/b Lcom/a/c/b/an;
astore 7
aload 6
getfield com/a/c/b/an/c Lcom/a/c/b/an;
astore 8
aload 1
aload 7
putfield com/a/c/b/an/c Lcom/a/c/b/an;
aload 7
ifnull L0
aload 7
aload 1
putfield com/a/c/b/an/a Lcom/a/c/b/an;
L0:
aload 0
aload 1
aload 6
invokespecial com/a/c/b/ag/a(Lcom/a/c/b/an;Lcom/a/c/b/an;)V
aload 6
aload 1
putfield com/a/c/b/an/b Lcom/a/c/b/an;
aload 1
aload 6
putfield com/a/c/b/an/a Lcom/a/c/b/an;
aload 5
ifnull L1
aload 5
getfield com/a/c/b/an/h I
istore 2
L2:
aload 7
ifnull L3
aload 7
getfield com/a/c/b/an/h I
istore 3
L4:
aload 1
iload 2
iload 3
invokestatic java/lang/Math/max(II)I
iconst_1
iadd
putfield com/a/c/b/an/h I
aload 1
getfield com/a/c/b/an/h I
istore 3
iload 4
istore 2
aload 8
ifnull L5
aload 8
getfield com/a/c/b/an/h I
istore 2
L5:
aload 6
iload 3
iload 2
invokestatic java/lang/Math/max(II)I
iconst_1
iadd
putfield com/a/c/b/an/h I
return
L1:
iconst_0
istore 2
goto L2
L3:
iconst_0
istore 3
goto L4
.limit locals 9
.limit stack 3
.end method

.method private a(Lcom/a/c/b/an;Lcom/a/c/b/an;)V
aload 1
getfield com/a/c/b/an/a Lcom/a/c/b/an;
astore 3
aload 1
aconst_null
putfield com/a/c/b/an/a Lcom/a/c/b/an;
aload 2
ifnull L0
aload 2
aload 3
putfield com/a/c/b/an/a Lcom/a/c/b/an;
L0:
aload 3
ifnull L1
aload 3
getfield com/a/c/b/an/b Lcom/a/c/b/an;
aload 1
if_acmpne L2
aload 3
aload 2
putfield com/a/c/b/an/b Lcom/a/c/b/an;
return
L2:
getstatic com/a/c/b/ag/f Z
ifne L3
aload 3
getfield com/a/c/b/an/c Lcom/a/c/b/an;
aload 1
if_acmpeq L3
new java/lang/AssertionError
dup
invokespecial java/lang/AssertionError/<init>()V
athrow
L3:
aload 3
aload 2
putfield com/a/c/b/an/c Lcom/a/c/b/an;
return
L1:
aload 0
aload 2
putfield com/a/c/b/ag/b Lcom/a/c/b/an;
return
.limit locals 4
.limit stack 2
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/Object;)Z
aload 0
aload 1
if_acmpeq L0
aload 0
ifnull L1
aload 0
aload 1
invokevirtual java/lang/Object/equals(Ljava/lang/Object;)Z
ifeq L1
L0:
iconst_1
ireturn
L1:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method private b(Ljava/lang/Object;)Lcom/a/c/b/an;
.catch java/lang/ClassCastException from L0 to L1 using L2
aconst_null
astore 2
aload 1
ifnull L1
L0:
aload 0
aload 1
iconst_0
invokespecial com/a/c/b/ag/a(Ljava/lang/Object;Z)Lcom/a/c/b/an;
astore 2
L1:
aload 2
areturn
L2:
astore 1
aconst_null
areturn
.limit locals 3
.limit stack 3
.end method

.method private b(Lcom/a/c/b/an;)V
iconst_0
istore 4
aload 1
getfield com/a/c/b/an/b Lcom/a/c/b/an;
astore 5
aload 1
getfield com/a/c/b/an/c Lcom/a/c/b/an;
astore 6
aload 5
getfield com/a/c/b/an/b Lcom/a/c/b/an;
astore 7
aload 5
getfield com/a/c/b/an/c Lcom/a/c/b/an;
astore 8
aload 1
aload 8
putfield com/a/c/b/an/b Lcom/a/c/b/an;
aload 8
ifnull L0
aload 8
aload 1
putfield com/a/c/b/an/a Lcom/a/c/b/an;
L0:
aload 0
aload 1
aload 5
invokespecial com/a/c/b/ag/a(Lcom/a/c/b/an;Lcom/a/c/b/an;)V
aload 5
aload 1
putfield com/a/c/b/an/c Lcom/a/c/b/an;
aload 1
aload 5
putfield com/a/c/b/an/a Lcom/a/c/b/an;
aload 6
ifnull L1
aload 6
getfield com/a/c/b/an/h I
istore 2
L2:
aload 8
ifnull L3
aload 8
getfield com/a/c/b/an/h I
istore 3
L4:
aload 1
iload 2
iload 3
invokestatic java/lang/Math/max(II)I
iconst_1
iadd
putfield com/a/c/b/an/h I
aload 1
getfield com/a/c/b/an/h I
istore 3
iload 4
istore 2
aload 7
ifnull L5
aload 7
getfield com/a/c/b/an/h I
istore 2
L5:
aload 5
iload 3
iload 2
invokestatic java/lang/Math/max(II)I
iconst_1
iadd
putfield com/a/c/b/an/h I
return
L1:
iconst_0
istore 2
goto L2
L3:
iconst_0
istore 3
goto L4
.limit locals 9
.limit stack 3
.end method

.method private b(Lcom/a/c/b/an;Z)V
L0:
aload 1
ifnull L1
aload 1
getfield com/a/c/b/an/b Lcom/a/c/b/an;
astore 6
aload 1
getfield com/a/c/b/an/c Lcom/a/c/b/an;
astore 7
aload 6
ifnull L2
aload 6
getfield com/a/c/b/an/h I
istore 3
L3:
aload 7
ifnull L4
aload 7
getfield com/a/c/b/an/h I
istore 4
L5:
iload 3
iload 4
isub
istore 5
iload 5
bipush -2
if_icmpne L6
aload 7
getfield com/a/c/b/an/b Lcom/a/c/b/an;
astore 6
aload 7
getfield com/a/c/b/an/c Lcom/a/c/b/an;
astore 8
aload 8
ifnull L7
aload 8
getfield com/a/c/b/an/h I
istore 3
L8:
aload 6
ifnull L9
aload 6
getfield com/a/c/b/an/h I
istore 4
L10:
iload 4
iload 3
isub
istore 3
iload 3
iconst_m1
if_icmpeq L11
iload 3
ifne L12
iload 2
ifne L12
L11:
aload 0
aload 1
invokespecial com/a/c/b/ag/a(Lcom/a/c/b/an;)V
L13:
iload 2
ifne L1
L14:
aload 1
getfield com/a/c/b/an/a Lcom/a/c/b/an;
astore 1
goto L0
L2:
iconst_0
istore 3
goto L3
L4:
iconst_0
istore 4
goto L5
L7:
iconst_0
istore 3
goto L8
L9:
iconst_0
istore 4
goto L10
L12:
getstatic com/a/c/b/ag/f Z
ifne L15
iload 3
iconst_1
if_icmpeq L15
new java/lang/AssertionError
dup
invokespecial java/lang/AssertionError/<init>()V
athrow
L15:
aload 0
aload 7
invokespecial com/a/c/b/ag/b(Lcom/a/c/b/an;)V
aload 0
aload 1
invokespecial com/a/c/b/ag/a(Lcom/a/c/b/an;)V
goto L13
L6:
iload 5
iconst_2
if_icmpne L16
aload 6
getfield com/a/c/b/an/b Lcom/a/c/b/an;
astore 7
aload 6
getfield com/a/c/b/an/c Lcom/a/c/b/an;
astore 8
aload 8
ifnull L17
aload 8
getfield com/a/c/b/an/h I
istore 3
L18:
aload 7
ifnull L19
aload 7
getfield com/a/c/b/an/h I
istore 4
L20:
iload 4
iload 3
isub
istore 3
iload 3
iconst_1
if_icmpeq L21
iload 3
ifne L22
iload 2
ifne L22
L21:
aload 0
aload 1
invokespecial com/a/c/b/ag/b(Lcom/a/c/b/an;)V
L23:
iload 2
ifeq L14
L1:
return
L17:
iconst_0
istore 3
goto L18
L19:
iconst_0
istore 4
goto L20
L22:
getstatic com/a/c/b/ag/f Z
ifne L24
iload 3
iconst_m1
if_icmpeq L24
new java/lang/AssertionError
dup
invokespecial java/lang/AssertionError/<init>()V
athrow
L24:
aload 0
aload 6
invokespecial com/a/c/b/ag/a(Lcom/a/c/b/an;)V
aload 0
aload 1
invokespecial com/a/c/b/ag/b(Lcom/a/c/b/an;)V
goto L23
L16:
iload 5
ifne L25
aload 1
iload 3
iconst_1
iadd
putfield com/a/c/b/an/h I
iload 2
ifeq L14
return
L25:
getstatic com/a/c/b/ag/f Z
ifne L26
iload 5
iconst_m1
if_icmpeq L26
iload 5
iconst_1
if_icmpeq L26
new java/lang/AssertionError
dup
invokespecial java/lang/AssertionError/<init>()V
athrow
L26:
aload 1
iload 3
iload 4
invokestatic java/lang/Math/max(II)I
iconst_1
iadd
putfield com/a/c/b/an/h I
iload 2
ifeq L1
goto L14
.limit locals 9
.limit stack 3
.end method

.method final a(Ljava/lang/Object;)Lcom/a/c/b/an;
aload 0
aload 1
invokespecial com/a/c/b/ag/b(Ljava/lang/Object;)Lcom/a/c/b/an;
astore 1
aload 1
ifnull L0
aload 0
aload 1
iconst_1
invokevirtual com/a/c/b/ag/a(Lcom/a/c/b/an;Z)V
L0:
aload 1
areturn
.limit locals 2
.limit stack 3
.end method

.method final a(Ljava/util/Map$Entry;)Lcom/a/c/b/an;
iconst_1
istore 3
aload 0
aload 1
invokeinterface java/util/Map$Entry/getKey()Ljava/lang/Object; 0
invokespecial com/a/c/b/ag/b(Ljava/lang/Object;)Lcom/a/c/b/an;
astore 4
aload 4
ifnull L0
aload 4
getfield com/a/c/b/an/g Ljava/lang/Object;
astore 5
aload 1
invokeinterface java/util/Map$Entry/getValue()Ljava/lang/Object; 0
astore 1
aload 5
aload 1
if_acmpeq L1
aload 5
ifnull L2
aload 5
aload 1
invokevirtual java/lang/Object/equals(Ljava/lang/Object;)Z
ifeq L2
L1:
iconst_1
istore 2
L3:
iload 2
ifeq L0
iload 3
istore 2
L4:
iload 2
ifeq L5
aload 4
areturn
L2:
iconst_0
istore 2
goto L3
L0:
iconst_0
istore 2
goto L4
L5:
aconst_null
areturn
.limit locals 6
.limit stack 2
.end method

.method final a(Lcom/a/c/b/an;Z)V
iconst_0
istore 4
iload 2
ifeq L0
aload 1
getfield com/a/c/b/an/e Lcom/a/c/b/an;
aload 1
getfield com/a/c/b/an/d Lcom/a/c/b/an;
putfield com/a/c/b/an/d Lcom/a/c/b/an;
aload 1
getfield com/a/c/b/an/d Lcom/a/c/b/an;
aload 1
getfield com/a/c/b/an/e Lcom/a/c/b/an;
putfield com/a/c/b/an/e Lcom/a/c/b/an;
L0:
aload 1
getfield com/a/c/b/an/b Lcom/a/c/b/an;
astore 6
aload 1
getfield com/a/c/b/an/c Lcom/a/c/b/an;
astore 7
aload 1
getfield com/a/c/b/an/a Lcom/a/c/b/an;
astore 5
aload 6
ifnull L1
aload 7
ifnull L1
aload 7
astore 5
aload 6
getfield com/a/c/b/an/h I
aload 7
getfield com/a/c/b/an/h I
if_icmple L2
aload 6
getfield com/a/c/b/an/c Lcom/a/c/b/an;
astore 5
L3:
aload 5
ifnull L4
aload 5
getfield com/a/c/b/an/c Lcom/a/c/b/an;
astore 7
aload 5
astore 6
aload 7
astore 5
goto L3
L5:
aload 6
astore 5
L2:
aload 5
getfield com/a/c/b/an/b Lcom/a/c/b/an;
astore 6
aload 6
ifnonnull L5
aload 5
astore 6
L4:
aload 0
aload 6
iconst_0
invokevirtual com/a/c/b/ag/a(Lcom/a/c/b/an;Z)V
aload 1
getfield com/a/c/b/an/b Lcom/a/c/b/an;
astore 5
aload 5
ifnull L6
aload 5
getfield com/a/c/b/an/h I
istore 3
aload 6
aload 5
putfield com/a/c/b/an/b Lcom/a/c/b/an;
aload 5
aload 6
putfield com/a/c/b/an/a Lcom/a/c/b/an;
aload 1
aconst_null
putfield com/a/c/b/an/b Lcom/a/c/b/an;
L7:
aload 1
getfield com/a/c/b/an/c Lcom/a/c/b/an;
astore 5
aload 5
ifnull L8
aload 5
getfield com/a/c/b/an/h I
istore 4
aload 6
aload 5
putfield com/a/c/b/an/c Lcom/a/c/b/an;
aload 5
aload 6
putfield com/a/c/b/an/a Lcom/a/c/b/an;
aload 1
aconst_null
putfield com/a/c/b/an/c Lcom/a/c/b/an;
L8:
aload 6
iload 3
iload 4
invokestatic java/lang/Math/max(II)I
iconst_1
iadd
putfield com/a/c/b/an/h I
aload 0
aload 1
aload 6
invokespecial com/a/c/b/ag/a(Lcom/a/c/b/an;Lcom/a/c/b/an;)V
return
L1:
aload 6
ifnull L9
aload 0
aload 1
aload 6
invokespecial com/a/c/b/ag/a(Lcom/a/c/b/an;Lcom/a/c/b/an;)V
aload 1
aconst_null
putfield com/a/c/b/an/b Lcom/a/c/b/an;
L10:
aload 0
aload 5
iconst_0
invokespecial com/a/c/b/ag/b(Lcom/a/c/b/an;Z)V
aload 0
aload 0
getfield com/a/c/b/ag/c I
iconst_1
isub
putfield com/a/c/b/ag/c I
aload 0
aload 0
getfield com/a/c/b/ag/d I
iconst_1
iadd
putfield com/a/c/b/ag/d I
return
L9:
aload 7
ifnull L11
aload 0
aload 1
aload 7
invokespecial com/a/c/b/ag/a(Lcom/a/c/b/an;Lcom/a/c/b/an;)V
aload 1
aconst_null
putfield com/a/c/b/an/c Lcom/a/c/b/an;
goto L10
L11:
aload 0
aload 1
aconst_null
invokespecial com/a/c/b/ag/a(Lcom/a/c/b/an;Lcom/a/c/b/an;)V
goto L10
L6:
iconst_0
istore 3
goto L7
.limit locals 8
.limit stack 3
.end method

.method public final clear()V
aload 0
aconst_null
putfield com/a/c/b/ag/b Lcom/a/c/b/an;
aload 0
iconst_0
putfield com/a/c/b/ag/c I
aload 0
aload 0
getfield com/a/c/b/ag/d I
iconst_1
iadd
putfield com/a/c/b/ag/d I
aload 0
getfield com/a/c/b/ag/e Lcom/a/c/b/an;
astore 1
aload 1
aload 1
putfield com/a/c/b/an/e Lcom/a/c/b/an;
aload 1
aload 1
putfield com/a/c/b/an/d Lcom/a/c/b/an;
return
.limit locals 2
.limit stack 3
.end method

.method public final containsKey(Ljava/lang/Object;)Z
aload 0
aload 1
invokespecial com/a/c/b/ag/b(Ljava/lang/Object;)Lcom/a/c/b/an;
ifnull L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final entrySet()Ljava/util/Set;
aload 0
getfield com/a/c/b/ag/h Lcom/a/c/b/ai;
astore 1
aload 1
ifnull L0
aload 1
areturn
L0:
new com/a/c/b/ai
dup
aload 0
invokespecial com/a/c/b/ai/<init>(Lcom/a/c/b/ag;)V
astore 1
aload 0
aload 1
putfield com/a/c/b/ag/h Lcom/a/c/b/ai;
aload 1
areturn
.limit locals 2
.limit stack 3
.end method

.method public final get(Ljava/lang/Object;)Ljava/lang/Object;
aload 0
aload 1
invokespecial com/a/c/b/ag/b(Ljava/lang/Object;)Lcom/a/c/b/an;
astore 1
aload 1
ifnull L0
aload 1
getfield com/a/c/b/an/g Ljava/lang/Object;
areturn
L0:
aconst_null
areturn
.limit locals 2
.limit stack 2
.end method

.method public final keySet()Ljava/util/Set;
aload 0
getfield com/a/c/b/ag/i Lcom/a/c/b/ak;
astore 1
aload 1
ifnull L0
aload 1
areturn
L0:
new com/a/c/b/ak
dup
aload 0
invokespecial com/a/c/b/ak/<init>(Lcom/a/c/b/ag;)V
astore 1
aload 0
aload 1
putfield com/a/c/b/ag/i Lcom/a/c/b/ak;
aload 1
areturn
.limit locals 2
.limit stack 3
.end method

.method public final put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
aload 1
ifnonnull L0
new java/lang/NullPointerException
dup
ldc "key == null"
invokespecial java/lang/NullPointerException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
aload 1
iconst_1
invokespecial com/a/c/b/ag/a(Ljava/lang/Object;Z)Lcom/a/c/b/an;
astore 1
aload 1
getfield com/a/c/b/an/g Ljava/lang/Object;
astore 3
aload 1
aload 2
putfield com/a/c/b/an/g Ljava/lang/Object;
aload 3
areturn
.limit locals 4
.limit stack 3
.end method

.method public final remove(Ljava/lang/Object;)Ljava/lang/Object;
aload 0
aload 1
invokevirtual com/a/c/b/ag/a(Ljava/lang/Object;)Lcom/a/c/b/an;
astore 1
aload 1
ifnull L0
aload 1
getfield com/a/c/b/an/g Ljava/lang/Object;
areturn
L0:
aconst_null
areturn
.limit locals 2
.limit stack 2
.end method

.method public final size()I
aload 0
getfield com/a/c/b/ag/c I
ireturn
.limit locals 1
.limit stack 1
.end method
