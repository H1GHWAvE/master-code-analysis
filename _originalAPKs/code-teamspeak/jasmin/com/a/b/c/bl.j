.bytecode 50.0
.class final synchronized com/a/b/c/bl
.super java/lang/Object
.implements com/a/b/c/cg

.field volatile 'a' Lcom/a/b/c/cg;

.field final 'b' Lcom/a/b/n/a/fq;

.field final 'c' Lcom/a/b/b/dw;

.method public <init>()V
aload 0
invokestatic com/a/b/c/ao/j()Lcom/a/b/c/cg;
invokespecial com/a/b/c/bl/<init>(Lcom/a/b/c/cg;)V
return
.limit locals 1
.limit stack 2
.end method

.method public <init>(Lcom/a/b/c/cg;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
invokestatic com/a/b/n/a/fq/a()Lcom/a/b/n/a/fq;
putfield com/a/b/c/bl/b Lcom/a/b/n/a/fq;
aload 0
new com/a/b/b/dw
dup
invokespecial com/a/b/b/dw/<init>()V
putfield com/a/b/c/bl/c Lcom/a/b/b/dw;
aload 0
aload 1
putfield com/a/b/c/bl/a Lcom/a/b/c/cg;
return
.limit locals 2
.limit stack 3
.end method

.method private static b(Ljava/lang/Throwable;)Lcom/a/b/n/a/dp;
aload 0
invokestatic com/a/b/n/a/ci/a(Ljava/lang/Throwable;)Lcom/a/b/n/a/dp;
areturn
.limit locals 1
.limit stack 1
.end method

.method private g()Lcom/a/b/c/cg;
aload 0
getfield com/a/b/c/bl/a Lcom/a/b/c/cg;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final a()I
aload 0
getfield com/a/b/c/bl/a Lcom/a/b/c/cg;
invokeinterface com/a/b/c/cg/a()I 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final a(Ljava/lang/ref/ReferenceQueue;Ljava/lang/Object;Lcom/a/b/c/bs;)Lcom/a/b/c/cg;
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
aload 0
areturn
.limit locals 4
.limit stack 1
.end method

.method public final a(Ljava/lang/Object;Lcom/a/b/c/ab;)Lcom/a/b/n/a/dp;
.catch java/lang/Throwable from L0 to L1 using L2
.catch java/lang/Throwable from L1 to L3 using L2
.catch java/lang/Throwable from L3 to L4 using L2
.catch java/lang/Throwable from L5 to L6 using L2
.catch java/lang/Throwable from L6 to L7 using L2
aload 0
getfield com/a/b/c/bl/c Lcom/a/b/b/dw;
invokevirtual com/a/b/b/dw/b()Lcom/a/b/b/dw;
pop
aload 0
getfield com/a/b/c/bl/a Lcom/a/b/c/cg;
invokeinterface com/a/b/c/cg/get()Ljava/lang/Object; 0
astore 3
aload 3
ifnonnull L3
L0:
aload 2
aload 1
invokevirtual com/a/b/c/ab/a(Ljava/lang/Object;)Ljava/lang/Object;
astore 1
aload 0
aload 1
invokevirtual com/a/b/c/bl/b(Ljava/lang/Object;)Z
ifeq L1
aload 0
getfield com/a/b/c/bl/b Lcom/a/b/n/a/fq;
areturn
L1:
aload 1
invokestatic com/a/b/n/a/ci/a(Ljava/lang/Object;)Lcom/a/b/n/a/dp;
areturn
L3:
aload 2
aload 1
aload 3
invokevirtual com/a/b/c/ab/a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/n/a/dp;
astore 1
L4:
aload 1
ifnonnull L6
L5:
aconst_null
invokestatic com/a/b/n/a/ci/a(Ljava/lang/Object;)Lcom/a/b/n/a/dp;
areturn
L6:
aload 1
new com/a/b/c/bm
dup
aload 0
invokespecial com/a/b/c/bm/<init>(Lcom/a/b/c/bl;)V
invokestatic com/a/b/n/a/ci/a(Lcom/a/b/n/a/dp;Lcom/a/b/b/bj;)Lcom/a/b/n/a/dp;
astore 1
L7:
aload 1
areturn
L2:
astore 1
aload 1
instanceof java/lang/InterruptedException
ifeq L8
invokestatic java/lang/Thread/currentThread()Ljava/lang/Thread;
invokevirtual java/lang/Thread/interrupt()V
L8:
aload 0
aload 1
invokevirtual com/a/b/c/bl/a(Ljava/lang/Throwable;)Z
ifeq L9
aload 0
getfield com/a/b/c/bl/b Lcom/a/b/n/a/fq;
areturn
L9:
aload 1
invokestatic com/a/b/n/a/ci/a(Ljava/lang/Throwable;)Lcom/a/b/n/a/dp;
areturn
.limit locals 4
.limit stack 4
.end method

.method public final a(Ljava/lang/Object;)V
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 1
ifnull L0
aload 0
aload 1
invokevirtual com/a/b/c/bl/b(Ljava/lang/Object;)Z
pop
return
L0:
aload 0
invokestatic com/a/b/c/ao/j()Lcom/a/b/c/cg;
putfield com/a/b/c/bl/a Lcom/a/b/c/cg;
return
.limit locals 2
.limit stack 2
.end method

.method public final a(Ljava/lang/Throwable;)Z
aload 0
getfield com/a/b/c/bl/b Lcom/a/b/n/a/fq;
aload 1
invokevirtual com/a/b/n/a/fq/a(Ljava/lang/Throwable;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final b()Lcom/a/b/c/bs;
aconst_null
areturn
.limit locals 1
.limit stack 1
.end method

.method public final b(Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
getfield com/a/b/c/bl/b Lcom/a/b/n/a/fq;
aload 1
invokevirtual com/a/b/n/a/fq/a(Ljava/lang/Object;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final c()Z
iconst_1
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final d()Z
aload 0
getfield com/a/b/c/bl/a Lcom/a/b/c/cg;
invokeinterface com/a/b/c/cg/d()Z 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final e()Ljava/lang/Object;
aload 0
getfield com/a/b/c/bl/b Lcom/a/b/n/a/fq;
invokestatic com/a/b/n/a/gs/a(Ljava/util/concurrent/Future;)Ljava/lang/Object;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final f()J
aload 0
getfield com/a/b/c/bl/c Lcom/a/b/b/dw;
getstatic java/util/concurrent/TimeUnit/NANOSECONDS Ljava/util/concurrent/TimeUnit;
invokevirtual com/a/b/b/dw/a(Ljava/util/concurrent/TimeUnit;)J
lreturn
.limit locals 1
.limit stack 2
.end method

.method public final get()Ljava/lang/Object;
aload 0
getfield com/a/b/c/bl/a Lcom/a/b/c/cg;
invokeinterface com/a/b/c/cg/get()Ljava/lang/Object; 0
areturn
.limit locals 1
.limit stack 1
.end method
