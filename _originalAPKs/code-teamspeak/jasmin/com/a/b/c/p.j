.bytecode 50.0
.class synchronized abstract com/a/b/c/p
.super java/lang/Object
.implements com/a/b/c/y

.method <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method protected abstract a(Lcom/a/b/c/l;JLjava/util/concurrent/TimeUnit;)V
.end method

.method public final a(Lcom/a/b/c/l;Ljava/lang/String;Ljava/lang/String;)V
.catch java/lang/NumberFormatException from L0 to L1 using L2
.catch java/lang/NumberFormatException from L1 to L2 using L2
.catch java/lang/NumberFormatException from L3 to L4 using L2
.catch java/lang/NumberFormatException from L4 to L5 using L2
.catch java/lang/NumberFormatException from L6 to L7 using L2
.catch java/lang/NumberFormatException from L8 to L9 using L2
.catch java/lang/NumberFormatException from L10 to L11 using L2
aload 3
ifnull L12
aload 3
invokevirtual java/lang/String/isEmpty()Z
ifne L12
iconst_1
istore 4
L13:
iload 4
ldc "value of key %s omitted"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 2
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
L0:
aload 3
aload 3
invokevirtual java/lang/String/length()I
iconst_1
isub
invokevirtual java/lang/String/charAt(I)C
lookupswitch
100 : L3
104 : L6
109 : L8
115 : L10
default : L14
L1:
new java/lang/IllegalArgumentException
dup
ldc "key %s invalid format.  was %s, must end with one of [dDhHmMsS]"
iconst_2
anewarray java/lang/Object
dup
iconst_0
aload 2
aastore
dup
iconst_1
aload 3
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L2:
astore 1
new java/lang/IllegalArgumentException
dup
ldc "key %s value set to %s, must be integer"
iconst_2
anewarray java/lang/Object
dup
iconst_0
aload 2
aastore
dup
iconst_1
aload 3
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L12:
iconst_0
istore 4
goto L13
L3:
getstatic java/util/concurrent/TimeUnit/DAYS Ljava/util/concurrent/TimeUnit;
astore 5
L4:
aload 0
aload 1
aload 3
iconst_0
aload 3
invokevirtual java/lang/String/length()I
iconst_1
isub
invokevirtual java/lang/String/substring(II)Ljava/lang/String;
invokestatic java/lang/Long/parseLong(Ljava/lang/String;)J
aload 5
invokevirtual com/a/b/c/p/a(Lcom/a/b/c/l;JLjava/util/concurrent/TimeUnit;)V
L5:
return
L6:
getstatic java/util/concurrent/TimeUnit/HOURS Ljava/util/concurrent/TimeUnit;
astore 5
L7:
goto L4
L8:
getstatic java/util/concurrent/TimeUnit/MINUTES Ljava/util/concurrent/TimeUnit;
astore 5
L9:
goto L4
L10:
getstatic java/util/concurrent/TimeUnit/SECONDS Ljava/util/concurrent/TimeUnit;
astore 5
L11:
goto L4
L14:
goto L1
.limit locals 6
.limit stack 7
.end method
