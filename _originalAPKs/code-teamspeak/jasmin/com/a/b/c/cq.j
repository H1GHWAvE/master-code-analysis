.bytecode 50.0
.class final synchronized com/a/b/c/cq
.super java/util/AbstractQueue

.field final 'a' Lcom/a/b/c/bs;

.method <init>()V
aload 0
invokespecial java/util/AbstractQueue/<init>()V
aload 0
new com/a/b/c/cr
dup
aload 0
invokespecial com/a/b/c/cr/<init>(Lcom/a/b/c/cq;)V
putfield com/a/b/c/cq/a Lcom/a/b/c/bs;
return
.limit locals 1
.limit stack 4
.end method

.method private a()Lcom/a/b/c/bs;
aload 0
getfield com/a/b/c/cq/a Lcom/a/b/c/bs;
invokeinterface com/a/b/c/bs/i()Lcom/a/b/c/bs; 0
astore 2
aload 2
astore 1
aload 2
aload 0
getfield com/a/b/c/cq/a Lcom/a/b/c/bs;
if_acmpne L0
aconst_null
astore 1
L0:
aload 1
areturn
.limit locals 3
.limit stack 2
.end method

.method private a(Lcom/a/b/c/bs;)Z
aload 1
invokeinterface com/a/b/c/bs/j()Lcom/a/b/c/bs; 0
aload 1
invokeinterface com/a/b/c/bs/i()Lcom/a/b/c/bs; 0
invokestatic com/a/b/c/ao/b(Lcom/a/b/c/bs;Lcom/a/b/c/bs;)V
aload 0
getfield com/a/b/c/cq/a Lcom/a/b/c/bs;
invokeinterface com/a/b/c/bs/j()Lcom/a/b/c/bs; 0
aload 1
invokestatic com/a/b/c/ao/b(Lcom/a/b/c/bs;Lcom/a/b/c/bs;)V
aload 1
aload 0
getfield com/a/b/c/cq/a Lcom/a/b/c/bs;
invokestatic com/a/b/c/ao/b(Lcom/a/b/c/bs;Lcom/a/b/c/bs;)V
iconst_1
ireturn
.limit locals 2
.limit stack 2
.end method

.method private b()Lcom/a/b/c/bs;
aload 0
getfield com/a/b/c/cq/a Lcom/a/b/c/bs;
invokeinterface com/a/b/c/bs/i()Lcom/a/b/c/bs; 0
astore 1
aload 1
aload 0
getfield com/a/b/c/cq/a Lcom/a/b/c/bs;
if_acmpne L0
aconst_null
areturn
L0:
aload 0
aload 1
invokevirtual com/a/b/c/cq/remove(Ljava/lang/Object;)Z
pop
aload 1
areturn
.limit locals 2
.limit stack 2
.end method

.method public final clear()V
aload 0
getfield com/a/b/c/cq/a Lcom/a/b/c/bs;
invokeinterface com/a/b/c/bs/i()Lcom/a/b/c/bs; 0
astore 1
L0:
aload 1
aload 0
getfield com/a/b/c/cq/a Lcom/a/b/c/bs;
if_acmpeq L1
aload 1
invokeinterface com/a/b/c/bs/i()Lcom/a/b/c/bs; 0
astore 2
aload 1
invokestatic com/a/b/c/ao/b(Lcom/a/b/c/bs;)V
aload 2
astore 1
goto L0
L1:
aload 0
getfield com/a/b/c/cq/a Lcom/a/b/c/bs;
aload 0
getfield com/a/b/c/cq/a Lcom/a/b/c/bs;
invokeinterface com/a/b/c/bs/c(Lcom/a/b/c/bs;)V 1
aload 0
getfield com/a/b/c/cq/a Lcom/a/b/c/bs;
aload 0
getfield com/a/b/c/cq/a Lcom/a/b/c/bs;
invokeinterface com/a/b/c/bs/d(Lcom/a/b/c/bs;)V 1
return
.limit locals 3
.limit stack 2
.end method

.method public final contains(Ljava/lang/Object;)Z
aload 1
checkcast com/a/b/c/bs
invokeinterface com/a/b/c/bs/i()Lcom/a/b/c/bs; 0
getstatic com/a/b/c/br/a Lcom/a/b/c/br;
if_acmpeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final isEmpty()Z
aload 0
getfield com/a/b/c/cq/a Lcom/a/b/c/bs;
invokeinterface com/a/b/c/bs/i()Lcom/a/b/c/bs; 0
aload 0
getfield com/a/b/c/cq/a Lcom/a/b/c/bs;
if_acmpne L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method public final iterator()Ljava/util/Iterator;
new com/a/b/c/cs
dup
aload 0
aload 0
invokespecial com/a/b/c/cq/a()Lcom/a/b/c/bs;
invokespecial com/a/b/c/cs/<init>(Lcom/a/b/c/cq;Lcom/a/b/c/bs;)V
areturn
.limit locals 1
.limit stack 4
.end method

.method public final synthetic offer(Ljava/lang/Object;)Z
aload 1
checkcast com/a/b/c/bs
astore 1
aload 1
invokeinterface com/a/b/c/bs/j()Lcom/a/b/c/bs; 0
aload 1
invokeinterface com/a/b/c/bs/i()Lcom/a/b/c/bs; 0
invokestatic com/a/b/c/ao/b(Lcom/a/b/c/bs;Lcom/a/b/c/bs;)V
aload 0
getfield com/a/b/c/cq/a Lcom/a/b/c/bs;
invokeinterface com/a/b/c/bs/j()Lcom/a/b/c/bs; 0
aload 1
invokestatic com/a/b/c/ao/b(Lcom/a/b/c/bs;Lcom/a/b/c/bs;)V
aload 1
aload 0
getfield com/a/b/c/cq/a Lcom/a/b/c/bs;
invokestatic com/a/b/c/ao/b(Lcom/a/b/c/bs;Lcom/a/b/c/bs;)V
iconst_1
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final synthetic peek()Ljava/lang/Object;
aload 0
invokespecial com/a/b/c/cq/a()Lcom/a/b/c/bs;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final synthetic poll()Ljava/lang/Object;
aload 0
getfield com/a/b/c/cq/a Lcom/a/b/c/bs;
invokeinterface com/a/b/c/bs/i()Lcom/a/b/c/bs; 0
astore 1
aload 1
aload 0
getfield com/a/b/c/cq/a Lcom/a/b/c/bs;
if_acmpne L0
aconst_null
areturn
L0:
aload 0
aload 1
invokevirtual com/a/b/c/cq/remove(Ljava/lang/Object;)Z
pop
aload 1
areturn
.limit locals 2
.limit stack 2
.end method

.method public final remove(Ljava/lang/Object;)Z
aload 1
checkcast com/a/b/c/bs
astore 1
aload 1
invokeinterface com/a/b/c/bs/j()Lcom/a/b/c/bs; 0
astore 2
aload 1
invokeinterface com/a/b/c/bs/i()Lcom/a/b/c/bs; 0
astore 3
aload 2
aload 3
invokestatic com/a/b/c/ao/b(Lcom/a/b/c/bs;Lcom/a/b/c/bs;)V
aload 1
invokestatic com/a/b/c/ao/b(Lcom/a/b/c/bs;)V
aload 3
getstatic com/a/b/c/br/a Lcom/a/b/c/br;
if_acmpeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 4
.limit stack 2
.end method

.method public final size()I
iconst_0
istore 1
aload 0
getfield com/a/b/c/cq/a Lcom/a/b/c/bs;
invokeinterface com/a/b/c/bs/i()Lcom/a/b/c/bs; 0
astore 2
L0:
aload 2
aload 0
getfield com/a/b/c/cq/a Lcom/a/b/c/bs;
if_acmpeq L1
iload 1
iconst_1
iadd
istore 1
aload 2
invokeinterface com/a/b/c/bs/i()Lcom/a/b/c/bs; 0
astore 2
goto L0
L1:
iload 1
ireturn
.limit locals 3
.limit stack 2
.end method
