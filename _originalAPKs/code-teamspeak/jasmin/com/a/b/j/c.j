.bytecode 50.0
.class public final synchronized com/a/b/j/c
.super java/lang/Object
.annotation invisible Lcom/a/b/a/b;
b Z = 1
.end annotation

.field static final 'a' I = 170

.annotation invisible Lcom/a/b/a/d;
.end annotation
.end field

.field static final 'b' [D
.annotation invisible Lcom/a/b/a/d;
.end annotation
.end field

.field private static final 'c' D = -2.147483648E9D


.field private static final 'd' D = 2.147483647E9D


.field private static final 'e' D = -9.223372036854776E18D


.field private static final 'f' D = 9.223372036854776E18D


.field private static final 'g' D

.method static <clinit>()V
ldc2_w 2.0D
invokestatic java/lang/Math/log(D)D
putstatic com/a/b/j/c/g D
bipush 11
newarray double
dup
iconst_0
dconst_1
dastore
dup
iconst_1
ldc2_w 2.0922789888E13D
dastore
dup
iconst_2
ldc2_w 2.631308369336935E35D
dastore
dup
iconst_3
ldc2_w 1.2413915592536073E61D
dastore
dup
iconst_4
ldc2_w 1.2688693218588417E89D
dastore
dup
iconst_5
ldc2_w 7.156945704626381E118D
dastore
dup
bipush 6
ldc2_w 9.916779348709496E149D
dastore
dup
bipush 7
ldc2_w 1.974506857221074E182D
dastore
dup
bipush 8
ldc2_w 3.856204823625804E215D
dastore
dup
bipush 9
ldc2_w 5.5502938327393044E249D
dastore
dup
bipush 10
ldc2_w 4.7147236359920616E284D
dastore
putstatic com/a/b/j/c/b [D
return
.limit locals 0
.limit stack 5
.end method

.method private <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a(I)D
ldc "n"
iload 0
invokestatic com/a/b/j/k/b(Ljava/lang/String;I)I
pop
iload 0
sipush 170
if_icmple L0
ldc2_w +doubleinfinity
dreturn
L0:
dconst_1
dstore 1
iload 0
bipush -16
iand
iconst_1
iadd
istore 3
L1:
iload 3
iload 0
if_icmpgt L2
dload 1
iload 3
i2d
dmul
dstore 1
iload 3
iconst_1
iadd
istore 3
goto L1
L2:
getstatic com/a/b/j/c/b [D
iload 0
iconst_4
ishr
daload
dload 1
dmul
dreturn
.limit locals 4
.limit stack 4
.end method

.method private static a(Ljava/lang/Iterable;)D
.annotation invisible Lcom/a/b/a/c;
a s = "MeanAccumulator"
.end annotation
new com/a/b/j/e
dup
iconst_0
invokespecial com/a/b/j/e/<init>(B)V
astore 1
aload 0
invokeinterface java/lang/Iterable/iterator()Ljava/util/Iterator; 0
astore 0
L0:
aload 0
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 1
aload 0
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/lang/Number
invokevirtual java/lang/Number/doubleValue()D
invokevirtual com/a/b/j/e/a(D)V
goto L0
L1:
aload 1
invokevirtual com/a/b/j/e/a()D
dreturn
.limit locals 2
.limit stack 3
.end method

.method private static a(Ljava/util/Iterator;)D
.annotation invisible Lcom/a/b/a/c;
a s = "MeanAccumulator"
.end annotation
new com/a/b/j/e
dup
iconst_0
invokespecial com/a/b/j/e/<init>(B)V
astore 1
L0:
aload 0
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 1
aload 0
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/lang/Number
invokevirtual java/lang/Number/doubleValue()D
invokevirtual com/a/b/j/e/a(D)V
goto L0
L1:
aload 1
invokevirtual com/a/b/j/e/a()D
dreturn
.limit locals 2
.limit stack 3
.end method

.method private static transient a([D)D
.annotation invisible Lcom/a/b/a/c;
a s = "MeanAccumulator"
.end annotation
iconst_0
istore 1
new com/a/b/j/e
dup
iconst_0
invokespecial com/a/b/j/e/<init>(B)V
astore 3
aload 0
arraylength
istore 2
L0:
iload 1
iload 2
if_icmpge L1
aload 3
aload 0
iload 1
daload
invokevirtual com/a/b/j/e/a(D)V
iload 1
iconst_1
iadd
istore 1
goto L0
L1:
aload 3
invokevirtual com/a/b/j/e/a()D
dreturn
.limit locals 4
.limit stack 3
.end method

.method private static transient a([I)D
.annotation invisible Lcom/a/b/a/c;
a s = "MeanAccumulator"
.end annotation
iconst_0
istore 1
new com/a/b/j/e
dup
iconst_0
invokespecial com/a/b/j/e/<init>(B)V
astore 3
aload 0
arraylength
istore 2
L0:
iload 1
iload 2
if_icmpge L1
aload 3
aload 0
iload 1
iaload
i2d
invokevirtual com/a/b/j/e/a(D)V
iload 1
iconst_1
iadd
istore 1
goto L0
L1:
aload 3
invokevirtual com/a/b/j/e/a()D
dreturn
.limit locals 4
.limit stack 3
.end method

.method private static transient a([J)D
.annotation invisible Lcom/a/b/a/c;
a s = "MeanAccumulator"
.end annotation
iconst_0
istore 1
new com/a/b/j/e
dup
iconst_0
invokespecial com/a/b/j/e/<init>(B)V
astore 3
aload 0
arraylength
istore 2
L0:
iload 1
iload 2
if_icmpge L1
aload 3
aload 0
iload 1
laload
l2d
invokevirtual com/a/b/j/e/a(D)V
iload 1
iconst_1
iadd
istore 1
goto L0
L1:
aload 3
invokevirtual com/a/b/j/e/a()D
dreturn
.limit locals 4
.limit stack 3
.end method

.method public static a(DLjava/math/RoundingMode;)Ljava/math/BigInteger;
.annotation invisible Lcom/a/b/a/c;
a s = "#roundIntermediate, java.lang.Math.getExponent, com.google.common.math.DoubleUtils"
.end annotation
iconst_1
istore 4
dload 0
aload 2
invokestatic com/a/b/j/c/b(DLjava/math/RoundingMode;)D
dstore 0
ldc2_w -9.223372036854776E18D
dload 0
dsub
dconst_1
dcmpg
ifge L0
iconst_1
istore 3
L1:
dload 0
ldc2_w 9.223372036854776E18D
dcmpg
ifge L2
L3:
iload 4
iload 3
iand
ifeq L4
dload 0
d2l
invokestatic java/math/BigInteger/valueOf(J)Ljava/math/BigInteger;
astore 2
L5:
aload 2
areturn
L0:
iconst_0
istore 3
goto L1
L2:
iconst_0
istore 4
goto L3
L4:
dload 0
invokestatic java/lang/Math/getExponent(D)I
istore 3
dload 0
invokestatic com/a/b/j/f/a(D)J
invokestatic java/math/BigInteger/valueOf(J)Ljava/math/BigInteger;
iload 3
bipush 52
isub
invokevirtual java/math/BigInteger/shiftLeft(I)Ljava/math/BigInteger;
astore 5
aload 5
astore 2
dload 0
dconst_0
dcmpg
ifge L5
aload 5
invokevirtual java/math/BigInteger/negate()Ljava/math/BigInteger;
areturn
.limit locals 6
.limit stack 4
.end method

.method private static a(D)Z
.annotation invisible Lcom/a/b/a/c;
a s = "com.google.common.math.DoubleUtils"
.end annotation
dload 0
dconst_0
dcmpl
ifle L0
dload 0
invokestatic com/a/b/j/f/b(D)Z
ifeq L0
dload 0
invokestatic com/a/b/j/f/a(D)J
invokestatic com/a/b/j/i/a(J)Z
ifeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 4
.end method

.method private static a(DDD)Z
dload 4
dconst_0
dcmpl
ifge L0
ldc "tolerance"
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 6
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
aload 6
invokevirtual java/lang/String/length()I
bipush 40
iadd
invokespecial java/lang/StringBuilder/<init>(I)V
aload 6
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " ("
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
dload 4
invokevirtual java/lang/StringBuilder/append(D)Ljava/lang/StringBuilder;
ldc ") must be >= 0"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
dload 0
dload 2
dsub
dconst_1
invokestatic java/lang/Math/copySign(DD)D
dload 4
dcmpg
ifle L1
dload 0
dload 2
dcmpl
ifeq L1
dload 0
invokestatic java/lang/Double/isNaN(D)Z
ifeq L2
dload 2
invokestatic java/lang/Double/isNaN(D)Z
ifeq L2
L1:
iconst_1
ireturn
L2:
iconst_0
ireturn
.limit locals 7
.limit stack 6
.end method

.method private static b(D)D
dload 0
invokestatic java/lang/Math/log(D)D
getstatic com/a/b/j/c/g D
ddiv
dreturn
.limit locals 2
.limit stack 4
.end method

.method private static b(DLjava/math/RoundingMode;)D
.annotation invisible Lcom/a/b/a/c;
a s = "#isMathematicalInteger, com.google.common.math.DoubleUtils"
.end annotation
dload 0
invokestatic com/a/b/j/f/b(D)Z
ifne L0
new java/lang/ArithmeticException
dup
ldc "input is infinite or NaN"
invokespecial java/lang/ArithmeticException/<init>(Ljava/lang/String;)V
athrow
L0:
getstatic com/a/b/j/d/a [I
aload 2
invokevirtual java/math/RoundingMode/ordinal()I
iaload
tableswitch 1
L1
L2
L3
L4
L5
L6
L7
L8
default : L9
L9:
new java/lang/AssertionError
dup
invokespecial java/lang/AssertionError/<init>()V
athrow
L1:
dload 0
invokestatic com/a/b/j/c/c(D)Z
invokestatic com/a/b/j/k/a(Z)V
L4:
dload 0
dreturn
L2:
dload 0
dconst_0
dcmpl
ifge L4
dload 0
invokestatic com/a/b/j/c/c(D)Z
ifne L4
dload 0
dconst_1
dsub
dreturn
L3:
dload 0
dconst_0
dcmpg
ifle L4
dload 0
invokestatic com/a/b/j/c/c(D)Z
ifne L4
dload 0
dconst_1
dadd
dreturn
L5:
dload 0
invokestatic com/a/b/j/c/c(D)Z
ifne L4
dload 0
dconst_1
dload 0
invokestatic java/lang/Math/copySign(DD)D
dadd
dreturn
L6:
dload 0
invokestatic java/lang/Math/rint(D)D
dreturn
L7:
dload 0
invokestatic java/lang/Math/rint(D)D
dstore 3
dload 0
dload 3
dsub
invokestatic java/lang/Math/abs(D)D
ldc2_w 0.5D
dcmpl
ifne L10
dload 0
ldc2_w 0.5D
dload 0
invokestatic java/lang/Math/copySign(DD)D
dadd
dreturn
L10:
dload 3
dreturn
L8:
dload 0
invokestatic java/lang/Math/rint(D)D
dstore 3
dload 0
dload 3
dsub
invokestatic java/lang/Math/abs(D)D
ldc2_w 0.5D
dcmpl
ifeq L4
dload 3
dreturn
.limit locals 5
.limit stack 6
.end method

.method private static b(DDD)I
dload 4
dconst_0
dcmpl
ifge L0
ldc "tolerance"
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 7
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
aload 7
invokevirtual java/lang/String/length()I
bipush 40
iadd
invokespecial java/lang/StringBuilder/<init>(I)V
aload 7
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " ("
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
dload 4
invokevirtual java/lang/StringBuilder/append(D)Ljava/lang/StringBuilder;
ldc ") must be >= 0"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
dload 0
dload 2
dsub
dconst_1
invokestatic java/lang/Math/copySign(DD)D
dload 4
dcmpg
ifle L1
dload 0
dload 2
dcmpl
ifeq L1
dload 0
invokestatic java/lang/Double/isNaN(D)Z
ifeq L2
dload 2
invokestatic java/lang/Double/isNaN(D)Z
ifeq L2
L1:
iconst_1
istore 6
L3:
iload 6
ifeq L4
iconst_0
ireturn
L2:
iconst_0
istore 6
goto L3
L4:
dload 0
dload 2
dcmpg
ifge L5
iconst_m1
ireturn
L5:
dload 0
dload 2
dcmpl
ifle L6
iconst_1
ireturn
L6:
dload 0
invokestatic java/lang/Double/isNaN(D)Z
dload 2
invokestatic java/lang/Double/isNaN(D)Z
invokestatic com/a/b/l/a/a(ZZ)I
ireturn
.limit locals 8
.limit stack 6
.end method

.method private static c(DLjava/math/RoundingMode;)I
.annotation invisible Lcom/a/b/a/c;
a s = "#roundIntermediate"
.end annotation
iconst_1
istore 4
dload 0
aload 2
invokestatic com/a/b/j/c/b(DLjava/math/RoundingMode;)D
dstore 0
dload 0
ldc2_w -2.147483649E9D
dcmpl
ifle L0
iconst_1
istore 3
L1:
dload 0
ldc2_w 2.147483648E9D
dcmpg
ifge L2
L3:
iload 4
iload 3
iand
invokestatic com/a/b/j/k/b(Z)V
dload 0
d2i
ireturn
L0:
iconst_0
istore 3
goto L1
L2:
iconst_0
istore 4
goto L3
.limit locals 5
.limit stack 4
.end method

.method private static c(D)Z
.annotation invisible Lcom/a/b/a/c;
a s = "java.lang.Math.getExponent, com.google.common.math.DoubleUtils"
.end annotation
dload 0
invokestatic com/a/b/j/f/b(D)Z
ifeq L0
dload 0
dconst_0
dcmpl
ifeq L1
bipush 52
dload 0
invokestatic com/a/b/j/f/a(D)J
invokestatic java/lang/Long/numberOfTrailingZeros(J)I
isub
dload 0
invokestatic java/lang/Math/getExponent(D)I
if_icmpgt L0
L1:
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 4
.end method

.method private static d(DLjava/math/RoundingMode;)J
.annotation invisible Lcom/a/b/a/c;
a s = "#roundIntermediate"
.end annotation
iconst_1
istore 4
dload 0
aload 2
invokestatic com/a/b/j/c/b(DLjava/math/RoundingMode;)D
dstore 0
ldc2_w -9.223372036854776E18D
dload 0
dsub
dconst_1
dcmpg
ifge L0
iconst_1
istore 3
L1:
dload 0
ldc2_w 9.223372036854776E18D
dcmpg
ifge L2
L3:
iload 4
iload 3
iand
invokestatic com/a/b/j/k/b(Z)V
dload 0
d2l
lreturn
L0:
iconst_0
istore 3
goto L1
L2:
iconst_0
istore 4
goto L3
.limit locals 5
.limit stack 4
.end method

.method private static e(DLjava/math/RoundingMode;)I
.annotation invisible Lcom/a/b/a/c;
a s = "java.lang.Math.getExponent, com.google.common.math.DoubleUtils"
.end annotation
iconst_1
istore 4
iconst_1
istore 5
iconst_1
istore 3
dload 0
dconst_0
dcmpl
ifle L0
dload 0
invokestatic com/a/b/j/f/b(D)Z
ifeq L0
iconst_1
istore 7
L1:
iload 7
ldc "x must be positive and finite"
invokestatic com/a/b/b/cn/a(ZLjava/lang/Object;)V
dload 0
invokestatic java/lang/Math/getExponent(D)I
istore 6
dload 0
invokestatic com/a/b/j/f/c(D)Z
ifne L2
ldc2_w 4.503599627370496E15D
dload 0
dmul
aload 2
invokestatic com/a/b/j/c/e(DLjava/math/RoundingMode;)I
bipush 52
isub
ireturn
L0:
iconst_0
istore 7
goto L1
L2:
getstatic com/a/b/j/d/a [I
aload 2
invokevirtual java/math/RoundingMode/ordinal()I
iaload
tableswitch 1
L3
L4
L5
L6
L7
L8
L8
L8
default : L9
L9:
new java/lang/AssertionError
dup
invokespecial java/lang/AssertionError/<init>()V
athrow
L3:
dload 0
invokestatic com/a/b/j/c/a(D)Z
invokestatic com/a/b/j/k/a(Z)V
L4:
iconst_0
istore 3
L10:
iload 3
ifeq L11
iload 6
iconst_1
iadd
ireturn
L5:
dload 0
invokestatic com/a/b/j/c/a(D)Z
ifeq L10
iconst_0
istore 3
goto L10
L6:
iload 6
ifge L12
iconst_1
istore 3
L13:
dload 0
invokestatic com/a/b/j/c/a(D)Z
ifne L14
L15:
iload 4
iload 3
iand
istore 3
goto L10
L12:
iconst_0
istore 3
goto L13
L14:
iconst_0
istore 4
goto L15
L7:
iload 6
iflt L16
iconst_1
istore 3
L17:
dload 0
invokestatic com/a/b/j/c/a(D)Z
ifne L18
iload 5
istore 4
L19:
iload 4
iload 3
iand
istore 3
goto L10
L16:
iconst_0
istore 3
goto L17
L18:
iconst_0
istore 4
goto L19
L8:
dload 0
invokestatic com/a/b/j/f/d(D)D
dstore 0
dload 0
dload 0
dmul
ldc2_w 2.0D
dcmpl
ifgt L10
iconst_0
istore 3
goto L10
L11:
iload 6
ireturn
.limit locals 8
.limit stack 4
.end method
