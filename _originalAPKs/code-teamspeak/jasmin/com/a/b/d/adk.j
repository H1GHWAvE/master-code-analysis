.bytecode 50.0
.class final synchronized com/a/b/d/adk
.super com/a/b/d/add
.implements com/a/b/d/xc

.field private static final 'c' J = 0L


.field transient 'a' Ljava/util/Set;

.field transient 'b' Ljava/util/Set;

.method <init>(Lcom/a/b/d/xc;Ljava/lang/Object;)V
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
aload 0
aload 1
aload 2
iconst_0
invokespecial com/a/b/d/add/<init>(Ljava/util/Collection;Ljava/lang/Object;B)V
return
.limit locals 3
.limit stack 4
.end method

.method private c()Lcom/a/b/d/xc;
aload 0
invokespecial com/a/b/d/add/b()Ljava/util/Collection;
checkcast com/a/b/d/xc
areturn
.limit locals 1
.limit stack 1
.end method

.method public final a(Ljava/lang/Object;)I
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/adk/h Ljava/lang/Object;
astore 3
aload 3
monitorenter
L0:
aload 0
invokespecial com/a/b/d/add/b()Ljava/util/Collection;
checkcast com/a/b/d/xc
aload 1
invokeinterface com/a/b/d/xc/a(Ljava/lang/Object;)I 1
istore 2
aload 3
monitorexit
L1:
iload 2
ireturn
L2:
astore 1
L3:
aload 3
monitorexit
L4:
aload 1
athrow
.limit locals 4
.limit stack 2
.end method

.method public final a(Ljava/lang/Object;I)I
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/adk/h Ljava/lang/Object;
astore 3
aload 3
monitorenter
L0:
aload 0
invokespecial com/a/b/d/add/b()Ljava/util/Collection;
checkcast com/a/b/d/xc
aload 1
iload 2
invokeinterface com/a/b/d/xc/a(Ljava/lang/Object;I)I 2
istore 2
aload 3
monitorexit
L1:
iload 2
ireturn
L2:
astore 1
L3:
aload 3
monitorexit
L4:
aload 1
athrow
.limit locals 4
.limit stack 3
.end method

.method public final a()Ljava/util/Set;
.catch all from L0 to L1 using L2
.catch all from L1 to L3 using L2
.catch all from L4 to L5 using L2
aload 0
getfield com/a/b/d/adk/h Ljava/lang/Object;
astore 1
aload 1
monitorenter
L0:
aload 0
getfield com/a/b/d/adk/b Ljava/util/Set;
ifnonnull L1
aload 0
aload 0
invokespecial com/a/b/d/add/b()Ljava/util/Collection;
checkcast com/a/b/d/xc
invokeinterface com/a/b/d/xc/a()Ljava/util/Set; 0
aload 0
getfield com/a/b/d/adk/h Ljava/lang/Object;
invokestatic com/a/b/d/acu/b(Ljava/util/Set;Ljava/lang/Object;)Ljava/util/Set;
putfield com/a/b/d/adk/b Ljava/util/Set;
L1:
aload 0
getfield com/a/b/d/adk/b Ljava/util/Set;
astore 2
aload 1
monitorexit
L3:
aload 2
areturn
L2:
astore 2
L4:
aload 1
monitorexit
L5:
aload 2
athrow
.limit locals 3
.limit stack 3
.end method

.method public final a(Ljava/lang/Object;II)Z
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/adk/h Ljava/lang/Object;
astore 5
aload 5
monitorenter
L0:
aload 0
invokespecial com/a/b/d/add/b()Ljava/util/Collection;
checkcast com/a/b/d/xc
aload 1
iload 2
iload 3
invokeinterface com/a/b/d/xc/a(Ljava/lang/Object;II)Z 3
istore 4
aload 5
monitorexit
L1:
iload 4
ireturn
L2:
astore 1
L3:
aload 5
monitorexit
L4:
aload 1
athrow
.limit locals 6
.limit stack 4
.end method

.method public final b(Ljava/lang/Object;I)I
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/adk/h Ljava/lang/Object;
astore 3
aload 3
monitorenter
L0:
aload 0
invokespecial com/a/b/d/add/b()Ljava/util/Collection;
checkcast com/a/b/d/xc
aload 1
iload 2
invokeinterface com/a/b/d/xc/b(Ljava/lang/Object;I)I 2
istore 2
aload 3
monitorexit
L1:
iload 2
ireturn
L2:
astore 1
L3:
aload 3
monitorexit
L4:
aload 1
athrow
.limit locals 4
.limit stack 3
.end method

.method final volatile synthetic b()Ljava/util/Collection;
aload 0
invokespecial com/a/b/d/add/b()Ljava/util/Collection;
checkcast com/a/b/d/xc
areturn
.limit locals 1
.limit stack 1
.end method

.method public final c(Ljava/lang/Object;I)I
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/adk/h Ljava/lang/Object;
astore 3
aload 3
monitorenter
L0:
aload 0
invokespecial com/a/b/d/add/b()Ljava/util/Collection;
checkcast com/a/b/d/xc
aload 1
iload 2
invokeinterface com/a/b/d/xc/c(Ljava/lang/Object;I)I 2
istore 2
aload 3
monitorexit
L1:
iload 2
ireturn
L2:
astore 1
L3:
aload 3
monitorexit
L4:
aload 1
athrow
.limit locals 4
.limit stack 3
.end method

.method final synthetic d()Ljava/lang/Object;
aload 0
invokespecial com/a/b/d/add/b()Ljava/util/Collection;
checkcast com/a/b/d/xc
areturn
.limit locals 1
.limit stack 1
.end method

.method public final equals(Ljava/lang/Object;)Z
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 1
aload 0
if_acmpne L5
iconst_1
ireturn
L5:
aload 0
getfield com/a/b/d/adk/h Ljava/lang/Object;
astore 3
aload 3
monitorenter
L0:
aload 0
invokespecial com/a/b/d/add/b()Ljava/util/Collection;
checkcast com/a/b/d/xc
aload 1
invokeinterface com/a/b/d/xc/equals(Ljava/lang/Object;)Z 1
istore 2
aload 3
monitorexit
L1:
iload 2
ireturn
L2:
astore 1
L3:
aload 3
monitorexit
L4:
aload 1
athrow
.limit locals 4
.limit stack 2
.end method

.method public final hashCode()I
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/adk/h Ljava/lang/Object;
astore 2
aload 2
monitorenter
L0:
aload 0
invokespecial com/a/b/d/add/b()Ljava/util/Collection;
checkcast com/a/b/d/xc
invokeinterface com/a/b/d/xc/hashCode()I 0
istore 1
aload 2
monitorexit
L1:
iload 1
ireturn
L2:
astore 3
L3:
aload 2
monitorexit
L4:
aload 3
athrow
.limit locals 4
.limit stack 1
.end method

.method public final n_()Ljava/util/Set;
.catch all from L0 to L1 using L2
.catch all from L1 to L3 using L2
.catch all from L4 to L5 using L2
aload 0
getfield com/a/b/d/adk/h Ljava/lang/Object;
astore 1
aload 1
monitorenter
L0:
aload 0
getfield com/a/b/d/adk/a Ljava/util/Set;
ifnonnull L1
aload 0
aload 0
invokespecial com/a/b/d/add/b()Ljava/util/Collection;
checkcast com/a/b/d/xc
invokeinterface com/a/b/d/xc/n_()Ljava/util/Set; 0
aload 0
getfield com/a/b/d/adk/h Ljava/lang/Object;
invokestatic com/a/b/d/acu/b(Ljava/util/Set;Ljava/lang/Object;)Ljava/util/Set;
putfield com/a/b/d/adk/a Ljava/util/Set;
L1:
aload 0
getfield com/a/b/d/adk/a Ljava/util/Set;
astore 2
aload 1
monitorexit
L3:
aload 2
areturn
L2:
astore 2
L4:
aload 1
monitorexit
L5:
aload 2
athrow
.limit locals 3
.limit stack 3
.end method
