.bytecode 50.0
.class final synchronized com/a/b/d/aat
.super com/a/b/d/hp
.implements java/io/Serializable
.implements java/util/NavigableSet
.annotation invisible Lcom/a/b/a/c;
a s = "NavigableSet"
.end annotation

.field private static final 'c' J = 0L


.field private final 'a' Ljava/util/NavigableSet;

.field private transient 'b' Lcom/a/b/d/aat;

.method <init>(Ljava/util/NavigableSet;)V
aload 0
invokespecial com/a/b/d/hp/<init>()V
aload 0
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/util/NavigableSet
putfield com/a/b/d/aat/a Ljava/util/NavigableSet;
return
.limit locals 2
.limit stack 2
.end method

.method protected final synthetic a()Ljava/util/Set;
aload 0
getfield com/a/b/d/aat/a Ljava/util/NavigableSet;
invokestatic java/util/Collections/unmodifiableSortedSet(Ljava/util/SortedSet;)Ljava/util/SortedSet;
areturn
.limit locals 1
.limit stack 1
.end method

.method protected final synthetic b()Ljava/util/Collection;
aload 0
getfield com/a/b/d/aat/a Ljava/util/NavigableSet;
invokestatic java/util/Collections/unmodifiableSortedSet(Ljava/util/SortedSet;)Ljava/util/SortedSet;
areturn
.limit locals 1
.limit stack 1
.end method

.method protected final c()Ljava/util/SortedSet;
aload 0
getfield com/a/b/d/aat/a Ljava/util/NavigableSet;
invokestatic java/util/Collections/unmodifiableSortedSet(Ljava/util/SortedSet;)Ljava/util/SortedSet;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final ceiling(Ljava/lang/Object;)Ljava/lang/Object;
aload 0
getfield com/a/b/d/aat/a Ljava/util/NavigableSet;
aload 1
invokeinterface java/util/NavigableSet/ceiling(Ljava/lang/Object;)Ljava/lang/Object; 1
areturn
.limit locals 2
.limit stack 2
.end method

.method public final descendingIterator()Ljava/util/Iterator;
aload 0
getfield com/a/b/d/aat/a Ljava/util/NavigableSet;
invokeinterface java/util/NavigableSet/descendingIterator()Ljava/util/Iterator; 0
invokestatic com/a/b/d/nj/a(Ljava/util/Iterator;)Lcom/a/b/d/agi;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final descendingSet()Ljava/util/NavigableSet;
aload 0
getfield com/a/b/d/aat/b Lcom/a/b/d/aat;
astore 2
aload 2
astore 1
aload 2
ifnonnull L0
new com/a/b/d/aat
dup
aload 0
getfield com/a/b/d/aat/a Ljava/util/NavigableSet;
invokeinterface java/util/NavigableSet/descendingSet()Ljava/util/NavigableSet; 0
invokespecial com/a/b/d/aat/<init>(Ljava/util/NavigableSet;)V
astore 1
aload 0
aload 1
putfield com/a/b/d/aat/b Lcom/a/b/d/aat;
aload 1
aload 0
putfield com/a/b/d/aat/b Lcom/a/b/d/aat;
L0:
aload 1
areturn
.limit locals 3
.limit stack 3
.end method

.method public final floor(Ljava/lang/Object;)Ljava/lang/Object;
aload 0
getfield com/a/b/d/aat/a Ljava/util/NavigableSet;
aload 1
invokeinterface java/util/NavigableSet/floor(Ljava/lang/Object;)Ljava/lang/Object; 1
areturn
.limit locals 2
.limit stack 2
.end method

.method public final headSet(Ljava/lang/Object;Z)Ljava/util/NavigableSet;
aload 0
getfield com/a/b/d/aat/a Ljava/util/NavigableSet;
aload 1
iload 2
invokeinterface java/util/NavigableSet/headSet(Ljava/lang/Object;Z)Ljava/util/NavigableSet; 2
invokestatic com/a/b/d/aad/a(Ljava/util/NavigableSet;)Ljava/util/NavigableSet;
areturn
.limit locals 3
.limit stack 3
.end method

.method public final higher(Ljava/lang/Object;)Ljava/lang/Object;
aload 0
getfield com/a/b/d/aat/a Ljava/util/NavigableSet;
aload 1
invokeinterface java/util/NavigableSet/higher(Ljava/lang/Object;)Ljava/lang/Object; 1
areturn
.limit locals 2
.limit stack 2
.end method

.method protected final synthetic k_()Ljava/lang/Object;
aload 0
getfield com/a/b/d/aat/a Ljava/util/NavigableSet;
invokestatic java/util/Collections/unmodifiableSortedSet(Ljava/util/SortedSet;)Ljava/util/SortedSet;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final lower(Ljava/lang/Object;)Ljava/lang/Object;
aload 0
getfield com/a/b/d/aat/a Ljava/util/NavigableSet;
aload 1
invokeinterface java/util/NavigableSet/lower(Ljava/lang/Object;)Ljava/lang/Object; 1
areturn
.limit locals 2
.limit stack 2
.end method

.method public final pollFirst()Ljava/lang/Object;
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
.limit locals 1
.limit stack 2
.end method

.method public final pollLast()Ljava/lang/Object;
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
.limit locals 1
.limit stack 2
.end method

.method public final subSet(Ljava/lang/Object;ZLjava/lang/Object;Z)Ljava/util/NavigableSet;
aload 0
getfield com/a/b/d/aat/a Ljava/util/NavigableSet;
aload 1
iload 2
aload 3
iload 4
invokeinterface java/util/NavigableSet/subSet(Ljava/lang/Object;ZLjava/lang/Object;Z)Ljava/util/NavigableSet; 4
invokestatic com/a/b/d/aad/a(Ljava/util/NavigableSet;)Ljava/util/NavigableSet;
areturn
.limit locals 5
.limit stack 5
.end method

.method public final tailSet(Ljava/lang/Object;Z)Ljava/util/NavigableSet;
aload 0
getfield com/a/b/d/aat/a Ljava/util/NavigableSet;
aload 1
iload 2
invokeinterface java/util/NavigableSet/tailSet(Ljava/lang/Object;Z)Ljava/util/NavigableSet; 2
invokestatic com/a/b/d/aad/a(Ljava/util/NavigableSet;)Ljava/util/NavigableSet;
areturn
.limit locals 3
.limit stack 3
.end method
