.bytecode 50.0
.class final synchronized com/a/b/d/pg
.super java/lang/Object
.implements java/util/ListIterator

.field 'a' Z

.field final synthetic 'b' Ljava/util/ListIterator;

.field final synthetic 'c' Lcom/a/b/d/pf;

.method <init>(Lcom/a/b/d/pf;Ljava/util/ListIterator;)V
aload 0
aload 1
putfield com/a/b/d/pg/c Lcom/a/b/d/pf;
aload 0
aload 2
putfield com/a/b/d/pg/b Ljava/util/ListIterator;
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 3
.limit stack 2
.end method

.method public final add(Ljava/lang/Object;)V
aload 0
getfield com/a/b/d/pg/b Ljava/util/ListIterator;
aload 1
invokeinterface java/util/ListIterator/add(Ljava/lang/Object;)V 1
aload 0
getfield com/a/b/d/pg/b Ljava/util/ListIterator;
invokeinterface java/util/ListIterator/previous()Ljava/lang/Object; 0
pop
aload 0
iconst_0
putfield com/a/b/d/pg/a Z
return
.limit locals 2
.limit stack 2
.end method

.method public final hasNext()Z
aload 0
getfield com/a/b/d/pg/b Ljava/util/ListIterator;
invokeinterface java/util/ListIterator/hasPrevious()Z 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final hasPrevious()Z
aload 0
getfield com/a/b/d/pg/b Ljava/util/ListIterator;
invokeinterface java/util/ListIterator/hasNext()Z 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final next()Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/pg/hasNext()Z
ifne L0
new java/util/NoSuchElementException
dup
invokespecial java/util/NoSuchElementException/<init>()V
athrow
L0:
aload 0
iconst_1
putfield com/a/b/d/pg/a Z
aload 0
getfield com/a/b/d/pg/b Ljava/util/ListIterator;
invokeinterface java/util/ListIterator/previous()Ljava/lang/Object; 0
areturn
.limit locals 1
.limit stack 2
.end method

.method public final nextIndex()I
aload 0
getfield com/a/b/d/pg/c Lcom/a/b/d/pf;
aload 0
getfield com/a/b/d/pg/b Ljava/util/ListIterator;
invokeinterface java/util/ListIterator/nextIndex()I 0
invokestatic com/a/b/d/pf/a(Lcom/a/b/d/pf;I)I
ireturn
.limit locals 1
.limit stack 2
.end method

.method public final previous()Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/pg/hasPrevious()Z
ifne L0
new java/util/NoSuchElementException
dup
invokespecial java/util/NoSuchElementException/<init>()V
athrow
L0:
aload 0
iconst_1
putfield com/a/b/d/pg/a Z
aload 0
getfield com/a/b/d/pg/b Ljava/util/ListIterator;
invokeinterface java/util/ListIterator/next()Ljava/lang/Object; 0
areturn
.limit locals 1
.limit stack 2
.end method

.method public final previousIndex()I
aload 0
invokevirtual com/a/b/d/pg/nextIndex()I
iconst_1
isub
ireturn
.limit locals 1
.limit stack 2
.end method

.method public final remove()V
aload 0
getfield com/a/b/d/pg/a Z
ldc "no calls to next() since the last call to remove()"
invokestatic com/a/b/b/cn/b(ZLjava/lang/Object;)V
aload 0
getfield com/a/b/d/pg/b Ljava/util/ListIterator;
invokeinterface java/util/ListIterator/remove()V 0
aload 0
iconst_0
putfield com/a/b/d/pg/a Z
return
.limit locals 1
.limit stack 2
.end method

.method public final set(Ljava/lang/Object;)V
aload 0
getfield com/a/b/d/pg/a Z
invokestatic com/a/b/b/cn/b(Z)V
aload 0
getfield com/a/b/d/pg/b Ljava/util/ListIterator;
aload 1
invokeinterface java/util/ListIterator/set(Ljava/lang/Object;)V 1
return
.limit locals 2
.limit stack 2
.end method
