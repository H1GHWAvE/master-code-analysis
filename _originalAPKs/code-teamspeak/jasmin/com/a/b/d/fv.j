.bytecode 50.0
.class final synchronized com/a/b/d/fv
.super com/a/b/d/gp

.field final 'a' Ljava/lang/Object;

.method <init>(Ljava/lang/Object;)V
aload 0
invokespecial com/a/b/d/gp/<init>()V
aload 0
aload 1
putfield com/a/b/d/fv/a Ljava/lang/Object;
return
.limit locals 2
.limit stack 2
.end method

.method protected final a()Ljava/util/List;
invokestatic java/util/Collections/emptyList()Ljava/util/List;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final add(ILjava/lang/Object;)V
iload 1
iconst_0
invokestatic com/a/b/b/cn/b(II)I
pop
aload 0
getfield com/a/b/d/fv/a Ljava/lang/Object;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 2
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
aload 2
invokevirtual java/lang/String/length()I
bipush 32
iadd
invokespecial java/lang/StringBuilder/<init>(I)V
ldc "Key does not satisfy predicate: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
.limit locals 3
.limit stack 6
.end method

.method public final add(Ljava/lang/Object;)Z
aload 0
iconst_0
aload 1
invokevirtual com/a/b/d/fv/add(ILjava/lang/Object;)V
iconst_1
ireturn
.limit locals 2
.limit stack 3
.end method

.method public final addAll(ILjava/util/Collection;)Z
aload 2
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
iload 1
iconst_0
invokestatic com/a/b/b/cn/b(II)I
pop
aload 0
getfield com/a/b/d/fv/a Ljava/lang/Object;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 2
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
aload 2
invokevirtual java/lang/String/length()I
bipush 32
iadd
invokespecial java/lang/StringBuilder/<init>(I)V
ldc "Key does not satisfy predicate: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
.limit locals 3
.limit stack 6
.end method

.method public final addAll(Ljava/util/Collection;)Z
aload 0
iconst_0
aload 1
invokevirtual com/a/b/d/fv/addAll(ILjava/util/Collection;)Z
pop
iconst_1
ireturn
.limit locals 2
.limit stack 3
.end method

.method protected final synthetic b()Ljava/util/Collection;
invokestatic java/util/Collections/emptyList()Ljava/util/List;
areturn
.limit locals 1
.limit stack 1
.end method

.method protected final synthetic k_()Ljava/lang/Object;
invokestatic java/util/Collections/emptyList()Ljava/util/List;
areturn
.limit locals 1
.limit stack 1
.end method
