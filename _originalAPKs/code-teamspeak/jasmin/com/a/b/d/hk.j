.bytecode 50.0
.class public synchronized abstract com/a/b/d/hk
.super com/a/b/d/gs
.implements java/util/SortedMap
.annotation invisible Lcom/a/b/a/b;
.end annotation

.method protected <init>()V
aload 0
invokespecial com/a/b/d/gs/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private b(Ljava/lang/Object;Ljava/lang/Object;)I
aload 0
invokevirtual com/a/b/d/hk/comparator()Ljava/util/Comparator;
astore 3
aload 3
ifnonnull L0
aload 1
checkcast java/lang/Comparable
aload 2
invokeinterface java/lang/Comparable/compareTo(Ljava/lang/Object;)I 1
ireturn
L0:
aload 3
aload 1
aload 2
invokeinterface java/util/Comparator/compare(Ljava/lang/Object;Ljava/lang/Object;)I 2
ireturn
.limit locals 4
.limit stack 3
.end method

.method protected synthetic a()Ljava/util/Map;
aload 0
invokevirtual com/a/b/d/hk/c()Ljava/util/SortedMap;
areturn
.limit locals 1
.limit stack 1
.end method

.method protected a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedMap;
.annotation invisible Lcom/a/b/a/a;
.end annotation
aload 0
aload 1
aload 2
invokespecial com/a/b/d/hk/b(Ljava/lang/Object;Ljava/lang/Object;)I
ifgt L0
iconst_1
istore 3
L1:
iload 3
ldc "fromKey must be <= toKey"
invokestatic com/a/b/b/cn/a(ZLjava/lang/Object;)V
aload 0
aload 1
invokevirtual com/a/b/d/hk/tailMap(Ljava/lang/Object;)Ljava/util/SortedMap;
aload 2
invokeinterface java/util/SortedMap/headMap(Ljava/lang/Object;)Ljava/util/SortedMap; 1
areturn
L0:
iconst_0
istore 3
goto L1
.limit locals 4
.limit stack 3
.end method

.method protected abstract c()Ljava/util/SortedMap;
.end method

.method protected final c(Ljava/lang/Object;)Z
.annotation invisible Lcom/a/b/a/a;
.end annotation
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
.catch java/lang/ClassCastException from L0 to L1 using L2
.catch java/util/NoSuchElementException from L0 to L1 using L3
.catch java/lang/NullPointerException from L0 to L1 using L4
iconst_0
istore 3
L0:
aload 0
aload 0
aload 1
invokeinterface java/util/SortedMap/tailMap(Ljava/lang/Object;)Ljava/util/SortedMap; 1
invokeinterface java/util/SortedMap/firstKey()Ljava/lang/Object; 0
aload 1
invokespecial com/a/b/d/hk/b(Ljava/lang/Object;Ljava/lang/Object;)I
istore 2
L1:
iload 2
ifne L5
iconst_1
istore 3
L5:
iload 3
ireturn
L4:
astore 1
iconst_0
ireturn
L3:
astore 1
iconst_0
ireturn
L2:
astore 1
iconst_0
ireturn
.limit locals 4
.limit stack 3
.end method

.method public comparator()Ljava/util/Comparator;
aload 0
invokevirtual com/a/b/d/hk/c()Ljava/util/SortedMap;
invokeinterface java/util/SortedMap/comparator()Ljava/util/Comparator; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method public firstKey()Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/hk/c()Ljava/util/SortedMap;
invokeinterface java/util/SortedMap/firstKey()Ljava/lang/Object; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method public headMap(Ljava/lang/Object;)Ljava/util/SortedMap;
aload 0
invokevirtual com/a/b/d/hk/c()Ljava/util/SortedMap;
aload 1
invokeinterface java/util/SortedMap/headMap(Ljava/lang/Object;)Ljava/util/SortedMap; 1
areturn
.limit locals 2
.limit stack 2
.end method

.method protected synthetic k_()Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/hk/c()Ljava/util/SortedMap;
areturn
.limit locals 1
.limit stack 1
.end method

.method public lastKey()Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/hk/c()Ljava/util/SortedMap;
invokeinterface java/util/SortedMap/lastKey()Ljava/lang/Object; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method public subMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedMap;
aload 0
invokevirtual com/a/b/d/hk/c()Ljava/util/SortedMap;
aload 1
aload 2
invokeinterface java/util/SortedMap/subMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedMap; 2
areturn
.limit locals 3
.limit stack 3
.end method

.method public tailMap(Ljava/lang/Object;)Ljava/util/SortedMap;
aload 0
invokevirtual com/a/b/d/hk/c()Ljava/util/SortedMap;
aload 1
invokeinterface java/util/SortedMap/tailMap(Ljava/lang/Object;)Ljava/util/SortedMap; 1
areturn
.limit locals 2
.limit stack 2
.end method
