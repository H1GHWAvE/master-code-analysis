.bytecode 50.0
.class final synchronized com/a/b/d/gb
.super java/util/AbstractCollection
.annotation invisible Lcom/a/b/a/b;
.end annotation

.field private final 'a' Lcom/a/b/d/ga;

.method <init>(Lcom/a/b/d/ga;)V
aload 0
invokespecial java/util/AbstractCollection/<init>()V
aload 0
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast com/a/b/d/ga
putfield com/a/b/d/gb/a Lcom/a/b/d/ga;
return
.limit locals 2
.limit stack 2
.end method

.method public final clear()V
aload 0
getfield com/a/b/d/gb/a Lcom/a/b/d/ga;
invokeinterface com/a/b/d/ga/g()V 0
return
.limit locals 1
.limit stack 1
.end method

.method public final contains(Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
getfield com/a/b/d/gb/a Lcom/a/b/d/ga;
aload 1
invokeinterface com/a/b/d/ga/g(Ljava/lang/Object;)Z 1
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final iterator()Ljava/util/Iterator;
aload 0
getfield com/a/b/d/gb/a Lcom/a/b/d/ga;
invokeinterface com/a/b/d/ga/k()Ljava/util/Collection; 0
invokeinterface java/util/Collection/iterator()Ljava/util/Iterator; 0
invokestatic com/a/b/d/sz/b(Ljava/util/Iterator;)Ljava/util/Iterator;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final remove(Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
getfield com/a/b/d/gb/a Lcom/a/b/d/ga;
invokeinterface com/a/b/d/ga/c()Lcom/a/b/b/co; 0
astore 2
aload 0
getfield com/a/b/d/gb/a Lcom/a/b/d/ga;
invokeinterface com/a/b/d/ga/a()Lcom/a/b/d/vi; 0
invokeinterface com/a/b/d/vi/k()Ljava/util/Collection; 0
invokeinterface java/util/Collection/iterator()Ljava/util/Iterator; 0
astore 3
L0:
aload 3
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 3
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/util/Map$Entry
astore 4
aload 2
aload 4
invokeinterface com/a/b/b/co/a(Ljava/lang/Object;)Z 1
ifeq L0
aload 4
invokeinterface java/util/Map$Entry/getValue()Ljava/lang/Object; 0
aload 1
invokestatic com/a/b/b/ce/a(Ljava/lang/Object;Ljava/lang/Object;)Z
ifeq L0
aload 3
invokeinterface java/util/Iterator/remove()V 0
iconst_1
ireturn
L1:
iconst_0
ireturn
.limit locals 5
.limit stack 2
.end method

.method public final removeAll(Ljava/util/Collection;)Z
aload 0
getfield com/a/b/d/gb/a Lcom/a/b/d/ga;
invokeinterface com/a/b/d/ga/a()Lcom/a/b/d/vi; 0
invokeinterface com/a/b/d/vi/k()Ljava/util/Collection; 0
aload 0
getfield com/a/b/d/gb/a Lcom/a/b/d/ga;
invokeinterface com/a/b/d/ga/c()Lcom/a/b/b/co; 0
aload 1
invokestatic com/a/b/b/cp/a(Ljava/util/Collection;)Lcom/a/b/b/co;
invokestatic com/a/b/d/sz/b(Lcom/a/b/b/co;)Lcom/a/b/b/co;
invokestatic com/a/b/b/cp/a(Lcom/a/b/b/co;Lcom/a/b/b/co;)Lcom/a/b/b/co;
invokestatic com/a/b/d/mq/a(Ljava/lang/Iterable;Lcom/a/b/b/co;)Z
ireturn
.limit locals 2
.limit stack 3
.end method

.method public final retainAll(Ljava/util/Collection;)Z
aload 0
getfield com/a/b/d/gb/a Lcom/a/b/d/ga;
invokeinterface com/a/b/d/ga/a()Lcom/a/b/d/vi; 0
invokeinterface com/a/b/d/vi/k()Ljava/util/Collection; 0
aload 0
getfield com/a/b/d/gb/a Lcom/a/b/d/ga;
invokeinterface com/a/b/d/ga/c()Lcom/a/b/b/co; 0
aload 1
invokestatic com/a/b/b/cp/a(Ljava/util/Collection;)Lcom/a/b/b/co;
invokestatic com/a/b/b/cp/a(Lcom/a/b/b/co;)Lcom/a/b/b/co;
invokestatic com/a/b/d/sz/b(Lcom/a/b/b/co;)Lcom/a/b/b/co;
invokestatic com/a/b/b/cp/a(Lcom/a/b/b/co;Lcom/a/b/b/co;)Lcom/a/b/b/co;
invokestatic com/a/b/d/mq/a(Ljava/lang/Iterable;Lcom/a/b/b/co;)Z
ireturn
.limit locals 2
.limit stack 3
.end method

.method public final size()I
aload 0
getfield com/a/b/d/gb/a Lcom/a/b/d/ga;
invokeinterface com/a/b/d/ga/f()I 0
ireturn
.limit locals 1
.limit stack 1
.end method
