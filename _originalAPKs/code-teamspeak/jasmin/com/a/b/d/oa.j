.bytecode 50.0
.class final synchronized com/a/b/d/oa
.super java/lang/Object
.implements com/a/b/d/yi

.field private final 'a' Ljava/util/Iterator;

.field private 'b' Z

.field private 'c' Ljava/lang/Object;

.method public <init>(Ljava/util/Iterator;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/util/Iterator
putfield com/a/b/d/oa/a Ljava/util/Iterator;
return
.limit locals 2
.limit stack 2
.end method

.method public final a()Ljava/lang/Object;
aload 0
getfield com/a/b/d/oa/b Z
ifne L0
aload 0
aload 0
getfield com/a/b/d/oa/a Ljava/util/Iterator;
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
putfield com/a/b/d/oa/c Ljava/lang/Object;
aload 0
iconst_1
putfield com/a/b/d/oa/b Z
L0:
aload 0
getfield com/a/b/d/oa/c Ljava/lang/Object;
areturn
.limit locals 1
.limit stack 2
.end method

.method public final hasNext()Z
aload 0
getfield com/a/b/d/oa/b Z
ifne L0
aload 0
getfield com/a/b/d/oa/a Ljava/util/Iterator;
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
L0:
iconst_1
ireturn
L1:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final next()Ljava/lang/Object;
aload 0
getfield com/a/b/d/oa/b Z
ifne L0
aload 0
getfield com/a/b/d/oa/a Ljava/util/Iterator;
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
areturn
L0:
aload 0
getfield com/a/b/d/oa/c Ljava/lang/Object;
astore 1
aload 0
iconst_0
putfield com/a/b/d/oa/b Z
aload 0
aconst_null
putfield com/a/b/d/oa/c Ljava/lang/Object;
aload 1
areturn
.limit locals 2
.limit stack 2
.end method

.method public final remove()V
aload 0
getfield com/a/b/d/oa/b Z
ifne L0
iconst_1
istore 1
L1:
iload 1
ldc "Can't remove after you've peeked at next"
invokestatic com/a/b/b/cn/b(ZLjava/lang/Object;)V
aload 0
getfield com/a/b/d/oa/a Ljava/util/Iterator;
invokeinterface java/util/Iterator/remove()V 0
return
L0:
iconst_0
istore 1
goto L1
.limit locals 2
.limit stack 2
.end method
