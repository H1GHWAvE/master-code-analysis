.bytecode 50.0
.class synchronized com/a/b/d/q
.super com/a/b/d/uj

.field final transient 'a' Ljava/util/Map;

.field final synthetic 'b' Lcom/a/b/d/n;

.method <init>(Lcom/a/b/d/n;Ljava/util/Map;)V
aload 0
aload 1
putfield com/a/b/d/q/b Lcom/a/b/d/n;
aload 0
invokespecial com/a/b/d/uj/<init>()V
aload 0
aload 2
putfield com/a/b/d/q/a Ljava/util/Map;
return
.limit locals 3
.limit stack 2
.end method

.method private a(Ljava/lang/Object;)Ljava/util/Collection;
aload 0
getfield com/a/b/d/q/a Ljava/util/Map;
aload 1
invokestatic com/a/b/d/sz/a(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/util/Collection
astore 2
aload 2
ifnonnull L0
aconst_null
areturn
L0:
aload 0
getfield com/a/b/d/q/b Lcom/a/b/d/n;
aload 1
aload 2
invokevirtual com/a/b/d/n/a(Ljava/lang/Object;Ljava/util/Collection;)Ljava/util/Collection;
areturn
.limit locals 3
.limit stack 3
.end method

.method private b(Ljava/lang/Object;)Ljava/util/Collection;
aload 0
getfield com/a/b/d/q/a Ljava/util/Map;
aload 1
invokeinterface java/util/Map/remove(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast java/util/Collection
astore 1
aload 1
ifnonnull L0
aconst_null
areturn
L0:
aload 0
getfield com/a/b/d/q/b Lcom/a/b/d/n;
invokevirtual com/a/b/d/n/c()Ljava/util/Collection;
astore 2
aload 2
aload 1
invokeinterface java/util/Collection/addAll(Ljava/util/Collection;)Z 1
pop
aload 0
getfield com/a/b/d/q/b Lcom/a/b/d/n;
aload 1
invokeinterface java/util/Collection/size()I 0
invokestatic com/a/b/d/n/b(Lcom/a/b/d/n;I)I
pop
aload 1
invokeinterface java/util/Collection/clear()V 0
aload 2
areturn
.limit locals 3
.limit stack 2
.end method

.method final a(Ljava/util/Map$Entry;)Ljava/util/Map$Entry;
aload 1
invokeinterface java/util/Map$Entry/getKey()Ljava/lang/Object; 0
astore 2
aload 2
aload 0
getfield com/a/b/d/q/b Lcom/a/b/d/n;
aload 2
aload 1
invokeinterface java/util/Map$Entry/getValue()Ljava/lang/Object; 0
checkcast java/util/Collection
invokevirtual com/a/b/d/n/a(Ljava/lang/Object;Ljava/util/Collection;)Ljava/util/Collection;
invokestatic com/a/b/d/sz/a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map$Entry;
areturn
.limit locals 3
.limit stack 4
.end method

.method protected final a()Ljava/util/Set;
new com/a/b/d/r
dup
aload 0
invokespecial com/a/b/d/r/<init>(Lcom/a/b/d/q;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method public clear()V
aload 0
getfield com/a/b/d/q/a Ljava/util/Map;
aload 0
getfield com/a/b/d/q/b Lcom/a/b/d/n;
invokestatic com/a/b/d/n/a(Lcom/a/b/d/n;)Ljava/util/Map;
if_acmpne L0
aload 0
getfield com/a/b/d/q/b Lcom/a/b/d/n;
invokevirtual com/a/b/d/n/g()V
return
L0:
new com/a/b/d/s
dup
aload 0
invokespecial com/a/b/d/s/<init>(Lcom/a/b/d/q;)V
invokestatic com/a/b/d/nj/i(Ljava/util/Iterator;)V
return
.limit locals 1
.limit stack 3
.end method

.method public containsKey(Ljava/lang/Object;)Z
aload 0
getfield com/a/b/d/q/a Ljava/util/Map;
aload 1
invokestatic com/a/b/d/sz/b(Ljava/util/Map;Ljava/lang/Object;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public equals(Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
aload 1
if_acmpeq L0
aload 0
getfield com/a/b/d/q/a Ljava/util/Map;
aload 1
invokeinterface java/util/Map/equals(Ljava/lang/Object;)Z 1
ifeq L1
L0:
iconst_1
ireturn
L1:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method public synthetic get(Ljava/lang/Object;)Ljava/lang/Object;
aload 0
getfield com/a/b/d/q/a Ljava/util/Map;
aload 1
invokestatic com/a/b/d/sz/a(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/util/Collection
astore 2
aload 2
ifnonnull L0
aconst_null
areturn
L0:
aload 0
getfield com/a/b/d/q/b Lcom/a/b/d/n;
aload 1
aload 2
invokevirtual com/a/b/d/n/a(Ljava/lang/Object;Ljava/util/Collection;)Ljava/util/Collection;
areturn
.limit locals 3
.limit stack 3
.end method

.method public hashCode()I
aload 0
getfield com/a/b/d/q/a Ljava/util/Map;
invokeinterface java/util/Map/hashCode()I 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public keySet()Ljava/util/Set;
aload 0
getfield com/a/b/d/q/b Lcom/a/b/d/n;
invokevirtual com/a/b/d/n/p()Ljava/util/Set;
areturn
.limit locals 1
.limit stack 1
.end method

.method public synthetic remove(Ljava/lang/Object;)Ljava/lang/Object;
aload 0
getfield com/a/b/d/q/a Ljava/util/Map;
aload 1
invokeinterface java/util/Map/remove(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast java/util/Collection
astore 1
aload 1
ifnonnull L0
aconst_null
areturn
L0:
aload 0
getfield com/a/b/d/q/b Lcom/a/b/d/n;
invokevirtual com/a/b/d/n/c()Ljava/util/Collection;
astore 2
aload 2
aload 1
invokeinterface java/util/Collection/addAll(Ljava/util/Collection;)Z 1
pop
aload 0
getfield com/a/b/d/q/b Lcom/a/b/d/n;
aload 1
invokeinterface java/util/Collection/size()I 0
invokestatic com/a/b/d/n/b(Lcom/a/b/d/n;I)I
pop
aload 1
invokeinterface java/util/Collection/clear()V 0
aload 2
areturn
.limit locals 3
.limit stack 2
.end method

.method public size()I
aload 0
getfield com/a/b/d/q/a Ljava/util/Map;
invokeinterface java/util/Map/size()I 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public toString()Ljava/lang/String;
aload 0
getfield com/a/b/d/q/a Ljava/util/Map;
invokevirtual java/lang/Object/toString()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method
