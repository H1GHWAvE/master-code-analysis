.bytecode 50.0
.class public final synchronized com/a/b/d/bk
.super com/a/b/d/m
.annotation invisible Lcom/a/b/a/b;
a Z = 1
b Z = 1
.end annotation

.field private static final 'b' I = 3


.field private static final 'c' J = 0L

.annotation invisible Lcom/a/b/a/c;
a s = "Not needed in emulated source."
.end annotation
.end field

.field transient 'a' I
.annotation invisible Lcom/a/b/a/d;
.end annotation
.end field

.method private <init>()V
aload 0
new java/util/HashMap
dup
invokespecial java/util/HashMap/<init>()V
invokespecial com/a/b/d/m/<init>(Ljava/util/Map;)V
aload 0
iconst_3
putfield com/a/b/d/bk/a I
return
.limit locals 1
.limit stack 3
.end method

.method private <init>(II)V
aload 0
iload 1
invokestatic com/a/b/d/sz/a(I)Ljava/util/HashMap;
invokespecial com/a/b/d/m/<init>(Ljava/util/Map;)V
iload 2
ldc "expectedValuesPerKey"
invokestatic com/a/b/d/cl/a(ILjava/lang/String;)I
pop
aload 0
iload 2
putfield com/a/b/d/bk/a I
return
.limit locals 3
.limit stack 2
.end method

.method private <init>(Lcom/a/b/d/vi;)V
aload 1
invokeinterface com/a/b/d/vi/p()Ljava/util/Set; 0
invokeinterface java/util/Set/size()I 0
istore 3
aload 1
instanceof com/a/b/d/bk
ifeq L0
aload 1
checkcast com/a/b/d/bk
getfield com/a/b/d/bk/a I
istore 2
L1:
aload 0
iload 3
iload 2
invokespecial com/a/b/d/bk/<init>(II)V
aload 0
aload 1
invokevirtual com/a/b/d/bk/a(Lcom/a/b/d/vi;)Z
pop
return
L0:
iconst_3
istore 2
goto L1
.limit locals 4
.limit stack 3
.end method

.method private static a(II)Lcom/a/b/d/bk;
new com/a/b/d/bk
dup
iload 0
iload 1
invokespecial com/a/b/d/bk/<init>(II)V
areturn
.limit locals 2
.limit stack 4
.end method

.method private a(Ljava/io/ObjectInputStream;)V
.annotation invisible Lcom/a/b/a/c;
a s = "java.io.ObjectOutputStream"
.end annotation
aload 1
invokevirtual java/io/ObjectInputStream/defaultReadObject()V
aload 0
aload 1
invokevirtual java/io/ObjectInputStream/readInt()I
putfield com/a/b/d/bk/a I
aload 1
invokevirtual java/io/ObjectInputStream/readInt()I
istore 2
aload 0
iload 2
invokestatic com/a/b/d/sz/a(I)Ljava/util/HashMap;
invokevirtual com/a/b/d/bk/a(Ljava/util/Map;)V
aload 0
aload 1
iload 2
invokestatic com/a/b/d/zz/a(Lcom/a/b/d/vi;Ljava/io/ObjectInputStream;I)V
return
.limit locals 3
.limit stack 3
.end method

.method private a(Ljava/io/ObjectOutputStream;)V
.annotation invisible Lcom/a/b/a/c;
a s = "java.io.ObjectOutputStream"
.end annotation
aload 1
invokevirtual java/io/ObjectOutputStream/defaultWriteObject()V
aload 1
aload 0
getfield com/a/b/d/bk/a I
invokevirtual java/io/ObjectOutputStream/writeInt(I)V
aload 0
aload 1
invokestatic com/a/b/d/zz/a(Lcom/a/b/d/vi;Ljava/io/ObjectOutputStream;)V
return
.limit locals 2
.limit stack 2
.end method

.method private static b(Lcom/a/b/d/vi;)Lcom/a/b/d/bk;
new com/a/b/d/bk
dup
aload 0
invokespecial com/a/b/d/bk/<init>(Lcom/a/b/d/vi;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private static t()Lcom/a/b/d/bk;
new com/a/b/d/bk
dup
invokespecial com/a/b/d/bk/<init>()V
areturn
.limit locals 0
.limit stack 2
.end method

.method private u()V
aload 0
invokevirtual com/a/b/d/bk/e()Ljava/util/Map;
invokeinterface java/util/Map/values()Ljava/util/Collection; 0
invokeinterface java/util/Collection/iterator()Ljava/util/Iterator; 0
astore 1
L0:
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/util/Collection
checkcast java/util/ArrayList
invokevirtual java/util/ArrayList/trimToSize()V
goto L0
L1:
return
.limit locals 2
.limit stack 1
.end method

.method final a()Ljava/util/List;
new java/util/ArrayList
dup
aload 0
getfield com/a/b/d/bk/a I
invokespecial java/util/ArrayList/<init>(I)V
areturn
.limit locals 1
.limit stack 3
.end method

.method public final volatile synthetic a(Ljava/lang/Object;)Ljava/util/List;
aload 0
aload 1
invokespecial com/a/b/d/m/a(Ljava/lang/Object;)Ljava/util/List;
areturn
.limit locals 2
.limit stack 2
.end method

.method public final volatile synthetic a(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/List;
aload 0
aload 1
aload 2
invokespecial com/a/b/d/m/a(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/List;
areturn
.limit locals 3
.limit stack 3
.end method

.method public final volatile synthetic a(Lcom/a/b/d/vi;)Z
aload 0
aload 1
invokespecial com/a/b/d/m/a(Lcom/a/b/d/vi;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final volatile synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Z
aload 0
aload 1
aload 2
invokespecial com/a/b/d/m/a(Ljava/lang/Object;Ljava/lang/Object;)Z
ireturn
.limit locals 3
.limit stack 3
.end method

.method public final volatile synthetic b(Ljava/lang/Object;)Ljava/util/List;
aload 0
aload 1
invokespecial com/a/b/d/m/b(Ljava/lang/Object;)Ljava/util/List;
areturn
.limit locals 2
.limit stack 2
.end method

.method public final volatile synthetic b()Ljava/util/Map;
aload 0
invokespecial com/a/b/d/m/b()Ljava/util/Map;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final volatile synthetic b(Ljava/lang/Object;Ljava/lang/Object;)Z
aload 0
aload 1
aload 2
invokespecial com/a/b/d/m/b(Ljava/lang/Object;Ljava/lang/Object;)Z
ireturn
.limit locals 3
.limit stack 3
.end method

.method final synthetic c()Ljava/util/Collection;
aload 0
invokevirtual com/a/b/d/bk/a()Ljava/util/List;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final volatile synthetic c(Ljava/lang/Object;Ljava/lang/Iterable;)Z
aload 0
aload 1
aload 2
invokespecial com/a/b/d/m/c(Ljava/lang/Object;Ljava/lang/Iterable;)Z
ireturn
.limit locals 3
.limit stack 3
.end method

.method public final volatile synthetic c(Ljava/lang/Object;Ljava/lang/Object;)Z
aload 0
aload 1
aload 2
invokespecial com/a/b/d/m/c(Ljava/lang/Object;Ljava/lang/Object;)Z
ireturn
.limit locals 3
.limit stack 3
.end method

.method public final volatile synthetic equals(Ljava/lang/Object;)Z
aload 0
aload 1
invokespecial com/a/b/d/m/equals(Ljava/lang/Object;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final volatile synthetic f()I
aload 0
invokespecial com/a/b/d/m/f()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final volatile synthetic f(Ljava/lang/Object;)Z
aload 0
aload 1
invokespecial com/a/b/d/m/f(Ljava/lang/Object;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final volatile synthetic g()V
aload 0
invokespecial com/a/b/d/m/g()V
return
.limit locals 1
.limit stack 1
.end method

.method public final volatile synthetic g(Ljava/lang/Object;)Z
aload 0
aload 1
invokespecial com/a/b/d/m/g(Ljava/lang/Object;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final volatile synthetic hashCode()I
aload 0
invokespecial com/a/b/d/m/hashCode()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final volatile synthetic i()Ljava/util/Collection;
aload 0
invokespecial com/a/b/d/m/i()Ljava/util/Collection;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final volatile synthetic k()Ljava/util/Collection;
aload 0
invokespecial com/a/b/d/m/k()Ljava/util/Collection;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final volatile synthetic n()Z
aload 0
invokespecial com/a/b/d/m/n()Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final volatile synthetic p()Ljava/util/Set;
aload 0
invokespecial com/a/b/d/m/p()Ljava/util/Set;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final volatile synthetic q()Lcom/a/b/d/xc;
aload 0
invokespecial com/a/b/d/m/q()Lcom/a/b/d/xc;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final volatile synthetic toString()Ljava/lang/String;
aload 0
invokespecial com/a/b/d/m/toString()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method
