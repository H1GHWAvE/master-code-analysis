.bytecode 50.0
.class synchronized com/a/b/d/adg
.super com/a/b/d/add
.implements java/util/List

.field private static final 'a' J = 0L


.method <init>(Ljava/util/List;Ljava/lang/Object;)V
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
aload 0
aload 1
aload 2
iconst_0
invokespecial com/a/b/d/add/<init>(Ljava/util/Collection;Ljava/lang/Object;B)V
return
.limit locals 3
.limit stack 4
.end method

.method private a()Ljava/util/List;
aload 0
invokespecial com/a/b/d/add/b()Ljava/util/Collection;
checkcast java/util/List
areturn
.limit locals 1
.limit stack 1
.end method

.method public add(ILjava/lang/Object;)V
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/adg/h Ljava/lang/Object;
astore 3
aload 3
monitorenter
L0:
aload 0
invokespecial com/a/b/d/add/b()Ljava/util/Collection;
checkcast java/util/List
iload 1
aload 2
invokeinterface java/util/List/add(ILjava/lang/Object;)V 2
aload 3
monitorexit
L1:
return
L2:
astore 2
L3:
aload 3
monitorexit
L4:
aload 2
athrow
.limit locals 4
.limit stack 3
.end method

.method public addAll(ILjava/util/Collection;)Z
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/adg/h Ljava/lang/Object;
astore 4
aload 4
monitorenter
L0:
aload 0
invokespecial com/a/b/d/add/b()Ljava/util/Collection;
checkcast java/util/List
iload 1
aload 2
invokeinterface java/util/List/addAll(ILjava/util/Collection;)Z 2
istore 3
aload 4
monitorexit
L1:
iload 3
ireturn
L2:
astore 2
L3:
aload 4
monitorexit
L4:
aload 2
athrow
.limit locals 5
.limit stack 3
.end method

.method final volatile synthetic b()Ljava/util/Collection;
aload 0
invokespecial com/a/b/d/add/b()Ljava/util/Collection;
checkcast java/util/List
areturn
.limit locals 1
.limit stack 1
.end method

.method final synthetic d()Ljava/lang/Object;
aload 0
invokespecial com/a/b/d/add/b()Ljava/util/Collection;
checkcast java/util/List
areturn
.limit locals 1
.limit stack 1
.end method

.method public equals(Ljava/lang/Object;)Z
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 1
aload 0
if_acmpne L5
iconst_1
ireturn
L5:
aload 0
getfield com/a/b/d/adg/h Ljava/lang/Object;
astore 3
aload 3
monitorenter
L0:
aload 0
invokespecial com/a/b/d/add/b()Ljava/util/Collection;
checkcast java/util/List
aload 1
invokeinterface java/util/List/equals(Ljava/lang/Object;)Z 1
istore 2
aload 3
monitorexit
L1:
iload 2
ireturn
L2:
astore 1
L3:
aload 3
monitorexit
L4:
aload 1
athrow
.limit locals 4
.limit stack 2
.end method

.method public get(I)Ljava/lang/Object;
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/adg/h Ljava/lang/Object;
astore 2
aload 2
monitorenter
L0:
aload 0
invokespecial com/a/b/d/add/b()Ljava/util/Collection;
checkcast java/util/List
iload 1
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
astore 3
aload 2
monitorexit
L1:
aload 3
areturn
L2:
astore 3
L3:
aload 2
monitorexit
L4:
aload 3
athrow
.limit locals 4
.limit stack 2
.end method

.method public hashCode()I
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/adg/h Ljava/lang/Object;
astore 2
aload 2
monitorenter
L0:
aload 0
invokespecial com/a/b/d/add/b()Ljava/util/Collection;
checkcast java/util/List
invokeinterface java/util/List/hashCode()I 0
istore 1
aload 2
monitorexit
L1:
iload 1
ireturn
L2:
astore 3
L3:
aload 2
monitorexit
L4:
aload 3
athrow
.limit locals 4
.limit stack 1
.end method

.method public indexOf(Ljava/lang/Object;)I
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/adg/h Ljava/lang/Object;
astore 3
aload 3
monitorenter
L0:
aload 0
invokespecial com/a/b/d/add/b()Ljava/util/Collection;
checkcast java/util/List
aload 1
invokeinterface java/util/List/indexOf(Ljava/lang/Object;)I 1
istore 2
aload 3
monitorexit
L1:
iload 2
ireturn
L2:
astore 1
L3:
aload 3
monitorexit
L4:
aload 1
athrow
.limit locals 4
.limit stack 2
.end method

.method public lastIndexOf(Ljava/lang/Object;)I
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/adg/h Ljava/lang/Object;
astore 3
aload 3
monitorenter
L0:
aload 0
invokespecial com/a/b/d/add/b()Ljava/util/Collection;
checkcast java/util/List
aload 1
invokeinterface java/util/List/lastIndexOf(Ljava/lang/Object;)I 1
istore 2
aload 3
monitorexit
L1:
iload 2
ireturn
L2:
astore 1
L3:
aload 3
monitorexit
L4:
aload 1
athrow
.limit locals 4
.limit stack 2
.end method

.method public listIterator()Ljava/util/ListIterator;
aload 0
invokespecial com/a/b/d/add/b()Ljava/util/Collection;
checkcast java/util/List
invokeinterface java/util/List/listIterator()Ljava/util/ListIterator; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method public listIterator(I)Ljava/util/ListIterator;
aload 0
invokespecial com/a/b/d/add/b()Ljava/util/Collection;
checkcast java/util/List
iload 1
invokeinterface java/util/List/listIterator(I)Ljava/util/ListIterator; 1
areturn
.limit locals 2
.limit stack 2
.end method

.method public remove(I)Ljava/lang/Object;
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/adg/h Ljava/lang/Object;
astore 2
aload 2
monitorenter
L0:
aload 0
invokespecial com/a/b/d/add/b()Ljava/util/Collection;
checkcast java/util/List
iload 1
invokeinterface java/util/List/remove(I)Ljava/lang/Object; 1
astore 3
aload 2
monitorexit
L1:
aload 3
areturn
L2:
astore 3
L3:
aload 2
monitorexit
L4:
aload 3
athrow
.limit locals 4
.limit stack 2
.end method

.method public set(ILjava/lang/Object;)Ljava/lang/Object;
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/adg/h Ljava/lang/Object;
astore 3
aload 3
monitorenter
L0:
aload 0
invokespecial com/a/b/d/add/b()Ljava/util/Collection;
checkcast java/util/List
iload 1
aload 2
invokeinterface java/util/List/set(ILjava/lang/Object;)Ljava/lang/Object; 2
astore 2
aload 3
monitorexit
L1:
aload 2
areturn
L2:
astore 2
L3:
aload 3
monitorexit
L4:
aload 2
athrow
.limit locals 4
.limit stack 3
.end method

.method public subList(II)Ljava/util/List;
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/adg/h Ljava/lang/Object;
astore 3
aload 3
monitorenter
L0:
aload 0
invokespecial com/a/b/d/add/b()Ljava/util/Collection;
checkcast java/util/List
iload 1
iload 2
invokeinterface java/util/List/subList(II)Ljava/util/List; 2
aload 0
getfield com/a/b/d/adg/h Ljava/lang/Object;
invokestatic com/a/b/d/acu/a(Ljava/util/List;Ljava/lang/Object;)Ljava/util/List;
astore 4
aload 3
monitorexit
L1:
aload 4
areturn
L2:
astore 4
L3:
aload 3
monitorexit
L4:
aload 4
athrow
.limit locals 5
.limit stack 3
.end method
