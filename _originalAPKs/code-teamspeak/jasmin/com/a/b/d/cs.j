.bytecode 50.0
.class final synchronized com/a/b/d/cs
.super com/a/b/d/j

.field final 'a' Ljava/util/List;

.field final 'b' [I

.field final 'c' [I

.field 'd' I

.method <init>(Ljava/util/List;)V
aload 0
invokespecial com/a/b/d/j/<init>()V
aload 0
new java/util/ArrayList
dup
aload 1
invokespecial java/util/ArrayList/<init>(Ljava/util/Collection;)V
putfield com/a/b/d/cs/a Ljava/util/List;
aload 1
invokeinterface java/util/List/size()I 0
istore 2
aload 0
iload 2
newarray int
putfield com/a/b/d/cs/b [I
aload 0
iload 2
newarray int
putfield com/a/b/d/cs/c [I
aload 0
getfield com/a/b/d/cs/b [I
iconst_0
invokestatic java/util/Arrays/fill([II)V
aload 0
getfield com/a/b/d/cs/c [I
iconst_1
invokestatic java/util/Arrays/fill([II)V
aload 0
ldc_w 2147483647
putfield com/a/b/d/cs/d I
return
.limit locals 3
.limit stack 4
.end method

.method private c()Ljava/util/List;
aload 0
getfield com/a/b/d/cs/d I
ifgt L0
aload 0
invokevirtual com/a/b/d/cs/b()Ljava/lang/Object;
pop
aconst_null
areturn
L0:
aload 0
getfield com/a/b/d/cs/a Ljava/util/List;
invokestatic com/a/b/d/jl/a(Ljava/util/Collection;)Lcom/a/b/d/jl;
astore 3
aload 0
aload 0
getfield com/a/b/d/cs/a Ljava/util/List;
invokeinterface java/util/List/size()I 0
iconst_1
isub
putfield com/a/b/d/cs/d I
iconst_0
istore 1
aload 0
getfield com/a/b/d/cs/d I
iconst_m1
if_icmpeq L1
L2:
aload 0
getfield com/a/b/d/cs/b [I
aload 0
getfield com/a/b/d/cs/d I
iaload
aload 0
getfield com/a/b/d/cs/c [I
aload 0
getfield com/a/b/d/cs/d I
iaload
iadd
istore 2
iload 2
ifge L3
aload 0
invokespecial com/a/b/d/cs/e()V
goto L2
L3:
iload 2
aload 0
getfield com/a/b/d/cs/d I
iconst_1
iadd
if_icmpne L4
aload 0
getfield com/a/b/d/cs/d I
ifeq L1
iload 1
iconst_1
iadd
istore 1
aload 0
invokespecial com/a/b/d/cs/e()V
goto L2
L4:
aload 0
getfield com/a/b/d/cs/a Ljava/util/List;
aload 0
getfield com/a/b/d/cs/d I
aload 0
getfield com/a/b/d/cs/b [I
aload 0
getfield com/a/b/d/cs/d I
iaload
isub
iload 1
iadd
iload 1
aload 0
getfield com/a/b/d/cs/d I
iload 2
isub
iadd
invokestatic java/util/Collections/swap(Ljava/util/List;II)V
aload 0
getfield com/a/b/d/cs/b [I
aload 0
getfield com/a/b/d/cs/d I
iload 2
iastore
L1:
aload 3
areturn
.limit locals 4
.limit stack 5
.end method

.method private d()V
aload 0
aload 0
getfield com/a/b/d/cs/a Ljava/util/List;
invokeinterface java/util/List/size()I 0
iconst_1
isub
putfield com/a/b/d/cs/d I
iconst_0
istore 1
aload 0
getfield com/a/b/d/cs/d I
iconst_m1
if_icmpne L0
L1:
return
L0:
aload 0
getfield com/a/b/d/cs/b [I
aload 0
getfield com/a/b/d/cs/d I
iaload
aload 0
getfield com/a/b/d/cs/c [I
aload 0
getfield com/a/b/d/cs/d I
iaload
iadd
istore 2
iload 2
ifge L2
aload 0
invokespecial com/a/b/d/cs/e()V
goto L0
L2:
iload 2
aload 0
getfield com/a/b/d/cs/d I
iconst_1
iadd
if_icmpne L3
aload 0
getfield com/a/b/d/cs/d I
ifeq L1
iload 1
iconst_1
iadd
istore 1
aload 0
invokespecial com/a/b/d/cs/e()V
goto L0
L3:
aload 0
getfield com/a/b/d/cs/a Ljava/util/List;
aload 0
getfield com/a/b/d/cs/d I
aload 0
getfield com/a/b/d/cs/b [I
aload 0
getfield com/a/b/d/cs/d I
iaload
isub
iload 1
iadd
iload 1
aload 0
getfield com/a/b/d/cs/d I
iload 2
isub
iadd
invokestatic java/util/Collections/swap(Ljava/util/List;II)V
aload 0
getfield com/a/b/d/cs/b [I
aload 0
getfield com/a/b/d/cs/d I
iload 2
iastore
return
.limit locals 3
.limit stack 5
.end method

.method private e()V
aload 0
getfield com/a/b/d/cs/c [I
aload 0
getfield com/a/b/d/cs/d I
aload 0
getfield com/a/b/d/cs/c [I
aload 0
getfield com/a/b/d/cs/d I
iaload
ineg
iastore
aload 0
aload 0
getfield com/a/b/d/cs/d I
iconst_1
isub
putfield com/a/b/d/cs/d I
return
.limit locals 1
.limit stack 4
.end method

.method protected final synthetic a()Ljava/lang/Object;
aload 0
getfield com/a/b/d/cs/d I
ifgt L0
aload 0
invokevirtual com/a/b/d/cs/b()Ljava/lang/Object;
pop
aconst_null
areturn
L0:
aload 0
getfield com/a/b/d/cs/a Ljava/util/List;
invokestatic com/a/b/d/jl/a(Ljava/util/Collection;)Lcom/a/b/d/jl;
astore 3
aload 0
aload 0
getfield com/a/b/d/cs/a Ljava/util/List;
invokeinterface java/util/List/size()I 0
iconst_1
isub
putfield com/a/b/d/cs/d I
iconst_0
istore 1
aload 0
getfield com/a/b/d/cs/d I
iconst_m1
if_icmpeq L1
L2:
aload 0
getfield com/a/b/d/cs/b [I
aload 0
getfield com/a/b/d/cs/d I
iaload
aload 0
getfield com/a/b/d/cs/c [I
aload 0
getfield com/a/b/d/cs/d I
iaload
iadd
istore 2
iload 2
ifge L3
aload 0
invokespecial com/a/b/d/cs/e()V
goto L2
L3:
iload 2
aload 0
getfield com/a/b/d/cs/d I
iconst_1
iadd
if_icmpne L4
aload 0
getfield com/a/b/d/cs/d I
ifeq L1
iload 1
iconst_1
iadd
istore 1
aload 0
invokespecial com/a/b/d/cs/e()V
goto L2
L4:
aload 0
getfield com/a/b/d/cs/a Ljava/util/List;
aload 0
getfield com/a/b/d/cs/d I
aload 0
getfield com/a/b/d/cs/b [I
aload 0
getfield com/a/b/d/cs/d I
iaload
isub
iload 1
iadd
iload 1
aload 0
getfield com/a/b/d/cs/d I
iload 2
isub
iadd
invokestatic java/util/Collections/swap(Ljava/util/List;II)V
aload 0
getfield com/a/b/d/cs/b [I
aload 0
getfield com/a/b/d/cs/d I
iload 2
iastore
L1:
aload 3
areturn
.limit locals 4
.limit stack 5
.end method
