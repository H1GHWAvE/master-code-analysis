.bytecode 50.0
.class final synchronized com/a/b/d/acu
.super java/lang/Object
.annotation invisible Lcom/a/b/a/b;
b Z = 1
.end annotation

.method private <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a(Lcom/a/b/d/aac;)Lcom/a/b/d/aac;
aload 0
instanceof com/a/b/d/adr
ifne L0
aload 0
instanceof com/a/b/d/lr
ifeq L1
L0:
aload 0
areturn
L1:
new com/a/b/d/adr
dup
aload 0
invokespecial com/a/b/d/adr/<init>(Lcom/a/b/d/aac;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private static a(Lcom/a/b/d/abs;)Lcom/a/b/d/abs;
aload 0
instanceof com/a/b/d/adu
ifeq L0
aload 0
areturn
L0:
new com/a/b/d/adu
dup
aload 0
invokespecial com/a/b/d/adu/<init>(Lcom/a/b/d/abs;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private static a(Lcom/a/b/d/bw;)Lcom/a/b/d/bw;
aload 0
instanceof com/a/b/d/adc
ifne L0
aload 0
instanceof com/a/b/d/it
ifeq L1
L0:
aload 0
areturn
L1:
new com/a/b/d/adc
dup
aload 0
invokespecial com/a/b/d/adc/<init>(Lcom/a/b/d/bw;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private static a(Lcom/a/b/d/ou;)Lcom/a/b/d/ou;
aload 0
instanceof com/a/b/d/adh
ifne L0
aload 0
instanceof com/a/b/d/jr
ifeq L1
L0:
aload 0
areturn
L1:
new com/a/b/d/adh
dup
aload 0
invokespecial com/a/b/d/adh/<init>(Lcom/a/b/d/ou;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private static a(Lcom/a/b/d/vi;)Lcom/a/b/d/vi;
aload 0
instanceof com/a/b/d/adj
ifne L0
aload 0
instanceof com/a/b/d/kk
ifeq L1
L0:
aload 0
areturn
L1:
new com/a/b/d/adj
dup
aload 0
invokespecial com/a/b/d/adj/<init>(Lcom/a/b/d/vi;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private static a(Lcom/a/b/d/xc;Ljava/lang/Object;)Lcom/a/b/d/xc;
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
aload 0
instanceof com/a/b/d/adk
ifne L0
aload 0
instanceof com/a/b/d/ku
ifeq L1
L0:
aload 0
areturn
L1:
new com/a/b/d/adk
dup
aload 0
aload 1
invokespecial com/a/b/d/adk/<init>(Lcom/a/b/d/xc;Ljava/lang/Object;)V
areturn
.limit locals 2
.limit stack 4
.end method

.method static a(Ljava/util/Collection;Ljava/lang/Object;)Ljava/util/Collection;
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
new com/a/b/d/add
dup
aload 0
aload 1
iconst_0
invokespecial com/a/b/d/add/<init>(Ljava/util/Collection;Ljava/lang/Object;B)V
areturn
.limit locals 2
.limit stack 5
.end method

.method private static a(Ljava/util/Deque;)Ljava/util/Deque;
.annotation invisible Lcom/a/b/a/c;
a s = "Deque"
.end annotation
new com/a/b/d/ade
dup
aload 0
invokespecial com/a/b/d/ade/<init>(Ljava/util/Deque;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method static a(Ljava/util/List;Ljava/lang/Object;)Ljava/util/List;
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
aload 0
instanceof java/util/RandomAccess
ifeq L0
new com/a/b/d/adp
dup
aload 0
aload 1
invokespecial com/a/b/d/adp/<init>(Ljava/util/List;Ljava/lang/Object;)V
areturn
L0:
new com/a/b/d/adg
dup
aload 0
aload 1
invokespecial com/a/b/d/adg/<init>(Ljava/util/List;Ljava/lang/Object;)V
areturn
.limit locals 2
.limit stack 4
.end method

.method static synthetic a(Ljava/util/Map$Entry;Ljava/lang/Object;)Ljava/util/Map$Entry;
aload 0
ifnonnull L0
aconst_null
areturn
L0:
new com/a/b/d/adf
dup
aload 0
aload 1
invokespecial com/a/b/d/adf/<init>(Ljava/util/Map$Entry;Ljava/lang/Object;)V
areturn
.limit locals 2
.limit stack 4
.end method

.method private static a(Ljava/util/Map;Ljava/lang/Object;)Ljava/util/Map;
.annotation invisible Lcom/a/b/a/d;
.end annotation
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
new com/a/b/d/adi
dup
aload 0
aload 1
invokespecial com/a/b/d/adi/<init>(Ljava/util/Map;Ljava/lang/Object;)V
areturn
.limit locals 2
.limit stack 4
.end method

.method private static a(Ljava/util/NavigableMap;)Ljava/util/NavigableMap;
.annotation invisible Lcom/a/b/a/c;
a s = "NavigableMap"
.end annotation
aload 0
aconst_null
invokestatic com/a/b/d/acu/a(Ljava/util/NavigableMap;Ljava/lang/Object;)Ljava/util/NavigableMap;
areturn
.limit locals 1
.limit stack 2
.end method

.method static a(Ljava/util/NavigableMap;Ljava/lang/Object;)Ljava/util/NavigableMap;
.annotation invisible Lcom/a/b/a/c;
a s = "NavigableMap"
.end annotation
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
new com/a/b/d/adl
dup
aload 0
aload 1
invokespecial com/a/b/d/adl/<init>(Ljava/util/NavigableMap;Ljava/lang/Object;)V
areturn
.limit locals 2
.limit stack 4
.end method

.method private static a(Ljava/util/NavigableSet;)Ljava/util/NavigableSet;
.annotation invisible Lcom/a/b/a/c;
a s = "NavigableSet"
.end annotation
aload 0
aconst_null
invokestatic com/a/b/d/acu/a(Ljava/util/NavigableSet;Ljava/lang/Object;)Ljava/util/NavigableSet;
areturn
.limit locals 1
.limit stack 2
.end method

.method static a(Ljava/util/NavigableSet;Ljava/lang/Object;)Ljava/util/NavigableSet;
.annotation invisible Lcom/a/b/a/c;
a s = "NavigableSet"
.end annotation
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
new com/a/b/d/adm
dup
aload 0
aload 1
invokespecial com/a/b/d/adm/<init>(Ljava/util/NavigableSet;Ljava/lang/Object;)V
areturn
.limit locals 2
.limit stack 4
.end method

.method private static a(Ljava/util/Queue;)Ljava/util/Queue;
aload 0
instanceof com/a/b/d/ado
ifeq L0
aload 0
areturn
L0:
new com/a/b/d/ado
dup
aload 0
invokespecial com/a/b/d/ado/<init>(Ljava/util/Queue;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method static a(Ljava/util/Set;Ljava/lang/Object;)Ljava/util/Set;
.annotation invisible Lcom/a/b/a/d;
.end annotation
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
new com/a/b/d/adq
dup
aload 0
aload 1
invokespecial com/a/b/d/adq/<init>(Ljava/util/Set;Ljava/lang/Object;)V
areturn
.limit locals 2
.limit stack 4
.end method

.method static a(Ljava/util/SortedMap;Ljava/lang/Object;)Ljava/util/SortedMap;
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
new com/a/b/d/ads
dup
aload 0
aload 1
invokespecial com/a/b/d/ads/<init>(Ljava/util/SortedMap;Ljava/lang/Object;)V
areturn
.limit locals 2
.limit stack 4
.end method

.method static a(Ljava/util/SortedSet;Ljava/lang/Object;)Ljava/util/SortedSet;
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
new com/a/b/d/adt
dup
aload 0
aload 1
invokespecial com/a/b/d/adt/<init>(Ljava/util/SortedSet;Ljava/lang/Object;)V
areturn
.limit locals 2
.limit stack 4
.end method

.method static synthetic b(Ljava/util/Collection;Ljava/lang/Object;)Ljava/util/Collection;
aload 0
instanceof java/util/SortedSet
ifeq L0
aload 0
checkcast java/util/SortedSet
aload 1
invokestatic com/a/b/d/acu/a(Ljava/util/SortedSet;Ljava/lang/Object;)Ljava/util/SortedSet;
areturn
L0:
aload 0
instanceof java/util/Set
ifeq L1
aload 0
checkcast java/util/Set
aload 1
invokestatic com/a/b/d/acu/a(Ljava/util/Set;Ljava/lang/Object;)Ljava/util/Set;
areturn
L1:
aload 0
instanceof java/util/List
ifeq L2
aload 0
checkcast java/util/List
aload 1
invokestatic com/a/b/d/acu/a(Ljava/util/List;Ljava/lang/Object;)Ljava/util/List;
areturn
L2:
aload 0
aload 1
invokestatic com/a/b/d/acu/a(Ljava/util/Collection;Ljava/lang/Object;)Ljava/util/Collection;
areturn
.limit locals 2
.limit stack 2
.end method

.method private static synthetic b(Ljava/util/List;Ljava/lang/Object;)Ljava/util/List;
aload 0
aload 1
invokestatic com/a/b/d/acu/a(Ljava/util/List;Ljava/lang/Object;)Ljava/util/List;
areturn
.limit locals 2
.limit stack 2
.end method

.method private static b(Ljava/util/Map$Entry;Ljava/lang/Object;)Ljava/util/Map$Entry;
.annotation invisible Lcom/a/b/a/c;
a s = "works but is needed only for NavigableMap"
.end annotation
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
aload 0
ifnonnull L0
aconst_null
areturn
L0:
new com/a/b/d/adf
dup
aload 0
aload 1
invokespecial com/a/b/d/adf/<init>(Ljava/util/Map$Entry;Ljava/lang/Object;)V
areturn
.limit locals 2
.limit stack 4
.end method

.method static synthetic b(Ljava/util/Set;Ljava/lang/Object;)Ljava/util/Set;
aload 0
instanceof java/util/SortedSet
ifeq L0
aload 0
checkcast java/util/SortedSet
aload 1
invokestatic com/a/b/d/acu/a(Ljava/util/SortedSet;Ljava/lang/Object;)Ljava/util/SortedSet;
areturn
L0:
aload 0
aload 1
invokestatic com/a/b/d/acu/a(Ljava/util/Set;Ljava/lang/Object;)Ljava/util/Set;
areturn
.limit locals 2
.limit stack 2
.end method

.method private static synthetic b(Ljava/util/SortedSet;Ljava/lang/Object;)Ljava/util/SortedSet;
aload 0
aload 1
invokestatic com/a/b/d/acu/a(Ljava/util/SortedSet;Ljava/lang/Object;)Ljava/util/SortedSet;
areturn
.limit locals 2
.limit stack 2
.end method

.method private static c(Ljava/util/Collection;Ljava/lang/Object;)Ljava/util/Collection;
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
aload 0
instanceof java/util/SortedSet
ifeq L0
aload 0
checkcast java/util/SortedSet
aload 1
invokestatic com/a/b/d/acu/a(Ljava/util/SortedSet;Ljava/lang/Object;)Ljava/util/SortedSet;
areturn
L0:
aload 0
instanceof java/util/Set
ifeq L1
aload 0
checkcast java/util/Set
aload 1
invokestatic com/a/b/d/acu/a(Ljava/util/Set;Ljava/lang/Object;)Ljava/util/Set;
areturn
L1:
aload 0
instanceof java/util/List
ifeq L2
aload 0
checkcast java/util/List
aload 1
invokestatic com/a/b/d/acu/a(Ljava/util/List;Ljava/lang/Object;)Ljava/util/List;
areturn
L2:
aload 0
aload 1
invokestatic com/a/b/d/acu/a(Ljava/util/Collection;Ljava/lang/Object;)Ljava/util/Collection;
areturn
.limit locals 2
.limit stack 2
.end method

.method private static c(Ljava/util/Set;Ljava/lang/Object;)Ljava/util/Set;
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
aload 0
instanceof java/util/SortedSet
ifeq L0
aload 0
checkcast java/util/SortedSet
aload 1
invokestatic com/a/b/d/acu/a(Ljava/util/SortedSet;Ljava/lang/Object;)Ljava/util/SortedSet;
areturn
L0:
aload 0
aload 1
invokestatic com/a/b/d/acu/a(Ljava/util/Set;Ljava/lang/Object;)Ljava/util/Set;
areturn
.limit locals 2
.limit stack 2
.end method

.method private static synthetic d(Ljava/util/Collection;Ljava/lang/Object;)Ljava/util/Collection;
aload 0
aload 1
invokestatic com/a/b/d/acu/a(Ljava/util/Collection;Ljava/lang/Object;)Ljava/util/Collection;
areturn
.limit locals 2
.limit stack 2
.end method
