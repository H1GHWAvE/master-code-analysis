.bytecode 50.0
.class public final synchronized com/a/b/d/io
.super com/a/b/d/ba
.annotation invisible Lcom/a/b/a/b;
a Z = 1
b Z = 1
.end annotation

.field private static final 'b' I = 2


.field private static final 'c' J = 0L

.annotation invisible Lcom/a/b/a/c;
a s = "Not needed in emulated source"
.end annotation
.end field

.field transient 'a' I
.annotation invisible Lcom/a/b/a/d;
.end annotation
.end field

.method private <init>()V
aload 0
new java/util/HashMap
dup
invokespecial java/util/HashMap/<init>()V
invokespecial com/a/b/d/ba/<init>(Ljava/util/Map;)V
aload 0
iconst_2
putfield com/a/b/d/io/a I
return
.limit locals 1
.limit stack 3
.end method

.method private <init>(II)V
aload 0
iload 1
invokestatic com/a/b/d/sz/a(I)Ljava/util/HashMap;
invokespecial com/a/b/d/ba/<init>(Ljava/util/Map;)V
aload 0
iconst_2
putfield com/a/b/d/io/a I
iload 2
iflt L0
iconst_1
istore 3
L1:
iload 3
invokestatic com/a/b/b/cn/a(Z)V
aload 0
iload 2
putfield com/a/b/d/io/a I
return
L0:
iconst_0
istore 3
goto L1
.limit locals 4
.limit stack 2
.end method

.method private <init>(Lcom/a/b/d/vi;)V
aload 0
aload 1
invokeinterface com/a/b/d/vi/p()Ljava/util/Set; 0
invokeinterface java/util/Set/size()I 0
invokestatic com/a/b/d/sz/a(I)Ljava/util/HashMap;
invokespecial com/a/b/d/ba/<init>(Ljava/util/Map;)V
aload 0
iconst_2
putfield com/a/b/d/io/a I
aload 0
aload 1
invokevirtual com/a/b/d/io/a(Lcom/a/b/d/vi;)Z
pop
return
.limit locals 2
.limit stack 2
.end method

.method private static a(II)Lcom/a/b/d/io;
new com/a/b/d/io
dup
iload 0
iload 1
invokespecial com/a/b/d/io/<init>(II)V
areturn
.limit locals 2
.limit stack 4
.end method

.method private a(Ljava/io/ObjectInputStream;)V
.annotation invisible Lcom/a/b/a/c;
a s = "java.io.ObjectInputStream"
.end annotation
aload 1
invokevirtual java/io/ObjectInputStream/defaultReadObject()V
aload 0
aload 1
invokevirtual java/io/ObjectInputStream/readInt()I
putfield com/a/b/d/io/a I
aload 1
invokevirtual java/io/ObjectInputStream/readInt()I
istore 2
aload 0
iload 2
invokestatic com/a/b/d/sz/a(I)Ljava/util/HashMap;
invokevirtual com/a/b/d/io/a(Ljava/util/Map;)V
aload 0
aload 1
iload 2
invokestatic com/a/b/d/zz/a(Lcom/a/b/d/vi;Ljava/io/ObjectInputStream;I)V
return
.limit locals 3
.limit stack 3
.end method

.method private a(Ljava/io/ObjectOutputStream;)V
.annotation invisible Lcom/a/b/a/c;
a s = "java.io.ObjectOutputStream"
.end annotation
aload 1
invokevirtual java/io/ObjectOutputStream/defaultWriteObject()V
aload 1
aload 0
getfield com/a/b/d/io/a I
invokevirtual java/io/ObjectOutputStream/writeInt(I)V
aload 0
aload 1
invokestatic com/a/b/d/zz/a(Lcom/a/b/d/vi;Ljava/io/ObjectOutputStream;)V
return
.limit locals 2
.limit stack 2
.end method

.method private static b(Lcom/a/b/d/vi;)Lcom/a/b/d/io;
new com/a/b/d/io
dup
aload 0
invokespecial com/a/b/d/io/<init>(Lcom/a/b/d/vi;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method public static v()Lcom/a/b/d/io;
new com/a/b/d/io
dup
invokespecial com/a/b/d/io/<init>()V
areturn
.limit locals 0
.limit stack 2
.end method

.method final a()Ljava/util/Set;
aload 0
getfield com/a/b/d/io/a I
invokestatic com/a/b/d/aad/a(I)Ljava/util/HashSet;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final volatile synthetic a(Ljava/lang/Object;)Ljava/util/Set;
aload 0
aload 1
invokespecial com/a/b/d/ba/a(Ljava/lang/Object;)Ljava/util/Set;
areturn
.limit locals 2
.limit stack 2
.end method

.method public final volatile synthetic a(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/Set;
aload 0
aload 1
aload 2
invokespecial com/a/b/d/ba/a(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/Set;
areturn
.limit locals 3
.limit stack 3
.end method

.method public final volatile synthetic a(Lcom/a/b/d/vi;)Z
aload 0
aload 1
invokespecial com/a/b/d/ba/a(Lcom/a/b/d/vi;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final volatile synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Z
aload 0
aload 1
aload 2
invokespecial com/a/b/d/ba/a(Ljava/lang/Object;Ljava/lang/Object;)Z
ireturn
.limit locals 3
.limit stack 3
.end method

.method public final volatile synthetic b()Ljava/util/Map;
aload 0
invokespecial com/a/b/d/ba/b()Ljava/util/Map;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final volatile synthetic b(Ljava/lang/Object;)Ljava/util/Set;
aload 0
aload 1
invokespecial com/a/b/d/ba/b(Ljava/lang/Object;)Ljava/util/Set;
areturn
.limit locals 2
.limit stack 2
.end method

.method public final volatile synthetic b(Ljava/lang/Object;Ljava/lang/Object;)Z
aload 0
aload 1
aload 2
invokespecial com/a/b/d/ba/b(Ljava/lang/Object;Ljava/lang/Object;)Z
ireturn
.limit locals 3
.limit stack 3
.end method

.method final synthetic c()Ljava/util/Collection;
aload 0
getfield com/a/b/d/io/a I
invokestatic com/a/b/d/aad/a(I)Ljava/util/HashSet;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final volatile synthetic c(Ljava/lang/Object;Ljava/lang/Iterable;)Z
aload 0
aload 1
aload 2
invokespecial com/a/b/d/ba/c(Ljava/lang/Object;Ljava/lang/Iterable;)Z
ireturn
.limit locals 3
.limit stack 3
.end method

.method public final volatile synthetic c(Ljava/lang/Object;Ljava/lang/Object;)Z
aload 0
aload 1
aload 2
invokespecial com/a/b/d/ba/c(Ljava/lang/Object;Ljava/lang/Object;)Z
ireturn
.limit locals 3
.limit stack 3
.end method

.method public final volatile synthetic equals(Ljava/lang/Object;)Z
aload 0
aload 1
invokespecial com/a/b/d/ba/equals(Ljava/lang/Object;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final volatile synthetic f()I
aload 0
invokespecial com/a/b/d/ba/f()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final volatile synthetic f(Ljava/lang/Object;)Z
aload 0
aload 1
invokespecial com/a/b/d/ba/f(Ljava/lang/Object;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final volatile synthetic g()V
aload 0
invokespecial com/a/b/d/ba/g()V
return
.limit locals 1
.limit stack 1
.end method

.method public final volatile synthetic g(Ljava/lang/Object;)Z
aload 0
aload 1
invokespecial com/a/b/d/ba/g(Ljava/lang/Object;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final volatile synthetic hashCode()I
aload 0
invokespecial com/a/b/d/ba/hashCode()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final volatile synthetic i()Ljava/util/Collection;
aload 0
invokespecial com/a/b/d/ba/i()Ljava/util/Collection;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final volatile synthetic n()Z
aload 0
invokespecial com/a/b/d/ba/n()Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final volatile synthetic p()Ljava/util/Set;
aload 0
invokespecial com/a/b/d/ba/p()Ljava/util/Set;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final volatile synthetic q()Lcom/a/b/d/xc;
aload 0
invokespecial com/a/b/d/ba/q()Lcom/a/b/d/xc;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final volatile synthetic toString()Ljava/lang/String;
aload 0
invokespecial com/a/b/d/ba/toString()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final volatile synthetic u()Ljava/util/Set;
aload 0
invokespecial com/a/b/d/ba/u()Ljava/util/Set;
areturn
.limit locals 1
.limit stack 1
.end method
