.bytecode 50.0
.class public final synchronized com/a/b/d/mj
.super java/lang/Object

.field private final 'a' Ljava/util/List;

.field private 'b' Ljava/util/Comparator;

.field private 'c' Ljava/util/Comparator;

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield com/a/b/d/mj/a Ljava/util/List;
return
.limit locals 1
.limit stack 3
.end method

.method private a()Lcom/a/b/d/mi;
aload 0
getfield com/a/b/d/mj/a Ljava/util/List;
invokeinterface java/util/List/size()I 0
tableswitch 0
L0
L1
default : L2
L2:
aload 0
getfield com/a/b/d/mj/a Ljava/util/List;
aload 0
getfield com/a/b/d/mj/b Ljava/util/Comparator;
aload 0
getfield com/a/b/d/mj/c Ljava/util/Comparator;
invokestatic com/a/b/d/zr/a(Ljava/util/List;Ljava/util/Comparator;Ljava/util/Comparator;)Lcom/a/b/d/zr;
areturn
L0:
invokestatic com/a/b/d/mi/p()Lcom/a/b/d/mi;
areturn
L1:
new com/a/b/d/aax
dup
aload 0
getfield com/a/b/d/mj/a Ljava/util/List;
invokestatic com/a/b/d/mq/b(Ljava/lang/Iterable;)Ljava/lang/Object;
checkcast com/a/b/d/adw
invokespecial com/a/b/d/aax/<init>(Lcom/a/b/d/adw;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private a(Lcom/a/b/d/adv;)Lcom/a/b/d/mj;
aload 1
invokeinterface com/a/b/d/adv/e()Ljava/util/Set; 0
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
astore 1
L0:
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/a/b/d/adw
astore 4
aload 4
instanceof com/a/b/d/aea
ifeq L2
aload 4
invokeinterface com/a/b/d/adw/a()Ljava/lang/Object; 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 4
invokeinterface com/a/b/d/adw/b()Ljava/lang/Object; 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 4
invokeinterface com/a/b/d/adw/c()Ljava/lang/Object; 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
getfield com/a/b/d/mj/a Ljava/util/List;
aload 4
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
goto L0
L2:
aload 4
invokeinterface com/a/b/d/adw/a()Ljava/lang/Object; 0
astore 2
aload 4
invokeinterface com/a/b/d/adw/b()Ljava/lang/Object; 0
astore 3
aload 4
invokeinterface com/a/b/d/adw/c()Ljava/lang/Object; 0
astore 4
aload 0
getfield com/a/b/d/mj/a Ljava/util/List;
aload 2
aload 3
aload 4
invokestatic com/a/b/d/mi/b(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/adw;
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
goto L0
L1:
aload 0
areturn
.limit locals 5
.limit stack 4
.end method

.method private a(Lcom/a/b/d/adw;)Lcom/a/b/d/mj;
aload 1
instanceof com/a/b/d/aea
ifeq L0
aload 1
invokeinterface com/a/b/d/adw/a()Ljava/lang/Object; 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 1
invokeinterface com/a/b/d/adw/b()Ljava/lang/Object; 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 1
invokeinterface com/a/b/d/adw/c()Ljava/lang/Object; 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
getfield com/a/b/d/mj/a Ljava/util/List;
aload 1
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
aload 0
areturn
L0:
aload 1
invokeinterface com/a/b/d/adw/a()Ljava/lang/Object; 0
astore 2
aload 1
invokeinterface com/a/b/d/adw/b()Ljava/lang/Object; 0
astore 3
aload 1
invokeinterface com/a/b/d/adw/c()Ljava/lang/Object; 0
astore 1
aload 0
getfield com/a/b/d/mj/a Ljava/util/List;
aload 2
aload 3
aload 1
invokestatic com/a/b/d/mi/b(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/adw;
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
aload 0
areturn
.limit locals 4
.limit stack 4
.end method

.method private a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/mj;
aload 0
getfield com/a/b/d/mj/a Ljava/util/List;
aload 1
aload 2
aload 3
invokestatic com/a/b/d/mi/b(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/adw;
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
aload 0
areturn
.limit locals 4
.limit stack 4
.end method

.method private a(Ljava/util/Comparator;)Lcom/a/b/d/mj;
aload 0
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/util/Comparator
putfield com/a/b/d/mj/b Ljava/util/Comparator;
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method private b(Ljava/util/Comparator;)Lcom/a/b/d/mj;
aload 0
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/util/Comparator
putfield com/a/b/d/mj/c Ljava/util/Comparator;
aload 0
areturn
.limit locals 2
.limit stack 2
.end method
