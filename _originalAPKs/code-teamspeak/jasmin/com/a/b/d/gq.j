.bytecode 50.0
.class public synchronized abstract com/a/b/d/gq
.super com/a/b/d/go
.implements java/util/ListIterator
.annotation invisible Lcom/a/b/a/b;
.end annotation

.method protected <init>()V
aload 0
invokespecial com/a/b/d/go/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method protected synthetic a()Ljava/util/Iterator;
aload 0
invokevirtual com/a/b/d/gq/b()Ljava/util/ListIterator;
areturn
.limit locals 1
.limit stack 1
.end method

.method public add(Ljava/lang/Object;)V
aload 0
invokevirtual com/a/b/d/gq/b()Ljava/util/ListIterator;
aload 1
invokeinterface java/util/ListIterator/add(Ljava/lang/Object;)V 1
return
.limit locals 2
.limit stack 2
.end method

.method protected abstract b()Ljava/util/ListIterator;
.end method

.method public hasPrevious()Z
aload 0
invokevirtual com/a/b/d/gq/b()Ljava/util/ListIterator;
invokeinterface java/util/ListIterator/hasPrevious()Z 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method protected synthetic k_()Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/gq/b()Ljava/util/ListIterator;
areturn
.limit locals 1
.limit stack 1
.end method

.method public nextIndex()I
aload 0
invokevirtual com/a/b/d/gq/b()Ljava/util/ListIterator;
invokeinterface java/util/ListIterator/nextIndex()I 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public previous()Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/gq/b()Ljava/util/ListIterator;
invokeinterface java/util/ListIterator/previous()Ljava/lang/Object; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method public previousIndex()I
aload 0
invokevirtual com/a/b/d/gq/b()Ljava/util/ListIterator;
invokeinterface java/util/ListIterator/previousIndex()I 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public set(Ljava/lang/Object;)V
aload 0
invokevirtual com/a/b/d/gq/b()Ljava/util/ListIterator;
aload 1
invokeinterface java/util/ListIterator/set(Ljava/lang/Object;)V 1
return
.limit locals 2
.limit stack 2
.end method
