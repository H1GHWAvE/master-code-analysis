.bytecode 50.0
.class final synchronized com/a/b/d/afc
.super java/lang/Object
.implements com/a/b/d/yq

.method <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public final a()Lcom/a/b/d/yl;
new java/util/NoSuchElementException
dup
invokespecial java/util/NoSuchElementException/<init>()V
athrow
.limit locals 1
.limit stack 2
.end method

.method public final a(Ljava/lang/Comparable;)Ljava/lang/Object;
.annotation visible Ljavax/annotation/Nullable;
.end annotation
aconst_null
areturn
.limit locals 2
.limit stack 1
.end method

.method public final a(Lcom/a/b/d/yl;)V
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
return
.limit locals 2
.limit stack 1
.end method

.method public final a(Lcom/a/b/d/yl;Ljava/lang/Object;)V
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 1
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 1
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
aload 1
invokevirtual java/lang/String/length()I
bipush 46
iadd
invokespecial java/lang/StringBuilder/<init>(I)V
ldc "Cannot insert range "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " into an empty subRangeMap"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
.limit locals 3
.limit stack 6
.end method

.method public final a(Lcom/a/b/d/yq;)V
aload 1
invokeinterface com/a/b/d/yq/d()Ljava/util/Map; 0
invokeinterface java/util/Map/isEmpty()Z 0
ifne L0
new java/lang/IllegalArgumentException
dup
ldc "Cannot putAll(nonEmptyRangeMap) into an empty subRangeMap"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
return
.limit locals 2
.limit stack 3
.end method

.method public final b(Ljava/lang/Comparable;)Ljava/util/Map$Entry;
.annotation visible Ljavax/annotation/Nullable;
.end annotation
aconst_null
areturn
.limit locals 2
.limit stack 1
.end method

.method public final b()V
return
.limit locals 1
.limit stack 0
.end method

.method public final c(Lcom/a/b/d/yl;)Lcom/a/b/d/yq;
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
areturn
.limit locals 2
.limit stack 1
.end method

.method public final d()Ljava/util/Map;
invokestatic java/util/Collections/emptyMap()Ljava/util/Map;
areturn
.limit locals 1
.limit stack 1
.end method
