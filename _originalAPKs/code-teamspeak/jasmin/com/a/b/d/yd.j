.bytecode 50.0
.class public synchronized abstract com/a/b/d/yd
.super java/lang/Object
.implements java/util/Comparator
.annotation invisible Lcom/a/b/a/b;
.end annotation

.field static final 'c' I = 1


.field static final 'd' I = -1


.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private a(Ljava/util/List;Ljava/lang/Object;)I
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
aload 1
aload 2
aload 0
invokestatic java/util/Collections/binarySearch(Ljava/util/List;Ljava/lang/Object;Ljava/util/Comparator;)I
ireturn
.limit locals 3
.limit stack 3
.end method

.method private a([Ljava/lang/Object;III)I
aload 1
iload 4
aaload
astore 6
aload 1
iload 4
aload 1
iload 3
aaload
aastore
aload 1
iload 3
aload 6
aastore
iload 2
istore 4
L0:
iload 2
iload 3
if_icmpge L1
iload 4
istore 5
aload 0
aload 1
iload 2
aaload
aload 6
invokevirtual com/a/b/d/yd/compare(Ljava/lang/Object;Ljava/lang/Object;)I
ifge L2
aload 1
iload 4
iload 2
invokestatic com/a/b/d/yc/a([Ljava/lang/Object;II)V
iload 4
iconst_1
iadd
istore 5
L2:
iload 2
iconst_1
iadd
istore 2
iload 5
istore 4
goto L0
L1:
aload 1
iload 3
iload 4
invokestatic com/a/b/d/yc/a([Ljava/lang/Object;II)V
iload 4
ireturn
.limit locals 7
.limit stack 4
.end method

.method private static a(Lcom/a/b/d/yd;)Lcom/a/b/d/yd;
.annotation visible Ljava/lang/Deprecated;
.end annotation
.annotation invisible Lcom/a/b/a/b;
a Z = 1
.end annotation
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast com/a/b/d/yd
areturn
.limit locals 1
.limit stack 1
.end method

.method private static transient a(Ljava/lang/Object;[Ljava/lang/Object;)Lcom/a/b/d/yd;
.annotation invisible Lcom/a/b/a/b;
a Z = 1
.end annotation
new com/a/b/d/fh
dup
new com/a/b/d/pa
dup
aload 0
aload 1
invokespecial com/a/b/d/pa/<init>(Ljava/lang/Object;[Ljava/lang/Object;)V
invokespecial com/a/b/d/fh/<init>(Ljava/util/List;)V
areturn
.limit locals 2
.limit stack 6
.end method

.method public static a(Ljava/util/Comparator;)Lcom/a/b/d/yd;
.annotation invisible Lcom/a/b/a/b;
a Z = 1
.end annotation
aload 0
instanceof com/a/b/d/yd
ifeq L0
aload 0
checkcast com/a/b/d/yd
areturn
L0:
new com/a/b/d/cu
dup
aload 0
invokespecial com/a/b/d/cu/<init>(Ljava/util/Comparator;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private static a(Ljava/util/List;)Lcom/a/b/d/yd;
.annotation invisible Lcom/a/b/a/b;
a Z = 1
.end annotation
new com/a/b/d/fh
dup
aload 0
invokespecial com/a/b/d/fh/<init>(Ljava/util/List;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private a(Ljava/lang/Iterable;I)Ljava/util/List;
aload 1
instanceof java/util/Collection
ifeq L0
aload 1
checkcast java/util/Collection
astore 3
aload 3
invokeinterface java/util/Collection/size()I 0
i2l
ldc2_w 2L
iload 2
i2l
lmul
lcmp
ifgt L0
aload 3
invokeinterface java/util/Collection/toArray()[Ljava/lang/Object; 0
checkcast [Ljava/lang/Object;
astore 3
aload 3
aload 0
invokestatic java/util/Arrays/sort([Ljava/lang/Object;Ljava/util/Comparator;)V
aload 3
astore 1
aload 3
arraylength
iload 2
if_icmple L1
aload 3
iload 2
invokestatic com/a/b/d/yc/b([Ljava/lang/Object;I)[Ljava/lang/Object;
astore 1
L1:
aload 1
invokestatic java/util/Arrays/asList([Ljava/lang/Object;)Ljava/util/List;
invokestatic java/util/Collections/unmodifiableList(Ljava/util/List;)Ljava/util/List;
areturn
L0:
aload 0
aload 1
invokeinterface java/lang/Iterable/iterator()Ljava/util/Iterator; 0
iload 2
invokespecial com/a/b/d/yd/a(Ljava/util/Iterator;I)Ljava/util/List;
areturn
.limit locals 4
.limit stack 6
.end method

.method private a(Ljava/util/Iterator;I)Ljava/util/List;
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
iload 2
ldc "k"
invokestatic com/a/b/d/cl/a(ILjava/lang/String;)I
pop
iload 2
ifeq L0
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifne L1
L0:
invokestatic com/a/b/d/jl/d()Lcom/a/b/d/jl;
areturn
L1:
iload 2
ldc_w 1073741823
if_icmplt L2
aload 1
invokestatic com/a/b/d/ov/a(Ljava/util/Iterator;)Ljava/util/ArrayList;
astore 1
aload 1
aload 0
invokestatic java/util/Collections/sort(Ljava/util/List;Ljava/util/Comparator;)V
aload 1
invokevirtual java/util/ArrayList/size()I
iload 2
if_icmple L3
aload 1
iload 2
aload 1
invokevirtual java/util/ArrayList/size()I
invokevirtual java/util/ArrayList/subList(II)Ljava/util/List;
invokeinterface java/util/List/clear()V 0
L3:
aload 1
invokevirtual java/util/ArrayList/trimToSize()V
aload 1
invokestatic java/util/Collections/unmodifiableList(Ljava/util/List;)Ljava/util/List;
areturn
L2:
iload 2
iconst_2
imul
istore 9
iload 9
anewarray java/lang/Object
checkcast [Ljava/lang/Object;
astore 12
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
astore 10
aload 12
iconst_0
aload 10
aastore
iconst_1
istore 4
L4:
iload 4
istore 3
aload 10
astore 11
iload 4
iload 2
if_icmpge L5
iload 4
istore 3
aload 10
astore 11
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L5
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
astore 11
aload 12
iload 4
aload 11
aastore
aload 0
aload 10
aload 11
invokevirtual com/a/b/d/yd/b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
astore 10
iload 4
iconst_1
iadd
istore 4
goto L4
L6:
iload 2
istore 3
aload 10
astore 11
L5:
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L7
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
astore 10
aload 0
aload 10
aload 11
invokevirtual com/a/b/d/yd/compare(Ljava/lang/Object;Ljava/lang/Object;)I
ifge L5
iload 3
iconst_1
iadd
istore 4
aload 12
iload 3
aload 10
aastore
iload 4
iload 9
if_icmpne L8
iconst_0
istore 6
iload 9
iconst_1
isub
istore 5
iconst_0
istore 3
L9:
iload 3
iload 5
if_icmpge L10
iload 3
iload 5
iadd
iconst_1
iadd
iconst_1
iushr
istore 4
aload 12
iload 4
aaload
astore 10
aload 12
iload 4
aload 12
iload 5
aaload
aastore
aload 12
iload 5
aload 10
aastore
iload 3
istore 4
iload 3
istore 7
L11:
iload 7
iload 5
if_icmpge L12
iload 4
istore 8
aload 0
aload 12
iload 7
aaload
aload 10
invokevirtual com/a/b/d/yd/compare(Ljava/lang/Object;Ljava/lang/Object;)I
ifge L13
aload 12
iload 4
iload 7
invokestatic com/a/b/d/yc/a([Ljava/lang/Object;II)V
iload 4
iconst_1
iadd
istore 8
L13:
iload 7
iconst_1
iadd
istore 7
iload 8
istore 4
goto L11
L12:
aload 12
iload 5
iload 4
invokestatic com/a/b/d/yc/a([Ljava/lang/Object;II)V
iload 4
iload 2
if_icmple L14
iload 4
iconst_1
isub
istore 5
goto L9
L14:
iload 4
iload 2
if_icmpge L10
iload 4
iload 3
iconst_1
iadd
invokestatic java/lang/Math/max(II)I
istore 3
iload 4
istore 6
goto L9
L10:
aload 12
iload 6
aaload
astore 10
iload 6
iconst_1
iadd
istore 3
L15:
iload 3
iload 2
if_icmpge L6
aload 0
aload 10
aload 12
iload 3
aaload
invokevirtual com/a/b/d/yd/b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
astore 10
iload 3
iconst_1
iadd
istore 3
goto L15
L7:
aload 12
iconst_0
iload 3
aload 0
invokestatic java/util/Arrays/sort([Ljava/lang/Object;IILjava/util/Comparator;)V
aload 12
iload 3
iload 2
invokestatic java/lang/Math/min(II)I
invokestatic com/a/b/d/yc/b([Ljava/lang/Object;I)[Ljava/lang/Object;
invokestatic java/util/Arrays/asList([Ljava/lang/Object;)Ljava/util/List;
invokestatic java/util/Collections/unmodifiableList(Ljava/util/List;)Ljava/util/List;
areturn
L8:
iload 4
istore 3
goto L5
.limit locals 13
.limit stack 4
.end method

.method private b(Ljava/util/Comparator;)Lcom/a/b/d/yd;
.annotation invisible Lcom/a/b/a/b;
a Z = 1
.end annotation
new com/a/b/d/cy
dup
aload 0
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/util/Comparator
invokespecial com/a/b/d/cy/<init>(Ljava/util/Comparator;Ljava/util/Comparator;)V
areturn
.limit locals 2
.limit stack 4
.end method

.method private b(Ljava/lang/Iterable;I)Ljava/util/List;
aload 0
invokevirtual com/a/b/d/yd/a()Lcom/a/b/d/yd;
astore 4
aload 1
instanceof java/util/Collection
ifeq L0
aload 1
checkcast java/util/Collection
astore 3
aload 3
invokeinterface java/util/Collection/size()I 0
i2l
ldc2_w 2L
iload 2
i2l
lmul
lcmp
ifgt L0
aload 3
invokeinterface java/util/Collection/toArray()[Ljava/lang/Object; 0
checkcast [Ljava/lang/Object;
astore 3
aload 3
aload 4
invokestatic java/util/Arrays/sort([Ljava/lang/Object;Ljava/util/Comparator;)V
aload 3
astore 1
aload 3
arraylength
iload 2
if_icmple L1
aload 3
iload 2
invokestatic com/a/b/d/yc/b([Ljava/lang/Object;I)[Ljava/lang/Object;
astore 1
L1:
aload 1
invokestatic java/util/Arrays/asList([Ljava/lang/Object;)Ljava/util/List;
invokestatic java/util/Collections/unmodifiableList(Ljava/util/List;)Ljava/util/List;
areturn
L0:
aload 4
aload 1
invokeinterface java/lang/Iterable/iterator()Ljava/util/Iterator; 0
iload 2
invokespecial com/a/b/d/yd/a(Ljava/util/Iterator;I)Ljava/util/List;
areturn
.limit locals 5
.limit stack 6
.end method

.method private b(Ljava/util/Iterator;I)Ljava/util/List;
aload 0
invokevirtual com/a/b/d/yd/a()Lcom/a/b/d/yd;
aload 1
iload 2
invokespecial com/a/b/d/yd/a(Ljava/util/Iterator;I)Ljava/util/List;
areturn
.limit locals 3
.limit stack 3
.end method

.method public static d()Lcom/a/b/d/yd;
.annotation invisible Lcom/a/b/a/b;
a Z = 1
.end annotation
getstatic com/a/b/d/xz/a Lcom/a/b/d/xz;
areturn
.limit locals 0
.limit stack 1
.end method

.method public static e()Lcom/a/b/d/yd;
.annotation invisible Lcom/a/b/a/b;
a Z = 1
.end annotation
getstatic com/a/b/d/agl/a Lcom/a/b/d/agl;
areturn
.limit locals 0
.limit stack 1
.end method

.method private static e(Ljava/lang/Iterable;)Lcom/a/b/d/yd;
.annotation invisible Lcom/a/b/a/b;
a Z = 1
.end annotation
new com/a/b/d/cy
dup
aload 0
invokespecial com/a/b/d/cy/<init>(Ljava/lang/Iterable;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private static f()Lcom/a/b/d/yd;
.annotation invisible Lcom/a/b/a/b;
a Z = 1
.end annotation
getstatic com/a/b/d/bj/a Lcom/a/b/d/bj;
areturn
.limit locals 0
.limit stack 1
.end method

.method private f(Ljava/lang/Iterable;)Z
aload 1
invokeinterface java/lang/Iterable/iterator()Ljava/util/Iterator; 0
astore 3
aload 3
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L0
aload 3
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
astore 1
L1:
aload 3
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L0
aload 3
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
astore 2
aload 0
aload 1
aload 2
invokevirtual com/a/b/d/yd/compare(Ljava/lang/Object;Ljava/lang/Object;)I
ifle L2
iconst_0
ireturn
L2:
aload 2
astore 1
goto L1
L0:
iconst_1
ireturn
.limit locals 4
.limit stack 3
.end method

.method private static g()Lcom/a/b/d/yd;
getstatic com/a/b/d/yg/a Lcom/a/b/d/yd;
areturn
.limit locals 0
.limit stack 1
.end method

.method private g(Ljava/lang/Iterable;)Z
aload 1
invokeinterface java/lang/Iterable/iterator()Ljava/util/Iterator; 0
astore 3
aload 3
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L0
aload 3
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
astore 1
L1:
aload 3
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L0
aload 3
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
astore 2
aload 0
aload 1
aload 2
invokevirtual com/a/b/d/yd/compare(Ljava/lang/Object;Ljava/lang/Object;)I
iflt L2
iconst_0
ireturn
L2:
aload 2
astore 1
goto L1
L0:
iconst_1
ireturn
.limit locals 4
.limit stack 3
.end method

.method private h()Lcom/a/b/d/yd;
aload 0
invokestatic com/a/b/d/sz/a()Lcom/a/b/b/bj;
invokevirtual com/a/b/d/yd/a(Lcom/a/b/b/bj;)Lcom/a/b/d/yd;
areturn
.limit locals 1
.limit stack 2
.end method

.method private i()Lcom/a/b/d/yd;
.annotation invisible Lcom/a/b/a/b;
a Z = 1
.end annotation
new com/a/b/d/ob
dup
aload 0
invokespecial com/a/b/d/ob/<init>(Lcom/a/b/d/yd;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method public a()Lcom/a/b/d/yd;
.annotation invisible Lcom/a/b/a/b;
a Z = 1
.end annotation
new com/a/b/d/zx
dup
aload 0
invokespecial com/a/b/d/zx/<init>(Lcom/a/b/d/yd;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method public final a(Lcom/a/b/b/bj;)Lcom/a/b/d/yd;
.annotation invisible Lcom/a/b/a/b;
a Z = 1
.end annotation
new com/a/b/d/ch
dup
aload 1
aload 0
invokespecial com/a/b/d/ch/<init>(Lcom/a/b/b/bj;Lcom/a/b/d/yd;)V
areturn
.limit locals 2
.limit stack 4
.end method

.method public a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
aload 0
aload 1
aload 2
invokevirtual com/a/b/d/yd/compare(Ljava/lang/Object;Ljava/lang/Object;)I
ifgt L0
aload 1
areturn
L0:
aload 2
areturn
.limit locals 3
.limit stack 3
.end method

.method public transient a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
.annotation visibleparam 3 Ljavax/annotation/Nullable;
.end annotation
aload 0
aload 0
aload 1
aload 2
invokevirtual com/a/b/d/yd/a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
aload 3
invokevirtual com/a/b/d/yd/a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
astore 1
aload 4
arraylength
istore 6
iconst_0
istore 5
L0:
iload 5
iload 6
if_icmpge L1
aload 0
aload 1
aload 4
iload 5
aaload
invokevirtual com/a/b/d/yd/a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
astore 1
iload 5
iconst_1
iadd
istore 5
goto L0
L1:
aload 1
areturn
.limit locals 7
.limit stack 4
.end method

.method public a(Ljava/util/Iterator;)Ljava/lang/Object;
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
astore 2
L0:
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 0
aload 2
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
invokevirtual com/a/b/d/yd/a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
astore 2
goto L0
L1:
aload 2
areturn
.limit locals 3
.limit stack 3
.end method

.method public a(Ljava/lang/Iterable;)Ljava/util/List;
aload 1
invokestatic com/a/b/d/mq/c(Ljava/lang/Iterable;)[Ljava/lang/Object;
checkcast [Ljava/lang/Object;
astore 1
aload 1
aload 0
invokestatic java/util/Arrays/sort([Ljava/lang/Object;Ljava/util/Comparator;)V
aload 1
invokestatic java/util/Arrays/asList([Ljava/lang/Object;)Ljava/util/List;
invokestatic com/a/b/d/ov/a(Ljava/lang/Iterable;)Ljava/util/ArrayList;
areturn
.limit locals 2
.limit stack 2
.end method

.method public b(Ljava/lang/Iterable;)Lcom/a/b/d/jl;
aload 1
invokestatic com/a/b/d/mq/c(Ljava/lang/Iterable;)[Ljava/lang/Object;
checkcast [Ljava/lang/Object;
astore 1
aload 1
arraylength
istore 3
iconst_0
istore 2
L0:
iload 2
iload 3
if_icmpge L1
aload 1
iload 2
aaload
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
iload 2
iconst_1
iadd
istore 2
goto L0
L1:
aload 1
aload 0
invokestatic java/util/Arrays/sort([Ljava/lang/Object;Ljava/util/Comparator;)V
aload 1
invokestatic com/a/b/d/jl/b([Ljava/lang/Object;)Lcom/a/b/d/jl;
areturn
.limit locals 4
.limit stack 2
.end method

.method public b()Lcom/a/b/d/yd;
.annotation invisible Lcom/a/b/a/b;
a Z = 1
.end annotation
new com/a/b/d/ya
dup
aload 0
invokespecial com/a/b/d/ya/<init>(Lcom/a/b/d/yd;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method public b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
aload 0
aload 1
aload 2
invokevirtual com/a/b/d/yd/compare(Ljava/lang/Object;Ljava/lang/Object;)I
iflt L0
aload 1
areturn
L0:
aload 2
areturn
.limit locals 3
.limit stack 3
.end method

.method public transient b(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
.annotation visibleparam 3 Ljavax/annotation/Nullable;
.end annotation
aload 0
aload 0
aload 1
aload 2
invokevirtual com/a/b/d/yd/b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
aload 3
invokevirtual com/a/b/d/yd/b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
astore 1
aload 4
arraylength
istore 6
iconst_0
istore 5
L0:
iload 5
iload 6
if_icmpge L1
aload 0
aload 1
aload 4
iload 5
aaload
invokevirtual com/a/b/d/yd/b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
astore 1
iload 5
iconst_1
iadd
istore 5
goto L0
L1:
aload 1
areturn
.limit locals 7
.limit stack 4
.end method

.method public b(Ljava/util/Iterator;)Ljava/lang/Object;
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
astore 2
L0:
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 0
aload 2
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
invokevirtual com/a/b/d/yd/b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
astore 2
goto L0
L1:
aload 2
areturn
.limit locals 3
.limit stack 3
.end method

.method public c()Lcom/a/b/d/yd;
.annotation invisible Lcom/a/b/a/b;
a Z = 1
.end annotation
new com/a/b/d/yb
dup
aload 0
invokespecial com/a/b/d/yb/<init>(Lcom/a/b/d/yd;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method public c(Ljava/lang/Iterable;)Ljava/lang/Object;
aload 0
aload 1
invokeinterface java/lang/Iterable/iterator()Ljava/util/Iterator; 0
invokevirtual com/a/b/d/yd/a(Ljava/util/Iterator;)Ljava/lang/Object;
areturn
.limit locals 2
.limit stack 2
.end method

.method public abstract compare(Ljava/lang/Object;Ljava/lang/Object;)I
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
.end method

.method public d(Ljava/lang/Iterable;)Ljava/lang/Object;
aload 0
aload 1
invokeinterface java/lang/Iterable/iterator()Ljava/util/Iterator; 0
invokevirtual com/a/b/d/yd/b(Ljava/util/Iterator;)Ljava/lang/Object;
areturn
.limit locals 2
.limit stack 2
.end method
