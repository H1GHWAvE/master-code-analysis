.bytecode 50.0
.class public synchronized abstract com/a/b/d/gy
.super com/a/b/d/gh
.implements com/a/b/d/xc
.annotation invisible Lcom/a/b/a/b;
.end annotation

.method protected <init>()V
aload 0
invokespecial com/a/b/d/gh/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private b(Ljava/lang/Object;II)Z
aload 0
aload 1
iload 2
iload 3
invokestatic com/a/b/d/xe/a(Lcom/a/b/d/xc;Ljava/lang/Object;II)Z
ireturn
.limit locals 4
.limit stack 4
.end method

.method private c()Ljava/util/Iterator;
aload 0
invokestatic com/a/b/d/xe/b(Lcom/a/b/d/xc;)Ljava/util/Iterator;
areturn
.limit locals 1
.limit stack 1
.end method

.method private d(Ljava/lang/Object;)I
.annotation invisible Lcom/a/b/a/a;
.end annotation
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
invokevirtual com/a/b/d/gy/a()Ljava/util/Set;
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
astore 2
L0:
aload 2
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 2
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/a/b/d/xd
astore 3
aload 3
invokeinterface com/a/b/d/xd/a()Ljava/lang/Object; 0
aload 1
invokestatic com/a/b/b/ce/a(Ljava/lang/Object;Ljava/lang/Object;)Z
ifeq L0
aload 3
invokeinterface com/a/b/d/xd/b()I 0
ireturn
L1:
iconst_0
ireturn
.limit locals 4
.limit stack 2
.end method

.method private d(Ljava/lang/Object;I)I
aload 0
aload 1
iload 2
invokestatic com/a/b/d/xe/a(Lcom/a/b/d/xc;Ljava/lang/Object;I)I
ireturn
.limit locals 3
.limit stack 3
.end method

.method private e()I
aload 0
invokestatic com/a/b/d/xe/c(Lcom/a/b/d/xc;)I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private e(Ljava/lang/Object;)Z
aload 0
aload 1
iconst_1
invokevirtual com/a/b/d/gy/a(Ljava/lang/Object;I)I
pop
iconst_1
ireturn
.limit locals 2
.limit stack 3
.end method

.method private f(Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
aload 1
invokestatic com/a/b/d/xe/a(Lcom/a/b/d/xc;Ljava/lang/Object;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method private q()I
aload 0
invokevirtual com/a/b/d/gy/a()Ljava/util/Set;
invokeinterface java/util/Set/hashCode()I 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final a(Ljava/lang/Object;)I
aload 0
invokevirtual com/a/b/d/gy/f()Lcom/a/b/d/xc;
aload 1
invokeinterface com/a/b/d/xc/a(Ljava/lang/Object;)I 1
ireturn
.limit locals 2
.limit stack 2
.end method

.method public a(Ljava/lang/Object;I)I
aload 0
invokevirtual com/a/b/d/gy/f()Lcom/a/b/d/xc;
aload 1
iload 2
invokeinterface com/a/b/d/xc/a(Ljava/lang/Object;I)I 2
ireturn
.limit locals 3
.limit stack 3
.end method

.method public a()Ljava/util/Set;
aload 0
invokevirtual com/a/b/d/gy/f()Lcom/a/b/d/xc;
invokeinterface com/a/b/d/xc/a()Ljava/util/Set; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method public a(Ljava/lang/Object;II)Z
aload 0
invokevirtual com/a/b/d/gy/f()Lcom/a/b/d/xc;
aload 1
iload 2
iload 3
invokeinterface com/a/b/d/xc/a(Ljava/lang/Object;II)Z 3
ireturn
.limit locals 4
.limit stack 4
.end method

.method protected final a(Ljava/util/Collection;)Z
.annotation invisible Lcom/a/b/a/a;
.end annotation
aload 0
aload 1
invokestatic com/a/b/d/xe/a(Lcom/a/b/d/xc;Ljava/util/Collection;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public b(Ljava/lang/Object;I)I
aload 0
invokevirtual com/a/b/d/gy/f()Lcom/a/b/d/xc;
aload 1
iload 2
invokeinterface com/a/b/d/xc/b(Ljava/lang/Object;I)I 2
ireturn
.limit locals 3
.limit stack 3
.end method

.method protected synthetic b()Ljava/util/Collection;
aload 0
invokevirtual com/a/b/d/gy/f()Lcom/a/b/d/xc;
areturn
.limit locals 1
.limit stack 1
.end method

.method protected final b(Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
aload 1
invokevirtual com/a/b/d/gy/a(Ljava/lang/Object;)I
ifle L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method protected final b(Ljava/util/Collection;)Z
aload 0
aload 1
invokestatic com/a/b/d/xe/b(Lcom/a/b/d/xc;Ljava/util/Collection;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public c(Ljava/lang/Object;I)I
aload 0
invokevirtual com/a/b/d/gy/f()Lcom/a/b/d/xc;
aload 1
iload 2
invokeinterface com/a/b/d/xc/c(Ljava/lang/Object;I)I 2
ireturn
.limit locals 3
.limit stack 3
.end method

.method protected final c(Ljava/lang/Object;)Z
aload 0
aload 1
iconst_1
invokevirtual com/a/b/d/gy/b(Ljava/lang/Object;I)I
ifle L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 3
.end method

.method protected final c(Ljava/util/Collection;)Z
aload 0
aload 1
invokestatic com/a/b/d/xe/c(Lcom/a/b/d/xc;Ljava/util/Collection;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public equals(Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 1
aload 0
if_acmpeq L0
aload 0
invokevirtual com/a/b/d/gy/f()Lcom/a/b/d/xc;
aload 1
invokeinterface com/a/b/d/xc/equals(Ljava/lang/Object;)Z 1
ifeq L1
L0:
iconst_1
ireturn
L1:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method protected abstract f()Lcom/a/b/d/xc;
.end method

.method public hashCode()I
aload 0
invokevirtual com/a/b/d/gy/f()Lcom/a/b/d/xc;
invokeinterface com/a/b/d/xc/hashCode()I 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method protected synthetic k_()Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/gy/f()Lcom/a/b/d/xc;
areturn
.limit locals 1
.limit stack 1
.end method

.method protected final l()V
aload 0
invokevirtual com/a/b/d/gy/a()Ljava/util/Set;
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
invokestatic com/a/b/d/nj/i(Ljava/util/Iterator;)V
return
.limit locals 1
.limit stack 1
.end method

.method public n_()Ljava/util/Set;
aload 0
invokevirtual com/a/b/d/gy/f()Lcom/a/b/d/xc;
invokeinterface com/a/b/d/xc/n_()Ljava/util/Set; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method protected final o()Ljava/lang/String;
aload 0
invokevirtual com/a/b/d/gy/a()Ljava/util/Set;
invokevirtual java/lang/Object/toString()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method
