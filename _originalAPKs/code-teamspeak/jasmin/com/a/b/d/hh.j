.bytecode 50.0
.class public synchronized abstract com/a/b/d/hh
.super com/a/b/d/gh
.implements java/util/Queue
.annotation invisible Lcom/a/b/a/b;
.end annotation

.method public <init>()V
aload 0
invokespecial com/a/b/d/gh/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private a(Ljava/lang/Object;)Z
.catch java/lang/IllegalStateException from L0 to L1 using L2
L0:
aload 0
aload 1
invokevirtual com/a/b/d/hh/add(Ljava/lang/Object;)Z
istore 2
L1:
iload 2
ireturn
L2:
astore 1
iconst_0
ireturn
.limit locals 3
.limit stack 2
.end method

.method private c()Ljava/lang/Object;
.catch java/util/NoSuchElementException from L0 to L1 using L2
L0:
aload 0
invokevirtual com/a/b/d/hh/element()Ljava/lang/Object;
astore 1
L1:
aload 1
areturn
L2:
astore 1
aconst_null
areturn
.limit locals 2
.limit stack 1
.end method

.method private d()Ljava/lang/Object;
.catch java/util/NoSuchElementException from L0 to L1 using L2
L0:
aload 0
invokevirtual com/a/b/d/hh/remove()Ljava/lang/Object;
astore 1
L1:
aload 1
areturn
L2:
astore 1
aconst_null
areturn
.limit locals 2
.limit stack 1
.end method

.method public abstract a()Ljava/util/Queue;
.end method

.method public synthetic b()Ljava/util/Collection;
aload 0
invokevirtual com/a/b/d/hh/a()Ljava/util/Queue;
areturn
.limit locals 1
.limit stack 1
.end method

.method public element()Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/hh/a()Ljava/util/Queue;
invokeinterface java/util/Queue/element()Ljava/lang/Object; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method public synthetic k_()Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/hh/a()Ljava/util/Queue;
areturn
.limit locals 1
.limit stack 1
.end method

.method public offer(Ljava/lang/Object;)Z
aload 0
invokevirtual com/a/b/d/hh/a()Ljava/util/Queue;
aload 1
invokeinterface java/util/Queue/offer(Ljava/lang/Object;)Z 1
ireturn
.limit locals 2
.limit stack 2
.end method

.method public peek()Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/hh/a()Ljava/util/Queue;
invokeinterface java/util/Queue/peek()Ljava/lang/Object; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method public poll()Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/hh/a()Ljava/util/Queue;
invokeinterface java/util/Queue/poll()Ljava/lang/Object; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method public remove()Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/hh/a()Ljava/util/Queue;
invokeinterface java/util/Queue/remove()Ljava/lang/Object; 0
areturn
.limit locals 1
.limit stack 1
.end method
