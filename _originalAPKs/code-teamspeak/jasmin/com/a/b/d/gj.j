.bytecode 50.0
.class public synchronized abstract com/a/b/d/gj
.super com/a/b/d/hh
.implements java/util/Deque

.method protected <init>()V
aload 0
invokespecial com/a/b/d/hh/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method protected synthetic a()Ljava/util/Queue;
aload 0
invokevirtual com/a/b/d/gj/d()Ljava/util/Deque;
areturn
.limit locals 1
.limit stack 1
.end method

.method public addFirst(Ljava/lang/Object;)V
aload 0
invokevirtual com/a/b/d/gj/d()Ljava/util/Deque;
aload 1
invokeinterface java/util/Deque/addFirst(Ljava/lang/Object;)V 1
return
.limit locals 2
.limit stack 2
.end method

.method public addLast(Ljava/lang/Object;)V
aload 0
invokevirtual com/a/b/d/gj/d()Ljava/util/Deque;
aload 1
invokeinterface java/util/Deque/addLast(Ljava/lang/Object;)V 1
return
.limit locals 2
.limit stack 2
.end method

.method protected synthetic b()Ljava/util/Collection;
aload 0
invokevirtual com/a/b/d/gj/d()Ljava/util/Deque;
areturn
.limit locals 1
.limit stack 1
.end method

.method protected abstract d()Ljava/util/Deque;
.end method

.method public descendingIterator()Ljava/util/Iterator;
aload 0
invokevirtual com/a/b/d/gj/d()Ljava/util/Deque;
invokeinterface java/util/Deque/descendingIterator()Ljava/util/Iterator; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method public getFirst()Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/gj/d()Ljava/util/Deque;
invokeinterface java/util/Deque/getFirst()Ljava/lang/Object; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method public getLast()Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/gj/d()Ljava/util/Deque;
invokeinterface java/util/Deque/getLast()Ljava/lang/Object; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method protected synthetic k_()Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/gj/d()Ljava/util/Deque;
areturn
.limit locals 1
.limit stack 1
.end method

.method public offerFirst(Ljava/lang/Object;)Z
aload 0
invokevirtual com/a/b/d/gj/d()Ljava/util/Deque;
aload 1
invokeinterface java/util/Deque/offerFirst(Ljava/lang/Object;)Z 1
ireturn
.limit locals 2
.limit stack 2
.end method

.method public offerLast(Ljava/lang/Object;)Z
aload 0
invokevirtual com/a/b/d/gj/d()Ljava/util/Deque;
aload 1
invokeinterface java/util/Deque/offerLast(Ljava/lang/Object;)Z 1
ireturn
.limit locals 2
.limit stack 2
.end method

.method public peekFirst()Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/gj/d()Ljava/util/Deque;
invokeinterface java/util/Deque/peekFirst()Ljava/lang/Object; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method public peekLast()Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/gj/d()Ljava/util/Deque;
invokeinterface java/util/Deque/peekLast()Ljava/lang/Object; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method public pollFirst()Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/gj/d()Ljava/util/Deque;
invokeinterface java/util/Deque/pollFirst()Ljava/lang/Object; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method public pollLast()Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/gj/d()Ljava/util/Deque;
invokeinterface java/util/Deque/pollLast()Ljava/lang/Object; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method public pop()Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/gj/d()Ljava/util/Deque;
invokeinterface java/util/Deque/pop()Ljava/lang/Object; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method public push(Ljava/lang/Object;)V
aload 0
invokevirtual com/a/b/d/gj/d()Ljava/util/Deque;
aload 1
invokeinterface java/util/Deque/push(Ljava/lang/Object;)V 1
return
.limit locals 2
.limit stack 2
.end method

.method public removeFirst()Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/gj/d()Ljava/util/Deque;
invokeinterface java/util/Deque/removeFirst()Ljava/lang/Object; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method public removeFirstOccurrence(Ljava/lang/Object;)Z
aload 0
invokevirtual com/a/b/d/gj/d()Ljava/util/Deque;
aload 1
invokeinterface java/util/Deque/removeFirstOccurrence(Ljava/lang/Object;)Z 1
ireturn
.limit locals 2
.limit stack 2
.end method

.method public removeLast()Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/gj/d()Ljava/util/Deque;
invokeinterface java/util/Deque/removeLast()Ljava/lang/Object; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method public removeLastOccurrence(Ljava/lang/Object;)Z
aload 0
invokevirtual com/a/b/d/gj/d()Ljava/util/Deque;
aload 1
invokeinterface java/util/Deque/removeLastOccurrence(Ljava/lang/Object;)Z 1
ireturn
.limit locals 2
.limit stack 2
.end method
