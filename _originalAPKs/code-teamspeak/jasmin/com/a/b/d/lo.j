.bytecode 50.0
.class public synchronized abstract com/a/b/d/lo
.super com/a/b/d/iz
.implements java/util/Set
.annotation invisible Lcom/a/b/a/b;
a Z = 1
b Z = 1
.end annotation

.field private static final 'a' D = 0.7D


.field static final 'b' I = 1073741824


.field private static final 'c' I = 751619276


.method <init>()V
aload 0
invokespecial com/a/b/d/iz/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a(I)I
.annotation invisible Lcom/a/b/a/d;
.end annotation
iload 0
ldc_w 751619276
if_icmpge L0
iload 0
iconst_1
isub
invokestatic java/lang/Integer/highestOneBit(I)I
iconst_1
ishl
istore 1
L1:
iload 1
istore 2
iload 1
i2d
ldc2_w 0.7D
dmul
iload 0
i2d
dcmpg
ifge L2
iload 1
iconst_1
ishl
istore 1
goto L1
L0:
iload 0
ldc_w 1073741824
if_icmpge L3
iconst_1
istore 3
L4:
iload 3
ldc "collection too large"
invokestatic com/a/b/b/cn/a(ZLjava/lang/Object;)V
ldc_w 1073741824
istore 2
L2:
iload 2
ireturn
L3:
iconst_0
istore 3
goto L4
.limit locals 4
.limit stack 4
.end method

.method static synthetic a(I[Ljava/lang/Object;)Lcom/a/b/d/lo;
iload 0
aload 1
invokestatic com/a/b/d/lo/b(I[Ljava/lang/Object;)Lcom/a/b/d/lo;
areturn
.limit locals 2
.limit stack 2
.end method

.method public static a(Ljava/lang/Iterable;)Lcom/a/b/d/lo;
aload 0
instanceof java/util/Collection
ifeq L0
aload 0
checkcast java/util/Collection
invokestatic com/a/b/d/lo/a(Ljava/util/Collection;)Lcom/a/b/d/lo;
areturn
L0:
aload 0
invokeinterface java/lang/Iterable/iterator()Ljava/util/Iterator; 0
astore 0
aload 0
invokeinterface java/util/Iterator/hasNext()Z 0
ifne L1
getstatic com/a/b/d/ey/a Lcom/a/b/d/ey;
areturn
L1:
aload 0
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
astore 1
aload 0
invokeinterface java/util/Iterator/hasNext()Z 0
ifne L2
aload 1
invokestatic com/a/b/d/lo/d(Ljava/lang/Object;)Lcom/a/b/d/lo;
areturn
L2:
new com/a/b/d/lp
dup
invokespecial com/a/b/d/lp/<init>()V
aload 1
invokevirtual com/a/b/d/lp/c(Ljava/lang/Object;)Lcom/a/b/d/lp;
aload 0
invokevirtual com/a/b/d/lp/b(Ljava/util/Iterator;)Lcom/a/b/d/lp;
invokevirtual com/a/b/d/lp/b()Lcom/a/b/d/lo;
areturn
.limit locals 2
.limit stack 2
.end method

.method public static a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/lo;
iconst_3
iconst_3
anewarray java/lang/Object
dup
iconst_0
aload 0
aastore
dup
iconst_1
aload 1
aastore
dup
iconst_2
aload 2
aastore
invokestatic com/a/b/d/lo/b(I[Ljava/lang/Object;)Lcom/a/b/d/lo;
areturn
.limit locals 3
.limit stack 5
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/lo;
iconst_4
iconst_4
anewarray java/lang/Object
dup
iconst_0
aload 0
aastore
dup
iconst_1
aload 1
aastore
dup
iconst_2
aload 2
aastore
dup
iconst_3
aload 3
aastore
invokestatic com/a/b/d/lo/b(I[Ljava/lang/Object;)Lcom/a/b/d/lo;
areturn
.limit locals 4
.limit stack 5
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/lo;
iconst_5
iconst_5
anewarray java/lang/Object
dup
iconst_0
aload 0
aastore
dup
iconst_1
aload 1
aastore
dup
iconst_2
aload 2
aastore
dup
iconst_3
aload 3
aastore
dup
iconst_4
aload 4
aastore
invokestatic com/a/b/d/lo/b(I[Ljava/lang/Object;)Lcom/a/b/d/lo;
areturn
.limit locals 5
.limit stack 5
.end method

.method private static transient a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)Lcom/a/b/d/lo;
aload 6
arraylength
bipush 6
iadd
anewarray java/lang/Object
astore 7
aload 7
iconst_0
aload 0
aastore
aload 7
iconst_1
aload 1
aastore
aload 7
iconst_2
aload 2
aastore
aload 7
iconst_3
aload 3
aastore
aload 7
iconst_4
aload 4
aastore
aload 7
iconst_5
aload 5
aastore
aload 6
iconst_0
aload 7
bipush 6
aload 6
arraylength
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 7
arraylength
aload 7
invokestatic com/a/b/d/lo/b(I[Ljava/lang/Object;)Lcom/a/b/d/lo;
areturn
.limit locals 8
.limit stack 5
.end method

.method public static a(Ljava/util/Collection;)Lcom/a/b/d/lo;
aload 0
instanceof com/a/b/d/lo
ifeq L0
aload 0
instanceof com/a/b/d/me
ifne L0
aload 0
checkcast com/a/b/d/lo
astore 1
aload 1
invokevirtual com/a/b/d/lo/h_()Z
ifne L1
aload 1
areturn
L0:
aload 0
instanceof java/util/EnumSet
ifeq L1
aload 0
checkcast java/util/EnumSet
invokestatic java/util/EnumSet/copyOf(Ljava/util/EnumSet;)Ljava/util/EnumSet;
invokestatic com/a/b/d/ji/a(Ljava/util/EnumSet;)Lcom/a/b/d/lo;
areturn
L1:
aload 0
invokeinterface java/util/Collection/toArray()[Ljava/lang/Object; 0
astore 0
aload 0
arraylength
aload 0
invokestatic com/a/b/d/lo/b(I[Ljava/lang/Object;)Lcom/a/b/d/lo;
areturn
.limit locals 2
.limit stack 2
.end method

.method private static a(Ljava/util/EnumSet;)Lcom/a/b/d/lo;
aload 0
invokestatic java/util/EnumSet/copyOf(Ljava/util/EnumSet;)Ljava/util/EnumSet;
invokestatic com/a/b/d/ji/a(Ljava/util/EnumSet;)Lcom/a/b/d/lo;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static a(Ljava/util/Iterator;)Lcom/a/b/d/lo;
aload 0
invokeinterface java/util/Iterator/hasNext()Z 0
ifne L0
getstatic com/a/b/d/ey/a Lcom/a/b/d/ey;
areturn
L0:
aload 0
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
astore 1
aload 0
invokeinterface java/util/Iterator/hasNext()Z 0
ifne L1
aload 1
invokestatic com/a/b/d/lo/d(Ljava/lang/Object;)Lcom/a/b/d/lo;
areturn
L1:
new com/a/b/d/lp
dup
invokespecial com/a/b/d/lp/<init>()V
aload 1
invokevirtual com/a/b/d/lp/c(Ljava/lang/Object;)Lcom/a/b/d/lp;
aload 0
invokevirtual com/a/b/d/lp/b(Ljava/util/Iterator;)Lcom/a/b/d/lp;
invokevirtual com/a/b/d/lp/b()Lcom/a/b/d/lo;
areturn
.limit locals 2
.limit stack 2
.end method

.method public static a([Ljava/lang/Object;)Lcom/a/b/d/lo;
aload 0
arraylength
tableswitch 0
L0
L1
default : L2
L2:
aload 0
arraylength
aload 0
invokevirtual [Ljava/lang/Object;/clone()Ljava/lang/Object;
checkcast [Ljava/lang/Object;
invokestatic com/a/b/d/lo/b(I[Ljava/lang/Object;)Lcom/a/b/d/lo;
areturn
L0:
getstatic com/a/b/d/ey/a Lcom/a/b/d/ey;
areturn
L1:
aload 0
iconst_0
aaload
invokestatic com/a/b/d/lo/d(Ljava/lang/Object;)Lcom/a/b/d/lo;
areturn
.limit locals 1
.limit stack 2
.end method

.method private static transient b(I[Ljava/lang/Object;)Lcom/a/b/d/lo;
iload 0
istore 3
L0:
iload 3
tableswitch 0
L1
L2
default : L3
L3:
iload 3
invokestatic com/a/b/d/lo/a(I)I
istore 6
iload 6
anewarray java/lang/Object
astore 11
iload 6
iconst_1
isub
istore 7
iconst_0
istore 4
iconst_0
istore 0
iconst_0
istore 2
L4:
iload 4
iload 3
if_icmpge L5
aload 1
iload 4
aaload
iload 4
invokestatic com/a/b/d/yc/a(Ljava/lang/Object;I)Ljava/lang/Object;
astore 10
aload 10
invokevirtual java/lang/Object/hashCode()I
istore 8
iload 8
invokestatic com/a/b/d/iq/a(I)I
istore 5
L6:
iload 5
iload 7
iand
istore 9
aload 11
iload 9
aaload
astore 12
aload 12
ifnonnull L7
iload 0
iconst_1
iadd
istore 5
aload 1
iload 0
aload 10
aastore
aload 11
iload 9
aload 10
aastore
iload 2
iload 8
iadd
istore 2
iload 5
istore 0
L8:
iload 4
iconst_1
iadd
istore 4
goto L4
L1:
getstatic com/a/b/d/ey/a Lcom/a/b/d/ey;
areturn
L2:
aload 1
iconst_0
aaload
invokestatic com/a/b/d/lo/d(Ljava/lang/Object;)Lcom/a/b/d/lo;
areturn
L7:
aload 12
aload 10
invokevirtual java/lang/Object/equals(Ljava/lang/Object;)Z
ifne L9
iload 5
iconst_1
iadd
istore 5
goto L6
L5:
aload 1
iload 0
iload 3
aconst_null
invokestatic java/util/Arrays/fill([Ljava/lang/Object;IILjava/lang/Object;)V
iload 0
iconst_1
if_icmpne L10
new com/a/b/d/aaw
dup
aload 1
iconst_0
aaload
iload 2
invokespecial com/a/b/d/aaw/<init>(Ljava/lang/Object;I)V
areturn
L10:
iload 6
iload 0
invokestatic com/a/b/d/lo/a(I)I
if_icmpeq L11
iload 0
istore 3
goto L0
L11:
aload 1
astore 10
iload 0
aload 1
arraylength
if_icmpge L12
aload 1
iload 0
invokestatic com/a/b/d/yc/b([Ljava/lang/Object;I)[Ljava/lang/Object;
astore 10
L12:
new com/a/b/d/zk
dup
aload 10
iload 2
aload 11
iload 7
invokespecial com/a/b/d/zk/<init>([Ljava/lang/Object;I[Ljava/lang/Object;I)V
areturn
L9:
goto L8
.limit locals 13
.limit stack 6
.end method

.method public static b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/lo;
iconst_2
iconst_2
anewarray java/lang/Object
dup
iconst_0
aload 0
aastore
dup
iconst_1
aload 1
aastore
invokestatic com/a/b/d/lo/b(I[Ljava/lang/Object;)Lcom/a/b/d/lo;
areturn
.limit locals 2
.limit stack 5
.end method

.method public static d(Ljava/lang/Object;)Lcom/a/b/d/lo;
new com/a/b/d/aaw
dup
aload 0
invokespecial com/a/b/d/aaw/<init>(Ljava/lang/Object;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method public static h()Lcom/a/b/d/lo;
getstatic com/a/b/d/ey/a Lcom/a/b/d/ey;
areturn
.limit locals 0
.limit stack 1
.end method

.method public static i()Lcom/a/b/d/lp;
new com/a/b/d/lp
dup
invokespecial com/a/b/d/lp/<init>()V
areturn
.limit locals 0
.limit stack 2
.end method

.method public abstract c()Lcom/a/b/d/agi;
.end method

.method public equals(Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 1
aload 0
if_acmpne L0
iconst_1
ireturn
L0:
aload 1
instanceof com/a/b/d/lo
ifeq L1
aload 0
invokevirtual com/a/b/d/lo/g_()Z
ifeq L1
aload 1
checkcast com/a/b/d/lo
invokevirtual com/a/b/d/lo/g_()Z
ifeq L1
aload 0
invokevirtual com/a/b/d/lo/hashCode()I
aload 1
invokevirtual java/lang/Object/hashCode()I
if_icmpeq L1
iconst_0
ireturn
L1:
aload 0
aload 1
invokestatic com/a/b/d/aad/a(Ljava/util/Set;Ljava/lang/Object;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method g()Ljava/lang/Object;
new com/a/b/d/lq
dup
aload 0
invokevirtual com/a/b/d/lo/toArray()[Ljava/lang/Object;
invokespecial com/a/b/d/lq/<init>([Ljava/lang/Object;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method g_()Z
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public hashCode()I
aload 0
invokestatic com/a/b/d/aad/a(Ljava/util/Set;)I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public synthetic iterator()Ljava/util/Iterator;
aload 0
invokevirtual com/a/b/d/lo/c()Lcom/a/b/d/agi;
areturn
.limit locals 1
.limit stack 1
.end method
