.bytecode 50.0
.class final synchronized com/a/b/d/cq
.super com/a/b/d/j

.field 'a' Ljava/util/List;

.field final 'b' Ljava/util/Comparator;

.method <init>(Ljava/util/List;Ljava/util/Comparator;)V
aload 0
invokespecial com/a/b/d/j/<init>()V
aload 0
aload 1
invokestatic com/a/b/d/ov/a(Ljava/lang/Iterable;)Ljava/util/ArrayList;
putfield com/a/b/d/cq/a Ljava/util/List;
aload 0
aload 2
putfield com/a/b/d/cq/b Ljava/util/Comparator;
return
.limit locals 3
.limit stack 2
.end method

.method private a(I)I
aload 0
getfield com/a/b/d/cq/a Ljava/util/List;
iload 1
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
astore 3
aload 0
getfield com/a/b/d/cq/a Ljava/util/List;
invokeinterface java/util/List/size()I 0
iconst_1
isub
istore 2
L0:
iload 2
iload 1
if_icmple L1
aload 0
getfield com/a/b/d/cq/b Ljava/util/Comparator;
aload 3
aload 0
getfield com/a/b/d/cq/a Ljava/util/List;
iload 2
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
invokeinterface java/util/Comparator/compare(Ljava/lang/Object;Ljava/lang/Object;)I 2
ifge L2
iload 2
ireturn
L2:
iload 2
iconst_1
isub
istore 2
goto L0
L1:
new java/lang/AssertionError
dup
ldc "this statement should be unreachable"
invokespecial java/lang/AssertionError/<init>(Ljava/lang/Object;)V
athrow
.limit locals 4
.limit stack 4
.end method

.method private c()Ljava/util/List;
aload 0
getfield com/a/b/d/cq/a Ljava/util/List;
ifnonnull L0
aload 0
invokevirtual com/a/b/d/cq/b()Ljava/lang/Object;
pop
aconst_null
areturn
L0:
aload 0
getfield com/a/b/d/cq/a Ljava/util/List;
invokestatic com/a/b/d/jl/a(Ljava/util/Collection;)Lcom/a/b/d/jl;
astore 3
aload 0
getfield com/a/b/d/cq/a Ljava/util/List;
invokeinterface java/util/List/size()I 0
iconst_2
isub
istore 1
L1:
iload 1
iflt L2
aload 0
getfield com/a/b/d/cq/b Ljava/util/Comparator;
aload 0
getfield com/a/b/d/cq/a Ljava/util/List;
iload 1
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
aload 0
getfield com/a/b/d/cq/a Ljava/util/List;
iload 1
iconst_1
iadd
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
invokeinterface java/util/Comparator/compare(Ljava/lang/Object;Ljava/lang/Object;)I 2
ifge L3
L4:
iload 1
iconst_m1
if_icmpne L5
aload 0
aconst_null
putfield com/a/b/d/cq/a Ljava/util/List;
L6:
aload 3
areturn
L3:
iload 1
iconst_1
isub
istore 1
goto L1
L2:
iconst_m1
istore 1
goto L4
L5:
aload 0
getfield com/a/b/d/cq/a Ljava/util/List;
iload 1
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
astore 4
aload 0
getfield com/a/b/d/cq/a Ljava/util/List;
invokeinterface java/util/List/size()I 0
iconst_1
isub
istore 2
L7:
iload 2
iload 1
if_icmple L8
aload 0
getfield com/a/b/d/cq/b Ljava/util/Comparator;
aload 4
aload 0
getfield com/a/b/d/cq/a Ljava/util/List;
iload 2
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
invokeinterface java/util/Comparator/compare(Ljava/lang/Object;Ljava/lang/Object;)I 2
ifge L9
aload 0
getfield com/a/b/d/cq/a Ljava/util/List;
iload 1
iload 2
invokestatic java/util/Collections/swap(Ljava/util/List;II)V
aload 0
getfield com/a/b/d/cq/a Ljava/util/List;
invokeinterface java/util/List/size()I 0
istore 2
aload 0
getfield com/a/b/d/cq/a Ljava/util/List;
iload 1
iconst_1
iadd
iload 2
invokeinterface java/util/List/subList(II)Ljava/util/List; 2
invokestatic java/util/Collections/reverse(Ljava/util/List;)V
goto L6
L9:
iload 2
iconst_1
isub
istore 2
goto L7
L8:
new java/lang/AssertionError
dup
ldc "this statement should be unreachable"
invokespecial java/lang/AssertionError/<init>(Ljava/lang/Object;)V
athrow
.limit locals 5
.limit stack 5
.end method

.method private d()V
aload 0
getfield com/a/b/d/cq/a Ljava/util/List;
invokeinterface java/util/List/size()I 0
iconst_2
isub
istore 1
L0:
iload 1
iflt L1
aload 0
getfield com/a/b/d/cq/b Ljava/util/Comparator;
aload 0
getfield com/a/b/d/cq/a Ljava/util/List;
iload 1
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
aload 0
getfield com/a/b/d/cq/a Ljava/util/List;
iload 1
iconst_1
iadd
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
invokeinterface java/util/Comparator/compare(Ljava/lang/Object;Ljava/lang/Object;)I 2
ifge L2
L3:
iload 1
iconst_m1
if_icmpne L4
aload 0
aconst_null
putfield com/a/b/d/cq/a Ljava/util/List;
return
L2:
iload 1
iconst_1
isub
istore 1
goto L0
L1:
iconst_m1
istore 1
goto L3
L4:
aload 0
getfield com/a/b/d/cq/a Ljava/util/List;
iload 1
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
astore 3
aload 0
getfield com/a/b/d/cq/a Ljava/util/List;
invokeinterface java/util/List/size()I 0
iconst_1
isub
istore 2
L5:
iload 2
iload 1
if_icmple L6
aload 0
getfield com/a/b/d/cq/b Ljava/util/Comparator;
aload 3
aload 0
getfield com/a/b/d/cq/a Ljava/util/List;
iload 2
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
invokeinterface java/util/Comparator/compare(Ljava/lang/Object;Ljava/lang/Object;)I 2
ifge L7
aload 0
getfield com/a/b/d/cq/a Ljava/util/List;
iload 1
iload 2
invokestatic java/util/Collections/swap(Ljava/util/List;II)V
aload 0
getfield com/a/b/d/cq/a Ljava/util/List;
invokeinterface java/util/List/size()I 0
istore 2
aload 0
getfield com/a/b/d/cq/a Ljava/util/List;
iload 1
iconst_1
iadd
iload 2
invokeinterface java/util/List/subList(II)Ljava/util/List; 2
invokestatic java/util/Collections/reverse(Ljava/util/List;)V
return
L7:
iload 2
iconst_1
isub
istore 2
goto L5
L6:
new java/lang/AssertionError
dup
ldc "this statement should be unreachable"
invokespecial java/lang/AssertionError/<init>(Ljava/lang/Object;)V
athrow
.limit locals 4
.limit stack 5
.end method

.method private e()I
aload 0
getfield com/a/b/d/cq/a Ljava/util/List;
invokeinterface java/util/List/size()I 0
iconst_2
isub
istore 1
L0:
iload 1
iflt L1
aload 0
getfield com/a/b/d/cq/b Ljava/util/Comparator;
aload 0
getfield com/a/b/d/cq/a Ljava/util/List;
iload 1
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
aload 0
getfield com/a/b/d/cq/a Ljava/util/List;
iload 1
iconst_1
iadd
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
invokeinterface java/util/Comparator/compare(Ljava/lang/Object;Ljava/lang/Object;)I 2
ifge L2
iload 1
ireturn
L2:
iload 1
iconst_1
isub
istore 1
goto L0
L1:
iconst_m1
ireturn
.limit locals 2
.limit stack 5
.end method

.method protected final synthetic a()Ljava/lang/Object;
aload 0
getfield com/a/b/d/cq/a Ljava/util/List;
ifnonnull L0
aload 0
invokevirtual com/a/b/d/cq/b()Ljava/lang/Object;
pop
aconst_null
areturn
L0:
aload 0
getfield com/a/b/d/cq/a Ljava/util/List;
invokestatic com/a/b/d/jl/a(Ljava/util/Collection;)Lcom/a/b/d/jl;
astore 3
aload 0
getfield com/a/b/d/cq/a Ljava/util/List;
invokeinterface java/util/List/size()I 0
iconst_2
isub
istore 1
L1:
iload 1
iflt L2
aload 0
getfield com/a/b/d/cq/b Ljava/util/Comparator;
aload 0
getfield com/a/b/d/cq/a Ljava/util/List;
iload 1
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
aload 0
getfield com/a/b/d/cq/a Ljava/util/List;
iload 1
iconst_1
iadd
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
invokeinterface java/util/Comparator/compare(Ljava/lang/Object;Ljava/lang/Object;)I 2
ifge L3
L4:
iload 1
iconst_m1
if_icmpne L5
aload 0
aconst_null
putfield com/a/b/d/cq/a Ljava/util/List;
L6:
aload 3
areturn
L3:
iload 1
iconst_1
isub
istore 1
goto L1
L2:
iconst_m1
istore 1
goto L4
L5:
aload 0
getfield com/a/b/d/cq/a Ljava/util/List;
iload 1
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
astore 4
aload 0
getfield com/a/b/d/cq/a Ljava/util/List;
invokeinterface java/util/List/size()I 0
iconst_1
isub
istore 2
L7:
iload 2
iload 1
if_icmple L8
aload 0
getfield com/a/b/d/cq/b Ljava/util/Comparator;
aload 4
aload 0
getfield com/a/b/d/cq/a Ljava/util/List;
iload 2
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
invokeinterface java/util/Comparator/compare(Ljava/lang/Object;Ljava/lang/Object;)I 2
ifge L9
aload 0
getfield com/a/b/d/cq/a Ljava/util/List;
iload 1
iload 2
invokestatic java/util/Collections/swap(Ljava/util/List;II)V
aload 0
getfield com/a/b/d/cq/a Ljava/util/List;
invokeinterface java/util/List/size()I 0
istore 2
aload 0
getfield com/a/b/d/cq/a Ljava/util/List;
iload 1
iconst_1
iadd
iload 2
invokeinterface java/util/List/subList(II)Ljava/util/List; 2
invokestatic java/util/Collections/reverse(Ljava/util/List;)V
goto L6
L9:
iload 2
iconst_1
isub
istore 2
goto L7
L8:
new java/lang/AssertionError
dup
ldc "this statement should be unreachable"
invokespecial java/lang/AssertionError/<init>(Ljava/lang/Object;)V
athrow
.limit locals 5
.limit stack 5
.end method
