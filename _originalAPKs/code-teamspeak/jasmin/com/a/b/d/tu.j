.bytecode 50.0
.class synchronized abstract com/a/b/d/tu
.super com/a/b/d/aan

.method <init>()V
aload 0
invokespecial com/a/b/d/aan/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method abstract a()Ljava/util/Map;
.end method

.method public clear()V
aload 0
invokevirtual com/a/b/d/tu/a()Ljava/util/Map;
invokeinterface java/util/Map/clear()V 0
return
.limit locals 1
.limit stack 1
.end method

.method public contains(Ljava/lang/Object;)Z
iconst_0
istore 3
iload 3
istore 2
aload 1
instanceof java/util/Map$Entry
ifeq L0
aload 1
checkcast java/util/Map$Entry
astore 1
aload 1
invokeinterface java/util/Map$Entry/getKey()Ljava/lang/Object; 0
astore 4
aload 0
invokevirtual com/a/b/d/tu/a()Ljava/util/Map;
aload 4
invokestatic com/a/b/d/sz/a(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;
astore 5
iload 3
istore 2
aload 5
aload 1
invokeinterface java/util/Map$Entry/getValue()Ljava/lang/Object; 0
invokestatic com/a/b/b/ce/a(Ljava/lang/Object;Ljava/lang/Object;)Z
ifeq L0
aload 5
ifnonnull L1
iload 3
istore 2
aload 0
invokevirtual com/a/b/d/tu/a()Ljava/util/Map;
aload 4
invokeinterface java/util/Map/containsKey(Ljava/lang/Object;)Z 1
ifeq L0
L1:
iconst_1
istore 2
L0:
iload 2
ireturn
.limit locals 6
.limit stack 2
.end method

.method public isEmpty()Z
aload 0
invokevirtual com/a/b/d/tu/a()Ljava/util/Map;
invokeinterface java/util/Map/isEmpty()Z 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public remove(Ljava/lang/Object;)Z
aload 0
aload 1
invokevirtual com/a/b/d/tu/contains(Ljava/lang/Object;)Z
ifeq L0
aload 1
checkcast java/util/Map$Entry
astore 1
aload 0
invokevirtual com/a/b/d/tu/a()Ljava/util/Map;
invokeinterface java/util/Map/keySet()Ljava/util/Set; 0
aload 1
invokeinterface java/util/Map$Entry/getKey()Ljava/lang/Object; 0
invokeinterface java/util/Set/remove(Ljava/lang/Object;)Z 1
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method public removeAll(Ljava/util/Collection;)Z
.catch java/lang/UnsupportedOperationException from L0 to L1 using L2
L0:
aload 0
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/util/Collection
invokespecial com/a/b/d/aan/removeAll(Ljava/util/Collection;)Z
istore 2
L1:
iload 2
ireturn
L2:
astore 3
aload 0
aload 1
invokeinterface java/util/Collection/iterator()Ljava/util/Iterator; 0
invokestatic com/a/b/d/aad/a(Ljava/util/Set;Ljava/util/Iterator;)Z
ireturn
.limit locals 4
.limit stack 2
.end method

.method public retainAll(Ljava/util/Collection;)Z
.catch java/lang/UnsupportedOperationException from L0 to L1 using L2
L0:
aload 0
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/util/Collection
invokespecial com/a/b/d/aan/retainAll(Ljava/util/Collection;)Z
istore 2
L1:
iload 2
ireturn
L2:
astore 3
aload 1
invokeinterface java/util/Collection/size()I 0
invokestatic com/a/b/d/aad/a(I)Ljava/util/HashSet;
astore 3
aload 1
invokeinterface java/util/Collection/iterator()Ljava/util/Iterator; 0
astore 1
L3:
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L4
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
astore 4
aload 0
aload 4
invokevirtual com/a/b/d/tu/contains(Ljava/lang/Object;)Z
ifeq L3
aload 3
aload 4
checkcast java/util/Map$Entry
invokeinterface java/util/Map$Entry/getKey()Ljava/lang/Object; 0
invokeinterface java/util/Set/add(Ljava/lang/Object;)Z 1
pop
goto L3
L4:
aload 0
invokevirtual com/a/b/d/tu/a()Ljava/util/Map;
invokeinterface java/util/Map/keySet()Ljava/util/Set; 0
aload 3
invokeinterface java/util/Set/retainAll(Ljava/util/Collection;)Z 1
ireturn
.limit locals 5
.limit stack 2
.end method

.method public size()I
aload 0
invokevirtual com/a/b/d/tu/a()Ljava/util/Map;
invokeinterface java/util/Map/size()I 0
ireturn
.limit locals 1
.limit stack 1
.end method
