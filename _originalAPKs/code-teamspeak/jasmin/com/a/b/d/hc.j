.bytecode 50.0
.class final synchronized com/a/b/d/hc
.super java/lang/Object
.implements java/util/Iterator

.field final synthetic 'a' Lcom/a/b/d/hb;

.field private 'b' Ljava/util/Map$Entry;

.field private 'c' Ljava/util/Map$Entry;

.method <init>(Lcom/a/b/d/hb;)V
aload 0
aload 1
putfield com/a/b/d/hc/a Lcom/a/b/d/hb;
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aconst_null
putfield com/a/b/d/hc/b Ljava/util/Map$Entry;
aload 0
aload 0
getfield com/a/b/d/hc/a Lcom/a/b/d/hb;
getfield com/a/b/d/hb/a Lcom/a/b/d/ha;
invokeinterface java/util/NavigableMap/lastEntry()Ljava/util/Map$Entry; 0
putfield com/a/b/d/hc/c Ljava/util/Map$Entry;
return
.limit locals 2
.limit stack 2
.end method

.method private a()Ljava/util/Map$Entry;
.catch all from L0 to L1 using L2
aload 0
invokevirtual com/a/b/d/hc/hasNext()Z
ifne L0
new java/util/NoSuchElementException
dup
invokespecial java/util/NoSuchElementException/<init>()V
athrow
L0:
aload 0
getfield com/a/b/d/hc/c Ljava/util/Map$Entry;
astore 1
L1:
aload 0
aload 0
getfield com/a/b/d/hc/c Ljava/util/Map$Entry;
putfield com/a/b/d/hc/b Ljava/util/Map$Entry;
aload 0
aload 0
getfield com/a/b/d/hc/a Lcom/a/b/d/hb;
getfield com/a/b/d/hb/a Lcom/a/b/d/ha;
aload 0
getfield com/a/b/d/hc/c Ljava/util/Map$Entry;
invokeinterface java/util/Map$Entry/getKey()Ljava/lang/Object; 0
invokeinterface java/util/NavigableMap/lowerEntry(Ljava/lang/Object;)Ljava/util/Map$Entry; 1
putfield com/a/b/d/hc/c Ljava/util/Map$Entry;
aload 1
areturn
L2:
astore 1
aload 0
aload 0
getfield com/a/b/d/hc/c Ljava/util/Map$Entry;
putfield com/a/b/d/hc/b Ljava/util/Map$Entry;
aload 0
aload 0
getfield com/a/b/d/hc/a Lcom/a/b/d/hb;
getfield com/a/b/d/hb/a Lcom/a/b/d/ha;
aload 0
getfield com/a/b/d/hc/c Ljava/util/Map$Entry;
invokeinterface java/util/Map$Entry/getKey()Ljava/lang/Object; 0
invokeinterface java/util/NavigableMap/lowerEntry(Ljava/lang/Object;)Ljava/util/Map$Entry; 1
putfield com/a/b/d/hc/c Ljava/util/Map$Entry;
aload 1
athrow
.limit locals 2
.limit stack 3
.end method

.method public final hasNext()Z
aload 0
getfield com/a/b/d/hc/c Ljava/util/Map$Entry;
ifnull L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final synthetic next()Ljava/lang/Object;
aload 0
invokespecial com/a/b/d/hc/a()Ljava/util/Map$Entry;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final remove()V
aload 0
getfield com/a/b/d/hc/b Ljava/util/Map$Entry;
ifnull L0
iconst_1
istore 1
L1:
iload 1
ldc "no calls to next() since the last call to remove()"
invokestatic com/a/b/b/cn/b(ZLjava/lang/Object;)V
aload 0
getfield com/a/b/d/hc/a Lcom/a/b/d/hb;
getfield com/a/b/d/hb/a Lcom/a/b/d/ha;
aload 0
getfield com/a/b/d/hc/b Ljava/util/Map$Entry;
invokeinterface java/util/Map$Entry/getKey()Ljava/lang/Object; 0
invokeinterface java/util/NavigableMap/remove(Ljava/lang/Object;)Ljava/lang/Object; 1
pop
aload 0
aconst_null
putfield com/a/b/d/hc/b Ljava/util/Map$Entry;
return
L0:
iconst_0
istore 1
goto L1
.limit locals 2
.limit stack 2
.end method
