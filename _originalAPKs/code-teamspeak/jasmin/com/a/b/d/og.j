.bytecode 50.0
.class final synchronized com/a/b/d/og
.super java/lang/Object
.implements java/util/Iterator

.field 'a' Lcom/a/b/d/oh;

.field 'b' Lcom/a/b/d/oe;

.field 'c' I

.field final synthetic 'd' Lcom/a/b/d/of;

.method <init>(Lcom/a/b/d/of;)V
aload 0
aload 1
putfield com/a/b/d/og/d Lcom/a/b/d/of;
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 0
getfield com/a/b/d/og/d Lcom/a/b/d/of;
invokestatic com/a/b/d/of/a(Lcom/a/b/d/of;)Lcom/a/b/d/oh;
putfield com/a/b/d/og/a Lcom/a/b/d/oh;
aload 0
aload 0
getfield com/a/b/d/og/d Lcom/a/b/d/of;
invokestatic com/a/b/d/of/b(Lcom/a/b/d/of;)I
putfield com/a/b/d/og/c I
return
.limit locals 2
.limit stack 2
.end method

.method private a()V
aload 0
getfield com/a/b/d/og/d Lcom/a/b/d/of;
invokestatic com/a/b/d/of/b(Lcom/a/b/d/of;)I
aload 0
getfield com/a/b/d/og/c I
if_icmpeq L0
new java/util/ConcurrentModificationException
dup
invokespecial java/util/ConcurrentModificationException/<init>()V
athrow
L0:
return
.limit locals 1
.limit stack 2
.end method

.method public final hasNext()Z
aload 0
invokespecial com/a/b/d/og/a()V
aload 0
getfield com/a/b/d/og/a Lcom/a/b/d/oh;
aload 0
getfield com/a/b/d/og/d Lcom/a/b/d/of;
if_acmpeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method public final next()Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/og/hasNext()Z
ifne L0
new java/util/NoSuchElementException
dup
invokespecial java/util/NoSuchElementException/<init>()V
athrow
L0:
aload 0
getfield com/a/b/d/og/a Lcom/a/b/d/oh;
checkcast com/a/b/d/oe
astore 1
aload 1
invokevirtual com/a/b/d/oe/getValue()Ljava/lang/Object;
astore 2
aload 0
aload 1
putfield com/a/b/d/og/b Lcom/a/b/d/oe;
aload 0
aload 1
getfield com/a/b/d/oe/d Lcom/a/b/d/oh;
putfield com/a/b/d/og/a Lcom/a/b/d/oh;
aload 2
areturn
.limit locals 3
.limit stack 2
.end method

.method public final remove()V
aload 0
invokespecial com/a/b/d/og/a()V
aload 0
getfield com/a/b/d/og/b Lcom/a/b/d/oe;
ifnull L0
iconst_1
istore 1
L1:
iload 1
ldc "no calls to next() since the last call to remove()"
invokestatic com/a/b/b/cn/b(ZLjava/lang/Object;)V
aload 0
getfield com/a/b/d/og/d Lcom/a/b/d/of;
aload 0
getfield com/a/b/d/og/b Lcom/a/b/d/oe;
invokevirtual com/a/b/d/oe/getValue()Ljava/lang/Object;
invokevirtual com/a/b/d/of/remove(Ljava/lang/Object;)Z
pop
aload 0
aload 0
getfield com/a/b/d/og/d Lcom/a/b/d/of;
invokestatic com/a/b/d/of/b(Lcom/a/b/d/of;)I
putfield com/a/b/d/og/c I
aload 0
aconst_null
putfield com/a/b/d/og/b Lcom/a/b/d/oe;
return
L0:
iconst_0
istore 1
goto L1
.limit locals 2
.limit stack 2
.end method
