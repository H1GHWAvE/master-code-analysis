.bytecode 50.0
.class final synchronized com/a/b/d/ug
.super com/a/b/d/uc
.implements java/util/SortedSet

.field final synthetic 'b' Lcom/a/b/d/uf;

.method <init>(Lcom/a/b/d/uf;)V
aload 0
aload 1
putfield com/a/b/d/ug/b Lcom/a/b/d/uf;
aload 0
aload 1
invokespecial com/a/b/d/uc/<init>(Lcom/a/b/d/ty;)V
return
.limit locals 2
.limit stack 2
.end method

.method public final comparator()Ljava/util/Comparator;
aload 0
getfield com/a/b/d/ug/b Lcom/a/b/d/uf;
getfield com/a/b/d/uf/a Ljava/util/Map;
checkcast java/util/SortedMap
invokeinterface java/util/SortedMap/comparator()Ljava/util/Comparator; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method public final first()Ljava/lang/Object;
aload 0
getfield com/a/b/d/ug/b Lcom/a/b/d/uf;
invokevirtual com/a/b/d/uf/firstKey()Ljava/lang/Object;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final headSet(Ljava/lang/Object;)Ljava/util/SortedSet;
aload 0
getfield com/a/b/d/ug/b Lcom/a/b/d/uf;
aload 1
invokevirtual com/a/b/d/uf/headMap(Ljava/lang/Object;)Ljava/util/SortedMap;
invokeinterface java/util/SortedMap/keySet()Ljava/util/Set; 0
checkcast java/util/SortedSet
areturn
.limit locals 2
.limit stack 2
.end method

.method public final last()Ljava/lang/Object;
aload 0
getfield com/a/b/d/ug/b Lcom/a/b/d/uf;
invokevirtual com/a/b/d/uf/lastKey()Ljava/lang/Object;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final subSet(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedSet;
aload 0
getfield com/a/b/d/ug/b Lcom/a/b/d/uf;
aload 1
aload 2
invokevirtual com/a/b/d/uf/subMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedMap;
invokeinterface java/util/SortedMap/keySet()Ljava/util/Set; 0
checkcast java/util/SortedSet
areturn
.limit locals 3
.limit stack 3
.end method

.method public final tailSet(Ljava/lang/Object;)Ljava/util/SortedSet;
aload 0
getfield com/a/b/d/ug/b Lcom/a/b/d/uf;
aload 1
invokevirtual com/a/b/d/uf/tailMap(Ljava/lang/Object;)Ljava/util/SortedMap;
invokeinterface java/util/SortedMap/keySet()Ljava/util/Set; 0
checkcast java/util/SortedSet
areturn
.limit locals 2
.limit stack 2
.end method
