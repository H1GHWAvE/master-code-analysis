.bytecode 50.0
.class public synchronized abstract com/a/b/d/gp
.super com/a/b/d/gh
.implements java/util/List
.annotation invisible Lcom/a/b/a/b;
.end annotation

.method protected <init>()V
aload 0
invokespecial com/a/b/d/gh/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private a(II)Ljava/util/List;
.annotation invisible Lcom/a/b/a/a;
.end annotation
aload 0
instanceof java/util/RandomAccess
ifeq L0
new com/a/b/d/ow
dup
aload 0
invokespecial com/a/b/d/ow/<init>(Ljava/util/List;)V
astore 3
L1:
aload 3
iload 1
iload 2
invokeinterface java/util/List/subList(II)Ljava/util/List; 2
areturn
L0:
new com/a/b/d/ox
dup
aload 0
invokespecial com/a/b/d/ox/<init>(Ljava/util/List;)V
astore 3
goto L1
.limit locals 4
.limit stack 3
.end method

.method private a(I)Ljava/util/ListIterator;
.annotation invisible Lcom/a/b/a/a;
.end annotation
new com/a/b/d/oy
dup
aload 0
invokespecial com/a/b/d/oy/<init>(Ljava/util/List;)V
iload 1
invokevirtual com/a/b/d/oy/listIterator(I)Ljava/util/ListIterator;
areturn
.limit locals 2
.limit stack 3
.end method

.method private a(ILjava/lang/Iterable;)Z
iconst_0
istore 3
aload 0
iload 1
invokeinterface java/util/List/listIterator(I)Ljava/util/ListIterator; 1
astore 4
aload 2
invokeinterface java/lang/Iterable/iterator()Ljava/util/Iterator; 0
astore 2
L0:
aload 2
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 4
aload 2
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
invokeinterface java/util/ListIterator/add(Ljava/lang/Object;)V 1
iconst_1
istore 3
goto L0
L1:
iload 3
ireturn
.limit locals 5
.limit stack 2
.end method

.method private a(Ljava/lang/Object;)Z
aload 0
aload 0
invokevirtual com/a/b/d/gp/size()I
aload 1
invokevirtual com/a/b/d/gp/add(ILjava/lang/Object;)V
iconst_1
ireturn
.limit locals 2
.limit stack 3
.end method

.method private c()Ljava/util/Iterator;
aload 0
invokevirtual com/a/b/d/gp/listIterator()Ljava/util/ListIterator;
areturn
.limit locals 1
.limit stack 1
.end method

.method private d(Ljava/lang/Object;)I
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
aload 1
invokestatic com/a/b/d/ov/b(Ljava/util/List;Ljava/lang/Object;)I
ireturn
.limit locals 2
.limit stack 2
.end method

.method private d()Ljava/util/ListIterator;
aload 0
iconst_0
invokevirtual com/a/b/d/gp/listIterator(I)Ljava/util/ListIterator;
areturn
.limit locals 1
.limit stack 2
.end method

.method private e()I
.annotation invisible Lcom/a/b/a/a;
.end annotation
iconst_1
istore 1
aload 0
invokeinterface java/util/List/iterator()Ljava/util/Iterator; 0
astore 3
L0:
aload 3
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 3
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
astore 4
aload 4
ifnonnull L2
iconst_0
istore 2
L3:
iload 2
iload 1
bipush 31
imul
iadd
iconst_m1
ixor
iconst_m1
ixor
istore 1
goto L0
L2:
aload 4
invokevirtual java/lang/Object/hashCode()I
istore 2
goto L3
L1:
iload 1
ireturn
.limit locals 5
.limit stack 3
.end method

.method private e(Ljava/lang/Object;)I
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
aload 1
invokestatic com/a/b/d/ov/c(Ljava/util/List;Ljava/lang/Object;)I
ireturn
.limit locals 2
.limit stack 2
.end method

.method private f(Ljava/lang/Object;)Z
.annotation invisible Lcom/a/b/a/a;
.end annotation
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
aload 1
invokestatic com/a/b/d/ov/a(Ljava/util/List;Ljava/lang/Object;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method protected abstract a()Ljava/util/List;
.end method

.method public add(ILjava/lang/Object;)V
aload 0
invokevirtual com/a/b/d/gp/a()Ljava/util/List;
iload 1
aload 2
invokeinterface java/util/List/add(ILjava/lang/Object;)V 2
return
.limit locals 3
.limit stack 3
.end method

.method public addAll(ILjava/util/Collection;)Z
aload 0
invokevirtual com/a/b/d/gp/a()Ljava/util/List;
iload 1
aload 2
invokeinterface java/util/List/addAll(ILjava/util/Collection;)Z 2
ireturn
.limit locals 3
.limit stack 3
.end method

.method protected synthetic b()Ljava/util/Collection;
aload 0
invokevirtual com/a/b/d/gp/a()Ljava/util/List;
areturn
.limit locals 1
.limit stack 1
.end method

.method public equals(Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 1
aload 0
if_acmpeq L0
aload 0
invokevirtual com/a/b/d/gp/a()Ljava/util/List;
aload 1
invokeinterface java/util/List/equals(Ljava/lang/Object;)Z 1
ifeq L1
L0:
iconst_1
ireturn
L1:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method public get(I)Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/gp/a()Ljava/util/List;
iload 1
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
areturn
.limit locals 2
.limit stack 2
.end method

.method public hashCode()I
aload 0
invokevirtual com/a/b/d/gp/a()Ljava/util/List;
invokeinterface java/util/List/hashCode()I 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public indexOf(Ljava/lang/Object;)I
aload 0
invokevirtual com/a/b/d/gp/a()Ljava/util/List;
aload 1
invokeinterface java/util/List/indexOf(Ljava/lang/Object;)I 1
ireturn
.limit locals 2
.limit stack 2
.end method

.method protected synthetic k_()Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/gp/a()Ljava/util/List;
areturn
.limit locals 1
.limit stack 1
.end method

.method public lastIndexOf(Ljava/lang/Object;)I
aload 0
invokevirtual com/a/b/d/gp/a()Ljava/util/List;
aload 1
invokeinterface java/util/List/lastIndexOf(Ljava/lang/Object;)I 1
ireturn
.limit locals 2
.limit stack 2
.end method

.method public listIterator()Ljava/util/ListIterator;
aload 0
invokevirtual com/a/b/d/gp/a()Ljava/util/List;
invokeinterface java/util/List/listIterator()Ljava/util/ListIterator; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method public listIterator(I)Ljava/util/ListIterator;
aload 0
invokevirtual com/a/b/d/gp/a()Ljava/util/List;
iload 1
invokeinterface java/util/List/listIterator(I)Ljava/util/ListIterator; 1
areturn
.limit locals 2
.limit stack 2
.end method

.method public remove(I)Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/gp/a()Ljava/util/List;
iload 1
invokeinterface java/util/List/remove(I)Ljava/lang/Object; 1
areturn
.limit locals 2
.limit stack 2
.end method

.method public set(ILjava/lang/Object;)Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/gp/a()Ljava/util/List;
iload 1
aload 2
invokeinterface java/util/List/set(ILjava/lang/Object;)Ljava/lang/Object; 2
areturn
.limit locals 3
.limit stack 3
.end method

.method public subList(II)Ljava/util/List;
aload 0
invokevirtual com/a/b/d/gp/a()Ljava/util/List;
iload 1
iload 2
invokeinterface java/util/List/subList(II)Ljava/util/List; 2
areturn
.limit locals 3
.limit stack 3
.end method
