.bytecode 50.0
.class synchronized abstract com/a/b/d/il
.super java/lang/Object
.implements java/util/Iterator

.field 'b' I

.field 'c' Lcom/a/b/d/ia;

.field 'd' Lcom/a/b/d/ia;

.field 'e' I

.field final synthetic 'f' Lcom/a/b/d/hy;

.method <init>(Lcom/a/b/d/hy;)V
aload 0
aload 1
putfield com/a/b/d/il/f Lcom/a/b/d/hy;
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
iconst_0
putfield com/a/b/d/il/b I
aload 0
aconst_null
putfield com/a/b/d/il/c Lcom/a/b/d/ia;
aload 0
aconst_null
putfield com/a/b/d/il/d Lcom/a/b/d/ia;
aload 0
aload 0
getfield com/a/b/d/il/f Lcom/a/b/d/hy;
invokestatic com/a/b/d/hy/a(Lcom/a/b/d/hy;)I
putfield com/a/b/d/il/e I
return
.limit locals 2
.limit stack 2
.end method

.method private a()V
aload 0
getfield com/a/b/d/il/f Lcom/a/b/d/hy;
invokestatic com/a/b/d/hy/a(Lcom/a/b/d/hy;)I
aload 0
getfield com/a/b/d/il/e I
if_icmpeq L0
new java/util/ConcurrentModificationException
dup
invokespecial java/util/ConcurrentModificationException/<init>()V
athrow
L0:
return
.limit locals 1
.limit stack 2
.end method

.method abstract a(Lcom/a/b/d/ia;)Ljava/lang/Object;
.end method

.method public hasNext()Z
aload 0
invokespecial com/a/b/d/il/a()V
aload 0
getfield com/a/b/d/il/c Lcom/a/b/d/ia;
ifnull L0
iconst_1
ireturn
L1:
aload 0
aload 0
getfield com/a/b/d/il/b I
iconst_1
iadd
putfield com/a/b/d/il/b I
L0:
aload 0
getfield com/a/b/d/il/b I
aload 0
getfield com/a/b/d/il/f Lcom/a/b/d/hy;
invokestatic com/a/b/d/hy/b(Lcom/a/b/d/hy;)[Lcom/a/b/d/ia;
arraylength
if_icmpge L2
aload 0
getfield com/a/b/d/il/f Lcom/a/b/d/hy;
invokestatic com/a/b/d/hy/b(Lcom/a/b/d/hy;)[Lcom/a/b/d/ia;
aload 0
getfield com/a/b/d/il/b I
aaload
ifnull L1
aload 0
getfield com/a/b/d/il/f Lcom/a/b/d/hy;
invokestatic com/a/b/d/hy/b(Lcom/a/b/d/hy;)[Lcom/a/b/d/ia;
astore 2
aload 0
getfield com/a/b/d/il/b I
istore 1
aload 0
iload 1
iconst_1
iadd
putfield com/a/b/d/il/b I
aload 0
aload 2
iload 1
aaload
putfield com/a/b/d/il/c Lcom/a/b/d/ia;
iconst_1
ireturn
L2:
iconst_0
ireturn
.limit locals 3
.limit stack 3
.end method

.method public next()Ljava/lang/Object;
aload 0
invokespecial com/a/b/d/il/a()V
aload 0
invokevirtual com/a/b/d/il/hasNext()Z
ifne L0
new java/util/NoSuchElementException
dup
invokespecial java/util/NoSuchElementException/<init>()V
athrow
L0:
aload 0
getfield com/a/b/d/il/c Lcom/a/b/d/ia;
astore 1
aload 0
aload 1
getfield com/a/b/d/ia/c Lcom/a/b/d/ia;
putfield com/a/b/d/il/c Lcom/a/b/d/ia;
aload 0
aload 1
putfield com/a/b/d/il/d Lcom/a/b/d/ia;
aload 0
aload 1
invokevirtual com/a/b/d/il/a(Lcom/a/b/d/ia;)Ljava/lang/Object;
areturn
.limit locals 2
.limit stack 2
.end method

.method public remove()V
aload 0
invokespecial com/a/b/d/il/a()V
aload 0
getfield com/a/b/d/il/d Lcom/a/b/d/ia;
ifnull L0
iconst_1
istore 1
L1:
iload 1
ldc "no calls to next() since the last call to remove()"
invokestatic com/a/b/b/cn/b(ZLjava/lang/Object;)V
aload 0
getfield com/a/b/d/il/f Lcom/a/b/d/hy;
aload 0
getfield com/a/b/d/il/d Lcom/a/b/d/ia;
invokestatic com/a/b/d/hy/a(Lcom/a/b/d/hy;Lcom/a/b/d/ia;)V
aload 0
aload 0
getfield com/a/b/d/il/f Lcom/a/b/d/hy;
invokestatic com/a/b/d/hy/a(Lcom/a/b/d/hy;)I
putfield com/a/b/d/il/e I
aload 0
aconst_null
putfield com/a/b/d/il/d Lcom/a/b/d/ia;
return
L0:
iconst_0
istore 1
goto L1
.limit locals 2
.limit stack 2
.end method
