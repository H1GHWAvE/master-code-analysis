.bytecode 50.0
.class final synchronized com/a/b/d/w
.super com/a/b/d/z
.implements java/util/NavigableMap
.annotation invisible Lcom/a/b/a/c;
a s = "NavigableAsMap"
.end annotation

.field final synthetic 'c' Lcom/a/b/d/n;

.method <init>(Lcom/a/b/d/n;Ljava/util/NavigableMap;)V
aload 0
aload 1
putfield com/a/b/d/w/c Lcom/a/b/d/n;
aload 0
aload 1
aload 2
invokespecial com/a/b/d/z/<init>(Lcom/a/b/d/n;Ljava/util/SortedMap;)V
return
.limit locals 3
.limit stack 3
.end method

.method private a(Ljava/util/Iterator;)Ljava/util/Map$Entry;
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifne L0
aconst_null
areturn
L0:
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/util/Map$Entry
astore 2
aload 0
getfield com/a/b/d/w/c Lcom/a/b/d/n;
invokevirtual com/a/b/d/n/c()Ljava/util/Collection;
astore 3
aload 3
aload 2
invokeinterface java/util/Map$Entry/getValue()Ljava/lang/Object; 0
checkcast java/util/Collection
invokeinterface java/util/Collection/addAll(Ljava/util/Collection;)Z 1
pop
aload 1
invokeinterface java/util/Iterator/remove()V 0
aload 2
invokeinterface java/util/Map$Entry/getKey()Ljava/lang/Object; 0
aload 0
getfield com/a/b/d/w/c Lcom/a/b/d/n;
aload 3
invokevirtual com/a/b/d/n/a(Ljava/util/Collection;)Ljava/util/Collection;
invokestatic com/a/b/d/sz/a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map$Entry;
areturn
.limit locals 4
.limit stack 3
.end method

.method private a(Ljava/lang/Object;)Ljava/util/NavigableMap;
aload 0
aload 1
iconst_0
invokevirtual com/a/b/d/w/headMap(Ljava/lang/Object;Z)Ljava/util/NavigableMap;
areturn
.limit locals 2
.limit stack 3
.end method

.method private a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/NavigableMap;
aload 0
aload 1
iconst_1
aload 2
iconst_0
invokevirtual com/a/b/d/w/subMap(Ljava/lang/Object;ZLjava/lang/Object;Z)Ljava/util/NavigableMap;
areturn
.limit locals 3
.limit stack 5
.end method

.method private b(Ljava/lang/Object;)Ljava/util/NavigableMap;
aload 0
aload 1
iconst_1
invokevirtual com/a/b/d/w/tailMap(Ljava/lang/Object;Z)Ljava/util/NavigableMap;
areturn
.limit locals 2
.limit stack 3
.end method

.method private f()Ljava/util/NavigableMap;
aload 0
invokespecial com/a/b/d/z/d()Ljava/util/SortedMap;
checkcast java/util/NavigableMap
areturn
.limit locals 1
.limit stack 1
.end method

.method private g()Ljava/util/NavigableSet;
aload 0
invokespecial com/a/b/d/z/c()Ljava/util/SortedSet;
checkcast java/util/NavigableSet
areturn
.limit locals 1
.limit stack 1
.end method

.method private h()Ljava/util/NavigableSet;
new com/a/b/d/x
dup
aload 0
getfield com/a/b/d/w/c Lcom/a/b/d/n;
aload 0
invokespecial com/a/b/d/z/d()Ljava/util/SortedMap;
checkcast java/util/NavigableMap
invokespecial com/a/b/d/x/<init>(Lcom/a/b/d/n;Ljava/util/NavigableMap;)V
areturn
.limit locals 1
.limit stack 4
.end method

.method final synthetic b()Ljava/util/SortedSet;
aload 0
invokespecial com/a/b/d/w/h()Ljava/util/NavigableSet;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final volatile synthetic c()Ljava/util/SortedSet;
aload 0
invokespecial com/a/b/d/z/c()Ljava/util/SortedSet;
checkcast java/util/NavigableSet
areturn
.limit locals 1
.limit stack 1
.end method

.method public final ceilingEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;
aload 0
invokespecial com/a/b/d/z/d()Ljava/util/SortedMap;
checkcast java/util/NavigableMap
aload 1
invokeinterface java/util/NavigableMap/ceilingEntry(Ljava/lang/Object;)Ljava/util/Map$Entry; 1
astore 1
aload 1
ifnonnull L0
aconst_null
areturn
L0:
aload 0
aload 1
invokevirtual com/a/b/d/w/a(Ljava/util/Map$Entry;)Ljava/util/Map$Entry;
areturn
.limit locals 2
.limit stack 2
.end method

.method public final ceilingKey(Ljava/lang/Object;)Ljava/lang/Object;
aload 0
invokespecial com/a/b/d/z/d()Ljava/util/SortedMap;
checkcast java/util/NavigableMap
aload 1
invokeinterface java/util/NavigableMap/ceilingKey(Ljava/lang/Object;)Ljava/lang/Object; 1
areturn
.limit locals 2
.limit stack 2
.end method

.method final volatile synthetic d()Ljava/util/SortedMap;
aload 0
invokespecial com/a/b/d/z/d()Ljava/util/SortedMap;
checkcast java/util/NavigableMap
areturn
.limit locals 1
.limit stack 1
.end method

.method public final descendingKeySet()Ljava/util/NavigableSet;
aload 0
invokevirtual com/a/b/d/w/descendingMap()Ljava/util/NavigableMap;
invokeinterface java/util/NavigableMap/navigableKeySet()Ljava/util/NavigableSet; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method public final descendingMap()Ljava/util/NavigableMap;
new com/a/b/d/w
dup
aload 0
getfield com/a/b/d/w/c Lcom/a/b/d/n;
aload 0
invokespecial com/a/b/d/z/d()Ljava/util/SortedMap;
checkcast java/util/NavigableMap
invokeinterface java/util/NavigableMap/descendingMap()Ljava/util/NavigableMap; 0
invokespecial com/a/b/d/w/<init>(Lcom/a/b/d/n;Ljava/util/NavigableMap;)V
areturn
.limit locals 1
.limit stack 4
.end method

.method final synthetic e()Ljava/util/Set;
aload 0
invokespecial com/a/b/d/w/h()Ljava/util/NavigableSet;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final firstEntry()Ljava/util/Map$Entry;
aload 0
invokespecial com/a/b/d/z/d()Ljava/util/SortedMap;
checkcast java/util/NavigableMap
invokeinterface java/util/NavigableMap/firstEntry()Ljava/util/Map$Entry; 0
astore 1
aload 1
ifnonnull L0
aconst_null
areturn
L0:
aload 0
aload 1
invokevirtual com/a/b/d/w/a(Ljava/util/Map$Entry;)Ljava/util/Map$Entry;
areturn
.limit locals 2
.limit stack 2
.end method

.method public final floorEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;
aload 0
invokespecial com/a/b/d/z/d()Ljava/util/SortedMap;
checkcast java/util/NavigableMap
aload 1
invokeinterface java/util/NavigableMap/floorEntry(Ljava/lang/Object;)Ljava/util/Map$Entry; 1
astore 1
aload 1
ifnonnull L0
aconst_null
areturn
L0:
aload 0
aload 1
invokevirtual com/a/b/d/w/a(Ljava/util/Map$Entry;)Ljava/util/Map$Entry;
areturn
.limit locals 2
.limit stack 2
.end method

.method public final floorKey(Ljava/lang/Object;)Ljava/lang/Object;
aload 0
invokespecial com/a/b/d/z/d()Ljava/util/SortedMap;
checkcast java/util/NavigableMap
aload 1
invokeinterface java/util/NavigableMap/floorKey(Ljava/lang/Object;)Ljava/lang/Object; 1
areturn
.limit locals 2
.limit stack 2
.end method

.method public final headMap(Ljava/lang/Object;Z)Ljava/util/NavigableMap;
new com/a/b/d/w
dup
aload 0
getfield com/a/b/d/w/c Lcom/a/b/d/n;
aload 0
invokespecial com/a/b/d/z/d()Ljava/util/SortedMap;
checkcast java/util/NavigableMap
aload 1
iload 2
invokeinterface java/util/NavigableMap/headMap(Ljava/lang/Object;Z)Ljava/util/NavigableMap; 2
invokespecial com/a/b/d/w/<init>(Lcom/a/b/d/n;Ljava/util/NavigableMap;)V
areturn
.limit locals 3
.limit stack 6
.end method

.method public final volatile synthetic headMap(Ljava/lang/Object;)Ljava/util/SortedMap;
aload 0
aload 1
iconst_0
invokevirtual com/a/b/d/w/headMap(Ljava/lang/Object;Z)Ljava/util/NavigableMap;
areturn
.limit locals 2
.limit stack 3
.end method

.method public final higherEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;
aload 0
invokespecial com/a/b/d/z/d()Ljava/util/SortedMap;
checkcast java/util/NavigableMap
aload 1
invokeinterface java/util/NavigableMap/higherEntry(Ljava/lang/Object;)Ljava/util/Map$Entry; 1
astore 1
aload 1
ifnonnull L0
aconst_null
areturn
L0:
aload 0
aload 1
invokevirtual com/a/b/d/w/a(Ljava/util/Map$Entry;)Ljava/util/Map$Entry;
areturn
.limit locals 2
.limit stack 2
.end method

.method public final higherKey(Ljava/lang/Object;)Ljava/lang/Object;
aload 0
invokespecial com/a/b/d/z/d()Ljava/util/SortedMap;
checkcast java/util/NavigableMap
aload 1
invokeinterface java/util/NavigableMap/higherKey(Ljava/lang/Object;)Ljava/lang/Object; 1
areturn
.limit locals 2
.limit stack 2
.end method

.method public final synthetic keySet()Ljava/util/Set;
aload 0
invokespecial com/a/b/d/z/c()Ljava/util/SortedSet;
checkcast java/util/NavigableSet
areturn
.limit locals 1
.limit stack 1
.end method

.method public final lastEntry()Ljava/util/Map$Entry;
aload 0
invokespecial com/a/b/d/z/d()Ljava/util/SortedMap;
checkcast java/util/NavigableMap
invokeinterface java/util/NavigableMap/lastEntry()Ljava/util/Map$Entry; 0
astore 1
aload 1
ifnonnull L0
aconst_null
areturn
L0:
aload 0
aload 1
invokevirtual com/a/b/d/w/a(Ljava/util/Map$Entry;)Ljava/util/Map$Entry;
areturn
.limit locals 2
.limit stack 2
.end method

.method public final lowerEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;
aload 0
invokespecial com/a/b/d/z/d()Ljava/util/SortedMap;
checkcast java/util/NavigableMap
aload 1
invokeinterface java/util/NavigableMap/lowerEntry(Ljava/lang/Object;)Ljava/util/Map$Entry; 1
astore 1
aload 1
ifnonnull L0
aconst_null
areturn
L0:
aload 0
aload 1
invokevirtual com/a/b/d/w/a(Ljava/util/Map$Entry;)Ljava/util/Map$Entry;
areturn
.limit locals 2
.limit stack 2
.end method

.method public final lowerKey(Ljava/lang/Object;)Ljava/lang/Object;
aload 0
invokespecial com/a/b/d/z/d()Ljava/util/SortedMap;
checkcast java/util/NavigableMap
aload 1
invokeinterface java/util/NavigableMap/lowerKey(Ljava/lang/Object;)Ljava/lang/Object; 1
areturn
.limit locals 2
.limit stack 2
.end method

.method public final navigableKeySet()Ljava/util/NavigableSet;
aload 0
invokespecial com/a/b/d/z/c()Ljava/util/SortedSet;
checkcast java/util/NavigableSet
areturn
.limit locals 1
.limit stack 1
.end method

.method public final pollFirstEntry()Ljava/util/Map$Entry;
aload 0
aload 0
invokevirtual com/a/b/d/w/entrySet()Ljava/util/Set;
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
invokespecial com/a/b/d/w/a(Ljava/util/Iterator;)Ljava/util/Map$Entry;
areturn
.limit locals 1
.limit stack 2
.end method

.method public final pollLastEntry()Ljava/util/Map$Entry;
aload 0
aload 0
invokevirtual com/a/b/d/w/descendingMap()Ljava/util/NavigableMap;
invokeinterface java/util/NavigableMap/entrySet()Ljava/util/Set; 0
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
invokespecial com/a/b/d/w/a(Ljava/util/Iterator;)Ljava/util/Map$Entry;
areturn
.limit locals 1
.limit stack 2
.end method

.method public final subMap(Ljava/lang/Object;ZLjava/lang/Object;Z)Ljava/util/NavigableMap;
new com/a/b/d/w
dup
aload 0
getfield com/a/b/d/w/c Lcom/a/b/d/n;
aload 0
invokespecial com/a/b/d/z/d()Ljava/util/SortedMap;
checkcast java/util/NavigableMap
aload 1
iload 2
aload 3
iload 4
invokeinterface java/util/NavigableMap/subMap(Ljava/lang/Object;ZLjava/lang/Object;Z)Ljava/util/NavigableMap; 4
invokespecial com/a/b/d/w/<init>(Lcom/a/b/d/n;Ljava/util/NavigableMap;)V
areturn
.limit locals 5
.limit stack 8
.end method

.method public final volatile synthetic subMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedMap;
aload 0
aload 1
iconst_1
aload 2
iconst_0
invokevirtual com/a/b/d/w/subMap(Ljava/lang/Object;ZLjava/lang/Object;Z)Ljava/util/NavigableMap;
areturn
.limit locals 3
.limit stack 5
.end method

.method public final tailMap(Ljava/lang/Object;Z)Ljava/util/NavigableMap;
new com/a/b/d/w
dup
aload 0
getfield com/a/b/d/w/c Lcom/a/b/d/n;
aload 0
invokespecial com/a/b/d/z/d()Ljava/util/SortedMap;
checkcast java/util/NavigableMap
aload 1
iload 2
invokeinterface java/util/NavigableMap/tailMap(Ljava/lang/Object;Z)Ljava/util/NavigableMap; 2
invokespecial com/a/b/d/w/<init>(Lcom/a/b/d/n;Ljava/util/NavigableMap;)V
areturn
.limit locals 3
.limit stack 6
.end method

.method public final volatile synthetic tailMap(Ljava/lang/Object;)Ljava/util/SortedMap;
aload 0
aload 1
iconst_1
invokevirtual com/a/b/d/w/tailMap(Ljava/lang/Object;Z)Ljava/util/NavigableMap;
areturn
.limit locals 2
.limit stack 3
.end method
