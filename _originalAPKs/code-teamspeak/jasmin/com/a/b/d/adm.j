.bytecode 50.0
.class final synchronized com/a/b/d/adm
.super com/a/b/d/adt
.implements java/util/NavigableSet
.annotation invisible Lcom/a/b/a/c;
a s = "NavigableSet"
.end annotation
.annotation invisible Lcom/a/b/a/d;
.end annotation

.field private static final 'b' J = 0L


.field transient 'a' Ljava/util/NavigableSet;

.method <init>(Ljava/util/NavigableSet;Ljava/lang/Object;)V
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
aload 0
aload 1
aload 2
invokespecial com/a/b/d/adt/<init>(Ljava/util/SortedSet;Ljava/lang/Object;)V
return
.limit locals 3
.limit stack 3
.end method

.method private e()Ljava/util/NavigableSet;
aload 0
invokespecial com/a/b/d/adt/a()Ljava/util/SortedSet;
checkcast java/util/NavigableSet
areturn
.limit locals 1
.limit stack 1
.end method

.method final volatile synthetic a()Ljava/util/SortedSet;
aload 0
invokespecial com/a/b/d/adt/a()Ljava/util/SortedSet;
checkcast java/util/NavigableSet
areturn
.limit locals 1
.limit stack 1
.end method

.method final synthetic b()Ljava/util/Collection;
aload 0
invokespecial com/a/b/d/adt/a()Ljava/util/SortedSet;
checkcast java/util/NavigableSet
areturn
.limit locals 1
.limit stack 1
.end method

.method final synthetic c()Ljava/util/Set;
aload 0
invokespecial com/a/b/d/adt/a()Ljava/util/SortedSet;
checkcast java/util/NavigableSet
areturn
.limit locals 1
.limit stack 1
.end method

.method public final ceiling(Ljava/lang/Object;)Ljava/lang/Object;
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/adm/h Ljava/lang/Object;
astore 2
aload 2
monitorenter
L0:
aload 0
invokespecial com/a/b/d/adt/a()Ljava/util/SortedSet;
checkcast java/util/NavigableSet
aload 1
invokeinterface java/util/NavigableSet/ceiling(Ljava/lang/Object;)Ljava/lang/Object; 1
astore 1
aload 2
monitorexit
L1:
aload 1
areturn
L2:
astore 1
L3:
aload 2
monitorexit
L4:
aload 1
athrow
.limit locals 3
.limit stack 2
.end method

.method final synthetic d()Ljava/lang/Object;
aload 0
invokespecial com/a/b/d/adt/a()Ljava/util/SortedSet;
checkcast java/util/NavigableSet
areturn
.limit locals 1
.limit stack 1
.end method

.method public final descendingIterator()Ljava/util/Iterator;
aload 0
invokespecial com/a/b/d/adt/a()Ljava/util/SortedSet;
checkcast java/util/NavigableSet
invokeinterface java/util/NavigableSet/descendingIterator()Ljava/util/Iterator; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method public final descendingSet()Ljava/util/NavigableSet;
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch all from L5 to L6 using L2
aload 0
getfield com/a/b/d/adm/h Ljava/lang/Object;
astore 1
aload 1
monitorenter
L0:
aload 0
getfield com/a/b/d/adm/a Ljava/util/NavigableSet;
ifnonnull L3
aload 0
invokespecial com/a/b/d/adt/a()Ljava/util/SortedSet;
checkcast java/util/NavigableSet
invokeinterface java/util/NavigableSet/descendingSet()Ljava/util/NavigableSet; 0
aload 0
getfield com/a/b/d/adm/h Ljava/lang/Object;
invokestatic com/a/b/d/acu/a(Ljava/util/NavigableSet;Ljava/lang/Object;)Ljava/util/NavigableSet;
astore 2
aload 0
aload 2
putfield com/a/b/d/adm/a Ljava/util/NavigableSet;
aload 1
monitorexit
L1:
aload 2
areturn
L3:
aload 0
getfield com/a/b/d/adm/a Ljava/util/NavigableSet;
astore 2
aload 1
monitorexit
L4:
aload 2
areturn
L2:
astore 2
L5:
aload 1
monitorexit
L6:
aload 2
athrow
.limit locals 3
.limit stack 2
.end method

.method public final floor(Ljava/lang/Object;)Ljava/lang/Object;
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/adm/h Ljava/lang/Object;
astore 2
aload 2
monitorenter
L0:
aload 0
invokespecial com/a/b/d/adt/a()Ljava/util/SortedSet;
checkcast java/util/NavigableSet
aload 1
invokeinterface java/util/NavigableSet/floor(Ljava/lang/Object;)Ljava/lang/Object; 1
astore 1
aload 2
monitorexit
L1:
aload 1
areturn
L2:
astore 1
L3:
aload 2
monitorexit
L4:
aload 1
athrow
.limit locals 3
.limit stack 2
.end method

.method public final headSet(Ljava/lang/Object;Z)Ljava/util/NavigableSet;
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/adm/h Ljava/lang/Object;
astore 3
aload 3
monitorenter
L0:
aload 0
invokespecial com/a/b/d/adt/a()Ljava/util/SortedSet;
checkcast java/util/NavigableSet
aload 1
iload 2
invokeinterface java/util/NavigableSet/headSet(Ljava/lang/Object;Z)Ljava/util/NavigableSet; 2
aload 0
getfield com/a/b/d/adm/h Ljava/lang/Object;
invokestatic com/a/b/d/acu/a(Ljava/util/NavigableSet;Ljava/lang/Object;)Ljava/util/NavigableSet;
astore 1
aload 3
monitorexit
L1:
aload 1
areturn
L2:
astore 1
L3:
aload 3
monitorexit
L4:
aload 1
athrow
.limit locals 4
.limit stack 3
.end method

.method public final headSet(Ljava/lang/Object;)Ljava/util/SortedSet;
aload 0
aload 1
iconst_0
invokevirtual com/a/b/d/adm/headSet(Ljava/lang/Object;Z)Ljava/util/NavigableSet;
areturn
.limit locals 2
.limit stack 3
.end method

.method public final higher(Ljava/lang/Object;)Ljava/lang/Object;
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/adm/h Ljava/lang/Object;
astore 2
aload 2
monitorenter
L0:
aload 0
invokespecial com/a/b/d/adt/a()Ljava/util/SortedSet;
checkcast java/util/NavigableSet
aload 1
invokeinterface java/util/NavigableSet/higher(Ljava/lang/Object;)Ljava/lang/Object; 1
astore 1
aload 2
monitorexit
L1:
aload 1
areturn
L2:
astore 1
L3:
aload 2
monitorexit
L4:
aload 1
athrow
.limit locals 3
.limit stack 2
.end method

.method public final lower(Ljava/lang/Object;)Ljava/lang/Object;
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/adm/h Ljava/lang/Object;
astore 2
aload 2
monitorenter
L0:
aload 0
invokespecial com/a/b/d/adt/a()Ljava/util/SortedSet;
checkcast java/util/NavigableSet
aload 1
invokeinterface java/util/NavigableSet/lower(Ljava/lang/Object;)Ljava/lang/Object; 1
astore 1
aload 2
monitorexit
L1:
aload 1
areturn
L2:
astore 1
L3:
aload 2
monitorexit
L4:
aload 1
athrow
.limit locals 3
.limit stack 2
.end method

.method public final pollFirst()Ljava/lang/Object;
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/adm/h Ljava/lang/Object;
astore 1
aload 1
monitorenter
L0:
aload 0
invokespecial com/a/b/d/adt/a()Ljava/util/SortedSet;
checkcast java/util/NavigableSet
invokeinterface java/util/NavigableSet/pollFirst()Ljava/lang/Object; 0
astore 2
aload 1
monitorexit
L1:
aload 2
areturn
L2:
astore 2
L3:
aload 1
monitorexit
L4:
aload 2
athrow
.limit locals 3
.limit stack 1
.end method

.method public final pollLast()Ljava/lang/Object;
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/adm/h Ljava/lang/Object;
astore 1
aload 1
monitorenter
L0:
aload 0
invokespecial com/a/b/d/adt/a()Ljava/util/SortedSet;
checkcast java/util/NavigableSet
invokeinterface java/util/NavigableSet/pollLast()Ljava/lang/Object; 0
astore 2
aload 1
monitorexit
L1:
aload 2
areturn
L2:
astore 2
L3:
aload 1
monitorexit
L4:
aload 2
athrow
.limit locals 3
.limit stack 1
.end method

.method public final subSet(Ljava/lang/Object;ZLjava/lang/Object;Z)Ljava/util/NavigableSet;
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/adm/h Ljava/lang/Object;
astore 5
aload 5
monitorenter
L0:
aload 0
invokespecial com/a/b/d/adt/a()Ljava/util/SortedSet;
checkcast java/util/NavigableSet
aload 1
iload 2
aload 3
iload 4
invokeinterface java/util/NavigableSet/subSet(Ljava/lang/Object;ZLjava/lang/Object;Z)Ljava/util/NavigableSet; 4
aload 0
getfield com/a/b/d/adm/h Ljava/lang/Object;
invokestatic com/a/b/d/acu/a(Ljava/util/NavigableSet;Ljava/lang/Object;)Ljava/util/NavigableSet;
astore 1
aload 5
monitorexit
L1:
aload 1
areturn
L2:
astore 1
L3:
aload 5
monitorexit
L4:
aload 1
athrow
.limit locals 6
.limit stack 5
.end method

.method public final subSet(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedSet;
aload 0
aload 1
iconst_1
aload 2
iconst_0
invokevirtual com/a/b/d/adm/subSet(Ljava/lang/Object;ZLjava/lang/Object;Z)Ljava/util/NavigableSet;
areturn
.limit locals 3
.limit stack 5
.end method

.method public final tailSet(Ljava/lang/Object;Z)Ljava/util/NavigableSet;
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/adm/h Ljava/lang/Object;
astore 3
aload 3
monitorenter
L0:
aload 0
invokespecial com/a/b/d/adt/a()Ljava/util/SortedSet;
checkcast java/util/NavigableSet
aload 1
iload 2
invokeinterface java/util/NavigableSet/tailSet(Ljava/lang/Object;Z)Ljava/util/NavigableSet; 2
aload 0
getfield com/a/b/d/adm/h Ljava/lang/Object;
invokestatic com/a/b/d/acu/a(Ljava/util/NavigableSet;Ljava/lang/Object;)Ljava/util/NavigableSet;
astore 1
aload 3
monitorexit
L1:
aload 1
areturn
L2:
astore 1
L3:
aload 3
monitorexit
L4:
aload 1
athrow
.limit locals 4
.limit stack 3
.end method

.method public final tailSet(Ljava/lang/Object;)Ljava/util/SortedSet;
aload 0
aload 1
iconst_1
invokevirtual com/a/b/d/adm/tailSet(Ljava/lang/Object;Z)Ljava/util/NavigableSet;
areturn
.limit locals 2
.limit stack 3
.end method
