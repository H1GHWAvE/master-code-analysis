.bytecode 50.0
.class synchronized abstract com/a/b/d/n
.super com/a/b/d/an
.implements java/io/Serializable
.annotation invisible Lcom/a/b/a/b;
b Z = 1
.end annotation

.field private static final 'c' J = 2447537837011683357L


.field private transient 'a' Ljava/util/Map;

.field private transient 'b' I

.method protected <init>(Ljava/util/Map;)V
aload 0
invokespecial com/a/b/d/an/<init>()V
aload 1
invokeinterface java/util/Map/isEmpty()Z 0
invokestatic com/a/b/b/cn/a(Z)V
aload 0
aload 1
putfield com/a/b/d/n/a Ljava/util/Map;
return
.limit locals 2
.limit stack 2
.end method

.method static synthetic a(Lcom/a/b/d/n;I)I
aload 0
getfield com/a/b/d/n/b I
iload 1
iadd
istore 1
aload 0
iload 1
putfield com/a/b/d/n/b I
iload 1
ireturn
.limit locals 2
.limit stack 2
.end method

.method static synthetic a(Lcom/a/b/d/n;Ljava/lang/Object;)I
aload 0
getfield com/a/b/d/n/a Ljava/util/Map;
aload 1
invokestatic com/a/b/d/sz/c(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/util/Collection
astore 1
iconst_0
istore 2
aload 1
ifnull L0
aload 1
invokeinterface java/util/Collection/size()I 0
istore 2
aload 1
invokeinterface java/util/Collection/clear()V 0
aload 0
aload 0
getfield com/a/b/d/n/b I
iload 2
isub
putfield com/a/b/d/n/b I
L0:
iload 2
ireturn
.limit locals 3
.limit stack 3
.end method

.method static synthetic a(Lcom/a/b/d/n;Ljava/lang/Object;Ljava/util/List;Lcom/a/b/d/ab;)Ljava/util/List;
aload 0
aload 1
aload 2
aload 3
invokespecial com/a/b/d/n/a(Ljava/lang/Object;Ljava/util/List;Lcom/a/b/d/ab;)Ljava/util/List;
areturn
.limit locals 4
.limit stack 4
.end method

.method private a(Ljava/lang/Object;Ljava/util/List;Lcom/a/b/d/ab;)Ljava/util/List;
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
.annotation visibleparam 3 Ljavax/annotation/Nullable;
.end annotation
aload 2
instanceof java/util/RandomAccess
ifeq L0
new com/a/b/d/y
dup
aload 0
aload 1
aload 2
aload 3
invokespecial com/a/b/d/y/<init>(Lcom/a/b/d/n;Ljava/lang/Object;Ljava/util/List;Lcom/a/b/d/ab;)V
areturn
L0:
new com/a/b/d/ad
dup
aload 0
aload 1
aload 2
aload 3
invokespecial com/a/b/d/ad/<init>(Lcom/a/b/d/n;Ljava/lang/Object;Ljava/util/List;Lcom/a/b/d/ab;)V
areturn
.limit locals 4
.limit stack 6
.end method

.method static synthetic a(Lcom/a/b/d/n;)Ljava/util/Map;
aload 0
getfield com/a/b/d/n/a Ljava/util/Map;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic b(Lcom/a/b/d/n;)I
aload 0
getfield com/a/b/d/n/b I
istore 1
aload 0
iload 1
iconst_1
isub
putfield com/a/b/d/n/b I
iload 1
ireturn
.limit locals 2
.limit stack 3
.end method

.method static synthetic b(Lcom/a/b/d/n;I)I
aload 0
getfield com/a/b/d/n/b I
iload 1
isub
istore 1
aload 0
iload 1
putfield com/a/b/d/n/b I
iload 1
ireturn
.limit locals 2
.limit stack 2
.end method

.method static synthetic b(Ljava/util/Collection;)Ljava/util/Iterator;
aload 0
instanceof java/util/List
ifeq L0
aload 0
checkcast java/util/List
invokeinterface java/util/List/listIterator()Ljava/util/ListIterator; 0
areturn
L0:
aload 0
invokeinterface java/util/Collection/iterator()Ljava/util/Iterator; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic c(Lcom/a/b/d/n;)I
aload 0
getfield com/a/b/d/n/b I
istore 1
aload 0
iload 1
iconst_1
iadd
putfield com/a/b/d/n/b I
iload 1
ireturn
.limit locals 2
.limit stack 3
.end method

.method private static c(Ljava/util/Collection;)Ljava/util/Iterator;
aload 0
instanceof java/util/List
ifeq L0
aload 0
checkcast java/util/List
invokeinterface java/util/List/listIterator()Ljava/util/ListIterator; 0
areturn
L0:
aload 0
invokeinterface java/util/Collection/iterator()Ljava/util/Iterator; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method private j(Ljava/lang/Object;)Ljava/util/Collection;
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
getfield com/a/b/d/n/a Ljava/util/Map;
aload 1
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast java/util/Collection
astore 3
aload 3
astore 2
aload 3
ifnonnull L0
aload 0
aload 1
invokevirtual com/a/b/d/n/e(Ljava/lang/Object;)Ljava/util/Collection;
astore 2
aload 0
getfield com/a/b/d/n/a Ljava/util/Map;
aload 1
aload 2
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
L0:
aload 2
areturn
.limit locals 4
.limit stack 3
.end method

.method private k(Ljava/lang/Object;)I
aload 0
getfield com/a/b/d/n/a Ljava/util/Map;
aload 1
invokestatic com/a/b/d/sz/c(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/util/Collection
astore 1
iconst_0
istore 2
aload 1
ifnull L0
aload 1
invokeinterface java/util/Collection/size()I 0
istore 2
aload 1
invokeinterface java/util/Collection/clear()V 0
aload 0
aload 0
getfield com/a/b/d/n/b I
iload 2
isub
putfield com/a/b/d/n/b I
L0:
iload 2
ireturn
.limit locals 3
.limit stack 3
.end method

.method a(Ljava/lang/Object;Ljava/util/Collection;)Ljava/util/Collection;
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 2
instanceof java/util/SortedSet
ifeq L0
new com/a/b/d/ah
dup
aload 0
aload 1
aload 2
checkcast java/util/SortedSet
aconst_null
invokespecial com/a/b/d/ah/<init>(Lcom/a/b/d/n;Ljava/lang/Object;Ljava/util/SortedSet;Lcom/a/b/d/ab;)V
areturn
L0:
aload 2
instanceof java/util/Set
ifeq L1
new com/a/b/d/ag
dup
aload 0
aload 1
aload 2
checkcast java/util/Set
invokespecial com/a/b/d/ag/<init>(Lcom/a/b/d/n;Ljava/lang/Object;Ljava/util/Set;)V
areturn
L1:
aload 2
instanceof java/util/List
ifeq L2
aload 0
aload 1
aload 2
checkcast java/util/List
aconst_null
invokespecial com/a/b/d/n/a(Ljava/lang/Object;Ljava/util/List;Lcom/a/b/d/ab;)Ljava/util/List;
areturn
L2:
new com/a/b/d/ab
dup
aload 0
aload 1
aload 2
aconst_null
invokespecial com/a/b/d/ab/<init>(Lcom/a/b/d/n;Ljava/lang/Object;Ljava/util/Collection;Lcom/a/b/d/ab;)V
areturn
.limit locals 3
.limit stack 6
.end method

.method a(Ljava/util/Collection;)Ljava/util/Collection;
aload 1
instanceof java/util/SortedSet
ifeq L0
aload 1
checkcast java/util/SortedSet
invokestatic java/util/Collections/unmodifiableSortedSet(Ljava/util/SortedSet;)Ljava/util/SortedSet;
areturn
L0:
aload 1
instanceof java/util/Set
ifeq L1
aload 1
checkcast java/util/Set
invokestatic java/util/Collections/unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;
areturn
L1:
aload 1
instanceof java/util/List
ifeq L2
aload 1
checkcast java/util/List
invokestatic java/util/Collections/unmodifiableList(Ljava/util/List;)Ljava/util/List;
areturn
L2:
aload 1
invokestatic java/util/Collections/unmodifiableCollection(Ljava/util/Collection;)Ljava/util/Collection;
areturn
.limit locals 2
.limit stack 1
.end method

.method final a(Ljava/util/Map;)V
aload 0
aload 1
putfield com/a/b/d/n/a Ljava/util/Map;
aload 0
iconst_0
putfield com/a/b/d/n/b I
aload 1
invokeinterface java/util/Map/values()Ljava/util/Collection; 0
invokeinterface java/util/Collection/iterator()Ljava/util/Iterator; 0
astore 1
L0:
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/util/Collection
astore 4
aload 4
invokeinterface java/util/Collection/isEmpty()Z 0
ifne L2
iconst_1
istore 3
L3:
iload 3
invokestatic com/a/b/b/cn/a(Z)V
aload 0
getfield com/a/b/d/n/b I
istore 2
aload 0
aload 4
invokeinterface java/util/Collection/size()I 0
iload 2
iadd
putfield com/a/b/d/n/b I
goto L0
L2:
iconst_0
istore 3
goto L3
L1:
return
.limit locals 5
.limit stack 3
.end method

.method public a(Ljava/lang/Object;Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
aload 0
getfield com/a/b/d/n/a Ljava/util/Map;
aload 1
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast java/util/Collection
astore 3
aload 3
ifnonnull L0
aload 0
aload 1
invokevirtual com/a/b/d/n/e(Ljava/lang/Object;)Ljava/util/Collection;
astore 3
aload 3
aload 2
invokeinterface java/util/Collection/add(Ljava/lang/Object;)Z 1
ifeq L1
aload 0
aload 0
getfield com/a/b/d/n/b I
iconst_1
iadd
putfield com/a/b/d/n/b I
aload 0
getfield com/a/b/d/n/a Ljava/util/Map;
aload 1
aload 3
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
iconst_1
ireturn
L1:
new java/lang/AssertionError
dup
ldc "New Collection violated the Collection spec"
invokespecial java/lang/AssertionError/<init>(Ljava/lang/Object;)V
athrow
L0:
aload 3
aload 2
invokeinterface java/util/Collection/add(Ljava/lang/Object;)Z 1
ifeq L2
aload 0
aload 0
getfield com/a/b/d/n/b I
iconst_1
iadd
putfield com/a/b/d/n/b I
iconst_1
ireturn
L2:
iconst_0
ireturn
.limit locals 4
.limit stack 3
.end method

.method public b(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/Collection;
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 2
invokeinterface java/lang/Iterable/iterator()Ljava/util/Iterator; 0
astore 4
aload 4
invokeinterface java/util/Iterator/hasNext()Z 0
ifne L0
aload 0
aload 1
invokevirtual com/a/b/d/n/d(Ljava/lang/Object;)Ljava/util/Collection;
areturn
L0:
aload 0
getfield com/a/b/d/n/a Ljava/util/Map;
aload 1
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast java/util/Collection
astore 3
aload 3
astore 2
aload 3
ifnonnull L1
aload 0
aload 1
invokevirtual com/a/b/d/n/e(Ljava/lang/Object;)Ljava/util/Collection;
astore 2
aload 0
getfield com/a/b/d/n/a Ljava/util/Map;
aload 1
aload 2
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
L1:
aload 0
invokevirtual com/a/b/d/n/c()Ljava/util/Collection;
astore 1
aload 1
aload 2
invokeinterface java/util/Collection/addAll(Ljava/util/Collection;)Z 1
pop
aload 0
aload 0
getfield com/a/b/d/n/b I
aload 2
invokeinterface java/util/Collection/size()I 0
isub
putfield com/a/b/d/n/b I
aload 2
invokeinterface java/util/Collection/clear()V 0
L2:
aload 4
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L3
aload 2
aload 4
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
invokeinterface java/util/Collection/add(Ljava/lang/Object;)Z 1
ifeq L2
aload 0
aload 0
getfield com/a/b/d/n/b I
iconst_1
iadd
putfield com/a/b/d/n/b I
goto L2
L3:
aload 0
aload 1
invokevirtual com/a/b/d/n/a(Ljava/util/Collection;)Ljava/util/Collection;
areturn
.limit locals 5
.limit stack 3
.end method

.method abstract c()Ljava/util/Collection;
.end method

.method public c(Ljava/lang/Object;)Ljava/util/Collection;
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
getfield com/a/b/d/n/a Ljava/util/Map;
aload 1
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast java/util/Collection
astore 3
aload 3
astore 2
aload 3
ifnonnull L0
aload 0
aload 1
invokevirtual com/a/b/d/n/e(Ljava/lang/Object;)Ljava/util/Collection;
astore 2
L0:
aload 0
aload 1
aload 2
invokevirtual com/a/b/d/n/a(Ljava/lang/Object;Ljava/util/Collection;)Ljava/util/Collection;
areturn
.limit locals 4
.limit stack 3
.end method

.method d()Ljava/util/Collection;
aload 0
aload 0
invokevirtual com/a/b/d/n/c()Ljava/util/Collection;
invokevirtual com/a/b/d/n/a(Ljava/util/Collection;)Ljava/util/Collection;
areturn
.limit locals 1
.limit stack 2
.end method

.method public d(Ljava/lang/Object;)Ljava/util/Collection;
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
getfield com/a/b/d/n/a Ljava/util/Map;
aload 1
invokeinterface java/util/Map/remove(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast java/util/Collection
astore 1
aload 1
ifnonnull L0
aload 0
invokevirtual com/a/b/d/n/d()Ljava/util/Collection;
areturn
L0:
aload 0
invokevirtual com/a/b/d/n/c()Ljava/util/Collection;
astore 2
aload 2
aload 1
invokeinterface java/util/Collection/addAll(Ljava/util/Collection;)Z 1
pop
aload 0
aload 0
getfield com/a/b/d/n/b I
aload 1
invokeinterface java/util/Collection/size()I 0
isub
putfield com/a/b/d/n/b I
aload 1
invokeinterface java/util/Collection/clear()V 0
aload 0
aload 2
invokevirtual com/a/b/d/n/a(Ljava/util/Collection;)Ljava/util/Collection;
areturn
.limit locals 3
.limit stack 3
.end method

.method e(Ljava/lang/Object;)Ljava/util/Collection;
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
invokevirtual com/a/b/d/n/c()Ljava/util/Collection;
areturn
.limit locals 2
.limit stack 1
.end method

.method e()Ljava/util/Map;
aload 0
getfield com/a/b/d/n/a Ljava/util/Map;
areturn
.limit locals 1
.limit stack 1
.end method

.method public f()I
aload 0
getfield com/a/b/d/n/b I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public f(Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
getfield com/a/b/d/n/a Ljava/util/Map;
aload 1
invokeinterface java/util/Map/containsKey(Ljava/lang/Object;)Z 1
ireturn
.limit locals 2
.limit stack 2
.end method

.method public g()V
aload 0
getfield com/a/b/d/n/a Ljava/util/Map;
invokeinterface java/util/Map/values()Ljava/util/Collection; 0
invokeinterface java/util/Collection/iterator()Ljava/util/Iterator; 0
astore 1
L0:
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/util/Collection
invokeinterface java/util/Collection/clear()V 0
goto L0
L1:
aload 0
getfield com/a/b/d/n/a Ljava/util/Map;
invokeinterface java/util/Map/clear()V 0
aload 0
iconst_0
putfield com/a/b/d/n/b I
return
.limit locals 2
.limit stack 2
.end method

.method h()Ljava/util/Set;
aload 0
getfield com/a/b/d/n/a Ljava/util/Map;
instanceof java/util/SortedMap
ifeq L0
new com/a/b/d/aa
dup
aload 0
aload 0
getfield com/a/b/d/n/a Ljava/util/Map;
checkcast java/util/SortedMap
invokespecial com/a/b/d/aa/<init>(Lcom/a/b/d/n;Ljava/util/SortedMap;)V
areturn
L0:
new com/a/b/d/u
dup
aload 0
aload 0
getfield com/a/b/d/n/a Ljava/util/Map;
invokespecial com/a/b/d/u/<init>(Lcom/a/b/d/n;Ljava/util/Map;)V
areturn
.limit locals 1
.limit stack 4
.end method

.method public i()Ljava/util/Collection;
aload 0
invokespecial com/a/b/d/an/i()Ljava/util/Collection;
areturn
.limit locals 1
.limit stack 1
.end method

.method j()Ljava/util/Iterator;
new com/a/b/d/o
dup
aload 0
invokespecial com/a/b/d/o/<init>(Lcom/a/b/d/n;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method public k()Ljava/util/Collection;
aload 0
invokespecial com/a/b/d/an/k()Ljava/util/Collection;
areturn
.limit locals 1
.limit stack 1
.end method

.method l()Ljava/util/Iterator;
new com/a/b/d/p
dup
aload 0
invokespecial com/a/b/d/p/<init>(Lcom/a/b/d/n;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method m()Ljava/util/Map;
aload 0
getfield com/a/b/d/n/a Ljava/util/Map;
instanceof java/util/SortedMap
ifeq L0
new com/a/b/d/z
dup
aload 0
aload 0
getfield com/a/b/d/n/a Ljava/util/Map;
checkcast java/util/SortedMap
invokespecial com/a/b/d/z/<init>(Lcom/a/b/d/n;Ljava/util/SortedMap;)V
areturn
L0:
new com/a/b/d/q
dup
aload 0
aload 0
getfield com/a/b/d/n/a Ljava/util/Map;
invokespecial com/a/b/d/q/<init>(Lcom/a/b/d/n;Ljava/util/Map;)V
areturn
.limit locals 1
.limit stack 4
.end method
