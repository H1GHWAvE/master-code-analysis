.bytecode 50.0
.class synchronized abstract com/a/b/d/bf
.super java/lang/Object
.implements com/a/b/d/adv
.annotation invisible Lcom/a/b/a/b;
.end annotation

.field private transient 'a' Ljava/util/Set;

.field private transient 'b' Ljava/util/Collection;

.method <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
aload 0
aload 1
invokevirtual com/a/b/d/bf/e(Ljava/lang/Object;)Ljava/util/Map;
aload 2
aload 3
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
areturn
.limit locals 4
.limit stack 3
.end method

.method public a()Ljava/util/Set;
aload 0
invokevirtual com/a/b/d/bf/m()Ljava/util/Map;
invokeinterface java/util/Map/keySet()Ljava/util/Set; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method public a(Lcom/a/b/d/adv;)V
aload 1
invokeinterface com/a/b/d/adv/e()Ljava/util/Set; 0
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
astore 1
L0:
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/a/b/d/adw
astore 2
aload 0
aload 2
invokeinterface com/a/b/d/adw/a()Ljava/lang/Object; 0
aload 2
invokeinterface com/a/b/d/adw/b()Ljava/lang/Object; 0
aload 2
invokeinterface com/a/b/d/adw/c()Ljava/lang/Object; 0
invokevirtual com/a/b/d/bf/a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
goto L0
L1:
return
.limit locals 3
.limit stack 4
.end method

.method public a(Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
invokevirtual com/a/b/d/bf/m()Ljava/util/Map;
aload 1
invokestatic com/a/b/d/sz/b(Ljava/util/Map;Ljava/lang/Object;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public a(Ljava/lang/Object;Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
aload 0
invokevirtual com/a/b/d/bf/m()Ljava/util/Map;
aload 1
invokestatic com/a/b/d/sz/a(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/util/Map
astore 1
aload 1
ifnull L0
aload 1
aload 2
invokestatic com/a/b/d/sz/b(Ljava/util/Map;Ljava/lang/Object;)Z
ifeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 3
.limit stack 2
.end method

.method public b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
aload 0
invokevirtual com/a/b/d/bf/m()Ljava/util/Map;
aload 1
invokestatic com/a/b/d/sz/a(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/util/Map
astore 1
aload 1
ifnonnull L0
aconst_null
areturn
L0:
aload 1
aload 2
invokestatic com/a/b/d/sz/a(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;
areturn
.limit locals 3
.limit stack 2
.end method

.method public b()Ljava/util/Set;
aload 0
invokevirtual com/a/b/d/bf/l()Ljava/util/Map;
invokeinterface java/util/Map/keySet()Ljava/util/Set; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method public b(Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
invokevirtual com/a/b/d/bf/l()Ljava/util/Map;
aload 1
invokestatic com/a/b/d/sz/b(Ljava/util/Map;Ljava/lang/Object;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
aload 0
invokevirtual com/a/b/d/bf/m()Ljava/util/Map;
aload 1
invokestatic com/a/b/d/sz/a(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/util/Map
astore 1
aload 1
ifnonnull L0
aconst_null
areturn
L0:
aload 1
aload 2
invokestatic com/a/b/d/sz/c(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;
areturn
.limit locals 3
.limit stack 2
.end method

.method public c()Z
aload 0
invokevirtual com/a/b/d/bf/k()I
ifne L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public c(Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
invokevirtual com/a/b/d/bf/m()Ljava/util/Map;
invokeinterface java/util/Map/values()Ljava/util/Collection; 0
invokeinterface java/util/Collection/iterator()Ljava/util/Iterator; 0
astore 2
L0:
aload 2
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 2
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/util/Map
aload 1
invokeinterface java/util/Map/containsValue(Ljava/lang/Object;)Z 1
ifeq L0
iconst_1
ireturn
L1:
iconst_0
ireturn
.limit locals 3
.limit stack 2
.end method

.method public d()V
aload 0
invokevirtual com/a/b/d/bf/e()Ljava/util/Set;
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
invokestatic com/a/b/d/nj/i(Ljava/util/Iterator;)V
return
.limit locals 1
.limit stack 1
.end method

.method public e()Ljava/util/Set;
aload 0
getfield com/a/b/d/bf/a Ljava/util/Set;
astore 2
aload 2
astore 1
aload 2
ifnonnull L0
aload 0
invokevirtual com/a/b/d/bf/f()Ljava/util/Set;
astore 1
aload 0
aload 1
putfield com/a/b/d/bf/a Ljava/util/Set;
L0:
aload 1
areturn
.limit locals 3
.limit stack 2
.end method

.method public equals(Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
aload 1
invokestatic com/a/b/d/adx/a(Lcom/a/b/d/adv;Ljava/lang/Object;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method f()Ljava/util/Set;
new com/a/b/d/bh
dup
aload 0
invokespecial com/a/b/d/bh/<init>(Lcom/a/b/d/bf;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method abstract g()Ljava/util/Iterator;
.end method

.method public h()Ljava/util/Collection;
aload 0
getfield com/a/b/d/bf/b Ljava/util/Collection;
astore 2
aload 2
astore 1
aload 2
ifnonnull L0
aload 0
invokevirtual com/a/b/d/bf/i()Ljava/util/Collection;
astore 1
aload 0
aload 1
putfield com/a/b/d/bf/b Ljava/util/Collection;
L0:
aload 1
areturn
.limit locals 3
.limit stack 2
.end method

.method public hashCode()I
aload 0
invokevirtual com/a/b/d/bf/e()Ljava/util/Set;
invokeinterface java/util/Set/hashCode()I 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method i()Ljava/util/Collection;
new com/a/b/d/bi
dup
aload 0
invokespecial com/a/b/d/bi/<init>(Lcom/a/b/d/bf;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method m_()Ljava/util/Iterator;
new com/a/b/d/bg
dup
aload 0
aload 0
invokevirtual com/a/b/d/bf/e()Ljava/util/Set;
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
invokespecial com/a/b/d/bg/<init>(Lcom/a/b/d/bf;Ljava/util/Iterator;)V
areturn
.limit locals 1
.limit stack 4
.end method

.method public toString()Ljava/lang/String;
aload 0
invokevirtual com/a/b/d/bf/m()Ljava/util/Map;
invokevirtual java/lang/Object/toString()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method
