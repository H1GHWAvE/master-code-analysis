.bytecode 50.0
.class final synchronized com/a/b/d/ae
.super com/a/b/d/ac
.implements java/util/ListIterator

.field final synthetic 'd' Lcom/a/b/d/ad;

.method <init>(Lcom/a/b/d/ad;)V
aload 0
aload 1
putfield com/a/b/d/ae/d Lcom/a/b/d/ad;
aload 0
aload 1
invokespecial com/a/b/d/ac/<init>(Lcom/a/b/d/ab;)V
return
.limit locals 2
.limit stack 2
.end method

.method public <init>(Lcom/a/b/d/ad;I)V
aload 0
aload 1
putfield com/a/b/d/ae/d Lcom/a/b/d/ad;
aload 0
aload 1
aload 1
getfield com/a/b/d/ab/c Ljava/util/Collection;
checkcast java/util/List
iload 2
invokeinterface java/util/List/listIterator(I)Ljava/util/ListIterator; 1
invokespecial com/a/b/d/ac/<init>(Lcom/a/b/d/ab;Ljava/util/Iterator;)V
return
.limit locals 3
.limit stack 4
.end method

.method private b()Ljava/util/ListIterator;
aload 0
invokevirtual com/a/b/d/ac/a()V
aload 0
getfield com/a/b/d/ac/a Ljava/util/Iterator;
checkcast java/util/ListIterator
areturn
.limit locals 1
.limit stack 1
.end method

.method public final add(Ljava/lang/Object;)V
aload 0
getfield com/a/b/d/ae/d Lcom/a/b/d/ad;
invokevirtual com/a/b/d/ad/isEmpty()Z
istore 2
aload 0
invokespecial com/a/b/d/ae/b()Ljava/util/ListIterator;
aload 1
invokeinterface java/util/ListIterator/add(Ljava/lang/Object;)V 1
aload 0
getfield com/a/b/d/ae/d Lcom/a/b/d/ad;
getfield com/a/b/d/ad/g Lcom/a/b/d/n;
invokestatic com/a/b/d/n/c(Lcom/a/b/d/n;)I
pop
iload 2
ifeq L0
aload 0
getfield com/a/b/d/ae/d Lcom/a/b/d/ad;
invokevirtual com/a/b/d/ad/c()V
L0:
return
.limit locals 3
.limit stack 2
.end method

.method public final hasPrevious()Z
aload 0
invokespecial com/a/b/d/ae/b()Ljava/util/ListIterator;
invokeinterface java/util/ListIterator/hasPrevious()Z 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final nextIndex()I
aload 0
invokespecial com/a/b/d/ae/b()Ljava/util/ListIterator;
invokeinterface java/util/ListIterator/nextIndex()I 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final previous()Ljava/lang/Object;
aload 0
invokespecial com/a/b/d/ae/b()Ljava/util/ListIterator;
invokeinterface java/util/ListIterator/previous()Ljava/lang/Object; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method public final previousIndex()I
aload 0
invokespecial com/a/b/d/ae/b()Ljava/util/ListIterator;
invokeinterface java/util/ListIterator/previousIndex()I 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final set(Ljava/lang/Object;)V
aload 0
invokespecial com/a/b/d/ae/b()Ljava/util/ListIterator;
aload 1
invokeinterface java/util/ListIterator/set(Ljava/lang/Object;)V 1
return
.limit locals 2
.limit stack 2
.end method
