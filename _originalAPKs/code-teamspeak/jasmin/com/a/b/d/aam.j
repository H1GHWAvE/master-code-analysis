.bytecode 50.0
.class synchronized com/a/b/d/aam
.super com/a/b/d/aal
.implements java/util/SortedSet

.method <init>(Ljava/util/SortedSet;Lcom/a/b/b/co;)V
aload 0
aload 1
aload 2
invokespecial com/a/b/d/aal/<init>(Ljava/util/Set;Lcom/a/b/b/co;)V
return
.limit locals 3
.limit stack 3
.end method

.method public comparator()Ljava/util/Comparator;
aload 0
getfield com/a/b/d/aam/a Ljava/util/Collection;
checkcast java/util/SortedSet
invokeinterface java/util/SortedSet/comparator()Ljava/util/Comparator; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method public first()Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/aam/iterator()Ljava/util/Iterator;
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method public headSet(Ljava/lang/Object;)Ljava/util/SortedSet;
new com/a/b/d/aam
dup
aload 0
getfield com/a/b/d/aam/a Ljava/util/Collection;
checkcast java/util/SortedSet
aload 1
invokeinterface java/util/SortedSet/headSet(Ljava/lang/Object;)Ljava/util/SortedSet; 1
aload 0
getfield com/a/b/d/aam/b Lcom/a/b/b/co;
invokespecial com/a/b/d/aam/<init>(Ljava/util/SortedSet;Lcom/a/b/b/co;)V
areturn
.limit locals 2
.limit stack 4
.end method

.method public last()Ljava/lang/Object;
aload 0
getfield com/a/b/d/aam/a Ljava/util/Collection;
checkcast java/util/SortedSet
astore 1
L0:
aload 1
invokeinterface java/util/SortedSet/last()Ljava/lang/Object; 0
astore 2
aload 0
getfield com/a/b/d/aam/b Lcom/a/b/b/co;
aload 2
invokeinterface com/a/b/b/co/a(Ljava/lang/Object;)Z 1
ifeq L1
aload 2
areturn
L1:
aload 1
aload 2
invokeinterface java/util/SortedSet/headSet(Ljava/lang/Object;)Ljava/util/SortedSet; 1
astore 1
goto L0
.limit locals 3
.limit stack 2
.end method

.method public subSet(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedSet;
new com/a/b/d/aam
dup
aload 0
getfield com/a/b/d/aam/a Ljava/util/Collection;
checkcast java/util/SortedSet
aload 1
aload 2
invokeinterface java/util/SortedSet/subSet(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedSet; 2
aload 0
getfield com/a/b/d/aam/b Lcom/a/b/b/co;
invokespecial com/a/b/d/aam/<init>(Ljava/util/SortedSet;Lcom/a/b/b/co;)V
areturn
.limit locals 3
.limit stack 5
.end method

.method public tailSet(Ljava/lang/Object;)Ljava/util/SortedSet;
new com/a/b/d/aam
dup
aload 0
getfield com/a/b/d/aam/a Ljava/util/Collection;
checkcast java/util/SortedSet
aload 1
invokeinterface java/util/SortedSet/tailSet(Ljava/lang/Object;)Ljava/util/SortedSet; 1
aload 0
getfield com/a/b/d/aam/b Lcom/a/b/b/co;
invokespecial com/a/b/d/aam/<init>(Ljava/util/SortedSet;Lcom/a/b/b/co;)V
areturn
.limit locals 2
.limit stack 4
.end method
