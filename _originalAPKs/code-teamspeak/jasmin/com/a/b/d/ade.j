.bytecode 50.0
.class final synchronized com/a/b/d/ade
.super com/a/b/d/ado
.implements java/util/Deque
.annotation invisible Lcom/a/b/a/c;
a s = "Deque"
.end annotation

.field private static final 'a' J = 0L


.method <init>(Ljava/util/Deque;)V
aload 0
aload 1
invokespecial com/a/b/d/ado/<init>(Ljava/util/Queue;)V
return
.limit locals 2
.limit stack 2
.end method

.method private c()Ljava/util/Deque;
aload 0
invokespecial com/a/b/d/ado/a()Ljava/util/Queue;
checkcast java/util/Deque
areturn
.limit locals 1
.limit stack 1
.end method

.method final volatile synthetic a()Ljava/util/Queue;
aload 0
invokespecial com/a/b/d/ado/a()Ljava/util/Queue;
checkcast java/util/Deque
areturn
.limit locals 1
.limit stack 1
.end method

.method public final addFirst(Ljava/lang/Object;)V
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/ade/h Ljava/lang/Object;
astore 2
aload 2
monitorenter
L0:
aload 0
invokespecial com/a/b/d/ado/a()Ljava/util/Queue;
checkcast java/util/Deque
aload 1
invokeinterface java/util/Deque/addFirst(Ljava/lang/Object;)V 1
aload 2
monitorexit
L1:
return
L2:
astore 1
L3:
aload 2
monitorexit
L4:
aload 1
athrow
.limit locals 3
.limit stack 2
.end method

.method public final addLast(Ljava/lang/Object;)V
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/ade/h Ljava/lang/Object;
astore 2
aload 2
monitorenter
L0:
aload 0
invokespecial com/a/b/d/ado/a()Ljava/util/Queue;
checkcast java/util/Deque
aload 1
invokeinterface java/util/Deque/addLast(Ljava/lang/Object;)V 1
aload 2
monitorexit
L1:
return
L2:
astore 1
L3:
aload 2
monitorexit
L4:
aload 1
athrow
.limit locals 3
.limit stack 2
.end method

.method final synthetic b()Ljava/util/Collection;
aload 0
invokespecial com/a/b/d/ado/a()Ljava/util/Queue;
checkcast java/util/Deque
areturn
.limit locals 1
.limit stack 1
.end method

.method final synthetic d()Ljava/lang/Object;
aload 0
invokespecial com/a/b/d/ado/a()Ljava/util/Queue;
checkcast java/util/Deque
areturn
.limit locals 1
.limit stack 1
.end method

.method public final descendingIterator()Ljava/util/Iterator;
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/ade/h Ljava/lang/Object;
astore 1
aload 1
monitorenter
L0:
aload 0
invokespecial com/a/b/d/ado/a()Ljava/util/Queue;
checkcast java/util/Deque
invokeinterface java/util/Deque/descendingIterator()Ljava/util/Iterator; 0
astore 2
aload 1
monitorexit
L1:
aload 2
areturn
L2:
astore 2
L3:
aload 1
monitorexit
L4:
aload 2
athrow
.limit locals 3
.limit stack 1
.end method

.method public final getFirst()Ljava/lang/Object;
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/ade/h Ljava/lang/Object;
astore 1
aload 1
monitorenter
L0:
aload 0
invokespecial com/a/b/d/ado/a()Ljava/util/Queue;
checkcast java/util/Deque
invokeinterface java/util/Deque/getFirst()Ljava/lang/Object; 0
astore 2
aload 1
monitorexit
L1:
aload 2
areturn
L2:
astore 2
L3:
aload 1
monitorexit
L4:
aload 2
athrow
.limit locals 3
.limit stack 1
.end method

.method public final getLast()Ljava/lang/Object;
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/ade/h Ljava/lang/Object;
astore 1
aload 1
monitorenter
L0:
aload 0
invokespecial com/a/b/d/ado/a()Ljava/util/Queue;
checkcast java/util/Deque
invokeinterface java/util/Deque/getLast()Ljava/lang/Object; 0
astore 2
aload 1
monitorexit
L1:
aload 2
areturn
L2:
astore 2
L3:
aload 1
monitorexit
L4:
aload 2
athrow
.limit locals 3
.limit stack 1
.end method

.method public final offerFirst(Ljava/lang/Object;)Z
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/ade/h Ljava/lang/Object;
astore 3
aload 3
monitorenter
L0:
aload 0
invokespecial com/a/b/d/ado/a()Ljava/util/Queue;
checkcast java/util/Deque
aload 1
invokeinterface java/util/Deque/offerFirst(Ljava/lang/Object;)Z 1
istore 2
aload 3
monitorexit
L1:
iload 2
ireturn
L2:
astore 1
L3:
aload 3
monitorexit
L4:
aload 1
athrow
.limit locals 4
.limit stack 2
.end method

.method public final offerLast(Ljava/lang/Object;)Z
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/ade/h Ljava/lang/Object;
astore 3
aload 3
monitorenter
L0:
aload 0
invokespecial com/a/b/d/ado/a()Ljava/util/Queue;
checkcast java/util/Deque
aload 1
invokeinterface java/util/Deque/offerLast(Ljava/lang/Object;)Z 1
istore 2
aload 3
monitorexit
L1:
iload 2
ireturn
L2:
astore 1
L3:
aload 3
monitorexit
L4:
aload 1
athrow
.limit locals 4
.limit stack 2
.end method

.method public final peekFirst()Ljava/lang/Object;
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/ade/h Ljava/lang/Object;
astore 1
aload 1
monitorenter
L0:
aload 0
invokespecial com/a/b/d/ado/a()Ljava/util/Queue;
checkcast java/util/Deque
invokeinterface java/util/Deque/peekFirst()Ljava/lang/Object; 0
astore 2
aload 1
monitorexit
L1:
aload 2
areturn
L2:
astore 2
L3:
aload 1
monitorexit
L4:
aload 2
athrow
.limit locals 3
.limit stack 1
.end method

.method public final peekLast()Ljava/lang/Object;
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/ade/h Ljava/lang/Object;
astore 1
aload 1
monitorenter
L0:
aload 0
invokespecial com/a/b/d/ado/a()Ljava/util/Queue;
checkcast java/util/Deque
invokeinterface java/util/Deque/peekLast()Ljava/lang/Object; 0
astore 2
aload 1
monitorexit
L1:
aload 2
areturn
L2:
astore 2
L3:
aload 1
monitorexit
L4:
aload 2
athrow
.limit locals 3
.limit stack 1
.end method

.method public final pollFirst()Ljava/lang/Object;
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/ade/h Ljava/lang/Object;
astore 1
aload 1
monitorenter
L0:
aload 0
invokespecial com/a/b/d/ado/a()Ljava/util/Queue;
checkcast java/util/Deque
invokeinterface java/util/Deque/pollFirst()Ljava/lang/Object; 0
astore 2
aload 1
monitorexit
L1:
aload 2
areturn
L2:
astore 2
L3:
aload 1
monitorexit
L4:
aload 2
athrow
.limit locals 3
.limit stack 1
.end method

.method public final pollLast()Ljava/lang/Object;
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/ade/h Ljava/lang/Object;
astore 1
aload 1
monitorenter
L0:
aload 0
invokespecial com/a/b/d/ado/a()Ljava/util/Queue;
checkcast java/util/Deque
invokeinterface java/util/Deque/pollLast()Ljava/lang/Object; 0
astore 2
aload 1
monitorexit
L1:
aload 2
areturn
L2:
astore 2
L3:
aload 1
monitorexit
L4:
aload 2
athrow
.limit locals 3
.limit stack 1
.end method

.method public final pop()Ljava/lang/Object;
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/ade/h Ljava/lang/Object;
astore 1
aload 1
monitorenter
L0:
aload 0
invokespecial com/a/b/d/ado/a()Ljava/util/Queue;
checkcast java/util/Deque
invokeinterface java/util/Deque/pop()Ljava/lang/Object; 0
astore 2
aload 1
monitorexit
L1:
aload 2
areturn
L2:
astore 2
L3:
aload 1
monitorexit
L4:
aload 2
athrow
.limit locals 3
.limit stack 1
.end method

.method public final push(Ljava/lang/Object;)V
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/ade/h Ljava/lang/Object;
astore 2
aload 2
monitorenter
L0:
aload 0
invokespecial com/a/b/d/ado/a()Ljava/util/Queue;
checkcast java/util/Deque
aload 1
invokeinterface java/util/Deque/push(Ljava/lang/Object;)V 1
aload 2
monitorexit
L1:
return
L2:
astore 1
L3:
aload 2
monitorexit
L4:
aload 1
athrow
.limit locals 3
.limit stack 2
.end method

.method public final removeFirst()Ljava/lang/Object;
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/ade/h Ljava/lang/Object;
astore 1
aload 1
monitorenter
L0:
aload 0
invokespecial com/a/b/d/ado/a()Ljava/util/Queue;
checkcast java/util/Deque
invokeinterface java/util/Deque/removeFirst()Ljava/lang/Object; 0
astore 2
aload 1
monitorexit
L1:
aload 2
areturn
L2:
astore 2
L3:
aload 1
monitorexit
L4:
aload 2
athrow
.limit locals 3
.limit stack 1
.end method

.method public final removeFirstOccurrence(Ljava/lang/Object;)Z
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/ade/h Ljava/lang/Object;
astore 3
aload 3
monitorenter
L0:
aload 0
invokespecial com/a/b/d/ado/a()Ljava/util/Queue;
checkcast java/util/Deque
aload 1
invokeinterface java/util/Deque/removeFirstOccurrence(Ljava/lang/Object;)Z 1
istore 2
aload 3
monitorexit
L1:
iload 2
ireturn
L2:
astore 1
L3:
aload 3
monitorexit
L4:
aload 1
athrow
.limit locals 4
.limit stack 2
.end method

.method public final removeLast()Ljava/lang/Object;
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/ade/h Ljava/lang/Object;
astore 1
aload 1
monitorenter
L0:
aload 0
invokespecial com/a/b/d/ado/a()Ljava/util/Queue;
checkcast java/util/Deque
invokeinterface java/util/Deque/removeLast()Ljava/lang/Object; 0
astore 2
aload 1
monitorexit
L1:
aload 2
areturn
L2:
astore 2
L3:
aload 1
monitorexit
L4:
aload 2
athrow
.limit locals 3
.limit stack 1
.end method

.method public final removeLastOccurrence(Ljava/lang/Object;)Z
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/ade/h Ljava/lang/Object;
astore 3
aload 3
monitorenter
L0:
aload 0
invokespecial com/a/b/d/ado/a()Ljava/util/Queue;
checkcast java/util/Deque
aload 1
invokeinterface java/util/Deque/removeLastOccurrence(Ljava/lang/Object;)Z 1
istore 2
aload 3
monitorexit
L1:
iload 2
ireturn
L2:
astore 1
L3:
aload 3
monitorexit
L4:
aload 1
athrow
.limit locals 4
.limit stack 2
.end method
