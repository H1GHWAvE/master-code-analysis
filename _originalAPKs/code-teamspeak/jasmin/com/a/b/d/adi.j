.bytecode 50.0
.class synchronized com/a/b/d/adi
.super com/a/b/d/adn
.implements java/util/Map

.field private static final 'a' J = 0L


.field transient 'c' Ljava/util/Set;

.field transient 'd' Ljava/util/Collection;

.field transient 'e' Ljava/util/Set;

.method <init>(Ljava/util/Map;Ljava/lang/Object;)V
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
aload 0
aload 1
aload 2
invokespecial com/a/b/d/adn/<init>(Ljava/lang/Object;Ljava/lang/Object;)V
return
.limit locals 3
.limit stack 3
.end method

.method a()Ljava/util/Map;
aload 0
invokespecial com/a/b/d/adn/d()Ljava/lang/Object;
checkcast java/util/Map
areturn
.limit locals 1
.limit stack 1
.end method

.method public clear()V
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/adi/h Ljava/lang/Object;
astore 1
aload 1
monitorenter
L0:
aload 0
invokevirtual com/a/b/d/adi/a()Ljava/util/Map;
invokeinterface java/util/Map/clear()V 0
aload 1
monitorexit
L1:
return
L2:
astore 2
L3:
aload 1
monitorexit
L4:
aload 2
athrow
.limit locals 3
.limit stack 1
.end method

.method public containsKey(Ljava/lang/Object;)Z
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/adi/h Ljava/lang/Object;
astore 3
aload 3
monitorenter
L0:
aload 0
invokevirtual com/a/b/d/adi/a()Ljava/util/Map;
aload 1
invokeinterface java/util/Map/containsKey(Ljava/lang/Object;)Z 1
istore 2
aload 3
monitorexit
L1:
iload 2
ireturn
L2:
astore 1
L3:
aload 3
monitorexit
L4:
aload 1
athrow
.limit locals 4
.limit stack 2
.end method

.method public containsValue(Ljava/lang/Object;)Z
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/adi/h Ljava/lang/Object;
astore 3
aload 3
monitorenter
L0:
aload 0
invokevirtual com/a/b/d/adi/a()Ljava/util/Map;
aload 1
invokeinterface java/util/Map/containsValue(Ljava/lang/Object;)Z 1
istore 2
aload 3
monitorexit
L1:
iload 2
ireturn
L2:
astore 1
L3:
aload 3
monitorexit
L4:
aload 1
athrow
.limit locals 4
.limit stack 2
.end method

.method synthetic d()Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/adi/a()Ljava/util/Map;
areturn
.limit locals 1
.limit stack 1
.end method

.method public entrySet()Ljava/util/Set;
.catch all from L0 to L1 using L2
.catch all from L1 to L3 using L2
.catch all from L4 to L5 using L2
aload 0
getfield com/a/b/d/adi/h Ljava/lang/Object;
astore 1
aload 1
monitorenter
L0:
aload 0
getfield com/a/b/d/adi/e Ljava/util/Set;
ifnonnull L1
aload 0
aload 0
invokevirtual com/a/b/d/adi/a()Ljava/util/Map;
invokeinterface java/util/Map/entrySet()Ljava/util/Set; 0
aload 0
getfield com/a/b/d/adi/h Ljava/lang/Object;
invokestatic com/a/b/d/acu/a(Ljava/util/Set;Ljava/lang/Object;)Ljava/util/Set;
putfield com/a/b/d/adi/e Ljava/util/Set;
L1:
aload 0
getfield com/a/b/d/adi/e Ljava/util/Set;
astore 2
aload 1
monitorexit
L3:
aload 2
areturn
L2:
astore 2
L4:
aload 1
monitorexit
L5:
aload 2
athrow
.limit locals 3
.limit stack 3
.end method

.method public equals(Ljava/lang/Object;)Z
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 1
aload 0
if_acmpne L5
iconst_1
ireturn
L5:
aload 0
getfield com/a/b/d/adi/h Ljava/lang/Object;
astore 3
aload 3
monitorenter
L0:
aload 0
invokevirtual com/a/b/d/adi/a()Ljava/util/Map;
aload 1
invokeinterface java/util/Map/equals(Ljava/lang/Object;)Z 1
istore 2
aload 3
monitorexit
L1:
iload 2
ireturn
L2:
astore 1
L3:
aload 3
monitorexit
L4:
aload 1
athrow
.limit locals 4
.limit stack 2
.end method

.method public get(Ljava/lang/Object;)Ljava/lang/Object;
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/adi/h Ljava/lang/Object;
astore 2
aload 2
monitorenter
L0:
aload 0
invokevirtual com/a/b/d/adi/a()Ljava/util/Map;
aload 1
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
astore 1
aload 2
monitorexit
L1:
aload 1
areturn
L2:
astore 1
L3:
aload 2
monitorexit
L4:
aload 1
athrow
.limit locals 3
.limit stack 2
.end method

.method public hashCode()I
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/adi/h Ljava/lang/Object;
astore 2
aload 2
monitorenter
L0:
aload 0
invokevirtual com/a/b/d/adi/a()Ljava/util/Map;
invokeinterface java/util/Map/hashCode()I 0
istore 1
aload 2
monitorexit
L1:
iload 1
ireturn
L2:
astore 3
L3:
aload 2
monitorexit
L4:
aload 3
athrow
.limit locals 4
.limit stack 1
.end method

.method public isEmpty()Z
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/adi/h Ljava/lang/Object;
astore 2
aload 2
monitorenter
L0:
aload 0
invokevirtual com/a/b/d/adi/a()Ljava/util/Map;
invokeinterface java/util/Map/isEmpty()Z 0
istore 1
aload 2
monitorexit
L1:
iload 1
ireturn
L2:
astore 3
L3:
aload 2
monitorexit
L4:
aload 3
athrow
.limit locals 4
.limit stack 1
.end method

.method public keySet()Ljava/util/Set;
.catch all from L0 to L1 using L2
.catch all from L1 to L3 using L2
.catch all from L4 to L5 using L2
aload 0
getfield com/a/b/d/adi/h Ljava/lang/Object;
astore 1
aload 1
monitorenter
L0:
aload 0
getfield com/a/b/d/adi/c Ljava/util/Set;
ifnonnull L1
aload 0
aload 0
invokevirtual com/a/b/d/adi/a()Ljava/util/Map;
invokeinterface java/util/Map/keySet()Ljava/util/Set; 0
aload 0
getfield com/a/b/d/adi/h Ljava/lang/Object;
invokestatic com/a/b/d/acu/a(Ljava/util/Set;Ljava/lang/Object;)Ljava/util/Set;
putfield com/a/b/d/adi/c Ljava/util/Set;
L1:
aload 0
getfield com/a/b/d/adi/c Ljava/util/Set;
astore 2
aload 1
monitorexit
L3:
aload 2
areturn
L2:
astore 2
L4:
aload 1
monitorexit
L5:
aload 2
athrow
.limit locals 3
.limit stack 3
.end method

.method public put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/adi/h Ljava/lang/Object;
astore 3
aload 3
monitorenter
L0:
aload 0
invokevirtual com/a/b/d/adi/a()Ljava/util/Map;
aload 1
aload 2
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
astore 1
aload 3
monitorexit
L1:
aload 1
areturn
L2:
astore 1
L3:
aload 3
monitorexit
L4:
aload 1
athrow
.limit locals 4
.limit stack 3
.end method

.method public putAll(Ljava/util/Map;)V
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/adi/h Ljava/lang/Object;
astore 2
aload 2
monitorenter
L0:
aload 0
invokevirtual com/a/b/d/adi/a()Ljava/util/Map;
aload 1
invokeinterface java/util/Map/putAll(Ljava/util/Map;)V 1
aload 2
monitorexit
L1:
return
L2:
astore 1
L3:
aload 2
monitorexit
L4:
aload 1
athrow
.limit locals 3
.limit stack 2
.end method

.method public remove(Ljava/lang/Object;)Ljava/lang/Object;
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/adi/h Ljava/lang/Object;
astore 2
aload 2
monitorenter
L0:
aload 0
invokevirtual com/a/b/d/adi/a()Ljava/util/Map;
aload 1
invokeinterface java/util/Map/remove(Ljava/lang/Object;)Ljava/lang/Object; 1
astore 1
aload 2
monitorexit
L1:
aload 1
areturn
L2:
astore 1
L3:
aload 2
monitorexit
L4:
aload 1
athrow
.limit locals 3
.limit stack 2
.end method

.method public size()I
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/adi/h Ljava/lang/Object;
astore 2
aload 2
monitorenter
L0:
aload 0
invokevirtual com/a/b/d/adi/a()Ljava/util/Map;
invokeinterface java/util/Map/size()I 0
istore 1
aload 2
monitorexit
L1:
iload 1
ireturn
L2:
astore 3
L3:
aload 2
monitorexit
L4:
aload 3
athrow
.limit locals 4
.limit stack 1
.end method

.method public values()Ljava/util/Collection;
.catch all from L0 to L1 using L2
.catch all from L1 to L3 using L2
.catch all from L4 to L5 using L2
aload 0
getfield com/a/b/d/adi/h Ljava/lang/Object;
astore 1
aload 1
monitorenter
L0:
aload 0
getfield com/a/b/d/adi/d Ljava/util/Collection;
ifnonnull L1
aload 0
aload 0
invokevirtual com/a/b/d/adi/a()Ljava/util/Map;
invokeinterface java/util/Map/values()Ljava/util/Collection; 0
aload 0
getfield com/a/b/d/adi/h Ljava/lang/Object;
invokestatic com/a/b/d/acu/a(Ljava/util/Collection;Ljava/lang/Object;)Ljava/util/Collection;
putfield com/a/b/d/adi/d Ljava/util/Collection;
L1:
aload 0
getfield com/a/b/d/adi/d Ljava/util/Collection;
astore 2
aload 1
monitorexit
L3:
aload 2
areturn
L2:
astore 2
L4:
aload 1
monitorexit
L5:
aload 2
athrow
.limit locals 3
.limit stack 3
.end method
