.bytecode 50.0
.class final synchronized com/a/b/d/af
.super com/a/b/d/ah
.implements java/util/NavigableSet
.annotation invisible Lcom/a/b/a/c;
a s = "NavigableSet"
.end annotation

.field final synthetic 'a' Lcom/a/b/d/n;

.method <init>(Lcom/a/b/d/n;Ljava/lang/Object;Ljava/util/NavigableSet;Lcom/a/b/d/ab;)V
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
.annotation visibleparam 4 Ljavax/annotation/Nullable;
.end annotation
aload 0
aload 1
putfield com/a/b/d/af/a Lcom/a/b/d/n;
aload 0
aload 1
aload 2
aload 3
aload 4
invokespecial com/a/b/d/ah/<init>(Lcom/a/b/d/n;Ljava/lang/Object;Ljava/util/SortedSet;Lcom/a/b/d/ab;)V
return
.limit locals 5
.limit stack 5
.end method

.method private a(Ljava/util/NavigableSet;)Ljava/util/NavigableSet;
aload 0
getfield com/a/b/d/af/a Lcom/a/b/d/n;
astore 3
aload 0
getfield com/a/b/d/af/b Ljava/lang/Object;
astore 4
aload 0
getfield com/a/b/d/ab/d Lcom/a/b/d/ab;
ifnonnull L0
aload 0
astore 2
L1:
new com/a/b/d/af
dup
aload 3
aload 4
aload 1
aload 2
invokespecial com/a/b/d/af/<init>(Lcom/a/b/d/n;Ljava/lang/Object;Ljava/util/NavigableSet;Lcom/a/b/d/ab;)V
areturn
L0:
aload 0
getfield com/a/b/d/ab/d Lcom/a/b/d/ab;
astore 2
goto L1
.limit locals 5
.limit stack 6
.end method

.method private e()Ljava/util/NavigableSet;
aload 0
invokespecial com/a/b/d/ah/d()Ljava/util/SortedSet;
checkcast java/util/NavigableSet
areturn
.limit locals 1
.limit stack 1
.end method

.method public final ceiling(Ljava/lang/Object;)Ljava/lang/Object;
aload 0
invokespecial com/a/b/d/ah/d()Ljava/util/SortedSet;
checkcast java/util/NavigableSet
aload 1
invokeinterface java/util/NavigableSet/ceiling(Ljava/lang/Object;)Ljava/lang/Object; 1
areturn
.limit locals 2
.limit stack 2
.end method

.method final volatile synthetic d()Ljava/util/SortedSet;
aload 0
invokespecial com/a/b/d/ah/d()Ljava/util/SortedSet;
checkcast java/util/NavigableSet
areturn
.limit locals 1
.limit stack 1
.end method

.method public final descendingIterator()Ljava/util/Iterator;
new com/a/b/d/ac
dup
aload 0
aload 0
invokespecial com/a/b/d/ah/d()Ljava/util/SortedSet;
checkcast java/util/NavigableSet
invokeinterface java/util/NavigableSet/descendingIterator()Ljava/util/Iterator; 0
invokespecial com/a/b/d/ac/<init>(Lcom/a/b/d/ab;Ljava/util/Iterator;)V
areturn
.limit locals 1
.limit stack 4
.end method

.method public final descendingSet()Ljava/util/NavigableSet;
aload 0
aload 0
invokespecial com/a/b/d/ah/d()Ljava/util/SortedSet;
checkcast java/util/NavigableSet
invokeinterface java/util/NavigableSet/descendingSet()Ljava/util/NavigableSet; 0
invokespecial com/a/b/d/af/a(Ljava/util/NavigableSet;)Ljava/util/NavigableSet;
areturn
.limit locals 1
.limit stack 2
.end method

.method public final floor(Ljava/lang/Object;)Ljava/lang/Object;
aload 0
invokespecial com/a/b/d/ah/d()Ljava/util/SortedSet;
checkcast java/util/NavigableSet
aload 1
invokeinterface java/util/NavigableSet/floor(Ljava/lang/Object;)Ljava/lang/Object; 1
areturn
.limit locals 2
.limit stack 2
.end method

.method public final headSet(Ljava/lang/Object;Z)Ljava/util/NavigableSet;
aload 0
aload 0
invokespecial com/a/b/d/ah/d()Ljava/util/SortedSet;
checkcast java/util/NavigableSet
aload 1
iload 2
invokeinterface java/util/NavigableSet/headSet(Ljava/lang/Object;Z)Ljava/util/NavigableSet; 2
invokespecial com/a/b/d/af/a(Ljava/util/NavigableSet;)Ljava/util/NavigableSet;
areturn
.limit locals 3
.limit stack 4
.end method

.method public final higher(Ljava/lang/Object;)Ljava/lang/Object;
aload 0
invokespecial com/a/b/d/ah/d()Ljava/util/SortedSet;
checkcast java/util/NavigableSet
aload 1
invokeinterface java/util/NavigableSet/higher(Ljava/lang/Object;)Ljava/lang/Object; 1
areturn
.limit locals 2
.limit stack 2
.end method

.method public final lower(Ljava/lang/Object;)Ljava/lang/Object;
aload 0
invokespecial com/a/b/d/ah/d()Ljava/util/SortedSet;
checkcast java/util/NavigableSet
aload 1
invokeinterface java/util/NavigableSet/lower(Ljava/lang/Object;)Ljava/lang/Object; 1
areturn
.limit locals 2
.limit stack 2
.end method

.method public final pollFirst()Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/af/iterator()Ljava/util/Iterator;
invokestatic com/a/b/d/nj/h(Ljava/util/Iterator;)Ljava/lang/Object;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final pollLast()Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/af/descendingIterator()Ljava/util/Iterator;
invokestatic com/a/b/d/nj/h(Ljava/util/Iterator;)Ljava/lang/Object;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final subSet(Ljava/lang/Object;ZLjava/lang/Object;Z)Ljava/util/NavigableSet;
aload 0
aload 0
invokespecial com/a/b/d/ah/d()Ljava/util/SortedSet;
checkcast java/util/NavigableSet
aload 1
iload 2
aload 3
iload 4
invokeinterface java/util/NavigableSet/subSet(Ljava/lang/Object;ZLjava/lang/Object;Z)Ljava/util/NavigableSet; 4
invokespecial com/a/b/d/af/a(Ljava/util/NavigableSet;)Ljava/util/NavigableSet;
areturn
.limit locals 5
.limit stack 6
.end method

.method public final tailSet(Ljava/lang/Object;Z)Ljava/util/NavigableSet;
aload 0
aload 0
invokespecial com/a/b/d/ah/d()Ljava/util/SortedSet;
checkcast java/util/NavigableSet
aload 1
iload 2
invokeinterface java/util/NavigableSet/tailSet(Ljava/lang/Object;Z)Ljava/util/NavigableSet; 2
invokespecial com/a/b/d/af/a(Ljava/util/NavigableSet;)Ljava/util/NavigableSet;
areturn
.limit locals 3
.limit stack 4
.end method
