.bytecode 50.0
.class final synchronized com/a/b/d/fh
.super com/a/b/d/yd
.implements java/io/Serializable
.annotation invisible Lcom/a/b/a/b;
a Z = 1
.end annotation

.field private static final 'b' J = 0L


.field final 'a' Lcom/a/b/d/jt;

.method private <init>(Lcom/a/b/d/jt;)V
aload 0
invokespecial com/a/b/d/yd/<init>()V
aload 0
aload 1
putfield com/a/b/d/fh/a Lcom/a/b/d/jt;
return
.limit locals 2
.limit stack 2
.end method

.method <init>(Ljava/util/List;)V
aload 0
aload 1
invokestatic com/a/b/d/fh/a(Ljava/util/List;)Lcom/a/b/d/jt;
invokespecial com/a/b/d/fh/<init>(Lcom/a/b/d/jt;)V
return
.limit locals 2
.limit stack 2
.end method

.method private a(Ljava/lang/Object;)I
aload 0
getfield com/a/b/d/fh/a Lcom/a/b/d/jt;
aload 1
invokevirtual com/a/b/d/jt/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/Integer
astore 2
aload 2
ifnonnull L0
new com/a/b/d/yh
dup
aload 1
invokespecial com/a/b/d/yh/<init>(Ljava/lang/Object;)V
athrow
L0:
aload 2
invokevirtual java/lang/Integer/intValue()I
ireturn
.limit locals 3
.limit stack 3
.end method

.method private static a(Ljava/util/List;)Lcom/a/b/d/jt;
invokestatic com/a/b/d/jt/l()Lcom/a/b/d/ju;
astore 2
iconst_0
istore 1
aload 0
invokeinterface java/util/List/iterator()Ljava/util/Iterator; 0
astore 0
L0:
aload 0
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 2
aload 0
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
iload 1
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual com/a/b/d/ju/a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/ju;
pop
iload 1
iconst_1
iadd
istore 1
goto L0
L1:
aload 2
invokevirtual com/a/b/d/ju/a()Lcom/a/b/d/jt;
areturn
.limit locals 3
.limit stack 3
.end method

.method public final compare(Ljava/lang/Object;Ljava/lang/Object;)I
aload 0
aload 1
invokespecial com/a/b/d/fh/a(Ljava/lang/Object;)I
aload 0
aload 2
invokespecial com/a/b/d/fh/a(Ljava/lang/Object;)I
isub
ireturn
.limit locals 3
.limit stack 3
.end method

.method public final equals(Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 1
instanceof com/a/b/d/fh
ifeq L0
aload 1
checkcast com/a/b/d/fh
astore 1
aload 0
getfield com/a/b/d/fh/a Lcom/a/b/d/jt;
aload 1
getfield com/a/b/d/fh/a Lcom/a/b/d/jt;
invokevirtual com/a/b/d/jt/equals(Ljava/lang/Object;)Z
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final hashCode()I
aload 0
getfield com/a/b/d/fh/a Lcom/a/b/d/jt;
invokevirtual com/a/b/d/jt/hashCode()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final toString()Ljava/lang/String;
aload 0
getfield com/a/b/d/fh/a Lcom/a/b/d/jt;
invokevirtual com/a/b/d/jt/g()Lcom/a/b/d/lo;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 1
new java/lang/StringBuilder
dup
aload 1
invokevirtual java/lang/String/length()I
bipush 19
iadd
invokespecial java/lang/StringBuilder/<init>(I)V
ldc "Ordering.explicit("
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ")"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 2
.limit stack 4
.end method
