.bytecode 50.0
.class public final synchronized com/a/b/d/aad
.super java/lang/Object
.annotation invisible Lcom/a/b/a/b;
b Z = 1
.end annotation

.method private <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method static a(Ljava/util/Set;)I
aload 0
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
astore 0
iconst_0
istore 1
L0:
aload 0
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 0
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
astore 3
aload 3
ifnull L2
aload 3
invokevirtual java/lang/Object/hashCode()I
istore 2
L3:
iload 1
iload 2
iadd
iconst_m1
ixor
iconst_m1
ixor
istore 1
goto L0
L2:
iconst_0
istore 2
goto L3
L1:
iload 1
ireturn
.limit locals 4
.limit stack 2
.end method

.method public static a(Ljava/util/Set;Ljava/util/Set;)Lcom/a/b/d/aaq;
aload 0
ldc "set1"
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 1
ldc "set2"
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
new com/a/b/d/aae
dup
aload 0
aload 1
aload 0
invokestatic com/a/b/d/aad/c(Ljava/util/Set;Ljava/util/Set;)Lcom/a/b/d/aaq;
aload 1
invokespecial com/a/b/d/aae/<init>(Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;)V
areturn
.limit locals 2
.limit stack 5
.end method

.method private static transient a(Ljava/lang/Enum;[Ljava/lang/Enum;)Lcom/a/b/d/lo;
.annotation invisible Lcom/a/b/a/b;
a Z = 1
.end annotation
aload 0
aload 1
invokestatic java/util/EnumSet/of(Ljava/lang/Enum;[Ljava/lang/Enum;)Ljava/util/EnumSet;
invokestatic com/a/b/d/ji/a(Ljava/util/EnumSet;)Lcom/a/b/d/lo;
areturn
.limit locals 2
.limit stack 2
.end method

.method private static a(Ljava/lang/Iterable;)Lcom/a/b/d/lo;
.annotation invisible Lcom/a/b/a/b;
a Z = 1
.end annotation
aload 0
instanceof com/a/b/d/ji
ifeq L0
aload 0
checkcast com/a/b/d/ji
areturn
L0:
aload 0
instanceof java/util/Collection
ifeq L1
aload 0
checkcast java/util/Collection
astore 0
aload 0
invokeinterface java/util/Collection/isEmpty()Z 0
ifeq L2
invokestatic com/a/b/d/lo/h()Lcom/a/b/d/lo;
areturn
L2:
aload 0
invokestatic java/util/EnumSet/copyOf(Ljava/util/Collection;)Ljava/util/EnumSet;
invokestatic com/a/b/d/ji/a(Ljava/util/EnumSet;)Lcom/a/b/d/lo;
areturn
L1:
aload 0
invokeinterface java/lang/Iterable/iterator()Ljava/util/Iterator; 0
astore 0
aload 0
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L3
aload 0
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/lang/Enum
invokestatic java/util/EnumSet/of(Ljava/lang/Enum;)Ljava/util/EnumSet;
astore 1
aload 1
aload 0
invokestatic com/a/b/d/nj/a(Ljava/util/Collection;Ljava/util/Iterator;)Z
pop
aload 1
invokestatic com/a/b/d/ji/a(Ljava/util/EnumSet;)Lcom/a/b/d/lo;
areturn
L3:
invokestatic com/a/b/d/lo/h()Lcom/a/b/d/lo;
areturn
.limit locals 2
.limit stack 2
.end method

.method private static a(Ljava/lang/Iterable;Ljava/lang/Class;)Ljava/util/EnumSet;
aload 1
invokestatic java/util/EnumSet/noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;
astore 1
aload 1
aload 0
invokestatic com/a/b/d/mq/a(Ljava/util/Collection;Ljava/lang/Iterable;)Z
pop
aload 1
areturn
.limit locals 2
.limit stack 2
.end method

.method private static a(Ljava/util/Collection;)Ljava/util/EnumSet;
aload 0
instanceof java/util/EnumSet
ifeq L0
aload 0
checkcast java/util/EnumSet
invokestatic java/util/EnumSet/complementOf(Ljava/util/EnumSet;)Ljava/util/EnumSet;
areturn
L0:
aload 0
invokeinterface java/util/Collection/isEmpty()Z 0
ifne L1
iconst_1
istore 1
L2:
iload 1
ldc "collection is empty; use the other version of this method"
invokestatic com/a/b/b/cn/a(ZLjava/lang/Object;)V
aload 0
aload 0
invokeinterface java/util/Collection/iterator()Ljava/util/Iterator; 0
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/lang/Enum
invokevirtual java/lang/Enum/getDeclaringClass()Ljava/lang/Class;
invokestatic com/a/b/d/aad/b(Ljava/util/Collection;Ljava/lang/Class;)Ljava/util/EnumSet;
areturn
L1:
iconst_0
istore 1
goto L2
.limit locals 2
.limit stack 2
.end method

.method private static a(Ljava/util/Collection;Ljava/lang/Class;)Ljava/util/EnumSet;
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
instanceof java/util/EnumSet
ifeq L0
aload 0
checkcast java/util/EnumSet
invokestatic java/util/EnumSet/complementOf(Ljava/util/EnumSet;)Ljava/util/EnumSet;
areturn
L0:
aload 0
aload 1
invokestatic com/a/b/d/aad/b(Ljava/util/Collection;Ljava/lang/Class;)Ljava/util/EnumSet;
areturn
.limit locals 2
.limit stack 2
.end method

.method private static a()Ljava/util/HashSet;
new java/util/HashSet
dup
invokespecial java/util/HashSet/<init>()V
areturn
.limit locals 0
.limit stack 2
.end method

.method public static a(I)Ljava/util/HashSet;
new java/util/HashSet
dup
iload 0
invokestatic com/a/b/d/sz/b(I)I
invokespecial java/util/HashSet/<init>(I)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private static a(Ljava/util/Iterator;)Ljava/util/HashSet;
new java/util/HashSet
dup
invokespecial java/util/HashSet/<init>()V
astore 1
aload 1
aload 0
invokestatic com/a/b/d/nj/a(Ljava/util/Collection;Ljava/util/Iterator;)Z
pop
aload 1
areturn
.limit locals 2
.limit stack 2
.end method

.method private static transient a([Ljava/lang/Object;)Ljava/util/HashSet;
aload 0
arraylength
invokestatic com/a/b/d/aad/a(I)Ljava/util/HashSet;
astore 1
aload 1
aload 0
invokestatic java/util/Collections/addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z
pop
aload 1
areturn
.limit locals 2
.limit stack 2
.end method

.method public static a(Ljava/util/NavigableSet;)Ljava/util/NavigableSet;
.annotation invisible Lcom/a/b/a/c;
a s = "NavigableSet"
.end annotation
aload 0
instanceof com/a/b/d/me
ifne L0
aload 0
instanceof com/a/b/d/aat
ifeq L1
L0:
aload 0
areturn
L1:
new com/a/b/d/aat
dup
aload 0
invokespecial com/a/b/d/aat/<init>(Ljava/util/NavigableSet;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method public static a(Ljava/util/NavigableSet;Lcom/a/b/b/co;)Ljava/util/NavigableSet;
.annotation invisible Lcom/a/b/a/c;
a s = "NavigableSet"
.end annotation
aload 0
instanceof com/a/b/d/aal
ifeq L0
aload 0
checkcast com/a/b/d/aal
astore 0
aload 0
getfield com/a/b/d/aal/b Lcom/a/b/b/co;
aload 1
invokestatic com/a/b/b/cp/a(Lcom/a/b/b/co;Lcom/a/b/b/co;)Lcom/a/b/b/co;
astore 1
new com/a/b/d/aak
dup
aload 0
getfield com/a/b/d/aal/a Ljava/util/Collection;
checkcast java/util/NavigableSet
aload 1
invokespecial com/a/b/d/aak/<init>(Ljava/util/NavigableSet;Lcom/a/b/b/co;)V
areturn
L0:
new com/a/b/d/aak
dup
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/util/NavigableSet
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast com/a/b/b/co
invokespecial com/a/b/d/aak/<init>(Ljava/util/NavigableSet;Lcom/a/b/b/co;)V
areturn
.limit locals 2
.limit stack 4
.end method

.method private static a(Ljava/util/List;)Ljava/util/Set;
aload 0
invokestatic com/a/b/d/aah/a(Ljava/util/List;)Ljava/util/Set;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static a(Ljava/util/Map;)Ljava/util/Set;
aload 0
invokestatic java/util/Collections/newSetFromMap(Ljava/util/Map;)Ljava/util/Set;
areturn
.limit locals 1
.limit stack 1
.end method

.method public static a(Ljava/util/Set;Lcom/a/b/b/co;)Ljava/util/Set;
aload 0
instanceof java/util/SortedSet
ifeq L0
aload 0
checkcast java/util/SortedSet
astore 0
aload 0
instanceof java/util/NavigableSet
ifeq L1
aload 0
checkcast java/util/NavigableSet
aload 1
invokestatic com/a/b/d/aad/a(Ljava/util/NavigableSet;Lcom/a/b/b/co;)Ljava/util/NavigableSet;
areturn
L1:
aload 0
instanceof com/a/b/d/aal
ifeq L2
aload 0
checkcast com/a/b/d/aal
astore 0
aload 0
getfield com/a/b/d/aal/b Lcom/a/b/b/co;
aload 1
invokestatic com/a/b/b/cp/a(Lcom/a/b/b/co;Lcom/a/b/b/co;)Lcom/a/b/b/co;
astore 1
new com/a/b/d/aam
dup
aload 0
getfield com/a/b/d/aal/a Ljava/util/Collection;
checkcast java/util/SortedSet
aload 1
invokespecial com/a/b/d/aam/<init>(Ljava/util/SortedSet;Lcom/a/b/b/co;)V
areturn
L2:
new com/a/b/d/aam
dup
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/util/SortedSet
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast com/a/b/b/co
invokespecial com/a/b/d/aam/<init>(Ljava/util/SortedSet;Lcom/a/b/b/co;)V
areturn
L0:
aload 0
instanceof com/a/b/d/aal
ifeq L3
aload 0
checkcast com/a/b/d/aal
astore 0
aload 0
getfield com/a/b/d/aal/b Lcom/a/b/b/co;
aload 1
invokestatic com/a/b/b/cp/a(Lcom/a/b/b/co;Lcom/a/b/b/co;)Lcom/a/b/b/co;
astore 1
new com/a/b/d/aal
dup
aload 0
getfield com/a/b/d/aal/a Ljava/util/Collection;
checkcast java/util/Set
aload 1
invokespecial com/a/b/d/aal/<init>(Ljava/util/Set;Lcom/a/b/b/co;)V
areturn
L3:
new com/a/b/d/aal
dup
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/util/Set
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast com/a/b/b/co
invokespecial com/a/b/d/aal/<init>(Ljava/util/Set;Lcom/a/b/b/co;)V
areturn
.limit locals 2
.limit stack 4
.end method

.method private static transient a([Ljava/util/Set;)Ljava/util/Set;
aload 0
invokestatic java/util/Arrays/asList([Ljava/lang/Object;)Ljava/util/List;
invokestatic com/a/b/d/aah/a(Ljava/util/List;)Ljava/util/Set;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static a(Ljava/util/SortedSet;Lcom/a/b/b/co;)Ljava/util/SortedSet;
aload 0
instanceof java/util/NavigableSet
ifeq L0
aload 0
checkcast java/util/NavigableSet
aload 1
invokestatic com/a/b/d/aad/a(Ljava/util/NavigableSet;Lcom/a/b/b/co;)Ljava/util/NavigableSet;
areturn
L0:
aload 0
instanceof com/a/b/d/aal
ifeq L1
aload 0
checkcast com/a/b/d/aal
astore 0
aload 0
getfield com/a/b/d/aal/b Lcom/a/b/b/co;
aload 1
invokestatic com/a/b/b/cp/a(Lcom/a/b/b/co;Lcom/a/b/b/co;)Lcom/a/b/b/co;
astore 1
new com/a/b/d/aam
dup
aload 0
getfield com/a/b/d/aal/a Ljava/util/Collection;
checkcast java/util/SortedSet
aload 1
invokespecial com/a/b/d/aam/<init>(Ljava/util/SortedSet;Lcom/a/b/b/co;)V
areturn
L1:
new com/a/b/d/aam
dup
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/util/SortedSet
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast com/a/b/b/co
invokespecial com/a/b/d/aam/<init>(Ljava/util/SortedSet;Lcom/a/b/b/co;)V
areturn
.limit locals 2
.limit stack 4
.end method

.method private static a(Ljava/util/Comparator;)Ljava/util/TreeSet;
new java/util/TreeSet
dup
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/util/Comparator
invokespecial java/util/TreeSet/<init>(Ljava/util/Comparator;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method static a(Ljava/util/Set;Ljava/lang/Object;)Z
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
.catch java/lang/NullPointerException from L0 to L1 using L2
.catch java/lang/ClassCastException from L0 to L1 using L3
aload 0
aload 1
if_acmpne L4
L5:
iconst_1
ireturn
L4:
aload 1
instanceof java/util/Set
ifeq L6
aload 1
checkcast java/util/Set
astore 1
L0:
aload 0
invokeinterface java/util/Set/size()I 0
aload 1
invokeinterface java/util/Set/size()I 0
if_icmpne L7
aload 0
aload 1
invokeinterface java/util/Set/containsAll(Ljava/util/Collection;)Z 1
istore 2
L1:
iload 2
ifne L5
L7:
iconst_0
ireturn
L2:
astore 0
iconst_0
ireturn
L3:
astore 0
iconst_0
ireturn
L6:
iconst_0
ireturn
.limit locals 3
.limit stack 2
.end method

.method static a(Ljava/util/Set;Ljava/util/Collection;)Z
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 1
astore 2
aload 1
instanceof com/a/b/d/xc
ifeq L0
aload 1
checkcast com/a/b/d/xc
invokeinterface com/a/b/d/xc/n_()Ljava/util/Set; 0
astore 2
L0:
aload 2
instanceof java/util/Set
ifeq L1
aload 2
invokeinterface java/util/Collection/size()I 0
aload 0
invokeinterface java/util/Set/size()I 0
if_icmple L1
aload 0
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
aload 2
invokestatic com/a/b/d/nj/a(Ljava/util/Iterator;Ljava/util/Collection;)Z
ireturn
L1:
aload 0
aload 2
invokeinterface java/util/Collection/iterator()Ljava/util/Iterator; 0
invokestatic com/a/b/d/aad/a(Ljava/util/Set;Ljava/util/Iterator;)Z
ireturn
.limit locals 3
.limit stack 2
.end method

.method static a(Ljava/util/Set;Ljava/util/Iterator;)Z
iconst_0
istore 2
L0:
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
iload 2
aload 0
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
invokeinterface java/util/Set/remove(Ljava/lang/Object;)Z 1
ior
istore 2
goto L0
L1:
iload 2
ireturn
.limit locals 3
.limit stack 3
.end method

.method public static b(Ljava/util/Set;Ljava/util/Set;)Lcom/a/b/d/aaq;
aload 0
ldc "set1"
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 1
ldc "set2"
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
new com/a/b/d/aaf
dup
aload 0
aload 1
invokestatic com/a/b/b/cp/a(Ljava/util/Collection;)Lcom/a/b/b/co;
aload 1
invokespecial com/a/b/d/aaf/<init>(Ljava/util/Set;Lcom/a/b/b/co;Ljava/util/Set;)V
areturn
.limit locals 2
.limit stack 5
.end method

.method private static b(Ljava/util/Collection;Ljava/lang/Class;)Ljava/util/EnumSet;
aload 1
invokestatic java/util/EnumSet/allOf(Ljava/lang/Class;)Ljava/util/EnumSet;
astore 1
aload 1
aload 0
invokevirtual java/util/EnumSet/removeAll(Ljava/util/Collection;)Z
pop
aload 1
areturn
.limit locals 2
.limit stack 2
.end method

.method private static b(Ljava/lang/Iterable;)Ljava/util/HashSet;
aload 0
instanceof java/util/Collection
ifeq L0
new java/util/HashSet
dup
aload 0
invokestatic com/a/b/d/cm/a(Ljava/lang/Iterable;)Ljava/util/Collection;
invokespecial java/util/HashSet/<init>(Ljava/util/Collection;)V
areturn
L0:
aload 0
invokeinterface java/lang/Iterable/iterator()Ljava/util/Iterator; 0
astore 0
new java/util/HashSet
dup
invokespecial java/util/HashSet/<init>()V
astore 1
aload 1
aload 0
invokestatic com/a/b/d/nj/a(Ljava/util/Collection;Ljava/util/Iterator;)Z
pop
aload 1
areturn
.limit locals 2
.limit stack 3
.end method

.method private static b(I)Ljava/util/LinkedHashSet;
new java/util/LinkedHashSet
dup
iload 0
invokestatic com/a/b/d/sz/b(I)I
invokespecial java/util/LinkedHashSet/<init>(I)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private static b(Ljava/util/NavigableSet;)Ljava/util/NavigableSet;
.annotation invisible Lcom/a/b/a/c;
a s = "NavigableSet"
.end annotation
aload 0
aconst_null
invokestatic com/a/b/d/acu/a(Ljava/util/NavigableSet;Ljava/lang/Object;)Ljava/util/NavigableSet;
areturn
.limit locals 1
.limit stack 2
.end method

.method private static b()Ljava/util/Set;
new java/util/concurrent/ConcurrentHashMap
dup
invokespecial java/util/concurrent/ConcurrentHashMap/<init>()V
invokestatic java/util/Collections/newSetFromMap(Ljava/util/Map;)Ljava/util/Set;
areturn
.limit locals 0
.limit stack 2
.end method

.method private static b(Ljava/util/Set;)Ljava/util/Set;
.annotation invisible Lcom/a/b/a/b;
a Z = 0
.end annotation
new com/a/b/d/aao
dup
aload 0
invokespecial com/a/b/d/aao/<init>(Ljava/util/Set;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private static b(Ljava/util/SortedSet;Lcom/a/b/b/co;)Ljava/util/SortedSet;
aload 0
instanceof com/a/b/d/aal
ifeq L0
aload 0
checkcast com/a/b/d/aal
astore 0
aload 0
getfield com/a/b/d/aal/b Lcom/a/b/b/co;
aload 1
invokestatic com/a/b/b/cp/a(Lcom/a/b/b/co;Lcom/a/b/b/co;)Lcom/a/b/b/co;
astore 1
new com/a/b/d/aam
dup
aload 0
getfield com/a/b/d/aal/a Ljava/util/Collection;
checkcast java/util/SortedSet
aload 1
invokespecial com/a/b/d/aam/<init>(Ljava/util/SortedSet;Lcom/a/b/b/co;)V
areturn
L0:
new com/a/b/d/aam
dup
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/util/SortedSet
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast com/a/b/b/co
invokespecial com/a/b/d/aam/<init>(Ljava/util/SortedSet;Lcom/a/b/b/co;)V
areturn
.limit locals 2
.limit stack 4
.end method

.method private static c(Ljava/util/Set;Ljava/util/Set;)Lcom/a/b/d/aaq;
aload 0
ldc "set1"
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 1
ldc "set2"
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
new com/a/b/d/aag
dup
aload 0
aload 1
invokestatic com/a/b/b/cp/a(Ljava/util/Collection;)Lcom/a/b/b/co;
invokestatic com/a/b/b/cp/a(Lcom/a/b/b/co;)Lcom/a/b/b/co;
aload 1
invokespecial com/a/b/d/aag/<init>(Ljava/util/Set;Lcom/a/b/b/co;Ljava/util/Set;)V
areturn
.limit locals 2
.limit stack 5
.end method

.method private static c()Ljava/util/LinkedHashSet;
new java/util/LinkedHashSet
dup
invokespecial java/util/LinkedHashSet/<init>()V
areturn
.limit locals 0
.limit stack 2
.end method

.method private static c(Ljava/lang/Iterable;)Ljava/util/Set;
new java/util/concurrent/ConcurrentHashMap
dup
invokespecial java/util/concurrent/ConcurrentHashMap/<init>()V
invokestatic java/util/Collections/newSetFromMap(Ljava/util/Map;)Ljava/util/Set;
astore 1
aload 1
aload 0
invokestatic com/a/b/d/mq/a(Ljava/util/Collection;Ljava/lang/Iterable;)Z
pop
aload 1
areturn
.limit locals 2
.limit stack 2
.end method

.method private static d(Ljava/util/Set;Ljava/util/Set;)Lcom/a/b/d/aaq;
aload 0
ldc "set1"
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 1
ldc "set2"
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
aload 1
invokestatic com/a/b/d/aad/a(Ljava/util/Set;Ljava/util/Set;)Lcom/a/b/d/aaq;
aload 0
aload 1
invokestatic com/a/b/d/aad/b(Ljava/util/Set;Ljava/util/Set;)Lcom/a/b/d/aaq;
invokestatic com/a/b/d/aad/c(Ljava/util/Set;Ljava/util/Set;)Lcom/a/b/d/aaq;
areturn
.limit locals 2
.limit stack 3
.end method

.method private static d(Ljava/lang/Iterable;)Ljava/util/LinkedHashSet;
aload 0
instanceof java/util/Collection
ifeq L0
new java/util/LinkedHashSet
dup
aload 0
invokestatic com/a/b/d/cm/a(Ljava/lang/Iterable;)Ljava/util/Collection;
invokespecial java/util/LinkedHashSet/<init>(Ljava/util/Collection;)V
areturn
L0:
new java/util/LinkedHashSet
dup
invokespecial java/util/LinkedHashSet/<init>()V
astore 1
aload 1
aload 0
invokestatic com/a/b/d/mq/a(Ljava/util/Collection;Ljava/lang/Iterable;)Z
pop
aload 1
areturn
.limit locals 2
.limit stack 3
.end method

.method private static d()Ljava/util/TreeSet;
new java/util/TreeSet
dup
invokespecial java/util/TreeSet/<init>()V
areturn
.limit locals 0
.limit stack 2
.end method

.method private static e()Ljava/util/Set;
invokestatic com/a/b/d/sz/f()Ljava/util/IdentityHashMap;
invokestatic java/util/Collections/newSetFromMap(Ljava/util/Map;)Ljava/util/Set;
areturn
.limit locals 0
.limit stack 1
.end method

.method private static e(Ljava/lang/Iterable;)Ljava/util/TreeSet;
new java/util/TreeSet
dup
invokespecial java/util/TreeSet/<init>()V
astore 1
aload 1
aload 0
invokestatic com/a/b/d/mq/a(Ljava/util/Collection;Ljava/lang/Iterable;)Z
pop
aload 1
areturn
.limit locals 2
.limit stack 2
.end method

.method private static f()Ljava/util/concurrent/CopyOnWriteArraySet;
.annotation invisible Lcom/a/b/a/c;
a s = "CopyOnWriteArraySet"
.end annotation
new java/util/concurrent/CopyOnWriteArraySet
dup
invokespecial java/util/concurrent/CopyOnWriteArraySet/<init>()V
areturn
.limit locals 0
.limit stack 2
.end method

.method private static f(Ljava/lang/Iterable;)Ljava/util/concurrent/CopyOnWriteArraySet;
.annotation invisible Lcom/a/b/a/c;
a s = "CopyOnWriteArraySet"
.end annotation
aload 0
instanceof java/util/Collection
ifeq L0
aload 0
invokestatic com/a/b/d/cm/a(Ljava/lang/Iterable;)Ljava/util/Collection;
astore 0
L1:
new java/util/concurrent/CopyOnWriteArraySet
dup
aload 0
invokespecial java/util/concurrent/CopyOnWriteArraySet/<init>(Ljava/util/Collection;)V
areturn
L0:
aload 0
invokestatic com/a/b/d/ov/a(Ljava/lang/Iterable;)Ljava/util/ArrayList;
astore 0
goto L1
.limit locals 1
.limit stack 3
.end method
