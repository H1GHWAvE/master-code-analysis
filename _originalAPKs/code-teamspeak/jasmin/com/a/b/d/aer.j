.bytecode 50.0
.class public final synchronized com/a/b/d/aer
.super com/a/b/d/bc
.implements java/io/Serializable
.annotation invisible Lcom/a/b/a/b;
b Z = 1
.end annotation

.field private static final 'e' J = 1L

.annotation invisible Lcom/a/b/a/c;
a s = "not needed in emulated source"
.end annotation
.end field

.field private final transient 'b' Lcom/a/b/d/afa;

.field private final transient 'c' Lcom/a/b/d/hs;

.field private final transient 'd' Lcom/a/b/d/aez;

.method private <init>(Lcom/a/b/d/afa;Lcom/a/b/d/hs;Lcom/a/b/d/aez;)V
aload 0
aload 2
getfield com/a/b/d/hs/a Ljava/util/Comparator;
invokespecial com/a/b/d/bc/<init>(Ljava/util/Comparator;)V
aload 0
aload 1
putfield com/a/b/d/aer/b Lcom/a/b/d/afa;
aload 0
aload 2
putfield com/a/b/d/aer/c Lcom/a/b/d/hs;
aload 0
aload 3
putfield com/a/b/d/aer/d Lcom/a/b/d/aez;
return
.limit locals 4
.limit stack 2
.end method

.method private <init>(Ljava/util/Comparator;)V
aload 0
aload 1
invokespecial com/a/b/d/bc/<init>(Ljava/util/Comparator;)V
aload 0
aload 1
invokestatic com/a/b/d/hs/a(Ljava/util/Comparator;)Lcom/a/b/d/hs;
putfield com/a/b/d/aer/c Lcom/a/b/d/hs;
aload 0
new com/a/b/d/aez
dup
aconst_null
iconst_1
invokespecial com/a/b/d/aez/<init>(Ljava/lang/Object;I)V
putfield com/a/b/d/aer/d Lcom/a/b/d/aez;
aload 0
getfield com/a/b/d/aer/d Lcom/a/b/d/aez;
aload 0
getfield com/a/b/d/aer/d Lcom/a/b/d/aez;
invokestatic com/a/b/d/aer/b(Lcom/a/b/d/aez;Lcom/a/b/d/aez;)V
aload 0
new com/a/b/d/afa
dup
iconst_0
invokespecial com/a/b/d/afa/<init>(B)V
putfield com/a/b/d/aer/b Lcom/a/b/d/afa;
return
.limit locals 2
.limit stack 5
.end method

.method static a(Lcom/a/b/d/aez;)I
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
ifnonnull L0
iconst_0
ireturn
L0:
aload 0
getfield com/a/b/d/aez/c I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private a(Lcom/a/b/d/aew;)J
aload 0
getfield com/a/b/d/aer/b Lcom/a/b/d/afa;
getfield com/a/b/d/afa/a Ljava/lang/Object;
checkcast com/a/b/d/aez
astore 6
aload 1
aload 6
invokevirtual com/a/b/d/aew/b(Lcom/a/b/d/aez;)J
lstore 4
lload 4
lstore 2
aload 0
getfield com/a/b/d/aer/c Lcom/a/b/d/hs;
getfield com/a/b/d/hs/b Z
ifeq L0
lload 4
aload 0
aload 1
aload 6
invokespecial com/a/b/d/aer/a(Lcom/a/b/d/aew;Lcom/a/b/d/aez;)J
lsub
lstore 2
L0:
lload 2
lstore 4
aload 0
getfield com/a/b/d/aer/c Lcom/a/b/d/hs;
getfield com/a/b/d/hs/e Z
ifeq L1
lload 2
aload 0
aload 1
aload 6
invokespecial com/a/b/d/aer/b(Lcom/a/b/d/aew;Lcom/a/b/d/aez;)J
lsub
lstore 4
L1:
lload 4
lreturn
.limit locals 7
.limit stack 5
.end method

.method private a(Lcom/a/b/d/aew;Lcom/a/b/d/aez;)J
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
L0:
aload 2
ifnonnull L1
lconst_0
lreturn
L1:
aload 0
invokevirtual com/a/b/d/aer/comparator()Ljava/util/Comparator;
aload 0
getfield com/a/b/d/aer/c Lcom/a/b/d/hs;
getfield com/a/b/d/hs/c Ljava/lang/Object;
aload 2
getfield com/a/b/d/aez/a Ljava/lang/Object;
invokeinterface java/util/Comparator/compare(Ljava/lang/Object;Ljava/lang/Object;)I 2
istore 3
iload 3
ifge L2
aload 2
getfield com/a/b/d/aez/e Lcom/a/b/d/aez;
astore 2
goto L0
L2:
iload 3
ifne L3
getstatic com/a/b/d/aev/a [I
aload 0
getfield com/a/b/d/aer/c Lcom/a/b/d/hs;
getfield com/a/b/d/hs/d Lcom/a/b/d/ce;
invokevirtual com/a/b/d/ce/ordinal()I
iaload
tableswitch 1
L4
L5
default : L6
L6:
new java/lang/AssertionError
dup
invokespecial java/lang/AssertionError/<init>()V
athrow
L4:
aload 1
aload 2
invokevirtual com/a/b/d/aew/a(Lcom/a/b/d/aez;)I
i2l
aload 1
aload 2
getfield com/a/b/d/aez/e Lcom/a/b/d/aez;
invokevirtual com/a/b/d/aew/b(Lcom/a/b/d/aez;)J
ladd
lreturn
L5:
aload 1
aload 2
getfield com/a/b/d/aez/e Lcom/a/b/d/aez;
invokevirtual com/a/b/d/aew/b(Lcom/a/b/d/aez;)J
lreturn
L3:
aload 1
aload 2
getfield com/a/b/d/aez/e Lcom/a/b/d/aez;
invokevirtual com/a/b/d/aew/b(Lcom/a/b/d/aez;)J
aload 1
aload 2
invokevirtual com/a/b/d/aew/a(Lcom/a/b/d/aez;)I
i2l
ladd
aload 0
aload 1
aload 2
getfield com/a/b/d/aez/f Lcom/a/b/d/aez;
invokespecial com/a/b/d/aer/a(Lcom/a/b/d/aew;Lcom/a/b/d/aez;)J
ladd
lreturn
.limit locals 4
.limit stack 5
.end method

.method private static a(Ljava/lang/Iterable;)Lcom/a/b/d/aer;
new com/a/b/d/aer
dup
invokestatic com/a/b/d/yd/d()Lcom/a/b/d/yd;
invokespecial com/a/b/d/aer/<init>(Ljava/util/Comparator;)V
astore 1
aload 1
aload 0
invokestatic com/a/b/d/mq/a(Ljava/util/Collection;Ljava/lang/Iterable;)Z
pop
aload 1
areturn
.limit locals 2
.limit stack 3
.end method

.method public static a(Ljava/util/Comparator;)Lcom/a/b/d/aer;
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
ifnonnull L0
new com/a/b/d/aer
dup
invokestatic com/a/b/d/yd/d()Lcom/a/b/d/yd;
invokespecial com/a/b/d/aer/<init>(Ljava/util/Comparator;)V
areturn
L0:
new com/a/b/d/aer
dup
aload 0
invokespecial com/a/b/d/aer/<init>(Ljava/util/Comparator;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method static synthetic a(Lcom/a/b/d/aer;)Lcom/a/b/d/aez;
aload 0
getfield com/a/b/d/aer/b Lcom/a/b/d/afa;
getfield com/a/b/d/afa/a Ljava/lang/Object;
checkcast com/a/b/d/aez
ifnonnull L0
aconst_null
astore 2
L1:
aload 2
areturn
L0:
aload 0
getfield com/a/b/d/aer/c Lcom/a/b/d/hs;
getfield com/a/b/d/hs/b Z
ifeq L2
aload 0
getfield com/a/b/d/aer/c Lcom/a/b/d/hs;
getfield com/a/b/d/hs/c Ljava/lang/Object;
astore 3
aload 0
getfield com/a/b/d/aer/b Lcom/a/b/d/afa;
getfield com/a/b/d/afa/a Ljava/lang/Object;
checkcast com/a/b/d/aez
aload 0
invokevirtual com/a/b/d/aer/comparator()Ljava/util/Comparator;
aload 3
invokevirtual com/a/b/d/aez/a(Ljava/util/Comparator;Ljava/lang/Object;)Lcom/a/b/d/aez;
astore 2
aload 2
ifnonnull L3
aconst_null
areturn
L3:
aload 2
astore 1
aload 0
getfield com/a/b/d/aer/c Lcom/a/b/d/hs;
getfield com/a/b/d/hs/d Lcom/a/b/d/ce;
getstatic com/a/b/d/ce/a Lcom/a/b/d/ce;
if_acmpne L4
aload 2
astore 1
aload 0
invokevirtual com/a/b/d/aer/comparator()Ljava/util/Comparator;
aload 3
aload 2
getfield com/a/b/d/aez/a Ljava/lang/Object;
invokeinterface java/util/Comparator/compare(Ljava/lang/Object;Ljava/lang/Object;)I 2
ifne L4
aload 2
getfield com/a/b/d/aez/h Lcom/a/b/d/aez;
astore 1
L4:
aload 1
aload 0
getfield com/a/b/d/aer/d Lcom/a/b/d/aez;
if_acmpeq L5
aload 1
astore 2
aload 0
getfield com/a/b/d/aer/c Lcom/a/b/d/hs;
aload 1
getfield com/a/b/d/aez/a Ljava/lang/Object;
invokevirtual com/a/b/d/hs/c(Ljava/lang/Object;)Z
ifne L1
L5:
aconst_null
areturn
L2:
aload 0
getfield com/a/b/d/aer/d Lcom/a/b/d/aez;
getfield com/a/b/d/aez/h Lcom/a/b/d/aez;
astore 1
goto L4
.limit locals 4
.limit stack 3
.end method

.method static synthetic a(Lcom/a/b/d/aer;Lcom/a/b/d/aez;)Lcom/a/b/d/xd;
new com/a/b/d/aes
dup
aload 0
aload 1
invokespecial com/a/b/d/aes/<init>(Lcom/a/b/d/aer;Lcom/a/b/d/aez;)V
areturn
.limit locals 2
.limit stack 4
.end method

.method static synthetic a(Lcom/a/b/d/aez;Lcom/a/b/d/aez;)V
aload 0
aload 1
invokestatic com/a/b/d/aer/b(Lcom/a/b/d/aez;Lcom/a/b/d/aez;)V
return
.limit locals 2
.limit stack 2
.end method

.method static synthetic a(Lcom/a/b/d/aez;Lcom/a/b/d/aez;Lcom/a/b/d/aez;)V
aload 0
aload 1
aload 2
invokestatic com/a/b/d/aer/b(Lcom/a/b/d/aez;Lcom/a/b/d/aez;Lcom/a/b/d/aez;)V
return
.limit locals 3
.limit stack 3
.end method

.method private a(Ljava/io/ObjectInputStream;)V
.annotation invisible Lcom/a/b/a/c;
a s = "java.io.ObjectInputStream"
.end annotation
aload 1
invokevirtual java/io/ObjectInputStream/defaultReadObject()V
aload 1
invokevirtual java/io/ObjectInputStream/readObject()Ljava/lang/Object;
checkcast java/util/Comparator
astore 2
ldc com/a/b/d/bc
ldc "comparator"
invokestatic com/a/b/d/zz/a(Ljava/lang/Class;Ljava/lang/String;)Lcom/a/b/d/aab;
aload 0
aload 2
invokevirtual com/a/b/d/aab/a(Ljava/lang/Object;Ljava/lang/Object;)V
ldc com/a/b/d/aer
ldc "range"
invokestatic com/a/b/d/zz/a(Ljava/lang/Class;Ljava/lang/String;)Lcom/a/b/d/aab;
aload 0
aload 2
invokestatic com/a/b/d/hs/a(Ljava/util/Comparator;)Lcom/a/b/d/hs;
invokevirtual com/a/b/d/aab/a(Ljava/lang/Object;Ljava/lang/Object;)V
ldc com/a/b/d/aer
ldc "rootReference"
invokestatic com/a/b/d/zz/a(Ljava/lang/Class;Ljava/lang/String;)Lcom/a/b/d/aab;
aload 0
new com/a/b/d/afa
dup
iconst_0
invokespecial com/a/b/d/afa/<init>(B)V
invokevirtual com/a/b/d/aab/a(Ljava/lang/Object;Ljava/lang/Object;)V
new com/a/b/d/aez
dup
aconst_null
iconst_1
invokespecial com/a/b/d/aez/<init>(Ljava/lang/Object;I)V
astore 2
ldc com/a/b/d/aer
ldc "header"
invokestatic com/a/b/d/zz/a(Ljava/lang/Class;Ljava/lang/String;)Lcom/a/b/d/aab;
aload 0
aload 2
invokevirtual com/a/b/d/aab/a(Ljava/lang/Object;Ljava/lang/Object;)V
aload 2
aload 2
invokestatic com/a/b/d/aer/b(Lcom/a/b/d/aez;Lcom/a/b/d/aez;)V
aload 0
aload 1
invokestatic com/a/b/d/zz/a(Lcom/a/b/d/xc;Ljava/io/ObjectInputStream;)V
return
.limit locals 3
.limit stack 5
.end method

.method private a(Ljava/io/ObjectOutputStream;)V
.annotation invisible Lcom/a/b/a/c;
a s = "java.io.ObjectOutputStream"
.end annotation
aload 1
invokevirtual java/io/ObjectOutputStream/defaultWriteObject()V
aload 1
aload 0
invokespecial com/a/b/d/bc/e_()Ljava/util/NavigableSet;
invokeinterface java/util/NavigableSet/comparator()Ljava/util/Comparator; 0
invokevirtual java/io/ObjectOutputStream/writeObject(Ljava/lang/Object;)V
aload 0
aload 1
invokestatic com/a/b/d/zz/a(Lcom/a/b/d/xc;Ljava/io/ObjectOutputStream;)V
return
.limit locals 2
.limit stack 2
.end method

.method private b(Lcom/a/b/d/aew;Lcom/a/b/d/aez;)J
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
L0:
aload 2
ifnonnull L1
lconst_0
lreturn
L1:
aload 0
invokevirtual com/a/b/d/aer/comparator()Ljava/util/Comparator;
aload 0
getfield com/a/b/d/aer/c Lcom/a/b/d/hs;
getfield com/a/b/d/hs/f Ljava/lang/Object;
aload 2
getfield com/a/b/d/aez/a Ljava/lang/Object;
invokeinterface java/util/Comparator/compare(Ljava/lang/Object;Ljava/lang/Object;)I 2
istore 3
iload 3
ifle L2
aload 2
getfield com/a/b/d/aez/f Lcom/a/b/d/aez;
astore 2
goto L0
L2:
iload 3
ifne L3
getstatic com/a/b/d/aev/a [I
aload 0
getfield com/a/b/d/aer/c Lcom/a/b/d/hs;
getfield com/a/b/d/hs/g Lcom/a/b/d/ce;
invokevirtual com/a/b/d/ce/ordinal()I
iaload
tableswitch 1
L4
L5
default : L6
L6:
new java/lang/AssertionError
dup
invokespecial java/lang/AssertionError/<init>()V
athrow
L4:
aload 1
aload 2
invokevirtual com/a/b/d/aew/a(Lcom/a/b/d/aez;)I
i2l
aload 1
aload 2
getfield com/a/b/d/aez/f Lcom/a/b/d/aez;
invokevirtual com/a/b/d/aew/b(Lcom/a/b/d/aez;)J
ladd
lreturn
L5:
aload 1
aload 2
getfield com/a/b/d/aez/f Lcom/a/b/d/aez;
invokevirtual com/a/b/d/aew/b(Lcom/a/b/d/aez;)J
lreturn
L3:
aload 1
aload 2
getfield com/a/b/d/aez/f Lcom/a/b/d/aez;
invokevirtual com/a/b/d/aew/b(Lcom/a/b/d/aez;)J
aload 1
aload 2
invokevirtual com/a/b/d/aew/a(Lcom/a/b/d/aez;)I
i2l
ladd
aload 0
aload 1
aload 2
getfield com/a/b/d/aez/e Lcom/a/b/d/aez;
invokespecial com/a/b/d/aer/b(Lcom/a/b/d/aew;Lcom/a/b/d/aez;)J
ladd
lreturn
.limit locals 4
.limit stack 5
.end method

.method static synthetic b(Lcom/a/b/d/aer;)Lcom/a/b/d/hs;
aload 0
getfield com/a/b/d/aer/c Lcom/a/b/d/hs;
areturn
.limit locals 1
.limit stack 1
.end method

.method private b(Lcom/a/b/d/aez;)Lcom/a/b/d/xd;
new com/a/b/d/aes
dup
aload 0
aload 1
invokespecial com/a/b/d/aes/<init>(Lcom/a/b/d/aer;Lcom/a/b/d/aez;)V
areturn
.limit locals 2
.limit stack 4
.end method

.method private static b(Lcom/a/b/d/aez;Lcom/a/b/d/aez;)V
aload 0
aload 1
putfield com/a/b/d/aez/h Lcom/a/b/d/aez;
aload 1
aload 0
putfield com/a/b/d/aez/g Lcom/a/b/d/aez;
return
.limit locals 2
.limit stack 2
.end method

.method private static b(Lcom/a/b/d/aez;Lcom/a/b/d/aez;Lcom/a/b/d/aez;)V
aload 0
aload 1
invokestatic com/a/b/d/aer/b(Lcom/a/b/d/aez;Lcom/a/b/d/aez;)V
aload 1
aload 2
invokestatic com/a/b/d/aer/b(Lcom/a/b/d/aez;Lcom/a/b/d/aez;)V
return
.limit locals 3
.limit stack 2
.end method

.method static synthetic c(Lcom/a/b/d/aer;)Lcom/a/b/d/aez;
aload 0
getfield com/a/b/d/aer/d Lcom/a/b/d/aez;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic d(Lcom/a/b/d/aer;)Lcom/a/b/d/aez;
aload 0
getfield com/a/b/d/aer/b Lcom/a/b/d/afa;
getfield com/a/b/d/afa/a Ljava/lang/Object;
checkcast com/a/b/d/aez
ifnonnull L0
aconst_null
astore 2
L1:
aload 2
areturn
L0:
aload 0
getfield com/a/b/d/aer/c Lcom/a/b/d/hs;
getfield com/a/b/d/hs/e Z
ifeq L2
aload 0
getfield com/a/b/d/aer/c Lcom/a/b/d/hs;
getfield com/a/b/d/hs/f Ljava/lang/Object;
astore 3
aload 0
getfield com/a/b/d/aer/b Lcom/a/b/d/afa;
getfield com/a/b/d/afa/a Ljava/lang/Object;
checkcast com/a/b/d/aez
aload 0
invokevirtual com/a/b/d/aer/comparator()Ljava/util/Comparator;
aload 3
invokevirtual com/a/b/d/aez/b(Ljava/util/Comparator;Ljava/lang/Object;)Lcom/a/b/d/aez;
astore 2
aload 2
ifnonnull L3
aconst_null
areturn
L3:
aload 2
astore 1
aload 0
getfield com/a/b/d/aer/c Lcom/a/b/d/hs;
getfield com/a/b/d/hs/g Lcom/a/b/d/ce;
getstatic com/a/b/d/ce/a Lcom/a/b/d/ce;
if_acmpne L4
aload 2
astore 1
aload 0
invokevirtual com/a/b/d/aer/comparator()Ljava/util/Comparator;
aload 3
aload 2
getfield com/a/b/d/aez/a Ljava/lang/Object;
invokeinterface java/util/Comparator/compare(Ljava/lang/Object;Ljava/lang/Object;)I 2
ifne L4
aload 2
getfield com/a/b/d/aez/g Lcom/a/b/d/aez;
astore 1
L4:
aload 1
aload 0
getfield com/a/b/d/aer/d Lcom/a/b/d/aez;
if_acmpeq L5
aload 1
astore 2
aload 0
getfield com/a/b/d/aer/c Lcom/a/b/d/hs;
aload 1
getfield com/a/b/d/aez/a Ljava/lang/Object;
invokevirtual com/a/b/d/hs/c(Ljava/lang/Object;)Z
ifne L1
L5:
aconst_null
areturn
L2:
aload 0
getfield com/a/b/d/aer/d Lcom/a/b/d/aez;
getfield com/a/b/d/aez/g Lcom/a/b/d/aez;
astore 1
goto L4
.limit locals 4
.limit stack 3
.end method

.method private static o()Lcom/a/b/d/aer;
new com/a/b/d/aer
dup
invokestatic com/a/b/d/yd/d()Lcom/a/b/d/yd;
invokespecial com/a/b/d/aer/<init>(Ljava/util/Comparator;)V
areturn
.limit locals 0
.limit stack 3
.end method

.method private p()Lcom/a/b/d/aez;
.annotation visible Ljavax/annotation/Nullable;
.end annotation
aload 0
getfield com/a/b/d/aer/b Lcom/a/b/d/afa;
getfield com/a/b/d/afa/a Ljava/lang/Object;
checkcast com/a/b/d/aez
ifnonnull L0
aconst_null
astore 2
L1:
aload 2
areturn
L0:
aload 0
getfield com/a/b/d/aer/c Lcom/a/b/d/hs;
getfield com/a/b/d/hs/b Z
ifeq L2
aload 0
getfield com/a/b/d/aer/c Lcom/a/b/d/hs;
getfield com/a/b/d/hs/c Ljava/lang/Object;
astore 3
aload 0
getfield com/a/b/d/aer/b Lcom/a/b/d/afa;
getfield com/a/b/d/afa/a Ljava/lang/Object;
checkcast com/a/b/d/aez
aload 0
invokevirtual com/a/b/d/aer/comparator()Ljava/util/Comparator;
aload 3
invokevirtual com/a/b/d/aez/a(Ljava/util/Comparator;Ljava/lang/Object;)Lcom/a/b/d/aez;
astore 2
aload 2
ifnonnull L3
aconst_null
areturn
L3:
aload 2
astore 1
aload 0
getfield com/a/b/d/aer/c Lcom/a/b/d/hs;
getfield com/a/b/d/hs/d Lcom/a/b/d/ce;
getstatic com/a/b/d/ce/a Lcom/a/b/d/ce;
if_acmpne L4
aload 2
astore 1
aload 0
invokevirtual com/a/b/d/aer/comparator()Ljava/util/Comparator;
aload 3
aload 2
getfield com/a/b/d/aez/a Ljava/lang/Object;
invokeinterface java/util/Comparator/compare(Ljava/lang/Object;Ljava/lang/Object;)I 2
ifne L4
aload 2
getfield com/a/b/d/aez/h Lcom/a/b/d/aez;
astore 1
L4:
aload 1
aload 0
getfield com/a/b/d/aer/d Lcom/a/b/d/aez;
if_acmpeq L5
aload 1
astore 2
aload 0
getfield com/a/b/d/aer/c Lcom/a/b/d/hs;
aload 1
getfield com/a/b/d/aez/a Ljava/lang/Object;
invokevirtual com/a/b/d/hs/c(Ljava/lang/Object;)Z
ifne L1
L5:
aconst_null
areturn
L2:
aload 0
getfield com/a/b/d/aer/d Lcom/a/b/d/aez;
getfield com/a/b/d/aez/h Lcom/a/b/d/aez;
astore 1
goto L4
.limit locals 4
.limit stack 3
.end method

.method private q()Lcom/a/b/d/aez;
.annotation visible Ljavax/annotation/Nullable;
.end annotation
aload 0
getfield com/a/b/d/aer/b Lcom/a/b/d/afa;
getfield com/a/b/d/afa/a Ljava/lang/Object;
checkcast com/a/b/d/aez
ifnonnull L0
aconst_null
astore 2
L1:
aload 2
areturn
L0:
aload 0
getfield com/a/b/d/aer/c Lcom/a/b/d/hs;
getfield com/a/b/d/hs/e Z
ifeq L2
aload 0
getfield com/a/b/d/aer/c Lcom/a/b/d/hs;
getfield com/a/b/d/hs/f Ljava/lang/Object;
astore 3
aload 0
getfield com/a/b/d/aer/b Lcom/a/b/d/afa;
getfield com/a/b/d/afa/a Ljava/lang/Object;
checkcast com/a/b/d/aez
aload 0
invokevirtual com/a/b/d/aer/comparator()Ljava/util/Comparator;
aload 3
invokevirtual com/a/b/d/aez/b(Ljava/util/Comparator;Ljava/lang/Object;)Lcom/a/b/d/aez;
astore 2
aload 2
ifnonnull L3
aconst_null
areturn
L3:
aload 2
astore 1
aload 0
getfield com/a/b/d/aer/c Lcom/a/b/d/hs;
getfield com/a/b/d/hs/g Lcom/a/b/d/ce;
getstatic com/a/b/d/ce/a Lcom/a/b/d/ce;
if_acmpne L4
aload 2
astore 1
aload 0
invokevirtual com/a/b/d/aer/comparator()Ljava/util/Comparator;
aload 3
aload 2
getfield com/a/b/d/aez/a Ljava/lang/Object;
invokeinterface java/util/Comparator/compare(Ljava/lang/Object;Ljava/lang/Object;)I 2
ifne L4
aload 2
getfield com/a/b/d/aez/g Lcom/a/b/d/aez;
astore 1
L4:
aload 1
aload 0
getfield com/a/b/d/aer/d Lcom/a/b/d/aez;
if_acmpeq L5
aload 1
astore 2
aload 0
getfield com/a/b/d/aer/c Lcom/a/b/d/hs;
aload 1
getfield com/a/b/d/aez/a Ljava/lang/Object;
invokevirtual com/a/b/d/hs/c(Ljava/lang/Object;)Z
ifne L1
L5:
aconst_null
areturn
L2:
aload 0
getfield com/a/b/d/aer/d Lcom/a/b/d/aez;
getfield com/a/b/d/aez/g Lcom/a/b/d/aez;
astore 1
goto L4
.limit locals 4
.limit stack 3
.end method

.method public final a(Ljava/lang/Object;)I
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
.catch java/lang/ClassCastException from L0 to L1 using L2
.catch java/lang/NullPointerException from L0 to L1 using L3
.catch java/lang/ClassCastException from L4 to L5 using L2
.catch java/lang/NullPointerException from L4 to L5 using L3
.catch java/lang/ClassCastException from L5 to L6 using L2
.catch java/lang/NullPointerException from L5 to L6 using L3
.catch java/lang/ClassCastException from L7 to L8 using L2
.catch java/lang/NullPointerException from L7 to L8 using L3
.catch java/lang/ClassCastException from L9 to L10 using L2
.catch java/lang/NullPointerException from L9 to L10 using L3
.catch java/lang/ClassCastException from L11 to L12 using L2
.catch java/lang/NullPointerException from L11 to L12 using L3
.catch java/lang/ClassCastException from L13 to L14 using L2
.catch java/lang/NullPointerException from L13 to L14 using L3
.catch java/lang/ClassCastException from L15 to L16 using L2
.catch java/lang/NullPointerException from L15 to L16 using L3
L0:
aload 0
getfield com/a/b/d/aer/b Lcom/a/b/d/afa;
getfield com/a/b/d/afa/a Ljava/lang/Object;
checkcast com/a/b/d/aez
astore 3
aload 0
getfield com/a/b/d/aer/c Lcom/a/b/d/hs;
aload 1
invokevirtual com/a/b/d/hs/c(Ljava/lang/Object;)Z
ifeq L17
L1:
aload 3
ifnonnull L4
goto L17
L4:
aload 0
invokevirtual com/a/b/d/aer/comparator()Ljava/util/Comparator;
astore 4
L5:
aload 4
aload 1
aload 3
getfield com/a/b/d/aez/a Ljava/lang/Object;
invokeinterface java/util/Comparator/compare(Ljava/lang/Object;Ljava/lang/Object;)I 2
istore 2
L6:
iload 2
ifge L18
L7:
aload 3
getfield com/a/b/d/aez/e Lcom/a/b/d/aez;
ifnonnull L9
L8:
iconst_0
ireturn
L9:
aload 3
getfield com/a/b/d/aez/e Lcom/a/b/d/aez;
astore 3
L10:
goto L5
L18:
iload 2
ifle L15
L11:
aload 3
getfield com/a/b/d/aez/f Lcom/a/b/d/aez;
ifnonnull L13
L12:
iconst_0
ireturn
L13:
aload 3
getfield com/a/b/d/aez/f Lcom/a/b/d/aez;
astore 3
L14:
goto L5
L15:
aload 3
getfield com/a/b/d/aez/b I
istore 2
L16:
iload 2
ireturn
L2:
astore 1
iconst_0
ireturn
L3:
astore 1
iconst_0
ireturn
L17:
iconst_0
ireturn
.limit locals 5
.limit stack 3
.end method

.method public final a(Ljava/lang/Object;I)I
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
iload 2
ldc "occurrences"
invokestatic com/a/b/d/cl/a(ILjava/lang/String;)I
pop
iload 2
ifne L0
aload 0
aload 1
invokevirtual com/a/b/d/aer/a(Ljava/lang/Object;)I
ireturn
L0:
aload 0
getfield com/a/b/d/aer/c Lcom/a/b/d/hs;
aload 1
invokevirtual com/a/b/d/hs/c(Ljava/lang/Object;)Z
invokestatic com/a/b/b/cn/a(Z)V
aload 0
getfield com/a/b/d/aer/b Lcom/a/b/d/afa;
getfield com/a/b/d/afa/a Ljava/lang/Object;
checkcast com/a/b/d/aez
astore 3
aload 3
ifnonnull L1
aload 0
invokevirtual com/a/b/d/aer/comparator()Ljava/util/Comparator;
aload 1
aload 1
invokeinterface java/util/Comparator/compare(Ljava/lang/Object;Ljava/lang/Object;)I 2
pop
new com/a/b/d/aez
dup
aload 1
iload 2
invokespecial com/a/b/d/aez/<init>(Ljava/lang/Object;I)V
astore 1
aload 0
getfield com/a/b/d/aer/d Lcom/a/b/d/aez;
aload 1
aload 0
getfield com/a/b/d/aer/d Lcom/a/b/d/aez;
invokestatic com/a/b/d/aer/b(Lcom/a/b/d/aez;Lcom/a/b/d/aez;Lcom/a/b/d/aez;)V
aload 0
getfield com/a/b/d/aer/b Lcom/a/b/d/afa;
aload 3
aload 1
invokevirtual com/a/b/d/afa/a(Ljava/lang/Object;Ljava/lang/Object;)V
iconst_0
ireturn
L1:
iconst_1
newarray int
astore 4
aload 3
aload 0
invokevirtual com/a/b/d/aer/comparator()Ljava/util/Comparator;
aload 1
iload 2
aload 4
invokevirtual com/a/b/d/aez/a(Ljava/util/Comparator;Ljava/lang/Object;I[I)Lcom/a/b/d/aez;
astore 1
aload 0
getfield com/a/b/d/aer/b Lcom/a/b/d/afa;
aload 3
aload 1
invokevirtual com/a/b/d/afa/a(Ljava/lang/Object;Ljava/lang/Object;)V
aload 4
iconst_0
iaload
ireturn
.limit locals 5
.limit stack 5
.end method

.method public final volatile synthetic a(Ljava/lang/Object;Lcom/a/b/d/ce;Ljava/lang/Object;Lcom/a/b/d/ce;)Lcom/a/b/d/abn;
aload 0
aload 1
aload 2
aload 3
aload 4
invokespecial com/a/b/d/bc/a(Ljava/lang/Object;Lcom/a/b/d/ce;Ljava/lang/Object;Lcom/a/b/d/ce;)Lcom/a/b/d/abn;
areturn
.limit locals 5
.limit stack 5
.end method

.method public final volatile synthetic a()Ljava/util/Set;
aload 0
invokespecial com/a/b/d/bc/a()Ljava/util/Set;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final a(Ljava/lang/Object;II)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
iload 3
ldc "newCount"
invokestatic com/a/b/d/cl/a(ILjava/lang/String;)I
pop
iload 2
ldc "oldCount"
invokestatic com/a/b/d/cl/a(ILjava/lang/String;)I
pop
aload 0
getfield com/a/b/d/aer/c Lcom/a/b/d/hs;
aload 1
invokevirtual com/a/b/d/hs/c(Ljava/lang/Object;)Z
invokestatic com/a/b/b/cn/a(Z)V
aload 0
getfield com/a/b/d/aer/b Lcom/a/b/d/afa;
getfield com/a/b/d/afa/a Ljava/lang/Object;
checkcast com/a/b/d/aez
astore 4
aload 4
ifnonnull L0
iload 2
ifne L1
iload 3
ifle L2
aload 0
aload 1
iload 3
invokevirtual com/a/b/d/aer/a(Ljava/lang/Object;I)I
pop
L2:
iconst_1
ireturn
L1:
iconst_0
ireturn
L0:
iconst_1
newarray int
astore 5
aload 4
aload 0
invokevirtual com/a/b/d/aer/comparator()Ljava/util/Comparator;
aload 1
iload 2
iload 3
aload 5
invokevirtual com/a/b/d/aez/a(Ljava/util/Comparator;Ljava/lang/Object;II[I)Lcom/a/b/d/aez;
astore 1
aload 0
getfield com/a/b/d/aer/b Lcom/a/b/d/afa;
aload 4
aload 1
invokevirtual com/a/b/d/afa/a(Ljava/lang/Object;Ljava/lang/Object;)V
aload 5
iconst_0
iaload
iload 2
if_icmpne L3
iconst_1
ireturn
L3:
iconst_0
ireturn
.limit locals 6
.limit stack 6
.end method

.method public final volatile synthetic add(Ljava/lang/Object;)Z
aload 0
aload 1
invokespecial com/a/b/d/bc/add(Ljava/lang/Object;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final volatile synthetic addAll(Ljava/util/Collection;)Z
aload 0
aload 1
invokespecial com/a/b/d/bc/addAll(Ljava/util/Collection;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final b(Ljava/lang/Object;I)I
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
.catch java/lang/ClassCastException from L0 to L1 using L2
.catch java/lang/NullPointerException from L0 to L1 using L3
.catch java/lang/ClassCastException from L4 to L5 using L2
.catch java/lang/NullPointerException from L4 to L5 using L3
iload 2
ldc "occurrences"
invokestatic com/a/b/d/cl/a(ILjava/lang/String;)I
pop
iload 2
ifne L6
aload 0
aload 1
invokevirtual com/a/b/d/aer/a(Ljava/lang/Object;)I
ireturn
L6:
aload 0
getfield com/a/b/d/aer/b Lcom/a/b/d/afa;
getfield com/a/b/d/afa/a Ljava/lang/Object;
checkcast com/a/b/d/aez
astore 3
iconst_1
newarray int
astore 4
L0:
aload 0
getfield com/a/b/d/aer/c Lcom/a/b/d/hs;
aload 1
invokevirtual com/a/b/d/hs/c(Ljava/lang/Object;)Z
ifeq L7
L1:
aload 3
ifnonnull L4
goto L7
L4:
aload 3
aload 0
invokevirtual com/a/b/d/aer/comparator()Ljava/util/Comparator;
aload 1
iload 2
aload 4
invokevirtual com/a/b/d/aez/b(Ljava/util/Comparator;Ljava/lang/Object;I[I)Lcom/a/b/d/aez;
astore 1
L5:
aload 0
getfield com/a/b/d/aer/b Lcom/a/b/d/afa;
aload 3
aload 1
invokevirtual com/a/b/d/afa/a(Ljava/lang/Object;Ljava/lang/Object;)V
aload 4
iconst_0
iaload
ireturn
L2:
astore 1
iconst_0
ireturn
L3:
astore 1
iconst_0
ireturn
L7:
iconst_0
ireturn
.limit locals 5
.limit stack 5
.end method

.method final b()Ljava/util/Iterator;
new com/a/b/d/aet
dup
aload 0
invokespecial com/a/b/d/aet/<init>(Lcom/a/b/d/aer;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method final c()I
aload 0
getstatic com/a/b/d/aew/b Lcom/a/b/d/aew;
invokespecial com/a/b/d/aer/a(Lcom/a/b/d/aew;)J
invokestatic com/a/b/l/q/b(J)I
ireturn
.limit locals 1
.limit stack 2
.end method

.method public final c(Ljava/lang/Object;I)I
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
iload 2
ldc "count"
invokestatic com/a/b/d/cl/a(ILjava/lang/String;)I
pop
aload 0
getfield com/a/b/d/aer/c Lcom/a/b/d/hs;
aload 1
invokevirtual com/a/b/d/hs/c(Ljava/lang/Object;)Z
ifne L0
iload 2
ifne L1
iconst_1
istore 3
L2:
iload 3
invokestatic com/a/b/b/cn/a(Z)V
L3:
iconst_0
ireturn
L1:
iconst_0
istore 3
goto L2
L0:
aload 0
getfield com/a/b/d/aer/b Lcom/a/b/d/afa;
getfield com/a/b/d/afa/a Ljava/lang/Object;
checkcast com/a/b/d/aez
astore 4
aload 4
ifnonnull L4
iload 2
ifle L3
aload 0
aload 1
iload 2
invokevirtual com/a/b/d/aer/a(Ljava/lang/Object;I)I
pop
iconst_0
ireturn
L4:
iconst_1
newarray int
astore 5
aload 4
aload 0
invokevirtual com/a/b/d/aer/comparator()Ljava/util/Comparator;
aload 1
iload 2
aload 5
invokevirtual com/a/b/d/aez/c(Ljava/util/Comparator;Ljava/lang/Object;I[I)Lcom/a/b/d/aez;
astore 1
aload 0
getfield com/a/b/d/aer/b Lcom/a/b/d/afa;
aload 4
aload 1
invokevirtual com/a/b/d/afa/a(Ljava/lang/Object;Ljava/lang/Object;)V
aload 5
iconst_0
iaload
ireturn
.limit locals 6
.limit stack 5
.end method

.method public final c(Ljava/lang/Object;Lcom/a/b/d/ce;)Lcom/a/b/d/abn;
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
new com/a/b/d/aer
dup
aload 0
getfield com/a/b/d/aer/b Lcom/a/b/d/afa;
aload 0
getfield com/a/b/d/aer/c Lcom/a/b/d/hs;
new com/a/b/d/hs
dup
aload 0
invokevirtual com/a/b/d/aer/comparator()Ljava/util/Comparator;
iconst_1
aload 1
aload 2
iconst_0
aconst_null
getstatic com/a/b/d/ce/a Lcom/a/b/d/ce;
invokespecial com/a/b/d/hs/<init>(Ljava/util/Comparator;ZLjava/lang/Object;Lcom/a/b/d/ce;ZLjava/lang/Object;Lcom/a/b/d/ce;)V
invokevirtual com/a/b/d/hs/a(Lcom/a/b/d/hs;)Lcom/a/b/d/hs;
aload 0
getfield com/a/b/d/aer/d Lcom/a/b/d/aez;
invokespecial com/a/b/d/aer/<init>(Lcom/a/b/d/afa;Lcom/a/b/d/hs;Lcom/a/b/d/aez;)V
areturn
.limit locals 3
.limit stack 13
.end method

.method public final volatile synthetic clear()V
aload 0
invokespecial com/a/b/d/bc/clear()V
return
.limit locals 1
.limit stack 1
.end method

.method public final volatile synthetic comparator()Ljava/util/Comparator;
aload 0
invokespecial com/a/b/d/bc/comparator()Ljava/util/Comparator;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final volatile synthetic contains(Ljava/lang/Object;)Z
aload 0
aload 1
invokespecial com/a/b/d/bc/contains(Ljava/lang/Object;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final d(Ljava/lang/Object;Lcom/a/b/d/ce;)Lcom/a/b/d/abn;
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
new com/a/b/d/aer
dup
aload 0
getfield com/a/b/d/aer/b Lcom/a/b/d/afa;
aload 0
getfield com/a/b/d/aer/c Lcom/a/b/d/hs;
new com/a/b/d/hs
dup
aload 0
invokevirtual com/a/b/d/aer/comparator()Ljava/util/Comparator;
iconst_0
aconst_null
getstatic com/a/b/d/ce/a Lcom/a/b/d/ce;
iconst_1
aload 1
aload 2
invokespecial com/a/b/d/hs/<init>(Ljava/util/Comparator;ZLjava/lang/Object;Lcom/a/b/d/ce;ZLjava/lang/Object;Lcom/a/b/d/ce;)V
invokevirtual com/a/b/d/hs/a(Lcom/a/b/d/hs;)Lcom/a/b/d/hs;
aload 0
getfield com/a/b/d/aer/d Lcom/a/b/d/aez;
invokespecial com/a/b/d/aer/<init>(Lcom/a/b/d/afa;Lcom/a/b/d/hs;Lcom/a/b/d/aez;)V
areturn
.limit locals 3
.limit stack 13
.end method

.method public final volatile synthetic e_()Ljava/util/NavigableSet;
aload 0
invokespecial com/a/b/d/bc/e_()Ljava/util/NavigableSet;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final volatile synthetic equals(Ljava/lang/Object;)Z
aload 0
aload 1
invokespecial com/a/b/d/bc/equals(Ljava/lang/Object;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final volatile synthetic h()Lcom/a/b/d/xd;
aload 0
invokespecial com/a/b/d/bc/h()Lcom/a/b/d/xd;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final volatile synthetic hashCode()I
aload 0
invokespecial com/a/b/d/bc/hashCode()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final volatile synthetic i()Lcom/a/b/d/xd;
aload 0
invokespecial com/a/b/d/bc/i()Lcom/a/b/d/xd;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final volatile synthetic isEmpty()Z
aload 0
invokespecial com/a/b/d/bc/isEmpty()Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final volatile synthetic iterator()Ljava/util/Iterator;
aload 0
invokespecial com/a/b/d/bc/iterator()Ljava/util/Iterator;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final volatile synthetic j()Lcom/a/b/d/xd;
aload 0
invokespecial com/a/b/d/bc/j()Lcom/a/b/d/xd;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final volatile synthetic k()Lcom/a/b/d/xd;
aload 0
invokespecial com/a/b/d/bc/k()Lcom/a/b/d/xd;
areturn
.limit locals 1
.limit stack 1
.end method

.method final l()Ljava/util/Iterator;
new com/a/b/d/aeu
dup
aload 0
invokespecial com/a/b/d/aeu/<init>(Lcom/a/b/d/aer;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method public final volatile synthetic m()Lcom/a/b/d/abn;
aload 0
invokespecial com/a/b/d/bc/m()Lcom/a/b/d/abn;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final volatile synthetic remove(Ljava/lang/Object;)Z
aload 0
aload 1
invokespecial com/a/b/d/bc/remove(Ljava/lang/Object;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final volatile synthetic removeAll(Ljava/util/Collection;)Z
aload 0
aload 1
invokespecial com/a/b/d/bc/removeAll(Ljava/util/Collection;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final volatile synthetic retainAll(Ljava/util/Collection;)Z
aload 0
aload 1
invokespecial com/a/b/d/bc/retainAll(Ljava/util/Collection;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final size()I
aload 0
getstatic com/a/b/d/aew/a Lcom/a/b/d/aew;
invokespecial com/a/b/d/aer/a(Lcom/a/b/d/aew;)J
invokestatic com/a/b/l/q/b(J)I
ireturn
.limit locals 1
.limit stack 2
.end method

.method public final volatile synthetic toString()Ljava/lang/String;
aload 0
invokespecial com/a/b/d/bc/toString()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method
