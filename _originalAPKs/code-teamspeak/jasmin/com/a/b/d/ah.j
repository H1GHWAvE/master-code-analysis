.bytecode 50.0
.class synchronized com/a/b/d/ah
.super com/a/b/d/ab
.implements java/util/SortedSet

.field final synthetic 'g' Lcom/a/b/d/n;

.method <init>(Lcom/a/b/d/n;Ljava/lang/Object;Ljava/util/SortedSet;Lcom/a/b/d/ab;)V
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
.annotation visibleparam 4 Ljavax/annotation/Nullable;
.end annotation
aload 0
aload 1
putfield com/a/b/d/ah/g Lcom/a/b/d/n;
aload 0
aload 1
aload 2
aload 3
aload 4
invokespecial com/a/b/d/ab/<init>(Lcom/a/b/d/n;Ljava/lang/Object;Ljava/util/Collection;Lcom/a/b/d/ab;)V
return
.limit locals 5
.limit stack 5
.end method

.method public comparator()Ljava/util/Comparator;
aload 0
invokevirtual com/a/b/d/ah/d()Ljava/util/SortedSet;
invokeinterface java/util/SortedSet/comparator()Ljava/util/Comparator; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method d()Ljava/util/SortedSet;
aload 0
getfield com/a/b/d/ab/c Ljava/util/Collection;
checkcast java/util/SortedSet
areturn
.limit locals 1
.limit stack 1
.end method

.method public first()Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/ah/a()V
aload 0
invokevirtual com/a/b/d/ah/d()Ljava/util/SortedSet;
invokeinterface java/util/SortedSet/first()Ljava/lang/Object; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method public headSet(Ljava/lang/Object;)Ljava/util/SortedSet;
aload 0
invokevirtual com/a/b/d/ah/a()V
aload 0
getfield com/a/b/d/ah/g Lcom/a/b/d/n;
astore 2
aload 0
getfield com/a/b/d/ab/b Ljava/lang/Object;
astore 3
aload 0
invokevirtual com/a/b/d/ah/d()Ljava/util/SortedSet;
aload 1
invokeinterface java/util/SortedSet/headSet(Ljava/lang/Object;)Ljava/util/SortedSet; 1
astore 4
aload 0
getfield com/a/b/d/ab/d Lcom/a/b/d/ab;
ifnonnull L0
aload 0
astore 1
L1:
new com/a/b/d/ah
dup
aload 2
aload 3
aload 4
aload 1
invokespecial com/a/b/d/ah/<init>(Lcom/a/b/d/n;Ljava/lang/Object;Ljava/util/SortedSet;Lcom/a/b/d/ab;)V
areturn
L0:
aload 0
getfield com/a/b/d/ab/d Lcom/a/b/d/ab;
astore 1
goto L1
.limit locals 5
.limit stack 6
.end method

.method public last()Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/ah/a()V
aload 0
invokevirtual com/a/b/d/ah/d()Ljava/util/SortedSet;
invokeinterface java/util/SortedSet/last()Ljava/lang/Object; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method public subSet(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedSet;
aload 0
invokevirtual com/a/b/d/ah/a()V
aload 0
getfield com/a/b/d/ah/g Lcom/a/b/d/n;
astore 3
aload 0
getfield com/a/b/d/ab/b Ljava/lang/Object;
astore 4
aload 0
invokevirtual com/a/b/d/ah/d()Ljava/util/SortedSet;
aload 1
aload 2
invokeinterface java/util/SortedSet/subSet(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedSet; 2
astore 2
aload 0
getfield com/a/b/d/ab/d Lcom/a/b/d/ab;
ifnonnull L0
aload 0
astore 1
L1:
new com/a/b/d/ah
dup
aload 3
aload 4
aload 2
aload 1
invokespecial com/a/b/d/ah/<init>(Lcom/a/b/d/n;Ljava/lang/Object;Ljava/util/SortedSet;Lcom/a/b/d/ab;)V
areturn
L0:
aload 0
getfield com/a/b/d/ab/d Lcom/a/b/d/ab;
astore 1
goto L1
.limit locals 5
.limit stack 6
.end method

.method public tailSet(Ljava/lang/Object;)Ljava/util/SortedSet;
aload 0
invokevirtual com/a/b/d/ah/a()V
aload 0
getfield com/a/b/d/ah/g Lcom/a/b/d/n;
astore 2
aload 0
getfield com/a/b/d/ab/b Ljava/lang/Object;
astore 3
aload 0
invokevirtual com/a/b/d/ah/d()Ljava/util/SortedSet;
aload 1
invokeinterface java/util/SortedSet/tailSet(Ljava/lang/Object;)Ljava/util/SortedSet; 1
astore 4
aload 0
getfield com/a/b/d/ab/d Lcom/a/b/d/ab;
ifnonnull L0
aload 0
astore 1
L1:
new com/a/b/d/ah
dup
aload 2
aload 3
aload 4
aload 1
invokespecial com/a/b/d/ah/<init>(Lcom/a/b/d/n;Ljava/lang/Object;Ljava/util/SortedSet;Lcom/a/b/d/ab;)V
areturn
L0:
aload 0
getfield com/a/b/d/ab/d Lcom/a/b/d/ab;
astore 1
goto L1
.limit locals 5
.limit stack 6
.end method
