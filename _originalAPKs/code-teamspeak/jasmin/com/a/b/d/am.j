.bytecode 50.0
.class synchronized abstract com/a/b/d/am
.super java/lang/Object
.implements java/util/Map$Entry
.annotation invisible Lcom/a/b/a/b;
.end annotation

.method <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public equals(Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
iconst_0
istore 3
iload 3
istore 2
aload 1
instanceof java/util/Map$Entry
ifeq L0
aload 1
checkcast java/util/Map$Entry
astore 1
iload 3
istore 2
aload 0
invokevirtual com/a/b/d/am/getKey()Ljava/lang/Object;
aload 1
invokeinterface java/util/Map$Entry/getKey()Ljava/lang/Object; 0
invokestatic com/a/b/b/ce/a(Ljava/lang/Object;Ljava/lang/Object;)Z
ifeq L0
iload 3
istore 2
aload 0
invokevirtual com/a/b/d/am/getValue()Ljava/lang/Object;
aload 1
invokeinterface java/util/Map$Entry/getValue()Ljava/lang/Object; 0
invokestatic com/a/b/b/ce/a(Ljava/lang/Object;Ljava/lang/Object;)Z
ifeq L0
iconst_1
istore 2
L0:
iload 2
ireturn
.limit locals 4
.limit stack 2
.end method

.method public abstract getKey()Ljava/lang/Object;
.end method

.method public abstract getValue()Ljava/lang/Object;
.end method

.method public hashCode()I
iconst_0
istore 2
aload 0
invokevirtual com/a/b/d/am/getKey()Ljava/lang/Object;
astore 3
aload 0
invokevirtual com/a/b/d/am/getValue()Ljava/lang/Object;
astore 4
aload 3
ifnonnull L0
iconst_0
istore 1
L1:
aload 4
ifnonnull L2
L3:
iload 2
iload 1
ixor
ireturn
L0:
aload 3
invokevirtual java/lang/Object/hashCode()I
istore 1
goto L1
L2:
aload 4
invokevirtual java/lang/Object/hashCode()I
istore 2
goto L3
.limit locals 5
.limit stack 2
.end method

.method public setValue(Ljava/lang/Object;)Ljava/lang/Object;
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
.limit locals 2
.limit stack 2
.end method

.method public toString()Ljava/lang/String;
aload 0
invokevirtual com/a/b/d/am/getKey()Ljava/lang/Object;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 1
aload 0
invokevirtual com/a/b/d/am/getValue()Ljava/lang/Object;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 2
new java/lang/StringBuilder
dup
aload 1
invokevirtual java/lang/String/length()I
iconst_1
iadd
aload 2
invokevirtual java/lang/String/length()I
iadd
invokespecial java/lang/StringBuilder/<init>(I)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 3
.limit stack 4
.end method
