.bytecode 50.0
.class final synchronized com/a/b/n/a/cm
.super java/lang/Object
.implements java/util/concurrent/Future

.field final synthetic 'a' Ljava/util/concurrent/Future;

.field final synthetic 'b' Lcom/a/b/b/bj;

.method <init>(Ljava/util/concurrent/Future;Lcom/a/b/b/bj;)V
aload 0
aload 1
putfield com/a/b/n/a/cm/a Ljava/util/concurrent/Future;
aload 0
aload 2
putfield com/a/b/n/a/cm/b Lcom/a/b/b/bj;
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 3
.limit stack 2
.end method

.method private a(Ljava/lang/Object;)Ljava/lang/Object;
.catch java/lang/Throwable from L0 to L1 using L2
L0:
aload 0
getfield com/a/b/n/a/cm/b Lcom/a/b/b/bj;
aload 1
invokeinterface com/a/b/b/bj/e(Ljava/lang/Object;)Ljava/lang/Object; 1
astore 1
L1:
aload 1
areturn
L2:
astore 1
new java/util/concurrent/ExecutionException
dup
aload 1
invokespecial java/util/concurrent/ExecutionException/<init>(Ljava/lang/Throwable;)V
athrow
.limit locals 2
.limit stack 3
.end method

.method public final cancel(Z)Z
aload 0
getfield com/a/b/n/a/cm/a Ljava/util/concurrent/Future;
iload 1
invokeinterface java/util/concurrent/Future/cancel(Z)Z 1
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final get()Ljava/lang/Object;
aload 0
aload 0
getfield com/a/b/n/a/cm/a Ljava/util/concurrent/Future;
invokeinterface java/util/concurrent/Future/get()Ljava/lang/Object; 0
invokespecial com/a/b/n/a/cm/a(Ljava/lang/Object;)Ljava/lang/Object;
areturn
.limit locals 1
.limit stack 2
.end method

.method public final get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
aload 0
aload 0
getfield com/a/b/n/a/cm/a Ljava/util/concurrent/Future;
lload 1
aload 3
invokeinterface java/util/concurrent/Future/get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object; 3
invokespecial com/a/b/n/a/cm/a(Ljava/lang/Object;)Ljava/lang/Object;
areturn
.limit locals 4
.limit stack 5
.end method

.method public final isCancelled()Z
aload 0
getfield com/a/b/n/a/cm/a Ljava/util/concurrent/Future;
invokeinterface java/util/concurrent/Future/isCancelled()Z 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final isDone()Z
aload 0
getfield com/a/b/n/a/cm/a Ljava/util/concurrent/Future;
invokeinterface java/util/concurrent/Future/isDone()Z 0
ireturn
.limit locals 1
.limit stack 1
.end method
