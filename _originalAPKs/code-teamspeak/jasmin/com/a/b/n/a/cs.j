.bytecode 50.0
.class final synchronized com/a/b/n/a/cs
.super com/a/b/n/a/g
.implements java/lang/Runnable

.field private 'b' Lcom/a/b/n/a/ap;

.field private 'c' Lcom/a/b/n/a/dp;

.field private volatile 'd' Lcom/a/b/n/a/dp;

.method private <init>(Lcom/a/b/n/a/ap;Lcom/a/b/n/a/dp;)V
aload 0
invokespecial com/a/b/n/a/g/<init>()V
aload 0
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast com/a/b/n/a/ap
putfield com/a/b/n/a/cs/b Lcom/a/b/n/a/ap;
aload 0
aload 2
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast com/a/b/n/a/dp
putfield com/a/b/n/a/cs/c Lcom/a/b/n/a/dp;
return
.limit locals 3
.limit stack 2
.end method

.method synthetic <init>(Lcom/a/b/n/a/ap;Lcom/a/b/n/a/dp;B)V
aload 0
aload 1
aload 2
invokespecial com/a/b/n/a/cs/<init>(Lcom/a/b/n/a/ap;Lcom/a/b/n/a/dp;)V
return
.limit locals 4
.limit stack 3
.end method

.method static synthetic a(Lcom/a/b/n/a/cs;)Lcom/a/b/n/a/dp;
aload 0
aconst_null
putfield com/a/b/n/a/cs/d Lcom/a/b/n/a/dp;
aconst_null
areturn
.limit locals 1
.limit stack 2
.end method

.method private static a(Ljava/util/concurrent/Future;Z)V
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
ifnull L0
aload 0
iload 1
invokeinterface java/util/concurrent/Future/cancel(Z)Z 1
pop
L0:
return
.limit locals 2
.limit stack 2
.end method

.method public final cancel(Z)Z
aload 0
iload 1
invokespecial com/a/b/n/a/g/cancel(Z)Z
ifeq L0
aload 0
getfield com/a/b/n/a/cs/c Lcom/a/b/n/a/dp;
iload 1
invokestatic com/a/b/n/a/cs/a(Ljava/util/concurrent/Future;Z)V
aload 0
getfield com/a/b/n/a/cs/d Lcom/a/b/n/a/dp;
iload 1
invokestatic com/a/b/n/a/cs/a(Ljava/util/concurrent/Future;Z)V
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final run()V
.catch java/util/concurrent/CancellationException from L0 to L1 using L2
.catch java/util/concurrent/ExecutionException from L0 to L1 using L3
.catch java/lang/reflect/UndeclaredThrowableException from L0 to L1 using L4
.catch java/lang/Throwable from L0 to L1 using L5
.catch all from L0 to L1 using L6
.catch java/lang/reflect/UndeclaredThrowableException from L1 to L7 using L4
.catch java/lang/Throwable from L1 to L7 using L5
.catch all from L1 to L7 using L6
.catch java/lang/reflect/UndeclaredThrowableException from L8 to L9 using L4
.catch java/lang/Throwable from L8 to L9 using L5
.catch all from L8 to L9 using L6
.catch java/lang/reflect/UndeclaredThrowableException from L10 to L11 using L4
.catch java/lang/Throwable from L10 to L11 using L5
.catch all from L10 to L11 using L6
.catch java/lang/reflect/UndeclaredThrowableException from L12 to L13 using L4
.catch java/lang/Throwable from L12 to L13 using L5
.catch all from L12 to L13 using L6
.catch all from L14 to L15 using L6
.catch all from L16 to L17 using L6
L0:
aload 0
getfield com/a/b/n/a/cs/c Lcom/a/b/n/a/dp;
invokestatic com/a/b/n/a/gs/a(Ljava/util/concurrent/Future;)Ljava/lang/Object;
astore 1
L1:
aload 0
getfield com/a/b/n/a/cs/b Lcom/a/b/n/a/ap;
aload 1
invokeinterface com/a/b/n/a/ap/a(Ljava/lang/Object;)Lcom/a/b/n/a/dp; 1
ldc "AsyncFunction may not return null."
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
checkcast com/a/b/n/a/dp
astore 1
aload 0
aload 1
putfield com/a/b/n/a/cs/d Lcom/a/b/n/a/dp;
aload 0
invokevirtual com/a/b/n/a/cs/isCancelled()Z
ifeq L12
aload 1
aload 0
getfield com/a/b/n/a/g/a Lcom/a/b/n/a/h;
invokevirtual com/a/b/n/a/h/d()Z
invokeinterface com/a/b/n/a/dp/cancel(Z)Z 1
pop
aload 0
aconst_null
putfield com/a/b/n/a/cs/d Lcom/a/b/n/a/dp;
L7:
aload 0
aconst_null
putfield com/a/b/n/a/cs/b Lcom/a/b/n/a/ap;
aload 0
aconst_null
putfield com/a/b/n/a/cs/c Lcom/a/b/n/a/dp;
return
L2:
astore 1
L8:
aload 0
iconst_0
invokevirtual com/a/b/n/a/cs/cancel(Z)Z
pop
L9:
aload 0
aconst_null
putfield com/a/b/n/a/cs/b Lcom/a/b/n/a/ap;
aload 0
aconst_null
putfield com/a/b/n/a/cs/c Lcom/a/b/n/a/dp;
return
L3:
astore 1
L10:
aload 0
aload 1
invokevirtual java/util/concurrent/ExecutionException/getCause()Ljava/lang/Throwable;
invokevirtual com/a/b/n/a/cs/a(Ljava/lang/Throwable;)Z
pop
L11:
aload 0
aconst_null
putfield com/a/b/n/a/cs/b Lcom/a/b/n/a/ap;
aload 0
aconst_null
putfield com/a/b/n/a/cs/c Lcom/a/b/n/a/dp;
return
L12:
aload 1
new com/a/b/n/a/ct
dup
aload 0
aload 1
invokespecial com/a/b/n/a/ct/<init>(Lcom/a/b/n/a/cs;Lcom/a/b/n/a/dp;)V
getstatic com/a/b/n/a/ef/a Lcom/a/b/n/a/ef;
invokeinterface com/a/b/n/a/dp/a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V 2
L13:
aload 0
aconst_null
putfield com/a/b/n/a/cs/b Lcom/a/b/n/a/ap;
aload 0
aconst_null
putfield com/a/b/n/a/cs/c Lcom/a/b/n/a/dp;
return
L4:
astore 1
L14:
aload 0
aload 1
invokevirtual java/lang/reflect/UndeclaredThrowableException/getCause()Ljava/lang/Throwable;
invokevirtual com/a/b/n/a/cs/a(Ljava/lang/Throwable;)Z
pop
L15:
aload 0
aconst_null
putfield com/a/b/n/a/cs/b Lcom/a/b/n/a/ap;
aload 0
aconst_null
putfield com/a/b/n/a/cs/c Lcom/a/b/n/a/dp;
return
L5:
astore 1
L16:
aload 0
aload 1
invokevirtual com/a/b/n/a/cs/a(Ljava/lang/Throwable;)Z
pop
L17:
aload 0
aconst_null
putfield com/a/b/n/a/cs/b Lcom/a/b/n/a/ap;
aload 0
aconst_null
putfield com/a/b/n/a/cs/c Lcom/a/b/n/a/dp;
return
L6:
astore 1
aload 0
aconst_null
putfield com/a/b/n/a/cs/b Lcom/a/b/n/a/ap;
aload 0
aconst_null
putfield com/a/b/n/a/cs/c Lcom/a/b/n/a/dp;
aload 1
athrow
.limit locals 2
.limit stack 5
.end method
