.bytecode 50.0
.class final synchronized com/a/b/n/a/ds
.super java/lang/Object
.implements java/lang/Runnable

.field private static final 'a' Ljava/util/logging/Logger;

.field private final 'b' Ljava/lang/Object;

.field private final 'c' Ljava/util/concurrent/Executor;

.field private final 'd' Ljava/util/Queue;
.annotation invisible Ljavax/annotation/concurrent/GuardedBy;
value s = "this"
.end annotation
.end field

.field private 'e' Z
.annotation invisible Ljavax/annotation/concurrent/GuardedBy;
value s = "this"
.end annotation
.end field

.method static <clinit>()V
ldc com/a/b/n/a/ds
invokevirtual java/lang/Class/getName()Ljava/lang/String;
invokestatic java/util/logging/Logger/getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;
putstatic com/a/b/n/a/ds/a Ljava/util/logging/Logger;
return
.limit locals 0
.limit stack 1
.end method

.method <init>(Ljava/lang/Object;Ljava/util/concurrent/Executor;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
new java/util/ArrayDeque
dup
invokespecial java/util/ArrayDeque/<init>()V
putfield com/a/b/n/a/ds/d Ljava/util/Queue;
aload 0
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
putfield com/a/b/n/a/ds/b Ljava/lang/Object;
aload 0
aload 2
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/util/concurrent/Executor
putfield com/a/b/n/a/ds/c Ljava/util/concurrent/Executor;
return
.limit locals 3
.limit stack 3
.end method

.method final a()V
.catch all from L0 to L1 using L2
.catch all from L1 to L3 using L2
.catch java/lang/RuntimeException from L4 to L5 using L6
.catch all from L7 to L8 using L2
.catch all from L9 to L10 using L11
.catch all from L12 to L13 using L11
iconst_1
istore 1
aload 0
monitorenter
L0:
aload 0
getfield com/a/b/n/a/ds/e Z
ifne L14
aload 0
iconst_1
putfield com/a/b/n/a/ds/e Z
L1:
aload 0
monitorexit
L3:
iload 1
ifeq L5
L4:
aload 0
getfield com/a/b/n/a/ds/c Ljava/util/concurrent/Executor;
aload 0
invokeinterface java/util/concurrent/Executor/execute(Ljava/lang/Runnable;)V 1
L5:
return
L2:
astore 2
L7:
aload 0
monitorexit
L8:
aload 2
athrow
L6:
astore 2
aload 0
monitorenter
L9:
aload 0
iconst_0
putfield com/a/b/n/a/ds/e Z
aload 0
monitorexit
L10:
getstatic com/a/b/n/a/ds/a Ljava/util/logging/Logger;
astore 3
getstatic java/util/logging/Level/SEVERE Ljava/util/logging/Level;
astore 4
aload 0
getfield com/a/b/n/a/ds/b Ljava/lang/Object;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 5
aload 0
getfield com/a/b/n/a/ds/c Ljava/util/concurrent/Executor;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 6
aload 3
aload 4
new java/lang/StringBuilder
dup
aload 5
invokevirtual java/lang/String/length()I
bipush 42
iadd
aload 6
invokevirtual java/lang/String/length()I
iadd
invokespecial java/lang/StringBuilder/<init>(I)V
ldc "Exception while running callbacks for "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 5
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " on "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 6
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aload 2
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V
aload 2
athrow
L11:
astore 2
L12:
aload 0
monitorexit
L13:
aload 2
athrow
L14:
iconst_0
istore 1
goto L1
.limit locals 7
.limit stack 6
.end method

.method final a(Lcom/a/b/n/a/dt;)V
.catch all from L0 to L1 using L2
aload 0
monitorenter
L0:
aload 0
getfield com/a/b/n/a/ds/d Ljava/util/Queue;
aload 1
invokeinterface java/util/Queue/add(Ljava/lang/Object;)Z 1
pop
L1:
aload 0
monitorexit
return
L2:
astore 1
aload 0
monitorexit
aload 1
athrow
.limit locals 2
.limit stack 2
.end method

.method public final run()V
.catch all from L0 to L1 using L2
.catch all from L1 to L3 using L4
.catch all from L5 to L6 using L4
.catch all from L6 to L7 using L8
.catch all from L9 to L10 using L4
.catch java/lang/RuntimeException from L10 to L11 using L12
.catch all from L10 to L11 using L2
.catch all from L13 to L14 using L2
.catch all from L15 to L16 using L17
.catch all from L18 to L19 using L8
.catch all from L19 to L20 using L20
.catch all from L21 to L22 using L17
iconst_0
istore 2
iconst_1
istore 1
L0:
aload 0
monitorenter
L1:
aload 0
getfield com/a/b/n/a/ds/e Z
invokestatic com/a/b/b/cn/b(Z)V
aload 0
getfield com/a/b/n/a/ds/d Ljava/util/Queue;
invokeinterface java/util/Queue/poll()Ljava/lang/Object; 0
checkcast com/a/b/n/a/dt
astore 7
L3:
aload 7
ifnonnull L9
L5:
aload 0
iconst_0
putfield com/a/b/n/a/ds/e Z
L6:
aload 0
monitorexit
L7:
return
L9:
aload 0
monitorexit
L10:
aload 7
aload 0
getfield com/a/b/n/a/ds/b Ljava/lang/Object;
invokevirtual com/a/b/n/a/dt/a(Ljava/lang/Object;)V
L11:
goto L0
L12:
astore 3
L13:
getstatic com/a/b/n/a/ds/a Ljava/util/logging/Logger;
astore 4
getstatic java/util/logging/Level/SEVERE Ljava/util/logging/Level;
astore 5
aload 0
getfield com/a/b/n/a/ds/b Ljava/lang/Object;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 6
aload 7
getfield com/a/b/n/a/dt/d Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 7
aload 4
aload 5
new java/lang/StringBuilder
dup
aload 6
invokevirtual java/lang/String/length()I
bipush 37
iadd
aload 7
invokevirtual java/lang/String/length()I
iadd
invokespecial java/lang/StringBuilder/<init>(I)V
ldc "Exception while executing callback: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 6
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "."
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 7
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aload 3
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V
L14:
goto L0
L2:
astore 3
L23:
iload 1
ifeq L16
aload 0
monitorenter
L15:
aload 0
iconst_0
putfield com/a/b/n/a/ds/e Z
aload 0
monitorexit
L16:
aload 3
athrow
L4:
astore 3
iconst_1
istore 1
L24:
iload 1
istore 2
L18:
aload 0
monitorexit
L19:
aload 3
athrow
L20:
astore 3
goto L23
L17:
astore 3
L21:
aload 0
monitorexit
L22:
aload 3
athrow
L8:
astore 3
iload 2
istore 1
goto L24
.limit locals 8
.limit stack 6
.end method
