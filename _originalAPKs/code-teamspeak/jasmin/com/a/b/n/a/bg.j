.bytecode 50.0
.class final synchronized com/a/b/n/a/bg
.super java/util/concurrent/locks/ReentrantLock
.implements com/a/b/n/a/bf

.field final synthetic 'a' Lcom/a/b/n/a/bd;

.field private final 'b' Lcom/a/b/n/a/bl;

.method private <init>(Lcom/a/b/n/a/bd;Lcom/a/b/n/a/bl;)V
aload 0
aload 1
putfield com/a/b/n/a/bg/a Lcom/a/b/n/a/bd;
aload 0
iconst_0
invokespecial java/util/concurrent/locks/ReentrantLock/<init>(Z)V
aload 0
aload 2
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast com/a/b/n/a/bl
putfield com/a/b/n/a/bg/b Lcom/a/b/n/a/bl;
return
.limit locals 3
.limit stack 2
.end method

.method synthetic <init>(Lcom/a/b/n/a/bd;Lcom/a/b/n/a/bl;B)V
aload 0
aload 1
aload 2
invokespecial com/a/b/n/a/bg/<init>(Lcom/a/b/n/a/bd;Lcom/a/b/n/a/bl;)V
return
.limit locals 4
.limit stack 3
.end method

.method public final a()Lcom/a/b/n/a/bl;
aload 0
getfield com/a/b/n/a/bg/b Lcom/a/b/n/a/bl;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final b()Z
aload 0
invokevirtual com/a/b/n/a/bg/isHeldByCurrentThread()Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final lock()V
.catch all from L0 to L1 using L2
aload 0
getfield com/a/b/n/a/bg/a Lcom/a/b/n/a/bd;
aload 0
invokestatic com/a/b/n/a/bd/a(Lcom/a/b/n/a/bd;Lcom/a/b/n/a/bf;)V
L0:
aload 0
invokespecial java/util/concurrent/locks/ReentrantLock/lock()V
L1:
aload 0
invokestatic com/a/b/n/a/bd/a(Lcom/a/b/n/a/bf;)V
return
L2:
astore 1
aload 0
invokestatic com/a/b/n/a/bd/a(Lcom/a/b/n/a/bf;)V
aload 1
athrow
.limit locals 2
.limit stack 2
.end method

.method public final lockInterruptibly()V
.catch all from L0 to L1 using L2
aload 0
getfield com/a/b/n/a/bg/a Lcom/a/b/n/a/bd;
aload 0
invokestatic com/a/b/n/a/bd/a(Lcom/a/b/n/a/bd;Lcom/a/b/n/a/bf;)V
L0:
aload 0
invokespecial java/util/concurrent/locks/ReentrantLock/lockInterruptibly()V
L1:
aload 0
invokestatic com/a/b/n/a/bd/a(Lcom/a/b/n/a/bf;)V
return
L2:
astore 1
aload 0
invokestatic com/a/b/n/a/bd/a(Lcom/a/b/n/a/bf;)V
aload 1
athrow
.limit locals 2
.limit stack 2
.end method

.method public final tryLock()Z
.catch all from L0 to L1 using L2
aload 0
getfield com/a/b/n/a/bg/a Lcom/a/b/n/a/bd;
aload 0
invokestatic com/a/b/n/a/bd/a(Lcom/a/b/n/a/bd;Lcom/a/b/n/a/bf;)V
L0:
aload 0
invokespecial java/util/concurrent/locks/ReentrantLock/tryLock()Z
istore 1
L1:
aload 0
invokestatic com/a/b/n/a/bd/a(Lcom/a/b/n/a/bf;)V
iload 1
ireturn
L2:
astore 2
aload 0
invokestatic com/a/b/n/a/bd/a(Lcom/a/b/n/a/bf;)V
aload 2
athrow
.limit locals 3
.limit stack 2
.end method

.method public final tryLock(JLjava/util/concurrent/TimeUnit;)Z
.catch all from L0 to L1 using L2
aload 0
getfield com/a/b/n/a/bg/a Lcom/a/b/n/a/bd;
aload 0
invokestatic com/a/b/n/a/bd/a(Lcom/a/b/n/a/bd;Lcom/a/b/n/a/bf;)V
L0:
aload 0
lload 1
aload 3
invokespecial java/util/concurrent/locks/ReentrantLock/tryLock(JLjava/util/concurrent/TimeUnit;)Z
istore 4
L1:
aload 0
invokestatic com/a/b/n/a/bd/a(Lcom/a/b/n/a/bf;)V
iload 4
ireturn
L2:
astore 3
aload 0
invokestatic com/a/b/n/a/bd/a(Lcom/a/b/n/a/bf;)V
aload 3
athrow
.limit locals 5
.limit stack 4
.end method

.method public final unlock()V
.catch all from L0 to L1 using L2
L0:
aload 0
invokespecial java/util/concurrent/locks/ReentrantLock/unlock()V
L1:
aload 0
invokestatic com/a/b/n/a/bd/a(Lcom/a/b/n/a/bf;)V
return
L2:
astore 1
aload 0
invokestatic com/a/b/n/a/bd/a(Lcom/a/b/n/a/bf;)V
aload 1
athrow
.limit locals 2
.limit stack 1
.end method
