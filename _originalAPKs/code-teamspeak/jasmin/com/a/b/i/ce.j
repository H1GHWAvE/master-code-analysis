.bytecode 50.0
.class final synchronized com/a/b/i/ce
.super java/io/Reader

.field private final 'a' Ljava/util/Iterator;

.field private 'b' Ljava/io/Reader;

.method <init>(Ljava/util/Iterator;)V
aload 0
invokespecial java/io/Reader/<init>()V
aload 0
aload 1
putfield com/a/b/i/ce/a Ljava/util/Iterator;
aload 0
invokespecial com/a/b/i/ce/a()V
return
.limit locals 2
.limit stack 2
.end method

.method private a()V
aload 0
invokevirtual com/a/b/i/ce/close()V
aload 0
getfield com/a/b/i/ce/a Ljava/util/Iterator;
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L0
aload 0
aload 0
getfield com/a/b/i/ce/a Ljava/util/Iterator;
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/a/b/i/ah
invokevirtual com/a/b/i/ah/a()Ljava/io/Reader;
putfield com/a/b/i/ce/b Ljava/io/Reader;
L0:
return
.limit locals 1
.limit stack 2
.end method

.method public final close()V
.catch all from L0 to L1 using L2
aload 0
getfield com/a/b/i/ce/b Ljava/io/Reader;
ifnull L3
L0:
aload 0
getfield com/a/b/i/ce/b Ljava/io/Reader;
invokevirtual java/io/Reader/close()V
L1:
aload 0
aconst_null
putfield com/a/b/i/ce/b Ljava/io/Reader;
L3:
return
L2:
astore 1
aload 0
aconst_null
putfield com/a/b/i/ce/b Ljava/io/Reader;
aload 1
athrow
.limit locals 2
.limit stack 2
.end method

.method public final read([CII)I
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
L0:
aload 0
getfield com/a/b/i/ce/b Ljava/io/Reader;
ifnonnull L1
iconst_m1
ireturn
L1:
aload 0
getfield com/a/b/i/ce/b Ljava/io/Reader;
aload 1
iload 2
iload 3
invokevirtual java/io/Reader/read([CII)I
istore 4
iload 4
iconst_m1
if_icmpne L2
aload 0
invokespecial com/a/b/i/ce/a()V
goto L0
L2:
iload 4
ireturn
.limit locals 5
.limit stack 4
.end method

.method public final ready()Z
aload 0
getfield com/a/b/i/ce/b Ljava/io/Reader;
ifnull L0
aload 0
getfield com/a/b/i/ce/b Ljava/io/Reader;
invokevirtual java/io/Reader/ready()Z
ifeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final skip(J)J
lload 1
lconst_0
lcmp
iflt L0
iconst_1
istore 5
L1:
iload 5
ldc "n is negative"
invokestatic com/a/b/b/cn/a(ZLjava/lang/Object;)V
lload 1
lconst_0
lcmp
ifle L2
L3:
aload 0
getfield com/a/b/i/ce/b Ljava/io/Reader;
ifnull L2
aload 0
getfield com/a/b/i/ce/b Ljava/io/Reader;
lload 1
invokevirtual java/io/Reader/skip(J)J
lstore 3
lload 3
lconst_0
lcmp
ifle L4
lload 3
lreturn
L0:
iconst_0
istore 5
goto L1
L4:
aload 0
invokespecial com/a/b/i/ce/a()V
goto L3
L2:
lconst_0
lreturn
.limit locals 6
.limit stack 4
.end method
