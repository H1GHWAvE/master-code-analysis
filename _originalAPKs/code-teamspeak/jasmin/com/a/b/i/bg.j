.bytecode 50.0
.class final synchronized com/a/b/i/bg
.super com/a/b/i/s

.field private final 'a' Ljava/io/File;

.method private <init>(Ljava/io/File;)V
aload 0
invokespecial com/a/b/i/s/<init>()V
aload 0
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/io/File
putfield com/a/b/i/bg/a Ljava/io/File;
return
.limit locals 2
.limit stack 2
.end method

.method synthetic <init>(Ljava/io/File;B)V
aload 0
aload 1
invokespecial com/a/b/i/bg/<init>(Ljava/io/File;)V
return
.limit locals 3
.limit stack 2
.end method

.method private f()Ljava/io/FileInputStream;
new java/io/FileInputStream
dup
aload 0
getfield com/a/b/i/bg/a Ljava/io/File;
invokespecial java/io/FileInputStream/<init>(Ljava/io/File;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method public final synthetic a()Ljava/io/InputStream;
aload 0
invokespecial com/a/b/i/bg/f()Ljava/io/FileInputStream;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final d()J
aload 0
getfield com/a/b/i/bg/a Ljava/io/File;
invokevirtual java/io/File/isFile()Z
ifne L0
new java/io/FileNotFoundException
dup
aload 0
getfield com/a/b/i/bg/a Ljava/io/File;
invokevirtual java/io/File/toString()Ljava/lang/String;
invokespecial java/io/FileNotFoundException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
getfield com/a/b/i/bg/a Ljava/io/File;
invokevirtual java/io/File/length()J
lreturn
.limit locals 1
.limit stack 3
.end method

.method public final e()[B
.catch java/lang/Throwable from L0 to L1 using L2
.catch all from L0 to L1 using L3
.catch all from L4 to L3 using L3
invokestatic com/a/b/i/ar/a()Lcom/a/b/i/ar;
astore 1
L0:
aload 1
aload 0
invokespecial com/a/b/i/bg/f()Ljava/io/FileInputStream;
invokevirtual com/a/b/i/ar/a(Ljava/io/Closeable;)Ljava/io/Closeable;
checkcast java/io/FileInputStream
astore 2
aload 2
aload 2
invokevirtual java/io/FileInputStream/getChannel()Ljava/nio/channels/FileChannel;
invokevirtual java/nio/channels/FileChannel/size()J
invokestatic com/a/b/i/bc/a(Ljava/io/InputStream;J)[B
astore 2
L1:
aload 1
invokevirtual com/a/b/i/ar/close()V
aload 2
areturn
L2:
astore 2
L4:
aload 1
aload 2
invokevirtual com/a/b/i/ar/a(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;
athrow
L3:
astore 2
aload 1
invokevirtual com/a/b/i/ar/close()V
aload 2
athrow
.limit locals 3
.limit stack 3
.end method

.method public final toString()Ljava/lang/String;
aload 0
getfield com/a/b/i/bg/a Ljava/io/File;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 1
new java/lang/StringBuilder
dup
aload 1
invokevirtual java/lang/String/length()I
bipush 20
iadd
invokespecial java/lang/StringBuilder/<init>(I)V
ldc "Files.asByteSource("
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ")"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 2
.limit stack 4
.end method
