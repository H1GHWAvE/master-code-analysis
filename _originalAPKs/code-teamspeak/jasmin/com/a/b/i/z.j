.bytecode 50.0
.class public final synchronized com/a/b/i/z
.super java/lang/Object
.annotation invisible Lcom/a/b/a/a;
.end annotation

.field private static final 'a' I = 4096


.field private static final 'b' Ljava/io/OutputStream;

.method static <clinit>()V
new com/a/b/i/aa
dup
invokespecial com/a/b/i/aa/<init>()V
putstatic com/a/b/i/z/b Ljava/io/OutputStream;
return
.limit locals 0
.limit stack 2
.end method

.method private <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static a(Ljava/io/InputStream;Ljava/io/OutputStream;)J
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
sipush 4096
newarray byte
astore 5
lconst_0
lstore 3
L0:
aload 0
aload 5
invokevirtual java/io/InputStream/read([B)I
istore 2
iload 2
iconst_m1
if_icmpeq L1
aload 1
aload 5
iconst_0
iload 2
invokevirtual java/io/OutputStream/write([BII)V
lload 3
iload 2
i2l
ladd
lstore 3
goto L0
L1:
lload 3
lreturn
.limit locals 6
.limit stack 4
.end method

.method private static a(Ljava/nio/channels/ReadableByteChannel;Ljava/nio/channels/WritableByteChannel;)J
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
sipush 4096
invokestatic java/nio/ByteBuffer/allocate(I)Ljava/nio/ByteBuffer;
astore 4
lconst_0
lstore 2
L0:
aload 0
aload 4
invokeinterface java/nio/channels/ReadableByteChannel/read(Ljava/nio/ByteBuffer;)I 1
iconst_m1
if_icmpeq L1
aload 4
invokevirtual java/nio/ByteBuffer/flip()Ljava/nio/Buffer;
pop
L2:
aload 4
invokevirtual java/nio/ByteBuffer/hasRemaining()Z
ifeq L3
lload 2
aload 1
aload 4
invokeinterface java/nio/channels/WritableByteChannel/write(Ljava/nio/ByteBuffer;)I 1
i2l
ladd
lstore 2
goto L2
L3:
aload 4
invokevirtual java/nio/ByteBuffer/clear()Ljava/nio/Buffer;
pop
goto L0
L1:
lload 2
lreturn
.limit locals 5
.limit stack 4
.end method

.method private static a(Ljava/io/ByteArrayInputStream;)Lcom/a/b/i/m;
new com/a/b/i/ab
dup
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/io/ByteArrayInputStream
invokespecial com/a/b/i/ab/<init>(Ljava/io/ByteArrayInputStream;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method public static a([B)Lcom/a/b/i/m;
new java/io/ByteArrayInputStream
dup
aload 0
invokespecial java/io/ByteArrayInputStream/<init>([B)V
invokestatic com/a/b/i/z/a(Ljava/io/ByteArrayInputStream;)Lcom/a/b/i/m;
areturn
.limit locals 1
.limit stack 3
.end method

.method public static a([BI)Lcom/a/b/i/m;
iload 1
aload 0
arraylength
invokestatic com/a/b/b/cn/b(II)I
pop
new java/io/ByteArrayInputStream
dup
aload 0
iload 1
aload 0
arraylength
iload 1
isub
invokespecial java/io/ByteArrayInputStream/<init>([BII)V
invokestatic com/a/b/i/z/a(Ljava/io/ByteArrayInputStream;)Lcom/a/b/i/m;
areturn
.limit locals 2
.limit stack 6
.end method

.method private static a()Lcom/a/b/i/n;
new java/io/ByteArrayOutputStream
dup
invokespecial java/io/ByteArrayOutputStream/<init>()V
invokestatic com/a/b/i/z/a(Ljava/io/ByteArrayOutputStream;)Lcom/a/b/i/n;
areturn
.limit locals 0
.limit stack 2
.end method

.method private static a(I)Lcom/a/b/i/n;
iload 0
iflt L0
iconst_1
istore 1
L1:
iload 1
ldc "Invalid size: %s"
iconst_1
anewarray java/lang/Object
dup
iconst_0
iload 0
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
new java/io/ByteArrayOutputStream
dup
iload 0
invokespecial java/io/ByteArrayOutputStream/<init>(I)V
invokestatic com/a/b/i/z/a(Ljava/io/ByteArrayOutputStream;)Lcom/a/b/i/n;
areturn
L0:
iconst_0
istore 1
goto L1
.limit locals 2
.limit stack 6
.end method

.method private static a(Ljava/io/ByteArrayOutputStream;)Lcom/a/b/i/n;
new com/a/b/i/ac
dup
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/io/ByteArrayOutputStream
invokespecial com/a/b/i/ac/<init>(Ljava/io/ByteArrayOutputStream;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method public static a(Ljava/io/InputStream;J)Ljava/io/InputStream;
new com/a/b/i/ae
dup
aload 0
lload 1
invokespecial com/a/b/i/ae/<init>(Ljava/io/InputStream;J)V
areturn
.limit locals 3
.limit stack 5
.end method

.method public static a(Ljava/io/InputStream;Lcom/a/b/i/o;)Ljava/lang/Object;
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
sipush 4096
newarray byte
astore 2
L0:
aload 0
aload 2
invokevirtual java/io/InputStream/read([B)I
iconst_m1
if_icmpeq L1
aload 1
invokeinterface com/a/b/i/o/a()Z 0
ifne L0
L1:
aload 1
invokeinterface com/a/b/i/o/b()Ljava/lang/Object; 0
areturn
.limit locals 3
.limit stack 2
.end method

.method public static a(Ljava/io/InputStream;[B)V
aload 0
aload 1
iconst_0
aload 1
arraylength
invokestatic com/a/b/i/z/a(Ljava/io/InputStream;[BII)V
return
.limit locals 2
.limit stack 4
.end method

.method public static a(Ljava/io/InputStream;[BII)V
aload 0
aload 1
iload 2
iload 3
invokestatic com/a/b/i/z/b(Ljava/io/InputStream;[BII)I
istore 2
iload 2
iload 3
if_icmpeq L0
new java/io/EOFException
dup
new java/lang/StringBuilder
dup
bipush 81
invokespecial java/lang/StringBuilder/<init>(I)V
ldc "reached end of stream after reading "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 2
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc " bytes; "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 3
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc " bytes expected"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/EOFException/<init>(Ljava/lang/String;)V
athrow
L0:
return
.limit locals 4
.limit stack 5
.end method

.method public static a(Ljava/io/InputStream;)[B
new java/io/ByteArrayOutputStream
dup
invokespecial java/io/ByteArrayOutputStream/<init>()V
astore 1
aload 0
aload 1
invokestatic com/a/b/i/z/a(Ljava/io/InputStream;Ljava/io/OutputStream;)J
pop2
aload 1
invokevirtual java/io/ByteArrayOutputStream/toByteArray()[B
areturn
.limit locals 2
.limit stack 2
.end method

.method static a(Ljava/io/InputStream;I)[B
iload 1
newarray byte
astore 6
iload 1
istore 2
L0:
iload 2
ifle L1
iload 1
iload 2
isub
istore 3
aload 0
aload 6
iload 3
iload 2
invokevirtual java/io/InputStream/read([BII)I
istore 4
iload 4
iconst_m1
if_icmpne L2
aload 6
iload 3
invokestatic java/util/Arrays/copyOf([BI)[B
astore 5
L3:
aload 5
areturn
L2:
iload 2
iload 4
isub
istore 2
goto L0
L1:
aload 0
invokevirtual java/io/InputStream/read()I
istore 1
aload 6
astore 5
iload 1
iconst_m1
if_icmpeq L3
new com/a/b/i/ad
dup
iconst_0
invokespecial com/a/b/i/ad/<init>(B)V
astore 5
aload 5
iload 1
invokevirtual com/a/b/i/ad/write(I)V
aload 0
aload 5
invokestatic com/a/b/i/z/a(Ljava/io/InputStream;Ljava/io/OutputStream;)J
pop2
aload 6
arraylength
aload 5
invokevirtual com/a/b/i/ad/size()I
iadd
newarray byte
astore 0
aload 6
iconst_0
aload 0
iconst_0
aload 6
arraylength
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 5
aload 0
aload 6
arraylength
invokevirtual com/a/b/i/ad/a([BI)V
aload 0
areturn
.limit locals 7
.limit stack 5
.end method

.method public static b(Ljava/io/InputStream;[BII)I
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
iload 3
ifge L0
new java/lang/IndexOutOfBoundsException
dup
ldc "len is negative"
invokespecial java/lang/IndexOutOfBoundsException/<init>(Ljava/lang/String;)V
athrow
L0:
iconst_0
istore 4
L1:
iload 4
iload 3
if_icmpge L2
aload 0
aload 1
iload 2
iload 4
iadd
iload 3
iload 4
isub
invokevirtual java/io/InputStream/read([BII)I
istore 5
iload 5
iconst_m1
if_icmpeq L2
iload 4
iload 5
iadd
istore 4
goto L1
L2:
iload 4
ireturn
.limit locals 6
.limit stack 5
.end method

.method private static b()Ljava/io/OutputStream;
getstatic com/a/b/i/z/b Ljava/io/OutputStream;
areturn
.limit locals 0
.limit stack 1
.end method

.method public static b(Ljava/io/InputStream;J)V
lload 1
lstore 3
L0:
lload 3
lconst_0
lcmp
ifle L1
aload 0
lload 3
invokevirtual java/io/InputStream/skip(J)J
lstore 5
lload 5
lconst_0
lcmp
ifne L2
aload 0
invokevirtual java/io/InputStream/read()I
iconst_m1
if_icmpne L3
new java/io/EOFException
dup
new java/lang/StringBuilder
dup
bipush 100
invokespecial java/lang/StringBuilder/<init>(I)V
ldc "reached end of stream after skipping "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
lload 1
lload 3
lsub
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
ldc " bytes; "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
lload 1
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
ldc " bytes expected"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/EOFException/<init>(Ljava/lang/String;)V
athrow
L3:
lload 3
lconst_1
lsub
lstore 3
goto L0
L2:
lload 3
lload 5
lsub
lstore 3
goto L0
L1:
return
.limit locals 7
.limit stack 7
.end method
