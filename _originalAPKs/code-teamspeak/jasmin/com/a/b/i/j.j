.bytecode 50.0
.class final synchronized com/a/b/i/j
.super com/a/b/i/b

.field private final 'a' Lcom/a/b/i/g;

.field private final 'b' Ljava/lang/Character;
.annotation visible Ljavax/annotation/Nullable;
.end annotation
.end field

.field private transient 'c' Lcom/a/b/i/b;

.field private transient 'd' Lcom/a/b/i/b;

.method private <init>(Lcom/a/b/i/g;Ljava/lang/Character;)V
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
aload 0
invokespecial com/a/b/i/b/<init>()V
aload 0
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast com/a/b/i/g
putfield com/a/b/i/j/a Lcom/a/b/i/g;
aload 2
ifnull L0
aload 1
aload 2
invokevirtual java/lang/Character/charValue()C
invokevirtual com/a/b/i/g/c(C)Z
ifne L1
L0:
iconst_1
istore 3
L2:
iload 3
ldc "Padding character %s was already in alphabet"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 2
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 0
aload 2
putfield com/a/b/i/j/b Ljava/lang/Character;
return
L1:
iconst_0
istore 3
goto L2
.limit locals 4
.limit stack 6
.end method

.method <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Character;)V
.annotation visibleparam 3 Ljavax/annotation/Nullable;
.end annotation
aload 0
new com/a/b/i/g
dup
aload 1
aload 2
invokevirtual java/lang/String/toCharArray()[C
invokespecial com/a/b/i/g/<init>(Ljava/lang/String;[C)V
aload 3
invokespecial com/a/b/i/j/<init>(Lcom/a/b/i/g;Ljava/lang/Character;)V
return
.limit locals 4
.limit stack 5
.end method

.method static synthetic a(Lcom/a/b/i/j;)Lcom/a/b/i/g;
aload 0
getfield com/a/b/i/j/a Lcom/a/b/i/g;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic b(Lcom/a/b/i/j;)Ljava/lang/Character;
aload 0
getfield com/a/b/i/j/b Ljava/lang/Character;
areturn
.limit locals 1
.limit stack 1
.end method

.method final a(I)I
aload 0
getfield com/a/b/i/j/a Lcom/a/b/i/g;
getfield com/a/b/i/g/w I
iload 1
aload 0
getfield com/a/b/i/j/a Lcom/a/b/i/g;
getfield com/a/b/i/g/x I
getstatic java/math/RoundingMode/CEILING Ljava/math/RoundingMode;
invokestatic com/a/b/j/g/a(IILjava/math/RoundingMode;)I
imul
ireturn
.limit locals 2
.limit stack 4
.end method

.method final a()Lcom/a/b/b/m;
aload 0
getfield com/a/b/i/j/b Ljava/lang/Character;
ifnonnull L0
getstatic com/a/b/b/m/m Lcom/a/b/b/m;
areturn
L0:
aload 0
getfield com/a/b/i/j/b Ljava/lang/Character;
invokevirtual java/lang/Character/charValue()C
invokestatic com/a/b/b/m/a(C)Lcom/a/b/b/m;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final a(C)Lcom/a/b/i/b;
bipush 8
aload 0
getfield com/a/b/i/j/a Lcom/a/b/i/g;
getfield com/a/b/i/g/v I
irem
ifeq L0
aload 0
getfield com/a/b/i/j/b Ljava/lang/Character;
ifnull L1
aload 0
getfield com/a/b/i/j/b Ljava/lang/Character;
invokevirtual java/lang/Character/charValue()C
iload 1
if_icmpne L1
L0:
aload 0
areturn
L1:
new com/a/b/i/j
dup
aload 0
getfield com/a/b/i/j/a Lcom/a/b/i/g;
iload 1
invokestatic java/lang/Character/valueOf(C)Ljava/lang/Character;
invokespecial com/a/b/i/j/<init>(Lcom/a/b/i/g;Ljava/lang/Character;)V
areturn
.limit locals 2
.limit stack 4
.end method

.method public final a(Ljava/lang/String;I)Lcom/a/b/i/b;
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
invokevirtual com/a/b/i/j/a()Lcom/a/b/b/m;
aload 0
getfield com/a/b/i/j/a Lcom/a/b/i/g;
invokevirtual com/a/b/b/m/b(Lcom/a/b/b/m;)Lcom/a/b/b/m;
aload 1
invokevirtual com/a/b/b/m/d(Ljava/lang/CharSequence;)Z
ldc "Separator cannot contain alphabet or padding characters"
invokestatic com/a/b/b/cn/a(ZLjava/lang/Object;)V
new com/a/b/i/i
dup
aload 0
aload 1
iload 2
invokespecial com/a/b/i/i/<init>(Lcom/a/b/i/b;Ljava/lang/String;I)V
areturn
.limit locals 3
.limit stack 5
.end method

.method final a(Lcom/a/b/i/bu;)Lcom/a/b/i/bs;
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
new com/a/b/i/l
dup
aload 0
aload 1
invokespecial com/a/b/i/l/<init>(Lcom/a/b/i/j;Lcom/a/b/i/bu;)V
areturn
.limit locals 2
.limit stack 4
.end method

.method final a(Lcom/a/b/i/bv;)Lcom/a/b/i/bt;
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
new com/a/b/i/k
dup
aload 0
aload 1
invokespecial com/a/b/i/k/<init>(Lcom/a/b/i/j;Lcom/a/b/i/bv;)V
areturn
.limit locals 2
.limit stack 4
.end method

.method final b(I)I
aload 0
getfield com/a/b/i/j/a Lcom/a/b/i/g;
getfield com/a/b/i/g/v I
i2l
iload 1
i2l
lmul
ldc2_w 7L
ladd
ldc2_w 8L
ldiv
l2i
ireturn
.limit locals 2
.limit stack 4
.end method

.method public final b()Lcom/a/b/i/b;
aload 0
getfield com/a/b/i/j/b Ljava/lang/Character;
ifnonnull L0
aload 0
areturn
L0:
new com/a/b/i/j
dup
aload 0
getfield com/a/b/i/j/a Lcom/a/b/i/g;
aconst_null
invokespecial com/a/b/i/j/<init>(Lcom/a/b/i/g;Ljava/lang/Character;)V
areturn
.limit locals 1
.limit stack 4
.end method

.method public final c()Lcom/a/b/i/b;
iconst_0
istore 1
aload 0
getfield com/a/b/i/j/c Lcom/a/b/i/b;
astore 4
aload 4
astore 3
aload 4
ifnonnull L0
aload 0
getfield com/a/b/i/j/a Lcom/a/b/i/g;
astore 3
aload 3
invokevirtual com/a/b/i/g/c()Z
ifne L1
L2:
aload 3
aload 0
getfield com/a/b/i/j/a Lcom/a/b/i/g;
if_acmpne L3
aload 0
astore 3
L4:
aload 0
aload 3
putfield com/a/b/i/j/c Lcom/a/b/i/b;
L0:
aload 3
areturn
L1:
aload 3
invokevirtual com/a/b/i/g/d()Z
ifne L5
iconst_1
istore 2
L6:
iload 2
ldc "Cannot call upperCase() on a mixed-case alphabet"
invokestatic com/a/b/b/cn/b(ZLjava/lang/Object;)V
aload 3
getfield com/a/b/i/g/t [C
arraylength
newarray char
astore 4
L7:
iload 1
aload 3
getfield com/a/b/i/g/t [C
arraylength
if_icmpge L8
aload 4
iload 1
aload 3
getfield com/a/b/i/g/t [C
iload 1
caload
invokestatic com/a/b/b/e/b(C)C
castore
iload 1
iconst_1
iadd
istore 1
goto L7
L5:
iconst_0
istore 2
goto L6
L8:
new com/a/b/i/g
dup
aload 3
getfield com/a/b/i/g/s Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
ldc ".upperCase()"
invokevirtual java/lang/String/concat(Ljava/lang/String;)Ljava/lang/String;
aload 4
invokespecial com/a/b/i/g/<init>(Ljava/lang/String;[C)V
astore 3
goto L2
L3:
new com/a/b/i/j
dup
aload 3
aload 0
getfield com/a/b/i/j/b Ljava/lang/Character;
invokespecial com/a/b/i/j/<init>(Lcom/a/b/i/g;Ljava/lang/Character;)V
astore 3
goto L4
.limit locals 5
.limit stack 4
.end method

.method public final d()Lcom/a/b/i/b;
iconst_0
istore 1
aload 0
getfield com/a/b/i/j/d Lcom/a/b/i/b;
astore 4
aload 4
astore 3
aload 4
ifnonnull L0
aload 0
getfield com/a/b/i/j/a Lcom/a/b/i/g;
astore 3
aload 3
invokevirtual com/a/b/i/g/d()Z
ifne L1
L2:
aload 3
aload 0
getfield com/a/b/i/j/a Lcom/a/b/i/g;
if_acmpne L3
aload 0
astore 3
L4:
aload 0
aload 3
putfield com/a/b/i/j/d Lcom/a/b/i/b;
L0:
aload 3
areturn
L1:
aload 3
invokevirtual com/a/b/i/g/c()Z
ifne L5
iconst_1
istore 2
L6:
iload 2
ldc "Cannot call lowerCase() on a mixed-case alphabet"
invokestatic com/a/b/b/cn/b(ZLjava/lang/Object;)V
aload 3
getfield com/a/b/i/g/t [C
arraylength
newarray char
astore 4
L7:
iload 1
aload 3
getfield com/a/b/i/g/t [C
arraylength
if_icmpge L8
aload 4
iload 1
aload 3
getfield com/a/b/i/g/t [C
iload 1
caload
invokestatic com/a/b/b/e/a(C)C
castore
iload 1
iconst_1
iadd
istore 1
goto L7
L5:
iconst_0
istore 2
goto L6
L8:
new com/a/b/i/g
dup
aload 3
getfield com/a/b/i/g/s Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
ldc ".lowerCase()"
invokevirtual java/lang/String/concat(Ljava/lang/String;)Ljava/lang/String;
aload 4
invokespecial com/a/b/i/g/<init>(Ljava/lang/String;[C)V
astore 3
goto L2
L3:
new com/a/b/i/j
dup
aload 3
aload 0
getfield com/a/b/i/j/b Ljava/lang/Character;
invokespecial com/a/b/i/j/<init>(Lcom/a/b/i/g;Ljava/lang/Character;)V
astore 3
goto L4
.limit locals 5
.limit stack 4
.end method

.method public final toString()Ljava/lang/String;
new java/lang/StringBuilder
dup
ldc "BaseEncoding."
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
astore 1
aload 1
aload 0
getfield com/a/b/i/j/a Lcom/a/b/i/g;
invokevirtual com/a/b/i/g/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
bipush 8
aload 0
getfield com/a/b/i/j/a Lcom/a/b/i/g;
getfield com/a/b/i/g/v I
irem
ifeq L0
aload 0
getfield com/a/b/i/j/b Ljava/lang/Character;
ifnonnull L1
aload 1
ldc ".omitPadding()"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
L0:
aload 1
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
L1:
aload 1
ldc ".withPadChar("
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/a/b/i/j/b Ljava/lang/Character;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
bipush 41
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
pop
goto L0
.limit locals 2
.limit stack 3
.end method
