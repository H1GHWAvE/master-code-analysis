.bytecode 50.0
.class synchronized com/a/b/i/v
.super com/a/b/i/s

.field protected final 'a' [B

.method protected <init>([B)V
aload 0
invokespecial com/a/b/i/s/<init>()V
aload 0
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast [B
putfield com/a/b/i/v/a [B
return
.limit locals 2
.limit stack 2
.end method

.method public final a(Ljava/io/OutputStream;)J
aload 1
aload 0
getfield com/a/b/i/v/a [B
invokevirtual java/io/OutputStream/write([B)V
aload 0
getfield com/a/b/i/v/a [B
arraylength
i2l
lreturn
.limit locals 2
.limit stack 2
.end method

.method public final a(Lcom/a/b/g/ak;)Lcom/a/b/g/ag;
aload 1
aload 0
getfield com/a/b/i/v/a [B
invokeinterface com/a/b/g/ak/a([B)Lcom/a/b/g/ag; 1
areturn
.limit locals 2
.limit stack 2
.end method

.method public final a()Ljava/io/InputStream;
new java/io/ByteArrayInputStream
dup
aload 0
getfield com/a/b/i/v/a [B
invokespecial java/io/ByteArrayInputStream/<init>([B)V
areturn
.limit locals 1
.limit stack 3
.end method

.method public final a(Lcom/a/b/i/o;)Ljava/lang/Object;
aload 1
invokeinterface com/a/b/i/o/b()Ljava/lang/Object; 0
areturn
.limit locals 2
.limit stack 1
.end method

.method public final b()Ljava/io/InputStream;
aload 0
invokevirtual com/a/b/i/v/a()Ljava/io/InputStream;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final c()Z
aload 0
getfield com/a/b/i/v/a [B
arraylength
ifne L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final d()J
aload 0
getfield com/a/b/i/v/a [B
arraylength
i2l
lreturn
.limit locals 1
.limit stack 2
.end method

.method public e()[B
aload 0
getfield com/a/b/i/v/a [B
invokevirtual [B/clone()Ljava/lang/Object;
checkcast [B
areturn
.limit locals 1
.limit stack 1
.end method

.method public toString()Ljava/lang/String;
invokestatic com/a/b/i/b/e()Lcom/a/b/i/b;
astore 1
aload 0
getfield com/a/b/i/v/a [B
astore 2
aload 1
aload 2
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast [B
aload 2
arraylength
invokevirtual com/a/b/i/b/a([BI)Ljava/lang/String;
ldc "..."
invokestatic com/a/b/b/e/a(Ljava/lang/CharSequence;Ljava/lang/String;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 1
new java/lang/StringBuilder
dup
aload 1
invokevirtual java/lang/String/length()I
bipush 17
iadd
invokespecial java/lang/StringBuilder/<init>(I)V
ldc "ByteSource.wrap("
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ")"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 3
.limit stack 4
.end method
