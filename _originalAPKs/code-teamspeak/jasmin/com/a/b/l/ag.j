.bytecode 50.0
.class public final synchronized com/a/b/l/ag
.super java/lang/Object

.field public static final 'a' B = -128


.field public static final 'b' B = -1


.field private static final 'c' I = 255


.method private <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a(J)B
lload 0
bipush 8
lshr
lconst_0
lcmp
ifeq L0
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
bipush 34
invokespecial java/lang/StringBuilder/<init>(I)V
ldc "Out of range: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
lload 0
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
lload 0
l2i
i2b
ireturn
.limit locals 2
.limit stack 5
.end method

.method private static a(Ljava/lang/String;)B
.annotation invisible Lcom/a/b/a/a;
.end annotation
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/String
bipush 10
invokestatic java/lang/Integer/parseInt(Ljava/lang/String;I)I
istore 1
iload 1
bipush 8
ishr
ifne L0
iload 1
i2b
ireturn
L0:
new java/lang/NumberFormatException
dup
new java/lang/StringBuilder
dup
bipush 25
invokespecial java/lang/StringBuilder/<init>(I)V
ldc "out of range: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 1
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/NumberFormatException/<init>(Ljava/lang/String;)V
athrow
.limit locals 2
.limit stack 5
.end method

.method private static transient a([B)B
iconst_1
istore 1
aload 0
arraylength
ifle L0
iconst_1
istore 5
L1:
iload 5
invokestatic com/a/b/b/cn/a(Z)V
aload 0
iconst_0
baload
sipush 255
iand
istore 2
L2:
iload 1
aload 0
arraylength
if_icmpge L3
aload 0
iload 1
baload
sipush 255
iand
istore 4
iload 2
istore 3
iload 4
iload 2
if_icmpge L4
iload 4
istore 3
L4:
iload 1
iconst_1
iadd
istore 1
iload 3
istore 2
goto L2
L0:
iconst_0
istore 5
goto L1
L3:
iload 2
i2b
ireturn
.limit locals 6
.limit stack 2
.end method

.method private static a(B)I
iload 0
sipush 255
iand
ireturn
.limit locals 1
.limit stack 2
.end method

.method public static a(BB)I
iload 0
sipush 255
iand
iload 1
sipush 255
iand
isub
ireturn
.limit locals 2
.limit stack 3
.end method

.method private static transient a(Ljava/lang/String;[B)Ljava/lang/String;
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 1
arraylength
ifne L0
ldc ""
areturn
L0:
new java/lang/StringBuilder
dup
aload 1
arraylength
aload 0
invokevirtual java/lang/String/length()I
iconst_3
iadd
imul
invokespecial java/lang/StringBuilder/<init>(I)V
astore 4
aload 4
aload 1
iconst_0
baload
sipush 255
iand
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
pop
iconst_1
istore 2
L1:
iload 2
aload 1
arraylength
if_icmpge L2
aload 4
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
astore 5
aload 1
iload 2
baload
istore 3
iconst_1
ldc "radix (%s) must be between Character.MIN_RADIX and Character.MAX_RADIX"
iconst_1
anewarray java/lang/Object
dup
iconst_0
bipush 10
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 5
iload 3
sipush 255
iand
bipush 10
invokestatic java/lang/Integer/toString(II)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
iload 2
iconst_1
iadd
istore 2
goto L1
L2:
aload 4
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 6
.limit stack 6
.end method

.method private static a()Ljava/util/Comparator;
getstatic com/a/b/l/ah/b Ljava/util/Comparator;
areturn
.limit locals 0
.limit stack 1
.end method

.method private static b(J)B
lload 0
ldc2_w 255L
lcmp
ifle L0
iconst_m1
ireturn
L0:
lload 0
lconst_0
lcmp
ifge L1
iconst_0
ireturn
L1:
lload 0
l2i
i2b
ireturn
.limit locals 2
.limit stack 4
.end method

.method private static b(Ljava/lang/String;)B
.annotation invisible Lcom/a/b/a/a;
.end annotation
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/String
bipush 10
invokestatic java/lang/Integer/parseInt(Ljava/lang/String;I)I
istore 1
iload 1
bipush 8
ishr
ifne L0
iload 1
i2b
ireturn
L0:
new java/lang/NumberFormatException
dup
new java/lang/StringBuilder
dup
bipush 25
invokespecial java/lang/StringBuilder/<init>(I)V
ldc "out of range: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 1
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/NumberFormatException/<init>(Ljava/lang/String;)V
athrow
.limit locals 2
.limit stack 5
.end method

.method private static transient b([B)B
iconst_1
istore 1
aload 0
arraylength
ifle L0
iconst_1
istore 5
L1:
iload 5
invokestatic com/a/b/b/cn/a(Z)V
aload 0
iconst_0
baload
sipush 255
iand
istore 2
L2:
iload 1
aload 0
arraylength
if_icmpge L3
aload 0
iload 1
baload
sipush 255
iand
istore 4
iload 2
istore 3
iload 4
iload 2
if_icmple L4
iload 4
istore 3
L4:
iload 1
iconst_1
iadd
istore 1
iload 3
istore 2
goto L2
L0:
iconst_0
istore 5
goto L1
L3:
iload 2
i2b
ireturn
.limit locals 6
.limit stack 2
.end method

.method private static b(B)Ljava/lang/String;
.annotation invisible Lcom/a/b/a/a;
.end annotation
iconst_1
ldc "radix (%s) must be between Character.MIN_RADIX and Character.MAX_RADIX"
iconst_1
anewarray java/lang/Object
dup
iconst_0
bipush 10
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
iload 0
sipush 255
iand
bipush 10
invokestatic java/lang/Integer/toString(II)Ljava/lang/String;
areturn
.limit locals 1
.limit stack 6
.end method

.method private static b()Ljava/util/Comparator;
.annotation invisible Lcom/a/b/a/d;
.end annotation
getstatic com/a/b/l/ai/a Lcom/a/b/l/ai;
areturn
.limit locals 0
.limit stack 1
.end method

.method private static c(B)Ljava/lang/String;
.annotation invisible Lcom/a/b/a/a;
.end annotation
iconst_1
ldc "radix (%s) must be between Character.MIN_RADIX and Character.MAX_RADIX"
iconst_1
anewarray java/lang/Object
dup
iconst_0
bipush 10
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
iload 0
sipush 255
iand
bipush 10
invokestatic java/lang/Integer/toString(II)Ljava/lang/String;
areturn
.limit locals 1
.limit stack 6
.end method
