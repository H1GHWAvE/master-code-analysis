.bytecode 50.0
.class public final synchronized com/a/b/l/i
.super java/lang/Object
.annotation invisible Lcom/a/b/a/b;
b Z = 1
.end annotation

.field public static final 'a' I = 8


.field static final 'b' Ljava/util/regex/Pattern;
.annotation invisible Lcom/a/b/a/c;
a s = "regular expressions"
.end annotation
.end field

.method static <clinit>()V
ldc "(?:\\d++(?:\\.\\d*+)?|\\.\\d++)"
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
ldc "(?:[eE][+-]?\\d++)?[fFdD]?"
invokevirtual java/lang/String/concat(Ljava/lang/String;)Ljava/lang/String;
astore 0
ldc "(?:\\p{XDigit}++(?:\\.\\p{XDigit}*+)?|\\.\\p{XDigit}++)"
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 1
new java/lang/StringBuilder
dup
aload 1
invokevirtual java/lang/String/length()I
bipush 25
iadd
invokespecial java/lang/StringBuilder/<init>(I)V
ldc "0[xX]"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "[pP][+-]?\\d++[fFdD]?"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 1
aload 0
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 0
aload 1
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 1
new java/lang/StringBuilder
dup
aload 0
invokevirtual java/lang/String/length()I
bipush 23
iadd
aload 1
invokevirtual java/lang/String/length()I
iadd
invokespecial java/lang/StringBuilder/<init>(I)V
ldc "[+-]?(?:NaN|Infinity|"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "|"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ")"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic java/util/regex/Pattern/compile(Ljava/lang/String;)Ljava/util/regex/Pattern;
putstatic com/a/b/l/i/b Ljava/util/regex/Pattern;
return
.limit locals 2
.limit stack 4
.end method

.method private <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static transient a([D)D
iconst_1
istore 3
aload 0
arraylength
ifle L0
iconst_1
istore 4
L1:
iload 4
invokestatic com/a/b/b/cn/a(Z)V
aload 0
iconst_0
daload
dstore 1
L2:
iload 3
aload 0
arraylength
if_icmpge L3
dload 1
aload 0
iload 3
daload
invokestatic java/lang/Math/min(DD)D
dstore 1
iload 3
iconst_1
iadd
istore 3
goto L2
L0:
iconst_0
istore 4
goto L1
L3:
dload 1
dreturn
.limit locals 5
.limit stack 4
.end method

.method public static a(D)I
dload 0
invokestatic java/lang/Double/valueOf(D)Ljava/lang/Double;
invokevirtual java/lang/Double/hashCode()I
ireturn
.limit locals 2
.limit stack 2
.end method

.method private static a(DD)I
dload 0
dload 2
invokestatic java/lang/Double/compare(DD)I
ireturn
.limit locals 4
.limit stack 4
.end method

.method static synthetic a([DDII)I
aload 0
dload 1
iload 3
iload 4
invokestatic com/a/b/l/i/c([DDII)I
ireturn
.limit locals 5
.limit stack 5
.end method

.method private static a([D[D)I
aload 0
ldc "array"
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 1
ldc "target"
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 1
arraylength
ifne L0
iconst_0
ireturn
L0:
iconst_0
istore 2
L1:
iload 2
aload 0
arraylength
aload 1
arraylength
isub
iconst_1
iadd
if_icmpge L2
iconst_0
istore 3
L3:
iload 3
aload 1
arraylength
if_icmpge L4
aload 0
iload 2
iload 3
iadd
daload
aload 1
iload 3
daload
dcmpl
ifne L5
iload 3
iconst_1
iadd
istore 3
goto L3
L4:
iload 2
ireturn
L5:
iload 2
iconst_1
iadd
istore 2
goto L1
L2:
iconst_m1
ireturn
.limit locals 4
.limit stack 4
.end method

.method private static a()Lcom/a/b/b/ak;
.annotation invisible Lcom/a/b/a/a;
.end annotation
getstatic com/a/b/l/k/a Lcom/a/b/l/k;
areturn
.limit locals 0
.limit stack 1
.end method

.method private static a(Ljava/lang/String;)Ljava/lang/Double;
.annotation visible Ljavax/annotation/Nullable;
.end annotation
.annotation invisible Lcom/a/b/a/a;
.end annotation
.annotation invisible Lcom/a/b/a/c;
a s = "regular expressions"
.end annotation
.catch java/lang/NumberFormatException from L0 to L1 using L2
getstatic com/a/b/l/i/b Ljava/util/regex/Pattern;
aload 0
invokevirtual java/util/regex/Pattern/matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;
invokevirtual java/util/regex/Matcher/matches()Z
ifeq L3
L0:
aload 0
invokestatic java/lang/Double/parseDouble(Ljava/lang/String;)D
dstore 1
L1:
dload 1
invokestatic java/lang/Double/valueOf(D)Ljava/lang/Double;
areturn
L2:
astore 0
L3:
aconst_null
areturn
.limit locals 3
.limit stack 2
.end method

.method private static transient a(Ljava/lang/String;[D)Ljava/lang/String;
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 1
arraylength
ifne L0
ldc ""
areturn
L0:
new java/lang/StringBuilder
dup
aload 1
arraylength
bipush 12
imul
invokespecial java/lang/StringBuilder/<init>(I)V
astore 3
aload 3
aload 1
iconst_0
daload
invokevirtual java/lang/StringBuilder/append(D)Ljava/lang/StringBuilder;
pop
iconst_1
istore 2
L1:
iload 2
aload 1
arraylength
if_icmpge L2
aload 3
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
iload 2
daload
invokevirtual java/lang/StringBuilder/append(D)Ljava/lang/StringBuilder;
pop
iload 2
iconst_1
iadd
istore 2
goto L1
L2:
aload 3
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 4
.limit stack 4
.end method

.method private static a([DD)Z
iconst_0
istore 6
aload 0
arraylength
istore 4
iconst_0
istore 3
L0:
iload 6
istore 5
iload 3
iload 4
if_icmpge L1
aload 0
iload 3
daload
dload 1
dcmpl
ifne L2
iconst_1
istore 5
L1:
iload 5
ireturn
L2:
iload 3
iconst_1
iadd
istore 3
goto L0
.limit locals 7
.limit stack 4
.end method

.method private static a(Ljava/util/Collection;)[D
iconst_0
istore 1
aload 0
instanceof com/a/b/l/j
ifeq L0
aload 0
checkcast com/a/b/l/j
astore 0
aload 0
invokevirtual com/a/b/l/j/size()I
istore 1
iload 1
newarray double
astore 3
aload 0
getfield com/a/b/l/j/a [D
aload 0
getfield com/a/b/l/j/b I
aload 3
iconst_0
iload 1
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 3
areturn
L0:
aload 0
invokeinterface java/util/Collection/toArray()[Ljava/lang/Object; 0
astore 0
aload 0
arraylength
istore 2
iload 2
newarray double
astore 3
L1:
iload 1
iload 2
if_icmpge L2
aload 3
iload 1
aload 0
iload 1
aaload
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/Number
invokevirtual java/lang/Number/doubleValue()D
dastore
iload 1
iconst_1
iadd
istore 1
goto L1
L2:
aload 3
areturn
.limit locals 4
.limit stack 5
.end method

.method private static a([DI)[D
iload 1
newarray double
astore 2
aload 0
iconst_0
aload 2
iconst_0
aload 0
arraylength
iload 1
invokestatic java/lang/Math/min(II)I
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 2
areturn
.limit locals 3
.limit stack 6
.end method

.method private static a([DII)[D
iload 1
iflt L0
iconst_1
istore 3
L1:
iload 3
ldc "Invalid minLength: %s"
iconst_1
anewarray java/lang/Object
dup
iconst_0
iload 1
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
iload 2
iflt L2
iconst_1
istore 3
L3:
iload 3
ldc "Invalid padding: %s"
iconst_1
anewarray java/lang/Object
dup
iconst_0
iload 2
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 0
astore 4
aload 0
arraylength
iload 1
if_icmpge L4
iload 1
iload 2
iadd
istore 1
iload 1
newarray double
astore 4
aload 0
iconst_0
aload 4
iconst_0
aload 0
arraylength
iload 1
invokestatic java/lang/Math/min(II)I
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
L4:
aload 4
areturn
L0:
iconst_0
istore 3
goto L1
L2:
iconst_0
istore 3
goto L3
.limit locals 5
.limit stack 6
.end method

.method private static transient a([[D)[D
aload 0
arraylength
istore 3
iconst_0
istore 1
iconst_0
istore 2
L0:
iload 1
iload 3
if_icmpge L1
iload 2
aload 0
iload 1
aaload
arraylength
iadd
istore 2
iload 1
iconst_1
iadd
istore 1
goto L0
L1:
iload 2
newarray double
astore 4
aload 0
arraylength
istore 3
iconst_0
istore 2
iconst_0
istore 1
L2:
iload 1
iload 3
if_icmpge L3
aload 0
iload 1
aaload
astore 5
aload 5
iconst_0
aload 4
iload 2
aload 5
arraylength
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
iload 2
aload 5
arraylength
iadd
istore 2
iload 1
iconst_1
iadd
istore 1
goto L2
L3:
aload 4
areturn
.limit locals 6
.limit stack 5
.end method

.method private static transient b([D)D
iconst_1
istore 3
aload 0
arraylength
ifle L0
iconst_1
istore 4
L1:
iload 4
invokestatic com/a/b/b/cn/a(Z)V
aload 0
iconst_0
daload
dstore 1
L2:
iload 3
aload 0
arraylength
if_icmpge L3
dload 1
aload 0
iload 3
daload
invokestatic java/lang/Math/max(DD)D
dstore 1
iload 3
iconst_1
iadd
istore 3
goto L2
L0:
iconst_0
istore 4
goto L1
L3:
dload 1
dreturn
.limit locals 5
.limit stack 4
.end method

.method private static b([DD)I
aload 0
dload 1
iconst_0
aload 0
arraylength
invokestatic com/a/b/l/i/c([DDII)I
ireturn
.limit locals 3
.limit stack 5
.end method

.method static synthetic b([DDII)I
aload 0
dload 1
iload 3
iload 4
invokestatic com/a/b/l/i/d([DDII)I
ireturn
.limit locals 5
.limit stack 5
.end method

.method private static b()Ljava/util/Comparator;
getstatic com/a/b/l/l/a Lcom/a/b/l/l;
areturn
.limit locals 0
.limit stack 1
.end method

.method private static b(D)Z
iconst_1
istore 3
ldc2_w -doubleinfinity
dload 0
dcmpg
ifge L0
iconst_1
istore 2
L1:
dload 0
ldc2_w +doubleinfinity
dcmpg
ifge L2
L3:
iload 3
iload 2
iand
ireturn
L0:
iconst_0
istore 2
goto L1
L2:
iconst_0
istore 3
goto L3
.limit locals 4
.limit stack 4
.end method

.method private static c([DD)I
aload 0
dload 1
iconst_0
aload 0
arraylength
invokestatic com/a/b/l/i/d([DDII)I
ireturn
.limit locals 3
.limit stack 5
.end method

.method private static c([DDII)I
L0:
iload 3
iload 4
if_icmpge L1
aload 0
iload 3
daload
dload 1
dcmpl
ifne L2
iload 3
ireturn
L2:
iload 3
iconst_1
iadd
istore 3
goto L0
L1:
iconst_m1
ireturn
.limit locals 5
.limit stack 4
.end method

.method private static transient c([D)Ljava/util/List;
aload 0
arraylength
ifne L0
invokestatic java/util/Collections/emptyList()Ljava/util/List;
areturn
L0:
new com/a/b/l/j
dup
aload 0
invokespecial com/a/b/l/j/<init>([D)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private static c()Ljava/util/regex/Pattern;
.annotation invisible Lcom/a/b/a/c;
a s = "regular expressions"
.end annotation
ldc "(?:\\d++(?:\\.\\d*+)?|\\.\\d++)"
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
ldc "(?:[eE][+-]?\\d++)?[fFdD]?"
invokevirtual java/lang/String/concat(Ljava/lang/String;)Ljava/lang/String;
astore 0
ldc "(?:\\p{XDigit}++(?:\\.\\p{XDigit}*+)?|\\.\\p{XDigit}++)"
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 1
new java/lang/StringBuilder
dup
aload 1
invokevirtual java/lang/String/length()I
bipush 25
iadd
invokespecial java/lang/StringBuilder/<init>(I)V
ldc "0[xX]"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "[pP][+-]?\\d++[fFdD]?"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 1
aload 0
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 0
aload 1
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 1
new java/lang/StringBuilder
dup
aload 0
invokevirtual java/lang/String/length()I
bipush 23
iadd
aload 1
invokevirtual java/lang/String/length()I
iadd
invokespecial java/lang/StringBuilder/<init>(I)V
ldc "[+-]?(?:NaN|Infinity|"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "|"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ")"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic java/util/regex/Pattern/compile(Ljava/lang/String;)Ljava/util/regex/Pattern;
areturn
.limit locals 2
.limit stack 4
.end method

.method private static d([DDII)I
iload 4
iconst_1
isub
istore 4
L0:
iload 4
iload 3
if_icmplt L1
aload 0
iload 4
daload
dload 1
dcmpl
ifne L2
iload 4
ireturn
L2:
iload 4
iconst_1
isub
istore 4
goto L0
L1:
iconst_m1
ireturn
.limit locals 5
.limit stack 4
.end method
