.bytecode 50.0
.class final synchronized com/a/b/m/y
.super com/a/b/m/ax

.field private static final 'a' Lcom/a/b/m/ac;

.field private final 'b' Ljava/util/Map;

.method static <clinit>()V
new com/a/b/m/ac
dup
iconst_0
invokespecial com/a/b/m/ac/<init>(B)V
putstatic com/a/b/m/y/a Lcom/a/b/m/ac;
return
.limit locals 0
.limit stack 3
.end method

.method private <init>()V
aload 0
invokespecial com/a/b/m/ax/<init>()V
aload 0
invokestatic com/a/b/d/sz/c()Ljava/util/HashMap;
putfield com/a/b/m/y/b Ljava/util/Map;
return
.limit locals 1
.limit stack 2
.end method

.method static a(Ljava/lang/reflect/Type;)Lcom/a/b/d/jt;
new com/a/b/m/y
dup
invokespecial com/a/b/m/y/<init>()V
astore 1
aload 1
iconst_1
anewarray java/lang/reflect/Type
dup
iconst_0
getstatic com/a/b/m/y/a Lcom/a/b/m/ac;
aload 0
invokevirtual com/a/b/m/ac/a(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;
aastore
invokevirtual com/a/b/m/y/a([Ljava/lang/reflect/Type;)V
aload 1
getfield com/a/b/m/y/b Ljava/util/Map;
invokestatic com/a/b/d/jt/a(Ljava/util/Map;)Lcom/a/b/d/jt;
areturn
.limit locals 2
.limit stack 6
.end method

.method private a(Lcom/a/b/m/ab;Ljava/lang/reflect/Type;)V
aload 0
getfield com/a/b/m/y/b Ljava/util/Map;
aload 1
invokeinterface java/util/Map/containsKey(Ljava/lang/Object;)Z 1
ifeq L0
L1:
return
L0:
aload 2
astore 3
L2:
aload 3
ifnull L3
aload 1
aload 3
invokevirtual com/a/b/m/ab/b(Ljava/lang/reflect/Type;)Z
ifeq L4
L5:
aload 2
ifnull L1
aload 0
getfield com/a/b/m/y/b Ljava/util/Map;
aload 2
invokestatic com/a/b/m/ab/a(Ljava/lang/reflect/Type;)Ljava/lang/Object;
invokeinterface java/util/Map/remove(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast java/lang/reflect/Type
astore 2
goto L5
L4:
aload 0
getfield com/a/b/m/y/b Ljava/util/Map;
aload 3
invokestatic com/a/b/m/ab/a(Ljava/lang/reflect/Type;)Ljava/lang/Object;
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast java/lang/reflect/Type
astore 3
goto L2
L3:
aload 0
getfield com/a/b/m/y/b Ljava/util/Map;
aload 1
aload 2
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
return
.limit locals 4
.limit stack 3
.end method

.method final a(Ljava/lang/Class;)V
aload 0
iconst_1
anewarray java/lang/reflect/Type
dup
iconst_0
aload 1
invokevirtual java/lang/Class/getGenericSuperclass()Ljava/lang/reflect/Type;
aastore
invokevirtual com/a/b/m/y/a([Ljava/lang/reflect/Type;)V
aload 0
aload 1
invokevirtual java/lang/Class/getGenericInterfaces()[Ljava/lang/reflect/Type;
invokevirtual com/a/b/m/y/a([Ljava/lang/reflect/Type;)V
return
.limit locals 2
.limit stack 5
.end method

.method final a(Ljava/lang/reflect/ParameterizedType;)V
aload 1
invokeinterface java/lang/reflect/ParameterizedType/getRawType()Ljava/lang/reflect/Type; 0
checkcast java/lang/Class
astore 6
aload 6
invokevirtual java/lang/Class/getTypeParameters()[Ljava/lang/reflect/TypeVariable;
astore 7
aload 1
invokeinterface java/lang/reflect/ParameterizedType/getActualTypeArguments()[Ljava/lang/reflect/Type; 0
astore 8
aload 7
arraylength
aload 8
arraylength
if_icmpne L0
iconst_1
istore 3
L1:
iload 3
invokestatic com/a/b/b/cn/b(Z)V
iconst_0
istore 2
L2:
iload 2
aload 7
arraylength
if_icmpge L3
new com/a/b/m/ab
dup
aload 7
iload 2
aaload
invokespecial com/a/b/m/ab/<init>(Ljava/lang/reflect/TypeVariable;)V
astore 9
aload 8
iload 2
aaload
astore 4
aload 0
getfield com/a/b/m/y/b Ljava/util/Map;
aload 9
invokeinterface java/util/Map/containsKey(Ljava/lang/Object;)Z 1
ifne L4
aload 4
astore 5
L5:
aload 5
ifnull L6
aload 9
aload 5
invokevirtual com/a/b/m/ab/b(Ljava/lang/reflect/Type;)Z
ifeq L7
L8:
aload 4
ifnull L4
aload 0
getfield com/a/b/m/y/b Ljava/util/Map;
aload 4
invokestatic com/a/b/m/ab/a(Ljava/lang/reflect/Type;)Ljava/lang/Object;
invokeinterface java/util/Map/remove(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast java/lang/reflect/Type
astore 4
goto L8
L0:
iconst_0
istore 3
goto L1
L7:
aload 0
getfield com/a/b/m/y/b Ljava/util/Map;
aload 5
invokestatic com/a/b/m/ab/a(Ljava/lang/reflect/Type;)Ljava/lang/Object;
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast java/lang/reflect/Type
astore 5
goto L5
L6:
aload 0
getfield com/a/b/m/y/b Ljava/util/Map;
aload 9
aload 4
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
L4:
iload 2
iconst_1
iadd
istore 2
goto L2
L3:
aload 0
iconst_1
anewarray java/lang/reflect/Type
dup
iconst_0
aload 6
aastore
invokevirtual com/a/b/m/y/a([Ljava/lang/reflect/Type;)V
aload 0
iconst_1
anewarray java/lang/reflect/Type
dup
iconst_0
aload 1
invokeinterface java/lang/reflect/ParameterizedType/getOwnerType()Ljava/lang/reflect/Type; 0
aastore
invokevirtual com/a/b/m/y/a([Ljava/lang/reflect/Type;)V
return
.limit locals 10
.limit stack 5
.end method

.method final a(Ljava/lang/reflect/TypeVariable;)V
aload 0
aload 1
invokeinterface java/lang/reflect/TypeVariable/getBounds()[Ljava/lang/reflect/Type; 0
invokevirtual com/a/b/m/y/a([Ljava/lang/reflect/Type;)V
return
.limit locals 2
.limit stack 2
.end method

.method final a(Ljava/lang/reflect/WildcardType;)V
aload 0
aload 1
invokeinterface java/lang/reflect/WildcardType/getUpperBounds()[Ljava/lang/reflect/Type; 0
invokevirtual com/a/b/m/y/a([Ljava/lang/reflect/Type;)V
return
.limit locals 2
.limit stack 2
.end method
