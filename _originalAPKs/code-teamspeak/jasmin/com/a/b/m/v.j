.bytecode 50.0
.class public synchronized abstract com/a/b/m/v
.super com/a/b/m/u
.annotation invisible Lcom/a/b/a/a;
.end annotation

.field final 'a' Ljava/lang/reflect/TypeVariable;

.method protected <init>()V
aload 0
invokespecial com/a/b/m/u/<init>()V
aload 0
invokevirtual com/a/b/m/v/a()Ljava/lang/reflect/Type;
astore 1
aload 1
instanceof java/lang/reflect/TypeVariable
ldc "%s should be a type variable."
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 1
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 0
aload 1
checkcast java/lang/reflect/TypeVariable
putfield com/a/b/m/v/a Ljava/lang/reflect/TypeVariable;
return
.limit locals 2
.limit stack 6
.end method

.method public final equals(Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 1
instanceof com/a/b/m/v
ifeq L0
aload 1
checkcast com/a/b/m/v
astore 1
aload 0
getfield com/a/b/m/v/a Ljava/lang/reflect/TypeVariable;
aload 1
getfield com/a/b/m/v/a Ljava/lang/reflect/TypeVariable;
invokevirtual java/lang/Object/equals(Ljava/lang/Object;)Z
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final hashCode()I
aload 0
getfield com/a/b/m/v/a Ljava/lang/reflect/TypeVariable;
invokevirtual java/lang/Object/hashCode()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public toString()Ljava/lang/String;
aload 0
getfield com/a/b/m/v/a Ljava/lang/reflect/TypeVariable;
invokevirtual java/lang/Object/toString()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method
