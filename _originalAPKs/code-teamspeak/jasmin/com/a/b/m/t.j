.bytecode 50.0
.class public final synchronized com/a/b/m/t
.super java/lang/Object
.annotation invisible Lcom/a/b/a/a;
.end annotation

.method private <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a(Ljava/lang/Class;Ljava/lang/reflect/InvocationHandler;)Ljava/lang/Object;
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
invokevirtual java/lang/Class/isInterface()Z
ldc "%s is not an interface"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 0
aload 0
invokevirtual java/lang/Class/getClassLoader()Ljava/lang/ClassLoader;
iconst_1
anewarray java/lang/Class
dup
iconst_0
aload 0
aastore
aload 1
invokestatic java/lang/reflect/Proxy/newProxyInstance(Ljava/lang/ClassLoader;[Ljava/lang/Class;Ljava/lang/reflect/InvocationHandler;)Ljava/lang/Object;
invokevirtual java/lang/Class/cast(Ljava/lang/Object;)Ljava/lang/Object;
areturn
.limit locals 2
.limit stack 6
.end method

.method private static a(Ljava/lang/Class;)Ljava/lang/String;
aload 0
invokevirtual java/lang/Class/getName()Ljava/lang/String;
invokestatic com/a/b/m/t/a(Ljava/lang/String;)Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
aload 0
bipush 46
invokevirtual java/lang/String/lastIndexOf(I)I
istore 1
iload 1
ifge L0
ldc ""
areturn
L0:
aload 0
iconst_0
iload 1
invokevirtual java/lang/String/substring(II)Ljava/lang/String;
areturn
.limit locals 2
.limit stack 3
.end method

.method private static transient a([Ljava/lang/Class;)V
.catch java/lang/ClassNotFoundException from L0 to L1 using L2
aload 0
arraylength
istore 2
iconst_0
istore 1
L3:
iload 1
iload 2
if_icmpge L4
aload 0
iload 1
aaload
astore 3
L0:
aload 3
invokevirtual java/lang/Class/getName()Ljava/lang/String;
iconst_1
aload 3
invokevirtual java/lang/Class/getClassLoader()Ljava/lang/ClassLoader;
invokestatic java/lang/Class/forName(Ljava/lang/String;ZLjava/lang/ClassLoader;)Ljava/lang/Class;
pop
L1:
iload 1
iconst_1
iadd
istore 1
goto L3
L2:
astore 0
new java/lang/AssertionError
dup
aload 0
invokespecial java/lang/AssertionError/<init>(Ljava/lang/Object;)V
athrow
L4:
return
.limit locals 4
.limit stack 3
.end method
