.bytecode 50.0
.class synchronized com/a/b/m/l
.super com/a/b/m/k

.field final 'a' Ljava/lang/reflect/Constructor;

.method <init>(Ljava/lang/reflect/Constructor;)V
aload 0
aload 1
invokespecial com/a/b/m/k/<init>(Ljava/lang/reflect/AccessibleObject;)V
aload 0
aload 1
putfield com/a/b/m/l/a Ljava/lang/reflect/Constructor;
return
.limit locals 2
.limit stack 2
.end method

.method private h()Z
aload 0
getfield com/a/b/m/l/a Ljava/lang/reflect/Constructor;
invokevirtual java/lang/reflect/Constructor/getDeclaringClass()Ljava/lang/Class;
astore 1
aload 1
invokevirtual java/lang/Class/getEnclosingConstructor()Ljava/lang/reflect/Constructor;
ifnull L0
L1:
iconst_1
ireturn
L0:
aload 1
invokevirtual java/lang/Class/getEnclosingMethod()Ljava/lang/reflect/Method;
astore 2
aload 2
ifnull L2
aload 2
invokevirtual java/lang/reflect/Method/getModifiers()I
invokestatic java/lang/reflect/Modifier/isStatic(I)Z
ifeq L1
iconst_0
ireturn
L2:
aload 1
invokevirtual java/lang/Class/getEnclosingClass()Ljava/lang/Class;
ifnull L3
aload 1
invokevirtual java/lang/Class/getModifiers()I
invokestatic java/lang/reflect/Modifier/isStatic(I)Z
ifeq L1
L3:
iconst_0
ireturn
.limit locals 3
.limit stack 1
.end method

.method final a(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
.catch java/lang/InstantiationException from L0 to L1 using L2
L0:
aload 0
getfield com/a/b/m/l/a Ljava/lang/reflect/Constructor;
aload 2
invokevirtual java/lang/reflect/Constructor/newInstance([Ljava/lang/Object;)Ljava/lang/Object;
astore 1
L1:
aload 1
areturn
L2:
astore 1
aload 0
getfield com/a/b/m/l/a Ljava/lang/reflect/Constructor;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 2
new java/lang/RuntimeException
dup
new java/lang/StringBuilder
dup
aload 2
invokevirtual java/lang/String/length()I
bipush 8
iadd
invokespecial java/lang/StringBuilder/<init>(I)V
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " failed."
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aload 1
invokespecial java/lang/RuntimeException/<init>(Ljava/lang/String;Ljava/lang/Throwable;)V
athrow
.limit locals 3
.limit stack 6
.end method

.method public final b()Z
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final c()Z
aload 0
getfield com/a/b/m/l/a Ljava/lang/reflect/Constructor;
invokevirtual java/lang/reflect/Constructor/isVarArgs()Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method d()[Ljava/lang/reflect/Type;
aload 0
getfield com/a/b/m/l/a Ljava/lang/reflect/Constructor;
invokevirtual java/lang/reflect/Constructor/getGenericParameterTypes()[Ljava/lang/reflect/Type;
astore 3
aload 3
astore 2
aload 3
arraylength
ifle L0
aload 0
getfield com/a/b/m/l/a Ljava/lang/reflect/Constructor;
invokevirtual java/lang/reflect/Constructor/getDeclaringClass()Ljava/lang/Class;
astore 2
aload 2
invokevirtual java/lang/Class/getEnclosingConstructor()Ljava/lang/reflect/Constructor;
ifnull L1
iconst_1
istore 1
L2:
aload 3
astore 2
iload 1
ifeq L0
aload 0
getfield com/a/b/m/l/a Ljava/lang/reflect/Constructor;
invokevirtual java/lang/reflect/Constructor/getParameterTypes()[Ljava/lang/Class;
astore 4
aload 3
astore 2
aload 3
arraylength
aload 4
arraylength
if_icmpne L0
aload 3
astore 2
aload 4
iconst_0
aaload
aload 0
invokevirtual com/a/b/m/l/getDeclaringClass()Ljava/lang/Class;
invokevirtual java/lang/Class/getEnclosingClass()Ljava/lang/Class;
if_acmpne L0
aload 3
iconst_1
aload 3
arraylength
invokestatic java/util/Arrays/copyOfRange([Ljava/lang/Object;II)[Ljava/lang/Object;
checkcast [Ljava/lang/reflect/Type;
astore 2
L0:
aload 2
areturn
L1:
aload 2
invokevirtual java/lang/Class/getEnclosingMethod()Ljava/lang/reflect/Method;
astore 4
aload 4
ifnull L3
aload 4
invokevirtual java/lang/reflect/Method/getModifiers()I
invokestatic java/lang/reflect/Modifier/isStatic(I)Z
ifne L4
iconst_1
istore 1
goto L2
L4:
iconst_0
istore 1
goto L2
L3:
aload 2
invokevirtual java/lang/Class/getEnclosingClass()Ljava/lang/Class;
ifnull L5
aload 2
invokevirtual java/lang/Class/getModifiers()I
invokestatic java/lang/reflect/Modifier/isStatic(I)Z
ifne L5
iconst_1
istore 1
goto L2
L5:
iconst_0
istore 1
goto L2
.limit locals 5
.limit stack 3
.end method

.method e()[Ljava/lang/reflect/Type;
aload 0
getfield com/a/b/m/l/a Ljava/lang/reflect/Constructor;
invokevirtual java/lang/reflect/Constructor/getGenericExceptionTypes()[Ljava/lang/reflect/Type;
areturn
.limit locals 1
.limit stack 1
.end method

.method final f()[[Ljava/lang/annotation/Annotation;
aload 0
getfield com/a/b/m/l/a Ljava/lang/reflect/Constructor;
invokevirtual java/lang/reflect/Constructor/getParameterAnnotations()[[Ljava/lang/annotation/Annotation;
areturn
.limit locals 1
.limit stack 1
.end method

.method g()Ljava/lang/reflect/Type;
aload 0
invokevirtual com/a/b/m/l/getDeclaringClass()Ljava/lang/Class;
astore 2
aload 2
invokevirtual java/lang/Class/getTypeParameters()[Ljava/lang/reflect/TypeVariable;
astore 3
aload 2
astore 1
aload 3
arraylength
ifle L0
aload 2
aload 3
invokestatic com/a/b/m/ay/a(Ljava/lang/Class;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;
astore 1
L0:
aload 1
areturn
.limit locals 4
.limit stack 2
.end method

.method public final getTypeParameters()[Ljava/lang/reflect/TypeVariable;
aload 0
invokevirtual com/a/b/m/l/getDeclaringClass()Ljava/lang/Class;
invokevirtual java/lang/Class/getTypeParameters()[Ljava/lang/reflect/TypeVariable;
astore 1
aload 0
getfield com/a/b/m/l/a Ljava/lang/reflect/Constructor;
invokevirtual java/lang/reflect/Constructor/getTypeParameters()[Ljava/lang/reflect/TypeVariable;
astore 2
aload 1
arraylength
aload 2
arraylength
iadd
anewarray java/lang/reflect/TypeVariable
astore 3
aload 1
iconst_0
aload 3
iconst_0
aload 1
arraylength
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 2
iconst_0
aload 3
aload 1
arraylength
aload 2
arraylength
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 3
areturn
.limit locals 4
.limit stack 5
.end method
