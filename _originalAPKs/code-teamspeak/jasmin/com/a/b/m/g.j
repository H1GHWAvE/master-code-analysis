.bytecode 50.0
.class synchronized com/a/b/m/g
.super java/lang/reflect/AccessibleObject
.implements java/lang/reflect/Member

.field private final 'a' Ljava/lang/reflect/AccessibleObject;

.field private final 'b' Ljava/lang/reflect/Member;

.method <init>(Ljava/lang/reflect/AccessibleObject;)V
aload 0
invokespecial java/lang/reflect/AccessibleObject/<init>()V
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
aload 1
putfield com/a/b/m/g/a Ljava/lang/reflect/AccessibleObject;
aload 0
aload 1
checkcast java/lang/reflect/Member
putfield com/a/b/m/g/b Ljava/lang/reflect/Member;
return
.limit locals 2
.limit stack 2
.end method

.method private b()Z
aload 0
invokevirtual com/a/b/m/g/getModifiers()I
invokestatic java/lang/reflect/Modifier/isPublic(I)Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private c()Z
aload 0
invokevirtual com/a/b/m/g/getModifiers()I
invokestatic java/lang/reflect/Modifier/isProtected(I)Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private d()Z
aload 0
invokevirtual com/a/b/m/g/getModifiers()I
invokestatic java/lang/reflect/Modifier/isPrivate(I)Z
ifne L0
aload 0
invokevirtual com/a/b/m/g/getModifiers()I
invokestatic java/lang/reflect/Modifier/isPublic(I)Z
ifne L0
aload 0
invokevirtual com/a/b/m/g/getModifiers()I
invokestatic java/lang/reflect/Modifier/isProtected(I)Z
ifne L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method private e()Z
aload 0
invokevirtual com/a/b/m/g/getModifiers()I
invokestatic java/lang/reflect/Modifier/isPrivate(I)Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private f()Z
aload 0
invokevirtual com/a/b/m/g/getModifiers()I
invokestatic java/lang/reflect/Modifier/isStatic(I)Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private g()Z
aload 0
invokevirtual com/a/b/m/g/getModifiers()I
invokestatic java/lang/reflect/Modifier/isFinal(I)Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private h()Z
aload 0
invokevirtual com/a/b/m/g/getModifiers()I
invokestatic java/lang/reflect/Modifier/isAbstract(I)Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private i()Z
aload 0
invokevirtual com/a/b/m/g/getModifiers()I
invokestatic java/lang/reflect/Modifier/isNative(I)Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private j()Z
aload 0
invokevirtual com/a/b/m/g/getModifiers()I
invokestatic java/lang/reflect/Modifier/isSynchronized(I)Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private k()Z
aload 0
invokevirtual com/a/b/m/g/getModifiers()I
invokestatic java/lang/reflect/Modifier/isVolatile(I)Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private l()Z
aload 0
invokevirtual com/a/b/m/g/getModifiers()I
invokestatic java/lang/reflect/Modifier/isTransient(I)Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public a()Lcom/a/b/m/ae;
aload 0
invokevirtual com/a/b/m/g/getDeclaringClass()Ljava/lang/Class;
invokestatic com/a/b/m/ae/a(Ljava/lang/Class;)Lcom/a/b/m/ae;
areturn
.limit locals 1
.limit stack 1
.end method

.method public equals(Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
iconst_0
istore 3
iload 3
istore 2
aload 1
instanceof com/a/b/m/g
ifeq L0
aload 1
checkcast com/a/b/m/g
astore 1
iload 3
istore 2
aload 0
invokevirtual com/a/b/m/g/a()Lcom/a/b/m/ae;
aload 1
invokevirtual com/a/b/m/g/a()Lcom/a/b/m/ae;
invokevirtual com/a/b/m/ae/equals(Ljava/lang/Object;)Z
ifeq L0
iload 3
istore 2
aload 0
getfield com/a/b/m/g/b Ljava/lang/reflect/Member;
aload 1
getfield com/a/b/m/g/b Ljava/lang/reflect/Member;
invokevirtual java/lang/Object/equals(Ljava/lang/Object;)Z
ifeq L0
iconst_1
istore 2
L0:
iload 2
ireturn
.limit locals 4
.limit stack 2
.end method

.method public final getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;
aload 0
getfield com/a/b/m/g/a Ljava/lang/reflect/AccessibleObject;
aload 1
invokevirtual java/lang/reflect/AccessibleObject/getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;
areturn
.limit locals 2
.limit stack 2
.end method

.method public final getAnnotations()[Ljava/lang/annotation/Annotation;
aload 0
getfield com/a/b/m/g/a Ljava/lang/reflect/AccessibleObject;
invokevirtual java/lang/reflect/AccessibleObject/getAnnotations()[Ljava/lang/annotation/Annotation;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final getDeclaredAnnotations()[Ljava/lang/annotation/Annotation;
aload 0
getfield com/a/b/m/g/a Ljava/lang/reflect/AccessibleObject;
invokevirtual java/lang/reflect/AccessibleObject/getDeclaredAnnotations()[Ljava/lang/annotation/Annotation;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getDeclaringClass()Ljava/lang/Class;
aload 0
getfield com/a/b/m/g/b Ljava/lang/reflect/Member;
invokeinterface java/lang/reflect/Member/getDeclaringClass()Ljava/lang/Class; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method public final getModifiers()I
aload 0
getfield com/a/b/m/g/b Ljava/lang/reflect/Member;
invokeinterface java/lang/reflect/Member/getModifiers()I 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final getName()Ljava/lang/String;
aload 0
getfield com/a/b/m/g/b Ljava/lang/reflect/Member;
invokeinterface java/lang/reflect/Member/getName()Ljava/lang/String; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method public hashCode()I
aload 0
getfield com/a/b/m/g/b Ljava/lang/reflect/Member;
invokevirtual java/lang/Object/hashCode()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final isAccessible()Z
aload 0
getfield com/a/b/m/g/a Ljava/lang/reflect/AccessibleObject;
invokevirtual java/lang/reflect/AccessibleObject/isAccessible()Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final isAnnotationPresent(Ljava/lang/Class;)Z
aload 0
getfield com/a/b/m/g/a Ljava/lang/reflect/AccessibleObject;
aload 1
invokevirtual java/lang/reflect/AccessibleObject/isAnnotationPresent(Ljava/lang/Class;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final isSynthetic()Z
aload 0
getfield com/a/b/m/g/b Ljava/lang/reflect/Member;
invokeinterface java/lang/reflect/Member/isSynthetic()Z 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final setAccessible(Z)V
aload 0
getfield com/a/b/m/g/a Ljava/lang/reflect/AccessibleObject;
iload 1
invokevirtual java/lang/reflect/AccessibleObject/setAccessible(Z)V
return
.limit locals 2
.limit stack 2
.end method

.method public toString()Ljava/lang/String;
aload 0
getfield com/a/b/m/g/b Ljava/lang/reflect/Member;
invokevirtual java/lang/Object/toString()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method
