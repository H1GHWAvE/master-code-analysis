.bytecode 50.0
.class synchronized com/a/b/m/z
.super java/lang/Object

.field private final 'a' Lcom/a/b/d/jt;

.method <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
invokestatic com/a/b/d/jt/k()Lcom/a/b/d/jt;
putfield com/a/b/m/z/a Lcom/a/b/d/jt;
return
.limit locals 1
.limit stack 2
.end method

.method private <init>(Lcom/a/b/d/jt;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
putfield com/a/b/m/z/a Lcom/a/b/d/jt;
return
.limit locals 2
.limit stack 2
.end method

.method private a(Ljava/lang/reflect/TypeVariable;)Ljava/lang/reflect/Type;
aload 0
aload 1
new com/a/b/m/aa
dup
aload 0
aload 1
aload 0
invokespecial com/a/b/m/aa/<init>(Lcom/a/b/m/z;Ljava/lang/reflect/TypeVariable;Lcom/a/b/m/z;)V
invokevirtual com/a/b/m/z/a(Ljava/lang/reflect/TypeVariable;Lcom/a/b/m/z;)Ljava/lang/reflect/Type;
areturn
.limit locals 2
.limit stack 7
.end method

.method final a(Ljava/util/Map;)Lcom/a/b/m/z;
invokestatic com/a/b/d/jt/l()Lcom/a/b/d/ju;
astore 3
aload 3
aload 0
getfield com/a/b/m/z/a Lcom/a/b/d/jt;
invokevirtual com/a/b/d/ju/a(Ljava/util/Map;)Lcom/a/b/d/ju;
pop
aload 1
invokeinterface java/util/Map/entrySet()Ljava/util/Set; 0
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
astore 1
L0:
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/util/Map$Entry
astore 5
aload 5
invokeinterface java/util/Map$Entry/getKey()Ljava/lang/Object; 0
checkcast com/a/b/m/ab
astore 4
aload 5
invokeinterface java/util/Map$Entry/getValue()Ljava/lang/Object; 0
checkcast java/lang/reflect/Type
astore 5
aload 4
aload 5
invokevirtual com/a/b/m/ab/b(Ljava/lang/reflect/Type;)Z
ifne L2
iconst_1
istore 2
L3:
iload 2
ldc "Type variable %s bound to itself"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 4
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 3
aload 4
aload 5
invokevirtual com/a/b/d/ju/a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/ju;
pop
goto L0
L2:
iconst_0
istore 2
goto L3
L1:
new com/a/b/m/z
dup
aload 3
invokevirtual com/a/b/d/ju/a()Lcom/a/b/d/jt;
invokespecial com/a/b/m/z/<init>(Lcom/a/b/d/jt;)V
areturn
.limit locals 6
.limit stack 6
.end method

.method a(Ljava/lang/reflect/TypeVariable;Lcom/a/b/m/z;)Ljava/lang/reflect/Type;
aload 0
getfield com/a/b/m/z/a Lcom/a/b/d/jt;
new com/a/b/m/ab
dup
aload 1
invokespecial com/a/b/m/ab/<init>(Ljava/lang/reflect/TypeVariable;)V
invokevirtual com/a/b/d/jt/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/reflect/Type
astore 3
aload 3
ifnonnull L0
aload 1
invokeinterface java/lang/reflect/TypeVariable/getBounds()[Ljava/lang/reflect/Type; 0
astore 3
aload 3
arraylength
ifne L1
L2:
aload 1
areturn
L1:
new com/a/b/m/w
dup
aload 2
iconst_0
invokespecial com/a/b/m/w/<init>(Lcom/a/b/m/z;B)V
aload 3
invokevirtual com/a/b/m/w/a([Ljava/lang/reflect/Type;)[Ljava/lang/reflect/Type;
astore 2
getstatic com/a/b/m/bm/a Z
ifeq L3
aload 3
aload 2
invokestatic java/util/Arrays/equals([Ljava/lang/Object;[Ljava/lang/Object;)Z
ifne L2
L3:
aload 1
invokeinterface java/lang/reflect/TypeVariable/getGenericDeclaration()Ljava/lang/reflect/GenericDeclaration; 0
aload 1
invokeinterface java/lang/reflect/TypeVariable/getName()Ljava/lang/String; 0
aload 2
invokestatic com/a/b/m/ay/a(Ljava/lang/reflect/GenericDeclaration;Ljava/lang/String;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/TypeVariable;
areturn
L0:
new com/a/b/m/w
dup
aload 2
iconst_0
invokespecial com/a/b/m/w/<init>(Lcom/a/b/m/z;B)V
aload 3
invokevirtual com/a/b/m/w/a(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;
areturn
.limit locals 4
.limit stack 4
.end method
