.bytecode 50.0
.class final synchronized com/a/b/m/ac
.super java/lang/Object

.field private final 'a' Ljava/util/concurrent/atomic/AtomicInteger;

.method private <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
new java/util/concurrent/atomic/AtomicInteger
dup
invokespecial java/util/concurrent/atomic/AtomicInteger/<init>()V
putfield com/a/b/m/ac/a Ljava/util/concurrent/atomic/AtomicInteger;
return
.limit locals 1
.limit stack 3
.end method

.method synthetic <init>(B)V
aload 0
invokespecial com/a/b/m/ac/<init>()V
return
.limit locals 2
.limit stack 1
.end method

.method private a([Ljava/lang/reflect/Type;)[Ljava/lang/reflect/Type;
aload 1
arraylength
anewarray java/lang/reflect/Type
astore 3
iconst_0
istore 2
L0:
iload 2
aload 1
arraylength
if_icmpge L1
aload 3
iload 2
aload 0
aload 1
iload 2
aaload
invokevirtual com/a/b/m/ac/a(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;
aastore
iload 2
iconst_1
iadd
istore 2
goto L0
L1:
aload 3
areturn
.limit locals 4
.limit stack 5
.end method

.method private b(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 1
ifnonnull L0
aconst_null
areturn
L0:
aload 0
aload 1
invokevirtual com/a/b/m/ac/a(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;
areturn
.limit locals 2
.limit stack 2
.end method

.method final a(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 1
instanceof java/lang/Class
ifeq L0
L1:
aload 1
areturn
L0:
aload 1
instanceof java/lang/reflect/TypeVariable
ifne L1
aload 1
instanceof java/lang/reflect/GenericArrayType
ifeq L2
aload 0
aload 1
checkcast java/lang/reflect/GenericArrayType
invokeinterface java/lang/reflect/GenericArrayType/getGenericComponentType()Ljava/lang/reflect/Type; 0
invokevirtual com/a/b/m/ac/a(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;
invokestatic com/a/b/m/ay/a(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;
areturn
L2:
aload 1
instanceof java/lang/reflect/ParameterizedType
ifeq L3
aload 1
checkcast java/lang/reflect/ParameterizedType
astore 3
aload 3
invokeinterface java/lang/reflect/ParameterizedType/getOwnerType()Ljava/lang/reflect/Type; 0
astore 1
aload 1
ifnonnull L4
aconst_null
astore 1
L5:
aload 3
invokeinterface java/lang/reflect/ParameterizedType/getRawType()Ljava/lang/reflect/Type; 0
checkcast java/lang/Class
astore 4
aload 3
invokeinterface java/lang/reflect/ParameterizedType/getActualTypeArguments()[Ljava/lang/reflect/Type; 0
astore 3
aload 3
arraylength
anewarray java/lang/reflect/Type
astore 5
iconst_0
istore 2
L6:
iload 2
aload 3
arraylength
if_icmpge L7
aload 5
iload 2
aload 0
aload 3
iload 2
aaload
invokevirtual com/a/b/m/ac/a(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;
aastore
iload 2
iconst_1
iadd
istore 2
goto L6
L4:
aload 0
aload 1
invokevirtual com/a/b/m/ac/a(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;
astore 1
goto L5
L7:
aload 1
aload 4
aload 5
invokestatic com/a/b/m/ay/a(Ljava/lang/reflect/Type;Ljava/lang/Class;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;
areturn
L3:
aload 1
instanceof java/lang/reflect/WildcardType
ifeq L8
aload 1
checkcast java/lang/reflect/WildcardType
astore 3
aload 3
invokeinterface java/lang/reflect/WildcardType/getLowerBounds()[Ljava/lang/reflect/Type; 0
arraylength
ifne L1
aload 3
invokeinterface java/lang/reflect/WildcardType/getUpperBounds()[Ljava/lang/reflect/Type; 0
astore 1
aload 0
getfield com/a/b/m/ac/a Ljava/util/concurrent/atomic/AtomicInteger;
invokevirtual java/util/concurrent/atomic/AtomicInteger/incrementAndGet()I
istore 2
bipush 38
invokestatic com/a/b/b/bv/a(C)Lcom/a/b/b/bv;
aload 1
invokevirtual com/a/b/b/bv/a([Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 1
ldc com/a/b/m/ac
new java/lang/StringBuilder
dup
aload 1
invokevirtual java/lang/String/length()I
bipush 33
iadd
invokespecial java/lang/StringBuilder/<init>(I)V
ldc "capture#"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 2
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc "-of ? extends "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aload 3
invokeinterface java/lang/reflect/WildcardType/getUpperBounds()[Ljava/lang/reflect/Type; 0
invokestatic com/a/b/m/ay/a(Ljava/lang/reflect/GenericDeclaration;Ljava/lang/String;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/TypeVariable;
areturn
L8:
new java/lang/AssertionError
dup
ldc "must have been one of the known types"
invokespecial java/lang/AssertionError/<init>(Ljava/lang/Object;)V
athrow
.limit locals 6
.limit stack 5
.end method
