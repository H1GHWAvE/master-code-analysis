.bytecode 50.0
.class final synchronized com/a/b/m/bg
.super java/lang/Object
.implements java/io/Serializable
.implements java/lang/reflect/GenericArrayType

.field private static final 'b' J = 0L


.field private final 'a' Ljava/lang/reflect/Type;

.method <init>(Ljava/lang/reflect/Type;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
getstatic com/a/b/m/bh/d Lcom/a/b/m/bh;
aload 1
invokevirtual com/a/b/m/bh/b(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;
putfield com/a/b/m/bg/a Ljava/lang/reflect/Type;
return
.limit locals 2
.limit stack 3
.end method

.method public final equals(Ljava/lang/Object;)Z
aload 1
instanceof java/lang/reflect/GenericArrayType
ifeq L0
aload 1
checkcast java/lang/reflect/GenericArrayType
astore 1
aload 0
invokevirtual com/a/b/m/bg/getGenericComponentType()Ljava/lang/reflect/Type;
aload 1
invokeinterface java/lang/reflect/GenericArrayType/getGenericComponentType()Ljava/lang/reflect/Type; 0
invokestatic com/a/b/b/ce/a(Ljava/lang/Object;Ljava/lang/Object;)Z
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final getGenericComponentType()Ljava/lang/reflect/Type;
aload 0
getfield com/a/b/m/bg/a Ljava/lang/reflect/Type;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final hashCode()I
aload 0
getfield com/a/b/m/bg/a Ljava/lang/reflect/Type;
invokevirtual java/lang/Object/hashCode()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final toString()Ljava/lang/String;
aload 0
getfield com/a/b/m/bg/a Ljava/lang/reflect/Type;
invokestatic com/a/b/m/ay/b(Ljava/lang/reflect/Type;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
ldc "[]"
invokevirtual java/lang/String/concat(Ljava/lang/String;)Ljava/lang/String;
areturn
.limit locals 1
.limit stack 2
.end method
