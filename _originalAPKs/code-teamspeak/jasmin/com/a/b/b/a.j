.bytecode 50.0
.class final synchronized com/a/b/b/a
.super com/a/b/b/ci
.annotation invisible Lcom/a/b/a/b;
.end annotation

.field static final 'a' Lcom/a/b/b/a;

.field private static final 'b' J = 0L


.method static <clinit>()V
new com/a/b/b/a
dup
invokespecial com/a/b/b/a/<init>()V
putstatic com/a/b/b/a/a Lcom/a/b/b/a;
return
.limit locals 0
.limit stack 2
.end method

.method private <init>()V
aload 0
invokespecial com/a/b/b/ci/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method static a()Lcom/a/b/b/ci;
getstatic com/a/b/b/a/a Lcom/a/b/b/a;
areturn
.limit locals 0
.limit stack 1
.end method

.method private static g()Ljava/lang/Object;
getstatic com/a/b/b/a/a Lcom/a/b/b/a;
areturn
.limit locals 0
.limit stack 1
.end method

.method public final a(Lcom/a/b/b/bj;)Lcom/a/b/b/ci;
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/a/b/b/a/a Lcom/a/b/b/a;
areturn
.limit locals 2
.limit stack 1
.end method

.method public final a(Lcom/a/b/b/ci;)Lcom/a/b/b/ci;
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast com/a/b/b/ci
areturn
.limit locals 2
.limit stack 1
.end method

.method public final a(Lcom/a/b/b/dz;)Ljava/lang/Object;
aload 1
invokeinterface com/a/b/b/dz/a()Ljava/lang/Object; 0
ldc "use Optional.orNull() instead of a Supplier that returns null"
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
areturn
.limit locals 2
.limit stack 2
.end method

.method public final a(Ljava/lang/Object;)Ljava/lang/Object;
aload 1
ldc "use Optional.orNull() instead of Optional.or(null)"
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
areturn
.limit locals 2
.limit stack 2
.end method

.method public final b()Z
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final c()Ljava/lang/Object;
new java/lang/IllegalStateException
dup
ldc "Optional.get() cannot be called on an absent value"
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
.limit locals 1
.limit stack 3
.end method

.method public final d()Ljava/lang/Object;
.annotation visible Ljavax/annotation/Nullable;
.end annotation
aconst_null
areturn
.limit locals 1
.limit stack 1
.end method

.method public final e()Ljava/util/Set;
invokestatic java/util/Collections/emptySet()Ljava/util/Set;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final equals(Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 1
aload 0
if_acmpne L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final hashCode()I
ldc_w 1502476572
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final toString()Ljava/lang/String;
ldc "Optional.absent()"
areturn
.limit locals 1
.limit stack 1
.end method
