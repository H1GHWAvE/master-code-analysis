.bytecode 50.0
.class synchronized com/a/b/b/cv
.super java/lang/Object
.implements com/a/b/b/co
.implements java/io/Serializable
.annotation invisible Lcom/a/b/a/c;
a s = "Only used by other GWT-incompatible code."
.end annotation

.field private static final 'b' J = 0L


.field final 'a' Ljava/util/regex/Pattern;

.method <init>(Ljava/util/regex/Pattern;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/util/regex/Pattern
putfield com/a/b/b/cv/a Ljava/util/regex/Pattern;
return
.limit locals 2
.limit stack 2
.end method

.method private a(Ljava/lang/CharSequence;)Z
aload 0
getfield com/a/b/b/cv/a Ljava/util/regex/Pattern;
aload 1
invokevirtual java/util/regex/Pattern/matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;
invokevirtual java/util/regex/Matcher/find()Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final synthetic a(Ljava/lang/Object;)Z
aload 1
checkcast java/lang/CharSequence
astore 1
aload 0
getfield com/a/b/b/cv/a Ljava/util/regex/Pattern;
aload 1
invokevirtual java/util/regex/Pattern/matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;
invokevirtual java/util/regex/Matcher/find()Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public equals(Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
iconst_0
istore 3
iload 3
istore 2
aload 1
instanceof com/a/b/b/cv
ifeq L0
aload 1
checkcast com/a/b/b/cv
astore 1
iload 3
istore 2
aload 0
getfield com/a/b/b/cv/a Ljava/util/regex/Pattern;
invokevirtual java/util/regex/Pattern/pattern()Ljava/lang/String;
aload 1
getfield com/a/b/b/cv/a Ljava/util/regex/Pattern;
invokevirtual java/util/regex/Pattern/pattern()Ljava/lang/String;
invokestatic com/a/b/b/ce/a(Ljava/lang/Object;Ljava/lang/Object;)Z
ifeq L0
iload 3
istore 2
aload 0
getfield com/a/b/b/cv/a Ljava/util/regex/Pattern;
invokevirtual java/util/regex/Pattern/flags()I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aload 1
getfield com/a/b/b/cv/a Ljava/util/regex/Pattern;
invokevirtual java/util/regex/Pattern/flags()I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokestatic com/a/b/b/ce/a(Ljava/lang/Object;Ljava/lang/Object;)Z
ifeq L0
iconst_1
istore 2
L0:
iload 2
ireturn
.limit locals 4
.limit stack 2
.end method

.method public hashCode()I
iconst_2
anewarray java/lang/Object
dup
iconst_0
aload 0
getfield com/a/b/b/cv/a Ljava/util/regex/Pattern;
invokevirtual java/util/regex/Pattern/pattern()Ljava/lang/String;
aastore
dup
iconst_1
aload 0
getfield com/a/b/b/cv/a Ljava/util/regex/Pattern;
invokevirtual java/util/regex/Pattern/flags()I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic java/util/Arrays/hashCode([Ljava/lang/Object;)I
ireturn
.limit locals 1
.limit stack 4
.end method

.method public toString()Ljava/lang/String;
new com/a/b/b/cg
dup
aload 0
getfield com/a/b/b/cv/a Ljava/util/regex/Pattern;
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
invokestatic com/a/b/b/ca/a(Ljava/lang/Class;)Ljava/lang/String;
iconst_0
invokespecial com/a/b/b/cg/<init>(Ljava/lang/String;B)V
ldc "pattern"
aload 0
getfield com/a/b/b/cv/a Ljava/util/regex/Pattern;
invokevirtual java/util/regex/Pattern/pattern()Ljava/lang/String;
invokevirtual com/a/b/b/cg/a(Ljava/lang/String;Ljava/lang/Object;)Lcom/a/b/b/cg;
ldc "pattern.flags"
aload 0
getfield com/a/b/b/cv/a Ljava/util/regex/Pattern;
invokevirtual java/util/regex/Pattern/flags()I
invokestatic java/lang/String/valueOf(I)Ljava/lang/String;
invokevirtual com/a/b/b/cg/a(Ljava/lang/String;Ljava/lang/Object;)Lcom/a/b/b/cg;
invokevirtual com/a/b/b/cg/toString()Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 1
new java/lang/StringBuilder
dup
aload 1
invokevirtual java/lang/String/length()I
bipush 21
iadd
invokespecial java/lang/StringBuilder/<init>(I)V
ldc "Predicates.contains("
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ")"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 2
.limit stack 4
.end method
