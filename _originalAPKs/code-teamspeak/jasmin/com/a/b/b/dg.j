.bytecode 50.0
.class final synchronized com/a/b/b/dg
.super com/a/b/b/ci
.annotation invisible Lcom/a/b/a/b;
.end annotation

.field private static final 'b' J = 0L


.field private final 'a' Ljava/lang/Object;

.method <init>(Ljava/lang/Object;)V
aload 0
invokespecial com/a/b/b/ci/<init>()V
aload 0
aload 1
putfield com/a/b/b/dg/a Ljava/lang/Object;
return
.limit locals 2
.limit stack 2
.end method

.method public final a(Lcom/a/b/b/bj;)Lcom/a/b/b/ci;
new com/a/b/b/dg
dup
aload 1
aload 0
getfield com/a/b/b/dg/a Ljava/lang/Object;
invokeinterface com/a/b/b/bj/e(Ljava/lang/Object;)Ljava/lang/Object; 1
ldc "the Function passed to Optional.transform() must not return null."
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
invokespecial com/a/b/b/dg/<init>(Ljava/lang/Object;)V
areturn
.limit locals 2
.limit stack 4
.end method

.method public final a(Lcom/a/b/b/ci;)Lcom/a/b/b/ci;
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
areturn
.limit locals 2
.limit stack 1
.end method

.method public final a(Lcom/a/b/b/dz;)Ljava/lang/Object;
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
getfield com/a/b/b/dg/a Ljava/lang/Object;
areturn
.limit locals 2
.limit stack 1
.end method

.method public final a(Ljava/lang/Object;)Ljava/lang/Object;
aload 1
ldc "use Optional.orNull() instead of Optional.or(null)"
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
getfield com/a/b/b/dg/a Ljava/lang/Object;
areturn
.limit locals 2
.limit stack 2
.end method

.method public final b()Z
iconst_1
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final c()Ljava/lang/Object;
aload 0
getfield com/a/b/b/dg/a Ljava/lang/Object;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final d()Ljava/lang/Object;
aload 0
getfield com/a/b/b/dg/a Ljava/lang/Object;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final e()Ljava/util/Set;
aload 0
getfield com/a/b/b/dg/a Ljava/lang/Object;
invokestatic java/util/Collections/singleton(Ljava/lang/Object;)Ljava/util/Set;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final equals(Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 1
instanceof com/a/b/b/dg
ifeq L0
aload 1
checkcast com/a/b/b/dg
astore 1
aload 0
getfield com/a/b/b/dg/a Ljava/lang/Object;
aload 1
getfield com/a/b/b/dg/a Ljava/lang/Object;
invokevirtual java/lang/Object/equals(Ljava/lang/Object;)Z
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final hashCode()I
ldc_w 1502476572
aload 0
getfield com/a/b/b/dg/a Ljava/lang/Object;
invokevirtual java/lang/Object/hashCode()I
iadd
ireturn
.limit locals 1
.limit stack 2
.end method

.method public final toString()Ljava/lang/String;
aload 0
getfield com/a/b/b/dg/a Ljava/lang/Object;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 1
new java/lang/StringBuilder
dup
aload 1
invokevirtual java/lang/String/length()I
bipush 13
iadd
invokespecial java/lang/StringBuilder/<init>(I)V
ldc "Optional.of("
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ")"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 2
.limit stack 4
.end method
