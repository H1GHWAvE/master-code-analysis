.bytecode 50.0
.class public final synchronized com/a/b/b/e
.super java/lang/Object
.annotation invisible Lcom/a/b/a/b;
.end annotation

.field public static final 'A' B = 23


.field public static final 'B' B = 24


.field public static final 'C' B = 25


.field public static final 'D' B = 26


.field public static final 'E' B = 27


.field public static final 'F' B = 28


.field public static final 'G' B = 29


.field public static final 'H' B = 30


.field public static final 'I' B = 31


.field public static final 'J' B = 32


.field public static final 'K' B = 32


.field public static final 'L' B = 127


.field public static final 'M' C = 0


.field public static final 'N' C = 127


.field public static final 'a' B = 0


.field public static final 'b' B = 1


.field public static final 'c' B = 2


.field public static final 'd' B = 3


.field public static final 'e' B = 4


.field public static final 'f' B = 5


.field public static final 'g' B = 6


.field public static final 'h' B = 7


.field public static final 'i' B = 8


.field public static final 'j' B = 9


.field public static final 'k' B = 10


.field public static final 'l' B = 10


.field public static final 'm' B = 11


.field public static final 'n' B = 12


.field public static final 'o' B = 13


.field public static final 'p' B = 14


.field public static final 'q' B = 15


.field public static final 'r' B = 16


.field public static final 's' B = 17


.field public static final 't' B = 17


.field public static final 'u' B = 18


.field public static final 'v' B = 19


.field public static final 'w' B = 19


.field public static final 'x' B = 20


.field public static final 'y' B = 21


.field public static final 'z' B = 22


.method private <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static a(C)C
iload 0
istore 1
iload 0
invokestatic com/a/b/b/e/d(C)Z
ifeq L0
iload 0
bipush 32
ixor
i2c
istore 1
L0:
iload 1
ireturn
.limit locals 2
.limit stack 2
.end method

.method private static a(Ljava/lang/CharSequence;)Ljava/lang/String;
aload 0
instanceof java/lang/String
ifeq L0
aload 0
checkcast java/lang/String
invokestatic com/a/b/b/e/a(Ljava/lang/String;)Ljava/lang/String;
areturn
L0:
aload 0
invokeinterface java/lang/CharSequence/length()I 0
istore 2
new java/lang/StringBuilder
dup
iload 2
invokespecial java/lang/StringBuilder/<init>(I)V
astore 3
iconst_0
istore 1
L1:
iload 1
iload 2
if_icmpge L2
aload 3
aload 0
iload 1
invokeinterface java/lang/CharSequence/charAt(I)C 1
invokestatic com/a/b/b/e/a(C)C
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
pop
iload 1
iconst_1
iadd
istore 1
goto L1
L2:
aload 3
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 4
.limit stack 3
.end method

.method public static a(Ljava/lang/CharSequence;Ljava/lang/String;)Ljava/lang/String;
.annotation visible Ljavax/annotation/CheckReturnValue;
.end annotation
.annotation invisible Lcom/a/b/a/a;
.end annotation
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
bipush 30
aload 1
invokevirtual java/lang/String/length()I
isub
istore 2
iload 2
iflt L0
iconst_1
istore 3
L1:
iload 3
ldc "maxLength (%s) must be >= length of the truncation indicator (%s)"
iconst_2
anewarray java/lang/Object
dup
iconst_0
bipush 30
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
dup
iconst_1
aload 1
invokevirtual java/lang/String/length()I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 0
astore 4
aload 0
invokeinterface java/lang/CharSequence/length()I 0
bipush 30
if_icmpgt L2
aload 0
invokeinterface java/lang/CharSequence/toString()Ljava/lang/String; 0
astore 0
aload 0
astore 4
aload 0
invokevirtual java/lang/String/length()I
bipush 30
if_icmpgt L2
aload 0
areturn
L0:
iconst_0
istore 3
goto L1
L2:
new java/lang/StringBuilder
dup
bipush 30
invokespecial java/lang/StringBuilder/<init>(I)V
aload 4
iconst_0
iload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/CharSequence;II)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 5
.limit stack 6
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
aload 0
invokevirtual java/lang/String/length()I
istore 3
iconst_0
istore 2
L0:
aload 0
astore 4
iload 2
iload 3
if_icmpge L1
aload 0
iload 2
invokevirtual java/lang/String/charAt(I)C
invokestatic com/a/b/b/e/d(C)Z
ifeq L2
aload 0
invokevirtual java/lang/String/toCharArray()[C
astore 0
L3:
iload 2
iload 3
if_icmpge L4
aload 0
iload 2
caload
istore 1
iload 1
invokestatic com/a/b/b/e/d(C)Z
ifeq L5
aload 0
iload 2
iload 1
bipush 32
ixor
i2c
castore
L5:
iload 2
iconst_1
iadd
istore 2
goto L3
L4:
aload 0
invokestatic java/lang/String/valueOf([C)Ljava/lang/String;
astore 4
L1:
aload 4
areturn
L2:
iload 2
iconst_1
iadd
istore 2
goto L0
.limit locals 5
.limit stack 4
.end method

.method private static a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
.annotation invisible Lcom/a/b/a/a;
.end annotation
aload 0
invokeinterface java/lang/CharSequence/length()I 0
istore 5
aload 0
aload 1
if_acmpne L0
L1:
iconst_1
ireturn
L0:
iload 5
aload 1
invokeinterface java/lang/CharSequence/length()I 0
if_icmpeq L2
iconst_0
ireturn
L2:
iconst_0
istore 4
L3:
iload 4
iload 5
if_icmpge L1
aload 0
iload 4
invokeinterface java/lang/CharSequence/charAt(I)C 1
istore 2
aload 1
iload 4
invokeinterface java/lang/CharSequence/charAt(I)C 1
istore 3
iload 2
iload 3
if_icmpeq L4
iload 2
invokestatic com/a/b/b/e/e(C)I
istore 6
iload 6
bipush 26
if_icmpge L5
iload 6
iload 3
invokestatic com/a/b/b/e/e(C)I
if_icmpeq L4
L5:
iconst_0
ireturn
L4:
iload 4
iconst_1
iadd
istore 4
goto L3
.limit locals 7
.limit stack 2
.end method

.method public static b(C)C
iload 0
istore 1
iload 0
invokestatic com/a/b/b/e/c(C)Z
ifeq L0
iload 0
bipush 95
iand
i2c
istore 1
L0:
iload 1
ireturn
.limit locals 2
.limit stack 2
.end method

.method private static b(Ljava/lang/CharSequence;)Ljava/lang/String;
aload 0
instanceof java/lang/String
ifeq L0
aload 0
checkcast java/lang/String
invokestatic com/a/b/b/e/b(Ljava/lang/String;)Ljava/lang/String;
areturn
L0:
aload 0
invokeinterface java/lang/CharSequence/length()I 0
istore 2
new java/lang/StringBuilder
dup
iload 2
invokespecial java/lang/StringBuilder/<init>(I)V
astore 3
iconst_0
istore 1
L1:
iload 1
iload 2
if_icmpge L2
aload 3
aload 0
iload 1
invokeinterface java/lang/CharSequence/charAt(I)C 1
invokestatic com/a/b/b/e/b(C)C
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
pop
iload 1
iconst_1
iadd
istore 1
goto L1
L2:
aload 3
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 4
.limit stack 3
.end method

.method public static b(Ljava/lang/String;)Ljava/lang/String;
aload 0
invokevirtual java/lang/String/length()I
istore 3
iconst_0
istore 2
L0:
aload 0
astore 4
iload 2
iload 3
if_icmpge L1
aload 0
iload 2
invokevirtual java/lang/String/charAt(I)C
invokestatic com/a/b/b/e/c(C)Z
ifeq L2
aload 0
invokevirtual java/lang/String/toCharArray()[C
astore 0
L3:
iload 2
iload 3
if_icmpge L4
aload 0
iload 2
caload
istore 1
iload 1
invokestatic com/a/b/b/e/c(C)Z
ifeq L5
aload 0
iload 2
iload 1
bipush 95
iand
i2c
castore
L5:
iload 2
iconst_1
iadd
istore 2
goto L3
L4:
aload 0
invokestatic java/lang/String/valueOf([C)Ljava/lang/String;
astore 4
L1:
aload 4
areturn
L2:
iload 2
iconst_1
iadd
istore 2
goto L0
.limit locals 5
.limit stack 4
.end method

.method public static c(C)Z
iload 0
bipush 97
if_icmplt L0
iload 0
bipush 122
if_icmpgt L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method public static d(C)Z
iload 0
bipush 65
if_icmplt L0
iload 0
bipush 90
if_icmpgt L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method private static e(C)I
iload 0
bipush 32
ior
bipush 97
isub
i2c
ireturn
.limit locals 1
.limit stack 2
.end method
