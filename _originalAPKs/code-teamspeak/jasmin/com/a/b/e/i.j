.bytecode 50.0
.class public final synchronized com/a/b/e/i
.super java/lang/Object
.annotation invisible Lcom/a/b/a/a;
.end annotation
.annotation invisible Lcom/a/b/a/b;
.end annotation

.field private static final 'a' Lcom/a/b/e/g;

.method static <clinit>()V
new com/a/b/e/j
dup
invokespecial com/a/b/e/j/<init>()V
putstatic com/a/b/e/i/a Lcom/a/b/e/g;
return
.limit locals 0
.limit stack 2
.end method

.method private <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static a()Lcom/a/b/e/l;
new com/a/b/e/l
dup
iconst_0
invokespecial com/a/b/e/l/<init>(B)V
areturn
.limit locals 0
.limit stack 3
.end method

.method private static a(Lcom/a/b/e/d;)Lcom/a/b/e/p;
new com/a/b/e/k
dup
aload 0
invokespecial com/a/b/e/k/<init>(Lcom/a/b/e/d;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private static a(Lcom/a/b/e/g;)Lcom/a/b/e/p;
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
instanceof com/a/b/e/p
ifeq L0
aload 0
checkcast com/a/b/e/p
areturn
L0:
aload 0
instanceof com/a/b/e/d
ifeq L1
new com/a/b/e/k
dup
aload 0
checkcast com/a/b/e/d
invokespecial com/a/b/e/k/<init>(Lcom/a/b/e/d;)V
areturn
L1:
aload 0
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
invokevirtual java/lang/Class/getName()Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 0
aload 0
invokevirtual java/lang/String/length()I
ifeq L2
ldc "Cannot create a UnicodeEscaper from: "
aload 0
invokevirtual java/lang/String/concat(Ljava/lang/String;)Ljava/lang/String;
astore 0
L3:
new java/lang/IllegalArgumentException
dup
aload 0
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L2:
new java/lang/String
dup
ldc "Cannot create a UnicodeEscaper from: "
invokespecial java/lang/String/<init>(Ljava/lang/String;)V
astore 0
goto L3
.limit locals 1
.limit stack 3
.end method

.method private static a(Lcom/a/b/e/d;C)Ljava/lang/String;
aload 0
iload 1
invokevirtual com/a/b/e/d/a(C)[C
invokestatic com/a/b/e/i/a([C)Ljava/lang/String;
areturn
.limit locals 2
.limit stack 2
.end method

.method private static a(Lcom/a/b/e/p;I)Ljava/lang/String;
aload 0
iload 1
invokevirtual com/a/b/e/p/a(I)[C
invokestatic com/a/b/e/i/a([C)Ljava/lang/String;
areturn
.limit locals 2
.limit stack 2
.end method

.method private static a([C)Ljava/lang/String;
aload 0
ifnonnull L0
aconst_null
areturn
L0:
new java/lang/String
dup
aload 0
invokespecial java/lang/String/<init>([C)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private static b()Lcom/a/b/e/g;
getstatic com/a/b/e/i/a Lcom/a/b/e/g;
areturn
.limit locals 0
.limit stack 1
.end method
