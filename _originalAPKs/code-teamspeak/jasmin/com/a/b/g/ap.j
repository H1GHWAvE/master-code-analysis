.bytecode 50.0
.class synchronized abstract enum com/a/b/g/ap
.super java/lang/Enum
.implements com/a/b/b/dz

.field public static final enum 'a' Lcom/a/b/g/ap;

.field public static final enum 'b' Lcom/a/b/g/ap;

.field private static final synthetic 'd' [Lcom/a/b/g/ap;

.field private final 'c' I

.method static <clinit>()V
new com/a/b/g/aq
dup
ldc "CRC_32"
invokespecial com/a/b/g/aq/<init>(Ljava/lang/String;)V
putstatic com/a/b/g/ap/a Lcom/a/b/g/ap;
new com/a/b/g/ar
dup
ldc "ADLER_32"
invokespecial com/a/b/g/ar/<init>(Ljava/lang/String;)V
putstatic com/a/b/g/ap/b Lcom/a/b/g/ap;
iconst_2
anewarray com/a/b/g/ap
dup
iconst_0
getstatic com/a/b/g/ap/a Lcom/a/b/g/ap;
aastore
dup
iconst_1
getstatic com/a/b/g/ap/b Lcom/a/b/g/ap;
aastore
putstatic com/a/b/g/ap/d [Lcom/a/b/g/ap;
return
.limit locals 0
.limit stack 4
.end method

.method private <init>(Ljava/lang/String;I)V
aload 0
aload 1
iload 2
invokespecial java/lang/Enum/<init>(Ljava/lang/String;I)V
aload 0
bipush 32
putfield com/a/b/g/ap/c I
return
.limit locals 3
.limit stack 3
.end method

.method synthetic <init>(Ljava/lang/String;IB)V
aload 0
aload 1
iload 2
invokespecial com/a/b/g/ap/<init>(Ljava/lang/String;I)V
return
.limit locals 4
.limit stack 3
.end method

.method static synthetic a(Lcom/a/b/g/ap;)I
aload 0
getfield com/a/b/g/ap/c I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/a/b/g/ap;
ldc com/a/b/g/ap
aload 0
invokestatic java/lang/Enum/valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;
checkcast com/a/b/g/ap
areturn
.limit locals 1
.limit stack 2
.end method

.method public static values()[Lcom/a/b/g/ap;
getstatic com/a/b/g/ap/d [Lcom/a/b/g/ap;
invokevirtual [Lcom/a/b/g/ap;/clone()Ljava/lang/Object;
checkcast [Lcom/a/b/g/ap;
areturn
.limit locals 0
.limit stack 1
.end method

.method public synthetic a()Ljava/lang/Object;
aload 0
invokevirtual com/a/b/g/ap/b()Ljava/util/zip/Checksum;
areturn
.limit locals 1
.limit stack 1
.end method

.method public abstract b()Ljava/util/zip/Checksum;
.end method
