.bytecode 50.0
.class public final synchronized com/a/b/g/am
.super java/lang/Object
.annotation invisible Lcom/a/b/a/a;
.end annotation

.field private static final 'a' I

.method static <clinit>()V
invokestatic java/lang/System/currentTimeMillis()J
l2i
putstatic com/a/b/g/am/a I
return
.limit locals 0
.limit stack 2
.end method

.method private <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a(JI)I
iconst_0
istore 5
iload 2
ifle L0
iconst_1
istore 7
L1:
iload 7
ldc "buckets must be positive: %s"
iconst_1
anewarray java/lang/Object
dup
iconst_0
iload 2
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
new com/a/b/g/av
dup
lload 0
invokespecial com/a/b/g/av/<init>(J)V
astore 8
L2:
iload 5
iconst_1
iadd
i2d
dstore 3
aload 8
ldc2_w 2862933555777941757L
aload 8
getfield com/a/b/g/av/a J
lmul
lconst_1
ladd
putfield com/a/b/g/av/a J
dload 3
aload 8
getfield com/a/b/g/av/a J
bipush 33
lushr
l2i
iconst_1
iadd
i2d
ldc2_w 2.147483648E9D
ddiv
ddiv
d2i
istore 6
iload 6
iflt L3
iload 6
iload 2
if_icmpge L3
iload 6
istore 5
goto L2
L0:
iconst_0
istore 7
goto L1
L3:
iload 5
ireturn
.limit locals 9
.limit stack 6
.end method

.method private static a(Lcom/a/b/g/ag;I)I
iconst_0
istore 4
aload 0
invokevirtual com/a/b/g/ag/d()J
lstore 6
iload 1
ifle L0
iconst_1
istore 8
L1:
iload 8
ldc "buckets must be positive: %s"
iconst_1
anewarray java/lang/Object
dup
iconst_0
iload 1
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
new com/a/b/g/av
dup
lload 6
invokespecial com/a/b/g/av/<init>(J)V
astore 0
L2:
iload 4
iconst_1
iadd
i2d
dstore 2
aload 0
ldc2_w 2862933555777941757L
aload 0
getfield com/a/b/g/av/a J
lmul
lconst_1
ladd
putfield com/a/b/g/av/a J
dload 2
aload 0
getfield com/a/b/g/av/a J
bipush 33
lushr
l2i
iconst_1
iadd
i2d
ldc2_w 2.147483648E9D
ddiv
ddiv
d2i
istore 5
iload 5
iflt L3
iload 5
iload 1
if_icmpge L3
iload 5
istore 4
goto L2
L0:
iconst_0
istore 8
goto L1
L3:
iload 4
ireturn
.limit locals 9
.limit stack 6
.end method

.method private static a(Ljava/lang/Iterable;)Lcom/a/b/g/ag;
aload 0
invokeinterface java/lang/Iterable/iterator()Ljava/util/Iterator; 0
astore 3
aload 3
invokeinterface java/util/Iterator/hasNext()Z 0
ldc "Must be at least 1 hash code to combine."
invokestatic com/a/b/b/cn/a(ZLjava/lang/Object;)V
aload 3
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/a/b/g/ag
invokevirtual com/a/b/g/ag/a()I
bipush 8
idiv
newarray byte
astore 3
aload 0
invokeinterface java/lang/Iterable/iterator()Ljava/util/Iterator; 0
astore 0
L0:
aload 0
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 0
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/a/b/g/ag
invokevirtual com/a/b/g/ag/e()[B
astore 4
aload 4
arraylength
aload 3
arraylength
if_icmpne L2
iconst_1
istore 2
L3:
iload 2
ldc "All hashcodes must have the same bit length."
invokestatic com/a/b/b/cn/a(ZLjava/lang/Object;)V
iconst_0
istore 1
L4:
iload 1
aload 4
arraylength
if_icmpge L0
aload 3
iload 1
aload 3
iload 1
baload
bipush 37
imul
aload 4
iload 1
baload
ixor
i2b
bastore
iload 1
iconst_1
iadd
istore 1
goto L4
L2:
iconst_0
istore 2
goto L3
L1:
aload 3
invokestatic com/a/b/g/ag/a([B)Lcom/a/b/g/ag;
areturn
.limit locals 5
.limit stack 5
.end method

.method public static a()Lcom/a/b/g/ak;
getstatic com/a/b/g/ay/a Lcom/a/b/g/ak;
areturn
.limit locals 0
.limit stack 1
.end method

.method public static a(I)Lcom/a/b/g/ak;
new com/a/b/g/bl
dup
iload 0
invokespecial com/a/b/g/bl/<init>(I)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private static a(JJ)Lcom/a/b/g/ak;
new com/a/b/g/bo
dup
lload 0
lload 2
invokespecial com/a/b/g/bo/<init>(JJ)V
areturn
.limit locals 4
.limit stack 6
.end method

.method static synthetic a(Lcom/a/b/g/ap;Ljava/lang/String;)Lcom/a/b/g/ak;
new com/a/b/g/r
dup
aload 0
aload 0
invokestatic com/a/b/g/ap/a(Lcom/a/b/g/ap;)I
aload 1
invokespecial com/a/b/g/r/<init>(Lcom/a/b/b/dz;ILjava/lang/String;)V
areturn
.limit locals 2
.limit stack 5
.end method

.method private static b(Ljava/lang/Iterable;)Lcom/a/b/g/ag;
aload 0
invokeinterface java/lang/Iterable/iterator()Ljava/util/Iterator; 0
astore 3
aload 3
invokeinterface java/util/Iterator/hasNext()Z 0
ldc "Must be at least 1 hash code to combine."
invokestatic com/a/b/b/cn/a(ZLjava/lang/Object;)V
aload 3
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/a/b/g/ag
invokevirtual com/a/b/g/ag/a()I
bipush 8
idiv
newarray byte
astore 3
aload 0
invokeinterface java/lang/Iterable/iterator()Ljava/util/Iterator; 0
astore 0
L0:
aload 0
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 0
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/a/b/g/ag
invokevirtual com/a/b/g/ag/e()[B
astore 4
aload 4
arraylength
aload 3
arraylength
if_icmpne L2
iconst_1
istore 2
L3:
iload 2
ldc "All hashcodes must have the same bit length."
invokestatic com/a/b/b/cn/a(ZLjava/lang/Object;)V
iconst_0
istore 1
L4:
iload 1
aload 4
arraylength
if_icmpge L0
aload 3
iload 1
aload 3
iload 1
baload
aload 4
iload 1
baload
iadd
i2b
bastore
iload 1
iconst_1
iadd
istore 1
goto L4
L2:
iconst_0
istore 2
goto L3
L1:
aload 3
invokestatic com/a/b/g/ag/a([B)Lcom/a/b/g/ag;
areturn
.limit locals 5
.limit stack 5
.end method

.method public static b()Lcom/a/b/g/ak;
getstatic com/a/b/g/ax/a Lcom/a/b/g/ak;
areturn
.limit locals 0
.limit stack 1
.end method

.method public static b(I)Lcom/a/b/g/ak;
new com/a/b/g/bj
dup
iload 0
invokespecial com/a/b/g/bj/<init>(I)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private static b(Lcom/a/b/g/ap;Ljava/lang/String;)Lcom/a/b/g/ak;
new com/a/b/g/r
dup
aload 0
aload 0
invokestatic com/a/b/g/ap/a(Lcom/a/b/g/ap;)I
aload 1
invokespecial com/a/b/g/r/<init>(Lcom/a/b/b/dz;ILjava/lang/String;)V
areturn
.limit locals 2
.limit stack 5
.end method

.method static synthetic c()I
getstatic com/a/b/g/am/a I
ireturn
.limit locals 0
.limit stack 1
.end method

.method private static c(I)Lcom/a/b/g/ak;
iconst_1
istore 1
iload 0
ifle L0
iconst_1
istore 4
L1:
iload 4
ldc "Number of bits must be positive"
invokestatic com/a/b/b/cn/a(ZLjava/lang/Object;)V
iload 0
bipush 31
iadd
bipush -32
iand
istore 0
iload 0
bipush 32
if_icmpne L2
getstatic com/a/b/g/ay/b Lcom/a/b/g/ak;
areturn
L0:
iconst_0
istore 4
goto L1
L2:
iload 0
sipush 128
if_icmpgt L3
getstatic com/a/b/g/ax/b Lcom/a/b/g/ak;
areturn
L3:
iload 0
bipush 127
iadd
sipush 128
idiv
istore 3
iload 3
anewarray com/a/b/g/ak
astore 5
aload 5
iconst_0
getstatic com/a/b/g/ax/b Lcom/a/b/g/ak;
aastore
getstatic com/a/b/g/am/a I
istore 2
iload 1
istore 0
iload 2
istore 1
L4:
iload 0
iload 3
if_icmpge L5
iload 1
ldc_w 1500450271
iadd
istore 1
aload 5
iload 0
iload 1
invokestatic com/a/b/g/am/b(I)Lcom/a/b/g/ak;
aastore
iload 0
iconst_1
iadd
istore 0
goto L4
L5:
new com/a/b/g/as
dup
aload 5
invokespecial com/a/b/g/as/<init>([Lcom/a/b/g/ak;)V
areturn
.limit locals 6
.limit stack 3
.end method

.method private static d(I)I
iload 0
ifle L0
iconst_1
istore 1
L1:
iload 1
ldc "Number of bits must be positive"
invokestatic com/a/b/b/cn/a(ZLjava/lang/Object;)V
iload 0
bipush 31
iadd
bipush -32
iand
ireturn
L0:
iconst_0
istore 1
goto L1
.limit locals 2
.limit stack 2
.end method

.method private static d()Lcom/a/b/g/ak;
getstatic com/a/b/g/bc/a Lcom/a/b/g/ak;
areturn
.limit locals 0
.limit stack 1
.end method

.method private static e()Lcom/a/b/g/ak;
getstatic com/a/b/g/aw/a Lcom/a/b/g/ak;
areturn
.limit locals 0
.limit stack 1
.end method

.method private static f()Lcom/a/b/g/ak;
getstatic com/a/b/g/az/a Lcom/a/b/g/ak;
areturn
.limit locals 0
.limit stack 1
.end method

.method private static g()Lcom/a/b/g/ak;
getstatic com/a/b/g/ba/a Lcom/a/b/g/ak;
areturn
.limit locals 0
.limit stack 1
.end method

.method private static h()Lcom/a/b/g/ak;
getstatic com/a/b/g/bb/a Lcom/a/b/g/ak;
areturn
.limit locals 0
.limit stack 1
.end method

.method private static i()Lcom/a/b/g/ak;
getstatic com/a/b/g/au/a Lcom/a/b/g/ak;
areturn
.limit locals 0
.limit stack 1
.end method

.method private static j()Lcom/a/b/g/ak;
getstatic com/a/b/g/at/a Lcom/a/b/g/ak;
areturn
.limit locals 0
.limit stack 1
.end method

.method private static k()Lcom/a/b/g/ak;
getstatic com/a/b/g/ao/a Lcom/a/b/g/ak;
areturn
.limit locals 0
.limit stack 1
.end method
