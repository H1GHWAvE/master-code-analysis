.bytecode 50.0
.class synchronized com/a/b/g/ad
.super java/lang/Object
.implements com/a/b/g/w
.implements java/io/Serializable

.field private final 'a' Ljava/nio/charset/Charset;

.method <init>(Ljava/nio/charset/Charset;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/nio/charset/Charset
putfield com/a/b/g/ad/a Ljava/nio/charset/Charset;
return
.limit locals 2
.limit stack 2
.end method

.method private a()Ljava/lang/Object;
new com/a/b/g/ae
dup
aload 0
getfield com/a/b/g/ad/a Ljava/nio/charset/Charset;
invokespecial com/a/b/g/ae/<init>(Ljava/nio/charset/Charset;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private a(Ljava/lang/CharSequence;Lcom/a/b/g/bn;)V
aload 2
aload 1
aload 0
getfield com/a/b/g/ad/a Ljava/nio/charset/Charset;
invokeinterface com/a/b/g/bn/b(Ljava/lang/CharSequence;Ljava/nio/charset/Charset;)Lcom/a/b/g/bn; 2
pop
return
.limit locals 3
.limit stack 3
.end method

.method public final synthetic a(Ljava/lang/Object;Lcom/a/b/g/bn;)V
aload 2
aload 1
checkcast java/lang/CharSequence
aload 0
getfield com/a/b/g/ad/a Ljava/nio/charset/Charset;
invokeinterface com/a/b/g/bn/b(Ljava/lang/CharSequence;Ljava/nio/charset/Charset;)Lcom/a/b/g/bn; 2
pop
return
.limit locals 3
.limit stack 3
.end method

.method public equals(Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 1
instanceof com/a/b/g/ad
ifeq L0
aload 1
checkcast com/a/b/g/ad
astore 1
aload 0
getfield com/a/b/g/ad/a Ljava/nio/charset/Charset;
aload 1
getfield com/a/b/g/ad/a Ljava/nio/charset/Charset;
invokevirtual java/nio/charset/Charset/equals(Ljava/lang/Object;)Z
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method public hashCode()I
ldc com/a/b/g/ad
invokevirtual java/lang/Object/hashCode()I
aload 0
getfield com/a/b/g/ad/a Ljava/nio/charset/Charset;
invokevirtual java/nio/charset/Charset/hashCode()I
ixor
ireturn
.limit locals 1
.limit stack 2
.end method

.method public toString()Ljava/lang/String;
aload 0
getfield com/a/b/g/ad/a Ljava/nio/charset/Charset;
invokevirtual java/nio/charset/Charset/name()Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 1
new java/lang/StringBuilder
dup
aload 1
invokevirtual java/lang/String/length()I
bipush 22
iadd
invokespecial java/lang/StringBuilder/<init>(I)V
ldc "Funnels.stringFunnel("
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ")"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 2
.limit stack 4
.end method
