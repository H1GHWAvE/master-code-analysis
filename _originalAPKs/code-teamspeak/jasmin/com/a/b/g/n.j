.bytecode 50.0
.class synchronized abstract enum com/a/b/g/n
.super java/lang/Enum
.implements com/a/b/g/m

.field public static final enum 'a' Lcom/a/b/g/n;

.field public static final enum 'b' Lcom/a/b/g/n;

.field private static final synthetic 'c' [Lcom/a/b/g/n;

.method static <clinit>()V
new com/a/b/g/o
dup
ldc "MURMUR128_MITZ_32"
invokespecial com/a/b/g/o/<init>(Ljava/lang/String;)V
putstatic com/a/b/g/n/a Lcom/a/b/g/n;
new com/a/b/g/p
dup
ldc "MURMUR128_MITZ_64"
invokespecial com/a/b/g/p/<init>(Ljava/lang/String;)V
putstatic com/a/b/g/n/b Lcom/a/b/g/n;
iconst_2
anewarray com/a/b/g/n
dup
iconst_0
getstatic com/a/b/g/n/a Lcom/a/b/g/n;
aastore
dup
iconst_1
getstatic com/a/b/g/n/b Lcom/a/b/g/n;
aastore
putstatic com/a/b/g/n/c [Lcom/a/b/g/n;
return
.limit locals 0
.limit stack 4
.end method

.method private <init>(Ljava/lang/String;I)V
aload 0
aload 1
iload 2
invokespecial java/lang/Enum/<init>(Ljava/lang/String;I)V
return
.limit locals 3
.limit stack 3
.end method

.method synthetic <init>(Ljava/lang/String;IB)V
aload 0
aload 1
iload 2
invokespecial com/a/b/g/n/<init>(Ljava/lang/String;I)V
return
.limit locals 4
.limit stack 3
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/a/b/g/n;
ldc com/a/b/g/n
aload 0
invokestatic java/lang/Enum/valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;
checkcast com/a/b/g/n
areturn
.limit locals 1
.limit stack 2
.end method

.method public static values()[Lcom/a/b/g/n;
getstatic com/a/b/g/n/c [Lcom/a/b/g/n;
invokevirtual [Lcom/a/b/g/n;/clone()Ljava/lang/Object;
checkcast [Lcom/a/b/g/n;
areturn
.limit locals 0
.limit stack 1
.end method
