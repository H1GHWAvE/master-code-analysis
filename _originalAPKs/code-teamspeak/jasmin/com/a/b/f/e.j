.bytecode 50.0
.class public final synchronized com/a/b/f/e
.super com/a/b/f/h
.annotation invisible Lcom/a/b/a/a;
.end annotation

.field private final 'a' Ljava/util/concurrent/Executor;

.field private final 'b' Ljava/util/concurrent/ConcurrentLinkedQueue;

.method private <init>(Ljava/lang/String;Ljava/util/concurrent/Executor;)V
aload 0
aload 1
invokespecial com/a/b/f/h/<init>(Ljava/lang/String;)V
aload 0
new java/util/concurrent/ConcurrentLinkedQueue
dup
invokespecial java/util/concurrent/ConcurrentLinkedQueue/<init>()V
putfield com/a/b/f/e/b Ljava/util/concurrent/ConcurrentLinkedQueue;
aload 0
aload 2
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/util/concurrent/Executor
putfield com/a/b/f/e/a Ljava/util/concurrent/Executor;
return
.limit locals 3
.limit stack 3
.end method

.method private <init>(Ljava/util/concurrent/Executor;)V
aload 0
ldc "default"
invokespecial com/a/b/f/h/<init>(Ljava/lang/String;)V
aload 0
new java/util/concurrent/ConcurrentLinkedQueue
dup
invokespecial java/util/concurrent/ConcurrentLinkedQueue/<init>()V
putfield com/a/b/f/e/b Ljava/util/concurrent/ConcurrentLinkedQueue;
aload 0
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/util/concurrent/Executor
putfield com/a/b/f/e/a Ljava/util/concurrent/Executor;
return
.limit locals 2
.limit stack 3
.end method

.method private <init>(Ljava/util/concurrent/Executor;Lcom/a/b/f/q;)V
aload 0
aload 2
invokespecial com/a/b/f/h/<init>(Lcom/a/b/f/q;)V
aload 0
new java/util/concurrent/ConcurrentLinkedQueue
dup
invokespecial java/util/concurrent/ConcurrentLinkedQueue/<init>()V
putfield com/a/b/f/e/b Ljava/util/concurrent/ConcurrentLinkedQueue;
aload 0
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/util/concurrent/Executor
putfield com/a/b/f/e/a Ljava/util/concurrent/Executor;
return
.limit locals 3
.limit stack 3
.end method

.method static synthetic a(Lcom/a/b/f/e;Ljava/lang/Object;Lcom/a/b/f/n;)V
aload 0
aload 1
aload 2
invokespecial com/a/b/f/h/b(Ljava/lang/Object;Lcom/a/b/f/n;)V
return
.limit locals 3
.limit stack 3
.end method

.method protected final a()V
L0:
aload 0
getfield com/a/b/f/e/b Ljava/util/concurrent/ConcurrentLinkedQueue;
invokevirtual java/util/concurrent/ConcurrentLinkedQueue/poll()Ljava/lang/Object;
checkcast com/a/b/f/l
astore 1
aload 1
ifnull L1
aload 0
aload 1
getfield com/a/b/f/l/a Ljava/lang/Object;
aload 1
getfield com/a/b/f/l/b Lcom/a/b/f/n;
invokevirtual com/a/b/f/e/b(Ljava/lang/Object;Lcom/a/b/f/n;)V
goto L0
L1:
return
.limit locals 2
.limit stack 3
.end method

.method final a(Ljava/lang/Object;Lcom/a/b/f/n;)V
aload 0
getfield com/a/b/f/e/b Ljava/util/concurrent/ConcurrentLinkedQueue;
new com/a/b/f/l
dup
aload 1
aload 2
invokespecial com/a/b/f/l/<init>(Ljava/lang/Object;Lcom/a/b/f/n;)V
invokevirtual java/util/concurrent/ConcurrentLinkedQueue/offer(Ljava/lang/Object;)Z
pop
return
.limit locals 3
.limit stack 5
.end method

.method final b(Ljava/lang/Object;Lcom/a/b/f/n;)V
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 2
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
getfield com/a/b/f/e/a Ljava/util/concurrent/Executor;
new com/a/b/f/f
dup
aload 0
aload 1
aload 2
invokespecial com/a/b/f/f/<init>(Lcom/a/b/f/e;Ljava/lang/Object;Lcom/a/b/f/n;)V
invokeinterface java/util/concurrent/Executor/execute(Ljava/lang/Runnable;)V 1
return
.limit locals 3
.limit stack 6
.end method
