.bytecode 50.0
.class public final synchronized net/hockeyapp/android/e/m
.super java/lang/Object

.field public static final 'a' I = 0


.field public static final 'b' I = 1


.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static a(Landroid/content/Context;Landroid/net/Uri;)I
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aconst_null
astore 3
L0:
aload 0
invokevirtual android/content/Context/getContentResolver()Landroid/content/ContentResolver;
aload 1
invokevirtual android/content/ContentResolver/openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;
astore 0
L1:
aload 0
astore 3
L3:
aload 0
invokestatic net/hockeyapp/android/e/m/a(Ljava/io/InputStream;)I
istore 2
L4:
aload 0
ifnull L5
aload 0
invokevirtual java/io/InputStream/close()V
L5:
iload 2
ireturn
L2:
astore 0
aload 3
ifnull L6
aload 3
invokevirtual java/io/InputStream/close()V
L6:
aload 0
athrow
.limit locals 4
.limit stack 2
.end method

.method public static a(Landroid/graphics/BitmapFactory$Options;II)I
aload 0
getfield android/graphics/BitmapFactory$Options/outHeight I
istore 5
aload 0
getfield android/graphics/BitmapFactory$Options/outWidth I
istore 6
iconst_1
istore 4
iconst_1
istore 3
iload 5
iload 2
if_icmpgt L0
iload 6
iload 1
if_icmple L1
L0:
iload 5
iconst_2
idiv
istore 5
iload 6
iconst_2
idiv
istore 6
L2:
iload 3
istore 4
iload 5
iload 3
idiv
iload 2
if_icmple L1
iload 3
istore 4
iload 6
iload 3
idiv
iload 1
if_icmple L1
iload 3
iconst_2
imul
istore 3
goto L2
L1:
iload 4
ireturn
.limit locals 7
.limit stack 2
.end method

.method public static a(Ljava/io/File;)I
.catch all from L0 to L1 using L2
.catch all from L1 to L3 using L4
L0:
new java/io/FileInputStream
dup
aload 0
invokespecial java/io/FileInputStream/<init>(Ljava/io/File;)V
astore 2
L1:
aload 2
invokestatic net/hockeyapp/android/e/m/a(Ljava/io/InputStream;)I
istore 1
L3:
aload 2
invokevirtual java/io/InputStream/close()V
iload 1
ireturn
L2:
astore 0
aconst_null
astore 2
L5:
aload 2
ifnull L6
aload 2
invokevirtual java/io/InputStream/close()V
L6:
aload 0
athrow
L4:
astore 0
goto L5
.limit locals 3
.limit stack 3
.end method

.method private static a(Ljava/io/InputStream;)I
iconst_1
istore 1
new android/graphics/BitmapFactory$Options
dup
invokespecial android/graphics/BitmapFactory$Options/<init>()V
astore 2
aload 2
iconst_1
putfield android/graphics/BitmapFactory$Options/inJustDecodeBounds Z
aload 0
aconst_null
aload 2
invokestatic android/graphics/BitmapFactory/decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
pop
aload 2
getfield android/graphics/BitmapFactory$Options/outWidth I
iconst_m1
if_icmpeq L0
aload 2
getfield android/graphics/BitmapFactory$Options/outHeight I
iconst_m1
if_icmpne L1
L0:
iconst_0
istore 1
L2:
iload 1
ireturn
L1:
aload 2
getfield android/graphics/BitmapFactory$Options/outWidth I
i2f
aload 2
getfield android/graphics/BitmapFactory$Options/outHeight I
i2f
fdiv
fconst_1
fcmpl
ifgt L2
iconst_0
ireturn
.limit locals 3
.limit stack 3
.end method

.method private static a(Landroid/content/Context;Landroid/net/Uri;II)Landroid/graphics/Bitmap;
new android/graphics/BitmapFactory$Options
dup
invokespecial android/graphics/BitmapFactory$Options/<init>()V
astore 4
aload 4
iconst_1
putfield android/graphics/BitmapFactory$Options/inJustDecodeBounds Z
aload 0
invokevirtual android/content/Context/getContentResolver()Landroid/content/ContentResolver;
aload 1
invokevirtual android/content/ContentResolver/openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;
aconst_null
aload 4
invokestatic android/graphics/BitmapFactory/decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
pop
aload 4
aload 4
iload 2
iload 3
invokestatic net/hockeyapp/android/e/m/a(Landroid/graphics/BitmapFactory$Options;II)I
putfield android/graphics/BitmapFactory$Options/inSampleSize I
aload 4
iconst_0
putfield android/graphics/BitmapFactory$Options/inJustDecodeBounds Z
aload 0
invokevirtual android/content/Context/getContentResolver()Landroid/content/ContentResolver;
aload 1
invokevirtual android/content/ContentResolver/openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;
aconst_null
aload 4
invokestatic android/graphics/BitmapFactory/decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
areturn
.limit locals 5
.limit stack 4
.end method

.method private static a(Ljava/io/File;II)Landroid/graphics/Bitmap;
new android/graphics/BitmapFactory$Options
dup
invokespecial android/graphics/BitmapFactory$Options/<init>()V
astore 3
aload 3
iconst_1
putfield android/graphics/BitmapFactory$Options/inJustDecodeBounds Z
aload 0
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
aload 3
invokestatic android/graphics/BitmapFactory/decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
pop
aload 3
aload 3
iload 1
iload 2
invokestatic net/hockeyapp/android/e/m/a(Landroid/graphics/BitmapFactory$Options;II)I
putfield android/graphics/BitmapFactory$Options/inSampleSize I
aload 3
iconst_0
putfield android/graphics/BitmapFactory$Options/inJustDecodeBounds Z
aload 0
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
aload 3
invokestatic android/graphics/BitmapFactory/decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
areturn
.limit locals 4
.limit stack 4
.end method
