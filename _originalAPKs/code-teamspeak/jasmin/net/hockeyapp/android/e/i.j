.bytecode 50.0
.class public final synchronized net/hockeyapp/android/e/i
.super java/lang/Object

.method private <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method synthetic <init>(B)V
aload 0
invokespecial net/hockeyapp/android/e/i/<init>()V
return
.limit locals 2
.limit stack 1
.end method

.method public static a(Ljava/lang/String;)Lnet/hockeyapp/android/c/h;
.catch org/json/JSONException from L0 to L1 using L2
.catch org/json/JSONException from L3 to L4 using L2
.catch org/json/JSONException from L5 to L6 using L2
.catch org/json/JSONException from L7 to L8 using L2
.catch org/json/JSONException from L9 to L10 using L2
.catch org/json/JSONException from L11 to L12 using L2
.catch org/json/JSONException from L13 to L14 using L2
.catch org/json/JSONException from L14 to L15 using L16
.catch org/json/JSONException from L15 to L17 using L18
.catch org/json/JSONException from L17 to L19 using L20
.catch org/json/JSONException from L19 to L21 using L22
.catch org/json/JSONException from L21 to L23 using L2
.catch org/json/JSONException from L23 to L24 using L25
.catch org/json/JSONException from L24 to L26 using L27
.catch org/json/JSONException from L26 to L28 using L29
.catch org/json/JSONException from L30 to L31 using L2
.catch org/json/JSONException from L32 to L33 using L2
.catch org/json/JSONException from L34 to L35 using L2
.catch org/json/JSONException from L36 to L37 using L2
.catch org/json/JSONException from L38 to L39 using L25
.catch org/json/JSONException from L40 to L41 using L25
aload 0
ifnull L42
L0:
new org/json/JSONObject
dup
aload 0
invokespecial org/json/JSONObject/<init>(Ljava/lang/String;)V
astore 9
aload 9
ldc "feedback"
invokevirtual org/json/JSONObject/getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;
astore 11
new net/hockeyapp/android/c/d
dup
invokespecial net/hockeyapp/android/c/d/<init>()V
astore 10
aload 11
ldc "messages"
invokevirtual org/json/JSONObject/getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
astore 12
L1:
aconst_null
astore 0
L3:
aload 12
invokevirtual org/json/JSONArray/length()I
ifle L13
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 7
L4:
iconst_0
istore 1
L43:
aload 7
astore 0
L5:
iload 1
aload 12
invokevirtual org/json/JSONArray/length()I
if_icmpge L13
aload 12
iload 1
invokevirtual org/json/JSONArray/getJSONObject(I)Lorg/json/JSONObject;
ldc "subject"
invokevirtual org/json/JSONObject/getString(Ljava/lang/String;)Ljava/lang/String;
invokevirtual java/lang/String/toString()Ljava/lang/String;
astore 13
aload 12
iload 1
invokevirtual org/json/JSONArray/getJSONObject(I)Lorg/json/JSONObject;
ldc "text"
invokevirtual org/json/JSONObject/getString(Ljava/lang/String;)Ljava/lang/String;
invokevirtual java/lang/String/toString()Ljava/lang/String;
astore 14
aload 12
iload 1
invokevirtual org/json/JSONArray/getJSONObject(I)Lorg/json/JSONObject;
ldc "oem"
invokevirtual org/json/JSONObject/getString(Ljava/lang/String;)Ljava/lang/String;
invokevirtual java/lang/String/toString()Ljava/lang/String;
astore 15
aload 12
iload 1
invokevirtual org/json/JSONArray/getJSONObject(I)Lorg/json/JSONObject;
ldc "model"
invokevirtual org/json/JSONObject/getString(Ljava/lang/String;)Ljava/lang/String;
invokevirtual java/lang/String/toString()Ljava/lang/String;
astore 16
aload 12
iload 1
invokevirtual org/json/JSONArray/getJSONObject(I)Lorg/json/JSONObject;
ldc "os_version"
invokevirtual org/json/JSONObject/getString(Ljava/lang/String;)Ljava/lang/String;
invokevirtual java/lang/String/toString()Ljava/lang/String;
astore 17
aload 12
iload 1
invokevirtual org/json/JSONArray/getJSONObject(I)Lorg/json/JSONObject;
ldc "created_at"
invokevirtual org/json/JSONObject/getString(Ljava/lang/String;)Ljava/lang/String;
invokevirtual java/lang/String/toString()Ljava/lang/String;
astore 18
aload 12
iload 1
invokevirtual org/json/JSONArray/getJSONObject(I)Lorg/json/JSONObject;
ldc "id"
invokevirtual org/json/JSONObject/getInt(Ljava/lang/String;)I
istore 3
aload 12
iload 1
invokevirtual org/json/JSONArray/getJSONObject(I)Lorg/json/JSONObject;
ldc "token"
invokevirtual org/json/JSONObject/getString(Ljava/lang/String;)Ljava/lang/String;
invokevirtual java/lang/String/toString()Ljava/lang/String;
astore 19
aload 12
iload 1
invokevirtual org/json/JSONArray/getJSONObject(I)Lorg/json/JSONObject;
ldc "via"
invokevirtual org/json/JSONObject/getInt(Ljava/lang/String;)I
istore 4
aload 12
iload 1
invokevirtual org/json/JSONArray/getJSONObject(I)Lorg/json/JSONObject;
ldc "user_string"
invokevirtual org/json/JSONObject/getString(Ljava/lang/String;)Ljava/lang/String;
invokevirtual java/lang/String/toString()Ljava/lang/String;
astore 20
aload 12
iload 1
invokevirtual org/json/JSONArray/getJSONObject(I)Lorg/json/JSONObject;
ldc "clean_text"
invokevirtual org/json/JSONObject/getString(Ljava/lang/String;)Ljava/lang/String;
invokevirtual java/lang/String/toString()Ljava/lang/String;
astore 21
aload 12
iload 1
invokevirtual org/json/JSONArray/getJSONObject(I)Lorg/json/JSONObject;
ldc "name"
invokevirtual org/json/JSONObject/getString(Ljava/lang/String;)Ljava/lang/String;
invokevirtual java/lang/String/toString()Ljava/lang/String;
astore 22
aload 12
iload 1
invokevirtual org/json/JSONArray/getJSONObject(I)Lorg/json/JSONObject;
ldc "app_id"
invokevirtual org/json/JSONObject/getString(Ljava/lang/String;)Ljava/lang/String;
invokevirtual java/lang/String/toString()Ljava/lang/String;
astore 23
aload 12
iload 1
invokevirtual org/json/JSONArray/getJSONObject(I)Lorg/json/JSONObject;
ldc "attachments"
invokevirtual org/json/JSONObject/optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
astore 24
invokestatic java/util/Collections/emptyList()Ljava/util/List;
astore 0
L6:
aload 24
ifnull L11
L7:
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 8
L8:
iconst_0
istore 2
L44:
aload 8
astore 0
L9:
iload 2
aload 24
invokevirtual org/json/JSONArray/length()I
if_icmpge L11
aload 24
iload 2
invokevirtual org/json/JSONArray/getJSONObject(I)Lorg/json/JSONObject;
ldc "id"
invokevirtual org/json/JSONObject/getInt(Ljava/lang/String;)I
istore 5
aload 24
iload 2
invokevirtual org/json/JSONArray/getJSONObject(I)Lorg/json/JSONObject;
ldc "feedback_message_id"
invokevirtual org/json/JSONObject/getInt(Ljava/lang/String;)I
istore 6
aload 24
iload 2
invokevirtual org/json/JSONArray/getJSONObject(I)Lorg/json/JSONObject;
ldc "file_name"
invokevirtual org/json/JSONObject/getString(Ljava/lang/String;)Ljava/lang/String;
astore 0
aload 24
iload 2
invokevirtual org/json/JSONArray/getJSONObject(I)Lorg/json/JSONObject;
ldc "url"
invokevirtual org/json/JSONObject/getString(Ljava/lang/String;)Ljava/lang/String;
astore 25
aload 24
iload 2
invokevirtual org/json/JSONArray/getJSONObject(I)Lorg/json/JSONObject;
ldc "created_at"
invokevirtual org/json/JSONObject/getString(Ljava/lang/String;)Ljava/lang/String;
astore 26
aload 24
iload 2
invokevirtual org/json/JSONArray/getJSONObject(I)Lorg/json/JSONObject;
ldc "updated_at"
invokevirtual org/json/JSONObject/getString(Ljava/lang/String;)Ljava/lang/String;
astore 27
new net/hockeyapp/android/c/e
dup
invokespecial net/hockeyapp/android/c/e/<init>()V
astore 28
aload 28
iload 5
putfield net/hockeyapp/android/c/e/a I
aload 28
iload 6
putfield net/hockeyapp/android/c/e/b I
aload 28
aload 0
putfield net/hockeyapp/android/c/e/c Ljava/lang/String;
aload 28
aload 25
putfield net/hockeyapp/android/c/e/d Ljava/lang/String;
aload 28
aload 26
putfield net/hockeyapp/android/c/e/e Ljava/lang/String;
aload 28
aload 27
putfield net/hockeyapp/android/c/e/f Ljava/lang/String;
aload 8
aload 28
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
L10:
iload 2
iconst_1
iadd
istore 2
goto L44
L11:
new net/hockeyapp/android/c/g
dup
invokespecial net/hockeyapp/android/c/g/<init>()V
astore 8
aload 8
aload 23
putfield net/hockeyapp/android/c/g/m Ljava/lang/String;
aload 8
aload 21
putfield net/hockeyapp/android/c/g/k Ljava/lang/String;
aload 8
aload 18
putfield net/hockeyapp/android/c/g/f Ljava/lang/String;
aload 8
iload 3
putfield net/hockeyapp/android/c/g/g I
aload 8
aload 16
putfield net/hockeyapp/android/c/g/d Ljava/lang/String;
aload 8
aload 22
putfield net/hockeyapp/android/c/g/l Ljava/lang/String;
aload 8
aload 15
putfield net/hockeyapp/android/c/g/c Ljava/lang/String;
aload 8
aload 17
putfield net/hockeyapp/android/c/g/e Ljava/lang/String;
aload 8
aload 13
putfield net/hockeyapp/android/c/g/a Ljava/lang/String;
aload 8
aload 14
putfield net/hockeyapp/android/c/g/b Ljava/lang/String;
aload 8
aload 19
putfield net/hockeyapp/android/c/g/h Ljava/lang/String;
aload 8
aload 20
putfield net/hockeyapp/android/c/g/j Ljava/lang/String;
aload 8
iload 4
putfield net/hockeyapp/android/c/g/i I
aload 8
aload 0
putfield net/hockeyapp/android/c/g/n Ljava/util/List;
aload 7
aload 8
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L12:
iload 1
iconst_1
iadd
istore 1
goto L43
L13:
aload 10
aload 0
putfield net/hockeyapp/android/c/d/e Ljava/util/ArrayList;
L14:
aload 10
aload 11
ldc "name"
invokevirtual org/json/JSONObject/getString(Ljava/lang/String;)Ljava/lang/String;
invokevirtual java/lang/String/toString()Ljava/lang/String;
putfield net/hockeyapp/android/c/d/a Ljava/lang/String;
L15:
aload 10
aload 11
ldc "email"
invokevirtual org/json/JSONObject/getString(Ljava/lang/String;)Ljava/lang/String;
invokevirtual java/lang/String/toString()Ljava/lang/String;
putfield net/hockeyapp/android/c/d/b Ljava/lang/String;
L17:
aload 10
aload 11
ldc "id"
invokevirtual org/json/JSONObject/getInt(Ljava/lang/String;)I
putfield net/hockeyapp/android/c/d/c I
L19:
aload 10
aload 11
ldc "created_at"
invokevirtual org/json/JSONObject/getString(Ljava/lang/String;)Ljava/lang/String;
invokevirtual java/lang/String/toString()Ljava/lang/String;
putfield net/hockeyapp/android/c/d/d Ljava/lang/String;
L21:
new net/hockeyapp/android/c/h
dup
invokespecial net/hockeyapp/android/c/h/<init>()V
astore 0
L23:
aload 0
aload 10
putfield net/hockeyapp/android/c/h/b Lnet/hockeyapp/android/c/d;
L24:
aload 0
aload 9
ldc "status"
invokevirtual org/json/JSONObject/getString(Ljava/lang/String;)Ljava/lang/String;
invokevirtual java/lang/String/toString()Ljava/lang/String;
putfield net/hockeyapp/android/c/h/a Ljava/lang/String;
L26:
aload 0
aload 9
ldc "token"
invokevirtual org/json/JSONObject/getString(Ljava/lang/String;)Ljava/lang/String;
invokevirtual java/lang/String/toString()Ljava/lang/String;
putfield net/hockeyapp/android/c/h/c Ljava/lang/String;
L28:
aload 0
areturn
L16:
astore 0
L30:
aload 0
invokevirtual org/json/JSONException/printStackTrace()V
L31:
goto L15
L2:
astore 7
aconst_null
astore 0
L45:
aload 7
invokevirtual org/json/JSONException/printStackTrace()V
aload 0
areturn
L18:
astore 0
L32:
aload 0
invokevirtual org/json/JSONException/printStackTrace()V
L33:
goto L17
L20:
astore 0
L34:
aload 0
invokevirtual org/json/JSONException/printStackTrace()V
L35:
goto L19
L22:
astore 0
L36:
aload 0
invokevirtual org/json/JSONException/printStackTrace()V
L37:
goto L21
L27:
astore 7
L38:
aload 7
invokevirtual org/json/JSONException/printStackTrace()V
L39:
goto L26
L29:
astore 7
L40:
aload 7
invokevirtual org/json/JSONException/printStackTrace()V
L41:
aload 0
areturn
L42:
aconst_null
areturn
L25:
astore 7
goto L45
.limit locals 29
.limit stack 3
.end method

.method private static a()Lnet/hockeyapp/android/e/i;
getstatic net/hockeyapp/android/e/k/a Lnet/hockeyapp/android/e/i;
areturn
.limit locals 0
.limit stack 1
.end method
