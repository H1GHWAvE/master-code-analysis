.bytecode 50.0
.class public synchronized net/hockeyapp/android/e/b
.super java/lang/Object

.field public static final 'a' I = 0


.field public static final 'b' I = 1


.field public static final 'c' I = 2


.field public static final 'd' I = 4


.field public static final 'e' I = 8


.field static final synthetic 'f' Z

.method static <clinit>()V
ldc net/hockeyapp/android/e/b
invokevirtual java/lang/Class/desiredAssertionStatus()Z
ifne L0
iconst_1
istore 0
L1:
iload 0
putstatic net/hockeyapp/android/e/b/f Z
return
L0:
iconst_0
istore 0
goto L1
.limit locals 1
.limit stack 1
.end method

.method private <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static a([B)Ljava/lang/String;
.catch java/io/UnsupportedEncodingException from L0 to L1 using L2
L0:
new java/lang/String
dup
aload 0
iconst_0
aload 0
arraylength
iconst_2
invokestatic net/hockeyapp/android/e/b/b([BIII)[B
ldc "US-ASCII"
invokespecial java/lang/String/<init>([BLjava/lang/String;)V
astore 0
L1:
aload 0
areturn
L2:
astore 0
new java/lang/AssertionError
dup
aload 0
invokespecial java/lang/AssertionError/<init>(Ljava/lang/Object;)V
athrow
.limit locals 1
.limit stack 6
.end method

.method private static a([BIII)Ljava/lang/String;
.catch java/io/UnsupportedEncodingException from L0 to L1 using L2
L0:
new java/lang/String
dup
aload 0
iload 1
iload 2
iload 3
invokestatic net/hockeyapp/android/e/b/b([BIII)[B
ldc "US-ASCII"
invokespecial java/lang/String/<init>([BLjava/lang/String;)V
astore 0
L1:
aload 0
areturn
L2:
astore 0
new java/lang/AssertionError
dup
aload 0
invokespecial java/lang/AssertionError/<init>(Ljava/lang/Object;)V
athrow
.limit locals 4
.limit stack 6
.end method

.method private static a(Ljava/lang/String;I)[B
aload 0
invokevirtual java/lang/String/getBytes()[B
astore 3
aload 3
arraylength
istore 2
new net/hockeyapp/android/e/d
dup
iload 1
iload 2
iconst_3
imul
iconst_4
idiv
newarray byte
invokespecial net/hockeyapp/android/e/d/<init>(I[B)V
astore 0
aload 0
aload 3
iconst_0
iload 2
invokevirtual net/hockeyapp/android/e/d/a([BII)Z
ifne L0
new java/lang/IllegalArgumentException
dup
ldc "bad base-64"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
getfield net/hockeyapp/android/e/d/b I
aload 0
getfield net/hockeyapp/android/e/d/a [B
arraylength
if_icmpne L1
aload 0
getfield net/hockeyapp/android/e/d/a [B
areturn
L1:
aload 0
getfield net/hockeyapp/android/e/d/b I
newarray byte
astore 3
aload 0
getfield net/hockeyapp/android/e/d/a [B
iconst_0
aload 3
iconst_0
aload 0
getfield net/hockeyapp/android/e/d/b I
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 3
areturn
.limit locals 4
.limit stack 5
.end method

.method private static a([BI)[B
aload 0
arraylength
istore 2
new net/hockeyapp/android/e/d
dup
iload 1
iload 2
iconst_3
imul
iconst_4
idiv
newarray byte
invokespecial net/hockeyapp/android/e/d/<init>(I[B)V
astore 3
aload 3
aload 0
iconst_0
iload 2
invokevirtual net/hockeyapp/android/e/d/a([BII)Z
ifne L0
new java/lang/IllegalArgumentException
dup
ldc "bad base-64"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 3
getfield net/hockeyapp/android/e/d/b I
aload 3
getfield net/hockeyapp/android/e/d/a [B
arraylength
if_icmpne L1
aload 3
getfield net/hockeyapp/android/e/d/a [B
areturn
L1:
aload 3
getfield net/hockeyapp/android/e/d/b I
newarray byte
astore 0
aload 3
getfield net/hockeyapp/android/e/d/a [B
iconst_0
aload 0
iconst_0
aload 3
getfield net/hockeyapp/android/e/d/b I
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 0
areturn
.limit locals 4
.limit stack 5
.end method

.method private static a([BII)[B
new net/hockeyapp/android/e/d
dup
iload 2
iload 1
iconst_3
imul
iconst_4
idiv
newarray byte
invokespecial net/hockeyapp/android/e/d/<init>(I[B)V
astore 3
aload 3
aload 0
iconst_0
iload 1
invokevirtual net/hockeyapp/android/e/d/a([BII)Z
ifne L0
new java/lang/IllegalArgumentException
dup
ldc "bad base-64"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 3
getfield net/hockeyapp/android/e/d/b I
aload 3
getfield net/hockeyapp/android/e/d/a [B
arraylength
if_icmpne L1
aload 3
getfield net/hockeyapp/android/e/d/a [B
areturn
L1:
aload 3
getfield net/hockeyapp/android/e/d/b I
newarray byte
astore 0
aload 3
getfield net/hockeyapp/android/e/d/a [B
iconst_0
aload 0
iconst_0
aload 3
getfield net/hockeyapp/android/e/d/b I
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 0
areturn
.limit locals 4
.limit stack 5
.end method

.method private static b([BI)[B
aload 0
iconst_0
aload 0
arraylength
iload 1
invokestatic net/hockeyapp/android/e/b/b([BIII)[B
areturn
.limit locals 2
.limit stack 4
.end method

.method private static b([BIII)[B
new net/hockeyapp/android/e/e
dup
iload 3
invokespecial net/hockeyapp/android/e/e/<init>(I)V
astore 6
iload 2
iconst_3
idiv
iconst_4
imul
istore 4
aload 6
getfield net/hockeyapp/android/e/e/e Z
ifeq L0
iload 4
istore 3
iload 2
iconst_3
irem
ifle L1
iload 4
iconst_4
iadd
istore 3
L1:
iload 3
istore 4
aload 6
getfield net/hockeyapp/android/e/e/f Z
ifeq L2
iload 3
istore 4
iload 2
ifle L2
iload 2
iconst_1
isub
bipush 57
idiv
istore 5
aload 6
getfield net/hockeyapp/android/e/e/g Z
ifeq L3
iconst_2
istore 4
L4:
iload 3
iload 4
iload 5
iconst_1
iadd
imul
iadd
istore 4
L2:
aload 6
iload 4
newarray byte
putfield net/hockeyapp/android/e/e/a [B
aload 6
aload 0
iload 1
iload 2
invokevirtual net/hockeyapp/android/e/e/a([BII)Z
pop
getstatic net/hockeyapp/android/e/b/f Z
ifne L5
aload 6
getfield net/hockeyapp/android/e/e/b I
iload 4
if_icmpeq L5
new java/lang/AssertionError
dup
invokespecial java/lang/AssertionError/<init>()V
athrow
L0:
iload 4
istore 3
iload 2
iconst_3
irem
tableswitch 0
L1
L6
L7
default : L8
L8:
iload 4
istore 3
goto L1
L6:
iload 4
iconst_2
iadd
istore 3
goto L1
L7:
iload 4
iconst_3
iadd
istore 3
goto L1
L3:
iconst_1
istore 4
goto L4
L5:
aload 6
getfield net/hockeyapp/android/e/e/a [B
areturn
.limit locals 7
.limit stack 4
.end method
