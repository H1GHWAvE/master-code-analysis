.bytecode 50.0
.class public final synchronized net/hockeyapp/android/e/q
.super java/lang/Object

.field private static final 'c' [C

.field 'a' Ljava/io/ByteArrayOutputStream;

.field 'b' Ljava/lang/String;

.field private 'd' Z

.field private 'e' Z

.method static <clinit>()V
ldc "-_1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
invokevirtual java/lang/String/toCharArray()[C
putstatic net/hockeyapp/android/e/q/c [C
return
.limit locals 0
.limit stack 1
.end method

.method public <init>()V
iconst_0
istore 1
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
iconst_0
putfield net/hockeyapp/android/e/q/e Z
aload 0
iconst_0
putfield net/hockeyapp/android/e/q/d Z
aload 0
new java/io/ByteArrayOutputStream
dup
invokespecial java/io/ByteArrayOutputStream/<init>()V
putfield net/hockeyapp/android/e/q/a Ljava/io/ByteArrayOutputStream;
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
astore 2
new java/util/Random
dup
invokespecial java/util/Random/<init>()V
astore 3
L0:
iload 1
bipush 30
if_icmpge L1
aload 2
getstatic net/hockeyapp/android/e/q/c [C
aload 3
getstatic net/hockeyapp/android/e/q/c [C
arraylength
invokevirtual java/util/Random/nextInt(I)I
caload
invokevirtual java/lang/StringBuffer/append(C)Ljava/lang/StringBuffer;
pop
iload 1
iconst_1
iadd
istore 1
goto L0
L1:
aload 0
aload 2
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
putfield net/hockeyapp/android/e/q/b Ljava/lang/String;
return
.limit locals 4
.limit stack 4
.end method

.method private a(Ljava/lang/String;Ljava/io/File;Z)V
aload 0
aload 1
aload 2
invokevirtual java/io/File/getName()Ljava/lang/String;
new java/io/FileInputStream
dup
aload 2
invokespecial java/io/FileInputStream/<init>(Ljava/io/File;)V
iload 3
invokevirtual net/hockeyapp/android/e/q/a(Ljava/lang/String;Ljava/lang/String;Ljava/io/InputStream;Z)V
return
.limit locals 4
.limit stack 6
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
aload 0
invokevirtual net/hockeyapp/android/e/q/a()V
aload 0
getfield net/hockeyapp/android/e/q/a Ljava/io/ByteArrayOutputStream;
new java/lang/StringBuilder
dup
ldc "Content-Disposition: form-data; name=\""
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\"\r\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/lang/String/getBytes()[B
invokevirtual java/io/ByteArrayOutputStream/write([B)V
aload 0
getfield net/hockeyapp/android/e/q/a Ljava/io/ByteArrayOutputStream;
ldc "Content-Type: text/plain; charset=UTF-8\r\n"
invokevirtual java/lang/String/getBytes()[B
invokevirtual java/io/ByteArrayOutputStream/write([B)V
aload 0
getfield net/hockeyapp/android/e/q/a Ljava/io/ByteArrayOutputStream;
ldc "Content-Transfer-Encoding: 8bit\r\n\r\n"
invokevirtual java/lang/String/getBytes()[B
invokevirtual java/io/ByteArrayOutputStream/write([B)V
aload 0
getfield net/hockeyapp/android/e/q/a Ljava/io/ByteArrayOutputStream;
aload 2
invokevirtual java/lang/String/getBytes()[B
invokevirtual java/io/ByteArrayOutputStream/write([B)V
aload 0
getfield net/hockeyapp/android/e/q/a Ljava/io/ByteArrayOutputStream;
new java/lang/StringBuilder
dup
ldc "\r\n--"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
getfield net/hockeyapp/android/e/q/b Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\r\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/lang/String/getBytes()[B
invokevirtual java/io/ByteArrayOutputStream/write([B)V
return
.limit locals 3
.limit stack 4
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/io/InputStream;Ljava/lang/String;Z)V
.catch all from L0 to L1 using L2
.catch all from L1 to L3 using L2
.catch all from L4 to L5 using L2
.catch java/io/IOException from L6 to L7 using L8
.catch all from L9 to L10 using L2
.catch all from L11 to L12 using L2
.catch java/io/IOException from L12 to L13 using L14
.catch all from L15 to L16 using L2
aload 0
invokevirtual net/hockeyapp/android/e/q/a()V
L0:
new java/lang/StringBuilder
dup
ldc "Content-Type: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\r\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 4
aload 0
getfield net/hockeyapp/android/e/q/a Ljava/io/ByteArrayOutputStream;
new java/lang/StringBuilder
dup
ldc "Content-Disposition: form-data; name=\""
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\"; filename=\""
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\"\r\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/lang/String/getBytes()[B
invokevirtual java/io/ByteArrayOutputStream/write([B)V
aload 0
getfield net/hockeyapp/android/e/q/a Ljava/io/ByteArrayOutputStream;
aload 4
invokevirtual java/lang/String/getBytes()[B
invokevirtual java/io/ByteArrayOutputStream/write([B)V
aload 0
getfield net/hockeyapp/android/e/q/a Ljava/io/ByteArrayOutputStream;
ldc "Content-Transfer-Encoding: binary\r\n\r\n"
invokevirtual java/lang/String/getBytes()[B
invokevirtual java/io/ByteArrayOutputStream/write([B)V
sipush 4096
newarray byte
astore 1
L1:
aload 3
aload 1
invokevirtual java/io/InputStream/read([B)I
istore 6
L3:
iload 6
iconst_m1
if_icmpeq L9
L4:
aload 0
getfield net/hockeyapp/android/e/q/a Ljava/io/ByteArrayOutputStream;
aload 1
iconst_0
iload 6
invokevirtual java/io/ByteArrayOutputStream/write([BII)V
L5:
goto L1
L2:
astore 1
L6:
aload 3
invokevirtual java/io/InputStream/close()V
L7:
aload 1
athrow
L9:
aload 0
getfield net/hockeyapp/android/e/q/a Ljava/io/ByteArrayOutputStream;
invokevirtual java/io/ByteArrayOutputStream/flush()V
L10:
iload 5
ifeq L15
L11:
aload 0
invokevirtual net/hockeyapp/android/e/q/b()V
L12:
aload 3
invokevirtual java/io/InputStream/close()V
L13:
return
L15:
aload 0
getfield net/hockeyapp/android/e/q/a Ljava/io/ByteArrayOutputStream;
new java/lang/StringBuilder
dup
ldc "\r\n--"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
getfield net/hockeyapp/android/e/q/b Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\r\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/lang/String/getBytes()[B
invokevirtual java/io/ByteArrayOutputStream/write([B)V
L16:
goto L12
L14:
astore 1
aload 1
invokevirtual java/io/IOException/printStackTrace()V
return
L8:
astore 2
aload 2
invokevirtual java/io/IOException/printStackTrace()V
goto L7
.limit locals 7
.limit stack 4
.end method

.method private c()Ljava/lang/String;
aload 0
getfield net/hockeyapp/android/e/q/b Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method private d()J
aload 0
invokevirtual net/hockeyapp/android/e/q/b()V
aload 0
getfield net/hockeyapp/android/e/q/a Ljava/io/ByteArrayOutputStream;
invokevirtual java/io/ByteArrayOutputStream/toByteArray()[B
arraylength
i2l
lreturn
.limit locals 1
.limit stack 2
.end method

.method private e()Ljava/lang/String;
new java/lang/StringBuilder
dup
ldc "multipart/form-data; boundary="
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
getfield net/hockeyapp/android/e/q/b Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 3
.end method

.method private f()Ljava/io/ByteArrayOutputStream;
aload 0
invokevirtual net/hockeyapp/android/e/q/b()V
aload 0
getfield net/hockeyapp/android/e/q/a Ljava/io/ByteArrayOutputStream;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final a()V
aload 0
getfield net/hockeyapp/android/e/q/e Z
ifne L0
aload 0
getfield net/hockeyapp/android/e/q/a Ljava/io/ByteArrayOutputStream;
new java/lang/StringBuilder
dup
ldc "--"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
getfield net/hockeyapp/android/e/q/b Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\r\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/lang/String/getBytes()[B
invokevirtual java/io/ByteArrayOutputStream/write([B)V
L0:
aload 0
iconst_1
putfield net/hockeyapp/android/e/q/e Z
return
.limit locals 1
.limit stack 4
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/io/InputStream;Z)V
.catch all from L0 to L1 using L2
.catch all from L1 to L3 using L2
.catch all from L4 to L5 using L2
.catch java/io/IOException from L6 to L7 using L8
.catch all from L9 to L10 using L2
.catch all from L11 to L12 using L2
.catch java/io/IOException from L12 to L13 using L14
.catch all from L15 to L16 using L2
aload 0
invokevirtual net/hockeyapp/android/e/q/a()V
L0:
new java/lang/StringBuilder
dup
ldc "Content-Type: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
ldc "application/octet-stream"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\r\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 6
aload 0
getfield net/hockeyapp/android/e/q/a Ljava/io/ByteArrayOutputStream;
new java/lang/StringBuilder
dup
ldc "Content-Disposition: form-data; name=\""
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\"; filename=\""
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\"\r\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/lang/String/getBytes()[B
invokevirtual java/io/ByteArrayOutputStream/write([B)V
aload 0
getfield net/hockeyapp/android/e/q/a Ljava/io/ByteArrayOutputStream;
aload 6
invokevirtual java/lang/String/getBytes()[B
invokevirtual java/io/ByteArrayOutputStream/write([B)V
aload 0
getfield net/hockeyapp/android/e/q/a Ljava/io/ByteArrayOutputStream;
ldc "Content-Transfer-Encoding: binary\r\n\r\n"
invokevirtual java/lang/String/getBytes()[B
invokevirtual java/io/ByteArrayOutputStream/write([B)V
sipush 4096
newarray byte
astore 1
L1:
aload 3
aload 1
invokevirtual java/io/InputStream/read([B)I
istore 5
L3:
iload 5
iconst_m1
if_icmpeq L9
L4:
aload 0
getfield net/hockeyapp/android/e/q/a Ljava/io/ByteArrayOutputStream;
aload 1
iconst_0
iload 5
invokevirtual java/io/ByteArrayOutputStream/write([BII)V
L5:
goto L1
L2:
astore 1
L6:
aload 3
invokevirtual java/io/InputStream/close()V
L7:
aload 1
athrow
L9:
aload 0
getfield net/hockeyapp/android/e/q/a Ljava/io/ByteArrayOutputStream;
invokevirtual java/io/ByteArrayOutputStream/flush()V
L10:
iload 4
ifeq L15
L11:
aload 0
invokevirtual net/hockeyapp/android/e/q/b()V
L12:
aload 3
invokevirtual java/io/InputStream/close()V
L13:
return
L15:
aload 0
getfield net/hockeyapp/android/e/q/a Ljava/io/ByteArrayOutputStream;
new java/lang/StringBuilder
dup
ldc "\r\n--"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
getfield net/hockeyapp/android/e/q/b Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\r\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/lang/String/getBytes()[B
invokevirtual java/io/ByteArrayOutputStream/write([B)V
L16:
goto L12
L14:
astore 1
aload 1
invokevirtual java/io/IOException/printStackTrace()V
return
L8:
astore 2
aload 2
invokevirtual java/io/IOException/printStackTrace()V
goto L7
.limit locals 7
.limit stack 4
.end method

.method public final b()V
.catch java/io/IOException from L0 to L1 using L2
aload 0
getfield net/hockeyapp/android/e/q/d Z
ifeq L0
return
L0:
aload 0
getfield net/hockeyapp/android/e/q/a Ljava/io/ByteArrayOutputStream;
new java/lang/StringBuilder
dup
ldc "\r\n--"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
getfield net/hockeyapp/android/e/q/b Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "--\r\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/lang/String/getBytes()[B
invokevirtual java/io/ByteArrayOutputStream/write([B)V
L1:
aload 0
iconst_1
putfield net/hockeyapp/android/e/q/d Z
return
L2:
astore 1
aload 1
invokevirtual java/io/IOException/printStackTrace()V
goto L1
.limit locals 2
.limit stack 4
.end method
