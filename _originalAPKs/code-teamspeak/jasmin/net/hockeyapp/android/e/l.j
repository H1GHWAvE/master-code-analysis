.bytecode 50.0
.class public final synchronized net/hockeyapp/android/e/l
.super java/lang/Object

.field public static final 'a' Ljava/lang/String; = "UTF-8"

.field private static final 'c' I = 120000


.field public 'b' Ljava/lang/String;

.field private final 'd' Ljava/lang/String;

.field private 'e' Ljava/lang/String;

.field private 'f' Lnet/hockeyapp/android/e/q;

.field private 'g' I

.field private final 'h' Ljava/util/Map;

.method public <init>(Ljava/lang/String;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
ldc_w 120000
putfield net/hockeyapp/android/e/l/g I
aload 0
aload 1
putfield net/hockeyapp/android/e/l/d Ljava/lang/String;
aload 0
new java/util/HashMap
dup
invokespecial java/util/HashMap/<init>()V
putfield net/hockeyapp/android/e/l/h Ljava/util/Map;
return
.limit locals 2
.limit stack 3
.end method

.method private static a(Ljava/util/Map;Ljava/lang/String;)Ljava/lang/String;
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 2
aload 0
invokeinterface java/util/Map/keySet()Ljava/util/Set; 0
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
astore 3
L0:
aload 3
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 3
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/lang/String
astore 5
aload 0
aload 5
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast java/lang/String
astore 4
aload 5
aload 1
invokestatic java/net/URLEncoder/encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
astore 5
aload 4
aload 1
invokestatic java/net/URLEncoder/encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
astore 4
aload 2
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 5
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
goto L0
L1:
ldc "&"
aload 2
invokestatic android/text/TextUtils/join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;
areturn
.limit locals 6
.limit stack 3
.end method

.method private a(I)Lnet/hockeyapp/android/e/l;
iload 1
ifge L0
new java/lang/IllegalArgumentException
dup
ldc "Timeout has to be positive."
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
iload 1
putfield net/hockeyapp/android/e/l/g I
aload 0
areturn
.limit locals 2
.limit stack 3
.end method

.method private a(Ljava/lang/String;)Lnet/hockeyapp/android/e/l;
aload 0
aload 1
putfield net/hockeyapp/android/e/l/b Ljava/lang/String;
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method private b(Ljava/lang/String;)Lnet/hockeyapp/android/e/l;
aload 0
aload 1
putfield net/hockeyapp/android/e/l/e Ljava/lang/String;
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;)Lnet/hockeyapp/android/e/l;
aload 0
ldc "Authorization"
new java/lang/StringBuilder
dup
ldc "Basic "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ":"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/lang/String/getBytes()[B
invokestatic net/hockeyapp/android/e/b/a([B)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual net/hockeyapp/android/e/l/a(Ljava/lang/String;Ljava/lang/String;)Lnet/hockeyapp/android/e/l;
pop
aload 0
areturn
.limit locals 3
.limit stack 5
.end method

.method public final a()Ljava/net/HttpURLConnection;
.catch java/io/IOException from L0 to L1 using L2
.catch java/io/IOException from L1 to L3 using L2
.catch java/io/IOException from L3 to L4 using L2
.catch java/io/IOException from L4 to L5 using L2
.catch java/io/IOException from L5 to L6 using L2
.catch java/io/IOException from L7 to L8 using L2
.catch java/io/IOException from L8 to L9 using L2
L0:
new java/net/URL
dup
aload 0
getfield net/hockeyapp/android/e/l/d Ljava/lang/String;
invokespecial java/net/URL/<init>(Ljava/lang/String;)V
invokevirtual java/net/URL/openConnection()Ljava/net/URLConnection;
checkcast java/net/HttpURLConnection
astore 1
aload 1
aload 0
getfield net/hockeyapp/android/e/l/g I
invokevirtual java/net/HttpURLConnection/setConnectTimeout(I)V
aload 1
aload 0
getfield net/hockeyapp/android/e/l/g I
invokevirtual java/net/HttpURLConnection/setReadTimeout(I)V
getstatic android/os/Build$VERSION/SDK_INT I
bipush 9
if_icmpgt L1
aload 1
ldc "Connection"
ldc "close"
invokevirtual java/net/HttpURLConnection/setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V
L1:
aload 0
getfield net/hockeyapp/android/e/l/b Ljava/lang/String;
invokestatic android/text/TextUtils/isEmpty(Ljava/lang/CharSequence;)Z
ifne L4
aload 1
aload 0
getfield net/hockeyapp/android/e/l/b Ljava/lang/String;
invokevirtual java/net/HttpURLConnection/setRequestMethod(Ljava/lang/String;)V
aload 0
getfield net/hockeyapp/android/e/l/e Ljava/lang/String;
invokestatic android/text/TextUtils/isEmpty(Ljava/lang/CharSequence;)Z
ifeq L3
aload 0
getfield net/hockeyapp/android/e/l/b Ljava/lang/String;
ldc "POST"
invokevirtual java/lang/String/equalsIgnoreCase(Ljava/lang/String;)Z
ifne L3
aload 0
getfield net/hockeyapp/android/e/l/b Ljava/lang/String;
ldc "PUT"
invokevirtual java/lang/String/equalsIgnoreCase(Ljava/lang/String;)Z
ifeq L4
L3:
aload 1
iconst_1
invokevirtual java/net/HttpURLConnection/setDoOutput(Z)V
L4:
aload 0
getfield net/hockeyapp/android/e/l/h Ljava/util/Map;
invokeinterface java/util/Map/keySet()Ljava/util/Set; 0
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
astore 2
L5:
aload 2
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L7
aload 2
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/lang/String
astore 3
aload 1
aload 3
aload 0
getfield net/hockeyapp/android/e/l/h Ljava/util/Map;
aload 3
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast java/lang/String
invokevirtual java/net/HttpURLConnection/setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V
L6:
goto L5
L2:
astore 1
new java/lang/RuntimeException
dup
aload 1
invokespecial java/lang/RuntimeException/<init>(Ljava/lang/Throwable;)V
athrow
L7:
aload 0
getfield net/hockeyapp/android/e/l/e Ljava/lang/String;
invokestatic android/text/TextUtils/isEmpty(Ljava/lang/CharSequence;)Z
ifne L8
new java/io/BufferedWriter
dup
new java/io/OutputStreamWriter
dup
aload 1
invokevirtual java/net/HttpURLConnection/getOutputStream()Ljava/io/OutputStream;
ldc "UTF-8"
invokespecial java/io/OutputStreamWriter/<init>(Ljava/io/OutputStream;Ljava/lang/String;)V
invokespecial java/io/BufferedWriter/<init>(Ljava/io/Writer;)V
astore 2
aload 2
aload 0
getfield net/hockeyapp/android/e/l/e Ljava/lang/String;
invokevirtual java/io/BufferedWriter/write(Ljava/lang/String;)V
aload 2
invokevirtual java/io/BufferedWriter/flush()V
aload 2
invokevirtual java/io/BufferedWriter/close()V
L8:
aload 0
getfield net/hockeyapp/android/e/l/f Lnet/hockeyapp/android/e/q;
ifnull L9
aload 0
getfield net/hockeyapp/android/e/l/f Lnet/hockeyapp/android/e/q;
astore 2
aload 2
invokevirtual net/hockeyapp/android/e/q/b()V
aload 1
ldc "Content-Length"
aload 2
getfield net/hockeyapp/android/e/q/a Ljava/io/ByteArrayOutputStream;
invokevirtual java/io/ByteArrayOutputStream/toByteArray()[B
arraylength
i2l
invokestatic java/lang/String/valueOf(J)Ljava/lang/String;
invokevirtual java/net/HttpURLConnection/setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V
new java/io/BufferedOutputStream
dup
aload 1
invokevirtual java/net/HttpURLConnection/getOutputStream()Ljava/io/OutputStream;
invokespecial java/io/BufferedOutputStream/<init>(Ljava/io/OutputStream;)V
astore 2
aload 0
getfield net/hockeyapp/android/e/l/f Lnet/hockeyapp/android/e/q;
astore 3
aload 3
invokevirtual net/hockeyapp/android/e/q/b()V
aload 2
aload 3
getfield net/hockeyapp/android/e/q/a Ljava/io/ByteArrayOutputStream;
invokevirtual java/io/ByteArrayOutputStream/toByteArray()[B
invokevirtual java/io/BufferedOutputStream/write([B)V
aload 2
invokevirtual java/io/BufferedOutputStream/flush()V
aload 2
invokevirtual java/io/BufferedOutputStream/close()V
L9:
aload 1
areturn
.limit locals 4
.limit stack 6
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)Lnet/hockeyapp/android/e/l;
aload 0
getfield net/hockeyapp/android/e/l/h Ljava/util/Map;
aload 1
aload 2
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
aload 0
areturn
.limit locals 3
.limit stack 3
.end method

.method public final a(Ljava/util/Map;)Lnet/hockeyapp/android/e/l;
.catch java/io/UnsupportedEncodingException from L0 to L1 using L2
.catch java/io/UnsupportedEncodingException from L1 to L3 using L2
.catch java/io/UnsupportedEncodingException from L4 to L5 using L2
L0:
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 2
aload 1
invokeinterface java/util/Map/keySet()Ljava/util/Set; 0
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
astore 3
L1:
aload 3
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L4
aload 3
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/lang/String
astore 5
aload 1
aload 5
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast java/lang/String
astore 4
aload 5
ldc "UTF-8"
invokestatic java/net/URLEncoder/encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
astore 5
aload 4
ldc "UTF-8"
invokestatic java/net/URLEncoder/encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
astore 4
aload 2
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 5
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
L3:
goto L1
L2:
astore 1
new java/lang/RuntimeException
dup
aload 1
invokespecial java/lang/RuntimeException/<init>(Ljava/lang/Throwable;)V
athrow
L4:
ldc "&"
aload 2
invokestatic android/text/TextUtils/join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;
astore 1
aload 0
ldc "Content-Type"
ldc "application/x-www-form-urlencoded"
invokevirtual net/hockeyapp/android/e/l/a(Ljava/lang/String;Ljava/lang/String;)Lnet/hockeyapp/android/e/l;
pop
aload 0
aload 1
putfield net/hockeyapp/android/e/l/e Ljava/lang/String;
L5:
aload 0
areturn
.limit locals 6
.limit stack 3
.end method

.method public final a(Ljava/util/Map;Landroid/content/Context;Ljava/util/List;)Lnet/hockeyapp/android/e/l;
.catch java/io/IOException from L0 to L1 using L2
.catch java/io/IOException from L1 to L3 using L2
.catch java/io/IOException from L4 to L5 using L2
.catch java/io/IOException from L6 to L7 using L2
.catch java/io/IOException from L8 to L9 using L2
L0:
aload 0
new net/hockeyapp/android/e/q
dup
invokespecial net/hockeyapp/android/e/q/<init>()V
putfield net/hockeyapp/android/e/l/f Lnet/hockeyapp/android/e/q;
aload 0
getfield net/hockeyapp/android/e/l/f Lnet/hockeyapp/android/e/q;
invokevirtual net/hockeyapp/android/e/q/a()V
aload 1
invokeinterface java/util/Map/keySet()Ljava/util/Set; 0
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
astore 6
L1:
aload 6
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L10
aload 6
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/lang/String
astore 7
aload 0
getfield net/hockeyapp/android/e/l/f Lnet/hockeyapp/android/e/q;
astore 8
aload 1
aload 7
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast java/lang/String
astore 9
aload 8
invokevirtual net/hockeyapp/android/e/q/a()V
aload 8
getfield net/hockeyapp/android/e/q/a Ljava/io/ByteArrayOutputStream;
new java/lang/StringBuilder
dup
ldc "Content-Disposition: form-data; name=\""
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 7
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\"\r\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/lang/String/getBytes()[B
invokevirtual java/io/ByteArrayOutputStream/write([B)V
aload 8
getfield net/hockeyapp/android/e/q/a Ljava/io/ByteArrayOutputStream;
ldc "Content-Type: text/plain; charset=UTF-8\r\n"
invokevirtual java/lang/String/getBytes()[B
invokevirtual java/io/ByteArrayOutputStream/write([B)V
aload 8
getfield net/hockeyapp/android/e/q/a Ljava/io/ByteArrayOutputStream;
ldc "Content-Transfer-Encoding: 8bit\r\n\r\n"
invokevirtual java/lang/String/getBytes()[B
invokevirtual java/io/ByteArrayOutputStream/write([B)V
aload 8
getfield net/hockeyapp/android/e/q/a Ljava/io/ByteArrayOutputStream;
aload 9
invokevirtual java/lang/String/getBytes()[B
invokevirtual java/io/ByteArrayOutputStream/write([B)V
aload 8
getfield net/hockeyapp/android/e/q/a Ljava/io/ByteArrayOutputStream;
new java/lang/StringBuilder
dup
ldc "\r\n--"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 8
getfield net/hockeyapp/android/e/q/b Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\r\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/lang/String/getBytes()[B
invokevirtual java/io/ByteArrayOutputStream/write([B)V
L3:
goto L1
L2:
astore 1
new java/lang/RuntimeException
dup
aload 1
invokespecial java/lang/RuntimeException/<init>(Ljava/lang/Throwable;)V
athrow
L10:
iconst_0
istore 4
L4:
iload 4
aload 3
invokeinterface java/util/List/size()I 0
if_icmpge L8
aload 3
iload 4
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast android/net/Uri
astore 6
iload 4
aload 3
invokeinterface java/util/List/size()I 0
iconst_1
isub
if_icmpne L11
L5:
iconst_1
istore 5
L6:
aload 2
invokevirtual android/content/Context/getContentResolver()Landroid/content/ContentResolver;
aload 6
invokevirtual android/content/ContentResolver/openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;
astore 1
aload 6
invokevirtual android/net/Uri/getLastPathSegment()Ljava/lang/String;
astore 6
aload 0
getfield net/hockeyapp/android/e/l/f Lnet/hockeyapp/android/e/q;
new java/lang/StringBuilder
dup
ldc "attachment"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
iload 4
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aload 6
aload 1
iload 5
invokevirtual net/hockeyapp/android/e/q/a(Ljava/lang/String;Ljava/lang/String;Ljava/io/InputStream;Z)V
L7:
iload 4
iconst_1
iadd
istore 4
goto L4
L8:
aload 0
getfield net/hockeyapp/android/e/l/f Lnet/hockeyapp/android/e/q;
invokevirtual net/hockeyapp/android/e/q/b()V
aload 0
ldc "Content-Type"
new java/lang/StringBuilder
dup
ldc "multipart/form-data; boundary="
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
getfield net/hockeyapp/android/e/l/f Lnet/hockeyapp/android/e/q;
getfield net/hockeyapp/android/e/q/b Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual net/hockeyapp/android/e/l/a(Ljava/lang/String;Ljava/lang/String;)Lnet/hockeyapp/android/e/l;
pop
L9:
aload 0
areturn
L11:
iconst_0
istore 5
goto L6
.limit locals 10
.limit stack 5
.end method
