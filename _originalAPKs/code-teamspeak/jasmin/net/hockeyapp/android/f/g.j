.bytecode 50.0
.class public final synchronized net/hockeyapp/android/f/g
.super android/widget/RelativeLayout

.method private <init>(Landroid/content/Context;)V
aload 0
aload 1
ldc ""
invokespecial net/hockeyapp/android/f/g/<init>(Landroid/content/Context;Ljava/lang/String;)V
return
.limit locals 2
.limit stack 3
.end method

.method public <init>(Landroid/content/Context;Ljava/lang/String;)V
aload 0
aload 1
invokespecial android/widget/RelativeLayout/<init>(Landroid/content/Context;)V
new android/widget/RelativeLayout$LayoutParams
dup
iconst_m1
iconst_m1
invokespecial android/widget/RelativeLayout$LayoutParams/<init>(II)V
astore 4
aload 0
iconst_m1
invokevirtual net/hockeyapp/android/f/g/setBackgroundColor(I)V
aload 0
aload 4
invokevirtual net/hockeyapp/android/f/g/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
new android/widget/RelativeLayout$LayoutParams
dup
iconst_m1
iconst_1
ldc_w 3.0F
aload 0
invokevirtual net/hockeyapp/android/f/g/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
invokestatic android/util/TypedValue/applyDimension(IFLandroid/util/DisplayMetrics;)F
f2i
invokespecial android/widget/RelativeLayout$LayoutParams/<init>(II)V
astore 4
aload 4
bipush 10
iconst_m1
invokevirtual android/widget/RelativeLayout$LayoutParams/addRule(II)V
new android/widget/ImageView
dup
aload 1
invokespecial android/widget/ImageView/<init>(Landroid/content/Context;)V
astore 5
aload 5
aload 4
invokevirtual android/widget/ImageView/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 5
invokestatic net/hockeyapp/android/e/aa/a()Landroid/graphics/drawable/Drawable;
invokevirtual android/widget/ImageView/setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
aload 0
aload 5
invokevirtual net/hockeyapp/android/f/g/addView(Landroid/view/View;)V
iconst_1
ldc_w 20.0F
aload 0
invokevirtual net/hockeyapp/android/f/g/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
invokestatic android/util/TypedValue/applyDimension(IFLandroid/util/DisplayMetrics;)F
f2i
istore 3
new android/widget/RelativeLayout$LayoutParams
dup
iconst_m1
bipush -2
invokespecial android/widget/RelativeLayout$LayoutParams/<init>(II)V
astore 4
aload 4
bipush 13
iconst_m1
invokevirtual android/widget/RelativeLayout$LayoutParams/addRule(II)V
aload 4
iload 3
iload 3
iload 3
iload 3
invokevirtual android/widget/RelativeLayout$LayoutParams/setMargins(IIII)V
new android/widget/TextView
dup
aload 1
invokespecial android/widget/TextView/<init>(Landroid/content/Context;)V
astore 1
aload 1
bipush 17
invokevirtual android/widget/TextView/setGravity(I)V
aload 1
aload 4
invokevirtual android/widget/TextView/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 1
aload 2
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
aload 1
ldc_w -16777216
invokevirtual android/widget/TextView/setTextColor(I)V
aload 0
aload 1
invokevirtual net/hockeyapp/android/f/g/addView(Landroid/view/View;)V
return
.limit locals 6
.limit stack 6
.end method

.method private a()V
new android/widget/RelativeLayout$LayoutParams
dup
iconst_m1
iconst_m1
invokespecial android/widget/RelativeLayout$LayoutParams/<init>(II)V
astore 1
aload 0
iconst_m1
invokevirtual net/hockeyapp/android/f/g/setBackgroundColor(I)V
aload 0
aload 1
invokevirtual net/hockeyapp/android/f/g/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
return
.limit locals 2
.limit stack 4
.end method

.method private a(Landroid/content/Context;)V
new android/widget/RelativeLayout$LayoutParams
dup
iconst_m1
iconst_1
ldc_w 3.0F
aload 0
invokevirtual net/hockeyapp/android/f/g/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
invokestatic android/util/TypedValue/applyDimension(IFLandroid/util/DisplayMetrics;)F
f2i
invokespecial android/widget/RelativeLayout$LayoutParams/<init>(II)V
astore 2
aload 2
bipush 10
iconst_m1
invokevirtual android/widget/RelativeLayout$LayoutParams/addRule(II)V
new android/widget/ImageView
dup
aload 1
invokespecial android/widget/ImageView/<init>(Landroid/content/Context;)V
astore 1
aload 1
aload 2
invokevirtual android/widget/ImageView/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 1
invokestatic net/hockeyapp/android/e/aa/a()Landroid/graphics/drawable/Drawable;
invokevirtual android/widget/ImageView/setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
aload 0
aload 1
invokevirtual net/hockeyapp/android/f/g/addView(Landroid/view/View;)V
return
.limit locals 3
.limit stack 6
.end method

.method private a(Landroid/content/Context;Ljava/lang/String;)V
iconst_1
ldc_w 20.0F
aload 0
invokevirtual net/hockeyapp/android/f/g/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
invokestatic android/util/TypedValue/applyDimension(IFLandroid/util/DisplayMetrics;)F
f2i
istore 3
new android/widget/RelativeLayout$LayoutParams
dup
iconst_m1
bipush -2
invokespecial android/widget/RelativeLayout$LayoutParams/<init>(II)V
astore 4
aload 4
bipush 13
iconst_m1
invokevirtual android/widget/RelativeLayout$LayoutParams/addRule(II)V
aload 4
iload 3
iload 3
iload 3
iload 3
invokevirtual android/widget/RelativeLayout$LayoutParams/setMargins(IIII)V
new android/widget/TextView
dup
aload 1
invokespecial android/widget/TextView/<init>(Landroid/content/Context;)V
astore 1
aload 1
bipush 17
invokevirtual android/widget/TextView/setGravity(I)V
aload 1
aload 4
invokevirtual android/widget/TextView/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 1
aload 2
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
aload 1
ldc_w -16777216
invokevirtual android/widget/TextView/setTextColor(I)V
aload 0
aload 1
invokevirtual net/hockeyapp/android/f/g/addView(Landroid/view/View;)V
return
.limit locals 5
.limit stack 5
.end method
