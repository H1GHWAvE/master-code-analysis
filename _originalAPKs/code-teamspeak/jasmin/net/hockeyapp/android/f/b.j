.bytecode 50.0
.class public final synchronized net/hockeyapp/android/f/b
.super android/widget/FrameLayout

.field private static final 'e' I = 3


.field private static final 'f' I = 2


.field final 'a' Landroid/view/ViewGroup;

.field public final 'b' Ljava/lang/String;

.field public 'c' Landroid/widget/TextView;

.field public 'd' I

.field private final 'g' Landroid/content/Context;

.field private final 'h' Lnet/hockeyapp/android/c/e;

.field private final 'i' Landroid/net/Uri;

.field private 'j' Landroid/widget/ImageView;

.field private 'k' I

.field private 'l' I

.field private 'm' I

.field private 'n' I

.field private 'o' I

.method public <init>(Landroid/content/Context;Landroid/view/ViewGroup;Landroid/net/Uri;)V
aload 0
aload 1
invokespecial android/widget/FrameLayout/<init>(Landroid/content/Context;)V
aload 0
aload 1
putfield net/hockeyapp/android/f/b/g Landroid/content/Context;
aload 0
aload 2
putfield net/hockeyapp/android/f/b/a Landroid/view/ViewGroup;
aload 0
aconst_null
putfield net/hockeyapp/android/f/b/h Lnet/hockeyapp/android/c/e;
aload 0
aload 3
putfield net/hockeyapp/android/f/b/i Landroid/net/Uri;
aload 0
aload 3
invokevirtual android/net/Uri/getLastPathSegment()Ljava/lang/String;
putfield net/hockeyapp/android/f/b/b Ljava/lang/String;
aload 0
bipush 20
invokespecial net/hockeyapp/android/f/b/a(I)V
aload 0
aload 1
iconst_1
invokespecial net/hockeyapp/android/f/b/a(Landroid/content/Context;Z)V
aload 0
getfield net/hockeyapp/android/f/b/c Landroid/widget/TextView;
aload 0
getfield net/hockeyapp/android/f/b/b Ljava/lang/String;
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
new net/hockeyapp/android/f/c
dup
aload 0
invokespecial net/hockeyapp/android/f/c/<init>(Lnet/hockeyapp/android/f/b;)V
iconst_0
anewarray java/lang/Void
invokevirtual net/hockeyapp/android/f/c/execute([Ljava/lang/Object;)Landroid/os/AsyncTask;
pop
return
.limit locals 4
.limit stack 3
.end method

.method public <init>(Landroid/content/Context;Landroid/view/ViewGroup;Lnet/hockeyapp/android/c/e;)V
aload 0
aload 1
invokespecial android/widget/FrameLayout/<init>(Landroid/content/Context;)V
aload 0
aload 1
putfield net/hockeyapp/android/f/b/g Landroid/content/Context;
aload 0
aload 2
putfield net/hockeyapp/android/f/b/a Landroid/view/ViewGroup;
aload 0
aload 3
putfield net/hockeyapp/android/f/b/h Lnet/hockeyapp/android/c/e;
aload 0
new java/io/File
dup
invokestatic net/hockeyapp/android/a/a()Ljava/io/File;
aload 3
invokevirtual net/hockeyapp/android/c/e/a()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/io/File;Ljava/lang/String;)V
invokestatic android/net/Uri/fromFile(Ljava/io/File;)Landroid/net/Uri;
putfield net/hockeyapp/android/f/b/i Landroid/net/Uri;
aload 0
aload 3
getfield net/hockeyapp/android/c/e/c Ljava/lang/String;
putfield net/hockeyapp/android/f/b/b Ljava/lang/String;
aload 0
bipush 30
invokespecial net/hockeyapp/android/f/b/a(I)V
aload 0
aload 1
iconst_0
invokespecial net/hockeyapp/android/f/b/a(Landroid/content/Context;Z)V
aload 0
iconst_0
putfield net/hockeyapp/android/f/b/d I
aload 0
getfield net/hockeyapp/android/f/b/c Landroid/widget/TextView;
ldc "Loading..."
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
aload 0
iconst_0
invokevirtual net/hockeyapp/android/f/b/a(Z)V
return
.limit locals 4
.limit stack 5
.end method

.method static synthetic a(Lnet/hockeyapp/android/f/b;)Landroid/graphics/Bitmap;
aload 0
invokespecial net/hockeyapp/android/f/b/c()Landroid/graphics/Bitmap;
areturn
.limit locals 1
.limit stack 1
.end method

.method private a(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
aload 0
invokevirtual net/hockeyapp/android/f/b/getResources()Landroid/content/res/Resources;
aload 0
invokevirtual net/hockeyapp/android/f/b/getResources()Landroid/content/res/Resources;
aload 1
ldc "drawable"
ldc "android"
invokevirtual android/content/res/Resources/getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
invokevirtual android/content/res/Resources/getDrawable(I)Landroid/graphics/drawable/Drawable;
areturn
.limit locals 2
.limit stack 5
.end method

.method private a()V
aload 0
getfield net/hockeyapp/android/f/b/a Landroid/view/ViewGroup;
aload 0
invokevirtual android/view/ViewGroup/removeView(Landroid/view/View;)V
return
.limit locals 1
.limit stack 2
.end method

.method private a(I)V
aload 0
invokevirtual net/hockeyapp/android/f/b/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
astore 5
aload 0
iconst_1
ldc_w 10.0F
aload 5
invokestatic android/util/TypedValue/applyDimension(IFLandroid/util/DisplayMetrics;)F
invokestatic java/lang/Math/round(F)I
putfield net/hockeyapp/android/f/b/o I
iconst_1
iload 1
i2f
aload 5
invokestatic android/util/TypedValue/applyDimension(IFLandroid/util/DisplayMetrics;)F
invokestatic java/lang/Math/round(F)I
istore 1
aload 5
getfield android/util/DisplayMetrics/widthPixels I
istore 2
aload 0
getfield net/hockeyapp/android/f/b/o I
istore 3
aload 0
getfield net/hockeyapp/android/f/b/o I
istore 4
aload 0
iload 2
iload 1
iconst_2
imul
isub
iload 3
iconst_2
imul
isub
iconst_3
idiv
putfield net/hockeyapp/android/f/b/k I
aload 0
iload 2
iload 1
iconst_2
imul
isub
iload 4
iconst_1
imul
isub
iconst_2
idiv
putfield net/hockeyapp/android/f/b/m I
aload 0
aload 0
getfield net/hockeyapp/android/f/b/k I
iconst_2
imul
putfield net/hockeyapp/android/f/b/l I
aload 0
aload 0
getfield net/hockeyapp/android/f/b/m I
putfield net/hockeyapp/android/f/b/n I
return
.limit locals 6
.limit stack 4
.end method

.method private a(Landroid/content/Context;Z)V
aload 0
new android/widget/FrameLayout$LayoutParams
dup
bipush -2
bipush -2
bipush 80
invokespecial android/widget/FrameLayout$LayoutParams/<init>(III)V
invokevirtual net/hockeyapp/android/f/b/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 0
iconst_0
aload 0
getfield net/hockeyapp/android/f/b/o I
iconst_0
iconst_0
invokevirtual net/hockeyapp/android/f/b/setPadding(IIII)V
aload 0
new android/widget/ImageView
dup
aload 1
invokespecial android/widget/ImageView/<init>(Landroid/content/Context;)V
putfield net/hockeyapp/android/f/b/j Landroid/widget/ImageView;
new android/widget/LinearLayout
dup
aload 1
invokespecial android/widget/LinearLayout/<init>(Landroid/content/Context;)V
astore 3
aload 3
new android/widget/FrameLayout$LayoutParams
dup
iconst_m1
bipush -2
bipush 80
invokespecial android/widget/FrameLayout$LayoutParams/<init>(III)V
invokevirtual android/widget/LinearLayout/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 3
iconst_3
invokevirtual android/widget/LinearLayout/setGravity(I)V
aload 3
iconst_1
invokevirtual android/widget/LinearLayout/setOrientation(I)V
aload 3
ldc "#80262626"
invokestatic android/graphics/Color/parseColor(Ljava/lang/String;)I
invokevirtual android/widget/LinearLayout/setBackgroundColor(I)V
aload 0
new android/widget/TextView
dup
aload 1
invokespecial android/widget/TextView/<init>(Landroid/content/Context;)V
putfield net/hockeyapp/android/f/b/c Landroid/widget/TextView;
aload 0
getfield net/hockeyapp/android/f/b/c Landroid/widget/TextView;
new android/widget/FrameLayout$LayoutParams
dup
iconst_m1
bipush -2
bipush 17
invokespecial android/widget/FrameLayout$LayoutParams/<init>(III)V
invokevirtual android/widget/TextView/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 0
getfield net/hockeyapp/android/f/b/c Landroid/widget/TextView;
bipush 17
invokevirtual android/widget/TextView/setGravity(I)V
aload 0
getfield net/hockeyapp/android/f/b/c Landroid/widget/TextView;
ldc "#FFFFFF"
invokestatic android/graphics/Color/parseColor(Ljava/lang/String;)I
invokevirtual android/widget/TextView/setTextColor(I)V
aload 0
getfield net/hockeyapp/android/f/b/c Landroid/widget/TextView;
invokevirtual android/widget/TextView/setSingleLine()V
aload 0
getfield net/hockeyapp/android/f/b/c Landroid/widget/TextView;
getstatic android/text/TextUtils$TruncateAt/MIDDLE Landroid/text/TextUtils$TruncateAt;
invokevirtual android/widget/TextView/setEllipsize(Landroid/text/TextUtils$TruncateAt;)V
iload 2
ifeq L0
new android/widget/ImageButton
dup
aload 1
invokespecial android/widget/ImageButton/<init>(Landroid/content/Context;)V
astore 1
aload 1
new android/widget/FrameLayout$LayoutParams
dup
iconst_m1
bipush -2
bipush 80
invokespecial android/widget/FrameLayout$LayoutParams/<init>(III)V
invokevirtual android/widget/ImageButton/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 1
iconst_1
invokevirtual android/widget/ImageButton/setAdjustViewBounds(Z)V
aload 1
aload 0
ldc "ic_menu_delete"
invokespecial net/hockeyapp/android/f/b/a(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
invokevirtual android/widget/ImageButton/setImageDrawable(Landroid/graphics/drawable/Drawable;)V
aload 1
iconst_0
invokevirtual android/widget/ImageButton/setBackgroundResource(I)V
aload 1
new net/hockeyapp/android/f/d
dup
aload 0
invokespecial net/hockeyapp/android/f/d/<init>(Lnet/hockeyapp/android/f/b;)V
invokevirtual android/widget/ImageButton/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 3
aload 1
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
L0:
aload 3
aload 0
getfield net/hockeyapp/android/f/b/c Landroid/widget/TextView;
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
aload 0
aload 0
getfield net/hockeyapp/android/f/b/j Landroid/widget/ImageView;
invokevirtual net/hockeyapp/android/f/b/addView(Landroid/view/View;)V
aload 0
aload 3
invokevirtual net/hockeyapp/android/f/b/addView(Landroid/view/View;)V
return
.limit locals 4
.limit stack 6
.end method

.method private a(Landroid/graphics/Bitmap;I)V
aload 0
getfield net/hockeyapp/android/f/b/c Landroid/widget/TextView;
aload 0
getfield net/hockeyapp/android/f/b/b Ljava/lang/String;
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
aload 0
iload 2
putfield net/hockeyapp/android/f/b/d I
aload 1
ifnonnull L0
aload 0
iconst_1
invokevirtual net/hockeyapp/android/f/b/a(Z)V
return
L0:
aload 0
aload 1
iconst_1
invokevirtual net/hockeyapp/android/f/b/a(Landroid/graphics/Bitmap;Z)V
return
.limit locals 3
.limit stack 3
.end method

.method static synthetic a(Lnet/hockeyapp/android/f/b;Landroid/graphics/Bitmap;)V
aload 0
aload 1
iconst_0
invokevirtual net/hockeyapp/android/f/b/a(Landroid/graphics/Bitmap;Z)V
return
.limit locals 2
.limit stack 3
.end method

.method private b()V
aload 0
getfield net/hockeyapp/android/f/b/c Landroid/widget/TextView;
ldc "Error"
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
return
.limit locals 1
.limit stack 2
.end method

.method static synthetic b(Lnet/hockeyapp/android/f/b;)V
aload 0
iconst_0
invokevirtual net/hockeyapp/android/f/b/a(Z)V
return
.limit locals 1
.limit stack 2
.end method

.method private c()Landroid/graphics/Bitmap;
.catch java/lang/Throwable from L0 to L1 using L2
.catch java/lang/Throwable from L1 to L3 using L2
.catch java/lang/Throwable from L3 to L4 using L2
.catch java/lang/Throwable from L4 to L5 using L2
.catch java/lang/Throwable from L6 to L7 using L2
L0:
aload 0
aload 0
getfield net/hockeyapp/android/f/b/g Landroid/content/Context;
aload 0
getfield net/hockeyapp/android/f/b/i Landroid/net/Uri;
invokestatic net/hockeyapp/android/e/m/a(Landroid/content/Context;Landroid/net/Uri;)I
putfield net/hockeyapp/android/f/b/d I
aload 0
getfield net/hockeyapp/android/f/b/d I
iconst_1
if_icmpne L4
aload 0
getfield net/hockeyapp/android/f/b/m I
istore 1
L1:
aload 0
getfield net/hockeyapp/android/f/b/d I
iconst_1
if_icmpne L6
aload 0
getfield net/hockeyapp/android/f/b/n I
istore 2
L3:
aload 0
getfield net/hockeyapp/android/f/b/g Landroid/content/Context;
astore 3
aload 0
getfield net/hockeyapp/android/f/b/i Landroid/net/Uri;
astore 4
new android/graphics/BitmapFactory$Options
dup
invokespecial android/graphics/BitmapFactory$Options/<init>()V
astore 5
aload 5
iconst_1
putfield android/graphics/BitmapFactory$Options/inJustDecodeBounds Z
aload 3
invokevirtual android/content/Context/getContentResolver()Landroid/content/ContentResolver;
aload 4
invokevirtual android/content/ContentResolver/openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;
aconst_null
aload 5
invokestatic android/graphics/BitmapFactory/decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
pop
aload 5
aload 5
iload 1
iload 2
invokestatic net/hockeyapp/android/e/m/a(Landroid/graphics/BitmapFactory$Options;II)I
putfield android/graphics/BitmapFactory$Options/inSampleSize I
aload 5
iconst_0
putfield android/graphics/BitmapFactory$Options/inJustDecodeBounds Z
aload 3
invokevirtual android/content/Context/getContentResolver()Landroid/content/ContentResolver;
aload 4
invokevirtual android/content/ContentResolver/openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;
aconst_null
aload 5
invokestatic android/graphics/BitmapFactory/decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
areturn
L4:
aload 0
getfield net/hockeyapp/android/f/b/k I
istore 1
L5:
goto L1
L6:
aload 0
getfield net/hockeyapp/android/f/b/l I
istore 2
L7:
goto L3
L2:
astore 3
aconst_null
areturn
.limit locals 6
.limit stack 4
.end method

.method static synthetic c(Lnet/hockeyapp/android/f/b;)Landroid/net/Uri;
aload 0
getfield net/hockeyapp/android/f/b/i Landroid/net/Uri;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic d(Lnet/hockeyapp/android/f/b;)Landroid/content/Context;
aload 0
getfield net/hockeyapp/android/f/b/g Landroid/content/Context;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final a(Landroid/graphics/Bitmap;Z)V
aload 0
getfield net/hockeyapp/android/f/b/d I
iconst_1
if_icmpne L0
aload 0
getfield net/hockeyapp/android/f/b/m I
istore 3
L1:
aload 0
getfield net/hockeyapp/android/f/b/d I
iconst_1
if_icmpne L2
aload 0
getfield net/hockeyapp/android/f/b/n I
istore 4
L3:
aload 0
getfield net/hockeyapp/android/f/b/c Landroid/widget/TextView;
iload 3
invokevirtual android/widget/TextView/setMaxWidth(I)V
aload 0
getfield net/hockeyapp/android/f/b/c Landroid/widget/TextView;
iload 3
invokevirtual android/widget/TextView/setMinWidth(I)V
aload 0
getfield net/hockeyapp/android/f/b/j Landroid/widget/ImageView;
new android/widget/FrameLayout$LayoutParams
dup
bipush -2
bipush -2
invokespecial android/widget/FrameLayout$LayoutParams/<init>(II)V
invokevirtual android/widget/ImageView/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 0
getfield net/hockeyapp/android/f/b/j Landroid/widget/ImageView;
iconst_1
invokevirtual android/widget/ImageView/setAdjustViewBounds(Z)V
aload 0
getfield net/hockeyapp/android/f/b/j Landroid/widget/ImageView;
iload 3
invokevirtual android/widget/ImageView/setMinimumWidth(I)V
aload 0
getfield net/hockeyapp/android/f/b/j Landroid/widget/ImageView;
iload 3
invokevirtual android/widget/ImageView/setMaxWidth(I)V
aload 0
getfield net/hockeyapp/android/f/b/j Landroid/widget/ImageView;
iload 4
invokevirtual android/widget/ImageView/setMaxHeight(I)V
aload 0
getfield net/hockeyapp/android/f/b/j Landroid/widget/ImageView;
getstatic android/widget/ImageView$ScaleType/CENTER_INSIDE Landroid/widget/ImageView$ScaleType;
invokevirtual android/widget/ImageView/setScaleType(Landroid/widget/ImageView$ScaleType;)V
aload 0
getfield net/hockeyapp/android/f/b/j Landroid/widget/ImageView;
aload 1
invokevirtual android/widget/ImageView/setImageBitmap(Landroid/graphics/Bitmap;)V
aload 0
getfield net/hockeyapp/android/f/b/j Landroid/widget/ImageView;
new net/hockeyapp/android/f/e
dup
aload 0
iload 2
invokespecial net/hockeyapp/android/f/e/<init>(Lnet/hockeyapp/android/f/b;Z)V
invokevirtual android/widget/ImageView/setOnClickListener(Landroid/view/View$OnClickListener;)V
return
L0:
aload 0
getfield net/hockeyapp/android/f/b/k I
istore 3
goto L1
L2:
aload 0
getfield net/hockeyapp/android/f/b/l I
istore 4
goto L3
.limit locals 5
.limit stack 5
.end method

.method public final a(Z)V
aload 0
getfield net/hockeyapp/android/f/b/c Landroid/widget/TextView;
aload 0
getfield net/hockeyapp/android/f/b/k I
invokevirtual android/widget/TextView/setMaxWidth(I)V
aload 0
getfield net/hockeyapp/android/f/b/c Landroid/widget/TextView;
aload 0
getfield net/hockeyapp/android/f/b/k I
invokevirtual android/widget/TextView/setMinWidth(I)V
aload 0
getfield net/hockeyapp/android/f/b/j Landroid/widget/ImageView;
new android/widget/FrameLayout$LayoutParams
dup
bipush -2
bipush -2
invokespecial android/widget/FrameLayout$LayoutParams/<init>(II)V
invokevirtual android/widget/ImageView/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 0
getfield net/hockeyapp/android/f/b/j Landroid/widget/ImageView;
iconst_0
invokevirtual android/widget/ImageView/setAdjustViewBounds(Z)V
aload 0
getfield net/hockeyapp/android/f/b/j Landroid/widget/ImageView;
ldc "#eeeeee"
invokestatic android/graphics/Color/parseColor(Ljava/lang/String;)I
invokevirtual android/widget/ImageView/setBackgroundColor(I)V
aload 0
getfield net/hockeyapp/android/f/b/j Landroid/widget/ImageView;
aload 0
getfield net/hockeyapp/android/f/b/k I
i2f
ldc_w 1.2F
fmul
f2i
invokevirtual android/widget/ImageView/setMinimumHeight(I)V
aload 0
getfield net/hockeyapp/android/f/b/j Landroid/widget/ImageView;
aload 0
getfield net/hockeyapp/android/f/b/k I
invokevirtual android/widget/ImageView/setMinimumWidth(I)V
aload 0
getfield net/hockeyapp/android/f/b/j Landroid/widget/ImageView;
getstatic android/widget/ImageView$ScaleType/FIT_CENTER Landroid/widget/ImageView$ScaleType;
invokevirtual android/widget/ImageView/setScaleType(Landroid/widget/ImageView$ScaleType;)V
aload 0
getfield net/hockeyapp/android/f/b/j Landroid/widget/ImageView;
aload 0
ldc "ic_menu_attachment"
invokespecial net/hockeyapp/android/f/b/a(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
invokevirtual android/widget/ImageView/setImageDrawable(Landroid/graphics/drawable/Drawable;)V
aload 0
getfield net/hockeyapp/android/f/b/j Landroid/widget/ImageView;
new net/hockeyapp/android/f/f
dup
aload 0
iload 1
invokespecial net/hockeyapp/android/f/f/<init>(Lnet/hockeyapp/android/f/b;Z)V
invokevirtual android/widget/ImageView/setOnClickListener(Landroid/view/View$OnClickListener;)V
return
.limit locals 2
.limit stack 5
.end method

.method public final getAttachment()Lnet/hockeyapp/android/c/e;
aload 0
getfield net/hockeyapp/android/f/b/h Lnet/hockeyapp/android/c/e;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final getAttachmentUri()Landroid/net/Uri;
aload 0
getfield net/hockeyapp/android/f/b/i Landroid/net/Uri;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final getEffectiveMaxHeight()I
aload 0
getfield net/hockeyapp/android/f/b/d I
iconst_1
if_icmpne L0
aload 0
getfield net/hockeyapp/android/f/b/n I
ireturn
L0:
aload 0
getfield net/hockeyapp/android/f/b/l I
ireturn
.limit locals 1
.limit stack 2
.end method

.method public final getGap()I
aload 0
getfield net/hockeyapp/android/f/b/o I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final getMaxHeightLandscape()I
aload 0
getfield net/hockeyapp/android/f/b/n I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final getMaxHeightPortrait()I
aload 0
getfield net/hockeyapp/android/f/b/l I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final getWidthLandscape()I
aload 0
getfield net/hockeyapp/android/f/b/m I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final getWidthPortrait()I
aload 0
getfield net/hockeyapp/android/f/b/k I
ireturn
.limit locals 1
.limit stack 1
.end method
