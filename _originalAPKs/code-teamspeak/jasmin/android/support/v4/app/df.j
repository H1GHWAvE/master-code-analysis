.bytecode 50.0
.class public final synchronized android/support/v4/app/df
.super android/support/v4/app/ek

.field public static final 'e' Landroid/support/v4/app/el;

.field final 'a' Landroid/os/Bundle;

.field public 'b' I

.field public 'c' Ljava/lang/CharSequence;

.field public 'd' Landroid/app/PendingIntent;

.field private final 'f' [Landroid/support/v4/app/fn;

.method static <clinit>()V
new android/support/v4/app/dg
dup
invokespecial android/support/v4/app/dg/<init>()V
putstatic android/support/v4/app/df/e Landroid/support/v4/app/el;
return
.limit locals 0
.limit stack 2
.end method

.method public <init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)V
aload 0
iload 1
aload 2
aload 3
new android/os/Bundle
dup
invokespecial android/os/Bundle/<init>()V
aconst_null
invokespecial android/support/v4/app/df/<init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;Landroid/os/Bundle;[Landroid/support/v4/app/fn;)V
return
.limit locals 4
.limit stack 6
.end method

.method private <init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;Landroid/os/Bundle;[Landroid/support/v4/app/fn;)V
aload 0
invokespecial android/support/v4/app/ek/<init>()V
aload 0
iload 1
putfield android/support/v4/app/df/b I
aload 0
aload 2
invokestatic android/support/v4/app/dm/d(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
putfield android/support/v4/app/df/c Ljava/lang/CharSequence;
aload 0
aload 3
putfield android/support/v4/app/df/d Landroid/app/PendingIntent;
aload 4
ifnull L0
L1:
aload 0
aload 4
putfield android/support/v4/app/df/a Landroid/os/Bundle;
aload 0
aload 5
putfield android/support/v4/app/df/f [Landroid/support/v4/app/fn;
return
L0:
new android/os/Bundle
dup
invokespecial android/os/Bundle/<init>()V
astore 4
goto L1
.limit locals 6
.limit stack 2
.end method

.method synthetic <init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;Landroid/os/Bundle;[Landroid/support/v4/app/fn;B)V
aload 0
iload 1
aload 2
aload 3
aload 4
aload 5
invokespecial android/support/v4/app/df/<init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;Landroid/os/Bundle;[Landroid/support/v4/app/fn;)V
return
.limit locals 7
.limit stack 6
.end method

.method static synthetic a(Landroid/support/v4/app/df;)Landroid/os/Bundle;
aload 0
getfield android/support/v4/app/df/a Landroid/os/Bundle;
areturn
.limit locals 1
.limit stack 1
.end method

.method private f()[Landroid/support/v4/app/fn;
aload 0
getfield android/support/v4/app/df/f [Landroid/support/v4/app/fn;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final a()I
aload 0
getfield android/support/v4/app/df/b I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final b()Ljava/lang/CharSequence;
aload 0
getfield android/support/v4/app/df/c Ljava/lang/CharSequence;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final c()Landroid/app/PendingIntent;
aload 0
getfield android/support/v4/app/df/d Landroid/app/PendingIntent;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final d()Landroid/os/Bundle;
aload 0
getfield android/support/v4/app/df/a Landroid/os/Bundle;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final volatile synthetic e()[Landroid/support/v4/app/fw;
aload 0
getfield android/support/v4/app/df/f [Landroid/support/v4/app/fn;
areturn
.limit locals 1
.limit stack 1
.end method
