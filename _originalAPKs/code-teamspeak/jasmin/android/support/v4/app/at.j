.bytecode 50.0
.class public final synchronized android/support/v4/app/at
.super java/lang/Object

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a(Landroid/os/Bundle;Ljava/lang/String;)Landroid/os/IBinder;
getstatic android/os/Build$VERSION/SDK_INT I
bipush 18
if_icmplt L0
aload 0
aload 1
invokevirtual android/os/Bundle/getBinder(Ljava/lang/String;)Landroid/os/IBinder;
areturn
L0:
aload 0
aload 1
invokestatic android/support/v4/app/au/a(Landroid/os/Bundle;Ljava/lang/String;)Landroid/os/IBinder;
areturn
.limit locals 2
.limit stack 2
.end method

.method private static a(Landroid/os/Bundle;Ljava/lang/String;Landroid/os/IBinder;)V
.catch java/lang/NoSuchMethodException from L0 to L1 using L2
.catch java/lang/reflect/InvocationTargetException from L3 to L4 using L5
.catch java/lang/IllegalAccessException from L3 to L4 using L6
.catch java/lang/IllegalArgumentException from L3 to L4 using L7
getstatic android/os/Build$VERSION/SDK_INT I
bipush 18
if_icmplt L8
aload 0
aload 1
aload 2
invokevirtual android/os/Bundle/putBinder(Ljava/lang/String;Landroid/os/IBinder;)V
L9:
return
L8:
getstatic android/support/v4/app/au/b Z
ifne L10
L0:
ldc android/os/Bundle
ldc "putIBinder"
iconst_2
anewarray java/lang/Class
dup
iconst_0
ldc java/lang/String
aastore
dup
iconst_1
ldc android/os/IBinder
aastore
invokevirtual java/lang/Class/getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
astore 3
aload 3
putstatic android/support/v4/app/au/a Ljava/lang/reflect/Method;
aload 3
iconst_1
invokevirtual java/lang/reflect/Method/setAccessible(Z)V
L1:
iconst_1
putstatic android/support/v4/app/au/b Z
L10:
getstatic android/support/v4/app/au/a Ljava/lang/reflect/Method;
ifnull L9
L3:
getstatic android/support/v4/app/au/a Ljava/lang/reflect/Method;
aload 0
iconst_2
anewarray java/lang/Object
dup
iconst_0
aload 1
aastore
dup
iconst_1
aload 2
aastore
invokevirtual java/lang/reflect/Method/invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
pop
L4:
return
L5:
astore 0
L11:
ldc "BundleCompatDonut"
ldc "Failed to invoke putIBinder via reflection"
aload 0
invokestatic android/util/Log/i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
aconst_null
putstatic android/support/v4/app/au/a Ljava/lang/reflect/Method;
return
L2:
astore 3
ldc "BundleCompatDonut"
ldc "Failed to retrieve putIBinder method"
aload 3
invokestatic android/util/Log/i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
goto L1
L7:
astore 0
goto L11
L6:
astore 0
goto L11
.limit locals 4
.limit stack 6
.end method
