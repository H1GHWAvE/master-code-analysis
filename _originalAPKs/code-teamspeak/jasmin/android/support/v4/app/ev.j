.bytecode 50.0
.class final synchronized android/support/v4/app/ev
.super java/lang/Object

.method <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a(Landroid/app/Notification;)Landroid/os/Bundle;
aload 0
getfield android/app/Notification/extras Landroid/os/Bundle;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static a(Landroid/app/Notification;ILandroid/support/v4/app/el;Landroid/support/v4/app/fx;)Landroid/support/v4/app/ek;
aload 0
getfield android/app/Notification/actions [Landroid/app/Notification$Action;
iload 1
aaload
astore 5
aconst_null
astore 4
aload 0
getfield android/app/Notification/extras Landroid/os/Bundle;
ldc "android.support.actionExtras"
invokevirtual android/os/Bundle/getSparseParcelableArray(Ljava/lang/String;)Landroid/util/SparseArray;
astore 6
aload 4
astore 0
aload 6
ifnull L0
aload 6
iload 1
invokevirtual android/util/SparseArray/get(I)Ljava/lang/Object;
checkcast android/os/Bundle
astore 0
L0:
aload 2
aload 3
aload 5
getfield android/app/Notification$Action/icon I
aload 5
getfield android/app/Notification$Action/title Ljava/lang/CharSequence;
aload 5
getfield android/app/Notification$Action/actionIntent Landroid/app/PendingIntent;
aload 0
invokestatic android/support/v4/app/et/a(Landroid/support/v4/app/el;Landroid/support/v4/app/fx;ILjava/lang/CharSequence;Landroid/app/PendingIntent;Landroid/os/Bundle;)Landroid/support/v4/app/ek;
areturn
.limit locals 7
.limit stack 6
.end method

.method private static b(Landroid/app/Notification;)I
aload 0
getfield android/app/Notification/actions [Landroid/app/Notification$Action;
ifnull L0
aload 0
getfield android/app/Notification/actions [Landroid/app/Notification$Action;
arraylength
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method private static c(Landroid/app/Notification;)Z
aload 0
getfield android/app/Notification/extras Landroid/os/Bundle;
ldc "android.support.localOnly"
invokevirtual android/os/Bundle/getBoolean(Ljava/lang/String;)Z
ireturn
.limit locals 1
.limit stack 2
.end method

.method private static d(Landroid/app/Notification;)Ljava/lang/String;
aload 0
getfield android/app/Notification/extras Landroid/os/Bundle;
ldc "android.support.groupKey"
invokevirtual android/os/Bundle/getString(Ljava/lang/String;)Ljava/lang/String;
areturn
.limit locals 1
.limit stack 2
.end method

.method private static e(Landroid/app/Notification;)Z
aload 0
getfield android/app/Notification/extras Landroid/os/Bundle;
ldc "android.support.isGroupSummary"
invokevirtual android/os/Bundle/getBoolean(Ljava/lang/String;)Z
ireturn
.limit locals 1
.limit stack 2
.end method

.method private static f(Landroid/app/Notification;)Ljava/lang/String;
aload 0
getfield android/app/Notification/extras Landroid/os/Bundle;
ldc "android.support.sortKey"
invokevirtual android/os/Bundle/getString(Ljava/lang/String;)Ljava/lang/String;
areturn
.limit locals 1
.limit stack 2
.end method
