.bytecode 50.0
.class public final synchronized android/support/v4/app/m
.super android/support/v4/c/h

.method public <init>()V
aload 0
invokespecial android/support/v4/c/h/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method static a(Landroid/support/v4/app/gj;)Landroid/support/v4/app/r;
aconst_null
astore 1
aload 0
ifnull L0
new android/support/v4/app/p
dup
aload 0
invokespecial android/support/v4/app/p/<init>(Landroid/support/v4/app/gj;)V
astore 1
L0:
aload 1
areturn
.limit locals 2
.limit stack 3
.end method

.method private static a(Landroid/app/Activity;Landroid/content/Intent;ILandroid/os/Bundle;)V
.annotation invisibleparam 4 Landroid/support/a/z;
.end annotation
getstatic android/os/Build$VERSION/SDK_INT I
bipush 16
if_icmplt L0
aload 0
aload 1
iload 2
aload 3
invokevirtual android/app/Activity/startActivityForResult(Landroid/content/Intent;ILandroid/os/Bundle;)V
return
L0:
aload 0
aload 1
iload 2
invokevirtual android/app/Activity/startActivityForResult(Landroid/content/Intent;I)V
return
.limit locals 4
.limit stack 4
.end method

.method private static a(Landroid/app/Activity;Landroid/content/Intent;Landroid/os/Bundle;)V
.annotation invisibleparam 3 Landroid/support/a/z;
.end annotation
getstatic android/os/Build$VERSION/SDK_INT I
bipush 16
if_icmplt L0
aload 0
aload 1
aload 2
invokevirtual android/content/Context/startActivity(Landroid/content/Intent;Landroid/os/Bundle;)V
return
L0:
aload 0
aload 1
invokevirtual android/app/Activity/startActivity(Landroid/content/Intent;)V
return
.limit locals 3
.limit stack 3
.end method

.method private static a(Landroid/app/Activity;Landroid/support/v4/app/gj;)V
getstatic android/os/Build$VERSION/SDK_INT I
bipush 21
if_icmplt L0
aload 0
aload 1
invokestatic android/support/v4/app/m/a(Landroid/support/v4/app/gj;)Landroid/support/v4/app/r;
invokestatic android/support/v4/app/q/a(Landroid/support/v4/app/r;)Landroid/app/SharedElementCallback;
invokevirtual android/app/Activity/setEnterSharedElementCallback(Landroid/app/SharedElementCallback;)V
L0:
return
.limit locals 2
.limit stack 2
.end method

.method public static a(Landroid/app/Activity;[Ljava/lang/String;I)V
.annotation invisibleparam 1 Landroid/support/a/y;
.end annotation
.annotation invisibleparam 2 Landroid/support/a/y;
.end annotation
getstatic android/os/Build$VERSION/SDK_INT I
bipush 23
if_icmplt L0
aload 0
instanceof android/support/v4/app/v
ifeq L1
aload 0
checkcast android/support/v4/app/v
iload 2
invokeinterface android/support/v4/app/v/a(I)V 1
L1:
aload 0
aload 1
iload 2
invokevirtual android/app/Activity/requestPermissions([Ljava/lang/String;I)V
L2:
return
L0:
aload 0
instanceof android/support/v4/app/o
ifeq L2
new android/os/Handler
dup
invokestatic android/os/Looper/getMainLooper()Landroid/os/Looper;
invokespecial android/os/Handler/<init>(Landroid/os/Looper;)V
new android/support/v4/app/n
dup
aload 1
aload 0
iload 2
invokespecial android/support/v4/app/n/<init>([Ljava/lang/String;Landroid/app/Activity;I)V
invokevirtual android/os/Handler/post(Ljava/lang/Runnable;)Z
pop
return
.limit locals 3
.limit stack 6
.end method

.method private static a(Landroid/app/Activity;)Z
getstatic android/os/Build$VERSION/SDK_INT I
bipush 11
if_icmplt L0
aload 0
invokevirtual android/app/Activity/invalidateOptionsMenu()V
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method private static a(Landroid/app/Activity;Ljava/lang/String;)Z
.annotation invisibleparam 1 Landroid/support/a/y;
.end annotation
.annotation invisibleparam 2 Landroid/support/a/y;
.end annotation
getstatic android/os/Build$VERSION/SDK_INT I
bipush 23
if_icmplt L0
aload 0
aload 1
invokevirtual android/app/Activity/shouldShowRequestPermissionRationale(Ljava/lang/String;)Z
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method private static b(Landroid/app/Activity;)V
getstatic android/os/Build$VERSION/SDK_INT I
bipush 16
if_icmplt L0
aload 0
invokevirtual android/app/Activity/finishAffinity()V
return
L0:
aload 0
invokevirtual android/app/Activity/finish()V
return
.limit locals 1
.limit stack 2
.end method

.method private static b(Landroid/app/Activity;Landroid/support/v4/app/gj;)V
getstatic android/os/Build$VERSION/SDK_INT I
bipush 21
if_icmplt L0
aload 0
aload 1
invokestatic android/support/v4/app/m/a(Landroid/support/v4/app/gj;)Landroid/support/v4/app/r;
invokestatic android/support/v4/app/q/a(Landroid/support/v4/app/r;)Landroid/app/SharedElementCallback;
invokevirtual android/app/Activity/setExitSharedElementCallback(Landroid/app/SharedElementCallback;)V
L0:
return
.limit locals 2
.limit stack 2
.end method

.method private static c(Landroid/app/Activity;)V
getstatic android/os/Build$VERSION/SDK_INT I
bipush 21
if_icmplt L0
aload 0
invokevirtual android/app/Activity/finishAfterTransition()V
return
L0:
aload 0
invokevirtual android/app/Activity/finish()V
return
.limit locals 1
.limit stack 2
.end method

.method private static d(Landroid/app/Activity;)Landroid/net/Uri;
getstatic android/os/Build$VERSION/SDK_INT I
bipush 22
if_icmplt L0
aload 0
invokevirtual android/app/Activity/getReferrer()Landroid/net/Uri;
astore 0
L1:
aload 0
areturn
L0:
aload 0
invokevirtual android/app/Activity/getIntent()Landroid/content/Intent;
astore 2
aload 2
ldc "android.intent.extra.REFERRER"
invokevirtual android/content/Intent/getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;
checkcast android/net/Uri
astore 1
aload 1
astore 0
aload 1
ifnonnull L1
aload 2
ldc "android.intent.extra.REFERRER_NAME"
invokevirtual android/content/Intent/getStringExtra(Ljava/lang/String;)Ljava/lang/String;
astore 0
aload 0
ifnull L2
aload 0
invokestatic android/net/Uri/parse(Ljava/lang/String;)Landroid/net/Uri;
areturn
L2:
aconst_null
areturn
.limit locals 3
.limit stack 2
.end method

.method private static e(Landroid/app/Activity;)V
getstatic android/os/Build$VERSION/SDK_INT I
bipush 21
if_icmplt L0
aload 0
invokevirtual android/app/Activity/postponeEnterTransition()V
L0:
return
.limit locals 1
.limit stack 2
.end method

.method private static f(Landroid/app/Activity;)V
getstatic android/os/Build$VERSION/SDK_INT I
bipush 21
if_icmplt L0
aload 0
invokevirtual android/app/Activity/startPostponedEnterTransition()V
L0:
return
.limit locals 1
.limit stack 2
.end method
