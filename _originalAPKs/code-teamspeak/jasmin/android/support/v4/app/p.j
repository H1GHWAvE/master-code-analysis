.bytecode 50.0
.class final synchronized android/support/v4/app/p
.super android/support/v4/app/r

.field private 'a' Landroid/support/v4/app/gj;

.method public <init>(Landroid/support/v4/app/gj;)V
aload 0
invokespecial android/support/v4/app/r/<init>()V
aload 0
aload 1
putfield android/support/v4/app/p/a Landroid/support/v4/app/gj;
return
.limit locals 2
.limit stack 2
.end method

.method public final a(Landroid/view/View;Landroid/graphics/Matrix;Landroid/graphics/RectF;)Landroid/os/Parcelable;
aload 0
getfield android/support/v4/app/p/a Landroid/support/v4/app/gj;
astore 9
aload 1
instanceof android/widget/ImageView
ifeq L0
aload 1
checkcast android/widget/ImageView
astore 8
aload 8
invokevirtual android/widget/ImageView/getDrawable()Landroid/graphics/drawable/Drawable;
astore 7
aload 8
invokevirtual android/widget/ImageView/getBackground()Landroid/graphics/drawable/Drawable;
astore 10
aload 7
ifnull L0
aload 10
ifnonnull L0
aload 7
invokestatic android/support/v4/app/gj/a(Landroid/graphics/drawable/Drawable;)Landroid/graphics/Bitmap;
astore 10
aload 10
ifnull L0
new android/os/Bundle
dup
invokespecial android/os/Bundle/<init>()V
astore 7
aload 7
ldc "sharedElement:snapshot:bitmap"
aload 10
invokevirtual android/os/Bundle/putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V
aload 7
ldc "sharedElement:snapshot:imageScaleType"
aload 8
invokevirtual android/widget/ImageView/getScaleType()Landroid/widget/ImageView$ScaleType;
invokevirtual android/widget/ImageView$ScaleType/toString()Ljava/lang/String;
invokevirtual android/os/Bundle/putString(Ljava/lang/String;Ljava/lang/String;)V
aload 8
invokevirtual android/widget/ImageView/getScaleType()Landroid/widget/ImageView$ScaleType;
getstatic android/widget/ImageView$ScaleType/MATRIX Landroid/widget/ImageView$ScaleType;
if_acmpne L1
aload 8
invokevirtual android/widget/ImageView/getImageMatrix()Landroid/graphics/Matrix;
astore 1
bipush 9
newarray float
astore 2
aload 1
aload 2
invokevirtual android/graphics/Matrix/getValues([F)V
aload 7
ldc "sharedElement:snapshot:imageMatrix"
aload 2
invokevirtual android/os/Bundle/putFloatArray(Ljava/lang/String;[F)V
L1:
aload 7
areturn
L0:
aload 3
invokevirtual android/graphics/RectF/width()F
invokestatic java/lang/Math/round(F)I
istore 6
aload 3
invokevirtual android/graphics/RectF/height()F
invokestatic java/lang/Math/round(F)I
istore 5
aconst_null
astore 8
aload 8
astore 7
iload 6
ifle L1
aload 8
astore 7
iload 5
ifle L1
fconst_1
getstatic android/support/v4/app/gj/b I
i2f
iload 6
iload 5
imul
i2f
fdiv
invokestatic java/lang/Math/min(FF)F
fstore 4
iload 6
i2f
fload 4
fmul
f2i
istore 6
iload 5
i2f
fload 4
fmul
f2i
istore 5
aload 9
getfield android/support/v4/app/gj/a Landroid/graphics/Matrix;
ifnonnull L2
aload 9
new android/graphics/Matrix
dup
invokespecial android/graphics/Matrix/<init>()V
putfield android/support/v4/app/gj/a Landroid/graphics/Matrix;
L2:
aload 9
getfield android/support/v4/app/gj/a Landroid/graphics/Matrix;
aload 2
invokevirtual android/graphics/Matrix/set(Landroid/graphics/Matrix;)V
aload 9
getfield android/support/v4/app/gj/a Landroid/graphics/Matrix;
aload 3
getfield android/graphics/RectF/left F
fneg
aload 3
getfield android/graphics/RectF/top F
fneg
invokevirtual android/graphics/Matrix/postTranslate(FF)Z
pop
aload 9
getfield android/support/v4/app/gj/a Landroid/graphics/Matrix;
fload 4
fload 4
invokevirtual android/graphics/Matrix/postScale(FF)Z
pop
iload 6
iload 5
getstatic android/graphics/Bitmap$Config/ARGB_8888 Landroid/graphics/Bitmap$Config;
invokestatic android/graphics/Bitmap/createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
astore 2
new android/graphics/Canvas
dup
aload 2
invokespecial android/graphics/Canvas/<init>(Landroid/graphics/Bitmap;)V
astore 3
aload 3
aload 9
getfield android/support/v4/app/gj/a Landroid/graphics/Matrix;
invokevirtual android/graphics/Canvas/concat(Landroid/graphics/Matrix;)V
aload 1
aload 3
invokevirtual android/view/View/draw(Landroid/graphics/Canvas;)V
aload 2
areturn
.limit locals 11
.limit stack 4
.end method

.method public final a(Landroid/content/Context;Landroid/os/Parcelable;)Landroid/view/View;
aload 1
aload 2
invokestatic android/support/v4/app/gj/a(Landroid/content/Context;Landroid/os/Parcelable;)Landroid/view/View;
areturn
.limit locals 3
.limit stack 2
.end method

.method public final a()V
return
.limit locals 1
.limit stack 0
.end method

.method public final b()V
return
.limit locals 1
.limit stack 0
.end method

.method public final c()V
return
.limit locals 1
.limit stack 0
.end method

.method public final d()V
return
.limit locals 1
.limit stack 0
.end method
