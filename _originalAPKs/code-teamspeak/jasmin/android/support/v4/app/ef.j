.bytecode 50.0
.class final synchronized android/support/v4/app/ef
.super java/lang/Object

.method <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method static a(Landroid/support/v4/app/ek;)Landroid/app/Notification$Action;
new android/app/Notification$Action$Builder
dup
aload 0
invokevirtual android/support/v4/app/ek/a()I
aload 0
invokevirtual android/support/v4/app/ek/b()Ljava/lang/CharSequence;
aload 0
invokevirtual android/support/v4/app/ek/c()Landroid/app/PendingIntent;
invokespecial android/app/Notification$Action$Builder/<init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)V
aload 0
invokevirtual android/support/v4/app/ek/d()Landroid/os/Bundle;
invokevirtual android/app/Notification$Action$Builder/addExtras(Landroid/os/Bundle;)Landroid/app/Notification$Action$Builder;
astore 3
aload 0
invokevirtual android/support/v4/app/ek/e()[Landroid/support/v4/app/fw;
astore 0
aload 0
ifnull L0
aload 0
invokestatic android/support/v4/app/fu/a([Landroid/support/v4/app/fw;)[Landroid/app/RemoteInput;
astore 0
aload 0
arraylength
istore 2
iconst_0
istore 1
L1:
iload 1
iload 2
if_icmpge L0
aload 3
aload 0
iload 1
aaload
invokevirtual android/app/Notification$Action$Builder/addRemoteInput(Landroid/app/RemoteInput;)Landroid/app/Notification$Action$Builder;
pop
iload 1
iconst_1
iadd
istore 1
goto L1
L0:
aload 3
invokevirtual android/app/Notification$Action$Builder/build()Landroid/app/Notification$Action;
areturn
.limit locals 4
.limit stack 5
.end method

.method static a(Landroid/app/Notification$Action;Landroid/support/v4/app/el;Landroid/support/v4/app/fx;)Landroid/support/v4/app/ek;
aload 0
invokevirtual android/app/Notification$Action/getRemoteInputs()[Landroid/app/RemoteInput;
astore 5
aload 5
ifnonnull L0
aconst_null
astore 2
L1:
aload 1
aload 0
getfield android/app/Notification$Action/icon I
aload 0
getfield android/app/Notification$Action/title Ljava/lang/CharSequence;
aload 0
getfield android/app/Notification$Action/actionIntent Landroid/app/PendingIntent;
aload 0
invokevirtual android/app/Notification$Action/getExtras()Landroid/os/Bundle;
aload 2
invokeinterface android/support/v4/app/el/a(ILjava/lang/CharSequence;Landroid/app/PendingIntent;Landroid/os/Bundle;[Landroid/support/v4/app/fw;)Landroid/support/v4/app/ek; 5
areturn
L0:
aload 2
aload 5
arraylength
invokeinterface android/support/v4/app/fx/a(I)[Landroid/support/v4/app/fw; 1
astore 4
iconst_0
istore 3
L2:
iload 3
aload 5
arraylength
if_icmpge L3
aload 5
iload 3
aaload
astore 6
aload 4
iload 3
aload 2
aload 6
invokevirtual android/app/RemoteInput/getResultKey()Ljava/lang/String;
aload 6
invokevirtual android/app/RemoteInput/getLabel()Ljava/lang/CharSequence;
aload 6
invokevirtual android/app/RemoteInput/getChoices()[Ljava/lang/CharSequence;
aload 6
invokevirtual android/app/RemoteInput/getAllowFreeFormInput()Z
aload 6
invokevirtual android/app/RemoteInput/getExtras()Landroid/os/Bundle;
invokeinterface android/support/v4/app/fx/a(Ljava/lang/String;Ljava/lang/CharSequence;[Ljava/lang/CharSequence;ZLandroid/os/Bundle;)Landroid/support/v4/app/fw; 5
aastore
iload 3
iconst_1
iadd
istore 3
goto L2
L3:
aload 4
astore 2
goto L1
.limit locals 7
.limit stack 8
.end method

.method private static a(Landroid/app/Notification;ILandroid/support/v4/app/el;Landroid/support/v4/app/fx;)Landroid/support/v4/app/ek;
aload 0
getfield android/app/Notification/actions [Landroid/app/Notification$Action;
iload 1
aaload
aload 2
aload 3
invokestatic android/support/v4/app/ef/a(Landroid/app/Notification$Action;Landroid/support/v4/app/el;Landroid/support/v4/app/fx;)Landroid/support/v4/app/ek;
areturn
.limit locals 4
.limit stack 3
.end method

.method private static a([Landroid/support/v4/app/ek;)Ljava/util/ArrayList;
aload 0
ifnonnull L0
aconst_null
astore 3
L1:
aload 3
areturn
L0:
new java/util/ArrayList
dup
aload 0
arraylength
invokespecial java/util/ArrayList/<init>(I)V
astore 4
aload 0
arraylength
istore 2
iconst_0
istore 1
L2:
aload 4
astore 3
iload 1
iload 2
if_icmpge L1
aload 4
aload 0
iload 1
aaload
invokestatic android/support/v4/app/ef/a(Landroid/support/v4/app/ek;)Landroid/app/Notification$Action;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
iload 1
iconst_1
iadd
istore 1
goto L2
.limit locals 5
.limit stack 3
.end method

.method public static a(Landroid/app/Notification$Builder;Landroid/support/v4/app/ek;)V
new android/app/Notification$Action$Builder
dup
aload 1
invokevirtual android/support/v4/app/ek/a()I
aload 1
invokevirtual android/support/v4/app/ek/b()Ljava/lang/CharSequence;
aload 1
invokevirtual android/support/v4/app/ek/c()Landroid/app/PendingIntent;
invokespecial android/app/Notification$Action$Builder/<init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)V
astore 4
aload 1
invokevirtual android/support/v4/app/ek/e()[Landroid/support/v4/app/fw;
ifnull L0
aload 1
invokevirtual android/support/v4/app/ek/e()[Landroid/support/v4/app/fw;
invokestatic android/support/v4/app/fu/a([Landroid/support/v4/app/fw;)[Landroid/app/RemoteInput;
astore 5
aload 5
arraylength
istore 3
iconst_0
istore 2
L1:
iload 2
iload 3
if_icmpge L0
aload 4
aload 5
iload 2
aaload
invokevirtual android/app/Notification$Action$Builder/addRemoteInput(Landroid/app/RemoteInput;)Landroid/app/Notification$Action$Builder;
pop
iload 2
iconst_1
iadd
istore 2
goto L1
L0:
aload 1
invokevirtual android/support/v4/app/ek/d()Landroid/os/Bundle;
ifnull L2
aload 4
aload 1
invokevirtual android/support/v4/app/ek/d()Landroid/os/Bundle;
invokevirtual android/app/Notification$Action$Builder/addExtras(Landroid/os/Bundle;)Landroid/app/Notification$Action$Builder;
pop
L2:
aload 0
aload 4
invokevirtual android/app/Notification$Action$Builder/build()Landroid/app/Notification$Action;
invokevirtual android/app/Notification$Builder/addAction(Landroid/app/Notification$Action;)Landroid/app/Notification$Builder;
pop
return
.limit locals 6
.limit stack 5
.end method

.method private static a(Landroid/app/Notification;)Z
aload 0
getfield android/app/Notification/flags I
sipush 256
iand
ifeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method private static a(Ljava/util/ArrayList;Landroid/support/v4/app/el;Landroid/support/v4/app/fx;)[Landroid/support/v4/app/ek;
aload 0
ifnonnull L0
aconst_null
areturn
L0:
aload 1
aload 0
invokevirtual java/util/ArrayList/size()I
invokeinterface android/support/v4/app/el/a(I)[Landroid/support/v4/app/ek; 1
astore 4
iconst_0
istore 3
L1:
iload 3
aload 4
arraylength
if_icmpge L2
aload 4
iload 3
aload 0
iload 3
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/app/Notification$Action
aload 1
aload 2
invokestatic android/support/v4/app/ef/a(Landroid/app/Notification$Action;Landroid/support/v4/app/el;Landroid/support/v4/app/fx;)Landroid/support/v4/app/ek;
aastore
iload 3
iconst_1
iadd
istore 3
goto L1
L2:
aload 4
areturn
.limit locals 5
.limit stack 5
.end method

.method private static b(Landroid/app/Notification;)Ljava/lang/String;
aload 0
invokevirtual android/app/Notification/getGroup()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static c(Landroid/app/Notification;)Z
aload 0
getfield android/app/Notification/flags I
sipush 512
iand
ifeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method private static d(Landroid/app/Notification;)Ljava/lang/String;
aload 0
invokevirtual android/app/Notification/getSortKey()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method
