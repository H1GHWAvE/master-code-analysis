.bytecode 50.0
.class final synchronized android/support/v4/app/cu
.super java/lang/Object
.implements android/support/v4/c/ac
.implements android/support/v4/c/ad

.field final 'a' I

.field final 'b' Landroid/os/Bundle;

.field 'c' Landroid/support/v4/app/cs;

.field 'd' Landroid/support/v4/c/aa;

.field 'e' Z

.field 'f' Z

.field 'g' Ljava/lang/Object;

.field 'h' Z

.field 'i' Z

.field 'j' Z

.field 'k' Z

.field 'l' Z

.field 'm' Z

.field 'n' Landroid/support/v4/app/cu;

.field final synthetic 'o' Landroid/support/v4/app/ct;

.method public <init>(Landroid/support/v4/app/ct;ILandroid/os/Bundle;Landroid/support/v4/app/cs;)V
aload 0
aload 1
putfield android/support/v4/app/cu/o Landroid/support/v4/app/ct;
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
iload 2
putfield android/support/v4/app/cu/a I
aload 0
aload 3
putfield android/support/v4/app/cu/b Landroid/os/Bundle;
aload 0
aload 4
putfield android/support/v4/app/cu/c Landroid/support/v4/app/cs;
return
.limit locals 5
.limit stack 2
.end method

.method private e()V
getstatic android/support/v4/app/ct/b Z
ifeq L0
ldc "LoaderManager"
new java/lang/StringBuilder
dup
ldc "  Retaining: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/v(Ljava/lang/String;Ljava/lang/String;)I
pop
L0:
aload 0
iconst_1
putfield android/support/v4/app/cu/i Z
aload 0
aload 0
getfield android/support/v4/app/cu/h Z
putfield android/support/v4/app/cu/j Z
aload 0
iconst_0
putfield android/support/v4/app/cu/h Z
aload 0
aconst_null
putfield android/support/v4/app/cu/c Landroid/support/v4/app/cs;
return
.limit locals 1
.limit stack 4
.end method

.method private f()V
aload 0
getfield android/support/v4/app/cu/i Z
ifeq L0
getstatic android/support/v4/app/ct/b Z
ifeq L1
ldc "LoaderManager"
new java/lang/StringBuilder
dup
ldc "  Finished Retaining: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/v(Ljava/lang/String;Ljava/lang/String;)I
pop
L1:
aload 0
iconst_0
putfield android/support/v4/app/cu/i Z
aload 0
getfield android/support/v4/app/cu/h Z
aload 0
getfield android/support/v4/app/cu/j Z
if_icmpeq L0
aload 0
getfield android/support/v4/app/cu/h Z
ifne L0
aload 0
invokevirtual android/support/v4/app/cu/b()V
L0:
aload 0
getfield android/support/v4/app/cu/h Z
ifeq L2
aload 0
getfield android/support/v4/app/cu/e Z
ifeq L2
aload 0
getfield android/support/v4/app/cu/k Z
ifne L2
aload 0
aload 0
getfield android/support/v4/app/cu/d Landroid/support/v4/c/aa;
aload 0
getfield android/support/v4/app/cu/g Ljava/lang/Object;
invokevirtual android/support/v4/app/cu/b(Landroid/support/v4/c/aa;Ljava/lang/Object;)V
L2:
return
.limit locals 1
.limit stack 4
.end method

.method private g()V
aload 0
getfield android/support/v4/app/cu/h Z
ifeq L0
aload 0
getfield android/support/v4/app/cu/k Z
ifeq L0
aload 0
iconst_0
putfield android/support/v4/app/cu/k Z
aload 0
getfield android/support/v4/app/cu/e Z
ifeq L0
aload 0
aload 0
getfield android/support/v4/app/cu/d Landroid/support/v4/c/aa;
aload 0
getfield android/support/v4/app/cu/g Ljava/lang/Object;
invokevirtual android/support/v4/app/cu/b(Landroid/support/v4/c/aa;Ljava/lang/Object;)V
L0:
return
.limit locals 1
.limit stack 3
.end method

.method private h()V
getstatic android/support/v4/app/ct/b Z
ifeq L0
ldc "LoaderManager"
new java/lang/StringBuilder
dup
ldc "  Canceling: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/v(Ljava/lang/String;Ljava/lang/String;)I
pop
L0:
aload 0
getfield android/support/v4/app/cu/h Z
ifeq L1
aload 0
getfield android/support/v4/app/cu/d Landroid/support/v4/c/aa;
ifnull L1
aload 0
getfield android/support/v4/app/cu/m Z
ifeq L1
aload 0
getfield android/support/v4/app/cu/d Landroid/support/v4/c/aa;
invokevirtual android/support/v4/c/aa/j()Z
ifne L1
aload 0
invokevirtual android/support/v4/app/cu/d()V
L1:
return
.limit locals 1
.limit stack 4
.end method

.method final a()V
aload 0
getfield android/support/v4/app/cu/i Z
ifeq L0
aload 0
getfield android/support/v4/app/cu/j Z
ifeq L0
aload 0
iconst_1
putfield android/support/v4/app/cu/h Z
L1:
return
L0:
aload 0
getfield android/support/v4/app/cu/h Z
ifne L1
aload 0
iconst_1
putfield android/support/v4/app/cu/h Z
getstatic android/support/v4/app/ct/b Z
ifeq L2
ldc "LoaderManager"
new java/lang/StringBuilder
dup
ldc "  Starting: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/v(Ljava/lang/String;Ljava/lang/String;)I
pop
L2:
aload 0
getfield android/support/v4/app/cu/d Landroid/support/v4/c/aa;
ifnonnull L3
aload 0
getfield android/support/v4/app/cu/c Landroid/support/v4/app/cs;
ifnull L3
aload 0
aload 0
getfield android/support/v4/app/cu/c Landroid/support/v4/app/cs;
invokeinterface android/support/v4/app/cs/a()Landroid/support/v4/c/aa; 0
putfield android/support/v4/app/cu/d Landroid/support/v4/c/aa;
L3:
aload 0
getfield android/support/v4/app/cu/d Landroid/support/v4/c/aa;
ifnull L1
aload 0
getfield android/support/v4/app/cu/d Landroid/support/v4/c/aa;
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
invokevirtual java/lang/Class/isMemberClass()Z
ifeq L4
aload 0
getfield android/support/v4/app/cu/d Landroid/support/v4/c/aa;
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
invokevirtual java/lang/Class/getModifiers()I
invokestatic java/lang/reflect/Modifier/isStatic(I)Z
ifne L4
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
ldc "Object returned from onCreateLoader must not be a non-static inner member class: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
getfield android/support/v4/app/cu/d Landroid/support/v4/c/aa;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L4:
aload 0
getfield android/support/v4/app/cu/m Z
ifne L5
aload 0
getfield android/support/v4/app/cu/d Landroid/support/v4/c/aa;
astore 2
aload 0
getfield android/support/v4/app/cu/a I
istore 1
aload 2
getfield android/support/v4/c/aa/q Landroid/support/v4/c/ad;
ifnull L6
new java/lang/IllegalStateException
dup
ldc "There is already a listener registered"
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L6:
aload 2
aload 0
putfield android/support/v4/c/aa/q Landroid/support/v4/c/ad;
aload 2
iload 1
putfield android/support/v4/c/aa/p I
aload 0
getfield android/support/v4/app/cu/d Landroid/support/v4/c/aa;
astore 2
aload 2
getfield android/support/v4/c/aa/r Landroid/support/v4/c/ac;
ifnull L7
new java/lang/IllegalStateException
dup
ldc "There is already a listener registered"
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L7:
aload 2
aload 0
putfield android/support/v4/c/aa/r Landroid/support/v4/c/ac;
aload 0
iconst_1
putfield android/support/v4/app/cu/m Z
L5:
aload 0
getfield android/support/v4/app/cu/d Landroid/support/v4/c/aa;
invokevirtual android/support/v4/c/aa/i()V
return
.limit locals 3
.limit stack 5
.end method

.method public final a(Landroid/support/v4/c/aa;Ljava/lang/Object;)V
getstatic android/support/v4/app/ct/b Z
ifeq L0
ldc "LoaderManager"
new java/lang/StringBuilder
dup
ldc "onLoadComplete: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/v(Ljava/lang/String;Ljava/lang/String;)I
pop
L0:
aload 0
getfield android/support/v4/app/cu/l Z
ifeq L1
getstatic android/support/v4/app/ct/b Z
ifeq L2
ldc "LoaderManager"
ldc "  Ignoring load complete -- destroyed"
invokestatic android/util/Log/v(Ljava/lang/String;Ljava/lang/String;)I
pop
L2:
return
L1:
aload 0
getfield android/support/v4/app/cu/o Landroid/support/v4/app/ct;
getfield android/support/v4/app/ct/c Landroid/support/v4/n/w;
aload 0
getfield android/support/v4/app/cu/a I
invokevirtual android/support/v4/n/w/a(I)Ljava/lang/Object;
aload 0
if_acmpeq L3
getstatic android/support/v4/app/ct/b Z
ifeq L2
ldc "LoaderManager"
ldc "  Ignoring load complete -- not active"
invokestatic android/util/Log/v(Ljava/lang/String;Ljava/lang/String;)I
pop
return
L3:
aload 0
getfield android/support/v4/app/cu/n Landroid/support/v4/app/cu;
astore 4
aload 4
ifnull L4
getstatic android/support/v4/app/ct/b Z
ifeq L5
ldc "LoaderManager"
new java/lang/StringBuilder
dup
ldc "  Switching to pending loader: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/v(Ljava/lang/String;Ljava/lang/String;)I
pop
L5:
aload 0
aconst_null
putfield android/support/v4/app/cu/n Landroid/support/v4/app/cu;
aload 0
getfield android/support/v4/app/cu/o Landroid/support/v4/app/ct;
getfield android/support/v4/app/ct/c Landroid/support/v4/n/w;
aload 0
getfield android/support/v4/app/cu/a I
aconst_null
invokevirtual android/support/v4/n/w/a(ILjava/lang/Object;)V
aload 0
invokevirtual android/support/v4/app/cu/c()V
aload 0
getfield android/support/v4/app/cu/o Landroid/support/v4/app/ct;
aload 4
invokevirtual android/support/v4/app/ct/a(Landroid/support/v4/app/cu;)V
return
L4:
aload 0
getfield android/support/v4/app/cu/g Ljava/lang/Object;
aload 2
if_acmpne L6
aload 0
getfield android/support/v4/app/cu/e Z
ifne L7
L6:
aload 0
aload 2
putfield android/support/v4/app/cu/g Ljava/lang/Object;
aload 0
iconst_1
putfield android/support/v4/app/cu/e Z
aload 0
getfield android/support/v4/app/cu/h Z
ifeq L7
aload 0
aload 1
aload 2
invokevirtual android/support/v4/app/cu/b(Landroid/support/v4/c/aa;Ljava/lang/Object;)V
L7:
aload 0
getfield android/support/v4/app/cu/o Landroid/support/v4/app/ct;
getfield android/support/v4/app/ct/d Landroid/support/v4/n/w;
aload 0
getfield android/support/v4/app/cu/a I
invokevirtual android/support/v4/n/w/a(I)Ljava/lang/Object;
checkcast android/support/v4/app/cu
astore 1
aload 1
ifnull L8
aload 1
aload 0
if_acmpeq L8
aload 1
iconst_0
putfield android/support/v4/app/cu/f Z
aload 1
invokevirtual android/support/v4/app/cu/c()V
aload 0
getfield android/support/v4/app/cu/o Landroid/support/v4/app/ct;
getfield android/support/v4/app/ct/d Landroid/support/v4/n/w;
astore 1
aload 0
getfield android/support/v4/app/cu/a I
istore 3
aload 1
getfield android/support/v4/n/w/c [I
aload 1
getfield android/support/v4/n/w/e I
iload 3
invokestatic android/support/v4/n/f/a([III)I
istore 3
iload 3
iflt L8
aload 1
getfield android/support/v4/n/w/d [Ljava/lang/Object;
iload 3
aaload
getstatic android/support/v4/n/w/a Ljava/lang/Object;
if_acmpeq L8
aload 1
getfield android/support/v4/n/w/d [Ljava/lang/Object;
iload 3
getstatic android/support/v4/n/w/a Ljava/lang/Object;
aastore
aload 1
iconst_1
putfield android/support/v4/n/w/b Z
L8:
aload 0
getfield android/support/v4/app/cu/o Landroid/support/v4/app/ct;
invokestatic android/support/v4/app/ct/a(Landroid/support/v4/app/ct;)Landroid/support/v4/app/bh;
ifnull L2
aload 0
getfield android/support/v4/app/cu/o Landroid/support/v4/app/ct;
invokevirtual android/support/v4/app/ct/a()Z
ifne L2
aload 0
getfield android/support/v4/app/cu/o Landroid/support/v4/app/ct;
invokestatic android/support/v4/app/ct/a(Landroid/support/v4/app/ct;)Landroid/support/v4/app/bh;
getfield android/support/v4/app/bh/f Landroid/support/v4/app/bl;
invokevirtual android/support/v4/app/bl/j()V
return
.limit locals 5
.limit stack 4
.end method

.method public final a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
aload 1
astore 5
aload 0
astore 1
L0:
aload 3
aload 5
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
ldc "mId="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 1
getfield android/support/v4/app/cu/a I
invokevirtual java/io/PrintWriter/print(I)V
aload 3
ldc " mArgs="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 1
getfield android/support/v4/app/cu/b Landroid/os/Bundle;
invokevirtual java/io/PrintWriter/println(Ljava/lang/Object;)V
aload 3
aload 5
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
ldc "mCallbacks="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 1
getfield android/support/v4/app/cu/c Landroid/support/v4/app/cs;
invokevirtual java/io/PrintWriter/println(Ljava/lang/Object;)V
aload 3
aload 5
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
ldc "mLoader="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 1
getfield android/support/v4/app/cu/d Landroid/support/v4/c/aa;
invokevirtual java/io/PrintWriter/println(Ljava/lang/Object;)V
aload 1
getfield android/support/v4/app/cu/d Landroid/support/v4/c/aa;
ifnull L1
aload 1
getfield android/support/v4/app/cu/d Landroid/support/v4/c/aa;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 5
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "  "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aload 2
aload 3
aload 4
invokevirtual android/support/v4/c/aa/a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
L1:
aload 1
getfield android/support/v4/app/cu/e Z
ifne L2
aload 1
getfield android/support/v4/app/cu/f Z
ifeq L3
L2:
aload 3
aload 5
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
ldc "mHaveData="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 1
getfield android/support/v4/app/cu/e Z
invokevirtual java/io/PrintWriter/print(Z)V
aload 3
ldc "  mDeliveredData="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 1
getfield android/support/v4/app/cu/f Z
invokevirtual java/io/PrintWriter/println(Z)V
aload 3
aload 5
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
ldc "mData="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 1
getfield android/support/v4/app/cu/g Ljava/lang/Object;
invokevirtual java/io/PrintWriter/println(Ljava/lang/Object;)V
L3:
aload 3
aload 5
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
ldc "mStarted="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 1
getfield android/support/v4/app/cu/h Z
invokevirtual java/io/PrintWriter/print(Z)V
aload 3
ldc " mReportNextStart="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 1
getfield android/support/v4/app/cu/k Z
invokevirtual java/io/PrintWriter/print(Z)V
aload 3
ldc " mDestroyed="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 1
getfield android/support/v4/app/cu/l Z
invokevirtual java/io/PrintWriter/println(Z)V
aload 3
aload 5
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
ldc "mRetaining="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 1
getfield android/support/v4/app/cu/i Z
invokevirtual java/io/PrintWriter/print(Z)V
aload 3
ldc " mRetainingStarted="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 1
getfield android/support/v4/app/cu/j Z
invokevirtual java/io/PrintWriter/print(Z)V
aload 3
ldc " mListenerRegistered="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 1
getfield android/support/v4/app/cu/m Z
invokevirtual java/io/PrintWriter/println(Z)V
aload 1
getfield android/support/v4/app/cu/n Landroid/support/v4/app/cu;
ifnull L4
aload 3
aload 5
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
ldc "Pending Loader "
invokevirtual java/io/PrintWriter/println(Ljava/lang/String;)V
aload 3
aload 1
getfield android/support/v4/app/cu/n Landroid/support/v4/app/cu;
invokevirtual java/io/PrintWriter/print(Ljava/lang/Object;)V
aload 3
ldc ":"
invokevirtual java/io/PrintWriter/println(Ljava/lang/String;)V
aload 1
getfield android/support/v4/app/cu/n Landroid/support/v4/app/cu;
astore 1
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 5
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "  "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 5
goto L0
L4:
return
.limit locals 6
.limit stack 5
.end method

.method final b()V
getstatic android/support/v4/app/ct/b Z
ifeq L0
ldc "LoaderManager"
new java/lang/StringBuilder
dup
ldc "  Stopping: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/v(Ljava/lang/String;Ljava/lang/String;)I
pop
L0:
aload 0
iconst_0
putfield android/support/v4/app/cu/h Z
aload 0
getfield android/support/v4/app/cu/i Z
ifne L1
aload 0
getfield android/support/v4/app/cu/d Landroid/support/v4/c/aa;
ifnull L1
aload 0
getfield android/support/v4/app/cu/m Z
ifeq L1
aload 0
iconst_0
putfield android/support/v4/app/cu/m Z
aload 0
getfield android/support/v4/app/cu/d Landroid/support/v4/c/aa;
aload 0
invokevirtual android/support/v4/c/aa/a(Landroid/support/v4/c/ad;)V
aload 0
getfield android/support/v4/app/cu/d Landroid/support/v4/c/aa;
aload 0
invokevirtual android/support/v4/c/aa/a(Landroid/support/v4/c/ac;)V
aload 0
getfield android/support/v4/app/cu/d Landroid/support/v4/c/aa;
invokevirtual android/support/v4/c/aa/l()V
L1:
return
.limit locals 1
.limit stack 4
.end method

.method final b(Landroid/support/v4/c/aa;Ljava/lang/Object;)V
.catch all from L0 to L1 using L2
aload 0
getfield android/support/v4/app/cu/c Landroid/support/v4/app/cs;
ifnull L3
aload 0
getfield android/support/v4/app/cu/o Landroid/support/v4/app/ct;
invokestatic android/support/v4/app/ct/a(Landroid/support/v4/app/ct;)Landroid/support/v4/app/bh;
ifnull L4
aload 0
getfield android/support/v4/app/cu/o Landroid/support/v4/app/ct;
invokestatic android/support/v4/app/ct/a(Landroid/support/v4/app/ct;)Landroid/support/v4/app/bh;
getfield android/support/v4/app/bh/f Landroid/support/v4/app/bl;
getfield android/support/v4/app/bl/B Ljava/lang/String;
astore 3
aload 0
getfield android/support/v4/app/cu/o Landroid/support/v4/app/ct;
invokestatic android/support/v4/app/ct/a(Landroid/support/v4/app/ct;)Landroid/support/v4/app/bh;
getfield android/support/v4/app/bh/f Landroid/support/v4/app/bl;
ldc "onLoadFinished"
putfield android/support/v4/app/bl/B Ljava/lang/String;
L0:
getstatic android/support/v4/app/ct/b Z
ifeq L1
new java/lang/StringBuilder
dup
ldc "  onLoadFinished in "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc ": "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
astore 1
new java/lang/StringBuilder
dup
bipush 64
invokespecial java/lang/StringBuilder/<init>(I)V
astore 4
aload 2
aload 4
invokestatic android/support/v4/n/g/a(Ljava/lang/Object;Ljava/lang/StringBuilder;)V
aload 4
ldc "}"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
ldc "LoaderManager"
aload 1
aload 4
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/v(Ljava/lang/String;Ljava/lang/String;)I
pop
L1:
aload 0
getfield android/support/v4/app/cu/o Landroid/support/v4/app/ct;
invokestatic android/support/v4/app/ct/a(Landroid/support/v4/app/ct;)Landroid/support/v4/app/bh;
ifnull L5
aload 0
getfield android/support/v4/app/cu/o Landroid/support/v4/app/ct;
invokestatic android/support/v4/app/ct/a(Landroid/support/v4/app/ct;)Landroid/support/v4/app/bh;
getfield android/support/v4/app/bh/f Landroid/support/v4/app/bl;
aload 3
putfield android/support/v4/app/bl/B Ljava/lang/String;
L5:
aload 0
iconst_1
putfield android/support/v4/app/cu/f Z
L3:
return
L2:
astore 1
aload 0
getfield android/support/v4/app/cu/o Landroid/support/v4/app/ct;
invokestatic android/support/v4/app/ct/a(Landroid/support/v4/app/ct;)Landroid/support/v4/app/bh;
ifnull L6
aload 0
getfield android/support/v4/app/cu/o Landroid/support/v4/app/ct;
invokestatic android/support/v4/app/ct/a(Landroid/support/v4/app/ct;)Landroid/support/v4/app/bh;
getfield android/support/v4/app/bh/f Landroid/support/v4/app/bl;
aload 3
putfield android/support/v4/app/bl/B Ljava/lang/String;
L6:
aload 1
athrow
L4:
aconst_null
astore 3
goto L0
.limit locals 5
.limit stack 3
.end method

.method final c()V
aload 0
astore 2
L0:
getstatic android/support/v4/app/ct/b Z
ifeq L1
ldc "LoaderManager"
new java/lang/StringBuilder
dup
ldc "  Destroying: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/v(Ljava/lang/String;Ljava/lang/String;)I
pop
L1:
aload 2
iconst_1
putfield android/support/v4/app/cu/l Z
aload 2
getfield android/support/v4/app/cu/f Z
istore 1
aload 2
iconst_0
putfield android/support/v4/app/cu/f Z
aload 2
getfield android/support/v4/app/cu/c Landroid/support/v4/app/cs;
ifnull L2
aload 2
getfield android/support/v4/app/cu/d Landroid/support/v4/c/aa;
ifnull L2
aload 2
getfield android/support/v4/app/cu/e Z
ifeq L2
iload 1
ifeq L2
getstatic android/support/v4/app/ct/b Z
ifeq L3
ldc "LoaderManager"
new java/lang/StringBuilder
dup
ldc "  Reseting: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/v(Ljava/lang/String;Ljava/lang/String;)I
pop
L3:
aload 2
getfield android/support/v4/app/cu/o Landroid/support/v4/app/ct;
invokestatic android/support/v4/app/ct/a(Landroid/support/v4/app/ct;)Landroid/support/v4/app/bh;
ifnull L4
aload 2
getfield android/support/v4/app/cu/o Landroid/support/v4/app/ct;
invokestatic android/support/v4/app/ct/a(Landroid/support/v4/app/ct;)Landroid/support/v4/app/bh;
getfield android/support/v4/app/bh/f Landroid/support/v4/app/bl;
getfield android/support/v4/app/bl/B Ljava/lang/String;
astore 3
aload 2
getfield android/support/v4/app/cu/o Landroid/support/v4/app/ct;
invokestatic android/support/v4/app/ct/a(Landroid/support/v4/app/ct;)Landroid/support/v4/app/bh;
getfield android/support/v4/app/bh/f Landroid/support/v4/app/bl;
ldc "onLoaderReset"
putfield android/support/v4/app/bl/B Ljava/lang/String;
L5:
aload 2
getfield android/support/v4/app/cu/o Landroid/support/v4/app/ct;
invokestatic android/support/v4/app/ct/a(Landroid/support/v4/app/ct;)Landroid/support/v4/app/bh;
ifnull L2
aload 2
getfield android/support/v4/app/cu/o Landroid/support/v4/app/ct;
invokestatic android/support/v4/app/ct/a(Landroid/support/v4/app/ct;)Landroid/support/v4/app/bh;
getfield android/support/v4/app/bh/f Landroid/support/v4/app/bl;
aload 3
putfield android/support/v4/app/bl/B Ljava/lang/String;
L2:
aload 2
aconst_null
putfield android/support/v4/app/cu/c Landroid/support/v4/app/cs;
aload 2
aconst_null
putfield android/support/v4/app/cu/g Ljava/lang/Object;
aload 2
iconst_0
putfield android/support/v4/app/cu/e Z
aload 2
getfield android/support/v4/app/cu/d Landroid/support/v4/c/aa;
ifnull L6
aload 2
getfield android/support/v4/app/cu/m Z
ifeq L7
aload 2
iconst_0
putfield android/support/v4/app/cu/m Z
aload 2
getfield android/support/v4/app/cu/d Landroid/support/v4/c/aa;
aload 2
invokevirtual android/support/v4/c/aa/a(Landroid/support/v4/c/ad;)V
aload 2
getfield android/support/v4/app/cu/d Landroid/support/v4/c/aa;
aload 2
invokevirtual android/support/v4/c/aa/a(Landroid/support/v4/c/ac;)V
L7:
aload 2
getfield android/support/v4/app/cu/d Landroid/support/v4/c/aa;
invokevirtual android/support/v4/c/aa/m()V
L6:
aload 2
getfield android/support/v4/app/cu/n Landroid/support/v4/app/cu;
ifnull L8
aload 2
getfield android/support/v4/app/cu/n Landroid/support/v4/app/cu;
astore 2
goto L0
L8:
return
L4:
aconst_null
astore 3
goto L5
.limit locals 4
.limit stack 4
.end method

.method public final d()V
getstatic android/support/v4/app/ct/b Z
ifeq L0
ldc "LoaderManager"
new java/lang/StringBuilder
dup
ldc "onLoadCanceled: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/v(Ljava/lang/String;Ljava/lang/String;)I
pop
L0:
aload 0
getfield android/support/v4/app/cu/l Z
ifeq L1
getstatic android/support/v4/app/ct/b Z
ifeq L2
ldc "LoaderManager"
ldc "  Ignoring load canceled -- destroyed"
invokestatic android/util/Log/v(Ljava/lang/String;Ljava/lang/String;)I
pop
L2:
return
L1:
aload 0
getfield android/support/v4/app/cu/o Landroid/support/v4/app/ct;
getfield android/support/v4/app/ct/c Landroid/support/v4/n/w;
aload 0
getfield android/support/v4/app/cu/a I
invokevirtual android/support/v4/n/w/a(I)Ljava/lang/Object;
aload 0
if_acmpeq L3
getstatic android/support/v4/app/ct/b Z
ifeq L2
ldc "LoaderManager"
ldc "  Ignoring load canceled -- not active"
invokestatic android/util/Log/v(Ljava/lang/String;Ljava/lang/String;)I
pop
return
L3:
aload 0
getfield android/support/v4/app/cu/n Landroid/support/v4/app/cu;
astore 1
aload 1
ifnull L2
getstatic android/support/v4/app/ct/b Z
ifeq L4
ldc "LoaderManager"
new java/lang/StringBuilder
dup
ldc "  Switching to pending loader: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/v(Ljava/lang/String;Ljava/lang/String;)I
pop
L4:
aload 0
aconst_null
putfield android/support/v4/app/cu/n Landroid/support/v4/app/cu;
aload 0
getfield android/support/v4/app/cu/o Landroid/support/v4/app/ct;
getfield android/support/v4/app/ct/c Landroid/support/v4/n/w;
aload 0
getfield android/support/v4/app/cu/a I
aconst_null
invokevirtual android/support/v4/n/w/a(ILjava/lang/Object;)V
aload 0
invokevirtual android/support/v4/app/cu/c()V
aload 0
getfield android/support/v4/app/cu/o Landroid/support/v4/app/ct;
aload 1
invokevirtual android/support/v4/app/ct/a(Landroid/support/v4/app/cu;)V
return
.limit locals 2
.limit stack 4
.end method

.method public final toString()Ljava/lang/String;
new java/lang/StringBuilder
dup
bipush 64
invokespecial java/lang/StringBuilder/<init>(I)V
astore 1
aload 1
ldc "LoaderInfo{"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
aload 1
aload 0
invokestatic java/lang/System/identityHashCode(Ljava/lang/Object;)I
invokestatic java/lang/Integer/toHexString(I)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
aload 1
ldc " #"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
aload 1
aload 0
getfield android/support/v4/app/cu/a I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
pop
aload 1
ldc " : "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
aload 0
getfield android/support/v4/app/cu/d Landroid/support/v4/c/aa;
aload 1
invokestatic android/support/v4/n/g/a(Ljava/lang/Object;Ljava/lang/StringBuilder;)V
aload 1
ldc "}}"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
aload 1
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 2
.limit stack 3
.end method
