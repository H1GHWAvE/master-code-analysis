.bytecode 50.0
.class final synchronized android/support/v4/app/FragmentManagerState
.super java/lang/Object
.implements android/os/Parcelable

.field public static final 'CREATOR' Landroid/os/Parcelable$Creator;

.field 'a' [Landroid/support/v4/app/FragmentState;

.field 'b' [I

.field 'c' [Landroid/support/v4/app/BackStackState;

.method static <clinit>()V
new android/support/v4/app/bv
dup
invokespecial android/support/v4/app/bv/<init>()V
putstatic android/support/v4/app/FragmentManagerState/CREATOR Landroid/os/Parcelable$Creator;
return
.limit locals 0
.limit stack 2
.end method

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public <init>(Landroid/os/Parcel;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
getstatic android/support/v4/app/FragmentState/CREATOR Landroid/os/Parcelable$Creator;
invokevirtual android/os/Parcel/createTypedArray(Landroid/os/Parcelable$Creator;)[Ljava/lang/Object;
checkcast [Landroid/support/v4/app/FragmentState;
putfield android/support/v4/app/FragmentManagerState/a [Landroid/support/v4/app/FragmentState;
aload 0
aload 1
invokevirtual android/os/Parcel/createIntArray()[I
putfield android/support/v4/app/FragmentManagerState/b [I
aload 0
aload 1
getstatic android/support/v4/app/BackStackState/CREATOR Landroid/os/Parcelable$Creator;
invokevirtual android/os/Parcel/createTypedArray(Landroid/os/Parcelable$Creator;)[Ljava/lang/Object;
checkcast [Landroid/support/v4/app/BackStackState;
putfield android/support/v4/app/FragmentManagerState/c [Landroid/support/v4/app/BackStackState;
return
.limit locals 2
.limit stack 3
.end method

.method public final describeContents()I
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
aload 1
aload 0
getfield android/support/v4/app/FragmentManagerState/a [Landroid/support/v4/app/FragmentState;
iload 2
invokevirtual android/os/Parcel/writeTypedArray([Landroid/os/Parcelable;I)V
aload 1
aload 0
getfield android/support/v4/app/FragmentManagerState/b [I
invokevirtual android/os/Parcel/writeIntArray([I)V
aload 1
aload 0
getfield android/support/v4/app/FragmentManagerState/c [Landroid/support/v4/app/BackStackState;
iload 2
invokevirtual android/os/Parcel/writeTypedArray([Landroid/os/Parcelable;I)V
return
.limit locals 3
.limit stack 3
.end method
