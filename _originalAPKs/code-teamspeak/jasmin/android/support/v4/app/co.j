.bytecode 50.0
.class public final synchronized android/support/v4/app/co
.super android/support/v4/app/Fragment

.field static final 'a' I = 16711681


.field static final 'b' I = 16711682


.field static final 'c' I = 16711683


.field private final 'at' Landroid/widget/AdapterView$OnItemClickListener;

.field 'd' Landroid/widget/ListAdapter;

.field 'e' Landroid/widget/ListView;

.field 'f' Landroid/view/View;

.field 'g' Landroid/widget/TextView;

.field 'h' Landroid/view/View;

.field 'i' Landroid/view/View;

.field 'j' Ljava/lang/CharSequence;

.field 'k' Z

.field private final 'l' Landroid/os/Handler;

.field private final 'm' Ljava/lang/Runnable;

.method public <init>()V
aload 0
invokespecial android/support/v4/app/Fragment/<init>()V
aload 0
new android/os/Handler
dup
invokespecial android/os/Handler/<init>()V
putfield android/support/v4/app/co/l Landroid/os/Handler;
aload 0
new android/support/v4/app/cp
dup
aload 0
invokespecial android/support/v4/app/cp/<init>(Landroid/support/v4/app/co;)V
putfield android/support/v4/app/co/m Ljava/lang/Runnable;
aload 0
new android/support/v4/app/cq
dup
aload 0
invokespecial android/support/v4/app/cq/<init>(Landroid/support/v4/app/co;)V
putfield android/support/v4/app/co/at Landroid/widget/AdapterView$OnItemClickListener;
return
.limit locals 1
.limit stack 4
.end method

.method private A()V
iconst_0
istore 2
aload 0
getfield android/support/v4/app/co/e Landroid/widget/ListView;
ifnull L0
return
L0:
aload 0
getfield android/support/v4/app/Fragment/ac Landroid/view/View;
astore 3
aload 3
ifnonnull L1
new java/lang/IllegalStateException
dup
ldc "Content view not yet created"
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L1:
aload 3
instanceof android/widget/ListView
ifeq L2
aload 0
aload 3
checkcast android/widget/ListView
putfield android/support/v4/app/co/e Landroid/widget/ListView;
L3:
aload 0
iconst_1
putfield android/support/v4/app/co/k Z
aload 0
getfield android/support/v4/app/co/e Landroid/widget/ListView;
aload 0
getfield android/support/v4/app/co/at Landroid/widget/AdapterView$OnItemClickListener;
invokevirtual android/widget/ListView/setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V
aload 0
getfield android/support/v4/app/co/d Landroid/widget/ListAdapter;
ifnull L4
aload 0
getfield android/support/v4/app/co/d Landroid/widget/ListAdapter;
astore 3
aload 0
aconst_null
putfield android/support/v4/app/co/d Landroid/widget/ListAdapter;
aload 0
getfield android/support/v4/app/co/d Landroid/widget/ListAdapter;
ifnull L5
iconst_1
istore 1
L6:
aload 0
aload 3
putfield android/support/v4/app/co/d Landroid/widget/ListAdapter;
aload 0
getfield android/support/v4/app/co/e Landroid/widget/ListView;
ifnull L7
aload 0
getfield android/support/v4/app/co/e Landroid/widget/ListView;
aload 3
invokevirtual android/widget/ListView/setAdapter(Landroid/widget/ListAdapter;)V
aload 0
getfield android/support/v4/app/co/k Z
ifne L7
iload 1
ifne L7
aload 0
getfield android/support/v4/app/Fragment/ac Landroid/view/View;
invokevirtual android/view/View/getWindowToken()Landroid/os/IBinder;
ifnull L8
iconst_1
istore 2
L8:
aload 0
iconst_1
iload 2
invokespecial android/support/v4/app/co/a(ZZ)V
L7:
aload 0
getfield android/support/v4/app/co/l Landroid/os/Handler;
aload 0
getfield android/support/v4/app/co/m Ljava/lang/Runnable;
invokevirtual android/os/Handler/post(Ljava/lang/Runnable;)Z
pop
return
L2:
aload 0
aload 3
ldc_w 16711681
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/TextView
putfield android/support/v4/app/co/g Landroid/widget/TextView;
aload 0
getfield android/support/v4/app/co/g Landroid/widget/TextView;
ifnonnull L9
aload 0
aload 3
ldc_w 16908292
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
putfield android/support/v4/app/co/f Landroid/view/View;
L10:
aload 0
aload 3
ldc_w 16711682
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
putfield android/support/v4/app/co/h Landroid/view/View;
aload 0
aload 3
ldc_w 16711683
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
putfield android/support/v4/app/co/i Landroid/view/View;
aload 3
ldc_w 16908298
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
astore 3
aload 3
instanceof android/widget/ListView
ifne L11
aload 3
ifnonnull L12
new java/lang/RuntimeException
dup
ldc "Your content must have a ListView whose id attribute is 'android.R.id.list'"
invokespecial java/lang/RuntimeException/<init>(Ljava/lang/String;)V
athrow
L9:
aload 0
getfield android/support/v4/app/co/g Landroid/widget/TextView;
bipush 8
invokevirtual android/widget/TextView/setVisibility(I)V
goto L10
L12:
new java/lang/RuntimeException
dup
ldc "Content has view with id attribute 'android.R.id.list' that is not a ListView class"
invokespecial java/lang/RuntimeException/<init>(Ljava/lang/String;)V
athrow
L11:
aload 0
aload 3
checkcast android/widget/ListView
putfield android/support/v4/app/co/e Landroid/widget/ListView;
aload 0
getfield android/support/v4/app/co/f Landroid/view/View;
ifnull L13
aload 0
getfield android/support/v4/app/co/e Landroid/widget/ListView;
aload 0
getfield android/support/v4/app/co/f Landroid/view/View;
invokevirtual android/widget/ListView/setEmptyView(Landroid/view/View;)V
goto L3
L13:
aload 0
getfield android/support/v4/app/co/j Ljava/lang/CharSequence;
ifnull L3
aload 0
getfield android/support/v4/app/co/g Landroid/widget/TextView;
aload 0
getfield android/support/v4/app/co/j Ljava/lang/CharSequence;
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
aload 0
getfield android/support/v4/app/co/e Landroid/widget/ListView;
aload 0
getfield android/support/v4/app/co/g Landroid/widget/TextView;
invokevirtual android/widget/ListView/setEmptyView(Landroid/view/View;)V
goto L3
L5:
iconst_0
istore 1
goto L6
L4:
aload 0
getfield android/support/v4/app/co/h Landroid/view/View;
ifnull L7
aload 0
iconst_0
iconst_0
invokespecial android/support/v4/app/co/a(ZZ)V
goto L7
.limit locals 4
.limit stack 3
.end method

.method public static a()V
return
.limit locals 0
.limit stack 0
.end method

.method private a(I)V
aload 0
invokespecial android/support/v4/app/co/A()V
aload 0
getfield android/support/v4/app/co/e Landroid/widget/ListView;
iload 1
invokevirtual android/widget/ListView/setSelection(I)V
return
.limit locals 2
.limit stack 2
.end method

.method private a(Landroid/widget/ListAdapter;)V
iconst_0
istore 3
aload 0
getfield android/support/v4/app/co/d Landroid/widget/ListAdapter;
ifnull L0
iconst_1
istore 2
L1:
aload 0
aload 1
putfield android/support/v4/app/co/d Landroid/widget/ListAdapter;
aload 0
getfield android/support/v4/app/co/e Landroid/widget/ListView;
ifnull L2
aload 0
getfield android/support/v4/app/co/e Landroid/widget/ListView;
aload 1
invokevirtual android/widget/ListView/setAdapter(Landroid/widget/ListAdapter;)V
aload 0
getfield android/support/v4/app/co/k Z
ifne L2
iload 2
ifne L2
aload 0
getfield android/support/v4/app/Fragment/ac Landroid/view/View;
invokevirtual android/view/View/getWindowToken()Landroid/os/IBinder;
ifnull L3
iconst_1
istore 3
L3:
aload 0
iconst_1
iload 3
invokespecial android/support/v4/app/co/a(ZZ)V
L2:
return
L0:
iconst_0
istore 2
goto L1
.limit locals 4
.limit stack 3
.end method

.method private a(Ljava/lang/CharSequence;)V
aload 0
invokespecial android/support/v4/app/co/A()V
aload 0
getfield android/support/v4/app/co/g Landroid/widget/TextView;
ifnonnull L0
new java/lang/IllegalStateException
dup
ldc "Can't be used with a custom content view"
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
getfield android/support/v4/app/co/g Landroid/widget/TextView;
aload 1
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
aload 0
getfield android/support/v4/app/co/j Ljava/lang/CharSequence;
ifnonnull L1
aload 0
getfield android/support/v4/app/co/e Landroid/widget/ListView;
aload 0
getfield android/support/v4/app/co/g Landroid/widget/TextView;
invokevirtual android/widget/ListView/setEmptyView(Landroid/view/View;)V
L1:
aload 0
aload 1
putfield android/support/v4/app/co/j Ljava/lang/CharSequence;
return
.limit locals 2
.limit stack 3
.end method

.method private a(Z)V
aload 0
iload 1
iconst_1
invokespecial android/support/v4/app/co/a(ZZ)V
return
.limit locals 2
.limit stack 3
.end method

.method private a(ZZ)V
aload 0
invokespecial android/support/v4/app/co/A()V
aload 0
getfield android/support/v4/app/co/h Landroid/view/View;
ifnonnull L0
new java/lang/IllegalStateException
dup
ldc "Can't be used with a custom content view"
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
getfield android/support/v4/app/co/k Z
iload 1
if_icmpne L1
return
L1:
aload 0
iload 1
putfield android/support/v4/app/co/k Z
iload 1
ifeq L2
iload 2
ifeq L3
aload 0
getfield android/support/v4/app/co/h Landroid/view/View;
aload 0
invokevirtual android/support/v4/app/co/i()Landroid/support/v4/app/bb;
ldc_w 17432577
invokestatic android/view/animation/AnimationUtils/loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;
invokevirtual android/view/View/startAnimation(Landroid/view/animation/Animation;)V
aload 0
getfield android/support/v4/app/co/i Landroid/view/View;
aload 0
invokevirtual android/support/v4/app/co/i()Landroid/support/v4/app/bb;
ldc_w 17432576
invokestatic android/view/animation/AnimationUtils/loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;
invokevirtual android/view/View/startAnimation(Landroid/view/animation/Animation;)V
L4:
aload 0
getfield android/support/v4/app/co/h Landroid/view/View;
bipush 8
invokevirtual android/view/View/setVisibility(I)V
aload 0
getfield android/support/v4/app/co/i Landroid/view/View;
iconst_0
invokevirtual android/view/View/setVisibility(I)V
return
L3:
aload 0
getfield android/support/v4/app/co/h Landroid/view/View;
invokevirtual android/view/View/clearAnimation()V
aload 0
getfield android/support/v4/app/co/i Landroid/view/View;
invokevirtual android/view/View/clearAnimation()V
goto L4
L2:
iload 2
ifeq L5
aload 0
getfield android/support/v4/app/co/h Landroid/view/View;
aload 0
invokevirtual android/support/v4/app/co/i()Landroid/support/v4/app/bb;
ldc_w 17432576
invokestatic android/view/animation/AnimationUtils/loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;
invokevirtual android/view/View/startAnimation(Landroid/view/animation/Animation;)V
aload 0
getfield android/support/v4/app/co/i Landroid/view/View;
aload 0
invokevirtual android/support/v4/app/co/i()Landroid/support/v4/app/bb;
ldc_w 17432577
invokestatic android/view/animation/AnimationUtils/loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;
invokevirtual android/view/View/startAnimation(Landroid/view/animation/Animation;)V
L6:
aload 0
getfield android/support/v4/app/co/h Landroid/view/View;
iconst_0
invokevirtual android/view/View/setVisibility(I)V
aload 0
getfield android/support/v4/app/co/i Landroid/view/View;
bipush 8
invokevirtual android/view/View/setVisibility(I)V
return
L5:
aload 0
getfield android/support/v4/app/co/h Landroid/view/View;
invokevirtual android/view/View/clearAnimation()V
aload 0
getfield android/support/v4/app/co/i Landroid/view/View;
invokevirtual android/view/View/clearAnimation()V
goto L6
.limit locals 3
.limit stack 3
.end method

.method private b()I
aload 0
invokespecial android/support/v4/app/co/A()V
aload 0
getfield android/support/v4/app/co/e Landroid/widget/ListView;
invokevirtual android/widget/ListView/getSelectedItemPosition()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private d()J
aload 0
invokespecial android/support/v4/app/co/A()V
aload 0
getfield android/support/v4/app/co/e Landroid/widget/ListView;
invokevirtual android/widget/ListView/getSelectedItemId()J
lreturn
.limit locals 1
.limit stack 2
.end method

.method private d(Z)V
aload 0
iload 1
iconst_0
invokespecial android/support/v4/app/co/a(ZZ)V
return
.limit locals 2
.limit stack 3
.end method

.method private y()Landroid/widget/ListView;
aload 0
invokespecial android/support/v4/app/co/A()V
aload 0
getfield android/support/v4/app/co/e Landroid/widget/ListView;
areturn
.limit locals 1
.limit stack 1
.end method

.method private z()Landroid/widget/ListAdapter;
aload 0
getfield android/support/v4/app/co/d Landroid/widget/ListAdapter;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
aload 0
invokevirtual android/support/v4/app/co/i()Landroid/support/v4/app/bb;
astore 2
new android/widget/FrameLayout
dup
aload 2
invokespecial android/widget/FrameLayout/<init>(Landroid/content/Context;)V
astore 1
new android/widget/LinearLayout
dup
aload 2
invokespecial android/widget/LinearLayout/<init>(Landroid/content/Context;)V
astore 3
aload 3
ldc_w 16711682
invokevirtual android/widget/LinearLayout/setId(I)V
aload 3
iconst_1
invokevirtual android/widget/LinearLayout/setOrientation(I)V
aload 3
bipush 8
invokevirtual android/widget/LinearLayout/setVisibility(I)V
aload 3
bipush 17
invokevirtual android/widget/LinearLayout/setGravity(I)V
aload 3
new android/widget/ProgressBar
dup
aload 2
aconst_null
ldc_w 16842874
invokespecial android/widget/ProgressBar/<init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
new android/widget/FrameLayout$LayoutParams
dup
bipush -2
bipush -2
invokespecial android/widget/FrameLayout$LayoutParams/<init>(II)V
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
aload 1
aload 3
new android/widget/FrameLayout$LayoutParams
dup
iconst_m1
iconst_m1
invokespecial android/widget/FrameLayout$LayoutParams/<init>(II)V
invokevirtual android/widget/FrameLayout/addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
new android/widget/FrameLayout
dup
aload 2
invokespecial android/widget/FrameLayout/<init>(Landroid/content/Context;)V
astore 2
aload 2
ldc_w 16711683
invokevirtual android/widget/FrameLayout/setId(I)V
new android/widget/TextView
dup
aload 0
invokevirtual android/support/v4/app/co/i()Landroid/support/v4/app/bb;
invokespecial android/widget/TextView/<init>(Landroid/content/Context;)V
astore 3
aload 3
ldc_w 16711681
invokevirtual android/widget/TextView/setId(I)V
aload 3
bipush 17
invokevirtual android/widget/TextView/setGravity(I)V
aload 2
aload 3
new android/widget/FrameLayout$LayoutParams
dup
iconst_m1
iconst_m1
invokespecial android/widget/FrameLayout$LayoutParams/<init>(II)V
invokevirtual android/widget/FrameLayout/addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
new android/widget/ListView
dup
aload 0
invokevirtual android/support/v4/app/co/i()Landroid/support/v4/app/bb;
invokespecial android/widget/ListView/<init>(Landroid/content/Context;)V
astore 3
aload 3
ldc_w 16908298
invokevirtual android/widget/ListView/setId(I)V
aload 3
iconst_0
invokevirtual android/widget/ListView/setDrawSelectorOnTop(Z)V
aload 2
aload 3
new android/widget/FrameLayout$LayoutParams
dup
iconst_m1
iconst_m1
invokespecial android/widget/FrameLayout$LayoutParams/<init>(II)V
invokevirtual android/widget/FrameLayout/addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
aload 1
aload 2
new android/widget/FrameLayout$LayoutParams
dup
iconst_m1
iconst_m1
invokespecial android/widget/FrameLayout$LayoutParams/<init>(II)V
invokevirtual android/widget/FrameLayout/addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
aload 1
new android/widget/FrameLayout$LayoutParams
dup
iconst_m1
iconst_m1
invokespecial android/widget/FrameLayout$LayoutParams/<init>(II)V
invokevirtual android/widget/FrameLayout/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 1
areturn
.limit locals 4
.limit stack 6
.end method

.method public final a(Landroid/view/View;Landroid/os/Bundle;)V
aload 0
aload 1
aload 2
invokespecial android/support/v4/app/Fragment/a(Landroid/view/View;Landroid/os/Bundle;)V
aload 0
invokespecial android/support/v4/app/co/A()V
return
.limit locals 3
.limit stack 3
.end method

.method public final g()V
aload 0
getfield android/support/v4/app/co/l Landroid/os/Handler;
aload 0
getfield android/support/v4/app/co/m Ljava/lang/Runnable;
invokevirtual android/os/Handler/removeCallbacks(Ljava/lang/Runnable;)V
aload 0
aconst_null
putfield android/support/v4/app/co/e Landroid/widget/ListView;
aload 0
iconst_0
putfield android/support/v4/app/co/k Z
aload 0
aconst_null
putfield android/support/v4/app/co/i Landroid/view/View;
aload 0
aconst_null
putfield android/support/v4/app/co/h Landroid/view/View;
aload 0
aconst_null
putfield android/support/v4/app/co/f Landroid/view/View;
aload 0
aconst_null
putfield android/support/v4/app/co/g Landroid/widget/TextView;
aload 0
invokespecial android/support/v4/app/Fragment/g()V
return
.limit locals 1
.limit stack 2
.end method
