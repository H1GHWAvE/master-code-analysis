.bytecode 50.0
.class public final synchronized android/support/v4/app/cv
.super java/lang/Object

.field public static final 'a' Ljava/lang/String; = "android.support.PARENT_ACTIVITY"

.field private static final 'b' Ljava/lang/String; = "NavUtils"

.field private static final 'c' Landroid/support/v4/app/cw;

.method static <clinit>()V
getstatic android/os/Build$VERSION/SDK_INT I
bipush 16
if_icmplt L0
new android/support/v4/app/cy
dup
invokespecial android/support/v4/app/cy/<init>()V
putstatic android/support/v4/app/cv/c Landroid/support/v4/app/cw;
return
L0:
new android/support/v4/app/cx
dup
invokespecial android/support/v4/app/cx/<init>()V
putstatic android/support/v4/app/cv/c Landroid/support/v4/app/cw;
return
.limit locals 0
.limit stack 2
.end method

.method private <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static a(Landroid/app/Activity;)Landroid/content/Intent;
getstatic android/support/v4/app/cv/c Landroid/support/v4/app/cw;
aload 0
invokeinterface android/support/v4/app/cw/a(Landroid/app/Activity;)Landroid/content/Intent; 1
areturn
.limit locals 1
.limit stack 2
.end method

.method public static a(Landroid/content/Context;Landroid/content/ComponentName;)Landroid/content/Intent;
aload 0
aload 1
invokestatic android/support/v4/app/cv/b(Landroid/content/Context;Landroid/content/ComponentName;)Ljava/lang/String;
astore 2
aload 2
ifnonnull L0
aconst_null
areturn
L0:
new android/content/ComponentName
dup
aload 1
invokevirtual android/content/ComponentName/getPackageName()Ljava/lang/String;
aload 2
invokespecial android/content/ComponentName/<init>(Ljava/lang/String;Ljava/lang/String;)V
astore 1
aload 0
aload 1
invokestatic android/support/v4/app/cv/b(Landroid/content/Context;Landroid/content/ComponentName;)Ljava/lang/String;
ifnonnull L1
aload 1
invokestatic android/support/v4/c/t/a(Landroid/content/ComponentName;)Landroid/content/Intent;
areturn
L1:
new android/content/Intent
dup
invokespecial android/content/Intent/<init>()V
aload 1
invokevirtual android/content/Intent/setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;
areturn
.limit locals 3
.limit stack 4
.end method

.method private static a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;
aload 0
new android/content/ComponentName
dup
aload 0
aload 1
invokespecial android/content/ComponentName/<init>(Landroid/content/Context;Ljava/lang/Class;)V
invokestatic android/support/v4/app/cv/b(Landroid/content/Context;Landroid/content/ComponentName;)Ljava/lang/String;
astore 1
aload 1
ifnonnull L0
aconst_null
areturn
L0:
new android/content/ComponentName
dup
aload 0
aload 1
invokespecial android/content/ComponentName/<init>(Landroid/content/Context;Ljava/lang/String;)V
astore 1
aload 0
aload 1
invokestatic android/support/v4/app/cv/b(Landroid/content/Context;Landroid/content/ComponentName;)Ljava/lang/String;
ifnonnull L1
aload 1
invokestatic android/support/v4/c/t/a(Landroid/content/ComponentName;)Landroid/content/Intent;
areturn
L1:
new android/content/Intent
dup
invokespecial android/content/Intent/<init>()V
aload 1
invokevirtual android/content/Intent/setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;
areturn
.limit locals 2
.limit stack 5
.end method

.method public static a(Landroid/app/Activity;Landroid/content/Intent;)Z
getstatic android/support/v4/app/cv/c Landroid/support/v4/app/cw;
aload 0
aload 1
invokeinterface android/support/v4/app/cw/a(Landroid/app/Activity;Landroid/content/Intent;)Z 2
ireturn
.limit locals 2
.limit stack 3
.end method

.method public static b(Landroid/app/Activity;)Ljava/lang/String;
.annotation invisible Landroid/support/a/z;
.end annotation
.catch android/content/pm/PackageManager$NameNotFoundException from L0 to L1 using L2
L0:
aload 0
aload 0
invokevirtual android/app/Activity/getComponentName()Landroid/content/ComponentName;
invokestatic android/support/v4/app/cv/b(Landroid/content/Context;Landroid/content/ComponentName;)Ljava/lang/String;
astore 0
L1:
aload 0
areturn
L2:
astore 0
new java/lang/IllegalArgumentException
dup
aload 0
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/Throwable;)V
athrow
.limit locals 1
.limit stack 3
.end method

.method public static b(Landroid/content/Context;Landroid/content/ComponentName;)Ljava/lang/String;
.annotation invisible Landroid/support/a/z;
.end annotation
aload 0
invokevirtual android/content/Context/getPackageManager()Landroid/content/pm/PackageManager;
aload 1
sipush 128
invokevirtual android/content/pm/PackageManager/getActivityInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;
astore 1
getstatic android/support/v4/app/cv/c Landroid/support/v4/app/cw;
aload 0
aload 1
invokeinterface android/support/v4/app/cw/a(Landroid/content/Context;Landroid/content/pm/ActivityInfo;)Ljava/lang/String; 2
areturn
.limit locals 2
.limit stack 3
.end method

.method public static b(Landroid/app/Activity;Landroid/content/Intent;)V
getstatic android/support/v4/app/cv/c Landroid/support/v4/app/cw;
aload 0
aload 1
invokeinterface android/support/v4/app/cw/b(Landroid/app/Activity;Landroid/content/Intent;)V 2
return
.limit locals 2
.limit stack 3
.end method

.method private static c(Landroid/app/Activity;)V
aload 0
invokestatic android/support/v4/app/cv/a(Landroid/app/Activity;)Landroid/content/Intent;
astore 1
aload 1
ifnonnull L0
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
ldc "Activity "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
invokevirtual java/lang/Class/getSimpleName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " does not have a parent activity name specified. (Did you forget to add the android.support.PARENT_ACTIVITY <meta-data>  element in your manifest?)"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
aload 1
invokestatic android/support/v4/app/cv/b(Landroid/app/Activity;Landroid/content/Intent;)V
return
.limit locals 2
.limit stack 5
.end method
