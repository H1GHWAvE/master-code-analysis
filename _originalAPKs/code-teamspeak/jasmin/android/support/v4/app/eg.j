.bytecode 50.0
.class public final synchronized android/support/v4/app/eg
.super java/lang/Object
.implements android/support/v4/app/db
.implements android/support/v4/app/dc

.field private 'a' Landroid/app/Notification$Builder;

.field private 'b' Landroid/os/Bundle;

.method public <init>(Landroid/content/Context;Landroid/app/Notification;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/widget/RemoteViews;ILandroid/app/PendingIntent;Landroid/app/PendingIntent;Landroid/graphics/Bitmap;IIZZZILjava/lang/CharSequence;ZLjava/util/ArrayList;Landroid/os/Bundle;Ljava/lang/String;ZLjava/lang/String;)V
aload 0
invokespecial java/lang/Object/<init>()V
new android/app/Notification$Builder
dup
aload 1
invokespecial android/app/Notification$Builder/<init>(Landroid/content/Context;)V
aload 2
getfield android/app/Notification/when J
invokevirtual android/app/Notification$Builder/setWhen(J)Landroid/app/Notification$Builder;
iload 14
invokevirtual android/app/Notification$Builder/setShowWhen(Z)Landroid/app/Notification$Builder;
aload 2
getfield android/app/Notification/icon I
aload 2
getfield android/app/Notification/iconLevel I
invokevirtual android/app/Notification$Builder/setSmallIcon(II)Landroid/app/Notification$Builder;
aload 2
getfield android/app/Notification/contentView Landroid/widget/RemoteViews;
invokevirtual android/app/Notification$Builder/setContent(Landroid/widget/RemoteViews;)Landroid/app/Notification$Builder;
aload 2
getfield android/app/Notification/tickerText Ljava/lang/CharSequence;
aload 6
invokevirtual android/app/Notification$Builder/setTicker(Ljava/lang/CharSequence;Landroid/widget/RemoteViews;)Landroid/app/Notification$Builder;
aload 2
getfield android/app/Notification/sound Landroid/net/Uri;
aload 2
getfield android/app/Notification/audioStreamType I
invokevirtual android/app/Notification$Builder/setSound(Landroid/net/Uri;I)Landroid/app/Notification$Builder;
aload 2
getfield android/app/Notification/vibrate [J
invokevirtual android/app/Notification$Builder/setVibrate([J)Landroid/app/Notification$Builder;
aload 2
getfield android/app/Notification/ledARGB I
aload 2
getfield android/app/Notification/ledOnMS I
aload 2
getfield android/app/Notification/ledOffMS I
invokevirtual android/app/Notification$Builder/setLights(III)Landroid/app/Notification$Builder;
astore 1
aload 2
getfield android/app/Notification/flags I
iconst_2
iand
ifeq L0
iconst_1
istore 14
L1:
aload 1
iload 14
invokevirtual android/app/Notification$Builder/setOngoing(Z)Landroid/app/Notification$Builder;
astore 1
aload 2
getfield android/app/Notification/flags I
bipush 8
iand
ifeq L2
iconst_1
istore 14
L3:
aload 1
iload 14
invokevirtual android/app/Notification$Builder/setOnlyAlertOnce(Z)Landroid/app/Notification$Builder;
astore 1
aload 2
getfield android/app/Notification/flags I
bipush 16
iand
ifeq L4
iconst_1
istore 14
L5:
aload 1
iload 14
invokevirtual android/app/Notification$Builder/setAutoCancel(Z)Landroid/app/Notification$Builder;
aload 2
getfield android/app/Notification/defaults I
invokevirtual android/app/Notification$Builder/setDefaults(I)Landroid/app/Notification$Builder;
aload 3
invokevirtual android/app/Notification$Builder/setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;
aload 4
invokevirtual android/app/Notification$Builder/setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;
aload 17
invokevirtual android/app/Notification$Builder/setSubText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;
aload 5
invokevirtual android/app/Notification$Builder/setContentInfo(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;
aload 8
invokevirtual android/app/Notification$Builder/setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;
aload 2
getfield android/app/Notification/deleteIntent Landroid/app/PendingIntent;
invokevirtual android/app/Notification$Builder/setDeleteIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;
astore 1
aload 2
getfield android/app/Notification/flags I
sipush 128
iand
ifeq L6
iconst_1
istore 14
L7:
aload 0
aload 1
aload 9
iload 14
invokevirtual android/app/Notification$Builder/setFullScreenIntent(Landroid/app/PendingIntent;Z)Landroid/app/Notification$Builder;
aload 10
invokevirtual android/app/Notification$Builder/setLargeIcon(Landroid/graphics/Bitmap;)Landroid/app/Notification$Builder;
iload 7
invokevirtual android/app/Notification$Builder/setNumber(I)Landroid/app/Notification$Builder;
iload 15
invokevirtual android/app/Notification$Builder/setUsesChronometer(Z)Landroid/app/Notification$Builder;
iload 16
invokevirtual android/app/Notification$Builder/setPriority(I)Landroid/app/Notification$Builder;
iload 11
iload 12
iload 13
invokevirtual android/app/Notification$Builder/setProgress(IIZ)Landroid/app/Notification$Builder;
iload 18
invokevirtual android/app/Notification$Builder/setLocalOnly(Z)Landroid/app/Notification$Builder;
aload 21
invokevirtual android/app/Notification$Builder/setGroup(Ljava/lang/String;)Landroid/app/Notification$Builder;
iload 22
invokevirtual android/app/Notification$Builder/setGroupSummary(Z)Landroid/app/Notification$Builder;
aload 23
invokevirtual android/app/Notification$Builder/setSortKey(Ljava/lang/String;)Landroid/app/Notification$Builder;
putfield android/support/v4/app/eg/a Landroid/app/Notification$Builder;
aload 0
new android/os/Bundle
dup
invokespecial android/os/Bundle/<init>()V
putfield android/support/v4/app/eg/b Landroid/os/Bundle;
aload 20
ifnull L8
aload 0
getfield android/support/v4/app/eg/b Landroid/os/Bundle;
aload 20
invokevirtual android/os/Bundle/putAll(Landroid/os/Bundle;)V
L8:
aload 19
ifnull L9
aload 19
invokevirtual java/util/ArrayList/isEmpty()Z
ifne L9
aload 0
getfield android/support/v4/app/eg/b Landroid/os/Bundle;
ldc "android.people"
aload 19
aload 19
invokevirtual java/util/ArrayList/size()I
anewarray java/lang/String
invokevirtual java/util/ArrayList/toArray([Ljava/lang/Object;)[Ljava/lang/Object;
checkcast [Ljava/lang/String;
invokevirtual android/os/Bundle/putStringArray(Ljava/lang/String;[Ljava/lang/String;)V
L9:
return
L0:
iconst_0
istore 14
goto L1
L2:
iconst_0
istore 14
goto L3
L4:
iconst_0
istore 14
goto L5
L6:
iconst_0
istore 14
goto L7
.limit locals 24
.limit stack 5
.end method

.method public final a()Landroid/app/Notification$Builder;
aload 0
getfield android/support/v4/app/eg/a Landroid/app/Notification$Builder;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final a(Landroid/support/v4/app/ek;)V
aload 0
getfield android/support/v4/app/eg/a Landroid/app/Notification$Builder;
aload 1
invokestatic android/support/v4/app/ef/a(Landroid/app/Notification$Builder;Landroid/support/v4/app/ek;)V
return
.limit locals 2
.limit stack 2
.end method

.method public final b()Landroid/app/Notification;
aload 0
getfield android/support/v4/app/eg/a Landroid/app/Notification$Builder;
aload 0
getfield android/support/v4/app/eg/b Landroid/os/Bundle;
invokevirtual android/app/Notification$Builder/setExtras(Landroid/os/Bundle;)Landroid/app/Notification$Builder;
pop
aload 0
getfield android/support/v4/app/eg/a Landroid/app/Notification$Builder;
invokevirtual android/app/Notification$Builder/build()Landroid/app/Notification;
areturn
.limit locals 1
.limit stack 2
.end method
