.bytecode 50.0
.class public final synchronized android/support/v4/app/ee
.super java/lang/Object
.implements android/support/v4/app/ds

.field private static final 'A' I = 16


.field private static final 'B' I = 1


.field private static final 'C' I = 8388613


.field private static final 'D' I = 80


.field public static final 'a' I = -1


.field public static final 'b' I = 0


.field public static final 'c' I = 1


.field public static final 'd' I = 2


.field public static final 'e' I = 3


.field public static final 'f' I = 4


.field public static final 'g' I = 5


.field public static final 'h' I = 0


.field public static final 'i' I = -1


.field private static final 'j' Ljava/lang/String; = "android.wearable.EXTENSIONS"

.field private static final 'k' Ljava/lang/String; = "actions"

.field private static final 'l' Ljava/lang/String; = "flags"

.field private static final 'm' Ljava/lang/String; = "displayIntent"

.field private static final 'n' Ljava/lang/String; = "pages"

.field private static final 'o' Ljava/lang/String; = "background"

.field private static final 'p' Ljava/lang/String; = "contentIcon"

.field private static final 'q' Ljava/lang/String; = "contentIconGravity"

.field private static final 'r' Ljava/lang/String; = "contentActionIndex"

.field private static final 's' Ljava/lang/String; = "customSizePreset"

.field private static final 't' Ljava/lang/String; = "customContentHeight"

.field private static final 'u' Ljava/lang/String; = "gravity"

.field private static final 'v' Ljava/lang/String; = "hintScreenTimeout"

.field private static final 'w' I = 1


.field private static final 'x' I = 2


.field private static final 'y' I = 4


.field private static final 'z' I = 8


.field private 'E' Ljava/util/ArrayList;

.field private 'F' I

.field private 'G' Landroid/app/PendingIntent;

.field private 'H' Ljava/util/ArrayList;

.field private 'I' Landroid/graphics/Bitmap;

.field private 'J' I

.field private 'K' I

.field private 'L' I

.field private 'M' I

.field private 'N' I

.field private 'O' I

.field private 'P' I

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield android/support/v4/app/ee/E Ljava/util/ArrayList;
aload 0
iconst_1
putfield android/support/v4/app/ee/F I
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield android/support/v4/app/ee/H Ljava/util/ArrayList;
aload 0
ldc_w 8388613
putfield android/support/v4/app/ee/K I
aload 0
iconst_m1
putfield android/support/v4/app/ee/L I
aload 0
iconst_0
putfield android/support/v4/app/ee/M I
aload 0
bipush 80
putfield android/support/v4/app/ee/O I
return
.limit locals 1
.limit stack 3
.end method

.method private <init>(Landroid/app/Notification;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield android/support/v4/app/ee/E Ljava/util/ArrayList;
aload 0
iconst_1
putfield android/support/v4/app/ee/F I
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield android/support/v4/app/ee/H Ljava/util/ArrayList;
aload 0
ldc_w 8388613
putfield android/support/v4/app/ee/K I
aload 0
iconst_m1
putfield android/support/v4/app/ee/L I
aload 0
iconst_0
putfield android/support/v4/app/ee/M I
aload 0
bipush 80
putfield android/support/v4/app/ee/O I
aload 1
invokestatic android/support/v4/app/dd/a(Landroid/app/Notification;)Landroid/os/Bundle;
astore 1
aload 1
ifnull L0
aload 1
ldc "android.wearable.EXTENSIONS"
invokevirtual android/os/Bundle/getBundle(Ljava/lang/String;)Landroid/os/Bundle;
astore 1
L1:
aload 1
ifnull L2
invokestatic android/support/v4/app/dd/a()Landroid/support/v4/app/du;
aload 1
ldc "actions"
invokevirtual android/os/Bundle/getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;
invokeinterface android/support/v4/app/du/a(Ljava/util/ArrayList;)[Landroid/support/v4/app/df; 1
astore 2
aload 2
ifnull L3
aload 0
getfield android/support/v4/app/ee/E Ljava/util/ArrayList;
aload 2
invokestatic java/util/Collections/addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z
pop
L3:
aload 0
aload 1
ldc "flags"
iconst_1
invokevirtual android/os/Bundle/getInt(Ljava/lang/String;I)I
putfield android/support/v4/app/ee/F I
aload 0
aload 1
ldc "displayIntent"
invokevirtual android/os/Bundle/getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;
checkcast android/app/PendingIntent
putfield android/support/v4/app/ee/G Landroid/app/PendingIntent;
aload 1
ldc "pages"
invokestatic android/support/v4/app/dd/a(Landroid/os/Bundle;Ljava/lang/String;)[Landroid/app/Notification;
astore 2
aload 2
ifnull L4
aload 0
getfield android/support/v4/app/ee/H Ljava/util/ArrayList;
aload 2
invokestatic java/util/Collections/addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z
pop
L4:
aload 0
aload 1
ldc "background"
invokevirtual android/os/Bundle/getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;
checkcast android/graphics/Bitmap
putfield android/support/v4/app/ee/I Landroid/graphics/Bitmap;
aload 0
aload 1
ldc "contentIcon"
invokevirtual android/os/Bundle/getInt(Ljava/lang/String;)I
putfield android/support/v4/app/ee/J I
aload 0
aload 1
ldc "contentIconGravity"
ldc_w 8388613
invokevirtual android/os/Bundle/getInt(Ljava/lang/String;I)I
putfield android/support/v4/app/ee/K I
aload 0
aload 1
ldc "contentActionIndex"
iconst_m1
invokevirtual android/os/Bundle/getInt(Ljava/lang/String;I)I
putfield android/support/v4/app/ee/L I
aload 0
aload 1
ldc "customSizePreset"
iconst_0
invokevirtual android/os/Bundle/getInt(Ljava/lang/String;I)I
putfield android/support/v4/app/ee/M I
aload 0
aload 1
ldc "customContentHeight"
invokevirtual android/os/Bundle/getInt(Ljava/lang/String;)I
putfield android/support/v4/app/ee/N I
aload 0
aload 1
ldc "gravity"
bipush 80
invokevirtual android/os/Bundle/getInt(Ljava/lang/String;I)I
putfield android/support/v4/app/ee/O I
aload 0
aload 1
ldc "hintScreenTimeout"
invokevirtual android/os/Bundle/getInt(Ljava/lang/String;)I
putfield android/support/v4/app/ee/P I
L2:
return
L0:
aconst_null
astore 1
goto L1
.limit locals 3
.limit stack 4
.end method

.method private a()Landroid/support/v4/app/ee;
new android/support/v4/app/ee
dup
invokespecial android/support/v4/app/ee/<init>()V
astore 1
aload 1
new java/util/ArrayList
dup
aload 0
getfield android/support/v4/app/ee/E Ljava/util/ArrayList;
invokespecial java/util/ArrayList/<init>(Ljava/util/Collection;)V
putfield android/support/v4/app/ee/E Ljava/util/ArrayList;
aload 1
aload 0
getfield android/support/v4/app/ee/F I
putfield android/support/v4/app/ee/F I
aload 1
aload 0
getfield android/support/v4/app/ee/G Landroid/app/PendingIntent;
putfield android/support/v4/app/ee/G Landroid/app/PendingIntent;
aload 1
new java/util/ArrayList
dup
aload 0
getfield android/support/v4/app/ee/H Ljava/util/ArrayList;
invokespecial java/util/ArrayList/<init>(Ljava/util/Collection;)V
putfield android/support/v4/app/ee/H Ljava/util/ArrayList;
aload 1
aload 0
getfield android/support/v4/app/ee/I Landroid/graphics/Bitmap;
putfield android/support/v4/app/ee/I Landroid/graphics/Bitmap;
aload 1
aload 0
getfield android/support/v4/app/ee/J I
putfield android/support/v4/app/ee/J I
aload 1
aload 0
getfield android/support/v4/app/ee/K I
putfield android/support/v4/app/ee/K I
aload 1
aload 0
getfield android/support/v4/app/ee/L I
putfield android/support/v4/app/ee/L I
aload 1
aload 0
getfield android/support/v4/app/ee/M I
putfield android/support/v4/app/ee/M I
aload 1
aload 0
getfield android/support/v4/app/ee/N I
putfield android/support/v4/app/ee/N I
aload 1
aload 0
getfield android/support/v4/app/ee/O I
putfield android/support/v4/app/ee/O I
aload 1
aload 0
getfield android/support/v4/app/ee/P I
putfield android/support/v4/app/ee/P I
aload 1
areturn
.limit locals 2
.limit stack 4
.end method

.method private a(I)Landroid/support/v4/app/ee;
aload 0
iload 1
putfield android/support/v4/app/ee/J I
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method private a(Landroid/app/Notification;)Landroid/support/v4/app/ee;
aload 0
getfield android/support/v4/app/ee/H Ljava/util/ArrayList;
aload 1
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method private a(Landroid/app/PendingIntent;)Landroid/support/v4/app/ee;
aload 0
aload 1
putfield android/support/v4/app/ee/G Landroid/app/PendingIntent;
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method private a(Landroid/graphics/Bitmap;)Landroid/support/v4/app/ee;
aload 0
aload 1
putfield android/support/v4/app/ee/I Landroid/graphics/Bitmap;
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method private a(Landroid/support/v4/app/df;)Landroid/support/v4/app/ee;
aload 0
getfield android/support/v4/app/ee/E Ljava/util/ArrayList;
aload 1
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method private a(Ljava/util/List;)Landroid/support/v4/app/ee;
aload 0
getfield android/support/v4/app/ee/E Ljava/util/ArrayList;
aload 1
invokevirtual java/util/ArrayList/addAll(Ljava/util/Collection;)Z
pop
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method private a(Z)Landroid/support/v4/app/ee;
aload 0
bipush 8
iload 1
invokespecial android/support/v4/app/ee/a(IZ)V
aload 0
areturn
.limit locals 2
.limit stack 3
.end method

.method private a(IZ)V
iload 2
ifeq L0
aload 0
aload 0
getfield android/support/v4/app/ee/F I
iload 1
ior
putfield android/support/v4/app/ee/F I
return
L0:
aload 0
aload 0
getfield android/support/v4/app/ee/F I
iload 1
iconst_m1
ixor
iand
putfield android/support/v4/app/ee/F I
return
.limit locals 3
.limit stack 4
.end method

.method private b()Landroid/support/v4/app/ee;
aload 0
getfield android/support/v4/app/ee/E Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/clear()V
aload 0
areturn
.limit locals 1
.limit stack 1
.end method

.method private b(I)Landroid/support/v4/app/ee;
aload 0
iload 1
putfield android/support/v4/app/ee/K I
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method private b(Ljava/util/List;)Landroid/support/v4/app/ee;
aload 0
getfield android/support/v4/app/ee/H Ljava/util/ArrayList;
aload 1
invokevirtual java/util/ArrayList/addAll(Ljava/util/Collection;)Z
pop
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method private b(Z)Landroid/support/v4/app/ee;
aload 0
iconst_1
iload 1
invokespecial android/support/v4/app/ee/a(IZ)V
aload 0
areturn
.limit locals 2
.limit stack 3
.end method

.method private c(I)Landroid/support/v4/app/ee;
aload 0
iload 1
putfield android/support/v4/app/ee/L I
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method private c(Z)Landroid/support/v4/app/ee;
aload 0
iconst_2
iload 1
invokespecial android/support/v4/app/ee/a(IZ)V
aload 0
areturn
.limit locals 2
.limit stack 3
.end method

.method private c()Ljava/util/List;
aload 0
getfield android/support/v4/app/ee/E Ljava/util/ArrayList;
areturn
.limit locals 1
.limit stack 1
.end method

.method private d()Landroid/app/PendingIntent;
aload 0
getfield android/support/v4/app/ee/G Landroid/app/PendingIntent;
areturn
.limit locals 1
.limit stack 1
.end method

.method private d(I)Landroid/support/v4/app/ee;
aload 0
iload 1
putfield android/support/v4/app/ee/O I
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method private d(Z)Landroid/support/v4/app/ee;
aload 0
iconst_4
iload 1
invokespecial android/support/v4/app/ee/a(IZ)V
aload 0
areturn
.limit locals 2
.limit stack 3
.end method

.method private e()Landroid/support/v4/app/ee;
aload 0
getfield android/support/v4/app/ee/H Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/clear()V
aload 0
areturn
.limit locals 1
.limit stack 1
.end method

.method private e(I)Landroid/support/v4/app/ee;
aload 0
iload 1
putfield android/support/v4/app/ee/M I
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method private e(Z)Landroid/support/v4/app/ee;
aload 0
bipush 16
iload 1
invokespecial android/support/v4/app/ee/a(IZ)V
aload 0
areturn
.limit locals 2
.limit stack 3
.end method

.method private f(I)Landroid/support/v4/app/ee;
aload 0
iload 1
putfield android/support/v4/app/ee/N I
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method private f()Ljava/util/List;
aload 0
getfield android/support/v4/app/ee/H Ljava/util/ArrayList;
areturn
.limit locals 1
.limit stack 1
.end method

.method private g()Landroid/graphics/Bitmap;
aload 0
getfield android/support/v4/app/ee/I Landroid/graphics/Bitmap;
areturn
.limit locals 1
.limit stack 1
.end method

.method private g(I)Landroid/support/v4/app/ee;
aload 0
iload 1
putfield android/support/v4/app/ee/P I
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method private h()I
aload 0
getfield android/support/v4/app/ee/J I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private i()I
aload 0
getfield android/support/v4/app/ee/K I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private j()I
aload 0
getfield android/support/v4/app/ee/L I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private k()I
aload 0
getfield android/support/v4/app/ee/O I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private l()I
aload 0
getfield android/support/v4/app/ee/M I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private m()I
aload 0
getfield android/support/v4/app/ee/N I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private n()Z
aload 0
getfield android/support/v4/app/ee/F I
bipush 8
iand
ifeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method private o()Z
aload 0
getfield android/support/v4/app/ee/F I
iconst_1
iand
ifeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method private p()Z
aload 0
getfield android/support/v4/app/ee/F I
iconst_2
iand
ifeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method private q()Z
aload 0
getfield android/support/v4/app/ee/F I
iconst_4
iand
ifeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method private r()Z
aload 0
getfield android/support/v4/app/ee/F I
bipush 16
iand
ifeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method private s()I
aload 0
getfield android/support/v4/app/ee/P I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final a(Landroid/support/v4/app/dm;)Landroid/support/v4/app/dm;
new android/os/Bundle
dup
invokespecial android/os/Bundle/<init>()V
astore 2
aload 0
getfield android/support/v4/app/ee/E Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/isEmpty()Z
ifne L0
aload 2
ldc "actions"
invokestatic android/support/v4/app/dd/a()Landroid/support/v4/app/du;
aload 0
getfield android/support/v4/app/ee/E Ljava/util/ArrayList;
aload 0
getfield android/support/v4/app/ee/E Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
anewarray android/support/v4/app/df
invokevirtual java/util/ArrayList/toArray([Ljava/lang/Object;)[Ljava/lang/Object;
checkcast [Landroid/support/v4/app/df;
invokeinterface android/support/v4/app/du/a([Landroid/support/v4/app/df;)Ljava/util/ArrayList; 1
invokevirtual android/os/Bundle/putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V
L0:
aload 0
getfield android/support/v4/app/ee/F I
iconst_1
if_icmpeq L1
aload 2
ldc "flags"
aload 0
getfield android/support/v4/app/ee/F I
invokevirtual android/os/Bundle/putInt(Ljava/lang/String;I)V
L1:
aload 0
getfield android/support/v4/app/ee/G Landroid/app/PendingIntent;
ifnull L2
aload 2
ldc "displayIntent"
aload 0
getfield android/support/v4/app/ee/G Landroid/app/PendingIntent;
invokevirtual android/os/Bundle/putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V
L2:
aload 0
getfield android/support/v4/app/ee/H Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/isEmpty()Z
ifne L3
aload 2
ldc "pages"
aload 0
getfield android/support/v4/app/ee/H Ljava/util/ArrayList;
aload 0
getfield android/support/v4/app/ee/H Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
anewarray android/app/Notification
invokevirtual java/util/ArrayList/toArray([Ljava/lang/Object;)[Ljava/lang/Object;
checkcast [Landroid/os/Parcelable;
invokevirtual android/os/Bundle/putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V
L3:
aload 0
getfield android/support/v4/app/ee/I Landroid/graphics/Bitmap;
ifnull L4
aload 2
ldc "background"
aload 0
getfield android/support/v4/app/ee/I Landroid/graphics/Bitmap;
invokevirtual android/os/Bundle/putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V
L4:
aload 0
getfield android/support/v4/app/ee/J I
ifeq L5
aload 2
ldc "contentIcon"
aload 0
getfield android/support/v4/app/ee/J I
invokevirtual android/os/Bundle/putInt(Ljava/lang/String;I)V
L5:
aload 0
getfield android/support/v4/app/ee/K I
ldc_w 8388613
if_icmpeq L6
aload 2
ldc "contentIconGravity"
aload 0
getfield android/support/v4/app/ee/K I
invokevirtual android/os/Bundle/putInt(Ljava/lang/String;I)V
L6:
aload 0
getfield android/support/v4/app/ee/L I
iconst_m1
if_icmpeq L7
aload 2
ldc "contentActionIndex"
aload 0
getfield android/support/v4/app/ee/L I
invokevirtual android/os/Bundle/putInt(Ljava/lang/String;I)V
L7:
aload 0
getfield android/support/v4/app/ee/M I
ifeq L8
aload 2
ldc "customSizePreset"
aload 0
getfield android/support/v4/app/ee/M I
invokevirtual android/os/Bundle/putInt(Ljava/lang/String;I)V
L8:
aload 0
getfield android/support/v4/app/ee/N I
ifeq L9
aload 2
ldc "customContentHeight"
aload 0
getfield android/support/v4/app/ee/N I
invokevirtual android/os/Bundle/putInt(Ljava/lang/String;I)V
L9:
aload 0
getfield android/support/v4/app/ee/O I
bipush 80
if_icmpeq L10
aload 2
ldc "gravity"
aload 0
getfield android/support/v4/app/ee/O I
invokevirtual android/os/Bundle/putInt(Ljava/lang/String;I)V
L10:
aload 0
getfield android/support/v4/app/ee/P I
ifeq L11
aload 2
ldc "hintScreenTimeout"
aload 0
getfield android/support/v4/app/ee/P I
invokevirtual android/os/Bundle/putInt(Ljava/lang/String;I)V
L11:
aload 1
invokevirtual android/support/v4/app/dm/b()Landroid/os/Bundle;
ldc "android.wearable.EXTENSIONS"
aload 2
invokevirtual android/os/Bundle/putBundle(Ljava/lang/String;Landroid/os/Bundle;)V
aload 1
areturn
.limit locals 3
.limit stack 5
.end method

.method public final synthetic clone()Ljava/lang/Object;
new android/support/v4/app/ee
dup
invokespecial android/support/v4/app/ee/<init>()V
astore 1
aload 1
new java/util/ArrayList
dup
aload 0
getfield android/support/v4/app/ee/E Ljava/util/ArrayList;
invokespecial java/util/ArrayList/<init>(Ljava/util/Collection;)V
putfield android/support/v4/app/ee/E Ljava/util/ArrayList;
aload 1
aload 0
getfield android/support/v4/app/ee/F I
putfield android/support/v4/app/ee/F I
aload 1
aload 0
getfield android/support/v4/app/ee/G Landroid/app/PendingIntent;
putfield android/support/v4/app/ee/G Landroid/app/PendingIntent;
aload 1
new java/util/ArrayList
dup
aload 0
getfield android/support/v4/app/ee/H Ljava/util/ArrayList;
invokespecial java/util/ArrayList/<init>(Ljava/util/Collection;)V
putfield android/support/v4/app/ee/H Ljava/util/ArrayList;
aload 1
aload 0
getfield android/support/v4/app/ee/I Landroid/graphics/Bitmap;
putfield android/support/v4/app/ee/I Landroid/graphics/Bitmap;
aload 1
aload 0
getfield android/support/v4/app/ee/J I
putfield android/support/v4/app/ee/J I
aload 1
aload 0
getfield android/support/v4/app/ee/K I
putfield android/support/v4/app/ee/K I
aload 1
aload 0
getfield android/support/v4/app/ee/L I
putfield android/support/v4/app/ee/L I
aload 1
aload 0
getfield android/support/v4/app/ee/M I
putfield android/support/v4/app/ee/M I
aload 1
aload 0
getfield android/support/v4/app/ee/N I
putfield android/support/v4/app/ee/N I
aload 1
aload 0
getfield android/support/v4/app/ee/O I
putfield android/support/v4/app/ee/O I
aload 1
aload 0
getfield android/support/v4/app/ee/P I
putfield android/support/v4/app/ee/P I
aload 1
areturn
.limit locals 2
.limit stack 4
.end method
