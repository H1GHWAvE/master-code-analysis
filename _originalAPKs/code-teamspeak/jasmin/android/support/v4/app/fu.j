.bytecode 50.0
.class final synchronized android/support/v4/app/fu
.super java/lang/Object

.method <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a(Landroid/content/Intent;)Landroid/os/Bundle;
aload 0
invokestatic android/app/RemoteInput/getResultsFromIntent(Landroid/content/Intent;)Landroid/os/Bundle;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static a([Landroid/support/v4/app/fw;Landroid/content/Intent;Landroid/os/Bundle;)V
aload 0
invokestatic android/support/v4/app/fu/a([Landroid/support/v4/app/fw;)[Landroid/app/RemoteInput;
aload 1
aload 2
invokestatic android/app/RemoteInput/addResultsToIntent([Landroid/app/RemoteInput;Landroid/content/Intent;Landroid/os/Bundle;)V
return
.limit locals 3
.limit stack 3
.end method

.method static a([Landroid/support/v4/app/fw;)[Landroid/app/RemoteInput;
aload 0
ifnonnull L0
aconst_null
areturn
L0:
aload 0
arraylength
anewarray android/app/RemoteInput
astore 2
iconst_0
istore 1
L1:
iload 1
aload 0
arraylength
if_icmpge L2
aload 0
iload 1
aaload
astore 3
aload 2
iload 1
new android/app/RemoteInput$Builder
dup
aload 3
invokevirtual android/support/v4/app/fw/a()Ljava/lang/String;
invokespecial android/app/RemoteInput$Builder/<init>(Ljava/lang/String;)V
aload 3
invokevirtual android/support/v4/app/fw/b()Ljava/lang/CharSequence;
invokevirtual android/app/RemoteInput$Builder/setLabel(Ljava/lang/CharSequence;)Landroid/app/RemoteInput$Builder;
aload 3
invokevirtual android/support/v4/app/fw/c()[Ljava/lang/CharSequence;
invokevirtual android/app/RemoteInput$Builder/setChoices([Ljava/lang/CharSequence;)Landroid/app/RemoteInput$Builder;
aload 3
invokevirtual android/support/v4/app/fw/d()Z
invokevirtual android/app/RemoteInput$Builder/setAllowFreeFormInput(Z)Landroid/app/RemoteInput$Builder;
aload 3
invokevirtual android/support/v4/app/fw/e()Landroid/os/Bundle;
invokevirtual android/app/RemoteInput$Builder/addExtras(Landroid/os/Bundle;)Landroid/app/RemoteInput$Builder;
invokevirtual android/app/RemoteInput$Builder/build()Landroid/app/RemoteInput;
aastore
iload 1
iconst_1
iadd
istore 1
goto L1
L2:
aload 2
areturn
.limit locals 4
.limit stack 5
.end method

.method private static a([Landroid/app/RemoteInput;Landroid/support/v4/app/fx;)[Landroid/support/v4/app/fw;
aload 0
ifnonnull L0
aconst_null
areturn
L0:
aload 1
aload 0
arraylength
invokeinterface android/support/v4/app/fx/a(I)[Landroid/support/v4/app/fw; 1
astore 3
iconst_0
istore 2
L1:
iload 2
aload 0
arraylength
if_icmpge L2
aload 0
iload 2
aaload
astore 4
aload 3
iload 2
aload 1
aload 4
invokevirtual android/app/RemoteInput/getResultKey()Ljava/lang/String;
aload 4
invokevirtual android/app/RemoteInput/getLabel()Ljava/lang/CharSequence;
aload 4
invokevirtual android/app/RemoteInput/getChoices()[Ljava/lang/CharSequence;
aload 4
invokevirtual android/app/RemoteInput/getAllowFreeFormInput()Z
aload 4
invokevirtual android/app/RemoteInput/getExtras()Landroid/os/Bundle;
invokeinterface android/support/v4/app/fx/a(Ljava/lang/String;Ljava/lang/CharSequence;[Ljava/lang/CharSequence;ZLandroid/os/Bundle;)Landroid/support/v4/app/fw; 5
aastore
iload 2
iconst_1
iadd
istore 2
goto L1
L2:
aload 3
areturn
.limit locals 5
.limit stack 8
.end method
