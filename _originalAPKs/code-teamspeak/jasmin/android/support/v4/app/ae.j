.bytecode 50.0
.class final synchronized android/support/v4/app/ae
.super java/lang/Object

.field final 'a' Landroid/app/ActivityOptions;

.method <init>(Landroid/app/ActivityOptions;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
putfield android/support/v4/app/ae/a Landroid/app/ActivityOptions;
return
.limit locals 2
.limit stack 2
.end method

.method private a()Landroid/os/Bundle;
aload 0
getfield android/support/v4/app/ae/a Landroid/app/ActivityOptions;
invokevirtual android/app/ActivityOptions/toBundle()Landroid/os/Bundle;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static a(Landroid/content/Context;II)Landroid/support/v4/app/ae;
new android/support/v4/app/ae
dup
aload 0
iload 1
iload 2
invokestatic android/app/ActivityOptions/makeCustomAnimation(Landroid/content/Context;II)Landroid/app/ActivityOptions;
invokespecial android/support/v4/app/ae/<init>(Landroid/app/ActivityOptions;)V
areturn
.limit locals 3
.limit stack 5
.end method

.method private static a(Landroid/view/View;IIII)Landroid/support/v4/app/ae;
new android/support/v4/app/ae
dup
aload 0
iload 1
iload 2
iload 3
iload 4
invokestatic android/app/ActivityOptions/makeScaleUpAnimation(Landroid/view/View;IIII)Landroid/app/ActivityOptions;
invokespecial android/support/v4/app/ae/<init>(Landroid/app/ActivityOptions;)V
areturn
.limit locals 5
.limit stack 7
.end method

.method private static a(Landroid/view/View;Landroid/graphics/Bitmap;II)Landroid/support/v4/app/ae;
new android/support/v4/app/ae
dup
aload 0
aload 1
iload 2
iload 3
invokestatic android/app/ActivityOptions/makeThumbnailScaleUpAnimation(Landroid/view/View;Landroid/graphics/Bitmap;II)Landroid/app/ActivityOptions;
invokespecial android/support/v4/app/ae/<init>(Landroid/app/ActivityOptions;)V
areturn
.limit locals 4
.limit stack 6
.end method

.method private a(Landroid/support/v4/app/ae;)V
aload 0
getfield android/support/v4/app/ae/a Landroid/app/ActivityOptions;
aload 1
getfield android/support/v4/app/ae/a Landroid/app/ActivityOptions;
invokevirtual android/app/ActivityOptions/update(Landroid/app/ActivityOptions;)V
return
.limit locals 2
.limit stack 2
.end method
