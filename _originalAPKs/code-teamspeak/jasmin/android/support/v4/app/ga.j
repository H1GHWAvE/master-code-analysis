.bytecode 50.0
.class public final synchronized android/support/v4/app/ga
.super java/lang/Object

.field public static final 'a' Ljava/lang/String; = "android.support.v4.app.EXTRA_CALLING_PACKAGE"

.field public static final 'b' Ljava/lang/String; = "android.support.v4.app.EXTRA_CALLING_ACTIVITY"

.field private static 'c' Landroid/support/v4/app/gd;

.method static <clinit>()V
getstatic android/os/Build$VERSION/SDK_INT I
bipush 16
if_icmplt L0
new android/support/v4/app/gg
dup
invokespecial android/support/v4/app/gg/<init>()V
putstatic android/support/v4/app/ga/c Landroid/support/v4/app/gd;
return
L0:
getstatic android/os/Build$VERSION/SDK_INT I
bipush 14
if_icmplt L1
new android/support/v4/app/gf
dup
invokespecial android/support/v4/app/gf/<init>()V
putstatic android/support/v4/app/ga/c Landroid/support/v4/app/gd;
return
L1:
new android/support/v4/app/ge
dup
invokespecial android/support/v4/app/ge/<init>()V
putstatic android/support/v4/app/ga/c Landroid/support/v4/app/gd;
return
.limit locals 0
.limit stack 2
.end method

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method static synthetic a()Landroid/support/v4/app/gd;
getstatic android/support/v4/app/ga/c Landroid/support/v4/app/gd;
areturn
.limit locals 0
.limit stack 1
.end method

.method public static a(Landroid/app/Activity;)Ljava/lang/String;
aload 0
invokevirtual android/app/Activity/getCallingPackage()Ljava/lang/String;
astore 2
aload 2
astore 1
aload 2
ifnonnull L0
aload 0
invokevirtual android/app/Activity/getIntent()Landroid/content/Intent;
ldc "android.support.v4.app.EXTRA_CALLING_PACKAGE"
invokevirtual android/content/Intent/getStringExtra(Ljava/lang/String;)Ljava/lang/String;
astore 1
L0:
aload 1
areturn
.limit locals 3
.limit stack 2
.end method

.method private static a(Landroid/view/Menu;ILandroid/support/v4/app/gb;)V
aload 0
iload 1
invokeinterface android/view/Menu/findItem(I)Landroid/view/MenuItem; 1
astore 0
aload 0
ifnonnull L0
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
ldc "Could not find menu item with id "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
iload 1
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc " in the supplied menu"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
getstatic android/support/v4/app/ga/c Landroid/support/v4/app/gd;
aload 0
aload 2
invokeinterface android/support/v4/app/gd/a(Landroid/view/MenuItem;Landroid/support/v4/app/gb;)V 2
return
.limit locals 3
.limit stack 5
.end method

.method private static a(Landroid/view/MenuItem;Landroid/support/v4/app/gb;)V
getstatic android/support/v4/app/ga/c Landroid/support/v4/app/gd;
aload 0
aload 1
invokeinterface android/support/v4/app/gd/a(Landroid/view/MenuItem;Landroid/support/v4/app/gb;)V 2
return
.limit locals 2
.limit stack 3
.end method

.method public static b(Landroid/app/Activity;)Landroid/content/ComponentName;
aload 0
invokevirtual android/app/Activity/getCallingActivity()Landroid/content/ComponentName;
astore 2
aload 2
astore 1
aload 2
ifnonnull L0
aload 0
invokevirtual android/app/Activity/getIntent()Landroid/content/Intent;
ldc "android.support.v4.app.EXTRA_CALLING_ACTIVITY"
invokevirtual android/content/Intent/getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;
checkcast android/content/ComponentName
astore 1
L0:
aload 1
areturn
.limit locals 3
.limit stack 2
.end method
