.bytecode 50.0
.class public final synchronized android/support/v4/n/f
.super java/lang/Object

.field static final 'a' [I

.field static final 'b' [J

.field static final 'c' [Ljava/lang/Object;

.method static <clinit>()V
iconst_0
newarray int
putstatic android/support/v4/n/f/a [I
iconst_0
newarray long
putstatic android/support/v4/n/f/b [J
iconst_0
anewarray java/lang/Object
putstatic android/support/v4/n/f/c [Ljava/lang/Object;
return
.limit locals 0
.limit stack 1
.end method

.method <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static a(I)I
iload 0
iconst_4
imul
invokestatic android/support/v4/n/f/c(I)I
iconst_4
idiv
ireturn
.limit locals 1
.limit stack 2
.end method

.method public static a([III)I
iload 1
iconst_1
isub
istore 3
iconst_0
istore 1
L0:
iload 1
iload 3
if_icmpgt L1
iload 1
iload 3
iadd
iconst_1
iushr
istore 4
aload 0
iload 4
iaload
istore 5
iload 5
iload 2
if_icmpge L2
iload 4
iconst_1
iadd
istore 1
goto L0
L2:
iload 4
istore 3
iload 5
iload 2
if_icmple L3
iload 4
iconst_1
isub
istore 3
goto L0
L1:
iload 1
iconst_m1
ixor
istore 3
L3:
iload 3
ireturn
.limit locals 6
.limit stack 2
.end method

.method static a([JIJ)I
iload 1
iconst_1
isub
istore 4
iconst_0
istore 1
L0:
iload 1
iload 4
if_icmpgt L1
iload 1
iload 4
iadd
iconst_1
iushr
istore 5
aload 0
iload 5
laload
lstore 6
lload 6
lload 2
lcmp
ifge L2
iload 5
iconst_1
iadd
istore 1
goto L0
L2:
iload 5
istore 4
lload 6
lload 2
lcmp
ifle L3
iload 5
iconst_1
isub
istore 4
goto L0
L1:
iload 1
iconst_m1
ixor
istore 4
L3:
iload 4
ireturn
.limit locals 8
.limit stack 4
.end method

.method public static a(Ljava/lang/Object;Ljava/lang/Object;)Z
aload 0
aload 1
if_acmpeq L0
aload 0
ifnull L1
aload 0
aload 1
invokevirtual java/lang/Object/equals(Ljava/lang/Object;)Z
ifeq L1
L0:
iconst_1
ireturn
L1:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method public static b(I)I
iload 0
bipush 8
imul
invokestatic android/support/v4/n/f/c(I)I
bipush 8
idiv
ireturn
.limit locals 1
.limit stack 2
.end method

.method private static c(I)I
iconst_4
istore 1
L0:
iload 0
istore 2
iload 1
bipush 32
if_icmpge L1
iload 0
iconst_1
iload 1
ishl
bipush 12
isub
if_icmpgt L2
iconst_1
iload 1
ishl
bipush 12
isub
istore 2
L1:
iload 2
ireturn
L2:
iload 1
iconst_1
iadd
istore 1
goto L0
.limit locals 3
.limit stack 3
.end method
