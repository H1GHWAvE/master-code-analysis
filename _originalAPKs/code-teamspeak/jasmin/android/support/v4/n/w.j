.bytecode 50.0
.class public final synchronized android/support/v4/n/w
.super java/lang/Object
.implements java/lang/Cloneable

.field public static final 'a' Ljava/lang/Object;

.field public 'b' Z

.field public 'c' [I

.field public 'd' [Ljava/lang/Object;

.field public 'e' I

.method static <clinit>()V
new java/lang/Object
dup
invokespecial java/lang/Object/<init>()V
putstatic android/support/v4/n/w/a Ljava/lang/Object;
return
.limit locals 0
.limit stack 2
.end method

.method public <init>()V
aload 0
iconst_0
invokespecial android/support/v4/n/w/<init>(B)V
return
.limit locals 1
.limit stack 2
.end method

.method private <init>(B)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
iconst_0
putfield android/support/v4/n/w/b Z
bipush 10
invokestatic android/support/v4/n/f/a(I)I
istore 1
aload 0
iload 1
newarray int
putfield android/support/v4/n/w/c [I
aload 0
iload 1
anewarray java/lang/Object
putfield android/support/v4/n/w/d [Ljava/lang/Object;
aload 0
iconst_0
putfield android/support/v4/n/w/e I
return
.limit locals 2
.limit stack 2
.end method

.method private a(Ljava/lang/Object;)I
aload 0
getfield android/support/v4/n/w/b Z
ifeq L0
aload 0
invokespecial android/support/v4/n/w/d()V
L0:
iconst_0
istore 2
L1:
iload 2
aload 0
getfield android/support/v4/n/w/e I
if_icmpge L2
aload 0
getfield android/support/v4/n/w/d [Ljava/lang/Object;
iload 2
aaload
aload 1
if_acmpne L3
iload 2
ireturn
L3:
iload 2
iconst_1
iadd
istore 2
goto L1
L2:
iconst_m1
ireturn
.limit locals 3
.limit stack 2
.end method

.method private a(II)V
aload 0
getfield android/support/v4/n/w/e I
iload 1
iload 2
iadd
invokestatic java/lang/Math/min(II)I
istore 2
L0:
iload 1
iload 2
if_icmpge L1
aload 0
iload 1
invokevirtual android/support/v4/n/w/b(I)V
iload 1
iconst_1
iadd
istore 1
goto L0
L1:
return
.limit locals 3
.limit stack 3
.end method

.method private b(ILjava/lang/Object;)V
aload 0
getfield android/support/v4/n/w/b Z
ifeq L0
aload 0
invokespecial android/support/v4/n/w/d()V
L0:
aload 0
getfield android/support/v4/n/w/d [Ljava/lang/Object;
iload 1
aload 2
aastore
return
.limit locals 3
.limit stack 3
.end method

.method private c()Landroid/support/v4/n/w;
.catch java/lang/CloneNotSupportedException from L0 to L1 using L2
.catch java/lang/CloneNotSupportedException from L1 to L3 using L4
L0:
aload 0
invokespecial java/lang/Object/clone()Ljava/lang/Object;
checkcast android/support/v4/n/w
astore 1
L1:
aload 1
aload 0
getfield android/support/v4/n/w/c [I
invokevirtual [I/clone()Ljava/lang/Object;
checkcast [I
putfield android/support/v4/n/w/c [I
aload 1
aload 0
getfield android/support/v4/n/w/d [Ljava/lang/Object;
invokevirtual [Ljava/lang/Object;/clone()Ljava/lang/Object;
checkcast [Ljava/lang/Object;
putfield android/support/v4/n/w/d [Ljava/lang/Object;
L3:
aload 1
areturn
L2:
astore 1
aconst_null
areturn
L4:
astore 2
aload 1
areturn
.limit locals 3
.limit stack 2
.end method

.method private c(ILjava/lang/Object;)V
aload 0
getfield android/support/v4/n/w/e I
ifeq L0
iload 1
aload 0
getfield android/support/v4/n/w/c [I
aload 0
getfield android/support/v4/n/w/e I
iconst_1
isub
iaload
if_icmpgt L0
aload 0
iload 1
aload 2
invokevirtual android/support/v4/n/w/a(ILjava/lang/Object;)V
return
L0:
aload 0
getfield android/support/v4/n/w/b Z
ifeq L1
aload 0
getfield android/support/v4/n/w/e I
aload 0
getfield android/support/v4/n/w/c [I
arraylength
if_icmplt L1
aload 0
invokespecial android/support/v4/n/w/d()V
L1:
aload 0
getfield android/support/v4/n/w/e I
istore 3
iload 3
aload 0
getfield android/support/v4/n/w/c [I
arraylength
if_icmplt L2
iload 3
iconst_1
iadd
invokestatic android/support/v4/n/f/a(I)I
istore 4
iload 4
newarray int
astore 5
iload 4
anewarray java/lang/Object
astore 6
aload 0
getfield android/support/v4/n/w/c [I
iconst_0
aload 5
iconst_0
aload 0
getfield android/support/v4/n/w/c [I
arraylength
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 0
getfield android/support/v4/n/w/d [Ljava/lang/Object;
iconst_0
aload 6
iconst_0
aload 0
getfield android/support/v4/n/w/d [Ljava/lang/Object;
arraylength
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 0
aload 5
putfield android/support/v4/n/w/c [I
aload 0
aload 6
putfield android/support/v4/n/w/d [Ljava/lang/Object;
L2:
aload 0
getfield android/support/v4/n/w/c [I
iload 3
iload 1
iastore
aload 0
getfield android/support/v4/n/w/d [Ljava/lang/Object;
iload 3
aload 2
aastore
aload 0
iload 3
iconst_1
iadd
putfield android/support/v4/n/w/e I
return
.limit locals 7
.limit stack 5
.end method

.method private d()V
aload 0
getfield android/support/v4/n/w/e I
istore 4
aload 0
getfield android/support/v4/n/w/c [I
astore 5
aload 0
getfield android/support/v4/n/w/d [Ljava/lang/Object;
astore 6
iconst_0
istore 1
iconst_0
istore 2
L0:
iload 1
iload 4
if_icmpge L1
aload 6
iload 1
aaload
astore 7
iload 2
istore 3
aload 7
getstatic android/support/v4/n/w/a Ljava/lang/Object;
if_acmpeq L2
iload 1
iload 2
if_icmpeq L3
aload 5
iload 2
aload 5
iload 1
iaload
iastore
aload 6
iload 2
aload 7
aastore
aload 6
iload 1
aconst_null
aastore
L3:
iload 2
iconst_1
iadd
istore 3
L2:
iload 1
iconst_1
iadd
istore 1
iload 3
istore 2
goto L0
L1:
aload 0
iconst_0
putfield android/support/v4/n/w/b Z
aload 0
iload 2
putfield android/support/v4/n/w/e I
return
.limit locals 8
.limit stack 4
.end method

.method private f(I)Ljava/lang/Object;
aload 0
getfield android/support/v4/n/w/c [I
aload 0
getfield android/support/v4/n/w/e I
iload 1
invokestatic android/support/v4/n/f/a([III)I
istore 1
iload 1
iflt L0
aload 0
getfield android/support/v4/n/w/d [Ljava/lang/Object;
iload 1
aaload
getstatic android/support/v4/n/w/a Ljava/lang/Object;
if_acmpne L1
L0:
aconst_null
areturn
L1:
aload 0
getfield android/support/v4/n/w/d [Ljava/lang/Object;
iload 1
aaload
areturn
.limit locals 2
.limit stack 3
.end method

.method private g(I)V
aload 0
getfield android/support/v4/n/w/c [I
aload 0
getfield android/support/v4/n/w/e I
iload 1
invokestatic android/support/v4/n/f/a([III)I
istore 1
iload 1
iflt L0
aload 0
getfield android/support/v4/n/w/d [Ljava/lang/Object;
iload 1
aaload
getstatic android/support/v4/n/w/a Ljava/lang/Object;
if_acmpeq L0
aload 0
getfield android/support/v4/n/w/d [Ljava/lang/Object;
iload 1
getstatic android/support/v4/n/w/a Ljava/lang/Object;
aastore
aload 0
iconst_1
putfield android/support/v4/n/w/b Z
L0:
return
.limit locals 2
.limit stack 3
.end method

.method private h(I)V
aload 0
getfield android/support/v4/n/w/c [I
aload 0
getfield android/support/v4/n/w/e I
iload 1
invokestatic android/support/v4/n/f/a([III)I
istore 1
iload 1
iflt L0
aload 0
getfield android/support/v4/n/w/d [Ljava/lang/Object;
iload 1
aaload
getstatic android/support/v4/n/w/a Ljava/lang/Object;
if_acmpeq L0
aload 0
getfield android/support/v4/n/w/d [Ljava/lang/Object;
iload 1
getstatic android/support/v4/n/w/a Ljava/lang/Object;
aastore
aload 0
iconst_1
putfield android/support/v4/n/w/b Z
L0:
return
.limit locals 2
.limit stack 3
.end method

.method public final a()I
aload 0
getfield android/support/v4/n/w/b Z
ifeq L0
aload 0
invokespecial android/support/v4/n/w/d()V
L0:
aload 0
getfield android/support/v4/n/w/e I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final a(I)Ljava/lang/Object;
aload 0
getfield android/support/v4/n/w/c [I
aload 0
getfield android/support/v4/n/w/e I
iload 1
invokestatic android/support/v4/n/f/a([III)I
istore 1
iload 1
iflt L0
aload 0
getfield android/support/v4/n/w/d [Ljava/lang/Object;
iload 1
aaload
getstatic android/support/v4/n/w/a Ljava/lang/Object;
if_acmpne L1
L0:
aconst_null
areturn
L1:
aload 0
getfield android/support/v4/n/w/d [Ljava/lang/Object;
iload 1
aaload
areturn
.limit locals 2
.limit stack 3
.end method

.method public final a(ILjava/lang/Object;)V
aload 0
getfield android/support/v4/n/w/c [I
aload 0
getfield android/support/v4/n/w/e I
iload 1
invokestatic android/support/v4/n/f/a([III)I
istore 3
iload 3
iflt L0
aload 0
getfield android/support/v4/n/w/d [Ljava/lang/Object;
iload 3
aload 2
aastore
return
L0:
iload 3
iconst_m1
ixor
istore 4
iload 4
aload 0
getfield android/support/v4/n/w/e I
if_icmpge L1
aload 0
getfield android/support/v4/n/w/d [Ljava/lang/Object;
iload 4
aaload
getstatic android/support/v4/n/w/a Ljava/lang/Object;
if_acmpne L1
aload 0
getfield android/support/v4/n/w/c [I
iload 4
iload 1
iastore
aload 0
getfield android/support/v4/n/w/d [Ljava/lang/Object;
iload 4
aload 2
aastore
return
L1:
iload 4
istore 3
aload 0
getfield android/support/v4/n/w/b Z
ifeq L2
iload 4
istore 3
aload 0
getfield android/support/v4/n/w/e I
aload 0
getfield android/support/v4/n/w/c [I
arraylength
if_icmplt L2
aload 0
invokespecial android/support/v4/n/w/d()V
aload 0
getfield android/support/v4/n/w/c [I
aload 0
getfield android/support/v4/n/w/e I
iload 1
invokestatic android/support/v4/n/f/a([III)I
iconst_m1
ixor
istore 3
L2:
aload 0
getfield android/support/v4/n/w/e I
aload 0
getfield android/support/v4/n/w/c [I
arraylength
if_icmplt L3
aload 0
getfield android/support/v4/n/w/e I
iconst_1
iadd
invokestatic android/support/v4/n/f/a(I)I
istore 4
iload 4
newarray int
astore 5
iload 4
anewarray java/lang/Object
astore 6
aload 0
getfield android/support/v4/n/w/c [I
iconst_0
aload 5
iconst_0
aload 0
getfield android/support/v4/n/w/c [I
arraylength
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 0
getfield android/support/v4/n/w/d [Ljava/lang/Object;
iconst_0
aload 6
iconst_0
aload 0
getfield android/support/v4/n/w/d [Ljava/lang/Object;
arraylength
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 0
aload 5
putfield android/support/v4/n/w/c [I
aload 0
aload 6
putfield android/support/v4/n/w/d [Ljava/lang/Object;
L3:
aload 0
getfield android/support/v4/n/w/e I
iload 3
isub
ifeq L4
aload 0
getfield android/support/v4/n/w/c [I
iload 3
aload 0
getfield android/support/v4/n/w/c [I
iload 3
iconst_1
iadd
aload 0
getfield android/support/v4/n/w/e I
iload 3
isub
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 0
getfield android/support/v4/n/w/d [Ljava/lang/Object;
iload 3
aload 0
getfield android/support/v4/n/w/d [Ljava/lang/Object;
iload 3
iconst_1
iadd
aload 0
getfield android/support/v4/n/w/e I
iload 3
isub
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
L4:
aload 0
getfield android/support/v4/n/w/c [I
iload 3
iload 1
iastore
aload 0
getfield android/support/v4/n/w/d [Ljava/lang/Object;
iload 3
aload 2
aastore
aload 0
aload 0
getfield android/support/v4/n/w/e I
iconst_1
iadd
putfield android/support/v4/n/w/e I
return
.limit locals 7
.limit stack 6
.end method

.method public final b()V
aload 0
getfield android/support/v4/n/w/e I
istore 2
aload 0
getfield android/support/v4/n/w/d [Ljava/lang/Object;
astore 3
iconst_0
istore 1
L0:
iload 1
iload 2
if_icmpge L1
aload 3
iload 1
aconst_null
aastore
iload 1
iconst_1
iadd
istore 1
goto L0
L1:
aload 0
iconst_0
putfield android/support/v4/n/w/e I
aload 0
iconst_0
putfield android/support/v4/n/w/b Z
return
.limit locals 4
.limit stack 3
.end method

.method public final b(I)V
aload 0
getfield android/support/v4/n/w/d [Ljava/lang/Object;
iload 1
aaload
getstatic android/support/v4/n/w/a Ljava/lang/Object;
if_acmpeq L0
aload 0
getfield android/support/v4/n/w/d [Ljava/lang/Object;
iload 1
getstatic android/support/v4/n/w/a Ljava/lang/Object;
aastore
aload 0
iconst_1
putfield android/support/v4/n/w/b Z
L0:
return
.limit locals 2
.limit stack 3
.end method

.method public final c(I)I
aload 0
getfield android/support/v4/n/w/b Z
ifeq L0
aload 0
invokespecial android/support/v4/n/w/d()V
L0:
aload 0
getfield android/support/v4/n/w/c [I
iload 1
iaload
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final synthetic clone()Ljava/lang/Object;
aload 0
invokespecial android/support/v4/n/w/c()Landroid/support/v4/n/w;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final d(I)Ljava/lang/Object;
aload 0
getfield android/support/v4/n/w/b Z
ifeq L0
aload 0
invokespecial android/support/v4/n/w/d()V
L0:
aload 0
getfield android/support/v4/n/w/d [Ljava/lang/Object;
iload 1
aaload
areturn
.limit locals 2
.limit stack 2
.end method

.method public final e(I)I
aload 0
getfield android/support/v4/n/w/b Z
ifeq L0
aload 0
invokespecial android/support/v4/n/w/d()V
L0:
aload 0
getfield android/support/v4/n/w/c [I
aload 0
getfield android/support/v4/n/w/e I
iload 1
invokestatic android/support/v4/n/f/a([III)I
ireturn
.limit locals 2
.limit stack 3
.end method

.method public final toString()Ljava/lang/String;
aload 0
invokevirtual android/support/v4/n/w/a()I
ifgt L0
ldc "{}"
areturn
L0:
new java/lang/StringBuilder
dup
aload 0
getfield android/support/v4/n/w/e I
bipush 28
imul
invokespecial java/lang/StringBuilder/<init>(I)V
astore 2
aload 2
bipush 123
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
pop
iconst_0
istore 1
L1:
iload 1
aload 0
getfield android/support/v4/n/w/e I
if_icmpge L2
iload 1
ifle L3
aload 2
ldc ", "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
L3:
aload 2
aload 0
iload 1
invokevirtual android/support/v4/n/w/c(I)I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
pop
aload 2
bipush 61
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
pop
aload 0
iload 1
invokevirtual android/support/v4/n/w/d(I)Ljava/lang/Object;
astore 3
aload 3
aload 0
if_acmpeq L4
aload 2
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
pop
L5:
iload 1
iconst_1
iadd
istore 1
goto L1
L4:
aload 2
ldc "(this Map)"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
goto L5
L2:
aload 2
bipush 125
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
pop
aload 2
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 4
.limit stack 4
.end method
