.bytecode 50.0
.class public synchronized abstract android/support/v4/n/k
.super java/lang/Object

.field 'b' Landroid/support/v4/n/m;

.field 'c' Landroid/support/v4/n/n;

.field 'd' Landroid/support/v4/n/p;

.method <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static a(Ljava/util/Map;Ljava/util/Collection;)Z
aload 1
invokeinterface java/util/Collection/iterator()Ljava/util/Iterator; 0
astore 1
L0:
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 0
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
invokeinterface java/util/Map/containsKey(Ljava/lang/Object;)Z 1
ifne L0
iconst_0
ireturn
L1:
iconst_1
ireturn
.limit locals 2
.limit stack 2
.end method

.method public static a(Ljava/util/Set;Ljava/lang/Object;)Z
.catch java/lang/NullPointerException from L0 to L1 using L2
.catch java/lang/ClassCastException from L0 to L1 using L3
aload 0
aload 1
if_acmpne L4
L5:
iconst_1
ireturn
L4:
aload 1
instanceof java/util/Set
ifeq L6
aload 1
checkcast java/util/Set
astore 1
L0:
aload 0
invokeinterface java/util/Set/size()I 0
aload 1
invokeinterface java/util/Set/size()I 0
if_icmpne L7
aload 0
aload 1
invokeinterface java/util/Set/containsAll(Ljava/util/Collection;)Z 1
istore 2
L1:
iload 2
ifne L5
L7:
iconst_0
ireturn
L2:
astore 0
iconst_0
ireturn
L3:
astore 0
iconst_0
ireturn
L6:
iconst_0
ireturn
.limit locals 3
.limit stack 2
.end method

.method public static b(Ljava/util/Map;Ljava/util/Collection;)Z
aload 0
invokeinterface java/util/Map/size()I 0
istore 2
aload 1
invokeinterface java/util/Collection/iterator()Ljava/util/Iterator; 0
astore 1
L0:
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 0
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
invokeinterface java/util/Map/remove(Ljava/lang/Object;)Ljava/lang/Object; 1
pop
goto L0
L1:
iload 2
aload 0
invokeinterface java/util/Map/size()I 0
if_icmpeq L2
iconst_1
ireturn
L2:
iconst_0
ireturn
.limit locals 3
.limit stack 2
.end method

.method public static c(Ljava/util/Map;Ljava/util/Collection;)Z
aload 0
invokeinterface java/util/Map/size()I 0
istore 2
aload 0
invokeinterface java/util/Map/keySet()Ljava/util/Set; 0
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
astore 3
L0:
aload 3
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 1
aload 3
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
invokeinterface java/util/Collection/contains(Ljava/lang/Object;)Z 1
ifne L0
aload 3
invokeinterface java/util/Iterator/remove()V 0
goto L0
L1:
iload 2
aload 0
invokeinterface java/util/Map/size()I 0
if_icmpeq L2
iconst_1
ireturn
L2:
iconst_0
ireturn
.limit locals 4
.limit stack 2
.end method

.method private d()Ljava/util/Set;
aload 0
getfield android/support/v4/n/k/b Landroid/support/v4/n/m;
ifnonnull L0
aload 0
new android/support/v4/n/m
dup
aload 0
invokespecial android/support/v4/n/m/<init>(Landroid/support/v4/n/k;)V
putfield android/support/v4/n/k/b Landroid/support/v4/n/m;
L0:
aload 0
getfield android/support/v4/n/k/b Landroid/support/v4/n/m;
areturn
.limit locals 1
.limit stack 4
.end method

.method private e()Ljava/util/Set;
aload 0
getfield android/support/v4/n/k/c Landroid/support/v4/n/n;
ifnonnull L0
aload 0
new android/support/v4/n/n
dup
aload 0
invokespecial android/support/v4/n/n/<init>(Landroid/support/v4/n/k;)V
putfield android/support/v4/n/k/c Landroid/support/v4/n/n;
L0:
aload 0
getfield android/support/v4/n/k/c Landroid/support/v4/n/n;
areturn
.limit locals 1
.limit stack 4
.end method

.method private f()Ljava/util/Collection;
aload 0
getfield android/support/v4/n/k/d Landroid/support/v4/n/p;
ifnonnull L0
aload 0
new android/support/v4/n/p
dup
aload 0
invokespecial android/support/v4/n/p/<init>(Landroid/support/v4/n/k;)V
putfield android/support/v4/n/k/d Landroid/support/v4/n/p;
L0:
aload 0
getfield android/support/v4/n/k/d Landroid/support/v4/n/p;
areturn
.limit locals 1
.limit stack 4
.end method

.method protected abstract a()I
.end method

.method protected abstract a(Ljava/lang/Object;)I
.end method

.method protected abstract a(II)Ljava/lang/Object;
.end method

.method protected abstract a(ILjava/lang/Object;)Ljava/lang/Object;
.end method

.method protected abstract a(I)V
.end method

.method protected abstract a(Ljava/lang/Object;Ljava/lang/Object;)V
.end method

.method public final a([Ljava/lang/Object;I)[Ljava/lang/Object;
aload 0
invokevirtual android/support/v4/n/k/a()I
istore 4
aload 1
arraylength
iload 4
if_icmpge L0
aload 1
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
invokevirtual java/lang/Class/getComponentType()Ljava/lang/Class;
iload 4
invokestatic java/lang/reflect/Array/newInstance(Ljava/lang/Class;I)Ljava/lang/Object;
checkcast [Ljava/lang/Object;
checkcast [Ljava/lang/Object;
astore 1
L1:
iconst_0
istore 3
L2:
iload 3
iload 4
if_icmpge L3
aload 1
iload 3
aload 0
iload 3
iload 2
invokevirtual android/support/v4/n/k/a(II)Ljava/lang/Object;
aastore
iload 3
iconst_1
iadd
istore 3
goto L2
L3:
aload 1
arraylength
iload 4
if_icmple L4
aload 1
iload 4
aconst_null
aastore
L4:
aload 1
areturn
L0:
goto L1
.limit locals 5
.limit stack 5
.end method

.method protected abstract b(Ljava/lang/Object;)I
.end method

.method protected abstract b()Ljava/util/Map;
.end method

.method public final b(I)[Ljava/lang/Object;
aload 0
invokevirtual android/support/v4/n/k/a()I
istore 3
iload 3
anewarray java/lang/Object
astore 4
iconst_0
istore 2
L0:
iload 2
iload 3
if_icmpge L1
aload 4
iload 2
aload 0
iload 2
iload 1
invokevirtual android/support/v4/n/k/a(II)Ljava/lang/Object;
aastore
iload 2
iconst_1
iadd
istore 2
goto L0
L1:
aload 4
areturn
.limit locals 5
.limit stack 5
.end method

.method protected abstract c()V
.end method
