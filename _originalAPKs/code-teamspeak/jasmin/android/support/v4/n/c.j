.bytecode 50.0
.class public final synchronized android/support/v4/n/c
.super java/lang/Object

.field private final 'a' Ljava/io/File;

.field private final 'b' Ljava/io/File;

.method private <init>(Ljava/io/File;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
putfield android/support/v4/n/c/a Ljava/io/File;
aload 0
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 1
invokevirtual java/io/File/getPath()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ".bak"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
putfield android/support/v4/n/c/b Ljava/io/File;
return
.limit locals 2
.limit stack 5
.end method

.method private a()Ljava/io/File;
aload 0
getfield android/support/v4/n/c/a Ljava/io/File;
areturn
.limit locals 1
.limit stack 1
.end method

.method private a(Ljava/io/FileOutputStream;)V
.catch java/io/IOException from L0 to L1 using L2
aload 1
ifnull L1
aload 1
invokestatic android/support/v4/n/c/c(Ljava/io/FileOutputStream;)Z
pop
L0:
aload 1
invokevirtual java/io/FileOutputStream/close()V
aload 0
getfield android/support/v4/n/c/b Ljava/io/File;
invokevirtual java/io/File/delete()Z
pop
L1:
return
L2:
astore 1
ldc "AtomicFile"
ldc "finishWrite: Got exception:"
aload 1
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
return
.limit locals 2
.limit stack 3
.end method

.method private b()V
aload 0
getfield android/support/v4/n/c/a Ljava/io/File;
invokevirtual java/io/File/delete()Z
pop
aload 0
getfield android/support/v4/n/c/b Ljava/io/File;
invokevirtual java/io/File/delete()Z
pop
return
.limit locals 1
.limit stack 1
.end method

.method private b(Ljava/io/FileOutputStream;)V
.catch java/io/IOException from L0 to L1 using L2
aload 1
ifnull L1
aload 1
invokestatic android/support/v4/n/c/c(Ljava/io/FileOutputStream;)Z
pop
L0:
aload 1
invokevirtual java/io/FileOutputStream/close()V
aload 0
getfield android/support/v4/n/c/a Ljava/io/File;
invokevirtual java/io/File/delete()Z
pop
aload 0
getfield android/support/v4/n/c/b Ljava/io/File;
aload 0
getfield android/support/v4/n/c/a Ljava/io/File;
invokevirtual java/io/File/renameTo(Ljava/io/File;)Z
pop
L1:
return
L2:
astore 1
ldc "AtomicFile"
ldc "failWrite: Got exception:"
aload 1
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
return
.limit locals 2
.limit stack 3
.end method

.method private c()Ljava/io/FileOutputStream;
.catch java/io/FileNotFoundException from L0 to L1 using L2
.catch java/io/FileNotFoundException from L3 to L4 using L5
aload 0
getfield android/support/v4/n/c/a Ljava/io/File;
invokevirtual java/io/File/exists()Z
ifeq L0
aload 0
getfield android/support/v4/n/c/b Ljava/io/File;
invokevirtual java/io/File/exists()Z
ifne L6
aload 0
getfield android/support/v4/n/c/a Ljava/io/File;
aload 0
getfield android/support/v4/n/c/b Ljava/io/File;
invokevirtual java/io/File/renameTo(Ljava/io/File;)Z
ifne L0
ldc "AtomicFile"
new java/lang/StringBuilder
dup
ldc "Couldn't rename file "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
getfield android/support/v4/n/c/a Ljava/io/File;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc " to backup file "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield android/support/v4/n/c/b Ljava/io/File;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/String;)I
pop
L0:
new java/io/FileOutputStream
dup
aload 0
getfield android/support/v4/n/c/a Ljava/io/File;
invokespecial java/io/FileOutputStream/<init>(Ljava/io/File;)V
astore 1
L1:
aload 1
areturn
L6:
aload 0
getfield android/support/v4/n/c/a Ljava/io/File;
invokevirtual java/io/File/delete()Z
pop
goto L0
L2:
astore 1
aload 0
getfield android/support/v4/n/c/a Ljava/io/File;
invokevirtual java/io/File/getParentFile()Ljava/io/File;
invokevirtual java/io/File/mkdir()Z
ifne L3
new java/io/IOException
dup
new java/lang/StringBuilder
dup
ldc "Couldn't create directory "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
getfield android/support/v4/n/c/a Ljava/io/File;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/IOException/<init>(Ljava/lang/String;)V
athrow
L3:
new java/io/FileOutputStream
dup
aload 0
getfield android/support/v4/n/c/a Ljava/io/File;
invokespecial java/io/FileOutputStream/<init>(Ljava/io/File;)V
astore 1
L4:
aload 1
areturn
L5:
astore 1
new java/io/IOException
dup
new java/lang/StringBuilder
dup
ldc "Couldn't create "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
getfield android/support/v4/n/c/a Ljava/io/File;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/IOException/<init>(Ljava/lang/String;)V
athrow
.limit locals 2
.limit stack 5
.end method

.method private static c(Ljava/io/FileOutputStream;)Z
.catch java/io/IOException from L0 to L1 using L2
aload 0
ifnull L1
L0:
aload 0
invokevirtual java/io/FileOutputStream/getFD()Ljava/io/FileDescriptor;
invokevirtual java/io/FileDescriptor/sync()V
L1:
iconst_1
ireturn
L2:
astore 0
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method private d()Ljava/io/FileInputStream;
aload 0
getfield android/support/v4/n/c/b Ljava/io/File;
invokevirtual java/io/File/exists()Z
ifeq L0
aload 0
getfield android/support/v4/n/c/a Ljava/io/File;
invokevirtual java/io/File/delete()Z
pop
aload 0
getfield android/support/v4/n/c/b Ljava/io/File;
aload 0
getfield android/support/v4/n/c/a Ljava/io/File;
invokevirtual java/io/File/renameTo(Ljava/io/File;)Z
pop
L0:
new java/io/FileInputStream
dup
aload 0
getfield android/support/v4/n/c/a Ljava/io/File;
invokespecial java/io/FileInputStream/<init>(Ljava/io/File;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private e()[B
.catch all from L0 to L1 using L2
.catch all from L1 to L3 using L2
.catch all from L4 to L5 using L2
iconst_0
istore 1
aload 0
getfield android/support/v4/n/c/b Ljava/io/File;
invokevirtual java/io/File/exists()Z
ifeq L6
aload 0
getfield android/support/v4/n/c/a Ljava/io/File;
invokevirtual java/io/File/delete()Z
pop
aload 0
getfield android/support/v4/n/c/b Ljava/io/File;
aload 0
getfield android/support/v4/n/c/a Ljava/io/File;
invokevirtual java/io/File/renameTo(Ljava/io/File;)Z
pop
L6:
new java/io/FileInputStream
dup
aload 0
getfield android/support/v4/n/c/a Ljava/io/File;
invokespecial java/io/FileInputStream/<init>(Ljava/io/File;)V
astore 5
L0:
aload 5
invokevirtual java/io/FileInputStream/available()I
newarray byte
astore 3
L1:
aload 5
aload 3
iload 1
aload 3
arraylength
iload 1
isub
invokevirtual java/io/FileInputStream/read([BII)I
istore 2
L3:
iload 2
ifgt L7
aload 5
invokevirtual java/io/FileInputStream/close()V
aload 3
areturn
L7:
iload 2
iload 1
iadd
istore 1
L4:
aload 5
invokevirtual java/io/FileInputStream/available()I
istore 2
iload 2
aload 3
arraylength
iload 1
isub
if_icmple L8
iload 2
iload 1
iadd
newarray byte
astore 4
aload 3
iconst_0
aload 4
iconst_0
iload 1
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
L5:
aload 4
astore 3
L9:
goto L1
L2:
astore 3
aload 5
invokevirtual java/io/FileInputStream/close()V
aload 3
athrow
L8:
goto L9
.limit locals 6
.limit stack 5
.end method
