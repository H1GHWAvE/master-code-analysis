.bytecode 50.0
.class final synchronized android/support/v4/view/ap
.super java/lang/Object

.field private static final 'a' Ljava/lang/String; = "LayoutInflaterCompatHC"

.field private static 'b' Ljava/lang/reflect/Field;

.field private static 'c' Z

.method <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a(Landroid/view/LayoutInflater;Landroid/support/v4/view/as;)V
aload 1
ifnull L0
new android/support/v4/view/aq
dup
aload 1
invokespecial android/support/v4/view/aq/<init>(Landroid/support/v4/view/as;)V
astore 1
L1:
aload 0
aload 1
invokevirtual android/view/LayoutInflater/setFactory2(Landroid/view/LayoutInflater$Factory2;)V
aload 0
invokevirtual android/view/LayoutInflater/getFactory()Landroid/view/LayoutInflater$Factory;
astore 2
aload 2
instanceof android/view/LayoutInflater$Factory2
ifeq L2
aload 0
aload 2
checkcast android/view/LayoutInflater$Factory2
invokestatic android/support/v4/view/ap/a(Landroid/view/LayoutInflater;Landroid/view/LayoutInflater$Factory2;)V
return
L0:
aconst_null
astore 1
goto L1
L2:
aload 0
aload 1
invokestatic android/support/v4/view/ap/a(Landroid/view/LayoutInflater;Landroid/view/LayoutInflater$Factory2;)V
return
.limit locals 3
.limit stack 3
.end method

.method static a(Landroid/view/LayoutInflater;Landroid/view/LayoutInflater$Factory2;)V
.catch java/lang/NoSuchFieldException from L0 to L1 using L2
.catch java/lang/IllegalAccessException from L3 to L4 using L5
getstatic android/support/v4/view/ap/c Z
ifne L6
L0:
ldc android/view/LayoutInflater
ldc "mFactory2"
invokevirtual java/lang/Class/getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;
astore 2
aload 2
putstatic android/support/v4/view/ap/b Ljava/lang/reflect/Field;
aload 2
iconst_1
invokevirtual java/lang/reflect/Field/setAccessible(Z)V
L1:
iconst_1
putstatic android/support/v4/view/ap/c Z
L6:
getstatic android/support/v4/view/ap/b Ljava/lang/reflect/Field;
ifnull L4
L3:
getstatic android/support/v4/view/ap/b Ljava/lang/reflect/Field;
aload 0
aload 1
invokevirtual java/lang/reflect/Field/set(Ljava/lang/Object;Ljava/lang/Object;)V
L4:
return
L2:
astore 2
ldc "LayoutInflaterCompatHC"
new java/lang/StringBuilder
dup
ldc "forceSetFactory2 Could not find field 'mFactory2' on class "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
ldc android/view/LayoutInflater
invokevirtual java/lang/Class/getName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "; inflation may have unexpected results."
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aload 2
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
goto L1
L5:
astore 1
ldc "LayoutInflaterCompatHC"
new java/lang/StringBuilder
dup
ldc "forceSetFactory2 could not set the Factory2 on LayoutInflater "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc "; inflation may have unexpected results."
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aload 1
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
return
.limit locals 3
.limit stack 4
.end method
