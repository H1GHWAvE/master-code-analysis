.bytecode 50.0
.class public final synchronized android/support/v4/view/bu
.super java/lang/Object

.field public 'a' Z

.field private final 'b' Landroid/view/View;

.field private 'c' Landroid/view/ViewParent;

.field private 'd' [I

.method public <init>(Landroid/view/View;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
putfield android/support/v4/view/bu/b Landroid/view/View;
return
.limit locals 2
.limit stack 2
.end method

.method private c()Z
aload 0
getfield android/support/v4/view/bu/a Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private d()V
aload 0
getfield android/support/v4/view/bu/b Landroid/view/View;
invokestatic android/support/v4/view/cx/A(Landroid/view/View;)V
return
.limit locals 1
.limit stack 1
.end method

.method private e()V
aload 0
getfield android/support/v4/view/bu/b Landroid/view/View;
invokestatic android/support/v4/view/cx/A(Landroid/view/View;)V
return
.limit locals 1
.limit stack 1
.end method

.method public final a(Z)V
aload 0
getfield android/support/v4/view/bu/a Z
ifeq L0
aload 0
getfield android/support/v4/view/bu/b Landroid/view/View;
invokestatic android/support/v4/view/cx/A(Landroid/view/View;)V
L0:
aload 0
iload 1
putfield android/support/v4/view/bu/a Z
return
.limit locals 2
.limit stack 2
.end method

.method public final a()Z
aload 0
getfield android/support/v4/view/bu/c Landroid/view/ViewParent;
ifnull L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final a(FF)Z
aload 0
getfield android/support/v4/view/bu/a Z
ifeq L0
aload 0
getfield android/support/v4/view/bu/c Landroid/view/ViewParent;
ifnull L0
aload 0
getfield android/support/v4/view/bu/c Landroid/view/ViewParent;
aload 0
getfield android/support/v4/view/bu/b Landroid/view/View;
fload 1
fload 2
invokestatic android/support/v4/view/fb/a(Landroid/view/ViewParent;Landroid/view/View;FF)Z
ireturn
L0:
iconst_0
ireturn
.limit locals 3
.limit stack 4
.end method

.method public final a(FFZ)Z
aload 0
getfield android/support/v4/view/bu/a Z
ifeq L0
aload 0
getfield android/support/v4/view/bu/c Landroid/view/ViewParent;
ifnull L0
aload 0
getfield android/support/v4/view/bu/c Landroid/view/ViewParent;
aload 0
getfield android/support/v4/view/bu/b Landroid/view/View;
fload 1
fload 2
iload 3
invokestatic android/support/v4/view/fb/a(Landroid/view/ViewParent;Landroid/view/View;FFZ)Z
ireturn
L0:
iconst_0
ireturn
.limit locals 4
.limit stack 5
.end method

.method public final a(I)Z
aload 0
invokevirtual android/support/v4/view/bu/a()Z
ifeq L0
iconst_1
ireturn
L0:
aload 0
getfield android/support/v4/view/bu/a Z
ifeq L1
aload 0
getfield android/support/v4/view/bu/b Landroid/view/View;
invokevirtual android/view/View/getParent()Landroid/view/ViewParent;
astore 2
aload 0
getfield android/support/v4/view/bu/b Landroid/view/View;
astore 3
L2:
aload 2
ifnull L1
aload 2
aload 3
aload 0
getfield android/support/v4/view/bu/b Landroid/view/View;
iload 1
invokestatic android/support/v4/view/fb/a(Landroid/view/ViewParent;Landroid/view/View;Landroid/view/View;I)Z
ifeq L3
aload 0
aload 2
putfield android/support/v4/view/bu/c Landroid/view/ViewParent;
aload 2
aload 3
aload 0
getfield android/support/v4/view/bu/b Landroid/view/View;
iload 1
invokestatic android/support/v4/view/fb/b(Landroid/view/ViewParent;Landroid/view/View;Landroid/view/View;I)V
iconst_1
ireturn
L3:
aload 2
instanceof android/view/View
ifeq L4
aload 2
checkcast android/view/View
astore 3
L4:
aload 2
invokeinterface android/view/ViewParent/getParent()Landroid/view/ViewParent; 0
astore 2
goto L2
L1:
iconst_0
ireturn
.limit locals 4
.limit stack 4
.end method

.method public final a(IIII[I)Z
iconst_0
istore 9
iload 9
istore 8
aload 0
getfield android/support/v4/view/bu/a Z
ifeq L0
iload 9
istore 8
aload 0
getfield android/support/v4/view/bu/c Landroid/view/ViewParent;
ifnull L0
iload 1
ifne L1
iload 2
ifne L1
iload 3
ifne L1
iload 4
ifeq L2
L1:
aload 5
ifnull L3
aload 0
getfield android/support/v4/view/bu/b Landroid/view/View;
aload 5
invokevirtual android/view/View/getLocationInWindow([I)V
aload 5
iconst_0
iaload
istore 7
aload 5
iconst_1
iaload
istore 6
L4:
aload 0
getfield android/support/v4/view/bu/c Landroid/view/ViewParent;
aload 0
getfield android/support/v4/view/bu/b Landroid/view/View;
iload 1
iload 2
iload 3
iload 4
invokestatic android/support/v4/view/fb/a(Landroid/view/ViewParent;Landroid/view/View;IIII)V
aload 5
ifnull L5
aload 0
getfield android/support/v4/view/bu/b Landroid/view/View;
aload 5
invokevirtual android/view/View/getLocationInWindow([I)V
aload 5
iconst_0
aload 5
iconst_0
iaload
iload 7
isub
iastore
aload 5
iconst_1
aload 5
iconst_1
iaload
iload 6
isub
iastore
L5:
iconst_1
istore 8
L0:
iload 8
ireturn
L2:
iload 9
istore 8
aload 5
ifnull L0
aload 5
iconst_0
iconst_0
iastore
aload 5
iconst_1
iconst_0
iastore
iconst_0
ireturn
L3:
iconst_0
istore 6
iconst_0
istore 7
goto L4
.limit locals 10
.limit stack 6
.end method

.method public final a(II[I[I)Z
iconst_0
istore 8
iload 8
istore 7
aload 0
getfield android/support/v4/view/bu/a Z
ifeq L0
iload 8
istore 7
aload 0
getfield android/support/v4/view/bu/c Landroid/view/ViewParent;
ifnull L0
iload 1
ifne L1
iload 2
ifeq L2
L1:
aload 4
ifnull L3
aload 0
getfield android/support/v4/view/bu/b Landroid/view/View;
aload 4
invokevirtual android/view/View/getLocationInWindow([I)V
aload 4
iconst_0
iaload
istore 6
aload 4
iconst_1
iaload
istore 5
L4:
aload 3
astore 9
aload 3
ifnonnull L5
aload 0
getfield android/support/v4/view/bu/d [I
ifnonnull L6
aload 0
iconst_2
newarray int
putfield android/support/v4/view/bu/d [I
L6:
aload 0
getfield android/support/v4/view/bu/d [I
astore 9
L5:
aload 9
iconst_0
iconst_0
iastore
aload 9
iconst_1
iconst_0
iastore
aload 0
getfield android/support/v4/view/bu/c Landroid/view/ViewParent;
aload 0
getfield android/support/v4/view/bu/b Landroid/view/View;
iload 1
iload 2
aload 9
invokestatic android/support/v4/view/fb/a(Landroid/view/ViewParent;Landroid/view/View;II[I)V
aload 4
ifnull L7
aload 0
getfield android/support/v4/view/bu/b Landroid/view/View;
aload 4
invokevirtual android/view/View/getLocationInWindow([I)V
aload 4
iconst_0
aload 4
iconst_0
iaload
iload 6
isub
iastore
aload 4
iconst_1
aload 4
iconst_1
iaload
iload 5
isub
iastore
L7:
aload 9
iconst_0
iaload
ifne L8
iload 8
istore 7
aload 9
iconst_1
iaload
ifeq L0
L8:
iconst_1
istore 7
L0:
iload 7
ireturn
L2:
iload 8
istore 7
aload 4
ifnull L0
aload 4
iconst_0
iconst_0
iastore
aload 4
iconst_1
iconst_0
iastore
iconst_0
ireturn
L3:
iconst_0
istore 5
iconst_0
istore 6
goto L4
.limit locals 10
.limit stack 5
.end method

.method public final b()V
aload 0
getfield android/support/v4/view/bu/c Landroid/view/ViewParent;
ifnull L0
aload 0
getfield android/support/v4/view/bu/c Landroid/view/ViewParent;
aload 0
getfield android/support/v4/view/bu/b Landroid/view/View;
invokestatic android/support/v4/view/fb/a(Landroid/view/ViewParent;Landroid/view/View;)V
aload 0
aconst_null
putfield android/support/v4/view/bu/c Landroid/view/ViewParent;
L0:
return
.limit locals 1
.limit stack 2
.end method
