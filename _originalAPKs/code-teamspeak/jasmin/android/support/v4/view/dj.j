.bytecode 50.0
.class final synchronized android/support/v4/view/dj
.super java/lang/Object

.field private static final 'a' Ljava/lang/String; = "ViewCompatBase"

.field private static 'b' Ljava/lang/reflect/Field;

.field private static 'c' Z

.field private static 'd' Ljava/lang/reflect/Field;

.field private static 'e' Z

.method <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method static a(Landroid/view/View;)I
.catch java/lang/NoSuchFieldException from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L5
getstatic android/support/v4/view/dj/c Z
ifne L6
L0:
ldc android/view/View
ldc "mMinWidth"
invokevirtual java/lang/Class/getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;
astore 2
aload 2
putstatic android/support/v4/view/dj/b Ljava/lang/reflect/Field;
aload 2
iconst_1
invokevirtual java/lang/reflect/Field/setAccessible(Z)V
L1:
iconst_1
putstatic android/support/v4/view/dj/c Z
L6:
getstatic android/support/v4/view/dj/b Ljava/lang/reflect/Field;
ifnull L7
L3:
getstatic android/support/v4/view/dj/b Ljava/lang/reflect/Field;
aload 0
invokevirtual java/lang/reflect/Field/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/Integer
invokevirtual java/lang/Integer/intValue()I
istore 1
L4:
iload 1
ireturn
L5:
astore 0
L7:
iconst_0
ireturn
L2:
astore 2
goto L1
.limit locals 3
.limit stack 2
.end method

.method private static a(Landroid/view/View;Landroid/content/res/ColorStateList;)V
aload 0
instanceof android/support/v4/view/cr
ifeq L0
aload 0
checkcast android/support/v4/view/cr
aload 1
invokeinterface android/support/v4/view/cr/setSupportBackgroundTintList(Landroid/content/res/ColorStateList;)V 1
L0:
return
.limit locals 2
.limit stack 2
.end method

.method private static a(Landroid/view/View;Landroid/graphics/PorterDuff$Mode;)V
aload 0
instanceof android/support/v4/view/cr
ifeq L0
aload 0
checkcast android/support/v4/view/cr
aload 1
invokeinterface android/support/v4/view/cr/setSupportBackgroundTintMode(Landroid/graphics/PorterDuff$Mode;)V 1
L0:
return
.limit locals 2
.limit stack 2
.end method

.method static b(Landroid/view/View;)I
.catch java/lang/NoSuchFieldException from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L5
getstatic android/support/v4/view/dj/e Z
ifne L6
L0:
ldc android/view/View
ldc "mMinHeight"
invokevirtual java/lang/Class/getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;
astore 2
aload 2
putstatic android/support/v4/view/dj/d Ljava/lang/reflect/Field;
aload 2
iconst_1
invokevirtual java/lang/reflect/Field/setAccessible(Z)V
L1:
iconst_1
putstatic android/support/v4/view/dj/e Z
L6:
getstatic android/support/v4/view/dj/d Ljava/lang/reflect/Field;
ifnull L7
L3:
getstatic android/support/v4/view/dj/d Ljava/lang/reflect/Field;
aload 0
invokevirtual java/lang/reflect/Field/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/Integer
invokevirtual java/lang/Integer/intValue()I
istore 1
L4:
iload 1
ireturn
L5:
astore 0
L7:
iconst_0
ireturn
L2:
astore 2
goto L1
.limit locals 3
.limit stack 2
.end method

.method private static c(Landroid/view/View;)Landroid/content/res/ColorStateList;
aload 0
instanceof android/support/v4/view/cr
ifeq L0
aload 0
checkcast android/support/v4/view/cr
invokeinterface android/support/v4/view/cr/getSupportBackgroundTintList()Landroid/content/res/ColorStateList; 0
areturn
L0:
aconst_null
areturn
.limit locals 1
.limit stack 1
.end method

.method private static d(Landroid/view/View;)Landroid/graphics/PorterDuff$Mode;
aload 0
instanceof android/support/v4/view/cr
ifeq L0
aload 0
checkcast android/support/v4/view/cr
invokeinterface android/support/v4/view/cr/getSupportBackgroundTintMode()Landroid/graphics/PorterDuff$Mode; 0
areturn
L0:
aconst_null
areturn
.limit locals 1
.limit stack 1
.end method

.method private static e(Landroid/view/View;)Z
aload 0
invokevirtual android/view/View/getWidth()I
ifle L0
aload 0
invokevirtual android/view/View/getHeight()I
ifle L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method private static f(Landroid/view/View;)Z
aload 0
invokevirtual android/view/View/getWindowToken()Landroid/os/IBinder;
ifnull L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method
