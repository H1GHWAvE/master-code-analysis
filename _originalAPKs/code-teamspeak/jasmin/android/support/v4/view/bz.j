.bytecode 50.0
.class public final synchronized android/support/v4/view/bz
.super android/support/v4/view/cc

.field private static final 'f' Ljava/lang/String; = "PagerTabStrip"

.field private static final 'g' I = 3


.field private static final 'h' I = 6


.field private static final 'i' I = 16


.field private static final 'j' I = 32


.field private static final 'k' I = 64


.field private static final 'l' I = 1


.field private static final 'm' I = 32


.field private 'A' F

.field private 'B' F

.field private 'C' I

.field private 'n' I

.field private 'o' I

.field private 'p' I

.field private 'q' I

.field private 'r' I

.field private 's' I

.field private final 't' Landroid/graphics/Paint;

.field private final 'u' Landroid/graphics/Rect;

.field private 'v' I

.field private 'w' Z

.field private 'x' Z

.field private 'y' I

.field private 'z' Z

.method private <init>(Landroid/content/Context;)V
aload 0
aload 1
iconst_0
invokespecial android/support/v4/view/bz/<init>(Landroid/content/Context;B)V
return
.limit locals 2
.limit stack 3
.end method

.method private <init>(Landroid/content/Context;B)V
aload 0
aload 1
iconst_0
invokespecial android/support/v4/view/cc/<init>(Landroid/content/Context;B)V
aload 0
new android/graphics/Paint
dup
invokespecial android/graphics/Paint/<init>()V
putfield android/support/v4/view/bz/t Landroid/graphics/Paint;
aload 0
new android/graphics/Rect
dup
invokespecial android/graphics/Rect/<init>()V
putfield android/support/v4/view/bz/u Landroid/graphics/Rect;
aload 0
sipush 255
putfield android/support/v4/view/bz/v I
aload 0
iconst_0
putfield android/support/v4/view/bz/w Z
aload 0
iconst_0
putfield android/support/v4/view/bz/x Z
aload 0
aload 0
getfield android/support/v4/view/bz/e I
putfield android/support/v4/view/bz/n I
aload 0
getfield android/support/v4/view/bz/t Landroid/graphics/Paint;
aload 0
getfield android/support/v4/view/bz/n I
invokevirtual android/graphics/Paint/setColor(I)V
aload 1
invokevirtual android/content/Context/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
getfield android/util/DisplayMetrics/density F
fstore 3
aload 0
ldc_w 3.0F
fload 3
fmul
ldc_w 0.5F
fadd
f2i
putfield android/support/v4/view/bz/o I
aload 0
ldc_w 6.0F
fload 3
fmul
ldc_w 0.5F
fadd
f2i
putfield android/support/v4/view/bz/p I
aload 0
ldc_w 64.0F
fload 3
fmul
f2i
putfield android/support/v4/view/bz/q I
aload 0
ldc_w 16.0F
fload 3
fmul
ldc_w 0.5F
fadd
f2i
putfield android/support/v4/view/bz/s I
aload 0
fconst_1
fload 3
fmul
ldc_w 0.5F
fadd
f2i
putfield android/support/v4/view/bz/y I
aload 0
fload 3
ldc_w 32.0F
fmul
ldc_w 0.5F
fadd
f2i
putfield android/support/v4/view/bz/r I
aload 0
aload 1
invokestatic android/view/ViewConfiguration/get(Landroid/content/Context;)Landroid/view/ViewConfiguration;
invokevirtual android/view/ViewConfiguration/getScaledTouchSlop()I
putfield android/support/v4/view/bz/C I
aload 0
aload 0
invokevirtual android/support/v4/view/bz/getPaddingLeft()I
aload 0
invokevirtual android/support/v4/view/bz/getPaddingTop()I
aload 0
invokevirtual android/support/v4/view/bz/getPaddingRight()I
aload 0
invokevirtual android/support/v4/view/bz/getPaddingBottom()I
invokevirtual android/support/v4/view/bz/setPadding(IIII)V
aload 0
aload 0
invokevirtual android/support/v4/view/bz/getTextSpacing()I
invokevirtual android/support/v4/view/bz/setTextSpacing(I)V
aload 0
iconst_0
invokevirtual android/support/v4/view/bz/setWillNotDraw(Z)V
aload 0
getfield android/support/v4/view/bz/b Landroid/widget/TextView;
iconst_1
invokevirtual android/widget/TextView/setFocusable(Z)V
aload 0
getfield android/support/v4/view/bz/b Landroid/widget/TextView;
new android/support/v4/view/ca
dup
aload 0
invokespecial android/support/v4/view/ca/<init>(Landroid/support/v4/view/bz;)V
invokevirtual android/widget/TextView/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 0
getfield android/support/v4/view/bz/d Landroid/widget/TextView;
iconst_1
invokevirtual android/widget/TextView/setFocusable(Z)V
aload 0
getfield android/support/v4/view/bz/d Landroid/widget/TextView;
new android/support/v4/view/cb
dup
aload 0
invokespecial android/support/v4/view/cb/<init>(Landroid/support/v4/view/bz;)V
invokevirtual android/widget/TextView/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 0
invokevirtual android/support/v4/view/bz/getBackground()Landroid/graphics/drawable/Drawable;
ifnonnull L0
aload 0
iconst_1
putfield android/support/v4/view/bz/w Z
L0:
return
.limit locals 4
.limit stack 5
.end method

.method final a(IFZ)V
aload 0
getfield android/support/v4/view/bz/u Landroid/graphics/Rect;
astore 10
aload 0
invokevirtual android/support/v4/view/bz/getHeight()I
istore 4
aload 0
getfield android/support/v4/view/bz/c Landroid/widget/TextView;
invokevirtual android/widget/TextView/getLeft()I
istore 5
aload 0
getfield android/support/v4/view/bz/s I
istore 6
aload 0
getfield android/support/v4/view/bz/c Landroid/widget/TextView;
invokevirtual android/widget/TextView/getRight()I
istore 7
aload 0
getfield android/support/v4/view/bz/s I
istore 8
iload 4
aload 0
getfield android/support/v4/view/bz/o I
isub
istore 9
aload 10
iload 5
iload 6
isub
iload 9
iload 7
iload 8
iadd
iload 4
invokevirtual android/graphics/Rect/set(IIII)V
aload 0
iload 1
fload 2
iload 3
invokespecial android/support/v4/view/cc/a(IFZ)V
aload 0
fload 2
ldc_w 0.5F
fsub
invokestatic java/lang/Math/abs(F)F
fconst_2
fmul
ldc_w 255.0F
fmul
f2i
putfield android/support/v4/view/bz/v I
aload 10
aload 0
getfield android/support/v4/view/bz/c Landroid/widget/TextView;
invokevirtual android/widget/TextView/getLeft()I
aload 0
getfield android/support/v4/view/bz/s I
isub
iload 9
aload 0
getfield android/support/v4/view/bz/c Landroid/widget/TextView;
invokevirtual android/widget/TextView/getRight()I
aload 0
getfield android/support/v4/view/bz/s I
iadd
iload 4
invokevirtual android/graphics/Rect/union(IIII)V
aload 0
aload 10
invokevirtual android/support/v4/view/bz/invalidate(Landroid/graphics/Rect;)V
return
.limit locals 11
.limit stack 5
.end method

.method public final getDrawFullUnderline()Z
aload 0
getfield android/support/v4/view/bz/w Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method final getMinHeight()I
aload 0
invokespecial android/support/v4/view/cc/getMinHeight()I
aload 0
getfield android/support/v4/view/bz/r I
invokestatic java/lang/Math/max(II)I
ireturn
.limit locals 1
.limit stack 2
.end method

.method public final getTabIndicatorColor()I
.annotation invisible Landroid/support/a/j;
.end annotation
aload 0
getfield android/support/v4/view/bz/n I
ireturn
.limit locals 1
.limit stack 1
.end method

.method protected final onDraw(Landroid/graphics/Canvas;)V
aload 0
aload 1
invokespecial android/support/v4/view/cc/onDraw(Landroid/graphics/Canvas;)V
aload 0
invokevirtual android/support/v4/view/bz/getHeight()I
istore 2
aload 0
getfield android/support/v4/view/bz/c Landroid/widget/TextView;
invokevirtual android/widget/TextView/getLeft()I
istore 3
aload 0
getfield android/support/v4/view/bz/s I
istore 4
aload 0
getfield android/support/v4/view/bz/c Landroid/widget/TextView;
invokevirtual android/widget/TextView/getRight()I
istore 5
aload 0
getfield android/support/v4/view/bz/s I
istore 6
aload 0
getfield android/support/v4/view/bz/o I
istore 7
aload 0
getfield android/support/v4/view/bz/t Landroid/graphics/Paint;
aload 0
getfield android/support/v4/view/bz/v I
bipush 24
ishl
aload 0
getfield android/support/v4/view/bz/n I
ldc_w 16777215
iand
ior
invokevirtual android/graphics/Paint/setColor(I)V
aload 1
iload 3
iload 4
isub
i2f
iload 2
iload 7
isub
i2f
iload 5
iload 6
iadd
i2f
iload 2
i2f
aload 0
getfield android/support/v4/view/bz/t Landroid/graphics/Paint;
invokevirtual android/graphics/Canvas/drawRect(FFFFLandroid/graphics/Paint;)V
aload 0
getfield android/support/v4/view/bz/w Z
ifeq L0
aload 0
getfield android/support/v4/view/bz/t Landroid/graphics/Paint;
ldc_w -16777216
aload 0
getfield android/support/v4/view/bz/n I
ldc_w 16777215
iand
ior
invokevirtual android/graphics/Paint/setColor(I)V
aload 1
aload 0
invokevirtual android/support/v4/view/bz/getPaddingLeft()I
i2f
iload 2
aload 0
getfield android/support/v4/view/bz/y I
isub
i2f
aload 0
invokevirtual android/support/v4/view/bz/getWidth()I
aload 0
invokevirtual android/support/v4/view/bz/getPaddingRight()I
isub
i2f
iload 2
i2f
aload 0
getfield android/support/v4/view/bz/t Landroid/graphics/Paint;
invokevirtual android/graphics/Canvas/drawRect(FFFFLandroid/graphics/Paint;)V
L0:
return
.limit locals 8
.limit stack 6
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
aload 1
invokevirtual android/view/MotionEvent/getAction()I
istore 4
iload 4
ifeq L0
aload 0
getfield android/support/v4/view/bz/z Z
ifeq L0
iconst_0
ireturn
L0:
aload 1
invokevirtual android/view/MotionEvent/getX()F
fstore 2
aload 1
invokevirtual android/view/MotionEvent/getY()F
fstore 3
iload 4
tableswitch 0
L1
L2
L3
default : L4
L4:
iconst_1
ireturn
L1:
aload 0
fload 2
putfield android/support/v4/view/bz/A F
aload 0
fload 3
putfield android/support/v4/view/bz/B F
aload 0
iconst_0
putfield android/support/v4/view/bz/z Z
goto L4
L3:
fload 2
aload 0
getfield android/support/v4/view/bz/A F
fsub
invokestatic java/lang/Math/abs(F)F
aload 0
getfield android/support/v4/view/bz/C I
i2f
fcmpl
ifgt L5
fload 3
aload 0
getfield android/support/v4/view/bz/B F
fsub
invokestatic java/lang/Math/abs(F)F
aload 0
getfield android/support/v4/view/bz/C I
i2f
fcmpl
ifle L4
L5:
aload 0
iconst_1
putfield android/support/v4/view/bz/z Z
goto L4
L2:
fload 2
aload 0
getfield android/support/v4/view/bz/c Landroid/widget/TextView;
invokevirtual android/widget/TextView/getLeft()I
aload 0
getfield android/support/v4/view/bz/s I
isub
i2f
fcmpg
ifge L6
aload 0
getfield android/support/v4/view/bz/a Landroid/support/v4/view/ViewPager;
aload 0
getfield android/support/v4/view/bz/a Landroid/support/v4/view/ViewPager;
invokevirtual android/support/v4/view/ViewPager/getCurrentItem()I
iconst_1
isub
invokevirtual android/support/v4/view/ViewPager/setCurrentItem(I)V
goto L4
L6:
fload 2
aload 0
getfield android/support/v4/view/bz/c Landroid/widget/TextView;
invokevirtual android/widget/TextView/getRight()I
aload 0
getfield android/support/v4/view/bz/s I
iadd
i2f
fcmpl
ifle L4
aload 0
getfield android/support/v4/view/bz/a Landroid/support/v4/view/ViewPager;
aload 0
getfield android/support/v4/view/bz/a Landroid/support/v4/view/ViewPager;
invokevirtual android/support/v4/view/ViewPager/getCurrentItem()I
iconst_1
iadd
invokevirtual android/support/v4/view/ViewPager/setCurrentItem(I)V
goto L4
.limit locals 5
.limit stack 3
.end method

.method public final setBackgroundColor(I)V
.annotation invisibleparam 1 Landroid/support/a/j;
.end annotation
aload 0
iload 1
invokespecial android/support/v4/view/cc/setBackgroundColor(I)V
aload 0
getfield android/support/v4/view/bz/x Z
ifne L0
ldc_w -16777216
iload 1
iand
ifne L1
iconst_1
istore 2
L2:
aload 0
iload 2
putfield android/support/v4/view/bz/w Z
L0:
return
L1:
iconst_0
istore 2
goto L2
.limit locals 3
.limit stack 2
.end method

.method public final setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
aload 0
aload 1
invokespecial android/support/v4/view/cc/setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
aload 0
getfield android/support/v4/view/bz/x Z
ifne L0
aload 1
ifnonnull L1
iconst_1
istore 2
L2:
aload 0
iload 2
putfield android/support/v4/view/bz/w Z
L0:
return
L1:
iconst_0
istore 2
goto L2
.limit locals 3
.limit stack 2
.end method

.method public final setBackgroundResource(I)V
.annotation invisibleparam 1 Landroid/support/a/m;
.end annotation
aload 0
iload 1
invokespecial android/support/v4/view/cc/setBackgroundResource(I)V
aload 0
getfield android/support/v4/view/bz/x Z
ifne L0
iload 1
ifne L1
iconst_1
istore 2
L2:
aload 0
iload 2
putfield android/support/v4/view/bz/w Z
L0:
return
L1:
iconst_0
istore 2
goto L2
.limit locals 3
.limit stack 2
.end method

.method public final setDrawFullUnderline(Z)V
aload 0
iload 1
putfield android/support/v4/view/bz/w Z
aload 0
iconst_1
putfield android/support/v4/view/bz/x Z
aload 0
invokevirtual android/support/v4/view/bz/invalidate()V
return
.limit locals 2
.limit stack 2
.end method

.method public final setPadding(IIII)V
iload 4
istore 5
iload 4
aload 0
getfield android/support/v4/view/bz/p I
if_icmpge L0
aload 0
getfield android/support/v4/view/bz/p I
istore 5
L0:
aload 0
iload 1
iload 2
iload 3
iload 5
invokespecial android/support/v4/view/cc/setPadding(IIII)V
return
.limit locals 6
.limit stack 5
.end method

.method public final setTabIndicatorColor(I)V
.annotation invisibleparam 1 Landroid/support/a/j;
.end annotation
aload 0
iload 1
putfield android/support/v4/view/bz/n I
aload 0
getfield android/support/v4/view/bz/t Landroid/graphics/Paint;
aload 0
getfield android/support/v4/view/bz/n I
invokevirtual android/graphics/Paint/setColor(I)V
aload 0
invokevirtual android/support/v4/view/bz/invalidate()V
return
.limit locals 2
.limit stack 2
.end method

.method public final setTabIndicatorColorResource(I)V
.annotation invisibleparam 1 Landroid/support/a/k;
.end annotation
aload 0
aload 0
invokevirtual android/support/v4/view/bz/getContext()Landroid/content/Context;
invokevirtual android/content/Context/getResources()Landroid/content/res/Resources;
iload 1
invokevirtual android/content/res/Resources/getColor(I)I
invokevirtual android/support/v4/view/bz/setTabIndicatorColor(I)V
return
.limit locals 2
.limit stack 3
.end method

.method public final setTextSpacing(I)V
iload 1
istore 2
iload 1
aload 0
getfield android/support/v4/view/bz/q I
if_icmpge L0
aload 0
getfield android/support/v4/view/bz/q I
istore 2
L0:
aload 0
iload 2
invokespecial android/support/v4/view/cc/setTextSpacing(I)V
return
.limit locals 3
.limit stack 2
.end method
