.bytecode 50.0
.class public final synchronized android/support/v4/view/az
.super java/lang/Object

.field public static final 'a' I = 0


.field public static final 'b' I = 1


.field public static final 'c' I = 2


.field public static final 'd' I = 4


.field public static final 'e' I = 8


.field static final 'f' Landroid/support/v4/view/be;

.field private static final 'g' Ljava/lang/String; = "MenuItemCompat"

.method static <clinit>()V
getstatic android/os/Build$VERSION/SDK_INT I
istore 0
iload 0
bipush 14
if_icmplt L0
new android/support/v4/view/bc
dup
invokespecial android/support/v4/view/bc/<init>()V
putstatic android/support/v4/view/az/f Landroid/support/v4/view/be;
return
L0:
iload 0
bipush 11
if_icmplt L1
new android/support/v4/view/bb
dup
invokespecial android/support/v4/view/bb/<init>()V
putstatic android/support/v4/view/az/f Landroid/support/v4/view/be;
return
L1:
new android/support/v4/view/ba
dup
invokespecial android/support/v4/view/ba/<init>()V
putstatic android/support/v4/view/az/f Landroid/support/v4/view/be;
return
.limit locals 1
.limit stack 2
.end method

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a(Landroid/view/MenuItem;Landroid/support/v4/view/bf;)Landroid/view/MenuItem;
aload 0
instanceof android/support/v4/g/a/b
ifeq L0
aload 0
checkcast android/support/v4/g/a/b
aload 1
invokeinterface android/support/v4/g/a/b/a(Landroid/support/v4/view/bf;)Landroid/support/v4/g/a/b; 1
areturn
L0:
getstatic android/support/v4/view/az/f Landroid/support/v4/view/be;
aload 0
aload 1
invokeinterface android/support/v4/view/be/a(Landroid/view/MenuItem;Landroid/support/v4/view/bf;)Landroid/view/MenuItem; 2
areturn
.limit locals 2
.limit stack 3
.end method

.method public static a(Landroid/view/MenuItem;Landroid/support/v4/view/n;)Landroid/view/MenuItem;
aload 0
instanceof android/support/v4/g/a/b
ifeq L0
aload 0
checkcast android/support/v4/g/a/b
aload 1
invokeinterface android/support/v4/g/a/b/a(Landroid/support/v4/view/n;)Landroid/support/v4/g/a/b; 1
areturn
L0:
ldc "MenuItemCompat"
ldc "setActionProvider: item does not implement SupportMenuItem; ignoring"
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/String;)I
pop
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method public static a(Landroid/view/MenuItem;Landroid/view/View;)Landroid/view/MenuItem;
aload 0
instanceof android/support/v4/g/a/b
ifeq L0
aload 0
checkcast android/support/v4/g/a/b
aload 1
invokeinterface android/support/v4/g/a/b/setActionView(Landroid/view/View;)Landroid/view/MenuItem; 1
areturn
L0:
getstatic android/support/v4/view/az/f Landroid/support/v4/view/be;
aload 0
aload 1
invokeinterface android/support/v4/view/be/a(Landroid/view/MenuItem;Landroid/view/View;)Landroid/view/MenuItem; 2
areturn
.limit locals 2
.limit stack 3
.end method

.method public static a(Landroid/view/MenuItem;)Landroid/view/View;
aload 0
instanceof android/support/v4/g/a/b
ifeq L0
aload 0
checkcast android/support/v4/g/a/b
invokeinterface android/support/v4/g/a/b/getActionView()Landroid/view/View; 0
areturn
L0:
getstatic android/support/v4/view/az/f Landroid/support/v4/view/be;
aload 0
invokeinterface android/support/v4/view/be/a(Landroid/view/MenuItem;)Landroid/view/View; 1
areturn
.limit locals 1
.limit stack 2
.end method

.method public static a(Landroid/view/MenuItem;I)V
aload 0
instanceof android/support/v4/g/a/b
ifeq L0
aload 0
checkcast android/support/v4/g/a/b
iload 1
invokeinterface android/support/v4/g/a/b/setShowAsAction(I)V 1
return
L0:
getstatic android/support/v4/view/az/f Landroid/support/v4/view/be;
aload 0
iload 1
invokeinterface android/support/v4/view/be/a(Landroid/view/MenuItem;I)V 2
return
.limit locals 2
.limit stack 3
.end method

.method public static b(Landroid/view/MenuItem;I)Landroid/view/MenuItem;
aload 0
instanceof android/support/v4/g/a/b
ifeq L0
aload 0
checkcast android/support/v4/g/a/b
iload 1
invokeinterface android/support/v4/g/a/b/setActionView(I)Landroid/view/MenuItem; 1
areturn
L0:
getstatic android/support/v4/view/az/f Landroid/support/v4/view/be;
aload 0
iload 1
invokeinterface android/support/v4/view/be/b(Landroid/view/MenuItem;I)Landroid/view/MenuItem; 2
areturn
.limit locals 2
.limit stack 3
.end method

.method public static b(Landroid/view/MenuItem;)Z
aload 0
instanceof android/support/v4/g/a/b
ifeq L0
aload 0
checkcast android/support/v4/g/a/b
invokeinterface android/support/v4/g/a/b/expandActionView()Z 0
ireturn
L0:
getstatic android/support/v4/view/az/f Landroid/support/v4/view/be;
aload 0
invokeinterface android/support/v4/view/be/b(Landroid/view/MenuItem;)Z 1
ireturn
.limit locals 1
.limit stack 2
.end method

.method public static c(Landroid/view/MenuItem;)Z
aload 0
instanceof android/support/v4/g/a/b
ifeq L0
aload 0
checkcast android/support/v4/g/a/b
invokeinterface android/support/v4/g/a/b/isActionViewExpanded()Z 0
ireturn
L0:
getstatic android/support/v4/view/az/f Landroid/support/v4/view/be;
aload 0
invokeinterface android/support/v4/view/be/d(Landroid/view/MenuItem;)Z 1
ireturn
.limit locals 1
.limit stack 2
.end method

.method private static d(Landroid/view/MenuItem;)Landroid/support/v4/view/n;
aload 0
instanceof android/support/v4/g/a/b
ifeq L0
aload 0
checkcast android/support/v4/g/a/b
invokeinterface android/support/v4/g/a/b/a()Landroid/support/v4/view/n; 0
areturn
L0:
ldc "MenuItemCompat"
ldc "getActionProvider: item does not implement SupportMenuItem; returning null"
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/String;)I
pop
aconst_null
areturn
.limit locals 1
.limit stack 2
.end method

.method private static e(Landroid/view/MenuItem;)Z
aload 0
instanceof android/support/v4/g/a/b
ifeq L0
aload 0
checkcast android/support/v4/g/a/b
invokeinterface android/support/v4/g/a/b/collapseActionView()Z 0
ireturn
L0:
getstatic android/support/v4/view/az/f Landroid/support/v4/view/be;
aload 0
invokeinterface android/support/v4/view/be/c(Landroid/view/MenuItem;)Z 1
ireturn
.limit locals 1
.limit stack 2
.end method
