.bytecode 50.0
.class final synchronized android/support/v4/view/ax
.super java/lang/Object

.method <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a(Landroid/view/ViewGroup$MarginLayoutParams;)I
aload 0
invokevirtual android/view/ViewGroup$MarginLayoutParams/getMarginStart()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private static a(Landroid/view/ViewGroup$MarginLayoutParams;I)V
aload 0
iload 1
invokevirtual android/view/ViewGroup$MarginLayoutParams/setMarginStart(I)V
return
.limit locals 2
.limit stack 2
.end method

.method private static b(Landroid/view/ViewGroup$MarginLayoutParams;)I
aload 0
invokevirtual android/view/ViewGroup$MarginLayoutParams/getMarginEnd()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private static b(Landroid/view/ViewGroup$MarginLayoutParams;I)V
aload 0
iload 1
invokevirtual android/view/ViewGroup$MarginLayoutParams/setMarginEnd(I)V
return
.limit locals 2
.limit stack 2
.end method

.method private static c(Landroid/view/ViewGroup$MarginLayoutParams;I)V
aload 0
iload 1
invokevirtual android/view/ViewGroup$MarginLayoutParams/setLayoutDirection(I)V
return
.limit locals 2
.limit stack 2
.end method

.method private static c(Landroid/view/ViewGroup$MarginLayoutParams;)Z
aload 0
invokevirtual android/view/ViewGroup$MarginLayoutParams/isMarginRelative()Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private static d(Landroid/view/ViewGroup$MarginLayoutParams;)I
aload 0
invokevirtual android/view/ViewGroup$MarginLayoutParams/getLayoutDirection()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private static d(Landroid/view/ViewGroup$MarginLayoutParams;I)V
aload 0
iload 1
invokevirtual android/view/ViewGroup$MarginLayoutParams/resolveLayoutDirection(I)V
return
.limit locals 2
.limit stack 2
.end method
