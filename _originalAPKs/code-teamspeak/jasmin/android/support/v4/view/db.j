.bytecode 50.0
.class synchronized android/support/v4/view/db
.super android/support/v4/view/da

.method <init>()V
aload 0
invokespecial android/support/v4/view/da/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public final A(Landroid/view/View;)F
aload 1
invokevirtual android/view/View/getRotation()F
freturn
.limit locals 2
.limit stack 1
.end method

.method public final B(Landroid/view/View;)F
aload 1
invokevirtual android/view/View/getRotationX()F
freturn
.limit locals 2
.limit stack 1
.end method

.method public final C(Landroid/view/View;)F
aload 1
invokevirtual android/view/View/getRotationY()F
freturn
.limit locals 2
.limit stack 1
.end method

.method public final D(Landroid/view/View;)F
aload 1
invokevirtual android/view/View/getScaleX()F
freturn
.limit locals 2
.limit stack 1
.end method

.method public final E(Landroid/view/View;)F
aload 1
invokevirtual android/view/View/getScaleY()F
freturn
.limit locals 2
.limit stack 1
.end method

.method public final I(Landroid/view/View;)F
aload 1
invokevirtual android/view/View/getPivotX()F
freturn
.limit locals 2
.limit stack 1
.end method

.method public final J(Landroid/view/View;)F
aload 1
invokevirtual android/view/View/getPivotY()F
freturn
.limit locals 2
.limit stack 1
.end method

.method public final R(Landroid/view/View;)V
aload 1
invokevirtual android/view/View/jumpDrawablesToCurrentState()V
return
.limit locals 2
.limit stack 1
.end method

.method public final S(Landroid/view/View;)V
aload 1
iconst_0
invokevirtual android/view/View/setSaveFromParentEnabled(Z)V
return
.limit locals 2
.limit stack 2
.end method

.method public final a(II)I
iload 1
iload 2
invokestatic android/view/View/combineMeasuredStates(II)I
ireturn
.limit locals 3
.limit stack 2
.end method

.method public final a(III)I
iload 1
iload 2
iload 3
invokestatic android/view/View/resolveSizeAndState(III)I
ireturn
.limit locals 4
.limit stack 3
.end method

.method final a()J
invokestatic android/animation/ValueAnimator/getFrameDelay()J
lreturn
.limit locals 1
.limit stack 2
.end method

.method public final a(Landroid/view/View;F)V
aload 1
fload 2
invokevirtual android/view/View/setRotation(F)V
return
.limit locals 3
.limit stack 2
.end method

.method public final a(Landroid/view/View;ILandroid/graphics/Paint;)V
aload 1
iload 2
aload 3
invokevirtual android/view/View/setLayerType(ILandroid/graphics/Paint;)V
return
.limit locals 4
.limit stack 3
.end method

.method public a(Landroid/view/View;Landroid/graphics/Paint;)V
aload 1
aload 1
invokevirtual android/view/View/getLayerType()I
aload 2
invokevirtual android/view/View/setLayerType(ILandroid/graphics/Paint;)V
aload 1
invokevirtual android/view/View/invalidate()V
return
.limit locals 3
.limit stack 3
.end method

.method public final b(Landroid/view/View;F)V
aload 1
fload 2
invokevirtual android/view/View/setTranslationX(F)V
return
.limit locals 3
.limit stack 2
.end method

.method public final c(Landroid/view/View;F)V
aload 1
fload 2
invokevirtual android/view/View/setTranslationY(F)V
return
.limit locals 3
.limit stack 2
.end method

.method public final c(Landroid/view/View;Z)V
aload 1
iload 2
invokevirtual android/view/View/setActivated(Z)V
return
.limit locals 3
.limit stack 2
.end method

.method public final d(Landroid/view/View;F)V
aload 1
fload 2
invokevirtual android/view/View/setAlpha(F)V
return
.limit locals 3
.limit stack 2
.end method

.method public final e(Landroid/view/View;F)V
aload 1
fload 2
invokevirtual android/view/View/setRotationX(F)V
return
.limit locals 3
.limit stack 2
.end method

.method public final f(Landroid/view/View;F)V
aload 1
fload 2
invokevirtual android/view/View/setRotationY(F)V
return
.limit locals 3
.limit stack 2
.end method

.method public final g(Landroid/view/View;F)V
aload 1
fload 2
invokevirtual android/view/View/setScaleX(F)V
return
.limit locals 3
.limit stack 2
.end method

.method public final h(Landroid/view/View;)F
aload 1
invokevirtual android/view/View/getAlpha()F
freturn
.limit locals 2
.limit stack 1
.end method

.method public final h(Landroid/view/View;F)V
aload 1
fload 2
invokevirtual android/view/View/setScaleY(F)V
return
.limit locals 3
.limit stack 2
.end method

.method public final i(Landroid/view/View;)I
aload 1
invokevirtual android/view/View/getLayerType()I
ireturn
.limit locals 2
.limit stack 1
.end method

.method public final i(Landroid/view/View;F)V
aload 1
fload 2
invokevirtual android/view/View/setX(F)V
return
.limit locals 3
.limit stack 2
.end method

.method public final j(Landroid/view/View;F)V
aload 1
fload 2
invokevirtual android/view/View/setY(F)V
return
.limit locals 3
.limit stack 2
.end method

.method public final k(Landroid/view/View;F)V
aload 1
fload 2
invokevirtual android/view/View/setPivotX(F)V
return
.limit locals 3
.limit stack 2
.end method

.method public final l(Landroid/view/View;F)V
aload 1
fload 2
invokevirtual android/view/View/setPivotY(F)V
return
.limit locals 3
.limit stack 2
.end method

.method public final n(Landroid/view/View;)I
aload 1
invokevirtual android/view/View/getMeasuredWidthAndState()I
ireturn
.limit locals 2
.limit stack 1
.end method

.method public final o(Landroid/view/View;)I
aload 1
invokevirtual android/view/View/getMeasuredHeightAndState()I
ireturn
.limit locals 2
.limit stack 1
.end method

.method public final p(Landroid/view/View;)I
aload 1
invokevirtual android/view/View/getMeasuredState()I
ireturn
.limit locals 2
.limit stack 1
.end method

.method public final w(Landroid/view/View;)F
aload 1
invokevirtual android/view/View/getTranslationX()F
freturn
.limit locals 2
.limit stack 1
.end method

.method public final x(Landroid/view/View;)F
aload 1
invokevirtual android/view/View/getTranslationY()F
freturn
.limit locals 2
.limit stack 1
.end method

.method public final y(Landroid/view/View;)F
aload 1
invokevirtual android/view/View/getX()F
freturn
.limit locals 2
.limit stack 1
.end method

.method public final z(Landroid/view/View;)F
aload 1
invokevirtual android/view/View/getY()F
freturn
.limit locals 2
.limit stack 1
.end method
