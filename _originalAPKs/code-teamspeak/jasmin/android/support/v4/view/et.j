.bytecode 50.0
.class final synchronized android/support/v4/view/et
.super android/support/v4/view/a

.field final synthetic 'a' Landroid/support/v4/view/ViewPager;

.method <init>(Landroid/support/v4/view/ViewPager;)V
aload 0
aload 1
putfield android/support/v4/view/et/a Landroid/support/v4/view/ViewPager;
aload 0
invokespecial android/support/v4/view/a/<init>()V
return
.limit locals 2
.limit stack 2
.end method

.method private a()Z
aload 0
getfield android/support/v4/view/et/a Landroid/support/v4/view/ViewPager;
invokestatic android/support/v4/view/ViewPager/b(Landroid/support/v4/view/ViewPager;)Landroid/support/v4/view/by;
ifnull L0
aload 0
getfield android/support/v4/view/et/a Landroid/support/v4/view/ViewPager;
invokestatic android/support/v4/view/ViewPager/b(Landroid/support/v4/view/ViewPager;)Landroid/support/v4/view/by;
invokevirtual android/support/v4/view/by/e()I
iconst_1
if_icmple L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method public final a(Landroid/view/View;Landroid/support/v4/view/a/q;)V
aload 0
aload 1
aload 2
invokespecial android/support/v4/view/a/a(Landroid/view/View;Landroid/support/v4/view/a/q;)V
aload 2
ldc android/support/v4/view/ViewPager
invokevirtual java/lang/Class/getName()Ljava/lang/String;
invokevirtual android/support/v4/view/a/q/b(Ljava/lang/CharSequence;)V
aload 2
aload 0
invokespecial android/support/v4/view/et/a()Z
invokevirtual android/support/v4/view/a/q/i(Z)V
aload 0
getfield android/support/v4/view/et/a Landroid/support/v4/view/ViewPager;
iconst_1
invokevirtual android/support/v4/view/ViewPager/canScrollHorizontally(I)Z
ifeq L0
aload 2
sipush 4096
invokevirtual android/support/v4/view/a/q/a(I)V
L0:
aload 0
getfield android/support/v4/view/et/a Landroid/support/v4/view/ViewPager;
iconst_m1
invokevirtual android/support/v4/view/ViewPager/canScrollHorizontally(I)Z
ifeq L1
aload 2
sipush 8192
invokevirtual android/support/v4/view/a/q/a(I)V
L1:
return
.limit locals 3
.limit stack 3
.end method

.method public final a(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
aload 0
aload 1
aload 2
invokespecial android/support/v4/view/a/a(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
aload 2
ldc android/support/v4/view/ViewPager
invokevirtual java/lang/Class/getName()Ljava/lang/String;
invokevirtual android/view/accessibility/AccessibilityEvent/setClassName(Ljava/lang/CharSequence;)V
invokestatic android/support/v4/view/a/bd/a()Landroid/support/v4/view/a/bd;
astore 1
aload 1
aload 0
invokespecial android/support/v4/view/et/a()Z
invokevirtual android/support/v4/view/a/bd/a(Z)V
aload 2
invokevirtual android/view/accessibility/AccessibilityEvent/getEventType()I
sipush 4096
if_icmpne L0
aload 0
getfield android/support/v4/view/et/a Landroid/support/v4/view/ViewPager;
invokestatic android/support/v4/view/ViewPager/b(Landroid/support/v4/view/ViewPager;)Landroid/support/v4/view/by;
ifnull L0
aload 0
getfield android/support/v4/view/et/a Landroid/support/v4/view/ViewPager;
invokestatic android/support/v4/view/ViewPager/b(Landroid/support/v4/view/ViewPager;)Landroid/support/v4/view/by;
invokevirtual android/support/v4/view/by/e()I
istore 3
getstatic android/support/v4/view/a/bd/a Landroid/support/v4/view/a/bg;
aload 1
getfield android/support/v4/view/a/bd/b Ljava/lang/Object;
iload 3
invokeinterface android/support/v4/view/a/bg/d(Ljava/lang/Object;I)V 2
aload 0
getfield android/support/v4/view/et/a Landroid/support/v4/view/ViewPager;
invokestatic android/support/v4/view/ViewPager/c(Landroid/support/v4/view/ViewPager;)I
istore 3
getstatic android/support/v4/view/a/bd/a Landroid/support/v4/view/a/bg;
aload 1
getfield android/support/v4/view/a/bd/b Ljava/lang/Object;
iload 3
invokeinterface android/support/v4/view/a/bg/c(Ljava/lang/Object;I)V 2
aload 0
getfield android/support/v4/view/et/a Landroid/support/v4/view/ViewPager;
invokestatic android/support/v4/view/ViewPager/c(Landroid/support/v4/view/ViewPager;)I
istore 3
getstatic android/support/v4/view/a/bd/a Landroid/support/v4/view/a/bg;
aload 1
getfield android/support/v4/view/a/bd/b Ljava/lang/Object;
iload 3
invokeinterface android/support/v4/view/a/bg/h(Ljava/lang/Object;I)V 2
L0:
return
.limit locals 4
.limit stack 3
.end method

.method public final a(Landroid/view/View;ILandroid/os/Bundle;)Z
aload 0
aload 1
iload 2
aload 3
invokespecial android/support/v4/view/a/a(Landroid/view/View;ILandroid/os/Bundle;)Z
ifeq L0
iconst_1
ireturn
L0:
iload 2
lookupswitch
4096 : L1
8192 : L2
default : L3
L3:
iconst_0
ireturn
L1:
aload 0
getfield android/support/v4/view/et/a Landroid/support/v4/view/ViewPager;
iconst_1
invokevirtual android/support/v4/view/ViewPager/canScrollHorizontally(I)Z
ifeq L4
aload 0
getfield android/support/v4/view/et/a Landroid/support/v4/view/ViewPager;
aload 0
getfield android/support/v4/view/et/a Landroid/support/v4/view/ViewPager;
invokestatic android/support/v4/view/ViewPager/c(Landroid/support/v4/view/ViewPager;)I
iconst_1
iadd
invokevirtual android/support/v4/view/ViewPager/setCurrentItem(I)V
iconst_1
ireturn
L4:
iconst_0
ireturn
L2:
aload 0
getfield android/support/v4/view/et/a Landroid/support/v4/view/ViewPager;
iconst_m1
invokevirtual android/support/v4/view/ViewPager/canScrollHorizontally(I)Z
ifeq L5
aload 0
getfield android/support/v4/view/et/a Landroid/support/v4/view/ViewPager;
aload 0
getfield android/support/v4/view/et/a Landroid/support/v4/view/ViewPager;
invokestatic android/support/v4/view/ViewPager/c(Landroid/support/v4/view/ViewPager;)I
iconst_1
isub
invokevirtual android/support/v4/view/ViewPager/setCurrentItem(I)V
iconst_1
ireturn
L5:
iconst_0
ireturn
.limit locals 4
.limit stack 4
.end method
