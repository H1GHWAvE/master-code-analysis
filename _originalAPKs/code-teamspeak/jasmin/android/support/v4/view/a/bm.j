.bytecode 50.0
.class public final synchronized android/support/v4/view/a/bm
.super java/lang/Object

.field public static final 'a' I = 1


.field public static final 'b' I = 2


.field public static final 'c' I = 3


.field public static final 'd' I = 4


.field private static final 'e' Landroid/support/v4/view/a/bp;

.field private static final 'g' I = -1


.field private 'f' Ljava/lang/Object;

.method static <clinit>()V
getstatic android/os/Build$VERSION/SDK_INT I
bipush 21
if_icmplt L0
new android/support/v4/view/a/bo
dup
iconst_0
invokespecial android/support/v4/view/a/bo/<init>(B)V
putstatic android/support/v4/view/a/bm/e Landroid/support/v4/view/a/bp;
return
L0:
new android/support/v4/view/a/bq
dup
iconst_0
invokespecial android/support/v4/view/a/bq/<init>(B)V
putstatic android/support/v4/view/a/bm/e Landroid/support/v4/view/a/bp;
return
.limit locals 0
.limit stack 3
.end method

.method private <init>(Ljava/lang/Object;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
putfield android/support/v4/view/a/bm/f Ljava/lang/Object;
return
.limit locals 2
.limit stack 2
.end method

.method private a()I
getstatic android/support/v4/view/a/bm/e Landroid/support/v4/view/a/bp;
aload 0
getfield android/support/v4/view/a/bm/f Ljava/lang/Object;
invokeinterface android/support/v4/view/a/bp/b(Ljava/lang/Object;)I 1
ireturn
.limit locals 1
.limit stack 2
.end method

.method private a(I)Landroid/support/v4/view/a/bm;
getstatic android/support/v4/view/a/bm/e Landroid/support/v4/view/a/bp;
aload 0
getfield android/support/v4/view/a/bm/f Ljava/lang/Object;
iload 1
invokeinterface android/support/v4/view/a/bp/a(Ljava/lang/Object;I)Ljava/lang/Object; 2
invokestatic android/support/v4/view/a/bm/a(Ljava/lang/Object;)Landroid/support/v4/view/a/bm;
areturn
.limit locals 2
.limit stack 3
.end method

.method private static a(Landroid/support/v4/view/a/bm;)Landroid/support/v4/view/a/bm;
getstatic android/support/v4/view/a/bm/e Landroid/support/v4/view/a/bp;
aload 0
getfield android/support/v4/view/a/bm/f Ljava/lang/Object;
invokeinterface android/support/v4/view/a/bp/a(Ljava/lang/Object;)Ljava/lang/Object; 1
invokestatic android/support/v4/view/a/bm/a(Ljava/lang/Object;)Landroid/support/v4/view/a/bm;
areturn
.limit locals 1
.limit stack 2
.end method

.method static a(Ljava/lang/Object;)Landroid/support/v4/view/a/bm;
aload 0
ifnull L0
new android/support/v4/view/a/bm
dup
aload 0
invokespecial android/support/v4/view/a/bm/<init>(Ljava/lang/Object;)V
areturn
L0:
aconst_null
areturn
.limit locals 1
.limit stack 3
.end method

.method private a(Landroid/graphics/Rect;)V
getstatic android/support/v4/view/a/bm/e Landroid/support/v4/view/a/bp;
aload 0
getfield android/support/v4/view/a/bm/f Ljava/lang/Object;
aload 1
invokeinterface android/support/v4/view/a/bp/a(Ljava/lang/Object;Landroid/graphics/Rect;)V 2
return
.limit locals 2
.limit stack 3
.end method

.method private b()I
getstatic android/support/v4/view/a/bm/e Landroid/support/v4/view/a/bp;
aload 0
getfield android/support/v4/view/a/bm/f Ljava/lang/Object;
invokeinterface android/support/v4/view/a/bp/c(Ljava/lang/Object;)I 1
ireturn
.limit locals 1
.limit stack 2
.end method

.method private static b(I)Ljava/lang/String;
iload 0
tableswitch 1
L0
L1
L2
L3
default : L4
L4:
ldc "<UNKNOWN>"
areturn
L0:
ldc "TYPE_APPLICATION"
areturn
L1:
ldc "TYPE_INPUT_METHOD"
areturn
L2:
ldc "TYPE_SYSTEM"
areturn
L3:
ldc "TYPE_ACCESSIBILITY_OVERLAY"
areturn
.limit locals 1
.limit stack 1
.end method

.method private c()Landroid/support/v4/view/a/q;
getstatic android/support/v4/view/a/bm/e Landroid/support/v4/view/a/bp;
aload 0
getfield android/support/v4/view/a/bm/f Ljava/lang/Object;
invokeinterface android/support/v4/view/a/bp/d(Ljava/lang/Object;)Ljava/lang/Object; 1
invokestatic android/support/v4/view/a/q/a(Ljava/lang/Object;)Landroid/support/v4/view/a/q;
areturn
.limit locals 1
.limit stack 2
.end method

.method private d()Landroid/support/v4/view/a/bm;
getstatic android/support/v4/view/a/bm/e Landroid/support/v4/view/a/bp;
aload 0
getfield android/support/v4/view/a/bm/f Ljava/lang/Object;
invokeinterface android/support/v4/view/a/bp/e(Ljava/lang/Object;)Ljava/lang/Object; 1
invokestatic android/support/v4/view/a/bm/a(Ljava/lang/Object;)Landroid/support/v4/view/a/bm;
areturn
.limit locals 1
.limit stack 2
.end method

.method private e()I
getstatic android/support/v4/view/a/bm/e Landroid/support/v4/view/a/bp;
aload 0
getfield android/support/v4/view/a/bm/f Ljava/lang/Object;
invokeinterface android/support/v4/view/a/bp/f(Ljava/lang/Object;)I 1
ireturn
.limit locals 1
.limit stack 2
.end method

.method private f()Z
getstatic android/support/v4/view/a/bm/e Landroid/support/v4/view/a/bp;
aload 0
getfield android/support/v4/view/a/bm/f Ljava/lang/Object;
invokeinterface android/support/v4/view/a/bp/g(Ljava/lang/Object;)Z 1
ireturn
.limit locals 1
.limit stack 2
.end method

.method private g()Z
getstatic android/support/v4/view/a/bm/e Landroid/support/v4/view/a/bp;
aload 0
getfield android/support/v4/view/a/bm/f Ljava/lang/Object;
invokeinterface android/support/v4/view/a/bp/h(Ljava/lang/Object;)Z 1
ireturn
.limit locals 1
.limit stack 2
.end method

.method private h()Z
getstatic android/support/v4/view/a/bm/e Landroid/support/v4/view/a/bp;
aload 0
getfield android/support/v4/view/a/bm/f Ljava/lang/Object;
invokeinterface android/support/v4/view/a/bp/i(Ljava/lang/Object;)Z 1
ireturn
.limit locals 1
.limit stack 2
.end method

.method private i()I
getstatic android/support/v4/view/a/bm/e Landroid/support/v4/view/a/bp;
aload 0
getfield android/support/v4/view/a/bm/f Ljava/lang/Object;
invokeinterface android/support/v4/view/a/bp/j(Ljava/lang/Object;)I 1
ireturn
.limit locals 1
.limit stack 2
.end method

.method private static j()Landroid/support/v4/view/a/bm;
getstatic android/support/v4/view/a/bm/e Landroid/support/v4/view/a/bp;
invokeinterface android/support/v4/view/a/bp/a()Ljava/lang/Object; 0
invokestatic android/support/v4/view/a/bm/a(Ljava/lang/Object;)Landroid/support/v4/view/a/bm;
areturn
.limit locals 0
.limit stack 1
.end method

.method private k()V
getstatic android/support/v4/view/a/bm/e Landroid/support/v4/view/a/bp;
aload 0
getfield android/support/v4/view/a/bm/f Ljava/lang/Object;
invokeinterface android/support/v4/view/a/bp/k(Ljava/lang/Object;)V 1
return
.limit locals 1
.limit stack 2
.end method

.method public final equals(Ljava/lang/Object;)Z
aload 0
aload 1
if_acmpne L0
L1:
iconst_1
ireturn
L0:
aload 1
ifnonnull L2
iconst_0
ireturn
L2:
aload 0
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
aload 1
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
if_acmpeq L3
iconst_0
ireturn
L3:
aload 1
checkcast android/support/v4/view/a/bm
astore 1
aload 0
getfield android/support/v4/view/a/bm/f Ljava/lang/Object;
ifnonnull L4
aload 1
getfield android/support/v4/view/a/bm/f Ljava/lang/Object;
ifnull L1
iconst_0
ireturn
L4:
aload 0
getfield android/support/v4/view/a/bm/f Ljava/lang/Object;
aload 1
getfield android/support/v4/view/a/bm/f Ljava/lang/Object;
invokevirtual java/lang/Object/equals(Ljava/lang/Object;)Z
ifne L1
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final hashCode()I
aload 0
getfield android/support/v4/view/a/bm/f Ljava/lang/Object;
ifnonnull L0
iconst_0
ireturn
L0:
aload 0
getfield android/support/v4/view/a/bm/f Ljava/lang/Object;
invokevirtual java/lang/Object/hashCode()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final toString()Ljava/lang/String;
iconst_1
istore 2
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
astore 4
new android/graphics/Rect
dup
invokespecial android/graphics/Rect/<init>()V
astore 5
getstatic android/support/v4/view/a/bm/e Landroid/support/v4/view/a/bp;
aload 0
getfield android/support/v4/view/a/bm/f Ljava/lang/Object;
aload 5
invokeinterface android/support/v4/view/a/bp/a(Ljava/lang/Object;Landroid/graphics/Rect;)V 2
aload 4
ldc "AccessibilityWindowInfo["
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
aload 4
ldc "id="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic android/support/v4/view/a/bm/e Landroid/support/v4/view/a/bp;
aload 0
getfield android/support/v4/view/a/bm/f Ljava/lang/Object;
invokeinterface android/support/v4/view/a/bp/f(Ljava/lang/Object;)I 1
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
pop
aload 4
ldc ", type="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
astore 6
getstatic android/support/v4/view/a/bm/e Landroid/support/v4/view/a/bp;
aload 0
getfield android/support/v4/view/a/bm/f Ljava/lang/Object;
invokeinterface android/support/v4/view/a/bp/b(Ljava/lang/Object;)I 1
tableswitch 1
L0
L1
L2
L3
default : L4
L4:
ldc "<UNKNOWN>"
astore 3
L5:
aload 6
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
aload 4
ldc ", layer="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic android/support/v4/view/a/bm/e Landroid/support/v4/view/a/bp;
aload 0
getfield android/support/v4/view/a/bm/f Ljava/lang/Object;
invokeinterface android/support/v4/view/a/bp/c(Ljava/lang/Object;)I 1
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
pop
aload 4
ldc ", bounds="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 5
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
pop
aload 4
ldc ", focused="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic android/support/v4/view/a/bm/e Landroid/support/v4/view/a/bp;
aload 0
getfield android/support/v4/view/a/bm/f Ljava/lang/Object;
invokeinterface android/support/v4/view/a/bp/h(Ljava/lang/Object;)Z 1
invokevirtual java/lang/StringBuilder/append(Z)Ljava/lang/StringBuilder;
pop
aload 4
ldc ", active="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic android/support/v4/view/a/bm/e Landroid/support/v4/view/a/bp;
aload 0
getfield android/support/v4/view/a/bm/f Ljava/lang/Object;
invokeinterface android/support/v4/view/a/bp/g(Ljava/lang/Object;)Z 1
invokevirtual java/lang/StringBuilder/append(Z)Ljava/lang/StringBuilder;
pop
aload 4
ldc ", hasParent="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
astore 3
getstatic android/support/v4/view/a/bm/e Landroid/support/v4/view/a/bp;
aload 0
getfield android/support/v4/view/a/bm/f Ljava/lang/Object;
invokeinterface android/support/v4/view/a/bp/e(Ljava/lang/Object;)Ljava/lang/Object; 1
invokestatic android/support/v4/view/a/bm/a(Ljava/lang/Object;)Landroid/support/v4/view/a/bm;
ifnull L6
iconst_1
istore 1
L7:
aload 3
iload 1
invokevirtual java/lang/StringBuilder/append(Z)Ljava/lang/StringBuilder;
pop
aload 4
ldc ", hasChildren="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
astore 3
getstatic android/support/v4/view/a/bm/e Landroid/support/v4/view/a/bp;
aload 0
getfield android/support/v4/view/a/bm/f Ljava/lang/Object;
invokeinterface android/support/v4/view/a/bp/j(Ljava/lang/Object;)I 1
ifle L8
iload 2
istore 1
L9:
aload 3
iload 1
invokevirtual java/lang/StringBuilder/append(Z)Ljava/lang/StringBuilder;
pop
aload 4
bipush 93
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
pop
aload 4
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
L0:
ldc "TYPE_APPLICATION"
astore 3
goto L5
L1:
ldc "TYPE_INPUT_METHOD"
astore 3
goto L5
L2:
ldc "TYPE_SYSTEM"
astore 3
goto L5
L3:
ldc "TYPE_ACCESSIBILITY_OVERLAY"
astore 3
goto L5
L6:
iconst_0
istore 1
goto L7
L8:
iconst_0
istore 1
goto L9
.limit locals 7
.limit stack 3
.end method
