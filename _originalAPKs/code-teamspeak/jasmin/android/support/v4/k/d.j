.bytecode 50.0
.class final synchronized android/support/v4/k/d
.super android/support/v4/k/a

.field private 'b' Ljava/io/File;

.method <init>(Landroid/support/v4/k/a;Ljava/io/File;)V
aload 0
aload 1
invokespecial android/support/v4/k/a/<init>(Landroid/support/v4/k/a;)V
aload 0
aload 2
putfield android/support/v4/k/d/b Ljava/io/File;
return
.limit locals 3
.limit stack 2
.end method

.method private static a(Ljava/io/File;)Z
aload 0
invokevirtual java/io/File/listFiles()[Ljava/io/File;
astore 0
iconst_1
istore 4
iconst_1
istore 3
aload 0
ifnull L0
aload 0
arraylength
istore 2
iconst_0
istore 1
L1:
iload 3
istore 4
iload 1
iload 2
if_icmpge L0
aload 0
iload 1
aaload
astore 5
iload 3
istore 4
aload 5
invokevirtual java/io/File/isDirectory()Z
ifeq L2
iload 3
aload 5
invokestatic android/support/v4/k/d/a(Ljava/io/File;)Z
iand
istore 4
L2:
iload 4
istore 3
aload 5
invokevirtual java/io/File/delete()Z
ifne L3
ldc "DocumentFile"
new java/lang/StringBuilder
dup
ldc "Failed to delete "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 5
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/String;)I
pop
iconst_0
istore 3
L3:
iload 1
iconst_1
iadd
istore 1
goto L1
L0:
iload 4
ireturn
.limit locals 6
.limit stack 4
.end method

.method private static c(Ljava/lang/String;)Ljava/lang/String;
aload 0
bipush 46
invokevirtual java/lang/String/lastIndexOf(I)I
istore 1
iload 1
iflt L0
aload 0
iload 1
iconst_1
iadd
invokevirtual java/lang/String/substring(I)Ljava/lang/String;
invokevirtual java/lang/String/toLowerCase()Ljava/lang/String;
astore 0
invokestatic android/webkit/MimeTypeMap/getSingleton()Landroid/webkit/MimeTypeMap;
aload 0
invokevirtual android/webkit/MimeTypeMap/getMimeTypeFromExtension(Ljava/lang/String;)Ljava/lang/String;
astore 0
aload 0
ifnull L0
aload 0
areturn
L0:
ldc "application/octet-stream"
areturn
.limit locals 2
.limit stack 3
.end method

.method public final a()Landroid/net/Uri;
aload 0
getfield android/support/v4/k/d/b Ljava/io/File;
invokestatic android/net/Uri/fromFile(Ljava/io/File;)Landroid/net/Uri;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final a(Ljava/lang/String;)Landroid/support/v4/k/a;
new java/io/File
dup
aload 0
getfield android/support/v4/k/d/b Ljava/io/File;
aload 1
invokespecial java/io/File/<init>(Ljava/io/File;Ljava/lang/String;)V
astore 1
aload 1
invokevirtual java/io/File/isDirectory()Z
ifne L0
aload 1
invokevirtual java/io/File/mkdir()Z
ifeq L1
L0:
new android/support/v4/k/d
dup
aload 0
aload 1
invokespecial android/support/v4/k/d/<init>(Landroid/support/v4/k/a;Ljava/io/File;)V
areturn
L1:
aconst_null
areturn
.limit locals 2
.limit stack 4
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)Landroid/support/v4/k/a;
.catch java/io/IOException from L0 to L1 using L2
invokestatic android/webkit/MimeTypeMap/getSingleton()Landroid/webkit/MimeTypeMap;
aload 1
invokevirtual android/webkit/MimeTypeMap/getExtensionFromMimeType(Ljava/lang/String;)Ljava/lang/String;
astore 3
aload 2
astore 1
aload 3
ifnull L3
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "."
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 1
L3:
new java/io/File
dup
aload 0
getfield android/support/v4/k/d/b Ljava/io/File;
aload 1
invokespecial java/io/File/<init>(Ljava/io/File;Ljava/lang/String;)V
astore 1
L0:
aload 1
invokevirtual java/io/File/createNewFile()Z
pop
new android/support/v4/k/d
dup
aload 0
aload 1
invokespecial android/support/v4/k/d/<init>(Landroid/support/v4/k/a;Ljava/io/File;)V
astore 1
L1:
aload 1
areturn
L2:
astore 1
ldc "DocumentFile"
new java/lang/StringBuilder
dup
ldc "Failed to createFile: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/String;)I
pop
aconst_null
areturn
.limit locals 4
.limit stack 4
.end method

.method public final b()Ljava/lang/String;
aload 0
getfield android/support/v4/k/d/b Ljava/io/File;
invokevirtual java/io/File/getName()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final b(Ljava/lang/String;)Z
new java/io/File
dup
aload 0
getfield android/support/v4/k/d/b Ljava/io/File;
invokevirtual java/io/File/getParentFile()Ljava/io/File;
aload 1
invokespecial java/io/File/<init>(Ljava/io/File;Ljava/lang/String;)V
astore 1
aload 0
getfield android/support/v4/k/d/b Ljava/io/File;
aload 1
invokevirtual java/io/File/renameTo(Ljava/io/File;)Z
ifeq L0
aload 0
aload 1
putfield android/support/v4/k/d/b Ljava/io/File;
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 4
.end method

.method public final c()Ljava/lang/String;
aload 0
getfield android/support/v4/k/d/b Ljava/io/File;
invokevirtual java/io/File/isDirectory()Z
ifeq L0
aconst_null
astore 2
L1:
aload 2
areturn
L0:
aload 0
getfield android/support/v4/k/d/b Ljava/io/File;
invokevirtual java/io/File/getName()Ljava/lang/String;
astore 2
aload 2
bipush 46
invokevirtual java/lang/String/lastIndexOf(I)I
istore 1
iload 1
iflt L2
aload 2
iload 1
iconst_1
iadd
invokevirtual java/lang/String/substring(I)Ljava/lang/String;
invokevirtual java/lang/String/toLowerCase()Ljava/lang/String;
astore 2
invokestatic android/webkit/MimeTypeMap/getSingleton()Landroid/webkit/MimeTypeMap;
aload 2
invokevirtual android/webkit/MimeTypeMap/getMimeTypeFromExtension(Ljava/lang/String;)Ljava/lang/String;
astore 3
aload 3
astore 2
aload 3
ifnonnull L1
L2:
ldc "application/octet-stream"
areturn
.limit locals 4
.limit stack 3
.end method

.method public final d()Z
aload 0
getfield android/support/v4/k/d/b Ljava/io/File;
invokevirtual java/io/File/isDirectory()Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final e()Z
aload 0
getfield android/support/v4/k/d/b Ljava/io/File;
invokevirtual java/io/File/isFile()Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final f()J
aload 0
getfield android/support/v4/k/d/b Ljava/io/File;
invokevirtual java/io/File/lastModified()J
lreturn
.limit locals 1
.limit stack 2
.end method

.method public final g()J
aload 0
getfield android/support/v4/k/d/b Ljava/io/File;
invokevirtual java/io/File/length()J
lreturn
.limit locals 1
.limit stack 2
.end method

.method public final h()Z
aload 0
getfield android/support/v4/k/d/b Ljava/io/File;
invokevirtual java/io/File/canRead()Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final i()Z
aload 0
getfield android/support/v4/k/d/b Ljava/io/File;
invokevirtual java/io/File/canWrite()Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final j()Z
aload 0
getfield android/support/v4/k/d/b Ljava/io/File;
invokestatic android/support/v4/k/d/a(Ljava/io/File;)Z
pop
aload 0
getfield android/support/v4/k/d/b Ljava/io/File;
invokevirtual java/io/File/delete()Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final k()Z
aload 0
getfield android/support/v4/k/d/b Ljava/io/File;
invokevirtual java/io/File/exists()Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final l()[Landroid/support/v4/k/a;
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 3
aload 0
getfield android/support/v4/k/d/b Ljava/io/File;
invokevirtual java/io/File/listFiles()[Ljava/io/File;
astore 4
aload 4
ifnull L0
aload 4
arraylength
istore 2
iconst_0
istore 1
L1:
iload 1
iload 2
if_icmpge L0
aload 3
new android/support/v4/k/d
dup
aload 0
aload 4
iload 1
aaload
invokespecial android/support/v4/k/d/<init>(Landroid/support/v4/k/a;Ljava/io/File;)V
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
iload 1
iconst_1
iadd
istore 1
goto L1
L0:
aload 3
aload 3
invokevirtual java/util/ArrayList/size()I
anewarray android/support/v4/k/a
invokevirtual java/util/ArrayList/toArray([Ljava/lang/Object;)[Ljava/lang/Object;
checkcast [Landroid/support/v4/k/a;
areturn
.limit locals 5
.limit stack 6
.end method
