.bytecode 50.0
.class public final synchronized android/support/v4/m/a
.super java/lang/Object

.field private static 'a' Landroid/support/v4/m/l;

.field private static final 'b' C = 8234


.field private static final 'c' C = 8235


.field private static final 'd' C = 8236


.field private static final 'e' C = 8206


.field private static final 'f' C = 8207


.field private static final 'g' Ljava/lang/String;

.field private static final 'h' Ljava/lang/String;

.field private static final 'i' Ljava/lang/String; = ""

.field private static final 'j' I = 2


.field private static final 'k' I = 2


.field private static final 'l' Landroid/support/v4/m/a;

.field private static final 'm' Landroid/support/v4/m/a;

.field private static final 'q' I = -1


.field private static final 'r' I = 0


.field private static final 's' I = 1


.field private final 'n' Z

.field private final 'o' I

.field private final 'p' Landroid/support/v4/m/l;

.method static <clinit>()V
getstatic android/support/v4/m/m/c Landroid/support/v4/m/l;
putstatic android/support/v4/m/a/a Landroid/support/v4/m/l;
sipush 8206
invokestatic java/lang/Character/toString(C)Ljava/lang/String;
putstatic android/support/v4/m/a/g Ljava/lang/String;
sipush 8207
invokestatic java/lang/Character/toString(C)Ljava/lang/String;
putstatic android/support/v4/m/a/h Ljava/lang/String;
new android/support/v4/m/a
dup
iconst_0
iconst_2
getstatic android/support/v4/m/a/a Landroid/support/v4/m/l;
invokespecial android/support/v4/m/a/<init>(ZILandroid/support/v4/m/l;)V
putstatic android/support/v4/m/a/l Landroid/support/v4/m/a;
new android/support/v4/m/a
dup
iconst_1
iconst_2
getstatic android/support/v4/m/a/a Landroid/support/v4/m/l;
invokespecial android/support/v4/m/a/<init>(ZILandroid/support/v4/m/l;)V
putstatic android/support/v4/m/a/m Landroid/support/v4/m/a;
return
.limit locals 0
.limit stack 5
.end method

.method private <init>(ZILandroid/support/v4/m/l;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
iload 1
putfield android/support/v4/m/a/n Z
aload 0
iload 2
putfield android/support/v4/m/a/o I
aload 0
aload 3
putfield android/support/v4/m/a/p Landroid/support/v4/m/l;
return
.limit locals 4
.limit stack 2
.end method

.method synthetic <init>(ZILandroid/support/v4/m/l;B)V
aload 0
iload 1
iload 2
aload 3
invokespecial android/support/v4/m/a/<init>(ZILandroid/support/v4/m/l;)V
return
.limit locals 5
.limit stack 4
.end method

.method private static a(Z)Landroid/support/v4/m/a;
new android/support/v4/m/c
dup
iload 0
invokespecial android/support/v4/m/c/<init>(Z)V
invokevirtual android/support/v4/m/c/a()Landroid/support/v4/m/a;
areturn
.limit locals 1
.limit stack 3
.end method

.method static synthetic a()Landroid/support/v4/m/l;
getstatic android/support/v4/m/a/a Landroid/support/v4/m/l;
areturn
.limit locals 0
.limit stack 1
.end method

.method private a(Ljava/lang/String;Landroid/support/v4/m/l;)Ljava/lang/String;
aload 2
aload 1
iconst_0
aload 1
invokevirtual java/lang/String/length()I
invokeinterface android/support/v4/m/l/a(Ljava/lang/CharSequence;II)Z 3
istore 3
aload 0
getfield android/support/v4/m/a/n Z
ifne L0
iload 3
ifne L1
aload 1
invokestatic android/support/v4/m/a/c(Ljava/lang/String;)I
iconst_1
if_icmpne L0
L1:
getstatic android/support/v4/m/a/g Ljava/lang/String;
areturn
L0:
aload 0
getfield android/support/v4/m/a/n Z
ifeq L2
iload 3
ifeq L3
aload 1
invokestatic android/support/v4/m/a/c(Ljava/lang/String;)I
iconst_m1
if_icmpne L2
L3:
getstatic android/support/v4/m/a/h Ljava/lang/String;
areturn
L2:
ldc ""
areturn
.limit locals 4
.limit stack 4
.end method

.method private a(Ljava/lang/String;Landroid/support/v4/m/l;Z)Ljava/lang/String;
aload 1
ifnonnull L0
aconst_null
areturn
L0:
aload 2
aload 1
iconst_0
aload 1
invokevirtual java/lang/String/length()I
invokeinterface android/support/v4/m/l/a(Ljava/lang/CharSequence;II)Z 3
istore 6
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
astore 8
aload 0
getfield android/support/v4/m/a/o I
iconst_2
iand
ifeq L1
iconst_1
istore 5
L2:
iload 5
ifeq L3
iload 3
ifeq L3
iload 6
ifeq L4
getstatic android/support/v4/m/m/b Landroid/support/v4/m/l;
astore 2
L5:
aload 2
aload 1
iconst_0
aload 1
invokevirtual java/lang/String/length()I
invokeinterface android/support/v4/m/l/a(Ljava/lang/CharSequence;II)Z 3
istore 7
aload 0
getfield android/support/v4/m/a/n Z
ifne L6
iload 7
ifne L7
aload 1
invokestatic android/support/v4/m/a/d(Ljava/lang/String;)I
iconst_1
if_icmpne L6
L7:
getstatic android/support/v4/m/a/g Ljava/lang/String;
astore 2
L8:
aload 8
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
L3:
iload 6
aload 0
getfield android/support/v4/m/a/n Z
if_icmpeq L9
iload 6
ifeq L10
sipush 8235
istore 4
L11:
aload 8
iload 4
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
pop
aload 8
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
aload 8
sipush 8236
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
pop
L12:
iload 3
ifeq L13
iload 6
ifeq L14
getstatic android/support/v4/m/m/b Landroid/support/v4/m/l;
astore 2
L15:
aload 2
aload 1
iconst_0
aload 1
invokevirtual java/lang/String/length()I
invokeinterface android/support/v4/m/l/a(Ljava/lang/CharSequence;II)Z 3
istore 3
aload 0
getfield android/support/v4/m/a/n Z
ifne L16
iload 3
ifne L17
aload 1
invokestatic android/support/v4/m/a/c(Ljava/lang/String;)I
iconst_1
if_icmpne L16
L17:
getstatic android/support/v4/m/a/g Ljava/lang/String;
astore 1
L18:
aload 8
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
L13:
aload 8
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
L1:
iconst_0
istore 5
goto L2
L4:
getstatic android/support/v4/m/m/a Landroid/support/v4/m/l;
astore 2
goto L5
L6:
aload 0
getfield android/support/v4/m/a/n Z
ifeq L19
iload 7
ifeq L20
aload 1
invokestatic android/support/v4/m/a/d(Ljava/lang/String;)I
iconst_m1
if_icmpne L19
L20:
getstatic android/support/v4/m/a/h Ljava/lang/String;
astore 2
goto L8
L19:
ldc ""
astore 2
goto L8
L10:
sipush 8234
istore 4
goto L11
L9:
aload 8
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
goto L12
L14:
getstatic android/support/v4/m/m/a Landroid/support/v4/m/l;
astore 2
goto L15
L16:
aload 0
getfield android/support/v4/m/a/n Z
ifeq L21
iload 3
ifeq L22
aload 1
invokestatic android/support/v4/m/a/c(Ljava/lang/String;)I
iconst_m1
if_icmpne L21
L22:
getstatic android/support/v4/m/a/h Ljava/lang/String;
astore 1
goto L18
L21:
ldc ""
astore 1
goto L18
.limit locals 9
.limit stack 4
.end method

.method private a(Ljava/lang/String;Z)Ljava/lang/String;
aload 0
aload 1
aload 0
getfield android/support/v4/m/a/p Landroid/support/v4/m/l;
iload 2
invokespecial android/support/v4/m/a/a(Ljava/lang/String;Landroid/support/v4/m/l;Z)Ljava/lang/String;
areturn
.limit locals 3
.limit stack 4
.end method

.method private a(Ljava/lang/String;)Z
aload 0
getfield android/support/v4/m/a/p Landroid/support/v4/m/l;
aload 1
iconst_0
aload 1
invokevirtual java/lang/String/length()I
invokeinterface android/support/v4/m/l/a(Ljava/lang/CharSequence;II)Z 3
ireturn
.limit locals 2
.limit stack 4
.end method

.method static synthetic a(Ljava/util/Locale;)Z
aload 0
invokestatic android/support/v4/m/u/a(Ljava/util/Locale;)I
iconst_1
if_icmpne L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method static synthetic b()Landroid/support/v4/m/a;
getstatic android/support/v4/m/a/m Landroid/support/v4/m/a;
areturn
.limit locals 0
.limit stack 1
.end method

.method private static b(Ljava/util/Locale;)Landroid/support/v4/m/a;
new android/support/v4/m/c
dup
aload 0
invokespecial android/support/v4/m/c/<init>(Ljava/util/Locale;)V
invokevirtual android/support/v4/m/c/a()Landroid/support/v4/m/a;
areturn
.limit locals 1
.limit stack 3
.end method

.method private b(Ljava/lang/String;)Ljava/lang/String;
aload 0
aload 1
aload 0
getfield android/support/v4/m/a/p Landroid/support/v4/m/l;
iconst_1
invokespecial android/support/v4/m/a/a(Ljava/lang/String;Landroid/support/v4/m/l;Z)Ljava/lang/String;
areturn
.limit locals 2
.limit stack 4
.end method

.method private b(Ljava/lang/String;Landroid/support/v4/m/l;)Ljava/lang/String;
aload 2
aload 1
iconst_0
aload 1
invokevirtual java/lang/String/length()I
invokeinterface android/support/v4/m/l/a(Ljava/lang/CharSequence;II)Z 3
istore 3
aload 0
getfield android/support/v4/m/a/n Z
ifne L0
iload 3
ifne L1
aload 1
invokestatic android/support/v4/m/a/d(Ljava/lang/String;)I
iconst_1
if_icmpne L0
L1:
getstatic android/support/v4/m/a/g Ljava/lang/String;
areturn
L0:
aload 0
getfield android/support/v4/m/a/n Z
ifeq L2
iload 3
ifeq L3
aload 1
invokestatic android/support/v4/m/a/d(Ljava/lang/String;)I
iconst_m1
if_icmpne L2
L3:
getstatic android/support/v4/m/a/h Ljava/lang/String;
areturn
L2:
ldc ""
areturn
.limit locals 4
.limit stack 4
.end method

.method private static c(Ljava/lang/String;)I
iconst_0
istore 4
new android/support/v4/m/d
dup
aload 0
invokespecial android/support/v4/m/d/<init>(Ljava/lang/String;)V
astore 0
aload 0
aload 0
getfield android/support/v4/m/d/c I
putfield android/support/v4/m/d/d I
iconst_0
istore 2
iconst_0
istore 1
L0:
iload 4
istore 3
aload 0
getfield android/support/v4/m/d/d I
ifle L1
aload 0
invokevirtual android/support/v4/m/d/a()B
tableswitch 0
L2
L3
L3
L4
L4
L4
L4
L4
L4
L0
L4
L4
L4
L4
L5
L5
L6
L6
L7
default : L4
L4:
iload 2
ifne L0
iload 1
istore 2
goto L0
L2:
iload 1
ifne L8
iconst_m1
istore 3
L1:
iload 3
ireturn
L8:
iload 2
ifne L0
iload 1
istore 2
goto L0
L5:
iload 2
iload 1
if_icmpne L9
iconst_m1
ireturn
L9:
iload 1
iconst_1
isub
istore 1
goto L0
L3:
iload 1
ifne L10
iconst_1
ireturn
L10:
iload 2
ifne L0
iload 1
istore 2
goto L0
L6:
iload 2
iload 1
if_icmpne L11
iconst_1
ireturn
L11:
iload 1
iconst_1
isub
istore 1
goto L0
L7:
iload 1
iconst_1
iadd
istore 1
goto L0
.limit locals 5
.limit stack 3
.end method

.method static synthetic c()Landroid/support/v4/m/a;
getstatic android/support/v4/m/a/l Landroid/support/v4/m/a;
areturn
.limit locals 0
.limit stack 1
.end method

.method private c(Ljava/lang/String;Landroid/support/v4/m/l;)Ljava/lang/String;
aload 0
aload 1
aload 2
iconst_1
invokespecial android/support/v4/m/a/a(Ljava/lang/String;Landroid/support/v4/m/l;Z)Ljava/lang/String;
areturn
.limit locals 3
.limit stack 4
.end method

.method private static c(Ljava/util/Locale;)Z
aload 0
invokestatic android/support/v4/m/u/a(Ljava/util/Locale;)I
iconst_1
if_icmpne L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method private static d(Ljava/lang/String;)I
new android/support/v4/m/d
dup
aload 0
invokespecial android/support/v4/m/d/<init>(Ljava/lang/String;)V
astore 0
aload 0
iconst_0
putfield android/support/v4/m/d/d I
iconst_0
istore 4
iconst_0
istore 5
iconst_0
istore 2
L0:
aload 0
getfield android/support/v4/m/d/d I
aload 0
getfield android/support/v4/m/d/c I
if_icmpge L1
iload 4
ifne L1
aload 0
aload 0
getfield android/support/v4/m/d/a Ljava/lang/String;
aload 0
getfield android/support/v4/m/d/d I
invokevirtual java/lang/String/charAt(I)C
putfield android/support/v4/m/d/e C
aload 0
getfield android/support/v4/m/d/e C
invokestatic java/lang/Character/isHighSurrogate(C)Z
ifeq L2
aload 0
getfield android/support/v4/m/d/a Ljava/lang/String;
aload 0
getfield android/support/v4/m/d/d I
invokestatic java/lang/Character/codePointAt(Ljava/lang/CharSequence;I)I
istore 3
aload 0
aload 0
getfield android/support/v4/m/d/d I
iload 3
invokestatic java/lang/Character/charCount(I)I
iadd
putfield android/support/v4/m/d/d I
iload 3
invokestatic java/lang/Character/getDirectionality(I)B
istore 3
L3:
iload 3
tableswitch 0
L4
L5
L5
L6
L6
L6
L6
L6
L6
L0
L6
L6
L6
L6
L7
L7
L8
L8
L9
default : L6
L6:
iload 2
istore 4
goto L0
L2:
aload 0
aload 0
getfield android/support/v4/m/d/d I
iconst_1
iadd
putfield android/support/v4/m/d/d I
aload 0
getfield android/support/v4/m/d/e C
invokestatic android/support/v4/m/d/a(C)B
istore 6
iload 6
istore 3
aload 0
getfield android/support/v4/m/d/b Z
ifeq L3
aload 0
getfield android/support/v4/m/d/e C
bipush 60
if_icmpne L10
aload 0
getfield android/support/v4/m/d/d I
istore 3
L11:
aload 0
getfield android/support/v4/m/d/d I
aload 0
getfield android/support/v4/m/d/c I
if_icmpge L12
aload 0
getfield android/support/v4/m/d/a Ljava/lang/String;
astore 8
aload 0
getfield android/support/v4/m/d/d I
istore 6
aload 0
iload 6
iconst_1
iadd
putfield android/support/v4/m/d/d I
aload 0
aload 8
iload 6
invokevirtual java/lang/String/charAt(I)C
putfield android/support/v4/m/d/e C
aload 0
getfield android/support/v4/m/d/e C
bipush 62
if_icmpne L13
bipush 12
istore 3
goto L3
L13:
aload 0
getfield android/support/v4/m/d/e C
bipush 34
if_icmpeq L14
aload 0
getfield android/support/v4/m/d/e C
bipush 39
if_icmpne L11
L14:
aload 0
getfield android/support/v4/m/d/e C
istore 6
L15:
aload 0
getfield android/support/v4/m/d/d I
aload 0
getfield android/support/v4/m/d/c I
if_icmpge L11
aload 0
getfield android/support/v4/m/d/a Ljava/lang/String;
astore 8
aload 0
getfield android/support/v4/m/d/d I
istore 7
aload 0
iload 7
iconst_1
iadd
putfield android/support/v4/m/d/d I
aload 8
iload 7
invokevirtual java/lang/String/charAt(I)C
istore 1
aload 0
iload 1
putfield android/support/v4/m/d/e C
iload 1
iload 6
if_icmpne L15
goto L11
L12:
aload 0
iload 3
putfield android/support/v4/m/d/d I
aload 0
bipush 60
putfield android/support/v4/m/d/e C
bipush 13
istore 3
goto L3
L10:
iload 6
istore 3
aload 0
getfield android/support/v4/m/d/e C
bipush 38
if_icmpne L3
L16:
aload 0
getfield android/support/v4/m/d/d I
aload 0
getfield android/support/v4/m/d/c I
if_icmpge L17
aload 0
getfield android/support/v4/m/d/a Ljava/lang/String;
astore 8
aload 0
getfield android/support/v4/m/d/d I
istore 3
aload 0
iload 3
iconst_1
iadd
putfield android/support/v4/m/d/d I
aload 8
iload 3
invokevirtual java/lang/String/charAt(I)C
istore 1
aload 0
iload 1
putfield android/support/v4/m/d/e C
iload 1
bipush 59
if_icmpne L16
L17:
bipush 12
istore 3
goto L3
L7:
iload 2
iconst_1
iadd
istore 2
iconst_m1
istore 5
goto L0
L8:
iload 2
iconst_1
iadd
istore 2
iconst_1
istore 5
goto L0
L9:
iload 2
iconst_1
isub
istore 2
iconst_0
istore 5
goto L0
L4:
iload 2
ifne L18
L19:
iconst_m1
ireturn
L18:
iload 2
istore 4
goto L0
L5:
iload 2
ifne L20
iconst_1
ireturn
L20:
iload 2
istore 4
goto L0
L1:
iload 4
ifeq L21
iload 5
ifeq L22
iload 5
ireturn
L22:
aload 0
getfield android/support/v4/m/d/d I
ifle L21
aload 0
invokevirtual android/support/v4/m/d/a()B
tableswitch 14
L23
L23
L24
L24
L25
default : L26
L26:
goto L22
L23:
iload 4
iload 2
if_icmpeq L19
iload 2
iconst_1
isub
istore 2
goto L22
L24:
iload 4
iload 2
if_icmpne L27
iconst_1
ireturn
L27:
iload 2
iconst_1
isub
istore 2
goto L22
L25:
iload 2
iconst_1
iadd
istore 2
goto L22
L21:
iconst_0
ireturn
.limit locals 9
.limit stack 3
.end method

.method private static d()Landroid/support/v4/m/a;
new android/support/v4/m/c
dup
invokespecial android/support/v4/m/c/<init>()V
invokevirtual android/support/v4/m/c/a()Landroid/support/v4/m/a;
areturn
.limit locals 0
.limit stack 2
.end method

.method private e()Z
aload 0
getfield android/support/v4/m/a/n Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private f()Z
aload 0
getfield android/support/v4/m/a/o I
iconst_2
iand
ifeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method
