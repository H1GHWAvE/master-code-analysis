.bytecode 50.0
.class final synchronized android/support/v4/e/a/u
.super android/support/v4/e/a/t

.method <init>(Landroid/graphics/drawable/Drawable;)V
aload 0
aload 1
invokespecial android/support/v4/e/a/t/<init>(Landroid/graphics/drawable/Drawable;)V
return
.limit locals 2
.limit stack 2
.end method

.method public final applyTheme(Landroid/content/res/Resources$Theme;)V
aload 0
getfield android/support/v4/e/a/u/b Landroid/graphics/drawable/Drawable;
aload 1
invokevirtual android/graphics/drawable/Drawable/applyTheme(Landroid/content/res/Resources$Theme;)V
return
.limit locals 2
.limit stack 2
.end method

.method public final canApplyTheme()Z
aload 0
getfield android/support/v4/e/a/u/b Landroid/graphics/drawable/Drawable;
invokevirtual android/graphics/drawable/Drawable/canApplyTheme()Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final getDirtyBounds()Landroid/graphics/Rect;
aload 0
getfield android/support/v4/e/a/u/b Landroid/graphics/drawable/Drawable;
invokevirtual android/graphics/drawable/Drawable/getDirtyBounds()Landroid/graphics/Rect;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final getOutline(Landroid/graphics/Outline;)V
aload 0
getfield android/support/v4/e/a/u/b Landroid/graphics/drawable/Drawable;
aload 1
invokevirtual android/graphics/drawable/Drawable/getOutline(Landroid/graphics/Outline;)V
return
.limit locals 2
.limit stack 2
.end method

.method public final setHotspot(FF)V
aload 0
getfield android/support/v4/e/a/u/b Landroid/graphics/drawable/Drawable;
fload 1
fload 2
invokevirtual android/graphics/drawable/Drawable/setHotspot(FF)V
return
.limit locals 3
.limit stack 3
.end method

.method public final setHotspotBounds(IIII)V
aload 0
getfield android/support/v4/e/a/u/b Landroid/graphics/drawable/Drawable;
iload 1
iload 2
iload 3
iload 4
invokevirtual android/graphics/drawable/Drawable/setHotspotBounds(IIII)V
return
.limit locals 5
.limit stack 5
.end method
