.bytecode 50.0
.class final synchronized android/support/v4/e/a/l
.super java/lang/Object

.method <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;
aload 0
astore 1
aload 0
instanceof android/support/v4/e/a/r
ifne L0
new android/support/v4/e/a/r
dup
aload 0
invokespecial android/support/v4/e/a/r/<init>(Landroid/graphics/drawable/Drawable;)V
astore 1
L0:
aload 1
areturn
.limit locals 2
.limit stack 3
.end method

.method public static a(Landroid/graphics/drawable/Drawable;I)V
aload 0
instanceof android/support/v4/e/a/q
ifeq L0
aload 0
checkcast android/support/v4/e/a/q
iload 1
invokeinterface android/support/v4/e/a/q/setTint(I)V 1
L0:
return
.limit locals 2
.limit stack 2
.end method

.method public static a(Landroid/graphics/drawable/Drawable;Landroid/content/res/ColorStateList;)V
aload 0
instanceof android/support/v4/e/a/q
ifeq L0
aload 0
checkcast android/support/v4/e/a/q
aload 1
invokeinterface android/support/v4/e/a/q/setTintList(Landroid/content/res/ColorStateList;)V 1
L0:
return
.limit locals 2
.limit stack 2
.end method

.method public static a(Landroid/graphics/drawable/Drawable;Landroid/graphics/PorterDuff$Mode;)V
aload 0
instanceof android/support/v4/e/a/q
ifeq L0
aload 0
checkcast android/support/v4/e/a/q
aload 1
invokeinterface android/support/v4/e/a/q/setTintMode(Landroid/graphics/PorterDuff$Mode;)V 1
L0:
return
.limit locals 2
.limit stack 2
.end method
