.bytecode 50.0
.class final synchronized android/support/v4/c/aj
.super java/lang/Object
.implements java/util/concurrent/ThreadFactory

.field private final 'a' Ljava/util/concurrent/atomic/AtomicInteger;

.method <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
new java/util/concurrent/atomic/AtomicInteger
dup
iconst_1
invokespecial java/util/concurrent/atomic/AtomicInteger/<init>(I)V
putfield android/support/v4/c/aj/a Ljava/util/concurrent/atomic/AtomicInteger;
return
.limit locals 1
.limit stack 4
.end method

.method public final newThread(Ljava/lang/Runnable;)Ljava/lang/Thread;
new java/lang/Thread
dup
aload 1
new java/lang/StringBuilder
dup
ldc "ModernAsyncTask #"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
getfield android/support/v4/c/aj/a Ljava/util/concurrent/atomic/AtomicInteger;
invokevirtual java/util/concurrent/atomic/AtomicInteger/getAndIncrement()I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/Thread/<init>(Ljava/lang/Runnable;Ljava/lang/String;)V
areturn
.limit locals 2
.limit stack 6
.end method
