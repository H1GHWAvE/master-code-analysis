.bytecode 50.0
.class final synchronized android/support/v4/c/b
.super android/support/v4/c/ai
.implements java/lang/Runnable

.field final 'a' Ljava/util/concurrent/CountDownLatch;

.field 'b' Z

.field final synthetic 'c' Landroid/support/v4/c/a;

.method <init>(Landroid/support/v4/c/a;)V
aload 0
aload 1
putfield android/support/v4/c/b/c Landroid/support/v4/c/a;
aload 0
invokespecial android/support/v4/c/ai/<init>()V
aload 0
new java/util/concurrent/CountDownLatch
dup
iconst_1
invokespecial java/util/concurrent/CountDownLatch/<init>(I)V
putfield android/support/v4/c/b/a Ljava/util/concurrent/CountDownLatch;
return
.limit locals 2
.limit stack 4
.end method

.method private transient c()Ljava/lang/Object;
.catch android/support/v4/i/h from L0 to L1 using L2
L0:
aload 0
getfield android/support/v4/c/b/c Landroid/support/v4/c/a;
invokevirtual android/support/v4/c/a/d()Ljava/lang/Object;
astore 1
L1:
aload 1
areturn
L2:
astore 1
aload 0
getfield android/support/v4/c/ai/e Ljava/util/concurrent/FutureTask;
invokevirtual java/util/concurrent/FutureTask/isCancelled()Z
ifne L3
aload 1
athrow
L3:
aconst_null
areturn
.limit locals 2
.limit stack 1
.end method

.method private d()V
.catch java/lang/InterruptedException from L0 to L1 using L2
L0:
aload 0
getfield android/support/v4/c/b/a Ljava/util/concurrent/CountDownLatch;
invokevirtual java/util/concurrent/CountDownLatch/await()V
L1:
return
L2:
astore 1
return
.limit locals 2
.limit stack 1
.end method

.method protected final synthetic a()Ljava/lang/Object;
aload 0
invokespecial android/support/v4/c/b/c()Ljava/lang/Object;
areturn
.limit locals 1
.limit stack 1
.end method

.method protected final a(Ljava/lang/Object;)V
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch all from L5 to L6 using L2
L0:
aload 0
getfield android/support/v4/c/b/c Landroid/support/v4/c/a;
astore 2
aload 2
getfield android/support/v4/c/a/c Landroid/support/v4/c/b;
aload 0
if_acmpeq L3
aload 2
aload 0
aload 1
invokevirtual android/support/v4/c/a/a(Landroid/support/v4/c/b;Ljava/lang/Object;)V
L1:
aload 0
getfield android/support/v4/c/b/a Ljava/util/concurrent/CountDownLatch;
invokevirtual java/util/concurrent/CountDownLatch/countDown()V
return
L3:
aload 2
getfield android/support/v4/c/aa/u Z
ifeq L5
aload 2
aload 1
invokevirtual android/support/v4/c/a/a(Ljava/lang/Object;)V
L4:
goto L1
L2:
astore 1
aload 0
getfield android/support/v4/c/b/a Ljava/util/concurrent/CountDownLatch;
invokevirtual java/util/concurrent/CountDownLatch/countDown()V
aload 1
athrow
L5:
aload 2
iconst_0
putfield android/support/v4/c/aa/x Z
aload 2
invokestatic android/os/SystemClock/uptimeMillis()J
putfield android/support/v4/c/a/f J
aload 2
aconst_null
putfield android/support/v4/c/a/c Landroid/support/v4/c/b;
aload 2
aload 1
invokevirtual android/support/v4/c/a/b(Ljava/lang/Object;)V
L6:
goto L1
.limit locals 3
.limit stack 3
.end method

.method protected final b(Ljava/lang/Object;)V
.catch all from L0 to L1 using L2
L0:
aload 0
getfield android/support/v4/c/b/c Landroid/support/v4/c/a;
aload 0
aload 1
invokevirtual android/support/v4/c/a/a(Landroid/support/v4/c/b;Ljava/lang/Object;)V
L1:
aload 0
getfield android/support/v4/c/b/a Ljava/util/concurrent/CountDownLatch;
invokevirtual java/util/concurrent/CountDownLatch/countDown()V
return
L2:
astore 1
aload 0
getfield android/support/v4/c/b/a Ljava/util/concurrent/CountDownLatch;
invokevirtual java/util/concurrent/CountDownLatch/countDown()V
aload 1
athrow
.limit locals 2
.limit stack 3
.end method

.method public final run()V
aload 0
iconst_0
putfield android/support/v4/c/b/b Z
aload 0
getfield android/support/v4/c/b/c Landroid/support/v4/c/a;
invokevirtual android/support/v4/c/a/c()V
return
.limit locals 1
.limit stack 2
.end method
