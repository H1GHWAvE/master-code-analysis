.bytecode 50.0
.class public final synchronized android/support/v4/c/ar
.super java/lang/Object

.field public static final 'a' I = 0


.field public static final 'b' I = -1


.field public static final 'c' I = -2


.method private <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)I
.annotation invisibleparam 1 Landroid/support/a/y;
.end annotation
.annotation invisibleparam 2 Landroid/support/a/y;
.end annotation
aload 0
aload 1
invokestatic android/os/Process/myPid()I
invokestatic android/os/Process/myUid()I
aload 0
invokevirtual android/content/Context/getPackageName()Ljava/lang/String;
invokestatic android/support/v4/c/ar/a(Landroid/content/Context;Ljava/lang/String;IILjava/lang/String;)I
ireturn
.limit locals 2
.limit stack 5
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;IILjava/lang/String;)I
.annotation invisibleparam 1 Landroid/support/a/y;
.end annotation
.annotation invisibleparam 2 Landroid/support/a/y;
.end annotation
aload 0
aload 1
iload 2
iload 3
invokevirtual android/content/Context/checkPermission(Ljava/lang/String;II)I
iconst_m1
if_icmpne L0
L1:
iconst_m1
ireturn
L0:
aload 1
invokestatic android/support/v4/app/af/a(Ljava/lang/String;)Ljava/lang/String;
astore 5
aload 5
ifnonnull L2
iconst_0
ireturn
L2:
aload 4
astore 1
aload 4
ifnonnull L3
aload 0
invokevirtual android/content/Context/getPackageManager()Landroid/content/pm/PackageManager;
iload 3
invokevirtual android/content/pm/PackageManager/getPackagesForUid(I)[Ljava/lang/String;
astore 1
aload 1
ifnull L1
aload 1
arraylength
ifle L1
aload 1
iconst_0
aaload
astore 1
L3:
aload 0
aload 5
aload 1
invokestatic android/support/v4/app/af/a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I
ifeq L4
bipush -2
ireturn
L4:
iconst_0
ireturn
.limit locals 6
.limit stack 4
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I
.annotation invisibleparam 1 Landroid/support/a/y;
.end annotation
.annotation invisibleparam 2 Landroid/support/a/y;
.end annotation
invokestatic android/os/Binder/getCallingPid()I
invokestatic android/os/Process/myPid()I
if_icmpne L0
iconst_m1
ireturn
L0:
aload 0
aload 1
invokestatic android/os/Binder/getCallingPid()I
invokestatic android/os/Binder/getCallingUid()I
aload 2
invokestatic android/support/v4/c/ar/a(Landroid/content/Context;Ljava/lang/String;IILjava/lang/String;)I
ireturn
.limit locals 3
.limit stack 5
.end method

.method private static b(Landroid/content/Context;Ljava/lang/String;)I
.annotation invisibleparam 1 Landroid/support/a/y;
.end annotation
.annotation invisibleparam 2 Landroid/support/a/y;
.end annotation
invokestatic android/os/Binder/getCallingPid()I
invokestatic android/os/Process/myPid()I
if_icmpne L0
aload 0
invokevirtual android/content/Context/getPackageName()Ljava/lang/String;
astore 2
L1:
aload 0
aload 1
invokestatic android/os/Binder/getCallingPid()I
invokestatic android/os/Binder/getCallingUid()I
aload 2
invokestatic android/support/v4/c/ar/a(Landroid/content/Context;Ljava/lang/String;IILjava/lang/String;)I
ireturn
L0:
aconst_null
astore 2
goto L1
.limit locals 3
.limit stack 5
.end method
