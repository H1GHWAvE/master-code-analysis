.bytecode 50.0
.class public synchronized android/support/v4/widget/DrawerLayout
.super android/view/ViewGroup
.implements android/support/v4/widget/ak

.field public static final 'a' I = 0


.field public static final 'b' I = 1


.field public static final 'c' I = 2


.field public static final 'd' I = 0


.field public static final 'e' I = 1


.field public static final 'f' I = 2


.field static final 'n' Landroid/support/v4/widget/z;

.field private static final 'o' Ljava/lang/String; = "DrawerLayout"

.field private static final 'p' I = 64


.field private static final 'q' I = 10


.field private static final 'r' I = -1728053248


.field private static final 's' I = 160


.field private static final 't' I = 400


.field private static final 'u' Z = 0


.field private static final 'v' Z = 1


.field private static final 'w' F = 1.0F


.field private static final 'x' [I

.field private static final 'y' Z

.field private static final 'z' Z

.field private final 'A' Landroid/support/v4/widget/y;

.field private 'B' F

.field private 'C' I

.field private 'D' I

.field private 'E' F

.field private 'F' Landroid/graphics/Paint;

.field private final 'G' Landroid/support/v4/widget/ag;

.field private final 'H' Landroid/support/v4/widget/ag;

.field private 'I' Z

.field private 'J' Z

.field private 'K' I

.field private 'L' I

.field private 'M' Z

.field private 'N' F

.field private 'O' F

.field private 'P' Landroid/graphics/drawable/Drawable;

.field private 'Q' Landroid/graphics/drawable/Drawable;

.field private 'R' Landroid/graphics/drawable/Drawable;

.field private 'S' Ljava/lang/Object;

.field private 'T' Z

.field private 'U' Landroid/graphics/drawable/Drawable;

.field private 'V' Landroid/graphics/drawable/Drawable;

.field private 'W' Landroid/graphics/drawable/Drawable;

.field private 'aa' Landroid/graphics/drawable/Drawable;

.field private final 'ab' Ljava/util/ArrayList;

.field final 'g' Landroid/support/v4/widget/eg;

.field final 'h' Landroid/support/v4/widget/eg;

.field 'i' I

.field 'j' Z

.field 'k' Landroid/support/v4/widget/ac;

.field 'l' Ljava/lang/CharSequence;

.field 'm' Ljava/lang/CharSequence;

.method static <clinit>()V
iconst_1
istore 1
iconst_1
newarray int
dup
iconst_0
ldc_w 16842931
iastore
putstatic android/support/v4/widget/DrawerLayout/x [I
getstatic android/os/Build$VERSION/SDK_INT I
bipush 19
if_icmplt L0
iconst_1
istore 0
L1:
iload 0
putstatic android/support/v4/widget/DrawerLayout/y Z
getstatic android/os/Build$VERSION/SDK_INT I
bipush 21
if_icmplt L2
iload 1
istore 0
L3:
iload 0
putstatic android/support/v4/widget/DrawerLayout/z Z
getstatic android/os/Build$VERSION/SDK_INT I
bipush 21
if_icmplt L4
new android/support/v4/widget/aa
dup
invokespecial android/support/v4/widget/aa/<init>()V
putstatic android/support/v4/widget/DrawerLayout/n Landroid/support/v4/widget/z;
return
L0:
iconst_0
istore 0
goto L1
L2:
iconst_0
istore 0
goto L3
L4:
new android/support/v4/widget/ab
dup
invokespecial android/support/v4/widget/ab/<init>()V
putstatic android/support/v4/widget/DrawerLayout/n Landroid/support/v4/widget/z;
return
.limit locals 2
.limit stack 4
.end method

.method private <init>(Landroid/content/Context;)V
aload 0
aload 1
iconst_0
invokespecial android/support/v4/widget/DrawerLayout/<init>(Landroid/content/Context;B)V
return
.limit locals 2
.limit stack 3
.end method

.method private <init>(Landroid/content/Context;B)V
aload 0
aload 1
iconst_0
invokespecial android/support/v4/widget/DrawerLayout/<init>(Landroid/content/Context;C)V
return
.limit locals 3
.limit stack 3
.end method

.method private <init>(Landroid/content/Context;C)V
aload 0
aload 1
aconst_null
iconst_0
invokespecial android/view/ViewGroup/<init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
aload 0
new android/support/v4/widget/y
dup
aload 0
invokespecial android/support/v4/widget/y/<init>(Landroid/support/v4/widget/DrawerLayout;)V
putfield android/support/v4/widget/DrawerLayout/A Landroid/support/v4/widget/y;
aload 0
ldc_w -1728053248
putfield android/support/v4/widget/DrawerLayout/D I
aload 0
new android/graphics/Paint
dup
invokespecial android/graphics/Paint/<init>()V
putfield android/support/v4/widget/DrawerLayout/F Landroid/graphics/Paint;
aload 0
iconst_1
putfield android/support/v4/widget/DrawerLayout/J Z
aload 0
aconst_null
putfield android/support/v4/widget/DrawerLayout/U Landroid/graphics/drawable/Drawable;
aload 0
aconst_null
putfield android/support/v4/widget/DrawerLayout/V Landroid/graphics/drawable/Drawable;
aload 0
aconst_null
putfield android/support/v4/widget/DrawerLayout/W Landroid/graphics/drawable/Drawable;
aload 0
aconst_null
putfield android/support/v4/widget/DrawerLayout/aa Landroid/graphics/drawable/Drawable;
aload 0
ldc_w 262144
invokevirtual android/support/v4/widget/DrawerLayout/setDescendantFocusability(I)V
aload 0
invokevirtual android/support/v4/widget/DrawerLayout/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
getfield android/util/DisplayMetrics/density F
fstore 3
aload 0
ldc_w 64.0F
fload 3
fmul
ldc_w 0.5F
fadd
f2i
putfield android/support/v4/widget/DrawerLayout/C I
ldc_w 400.0F
fload 3
fmul
fstore 4
aload 0
new android/support/v4/widget/ag
dup
aload 0
iconst_3
invokespecial android/support/v4/widget/ag/<init>(Landroid/support/v4/widget/DrawerLayout;I)V
putfield android/support/v4/widget/DrawerLayout/G Landroid/support/v4/widget/ag;
aload 0
new android/support/v4/widget/ag
dup
aload 0
iconst_5
invokespecial android/support/v4/widget/ag/<init>(Landroid/support/v4/widget/DrawerLayout;I)V
putfield android/support/v4/widget/DrawerLayout/H Landroid/support/v4/widget/ag;
aload 0
aload 0
fconst_1
aload 0
getfield android/support/v4/widget/DrawerLayout/G Landroid/support/v4/widget/ag;
invokestatic android/support/v4/widget/eg/a(Landroid/view/ViewGroup;FLandroid/support/v4/widget/ej;)Landroid/support/v4/widget/eg;
putfield android/support/v4/widget/DrawerLayout/g Landroid/support/v4/widget/eg;
aload 0
getfield android/support/v4/widget/DrawerLayout/g Landroid/support/v4/widget/eg;
iconst_1
putfield android/support/v4/widget/eg/u I
aload 0
getfield android/support/v4/widget/DrawerLayout/g Landroid/support/v4/widget/eg;
fload 4
putfield android/support/v4/widget/eg/s F
aload 0
getfield android/support/v4/widget/DrawerLayout/G Landroid/support/v4/widget/ag;
aload 0
getfield android/support/v4/widget/DrawerLayout/g Landroid/support/v4/widget/eg;
putfield android/support/v4/widget/ag/b Landroid/support/v4/widget/eg;
aload 0
aload 0
fconst_1
aload 0
getfield android/support/v4/widget/DrawerLayout/H Landroid/support/v4/widget/ag;
invokestatic android/support/v4/widget/eg/a(Landroid/view/ViewGroup;FLandroid/support/v4/widget/ej;)Landroid/support/v4/widget/eg;
putfield android/support/v4/widget/DrawerLayout/h Landroid/support/v4/widget/eg;
aload 0
getfield android/support/v4/widget/DrawerLayout/h Landroid/support/v4/widget/eg;
iconst_2
putfield android/support/v4/widget/eg/u I
aload 0
getfield android/support/v4/widget/DrawerLayout/h Landroid/support/v4/widget/eg;
fload 4
putfield android/support/v4/widget/eg/s F
aload 0
getfield android/support/v4/widget/DrawerLayout/H Landroid/support/v4/widget/ag;
aload 0
getfield android/support/v4/widget/DrawerLayout/h Landroid/support/v4/widget/eg;
putfield android/support/v4/widget/ag/b Landroid/support/v4/widget/eg;
aload 0
iconst_1
invokevirtual android/support/v4/widget/DrawerLayout/setFocusableInTouchMode(Z)V
aload 0
iconst_1
invokestatic android/support/v4/view/cx/c(Landroid/view/View;I)V
aload 0
new android/support/v4/widget/x
dup
aload 0
invokespecial android/support/v4/widget/x/<init>(Landroid/support/v4/widget/DrawerLayout;)V
invokestatic android/support/v4/view/cx/a(Landroid/view/View;Landroid/support/v4/view/a;)V
aload 0
invokestatic android/support/v4/view/ec/a(Landroid/view/ViewGroup;)V
aload 0
invokestatic android/support/v4/view/cx/u(Landroid/view/View;)Z
ifeq L0
getstatic android/support/v4/widget/DrawerLayout/n Landroid/support/v4/widget/z;
aload 0
invokeinterface android/support/v4/widget/z/a(Landroid/view/View;)V 1
aload 0
getstatic android/support/v4/widget/DrawerLayout/n Landroid/support/v4/widget/z;
aload 1
invokeinterface android/support/v4/widget/z/a(Landroid/content/Context;)Landroid/graphics/drawable/Drawable; 1
putfield android/support/v4/widget/DrawerLayout/P Landroid/graphics/drawable/Drawable;
L0:
aload 0
fload 3
ldc_w 10.0F
fmul
putfield android/support/v4/widget/DrawerLayout/B F
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield android/support/v4/widget/DrawerLayout/ab Ljava/util/ArrayList;
return
.limit locals 5
.limit stack 5
.end method

.method static synthetic a(Landroid/support/v4/widget/DrawerLayout;)Landroid/view/View;
aload 0
invokespecial android/support/v4/widget/DrawerLayout/n()Landroid/view/View;
areturn
.limit locals 1
.limit stack 1
.end method

.method private a(F)V
aload 0
getfield android/support/v4/widget/DrawerLayout/k Landroid/support/v4/widget/ac;
ifnull L0
aload 0
getfield android/support/v4/widget/DrawerLayout/k Landroid/support/v4/widget/ac;
fload 1
invokeinterface android/support/v4/widget/ac/a(F)V 1
L0:
return
.limit locals 2
.limit stack 2
.end method

.method private a(II)V
.annotation invisibleparam 1 Landroid/support/a/m;
.end annotation
aload 0
invokevirtual android/support/v4/widget/DrawerLayout/getResources()Landroid/content/res/Resources;
iload 1
invokevirtual android/content/res/Resources/getDrawable(I)Landroid/graphics/drawable/Drawable;
astore 3
getstatic android/support/v4/widget/DrawerLayout/z Z
ifne L0
iload 2
ldc_w 8388611
iand
ldc_w 8388611
if_icmpne L1
aload 0
aload 3
putfield android/support/v4/widget/DrawerLayout/U Landroid/graphics/drawable/Drawable;
L2:
aload 0
invokespecial android/support/v4/widget/DrawerLayout/h()V
aload 0
invokevirtual android/support/v4/widget/DrawerLayout/invalidate()V
L0:
return
L1:
iload 2
ldc_w 8388613
iand
ldc_w 8388613
if_icmpne L3
aload 0
aload 3
putfield android/support/v4/widget/DrawerLayout/V Landroid/graphics/drawable/Drawable;
goto L2
L3:
iload 2
iconst_3
iand
iconst_3
if_icmpne L4
aload 0
aload 3
putfield android/support/v4/widget/DrawerLayout/W Landroid/graphics/drawable/Drawable;
goto L2
L4:
iload 2
iconst_5
iand
iconst_5
if_icmpne L0
aload 0
aload 3
putfield android/support/v4/widget/DrawerLayout/aa Landroid/graphics/drawable/Drawable;
goto L2
.limit locals 4
.limit stack 2
.end method

.method private a(ILandroid/view/View;)V
aload 2
invokestatic android/support/v4/widget/DrawerLayout/d(Landroid/view/View;)Z
ifne L0
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
ldc "View "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc " is not a drawer with appropriate layout_gravity"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
iload 1
aload 2
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v4/widget/ad
getfield android/support/v4/widget/ad/a I
invokespecial android/support/v4/widget/DrawerLayout/b(II)V
return
.limit locals 3
.limit stack 5
.end method

.method private a(ILjava/lang/CharSequence;)V
iload 1
aload 0
invokestatic android/support/v4/view/cx/f(Landroid/view/View;)I
invokestatic android/support/v4/view/v/a(II)I
istore 1
iload 1
iconst_3
if_icmpne L0
aload 0
aload 2
putfield android/support/v4/widget/DrawerLayout/l Ljava/lang/CharSequence;
L1:
return
L0:
iload 1
iconst_5
if_icmpne L1
aload 0
aload 2
putfield android/support/v4/widget/DrawerLayout/m Ljava/lang/CharSequence;
return
.limit locals 3
.limit stack 2
.end method

.method private a(Landroid/graphics/drawable/Drawable;I)V
getstatic android/support/v4/widget/DrawerLayout/z Z
ifeq L0
L1:
return
L0:
iload 2
ldc_w 8388611
iand
ldc_w 8388611
if_icmpne L2
aload 0
aload 1
putfield android/support/v4/widget/DrawerLayout/U Landroid/graphics/drawable/Drawable;
L3:
aload 0
invokespecial android/support/v4/widget/DrawerLayout/h()V
aload 0
invokevirtual android/support/v4/widget/DrawerLayout/invalidate()V
return
L2:
iload 2
ldc_w 8388613
iand
ldc_w 8388613
if_icmpne L4
aload 0
aload 1
putfield android/support/v4/widget/DrawerLayout/V Landroid/graphics/drawable/Drawable;
goto L3
L4:
iload 2
iconst_3
iand
iconst_3
if_icmpne L5
aload 0
aload 1
putfield android/support/v4/widget/DrawerLayout/W Landroid/graphics/drawable/Drawable;
goto L3
L5:
iload 2
iconst_5
iand
iconst_5
if_icmpne L1
aload 0
aload 1
putfield android/support/v4/widget/DrawerLayout/aa Landroid/graphics/drawable/Drawable;
goto L3
.limit locals 3
.limit stack 2
.end method

.method private a(Z)V
aload 0
invokevirtual android/support/v4/widget/DrawerLayout/getChildCount()I
istore 5
iconst_0
istore 3
iconst_0
istore 2
L0:
iload 3
iload 5
if_icmpge L1
aload 0
iload 3
invokevirtual android/support/v4/widget/DrawerLayout/getChildAt(I)Landroid/view/View;
astore 6
aload 6
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v4/widget/ad
astore 7
iload 2
istore 4
aload 6
invokestatic android/support/v4/widget/DrawerLayout/d(Landroid/view/View;)Z
ifeq L2
iload 1
ifeq L3
iload 2
istore 4
aload 7
getfield android/support/v4/widget/ad/c Z
ifeq L2
L3:
aload 6
invokevirtual android/view/View/getWidth()I
istore 4
aload 0
aload 6
iconst_3
invokevirtual android/support/v4/widget/DrawerLayout/a(Landroid/view/View;I)Z
ifeq L4
iload 2
aload 0
getfield android/support/v4/widget/DrawerLayout/g Landroid/support/v4/widget/eg;
aload 6
iload 4
ineg
aload 6
invokevirtual android/view/View/getTop()I
invokevirtual android/support/v4/widget/eg/a(Landroid/view/View;II)Z
ior
istore 2
L5:
aload 7
iconst_0
putfield android/support/v4/widget/ad/c Z
iload 2
istore 4
L2:
iload 3
iconst_1
iadd
istore 3
iload 4
istore 2
goto L0
L4:
iload 2
aload 0
getfield android/support/v4/widget/DrawerLayout/h Landroid/support/v4/widget/eg;
aload 6
aload 0
invokevirtual android/support/v4/widget/DrawerLayout/getWidth()I
aload 6
invokevirtual android/view/View/getTop()I
invokevirtual android/support/v4/widget/eg/a(Landroid/view/View;II)Z
ior
istore 2
goto L5
L1:
aload 0
getfield android/support/v4/widget/DrawerLayout/G Landroid/support/v4/widget/ag;
invokevirtual android/support/v4/widget/ag/a()V
aload 0
getfield android/support/v4/widget/DrawerLayout/H Landroid/support/v4/widget/ag;
invokevirtual android/support/v4/widget/ag/a()V
iload 2
ifeq L6
aload 0
invokevirtual android/support/v4/widget/DrawerLayout/invalidate()V
L6:
return
.limit locals 8
.limit stack 5
.end method

.method static b(Landroid/view/View;)F
aload 0
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v4/widget/ad
getfield android/support/v4/widget/ad/b F
freturn
.limit locals 1
.limit stack 1
.end method

.method private b(I)I
iload 1
aload 0
invokestatic android/support/v4/view/cx/f(Landroid/view/View;)I
invokestatic android/support/v4/view/v/a(II)I
istore 1
iload 1
iconst_3
if_icmpne L0
aload 0
getfield android/support/v4/widget/DrawerLayout/K I
ireturn
L0:
iload 1
iconst_5
if_icmpne L1
aload 0
getfield android/support/v4/widget/DrawerLayout/L I
ireturn
L1:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method private b(II)V
iload 2
aload 0
invokestatic android/support/v4/view/cx/f(Landroid/view/View;)I
invokestatic android/support/v4/view/v/a(II)I
istore 2
iload 2
iconst_3
if_icmpne L0
aload 0
iload 1
putfield android/support/v4/widget/DrawerLayout/K I
L1:
iload 1
ifeq L2
iload 2
iconst_3
if_icmpne L3
aload 0
getfield android/support/v4/widget/DrawerLayout/g Landroid/support/v4/widget/eg;
astore 3
L4:
aload 3
invokevirtual android/support/v4/widget/eg/a()V
L2:
iload 1
tableswitch 1
L5
L6
default : L7
L7:
return
L0:
iload 2
iconst_5
if_icmpne L1
aload 0
iload 1
putfield android/support/v4/widget/DrawerLayout/L I
goto L1
L3:
aload 0
getfield android/support/v4/widget/DrawerLayout/h Landroid/support/v4/widget/eg;
astore 3
goto L4
L6:
aload 0
iload 2
invokevirtual android/support/v4/widget/DrawerLayout/a(I)Landroid/view/View;
astore 3
aload 3
ifnull L7
aload 0
aload 3
invokespecial android/support/v4/widget/DrawerLayout/k(Landroid/view/View;)V
return
L5:
aload 0
iload 2
invokevirtual android/support/v4/widget/DrawerLayout/a(I)Landroid/view/View;
astore 3
aload 3
ifnull L7
aload 0
aload 3
invokevirtual android/support/v4/widget/DrawerLayout/e(Landroid/view/View;)V
return
.limit locals 4
.limit stack 2
.end method

.method private b(ILandroid/view/View;)V
aload 0
getfield android/support/v4/widget/DrawerLayout/g Landroid/support/v4/widget/eg;
getfield android/support/v4/widget/eg/m I
istore 3
aload 0
getfield android/support/v4/widget/DrawerLayout/h Landroid/support/v4/widget/eg;
getfield android/support/v4/widget/eg/m I
istore 4
iload 3
iconst_1
if_icmpeq L0
iload 4
iconst_1
if_icmpne L1
L0:
iconst_1
istore 3
L2:
aload 2
ifnull L3
iload 1
ifne L3
aload 2
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v4/widget/ad
astore 5
aload 5
getfield android/support/v4/widget/ad/b F
fconst_0
fcmpl
ifne L4
aload 2
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v4/widget/ad
astore 5
aload 5
getfield android/support/v4/widget/ad/d Z
ifeq L3
aload 5
iconst_0
putfield android/support/v4/widget/ad/d Z
aload 0
getfield android/support/v4/widget/DrawerLayout/k Landroid/support/v4/widget/ac;
ifnull L5
aload 0
getfield android/support/v4/widget/DrawerLayout/k Landroid/support/v4/widget/ac;
invokeinterface android/support/v4/widget/ac/b()V 0
L5:
aload 0
aload 2
iconst_0
invokevirtual android/support/v4/widget/DrawerLayout/a(Landroid/view/View;Z)V
aload 0
invokevirtual android/support/v4/widget/DrawerLayout/hasWindowFocus()Z
ifeq L3
aload 0
invokevirtual android/support/v4/widget/DrawerLayout/getRootView()Landroid/view/View;
astore 2
aload 2
ifnull L3
aload 2
bipush 32
invokevirtual android/view/View/sendAccessibilityEvent(I)V
L3:
iload 3
aload 0
getfield android/support/v4/widget/DrawerLayout/i I
if_icmpeq L6
aload 0
iload 3
putfield android/support/v4/widget/DrawerLayout/i I
L6:
return
L1:
iload 3
iconst_2
if_icmpeq L7
iload 4
iconst_2
if_icmpne L8
L7:
iconst_2
istore 3
goto L2
L8:
iconst_0
istore 3
goto L2
L4:
aload 5
getfield android/support/v4/widget/ad/b F
fconst_1
fcmpl
ifne L3
aload 2
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v4/widget/ad
astore 5
aload 5
getfield android/support/v4/widget/ad/d Z
ifne L3
aload 5
iconst_1
putfield android/support/v4/widget/ad/d Z
aload 0
getfield android/support/v4/widget/DrawerLayout/k Landroid/support/v4/widget/ac;
ifnull L9
aload 0
getfield android/support/v4/widget/DrawerLayout/k Landroid/support/v4/widget/ac;
invokeinterface android/support/v4/widget/ac/a()V 0
L9:
aload 0
aload 2
iconst_1
invokevirtual android/support/v4/widget/DrawerLayout/a(Landroid/view/View;Z)V
aload 0
invokevirtual android/support/v4/widget/DrawerLayout/hasWindowFocus()Z
ifeq L10
aload 0
bipush 32
invokevirtual android/support/v4/widget/DrawerLayout/sendAccessibilityEvent(I)V
L10:
aload 2
invokevirtual android/view/View/requestFocus()Z
pop
goto L3
.limit locals 6
.limit stack 3
.end method

.method private b(Landroid/view/View;F)V
aload 1
invokestatic android/support/v4/widget/DrawerLayout/b(Landroid/view/View;)F
fstore 3
aload 1
invokevirtual android/view/View/getWidth()I
istore 4
fload 3
iload 4
i2f
fmul
f2i
istore 5
iload 4
i2f
fload 2
fmul
f2i
iload 5
isub
istore 4
aload 0
aload 1
iconst_3
invokevirtual android/support/v4/widget/DrawerLayout/a(Landroid/view/View;I)Z
ifeq L0
L1:
aload 1
iload 4
invokevirtual android/view/View/offsetLeftAndRight(I)V
aload 0
aload 1
fload 2
invokevirtual android/support/v4/widget/DrawerLayout/a(Landroid/view/View;F)V
return
L0:
iload 4
ineg
istore 4
goto L1
.limit locals 6
.limit stack 3
.end method

.method private static b(Landroid/graphics/drawable/Drawable;I)Z
aload 0
ifnull L0
aload 0
invokestatic android/support/v4/e/a/a/b(Landroid/graphics/drawable/Drawable;)Z
ifne L1
L0:
iconst_0
ireturn
L1:
aload 0
iload 1
invokestatic android/support/v4/e/a/a/b(Landroid/graphics/drawable/Drawable;I)V
iconst_1
ireturn
.limit locals 2
.limit stack 2
.end method

.method private c(I)Ljava/lang/CharSequence;
.annotation invisible Landroid/support/a/z;
.end annotation
iload 1
aload 0
invokestatic android/support/v4/view/cx/f(Landroid/view/View;)I
invokestatic android/support/v4/view/v/a(II)I
istore 1
iload 1
iconst_3
if_icmpne L0
aload 0
getfield android/support/v4/widget/DrawerLayout/l Ljava/lang/CharSequence;
areturn
L0:
iload 1
iconst_5
if_icmpne L1
aload 0
getfield android/support/v4/widget/DrawerLayout/m Ljava/lang/CharSequence;
areturn
L1:
aconst_null
areturn
.limit locals 2
.limit stack 2
.end method

.method private static d(I)Ljava/lang/String;
iload 0
iconst_3
iand
iconst_3
if_icmpne L0
ldc "LEFT"
areturn
L0:
iload 0
iconst_5
iand
iconst_5
if_icmpne L1
ldc "RIGHT"
areturn
L1:
iload 0
invokestatic java/lang/Integer/toHexString(I)Ljava/lang/String;
areturn
.limit locals 1
.limit stack 2
.end method

.method static d(Landroid/view/View;)Z
aload 0
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v4/widget/ad
getfield android/support/v4/widget/ad/a I
aload 0
invokestatic android/support/v4/view/cx/f(Landroid/view/View;)I
invokestatic android/support/v4/view/v/a(II)I
bipush 7
iand
ifeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method static synthetic e()[I
getstatic android/support/v4/widget/DrawerLayout/x [I
areturn
.limit locals 0
.limit stack 1
.end method

.method static synthetic f()Z
getstatic android/support/v4/widget/DrawerLayout/y Z
ireturn
.limit locals 0
.limit stack 1
.end method

.method static synthetic f(Landroid/view/View;)Z
aload 0
invokestatic android/support/v4/view/cx/c(Landroid/view/View;)I
iconst_4
if_icmpeq L0
aload 0
invokestatic android/support/v4/view/cx/c(Landroid/view/View;)I
iconst_2
if_icmpeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method private g()Landroid/view/View;
aload 0
invokevirtual android/support/v4/widget/DrawerLayout/getChildCount()I
istore 2
iconst_0
istore 1
L0:
iload 1
iload 2
if_icmpge L1
aload 0
iload 1
invokevirtual android/support/v4/widget/DrawerLayout/getChildAt(I)Landroid/view/View;
astore 3
aload 3
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v4/widget/ad
getfield android/support/v4/widget/ad/d Z
ifeq L2
aload 3
areturn
L2:
iload 1
iconst_1
iadd
istore 1
goto L0
L1:
aconst_null
areturn
.limit locals 4
.limit stack 2
.end method

.method private g(Landroid/view/View;)V
aload 1
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v4/widget/ad
astore 2
aload 2
getfield android/support/v4/widget/ad/d Z
ifeq L0
aload 2
iconst_0
putfield android/support/v4/widget/ad/d Z
aload 0
getfield android/support/v4/widget/DrawerLayout/k Landroid/support/v4/widget/ac;
ifnull L1
aload 0
getfield android/support/v4/widget/DrawerLayout/k Landroid/support/v4/widget/ac;
invokeinterface android/support/v4/widget/ac/b()V 0
L1:
aload 0
aload 1
iconst_0
invokevirtual android/support/v4/widget/DrawerLayout/a(Landroid/view/View;Z)V
aload 0
invokevirtual android/support/v4/widget/DrawerLayout/hasWindowFocus()Z
ifeq L0
aload 0
invokevirtual android/support/v4/widget/DrawerLayout/getRootView()Landroid/view/View;
astore 1
aload 1
ifnull L0
aload 1
bipush 32
invokevirtual android/view/View/sendAccessibilityEvent(I)V
L0:
return
.limit locals 3
.limit stack 3
.end method

.method private h()V
getstatic android/support/v4/widget/DrawerLayout/z Z
ifeq L0
return
L0:
aload 0
invokestatic android/support/v4/view/cx/f(Landroid/view/View;)I
istore 1
iload 1
ifne L1
aload 0
getfield android/support/v4/widget/DrawerLayout/U Landroid/graphics/drawable/Drawable;
ifnull L2
aload 0
getfield android/support/v4/widget/DrawerLayout/U Landroid/graphics/drawable/Drawable;
iload 1
invokestatic android/support/v4/widget/DrawerLayout/b(Landroid/graphics/drawable/Drawable;I)Z
pop
aload 0
getfield android/support/v4/widget/DrawerLayout/U Landroid/graphics/drawable/Drawable;
astore 2
L3:
aload 0
aload 2
putfield android/support/v4/widget/DrawerLayout/Q Landroid/graphics/drawable/Drawable;
aload 0
invokestatic android/support/v4/view/cx/f(Landroid/view/View;)I
istore 1
iload 1
ifne L4
aload 0
getfield android/support/v4/widget/DrawerLayout/V Landroid/graphics/drawable/Drawable;
ifnull L5
aload 0
getfield android/support/v4/widget/DrawerLayout/V Landroid/graphics/drawable/Drawable;
iload 1
invokestatic android/support/v4/widget/DrawerLayout/b(Landroid/graphics/drawable/Drawable;I)Z
pop
aload 0
getfield android/support/v4/widget/DrawerLayout/V Landroid/graphics/drawable/Drawable;
astore 2
L6:
aload 0
aload 2
putfield android/support/v4/widget/DrawerLayout/R Landroid/graphics/drawable/Drawable;
return
L1:
aload 0
getfield android/support/v4/widget/DrawerLayout/V Landroid/graphics/drawable/Drawable;
ifnull L2
aload 0
getfield android/support/v4/widget/DrawerLayout/V Landroid/graphics/drawable/Drawable;
iload 1
invokestatic android/support/v4/widget/DrawerLayout/b(Landroid/graphics/drawable/Drawable;I)Z
pop
aload 0
getfield android/support/v4/widget/DrawerLayout/V Landroid/graphics/drawable/Drawable;
astore 2
goto L3
L2:
aload 0
getfield android/support/v4/widget/DrawerLayout/W Landroid/graphics/drawable/Drawable;
astore 2
goto L3
L4:
aload 0
getfield android/support/v4/widget/DrawerLayout/U Landroid/graphics/drawable/Drawable;
ifnull L5
aload 0
getfield android/support/v4/widget/DrawerLayout/U Landroid/graphics/drawable/Drawable;
iload 1
invokestatic android/support/v4/widget/DrawerLayout/b(Landroid/graphics/drawable/Drawable;I)Z
pop
aload 0
getfield android/support/v4/widget/DrawerLayout/U Landroid/graphics/drawable/Drawable;
astore 2
goto L6
L5:
aload 0
getfield android/support/v4/widget/DrawerLayout/aa Landroid/graphics/drawable/Drawable;
astore 2
goto L6
.limit locals 3
.limit stack 2
.end method

.method private h(Landroid/view/View;)V
aload 1
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v4/widget/ad
astore 2
aload 2
getfield android/support/v4/widget/ad/d Z
ifne L0
aload 2
iconst_1
putfield android/support/v4/widget/ad/d Z
aload 0
getfield android/support/v4/widget/DrawerLayout/k Landroid/support/v4/widget/ac;
ifnull L1
aload 0
getfield android/support/v4/widget/DrawerLayout/k Landroid/support/v4/widget/ac;
invokeinterface android/support/v4/widget/ac/a()V 0
L1:
aload 0
aload 1
iconst_1
invokevirtual android/support/v4/widget/DrawerLayout/a(Landroid/view/View;Z)V
aload 0
invokevirtual android/support/v4/widget/DrawerLayout/hasWindowFocus()Z
ifeq L2
aload 0
bipush 32
invokevirtual android/support/v4/widget/DrawerLayout/sendAccessibilityEvent(I)V
L2:
aload 1
invokevirtual android/view/View/requestFocus()Z
pop
L0:
return
.limit locals 3
.limit stack 3
.end method

.method private i()Landroid/graphics/drawable/Drawable;
aload 0
invokestatic android/support/v4/view/cx/f(Landroid/view/View;)I
istore 1
iload 1
ifne L0
aload 0
getfield android/support/v4/widget/DrawerLayout/U Landroid/graphics/drawable/Drawable;
ifnull L1
aload 0
getfield android/support/v4/widget/DrawerLayout/U Landroid/graphics/drawable/Drawable;
iload 1
invokestatic android/support/v4/widget/DrawerLayout/b(Landroid/graphics/drawable/Drawable;I)Z
pop
aload 0
getfield android/support/v4/widget/DrawerLayout/U Landroid/graphics/drawable/Drawable;
areturn
L0:
aload 0
getfield android/support/v4/widget/DrawerLayout/V Landroid/graphics/drawable/Drawable;
ifnull L1
aload 0
getfield android/support/v4/widget/DrawerLayout/V Landroid/graphics/drawable/Drawable;
iload 1
invokestatic android/support/v4/widget/DrawerLayout/b(Landroid/graphics/drawable/Drawable;I)Z
pop
aload 0
getfield android/support/v4/widget/DrawerLayout/V Landroid/graphics/drawable/Drawable;
areturn
L1:
aload 0
getfield android/support/v4/widget/DrawerLayout/W Landroid/graphics/drawable/Drawable;
areturn
.limit locals 2
.limit stack 2
.end method

.method private static i(Landroid/view/View;)Z
iconst_0
istore 2
aload 0
invokevirtual android/view/View/getBackground()Landroid/graphics/drawable/Drawable;
astore 0
iload 2
istore 1
aload 0
ifnull L0
iload 2
istore 1
aload 0
invokevirtual android/graphics/drawable/Drawable/getOpacity()I
iconst_m1
if_icmpne L0
iconst_1
istore 1
L0:
iload 1
ireturn
.limit locals 3
.limit stack 2
.end method

.method private j()Landroid/graphics/drawable/Drawable;
aload 0
invokestatic android/support/v4/view/cx/f(Landroid/view/View;)I
istore 1
iload 1
ifne L0
aload 0
getfield android/support/v4/widget/DrawerLayout/V Landroid/graphics/drawable/Drawable;
ifnull L1
aload 0
getfield android/support/v4/widget/DrawerLayout/V Landroid/graphics/drawable/Drawable;
iload 1
invokestatic android/support/v4/widget/DrawerLayout/b(Landroid/graphics/drawable/Drawable;I)Z
pop
aload 0
getfield android/support/v4/widget/DrawerLayout/V Landroid/graphics/drawable/Drawable;
areturn
L0:
aload 0
getfield android/support/v4/widget/DrawerLayout/U Landroid/graphics/drawable/Drawable;
ifnull L1
aload 0
getfield android/support/v4/widget/DrawerLayout/U Landroid/graphics/drawable/Drawable;
iload 1
invokestatic android/support/v4/widget/DrawerLayout/b(Landroid/graphics/drawable/Drawable;I)Z
pop
aload 0
getfield android/support/v4/widget/DrawerLayout/U Landroid/graphics/drawable/Drawable;
areturn
L1:
aload 0
getfield android/support/v4/widget/DrawerLayout/aa Landroid/graphics/drawable/Drawable;
areturn
.limit locals 2
.limit stack 2
.end method

.method private static j(Landroid/view/View;)Z
aload 0
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v4/widget/ad
getfield android/support/v4/widget/ad/a I
ifne L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method private k()V
aload 0
iconst_0
invokespecial android/support/v4/widget/DrawerLayout/a(Z)V
return
.limit locals 1
.limit stack 2
.end method

.method private k(Landroid/view/View;)V
aload 1
invokestatic android/support/v4/widget/DrawerLayout/d(Landroid/view/View;)Z
ifne L0
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
ldc "View "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc " is not a sliding drawer"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
getfield android/support/v4/widget/DrawerLayout/J Z
ifeq L1
aload 1
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v4/widget/ad
astore 2
aload 2
fconst_1
putfield android/support/v4/widget/ad/b F
aload 2
iconst_1
putfield android/support/v4/widget/ad/d Z
aload 0
aload 1
iconst_1
invokevirtual android/support/v4/widget/DrawerLayout/a(Landroid/view/View;Z)V
L2:
aload 0
invokevirtual android/support/v4/widget/DrawerLayout/invalidate()V
return
L1:
aload 0
aload 1
iconst_3
invokevirtual android/support/v4/widget/DrawerLayout/a(Landroid/view/View;I)Z
ifeq L3
aload 0
getfield android/support/v4/widget/DrawerLayout/g Landroid/support/v4/widget/eg;
aload 1
iconst_0
aload 1
invokevirtual android/view/View/getTop()I
invokevirtual android/support/v4/widget/eg/a(Landroid/view/View;II)Z
pop
goto L2
L3:
aload 0
getfield android/support/v4/widget/DrawerLayout/h Landroid/support/v4/widget/eg;
aload 1
aload 0
invokevirtual android/support/v4/widget/DrawerLayout/getWidth()I
aload 1
invokevirtual android/view/View/getWidth()I
isub
aload 1
invokevirtual android/view/View/getTop()I
invokevirtual android/support/v4/widget/eg/a(Landroid/view/View;II)Z
pop
goto L2
.limit locals 3
.limit stack 5
.end method

.method private l()Z
aload 0
invokevirtual android/support/v4/widget/DrawerLayout/getChildCount()I
istore 2
iconst_0
istore 1
L0:
iload 1
iload 2
if_icmpge L1
aload 0
iload 1
invokevirtual android/support/v4/widget/DrawerLayout/getChildAt(I)Landroid/view/View;
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v4/widget/ad
getfield android/support/v4/widget/ad/c Z
ifeq L2
iconst_1
ireturn
L2:
iload 1
iconst_1
iadd
istore 1
goto L0
L1:
iconst_0
ireturn
.limit locals 3
.limit stack 2
.end method

.method private static l(Landroid/view/View;)Z
aload 0
invokestatic android/support/v4/widget/DrawerLayout/d(Landroid/view/View;)Z
ifne L0
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
ldc "View "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc " is not a drawer"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v4/widget/ad
getfield android/support/v4/widget/ad/d Z
ireturn
.limit locals 1
.limit stack 5
.end method

.method private m()Z
aload 0
invokespecial android/support/v4/widget/DrawerLayout/n()Landroid/view/View;
ifnull L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method private static m(Landroid/view/View;)Z
aload 0
invokestatic android/support/v4/widget/DrawerLayout/d(Landroid/view/View;)Z
ifne L0
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
ldc "View "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc " is not a drawer"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v4/widget/ad
getfield android/support/v4/widget/ad/b F
fconst_0
fcmpl
ifle L1
iconst_1
ireturn
L1:
iconst_0
ireturn
.limit locals 1
.limit stack 5
.end method

.method private n()Landroid/view/View;
aload 0
invokevirtual android/support/v4/widget/DrawerLayout/getChildCount()I
istore 2
iconst_0
istore 1
L0:
iload 1
iload 2
if_icmpge L1
aload 0
iload 1
invokevirtual android/support/v4/widget/DrawerLayout/getChildAt(I)Landroid/view/View;
astore 3
aload 3
invokestatic android/support/v4/widget/DrawerLayout/d(Landroid/view/View;)Z
ifeq L2
aload 3
invokestatic android/support/v4/widget/DrawerLayout/m(Landroid/view/View;)Z
ifeq L2
aload 3
areturn
L2:
iload 1
iconst_1
iadd
istore 1
goto L0
L1:
aconst_null
areturn
.limit locals 4
.limit stack 2
.end method

.method private static n(Landroid/view/View;)Z
aload 0
invokestatic android/support/v4/view/cx/c(Landroid/view/View;)I
iconst_4
if_icmpeq L0
aload 0
invokestatic android/support/v4/view/cx/c(Landroid/view/View;)I
iconst_2
if_icmpeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method private o()V
iconst_0
istore 1
aload 0
getfield android/support/v4/widget/DrawerLayout/j Z
ifne L0
invokestatic android/os/SystemClock/uptimeMillis()J
lstore 3
lload 3
lload 3
iconst_3
fconst_0
fconst_0
iconst_0
invokestatic android/view/MotionEvent/obtain(JJIFFI)Landroid/view/MotionEvent;
astore 5
aload 0
invokevirtual android/support/v4/widget/DrawerLayout/getChildCount()I
istore 2
L1:
iload 1
iload 2
if_icmpge L2
aload 0
iload 1
invokevirtual android/support/v4/widget/DrawerLayout/getChildAt(I)Landroid/view/View;
aload 5
invokevirtual android/view/View/dispatchTouchEvent(Landroid/view/MotionEvent;)Z
pop
iload 1
iconst_1
iadd
istore 1
goto L1
L2:
aload 5
invokevirtual android/view/MotionEvent/recycle()V
aload 0
iconst_1
putfield android/support/v4/widget/DrawerLayout/j Z
L0:
return
.limit locals 6
.limit stack 8
.end method

.method public final a(Landroid/view/View;)I
aload 0
aload 1
invokevirtual android/support/v4/widget/DrawerLayout/c(Landroid/view/View;)I
istore 2
iload 2
iconst_3
if_icmpne L0
aload 0
getfield android/support/v4/widget/DrawerLayout/K I
ireturn
L0:
iload 2
iconst_5
if_icmpne L1
aload 0
getfield android/support/v4/widget/DrawerLayout/L I
ireturn
L1:
iconst_0
ireturn
.limit locals 3
.limit stack 2
.end method

.method final a(I)Landroid/view/View;
iload 1
aload 0
invokestatic android/support/v4/view/cx/f(Landroid/view/View;)I
invokestatic android/support/v4/view/v/a(II)I
istore 2
aload 0
invokevirtual android/support/v4/widget/DrawerLayout/getChildCount()I
istore 3
iconst_0
istore 1
L0:
iload 1
iload 3
if_icmpge L1
aload 0
iload 1
invokevirtual android/support/v4/widget/DrawerLayout/getChildAt(I)Landroid/view/View;
astore 4
aload 0
aload 4
invokevirtual android/support/v4/widget/DrawerLayout/c(Landroid/view/View;)I
bipush 7
iand
iload 2
bipush 7
iand
if_icmpne L2
aload 4
areturn
L2:
iload 1
iconst_1
iadd
istore 1
goto L0
L1:
aconst_null
areturn
.limit locals 5
.limit stack 3
.end method

.method public final a()V
aload 0
ldc_w 8388611
invokevirtual android/support/v4/widget/DrawerLayout/a(I)Landroid/view/View;
astore 1
aload 1
ifnonnull L0
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
ldc "No drawer view found with gravity "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
ldc_w 8388611
invokestatic android/support/v4/widget/DrawerLayout/d(I)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
aload 1
invokespecial android/support/v4/widget/DrawerLayout/k(Landroid/view/View;)V
return
.limit locals 2
.limit stack 5
.end method

.method final a(Landroid/view/View;F)V
aload 1
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v4/widget/ad
astore 1
fload 2
aload 1
getfield android/support/v4/widget/ad/b F
fcmpl
ifne L0
L1:
return
L0:
aload 1
fload 2
putfield android/support/v4/widget/ad/b F
aload 0
getfield android/support/v4/widget/DrawerLayout/k Landroid/support/v4/widget/ac;
ifnull L1
aload 0
getfield android/support/v4/widget/DrawerLayout/k Landroid/support/v4/widget/ac;
fload 2
invokeinterface android/support/v4/widget/ac/a(F)V 1
return
.limit locals 3
.limit stack 2
.end method

.method final a(Landroid/view/View;Z)V
aload 0
invokevirtual android/support/v4/widget/DrawerLayout/getChildCount()I
istore 4
iconst_0
istore 3
L0:
iload 3
iload 4
if_icmpge L1
aload 0
iload 3
invokevirtual android/support/v4/widget/DrawerLayout/getChildAt(I)Landroid/view/View;
astore 5
iload 2
ifne L2
aload 5
invokestatic android/support/v4/widget/DrawerLayout/d(Landroid/view/View;)Z
ifeq L3
L2:
iload 2
ifeq L4
aload 5
aload 1
if_acmpne L4
L3:
aload 5
iconst_1
invokestatic android/support/v4/view/cx/c(Landroid/view/View;I)V
L5:
iload 3
iconst_1
iadd
istore 3
goto L0
L4:
aload 5
iconst_4
invokestatic android/support/v4/view/cx/c(Landroid/view/View;I)V
goto L5
L1:
return
.limit locals 6
.limit stack 2
.end method

.method public final a(Ljava/lang/Object;Z)V
aload 0
aload 1
putfield android/support/v4/widget/DrawerLayout/S Ljava/lang/Object;
aload 0
iload 2
putfield android/support/v4/widget/DrawerLayout/T Z
iload 2
ifne L0
aload 0
invokevirtual android/support/v4/widget/DrawerLayout/getBackground()Landroid/graphics/drawable/Drawable;
ifnonnull L0
iconst_1
istore 2
L1:
aload 0
iload 2
invokevirtual android/support/v4/widget/DrawerLayout/setWillNotDraw(Z)V
aload 0
invokevirtual android/support/v4/widget/DrawerLayout/requestLayout()V
return
L0:
iconst_0
istore 2
goto L1
.limit locals 3
.limit stack 2
.end method

.method final a(Landroid/view/View;I)Z
aload 0
aload 1
invokevirtual android/support/v4/widget/DrawerLayout/c(Landroid/view/View;)I
iload 2
iand
iload 2
if_icmpne L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 3
.limit stack 2
.end method

.method public addFocusables(Ljava/util/ArrayList;II)V
iconst_0
istore 6
aload 0
invokevirtual android/support/v4/widget/DrawerLayout/getDescendantFocusability()I
ldc_w 393216
if_icmpne L0
return
L0:
aload 0
invokevirtual android/support/v4/widget/DrawerLayout/getChildCount()I
istore 7
iconst_0
istore 4
iconst_0
istore 5
L1:
iload 4
iload 7
if_icmpge L2
aload 0
iload 4
invokevirtual android/support/v4/widget/DrawerLayout/getChildAt(I)Landroid/view/View;
astore 8
aload 8
invokestatic android/support/v4/widget/DrawerLayout/d(Landroid/view/View;)Z
ifeq L3
aload 8
invokestatic android/support/v4/widget/DrawerLayout/l(Landroid/view/View;)Z
ifeq L4
iconst_1
istore 5
aload 8
aload 1
iload 2
iload 3
invokevirtual android/view/View/addFocusables(Ljava/util/ArrayList;II)V
L4:
iload 4
iconst_1
iadd
istore 4
goto L1
L3:
aload 0
getfield android/support/v4/widget/DrawerLayout/ab Ljava/util/ArrayList;
aload 8
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
goto L4
L2:
iload 5
ifne L5
aload 0
getfield android/support/v4/widget/DrawerLayout/ab Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
istore 5
iload 6
istore 4
L6:
iload 4
iload 5
if_icmpge L5
aload 0
getfield android/support/v4/widget/DrawerLayout/ab Ljava/util/ArrayList;
iload 4
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/view/View
astore 8
aload 8
invokevirtual android/view/View/getVisibility()I
ifne L7
aload 8
aload 1
iload 2
iload 3
invokevirtual android/view/View/addFocusables(Ljava/util/ArrayList;II)V
L7:
iload 4
iconst_1
iadd
istore 4
goto L6
L5:
aload 0
getfield android/support/v4/widget/DrawerLayout/ab Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/clear()V
return
.limit locals 9
.limit stack 4
.end method

.method public addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
aload 0
aload 1
iload 2
aload 3
invokespecial android/view/ViewGroup/addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
aload 0
invokespecial android/support/v4/widget/DrawerLayout/g()Landroid/view/View;
ifnonnull L0
aload 1
invokestatic android/support/v4/widget/DrawerLayout/d(Landroid/view/View;)Z
ifeq L1
L0:
aload 1
iconst_4
invokestatic android/support/v4/view/cx/c(Landroid/view/View;I)V
L2:
getstatic android/support/v4/widget/DrawerLayout/y Z
ifne L3
aload 1
aload 0
getfield android/support/v4/widget/DrawerLayout/A Landroid/support/v4/widget/y;
invokestatic android/support/v4/view/cx/a(Landroid/view/View;Landroid/support/v4/view/a;)V
L3:
return
L1:
aload 1
iconst_1
invokestatic android/support/v4/view/cx/c(Landroid/view/View;I)V
goto L2
.limit locals 4
.limit stack 4
.end method

.method public final b()V
aload 0
ldc_w 8388611
invokevirtual android/support/v4/widget/DrawerLayout/a(I)Landroid/view/View;
astore 1
aload 1
ifnonnull L0
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
ldc "No drawer view found with gravity "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
ldc_w 8388611
invokestatic android/support/v4/widget/DrawerLayout/d(I)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
aload 1
invokevirtual android/support/v4/widget/DrawerLayout/e(Landroid/view/View;)V
return
.limit locals 2
.limit stack 5
.end method

.method final c(Landroid/view/View;)I
aload 1
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v4/widget/ad
getfield android/support/v4/widget/ad/a I
aload 0
invokestatic android/support/v4/view/cx/f(Landroid/view/View;)I
invokestatic android/support/v4/view/v/a(II)I
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final c()Z
aload 0
ldc_w 8388611
invokevirtual android/support/v4/widget/DrawerLayout/a(I)Landroid/view/View;
astore 1
aload 1
ifnull L0
aload 1
invokestatic android/support/v4/widget/DrawerLayout/l(Landroid/view/View;)Z
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
aload 1
instanceof android/support/v4/widget/ad
ifeq L0
aload 0
aload 1
invokespecial android/view/ViewGroup/checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
ifeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method public computeScroll()V
aload 0
invokevirtual android/support/v4/widget/DrawerLayout/getChildCount()I
istore 3
fconst_0
fstore 1
iconst_0
istore 2
L0:
iload 2
iload 3
if_icmpge L1
fload 1
aload 0
iload 2
invokevirtual android/support/v4/widget/DrawerLayout/getChildAt(I)Landroid/view/View;
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v4/widget/ad
getfield android/support/v4/widget/ad/b F
invokestatic java/lang/Math/max(FF)F
fstore 1
iload 2
iconst_1
iadd
istore 2
goto L0
L1:
aload 0
fload 1
putfield android/support/v4/widget/DrawerLayout/E F
aload 0
getfield android/support/v4/widget/DrawerLayout/g Landroid/support/v4/widget/eg;
invokevirtual android/support/v4/widget/eg/c()Z
aload 0
getfield android/support/v4/widget/DrawerLayout/h Landroid/support/v4/widget/eg;
invokevirtual android/support/v4/widget/eg/c()Z
ior
ifeq L2
aload 0
invokestatic android/support/v4/view/cx/b(Landroid/view/View;)V
L2:
return
.limit locals 4
.limit stack 3
.end method

.method public final d()Z
aload 0
ldc_w 8388611
invokevirtual android/support/v4/widget/DrawerLayout/a(I)Landroid/view/View;
astore 1
aload 1
ifnull L0
aload 1
invokestatic android/support/v4/widget/DrawerLayout/m(Landroid/view/View;)Z
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method protected drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z
aload 0
invokevirtual android/support/v4/widget/DrawerLayout/getHeight()I
istore 12
aload 2
invokestatic android/support/v4/widget/DrawerLayout/j(Landroid/view/View;)Z
istore 15
iconst_0
istore 7
iconst_0
istore 10
aload 0
invokevirtual android/support/v4/widget/DrawerLayout/getWidth()I
istore 6
aload 1
invokevirtual android/graphics/Canvas/save()I
istore 13
iload 6
istore 8
iload 15
ifeq L0
aload 0
invokevirtual android/support/v4/widget/DrawerLayout/getChildCount()I
istore 14
iconst_0
istore 9
iload 10
istore 7
L1:
iload 9
iload 14
if_icmpge L2
aload 0
iload 9
invokevirtual android/support/v4/widget/DrawerLayout/getChildAt(I)Landroid/view/View;
astore 17
aload 17
aload 2
if_acmpeq L3
aload 17
invokevirtual android/view/View/getVisibility()I
ifne L3
aload 17
invokevirtual android/view/View/getBackground()Landroid/graphics/drawable/Drawable;
astore 18
aload 18
ifnull L4
aload 18
invokevirtual android/graphics/drawable/Drawable/getOpacity()I
iconst_m1
if_icmpne L5
iconst_1
istore 8
L6:
iload 8
ifeq L3
aload 17
invokestatic android/support/v4/widget/DrawerLayout/d(Landroid/view/View;)Z
ifeq L3
aload 17
invokevirtual android/view/View/getHeight()I
iload 12
if_icmplt L3
aload 0
aload 17
iconst_3
invokevirtual android/support/v4/widget/DrawerLayout/a(Landroid/view/View;I)Z
ifeq L7
aload 17
invokevirtual android/view/View/getRight()I
istore 8
iload 8
iload 7
if_icmple L8
iload 8
istore 7
L9:
iload 7
istore 10
iload 6
istore 8
L10:
iload 9
iconst_1
iadd
istore 9
iload 8
istore 6
iload 10
istore 7
goto L1
L5:
iconst_0
istore 8
goto L6
L4:
iconst_0
istore 8
goto L6
L7:
aload 17
invokevirtual android/view/View/getLeft()I
istore 11
iload 11
istore 8
iload 7
istore 10
iload 11
iload 6
if_icmplt L10
L3:
iload 6
istore 8
iload 7
istore 10
goto L10
L2:
aload 1
iload 7
iconst_0
iload 6
aload 0
invokevirtual android/support/v4/widget/DrawerLayout/getHeight()I
invokevirtual android/graphics/Canvas/clipRect(IIII)Z
pop
iload 6
istore 8
L0:
aload 0
aload 1
aload 2
lload 3
invokespecial android/view/ViewGroup/drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z
istore 16
aload 1
iload 13
invokevirtual android/graphics/Canvas/restoreToCount(I)V
aload 0
getfield android/support/v4/widget/DrawerLayout/E F
fconst_0
fcmpl
ifle L11
iload 15
ifeq L11
aload 0
getfield android/support/v4/widget/DrawerLayout/D I
ldc_w -16777216
iand
bipush 24
iushr
i2f
aload 0
getfield android/support/v4/widget/DrawerLayout/E F
fmul
f2i
istore 6
aload 0
getfield android/support/v4/widget/DrawerLayout/D I
istore 9
aload 0
getfield android/support/v4/widget/DrawerLayout/F Landroid/graphics/Paint;
iload 6
bipush 24
ishl
iload 9
ldc_w 16777215
iand
ior
invokevirtual android/graphics/Paint/setColor(I)V
aload 1
iload 7
i2f
fconst_0
iload 8
i2f
aload 0
invokevirtual android/support/v4/widget/DrawerLayout/getHeight()I
i2f
aload 0
getfield android/support/v4/widget/DrawerLayout/F Landroid/graphics/Paint;
invokevirtual android/graphics/Canvas/drawRect(FFFFLandroid/graphics/Paint;)V
L12:
iload 16
ireturn
L11:
aload 0
getfield android/support/v4/widget/DrawerLayout/Q Landroid/graphics/drawable/Drawable;
ifnull L13
aload 0
aload 2
iconst_3
invokevirtual android/support/v4/widget/DrawerLayout/a(Landroid/view/View;I)Z
ifeq L13
aload 0
getfield android/support/v4/widget/DrawerLayout/Q Landroid/graphics/drawable/Drawable;
invokevirtual android/graphics/drawable/Drawable/getIntrinsicWidth()I
istore 6
aload 2
invokevirtual android/view/View/getRight()I
istore 7
aload 0
getfield android/support/v4/widget/DrawerLayout/g Landroid/support/v4/widget/eg;
getfield android/support/v4/widget/eg/t I
istore 8
fconst_0
iload 7
i2f
iload 8
i2f
fdiv
fconst_1
invokestatic java/lang/Math/min(FF)F
invokestatic java/lang/Math/max(FF)F
fstore 5
aload 0
getfield android/support/v4/widget/DrawerLayout/Q Landroid/graphics/drawable/Drawable;
iload 7
aload 2
invokevirtual android/view/View/getTop()I
iload 6
iload 7
iadd
aload 2
invokevirtual android/view/View/getBottom()I
invokevirtual android/graphics/drawable/Drawable/setBounds(IIII)V
aload 0
getfield android/support/v4/widget/DrawerLayout/Q Landroid/graphics/drawable/Drawable;
ldc_w 255.0F
fload 5
fmul
f2i
invokevirtual android/graphics/drawable/Drawable/setAlpha(I)V
aload 0
getfield android/support/v4/widget/DrawerLayout/Q Landroid/graphics/drawable/Drawable;
aload 1
invokevirtual android/graphics/drawable/Drawable/draw(Landroid/graphics/Canvas;)V
iload 16
ireturn
L13:
aload 0
getfield android/support/v4/widget/DrawerLayout/R Landroid/graphics/drawable/Drawable;
ifnull L12
aload 0
aload 2
iconst_5
invokevirtual android/support/v4/widget/DrawerLayout/a(Landroid/view/View;I)Z
ifeq L12
aload 0
getfield android/support/v4/widget/DrawerLayout/R Landroid/graphics/drawable/Drawable;
invokevirtual android/graphics/drawable/Drawable/getIntrinsicWidth()I
istore 6
aload 2
invokevirtual android/view/View/getLeft()I
istore 7
aload 0
invokevirtual android/support/v4/widget/DrawerLayout/getWidth()I
istore 8
aload 0
getfield android/support/v4/widget/DrawerLayout/h Landroid/support/v4/widget/eg;
getfield android/support/v4/widget/eg/t I
istore 9
fconst_0
iload 8
iload 7
isub
i2f
iload 9
i2f
fdiv
fconst_1
invokestatic java/lang/Math/min(FF)F
invokestatic java/lang/Math/max(FF)F
fstore 5
aload 0
getfield android/support/v4/widget/DrawerLayout/R Landroid/graphics/drawable/Drawable;
iload 7
iload 6
isub
aload 2
invokevirtual android/view/View/getTop()I
iload 7
aload 2
invokevirtual android/view/View/getBottom()I
invokevirtual android/graphics/drawable/Drawable/setBounds(IIII)V
aload 0
getfield android/support/v4/widget/DrawerLayout/R Landroid/graphics/drawable/Drawable;
ldc_w 255.0F
fload 5
fmul
f2i
invokevirtual android/graphics/drawable/Drawable/setAlpha(I)V
aload 0
getfield android/support/v4/widget/DrawerLayout/R Landroid/graphics/drawable/Drawable;
aload 1
invokevirtual android/graphics/drawable/Drawable/draw(Landroid/graphics/Canvas;)V
iload 16
ireturn
L8:
goto L9
.limit locals 19
.limit stack 6
.end method

.method public final e(Landroid/view/View;)V
aload 1
invokestatic android/support/v4/widget/DrawerLayout/d(Landroid/view/View;)Z
ifne L0
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
ldc "View "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc " is not a sliding drawer"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
getfield android/support/v4/widget/DrawerLayout/J Z
ifeq L1
aload 1
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v4/widget/ad
astore 1
aload 1
fconst_0
putfield android/support/v4/widget/ad/b F
aload 1
iconst_0
putfield android/support/v4/widget/ad/d Z
L2:
aload 0
invokevirtual android/support/v4/widget/DrawerLayout/invalidate()V
return
L1:
aload 0
aload 1
iconst_3
invokevirtual android/support/v4/widget/DrawerLayout/a(Landroid/view/View;I)Z
ifeq L3
aload 0
getfield android/support/v4/widget/DrawerLayout/g Landroid/support/v4/widget/eg;
aload 1
aload 1
invokevirtual android/view/View/getWidth()I
ineg
aload 1
invokevirtual android/view/View/getTop()I
invokevirtual android/support/v4/widget/eg/a(Landroid/view/View;II)Z
pop
goto L2
L3:
aload 0
getfield android/support/v4/widget/DrawerLayout/h Landroid/support/v4/widget/eg;
aload 1
aload 0
invokevirtual android/support/v4/widget/DrawerLayout/getWidth()I
aload 1
invokevirtual android/view/View/getTop()I
invokevirtual android/support/v4/widget/eg/a(Landroid/view/View;II)Z
pop
goto L2
.limit locals 2
.limit stack 5
.end method

.method protected generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
new android/support/v4/widget/ad
dup
iconst_m1
iconst_m1
invokespecial android/support/v4/widget/ad/<init>(II)V
areturn
.limit locals 1
.limit stack 4
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
new android/support/v4/widget/ad
dup
aload 0
invokevirtual android/support/v4/widget/DrawerLayout/getContext()Landroid/content/Context;
aload 1
invokespecial android/support/v4/widget/ad/<init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
areturn
.limit locals 2
.limit stack 4
.end method

.method protected generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
aload 1
instanceof android/support/v4/widget/ad
ifeq L0
new android/support/v4/widget/ad
dup
aload 1
checkcast android/support/v4/widget/ad
invokespecial android/support/v4/widget/ad/<init>(Landroid/support/v4/widget/ad;)V
areturn
L0:
aload 1
instanceof android/view/ViewGroup$MarginLayoutParams
ifeq L1
new android/support/v4/widget/ad
dup
aload 1
checkcast android/view/ViewGroup$MarginLayoutParams
invokespecial android/support/v4/widget/ad/<init>(Landroid/view/ViewGroup$MarginLayoutParams;)V
areturn
L1:
new android/support/v4/widget/ad
dup
aload 1
invokespecial android/support/v4/widget/ad/<init>(Landroid/view/ViewGroup$LayoutParams;)V
areturn
.limit locals 2
.limit stack 3
.end method

.method public getDrawerElevation()F
getstatic android/support/v4/widget/DrawerLayout/z Z
ifeq L0
aload 0
getfield android/support/v4/widget/DrawerLayout/B F
freturn
L0:
fconst_0
freturn
.limit locals 1
.limit stack 1
.end method

.method public getStatusBarBackgroundDrawable()Landroid/graphics/drawable/Drawable;
aload 0
getfield android/support/v4/widget/DrawerLayout/P Landroid/graphics/drawable/Drawable;
areturn
.limit locals 1
.limit stack 1
.end method

.method protected onAttachedToWindow()V
aload 0
invokespecial android/view/ViewGroup/onAttachedToWindow()V
aload 0
iconst_1
putfield android/support/v4/widget/DrawerLayout/J Z
return
.limit locals 1
.limit stack 2
.end method

.method protected onDetachedFromWindow()V
aload 0
invokespecial android/view/ViewGroup/onDetachedFromWindow()V
aload 0
iconst_1
putfield android/support/v4/widget/DrawerLayout/J Z
return
.limit locals 1
.limit stack 2
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
aload 0
aload 1
invokespecial android/view/ViewGroup/onDraw(Landroid/graphics/Canvas;)V
aload 0
getfield android/support/v4/widget/DrawerLayout/T Z
ifeq L0
aload 0
getfield android/support/v4/widget/DrawerLayout/P Landroid/graphics/drawable/Drawable;
ifnull L0
getstatic android/support/v4/widget/DrawerLayout/n Landroid/support/v4/widget/z;
aload 0
getfield android/support/v4/widget/DrawerLayout/S Ljava/lang/Object;
invokeinterface android/support/v4/widget/z/a(Ljava/lang/Object;)I 1
istore 2
iload 2
ifle L0
aload 0
getfield android/support/v4/widget/DrawerLayout/P Landroid/graphics/drawable/Drawable;
iconst_0
iconst_0
aload 0
invokevirtual android/support/v4/widget/DrawerLayout/getWidth()I
iload 2
invokevirtual android/graphics/drawable/Drawable/setBounds(IIII)V
aload 0
getfield android/support/v4/widget/DrawerLayout/P Landroid/graphics/drawable/Drawable;
aload 1
invokevirtual android/graphics/drawable/Drawable/draw(Landroid/graphics/Canvas;)V
L0:
return
.limit locals 3
.limit stack 5
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
iconst_0
istore 7
aload 1
invokestatic android/support/v4/view/bk/a(Landroid/view/MotionEvent;)I
istore 4
aload 0
getfield android/support/v4/widget/DrawerLayout/g Landroid/support/v4/widget/eg;
aload 1
invokevirtual android/support/v4/widget/eg/a(Landroid/view/MotionEvent;)Z
istore 8
aload 0
getfield android/support/v4/widget/DrawerLayout/h Landroid/support/v4/widget/eg;
aload 1
invokevirtual android/support/v4/widget/eg/a(Landroid/view/MotionEvent;)Z
istore 9
iload 4
tableswitch 0
L0
L1
L2
L1
default : L3
L3:
iconst_0
istore 4
L4:
iload 9
iload 8
ior
ifne L5
iload 4
ifne L5
aload 0
invokevirtual android/support/v4/widget/DrawerLayout/getChildCount()I
istore 5
iconst_0
istore 4
L6:
iload 4
iload 5
if_icmpge L7
aload 0
iload 4
invokevirtual android/support/v4/widget/DrawerLayout/getChildAt(I)Landroid/view/View;
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v4/widget/ad
getfield android/support/v4/widget/ad/c Z
ifeq L8
iconst_1
istore 4
L9:
iload 4
ifne L5
aload 0
getfield android/support/v4/widget/DrawerLayout/j Z
ifeq L10
L5:
iconst_1
istore 7
L10:
iload 7
ireturn
L0:
aload 1
invokevirtual android/view/MotionEvent/getX()F
fstore 2
aload 1
invokevirtual android/view/MotionEvent/getY()F
fstore 3
aload 0
fload 2
putfield android/support/v4/widget/DrawerLayout/N F
aload 0
fload 3
putfield android/support/v4/widget/DrawerLayout/O F
aload 0
getfield android/support/v4/widget/DrawerLayout/E F
fconst_0
fcmpl
ifle L11
aload 0
getfield android/support/v4/widget/DrawerLayout/g Landroid/support/v4/widget/eg;
fload 2
f2i
fload 3
f2i
invokevirtual android/support/v4/widget/eg/b(II)Landroid/view/View;
astore 1
aload 1
ifnull L11
aload 1
invokestatic android/support/v4/widget/DrawerLayout/j(Landroid/view/View;)Z
ifeq L11
iconst_1
istore 4
L12:
aload 0
iconst_0
putfield android/support/v4/widget/DrawerLayout/M Z
aload 0
iconst_0
putfield android/support/v4/widget/DrawerLayout/j Z
goto L4
L2:
aload 0
getfield android/support/v4/widget/DrawerLayout/g Landroid/support/v4/widget/eg;
astore 1
aload 1
getfield android/support/v4/widget/eg/o [F
arraylength
istore 6
iconst_0
istore 5
L13:
iload 5
iload 6
if_icmpge L14
aload 1
iload 5
invokevirtual android/support/v4/widget/eg/a(I)Z
ifeq L15
aload 1
getfield android/support/v4/widget/eg/q [F
iload 5
faload
aload 1
getfield android/support/v4/widget/eg/o [F
iload 5
faload
fsub
fstore 2
aload 1
getfield android/support/v4/widget/eg/r [F
iload 5
faload
aload 1
getfield android/support/v4/widget/eg/p [F
iload 5
faload
fsub
fstore 3
fload 2
fload 2
fmul
fload 3
fload 3
fmul
fadd
aload 1
getfield android/support/v4/widget/eg/n I
aload 1
getfield android/support/v4/widget/eg/n I
imul
i2f
fcmpl
ifle L16
iconst_1
istore 4
L17:
iload 4
ifeq L18
iconst_1
istore 4
L19:
iload 4
ifeq L3
aload 0
getfield android/support/v4/widget/DrawerLayout/G Landroid/support/v4/widget/ag;
invokevirtual android/support/v4/widget/ag/a()V
aload 0
getfield android/support/v4/widget/DrawerLayout/H Landroid/support/v4/widget/ag;
invokevirtual android/support/v4/widget/ag/a()V
iconst_0
istore 4
goto L4
L16:
iconst_0
istore 4
goto L17
L15:
iconst_0
istore 4
goto L17
L18:
iload 5
iconst_1
iadd
istore 5
goto L13
L14:
iconst_0
istore 4
goto L19
L1:
aload 0
iconst_1
invokespecial android/support/v4/widget/DrawerLayout/a(Z)V
aload 0
iconst_0
putfield android/support/v4/widget/DrawerLayout/M Z
aload 0
iconst_0
putfield android/support/v4/widget/DrawerLayout/j Z
goto L3
L8:
iload 4
iconst_1
iadd
istore 4
goto L6
L7:
iconst_0
istore 4
goto L9
L11:
iconst_0
istore 4
goto L12
.limit locals 10
.limit stack 3
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
iload 1
iconst_4
if_icmpne L0
aload 0
invokespecial android/support/v4/widget/DrawerLayout/n()Landroid/view/View;
ifnull L1
iconst_1
istore 3
L2:
iload 3
ifeq L0
aload 2
invokestatic android/support/v4/view/ab/c(Landroid/view/KeyEvent;)V
iconst_1
ireturn
L1:
iconst_0
istore 3
goto L2
L0:
aload 0
iload 1
aload 2
invokespecial android/view/ViewGroup/onKeyDown(ILandroid/view/KeyEvent;)Z
ireturn
.limit locals 4
.limit stack 3
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
iconst_0
istore 3
iload 1
iconst_4
if_icmpne L0
aload 0
invokespecial android/support/v4/widget/DrawerLayout/n()Landroid/view/View;
astore 2
aload 2
ifnull L1
aload 0
aload 2
invokevirtual android/support/v4/widget/DrawerLayout/a(Landroid/view/View;)I
ifne L1
aload 0
iconst_0
invokespecial android/support/v4/widget/DrawerLayout/a(Z)V
L1:
aload 2
ifnull L2
iconst_1
istore 3
L2:
iload 3
ireturn
L0:
aload 0
iload 1
aload 2
invokespecial android/view/ViewGroup/onKeyUp(ILandroid/view/KeyEvent;)Z
ireturn
.limit locals 4
.limit stack 3
.end method

.method protected onLayout(ZIIII)V
aload 0
iconst_1
putfield android/support/v4/widget/DrawerLayout/I Z
iload 4
iload 2
isub
istore 10
aload 0
invokevirtual android/support/v4/widget/DrawerLayout/getChildCount()I
istore 11
iconst_0
istore 4
L0:
iload 4
iload 11
if_icmpge L1
aload 0
iload 4
invokevirtual android/support/v4/widget/DrawerLayout/getChildAt(I)Landroid/view/View;
astore 15
aload 15
invokevirtual android/view/View/getVisibility()I
bipush 8
if_icmpeq L2
aload 15
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v4/widget/ad
astore 16
aload 15
invokestatic android/support/v4/widget/DrawerLayout/j(Landroid/view/View;)Z
ifeq L3
aload 15
aload 16
getfield android/support/v4/widget/ad/leftMargin I
aload 16
getfield android/support/v4/widget/ad/topMargin I
aload 16
getfield android/support/v4/widget/ad/leftMargin I
aload 15
invokevirtual android/view/View/getMeasuredWidth()I
iadd
aload 16
getfield android/support/v4/widget/ad/topMargin I
aload 15
invokevirtual android/view/View/getMeasuredHeight()I
iadd
invokevirtual android/view/View/layout(IIII)V
L2:
iload 4
iconst_1
iadd
istore 4
goto L0
L3:
aload 15
invokevirtual android/view/View/getMeasuredWidth()I
istore 12
aload 15
invokevirtual android/view/View/getMeasuredHeight()I
istore 13
aload 0
aload 15
iconst_3
invokevirtual android/support/v4/widget/DrawerLayout/a(Landroid/view/View;I)Z
ifeq L4
iload 12
ineg
istore 2
iload 12
i2f
aload 16
getfield android/support/v4/widget/ad/b F
fmul
f2i
iload 2
iadd
istore 7
iload 12
iload 7
iadd
i2f
iload 12
i2f
fdiv
fstore 6
L5:
fload 6
aload 16
getfield android/support/v4/widget/ad/b F
fcmpl
ifeq L6
iconst_1
istore 8
L7:
aload 16
getfield android/support/v4/widget/ad/a I
bipush 112
iand
lookupswitch
16 : L8
80 : L9
default : L10
L10:
aload 15
iload 7
aload 16
getfield android/support/v4/widget/ad/topMargin I
iload 12
iload 7
iadd
iload 13
aload 16
getfield android/support/v4/widget/ad/topMargin I
iadd
invokevirtual android/view/View/layout(IIII)V
L11:
iload 8
ifeq L12
aload 0
aload 15
fload 6
invokevirtual android/support/v4/widget/DrawerLayout/a(Landroid/view/View;F)V
L12:
aload 16
getfield android/support/v4/widget/ad/b F
fconst_0
fcmpl
ifle L13
iconst_0
istore 2
L14:
aload 15
invokevirtual android/view/View/getVisibility()I
iload 2
if_icmpeq L2
aload 15
iload 2
invokevirtual android/view/View/setVisibility(I)V
goto L2
L4:
iload 10
iload 12
i2f
aload 16
getfield android/support/v4/widget/ad/b F
fmul
f2i
isub
istore 7
iload 10
iload 7
isub
i2f
iload 12
i2f
fdiv
fstore 6
goto L5
L6:
iconst_0
istore 8
goto L7
L9:
iload 5
iload 3
isub
istore 2
aload 15
iload 7
iload 2
aload 16
getfield android/support/v4/widget/ad/bottomMargin I
isub
aload 15
invokevirtual android/view/View/getMeasuredHeight()I
isub
iload 12
iload 7
iadd
iload 2
aload 16
getfield android/support/v4/widget/ad/bottomMargin I
isub
invokevirtual android/view/View/layout(IIII)V
goto L11
L8:
iload 5
iload 3
isub
istore 14
iload 14
iload 13
isub
iconst_2
idiv
istore 9
iload 9
aload 16
getfield android/support/v4/widget/ad/topMargin I
if_icmpge L15
aload 16
getfield android/support/v4/widget/ad/topMargin I
istore 2
L16:
aload 15
iload 7
iload 2
iload 12
iload 7
iadd
iload 13
iload 2
iadd
invokevirtual android/view/View/layout(IIII)V
goto L11
L15:
iload 9
istore 2
iload 9
iload 13
iadd
iload 14
aload 16
getfield android/support/v4/widget/ad/bottomMargin I
isub
if_icmple L16
iload 14
aload 16
getfield android/support/v4/widget/ad/bottomMargin I
isub
iload 13
isub
istore 2
goto L16
L13:
iconst_4
istore 2
goto L14
L1:
aload 0
iconst_0
putfield android/support/v4/widget/DrawerLayout/I Z
aload 0
iconst_0
putfield android/support/v4/widget/DrawerLayout/J Z
return
.limit locals 17
.limit stack 6
.end method

.method protected onMeasure(II)V
sipush 300
istore 7
iconst_0
istore 6
iload 1
invokestatic android/view/View$MeasureSpec/getMode(I)I
istore 10
iload 2
invokestatic android/view/View$MeasureSpec/getMode(I)I
istore 9
iload 1
invokestatic android/view/View$MeasureSpec/getSize(I)I
istore 5
iload 2
invokestatic android/view/View$MeasureSpec/getSize(I)I
istore 8
iload 10
ldc_w 1073741824
if_icmpne L0
iload 5
istore 4
iload 9
ldc_w 1073741824
if_icmpeq L1
L0:
aload 0
invokevirtual android/support/v4/widget/DrawerLayout/isInEditMode()Z
ifeq L2
iload 5
istore 3
iload 10
ldc_w -2147483648
if_icmpeq L3
iload 5
istore 3
iload 10
ifne L3
sipush 300
istore 3
L3:
iload 3
istore 4
iload 9
ldc_w -2147483648
if_icmpeq L1
iload 3
istore 4
iload 9
ifne L1
iload 3
istore 4
iload 7
istore 3
L4:
aload 0
iload 4
iload 3
invokevirtual android/support/v4/widget/DrawerLayout/setMeasuredDimension(II)V
aload 0
getfield android/support/v4/widget/DrawerLayout/S Ljava/lang/Object;
ifnull L5
aload 0
invokestatic android/support/v4/view/cx/u(Landroid/view/View;)Z
ifeq L5
iconst_1
istore 5
L6:
aload 0
invokestatic android/support/v4/view/cx/f(Landroid/view/View;)I
istore 7
aload 0
invokevirtual android/support/v4/widget/DrawerLayout/getChildCount()I
istore 8
L7:
iload 6
iload 8
if_icmpge L8
aload 0
iload 6
invokevirtual android/support/v4/widget/DrawerLayout/getChildAt(I)Landroid/view/View;
astore 11
aload 11
invokevirtual android/view/View/getVisibility()I
bipush 8
if_icmpeq L9
aload 11
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v4/widget/ad
astore 12
iload 5
ifeq L10
aload 12
getfield android/support/v4/widget/ad/a I
iload 7
invokestatic android/support/v4/view/v/a(II)I
istore 9
aload 11
invokestatic android/support/v4/view/cx/u(Landroid/view/View;)Z
ifeq L11
getstatic android/support/v4/widget/DrawerLayout/n Landroid/support/v4/widget/z;
aload 11
aload 0
getfield android/support/v4/widget/DrawerLayout/S Ljava/lang/Object;
iload 9
invokeinterface android/support/v4/widget/z/a(Landroid/view/View;Ljava/lang/Object;I)V 3
L10:
aload 11
invokestatic android/support/v4/widget/DrawerLayout/j(Landroid/view/View;)Z
ifeq L12
aload 11
iload 4
aload 12
getfield android/support/v4/widget/ad/leftMargin I
isub
aload 12
getfield android/support/v4/widget/ad/rightMargin I
isub
ldc_w 1073741824
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
iload 3
aload 12
getfield android/support/v4/widget/ad/topMargin I
isub
aload 12
getfield android/support/v4/widget/ad/bottomMargin I
isub
ldc_w 1073741824
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
invokevirtual android/view/View/measure(II)V
L9:
iload 6
iconst_1
iadd
istore 6
goto L7
L2:
new java/lang/IllegalArgumentException
dup
ldc "DrawerLayout must be measured with MeasureSpec.EXACTLY."
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L5:
iconst_0
istore 5
goto L6
L11:
getstatic android/support/v4/widget/DrawerLayout/n Landroid/support/v4/widget/z;
aload 12
aload 0
getfield android/support/v4/widget/DrawerLayout/S Ljava/lang/Object;
iload 9
invokeinterface android/support/v4/widget/z/a(Landroid/view/ViewGroup$MarginLayoutParams;Ljava/lang/Object;I)V 3
goto L10
L12:
aload 11
invokestatic android/support/v4/widget/DrawerLayout/d(Landroid/view/View;)Z
ifeq L13
getstatic android/support/v4/widget/DrawerLayout/z Z
ifeq L14
aload 11
invokestatic android/support/v4/view/cx/r(Landroid/view/View;)F
aload 0
getfield android/support/v4/widget/DrawerLayout/B F
fcmpl
ifeq L14
aload 11
aload 0
getfield android/support/v4/widget/DrawerLayout/B F
invokestatic android/support/v4/view/cx/f(Landroid/view/View;F)V
L14:
aload 0
aload 11
invokevirtual android/support/v4/widget/DrawerLayout/c(Landroid/view/View;)I
bipush 7
iand
istore 9
iload 9
iconst_0
iand
ifeq L15
new java/lang/IllegalStateException
dup
new java/lang/StringBuilder
dup
ldc "Child drawer has absolute gravity "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
iload 9
invokestatic android/support/v4/widget/DrawerLayout/d(I)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " but this DrawerLayout already has a drawer view along that edge"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L15:
aload 11
iload 1
aload 0
getfield android/support/v4/widget/DrawerLayout/C I
aload 12
getfield android/support/v4/widget/ad/leftMargin I
iadd
aload 12
getfield android/support/v4/widget/ad/rightMargin I
iadd
aload 12
getfield android/support/v4/widget/ad/width I
invokestatic android/support/v4/widget/DrawerLayout/getChildMeasureSpec(III)I
iload 2
aload 12
getfield android/support/v4/widget/ad/topMargin I
aload 12
getfield android/support/v4/widget/ad/bottomMargin I
iadd
aload 12
getfield android/support/v4/widget/ad/height I
invokestatic android/support/v4/widget/DrawerLayout/getChildMeasureSpec(III)I
invokevirtual android/view/View/measure(II)V
goto L9
L13:
new java/lang/IllegalStateException
dup
new java/lang/StringBuilder
dup
ldc "Child "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 11
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc " at index "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 6
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc " does not have a valid layout_gravity - must be Gravity.LEFT, Gravity.RIGHT or Gravity.NO_GRAVITY"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L8:
return
L1:
iload 8
istore 3
goto L4
.limit locals 13
.limit stack 5
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
aload 1
checkcast android/support/v4/widget/DrawerLayout$SavedState
astore 1
aload 0
aload 1
invokevirtual android/support/v4/widget/DrawerLayout$SavedState/getSuperState()Landroid/os/Parcelable;
invokespecial android/view/ViewGroup/onRestoreInstanceState(Landroid/os/Parcelable;)V
aload 1
getfield android/support/v4/widget/DrawerLayout$SavedState/a I
ifeq L0
aload 0
aload 1
getfield android/support/v4/widget/DrawerLayout$SavedState/a I
invokevirtual android/support/v4/widget/DrawerLayout/a(I)Landroid/view/View;
astore 2
aload 2
ifnull L0
aload 0
aload 2
invokespecial android/support/v4/widget/DrawerLayout/k(Landroid/view/View;)V
L0:
aload 0
aload 1
getfield android/support/v4/widget/DrawerLayout$SavedState/b I
iconst_3
invokespecial android/support/v4/widget/DrawerLayout/b(II)V
aload 0
aload 1
getfield android/support/v4/widget/DrawerLayout$SavedState/c I
iconst_5
invokespecial android/support/v4/widget/DrawerLayout/b(II)V
return
.limit locals 3
.limit stack 3
.end method

.method public onRtlPropertiesChanged(I)V
aload 0
invokespecial android/support/v4/widget/DrawerLayout/h()V
return
.limit locals 2
.limit stack 1
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
new android/support/v4/widget/DrawerLayout$SavedState
dup
aload 0
invokespecial android/view/ViewGroup/onSaveInstanceState()Landroid/os/Parcelable;
invokespecial android/support/v4/widget/DrawerLayout$SavedState/<init>(Landroid/os/Parcelable;)V
astore 1
aload 0
invokespecial android/support/v4/widget/DrawerLayout/g()Landroid/view/View;
astore 2
aload 2
ifnull L0
aload 1
aload 2
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v4/widget/ad
getfield android/support/v4/widget/ad/a I
putfield android/support/v4/widget/DrawerLayout$SavedState/a I
L0:
aload 1
aload 0
getfield android/support/v4/widget/DrawerLayout/K I
putfield android/support/v4/widget/DrawerLayout$SavedState/b I
aload 1
aload 0
getfield android/support/v4/widget/DrawerLayout/L I
putfield android/support/v4/widget/DrawerLayout$SavedState/c I
aload 1
areturn
.limit locals 3
.limit stack 3
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
aload 0
getfield android/support/v4/widget/DrawerLayout/g Landroid/support/v4/widget/eg;
aload 1
invokevirtual android/support/v4/widget/eg/b(Landroid/view/MotionEvent;)V
aload 0
getfield android/support/v4/widget/DrawerLayout/h Landroid/support/v4/widget/eg;
aload 1
invokevirtual android/support/v4/widget/eg/b(Landroid/view/MotionEvent;)V
aload 1
invokevirtual android/view/MotionEvent/getAction()I
sipush 255
iand
tableswitch 0
L0
L1
L2
L3
default : L2
L2:
iconst_1
ireturn
L0:
aload 1
invokevirtual android/view/MotionEvent/getX()F
fstore 2
aload 1
invokevirtual android/view/MotionEvent/getY()F
fstore 3
aload 0
fload 2
putfield android/support/v4/widget/DrawerLayout/N F
aload 0
fload 3
putfield android/support/v4/widget/DrawerLayout/O F
aload 0
iconst_0
putfield android/support/v4/widget/DrawerLayout/M Z
aload 0
iconst_0
putfield android/support/v4/widget/DrawerLayout/j Z
iconst_1
ireturn
L1:
aload 1
invokevirtual android/view/MotionEvent/getX()F
fstore 3
aload 1
invokevirtual android/view/MotionEvent/getY()F
fstore 2
aload 0
getfield android/support/v4/widget/DrawerLayout/g Landroid/support/v4/widget/eg;
fload 3
f2i
fload 2
f2i
invokevirtual android/support/v4/widget/eg/b(II)Landroid/view/View;
astore 1
aload 1
ifnull L4
aload 1
invokestatic android/support/v4/widget/DrawerLayout/j(Landroid/view/View;)Z
ifeq L4
fload 3
aload 0
getfield android/support/v4/widget/DrawerLayout/N F
fsub
fstore 3
fload 2
aload 0
getfield android/support/v4/widget/DrawerLayout/O F
fsub
fstore 2
aload 0
getfield android/support/v4/widget/DrawerLayout/g Landroid/support/v4/widget/eg;
getfield android/support/v4/widget/eg/n I
istore 4
fload 3
fload 3
fmul
fload 2
fload 2
fmul
fadd
iload 4
iload 4
imul
i2f
fcmpg
ifge L4
aload 0
invokespecial android/support/v4/widget/DrawerLayout/g()Landroid/view/View;
astore 1
aload 1
ifnull L4
aload 0
aload 1
invokevirtual android/support/v4/widget/DrawerLayout/a(Landroid/view/View;)I
iconst_2
if_icmpne L5
iconst_1
istore 5
L6:
aload 0
iload 5
invokespecial android/support/v4/widget/DrawerLayout/a(Z)V
aload 0
iconst_0
putfield android/support/v4/widget/DrawerLayout/M Z
iconst_1
ireturn
L5:
iconst_0
istore 5
goto L6
L3:
aload 0
iconst_1
invokespecial android/support/v4/widget/DrawerLayout/a(Z)V
aload 0
iconst_0
putfield android/support/v4/widget/DrawerLayout/M Z
aload 0
iconst_0
putfield android/support/v4/widget/DrawerLayout/j Z
iconst_1
ireturn
L4:
iconst_1
istore 5
goto L6
.limit locals 6
.limit stack 3
.end method

.method public requestDisallowInterceptTouchEvent(Z)V
aload 0
iload 1
invokespecial android/view/ViewGroup/requestDisallowInterceptTouchEvent(Z)V
aload 0
iload 1
putfield android/support/v4/widget/DrawerLayout/M Z
iload 1
ifeq L0
aload 0
iconst_1
invokespecial android/support/v4/widget/DrawerLayout/a(Z)V
L0:
return
.limit locals 2
.limit stack 2
.end method

.method public requestLayout()V
aload 0
getfield android/support/v4/widget/DrawerLayout/I Z
ifne L0
aload 0
invokespecial android/view/ViewGroup/requestLayout()V
L0:
return
.limit locals 1
.limit stack 1
.end method

.method public setDrawerElevation(F)V
aload 0
fload 1
putfield android/support/v4/widget/DrawerLayout/B F
iconst_0
istore 2
L0:
iload 2
aload 0
invokevirtual android/support/v4/widget/DrawerLayout/getChildCount()I
if_icmpge L1
aload 0
iload 2
invokevirtual android/support/v4/widget/DrawerLayout/getChildAt(I)Landroid/view/View;
astore 3
aload 3
invokestatic android/support/v4/widget/DrawerLayout/d(Landroid/view/View;)Z
ifeq L2
aload 3
aload 0
getfield android/support/v4/widget/DrawerLayout/B F
invokestatic android/support/v4/view/cx/f(Landroid/view/View;F)V
L2:
iload 2
iconst_1
iadd
istore 2
goto L0
L1:
return
.limit locals 4
.limit stack 2
.end method

.method public setDrawerListener(Landroid/support/v4/widget/ac;)V
aload 0
aload 1
putfield android/support/v4/widget/DrawerLayout/k Landroid/support/v4/widget/ac;
return
.limit locals 2
.limit stack 2
.end method

.method public setDrawerLockMode(I)V
aload 0
iload 1
iconst_3
invokespecial android/support/v4/widget/DrawerLayout/b(II)V
aload 0
iload 1
iconst_5
invokespecial android/support/v4/widget/DrawerLayout/b(II)V
return
.limit locals 2
.limit stack 3
.end method

.method public setScrimColor(I)V
.annotation invisibleparam 1 Landroid/support/a/j;
.end annotation
aload 0
iload 1
putfield android/support/v4/widget/DrawerLayout/D I
aload 0
invokevirtual android/support/v4/widget/DrawerLayout/invalidate()V
return
.limit locals 2
.limit stack 2
.end method

.method public setStatusBarBackground(I)V
iload 1
ifeq L0
aload 0
invokevirtual android/support/v4/widget/DrawerLayout/getContext()Landroid/content/Context;
iload 1
invokestatic android/support/v4/c/h/a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;
astore 2
L1:
aload 0
aload 2
putfield android/support/v4/widget/DrawerLayout/P Landroid/graphics/drawable/Drawable;
aload 0
invokevirtual android/support/v4/widget/DrawerLayout/invalidate()V
return
L0:
aconst_null
astore 2
goto L1
.limit locals 3
.limit stack 2
.end method

.method public setStatusBarBackground(Landroid/graphics/drawable/Drawable;)V
aload 0
aload 1
putfield android/support/v4/widget/DrawerLayout/P Landroid/graphics/drawable/Drawable;
aload 0
invokevirtual android/support/v4/widget/DrawerLayout/invalidate()V
return
.limit locals 2
.limit stack 2
.end method

.method public setStatusBarBackgroundColor(I)V
.annotation invisibleparam 1 Landroid/support/a/j;
.end annotation
aload 0
new android/graphics/drawable/ColorDrawable
dup
iload 1
invokespecial android/graphics/drawable/ColorDrawable/<init>(I)V
putfield android/support/v4/widget/DrawerLayout/P Landroid/graphics/drawable/Drawable;
aload 0
invokevirtual android/support/v4/widget/DrawerLayout/invalidate()V
return
.limit locals 2
.limit stack 4
.end method
