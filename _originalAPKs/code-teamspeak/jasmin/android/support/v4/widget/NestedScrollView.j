.bytecode 50.0
.class public final synchronized android/support/v4/widget/NestedScrollView
.super android/widget/FrameLayout
.implements android/support/v4/view/bt
.implements android/support/v4/view/bv

.field private static final 'A' [I

.field static final 'a' I = 250


.field static final 'b' F = 0.5F


.field private static final 'c' Ljava/lang/String; = "NestedScrollView"

.field private static final 'x' I = -1


.field private static final 'z' Landroid/support/v4/widget/bh;

.field private final 'B' Landroid/support/v4/view/bw;

.field private final 'C' Landroid/support/v4/view/bu;

.field private 'D' F

.field private 'd' J

.field private final 'e' Landroid/graphics/Rect;

.field private 'f' Landroid/support/v4/widget/ca;

.field private 'g' Landroid/support/v4/widget/al;

.field private 'h' Landroid/support/v4/widget/al;

.field private 'i' I

.field private 'j' Z

.field private 'k' Z

.field private 'l' Landroid/view/View;

.field private 'm' Z

.field private 'n' Landroid/view/VelocityTracker;

.field private 'o' Z

.field private 'p' Z

.field private 'q' I

.field private 'r' I

.field private 's' I

.field private 't' I

.field private final 'u' [I

.field private final 'v' [I

.field private 'w' I

.field private 'y' Landroid/support/v4/widget/NestedScrollView$SavedState;

.method static <clinit>()V
new android/support/v4/widget/bh
dup
invokespecial android/support/v4/widget/bh/<init>()V
putstatic android/support/v4/widget/NestedScrollView/z Landroid/support/v4/widget/bh;
iconst_1
newarray int
dup
iconst_0
ldc_w 16843130
iastore
putstatic android/support/v4/widget/NestedScrollView/A [I
return
.limit locals 0
.limit stack 4
.end method

.method private <init>(Landroid/content/Context;)V
aload 0
aload 1
iconst_0
invokespecial android/support/v4/widget/NestedScrollView/<init>(Landroid/content/Context;B)V
return
.limit locals 2
.limit stack 3
.end method

.method private <init>(Landroid/content/Context;B)V
aload 0
aload 1
iconst_0
invokespecial android/support/v4/widget/NestedScrollView/<init>(Landroid/content/Context;C)V
return
.limit locals 3
.limit stack 3
.end method

.method private <init>(Landroid/content/Context;C)V
aload 0
aload 1
aconst_null
iconst_0
invokespecial android/widget/FrameLayout/<init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
aload 0
new android/graphics/Rect
dup
invokespecial android/graphics/Rect/<init>()V
putfield android/support/v4/widget/NestedScrollView/e Landroid/graphics/Rect;
aload 0
iconst_1
putfield android/support/v4/widget/NestedScrollView/j Z
aload 0
iconst_0
putfield android/support/v4/widget/NestedScrollView/k Z
aload 0
aconst_null
putfield android/support/v4/widget/NestedScrollView/l Landroid/view/View;
aload 0
iconst_0
putfield android/support/v4/widget/NestedScrollView/m Z
aload 0
iconst_1
putfield android/support/v4/widget/NestedScrollView/p Z
aload 0
iconst_m1
putfield android/support/v4/widget/NestedScrollView/t I
aload 0
iconst_2
newarray int
putfield android/support/v4/widget/NestedScrollView/u [I
aload 0
iconst_2
newarray int
putfield android/support/v4/widget/NestedScrollView/v [I
aload 0
new android/support/v4/widget/ca
dup
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getContext()Landroid/content/Context;
aconst_null
invokespecial android/support/v4/widget/ca/<init>(Landroid/content/Context;Landroid/view/animation/Interpolator;)V
putfield android/support/v4/widget/NestedScrollView/f Landroid/support/v4/widget/ca;
aload 0
iconst_1
invokevirtual android/support/v4/widget/NestedScrollView/setFocusable(Z)V
aload 0
ldc_w 262144
invokevirtual android/support/v4/widget/NestedScrollView/setDescendantFocusability(I)V
aload 0
iconst_0
invokevirtual android/support/v4/widget/NestedScrollView/setWillNotDraw(Z)V
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getContext()Landroid/content/Context;
invokestatic android/view/ViewConfiguration/get(Landroid/content/Context;)Landroid/view/ViewConfiguration;
astore 3
aload 0
aload 3
invokevirtual android/view/ViewConfiguration/getScaledTouchSlop()I
putfield android/support/v4/widget/NestedScrollView/q I
aload 0
aload 3
invokevirtual android/view/ViewConfiguration/getScaledMinimumFlingVelocity()I
putfield android/support/v4/widget/NestedScrollView/r I
aload 0
aload 3
invokevirtual android/view/ViewConfiguration/getScaledMaximumFlingVelocity()I
putfield android/support/v4/widget/NestedScrollView/s I
aload 1
aconst_null
getstatic android/support/v4/widget/NestedScrollView/A [I
iconst_0
iconst_0
invokevirtual android/content/Context/obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;
astore 1
aload 0
aload 1
iconst_0
iconst_0
invokevirtual android/content/res/TypedArray/getBoolean(IZ)Z
invokevirtual android/support/v4/widget/NestedScrollView/setFillViewport(Z)V
aload 1
invokevirtual android/content/res/TypedArray/recycle()V
aload 0
new android/support/v4/view/bw
dup
aload 0
invokespecial android/support/v4/view/bw/<init>(Landroid/view/ViewGroup;)V
putfield android/support/v4/widget/NestedScrollView/B Landroid/support/v4/view/bw;
aload 0
new android/support/v4/view/bu
dup
aload 0
invokespecial android/support/v4/view/bu/<init>(Landroid/view/View;)V
putfield android/support/v4/widget/NestedScrollView/C Landroid/support/v4/view/bu;
aload 0
iconst_1
invokevirtual android/support/v4/widget/NestedScrollView/setNestedScrollingEnabled(Z)V
aload 0
getstatic android/support/v4/widget/NestedScrollView/z Landroid/support/v4/widget/bh;
invokestatic android/support/v4/view/cx/a(Landroid/view/View;Landroid/support/v4/view/a;)V
return
.limit locals 4
.limit stack 5
.end method

.method private a(Landroid/graphics/Rect;)I
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getChildCount()I
ifne L0
iconst_0
ireturn
L0:
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getHeight()I
istore 5
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getScrollY()I
istore 2
iload 2
iload 5
iadd
istore 4
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getVerticalFadingEdgeLength()I
istore 6
iload 2
istore 3
aload 1
getfield android/graphics/Rect/top I
ifle L1
iload 2
iload 6
iadd
istore 3
L1:
iload 4
istore 2
aload 1
getfield android/graphics/Rect/bottom I
aload 0
iconst_0
invokevirtual android/support/v4/widget/NestedScrollView/getChildAt(I)Landroid/view/View;
invokevirtual android/view/View/getHeight()I
if_icmpge L2
iload 4
iload 6
isub
istore 2
L2:
aload 1
getfield android/graphics/Rect/bottom I
iload 2
if_icmple L3
aload 1
getfield android/graphics/Rect/top I
iload 3
if_icmple L3
aload 1
invokevirtual android/graphics/Rect/height()I
iload 5
if_icmple L4
aload 1
getfield android/graphics/Rect/top I
iload 3
isub
iconst_0
iadd
istore 3
L5:
iload 3
aload 0
iconst_0
invokevirtual android/support/v4/widget/NestedScrollView/getChildAt(I)Landroid/view/View;
invokevirtual android/view/View/getBottom()I
iload 2
isub
invokestatic java/lang/Math/min(II)I
istore 2
L6:
iload 2
ireturn
L4:
aload 1
getfield android/graphics/Rect/bottom I
iload 2
isub
iconst_0
iadd
istore 3
goto L5
L3:
aload 1
getfield android/graphics/Rect/top I
iload 3
if_icmpge L7
aload 1
getfield android/graphics/Rect/bottom I
iload 2
if_icmpge L7
aload 1
invokevirtual android/graphics/Rect/height()I
iload 5
if_icmple L8
iconst_0
iload 2
aload 1
getfield android/graphics/Rect/bottom I
isub
isub
istore 2
L9:
iload 2
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getScrollY()I
ineg
invokestatic java/lang/Math/max(II)I
istore 2
goto L6
L8:
iconst_0
iload 3
aload 1
getfield android/graphics/Rect/top I
isub
isub
istore 2
goto L9
L7:
iconst_0
istore 2
goto L6
.limit locals 7
.limit stack 3
.end method

.method static synthetic a(Landroid/support/v4/widget/NestedScrollView;)I
aload 0
invokespecial android/support/v4/widget/NestedScrollView/getScrollRange()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private a(ZII)Landroid/view/View;
aload 0
iconst_2
invokevirtual android/support/v4/widget/NestedScrollView/getFocusables(I)Ljava/util/ArrayList;
astore 12
aconst_null
astore 11
iconst_0
istore 4
aload 12
invokeinterface java/util/List/size()I 0
istore 8
iconst_0
istore 6
L0:
iload 6
iload 8
if_icmpge L1
aload 12
iload 6
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast android/view/View
astore 10
aload 10
invokevirtual android/view/View/getTop()I
istore 7
aload 10
invokevirtual android/view/View/getBottom()I
istore 9
iload 2
iload 9
if_icmpge L2
iload 7
iload 3
if_icmpge L2
iload 2
iload 7
if_icmpge L3
iload 9
iload 3
if_icmpge L3
iconst_1
istore 5
L4:
aload 11
ifnonnull L5
iload 5
istore 4
L6:
iload 6
iconst_1
iadd
istore 6
aload 10
astore 11
goto L0
L3:
iconst_0
istore 5
goto L4
L5:
iload 1
ifeq L7
iload 7
aload 11
invokevirtual android/view/View/getTop()I
if_icmplt L8
L7:
iload 1
ifne L9
iload 9
aload 11
invokevirtual android/view/View/getBottom()I
if_icmple L9
L8:
iconst_1
istore 7
L10:
iload 4
ifeq L11
iload 5
ifeq L2
iload 7
ifeq L2
goto L6
L9:
iconst_0
istore 7
goto L10
L11:
iload 5
ifeq L12
iconst_1
istore 4
goto L6
L12:
iload 7
ifeq L2
goto L6
L1:
aload 11
areturn
L2:
aload 11
astore 10
goto L6
.limit locals 13
.limit stack 2
.end method

.method private a()V
aload 0
new android/support/v4/widget/ca
dup
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getContext()Landroid/content/Context;
aconst_null
invokespecial android/support/v4/widget/ca/<init>(Landroid/content/Context;Landroid/view/animation/Interpolator;)V
putfield android/support/v4/widget/NestedScrollView/f Landroid/support/v4/widget/ca;
aload 0
iconst_1
invokevirtual android/support/v4/widget/NestedScrollView/setFocusable(Z)V
aload 0
ldc_w 262144
invokevirtual android/support/v4/widget/NestedScrollView/setDescendantFocusability(I)V
aload 0
iconst_0
invokevirtual android/support/v4/widget/NestedScrollView/setWillNotDraw(Z)V
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getContext()Landroid/content/Context;
invokestatic android/view/ViewConfiguration/get(Landroid/content/Context;)Landroid/view/ViewConfiguration;
astore 1
aload 0
aload 1
invokevirtual android/view/ViewConfiguration/getScaledTouchSlop()I
putfield android/support/v4/widget/NestedScrollView/q I
aload 0
aload 1
invokevirtual android/view/ViewConfiguration/getScaledMinimumFlingVelocity()I
putfield android/support/v4/widget/NestedScrollView/r I
aload 0
aload 1
invokevirtual android/view/ViewConfiguration/getScaledMaximumFlingVelocity()I
putfield android/support/v4/widget/NestedScrollView/s I
return
.limit locals 2
.limit stack 5
.end method

.method private a(Landroid/view/MotionEvent;)V
aload 1
invokevirtual android/view/MotionEvent/getAction()I
ldc_w 65280
iand
bipush 8
ishr
istore 2
aload 1
iload 2
invokestatic android/support/v4/view/bk/b(Landroid/view/MotionEvent;I)I
aload 0
getfield android/support/v4/widget/NestedScrollView/t I
if_icmpne L0
iload 2
ifne L1
iconst_1
istore 2
L2:
aload 0
aload 1
iload 2
invokestatic android/support/v4/view/bk/d(Landroid/view/MotionEvent;I)F
f2i
putfield android/support/v4/widget/NestedScrollView/i I
aload 0
aload 1
iload 2
invokestatic android/support/v4/view/bk/b(Landroid/view/MotionEvent;I)I
putfield android/support/v4/widget/NestedScrollView/t I
aload 0
getfield android/support/v4/widget/NestedScrollView/n Landroid/view/VelocityTracker;
ifnull L0
aload 0
getfield android/support/v4/widget/NestedScrollView/n Landroid/view/VelocityTracker;
invokevirtual android/view/VelocityTracker/clear()V
L0:
return
L1:
iconst_0
istore 2
goto L2
.limit locals 3
.limit stack 3
.end method

.method private a(II)Z
iconst_0
istore 5
iload 5
istore 4
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getChildCount()I
ifle L0
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getScrollY()I
istore 3
aload 0
iconst_0
invokevirtual android/support/v4/widget/NestedScrollView/getChildAt(I)Landroid/view/View;
astore 6
iload 5
istore 4
iload 2
aload 6
invokevirtual android/view/View/getTop()I
iload 3
isub
if_icmplt L0
iload 5
istore 4
iload 2
aload 6
invokevirtual android/view/View/getBottom()I
iload 3
isub
if_icmpge L0
iload 5
istore 4
iload 1
aload 6
invokevirtual android/view/View/getLeft()I
if_icmplt L0
iload 5
istore 4
iload 1
aload 6
invokevirtual android/view/View/getRight()I
if_icmpge L0
iconst_1
istore 4
L0:
iload 4
ireturn
.limit locals 7
.limit stack 3
.end method

.method private a(III)Z
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getHeight()I
istore 4
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getScrollY()I
istore 9
iload 9
iload 4
iadd
istore 10
iload 1
bipush 33
if_icmpne L0
iconst_1
istore 6
L1:
aload 0
iconst_2
invokevirtual android/support/v4/widget/NestedScrollView/getFocusables(I)Ljava/util/ArrayList;
astore 16
aconst_null
astore 14
iconst_0
istore 4
aload 16
invokeinterface java/util/List/size()I 0
istore 11
iconst_0
istore 7
L2:
iload 7
iload 11
if_icmpge L3
aload 16
iload 7
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast android/view/View
astore 15
aload 15
invokevirtual android/view/View/getTop()I
istore 8
aload 15
invokevirtual android/view/View/getBottom()I
istore 12
iload 2
iload 12
if_icmpge L4
iload 8
iload 3
if_icmpge L4
iload 2
iload 8
if_icmpge L5
iload 12
iload 3
if_icmpge L5
iconst_1
istore 5
L6:
aload 14
ifnonnull L7
aload 15
astore 14
iload 5
istore 4
L8:
iload 7
iconst_1
iadd
istore 7
goto L2
L0:
iconst_0
istore 6
goto L1
L5:
iconst_0
istore 5
goto L6
L7:
iload 6
ifeq L9
iload 8
aload 14
invokevirtual android/view/View/getTop()I
if_icmplt L10
L9:
iload 6
ifne L11
iload 12
aload 14
invokevirtual android/view/View/getBottom()I
if_icmple L11
L10:
iconst_1
istore 8
L12:
iload 4
ifeq L13
iload 5
ifeq L4
iload 8
ifeq L4
aload 15
astore 14
goto L8
L11:
iconst_0
istore 8
goto L12
L13:
iload 5
ifeq L14
aload 15
astore 14
iconst_1
istore 4
goto L8
L14:
iload 8
ifeq L4
aload 15
astore 14
goto L8
L3:
aload 14
astore 15
aload 14
ifnonnull L15
aload 0
astore 15
L15:
iload 2
iload 9
if_icmplt L16
iload 3
iload 10
if_icmpgt L16
iconst_0
istore 13
L17:
aload 15
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/findFocus()Landroid/view/View;
if_acmpeq L18
aload 15
iload 1
invokevirtual android/view/View/requestFocus(I)Z
pop
L18:
iload 13
ireturn
L16:
iload 6
ifeq L19
iload 2
iload 9
isub
istore 2
L20:
aload 0
iload 2
invokespecial android/support/v4/widget/NestedScrollView/e(I)V
iconst_1
istore 13
goto L17
L19:
iload 3
iload 10
isub
istore 2
goto L20
L4:
goto L8
.limit locals 17
.limit stack 2
.end method

.method private a(IIIII)Z
iconst_0
istore 8
aload 0
invokestatic android/support/v4/view/cx/a(Landroid/view/View;)I
pop
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/computeHorizontalScrollRange()I
pop
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/computeHorizontalScrollExtent()I
pop
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/computeVerticalScrollRange()I
pop
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/computeVerticalScrollExtent()I
pop
iload 3
iload 1
iadd
istore 1
iload 4
iload 2
iadd
istore 2
iload 5
iconst_0
iadd
istore 3
iload 1
ifle L0
iconst_1
istore 6
iconst_0
istore 1
L1:
iload 2
iload 3
if_icmple L2
iload 3
istore 2
iconst_1
istore 7
L3:
aload 0
iload 1
iload 2
iload 6
iload 7
invokevirtual android/support/v4/widget/NestedScrollView/onOverScrolled(IIZZ)V
iload 6
ifne L4
iload 8
istore 6
iload 7
ifeq L5
L4:
iconst_1
istore 6
L5:
iload 6
ireturn
L0:
iload 1
ifge L6
iconst_1
istore 6
iconst_0
istore 1
goto L1
L2:
iload 2
ifge L7
iconst_1
istore 7
iconst_0
istore 2
goto L3
L7:
iconst_0
istore 7
goto L3
L6:
iconst_0
istore 6
goto L1
.limit locals 9
.limit stack 5
.end method

.method private a(Landroid/graphics/Rect;Z)Z
aload 0
aload 1
invokespecial android/support/v4/widget/NestedScrollView/a(Landroid/graphics/Rect;)I
istore 3
iload 3
ifeq L0
iconst_1
istore 4
L1:
iload 4
ifeq L2
iload 2
ifeq L3
aload 0
iconst_0
iload 3
invokevirtual android/support/v4/widget/NestedScrollView/scrollBy(II)V
L2:
iload 4
ireturn
L0:
iconst_0
istore 4
goto L1
L3:
aload 0
iconst_0
iload 3
invokespecial android/support/v4/widget/NestedScrollView/b(II)V
iload 4
ireturn
.limit locals 5
.limit stack 3
.end method

.method private a(Landroid/view/KeyEvent;)Z
iconst_0
istore 6
aload 0
getfield android/support/v4/widget/NestedScrollView/e Landroid/graphics/Rect;
invokevirtual android/graphics/Rect/setEmpty()V
aload 0
iconst_0
invokevirtual android/support/v4/widget/NestedScrollView/getChildAt(I)Landroid/view/View;
astore 7
aload 7
ifnull L0
aload 7
invokevirtual android/view/View/getHeight()I
istore 2
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getHeight()I
iload 2
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getPaddingTop()I
iadd
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getPaddingBottom()I
iadd
if_icmpge L1
iconst_1
istore 2
L2:
iload 2
ifne L3
iload 6
istore 5
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/isFocused()Z
ifeq L4
iload 6
istore 5
aload 1
invokevirtual android/view/KeyEvent/getKeyCode()I
iconst_4
if_icmpeq L4
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/findFocus()Landroid/view/View;
astore 7
aload 7
astore 1
aload 7
aload 0
if_acmpne L5
aconst_null
astore 1
L5:
invokestatic android/view/FocusFinder/getInstance()Landroid/view/FocusFinder;
aload 0
aload 1
sipush 130
invokevirtual android/view/FocusFinder/findNextFocus(Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;
astore 1
iload 6
istore 5
aload 1
ifnull L4
iload 6
istore 5
aload 1
aload 0
if_acmpeq L4
iload 6
istore 5
aload 1
sipush 130
invokevirtual android/view/View/requestFocus(I)Z
ifeq L4
iconst_1
istore 5
L4:
iload 5
ireturn
L1:
iconst_0
istore 2
goto L2
L0:
iconst_0
istore 2
goto L2
L3:
iload 6
istore 5
aload 1
invokevirtual android/view/KeyEvent/getAction()I
ifne L4
aload 1
invokevirtual android/view/KeyEvent/getKeyCode()I
lookupswitch
19 : L6
20 : L7
62 : L8
default : L9
L9:
iconst_0
ireturn
L6:
aload 1
invokevirtual android/view/KeyEvent/isAltPressed()Z
ifne L10
aload 0
bipush 33
invokespecial android/support/v4/widget/NestedScrollView/d(I)Z
ireturn
L10:
aload 0
bipush 33
invokespecial android/support/v4/widget/NestedScrollView/c(I)Z
ireturn
L7:
aload 1
invokevirtual android/view/KeyEvent/isAltPressed()Z
ifne L11
aload 0
sipush 130
invokespecial android/support/v4/widget/NestedScrollView/d(I)Z
ireturn
L11:
aload 0
sipush 130
invokespecial android/support/v4/widget/NestedScrollView/c(I)Z
ireturn
L8:
aload 1
invokevirtual android/view/KeyEvent/isShiftPressed()Z
ifeq L12
bipush 33
istore 2
L13:
iload 2
sipush 130
if_icmpne L14
iconst_1
istore 3
L15:
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getHeight()I
istore 4
iload 3
ifeq L16
aload 0
getfield android/support/v4/widget/NestedScrollView/e Landroid/graphics/Rect;
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getScrollY()I
iload 4
iadd
putfield android/graphics/Rect/top I
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getChildCount()I
istore 3
iload 3
ifle L17
aload 0
iload 3
iconst_1
isub
invokevirtual android/support/v4/widget/NestedScrollView/getChildAt(I)Landroid/view/View;
astore 1
aload 0
getfield android/support/v4/widget/NestedScrollView/e Landroid/graphics/Rect;
getfield android/graphics/Rect/top I
iload 4
iadd
aload 1
invokevirtual android/view/View/getBottom()I
if_icmple L17
aload 0
getfield android/support/v4/widget/NestedScrollView/e Landroid/graphics/Rect;
aload 1
invokevirtual android/view/View/getBottom()I
iload 4
isub
putfield android/graphics/Rect/top I
L17:
aload 0
getfield android/support/v4/widget/NestedScrollView/e Landroid/graphics/Rect;
iload 4
aload 0
getfield android/support/v4/widget/NestedScrollView/e Landroid/graphics/Rect;
getfield android/graphics/Rect/top I
iadd
putfield android/graphics/Rect/bottom I
aload 0
iload 2
aload 0
getfield android/support/v4/widget/NestedScrollView/e Landroid/graphics/Rect;
getfield android/graphics/Rect/top I
aload 0
getfield android/support/v4/widget/NestedScrollView/e Landroid/graphics/Rect;
getfield android/graphics/Rect/bottom I
invokespecial android/support/v4/widget/NestedScrollView/a(III)Z
pop
iconst_0
ireturn
L12:
sipush 130
istore 2
goto L13
L14:
iconst_0
istore 3
goto L15
L16:
aload 0
getfield android/support/v4/widget/NestedScrollView/e Landroid/graphics/Rect;
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getScrollY()I
iload 4
isub
putfield android/graphics/Rect/top I
aload 0
getfield android/support/v4/widget/NestedScrollView/e Landroid/graphics/Rect;
getfield android/graphics/Rect/top I
ifge L17
aload 0
getfield android/support/v4/widget/NestedScrollView/e Landroid/graphics/Rect;
iconst_0
putfield android/graphics/Rect/top I
goto L17
.limit locals 8
.limit stack 4
.end method

.method private a(Landroid/view/View;)Z
iconst_0
istore 2
aload 0
aload 1
iconst_0
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getHeight()I
invokespecial android/support/v4/widget/NestedScrollView/a(Landroid/view/View;II)Z
ifne L0
iconst_1
istore 2
L0:
iload 2
ireturn
.limit locals 3
.limit stack 4
.end method

.method private a(Landroid/view/View;II)Z
aload 1
aload 0
getfield android/support/v4/widget/NestedScrollView/e Landroid/graphics/Rect;
invokevirtual android/view/View/getDrawingRect(Landroid/graphics/Rect;)V
aload 0
aload 1
aload 0
getfield android/support/v4/widget/NestedScrollView/e Landroid/graphics/Rect;
invokevirtual android/support/v4/widget/NestedScrollView/offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V
aload 0
getfield android/support/v4/widget/NestedScrollView/e Landroid/graphics/Rect;
getfield android/graphics/Rect/bottom I
iload 2
iadd
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getScrollY()I
if_icmplt L0
aload 0
getfield android/support/v4/widget/NestedScrollView/e Landroid/graphics/Rect;
getfield android/graphics/Rect/top I
iload 2
isub
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getScrollY()I
iload 3
iadd
if_icmpgt L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 4
.limit stack 3
.end method

.method private static a(Landroid/view/View;Landroid/view/View;)Z
aload 0
aload 1
if_acmpne L0
iconst_1
ireturn
L0:
aload 0
invokevirtual android/view/View/getParent()Landroid/view/ViewParent;
astore 0
aload 0
instanceof android/view/ViewGroup
ifeq L1
aload 0
checkcast android/view/View
aload 1
invokestatic android/support/v4/widget/NestedScrollView/a(Landroid/view/View;Landroid/view/View;)Z
ifeq L1
iconst_1
ireturn
L1:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method private static b(III)I
iload 1
iload 2
if_icmpge L0
iload 0
ifge L1
L0:
iconst_0
istore 3
L2:
iload 3
ireturn
L1:
iload 0
istore 3
iload 1
iload 0
iadd
iload 2
if_icmple L2
iload 2
iload 1
isub
ireturn
.limit locals 4
.limit stack 2
.end method

.method private b(II)V
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getChildCount()I
ifne L0
return
L0:
invokestatic android/view/animation/AnimationUtils/currentAnimationTimeMillis()J
aload 0
getfield android/support/v4/widget/NestedScrollView/d J
lsub
ldc2_w 250L
lcmp
ifle L1
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getHeight()I
istore 1
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getPaddingBottom()I
istore 3
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getPaddingTop()I
istore 4
iconst_0
aload 0
iconst_0
invokevirtual android/support/v4/widget/NestedScrollView/getChildAt(I)Landroid/view/View;
invokevirtual android/view/View/getHeight()I
iload 1
iload 3
isub
iload 4
isub
isub
invokestatic java/lang/Math/max(II)I
istore 3
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getScrollY()I
istore 1
iconst_0
iload 1
iload 2
iadd
iload 3
invokestatic java/lang/Math/min(II)I
invokestatic java/lang/Math/max(II)I
istore 2
aload 0
getfield android/support/v4/widget/NestedScrollView/f Landroid/support/v4/widget/ca;
astore 5
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getScrollX()I
istore 3
aload 5
getfield android/support/v4/widget/ca/b Landroid/support/v4/widget/cb;
aload 5
getfield android/support/v4/widget/ca/a Ljava/lang/Object;
iload 3
iload 1
iload 2
iload 1
isub
invokeinterface android/support/v4/widget/cb/a(Ljava/lang/Object;III)V 4
aload 0
invokestatic android/support/v4/view/cx/b(Landroid/view/View;)V
L2:
aload 0
invokestatic android/view/animation/AnimationUtils/currentAnimationTimeMillis()J
putfield android/support/v4/widget/NestedScrollView/d J
return
L1:
aload 0
getfield android/support/v4/widget/NestedScrollView/f Landroid/support/v4/widget/ca;
invokevirtual android/support/v4/widget/ca/a()Z
ifne L3
aload 0
getfield android/support/v4/widget/NestedScrollView/f Landroid/support/v4/widget/ca;
invokevirtual android/support/v4/widget/ca/g()V
L3:
aload 0
iload 1
iload 2
invokevirtual android/support/v4/widget/NestedScrollView/scrollBy(II)V
goto L2
.limit locals 6
.limit stack 6
.end method

.method private b(Landroid/view/View;)V
aload 1
aload 0
getfield android/support/v4/widget/NestedScrollView/e Landroid/graphics/Rect;
invokevirtual android/view/View/getDrawingRect(Landroid/graphics/Rect;)V
aload 0
aload 1
aload 0
getfield android/support/v4/widget/NestedScrollView/e Landroid/graphics/Rect;
invokevirtual android/support/v4/widget/NestedScrollView/offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V
aload 0
aload 0
getfield android/support/v4/widget/NestedScrollView/e Landroid/graphics/Rect;
invokespecial android/support/v4/widget/NestedScrollView/a(Landroid/graphics/Rect;)I
istore 2
iload 2
ifeq L0
aload 0
iconst_0
iload 2
invokevirtual android/support/v4/widget/NestedScrollView/scrollBy(II)V
L0:
return
.limit locals 3
.limit stack 3
.end method

.method private b()Z
iconst_0
istore 3
aload 0
iconst_0
invokevirtual android/support/v4/widget/NestedScrollView/getChildAt(I)Landroid/view/View;
astore 4
iload 3
istore 2
aload 4
ifnull L0
aload 4
invokevirtual android/view/View/getHeight()I
istore 1
iload 3
istore 2
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getHeight()I
iload 1
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getPaddingTop()I
iadd
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getPaddingBottom()I
iadd
if_icmpge L0
iconst_1
istore 2
L0:
iload 2
ireturn
.limit locals 5
.limit stack 3
.end method

.method private b(I)Z
iload 1
sipush 130
if_icmpne L0
iconst_1
istore 2
L1:
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getHeight()I
istore 3
iload 2
ifeq L2
aload 0
getfield android/support/v4/widget/NestedScrollView/e Landroid/graphics/Rect;
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getScrollY()I
iload 3
iadd
putfield android/graphics/Rect/top I
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getChildCount()I
istore 2
iload 2
ifle L3
aload 0
iload 2
iconst_1
isub
invokevirtual android/support/v4/widget/NestedScrollView/getChildAt(I)Landroid/view/View;
astore 4
aload 0
getfield android/support/v4/widget/NestedScrollView/e Landroid/graphics/Rect;
getfield android/graphics/Rect/top I
iload 3
iadd
aload 4
invokevirtual android/view/View/getBottom()I
if_icmple L3
aload 0
getfield android/support/v4/widget/NestedScrollView/e Landroid/graphics/Rect;
aload 4
invokevirtual android/view/View/getBottom()I
iload 3
isub
putfield android/graphics/Rect/top I
L3:
aload 0
getfield android/support/v4/widget/NestedScrollView/e Landroid/graphics/Rect;
aload 0
getfield android/support/v4/widget/NestedScrollView/e Landroid/graphics/Rect;
getfield android/graphics/Rect/top I
iload 3
iadd
putfield android/graphics/Rect/bottom I
aload 0
iload 1
aload 0
getfield android/support/v4/widget/NestedScrollView/e Landroid/graphics/Rect;
getfield android/graphics/Rect/top I
aload 0
getfield android/support/v4/widget/NestedScrollView/e Landroid/graphics/Rect;
getfield android/graphics/Rect/bottom I
invokespecial android/support/v4/widget/NestedScrollView/a(III)Z
ireturn
L0:
iconst_0
istore 2
goto L1
L2:
aload 0
getfield android/support/v4/widget/NestedScrollView/e Landroid/graphics/Rect;
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getScrollY()I
iload 3
isub
putfield android/graphics/Rect/top I
aload 0
getfield android/support/v4/widget/NestedScrollView/e Landroid/graphics/Rect;
getfield android/graphics/Rect/top I
ifge L3
aload 0
getfield android/support/v4/widget/NestedScrollView/e Landroid/graphics/Rect;
iconst_0
putfield android/graphics/Rect/top I
goto L3
.limit locals 5
.limit stack 4
.end method

.method private c()Z
aload 0
getfield android/support/v4/widget/NestedScrollView/o Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private c(I)Z
iload 1
sipush 130
if_icmpne L0
iconst_1
istore 2
L1:
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getHeight()I
istore 3
aload 0
getfield android/support/v4/widget/NestedScrollView/e Landroid/graphics/Rect;
iconst_0
putfield android/graphics/Rect/top I
aload 0
getfield android/support/v4/widget/NestedScrollView/e Landroid/graphics/Rect;
iload 3
putfield android/graphics/Rect/bottom I
iload 2
ifeq L2
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getChildCount()I
istore 2
iload 2
ifle L2
aload 0
iload 2
iconst_1
isub
invokevirtual android/support/v4/widget/NestedScrollView/getChildAt(I)Landroid/view/View;
astore 4
aload 0
getfield android/support/v4/widget/NestedScrollView/e Landroid/graphics/Rect;
aload 4
invokevirtual android/view/View/getBottom()I
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getPaddingBottom()I
iadd
putfield android/graphics/Rect/bottom I
aload 0
getfield android/support/v4/widget/NestedScrollView/e Landroid/graphics/Rect;
aload 0
getfield android/support/v4/widget/NestedScrollView/e Landroid/graphics/Rect;
getfield android/graphics/Rect/bottom I
iload 3
isub
putfield android/graphics/Rect/top I
L2:
aload 0
iload 1
aload 0
getfield android/support/v4/widget/NestedScrollView/e Landroid/graphics/Rect;
getfield android/graphics/Rect/top I
aload 0
getfield android/support/v4/widget/NestedScrollView/e Landroid/graphics/Rect;
getfield android/graphics/Rect/bottom I
invokespecial android/support/v4/widget/NestedScrollView/a(III)Z
ireturn
L0:
iconst_0
istore 2
goto L1
.limit locals 5
.limit stack 4
.end method

.method private d()Z
aload 0
getfield android/support/v4/widget/NestedScrollView/p Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private d(I)Z
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/findFocus()Landroid/view/View;
astore 7
aload 7
astore 6
aload 7
aload 0
if_acmpne L0
aconst_null
astore 6
L0:
invokestatic android/view/FocusFinder/getInstance()Landroid/view/FocusFinder;
aload 0
aload 6
iload 1
invokevirtual android/view/FocusFinder/findNextFocus(Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;
astore 7
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getMaxScrollAmount()I
istore 3
aload 7
ifnull L1
aload 0
aload 7
iload 3
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getHeight()I
invokespecial android/support/v4/widget/NestedScrollView/a(Landroid/view/View;II)Z
ifeq L1
aload 7
aload 0
getfield android/support/v4/widget/NestedScrollView/e Landroid/graphics/Rect;
invokevirtual android/view/View/getDrawingRect(Landroid/graphics/Rect;)V
aload 0
aload 7
aload 0
getfield android/support/v4/widget/NestedScrollView/e Landroid/graphics/Rect;
invokevirtual android/support/v4/widget/NestedScrollView/offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V
aload 0
aload 0
aload 0
getfield android/support/v4/widget/NestedScrollView/e Landroid/graphics/Rect;
invokespecial android/support/v4/widget/NestedScrollView/a(Landroid/graphics/Rect;)I
invokespecial android/support/v4/widget/NestedScrollView/e(I)V
aload 7
iload 1
invokevirtual android/view/View/requestFocus(I)Z
pop
L2:
aload 6
ifnull L3
aload 6
invokevirtual android/view/View/isFocused()Z
ifeq L3
aload 0
aload 6
invokespecial android/support/v4/widget/NestedScrollView/a(Landroid/view/View;)Z
ifeq L3
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getDescendantFocusability()I
istore 1
aload 0
ldc_w 131072
invokevirtual android/support/v4/widget/NestedScrollView/setDescendantFocusability(I)V
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/requestFocus()Z
pop
aload 0
iload 1
invokevirtual android/support/v4/widget/NestedScrollView/setDescendantFocusability(I)V
L3:
iconst_1
ireturn
L1:
iload 1
bipush 33
if_icmpne L4
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getScrollY()I
iload 3
if_icmpge L4
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getScrollY()I
istore 2
L5:
iload 2
ifne L6
iconst_0
ireturn
L4:
iload 3
istore 2
iload 1
sipush 130
if_icmpne L5
iload 3
istore 2
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getChildCount()I
ifle L5
aload 0
iconst_0
invokevirtual android/support/v4/widget/NestedScrollView/getChildAt(I)Landroid/view/View;
invokevirtual android/view/View/getBottom()I
istore 4
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getScrollY()I
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getHeight()I
iadd
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getPaddingBottom()I
isub
istore 5
iload 3
istore 2
iload 4
iload 5
isub
iload 3
if_icmpge L5
iload 4
iload 5
isub
istore 2
goto L5
L6:
iload 1
sipush 130
if_icmpne L7
L8:
aload 0
iload 2
invokespecial android/support/v4/widget/NestedScrollView/e(I)V
goto L2
L7:
iload 2
ineg
istore 2
goto L8
.limit locals 8
.limit stack 4
.end method

.method private e()V
aload 0
getfield android/support/v4/widget/NestedScrollView/n Landroid/view/VelocityTracker;
ifnonnull L0
aload 0
invokestatic android/view/VelocityTracker/obtain()Landroid/view/VelocityTracker;
putfield android/support/v4/widget/NestedScrollView/n Landroid/view/VelocityTracker;
return
L0:
aload 0
getfield android/support/v4/widget/NestedScrollView/n Landroid/view/VelocityTracker;
invokevirtual android/view/VelocityTracker/clear()V
return
.limit locals 1
.limit stack 2
.end method

.method private e(I)V
iload 1
ifeq L0
aload 0
getfield android/support/v4/widget/NestedScrollView/p Z
ifeq L1
aload 0
iconst_0
iload 1
invokespecial android/support/v4/widget/NestedScrollView/b(II)V
L0:
return
L1:
aload 0
iconst_0
iload 1
invokevirtual android/support/v4/widget/NestedScrollView/scrollBy(II)V
return
.limit locals 2
.limit stack 3
.end method

.method private f()V
aload 0
getfield android/support/v4/widget/NestedScrollView/n Landroid/view/VelocityTracker;
ifnonnull L0
aload 0
invokestatic android/view/VelocityTracker/obtain()Landroid/view/VelocityTracker;
putfield android/support/v4/widget/NestedScrollView/n Landroid/view/VelocityTracker;
L0:
return
.limit locals 1
.limit stack 2
.end method

.method private f(I)V
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getChildCount()I
ifle L0
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getHeight()I
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getPaddingBottom()I
isub
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getPaddingTop()I
isub
istore 4
aload 0
iconst_0
invokevirtual android/support/v4/widget/NestedScrollView/getChildAt(I)Landroid/view/View;
invokevirtual android/view/View/getHeight()I
istore 5
aload 0
getfield android/support/v4/widget/NestedScrollView/f Landroid/support/v4/widget/ca;
astore 6
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getScrollX()I
istore 2
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getScrollY()I
istore 3
iconst_0
iload 5
iload 4
isub
invokestatic java/lang/Math/max(II)I
istore 5
iload 4
iconst_2
idiv
istore 4
aload 6
getfield android/support/v4/widget/ca/b Landroid/support/v4/widget/cb;
aload 6
getfield android/support/v4/widget/ca/a Ljava/lang/Object;
iload 2
iload 3
iload 1
iload 5
iload 4
invokeinterface android/support/v4/widget/cb/b(Ljava/lang/Object;IIIII)V 6
aload 0
invokestatic android/support/v4/view/cx/b(Landroid/view/View;)V
L0:
return
.limit locals 7
.limit stack 7
.end method

.method private g()V
aload 0
getfield android/support/v4/widget/NestedScrollView/n Landroid/view/VelocityTracker;
ifnull L0
aload 0
getfield android/support/v4/widget/NestedScrollView/n Landroid/view/VelocityTracker;
invokevirtual android/view/VelocityTracker/recycle()V
aload 0
aconst_null
putfield android/support/v4/widget/NestedScrollView/n Landroid/view/VelocityTracker;
L0:
return
.limit locals 1
.limit stack 2
.end method

.method private g(I)V
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getScrollY()I
istore 2
iload 2
ifgt L0
iload 1
ifle L1
L0:
iload 2
aload 0
invokespecial android/support/v4/widget/NestedScrollView/getScrollRange()I
if_icmplt L2
iload 1
ifge L1
L2:
iconst_1
istore 6
L3:
aload 0
fconst_0
iload 1
i2f
invokevirtual android/support/v4/widget/NestedScrollView/dispatchNestedPreFling(FF)Z
ifne L4
aload 0
fconst_0
iload 1
i2f
iload 6
invokevirtual android/support/v4/widget/NestedScrollView/dispatchNestedFling(FFZ)Z
pop
iload 6
ifeq L4
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getChildCount()I
ifle L4
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getHeight()I
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getPaddingBottom()I
isub
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getPaddingTop()I
isub
istore 4
aload 0
iconst_0
invokevirtual android/support/v4/widget/NestedScrollView/getChildAt(I)Landroid/view/View;
invokevirtual android/view/View/getHeight()I
istore 5
aload 0
getfield android/support/v4/widget/NestedScrollView/f Landroid/support/v4/widget/ca;
astore 7
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getScrollX()I
istore 2
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getScrollY()I
istore 3
iconst_0
iload 5
iload 4
isub
invokestatic java/lang/Math/max(II)I
istore 5
iload 4
iconst_2
idiv
istore 4
aload 7
getfield android/support/v4/widget/ca/b Landroid/support/v4/widget/cb;
aload 7
getfield android/support/v4/widget/ca/a Ljava/lang/Object;
iload 2
iload 3
iload 1
iload 5
iload 4
invokeinterface android/support/v4/widget/cb/b(Ljava/lang/Object;IIIII)V 6
aload 0
invokestatic android/support/v4/view/cx/b(Landroid/view/View;)V
L4:
return
L1:
iconst_0
istore 6
goto L3
.limit locals 8
.limit stack 7
.end method

.method private getScrollRange()I
iconst_0
istore 1
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getChildCount()I
ifle L0
iconst_0
aload 0
iconst_0
invokevirtual android/support/v4/widget/NestedScrollView/getChildAt(I)Landroid/view/View;
invokevirtual android/view/View/getHeight()I
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getHeight()I
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getPaddingBottom()I
isub
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getPaddingTop()I
isub
isub
invokestatic java/lang/Math/max(II)I
istore 1
L0:
iload 1
ireturn
.limit locals 2
.limit stack 4
.end method

.method private getVerticalScrollFactorCompat()F
aload 0
getfield android/support/v4/widget/NestedScrollView/D F
fconst_0
fcmpl
ifne L0
new android/util/TypedValue
dup
invokespecial android/util/TypedValue/<init>()V
astore 1
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getContext()Landroid/content/Context;
astore 2
aload 2
invokevirtual android/content/Context/getTheme()Landroid/content/res/Resources$Theme;
ldc_w 16842829
aload 1
iconst_1
invokevirtual android/content/res/Resources$Theme/resolveAttribute(ILandroid/util/TypedValue;Z)Z
ifne L1
new java/lang/IllegalStateException
dup
ldc "Expected theme to define listPreferredItemHeight."
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L1:
aload 0
aload 1
aload 2
invokevirtual android/content/Context/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
invokevirtual android/util/TypedValue/getDimension(Landroid/util/DisplayMetrics;)F
putfield android/support/v4/widget/NestedScrollView/D F
L0:
aload 0
getfield android/support/v4/widget/NestedScrollView/D F
freturn
.limit locals 3
.limit stack 4
.end method

.method private h()V
aload 0
iconst_0
putfield android/support/v4/widget/NestedScrollView/m Z
aload 0
invokespecial android/support/v4/widget/NestedScrollView/g()V
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/stopNestedScroll()V
aload 0
getfield android/support/v4/widget/NestedScrollView/g Landroid/support/v4/widget/al;
ifnull L0
aload 0
getfield android/support/v4/widget/NestedScrollView/g Landroid/support/v4/widget/al;
invokevirtual android/support/v4/widget/al/c()Z
pop
aload 0
getfield android/support/v4/widget/NestedScrollView/h Landroid/support/v4/widget/al;
invokevirtual android/support/v4/widget/al/c()Z
pop
L0:
return
.limit locals 1
.limit stack 2
.end method

.method private i()V
aload 0
invokestatic android/support/v4/view/cx/a(Landroid/view/View;)I
iconst_2
if_icmpeq L0
aload 0
getfield android/support/v4/widget/NestedScrollView/g Landroid/support/v4/widget/al;
ifnonnull L1
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getContext()Landroid/content/Context;
astore 1
aload 0
new android/support/v4/widget/al
dup
aload 1
invokespecial android/support/v4/widget/al/<init>(Landroid/content/Context;)V
putfield android/support/v4/widget/NestedScrollView/g Landroid/support/v4/widget/al;
aload 0
new android/support/v4/widget/al
dup
aload 1
invokespecial android/support/v4/widget/al/<init>(Landroid/content/Context;)V
putfield android/support/v4/widget/NestedScrollView/h Landroid/support/v4/widget/al;
L1:
return
L0:
aload 0
aconst_null
putfield android/support/v4/widget/NestedScrollView/g Landroid/support/v4/widget/al;
aload 0
aconst_null
putfield android/support/v4/widget/NestedScrollView/h Landroid/support/v4/widget/al;
return
.limit locals 2
.limit stack 4
.end method

.method public final a(I)V
aload 0
iconst_0
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getScrollX()I
isub
iload 1
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getScrollY()I
isub
invokespecial android/support/v4/widget/NestedScrollView/b(II)V
return
.limit locals 2
.limit stack 4
.end method

.method public final addView(Landroid/view/View;)V
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getChildCount()I
ifle L0
new java/lang/IllegalStateException
dup
ldc "ScrollView can host only one direct child"
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
aload 1
invokespecial android/widget/FrameLayout/addView(Landroid/view/View;)V
return
.limit locals 2
.limit stack 3
.end method

.method public final addView(Landroid/view/View;I)V
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getChildCount()I
ifle L0
new java/lang/IllegalStateException
dup
ldc "ScrollView can host only one direct child"
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
aload 1
iload 2
invokespecial android/widget/FrameLayout/addView(Landroid/view/View;I)V
return
.limit locals 3
.limit stack 3
.end method

.method public final addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getChildCount()I
ifle L0
new java/lang/IllegalStateException
dup
ldc "ScrollView can host only one direct child"
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
aload 1
iload 2
aload 3
invokespecial android/widget/FrameLayout/addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
return
.limit locals 4
.limit stack 4
.end method

.method public final addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getChildCount()I
ifle L0
new java/lang/IllegalStateException
dup
ldc "ScrollView can host only one direct child"
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
aload 1
aload 2
invokespecial android/widget/FrameLayout/addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
return
.limit locals 3
.limit stack 3
.end method

.method public final computeScroll()V
aload 0
getfield android/support/v4/widget/NestedScrollView/f Landroid/support/v4/widget/ca;
invokevirtual android/support/v4/widget/ca/f()Z
ifeq L0
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getScrollX()I
istore 2
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getScrollY()I
istore 3
aload 0
getfield android/support/v4/widget/NestedScrollView/f Landroid/support/v4/widget/ca;
invokevirtual android/support/v4/widget/ca/b()I
istore 4
aload 0
getfield android/support/v4/widget/NestedScrollView/f Landroid/support/v4/widget/ca;
invokevirtual android/support/v4/widget/ca/c()I
istore 5
iload 2
iload 4
if_icmpne L1
iload 3
iload 5
if_icmpeq L0
L1:
aload 0
invokespecial android/support/v4/widget/NestedScrollView/getScrollRange()I
istore 6
aload 0
invokestatic android/support/v4/view/cx/a(Landroid/view/View;)I
istore 1
iload 1
ifeq L2
iload 1
iconst_1
if_icmpne L3
iload 6
ifle L3
L2:
iconst_1
istore 1
L4:
aload 0
iload 4
iload 2
isub
iload 5
iload 3
isub
iload 2
iload 3
iload 6
invokespecial android/support/v4/widget/NestedScrollView/a(IIIII)Z
pop
iload 1
ifeq L0
aload 0
invokespecial android/support/v4/widget/NestedScrollView/i()V
iload 5
ifgt L5
iload 3
ifle L5
aload 0
getfield android/support/v4/widget/NestedScrollView/g Landroid/support/v4/widget/al;
aload 0
getfield android/support/v4/widget/NestedScrollView/f Landroid/support/v4/widget/ca;
invokevirtual android/support/v4/widget/ca/e()F
f2i
invokevirtual android/support/v4/widget/al/a(I)Z
pop
L0:
return
L3:
iconst_0
istore 1
goto L4
L5:
iload 5
iload 6
if_icmplt L0
iload 3
iload 6
if_icmpge L0
aload 0
getfield android/support/v4/widget/NestedScrollView/h Landroid/support/v4/widget/al;
aload 0
getfield android/support/v4/widget/NestedScrollView/f Landroid/support/v4/widget/ca;
invokevirtual android/support/v4/widget/ca/e()F
f2i
invokevirtual android/support/v4/widget/al/a(I)Z
pop
return
.limit locals 7
.limit stack 6
.end method

.method protected final computeVerticalScrollOffset()I
iconst_0
aload 0
invokespecial android/widget/FrameLayout/computeVerticalScrollOffset()I
invokestatic java/lang/Math/max(II)I
ireturn
.limit locals 1
.limit stack 2
.end method

.method protected final computeVerticalScrollRange()I
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getChildCount()I
istore 2
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getHeight()I
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getPaddingBottom()I
isub
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getPaddingTop()I
isub
istore 1
iload 2
ifne L0
L1:
iload 1
ireturn
L0:
aload 0
iconst_0
invokevirtual android/support/v4/widget/NestedScrollView/getChildAt(I)Landroid/view/View;
invokevirtual android/view/View/getBottom()I
istore 2
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getScrollY()I
istore 3
iconst_0
iload 2
iload 1
isub
invokestatic java/lang/Math/max(II)I
istore 4
iload 3
ifge L2
iload 2
iload 3
isub
ireturn
L2:
iload 2
istore 1
iload 3
iload 4
if_icmple L1
iload 2
iload 3
iload 4
isub
iadd
ireturn
.limit locals 5
.limit stack 3
.end method

.method public final dispatchKeyEvent(Landroid/view/KeyEvent;)Z
iconst_0
istore 6
aload 0
aload 1
invokespecial android/widget/FrameLayout/dispatchKeyEvent(Landroid/view/KeyEvent;)Z
ifne L0
aload 0
getfield android/support/v4/widget/NestedScrollView/e Landroid/graphics/Rect;
invokevirtual android/graphics/Rect/setEmpty()V
aload 0
iconst_0
invokevirtual android/support/v4/widget/NestedScrollView/getChildAt(I)Landroid/view/View;
astore 7
aload 7
ifnull L1
aload 7
invokevirtual android/view/View/getHeight()I
istore 2
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getHeight()I
iload 2
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getPaddingTop()I
iadd
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getPaddingBottom()I
iadd
if_icmpge L2
iconst_1
istore 2
L3:
iload 2
ifne L4
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/isFocused()Z
ifeq L5
aload 1
invokevirtual android/view/KeyEvent/getKeyCode()I
iconst_4
if_icmpeq L5
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/findFocus()Landroid/view/View;
astore 7
aload 7
astore 1
aload 7
aload 0
if_acmpne L6
aconst_null
astore 1
L6:
invokestatic android/view/FocusFinder/getInstance()Landroid/view/FocusFinder;
aload 0
aload 1
sipush 130
invokevirtual android/view/FocusFinder/findNextFocus(Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;
astore 1
aload 1
ifnull L7
aload 1
aload 0
if_acmpeq L7
aload 1
sipush 130
invokevirtual android/view/View/requestFocus(I)Z
ifeq L7
iconst_1
istore 5
L8:
iload 5
ifeq L9
L0:
iconst_1
istore 6
L9:
iload 6
ireturn
L2:
iconst_0
istore 2
goto L3
L1:
iconst_0
istore 2
goto L3
L7:
iconst_0
istore 5
goto L8
L5:
iconst_0
istore 5
goto L8
L4:
aload 1
invokevirtual android/view/KeyEvent/getAction()I
ifne L10
aload 1
invokevirtual android/view/KeyEvent/getKeyCode()I
lookupswitch
19 : L11
20 : L12
62 : L13
default : L10
L10:
iconst_0
istore 5
goto L8
L11:
aload 1
invokevirtual android/view/KeyEvent/isAltPressed()Z
ifne L14
aload 0
bipush 33
invokespecial android/support/v4/widget/NestedScrollView/d(I)Z
istore 5
goto L8
L14:
aload 0
bipush 33
invokespecial android/support/v4/widget/NestedScrollView/c(I)Z
istore 5
goto L8
L12:
aload 1
invokevirtual android/view/KeyEvent/isAltPressed()Z
ifne L15
aload 0
sipush 130
invokespecial android/support/v4/widget/NestedScrollView/d(I)Z
istore 5
goto L8
L15:
aload 0
sipush 130
invokespecial android/support/v4/widget/NestedScrollView/c(I)Z
istore 5
goto L8
L13:
aload 1
invokevirtual android/view/KeyEvent/isShiftPressed()Z
ifeq L16
bipush 33
istore 2
L17:
iload 2
sipush 130
if_icmpne L18
iconst_1
istore 3
L19:
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getHeight()I
istore 4
iload 3
ifeq L20
aload 0
getfield android/support/v4/widget/NestedScrollView/e Landroid/graphics/Rect;
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getScrollY()I
iload 4
iadd
putfield android/graphics/Rect/top I
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getChildCount()I
istore 3
iload 3
ifle L21
aload 0
iload 3
iconst_1
isub
invokevirtual android/support/v4/widget/NestedScrollView/getChildAt(I)Landroid/view/View;
astore 1
aload 0
getfield android/support/v4/widget/NestedScrollView/e Landroid/graphics/Rect;
getfield android/graphics/Rect/top I
iload 4
iadd
aload 1
invokevirtual android/view/View/getBottom()I
if_icmple L21
aload 0
getfield android/support/v4/widget/NestedScrollView/e Landroid/graphics/Rect;
aload 1
invokevirtual android/view/View/getBottom()I
iload 4
isub
putfield android/graphics/Rect/top I
L21:
aload 0
getfield android/support/v4/widget/NestedScrollView/e Landroid/graphics/Rect;
iload 4
aload 0
getfield android/support/v4/widget/NestedScrollView/e Landroid/graphics/Rect;
getfield android/graphics/Rect/top I
iadd
putfield android/graphics/Rect/bottom I
aload 0
iload 2
aload 0
getfield android/support/v4/widget/NestedScrollView/e Landroid/graphics/Rect;
getfield android/graphics/Rect/top I
aload 0
getfield android/support/v4/widget/NestedScrollView/e Landroid/graphics/Rect;
getfield android/graphics/Rect/bottom I
invokespecial android/support/v4/widget/NestedScrollView/a(III)Z
pop
goto L10
L16:
sipush 130
istore 2
goto L17
L18:
iconst_0
istore 3
goto L19
L20:
aload 0
getfield android/support/v4/widget/NestedScrollView/e Landroid/graphics/Rect;
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getScrollY()I
iload 4
isub
putfield android/graphics/Rect/top I
aload 0
getfield android/support/v4/widget/NestedScrollView/e Landroid/graphics/Rect;
getfield android/graphics/Rect/top I
ifge L21
aload 0
getfield android/support/v4/widget/NestedScrollView/e Landroid/graphics/Rect;
iconst_0
putfield android/graphics/Rect/top I
goto L21
.limit locals 8
.limit stack 4
.end method

.method public final dispatchNestedFling(FFZ)Z
aload 0
getfield android/support/v4/widget/NestedScrollView/C Landroid/support/v4/view/bu;
fload 1
fload 2
iload 3
invokevirtual android/support/v4/view/bu/a(FFZ)Z
ireturn
.limit locals 4
.limit stack 4
.end method

.method public final dispatchNestedPreFling(FF)Z
aload 0
getfield android/support/v4/widget/NestedScrollView/C Landroid/support/v4/view/bu;
fload 1
fload 2
invokevirtual android/support/v4/view/bu/a(FF)Z
ireturn
.limit locals 3
.limit stack 3
.end method

.method public final dispatchNestedPreScroll(II[I[I)Z
aload 0
getfield android/support/v4/widget/NestedScrollView/C Landroid/support/v4/view/bu;
iload 1
iload 2
aload 3
aload 4
invokevirtual android/support/v4/view/bu/a(II[I[I)Z
ireturn
.limit locals 5
.limit stack 5
.end method

.method public final dispatchNestedScroll(IIII[I)Z
aload 0
getfield android/support/v4/widget/NestedScrollView/C Landroid/support/v4/view/bu;
iload 1
iload 2
iload 3
iload 4
aload 5
invokevirtual android/support/v4/view/bu/a(IIII[I)Z
ireturn
.limit locals 6
.limit stack 6
.end method

.method public final draw(Landroid/graphics/Canvas;)V
aload 0
aload 1
invokespecial android/widget/FrameLayout/draw(Landroid/graphics/Canvas;)V
aload 0
getfield android/support/v4/widget/NestedScrollView/g Landroid/support/v4/widget/al;
ifnull L0
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getScrollY()I
istore 2
aload 0
getfield android/support/v4/widget/NestedScrollView/g Landroid/support/v4/widget/al;
invokevirtual android/support/v4/widget/al/a()Z
ifne L1
aload 1
invokevirtual android/graphics/Canvas/save()I
istore 3
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getWidth()I
istore 4
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getPaddingLeft()I
istore 5
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getPaddingRight()I
istore 6
aload 1
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getPaddingLeft()I
i2f
iconst_0
iload 2
invokestatic java/lang/Math/min(II)I
i2f
invokevirtual android/graphics/Canvas/translate(FF)V
aload 0
getfield android/support/v4/widget/NestedScrollView/g Landroid/support/v4/widget/al;
iload 4
iload 5
isub
iload 6
isub
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getHeight()I
invokevirtual android/support/v4/widget/al/a(II)V
aload 0
getfield android/support/v4/widget/NestedScrollView/g Landroid/support/v4/widget/al;
aload 1
invokevirtual android/support/v4/widget/al/a(Landroid/graphics/Canvas;)Z
ifeq L2
aload 0
invokestatic android/support/v4/view/cx/b(Landroid/view/View;)V
L2:
aload 1
iload 3
invokevirtual android/graphics/Canvas/restoreToCount(I)V
L1:
aload 0
getfield android/support/v4/widget/NestedScrollView/h Landroid/support/v4/widget/al;
invokevirtual android/support/v4/widget/al/a()Z
ifne L0
aload 1
invokevirtual android/graphics/Canvas/save()I
istore 3
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getWidth()I
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getPaddingLeft()I
isub
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getPaddingRight()I
isub
istore 4
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getHeight()I
istore 5
aload 1
iload 4
ineg
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getPaddingLeft()I
iadd
i2f
aload 0
invokespecial android/support/v4/widget/NestedScrollView/getScrollRange()I
iload 2
invokestatic java/lang/Math/max(II)I
iload 5
iadd
i2f
invokevirtual android/graphics/Canvas/translate(FF)V
aload 1
ldc_w 180.0F
iload 4
i2f
fconst_0
invokevirtual android/graphics/Canvas/rotate(FFF)V
aload 0
getfield android/support/v4/widget/NestedScrollView/h Landroid/support/v4/widget/al;
iload 4
iload 5
invokevirtual android/support/v4/widget/al/a(II)V
aload 0
getfield android/support/v4/widget/NestedScrollView/h Landroid/support/v4/widget/al;
aload 1
invokevirtual android/support/v4/widget/al/a(Landroid/graphics/Canvas;)Z
ifeq L3
aload 0
invokestatic android/support/v4/view/cx/b(Landroid/view/View;)V
L3:
aload 1
iload 3
invokevirtual android/graphics/Canvas/restoreToCount(I)V
L0:
return
.limit locals 7
.limit stack 4
.end method

.method protected final getBottomFadingEdgeStrength()F
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getChildCount()I
ifne L0
fconst_0
freturn
L0:
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getVerticalFadingEdgeLength()I
istore 1
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getHeight()I
istore 2
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getPaddingBottom()I
istore 3
aload 0
iconst_0
invokevirtual android/support/v4/widget/NestedScrollView/getChildAt(I)Landroid/view/View;
invokevirtual android/view/View/getBottom()I
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getScrollY()I
isub
iload 2
iload 3
isub
isub
istore 2
iload 2
iload 1
if_icmpge L1
iload 2
i2f
iload 1
i2f
fdiv
freturn
L1:
fconst_1
freturn
.limit locals 4
.limit stack 3
.end method

.method public final getMaxScrollAmount()I
ldc_w 0.5F
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getHeight()I
i2f
fmul
f2i
ireturn
.limit locals 1
.limit stack 2
.end method

.method public final getNestedScrollAxes()I
aload 0
getfield android/support/v4/widget/NestedScrollView/B Landroid/support/v4/view/bw;
getfield android/support/v4/view/bw/a I
ireturn
.limit locals 1
.limit stack 1
.end method

.method protected final getTopFadingEdgeStrength()F
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getChildCount()I
ifne L0
fconst_0
freturn
L0:
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getVerticalFadingEdgeLength()I
istore 1
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getScrollY()I
istore 2
iload 2
iload 1
if_icmpge L1
iload 2
i2f
iload 1
i2f
fdiv
freturn
L1:
fconst_1
freturn
.limit locals 3
.limit stack 2
.end method

.method public final hasNestedScrollingParent()Z
aload 0
getfield android/support/v4/widget/NestedScrollView/C Landroid/support/v4/view/bu;
invokevirtual android/support/v4/view/bu/a()Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final isNestedScrollingEnabled()Z
aload 0
getfield android/support/v4/widget/NestedScrollView/C Landroid/support/v4/view/bu;
getfield android/support/v4/view/bu/a Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method protected final measureChild(Landroid/view/View;II)V
aload 1
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
astore 4
aload 1
iload 2
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getPaddingLeft()I
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getPaddingRight()I
iadd
aload 4
getfield android/view/ViewGroup$LayoutParams/width I
invokestatic android/support/v4/widget/NestedScrollView/getChildMeasureSpec(III)I
iconst_0
iconst_0
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
invokevirtual android/view/View/measure(II)V
return
.limit locals 5
.limit stack 4
.end method

.method protected final measureChildWithMargins(Landroid/view/View;IIII)V
aload 1
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/view/ViewGroup$MarginLayoutParams
astore 6
iload 2
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getPaddingLeft()I
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getPaddingRight()I
iadd
aload 6
getfield android/view/ViewGroup$MarginLayoutParams/leftMargin I
iadd
aload 6
getfield android/view/ViewGroup$MarginLayoutParams/rightMargin I
iadd
iload 3
iadd
aload 6
getfield android/view/ViewGroup$MarginLayoutParams/width I
invokestatic android/support/v4/widget/NestedScrollView/getChildMeasureSpec(III)I
istore 2
aload 6
getfield android/view/ViewGroup$MarginLayoutParams/topMargin I
istore 3
aload 1
iload 2
aload 6
getfield android/view/ViewGroup$MarginLayoutParams/bottomMargin I
iload 3
iadd
iconst_0
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
invokevirtual android/view/View/measure(II)V
return
.limit locals 7
.limit stack 4
.end method

.method public final onAttachedToWindow()V
aload 0
iconst_0
putfield android/support/v4/widget/NestedScrollView/k Z
return
.limit locals 1
.limit stack 2
.end method

.method public final onGenericMotionEvent(Landroid/view/MotionEvent;)Z
aload 1
invokestatic android/support/v4/view/bk/d(Landroid/view/MotionEvent;)I
iconst_2
iand
ifeq L0
aload 1
invokevirtual android/view/MotionEvent/getAction()I
tableswitch 8
L1
default : L0
L0:
iconst_0
ireturn
L1:
aload 0
getfield android/support/v4/widget/NestedScrollView/m Z
ifne L0
aload 1
invokestatic android/support/v4/view/bk/e(Landroid/view/MotionEvent;)F
fstore 2
fload 2
fconst_0
fcmpl
ifeq L0
fload 2
aload 0
invokespecial android/support/v4/widget/NestedScrollView/getVerticalScrollFactorCompat()F
fmul
f2i
istore 3
aload 0
invokespecial android/support/v4/widget/NestedScrollView/getScrollRange()I
istore 4
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getScrollY()I
istore 6
iload 6
iload 3
isub
istore 5
iload 5
ifge L2
iconst_0
istore 3
L3:
iload 3
iload 6
if_icmpeq L0
aload 0
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getScrollX()I
iload 3
invokespecial android/widget/FrameLayout/scrollTo(II)V
iconst_1
ireturn
L2:
iload 4
istore 3
iload 5
iload 4
if_icmpgt L3
iload 5
istore 3
goto L3
.limit locals 7
.limit stack 3
.end method

.method public final onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
iconst_1
istore 5
aload 1
invokevirtual android/view/MotionEvent/getAction()I
istore 2
iload 2
iconst_2
if_icmpne L0
aload 0
getfield android/support/v4/widget/NestedScrollView/m Z
ifeq L0
iconst_1
ireturn
L0:
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getScrollY()I
ifne L1
aload 0
iconst_1
invokestatic android/support/v4/view/cx/b(Landroid/view/View;I)Z
ifne L1
iconst_0
ireturn
L1:
iload 2
sipush 255
iand
tableswitch 0
L2
L3
L4
L3
L5
L5
L6
default : L5
L5:
aload 0
getfield android/support/v4/widget/NestedScrollView/m Z
ireturn
L4:
aload 0
getfield android/support/v4/widget/NestedScrollView/t I
istore 2
iload 2
iconst_m1
if_icmpeq L5
aload 1
iload 2
invokestatic android/support/v4/view/bk/a(Landroid/view/MotionEvent;I)I
istore 3
iload 3
iconst_m1
if_icmpne L7
ldc "NestedScrollView"
new java/lang/StringBuilder
dup
ldc "Invalid pointerId="
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
iload 2
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc " in onInterceptTouchEvent"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
goto L5
L7:
aload 1
iload 3
invokestatic android/support/v4/view/bk/d(Landroid/view/MotionEvent;I)F
f2i
istore 2
iload 2
aload 0
getfield android/support/v4/widget/NestedScrollView/i I
isub
invokestatic java/lang/Math/abs(I)I
aload 0
getfield android/support/v4/widget/NestedScrollView/q I
if_icmple L5
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getNestedScrollAxes()I
iconst_2
iand
ifne L5
aload 0
iconst_1
putfield android/support/v4/widget/NestedScrollView/m Z
aload 0
iload 2
putfield android/support/v4/widget/NestedScrollView/i I
aload 0
invokespecial android/support/v4/widget/NestedScrollView/f()V
aload 0
getfield android/support/v4/widget/NestedScrollView/n Landroid/view/VelocityTracker;
aload 1
invokevirtual android/view/VelocityTracker/addMovement(Landroid/view/MotionEvent;)V
aload 0
iconst_0
putfield android/support/v4/widget/NestedScrollView/w I
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getParent()Landroid/view/ViewParent;
astore 1
aload 1
ifnull L5
aload 1
iconst_1
invokeinterface android/view/ViewParent/requestDisallowInterceptTouchEvent(Z)V 1
goto L5
L2:
aload 1
invokevirtual android/view/MotionEvent/getY()F
f2i
istore 3
aload 1
invokevirtual android/view/MotionEvent/getX()F
f2i
istore 2
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getChildCount()I
ifle L8
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getScrollY()I
istore 4
aload 0
iconst_0
invokevirtual android/support/v4/widget/NestedScrollView/getChildAt(I)Landroid/view/View;
astore 6
iload 3
aload 6
invokevirtual android/view/View/getTop()I
iload 4
isub
if_icmplt L9
iload 3
aload 6
invokevirtual android/view/View/getBottom()I
iload 4
isub
if_icmpge L9
iload 2
aload 6
invokevirtual android/view/View/getLeft()I
if_icmplt L9
iload 2
aload 6
invokevirtual android/view/View/getRight()I
if_icmpge L9
iconst_1
istore 2
L10:
iload 2
ifne L11
aload 0
iconst_0
putfield android/support/v4/widget/NestedScrollView/m Z
aload 0
invokespecial android/support/v4/widget/NestedScrollView/g()V
goto L5
L9:
iconst_0
istore 2
goto L10
L8:
iconst_0
istore 2
goto L10
L11:
aload 0
iload 3
putfield android/support/v4/widget/NestedScrollView/i I
aload 0
aload 1
iconst_0
invokestatic android/support/v4/view/bk/b(Landroid/view/MotionEvent;I)I
putfield android/support/v4/widget/NestedScrollView/t I
aload 0
getfield android/support/v4/widget/NestedScrollView/n Landroid/view/VelocityTracker;
ifnonnull L12
aload 0
invokestatic android/view/VelocityTracker/obtain()Landroid/view/VelocityTracker;
putfield android/support/v4/widget/NestedScrollView/n Landroid/view/VelocityTracker;
L13:
aload 0
getfield android/support/v4/widget/NestedScrollView/n Landroid/view/VelocityTracker;
aload 1
invokevirtual android/view/VelocityTracker/addMovement(Landroid/view/MotionEvent;)V
aload 0
getfield android/support/v4/widget/NestedScrollView/f Landroid/support/v4/widget/ca;
invokevirtual android/support/v4/widget/ca/a()Z
ifne L14
L15:
aload 0
iload 5
putfield android/support/v4/widget/NestedScrollView/m Z
aload 0
iconst_2
invokevirtual android/support/v4/widget/NestedScrollView/startNestedScroll(I)Z
pop
goto L5
L12:
aload 0
getfield android/support/v4/widget/NestedScrollView/n Landroid/view/VelocityTracker;
invokevirtual android/view/VelocityTracker/clear()V
goto L13
L14:
iconst_0
istore 5
goto L15
L3:
aload 0
iconst_0
putfield android/support/v4/widget/NestedScrollView/m Z
aload 0
iconst_m1
putfield android/support/v4/widget/NestedScrollView/t I
aload 0
invokespecial android/support/v4/widget/NestedScrollView/g()V
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/stopNestedScroll()V
goto L5
L6:
aload 0
aload 1
invokespecial android/support/v4/widget/NestedScrollView/a(Landroid/view/MotionEvent;)V
goto L5
.limit locals 7
.limit stack 4
.end method

.method protected final onLayout(ZIIII)V
aload 0
iload 1
iload 2
iload 3
iload 4
iload 5
invokespecial android/widget/FrameLayout/onLayout(ZIIII)V
aload 0
iconst_0
putfield android/support/v4/widget/NestedScrollView/j Z
aload 0
getfield android/support/v4/widget/NestedScrollView/l Landroid/view/View;
ifnull L0
aload 0
getfield android/support/v4/widget/NestedScrollView/l Landroid/view/View;
aload 0
invokestatic android/support/v4/widget/NestedScrollView/a(Landroid/view/View;Landroid/view/View;)Z
ifeq L0
aload 0
aload 0
getfield android/support/v4/widget/NestedScrollView/l Landroid/view/View;
invokespecial android/support/v4/widget/NestedScrollView/b(Landroid/view/View;)V
L0:
aload 0
aconst_null
putfield android/support/v4/widget/NestedScrollView/l Landroid/view/View;
aload 0
getfield android/support/v4/widget/NestedScrollView/k Z
ifne L1
aload 0
getfield android/support/v4/widget/NestedScrollView/y Landroid/support/v4/widget/NestedScrollView$SavedState;
ifnull L2
aload 0
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getScrollX()I
aload 0
getfield android/support/v4/widget/NestedScrollView/y Landroid/support/v4/widget/NestedScrollView$SavedState;
getfield android/support/v4/widget/NestedScrollView$SavedState/a I
invokevirtual android/support/v4/widget/NestedScrollView/scrollTo(II)V
aload 0
aconst_null
putfield android/support/v4/widget/NestedScrollView/y Landroid/support/v4/widget/NestedScrollView$SavedState;
L2:
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getChildCount()I
ifle L3
aload 0
iconst_0
invokevirtual android/support/v4/widget/NestedScrollView/getChildAt(I)Landroid/view/View;
invokevirtual android/view/View/getMeasuredHeight()I
istore 2
L4:
iconst_0
iload 2
iload 5
iload 3
isub
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getPaddingBottom()I
isub
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getPaddingTop()I
isub
isub
invokestatic java/lang/Math/max(II)I
istore 2
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getScrollY()I
iload 2
if_icmple L5
aload 0
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getScrollX()I
iload 2
invokevirtual android/support/v4/widget/NestedScrollView/scrollTo(II)V
L1:
aload 0
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getScrollX()I
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getScrollY()I
invokevirtual android/support/v4/widget/NestedScrollView/scrollTo(II)V
aload 0
iconst_1
putfield android/support/v4/widget/NestedScrollView/k Z
return
L3:
iconst_0
istore 2
goto L4
L5:
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getScrollY()I
ifge L1
aload 0
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getScrollX()I
iconst_0
invokevirtual android/support/v4/widget/NestedScrollView/scrollTo(II)V
goto L1
.limit locals 6
.limit stack 6
.end method

.method protected final onMeasure(II)V
aload 0
iload 1
iload 2
invokespecial android/widget/FrameLayout/onMeasure(II)V
aload 0
getfield android/support/v4/widget/NestedScrollView/o Z
ifne L0
L1:
return
L0:
iload 2
invokestatic android/view/View$MeasureSpec/getMode(I)I
ifeq L1
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getChildCount()I
ifle L1
aload 0
iconst_0
invokevirtual android/support/v4/widget/NestedScrollView/getChildAt(I)Landroid/view/View;
astore 3
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getMeasuredHeight()I
istore 2
aload 3
invokevirtual android/view/View/getMeasuredHeight()I
iload 2
if_icmpge L1
aload 3
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/widget/FrameLayout$LayoutParams
astore 4
aload 3
iload 1
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getPaddingLeft()I
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getPaddingRight()I
iadd
aload 4
getfield android/widget/FrameLayout$LayoutParams/width I
invokestatic android/support/v4/widget/NestedScrollView/getChildMeasureSpec(III)I
iload 2
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getPaddingTop()I
isub
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getPaddingBottom()I
isub
ldc_w 1073741824
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
invokevirtual android/view/View/measure(II)V
return
.limit locals 5
.limit stack 4
.end method

.method public final onNestedFling(Landroid/view/View;FFZ)Z
iload 4
ifne L0
aload 0
fload 3
f2i
invokespecial android/support/v4/widget/NestedScrollView/g(I)V
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 5
.limit stack 2
.end method

.method public final onNestedPreFling(Landroid/view/View;FF)Z
iconst_0
ireturn
.limit locals 4
.limit stack 1
.end method

.method public final onNestedPreScroll(Landroid/view/View;II[I)V
return
.limit locals 5
.limit stack 0
.end method

.method public final onNestedScroll(Landroid/view/View;IIII)V
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getScrollY()I
istore 2
aload 0
iconst_0
iload 5
invokevirtual android/support/v4/widget/NestedScrollView/scrollBy(II)V
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getScrollY()I
iload 2
isub
istore 2
aload 0
iconst_0
iload 2
iconst_0
iload 5
iload 2
isub
aconst_null
invokevirtual android/support/v4/widget/NestedScrollView/dispatchNestedScroll(IIII[I)Z
pop
return
.limit locals 6
.limit stack 6
.end method

.method public final onNestedScrollAccepted(Landroid/view/View;Landroid/view/View;I)V
aload 0
getfield android/support/v4/widget/NestedScrollView/B Landroid/support/v4/view/bw;
iload 3
putfield android/support/v4/view/bw/a I
aload 0
iconst_2
invokevirtual android/support/v4/widget/NestedScrollView/startNestedScroll(I)Z
pop
return
.limit locals 4
.limit stack 2
.end method

.method protected final onOverScrolled(IIZZ)V
aload 0
iload 1
iload 2
invokespecial android/widget/FrameLayout/scrollTo(II)V
return
.limit locals 5
.limit stack 3
.end method

.method protected final onRequestFocusInDescendants(ILandroid/graphics/Rect;)Z
iload 1
iconst_2
if_icmpne L0
sipush 130
istore 3
L1:
aload 2
ifnonnull L2
invokestatic android/view/FocusFinder/getInstance()Landroid/view/FocusFinder;
aload 0
aconst_null
iload 3
invokevirtual android/view/FocusFinder/findNextFocus(Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;
astore 4
L3:
aload 4
ifnonnull L4
L5:
iconst_0
ireturn
L0:
iload 1
istore 3
iload 1
iconst_1
if_icmpne L1
bipush 33
istore 3
goto L1
L2:
invokestatic android/view/FocusFinder/getInstance()Landroid/view/FocusFinder;
aload 0
aload 2
iload 3
invokevirtual android/view/FocusFinder/findNextFocusFromRect(Landroid/view/ViewGroup;Landroid/graphics/Rect;I)Landroid/view/View;
astore 4
goto L3
L4:
aload 0
aload 4
invokespecial android/support/v4/widget/NestedScrollView/a(Landroid/view/View;)Z
ifne L5
aload 4
iload 3
aload 2
invokevirtual android/view/View/requestFocus(ILandroid/graphics/Rect;)Z
ireturn
.limit locals 5
.limit stack 4
.end method

.method protected final onRestoreInstanceState(Landroid/os/Parcelable;)V
aload 1
checkcast android/support/v4/widget/NestedScrollView$SavedState
astore 1
aload 0
aload 1
invokevirtual android/support/v4/widget/NestedScrollView$SavedState/getSuperState()Landroid/os/Parcelable;
invokespecial android/widget/FrameLayout/onRestoreInstanceState(Landroid/os/Parcelable;)V
aload 0
aload 1
putfield android/support/v4/widget/NestedScrollView/y Landroid/support/v4/widget/NestedScrollView$SavedState;
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/requestLayout()V
return
.limit locals 2
.limit stack 2
.end method

.method protected final onSaveInstanceState()Landroid/os/Parcelable;
new android/support/v4/widget/NestedScrollView$SavedState
dup
aload 0
invokespecial android/widget/FrameLayout/onSaveInstanceState()Landroid/os/Parcelable;
invokespecial android/support/v4/widget/NestedScrollView$SavedState/<init>(Landroid/os/Parcelable;)V
astore 1
aload 1
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getScrollY()I
putfield android/support/v4/widget/NestedScrollView$SavedState/a I
aload 1
areturn
.limit locals 2
.limit stack 3
.end method

.method protected final onSizeChanged(IIII)V
aload 0
iload 1
iload 2
iload 3
iload 4
invokespecial android/widget/FrameLayout/onSizeChanged(IIII)V
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/findFocus()Landroid/view/View;
astore 5
aload 5
ifnull L0
aload 0
aload 5
if_acmpne L1
L0:
return
L1:
aload 0
aload 5
iconst_0
iload 4
invokespecial android/support/v4/widget/NestedScrollView/a(Landroid/view/View;II)Z
ifeq L0
aload 5
aload 0
getfield android/support/v4/widget/NestedScrollView/e Landroid/graphics/Rect;
invokevirtual android/view/View/getDrawingRect(Landroid/graphics/Rect;)V
aload 0
aload 5
aload 0
getfield android/support/v4/widget/NestedScrollView/e Landroid/graphics/Rect;
invokevirtual android/support/v4/widget/NestedScrollView/offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V
aload 0
aload 0
aload 0
getfield android/support/v4/widget/NestedScrollView/e Landroid/graphics/Rect;
invokespecial android/support/v4/widget/NestedScrollView/a(Landroid/graphics/Rect;)I
invokespecial android/support/v4/widget/NestedScrollView/e(I)V
return
.limit locals 6
.limit stack 5
.end method

.method public final onStartNestedScroll(Landroid/view/View;Landroid/view/View;I)Z
iload 3
iconst_2
iand
ifeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 4
.limit stack 2
.end method

.method public final onStopNestedScroll(Landroid/view/View;)V
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/stopNestedScroll()V
return
.limit locals 2
.limit stack 1
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
aload 0
invokespecial android/support/v4/widget/NestedScrollView/f()V
aload 1
invokestatic android/view/MotionEvent/obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;
astore 9
aload 1
invokestatic android/support/v4/view/bk/a(Landroid/view/MotionEvent;)I
istore 2
iload 2
ifne L0
aload 0
iconst_0
putfield android/support/v4/widget/NestedScrollView/w I
L0:
aload 9
fconst_0
aload 0
getfield android/support/v4/widget/NestedScrollView/w I
i2f
invokevirtual android/view/MotionEvent/offsetLocation(FF)V
iload 2
tableswitch 0
L1
L2
L3
L4
L5
L6
L7
default : L5
L5:
aload 0
getfield android/support/v4/widget/NestedScrollView/n Landroid/view/VelocityTracker;
ifnull L8
aload 0
getfield android/support/v4/widget/NestedScrollView/n Landroid/view/VelocityTracker;
aload 9
invokevirtual android/view/VelocityTracker/addMovement(Landroid/view/MotionEvent;)V
L8:
aload 9
invokevirtual android/view/MotionEvent/recycle()V
iconst_1
ireturn
L1:
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getChildCount()I
ifne L9
iconst_0
ireturn
L9:
aload 0
getfield android/support/v4/widget/NestedScrollView/f Landroid/support/v4/widget/ca;
invokevirtual android/support/v4/widget/ca/a()Z
ifne L10
iconst_1
istore 8
L11:
aload 0
iload 8
putfield android/support/v4/widget/NestedScrollView/m Z
iload 8
ifeq L12
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getParent()Landroid/view/ViewParent;
astore 10
aload 10
ifnull L12
aload 10
iconst_1
invokeinterface android/view/ViewParent/requestDisallowInterceptTouchEvent(Z)V 1
L12:
aload 0
getfield android/support/v4/widget/NestedScrollView/f Landroid/support/v4/widget/ca;
invokevirtual android/support/v4/widget/ca/a()Z
ifne L13
aload 0
getfield android/support/v4/widget/NestedScrollView/f Landroid/support/v4/widget/ca;
invokevirtual android/support/v4/widget/ca/g()V
L13:
aload 0
aload 1
invokevirtual android/view/MotionEvent/getY()F
f2i
putfield android/support/v4/widget/NestedScrollView/i I
aload 0
aload 1
iconst_0
invokestatic android/support/v4/view/bk/b(Landroid/view/MotionEvent;I)I
putfield android/support/v4/widget/NestedScrollView/t I
aload 0
iconst_2
invokevirtual android/support/v4/widget/NestedScrollView/startNestedScroll(I)Z
pop
goto L5
L10:
iconst_0
istore 8
goto L11
L3:
aload 1
aload 0
getfield android/support/v4/widget/NestedScrollView/t I
invokestatic android/support/v4/view/bk/a(Landroid/view/MotionEvent;I)I
istore 4
iload 4
iconst_m1
if_icmpne L14
ldc "NestedScrollView"
new java/lang/StringBuilder
dup
ldc "Invalid pointerId="
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
getfield android/support/v4/widget/NestedScrollView/t I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc " in onTouchEvent"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
goto L5
L14:
aload 1
iload 4
invokestatic android/support/v4/view/bk/d(Landroid/view/MotionEvent;I)F
f2i
istore 5
aload 0
getfield android/support/v4/widget/NestedScrollView/i I
iload 5
isub
istore 3
iload 3
istore 2
aload 0
iconst_0
iload 3
aload 0
getfield android/support/v4/widget/NestedScrollView/v [I
aload 0
getfield android/support/v4/widget/NestedScrollView/u [I
invokevirtual android/support/v4/widget/NestedScrollView/dispatchNestedPreScroll(II[I[I)Z
ifeq L15
iload 3
aload 0
getfield android/support/v4/widget/NestedScrollView/v [I
iconst_1
iaload
isub
istore 2
aload 9
fconst_0
aload 0
getfield android/support/v4/widget/NestedScrollView/u [I
iconst_1
iaload
i2f
invokevirtual android/view/MotionEvent/offsetLocation(FF)V
aload 0
aload 0
getfield android/support/v4/widget/NestedScrollView/w I
aload 0
getfield android/support/v4/widget/NestedScrollView/u [I
iconst_1
iaload
iadd
putfield android/support/v4/widget/NestedScrollView/w I
L15:
aload 0
getfield android/support/v4/widget/NestedScrollView/m Z
ifne L16
iload 2
invokestatic java/lang/Math/abs(I)I
aload 0
getfield android/support/v4/widget/NestedScrollView/q I
if_icmple L16
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getParent()Landroid/view/ViewParent;
astore 10
aload 10
ifnull L17
aload 10
iconst_1
invokeinterface android/view/ViewParent/requestDisallowInterceptTouchEvent(Z)V 1
L17:
aload 0
iconst_1
putfield android/support/v4/widget/NestedScrollView/m Z
iload 2
ifle L18
iload 2
aload 0
getfield android/support/v4/widget/NestedScrollView/q I
isub
istore 2
L19:
aload 0
getfield android/support/v4/widget/NestedScrollView/m Z
ifeq L5
aload 0
iload 5
aload 0
getfield android/support/v4/widget/NestedScrollView/u [I
iconst_1
iaload
isub
putfield android/support/v4/widget/NestedScrollView/i I
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getScrollY()I
istore 6
aload 0
invokespecial android/support/v4/widget/NestedScrollView/getScrollRange()I
istore 5
aload 0
invokestatic android/support/v4/view/cx/a(Landroid/view/View;)I
istore 3
iload 3
ifeq L20
iload 3
iconst_1
if_icmpne L21
iload 5
ifle L21
L20:
iconst_1
istore 3
L22:
aload 0
iconst_0
iload 2
iconst_0
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getScrollY()I
iload 5
invokespecial android/support/v4/widget/NestedScrollView/a(IIIII)Z
ifeq L23
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/hasNestedScrollingParent()Z
ifne L23
aload 0
getfield android/support/v4/widget/NestedScrollView/n Landroid/view/VelocityTracker;
invokevirtual android/view/VelocityTracker/clear()V
L23:
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getScrollY()I
iload 6
isub
istore 7
aload 0
iconst_0
iload 7
iconst_0
iload 2
iload 7
isub
aload 0
getfield android/support/v4/widget/NestedScrollView/u [I
invokevirtual android/support/v4/widget/NestedScrollView/dispatchNestedScroll(IIII[I)Z
ifeq L24
aload 0
aload 0
getfield android/support/v4/widget/NestedScrollView/i I
aload 0
getfield android/support/v4/widget/NestedScrollView/u [I
iconst_1
iaload
isub
putfield android/support/v4/widget/NestedScrollView/i I
aload 9
fconst_0
aload 0
getfield android/support/v4/widget/NestedScrollView/u [I
iconst_1
iaload
i2f
invokevirtual android/view/MotionEvent/offsetLocation(FF)V
aload 0
aload 0
getfield android/support/v4/widget/NestedScrollView/w I
aload 0
getfield android/support/v4/widget/NestedScrollView/u [I
iconst_1
iaload
iadd
putfield android/support/v4/widget/NestedScrollView/w I
goto L5
L18:
iload 2
aload 0
getfield android/support/v4/widget/NestedScrollView/q I
iadd
istore 2
goto L19
L21:
iconst_0
istore 3
goto L22
L24:
iload 3
ifeq L5
aload 0
invokespecial android/support/v4/widget/NestedScrollView/i()V
iload 6
iload 2
iadd
istore 3
iload 3
ifge L25
aload 0
getfield android/support/v4/widget/NestedScrollView/g Landroid/support/v4/widget/al;
iload 2
i2f
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getHeight()I
i2f
fdiv
aload 1
iload 4
invokestatic android/support/v4/view/bk/c(Landroid/view/MotionEvent;I)F
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getWidth()I
i2f
fdiv
invokevirtual android/support/v4/widget/al/a(FF)Z
pop
aload 0
getfield android/support/v4/widget/NestedScrollView/h Landroid/support/v4/widget/al;
invokevirtual android/support/v4/widget/al/a()Z
ifne L26
aload 0
getfield android/support/v4/widget/NestedScrollView/h Landroid/support/v4/widget/al;
invokevirtual android/support/v4/widget/al/c()Z
pop
L26:
aload 0
getfield android/support/v4/widget/NestedScrollView/g Landroid/support/v4/widget/al;
ifnull L5
aload 0
getfield android/support/v4/widget/NestedScrollView/g Landroid/support/v4/widget/al;
invokevirtual android/support/v4/widget/al/a()Z
ifeq L27
aload 0
getfield android/support/v4/widget/NestedScrollView/h Landroid/support/v4/widget/al;
invokevirtual android/support/v4/widget/al/a()Z
ifne L5
L27:
aload 0
invokestatic android/support/v4/view/cx/b(Landroid/view/View;)V
goto L5
L25:
iload 3
iload 5
if_icmple L26
aload 0
getfield android/support/v4/widget/NestedScrollView/h Landroid/support/v4/widget/al;
iload 2
i2f
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getHeight()I
i2f
fdiv
fconst_1
aload 1
iload 4
invokestatic android/support/v4/view/bk/c(Landroid/view/MotionEvent;I)F
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getWidth()I
i2f
fdiv
fsub
invokevirtual android/support/v4/widget/al/a(FF)Z
pop
aload 0
getfield android/support/v4/widget/NestedScrollView/g Landroid/support/v4/widget/al;
invokevirtual android/support/v4/widget/al/a()Z
ifne L26
aload 0
getfield android/support/v4/widget/NestedScrollView/g Landroid/support/v4/widget/al;
invokevirtual android/support/v4/widget/al/c()Z
pop
goto L26
L2:
aload 0
getfield android/support/v4/widget/NestedScrollView/m Z
ifeq L5
aload 0
getfield android/support/v4/widget/NestedScrollView/n Landroid/view/VelocityTracker;
astore 1
aload 1
sipush 1000
aload 0
getfield android/support/v4/widget/NestedScrollView/s I
i2f
invokevirtual android/view/VelocityTracker/computeCurrentVelocity(IF)V
aload 1
aload 0
getfield android/support/v4/widget/NestedScrollView/t I
invokestatic android/support/v4/view/cs/b(Landroid/view/VelocityTracker;I)F
f2i
istore 2
iload 2
invokestatic java/lang/Math/abs(I)I
aload 0
getfield android/support/v4/widget/NestedScrollView/r I
if_icmple L28
aload 0
iload 2
ineg
invokespecial android/support/v4/widget/NestedScrollView/g(I)V
L28:
aload 0
iconst_m1
putfield android/support/v4/widget/NestedScrollView/t I
aload 0
invokespecial android/support/v4/widget/NestedScrollView/h()V
goto L5
L4:
aload 0
getfield android/support/v4/widget/NestedScrollView/m Z
ifeq L5
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getChildCount()I
ifle L5
aload 0
iconst_m1
putfield android/support/v4/widget/NestedScrollView/t I
aload 0
invokespecial android/support/v4/widget/NestedScrollView/h()V
goto L5
L6:
aload 1
invokestatic android/support/v4/view/bk/b(Landroid/view/MotionEvent;)I
istore 2
aload 0
aload 1
iload 2
invokestatic android/support/v4/view/bk/d(Landroid/view/MotionEvent;I)F
f2i
putfield android/support/v4/widget/NestedScrollView/i I
aload 0
aload 1
iload 2
invokestatic android/support/v4/view/bk/b(Landroid/view/MotionEvent;I)I
putfield android/support/v4/widget/NestedScrollView/t I
goto L5
L7:
aload 0
aload 1
invokespecial android/support/v4/widget/NestedScrollView/a(Landroid/view/MotionEvent;)V
aload 0
aload 1
aload 1
aload 0
getfield android/support/v4/widget/NestedScrollView/t I
invokestatic android/support/v4/view/bk/a(Landroid/view/MotionEvent;I)I
invokestatic android/support/v4/view/bk/d(Landroid/view/MotionEvent;I)F
f2i
putfield android/support/v4/widget/NestedScrollView/i I
goto L5
L16:
goto L19
.limit locals 11
.limit stack 6
.end method

.method public final requestChildFocus(Landroid/view/View;Landroid/view/View;)V
aload 0
getfield android/support/v4/widget/NestedScrollView/j Z
ifne L0
aload 0
aload 2
invokespecial android/support/v4/widget/NestedScrollView/b(Landroid/view/View;)V
L1:
aload 0
aload 1
aload 2
invokespecial android/widget/FrameLayout/requestChildFocus(Landroid/view/View;Landroid/view/View;)V
return
L0:
aload 0
aload 2
putfield android/support/v4/widget/NestedScrollView/l Landroid/view/View;
goto L1
.limit locals 3
.limit stack 3
.end method

.method public final requestChildRectangleOnScreen(Landroid/view/View;Landroid/graphics/Rect;Z)Z
aload 2
aload 1
invokevirtual android/view/View/getLeft()I
aload 1
invokevirtual android/view/View/getScrollX()I
isub
aload 1
invokevirtual android/view/View/getTop()I
aload 1
invokevirtual android/view/View/getScrollY()I
isub
invokevirtual android/graphics/Rect/offset(II)V
aload 0
aload 2
invokespecial android/support/v4/widget/NestedScrollView/a(Landroid/graphics/Rect;)I
istore 4
iload 4
ifeq L0
iconst_1
istore 5
L1:
iload 5
ifeq L2
iload 3
ifeq L3
aload 0
iconst_0
iload 4
invokevirtual android/support/v4/widget/NestedScrollView/scrollBy(II)V
L2:
iload 5
ireturn
L0:
iconst_0
istore 5
goto L1
L3:
aload 0
iconst_0
iload 4
invokespecial android/support/v4/widget/NestedScrollView/b(II)V
iload 5
ireturn
.limit locals 6
.limit stack 4
.end method

.method public final requestDisallowInterceptTouchEvent(Z)V
iload 1
ifeq L0
aload 0
invokespecial android/support/v4/widget/NestedScrollView/g()V
L0:
aload 0
iload 1
invokespecial android/widget/FrameLayout/requestDisallowInterceptTouchEvent(Z)V
return
.limit locals 2
.limit stack 2
.end method

.method public final requestLayout()V
aload 0
iconst_1
putfield android/support/v4/widget/NestedScrollView/j Z
aload 0
invokespecial android/widget/FrameLayout/requestLayout()V
return
.limit locals 1
.limit stack 2
.end method

.method public final scrollTo(II)V
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getChildCount()I
ifle L0
aload 0
iconst_0
invokevirtual android/support/v4/widget/NestedScrollView/getChildAt(I)Landroid/view/View;
astore 3
iload 1
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getWidth()I
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getPaddingRight()I
isub
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getPaddingLeft()I
isub
aload 3
invokevirtual android/view/View/getWidth()I
invokestatic android/support/v4/widget/NestedScrollView/b(III)I
istore 1
iload 2
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getHeight()I
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getPaddingBottom()I
isub
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getPaddingTop()I
isub
aload 3
invokevirtual android/view/View/getHeight()I
invokestatic android/support/v4/widget/NestedScrollView/b(III)I
istore 2
iload 1
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getScrollX()I
if_icmpne L1
iload 2
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/getScrollY()I
if_icmpeq L0
L1:
aload 0
iload 1
iload 2
invokespecial android/widget/FrameLayout/scrollTo(II)V
L0:
return
.limit locals 4
.limit stack 3
.end method

.method public final setFillViewport(Z)V
iload 1
aload 0
getfield android/support/v4/widget/NestedScrollView/o Z
if_icmpeq L0
aload 0
iload 1
putfield android/support/v4/widget/NestedScrollView/o Z
aload 0
invokevirtual android/support/v4/widget/NestedScrollView/requestLayout()V
L0:
return
.limit locals 2
.limit stack 2
.end method

.method public final setNestedScrollingEnabled(Z)V
aload 0
getfield android/support/v4/widget/NestedScrollView/C Landroid/support/v4/view/bu;
iload 1
invokevirtual android/support/v4/view/bu/a(Z)V
return
.limit locals 2
.limit stack 2
.end method

.method public final setSmoothScrollingEnabled(Z)V
aload 0
iload 1
putfield android/support/v4/widget/NestedScrollView/p Z
return
.limit locals 2
.limit stack 2
.end method

.method public final shouldDelayChildPressedState()Z
iconst_1
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final startNestedScroll(I)Z
aload 0
getfield android/support/v4/widget/NestedScrollView/C Landroid/support/v4/view/bu;
iload 1
invokevirtual android/support/v4/view/bu/a(I)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final stopNestedScroll()V
aload 0
getfield android/support/v4/widget/NestedScrollView/C Landroid/support/v4/view/bu;
invokevirtual android/support/v4/view/bu/b()V
return
.limit locals 1
.limit stack 1
.end method
