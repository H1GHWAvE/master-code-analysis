.bytecode 50.0
.class synchronized android/support/v4/widget/an
.super java/lang/Object
.implements android/support/v4/widget/ao

.method <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public final a(Landroid/content/Context;)Ljava/lang/Object;
new android/widget/EdgeEffect
dup
aload 1
invokespecial android/widget/EdgeEffect/<init>(Landroid/content/Context;)V
areturn
.limit locals 2
.limit stack 3
.end method

.method public final a(Ljava/lang/Object;II)V
aload 1
checkcast android/widget/EdgeEffect
iload 2
iload 3
invokevirtual android/widget/EdgeEffect/setSize(II)V
return
.limit locals 4
.limit stack 3
.end method

.method public final a(Ljava/lang/Object;)Z
aload 1
checkcast android/widget/EdgeEffect
invokevirtual android/widget/EdgeEffect/isFinished()Z
ireturn
.limit locals 2
.limit stack 1
.end method

.method public final a(Ljava/lang/Object;F)Z
aload 1
fload 2
invokestatic android/support/v4/widget/aq/a(Ljava/lang/Object;F)Z
ireturn
.limit locals 3
.limit stack 2
.end method

.method public a(Ljava/lang/Object;FF)Z
aload 1
fload 2
invokestatic android/support/v4/widget/aq/a(Ljava/lang/Object;F)Z
ireturn
.limit locals 4
.limit stack 2
.end method

.method public final a(Ljava/lang/Object;I)Z
aload 1
checkcast android/widget/EdgeEffect
iload 2
invokevirtual android/widget/EdgeEffect/onAbsorb(I)V
iconst_1
ireturn
.limit locals 3
.limit stack 2
.end method

.method public final a(Ljava/lang/Object;Landroid/graphics/Canvas;)Z
aload 1
checkcast android/widget/EdgeEffect
aload 2
invokevirtual android/widget/EdgeEffect/draw(Landroid/graphics/Canvas;)Z
ireturn
.limit locals 3
.limit stack 2
.end method

.method public final b(Ljava/lang/Object;)V
aload 1
checkcast android/widget/EdgeEffect
invokevirtual android/widget/EdgeEffect/finish()V
return
.limit locals 2
.limit stack 1
.end method

.method public final c(Ljava/lang/Object;)Z
aload 1
checkcast android/widget/EdgeEffect
astore 1
aload 1
invokevirtual android/widget/EdgeEffect/onRelease()V
aload 1
invokevirtual android/widget/EdgeEffect/isFinished()Z
ireturn
.limit locals 2
.limit stack 1
.end method
