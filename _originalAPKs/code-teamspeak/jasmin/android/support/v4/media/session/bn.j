.bytecode 50.0
.class public final synchronized android/support/v4/media/session/bn
.super java/lang/Object

.field private final 'a' Ljava/lang/String;

.field private final 'b' Ljava/lang/CharSequence;

.field private final 'c' I

.field private 'd' Landroid/os/Bundle;

.method private <init>(Ljava/lang/String;Ljava/lang/CharSequence;I)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 1
invokestatic android/text/TextUtils/isEmpty(Ljava/lang/CharSequence;)Z
ifeq L0
new java/lang/IllegalArgumentException
dup
ldc "You must specify an action to build a CustomAction."
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 2
invokestatic android/text/TextUtils/isEmpty(Ljava/lang/CharSequence;)Z
ifeq L1
new java/lang/IllegalArgumentException
dup
ldc "You must specify a name to build a CustomAction."
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L1:
iload 3
ifne L2
new java/lang/IllegalArgumentException
dup
ldc "You must specify an icon resource id to build a CustomAction."
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L2:
aload 0
aload 1
putfield android/support/v4/media/session/bn/a Ljava/lang/String;
aload 0
aload 2
putfield android/support/v4/media/session/bn/b Ljava/lang/CharSequence;
aload 0
iload 3
putfield android/support/v4/media/session/bn/c I
return
.limit locals 4
.limit stack 3
.end method

.method private a()Landroid/support/v4/media/session/PlaybackStateCompat$CustomAction;
new android/support/v4/media/session/PlaybackStateCompat$CustomAction
dup
aload 0
getfield android/support/v4/media/session/bn/a Ljava/lang/String;
aload 0
getfield android/support/v4/media/session/bn/b Ljava/lang/CharSequence;
aload 0
getfield android/support/v4/media/session/bn/c I
aload 0
getfield android/support/v4/media/session/bn/d Landroid/os/Bundle;
iconst_0
invokespecial android/support/v4/media/session/PlaybackStateCompat$CustomAction/<init>(Ljava/lang/String;Ljava/lang/CharSequence;ILandroid/os/Bundle;B)V
areturn
.limit locals 1
.limit stack 7
.end method

.method private a(Landroid/os/Bundle;)Landroid/support/v4/media/session/bn;
aload 0
aload 1
putfield android/support/v4/media/session/bn/d Landroid/os/Bundle;
aload 0
areturn
.limit locals 2
.limit stack 2
.end method
