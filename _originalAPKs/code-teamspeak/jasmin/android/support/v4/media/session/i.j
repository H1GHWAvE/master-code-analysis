.bytecode 50.0
.class public synchronized abstract android/support/v4/media/session/i
.super java/lang/Object
.implements android/os/IBinder$DeathRecipient

.field private final 'a' Ljava/lang/Object;

.field private 'b' Landroid/support/v4/media/session/j;

.field private 'c' Z

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
iconst_0
putfield android/support/v4/media/session/i/c Z
getstatic android/os/Build$VERSION/SDK_INT I
bipush 21
if_icmplt L0
aload 0
new android/support/v4/media/session/x
dup
new android/support/v4/media/session/k
dup
aload 0
iconst_0
invokespecial android/support/v4/media/session/k/<init>(Landroid/support/v4/media/session/i;B)V
invokespecial android/support/v4/media/session/x/<init>(Landroid/support/v4/media/session/w;)V
putfield android/support/v4/media/session/i/a Ljava/lang/Object;
return
L0:
aload 0
new android/support/v4/media/session/l
dup
aload 0
iconst_0
invokespecial android/support/v4/media/session/l/<init>(Landroid/support/v4/media/session/i;B)V
putfield android/support/v4/media/session/i/a Ljava/lang/Object;
return
.limit locals 1
.limit stack 7
.end method

.method static synthetic a(Landroid/support/v4/media/session/i;)Landroid/support/v4/media/session/j;
aload 0
getfield android/support/v4/media/session/i/b Landroid/support/v4/media/session/j;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static a()V
return
.limit locals 0
.limit stack 0
.end method

.method private a(Landroid/os/Handler;)V
aload 0
new android/support/v4/media/session/j
dup
aload 0
aload 1
invokevirtual android/os/Handler/getLooper()Landroid/os/Looper;
invokespecial android/support/v4/media/session/j/<init>(Landroid/support/v4/media/session/i;Landroid/os/Looper;)V
putfield android/support/v4/media/session/i/b Landroid/support/v4/media/session/j;
return
.limit locals 2
.limit stack 5
.end method

.method static synthetic a(Landroid/support/v4/media/session/i;Landroid/os/Handler;)V
aload 0
new android/support/v4/media/session/j
dup
aload 0
aload 1
invokevirtual android/os/Handler/getLooper()Landroid/os/Looper;
invokespecial android/support/v4/media/session/j/<init>(Landroid/support/v4/media/session/i;Landroid/os/Looper;)V
putfield android/support/v4/media/session/i/b Landroid/support/v4/media/session/j;
return
.limit locals 2
.limit stack 5
.end method

.method static synthetic a(Landroid/support/v4/media/session/i;Z)Z
aload 0
iload 1
putfield android/support/v4/media/session/i/c Z
iload 1
ireturn
.limit locals 2
.limit stack 2
.end method

.method private static b()V
return
.limit locals 0
.limit stack 0
.end method

.method static synthetic b(Landroid/support/v4/media/session/i;)Z
aload 0
getfield android/support/v4/media/session/i/c Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic c(Landroid/support/v4/media/session/i;)Ljava/lang/Object;
aload 0
getfield android/support/v4/media/session/i/a Ljava/lang/Object;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static c()V
return
.limit locals 0
.limit stack 0
.end method

.method private static d()V
return
.limit locals 0
.limit stack 0
.end method

.method private static e()V
return
.limit locals 0
.limit stack 0
.end method

.method private static f()V
return
.limit locals 0
.limit stack 0
.end method

.method private static g()V
return
.limit locals 0
.limit stack 0
.end method

.method private static h()V
return
.limit locals 0
.limit stack 0
.end method

.method public binderDied()V
return
.limit locals 1
.limit stack 0
.end method
