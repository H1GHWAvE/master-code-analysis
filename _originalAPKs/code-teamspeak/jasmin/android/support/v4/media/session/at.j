.bytecode 50.0
.class public final synchronized android/support/v4/media/session/at
.super java/lang/Object

.field private static final 'A' Ljava/lang/String; = "android.media.metadata.ALBUM"

.field private static final 'B' Ljava/lang/String; = "android.media.metadata.AUTHOR"

.field private static final 'C' Ljava/lang/String; = "android.media.metadata.WRITER"

.field private static final 'D' Ljava/lang/String; = "android.media.metadata.COMPOSER"

.field private static final 'E' Ljava/lang/String; = "android.media.metadata.COMPILATION"

.field private static final 'F' Ljava/lang/String; = "android.media.metadata.DATE"

.field private static final 'G' Ljava/lang/String; = "android.media.metadata.GENRE"

.field private static final 'H' Ljava/lang/String; = "android.media.metadata.TRACK_NUMBER"

.field private static final 'I' Ljava/lang/String; = "android.media.metadata.DISC_NUMBER"

.field private static final 'J' Ljava/lang/String; = "android.media.metadata.ALBUM_ARTIST"

.field static final 'a' I = 0


.field static final 'b' I = 0


.field static final 'c' I = 1


.field static final 'd' I = 2


.field static final 'e' I = 3


.field static final 'f' I = 4


.field static final 'g' I = 5


.field static final 'h' I = 6


.field static final 'i' I = 7


.field static final 'j' I = 8


.field static final 'k' I = 9


.field static final 'l' I = 10


.field static final 'm' I = 11


.field private static final 'n' J = 1L


.field private static final 'o' J = 2L


.field private static final 'p' J = 4L


.field private static final 'q' J = 8L


.field private static final 'r' J = 16L


.field private static final 's' J = 32L


.field private static final 't' J = 64L


.field private static final 'u' J = 512L


.field private static final 'v' Ljava/lang/String; = "android.media.metadata.ART"

.field private static final 'w' Ljava/lang/String; = "android.media.metadata.ALBUM_ART"

.field private static final 'x' Ljava/lang/String; = "android.media.metadata.TITLE"

.field private static final 'y' Ljava/lang/String; = "android.media.metadata.ARTIST"

.field private static final 'z' Ljava/lang/String; = "android.media.metadata.DURATION"

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method static a(I)I
iload 0
tableswitch 0
L0
L1
L2
L3
L4
L5
L6
L7
L6
L8
L9
L9
default : L10
L10:
iconst_m1
ireturn
L6:
bipush 8
ireturn
L7:
bipush 9
ireturn
L4:
iconst_4
ireturn
L0:
iconst_0
ireturn
L2:
iconst_2
ireturn
L3:
iconst_3
ireturn
L5:
iconst_5
ireturn
L8:
bipush 7
ireturn
L9:
bipush 6
ireturn
L1:
iconst_1
ireturn
.limit locals 1
.limit stack 1
.end method

.method static a(J)I
iconst_0
istore 3
lconst_1
lload 0
land
lconst_0
lcmp
ifeq L0
bipush 32
istore 3
L0:
iload 3
istore 2
ldc2_w 2L
lload 0
land
lconst_0
lcmp
ifeq L1
iload 3
bipush 16
ior
istore 2
L1:
iload 2
istore 3
ldc2_w 4L
lload 0
land
lconst_0
lcmp
ifeq L2
iload 2
iconst_4
ior
istore 3
L2:
iload 3
istore 2
ldc2_w 8L
lload 0
land
lconst_0
lcmp
ifeq L3
iload 3
iconst_2
ior
istore 2
L3:
iload 2
istore 3
ldc2_w 16L
lload 0
land
lconst_0
lcmp
ifeq L4
iload 2
iconst_1
ior
istore 3
L4:
iload 3
istore 2
ldc2_w 32L
lload 0
land
lconst_0
lcmp
ifeq L5
iload 3
sipush 128
ior
istore 2
L5:
iload 2
istore 3
ldc2_w 64L
lload 0
land
lconst_0
lcmp
ifeq L6
iload 2
bipush 64
ior
istore 3
L6:
iload 3
istore 2
ldc2_w 512L
lload 0
land
lconst_0
lcmp
ifeq L7
iload 3
bipush 8
ior
istore 2
L7:
iload 2
ireturn
.limit locals 4
.limit stack 4
.end method

.method private static a(Landroid/app/PendingIntent;)Ljava/lang/Object;
new android/media/RemoteControlClient
dup
aload 0
invokespecial android/media/RemoteControlClient/<init>(Landroid/app/PendingIntent;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method public static a(Landroid/content/Context;Ljava/lang/Object;)V
aload 0
ldc "audio"
invokevirtual android/content/Context/getSystemService(Ljava/lang/String;)Ljava/lang/Object;
checkcast android/media/AudioManager
aload 1
checkcast android/media/RemoteControlClient
invokevirtual android/media/AudioManager/unregisterRemoteControlClient(Landroid/media/RemoteControlClient;)V
return
.limit locals 2
.limit stack 2
.end method

.method static a(Landroid/os/Bundle;Landroid/media/RemoteControlClient$MetadataEditor;)V
aload 0
ifnonnull L0
L1:
return
L0:
aload 0
ldc "android.media.metadata.ART"
invokevirtual android/os/Bundle/containsKey(Ljava/lang/String;)Z
ifeq L2
aload 1
bipush 100
aload 0
ldc "android.media.metadata.ART"
invokevirtual android/os/Bundle/getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;
checkcast android/graphics/Bitmap
invokevirtual android/media/RemoteControlClient$MetadataEditor/putBitmap(ILandroid/graphics/Bitmap;)Landroid/media/RemoteControlClient$MetadataEditor;
pop
L3:
aload 0
ldc "android.media.metadata.ALBUM"
invokevirtual android/os/Bundle/containsKey(Ljava/lang/String;)Z
ifeq L4
aload 1
iconst_1
aload 0
ldc "android.media.metadata.ALBUM"
invokevirtual android/os/Bundle/getString(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/media/RemoteControlClient$MetadataEditor/putString(ILjava/lang/String;)Landroid/media/RemoteControlClient$MetadataEditor;
pop
L4:
aload 0
ldc "android.media.metadata.ALBUM_ARTIST"
invokevirtual android/os/Bundle/containsKey(Ljava/lang/String;)Z
ifeq L5
aload 1
bipush 13
aload 0
ldc "android.media.metadata.ALBUM_ARTIST"
invokevirtual android/os/Bundle/getString(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/media/RemoteControlClient$MetadataEditor/putString(ILjava/lang/String;)Landroid/media/RemoteControlClient$MetadataEditor;
pop
L5:
aload 0
ldc "android.media.metadata.ARTIST"
invokevirtual android/os/Bundle/containsKey(Ljava/lang/String;)Z
ifeq L6
aload 1
iconst_2
aload 0
ldc "android.media.metadata.ARTIST"
invokevirtual android/os/Bundle/getString(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/media/RemoteControlClient$MetadataEditor/putString(ILjava/lang/String;)Landroid/media/RemoteControlClient$MetadataEditor;
pop
L6:
aload 0
ldc "android.media.metadata.AUTHOR"
invokevirtual android/os/Bundle/containsKey(Ljava/lang/String;)Z
ifeq L7
aload 1
iconst_3
aload 0
ldc "android.media.metadata.AUTHOR"
invokevirtual android/os/Bundle/getString(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/media/RemoteControlClient$MetadataEditor/putString(ILjava/lang/String;)Landroid/media/RemoteControlClient$MetadataEditor;
pop
L7:
aload 0
ldc "android.media.metadata.COMPILATION"
invokevirtual android/os/Bundle/containsKey(Ljava/lang/String;)Z
ifeq L8
aload 1
bipush 15
aload 0
ldc "android.media.metadata.COMPILATION"
invokevirtual android/os/Bundle/getString(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/media/RemoteControlClient$MetadataEditor/putString(ILjava/lang/String;)Landroid/media/RemoteControlClient$MetadataEditor;
pop
L8:
aload 0
ldc "android.media.metadata.COMPOSER"
invokevirtual android/os/Bundle/containsKey(Ljava/lang/String;)Z
ifeq L9
aload 1
iconst_4
aload 0
ldc "android.media.metadata.COMPOSER"
invokevirtual android/os/Bundle/getString(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/media/RemoteControlClient$MetadataEditor/putString(ILjava/lang/String;)Landroid/media/RemoteControlClient$MetadataEditor;
pop
L9:
aload 0
ldc "android.media.metadata.DATE"
invokevirtual android/os/Bundle/containsKey(Ljava/lang/String;)Z
ifeq L10
aload 1
iconst_5
aload 0
ldc "android.media.metadata.DATE"
invokevirtual android/os/Bundle/getString(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/media/RemoteControlClient$MetadataEditor/putString(ILjava/lang/String;)Landroid/media/RemoteControlClient$MetadataEditor;
pop
L10:
aload 0
ldc "android.media.metadata.DISC_NUMBER"
invokevirtual android/os/Bundle/containsKey(Ljava/lang/String;)Z
ifeq L11
aload 1
bipush 14
aload 0
ldc "android.media.metadata.DISC_NUMBER"
invokevirtual android/os/Bundle/getLong(Ljava/lang/String;)J
invokevirtual android/media/RemoteControlClient$MetadataEditor/putLong(IJ)Landroid/media/RemoteControlClient$MetadataEditor;
pop
L11:
aload 0
ldc "android.media.metadata.DURATION"
invokevirtual android/os/Bundle/containsKey(Ljava/lang/String;)Z
ifeq L12
aload 1
bipush 9
aload 0
ldc "android.media.metadata.DURATION"
invokevirtual android/os/Bundle/getLong(Ljava/lang/String;)J
invokevirtual android/media/RemoteControlClient$MetadataEditor/putLong(IJ)Landroid/media/RemoteControlClient$MetadataEditor;
pop
L12:
aload 0
ldc "android.media.metadata.GENRE"
invokevirtual android/os/Bundle/containsKey(Ljava/lang/String;)Z
ifeq L13
aload 1
bipush 6
aload 0
ldc "android.media.metadata.GENRE"
invokevirtual android/os/Bundle/getString(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/media/RemoteControlClient$MetadataEditor/putString(ILjava/lang/String;)Landroid/media/RemoteControlClient$MetadataEditor;
pop
L13:
aload 0
ldc "android.media.metadata.TITLE"
invokevirtual android/os/Bundle/containsKey(Ljava/lang/String;)Z
ifeq L14
aload 1
bipush 7
aload 0
ldc "android.media.metadata.TITLE"
invokevirtual android/os/Bundle/getString(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/media/RemoteControlClient$MetadataEditor/putString(ILjava/lang/String;)Landroid/media/RemoteControlClient$MetadataEditor;
pop
L14:
aload 0
ldc "android.media.metadata.TRACK_NUMBER"
invokevirtual android/os/Bundle/containsKey(Ljava/lang/String;)Z
ifeq L15
aload 1
iconst_0
aload 0
ldc "android.media.metadata.TRACK_NUMBER"
invokevirtual android/os/Bundle/getLong(Ljava/lang/String;)J
invokevirtual android/media/RemoteControlClient$MetadataEditor/putLong(IJ)Landroid/media/RemoteControlClient$MetadataEditor;
pop
L15:
aload 0
ldc "android.media.metadata.WRITER"
invokevirtual android/os/Bundle/containsKey(Ljava/lang/String;)Z
ifeq L1
aload 1
bipush 11
aload 0
ldc "android.media.metadata.WRITER"
invokevirtual android/os/Bundle/getString(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/media/RemoteControlClient$MetadataEditor/putString(ILjava/lang/String;)Landroid/media/RemoteControlClient$MetadataEditor;
pop
return
L2:
aload 0
ldc "android.media.metadata.ALBUM_ART"
invokevirtual android/os/Bundle/containsKey(Ljava/lang/String;)Z
ifeq L3
aload 1
bipush 100
aload 0
ldc "android.media.metadata.ALBUM_ART"
invokevirtual android/os/Bundle/getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;
checkcast android/graphics/Bitmap
invokevirtual android/media/RemoteControlClient$MetadataEditor/putBitmap(ILandroid/graphics/Bitmap;)Landroid/media/RemoteControlClient$MetadataEditor;
pop
goto L3
.limit locals 2
.limit stack 4
.end method

.method public static a(Ljava/lang/Object;I)V
aload 0
checkcast android/media/RemoteControlClient
iload 1
invokestatic android/support/v4/media/session/at/a(I)I
invokevirtual android/media/RemoteControlClient/setPlaybackState(I)V
return
.limit locals 2
.limit stack 2
.end method

.method public static a(Ljava/lang/Object;J)V
aload 0
checkcast android/media/RemoteControlClient
lload 1
invokestatic android/support/v4/media/session/at/a(J)I
invokevirtual android/media/RemoteControlClient/setTransportControlFlags(I)V
return
.limit locals 3
.limit stack 3
.end method

.method private static a(Ljava/lang/Object;Landroid/os/Bundle;)V
aload 0
checkcast android/media/RemoteControlClient
iconst_1
invokevirtual android/media/RemoteControlClient/editMetadata(Z)Landroid/media/RemoteControlClient$MetadataEditor;
astore 0
aload 1
aload 0
invokestatic android/support/v4/media/session/at/a(Landroid/os/Bundle;Landroid/media/RemoteControlClient$MetadataEditor;)V
aload 0
invokevirtual android/media/RemoteControlClient$MetadataEditor/apply()V
return
.limit locals 2
.limit stack 2
.end method

.method private static b(Landroid/content/Context;Ljava/lang/Object;)V
aload 0
ldc "audio"
invokevirtual android/content/Context/getSystemService(Ljava/lang/String;)Ljava/lang/Object;
checkcast android/media/AudioManager
aload 1
checkcast android/media/RemoteControlClient
invokevirtual android/media/AudioManager/registerRemoteControlClient(Landroid/media/RemoteControlClient;)V
return
.limit locals 2
.limit stack 2
.end method
