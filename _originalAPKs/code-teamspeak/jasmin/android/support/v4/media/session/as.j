.bytecode 50.0
.class final synchronized android/support/v4/media/session/as
.super java/lang/Object
.implements android/os/Parcelable$Creator

.method <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a(Landroid/os/Parcel;)Landroid/support/v4/media/session/MediaSessionCompat$Token;
getstatic android/os/Build$VERSION/SDK_INT I
bipush 21
if_icmplt L0
aload 0
aconst_null
invokevirtual android/os/Parcel/readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;
astore 0
L1:
new android/support/v4/media/session/MediaSessionCompat$Token
dup
aload 0
invokespecial android/support/v4/media/session/MediaSessionCompat$Token/<init>(Ljava/lang/Object;)V
areturn
L0:
aload 0
invokevirtual android/os/Parcel/readStrongBinder()Landroid/os/IBinder;
astore 0
goto L1
.limit locals 1
.limit stack 3
.end method

.method private static a(I)[Landroid/support/v4/media/session/MediaSessionCompat$Token;
iload 0
anewarray android/support/v4/media/session/MediaSessionCompat$Token
areturn
.limit locals 1
.limit stack 1
.end method

.method public final synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
getstatic android/os/Build$VERSION/SDK_INT I
bipush 21
if_icmplt L0
aload 1
aconst_null
invokevirtual android/os/Parcel/readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;
astore 1
L1:
new android/support/v4/media/session/MediaSessionCompat$Token
dup
aload 1
invokespecial android/support/v4/media/session/MediaSessionCompat$Token/<init>(Ljava/lang/Object;)V
areturn
L0:
aload 1
invokevirtual android/os/Parcel/readStrongBinder()Landroid/os/IBinder;
astore 1
goto L1
.limit locals 2
.limit stack 3
.end method

.method public final volatile synthetic newArray(I)[Ljava/lang/Object;
iload 1
anewarray android/support/v4/media/session/MediaSessionCompat$Token
areturn
.limit locals 2
.limit stack 1
.end method
