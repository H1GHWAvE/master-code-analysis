.bytecode 50.0
.class final synchronized android/support/v4/media/r
.super java/lang/Object

.method <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a(F)Ljava/lang/Object;
fload 0
invokestatic android/media/Rating/newPercentageRating(F)Landroid/media/Rating;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static a(I)Ljava/lang/Object;
iload 0
invokestatic android/media/Rating/newUnratedRating(I)Landroid/media/Rating;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static a(IF)Ljava/lang/Object;
iload 0
fload 1
invokestatic android/media/Rating/newStarRating(IF)Landroid/media/Rating;
areturn
.limit locals 2
.limit stack 2
.end method

.method private static a(Z)Ljava/lang/Object;
iload 0
invokestatic android/media/Rating/newHeartRating(Z)Landroid/media/Rating;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static a(Ljava/lang/Object;)Z
aload 0
checkcast android/media/Rating
invokevirtual android/media/Rating/isRated()Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private static b(Ljava/lang/Object;)I
aload 0
checkcast android/media/Rating
invokevirtual android/media/Rating/getRatingStyle()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private static b(Z)Ljava/lang/Object;
iload 0
invokestatic android/media/Rating/newThumbRating(Z)Landroid/media/Rating;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static c(Ljava/lang/Object;)Z
aload 0
checkcast android/media/Rating
invokevirtual android/media/Rating/hasHeart()Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private static d(Ljava/lang/Object;)Z
aload 0
checkcast android/media/Rating
invokevirtual android/media/Rating/isThumbUp()Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private static e(Ljava/lang/Object;)F
aload 0
checkcast android/media/Rating
invokevirtual android/media/Rating/getStarRating()F
freturn
.limit locals 1
.limit stack 1
.end method

.method private static f(Ljava/lang/Object;)F
aload 0
checkcast android/media/Rating
invokevirtual android/media/Rating/getPercentRating()F
freturn
.limit locals 1
.limit stack 1
.end method
