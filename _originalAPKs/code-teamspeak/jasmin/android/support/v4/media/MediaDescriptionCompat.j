.bytecode 50.0
.class public final synchronized android/support/v4/media/MediaDescriptionCompat
.super java/lang/Object
.implements android/os/Parcelable

.field public static final 'CREATOR' Landroid/os/Parcelable$Creator;

.field private final 'a' Ljava/lang/String;

.field private final 'b' Ljava/lang/CharSequence;

.field private final 'c' Ljava/lang/CharSequence;

.field private final 'd' Ljava/lang/CharSequence;

.field private final 'e' Landroid/graphics/Bitmap;

.field private final 'f' Landroid/net/Uri;

.field private final 'g' Landroid/os/Bundle;

.field private final 'h' Landroid/net/Uri;

.field private 'i' Ljava/lang/Object;

.method static <clinit>()V
new android/support/v4/media/a
dup
invokespecial android/support/v4/media/a/<init>()V
putstatic android/support/v4/media/MediaDescriptionCompat/CREATOR Landroid/os/Parcelable$Creator;
return
.limit locals 0
.limit stack 2
.end method

.method private <init>(Landroid/os/Parcel;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
invokevirtual android/os/Parcel/readString()Ljava/lang/String;
putfield android/support/v4/media/MediaDescriptionCompat/a Ljava/lang/String;
aload 0
getstatic android/text/TextUtils/CHAR_SEQUENCE_CREATOR Landroid/os/Parcelable$Creator;
aload 1
invokeinterface android/os/Parcelable$Creator/createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object; 1
checkcast java/lang/CharSequence
putfield android/support/v4/media/MediaDescriptionCompat/b Ljava/lang/CharSequence;
aload 0
getstatic android/text/TextUtils/CHAR_SEQUENCE_CREATOR Landroid/os/Parcelable$Creator;
aload 1
invokeinterface android/os/Parcelable$Creator/createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object; 1
checkcast java/lang/CharSequence
putfield android/support/v4/media/MediaDescriptionCompat/c Ljava/lang/CharSequence;
aload 0
getstatic android/text/TextUtils/CHAR_SEQUENCE_CREATOR Landroid/os/Parcelable$Creator;
aload 1
invokeinterface android/os/Parcelable$Creator/createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object; 1
checkcast java/lang/CharSequence
putfield android/support/v4/media/MediaDescriptionCompat/d Ljava/lang/CharSequence;
aload 0
aload 1
aconst_null
invokevirtual android/os/Parcel/readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;
checkcast android/graphics/Bitmap
putfield android/support/v4/media/MediaDescriptionCompat/e Landroid/graphics/Bitmap;
aload 0
aload 1
aconst_null
invokevirtual android/os/Parcel/readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;
checkcast android/net/Uri
putfield android/support/v4/media/MediaDescriptionCompat/f Landroid/net/Uri;
aload 0
aload 1
invokevirtual android/os/Parcel/readBundle()Landroid/os/Bundle;
putfield android/support/v4/media/MediaDescriptionCompat/g Landroid/os/Bundle;
aload 0
aload 1
aconst_null
invokevirtual android/os/Parcel/readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;
checkcast android/net/Uri
putfield android/support/v4/media/MediaDescriptionCompat/h Landroid/net/Uri;
return
.limit locals 2
.limit stack 3
.end method

.method synthetic <init>(Landroid/os/Parcel;B)V
aload 0
aload 1
invokespecial android/support/v4/media/MediaDescriptionCompat/<init>(Landroid/os/Parcel;)V
return
.limit locals 3
.limit stack 2
.end method

.method private <init>(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/graphics/Bitmap;Landroid/net/Uri;Landroid/os/Bundle;Landroid/net/Uri;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
putfield android/support/v4/media/MediaDescriptionCompat/a Ljava/lang/String;
aload 0
aload 2
putfield android/support/v4/media/MediaDescriptionCompat/b Ljava/lang/CharSequence;
aload 0
aload 3
putfield android/support/v4/media/MediaDescriptionCompat/c Ljava/lang/CharSequence;
aload 0
aload 4
putfield android/support/v4/media/MediaDescriptionCompat/d Ljava/lang/CharSequence;
aload 0
aload 5
putfield android/support/v4/media/MediaDescriptionCompat/e Landroid/graphics/Bitmap;
aload 0
aload 6
putfield android/support/v4/media/MediaDescriptionCompat/f Landroid/net/Uri;
aload 0
aload 7
putfield android/support/v4/media/MediaDescriptionCompat/g Landroid/os/Bundle;
aload 0
aload 8
putfield android/support/v4/media/MediaDescriptionCompat/h Landroid/net/Uri;
return
.limit locals 9
.limit stack 2
.end method

.method synthetic <init>(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/graphics/Bitmap;Landroid/net/Uri;Landroid/os/Bundle;Landroid/net/Uri;B)V
aload 0
aload 1
aload 2
aload 3
aload 4
aload 5
aload 6
aload 7
aload 8
invokespecial android/support/v4/media/MediaDescriptionCompat/<init>(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/graphics/Bitmap;Landroid/net/Uri;Landroid/os/Bundle;Landroid/net/Uri;)V
return
.limit locals 10
.limit stack 9
.end method

.method public static a(Ljava/lang/Object;)Landroid/support/v4/media/MediaDescriptionCompat;
aload 0
ifnull L0
getstatic android/os/Build$VERSION/SDK_INT I
bipush 21
if_icmpge L1
L0:
aconst_null
areturn
L1:
new android/support/v4/media/b
dup
invokespecial android/support/v4/media/b/<init>()V
astore 1
aload 1
aload 0
checkcast android/media/MediaDescription
invokevirtual android/media/MediaDescription/getMediaId()Ljava/lang/String;
putfield android/support/v4/media/b/a Ljava/lang/String;
aload 1
aload 0
checkcast android/media/MediaDescription
invokevirtual android/media/MediaDescription/getTitle()Ljava/lang/CharSequence;
putfield android/support/v4/media/b/b Ljava/lang/CharSequence;
aload 1
aload 0
checkcast android/media/MediaDescription
invokevirtual android/media/MediaDescription/getSubtitle()Ljava/lang/CharSequence;
putfield android/support/v4/media/b/c Ljava/lang/CharSequence;
aload 1
aload 0
checkcast android/media/MediaDescription
invokevirtual android/media/MediaDescription/getDescription()Ljava/lang/CharSequence;
putfield android/support/v4/media/b/d Ljava/lang/CharSequence;
aload 1
aload 0
checkcast android/media/MediaDescription
invokevirtual android/media/MediaDescription/getIconBitmap()Landroid/graphics/Bitmap;
putfield android/support/v4/media/b/e Landroid/graphics/Bitmap;
aload 1
aload 0
checkcast android/media/MediaDescription
invokevirtual android/media/MediaDescription/getIconUri()Landroid/net/Uri;
putfield android/support/v4/media/b/f Landroid/net/Uri;
aload 1
aload 0
checkcast android/media/MediaDescription
invokevirtual android/media/MediaDescription/getExtras()Landroid/os/Bundle;
putfield android/support/v4/media/b/g Landroid/os/Bundle;
getstatic android/os/Build$VERSION/SDK_INT I
bipush 23
if_icmplt L2
aload 1
aload 0
checkcast android/media/MediaDescription
invokevirtual android/media/MediaDescription/getMediaUri()Landroid/net/Uri;
putfield android/support/v4/media/b/h Landroid/net/Uri;
L2:
aload 1
invokevirtual android/support/v4/media/b/a()Landroid/support/v4/media/MediaDescriptionCompat;
astore 1
aload 1
aload 0
putfield android/support/v4/media/MediaDescriptionCompat/i Ljava/lang/Object;
aload 1
areturn
.limit locals 2
.limit stack 2
.end method

.method private b()Ljava/lang/String;
.annotation invisible Landroid/support/a/z;
.end annotation
aload 0
getfield android/support/v4/media/MediaDescriptionCompat/a Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method private c()Ljava/lang/CharSequence;
.annotation invisible Landroid/support/a/z;
.end annotation
aload 0
getfield android/support/v4/media/MediaDescriptionCompat/b Ljava/lang/CharSequence;
areturn
.limit locals 1
.limit stack 1
.end method

.method private d()Ljava/lang/CharSequence;
.annotation invisible Landroid/support/a/z;
.end annotation
aload 0
getfield android/support/v4/media/MediaDescriptionCompat/c Ljava/lang/CharSequence;
areturn
.limit locals 1
.limit stack 1
.end method

.method private e()Ljava/lang/CharSequence;
.annotation invisible Landroid/support/a/z;
.end annotation
aload 0
getfield android/support/v4/media/MediaDescriptionCompat/d Ljava/lang/CharSequence;
areturn
.limit locals 1
.limit stack 1
.end method

.method private f()Landroid/graphics/Bitmap;
.annotation invisible Landroid/support/a/z;
.end annotation
aload 0
getfield android/support/v4/media/MediaDescriptionCompat/e Landroid/graphics/Bitmap;
areturn
.limit locals 1
.limit stack 1
.end method

.method private g()Landroid/net/Uri;
.annotation invisible Landroid/support/a/z;
.end annotation
aload 0
getfield android/support/v4/media/MediaDescriptionCompat/f Landroid/net/Uri;
areturn
.limit locals 1
.limit stack 1
.end method

.method private h()Landroid/os/Bundle;
.annotation invisible Landroid/support/a/z;
.end annotation
aload 0
getfield android/support/v4/media/MediaDescriptionCompat/g Landroid/os/Bundle;
areturn
.limit locals 1
.limit stack 1
.end method

.method private i()Landroid/net/Uri;
.annotation invisible Landroid/support/a/z;
.end annotation
aload 0
getfield android/support/v4/media/MediaDescriptionCompat/h Landroid/net/Uri;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final a()Ljava/lang/Object;
aload 0
getfield android/support/v4/media/MediaDescriptionCompat/i Ljava/lang/Object;
ifnonnull L0
getstatic android/os/Build$VERSION/SDK_INT I
bipush 21
if_icmpge L1
L0:
aload 0
getfield android/support/v4/media/MediaDescriptionCompat/i Ljava/lang/Object;
areturn
L1:
new android/media/MediaDescription$Builder
dup
invokespecial android/media/MediaDescription$Builder/<init>()V
astore 1
aload 0
getfield android/support/v4/media/MediaDescriptionCompat/a Ljava/lang/String;
astore 2
aload 1
checkcast android/media/MediaDescription$Builder
aload 2
invokevirtual android/media/MediaDescription$Builder/setMediaId(Ljava/lang/String;)Landroid/media/MediaDescription$Builder;
pop
aload 0
getfield android/support/v4/media/MediaDescriptionCompat/b Ljava/lang/CharSequence;
astore 2
aload 1
checkcast android/media/MediaDescription$Builder
aload 2
invokevirtual android/media/MediaDescription$Builder/setTitle(Ljava/lang/CharSequence;)Landroid/media/MediaDescription$Builder;
pop
aload 0
getfield android/support/v4/media/MediaDescriptionCompat/c Ljava/lang/CharSequence;
astore 2
aload 1
checkcast android/media/MediaDescription$Builder
aload 2
invokevirtual android/media/MediaDescription$Builder/setSubtitle(Ljava/lang/CharSequence;)Landroid/media/MediaDescription$Builder;
pop
aload 0
getfield android/support/v4/media/MediaDescriptionCompat/d Ljava/lang/CharSequence;
astore 2
aload 1
checkcast android/media/MediaDescription$Builder
aload 2
invokevirtual android/media/MediaDescription$Builder/setDescription(Ljava/lang/CharSequence;)Landroid/media/MediaDescription$Builder;
pop
aload 0
getfield android/support/v4/media/MediaDescriptionCompat/e Landroid/graphics/Bitmap;
astore 2
aload 1
checkcast android/media/MediaDescription$Builder
aload 2
invokevirtual android/media/MediaDescription$Builder/setIconBitmap(Landroid/graphics/Bitmap;)Landroid/media/MediaDescription$Builder;
pop
aload 0
getfield android/support/v4/media/MediaDescriptionCompat/f Landroid/net/Uri;
astore 2
aload 1
checkcast android/media/MediaDescription$Builder
aload 2
invokevirtual android/media/MediaDescription$Builder/setIconUri(Landroid/net/Uri;)Landroid/media/MediaDescription$Builder;
pop
aload 0
getfield android/support/v4/media/MediaDescriptionCompat/g Landroid/os/Bundle;
astore 2
aload 1
checkcast android/media/MediaDescription$Builder
aload 2
invokevirtual android/media/MediaDescription$Builder/setExtras(Landroid/os/Bundle;)Landroid/media/MediaDescription$Builder;
pop
getstatic android/os/Build$VERSION/SDK_INT I
bipush 23
if_icmplt L2
aload 0
getfield android/support/v4/media/MediaDescriptionCompat/h Landroid/net/Uri;
astore 2
aload 1
checkcast android/media/MediaDescription$Builder
aload 2
invokevirtual android/media/MediaDescription$Builder/setMediaUri(Landroid/net/Uri;)Landroid/media/MediaDescription$Builder;
pop
L2:
aload 0
aload 1
checkcast android/media/MediaDescription$Builder
invokevirtual android/media/MediaDescription$Builder/build()Landroid/media/MediaDescription;
putfield android/support/v4/media/MediaDescriptionCompat/i Ljava/lang/Object;
aload 0
getfield android/support/v4/media/MediaDescriptionCompat/i Ljava/lang/Object;
areturn
.limit locals 3
.limit stack 2
.end method

.method public final describeContents()I
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final toString()Ljava/lang/String;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
getfield android/support/v4/media/MediaDescriptionCompat/b Ljava/lang/CharSequence;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc ", "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield android/support/v4/media/MediaDescriptionCompat/c Ljava/lang/CharSequence;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc ", "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield android/support/v4/media/MediaDescriptionCompat/d Ljava/lang/CharSequence;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 2
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
getstatic android/os/Build$VERSION/SDK_INT I
bipush 21
if_icmpge L0
aload 1
aload 0
getfield android/support/v4/media/MediaDescriptionCompat/a Ljava/lang/String;
invokevirtual android/os/Parcel/writeString(Ljava/lang/String;)V
aload 0
getfield android/support/v4/media/MediaDescriptionCompat/b Ljava/lang/CharSequence;
aload 1
iload 2
invokestatic android/text/TextUtils/writeToParcel(Ljava/lang/CharSequence;Landroid/os/Parcel;I)V
aload 0
getfield android/support/v4/media/MediaDescriptionCompat/c Ljava/lang/CharSequence;
aload 1
iload 2
invokestatic android/text/TextUtils/writeToParcel(Ljava/lang/CharSequence;Landroid/os/Parcel;I)V
aload 0
getfield android/support/v4/media/MediaDescriptionCompat/d Ljava/lang/CharSequence;
aload 1
iload 2
invokestatic android/text/TextUtils/writeToParcel(Ljava/lang/CharSequence;Landroid/os/Parcel;I)V
aload 1
aload 0
getfield android/support/v4/media/MediaDescriptionCompat/e Landroid/graphics/Bitmap;
iload 2
invokevirtual android/os/Parcel/writeParcelable(Landroid/os/Parcelable;I)V
aload 1
aload 0
getfield android/support/v4/media/MediaDescriptionCompat/f Landroid/net/Uri;
iload 2
invokevirtual android/os/Parcel/writeParcelable(Landroid/os/Parcelable;I)V
aload 1
aload 0
getfield android/support/v4/media/MediaDescriptionCompat/g Landroid/os/Bundle;
invokevirtual android/os/Parcel/writeBundle(Landroid/os/Bundle;)V
return
L0:
aload 0
invokevirtual android/support/v4/media/MediaDescriptionCompat/a()Ljava/lang/Object;
checkcast android/media/MediaDescription
aload 1
iload 2
invokevirtual android/media/MediaDescription/writeToParcel(Landroid/os/Parcel;I)V
return
.limit locals 3
.limit stack 3
.end method
