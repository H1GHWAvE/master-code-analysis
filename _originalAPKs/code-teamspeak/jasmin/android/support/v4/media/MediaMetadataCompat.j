.bytecode 50.0
.class public final synchronized android/support/v4/media/MediaMetadataCompat
.super java/lang/Object
.implements android/os/Parcelable

.field public static final 'A' Ljava/lang/String; = "android.media.metadata.MEDIA_ID"

.field public static final 'B' Landroid/support/v4/n/a;

.field public static final 'CREATOR' Landroid/os/Parcelable$Creator;

.field private static final 'E' Ljava/lang/String; = "MediaMetadata"

.field private static final 'F' I = 0


.field private static final 'G' I = 1


.field private static final 'H' I = 2


.field private static final 'I' I = 3


.field private static final 'J' [Ljava/lang/String;

.field private static final 'K' [Ljava/lang/String;

.field private static final 'L' [Ljava/lang/String;

.field public static final 'a' Ljava/lang/String; = "android.media.metadata.TITLE"

.field public static final 'b' Ljava/lang/String; = "android.media.metadata.ARTIST"

.field public static final 'c' Ljava/lang/String; = "android.media.metadata.DURATION"

.field public static final 'd' Ljava/lang/String; = "android.media.metadata.ALBUM"

.field public static final 'e' Ljava/lang/String; = "android.media.metadata.AUTHOR"

.field public static final 'f' Ljava/lang/String; = "android.media.metadata.WRITER"

.field public static final 'g' Ljava/lang/String; = "android.media.metadata.COMPOSER"

.field public static final 'h' Ljava/lang/String; = "android.media.metadata.COMPILATION"

.field public static final 'i' Ljava/lang/String; = "android.media.metadata.DATE"

.field public static final 'j' Ljava/lang/String; = "android.media.metadata.YEAR"

.field public static final 'k' Ljava/lang/String; = "android.media.metadata.GENRE"

.field public static final 'l' Ljava/lang/String; = "android.media.metadata.TRACK_NUMBER"

.field public static final 'm' Ljava/lang/String; = "android.media.metadata.NUM_TRACKS"

.field public static final 'n' Ljava/lang/String; = "android.media.metadata.DISC_NUMBER"

.field public static final 'o' Ljava/lang/String; = "android.media.metadata.ALBUM_ARTIST"

.field public static final 'p' Ljava/lang/String; = "android.media.metadata.ART"

.field public static final 'q' Ljava/lang/String; = "android.media.metadata.ART_URI"

.field public static final 'r' Ljava/lang/String; = "android.media.metadata.ALBUM_ART"

.field public static final 's' Ljava/lang/String; = "android.media.metadata.ALBUM_ART_URI"

.field public static final 't' Ljava/lang/String; = "android.media.metadata.USER_RATING"

.field public static final 'u' Ljava/lang/String; = "android.media.metadata.RATING"

.field public static final 'v' Ljava/lang/String; = "android.media.metadata.DISPLAY_TITLE"

.field public static final 'w' Ljava/lang/String; = "android.media.metadata.DISPLAY_SUBTITLE"

.field public static final 'x' Ljava/lang/String; = "android.media.metadata.DISPLAY_DESCRIPTION"

.field public static final 'y' Ljava/lang/String; = "android.media.metadata.DISPLAY_ICON"

.field public static final 'z' Ljava/lang/String; = "android.media.metadata.DISPLAY_ICON_URI"

.field public final 'C' Landroid/os/Bundle;

.field public 'D' Ljava/lang/Object;

.field private 'M' Landroid/support/v4/media/MediaDescriptionCompat;

.method static <clinit>()V
new android/support/v4/n/a
dup
invokespecial android/support/v4/n/a/<init>()V
astore 0
aload 0
putstatic android/support/v4/media/MediaMetadataCompat/B Landroid/support/v4/n/a;
aload 0
ldc "android.media.metadata.TITLE"
iconst_1
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual android/support/v4/n/a/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic android/support/v4/media/MediaMetadataCompat/B Landroid/support/v4/n/a;
ldc "android.media.metadata.ARTIST"
iconst_1
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual android/support/v4/n/a/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic android/support/v4/media/MediaMetadataCompat/B Landroid/support/v4/n/a;
ldc "android.media.metadata.DURATION"
iconst_0
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual android/support/v4/n/a/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic android/support/v4/media/MediaMetadataCompat/B Landroid/support/v4/n/a;
ldc "android.media.metadata.ALBUM"
iconst_1
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual android/support/v4/n/a/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic android/support/v4/media/MediaMetadataCompat/B Landroid/support/v4/n/a;
ldc "android.media.metadata.AUTHOR"
iconst_1
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual android/support/v4/n/a/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic android/support/v4/media/MediaMetadataCompat/B Landroid/support/v4/n/a;
ldc "android.media.metadata.WRITER"
iconst_1
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual android/support/v4/n/a/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic android/support/v4/media/MediaMetadataCompat/B Landroid/support/v4/n/a;
ldc "android.media.metadata.COMPOSER"
iconst_1
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual android/support/v4/n/a/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic android/support/v4/media/MediaMetadataCompat/B Landroid/support/v4/n/a;
ldc "android.media.metadata.COMPILATION"
iconst_1
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual android/support/v4/n/a/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic android/support/v4/media/MediaMetadataCompat/B Landroid/support/v4/n/a;
ldc "android.media.metadata.DATE"
iconst_1
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual android/support/v4/n/a/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic android/support/v4/media/MediaMetadataCompat/B Landroid/support/v4/n/a;
ldc "android.media.metadata.YEAR"
iconst_0
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual android/support/v4/n/a/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic android/support/v4/media/MediaMetadataCompat/B Landroid/support/v4/n/a;
ldc "android.media.metadata.GENRE"
iconst_1
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual android/support/v4/n/a/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic android/support/v4/media/MediaMetadataCompat/B Landroid/support/v4/n/a;
ldc "android.media.metadata.TRACK_NUMBER"
iconst_0
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual android/support/v4/n/a/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic android/support/v4/media/MediaMetadataCompat/B Landroid/support/v4/n/a;
ldc "android.media.metadata.NUM_TRACKS"
iconst_0
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual android/support/v4/n/a/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic android/support/v4/media/MediaMetadataCompat/B Landroid/support/v4/n/a;
ldc "android.media.metadata.DISC_NUMBER"
iconst_0
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual android/support/v4/n/a/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic android/support/v4/media/MediaMetadataCompat/B Landroid/support/v4/n/a;
ldc "android.media.metadata.ALBUM_ARTIST"
iconst_1
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual android/support/v4/n/a/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic android/support/v4/media/MediaMetadataCompat/B Landroid/support/v4/n/a;
ldc "android.media.metadata.ART"
iconst_2
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual android/support/v4/n/a/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic android/support/v4/media/MediaMetadataCompat/B Landroid/support/v4/n/a;
ldc "android.media.metadata.ART_URI"
iconst_1
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual android/support/v4/n/a/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic android/support/v4/media/MediaMetadataCompat/B Landroid/support/v4/n/a;
ldc "android.media.metadata.ALBUM_ART"
iconst_2
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual android/support/v4/n/a/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic android/support/v4/media/MediaMetadataCompat/B Landroid/support/v4/n/a;
ldc "android.media.metadata.ALBUM_ART_URI"
iconst_1
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual android/support/v4/n/a/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic android/support/v4/media/MediaMetadataCompat/B Landroid/support/v4/n/a;
ldc "android.media.metadata.USER_RATING"
iconst_3
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual android/support/v4/n/a/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic android/support/v4/media/MediaMetadataCompat/B Landroid/support/v4/n/a;
ldc "android.media.metadata.RATING"
iconst_3
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual android/support/v4/n/a/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic android/support/v4/media/MediaMetadataCompat/B Landroid/support/v4/n/a;
ldc "android.media.metadata.DISPLAY_TITLE"
iconst_1
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual android/support/v4/n/a/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic android/support/v4/media/MediaMetadataCompat/B Landroid/support/v4/n/a;
ldc "android.media.metadata.DISPLAY_SUBTITLE"
iconst_1
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual android/support/v4/n/a/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic android/support/v4/media/MediaMetadataCompat/B Landroid/support/v4/n/a;
ldc "android.media.metadata.DISPLAY_DESCRIPTION"
iconst_1
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual android/support/v4/n/a/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic android/support/v4/media/MediaMetadataCompat/B Landroid/support/v4/n/a;
ldc "android.media.metadata.DISPLAY_ICON"
iconst_2
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual android/support/v4/n/a/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic android/support/v4/media/MediaMetadataCompat/B Landroid/support/v4/n/a;
ldc "android.media.metadata.DISPLAY_ICON_URI"
iconst_1
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual android/support/v4/n/a/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic android/support/v4/media/MediaMetadataCompat/B Landroid/support/v4/n/a;
ldc "android.media.metadata.MEDIA_ID"
iconst_1
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual android/support/v4/n/a/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
bipush 7
anewarray java/lang/String
dup
iconst_0
ldc "android.media.metadata.TITLE"
aastore
dup
iconst_1
ldc "android.media.metadata.ARTIST"
aastore
dup
iconst_2
ldc "android.media.metadata.ALBUM"
aastore
dup
iconst_3
ldc "android.media.metadata.ALBUM_ARTIST"
aastore
dup
iconst_4
ldc "android.media.metadata.WRITER"
aastore
dup
iconst_5
ldc "android.media.metadata.AUTHOR"
aastore
dup
bipush 6
ldc "android.media.metadata.COMPOSER"
aastore
putstatic android/support/v4/media/MediaMetadataCompat/J [Ljava/lang/String;
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "android.media.metadata.DISPLAY_ICON"
aastore
dup
iconst_1
ldc "android.media.metadata.ART"
aastore
dup
iconst_2
ldc "android.media.metadata.ALBUM_ART"
aastore
putstatic android/support/v4/media/MediaMetadataCompat/K [Ljava/lang/String;
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "android.media.metadata.DISPLAY_ICON_URI"
aastore
dup
iconst_1
ldc "android.media.metadata.ART_URI"
aastore
dup
iconst_2
ldc "android.media.metadata.ALBUM_ART_URI"
aastore
putstatic android/support/v4/media/MediaMetadataCompat/L [Ljava/lang/String;
new android/support/v4/media/g
dup
invokespecial android/support/v4/media/g/<init>()V
putstatic android/support/v4/media/MediaMetadataCompat/CREATOR Landroid/os/Parcelable$Creator;
return
.limit locals 1
.limit stack 4
.end method

.method private <init>(Landroid/os/Bundle;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
new android/os/Bundle
dup
aload 1
invokespecial android/os/Bundle/<init>(Landroid/os/Bundle;)V
putfield android/support/v4/media/MediaMetadataCompat/C Landroid/os/Bundle;
return
.limit locals 2
.limit stack 4
.end method

.method synthetic <init>(Landroid/os/Bundle;B)V
aload 0
aload 1
invokespecial android/support/v4/media/MediaMetadataCompat/<init>(Landroid/os/Bundle;)V
return
.limit locals 3
.limit stack 2
.end method

.method private <init>(Landroid/os/Parcel;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
invokevirtual android/os/Parcel/readBundle()Landroid/os/Bundle;
putfield android/support/v4/media/MediaMetadataCompat/C Landroid/os/Bundle;
return
.limit locals 2
.limit stack 2
.end method

.method synthetic <init>(Landroid/os/Parcel;B)V
aload 0
aload 1
invokespecial android/support/v4/media/MediaMetadataCompat/<init>(Landroid/os/Parcel;)V
return
.limit locals 3
.limit stack 2
.end method

.method static synthetic a(Landroid/support/v4/media/MediaMetadataCompat;)Landroid/os/Bundle;
aload 0
getfield android/support/v4/media/MediaMetadataCompat/C Landroid/os/Bundle;
areturn
.limit locals 1
.limit stack 1
.end method

.method public static a(Ljava/lang/Object;)Landroid/support/v4/media/MediaMetadataCompat;
aload 0
ifnull L0
getstatic android/os/Build$VERSION/SDK_INT I
bipush 21
if_icmpge L1
L0:
aconst_null
areturn
L1:
new android/support/v4/media/i
dup
invokespecial android/support/v4/media/i/<init>()V
astore 3
aload 0
checkcast android/media/MediaMetadata
invokevirtual android/media/MediaMetadata/keySet()Ljava/util/Set;
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
astore 4
L2:
aload 4
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L3
aload 4
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/lang/String
astore 5
getstatic android/support/v4/media/MediaMetadataCompat/B Landroid/support/v4/n/a;
aload 5
invokevirtual android/support/v4/n/a/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/Integer
astore 6
aload 6
ifnull L2
aload 6
invokevirtual java/lang/Integer/intValue()I
tableswitch 0
L4
L5
L6
L7
default : L8
L8:
goto L2
L4:
aload 0
checkcast android/media/MediaMetadata
aload 5
invokevirtual android/media/MediaMetadata/getLong(Ljava/lang/String;)J
lstore 1
getstatic android/support/v4/media/MediaMetadataCompat/B Landroid/support/v4/n/a;
aload 5
invokevirtual android/support/v4/n/a/containsKey(Ljava/lang/Object;)Z
ifeq L9
getstatic android/support/v4/media/MediaMetadataCompat/B Landroid/support/v4/n/a;
aload 5
invokevirtual android/support/v4/n/a/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/Integer
invokevirtual java/lang/Integer/intValue()I
ifeq L9
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
ldc "The "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 5
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " key cannot be used to put a long"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L6:
aload 0
checkcast android/media/MediaMetadata
aload 5
invokevirtual android/media/MediaMetadata/getBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;
astore 6
getstatic android/support/v4/media/MediaMetadataCompat/B Landroid/support/v4/n/a;
aload 5
invokevirtual android/support/v4/n/a/containsKey(Ljava/lang/Object;)Z
ifeq L10
getstatic android/support/v4/media/MediaMetadataCompat/B Landroid/support/v4/n/a;
aload 5
invokevirtual android/support/v4/n/a/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/Integer
invokevirtual java/lang/Integer/intValue()I
iconst_2
if_icmpeq L10
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
ldc "The "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 5
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " key cannot be used to put a Bitmap"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L10:
aload 3
getfield android/support/v4/media/i/a Landroid/os/Bundle;
aload 5
aload 6
invokevirtual android/os/Bundle/putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V
goto L2
L9:
aload 3
getfield android/support/v4/media/i/a Landroid/os/Bundle;
aload 5
lload 1
invokevirtual android/os/Bundle/putLong(Ljava/lang/String;J)V
goto L2
L7:
aload 0
checkcast android/media/MediaMetadata
aload 5
invokevirtual android/media/MediaMetadata/getRating(Ljava/lang/String;)Landroid/media/Rating;
invokestatic android/support/v4/media/RatingCompat/a(Ljava/lang/Object;)Landroid/support/v4/media/RatingCompat;
astore 6
getstatic android/support/v4/media/MediaMetadataCompat/B Landroid/support/v4/n/a;
aload 5
invokevirtual android/support/v4/n/a/containsKey(Ljava/lang/Object;)Z
ifeq L11
getstatic android/support/v4/media/MediaMetadataCompat/B Landroid/support/v4/n/a;
aload 5
invokevirtual android/support/v4/n/a/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/Integer
invokevirtual java/lang/Integer/intValue()I
iconst_3
if_icmpeq L11
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
ldc "The "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 5
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " key cannot be used to put a Rating"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L11:
aload 3
getfield android/support/v4/media/i/a Landroid/os/Bundle;
aload 5
aload 6
invokevirtual android/os/Bundle/putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V
goto L2
L5:
aload 0
checkcast android/media/MediaMetadata
aload 5
invokevirtual android/media/MediaMetadata/getText(Ljava/lang/String;)Ljava/lang/CharSequence;
astore 6
getstatic android/support/v4/media/MediaMetadataCompat/B Landroid/support/v4/n/a;
aload 5
invokevirtual android/support/v4/n/a/containsKey(Ljava/lang/Object;)Z
ifeq L12
getstatic android/support/v4/media/MediaMetadataCompat/B Landroid/support/v4/n/a;
aload 5
invokevirtual android/support/v4/n/a/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/Integer
invokevirtual java/lang/Integer/intValue()I
iconst_1
if_icmpeq L12
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
ldc "The "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 5
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " key cannot be used to put a CharSequence"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L12:
aload 3
getfield android/support/v4/media/i/a Landroid/os/Bundle;
aload 5
aload 6
invokevirtual android/os/Bundle/putCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)V
goto L2
L3:
new android/support/v4/media/MediaMetadataCompat
dup
aload 3
getfield android/support/v4/media/i/a Landroid/os/Bundle;
iconst_0
invokespecial android/support/v4/media/MediaMetadataCompat/<init>(Landroid/os/Bundle;B)V
astore 3
aload 3
aload 0
putfield android/support/v4/media/MediaMetadataCompat/D Ljava/lang/Object;
aload 3
areturn
.limit locals 7
.limit stack 5
.end method

.method static synthetic a()Landroid/support/v4/n/a;
getstatic android/support/v4/media/MediaMetadataCompat/B Landroid/support/v4/n/a;
areturn
.limit locals 0
.limit stack 1
.end method

.method private b()Landroid/support/v4/media/MediaDescriptionCompat;
aconst_null
astore 6
aload 0
getfield android/support/v4/media/MediaMetadataCompat/M Landroid/support/v4/media/MediaDescriptionCompat;
ifnull L0
aload 0
getfield android/support/v4/media/MediaMetadataCompat/M Landroid/support/v4/media/MediaDescriptionCompat;
areturn
L0:
aload 0
ldc "android.media.metadata.MEDIA_ID"
invokespecial android/support/v4/media/MediaMetadataCompat/f(Ljava/lang/String;)Ljava/lang/String;
astore 7
iconst_3
anewarray java/lang/CharSequence
astore 8
aload 0
ldc "android.media.metadata.DISPLAY_TITLE"
invokevirtual android/support/v4/media/MediaMetadataCompat/a(Ljava/lang/String;)Ljava/lang/CharSequence;
astore 4
aload 4
invokestatic android/text/TextUtils/isEmpty(Ljava/lang/CharSequence;)Z
ifne L1
aload 8
iconst_0
aload 4
aastore
aload 8
iconst_1
aload 0
ldc "android.media.metadata.DISPLAY_SUBTITLE"
invokevirtual android/support/v4/media/MediaMetadataCompat/a(Ljava/lang/String;)Ljava/lang/CharSequence;
aastore
aload 8
iconst_2
aload 0
ldc "android.media.metadata.DISPLAY_DESCRIPTION"
invokevirtual android/support/v4/media/MediaMetadataCompat/a(Ljava/lang/String;)Ljava/lang/CharSequence;
aastore
L2:
iconst_0
istore 1
L3:
iload 1
getstatic android/support/v4/media/MediaMetadataCompat/K [Ljava/lang/String;
arraylength
if_icmpge L4
aload 0
getstatic android/support/v4/media/MediaMetadataCompat/K [Ljava/lang/String;
iload 1
aaload
invokevirtual android/support/v4/media/MediaMetadataCompat/d(Ljava/lang/String;)Landroid/graphics/Bitmap;
astore 4
aload 4
ifnull L5
L6:
iconst_0
istore 1
L7:
aload 6
astore 5
iload 1
getstatic android/support/v4/media/MediaMetadataCompat/L [Ljava/lang/String;
arraylength
if_icmpge L8
aload 0
getstatic android/support/v4/media/MediaMetadataCompat/L [Ljava/lang/String;
iload 1
aaload
invokespecial android/support/v4/media/MediaMetadataCompat/f(Ljava/lang/String;)Ljava/lang/String;
astore 5
aload 5
invokestatic android/text/TextUtils/isEmpty(Ljava/lang/CharSequence;)Z
ifne L9
aload 5
invokestatic android/net/Uri/parse(Ljava/lang/String;)Landroid/net/Uri;
astore 5
L8:
new android/support/v4/media/b
dup
invokespecial android/support/v4/media/b/<init>()V
astore 6
aload 6
aload 7
putfield android/support/v4/media/b/a Ljava/lang/String;
aload 6
aload 8
iconst_0
aaload
putfield android/support/v4/media/b/b Ljava/lang/CharSequence;
aload 6
aload 8
iconst_1
aaload
putfield android/support/v4/media/b/c Ljava/lang/CharSequence;
aload 6
aload 8
iconst_2
aaload
putfield android/support/v4/media/b/d Ljava/lang/CharSequence;
aload 6
aload 4
putfield android/support/v4/media/b/e Landroid/graphics/Bitmap;
aload 6
aload 5
putfield android/support/v4/media/b/f Landroid/net/Uri;
aload 0
aload 6
invokevirtual android/support/v4/media/b/a()Landroid/support/v4/media/MediaDescriptionCompat;
putfield android/support/v4/media/MediaMetadataCompat/M Landroid/support/v4/media/MediaDescriptionCompat;
aload 0
getfield android/support/v4/media/MediaMetadataCompat/M Landroid/support/v4/media/MediaDescriptionCompat;
areturn
L1:
iconst_0
istore 2
iconst_0
istore 1
L10:
iload 1
iconst_3
if_icmpge L2
iload 2
getstatic android/support/v4/media/MediaMetadataCompat/J [Ljava/lang/String;
arraylength
if_icmpge L2
aload 0
getstatic android/support/v4/media/MediaMetadataCompat/J [Ljava/lang/String;
iload 2
aaload
invokevirtual android/support/v4/media/MediaMetadataCompat/a(Ljava/lang/String;)Ljava/lang/CharSequence;
astore 4
aload 4
invokestatic android/text/TextUtils/isEmpty(Ljava/lang/CharSequence;)Z
ifne L11
iload 1
iconst_1
iadd
istore 3
aload 8
iload 1
aload 4
aastore
iload 3
istore 1
L12:
iload 2
iconst_1
iadd
istore 2
goto L10
L5:
iload 1
iconst_1
iadd
istore 1
goto L3
L9:
iload 1
iconst_1
iadd
istore 1
goto L7
L4:
aconst_null
astore 4
goto L6
L11:
goto L12
.limit locals 9
.limit stack 4
.end method

.method private c()I
aload 0
getfield android/support/v4/media/MediaMetadataCompat/C Landroid/os/Bundle;
invokevirtual android/os/Bundle/size()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private d()Ljava/util/Set;
aload 0
getfield android/support/v4/media/MediaMetadataCompat/C Landroid/os/Bundle;
invokevirtual android/os/Bundle/keySet()Ljava/util/Set;
areturn
.limit locals 1
.limit stack 1
.end method

.method private e()Landroid/os/Bundle;
aload 0
getfield android/support/v4/media/MediaMetadataCompat/C Landroid/os/Bundle;
areturn
.limit locals 1
.limit stack 1
.end method

.method private e(Ljava/lang/String;)Z
aload 0
getfield android/support/v4/media/MediaMetadataCompat/C Landroid/os/Bundle;
aload 1
invokevirtual android/os/Bundle/containsKey(Ljava/lang/String;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method private f()Ljava/lang/Object;
aload 0
getfield android/support/v4/media/MediaMetadataCompat/D Ljava/lang/Object;
ifnonnull L0
getstatic android/os/Build$VERSION/SDK_INT I
bipush 21
if_icmpge L1
L0:
aload 0
getfield android/support/v4/media/MediaMetadataCompat/D Ljava/lang/Object;
areturn
L1:
new android/media/MediaMetadata$Builder
dup
invokespecial android/media/MediaMetadata$Builder/<init>()V
astore 3
aload 0
getfield android/support/v4/media/MediaMetadataCompat/C Landroid/os/Bundle;
invokevirtual android/os/Bundle/keySet()Ljava/util/Set;
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
astore 4
L2:
aload 4
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L3
aload 4
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/lang/String
astore 5
getstatic android/support/v4/media/MediaMetadataCompat/B Landroid/support/v4/n/a;
aload 5
invokevirtual android/support/v4/n/a/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/Integer
astore 6
aload 6
ifnull L2
aload 6
invokevirtual java/lang/Integer/intValue()I
tableswitch 0
L4
L5
L6
L7
default : L8
L8:
goto L2
L4:
aload 0
aload 5
invokevirtual android/support/v4/media/MediaMetadataCompat/b(Ljava/lang/String;)J
lstore 1
aload 3
checkcast android/media/MediaMetadata$Builder
aload 5
lload 1
invokevirtual android/media/MediaMetadata$Builder/putLong(Ljava/lang/String;J)Landroid/media/MediaMetadata$Builder;
pop
goto L2
L6:
aload 0
aload 5
invokevirtual android/support/v4/media/MediaMetadataCompat/d(Ljava/lang/String;)Landroid/graphics/Bitmap;
astore 6
aload 3
checkcast android/media/MediaMetadata$Builder
aload 5
aload 6
invokevirtual android/media/MediaMetadata$Builder/putBitmap(Ljava/lang/String;Landroid/graphics/Bitmap;)Landroid/media/MediaMetadata$Builder;
pop
goto L2
L7:
aload 0
aload 5
invokevirtual android/support/v4/media/MediaMetadataCompat/c(Ljava/lang/String;)Landroid/support/v4/media/RatingCompat;
invokevirtual android/support/v4/media/RatingCompat/a()Ljava/lang/Object;
astore 6
aload 3
checkcast android/media/MediaMetadata$Builder
aload 5
aload 6
checkcast android/media/Rating
invokevirtual android/media/MediaMetadata$Builder/putRating(Ljava/lang/String;Landroid/media/Rating;)Landroid/media/MediaMetadata$Builder;
pop
goto L2
L5:
aload 0
aload 5
invokevirtual android/support/v4/media/MediaMetadataCompat/a(Ljava/lang/String;)Ljava/lang/CharSequence;
astore 6
aload 3
checkcast android/media/MediaMetadata$Builder
aload 5
aload 6
invokevirtual android/media/MediaMetadata$Builder/putText(Ljava/lang/String;Ljava/lang/CharSequence;)Landroid/media/MediaMetadata$Builder;
pop
goto L2
L3:
aload 0
aload 3
checkcast android/media/MediaMetadata$Builder
invokevirtual android/media/MediaMetadata$Builder/build()Landroid/media/MediaMetadata;
putfield android/support/v4/media/MediaMetadataCompat/D Ljava/lang/Object;
aload 0
getfield android/support/v4/media/MediaMetadataCompat/D Ljava/lang/Object;
areturn
.limit locals 7
.limit stack 4
.end method

.method private f(Ljava/lang/String;)Ljava/lang/String;
aload 0
getfield android/support/v4/media/MediaMetadataCompat/C Landroid/os/Bundle;
aload 1
invokevirtual android/os/Bundle/getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;
astore 1
aload 1
ifnull L0
aload 1
invokeinterface java/lang/CharSequence/toString()Ljava/lang/String; 0
areturn
L0:
aconst_null
areturn
.limit locals 2
.limit stack 2
.end method

.method public final a(Ljava/lang/String;)Ljava/lang/CharSequence;
aload 0
getfield android/support/v4/media/MediaMetadataCompat/C Landroid/os/Bundle;
aload 1
invokevirtual android/os/Bundle/getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;
areturn
.limit locals 2
.limit stack 2
.end method

.method public final b(Ljava/lang/String;)J
aload 0
getfield android/support/v4/media/MediaMetadataCompat/C Landroid/os/Bundle;
aload 1
lconst_0
invokevirtual android/os/Bundle/getLong(Ljava/lang/String;J)J
lreturn
.limit locals 2
.limit stack 4
.end method

.method public final c(Ljava/lang/String;)Landroid/support/v4/media/RatingCompat;
.catch java/lang/Exception from L0 to L1 using L2
L0:
aload 0
getfield android/support/v4/media/MediaMetadataCompat/C Landroid/os/Bundle;
aload 1
invokevirtual android/os/Bundle/getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;
checkcast android/support/v4/media/RatingCompat
astore 1
L1:
aload 1
areturn
L2:
astore 1
ldc "MediaMetadata"
ldc "Failed to retrieve a key as Rating."
aload 1
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
aconst_null
areturn
.limit locals 2
.limit stack 3
.end method

.method public final d(Ljava/lang/String;)Landroid/graphics/Bitmap;
.catch java/lang/Exception from L0 to L1 using L2
L0:
aload 0
getfield android/support/v4/media/MediaMetadataCompat/C Landroid/os/Bundle;
aload 1
invokevirtual android/os/Bundle/getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;
checkcast android/graphics/Bitmap
astore 1
L1:
aload 1
areturn
L2:
astore 1
ldc "MediaMetadata"
ldc "Failed to retrieve a key as Bitmap."
aload 1
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
aconst_null
areturn
.limit locals 2
.limit stack 3
.end method

.method public final describeContents()I
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
aload 1
aload 0
getfield android/support/v4/media/MediaMetadataCompat/C Landroid/os/Bundle;
invokevirtual android/os/Parcel/writeBundle(Landroid/os/Bundle;)V
return
.limit locals 3
.limit stack 2
.end method
