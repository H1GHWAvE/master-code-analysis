.bytecode 50.0
.class public final synchronized android/support/v4/media/b
.super java/lang/Object

.field 'a' Ljava/lang/String;

.field 'b' Ljava/lang/CharSequence;

.field 'c' Ljava/lang/CharSequence;

.field 'd' Ljava/lang/CharSequence;

.field 'e' Landroid/graphics/Bitmap;

.field 'f' Landroid/net/Uri;

.field 'g' Landroid/os/Bundle;

.field 'h' Landroid/net/Uri;

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private a(Landroid/graphics/Bitmap;)Landroid/support/v4/media/b;
.annotation invisibleparam 1 Landroid/support/a/z;
.end annotation
aload 0
aload 1
putfield android/support/v4/media/b/e Landroid/graphics/Bitmap;
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method private a(Landroid/net/Uri;)Landroid/support/v4/media/b;
.annotation invisibleparam 1 Landroid/support/a/z;
.end annotation
aload 0
aload 1
putfield android/support/v4/media/b/f Landroid/net/Uri;
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method private a(Landroid/os/Bundle;)Landroid/support/v4/media/b;
.annotation invisibleparam 1 Landroid/support/a/z;
.end annotation
aload 0
aload 1
putfield android/support/v4/media/b/g Landroid/os/Bundle;
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method private a(Ljava/lang/CharSequence;)Landroid/support/v4/media/b;
.annotation invisibleparam 1 Landroid/support/a/z;
.end annotation
aload 0
aload 1
putfield android/support/v4/media/b/b Ljava/lang/CharSequence;
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method private a(Ljava/lang/String;)Landroid/support/v4/media/b;
.annotation invisibleparam 1 Landroid/support/a/z;
.end annotation
aload 0
aload 1
putfield android/support/v4/media/b/a Ljava/lang/String;
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method private b(Landroid/net/Uri;)Landroid/support/v4/media/b;
.annotation invisibleparam 1 Landroid/support/a/z;
.end annotation
aload 0
aload 1
putfield android/support/v4/media/b/h Landroid/net/Uri;
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method private b(Ljava/lang/CharSequence;)Landroid/support/v4/media/b;
.annotation invisibleparam 1 Landroid/support/a/z;
.end annotation
aload 0
aload 1
putfield android/support/v4/media/b/c Ljava/lang/CharSequence;
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method private c(Ljava/lang/CharSequence;)Landroid/support/v4/media/b;
.annotation invisibleparam 1 Landroid/support/a/z;
.end annotation
aload 0
aload 1
putfield android/support/v4/media/b/d Ljava/lang/CharSequence;
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method public final a()Landroid/support/v4/media/MediaDescriptionCompat;
new android/support/v4/media/MediaDescriptionCompat
dup
aload 0
getfield android/support/v4/media/b/a Ljava/lang/String;
aload 0
getfield android/support/v4/media/b/b Ljava/lang/CharSequence;
aload 0
getfield android/support/v4/media/b/c Ljava/lang/CharSequence;
aload 0
getfield android/support/v4/media/b/d Ljava/lang/CharSequence;
aload 0
getfield android/support/v4/media/b/e Landroid/graphics/Bitmap;
aload 0
getfield android/support/v4/media/b/f Landroid/net/Uri;
aload 0
getfield android/support/v4/media/b/g Landroid/os/Bundle;
aload 0
getfield android/support/v4/media/b/h Landroid/net/Uri;
iconst_0
invokespecial android/support/v4/media/MediaDescriptionCompat/<init>(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/graphics/Bitmap;Landroid/net/Uri;Landroid/os/Bundle;Landroid/net/Uri;B)V
areturn
.limit locals 1
.limit stack 11
.end method
