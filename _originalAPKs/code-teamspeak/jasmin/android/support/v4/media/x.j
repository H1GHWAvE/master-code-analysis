.bytecode 50.0
.class final synchronized android/support/v4/media/x
.super java/lang/Object
.implements android/media/RemoteControlClient$OnGetPlaybackPositionListener
.implements android/media/RemoteControlClient$OnPlaybackPositionUpdateListener

.field final 'a' Landroid/content/Context;

.field final 'b' Landroid/media/AudioManager;

.field final 'c' Landroid/view/View;

.field final 'd' Landroid/support/v4/media/w;

.field final 'e' Ljava/lang/String;

.field final 'f' Landroid/content/IntentFilter;

.field final 'g' Landroid/content/Intent;

.field final 'h' Landroid/view/ViewTreeObserver$OnWindowAttachListener;

.field final 'i' Landroid/view/ViewTreeObserver$OnWindowFocusChangeListener;

.field final 'j' Landroid/content/BroadcastReceiver;

.field 'k' Landroid/media/AudioManager$OnAudioFocusChangeListener;

.field 'l' Landroid/app/PendingIntent;

.field 'm' Landroid/media/RemoteControlClient;

.field 'n' Z

.field 'o' I

.field 'p' Z

.method public <init>(Landroid/content/Context;Landroid/media/AudioManager;Landroid/view/View;Landroid/support/v4/media/w;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
new android/support/v4/media/y
dup
aload 0
invokespecial android/support/v4/media/y/<init>(Landroid/support/v4/media/x;)V
putfield android/support/v4/media/x/h Landroid/view/ViewTreeObserver$OnWindowAttachListener;
aload 0
new android/support/v4/media/z
dup
aload 0
invokespecial android/support/v4/media/z/<init>(Landroid/support/v4/media/x;)V
putfield android/support/v4/media/x/i Landroid/view/ViewTreeObserver$OnWindowFocusChangeListener;
aload 0
new android/support/v4/media/aa
dup
aload 0
invokespecial android/support/v4/media/aa/<init>(Landroid/support/v4/media/x;)V
putfield android/support/v4/media/x/j Landroid/content/BroadcastReceiver;
aload 0
new android/support/v4/media/ab
dup
aload 0
invokespecial android/support/v4/media/ab/<init>(Landroid/support/v4/media/x;)V
putfield android/support/v4/media/x/k Landroid/media/AudioManager$OnAudioFocusChangeListener;
aload 0
iconst_0
putfield android/support/v4/media/x/o I
aload 0
aload 1
putfield android/support/v4/media/x/a Landroid/content/Context;
aload 0
aload 2
putfield android/support/v4/media/x/b Landroid/media/AudioManager;
aload 0
aload 3
putfield android/support/v4/media/x/c Landroid/view/View;
aload 0
aload 4
putfield android/support/v4/media/x/d Landroid/support/v4/media/w;
aload 0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 1
invokevirtual android/content/Context/getPackageName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ":transport:"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokestatic java/lang/System/identityHashCode(Ljava/lang/Object;)I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
putfield android/support/v4/media/x/e Ljava/lang/String;
aload 0
new android/content/Intent
dup
aload 0
getfield android/support/v4/media/x/e Ljava/lang/String;
invokespecial android/content/Intent/<init>(Ljava/lang/String;)V
putfield android/support/v4/media/x/g Landroid/content/Intent;
aload 0
getfield android/support/v4/media/x/g Landroid/content/Intent;
aload 1
invokevirtual android/content/Context/getPackageName()Ljava/lang/String;
invokevirtual android/content/Intent/setPackage(Ljava/lang/String;)Landroid/content/Intent;
pop
aload 0
new android/content/IntentFilter
dup
invokespecial android/content/IntentFilter/<init>()V
putfield android/support/v4/media/x/f Landroid/content/IntentFilter;
aload 0
getfield android/support/v4/media/x/f Landroid/content/IntentFilter;
aload 0
getfield android/support/v4/media/x/e Ljava/lang/String;
invokevirtual android/content/IntentFilter/addAction(Ljava/lang/String;)V
aload 0
getfield android/support/v4/media/x/c Landroid/view/View;
invokevirtual android/view/View/getViewTreeObserver()Landroid/view/ViewTreeObserver;
aload 0
getfield android/support/v4/media/x/h Landroid/view/ViewTreeObserver$OnWindowAttachListener;
invokevirtual android/view/ViewTreeObserver/addOnWindowAttachListener(Landroid/view/ViewTreeObserver$OnWindowAttachListener;)V
aload 0
getfield android/support/v4/media/x/c Landroid/view/View;
invokevirtual android/view/View/getViewTreeObserver()Landroid/view/ViewTreeObserver;
aload 0
getfield android/support/v4/media/x/i Landroid/view/ViewTreeObserver$OnWindowFocusChangeListener;
invokevirtual android/view/ViewTreeObserver/addOnWindowFocusChangeListener(Landroid/view/ViewTreeObserver$OnWindowFocusChangeListener;)V
return
.limit locals 5
.limit stack 4
.end method

.method private a(ZJI)V
aload 0
getfield android/support/v4/media/x/m Landroid/media/RemoteControlClient;
ifnull L0
aload 0
getfield android/support/v4/media/x/m Landroid/media/RemoteControlClient;
astore 7
iload 1
ifeq L1
iconst_3
istore 6
L2:
iload 1
ifeq L3
fconst_1
fstore 5
L4:
aload 7
iload 6
lload 2
fload 5
invokevirtual android/media/RemoteControlClient/setPlaybackState(IJF)V
aload 0
getfield android/support/v4/media/x/m Landroid/media/RemoteControlClient;
iload 4
invokevirtual android/media/RemoteControlClient/setTransportControlFlags(I)V
L0:
return
L1:
iconst_1
istore 6
goto L2
L3:
fconst_0
fstore 5
goto L4
.limit locals 8
.limit stack 5
.end method

.method private e()Ljava/lang/Object;
aload 0
getfield android/support/v4/media/x/m Landroid/media/RemoteControlClient;
areturn
.limit locals 1
.limit stack 1
.end method

.method private f()V
aload 0
invokevirtual android/support/v4/media/x/d()V
aload 0
getfield android/support/v4/media/x/c Landroid/view/View;
invokevirtual android/view/View/getViewTreeObserver()Landroid/view/ViewTreeObserver;
aload 0
getfield android/support/v4/media/x/h Landroid/view/ViewTreeObserver$OnWindowAttachListener;
invokevirtual android/view/ViewTreeObserver/removeOnWindowAttachListener(Landroid/view/ViewTreeObserver$OnWindowAttachListener;)V
aload 0
getfield android/support/v4/media/x/c Landroid/view/View;
invokevirtual android/view/View/getViewTreeObserver()Landroid/view/ViewTreeObserver;
aload 0
getfield android/support/v4/media/x/i Landroid/view/ViewTreeObserver$OnWindowFocusChangeListener;
invokevirtual android/view/ViewTreeObserver/removeOnWindowFocusChangeListener(Landroid/view/ViewTreeObserver$OnWindowFocusChangeListener;)V
return
.limit locals 1
.limit stack 2
.end method

.method private g()V
aload 0
getfield android/support/v4/media/x/a Landroid/content/Context;
aload 0
getfield android/support/v4/media/x/j Landroid/content/BroadcastReceiver;
aload 0
getfield android/support/v4/media/x/f Landroid/content/IntentFilter;
invokevirtual android/content/Context/registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
pop
aload 0
aload 0
getfield android/support/v4/media/x/a Landroid/content/Context;
iconst_0
aload 0
getfield android/support/v4/media/x/g Landroid/content/Intent;
ldc_w 268435456
invokestatic android/app/PendingIntent/getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;
putfield android/support/v4/media/x/l Landroid/app/PendingIntent;
aload 0
new android/media/RemoteControlClient
dup
aload 0
getfield android/support/v4/media/x/l Landroid/app/PendingIntent;
invokespecial android/media/RemoteControlClient/<init>(Landroid/app/PendingIntent;)V
putfield android/support/v4/media/x/m Landroid/media/RemoteControlClient;
aload 0
getfield android/support/v4/media/x/m Landroid/media/RemoteControlClient;
aload 0
invokevirtual android/media/RemoteControlClient/setOnGetPlaybackPositionListener(Landroid/media/RemoteControlClient$OnGetPlaybackPositionListener;)V
aload 0
getfield android/support/v4/media/x/m Landroid/media/RemoteControlClient;
aload 0
invokevirtual android/media/RemoteControlClient/setPlaybackPositionUpdateListener(Landroid/media/RemoteControlClient$OnPlaybackPositionUpdateListener;)V
return
.limit locals 1
.limit stack 5
.end method

.method private h()V
aload 0
getfield android/support/v4/media/x/n Z
ifne L0
aload 0
iconst_1
putfield android/support/v4/media/x/n Z
aload 0
getfield android/support/v4/media/x/b Landroid/media/AudioManager;
aload 0
getfield android/support/v4/media/x/l Landroid/app/PendingIntent;
invokevirtual android/media/AudioManager/registerMediaButtonEventReceiver(Landroid/app/PendingIntent;)V
aload 0
getfield android/support/v4/media/x/b Landroid/media/AudioManager;
aload 0
getfield android/support/v4/media/x/m Landroid/media/RemoteControlClient;
invokevirtual android/media/AudioManager/registerRemoteControlClient(Landroid/media/RemoteControlClient;)V
aload 0
getfield android/support/v4/media/x/o I
iconst_3
if_icmpne L0
aload 0
invokevirtual android/support/v4/media/x/a()V
L0:
return
.limit locals 1
.limit stack 2
.end method

.method private i()V
aload 0
getfield android/support/v4/media/x/o I
iconst_3
if_icmpeq L0
aload 0
iconst_3
putfield android/support/v4/media/x/o I
aload 0
getfield android/support/v4/media/x/m Landroid/media/RemoteControlClient;
iconst_3
invokevirtual android/media/RemoteControlClient/setPlaybackState(I)V
L0:
aload 0
getfield android/support/v4/media/x/n Z
ifeq L1
aload 0
invokevirtual android/support/v4/media/x/a()V
L1:
return
.limit locals 1
.limit stack 2
.end method

.method private j()V
aload 0
getfield android/support/v4/media/x/o I
iconst_3
if_icmpne L0
aload 0
iconst_2
putfield android/support/v4/media/x/o I
aload 0
getfield android/support/v4/media/x/m Landroid/media/RemoteControlClient;
iconst_2
invokevirtual android/media/RemoteControlClient/setPlaybackState(I)V
L0:
aload 0
invokevirtual android/support/v4/media/x/b()V
return
.limit locals 1
.limit stack 2
.end method

.method private k()V
aload 0
getfield android/support/v4/media/x/o I
iconst_1
if_icmpeq L0
aload 0
iconst_1
putfield android/support/v4/media/x/o I
aload 0
getfield android/support/v4/media/x/m Landroid/media/RemoteControlClient;
iconst_1
invokevirtual android/media/RemoteControlClient/setPlaybackState(I)V
L0:
aload 0
invokevirtual android/support/v4/media/x/b()V
return
.limit locals 1
.limit stack 2
.end method

.method final a()V
aload 0
getfield android/support/v4/media/x/p Z
ifne L0
aload 0
iconst_1
putfield android/support/v4/media/x/p Z
aload 0
getfield android/support/v4/media/x/b Landroid/media/AudioManager;
aload 0
getfield android/support/v4/media/x/k Landroid/media/AudioManager$OnAudioFocusChangeListener;
iconst_3
iconst_1
invokevirtual android/media/AudioManager/requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I
pop
L0:
return
.limit locals 1
.limit stack 4
.end method

.method final b()V
aload 0
getfield android/support/v4/media/x/p Z
ifeq L0
aload 0
iconst_0
putfield android/support/v4/media/x/p Z
aload 0
getfield android/support/v4/media/x/b Landroid/media/AudioManager;
aload 0
getfield android/support/v4/media/x/k Landroid/media/AudioManager$OnAudioFocusChangeListener;
invokevirtual android/media/AudioManager/abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I
pop
L0:
return
.limit locals 1
.limit stack 2
.end method

.method final c()V
aload 0
invokevirtual android/support/v4/media/x/b()V
aload 0
getfield android/support/v4/media/x/n Z
ifeq L0
aload 0
iconst_0
putfield android/support/v4/media/x/n Z
aload 0
getfield android/support/v4/media/x/b Landroid/media/AudioManager;
aload 0
getfield android/support/v4/media/x/m Landroid/media/RemoteControlClient;
invokevirtual android/media/AudioManager/unregisterRemoteControlClient(Landroid/media/RemoteControlClient;)V
aload 0
getfield android/support/v4/media/x/b Landroid/media/AudioManager;
aload 0
getfield android/support/v4/media/x/l Landroid/app/PendingIntent;
invokevirtual android/media/AudioManager/unregisterMediaButtonEventReceiver(Landroid/app/PendingIntent;)V
L0:
return
.limit locals 1
.limit stack 2
.end method

.method final d()V
aload 0
invokevirtual android/support/v4/media/x/c()V
aload 0
getfield android/support/v4/media/x/l Landroid/app/PendingIntent;
ifnull L0
aload 0
getfield android/support/v4/media/x/a Landroid/content/Context;
aload 0
getfield android/support/v4/media/x/j Landroid/content/BroadcastReceiver;
invokevirtual android/content/Context/unregisterReceiver(Landroid/content/BroadcastReceiver;)V
aload 0
getfield android/support/v4/media/x/l Landroid/app/PendingIntent;
invokevirtual android/app/PendingIntent/cancel()V
aload 0
aconst_null
putfield android/support/v4/media/x/l Landroid/app/PendingIntent;
aload 0
aconst_null
putfield android/support/v4/media/x/m Landroid/media/RemoteControlClient;
L0:
return
.limit locals 1
.limit stack 2
.end method

.method public final onGetPlaybackPosition()J
aload 0
getfield android/support/v4/media/x/d Landroid/support/v4/media/w;
invokeinterface android/support/v4/media/w/a()J 0
lreturn
.limit locals 1
.limit stack 2
.end method

.method public final onPlaybackPositionUpdate(J)V
return
.limit locals 3
.limit stack 0
.end method
