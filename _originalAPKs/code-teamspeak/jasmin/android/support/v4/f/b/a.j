.bytecode 50.0
.class public final synchronized android/support/v4/f/b/a
.super java/lang/Object

.field static final 'a' Landroid/support/v4/f/b/g;

.field private 'b' Landroid/content/Context;

.method static <clinit>()V
getstatic android/os/Build$VERSION/SDK_INT I
bipush 23
if_icmplt L0
new android/support/v4/f/b/b
dup
invokespecial android/support/v4/f/b/b/<init>()V
putstatic android/support/v4/f/b/a/a Landroid/support/v4/f/b/g;
return
L0:
new android/support/v4/f/b/h
dup
invokespecial android/support/v4/f/b/h/<init>()V
putstatic android/support/v4/f/b/a/a Landroid/support/v4/f/b/g;
return
.limit locals 0
.limit stack 2
.end method

.method private <init>(Landroid/content/Context;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
putfield android/support/v4/f/b/a/b Landroid/content/Context;
return
.limit locals 2
.limit stack 2
.end method

.method private static a(Landroid/content/Context;)Landroid/support/v4/f/b/a;
new android/support/v4/f/b/a
dup
aload 0
invokespecial android/support/v4/f/b/a/<init>(Landroid/content/Context;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private a(Landroid/support/v4/f/b/f;ILandroid/support/v4/i/c;Landroid/support/v4/f/b/d;Landroid/os/Handler;)V
.annotation invisibleparam 1 Landroid/support/a/z;
.end annotation
.annotation invisibleparam 3 Landroid/support/a/z;
.end annotation
.annotation invisibleparam 4 Landroid/support/a/y;
.end annotation
.annotation invisibleparam 5 Landroid/support/a/z;
.end annotation
getstatic android/support/v4/f/b/a/a Landroid/support/v4/f/b/g;
aload 0
getfield android/support/v4/f/b/a/b Landroid/content/Context;
aload 1
iload 2
aload 3
aload 4
aload 5
invokeinterface android/support/v4/f/b/g/a(Landroid/content/Context;Landroid/support/v4/f/b/f;ILandroid/support/v4/i/c;Landroid/support/v4/f/b/d;Landroid/os/Handler;)V 6
return
.limit locals 6
.limit stack 7
.end method

.method private a()Z
getstatic android/support/v4/f/b/a/a Landroid/support/v4/f/b/g;
aload 0
getfield android/support/v4/f/b/a/b Landroid/content/Context;
invokeinterface android/support/v4/f/b/g/a(Landroid/content/Context;)Z 1
ireturn
.limit locals 1
.limit stack 2
.end method

.method private b()Z
getstatic android/support/v4/f/b/a/a Landroid/support/v4/f/b/g;
aload 0
getfield android/support/v4/f/b/a/b Landroid/content/Context;
invokeinterface android/support/v4/f/b/g/b(Landroid/content/Context;)Z 1
ireturn
.limit locals 1
.limit stack 2
.end method
