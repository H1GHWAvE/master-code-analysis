.bytecode 50.0
.class public final synchronized android/support/v4/f/b/i
.super java/lang/Object

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a(Landroid/support/v4/f/b/k;)Landroid/hardware/fingerprint/FingerprintManager$AuthenticationCallback;
new android/support/v4/f/b/j
dup
aload 0
invokespecial android/support/v4/f/b/j/<init>(Landroid/support/v4/f/b/k;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private static a(Landroid/support/v4/f/b/m;)Landroid/hardware/fingerprint/FingerprintManager$CryptoObject;
aload 0
ifnonnull L0
L1:
aconst_null
areturn
L0:
aload 0
getfield android/support/v4/f/b/m/b Ljavax/crypto/Cipher;
ifnull L2
new android/hardware/fingerprint/FingerprintManager$CryptoObject
dup
aload 0
getfield android/support/v4/f/b/m/b Ljavax/crypto/Cipher;
invokespecial android/hardware/fingerprint/FingerprintManager$CryptoObject/<init>(Ljavax/crypto/Cipher;)V
areturn
L2:
aload 0
getfield android/support/v4/f/b/m/a Ljava/security/Signature;
ifnull L3
new android/hardware/fingerprint/FingerprintManager$CryptoObject
dup
aload 0
getfield android/support/v4/f/b/m/a Ljava/security/Signature;
invokespecial android/hardware/fingerprint/FingerprintManager$CryptoObject/<init>(Ljava/security/Signature;)V
areturn
L3:
aload 0
getfield android/support/v4/f/b/m/c Ljavax/crypto/Mac;
ifnull L1
new android/hardware/fingerprint/FingerprintManager$CryptoObject
dup
aload 0
getfield android/support/v4/f/b/m/c Ljavax/crypto/Mac;
invokespecial android/hardware/fingerprint/FingerprintManager$CryptoObject/<init>(Ljavax/crypto/Mac;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method static a(Landroid/content/Context;)Landroid/hardware/fingerprint/FingerprintManager;
aload 0
ldc android/hardware/fingerprint/FingerprintManager
invokevirtual android/content/Context/getSystemService(Ljava/lang/Class;)Ljava/lang/Object;
checkcast android/hardware/fingerprint/FingerprintManager
areturn
.limit locals 1
.limit stack 2
.end method

.method private static a(Landroid/hardware/fingerprint/FingerprintManager$CryptoObject;)Landroid/support/v4/f/b/m;
aload 0
ifnonnull L0
L1:
aconst_null
areturn
L0:
aload 0
invokevirtual android/hardware/fingerprint/FingerprintManager$CryptoObject/getCipher()Ljavax/crypto/Cipher;
ifnull L2
new android/support/v4/f/b/m
dup
aload 0
invokevirtual android/hardware/fingerprint/FingerprintManager$CryptoObject/getCipher()Ljavax/crypto/Cipher;
invokespecial android/support/v4/f/b/m/<init>(Ljavax/crypto/Cipher;)V
areturn
L2:
aload 0
invokevirtual android/hardware/fingerprint/FingerprintManager$CryptoObject/getSignature()Ljava/security/Signature;
ifnull L3
new android/support/v4/f/b/m
dup
aload 0
invokevirtual android/hardware/fingerprint/FingerprintManager$CryptoObject/getSignature()Ljava/security/Signature;
invokespecial android/support/v4/f/b/m/<init>(Ljava/security/Signature;)V
areturn
L3:
aload 0
invokevirtual android/hardware/fingerprint/FingerprintManager$CryptoObject/getMac()Ljavax/crypto/Mac;
ifnull L1
new android/support/v4/f/b/m
dup
aload 0
invokevirtual android/hardware/fingerprint/FingerprintManager$CryptoObject/getMac()Ljavax/crypto/Mac;
invokespecial android/support/v4/f/b/m/<init>(Ljavax/crypto/Mac;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private static a(Landroid/content/Context;Landroid/support/v4/f/b/m;ILjava/lang/Object;Landroid/support/v4/f/b/k;Landroid/os/Handler;)V
aload 0
invokestatic android/support/v4/f/b/i/a(Landroid/content/Context;)Landroid/hardware/fingerprint/FingerprintManager;
astore 6
aload 1
ifnull L0
aload 1
getfield android/support/v4/f/b/m/b Ljavax/crypto/Cipher;
ifnull L1
new android/hardware/fingerprint/FingerprintManager$CryptoObject
dup
aload 1
getfield android/support/v4/f/b/m/b Ljavax/crypto/Cipher;
invokespecial android/hardware/fingerprint/FingerprintManager$CryptoObject/<init>(Ljavax/crypto/Cipher;)V
astore 0
L2:
aload 6
aload 0
aload 3
checkcast android/os/CancellationSignal
iload 2
new android/support/v4/f/b/j
dup
aload 4
invokespecial android/support/v4/f/b/j/<init>(Landroid/support/v4/f/b/k;)V
aload 5
invokevirtual android/hardware/fingerprint/FingerprintManager/authenticate(Landroid/hardware/fingerprint/FingerprintManager$CryptoObject;Landroid/os/CancellationSignal;ILandroid/hardware/fingerprint/FingerprintManager$AuthenticationCallback;Landroid/os/Handler;)V
return
L1:
aload 1
getfield android/support/v4/f/b/m/a Ljava/security/Signature;
ifnull L3
new android/hardware/fingerprint/FingerprintManager$CryptoObject
dup
aload 1
getfield android/support/v4/f/b/m/a Ljava/security/Signature;
invokespecial android/hardware/fingerprint/FingerprintManager$CryptoObject/<init>(Ljava/security/Signature;)V
astore 0
goto L2
L3:
aload 1
getfield android/support/v4/f/b/m/c Ljavax/crypto/Mac;
ifnull L0
new android/hardware/fingerprint/FingerprintManager$CryptoObject
dup
aload 1
getfield android/support/v4/f/b/m/c Ljavax/crypto/Mac;
invokespecial android/hardware/fingerprint/FingerprintManager$CryptoObject/<init>(Ljavax/crypto/Mac;)V
astore 0
goto L2
L0:
aconst_null
astore 0
goto L2
.limit locals 7
.limit stack 7
.end method

.method private static synthetic b(Landroid/hardware/fingerprint/FingerprintManager$CryptoObject;)Landroid/support/v4/f/b/m;
aload 0
ifnull L0
aload 0
invokevirtual android/hardware/fingerprint/FingerprintManager$CryptoObject/getCipher()Ljavax/crypto/Cipher;
ifnull L1
new android/support/v4/f/b/m
dup
aload 0
invokevirtual android/hardware/fingerprint/FingerprintManager$CryptoObject/getCipher()Ljavax/crypto/Cipher;
invokespecial android/support/v4/f/b/m/<init>(Ljavax/crypto/Cipher;)V
areturn
L1:
aload 0
invokevirtual android/hardware/fingerprint/FingerprintManager$CryptoObject/getSignature()Ljava/security/Signature;
ifnull L2
new android/support/v4/f/b/m
dup
aload 0
invokevirtual android/hardware/fingerprint/FingerprintManager$CryptoObject/getSignature()Ljava/security/Signature;
invokespecial android/support/v4/f/b/m/<init>(Ljava/security/Signature;)V
areturn
L2:
aload 0
invokevirtual android/hardware/fingerprint/FingerprintManager$CryptoObject/getMac()Ljavax/crypto/Mac;
ifnull L0
new android/support/v4/f/b/m
dup
aload 0
invokevirtual android/hardware/fingerprint/FingerprintManager$CryptoObject/getMac()Ljavax/crypto/Mac;
invokespecial android/support/v4/f/b/m/<init>(Ljavax/crypto/Mac;)V
areturn
L0:
aconst_null
areturn
.limit locals 1
.limit stack 3
.end method

.method private static b(Landroid/content/Context;)Z
aload 0
invokestatic android/support/v4/f/b/i/a(Landroid/content/Context;)Landroid/hardware/fingerprint/FingerprintManager;
invokevirtual android/hardware/fingerprint/FingerprintManager/hasEnrolledFingerprints()Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private static c(Landroid/content/Context;)Z
aload 0
invokestatic android/support/v4/f/b/i/a(Landroid/content/Context;)Landroid/hardware/fingerprint/FingerprintManager;
invokevirtual android/hardware/fingerprint/FingerprintManager/isHardwareDetected()Z
ireturn
.limit locals 1
.limit stack 1
.end method
