.bytecode 50.0
.class final synchronized android/support/v7/app/bc
.super android/widget/FrameLayout

.field final synthetic 'a' Landroid/support/v7/app/AppCompatDelegateImplV7;

.method public <init>(Landroid/support/v7/app/AppCompatDelegateImplV7;Landroid/content/Context;)V
aload 0
aload 1
putfield android/support/v7/app/bc/a Landroid/support/v7/app/AppCompatDelegateImplV7;
aload 0
aload 2
invokespecial android/widget/FrameLayout/<init>(Landroid/content/Context;)V
return
.limit locals 3
.limit stack 2
.end method

.method private a(II)Z
iload 1
bipush -5
if_icmplt L0
iload 2
bipush -5
if_icmplt L0
iload 1
aload 0
invokevirtual android/support/v7/app/bc/getWidth()I
iconst_5
iadd
if_icmpgt L0
iload 2
aload 0
invokevirtual android/support/v7/app/bc/getHeight()I
iconst_5
iadd
if_icmple L1
L0:
iconst_1
ireturn
L1:
iconst_0
ireturn
.limit locals 3
.limit stack 3
.end method

.method public final dispatchKeyEvent(Landroid/view/KeyEvent;)Z
aload 0
getfield android/support/v7/app/bc/a Landroid/support/v7/app/AppCompatDelegateImplV7;
aload 1
invokevirtual android/support/v7/app/AppCompatDelegateImplV7/a(Landroid/view/KeyEvent;)Z
ifne L0
aload 0
aload 1
invokespecial android/widget/FrameLayout/dispatchKeyEvent(Landroid/view/KeyEvent;)Z
ifeq L1
L0:
iconst_1
ireturn
L1:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
aload 1
invokevirtual android/view/MotionEvent/getAction()I
ifne L0
aload 1
invokevirtual android/view/MotionEvent/getX()F
f2i
istore 2
aload 1
invokevirtual android/view/MotionEvent/getY()F
f2i
istore 3
iload 2
bipush -5
if_icmplt L1
iload 3
bipush -5
if_icmplt L1
iload 2
aload 0
invokevirtual android/support/v7/app/bc/getWidth()I
iconst_5
iadd
if_icmpgt L1
iload 3
aload 0
invokevirtual android/support/v7/app/bc/getHeight()I
iconst_5
iadd
if_icmple L2
L1:
iconst_1
istore 2
L3:
iload 2
ifeq L0
aload 0
getfield android/support/v7/app/bc/a Landroid/support/v7/app/AppCompatDelegateImplV7;
astore 1
aload 1
aload 1
iconst_0
invokevirtual android/support/v7/app/AppCompatDelegateImplV7/f(I)Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;
iconst_1
invokevirtual android/support/v7/app/AppCompatDelegateImplV7/a(Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;Z)V
iconst_1
ireturn
L2:
iconst_0
istore 2
goto L3
L0:
aload 0
aload 1
invokespecial android/widget/FrameLayout/onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
ireturn
.limit locals 4
.limit stack 3
.end method

.method public final setBackgroundResource(I)V
aload 0
aload 0
invokevirtual android/support/v7/app/bc/getContext()Landroid/content/Context;
iload 1
invokestatic android/support/v7/internal/widget/av/a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;
invokevirtual android/support/v7/app/bc/setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
return
.limit locals 2
.limit stack 3
.end method
