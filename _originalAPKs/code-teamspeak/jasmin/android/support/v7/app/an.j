.bytecode 50.0
.class synchronized android/support/v7/app/an
.super android/support/v7/internal/view/k

.field final synthetic 'a' Landroid/support/v7/app/ak;

.method <init>(Landroid/support/v7/app/ak;Landroid/view/Window$Callback;)V
aload 0
aload 1
putfield android/support/v7/app/an/a Landroid/support/v7/app/ak;
aload 0
aload 2
invokespecial android/support/v7/internal/view/k/<init>(Landroid/view/Window$Callback;)V
return
.limit locals 3
.limit stack 2
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
aload 0
getfield android/support/v7/app/an/a Landroid/support/v7/app/ak;
aload 1
invokevirtual android/support/v7/app/ak/a(Landroid/view/KeyEvent;)Z
ifne L0
aload 0
aload 1
invokespecial android/support/v7/internal/view/k/dispatchKeyEvent(Landroid/view/KeyEvent;)Z
ifeq L1
L0:
iconst_1
ireturn
L1:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method public dispatchKeyShortcutEvent(Landroid/view/KeyEvent;)Z
aload 0
aload 1
invokespecial android/support/v7/internal/view/k/dispatchKeyShortcutEvent(Landroid/view/KeyEvent;)Z
ifne L0
aload 0
getfield android/support/v7/app/an/a Landroid/support/v7/app/ak;
aload 1
invokevirtual android/view/KeyEvent/getKeyCode()I
aload 1
invokevirtual android/support/v7/app/ak/a(ILandroid/view/KeyEvent;)Z
ifeq L1
L0:
iconst_1
ireturn
L1:
iconst_0
ireturn
.limit locals 2
.limit stack 3
.end method

.method public onContentChanged()V
return
.limit locals 1
.limit stack 0
.end method

.method public onCreatePanelMenu(ILandroid/view/Menu;)Z
iload 1
ifne L0
aload 2
instanceof android/support/v7/internal/view/menu/i
ifne L0
iconst_0
ireturn
L0:
aload 0
iload 1
aload 2
invokespecial android/support/v7/internal/view/k/onCreatePanelMenu(ILandroid/view/Menu;)Z
ireturn
.limit locals 3
.limit stack 3
.end method

.method public onMenuOpened(ILandroid/view/Menu;)Z
aload 0
iload 1
aload 2
invokespecial android/support/v7/internal/view/k/onMenuOpened(ILandroid/view/Menu;)Z
ifne L0
aload 0
getfield android/support/v7/app/an/a Landroid/support/v7/app/ak;
iload 1
invokevirtual android/support/v7/app/ak/e(I)Z
ifeq L1
L0:
iconst_1
ireturn
L1:
iconst_0
ireturn
.limit locals 3
.limit stack 3
.end method

.method public onPanelClosed(ILandroid/view/Menu;)V
aload 0
iload 1
aload 2
invokespecial android/support/v7/internal/view/k/onPanelClosed(ILandroid/view/Menu;)V
aload 0
getfield android/support/v7/app/an/a Landroid/support/v7/app/ak;
iload 1
invokevirtual android/support/v7/app/ak/d(I)V
return
.limit locals 3
.limit stack 3
.end method

.method public onPreparePanel(ILandroid/view/View;Landroid/view/Menu;)Z
aload 3
instanceof android/support/v7/internal/view/menu/i
ifeq L0
aload 3
checkcast android/support/v7/internal/view/menu/i
astore 6
L1:
iload 1
ifne L2
aload 6
ifnonnull L2
iconst_0
istore 4
L3:
iload 4
ireturn
L0:
aconst_null
astore 6
goto L1
L2:
aload 6
ifnull L4
aload 6
iconst_1
putfield android/support/v7/internal/view/menu/i/o Z
L4:
aload 0
iload 1
aload 2
aload 3
invokespecial android/support/v7/internal/view/k/onPreparePanel(ILandroid/view/View;Landroid/view/Menu;)Z
istore 5
iload 5
istore 4
aload 6
ifnull L3
aload 6
iconst_0
putfield android/support/v7/internal/view/menu/i/o Z
iload 5
ireturn
.limit locals 7
.limit stack 4
.end method
