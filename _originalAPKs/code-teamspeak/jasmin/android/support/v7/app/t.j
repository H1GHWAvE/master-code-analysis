.bytecode 50.0
.class final synchronized android/support/v7/app/t
.super java/lang/Object

.field private static final 'a' Ljava/lang/String; = "ActionBarDrawerToggleHoneycomb"

.field private static final 'b' [I

.method static <clinit>()V
iconst_1
newarray int
dup
iconst_0
ldc_w 16843531
iastore
putstatic android/support/v7/app/t/b [I
return
.limit locals 0
.limit stack 4
.end method

.method <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static a(Landroid/app/Activity;)Landroid/graphics/drawable/Drawable;
aload 0
getstatic android/support/v7/app/t/b [I
invokevirtual android/app/Activity/obtainStyledAttributes([I)Landroid/content/res/TypedArray;
astore 0
aload 0
iconst_0
invokevirtual android/content/res/TypedArray/getDrawable(I)Landroid/graphics/drawable/Drawable;
astore 1
aload 0
invokevirtual android/content/res/TypedArray/recycle()V
aload 1
areturn
.limit locals 2
.limit stack 2
.end method

.method public static a(Landroid/app/Activity;Landroid/graphics/drawable/Drawable;I)Landroid/support/v7/app/u;
.catch java/lang/Exception from L0 to L1 using L2
new android/support/v7/app/u
dup
aload 0
invokespecial android/support/v7/app/u/<init>(Landroid/app/Activity;)V
astore 3
aload 3
getfield android/support/v7/app/u/a Ljava/lang/reflect/Method;
ifnull L3
L0:
aload 0
invokevirtual android/app/Activity/getActionBar()Landroid/app/ActionBar;
astore 0
aload 3
getfield android/support/v7/app/u/a Ljava/lang/reflect/Method;
aload 0
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 1
aastore
invokevirtual java/lang/reflect/Method/invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 3
getfield android/support/v7/app/u/b Ljava/lang/reflect/Method;
aload 0
iconst_1
anewarray java/lang/Object
dup
iconst_0
iload 2
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokevirtual java/lang/reflect/Method/invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
pop
L1:
aload 3
areturn
L2:
astore 0
ldc "ActionBarDrawerToggleHoneycomb"
ldc "Couldn't set home-as-up indicator via JB-MR2 API"
aload 0
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
aload 3
areturn
L3:
aload 3
getfield android/support/v7/app/u/c Landroid/widget/ImageView;
ifnull L4
aload 3
getfield android/support/v7/app/u/c Landroid/widget/ImageView;
aload 1
invokevirtual android/widget/ImageView/setImageDrawable(Landroid/graphics/drawable/Drawable;)V
aload 3
areturn
L4:
ldc "ActionBarDrawerToggleHoneycomb"
ldc "Couldn't set home-as-up indicator"
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/String;)I
pop
aload 3
areturn
.limit locals 4
.limit stack 6
.end method

.method public static a(Landroid/support/v7/app/u;Landroid/app/Activity;I)Landroid/support/v7/app/u;
.catch java/lang/Exception from L0 to L1 using L2
aload 0
astore 3
aload 0
ifnonnull L3
new android/support/v7/app/u
dup
aload 1
invokespecial android/support/v7/app/u/<init>(Landroid/app/Activity;)V
astore 3
L3:
aload 3
getfield android/support/v7/app/u/a Ljava/lang/reflect/Method;
ifnull L1
L0:
aload 1
invokevirtual android/app/Activity/getActionBar()Landroid/app/ActionBar;
astore 0
aload 3
getfield android/support/v7/app/u/b Ljava/lang/reflect/Method;
aload 0
iconst_1
anewarray java/lang/Object
dup
iconst_0
iload 2
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokevirtual java/lang/reflect/Method/invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic android/os/Build$VERSION/SDK_INT I
bipush 19
if_icmpgt L1
aload 0
aload 0
invokevirtual android/app/ActionBar/getSubtitle()Ljava/lang/CharSequence;
invokevirtual android/app/ActionBar/setSubtitle(Ljava/lang/CharSequence;)V
L1:
aload 3
areturn
L2:
astore 0
ldc "ActionBarDrawerToggleHoneycomb"
ldc "Couldn't set content description via JB-MR2 API"
aload 0
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
aload 3
areturn
.limit locals 4
.limit stack 6
.end method
