.bytecode 50.0
.class public synchronized android/support/v7/widget/Toolbar$SavedState
.super android/view/View$BaseSavedState

.field public static final 'CREATOR' Landroid/os/Parcelable$Creator;

.field 'a' I

.field 'b' Z

.method static <clinit>()V
new android/support/v7/widget/cm
dup
invokespecial android/support/v7/widget/cm/<init>()V
putstatic android/support/v7/widget/Toolbar$SavedState/CREATOR Landroid/os/Parcelable$Creator;
return
.limit locals 0
.limit stack 2
.end method

.method public <init>(Landroid/os/Parcel;)V
aload 0
aload 1
invokespecial android/view/View$BaseSavedState/<init>(Landroid/os/Parcel;)V
aload 0
aload 1
invokevirtual android/os/Parcel/readInt()I
putfield android/support/v7/widget/Toolbar$SavedState/a I
aload 1
invokevirtual android/os/Parcel/readInt()I
ifeq L0
iconst_1
istore 2
L1:
aload 0
iload 2
putfield android/support/v7/widget/Toolbar$SavedState/b Z
return
L0:
iconst_0
istore 2
goto L1
.limit locals 3
.limit stack 2
.end method

.method public <init>(Landroid/os/Parcelable;)V
aload 0
aload 1
invokespecial android/view/View$BaseSavedState/<init>(Landroid/os/Parcelable;)V
return
.limit locals 2
.limit stack 2
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
aload 0
aload 1
iload 2
invokespecial android/view/View$BaseSavedState/writeToParcel(Landroid/os/Parcel;I)V
aload 1
aload 0
getfield android/support/v7/widget/Toolbar$SavedState/a I
invokevirtual android/os/Parcel/writeInt(I)V
aload 0
getfield android/support/v7/widget/Toolbar$SavedState/b Z
ifeq L0
iconst_1
istore 2
L1:
aload 1
iload 2
invokevirtual android/os/Parcel/writeInt(I)V
return
L0:
iconst_0
istore 2
goto L1
.limit locals 3
.limit stack 3
.end method
