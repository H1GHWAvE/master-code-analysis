.bytecode 50.0
.class final synchronized android/support/v7/widget/ac
.super java/lang/Object
.implements android/widget/ListAdapter
.implements android/widget/SpinnerAdapter

.field private 'a' Landroid/widget/SpinnerAdapter;

.field private 'b' Landroid/widget/ListAdapter;

.method public <init>(Landroid/widget/SpinnerAdapter;Landroid/content/res/Resources$Theme;)V
.annotation invisibleparam 1 Landroid/support/a/z;
.end annotation
.annotation invisibleparam 2 Landroid/support/a/z;
.end annotation
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
putfield android/support/v7/widget/ac/a Landroid/widget/SpinnerAdapter;
aload 1
instanceof android/widget/ListAdapter
ifeq L0
aload 0
aload 1
checkcast android/widget/ListAdapter
putfield android/support/v7/widget/ac/b Landroid/widget/ListAdapter;
L0:
aload 2
ifnull L1
invokestatic android/support/v7/widget/aa/a()Z
ifeq L1
aload 1
instanceof android/widget/ThemedSpinnerAdapter
ifeq L1
aload 1
checkcast android/widget/ThemedSpinnerAdapter
astore 1
aload 1
invokeinterface android/widget/ThemedSpinnerAdapter/getDropDownViewTheme()Landroid/content/res/Resources$Theme; 0
aload 2
if_acmpeq L1
aload 1
aload 2
invokeinterface android/widget/ThemedSpinnerAdapter/setDropDownViewTheme(Landroid/content/res/Resources$Theme;)V 1
L1:
return
.limit locals 3
.limit stack 2
.end method

.method public final areAllItemsEnabled()Z
aload 0
getfield android/support/v7/widget/ac/b Landroid/widget/ListAdapter;
astore 1
aload 1
ifnull L0
aload 1
invokeinterface android/widget/ListAdapter/areAllItemsEnabled()Z 0
ireturn
L0:
iconst_1
ireturn
.limit locals 2
.limit stack 1
.end method

.method public final getCount()I
aload 0
getfield android/support/v7/widget/ac/a Landroid/widget/SpinnerAdapter;
ifnonnull L0
iconst_0
ireturn
L0:
aload 0
getfield android/support/v7/widget/ac/a Landroid/widget/SpinnerAdapter;
invokeinterface android/widget/SpinnerAdapter/getCount()I 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
aload 0
getfield android/support/v7/widget/ac/a Landroid/widget/SpinnerAdapter;
ifnonnull L0
aconst_null
areturn
L0:
aload 0
getfield android/support/v7/widget/ac/a Landroid/widget/SpinnerAdapter;
iload 1
aload 2
aload 3
invokeinterface android/widget/SpinnerAdapter/getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View; 3
areturn
.limit locals 4
.limit stack 4
.end method

.method public final getItem(I)Ljava/lang/Object;
aload 0
getfield android/support/v7/widget/ac/a Landroid/widget/SpinnerAdapter;
ifnonnull L0
aconst_null
areturn
L0:
aload 0
getfield android/support/v7/widget/ac/a Landroid/widget/SpinnerAdapter;
iload 1
invokeinterface android/widget/SpinnerAdapter/getItem(I)Ljava/lang/Object; 1
areturn
.limit locals 2
.limit stack 2
.end method

.method public final getItemId(I)J
aload 0
getfield android/support/v7/widget/ac/a Landroid/widget/SpinnerAdapter;
ifnonnull L0
ldc2_w -1L
lreturn
L0:
aload 0
getfield android/support/v7/widget/ac/a Landroid/widget/SpinnerAdapter;
iload 1
invokeinterface android/widget/SpinnerAdapter/getItemId(I)J 1
lreturn
.limit locals 2
.limit stack 2
.end method

.method public final getItemViewType(I)I
iconst_0
ireturn
.limit locals 2
.limit stack 1
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
aload 0
iload 1
aload 2
aload 3
invokevirtual android/support/v7/widget/ac/getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
areturn
.limit locals 4
.limit stack 4
.end method

.method public final getViewTypeCount()I
iconst_1
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final hasStableIds()Z
aload 0
getfield android/support/v7/widget/ac/a Landroid/widget/SpinnerAdapter;
ifnull L0
aload 0
getfield android/support/v7/widget/ac/a Landroid/widget/SpinnerAdapter;
invokeinterface android/widget/SpinnerAdapter/hasStableIds()Z 0
ifeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final isEmpty()Z
aload 0
invokevirtual android/support/v7/widget/ac/getCount()I
ifne L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final isEnabled(I)Z
aload 0
getfield android/support/v7/widget/ac/b Landroid/widget/ListAdapter;
astore 2
aload 2
ifnull L0
aload 2
iload 1
invokeinterface android/widget/ListAdapter/isEnabled(I)Z 1
ireturn
L0:
iconst_1
ireturn
.limit locals 3
.limit stack 2
.end method

.method public final registerDataSetObserver(Landroid/database/DataSetObserver;)V
aload 0
getfield android/support/v7/widget/ac/a Landroid/widget/SpinnerAdapter;
ifnull L0
aload 0
getfield android/support/v7/widget/ac/a Landroid/widget/SpinnerAdapter;
aload 1
invokeinterface android/widget/SpinnerAdapter/registerDataSetObserver(Landroid/database/DataSetObserver;)V 1
L0:
return
.limit locals 2
.limit stack 2
.end method

.method public final unregisterDataSetObserver(Landroid/database/DataSetObserver;)V
aload 0
getfield android/support/v7/widget/ac/a Landroid/widget/SpinnerAdapter;
ifnull L0
aload 0
getfield android/support/v7/widget/ac/a Landroid/widget/SpinnerAdapter;
aload 1
invokeinterface android/widget/SpinnerAdapter/unregisterDataSetObserver(Landroid/database/DataSetObserver;)V 1
L0:
return
.limit locals 2
.limit stack 2
.end method
