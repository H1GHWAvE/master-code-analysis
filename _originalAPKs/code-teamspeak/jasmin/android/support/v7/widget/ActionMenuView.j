.bytecode 50.0
.class public synchronized android/support/v7/widget/ActionMenuView
.super android/support/v7/widget/aj
.implements android/support/v7/internal/view/menu/k
.implements android/support/v7/internal/view/menu/z

.field static final 'a' I = 56


.field static final 'b' I = 4


.field private static final 'n' Ljava/lang/String; = "ActionMenuView"

.field public 'c' Landroid/support/v7/internal/view/menu/i;

.field public 'd' Z

.field public 'e' Landroid/support/v7/widget/ActionMenuPresenter;

.field 'f' Landroid/support/v7/internal/view/menu/y;

.field 'g' Landroid/support/v7/internal/view/menu/j;

.field private 'o' Landroid/content/Context;

.field private 'p' I

.field private 'q' Z

.field private 'r' I

.field private 's' I

.field private 't' I

.field private 'u' Landroid/support/v7/widget/o;

.method public <init>(Landroid/content/Context;)V
aload 0
aload 1
aconst_null
invokespecial android/support/v7/widget/ActionMenuView/<init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
return
.limit locals 2
.limit stack 3
.end method

.method public <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
aload 0
aload 1
aload 2
invokespecial android/support/v7/widget/aj/<init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
aload 0
iconst_0
invokevirtual android/support/v7/widget/ActionMenuView/setBaselineAligned(Z)V
aload 1
invokevirtual android/content/Context/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
getfield android/util/DisplayMetrics/density F
fstore 3
aload 0
ldc_w 56.0F
fload 3
fmul
f2i
putfield android/support/v7/widget/ActionMenuView/s I
aload 0
fload 3
ldc_w 4.0F
fmul
f2i
putfield android/support/v7/widget/ActionMenuView/t I
aload 0
aload 1
putfield android/support/v7/widget/ActionMenuView/o Landroid/content/Context;
aload 0
iconst_0
putfield android/support/v7/widget/ActionMenuView/p I
return
.limit locals 4
.limit stack 3
.end method

.method static a(Landroid/view/View;IIII)I
iconst_0
istore 8
aload 0
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v7/widget/m
astore 10
iload 3
invokestatic android/view/View$MeasureSpec/getSize(I)I
iload 4
isub
iload 3
invokestatic android/view/View$MeasureSpec/getMode(I)I
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
istore 5
aload 0
instanceof android/support/v7/internal/view/menu/ActionMenuItemView
ifeq L0
aload 0
checkcast android/support/v7/internal/view/menu/ActionMenuItemView
astore 9
L1:
aload 9
ifnull L2
aload 9
invokevirtual android/support/v7/internal/view/menu/ActionMenuItemView/c()Z
ifeq L2
iconst_1
istore 4
L3:
iload 2
ifle L4
iload 4
ifeq L5
iload 2
iconst_2
if_icmplt L4
L5:
aload 0
iload 1
iload 2
imul
ldc_w -2147483648
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
iload 5
invokevirtual android/view/View/measure(II)V
aload 0
invokevirtual android/view/View/getMeasuredWidth()I
istore 6
iload 6
iload 1
idiv
istore 3
iload 3
istore 2
iload 6
iload 1
irem
ifeq L6
iload 3
iconst_1
iadd
istore 2
L6:
iload 2
istore 3
iload 4
ifeq L7
iload 2
istore 3
iload 2
iconst_2
if_icmpge L7
iconst_2
istore 3
L7:
iload 8
istore 7
aload 10
getfield android/support/v7/widget/m/a Z
ifne L8
iload 8
istore 7
iload 4
ifeq L8
iconst_1
istore 7
L8:
aload 10
iload 7
putfield android/support/v7/widget/m/d Z
aload 10
iload 3
putfield android/support/v7/widget/m/b I
aload 0
iload 3
iload 1
imul
ldc_w 1073741824
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
iload 5
invokevirtual android/view/View/measure(II)V
iload 3
ireturn
L0:
aconst_null
astore 9
goto L1
L2:
iconst_0
istore 4
goto L3
L4:
iconst_0
istore 3
goto L7
.limit locals 11
.limit stack 3
.end method

.method public static a()Landroid/support/v7/widget/m;
invokestatic android/support/v7/widget/ActionMenuView/e()Landroid/support/v7/widget/m;
astore 0
aload 0
iconst_1
putfield android/support/v7/widget/m/a Z
aload 0
areturn
.limit locals 1
.limit stack 2
.end method

.method protected static a(Landroid/view/ViewGroup$LayoutParams;)Landroid/support/v7/widget/m;
aload 0
ifnull L0
aload 0
instanceof android/support/v7/widget/m
ifeq L1
new android/support/v7/widget/m
dup
aload 0
checkcast android/support/v7/widget/m
invokespecial android/support/v7/widget/m/<init>(Landroid/support/v7/widget/m;)V
astore 0
L2:
aload 0
getfield android/support/v7/widget/m/h I
ifgt L3
aload 0
bipush 16
putfield android/support/v7/widget/m/h I
L3:
aload 0
areturn
L1:
new android/support/v7/widget/m
dup
aload 0
invokespecial android/support/v7/widget/m/<init>(Landroid/view/ViewGroup$LayoutParams;)V
astore 0
goto L2
L0:
invokestatic android/support/v7/widget/ActionMenuView/e()Landroid/support/v7/widget/m;
areturn
.limit locals 1
.limit stack 3
.end method

.method static synthetic a(Landroid/support/v7/widget/ActionMenuView;)Landroid/support/v7/widget/o;
aload 0
getfield android/support/v7/widget/ActionMenuView/u Landroid/support/v7/widget/o;
areturn
.limit locals 1
.limit stack 1
.end method

.method private a(II)V
iload 2
invokestatic android/view/View$MeasureSpec/getMode(I)I
istore 17
iload 1
invokestatic android/view/View$MeasureSpec/getSize(I)I
istore 1
iload 2
invokestatic android/view/View$MeasureSpec/getSize(I)I
istore 16
aload 0
invokevirtual android/support/v7/widget/ActionMenuView/getPaddingLeft()I
istore 6
aload 0
invokevirtual android/support/v7/widget/ActionMenuView/getPaddingRight()I
istore 7
aload 0
invokevirtual android/support/v7/widget/ActionMenuView/getPaddingTop()I
aload 0
invokevirtual android/support/v7/widget/ActionMenuView/getPaddingBottom()I
iadd
istore 14
iload 2
iload 14
bipush -2
invokestatic android/support/v7/widget/ActionMenuView/getChildMeasureSpec(III)I
istore 18
iload 1
iload 6
iload 7
iadd
isub
istore 19
iload 19
aload 0
getfield android/support/v7/widget/ActionMenuView/s I
idiv
istore 1
aload 0
getfield android/support/v7/widget/ActionMenuView/s I
istore 2
iload 1
ifne L0
aload 0
iload 19
iconst_0
invokevirtual android/support/v7/widget/ActionMenuView/setMeasuredDimension(II)V
return
L0:
aload 0
getfield android/support/v7/widget/ActionMenuView/s I
iload 19
iload 2
irem
iload 1
idiv
iadd
istore 20
iconst_0
istore 2
iconst_0
istore 10
iconst_0
istore 9
iconst_0
istore 11
iconst_0
istore 8
lconst_0
lstore 22
aload 0
invokevirtual android/support/v7/widget/ActionMenuView/getChildCount()I
istore 21
iconst_0
istore 12
L1:
iload 12
iload 21
if_icmpge L2
aload 0
iload 12
invokevirtual android/support/v7/widget/ActionMenuView/getChildAt(I)Landroid/view/View;
astore 29
aload 29
invokevirtual android/view/View/getVisibility()I
bipush 8
if_icmpeq L3
aload 29
instanceof android/support/v7/internal/view/menu/ActionMenuItemView
istore 28
iload 11
iconst_1
iadd
istore 11
iload 28
ifeq L4
aload 29
aload 0
getfield android/support/v7/widget/ActionMenuView/t I
iconst_0
aload 0
getfield android/support/v7/widget/ActionMenuView/t I
iconst_0
invokevirtual android/view/View/setPadding(IIII)V
L4:
aload 29
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v7/widget/m
astore 30
aload 30
iconst_0
putfield android/support/v7/widget/m/f Z
aload 30
iconst_0
putfield android/support/v7/widget/m/c I
aload 30
iconst_0
putfield android/support/v7/widget/m/b I
aload 30
iconst_0
putfield android/support/v7/widget/m/d Z
aload 30
iconst_0
putfield android/support/v7/widget/m/leftMargin I
aload 30
iconst_0
putfield android/support/v7/widget/m/rightMargin I
iload 28
ifeq L5
aload 29
checkcast android/support/v7/internal/view/menu/ActionMenuItemView
invokevirtual android/support/v7/internal/view/menu/ActionMenuItemView/c()Z
ifeq L5
iconst_1
istore 28
L6:
aload 30
iload 28
putfield android/support/v7/widget/m/e Z
aload 30
getfield android/support/v7/widget/m/a Z
ifeq L7
iconst_1
istore 6
L8:
aload 29
iload 20
iload 6
iload 18
iload 14
invokestatic android/support/v7/widget/ActionMenuView/a(Landroid/view/View;IIII)I
istore 13
iload 10
iload 13
invokestatic java/lang/Math/max(II)I
istore 10
aload 30
getfield android/support/v7/widget/m/d Z
ifeq L9
iload 9
iconst_1
iadd
istore 6
L10:
aload 30
getfield android/support/v7/widget/m/a Z
ifeq L11
iconst_1
istore 7
L12:
iload 1
iload 13
isub
istore 1
iload 2
aload 29
invokevirtual android/view/View/getMeasuredHeight()I
invokestatic java/lang/Math/max(II)I
istore 9
iload 13
iconst_1
if_icmpne L13
iconst_1
iload 12
ishl
i2l
lstore 24
iload 9
istore 2
iload 1
istore 8
iload 6
istore 9
iload 7
istore 13
lload 24
lload 22
lor
lstore 22
iload 11
istore 1
iload 8
istore 7
iload 2
istore 6
iload 13
istore 8
iload 10
istore 2
L14:
iload 12
iconst_1
iadd
istore 12
iload 1
istore 11
iload 7
istore 1
iload 2
istore 10
iload 6
istore 2
goto L1
L5:
iconst_0
istore 28
goto L6
L7:
iload 1
istore 6
goto L8
L2:
iload 8
ifeq L15
iload 11
iconst_2
if_icmpne L15
iconst_1
istore 13
L16:
iconst_0
istore 6
iload 1
istore 12
iload 6
istore 1
L17:
lload 22
lstore 26
iload 9
ifle L18
lload 22
lstore 26
iload 12
ifle L18
ldc_w 2147483647
istore 6
lconst_0
lstore 24
iconst_0
istore 7
iconst_0
istore 14
L19:
iload 14
iload 21
if_icmpge L20
aload 0
iload 14
invokevirtual android/support/v7/widget/ActionMenuView/getChildAt(I)Landroid/view/View;
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v7/widget/m
astore 29
aload 29
getfield android/support/v7/widget/m/d Z
ifeq L21
aload 29
getfield android/support/v7/widget/m/b I
iload 6
if_icmpge L22
aload 29
getfield android/support/v7/widget/m/b I
istore 7
iconst_1
iload 14
ishl
i2l
lstore 24
iconst_1
istore 6
L23:
iload 14
iconst_1
iadd
istore 15
iload 7
istore 14
iload 6
istore 7
iload 14
istore 6
iload 15
istore 14
goto L19
L15:
iconst_0
istore 13
goto L16
L22:
aload 29
getfield android/support/v7/widget/m/b I
iload 6
if_icmpne L21
lload 24
iconst_1
iload 14
ishl
i2l
lor
lstore 24
iload 7
iconst_1
iadd
istore 15
iload 6
istore 7
iload 15
istore 6
goto L23
L20:
lload 22
lload 24
lor
lstore 22
lload 22
lstore 26
iload 7
iload 12
if_icmpgt L18
iconst_0
istore 7
iload 12
istore 1
L24:
iload 7
iload 21
if_icmpge L25
aload 0
iload 7
invokevirtual android/support/v7/widget/ActionMenuView/getChildAt(I)Landroid/view/View;
astore 29
aload 29
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v7/widget/m
astore 30
iconst_1
iload 7
ishl
i2l
lload 24
land
lconst_0
lcmp
ifne L26
aload 30
getfield android/support/v7/widget/m/b I
iload 6
iconst_1
iadd
if_icmpne L27
lload 22
iconst_1
iload 7
ishl
i2l
lor
lstore 22
L28:
iload 7
iconst_1
iadd
istore 7
goto L24
L26:
iload 13
ifeq L29
aload 30
getfield android/support/v7/widget/m/e Z
ifeq L29
iload 1
iconst_1
if_icmpne L29
aload 29
aload 0
getfield android/support/v7/widget/ActionMenuView/t I
iload 20
iadd
iconst_0
aload 0
getfield android/support/v7/widget/ActionMenuView/t I
iconst_0
invokevirtual android/view/View/setPadding(IIII)V
L29:
aload 30
aload 30
getfield android/support/v7/widget/m/b I
iconst_1
iadd
putfield android/support/v7/widget/m/b I
aload 30
iconst_1
putfield android/support/v7/widget/m/f Z
iload 1
iconst_1
isub
istore 1
goto L28
L25:
iconst_1
istore 6
iload 1
istore 12
iload 6
istore 1
goto L17
L18:
iload 8
ifne L30
iload 11
iconst_1
if_icmpne L30
iconst_1
istore 6
L31:
iload 12
ifle L32
lload 26
lconst_0
lcmp
ifeq L32
iload 12
iload 11
iconst_1
isub
if_icmplt L33
iload 6
ifne L33
iload 10
iconst_1
if_icmple L32
L33:
lload 26
invokestatic java/lang/Long/bitCount(J)I
i2f
fstore 5
fload 5
fstore 4
iload 6
ifne L34
fload 5
fstore 3
lconst_1
lload 26
land
lconst_0
lcmp
ifeq L35
fload 5
fstore 3
aload 0
iconst_0
invokevirtual android/support/v7/widget/ActionMenuView/getChildAt(I)Landroid/view/View;
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v7/widget/m
getfield android/support/v7/widget/m/e Z
ifne L35
fload 5
ldc_w 0.5F
fsub
fstore 3
L35:
fload 3
fstore 4
iconst_1
iload 21
iconst_1
isub
ishl
i2l
lload 26
land
lconst_0
lcmp
ifeq L34
fload 3
fstore 4
aload 0
iload 21
iconst_1
isub
invokevirtual android/support/v7/widget/ActionMenuView/getChildAt(I)Landroid/view/View;
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v7/widget/m
getfield android/support/v7/widget/m/e Z
ifne L34
fload 3
ldc_w 0.5F
fsub
fstore 3
L36:
fload 3
fconst_0
fcmpl
ifle L37
iload 12
iload 20
imul
i2f
fload 3
fdiv
f2i
istore 6
L38:
iconst_0
istore 7
L39:
iload 1
istore 8
iload 7
iload 21
if_icmpge L40
iconst_1
iload 7
ishl
i2l
lload 26
land
lconst_0
lcmp
ifeq L41
aload 0
iload 7
invokevirtual android/support/v7/widget/ActionMenuView/getChildAt(I)Landroid/view/View;
astore 29
aload 29
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v7/widget/m
astore 30
aload 29
instanceof android/support/v7/internal/view/menu/ActionMenuItemView
ifeq L42
aload 30
iload 6
putfield android/support/v7/widget/m/c I
aload 30
iconst_1
putfield android/support/v7/widget/m/f Z
iload 7
ifne L43
aload 30
getfield android/support/v7/widget/m/e Z
ifne L43
aload 30
iload 6
ineg
iconst_2
idiv
putfield android/support/v7/widget/m/leftMargin I
L43:
iconst_1
istore 1
L44:
iload 7
iconst_1
iadd
istore 7
goto L39
L30:
iconst_0
istore 6
goto L31
L37:
iconst_0
istore 6
goto L38
L42:
aload 30
getfield android/support/v7/widget/m/a Z
ifeq L45
aload 30
iload 6
putfield android/support/v7/widget/m/c I
aload 30
iconst_1
putfield android/support/v7/widget/m/f Z
aload 30
iload 6
ineg
iconst_2
idiv
putfield android/support/v7/widget/m/rightMargin I
iconst_1
istore 1
goto L44
L45:
iload 7
ifeq L46
aload 30
iload 6
iconst_2
idiv
putfield android/support/v7/widget/m/leftMargin I
L46:
iload 7
iload 21
iconst_1
isub
if_icmpeq L41
aload 30
iload 6
iconst_2
idiv
putfield android/support/v7/widget/m/rightMargin I
L41:
goto L44
L32:
iload 1
istore 8
L40:
iload 8
ifeq L47
iconst_0
istore 1
L48:
iload 1
iload 21
if_icmpge L47
aload 0
iload 1
invokevirtual android/support/v7/widget/ActionMenuView/getChildAt(I)Landroid/view/View;
astore 29
aload 29
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v7/widget/m
astore 30
aload 30
getfield android/support/v7/widget/m/f Z
ifeq L49
aload 30
getfield android/support/v7/widget/m/b I
istore 6
aload 29
aload 30
getfield android/support/v7/widget/m/c I
iload 6
iload 20
imul
iadd
ldc_w 1073741824
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
iload 18
invokevirtual android/view/View/measure(II)V
L49:
iload 1
iconst_1
iadd
istore 1
goto L48
L47:
iload 17
ldc_w 1073741824
if_icmpeq L50
L51:
aload 0
iload 19
iload 2
invokevirtual android/support/v7/widget/ActionMenuView/setMeasuredDimension(II)V
return
L50:
iload 16
istore 2
goto L51
L34:
fload 4
fstore 3
goto L36
L27:
goto L28
L21:
iload 6
istore 15
iload 7
istore 6
iload 15
istore 7
goto L23
L13:
iload 11
istore 2
iload 10
istore 8
iload 9
istore 10
iload 1
istore 11
iload 2
istore 1
iload 8
istore 2
iload 6
istore 9
iload 7
istore 8
iload 10
istore 6
iload 11
istore 7
goto L14
L11:
iload 8
istore 7
goto L12
L9:
iload 9
istore 6
goto L10
L3:
iload 2
istore 6
iload 1
istore 7
iload 10
istore 2
iload 11
istore 1
goto L14
.limit locals 31
.limit stack 5
.end method

.method private a(Landroid/support/v7/internal/view/menu/y;Landroid/support/v7/internal/view/menu/j;)V
aload 0
aload 1
putfield android/support/v7/widget/ActionMenuView/f Landroid/support/v7/internal/view/menu/y;
aload 0
aload 2
putfield android/support/v7/widget/ActionMenuView/g Landroid/support/v7/internal/view/menu/j;
return
.limit locals 3
.limit stack 2
.end method

.method private a(I)Z
iconst_0
istore 3
iload 1
ifne L0
iconst_0
ireturn
L0:
aload 0
iload 1
iconst_1
isub
invokevirtual android/support/v7/widget/ActionMenuView/getChildAt(I)Landroid/view/View;
astore 4
aload 0
iload 1
invokevirtual android/support/v7/widget/ActionMenuView/getChildAt(I)Landroid/view/View;
astore 5
iload 3
istore 2
iload 1
aload 0
invokevirtual android/support/v7/widget/ActionMenuView/getChildCount()I
if_icmpge L1
iload 3
istore 2
aload 4
instanceof android/support/v7/widget/k
ifeq L1
aload 4
checkcast android/support/v7/widget/k
invokeinterface android/support/v7/widget/k/e()Z 0
iconst_0
ior
istore 2
L1:
iload 1
ifle L2
aload 5
instanceof android/support/v7/widget/k
ifeq L2
aload 5
checkcast android/support/v7/widget/k
invokeinterface android/support/v7/widget/k/d()Z 0
iload 2
ior
ireturn
L2:
iload 2
ireturn
.limit locals 6
.limit stack 3
.end method

.method static synthetic b(Landroid/support/v7/widget/ActionMenuView;)Landroid/support/v7/internal/view/menu/j;
aload 0
getfield android/support/v7/widget/ActionMenuView/g Landroid/support/v7/internal/view/menu/j;
areturn
.limit locals 1
.limit stack 1
.end method

.method private b(Landroid/util/AttributeSet;)Landroid/support/v7/widget/m;
new android/support/v7/widget/m
dup
aload 0
invokevirtual android/support/v7/widget/ActionMenuView/getContext()Landroid/content/Context;
aload 1
invokespecial android/support/v7/widget/m/<init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
areturn
.limit locals 2
.limit stack 4
.end method

.method private d()Z
aload 0
getfield android/support/v7/widget/ActionMenuView/d Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private static e()Landroid/support/v7/widget/m;
new android/support/v7/widget/m
dup
invokespecial android/support/v7/widget/m/<init>()V
astore 0
aload 0
bipush 16
putfield android/support/v7/widget/m/h I
aload 0
areturn
.limit locals 1
.limit stack 2
.end method

.method private f()Landroid/support/v7/internal/view/menu/i;
aload 0
getfield android/support/v7/widget/ActionMenuView/c Landroid/support/v7/internal/view/menu/i;
areturn
.limit locals 1
.limit stack 1
.end method

.method private g()Z
aload 0
getfield android/support/v7/widget/ActionMenuView/e Landroid/support/v7/widget/ActionMenuPresenter;
ifnull L0
aload 0
getfield android/support/v7/widget/ActionMenuView/e Landroid/support/v7/widget/ActionMenuPresenter;
invokevirtual android/support/v7/widget/ActionMenuPresenter/e()Z
ifeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method private h()Z
aload 0
getfield android/support/v7/widget/ActionMenuView/e Landroid/support/v7/widget/ActionMenuPresenter;
ifnull L0
aload 0
getfield android/support/v7/widget/ActionMenuView/e Landroid/support/v7/widget/ActionMenuPresenter;
invokevirtual android/support/v7/widget/ActionMenuPresenter/f()Z
ifeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method private i()Z
aload 0
getfield android/support/v7/widget/ActionMenuView/e Landroid/support/v7/widget/ActionMenuPresenter;
ifnull L0
aload 0
getfield android/support/v7/widget/ActionMenuView/e Landroid/support/v7/widget/ActionMenuPresenter;
invokevirtual android/support/v7/widget/ActionMenuPresenter/i()Z
ifeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method private j()Z
aload 0
getfield android/support/v7/widget/ActionMenuView/e Landroid/support/v7/widget/ActionMenuPresenter;
ifnull L0
aload 0
getfield android/support/v7/widget/ActionMenuView/e Landroid/support/v7/widget/ActionMenuPresenter;
invokevirtual android/support/v7/widget/ActionMenuPresenter/j()Z
ifeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final synthetic a(Landroid/util/AttributeSet;)Landroid/support/v7/widget/al;
aload 0
aload 1
invokespecial android/support/v7/widget/ActionMenuView/b(Landroid/util/AttributeSet;)Landroid/support/v7/widget/m;
areturn
.limit locals 2
.limit stack 2
.end method

.method public final a(Landroid/support/v7/internal/view/menu/i;)V
aload 0
aload 1
putfield android/support/v7/widget/ActionMenuView/c Landroid/support/v7/internal/view/menu/i;
return
.limit locals 2
.limit stack 2
.end method

.method public final a(Landroid/support/v7/internal/view/menu/m;)Z
aload 0
getfield android/support/v7/widget/ActionMenuView/c Landroid/support/v7/internal/view/menu/i;
aload 1
aconst_null
iconst_0
invokevirtual android/support/v7/internal/view/menu/i/a(Landroid/view/MenuItem;Landroid/support/v7/internal/view/menu/x;I)Z
ireturn
.limit locals 2
.limit stack 4
.end method

.method protected final synthetic b(Landroid/view/ViewGroup$LayoutParams;)Landroid/support/v7/widget/al;
aload 1
invokestatic android/support/v7/widget/ActionMenuView/a(Landroid/view/ViewGroup$LayoutParams;)Landroid/support/v7/widget/m;
areturn
.limit locals 2
.limit stack 1
.end method

.method public final b()V
aload 0
getfield android/support/v7/widget/ActionMenuView/e Landroid/support/v7/widget/ActionMenuPresenter;
ifnull L0
aload 0
getfield android/support/v7/widget/ActionMenuView/e Landroid/support/v7/widget/ActionMenuPresenter;
invokevirtual android/support/v7/widget/ActionMenuPresenter/g()Z
pop
L0:
return
.limit locals 1
.limit stack 1
.end method

.method protected final synthetic c()Landroid/support/v7/widget/al;
invokestatic android/support/v7/widget/ActionMenuView/e()Landroid/support/v7/widget/m;
areturn
.limit locals 1
.limit stack 1
.end method

.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
aload 1
ifnull L0
aload 1
instanceof android/support/v7/widget/m
ifeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 1
.end method

.method public dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z
iconst_0
ireturn
.limit locals 2
.limit stack 1
.end method

.method protected synthetic generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
invokestatic android/support/v7/widget/ActionMenuView/e()Landroid/support/v7/widget/m;
areturn
.limit locals 1
.limit stack 1
.end method

.method public synthetic generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
aload 0
aload 1
invokespecial android/support/v7/widget/ActionMenuView/b(Landroid/util/AttributeSet;)Landroid/support/v7/widget/m;
areturn
.limit locals 2
.limit stack 2
.end method

.method protected synthetic generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
aload 1
invokestatic android/support/v7/widget/ActionMenuView/a(Landroid/view/ViewGroup$LayoutParams;)Landroid/support/v7/widget/m;
areturn
.limit locals 2
.limit stack 1
.end method

.method public getMenu()Landroid/view/Menu;
aload 0
getfield android/support/v7/widget/ActionMenuView/c Landroid/support/v7/internal/view/menu/i;
ifnonnull L0
aload 0
invokevirtual android/support/v7/widget/ActionMenuView/getContext()Landroid/content/Context;
astore 1
aload 0
new android/support/v7/internal/view/menu/i
dup
aload 1
invokespecial android/support/v7/internal/view/menu/i/<init>(Landroid/content/Context;)V
putfield android/support/v7/widget/ActionMenuView/c Landroid/support/v7/internal/view/menu/i;
aload 0
getfield android/support/v7/widget/ActionMenuView/c Landroid/support/v7/internal/view/menu/i;
new android/support/v7/widget/n
dup
aload 0
iconst_0
invokespecial android/support/v7/widget/n/<init>(Landroid/support/v7/widget/ActionMenuView;B)V
invokevirtual android/support/v7/internal/view/menu/i/a(Landroid/support/v7/internal/view/menu/j;)V
aload 0
new android/support/v7/widget/ActionMenuPresenter
dup
aload 1
invokespecial android/support/v7/widget/ActionMenuPresenter/<init>(Landroid/content/Context;)V
putfield android/support/v7/widget/ActionMenuView/e Landroid/support/v7/widget/ActionMenuPresenter;
aload 0
getfield android/support/v7/widget/ActionMenuView/e Landroid/support/v7/widget/ActionMenuPresenter;
invokevirtual android/support/v7/widget/ActionMenuPresenter/d()V
aload 0
getfield android/support/v7/widget/ActionMenuView/e Landroid/support/v7/widget/ActionMenuPresenter;
astore 2
aload 0
getfield android/support/v7/widget/ActionMenuView/f Landroid/support/v7/internal/view/menu/y;
ifnull L1
aload 0
getfield android/support/v7/widget/ActionMenuView/f Landroid/support/v7/internal/view/menu/y;
astore 1
L2:
aload 2
aload 1
putfield android/support/v7/internal/view/menu/d/f Landroid/support/v7/internal/view/menu/y;
aload 0
getfield android/support/v7/widget/ActionMenuView/c Landroid/support/v7/internal/view/menu/i;
aload 0
getfield android/support/v7/widget/ActionMenuView/e Landroid/support/v7/widget/ActionMenuPresenter;
aload 0
getfield android/support/v7/widget/ActionMenuView/o Landroid/content/Context;
invokevirtual android/support/v7/internal/view/menu/i/a(Landroid/support/v7/internal/view/menu/x;Landroid/content/Context;)V
aload 0
getfield android/support/v7/widget/ActionMenuView/e Landroid/support/v7/widget/ActionMenuPresenter;
aload 0
invokevirtual android/support/v7/widget/ActionMenuPresenter/a(Landroid/support/v7/widget/ActionMenuView;)V
L0:
aload 0
getfield android/support/v7/widget/ActionMenuView/c Landroid/support/v7/internal/view/menu/i;
areturn
L1:
new android/support/v7/widget/l
dup
aload 0
iconst_0
invokespecial android/support/v7/widget/l/<init>(Landroid/support/v7/widget/ActionMenuView;B)V
astore 1
goto L2
.limit locals 3
.limit stack 5
.end method

.method public getOverflowIcon()Landroid/graphics/drawable/Drawable;
.annotation invisible Landroid/support/a/z;
.end annotation
aload 0
invokevirtual android/support/v7/widget/ActionMenuView/getMenu()Landroid/view/Menu;
pop
aload 0
getfield android/support/v7/widget/ActionMenuView/e Landroid/support/v7/widget/ActionMenuPresenter;
astore 1
aload 1
getfield android/support/v7/widget/ActionMenuPresenter/i Landroid/support/v7/widget/e;
ifnull L0
aload 1
getfield android/support/v7/widget/ActionMenuPresenter/i Landroid/support/v7/widget/e;
invokevirtual android/support/v7/widget/e/getDrawable()Landroid/graphics/drawable/Drawable;
areturn
L0:
aload 1
getfield android/support/v7/widget/ActionMenuPresenter/k Z
ifeq L1
aload 1
getfield android/support/v7/widget/ActionMenuPresenter/j Landroid/graphics/drawable/Drawable;
areturn
L1:
aconst_null
areturn
.limit locals 2
.limit stack 1
.end method

.method public getPopupTheme()I
aload 0
getfield android/support/v7/widget/ActionMenuView/p I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getWindowAnimations()I
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
getstatic android/os/Build$VERSION/SDK_INT I
bipush 8
if_icmplt L0
aload 0
aload 1
invokespecial android/support/v7/widget/aj/onConfigurationChanged(Landroid/content/res/Configuration;)V
L0:
aload 0
getfield android/support/v7/widget/ActionMenuView/e Landroid/support/v7/widget/ActionMenuPresenter;
ifnull L1
aload 0
getfield android/support/v7/widget/ActionMenuView/e Landroid/support/v7/widget/ActionMenuPresenter;
iconst_0
invokevirtual android/support/v7/widget/ActionMenuPresenter/a(Z)V
aload 0
getfield android/support/v7/widget/ActionMenuView/e Landroid/support/v7/widget/ActionMenuPresenter;
invokevirtual android/support/v7/widget/ActionMenuPresenter/i()Z
ifeq L1
aload 0
getfield android/support/v7/widget/ActionMenuView/e Landroid/support/v7/widget/ActionMenuPresenter;
invokevirtual android/support/v7/widget/ActionMenuPresenter/f()Z
pop
aload 0
getfield android/support/v7/widget/ActionMenuView/e Landroid/support/v7/widget/ActionMenuPresenter;
invokevirtual android/support/v7/widget/ActionMenuPresenter/e()Z
pop
L1:
return
.limit locals 2
.limit stack 2
.end method

.method public onDetachedFromWindow()V
aload 0
invokespecial android/support/v7/widget/aj/onDetachedFromWindow()V
aload 0
invokevirtual android/support/v7/widget/ActionMenuView/b()V
return
.limit locals 1
.limit stack 1
.end method

.method protected onLayout(ZIIII)V
aload 0
getfield android/support/v7/widget/ActionMenuView/q Z
ifne L0
aload 0
iload 1
iload 2
iload 3
iload 4
iload 5
invokespecial android/support/v7/widget/aj/onLayout(ZIIII)V
L1:
return
L0:
aload 0
invokevirtual android/support/v7/widget/ActionMenuView/getChildCount()I
istore 11
iload 5
iload 3
isub
iconst_2
idiv
istore 10
aload 0
invokevirtual android/support/v7/widget/ActionMenuView/getDividerWidth()I
istore 12
iconst_0
istore 3
iload 4
iload 2
isub
aload 0
invokevirtual android/support/v7/widget/ActionMenuView/getPaddingRight()I
isub
aload 0
invokevirtual android/support/v7/widget/ActionMenuView/getPaddingLeft()I
isub
istore 5
iconst_0
istore 7
aload 0
invokestatic android/support/v7/internal/widget/bd/a(Landroid/view/View;)Z
istore 1
iconst_0
istore 6
L2:
iload 6
iload 11
if_icmpge L3
aload 0
iload 6
invokevirtual android/support/v7/widget/ActionMenuView/getChildAt(I)Landroid/view/View;
astore 15
aload 15
invokevirtual android/view/View/getVisibility()I
bipush 8
if_icmpeq L4
aload 15
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v7/widget/m
astore 16
aload 16
getfield android/support/v7/widget/m/a Z
ifeq L5
aload 15
invokevirtual android/view/View/getMeasuredWidth()I
istore 8
iload 8
istore 7
aload 0
iload 6
invokespecial android/support/v7/widget/ActionMenuView/a(I)Z
ifeq L6
iload 8
iload 12
iadd
istore 7
L6:
aload 15
invokevirtual android/view/View/getMeasuredHeight()I
istore 13
iload 1
ifeq L7
aload 0
invokevirtual android/support/v7/widget/ActionMenuView/getPaddingLeft()I
istore 8
aload 16
getfield android/support/v7/widget/m/leftMargin I
iload 8
iadd
istore 8
iload 8
iload 7
iadd
istore 9
L8:
iload 10
iload 13
iconst_2
idiv
isub
istore 14
aload 15
iload 8
iload 14
iload 9
iload 13
iload 14
iadd
invokevirtual android/view/View/layout(IIII)V
iload 5
iload 7
isub
istore 8
iconst_1
istore 7
iload 3
istore 5
iload 8
istore 3
L9:
iload 6
iconst_1
iadd
istore 8
iload 5
istore 6
iload 3
istore 5
iload 6
istore 3
iload 8
istore 6
goto L2
L7:
aload 0
invokevirtual android/support/v7/widget/ActionMenuView/getWidth()I
aload 0
invokevirtual android/support/v7/widget/ActionMenuView/getPaddingRight()I
isub
aload 16
getfield android/support/v7/widget/m/rightMargin I
isub
istore 9
iload 9
iload 7
isub
istore 8
goto L8
L5:
aload 15
invokevirtual android/view/View/getMeasuredWidth()I
istore 8
aload 16
getfield android/support/v7/widget/m/leftMargin I
istore 9
iload 5
aload 16
getfield android/support/v7/widget/m/rightMargin I
iload 8
iload 9
iadd
iadd
isub
istore 5
aload 0
iload 6
invokespecial android/support/v7/widget/ActionMenuView/a(I)Z
pop
iload 3
iconst_1
iadd
istore 8
iload 5
istore 3
iload 8
istore 5
goto L9
L3:
iload 11
iconst_1
if_icmpne L10
iload 7
ifne L10
aload 0
iconst_0
invokevirtual android/support/v7/widget/ActionMenuView/getChildAt(I)Landroid/view/View;
astore 15
aload 15
invokevirtual android/view/View/getMeasuredWidth()I
istore 3
aload 15
invokevirtual android/view/View/getMeasuredHeight()I
istore 5
iload 4
iload 2
isub
iconst_2
idiv
iload 3
iconst_2
idiv
isub
istore 2
iload 10
iload 5
iconst_2
idiv
isub
istore 4
aload 15
iload 2
iload 4
iload 3
iload 2
iadd
iload 5
iload 4
iadd
invokevirtual android/view/View/layout(IIII)V
return
L10:
iload 7
ifeq L11
iconst_0
istore 2
L12:
iload 3
iload 2
isub
istore 2
iload 2
ifle L13
iload 5
iload 2
idiv
istore 2
L14:
iconst_0
iload 2
invokestatic java/lang/Math/max(II)I
istore 4
iload 1
ifeq L15
aload 0
invokevirtual android/support/v7/widget/ActionMenuView/getWidth()I
aload 0
invokevirtual android/support/v7/widget/ActionMenuView/getPaddingRight()I
isub
istore 2
iconst_0
istore 3
L16:
iload 3
iload 11
if_icmpge L1
aload 0
iload 3
invokevirtual android/support/v7/widget/ActionMenuView/getChildAt(I)Landroid/view/View;
astore 15
aload 15
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v7/widget/m
astore 16
aload 15
invokevirtual android/view/View/getVisibility()I
bipush 8
if_icmpeq L17
aload 16
getfield android/support/v7/widget/m/a Z
ifne L17
iload 2
aload 16
getfield android/support/v7/widget/m/rightMargin I
isub
istore 2
aload 15
invokevirtual android/view/View/getMeasuredWidth()I
istore 5
aload 15
invokevirtual android/view/View/getMeasuredHeight()I
istore 6
iload 10
iload 6
iconst_2
idiv
isub
istore 7
aload 15
iload 2
iload 5
isub
iload 7
iload 2
iload 6
iload 7
iadd
invokevirtual android/view/View/layout(IIII)V
iload 2
aload 16
getfield android/support/v7/widget/m/leftMargin I
iload 5
iadd
iload 4
iadd
isub
istore 2
L18:
iload 3
iconst_1
iadd
istore 3
goto L16
L11:
iconst_1
istore 2
goto L12
L13:
iconst_0
istore 2
goto L14
L15:
aload 0
invokevirtual android/support/v7/widget/ActionMenuView/getPaddingLeft()I
istore 2
iconst_0
istore 3
L19:
iload 3
iload 11
if_icmpge L1
aload 0
iload 3
invokevirtual android/support/v7/widget/ActionMenuView/getChildAt(I)Landroid/view/View;
astore 15
aload 15
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v7/widget/m
astore 16
aload 15
invokevirtual android/view/View/getVisibility()I
bipush 8
if_icmpeq L20
aload 16
getfield android/support/v7/widget/m/a Z
ifne L20
iload 2
aload 16
getfield android/support/v7/widget/m/leftMargin I
iadd
istore 2
aload 15
invokevirtual android/view/View/getMeasuredWidth()I
istore 5
aload 15
invokevirtual android/view/View/getMeasuredHeight()I
istore 6
iload 10
iload 6
iconst_2
idiv
isub
istore 7
aload 15
iload 2
iload 7
iload 2
iload 5
iadd
iload 6
iload 7
iadd
invokevirtual android/view/View/layout(IIII)V
aload 16
getfield android/support/v7/widget/m/rightMargin I
iload 5
iadd
iload 4
iadd
iload 2
iadd
istore 2
L21:
iload 3
iconst_1
iadd
istore 3
goto L19
L20:
goto L21
L17:
goto L18
L4:
iload 3
istore 8
iload 5
istore 3
iload 8
istore 5
goto L9
.limit locals 17
.limit stack 6
.end method

.method protected onMeasure(II)V
aload 0
getfield android/support/v7/widget/ActionMenuView/q Z
istore 23
iload 1
invokestatic android/view/View$MeasureSpec/getMode(I)I
ldc_w 1073741824
if_icmpne L0
iconst_1
istore 22
L1:
aload 0
iload 22
putfield android/support/v7/widget/ActionMenuView/q Z
iload 23
aload 0
getfield android/support/v7/widget/ActionMenuView/q Z
if_icmpeq L2
aload 0
iconst_0
putfield android/support/v7/widget/ActionMenuView/r I
L2:
iload 1
invokestatic android/view/View$MeasureSpec/getSize(I)I
istore 6
aload 0
getfield android/support/v7/widget/ActionMenuView/q Z
ifeq L3
aload 0
getfield android/support/v7/widget/ActionMenuView/c Landroid/support/v7/internal/view/menu/i;
ifnull L3
iload 6
aload 0
getfield android/support/v7/widget/ActionMenuView/r I
if_icmpeq L3
aload 0
iload 6
putfield android/support/v7/widget/ActionMenuView/r I
aload 0
getfield android/support/v7/widget/ActionMenuView/c Landroid/support/v7/internal/view/menu/i;
iconst_1
invokevirtual android/support/v7/internal/view/menu/i/c(Z)V
L3:
aload 0
invokevirtual android/support/v7/widget/ActionMenuView/getChildCount()I
istore 7
aload 0
getfield android/support/v7/widget/ActionMenuView/q Z
ifeq L4
iload 7
ifle L4
iload 2
invokestatic android/view/View$MeasureSpec/getMode(I)I
istore 17
iload 1
invokestatic android/view/View$MeasureSpec/getSize(I)I
istore 1
iload 2
invokestatic android/view/View$MeasureSpec/getSize(I)I
istore 16
aload 0
invokevirtual android/support/v7/widget/ActionMenuView/getPaddingLeft()I
istore 6
aload 0
invokevirtual android/support/v7/widget/ActionMenuView/getPaddingRight()I
istore 7
aload 0
invokevirtual android/support/v7/widget/ActionMenuView/getPaddingTop()I
aload 0
invokevirtual android/support/v7/widget/ActionMenuView/getPaddingBottom()I
iadd
istore 14
iload 2
iload 14
bipush -2
invokestatic android/support/v7/widget/ActionMenuView/getChildMeasureSpec(III)I
istore 18
iload 1
iload 6
iload 7
iadd
isub
istore 19
iload 19
aload 0
getfield android/support/v7/widget/ActionMenuView/s I
idiv
istore 1
aload 0
getfield android/support/v7/widget/ActionMenuView/s I
istore 2
iload 1
ifne L5
aload 0
iload 19
iconst_0
invokevirtual android/support/v7/widget/ActionMenuView/setMeasuredDimension(II)V
return
L0:
iconst_0
istore 22
goto L1
L5:
aload 0
getfield android/support/v7/widget/ActionMenuView/s I
iload 19
iload 2
irem
iload 1
idiv
iadd
istore 20
iconst_0
istore 2
iconst_0
istore 10
iconst_0
istore 9
iconst_0
istore 11
iconst_0
istore 8
lconst_0
lstore 24
aload 0
invokevirtual android/support/v7/widget/ActionMenuView/getChildCount()I
istore 21
iconst_0
istore 12
L6:
iload 12
iload 21
if_icmpge L7
aload 0
iload 12
invokevirtual android/support/v7/widget/ActionMenuView/getChildAt(I)Landroid/view/View;
astore 30
aload 30
invokevirtual android/view/View/getVisibility()I
bipush 8
if_icmpeq L8
aload 30
instanceof android/support/v7/internal/view/menu/ActionMenuItemView
istore 22
iload 11
iconst_1
iadd
istore 11
iload 22
ifeq L9
aload 30
aload 0
getfield android/support/v7/widget/ActionMenuView/t I
iconst_0
aload 0
getfield android/support/v7/widget/ActionMenuView/t I
iconst_0
invokevirtual android/view/View/setPadding(IIII)V
L9:
aload 30
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v7/widget/m
astore 31
aload 31
iconst_0
putfield android/support/v7/widget/m/f Z
aload 31
iconst_0
putfield android/support/v7/widget/m/c I
aload 31
iconst_0
putfield android/support/v7/widget/m/b I
aload 31
iconst_0
putfield android/support/v7/widget/m/d Z
aload 31
iconst_0
putfield android/support/v7/widget/m/leftMargin I
aload 31
iconst_0
putfield android/support/v7/widget/m/rightMargin I
iload 22
ifeq L10
aload 30
checkcast android/support/v7/internal/view/menu/ActionMenuItemView
invokevirtual android/support/v7/internal/view/menu/ActionMenuItemView/c()Z
ifeq L10
iconst_1
istore 22
L11:
aload 31
iload 22
putfield android/support/v7/widget/m/e Z
aload 31
getfield android/support/v7/widget/m/a Z
ifeq L12
iconst_1
istore 6
L13:
aload 30
iload 20
iload 6
iload 18
iload 14
invokestatic android/support/v7/widget/ActionMenuView/a(Landroid/view/View;IIII)I
istore 13
iload 10
iload 13
invokestatic java/lang/Math/max(II)I
istore 10
aload 31
getfield android/support/v7/widget/m/d Z
ifeq L14
iload 9
iconst_1
iadd
istore 6
L15:
aload 31
getfield android/support/v7/widget/m/a Z
ifeq L16
iconst_1
istore 7
L17:
iload 1
iload 13
isub
istore 1
iload 2
aload 30
invokevirtual android/view/View/getMeasuredHeight()I
invokestatic java/lang/Math/max(II)I
istore 9
iload 13
iconst_1
if_icmpne L18
iconst_1
iload 12
ishl
i2l
lstore 26
iload 9
istore 2
iload 1
istore 8
iload 6
istore 9
iload 7
istore 13
lload 26
lload 24
lor
lstore 24
iload 11
istore 1
iload 8
istore 7
iload 2
istore 6
iload 13
istore 8
iload 10
istore 2
L19:
iload 12
iconst_1
iadd
istore 12
iload 1
istore 11
iload 7
istore 1
iload 2
istore 10
iload 6
istore 2
goto L6
L10:
iconst_0
istore 22
goto L11
L12:
iload 1
istore 6
goto L13
L7:
iload 8
ifeq L20
iload 11
iconst_2
if_icmpne L20
iconst_1
istore 13
L21:
iconst_0
istore 6
iload 1
istore 12
iload 6
istore 1
L22:
lload 24
lstore 28
iload 9
ifle L23
lload 24
lstore 28
iload 12
ifle L23
ldc_w 2147483647
istore 6
lconst_0
lstore 26
iconst_0
istore 7
iconst_0
istore 14
L24:
iload 14
iload 21
if_icmpge L25
aload 0
iload 14
invokevirtual android/support/v7/widget/ActionMenuView/getChildAt(I)Landroid/view/View;
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v7/widget/m
astore 30
aload 30
getfield android/support/v7/widget/m/d Z
ifeq L26
aload 30
getfield android/support/v7/widget/m/b I
iload 6
if_icmpge L27
aload 30
getfield android/support/v7/widget/m/b I
istore 7
iconst_1
iload 14
ishl
i2l
lstore 26
iconst_1
istore 6
L28:
iload 14
iconst_1
iadd
istore 15
iload 7
istore 14
iload 6
istore 7
iload 14
istore 6
iload 15
istore 14
goto L24
L20:
iconst_0
istore 13
goto L21
L27:
aload 30
getfield android/support/v7/widget/m/b I
iload 6
if_icmpne L26
lload 26
iconst_1
iload 14
ishl
i2l
lor
lstore 26
iload 7
iconst_1
iadd
istore 15
iload 6
istore 7
iload 15
istore 6
goto L28
L25:
lload 24
lload 26
lor
lstore 24
lload 24
lstore 28
iload 7
iload 12
if_icmpgt L23
iconst_0
istore 7
iload 12
istore 1
L29:
iload 7
iload 21
if_icmpge L30
aload 0
iload 7
invokevirtual android/support/v7/widget/ActionMenuView/getChildAt(I)Landroid/view/View;
astore 30
aload 30
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v7/widget/m
astore 31
iconst_1
iload 7
ishl
i2l
lload 26
land
lconst_0
lcmp
ifne L31
aload 31
getfield android/support/v7/widget/m/b I
iload 6
iconst_1
iadd
if_icmpne L32
lload 24
iconst_1
iload 7
ishl
i2l
lor
lstore 24
L33:
iload 7
iconst_1
iadd
istore 7
goto L29
L31:
iload 13
ifeq L34
aload 31
getfield android/support/v7/widget/m/e Z
ifeq L34
iload 1
iconst_1
if_icmpne L34
aload 30
aload 0
getfield android/support/v7/widget/ActionMenuView/t I
iload 20
iadd
iconst_0
aload 0
getfield android/support/v7/widget/ActionMenuView/t I
iconst_0
invokevirtual android/view/View/setPadding(IIII)V
L34:
aload 31
aload 31
getfield android/support/v7/widget/m/b I
iconst_1
iadd
putfield android/support/v7/widget/m/b I
aload 31
iconst_1
putfield android/support/v7/widget/m/f Z
iload 1
iconst_1
isub
istore 1
goto L33
L30:
iconst_1
istore 6
iload 1
istore 12
iload 6
istore 1
goto L22
L23:
iload 8
ifne L35
iload 11
iconst_1
if_icmpne L35
iconst_1
istore 6
L36:
iload 12
ifle L37
lload 28
lconst_0
lcmp
ifeq L37
iload 12
iload 11
iconst_1
isub
if_icmplt L38
iload 6
ifne L38
iload 10
iconst_1
if_icmple L37
L38:
lload 28
invokestatic java/lang/Long/bitCount(J)I
i2f
fstore 5
fload 5
fstore 4
iload 6
ifne L39
fload 5
fstore 3
lconst_1
lload 28
land
lconst_0
lcmp
ifeq L40
fload 5
fstore 3
aload 0
iconst_0
invokevirtual android/support/v7/widget/ActionMenuView/getChildAt(I)Landroid/view/View;
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v7/widget/m
getfield android/support/v7/widget/m/e Z
ifne L40
fload 5
ldc_w 0.5F
fsub
fstore 3
L40:
fload 3
fstore 4
iconst_1
iload 21
iconst_1
isub
ishl
i2l
lload 28
land
lconst_0
lcmp
ifeq L39
fload 3
fstore 4
aload 0
iload 21
iconst_1
isub
invokevirtual android/support/v7/widget/ActionMenuView/getChildAt(I)Landroid/view/View;
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v7/widget/m
getfield android/support/v7/widget/m/e Z
ifne L39
fload 3
ldc_w 0.5F
fsub
fstore 3
L41:
fload 3
fconst_0
fcmpl
ifle L42
iload 12
iload 20
imul
i2f
fload 3
fdiv
f2i
istore 6
L43:
iconst_0
istore 7
L44:
iload 1
istore 8
iload 7
iload 21
if_icmpge L45
iconst_1
iload 7
ishl
i2l
lload 28
land
lconst_0
lcmp
ifeq L46
aload 0
iload 7
invokevirtual android/support/v7/widget/ActionMenuView/getChildAt(I)Landroid/view/View;
astore 30
aload 30
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v7/widget/m
astore 31
aload 30
instanceof android/support/v7/internal/view/menu/ActionMenuItemView
ifeq L47
aload 31
iload 6
putfield android/support/v7/widget/m/c I
aload 31
iconst_1
putfield android/support/v7/widget/m/f Z
iload 7
ifne L48
aload 31
getfield android/support/v7/widget/m/e Z
ifne L48
aload 31
iload 6
ineg
iconst_2
idiv
putfield android/support/v7/widget/m/leftMargin I
L48:
iconst_1
istore 1
L49:
iload 7
iconst_1
iadd
istore 7
goto L44
L35:
iconst_0
istore 6
goto L36
L42:
iconst_0
istore 6
goto L43
L47:
aload 31
getfield android/support/v7/widget/m/a Z
ifeq L50
aload 31
iload 6
putfield android/support/v7/widget/m/c I
aload 31
iconst_1
putfield android/support/v7/widget/m/f Z
aload 31
iload 6
ineg
iconst_2
idiv
putfield android/support/v7/widget/m/rightMargin I
iconst_1
istore 1
goto L49
L50:
iload 7
ifeq L51
aload 31
iload 6
iconst_2
idiv
putfield android/support/v7/widget/m/leftMargin I
L51:
iload 7
iload 21
iconst_1
isub
if_icmpeq L46
aload 31
iload 6
iconst_2
idiv
putfield android/support/v7/widget/m/rightMargin I
L46:
goto L49
L37:
iload 1
istore 8
L45:
iload 8
ifeq L52
iconst_0
istore 1
L53:
iload 1
iload 21
if_icmpge L52
aload 0
iload 1
invokevirtual android/support/v7/widget/ActionMenuView/getChildAt(I)Landroid/view/View;
astore 30
aload 30
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v7/widget/m
astore 31
aload 31
getfield android/support/v7/widget/m/f Z
ifeq L54
aload 31
getfield android/support/v7/widget/m/b I
istore 6
aload 30
aload 31
getfield android/support/v7/widget/m/c I
iload 6
iload 20
imul
iadd
ldc_w 1073741824
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
iload 18
invokevirtual android/view/View/measure(II)V
L54:
iload 1
iconst_1
iadd
istore 1
goto L53
L52:
iload 17
ldc_w 1073741824
if_icmpeq L55
L56:
aload 0
iload 19
iload 2
invokevirtual android/support/v7/widget/ActionMenuView/setMeasuredDimension(II)V
return
L4:
iconst_0
istore 6
L57:
iload 6
iload 7
if_icmpge L58
aload 0
iload 6
invokevirtual android/support/v7/widget/ActionMenuView/getChildAt(I)Landroid/view/View;
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v7/widget/m
astore 30
aload 30
iconst_0
putfield android/support/v7/widget/m/rightMargin I
aload 30
iconst_0
putfield android/support/v7/widget/m/leftMargin I
iload 6
iconst_1
iadd
istore 6
goto L57
L58:
aload 0
iload 1
iload 2
invokespecial android/support/v7/widget/aj/onMeasure(II)V
return
L55:
iload 16
istore 2
goto L56
L39:
fload 4
fstore 3
goto L41
L32:
goto L33
L26:
iload 6
istore 15
iload 7
istore 6
iload 15
istore 7
goto L28
L18:
iload 11
istore 2
iload 10
istore 8
iload 9
istore 10
iload 1
istore 11
iload 2
istore 1
iload 8
istore 2
iload 6
istore 9
iload 7
istore 8
iload 10
istore 6
iload 11
istore 7
goto L19
L16:
iload 8
istore 7
goto L17
L14:
iload 9
istore 6
goto L15
L8:
iload 2
istore 6
iload 1
istore 7
iload 10
istore 2
iload 11
istore 1
goto L19
.limit locals 32
.limit stack 5
.end method

.method public setExpandedActionViewsExclusive(Z)V
aload 0
getfield android/support/v7/widget/ActionMenuView/e Landroid/support/v7/widget/ActionMenuPresenter;
iload 1
putfield android/support/v7/widget/ActionMenuPresenter/o Z
return
.limit locals 2
.limit stack 2
.end method

.method public setOnMenuItemClickListener(Landroid/support/v7/widget/o;)V
aload 0
aload 1
putfield android/support/v7/widget/ActionMenuView/u Landroid/support/v7/widget/o;
return
.limit locals 2
.limit stack 2
.end method

.method public setOverflowIcon(Landroid/graphics/drawable/Drawable;)V
.annotation invisibleparam 1 Landroid/support/a/z;
.end annotation
aload 0
invokevirtual android/support/v7/widget/ActionMenuView/getMenu()Landroid/view/Menu;
pop
aload 0
getfield android/support/v7/widget/ActionMenuView/e Landroid/support/v7/widget/ActionMenuPresenter;
astore 2
aload 2
getfield android/support/v7/widget/ActionMenuPresenter/i Landroid/support/v7/widget/e;
ifnull L0
aload 2
getfield android/support/v7/widget/ActionMenuPresenter/i Landroid/support/v7/widget/e;
aload 1
invokevirtual android/support/v7/widget/e/setImageDrawable(Landroid/graphics/drawable/Drawable;)V
return
L0:
aload 2
iconst_1
putfield android/support/v7/widget/ActionMenuPresenter/k Z
aload 2
aload 1
putfield android/support/v7/widget/ActionMenuPresenter/j Landroid/graphics/drawable/Drawable;
return
.limit locals 3
.limit stack 2
.end method

.method public setOverflowReserved(Z)V
aload 0
iload 1
putfield android/support/v7/widget/ActionMenuView/d Z
return
.limit locals 2
.limit stack 2
.end method

.method public setPopupTheme(I)V
.annotation invisibleparam 1 Landroid/support/a/ai;
.end annotation
aload 0
getfield android/support/v7/widget/ActionMenuView/p I
iload 1
if_icmpeq L0
aload 0
iload 1
putfield android/support/v7/widget/ActionMenuView/p I
iload 1
ifne L1
aload 0
aload 0
invokevirtual android/support/v7/widget/ActionMenuView/getContext()Landroid/content/Context;
putfield android/support/v7/widget/ActionMenuView/o Landroid/content/Context;
L0:
return
L1:
aload 0
new android/view/ContextThemeWrapper
dup
aload 0
invokevirtual android/support/v7/widget/ActionMenuView/getContext()Landroid/content/Context;
iload 1
invokespecial android/view/ContextThemeWrapper/<init>(Landroid/content/Context;I)V
putfield android/support/v7/widget/ActionMenuView/o Landroid/content/Context;
return
.limit locals 2
.limit stack 5
.end method

.method public setPresenter(Landroid/support/v7/widget/ActionMenuPresenter;)V
aload 0
aload 1
putfield android/support/v7/widget/ActionMenuView/e Landroid/support/v7/widget/ActionMenuPresenter;
aload 0
getfield android/support/v7/widget/ActionMenuView/e Landroid/support/v7/widget/ActionMenuPresenter;
aload 0
invokevirtual android/support/v7/widget/ActionMenuPresenter/a(Landroid/support/v7/widget/ActionMenuView;)V
return
.limit locals 2
.limit stack 2
.end method
