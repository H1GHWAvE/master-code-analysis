.bytecode 50.0
.class final synchronized android/support/v7/widget/ar
.super android/support/v7/internal/widget/ah

.field private 'h' Z

.field private 'i' Z

.field private 'j' Z

.field private 'k' Landroid/support/v4/view/fk;

.field private 'l' Landroid/support/v4/widget/ba;

.method public <init>(Landroid/content/Context;Z)V
aload 0
aload 1
getstatic android/support/v7/a/d/dropDownListViewStyle I
invokespecial android/support/v7/internal/widget/ah/<init>(Landroid/content/Context;I)V
aload 0
iload 2
putfield android/support/v7/widget/ar/i Z
aload 0
iconst_0
invokevirtual android/support/v7/widget/ar/setCacheColorHint(I)V
return
.limit locals 3
.limit stack 3
.end method

.method private a(Landroid/view/View;I)V
aload 0
aload 1
iload 2
aload 0
iload 2
invokevirtual android/support/v7/widget/ar/getItemIdAtPosition(I)J
invokevirtual android/support/v7/widget/ar/performItemClick(Landroid/view/View;IJ)Z
pop
return
.limit locals 3
.limit stack 5
.end method

.method private a(Landroid/view/View;IFF)V
aload 0
iconst_1
putfield android/support/v7/widget/ar/j Z
aload 0
iconst_1
invokevirtual android/support/v7/widget/ar/setPressed(Z)V
aload 0
invokevirtual android/support/v7/widget/ar/layoutChildren()V
aload 0
iload 2
invokevirtual android/support/v7/widget/ar/setSelection(I)V
aload 0
iload 2
aload 1
fload 3
fload 4
invokevirtual android/support/v7/widget/ar/a(ILandroid/view/View;FF)V
aload 0
iconst_0
invokevirtual android/support/v7/widget/ar/setSelectorEnabled(Z)V
aload 0
invokevirtual android/support/v7/widget/ar/refreshDrawableState()V
return
.limit locals 5
.limit stack 5
.end method

.method static synthetic a(Landroid/support/v7/widget/ar;Z)Z
aload 0
iload 1
putfield android/support/v7/widget/ar/h Z
iload 1
ireturn
.limit locals 2
.limit stack 2
.end method

.method private b()V
aload 0
iconst_0
putfield android/support/v7/widget/ar/j Z
aload 0
iconst_0
invokevirtual android/support/v7/widget/ar/setPressed(Z)V
aload 0
invokevirtual android/support/v7/widget/ar/drawableStateChanged()V
aload 0
getfield android/support/v7/widget/ar/k Landroid/support/v4/view/fk;
ifnull L0
aload 0
getfield android/support/v7/widget/ar/k Landroid/support/v4/view/fk;
invokevirtual android/support/v4/view/fk/a()V
aload 0
aconst_null
putfield android/support/v7/widget/ar/k Landroid/support/v4/view/fk;
L0:
return
.limit locals 1
.limit stack 2
.end method

.method protected final a()Z
aload 0
getfield android/support/v7/widget/ar/j Z
ifne L0
aload 0
invokespecial android/support/v7/internal/widget/ah/a()Z
ifeq L1
L0:
iconst_1
ireturn
L1:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final a(Landroid/view/MotionEvent;I)Z
aload 1
invokestatic android/support/v4/view/bk/a(Landroid/view/MotionEvent;)I
istore 5
iload 5
tableswitch 1
L0
L1
L2
default : L3
L3:
iconst_0
istore 2
iconst_1
istore 8
L4:
iload 8
ifeq L5
iload 2
ifeq L6
L5:
aload 0
iconst_0
putfield android/support/v7/widget/ar/j Z
aload 0
iconst_0
invokevirtual android/support/v7/widget/ar/setPressed(Z)V
aload 0
invokevirtual android/support/v7/widget/ar/drawableStateChanged()V
aload 0
getfield android/support/v7/widget/ar/k Landroid/support/v4/view/fk;
ifnull L6
aload 0
getfield android/support/v7/widget/ar/k Landroid/support/v4/view/fk;
invokevirtual android/support/v4/view/fk/a()V
aload 0
aconst_null
putfield android/support/v7/widget/ar/k Landroid/support/v4/view/fk;
L6:
iload 8
ifeq L7
aload 0
getfield android/support/v7/widget/ar/l Landroid/support/v4/widget/ba;
ifnonnull L8
aload 0
new android/support/v4/widget/ba
dup
aload 0
invokespecial android/support/v4/widget/ba/<init>(Landroid/widget/ListView;)V
putfield android/support/v7/widget/ar/l Landroid/support/v4/widget/ba;
L8:
aload 0
getfield android/support/v7/widget/ar/l Landroid/support/v4/widget/ba;
iconst_1
invokevirtual android/support/v4/widget/ba/a(Z)Landroid/support/v4/widget/a;
pop
aload 0
getfield android/support/v7/widget/ar/l Landroid/support/v4/widget/ba;
aload 0
aload 1
invokevirtual android/support/v4/widget/ba/onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
pop
L9:
iload 8
ireturn
L2:
iconst_0
istore 2
iconst_0
istore 8
goto L4
L0:
iconst_0
istore 8
L10:
aload 1
iload 2
invokevirtual android/view/MotionEvent/findPointerIndex(I)I
istore 6
iload 6
ifge L11
iconst_0
istore 2
iconst_0
istore 8
goto L4
L11:
aload 1
iload 6
invokevirtual android/view/MotionEvent/getX(I)F
f2i
istore 2
aload 1
iload 6
invokevirtual android/view/MotionEvent/getY(I)F
f2i
istore 6
aload 0
iload 2
iload 6
invokevirtual android/support/v7/widget/ar/pointToPosition(II)I
istore 7
iload 7
iconst_m1
if_icmpne L12
iconst_1
istore 2
goto L4
L12:
aload 0
iload 7
aload 0
invokevirtual android/support/v7/widget/ar/getFirstVisiblePosition()I
isub
invokevirtual android/support/v7/widget/ar/getChildAt(I)Landroid/view/View;
astore 9
iload 2
i2f
fstore 3
iload 6
i2f
fstore 4
aload 0
iconst_1
putfield android/support/v7/widget/ar/j Z
aload 0
iconst_1
invokevirtual android/support/v7/widget/ar/setPressed(Z)V
aload 0
invokevirtual android/support/v7/widget/ar/layoutChildren()V
aload 0
iload 7
invokevirtual android/support/v7/widget/ar/setSelection(I)V
aload 0
iload 7
aload 9
fload 3
fload 4
invokevirtual android/support/v7/widget/ar/a(ILandroid/view/View;FF)V
aload 0
iconst_0
invokevirtual android/support/v7/widget/ar/setSelectorEnabled(Z)V
aload 0
invokevirtual android/support/v7/widget/ar/refreshDrawableState()V
iload 5
iconst_1
if_icmpne L3
aload 0
aload 9
iload 7
aload 0
iload 7
invokevirtual android/support/v7/widget/ar/getItemIdAtPosition(I)J
invokevirtual android/support/v7/widget/ar/performItemClick(Landroid/view/View;IJ)Z
pop
goto L3
L7:
aload 0
getfield android/support/v7/widget/ar/l Landroid/support/v4/widget/ba;
ifnull L9
aload 0
getfield android/support/v7/widget/ar/l Landroid/support/v4/widget/ba;
iconst_0
invokevirtual android/support/v4/widget/ba/a(Z)Landroid/support/v4/widget/a;
pop
iload 8
ireturn
L1:
iconst_1
istore 8
goto L10
.limit locals 10
.limit stack 5
.end method

.method public final hasFocus()Z
aload 0
getfield android/support/v7/widget/ar/i Z
ifne L0
aload 0
invokespecial android/support/v7/internal/widget/ah/hasFocus()Z
ifeq L1
L0:
iconst_1
ireturn
L1:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final hasWindowFocus()Z
aload 0
getfield android/support/v7/widget/ar/i Z
ifne L0
aload 0
invokespecial android/support/v7/internal/widget/ah/hasWindowFocus()Z
ifeq L1
L0:
iconst_1
ireturn
L1:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final isFocused()Z
aload 0
getfield android/support/v7/widget/ar/i Z
ifne L0
aload 0
invokespecial android/support/v7/internal/widget/ah/isFocused()Z
ifeq L1
L0:
iconst_1
ireturn
L1:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final isInTouchMode()Z
aload 0
getfield android/support/v7/widget/ar/i Z
ifeq L0
aload 0
getfield android/support/v7/widget/ar/h Z
ifne L1
L0:
aload 0
invokespecial android/support/v7/internal/widget/ah/isInTouchMode()Z
ifeq L2
L1:
iconst_1
ireturn
L2:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method
