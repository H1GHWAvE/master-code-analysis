.bytecode 50.0
.class public synchronized android/support/v7/widget/al
.super android/view/ViewGroup$MarginLayoutParams

.field public 'g' F

.field public 'h' I

.method public <init>()V
aload 0
iconst_0
iconst_m1
invokespecial android/view/ViewGroup$MarginLayoutParams/<init>(II)V
aload 0
iconst_m1
putfield android/support/v7/widget/al/h I
aload 0
fconst_1
putfield android/support/v7/widget/al/g F
return
.limit locals 1
.limit stack 3
.end method

.method public <init>(II)V
aload 0
iload 1
iload 2
invokespecial android/view/ViewGroup$MarginLayoutParams/<init>(II)V
aload 0
iconst_m1
putfield android/support/v7/widget/al/h I
aload 0
fconst_0
putfield android/support/v7/widget/al/g F
return
.limit locals 3
.limit stack 3
.end method

.method public <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
aload 0
aload 1
aload 2
invokespecial android/view/ViewGroup$MarginLayoutParams/<init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
aload 0
iconst_m1
putfield android/support/v7/widget/al/h I
aload 1
aload 2
getstatic android/support/v7/a/n/LinearLayoutCompat_Layout [I
invokevirtual android/content/Context/obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;
astore 1
aload 0
aload 1
getstatic android/support/v7/a/n/LinearLayoutCompat_Layout_android_layout_weight I
fconst_0
invokevirtual android/content/res/TypedArray/getFloat(IF)F
putfield android/support/v7/widget/al/g F
aload 0
aload 1
getstatic android/support/v7/a/n/LinearLayoutCompat_Layout_android_layout_gravity I
iconst_m1
invokevirtual android/content/res/TypedArray/getInt(II)I
putfield android/support/v7/widget/al/h I
aload 1
invokevirtual android/content/res/TypedArray/recycle()V
return
.limit locals 3
.limit stack 4
.end method

.method private <init>(Landroid/support/v7/widget/al;)V
aload 0
aload 1
invokespecial android/view/ViewGroup$MarginLayoutParams/<init>(Landroid/view/ViewGroup$MarginLayoutParams;)V
aload 0
iconst_m1
putfield android/support/v7/widget/al/h I
aload 0
aload 1
getfield android/support/v7/widget/al/g F
putfield android/support/v7/widget/al/g F
aload 0
aload 1
getfield android/support/v7/widget/al/h I
putfield android/support/v7/widget/al/h I
return
.limit locals 2
.limit stack 2
.end method

.method public <init>(Landroid/view/ViewGroup$LayoutParams;)V
aload 0
aload 1
invokespecial android/view/ViewGroup$MarginLayoutParams/<init>(Landroid/view/ViewGroup$LayoutParams;)V
aload 0
iconst_m1
putfield android/support/v7/widget/al/h I
return
.limit locals 2
.limit stack 2
.end method

.method private <init>(Landroid/view/ViewGroup$MarginLayoutParams;)V
aload 0
aload 1
invokespecial android/view/ViewGroup$MarginLayoutParams/<init>(Landroid/view/ViewGroup$MarginLayoutParams;)V
aload 0
iconst_m1
putfield android/support/v7/widget/al/h I
return
.limit locals 2
.limit stack 2
.end method
