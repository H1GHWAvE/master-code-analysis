.bytecode 50.0
.class public final synchronized android/support/v7/widget/ActionMenuPresenter
.super android/support/v7/internal/view/menu/d
.implements android/support/v4/view/o

.field private static final 'u' Ljava/lang/String; = "ActionMenuPresenter"

.field private 'A' I

.field private final 'B' Landroid/util/SparseBooleanArray;

.field private 'C' Landroid/view/View;

.field private 'D' Landroid/support/v7/widget/c;

.field 'i' Landroid/support/v7/widget/e;

.field 'j' Landroid/graphics/drawable/Drawable;

.field 'k' Z

.field public 'l' Z

.field public 'm' I

.field public 'n' Z

.field public 'o' Z

.field 'p' Landroid/support/v7/widget/g;

.field 'q' Landroid/support/v7/widget/b;

.field 'r' Landroid/support/v7/widget/d;

.field final 's' Landroid/support/v7/widget/h;

.field 't' I

.field private 'v' Z

.field private 'w' I

.field private 'x' I

.field private 'y' Z

.field private 'z' Z

.method public <init>(Landroid/content/Context;)V
aload 0
aload 1
getstatic android/support/v7/a/k/abc_action_menu_layout I
getstatic android/support/v7/a/k/abc_action_menu_item_layout I
invokespecial android/support/v7/internal/view/menu/d/<init>(Landroid/content/Context;II)V
aload 0
new android/util/SparseBooleanArray
dup
invokespecial android/util/SparseBooleanArray/<init>()V
putfield android/support/v7/widget/ActionMenuPresenter/B Landroid/util/SparseBooleanArray;
aload 0
new android/support/v7/widget/h
dup
aload 0
iconst_0
invokespecial android/support/v7/widget/h/<init>(Landroid/support/v7/widget/ActionMenuPresenter;B)V
putfield android/support/v7/widget/ActionMenuPresenter/s Landroid/support/v7/widget/h;
return
.limit locals 2
.limit stack 5
.end method

.method private static synthetic a(Landroid/support/v7/widget/ActionMenuPresenter;)Landroid/support/v7/widget/g;
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/p Landroid/support/v7/widget/g;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static synthetic a(Landroid/support/v7/widget/ActionMenuPresenter;Landroid/support/v7/widget/g;)Landroid/support/v7/widget/g;
aload 0
aload 1
putfield android/support/v7/widget/ActionMenuPresenter/p Landroid/support/v7/widget/g;
aload 1
areturn
.limit locals 2
.limit stack 2
.end method

.method private a(Landroid/view/MenuItem;)Landroid/view/View;
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/g Landroid/support/v7/internal/view/menu/z;
checkcast android/view/ViewGroup
astore 6
aload 6
ifnonnull L0
aconst_null
astore 4
L1:
aload 4
areturn
L0:
aload 6
invokevirtual android/view/ViewGroup/getChildCount()I
istore 3
iconst_0
istore 2
L2:
iload 2
iload 3
if_icmpge L3
aload 6
iload 2
invokevirtual android/view/ViewGroup/getChildAt(I)Landroid/view/View;
astore 5
aload 5
instanceof android/support/v7/internal/view/menu/aa
ifeq L4
aload 5
astore 4
aload 5
checkcast android/support/v7/internal/view/menu/aa
invokeinterface android/support/v7/internal/view/menu/aa/getItemData()Landroid/support/v7/internal/view/menu/m; 0
aload 1
if_acmpeq L1
L4:
iload 2
iconst_1
iadd
istore 2
goto L2
L3:
aconst_null
areturn
.limit locals 7
.limit stack 2
.end method

.method private a(I)V
aload 0
iload 1
putfield android/support/v7/widget/ActionMenuPresenter/m I
aload 0
iconst_1
putfield android/support/v7/widget/ActionMenuPresenter/n Z
return
.limit locals 2
.limit stack 2
.end method

.method private a(IZ)V
aload 0
iload 1
putfield android/support/v7/widget/ActionMenuPresenter/w I
aload 0
iload 2
putfield android/support/v7/widget/ActionMenuPresenter/y Z
aload 0
iconst_1
putfield android/support/v7/widget/ActionMenuPresenter/z Z
return
.limit locals 3
.limit stack 2
.end method

.method private a(Landroid/graphics/drawable/Drawable;)V
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/i Landroid/support/v7/widget/e;
ifnull L0
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/i Landroid/support/v7/widget/e;
aload 1
invokevirtual android/support/v7/widget/e/setImageDrawable(Landroid/graphics/drawable/Drawable;)V
return
L0:
aload 0
iconst_1
putfield android/support/v7/widget/ActionMenuPresenter/k Z
aload 0
aload 1
putfield android/support/v7/widget/ActionMenuPresenter/j Landroid/graphics/drawable/Drawable;
return
.limit locals 2
.limit stack 2
.end method

.method private static synthetic b(Landroid/support/v7/widget/ActionMenuPresenter;)Landroid/support/v7/widget/d;
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/r Landroid/support/v7/widget/d;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static synthetic c(Landroid/support/v7/widget/ActionMenuPresenter;)Landroid/support/v7/internal/view/menu/i;
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/c Landroid/support/v7/internal/view/menu/i;
areturn
.limit locals 1
.limit stack 1
.end method

.method private c(Z)V
aload 0
iload 1
putfield android/support/v7/widget/ActionMenuPresenter/o Z
return
.limit locals 2
.limit stack 2
.end method

.method private static synthetic d(Landroid/support/v7/widget/ActionMenuPresenter;)Landroid/support/v7/widget/e;
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/i Landroid/support/v7/widget/e;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static synthetic e(Landroid/support/v7/widget/ActionMenuPresenter;)Landroid/support/v7/internal/view/menu/z;
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/g Landroid/support/v7/internal/view/menu/z;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static synthetic f(Landroid/support/v7/widget/ActionMenuPresenter;)Landroid/support/v7/widget/b;
aload 0
aconst_null
putfield android/support/v7/widget/ActionMenuPresenter/q Landroid/support/v7/widget/b;
aconst_null
areturn
.limit locals 1
.limit stack 2
.end method

.method private static synthetic g(Landroid/support/v7/widget/ActionMenuPresenter;)Landroid/support/v7/internal/view/menu/i;
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/c Landroid/support/v7/internal/view/menu/i;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static synthetic h(Landroid/support/v7/widget/ActionMenuPresenter;)Landroid/support/v7/internal/view/menu/z;
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/g Landroid/support/v7/internal/view/menu/z;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static synthetic i(Landroid/support/v7/widget/ActionMenuPresenter;)Landroid/support/v7/widget/d;
aload 0
aconst_null
putfield android/support/v7/widget/ActionMenuPresenter/r Landroid/support/v7/widget/d;
aconst_null
areturn
.limit locals 1
.limit stack 2
.end method

.method private static synthetic j(Landroid/support/v7/widget/ActionMenuPresenter;)Landroid/support/v7/widget/b;
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/q Landroid/support/v7/widget/b;
areturn
.limit locals 1
.limit stack 1
.end method

.method private k()V
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/n Z
ifne L0
aload 0
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/b Landroid/content/Context;
invokevirtual android/content/Context/getResources()Landroid/content/res/Resources;
getstatic android/support/v7/a/j/abc_max_action_buttons I
invokevirtual android/content/res/Resources/getInteger(I)I
putfield android/support/v7/widget/ActionMenuPresenter/m I
L0:
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/c Landroid/support/v7/internal/view/menu/i;
ifnull L1
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/c Landroid/support/v7/internal/view/menu/i;
iconst_1
invokevirtual android/support/v7/internal/view/menu/i/c(Z)V
L1:
return
.limit locals 1
.limit stack 3
.end method

.method private l()Landroid/graphics/drawable/Drawable;
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/i Landroid/support/v7/widget/e;
ifnull L0
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/i Landroid/support/v7/widget/e;
invokevirtual android/support/v7/widget/e/getDrawable()Landroid/graphics/drawable/Drawable;
areturn
L0:
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/k Z
ifeq L1
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/j Landroid/graphics/drawable/Drawable;
areturn
L1:
aconst_null
areturn
.limit locals 1
.limit stack 1
.end method

.method private m()Z
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/l Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final a(Landroid/view/ViewGroup;)Landroid/support/v7/internal/view/menu/z;
aload 0
aload 1
invokespecial android/support/v7/internal/view/menu/d/a(Landroid/view/ViewGroup;)Landroid/support/v7/internal/view/menu/z;
astore 1
aload 1
checkcast android/support/v7/widget/ActionMenuView
aload 0
invokevirtual android/support/v7/widget/ActionMenuView/setPresenter(Landroid/support/v7/widget/ActionMenuPresenter;)V
aload 1
areturn
.limit locals 2
.limit stack 2
.end method

.method public final a(Landroid/support/v7/internal/view/menu/m;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
aload 1
invokevirtual android/support/v7/internal/view/menu/m/getActionView()Landroid/view/View;
astore 5
aload 5
ifnull L0
aload 1
invokevirtual android/support/v7/internal/view/menu/m/i()Z
ifeq L1
L0:
aload 0
aload 1
aload 2
aload 3
invokespecial android/support/v7/internal/view/menu/d/a(Landroid/support/v7/internal/view/menu/m;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
astore 5
L1:
aload 1
invokevirtual android/support/v7/internal/view/menu/m/isActionViewExpanded()Z
ifeq L2
bipush 8
istore 4
L3:
aload 5
iload 4
invokevirtual android/view/View/setVisibility(I)V
aload 3
checkcast android/support/v7/widget/ActionMenuView
astore 1
aload 5
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
astore 2
aload 1
aload 2
invokevirtual android/support/v7/widget/ActionMenuView/checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
ifne L4
aload 5
aload 2
invokestatic android/support/v7/widget/ActionMenuView/a(Landroid/view/ViewGroup$LayoutParams;)Landroid/support/v7/widget/m;
invokevirtual android/view/View/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
L4:
aload 5
areturn
L2:
iconst_0
istore 4
goto L3
.limit locals 6
.limit stack 4
.end method

.method public final a(Landroid/content/Context;Landroid/support/v7/internal/view/menu/i;)V
iconst_1
istore 5
aload 0
aload 1
aload 2
invokespecial android/support/v7/internal/view/menu/d/a(Landroid/content/Context;Landroid/support/v7/internal/view/menu/i;)V
aload 1
invokevirtual android/content/Context/getResources()Landroid/content/res/Resources;
astore 2
aload 1
invokestatic android/support/v7/internal/view/a/a(Landroid/content/Context;)Landroid/support/v7/internal/view/a;
astore 1
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/v Z
ifne L0
getstatic android/os/Build$VERSION/SDK_INT I
bipush 19
if_icmplt L1
L2:
aload 0
iload 5
putfield android/support/v7/widget/ActionMenuPresenter/l Z
L0:
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/z Z
ifne L3
aload 0
aload 1
getfield android/support/v7/internal/view/a/a Landroid/content/Context;
invokevirtual android/content/Context/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
getfield android/util/DisplayMetrics/widthPixels I
iconst_2
idiv
putfield android/support/v7/widget/ActionMenuPresenter/w I
L3:
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/n Z
ifne L4
aload 0
aload 1
getfield android/support/v7/internal/view/a/a Landroid/content/Context;
invokevirtual android/content/Context/getResources()Landroid/content/res/Resources;
getstatic android/support/v7/a/j/abc_max_action_buttons I
invokevirtual android/content/res/Resources/getInteger(I)I
putfield android/support/v7/widget/ActionMenuPresenter/m I
L4:
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/w I
istore 3
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/l Z
ifeq L5
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/i Landroid/support/v7/widget/e;
ifnonnull L6
aload 0
new android/support/v7/widget/e
dup
aload 0
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/a Landroid/content/Context;
invokespecial android/support/v7/widget/e/<init>(Landroid/support/v7/widget/ActionMenuPresenter;Landroid/content/Context;)V
putfield android/support/v7/widget/ActionMenuPresenter/i Landroid/support/v7/widget/e;
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/k Z
ifeq L7
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/i Landroid/support/v7/widget/e;
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/j Landroid/graphics/drawable/Drawable;
invokevirtual android/support/v7/widget/e/setImageDrawable(Landroid/graphics/drawable/Drawable;)V
aload 0
aconst_null
putfield android/support/v7/widget/ActionMenuPresenter/j Landroid/graphics/drawable/Drawable;
aload 0
iconst_0
putfield android/support/v7/widget/ActionMenuPresenter/k Z
L7:
iconst_0
iconst_0
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
istore 4
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/i Landroid/support/v7/widget/e;
iload 4
iload 4
invokevirtual android/support/v7/widget/e/measure(II)V
L6:
iload 3
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/i Landroid/support/v7/widget/e;
invokevirtual android/support/v7/widget/e/getMeasuredWidth()I
isub
istore 3
L8:
aload 0
iload 3
putfield android/support/v7/widget/ActionMenuPresenter/x I
aload 0
ldc_w 56.0F
aload 2
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
getfield android/util/DisplayMetrics/density F
fmul
f2i
putfield android/support/v7/widget/ActionMenuPresenter/A I
aload 0
aconst_null
putfield android/support/v7/widget/ActionMenuPresenter/C Landroid/view/View;
return
L1:
aload 1
getfield android/support/v7/internal/view/a/a Landroid/content/Context;
invokestatic android/view/ViewConfiguration/get(Landroid/content/Context;)Landroid/view/ViewConfiguration;
invokestatic android/support/v4/view/du/b(Landroid/view/ViewConfiguration;)Z
ifeq L2
iconst_0
istore 5
goto L2
L5:
aload 0
aconst_null
putfield android/support/v7/widget/ActionMenuPresenter/i Landroid/support/v7/widget/e;
goto L8
.limit locals 6
.limit stack 5
.end method

.method public final a(Landroid/os/Parcelable;)V
aload 1
checkcast android/support/v7/widget/ActionMenuPresenter$SavedState
astore 1
aload 1
getfield android/support/v7/widget/ActionMenuPresenter$SavedState/a I
ifle L0
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/c Landroid/support/v7/internal/view/menu/i;
aload 1
getfield android/support/v7/widget/ActionMenuPresenter$SavedState/a I
invokevirtual android/support/v7/internal/view/menu/i/findItem(I)Landroid/view/MenuItem;
astore 1
aload 1
ifnull L0
aload 0
aload 1
invokeinterface android/view/MenuItem/getSubMenu()Landroid/view/SubMenu; 0
checkcast android/support/v7/internal/view/menu/ad
invokevirtual android/support/v7/widget/ActionMenuPresenter/a(Landroid/support/v7/internal/view/menu/ad;)Z
pop
L0:
return
.limit locals 2
.limit stack 2
.end method

.method public final a(Landroid/support/v7/internal/view/menu/i;Z)V
aload 0
invokevirtual android/support/v7/widget/ActionMenuPresenter/g()Z
pop
aload 0
aload 1
iload 2
invokespecial android/support/v7/internal/view/menu/d/a(Landroid/support/v7/internal/view/menu/i;Z)V
return
.limit locals 3
.limit stack 3
.end method

.method public final a(Landroid/support/v7/internal/view/menu/m;Landroid/support/v7/internal/view/menu/aa;)V
aload 2
aload 1
invokeinterface android/support/v7/internal/view/menu/aa/a(Landroid/support/v7/internal/view/menu/m;)V 1
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/g Landroid/support/v7/internal/view/menu/z;
checkcast android/support/v7/widget/ActionMenuView
astore 1
aload 2
checkcast android/support/v7/internal/view/menu/ActionMenuItemView
astore 2
aload 2
aload 1
invokevirtual android/support/v7/internal/view/menu/ActionMenuItemView/setItemInvoker(Landroid/support/v7/internal/view/menu/k;)V
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/D Landroid/support/v7/widget/c;
ifnonnull L0
aload 0
new android/support/v7/widget/c
dup
aload 0
iconst_0
invokespecial android/support/v7/widget/c/<init>(Landroid/support/v7/widget/ActionMenuPresenter;B)V
putfield android/support/v7/widget/ActionMenuPresenter/D Landroid/support/v7/widget/c;
L0:
aload 2
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/D Landroid/support/v7/widget/c;
invokevirtual android/support/v7/internal/view/menu/ActionMenuItemView/setPopupCallback(Landroid/support/v7/internal/view/menu/c;)V
return
.limit locals 3
.limit stack 5
.end method

.method public final a(Landroid/support/v7/widget/ActionMenuView;)V
aload 0
aload 1
putfield android/support/v7/widget/ActionMenuPresenter/g Landroid/support/v7/internal/view/menu/z;
aload 1
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/c Landroid/support/v7/internal/view/menu/i;
putfield android/support/v7/widget/ActionMenuView/c Landroid/support/v7/internal/view/menu/i;
return
.limit locals 2
.limit stack 2
.end method

.method public final a(Z)V
iconst_1
istore 3
iconst_0
istore 4
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/g Landroid/support/v7/internal/view/menu/z;
checkcast android/view/View
invokevirtual android/view/View/getParent()Landroid/view/ViewParent;
pop
aload 0
iload 1
invokespecial android/support/v7/internal/view/menu/d/a(Z)V
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/g Landroid/support/v7/internal/view/menu/z;
checkcast android/view/View
invokevirtual android/view/View/requestLayout()V
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/c Landroid/support/v7/internal/view/menu/i;
ifnull L0
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/c Landroid/support/v7/internal/view/menu/i;
astore 6
aload 6
invokevirtual android/support/v7/internal/view/menu/i/i()V
aload 6
getfield android/support/v7/internal/view/menu/i/h Ljava/util/ArrayList;
astore 6
aload 6
invokevirtual java/util/ArrayList/size()I
istore 5
iconst_0
istore 2
L1:
iload 2
iload 5
if_icmpge L0
aload 6
iload 2
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v7/internal/view/menu/m
getfield android/support/v7/internal/view/menu/m/i Landroid/support/v4/view/n;
astore 7
aload 7
ifnull L2
aload 7
aload 0
putfield android/support/v4/view/n/a Landroid/support/v4/view/o;
L2:
iload 2
iconst_1
iadd
istore 2
goto L1
L0:
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/c Landroid/support/v7/internal/view/menu/i;
ifnull L3
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/c Landroid/support/v7/internal/view/menu/i;
invokevirtual android/support/v7/internal/view/menu/i/j()Ljava/util/ArrayList;
astore 6
L4:
iload 4
istore 2
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/l Z
ifeq L5
iload 4
istore 2
aload 6
ifnull L5
aload 6
invokevirtual java/util/ArrayList/size()I
istore 2
iload 2
iconst_1
if_icmpne L6
aload 6
iconst_0
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v7/internal/view/menu/m
invokevirtual android/support/v7/internal/view/menu/m/isActionViewExpanded()Z
ifne L7
iconst_1
istore 2
L5:
iload 2
ifeq L8
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/i Landroid/support/v7/widget/e;
ifnonnull L9
aload 0
new android/support/v7/widget/e
dup
aload 0
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/a Landroid/content/Context;
invokespecial android/support/v7/widget/e/<init>(Landroid/support/v7/widget/ActionMenuPresenter;Landroid/content/Context;)V
putfield android/support/v7/widget/ActionMenuPresenter/i Landroid/support/v7/widget/e;
L9:
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/i Landroid/support/v7/widget/e;
invokevirtual android/support/v7/widget/e/getParent()Landroid/view/ViewParent;
checkcast android/view/ViewGroup
astore 6
aload 6
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/g Landroid/support/v7/internal/view/menu/z;
if_acmpeq L10
aload 6
ifnull L11
aload 6
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/i Landroid/support/v7/widget/e;
invokevirtual android/view/ViewGroup/removeView(Landroid/view/View;)V
L11:
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/g Landroid/support/v7/internal/view/menu/z;
checkcast android/support/v7/widget/ActionMenuView
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/i Landroid/support/v7/widget/e;
invokestatic android/support/v7/widget/ActionMenuView/a()Landroid/support/v7/widget/m;
invokevirtual android/support/v7/widget/ActionMenuView/addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
L10:
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/g Landroid/support/v7/internal/view/menu/z;
checkcast android/support/v7/widget/ActionMenuView
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/l Z
invokevirtual android/support/v7/widget/ActionMenuView/setOverflowReserved(Z)V
return
L3:
aconst_null
astore 6
goto L4
L7:
iconst_0
istore 2
goto L5
L6:
iload 2
ifle L12
iload 3
istore 2
L13:
goto L5
L12:
iconst_0
istore 2
goto L13
L8:
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/i Landroid/support/v7/widget/e;
ifnull L10
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/i Landroid/support/v7/widget/e;
invokevirtual android/support/v7/widget/e/getParent()Landroid/view/ViewParent;
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/g Landroid/support/v7/internal/view/menu/z;
if_acmpne L10
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/g Landroid/support/v7/internal/view/menu/z;
checkcast android/view/ViewGroup
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/i Landroid/support/v7/widget/e;
invokevirtual android/view/ViewGroup/removeView(Landroid/view/View;)V
goto L10
.limit locals 8
.limit stack 5
.end method

.method public final a()Z
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/c Landroid/support/v7/internal/view/menu/i;
invokevirtual android/support/v7/internal/view/menu/i/h()Ljava/util/ArrayList;
astore 14
aload 14
invokevirtual java/util/ArrayList/size()I
istore 9
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/m I
istore 1
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/x I
istore 8
iconst_0
iconst_0
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
istore 10
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/g Landroid/support/v7/internal/view/menu/z;
checkcast android/view/ViewGroup
astore 15
iconst_0
istore 3
iconst_0
istore 4
iconst_0
istore 5
iconst_0
istore 2
L0:
iload 2
iload 9
if_icmpge L1
aload 14
iload 2
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v7/internal/view/menu/m
astore 16
aload 16
invokevirtual android/support/v7/internal/view/menu/m/h()Z
ifeq L2
iload 3
iconst_1
iadd
istore 3
L3:
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/o Z
ifeq L4
aload 16
invokevirtual android/support/v7/internal/view/menu/m/isActionViewExpanded()Z
ifeq L4
iconst_0
istore 1
L5:
iload 2
iconst_1
iadd
istore 2
goto L0
L2:
aload 16
invokevirtual android/support/v7/internal/view/menu/m/g()Z
ifeq L6
iload 4
iconst_1
iadd
istore 4
goto L3
L6:
iconst_1
istore 5
goto L3
L1:
iload 1
istore 2
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/l Z
ifeq L7
iload 5
ifne L8
iload 1
istore 2
iload 3
iload 4
iadd
iload 1
if_icmple L7
L8:
iload 1
iconst_1
isub
istore 2
L7:
iload 2
iload 3
isub
istore 2
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/B Landroid/util/SparseBooleanArray;
astore 16
aload 16
invokevirtual android/util/SparseBooleanArray/clear()V
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/y Z
ifeq L9
iload 8
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/A I
idiv
istore 1
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/A I
istore 3
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/A I
istore 4
iload 8
iload 3
irem
iload 1
idiv
iload 4
iadd
istore 6
L10:
iconst_0
istore 5
iconst_0
istore 7
iload 1
istore 3
iload 2
istore 1
iload 8
istore 4
iload 5
istore 2
L11:
iload 7
iload 9
if_icmpge L12
aload 14
iload 7
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v7/internal/view/menu/m
astore 17
aload 17
invokevirtual android/support/v7/internal/view/menu/m/h()Z
ifeq L13
aload 0
aload 17
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/C Landroid/view/View;
aload 15
invokevirtual android/support/v7/widget/ActionMenuPresenter/a(Landroid/support/v7/internal/view/menu/m;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
astore 18
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/C Landroid/view/View;
ifnonnull L14
aload 0
aload 18
putfield android/support/v7/widget/ActionMenuPresenter/C Landroid/view/View;
L14:
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/y Z
ifeq L15
iload 3
aload 18
iload 6
iload 3
iload 10
iconst_0
invokestatic android/support/v7/widget/ActionMenuView/a(Landroid/view/View;IIII)I
isub
istore 5
L16:
aload 18
invokevirtual android/view/View/getMeasuredWidth()I
istore 3
iload 2
ifne L17
iload 3
istore 2
L18:
aload 17
invokevirtual android/support/v7/internal/view/menu/m/getGroupId()I
istore 8
iload 8
ifeq L19
aload 16
iload 8
iconst_1
invokevirtual android/util/SparseBooleanArray/put(IZ)V
L19:
aload 17
iconst_1
invokevirtual android/support/v7/internal/view/menu/m/d(Z)V
iload 4
iload 3
isub
istore 3
iload 1
istore 4
iload 5
istore 1
L20:
iload 7
iconst_1
iadd
istore 8
iload 3
istore 5
iload 4
istore 7
iload 1
istore 3
iload 5
istore 4
iload 7
istore 1
iload 8
istore 7
goto L11
L15:
aload 18
iload 10
iload 10
invokevirtual android/view/View/measure(II)V
iload 3
istore 5
goto L16
L13:
aload 17
invokevirtual android/support/v7/internal/view/menu/m/g()Z
ifeq L21
aload 17
invokevirtual android/support/v7/internal/view/menu/m/getGroupId()I
istore 11
aload 16
iload 11
invokevirtual android/util/SparseBooleanArray/get(I)Z
istore 13
iload 1
ifgt L22
iload 13
ifeq L23
L22:
iload 4
ifle L23
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/y Z
ifeq L24
iload 3
ifle L23
L24:
iconst_1
istore 12
L25:
iload 12
ifeq L26
aload 0
aload 17
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/C Landroid/view/View;
aload 15
invokevirtual android/support/v7/widget/ActionMenuPresenter/a(Landroid/support/v7/internal/view/menu/m;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
astore 18
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/C Landroid/view/View;
ifnonnull L27
aload 0
aload 18
putfield android/support/v7/widget/ActionMenuPresenter/C Landroid/view/View;
L27:
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/y Z
ifeq L28
aload 18
iload 6
iload 3
iload 10
iconst_0
invokestatic android/support/v7/widget/ActionMenuView/a(Landroid/view/View;IIII)I
istore 8
iload 3
iload 8
isub
istore 5
iload 5
istore 3
iload 8
ifne L29
iconst_0
istore 12
iload 5
istore 3
L29:
aload 18
invokevirtual android/view/View/getMeasuredWidth()I
istore 8
iload 4
iload 8
isub
istore 4
iload 2
istore 5
iload 2
ifne L30
iload 8
istore 5
L30:
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/y Z
ifeq L31
iload 4
iflt L32
iconst_1
istore 2
L33:
iload 12
iload 2
iand
istore 12
iload 3
istore 2
iload 5
istore 3
L34:
iload 12
ifeq L35
iload 11
ifeq L35
aload 16
iload 11
iconst_1
invokevirtual android/util/SparseBooleanArray/put(IZ)V
L36:
iload 1
istore 5
iload 12
ifeq L37
iload 1
iconst_1
isub
istore 5
L37:
aload 17
iload 12
invokevirtual android/support/v7/internal/view/menu/m/d(Z)V
iload 3
istore 8
iload 4
istore 3
iload 5
istore 4
iload 2
istore 1
iload 8
istore 2
goto L20
L23:
iconst_0
istore 12
goto L25
L28:
aload 18
iload 10
iload 10
invokevirtual android/view/View/measure(II)V
goto L29
L32:
iconst_0
istore 2
goto L33
L31:
iload 4
iload 5
iadd
ifle L38
iconst_1
istore 2
L39:
iload 12
iload 2
iand
istore 12
iload 3
istore 2
iload 5
istore 3
goto L34
L38:
iconst_0
istore 2
goto L39
L35:
iload 13
ifeq L40
aload 16
iload 11
iconst_0
invokevirtual android/util/SparseBooleanArray/put(IZ)V
iconst_0
istore 8
L41:
iload 8
iload 7
if_icmpge L42
aload 14
iload 8
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v7/internal/view/menu/m
astore 18
iload 1
istore 5
aload 18
invokevirtual android/support/v7/internal/view/menu/m/getGroupId()I
iload 11
if_icmpne L43
iload 1
istore 5
aload 18
invokevirtual android/support/v7/internal/view/menu/m/f()Z
ifeq L44
iload 1
iconst_1
iadd
istore 5
L44:
aload 18
iconst_0
invokevirtual android/support/v7/internal/view/menu/m/d(Z)V
L43:
iload 8
iconst_1
iadd
istore 8
iload 5
istore 1
goto L41
L21:
aload 17
iconst_0
invokevirtual android/support/v7/internal/view/menu/m/d(Z)V
iload 1
istore 5
iload 3
istore 1
iload 4
istore 3
iload 5
istore 4
goto L20
L12:
iconst_1
ireturn
L42:
goto L36
L40:
goto L36
L26:
iload 3
istore 5
iload 2
istore 3
iload 5
istore 2
goto L34
L17:
goto L18
L9:
iconst_0
istore 6
iconst_0
istore 1
goto L10
L4:
goto L5
.limit locals 19
.limit stack 6
.end method

.method public final a(Landroid/support/v7/internal/view/menu/ad;)Z
aload 1
invokevirtual android/support/v7/internal/view/menu/ad/hasVisibleItems()Z
ifne L0
iconst_0
ireturn
L0:
aload 1
astore 4
L1:
aload 4
getfield android/support/v7/internal/view/menu/ad/p Landroid/support/v7/internal/view/menu/i;
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/c Landroid/support/v7/internal/view/menu/i;
if_acmpeq L2
aload 4
getfield android/support/v7/internal/view/menu/ad/p Landroid/support/v7/internal/view/menu/i;
checkcast android/support/v7/internal/view/menu/ad
astore 4
goto L1
L2:
aload 4
invokevirtual android/support/v7/internal/view/menu/ad/getItem()Landroid/view/MenuItem;
astore 5
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/g Landroid/support/v7/internal/view/menu/z;
checkcast android/view/ViewGroup
astore 6
aload 6
ifnull L3
aload 6
invokevirtual android/view/ViewGroup/getChildCount()I
istore 3
iconst_0
istore 2
L4:
iload 2
iload 3
if_icmpge L3
aload 6
iload 2
invokevirtual android/view/ViewGroup/getChildAt(I)Landroid/view/View;
astore 4
aload 4
instanceof android/support/v7/internal/view/menu/aa
ifeq L5
aload 4
checkcast android/support/v7/internal/view/menu/aa
invokeinterface android/support/v7/internal/view/menu/aa/getItemData()Landroid/support/v7/internal/view/menu/m; 0
aload 5
if_acmpne L5
L6:
aload 4
astore 5
aload 4
ifnonnull L7
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/i Landroid/support/v7/widget/e;
ifnonnull L8
iconst_0
ireturn
L5:
iload 2
iconst_1
iadd
istore 2
goto L4
L3:
aconst_null
astore 4
goto L6
L8:
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/i Landroid/support/v7/widget/e;
astore 5
L7:
aload 0
aload 1
invokevirtual android/support/v7/internal/view/menu/ad/getItem()Landroid/view/MenuItem;
invokeinterface android/view/MenuItem/getItemId()I 0
putfield android/support/v7/widget/ActionMenuPresenter/t I
aload 0
new android/support/v7/widget/b
dup
aload 0
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/b Landroid/content/Context;
aload 1
invokespecial android/support/v7/widget/b/<init>(Landroid/support/v7/widget/ActionMenuPresenter;Landroid/content/Context;Landroid/support/v7/internal/view/menu/ad;)V
putfield android/support/v7/widget/ActionMenuPresenter/q Landroid/support/v7/widget/b;
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/q Landroid/support/v7/widget/b;
aload 5
putfield android/support/v7/internal/view/menu/v/b Landroid/view/View;
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/q Landroid/support/v7/widget/b;
invokevirtual android/support/v7/widget/b/d()V
aload 0
aload 1
invokespecial android/support/v7/internal/view/menu/d/a(Landroid/support/v7/internal/view/menu/ad;)Z
pop
iconst_1
ireturn
.limit locals 7
.limit stack 6
.end method

.method public final a(Landroid/view/ViewGroup;I)Z
aload 1
iload 2
invokevirtual android/view/ViewGroup/getChildAt(I)Landroid/view/View;
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/i Landroid/support/v7/widget/e;
if_acmpne L0
iconst_0
ireturn
L0:
aload 0
aload 1
iload 2
invokespecial android/support/v7/internal/view/menu/d/a(Landroid/view/ViewGroup;I)Z
ireturn
.limit locals 3
.limit stack 3
.end method

.method public final b(Z)V
iload 1
ifeq L0
aload 0
aconst_null
invokespecial android/support/v7/internal/view/menu/d/a(Landroid/support/v7/internal/view/menu/ad;)Z
pop
return
L0:
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/c Landroid/support/v7/internal/view/menu/i;
iconst_0
invokevirtual android/support/v7/internal/view/menu/i/b(Z)V
return
.limit locals 2
.limit stack 2
.end method

.method public final c()Landroid/os/Parcelable;
new android/support/v7/widget/ActionMenuPresenter$SavedState
dup
invokespecial android/support/v7/widget/ActionMenuPresenter$SavedState/<init>()V
astore 1
aload 1
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/t I
putfield android/support/v7/widget/ActionMenuPresenter$SavedState/a I
aload 1
areturn
.limit locals 2
.limit stack 2
.end method

.method public final c(Landroid/support/v7/internal/view/menu/m;)Z
aload 1
invokevirtual android/support/v7/internal/view/menu/m/f()Z
ireturn
.limit locals 2
.limit stack 1
.end method

.method public final d()V
aload 0
iconst_1
putfield android/support/v7/widget/ActionMenuPresenter/l Z
aload 0
iconst_1
putfield android/support/v7/widget/ActionMenuPresenter/v Z
return
.limit locals 1
.limit stack 2
.end method

.method public final e()Z
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/l Z
ifeq L0
aload 0
invokevirtual android/support/v7/widget/ActionMenuPresenter/i()Z
ifne L0
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/c Landroid/support/v7/internal/view/menu/i;
ifnull L0
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/g Landroid/support/v7/internal/view/menu/z;
ifnull L0
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/r Landroid/support/v7/widget/d;
ifnonnull L0
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/c Landroid/support/v7/internal/view/menu/i;
invokevirtual android/support/v7/internal/view/menu/i/j()Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/isEmpty()Z
ifne L0
aload 0
new android/support/v7/widget/d
dup
aload 0
new android/support/v7/widget/g
dup
aload 0
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/b Landroid/content/Context;
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/c Landroid/support/v7/internal/view/menu/i;
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/i Landroid/support/v7/widget/e;
invokespecial android/support/v7/widget/g/<init>(Landroid/support/v7/widget/ActionMenuPresenter;Landroid/content/Context;Landroid/support/v7/internal/view/menu/i;Landroid/view/View;)V
invokespecial android/support/v7/widget/d/<init>(Landroid/support/v7/widget/ActionMenuPresenter;Landroid/support/v7/widget/g;)V
putfield android/support/v7/widget/ActionMenuPresenter/r Landroid/support/v7/widget/d;
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/g Landroid/support/v7/internal/view/menu/z;
checkcast android/view/View
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/r Landroid/support/v7/widget/d;
invokevirtual android/view/View/post(Ljava/lang/Runnable;)Z
pop
aload 0
aconst_null
invokespecial android/support/v7/internal/view/menu/d/a(Landroid/support/v7/internal/view/menu/ad;)Z
pop
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 10
.end method

.method public final f()Z
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/r Landroid/support/v7/widget/d;
ifnull L0
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/g Landroid/support/v7/internal/view/menu/z;
ifnull L0
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/g Landroid/support/v7/internal/view/menu/z;
checkcast android/view/View
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/r Landroid/support/v7/widget/d;
invokevirtual android/view/View/removeCallbacks(Ljava/lang/Runnable;)Z
pop
aload 0
aconst_null
putfield android/support/v7/widget/ActionMenuPresenter/r Landroid/support/v7/widget/d;
iconst_1
ireturn
L0:
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/p Landroid/support/v7/widget/g;
astore 1
aload 1
ifnull L1
aload 1
invokevirtual android/support/v7/internal/view/menu/v/f()V
iconst_1
ireturn
L1:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final g()Z
aload 0
invokevirtual android/support/v7/widget/ActionMenuPresenter/f()Z
aload 0
invokevirtual android/support/v7/widget/ActionMenuPresenter/h()Z
ior
ireturn
.limit locals 1
.limit stack 2
.end method

.method public final h()Z
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/q Landroid/support/v7/widget/b;
ifnull L0
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/q Landroid/support/v7/widget/b;
invokevirtual android/support/v7/widget/b/f()V
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final i()Z
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/p Landroid/support/v7/widget/g;
ifnull L0
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/p Landroid/support/v7/widget/g;
invokevirtual android/support/v7/widget/g/g()Z
ifeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final j()Z
aload 0
getfield android/support/v7/widget/ActionMenuPresenter/r Landroid/support/v7/widget/d;
ifnonnull L0
aload 0
invokevirtual android/support/v7/widget/ActionMenuPresenter/i()Z
ifeq L1
L0:
iconst_1
ireturn
L1:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method
