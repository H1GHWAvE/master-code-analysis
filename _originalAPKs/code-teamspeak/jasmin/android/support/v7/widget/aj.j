.bytecode 50.0
.class public synchronized android/support/v7/widget/aj
.super android/view/ViewGroup

.field public static final 'h' I = 0


.field public static final 'i' I = 1


.field public static final 'j' I = 0


.field public static final 'k' I = 1


.field public static final 'l' I = 2


.field public static final 'm' I = 4


.field private static final 'q' I = 4


.field private static final 'r' I = 0


.field private static final 's' I = 1


.field private static final 't' I = 2


.field private static final 'u' I = 3


.field private 'a' Z

.field private 'b' I

.field private 'c' I

.field private 'd' I

.field private 'e' I

.field private 'f' I

.field private 'g' F

.field private 'n' Z

.field private 'o' [I

.field private 'p' [I

.field private 'v' Landroid/graphics/drawable/Drawable;

.field private 'w' I

.field private 'x' I

.field private 'y' I

.field private 'z' I

.method public <init>(Landroid/content/Context;)V
aload 0
aload 1
aconst_null
invokespecial android/support/v7/widget/aj/<init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
return
.limit locals 2
.limit stack 3
.end method

.method public <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
aload 0
aload 1
aload 2
iconst_0
invokespecial android/support/v7/widget/aj/<init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
return
.limit locals 3
.limit stack 4
.end method

.method public <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
aload 0
aload 1
aload 2
iload 3
invokespecial android/view/ViewGroup/<init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
aload 0
iconst_1
putfield android/support/v7/widget/aj/a Z
aload 0
iconst_m1
putfield android/support/v7/widget/aj/b I
aload 0
iconst_0
putfield android/support/v7/widget/aj/c I
aload 0
ldc_w 8388659
putfield android/support/v7/widget/aj/e I
aload 1
aload 2
getstatic android/support/v7/a/n/LinearLayoutCompat [I
iload 3
invokestatic android/support/v7/internal/widget/ax/a(Landroid/content/Context;Landroid/util/AttributeSet;[II)Landroid/support/v7/internal/widget/ax;
astore 1
aload 1
getstatic android/support/v7/a/n/LinearLayoutCompat_android_orientation I
iconst_m1
invokevirtual android/support/v7/internal/widget/ax/a(II)I
istore 3
iload 3
iflt L0
aload 0
iload 3
invokevirtual android/support/v7/widget/aj/setOrientation(I)V
L0:
aload 1
getstatic android/support/v7/a/n/LinearLayoutCompat_android_gravity I
iconst_m1
invokevirtual android/support/v7/internal/widget/ax/a(II)I
istore 3
iload 3
iflt L1
aload 0
iload 3
invokevirtual android/support/v7/widget/aj/setGravity(I)V
L1:
aload 1
getstatic android/support/v7/a/n/LinearLayoutCompat_android_baselineAligned I
iconst_1
invokevirtual android/support/v7/internal/widget/ax/a(IZ)Z
istore 4
iload 4
ifne L2
aload 0
iload 4
invokevirtual android/support/v7/widget/aj/setBaselineAligned(Z)V
L2:
getstatic android/support/v7/a/n/LinearLayoutCompat_android_weightSum I
istore 3
aload 0
aload 1
getfield android/support/v7/internal/widget/ax/a Landroid/content/res/TypedArray;
iload 3
ldc_w -1.0F
invokevirtual android/content/res/TypedArray/getFloat(IF)F
putfield android/support/v7/widget/aj/g F
aload 0
aload 1
getstatic android/support/v7/a/n/LinearLayoutCompat_android_baselineAlignedChildIndex I
iconst_m1
invokevirtual android/support/v7/internal/widget/ax/a(II)I
putfield android/support/v7/widget/aj/b I
aload 0
aload 1
getstatic android/support/v7/a/n/LinearLayoutCompat_measureWithLargestChild I
iconst_0
invokevirtual android/support/v7/internal/widget/ax/a(IZ)Z
putfield android/support/v7/widget/aj/n Z
aload 0
aload 1
getstatic android/support/v7/a/n/LinearLayoutCompat_divider I
invokevirtual android/support/v7/internal/widget/ax/a(I)Landroid/graphics/drawable/Drawable;
invokevirtual android/support/v7/widget/aj/setDividerDrawable(Landroid/graphics/drawable/Drawable;)V
aload 0
aload 1
getstatic android/support/v7/a/n/LinearLayoutCompat_showDividers I
iconst_0
invokevirtual android/support/v7/internal/widget/ax/a(II)I
putfield android/support/v7/widget/aj/y I
aload 0
aload 1
getstatic android/support/v7/a/n/LinearLayoutCompat_dividerPadding I
iconst_0
invokevirtual android/support/v7/internal/widget/ax/c(II)I
putfield android/support/v7/widget/aj/z I
aload 1
getfield android/support/v7/internal/widget/ax/a Landroid/content/res/TypedArray;
invokevirtual android/content/res/TypedArray/recycle()V
return
.limit locals 5
.limit stack 4
.end method

.method private a(I)Landroid/view/View;
aload 0
iload 1
invokevirtual android/support/v7/widget/aj/getChildAt(I)Landroid/view/View;
areturn
.limit locals 2
.limit stack 2
.end method

.method private a(II)V
aload 0
iconst_0
putfield android/support/v7/widget/aj/f I
iconst_0
istore 7
iconst_0
istore 5
iconst_0
istore 6
iconst_0
istore 12
iconst_1
istore 8
fconst_0
fstore 3
aload 0
invokevirtual android/support/v7/widget/aj/getVirtualChildCount()I
istore 18
iload 1
invokestatic android/view/View$MeasureSpec/getMode(I)I
istore 19
iload 2
invokestatic android/view/View$MeasureSpec/getMode(I)I
istore 20
iconst_0
istore 11
iconst_0
istore 10
aload 0
getfield android/support/v7/widget/aj/b I
istore 21
aload 0
getfield android/support/v7/widget/aj/n Z
istore 22
ldc_w -2147483648
istore 9
iconst_0
istore 13
L0:
iload 13
iload 18
if_icmpge L1
aload 0
iload 13
invokevirtual android/support/v7/widget/aj/getChildAt(I)Landroid/view/View;
astore 23
aload 23
ifnonnull L2
aload 0
aload 0
getfield android/support/v7/widget/aj/f I
iconst_0
iadd
putfield android/support/v7/widget/aj/f I
iload 13
istore 17
iload 7
istore 16
iload 5
istore 15
iload 6
istore 14
iload 12
istore 13
iload 8
istore 7
iload 10
istore 6
iload 9
istore 5
L3:
iload 17
iconst_1
iadd
istore 17
iload 5
istore 9
iload 6
istore 10
iload 7
istore 8
iload 13
istore 12
iload 14
istore 6
iload 15
istore 5
iload 16
istore 7
iload 17
istore 13
goto L0
L2:
aload 23
invokevirtual android/view/View/getVisibility()I
bipush 8
if_icmpeq L4
aload 0
iload 13
invokespecial android/support/v7/widget/aj/b(I)Z
ifeq L5
aload 0
aload 0
getfield android/support/v7/widget/aj/f I
aload 0
getfield android/support/v7/widget/aj/x I
iadd
putfield android/support/v7/widget/aj/f I
L5:
aload 23
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v7/widget/al
astore 24
fload 3
aload 24
getfield android/support/v7/widget/al/g F
fadd
fstore 3
iload 20
ldc_w 1073741824
if_icmpne L6
aload 24
getfield android/support/v7/widget/al/height I
ifne L6
aload 24
getfield android/support/v7/widget/al/g F
fconst_0
fcmpl
ifle L6
aload 0
getfield android/support/v7/widget/aj/f I
istore 10
aload 0
iload 10
aload 24
getfield android/support/v7/widget/al/topMargin I
iload 10
iadd
aload 24
getfield android/support/v7/widget/al/bottomMargin I
iadd
invokestatic java/lang/Math/max(II)I
putfield android/support/v7/widget/aj/f I
iconst_1
istore 10
L7:
iload 21
iflt L8
iload 21
iload 13
iconst_1
iadd
if_icmpne L8
aload 0
aload 0
getfield android/support/v7/widget/aj/f I
putfield android/support/v7/widget/aj/c I
L8:
iload 13
iload 21
if_icmpge L9
aload 24
getfield android/support/v7/widget/al/g F
fconst_0
fcmpl
ifle L9
new java/lang/RuntimeException
dup
ldc "A child of LinearLayout with index less than mBaselineAlignedChildIndex has weight > 0, which won't work.  Either remove the weight, or don't set mBaselineAlignedChildIndex."
invokespecial java/lang/RuntimeException/<init>(Ljava/lang/String;)V
athrow
L6:
ldc_w -2147483648
istore 15
iload 15
istore 14
aload 24
getfield android/support/v7/widget/al/height I
ifne L10
iload 15
istore 14
aload 24
getfield android/support/v7/widget/al/g F
fconst_0
fcmpl
ifle L10
iconst_0
istore 14
aload 24
bipush -2
putfield android/support/v7/widget/al/height I
L10:
fload 3
fconst_0
fcmpl
ifne L11
aload 0
getfield android/support/v7/widget/aj/f I
istore 15
L12:
aload 0
aload 23
iload 1
iconst_0
iload 2
iload 15
invokespecial android/support/v7/widget/aj/a(Landroid/view/View;IIII)V
iload 14
ldc_w -2147483648
if_icmpeq L13
aload 24
iload 14
putfield android/support/v7/widget/al/height I
L13:
aload 23
invokevirtual android/view/View/getMeasuredHeight()I
istore 14
aload 0
getfield android/support/v7/widget/aj/f I
istore 15
aload 0
iload 15
iload 15
iload 14
iadd
aload 24
getfield android/support/v7/widget/al/topMargin I
iadd
aload 24
getfield android/support/v7/widget/al/bottomMargin I
iadd
iconst_0
iadd
invokestatic java/lang/Math/max(II)I
putfield android/support/v7/widget/aj/f I
iload 22
ifeq L14
iload 14
iload 9
invokestatic java/lang/Math/max(II)I
istore 9
goto L7
L11:
iconst_0
istore 15
goto L12
L9:
iconst_0
istore 14
iload 19
ldc_w 1073741824
if_icmpeq L15
aload 24
getfield android/support/v7/widget/al/width I
iconst_m1
if_icmpne L15
iconst_1
istore 11
iconst_1
istore 14
L16:
aload 24
getfield android/support/v7/widget/al/leftMargin I
aload 24
getfield android/support/v7/widget/al/rightMargin I
iadd
istore 15
aload 23
invokevirtual android/view/View/getMeasuredWidth()I
iload 15
iadd
istore 16
iload 7
iload 16
invokestatic java/lang/Math/max(II)I
istore 7
iload 5
aload 23
invokestatic android/support/v4/view/cx/j(Landroid/view/View;)I
invokestatic android/support/v7/internal/widget/bd/a(II)I
istore 17
iload 8
ifeq L17
aload 24
getfield android/support/v7/widget/al/width I
iconst_m1
if_icmpne L17
iconst_1
istore 5
L18:
aload 24
getfield android/support/v7/widget/al/g F
fconst_0
fcmpl
ifle L19
iload 14
ifeq L20
L21:
iload 12
iload 15
invokestatic java/lang/Math/max(II)I
istore 12
iload 6
istore 8
iload 10
istore 6
iload 7
istore 10
iload 12
istore 7
iload 9
istore 14
iload 17
istore 12
iload 8
istore 9
iload 7
istore 8
iload 5
istore 7
iload 6
istore 5
iload 14
istore 6
L22:
iload 13
iconst_0
iadd
istore 17
iload 5
istore 13
iload 6
istore 5
iload 13
istore 6
iload 8
istore 13
iload 9
istore 14
iload 12
istore 15
iload 10
istore 16
goto L3
L17:
iconst_0
istore 5
goto L18
L20:
iload 16
istore 15
goto L21
L19:
iload 14
ifeq L23
L24:
iload 6
iload 15
invokestatic java/lang/Math/max(II)I
istore 14
iload 5
istore 8
iload 10
istore 5
iload 9
istore 6
iload 7
istore 10
iload 8
istore 7
iload 12
istore 8
iload 14
istore 9
iload 17
istore 12
goto L22
L23:
iload 16
istore 15
goto L24
L1:
aload 0
getfield android/support/v7/widget/aj/f I
ifle L25
aload 0
iload 18
invokespecial android/support/v7/widget/aj/b(I)Z
ifeq L25
aload 0
aload 0
getfield android/support/v7/widget/aj/f I
aload 0
getfield android/support/v7/widget/aj/x I
iadd
putfield android/support/v7/widget/aj/f I
L25:
iload 22
ifeq L26
iload 20
ldc_w -2147483648
if_icmpeq L27
iload 20
ifne L26
L27:
aload 0
iconst_0
putfield android/support/v7/widget/aj/f I
iconst_0
istore 13
L28:
iload 13
iload 18
if_icmpge L26
aload 0
iload 13
invokevirtual android/support/v7/widget/aj/getChildAt(I)Landroid/view/View;
astore 23
aload 23
ifnonnull L29
aload 0
aload 0
getfield android/support/v7/widget/aj/f I
iconst_0
iadd
putfield android/support/v7/widget/aj/f I
L30:
iload 13
iconst_1
iadd
istore 13
goto L28
L29:
aload 23
invokevirtual android/view/View/getVisibility()I
bipush 8
if_icmpne L31
iload 13
iconst_0
iadd
istore 13
goto L30
L31:
aload 23
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v7/widget/al
astore 23
aload 0
getfield android/support/v7/widget/aj/f I
istore 14
aload 23
getfield android/support/v7/widget/al/topMargin I
istore 15
aload 0
iload 14
aload 23
getfield android/support/v7/widget/al/bottomMargin I
iload 14
iload 9
iadd
iload 15
iadd
iadd
iconst_0
iadd
invokestatic java/lang/Math/max(II)I
putfield android/support/v7/widget/aj/f I
goto L30
L26:
aload 0
aload 0
getfield android/support/v7/widget/aj/f I
aload 0
invokevirtual android/support/v7/widget/aj/getPaddingTop()I
aload 0
invokevirtual android/support/v7/widget/aj/getPaddingBottom()I
iadd
iadd
putfield android/support/v7/widget/aj/f I
aload 0
getfield android/support/v7/widget/aj/f I
aload 0
invokevirtual android/support/v7/widget/aj/getSuggestedMinimumHeight()I
invokestatic java/lang/Math/max(II)I
iload 2
iconst_0
invokestatic android/support/v4/view/cx/a(III)I
istore 15
ldc_w 16777215
iload 15
iand
aload 0
getfield android/support/v7/widget/aj/f I
isub
istore 13
iload 10
ifne L32
iload 13
ifeq L33
fload 3
fconst_0
fcmpl
ifle L33
L32:
aload 0
getfield android/support/v7/widget/aj/g F
fconst_0
fcmpl
ifle L34
aload 0
getfield android/support/v7/widget/aj/g F
fstore 3
L34:
aload 0
iconst_0
putfield android/support/v7/widget/aj/f I
iconst_0
istore 12
iload 8
istore 9
iload 7
istore 8
iload 6
istore 7
iload 9
istore 6
iload 13
istore 9
L35:
iload 12
iload 18
if_icmpge L36
aload 0
iload 12
invokevirtual android/support/v7/widget/aj/getChildAt(I)Landroid/view/View;
astore 23
aload 23
invokevirtual android/view/View/getVisibility()I
bipush 8
if_icmpeq L37
aload 23
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v7/widget/al
astore 24
aload 24
getfield android/support/v7/widget/al/g F
fstore 4
fload 4
fconst_0
fcmpl
ifle L38
iload 9
i2f
fload 4
fmul
fload 3
fdiv
f2i
istore 13
iload 1
aload 0
invokevirtual android/support/v7/widget/aj/getPaddingLeft()I
aload 0
invokevirtual android/support/v7/widget/aj/getPaddingRight()I
iadd
aload 24
getfield android/support/v7/widget/al/leftMargin I
iadd
aload 24
getfield android/support/v7/widget/al/rightMargin I
iadd
aload 24
getfield android/support/v7/widget/al/width I
invokestatic android/support/v7/widget/aj/getChildMeasureSpec(III)I
istore 16
aload 24
getfield android/support/v7/widget/al/height I
ifne L39
iload 20
ldc_w 1073741824
if_icmpeq L40
L39:
iload 13
aload 23
invokevirtual android/view/View/getMeasuredHeight()I
iadd
istore 14
iload 14
istore 10
iload 14
ifge L41
iconst_0
istore 10
L41:
aload 23
iload 16
iload 10
ldc_w 1073741824
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
invokevirtual android/view/View/measure(II)V
iload 5
aload 23
invokestatic android/support/v4/view/cx/j(Landroid/view/View;)I
sipush -256
iand
invokestatic android/support/v7/internal/widget/bd/a(II)I
istore 5
iload 9
iload 13
isub
istore 10
iload 5
istore 9
fload 3
fload 4
fsub
fstore 3
iload 10
istore 5
L42:
aload 24
getfield android/support/v7/widget/al/leftMargin I
aload 24
getfield android/support/v7/widget/al/rightMargin I
iadd
istore 13
aload 23
invokevirtual android/view/View/getMeasuredWidth()I
iload 13
iadd
istore 14
iload 8
iload 14
invokestatic java/lang/Math/max(II)I
istore 10
iload 19
ldc_w 1073741824
if_icmpeq L43
aload 24
getfield android/support/v7/widget/al/width I
iconst_m1
if_icmpne L43
iconst_1
istore 8
L44:
iload 8
ifeq L45
iload 13
istore 8
L46:
iload 7
iload 8
invokestatic java/lang/Math/max(II)I
istore 7
iload 6
ifeq L47
aload 24
getfield android/support/v7/widget/al/width I
iconst_m1
if_icmpne L47
iconst_1
istore 6
L48:
aload 0
getfield android/support/v7/widget/aj/f I
istore 8
aload 23
invokevirtual android/view/View/getMeasuredHeight()I
istore 13
aload 24
getfield android/support/v7/widget/al/topMargin I
istore 14
aload 0
iload 8
aload 24
getfield android/support/v7/widget/al/bottomMargin I
iload 13
iload 8
iadd
iload 14
iadd
iadd
iconst_0
iadd
invokestatic java/lang/Math/max(II)I
putfield android/support/v7/widget/aj/f I
iload 6
istore 8
iload 10
istore 6
L49:
iload 12
iconst_1
iadd
istore 13
iload 9
istore 12
iload 6
istore 10
iload 5
istore 9
iload 12
istore 5
iload 8
istore 6
iload 10
istore 8
iload 13
istore 12
goto L35
L40:
iload 13
ifle L50
iload 13
istore 10
goto L41
L50:
iconst_0
istore 10
goto L41
L43:
iconst_0
istore 8
goto L44
L45:
iload 14
istore 8
goto L46
L47:
iconst_0
istore 6
goto L48
L36:
aload 0
aload 0
getfield android/support/v7/widget/aj/f I
aload 0
invokevirtual android/support/v7/widget/aj/getPaddingTop()I
aload 0
invokevirtual android/support/v7/widget/aj/getPaddingBottom()I
iadd
iadd
putfield android/support/v7/widget/aj/f I
iload 7
istore 9
iload 8
istore 7
iload 6
istore 8
iload 9
istore 6
L51:
iload 8
ifne L52
iload 19
ldc_w 1073741824
if_icmpeq L52
L53:
aload 0
iload 6
aload 0
invokevirtual android/support/v7/widget/aj/getPaddingLeft()I
aload 0
invokevirtual android/support/v7/widget/aj/getPaddingRight()I
iadd
iadd
aload 0
invokevirtual android/support/v7/widget/aj/getSuggestedMinimumWidth()I
invokestatic java/lang/Math/max(II)I
iload 1
iload 5
invokestatic android/support/v4/view/cx/a(III)I
iload 15
invokevirtual android/support/v7/widget/aj/setMeasuredDimension(II)V
iload 11
ifeq L54
aload 0
iload 18
iload 2
invokespecial android/support/v7/widget/aj/b(II)V
L54:
return
L33:
iload 6
iload 12
invokestatic java/lang/Math/max(II)I
istore 10
iload 22
ifeq L55
iload 20
ldc_w 1073741824
if_icmpeq L55
iconst_0
istore 6
L56:
iload 6
iload 18
if_icmpge L55
aload 0
iload 6
invokevirtual android/support/v7/widget/aj/getChildAt(I)Landroid/view/View;
astore 23
aload 23
ifnull L57
aload 23
invokevirtual android/view/View/getVisibility()I
bipush 8
if_icmpeq L57
aload 23
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v7/widget/al
getfield android/support/v7/widget/al/g F
fconst_0
fcmpl
ifle L57
aload 23
aload 23
invokevirtual android/view/View/getMeasuredWidth()I
ldc_w 1073741824
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
iload 9
ldc_w 1073741824
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
invokevirtual android/view/View/measure(II)V
L57:
iload 6
iconst_1
iadd
istore 6
goto L56
L52:
iload 7
istore 6
goto L53
L55:
iload 10
istore 6
goto L51
L38:
iload 5
istore 10
iload 9
istore 5
iload 10
istore 9
goto L42
L37:
iload 6
istore 10
iload 8
istore 6
iload 5
istore 8
iload 9
istore 5
iload 8
istore 9
iload 10
istore 8
goto L49
L15:
goto L16
L14:
goto L7
L4:
iload 9
istore 14
iload 10
istore 15
iload 6
istore 9
iload 7
istore 10
iload 5
istore 16
iload 14
istore 6
iload 15
istore 5
iload 8
istore 7
iload 12
istore 8
iload 16
istore 12
goto L22
.limit locals 25
.limit stack 6
.end method

.method private a(IIII)V
aload 0
invokevirtual android/support/v7/widget/aj/getPaddingLeft()I
istore 5
iload 3
iload 1
isub
istore 6
aload 0
invokevirtual android/support/v7/widget/aj/getPaddingRight()I
istore 7
aload 0
invokevirtual android/support/v7/widget/aj/getPaddingRight()I
istore 8
aload 0
invokevirtual android/support/v7/widget/aj/getVirtualChildCount()I
istore 9
aload 0
getfield android/support/v7/widget/aj/e I
istore 1
aload 0
getfield android/support/v7/widget/aj/e I
istore 10
iload 1
bipush 112
iand
lookupswitch
16 : L0
80 : L1
default : L2
L2:
aload 0
invokevirtual android/support/v7/widget/aj/getPaddingTop()I
istore 1
L3:
iconst_0
istore 3
iload 1
istore 2
iload 3
istore 1
L4:
iload 1
iload 9
if_icmpge L5
aload 0
iload 1
invokevirtual android/support/v7/widget/aj/getChildAt(I)Landroid/view/View;
astore 13
aload 13
ifnonnull L6
iload 2
iconst_0
iadd
istore 2
L7:
iload 1
iconst_1
iadd
istore 1
goto L4
L1:
aload 0
invokevirtual android/support/v7/widget/aj/getPaddingTop()I
iload 4
iadd
iload 2
isub
aload 0
getfield android/support/v7/widget/aj/f I
isub
istore 1
goto L3
L0:
aload 0
invokevirtual android/support/v7/widget/aj/getPaddingTop()I
iload 4
iload 2
isub
aload 0
getfield android/support/v7/widget/aj/f I
isub
iconst_2
idiv
iadd
istore 1
goto L3
L6:
aload 13
invokevirtual android/view/View/getVisibility()I
bipush 8
if_icmpeq L8
aload 13
invokevirtual android/view/View/getMeasuredWidth()I
istore 11
aload 13
invokevirtual android/view/View/getMeasuredHeight()I
istore 12
aload 13
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v7/widget/al
astore 14
aload 14
getfield android/support/v7/widget/al/h I
istore 4
iload 4
istore 3
iload 4
ifge L9
ldc_w 8388615
iload 10
iand
istore 3
L9:
iload 3
aload 0
invokestatic android/support/v4/view/cx/f(Landroid/view/View;)I
invokestatic android/support/v4/view/v/a(II)I
bipush 7
iand
lookupswitch
1 : L10
5 : L11
default : L12
L12:
aload 14
getfield android/support/v7/widget/al/leftMargin I
iload 5
iadd
istore 3
L13:
iload 2
istore 4
aload 0
iload 1
invokespecial android/support/v7/widget/aj/b(I)Z
ifeq L14
iload 2
aload 0
getfield android/support/v7/widget/aj/x I
iadd
istore 4
L14:
iload 4
aload 14
getfield android/support/v7/widget/al/topMargin I
iadd
istore 2
aload 13
iload 3
iload 2
iconst_0
iadd
iload 11
iload 12
invokestatic android/support/v7/widget/aj/b(Landroid/view/View;IIII)V
iload 2
aload 14
getfield android/support/v7/widget/al/bottomMargin I
iload 12
iadd
iconst_0
iadd
iadd
istore 2
iload 1
iconst_0
iadd
istore 1
goto L7
L10:
iload 6
iload 5
isub
iload 8
isub
iload 11
isub
iconst_2
idiv
iload 5
iadd
aload 14
getfield android/support/v7/widget/al/leftMargin I
iadd
aload 14
getfield android/support/v7/widget/al/rightMargin I
isub
istore 3
goto L13
L11:
iload 6
iload 7
isub
iload 11
isub
aload 14
getfield android/support/v7/widget/al/rightMargin I
isub
istore 3
goto L13
L5:
return
L8:
goto L7
.limit locals 15
.limit stack 5
.end method

.method private a(Landroid/graphics/Canvas;)V
aload 0
invokevirtual android/support/v7/widget/aj/getVirtualChildCount()I
istore 3
iconst_0
istore 2
L0:
iload 2
iload 3
if_icmpge L1
aload 0
iload 2
invokevirtual android/support/v7/widget/aj/getChildAt(I)Landroid/view/View;
astore 4
aload 4
ifnull L2
aload 4
invokevirtual android/view/View/getVisibility()I
bipush 8
if_icmpeq L2
aload 0
iload 2
invokespecial android/support/v7/widget/aj/b(I)Z
ifeq L2
aload 4
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v7/widget/al
astore 5
aload 0
aload 1
aload 4
invokevirtual android/view/View/getTop()I
aload 5
getfield android/support/v7/widget/al/topMargin I
isub
aload 0
getfield android/support/v7/widget/aj/x I
isub
invokespecial android/support/v7/widget/aj/a(Landroid/graphics/Canvas;I)V
L2:
iload 2
iconst_1
iadd
istore 2
goto L0
L1:
aload 0
iload 3
invokespecial android/support/v7/widget/aj/b(I)Z
ifeq L3
aload 0
iload 3
iconst_1
isub
invokevirtual android/support/v7/widget/aj/getChildAt(I)Landroid/view/View;
astore 4
aload 4
ifnonnull L4
aload 0
invokevirtual android/support/v7/widget/aj/getHeight()I
aload 0
invokevirtual android/support/v7/widget/aj/getPaddingBottom()I
isub
aload 0
getfield android/support/v7/widget/aj/x I
isub
istore 2
L5:
aload 0
aload 1
iload 2
invokespecial android/support/v7/widget/aj/a(Landroid/graphics/Canvas;I)V
L3:
return
L4:
aload 4
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v7/widget/al
astore 5
aload 4
invokevirtual android/view/View/getBottom()I
istore 2
aload 5
getfield android/support/v7/widget/al/bottomMargin I
iload 2
iadd
istore 2
goto L5
.limit locals 6
.limit stack 4
.end method

.method private a(Landroid/graphics/Canvas;I)V
aload 0
getfield android/support/v7/widget/aj/v Landroid/graphics/drawable/Drawable;
aload 0
invokevirtual android/support/v7/widget/aj/getPaddingLeft()I
aload 0
getfield android/support/v7/widget/aj/z I
iadd
iload 2
aload 0
invokevirtual android/support/v7/widget/aj/getWidth()I
aload 0
invokevirtual android/support/v7/widget/aj/getPaddingRight()I
isub
aload 0
getfield android/support/v7/widget/aj/z I
isub
aload 0
getfield android/support/v7/widget/aj/x I
iload 2
iadd
invokevirtual android/graphics/drawable/Drawable/setBounds(IIII)V
aload 0
getfield android/support/v7/widget/aj/v Landroid/graphics/drawable/Drawable;
aload 1
invokevirtual android/graphics/drawable/Drawable/draw(Landroid/graphics/Canvas;)V
return
.limit locals 3
.limit stack 6
.end method

.method private a(Landroid/view/View;IIII)V
aload 0
aload 1
iload 2
iload 3
iload 4
iload 5
invokevirtual android/support/v7/widget/aj/measureChildWithMargins(Landroid/view/View;IIII)V
return
.limit locals 6
.limit stack 6
.end method

.method private a()Z
aload 0
getfield android/support/v7/widget/aj/a Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private b(II)V
aload 0
invokevirtual android/support/v7/widget/aj/getMeasuredWidth()I
ldc_w 1073741824
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
istore 4
iconst_0
istore 3
L0:
iload 3
iload 1
if_icmpge L1
aload 0
iload 3
invokevirtual android/support/v7/widget/aj/getChildAt(I)Landroid/view/View;
astore 6
aload 6
invokevirtual android/view/View/getVisibility()I
bipush 8
if_icmpeq L2
aload 6
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v7/widget/al
astore 7
aload 7
getfield android/support/v7/widget/al/width I
iconst_m1
if_icmpne L2
aload 7
getfield android/support/v7/widget/al/height I
istore 5
aload 7
aload 6
invokevirtual android/view/View/getMeasuredHeight()I
putfield android/support/v7/widget/al/height I
aload 0
aload 6
iload 4
iconst_0
iload 2
iconst_0
invokevirtual android/support/v7/widget/aj/measureChildWithMargins(Landroid/view/View;IIII)V
aload 7
iload 5
putfield android/support/v7/widget/al/height I
L2:
iload 3
iconst_1
iadd
istore 3
goto L0
L1:
return
.limit locals 8
.limit stack 6
.end method

.method private b(IIII)V
aload 0
invokestatic android/support/v7/internal/widget/bd/a(Landroid/view/View;)Z
istore 17
aload 0
invokevirtual android/support/v7/widget/aj/getPaddingTop()I
istore 7
iload 4
iload 2
isub
istore 9
aload 0
invokevirtual android/support/v7/widget/aj/getPaddingBottom()I
istore 10
aload 0
invokevirtual android/support/v7/widget/aj/getPaddingBottom()I
istore 11
aload 0
invokevirtual android/support/v7/widget/aj/getVirtualChildCount()I
istore 12
aload 0
getfield android/support/v7/widget/aj/e I
istore 2
aload 0
getfield android/support/v7/widget/aj/e I
istore 13
aload 0
getfield android/support/v7/widget/aj/a Z
istore 18
aload 0
getfield android/support/v7/widget/aj/o [I
astore 19
aload 0
getfield android/support/v7/widget/aj/p [I
astore 20
iload 2
ldc_w 8388615
iand
aload 0
invokestatic android/support/v4/view/cx/f(Landroid/view/View;)I
invokestatic android/support/v4/view/v/a(II)I
lookupswitch
1 : L0
5 : L1
default : L2
L2:
aload 0
invokevirtual android/support/v7/widget/aj/getPaddingLeft()I
istore 1
L3:
iload 17
ifeq L4
iload 12
iconst_1
isub
istore 5
iconst_m1
istore 4
L5:
iconst_0
istore 2
iload 1
istore 3
L6:
iload 2
iload 12
if_icmpge L7
iload 5
iload 4
iload 2
imul
iadd
istore 16
aload 0
iload 16
invokevirtual android/support/v7/widget/aj/getChildAt(I)Landroid/view/View;
astore 21
aload 21
ifnonnull L8
iload 3
iconst_0
iadd
istore 3
iload 2
istore 1
L9:
iload 1
iconst_1
iadd
istore 2
goto L6
L1:
aload 0
invokevirtual android/support/v7/widget/aj/getPaddingLeft()I
iload 3
iadd
iload 1
isub
aload 0
getfield android/support/v7/widget/aj/f I
isub
istore 1
goto L3
L0:
aload 0
invokevirtual android/support/v7/widget/aj/getPaddingLeft()I
iload 3
iload 1
isub
aload 0
getfield android/support/v7/widget/aj/f I
isub
iconst_2
idiv
iadd
istore 1
goto L3
L8:
aload 21
invokevirtual android/view/View/getVisibility()I
bipush 8
if_icmpeq L10
aload 21
invokevirtual android/view/View/getMeasuredWidth()I
istore 14
aload 21
invokevirtual android/view/View/getMeasuredHeight()I
istore 15
iconst_m1
istore 1
aload 21
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v7/widget/al
astore 22
iload 1
istore 6
iload 18
ifeq L11
iload 1
istore 6
aload 22
getfield android/support/v7/widget/al/height I
iconst_m1
if_icmpeq L11
aload 21
invokevirtual android/view/View/getBaseline()I
istore 6
L11:
aload 22
getfield android/support/v7/widget/al/h I
istore 8
iload 8
istore 1
iload 8
ifge L12
iload 13
bipush 112
iand
istore 1
L12:
iload 1
bipush 112
iand
lookupswitch
16 : L13
48 : L14
80 : L15
default : L16
L16:
iload 7
istore 1
L17:
aload 0
iload 16
invokespecial android/support/v7/widget/aj/b(I)Z
ifeq L18
aload 0
getfield android/support/v7/widget/aj/w I
iload 3
iadd
istore 3
L19:
iload 3
aload 22
getfield android/support/v7/widget/al/leftMargin I
iadd
istore 3
aload 21
iload 3
iconst_0
iadd
iload 1
iload 14
iload 15
invokestatic android/support/v7/widget/aj/b(Landroid/view/View;IIII)V
iload 3
aload 22
getfield android/support/v7/widget/al/rightMargin I
iload 14
iadd
iconst_0
iadd
iadd
istore 3
iload 2
iconst_0
iadd
istore 1
goto L9
L14:
aload 22
getfield android/support/v7/widget/al/topMargin I
iload 7
iadd
istore 8
iload 8
istore 1
iload 6
iconst_m1
if_icmpeq L20
aload 19
iconst_1
iaload
iload 6
isub
iload 8
iadd
istore 1
goto L17
L13:
iload 9
iload 7
isub
iload 11
isub
iload 15
isub
iconst_2
idiv
iload 7
iadd
aload 22
getfield android/support/v7/widget/al/topMargin I
iadd
aload 22
getfield android/support/v7/widget/al/bottomMargin I
isub
istore 1
goto L17
L15:
iload 9
iload 10
isub
iload 15
isub
aload 22
getfield android/support/v7/widget/al/bottomMargin I
isub
istore 8
iload 8
istore 1
iload 6
iconst_m1
if_icmpeq L20
aload 21
invokevirtual android/view/View/getMeasuredHeight()I
istore 1
iload 8
aload 20
iconst_2
iaload
iload 1
iload 6
isub
isub
isub
istore 1
goto L17
L7:
return
L18:
goto L19
L20:
goto L17
L10:
iload 2
istore 1
goto L9
L4:
iconst_0
istore 5
iconst_1
istore 4
goto L5
.limit locals 23
.limit stack 5
.end method

.method private b(Landroid/graphics/Canvas;)V
aload 0
invokevirtual android/support/v7/widget/aj/getVirtualChildCount()I
istore 4
aload 0
invokestatic android/support/v7/internal/widget/bd/a(Landroid/view/View;)Z
istore 5
iconst_0
istore 2
L0:
iload 2
iload 4
if_icmpge L1
aload 0
iload 2
invokevirtual android/support/v7/widget/aj/getChildAt(I)Landroid/view/View;
astore 6
aload 6
ifnull L2
aload 6
invokevirtual android/view/View/getVisibility()I
bipush 8
if_icmpeq L2
aload 0
iload 2
invokespecial android/support/v7/widget/aj/b(I)Z
ifeq L2
aload 6
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v7/widget/al
astore 7
iload 5
ifeq L3
aload 6
invokevirtual android/view/View/getRight()I
istore 3
aload 7
getfield android/support/v7/widget/al/rightMargin I
iload 3
iadd
istore 3
L4:
aload 0
aload 1
iload 3
invokespecial android/support/v7/widget/aj/b(Landroid/graphics/Canvas;I)V
L2:
iload 2
iconst_1
iadd
istore 2
goto L0
L3:
aload 6
invokevirtual android/view/View/getLeft()I
aload 7
getfield android/support/v7/widget/al/leftMargin I
isub
aload 0
getfield android/support/v7/widget/aj/w I
isub
istore 3
goto L4
L1:
aload 0
iload 4
invokespecial android/support/v7/widget/aj/b(I)Z
ifeq L5
aload 0
iload 4
iconst_1
isub
invokevirtual android/support/v7/widget/aj/getChildAt(I)Landroid/view/View;
astore 6
aload 6
ifnonnull L6
iload 5
ifeq L7
aload 0
invokevirtual android/support/v7/widget/aj/getPaddingLeft()I
istore 2
L8:
aload 0
aload 1
iload 2
invokespecial android/support/v7/widget/aj/b(Landroid/graphics/Canvas;I)V
L5:
return
L7:
aload 0
invokevirtual android/support/v7/widget/aj/getWidth()I
aload 0
invokevirtual android/support/v7/widget/aj/getPaddingRight()I
isub
aload 0
getfield android/support/v7/widget/aj/w I
isub
istore 2
goto L8
L6:
aload 6
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v7/widget/al
astore 7
iload 5
ifeq L9
aload 6
invokevirtual android/view/View/getLeft()I
aload 7
getfield android/support/v7/widget/al/leftMargin I
isub
aload 0
getfield android/support/v7/widget/aj/w I
isub
istore 2
goto L8
L9:
aload 6
invokevirtual android/view/View/getRight()I
istore 2
aload 7
getfield android/support/v7/widget/al/rightMargin I
iload 2
iadd
istore 2
goto L8
.limit locals 8
.limit stack 3
.end method

.method private b(Landroid/graphics/Canvas;I)V
aload 0
getfield android/support/v7/widget/aj/v Landroid/graphics/drawable/Drawable;
iload 2
aload 0
invokevirtual android/support/v7/widget/aj/getPaddingTop()I
aload 0
getfield android/support/v7/widget/aj/z I
iadd
aload 0
getfield android/support/v7/widget/aj/w I
iload 2
iadd
aload 0
invokevirtual android/support/v7/widget/aj/getHeight()I
aload 0
invokevirtual android/support/v7/widget/aj/getPaddingBottom()I
isub
aload 0
getfield android/support/v7/widget/aj/z I
isub
invokevirtual android/graphics/drawable/Drawable/setBounds(IIII)V
aload 0
getfield android/support/v7/widget/aj/v Landroid/graphics/drawable/Drawable;
aload 1
invokevirtual android/graphics/drawable/Drawable/draw(Landroid/graphics/Canvas;)V
return
.limit locals 3
.limit stack 6
.end method

.method private static b(Landroid/view/View;IIII)V
aload 0
iload 1
iload 2
iload 1
iload 3
iadd
iload 2
iload 4
iadd
invokevirtual android/view/View/layout(IIII)V
return
.limit locals 5
.limit stack 6
.end method

.method private b()Z
aload 0
getfield android/support/v7/widget/aj/n Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private b(I)Z
iload 1
ifne L0
aload 0
getfield android/support/v7/widget/aj/y I
iconst_1
iand
ifeq L1
L2:
iconst_1
ireturn
L1:
iconst_0
ireturn
L0:
iload 1
aload 0
invokevirtual android/support/v7/widget/aj/getChildCount()I
if_icmpne L3
aload 0
getfield android/support/v7/widget/aj/y I
iconst_4
iand
ifne L2
iconst_0
ireturn
L3:
aload 0
getfield android/support/v7/widget/aj/y I
iconst_2
iand
ifeq L4
iload 1
iconst_1
isub
istore 1
L5:
iload 1
iflt L6
aload 0
iload 1
invokevirtual android/support/v7/widget/aj/getChildAt(I)Landroid/view/View;
invokevirtual android/view/View/getVisibility()I
bipush 8
if_icmpne L2
iload 1
iconst_1
isub
istore 1
goto L5
L4:
iconst_0
ireturn
L6:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method private c(II)V
aload 0
iconst_0
putfield android/support/v7/widget/aj/f I
iconst_0
istore 10
iconst_0
istore 8
iconst_0
istore 7
iconst_0
istore 12
iconst_1
istore 9
fconst_0
fstore 3
aload 0
invokevirtual android/support/v7/widget/aj/getVirtualChildCount()I
istore 19
iload 1
invokestatic android/view/View$MeasureSpec/getMode(I)I
istore 21
iload 2
invokestatic android/view/View$MeasureSpec/getMode(I)I
istore 20
iconst_0
istore 11
iconst_0
istore 6
aload 0
getfield android/support/v7/widget/aj/o [I
ifnull L0
aload 0
getfield android/support/v7/widget/aj/p [I
ifnonnull L1
L0:
aload 0
iconst_4
newarray int
putfield android/support/v7/widget/aj/o [I
aload 0
iconst_4
newarray int
putfield android/support/v7/widget/aj/p [I
L1:
aload 0
getfield android/support/v7/widget/aj/o [I
astore 25
aload 0
getfield android/support/v7/widget/aj/p [I
astore 26
aload 25
iconst_3
iconst_m1
iastore
aload 25
iconst_2
iconst_m1
iastore
aload 25
iconst_1
iconst_m1
iastore
aload 25
iconst_0
iconst_m1
iastore
aload 26
iconst_3
iconst_m1
iastore
aload 26
iconst_2
iconst_m1
iastore
aload 26
iconst_1
iconst_m1
iastore
aload 26
iconst_0
iconst_m1
iastore
aload 0
getfield android/support/v7/widget/aj/a Z
istore 23
aload 0
getfield android/support/v7/widget/aj/n Z
istore 24
iload 21
ldc_w 1073741824
if_icmpne L2
iconst_1
istore 14
L3:
ldc_w -2147483648
istore 5
iconst_0
istore 13
L4:
iload 13
iload 19
if_icmpge L5
aload 0
iload 13
invokevirtual android/support/v7/widget/aj/getChildAt(I)Landroid/view/View;
astore 27
aload 27
ifnonnull L6
aload 0
aload 0
getfield android/support/v7/widget/aj/f I
iconst_0
iadd
putfield android/support/v7/widget/aj/f I
iload 13
istore 16
iload 8
istore 15
iload 7
istore 13
iload 12
istore 8
iload 9
istore 7
L7:
iload 16
iconst_1
iadd
istore 16
iload 7
istore 9
iload 8
istore 12
iload 13
istore 7
iload 15
istore 8
iload 16
istore 13
goto L4
L2:
iconst_0
istore 14
goto L3
L6:
aload 27
invokevirtual android/view/View/getVisibility()I
bipush 8
if_icmpeq L8
aload 0
iload 13
invokespecial android/support/v7/widget/aj/b(I)Z
ifeq L9
aload 0
aload 0
getfield android/support/v7/widget/aj/f I
aload 0
getfield android/support/v7/widget/aj/w I
iadd
putfield android/support/v7/widget/aj/f I
L9:
aload 27
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v7/widget/al
astore 28
fload 3
aload 28
getfield android/support/v7/widget/al/g F
fadd
fstore 3
iload 21
ldc_w 1073741824
if_icmpne L10
aload 28
getfield android/support/v7/widget/al/width I
ifne L10
aload 28
getfield android/support/v7/widget/al/g F
fconst_0
fcmpl
ifle L10
iload 14
ifeq L11
aload 0
aload 0
getfield android/support/v7/widget/aj/f I
aload 28
getfield android/support/v7/widget/al/leftMargin I
aload 28
getfield android/support/v7/widget/al/rightMargin I
iadd
iadd
putfield android/support/v7/widget/aj/f I
L12:
iload 23
ifeq L13
iconst_0
iconst_0
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
istore 15
aload 27
iload 15
iload 15
invokevirtual android/view/View/measure(II)V
L14:
iconst_0
istore 15
iload 20
ldc_w 1073741824
if_icmpeq L15
aload 28
getfield android/support/v7/widget/al/height I
iconst_m1
if_icmpne L15
iconst_1
istore 11
iconst_1
istore 15
L16:
aload 28
getfield android/support/v7/widget/al/topMargin I
aload 28
getfield android/support/v7/widget/al/bottomMargin I
iadd
istore 16
aload 27
invokevirtual android/view/View/getMeasuredHeight()I
iload 16
iadd
istore 17
iload 8
aload 27
invokestatic android/support/v4/view/cx/j(Landroid/view/View;)I
invokestatic android/support/v7/internal/widget/bd/a(II)I
istore 18
iload 23
ifeq L17
aload 27
invokevirtual android/view/View/getBaseline()I
istore 22
iload 22
iconst_m1
if_icmpeq L17
aload 28
getfield android/support/v7/widget/al/h I
ifge L18
aload 0
getfield android/support/v7/widget/aj/e I
istore 8
L19:
iload 8
bipush 112
iand
iconst_4
ishr
bipush -2
iand
iconst_1
ishr
istore 8
aload 25
iload 8
aload 25
iload 8
iaload
iload 22
invokestatic java/lang/Math/max(II)I
iastore
aload 26
iload 8
aload 26
iload 8
iaload
iload 17
iload 22
isub
invokestatic java/lang/Math/max(II)I
iastore
L17:
iload 10
iload 17
invokestatic java/lang/Math/max(II)I
istore 10
iload 9
ifeq L20
aload 28
getfield android/support/v7/widget/al/height I
iconst_m1
if_icmpne L20
iconst_1
istore 8
L21:
aload 28
getfield android/support/v7/widget/al/g F
fconst_0
fcmpl
ifle L22
iload 15
ifeq L23
L24:
iload 12
iload 16
invokestatic java/lang/Math/max(II)I
istore 9
iload 8
istore 15
iload 9
istore 8
iload 5
istore 16
iload 18
istore 12
iload 7
istore 9
iload 15
istore 7
iload 6
istore 5
iload 16
istore 6
L25:
iload 13
iconst_0
iadd
istore 16
iload 5
istore 13
iload 6
istore 5
iload 13
istore 6
iload 9
istore 13
iload 12
istore 15
goto L7
L11:
aload 0
getfield android/support/v7/widget/aj/f I
istore 15
aload 0
iload 15
aload 28
getfield android/support/v7/widget/al/leftMargin I
iload 15
iadd
aload 28
getfield android/support/v7/widget/al/rightMargin I
iadd
invokestatic java/lang/Math/max(II)I
putfield android/support/v7/widget/aj/f I
goto L12
L13:
iconst_1
istore 6
goto L14
L10:
ldc_w -2147483648
istore 16
iload 16
istore 15
aload 28
getfield android/support/v7/widget/al/width I
ifne L26
iload 16
istore 15
aload 28
getfield android/support/v7/widget/al/g F
fconst_0
fcmpl
ifle L26
iconst_0
istore 15
aload 28
bipush -2
putfield android/support/v7/widget/al/width I
L26:
fload 3
fconst_0
fcmpl
ifne L27
aload 0
getfield android/support/v7/widget/aj/f I
istore 16
L28:
aload 0
aload 27
iload 1
iload 16
iload 2
iconst_0
invokespecial android/support/v7/widget/aj/a(Landroid/view/View;IIII)V
iload 15
ldc_w -2147483648
if_icmpeq L29
aload 28
iload 15
putfield android/support/v7/widget/al/width I
L29:
aload 27
invokevirtual android/view/View/getMeasuredWidth()I
istore 15
iload 14
ifeq L30
aload 0
aload 0
getfield android/support/v7/widget/aj/f I
aload 28
getfield android/support/v7/widget/al/leftMargin I
iload 15
iadd
aload 28
getfield android/support/v7/widget/al/rightMargin I
iadd
iconst_0
iadd
iadd
putfield android/support/v7/widget/aj/f I
L31:
iload 24
ifeq L32
iload 15
iload 5
invokestatic java/lang/Math/max(II)I
istore 5
goto L14
L27:
iconst_0
istore 16
goto L28
L30:
aload 0
getfield android/support/v7/widget/aj/f I
istore 16
aload 0
iload 16
iload 16
iload 15
iadd
aload 28
getfield android/support/v7/widget/al/leftMargin I
iadd
aload 28
getfield android/support/v7/widget/al/rightMargin I
iadd
iconst_0
iadd
invokestatic java/lang/Math/max(II)I
putfield android/support/v7/widget/aj/f I
goto L31
L18:
aload 28
getfield android/support/v7/widget/al/h I
istore 8
goto L19
L20:
iconst_0
istore 8
goto L21
L23:
iload 17
istore 16
goto L24
L22:
iload 15
ifeq L33
L34:
iload 7
iload 16
invokestatic java/lang/Math/max(II)I
istore 9
iload 8
istore 7
iload 6
istore 15
iload 12
istore 8
iload 5
istore 6
iload 15
istore 5
iload 18
istore 12
goto L25
L33:
iload 17
istore 16
goto L34
L5:
aload 0
getfield android/support/v7/widget/aj/f I
ifle L35
aload 0
iload 19
invokespecial android/support/v7/widget/aj/b(I)Z
ifeq L35
aload 0
aload 0
getfield android/support/v7/widget/aj/f I
aload 0
getfield android/support/v7/widget/aj/w I
iadd
putfield android/support/v7/widget/aj/f I
L35:
aload 25
iconst_1
iaload
iconst_m1
if_icmpne L36
aload 25
iconst_0
iaload
iconst_m1
if_icmpne L36
aload 25
iconst_2
iaload
iconst_m1
if_icmpne L36
aload 25
iconst_3
iaload
iconst_m1
if_icmpeq L37
L36:
iload 10
aload 25
iconst_3
iaload
aload 25
iconst_0
iaload
aload 25
iconst_1
iaload
aload 25
iconst_2
iaload
invokestatic java/lang/Math/max(II)I
invokestatic java/lang/Math/max(II)I
invokestatic java/lang/Math/max(II)I
aload 26
iconst_3
iaload
aload 26
iconst_0
iaload
aload 26
iconst_1
iaload
aload 26
iconst_2
iaload
invokestatic java/lang/Math/max(II)I
invokestatic java/lang/Math/max(II)I
invokestatic java/lang/Math/max(II)I
iadd
invokestatic java/lang/Math/max(II)I
istore 13
L38:
iload 24
ifeq L39
iload 21
ldc_w -2147483648
if_icmpeq L40
iload 21
ifne L39
L40:
aload 0
iconst_0
putfield android/support/v7/widget/aj/f I
iconst_0
istore 10
L41:
iload 10
iload 19
if_icmpge L39
aload 0
iload 10
invokevirtual android/support/v7/widget/aj/getChildAt(I)Landroid/view/View;
astore 27
aload 27
ifnonnull L42
aload 0
aload 0
getfield android/support/v7/widget/aj/f I
iconst_0
iadd
putfield android/support/v7/widget/aj/f I
L43:
iload 10
iconst_1
iadd
istore 10
goto L41
L42:
aload 27
invokevirtual android/view/View/getVisibility()I
bipush 8
if_icmpne L44
iload 10
iconst_0
iadd
istore 10
goto L43
L44:
aload 27
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v7/widget/al
astore 27
iload 14
ifeq L45
aload 0
getfield android/support/v7/widget/aj/f I
istore 15
aload 27
getfield android/support/v7/widget/al/leftMargin I
istore 16
aload 0
aload 27
getfield android/support/v7/widget/al/rightMargin I
iload 16
iload 5
iadd
iadd
iconst_0
iadd
iload 15
iadd
putfield android/support/v7/widget/aj/f I
goto L43
L45:
aload 0
getfield android/support/v7/widget/aj/f I
istore 15
aload 27
getfield android/support/v7/widget/al/leftMargin I
istore 16
aload 0
iload 15
aload 27
getfield android/support/v7/widget/al/rightMargin I
iload 15
iload 5
iadd
iload 16
iadd
iadd
iconst_0
iadd
invokestatic java/lang/Math/max(II)I
putfield android/support/v7/widget/aj/f I
goto L43
L39:
aload 0
aload 0
getfield android/support/v7/widget/aj/f I
aload 0
invokevirtual android/support/v7/widget/aj/getPaddingLeft()I
aload 0
invokevirtual android/support/v7/widget/aj/getPaddingRight()I
iadd
iadd
putfield android/support/v7/widget/aj/f I
aload 0
getfield android/support/v7/widget/aj/f I
aload 0
invokevirtual android/support/v7/widget/aj/getSuggestedMinimumWidth()I
invokestatic java/lang/Math/max(II)I
iload 1
iconst_0
invokestatic android/support/v4/view/cx/a(III)I
istore 17
ldc_w 16777215
iload 17
iand
aload 0
getfield android/support/v7/widget/aj/f I
isub
istore 10
iload 6
ifne L46
iload 10
ifeq L47
fload 3
fconst_0
fcmpl
ifle L47
L46:
aload 0
getfield android/support/v7/widget/aj/g F
fconst_0
fcmpl
ifle L48
aload 0
getfield android/support/v7/widget/aj/g F
fstore 3
L48:
aload 25
iconst_3
iconst_m1
iastore
aload 25
iconst_2
iconst_m1
iastore
aload 25
iconst_1
iconst_m1
iastore
aload 25
iconst_0
iconst_m1
iastore
aload 26
iconst_3
iconst_m1
iastore
aload 26
iconst_2
iconst_m1
iastore
aload 26
iconst_1
iconst_m1
iastore
aload 26
iconst_0
iconst_m1
iastore
aload 0
iconst_0
putfield android/support/v7/widget/aj/f I
iconst_0
istore 12
iload 9
istore 6
iconst_m1
istore 9
iload 8
istore 5
iload 9
istore 8
iload 10
istore 9
L49:
iload 12
iload 19
if_icmpge L50
aload 0
iload 12
invokevirtual android/support/v7/widget/aj/getChildAt(I)Landroid/view/View;
astore 27
aload 27
ifnull L51
aload 27
invokevirtual android/view/View/getVisibility()I
bipush 8
if_icmpeq L51
aload 27
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v7/widget/al
astore 28
aload 28
getfield android/support/v7/widget/al/g F
fstore 4
fload 4
fconst_0
fcmpl
ifle L52
iload 9
i2f
fload 4
fmul
fload 3
fdiv
f2i
istore 13
iload 2
aload 0
invokevirtual android/support/v7/widget/aj/getPaddingTop()I
aload 0
invokevirtual android/support/v7/widget/aj/getPaddingBottom()I
iadd
aload 28
getfield android/support/v7/widget/al/topMargin I
iadd
aload 28
getfield android/support/v7/widget/al/bottomMargin I
iadd
aload 28
getfield android/support/v7/widget/al/height I
invokestatic android/support/v7/widget/aj/getChildMeasureSpec(III)I
istore 16
aload 28
getfield android/support/v7/widget/al/width I
ifne L53
iload 21
ldc_w 1073741824
if_icmpeq L54
L53:
iload 13
aload 27
invokevirtual android/view/View/getMeasuredWidth()I
iadd
istore 15
iload 15
istore 10
iload 15
ifge L55
iconst_0
istore 10
L55:
aload 27
iload 10
ldc_w 1073741824
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
iload 16
invokevirtual android/view/View/measure(II)V
iload 5
aload 27
invokestatic android/support/v4/view/cx/j(Landroid/view/View;)I
ldc_w -16777216
iand
invokestatic android/support/v7/internal/widget/bd/a(II)I
istore 5
fload 3
fload 4
fsub
fstore 3
iload 9
iload 13
isub
istore 10
iload 5
istore 9
iload 10
istore 5
L56:
iload 14
ifeq L57
aload 0
aload 0
getfield android/support/v7/widget/aj/f I
aload 27
invokevirtual android/view/View/getMeasuredWidth()I
aload 28
getfield android/support/v7/widget/al/leftMargin I
iadd
aload 28
getfield android/support/v7/widget/al/rightMargin I
iadd
iconst_0
iadd
iadd
putfield android/support/v7/widget/aj/f I
L58:
iload 20
ldc_w 1073741824
if_icmpeq L59
aload 28
getfield android/support/v7/widget/al/height I
iconst_m1
if_icmpne L59
iconst_1
istore 10
L60:
aload 28
getfield android/support/v7/widget/al/topMargin I
aload 28
getfield android/support/v7/widget/al/bottomMargin I
iadd
istore 16
aload 27
invokevirtual android/view/View/getMeasuredHeight()I
iload 16
iadd
istore 15
iload 8
iload 15
invokestatic java/lang/Math/max(II)I
istore 13
iload 10
ifeq L61
iload 16
istore 8
L62:
iload 7
iload 8
invokestatic java/lang/Math/max(II)I
istore 8
iload 6
ifeq L63
aload 28
getfield android/support/v7/widget/al/height I
iconst_m1
if_icmpne L63
iconst_1
istore 6
L64:
iload 23
ifeq L65
aload 27
invokevirtual android/view/View/getBaseline()I
istore 10
iload 10
iconst_m1
if_icmpeq L65
aload 28
getfield android/support/v7/widget/al/h I
ifge L66
aload 0
getfield android/support/v7/widget/aj/e I
istore 7
L67:
iload 7
bipush 112
iand
iconst_4
ishr
bipush -2
iand
iconst_1
ishr
istore 7
aload 25
iload 7
aload 25
iload 7
iaload
iload 10
invokestatic java/lang/Math/max(II)I
iastore
aload 26
iload 7
aload 26
iload 7
iaload
iload 15
iload 10
isub
invokestatic java/lang/Math/max(II)I
iastore
L65:
iload 9
istore 10
iload 6
istore 9
iload 13
istore 7
iload 10
istore 6
L68:
iload 12
iconst_1
iadd
istore 13
iload 9
istore 10
iload 7
istore 12
iload 5
istore 9
iload 6
istore 5
iload 10
istore 6
iload 8
istore 7
iload 12
istore 8
iload 13
istore 12
goto L49
L54:
iload 13
ifle L69
iload 13
istore 10
goto L55
L69:
iconst_0
istore 10
goto L55
L57:
aload 0
getfield android/support/v7/widget/aj/f I
istore 10
aload 0
iload 10
aload 27
invokevirtual android/view/View/getMeasuredWidth()I
iload 10
iadd
aload 28
getfield android/support/v7/widget/al/leftMargin I
iadd
aload 28
getfield android/support/v7/widget/al/rightMargin I
iadd
iconst_0
iadd
invokestatic java/lang/Math/max(II)I
putfield android/support/v7/widget/aj/f I
goto L58
L59:
iconst_0
istore 10
goto L60
L61:
iload 15
istore 8
goto L62
L63:
iconst_0
istore 6
goto L64
L66:
aload 28
getfield android/support/v7/widget/al/h I
istore 7
goto L67
L50:
aload 0
aload 0
getfield android/support/v7/widget/aj/f I
aload 0
invokevirtual android/support/v7/widget/aj/getPaddingLeft()I
aload 0
invokevirtual android/support/v7/widget/aj/getPaddingRight()I
iadd
iadd
putfield android/support/v7/widget/aj/f I
aload 25
iconst_1
iaload
iconst_m1
if_icmpne L70
aload 25
iconst_0
iaload
iconst_m1
if_icmpne L70
aload 25
iconst_2
iaload
iconst_m1
if_icmpne L70
iload 8
istore 9
aload 25
iconst_3
iaload
iconst_m1
if_icmpeq L71
L70:
iload 8
aload 25
iconst_3
iaload
aload 25
iconst_0
iaload
aload 25
iconst_1
iaload
aload 25
iconst_2
iaload
invokestatic java/lang/Math/max(II)I
invokestatic java/lang/Math/max(II)I
invokestatic java/lang/Math/max(II)I
aload 26
iconst_3
iaload
aload 26
iconst_0
iaload
aload 26
iconst_1
iaload
aload 26
iconst_2
iaload
invokestatic java/lang/Math/max(II)I
invokestatic java/lang/Math/max(II)I
invokestatic java/lang/Math/max(II)I
iadd
invokestatic java/lang/Math/max(II)I
istore 9
L71:
iload 5
istore 8
iload 6
istore 10
iload 9
istore 6
iload 7
istore 5
L72:
iload 10
ifne L73
iload 20
ldc_w 1073741824
if_icmpeq L73
L74:
aload 0
ldc_w -16777216
iload 8
iand
iload 17
ior
iload 5
aload 0
invokevirtual android/support/v7/widget/aj/getPaddingTop()I
aload 0
invokevirtual android/support/v7/widget/aj/getPaddingBottom()I
iadd
iadd
aload 0
invokevirtual android/support/v7/widget/aj/getSuggestedMinimumHeight()I
invokestatic java/lang/Math/max(II)I
iload 2
iload 8
bipush 16
ishl
invokestatic android/support/v4/view/cx/a(III)I
invokevirtual android/support/v7/widget/aj/setMeasuredDimension(II)V
iload 11
ifeq L75
aload 0
invokevirtual android/support/v7/widget/aj/getMeasuredHeight()I
ldc_w 1073741824
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
istore 5
iconst_0
istore 2
L76:
iload 2
iload 19
if_icmpge L75
aload 0
iload 2
invokevirtual android/support/v7/widget/aj/getChildAt(I)Landroid/view/View;
astore 25
aload 25
invokevirtual android/view/View/getVisibility()I
bipush 8
if_icmpeq L77
aload 25
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v7/widget/al
astore 26
aload 26
getfield android/support/v7/widget/al/height I
iconst_m1
if_icmpne L77
aload 26
getfield android/support/v7/widget/al/width I
istore 6
aload 26
aload 25
invokevirtual android/view/View/getMeasuredWidth()I
putfield android/support/v7/widget/al/width I
aload 0
aload 25
iload 1
iconst_0
iload 5
iconst_0
invokevirtual android/support/v7/widget/aj/measureChildWithMargins(Landroid/view/View;IIII)V
aload 26
iload 6
putfield android/support/v7/widget/al/width I
L77:
iload 2
iconst_1
iadd
istore 2
goto L76
L47:
iload 7
iload 12
invokestatic java/lang/Math/max(II)I
istore 7
iload 24
ifeq L78
iload 21
ldc_w 1073741824
if_icmpeq L78
iconst_0
istore 6
L79:
iload 6
iload 19
if_icmpge L78
aload 0
iload 6
invokevirtual android/support/v7/widget/aj/getChildAt(I)Landroid/view/View;
astore 25
aload 25
ifnull L80
aload 25
invokevirtual android/view/View/getVisibility()I
bipush 8
if_icmpeq L80
aload 25
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v7/widget/al
getfield android/support/v7/widget/al/g F
fconst_0
fcmpl
ifle L80
aload 25
iload 5
ldc_w 1073741824
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
aload 25
invokevirtual android/view/View/getMeasuredHeight()I
ldc_w 1073741824
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
invokevirtual android/view/View/measure(II)V
L80:
iload 6
iconst_1
iadd
istore 6
goto L79
L75:
return
L73:
iload 6
istore 5
goto L74
L78:
iload 7
istore 5
iload 13
istore 6
iload 9
istore 10
goto L72
L52:
iload 5
istore 10
iload 9
istore 5
iload 10
istore 9
goto L56
L51:
iload 6
istore 13
iload 5
istore 6
iload 8
istore 10
iload 9
istore 5
iload 13
istore 9
iload 7
istore 8
iload 10
istore 7
goto L68
L37:
iload 10
istore 13
goto L38
L15:
goto L16
L32:
goto L14
L8:
iload 6
istore 16
iload 7
istore 15
iload 8
istore 17
iload 5
istore 6
iload 16
istore 5
iload 9
istore 7
iload 12
istore 8
iload 15
istore 9
iload 17
istore 12
goto L25
.limit locals 29
.limit stack 7
.end method

.method private static d()I
iconst_0
ireturn
.limit locals 0
.limit stack 1
.end method

.method private d(II)V
aload 0
invokevirtual android/support/v7/widget/aj/getMeasuredHeight()I
ldc_w 1073741824
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
istore 4
iconst_0
istore 3
L0:
iload 3
iload 1
if_icmpge L1
aload 0
iload 3
invokevirtual android/support/v7/widget/aj/getChildAt(I)Landroid/view/View;
astore 6
aload 6
invokevirtual android/view/View/getVisibility()I
bipush 8
if_icmpeq L2
aload 6
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v7/widget/al
astore 7
aload 7
getfield android/support/v7/widget/al/height I
iconst_m1
if_icmpne L2
aload 7
getfield android/support/v7/widget/al/width I
istore 5
aload 7
aload 6
invokevirtual android/view/View/getMeasuredWidth()I
putfield android/support/v7/widget/al/width I
aload 0
aload 6
iload 2
iconst_0
iload 4
iconst_0
invokevirtual android/support/v7/widget/aj/measureChildWithMargins(Landroid/view/View;IIII)V
aload 7
iload 5
putfield android/support/v7/widget/al/width I
L2:
iload 3
iconst_1
iadd
istore 3
goto L0
L1:
return
.limit locals 8
.limit stack 6
.end method

.method private static getChildrenSkipCount$5359dca7()I
iconst_0
ireturn
.limit locals 0
.limit stack 1
.end method

.method private static getLocationOffset$3c7ec8d0()I
iconst_0
ireturn
.limit locals 0
.limit stack 1
.end method

.method private static getNextLocationOffset$3c7ec8d0()I
iconst_0
ireturn
.limit locals 0
.limit stack 1
.end method

.method public a(Landroid/util/AttributeSet;)Landroid/support/v7/widget/al;
new android/support/v7/widget/al
dup
aload 0
invokevirtual android/support/v7/widget/aj/getContext()Landroid/content/Context;
aload 1
invokespecial android/support/v7/widget/al/<init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
areturn
.limit locals 2
.limit stack 4
.end method

.method protected b(Landroid/view/ViewGroup$LayoutParams;)Landroid/support/v7/widget/al;
new android/support/v7/widget/al
dup
aload 1
invokespecial android/support/v7/widget/al/<init>(Landroid/view/ViewGroup$LayoutParams;)V
areturn
.limit locals 2
.limit stack 3
.end method

.method protected c()Landroid/support/v7/widget/al;
aload 0
getfield android/support/v7/widget/aj/d I
ifne L0
new android/support/v7/widget/al
dup
bipush -2
bipush -2
invokespecial android/support/v7/widget/al/<init>(II)V
areturn
L0:
aload 0
getfield android/support/v7/widget/aj/d I
iconst_1
if_icmpne L1
new android/support/v7/widget/al
dup
iconst_m1
bipush -2
invokespecial android/support/v7/widget/al/<init>(II)V
areturn
L1:
aconst_null
areturn
.limit locals 1
.limit stack 4
.end method

.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
aload 1
instanceof android/support/v7/widget/al
ireturn
.limit locals 2
.limit stack 1
.end method

.method protected synthetic generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
aload 0
invokevirtual android/support/v7/widget/aj/c()Landroid/support/v7/widget/al;
areturn
.limit locals 1
.limit stack 1
.end method

.method public synthetic generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
aload 0
aload 1
invokevirtual android/support/v7/widget/aj/a(Landroid/util/AttributeSet;)Landroid/support/v7/widget/al;
areturn
.limit locals 2
.limit stack 2
.end method

.method protected synthetic generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
aload 0
aload 1
invokevirtual android/support/v7/widget/aj/b(Landroid/view/ViewGroup$LayoutParams;)Landroid/support/v7/widget/al;
areturn
.limit locals 2
.limit stack 2
.end method

.method public getBaseline()I
iconst_m1
istore 1
aload 0
getfield android/support/v7/widget/aj/b I
ifge L0
aload 0
invokespecial android/view/ViewGroup/getBaseline()I
istore 1
L1:
iload 1
ireturn
L0:
aload 0
invokevirtual android/support/v7/widget/aj/getChildCount()I
aload 0
getfield android/support/v7/widget/aj/b I
if_icmpgt L2
new java/lang/RuntimeException
dup
ldc "mBaselineAlignedChildIndex of LinearLayout set to an index that is out of bounds."
invokespecial java/lang/RuntimeException/<init>(Ljava/lang/String;)V
athrow
L2:
aload 0
aload 0
getfield android/support/v7/widget/aj/b I
invokevirtual android/support/v7/widget/aj/getChildAt(I)Landroid/view/View;
astore 4
aload 4
invokevirtual android/view/View/getBaseline()I
istore 2
iload 2
iconst_m1
if_icmpne L3
aload 0
getfield android/support/v7/widget/aj/b I
ifeq L1
new java/lang/RuntimeException
dup
ldc "mBaselineAlignedChildIndex of LinearLayout points to a View that doesn't know how to get its baseline."
invokespecial java/lang/RuntimeException/<init>(Ljava/lang/String;)V
athrow
L3:
aload 0
getfield android/support/v7/widget/aj/c I
istore 1
aload 0
getfield android/support/v7/widget/aj/d I
iconst_1
if_icmpne L4
aload 0
getfield android/support/v7/widget/aj/e I
bipush 112
iand
istore 3
iload 3
bipush 48
if_icmpeq L4
iload 3
lookupswitch
16 : L5
80 : L6
default : L4
L4:
aload 4
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v7/widget/al
getfield android/support/v7/widget/al/topMargin I
iload 1
iadd
iload 2
iadd
ireturn
L6:
aload 0
invokevirtual android/support/v7/widget/aj/getBottom()I
aload 0
invokevirtual android/support/v7/widget/aj/getTop()I
isub
aload 0
invokevirtual android/support/v7/widget/aj/getPaddingBottom()I
isub
aload 0
getfield android/support/v7/widget/aj/f I
isub
istore 1
goto L4
L5:
iload 1
aload 0
invokevirtual android/support/v7/widget/aj/getBottom()I
aload 0
invokevirtual android/support/v7/widget/aj/getTop()I
isub
aload 0
invokevirtual android/support/v7/widget/aj/getPaddingTop()I
isub
aload 0
invokevirtual android/support/v7/widget/aj/getPaddingBottom()I
isub
aload 0
getfield android/support/v7/widget/aj/f I
isub
iconst_2
idiv
iadd
istore 1
goto L4
.limit locals 5
.limit stack 3
.end method

.method public getBaselineAlignedChildIndex()I
aload 0
getfield android/support/v7/widget/aj/b I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getDividerDrawable()Landroid/graphics/drawable/Drawable;
aload 0
getfield android/support/v7/widget/aj/v Landroid/graphics/drawable/Drawable;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getDividerPadding()I
aload 0
getfield android/support/v7/widget/aj/z I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getDividerWidth()I
aload 0
getfield android/support/v7/widget/aj/w I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getOrientation()I
aload 0
getfield android/support/v7/widget/aj/d I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getShowDividers()I
aload 0
getfield android/support/v7/widget/aj/y I
ireturn
.limit locals 1
.limit stack 1
.end method

.method getVirtualChildCount()I
aload 0
invokevirtual android/support/v7/widget/aj/getChildCount()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getWeightSum()F
aload 0
getfield android/support/v7/widget/aj/g F
freturn
.limit locals 1
.limit stack 1
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
aload 0
getfield android/support/v7/widget/aj/v Landroid/graphics/drawable/Drawable;
ifnonnull L0
L1:
return
L0:
aload 0
getfield android/support/v7/widget/aj/d I
iconst_1
if_icmpne L2
aload 0
invokevirtual android/support/v7/widget/aj/getVirtualChildCount()I
istore 3
iconst_0
istore 2
L3:
iload 2
iload 3
if_icmpge L4
aload 0
iload 2
invokevirtual android/support/v7/widget/aj/getChildAt(I)Landroid/view/View;
astore 6
aload 6
ifnull L5
aload 6
invokevirtual android/view/View/getVisibility()I
bipush 8
if_icmpeq L5
aload 0
iload 2
invokespecial android/support/v7/widget/aj/b(I)Z
ifeq L5
aload 6
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v7/widget/al
astore 7
aload 0
aload 1
aload 6
invokevirtual android/view/View/getTop()I
aload 7
getfield android/support/v7/widget/al/topMargin I
isub
aload 0
getfield android/support/v7/widget/aj/x I
isub
invokespecial android/support/v7/widget/aj/a(Landroid/graphics/Canvas;I)V
L5:
iload 2
iconst_1
iadd
istore 2
goto L3
L4:
aload 0
iload 3
invokespecial android/support/v7/widget/aj/b(I)Z
ifeq L1
aload 0
iload 3
iconst_1
isub
invokevirtual android/support/v7/widget/aj/getChildAt(I)Landroid/view/View;
astore 6
aload 6
ifnonnull L6
aload 0
invokevirtual android/support/v7/widget/aj/getHeight()I
aload 0
invokevirtual android/support/v7/widget/aj/getPaddingBottom()I
isub
aload 0
getfield android/support/v7/widget/aj/x I
isub
istore 2
L7:
aload 0
aload 1
iload 2
invokespecial android/support/v7/widget/aj/a(Landroid/graphics/Canvas;I)V
return
L6:
aload 6
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v7/widget/al
astore 7
aload 6
invokevirtual android/view/View/getBottom()I
istore 2
aload 7
getfield android/support/v7/widget/al/bottomMargin I
iload 2
iadd
istore 2
goto L7
L2:
aload 0
invokevirtual android/support/v7/widget/aj/getVirtualChildCount()I
istore 4
aload 0
invokestatic android/support/v7/internal/widget/bd/a(Landroid/view/View;)Z
istore 5
iconst_0
istore 2
L8:
iload 2
iload 4
if_icmpge L9
aload 0
iload 2
invokevirtual android/support/v7/widget/aj/getChildAt(I)Landroid/view/View;
astore 6
aload 6
ifnull L10
aload 6
invokevirtual android/view/View/getVisibility()I
bipush 8
if_icmpeq L10
aload 0
iload 2
invokespecial android/support/v7/widget/aj/b(I)Z
ifeq L10
aload 6
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v7/widget/al
astore 7
iload 5
ifeq L11
aload 6
invokevirtual android/view/View/getRight()I
istore 3
aload 7
getfield android/support/v7/widget/al/rightMargin I
iload 3
iadd
istore 3
L12:
aload 0
aload 1
iload 3
invokespecial android/support/v7/widget/aj/b(Landroid/graphics/Canvas;I)V
L10:
iload 2
iconst_1
iadd
istore 2
goto L8
L11:
aload 6
invokevirtual android/view/View/getLeft()I
aload 7
getfield android/support/v7/widget/al/leftMargin I
isub
aload 0
getfield android/support/v7/widget/aj/w I
isub
istore 3
goto L12
L9:
aload 0
iload 4
invokespecial android/support/v7/widget/aj/b(I)Z
ifeq L1
aload 0
iload 4
iconst_1
isub
invokevirtual android/support/v7/widget/aj/getChildAt(I)Landroid/view/View;
astore 6
aload 6
ifnonnull L13
iload 5
ifeq L14
aload 0
invokevirtual android/support/v7/widget/aj/getPaddingLeft()I
istore 2
L15:
aload 0
aload 1
iload 2
invokespecial android/support/v7/widget/aj/b(Landroid/graphics/Canvas;I)V
return
L14:
aload 0
invokevirtual android/support/v7/widget/aj/getWidth()I
aload 0
invokevirtual android/support/v7/widget/aj/getPaddingRight()I
isub
aload 0
getfield android/support/v7/widget/aj/w I
isub
istore 2
goto L15
L13:
aload 6
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v7/widget/al
astore 7
iload 5
ifeq L16
aload 6
invokevirtual android/view/View/getLeft()I
aload 7
getfield android/support/v7/widget/al/leftMargin I
isub
aload 0
getfield android/support/v7/widget/aj/w I
isub
istore 2
goto L15
L16:
aload 6
invokevirtual android/view/View/getRight()I
istore 2
aload 7
getfield android/support/v7/widget/al/rightMargin I
iload 2
iadd
istore 2
goto L15
.limit locals 8
.limit stack 4
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
getstatic android/os/Build$VERSION/SDK_INT I
bipush 14
if_icmplt L0
aload 0
aload 1
invokespecial android/view/ViewGroup/onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
aload 1
ldc android/support/v7/widget/aj
invokevirtual java/lang/Class/getName()Ljava/lang/String;
invokevirtual android/view/accessibility/AccessibilityEvent/setClassName(Ljava/lang/CharSequence;)V
L0:
return
.limit locals 2
.limit stack 2
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
getstatic android/os/Build$VERSION/SDK_INT I
bipush 14
if_icmplt L0
aload 0
aload 1
invokespecial android/view/ViewGroup/onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
aload 1
ldc android/support/v7/widget/aj
invokevirtual java/lang/Class/getName()Ljava/lang/String;
invokevirtual android/view/accessibility/AccessibilityNodeInfo/setClassName(Ljava/lang/CharSequence;)V
L0:
return
.limit locals 2
.limit stack 2
.end method

.method protected onLayout(ZIIII)V
aload 0
getfield android/support/v7/widget/aj/d I
iconst_1
if_icmpne L0
aload 0
invokevirtual android/support/v7/widget/aj/getPaddingLeft()I
istore 6
iload 4
iload 2
isub
istore 7
aload 0
invokevirtual android/support/v7/widget/aj/getPaddingRight()I
istore 8
aload 0
invokevirtual android/support/v7/widget/aj/getPaddingRight()I
istore 9
aload 0
invokevirtual android/support/v7/widget/aj/getVirtualChildCount()I
istore 10
aload 0
getfield android/support/v7/widget/aj/e I
istore 2
aload 0
getfield android/support/v7/widget/aj/e I
istore 11
iload 2
bipush 112
iand
lookupswitch
16 : L1
80 : L2
default : L3
L3:
aload 0
invokevirtual android/support/v7/widget/aj/getPaddingTop()I
istore 2
L4:
iconst_0
istore 4
iload 2
istore 3
iload 4
istore 2
L5:
iload 2
iload 10
if_icmpge L6
aload 0
iload 2
invokevirtual android/support/v7/widget/aj/getChildAt(I)Landroid/view/View;
astore 19
aload 19
ifnonnull L7
iload 3
iconst_0
iadd
istore 3
L8:
iload 2
iconst_1
iadd
istore 2
goto L5
L2:
aload 0
invokevirtual android/support/v7/widget/aj/getPaddingTop()I
iload 5
iadd
iload 3
isub
aload 0
getfield android/support/v7/widget/aj/f I
isub
istore 2
goto L4
L1:
aload 0
invokevirtual android/support/v7/widget/aj/getPaddingTop()I
iload 5
iload 3
isub
aload 0
getfield android/support/v7/widget/aj/f I
isub
iconst_2
idiv
iadd
istore 2
goto L4
L7:
aload 19
invokevirtual android/view/View/getVisibility()I
bipush 8
if_icmpeq L9
aload 19
invokevirtual android/view/View/getMeasuredWidth()I
istore 12
aload 19
invokevirtual android/view/View/getMeasuredHeight()I
istore 13
aload 19
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v7/widget/al
astore 20
aload 20
getfield android/support/v7/widget/al/h I
istore 5
iload 5
istore 4
iload 5
ifge L10
ldc_w 8388615
iload 11
iand
istore 4
L10:
iload 4
aload 0
invokestatic android/support/v4/view/cx/f(Landroid/view/View;)I
invokestatic android/support/v4/view/v/a(II)I
bipush 7
iand
lookupswitch
1 : L11
5 : L12
default : L13
L13:
aload 20
getfield android/support/v7/widget/al/leftMargin I
iload 6
iadd
istore 4
L14:
iload 3
istore 5
aload 0
iload 2
invokespecial android/support/v7/widget/aj/b(I)Z
ifeq L15
iload 3
aload 0
getfield android/support/v7/widget/aj/x I
iadd
istore 5
L15:
iload 5
aload 20
getfield android/support/v7/widget/al/topMargin I
iadd
istore 3
aload 19
iload 4
iload 3
iconst_0
iadd
iload 12
iload 13
invokestatic android/support/v7/widget/aj/b(Landroid/view/View;IIII)V
iload 3
aload 20
getfield android/support/v7/widget/al/bottomMargin I
iload 13
iadd
iconst_0
iadd
iadd
istore 3
iload 2
iconst_0
iadd
istore 2
goto L8
L11:
iload 7
iload 6
isub
iload 9
isub
iload 12
isub
iconst_2
idiv
iload 6
iadd
aload 20
getfield android/support/v7/widget/al/leftMargin I
iadd
aload 20
getfield android/support/v7/widget/al/rightMargin I
isub
istore 4
goto L14
L12:
iload 7
iload 8
isub
iload 12
isub
aload 20
getfield android/support/v7/widget/al/rightMargin I
isub
istore 4
goto L14
L0:
aload 0
invokestatic android/support/v7/internal/widget/bd/a(Landroid/view/View;)Z
istore 1
aload 0
invokevirtual android/support/v7/widget/aj/getPaddingTop()I
istore 8
iload 5
iload 3
isub
istore 10
aload 0
invokevirtual android/support/v7/widget/aj/getPaddingBottom()I
istore 11
aload 0
invokevirtual android/support/v7/widget/aj/getPaddingBottom()I
istore 12
aload 0
invokevirtual android/support/v7/widget/aj/getVirtualChildCount()I
istore 13
aload 0
getfield android/support/v7/widget/aj/e I
istore 3
aload 0
getfield android/support/v7/widget/aj/e I
istore 14
aload 0
getfield android/support/v7/widget/aj/a Z
istore 18
aload 0
getfield android/support/v7/widget/aj/o [I
astore 19
aload 0
getfield android/support/v7/widget/aj/p [I
astore 20
iload 3
ldc_w 8388615
iand
aload 0
invokestatic android/support/v4/view/cx/f(Landroid/view/View;)I
invokestatic android/support/v4/view/v/a(II)I
lookupswitch
1 : L16
5 : L17
default : L18
L18:
aload 0
invokevirtual android/support/v7/widget/aj/getPaddingLeft()I
istore 2
L19:
iload 1
ifeq L20
iload 13
iconst_1
isub
istore 6
iconst_m1
istore 5
L21:
iconst_0
istore 3
iload 2
istore 4
L22:
iload 3
iload 13
if_icmpge L6
iload 6
iload 5
iload 3
imul
iadd
istore 17
aload 0
iload 17
invokevirtual android/support/v7/widget/aj/getChildAt(I)Landroid/view/View;
astore 21
aload 21
ifnonnull L23
iload 4
iconst_0
iadd
istore 4
iload 3
istore 2
L24:
iload 2
iconst_1
iadd
istore 3
goto L22
L17:
aload 0
invokevirtual android/support/v7/widget/aj/getPaddingLeft()I
iload 4
iadd
iload 2
isub
aload 0
getfield android/support/v7/widget/aj/f I
isub
istore 2
goto L19
L16:
aload 0
invokevirtual android/support/v7/widget/aj/getPaddingLeft()I
iload 4
iload 2
isub
aload 0
getfield android/support/v7/widget/aj/f I
isub
iconst_2
idiv
iadd
istore 2
goto L19
L23:
aload 21
invokevirtual android/view/View/getVisibility()I
bipush 8
if_icmpeq L25
aload 21
invokevirtual android/view/View/getMeasuredWidth()I
istore 15
aload 21
invokevirtual android/view/View/getMeasuredHeight()I
istore 16
iconst_m1
istore 2
aload 21
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v7/widget/al
astore 22
iload 2
istore 7
iload 18
ifeq L26
iload 2
istore 7
aload 22
getfield android/support/v7/widget/al/height I
iconst_m1
if_icmpeq L26
aload 21
invokevirtual android/view/View/getBaseline()I
istore 7
L26:
aload 22
getfield android/support/v7/widget/al/h I
istore 9
iload 9
istore 2
iload 9
ifge L27
iload 14
bipush 112
iand
istore 2
L27:
iload 2
bipush 112
iand
lookupswitch
16 : L28
48 : L29
80 : L30
default : L31
L31:
iload 8
istore 2
L32:
aload 0
iload 17
invokespecial android/support/v7/widget/aj/b(I)Z
ifeq L33
aload 0
getfield android/support/v7/widget/aj/w I
iload 4
iadd
istore 4
L34:
iload 4
aload 22
getfield android/support/v7/widget/al/leftMargin I
iadd
istore 4
aload 21
iload 4
iconst_0
iadd
iload 2
iload 15
iload 16
invokestatic android/support/v7/widget/aj/b(Landroid/view/View;IIII)V
iload 4
aload 22
getfield android/support/v7/widget/al/rightMargin I
iload 15
iadd
iconst_0
iadd
iadd
istore 4
iload 3
iconst_0
iadd
istore 2
goto L24
L29:
aload 22
getfield android/support/v7/widget/al/topMargin I
iload 8
iadd
istore 9
iload 9
istore 2
iload 7
iconst_m1
if_icmpeq L35
aload 19
iconst_1
iaload
iload 7
isub
iload 9
iadd
istore 2
goto L32
L28:
iload 10
iload 8
isub
iload 12
isub
iload 16
isub
iconst_2
idiv
iload 8
iadd
aload 22
getfield android/support/v7/widget/al/topMargin I
iadd
aload 22
getfield android/support/v7/widget/al/bottomMargin I
isub
istore 2
goto L32
L30:
iload 10
iload 11
isub
iload 16
isub
aload 22
getfield android/support/v7/widget/al/bottomMargin I
isub
istore 9
iload 9
istore 2
iload 7
iconst_m1
if_icmpeq L35
aload 21
invokevirtual android/view/View/getMeasuredHeight()I
istore 2
iload 9
aload 20
iconst_2
iaload
iload 2
iload 7
isub
isub
isub
istore 2
goto L32
L6:
return
L33:
goto L34
L35:
goto L32
L25:
iload 3
istore 2
goto L24
L20:
iconst_0
istore 6
iconst_1
istore 5
goto L21
L9:
goto L8
.limit locals 23
.limit stack 5
.end method

.method public onMeasure(II)V
aload 0
getfield android/support/v7/widget/aj/d I
iconst_1
if_icmpne L0
aload 0
iconst_0
putfield android/support/v7/widget/aj/f I
iconst_0
istore 7
iconst_0
istore 5
iconst_0
istore 6
iconst_0
istore 12
iconst_1
istore 8
fconst_0
fstore 3
aload 0
invokevirtual android/support/v7/widget/aj/getVirtualChildCount()I
istore 18
iload 1
invokestatic android/view/View$MeasureSpec/getMode(I)I
istore 19
iload 2
invokestatic android/view/View$MeasureSpec/getMode(I)I
istore 20
iconst_0
istore 11
iconst_0
istore 10
aload 0
getfield android/support/v7/widget/aj/b I
istore 21
aload 0
getfield android/support/v7/widget/aj/n Z
istore 22
ldc_w -2147483648
istore 9
iconst_0
istore 13
L1:
iload 13
iload 18
if_icmpge L2
aload 0
iload 13
invokevirtual android/support/v7/widget/aj/getChildAt(I)Landroid/view/View;
astore 23
aload 23
ifnonnull L3
aload 0
aload 0
getfield android/support/v7/widget/aj/f I
iconst_0
iadd
putfield android/support/v7/widget/aj/f I
iload 13
istore 17
iload 7
istore 16
iload 5
istore 15
iload 6
istore 14
iload 12
istore 13
iload 8
istore 7
iload 10
istore 6
iload 9
istore 5
L4:
iload 17
iconst_1
iadd
istore 17
iload 5
istore 9
iload 6
istore 10
iload 7
istore 8
iload 13
istore 12
iload 14
istore 6
iload 15
istore 5
iload 16
istore 7
iload 17
istore 13
goto L1
L3:
aload 23
invokevirtual android/view/View/getVisibility()I
bipush 8
if_icmpeq L5
aload 0
iload 13
invokespecial android/support/v7/widget/aj/b(I)Z
ifeq L6
aload 0
aload 0
getfield android/support/v7/widget/aj/f I
aload 0
getfield android/support/v7/widget/aj/x I
iadd
putfield android/support/v7/widget/aj/f I
L6:
aload 23
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v7/widget/al
astore 24
fload 3
aload 24
getfield android/support/v7/widget/al/g F
fadd
fstore 3
iload 20
ldc_w 1073741824
if_icmpne L7
aload 24
getfield android/support/v7/widget/al/height I
ifne L7
aload 24
getfield android/support/v7/widget/al/g F
fconst_0
fcmpl
ifle L7
aload 0
getfield android/support/v7/widget/aj/f I
istore 10
aload 0
iload 10
aload 24
getfield android/support/v7/widget/al/topMargin I
iload 10
iadd
aload 24
getfield android/support/v7/widget/al/bottomMargin I
iadd
invokestatic java/lang/Math/max(II)I
putfield android/support/v7/widget/aj/f I
iconst_1
istore 10
L8:
iload 21
iflt L9
iload 21
iload 13
iconst_1
iadd
if_icmpne L9
aload 0
aload 0
getfield android/support/v7/widget/aj/f I
putfield android/support/v7/widget/aj/c I
L9:
iload 13
iload 21
if_icmpge L10
aload 24
getfield android/support/v7/widget/al/g F
fconst_0
fcmpl
ifle L10
new java/lang/RuntimeException
dup
ldc "A child of LinearLayout with index less than mBaselineAlignedChildIndex has weight > 0, which won't work.  Either remove the weight, or don't set mBaselineAlignedChildIndex."
invokespecial java/lang/RuntimeException/<init>(Ljava/lang/String;)V
athrow
L7:
ldc_w -2147483648
istore 15
iload 15
istore 14
aload 24
getfield android/support/v7/widget/al/height I
ifne L11
iload 15
istore 14
aload 24
getfield android/support/v7/widget/al/g F
fconst_0
fcmpl
ifle L11
iconst_0
istore 14
aload 24
bipush -2
putfield android/support/v7/widget/al/height I
L11:
fload 3
fconst_0
fcmpl
ifne L12
aload 0
getfield android/support/v7/widget/aj/f I
istore 15
L13:
aload 0
aload 23
iload 1
iconst_0
iload 2
iload 15
invokespecial android/support/v7/widget/aj/a(Landroid/view/View;IIII)V
iload 14
ldc_w -2147483648
if_icmpeq L14
aload 24
iload 14
putfield android/support/v7/widget/al/height I
L14:
aload 23
invokevirtual android/view/View/getMeasuredHeight()I
istore 14
aload 0
getfield android/support/v7/widget/aj/f I
istore 15
aload 0
iload 15
iload 15
iload 14
iadd
aload 24
getfield android/support/v7/widget/al/topMargin I
iadd
aload 24
getfield android/support/v7/widget/al/bottomMargin I
iadd
iconst_0
iadd
invokestatic java/lang/Math/max(II)I
putfield android/support/v7/widget/aj/f I
iload 22
ifeq L15
iload 14
iload 9
invokestatic java/lang/Math/max(II)I
istore 9
goto L8
L12:
iconst_0
istore 15
goto L13
L10:
iconst_0
istore 14
iload 19
ldc_w 1073741824
if_icmpeq L16
aload 24
getfield android/support/v7/widget/al/width I
iconst_m1
if_icmpne L16
iconst_1
istore 11
iconst_1
istore 14
L17:
aload 24
getfield android/support/v7/widget/al/leftMargin I
aload 24
getfield android/support/v7/widget/al/rightMargin I
iadd
istore 15
aload 23
invokevirtual android/view/View/getMeasuredWidth()I
iload 15
iadd
istore 16
iload 7
iload 16
invokestatic java/lang/Math/max(II)I
istore 7
iload 5
aload 23
invokestatic android/support/v4/view/cx/j(Landroid/view/View;)I
invokestatic android/support/v7/internal/widget/bd/a(II)I
istore 17
iload 8
ifeq L18
aload 24
getfield android/support/v7/widget/al/width I
iconst_m1
if_icmpne L18
iconst_1
istore 5
L19:
aload 24
getfield android/support/v7/widget/al/g F
fconst_0
fcmpl
ifle L20
iload 14
ifeq L21
L22:
iload 12
iload 15
invokestatic java/lang/Math/max(II)I
istore 12
iload 6
istore 8
iload 10
istore 6
iload 7
istore 10
iload 12
istore 7
iload 9
istore 14
iload 17
istore 12
iload 8
istore 9
iload 7
istore 8
iload 5
istore 7
iload 6
istore 5
iload 14
istore 6
L23:
iload 13
iconst_0
iadd
istore 17
iload 5
istore 13
iload 6
istore 5
iload 13
istore 6
iload 8
istore 13
iload 9
istore 14
iload 12
istore 15
iload 10
istore 16
goto L4
L18:
iconst_0
istore 5
goto L19
L21:
iload 16
istore 15
goto L22
L20:
iload 14
ifeq L24
L25:
iload 6
iload 15
invokestatic java/lang/Math/max(II)I
istore 14
iload 5
istore 8
iload 10
istore 5
iload 9
istore 6
iload 7
istore 10
iload 8
istore 7
iload 12
istore 8
iload 14
istore 9
iload 17
istore 12
goto L23
L24:
iload 16
istore 15
goto L25
L2:
aload 0
getfield android/support/v7/widget/aj/f I
ifle L26
aload 0
iload 18
invokespecial android/support/v7/widget/aj/b(I)Z
ifeq L26
aload 0
aload 0
getfield android/support/v7/widget/aj/f I
aload 0
getfield android/support/v7/widget/aj/x I
iadd
putfield android/support/v7/widget/aj/f I
L26:
iload 22
ifeq L27
iload 20
ldc_w -2147483648
if_icmpeq L28
iload 20
ifne L27
L28:
aload 0
iconst_0
putfield android/support/v7/widget/aj/f I
iconst_0
istore 13
L29:
iload 13
iload 18
if_icmpge L27
aload 0
iload 13
invokevirtual android/support/v7/widget/aj/getChildAt(I)Landroid/view/View;
astore 23
aload 23
ifnonnull L30
aload 0
aload 0
getfield android/support/v7/widget/aj/f I
iconst_0
iadd
putfield android/support/v7/widget/aj/f I
L31:
iload 13
iconst_1
iadd
istore 13
goto L29
L30:
aload 23
invokevirtual android/view/View/getVisibility()I
bipush 8
if_icmpne L32
iload 13
iconst_0
iadd
istore 13
goto L31
L32:
aload 23
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v7/widget/al
astore 23
aload 0
getfield android/support/v7/widget/aj/f I
istore 14
aload 23
getfield android/support/v7/widget/al/topMargin I
istore 15
aload 0
iload 14
aload 23
getfield android/support/v7/widget/al/bottomMargin I
iload 14
iload 9
iadd
iload 15
iadd
iadd
iconst_0
iadd
invokestatic java/lang/Math/max(II)I
putfield android/support/v7/widget/aj/f I
goto L31
L27:
aload 0
aload 0
getfield android/support/v7/widget/aj/f I
aload 0
invokevirtual android/support/v7/widget/aj/getPaddingTop()I
aload 0
invokevirtual android/support/v7/widget/aj/getPaddingBottom()I
iadd
iadd
putfield android/support/v7/widget/aj/f I
aload 0
getfield android/support/v7/widget/aj/f I
aload 0
invokevirtual android/support/v7/widget/aj/getSuggestedMinimumHeight()I
invokestatic java/lang/Math/max(II)I
iload 2
iconst_0
invokestatic android/support/v4/view/cx/a(III)I
istore 15
ldc_w 16777215
iload 15
iand
aload 0
getfield android/support/v7/widget/aj/f I
isub
istore 13
iload 10
ifne L33
iload 13
ifeq L34
fload 3
fconst_0
fcmpl
ifle L34
L33:
aload 0
getfield android/support/v7/widget/aj/g F
fconst_0
fcmpl
ifle L35
aload 0
getfield android/support/v7/widget/aj/g F
fstore 3
L35:
aload 0
iconst_0
putfield android/support/v7/widget/aj/f I
iconst_0
istore 12
iload 8
istore 9
iload 7
istore 8
iload 6
istore 7
iload 9
istore 6
iload 13
istore 9
L36:
iload 12
iload 18
if_icmpge L37
aload 0
iload 12
invokevirtual android/support/v7/widget/aj/getChildAt(I)Landroid/view/View;
astore 23
aload 23
invokevirtual android/view/View/getVisibility()I
bipush 8
if_icmpeq L38
aload 23
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v7/widget/al
astore 24
aload 24
getfield android/support/v7/widget/al/g F
fstore 4
fload 4
fconst_0
fcmpl
ifle L39
iload 9
i2f
fload 4
fmul
fload 3
fdiv
f2i
istore 13
iload 1
aload 0
invokevirtual android/support/v7/widget/aj/getPaddingLeft()I
aload 0
invokevirtual android/support/v7/widget/aj/getPaddingRight()I
iadd
aload 24
getfield android/support/v7/widget/al/leftMargin I
iadd
aload 24
getfield android/support/v7/widget/al/rightMargin I
iadd
aload 24
getfield android/support/v7/widget/al/width I
invokestatic android/support/v7/widget/aj/getChildMeasureSpec(III)I
istore 16
aload 24
getfield android/support/v7/widget/al/height I
ifne L40
iload 20
ldc_w 1073741824
if_icmpeq L41
L40:
iload 13
aload 23
invokevirtual android/view/View/getMeasuredHeight()I
iadd
istore 14
iload 14
istore 10
iload 14
ifge L42
iconst_0
istore 10
L42:
aload 23
iload 16
iload 10
ldc_w 1073741824
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
invokevirtual android/view/View/measure(II)V
iload 5
aload 23
invokestatic android/support/v4/view/cx/j(Landroid/view/View;)I
sipush -256
iand
invokestatic android/support/v7/internal/widget/bd/a(II)I
istore 5
iload 9
iload 13
isub
istore 10
iload 5
istore 9
fload 3
fload 4
fsub
fstore 3
iload 10
istore 5
L43:
aload 24
getfield android/support/v7/widget/al/leftMargin I
aload 24
getfield android/support/v7/widget/al/rightMargin I
iadd
istore 13
aload 23
invokevirtual android/view/View/getMeasuredWidth()I
iload 13
iadd
istore 14
iload 8
iload 14
invokestatic java/lang/Math/max(II)I
istore 10
iload 19
ldc_w 1073741824
if_icmpeq L44
aload 24
getfield android/support/v7/widget/al/width I
iconst_m1
if_icmpne L44
iconst_1
istore 8
L45:
iload 8
ifeq L46
iload 13
istore 8
L47:
iload 7
iload 8
invokestatic java/lang/Math/max(II)I
istore 7
iload 6
ifeq L48
aload 24
getfield android/support/v7/widget/al/width I
iconst_m1
if_icmpne L48
iconst_1
istore 6
L49:
aload 0
getfield android/support/v7/widget/aj/f I
istore 8
aload 23
invokevirtual android/view/View/getMeasuredHeight()I
istore 13
aload 24
getfield android/support/v7/widget/al/topMargin I
istore 14
aload 0
iload 8
aload 24
getfield android/support/v7/widget/al/bottomMargin I
iload 13
iload 8
iadd
iload 14
iadd
iadd
iconst_0
iadd
invokestatic java/lang/Math/max(II)I
putfield android/support/v7/widget/aj/f I
iload 6
istore 8
iload 10
istore 6
L50:
iload 12
iconst_1
iadd
istore 13
iload 9
istore 12
iload 6
istore 10
iload 5
istore 9
iload 12
istore 5
iload 8
istore 6
iload 10
istore 8
iload 13
istore 12
goto L36
L41:
iload 13
ifle L51
iload 13
istore 10
goto L42
L51:
iconst_0
istore 10
goto L42
L44:
iconst_0
istore 8
goto L45
L46:
iload 14
istore 8
goto L47
L48:
iconst_0
istore 6
goto L49
L37:
aload 0
aload 0
getfield android/support/v7/widget/aj/f I
aload 0
invokevirtual android/support/v7/widget/aj/getPaddingTop()I
aload 0
invokevirtual android/support/v7/widget/aj/getPaddingBottom()I
iadd
iadd
putfield android/support/v7/widget/aj/f I
iload 7
istore 9
iload 8
istore 7
iload 6
istore 8
iload 9
istore 6
L52:
iload 8
ifne L53
iload 19
ldc_w 1073741824
if_icmpeq L53
L54:
aload 0
iload 6
aload 0
invokevirtual android/support/v7/widget/aj/getPaddingLeft()I
aload 0
invokevirtual android/support/v7/widget/aj/getPaddingRight()I
iadd
iadd
aload 0
invokevirtual android/support/v7/widget/aj/getSuggestedMinimumWidth()I
invokestatic java/lang/Math/max(II)I
iload 1
iload 5
invokestatic android/support/v4/view/cx/a(III)I
iload 15
invokevirtual android/support/v7/widget/aj/setMeasuredDimension(II)V
iload 11
ifeq L55
aload 0
iload 18
iload 2
invokespecial android/support/v7/widget/aj/b(II)V
L55:
return
L34:
iload 6
iload 12
invokestatic java/lang/Math/max(II)I
istore 10
iload 22
ifeq L56
iload 20
ldc_w 1073741824
if_icmpeq L56
iconst_0
istore 6
L57:
iload 6
iload 18
if_icmpge L56
aload 0
iload 6
invokevirtual android/support/v7/widget/aj/getChildAt(I)Landroid/view/View;
astore 23
aload 23
ifnull L58
aload 23
invokevirtual android/view/View/getVisibility()I
bipush 8
if_icmpeq L58
aload 23
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v7/widget/al
getfield android/support/v7/widget/al/g F
fconst_0
fcmpl
ifle L58
aload 23
aload 23
invokevirtual android/view/View/getMeasuredWidth()I
ldc_w 1073741824
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
iload 9
ldc_w 1073741824
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
invokevirtual android/view/View/measure(II)V
L58:
iload 6
iconst_1
iadd
istore 6
goto L57
L0:
aload 0
iload 1
iload 2
invokespecial android/support/v7/widget/aj/c(II)V
return
L53:
iload 7
istore 6
goto L54
L56:
iload 10
istore 6
goto L52
L39:
iload 5
istore 10
iload 9
istore 5
iload 10
istore 9
goto L43
L38:
iload 6
istore 10
iload 8
istore 6
iload 5
istore 8
iload 9
istore 5
iload 8
istore 9
iload 10
istore 8
goto L50
L16:
goto L17
L15:
goto L8
L5:
iload 9
istore 14
iload 10
istore 15
iload 6
istore 9
iload 7
istore 10
iload 5
istore 16
iload 14
istore 6
iload 15
istore 5
iload 8
istore 7
iload 12
istore 8
iload 16
istore 12
goto L23
.limit locals 25
.limit stack 6
.end method

.method public setBaselineAligned(Z)V
aload 0
iload 1
putfield android/support/v7/widget/aj/a Z
return
.limit locals 2
.limit stack 2
.end method

.method public setBaselineAlignedChildIndex(I)V
iload 1
iflt L0
iload 1
aload 0
invokevirtual android/support/v7/widget/aj/getChildCount()I
if_icmplt L1
L0:
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
ldc "base aligned child index out of range (0, "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
invokevirtual android/support/v7/widget/aj/getChildCount()I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc ")"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L1:
aload 0
iload 1
putfield android/support/v7/widget/aj/b I
return
.limit locals 2
.limit stack 5
.end method

.method public setDividerDrawable(Landroid/graphics/drawable/Drawable;)V
iconst_0
istore 2
aload 1
aload 0
getfield android/support/v7/widget/aj/v Landroid/graphics/drawable/Drawable;
if_acmpne L0
return
L0:
aload 0
aload 1
putfield android/support/v7/widget/aj/v Landroid/graphics/drawable/Drawable;
aload 1
ifnull L1
aload 0
aload 1
invokevirtual android/graphics/drawable/Drawable/getIntrinsicWidth()I
putfield android/support/v7/widget/aj/w I
aload 0
aload 1
invokevirtual android/graphics/drawable/Drawable/getIntrinsicHeight()I
putfield android/support/v7/widget/aj/x I
L2:
aload 1
ifnonnull L3
iconst_1
istore 2
L3:
aload 0
iload 2
invokevirtual android/support/v7/widget/aj/setWillNotDraw(Z)V
aload 0
invokevirtual android/support/v7/widget/aj/requestLayout()V
return
L1:
aload 0
iconst_0
putfield android/support/v7/widget/aj/w I
aload 0
iconst_0
putfield android/support/v7/widget/aj/x I
goto L2
.limit locals 3
.limit stack 2
.end method

.method public setDividerPadding(I)V
aload 0
iload 1
putfield android/support/v7/widget/aj/z I
return
.limit locals 2
.limit stack 2
.end method

.method public setGravity(I)V
aload 0
getfield android/support/v7/widget/aj/e I
iload 1
if_icmpeq L0
ldc_w 8388615
iload 1
iand
ifne L1
ldc_w 8388611
iload 1
ior
istore 1
L2:
iload 1
istore 2
iload 1
bipush 112
iand
ifne L3
iload 1
bipush 48
ior
istore 2
L3:
aload 0
iload 2
putfield android/support/v7/widget/aj/e I
aload 0
invokevirtual android/support/v7/widget/aj/requestLayout()V
L0:
return
L1:
goto L2
.limit locals 3
.limit stack 2
.end method

.method public setHorizontalGravity(I)V
iload 1
ldc_w 8388615
iand
istore 1
aload 0
getfield android/support/v7/widget/aj/e I
ldc_w 8388615
iand
iload 1
if_icmpeq L0
aload 0
iload 1
aload 0
getfield android/support/v7/widget/aj/e I
ldc_w -8388616
iand
ior
putfield android/support/v7/widget/aj/e I
aload 0
invokevirtual android/support/v7/widget/aj/requestLayout()V
L0:
return
.limit locals 2
.limit stack 4
.end method

.method public setMeasureWithLargestChildEnabled(Z)V
aload 0
iload 1
putfield android/support/v7/widget/aj/n Z
return
.limit locals 2
.limit stack 2
.end method

.method public setOrientation(I)V
aload 0
getfield android/support/v7/widget/aj/d I
iload 1
if_icmpeq L0
aload 0
iload 1
putfield android/support/v7/widget/aj/d I
aload 0
invokevirtual android/support/v7/widget/aj/requestLayout()V
L0:
return
.limit locals 2
.limit stack 2
.end method

.method public setShowDividers(I)V
iload 1
aload 0
getfield android/support/v7/widget/aj/y I
if_icmpeq L0
aload 0
invokevirtual android/support/v7/widget/aj/requestLayout()V
L0:
aload 0
iload 1
putfield android/support/v7/widget/aj/y I
return
.limit locals 2
.limit stack 2
.end method

.method public setVerticalGravity(I)V
iload 1
bipush 112
iand
istore 1
aload 0
getfield android/support/v7/widget/aj/e I
bipush 112
iand
iload 1
if_icmpeq L0
aload 0
iload 1
aload 0
getfield android/support/v7/widget/aj/e I
bipush -113
iand
ior
putfield android/support/v7/widget/aj/e I
aload 0
invokevirtual android/support/v7/widget/aj/requestLayout()V
L0:
return
.limit locals 2
.limit stack 4
.end method

.method public setWeightSum(F)V
aload 0
fconst_0
fload 1
invokestatic java/lang/Math/max(FF)F
putfield android/support/v7/widget/aj/g F
return
.limit locals 2
.limit stack 3
.end method

.method public shouldDelayChildPressedState()Z
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method
