.bytecode 50.0
.class final synchronized android/support/v7/internal/a/k
.super android/support/v7/internal/view/k

.field final synthetic 'a' Landroid/support/v7/internal/a/e;

.method public <init>(Landroid/support/v7/internal/a/e;Landroid/view/Window$Callback;)V
aload 0
aload 1
putfield android/support/v7/internal/a/k/a Landroid/support/v7/internal/a/e;
aload 0
aload 2
invokespecial android/support/v7/internal/view/k/<init>(Landroid/view/Window$Callback;)V
return
.limit locals 3
.limit stack 2
.end method

.method public final onCreatePanelView(I)Landroid/view/View;
iload 1
tableswitch 0
L0
default : L1
L1:
aload 0
iload 1
invokespecial android/support/v7/internal/view/k/onCreatePanelView(I)Landroid/view/View;
areturn
L0:
aload 0
getfield android/support/v7/internal/a/k/a Landroid/support/v7/internal/a/e;
getfield android/support/v7/internal/a/e/i Landroid/support/v7/internal/widget/ad;
invokeinterface android/support/v7/internal/widget/ad/B()Landroid/view/Menu; 0
astore 2
aload 0
iload 1
aconst_null
aload 2
invokevirtual android/support/v7/internal/a/k/onPreparePanel(ILandroid/view/View;Landroid/view/Menu;)Z
ifeq L1
aload 0
iload 1
aload 2
invokevirtual android/support/v7/internal/a/k/onMenuOpened(ILandroid/view/Menu;)Z
ifeq L1
aload 0
getfield android/support/v7/internal/a/k/a Landroid/support/v7/internal/a/e;
astore 3
aload 3
getfield android/support/v7/internal/a/e/l Landroid/support/v7/internal/view/menu/g;
ifnonnull L2
aload 2
instanceof android/support/v7/internal/view/menu/i
ifeq L2
aload 2
checkcast android/support/v7/internal/view/menu/i
astore 4
aload 3
getfield android/support/v7/internal/a/e/i Landroid/support/v7/internal/widget/ad;
invokeinterface android/support/v7/internal/widget/ad/b()Landroid/content/Context; 0
astore 6
new android/util/TypedValue
dup
invokespecial android/util/TypedValue/<init>()V
astore 7
aload 6
invokevirtual android/content/Context/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/newTheme()Landroid/content/res/Resources$Theme;
astore 5
aload 5
aload 6
invokevirtual android/content/Context/getTheme()Landroid/content/res/Resources$Theme;
invokevirtual android/content/res/Resources$Theme/setTo(Landroid/content/res/Resources$Theme;)V
aload 5
getstatic android/support/v7/a/d/actionBarPopupTheme I
aload 7
iconst_1
invokevirtual android/content/res/Resources$Theme/resolveAttribute(ILandroid/util/TypedValue;Z)Z
pop
aload 7
getfield android/util/TypedValue/resourceId I
ifeq L3
aload 5
aload 7
getfield android/util/TypedValue/resourceId I
iconst_1
invokevirtual android/content/res/Resources$Theme/applyStyle(IZ)V
L3:
aload 5
getstatic android/support/v7/a/d/panelMenuListTheme I
aload 7
iconst_1
invokevirtual android/content/res/Resources$Theme/resolveAttribute(ILandroid/util/TypedValue;Z)Z
pop
aload 7
getfield android/util/TypedValue/resourceId I
ifeq L4
aload 5
aload 7
getfield android/util/TypedValue/resourceId I
iconst_1
invokevirtual android/content/res/Resources$Theme/applyStyle(IZ)V
L5:
new android/view/ContextThemeWrapper
dup
aload 6
iconst_0
invokespecial android/view/ContextThemeWrapper/<init>(Landroid/content/Context;I)V
astore 6
aload 6
invokevirtual android/content/Context/getTheme()Landroid/content/res/Resources$Theme;
aload 5
invokevirtual android/content/res/Resources$Theme/setTo(Landroid/content/res/Resources$Theme;)V
aload 3
new android/support/v7/internal/view/menu/g
dup
aload 6
getstatic android/support/v7/a/k/abc_list_menu_item_layout I
invokespecial android/support/v7/internal/view/menu/g/<init>(Landroid/content/Context;I)V
putfield android/support/v7/internal/a/e/l Landroid/support/v7/internal/view/menu/g;
aload 3
getfield android/support/v7/internal/a/e/l Landroid/support/v7/internal/view/menu/g;
new android/support/v7/internal/a/j
dup
aload 3
iconst_0
invokespecial android/support/v7/internal/a/j/<init>(Landroid/support/v7/internal/a/e;B)V
putfield android/support/v7/internal/view/menu/g/g Landroid/support/v7/internal/view/menu/y;
aload 4
aload 3
getfield android/support/v7/internal/a/e/l Landroid/support/v7/internal/view/menu/g;
invokevirtual android/support/v7/internal/view/menu/i/a(Landroid/support/v7/internal/view/menu/x;)V
L2:
aload 2
ifnull L6
aload 3
getfield android/support/v7/internal/a/e/l Landroid/support/v7/internal/view/menu/g;
ifnonnull L7
L6:
aconst_null
areturn
L4:
aload 5
getstatic android/support/v7/a/m/Theme_AppCompat_CompactMenu I
iconst_1
invokevirtual android/content/res/Resources$Theme/applyStyle(IZ)V
goto L5
L7:
aload 3
getfield android/support/v7/internal/a/e/l Landroid/support/v7/internal/view/menu/g;
invokevirtual android/support/v7/internal/view/menu/g/d()Landroid/widget/ListAdapter;
invokeinterface android/widget/ListAdapter/getCount()I 0
ifle L8
aload 3
getfield android/support/v7/internal/a/e/l Landroid/support/v7/internal/view/menu/g;
aload 3
getfield android/support/v7/internal/a/e/i Landroid/support/v7/internal/widget/ad;
invokeinterface android/support/v7/internal/widget/ad/a()Landroid/view/ViewGroup; 0
invokevirtual android/support/v7/internal/view/menu/g/a(Landroid/view/ViewGroup;)Landroid/support/v7/internal/view/menu/z;
checkcast android/view/View
areturn
L8:
aconst_null
areturn
.limit locals 8
.limit stack 5
.end method

.method public final onPreparePanel(ILandroid/view/View;Landroid/view/Menu;)Z
aload 0
iload 1
aload 2
aload 3
invokespecial android/support/v7/internal/view/k/onPreparePanel(ILandroid/view/View;Landroid/view/Menu;)Z
istore 4
iload 4
ifeq L0
aload 0
getfield android/support/v7/internal/a/k/a Landroid/support/v7/internal/a/e;
getfield android/support/v7/internal/a/e/j Z
ifne L0
aload 0
getfield android/support/v7/internal/a/k/a Landroid/support/v7/internal/a/e;
getfield android/support/v7/internal/a/e/i Landroid/support/v7/internal/widget/ad;
invokeinterface android/support/v7/internal/widget/ad/p()V 0
aload 0
getfield android/support/v7/internal/a/k/a Landroid/support/v7/internal/a/e;
iconst_1
putfield android/support/v7/internal/a/e/j Z
L0:
iload 4
ireturn
.limit locals 5
.limit stack 4
.end method
