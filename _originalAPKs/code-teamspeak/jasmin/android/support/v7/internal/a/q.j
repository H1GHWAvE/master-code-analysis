.bytecode 50.0
.class public final synchronized android/support/v7/internal/a/q
.super android/support/v7/app/g

.field 'b' Landroid/support/v7/app/h;

.field 'c' I

.field final synthetic 'd' Landroid/support/v7/internal/a/l;

.field private 'e' Ljava/lang/Object;

.field private 'f' Landroid/graphics/drawable/Drawable;

.field private 'g' Ljava/lang/CharSequence;

.field private 'h' Ljava/lang/CharSequence;

.field private 'i' Landroid/view/View;

.method public <init>(Landroid/support/v7/internal/a/l;)V
aload 0
aload 1
putfield android/support/v7/internal/a/q/d Landroid/support/v7/internal/a/l;
aload 0
invokespecial android/support/v7/app/g/<init>()V
aload 0
iconst_m1
putfield android/support/v7/internal/a/q/c I
return
.limit locals 2
.limit stack 2
.end method

.method private e(I)V
aload 0
iload 1
putfield android/support/v7/internal/a/q/c I
return
.limit locals 2
.limit stack 2
.end method

.method private h()Landroid/support/v7/app/h;
aload 0
getfield android/support/v7/internal/a/q/b Landroid/support/v7/app/h;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final a()I
aload 0
getfield android/support/v7/internal/a/q/c I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final a(I)Landroid/support/v7/app/g;
aload 0
getfield android/support/v7/internal/a/q/d Landroid/support/v7/internal/a/l;
astore 2
aload 2
getfield android/support/v7/internal/a/l/n Landroid/support/v7/internal/widget/av;
ifnonnull L0
aload 2
aload 2
getfield android/support/v7/internal/a/l/i Landroid/content/Context;
invokestatic android/support/v7/internal/widget/av/a(Landroid/content/Context;)Landroid/support/v7/internal/widget/av;
putfield android/support/v7/internal/a/l/n Landroid/support/v7/internal/widget/av;
L0:
aload 0
aload 2
getfield android/support/v7/internal/a/l/n Landroid/support/v7/internal/widget/av;
iload 1
iconst_0
invokevirtual android/support/v7/internal/widget/av/a(IZ)Landroid/graphics/drawable/Drawable;
putfield android/support/v7/internal/a/q/f Landroid/graphics/drawable/Drawable;
aload 0
getfield android/support/v7/internal/a/q/c I
iflt L1
aload 0
getfield android/support/v7/internal/a/q/d Landroid/support/v7/internal/a/l;
invokestatic android/support/v7/internal/a/l/k(Landroid/support/v7/internal/a/l;)Landroid/support/v7/internal/widget/al;
aload 0
getfield android/support/v7/internal/a/q/c I
invokevirtual android/support/v7/internal/widget/al/b(I)V
L1:
aload 0
areturn
.limit locals 3
.limit stack 4
.end method

.method public final a(Landroid/graphics/drawable/Drawable;)Landroid/support/v7/app/g;
aload 0
aload 1
putfield android/support/v7/internal/a/q/f Landroid/graphics/drawable/Drawable;
aload 0
getfield android/support/v7/internal/a/q/c I
iflt L0
aload 0
getfield android/support/v7/internal/a/q/d Landroid/support/v7/internal/a/l;
invokestatic android/support/v7/internal/a/l/k(Landroid/support/v7/internal/a/l;)Landroid/support/v7/internal/widget/al;
aload 0
getfield android/support/v7/internal/a/q/c I
invokevirtual android/support/v7/internal/widget/al/b(I)V
L0:
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method public final a(Landroid/support/v7/app/h;)Landroid/support/v7/app/g;
aload 0
aload 1
putfield android/support/v7/internal/a/q/b Landroid/support/v7/app/h;
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method public final a(Landroid/view/View;)Landroid/support/v7/app/g;
aload 0
aload 1
putfield android/support/v7/internal/a/q/i Landroid/view/View;
aload 0
getfield android/support/v7/internal/a/q/c I
iflt L0
aload 0
getfield android/support/v7/internal/a/q/d Landroid/support/v7/internal/a/l;
invokestatic android/support/v7/internal/a/l/k(Landroid/support/v7/internal/a/l;)Landroid/support/v7/internal/widget/al;
aload 0
getfield android/support/v7/internal/a/q/c I
invokevirtual android/support/v7/internal/widget/al/b(I)V
L0:
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method public final a(Ljava/lang/CharSequence;)Landroid/support/v7/app/g;
aload 0
aload 1
putfield android/support/v7/internal/a/q/g Ljava/lang/CharSequence;
aload 0
getfield android/support/v7/internal/a/q/c I
iflt L0
aload 0
getfield android/support/v7/internal/a/q/d Landroid/support/v7/internal/a/l;
invokestatic android/support/v7/internal/a/l/k(Landroid/support/v7/internal/a/l;)Landroid/support/v7/internal/widget/al;
aload 0
getfield android/support/v7/internal/a/q/c I
invokevirtual android/support/v7/internal/widget/al/b(I)V
L0:
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method public final a(Ljava/lang/Object;)Landroid/support/v7/app/g;
aload 0
aload 1
putfield android/support/v7/internal/a/q/e Ljava/lang/Object;
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method public final b()Landroid/graphics/drawable/Drawable;
aload 0
getfield android/support/v7/internal/a/q/f Landroid/graphics/drawable/Drawable;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final b(I)Landroid/support/v7/app/g;
aload 0
aload 0
getfield android/support/v7/internal/a/q/d Landroid/support/v7/internal/a/l;
invokestatic android/support/v7/internal/a/l/j(Landroid/support/v7/internal/a/l;)Landroid/content/Context;
invokevirtual android/content/Context/getResources()Landroid/content/res/Resources;
iload 1
invokevirtual android/content/res/Resources/getText(I)Ljava/lang/CharSequence;
putfield android/support/v7/internal/a/q/g Ljava/lang/CharSequence;
aload 0
getfield android/support/v7/internal/a/q/c I
iflt L0
aload 0
getfield android/support/v7/internal/a/q/d Landroid/support/v7/internal/a/l;
invokestatic android/support/v7/internal/a/l/k(Landroid/support/v7/internal/a/l;)Landroid/support/v7/internal/widget/al;
aload 0
getfield android/support/v7/internal/a/q/c I
invokevirtual android/support/v7/internal/widget/al/b(I)V
L0:
aload 0
areturn
.limit locals 2
.limit stack 3
.end method

.method public final b(Ljava/lang/CharSequence;)Landroid/support/v7/app/g;
aload 0
aload 1
putfield android/support/v7/internal/a/q/h Ljava/lang/CharSequence;
aload 0
getfield android/support/v7/internal/a/q/c I
iflt L0
aload 0
getfield android/support/v7/internal/a/q/d Landroid/support/v7/internal/a/l;
invokestatic android/support/v7/internal/a/l/k(Landroid/support/v7/internal/a/l;)Landroid/support/v7/internal/widget/al;
aload 0
getfield android/support/v7/internal/a/q/c I
invokevirtual android/support/v7/internal/widget/al/b(I)V
L0:
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method public final c(I)Landroid/support/v7/app/g;
aload 0
aload 0
getfield android/support/v7/internal/a/q/d Landroid/support/v7/internal/a/l;
invokevirtual android/support/v7/internal/a/l/r()Landroid/content/Context;
invokestatic android/view/LayoutInflater/from(Landroid/content/Context;)Landroid/view/LayoutInflater;
iload 1
aconst_null
invokevirtual android/view/LayoutInflater/inflate(ILandroid/view/ViewGroup;)Landroid/view/View;
putfield android/support/v7/internal/a/q/i Landroid/view/View;
aload 0
getfield android/support/v7/internal/a/q/c I
iflt L0
aload 0
getfield android/support/v7/internal/a/q/d Landroid/support/v7/internal/a/l;
invokestatic android/support/v7/internal/a/l/k(Landroid/support/v7/internal/a/l;)Landroid/support/v7/internal/widget/al;
aload 0
getfield android/support/v7/internal/a/q/c I
invokevirtual android/support/v7/internal/widget/al/b(I)V
L0:
aload 0
areturn
.limit locals 2
.limit stack 4
.end method

.method public final c()Ljava/lang/CharSequence;
aload 0
getfield android/support/v7/internal/a/q/g Ljava/lang/CharSequence;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final d(I)Landroid/support/v7/app/g;
aload 0
aload 0
getfield android/support/v7/internal/a/q/d Landroid/support/v7/internal/a/l;
invokestatic android/support/v7/internal/a/l/j(Landroid/support/v7/internal/a/l;)Landroid/content/Context;
invokevirtual android/content/Context/getResources()Landroid/content/res/Resources;
iload 1
invokevirtual android/content/res/Resources/getText(I)Ljava/lang/CharSequence;
putfield android/support/v7/internal/a/q/h Ljava/lang/CharSequence;
aload 0
getfield android/support/v7/internal/a/q/c I
iflt L0
aload 0
getfield android/support/v7/internal/a/q/d Landroid/support/v7/internal/a/l;
invokestatic android/support/v7/internal/a/l/k(Landroid/support/v7/internal/a/l;)Landroid/support/v7/internal/widget/al;
aload 0
getfield android/support/v7/internal/a/q/c I
invokevirtual android/support/v7/internal/widget/al/b(I)V
L0:
aload 0
areturn
.limit locals 2
.limit stack 3
.end method

.method public final d()Landroid/view/View;
aload 0
getfield android/support/v7/internal/a/q/i Landroid/view/View;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final e()Ljava/lang/Object;
aload 0
getfield android/support/v7/internal/a/q/e Ljava/lang/Object;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final f()V
aload 0
getfield android/support/v7/internal/a/q/d Landroid/support/v7/internal/a/l;
aload 0
invokevirtual android/support/v7/internal/a/l/c(Landroid/support/v7/app/g;)V
return
.limit locals 1
.limit stack 2
.end method

.method public final g()Ljava/lang/CharSequence;
aload 0
getfield android/support/v7/internal/a/q/h Ljava/lang/CharSequence;
areturn
.limit locals 1
.limit stack 1
.end method
