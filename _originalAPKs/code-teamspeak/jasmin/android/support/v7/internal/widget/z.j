.bytecode 50.0
.class final synchronized android/support/v7/internal/widget/z
.super java/lang/Object
.implements android/view/View$OnClickListener
.implements android/view/View$OnLongClickListener
.implements android/widget/AdapterView$OnItemClickListener
.implements android/widget/PopupWindow$OnDismissListener

.field final synthetic 'a' Landroid/support/v7/internal/widget/ActivityChooserView;

.method private <init>(Landroid/support/v7/internal/widget/ActivityChooserView;)V
aload 0
aload 1
putfield android/support/v7/internal/widget/z/a Landroid/support/v7/internal/widget/ActivityChooserView;
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 2
.limit stack 2
.end method

.method synthetic <init>(Landroid/support/v7/internal/widget/ActivityChooserView;B)V
aload 0
aload 1
invokespecial android/support/v7/internal/widget/z/<init>(Landroid/support/v7/internal/widget/ActivityChooserView;)V
return
.limit locals 3
.limit stack 2
.end method

.method private a()V
aload 0
getfield android/support/v7/internal/widget/z/a Landroid/support/v7/internal/widget/ActivityChooserView;
invokestatic android/support/v7/internal/widget/ActivityChooserView/h(Landroid/support/v7/internal/widget/ActivityChooserView;)Landroid/widget/PopupWindow$OnDismissListener;
ifnull L0
aload 0
getfield android/support/v7/internal/widget/z/a Landroid/support/v7/internal/widget/ActivityChooserView;
invokestatic android/support/v7/internal/widget/ActivityChooserView/h(Landroid/support/v7/internal/widget/ActivityChooserView;)Landroid/widget/PopupWindow$OnDismissListener;
invokeinterface android/widget/PopupWindow$OnDismissListener/onDismiss()V 0
L0:
return
.limit locals 1
.limit stack 1
.end method

.method public final onClick(Landroid/view/View;)V
aload 1
aload 0
getfield android/support/v7/internal/widget/z/a Landroid/support/v7/internal/widget/ActivityChooserView;
invokestatic android/support/v7/internal/widget/ActivityChooserView/e(Landroid/support/v7/internal/widget/ActivityChooserView;)Landroid/widget/FrameLayout;
if_acmpne L0
aload 0
getfield android/support/v7/internal/widget/z/a Landroid/support/v7/internal/widget/ActivityChooserView;
invokevirtual android/support/v7/internal/widget/ActivityChooserView/b()Z
pop
aload 0
getfield android/support/v7/internal/widget/z/a Landroid/support/v7/internal/widget/ActivityChooserView;
invokestatic android/support/v7/internal/widget/ActivityChooserView/a(Landroid/support/v7/internal/widget/ActivityChooserView;)Landroid/support/v7/internal/widget/y;
getfield android/support/v7/internal/widget/y/c Landroid/support/v7/internal/widget/l;
invokevirtual android/support/v7/internal/widget/l/b()Landroid/content/pm/ResolveInfo;
astore 1
aload 0
getfield android/support/v7/internal/widget/z/a Landroid/support/v7/internal/widget/ActivityChooserView;
invokestatic android/support/v7/internal/widget/ActivityChooserView/a(Landroid/support/v7/internal/widget/ActivityChooserView;)Landroid/support/v7/internal/widget/y;
getfield android/support/v7/internal/widget/y/c Landroid/support/v7/internal/widget/l;
aload 1
invokevirtual android/support/v7/internal/widget/l/a(Landroid/content/pm/ResolveInfo;)I
istore 2
aload 0
getfield android/support/v7/internal/widget/z/a Landroid/support/v7/internal/widget/ActivityChooserView;
invokestatic android/support/v7/internal/widget/ActivityChooserView/a(Landroid/support/v7/internal/widget/ActivityChooserView;)Landroid/support/v7/internal/widget/y;
getfield android/support/v7/internal/widget/y/c Landroid/support/v7/internal/widget/l;
iload 2
invokevirtual android/support/v7/internal/widget/l/b(I)Landroid/content/Intent;
astore 1
aload 1
ifnull L1
aload 1
ldc_w 524288
invokevirtual android/content/Intent/addFlags(I)Landroid/content/Intent;
pop
aload 0
getfield android/support/v7/internal/widget/z/a Landroid/support/v7/internal/widget/ActivityChooserView;
invokevirtual android/support/v7/internal/widget/ActivityChooserView/getContext()Landroid/content/Context;
aload 1
invokevirtual android/content/Context/startActivity(Landroid/content/Intent;)V
L1:
return
L0:
aload 1
aload 0
getfield android/support/v7/internal/widget/z/a Landroid/support/v7/internal/widget/ActivityChooserView;
invokestatic android/support/v7/internal/widget/ActivityChooserView/f(Landroid/support/v7/internal/widget/ActivityChooserView;)Landroid/widget/FrameLayout;
if_acmpne L2
aload 0
getfield android/support/v7/internal/widget/z/a Landroid/support/v7/internal/widget/ActivityChooserView;
iconst_0
invokestatic android/support/v7/internal/widget/ActivityChooserView/a(Landroid/support/v7/internal/widget/ActivityChooserView;Z)Z
pop
aload 0
getfield android/support/v7/internal/widget/z/a Landroid/support/v7/internal/widget/ActivityChooserView;
aload 0
getfield android/support/v7/internal/widget/z/a Landroid/support/v7/internal/widget/ActivityChooserView;
invokestatic android/support/v7/internal/widget/ActivityChooserView/g(Landroid/support/v7/internal/widget/ActivityChooserView;)I
invokestatic android/support/v7/internal/widget/ActivityChooserView/a(Landroid/support/v7/internal/widget/ActivityChooserView;I)V
return
L2:
new java/lang/IllegalArgumentException
dup
invokespecial java/lang/IllegalArgumentException/<init>()V
athrow
.limit locals 3
.limit stack 2
.end method

.method public final onDismiss()V
aload 0
getfield android/support/v7/internal/widget/z/a Landroid/support/v7/internal/widget/ActivityChooserView;
invokestatic android/support/v7/internal/widget/ActivityChooserView/h(Landroid/support/v7/internal/widget/ActivityChooserView;)Landroid/widget/PopupWindow$OnDismissListener;
ifnull L0
aload 0
getfield android/support/v7/internal/widget/z/a Landroid/support/v7/internal/widget/ActivityChooserView;
invokestatic android/support/v7/internal/widget/ActivityChooserView/h(Landroid/support/v7/internal/widget/ActivityChooserView;)Landroid/widget/PopupWindow$OnDismissListener;
invokeinterface android/widget/PopupWindow$OnDismissListener/onDismiss()V 0
L0:
aload 0
getfield android/support/v7/internal/widget/z/a Landroid/support/v7/internal/widget/ActivityChooserView;
getfield android/support/v7/internal/widget/ActivityChooserView/a Landroid/support/v4/view/n;
ifnull L1
aload 0
getfield android/support/v7/internal/widget/z/a Landroid/support/v7/internal/widget/ActivityChooserView;
getfield android/support/v7/internal/widget/ActivityChooserView/a Landroid/support/v4/view/n;
iconst_0
invokevirtual android/support/v4/view/n/a(Z)V
L1:
return
.limit locals 1
.limit stack 2
.end method

.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch all from L4 to L5 using L2
.catch all from L6 to L7 using L2
aload 1
invokevirtual android/widget/AdapterView/getAdapter()Landroid/widget/Adapter;
checkcast android/support/v7/internal/widget/y
iload 3
invokevirtual android/support/v7/internal/widget/y/getItemViewType(I)I
tableswitch 0
L8
L9
default : L10
L10:
new java/lang/IllegalArgumentException
dup
invokespecial java/lang/IllegalArgumentException/<init>()V
athrow
L9:
aload 0
getfield android/support/v7/internal/widget/z/a Landroid/support/v7/internal/widget/ActivityChooserView;
ldc_w 2147483647
invokestatic android/support/v7/internal/widget/ActivityChooserView/a(Landroid/support/v7/internal/widget/ActivityChooserView;I)V
L11:
return
L8:
aload 0
getfield android/support/v7/internal/widget/z/a Landroid/support/v7/internal/widget/ActivityChooserView;
invokevirtual android/support/v7/internal/widget/ActivityChooserView/b()Z
pop
aload 0
getfield android/support/v7/internal/widget/z/a Landroid/support/v7/internal/widget/ActivityChooserView;
invokestatic android/support/v7/internal/widget/ActivityChooserView/d(Landroid/support/v7/internal/widget/ActivityChooserView;)Z
ifeq L12
iload 3
ifle L11
aload 0
getfield android/support/v7/internal/widget/z/a Landroid/support/v7/internal/widget/ActivityChooserView;
invokestatic android/support/v7/internal/widget/ActivityChooserView/a(Landroid/support/v7/internal/widget/ActivityChooserView;)Landroid/support/v7/internal/widget/y;
getfield android/support/v7/internal/widget/y/c Landroid/support/v7/internal/widget/l;
astore 2
aload 2
getfield android/support/v7/internal/widget/l/c Ljava/lang/Object;
astore 1
aload 1
monitorenter
L0:
aload 2
invokevirtual android/support/v7/internal/widget/l/d()V
aload 2
getfield android/support/v7/internal/widget/l/d Ljava/util/List;
iload 3
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast android/support/v7/internal/widget/o
astore 7
aload 2
getfield android/support/v7/internal/widget/l/d Ljava/util/List;
iconst_0
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast android/support/v7/internal/widget/o
astore 8
L1:
aload 8
ifnull L13
L3:
aload 8
getfield android/support/v7/internal/widget/o/b F
aload 7
getfield android/support/v7/internal/widget/o/b F
fsub
ldc_w 5.0F
fadd
fstore 6
L4:
aload 2
new android/support/v7/internal/widget/r
dup
new android/content/ComponentName
dup
aload 7
getfield android/support/v7/internal/widget/o/a Landroid/content/pm/ResolveInfo;
getfield android/content/pm/ResolveInfo/activityInfo Landroid/content/pm/ActivityInfo;
getfield android/content/pm/ActivityInfo/packageName Ljava/lang/String;
aload 7
getfield android/support/v7/internal/widget/o/a Landroid/content/pm/ResolveInfo;
getfield android/content/pm/ResolveInfo/activityInfo Landroid/content/pm/ActivityInfo;
getfield android/content/pm/ActivityInfo/name Ljava/lang/String;
invokespecial android/content/ComponentName/<init>(Ljava/lang/String;Ljava/lang/String;)V
invokestatic java/lang/System/currentTimeMillis()J
fload 6
invokespecial android/support/v7/internal/widget/r/<init>(Landroid/content/ComponentName;JF)V
invokevirtual android/support/v7/internal/widget/l/a(Landroid/support/v7/internal/widget/r;)Z
pop
aload 1
monitorexit
L5:
return
L2:
astore 2
L6:
aload 1
monitorexit
L7:
aload 2
athrow
L13:
fconst_1
fstore 6
goto L4
L12:
aload 0
getfield android/support/v7/internal/widget/z/a Landroid/support/v7/internal/widget/ActivityChooserView;
invokestatic android/support/v7/internal/widget/ActivityChooserView/a(Landroid/support/v7/internal/widget/ActivityChooserView;)Landroid/support/v7/internal/widget/y;
getfield android/support/v7/internal/widget/y/d Z
ifeq L14
L15:
aload 0
getfield android/support/v7/internal/widget/z/a Landroid/support/v7/internal/widget/ActivityChooserView;
invokestatic android/support/v7/internal/widget/ActivityChooserView/a(Landroid/support/v7/internal/widget/ActivityChooserView;)Landroid/support/v7/internal/widget/y;
getfield android/support/v7/internal/widget/y/c Landroid/support/v7/internal/widget/l;
iload 3
invokevirtual android/support/v7/internal/widget/l/b(I)Landroid/content/Intent;
astore 1
aload 1
ifnull L11
aload 1
ldc_w 524288
invokevirtual android/content/Intent/addFlags(I)Landroid/content/Intent;
pop
aload 0
getfield android/support/v7/internal/widget/z/a Landroid/support/v7/internal/widget/ActivityChooserView;
invokevirtual android/support/v7/internal/widget/ActivityChooserView/getContext()Landroid/content/Context;
aload 1
invokevirtual android/content/Context/startActivity(Landroid/content/Intent;)V
return
L14:
iload 3
iconst_1
iadd
istore 3
goto L15
.limit locals 9
.limit stack 7
.end method

.method public final onLongClick(Landroid/view/View;)Z
aload 1
aload 0
getfield android/support/v7/internal/widget/z/a Landroid/support/v7/internal/widget/ActivityChooserView;
invokestatic android/support/v7/internal/widget/ActivityChooserView/e(Landroid/support/v7/internal/widget/ActivityChooserView;)Landroid/widget/FrameLayout;
if_acmpne L0
aload 0
getfield android/support/v7/internal/widget/z/a Landroid/support/v7/internal/widget/ActivityChooserView;
invokestatic android/support/v7/internal/widget/ActivityChooserView/a(Landroid/support/v7/internal/widget/ActivityChooserView;)Landroid/support/v7/internal/widget/y;
invokevirtual android/support/v7/internal/widget/y/getCount()I
ifle L1
aload 0
getfield android/support/v7/internal/widget/z/a Landroid/support/v7/internal/widget/ActivityChooserView;
iconst_1
invokestatic android/support/v7/internal/widget/ActivityChooserView/a(Landroid/support/v7/internal/widget/ActivityChooserView;Z)Z
pop
aload 0
getfield android/support/v7/internal/widget/z/a Landroid/support/v7/internal/widget/ActivityChooserView;
aload 0
getfield android/support/v7/internal/widget/z/a Landroid/support/v7/internal/widget/ActivityChooserView;
invokestatic android/support/v7/internal/widget/ActivityChooserView/g(Landroid/support/v7/internal/widget/ActivityChooserView;)I
invokestatic android/support/v7/internal/widget/ActivityChooserView/a(Landroid/support/v7/internal/widget/ActivityChooserView;I)V
L1:
iconst_1
ireturn
L0:
new java/lang/IllegalArgumentException
dup
invokespecial java/lang/IllegalArgumentException/<init>()V
athrow
.limit locals 2
.limit stack 2
.end method
