.bytecode 50.0
.class public final synchronized android/support/v7/internal/widget/al
.super android/widget/HorizontalScrollView
.implements android/widget/AdapterView$OnItemSelectedListener

.field private static final 'i' Ljava/lang/String; = "ScrollingTabContainerView"

.field private static final 'm' Landroid/view/animation/Interpolator;

.field private static final 'n' I = 200


.field 'a' Ljava/lang/Runnable;

.field public 'b' Landroid/support/v7/widget/aj;

.field public 'c' Landroid/widget/Spinner;

.field public 'd' Z

.field 'e' I

.field 'f' I

.field protected 'g' Landroid/support/v4/view/fk;

.field protected final 'h' Landroid/support/v7/internal/widget/aq;

.field private 'j' Landroid/support/v7/internal/widget/ao;

.field private 'k' I

.field private 'l' I

.method static <clinit>()V
new android/view/animation/DecelerateInterpolator
dup
invokespecial android/view/animation/DecelerateInterpolator/<init>()V
putstatic android/support/v7/internal/widget/al/m Landroid/view/animation/Interpolator;
return
.limit locals 0
.limit stack 2
.end method

.method public <init>(Landroid/content/Context;)V
aload 0
aload 1
invokespecial android/widget/HorizontalScrollView/<init>(Landroid/content/Context;)V
aload 0
new android/support/v7/internal/widget/aq
dup
aload 0
invokespecial android/support/v7/internal/widget/aq/<init>(Landroid/support/v7/internal/widget/al;)V
putfield android/support/v7/internal/widget/al/h Landroid/support/v7/internal/widget/aq;
aload 0
iconst_0
invokevirtual android/support/v7/internal/widget/al/setHorizontalScrollBarEnabled(Z)V
aload 1
invokestatic android/support/v7/internal/view/a/a(Landroid/content/Context;)Landroid/support/v7/internal/view/a;
astore 1
aload 0
aload 1
invokevirtual android/support/v7/internal/view/a/b()I
invokevirtual android/support/v7/internal/widget/al/setContentHeight(I)V
aload 0
aload 1
invokevirtual android/support/v7/internal/view/a/c()I
putfield android/support/v7/internal/widget/al/f I
new android/support/v7/widget/aj
dup
aload 0
invokevirtual android/support/v7/internal/widget/al/getContext()Landroid/content/Context;
aconst_null
getstatic android/support/v7/a/d/actionBarTabBarStyle I
invokespecial android/support/v7/widget/aj/<init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
astore 1
aload 1
iconst_1
invokevirtual android/support/v7/widget/aj/setMeasureWithLargestChildEnabled(Z)V
aload 1
bipush 17
invokevirtual android/support/v7/widget/aj/setGravity(I)V
aload 1
new android/support/v7/widget/al
dup
bipush -2
iconst_m1
invokespecial android/support/v7/widget/al/<init>(II)V
invokevirtual android/support/v7/widget/aj/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 0
aload 1
putfield android/support/v7/internal/widget/al/b Landroid/support/v7/widget/aj;
aload 0
aload 0
getfield android/support/v7/internal/widget/al/b Landroid/support/v7/widget/aj;
new android/view/ViewGroup$LayoutParams
dup
bipush -2
iconst_m1
invokespecial android/view/ViewGroup$LayoutParams/<init>(II)V
invokevirtual android/support/v7/internal/widget/al/addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
return
.limit locals 2
.limit stack 6
.end method

.method static synthetic a(Landroid/support/v7/internal/widget/al;Landroid/support/v7/app/g;)Landroid/support/v7/internal/widget/ap;
aload 0
aload 1
iconst_1
invokevirtual android/support/v7/internal/widget/al/a(Landroid/support/v7/app/g;Z)Landroid/support/v7/internal/widget/ap;
areturn
.limit locals 2
.limit stack 3
.end method

.method static synthetic a(Landroid/support/v7/internal/widget/al;)Landroid/support/v7/widget/aj;
aload 0
getfield android/support/v7/internal/widget/al/b Landroid/support/v7/widget/aj;
areturn
.limit locals 1
.limit stack 1
.end method

.method private a(Landroid/support/v7/app/g;IZ)V
aload 0
aload 1
iconst_0
invokevirtual android/support/v7/internal/widget/al/a(Landroid/support/v7/app/g;Z)Landroid/support/v7/internal/widget/ap;
astore 1
aload 0
getfield android/support/v7/internal/widget/al/b Landroid/support/v7/widget/aj;
aload 1
iload 2
new android/support/v7/widget/al
dup
invokespecial android/support/v7/widget/al/<init>()V
invokevirtual android/support/v7/widget/aj/addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
aload 0
getfield android/support/v7/internal/widget/al/c Landroid/widget/Spinner;
ifnull L0
aload 0
getfield android/support/v7/internal/widget/al/c Landroid/widget/Spinner;
invokevirtual android/widget/Spinner/getAdapter()Landroid/widget/SpinnerAdapter;
checkcast android/support/v7/internal/widget/an
invokevirtual android/support/v7/internal/widget/an/notifyDataSetChanged()V
L0:
iload 3
ifeq L1
aload 1
iconst_1
invokevirtual android/support/v7/internal/widget/ap/setSelected(Z)V
L1:
aload 0
getfield android/support/v7/internal/widget/al/d Z
ifeq L2
aload 0
invokevirtual android/support/v7/internal/widget/al/requestLayout()V
L2:
return
.limit locals 4
.limit stack 5
.end method

.method private a()Z
aload 0
getfield android/support/v7/internal/widget/al/c Landroid/widget/Spinner;
ifnull L0
aload 0
getfield android/support/v7/internal/widget/al/c Landroid/widget/Spinner;
invokevirtual android/widget/Spinner/getParent()Landroid/view/ViewParent;
aload 0
if_acmpne L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method private b()V
aload 0
invokespecial android/support/v7/internal/widget/al/a()Z
ifeq L0
return
L0:
aload 0
getfield android/support/v7/internal/widget/al/c Landroid/widget/Spinner;
ifnonnull L1
new android/support/v7/widget/aa
dup
aload 0
invokevirtual android/support/v7/internal/widget/al/getContext()Landroid/content/Context;
aconst_null
getstatic android/support/v7/a/d/actionDropDownStyle I
invokespecial android/support/v7/widget/aa/<init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
astore 1
aload 1
new android/support/v7/widget/al
dup
bipush -2
iconst_m1
invokespecial android/support/v7/widget/al/<init>(II)V
invokevirtual android/widget/Spinner/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 1
aload 0
invokevirtual android/widget/Spinner/setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V
aload 0
aload 1
putfield android/support/v7/internal/widget/al/c Landroid/widget/Spinner;
L1:
aload 0
aload 0
getfield android/support/v7/internal/widget/al/b Landroid/support/v7/widget/aj;
invokevirtual android/support/v7/internal/widget/al/removeView(Landroid/view/View;)V
aload 0
aload 0
getfield android/support/v7/internal/widget/al/c Landroid/widget/Spinner;
new android/view/ViewGroup$LayoutParams
dup
bipush -2
iconst_m1
invokespecial android/view/ViewGroup$LayoutParams/<init>(II)V
invokevirtual android/support/v7/internal/widget/al/addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
aload 0
getfield android/support/v7/internal/widget/al/c Landroid/widget/Spinner;
invokevirtual android/widget/Spinner/getAdapter()Landroid/widget/SpinnerAdapter;
ifnonnull L2
aload 0
getfield android/support/v7/internal/widget/al/c Landroid/widget/Spinner;
new android/support/v7/internal/widget/an
dup
aload 0
iconst_0
invokespecial android/support/v7/internal/widget/an/<init>(Landroid/support/v7/internal/widget/al;B)V
invokevirtual android/widget/Spinner/setAdapter(Landroid/widget/SpinnerAdapter;)V
L2:
aload 0
getfield android/support/v7/internal/widget/al/a Ljava/lang/Runnable;
ifnull L3
aload 0
aload 0
getfield android/support/v7/internal/widget/al/a Ljava/lang/Runnable;
invokevirtual android/support/v7/internal/widget/al/removeCallbacks(Ljava/lang/Runnable;)Z
pop
aload 0
aconst_null
putfield android/support/v7/internal/widget/al/a Ljava/lang/Runnable;
L3:
aload 0
getfield android/support/v7/internal/widget/al/c Landroid/widget/Spinner;
aload 0
getfield android/support/v7/internal/widget/al/l I
invokevirtual android/widget/Spinner/setSelection(I)V
return
.limit locals 2
.limit stack 6
.end method

.method private b(Landroid/support/v7/app/g;Z)V
aload 0
aload 1
iconst_0
invokevirtual android/support/v7/internal/widget/al/a(Landroid/support/v7/app/g;Z)Landroid/support/v7/internal/widget/ap;
astore 1
aload 0
getfield android/support/v7/internal/widget/al/b Landroid/support/v7/widget/aj;
aload 1
new android/support/v7/widget/al
dup
invokespecial android/support/v7/widget/al/<init>()V
invokevirtual android/support/v7/widget/aj/addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
aload 0
getfield android/support/v7/internal/widget/al/c Landroid/widget/Spinner;
ifnull L0
aload 0
getfield android/support/v7/internal/widget/al/c Landroid/widget/Spinner;
invokevirtual android/widget/Spinner/getAdapter()Landroid/widget/SpinnerAdapter;
checkcast android/support/v7/internal/widget/an
invokevirtual android/support/v7/internal/widget/an/notifyDataSetChanged()V
L0:
iload 2
ifeq L1
aload 1
iconst_1
invokevirtual android/support/v7/internal/widget/ap/setSelected(Z)V
L1:
aload 0
getfield android/support/v7/internal/widget/al/d Z
ifeq L2
aload 0
invokevirtual android/support/v7/internal/widget/al/requestLayout()V
L2:
return
.limit locals 3
.limit stack 4
.end method

.method private c(I)V
aload 0
getfield android/support/v7/internal/widget/al/g Landroid/support/v4/view/fk;
ifnull L0
aload 0
getfield android/support/v7/internal/widget/al/g Landroid/support/v4/view/fk;
invokevirtual android/support/v4/view/fk/a()V
L0:
iload 1
ifne L1
aload 0
invokevirtual android/support/v7/internal/widget/al/getVisibility()I
ifeq L2
aload 0
fconst_0
invokestatic android/support/v4/view/cx/c(Landroid/view/View;F)V
L2:
aload 0
invokestatic android/support/v4/view/cx/p(Landroid/view/View;)Landroid/support/v4/view/fk;
fconst_1
invokevirtual android/support/v4/view/fk/a(F)Landroid/support/v4/view/fk;
astore 2
aload 2
ldc2_w 200L
invokevirtual android/support/v4/view/fk/a(J)Landroid/support/v4/view/fk;
pop
aload 2
getstatic android/support/v7/internal/widget/al/m Landroid/view/animation/Interpolator;
invokevirtual android/support/v4/view/fk/a(Landroid/view/animation/Interpolator;)Landroid/support/v4/view/fk;
pop
aload 2
aload 0
getfield android/support/v7/internal/widget/al/h Landroid/support/v7/internal/widget/aq;
aload 2
iload 1
invokevirtual android/support/v7/internal/widget/aq/a(Landroid/support/v4/view/fk;I)Landroid/support/v7/internal/widget/aq;
invokevirtual android/support/v4/view/fk/a(Landroid/support/v4/view/gd;)Landroid/support/v4/view/fk;
pop
aload 2
invokevirtual android/support/v4/view/fk/b()V
return
L1:
aload 0
invokestatic android/support/v4/view/cx/p(Landroid/view/View;)Landroid/support/v4/view/fk;
fconst_0
invokevirtual android/support/v4/view/fk/a(F)Landroid/support/v4/view/fk;
astore 2
aload 2
ldc2_w 200L
invokevirtual android/support/v4/view/fk/a(J)Landroid/support/v4/view/fk;
pop
aload 2
getstatic android/support/v7/internal/widget/al/m Landroid/view/animation/Interpolator;
invokevirtual android/support/v4/view/fk/a(Landroid/view/animation/Interpolator;)Landroid/support/v4/view/fk;
pop
aload 2
aload 0
getfield android/support/v7/internal/widget/al/h Landroid/support/v7/internal/widget/aq;
aload 2
iload 1
invokevirtual android/support/v7/internal/widget/aq/a(Landroid/support/v4/view/fk;I)Landroid/support/v7/internal/widget/aq;
invokevirtual android/support/v4/view/fk/a(Landroid/support/v4/view/gd;)Landroid/support/v4/view/fk;
pop
aload 2
invokevirtual android/support/v4/view/fk/b()V
return
.limit locals 3
.limit stack 4
.end method

.method private c()Z
aload 0
invokespecial android/support/v7/internal/widget/al/a()Z
ifne L0
iconst_0
ireturn
L0:
aload 0
aload 0
getfield android/support/v7/internal/widget/al/c Landroid/widget/Spinner;
invokevirtual android/support/v7/internal/widget/al/removeView(Landroid/view/View;)V
aload 0
aload 0
getfield android/support/v7/internal/widget/al/b Landroid/support/v7/widget/aj;
new android/view/ViewGroup$LayoutParams
dup
bipush -2
iconst_m1
invokespecial android/view/ViewGroup$LayoutParams/<init>(II)V
invokevirtual android/support/v7/internal/widget/al/addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
aload 0
aload 0
getfield android/support/v7/internal/widget/al/c Landroid/widget/Spinner;
invokevirtual android/widget/Spinner/getSelectedItemPosition()I
invokevirtual android/support/v7/internal/widget/al/setTabSelected(I)V
iconst_0
ireturn
.limit locals 1
.limit stack 6
.end method

.method private d()Landroid/support/v7/widget/aj;
new android/support/v7/widget/aj
dup
aload 0
invokevirtual android/support/v7/internal/widget/al/getContext()Landroid/content/Context;
aconst_null
getstatic android/support/v7/a/d/actionBarTabBarStyle I
invokespecial android/support/v7/widget/aj/<init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
astore 1
aload 1
iconst_1
invokevirtual android/support/v7/widget/aj/setMeasureWithLargestChildEnabled(Z)V
aload 1
bipush 17
invokevirtual android/support/v7/widget/aj/setGravity(I)V
aload 1
new android/support/v7/widget/al
dup
bipush -2
iconst_m1
invokespecial android/support/v7/widget/al/<init>(II)V
invokevirtual android/support/v7/widget/aj/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 1
areturn
.limit locals 2
.limit stack 5
.end method

.method private d(I)V
aload 0
getfield android/support/v7/internal/widget/al/b Landroid/support/v7/widget/aj;
iload 1
invokevirtual android/support/v7/widget/aj/removeViewAt(I)V
aload 0
getfield android/support/v7/internal/widget/al/c Landroid/widget/Spinner;
ifnull L0
aload 0
getfield android/support/v7/internal/widget/al/c Landroid/widget/Spinner;
invokevirtual android/widget/Spinner/getAdapter()Landroid/widget/SpinnerAdapter;
checkcast android/support/v7/internal/widget/an
invokevirtual android/support/v7/internal/widget/an/notifyDataSetChanged()V
L0:
aload 0
getfield android/support/v7/internal/widget/al/d Z
ifeq L1
aload 0
invokevirtual android/support/v7/internal/widget/al/requestLayout()V
L1:
return
.limit locals 2
.limit stack 2
.end method

.method private e()Landroid/widget/Spinner;
new android/support/v7/widget/aa
dup
aload 0
invokevirtual android/support/v7/internal/widget/al/getContext()Landroid/content/Context;
aconst_null
getstatic android/support/v7/a/d/actionDropDownStyle I
invokespecial android/support/v7/widget/aa/<init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
astore 1
aload 1
new android/support/v7/widget/al
dup
bipush -2
iconst_m1
invokespecial android/support/v7/widget/al/<init>(II)V
invokevirtual android/widget/Spinner/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 1
aload 0
invokevirtual android/widget/Spinner/setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V
aload 1
areturn
.limit locals 2
.limit stack 5
.end method

.method private f()V
aload 0
getfield android/support/v7/internal/widget/al/b Landroid/support/v7/widget/aj;
invokevirtual android/support/v7/widget/aj/removeAllViews()V
aload 0
getfield android/support/v7/internal/widget/al/c Landroid/widget/Spinner;
ifnull L0
aload 0
getfield android/support/v7/internal/widget/al/c Landroid/widget/Spinner;
invokevirtual android/widget/Spinner/getAdapter()Landroid/widget/SpinnerAdapter;
checkcast android/support/v7/internal/widget/an
invokevirtual android/support/v7/internal/widget/an/notifyDataSetChanged()V
L0:
aload 0
getfield android/support/v7/internal/widget/al/d Z
ifeq L1
aload 0
invokevirtual android/support/v7/internal/widget/al/requestLayout()V
L1:
return
.limit locals 1
.limit stack 1
.end method

.method public final a(Landroid/support/v7/app/g;Z)Landroid/support/v7/internal/widget/ap;
new android/support/v7/internal/widget/ap
dup
aload 0
aload 0
invokevirtual android/support/v7/internal/widget/al/getContext()Landroid/content/Context;
aload 1
iload 2
invokespecial android/support/v7/internal/widget/ap/<init>(Landroid/support/v7/internal/widget/al;Landroid/content/Context;Landroid/support/v7/app/g;Z)V
astore 1
iload 2
ifeq L0
aload 1
aconst_null
invokevirtual android/support/v7/internal/widget/ap/setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
aload 1
new android/widget/AbsListView$LayoutParams
dup
iconst_m1
aload 0
getfield android/support/v7/internal/widget/al/k I
invokespecial android/widget/AbsListView$LayoutParams/<init>(II)V
invokevirtual android/support/v7/internal/widget/ap/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 1
areturn
L0:
aload 1
iconst_1
invokevirtual android/support/v7/internal/widget/ap/setFocusable(Z)V
aload 0
getfield android/support/v7/internal/widget/al/j Landroid/support/v7/internal/widget/ao;
ifnonnull L1
aload 0
new android/support/v7/internal/widget/ao
dup
aload 0
iconst_0
invokespecial android/support/v7/internal/widget/ao/<init>(Landroid/support/v7/internal/widget/al;B)V
putfield android/support/v7/internal/widget/al/j Landroid/support/v7/internal/widget/ao;
L1:
aload 1
aload 0
getfield android/support/v7/internal/widget/al/j Landroid/support/v7/internal/widget/ao;
invokevirtual android/support/v7/internal/widget/ap/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 1
areturn
.limit locals 3
.limit stack 6
.end method

.method public final a(I)V
aload 0
getfield android/support/v7/internal/widget/al/b Landroid/support/v7/widget/aj;
iload 1
invokevirtual android/support/v7/widget/aj/getChildAt(I)Landroid/view/View;
astore 2
aload 0
getfield android/support/v7/internal/widget/al/a Ljava/lang/Runnable;
ifnull L0
aload 0
aload 0
getfield android/support/v7/internal/widget/al/a Ljava/lang/Runnable;
invokevirtual android/support/v7/internal/widget/al/removeCallbacks(Ljava/lang/Runnable;)Z
pop
L0:
aload 0
new android/support/v7/internal/widget/am
dup
aload 0
aload 2
invokespecial android/support/v7/internal/widget/am/<init>(Landroid/support/v7/internal/widget/al;Landroid/view/View;)V
putfield android/support/v7/internal/widget/al/a Ljava/lang/Runnable;
aload 0
aload 0
getfield android/support/v7/internal/widget/al/a Ljava/lang/Runnable;
invokevirtual android/support/v7/internal/widget/al/post(Ljava/lang/Runnable;)Z
pop
return
.limit locals 3
.limit stack 5
.end method

.method public final b(I)V
aload 0
getfield android/support/v7/internal/widget/al/b Landroid/support/v7/widget/aj;
iload 1
invokevirtual android/support/v7/widget/aj/getChildAt(I)Landroid/view/View;
checkcast android/support/v7/internal/widget/ap
invokevirtual android/support/v7/internal/widget/ap/a()V
aload 0
getfield android/support/v7/internal/widget/al/c Landroid/widget/Spinner;
ifnull L0
aload 0
getfield android/support/v7/internal/widget/al/c Landroid/widget/Spinner;
invokevirtual android/widget/Spinner/getAdapter()Landroid/widget/SpinnerAdapter;
checkcast android/support/v7/internal/widget/an
invokevirtual android/support/v7/internal/widget/an/notifyDataSetChanged()V
L0:
aload 0
getfield android/support/v7/internal/widget/al/d Z
ifeq L1
aload 0
invokevirtual android/support/v7/internal/widget/al/requestLayout()V
L1:
return
.limit locals 2
.limit stack 2
.end method

.method public final onAttachedToWindow()V
aload 0
invokespecial android/widget/HorizontalScrollView/onAttachedToWindow()V
aload 0
getfield android/support/v7/internal/widget/al/a Ljava/lang/Runnable;
ifnull L0
aload 0
aload 0
getfield android/support/v7/internal/widget/al/a Ljava/lang/Runnable;
invokevirtual android/support/v7/internal/widget/al/post(Ljava/lang/Runnable;)Z
pop
L0:
return
.limit locals 1
.limit stack 2
.end method

.method protected final onConfigurationChanged(Landroid/content/res/Configuration;)V
getstatic android/os/Build$VERSION/SDK_INT I
bipush 8
if_icmplt L0
aload 0
aload 1
invokespecial android/widget/HorizontalScrollView/onConfigurationChanged(Landroid/content/res/Configuration;)V
L0:
aload 0
invokevirtual android/support/v7/internal/widget/al/getContext()Landroid/content/Context;
invokestatic android/support/v7/internal/view/a/a(Landroid/content/Context;)Landroid/support/v7/internal/view/a;
astore 1
aload 0
aload 1
invokevirtual android/support/v7/internal/view/a/b()I
invokevirtual android/support/v7/internal/widget/al/setContentHeight(I)V
aload 0
aload 1
invokevirtual android/support/v7/internal/view/a/c()I
putfield android/support/v7/internal/widget/al/f I
return
.limit locals 2
.limit stack 2
.end method

.method public final onDetachedFromWindow()V
aload 0
invokespecial android/widget/HorizontalScrollView/onDetachedFromWindow()V
aload 0
getfield android/support/v7/internal/widget/al/a Ljava/lang/Runnable;
ifnull L0
aload 0
aload 0
getfield android/support/v7/internal/widget/al/a Ljava/lang/Runnable;
invokevirtual android/support/v7/internal/widget/al/removeCallbacks(Ljava/lang/Runnable;)Z
pop
L0:
return
.limit locals 1
.limit stack 2
.end method

.method public final onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
aload 2
checkcast android/support/v7/internal/widget/ap
getfield android/support/v7/internal/widget/ap/a Landroid/support/v7/app/g;
invokevirtual android/support/v7/app/g/f()V
return
.limit locals 6
.limit stack 1
.end method

.method public final onMeasure(II)V
iconst_1
istore 2
iload 1
invokestatic android/view/View$MeasureSpec/getMode(I)I
istore 3
iload 3
ldc_w 1073741824
if_icmpne L0
iconst_1
istore 5
L1:
aload 0
iload 5
invokevirtual android/support/v7/internal/widget/al/setFillViewport(Z)V
aload 0
getfield android/support/v7/internal/widget/al/b Landroid/support/v7/widget/aj;
invokevirtual android/support/v7/widget/aj/getChildCount()I
istore 4
iload 4
iconst_1
if_icmple L2
iload 3
ldc_w 1073741824
if_icmpeq L3
iload 3
ldc_w -2147483648
if_icmpne L2
L3:
iload 4
iconst_2
if_icmple L4
aload 0
iload 1
invokestatic android/view/View$MeasureSpec/getSize(I)I
i2f
ldc_w 0.4F
fmul
f2i
putfield android/support/v7/internal/widget/al/e I
L5:
aload 0
aload 0
getfield android/support/v7/internal/widget/al/e I
aload 0
getfield android/support/v7/internal/widget/al/f I
invokestatic java/lang/Math/min(II)I
putfield android/support/v7/internal/widget/al/e I
L6:
aload 0
getfield android/support/v7/internal/widget/al/k I
ldc_w 1073741824
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
istore 3
iload 5
ifne L7
aload 0
getfield android/support/v7/internal/widget/al/d Z
ifeq L7
L8:
iload 2
ifeq L9
aload 0
getfield android/support/v7/internal/widget/al/b Landroid/support/v7/widget/aj;
iconst_0
iload 3
invokevirtual android/support/v7/widget/aj/measure(II)V
aload 0
getfield android/support/v7/internal/widget/al/b Landroid/support/v7/widget/aj;
invokevirtual android/support/v7/widget/aj/getMeasuredWidth()I
iload 1
invokestatic android/view/View$MeasureSpec/getSize(I)I
if_icmple L10
aload 0
invokespecial android/support/v7/internal/widget/al/a()Z
ifne L11
aload 0
getfield android/support/v7/internal/widget/al/c Landroid/widget/Spinner;
ifnonnull L12
new android/support/v7/widget/aa
dup
aload 0
invokevirtual android/support/v7/internal/widget/al/getContext()Landroid/content/Context;
aconst_null
getstatic android/support/v7/a/d/actionDropDownStyle I
invokespecial android/support/v7/widget/aa/<init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
astore 6
aload 6
new android/support/v7/widget/al
dup
bipush -2
iconst_m1
invokespecial android/support/v7/widget/al/<init>(II)V
invokevirtual android/widget/Spinner/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 6
aload 0
invokevirtual android/widget/Spinner/setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V
aload 0
aload 6
putfield android/support/v7/internal/widget/al/c Landroid/widget/Spinner;
L12:
aload 0
aload 0
getfield android/support/v7/internal/widget/al/b Landroid/support/v7/widget/aj;
invokevirtual android/support/v7/internal/widget/al/removeView(Landroid/view/View;)V
aload 0
aload 0
getfield android/support/v7/internal/widget/al/c Landroid/widget/Spinner;
new android/view/ViewGroup$LayoutParams
dup
bipush -2
iconst_m1
invokespecial android/view/ViewGroup$LayoutParams/<init>(II)V
invokevirtual android/support/v7/internal/widget/al/addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
aload 0
getfield android/support/v7/internal/widget/al/c Landroid/widget/Spinner;
invokevirtual android/widget/Spinner/getAdapter()Landroid/widget/SpinnerAdapter;
ifnonnull L13
aload 0
getfield android/support/v7/internal/widget/al/c Landroid/widget/Spinner;
new android/support/v7/internal/widget/an
dup
aload 0
iconst_0
invokespecial android/support/v7/internal/widget/an/<init>(Landroid/support/v7/internal/widget/al;B)V
invokevirtual android/widget/Spinner/setAdapter(Landroid/widget/SpinnerAdapter;)V
L13:
aload 0
getfield android/support/v7/internal/widget/al/a Ljava/lang/Runnable;
ifnull L14
aload 0
aload 0
getfield android/support/v7/internal/widget/al/a Ljava/lang/Runnable;
invokevirtual android/support/v7/internal/widget/al/removeCallbacks(Ljava/lang/Runnable;)Z
pop
aload 0
aconst_null
putfield android/support/v7/internal/widget/al/a Ljava/lang/Runnable;
L14:
aload 0
getfield android/support/v7/internal/widget/al/c Landroid/widget/Spinner;
aload 0
getfield android/support/v7/internal/widget/al/l I
invokevirtual android/widget/Spinner/setSelection(I)V
L11:
aload 0
invokevirtual android/support/v7/internal/widget/al/getMeasuredWidth()I
istore 2
aload 0
iload 1
iload 3
invokespecial android/widget/HorizontalScrollView/onMeasure(II)V
aload 0
invokevirtual android/support/v7/internal/widget/al/getMeasuredWidth()I
istore 1
iload 5
ifeq L15
iload 2
iload 1
if_icmpeq L15
aload 0
aload 0
getfield android/support/v7/internal/widget/al/l I
invokevirtual android/support/v7/internal/widget/al/setTabSelected(I)V
L15:
return
L0:
iconst_0
istore 5
goto L1
L4:
aload 0
iload 1
invokestatic android/view/View$MeasureSpec/getSize(I)I
iconst_2
idiv
putfield android/support/v7/internal/widget/al/e I
goto L5
L2:
aload 0
iconst_m1
putfield android/support/v7/internal/widget/al/e I
goto L6
L7:
iconst_0
istore 2
goto L8
L10:
aload 0
invokespecial android/support/v7/internal/widget/al/c()Z
pop
goto L11
L9:
aload 0
invokespecial android/support/v7/internal/widget/al/c()Z
pop
goto L11
.limit locals 7
.limit stack 6
.end method

.method public final onNothingSelected(Landroid/widget/AdapterView;)V
return
.limit locals 2
.limit stack 0
.end method

.method public final setAllowCollapse(Z)V
aload 0
iload 1
putfield android/support/v7/internal/widget/al/d Z
return
.limit locals 2
.limit stack 2
.end method

.method public final setContentHeight(I)V
aload 0
iload 1
putfield android/support/v7/internal/widget/al/k I
aload 0
invokevirtual android/support/v7/internal/widget/al/requestLayout()V
return
.limit locals 2
.limit stack 2
.end method

.method public final setTabSelected(I)V
aload 0
iload 1
putfield android/support/v7/internal/widget/al/l I
aload 0
getfield android/support/v7/internal/widget/al/b Landroid/support/v7/widget/aj;
invokevirtual android/support/v7/widget/aj/getChildCount()I
istore 3
iconst_0
istore 2
L0:
iload 2
iload 3
if_icmpge L1
aload 0
getfield android/support/v7/internal/widget/al/b Landroid/support/v7/widget/aj;
iload 2
invokevirtual android/support/v7/widget/aj/getChildAt(I)Landroid/view/View;
astore 5
iload 2
iload 1
if_icmpne L2
iconst_1
istore 4
L3:
aload 5
iload 4
invokevirtual android/view/View/setSelected(Z)V
iload 4
ifeq L4
aload 0
iload 1
invokevirtual android/support/v7/internal/widget/al/a(I)V
L4:
iload 2
iconst_1
iadd
istore 2
goto L0
L2:
iconst_0
istore 4
goto L3
L1:
aload 0
getfield android/support/v7/internal/widget/al/c Landroid/widget/Spinner;
ifnull L5
iload 1
iflt L5
aload 0
getfield android/support/v7/internal/widget/al/c Landroid/widget/Spinner;
iload 1
invokevirtual android/widget/Spinner/setSelection(I)V
L5:
return
.limit locals 6
.limit stack 2
.end method
