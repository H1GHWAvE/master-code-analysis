.bytecode 50.0
.class final synchronized android/support/v7/internal/widget/t
.super android/os/AsyncTask

.field final synthetic 'a' Landroid/support/v7/internal/widget/l;

.method private <init>(Landroid/support/v7/internal/widget/l;)V
aload 0
aload 1
putfield android/support/v7/internal/widget/t/a Landroid/support/v7/internal/widget/l;
aload 0
invokespecial android/os/AsyncTask/<init>()V
return
.limit locals 2
.limit stack 2
.end method

.method synthetic <init>(Landroid/support/v7/internal/widget/l;B)V
aload 0
aload 1
invokespecial android/support/v7/internal/widget/t/<init>(Landroid/support/v7/internal/widget/l;)V
return
.limit locals 3
.limit stack 2
.end method

.method private transient a([Ljava/lang/Object;)Ljava/lang/Void;
.catch java/io/FileNotFoundException from L0 to L1 using L2
.catch java/lang/IllegalArgumentException from L3 to L4 using L5
.catch java/lang/IllegalStateException from L3 to L4 using L6
.catch java/io/IOException from L3 to L4 using L7
.catch all from L3 to L4 using L8
.catch java/lang/IllegalArgumentException from L9 to L10 using L5
.catch java/lang/IllegalStateException from L9 to L10 using L6
.catch java/io/IOException from L9 to L10 using L7
.catch all from L9 to L10 using L8
.catch java/lang/IllegalArgumentException from L11 to L12 using L5
.catch java/lang/IllegalStateException from L11 to L12 using L6
.catch java/io/IOException from L11 to L12 using L7
.catch all from L11 to L12 using L8
.catch java/io/IOException from L13 to L14 using L15
.catch all from L16 to L17 using L8
.catch java/io/IOException from L18 to L19 using L20
.catch all from L21 to L22 using L8
.catch java/io/IOException from L23 to L24 using L25
.catch all from L26 to L27 using L8
.catch java/io/IOException from L28 to L29 using L30
.catch java/io/IOException from L31 to L32 using L33
iconst_0
istore 2
aload 1
iconst_0
aaload
checkcast java/util/List
astore 4
aload 1
iconst_1
aaload
checkcast java/lang/String
astore 5
L0:
aload 0
getfield android/support/v7/internal/widget/t/a Landroid/support/v7/internal/widget/l;
invokestatic android/support/v7/internal/widget/l/a(Landroid/support/v7/internal/widget/l;)Landroid/content/Context;
aload 5
iconst_0
invokevirtual android/content/Context/openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;
astore 1
L1:
invokestatic android/util/Xml/newSerializer()Lorg/xmlpull/v1/XmlSerializer;
astore 5
L3:
aload 5
aload 1
aconst_null
invokeinterface org/xmlpull/v1/XmlSerializer/setOutput(Ljava/io/OutputStream;Ljava/lang/String;)V 2
aload 5
ldc "UTF-8"
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokeinterface org/xmlpull/v1/XmlSerializer/startDocument(Ljava/lang/String;Ljava/lang/Boolean;)V 2
aload 5
aconst_null
ldc "historical-records"
invokeinterface org/xmlpull/v1/XmlSerializer/startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer; 2
pop
aload 4
invokeinterface java/util/List/size()I 0
istore 3
L4:
iload 2
iload 3
if_icmpge L11
L9:
aload 4
iconst_0
invokeinterface java/util/List/remove(I)Ljava/lang/Object; 1
checkcast android/support/v7/internal/widget/r
astore 6
aload 5
aconst_null
ldc "historical-record"
invokeinterface org/xmlpull/v1/XmlSerializer/startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer; 2
pop
aload 5
aconst_null
ldc "activity"
aload 6
getfield android/support/v7/internal/widget/r/a Landroid/content/ComponentName;
invokevirtual android/content/ComponentName/flattenToString()Ljava/lang/String;
invokeinterface org/xmlpull/v1/XmlSerializer/attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer; 3
pop
aload 5
aconst_null
ldc "time"
aload 6
getfield android/support/v7/internal/widget/r/b J
invokestatic java/lang/String/valueOf(J)Ljava/lang/String;
invokeinterface org/xmlpull/v1/XmlSerializer/attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer; 3
pop
aload 5
aconst_null
ldc "weight"
aload 6
getfield android/support/v7/internal/widget/r/c F
invokestatic java/lang/String/valueOf(F)Ljava/lang/String;
invokeinterface org/xmlpull/v1/XmlSerializer/attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer; 3
pop
aload 5
aconst_null
ldc "historical-record"
invokeinterface org/xmlpull/v1/XmlSerializer/endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer; 2
pop
L10:
iload 2
iconst_1
iadd
istore 2
goto L4
L2:
astore 1
invokestatic android/support/v7/internal/widget/l/e()Ljava/lang/String;
new java/lang/StringBuilder
dup
ldc "Error writing historical recrod file: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 5
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aload 1
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
L34:
aconst_null
areturn
L11:
aload 5
aconst_null
ldc "historical-records"
invokeinterface org/xmlpull/v1/XmlSerializer/endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer; 2
pop
aload 5
invokeinterface org/xmlpull/v1/XmlSerializer/endDocument()V 0
L12:
aload 0
getfield android/support/v7/internal/widget/t/a Landroid/support/v7/internal/widget/l;
invokestatic android/support/v7/internal/widget/l/c(Landroid/support/v7/internal/widget/l;)Z
pop
aload 1
ifnull L34
L13:
aload 1
invokevirtual java/io/FileOutputStream/close()V
L14:
aconst_null
areturn
L15:
astore 1
aconst_null
areturn
L5:
astore 4
L16:
invokestatic android/support/v7/internal/widget/l/e()Ljava/lang/String;
new java/lang/StringBuilder
dup
ldc "Error writing historical recrod file: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
getfield android/support/v7/internal/widget/t/a Landroid/support/v7/internal/widget/l;
invokestatic android/support/v7/internal/widget/l/b(Landroid/support/v7/internal/widget/l;)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aload 4
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
L17:
aload 0
getfield android/support/v7/internal/widget/t/a Landroid/support/v7/internal/widget/l;
invokestatic android/support/v7/internal/widget/l/c(Landroid/support/v7/internal/widget/l;)Z
pop
aload 1
ifnull L34
L18:
aload 1
invokevirtual java/io/FileOutputStream/close()V
L19:
aconst_null
areturn
L20:
astore 1
aconst_null
areturn
L6:
astore 4
L21:
invokestatic android/support/v7/internal/widget/l/e()Ljava/lang/String;
new java/lang/StringBuilder
dup
ldc "Error writing historical recrod file: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
getfield android/support/v7/internal/widget/t/a Landroid/support/v7/internal/widget/l;
invokestatic android/support/v7/internal/widget/l/b(Landroid/support/v7/internal/widget/l;)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aload 4
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
L22:
aload 0
getfield android/support/v7/internal/widget/t/a Landroid/support/v7/internal/widget/l;
invokestatic android/support/v7/internal/widget/l/c(Landroid/support/v7/internal/widget/l;)Z
pop
aload 1
ifnull L34
L23:
aload 1
invokevirtual java/io/FileOutputStream/close()V
L24:
aconst_null
areturn
L25:
astore 1
aconst_null
areturn
L7:
astore 4
L26:
invokestatic android/support/v7/internal/widget/l/e()Ljava/lang/String;
new java/lang/StringBuilder
dup
ldc "Error writing historical recrod file: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
getfield android/support/v7/internal/widget/t/a Landroid/support/v7/internal/widget/l;
invokestatic android/support/v7/internal/widget/l/b(Landroid/support/v7/internal/widget/l;)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aload 4
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
L27:
aload 0
getfield android/support/v7/internal/widget/t/a Landroid/support/v7/internal/widget/l;
invokestatic android/support/v7/internal/widget/l/c(Landroid/support/v7/internal/widget/l;)Z
pop
aload 1
ifnull L34
L28:
aload 1
invokevirtual java/io/FileOutputStream/close()V
L29:
aconst_null
areturn
L30:
astore 1
aconst_null
areturn
L8:
astore 4
aload 0
getfield android/support/v7/internal/widget/t/a Landroid/support/v7/internal/widget/l;
invokestatic android/support/v7/internal/widget/l/c(Landroid/support/v7/internal/widget/l;)Z
pop
aload 1
ifnull L32
L31:
aload 1
invokevirtual java/io/FileOutputStream/close()V
L32:
aload 4
athrow
L33:
astore 1
goto L32
.limit locals 7
.limit stack 5
.end method

.method public final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
aload 0
aload 1
invokespecial android/support/v7/internal/widget/t/a([Ljava/lang/Object;)Ljava/lang/Void;
areturn
.limit locals 2
.limit stack 2
.end method
