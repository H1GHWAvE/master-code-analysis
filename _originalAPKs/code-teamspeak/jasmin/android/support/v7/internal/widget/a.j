.bytecode 50.0
.class synchronized abstract android/support/v7/internal/widget/a
.super android/view/ViewGroup

.field private static final 'g' I = 200


.field protected final 'a' Landroid/support/v7/internal/widget/c;

.field protected final 'b' Landroid/content/Context;

.field protected 'c' Landroid/support/v7/widget/ActionMenuView;

.field protected 'd' Landroid/support/v7/widget/ActionMenuPresenter;

.field protected 'e' I

.field protected 'f' Landroid/support/v4/view/fk;

.field private 'h' Z

.field private 'i' Z

.method <init>(Landroid/content/Context;)V
aload 0
aload 1
aconst_null
invokespecial android/support/v7/internal/widget/a/<init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
return
.limit locals 2
.limit stack 3
.end method

.method <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
aload 0
aload 1
aload 2
iconst_0
invokespecial android/support/v7/internal/widget/a/<init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
return
.limit locals 3
.limit stack 4
.end method

.method <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
aload 0
aload 1
aload 2
iload 3
invokespecial android/view/ViewGroup/<init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
aload 0
new android/support/v7/internal/widget/c
dup
aload 0
invokespecial android/support/v7/internal/widget/c/<init>(Landroid/support/v7/internal/widget/a;)V
putfield android/support/v7/internal/widget/a/a Landroid/support/v7/internal/widget/c;
new android/util/TypedValue
dup
invokespecial android/util/TypedValue/<init>()V
astore 2
aload 1
invokevirtual android/content/Context/getTheme()Landroid/content/res/Resources$Theme;
getstatic android/support/v7/a/d/actionBarPopupTheme I
aload 2
iconst_1
invokevirtual android/content/res/Resources$Theme/resolveAttribute(ILandroid/util/TypedValue;Z)Z
ifeq L0
aload 2
getfield android/util/TypedValue/resourceId I
ifeq L0
aload 0
new android/view/ContextThemeWrapper
dup
aload 1
aload 2
getfield android/util/TypedValue/resourceId I
invokespecial android/view/ContextThemeWrapper/<init>(Landroid/content/Context;I)V
putfield android/support/v7/internal/widget/a/b Landroid/content/Context;
return
L0:
aload 0
aload 1
putfield android/support/v7/internal/widget/a/b Landroid/content/Context;
return
.limit locals 4
.limit stack 5
.end method

.method protected static a(IIZ)I
iload 2
ifeq L0
iload 0
iload 1
isub
ireturn
L0:
iload 0
iload 1
iadd
ireturn
.limit locals 3
.limit stack 2
.end method

.method protected static a(Landroid/view/View;II)I
aload 0
iload 1
ldc_w -2147483648
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
iload 2
invokevirtual android/view/View/measure(II)V
iconst_0
iload 1
aload 0
invokevirtual android/view/View/getMeasuredWidth()I
isub
iconst_0
iadd
invokestatic java/lang/Math/max(II)I
ireturn
.limit locals 3
.limit stack 3
.end method

.method protected static a(Landroid/view/View;IIIZ)I
aload 0
invokevirtual android/view/View/getMeasuredWidth()I
istore 5
aload 0
invokevirtual android/view/View/getMeasuredHeight()I
istore 6
iload 3
iload 6
isub
iconst_2
idiv
iload 2
iadd
istore 2
iload 4
ifeq L0
aload 0
iload 1
iload 5
isub
iload 2
iload 1
iload 6
iload 2
iadd
invokevirtual android/view/View/layout(IIII)V
L1:
iload 5
istore 1
iload 4
ifeq L2
iload 5
ineg
istore 1
L2:
iload 1
ireturn
L0:
aload 0
iload 1
iload 2
iload 1
iload 5
iadd
iload 6
iload 2
iadd
invokevirtual android/view/View/layout(IIII)V
goto L1
.limit locals 7
.limit stack 6
.end method

.method static synthetic a(Landroid/support/v7/internal/widget/a;)V
aload 0
iconst_0
invokespecial android/view/View/setVisibility(I)V
return
.limit locals 1
.limit stack 2
.end method

.method static synthetic a(Landroid/support/v7/internal/widget/a;I)V
aload 0
iload 1
invokespecial android/view/View/setVisibility(I)V
return
.limit locals 2
.limit stack 2
.end method

.method public a(IJ)Landroid/support/v4/view/fk;
aload 0
getfield android/support/v7/internal/widget/a/f Landroid/support/v4/view/fk;
ifnull L0
aload 0
getfield android/support/v7/internal/widget/a/f Landroid/support/v4/view/fk;
invokevirtual android/support/v4/view/fk/a()V
L0:
iload 1
ifne L1
aload 0
invokevirtual android/support/v7/internal/widget/a/getVisibility()I
ifeq L2
aload 0
fconst_0
invokestatic android/support/v4/view/cx/c(Landroid/view/View;F)V
L2:
aload 0
invokestatic android/support/v4/view/cx/p(Landroid/view/View;)Landroid/support/v4/view/fk;
fconst_1
invokevirtual android/support/v4/view/fk/a(F)Landroid/support/v4/view/fk;
astore 4
aload 4
lload 2
invokevirtual android/support/v4/view/fk/a(J)Landroid/support/v4/view/fk;
pop
aload 4
aload 0
getfield android/support/v7/internal/widget/a/a Landroid/support/v7/internal/widget/c;
aload 4
iload 1
invokevirtual android/support/v7/internal/widget/c/a(Landroid/support/v4/view/fk;I)Landroid/support/v7/internal/widget/c;
invokevirtual android/support/v4/view/fk/a(Landroid/support/v4/view/gd;)Landroid/support/v4/view/fk;
pop
aload 4
areturn
L1:
aload 0
invokestatic android/support/v4/view/cx/p(Landroid/view/View;)Landroid/support/v4/view/fk;
fconst_0
invokevirtual android/support/v4/view/fk/a(F)Landroid/support/v4/view/fk;
astore 4
aload 4
lload 2
invokevirtual android/support/v4/view/fk/a(J)Landroid/support/v4/view/fk;
pop
aload 4
aload 0
getfield android/support/v7/internal/widget/a/a Landroid/support/v7/internal/widget/c;
aload 4
iload 1
invokevirtual android/support/v7/internal/widget/c/a(Landroid/support/v4/view/fk;I)Landroid/support/v7/internal/widget/c;
invokevirtual android/support/v4/view/fk/a(Landroid/support/v4/view/gd;)Landroid/support/v4/view/fk;
pop
aload 4
areturn
.limit locals 5
.limit stack 4
.end method

.method public a(I)V
aload 0
iload 1
ldc2_w 200L
invokevirtual android/support/v7/internal/widget/a/a(IJ)Landroid/support/v4/view/fk;
invokevirtual android/support/v4/view/fk/b()V
return
.limit locals 2
.limit stack 4
.end method

.method public a()Z
aload 0
getfield android/support/v7/internal/widget/a/d Landroid/support/v7/widget/ActionMenuPresenter;
ifnull L0
aload 0
getfield android/support/v7/internal/widget/a/d Landroid/support/v7/widget/ActionMenuPresenter;
invokevirtual android/support/v7/widget/ActionMenuPresenter/e()Z
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public b()V
aload 0
new android/support/v7/internal/widget/b
dup
aload 0
invokespecial android/support/v7/internal/widget/b/<init>(Landroid/support/v7/internal/widget/a;)V
invokevirtual android/support/v7/internal/widget/a/post(Ljava/lang/Runnable;)Z
pop
return
.limit locals 1
.limit stack 4
.end method

.method public c()Z
aload 0
getfield android/support/v7/internal/widget/a/d Landroid/support/v7/widget/ActionMenuPresenter;
ifnull L0
aload 0
getfield android/support/v7/internal/widget/a/d Landroid/support/v7/widget/ActionMenuPresenter;
invokevirtual android/support/v7/widget/ActionMenuPresenter/f()Z
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public d()Z
aload 0
getfield android/support/v7/internal/widget/a/d Landroid/support/v7/widget/ActionMenuPresenter;
ifnull L0
aload 0
getfield android/support/v7/internal/widget/a/d Landroid/support/v7/widget/ActionMenuPresenter;
invokevirtual android/support/v7/widget/ActionMenuPresenter/i()Z
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public e()Z
aload 0
getfield android/support/v7/internal/widget/a/d Landroid/support/v7/widget/ActionMenuPresenter;
ifnull L0
aload 0
getfield android/support/v7/internal/widget/a/d Landroid/support/v7/widget/ActionMenuPresenter;
invokevirtual android/support/v7/widget/ActionMenuPresenter/j()Z
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public f()Z
aload 0
getfield android/support/v7/internal/widget/a/d Landroid/support/v7/widget/ActionMenuPresenter;
ifnull L0
aload 0
getfield android/support/v7/internal/widget/a/d Landroid/support/v7/widget/ActionMenuPresenter;
getfield android/support/v7/widget/ActionMenuPresenter/l Z
ifeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public g()Z
aload 0
invokevirtual android/support/v7/internal/widget/a/f()Z
ifeq L0
aload 0
invokevirtual android/support/v7/internal/widget/a/getVisibility()I
ifne L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getAnimatedVisibility()I
aload 0
getfield android/support/v7/internal/widget/a/f Landroid/support/v4/view/fk;
ifnull L0
aload 0
getfield android/support/v7/internal/widget/a/a Landroid/support/v7/internal/widget/c;
getfield android/support/v7/internal/widget/c/a I
ireturn
L0:
aload 0
invokevirtual android/support/v7/internal/widget/a/getVisibility()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getContentHeight()I
aload 0
getfield android/support/v7/internal/widget/a/e I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public h()V
aload 0
getfield android/support/v7/internal/widget/a/d Landroid/support/v7/widget/ActionMenuPresenter;
ifnull L0
aload 0
getfield android/support/v7/internal/widget/a/d Landroid/support/v7/widget/ActionMenuPresenter;
invokevirtual android/support/v7/widget/ActionMenuPresenter/g()Z
pop
L0:
return
.limit locals 1
.limit stack 1
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
getstatic android/os/Build$VERSION/SDK_INT I
bipush 8
if_icmplt L0
aload 0
aload 1
invokespecial android/view/ViewGroup/onConfigurationChanged(Landroid/content/res/Configuration;)V
L0:
aload 0
invokevirtual android/support/v7/internal/widget/a/getContext()Landroid/content/Context;
aconst_null
getstatic android/support/v7/a/n/ActionBar [I
getstatic android/support/v7/a/d/actionBarStyle I
iconst_0
invokevirtual android/content/Context/obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;
astore 1
aload 0
aload 1
getstatic android/support/v7/a/n/ActionBar_height I
iconst_0
invokevirtual android/content/res/TypedArray/getLayoutDimension(II)I
invokevirtual android/support/v7/internal/widget/a/setContentHeight(I)V
aload 1
invokevirtual android/content/res/TypedArray/recycle()V
aload 0
getfield android/support/v7/internal/widget/a/d Landroid/support/v7/widget/ActionMenuPresenter;
ifnull L1
aload 0
getfield android/support/v7/internal/widget/a/d Landroid/support/v7/widget/ActionMenuPresenter;
astore 1
aload 1
getfield android/support/v7/widget/ActionMenuPresenter/n Z
ifne L2
aload 1
aload 1
getfield android/support/v7/widget/ActionMenuPresenter/b Landroid/content/Context;
invokevirtual android/content/Context/getResources()Landroid/content/res/Resources;
getstatic android/support/v7/a/j/abc_max_action_buttons I
invokevirtual android/content/res/Resources/getInteger(I)I
putfield android/support/v7/widget/ActionMenuPresenter/m I
L2:
aload 1
getfield android/support/v7/widget/ActionMenuPresenter/c Landroid/support/v7/internal/view/menu/i;
ifnull L1
aload 1
getfield android/support/v7/widget/ActionMenuPresenter/c Landroid/support/v7/internal/view/menu/i;
iconst_1
invokevirtual android/support/v7/internal/view/menu/i/c(Z)V
L1:
return
.limit locals 2
.limit stack 5
.end method

.method public onHoverEvent(Landroid/view/MotionEvent;)Z
aload 1
invokestatic android/support/v4/view/bk/a(Landroid/view/MotionEvent;)I
istore 2
iload 2
bipush 9
if_icmpne L0
aload 0
iconst_0
putfield android/support/v7/internal/widget/a/i Z
L0:
aload 0
getfield android/support/v7/internal/widget/a/i Z
ifne L1
aload 0
aload 1
invokespecial android/view/ViewGroup/onHoverEvent(Landroid/view/MotionEvent;)Z
istore 3
iload 2
bipush 9
if_icmpne L1
iload 3
ifne L1
aload 0
iconst_1
putfield android/support/v7/internal/widget/a/i Z
L1:
iload 2
bipush 10
if_icmpeq L2
iload 2
iconst_3
if_icmpne L3
L2:
aload 0
iconst_0
putfield android/support/v7/internal/widget/a/i Z
L3:
iconst_1
ireturn
.limit locals 4
.limit stack 2
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
aload 1
invokestatic android/support/v4/view/bk/a(Landroid/view/MotionEvent;)I
istore 2
iload 2
ifne L0
aload 0
iconst_0
putfield android/support/v7/internal/widget/a/h Z
L0:
aload 0
getfield android/support/v7/internal/widget/a/h Z
ifne L1
aload 0
aload 1
invokespecial android/view/ViewGroup/onTouchEvent(Landroid/view/MotionEvent;)Z
istore 3
iload 2
ifne L1
iload 3
ifne L1
aload 0
iconst_1
putfield android/support/v7/internal/widget/a/h Z
L1:
iload 2
iconst_1
if_icmpeq L2
iload 2
iconst_3
if_icmpne L3
L2:
aload 0
iconst_0
putfield android/support/v7/internal/widget/a/h Z
L3:
iconst_1
ireturn
.limit locals 4
.limit stack 2
.end method

.method public setContentHeight(I)V
aload 0
iload 1
putfield android/support/v7/internal/widget/a/e I
aload 0
invokevirtual android/support/v7/internal/widget/a/requestLayout()V
return
.limit locals 2
.limit stack 2
.end method

.method public setVisibility(I)V
iload 1
aload 0
invokevirtual android/support/v7/internal/widget/a/getVisibility()I
if_icmpeq L0
aload 0
getfield android/support/v7/internal/widget/a/f Landroid/support/v4/view/fk;
ifnull L1
aload 0
getfield android/support/v7/internal/widget/a/f Landroid/support/v4/view/fk;
invokevirtual android/support/v4/view/fk/a()V
L1:
aload 0
iload 1
invokespecial android/view/ViewGroup/setVisibility(I)V
L0:
return
.limit locals 2
.limit stack 2
.end method
