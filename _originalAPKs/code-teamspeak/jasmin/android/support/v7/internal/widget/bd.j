.bytecode 50.0
.class public final synchronized android/support/v7/internal/widget/bd
.super java/lang/Object

.field private static final 'a' Ljava/lang/String; = "ViewUtils"

.field private static 'b' Ljava/lang/reflect/Method;

.method static <clinit>()V
.catch java/lang/NoSuchMethodException from L0 to L1 using L2
getstatic android/os/Build$VERSION/SDK_INT I
bipush 18
if_icmplt L1
L0:
ldc android/view/View
ldc "computeFitSystemWindows"
iconst_2
anewarray java/lang/Class
dup
iconst_0
ldc android/graphics/Rect
aastore
dup
iconst_1
ldc android/graphics/Rect
aastore
invokevirtual java/lang/Class/getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
astore 0
aload 0
putstatic android/support/v7/internal/widget/bd/b Ljava/lang/reflect/Method;
aload 0
invokevirtual java/lang/reflect/Method/isAccessible()Z
ifne L1
getstatic android/support/v7/internal/widget/bd/b Ljava/lang/reflect/Method;
iconst_1
invokevirtual java/lang/reflect/Method/setAccessible(Z)V
L1:
return
L2:
astore 0
ldc "ViewUtils"
ldc "Could not find method computeFitSystemWindows. Oh well."
invokestatic android/util/Log/d(Ljava/lang/String;Ljava/lang/String;)I
pop
return
.limit locals 1
.limit stack 6
.end method

.method private <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static a(II)I
iload 0
iload 1
ior
ireturn
.limit locals 2
.limit stack 2
.end method

.method public static a(Landroid/view/View;Landroid/graphics/Rect;Landroid/graphics/Rect;)V
.catch java/lang/Exception from L0 to L1 using L2
getstatic android/support/v7/internal/widget/bd/b Ljava/lang/reflect/Method;
ifnull L1
L0:
getstatic android/support/v7/internal/widget/bd/b Ljava/lang/reflect/Method;
aload 0
iconst_2
anewarray java/lang/Object
dup
iconst_0
aload 1
aastore
dup
iconst_1
aload 2
aastore
invokevirtual java/lang/reflect/Method/invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
pop
L1:
return
L2:
astore 0
ldc "ViewUtils"
ldc "Could not invoke computeFitSystemWindows"
aload 0
invokestatic android/util/Log/d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
return
.limit locals 3
.limit stack 6
.end method

.method public static a(Landroid/view/View;)Z
aload 0
invokestatic android/support/v4/view/cx/f(Landroid/view/View;)I
iconst_1
if_icmpne L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method public static b(Landroid/view/View;)V
.catch java/lang/NoSuchMethodException from L0 to L1 using L2
.catch java/lang/reflect/InvocationTargetException from L0 to L1 using L3
.catch java/lang/IllegalAccessException from L0 to L1 using L4
.catch java/lang/NoSuchMethodException from L1 to L5 using L2
.catch java/lang/reflect/InvocationTargetException from L1 to L5 using L3
.catch java/lang/IllegalAccessException from L1 to L5 using L4
getstatic android/os/Build$VERSION/SDK_INT I
bipush 16
if_icmplt L5
L0:
aload 0
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
ldc "makeOptionalFitsSystemWindows"
iconst_0
anewarray java/lang/Class
invokevirtual java/lang/Class/getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
astore 1
aload 1
invokevirtual java/lang/reflect/Method/isAccessible()Z
ifne L1
aload 1
iconst_1
invokevirtual java/lang/reflect/Method/setAccessible(Z)V
L1:
aload 1
aload 0
iconst_0
anewarray java/lang/Object
invokevirtual java/lang/reflect/Method/invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
pop
L5:
return
L2:
astore 0
ldc "ViewUtils"
ldc "Could not find method makeOptionalFitsSystemWindows. Oh well..."
invokestatic android/util/Log/d(Ljava/lang/String;Ljava/lang/String;)I
pop
return
L3:
astore 0
ldc "ViewUtils"
ldc "Could not invoke makeOptionalFitsSystemWindows"
aload 0
invokestatic android/util/Log/d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
return
L4:
astore 0
ldc "ViewUtils"
ldc "Could not invoke makeOptionalFitsSystemWindows"
aload 0
invokestatic android/util/Log/d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
return
.limit locals 2
.limit stack 3
.end method
