.bytecode 50.0
.class public final synchronized android/support/v7/internal/widget/ActivityChooserView
.super android/view/ViewGroup
.implements android/support/v7/internal/widget/n

.field private static final 'b' Ljava/lang/String; = "ActivityChooserView"

.field 'a' Landroid/support/v4/view/n;

.field private final 'c' Landroid/support/v7/internal/widget/y;

.field private final 'd' Landroid/support/v7/internal/widget/z;

.field private final 'e' Landroid/support/v7/widget/aj;

.field private final 'f' Landroid/graphics/drawable/Drawable;

.field private final 'g' Landroid/widget/FrameLayout;

.field private final 'h' Landroid/widget/ImageView;

.field private final 'i' Landroid/widget/FrameLayout;

.field private final 'j' Landroid/widget/ImageView;

.field private final 'k' I

.field private final 'l' Landroid/database/DataSetObserver;

.field private final 'm' Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

.field private 'n' Landroid/support/v7/widget/an;

.field private 'o' Landroid/widget/PopupWindow$OnDismissListener;

.field private 'p' Z

.field private 'q' I

.field private 'r' Z

.field private 's' I

.method public <init>(Landroid/content/Context;)V
aload 0
aload 1
iconst_0
invokespecial android/support/v7/internal/widget/ActivityChooserView/<init>(Landroid/content/Context;B)V
return
.limit locals 2
.limit stack 3
.end method

.method private <init>(Landroid/content/Context;B)V
aload 0
aload 1
iconst_0
invokespecial android/support/v7/internal/widget/ActivityChooserView/<init>(Landroid/content/Context;C)V
return
.limit locals 3
.limit stack 3
.end method

.method private <init>(Landroid/content/Context;C)V
aload 0
aload 1
aconst_null
iconst_0
invokespecial android/view/ViewGroup/<init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
aload 0
new android/support/v7/internal/widget/u
dup
aload 0
invokespecial android/support/v7/internal/widget/u/<init>(Landroid/support/v7/internal/widget/ActivityChooserView;)V
putfield android/support/v7/internal/widget/ActivityChooserView/l Landroid/database/DataSetObserver;
aload 0
new android/support/v7/internal/widget/v
dup
aload 0
invokespecial android/support/v7/internal/widget/v/<init>(Landroid/support/v7/internal/widget/ActivityChooserView;)V
putfield android/support/v7/internal/widget/ActivityChooserView/m Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;
aload 0
iconst_4
putfield android/support/v7/internal/widget/ActivityChooserView/q I
aload 1
aconst_null
getstatic android/support/v7/a/n/ActivityChooserView [I
iconst_0
iconst_0
invokevirtual android/content/Context/obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;
astore 4
aload 0
aload 4
getstatic android/support/v7/a/n/ActivityChooserView_initialActivityCount I
iconst_4
invokevirtual android/content/res/TypedArray/getInt(II)I
putfield android/support/v7/internal/widget/ActivityChooserView/q I
aload 4
getstatic android/support/v7/a/n/ActivityChooserView_expandActivityOverflowButtonDrawable I
invokevirtual android/content/res/TypedArray/getDrawable(I)Landroid/graphics/drawable/Drawable;
astore 3
aload 4
invokevirtual android/content/res/TypedArray/recycle()V
aload 0
invokevirtual android/support/v7/internal/widget/ActivityChooserView/getContext()Landroid/content/Context;
invokestatic android/view/LayoutInflater/from(Landroid/content/Context;)Landroid/view/LayoutInflater;
getstatic android/support/v7/a/k/abc_activity_chooser_view I
aload 0
iconst_1
invokevirtual android/view/LayoutInflater/inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;
pop
aload 0
new android/support/v7/internal/widget/z
dup
aload 0
iconst_0
invokespecial android/support/v7/internal/widget/z/<init>(Landroid/support/v7/internal/widget/ActivityChooserView;B)V
putfield android/support/v7/internal/widget/ActivityChooserView/d Landroid/support/v7/internal/widget/z;
aload 0
aload 0
getstatic android/support/v7/a/i/activity_chooser_view_content I
invokevirtual android/support/v7/internal/widget/ActivityChooserView/findViewById(I)Landroid/view/View;
checkcast android/support/v7/widget/aj
putfield android/support/v7/internal/widget/ActivityChooserView/e Landroid/support/v7/widget/aj;
aload 0
aload 0
getfield android/support/v7/internal/widget/ActivityChooserView/e Landroid/support/v7/widget/aj;
invokevirtual android/support/v7/widget/aj/getBackground()Landroid/graphics/drawable/Drawable;
putfield android/support/v7/internal/widget/ActivityChooserView/f Landroid/graphics/drawable/Drawable;
aload 0
aload 0
getstatic android/support/v7/a/i/default_activity_button I
invokevirtual android/support/v7/internal/widget/ActivityChooserView/findViewById(I)Landroid/view/View;
checkcast android/widget/FrameLayout
putfield android/support/v7/internal/widget/ActivityChooserView/i Landroid/widget/FrameLayout;
aload 0
getfield android/support/v7/internal/widget/ActivityChooserView/i Landroid/widget/FrameLayout;
aload 0
getfield android/support/v7/internal/widget/ActivityChooserView/d Landroid/support/v7/internal/widget/z;
invokevirtual android/widget/FrameLayout/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 0
getfield android/support/v7/internal/widget/ActivityChooserView/i Landroid/widget/FrameLayout;
aload 0
getfield android/support/v7/internal/widget/ActivityChooserView/d Landroid/support/v7/internal/widget/z;
invokevirtual android/widget/FrameLayout/setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V
aload 0
aload 0
getfield android/support/v7/internal/widget/ActivityChooserView/i Landroid/widget/FrameLayout;
getstatic android/support/v7/a/i/image I
invokevirtual android/widget/FrameLayout/findViewById(I)Landroid/view/View;
checkcast android/widget/ImageView
putfield android/support/v7/internal/widget/ActivityChooserView/j Landroid/widget/ImageView;
aload 0
getstatic android/support/v7/a/i/expand_activities_button I
invokevirtual android/support/v7/internal/widget/ActivityChooserView/findViewById(I)Landroid/view/View;
checkcast android/widget/FrameLayout
astore 4
aload 4
aload 0
getfield android/support/v7/internal/widget/ActivityChooserView/d Landroid/support/v7/internal/widget/z;
invokevirtual android/widget/FrameLayout/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 4
new android/support/v7/internal/widget/w
dup
aload 0
aload 4
invokespecial android/support/v7/internal/widget/w/<init>(Landroid/support/v7/internal/widget/ActivityChooserView;Landroid/view/View;)V
invokevirtual android/widget/FrameLayout/setOnTouchListener(Landroid/view/View$OnTouchListener;)V
aload 0
aload 4
putfield android/support/v7/internal/widget/ActivityChooserView/g Landroid/widget/FrameLayout;
aload 0
aload 4
getstatic android/support/v7/a/i/image I
invokevirtual android/widget/FrameLayout/findViewById(I)Landroid/view/View;
checkcast android/widget/ImageView
putfield android/support/v7/internal/widget/ActivityChooserView/h Landroid/widget/ImageView;
aload 0
getfield android/support/v7/internal/widget/ActivityChooserView/h Landroid/widget/ImageView;
aload 3
invokevirtual android/widget/ImageView/setImageDrawable(Landroid/graphics/drawable/Drawable;)V
aload 0
new android/support/v7/internal/widget/y
dup
aload 0
iconst_0
invokespecial android/support/v7/internal/widget/y/<init>(Landroid/support/v7/internal/widget/ActivityChooserView;B)V
putfield android/support/v7/internal/widget/ActivityChooserView/c Landroid/support/v7/internal/widget/y;
aload 0
getfield android/support/v7/internal/widget/ActivityChooserView/c Landroid/support/v7/internal/widget/y;
new android/support/v7/internal/widget/x
dup
aload 0
invokespecial android/support/v7/internal/widget/x/<init>(Landroid/support/v7/internal/widget/ActivityChooserView;)V
invokevirtual android/support/v7/internal/widget/y/registerDataSetObserver(Landroid/database/DataSetObserver;)V
aload 1
invokevirtual android/content/Context/getResources()Landroid/content/res/Resources;
astore 1
aload 0
aload 1
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
getfield android/util/DisplayMetrics/widthPixels I
iconst_2
idiv
aload 1
getstatic android/support/v7/a/g/abc_config_prefDialogWidth I
invokevirtual android/content/res/Resources/getDimensionPixelSize(I)I
invokestatic java/lang/Math/max(II)I
putfield android/support/v7/internal/widget/ActivityChooserView/k I
return
.limit locals 5
.limit stack 5
.end method

.method static synthetic a(Landroid/support/v7/internal/widget/ActivityChooserView;)Landroid/support/v7/internal/widget/y;
aload 0
getfield android/support/v7/internal/widget/ActivityChooserView/c Landroid/support/v7/internal/widget/y;
areturn
.limit locals 1
.limit stack 1
.end method

.method private a(I)V
aload 0
getfield android/support/v7/internal/widget/ActivityChooserView/c Landroid/support/v7/internal/widget/y;
getfield android/support/v7/internal/widget/y/c Landroid/support/v7/internal/widget/l;
ifnonnull L0
new java/lang/IllegalStateException
dup
ldc "No data model. Did you call #setDataModel?"
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
invokevirtual android/support/v7/internal/widget/ActivityChooserView/getViewTreeObserver()Landroid/view/ViewTreeObserver;
aload 0
getfield android/support/v7/internal/widget/ActivityChooserView/m Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;
invokevirtual android/view/ViewTreeObserver/addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V
aload 0
getfield android/support/v7/internal/widget/ActivityChooserView/i Landroid/widget/FrameLayout;
invokevirtual android/widget/FrameLayout/getVisibility()I
ifne L1
iconst_1
istore 4
L2:
aload 0
getfield android/support/v7/internal/widget/ActivityChooserView/c Landroid/support/v7/internal/widget/y;
getfield android/support/v7/internal/widget/y/c Landroid/support/v7/internal/widget/l;
invokevirtual android/support/v7/internal/widget/l/a()I
istore 3
iload 4
ifeq L3
iconst_1
istore 2
L4:
iload 1
ldc_w 2147483647
if_icmpeq L5
iload 3
iload 2
iload 1
iadd
if_icmple L5
aload 0
getfield android/support/v7/internal/widget/ActivityChooserView/c Landroid/support/v7/internal/widget/y;
iconst_1
invokevirtual android/support/v7/internal/widget/y/a(Z)V
aload 0
getfield android/support/v7/internal/widget/ActivityChooserView/c Landroid/support/v7/internal/widget/y;
iload 1
iconst_1
isub
invokevirtual android/support/v7/internal/widget/y/a(I)V
L6:
aload 0
invokespecial android/support/v7/internal/widget/ActivityChooserView/getListPopupWindow()Landroid/support/v7/widget/an;
astore 5
aload 5
getfield android/support/v7/widget/an/c Landroid/widget/PopupWindow;
invokevirtual android/widget/PopupWindow/isShowing()Z
ifne L7
aload 0
getfield android/support/v7/internal/widget/ActivityChooserView/p Z
ifne L8
iload 4
ifne L9
L8:
aload 0
getfield android/support/v7/internal/widget/ActivityChooserView/c Landroid/support/v7/internal/widget/y;
iconst_1
iload 4
invokevirtual android/support/v7/internal/widget/y/a(ZZ)V
L10:
aload 5
aload 0
getfield android/support/v7/internal/widget/ActivityChooserView/c Landroid/support/v7/internal/widget/y;
invokevirtual android/support/v7/internal/widget/y/a()I
aload 0
getfield android/support/v7/internal/widget/ActivityChooserView/k I
invokestatic java/lang/Math/min(II)I
invokevirtual android/support/v7/widget/an/a(I)V
aload 5
invokevirtual android/support/v7/widget/an/b()V
aload 0
getfield android/support/v7/internal/widget/ActivityChooserView/a Landroid/support/v4/view/n;
ifnull L11
aload 0
getfield android/support/v7/internal/widget/ActivityChooserView/a Landroid/support/v4/view/n;
iconst_1
invokevirtual android/support/v4/view/n/a(Z)V
L11:
aload 5
getfield android/support/v7/widget/an/d Landroid/support/v7/widget/ar;
aload 0
invokevirtual android/support/v7/internal/widget/ActivityChooserView/getContext()Landroid/content/Context;
getstatic android/support/v7/a/l/abc_activitychooserview_choose_application I
invokevirtual android/content/Context/getString(I)Ljava/lang/String;
invokevirtual android/widget/ListView/setContentDescription(Ljava/lang/CharSequence;)V
L7:
return
L1:
iconst_0
istore 4
goto L2
L3:
iconst_0
istore 2
goto L4
L5:
aload 0
getfield android/support/v7/internal/widget/ActivityChooserView/c Landroid/support/v7/internal/widget/y;
iconst_0
invokevirtual android/support/v7/internal/widget/y/a(Z)V
aload 0
getfield android/support/v7/internal/widget/ActivityChooserView/c Landroid/support/v7/internal/widget/y;
iload 1
invokevirtual android/support/v7/internal/widget/y/a(I)V
goto L6
L9:
aload 0
getfield android/support/v7/internal/widget/ActivityChooserView/c Landroid/support/v7/internal/widget/y;
iconst_0
iconst_0
invokevirtual android/support/v7/internal/widget/y/a(ZZ)V
goto L10
.limit locals 6
.limit stack 3
.end method

.method static synthetic a(Landroid/support/v7/internal/widget/ActivityChooserView;I)V
aload 0
iload 1
invokespecial android/support/v7/internal/widget/ActivityChooserView/a(I)V
return
.limit locals 2
.limit stack 2
.end method

.method static synthetic a(Landroid/support/v7/internal/widget/ActivityChooserView;Z)Z
aload 0
iload 1
putfield android/support/v7/internal/widget/ActivityChooserView/p Z
iload 1
ireturn
.limit locals 2
.limit stack 2
.end method

.method static synthetic b(Landroid/support/v7/internal/widget/ActivityChooserView;)Landroid/support/v7/widget/an;
aload 0
invokespecial android/support/v7/internal/widget/ActivityChooserView/getListPopupWindow()Landroid/support/v7/widget/an;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic c(Landroid/support/v7/internal/widget/ActivityChooserView;)V
aload 0
getfield android/support/v7/internal/widget/ActivityChooserView/c Landroid/support/v7/internal/widget/y;
invokevirtual android/support/v7/internal/widget/y/getCount()I
ifle L0
aload 0
getfield android/support/v7/internal/widget/ActivityChooserView/g Landroid/widget/FrameLayout;
iconst_1
invokevirtual android/widget/FrameLayout/setEnabled(Z)V
L1:
aload 0
getfield android/support/v7/internal/widget/ActivityChooserView/c Landroid/support/v7/internal/widget/y;
getfield android/support/v7/internal/widget/y/c Landroid/support/v7/internal/widget/l;
invokevirtual android/support/v7/internal/widget/l/a()I
istore 1
aload 0
getfield android/support/v7/internal/widget/ActivityChooserView/c Landroid/support/v7/internal/widget/y;
getfield android/support/v7/internal/widget/y/c Landroid/support/v7/internal/widget/l;
invokevirtual android/support/v7/internal/widget/l/c()I
istore 2
iload 1
iconst_1
if_icmpeq L2
iload 1
iconst_1
if_icmple L3
iload 2
ifle L3
L2:
aload 0
getfield android/support/v7/internal/widget/ActivityChooserView/i Landroid/widget/FrameLayout;
iconst_0
invokevirtual android/widget/FrameLayout/setVisibility(I)V
aload 0
getfield android/support/v7/internal/widget/ActivityChooserView/c Landroid/support/v7/internal/widget/y;
getfield android/support/v7/internal/widget/y/c Landroid/support/v7/internal/widget/l;
invokevirtual android/support/v7/internal/widget/l/b()Landroid/content/pm/ResolveInfo;
astore 3
aload 0
invokevirtual android/support/v7/internal/widget/ActivityChooserView/getContext()Landroid/content/Context;
invokevirtual android/content/Context/getPackageManager()Landroid/content/pm/PackageManager;
astore 4
aload 0
getfield android/support/v7/internal/widget/ActivityChooserView/j Landroid/widget/ImageView;
aload 3
aload 4
invokevirtual android/content/pm/ResolveInfo/loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;
invokevirtual android/widget/ImageView/setImageDrawable(Landroid/graphics/drawable/Drawable;)V
aload 0
getfield android/support/v7/internal/widget/ActivityChooserView/s I
ifeq L4
aload 3
aload 4
invokevirtual android/content/pm/ResolveInfo/loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;
astore 3
aload 0
invokevirtual android/support/v7/internal/widget/ActivityChooserView/getContext()Landroid/content/Context;
aload 0
getfield android/support/v7/internal/widget/ActivityChooserView/s I
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 3
aastore
invokevirtual android/content/Context/getString(I[Ljava/lang/Object;)Ljava/lang/String;
astore 3
aload 0
getfield android/support/v7/internal/widget/ActivityChooserView/i Landroid/widget/FrameLayout;
aload 3
invokevirtual android/widget/FrameLayout/setContentDescription(Ljava/lang/CharSequence;)V
L4:
aload 0
getfield android/support/v7/internal/widget/ActivityChooserView/i Landroid/widget/FrameLayout;
invokevirtual android/widget/FrameLayout/getVisibility()I
ifne L5
aload 0
getfield android/support/v7/internal/widget/ActivityChooserView/e Landroid/support/v7/widget/aj;
aload 0
getfield android/support/v7/internal/widget/ActivityChooserView/f Landroid/graphics/drawable/Drawable;
invokevirtual android/support/v7/widget/aj/setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
return
L0:
aload 0
getfield android/support/v7/internal/widget/ActivityChooserView/g Landroid/widget/FrameLayout;
iconst_0
invokevirtual android/widget/FrameLayout/setEnabled(Z)V
goto L1
L3:
aload 0
getfield android/support/v7/internal/widget/ActivityChooserView/i Landroid/widget/FrameLayout;
bipush 8
invokevirtual android/widget/FrameLayout/setVisibility(I)V
goto L4
L5:
aload 0
getfield android/support/v7/internal/widget/ActivityChooserView/e Landroid/support/v7/widget/aj;
aconst_null
invokevirtual android/support/v7/widget/aj/setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
return
.limit locals 5
.limit stack 6
.end method

.method private d()V
aload 0
getfield android/support/v7/internal/widget/ActivityChooserView/c Landroid/support/v7/internal/widget/y;
invokevirtual android/support/v7/internal/widget/y/getCount()I
ifle L0
aload 0
getfield android/support/v7/internal/widget/ActivityChooserView/g Landroid/widget/FrameLayout;
iconst_1
invokevirtual android/widget/FrameLayout/setEnabled(Z)V
L1:
aload 0
getfield android/support/v7/internal/widget/ActivityChooserView/c Landroid/support/v7/internal/widget/y;
getfield android/support/v7/internal/widget/y/c Landroid/support/v7/internal/widget/l;
invokevirtual android/support/v7/internal/widget/l/a()I
istore 1
aload 0
getfield android/support/v7/internal/widget/ActivityChooserView/c Landroid/support/v7/internal/widget/y;
getfield android/support/v7/internal/widget/y/c Landroid/support/v7/internal/widget/l;
invokevirtual android/support/v7/internal/widget/l/c()I
istore 2
iload 1
iconst_1
if_icmpeq L2
iload 1
iconst_1
if_icmple L3
iload 2
ifle L3
L2:
aload 0
getfield android/support/v7/internal/widget/ActivityChooserView/i Landroid/widget/FrameLayout;
iconst_0
invokevirtual android/widget/FrameLayout/setVisibility(I)V
aload 0
getfield android/support/v7/internal/widget/ActivityChooserView/c Landroid/support/v7/internal/widget/y;
getfield android/support/v7/internal/widget/y/c Landroid/support/v7/internal/widget/l;
invokevirtual android/support/v7/internal/widget/l/b()Landroid/content/pm/ResolveInfo;
astore 3
aload 0
invokevirtual android/support/v7/internal/widget/ActivityChooserView/getContext()Landroid/content/Context;
invokevirtual android/content/Context/getPackageManager()Landroid/content/pm/PackageManager;
astore 4
aload 0
getfield android/support/v7/internal/widget/ActivityChooserView/j Landroid/widget/ImageView;
aload 3
aload 4
invokevirtual android/content/pm/ResolveInfo/loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;
invokevirtual android/widget/ImageView/setImageDrawable(Landroid/graphics/drawable/Drawable;)V
aload 0
getfield android/support/v7/internal/widget/ActivityChooserView/s I
ifeq L4
aload 3
aload 4
invokevirtual android/content/pm/ResolveInfo/loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;
astore 3
aload 0
invokevirtual android/support/v7/internal/widget/ActivityChooserView/getContext()Landroid/content/Context;
aload 0
getfield android/support/v7/internal/widget/ActivityChooserView/s I
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 3
aastore
invokevirtual android/content/Context/getString(I[Ljava/lang/Object;)Ljava/lang/String;
astore 3
aload 0
getfield android/support/v7/internal/widget/ActivityChooserView/i Landroid/widget/FrameLayout;
aload 3
invokevirtual android/widget/FrameLayout/setContentDescription(Ljava/lang/CharSequence;)V
L4:
aload 0
getfield android/support/v7/internal/widget/ActivityChooserView/i Landroid/widget/FrameLayout;
invokevirtual android/widget/FrameLayout/getVisibility()I
ifne L5
aload 0
getfield android/support/v7/internal/widget/ActivityChooserView/e Landroid/support/v7/widget/aj;
aload 0
getfield android/support/v7/internal/widget/ActivityChooserView/f Landroid/graphics/drawable/Drawable;
invokevirtual android/support/v7/widget/aj/setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
return
L0:
aload 0
getfield android/support/v7/internal/widget/ActivityChooserView/g Landroid/widget/FrameLayout;
iconst_0
invokevirtual android/widget/FrameLayout/setEnabled(Z)V
goto L1
L3:
aload 0
getfield android/support/v7/internal/widget/ActivityChooserView/i Landroid/widget/FrameLayout;
bipush 8
invokevirtual android/widget/FrameLayout/setVisibility(I)V
goto L4
L5:
aload 0
getfield android/support/v7/internal/widget/ActivityChooserView/e Landroid/support/v7/widget/aj;
aconst_null
invokevirtual android/support/v7/widget/aj/setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
return
.limit locals 5
.limit stack 6
.end method

.method static synthetic d(Landroid/support/v7/internal/widget/ActivityChooserView;)Z
aload 0
getfield android/support/v7/internal/widget/ActivityChooserView/p Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic e(Landroid/support/v7/internal/widget/ActivityChooserView;)Landroid/widget/FrameLayout;
aload 0
getfield android/support/v7/internal/widget/ActivityChooserView/i Landroid/widget/FrameLayout;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic f(Landroid/support/v7/internal/widget/ActivityChooserView;)Landroid/widget/FrameLayout;
aload 0
getfield android/support/v7/internal/widget/ActivityChooserView/g Landroid/widget/FrameLayout;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic g(Landroid/support/v7/internal/widget/ActivityChooserView;)I
aload 0
getfield android/support/v7/internal/widget/ActivityChooserView/q I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private getListPopupWindow()Landroid/support/v7/widget/an;
aload 0
getfield android/support/v7/internal/widget/ActivityChooserView/n Landroid/support/v7/widget/an;
ifnonnull L0
aload 0
new android/support/v7/widget/an
dup
aload 0
invokevirtual android/support/v7/internal/widget/ActivityChooserView/getContext()Landroid/content/Context;
invokespecial android/support/v7/widget/an/<init>(Landroid/content/Context;)V
putfield android/support/v7/internal/widget/ActivityChooserView/n Landroid/support/v7/widget/an;
aload 0
getfield android/support/v7/internal/widget/ActivityChooserView/n Landroid/support/v7/widget/an;
aload 0
getfield android/support/v7/internal/widget/ActivityChooserView/c Landroid/support/v7/internal/widget/y;
invokevirtual android/support/v7/widget/an/a(Landroid/widget/ListAdapter;)V
aload 0
getfield android/support/v7/internal/widget/ActivityChooserView/n Landroid/support/v7/widget/an;
aload 0
putfield android/support/v7/widget/an/l Landroid/view/View;
aload 0
getfield android/support/v7/internal/widget/ActivityChooserView/n Landroid/support/v7/widget/an;
invokevirtual android/support/v7/widget/an/c()V
aload 0
getfield android/support/v7/internal/widget/ActivityChooserView/n Landroid/support/v7/widget/an;
aload 0
getfield android/support/v7/internal/widget/ActivityChooserView/d Landroid/support/v7/internal/widget/z;
putfield android/support/v7/widget/an/m Landroid/widget/AdapterView$OnItemClickListener;
aload 0
getfield android/support/v7/internal/widget/ActivityChooserView/n Landroid/support/v7/widget/an;
aload 0
getfield android/support/v7/internal/widget/ActivityChooserView/d Landroid/support/v7/internal/widget/z;
invokevirtual android/support/v7/widget/an/a(Landroid/widget/PopupWindow$OnDismissListener;)V
L0:
aload 0
getfield android/support/v7/internal/widget/ActivityChooserView/n Landroid/support/v7/widget/an;
areturn
.limit locals 1
.limit stack 4
.end method

.method static synthetic h(Landroid/support/v7/internal/widget/ActivityChooserView;)Landroid/widget/PopupWindow$OnDismissListener;
aload 0
getfield android/support/v7/internal/widget/ActivityChooserView/o Landroid/widget/PopupWindow$OnDismissListener;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic i(Landroid/support/v7/internal/widget/ActivityChooserView;)Landroid/database/DataSetObserver;
aload 0
getfield android/support/v7/internal/widget/ActivityChooserView/l Landroid/database/DataSetObserver;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final a()Z
aload 0
invokespecial android/support/v7/internal/widget/ActivityChooserView/getListPopupWindow()Landroid/support/v7/widget/an;
getfield android/support/v7/widget/an/c Landroid/widget/PopupWindow;
invokevirtual android/widget/PopupWindow/isShowing()Z
ifne L0
aload 0
getfield android/support/v7/internal/widget/ActivityChooserView/r Z
ifne L1
L0:
iconst_0
ireturn
L1:
aload 0
iconst_0
putfield android/support/v7/internal/widget/ActivityChooserView/p Z
aload 0
aload 0
getfield android/support/v7/internal/widget/ActivityChooserView/q I
invokespecial android/support/v7/internal/widget/ActivityChooserView/a(I)V
iconst_1
ireturn
.limit locals 1
.limit stack 2
.end method

.method public final b()Z
aload 0
invokespecial android/support/v7/internal/widget/ActivityChooserView/getListPopupWindow()Landroid/support/v7/widget/an;
getfield android/support/v7/widget/an/c Landroid/widget/PopupWindow;
invokevirtual android/widget/PopupWindow/isShowing()Z
ifeq L0
aload 0
invokespecial android/support/v7/internal/widget/ActivityChooserView/getListPopupWindow()Landroid/support/v7/widget/an;
invokevirtual android/support/v7/widget/an/d()V
aload 0
invokevirtual android/support/v7/internal/widget/ActivityChooserView/getViewTreeObserver()Landroid/view/ViewTreeObserver;
astore 1
aload 1
invokevirtual android/view/ViewTreeObserver/isAlive()Z
ifeq L0
aload 1
aload 0
getfield android/support/v7/internal/widget/ActivityChooserView/m Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;
invokevirtual android/view/ViewTreeObserver/removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V
L0:
iconst_1
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final c()Z
aload 0
invokespecial android/support/v7/internal/widget/ActivityChooserView/getListPopupWindow()Landroid/support/v7/widget/an;
getfield android/support/v7/widget/an/c Landroid/widget/PopupWindow;
invokevirtual android/widget/PopupWindow/isShowing()Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final getDataModel()Landroid/support/v7/internal/widget/l;
aload 0
getfield android/support/v7/internal/widget/ActivityChooserView/c Landroid/support/v7/internal/widget/y;
getfield android/support/v7/internal/widget/y/c Landroid/support/v7/internal/widget/l;
areturn
.limit locals 1
.limit stack 1
.end method

.method protected final onAttachedToWindow()V
aload 0
invokespecial android/view/ViewGroup/onAttachedToWindow()V
aload 0
getfield android/support/v7/internal/widget/ActivityChooserView/c Landroid/support/v7/internal/widget/y;
getfield android/support/v7/internal/widget/y/c Landroid/support/v7/internal/widget/l;
astore 1
aload 1
ifnull L0
aload 1
aload 0
getfield android/support/v7/internal/widget/ActivityChooserView/l Landroid/database/DataSetObserver;
invokevirtual android/support/v7/internal/widget/l/registerObserver(Ljava/lang/Object;)V
L0:
aload 0
iconst_1
putfield android/support/v7/internal/widget/ActivityChooserView/r Z
return
.limit locals 2
.limit stack 2
.end method

.method protected final onDetachedFromWindow()V
aload 0
invokespecial android/view/ViewGroup/onDetachedFromWindow()V
aload 0
getfield android/support/v7/internal/widget/ActivityChooserView/c Landroid/support/v7/internal/widget/y;
getfield android/support/v7/internal/widget/y/c Landroid/support/v7/internal/widget/l;
astore 1
aload 1
ifnull L0
aload 1
aload 0
getfield android/support/v7/internal/widget/ActivityChooserView/l Landroid/database/DataSetObserver;
invokevirtual android/support/v7/internal/widget/l/unregisterObserver(Ljava/lang/Object;)V
L0:
aload 0
invokevirtual android/support/v7/internal/widget/ActivityChooserView/getViewTreeObserver()Landroid/view/ViewTreeObserver;
astore 1
aload 1
invokevirtual android/view/ViewTreeObserver/isAlive()Z
ifeq L1
aload 1
aload 0
getfield android/support/v7/internal/widget/ActivityChooserView/m Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;
invokevirtual android/view/ViewTreeObserver/removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V
L1:
aload 0
invokevirtual android/support/v7/internal/widget/ActivityChooserView/c()Z
ifeq L2
aload 0
invokevirtual android/support/v7/internal/widget/ActivityChooserView/b()Z
pop
L2:
aload 0
iconst_0
putfield android/support/v7/internal/widget/ActivityChooserView/r Z
return
.limit locals 2
.limit stack 2
.end method

.method protected final onLayout(ZIIII)V
aload 0
getfield android/support/v7/internal/widget/ActivityChooserView/e Landroid/support/v7/widget/aj;
iconst_0
iconst_0
iload 4
iload 2
isub
iload 5
iload 3
isub
invokevirtual android/support/v7/widget/aj/layout(IIII)V
aload 0
invokevirtual android/support/v7/internal/widget/ActivityChooserView/c()Z
ifne L0
aload 0
invokevirtual android/support/v7/internal/widget/ActivityChooserView/b()Z
pop
L0:
return
.limit locals 6
.limit stack 6
.end method

.method protected final onMeasure(II)V
aload 0
getfield android/support/v7/internal/widget/ActivityChooserView/e Landroid/support/v7/widget/aj;
astore 4
iload 2
istore 3
aload 0
getfield android/support/v7/internal/widget/ActivityChooserView/i Landroid/widget/FrameLayout;
invokevirtual android/widget/FrameLayout/getVisibility()I
ifeq L0
iload 2
invokestatic android/view/View$MeasureSpec/getSize(I)I
ldc_w 1073741824
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
istore 3
L0:
aload 0
aload 4
iload 1
iload 3
invokevirtual android/support/v7/internal/widget/ActivityChooserView/measureChild(Landroid/view/View;II)V
aload 0
aload 4
invokevirtual android/view/View/getMeasuredWidth()I
aload 4
invokevirtual android/view/View/getMeasuredHeight()I
invokevirtual android/support/v7/internal/widget/ActivityChooserView/setMeasuredDimension(II)V
return
.limit locals 5
.limit stack 4
.end method

.method public final setActivityChooserModel(Landroid/support/v7/internal/widget/l;)V
aload 0
getfield android/support/v7/internal/widget/ActivityChooserView/c Landroid/support/v7/internal/widget/y;
astore 2
aload 2
getfield android/support/v7/internal/widget/y/e Landroid/support/v7/internal/widget/ActivityChooserView;
getfield android/support/v7/internal/widget/ActivityChooserView/c Landroid/support/v7/internal/widget/y;
getfield android/support/v7/internal/widget/y/c Landroid/support/v7/internal/widget/l;
astore 3
aload 3
ifnull L0
aload 2
getfield android/support/v7/internal/widget/y/e Landroid/support/v7/internal/widget/ActivityChooserView;
invokevirtual android/support/v7/internal/widget/ActivityChooserView/isShown()Z
ifeq L0
aload 3
aload 2
getfield android/support/v7/internal/widget/y/e Landroid/support/v7/internal/widget/ActivityChooserView;
getfield android/support/v7/internal/widget/ActivityChooserView/l Landroid/database/DataSetObserver;
invokevirtual android/support/v7/internal/widget/l/unregisterObserver(Ljava/lang/Object;)V
L0:
aload 2
aload 1
putfield android/support/v7/internal/widget/y/c Landroid/support/v7/internal/widget/l;
aload 1
ifnull L1
aload 2
getfield android/support/v7/internal/widget/y/e Landroid/support/v7/internal/widget/ActivityChooserView;
invokevirtual android/support/v7/internal/widget/ActivityChooserView/isShown()Z
ifeq L1
aload 1
aload 2
getfield android/support/v7/internal/widget/y/e Landroid/support/v7/internal/widget/ActivityChooserView;
getfield android/support/v7/internal/widget/ActivityChooserView/l Landroid/database/DataSetObserver;
invokevirtual android/support/v7/internal/widget/l/registerObserver(Ljava/lang/Object;)V
L1:
aload 2
invokevirtual android/support/v7/internal/widget/y/notifyDataSetChanged()V
aload 0
invokespecial android/support/v7/internal/widget/ActivityChooserView/getListPopupWindow()Landroid/support/v7/widget/an;
getfield android/support/v7/widget/an/c Landroid/widget/PopupWindow;
invokevirtual android/widget/PopupWindow/isShowing()Z
ifeq L2
aload 0
invokevirtual android/support/v7/internal/widget/ActivityChooserView/b()Z
pop
aload 0
invokevirtual android/support/v7/internal/widget/ActivityChooserView/a()Z
pop
L2:
return
.limit locals 4
.limit stack 2
.end method

.method public final setDefaultActionButtonContentDescription(I)V
aload 0
iload 1
putfield android/support/v7/internal/widget/ActivityChooserView/s I
return
.limit locals 2
.limit stack 2
.end method

.method public final setExpandActivityOverflowButtonContentDescription(I)V
aload 0
invokevirtual android/support/v7/internal/widget/ActivityChooserView/getContext()Landroid/content/Context;
iload 1
invokevirtual android/content/Context/getString(I)Ljava/lang/String;
astore 2
aload 0
getfield android/support/v7/internal/widget/ActivityChooserView/h Landroid/widget/ImageView;
aload 2
invokevirtual android/widget/ImageView/setContentDescription(Ljava/lang/CharSequence;)V
return
.limit locals 3
.limit stack 2
.end method

.method public final setExpandActivityOverflowButtonDrawable(Landroid/graphics/drawable/Drawable;)V
aload 0
getfield android/support/v7/internal/widget/ActivityChooserView/h Landroid/widget/ImageView;
aload 1
invokevirtual android/widget/ImageView/setImageDrawable(Landroid/graphics/drawable/Drawable;)V
return
.limit locals 2
.limit stack 2
.end method

.method public final setInitialActivityCount(I)V
aload 0
iload 1
putfield android/support/v7/internal/widget/ActivityChooserView/q I
return
.limit locals 2
.limit stack 2
.end method

.method public final setOnDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V
aload 0
aload 1
putfield android/support/v7/internal/widget/ActivityChooserView/o Landroid/widget/PopupWindow$OnDismissListener;
return
.limit locals 2
.limit stack 2
.end method

.method public final setProvider(Landroid/support/v4/view/n;)V
aload 0
aload 1
putfield android/support/v7/internal/widget/ActivityChooserView/a Landroid/support/v4/view/n;
return
.limit locals 2
.limit stack 2
.end method
