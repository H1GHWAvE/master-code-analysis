.bytecode 50.0
.class public final synchronized android/support/v7/internal/widget/ax
.super java/lang/Object

.field public final 'a' Landroid/content/res/TypedArray;

.field private final 'b' Landroid/content/Context;

.field private 'c' Landroid/support/v7/internal/widget/av;

.method private <init>(Landroid/content/Context;Landroid/content/res/TypedArray;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
putfield android/support/v7/internal/widget/ax/b Landroid/content/Context;
aload 0
aload 2
putfield android/support/v7/internal/widget/ax/a Landroid/content/res/TypedArray;
return
.limit locals 3
.limit stack 2
.end method

.method private a(IF)F
aload 0
getfield android/support/v7/internal/widget/ax/a Landroid/content/res/TypedArray;
iload 1
fload 2
invokevirtual android/content/res/TypedArray/getDimension(IF)F
freturn
.limit locals 3
.limit stack 3
.end method

.method private a(IIIF)F
aload 0
getfield android/support/v7/internal/widget/ax/a Landroid/content/res/TypedArray;
iload 1
iload 2
iload 3
fload 4
invokevirtual android/content/res/TypedArray/getFraction(IIIF)F
freturn
.limit locals 5
.limit stack 5
.end method

.method private a(ILjava/lang/String;)I
aload 0
getfield android/support/v7/internal/widget/ax/a Landroid/content/res/TypedArray;
iload 1
aload 2
invokevirtual android/content/res/TypedArray/getLayoutDimension(ILjava/lang/String;)I
ireturn
.limit locals 3
.limit stack 3
.end method

.method public static a(Landroid/content/Context;Landroid/util/AttributeSet;[I)Landroid/support/v7/internal/widget/ax;
new android/support/v7/internal/widget/ax
dup
aload 0
aload 0
aload 1
aload 2
invokevirtual android/content/Context/obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;
invokespecial android/support/v7/internal/widget/ax/<init>(Landroid/content/Context;Landroid/content/res/TypedArray;)V
areturn
.limit locals 3
.limit stack 6
.end method

.method public static a(Landroid/content/Context;Landroid/util/AttributeSet;[II)Landroid/support/v7/internal/widget/ax;
new android/support/v7/internal/widget/ax
dup
aload 0
aload 0
aload 1
aload 2
iload 3
iconst_0
invokevirtual android/content/Context/obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;
invokespecial android/support/v7/internal/widget/ax/<init>(Landroid/content/Context;Landroid/content/res/TypedArray;)V
areturn
.limit locals 4
.limit stack 8
.end method

.method private a(ILandroid/util/TypedValue;)Z
aload 0
getfield android/support/v7/internal/widget/ax/a Landroid/content/res/TypedArray;
iload 1
aload 2
invokevirtual android/content/res/TypedArray/getValue(ILandroid/util/TypedValue;)Z
ireturn
.limit locals 3
.limit stack 3
.end method

.method private b()I
aload 0
getfield android/support/v7/internal/widget/ax/a Landroid/content/res/TypedArray;
invokevirtual android/content/res/TypedArray/length()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private c()I
aload 0
getfield android/support/v7/internal/widget/ax/a Landroid/content/res/TypedArray;
invokevirtual android/content/res/TypedArray/getIndexCount()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private d()Landroid/content/res/Resources;
aload 0
getfield android/support/v7/internal/widget/ax/a Landroid/content/res/TypedArray;
invokevirtual android/content/res/TypedArray/getResources()Landroid/content/res/Resources;
areturn
.limit locals 1
.limit stack 1
.end method

.method private e()Ljava/lang/String;
aload 0
getfield android/support/v7/internal/widget/ax/a Landroid/content/res/TypedArray;
invokevirtual android/content/res/TypedArray/getPositionDescription()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method private f(I)I
aload 0
getfield android/support/v7/internal/widget/ax/a Landroid/content/res/TypedArray;
iload 1
invokevirtual android/content/res/TypedArray/getIndex(I)I
ireturn
.limit locals 2
.limit stack 2
.end method

.method private f(II)I
aload 0
getfield android/support/v7/internal/widget/ax/a Landroid/content/res/TypedArray;
iload 1
iload 2
invokevirtual android/content/res/TypedArray/getInteger(II)I
ireturn
.limit locals 3
.limit stack 3
.end method

.method private f()V
aload 0
getfield android/support/v7/internal/widget/ax/a Landroid/content/res/TypedArray;
invokevirtual android/content/res/TypedArray/recycle()V
return
.limit locals 1
.limit stack 1
.end method

.method private g()I
aload 0
getfield android/support/v7/internal/widget/ax/a Landroid/content/res/TypedArray;
invokevirtual android/content/res/TypedArray/getChangingConfigurations()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private g(I)Ljava/lang/String;
aload 0
getfield android/support/v7/internal/widget/ax/a Landroid/content/res/TypedArray;
iload 1
invokevirtual android/content/res/TypedArray/getString(I)Ljava/lang/String;
areturn
.limit locals 2
.limit stack 2
.end method

.method private h(I)Ljava/lang/String;
aload 0
getfield android/support/v7/internal/widget/ax/a Landroid/content/res/TypedArray;
iload 1
invokevirtual android/content/res/TypedArray/getNonResourceString(I)Ljava/lang/String;
areturn
.limit locals 2
.limit stack 2
.end method

.method private i(I)F
aload 0
getfield android/support/v7/internal/widget/ax/a Landroid/content/res/TypedArray;
iload 1
ldc_w -1.0F
invokevirtual android/content/res/TypedArray/getFloat(IF)F
freturn
.limit locals 2
.limit stack 3
.end method

.method private j(I)Landroid/content/res/ColorStateList;
aload 0
getfield android/support/v7/internal/widget/ax/a Landroid/content/res/TypedArray;
iload 1
invokevirtual android/content/res/TypedArray/getColorStateList(I)Landroid/content/res/ColorStateList;
areturn
.limit locals 2
.limit stack 2
.end method

.method private k(I)[Ljava/lang/CharSequence;
aload 0
getfield android/support/v7/internal/widget/ax/a Landroid/content/res/TypedArray;
iload 1
invokevirtual android/content/res/TypedArray/getTextArray(I)[Ljava/lang/CharSequence;
areturn
.limit locals 2
.limit stack 2
.end method

.method private l(I)I
aload 0
getfield android/support/v7/internal/widget/ax/a Landroid/content/res/TypedArray;
iload 1
invokevirtual android/content/res/TypedArray/getType(I)I
ireturn
.limit locals 2
.limit stack 2
.end method

.method private m(I)Landroid/util/TypedValue;
aload 0
getfield android/support/v7/internal/widget/ax/a Landroid/content/res/TypedArray;
iload 1
invokevirtual android/content/res/TypedArray/peekValue(I)Landroid/util/TypedValue;
areturn
.limit locals 2
.limit stack 2
.end method

.method public final a(II)I
aload 0
getfield android/support/v7/internal/widget/ax/a Landroid/content/res/TypedArray;
iload 1
iload 2
invokevirtual android/content/res/TypedArray/getInt(II)I
ireturn
.limit locals 3
.limit stack 3
.end method

.method public final a(I)Landroid/graphics/drawable/Drawable;
aload 0
getfield android/support/v7/internal/widget/ax/a Landroid/content/res/TypedArray;
iload 1
invokevirtual android/content/res/TypedArray/hasValue(I)Z
ifeq L0
aload 0
getfield android/support/v7/internal/widget/ax/a Landroid/content/res/TypedArray;
iload 1
iconst_0
invokevirtual android/content/res/TypedArray/getResourceId(II)I
istore 2
iload 2
ifeq L0
aload 0
invokevirtual android/support/v7/internal/widget/ax/a()Landroid/support/v7/internal/widget/av;
iload 2
iconst_0
invokevirtual android/support/v7/internal/widget/av/a(IZ)Landroid/graphics/drawable/Drawable;
areturn
L0:
aload 0
getfield android/support/v7/internal/widget/ax/a Landroid/content/res/TypedArray;
iload 1
invokevirtual android/content/res/TypedArray/getDrawable(I)Landroid/graphics/drawable/Drawable;
areturn
.limit locals 3
.limit stack 3
.end method

.method public final a()Landroid/support/v7/internal/widget/av;
aload 0
getfield android/support/v7/internal/widget/ax/c Landroid/support/v7/internal/widget/av;
ifnonnull L0
aload 0
aload 0
getfield android/support/v7/internal/widget/ax/b Landroid/content/Context;
invokestatic android/support/v7/internal/widget/av/a(Landroid/content/Context;)Landroid/support/v7/internal/widget/av;
putfield android/support/v7/internal/widget/ax/c Landroid/support/v7/internal/widget/av;
L0:
aload 0
getfield android/support/v7/internal/widget/ax/c Landroid/support/v7/internal/widget/av;
areturn
.limit locals 1
.limit stack 2
.end method

.method public final a(IZ)Z
aload 0
getfield android/support/v7/internal/widget/ax/a Landroid/content/res/TypedArray;
iload 1
iload 2
invokevirtual android/content/res/TypedArray/getBoolean(IZ)Z
ireturn
.limit locals 3
.limit stack 3
.end method

.method public final b(II)I
aload 0
getfield android/support/v7/internal/widget/ax/a Landroid/content/res/TypedArray;
iload 1
iload 2
invokevirtual android/content/res/TypedArray/getDimensionPixelOffset(II)I
ireturn
.limit locals 3
.limit stack 3
.end method

.method public final b(I)Landroid/graphics/drawable/Drawable;
aload 0
getfield android/support/v7/internal/widget/ax/a Landroid/content/res/TypedArray;
iload 1
invokevirtual android/content/res/TypedArray/hasValue(I)Z
ifeq L0
aload 0
getfield android/support/v7/internal/widget/ax/a Landroid/content/res/TypedArray;
iload 1
iconst_0
invokevirtual android/content/res/TypedArray/getResourceId(II)I
istore 1
iload 1
ifeq L0
aload 0
invokevirtual android/support/v7/internal/widget/ax/a()Landroid/support/v7/internal/widget/av;
iload 1
iconst_1
invokevirtual android/support/v7/internal/widget/av/a(IZ)Landroid/graphics/drawable/Drawable;
areturn
L0:
aconst_null
areturn
.limit locals 2
.limit stack 3
.end method

.method public final c(II)I
aload 0
getfield android/support/v7/internal/widget/ax/a Landroid/content/res/TypedArray;
iload 1
iload 2
invokevirtual android/content/res/TypedArray/getDimensionPixelSize(II)I
ireturn
.limit locals 3
.limit stack 3
.end method

.method public final c(I)Ljava/lang/CharSequence;
aload 0
getfield android/support/v7/internal/widget/ax/a Landroid/content/res/TypedArray;
iload 1
invokevirtual android/content/res/TypedArray/getText(I)Ljava/lang/CharSequence;
areturn
.limit locals 2
.limit stack 2
.end method

.method public final d(I)I
aload 0
getfield android/support/v7/internal/widget/ax/a Landroid/content/res/TypedArray;
iload 1
iconst_m1
invokevirtual android/content/res/TypedArray/getColor(II)I
ireturn
.limit locals 2
.limit stack 3
.end method

.method public final d(II)I
aload 0
getfield android/support/v7/internal/widget/ax/a Landroid/content/res/TypedArray;
iload 1
iload 2
invokevirtual android/content/res/TypedArray/getLayoutDimension(II)I
ireturn
.limit locals 3
.limit stack 3
.end method

.method public final e(II)I
aload 0
getfield android/support/v7/internal/widget/ax/a Landroid/content/res/TypedArray;
iload 1
iload 2
invokevirtual android/content/res/TypedArray/getResourceId(II)I
ireturn
.limit locals 3
.limit stack 3
.end method

.method public final e(I)Z
aload 0
getfield android/support/v7/internal/widget/ax/a Landroid/content/res/TypedArray;
iload 1
invokevirtual android/content/res/TypedArray/hasValue(I)Z
ireturn
.limit locals 2
.limit stack 2
.end method
