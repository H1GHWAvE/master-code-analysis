.bytecode 50.0
.class public synchronized android/support/v7/internal/widget/ActionBarContextView
.super android/support/v7/internal/widget/a

.field private static final 'i' Ljava/lang/String; = "ActionBarContextView"

.field public 'g' Landroid/view/View;

.field public 'h' Z

.field private 'j' Ljava/lang/CharSequence;

.field private 'k' Ljava/lang/CharSequence;

.field private 'l' Landroid/view/View;

.field private 'm' Landroid/widget/LinearLayout;

.field private 'n' Landroid/widget/TextView;

.field private 'o' Landroid/widget/TextView;

.field private 'p' I

.field private 'q' I

.field private 'r' I

.method public <init>(Landroid/content/Context;)V
aload 0
aload 1
aconst_null
invokespecial android/support/v7/internal/widget/ActionBarContextView/<init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
return
.limit locals 2
.limit stack 3
.end method

.method public <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
aload 0
aload 1
aload 2
getstatic android/support/v7/a/d/actionModeStyle I
invokespecial android/support/v7/internal/widget/ActionBarContextView/<init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
return
.limit locals 3
.limit stack 4
.end method

.method public <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
aload 0
aload 1
aload 2
iload 3
invokespecial android/support/v7/internal/widget/a/<init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
aload 1
aload 2
getstatic android/support/v7/a/n/ActionMode [I
iload 3
invokestatic android/support/v7/internal/widget/ax/a(Landroid/content/Context;Landroid/util/AttributeSet;[II)Landroid/support/v7/internal/widget/ax;
astore 1
aload 0
aload 1
getstatic android/support/v7/a/n/ActionMode_background I
invokevirtual android/support/v7/internal/widget/ax/a(I)Landroid/graphics/drawable/Drawable;
invokevirtual android/support/v7/internal/widget/ActionBarContextView/setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
aload 0
aload 1
getstatic android/support/v7/a/n/ActionMode_titleTextStyle I
iconst_0
invokevirtual android/support/v7/internal/widget/ax/e(II)I
putfield android/support/v7/internal/widget/ActionBarContextView/p I
aload 0
aload 1
getstatic android/support/v7/a/n/ActionMode_subtitleTextStyle I
iconst_0
invokevirtual android/support/v7/internal/widget/ax/e(II)I
putfield android/support/v7/internal/widget/ActionBarContextView/q I
aload 0
aload 1
getstatic android/support/v7/a/n/ActionMode_height I
iconst_0
invokevirtual android/support/v7/internal/widget/ax/d(II)I
putfield android/support/v7/internal/widget/ActionBarContextView/e I
aload 0
aload 1
getstatic android/support/v7/a/n/ActionMode_closeItemLayout I
getstatic android/support/v7/a/k/abc_action_mode_close_item_material I
invokevirtual android/support/v7/internal/widget/ax/e(II)I
putfield android/support/v7/internal/widget/ActionBarContextView/r I
aload 1
getfield android/support/v7/internal/widget/ax/a Landroid/content/res/TypedArray;
invokevirtual android/content/res/TypedArray/recycle()V
return
.limit locals 4
.limit stack 4
.end method

.method private j()V
bipush 8
istore 4
iconst_1
istore 2
aload 0
getfield android/support/v7/internal/widget/ActionBarContextView/m Landroid/widget/LinearLayout;
ifnonnull L0
aload 0
invokevirtual android/support/v7/internal/widget/ActionBarContextView/getContext()Landroid/content/Context;
invokestatic android/view/LayoutInflater/from(Landroid/content/Context;)Landroid/view/LayoutInflater;
getstatic android/support/v7/a/k/abc_action_bar_title_item I
aload 0
invokevirtual android/view/LayoutInflater/inflate(ILandroid/view/ViewGroup;)Landroid/view/View;
pop
aload 0
aload 0
aload 0
invokevirtual android/support/v7/internal/widget/ActionBarContextView/getChildCount()I
iconst_1
isub
invokevirtual android/support/v7/internal/widget/ActionBarContextView/getChildAt(I)Landroid/view/View;
checkcast android/widget/LinearLayout
putfield android/support/v7/internal/widget/ActionBarContextView/m Landroid/widget/LinearLayout;
aload 0
aload 0
getfield android/support/v7/internal/widget/ActionBarContextView/m Landroid/widget/LinearLayout;
getstatic android/support/v7/a/i/action_bar_title I
invokevirtual android/widget/LinearLayout/findViewById(I)Landroid/view/View;
checkcast android/widget/TextView
putfield android/support/v7/internal/widget/ActionBarContextView/n Landroid/widget/TextView;
aload 0
aload 0
getfield android/support/v7/internal/widget/ActionBarContextView/m Landroid/widget/LinearLayout;
getstatic android/support/v7/a/i/action_bar_subtitle I
invokevirtual android/widget/LinearLayout/findViewById(I)Landroid/view/View;
checkcast android/widget/TextView
putfield android/support/v7/internal/widget/ActionBarContextView/o Landroid/widget/TextView;
aload 0
getfield android/support/v7/internal/widget/ActionBarContextView/p I
ifeq L1
aload 0
getfield android/support/v7/internal/widget/ActionBarContextView/n Landroid/widget/TextView;
aload 0
invokevirtual android/support/v7/internal/widget/ActionBarContextView/getContext()Landroid/content/Context;
aload 0
getfield android/support/v7/internal/widget/ActionBarContextView/p I
invokevirtual android/widget/TextView/setTextAppearance(Landroid/content/Context;I)V
L1:
aload 0
getfield android/support/v7/internal/widget/ActionBarContextView/q I
ifeq L0
aload 0
getfield android/support/v7/internal/widget/ActionBarContextView/o Landroid/widget/TextView;
aload 0
invokevirtual android/support/v7/internal/widget/ActionBarContextView/getContext()Landroid/content/Context;
aload 0
getfield android/support/v7/internal/widget/ActionBarContextView/q I
invokevirtual android/widget/TextView/setTextAppearance(Landroid/content/Context;I)V
L0:
aload 0
getfield android/support/v7/internal/widget/ActionBarContextView/n Landroid/widget/TextView;
aload 0
getfield android/support/v7/internal/widget/ActionBarContextView/j Ljava/lang/CharSequence;
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
aload 0
getfield android/support/v7/internal/widget/ActionBarContextView/o Landroid/widget/TextView;
aload 0
getfield android/support/v7/internal/widget/ActionBarContextView/k Ljava/lang/CharSequence;
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
aload 0
getfield android/support/v7/internal/widget/ActionBarContextView/j Ljava/lang/CharSequence;
invokestatic android/text/TextUtils/isEmpty(Ljava/lang/CharSequence;)Z
ifne L2
iconst_1
istore 1
L3:
aload 0
getfield android/support/v7/internal/widget/ActionBarContextView/k Ljava/lang/CharSequence;
invokestatic android/text/TextUtils/isEmpty(Ljava/lang/CharSequence;)Z
ifne L4
L5:
aload 0
getfield android/support/v7/internal/widget/ActionBarContextView/o Landroid/widget/TextView;
astore 5
iload 2
ifeq L6
iconst_0
istore 3
L7:
aload 5
iload 3
invokevirtual android/widget/TextView/setVisibility(I)V
aload 0
getfield android/support/v7/internal/widget/ActionBarContextView/m Landroid/widget/LinearLayout;
astore 5
iload 1
ifne L8
iload 4
istore 1
iload 2
ifeq L9
L8:
iconst_0
istore 1
L9:
aload 5
iload 1
invokevirtual android/widget/LinearLayout/setVisibility(I)V
aload 0
getfield android/support/v7/internal/widget/ActionBarContextView/m Landroid/widget/LinearLayout;
invokevirtual android/widget/LinearLayout/getParent()Landroid/view/ViewParent;
ifnonnull L10
aload 0
aload 0
getfield android/support/v7/internal/widget/ActionBarContextView/m Landroid/widget/LinearLayout;
invokevirtual android/support/v7/internal/widget/ActionBarContextView/addView(Landroid/view/View;)V
L10:
return
L2:
iconst_0
istore 1
goto L3
L4:
iconst_0
istore 2
goto L5
L6:
bipush 8
istore 3
goto L7
.limit locals 6
.limit stack 4
.end method

.method private k()V
aload 0
getfield android/support/v7/internal/widget/ActionBarContextView/g Landroid/view/View;
ifnonnull L0
aload 0
invokevirtual android/support/v7/internal/widget/ActionBarContextView/i()V
L0:
return
.limit locals 1
.limit stack 1
.end method

.method private l()Z
aload 0
getfield android/support/v7/internal/widget/ActionBarContextView/h Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final volatile synthetic a(IJ)Landroid/support/v4/view/fk;
aload 0
iload 1
lload 2
invokespecial android/support/v7/internal/widget/a/a(IJ)Landroid/support/v4/view/fk;
areturn
.limit locals 4
.limit stack 4
.end method

.method public final volatile synthetic a(I)V
aload 0
iload 1
invokespecial android/support/v7/internal/widget/a/a(I)V
return
.limit locals 2
.limit stack 2
.end method

.method public final a(Landroid/support/v7/c/a;)V
aload 0
getfield android/support/v7/internal/widget/ActionBarContextView/g Landroid/view/View;
ifnonnull L0
aload 0
aload 0
invokevirtual android/support/v7/internal/widget/ActionBarContextView/getContext()Landroid/content/Context;
invokestatic android/view/LayoutInflater/from(Landroid/content/Context;)Landroid/view/LayoutInflater;
aload 0
getfield android/support/v7/internal/widget/ActionBarContextView/r I
aload 0
iconst_0
invokevirtual android/view/LayoutInflater/inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;
putfield android/support/v7/internal/widget/ActionBarContextView/g Landroid/view/View;
aload 0
aload 0
getfield android/support/v7/internal/widget/ActionBarContextView/g Landroid/view/View;
invokevirtual android/support/v7/internal/widget/ActionBarContextView/addView(Landroid/view/View;)V
L1:
aload 0
getfield android/support/v7/internal/widget/ActionBarContextView/g Landroid/view/View;
getstatic android/support/v7/a/i/action_mode_close_button I
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
new android/support/v7/internal/widget/f
dup
aload 0
aload 1
invokespecial android/support/v7/internal/widget/f/<init>(Landroid/support/v7/internal/widget/ActionBarContextView;Landroid/support/v7/c/a;)V
invokevirtual android/view/View/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 1
invokevirtual android/support/v7/c/a/b()Landroid/view/Menu;
checkcast android/support/v7/internal/view/menu/i
astore 1
aload 0
getfield android/support/v7/internal/widget/ActionBarContextView/d Landroid/support/v7/widget/ActionMenuPresenter;
ifnull L2
aload 0
getfield android/support/v7/internal/widget/ActionBarContextView/d Landroid/support/v7/widget/ActionMenuPresenter;
invokevirtual android/support/v7/widget/ActionMenuPresenter/g()Z
pop
L2:
aload 0
new android/support/v7/widget/ActionMenuPresenter
dup
aload 0
invokevirtual android/support/v7/internal/widget/ActionBarContextView/getContext()Landroid/content/Context;
invokespecial android/support/v7/widget/ActionMenuPresenter/<init>(Landroid/content/Context;)V
putfield android/support/v7/internal/widget/ActionBarContextView/d Landroid/support/v7/widget/ActionMenuPresenter;
aload 0
getfield android/support/v7/internal/widget/ActionBarContextView/d Landroid/support/v7/widget/ActionMenuPresenter;
invokevirtual android/support/v7/widget/ActionMenuPresenter/d()V
new android/view/ViewGroup$LayoutParams
dup
bipush -2
iconst_m1
invokespecial android/view/ViewGroup$LayoutParams/<init>(II)V
astore 2
aload 1
aload 0
getfield android/support/v7/internal/widget/ActionBarContextView/d Landroid/support/v7/widget/ActionMenuPresenter;
aload 0
getfield android/support/v7/internal/widget/ActionBarContextView/b Landroid/content/Context;
invokevirtual android/support/v7/internal/view/menu/i/a(Landroid/support/v7/internal/view/menu/x;Landroid/content/Context;)V
aload 0
aload 0
getfield android/support/v7/internal/widget/ActionBarContextView/d Landroid/support/v7/widget/ActionMenuPresenter;
aload 0
invokevirtual android/support/v7/widget/ActionMenuPresenter/a(Landroid/view/ViewGroup;)Landroid/support/v7/internal/view/menu/z;
checkcast android/support/v7/widget/ActionMenuView
putfield android/support/v7/internal/widget/ActionBarContextView/c Landroid/support/v7/widget/ActionMenuView;
aload 0
getfield android/support/v7/internal/widget/ActionBarContextView/c Landroid/support/v7/widget/ActionMenuView;
aconst_null
invokevirtual android/support/v7/widget/ActionMenuView/setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
aload 0
aload 0
getfield android/support/v7/internal/widget/ActionBarContextView/c Landroid/support/v7/widget/ActionMenuView;
aload 2
invokevirtual android/support/v7/internal/widget/ActionBarContextView/addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
return
L0:
aload 0
getfield android/support/v7/internal/widget/ActionBarContextView/g Landroid/view/View;
invokevirtual android/view/View/getParent()Landroid/view/ViewParent;
ifnonnull L1
aload 0
aload 0
getfield android/support/v7/internal/widget/ActionBarContextView/g Landroid/view/View;
invokevirtual android/support/v7/internal/widget/ActionBarContextView/addView(Landroid/view/View;)V
goto L1
.limit locals 3
.limit stack 5
.end method

.method public final a()Z
aload 0
getfield android/support/v7/internal/widget/ActionBarContextView/d Landroid/support/v7/widget/ActionMenuPresenter;
ifnull L0
aload 0
getfield android/support/v7/internal/widget/ActionBarContextView/d Landroid/support/v7/widget/ActionMenuPresenter;
invokevirtual android/support/v7/widget/ActionMenuPresenter/e()Z
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final volatile synthetic b()V
aload 0
invokespecial android/support/v7/internal/widget/a/b()V
return
.limit locals 1
.limit stack 1
.end method

.method public final c()Z
aload 0
getfield android/support/v7/internal/widget/ActionBarContextView/d Landroid/support/v7/widget/ActionMenuPresenter;
ifnull L0
aload 0
getfield android/support/v7/internal/widget/ActionBarContextView/d Landroid/support/v7/widget/ActionMenuPresenter;
invokevirtual android/support/v7/widget/ActionMenuPresenter/f()Z
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final d()Z
aload 0
getfield android/support/v7/internal/widget/ActionBarContextView/d Landroid/support/v7/widget/ActionMenuPresenter;
ifnull L0
aload 0
getfield android/support/v7/internal/widget/ActionBarContextView/d Landroid/support/v7/widget/ActionMenuPresenter;
invokevirtual android/support/v7/widget/ActionMenuPresenter/i()Z
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final volatile synthetic e()Z
aload 0
invokespecial android/support/v7/internal/widget/a/e()Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final volatile synthetic f()Z
aload 0
invokespecial android/support/v7/internal/widget/a/f()Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final volatile synthetic g()Z
aload 0
invokespecial android/support/v7/internal/widget/a/g()Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method protected generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
new android/view/ViewGroup$MarginLayoutParams
dup
iconst_m1
bipush -2
invokespecial android/view/ViewGroup$MarginLayoutParams/<init>(II)V
areturn
.limit locals 1
.limit stack 4
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
new android/view/ViewGroup$MarginLayoutParams
dup
aload 0
invokevirtual android/support/v7/internal/widget/ActionBarContextView/getContext()Landroid/content/Context;
aload 1
invokespecial android/view/ViewGroup$MarginLayoutParams/<init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
areturn
.limit locals 2
.limit stack 4
.end method

.method public volatile synthetic getAnimatedVisibility()I
aload 0
invokespecial android/support/v7/internal/widget/a/getAnimatedVisibility()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public volatile synthetic getContentHeight()I
aload 0
invokespecial android/support/v7/internal/widget/a/getContentHeight()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getSubtitle()Ljava/lang/CharSequence;
aload 0
getfield android/support/v7/internal/widget/ActionBarContextView/k Ljava/lang/CharSequence;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getTitle()Ljava/lang/CharSequence;
aload 0
getfield android/support/v7/internal/widget/ActionBarContextView/j Ljava/lang/CharSequence;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final volatile synthetic h()V
aload 0
invokespecial android/support/v7/internal/widget/a/h()V
return
.limit locals 1
.limit stack 1
.end method

.method public final i()V
aload 0
invokevirtual android/support/v7/internal/widget/ActionBarContextView/removeAllViews()V
aload 0
aconst_null
putfield android/support/v7/internal/widget/ActionBarContextView/l Landroid/view/View;
aload 0
aconst_null
putfield android/support/v7/internal/widget/ActionBarContextView/c Landroid/support/v7/widget/ActionMenuView;
return
.limit locals 1
.limit stack 2
.end method

.method public onDetachedFromWindow()V
aload 0
invokespecial android/support/v7/internal/widget/a/onDetachedFromWindow()V
aload 0
getfield android/support/v7/internal/widget/ActionBarContextView/d Landroid/support/v7/widget/ActionMenuPresenter;
ifnull L0
aload 0
getfield android/support/v7/internal/widget/ActionBarContextView/d Landroid/support/v7/widget/ActionMenuPresenter;
invokevirtual android/support/v7/widget/ActionMenuPresenter/f()Z
pop
aload 0
getfield android/support/v7/internal/widget/ActionBarContextView/d Landroid/support/v7/widget/ActionMenuPresenter;
invokevirtual android/support/v7/widget/ActionMenuPresenter/h()Z
pop
L0:
return
.limit locals 1
.limit stack 1
.end method

.method public volatile synthetic onHoverEvent(Landroid/view/MotionEvent;)Z
aload 0
aload 1
invokespecial android/support/v7/internal/widget/a/onHoverEvent(Landroid/view/MotionEvent;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
getstatic android/os/Build$VERSION/SDK_INT I
bipush 14
if_icmplt L0
aload 1
invokevirtual android/view/accessibility/AccessibilityEvent/getEventType()I
bipush 32
if_icmpne L1
aload 1
aload 0
invokevirtual android/view/accessibility/AccessibilityEvent/setSource(Landroid/view/View;)V
aload 1
aload 0
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
invokevirtual java/lang/Class/getName()Ljava/lang/String;
invokevirtual android/view/accessibility/AccessibilityEvent/setClassName(Ljava/lang/CharSequence;)V
aload 1
aload 0
invokevirtual android/support/v7/internal/widget/ActionBarContextView/getContext()Landroid/content/Context;
invokevirtual android/content/Context/getPackageName()Ljava/lang/String;
invokevirtual android/view/accessibility/AccessibilityEvent/setPackageName(Ljava/lang/CharSequence;)V
aload 1
aload 0
getfield android/support/v7/internal/widget/ActionBarContextView/j Ljava/lang/CharSequence;
invokevirtual android/view/accessibility/AccessibilityEvent/setContentDescription(Ljava/lang/CharSequence;)V
L0:
return
L1:
aload 0
aload 1
invokespecial android/support/v7/internal/widget/a/onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
return
.limit locals 2
.limit stack 2
.end method

.method protected onLayout(ZIIII)V
aload 0
invokestatic android/support/v7/internal/widget/bd/a(Landroid/view/View;)Z
istore 1
iload 1
ifeq L0
iload 4
iload 2
isub
aload 0
invokevirtual android/support/v7/internal/widget/ActionBarContextView/getPaddingRight()I
isub
istore 6
L1:
aload 0
invokevirtual android/support/v7/internal/widget/ActionBarContextView/getPaddingTop()I
istore 7
iload 5
iload 3
isub
aload 0
invokevirtual android/support/v7/internal/widget/ActionBarContextView/getPaddingTop()I
isub
aload 0
invokevirtual android/support/v7/internal/widget/ActionBarContextView/getPaddingBottom()I
isub
istore 8
iload 6
istore 3
aload 0
getfield android/support/v7/internal/widget/ActionBarContextView/g Landroid/view/View;
ifnull L2
iload 6
istore 3
aload 0
getfield android/support/v7/internal/widget/ActionBarContextView/g Landroid/view/View;
invokevirtual android/view/View/getVisibility()I
bipush 8
if_icmpeq L2
aload 0
getfield android/support/v7/internal/widget/ActionBarContextView/g Landroid/view/View;
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/view/ViewGroup$MarginLayoutParams
astore 9
iload 1
ifeq L3
aload 9
getfield android/view/ViewGroup$MarginLayoutParams/rightMargin I
istore 3
L4:
iload 1
ifeq L5
aload 9
getfield android/view/ViewGroup$MarginLayoutParams/leftMargin I
istore 5
L6:
iload 6
iload 3
iload 1
invokestatic android/support/v7/internal/widget/ActionBarContextView/a(IIZ)I
istore 3
iload 3
aload 0
getfield android/support/v7/internal/widget/ActionBarContextView/g Landroid/view/View;
iload 3
iload 7
iload 8
iload 1
invokestatic android/support/v7/internal/widget/ActionBarContextView/a(Landroid/view/View;IIIZ)I
iadd
iload 5
iload 1
invokestatic android/support/v7/internal/widget/ActionBarContextView/a(IIZ)I
istore 3
L2:
iload 3
istore 5
aload 0
getfield android/support/v7/internal/widget/ActionBarContextView/m Landroid/widget/LinearLayout;
ifnull L7
iload 3
istore 5
aload 0
getfield android/support/v7/internal/widget/ActionBarContextView/l Landroid/view/View;
ifnonnull L7
iload 3
istore 5
aload 0
getfield android/support/v7/internal/widget/ActionBarContextView/m Landroid/widget/LinearLayout;
invokevirtual android/widget/LinearLayout/getVisibility()I
bipush 8
if_icmpeq L7
iload 3
aload 0
getfield android/support/v7/internal/widget/ActionBarContextView/m Landroid/widget/LinearLayout;
iload 3
iload 7
iload 8
iload 1
invokestatic android/support/v7/internal/widget/ActionBarContextView/a(Landroid/view/View;IIIZ)I
iadd
istore 5
L7:
aload 0
getfield android/support/v7/internal/widget/ActionBarContextView/l Landroid/view/View;
ifnull L8
aload 0
getfield android/support/v7/internal/widget/ActionBarContextView/l Landroid/view/View;
iload 5
iload 7
iload 8
iload 1
invokestatic android/support/v7/internal/widget/ActionBarContextView/a(Landroid/view/View;IIIZ)I
pop
L8:
iload 1
ifeq L9
aload 0
invokevirtual android/support/v7/internal/widget/ActionBarContextView/getPaddingLeft()I
istore 2
L10:
aload 0
getfield android/support/v7/internal/widget/ActionBarContextView/c Landroid/support/v7/widget/ActionMenuView;
ifnull L11
aload 0
getfield android/support/v7/internal/widget/ActionBarContextView/c Landroid/support/v7/widget/ActionMenuView;
astore 9
iload 1
ifne L12
iconst_1
istore 1
L13:
aload 9
iload 2
iload 7
iload 8
iload 1
invokestatic android/support/v7/internal/widget/ActionBarContextView/a(Landroid/view/View;IIIZ)I
pop
L11:
return
L0:
aload 0
invokevirtual android/support/v7/internal/widget/ActionBarContextView/getPaddingLeft()I
istore 6
goto L1
L3:
aload 9
getfield android/view/ViewGroup$MarginLayoutParams/leftMargin I
istore 3
goto L4
L5:
aload 9
getfield android/view/ViewGroup$MarginLayoutParams/rightMargin I
istore 5
goto L6
L9:
iload 4
iload 2
isub
aload 0
invokevirtual android/support/v7/internal/widget/ActionBarContextView/getPaddingRight()I
isub
istore 2
goto L10
L12:
iconst_0
istore 1
goto L13
.limit locals 10
.limit stack 6
.end method

.method protected onMeasure(II)V
ldc_w 1073741824
istore 5
iconst_0
istore 6
iload 1
invokestatic android/view/View$MeasureSpec/getMode(I)I
ldc_w 1073741824
if_icmpeq L0
new java/lang/IllegalStateException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
invokevirtual java/lang/Class/getSimpleName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " can only be used with android:layout_width=\"match_parent\" (or fill_parent)"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L0:
iload 2
invokestatic android/view/View$MeasureSpec/getMode(I)I
ifne L1
new java/lang/IllegalStateException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
invokevirtual java/lang/Class/getSimpleName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " can only be used with android:layout_height=\"wrap_content\""
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L1:
iload 1
invokestatic android/view/View$MeasureSpec/getSize(I)I
istore 8
aload 0
getfield android/support/v7/internal/widget/ActionBarContextView/e I
ifle L2
aload 0
getfield android/support/v7/internal/widget/ActionBarContextView/e I
istore 3
L3:
aload 0
invokevirtual android/support/v7/internal/widget/ActionBarContextView/getPaddingTop()I
aload 0
invokevirtual android/support/v7/internal/widget/ActionBarContextView/getPaddingBottom()I
iadd
istore 9
iload 8
aload 0
invokevirtual android/support/v7/internal/widget/ActionBarContextView/getPaddingLeft()I
isub
aload 0
invokevirtual android/support/v7/internal/widget/ActionBarContextView/getPaddingRight()I
isub
istore 1
iload 3
iload 9
isub
istore 7
iload 7
ldc_w -2147483648
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
istore 4
iload 1
istore 2
aload 0
getfield android/support/v7/internal/widget/ActionBarContextView/g Landroid/view/View;
ifnull L4
aload 0
getfield android/support/v7/internal/widget/ActionBarContextView/g Landroid/view/View;
iload 1
iload 4
invokestatic android/support/v7/internal/widget/ActionBarContextView/a(Landroid/view/View;II)I
istore 1
aload 0
getfield android/support/v7/internal/widget/ActionBarContextView/g Landroid/view/View;
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/view/ViewGroup$MarginLayoutParams
astore 11
aload 11
getfield android/view/ViewGroup$MarginLayoutParams/leftMargin I
istore 2
iload 1
aload 11
getfield android/view/ViewGroup$MarginLayoutParams/rightMargin I
iload 2
iadd
isub
istore 2
L4:
iload 2
istore 1
aload 0
getfield android/support/v7/internal/widget/ActionBarContextView/c Landroid/support/v7/widget/ActionMenuView;
ifnull L5
iload 2
istore 1
aload 0
getfield android/support/v7/internal/widget/ActionBarContextView/c Landroid/support/v7/widget/ActionMenuView;
invokevirtual android/support/v7/widget/ActionMenuView/getParent()Landroid/view/ViewParent;
aload 0
if_acmpne L5
aload 0
getfield android/support/v7/internal/widget/ActionBarContextView/c Landroid/support/v7/widget/ActionMenuView;
iload 2
iload 4
invokestatic android/support/v7/internal/widget/ActionBarContextView/a(Landroid/view/View;II)I
istore 1
L5:
iload 1
istore 2
aload 0
getfield android/support/v7/internal/widget/ActionBarContextView/m Landroid/widget/LinearLayout;
ifnull L6
iload 1
istore 2
aload 0
getfield android/support/v7/internal/widget/ActionBarContextView/l Landroid/view/View;
ifnonnull L6
aload 0
getfield android/support/v7/internal/widget/ActionBarContextView/h Z
ifeq L7
iconst_0
iconst_0
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
istore 2
aload 0
getfield android/support/v7/internal/widget/ActionBarContextView/m Landroid/widget/LinearLayout;
iload 2
iload 4
invokevirtual android/widget/LinearLayout/measure(II)V
aload 0
getfield android/support/v7/internal/widget/ActionBarContextView/m Landroid/widget/LinearLayout;
invokevirtual android/widget/LinearLayout/getMeasuredWidth()I
istore 10
iload 10
iload 1
if_icmpgt L8
iconst_1
istore 4
L9:
iload 1
istore 2
iload 4
ifeq L10
iload 1
iload 10
isub
istore 2
L10:
aload 0
getfield android/support/v7/internal/widget/ActionBarContextView/m Landroid/widget/LinearLayout;
astore 11
iload 4
ifeq L11
iconst_0
istore 1
L12:
aload 11
iload 1
invokevirtual android/widget/LinearLayout/setVisibility(I)V
L6:
aload 0
getfield android/support/v7/internal/widget/ActionBarContextView/l Landroid/view/View;
ifnull L13
aload 0
getfield android/support/v7/internal/widget/ActionBarContextView/l Landroid/view/View;
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
astore 11
aload 11
getfield android/view/ViewGroup$LayoutParams/width I
bipush -2
if_icmpeq L14
ldc_w 1073741824
istore 1
L15:
iload 2
istore 4
aload 11
getfield android/view/ViewGroup$LayoutParams/width I
iflt L16
aload 11
getfield android/view/ViewGroup$LayoutParams/width I
iload 2
invokestatic java/lang/Math/min(II)I
istore 4
L16:
aload 11
getfield android/view/ViewGroup$LayoutParams/height I
bipush -2
if_icmpeq L17
iload 5
istore 2
L18:
aload 11
getfield android/view/ViewGroup$LayoutParams/height I
iflt L19
aload 11
getfield android/view/ViewGroup$LayoutParams/height I
iload 7
invokestatic java/lang/Math/min(II)I
istore 5
L20:
aload 0
getfield android/support/v7/internal/widget/ActionBarContextView/l Landroid/view/View;
iload 4
iload 1
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
iload 5
iload 2
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
invokevirtual android/view/View/measure(II)V
L13:
aload 0
getfield android/support/v7/internal/widget/ActionBarContextView/e I
ifgt L21
aload 0
invokevirtual android/support/v7/internal/widget/ActionBarContextView/getChildCount()I
istore 4
iconst_0
istore 1
iload 6
istore 2
L22:
iload 2
iload 4
if_icmpge L23
aload 0
iload 2
invokevirtual android/support/v7/internal/widget/ActionBarContextView/getChildAt(I)Landroid/view/View;
invokevirtual android/view/View/getMeasuredHeight()I
iload 9
iadd
istore 3
iload 3
iload 1
if_icmple L24
iload 3
istore 1
L25:
iload 2
iconst_1
iadd
istore 2
goto L22
L2:
iload 2
invokestatic android/view/View$MeasureSpec/getSize(I)I
istore 3
goto L3
L8:
iconst_0
istore 4
goto L9
L11:
bipush 8
istore 1
goto L12
L7:
aload 0
getfield android/support/v7/internal/widget/ActionBarContextView/m Landroid/widget/LinearLayout;
iload 1
iload 4
invokestatic android/support/v7/internal/widget/ActionBarContextView/a(Landroid/view/View;II)I
istore 2
goto L6
L14:
ldc_w -2147483648
istore 1
goto L15
L17:
ldc_w -2147483648
istore 2
goto L18
L19:
iload 7
istore 5
goto L20
L23:
aload 0
iload 8
iload 1
invokevirtual android/support/v7/internal/widget/ActionBarContextView/setMeasuredDimension(II)V
return
L21:
aload 0
iload 8
iload 3
invokevirtual android/support/v7/internal/widget/ActionBarContextView/setMeasuredDimension(II)V
return
L24:
goto L25
.limit locals 12
.limit stack 4
.end method

.method public volatile synthetic onTouchEvent(Landroid/view/MotionEvent;)Z
aload 0
aload 1
invokespecial android/support/v7/internal/widget/a/onTouchEvent(Landroid/view/MotionEvent;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public setContentHeight(I)V
aload 0
iload 1
putfield android/support/v7/internal/widget/ActionBarContextView/e I
return
.limit locals 2
.limit stack 2
.end method

.method public setCustomView(Landroid/view/View;)V
aload 0
getfield android/support/v7/internal/widget/ActionBarContextView/l Landroid/view/View;
ifnull L0
aload 0
aload 0
getfield android/support/v7/internal/widget/ActionBarContextView/l Landroid/view/View;
invokevirtual android/support/v7/internal/widget/ActionBarContextView/removeView(Landroid/view/View;)V
L0:
aload 0
aload 1
putfield android/support/v7/internal/widget/ActionBarContextView/l Landroid/view/View;
aload 1
ifnull L1
aload 0
getfield android/support/v7/internal/widget/ActionBarContextView/m Landroid/widget/LinearLayout;
ifnull L1
aload 0
aload 0
getfield android/support/v7/internal/widget/ActionBarContextView/m Landroid/widget/LinearLayout;
invokevirtual android/support/v7/internal/widget/ActionBarContextView/removeView(Landroid/view/View;)V
aload 0
aconst_null
putfield android/support/v7/internal/widget/ActionBarContextView/m Landroid/widget/LinearLayout;
L1:
aload 1
ifnull L2
aload 0
aload 1
invokevirtual android/support/v7/internal/widget/ActionBarContextView/addView(Landroid/view/View;)V
L2:
aload 0
invokevirtual android/support/v7/internal/widget/ActionBarContextView/requestLayout()V
return
.limit locals 2
.limit stack 2
.end method

.method public setSubtitle(Ljava/lang/CharSequence;)V
aload 0
aload 1
putfield android/support/v7/internal/widget/ActionBarContextView/k Ljava/lang/CharSequence;
aload 0
invokespecial android/support/v7/internal/widget/ActionBarContextView/j()V
return
.limit locals 2
.limit stack 2
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
aload 0
aload 1
putfield android/support/v7/internal/widget/ActionBarContextView/j Ljava/lang/CharSequence;
aload 0
invokespecial android/support/v7/internal/widget/ActionBarContextView/j()V
return
.limit locals 2
.limit stack 2
.end method

.method public setTitleOptional(Z)V
iload 1
aload 0
getfield android/support/v7/internal/widget/ActionBarContextView/h Z
if_icmpeq L0
aload 0
invokevirtual android/support/v7/internal/widget/ActionBarContextView/requestLayout()V
L0:
aload 0
iload 1
putfield android/support/v7/internal/widget/ActionBarContextView/h Z
return
.limit locals 2
.limit stack 2
.end method

.method public volatile synthetic setVisibility(I)V
aload 0
iload 1
invokespecial android/support/v7/internal/widget/a/setVisibility(I)V
return
.limit locals 2
.limit stack 2
.end method

.method public shouldDelayChildPressedState()Z
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method
