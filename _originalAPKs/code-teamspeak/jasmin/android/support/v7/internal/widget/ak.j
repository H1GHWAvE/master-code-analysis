.bytecode 50.0
.class public final synchronized android/support/v7/internal/widget/ak
.super java/lang/Object

.field public static final 'a' I = -2147483648


.field public 'b' I

.field public 'c' I

.field public 'd' I

.field public 'e' I

.field public 'f' I

.field public 'g' I

.field public 'h' Z

.field public 'i' Z

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
iconst_0
putfield android/support/v7/internal/widget/ak/b I
aload 0
iconst_0
putfield android/support/v7/internal/widget/ak/c I
aload 0
ldc_w -2147483648
putfield android/support/v7/internal/widget/ak/d I
aload 0
ldc_w -2147483648
putfield android/support/v7/internal/widget/ak/e I
aload 0
iconst_0
putfield android/support/v7/internal/widget/ak/f I
aload 0
iconst_0
putfield android/support/v7/internal/widget/ak/g I
aload 0
iconst_0
putfield android/support/v7/internal/widget/ak/h Z
aload 0
iconst_0
putfield android/support/v7/internal/widget/ak/i Z
return
.limit locals 1
.limit stack 2
.end method

.method private a()I
aload 0
getfield android/support/v7/internal/widget/ak/b I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private a(Z)V
iload 1
aload 0
getfield android/support/v7/internal/widget/ak/h Z
if_icmpne L0
return
L0:
aload 0
iload 1
putfield android/support/v7/internal/widget/ak/h Z
aload 0
getfield android/support/v7/internal/widget/ak/i Z
ifeq L1
iload 1
ifeq L2
aload 0
getfield android/support/v7/internal/widget/ak/e I
ldc_w -2147483648
if_icmpeq L3
aload 0
getfield android/support/v7/internal/widget/ak/e I
istore 2
L4:
aload 0
iload 2
putfield android/support/v7/internal/widget/ak/b I
aload 0
getfield android/support/v7/internal/widget/ak/d I
ldc_w -2147483648
if_icmpeq L5
aload 0
getfield android/support/v7/internal/widget/ak/d I
istore 2
L6:
aload 0
iload 2
putfield android/support/v7/internal/widget/ak/c I
return
L3:
aload 0
getfield android/support/v7/internal/widget/ak/f I
istore 2
goto L4
L5:
aload 0
getfield android/support/v7/internal/widget/ak/g I
istore 2
goto L6
L2:
aload 0
getfield android/support/v7/internal/widget/ak/d I
ldc_w -2147483648
if_icmpeq L7
aload 0
getfield android/support/v7/internal/widget/ak/d I
istore 2
L8:
aload 0
iload 2
putfield android/support/v7/internal/widget/ak/b I
aload 0
getfield android/support/v7/internal/widget/ak/e I
ldc_w -2147483648
if_icmpeq L9
aload 0
getfield android/support/v7/internal/widget/ak/e I
istore 2
L10:
aload 0
iload 2
putfield android/support/v7/internal/widget/ak/c I
return
L7:
aload 0
getfield android/support/v7/internal/widget/ak/f I
istore 2
goto L8
L9:
aload 0
getfield android/support/v7/internal/widget/ak/g I
istore 2
goto L10
L1:
aload 0
aload 0
getfield android/support/v7/internal/widget/ak/f I
putfield android/support/v7/internal/widget/ak/b I
aload 0
aload 0
getfield android/support/v7/internal/widget/ak/g I
putfield android/support/v7/internal/widget/ak/c I
return
.limit locals 3
.limit stack 2
.end method

.method private b()I
aload 0
getfield android/support/v7/internal/widget/ak/c I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private c()I
aload 0
getfield android/support/v7/internal/widget/ak/h Z
ifeq L0
aload 0
getfield android/support/v7/internal/widget/ak/c I
ireturn
L0:
aload 0
getfield android/support/v7/internal/widget/ak/b I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private d()I
aload 0
getfield android/support/v7/internal/widget/ak/h Z
ifeq L0
aload 0
getfield android/support/v7/internal/widget/ak/b I
ireturn
L0:
aload 0
getfield android/support/v7/internal/widget/ak/c I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final a(II)V
aload 0
iload 1
putfield android/support/v7/internal/widget/ak/d I
aload 0
iload 2
putfield android/support/v7/internal/widget/ak/e I
aload 0
iconst_1
putfield android/support/v7/internal/widget/ak/i Z
aload 0
getfield android/support/v7/internal/widget/ak/h Z
ifeq L0
iload 2
ldc_w -2147483648
if_icmpeq L1
aload 0
iload 2
putfield android/support/v7/internal/widget/ak/b I
L1:
iload 1
ldc_w -2147483648
if_icmpeq L2
aload 0
iload 1
putfield android/support/v7/internal/widget/ak/c I
L2:
return
L0:
iload 1
ldc_w -2147483648
if_icmpeq L3
aload 0
iload 1
putfield android/support/v7/internal/widget/ak/b I
L3:
iload 2
ldc_w -2147483648
if_icmpeq L2
aload 0
iload 2
putfield android/support/v7/internal/widget/ak/c I
return
.limit locals 3
.limit stack 2
.end method

.method public final b(II)V
aload 0
iconst_0
putfield android/support/v7/internal/widget/ak/i Z
iload 1
ldc_w -2147483648
if_icmpeq L0
aload 0
iload 1
putfield android/support/v7/internal/widget/ak/f I
aload 0
iload 1
putfield android/support/v7/internal/widget/ak/b I
L0:
iload 2
ldc_w -2147483648
if_icmpeq L1
aload 0
iload 2
putfield android/support/v7/internal/widget/ak/g I
aload 0
iload 2
putfield android/support/v7/internal/widget/ak/c I
L1:
return
.limit locals 3
.limit stack 2
.end method
