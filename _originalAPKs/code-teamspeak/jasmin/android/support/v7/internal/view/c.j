.bytecode 50.0
.class public final synchronized android/support/v7/internal/view/c
.super android/support/v7/c/a
.implements android/support/v7/internal/view/menu/j

.field private 'a' Landroid/content/Context;

.field private 'd' Landroid/support/v7/internal/widget/ActionBarContextView;

.field private 'e' Landroid/support/v7/c/b;

.field private 'f' Ljava/lang/ref/WeakReference;

.field private 'g' Z

.field private 'h' Z

.field private 'i' Landroid/support/v7/internal/view/menu/i;

.method public <init>(Landroid/content/Context;Landroid/support/v7/internal/widget/ActionBarContextView;Landroid/support/v7/c/b;Z)V
aload 0
invokespecial android/support/v7/c/a/<init>()V
aload 0
aload 1
putfield android/support/v7/internal/view/c/a Landroid/content/Context;
aload 0
aload 2
putfield android/support/v7/internal/view/c/d Landroid/support/v7/internal/widget/ActionBarContextView;
aload 0
aload 3
putfield android/support/v7/internal/view/c/e Landroid/support/v7/c/b;
new android/support/v7/internal/view/menu/i
dup
aload 2
invokevirtual android/support/v7/internal/widget/ActionBarContextView/getContext()Landroid/content/Context;
invokespecial android/support/v7/internal/view/menu/i/<init>(Landroid/content/Context;)V
astore 1
aload 1
iconst_1
putfield android/support/v7/internal/view/menu/i/i I
aload 0
aload 1
putfield android/support/v7/internal/view/c/i Landroid/support/v7/internal/view/menu/i;
aload 0
getfield android/support/v7/internal/view/c/i Landroid/support/v7/internal/view/menu/i;
aload 0
invokevirtual android/support/v7/internal/view/menu/i/a(Landroid/support/v7/internal/view/menu/j;)V
aload 0
iload 4
putfield android/support/v7/internal/view/c/h Z
return
.limit locals 5
.limit stack 3
.end method

.method private a(Landroid/support/v7/internal/view/menu/ad;)Z
aload 1
invokevirtual android/support/v7/internal/view/menu/ad/hasVisibleItems()Z
ifne L0
iconst_1
ireturn
L0:
new android/support/v7/internal/view/menu/v
dup
aload 0
getfield android/support/v7/internal/view/c/d Landroid/support/v7/internal/widget/ActionBarContextView;
invokevirtual android/support/v7/internal/widget/ActionBarContextView/getContext()Landroid/content/Context;
aload 1
invokespecial android/support/v7/internal/view/menu/v/<init>(Landroid/content/Context;Landroid/support/v7/internal/view/menu/i;)V
invokevirtual android/support/v7/internal/view/menu/v/d()V
iconst_1
ireturn
.limit locals 2
.limit stack 4
.end method

.method private static j()V
return
.limit locals 0
.limit stack 0
.end method

.method private static k()V
return
.limit locals 0
.limit stack 0
.end method

.method public final a()Landroid/view/MenuInflater;
new android/view/MenuInflater
dup
aload 0
getfield android/support/v7/internal/view/c/d Landroid/support/v7/internal/widget/ActionBarContextView;
invokevirtual android/support/v7/internal/widget/ActionBarContextView/getContext()Landroid/content/Context;
invokespecial android/view/MenuInflater/<init>(Landroid/content/Context;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method public final a(I)V
aload 0
aload 0
getfield android/support/v7/internal/view/c/a Landroid/content/Context;
iload 1
invokevirtual android/content/Context/getString(I)Ljava/lang/String;
invokevirtual android/support/v7/internal/view/c/b(Ljava/lang/CharSequence;)V
return
.limit locals 2
.limit stack 3
.end method

.method public final a(Landroid/support/v7/internal/view/menu/i;)V
aload 0
invokevirtual android/support/v7/internal/view/c/d()V
aload 0
getfield android/support/v7/internal/view/c/d Landroid/support/v7/internal/widget/ActionBarContextView;
invokevirtual android/support/v7/internal/widget/ActionBarContextView/a()Z
pop
return
.limit locals 2
.limit stack 1
.end method

.method public final a(Landroid/view/View;)V
aload 0
getfield android/support/v7/internal/view/c/d Landroid/support/v7/internal/widget/ActionBarContextView;
aload 1
invokevirtual android/support/v7/internal/widget/ActionBarContextView/setCustomView(Landroid/view/View;)V
aload 1
ifnull L0
new java/lang/ref/WeakReference
dup
aload 1
invokespecial java/lang/ref/WeakReference/<init>(Ljava/lang/Object;)V
astore 1
L1:
aload 0
aload 1
putfield android/support/v7/internal/view/c/f Ljava/lang/ref/WeakReference;
return
L0:
aconst_null
astore 1
goto L1
.limit locals 2
.limit stack 3
.end method

.method public final a(Ljava/lang/CharSequence;)V
aload 0
getfield android/support/v7/internal/view/c/d Landroid/support/v7/internal/widget/ActionBarContextView;
aload 1
invokevirtual android/support/v7/internal/widget/ActionBarContextView/setSubtitle(Ljava/lang/CharSequence;)V
return
.limit locals 2
.limit stack 2
.end method

.method public final a(Z)V
aload 0
iload 1
invokespecial android/support/v7/c/a/a(Z)V
aload 0
getfield android/support/v7/internal/view/c/d Landroid/support/v7/internal/widget/ActionBarContextView;
iload 1
invokevirtual android/support/v7/internal/widget/ActionBarContextView/setTitleOptional(Z)V
return
.limit locals 2
.limit stack 2
.end method

.method public final a(Landroid/support/v7/internal/view/menu/i;Landroid/view/MenuItem;)Z
aload 0
getfield android/support/v7/internal/view/c/e Landroid/support/v7/c/b;
aload 0
aload 2
invokeinterface android/support/v7/c/b/a(Landroid/support/v7/c/a;Landroid/view/MenuItem;)Z 2
ireturn
.limit locals 3
.limit stack 3
.end method

.method public final b()Landroid/view/Menu;
aload 0
getfield android/support/v7/internal/view/c/i Landroid/support/v7/internal/view/menu/i;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final b(I)V
aload 0
aload 0
getfield android/support/v7/internal/view/c/a Landroid/content/Context;
iload 1
invokevirtual android/content/Context/getString(I)Ljava/lang/String;
invokevirtual android/support/v7/internal/view/c/a(Ljava/lang/CharSequence;)V
return
.limit locals 2
.limit stack 3
.end method

.method public final b(Ljava/lang/CharSequence;)V
aload 0
getfield android/support/v7/internal/view/c/d Landroid/support/v7/internal/widget/ActionBarContextView;
aload 1
invokevirtual android/support/v7/internal/widget/ActionBarContextView/setTitle(Ljava/lang/CharSequence;)V
return
.limit locals 2
.limit stack 2
.end method

.method public final b_()Z
aload 0
getfield android/support/v7/internal/view/c/h Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final c()V
aload 0
getfield android/support/v7/internal/view/c/g Z
ifeq L0
return
L0:
aload 0
iconst_1
putfield android/support/v7/internal/view/c/g Z
aload 0
getfield android/support/v7/internal/view/c/d Landroid/support/v7/internal/widget/ActionBarContextView;
bipush 32
invokevirtual android/support/v7/internal/widget/ActionBarContextView/sendAccessibilityEvent(I)V
aload 0
getfield android/support/v7/internal/view/c/e Landroid/support/v7/c/b;
aload 0
invokeinterface android/support/v7/c/b/a(Landroid/support/v7/c/a;)V 1
return
.limit locals 1
.limit stack 2
.end method

.method public final d()V
aload 0
getfield android/support/v7/internal/view/c/e Landroid/support/v7/c/b;
aload 0
aload 0
getfield android/support/v7/internal/view/c/i Landroid/support/v7/internal/view/menu/i;
invokeinterface android/support/v7/c/b/b(Landroid/support/v7/c/a;Landroid/view/Menu;)Z 2
pop
return
.limit locals 1
.limit stack 3
.end method

.method public final f()Ljava/lang/CharSequence;
aload 0
getfield android/support/v7/internal/view/c/d Landroid/support/v7/internal/widget/ActionBarContextView;
invokevirtual android/support/v7/internal/widget/ActionBarContextView/getTitle()Ljava/lang/CharSequence;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final g()Ljava/lang/CharSequence;
aload 0
getfield android/support/v7/internal/view/c/d Landroid/support/v7/internal/widget/ActionBarContextView;
invokevirtual android/support/v7/internal/widget/ActionBarContextView/getSubtitle()Ljava/lang/CharSequence;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final h()Z
aload 0
getfield android/support/v7/internal/view/c/d Landroid/support/v7/internal/widget/ActionBarContextView;
getfield android/support/v7/internal/widget/ActionBarContextView/h Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final i()Landroid/view/View;
aload 0
getfield android/support/v7/internal/view/c/f Ljava/lang/ref/WeakReference;
ifnull L0
aload 0
getfield android/support/v7/internal/view/c/f Ljava/lang/ref/WeakReference;
invokevirtual java/lang/ref/WeakReference/get()Ljava/lang/Object;
checkcast android/view/View
areturn
L0:
aconst_null
areturn
.limit locals 1
.limit stack 1
.end method
