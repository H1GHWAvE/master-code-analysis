.bytecode 50.0
.class public final synchronized android/support/v7/internal/view/f
.super android/view/MenuInflater

.field private static final 'a' Ljava/lang/String; = "SupportMenuInflater"

.field private static final 'b' Ljava/lang/String; = "menu"

.field private static final 'c' Ljava/lang/String; = "group"

.field private static final 'd' Ljava/lang/String; = "item"

.field private static final 'e' I = 0


.field private static final 'f' [Ljava/lang/Class;

.field private static final 'g' [Ljava/lang/Class;

.field private final 'h' [Ljava/lang/Object;

.field private final 'i' [Ljava/lang/Object;

.field private 'j' Landroid/content/Context;

.field private 'k' Ljava/lang/Object;

.method static <clinit>()V
iconst_1
anewarray java/lang/Class
astore 0
aload 0
iconst_0
ldc android/content/Context
aastore
aload 0
putstatic android/support/v7/internal/view/f/f [Ljava/lang/Class;
aload 0
putstatic android/support/v7/internal/view/f/g [Ljava/lang/Class;
return
.limit locals 1
.limit stack 3
.end method

.method public <init>(Landroid/content/Context;)V
aload 0
aload 1
invokespecial android/view/MenuInflater/<init>(Landroid/content/Context;)V
aload 0
aload 1
putfield android/support/v7/internal/view/f/j Landroid/content/Context;
aload 0
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 1
aastore
putfield android/support/v7/internal/view/f/h [Ljava/lang/Object;
aload 0
aload 0
getfield android/support/v7/internal/view/f/h [Ljava/lang/Object;
putfield android/support/v7/internal/view/f/i [Ljava/lang/Object;
return
.limit locals 2
.limit stack 5
.end method

.method static synthetic a(Landroid/support/v7/internal/view/f;)Landroid/content/Context;
aload 0
getfield android/support/v7/internal/view/f/j Landroid/content/Context;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static a(Ljava/lang/Object;)Ljava/lang/Object;
L0:
aload 0
instanceof android/app/Activity
ifeq L1
L2:
aload 0
areturn
L1:
aload 0
instanceof android/content/ContextWrapper
ifeq L2
aload 0
checkcast android/content/ContextWrapper
invokevirtual android/content/ContextWrapper/getBaseContext()Landroid/content/Context;
astore 0
goto L0
.limit locals 1
.limit stack 1
.end method

.method private a(Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;Landroid/view/Menu;)V
new android/support/v7/internal/view/h
dup
aload 0
aload 3
invokespecial android/support/v7/internal/view/h/<init>(Landroid/support/v7/internal/view/f;Landroid/view/Menu;)V
astore 8
aload 1
invokeinterface org/xmlpull/v1/XmlPullParser/getEventType()I 0
istore 4
iconst_0
istore 5
aconst_null
astore 3
L0:
iload 4
iconst_2
if_icmpne L1
aload 1
invokeinterface org/xmlpull/v1/XmlPullParser/getName()Ljava/lang/String; 0
astore 7
aload 7
ldc "menu"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L2
aload 1
invokeinterface org/xmlpull/v1/XmlPullParser/next()I 0
istore 6
L3:
iconst_0
istore 4
L4:
iload 4
ifne L5
iload 6
tableswitch 1
L6
L7
L8
default : L9
L9:
aload 1
invokeinterface org/xmlpull/v1/XmlPullParser/next()I 0
istore 6
goto L4
L2:
new java/lang/RuntimeException
dup
new java/lang/StringBuilder
dup
ldc "Expecting menu, got "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 7
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/RuntimeException/<init>(Ljava/lang/String;)V
athrow
L1:
aload 1
invokeinterface org/xmlpull/v1/XmlPullParser/next()I 0
istore 6
iload 6
istore 4
iload 6
iconst_1
if_icmpne L0
goto L3
L7:
iload 5
ifne L9
aload 1
invokeinterface org/xmlpull/v1/XmlPullParser/getName()Ljava/lang/String; 0
astore 7
aload 7
ldc "group"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L10
aload 8
getfield android/support/v7/internal/view/h/z Landroid/support/v7/internal/view/f;
getfield android/support/v7/internal/view/f/j Landroid/content/Context;
aload 2
getstatic android/support/v7/a/n/MenuGroup [I
invokevirtual android/content/Context/obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;
astore 7
aload 8
aload 7
getstatic android/support/v7/a/n/MenuGroup_android_id I
iconst_0
invokevirtual android/content/res/TypedArray/getResourceId(II)I
putfield android/support/v7/internal/view/h/b I
aload 8
aload 7
getstatic android/support/v7/a/n/MenuGroup_android_menuCategory I
iconst_0
invokevirtual android/content/res/TypedArray/getInt(II)I
putfield android/support/v7/internal/view/h/c I
aload 8
aload 7
getstatic android/support/v7/a/n/MenuGroup_android_orderInCategory I
iconst_0
invokevirtual android/content/res/TypedArray/getInt(II)I
putfield android/support/v7/internal/view/h/d I
aload 8
aload 7
getstatic android/support/v7/a/n/MenuGroup_android_checkableBehavior I
iconst_0
invokevirtual android/content/res/TypedArray/getInt(II)I
putfield android/support/v7/internal/view/h/e I
aload 8
aload 7
getstatic android/support/v7/a/n/MenuGroup_android_visible I
iconst_1
invokevirtual android/content/res/TypedArray/getBoolean(IZ)Z
putfield android/support/v7/internal/view/h/f Z
aload 8
aload 7
getstatic android/support/v7/a/n/MenuGroup_android_enabled I
iconst_1
invokevirtual android/content/res/TypedArray/getBoolean(IZ)Z
putfield android/support/v7/internal/view/h/g Z
aload 7
invokevirtual android/content/res/TypedArray/recycle()V
goto L9
L10:
aload 7
ldc "item"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L11
aload 8
getfield android/support/v7/internal/view/h/z Landroid/support/v7/internal/view/f;
getfield android/support/v7/internal/view/f/j Landroid/content/Context;
aload 2
getstatic android/support/v7/a/n/MenuItem [I
invokevirtual android/content/Context/obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;
astore 7
aload 8
aload 7
getstatic android/support/v7/a/n/MenuItem_android_id I
iconst_0
invokevirtual android/content/res/TypedArray/getResourceId(II)I
putfield android/support/v7/internal/view/h/i I
aload 8
aload 7
getstatic android/support/v7/a/n/MenuItem_android_menuCategory I
aload 8
getfield android/support/v7/internal/view/h/c I
invokevirtual android/content/res/TypedArray/getInt(II)I
ldc_w -65536
iand
aload 7
getstatic android/support/v7/a/n/MenuItem_android_orderInCategory I
aload 8
getfield android/support/v7/internal/view/h/d I
invokevirtual android/content/res/TypedArray/getInt(II)I
ldc_w 65535
iand
ior
putfield android/support/v7/internal/view/h/j I
aload 8
aload 7
getstatic android/support/v7/a/n/MenuItem_android_title I
invokevirtual android/content/res/TypedArray/getText(I)Ljava/lang/CharSequence;
putfield android/support/v7/internal/view/h/k Ljava/lang/CharSequence;
aload 8
aload 7
getstatic android/support/v7/a/n/MenuItem_android_titleCondensed I
invokevirtual android/content/res/TypedArray/getText(I)Ljava/lang/CharSequence;
putfield android/support/v7/internal/view/h/l Ljava/lang/CharSequence;
aload 8
aload 7
getstatic android/support/v7/a/n/MenuItem_android_icon I
iconst_0
invokevirtual android/content/res/TypedArray/getResourceId(II)I
putfield android/support/v7/internal/view/h/m I
aload 8
aload 7
getstatic android/support/v7/a/n/MenuItem_android_alphabeticShortcut I
invokevirtual android/content/res/TypedArray/getString(I)Ljava/lang/String;
invokestatic android/support/v7/internal/view/h/a(Ljava/lang/String;)C
putfield android/support/v7/internal/view/h/n C
aload 8
aload 7
getstatic android/support/v7/a/n/MenuItem_android_numericShortcut I
invokevirtual android/content/res/TypedArray/getString(I)Ljava/lang/String;
invokestatic android/support/v7/internal/view/h/a(Ljava/lang/String;)C
putfield android/support/v7/internal/view/h/o C
aload 7
getstatic android/support/v7/a/n/MenuItem_android_checkable I
invokevirtual android/content/res/TypedArray/hasValue(I)Z
ifeq L12
aload 7
getstatic android/support/v7/a/n/MenuItem_android_checkable I
iconst_0
invokevirtual android/content/res/TypedArray/getBoolean(IZ)Z
ifeq L13
iconst_1
istore 6
L14:
aload 8
iload 6
putfield android/support/v7/internal/view/h/p I
L15:
aload 8
aload 7
getstatic android/support/v7/a/n/MenuItem_android_checked I
iconst_0
invokevirtual android/content/res/TypedArray/getBoolean(IZ)Z
putfield android/support/v7/internal/view/h/q Z
aload 8
aload 7
getstatic android/support/v7/a/n/MenuItem_android_visible I
aload 8
getfield android/support/v7/internal/view/h/f Z
invokevirtual android/content/res/TypedArray/getBoolean(IZ)Z
putfield android/support/v7/internal/view/h/r Z
aload 8
aload 7
getstatic android/support/v7/a/n/MenuItem_android_enabled I
aload 8
getfield android/support/v7/internal/view/h/g Z
invokevirtual android/content/res/TypedArray/getBoolean(IZ)Z
putfield android/support/v7/internal/view/h/s Z
aload 8
aload 7
getstatic android/support/v7/a/n/MenuItem_showAsAction I
iconst_m1
invokevirtual android/content/res/TypedArray/getInt(II)I
putfield android/support/v7/internal/view/h/t I
aload 8
aload 7
getstatic android/support/v7/a/n/MenuItem_android_onClick I
invokevirtual android/content/res/TypedArray/getString(I)Ljava/lang/String;
putfield android/support/v7/internal/view/h/x Ljava/lang/String;
aload 8
aload 7
getstatic android/support/v7/a/n/MenuItem_actionLayout I
iconst_0
invokevirtual android/content/res/TypedArray/getResourceId(II)I
putfield android/support/v7/internal/view/h/u I
aload 8
aload 7
getstatic android/support/v7/a/n/MenuItem_actionViewClass I
invokevirtual android/content/res/TypedArray/getString(I)Ljava/lang/String;
putfield android/support/v7/internal/view/h/v Ljava/lang/String;
aload 8
aload 7
getstatic android/support/v7/a/n/MenuItem_actionProviderClass I
invokevirtual android/content/res/TypedArray/getString(I)Ljava/lang/String;
putfield android/support/v7/internal/view/h/w Ljava/lang/String;
aload 8
getfield android/support/v7/internal/view/h/w Ljava/lang/String;
ifnull L16
iconst_1
istore 6
L17:
iload 6
ifeq L18
aload 8
getfield android/support/v7/internal/view/h/u I
ifne L18
aload 8
getfield android/support/v7/internal/view/h/v Ljava/lang/String;
ifnonnull L18
aload 8
aload 8
aload 8
getfield android/support/v7/internal/view/h/w Ljava/lang/String;
getstatic android/support/v7/internal/view/f/g [Ljava/lang/Class;
aload 8
getfield android/support/v7/internal/view/h/z Landroid/support/v7/internal/view/f;
getfield android/support/v7/internal/view/f/i [Ljava/lang/Object;
invokevirtual android/support/v7/internal/view/h/a(Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;
checkcast android/support/v4/view/n
putfield android/support/v7/internal/view/h/y Landroid/support/v4/view/n;
L19:
aload 7
invokevirtual android/content/res/TypedArray/recycle()V
aload 8
iconst_0
putfield android/support/v7/internal/view/h/h Z
goto L9
L13:
iconst_0
istore 6
goto L14
L12:
aload 8
aload 8
getfield android/support/v7/internal/view/h/e I
putfield android/support/v7/internal/view/h/p I
goto L15
L16:
iconst_0
istore 6
goto L17
L18:
iload 6
ifeq L20
ldc "SupportMenuInflater"
ldc "Ignoring attribute 'actionProviderClass'. Action view already specified."
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/String;)I
pop
L20:
aload 8
aconst_null
putfield android/support/v7/internal/view/h/y Landroid/support/v4/view/n;
goto L19
L11:
aload 7
ldc "menu"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L21
aload 0
aload 1
aload 2
aload 8
invokevirtual android/support/v7/internal/view/h/b()Landroid/view/SubMenu;
invokespecial android/support/v7/internal/view/f/a(Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;Landroid/view/Menu;)V
goto L9
L21:
iconst_1
istore 5
aload 7
astore 3
goto L9
L8:
aload 1
invokeinterface org/xmlpull/v1/XmlPullParser/getName()Ljava/lang/String; 0
astore 7
iload 5
ifeq L22
aload 7
aload 3
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L22
iconst_0
istore 5
aconst_null
astore 3
goto L9
L22:
aload 7
ldc "group"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L23
aload 8
invokevirtual android/support/v7/internal/view/h/a()V
goto L9
L23:
aload 7
ldc "item"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L24
aload 8
getfield android/support/v7/internal/view/h/h Z
ifne L9
aload 8
getfield android/support/v7/internal/view/h/y Landroid/support/v4/view/n;
ifnull L25
aload 8
getfield android/support/v7/internal/view/h/y Landroid/support/v4/view/n;
invokevirtual android/support/v4/view/n/f()Z
ifeq L25
aload 8
invokevirtual android/support/v7/internal/view/h/b()Landroid/view/SubMenu;
pop
goto L9
L25:
aload 8
iconst_1
putfield android/support/v7/internal/view/h/h Z
aload 8
aload 8
getfield android/support/v7/internal/view/h/a Landroid/view/Menu;
aload 8
getfield android/support/v7/internal/view/h/b I
aload 8
getfield android/support/v7/internal/view/h/i I
aload 8
getfield android/support/v7/internal/view/h/j I
aload 8
getfield android/support/v7/internal/view/h/k Ljava/lang/CharSequence;
invokeinterface android/view/Menu/add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem; 4
invokevirtual android/support/v7/internal/view/h/a(Landroid/view/MenuItem;)V
goto L9
L24:
aload 7
ldc "menu"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L9
iconst_1
istore 4
goto L9
L6:
new java/lang/RuntimeException
dup
ldc "Unexpected end of document"
invokespecial java/lang/RuntimeException/<init>(Ljava/lang/String;)V
athrow
L5:
return
.limit locals 9
.limit stack 6
.end method

.method static synthetic a()[Ljava/lang/Class;
getstatic android/support/v7/internal/view/f/g [Ljava/lang/Class;
areturn
.limit locals 0
.limit stack 1
.end method

.method static synthetic b()[Ljava/lang/Class;
getstatic android/support/v7/internal/view/f/f [Ljava/lang/Class;
areturn
.limit locals 0
.limit stack 1
.end method

.method static synthetic b(Landroid/support/v7/internal/view/f;)[Ljava/lang/Object;
aload 0
getfield android/support/v7/internal/view/f/i [Ljava/lang/Object;
areturn
.limit locals 1
.limit stack 1
.end method

.method private c()Ljava/lang/Object;
aload 0
getfield android/support/v7/internal/view/f/k Ljava/lang/Object;
ifnonnull L0
aload 0
aload 0
getfield android/support/v7/internal/view/f/j Landroid/content/Context;
invokestatic android/support/v7/internal/view/f/a(Ljava/lang/Object;)Ljava/lang/Object;
putfield android/support/v7/internal/view/f/k Ljava/lang/Object;
L0:
aload 0
getfield android/support/v7/internal/view/f/k Ljava/lang/Object;
areturn
.limit locals 1
.limit stack 2
.end method

.method static synthetic c(Landroid/support/v7/internal/view/f;)Ljava/lang/Object;
aload 0
getfield android/support/v7/internal/view/f/k Ljava/lang/Object;
ifnonnull L0
aload 0
aload 0
getfield android/support/v7/internal/view/f/j Landroid/content/Context;
invokestatic android/support/v7/internal/view/f/a(Ljava/lang/Object;)Ljava/lang/Object;
putfield android/support/v7/internal/view/f/k Ljava/lang/Object;
L0:
aload 0
getfield android/support/v7/internal/view/f/k Ljava/lang/Object;
areturn
.limit locals 1
.limit stack 2
.end method

.method static synthetic d(Landroid/support/v7/internal/view/f;)[Ljava/lang/Object;
aload 0
getfield android/support/v7/internal/view/f/h [Ljava/lang/Object;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final inflate(ILandroid/view/Menu;)V
.catch org/xmlpull/v1/XmlPullParserException from L0 to L1 using L2
.catch java/io/IOException from L0 to L1 using L3
.catch all from L0 to L1 using L4
.catch org/xmlpull/v1/XmlPullParserException from L5 to L6 using L2
.catch java/io/IOException from L5 to L6 using L3
.catch all from L5 to L6 using L4
.catch all from L7 to L4 using L4
.catch all from L8 to L9 using L4
aload 2
instanceof android/support/v4/g/a/a
ifne L10
aload 0
iload 1
aload 2
invokespecial android/view/MenuInflater/inflate(ILandroid/view/Menu;)V
L11:
return
L10:
aconst_null
astore 3
aconst_null
astore 5
aconst_null
astore 4
L0:
aload 0
getfield android/support/v7/internal/view/f/j Landroid/content/Context;
invokevirtual android/content/Context/getResources()Landroid/content/res/Resources;
iload 1
invokevirtual android/content/res/Resources/getLayout(I)Landroid/content/res/XmlResourceParser;
astore 6
L1:
aload 6
astore 4
aload 6
astore 3
aload 6
astore 5
L5:
aload 0
aload 6
aload 6
invokestatic android/util/Xml/asAttributeSet(Lorg/xmlpull/v1/XmlPullParser;)Landroid/util/AttributeSet;
aload 2
invokespecial android/support/v7/internal/view/f/a(Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;Landroid/view/Menu;)V
L6:
aload 6
ifnull L11
aload 6
invokeinterface android/content/res/XmlResourceParser/close()V 0
return
L2:
astore 2
aload 4
astore 3
L7:
new android/view/InflateException
dup
ldc "Error inflating menu XML"
aload 2
invokespecial android/view/InflateException/<init>(Ljava/lang/String;Ljava/lang/Throwable;)V
athrow
L4:
astore 2
aload 3
ifnull L12
aload 3
invokeinterface android/content/res/XmlResourceParser/close()V 0
L12:
aload 2
athrow
L3:
astore 2
aload 5
astore 3
L8:
new android/view/InflateException
dup
ldc "Error inflating menu XML"
aload 2
invokespecial android/view/InflateException/<init>(Ljava/lang/String;Ljava/lang/Throwable;)V
athrow
L9:
.limit locals 7
.limit stack 4
.end method
