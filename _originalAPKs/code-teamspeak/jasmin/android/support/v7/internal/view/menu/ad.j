.bytecode 50.0
.class public synchronized android/support/v7/internal/view/menu/ad
.super android/support/v7/internal/view/menu/i
.implements android/view/SubMenu

.field public 'p' Landroid/support/v7/internal/view/menu/i;

.field private 'q' Landroid/support/v7/internal/view/menu/m;

.method public <init>(Landroid/content/Context;Landroid/support/v7/internal/view/menu/i;Landroid/support/v7/internal/view/menu/m;)V
aload 0
aload 1
invokespecial android/support/v7/internal/view/menu/i/<init>(Landroid/content/Context;)V
aload 0
aload 2
putfield android/support/v7/internal/view/menu/ad/p Landroid/support/v7/internal/view/menu/i;
aload 0
aload 3
putfield android/support/v7/internal/view/menu/ad/q Landroid/support/v7/internal/view/menu/m;
return
.limit locals 4
.limit stack 2
.end method

.method private l()Landroid/view/Menu;
aload 0
getfield android/support/v7/internal/view/menu/ad/p Landroid/support/v7/internal/view/menu/i;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final a()Ljava/lang/String;
aload 0
getfield android/support/v7/internal/view/menu/ad/q Landroid/support/v7/internal/view/menu/m;
ifnull L0
aload 0
getfield android/support/v7/internal/view/menu/ad/q Landroid/support/v7/internal/view/menu/m;
invokevirtual android/support/v7/internal/view/menu/m/getItemId()I
istore 1
L1:
iload 1
ifne L2
aconst_null
areturn
L0:
iconst_0
istore 1
goto L1
L2:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokespecial android/support/v7/internal/view/menu/i/a()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ":"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 1
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 2
.limit stack 2
.end method

.method public final a(Landroid/support/v7/internal/view/menu/j;)V
aload 0
getfield android/support/v7/internal/view/menu/ad/p Landroid/support/v7/internal/view/menu/i;
aload 1
invokevirtual android/support/v7/internal/view/menu/i/a(Landroid/support/v7/internal/view/menu/j;)V
return
.limit locals 2
.limit stack 2
.end method

.method public final a(Z)V
aload 0
getfield android/support/v7/internal/view/menu/ad/p Landroid/support/v7/internal/view/menu/i;
iload 1
invokevirtual android/support/v7/internal/view/menu/i/a(Z)V
return
.limit locals 2
.limit stack 2
.end method

.method final a(Landroid/support/v7/internal/view/menu/i;Landroid/view/MenuItem;)Z
aload 0
aload 1
aload 2
invokespecial android/support/v7/internal/view/menu/i/a(Landroid/support/v7/internal/view/menu/i;Landroid/view/MenuItem;)Z
ifne L0
aload 0
getfield android/support/v7/internal/view/menu/ad/p Landroid/support/v7/internal/view/menu/i;
aload 1
aload 2
invokevirtual android/support/v7/internal/view/menu/i/a(Landroid/support/v7/internal/view/menu/i;Landroid/view/MenuItem;)Z
ifeq L1
L0:
iconst_1
ireturn
L1:
iconst_0
ireturn
.limit locals 3
.limit stack 3
.end method

.method public final a(Landroid/support/v7/internal/view/menu/m;)Z
aload 0
getfield android/support/v7/internal/view/menu/ad/p Landroid/support/v7/internal/view/menu/i;
aload 1
invokevirtual android/support/v7/internal/view/menu/i/a(Landroid/support/v7/internal/view/menu/m;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final b()Z
aload 0
getfield android/support/v7/internal/view/menu/ad/p Landroid/support/v7/internal/view/menu/i;
invokevirtual android/support/v7/internal/view/menu/i/b()Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final b(Landroid/support/v7/internal/view/menu/m;)Z
aload 0
getfield android/support/v7/internal/view/menu/ad/p Landroid/support/v7/internal/view/menu/i;
aload 1
invokevirtual android/support/v7/internal/view/menu/i/b(Landroid/support/v7/internal/view/menu/m;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final c()Z
aload 0
getfield android/support/v7/internal/view/menu/ad/p Landroid/support/v7/internal/view/menu/i;
invokevirtual android/support/v7/internal/view/menu/i/c()Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getItem()Landroid/view/MenuItem;
aload 0
getfield android/support/v7/internal/view/menu/ad/q Landroid/support/v7/internal/view/menu/m;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final k()Landroid/support/v7/internal/view/menu/i;
aload 0
getfield android/support/v7/internal/view/menu/ad/p Landroid/support/v7/internal/view/menu/i;
areturn
.limit locals 1
.limit stack 1
.end method

.method public setHeaderIcon(I)Landroid/view/SubMenu;
aload 0
aload 0
getfield android/support/v7/internal/view/menu/i/e Landroid/content/Context;
iload 1
invokestatic android/support/v4/c/h/a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;
invokespecial android/support/v7/internal/view/menu/i/a(Landroid/graphics/drawable/Drawable;)Landroid/support/v7/internal/view/menu/i;
pop
aload 0
areturn
.limit locals 2
.limit stack 3
.end method

.method public setHeaderIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/SubMenu;
aload 0
aload 1
invokespecial android/support/v7/internal/view/menu/i/a(Landroid/graphics/drawable/Drawable;)Landroid/support/v7/internal/view/menu/i;
pop
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method public setHeaderTitle(I)Landroid/view/SubMenu;
aload 0
aload 0
getfield android/support/v7/internal/view/menu/i/e Landroid/content/Context;
invokevirtual android/content/Context/getResources()Landroid/content/res/Resources;
iload 1
invokevirtual android/content/res/Resources/getString(I)Ljava/lang/String;
invokespecial android/support/v7/internal/view/menu/i/a(Ljava/lang/CharSequence;)Landroid/support/v7/internal/view/menu/i;
pop
aload 0
areturn
.limit locals 2
.limit stack 3
.end method

.method public setHeaderTitle(Ljava/lang/CharSequence;)Landroid/view/SubMenu;
aload 0
aload 1
invokespecial android/support/v7/internal/view/menu/i/a(Ljava/lang/CharSequence;)Landroid/support/v7/internal/view/menu/i;
pop
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method public setHeaderView(Landroid/view/View;)Landroid/view/SubMenu;
aload 0
iconst_0
aconst_null
iconst_0
aconst_null
aload 1
invokespecial android/support/v7/internal/view/menu/i/a(ILjava/lang/CharSequence;ILandroid/graphics/drawable/Drawable;Landroid/view/View;)V
aload 0
areturn
.limit locals 2
.limit stack 6
.end method

.method public setIcon(I)Landroid/view/SubMenu;
aload 0
getfield android/support/v7/internal/view/menu/ad/q Landroid/support/v7/internal/view/menu/m;
iload 1
invokevirtual android/support/v7/internal/view/menu/m/setIcon(I)Landroid/view/MenuItem;
pop
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method public setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/SubMenu;
aload 0
getfield android/support/v7/internal/view/menu/ad/q Landroid/support/v7/internal/view/menu/m;
aload 1
invokevirtual android/support/v7/internal/view/menu/m/setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;
pop
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method public setQwertyMode(Z)V
aload 0
getfield android/support/v7/internal/view/menu/ad/p Landroid/support/v7/internal/view/menu/i;
iload 1
invokevirtual android/support/v7/internal/view/menu/i/setQwertyMode(Z)V
return
.limit locals 2
.limit stack 2
.end method
