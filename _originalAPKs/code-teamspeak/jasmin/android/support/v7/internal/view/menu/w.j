.bytecode 50.0
.class final synchronized android/support/v7/internal/view/menu/w
.super android/widget/BaseAdapter

.field final synthetic 'a' Landroid/support/v7/internal/view/menu/v;

.field private 'b' Landroid/support/v7/internal/view/menu/i;

.field private 'c' I

.method public <init>(Landroid/support/v7/internal/view/menu/v;Landroid/support/v7/internal/view/menu/i;)V
aload 0
aload 1
putfield android/support/v7/internal/view/menu/w/a Landroid/support/v7/internal/view/menu/v;
aload 0
invokespecial android/widget/BaseAdapter/<init>()V
aload 0
iconst_m1
putfield android/support/v7/internal/view/menu/w/c I
aload 0
aload 2
putfield android/support/v7/internal/view/menu/w/b Landroid/support/v7/internal/view/menu/i;
aload 0
invokespecial android/support/v7/internal/view/menu/w/a()V
return
.limit locals 3
.limit stack 2
.end method

.method static synthetic a(Landroid/support/v7/internal/view/menu/w;)Landroid/support/v7/internal/view/menu/i;
aload 0
getfield android/support/v7/internal/view/menu/w/b Landroid/support/v7/internal/view/menu/i;
areturn
.limit locals 1
.limit stack 1
.end method

.method private a()V
aload 0
getfield android/support/v7/internal/view/menu/w/a Landroid/support/v7/internal/view/menu/v;
invokestatic android/support/v7/internal/view/menu/v/c(Landroid/support/v7/internal/view/menu/v;)Landroid/support/v7/internal/view/menu/i;
getfield android/support/v7/internal/view/menu/i/n Landroid/support/v7/internal/view/menu/m;
astore 3
aload 3
ifnull L0
aload 0
getfield android/support/v7/internal/view/menu/w/a Landroid/support/v7/internal/view/menu/v;
invokestatic android/support/v7/internal/view/menu/v/c(Landroid/support/v7/internal/view/menu/v;)Landroid/support/v7/internal/view/menu/i;
invokevirtual android/support/v7/internal/view/menu/i/j()Ljava/util/ArrayList;
astore 4
aload 4
invokevirtual java/util/ArrayList/size()I
istore 2
iconst_0
istore 1
L1:
iload 1
iload 2
if_icmpge L0
aload 4
iload 1
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v7/internal/view/menu/m
aload 3
if_acmpne L2
aload 0
iload 1
putfield android/support/v7/internal/view/menu/w/c I
return
L2:
iload 1
iconst_1
iadd
istore 1
goto L1
L0:
aload 0
iconst_m1
putfield android/support/v7/internal/view/menu/w/c I
return
.limit locals 5
.limit stack 2
.end method

.method public final a(I)Landroid/support/v7/internal/view/menu/m;
aload 0
getfield android/support/v7/internal/view/menu/w/a Landroid/support/v7/internal/view/menu/v;
invokestatic android/support/v7/internal/view/menu/v/a(Landroid/support/v7/internal/view/menu/v;)Z
ifeq L0
aload 0
getfield android/support/v7/internal/view/menu/w/b Landroid/support/v7/internal/view/menu/i;
invokevirtual android/support/v7/internal/view/menu/i/j()Ljava/util/ArrayList;
astore 3
L1:
iload 1
istore 2
aload 0
getfield android/support/v7/internal/view/menu/w/c I
iflt L2
iload 1
istore 2
iload 1
aload 0
getfield android/support/v7/internal/view/menu/w/c I
if_icmplt L2
iload 1
iconst_1
iadd
istore 2
L2:
aload 3
iload 2
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v7/internal/view/menu/m
areturn
L0:
aload 0
getfield android/support/v7/internal/view/menu/w/b Landroid/support/v7/internal/view/menu/i;
invokevirtual android/support/v7/internal/view/menu/i/h()Ljava/util/ArrayList;
astore 3
goto L1
.limit locals 4
.limit stack 2
.end method

.method public final getCount()I
aload 0
getfield android/support/v7/internal/view/menu/w/a Landroid/support/v7/internal/view/menu/v;
invokestatic android/support/v7/internal/view/menu/v/a(Landroid/support/v7/internal/view/menu/v;)Z
ifeq L0
aload 0
getfield android/support/v7/internal/view/menu/w/b Landroid/support/v7/internal/view/menu/i;
invokevirtual android/support/v7/internal/view/menu/i/j()Ljava/util/ArrayList;
astore 1
L1:
aload 0
getfield android/support/v7/internal/view/menu/w/c I
ifge L2
aload 1
invokevirtual java/util/ArrayList/size()I
ireturn
L0:
aload 0
getfield android/support/v7/internal/view/menu/w/b Landroid/support/v7/internal/view/menu/i;
invokevirtual android/support/v7/internal/view/menu/i/h()Ljava/util/ArrayList;
astore 1
goto L1
L2:
aload 1
invokevirtual java/util/ArrayList/size()I
iconst_1
isub
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final synthetic getItem(I)Ljava/lang/Object;
aload 0
iload 1
invokevirtual android/support/v7/internal/view/menu/w/a(I)Landroid/support/v7/internal/view/menu/m;
areturn
.limit locals 2
.limit stack 2
.end method

.method public final getItemId(I)J
iload 1
i2l
lreturn
.limit locals 2
.limit stack 2
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
aload 2
ifnonnull L0
aload 0
getfield android/support/v7/internal/view/menu/w/a Landroid/support/v7/internal/view/menu/v;
invokestatic android/support/v7/internal/view/menu/v/b(Landroid/support/v7/internal/view/menu/v;)Landroid/view/LayoutInflater;
getstatic android/support/v7/internal/view/menu/v/a I
aload 3
iconst_0
invokevirtual android/view/LayoutInflater/inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;
astore 2
L1:
aload 2
checkcast android/support/v7/internal/view/menu/aa
astore 3
aload 0
getfield android/support/v7/internal/view/menu/w/a Landroid/support/v7/internal/view/menu/v;
getfield android/support/v7/internal/view/menu/v/e Z
ifeq L2
aload 2
checkcast android/support/v7/internal/view/menu/ListMenuItemView
iconst_1
invokevirtual android/support/v7/internal/view/menu/ListMenuItemView/setForceShowIcon(Z)V
L2:
aload 3
aload 0
iload 1
invokevirtual android/support/v7/internal/view/menu/w/a(I)Landroid/support/v7/internal/view/menu/m;
invokeinterface android/support/v7/internal/view/menu/aa/a(Landroid/support/v7/internal/view/menu/m;)V 1
aload 2
areturn
L0:
goto L1
.limit locals 4
.limit stack 4
.end method

.method public final notifyDataSetChanged()V
aload 0
invokespecial android/support/v7/internal/view/menu/w/a()V
aload 0
invokespecial android/widget/BaseAdapter/notifyDataSetChanged()V
return
.limit locals 1
.limit stack 1
.end method
