.bytecode 50.0
.class final synchronized android/support/v7/internal/view/menu/h
.super android/widget/BaseAdapter

.field final synthetic 'a' Landroid/support/v7/internal/view/menu/g;

.field private 'b' I

.method public <init>(Landroid/support/v7/internal/view/menu/g;)V
aload 0
aload 1
putfield android/support/v7/internal/view/menu/h/a Landroid/support/v7/internal/view/menu/g;
aload 0
invokespecial android/widget/BaseAdapter/<init>()V
aload 0
iconst_m1
putfield android/support/v7/internal/view/menu/h/b I
aload 0
invokespecial android/support/v7/internal/view/menu/h/a()V
return
.limit locals 2
.limit stack 2
.end method

.method private a()V
aload 0
getfield android/support/v7/internal/view/menu/h/a Landroid/support/v7/internal/view/menu/g;
getfield android/support/v7/internal/view/menu/g/c Landroid/support/v7/internal/view/menu/i;
getfield android/support/v7/internal/view/menu/i/n Landroid/support/v7/internal/view/menu/m;
astore 3
aload 3
ifnull L0
aload 0
getfield android/support/v7/internal/view/menu/h/a Landroid/support/v7/internal/view/menu/g;
getfield android/support/v7/internal/view/menu/g/c Landroid/support/v7/internal/view/menu/i;
invokevirtual android/support/v7/internal/view/menu/i/j()Ljava/util/ArrayList;
astore 4
aload 4
invokevirtual java/util/ArrayList/size()I
istore 2
iconst_0
istore 1
L1:
iload 1
iload 2
if_icmpge L0
aload 4
iload 1
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v7/internal/view/menu/m
aload 3
if_acmpne L2
aload 0
iload 1
putfield android/support/v7/internal/view/menu/h/b I
return
L2:
iload 1
iconst_1
iadd
istore 1
goto L1
L0:
aload 0
iconst_m1
putfield android/support/v7/internal/view/menu/h/b I
return
.limit locals 5
.limit stack 2
.end method

.method public final a(I)Landroid/support/v7/internal/view/menu/m;
aload 0
getfield android/support/v7/internal/view/menu/h/a Landroid/support/v7/internal/view/menu/g;
getfield android/support/v7/internal/view/menu/g/c Landroid/support/v7/internal/view/menu/i;
invokevirtual android/support/v7/internal/view/menu/i/j()Ljava/util/ArrayList;
astore 3
aload 0
getfield android/support/v7/internal/view/menu/h/a Landroid/support/v7/internal/view/menu/g;
invokestatic android/support/v7/internal/view/menu/g/a(Landroid/support/v7/internal/view/menu/g;)I
iload 1
iadd
istore 2
iload 2
istore 1
aload 0
getfield android/support/v7/internal/view/menu/h/b I
iflt L0
iload 2
istore 1
iload 2
aload 0
getfield android/support/v7/internal/view/menu/h/b I
if_icmplt L0
iload 2
iconst_1
iadd
istore 1
L0:
aload 3
iload 1
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v7/internal/view/menu/m
areturn
.limit locals 4
.limit stack 2
.end method

.method public final getCount()I
aload 0
getfield android/support/v7/internal/view/menu/h/a Landroid/support/v7/internal/view/menu/g;
getfield android/support/v7/internal/view/menu/g/c Landroid/support/v7/internal/view/menu/i;
invokevirtual android/support/v7/internal/view/menu/i/j()Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
aload 0
getfield android/support/v7/internal/view/menu/h/a Landroid/support/v7/internal/view/menu/g;
invokestatic android/support/v7/internal/view/menu/g/a(Landroid/support/v7/internal/view/menu/g;)I
isub
istore 1
aload 0
getfield android/support/v7/internal/view/menu/h/b I
ifge L0
iload 1
ireturn
L0:
iload 1
iconst_1
isub
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final synthetic getItem(I)Ljava/lang/Object;
aload 0
iload 1
invokevirtual android/support/v7/internal/view/menu/h/a(I)Landroid/support/v7/internal/view/menu/m;
areturn
.limit locals 2
.limit stack 2
.end method

.method public final getItemId(I)J
iload 1
i2l
lreturn
.limit locals 2
.limit stack 2
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
aload 2
ifnonnull L0
aload 0
getfield android/support/v7/internal/view/menu/h/a Landroid/support/v7/internal/view/menu/g;
getfield android/support/v7/internal/view/menu/g/b Landroid/view/LayoutInflater;
aload 0
getfield android/support/v7/internal/view/menu/h/a Landroid/support/v7/internal/view/menu/g;
getfield android/support/v7/internal/view/menu/g/f I
aload 3
iconst_0
invokevirtual android/view/LayoutInflater/inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;
astore 2
L1:
aload 2
checkcast android/support/v7/internal/view/menu/aa
aload 0
iload 1
invokevirtual android/support/v7/internal/view/menu/h/a(I)Landroid/support/v7/internal/view/menu/m;
invokeinterface android/support/v7/internal/view/menu/aa/a(Landroid/support/v7/internal/view/menu/m;)V 1
aload 2
areturn
L0:
goto L1
.limit locals 4
.limit stack 4
.end method

.method public final notifyDataSetChanged()V
aload 0
invokespecial android/support/v7/internal/view/menu/h/a()V
aload 0
invokespecial android/widget/BaseAdapter/notifyDataSetChanged()V
return
.limit locals 1
.limit stack 1
.end method
