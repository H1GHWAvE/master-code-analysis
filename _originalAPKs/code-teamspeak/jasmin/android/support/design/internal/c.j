.bytecode 50.0
.class public final synchronized android/support/design/internal/c
.super android/widget/BaseAdapter

.field private static final 'e' Ljava/lang/String; = "android:menu:checked"

.field private static final 'f' I = 0


.field private static final 'g' I = 1


.field private static final 'h' I = 2


.field final 'a' Ljava/util/ArrayList;

.field 'b' Landroid/support/v7/internal/view/menu/m;

.field 'c' Z

.field final synthetic 'd' Landroid/support/design/internal/b;

.field private 'i' Landroid/graphics/drawable/ColorDrawable;

.method <init>(Landroid/support/design/internal/b;)V
aload 0
aload 1
putfield android/support/design/internal/c/d Landroid/support/design/internal/b;
aload 0
invokespecial android/widget/BaseAdapter/<init>()V
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield android/support/design/internal/c/a Ljava/util/ArrayList;
aload 0
invokevirtual android/support/design/internal/c/a()V
return
.limit locals 2
.limit stack 3
.end method

.method private a(II)V
L0:
iload 1
iload 2
if_icmpge L1
aload 0
getfield android/support/design/internal/c/a Ljava/util/ArrayList;
iload 1
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/design/internal/d
getfield android/support/design/internal/d/a Landroid/support/v7/internal/view/menu/m;
astore 3
aload 3
invokeinterface android/view/MenuItem/getIcon()Landroid/graphics/drawable/Drawable; 0
ifnonnull L2
aload 0
getfield android/support/design/internal/c/i Landroid/graphics/drawable/ColorDrawable;
ifnonnull L3
aload 0
new android/graphics/drawable/ColorDrawable
dup
ldc_w 17170445
invokespecial android/graphics/drawable/ColorDrawable/<init>(I)V
putfield android/support/design/internal/c/i Landroid/graphics/drawable/ColorDrawable;
L3:
aload 3
aload 0
getfield android/support/design/internal/c/i Landroid/graphics/drawable/ColorDrawable;
invokeinterface android/view/MenuItem/setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem; 1
pop
L2:
iload 1
iconst_1
iadd
istore 1
goto L0
L1:
return
.limit locals 4
.limit stack 4
.end method

.method private a(Landroid/os/Bundle;)V
aload 1
ldc "android:menu:checked"
iconst_0
invokevirtual android/os/Bundle/getInt(Ljava/lang/String;I)I
istore 2
iload 2
ifeq L0
aload 0
iconst_1
putfield android/support/design/internal/c/c Z
aload 0
getfield android/support/design/internal/c/a Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 1
L1:
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L2
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast android/support/design/internal/d
getfield android/support/design/internal/d/a Landroid/support/v7/internal/view/menu/m;
astore 3
aload 3
ifnull L1
aload 3
invokevirtual android/support/v7/internal/view/menu/m/getItemId()I
iload 2
if_icmpne L1
aload 0
aload 3
invokevirtual android/support/design/internal/c/a(Landroid/support/v7/internal/view/menu/m;)V
L2:
aload 0
iconst_0
putfield android/support/design/internal/c/c Z
aload 0
invokevirtual android/support/design/internal/c/a()V
L0:
return
.limit locals 4
.limit stack 3
.end method

.method private a(Z)V
aload 0
iload 1
putfield android/support/design/internal/c/c Z
return
.limit locals 2
.limit stack 2
.end method

.method private b()Landroid/os/Bundle;
new android/os/Bundle
dup
invokespecial android/os/Bundle/<init>()V
astore 1
aload 0
getfield android/support/design/internal/c/b Landroid/support/v7/internal/view/menu/m;
ifnull L0
aload 1
ldc "android:menu:checked"
aload 0
getfield android/support/design/internal/c/b Landroid/support/v7/internal/view/menu/m;
invokevirtual android/support/v7/internal/view/menu/m/getItemId()I
invokevirtual android/os/Bundle/putInt(Ljava/lang/String;I)V
L0:
aload 1
areturn
.limit locals 2
.limit stack 3
.end method

.method public final a(I)Landroid/support/design/internal/d;
aload 0
getfield android/support/design/internal/c/a Ljava/util/ArrayList;
iload 1
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/design/internal/d
areturn
.limit locals 2
.limit stack 2
.end method

.method final a()V
aload 0
getfield android/support/design/internal/c/c Z
ifeq L0
return
L0:
aload 0
iconst_1
putfield android/support/design/internal/c/c Z
aload 0
getfield android/support/design/internal/c/a Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/clear()V
iconst_m1
istore 3
aload 0
getfield android/support/design/internal/c/d Landroid/support/design/internal/b;
invokestatic android/support/design/internal/b/g(Landroid/support/design/internal/b;)Landroid/support/v7/internal/view/menu/i;
invokevirtual android/support/v7/internal/view/menu/i/h()Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
istore 8
iconst_0
istore 6
iconst_0
istore 2
iconst_0
istore 1
L1:
iload 6
iload 8
if_icmpge L2
aload 0
getfield android/support/design/internal/c/d Landroid/support/design/internal/b;
invokestatic android/support/design/internal/b/g(Landroid/support/design/internal/b;)Landroid/support/v7/internal/view/menu/i;
invokevirtual android/support/v7/internal/view/menu/i/h()Ljava/util/ArrayList;
iload 6
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v7/internal/view/menu/m
astore 11
aload 11
invokevirtual android/support/v7/internal/view/menu/m/isChecked()Z
ifeq L3
aload 0
aload 11
invokevirtual android/support/design/internal/c/a(Landroid/support/v7/internal/view/menu/m;)V
L3:
aload 11
invokevirtual android/support/v7/internal/view/menu/m/isCheckable()Z
ifeq L4
aload 11
iconst_0
invokevirtual android/support/v7/internal/view/menu/m/a(Z)V
L4:
aload 11
invokevirtual android/support/v7/internal/view/menu/m/hasSubMenu()Z
ifeq L5
aload 11
invokevirtual android/support/v7/internal/view/menu/m/getSubMenu()Landroid/view/SubMenu;
astore 12
aload 12
invokeinterface android/view/SubMenu/hasVisibleItems()Z 0
ifeq L6
iload 6
ifeq L7
aload 0
getfield android/support/design/internal/c/a Ljava/util/ArrayList;
aload 0
getfield android/support/design/internal/c/d Landroid/support/design/internal/b;
invokestatic android/support/design/internal/b/h(Landroid/support/design/internal/b;)I
iconst_0
invokestatic android/support/design/internal/d/a(II)Landroid/support/design/internal/d;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L7:
aload 0
getfield android/support/design/internal/c/a Ljava/util/ArrayList;
aload 11
invokestatic android/support/design/internal/d/a(Landroid/support/v7/internal/view/menu/m;)Landroid/support/design/internal/d;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 0
getfield android/support/design/internal/c/a Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
istore 9
aload 12
invokeinterface android/view/SubMenu/size()I 0
istore 10
iconst_0
istore 7
iconst_0
istore 4
L8:
iload 7
iload 10
if_icmpge L9
aload 12
iload 7
invokeinterface android/view/SubMenu/getItem(I)Landroid/view/MenuItem; 1
checkcast android/support/v7/internal/view/menu/m
astore 13
iload 4
istore 5
aload 13
invokevirtual android/support/v7/internal/view/menu/m/isVisible()Z
ifeq L10
iload 4
istore 5
iload 4
ifne L11
iload 4
istore 5
aload 13
invokevirtual android/support/v7/internal/view/menu/m/getIcon()Landroid/graphics/drawable/Drawable;
ifnull L11
iconst_1
istore 5
L11:
aload 13
invokevirtual android/support/v7/internal/view/menu/m/isCheckable()Z
ifeq L12
aload 13
iconst_0
invokevirtual android/support/v7/internal/view/menu/m/a(Z)V
L12:
aload 11
invokevirtual android/support/v7/internal/view/menu/m/isChecked()Z
ifeq L13
aload 0
aload 11
invokevirtual android/support/design/internal/c/a(Landroid/support/v7/internal/view/menu/m;)V
L13:
aload 0
getfield android/support/design/internal/c/a Ljava/util/ArrayList;
aload 13
invokestatic android/support/design/internal/d/a(Landroid/support/v7/internal/view/menu/m;)Landroid/support/design/internal/d;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L10:
iload 7
iconst_1
iadd
istore 7
iload 5
istore 4
goto L8
L9:
iload 4
ifeq L6
aload 0
iload 9
aload 0
getfield android/support/design/internal/c/a Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
invokespecial android/support/design/internal/c/a(II)V
L6:
iload 2
istore 4
iload 3
istore 2
iload 1
istore 3
iload 4
istore 1
L14:
iload 6
iconst_1
iadd
istore 6
iload 2
istore 4
iload 1
istore 2
iload 3
istore 1
iload 4
istore 3
goto L1
L5:
aload 11
invokevirtual android/support/v7/internal/view/menu/m/getGroupId()I
istore 5
iload 5
iload 3
if_icmpeq L15
aload 0
getfield android/support/design/internal/c/a Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
istore 2
aload 11
invokevirtual android/support/v7/internal/view/menu/m/getIcon()Landroid/graphics/drawable/Drawable;
ifnull L16
iconst_1
istore 1
L17:
iload 1
istore 4
iload 2
istore 3
iload 6
ifeq L18
aload 0
getfield android/support/design/internal/c/a Ljava/util/ArrayList;
aload 0
getfield android/support/design/internal/c/d Landroid/support/design/internal/b;
invokestatic android/support/design/internal/b/h(Landroid/support/design/internal/b;)I
aload 0
getfield android/support/design/internal/c/d Landroid/support/design/internal/b;
invokestatic android/support/design/internal/b/h(Landroid/support/design/internal/b;)I
invokestatic android/support/design/internal/d/a(II)Landroid/support/design/internal/d;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
iload 2
iconst_1
iadd
istore 2
L19:
iload 1
ifeq L20
aload 11
invokevirtual android/support/v7/internal/view/menu/m/getIcon()Landroid/graphics/drawable/Drawable;
ifnonnull L20
aload 11
ldc_w 17170445
invokevirtual android/support/v7/internal/view/menu/m/setIcon(I)Landroid/view/MenuItem;
pop
L20:
aload 0
getfield android/support/design/internal/c/a Ljava/util/ArrayList;
aload 11
invokestatic android/support/design/internal/d/a(Landroid/support/v7/internal/view/menu/m;)Landroid/support/design/internal/d;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
iload 5
istore 4
iload 2
istore 3
iload 4
istore 2
goto L14
L16:
iconst_0
istore 1
goto L17
L15:
iload 2
istore 4
iload 1
istore 3
iload 2
ifne L18
iload 2
istore 4
iload 1
istore 3
aload 11
invokevirtual android/support/v7/internal/view/menu/m/getIcon()Landroid/graphics/drawable/Drawable;
ifnull L18
aload 0
iload 1
aload 0
getfield android/support/design/internal/c/a Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
invokespecial android/support/design/internal/c/a(II)V
iconst_1
istore 3
iload 1
istore 2
iload 3
istore 1
goto L19
L2:
aload 0
iconst_0
putfield android/support/design/internal/c/c Z
return
L18:
iload 4
istore 1
iload 3
istore 2
goto L19
.limit locals 14
.limit stack 3
.end method

.method public final a(Landroid/support/v7/internal/view/menu/m;)V
aload 0
getfield android/support/design/internal/c/b Landroid/support/v7/internal/view/menu/m;
aload 1
if_acmpeq L0
aload 1
invokevirtual android/support/v7/internal/view/menu/m/isCheckable()Z
ifne L1
L0:
return
L1:
aload 0
getfield android/support/design/internal/c/b Landroid/support/v7/internal/view/menu/m;
ifnull L2
aload 0
getfield android/support/design/internal/c/b Landroid/support/v7/internal/view/menu/m;
iconst_0
invokevirtual android/support/v7/internal/view/menu/m/setChecked(Z)Landroid/view/MenuItem;
pop
L2:
aload 0
aload 1
putfield android/support/design/internal/c/b Landroid/support/v7/internal/view/menu/m;
aload 1
iconst_1
invokevirtual android/support/v7/internal/view/menu/m/setChecked(Z)Landroid/view/MenuItem;
pop
return
.limit locals 2
.limit stack 2
.end method

.method public final areAllItemsEnabled()Z
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final getCount()I
aload 0
getfield android/support/design/internal/c/a Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final synthetic getItem(I)Ljava/lang/Object;
aload 0
iload 1
invokevirtual android/support/design/internal/c/a(I)Landroid/support/design/internal/d;
areturn
.limit locals 2
.limit stack 2
.end method

.method public final getItemId(I)J
iload 1
i2l
lreturn
.limit locals 2
.limit stack 2
.end method

.method public final getItemViewType(I)I
iconst_1
istore 2
aload 0
iload 1
invokevirtual android/support/design/internal/c/a(I)Landroid/support/design/internal/d;
astore 3
aload 3
getfield android/support/design/internal/d/a Landroid/support/v7/internal/view/menu/m;
ifnonnull L0
iconst_1
istore 1
L1:
iload 1
ifeq L2
iconst_2
istore 1
L3:
iload 1
ireturn
L0:
iconst_0
istore 1
goto L1
L2:
iload 2
istore 1
aload 3
getfield android/support/design/internal/d/a Landroid/support/v7/internal/view/menu/m;
invokevirtual android/support/v7/internal/view/menu/m/hasSubMenu()Z
ifne L3
iconst_0
ireturn
.limit locals 4
.limit stack 2
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
aload 0
iload 1
invokevirtual android/support/design/internal/c/a(I)Landroid/support/design/internal/d;
astore 5
aload 0
iload 1
invokevirtual android/support/design/internal/c/getItemViewType(I)I
tableswitch 0
L0
L1
L2
default : L3
L3:
aload 2
areturn
L0:
aload 2
ifnonnull L4
aload 0
getfield android/support/design/internal/c/d Landroid/support/design/internal/b;
invokestatic android/support/design/internal/b/a(Landroid/support/design/internal/b;)Landroid/view/LayoutInflater;
getstatic android/support/design/k/design_navigation_item I
aload 3
iconst_0
invokevirtual android/view/LayoutInflater/inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;
astore 2
L5:
aload 2
checkcast android/support/design/internal/NavigationMenuItemView
astore 4
aload 4
aload 0
getfield android/support/design/internal/c/d Landroid/support/design/internal/b;
invokestatic android/support/design/internal/b/b(Landroid/support/design/internal/b;)Landroid/content/res/ColorStateList;
invokevirtual android/support/design/internal/NavigationMenuItemView/setIconTintList(Landroid/content/res/ColorStateList;)V
aload 0
getfield android/support/design/internal/c/d Landroid/support/design/internal/b;
invokestatic android/support/design/internal/b/c(Landroid/support/design/internal/b;)Z
ifeq L6
aload 4
aload 4
invokevirtual android/support/design/internal/NavigationMenuItemView/getContext()Landroid/content/Context;
aload 0
getfield android/support/design/internal/c/d Landroid/support/design/internal/b;
invokestatic android/support/design/internal/b/d(Landroid/support/design/internal/b;)I
invokevirtual android/support/design/internal/NavigationMenuItemView/setTextAppearance(Landroid/content/Context;I)V
L6:
aload 0
getfield android/support/design/internal/c/d Landroid/support/design/internal/b;
invokestatic android/support/design/internal/b/e(Landroid/support/design/internal/b;)Landroid/content/res/ColorStateList;
ifnull L7
aload 4
aload 0
getfield android/support/design/internal/c/d Landroid/support/design/internal/b;
invokestatic android/support/design/internal/b/e(Landroid/support/design/internal/b;)Landroid/content/res/ColorStateList;
invokevirtual android/support/design/internal/NavigationMenuItemView/setTextColor(Landroid/content/res/ColorStateList;)V
L7:
aload 0
getfield android/support/design/internal/c/d Landroid/support/design/internal/b;
invokestatic android/support/design/internal/b/f(Landroid/support/design/internal/b;)Landroid/graphics/drawable/Drawable;
ifnull L8
aload 0
getfield android/support/design/internal/c/d Landroid/support/design/internal/b;
invokestatic android/support/design/internal/b/f(Landroid/support/design/internal/b;)Landroid/graphics/drawable/Drawable;
invokevirtual android/graphics/drawable/Drawable/getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;
invokevirtual android/graphics/drawable/Drawable$ConstantState/newDrawable()Landroid/graphics/drawable/Drawable;
astore 3
L9:
aload 4
aload 3
invokevirtual android/support/design/internal/NavigationMenuItemView/setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
aload 4
aload 5
getfield android/support/design/internal/d/a Landroid/support/v7/internal/view/menu/m;
invokevirtual android/support/design/internal/NavigationMenuItemView/a(Landroid/support/v7/internal/view/menu/m;)V
aload 2
areturn
L8:
aconst_null
astore 3
goto L9
L1:
aload 2
ifnonnull L10
aload 0
getfield android/support/design/internal/c/d Landroid/support/design/internal/b;
invokestatic android/support/design/internal/b/a(Landroid/support/design/internal/b;)Landroid/view/LayoutInflater;
getstatic android/support/design/k/design_navigation_item_subheader I
aload 3
iconst_0
invokevirtual android/view/LayoutInflater/inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;
astore 2
L11:
aload 2
checkcast android/widget/TextView
aload 5
getfield android/support/design/internal/d/a Landroid/support/v7/internal/view/menu/m;
invokevirtual android/support/v7/internal/view/menu/m/getTitle()Ljava/lang/CharSequence;
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
aload 2
areturn
L2:
aload 2
astore 4
aload 2
ifnonnull L12
aload 0
getfield android/support/design/internal/c/d Landroid/support/design/internal/b;
invokestatic android/support/design/internal/b/a(Landroid/support/design/internal/b;)Landroid/view/LayoutInflater;
getstatic android/support/design/k/design_navigation_item_separator I
aload 3
iconst_0
invokevirtual android/view/LayoutInflater/inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;
astore 4
L12:
aload 4
iconst_0
aload 5
getfield android/support/design/internal/d/b I
iconst_0
aload 5
getfield android/support/design/internal/d/c I
invokevirtual android/view/View/setPadding(IIII)V
aload 4
astore 2
goto L3
L10:
goto L11
L4:
goto L5
.limit locals 6
.limit stack 5
.end method

.method public final getViewTypeCount()I
iconst_3
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final isEnabled(I)Z
aload 0
iload 1
invokevirtual android/support/design/internal/c/a(I)Landroid/support/design/internal/d;
astore 2
aload 2
getfield android/support/design/internal/d/a Landroid/support/v7/internal/view/menu/m;
ifnull L0
aload 2
getfield android/support/design/internal/d/a Landroid/support/v7/internal/view/menu/m;
invokevirtual android/support/v7/internal/view/menu/m/hasSubMenu()Z
ifne L0
aload 2
getfield android/support/design/internal/d/a Landroid/support/v7/internal/view/menu/m;
invokevirtual android/support/v7/internal/view/menu/m/isEnabled()Z
ifeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 3
.limit stack 2
.end method

.method public final notifyDataSetChanged()V
aload 0
invokevirtual android/support/design/internal/c/a()V
aload 0
invokespecial android/widget/BaseAdapter/notifyDataSetChanged()V
return
.limit locals 1
.limit stack 1
.end method
