.bytecode 50.0
.class public synchronized android/support/design/internal/NavigationMenuItemView
.super android/widget/TextView
.implements android/support/v7/internal/view/menu/aa

.field private static final 'a' [I

.field private 'b' I

.field private 'c' Landroid/support/v7/internal/view/menu/m;

.field private 'd' Landroid/content/res/ColorStateList;

.method static <clinit>()V
iconst_1
newarray int
dup
iconst_0
ldc_w 16842912
iastore
putstatic android/support/design/internal/NavigationMenuItemView/a [I
return
.limit locals 0
.limit stack 4
.end method

.method public <init>(Landroid/content/Context;)V
aload 0
aload 1
aconst_null
invokespecial android/support/design/internal/NavigationMenuItemView/<init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
return
.limit locals 2
.limit stack 3
.end method

.method public <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
aload 0
aload 1
aload 2
iconst_0
invokespecial android/support/design/internal/NavigationMenuItemView/<init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
return
.limit locals 3
.limit stack 4
.end method

.method public <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
aload 0
aload 1
aload 2
iload 3
invokespecial android/widget/TextView/<init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
aload 0
aload 1
invokevirtual android/content/Context/getResources()Landroid/content/res/Resources;
getstatic android/support/design/g/design_navigation_icon_size I
invokevirtual android/content/res/Resources/getDimensionPixelSize(I)I
putfield android/support/design/internal/NavigationMenuItemView/b I
return
.limit locals 4
.limit stack 4
.end method

.method private c()Landroid/graphics/drawable/StateListDrawable;
new android/util/TypedValue
dup
invokespecial android/util/TypedValue/<init>()V
astore 1
aload 0
invokevirtual android/support/design/internal/NavigationMenuItemView/getContext()Landroid/content/Context;
invokevirtual android/content/Context/getTheme()Landroid/content/res/Resources$Theme;
getstatic android/support/design/d/colorControlHighlight I
aload 1
iconst_1
invokevirtual android/content/res/Resources$Theme/resolveAttribute(ILandroid/util/TypedValue;Z)Z
ifeq L0
new android/graphics/drawable/StateListDrawable
dup
invokespecial android/graphics/drawable/StateListDrawable/<init>()V
astore 2
aload 2
getstatic android/support/design/internal/NavigationMenuItemView/a [I
new android/graphics/drawable/ColorDrawable
dup
aload 1
getfield android/util/TypedValue/data I
invokespecial android/graphics/drawable/ColorDrawable/<init>(I)V
invokevirtual android/graphics/drawable/StateListDrawable/addState([ILandroid/graphics/drawable/Drawable;)V
aload 2
getstatic android/support/design/internal/NavigationMenuItemView/EMPTY_STATE_SET [I
new android/graphics/drawable/ColorDrawable
dup
iconst_0
invokespecial android/graphics/drawable/ColorDrawable/<init>(I)V
invokevirtual android/graphics/drawable/StateListDrawable/addState([ILandroid/graphics/drawable/Drawable;)V
aload 2
areturn
L0:
aconst_null
areturn
.limit locals 3
.limit stack 5
.end method

.method public final a(Landroid/support/v7/internal/view/menu/m;)V
aload 0
aload 1
putfield android/support/design/internal/NavigationMenuItemView/c Landroid/support/v7/internal/view/menu/m;
aload 1
invokevirtual android/support/v7/internal/view/menu/m/isVisible()Z
ifeq L0
iconst_0
istore 2
L1:
aload 0
iload 2
invokevirtual android/support/design/internal/NavigationMenuItemView/setVisibility(I)V
aload 0
invokevirtual android/support/design/internal/NavigationMenuItemView/getBackground()Landroid/graphics/drawable/Drawable;
ifnonnull L2
new android/util/TypedValue
dup
invokespecial android/util/TypedValue/<init>()V
astore 4
aload 0
invokevirtual android/support/design/internal/NavigationMenuItemView/getContext()Landroid/content/Context;
invokevirtual android/content/Context/getTheme()Landroid/content/res/Resources$Theme;
getstatic android/support/design/d/colorControlHighlight I
aload 4
iconst_1
invokevirtual android/content/res/Resources$Theme/resolveAttribute(ILandroid/util/TypedValue;Z)Z
ifeq L3
new android/graphics/drawable/StateListDrawable
dup
invokespecial android/graphics/drawable/StateListDrawable/<init>()V
astore 3
aload 3
getstatic android/support/design/internal/NavigationMenuItemView/a [I
new android/graphics/drawable/ColorDrawable
dup
aload 4
getfield android/util/TypedValue/data I
invokespecial android/graphics/drawable/ColorDrawable/<init>(I)V
invokevirtual android/graphics/drawable/StateListDrawable/addState([ILandroid/graphics/drawable/Drawable;)V
aload 3
getstatic android/support/design/internal/NavigationMenuItemView/EMPTY_STATE_SET [I
new android/graphics/drawable/ColorDrawable
dup
iconst_0
invokespecial android/graphics/drawable/ColorDrawable/<init>(I)V
invokevirtual android/graphics/drawable/StateListDrawable/addState([ILandroid/graphics/drawable/Drawable;)V
L4:
aload 0
aload 3
invokevirtual android/support/design/internal/NavigationMenuItemView/setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
L2:
aload 0
aload 1
invokevirtual android/support/v7/internal/view/menu/m/isCheckable()Z
invokevirtual android/support/design/internal/NavigationMenuItemView/setCheckable(Z)V
aload 0
aload 1
invokevirtual android/support/v7/internal/view/menu/m/isChecked()Z
invokevirtual android/support/design/internal/NavigationMenuItemView/setChecked(Z)V
aload 0
aload 1
invokevirtual android/support/v7/internal/view/menu/m/isEnabled()Z
invokevirtual android/support/design/internal/NavigationMenuItemView/setEnabled(Z)V
aload 0
aload 1
invokevirtual android/support/v7/internal/view/menu/m/getTitle()Ljava/lang/CharSequence;
invokevirtual android/support/design/internal/NavigationMenuItemView/setTitle(Ljava/lang/CharSequence;)V
aload 0
aload 1
invokevirtual android/support/v7/internal/view/menu/m/getIcon()Landroid/graphics/drawable/Drawable;
invokevirtual android/support/design/internal/NavigationMenuItemView/setIcon(Landroid/graphics/drawable/Drawable;)V
return
L0:
bipush 8
istore 2
goto L1
L3:
aconst_null
astore 3
goto L4
.limit locals 5
.limit stack 5
.end method

.method public final a()Z
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final b()Z
iconst_1
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getItemData()Landroid/support/v7/internal/view/menu/m;
aload 0
getfield android/support/design/internal/NavigationMenuItemView/c Landroid/support/v7/internal/view/menu/m;
areturn
.limit locals 1
.limit stack 1
.end method

.method protected onCreateDrawableState(I)[I
aload 0
iload 1
iconst_1
iadd
invokespecial android/widget/TextView/onCreateDrawableState(I)[I
astore 2
aload 0
getfield android/support/design/internal/NavigationMenuItemView/c Landroid/support/v7/internal/view/menu/m;
ifnull L0
aload 0
getfield android/support/design/internal/NavigationMenuItemView/c Landroid/support/v7/internal/view/menu/m;
invokevirtual android/support/v7/internal/view/menu/m/isCheckable()Z
ifeq L0
aload 0
getfield android/support/design/internal/NavigationMenuItemView/c Landroid/support/v7/internal/view/menu/m;
invokevirtual android/support/v7/internal/view/menu/m/isChecked()Z
ifeq L0
aload 2
getstatic android/support/design/internal/NavigationMenuItemView/a [I
invokestatic android/support/design/internal/NavigationMenuItemView/mergeDrawableStates([I[I)[I
pop
L0:
aload 2
areturn
.limit locals 3
.limit stack 3
.end method

.method public setCheckable(Z)V
aload 0
invokevirtual android/support/design/internal/NavigationMenuItemView/refreshDrawableState()V
return
.limit locals 2
.limit stack 1
.end method

.method public setChecked(Z)V
aload 0
invokevirtual android/support/design/internal/NavigationMenuItemView/refreshDrawableState()V
return
.limit locals 2
.limit stack 1
.end method

.method public setIcon(Landroid/graphics/drawable/Drawable;)V
aload 1
astore 2
aload 1
ifnull L0
aload 1
invokevirtual android/graphics/drawable/Drawable/getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;
invokevirtual android/graphics/drawable/Drawable$ConstantState/newDrawable()Landroid/graphics/drawable/Drawable;
invokestatic android/support/v4/e/a/a/c(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;
invokevirtual android/graphics/drawable/Drawable/mutate()Landroid/graphics/drawable/Drawable;
astore 2
aload 2
iconst_0
iconst_0
aload 0
getfield android/support/design/internal/NavigationMenuItemView/b I
aload 0
getfield android/support/design/internal/NavigationMenuItemView/b I
invokevirtual android/graphics/drawable/Drawable/setBounds(IIII)V
aload 2
aload 0
getfield android/support/design/internal/NavigationMenuItemView/d Landroid/content/res/ColorStateList;
invokestatic android/support/v4/e/a/a/a(Landroid/graphics/drawable/Drawable;Landroid/content/res/ColorStateList;)V
L0:
aload 0
aload 2
invokestatic android/support/v4/widget/dy/a(Landroid/widget/TextView;Landroid/graphics/drawable/Drawable;)V
return
.limit locals 3
.limit stack 5
.end method

.method setIconTintList(Landroid/content/res/ColorStateList;)V
aload 0
aload 1
putfield android/support/design/internal/NavigationMenuItemView/d Landroid/content/res/ColorStateList;
aload 0
getfield android/support/design/internal/NavigationMenuItemView/c Landroid/support/v7/internal/view/menu/m;
ifnull L0
aload 0
aload 0
getfield android/support/design/internal/NavigationMenuItemView/c Landroid/support/v7/internal/view/menu/m;
invokevirtual android/support/v7/internal/view/menu/m/getIcon()Landroid/graphics/drawable/Drawable;
invokevirtual android/support/design/internal/NavigationMenuItemView/setIcon(Landroid/graphics/drawable/Drawable;)V
L0:
return
.limit locals 2
.limit stack 2
.end method

.method public final setShortcut$25d965e(Z)V
return
.limit locals 2
.limit stack 0
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
aload 0
aload 1
invokevirtual android/support/design/internal/NavigationMenuItemView/setText(Ljava/lang/CharSequence;)V
return
.limit locals 2
.limit stack 2
.end method
