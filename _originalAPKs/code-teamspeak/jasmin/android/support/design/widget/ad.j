.bytecode 50.0
.class synchronized android/support/design/widget/ad
.super android/support/design/widget/al

.field 'a' Landroid/support/design/widget/ar;

.field private 'h' Landroid/graphics/drawable/Drawable;

.field private 'i' Landroid/graphics/drawable/Drawable;

.field private 'j' Landroid/graphics/drawable/Drawable;

.field private 'k' F

.field private 'l' F

.field private 'm' I

.field private 'n' Landroid/support/design/widget/bl;

.field private 'o' Z

.method <init>(Landroid/view/View;Landroid/support/design/widget/as;)V
aload 0
aload 1
aload 2
invokespecial android/support/design/widget/al/<init>(Landroid/view/View;Landroid/support/design/widget/as;)V
aload 0
aload 1
invokevirtual android/view/View/getResources()Landroid/content/res/Resources;
ldc_w 17694720
invokevirtual android/content/res/Resources/getInteger(I)I
putfield android/support/design/widget/ad/m I
aload 0
new android/support/design/widget/bl
dup
invokespecial android/support/design/widget/bl/<init>()V
putfield android/support/design/widget/ad/n Landroid/support/design/widget/bl;
aload 0
getfield android/support/design/widget/ad/n Landroid/support/design/widget/bl;
astore 2
aload 2
invokevirtual android/support/design/widget/bl/a()Landroid/view/View;
astore 5
aload 5
aload 1
if_acmpeq L0
aload 5
ifnull L1
aload 2
invokevirtual android/support/design/widget/bl/a()Landroid/view/View;
astore 5
aload 2
getfield android/support/design/widget/bl/a Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
istore 4
iconst_0
istore 3
L2:
iload 3
iload 4
if_icmpge L3
aload 2
getfield android/support/design/widget/bl/a Ljava/util/ArrayList;
iload 3
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/design/widget/bn
getfield android/support/design/widget/bn/b Landroid/view/animation/Animation;
astore 6
aload 5
invokevirtual android/view/View/getAnimation()Landroid/view/animation/Animation;
aload 6
if_acmpne L4
aload 5
invokevirtual android/view/View/clearAnimation()V
L4:
iload 3
iconst_1
iadd
istore 3
goto L2
L3:
aload 2
aconst_null
putfield android/support/design/widget/bl/d Ljava/lang/ref/WeakReference;
aload 2
aconst_null
putfield android/support/design/widget/bl/b Landroid/support/design/widget/bn;
aload 2
aconst_null
putfield android/support/design/widget/bl/c Landroid/view/animation/Animation;
L1:
aload 1
ifnull L0
aload 2
new java/lang/ref/WeakReference
dup
aload 1
invokespecial java/lang/ref/WeakReference/<init>(Ljava/lang/Object;)V
putfield android/support/design/widget/bl/d Ljava/lang/ref/WeakReference;
L0:
aload 0
getfield android/support/design/widget/ad/n Landroid/support/design/widget/bl;
getstatic android/support/design/widget/ad/c [I
aload 0
new android/support/design/widget/ag
dup
aload 0
iconst_0
invokespecial android/support/design/widget/ag/<init>(Landroid/support/design/widget/ad;B)V
invokespecial android/support/design/widget/ad/a(Landroid/view/animation/Animation;)Landroid/view/animation/Animation;
invokevirtual android/support/design/widget/bl/a([ILandroid/view/animation/Animation;)V
aload 0
getfield android/support/design/widget/ad/n Landroid/support/design/widget/bl;
getstatic android/support/design/widget/ad/d [I
aload 0
new android/support/design/widget/ag
dup
aload 0
iconst_0
invokespecial android/support/design/widget/ag/<init>(Landroid/support/design/widget/ad;B)V
invokespecial android/support/design/widget/ad/a(Landroid/view/animation/Animation;)Landroid/view/animation/Animation;
invokevirtual android/support/design/widget/bl/a([ILandroid/view/animation/Animation;)V
aload 0
getfield android/support/design/widget/ad/n Landroid/support/design/widget/bl;
getstatic android/support/design/widget/ad/e [I
aload 0
new android/support/design/widget/ah
dup
aload 0
iconst_0
invokespecial android/support/design/widget/ah/<init>(Landroid/support/design/widget/ad;B)V
invokespecial android/support/design/widget/ad/a(Landroid/view/animation/Animation;)Landroid/view/animation/Animation;
invokevirtual android/support/design/widget/bl/a([ILandroid/view/animation/Animation;)V
return
.limit locals 7
.limit stack 7
.end method

.method static synthetic a(Landroid/support/design/widget/ad;)F
aload 0
getfield android/support/design/widget/ad/k F
freturn
.limit locals 1
.limit stack 1
.end method

.method private a(Landroid/view/animation/Animation;)Landroid/view/animation/Animation;
aload 1
getstatic android/support/design/widget/a/b Landroid/view/animation/Interpolator;
invokevirtual android/view/animation/Animation/setInterpolator(Landroid/view/animation/Interpolator;)V
aload 1
aload 0
getfield android/support/design/widget/ad/m I
i2l
invokevirtual android/view/animation/Animation/setDuration(J)V
aload 1
areturn
.limit locals 2
.limit stack 3
.end method

.method static synthetic a(Landroid/support/design/widget/ad;Z)Z
aload 0
iload 1
putfield android/support/design/widget/ad/o Z
iload 1
ireturn
.limit locals 2
.limit stack 2
.end method

.method static synthetic b(Landroid/support/design/widget/ad;)F
aload 0
getfield android/support/design/widget/ad/l F
freturn
.limit locals 1
.limit stack 1
.end method

.method private static b(I)Landroid/content/res/ColorStateList;
new android/content/res/ColorStateList
dup
iconst_3
anewarray [I
dup
iconst_0
getstatic android/support/design/widget/ad/d [I
aastore
dup
iconst_1
getstatic android/support/design/widget/ad/c [I
aastore
dup
iconst_2
iconst_0
newarray int
aastore
iconst_3
newarray int
dup
iconst_0
iload 0
iastore
dup
iconst_1
iload 0
iastore
dup
iconst_2
iconst_0
iastore
invokespecial android/content/res/ColorStateList/<init>([[I[I)V
areturn
.limit locals 1
.limit stack 7
.end method

.method private e()V
new android/graphics/Rect
dup
invokespecial android/graphics/Rect/<init>()V
astore 1
aload 0
getfield android/support/design/widget/ad/a Landroid/support/design/widget/ar;
aload 1
invokevirtual android/support/design/widget/ar/getPadding(Landroid/graphics/Rect;)Z
pop
aload 0
getfield android/support/design/widget/ad/g Landroid/support/design/widget/as;
aload 1
getfield android/graphics/Rect/left I
aload 1
getfield android/graphics/Rect/top I
aload 1
getfield android/graphics/Rect/right I
aload 1
getfield android/graphics/Rect/bottom I
invokeinterface android/support/design/widget/as/a(IIII)V 4
return
.limit locals 2
.limit stack 5
.end method

.method a()V
aload 0
getfield android/support/design/widget/ad/n Landroid/support/design/widget/bl;
astore 1
aload 1
getfield android/support/design/widget/bl/c Landroid/view/animation/Animation;
ifnull L0
aload 1
invokevirtual android/support/design/widget/bl/a()Landroid/view/View;
astore 2
aload 2
ifnull L0
aload 2
invokevirtual android/view/View/getAnimation()Landroid/view/animation/Animation;
aload 1
getfield android/support/design/widget/bl/c Landroid/view/animation/Animation;
if_acmpne L0
aload 2
invokevirtual android/view/View/clearAnimation()V
L0:
return
.limit locals 3
.limit stack 2
.end method

.method a(F)V
aload 0
getfield android/support/design/widget/ad/k F
fload 1
fcmpl
ifeq L0
aload 0
getfield android/support/design/widget/ad/a Landroid/support/design/widget/ar;
ifnull L0
aload 0
getfield android/support/design/widget/ad/a Landroid/support/design/widget/ar;
fload 1
aload 0
getfield android/support/design/widget/ad/l F
fload 1
fadd
invokevirtual android/support/design/widget/ar/a(FF)V
aload 0
fload 1
putfield android/support/design/widget/ad/k F
aload 0
invokespecial android/support/design/widget/ad/e()V
L0:
return
.limit locals 2
.limit stack 4
.end method

.method a(I)V
aload 0
getfield android/support/design/widget/ad/i Landroid/graphics/drawable/Drawable;
iload 1
invokestatic android/support/design/widget/ad/b(I)Landroid/content/res/ColorStateList;
invokestatic android/support/v4/e/a/a/a(Landroid/graphics/drawable/Drawable;Landroid/content/res/ColorStateList;)V
return
.limit locals 2
.limit stack 2
.end method

.method a(Landroid/content/res/ColorStateList;)V
aload 0
getfield android/support/design/widget/ad/h Landroid/graphics/drawable/Drawable;
aload 1
invokestatic android/support/v4/e/a/a/a(Landroid/graphics/drawable/Drawable;Landroid/content/res/ColorStateList;)V
aload 0
getfield android/support/design/widget/ad/j Landroid/graphics/drawable/Drawable;
ifnull L0
aload 0
getfield android/support/design/widget/ad/j Landroid/graphics/drawable/Drawable;
aload 1
invokestatic android/support/v4/e/a/a/a(Landroid/graphics/drawable/Drawable;Landroid/content/res/ColorStateList;)V
L0:
return
.limit locals 2
.limit stack 2
.end method

.method a(Landroid/graphics/PorterDuff$Mode;)V
aload 0
getfield android/support/design/widget/ad/h Landroid/graphics/drawable/Drawable;
aload 1
invokestatic android/support/v4/e/a/a/a(Landroid/graphics/drawable/Drawable;Landroid/graphics/PorterDuff$Mode;)V
return
.limit locals 2
.limit stack 2
.end method

.method a(Landroid/graphics/drawable/Drawable;Landroid/content/res/ColorStateList;Landroid/graphics/PorterDuff$Mode;II)V
aload 0
aload 1
invokevirtual android/graphics/drawable/Drawable/mutate()Landroid/graphics/drawable/Drawable;
invokestatic android/support/v4/e/a/a/c(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;
putfield android/support/design/widget/ad/h Landroid/graphics/drawable/Drawable;
aload 0
getfield android/support/design/widget/ad/h Landroid/graphics/drawable/Drawable;
aload 2
invokestatic android/support/v4/e/a/a/a(Landroid/graphics/drawable/Drawable;Landroid/content/res/ColorStateList;)V
aload 3
ifnull L0
aload 0
getfield android/support/design/widget/ad/h Landroid/graphics/drawable/Drawable;
aload 3
invokestatic android/support/v4/e/a/a/a(Landroid/graphics/drawable/Drawable;Landroid/graphics/PorterDuff$Mode;)V
L0:
new android/graphics/drawable/GradientDrawable
dup
invokespecial android/graphics/drawable/GradientDrawable/<init>()V
astore 1
aload 1
iconst_1
invokevirtual android/graphics/drawable/GradientDrawable/setShape(I)V
aload 1
iconst_m1
invokevirtual android/graphics/drawable/GradientDrawable/setColor(I)V
aload 1
aload 0
getfield android/support/design/widget/ad/g Landroid/support/design/widget/as;
invokeinterface android/support/design/widget/as/a()F 0
invokevirtual android/graphics/drawable/GradientDrawable/setCornerRadius(F)V
aload 0
aload 1
invokestatic android/support/v4/e/a/a/c(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;
putfield android/support/design/widget/ad/i Landroid/graphics/drawable/Drawable;
aload 0
getfield android/support/design/widget/ad/i Landroid/graphics/drawable/Drawable;
iload 4
invokestatic android/support/design/widget/ad/b(I)Landroid/content/res/ColorStateList;
invokestatic android/support/v4/e/a/a/a(Landroid/graphics/drawable/Drawable;Landroid/content/res/ColorStateList;)V
aload 0
getfield android/support/design/widget/ad/i Landroid/graphics/drawable/Drawable;
getstatic android/graphics/PorterDuff$Mode/MULTIPLY Landroid/graphics/PorterDuff$Mode;
invokestatic android/support/v4/e/a/a/a(Landroid/graphics/drawable/Drawable;Landroid/graphics/PorterDuff$Mode;)V
iload 5
ifle L1
aload 0
aload 0
iload 5
aload 2
invokevirtual android/support/design/widget/ad/a(ILandroid/content/res/ColorStateList;)Landroid/graphics/drawable/Drawable;
putfield android/support/design/widget/ad/j Landroid/graphics/drawable/Drawable;
iconst_3
anewarray android/graphics/drawable/Drawable
dup
iconst_0
aload 0
getfield android/support/design/widget/ad/j Landroid/graphics/drawable/Drawable;
aastore
dup
iconst_1
aload 0
getfield android/support/design/widget/ad/h Landroid/graphics/drawable/Drawable;
aastore
dup
iconst_2
aload 0
getfield android/support/design/widget/ad/i Landroid/graphics/drawable/Drawable;
aastore
astore 1
L2:
aload 0
new android/support/design/widget/ar
dup
aload 0
getfield android/support/design/widget/ad/f Landroid/view/View;
invokevirtual android/view/View/getResources()Landroid/content/res/Resources;
new android/graphics/drawable/LayerDrawable
dup
aload 1
invokespecial android/graphics/drawable/LayerDrawable/<init>([Landroid/graphics/drawable/Drawable;)V
aload 0
getfield android/support/design/widget/ad/g Landroid/support/design/widget/as;
invokeinterface android/support/design/widget/as/a()F 0
aload 0
getfield android/support/design/widget/ad/k F
aload 0
getfield android/support/design/widget/ad/k F
aload 0
getfield android/support/design/widget/ad/l F
fadd
invokespecial android/support/design/widget/ar/<init>(Landroid/content/res/Resources;Landroid/graphics/drawable/Drawable;FFF)V
putfield android/support/design/widget/ad/a Landroid/support/design/widget/ar;
aload 0
getfield android/support/design/widget/ad/a Landroid/support/design/widget/ar;
astore 1
aload 1
iconst_0
putfield android/support/design/widget/ar/o Z
aload 1
invokevirtual android/support/design/widget/ar/invalidateSelf()V
aload 0
getfield android/support/design/widget/ad/g Landroid/support/design/widget/as;
aload 0
getfield android/support/design/widget/ad/a Landroid/support/design/widget/ar;
invokeinterface android/support/design/widget/as/a(Landroid/graphics/drawable/Drawable;)V 1
aload 0
invokespecial android/support/design/widget/ad/e()V
return
L1:
aload 0
aconst_null
putfield android/support/design/widget/ad/j Landroid/graphics/drawable/Drawable;
iconst_2
anewarray android/graphics/drawable/Drawable
dup
iconst_0
aload 0
getfield android/support/design/widget/ad/h Landroid/graphics/drawable/Drawable;
aastore
dup
iconst_1
aload 0
getfield android/support/design/widget/ad/i Landroid/graphics/drawable/Drawable;
aastore
astore 1
goto L2
.limit locals 6
.limit stack 9
.end method

.method a([I)V
aload 0
getfield android/support/design/widget/ad/n Landroid/support/design/widget/bl;
astore 5
aload 5
getfield android/support/design/widget/bl/a Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
istore 3
iconst_0
istore 2
L0:
iload 2
iload 3
if_icmpge L1
aload 5
getfield android/support/design/widget/bl/a Ljava/util/ArrayList;
iload 2
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/design/widget/bn
astore 4
aload 4
getfield android/support/design/widget/bn/a [I
aload 1
invokestatic android/util/StateSet/stateSetMatches([I[I)Z
ifeq L2
aload 4
astore 1
L3:
aload 1
aload 5
getfield android/support/design/widget/bl/b Landroid/support/design/widget/bn;
if_acmpeq L4
aload 5
getfield android/support/design/widget/bl/b Landroid/support/design/widget/bn;
ifnull L5
aload 5
getfield android/support/design/widget/bl/c Landroid/view/animation/Animation;
ifnull L5
aload 5
invokevirtual android/support/design/widget/bl/a()Landroid/view/View;
astore 4
aload 4
ifnull L6
aload 4
invokevirtual android/view/View/getAnimation()Landroid/view/animation/Animation;
aload 5
getfield android/support/design/widget/bl/c Landroid/view/animation/Animation;
if_acmpne L6
aload 4
invokevirtual android/view/View/clearAnimation()V
L6:
aload 5
aconst_null
putfield android/support/design/widget/bl/c Landroid/view/animation/Animation;
L5:
aload 5
aload 1
putfield android/support/design/widget/bl/b Landroid/support/design/widget/bn;
aload 1
ifnull L4
aload 5
aload 1
getfield android/support/design/widget/bn/b Landroid/view/animation/Animation;
putfield android/support/design/widget/bl/c Landroid/view/animation/Animation;
aload 5
invokevirtual android/support/design/widget/bl/a()Landroid/view/View;
astore 1
aload 1
ifnull L4
aload 1
aload 5
getfield android/support/design/widget/bl/c Landroid/view/animation/Animation;
invokevirtual android/view/View/startAnimation(Landroid/view/animation/Animation;)V
L4:
return
L2:
iload 2
iconst_1
iadd
istore 2
goto L0
L1:
aconst_null
astore 1
goto L3
.limit locals 6
.limit stack 2
.end method

.method b()V
aload 0
getfield android/support/design/widget/ad/o Z
ifne L0
aload 0
getfield android/support/design/widget/ad/f Landroid/view/View;
invokevirtual android/view/View/getVisibility()I
ifeq L1
L0:
return
L1:
aload 0
getfield android/support/design/widget/ad/f Landroid/view/View;
invokevirtual android/view/View/getContext()Landroid/content/Context;
getstatic android/support/design/c/design_fab_out I
invokestatic android/view/animation/AnimationUtils/loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;
astore 1
aload 1
getstatic android/support/design/widget/a/b Landroid/view/animation/Interpolator;
invokevirtual android/view/animation/Animation/setInterpolator(Landroid/view/animation/Interpolator;)V
aload 1
ldc2_w 200L
invokevirtual android/view/animation/Animation/setDuration(J)V
aload 1
new android/support/design/widget/ae
dup
aload 0
invokespecial android/support/design/widget/ae/<init>(Landroid/support/design/widget/ad;)V
invokevirtual android/view/animation/Animation/setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V
aload 0
getfield android/support/design/widget/ad/f Landroid/view/View;
aload 1
invokevirtual android/view/View/startAnimation(Landroid/view/animation/Animation;)V
return
.limit locals 2
.limit stack 4
.end method

.method b(F)V
aload 0
getfield android/support/design/widget/ad/l F
fload 1
fcmpl
ifeq L0
aload 0
getfield android/support/design/widget/ad/a Landroid/support/design/widget/ar;
ifnull L0
aload 0
fload 1
putfield android/support/design/widget/ad/l F
aload 0
getfield android/support/design/widget/ad/a Landroid/support/design/widget/ar;
astore 3
aload 0
getfield android/support/design/widget/ad/k F
fstore 2
aload 3
aload 3
getfield android/support/design/widget/ar/n F
fload 2
fload 1
fadd
invokevirtual android/support/design/widget/ar/a(FF)V
aload 0
invokespecial android/support/design/widget/ad/e()V
L0:
return
.limit locals 4
.limit stack 4
.end method

.method c()V
aload 0
getfield android/support/design/widget/ad/f Landroid/view/View;
invokevirtual android/view/View/getVisibility()I
ifne L0
aload 0
getfield android/support/design/widget/ad/o Z
ifeq L1
L0:
aload 0
getfield android/support/design/widget/ad/f Landroid/view/View;
invokevirtual android/view/View/clearAnimation()V
aload 0
getfield android/support/design/widget/ad/f Landroid/view/View;
iconst_0
invokevirtual android/view/View/setVisibility(I)V
aload 0
getfield android/support/design/widget/ad/f Landroid/view/View;
invokevirtual android/view/View/getContext()Landroid/content/Context;
getstatic android/support/design/c/design_fab_in I
invokestatic android/view/animation/AnimationUtils/loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;
astore 1
aload 1
ldc2_w 200L
invokevirtual android/view/animation/Animation/setDuration(J)V
aload 1
getstatic android/support/design/widget/a/b Landroid/view/animation/Interpolator;
invokevirtual android/view/animation/Animation/setInterpolator(Landroid/view/animation/Interpolator;)V
aload 0
getfield android/support/design/widget/ad/f Landroid/view/View;
aload 1
invokevirtual android/view/View/startAnimation(Landroid/view/animation/Animation;)V
L1:
return
.limit locals 2
.limit stack 3
.end method
