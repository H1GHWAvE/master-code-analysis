.bytecode 50.0
.class public synchronized android/support/design/widget/CoordinatorLayout$SavedState
.super android/view/View$BaseSavedState

.field public static final 'CREATOR' Landroid/os/Parcelable$Creator;

.field 'a' Landroid/util/SparseArray;

.method static <clinit>()V
new android/support/design/widget/y
dup
invokespecial android/support/design/widget/y/<init>()V
putstatic android/support/design/widget/CoordinatorLayout$SavedState/CREATOR Landroid/os/Parcelable$Creator;
return
.limit locals 0
.limit stack 2
.end method

.method public <init>(Landroid/os/Parcel;)V
aload 0
aload 1
invokespecial android/view/View$BaseSavedState/<init>(Landroid/os/Parcel;)V
aload 1
invokevirtual android/os/Parcel/readInt()I
istore 3
iload 3
newarray int
astore 4
aload 1
aload 4
invokevirtual android/os/Parcel/readIntArray([I)V
aload 1
ldc android/support/design/widget/CoordinatorLayout
invokevirtual java/lang/Class/getClassLoader()Ljava/lang/ClassLoader;
invokevirtual android/os/Parcel/readParcelableArray(Ljava/lang/ClassLoader;)[Landroid/os/Parcelable;
astore 1
aload 0
new android/util/SparseArray
dup
iload 3
invokespecial android/util/SparseArray/<init>(I)V
putfield android/support/design/widget/CoordinatorLayout$SavedState/a Landroid/util/SparseArray;
iconst_0
istore 2
L0:
iload 2
iload 3
if_icmpge L1
aload 0
getfield android/support/design/widget/CoordinatorLayout$SavedState/a Landroid/util/SparseArray;
aload 4
iload 2
iaload
aload 1
iload 2
aaload
invokevirtual android/util/SparseArray/append(ILjava/lang/Object;)V
iload 2
iconst_1
iadd
istore 2
goto L0
L1:
return
.limit locals 5
.limit stack 4
.end method

.method public <init>(Landroid/os/Parcelable;)V
aload 0
aload 1
invokespecial android/view/View$BaseSavedState/<init>(Landroid/os/Parcelable;)V
return
.limit locals 2
.limit stack 2
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
iconst_0
istore 4
aload 0
aload 1
iload 2
invokespecial android/view/View$BaseSavedState/writeToParcel(Landroid/os/Parcel;I)V
aload 0
getfield android/support/design/widget/CoordinatorLayout$SavedState/a Landroid/util/SparseArray;
ifnull L0
aload 0
getfield android/support/design/widget/CoordinatorLayout$SavedState/a Landroid/util/SparseArray;
invokevirtual android/util/SparseArray/size()I
istore 3
L1:
aload 1
iload 3
invokevirtual android/os/Parcel/writeInt(I)V
iload 3
newarray int
astore 5
iload 3
anewarray android/os/Parcelable
astore 6
L2:
iload 4
iload 3
if_icmpge L3
aload 5
iload 4
aload 0
getfield android/support/design/widget/CoordinatorLayout$SavedState/a Landroid/util/SparseArray;
iload 4
invokevirtual android/util/SparseArray/keyAt(I)I
iastore
aload 6
iload 4
aload 0
getfield android/support/design/widget/CoordinatorLayout$SavedState/a Landroid/util/SparseArray;
iload 4
invokevirtual android/util/SparseArray/valueAt(I)Ljava/lang/Object;
checkcast android/os/Parcelable
aastore
iload 4
iconst_1
iadd
istore 4
goto L2
L0:
iconst_0
istore 3
goto L1
L3:
aload 1
aload 5
invokevirtual android/os/Parcel/writeIntArray([I)V
aload 1
aload 6
iload 2
invokevirtual android/os/Parcel/writeParcelableArray([Landroid/os/Parcelable;I)V
return
.limit locals 7
.limit stack 4
.end method
