.bytecode 50.0
.class synchronized abstract android/support/design/widget/al
.super java/lang/Object

.field static final 'b' I = 200


.field static final 'c' [I

.field static final 'd' [I

.field static final 'e' [I

.field final 'f' Landroid/view/View;

.field final 'g' Landroid/support/design/widget/as;

.method static <clinit>()V
iconst_2
newarray int
dup
iconst_0
ldc_w 16842919
iastore
dup
iconst_1
ldc_w 16842910
iastore
putstatic android/support/design/widget/al/c [I
iconst_2
newarray int
dup
iconst_0
ldc_w 16842908
iastore
dup
iconst_1
ldc_w 16842910
iastore
putstatic android/support/design/widget/al/d [I
iconst_0
newarray int
putstatic android/support/design/widget/al/e [I
return
.limit locals 0
.limit stack 4
.end method

.method <init>(Landroid/view/View;Landroid/support/design/widget/as;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
putfield android/support/design/widget/al/f Landroid/view/View;
aload 0
aload 2
putfield android/support/design/widget/al/g Landroid/support/design/widget/as;
return
.limit locals 3
.limit stack 2
.end method

.method final a(ILandroid/content/res/ColorStateList;)Landroid/graphics/drawable/Drawable;
aload 0
getfield android/support/design/widget/al/f Landroid/view/View;
invokevirtual android/view/View/getResources()Landroid/content/res/Resources;
astore 8
aload 0
invokevirtual android/support/design/widget/al/d()Landroid/support/design/widget/j;
astore 9
aload 8
getstatic android/support/design/f/design_fab_stroke_top_outer_color I
invokevirtual android/content/res/Resources/getColor(I)I
istore 4
aload 8
getstatic android/support/design/f/design_fab_stroke_top_inner_color I
invokevirtual android/content/res/Resources/getColor(I)I
istore 5
aload 8
getstatic android/support/design/f/design_fab_stroke_end_inner_color I
invokevirtual android/content/res/Resources/getColor(I)I
istore 6
aload 8
getstatic android/support/design/f/design_fab_stroke_end_outer_color I
invokevirtual android/content/res/Resources/getColor(I)I
istore 7
aload 9
iload 4
putfield android/support/design/widget/j/e I
aload 9
iload 5
putfield android/support/design/widget/j/f I
aload 9
iload 6
putfield android/support/design/widget/j/g I
aload 9
iload 7
putfield android/support/design/widget/j/h I
iload 1
i2f
fstore 3
aload 9
getfield android/support/design/widget/j/d F
fload 3
fcmpl
ifeq L0
aload 9
fload 3
putfield android/support/design/widget/j/d F
aload 9
getfield android/support/design/widget/j/a Landroid/graphics/Paint;
fload 3
ldc_w 1.3333F
fmul
invokevirtual android/graphics/Paint/setStrokeWidth(F)V
aload 9
iconst_1
putfield android/support/design/widget/j/j Z
aload 9
invokevirtual android/support/design/widget/j/invalidateSelf()V
L0:
aload 9
aload 2
invokevirtual android/content/res/ColorStateList/getDefaultColor()I
putfield android/support/design/widget/j/i I
aload 9
iconst_1
putfield android/support/design/widget/j/j Z
aload 9
invokevirtual android/support/design/widget/j/invalidateSelf()V
aload 9
areturn
.limit locals 10
.limit stack 3
.end method

.method abstract a()V
.end method

.method abstract a(F)V
.end method

.method abstract a(I)V
.end method

.method abstract a(Landroid/content/res/ColorStateList;)V
.end method

.method abstract a(Landroid/graphics/PorterDuff$Mode;)V
.end method

.method abstract a(Landroid/graphics/drawable/Drawable;Landroid/content/res/ColorStateList;Landroid/graphics/PorterDuff$Mode;II)V
.end method

.method abstract a([I)V
.end method

.method abstract b()V
.end method

.method abstract b(F)V
.end method

.method abstract c()V
.end method

.method d()Landroid/support/design/widget/j;
new android/support/design/widget/j
dup
invokespecial android/support/design/widget/j/<init>()V
areturn
.limit locals 1
.limit stack 2
.end method
