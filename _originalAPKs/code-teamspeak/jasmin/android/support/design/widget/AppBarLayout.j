.bytecode 50.0
.class public final synchronized android/support/design/widget/AppBarLayout
.super android/widget/LinearLayout
.annotation visible Landroid/support/design/widget/u;
a c = Landroid/support/design/widget/AppBarLayout$Behavior;
.end annotation

.field private static final 'd' I = 0


.field private static final 'e' I = 1


.field private static final 'f' I = 2


.field private static final 'g' I = 4


.field private static final 'h' I = -1


.field 'a' Z

.field 'b' I

.field final 'c' Ljava/util/List;

.field private 'i' I

.field private 'j' I

.field private 'k' I

.field private 'l' F

.field private 'm' Landroid/support/v4/view/gh;

.method private <init>(Landroid/content/Context;)V
aload 0
aload 1
iconst_0
invokespecial android/support/design/widget/AppBarLayout/<init>(Landroid/content/Context;B)V
return
.limit locals 2
.limit stack 3
.end method

.method private <init>(Landroid/content/Context;B)V
aload 0
aload 1
aconst_null
invokespecial android/widget/LinearLayout/<init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
aload 0
iconst_m1
putfield android/support/design/widget/AppBarLayout/i I
aload 0
iconst_m1
putfield android/support/design/widget/AppBarLayout/j I
aload 0
iconst_m1
putfield android/support/design/widget/AppBarLayout/k I
aload 0
iconst_0
putfield android/support/design/widget/AppBarLayout/b I
aload 0
iconst_1
invokevirtual android/support/design/widget/AppBarLayout/setOrientation(I)V
aload 1
aconst_null
getstatic android/support/design/n/AppBarLayout [I
iconst_0
getstatic android/support/design/m/Widget_Design_AppBarLayout I
invokevirtual android/content/Context/obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;
astore 1
aload 0
aload 1
getstatic android/support/design/n/AppBarLayout_elevation I
iconst_0
invokevirtual android/content/res/TypedArray/getDimensionPixelSize(II)I
i2f
putfield android/support/design/widget/AppBarLayout/l F
aload 0
aload 1
getstatic android/support/design/n/AppBarLayout_android_background I
invokevirtual android/content/res/TypedArray/getDrawable(I)Landroid/graphics/drawable/Drawable;
invokevirtual android/support/design/widget/AppBarLayout/setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
aload 1
getstatic android/support/design/n/AppBarLayout_expanded I
invokevirtual android/content/res/TypedArray/hasValue(I)Z
ifeq L0
aload 0
aload 1
getstatic android/support/design/n/AppBarLayout_expanded I
iconst_0
invokevirtual android/content/res/TypedArray/getBoolean(IZ)Z
invokevirtual android/support/design/widget/AppBarLayout/setExpanded(Z)V
L0:
aload 1
invokevirtual android/content/res/TypedArray/recycle()V
aload 0
invokestatic android/support/design/widget/dh/a(Landroid/view/View;)V
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield android/support/design/widget/AppBarLayout/c Ljava/util/List;
aload 0
aload 0
getfield android/support/design/widget/AppBarLayout/l F
invokestatic android/support/v4/view/cx/f(Landroid/view/View;F)V
aload 0
new android/support/design/widget/c
dup
aload 0
invokespecial android/support/design/widget/c/<init>(Landroid/support/design/widget/AppBarLayout;)V
invokestatic android/support/v4/view/cx/a(Landroid/view/View;Landroid/support/v4/view/bx;)V
return
.limit locals 3
.limit stack 5
.end method

.method private static a()Landroid/support/design/widget/g;
new android/support/design/widget/g
dup
invokespecial android/support/design/widget/g/<init>()V
areturn
.limit locals 0
.limit stack 2
.end method

.method private a(Landroid/util/AttributeSet;)Landroid/support/design/widget/g;
new android/support/design/widget/g
dup
aload 0
invokevirtual android/support/design/widget/AppBarLayout/getContext()Landroid/content/Context;
aload 1
invokespecial android/support/design/widget/g/<init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
areturn
.limit locals 2
.limit stack 4
.end method

.method private static a(Landroid/view/ViewGroup$LayoutParams;)Landroid/support/design/widget/g;
aload 0
instanceof android/widget/LinearLayout$LayoutParams
ifeq L0
new android/support/design/widget/g
dup
aload 0
checkcast android/widget/LinearLayout$LayoutParams
invokespecial android/support/design/widget/g/<init>(Landroid/widget/LinearLayout$LayoutParams;)V
areturn
L0:
aload 0
instanceof android/view/ViewGroup$MarginLayoutParams
ifeq L1
new android/support/design/widget/g
dup
aload 0
checkcast android/view/ViewGroup$MarginLayoutParams
invokespecial android/support/design/widget/g/<init>(Landroid/view/ViewGroup$MarginLayoutParams;)V
areturn
L1:
new android/support/design/widget/g
dup
aload 0
invokespecial android/support/design/widget/g/<init>(Landroid/view/ViewGroup$LayoutParams;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method static synthetic a(Landroid/support/design/widget/AppBarLayout;)Ljava/util/List;
aload 0
getfield android/support/design/widget/AppBarLayout/c Ljava/util/List;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic a(Landroid/support/design/widget/AppBarLayout;Landroid/support/v4/view/gh;)V
aload 0
aload 1
invokespecial android/support/design/widget/AppBarLayout/setWindowInsets(Landroid/support/v4/view/gh;)V
return
.limit locals 2
.limit stack 2
.end method

.method private a(Landroid/support/design/widget/i;)V
aload 1
ifnull L0
aload 0
getfield android/support/design/widget/AppBarLayout/c Ljava/util/List;
aload 1
invokeinterface java/util/List/contains(Ljava/lang/Object;)Z 1
ifne L0
aload 0
getfield android/support/design/widget/AppBarLayout/c Ljava/util/List;
aload 1
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
L0:
return
.limit locals 2
.limit stack 2
.end method

.method private a(ZZ)V
iload 1
ifeq L0
iconst_1
istore 3
L1:
iload 2
ifeq L2
iconst_4
istore 4
L3:
aload 0
iload 4
iload 3
ior
putfield android/support/design/widget/AppBarLayout/b I
aload 0
invokevirtual android/support/design/widget/AppBarLayout/requestLayout()V
return
L0:
iconst_2
istore 3
goto L1
L2:
iconst_0
istore 4
goto L3
.limit locals 5
.limit stack 3
.end method

.method private b(Landroid/support/design/widget/i;)V
aload 1
ifnull L0
aload 0
getfield android/support/design/widget/AppBarLayout/c Ljava/util/List;
aload 1
invokeinterface java/util/List/remove(Ljava/lang/Object;)Z 1
pop
L0:
return
.limit locals 2
.limit stack 2
.end method

.method private b()Z
aload 0
getfield android/support/design/widget/AppBarLayout/a Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private c()Z
aload 0
invokevirtual android/support/design/widget/AppBarLayout/getTotalScrollRange()I
ifeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method private d()V
aload 0
iconst_0
putfield android/support/design/widget/AppBarLayout/b I
return
.limit locals 1
.limit stack 2
.end method

.method private setWindowInsets(Landroid/support/v4/view/gh;)V
aload 0
iconst_m1
putfield android/support/design/widget/AppBarLayout/i I
aload 0
aload 1
putfield android/support/design/widget/AppBarLayout/m Landroid/support/v4/view/gh;
iconst_0
istore 2
aload 0
invokevirtual android/support/design/widget/AppBarLayout/getChildCount()I
istore 3
L0:
iload 2
iload 3
if_icmpge L1
aload 0
iload 2
invokevirtual android/support/design/widget/AppBarLayout/getChildAt(I)Landroid/view/View;
aload 1
invokestatic android/support/v4/view/cx/b(Landroid/view/View;Landroid/support/v4/view/gh;)Landroid/support/v4/view/gh;
astore 1
aload 1
invokevirtual android/support/v4/view/gh/g()Z
ifne L1
iload 2
iconst_1
iadd
istore 2
goto L0
L1:
return
.limit locals 4
.limit stack 2
.end method

.method protected final checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
aload 1
instanceof android/support/design/widget/g
ireturn
.limit locals 2
.limit stack 1
.end method

.method protected final synthetic generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
new android/support/design/widget/g
dup
invokespecial android/support/design/widget/g/<init>()V
areturn
.limit locals 1
.limit stack 2
.end method

.method protected final synthetic generateDefaultLayoutParams()Landroid/widget/LinearLayout$LayoutParams;
new android/support/design/widget/g
dup
invokespecial android/support/design/widget/g/<init>()V
areturn
.limit locals 1
.limit stack 2
.end method

.method public final synthetic generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
aload 0
aload 1
invokespecial android/support/design/widget/AppBarLayout/a(Landroid/util/AttributeSet;)Landroid/support/design/widget/g;
areturn
.limit locals 2
.limit stack 2
.end method

.method protected final synthetic generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
aload 1
invokestatic android/support/design/widget/AppBarLayout/a(Landroid/view/ViewGroup$LayoutParams;)Landroid/support/design/widget/g;
areturn
.limit locals 2
.limit stack 1
.end method

.method public final synthetic generateLayoutParams(Landroid/util/AttributeSet;)Landroid/widget/LinearLayout$LayoutParams;
aload 0
aload 1
invokespecial android/support/design/widget/AppBarLayout/a(Landroid/util/AttributeSet;)Landroid/support/design/widget/g;
areturn
.limit locals 2
.limit stack 2
.end method

.method protected final synthetic generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/widget/LinearLayout$LayoutParams;
aload 1
invokestatic android/support/design/widget/AppBarLayout/a(Landroid/view/ViewGroup$LayoutParams;)Landroid/support/design/widget/g;
areturn
.limit locals 2
.limit stack 1
.end method

.method final getDownNestedPreScrollRange()I
aload 0
getfield android/support/design/widget/AppBarLayout/j I
iconst_m1
if_icmpeq L0
aload 0
getfield android/support/design/widget/AppBarLayout/j I
ireturn
L0:
iconst_0
istore 1
aload 0
invokevirtual android/support/design/widget/AppBarLayout/getChildCount()I
iconst_1
isub
istore 2
L1:
iload 2
iflt L2
aload 0
iload 2
invokevirtual android/support/design/widget/AppBarLayout/getChildAt(I)Landroid/view/View;
astore 6
aload 6
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/design/widget/g
astore 7
aload 6
invokestatic android/support/v4/view/cx/B(Landroid/view/View;)Z
ifeq L3
aload 6
invokevirtual android/view/View/getHeight()I
istore 3
L4:
aload 7
getfield android/support/design/widget/g/f I
istore 4
iload 4
iconst_5
iand
iconst_5
if_icmpne L5
aload 7
getfield android/support/design/widget/g/topMargin I
istore 5
aload 7
getfield android/support/design/widget/g/bottomMargin I
iload 5
iadd
iload 1
iadd
istore 1
iload 4
bipush 8
iand
ifeq L6
iload 1
aload 6
invokestatic android/support/v4/view/cx/o(Landroid/view/View;)I
iadd
istore 1
L7:
iload 2
iconst_1
isub
istore 2
goto L1
L3:
aload 6
invokevirtual android/view/View/getMeasuredHeight()I
istore 3
goto L4
L6:
iload 1
iload 3
iadd
istore 1
goto L7
L5:
iload 1
ifgt L2
goto L7
L2:
aload 0
iload 1
putfield android/support/design/widget/AppBarLayout/j I
iload 1
ireturn
.limit locals 8
.limit stack 2
.end method

.method final getDownNestedScrollRange()I
aload 0
getfield android/support/design/widget/AppBarLayout/k I
iconst_m1
if_icmpeq L0
aload 0
getfield android/support/design/widget/AppBarLayout/k I
ireturn
L0:
aload 0
invokevirtual android/support/design/widget/AppBarLayout/getChildCount()I
istore 4
iconst_0
istore 1
iconst_0
istore 2
L1:
iload 1
iload 4
if_icmpge L2
aload 0
iload 1
invokevirtual android/support/design/widget/AppBarLayout/getChildAt(I)Landroid/view/View;
astore 8
aload 8
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/design/widget/g
astore 9
aload 8
invokestatic android/support/v4/view/cx/B(Landroid/view/View;)Z
ifeq L3
aload 8
invokevirtual android/view/View/getHeight()I
istore 3
L4:
aload 9
getfield android/support/design/widget/g/topMargin I
istore 6
aload 9
getfield android/support/design/widget/g/bottomMargin I
istore 7
aload 9
getfield android/support/design/widget/g/f I
istore 5
iload 5
iconst_1
iand
ifeq L2
iload 2
iload 3
iload 6
iload 7
iadd
iadd
iadd
istore 2
iload 5
iconst_2
iand
ifeq L5
iload 2
aload 8
invokestatic android/support/v4/view/cx/o(Landroid/view/View;)I
isub
ireturn
L3:
aload 8
invokevirtual android/view/View/getMeasuredHeight()I
istore 3
goto L4
L5:
iload 1
iconst_1
iadd
istore 1
goto L1
L2:
aload 0
iload 2
putfield android/support/design/widget/AppBarLayout/k I
iload 2
ireturn
.limit locals 10
.limit stack 4
.end method

.method final getMinimumHeightForVisibleOverlappingContent()I
iconst_0
istore 2
aload 0
getfield android/support/design/widget/AppBarLayout/m Landroid/support/v4/view/gh;
ifnull L0
aload 0
getfield android/support/design/widget/AppBarLayout/m Landroid/support/v4/view/gh;
invokevirtual android/support/v4/view/gh/b()I
istore 1
L1:
aload 0
invokestatic android/support/v4/view/cx/o(Landroid/view/View;)I
istore 3
iload 3
ifeq L2
iload 3
iconst_2
imul
iload 1
iadd
istore 2
L3:
iload 2
ireturn
L0:
iconst_0
istore 1
goto L1
L2:
aload 0
invokevirtual android/support/design/widget/AppBarLayout/getChildCount()I
istore 3
iload 3
ifle L3
aload 0
iload 3
iconst_1
isub
invokevirtual android/support/design/widget/AppBarLayout/getChildAt(I)Landroid/view/View;
invokestatic android/support/v4/view/cx/o(Landroid/view/View;)I
iconst_2
imul
iload 1
iadd
ireturn
.limit locals 4
.limit stack 3
.end method

.method final getPendingAction()I
aload 0
getfield android/support/design/widget/AppBarLayout/b I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final getTargetElevation()F
aload 0
getfield android/support/design/widget/AppBarLayout/l F
freturn
.limit locals 1
.limit stack 1
.end method

.method public final getTotalScrollRange()I
aload 0
getfield android/support/design/widget/AppBarLayout/i I
iconst_m1
if_icmpeq L0
aload 0
getfield android/support/design/widget/AppBarLayout/i I
ireturn
L0:
aload 0
invokevirtual android/support/design/widget/AppBarLayout/getChildCount()I
istore 4
iconst_0
istore 2
iconst_0
istore 1
L1:
iload 2
iload 4
if_icmpge L2
aload 0
iload 2
invokevirtual android/support/design/widget/AppBarLayout/getChildAt(I)Landroid/view/View;
astore 7
aload 7
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/design/widget/g
astore 8
aload 7
invokestatic android/support/v4/view/cx/B(Landroid/view/View;)Z
ifeq L3
aload 7
invokevirtual android/view/View/getHeight()I
istore 3
L4:
aload 8
getfield android/support/design/widget/g/f I
istore 5
iload 5
iconst_1
iand
ifeq L2
aload 8
getfield android/support/design/widget/g/topMargin I
istore 6
iload 1
aload 8
getfield android/support/design/widget/g/bottomMargin I
iload 3
iload 6
iadd
iadd
iadd
istore 1
iload 5
iconst_2
iand
ifeq L5
iload 1
aload 7
invokestatic android/support/v4/view/cx/o(Landroid/view/View;)I
isub
istore 1
L6:
aload 0
getfield android/support/design/widget/AppBarLayout/m Landroid/support/v4/view/gh;
ifnull L7
aload 0
getfield android/support/design/widget/AppBarLayout/m Landroid/support/v4/view/gh;
invokevirtual android/support/v4/view/gh/b()I
istore 2
L8:
iload 1
iload 2
isub
istore 1
aload 0
iload 1
putfield android/support/design/widget/AppBarLayout/i I
iload 1
ireturn
L3:
aload 7
invokevirtual android/view/View/getMeasuredHeight()I
istore 3
goto L4
L5:
iload 2
iconst_1
iadd
istore 2
goto L1
L7:
iconst_0
istore 2
goto L8
L2:
goto L6
.limit locals 9
.limit stack 4
.end method

.method final getUpNestedPreScrollRange()I
aload 0
invokevirtual android/support/design/widget/AppBarLayout/getTotalScrollRange()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method protected final onLayout(ZIIII)V
aload 0
iload 1
iload 2
iload 3
iload 4
iload 5
invokespecial android/widget/LinearLayout/onLayout(ZIIII)V
aload 0
iconst_m1
putfield android/support/design/widget/AppBarLayout/i I
aload 0
iconst_m1
putfield android/support/design/widget/AppBarLayout/j I
aload 0
iconst_m1
putfield android/support/design/widget/AppBarLayout/j I
aload 0
iconst_0
putfield android/support/design/widget/AppBarLayout/a Z
aload 0
invokevirtual android/support/design/widget/AppBarLayout/getChildCount()I
istore 3
iconst_0
istore 2
L0:
iload 2
iload 3
if_icmpge L1
aload 0
iload 2
invokevirtual android/support/design/widget/AppBarLayout/getChildAt(I)Landroid/view/View;
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/design/widget/g
getfield android/support/design/widget/g/g Landroid/view/animation/Interpolator;
ifnull L2
aload 0
iconst_1
putfield android/support/design/widget/AppBarLayout/a Z
L1:
return
L2:
iload 2
iconst_1
iadd
istore 2
goto L0
.limit locals 6
.limit stack 6
.end method

.method public final setExpanded(Z)V
aload 0
invokestatic android/support/v4/view/cx/B(Landroid/view/View;)Z
istore 4
iload 1
ifeq L0
iconst_1
istore 2
L1:
iload 4
ifeq L2
iconst_4
istore 3
L3:
aload 0
iload 3
iload 2
ior
putfield android/support/design/widget/AppBarLayout/b I
aload 0
invokevirtual android/support/design/widget/AppBarLayout/requestLayout()V
return
L0:
iconst_2
istore 2
goto L1
L2:
iconst_0
istore 3
goto L3
.limit locals 5
.limit stack 3
.end method

.method public final setOrientation(I)V
iload 1
iconst_1
if_icmpeq L0
new java/lang/IllegalArgumentException
dup
ldc "AppBarLayout is always vertical and does not support horizontal orientation"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
iload 1
invokespecial android/widget/LinearLayout/setOrientation(I)V
return
.limit locals 2
.limit stack 3
.end method

.method public final setTargetElevation(F)V
aload 0
fload 1
putfield android/support/design/widget/AppBarLayout/l F
return
.limit locals 2
.limit stack 2
.end method
