.bytecode 50.0
.class final synchronized android/support/design/widget/de
.super java/lang/Object

.field private static final 'a' Ljava/lang/ThreadLocal;

.field private static final 'b' Ljava/lang/ThreadLocal;

.field private static final 'c' Landroid/graphics/Matrix;

.method static <clinit>()V
new java/lang/ThreadLocal
dup
invokespecial java/lang/ThreadLocal/<init>()V
putstatic android/support/design/widget/de/a Ljava/lang/ThreadLocal;
new java/lang/ThreadLocal
dup
invokespecial java/lang/ThreadLocal/<init>()V
putstatic android/support/design/widget/de/b Ljava/lang/ThreadLocal;
new android/graphics/Matrix
dup
invokespecial android/graphics/Matrix/<init>()V
putstatic android/support/design/widget/de/c Landroid/graphics/Matrix;
return
.limit locals 0
.limit stack 2
.end method

.method <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static a(Landroid/view/ViewGroup;Landroid/view/View;Landroid/graphics/Rect;)V
getstatic android/support/design/widget/de/a Ljava/lang/ThreadLocal;
invokevirtual java/lang/ThreadLocal/get()Ljava/lang/Object;
checkcast android/graphics/Matrix
astore 3
aload 3
ifnonnull L0
new android/graphics/Matrix
dup
invokespecial android/graphics/Matrix/<init>()V
astore 3
getstatic android/support/design/widget/de/a Ljava/lang/ThreadLocal;
aload 3
invokevirtual java/lang/ThreadLocal/set(Ljava/lang/Object;)V
L1:
aload 0
aload 1
aload 3
invokestatic android/support/design/widget/de/a(Landroid/view/ViewParent;Landroid/view/View;Landroid/graphics/Matrix;)V
getstatic android/support/design/widget/de/b Ljava/lang/ThreadLocal;
invokevirtual java/lang/ThreadLocal/get()Ljava/lang/Object;
checkcast android/graphics/RectF
astore 1
aload 1
astore 0
aload 1
ifnonnull L2
new android/graphics/RectF
dup
invokespecial android/graphics/RectF/<init>()V
astore 0
L2:
aload 0
aload 2
invokevirtual android/graphics/RectF/set(Landroid/graphics/Rect;)V
aload 3
aload 0
invokevirtual android/graphics/Matrix/mapRect(Landroid/graphics/RectF;)Z
pop
aload 2
aload 0
getfield android/graphics/RectF/left F
ldc_w 0.5F
fadd
f2i
aload 0
getfield android/graphics/RectF/top F
ldc_w 0.5F
fadd
f2i
aload 0
getfield android/graphics/RectF/right F
ldc_w 0.5F
fadd
f2i
aload 0
getfield android/graphics/RectF/bottom F
ldc_w 0.5F
fadd
f2i
invokevirtual android/graphics/Rect/set(IIII)V
return
L0:
aload 3
getstatic android/support/design/widget/de/c Landroid/graphics/Matrix;
invokevirtual android/graphics/Matrix/set(Landroid/graphics/Matrix;)V
goto L1
.limit locals 4
.limit stack 6
.end method

.method private static a(Landroid/view/ViewParent;Landroid/view/View;Landroid/graphics/Matrix;)V
aload 1
invokevirtual android/view/View/getParent()Landroid/view/ViewParent;
astore 3
aload 3
instanceof android/view/View
ifeq L0
aload 3
aload 0
if_acmpeq L0
aload 3
checkcast android/view/View
astore 3
aload 0
aload 3
aload 2
invokestatic android/support/design/widget/de/a(Landroid/view/ViewParent;Landroid/view/View;Landroid/graphics/Matrix;)V
aload 2
aload 3
invokevirtual android/view/View/getScrollX()I
ineg
i2f
aload 3
invokevirtual android/view/View/getScrollY()I
ineg
i2f
invokevirtual android/graphics/Matrix/preTranslate(FF)Z
pop
L0:
aload 2
aload 1
invokevirtual android/view/View/getLeft()I
i2f
aload 1
invokevirtual android/view/View/getTop()I
i2f
invokevirtual android/graphics/Matrix/preTranslate(FF)Z
pop
aload 1
invokevirtual android/view/View/getMatrix()Landroid/graphics/Matrix;
invokevirtual android/graphics/Matrix/isIdentity()Z
ifne L1
aload 2
aload 1
invokevirtual android/view/View/getMatrix()Landroid/graphics/Matrix;
invokevirtual android/graphics/Matrix/preConcat(Landroid/graphics/Matrix;)Z
pop
L1:
return
.limit locals 4
.limit stack 3
.end method
