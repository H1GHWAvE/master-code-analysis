.bytecode 50.0
.class final synchronized android/support/design/widget/k
.super android/support/design/widget/j

.field private 'k' Landroid/content/res/ColorStateList;

.field private 'l' Landroid/graphics/PorterDuff$Mode;

.field private 'm' Landroid/graphics/PorterDuffColorFilter;

.method <init>()V
aload 0
invokespecial android/support/design/widget/j/<init>()V
aload 0
getstatic android/graphics/PorterDuff$Mode/SRC_IN Landroid/graphics/PorterDuff$Mode;
putfield android/support/design/widget/k/l Landroid/graphics/PorterDuff$Mode;
return
.limit locals 1
.limit stack 2
.end method

.method private a(Landroid/content/res/ColorStateList;Landroid/graphics/PorterDuff$Mode;)Landroid/graphics/PorterDuffColorFilter;
aload 1
ifnull L0
aload 2
ifnonnull L1
L0:
aconst_null
areturn
L1:
new android/graphics/PorterDuffColorFilter
dup
aload 1
aload 0
invokevirtual android/support/design/widget/k/getState()[I
iconst_0
invokevirtual android/content/res/ColorStateList/getColorForState([II)I
aload 2
invokespecial android/graphics/PorterDuffColorFilter/<init>(ILandroid/graphics/PorterDuff$Mode;)V
areturn
.limit locals 3
.limit stack 5
.end method

.method public final draw(Landroid/graphics/Canvas;)V
aload 0
getfield android/support/design/widget/k/m Landroid/graphics/PorterDuffColorFilter;
ifnull L0
aload 0
getfield android/support/design/widget/k/a Landroid/graphics/Paint;
invokevirtual android/graphics/Paint/getColorFilter()Landroid/graphics/ColorFilter;
ifnonnull L0
aload 0
getfield android/support/design/widget/k/a Landroid/graphics/Paint;
aload 0
getfield android/support/design/widget/k/m Landroid/graphics/PorterDuffColorFilter;
invokevirtual android/graphics/Paint/setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;
pop
iconst_1
istore 2
L1:
aload 0
aload 1
invokespecial android/support/design/widget/j/draw(Landroid/graphics/Canvas;)V
iload 2
ifeq L2
aload 0
getfield android/support/design/widget/k/a Landroid/graphics/Paint;
aconst_null
invokevirtual android/graphics/Paint/setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;
pop
L2:
return
L0:
iconst_0
istore 2
goto L1
.limit locals 3
.limit stack 2
.end method

.method public final getOutline(Landroid/graphics/Outline;)V
aload 0
aload 0
getfield android/support/design/widget/k/b Landroid/graphics/Rect;
invokevirtual android/support/design/widget/k/copyBounds(Landroid/graphics/Rect;)V
aload 1
aload 0
getfield android/support/design/widget/k/b Landroid/graphics/Rect;
invokevirtual android/graphics/Outline/setOval(Landroid/graphics/Rect;)V
return
.limit locals 2
.limit stack 2
.end method

.method public final setTintList(Landroid/content/res/ColorStateList;)V
aload 0
aload 1
putfield android/support/design/widget/k/k Landroid/content/res/ColorStateList;
aload 0
aload 0
aload 1
aload 0
getfield android/support/design/widget/k/l Landroid/graphics/PorterDuff$Mode;
invokespecial android/support/design/widget/k/a(Landroid/content/res/ColorStateList;Landroid/graphics/PorterDuff$Mode;)Landroid/graphics/PorterDuffColorFilter;
putfield android/support/design/widget/k/m Landroid/graphics/PorterDuffColorFilter;
aload 0
invokevirtual android/support/design/widget/k/invalidateSelf()V
return
.limit locals 2
.limit stack 4
.end method

.method public final setTintMode(Landroid/graphics/PorterDuff$Mode;)V
aload 0
aload 1
putfield android/support/design/widget/k/l Landroid/graphics/PorterDuff$Mode;
aload 0
aload 0
aload 0
getfield android/support/design/widget/k/k Landroid/content/res/ColorStateList;
aload 1
invokespecial android/support/design/widget/k/a(Landroid/content/res/ColorStateList;Landroid/graphics/PorterDuff$Mode;)Landroid/graphics/PorterDuffColorFilter;
putfield android/support/design/widget/k/m Landroid/graphics/PorterDuffColorFilter;
aload 0
invokevirtual android/support/design/widget/k/invalidateSelf()V
return
.limit locals 2
.limit stack 4
.end method
