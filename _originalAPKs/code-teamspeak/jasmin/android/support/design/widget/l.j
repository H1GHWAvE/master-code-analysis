.bytecode 50.0
.class final synchronized android/support/design/widget/l
.super java/lang/Object

.field private static final 'j' Z

.field private static final 'k' Z = 0


.field private static final 'l' Landroid/graphics/Paint;

.field private 'A' Z

.field private 'B' Landroid/graphics/Bitmap;

.field private 'C' Landroid/graphics/Paint;

.field private 'D' F

.field private 'E' F

.field private 'F' F

.field private 'G' F

.field private 'H' Z

.field private 'I' Landroid/view/animation/Interpolator;

.field 'a' F

.field 'b' I

.field 'c' I

.field 'd' F

.field 'e' F

.field 'f' I

.field 'g' Ljava/lang/CharSequence;

.field final 'h' Landroid/text/TextPaint;

.field 'i' Landroid/view/animation/Interpolator;

.field private final 'm' Landroid/view/View;

.field private 'n' Z

.field private final 'o' Landroid/graphics/Rect;

.field private final 'p' Landroid/graphics/Rect;

.field private final 'q' Landroid/graphics/RectF;

.field private 'r' I

.field private 's' F

.field private 't' F

.field private 'u' F

.field private 'v' F

.field private 'w' F

.field private 'x' F

.field private 'y' Ljava/lang/CharSequence;

.field private 'z' Z

.method static <clinit>()V
getstatic android/os/Build$VERSION/SDK_INT I
bipush 18
if_icmpge L0
iconst_1
istore 0
L1:
iload 0
putstatic android/support/design/widget/l/j Z
aconst_null
putstatic android/support/design/widget/l/l Landroid/graphics/Paint;
return
L0:
iconst_0
istore 0
goto L1
.limit locals 1
.limit stack 2
.end method

.method public <init>(Landroid/view/View;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
bipush 16
putfield android/support/design/widget/l/b I
aload 0
bipush 16
putfield android/support/design/widget/l/c I
aload 0
ldc_w 15.0F
putfield android/support/design/widget/l/d F
aload 0
ldc_w 15.0F
putfield android/support/design/widget/l/e F
aload 0
aload 1
putfield android/support/design/widget/l/m Landroid/view/View;
aload 0
new android/text/TextPaint
dup
invokespecial android/text/TextPaint/<init>()V
putfield android/support/design/widget/l/h Landroid/text/TextPaint;
aload 0
getfield android/support/design/widget/l/h Landroid/text/TextPaint;
iconst_1
invokevirtual android/text/TextPaint/setAntiAlias(Z)V
aload 0
new android/graphics/Rect
dup
invokespecial android/graphics/Rect/<init>()V
putfield android/support/design/widget/l/p Landroid/graphics/Rect;
aload 0
new android/graphics/Rect
dup
invokespecial android/graphics/Rect/<init>()V
putfield android/support/design/widget/l/o Landroid/graphics/Rect;
aload 0
new android/graphics/RectF
dup
invokespecial android/graphics/RectF/<init>()V
putfield android/support/design/widget/l/q Landroid/graphics/RectF;
return
.limit locals 2
.limit stack 3
.end method

.method private static a(FFFLandroid/view/animation/Interpolator;)F
fload 2
fstore 4
aload 3
ifnull L0
aload 3
fload 2
invokeinterface android/view/animation/Interpolator/getInterpolation(F)F 1
fstore 4
L0:
fload 0
fload 1
fload 4
invokestatic android/support/design/widget/a/a(FFF)F
freturn
.limit locals 5
.limit stack 3
.end method

.method private static a(IIF)I
fconst_1
fload 2
fsub
fstore 3
iload 0
invokestatic android/graphics/Color/alpha(I)I
i2f
fstore 4
iload 1
invokestatic android/graphics/Color/alpha(I)I
i2f
fstore 5
iload 0
invokestatic android/graphics/Color/red(I)I
i2f
fstore 6
iload 1
invokestatic android/graphics/Color/red(I)I
i2f
fstore 7
iload 0
invokestatic android/graphics/Color/green(I)I
i2f
fstore 8
iload 1
invokestatic android/graphics/Color/green(I)I
i2f
fstore 9
iload 0
invokestatic android/graphics/Color/blue(I)I
i2f
fstore 10
iload 1
invokestatic android/graphics/Color/blue(I)I
i2f
fstore 11
fload 4
fload 3
fmul
fload 5
fload 2
fmul
fadd
f2i
fload 6
fload 3
fmul
fload 7
fload 2
fmul
fadd
f2i
fload 8
fload 3
fmul
fload 9
fload 2
fmul
fadd
f2i
fload 3
fload 10
fmul
fload 11
fload 2
fmul
fadd
f2i
invokestatic android/graphics/Color/argb(IIII)I
ireturn
.limit locals 12
.limit stack 6
.end method

.method private static a(FF)Z
fload 0
fload 1
fsub
invokestatic java/lang/Math/abs(F)F
ldc_w 0.001F
fcmpg
ifge L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method private static a(Landroid/graphics/Rect;IIII)Z
aload 0
getfield android/graphics/Rect/left I
iload 1
if_icmpne L0
aload 0
getfield android/graphics/Rect/top I
iload 2
if_icmpne L0
aload 0
getfield android/graphics/Rect/right I
iload 3
if_icmpne L0
aload 0
getfield android/graphics/Rect/bottom I
iload 4
if_icmpne L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 5
.limit stack 2
.end method

.method private b()V
aload 0
getfield android/support/design/widget/l/p Landroid/graphics/Rect;
invokevirtual android/graphics/Rect/width()I
ifle L0
aload 0
getfield android/support/design/widget/l/p Landroid/graphics/Rect;
invokevirtual android/graphics/Rect/height()I
ifle L0
aload 0
getfield android/support/design/widget/l/o Landroid/graphics/Rect;
invokevirtual android/graphics/Rect/width()I
ifle L0
aload 0
getfield android/support/design/widget/l/o Landroid/graphics/Rect;
invokevirtual android/graphics/Rect/height()I
ifle L0
iconst_1
istore 1
L1:
aload 0
iload 1
putfield android/support/design/widget/l/n Z
return
L0:
iconst_0
istore 1
goto L1
.limit locals 2
.limit stack 2
.end method

.method private b(F)V
aload 0
getfield android/support/design/widget/l/d F
fload 1
fcmpl
ifeq L0
aload 0
fload 1
putfield android/support/design/widget/l/d F
aload 0
invokevirtual android/support/design/widget/l/a()V
L0:
return
.limit locals 2
.limit stack 2
.end method

.method private b(Landroid/view/animation/Interpolator;)V
aload 0
aload 1
putfield android/support/design/widget/l/i Landroid/view/animation/Interpolator;
aload 0
invokevirtual android/support/design/widget/l/a()V
return
.limit locals 2
.limit stack 2
.end method

.method private b(Ljava/lang/CharSequence;)Z
iconst_1
istore 2
aload 0
getfield android/support/design/widget/l/m Landroid/view/View;
invokestatic android/support/v4/view/cx/f(Landroid/view/View;)I
iconst_1
if_icmpne L0
L1:
iload 2
ifeq L2
getstatic android/support/v4/m/m/d Landroid/support/v4/m/l;
astore 3
L3:
aload 3
aload 1
iconst_0
aload 1
invokeinterface java/lang/CharSequence/length()I 0
invokeinterface android/support/v4/m/l/a(Ljava/lang/CharSequence;II)Z 3
ireturn
L0:
iconst_0
istore 2
goto L1
L2:
getstatic android/support/v4/m/m/c Landroid/support/v4/m/l;
astore 3
goto L3
.limit locals 4
.limit stack 4
.end method

.method private c()I
aload 0
getfield android/support/design/widget/l/b I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private c(F)V
aload 0
getfield android/support/design/widget/l/e F
fload 1
fcmpl
ifeq L0
aload 0
fload 1
putfield android/support/design/widget/l/e F
aload 0
invokevirtual android/support/design/widget/l/a()V
L0:
return
.limit locals 2
.limit stack 2
.end method

.method private d()I
aload 0
getfield android/support/design/widget/l/c I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private d(F)V
aload 0
getfield android/support/design/widget/l/q Landroid/graphics/RectF;
aload 0
getfield android/support/design/widget/l/o Landroid/graphics/Rect;
getfield android/graphics/Rect/left I
i2f
aload 0
getfield android/support/design/widget/l/p Landroid/graphics/Rect;
getfield android/graphics/Rect/left I
i2f
fload 1
aload 0
getfield android/support/design/widget/l/i Landroid/view/animation/Interpolator;
invokestatic android/support/design/widget/l/a(FFFLandroid/view/animation/Interpolator;)F
putfield android/graphics/RectF/left F
aload 0
getfield android/support/design/widget/l/q Landroid/graphics/RectF;
aload 0
getfield android/support/design/widget/l/s F
aload 0
getfield android/support/design/widget/l/t F
fload 1
aload 0
getfield android/support/design/widget/l/i Landroid/view/animation/Interpolator;
invokestatic android/support/design/widget/l/a(FFFLandroid/view/animation/Interpolator;)F
putfield android/graphics/RectF/top F
aload 0
getfield android/support/design/widget/l/q Landroid/graphics/RectF;
aload 0
getfield android/support/design/widget/l/o Landroid/graphics/Rect;
getfield android/graphics/Rect/right I
i2f
aload 0
getfield android/support/design/widget/l/p Landroid/graphics/Rect;
getfield android/graphics/Rect/right I
i2f
fload 1
aload 0
getfield android/support/design/widget/l/i Landroid/view/animation/Interpolator;
invokestatic android/support/design/widget/l/a(FFFLandroid/view/animation/Interpolator;)F
putfield android/graphics/RectF/right F
aload 0
getfield android/support/design/widget/l/q Landroid/graphics/RectF;
aload 0
getfield android/support/design/widget/l/o Landroid/graphics/Rect;
getfield android/graphics/Rect/bottom I
i2f
aload 0
getfield android/support/design/widget/l/p Landroid/graphics/Rect;
getfield android/graphics/Rect/bottom I
i2f
fload 1
aload 0
getfield android/support/design/widget/l/i Landroid/view/animation/Interpolator;
invokestatic android/support/design/widget/l/a(FFFLandroid/view/animation/Interpolator;)F
putfield android/graphics/RectF/bottom F
return
.limit locals 2
.limit stack 5
.end method

.method private e()Landroid/graphics/Typeface;
aload 0
getfield android/support/design/widget/l/h Landroid/text/TextPaint;
invokevirtual android/text/TextPaint/getTypeface()Landroid/graphics/Typeface;
areturn
.limit locals 1
.limit stack 1
.end method

.method private e(F)V
iconst_1
istore 6
aload 0
getfield android/support/design/widget/l/g Ljava/lang/CharSequence;
ifnonnull L0
return
L0:
fload 1
aload 0
getfield android/support/design/widget/l/e F
invokestatic android/support/design/widget/l/a(FF)Z
ifeq L1
aload 0
getfield android/support/design/widget/l/p Landroid/graphics/Rect;
invokevirtual android/graphics/Rect/width()I
i2f
fstore 2
aload 0
getfield android/support/design/widget/l/e F
fstore 1
aload 0
fconst_1
putfield android/support/design/widget/l/F F
L2:
fload 2
fconst_0
fcmpl
ifle L3
aload 0
getfield android/support/design/widget/l/G F
fload 1
fcmpl
ifne L4
aload 0
getfield android/support/design/widget/l/H Z
ifeq L5
L4:
iconst_1
istore 4
L6:
aload 0
fload 1
putfield android/support/design/widget/l/G F
aload 0
iconst_0
putfield android/support/design/widget/l/H Z
L7:
aload 0
getfield android/support/design/widget/l/y Ljava/lang/CharSequence;
ifnull L8
iload 4
ifeq L9
L8:
aload 0
getfield android/support/design/widget/l/h Landroid/text/TextPaint;
aload 0
getfield android/support/design/widget/l/G F
invokevirtual android/text/TextPaint/setTextSize(F)V
aload 0
getfield android/support/design/widget/l/g Ljava/lang/CharSequence;
aload 0
getfield android/support/design/widget/l/h Landroid/text/TextPaint;
fload 2
getstatic android/text/TextUtils$TruncateAt/END Landroid/text/TextUtils$TruncateAt;
invokestatic android/text/TextUtils/ellipsize(Ljava/lang/CharSequence;Landroid/text/TextPaint;FLandroid/text/TextUtils$TruncateAt;)Ljava/lang/CharSequence;
astore 7
aload 0
getfield android/support/design/widget/l/y Ljava/lang/CharSequence;
ifnull L10
aload 0
getfield android/support/design/widget/l/y Ljava/lang/CharSequence;
aload 7
invokevirtual java/lang/Object/equals(Ljava/lang/Object;)Z
ifne L11
L10:
aload 0
aload 7
putfield android/support/design/widget/l/y Ljava/lang/CharSequence;
L11:
aload 0
getfield android/support/design/widget/l/y Ljava/lang/CharSequence;
astore 8
aload 0
getfield android/support/design/widget/l/m Landroid/view/View;
invokestatic android/support/v4/view/cx/f(Landroid/view/View;)I
iconst_1
if_icmpne L12
iconst_1
istore 4
L13:
iload 4
ifeq L14
getstatic android/support/v4/m/m/d Landroid/support/v4/m/l;
astore 7
L15:
aload 0
aload 7
aload 8
iconst_0
aload 8
invokeinterface java/lang/CharSequence/length()I 0
invokeinterface android/support/v4/m/l/a(Ljava/lang/CharSequence;II)Z 3
putfield android/support/design/widget/l/z Z
L9:
getstatic android/support/design/widget/l/j Z
ifeq L16
aload 0
getfield android/support/design/widget/l/F F
fconst_1
fcmpl
ifeq L16
L17:
aload 0
iload 6
putfield android/support/design/widget/l/A Z
aload 0
getfield android/support/design/widget/l/A Z
ifeq L18
aload 0
getfield android/support/design/widget/l/B Landroid/graphics/Bitmap;
ifnonnull L18
aload 0
getfield android/support/design/widget/l/o Landroid/graphics/Rect;
invokevirtual android/graphics/Rect/isEmpty()Z
ifne L18
aload 0
getfield android/support/design/widget/l/y Ljava/lang/CharSequence;
invokestatic android/text/TextUtils/isEmpty(Ljava/lang/CharSequence;)Z
ifeq L19
L18:
aload 0
getfield android/support/design/widget/l/m Landroid/view/View;
invokestatic android/support/v4/view/cx/b(Landroid/view/View;)V
return
L1:
aload 0
getfield android/support/design/widget/l/o Landroid/graphics/Rect;
invokevirtual android/graphics/Rect/width()I
i2f
fstore 3
aload 0
getfield android/support/design/widget/l/d F
fstore 2
fload 1
aload 0
getfield android/support/design/widget/l/d F
invokestatic android/support/design/widget/l/a(FF)Z
ifeq L20
aload 0
fconst_1
putfield android/support/design/widget/l/F F
fload 2
fstore 1
fload 3
fstore 2
goto L2
L20:
aload 0
fload 1
aload 0
getfield android/support/design/widget/l/d F
fdiv
putfield android/support/design/widget/l/F F
fload 2
fstore 1
fload 3
fstore 2
goto L2
L5:
iconst_0
istore 4
goto L6
L12:
iconst_0
istore 4
goto L13
L14:
getstatic android/support/v4/m/m/c Landroid/support/v4/m/l;
astore 7
goto L15
L16:
iconst_0
istore 6
goto L17
L19:
aload 0
getfield android/support/design/widget/l/h Landroid/text/TextPaint;
aload 0
getfield android/support/design/widget/l/d F
invokevirtual android/text/TextPaint/setTextSize(F)V
aload 0
getfield android/support/design/widget/l/h Landroid/text/TextPaint;
aload 0
getfield android/support/design/widget/l/r I
invokevirtual android/text/TextPaint/setColor(I)V
aload 0
aload 0
getfield android/support/design/widget/l/h Landroid/text/TextPaint;
invokevirtual android/text/TextPaint/ascent()F
putfield android/support/design/widget/l/D F
aload 0
aload 0
getfield android/support/design/widget/l/h Landroid/text/TextPaint;
invokevirtual android/text/TextPaint/descent()F
putfield android/support/design/widget/l/E F
aload 0
getfield android/support/design/widget/l/h Landroid/text/TextPaint;
aload 0
getfield android/support/design/widget/l/y Ljava/lang/CharSequence;
iconst_0
aload 0
getfield android/support/design/widget/l/y Ljava/lang/CharSequence;
invokeinterface java/lang/CharSequence/length()I 0
invokevirtual android/text/TextPaint/measureText(Ljava/lang/CharSequence;II)F
invokestatic java/lang/Math/round(F)I
istore 4
aload 0
getfield android/support/design/widget/l/E F
aload 0
getfield android/support/design/widget/l/D F
fsub
invokestatic java/lang/Math/round(F)I
istore 5
iload 4
ifgt L21
iload 5
ifle L18
L21:
aload 0
iload 4
iload 5
getstatic android/graphics/Bitmap$Config/ARGB_8888 Landroid/graphics/Bitmap$Config;
invokestatic android/graphics/Bitmap/createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
putfield android/support/design/widget/l/B Landroid/graphics/Bitmap;
new android/graphics/Canvas
dup
aload 0
getfield android/support/design/widget/l/B Landroid/graphics/Bitmap;
invokespecial android/graphics/Canvas/<init>(Landroid/graphics/Bitmap;)V
aload 0
getfield android/support/design/widget/l/y Ljava/lang/CharSequence;
iconst_0
aload 0
getfield android/support/design/widget/l/y Ljava/lang/CharSequence;
invokeinterface java/lang/CharSequence/length()I 0
fconst_0
iload 5
i2f
aload 0
getfield android/support/design/widget/l/h Landroid/text/TextPaint;
invokevirtual android/text/TextPaint/descent()F
fsub
aload 0
getfield android/support/design/widget/l/h Landroid/text/TextPaint;
invokevirtual android/graphics/Canvas/drawText(Ljava/lang/CharSequence;IIFFLandroid/graphics/Paint;)V
aload 0
getfield android/support/design/widget/l/C Landroid/graphics/Paint;
ifnonnull L18
aload 0
new android/graphics/Paint
dup
iconst_3
invokespecial android/graphics/Paint/<init>(I)V
putfield android/support/design/widget/l/C Landroid/graphics/Paint;
goto L18
L3:
iconst_0
istore 4
goto L7
.limit locals 9
.limit stack 7
.end method

.method private f()F
aload 0
getfield android/support/design/widget/l/a F
freturn
.limit locals 1
.limit stack 1
.end method

.method private g()F
aload 0
getfield android/support/design/widget/l/e F
freturn
.limit locals 1
.limit stack 1
.end method

.method private h()F
aload 0
getfield android/support/design/widget/l/d F
freturn
.limit locals 1
.limit stack 1
.end method

.method private i()V
iconst_1
istore 13
aload 0
getfield android/support/design/widget/l/a F
fstore 3
aload 0
getfield android/support/design/widget/l/q Landroid/graphics/RectF;
aload 0
getfield android/support/design/widget/l/o Landroid/graphics/Rect;
getfield android/graphics/Rect/left I
i2f
aload 0
getfield android/support/design/widget/l/p Landroid/graphics/Rect;
getfield android/graphics/Rect/left I
i2f
fload 3
aload 0
getfield android/support/design/widget/l/i Landroid/view/animation/Interpolator;
invokestatic android/support/design/widget/l/a(FFFLandroid/view/animation/Interpolator;)F
putfield android/graphics/RectF/left F
aload 0
getfield android/support/design/widget/l/q Landroid/graphics/RectF;
aload 0
getfield android/support/design/widget/l/s F
aload 0
getfield android/support/design/widget/l/t F
fload 3
aload 0
getfield android/support/design/widget/l/i Landroid/view/animation/Interpolator;
invokestatic android/support/design/widget/l/a(FFFLandroid/view/animation/Interpolator;)F
putfield android/graphics/RectF/top F
aload 0
getfield android/support/design/widget/l/q Landroid/graphics/RectF;
aload 0
getfield android/support/design/widget/l/o Landroid/graphics/Rect;
getfield android/graphics/Rect/right I
i2f
aload 0
getfield android/support/design/widget/l/p Landroid/graphics/Rect;
getfield android/graphics/Rect/right I
i2f
fload 3
aload 0
getfield android/support/design/widget/l/i Landroid/view/animation/Interpolator;
invokestatic android/support/design/widget/l/a(FFFLandroid/view/animation/Interpolator;)F
putfield android/graphics/RectF/right F
aload 0
getfield android/support/design/widget/l/q Landroid/graphics/RectF;
aload 0
getfield android/support/design/widget/l/o Landroid/graphics/Rect;
getfield android/graphics/Rect/bottom I
i2f
aload 0
getfield android/support/design/widget/l/p Landroid/graphics/Rect;
getfield android/graphics/Rect/bottom I
i2f
fload 3
aload 0
getfield android/support/design/widget/l/i Landroid/view/animation/Interpolator;
invokestatic android/support/design/widget/l/a(FFFLandroid/view/animation/Interpolator;)F
putfield android/graphics/RectF/bottom F
aload 0
aload 0
getfield android/support/design/widget/l/u F
aload 0
getfield android/support/design/widget/l/v F
fload 3
aload 0
getfield android/support/design/widget/l/i Landroid/view/animation/Interpolator;
invokestatic android/support/design/widget/l/a(FFFLandroid/view/animation/Interpolator;)F
putfield android/support/design/widget/l/w F
aload 0
aload 0
getfield android/support/design/widget/l/s F
aload 0
getfield android/support/design/widget/l/t F
fload 3
aload 0
getfield android/support/design/widget/l/i Landroid/view/animation/Interpolator;
invokestatic android/support/design/widget/l/a(FFFLandroid/view/animation/Interpolator;)F
putfield android/support/design/widget/l/x F
aload 0
getfield android/support/design/widget/l/d F
aload 0
getfield android/support/design/widget/l/e F
fload 3
aload 0
getfield android/support/design/widget/l/I Landroid/view/animation/Interpolator;
invokestatic android/support/design/widget/l/a(FFFLandroid/view/animation/Interpolator;)F
fstore 4
aload 0
getfield android/support/design/widget/l/g Ljava/lang/CharSequence;
ifnull L0
fload 4
aload 0
getfield android/support/design/widget/l/e F
invokestatic android/support/design/widget/l/a(FF)Z
ifeq L1
aload 0
getfield android/support/design/widget/l/p Landroid/graphics/Rect;
invokevirtual android/graphics/Rect/width()I
i2f
fstore 2
aload 0
getfield android/support/design/widget/l/e F
fstore 1
aload 0
fconst_1
putfield android/support/design/widget/l/F F
L2:
fload 2
fconst_0
fcmpl
ifle L3
aload 0
getfield android/support/design/widget/l/G F
fload 1
fcmpl
ifne L4
aload 0
getfield android/support/design/widget/l/H Z
ifeq L5
L4:
iconst_1
istore 11
L6:
aload 0
fload 1
putfield android/support/design/widget/l/G F
aload 0
iconst_0
putfield android/support/design/widget/l/H Z
L7:
aload 0
getfield android/support/design/widget/l/y Ljava/lang/CharSequence;
ifnull L8
iload 11
ifeq L9
L8:
aload 0
getfield android/support/design/widget/l/h Landroid/text/TextPaint;
aload 0
getfield android/support/design/widget/l/G F
invokevirtual android/text/TextPaint/setTextSize(F)V
aload 0
getfield android/support/design/widget/l/g Ljava/lang/CharSequence;
aload 0
getfield android/support/design/widget/l/h Landroid/text/TextPaint;
fload 2
getstatic android/text/TextUtils$TruncateAt/END Landroid/text/TextUtils$TruncateAt;
invokestatic android/text/TextUtils/ellipsize(Ljava/lang/CharSequence;Landroid/text/TextPaint;FLandroid/text/TextUtils$TruncateAt;)Ljava/lang/CharSequence;
astore 14
aload 0
getfield android/support/design/widget/l/y Ljava/lang/CharSequence;
ifnull L10
aload 0
getfield android/support/design/widget/l/y Ljava/lang/CharSequence;
aload 14
invokevirtual java/lang/Object/equals(Ljava/lang/Object;)Z
ifne L11
L10:
aload 0
aload 14
putfield android/support/design/widget/l/y Ljava/lang/CharSequence;
L11:
aload 0
getfield android/support/design/widget/l/y Ljava/lang/CharSequence;
astore 15
aload 0
getfield android/support/design/widget/l/m Landroid/view/View;
invokestatic android/support/v4/view/cx/f(Landroid/view/View;)I
iconst_1
if_icmpne L12
iconst_1
istore 11
L13:
iload 11
ifeq L14
getstatic android/support/v4/m/m/d Landroid/support/v4/m/l;
astore 14
L15:
aload 0
aload 14
aload 15
iconst_0
aload 15
invokeinterface java/lang/CharSequence/length()I 0
invokeinterface android/support/v4/m/l/a(Ljava/lang/CharSequence;II)Z 3
putfield android/support/design/widget/l/z Z
L9:
getstatic android/support/design/widget/l/j Z
ifeq L16
aload 0
getfield android/support/design/widget/l/F F
fconst_1
fcmpl
ifeq L16
L17:
aload 0
iload 13
putfield android/support/design/widget/l/A Z
aload 0
getfield android/support/design/widget/l/A Z
ifeq L18
aload 0
getfield android/support/design/widget/l/B Landroid/graphics/Bitmap;
ifnonnull L18
aload 0
getfield android/support/design/widget/l/o Landroid/graphics/Rect;
invokevirtual android/graphics/Rect/isEmpty()Z
ifne L18
aload 0
getfield android/support/design/widget/l/y Ljava/lang/CharSequence;
invokestatic android/text/TextUtils/isEmpty(Ljava/lang/CharSequence;)Z
ifeq L19
L18:
aload 0
getfield android/support/design/widget/l/m Landroid/view/View;
invokestatic android/support/v4/view/cx/b(Landroid/view/View;)V
L0:
aload 0
getfield android/support/design/widget/l/f I
aload 0
getfield android/support/design/widget/l/r I
if_icmpeq L20
aload 0
getfield android/support/design/widget/l/h Landroid/text/TextPaint;
astore 14
aload 0
getfield android/support/design/widget/l/r I
istore 11
aload 0
getfield android/support/design/widget/l/f I
istore 12
fconst_1
fload 3
fsub
fstore 1
iload 11
invokestatic android/graphics/Color/alpha(I)I
i2f
fstore 2
iload 12
invokestatic android/graphics/Color/alpha(I)I
i2f
fstore 4
iload 11
invokestatic android/graphics/Color/red(I)I
i2f
fstore 5
iload 12
invokestatic android/graphics/Color/red(I)I
i2f
fstore 6
iload 11
invokestatic android/graphics/Color/green(I)I
i2f
fstore 7
iload 12
invokestatic android/graphics/Color/green(I)I
i2f
fstore 8
iload 11
invokestatic android/graphics/Color/blue(I)I
i2f
fstore 9
iload 12
invokestatic android/graphics/Color/blue(I)I
i2f
fstore 10
aload 14
fload 2
fload 1
fmul
fload 4
fload 3
fmul
fadd
f2i
fload 5
fload 1
fmul
fload 6
fload 3
fmul
fadd
f2i
fload 7
fload 1
fmul
fload 8
fload 3
fmul
fadd
f2i
fload 9
fload 1
fmul
fload 10
fload 3
fmul
fadd
f2i
invokestatic android/graphics/Color/argb(IIII)I
invokevirtual android/text/TextPaint/setColor(I)V
L21:
aload 0
getfield android/support/design/widget/l/m Landroid/view/View;
invokestatic android/support/v4/view/cx/b(Landroid/view/View;)V
return
L1:
aload 0
getfield android/support/design/widget/l/o Landroid/graphics/Rect;
invokevirtual android/graphics/Rect/width()I
i2f
fstore 2
aload 0
getfield android/support/design/widget/l/d F
fstore 1
fload 4
aload 0
getfield android/support/design/widget/l/d F
invokestatic android/support/design/widget/l/a(FF)Z
ifeq L22
aload 0
fconst_1
putfield android/support/design/widget/l/F F
goto L2
L22:
aload 0
fload 4
aload 0
getfield android/support/design/widget/l/d F
fdiv
putfield android/support/design/widget/l/F F
goto L2
L5:
iconst_0
istore 11
goto L6
L12:
iconst_0
istore 11
goto L13
L14:
getstatic android/support/v4/m/m/c Landroid/support/v4/m/l;
astore 14
goto L15
L16:
iconst_0
istore 13
goto L17
L19:
aload 0
getfield android/support/design/widget/l/h Landroid/text/TextPaint;
aload 0
getfield android/support/design/widget/l/d F
invokevirtual android/text/TextPaint/setTextSize(F)V
aload 0
getfield android/support/design/widget/l/h Landroid/text/TextPaint;
aload 0
getfield android/support/design/widget/l/r I
invokevirtual android/text/TextPaint/setColor(I)V
aload 0
aload 0
getfield android/support/design/widget/l/h Landroid/text/TextPaint;
invokevirtual android/text/TextPaint/ascent()F
putfield android/support/design/widget/l/D F
aload 0
aload 0
getfield android/support/design/widget/l/h Landroid/text/TextPaint;
invokevirtual android/text/TextPaint/descent()F
putfield android/support/design/widget/l/E F
aload 0
getfield android/support/design/widget/l/h Landroid/text/TextPaint;
aload 0
getfield android/support/design/widget/l/y Ljava/lang/CharSequence;
iconst_0
aload 0
getfield android/support/design/widget/l/y Ljava/lang/CharSequence;
invokeinterface java/lang/CharSequence/length()I 0
invokevirtual android/text/TextPaint/measureText(Ljava/lang/CharSequence;II)F
invokestatic java/lang/Math/round(F)I
istore 11
aload 0
getfield android/support/design/widget/l/E F
aload 0
getfield android/support/design/widget/l/D F
fsub
invokestatic java/lang/Math/round(F)I
istore 12
iload 11
ifgt L23
iload 12
ifle L18
L23:
aload 0
iload 11
iload 12
getstatic android/graphics/Bitmap$Config/ARGB_8888 Landroid/graphics/Bitmap$Config;
invokestatic android/graphics/Bitmap/createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
putfield android/support/design/widget/l/B Landroid/graphics/Bitmap;
new android/graphics/Canvas
dup
aload 0
getfield android/support/design/widget/l/B Landroid/graphics/Bitmap;
invokespecial android/graphics/Canvas/<init>(Landroid/graphics/Bitmap;)V
aload 0
getfield android/support/design/widget/l/y Ljava/lang/CharSequence;
iconst_0
aload 0
getfield android/support/design/widget/l/y Ljava/lang/CharSequence;
invokeinterface java/lang/CharSequence/length()I 0
fconst_0
iload 12
i2f
aload 0
getfield android/support/design/widget/l/h Landroid/text/TextPaint;
invokevirtual android/text/TextPaint/descent()F
fsub
aload 0
getfield android/support/design/widget/l/h Landroid/text/TextPaint;
invokevirtual android/graphics/Canvas/drawText(Ljava/lang/CharSequence;IIFFLandroid/graphics/Paint;)V
aload 0
getfield android/support/design/widget/l/C Landroid/graphics/Paint;
ifnonnull L18
aload 0
new android/graphics/Paint
dup
iconst_3
invokespecial android/graphics/Paint/<init>(I)V
putfield android/support/design/widget/l/C Landroid/graphics/Paint;
goto L18
L20:
aload 0
getfield android/support/design/widget/l/h Landroid/text/TextPaint;
aload 0
getfield android/support/design/widget/l/f I
invokevirtual android/text/TextPaint/setColor(I)V
goto L21
L3:
iconst_0
istore 11
goto L7
.limit locals 16
.limit stack 7
.end method

.method private j()V
iconst_1
istore 4
fconst_0
fstore 2
aload 0
getfield android/support/design/widget/l/h Landroid/text/TextPaint;
aload 0
getfield android/support/design/widget/l/e F
invokevirtual android/text/TextPaint/setTextSize(F)V
aload 0
getfield android/support/design/widget/l/y Ljava/lang/CharSequence;
ifnull L0
aload 0
getfield android/support/design/widget/l/h Landroid/text/TextPaint;
aload 0
getfield android/support/design/widget/l/y Ljava/lang/CharSequence;
iconst_0
aload 0
getfield android/support/design/widget/l/y Ljava/lang/CharSequence;
invokeinterface java/lang/CharSequence/length()I 0
invokevirtual android/text/TextPaint/measureText(Ljava/lang/CharSequence;II)F
fstore 1
L1:
aload 0
getfield android/support/design/widget/l/c I
istore 5
aload 0
getfield android/support/design/widget/l/z Z
ifeq L2
iconst_1
istore 3
L3:
iload 5
iload 3
invokestatic android/support/v4/view/v/a(II)I
istore 3
iload 3
bipush 112
iand
lookupswitch
48 : L4
80 : L5
default : L6
L6:
aload 0
aload 0
getfield android/support/design/widget/l/h Landroid/text/TextPaint;
invokevirtual android/text/TextPaint/descent()F
aload 0
getfield android/support/design/widget/l/h Landroid/text/TextPaint;
invokevirtual android/text/TextPaint/ascent()F
fsub
fconst_2
fdiv
aload 0
getfield android/support/design/widget/l/h Landroid/text/TextPaint;
invokevirtual android/text/TextPaint/descent()F
fsub
aload 0
getfield android/support/design/widget/l/p Landroid/graphics/Rect;
invokevirtual android/graphics/Rect/centerY()I
i2f
fadd
putfield android/support/design/widget/l/t F
L7:
iload 3
bipush 7
iand
lookupswitch
1 : L8
5 : L9
default : L10
L10:
aload 0
aload 0
getfield android/support/design/widget/l/p Landroid/graphics/Rect;
getfield android/graphics/Rect/left I
i2f
putfield android/support/design/widget/l/v F
L11:
aload 0
getfield android/support/design/widget/l/h Landroid/text/TextPaint;
aload 0
getfield android/support/design/widget/l/d F
invokevirtual android/text/TextPaint/setTextSize(F)V
fload 2
fstore 1
aload 0
getfield android/support/design/widget/l/y Ljava/lang/CharSequence;
ifnull L12
aload 0
getfield android/support/design/widget/l/h Landroid/text/TextPaint;
aload 0
getfield android/support/design/widget/l/y Ljava/lang/CharSequence;
iconst_0
aload 0
getfield android/support/design/widget/l/y Ljava/lang/CharSequence;
invokeinterface java/lang/CharSequence/length()I 0
invokevirtual android/text/TextPaint/measureText(Ljava/lang/CharSequence;II)F
fstore 1
L12:
aload 0
getfield android/support/design/widget/l/b I
istore 5
aload 0
getfield android/support/design/widget/l/z Z
ifeq L13
iload 4
istore 3
L14:
iload 5
iload 3
invokestatic android/support/v4/view/v/a(II)I
istore 3
iload 3
bipush 112
iand
lookupswitch
48 : L15
80 : L16
default : L17
L17:
aload 0
aload 0
getfield android/support/design/widget/l/h Landroid/text/TextPaint;
invokevirtual android/text/TextPaint/descent()F
aload 0
getfield android/support/design/widget/l/h Landroid/text/TextPaint;
invokevirtual android/text/TextPaint/ascent()F
fsub
fconst_2
fdiv
aload 0
getfield android/support/design/widget/l/h Landroid/text/TextPaint;
invokevirtual android/text/TextPaint/descent()F
fsub
aload 0
getfield android/support/design/widget/l/o Landroid/graphics/Rect;
invokevirtual android/graphics/Rect/centerY()I
i2f
fadd
putfield android/support/design/widget/l/s F
L18:
iload 3
bipush 7
iand
lookupswitch
1 : L19
5 : L20
default : L21
L21:
aload 0
aload 0
getfield android/support/design/widget/l/o Landroid/graphics/Rect;
getfield android/graphics/Rect/left I
i2f
putfield android/support/design/widget/l/u F
L22:
aload 0
invokespecial android/support/design/widget/l/m()V
return
L0:
fconst_0
fstore 1
goto L1
L2:
iconst_0
istore 3
goto L3
L5:
aload 0
aload 0
getfield android/support/design/widget/l/p Landroid/graphics/Rect;
getfield android/graphics/Rect/bottom I
i2f
putfield android/support/design/widget/l/t F
goto L7
L4:
aload 0
aload 0
getfield android/support/design/widget/l/p Landroid/graphics/Rect;
getfield android/graphics/Rect/top I
i2f
aload 0
getfield android/support/design/widget/l/h Landroid/text/TextPaint;
invokevirtual android/text/TextPaint/ascent()F
fsub
putfield android/support/design/widget/l/t F
goto L7
L8:
aload 0
aload 0
getfield android/support/design/widget/l/p Landroid/graphics/Rect;
invokevirtual android/graphics/Rect/centerX()I
i2f
fload 1
fconst_2
fdiv
fsub
putfield android/support/design/widget/l/v F
goto L11
L9:
aload 0
aload 0
getfield android/support/design/widget/l/p Landroid/graphics/Rect;
getfield android/graphics/Rect/right I
i2f
fload 1
fsub
putfield android/support/design/widget/l/v F
goto L11
L13:
iconst_0
istore 3
goto L14
L16:
aload 0
aload 0
getfield android/support/design/widget/l/o Landroid/graphics/Rect;
getfield android/graphics/Rect/bottom I
i2f
putfield android/support/design/widget/l/s F
goto L18
L15:
aload 0
aload 0
getfield android/support/design/widget/l/o Landroid/graphics/Rect;
getfield android/graphics/Rect/top I
i2f
aload 0
getfield android/support/design/widget/l/h Landroid/text/TextPaint;
invokevirtual android/text/TextPaint/ascent()F
fsub
putfield android/support/design/widget/l/s F
goto L18
L19:
aload 0
aload 0
getfield android/support/design/widget/l/o Landroid/graphics/Rect;
invokevirtual android/graphics/Rect/centerX()I
i2f
fload 1
fconst_2
fdiv
fsub
putfield android/support/design/widget/l/u F
goto L22
L20:
aload 0
aload 0
getfield android/support/design/widget/l/o Landroid/graphics/Rect;
getfield android/graphics/Rect/right I
i2f
fload 1
fsub
putfield android/support/design/widget/l/u F
goto L22
.limit locals 6
.limit stack 4
.end method

.method private k()V
aload 0
getfield android/support/design/widget/l/B Landroid/graphics/Bitmap;
ifnonnull L0
aload 0
getfield android/support/design/widget/l/o Landroid/graphics/Rect;
invokevirtual android/graphics/Rect/isEmpty()Z
ifne L0
aload 0
getfield android/support/design/widget/l/y Ljava/lang/CharSequence;
invokestatic android/text/TextUtils/isEmpty(Ljava/lang/CharSequence;)Z
ifeq L1
L0:
return
L1:
aload 0
getfield android/support/design/widget/l/h Landroid/text/TextPaint;
aload 0
getfield android/support/design/widget/l/d F
invokevirtual android/text/TextPaint/setTextSize(F)V
aload 0
getfield android/support/design/widget/l/h Landroid/text/TextPaint;
aload 0
getfield android/support/design/widget/l/r I
invokevirtual android/text/TextPaint/setColor(I)V
aload 0
aload 0
getfield android/support/design/widget/l/h Landroid/text/TextPaint;
invokevirtual android/text/TextPaint/ascent()F
putfield android/support/design/widget/l/D F
aload 0
aload 0
getfield android/support/design/widget/l/h Landroid/text/TextPaint;
invokevirtual android/text/TextPaint/descent()F
putfield android/support/design/widget/l/E F
aload 0
getfield android/support/design/widget/l/h Landroid/text/TextPaint;
aload 0
getfield android/support/design/widget/l/y Ljava/lang/CharSequence;
iconst_0
aload 0
getfield android/support/design/widget/l/y Ljava/lang/CharSequence;
invokeinterface java/lang/CharSequence/length()I 0
invokevirtual android/text/TextPaint/measureText(Ljava/lang/CharSequence;II)F
invokestatic java/lang/Math/round(F)I
istore 1
aload 0
getfield android/support/design/widget/l/E F
aload 0
getfield android/support/design/widget/l/D F
fsub
invokestatic java/lang/Math/round(F)I
istore 2
iload 1
ifgt L2
iload 2
ifle L0
L2:
aload 0
iload 1
iload 2
getstatic android/graphics/Bitmap$Config/ARGB_8888 Landroid/graphics/Bitmap$Config;
invokestatic android/graphics/Bitmap/createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
putfield android/support/design/widget/l/B Landroid/graphics/Bitmap;
new android/graphics/Canvas
dup
aload 0
getfield android/support/design/widget/l/B Landroid/graphics/Bitmap;
invokespecial android/graphics/Canvas/<init>(Landroid/graphics/Bitmap;)V
aload 0
getfield android/support/design/widget/l/y Ljava/lang/CharSequence;
iconst_0
aload 0
getfield android/support/design/widget/l/y Ljava/lang/CharSequence;
invokeinterface java/lang/CharSequence/length()I 0
fconst_0
iload 2
i2f
aload 0
getfield android/support/design/widget/l/h Landroid/text/TextPaint;
invokevirtual android/text/TextPaint/descent()F
fsub
aload 0
getfield android/support/design/widget/l/h Landroid/text/TextPaint;
invokevirtual android/graphics/Canvas/drawText(Ljava/lang/CharSequence;IIFFLandroid/graphics/Paint;)V
aload 0
getfield android/support/design/widget/l/C Landroid/graphics/Paint;
ifnonnull L0
aload 0
new android/graphics/Paint
dup
iconst_3
invokespecial android/graphics/Paint/<init>(I)V
putfield android/support/design/widget/l/C Landroid/graphics/Paint;
return
.limit locals 3
.limit stack 7
.end method

.method private l()Ljava/lang/CharSequence;
aload 0
getfield android/support/design/widget/l/g Ljava/lang/CharSequence;
areturn
.limit locals 1
.limit stack 1
.end method

.method private m()V
aload 0
getfield android/support/design/widget/l/B Landroid/graphics/Bitmap;
ifnull L0
aload 0
getfield android/support/design/widget/l/B Landroid/graphics/Bitmap;
invokevirtual android/graphics/Bitmap/recycle()V
aload 0
aconst_null
putfield android/support/design/widget/l/B Landroid/graphics/Bitmap;
L0:
return
.limit locals 1
.limit stack 2
.end method

.method private n()I
aload 0
getfield android/support/design/widget/l/r I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private o()I
aload 0
getfield android/support/design/widget/l/f I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final a()V
iconst_1
istore 4
fconst_0
fstore 2
aload 0
getfield android/support/design/widget/l/m Landroid/view/View;
invokevirtual android/view/View/getHeight()I
ifle L0
aload 0
getfield android/support/design/widget/l/m Landroid/view/View;
invokevirtual android/view/View/getWidth()I
ifle L0
aload 0
getfield android/support/design/widget/l/h Landroid/text/TextPaint;
aload 0
getfield android/support/design/widget/l/e F
invokevirtual android/text/TextPaint/setTextSize(F)V
aload 0
getfield android/support/design/widget/l/y Ljava/lang/CharSequence;
ifnull L1
aload 0
getfield android/support/design/widget/l/h Landroid/text/TextPaint;
aload 0
getfield android/support/design/widget/l/y Ljava/lang/CharSequence;
iconst_0
aload 0
getfield android/support/design/widget/l/y Ljava/lang/CharSequence;
invokeinterface java/lang/CharSequence/length()I 0
invokevirtual android/text/TextPaint/measureText(Ljava/lang/CharSequence;II)F
fstore 1
L2:
aload 0
getfield android/support/design/widget/l/c I
istore 5
aload 0
getfield android/support/design/widget/l/z Z
ifeq L3
iconst_1
istore 3
L4:
iload 5
iload 3
invokestatic android/support/v4/view/v/a(II)I
istore 3
iload 3
bipush 112
iand
lookupswitch
48 : L5
80 : L6
default : L7
L7:
aload 0
aload 0
getfield android/support/design/widget/l/h Landroid/text/TextPaint;
invokevirtual android/text/TextPaint/descent()F
aload 0
getfield android/support/design/widget/l/h Landroid/text/TextPaint;
invokevirtual android/text/TextPaint/ascent()F
fsub
fconst_2
fdiv
aload 0
getfield android/support/design/widget/l/h Landroid/text/TextPaint;
invokevirtual android/text/TextPaint/descent()F
fsub
aload 0
getfield android/support/design/widget/l/p Landroid/graphics/Rect;
invokevirtual android/graphics/Rect/centerY()I
i2f
fadd
putfield android/support/design/widget/l/t F
L8:
iload 3
bipush 7
iand
lookupswitch
1 : L9
5 : L10
default : L11
L11:
aload 0
aload 0
getfield android/support/design/widget/l/p Landroid/graphics/Rect;
getfield android/graphics/Rect/left I
i2f
putfield android/support/design/widget/l/v F
L12:
aload 0
getfield android/support/design/widget/l/h Landroid/text/TextPaint;
aload 0
getfield android/support/design/widget/l/d F
invokevirtual android/text/TextPaint/setTextSize(F)V
fload 2
fstore 1
aload 0
getfield android/support/design/widget/l/y Ljava/lang/CharSequence;
ifnull L13
aload 0
getfield android/support/design/widget/l/h Landroid/text/TextPaint;
aload 0
getfield android/support/design/widget/l/y Ljava/lang/CharSequence;
iconst_0
aload 0
getfield android/support/design/widget/l/y Ljava/lang/CharSequence;
invokeinterface java/lang/CharSequence/length()I 0
invokevirtual android/text/TextPaint/measureText(Ljava/lang/CharSequence;II)F
fstore 1
L13:
aload 0
getfield android/support/design/widget/l/b I
istore 5
aload 0
getfield android/support/design/widget/l/z Z
ifeq L14
iload 4
istore 3
L15:
iload 5
iload 3
invokestatic android/support/v4/view/v/a(II)I
istore 3
iload 3
bipush 112
iand
lookupswitch
48 : L16
80 : L17
default : L18
L18:
aload 0
aload 0
getfield android/support/design/widget/l/h Landroid/text/TextPaint;
invokevirtual android/text/TextPaint/descent()F
aload 0
getfield android/support/design/widget/l/h Landroid/text/TextPaint;
invokevirtual android/text/TextPaint/ascent()F
fsub
fconst_2
fdiv
aload 0
getfield android/support/design/widget/l/h Landroid/text/TextPaint;
invokevirtual android/text/TextPaint/descent()F
fsub
aload 0
getfield android/support/design/widget/l/o Landroid/graphics/Rect;
invokevirtual android/graphics/Rect/centerY()I
i2f
fadd
putfield android/support/design/widget/l/s F
L19:
iload 3
bipush 7
iand
lookupswitch
1 : L20
5 : L21
default : L22
L22:
aload 0
aload 0
getfield android/support/design/widget/l/o Landroid/graphics/Rect;
getfield android/graphics/Rect/left I
i2f
putfield android/support/design/widget/l/u F
L23:
aload 0
invokespecial android/support/design/widget/l/m()V
aload 0
invokespecial android/support/design/widget/l/i()V
L0:
return
L1:
fconst_0
fstore 1
goto L2
L3:
iconst_0
istore 3
goto L4
L6:
aload 0
aload 0
getfield android/support/design/widget/l/p Landroid/graphics/Rect;
getfield android/graphics/Rect/bottom I
i2f
putfield android/support/design/widget/l/t F
goto L8
L5:
aload 0
aload 0
getfield android/support/design/widget/l/p Landroid/graphics/Rect;
getfield android/graphics/Rect/top I
i2f
aload 0
getfield android/support/design/widget/l/h Landroid/text/TextPaint;
invokevirtual android/text/TextPaint/ascent()F
fsub
putfield android/support/design/widget/l/t F
goto L8
L9:
aload 0
aload 0
getfield android/support/design/widget/l/p Landroid/graphics/Rect;
invokevirtual android/graphics/Rect/centerX()I
i2f
fload 1
fconst_2
fdiv
fsub
putfield android/support/design/widget/l/v F
goto L12
L10:
aload 0
aload 0
getfield android/support/design/widget/l/p Landroid/graphics/Rect;
getfield android/graphics/Rect/right I
i2f
fload 1
fsub
putfield android/support/design/widget/l/v F
goto L12
L14:
iconst_0
istore 3
goto L15
L17:
aload 0
aload 0
getfield android/support/design/widget/l/o Landroid/graphics/Rect;
getfield android/graphics/Rect/bottom I
i2f
putfield android/support/design/widget/l/s F
goto L19
L16:
aload 0
aload 0
getfield android/support/design/widget/l/o Landroid/graphics/Rect;
getfield android/graphics/Rect/top I
i2f
aload 0
getfield android/support/design/widget/l/h Landroid/text/TextPaint;
invokevirtual android/text/TextPaint/ascent()F
fsub
putfield android/support/design/widget/l/s F
goto L19
L20:
aload 0
aload 0
getfield android/support/design/widget/l/o Landroid/graphics/Rect;
invokevirtual android/graphics/Rect/centerX()I
i2f
fload 1
fconst_2
fdiv
fsub
putfield android/support/design/widget/l/u F
goto L23
L21:
aload 0
aload 0
getfield android/support/design/widget/l/o Landroid/graphics/Rect;
getfield android/graphics/Rect/right I
i2f
fload 1
fsub
putfield android/support/design/widget/l/u F
goto L23
.limit locals 6
.limit stack 4
.end method

.method final a(F)V
fload 1
fconst_0
fcmpg
ifge L0
fconst_0
fstore 2
L1:
fload 2
aload 0
getfield android/support/design/widget/l/a F
fcmpl
ifeq L2
aload 0
fload 2
putfield android/support/design/widget/l/a F
aload 0
invokespecial android/support/design/widget/l/i()V
L2:
return
L0:
fload 1
fstore 2
fload 1
fconst_1
fcmpl
ifle L1
fconst_1
fstore 2
goto L1
.limit locals 3
.limit stack 2
.end method

.method final a(I)V
aload 0
getfield android/support/design/widget/l/f I
iload 1
if_icmpeq L0
aload 0
iload 1
putfield android/support/design/widget/l/f I
aload 0
invokevirtual android/support/design/widget/l/a()V
L0:
return
.limit locals 2
.limit stack 2
.end method

.method final a(IIII)V
aload 0
getfield android/support/design/widget/l/o Landroid/graphics/Rect;
iload 1
iload 2
iload 3
iload 4
invokestatic android/support/design/widget/l/a(Landroid/graphics/Rect;IIII)Z
ifne L0
aload 0
getfield android/support/design/widget/l/o Landroid/graphics/Rect;
iload 1
iload 2
iload 3
iload 4
invokevirtual android/graphics/Rect/set(IIII)V
aload 0
iconst_1
putfield android/support/design/widget/l/H Z
aload 0
invokespecial android/support/design/widget/l/b()V
L0:
return
.limit locals 5
.limit stack 5
.end method

.method public final a(Landroid/graphics/Canvas;)V
aload 1
invokevirtual android/graphics/Canvas/save()I
istore 7
aload 0
getfield android/support/design/widget/l/y Ljava/lang/CharSequence;
ifnull L0
aload 0
getfield android/support/design/widget/l/n Z
ifeq L0
aload 0
getfield android/support/design/widget/l/w F
fstore 5
aload 0
getfield android/support/design/widget/l/x F
fstore 4
aload 0
getfield android/support/design/widget/l/A Z
ifeq L1
aload 0
getfield android/support/design/widget/l/B Landroid/graphics/Bitmap;
ifnull L1
iconst_1
istore 6
L2:
aload 0
getfield android/support/design/widget/l/h Landroid/text/TextPaint;
aload 0
getfield android/support/design/widget/l/G F
invokevirtual android/text/TextPaint/setTextSize(F)V
iload 6
ifeq L3
aload 0
getfield android/support/design/widget/l/D F
aload 0
getfield android/support/design/widget/l/F F
fmul
fstore 2
L4:
fload 4
fstore 3
iload 6
ifeq L5
fload 4
fload 2
fadd
fstore 3
L5:
aload 0
getfield android/support/design/widget/l/F F
fconst_1
fcmpl
ifeq L6
aload 1
aload 0
getfield android/support/design/widget/l/F F
aload 0
getfield android/support/design/widget/l/F F
fload 5
fload 3
invokevirtual android/graphics/Canvas/scale(FFFF)V
L6:
iload 6
ifeq L7
aload 1
aload 0
getfield android/support/design/widget/l/B Landroid/graphics/Bitmap;
fload 5
fload 3
aload 0
getfield android/support/design/widget/l/C Landroid/graphics/Paint;
invokevirtual android/graphics/Canvas/drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V
L0:
aload 1
iload 7
invokevirtual android/graphics/Canvas/restoreToCount(I)V
return
L1:
iconst_0
istore 6
goto L2
L3:
aload 0
getfield android/support/design/widget/l/h Landroid/text/TextPaint;
invokevirtual android/text/TextPaint/ascent()F
pop
fconst_0
fstore 2
aload 0
getfield android/support/design/widget/l/h Landroid/text/TextPaint;
invokevirtual android/text/TextPaint/descent()F
pop
goto L4
L7:
aload 1
aload 0
getfield android/support/design/widget/l/y Ljava/lang/CharSequence;
iconst_0
aload 0
getfield android/support/design/widget/l/y Ljava/lang/CharSequence;
invokeinterface java/lang/CharSequence/length()I 0
fload 5
fload 3
aload 0
getfield android/support/design/widget/l/h Landroid/text/TextPaint;
invokevirtual android/graphics/Canvas/drawText(Ljava/lang/CharSequence;IIFFLandroid/graphics/Paint;)V
goto L0
.limit locals 8
.limit stack 7
.end method

.method final a(Landroid/graphics/Typeface;)V
aload 1
astore 2
aload 1
ifnonnull L0
getstatic android/graphics/Typeface/DEFAULT Landroid/graphics/Typeface;
astore 2
L0:
aload 0
getfield android/support/design/widget/l/h Landroid/text/TextPaint;
invokevirtual android/text/TextPaint/getTypeface()Landroid/graphics/Typeface;
aload 2
if_acmpeq L1
aload 0
getfield android/support/design/widget/l/h Landroid/text/TextPaint;
aload 2
invokevirtual android/text/TextPaint/setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;
pop
aload 0
invokevirtual android/support/design/widget/l/a()V
L1:
return
.limit locals 3
.limit stack 2
.end method

.method final a(Landroid/view/animation/Interpolator;)V
aload 0
aload 1
putfield android/support/design/widget/l/I Landroid/view/animation/Interpolator;
aload 0
invokevirtual android/support/design/widget/l/a()V
return
.limit locals 2
.limit stack 2
.end method

.method final a(Ljava/lang/CharSequence;)V
aload 1
ifnull L0
aload 1
aload 0
getfield android/support/design/widget/l/g Ljava/lang/CharSequence;
invokevirtual java/lang/Object/equals(Ljava/lang/Object;)Z
ifne L1
L0:
aload 0
aload 1
putfield android/support/design/widget/l/g Ljava/lang/CharSequence;
aload 0
aconst_null
putfield android/support/design/widget/l/y Ljava/lang/CharSequence;
aload 0
invokespecial android/support/design/widget/l/m()V
aload 0
invokevirtual android/support/design/widget/l/a()V
L1:
return
.limit locals 2
.limit stack 2
.end method

.method final b(I)V
aload 0
getfield android/support/design/widget/l/r I
iload 1
if_icmpeq L0
aload 0
iload 1
putfield android/support/design/widget/l/r I
aload 0
invokevirtual android/support/design/widget/l/a()V
L0:
return
.limit locals 2
.limit stack 2
.end method

.method final b(IIII)V
aload 0
getfield android/support/design/widget/l/p Landroid/graphics/Rect;
iload 1
iload 2
iload 3
iload 4
invokestatic android/support/design/widget/l/a(Landroid/graphics/Rect;IIII)Z
ifne L0
aload 0
getfield android/support/design/widget/l/p Landroid/graphics/Rect;
iload 1
iload 2
iload 3
iload 4
invokevirtual android/graphics/Rect/set(IIII)V
aload 0
iconst_1
putfield android/support/design/widget/l/H Z
aload 0
invokespecial android/support/design/widget/l/b()V
L0:
return
.limit locals 5
.limit stack 5
.end method

.method final c(I)V
aload 0
getfield android/support/design/widget/l/b I
iload 1
if_icmpeq L0
aload 0
iload 1
putfield android/support/design/widget/l/b I
aload 0
invokevirtual android/support/design/widget/l/a()V
L0:
return
.limit locals 2
.limit stack 2
.end method

.method final d(I)V
aload 0
getfield android/support/design/widget/l/c I
iload 1
if_icmpeq L0
aload 0
iload 1
putfield android/support/design/widget/l/c I
aload 0
invokevirtual android/support/design/widget/l/a()V
L0:
return
.limit locals 2
.limit stack 2
.end method

.method final e(I)V
aload 0
getfield android/support/design/widget/l/m Landroid/view/View;
invokevirtual android/view/View/getContext()Landroid/content/Context;
iload 1
getstatic android/support/design/n/TextAppearance [I
invokevirtual android/content/Context/obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;
astore 2
aload 2
getstatic android/support/design/n/TextAppearance_android_textColor I
invokevirtual android/content/res/TypedArray/hasValue(I)Z
ifeq L0
aload 0
aload 2
getstatic android/support/design/n/TextAppearance_android_textColor I
aload 0
getfield android/support/design/widget/l/f I
invokevirtual android/content/res/TypedArray/getColor(II)I
putfield android/support/design/widget/l/f I
L0:
aload 2
getstatic android/support/design/n/TextAppearance_android_textSize I
invokevirtual android/content/res/TypedArray/hasValue(I)Z
ifeq L1
aload 0
aload 2
getstatic android/support/design/n/TextAppearance_android_textSize I
aload 0
getfield android/support/design/widget/l/e F
f2i
invokevirtual android/content/res/TypedArray/getDimensionPixelSize(II)I
i2f
putfield android/support/design/widget/l/e F
L1:
aload 2
invokevirtual android/content/res/TypedArray/recycle()V
aload 0
invokevirtual android/support/design/widget/l/a()V
return
.limit locals 3
.limit stack 4
.end method

.method final f(I)V
aload 0
getfield android/support/design/widget/l/m Landroid/view/View;
invokevirtual android/view/View/getContext()Landroid/content/Context;
iload 1
getstatic android/support/design/n/TextAppearance [I
invokevirtual android/content/Context/obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;
astore 2
aload 2
getstatic android/support/design/n/TextAppearance_android_textColor I
invokevirtual android/content/res/TypedArray/hasValue(I)Z
ifeq L0
aload 0
aload 2
getstatic android/support/design/n/TextAppearance_android_textColor I
aload 0
getfield android/support/design/widget/l/r I
invokevirtual android/content/res/TypedArray/getColor(II)I
putfield android/support/design/widget/l/r I
L0:
aload 2
getstatic android/support/design/n/TextAppearance_android_textSize I
invokevirtual android/content/res/TypedArray/hasValue(I)Z
ifeq L1
aload 0
aload 2
getstatic android/support/design/n/TextAppearance_android_textSize I
aload 0
getfield android/support/design/widget/l/d F
f2i
invokevirtual android/content/res/TypedArray/getDimensionPixelSize(II)I
i2f
putfield android/support/design/widget/l/d F
L1:
aload 2
invokevirtual android/content/res/TypedArray/recycle()V
aload 0
invokevirtual android/support/design/widget/l/a()V
return
.limit locals 3
.limit stack 4
.end method
