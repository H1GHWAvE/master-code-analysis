.bytecode 50.0
.class final synchronized android/support/design/widget/cw
.super android/support/design/widget/cr

.field final 'a' Landroid/animation/ValueAnimator;

.method <init>()V
aload 0
invokespecial android/support/design/widget/cr/<init>()V
aload 0
new android/animation/ValueAnimator
dup
invokespecial android/animation/ValueAnimator/<init>()V
putfield android/support/design/widget/cw/a Landroid/animation/ValueAnimator;
return
.limit locals 1
.limit stack 3
.end method

.method public final a()V
aload 0
getfield android/support/design/widget/cw/a Landroid/animation/ValueAnimator;
invokevirtual android/animation/ValueAnimator/start()V
return
.limit locals 1
.limit stack 1
.end method

.method public final a(FF)V
aload 0
getfield android/support/design/widget/cw/a Landroid/animation/ValueAnimator;
iconst_2
newarray float
dup
iconst_0
fload 1
fastore
dup
iconst_1
fload 2
fastore
invokevirtual android/animation/ValueAnimator/setFloatValues([F)V
return
.limit locals 3
.limit stack 5
.end method

.method public final a(I)V
aload 0
getfield android/support/design/widget/cw/a Landroid/animation/ValueAnimator;
iload 1
i2l
invokevirtual android/animation/ValueAnimator/setDuration(J)Landroid/animation/ValueAnimator;
pop
return
.limit locals 2
.limit stack 3
.end method

.method public final a(II)V
aload 0
getfield android/support/design/widget/cw/a Landroid/animation/ValueAnimator;
iconst_2
newarray int
dup
iconst_0
iload 1
iastore
dup
iconst_1
iload 2
iastore
invokevirtual android/animation/ValueAnimator/setIntValues([I)V
return
.limit locals 3
.limit stack 5
.end method

.method public final a(Landroid/support/design/widget/cs;)V
aload 0
getfield android/support/design/widget/cw/a Landroid/animation/ValueAnimator;
new android/support/design/widget/cy
dup
aload 0
aload 1
invokespecial android/support/design/widget/cy/<init>(Landroid/support/design/widget/cw;Landroid/support/design/widget/cs;)V
invokevirtual android/animation/ValueAnimator/addListener(Landroid/animation/Animator$AnimatorListener;)V
return
.limit locals 2
.limit stack 5
.end method

.method public final a(Landroid/support/design/widget/ct;)V
aload 0
getfield android/support/design/widget/cw/a Landroid/animation/ValueAnimator;
new android/support/design/widget/cx
dup
aload 0
aload 1
invokespecial android/support/design/widget/cx/<init>(Landroid/support/design/widget/cw;Landroid/support/design/widget/ct;)V
invokevirtual android/animation/ValueAnimator/addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V
return
.limit locals 2
.limit stack 5
.end method

.method public final a(Landroid/view/animation/Interpolator;)V
aload 0
getfield android/support/design/widget/cw/a Landroid/animation/ValueAnimator;
aload 1
invokevirtual android/animation/ValueAnimator/setInterpolator(Landroid/animation/TimeInterpolator;)V
return
.limit locals 2
.limit stack 2
.end method

.method public final b()Z
aload 0
getfield android/support/design/widget/cw/a Landroid/animation/ValueAnimator;
invokevirtual android/animation/ValueAnimator/isRunning()Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final c()I
aload 0
getfield android/support/design/widget/cw/a Landroid/animation/ValueAnimator;
invokevirtual android/animation/ValueAnimator/getAnimatedValue()Ljava/lang/Object;
checkcast java/lang/Integer
invokevirtual java/lang/Integer/intValue()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final d()F
aload 0
getfield android/support/design/widget/cw/a Landroid/animation/ValueAnimator;
invokevirtual android/animation/ValueAnimator/getAnimatedValue()Ljava/lang/Object;
checkcast java/lang/Float
invokevirtual java/lang/Float/floatValue()F
freturn
.limit locals 1
.limit stack 1
.end method

.method public final e()V
aload 0
getfield android/support/design/widget/cw/a Landroid/animation/ValueAnimator;
invokevirtual android/animation/ValueAnimator/cancel()V
return
.limit locals 1
.limit stack 1
.end method

.method public final f()F
aload 0
getfield android/support/design/widget/cw/a Landroid/animation/ValueAnimator;
invokevirtual android/animation/ValueAnimator/getAnimatedFraction()F
freturn
.limit locals 1
.limit stack 1
.end method

.method public final g()V
aload 0
getfield android/support/design/widget/cw/a Landroid/animation/ValueAnimator;
invokevirtual android/animation/ValueAnimator/end()V
return
.limit locals 1
.limit stack 1
.end method
