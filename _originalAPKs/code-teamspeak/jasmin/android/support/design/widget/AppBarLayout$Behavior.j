.bytecode 50.0
.class public synchronized android/support/design/widget/AppBarLayout$Behavior
.super android/support/design/widget/df

.field private static final 'a' I = -1


.field private static final 'b' I = -1


.field private 'c' I

.field private 'd' Z

.field private 'e' Ljava/lang/Runnable;

.field private 'f' Landroid/support/v4/widget/ca;

.field private 'g' Landroid/support/design/widget/ck;

.field private 'h' I

.field private 'i' Z

.field private 'j' F

.field private 'k' Z

.field private 'l' I

.field private 'm' I

.field private 'n' I

.field private 'o' Ljava/lang/ref/WeakReference;

.method public <init>()V
aload 0
invokespecial android/support/design/widget/df/<init>()V
aload 0
iconst_m1
putfield android/support/design/widget/AppBarLayout$Behavior/h I
aload 0
iconst_m1
putfield android/support/design/widget/AppBarLayout$Behavior/l I
aload 0
iconst_m1
putfield android/support/design/widget/AppBarLayout$Behavior/n I
return
.limit locals 1
.limit stack 2
.end method

.method public <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
aload 0
aload 1
aload 2
invokespecial android/support/design/widget/df/<init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
aload 0
iconst_m1
putfield android/support/design/widget/AppBarLayout$Behavior/h I
aload 0
iconst_m1
putfield android/support/design/widget/AppBarLayout$Behavior/l I
aload 0
iconst_m1
putfield android/support/design/widget/AppBarLayout$Behavior/n I
return
.limit locals 3
.limit stack 3
.end method

.method private static a(Landroid/support/design/widget/AppBarLayout;I)I
iload 1
invokestatic java/lang/Math/abs(I)I
istore 5
aload 0
invokevirtual android/support/design/widget/AppBarLayout/getChildCount()I
istore 6
iconst_0
istore 3
L0:
iload 1
istore 4
iload 3
iload 6
if_icmpge L1
aload 0
iload 3
invokevirtual android/support/design/widget/AppBarLayout/getChildAt(I)Landroid/view/View;
astore 7
aload 7
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/design/widget/g
astore 8
aload 8
getfield android/support/design/widget/g/g Landroid/view/animation/Interpolator;
astore 9
iload 5
aload 7
invokevirtual android/view/View/getTop()I
if_icmplt L2
iload 5
aload 7
invokevirtual android/view/View/getBottom()I
if_icmpgt L2
iload 1
istore 4
aload 9
ifnull L1
aload 8
getfield android/support/design/widget/g/f I
istore 6
iload 6
iconst_1
iand
ifeq L3
aload 7
invokevirtual android/view/View/getHeight()I
istore 3
aload 8
getfield android/support/design/widget/g/topMargin I
istore 4
aload 8
getfield android/support/design/widget/g/bottomMargin I
iload 3
iload 4
iadd
iadd
iconst_0
iadd
istore 4
iload 4
istore 3
iload 6
iconst_2
iand
ifeq L4
iload 4
aload 7
invokestatic android/support/v4/view/cx/o(Landroid/view/View;)I
isub
istore 3
L4:
iload 1
istore 4
iload 3
ifle L1
aload 7
invokevirtual android/view/View/getTop()I
istore 4
iload 3
i2f
fstore 2
aload 9
iload 5
iload 4
isub
i2f
iload 3
i2f
fdiv
invokeinterface android/view/animation/Interpolator/getInterpolation(F)F 1
fload 2
fmul
invokestatic java/lang/Math/round(F)I
istore 3
iload 1
invokestatic java/lang/Integer/signum(I)I
iload 3
aload 7
invokevirtual android/view/View/getTop()I
iadd
imul
istore 4
L1:
iload 4
ireturn
L2:
iload 3
iconst_1
iadd
istore 3
goto L0
L3:
iconst_0
istore 3
goto L4
.limit locals 10
.limit stack 3
.end method

.method private a(Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/AppBarLayout;III)I
aload 0
aload 1
aload 2
aload 0
invokevirtual android/support/design/widget/AppBarLayout$Behavior/a()I
iload 3
isub
iload 4
iload 5
invokespecial android/support/design/widget/AppBarLayout$Behavior/b(Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/AppBarLayout;III)I
ireturn
.limit locals 6
.limit stack 6
.end method

.method private a(Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/AppBarLayout;)Landroid/os/Parcelable;
iconst_0
istore 7
aload 0
aload 1
aload 2
invokespecial android/support/design/widget/df/a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;)Landroid/os/Parcelable;
astore 8
aload 0
invokespecial android/support/design/widget/df/c()I
istore 4
aload 2
invokevirtual android/support/design/widget/AppBarLayout/getChildCount()I
istore 5
iconst_0
istore 3
L0:
iload 3
iload 5
if_icmpge L1
aload 2
iload 3
invokevirtual android/support/design/widget/AppBarLayout/getChildAt(I)Landroid/view/View;
astore 1
aload 1
invokevirtual android/view/View/getBottom()I
iload 4
iadd
istore 6
aload 1
invokevirtual android/view/View/getTop()I
iload 4
iadd
ifgt L2
iload 6
iflt L2
new android/support/design/widget/AppBarLayout$Behavior$SavedState
dup
aload 8
invokespecial android/support/design/widget/AppBarLayout$Behavior$SavedState/<init>(Landroid/os/Parcelable;)V
astore 2
aload 2
iload 3
putfield android/support/design/widget/AppBarLayout$Behavior$SavedState/a I
iload 6
aload 1
invokestatic android/support/v4/view/cx/o(Landroid/view/View;)I
if_icmpne L3
iconst_1
istore 7
L3:
aload 2
iload 7
putfield android/support/design/widget/AppBarLayout$Behavior$SavedState/c Z
aload 2
iload 6
i2f
aload 1
invokevirtual android/view/View/getHeight()I
i2f
fdiv
putfield android/support/design/widget/AppBarLayout$Behavior$SavedState/b F
aload 2
areturn
L2:
iload 3
iconst_1
iadd
istore 3
goto L0
L1:
aload 8
areturn
.limit locals 9
.limit stack 3
.end method

.method static synthetic a(Landroid/support/design/widget/AppBarLayout$Behavior;)Landroid/support/v4/widget/ca;
aload 0
getfield android/support/design/widget/AppBarLayout$Behavior/f Landroid/support/v4/widget/ca;
areturn
.limit locals 1
.limit stack 1
.end method

.method private a(Landroid/support/design/widget/AppBarLayout;)V
aload 1
invokestatic android/support/design/widget/AppBarLayout/a(Landroid/support/design/widget/AppBarLayout;)Ljava/util/List;
astore 4
aload 4
invokeinterface java/util/List/size()I 0
istore 3
iconst_0
istore 2
L0:
iload 2
iload 3
if_icmpge L1
aload 4
iload 2
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast android/support/design/widget/i
astore 5
aload 5
ifnull L2
aload 5
aload 1
aload 0
invokespecial android/support/design/widget/df/c()I
invokeinterface android/support/design/widget/i/a(Landroid/support/design/widget/AppBarLayout;I)V 2
L2:
iload 2
iconst_1
iadd
istore 2
goto L0
L1:
return
.limit locals 6
.limit stack 3
.end method

.method private a(Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/AppBarLayout;I[I)V
iload 3
ifeq L0
aload 0
getfield android/support/design/widget/AppBarLayout$Behavior/d Z
ifne L0
iload 3
ifge L1
aload 2
invokevirtual android/support/design/widget/AppBarLayout/getTotalScrollRange()I
ineg
istore 5
iload 5
aload 2
invokevirtual android/support/design/widget/AppBarLayout/getDownNestedPreScrollRange()I
iadd
istore 6
L2:
aload 4
iconst_1
aload 0
aload 1
aload 2
iload 3
iload 5
iload 6
invokespecial android/support/design/widget/AppBarLayout$Behavior/a(Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/AppBarLayout;III)I
iastore
L0:
return
L1:
aload 2
invokevirtual android/support/design/widget/AppBarLayout/getUpNestedPreScrollRange()I
ineg
istore 5
iconst_0
istore 6
goto L2
.limit locals 7
.limit stack 8
.end method

.method private a(Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/AppBarLayout;Landroid/os/Parcelable;)V
aload 3
instanceof android/support/design/widget/AppBarLayout$Behavior$SavedState
ifeq L0
aload 3
checkcast android/support/design/widget/AppBarLayout$Behavior$SavedState
astore 3
aload 0
aload 1
aload 2
aload 3
invokevirtual android/support/design/widget/AppBarLayout$Behavior$SavedState/getSuperState()Landroid/os/Parcelable;
invokespecial android/support/design/widget/df/a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/os/Parcelable;)V
aload 0
aload 3
getfield android/support/design/widget/AppBarLayout$Behavior$SavedState/a I
putfield android/support/design/widget/AppBarLayout$Behavior/h I
aload 0
aload 3
getfield android/support/design/widget/AppBarLayout$Behavior$SavedState/b F
putfield android/support/design/widget/AppBarLayout$Behavior/j F
aload 0
aload 3
getfield android/support/design/widget/AppBarLayout$Behavior$SavedState/c Z
putfield android/support/design/widget/AppBarLayout$Behavior/i Z
return
L0:
aload 0
aload 1
aload 2
aload 3
invokespecial android/support/design/widget/df/a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/os/Parcelable;)V
aload 0
iconst_m1
putfield android/support/design/widget/AppBarLayout$Behavior/h I
return
.limit locals 4
.limit stack 4
.end method

.method private a(Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/AppBarLayout;FZ)Z
iconst_0
istore 7
iload 4
ifne L0
aload 2
invokevirtual android/support/design/widget/AppBarLayout/getTotalScrollRange()I
ineg
istore 5
fload 3
fneg
fstore 3
aload 0
getfield android/support/design/widget/AppBarLayout$Behavior/e Ljava/lang/Runnable;
ifnull L1
aload 2
aload 0
getfield android/support/design/widget/AppBarLayout$Behavior/e Ljava/lang/Runnable;
invokevirtual android/support/design/widget/AppBarLayout/removeCallbacks(Ljava/lang/Runnable;)Z
pop
L1:
aload 0
getfield android/support/design/widget/AppBarLayout$Behavior/f Landroid/support/v4/widget/ca;
ifnonnull L2
aload 0
aload 2
invokevirtual android/support/design/widget/AppBarLayout/getContext()Landroid/content/Context;
aconst_null
invokestatic android/support/v4/widget/ca/a(Landroid/content/Context;Landroid/view/animation/Interpolator;)Landroid/support/v4/widget/ca;
putfield android/support/design/widget/AppBarLayout$Behavior/f Landroid/support/v4/widget/ca;
L2:
aload 0
getfield android/support/design/widget/AppBarLayout$Behavior/f Landroid/support/v4/widget/ca;
iconst_0
aload 0
invokevirtual android/support/design/widget/AppBarLayout$Behavior/a()I
iconst_0
fload 3
invokestatic java/lang/Math/round(F)I
iconst_0
iconst_0
iload 5
iconst_0
invokevirtual android/support/v4/widget/ca/a(IIIIIIII)V
aload 0
getfield android/support/design/widget/AppBarLayout$Behavior/f Landroid/support/v4/widget/ca;
invokevirtual android/support/v4/widget/ca/f()Z
ifeq L3
aload 0
new android/support/design/widget/e
dup
aload 0
aload 1
aload 2
invokespecial android/support/design/widget/e/<init>(Landroid/support/design/widget/AppBarLayout$Behavior;Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/AppBarLayout;)V
putfield android/support/design/widget/AppBarLayout$Behavior/e Ljava/lang/Runnable;
aload 2
aload 0
getfield android/support/design/widget/AppBarLayout$Behavior/e Ljava/lang/Runnable;
invokestatic android/support/v4/view/cx/a(Landroid/view/View;Ljava/lang/Runnable;)V
iconst_1
istore 4
L4:
iload 4
ireturn
L3:
aload 0
aconst_null
putfield android/support/design/widget/AppBarLayout$Behavior/e Ljava/lang/Runnable;
iconst_0
ireturn
L0:
fload 3
fconst_0
fcmpg
ifge L5
aload 2
invokevirtual android/support/design/widget/AppBarLayout/getTotalScrollRange()I
ineg
aload 2
invokevirtual android/support/design/widget/AppBarLayout/getDownNestedPreScrollRange()I
iadd
istore 5
iload 7
istore 4
aload 0
invokevirtual android/support/design/widget/AppBarLayout$Behavior/a()I
iload 5
if_icmpgt L4
L6:
iload 7
istore 4
aload 0
invokevirtual android/support/design/widget/AppBarLayout$Behavior/a()I
iload 5
if_icmpeq L4
aload 0
aload 1
aload 2
iload 5
invokespecial android/support/design/widget/AppBarLayout$Behavior/c(Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/AppBarLayout;I)V
iconst_1
ireturn
L5:
aload 2
invokevirtual android/support/design/widget/AppBarLayout/getUpNestedPreScrollRange()I
ineg
istore 6
iload 6
istore 5
aload 0
invokevirtual android/support/design/widget/AppBarLayout$Behavior/a()I
iload 6
if_icmpge L6
iconst_0
ireturn
.limit locals 8
.limit stack 9
.end method

.method private a(Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/AppBarLayout;IF)Z
aload 0
getfield android/support/design/widget/AppBarLayout$Behavior/e Ljava/lang/Runnable;
ifnull L0
aload 2
aload 0
getfield android/support/design/widget/AppBarLayout$Behavior/e Ljava/lang/Runnable;
invokevirtual android/support/design/widget/AppBarLayout/removeCallbacks(Ljava/lang/Runnable;)Z
pop
L0:
aload 0
getfield android/support/design/widget/AppBarLayout$Behavior/f Landroid/support/v4/widget/ca;
ifnonnull L1
aload 0
aload 2
invokevirtual android/support/design/widget/AppBarLayout/getContext()Landroid/content/Context;
aconst_null
invokestatic android/support/v4/widget/ca/a(Landroid/content/Context;Landroid/view/animation/Interpolator;)Landroid/support/v4/widget/ca;
putfield android/support/design/widget/AppBarLayout$Behavior/f Landroid/support/v4/widget/ca;
L1:
aload 0
getfield android/support/design/widget/AppBarLayout$Behavior/f Landroid/support/v4/widget/ca;
iconst_0
aload 0
invokevirtual android/support/design/widget/AppBarLayout$Behavior/a()I
iconst_0
fload 4
invokestatic java/lang/Math/round(F)I
iconst_0
iconst_0
iload 3
iconst_0
invokevirtual android/support/v4/widget/ca/a(IIIIIIII)V
aload 0
getfield android/support/design/widget/AppBarLayout$Behavior/f Landroid/support/v4/widget/ca;
invokevirtual android/support/v4/widget/ca/f()Z
ifeq L2
aload 0
new android/support/design/widget/e
dup
aload 0
aload 1
aload 2
invokespecial android/support/design/widget/e/<init>(Landroid/support/design/widget/AppBarLayout$Behavior;Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/AppBarLayout;)V
putfield android/support/design/widget/AppBarLayout$Behavior/e Ljava/lang/Runnable;
aload 2
aload 0
getfield android/support/design/widget/AppBarLayout$Behavior/e Ljava/lang/Runnable;
invokestatic android/support/v4/view/cx/a(Landroid/view/View;Ljava/lang/Runnable;)V
iconst_1
ireturn
L2:
aload 0
aconst_null
putfield android/support/design/widget/AppBarLayout$Behavior/e Ljava/lang/Runnable;
iconst_0
ireturn
.limit locals 5
.limit stack 9
.end method

.method private a(Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/AppBarLayout;Landroid/view/MotionEvent;)Z
aload 0
getfield android/support/design/widget/AppBarLayout$Behavior/n I
ifge L0
aload 0
aload 1
invokevirtual android/support/design/widget/CoordinatorLayout/getContext()Landroid/content/Context;
invokestatic android/view/ViewConfiguration/get(Landroid/content/Context;)Landroid/view/ViewConfiguration;
invokevirtual android/view/ViewConfiguration/getScaledTouchSlop()I
putfield android/support/design/widget/AppBarLayout$Behavior/n I
L0:
aload 3
invokevirtual android/view/MotionEvent/getAction()I
iconst_2
if_icmpne L1
aload 0
getfield android/support/design/widget/AppBarLayout$Behavior/k Z
ifeq L1
iconst_1
ireturn
L1:
aload 3
invokestatic android/support/v4/view/bk/a(Landroid/view/MotionEvent;)I
tableswitch 0
L2
L3
L4
L3
default : L5
L5:
aload 0
getfield android/support/design/widget/AppBarLayout$Behavior/k Z
ireturn
L4:
aload 0
getfield android/support/design/widget/AppBarLayout$Behavior/l I
istore 4
iload 4
iconst_m1
if_icmpeq L5
aload 3
iload 4
invokestatic android/support/v4/view/bk/a(Landroid/view/MotionEvent;I)I
istore 4
iload 4
iconst_m1
if_icmpeq L5
aload 3
iload 4
invokestatic android/support/v4/view/bk/d(Landroid/view/MotionEvent;I)F
f2i
istore 4
iload 4
aload 0
getfield android/support/design/widget/AppBarLayout$Behavior/m I
isub
invokestatic java/lang/Math/abs(I)I
aload 0
getfield android/support/design/widget/AppBarLayout$Behavior/n I
if_icmple L5
aload 0
iconst_1
putfield android/support/design/widget/AppBarLayout$Behavior/k Z
aload 0
iload 4
putfield android/support/design/widget/AppBarLayout$Behavior/m I
goto L5
L2:
aload 0
iconst_0
putfield android/support/design/widget/AppBarLayout$Behavior/k Z
aload 3
invokevirtual android/view/MotionEvent/getX()F
f2i
istore 4
aload 3
invokevirtual android/view/MotionEvent/getY()F
f2i
istore 5
aload 1
aload 2
iload 4
iload 5
invokevirtual android/support/design/widget/CoordinatorLayout/a(Landroid/view/View;II)Z
ifeq L5
aload 0
invokespecial android/support/design/widget/AppBarLayout$Behavior/d()Z
ifeq L5
aload 0
iload 5
putfield android/support/design/widget/AppBarLayout$Behavior/m I
aload 0
aload 3
iconst_0
invokestatic android/support/v4/view/bk/b(Landroid/view/MotionEvent;I)I
putfield android/support/design/widget/AppBarLayout$Behavior/l I
goto L5
L3:
aload 0
iconst_0
putfield android/support/design/widget/AppBarLayout$Behavior/k Z
aload 0
iconst_m1
putfield android/support/design/widget/AppBarLayout$Behavior/l I
goto L5
.limit locals 6
.limit stack 4
.end method

.method private a(Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/AppBarLayout;Landroid/view/View;I)Z
iconst_1
istore 5
iload 4
iconst_2
iand
ifeq L0
aload 2
invokevirtual android/support/design/widget/AppBarLayout/getTotalScrollRange()I
ifeq L1
iconst_1
istore 4
L2:
iload 4
ifeq L0
aload 1
invokevirtual android/support/design/widget/CoordinatorLayout/getHeight()I
aload 3
invokevirtual android/view/View/getHeight()I
isub
aload 2
invokevirtual android/support/design/widget/AppBarLayout/getHeight()I
if_icmpgt L0
L3:
iload 5
ifeq L4
aload 0
getfield android/support/design/widget/AppBarLayout$Behavior/g Landroid/support/design/widget/ck;
ifnull L4
aload 0
getfield android/support/design/widget/AppBarLayout$Behavior/g Landroid/support/design/widget/ck;
getfield android/support/design/widget/ck/a Landroid/support/design/widget/cr;
invokevirtual android/support/design/widget/cr/e()V
L4:
aload 0
aconst_null
putfield android/support/design/widget/AppBarLayout$Behavior/o Ljava/lang/ref/WeakReference;
iload 5
ireturn
L1:
iconst_0
istore 4
goto L2
L0:
iconst_0
istore 5
goto L3
.limit locals 6
.limit stack 2
.end method

.method private b(Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/AppBarLayout;III)I
iconst_0
istore 7
aload 0
invokevirtual android/support/design/widget/AppBarLayout$Behavior/a()I
istore 8
iload 4
ifeq L0
iload 8
iload 4
if_icmplt L0
iload 8
iload 5
if_icmpgt L0
iload 3
iload 4
iload 5
invokestatic android/support/design/widget/an/a(III)I
istore 4
iload 8
iload 4
if_icmpeq L0
aload 2
getfield android/support/design/widget/AppBarLayout/a Z
ifeq L1
iload 4
invokestatic java/lang/Math/abs(I)I
istore 9
aload 2
invokevirtual android/support/design/widget/AppBarLayout/getChildCount()I
istore 5
iconst_0
istore 3
L2:
iload 3
iload 5
if_icmpge L3
aload 2
iload 3
invokevirtual android/support/design/widget/AppBarLayout/getChildAt(I)Landroid/view/View;
astore 12
aload 12
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/design/widget/g
astore 13
aload 13
getfield android/support/design/widget/g/g Landroid/view/animation/Interpolator;
astore 14
iload 9
aload 12
invokevirtual android/view/View/getTop()I
if_icmplt L4
iload 9
aload 12
invokevirtual android/view/View/getBottom()I
if_icmpgt L4
aload 14
ifnull L3
aload 13
getfield android/support/design/widget/g/f I
istore 10
iload 10
iconst_1
iand
ifeq L5
aload 12
invokevirtual android/view/View/getHeight()I
istore 3
aload 13
getfield android/support/design/widget/g/topMargin I
istore 5
aload 13
getfield android/support/design/widget/g/bottomMargin I
iload 3
iload 5
iadd
iadd
iconst_0
iadd
istore 5
iload 5
istore 3
iload 10
iconst_2
iand
ifeq L6
iload 5
aload 12
invokestatic android/support/v4/view/cx/o(Landroid/view/View;)I
isub
istore 3
L6:
iload 3
ifle L3
aload 12
invokevirtual android/view/View/getTop()I
istore 5
iload 3
i2f
fstore 6
aload 14
iload 9
iload 5
isub
i2f
iload 3
i2f
fdiv
invokeinterface android/view/animation/Interpolator/getInterpolation(F)F 1
fload 6
fmul
invokestatic java/lang/Math/round(F)I
istore 3
iload 4
invokestatic java/lang/Integer/signum(I)I
istore 5
iload 3
aload 12
invokevirtual android/view/View/getTop()I
iadd
iload 5
imul
istore 3
L7:
aload 0
iload 3
invokespecial android/support/design/widget/df/b(I)Z
istore 11
aload 0
iload 4
iload 3
isub
putfield android/support/design/widget/AppBarLayout$Behavior/c I
iload 11
ifne L8
aload 2
getfield android/support/design/widget/AppBarLayout/a Z
ifeq L8
aload 1
getfield android/support/design/widget/CoordinatorLayout/h Ljava/util/List;
invokeinterface java/util/List/size()I 0
istore 9
iconst_0
istore 5
iload 7
istore 3
L9:
iload 5
iload 9
if_icmpge L8
aload 1
getfield android/support/design/widget/CoordinatorLayout/h Ljava/util/List;
iload 5
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast android/view/View
astore 12
aload 12
aload 2
if_acmpne L10
iconst_1
istore 3
L11:
iload 5
iconst_1
iadd
istore 5
goto L9
L4:
iload 3
iconst_1
iadd
istore 3
goto L2
L3:
iload 4
istore 3
goto L7
L1:
iload 4
istore 3
goto L7
L10:
iload 3
ifeq L12
aload 12
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/design/widget/w
astore 13
aload 13
getfield android/support/design/widget/w/a Landroid/support/design/widget/t;
astore 14
aload 14
ifnull L12
aload 13
aload 2
invokevirtual android/support/design/widget/w/a(Landroid/view/View;)Z
ifeq L12
aload 14
aload 1
aload 12
aload 2
invokevirtual android/support/design/widget/t/a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/view/View;)Z
pop
L12:
goto L11
L8:
aload 0
aload 2
invokespecial android/support/design/widget/AppBarLayout$Behavior/a(Landroid/support/design/widget/AppBarLayout;)V
iload 8
iload 4
isub
ireturn
L5:
iconst_0
istore 3
goto L6
L0:
iconst_0
ireturn
.limit locals 15
.limit stack 4
.end method

.method private b(Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/AppBarLayout;I)V
iload 3
ifge L0
aload 0
aload 1
aload 2
iload 3
aload 2
invokevirtual android/support/design/widget/AppBarLayout/getDownNestedScrollRange()I
ineg
iconst_0
invokespecial android/support/design/widget/AppBarLayout$Behavior/a(Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/AppBarLayout;III)I
pop
aload 0
iconst_1
putfield android/support/design/widget/AppBarLayout$Behavior/d Z
return
L0:
aload 0
iconst_0
putfield android/support/design/widget/AppBarLayout$Behavior/d Z
return
.limit locals 4
.limit stack 6
.end method

.method private b(Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/AppBarLayout;Landroid/view/MotionEvent;)Z
iconst_0
istore 8
aload 0
getfield android/support/design/widget/AppBarLayout$Behavior/n I
ifge L0
aload 0
aload 1
invokevirtual android/support/design/widget/CoordinatorLayout/getContext()Landroid/content/Context;
invokestatic android/view/ViewConfiguration/get(Landroid/content/Context;)Landroid/view/ViewConfiguration;
invokevirtual android/view/ViewConfiguration/getScaledTouchSlop()I
putfield android/support/design/widget/AppBarLayout$Behavior/n I
L0:
aload 3
invokevirtual android/view/MotionEvent/getX()F
f2i
istore 4
aload 3
invokevirtual android/view/MotionEvent/getY()F
f2i
istore 5
aload 3
invokestatic android/support/v4/view/bk/a(Landroid/view/MotionEvent;)I
tableswitch 0
L1
L2
L3
L2
default : L4
L4:
iconst_1
istore 7
L5:
iload 7
ireturn
L1:
iload 8
istore 7
aload 1
aload 2
iload 4
iload 5
invokevirtual android/support/design/widget/CoordinatorLayout/a(Landroid/view/View;II)Z
ifeq L5
iload 8
istore 7
aload 0
invokespecial android/support/design/widget/AppBarLayout$Behavior/d()Z
ifeq L5
aload 0
iload 5
putfield android/support/design/widget/AppBarLayout$Behavior/m I
aload 0
aload 3
iconst_0
invokestatic android/support/v4/view/bk/b(Landroid/view/MotionEvent;I)I
putfield android/support/design/widget/AppBarLayout$Behavior/l I
goto L4
L3:
aload 3
aload 0
getfield android/support/design/widget/AppBarLayout$Behavior/l I
invokestatic android/support/v4/view/bk/a(Landroid/view/MotionEvent;I)I
istore 4
iload 8
istore 7
iload 4
iconst_m1
if_icmpeq L5
aload 3
iload 4
invokestatic android/support/v4/view/bk/d(Landroid/view/MotionEvent;I)F
f2i
istore 6
aload 0
getfield android/support/design/widget/AppBarLayout$Behavior/m I
iload 6
isub
istore 5
iload 5
istore 4
aload 0
getfield android/support/design/widget/AppBarLayout$Behavior/k Z
ifne L6
iload 5
istore 4
iload 5
invokestatic java/lang/Math/abs(I)I
aload 0
getfield android/support/design/widget/AppBarLayout$Behavior/n I
if_icmple L6
aload 0
iconst_1
putfield android/support/design/widget/AppBarLayout$Behavior/k Z
iload 5
ifle L7
iload 5
aload 0
getfield android/support/design/widget/AppBarLayout$Behavior/n I
isub
istore 4
L6:
aload 0
getfield android/support/design/widget/AppBarLayout$Behavior/k Z
ifeq L4
aload 0
iload 6
putfield android/support/design/widget/AppBarLayout$Behavior/m I
aload 0
aload 1
aload 2
iload 4
aload 2
invokevirtual android/support/design/widget/AppBarLayout/getDownNestedScrollRange()I
ineg
iconst_0
invokespecial android/support/design/widget/AppBarLayout$Behavior/a(Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/AppBarLayout;III)I
pop
goto L4
L7:
iload 5
aload 0
getfield android/support/design/widget/AppBarLayout$Behavior/n I
iadd
istore 4
goto L6
L2:
aload 0
iconst_0
putfield android/support/design/widget/AppBarLayout$Behavior/k Z
aload 0
iconst_m1
putfield android/support/design/widget/AppBarLayout$Behavior/l I
goto L4
.limit locals 9
.limit stack 6
.end method

.method private c(Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/AppBarLayout;I)V
aload 0
getfield android/support/design/widget/AppBarLayout$Behavior/g Landroid/support/design/widget/ck;
ifnonnull L0
aload 0
invokestatic android/support/design/widget/dh/a()Landroid/support/design/widget/ck;
putfield android/support/design/widget/AppBarLayout$Behavior/g Landroid/support/design/widget/ck;
aload 0
getfield android/support/design/widget/AppBarLayout$Behavior/g Landroid/support/design/widget/ck;
getstatic android/support/design/widget/a/c Landroid/view/animation/Interpolator;
invokevirtual android/support/design/widget/ck/a(Landroid/view/animation/Interpolator;)V
aload 0
getfield android/support/design/widget/AppBarLayout$Behavior/g Landroid/support/design/widget/ck;
new android/support/design/widget/d
dup
aload 0
aload 1
aload 2
invokespecial android/support/design/widget/d/<init>(Landroid/support/design/widget/AppBarLayout$Behavior;Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/AppBarLayout;)V
invokevirtual android/support/design/widget/ck/a(Landroid/support/design/widget/cp;)V
L1:
aload 0
getfield android/support/design/widget/AppBarLayout$Behavior/g Landroid/support/design/widget/ck;
aload 0
invokevirtual android/support/design/widget/AppBarLayout$Behavior/a()I
iload 3
invokevirtual android/support/design/widget/ck/a(II)V
aload 0
getfield android/support/design/widget/AppBarLayout$Behavior/g Landroid/support/design/widget/ck;
getfield android/support/design/widget/ck/a Landroid/support/design/widget/cr;
invokevirtual android/support/design/widget/cr/a()V
return
L0:
aload 0
getfield android/support/design/widget/AppBarLayout$Behavior/g Landroid/support/design/widget/ck;
getfield android/support/design/widget/ck/a Landroid/support/design/widget/cr;
invokevirtual android/support/design/widget/cr/e()V
goto L1
.limit locals 4
.limit stack 6
.end method

.method private c(Landroid/view/View;)V
aload 0
iconst_0
putfield android/support/design/widget/AppBarLayout$Behavior/d Z
aload 0
new java/lang/ref/WeakReference
dup
aload 1
invokespecial java/lang/ref/WeakReference/<init>(Ljava/lang/Object;)V
putfield android/support/design/widget/AppBarLayout$Behavior/o Ljava/lang/ref/WeakReference;
return
.limit locals 2
.limit stack 4
.end method

.method private d()Z
aload 0
getfield android/support/design/widget/AppBarLayout$Behavior/o Ljava/lang/ref/WeakReference;
ifnull L0
aload 0
getfield android/support/design/widget/AppBarLayout$Behavior/o Ljava/lang/ref/WeakReference;
invokevirtual java/lang/ref/WeakReference/get()Ljava/lang/Object;
checkcast android/view/View
astore 1
aload 1
ifnull L1
aload 1
invokevirtual android/view/View/isShown()Z
ifeq L1
aload 1
iconst_m1
invokestatic android/support/v4/view/cx/b(Landroid/view/View;I)Z
ifne L1
iconst_1
ireturn
L1:
iconst_0
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method private d(Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/AppBarLayout;I)Z
aload 0
aload 1
aload 2
iload 3
invokespecial android/support/design/widget/df/a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;I)Z
istore 5
aload 2
invokevirtual android/support/design/widget/AppBarLayout/getPendingAction()I
istore 4
iload 4
ifeq L0
iload 4
iconst_4
iand
ifeq L1
iconst_1
istore 3
L2:
iload 4
iconst_2
iand
ifeq L3
aload 2
invokevirtual android/support/design/widget/AppBarLayout/getUpNestedPreScrollRange()I
ineg
istore 4
iload 3
ifeq L4
aload 0
aload 1
aload 2
iload 4
invokespecial android/support/design/widget/AppBarLayout$Behavior/c(Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/AppBarLayout;I)V
L5:
aload 2
iconst_0
putfield android/support/design/widget/AppBarLayout/b I
L6:
aload 0
aload 2
invokespecial android/support/design/widget/AppBarLayout$Behavior/a(Landroid/support/design/widget/AppBarLayout;)V
iload 5
ireturn
L1:
iconst_0
istore 3
goto L2
L4:
aload 0
aload 1
aload 2
iload 4
invokevirtual android/support/design/widget/AppBarLayout$Behavior/a(Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/AppBarLayout;I)I
pop
goto L5
L3:
iload 4
iconst_1
iand
ifeq L5
iload 3
ifeq L7
aload 0
aload 1
aload 2
iconst_0
invokespecial android/support/design/widget/AppBarLayout$Behavior/c(Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/AppBarLayout;I)V
goto L5
L7:
aload 0
aload 1
aload 2
iconst_0
invokevirtual android/support/design/widget/AppBarLayout$Behavior/a(Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/AppBarLayout;I)I
pop
goto L5
L0:
aload 0
getfield android/support/design/widget/AppBarLayout$Behavior/h I
iflt L6
aload 2
aload 0
getfield android/support/design/widget/AppBarLayout$Behavior/h I
invokevirtual android/support/design/widget/AppBarLayout/getChildAt(I)Landroid/view/View;
astore 1
aload 1
invokevirtual android/view/View/getBottom()I
ineg
istore 3
aload 0
getfield android/support/design/widget/AppBarLayout$Behavior/i Z
ifeq L8
aload 1
invokestatic android/support/v4/view/cx/o(Landroid/view/View;)I
iload 3
iadd
istore 3
L9:
aload 0
iload 3
invokespecial android/support/design/widget/df/b(I)Z
pop
aload 0
iconst_m1
putfield android/support/design/widget/AppBarLayout$Behavior/h I
goto L6
L8:
aload 1
invokevirtual android/view/View/getHeight()I
i2f
aload 0
getfield android/support/design/widget/AppBarLayout$Behavior/j F
fmul
invokestatic java/lang/Math/round(F)I
iload 3
iadd
istore 3
goto L9
.limit locals 6
.limit stack 4
.end method

.method final a()I
aload 0
invokespecial android/support/design/widget/df/c()I
aload 0
getfield android/support/design/widget/AppBarLayout$Behavior/c I
iadd
ireturn
.limit locals 1
.limit stack 2
.end method

.method final a(Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/AppBarLayout;I)I
aload 0
aload 1
aload 2
iload 3
ldc_w -2147483648
ldc_w 2147483647
invokespecial android/support/design/widget/AppBarLayout$Behavior/b(Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/AppBarLayout;III)I
ireturn
.limit locals 4
.limit stack 6
.end method

.method public final synthetic a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;)Landroid/os/Parcelable;
iconst_0
istore 7
aload 2
checkcast android/support/design/widget/AppBarLayout
astore 2
aload 0
aload 1
aload 2
invokespecial android/support/design/widget/df/a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;)Landroid/os/Parcelable;
astore 8
aload 0
invokespecial android/support/design/widget/df/c()I
istore 4
aload 2
invokevirtual android/support/design/widget/AppBarLayout/getChildCount()I
istore 5
iconst_0
istore 3
L0:
iload 3
iload 5
if_icmpge L1
aload 2
iload 3
invokevirtual android/support/design/widget/AppBarLayout/getChildAt(I)Landroid/view/View;
astore 1
aload 1
invokevirtual android/view/View/getBottom()I
iload 4
iadd
istore 6
aload 1
invokevirtual android/view/View/getTop()I
iload 4
iadd
ifgt L2
iload 6
iflt L2
new android/support/design/widget/AppBarLayout$Behavior$SavedState
dup
aload 8
invokespecial android/support/design/widget/AppBarLayout$Behavior$SavedState/<init>(Landroid/os/Parcelable;)V
astore 2
aload 2
iload 3
putfield android/support/design/widget/AppBarLayout$Behavior$SavedState/a I
iload 6
aload 1
invokestatic android/support/v4/view/cx/o(Landroid/view/View;)I
if_icmpne L3
iconst_1
istore 7
L3:
aload 2
iload 7
putfield android/support/design/widget/AppBarLayout$Behavior$SavedState/c Z
aload 2
iload 6
i2f
aload 1
invokevirtual android/view/View/getHeight()I
i2f
fdiv
putfield android/support/design/widget/AppBarLayout$Behavior$SavedState/b F
aload 2
areturn
L2:
iload 3
iconst_1
iadd
istore 3
goto L0
L1:
aload 8
areturn
.limit locals 9
.limit stack 3
.end method

.method public final synthetic a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;I[I)V
aload 2
checkcast android/support/design/widget/AppBarLayout
astore 2
iload 3
ifeq L0
aload 0
getfield android/support/design/widget/AppBarLayout$Behavior/d Z
ifne L0
iload 3
ifge L1
aload 2
invokevirtual android/support/design/widget/AppBarLayout/getTotalScrollRange()I
ineg
istore 5
iload 5
aload 2
invokevirtual android/support/design/widget/AppBarLayout/getDownNestedPreScrollRange()I
iadd
istore 6
L2:
aload 4
iconst_1
aload 0
aload 1
aload 2
iload 3
iload 5
iload 6
invokespecial android/support/design/widget/AppBarLayout$Behavior/a(Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/AppBarLayout;III)I
iastore
L0:
return
L1:
aload 2
invokevirtual android/support/design/widget/AppBarLayout/getUpNestedPreScrollRange()I
ineg
istore 5
iconst_0
istore 6
goto L2
.limit locals 7
.limit stack 8
.end method

.method public final synthetic a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/os/Parcelable;)V
aload 2
checkcast android/support/design/widget/AppBarLayout
astore 2
aload 3
instanceof android/support/design/widget/AppBarLayout$Behavior$SavedState
ifeq L0
aload 3
checkcast android/support/design/widget/AppBarLayout$Behavior$SavedState
astore 3
aload 0
aload 1
aload 2
aload 3
invokevirtual android/support/design/widget/AppBarLayout$Behavior$SavedState/getSuperState()Landroid/os/Parcelable;
invokespecial android/support/design/widget/df/a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/os/Parcelable;)V
aload 0
aload 3
getfield android/support/design/widget/AppBarLayout$Behavior$SavedState/a I
putfield android/support/design/widget/AppBarLayout$Behavior/h I
aload 0
aload 3
getfield android/support/design/widget/AppBarLayout$Behavior$SavedState/b F
putfield android/support/design/widget/AppBarLayout$Behavior/j F
aload 0
aload 3
getfield android/support/design/widget/AppBarLayout$Behavior$SavedState/c Z
putfield android/support/design/widget/AppBarLayout$Behavior/i Z
return
L0:
aload 0
aload 1
aload 2
aload 3
invokespecial android/support/design/widget/df/a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/os/Parcelable;)V
aload 0
iconst_m1
putfield android/support/design/widget/AppBarLayout$Behavior/h I
return
.limit locals 4
.limit stack 4
.end method

.method public final synthetic a(Landroid/view/View;)V
aload 0
iconst_0
putfield android/support/design/widget/AppBarLayout$Behavior/d Z
aload 0
new java/lang/ref/WeakReference
dup
aload 1
invokespecial java/lang/ref/WeakReference/<init>(Ljava/lang/Object;)V
putfield android/support/design/widget/AppBarLayout$Behavior/o Ljava/lang/ref/WeakReference;
return
.limit locals 2
.limit stack 4
.end method

.method public final volatile synthetic a(I)Z
aload 0
iload 1
invokespecial android/support/design/widget/df/a(I)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final synthetic a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;FZ)Z
iconst_0
istore 6
aload 2
checkcast android/support/design/widget/AppBarLayout
astore 2
iload 4
ifne L0
aload 2
invokevirtual android/support/design/widget/AppBarLayout/getTotalScrollRange()I
ineg
istore 5
fload 3
fneg
fstore 3
aload 0
getfield android/support/design/widget/AppBarLayout$Behavior/e Ljava/lang/Runnable;
ifnull L1
aload 2
aload 0
getfield android/support/design/widget/AppBarLayout$Behavior/e Ljava/lang/Runnable;
invokevirtual android/support/design/widget/AppBarLayout/removeCallbacks(Ljava/lang/Runnable;)Z
pop
L1:
aload 0
getfield android/support/design/widget/AppBarLayout$Behavior/f Landroid/support/v4/widget/ca;
ifnonnull L2
aload 0
aload 2
invokevirtual android/support/design/widget/AppBarLayout/getContext()Landroid/content/Context;
aconst_null
invokestatic android/support/v4/widget/ca/a(Landroid/content/Context;Landroid/view/animation/Interpolator;)Landroid/support/v4/widget/ca;
putfield android/support/design/widget/AppBarLayout$Behavior/f Landroid/support/v4/widget/ca;
L2:
aload 0
getfield android/support/design/widget/AppBarLayout$Behavior/f Landroid/support/v4/widget/ca;
iconst_0
aload 0
invokevirtual android/support/design/widget/AppBarLayout$Behavior/a()I
iconst_0
fload 3
invokestatic java/lang/Math/round(F)I
iconst_0
iconst_0
iload 5
iconst_0
invokevirtual android/support/v4/widget/ca/a(IIIIIIII)V
aload 0
getfield android/support/design/widget/AppBarLayout$Behavior/f Landroid/support/v4/widget/ca;
invokevirtual android/support/v4/widget/ca/f()Z
ifeq L3
aload 0
new android/support/design/widget/e
dup
aload 0
aload 1
aload 2
invokespecial android/support/design/widget/e/<init>(Landroid/support/design/widget/AppBarLayout$Behavior;Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/AppBarLayout;)V
putfield android/support/design/widget/AppBarLayout$Behavior/e Ljava/lang/Runnable;
aload 2
aload 0
getfield android/support/design/widget/AppBarLayout$Behavior/e Ljava/lang/Runnable;
invokestatic android/support/v4/view/cx/a(Landroid/view/View;Ljava/lang/Runnable;)V
iconst_1
istore 4
L4:
iload 4
ireturn
L3:
aload 0
aconst_null
putfield android/support/design/widget/AppBarLayout$Behavior/e Ljava/lang/Runnable;
iconst_0
ireturn
L0:
fload 3
fconst_0
fcmpg
ifge L5
aload 2
invokevirtual android/support/design/widget/AppBarLayout/getTotalScrollRange()I
ineg
aload 2
invokevirtual android/support/design/widget/AppBarLayout/getDownNestedPreScrollRange()I
iadd
istore 5
iload 6
istore 4
aload 0
invokevirtual android/support/design/widget/AppBarLayout$Behavior/a()I
iload 5
if_icmpgt L4
L6:
iload 6
istore 4
aload 0
invokevirtual android/support/design/widget/AppBarLayout$Behavior/a()I
iload 5
if_icmpeq L4
aload 0
aload 1
aload 2
iload 5
invokespecial android/support/design/widget/AppBarLayout$Behavior/c(Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/AppBarLayout;I)V
iconst_1
ireturn
L5:
aload 2
invokevirtual android/support/design/widget/AppBarLayout/getUpNestedPreScrollRange()I
ineg
istore 5
iload 6
istore 4
aload 0
invokevirtual android/support/design/widget/AppBarLayout$Behavior/a()I
iload 5
if_icmplt L4
goto L6
.limit locals 7
.limit stack 9
.end method

.method public final synthetic a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;I)Z
aload 2
checkcast android/support/design/widget/AppBarLayout
astore 2
aload 0
aload 1
aload 2
iload 3
invokespecial android/support/design/widget/df/a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;I)Z
istore 5
aload 2
invokevirtual android/support/design/widget/AppBarLayout/getPendingAction()I
istore 4
iload 4
ifeq L0
iload 4
iconst_4
iand
ifeq L1
iconst_1
istore 3
L2:
iload 4
iconst_2
iand
ifeq L3
aload 2
invokevirtual android/support/design/widget/AppBarLayout/getUpNestedPreScrollRange()I
ineg
istore 4
iload 3
ifeq L4
aload 0
aload 1
aload 2
iload 4
invokespecial android/support/design/widget/AppBarLayout$Behavior/c(Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/AppBarLayout;I)V
L5:
aload 2
iconst_0
putfield android/support/design/widget/AppBarLayout/b I
L6:
aload 0
aload 2
invokespecial android/support/design/widget/AppBarLayout$Behavior/a(Landroid/support/design/widget/AppBarLayout;)V
iload 5
ireturn
L1:
iconst_0
istore 3
goto L2
L4:
aload 0
aload 1
aload 2
iload 4
invokevirtual android/support/design/widget/AppBarLayout$Behavior/a(Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/AppBarLayout;I)I
pop
goto L5
L3:
iload 4
iconst_1
iand
ifeq L5
iload 3
ifeq L7
aload 0
aload 1
aload 2
iconst_0
invokespecial android/support/design/widget/AppBarLayout$Behavior/c(Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/AppBarLayout;I)V
goto L5
L7:
aload 0
aload 1
aload 2
iconst_0
invokevirtual android/support/design/widget/AppBarLayout$Behavior/a(Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/AppBarLayout;I)I
pop
goto L5
L0:
aload 0
getfield android/support/design/widget/AppBarLayout$Behavior/h I
iflt L6
aload 2
aload 0
getfield android/support/design/widget/AppBarLayout$Behavior/h I
invokevirtual android/support/design/widget/AppBarLayout/getChildAt(I)Landroid/view/View;
astore 1
aload 1
invokevirtual android/view/View/getBottom()I
ineg
istore 3
aload 0
getfield android/support/design/widget/AppBarLayout$Behavior/i Z
ifeq L8
aload 1
invokestatic android/support/v4/view/cx/o(Landroid/view/View;)I
iload 3
iadd
istore 3
L9:
aload 0
iload 3
invokespecial android/support/design/widget/df/b(I)Z
pop
aload 0
iconst_m1
putfield android/support/design/widget/AppBarLayout$Behavior/h I
goto L6
L8:
aload 1
invokevirtual android/view/View/getHeight()I
i2f
aload 0
getfield android/support/design/widget/AppBarLayout$Behavior/j F
fmul
invokestatic java/lang/Math/round(F)I
iload 3
iadd
istore 3
goto L9
.limit locals 6
.limit stack 4
.end method

.method public final synthetic a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/view/MotionEvent;)Z
iconst_0
istore 8
aload 2
checkcast android/support/design/widget/AppBarLayout
astore 2
aload 0
getfield android/support/design/widget/AppBarLayout$Behavior/n I
ifge L0
aload 0
aload 1
invokevirtual android/support/design/widget/CoordinatorLayout/getContext()Landroid/content/Context;
invokestatic android/view/ViewConfiguration/get(Landroid/content/Context;)Landroid/view/ViewConfiguration;
invokevirtual android/view/ViewConfiguration/getScaledTouchSlop()I
putfield android/support/design/widget/AppBarLayout$Behavior/n I
L0:
aload 3
invokevirtual android/view/MotionEvent/getX()F
f2i
istore 4
aload 3
invokevirtual android/view/MotionEvent/getY()F
f2i
istore 5
aload 3
invokestatic android/support/v4/view/bk/a(Landroid/view/MotionEvent;)I
tableswitch 0
L1
L2
L3
L2
default : L4
L4:
iconst_1
istore 7
L5:
iload 7
ireturn
L1:
iload 8
istore 7
aload 1
aload 2
iload 4
iload 5
invokevirtual android/support/design/widget/CoordinatorLayout/a(Landroid/view/View;II)Z
ifeq L5
iload 8
istore 7
aload 0
invokespecial android/support/design/widget/AppBarLayout$Behavior/d()Z
ifeq L5
aload 0
iload 5
putfield android/support/design/widget/AppBarLayout$Behavior/m I
aload 0
aload 3
iconst_0
invokestatic android/support/v4/view/bk/b(Landroid/view/MotionEvent;I)I
putfield android/support/design/widget/AppBarLayout$Behavior/l I
goto L4
L3:
aload 3
aload 0
getfield android/support/design/widget/AppBarLayout$Behavior/l I
invokestatic android/support/v4/view/bk/a(Landroid/view/MotionEvent;I)I
istore 4
iload 8
istore 7
iload 4
iconst_m1
if_icmpeq L5
aload 3
iload 4
invokestatic android/support/v4/view/bk/d(Landroid/view/MotionEvent;I)F
f2i
istore 6
aload 0
getfield android/support/design/widget/AppBarLayout$Behavior/m I
iload 6
isub
istore 5
iload 5
istore 4
aload 0
getfield android/support/design/widget/AppBarLayout$Behavior/k Z
ifne L6
iload 5
istore 4
iload 5
invokestatic java/lang/Math/abs(I)I
aload 0
getfield android/support/design/widget/AppBarLayout$Behavior/n I
if_icmple L6
aload 0
iconst_1
putfield android/support/design/widget/AppBarLayout$Behavior/k Z
iload 5
ifle L7
iload 5
aload 0
getfield android/support/design/widget/AppBarLayout$Behavior/n I
isub
istore 4
L6:
aload 0
getfield android/support/design/widget/AppBarLayout$Behavior/k Z
ifeq L4
aload 0
iload 6
putfield android/support/design/widget/AppBarLayout$Behavior/m I
aload 0
aload 1
aload 2
iload 4
aload 2
invokevirtual android/support/design/widget/AppBarLayout/getDownNestedScrollRange()I
ineg
iconst_0
invokespecial android/support/design/widget/AppBarLayout$Behavior/a(Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/AppBarLayout;III)I
pop
goto L4
L7:
iload 5
aload 0
getfield android/support/design/widget/AppBarLayout$Behavior/n I
iadd
istore 4
goto L6
L2:
aload 0
iconst_0
putfield android/support/design/widget/AppBarLayout$Behavior/k Z
aload 0
iconst_m1
putfield android/support/design/widget/AppBarLayout$Behavior/l I
goto L4
.limit locals 9
.limit stack 6
.end method

.method public final synthetic a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/view/View;I)Z
iconst_1
istore 5
aload 2
checkcast android/support/design/widget/AppBarLayout
astore 2
iload 4
iconst_2
iand
ifeq L0
aload 2
invokevirtual android/support/design/widget/AppBarLayout/getTotalScrollRange()I
ifeq L1
iconst_1
istore 4
L2:
iload 4
ifeq L0
aload 1
invokevirtual android/support/design/widget/CoordinatorLayout/getHeight()I
aload 3
invokevirtual android/view/View/getHeight()I
isub
aload 2
invokevirtual android/support/design/widget/AppBarLayout/getHeight()I
if_icmpgt L0
L3:
iload 5
ifeq L4
aload 0
getfield android/support/design/widget/AppBarLayout$Behavior/g Landroid/support/design/widget/ck;
ifnull L4
aload 0
getfield android/support/design/widget/AppBarLayout$Behavior/g Landroid/support/design/widget/ck;
getfield android/support/design/widget/ck/a Landroid/support/design/widget/cr;
invokevirtual android/support/design/widget/cr/e()V
L4:
aload 0
aconst_null
putfield android/support/design/widget/AppBarLayout$Behavior/o Ljava/lang/ref/WeakReference;
iload 5
ireturn
L1:
iconst_0
istore 4
goto L2
L0:
iconst_0
istore 5
goto L3
.limit locals 6
.limit stack 2
.end method

.method public final volatile synthetic b()I
aload 0
invokespecial android/support/design/widget/df/b()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final synthetic b(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;I)V
aload 2
checkcast android/support/design/widget/AppBarLayout
astore 2
iload 3
ifge L0
aload 0
aload 1
aload 2
iload 3
aload 2
invokevirtual android/support/design/widget/AppBarLayout/getDownNestedScrollRange()I
ineg
iconst_0
invokespecial android/support/design/widget/AppBarLayout$Behavior/a(Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/AppBarLayout;III)I
pop
aload 0
iconst_1
putfield android/support/design/widget/AppBarLayout$Behavior/d Z
return
L0:
aload 0
iconst_0
putfield android/support/design/widget/AppBarLayout$Behavior/d Z
return
.limit locals 4
.limit stack 6
.end method

.method public final volatile synthetic b(I)Z
aload 0
iload 1
invokespecial android/support/design/widget/df/b(I)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final synthetic b(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/view/MotionEvent;)Z
aload 2
checkcast android/support/design/widget/AppBarLayout
astore 2
aload 0
getfield android/support/design/widget/AppBarLayout$Behavior/n I
ifge L0
aload 0
aload 1
invokevirtual android/support/design/widget/CoordinatorLayout/getContext()Landroid/content/Context;
invokestatic android/view/ViewConfiguration/get(Landroid/content/Context;)Landroid/view/ViewConfiguration;
invokevirtual android/view/ViewConfiguration/getScaledTouchSlop()I
putfield android/support/design/widget/AppBarLayout$Behavior/n I
L0:
aload 3
invokevirtual android/view/MotionEvent/getAction()I
iconst_2
if_icmpne L1
aload 0
getfield android/support/design/widget/AppBarLayout$Behavior/k Z
ifeq L1
iconst_1
ireturn
L1:
aload 3
invokestatic android/support/v4/view/bk/a(Landroid/view/MotionEvent;)I
tableswitch 0
L2
L3
L4
L3
default : L5
L5:
aload 0
getfield android/support/design/widget/AppBarLayout$Behavior/k Z
ireturn
L4:
aload 0
getfield android/support/design/widget/AppBarLayout$Behavior/l I
istore 4
iload 4
iconst_m1
if_icmpeq L5
aload 3
iload 4
invokestatic android/support/v4/view/bk/a(Landroid/view/MotionEvent;I)I
istore 4
iload 4
iconst_m1
if_icmpeq L5
aload 3
iload 4
invokestatic android/support/v4/view/bk/d(Landroid/view/MotionEvent;I)F
f2i
istore 4
iload 4
aload 0
getfield android/support/design/widget/AppBarLayout$Behavior/m I
isub
invokestatic java/lang/Math/abs(I)I
aload 0
getfield android/support/design/widget/AppBarLayout$Behavior/n I
if_icmple L5
aload 0
iconst_1
putfield android/support/design/widget/AppBarLayout$Behavior/k Z
aload 0
iload 4
putfield android/support/design/widget/AppBarLayout$Behavior/m I
goto L5
L2:
aload 0
iconst_0
putfield android/support/design/widget/AppBarLayout$Behavior/k Z
aload 3
invokevirtual android/view/MotionEvent/getX()F
f2i
istore 4
aload 3
invokevirtual android/view/MotionEvent/getY()F
f2i
istore 5
aload 1
aload 2
iload 4
iload 5
invokevirtual android/support/design/widget/CoordinatorLayout/a(Landroid/view/View;II)Z
ifeq L5
aload 0
invokespecial android/support/design/widget/AppBarLayout$Behavior/d()Z
ifeq L5
aload 0
iload 5
putfield android/support/design/widget/AppBarLayout$Behavior/m I
aload 0
aload 3
iconst_0
invokestatic android/support/v4/view/bk/b(Landroid/view/MotionEvent;I)I
putfield android/support/design/widget/AppBarLayout$Behavior/l I
goto L5
L3:
aload 0
iconst_0
putfield android/support/design/widget/AppBarLayout$Behavior/k Z
aload 0
iconst_m1
putfield android/support/design/widget/AppBarLayout$Behavior/l I
goto L5
.limit locals 6
.limit stack 4
.end method

.method public final volatile synthetic c()I
aload 0
invokespecial android/support/design/widget/df/c()I
ireturn
.limit locals 1
.limit stack 1
.end method
