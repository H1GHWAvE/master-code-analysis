.class public Lorg/xbill/DNS/Generator;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private current:J

.field public final dclass:I

.field public end:J

.field public final namePattern:Ljava/lang/String;

.field public final origin:Lorg/xbill/DNS/Name;

.field public final rdataPattern:Ljava/lang/String;

.field public start:J

.field public step:J

.field public final ttl:J

.field public final type:I


# direct methods
.method public constructor <init>(JJJLjava/lang/String;IIJLjava/lang/String;Lorg/xbill/DNS/Name;)V
    .registers 19

    .prologue
    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    const-wide/16 v2, 0x0

    cmp-long v2, p1, v2

    if-ltz v2, :cond_19

    const-wide/16 v2, 0x0

    cmp-long v2, p3, v2

    if-ltz v2, :cond_19

    cmp-long v2, p1, p3

    if-gtz v2, :cond_19

    const-wide/16 v2, 0x0

    cmp-long v2, p5, v2

    if-gtz v2, :cond_21

    .line 78
    :cond_19
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "invalid range specification"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 80
    :cond_21
    invoke-static {p8}, Lorg/xbill/DNS/Generator;->supportedType(I)Z

    move-result v2

    if-nez v2, :cond_2f

    .line 81
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "unsupported type"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 82
    :cond_2f
    invoke-static {p9}, Lorg/xbill/DNS/DClass;->check(I)V

    .line 84
    iput-wide p1, p0, Lorg/xbill/DNS/Generator;->start:J

    .line 85
    iput-wide p3, p0, Lorg/xbill/DNS/Generator;->end:J

    .line 86
    iput-wide p5, p0, Lorg/xbill/DNS/Generator;->step:J

    .line 87
    iput-object p7, p0, Lorg/xbill/DNS/Generator;->namePattern:Ljava/lang/String;

    .line 88
    iput p8, p0, Lorg/xbill/DNS/Generator;->type:I

    .line 89
    iput p9, p0, Lorg/xbill/DNS/Generator;->dclass:I

    .line 90
    iput-wide p10, p0, Lorg/xbill/DNS/Generator;->ttl:J

    .line 91
    move-object/from16 v0, p12

    iput-object v0, p0, Lorg/xbill/DNS/Generator;->rdataPattern:Ljava/lang/String;

    .line 92
    move-object/from16 v0, p13

    iput-object v0, p0, Lorg/xbill/DNS/Generator;->origin:Lorg/xbill/DNS/Name;

    .line 93
    iput-wide p1, p0, Lorg/xbill/DNS/Generator;->current:J

    .line 94
    return-void
.end method

.method private substitute(Ljava/lang/String;J)Ljava/lang/String;
    .registers 22

    .prologue
    .line 98
    const/4 v1, 0x0

    .line 99
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v14

    .line 100
    new-instance v15, Ljava/lang/StringBuffer;

    invoke-direct {v15}, Ljava/lang/StringBuffer;-><init>()V

    .line 102
    const/4 v0, 0x0

    :goto_b
    array-length v2, v14

    if-ge v0, v2, :cond_192

    .line 103
    aget-byte v2, v14, v0

    and-int/lit16 v2, v2, 0xff

    int-to-char v5, v2

    .line 104
    if-eqz v1, :cond_1c

    .line 105
    invoke-virtual {v15, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 106
    const/4 v1, 0x0

    .line 102
    :goto_19
    add-int/lit8 v0, v0, 0x1

    goto :goto_b

    .line 107
    :cond_1c
    const/16 v2, 0x5c

    if-ne v5, v2, :cond_2f

    .line 108
    add-int/lit8 v1, v0, 0x1

    array-length v2, v14

    if-ne v1, v2, :cond_2d

    .line 109
    new-instance v0, Lorg/xbill/DNS/TextParseException;

    const-string v1, "invalid escape character"

    invoke-direct {v0, v1}, Lorg/xbill/DNS/TextParseException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 111
    :cond_2d
    const/4 v1, 0x1

    goto :goto_19

    .line 112
    :cond_2f
    const/16 v2, 0x24

    if-ne v5, v2, :cond_18d

    .line 113
    const/4 v4, 0x0

    .line 114
    const-wide/16 v2, 0x0

    .line 115
    const-wide/16 v8, 0x0

    .line 116
    const-wide/16 v10, 0xa

    .line 117
    const/4 v7, 0x0

    .line 118
    add-int/lit8 v6, v0, 0x1

    array-length v12, v14

    if-ge v6, v12, :cond_53

    add-int/lit8 v6, v0, 0x1

    aget-byte v6, v14, v6

    const/16 v12, 0x24

    if-ne v6, v12, :cond_53

    .line 121
    add-int/lit8 v0, v0, 0x1

    aget-byte v2, v14, v0

    and-int/lit16 v2, v2, 0xff

    int-to-char v2, v2

    .line 122
    invoke-virtual {v15, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_19

    .line 124
    :cond_53
    add-int/lit8 v6, v0, 0x1

    array-length v12, v14

    if-ge v6, v12, :cond_137

    add-int/lit8 v6, v0, 0x1

    aget-byte v6, v14, v6

    const/16 v12, 0x7b

    if-ne v6, v12, :cond_137

    .line 126
    add-int/lit8 v6, v0, 0x1

    .line 127
    add-int/lit8 v0, v6, 0x1

    array-length v12, v14

    if-ge v0, v12, :cond_1a8

    add-int/lit8 v0, v6, 0x1

    aget-byte v0, v14, v0

    const/16 v12, 0x2d

    if-ne v0, v12, :cond_1a8

    .line 128
    const/4 v0, 0x1

    .line 129
    add-int/lit8 v4, v6, 0x1

    move/from16 v16, v5

    move v5, v4

    move/from16 v4, v16

    .line 131
    :goto_77
    add-int/lit8 v6, v5, 0x1

    array-length v12, v14

    if-ge v6, v12, :cond_a4

    .line 132
    add-int/lit8 v5, v5, 0x1

    aget-byte v4, v14, v5

    and-int/lit16 v4, v4, 0xff

    int-to-char v4, v4

    .line 133
    const/16 v6, 0x2c

    if-eq v4, v6, :cond_a4

    const/16 v6, 0x7d

    if-eq v4, v6, :cond_a4

    .line 135
    const/16 v6, 0x30

    if-lt v4, v6, :cond_93

    const/16 v6, 0x39

    if-le v4, v6, :cond_9b

    .line 136
    :cond_93
    new-instance v0, Lorg/xbill/DNS/TextParseException;

    const-string v1, "invalid offset"

    invoke-direct {v0, v1}, Lorg/xbill/DNS/TextParseException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 138
    :cond_9b
    add-int/lit8 v4, v4, -0x30

    int-to-char v4, v4

    .line 139
    const-wide/16 v12, 0xa

    mul-long/2addr v2, v12

    .line 140
    int-to-long v12, v4

    add-long/2addr v2, v12

    goto :goto_77

    .line 142
    :cond_a4
    if-eqz v0, :cond_1a5

    .line 143
    neg-long v12, v2

    .line 145
    :goto_a7
    const/16 v0, 0x2c

    if-ne v4, v0, :cond_1a2

    move-wide v2, v8

    move v0, v4

    move v4, v5

    .line 146
    :goto_ae
    add-int/lit8 v5, v4, 0x1

    array-length v6, v14

    if-ge v5, v6, :cond_db

    .line 147
    add-int/lit8 v4, v4, 0x1

    aget-byte v0, v14, v4

    and-int/lit16 v0, v0, 0xff

    int-to-char v0, v0

    .line 148
    const/16 v5, 0x2c

    if-eq v0, v5, :cond_db

    const/16 v5, 0x7d

    if-eq v0, v5, :cond_db

    .line 150
    const/16 v5, 0x30

    if-lt v0, v5, :cond_ca

    const/16 v5, 0x39

    if-le v0, v5, :cond_d2

    .line 151
    :cond_ca
    new-instance v0, Lorg/xbill/DNS/TextParseException;

    const-string v1, "invalid width"

    invoke-direct {v0, v1}, Lorg/xbill/DNS/TextParseException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 154
    :cond_d2
    add-int/lit8 v0, v0, -0x30

    int-to-char v0, v0

    .line 155
    const-wide/16 v8, 0xa

    mul-long/2addr v2, v8

    .line 156
    int-to-long v8, v0

    add-long/2addr v2, v8

    goto :goto_ae

    :cond_db
    move-wide v8, v2

    move/from16 v16, v0

    move v0, v4

    move/from16 v4, v16

    .line 160
    :goto_e1
    const/16 v2, 0x2c

    if-ne v4, v2, :cond_19d

    .line 161
    add-int/lit8 v2, v0, 0x1

    array-length v3, v14

    if-ne v2, v3, :cond_f2

    .line 162
    new-instance v0, Lorg/xbill/DNS/TextParseException;

    const-string v1, "invalid base"

    invoke-direct {v0, v1}, Lorg/xbill/DNS/TextParseException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 164
    :cond_f2
    add-int/lit8 v4, v0, 0x1

    aget-byte v0, v14, v4

    and-int/lit16 v0, v0, 0xff

    int-to-char v0, v0

    .line 165
    const/16 v2, 0x6f

    if-ne v0, v2, :cond_115

    .line 166
    const-wide/16 v2, 0x8

    move v0, v7

    .line 178
    :goto_100
    add-int/lit8 v5, v4, 0x1

    array-length v6, v14

    if-eq v5, v6, :cond_10d

    add-int/lit8 v5, v4, 0x1

    aget-byte v5, v14, v5

    const/16 v6, 0x7d

    if-eq v5, v6, :cond_131

    .line 179
    :cond_10d
    new-instance v0, Lorg/xbill/DNS/TextParseException;

    const-string v1, "invalid modifiers"

    invoke-direct {v0, v1}, Lorg/xbill/DNS/TextParseException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 167
    :cond_115
    const/16 v2, 0x78

    if-ne v0, v2, :cond_11d

    .line 168
    const-wide/16 v2, 0x10

    move v0, v7

    goto :goto_100

    .line 169
    :cond_11d
    const/16 v2, 0x58

    if-ne v0, v2, :cond_125

    .line 170
    const-wide/16 v2, 0x10

    .line 171
    const/4 v0, 0x1

    goto :goto_100

    .line 173
    :cond_125
    const/16 v2, 0x64

    if-eq v0, v2, :cond_199

    .line 174
    new-instance v0, Lorg/xbill/DNS/TextParseException;

    const-string v1, "invalid base"

    invoke-direct {v0, v1}, Lorg/xbill/DNS/TextParseException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 181
    :cond_131
    add-int/lit8 v4, v4, 0x1

    move-wide v10, v2

    move v7, v0

    move-wide v2, v12

    move v0, v4

    .line 183
    :cond_137
    add-long v2, v2, p2

    .line 184
    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-gez v4, :cond_147

    .line 185
    new-instance v0, Lorg/xbill/DNS/TextParseException;

    const-string v1, "invalid offset expansion"

    invoke-direct {v0, v1}, Lorg/xbill/DNS/TextParseException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 188
    :cond_147
    const-wide/16 v4, 0x8

    cmp-long v4, v10, v4

    if-nez v4, :cond_178

    .line 189
    invoke-static {v2, v3}, Ljava/lang/Long;->toOctalString(J)Ljava/lang/String;

    move-result-object v2

    .line 194
    :goto_151
    if-eqz v7, :cond_197

    .line 195
    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    move-object v4, v2

    .line 196
    :goto_158
    const-wide/16 v2, 0x0

    cmp-long v2, v8, v2

    if-eqz v2, :cond_188

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v2

    int-to-long v2, v2

    cmp-long v2, v8, v2

    if-lez v2, :cond_188

    .line 197
    long-to-int v2, v8

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v3

    sub-int/2addr v2, v3

    .line 198
    :goto_16d
    add-int/lit8 v3, v2, -0x1

    if-lez v2, :cond_188

    .line 199
    const/16 v2, 0x30

    invoke-virtual {v15, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move v2, v3

    goto :goto_16d

    .line 190
    :cond_178
    const-wide/16 v4, 0x10

    cmp-long v4, v10, v4

    if-nez v4, :cond_183

    .line 191
    invoke-static {v2, v3}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v2

    goto :goto_151

    .line 193
    :cond_183
    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    goto :goto_151

    .line 201
    :cond_188
    invoke-virtual {v15, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto/16 :goto_19

    .line 203
    :cond_18d
    invoke-virtual {v15, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto/16 :goto_19

    .line 206
    :cond_192
    invoke-virtual {v15}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_197
    move-object v4, v2

    goto :goto_158

    :cond_199
    move-wide v2, v10

    move v0, v7

    goto/16 :goto_100

    :cond_19d
    move-wide v2, v10

    move v4, v0

    move v0, v7

    goto/16 :goto_100

    :cond_1a2
    move v0, v5

    goto/16 :goto_e1

    :cond_1a5
    move-wide v12, v2

    goto/16 :goto_a7

    :cond_1a8
    move v0, v4

    move v4, v5

    move v5, v6

    goto/16 :goto_77
.end method

.method public static supportedType(I)Z
    .registers 3

    .prologue
    const/4 v0, 0x1

    .line 51
    invoke-static {p0}, Lorg/xbill/DNS/Type;->check(I)V

    .line 52
    const/16 v1, 0xc

    if-eq p0, v1, :cond_18

    const/4 v1, 0x5

    if-eq p0, v1, :cond_18

    const/16 v1, 0x27

    if-eq p0, v1, :cond_18

    if-eq p0, v0, :cond_18

    const/16 v1, 0x1c

    if-eq p0, v1, :cond_18

    const/4 v1, 0x2

    if-ne p0, v1, :cond_19

    :cond_18
    :goto_18
    return v0

    :cond_19
    const/4 v0, 0x0

    goto :goto_18
.end method


# virtual methods
.method public expand()[Lorg/xbill/DNS/Record;
    .registers 12

    .prologue
    .line 232
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 233
    iget-wide v0, p0, Lorg/xbill/DNS/Generator;->start:J

    move-wide v8, v0

    :goto_8
    iget-wide v0, p0, Lorg/xbill/DNS/Generator;->end:J

    cmp-long v0, v8, v0

    if-gez v0, :cond_38

    .line 234
    iget-object v0, p0, Lorg/xbill/DNS/Generator;->namePattern:Ljava/lang/String;

    iget-wide v2, p0, Lorg/xbill/DNS/Generator;->current:J

    invoke-direct {p0, v0, v2, v3}, Lorg/xbill/DNS/Generator;->substitute(Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v0

    .line 235
    iget-object v1, p0, Lorg/xbill/DNS/Generator;->origin:Lorg/xbill/DNS/Name;

    invoke-static {v0, v1}, Lorg/xbill/DNS/Name;->fromString(Ljava/lang/String;Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/Name;

    move-result-object v1

    .line 236
    iget-object v0, p0, Lorg/xbill/DNS/Generator;->rdataPattern:Ljava/lang/String;

    iget-wide v2, p0, Lorg/xbill/DNS/Generator;->current:J

    invoke-direct {p0, v0, v2, v3}, Lorg/xbill/DNS/Generator;->substitute(Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v6

    .line 237
    iget v2, p0, Lorg/xbill/DNS/Generator;->type:I

    iget v3, p0, Lorg/xbill/DNS/Generator;->dclass:I

    iget-wide v4, p0, Lorg/xbill/DNS/Generator;->ttl:J

    iget-object v7, p0, Lorg/xbill/DNS/Generator;->origin:Lorg/xbill/DNS/Name;

    invoke-static/range {v1 .. v7}, Lorg/xbill/DNS/Record;->fromString(Lorg/xbill/DNS/Name;IIJLjava/lang/String;Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/Record;

    move-result-object v0

    invoke-interface {v10, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 233
    iget-wide v0, p0, Lorg/xbill/DNS/Generator;->step:J

    add-long/2addr v0, v8

    move-wide v8, v0

    goto :goto_8

    .line 240
    :cond_38
    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lorg/xbill/DNS/Record;

    invoke-interface {v10, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lorg/xbill/DNS/Record;

    check-cast v0, [Lorg/xbill/DNS/Record;

    return-object v0
.end method

.method public nextRecord()Lorg/xbill/DNS/Record;
    .registers 9

    .prologue
    .line 216
    iget-wide v0, p0, Lorg/xbill/DNS/Generator;->current:J

    iget-wide v2, p0, Lorg/xbill/DNS/Generator;->end:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_a

    .line 217
    const/4 v0, 0x0

    .line 222
    :goto_9
    return-object v0

    .line 218
    :cond_a
    iget-object v0, p0, Lorg/xbill/DNS/Generator;->namePattern:Ljava/lang/String;

    iget-wide v2, p0, Lorg/xbill/DNS/Generator;->current:J

    invoke-direct {p0, v0, v2, v3}, Lorg/xbill/DNS/Generator;->substitute(Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v0

    .line 219
    iget-object v1, p0, Lorg/xbill/DNS/Generator;->origin:Lorg/xbill/DNS/Name;

    invoke-static {v0, v1}, Lorg/xbill/DNS/Name;->fromString(Ljava/lang/String;Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/Name;

    move-result-object v1

    .line 220
    iget-object v0, p0, Lorg/xbill/DNS/Generator;->rdataPattern:Ljava/lang/String;

    iget-wide v2, p0, Lorg/xbill/DNS/Generator;->current:J

    invoke-direct {p0, v0, v2, v3}, Lorg/xbill/DNS/Generator;->substitute(Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v6

    .line 221
    iget-wide v2, p0, Lorg/xbill/DNS/Generator;->current:J

    iget-wide v4, p0, Lorg/xbill/DNS/Generator;->step:J

    add-long/2addr v2, v4

    iput-wide v2, p0, Lorg/xbill/DNS/Generator;->current:J

    .line 222
    iget v2, p0, Lorg/xbill/DNS/Generator;->type:I

    iget v3, p0, Lorg/xbill/DNS/Generator;->dclass:I

    iget-wide v4, p0, Lorg/xbill/DNS/Generator;->ttl:J

    iget-object v7, p0, Lorg/xbill/DNS/Generator;->origin:Lorg/xbill/DNS/Name;

    invoke-static/range {v1 .. v7}, Lorg/xbill/DNS/Record;->fromString(Lorg/xbill/DNS/Name;IIJLjava/lang/String;Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/Record;

    move-result-object v0

    goto :goto_9
.end method

.method public toString()Ljava/lang/String;
    .registers 7

    .prologue
    .line 249
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 250
    const-string v1, "$GENERATE "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 251
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    iget-wide v2, p0, Lorg/xbill/DNS/Generator;->start:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-wide v2, p0, Lorg/xbill/DNS/Generator;->end:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 252
    iget-wide v2, p0, Lorg/xbill/DNS/Generator;->step:J

    const-wide/16 v4, 0x1

    cmp-long v1, v2, v4

    if-lez v1, :cond_44

    .line 253
    new-instance v1, Ljava/lang/StringBuffer;

    const-string v2, "/"

    invoke-direct {v1, v2}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, Lorg/xbill/DNS/Generator;->step:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 254
    :cond_44
    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 255
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    iget-object v2, p0, Lorg/xbill/DNS/Generator;->namePattern:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 256
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    iget-wide v2, p0, Lorg/xbill/DNS/Generator;->ttl:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 257
    iget v1, p0, Lorg/xbill/DNS/Generator;->dclass:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_86

    const-string v1, "noPrintIN"

    invoke-static {v1}, Lorg/xbill/DNS/Options;->check(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_a2

    .line 258
    :cond_86
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    iget v2, p0, Lorg/xbill/DNS/Generator;->dclass:I

    invoke-static {v2}, Lorg/xbill/DNS/DClass;->string(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 259
    :cond_a2
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    iget v2, p0, Lorg/xbill/DNS/Generator;->type:I

    invoke-static {v2}, Lorg/xbill/DNS/Type;->string(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 260
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    iget-object v2, p0, Lorg/xbill/DNS/Generator;->rdataPattern:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 261
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
