.class public Lorg/xbill/DNS/Tokenizer$Token;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public type:I

.field public value:Ljava/lang/String;


# direct methods
.method private constructor <init>()V
    .registers 2

    .prologue
    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    const/4 v0, -0x1

    iput v0, p0, Lorg/xbill/DNS/Tokenizer$Token;->type:I

    .line 77
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/xbill/DNS/Tokenizer$Token;->value:Ljava/lang/String;

    .line 78
    return-void
.end method

.method constructor <init>(Lorg/xbill/DNS/Tokenizer$1;)V
    .registers 2

    .prologue
    .line 67
    invoke-direct {p0}, Lorg/xbill/DNS/Tokenizer$Token;-><init>()V

    return-void
.end method

.method static access$100(Lorg/xbill/DNS/Tokenizer$Token;ILjava/lang/StringBuffer;)Lorg/xbill/DNS/Tokenizer$Token;
    .registers 4

    .prologue
    .line 67
    invoke-direct {p0, p1, p2}, Lorg/xbill/DNS/Tokenizer$Token;->set(ILjava/lang/StringBuffer;)Lorg/xbill/DNS/Tokenizer$Token;

    move-result-object v0

    return-object v0
.end method

.method private set(ILjava/lang/StringBuffer;)Lorg/xbill/DNS/Tokenizer$Token;
    .registers 4

    .prologue
    .line 82
    if-gez p1, :cond_8

    .line 83
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 84
    :cond_8
    iput p1, p0, Lorg/xbill/DNS/Tokenizer$Token;->type:I

    .line 85
    if-nez p2, :cond_10

    const/4 v0, 0x0

    :goto_d
    iput-object v0, p0, Lorg/xbill/DNS/Tokenizer$Token;->value:Ljava/lang/String;

    .line 86
    return-object p0

    .line 85
    :cond_10
    invoke-virtual {p2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_d
.end method


# virtual methods
.method public isEOL()Z
    .registers 3

    .prologue
    const/4 v0, 0x1

    .line 122
    iget v1, p0, Lorg/xbill/DNS/Tokenizer$Token;->type:I

    if-eq v1, v0, :cond_9

    iget v1, p0, Lorg/xbill/DNS/Tokenizer$Token;->type:I

    if-nez v1, :cond_a

    :cond_9
    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public isString()Z
    .registers 3

    .prologue
    .line 116
    iget v0, p0, Lorg/xbill/DNS/Tokenizer$Token;->type:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_a

    iget v0, p0, Lorg/xbill/DNS/Tokenizer$Token;->type:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_c

    :cond_a
    const/4 v0, 0x1

    :goto_b
    return v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    .prologue
    .line 95
    iget v0, p0, Lorg/xbill/DNS/Tokenizer$Token;->type:I

    packed-switch v0, :pswitch_data_5a

    .line 109
    const-string v0, "<unknown>"

    :goto_7
    return-object v0

    .line 97
    :pswitch_8
    const-string v0, "<eof>"

    goto :goto_7

    .line 99
    :pswitch_b
    const-string v0, "<eol>"

    goto :goto_7

    .line 101
    :pswitch_e
    const-string v0, "<whitespace>"

    goto :goto_7

    .line 103
    :pswitch_11
    new-instance v0, Ljava/lang/StringBuffer;

    const-string v1, "<identifier: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lorg/xbill/DNS/Tokenizer$Token;->value:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, ">"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_7

    .line 105
    :pswitch_29
    new-instance v0, Ljava/lang/StringBuffer;

    const-string v1, "<quoted_string: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lorg/xbill/DNS/Tokenizer$Token;->value:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, ">"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_7

    .line 107
    :pswitch_41
    new-instance v0, Ljava/lang/StringBuffer;

    const-string v1, "<comment: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lorg/xbill/DNS/Tokenizer$Token;->value:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, ">"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_7

    .line 95
    nop

    :pswitch_data_5a
    .packed-switch 0x0
        :pswitch_8
        :pswitch_b
        :pswitch_e
        :pswitch_11
        :pswitch_29
        :pswitch_41
    .end packed-switch
.end method
