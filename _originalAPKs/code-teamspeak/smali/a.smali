.class final La;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final a:Ljava/net/Socket;

.field private final b:Ljnamed;


# direct methods
.method constructor <init>(Ljnamed;Ljava/net/Socket;)V
    .registers 3

    .prologue
    .line 542
    iput-object p1, p0, La;->b:Ljnamed;

    iput-object p2, p0, La;->a:Ljava/net/Socket;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .registers 7

    .prologue
    .line 543
    iget-object v0, p0, La;->b:Ljnamed;

    iget-object v1, p0, La;->a:Ljava/net/Socket;

    .line 1500
    :try_start_4
    invoke-virtual {v1}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object v2

    .line 1501
    new-instance v3, Ljava/io/DataInputStream;

    invoke-direct {v3, v2}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 1502
    invoke-virtual {v3}, Ljava/io/DataInputStream;->readUnsignedShort()I

    move-result v2

    .line 1503
    new-array v4, v2, [B

    .line 1504
    invoke-virtual {v3, v4}, Ljava/io/DataInputStream;->readFully([B)V
    :try_end_16
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_16} :catch_40
    .catchall {:try_start_4 .. :try_end_16} :catchall_71

    .line 1509
    :try_start_16
    new-instance v3, Lorg/xbill/DNS/Message;

    invoke-direct {v3, v4}, Lorg/xbill/DNS/Message;-><init>([B)V

    .line 1510
    invoke-virtual {v0, v3, v4, v2, v1}, Ljnamed;->a(Lorg/xbill/DNS/Message;[BILjava/net/Socket;)[B
    :try_end_1e
    .catch Ljava/io/IOException; {:try_start_16 .. :try_end_1e} :catch_25
    .catchall {:try_start_16 .. :try_end_1e} :catchall_71

    move-result-object v0

    .line 1511
    if-nez v0, :cond_2a

    .line 1529
    :try_start_21
    invoke-virtual {v1}, Ljava/net/Socket;->close()V
    :try_end_24
    .catch Ljava/io/IOException; {:try_start_21 .. :try_end_24} :catch_76

    .line 1532
    :goto_24
    return-void

    .line 1515
    :catch_25
    move-exception v0

    :try_start_26
    invoke-static {v4}, Ljnamed;->a([B)[B

    move-result-object v0

    .line 1517
    :cond_2a
    new-instance v2, Ljava/io/DataOutputStream;

    invoke-virtual {v1}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 1518
    array-length v3, v0

    invoke-virtual {v2, v3}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 1519
    invoke-virtual {v2, v0}, Ljava/io/DataOutputStream;->write([B)V
    :try_end_3a
    .catch Ljava/io/IOException; {:try_start_26 .. :try_end_3a} :catch_40
    .catchall {:try_start_26 .. :try_end_3a} :catchall_71

    .line 1529
    :try_start_3a
    invoke-virtual {v1}, Ljava/net/Socket;->close()V
    :try_end_3d
    .catch Ljava/io/IOException; {:try_start_3a .. :try_end_3d} :catch_3e

    goto :goto_24

    .line 1532
    :catch_3e
    move-exception v0

    goto :goto_24

    .line 1521
    :catch_40
    move-exception v0

    .line 1522
    :try_start_41
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v3, Ljava/lang/StringBuffer;

    const-string v4, "TCPclient("

    invoke-direct {v3, v4}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/net/Socket;->getLocalAddress()Ljava/net/InetAddress;

    move-result-object v4

    invoke-virtual {v1}, Ljava/net/Socket;->getLocalPort()I

    move-result v5

    invoke-static {v4, v5}, Ljnamed;->a(Ljava/net/InetAddress;I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string v4, "): "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_6b
    .catchall {:try_start_41 .. :try_end_6b} :catchall_71

    .line 1529
    :try_start_6b
    invoke-virtual {v1}, Ljava/net/Socket;->close()V
    :try_end_6e
    .catch Ljava/io/IOException; {:try_start_6b .. :try_end_6e} :catch_6f

    goto :goto_24

    .line 1532
    :catch_6f
    move-exception v0

    goto :goto_24

    .line 1528
    :catchall_71
    move-exception v0

    .line 1529
    :try_start_72
    invoke-virtual {v1}, Ljava/net/Socket;->close()V
    :try_end_75
    .catch Ljava/io/IOException; {:try_start_72 .. :try_end_75} :catch_78

    .line 1531
    :goto_75
    throw v0

    :catch_76
    move-exception v0

    goto :goto_24

    :catch_78
    move-exception v1

    goto :goto_75
.end method
