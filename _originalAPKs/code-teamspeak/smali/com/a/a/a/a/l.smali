.class final Lcom/a/a/a/a/l;
.super Lcom/a/a/a/a/f;
.source "SourceFile"


# static fields
.field private static final e:I = 0x101

.field private static final f:I = 0x102

.field private static final g:I = 0x103


# instance fields
.field final synthetic b:Lcom/a/a/a/a/k;

.field private final c:Lcom/a/a/a/a/p;

.field private d:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Lcom/a/a/a/a/k;Lcom/a/a/a/a/p;)V
    .registers 7

    .prologue
    .line 214
    iput-object p1, p0, Lcom/a/a/a/a/l;->b:Lcom/a/a/a/a/k;

    invoke-direct {p0}, Lcom/a/a/a/a/f;-><init>()V

    .line 215
    iput-object p2, p0, Lcom/a/a/a/a/l;->c:Lcom/a/a/a/a/p;

    .line 216
    new-instance v0, Lcom/a/a/a/a/m;

    invoke-direct {v0, p0, p1}, Lcom/a/a/a/a/m;-><init>(Lcom/a/a/a/a/l;Lcom/a/a/a/a/k;)V

    iput-object v0, p0, Lcom/a/a/a/a/l;->d:Ljava/lang/Runnable;

    .line 1278
    const-string v0, "LicenseChecker"

    const-string v1, "Start monitoring timeout."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1279
    iget-object v0, p0, Lcom/a/a/a/a/l;->b:Lcom/a/a/a/a/k;

    invoke-static {v0}, Lcom/a/a/a/a/k;->c(Lcom/a/a/a/a/k;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/a/a/a/a/l;->d:Ljava/lang/Runnable;

    const-wide/16 v2, 0x2710

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 224
    return-void
.end method

.method static synthetic a(Lcom/a/a/a/a/l;)Lcom/a/a/a/a/p;
    .registers 2

    .prologue
    .line 210
    iget-object v0, p0, Lcom/a/a/a/a/l;->c:Lcom/a/a/a/a/p;

    return-object v0
.end method

.method private a()V
    .registers 5

    .prologue
    .line 278
    const-string v0, "LicenseChecker"

    const-string v1, "Start monitoring timeout."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 279
    iget-object v0, p0, Lcom/a/a/a/a/l;->b:Lcom/a/a/a/a/k;

    invoke-static {v0}, Lcom/a/a/a/a/k;->c(Lcom/a/a/a/a/k;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/a/a/a/a/l;->d:Ljava/lang/Runnable;

    const-wide/16 v2, 0x2710

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 280
    return-void
.end method

.method private b()V
    .registers 3

    .prologue
    .line 283
    const-string v0, "LicenseChecker"

    const-string v1, "Clearing timeout."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 284
    iget-object v0, p0, Lcom/a/a/a/a/l;->b:Lcom/a/a/a/a/k;

    invoke-static {v0}, Lcom/a/a/a/a/k;->c(Lcom/a/a/a/a/k;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/a/a/a/a/l;->d:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 285
    return-void
.end method

.method static synthetic b(Lcom/a/a/a/a/l;)V
    .registers 3

    .prologue
    .line 1283
    const-string v0, "LicenseChecker"

    const-string v1, "Clearing timeout."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1284
    iget-object v0, p0, Lcom/a/a/a/a/l;->b:Lcom/a/a/a/a/k;

    invoke-static {v0}, Lcom/a/a/a/a/k;->c(Lcom/a/a/a/a/k;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/a/a/a/a/l;->d:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 210
    return-void
.end method


# virtual methods
.method public final a(ILjava/lang/String;Ljava/lang/String;)V
    .registers 6

    .prologue
    .line 234
    iget-object v0, p0, Lcom/a/a/a/a/l;->b:Lcom/a/a/a/a/k;

    invoke-static {v0}, Lcom/a/a/a/a/k;->c(Lcom/a/a/a/a/k;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/a/a/a/a/n;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/a/a/a/a/n;-><init>(Lcom/a/a/a/a/l;ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 275
    return-void
.end method
