.class public final Lcom/a/a/a/a/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/a/a/a/a/s;


# static fields
.field public static final a:I = 0x0

.field public static final b:I = 0x1

.field private static final f:Ljava/lang/String; = "APKExpansionPolicy"

.field private static final g:Ljava/lang/String; = "com.android.vending.licensing.APKExpansionPolicy"

.field private static final h:Ljava/lang/String; = "lastResponse"

.field private static final i:Ljava/lang/String; = "validityTimestamp"

.field private static final j:Ljava/lang/String; = "retryUntil"

.field private static final k:Ljava/lang/String; = "maxRetries"

.field private static final l:Ljava/lang/String; = "retryCount"

.field private static final m:Ljava/lang/String; = "0"

.field private static final n:Ljava/lang/String; = "0"

.field private static final o:Ljava/lang/String; = "0"

.field private static final p:Ljava/lang/String; = "0"

.field private static final q:J = 0xea60L


# instance fields
.field private A:Ljava/util/Vector;

.field private r:J

.field private s:J

.field private t:J

.field private u:J

.field private v:J

.field private w:I

.field private x:Lcom/a/a/a/a/t;

.field private y:Ljava/util/Vector;

.field private z:Ljava/util/Vector;


# direct methods
.method private constructor <init>(Landroid/content/Context;Lcom/a/a/a/a/r;)V
    .registers 6

    .prologue
    .line 85
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/a/a/a/a/b;->v:J

    .line 69
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/a/a/a/a/b;->y:Ljava/util/Vector;

    .line 70
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/a/a/a/a/b;->z:Ljava/util/Vector;

    .line 71
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/a/a/a/a/b;->A:Ljava/util/Vector;

    .line 87
    const-string v0, "com.android.vending.licensing.APKExpansionPolicy"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 88
    new-instance v1, Lcom/a/a/a/a/t;

    invoke-direct {v1, v0, p2}, Lcom/a/a/a/a/t;-><init>(Landroid/content/SharedPreferences;Lcom/a/a/a/a/r;)V

    iput-object v1, p0, Lcom/a/a/a/a/b;->x:Lcom/a/a/a/a/t;

    .line 89
    iget-object v0, p0, Lcom/a/a/a/a/b;->x:Lcom/a/a/a/a/t;

    const-string v1, "lastResponse"

    const/16 v2, 0x123

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/a/a/a/a/t;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/a/a/a/a/b;->w:I

    .line 91
    iget-object v0, p0, Lcom/a/a/a/a/b;->x:Lcom/a/a/a/a/t;

    const-string v1, "validityTimestamp"

    const-string v2, "0"

    invoke-virtual {v0, v1, v2}, Lcom/a/a/a/a/t;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/a/a/a/a/b;->r:J

    .line 93
    iget-object v0, p0, Lcom/a/a/a/a/b;->x:Lcom/a/a/a/a/t;

    const-string v1, "retryUntil"

    const-string v2, "0"

    invoke-virtual {v0, v1, v2}, Lcom/a/a/a/a/t;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/a/a/a/a/b;->s:J

    .line 94
    iget-object v0, p0, Lcom/a/a/a/a/b;->x:Lcom/a/a/a/a/t;

    const-string v1, "maxRetries"

    const-string v2, "0"

    invoke-virtual {v0, v1, v2}, Lcom/a/a/a/a/t;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/a/a/a/a/b;->t:J

    .line 95
    iget-object v0, p0, Lcom/a/a/a/a/b;->x:Lcom/a/a/a/a/t;

    const-string v1, "retryCount"

    const-string v2, "0"

    invoke-virtual {v0, v1, v2}, Lcom/a/a/a/a/t;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/a/a/a/a/b;->u:J

    .line 96
    return-void
.end method

.method private a(I)V
    .registers 5

    .prologue
    .line 179
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/a/a/a/a/b;->v:J

    .line 180
    iput p1, p0, Lcom/a/a/a/a/b;->w:I

    .line 181
    iget-object v0, p0, Lcom/a/a/a/a/b;->x:Lcom/a/a/a/a/t;

    const-string v1, "lastResponse"

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/a/a/a/a/t;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    return-void
.end method

.method private a(IJ)V
    .registers 6

    .prologue
    .line 341
    iget-object v0, p0, Lcom/a/a/a/a/b;->A:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-lt p1, v0, :cond_f

    .line 342
    iget-object v0, p0, Lcom/a/a/a/a/b;->A:Ljava/util/Vector;

    add-int/lit8 v1, p1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/Vector;->setSize(I)V

    .line 344
    :cond_f
    iget-object v0, p0, Lcom/a/a/a/a/b;->A:Ljava/util/Vector;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Ljava/util/Vector;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 345
    return-void
.end method

.method private a(ILjava/lang/String;)V
    .registers 5

    .prologue
    .line 313
    iget-object v0, p0, Lcom/a/a/a/a/b;->y:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-lt p1, v0, :cond_f

    .line 314
    iget-object v0, p0, Lcom/a/a/a/a/b;->y:Ljava/util/Vector;

    add-int/lit8 v1, p1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/Vector;->setSize(I)V

    .line 316
    :cond_f
    iget-object v0, p0, Lcom/a/a/a/a/b;->y:Ljava/util/Vector;

    invoke-virtual {v0, p1, p2}, Ljava/util/Vector;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 317
    return-void
.end method

.method private a(J)V
    .registers 6

    .prologue
    .line 191
    iput-wide p1, p0, Lcom/a/a/a/a/b;->u:J

    .line 192
    iget-object v0, p0, Lcom/a/a/a/a/b;->x:Lcom/a/a/a/a/t;

    const-string v1, "retryCount"

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/a/a/a/a/t;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 193
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .registers 6

    .prologue
    .line 209
    :try_start_0
    invoke-static {p1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_7
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_7} :catch_16

    move-result-object v0

    .line 217
    :goto_8
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/a/a/a/a/b;->r:J

    .line 218
    iget-object v0, p0, Lcom/a/a/a/a/b;->x:Lcom/a/a/a/a/t;

    const-string v1, "validityTimestamp"

    invoke-virtual {v0, v1, p1}, Lcom/a/a/a/a/t;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 219
    return-void

    .line 212
    :catch_16
    move-exception v0

    const-string v0, "APKExpansionPolicy"

    const-string v1, "License validity timestamp (VT) missing, caching for a minute"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 213
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-wide/32 v2, 0xea60

    add-long/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 214
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object p1

    goto :goto_8
.end method

.method private b(I)Ljava/lang/String;
    .registers 3

    .prologue
    .line 297
    iget-object v0, p0, Lcom/a/a/a/a/b;->y:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge p1, v0, :cond_11

    .line 298
    iget-object v0, p0, Lcom/a/a/a/a/b;->y:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 300
    :goto_10
    return-object v0

    :cond_11
    const/4 v0, 0x0

    goto :goto_10
.end method

.method private b(ILjava/lang/String;)V
    .registers 5

    .prologue
    .line 327
    iget-object v0, p0, Lcom/a/a/a/a/b;->z:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-lt p1, v0, :cond_f

    .line 328
    iget-object v0, p0, Lcom/a/a/a/a/b;->z:Ljava/util/Vector;

    add-int/lit8 v1, p1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/Vector;->setSize(I)V

    .line 330
    :cond_f
    iget-object v0, p0, Lcom/a/a/a/a/b;->z:Ljava/util/Vector;

    invoke-virtual {v0, p1, p2}, Ljava/util/Vector;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 331
    return-void
.end method

.method private b(Ljava/lang/String;)V
    .registers 4

    .prologue
    .line 235
    :try_start_0
    invoke-static {p1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_7
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_7} :catch_16

    move-result-object v0

    .line 243
    :goto_8
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/a/a/a/a/b;->s:J

    .line 244
    iget-object v0, p0, Lcom/a/a/a/a/b;->x:Lcom/a/a/a/a/t;

    const-string v1, "retryUntil"

    invoke-virtual {v0, v1, p1}, Lcom/a/a/a/a/t;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 245
    return-void

    .line 238
    :catch_16
    move-exception v0

    const-string v0, "APKExpansionPolicy"

    const-string v1, "License retry timestamp (GT) missing, grace period disabled"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 239
    const-string p1, "0"

    .line 240
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_8
.end method

.method private c(I)Ljava/lang/String;
    .registers 3

    .prologue
    .line 320
    iget-object v0, p0, Lcom/a/a/a/a/b;->z:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge p1, v0, :cond_11

    .line 321
    iget-object v0, p0, Lcom/a/a/a/a/b;->z:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 323
    :goto_10
    return-object v0

    :cond_11
    const/4 v0, 0x0

    goto :goto_10
.end method

.method private c()V
    .registers 4

    .prologue
    .line 103
    iget-object v0, p0, Lcom/a/a/a/a/b;->x:Lcom/a/a/a/a/t;

    const-string v1, "lastResponse"

    const/16 v2, 0x123

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/a/a/a/a/t;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    const-string v0, "0"

    invoke-direct {p0, v0}, Lcom/a/a/a/a/b;->b(Ljava/lang/String;)V

    .line 105
    const-string v0, "0"

    invoke-direct {p0, v0}, Lcom/a/a/a/a/b;->c(Ljava/lang/String;)V

    .line 106
    const-string v0, "0"

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/a/a/a/a/b;->a(J)V

    .line 107
    const-string v0, "0"

    invoke-direct {p0, v0}, Lcom/a/a/a/a/b;->a(Ljava/lang/String;)V

    .line 108
    iget-object v0, p0, Lcom/a/a/a/a/b;->x:Lcom/a/a/a/a/t;

    invoke-virtual {v0}, Lcom/a/a/a/a/t;->a()V

    .line 109
    return-void
.end method

.method private c(Ljava/lang/String;)V
    .registers 4

    .prologue
    .line 261
    :try_start_0
    invoke-static {p1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_7
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_7} :catch_16

    move-result-object v0

    .line 269
    :goto_8
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/a/a/a/a/b;->t:J

    .line 270
    iget-object v0, p0, Lcom/a/a/a/a/b;->x:Lcom/a/a/a/a/t;

    const-string v1, "maxRetries"

    invoke-virtual {v0, v1, p1}, Lcom/a/a/a/a/t;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 271
    return-void

    .line 264
    :catch_16
    move-exception v0

    const-string v0, "APKExpansionPolicy"

    const-string v1, "Licence retry count (GR) missing, grace period disabled"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 265
    const-string p1, "0"

    .line 266
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_8
.end method

.method private d()J
    .registers 3

    .prologue
    .line 196
    iget-wide v0, p0, Lcom/a/a/a/a/b;->u:J

    return-wide v0
.end method

.method private d(I)J
    .registers 4

    .prologue
    .line 334
    iget-object v0, p0, Lcom/a/a/a/a/b;->A:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge p1, v0, :cond_15

    .line 335
    iget-object v0, p0, Lcom/a/a/a/a/b;->A:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 337
    :goto_14
    return-wide v0

    :cond_15
    const-wide/16 v0, -0x1

    goto :goto_14
.end method

.method private static d(Ljava/lang/String;)Ljava/util/Map;
    .registers 10

    .prologue
    const/4 v1, 0x0

    .line 375
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 377
    :try_start_6
    const-string v0, "&"

    invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 378
    array-length v6, v5

    move v3, v1

    :goto_e
    if-ge v3, v6, :cond_4c

    aget-object v0, v5, v3

    .line 379
    const-string v2, "="

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    .line 380
    const/4 v0, 0x0

    aget-object v0, v7, v0

    move-object v2, v0

    move v0, v1

    .line 382
    :goto_1d
    invoke-interface {v4, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3a

    .line 383
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v8, 0x0

    aget-object v8, v7, v8

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_1d

    .line 385
    :cond_3a
    const/4 v0, 0x1

    aget-object v0, v7, v0

    invoke-interface {v4, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_40
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_40} :catch_44

    .line 378
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_e

    .line 388
    :catch_44
    move-exception v0

    const-string v0, "APKExpansionPolicy"

    const-string v1, "Invalid syntax error while decoding extras data from server."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 390
    :cond_4c
    return-object v4
.end method

.method private e()J
    .registers 3

    .prologue
    .line 222
    iget-wide v0, p0, Lcom/a/a/a/a/b;->r:J

    return-wide v0
.end method

.method private f()J
    .registers 3

    .prologue
    .line 248
    iget-wide v0, p0, Lcom/a/a/a/a/b;->s:J

    return-wide v0
.end method

.method private g()J
    .registers 3

    .prologue
    .line 274
    iget-wide v0, p0, Lcom/a/a/a/a/b;->t:J

    return-wide v0
.end method

.method private h()I
    .registers 2

    .prologue
    .line 285
    iget-object v0, p0, Lcom/a/a/a/a/b;->y:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(ILcom/a/a/a/a/u;)V
    .registers 11

    .prologue
    const/16 v7, 0x9

    .line 130
    const/16 v0, 0x123

    if-eq p1, v0, :cond_4c

    .line 131
    const-wide/16 v0, 0x0

    invoke-direct {p0, v0, v1}, Lcom/a/a/a/a/b;->a(J)V

    .line 136
    :goto_b
    const/16 v0, 0x100

    if-ne p1, v0, :cond_10c

    .line 138
    iget-object v0, p2, Lcom/a/a/a/a/u;->g:Ljava/lang/String;

    invoke-static {v0}, Lcom/a/a/a/a/b;->d(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v1

    .line 139
    iput p1, p0, Lcom/a/a/a/a/b;->w:I

    .line 140
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/32 v4, 0xea60

    add-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/a/a/a/a/b;->a(Ljava/lang/String;)V

    .line 141
    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    .line 142
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2e
    :goto_2e
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_11f

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 143
    const-string v3, "VT"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_55

    .line 144
    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/a/a/a/a/b;->a(Ljava/lang/String;)V

    goto :goto_2e

    .line 133
    :cond_4c
    iget-wide v0, p0, Lcom/a/a/a/a/b;->u:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    invoke-direct {p0, v0, v1}, Lcom/a/a/a/a/b;->a(J)V

    goto :goto_b

    .line 145
    :cond_55
    const-string v3, "GT"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_67

    .line 146
    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/a/a/a/a/b;->b(Ljava/lang/String;)V

    goto :goto_2e

    .line 147
    :cond_67
    const-string v3, "GR"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_79

    .line 148
    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/a/a/a/a/b;->c(Ljava/lang/String;)V

    goto :goto_2e

    .line 149
    :cond_79
    const-string v3, "FILE_URL"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_a8

    .line 150
    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    .line 151
    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1313
    iget-object v4, p0, Lcom/a/a/a/a/b;->y:Ljava/util/Vector;

    invoke-virtual {v4}, Ljava/util/Vector;->size()I

    move-result v4

    if-lt v3, v4, :cond_a2

    .line 1314
    iget-object v4, p0, Lcom/a/a/a/a/b;->y:Ljava/util/Vector;

    add-int/lit8 v5, v3, 0x1

    invoke-virtual {v4, v5}, Ljava/util/Vector;->setSize(I)V

    .line 1316
    :cond_a2
    iget-object v4, p0, Lcom/a/a/a/a/b;->y:Ljava/util/Vector;

    invoke-virtual {v4, v3, v0}, Ljava/util/Vector;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_2e

    .line 152
    :cond_a8
    const-string v3, "FILE_NAME"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_d6

    .line 153
    invoke-virtual {v0, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    .line 154
    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1327
    iget-object v4, p0, Lcom/a/a/a/a/b;->z:Ljava/util/Vector;

    invoke-virtual {v4}, Ljava/util/Vector;->size()I

    move-result v4

    if-lt v3, v4, :cond_cf

    .line 1328
    iget-object v4, p0, Lcom/a/a/a/a/b;->z:Ljava/util/Vector;

    add-int/lit8 v5, v3, 0x1

    invoke-virtual {v4, v5}, Ljava/util/Vector;->setSize(I)V

    .line 1330
    :cond_cf
    iget-object v4, p0, Lcom/a/a/a/a/b;->z:Ljava/util/Vector;

    invoke-virtual {v4, v3, v0}, Ljava/util/Vector;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2e

    .line 155
    :cond_d6
    const-string v3, "FILE_SIZE"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2e

    .line 156
    invoke-virtual {v0, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    .line 157
    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 1341
    iget-object v0, p0, Lcom/a/a/a/a/b;->A:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-lt v3, v0, :cond_101

    .line 1342
    iget-object v0, p0, Lcom/a/a/a/a/b;->A:Ljava/util/Vector;

    add-int/lit8 v6, v3, 0x1

    invoke-virtual {v0, v6}, Ljava/util/Vector;->setSize(I)V

    .line 1344
    :cond_101
    iget-object v0, p0, Lcom/a/a/a/a/b;->A:Ljava/util/Vector;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Ljava/util/Vector;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2e

    .line 160
    :cond_10c
    const/16 v0, 0x231

    if-ne p1, v0, :cond_11f

    .line 162
    const-string v0, "0"

    invoke-direct {p0, v0}, Lcom/a/a/a/a/b;->a(Ljava/lang/String;)V

    .line 163
    const-string v0, "0"

    invoke-direct {p0, v0}, Lcom/a/a/a/a/b;->b(Ljava/lang/String;)V

    .line 164
    const-string v0, "0"

    invoke-direct {p0, v0}, Lcom/a/a/a/a/b;->c(Ljava/lang/String;)V

    .line 2179
    :cond_11f
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/a/a/a/a/b;->v:J

    .line 2180
    iput p1, p0, Lcom/a/a/a/a/b;->w:I

    .line 2181
    iget-object v0, p0, Lcom/a/a/a/a/b;->x:Lcom/a/a/a/a/t;

    const-string v1, "lastResponse"

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/a/a/a/a/t;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    iget-object v0, p0, Lcom/a/a/a/a/b;->x:Lcom/a/a/a/a/t;

    invoke-virtual {v0}, Lcom/a/a/a/a/t;->a()V

    .line 169
    return-void
.end method

.method public final a(Lcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V
    .registers 4

    .prologue
    .line 403
    return-void
.end method

.method public final a()Z
    .registers 9

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 356
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 357
    iget v4, p0, Lcom/a/a/a/a/b;->w:I

    const/16 v5, 0x100

    if-ne v4, v5, :cond_13

    .line 360
    iget-wide v4, p0, Lcom/a/a/a/a/b;->r:J

    cmp-long v2, v2, v4

    if-gtz v2, :cond_33

    .line 371
    :cond_12
    :goto_12
    return v0

    .line 364
    :cond_13
    iget v4, p0, Lcom/a/a/a/a/b;->w:I

    const/16 v5, 0x123

    if-ne v4, v5, :cond_33

    iget-wide v4, p0, Lcom/a/a/a/a/b;->v:J

    const-wide/32 v6, 0xea60

    add-long/2addr v4, v6

    cmp-long v4, v2, v4

    if-gez v4, :cond_33

    .line 369
    iget-wide v4, p0, Lcom/a/a/a/a/b;->s:J

    cmp-long v2, v2, v4

    if-lez v2, :cond_12

    iget-wide v2, p0, Lcom/a/a/a/a/b;->u:J

    iget-wide v4, p0, Lcom/a/a/a/a/b;->t:J

    cmp-long v2, v2, v4

    if-lez v2, :cond_12

    move v0, v1

    goto :goto_12

    :cond_33
    move v0, v1

    .line 371
    goto :goto_12
.end method

.method public final b()Lcom/a/a/a/a/w;
    .registers 2

    .prologue
    .line 396
    const/4 v0, 0x0

    return-object v0
.end method
