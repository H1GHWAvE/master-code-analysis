.class final Lcom/a/a/a/a/p;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final e:Ljava/lang/String; = "LicenseValidator"

.field private static final f:I = 0x0

.field private static final g:I = 0x1

.field private static final h:I = 0x2

.field private static final i:I = 0x3

.field private static final j:I = 0x4

.field private static final k:I = 0x5

.field private static final l:I = 0x101

.field private static final m:I = 0x102

.field private static final n:I = 0x103

.field private static final q:Ljava/lang/String; = "SHA1withRSA"


# instance fields
.field final a:Lcom/a/a/a/a/o;

.field final b:I

.field final c:Ljava/lang/String;

.field final d:Ljava/lang/String;

.field private final o:Lcom/a/a/a/a/s;

.field private final p:Lcom/a/a/a/a/d;


# direct methods
.method constructor <init>(Lcom/a/a/a/a/s;Lcom/a/a/a/a/d;Lcom/a/a/a/a/o;ILjava/lang/String;Ljava/lang/String;)V
    .registers 7

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    iput-object p1, p0, Lcom/a/a/a/a/p;->o:Lcom/a/a/a/a/s;

    .line 62
    iput-object p2, p0, Lcom/a/a/a/a/p;->p:Lcom/a/a/a/a/d;

    .line 63
    iput-object p3, p0, Lcom/a/a/a/a/p;->a:Lcom/a/a/a/a/o;

    .line 64
    iput p4, p0, Lcom/a/a/a/a/p;->b:I

    .line 65
    iput-object p5, p0, Lcom/a/a/a/a/p;->c:Ljava/lang/String;

    .line 66
    iput-object p6, p0, Lcom/a/a/a/a/p;->d:Ljava/lang/String;

    .line 67
    return-void
.end method

.method private a(Ljava/security/PublicKey;ILjava/lang/String;Ljava/lang/String;)V
    .registers 12

    .prologue
    .line 98
    new-instance v0, Ljava/util/zip/CRC32;

    invoke-direct {v0}, Ljava/util/zip/CRC32;-><init>()V

    .line 99
    const/16 v1, 0x103

    if-ne p2, v1, :cond_40

    .line 100
    add-int/lit8 v1, p2, 0x1

    invoke-virtual {v0, v1}, Ljava/util/zip/CRC32;->update(I)V

    .line 103
    :goto_e
    invoke-virtual {v0}, Ljava/util/zip/CRC32;->getValue()J

    move-result-wide v2

    .line 106
    const/4 v0, 0x0

    .line 107
    if-eqz p2, :cond_1b

    const/4 v1, 0x1

    if-eq p2, v1, :cond_1b

    const/4 v1, 0x2

    if-ne p2, v1, :cond_141

    .line 111
    :cond_1b
    :try_start_1b
    const-string v0, "SHA1withRSA"

    invoke-static {v0}, Ljava/security/Signature;->getInstance(Ljava/lang/String;)Ljava/security/Signature;

    move-result-object v0

    .line 112
    invoke-virtual {v0, p1}, Ljava/security/Signature;->initVerify(Ljava/security/PublicKey;)V

    .line 113
    invoke-virtual {p3}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/security/Signature;->update([B)V

    .line 115
    invoke-static {p4}, Lcom/a/a/a/a/a/a;->a(Ljava/lang/String;)[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/security/Signature;->verify([B)Z

    move-result v0

    if-nez v0, :cond_64

    .line 116
    const-string v0, "LicenseValidator"

    const-string v1, "Signature verification failed."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 117
    invoke-virtual {p0}, Lcom/a/a/a/a/p;->a()V
    :try_end_3f
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_1b .. :try_end_3f} :catch_44
    .catch Ljava/security/InvalidKeyException; {:try_start_1b .. :try_end_3f} :catch_4b
    .catch Ljava/security/SignatureException; {:try_start_1b .. :try_end_3f} :catch_51
    .catch Lcom/a/a/a/a/a/b; {:try_start_1b .. :try_end_3f} :catch_58

    .line 377
    :goto_3f
    return-void

    .line 102
    :cond_40
    invoke-virtual {v0, p2}, Ljava/util/zip/CRC32;->update(I)V

    goto :goto_e

    .line 120
    :catch_44
    move-exception v0

    .line 122
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 124
    :catch_4b
    move-exception v0

    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lcom/a/a/a/a/p;->a(I)V

    goto :goto_3f

    .line 126
    :catch_51
    move-exception v0

    .line 127
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 129
    :catch_58
    move-exception v0

    const-string v0, "LicenseValidator"

    const-string v1, "Could not Base64-decode signature."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 130
    invoke-virtual {p0}, Lcom/a/a/a/a/p;->a()V

    goto :goto_3f

    .line 1046
    :cond_64
    const/16 v0, 0x3a

    :try_start_66
    invoke-virtual {p3, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    .line 1048
    const/4 v1, -0x1

    if-ne v1, v0, :cond_93

    .line 1050
    const-string v0, ""

    move-object v1, v0

    move-object v0, p3

    .line 1056
    :goto_71
    const-string v4, "|"

    invoke-static {v4}, Ljava/util/regex/Pattern;->quote(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/text/TextUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 1057
    array-length v0, v4

    const/4 v5, 0x6

    if-ge v0, v5, :cond_ae

    .line 1058
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Wrong number of fields."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_87
    .catch Ljava/lang/IllegalArgumentException; {:try_start_66 .. :try_end_87} :catch_87

    .line 138
    :catch_87
    move-exception v0

    const-string v0, "LicenseValidator"

    const-string v1, "Could not parse response."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 139
    invoke-virtual {p0}, Lcom/a/a/a/a/p;->a()V

    goto :goto_3f

    .line 1052
    :cond_93
    const/4 v1, 0x0

    :try_start_94
    invoke-virtual {p3, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 1053
    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v4

    if-lt v0, v4, :cond_a4

    const-string v0, ""

    move-object v6, v0

    move-object v0, v1

    move-object v1, v6

    goto :goto_71

    :cond_a4
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p3, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    move-object v6, v0

    move-object v0, v1

    move-object v1, v6

    goto :goto_71

    .line 1061
    :cond_ae
    new-instance v0, Lcom/a/a/a/a/u;

    invoke-direct {v0}, Lcom/a/a/a/a/u;-><init>()V

    .line 1062
    iput-object v1, v0, Lcom/a/a/a/a/u;->g:Ljava/lang/String;

    .line 1063
    const/4 v1, 0x0

    aget-object v1, v4, v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    iput v1, v0, Lcom/a/a/a/a/u;->a:I

    .line 1064
    const/4 v1, 0x1

    aget-object v1, v4, v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    iput v1, v0, Lcom/a/a/a/a/u;->b:I

    .line 1065
    const/4 v1, 0x2

    aget-object v1, v4, v1

    iput-object v1, v0, Lcom/a/a/a/a/u;->c:Ljava/lang/String;

    .line 1066
    const/4 v1, 0x3

    aget-object v1, v4, v1

    iput-object v1, v0, Lcom/a/a/a/a/u;->d:Ljava/lang/String;

    .line 1068
    const/4 v1, 0x4

    aget-object v1, v4, v1

    iput-object v1, v0, Lcom/a/a/a/a/u;->e:Ljava/lang/String;

    .line 1069
    const/4 v1, 0x5

    aget-object v1, v4, v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    iput-wide v4, v0, Lcom/a/a/a/a/u;->f:J
    :try_end_df
    .catch Ljava/lang/IllegalArgumentException; {:try_start_94 .. :try_end_df} :catch_87

    .line 143
    iget v1, v0, Lcom/a/a/a/a/u;->a:I

    if-eq v1, p2, :cond_ef

    .line 144
    const-string v0, "LicenseValidator"

    const-string v1, "Response codes don\'t match."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 145
    invoke-virtual {p0}, Lcom/a/a/a/a/p;->a()V

    goto/16 :goto_3f

    .line 149
    :cond_ef
    iget v1, v0, Lcom/a/a/a/a/u;->b:I

    iget v4, p0, Lcom/a/a/a/a/p;->b:I

    if-eq v1, v4, :cond_101

    .line 150
    const-string v0, "LicenseValidator"

    const-string v1, "Nonce doesn\'t match."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 151
    invoke-virtual {p0}, Lcom/a/a/a/a/p;->a()V

    goto/16 :goto_3f

    .line 155
    :cond_101
    iget-object v1, v0, Lcom/a/a/a/a/u;->c:Ljava/lang/String;

    iget-object v4, p0, Lcom/a/a/a/a/p;->c:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_117

    .line 156
    const-string v0, "LicenseValidator"

    const-string v1, "Package name doesn\'t match."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 157
    invoke-virtual {p0}, Lcom/a/a/a/a/p;->a()V

    goto/16 :goto_3f

    .line 161
    :cond_117
    iget-object v1, v0, Lcom/a/a/a/a/u;->d:Ljava/lang/String;

    iget-object v4, p0, Lcom/a/a/a/a/p;->d:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_12d

    .line 162
    const-string v0, "LicenseValidator"

    const-string v1, "Version codes don\'t match."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 163
    invoke-virtual {p0}, Lcom/a/a/a/a/p;->a()V

    goto/16 :goto_3f

    .line 168
    :cond_12d
    iget-object v1, v0, Lcom/a/a/a/a/u;->e:Ljava/lang/String;

    .line 169
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_141

    .line 170
    const-string v0, "LicenseValidator"

    const-string v1, "User identifier is empty."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 171
    invoke-virtual {p0}, Lcom/a/a/a/a/p;->a()V

    goto/16 :goto_3f

    .line 176
    :cond_141
    const-wide v4, 0xd202ef8dL

    cmp-long v1, v2, v4

    if-nez v1, :cond_151

    .line 178
    const/16 v1, 0x100

    invoke-virtual {p0, v1, v0, p3, p4}, Lcom/a/a/a/a/p;->a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3f

    .line 182
    :cond_151
    const-wide v4, 0xd203169dL

    cmp-long v1, v2, v4

    if-nez v1, :cond_161

    .line 184
    const/16 v1, 0x100

    invoke-virtual {p0, v1, v0, p3, p4}, Lcom/a/a/a/a/p;->a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3f

    .line 188
    :cond_161
    const-wide v4, 0xd20316bbL

    cmp-long v1, v2, v4

    if-nez v1, :cond_171

    .line 189
    const/16 v1, 0x231

    invoke-virtual {p0, v1, v0, p3, p4}, Lcom/a/a/a/a/p;->a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3f

    .line 193
    :cond_171
    const-wide v4, 0x96684c9dL

    cmp-long v1, v2, v4

    if-nez v1, :cond_180

    .line 194
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/a/a/a/a/p;->a(I)V

    goto/16 :goto_3f

    .line 198
    :cond_180
    const-wide/32 v4, 0x3c0c8ea1

    cmp-long v1, v2, v4

    if-nez v1, :cond_18e

    .line 200
    const/16 v1, 0x100

    invoke-virtual {p0, v1, v0, p3, p4}, Lcom/a/a/a/a/p;->a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3f

    .line 204
    :cond_18e
    const-wide/32 v4, 0x3d3dbba1

    cmp-long v1, v2, v4

    if-nez v1, :cond_19c

    .line 206
    const/16 v1, 0x123

    invoke-virtual {p0, v1, v0, p3, p4}, Lcom/a/a/a/a/p;->a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3f

    .line 210
    :cond_19c
    const-wide/32 v4, 0x3d3db949

    cmp-long v1, v2, v4

    if-nez v1, :cond_1aa

    .line 212
    const/16 v1, 0x100

    invoke-virtual {p0, v1, v0, p3, p4}, Lcom/a/a/a/a/p;->a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3f

    .line 216
    :cond_1aa
    const-wide/32 v4, 0x43339ca1

    cmp-long v1, v2, v4

    if-nez v1, :cond_1b8

    .line 218
    const/16 v1, 0x100

    invoke-virtual {p0, v1, v0, p3, p4}, Lcom/a/a/a/a/p;->a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3f

    .line 222
    :cond_1b8
    const-wide/32 v4, 0x3c0c8ea1

    cmp-long v1, v2, v4

    if-nez v1, :cond_1c6

    .line 223
    const/16 v1, 0x231

    invoke-virtual {p0, v1, v0, p3, p4}, Lcom/a/a/a/a/p;->a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3f

    .line 227
    :cond_1c6
    const-wide/32 v4, 0x3c0c8efb

    cmp-long v1, v2, v4

    if-nez v1, :cond_1d4

    .line 228
    const/16 v1, 0x231

    invoke-virtual {p0, v1, v0, p3, p4}, Lcom/a/a/a/a/p;->a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3f

    .line 232
    :cond_1d4
    const-wide v4, 0x25e73701bL

    cmp-long v1, v2, v4

    if-nez v1, :cond_1e4

    .line 234
    const/16 v1, 0x123

    invoke-virtual {p0, v1, v0, p3, p4}, Lcom/a/a/a/a/p;->a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3f

    .line 238
    :cond_1e4
    const-wide/32 v4, 0x4dee313d

    cmp-long v1, v2, v4

    if-nez v1, :cond_1f2

    .line 239
    const/16 v1, 0x231

    invoke-virtual {p0, v1, v0, p3, p4}, Lcom/a/a/a/a/p;->a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3f

    .line 243
    :cond_1f2
    const-wide v4, 0xa505df1bL

    cmp-long v1, v2, v4

    if-nez v1, :cond_202

    .line 245
    const/16 v1, 0x123

    invoke-virtual {p0, v1, v0, p3, p4}, Lcom/a/a/a/a/p;->a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3f

    .line 249
    :cond_202
    const-wide v4, 0x873885d3L

    cmp-long v1, v2, v4

    if-nez v1, :cond_211

    .line 250
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/a/a/a/a/p;->a(I)V

    goto/16 :goto_3f

    .line 254
    :cond_211
    const-wide v4, 0x8142a4d3L

    cmp-long v1, v2, v4

    if-nez v1, :cond_221

    .line 256
    const/16 v1, 0x123

    invoke-virtual {p0, v1, v0, p3, p4}, Lcom/a/a/a/a/p;->a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3f

    .line 260
    :cond_221
    const-wide v4, 0xa506d533L

    cmp-long v1, v2, v4

    if-nez v1, :cond_231

    .line 262
    const/16 v1, 0x123

    invoke-virtual {p0, v1, v0, p3, p4}, Lcom/a/a/a/a/p;->a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3f

    .line 266
    :cond_231
    const-wide v4, 0xd56f2b94L

    cmp-long v1, v2, v4

    if-nez v1, :cond_241

    .line 268
    const/16 v1, 0x123

    invoke-virtual {p0, v1, v0, p3, p4}, Lcom/a/a/a/a/p;->a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3f

    .line 272
    :cond_241
    const-wide/32 v4, 0x5e399794

    cmp-long v1, v2, v4

    if-nez v1, :cond_24f

    .line 274
    const/16 v1, 0x100

    invoke-virtual {p0, v1, v0, p3, p4}, Lcom/a/a/a/a/p;->a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3f

    .line 278
    :cond_24f
    const-wide v4, 0xd5691114L

    cmp-long v1, v2, v4

    if-nez v1, :cond_25f

    .line 280
    const/16 v1, 0x123

    invoke-virtual {p0, v1, v0, p3, p4}, Lcom/a/a/a/a/p;->a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3f

    .line 284
    :cond_25f
    const-wide v4, 0xd56e4134L

    cmp-long v1, v2, v4

    if-nez v1, :cond_26f

    .line 286
    const/16 v1, 0x123

    invoke-virtual {p0, v1, v0, p3, p4}, Lcom/a/a/a/a/p;->a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3f

    .line 290
    :cond_26f
    const-wide v4, 0xa2681b02L

    cmp-long v1, v2, v4

    if-nez v1, :cond_27f

    .line 292
    const/16 v1, 0x123

    invoke-virtual {p0, v1, v0, p3, p4}, Lcom/a/a/a/a/p;->a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3f

    .line 296
    :cond_27f
    const-wide v4, 0xa2681adcL

    cmp-long v1, v2, v4

    if-nez v1, :cond_28e

    .line 297
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/a/a/a/a/p;->a(I)V

    goto/16 :goto_3f

    .line 301
    :cond_28e
    const-wide v4, 0x908677dcL

    cmp-long v1, v2, v4

    if-nez v1, :cond_29e

    .line 303
    const/16 v1, 0x123

    invoke-virtual {p0, v1, v0, p3, p4}, Lcom/a/a/a/a/p;->a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3f

    .line 307
    :cond_29e
    const-wide v4, 0x8a9096dcL

    cmp-long v1, v2, v4

    if-nez v1, :cond_2ae

    .line 309
    const/16 v1, 0x123

    invoke-virtual {p0, v1, v0, p3, p4}, Lcom/a/a/a/a/p;->a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3f

    .line 313
    :cond_2ae
    const-wide/32 v4, 0x3c0c8ea1

    cmp-long v1, v2, v4

    if-nez v1, :cond_2bc

    .line 315
    const/16 v1, 0x123

    invoke-virtual {p0, v1, v0, p3, p4}, Lcom/a/a/a/a/p;->a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3f

    .line 319
    :cond_2bc
    const-wide/32 v4, 0x3c0c9e41

    cmp-long v1, v2, v4

    if-nez v1, :cond_2c9

    .line 320
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/a/a/a/a/p;->a(I)V

    goto/16 :goto_3f

    .line 324
    :cond_2c9
    const-wide/32 v4, 0x47f850a1

    cmp-long v1, v2, v4

    if-nez v1, :cond_2d5

    .line 326
    const/16 v1, 0x100

    invoke-virtual {p0, v1, v0, p3, p4}, Lcom/a/a/a/a/p;->a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V

    .line 329
    :cond_2d5
    const-wide/32 v4, 0x3dd65221

    cmp-long v1, v2, v4

    if-nez v1, :cond_2e2

    .line 330
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/a/a/a/a/p;->a(I)V

    goto/16 :goto_3f

    .line 334
    :cond_2e2
    const-wide v4, 0xd56f2b94L

    cmp-long v1, v2, v4

    if-nez v1, :cond_2f1

    .line 335
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/a/a/a/a/p;->a(I)V

    goto/16 :goto_3f

    .line 339
    :cond_2f1
    const-wide/32 v4, 0x4addf777

    cmp-long v1, v2, v4

    if-nez v1, :cond_2ff

    .line 340
    const/16 v1, 0x231

    invoke-virtual {p0, v1, v0, p3, p4}, Lcom/a/a/a/a/p;->a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3f

    .line 344
    :cond_2ff
    const-wide/32 v4, 0x4b0bc607

    cmp-long v1, v2, v4

    if-nez v1, :cond_30c

    .line 345
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/a/a/a/a/p;->a(I)V

    goto/16 :goto_3f

    .line 349
    :cond_30c
    const-wide/32 v4, 0x4b0bbde7

    cmp-long v1, v2, v4

    if-nez v1, :cond_318

    .line 351
    const/16 v1, 0x100

    invoke-virtual {p0, v1, v0, p3, p4}, Lcom/a/a/a/a/p;->a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V

    .line 354
    :cond_318
    const-wide/32 v4, 0x4b0bbe37

    cmp-long v1, v2, v4

    if-nez v1, :cond_325

    .line 355
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/a/a/a/a/p;->a(I)V

    goto/16 :goto_3f

    .line 359
    :cond_325
    const-wide/32 v4, 0x4a7327b7

    cmp-long v1, v2, v4

    if-nez v1, :cond_332

    .line 360
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/a/a/a/a/p;->a(I)V

    goto/16 :goto_3f

    .line 364
    :cond_332
    const-wide/32 v4, 0x4b0be0fb

    cmp-long v1, v2, v4

    if-nez v1, :cond_33f

    .line 365
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/a/a/a/a/p;->a(I)V

    goto/16 :goto_3f

    .line 369
    :cond_33f
    const-wide/32 v4, 0x4ab03085

    cmp-long v1, v2, v4

    if-nez v1, :cond_34d

    .line 370
    const/16 v1, 0x231

    invoke-virtual {p0, v1, v0, p3, p4}, Lcom/a/a/a/a/p;->a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3f

    .line 375
    :cond_34d
    const-string v0, "LicenseValidator"

    const-string v1, "Unknown response code for license check."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 376
    invoke-virtual {p0}, Lcom/a/a/a/a/p;->a()V

    goto/16 :goto_3f
.end method

.method private b()Lcom/a/a/a/a/o;
    .registers 2

    .prologue
    .line 70
    iget-object v0, p0, Lcom/a/a/a/a/p;->a:Lcom/a/a/a/a/o;

    return-object v0
.end method

.method private c()I
    .registers 2

    .prologue
    .line 74
    iget v0, p0, Lcom/a/a/a/a/p;->b:I

    return v0
.end method

.method private d()Ljava/lang/String;
    .registers 2

    .prologue
    .line 78
    iget-object v0, p0, Lcom/a/a/a/a/p;->c:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method final a()V
    .registers 3

    .prologue
    .line 406
    iget-object v0, p0, Lcom/a/a/a/a/p;->a:Lcom/a/a/a/a/o;

    const/16 v1, 0x231

    invoke-interface {v0, v1}, Lcom/a/a/a/a/o;->a(I)V

    .line 407
    return-void
.end method

.method final a(I)V
    .registers 3

    .prologue
    .line 402
    iget-object v0, p0, Lcom/a/a/a/a/p;->a:Lcom/a/a/a/a/o;

    invoke-interface {v0, p1}, Lcom/a/a/a/a/o;->b(I)V

    .line 403
    return-void
.end method

.method final a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V
    .registers 6

    .prologue
    .line 387
    iget-object v0, p0, Lcom/a/a/a/a/p;->o:Lcom/a/a/a/a/s;

    invoke-interface {v0, p1, p2}, Lcom/a/a/a/a/s;->a(ILcom/a/a/a/a/u;)V

    .line 392
    iget-object v0, p0, Lcom/a/a/a/a/p;->o:Lcom/a/a/a/a/s;

    invoke-interface {v0}, Lcom/a/a/a/a/s;->a()Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 393
    iget-object v0, p0, Lcom/a/a/a/a/p;->a:Lcom/a/a/a/a/o;

    invoke-interface {v0}, Lcom/a/a/a/a/o;->a()V

    .line 394
    iget-object v0, p0, Lcom/a/a/a/a/p;->a:Lcom/a/a/a/a/o;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/a/a/a/a/o;->a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V

    .line 395
    iget-object v0, p0, Lcom/a/a/a/a/p;->o:Lcom/a/a/a/a/s;

    invoke-interface {v0, p2, p3, p4}, Lcom/a/a/a/a/s;->a(Lcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V

    .line 399
    :goto_1c
    return-void

    .line 397
    :cond_1d
    iget-object v0, p0, Lcom/a/a/a/a/p;->a:Lcom/a/a/a/a/o;

    invoke-interface {v0, p1}, Lcom/a/a/a/a/o;->a(I)V

    goto :goto_1c
.end method
