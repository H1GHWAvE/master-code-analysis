.class public final Lcom/a/c/ab;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Lcom/a/c/d/a;)Lcom/a/c/w;
    .registers 6

    .prologue
    .line 81
    .line 1333
    iget-boolean v1, p0, Lcom/a/c/d/a;->b:Z

    .line 2326
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/a/c/d/a;->b:Z

    .line 84
    :try_start_5
    invoke-static {p0}, Lcom/a/c/b/aq;->a(Lcom/a/c/d/a;)Lcom/a/c/w;
    :try_end_8
    .catch Ljava/lang/StackOverflowError; {:try_start_5 .. :try_end_8} :catch_c
    .catch Ljava/lang/OutOfMemoryError; {:try_start_5 .. :try_end_8} :catch_2c
    .catchall {:try_start_5 .. :try_end_8} :catchall_28

    move-result-object v0

    .line 3326
    iput-boolean v1, p0, Lcom/a/c/d/a;->b:Z

    .line 90
    return-object v0

    .line 85
    :catch_c
    move-exception v0

    .line 86
    :try_start_d
    new-instance v2, Lcom/a/c/aa;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Failed parsing JSON source: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " to Json"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v0}, Lcom/a/c/aa;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
    :try_end_28
    .catchall {:try_start_d .. :try_end_28} :catchall_28

    .line 90
    :catchall_28
    move-exception v0

    .line 4326
    iput-boolean v1, p0, Lcom/a/c/d/a;->b:Z

    .line 90
    throw v0

    .line 87
    :catch_2c
    move-exception v0

    .line 88
    :try_start_2d
    new-instance v2, Lcom/a/c/aa;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Failed parsing JSON source: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " to Json"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v0}, Lcom/a/c/aa;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
    :try_end_48
    .catchall {:try_start_2d .. :try_end_48} :catchall_28
.end method

.method private static a(Ljava/io/Reader;)Lcom/a/c/w;
    .registers 4

    .prologue
    .line 58
    :try_start_0
    new-instance v0, Lcom/a/c/d/a;

    invoke-direct {v0, p0}, Lcom/a/c/d/a;-><init>(Ljava/io/Reader;)V

    .line 59
    invoke-static {v0}, Lcom/a/c/ab;->a(Lcom/a/c/d/a;)Lcom/a/c/w;

    move-result-object v1

    .line 1074
    instance-of v2, v1, Lcom/a/c/y;

    .line 60
    if-nez v2, :cond_32

    invoke-virtual {v0}, Lcom/a/c/d/a;->f()Lcom/a/c/d/d;

    move-result-object v0

    sget-object v2, Lcom/a/c/d/d;->j:Lcom/a/c/d/d;

    if-eq v0, v2, :cond_32

    .line 61
    new-instance v0, Lcom/a/c/ag;

    const-string v1, "Did not consume the entire document."

    invoke-direct {v0, v1}, Lcom/a/c/ag;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1d
    .catch Lcom/a/c/d/f; {:try_start_0 .. :try_end_1d} :catch_1d
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_1d} :catch_24
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_1d} :catch_2b

    .line 64
    :catch_1d
    move-exception v0

    .line 65
    new-instance v1, Lcom/a/c/ag;

    invoke-direct {v1, v0}, Lcom/a/c/ag;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 66
    :catch_24
    move-exception v0

    .line 67
    new-instance v1, Lcom/a/c/x;

    invoke-direct {v1, v0}, Lcom/a/c/x;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 68
    :catch_2b
    move-exception v0

    .line 69
    new-instance v1, Lcom/a/c/ag;

    invoke-direct {v1, v0}, Lcom/a/c/ag;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 63
    :cond_32
    return-object v1
.end method

.method private static a(Ljava/lang/String;)Lcom/a/c/w;
    .registers 2

    .prologue
    .line 45
    new-instance v0, Ljava/io/StringReader;

    invoke-direct {v0, p0}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/a/c/ab;->a(Ljava/io/Reader;)Lcom/a/c/w;

    move-result-object v0

    return-object v0
.end method
