.class final Lcom/a/c/b/a/y;
.super Lcom/a/c/an;
.source "SourceFile"


# instance fields
.field private final a:Lcom/a/c/k;

.field private final b:Lcom/a/c/an;

.field private final c:Ljava/lang/reflect/Type;


# direct methods
.method constructor <init>(Lcom/a/c/k;Lcom/a/c/an;Ljava/lang/reflect/Type;)V
    .registers 4

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/a/c/an;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/a/c/b/a/y;->a:Lcom/a/c/k;

    .line 34
    iput-object p2, p0, Lcom/a/c/b/a/y;->b:Lcom/a/c/an;

    .line 35
    iput-object p3, p0, Lcom/a/c/b/a/y;->c:Ljava/lang/reflect/Type;

    .line 36
    return-void
.end method

.method private static a(Ljava/lang/reflect/Type;Ljava/lang/Object;)Ljava/lang/reflect/Type;
    .registers 3

    .prologue
    .line 75
    if-eqz p1, :cond_12

    const-class v0, Ljava/lang/Object;

    if-eq p0, v0, :cond_e

    instance-of v0, p0, Ljava/lang/reflect/TypeVariable;

    if-nez v0, :cond_e

    instance-of v0, p0, Ljava/lang/Class;

    if-eqz v0, :cond_12

    .line 77
    :cond_e
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p0

    .line 79
    :cond_12
    return-object p0
.end method


# virtual methods
.method public final a(Lcom/a/c/d/a;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 40
    iget-object v0, p0, Lcom/a/c/b/a/y;->b:Lcom/a/c/an;

    invoke-virtual {v0, p1}, Lcom/a/c/an;->a(Lcom/a/c/d/a;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/a/c/d/e;Ljava/lang/Object;)V
    .registers 6

    .prologue
    .line 52
    iget-object v1, p0, Lcom/a/c/b/a/y;->b:Lcom/a/c/an;

    .line 53
    iget-object v0, p0, Lcom/a/c/b/a/y;->c:Ljava/lang/reflect/Type;

    .line 1075
    if-eqz p2, :cond_16

    const-class v2, Ljava/lang/Object;

    if-eq v0, v2, :cond_12

    instance-of v2, v0, Ljava/lang/reflect/TypeVariable;

    if-nez v2, :cond_12

    instance-of v2, v0, Ljava/lang/Class;

    if-eqz v2, :cond_16

    .line 1077
    :cond_12
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    .line 54
    :cond_16
    iget-object v2, p0, Lcom/a/c/b/a/y;->c:Ljava/lang/reflect/Type;

    if-eq v0, v2, :cond_34

    .line 55
    iget-object v1, p0, Lcom/a/c/b/a/y;->a:Lcom/a/c/k;

    invoke-static {v0}, Lcom/a/c/c/a;->a(Ljava/lang/reflect/Type;)Lcom/a/c/c/a;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/a/c/k;->a(Lcom/a/c/c/a;)Lcom/a/c/an;

    move-result-object v0

    .line 56
    instance-of v1, v0, Lcom/a/c/b/a/s;

    if-eqz v1, :cond_30

    .line 59
    iget-object v1, p0, Lcom/a/c/b/a/y;->b:Lcom/a/c/an;

    instance-of v1, v1, Lcom/a/c/b/a/s;

    if-nez v1, :cond_30

    .line 62
    iget-object v0, p0, Lcom/a/c/b/a/y;->b:Lcom/a/c/an;

    .line 68
    :cond_30
    :goto_30
    invoke-virtual {v0, p1, p2}, Lcom/a/c/an;->a(Lcom/a/c/d/e;Ljava/lang/Object;)V

    .line 69
    return-void

    :cond_34
    move-object v0, v1

    goto :goto_30
.end method
