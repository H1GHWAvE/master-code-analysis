.class final Lcom/a/c/b/a/am;
.super Lcom/a/c/an;
.source "SourceFile"


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    .line 480
    invoke-direct {p0}, Lcom/a/c/an;-><init>()V

    return-void
.end method

.method private static a(Lcom/a/c/d/e;Ljava/net/InetAddress;)V
    .registers 3

    .prologue
    .line 492
    if-nez p1, :cond_7

    const/4 v0, 0x0

    :goto_3
    invoke-virtual {p0, v0}, Lcom/a/c/d/e;->b(Ljava/lang/String;)Lcom/a/c/d/e;

    .line 493
    return-void

    .line 492
    :cond_7
    invoke-virtual {p1}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v0

    goto :goto_3
.end method

.method private static b(Lcom/a/c/d/a;)Ljava/net/InetAddress;
    .registers 3

    .prologue
    .line 483
    invoke-virtual {p0}, Lcom/a/c/d/a;->f()Lcom/a/c/d/d;

    move-result-object v0

    sget-object v1, Lcom/a/c/d/d;->i:Lcom/a/c/d/d;

    if-ne v0, v1, :cond_d

    .line 484
    invoke-virtual {p0}, Lcom/a/c/d/a;->k()V

    .line 485
    const/4 v0, 0x0

    .line 488
    :goto_c
    return-object v0

    :cond_d
    invoke-virtual {p0}, Lcom/a/c/d/a;->i()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v0

    goto :goto_c
.end method


# virtual methods
.method public final synthetic a(Lcom/a/c/d/a;)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 480
    .line 1483
    invoke-virtual {p1}, Lcom/a/c/d/a;->f()Lcom/a/c/d/d;

    move-result-object v0

    sget-object v1, Lcom/a/c/d/d;->i:Lcom/a/c/d/d;

    if-ne v0, v1, :cond_d

    .line 1484
    invoke-virtual {p1}, Lcom/a/c/d/a;->k()V

    .line 1485
    const/4 v0, 0x0

    :goto_c
    return-object v0

    .line 1488
    :cond_d
    invoke-virtual {p1}, Lcom/a/c/d/a;->i()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v0

    goto :goto_c
.end method

.method public final synthetic a(Lcom/a/c/d/e;Ljava/lang/Object;)V
    .registers 4

    .prologue
    .line 480
    check-cast p2, Ljava/net/InetAddress;

    .line 1492
    if-nez p2, :cond_9

    const/4 v0, 0x0

    :goto_5
    invoke-virtual {p1, v0}, Lcom/a/c/d/e;->b(Ljava/lang/String;)Lcom/a/c/d/e;

    .line 480
    return-void

    .line 1492
    :cond_9
    invoke-virtual {p2}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v0

    goto :goto_5
.end method
