.class public final Lcom/a/c/b/a/s;
.super Lcom/a/c/an;
.source "SourceFile"


# instance fields
.field private final a:Lcom/a/c/b/ao;

.field private final b:Ljava/util/Map;


# direct methods
.method private constructor <init>(Lcom/a/c/b/ao;Ljava/util/Map;)V
    .registers 3

    .prologue
    .line 162
    invoke-direct {p0}, Lcom/a/c/an;-><init>()V

    .line 163
    iput-object p1, p0, Lcom/a/c/b/a/s;->a:Lcom/a/c/b/ao;

    .line 164
    iput-object p2, p0, Lcom/a/c/b/a/s;->b:Ljava/util/Map;

    .line 165
    return-void
.end method

.method synthetic constructor <init>(Lcom/a/c/b/ao;Ljava/util/Map;B)V
    .registers 4

    .prologue
    .line 158
    invoke-direct {p0, p1, p2}, Lcom/a/c/b/a/s;-><init>(Lcom/a/c/b/ao;Ljava/util/Map;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/a/c/d/a;)Ljava/lang/Object;
    .registers 5

    .prologue
    .line 168
    invoke-virtual {p1}, Lcom/a/c/d/a;->f()Lcom/a/c/d/d;

    move-result-object v0

    sget-object v1, Lcom/a/c/d/d;->i:Lcom/a/c/d/d;

    if-ne v0, v1, :cond_d

    .line 169
    invoke-virtual {p1}, Lcom/a/c/d/a;->k()V

    .line 170
    const/4 v0, 0x0

    .line 192
    :goto_c
    return-object v0

    .line 173
    :cond_d
    iget-object v0, p0, Lcom/a/c/b/a/s;->a:Lcom/a/c/b/ao;

    invoke-interface {v0}, Lcom/a/c/b/ao;->a()Ljava/lang/Object;

    move-result-object v1

    .line 176
    :try_start_13
    invoke-virtual {p1}, Lcom/a/c/d/a;->c()V

    .line 177
    :goto_16
    invoke-virtual {p1}, Lcom/a/c/d/a;->e()Z

    move-result v0

    if-eqz v0, :cond_44

    .line 178
    invoke-virtual {p1}, Lcom/a/c/d/a;->h()Ljava/lang/String;

    move-result-object v0

    .line 179
    iget-object v2, p0, Lcom/a/c/b/a/s;->b:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/c/b/a/t;

    .line 180
    if-eqz v0, :cond_2e

    iget-boolean v2, v0, Lcom/a/c/b/a/t;->i:Z

    if-nez v2, :cond_39

    .line 181
    :cond_2e
    invoke-virtual {p1}, Lcom/a/c/d/a;->o()V
    :try_end_31
    .catch Ljava/lang/IllegalStateException; {:try_start_13 .. :try_end_31} :catch_32
    .catch Ljava/lang/IllegalAccessException; {:try_start_13 .. :try_end_31} :catch_3d

    goto :goto_16

    .line 186
    :catch_32
    move-exception v0

    .line 187
    new-instance v1, Lcom/a/c/ag;

    invoke-direct {v1, v0}, Lcom/a/c/ag;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 183
    :cond_39
    :try_start_39
    invoke-virtual {v0, p1, v1}, Lcom/a/c/b/a/t;->a(Lcom/a/c/d/a;Ljava/lang/Object;)V
    :try_end_3c
    .catch Ljava/lang/IllegalStateException; {:try_start_39 .. :try_end_3c} :catch_32
    .catch Ljava/lang/IllegalAccessException; {:try_start_39 .. :try_end_3c} :catch_3d

    goto :goto_16

    .line 188
    :catch_3d
    move-exception v0

    .line 189
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 191
    :cond_44
    invoke-virtual {p1}, Lcom/a/c/d/a;->d()V

    move-object v0, v1

    .line 192
    goto :goto_c
.end method

.method public final a(Lcom/a/c/d/e;Ljava/lang/Object;)V
    .registers 6

    .prologue
    .line 196
    if-nez p2, :cond_6

    .line 197
    invoke-virtual {p1}, Lcom/a/c/d/e;->f()Lcom/a/c/d/e;

    .line 213
    :goto_5
    return-void

    .line 201
    :cond_6
    invoke-virtual {p1}, Lcom/a/c/d/e;->d()Lcom/a/c/d/e;

    .line 203
    :try_start_9
    iget-object v0, p0, Lcom/a/c/b/a/s;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_13
    :goto_13
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_33

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/c/b/a/t;

    .line 204
    iget-boolean v2, v0, Lcom/a/c/b/a/t;->h:Z

    if-eqz v2, :cond_13

    .line 205
    iget-object v2, v0, Lcom/a/c/b/a/t;->g:Ljava/lang/String;

    invoke-virtual {p1, v2}, Lcom/a/c/d/e;->a(Ljava/lang/String;)Lcom/a/c/d/e;

    .line 206
    invoke-virtual {v0, p1, p2}, Lcom/a/c/b/a/t;->a(Lcom/a/c/d/e;Ljava/lang/Object;)V
    :try_end_2b
    .catch Ljava/lang/IllegalAccessException; {:try_start_9 .. :try_end_2b} :catch_2c

    goto :goto_13

    .line 210
    :catch_2c
    move-exception v0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 212
    :cond_33
    invoke-virtual {p1}, Lcom/a/c/d/e;->e()Lcom/a/c/d/e;

    goto :goto_5
.end method
