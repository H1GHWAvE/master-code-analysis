.class final Lcom/a/c/b/aa;
.super Ljava/util/AbstractSet;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/a/c/b/w;


# direct methods
.method constructor <init>(Lcom/a/c/b/w;)V
    .registers 2

    .prologue
    .line 791
    iput-object p1, p0, Lcom/a/c/b/aa;->a:Lcom/a/c/b/w;

    invoke-direct {p0}, Ljava/util/AbstractSet;-><init>()V

    return-void
.end method


# virtual methods
.method public final clear()V
    .registers 2

    .prologue
    .line 822
    iget-object v0, p0, Lcom/a/c/b/aa;->a:Lcom/a/c/b/w;

    invoke-virtual {v0}, Lcom/a/c/b/w;->clear()V

    .line 823
    return-void
.end method

.method public final contains(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 805
    instance-of v0, p1, Ljava/util/Map$Entry;

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/a/c/b/aa;->a:Lcom/a/c/b/w;

    check-cast p1, Ljava/util/Map$Entry;

    invoke-virtual {v0, p1}, Lcom/a/c/b/w;->a(Ljava/util/Map$Entry;)Lcom/a/c/b/af;

    move-result-object v0

    if-eqz v0, :cond_10

    const/4 v0, 0x1

    :goto_f
    return v0

    :cond_10
    const/4 v0, 0x0

    goto :goto_f
.end method

.method public final iterator()Ljava/util/Iterator;
    .registers 2

    .prologue
    .line 797
    new-instance v0, Lcom/a/c/b/ab;

    invoke-direct {v0, p0}, Lcom/a/c/b/ab;-><init>(Lcom/a/c/b/aa;)V

    return-object v0
.end method

.method public final remove(Ljava/lang/Object;)Z
    .registers 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 809
    instance-of v2, p1, Ljava/util/Map$Entry;

    if-nez v2, :cond_7

    .line 818
    :cond_6
    :goto_6
    return v0

    .line 813
    :cond_7
    iget-object v2, p0, Lcom/a/c/b/aa;->a:Lcom/a/c/b/w;

    check-cast p1, Ljava/util/Map$Entry;

    invoke-virtual {v2, p1}, Lcom/a/c/b/w;->a(Ljava/util/Map$Entry;)Lcom/a/c/b/af;

    move-result-object v2

    .line 814
    if-eqz v2, :cond_6

    .line 817
    iget-object v0, p0, Lcom/a/c/b/aa;->a:Lcom/a/c/b/w;

    invoke-virtual {v0, v2, v1}, Lcom/a/c/b/w;->a(Lcom/a/c/b/af;Z)V

    move v0, v1

    .line 818
    goto :goto_6
.end method

.method public final size()I
    .registers 2

    .prologue
    .line 793
    iget-object v0, p0, Lcom/a/c/b/aa;->a:Lcom/a/c/b/w;

    iget v0, v0, Lcom/a/c/b/w;->d:I

    return v0
.end method
