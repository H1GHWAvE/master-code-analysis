.class final Lcom/a/c/b/e;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/lang/reflect/WildcardType;


# static fields
.field private static final c:J


# instance fields
.field private final a:Ljava/lang/reflect/Type;

.field private final b:Ljava/lang/reflect/Type;


# direct methods
.method public constructor <init>([Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)V
    .registers 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 536
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 537
    array-length v0, p2

    if-gt v0, v1, :cond_36

    move v0, v1

    :goto_9
    invoke-static {v0}, Lcom/a/c/b/a;->a(Z)V

    .line 538
    array-length v0, p1

    if-ne v0, v1, :cond_38

    move v0, v1

    :goto_10
    invoke-static {v0}, Lcom/a/c/b/a;->a(Z)V

    .line 540
    array-length v0, p2

    if-ne v0, v1, :cond_3c

    .line 541
    aget-object v0, p2, v2

    invoke-static {v0}, Lcom/a/c/b/a;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 542
    aget-object v0, p2, v2

    invoke-static {v0}, Lcom/a/c/b/b;->e(Ljava/lang/reflect/Type;)V

    .line 543
    aget-object v0, p1, v2

    const-class v3, Ljava/lang/Object;

    if-ne v0, v3, :cond_3a

    :goto_26
    invoke-static {v1}, Lcom/a/c/b/a;->a(Z)V

    .line 544
    aget-object v0, p2, v2

    invoke-static {v0}, Lcom/a/c/b/b;->a(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;

    move-result-object v0

    iput-object v0, p0, Lcom/a/c/b/e;->b:Ljava/lang/reflect/Type;

    .line 545
    const-class v0, Ljava/lang/Object;

    iput-object v0, p0, Lcom/a/c/b/e;->a:Ljava/lang/reflect/Type;

    .line 553
    :goto_35
    return-void

    :cond_36
    move v0, v2

    .line 537
    goto :goto_9

    :cond_38
    move v0, v2

    .line 538
    goto :goto_10

    :cond_3a
    move v1, v2

    .line 543
    goto :goto_26

    .line 548
    :cond_3c
    aget-object v0, p1, v2

    invoke-static {v0}, Lcom/a/c/b/a;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 549
    aget-object v0, p1, v2

    invoke-static {v0}, Lcom/a/c/b/b;->e(Ljava/lang/reflect/Type;)V

    .line 550
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/a/c/b/e;->b:Ljava/lang/reflect/Type;

    .line 551
    aget-object v0, p1, v2

    invoke-static {v0}, Lcom/a/c/b/b;->a(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;

    move-result-object v0

    iput-object v0, p0, Lcom/a/c/b/e;->a:Ljava/lang/reflect/Type;

    goto :goto_35
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 564
    instance-of v0, p1, Ljava/lang/reflect/WildcardType;

    if-eqz v0, :cond_e

    check-cast p1, Ljava/lang/reflect/WildcardType;

    .line 565
    invoke-static {p0, p1}, Lcom/a/c/b/b;->a(Ljava/lang/reflect/Type;Ljava/lang/reflect/Type;)Z

    move-result v0

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public final getLowerBounds()[Ljava/lang/reflect/Type;
    .registers 4

    .prologue
    .line 560
    iget-object v0, p0, Lcom/a/c/b/e;->b:Ljava/lang/reflect/Type;

    if-eqz v0, :cond_d

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/reflect/Type;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/a/c/b/e;->b:Ljava/lang/reflect/Type;

    aput-object v2, v0, v1

    :goto_c
    return-object v0

    :cond_d
    sget-object v0, Lcom/a/c/b/b;->a:[Ljava/lang/reflect/Type;

    goto :goto_c
.end method

.method public final getUpperBounds()[Ljava/lang/reflect/Type;
    .registers 4

    .prologue
    .line 556
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/reflect/Type;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/a/c/b/e;->a:Ljava/lang/reflect/Type;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final hashCode()I
    .registers 3

    .prologue
    .line 570
    iget-object v0, p0, Lcom/a/c/b/e;->b:Ljava/lang/reflect/Type;

    if-eqz v0, :cond_16

    iget-object v0, p0, Lcom/a/c/b/e;->b:Ljava/lang/reflect/Type;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    add-int/lit8 v0, v0, 0x1f

    :goto_c
    iget-object v1, p0, Lcom/a/c/b/e;->a:Ljava/lang/reflect/Type;

    .line 571
    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    add-int/lit8 v1, v1, 0x1f

    xor-int/2addr v0, v1

    return v0

    .line 570
    :cond_16
    const/4 v0, 0x1

    goto :goto_c
.end method

.method public final toString()Ljava/lang/String;
    .registers 3

    .prologue
    .line 575
    iget-object v0, p0, Lcom/a/c/b/e;->b:Ljava/lang/reflect/Type;

    if-eqz v0, :cond_1a

    .line 576
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "? super "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/a/c/b/e;->b:Ljava/lang/reflect/Type;

    invoke-static {v1}, Lcom/a/c/b/b;->c(Ljava/lang/reflect/Type;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 580
    :goto_19
    return-object v0

    .line 577
    :cond_1a
    iget-object v0, p0, Lcom/a/c/b/e;->a:Ljava/lang/reflect/Type;

    const-class v1, Ljava/lang/Object;

    if-ne v0, v1, :cond_23

    .line 578
    const-string v0, "?"

    goto :goto_19

    .line 580
    :cond_23
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "? extends "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/a/c/b/e;->a:Ljava/lang/reflect/Type;

    invoke-static {v1}, Lcom/a/c/b/b;->c(Ljava/lang/reflect/Type;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_19
.end method
