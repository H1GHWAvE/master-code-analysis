.class final Lcom/a/c/b/ak;
.super Ljava/util/AbstractSet;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/a/c/b/ag;


# direct methods
.method constructor <init>(Lcom/a/c/b/ag;)V
    .registers 2

    .prologue
    .line 592
    iput-object p1, p0, Lcom/a/c/b/ak;->a:Lcom/a/c/b/ag;

    invoke-direct {p0}, Ljava/util/AbstractSet;-><init>()V

    return-void
.end method


# virtual methods
.method public final clear()V
    .registers 2

    .prologue
    .line 614
    iget-object v0, p0, Lcom/a/c/b/ak;->a:Lcom/a/c/b/ag;

    invoke-virtual {v0}, Lcom/a/c/b/ag;->clear()V

    .line 615
    return-void
.end method

.method public final contains(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 606
    iget-object v0, p0, Lcom/a/c/b/ak;->a:Lcom/a/c/b/ag;

    invoke-virtual {v0, p1}, Lcom/a/c/b/ag;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final iterator()Ljava/util/Iterator;
    .registers 2

    .prologue
    .line 598
    new-instance v0, Lcom/a/c/b/al;

    invoke-direct {v0, p0}, Lcom/a/c/b/al;-><init>(Lcom/a/c/b/ak;)V

    return-object v0
.end method

.method public final remove(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 610
    iget-object v0, p0, Lcom/a/c/b/ak;->a:Lcom/a/c/b/ag;

    invoke-virtual {v0, p1}, Lcom/a/c/b/ag;->a(Ljava/lang/Object;)Lcom/a/c/b/an;

    move-result-object v0

    if-eqz v0, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public final size()I
    .registers 2

    .prologue
    .line 594
    iget-object v0, p0, Lcom/a/c/b/ak;->a:Lcom/a/c/b/ag;

    iget v0, v0, Lcom/a/c/b/ag;->c:I

    return v0
.end method
