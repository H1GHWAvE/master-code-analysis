.class public Lcom/a/c/d/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Closeable;


# static fields
.field private static final A:I = 0x5

.field private static final B:I = 0x6

.field private static final C:I = 0x7

.field private static final a:[C

.field private static final c:J = -0xcccccccccccccccL

.field private static final d:I = 0x0

.field private static final e:I = 0x1

.field private static final f:I = 0x2

.field private static final g:I = 0x3

.field private static final h:I = 0x4

.field private static final i:I = 0x5

.field private static final j:I = 0x6

.field private static final k:I = 0x7

.field private static final l:I = 0x8

.field private static final m:I = 0x9

.field private static final n:I = 0xa

.field private static final o:I = 0xb

.field private static final p:I = 0xc

.field private static final q:I = 0xd

.field private static final r:I = 0xe

.field private static final s:I = 0xf

.field private static final t:I = 0x10

.field private static final u:I = 0x11

.field private static final v:I = 0x0

.field private static final w:I = 0x1

.field private static final x:I = 0x2

.field private static final y:I = 0x3

.field private static final z:I = 0x4


# instance fields
.field private final D:Ljava/io/Reader;

.field private final E:[C

.field private F:I

.field private G:I

.field private H:I

.field private I:I

.field private J:I

.field private K:J

.field private L:I

.field private M:Ljava/lang/String;

.field private N:[I

.field private O:I

.field private P:[Ljava/lang/String;

.field private Q:[I

.field public b:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 192
    const-string v0, ")]}\'\n"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    sput-object v0, Lcom/a/c/d/a;->a:[C

    .line 1598
    new-instance v0, Lcom/a/c/d/b;

    invoke-direct {v0}, Lcom/a/c/d/b;-><init>()V

    sput-object v0, Lcom/a/c/b/u;->a:Lcom/a/c/b/u;

    .line 1621
    return-void
.end method

.method public constructor <init>(Ljava/io/Reader;)V
    .registers 6

    .prologue
    const/16 v3, 0x20

    const/4 v1, 0x0

    .line 289
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 230
    iput-boolean v1, p0, Lcom/a/c/d/a;->b:Z

    .line 238
    const/16 v0, 0x400

    new-array v0, v0, [C

    iput-object v0, p0, Lcom/a/c/d/a;->E:[C

    .line 239
    iput v1, p0, Lcom/a/c/d/a;->F:I

    .line 240
    iput v1, p0, Lcom/a/c/d/a;->G:I

    .line 242
    iput v1, p0, Lcom/a/c/d/a;->H:I

    .line 243
    iput v1, p0, Lcom/a/c/d/a;->I:I

    .line 245
    iput v1, p0, Lcom/a/c/d/a;->J:I

    .line 269
    new-array v0, v3, [I

    iput-object v0, p0, Lcom/a/c/d/a;->N:[I

    .line 270
    iput v1, p0, Lcom/a/c/d/a;->O:I

    .line 272
    iget-object v0, p0, Lcom/a/c/d/a;->N:[I

    iget v1, p0, Lcom/a/c/d/a;->O:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/a/c/d/a;->O:I

    const/4 v2, 0x6

    aput v2, v0, v1

    .line 283
    new-array v0, v3, [Ljava/lang/String;

    iput-object v0, p0, Lcom/a/c/d/a;->P:[Ljava/lang/String;

    .line 284
    new-array v0, v3, [I

    iput-object v0, p0, Lcom/a/c/d/a;->Q:[I

    .line 290
    if-nez p1, :cond_3b

    .line 291
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "in == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 293
    :cond_3b
    iput-object p1, p0, Lcom/a/c/d/a;->D:Ljava/io/Reader;

    .line 294
    return-void
.end method

.method private A()V
    .registers 4

    .prologue
    .line 1580
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/a/c/d/a;->b(Z)I

    .line 1581
    iget v0, p0, Lcom/a/c/d/a;->F:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/a/c/d/a;->F:I

    .line 1583
    iget v0, p0, Lcom/a/c/d/a;->F:I

    sget-object v1, Lcom/a/c/d/a;->a:[C

    array-length v1, v1

    add-int/2addr v0, v1

    iget v1, p0, Lcom/a/c/d/a;->G:I

    if-le v0, v1, :cond_1e

    sget-object v0, Lcom/a/c/d/a;->a:[C

    array-length v0, v0

    invoke-direct {p0, v0}, Lcom/a/c/d/a;->b(I)Z

    move-result v0

    if-nez v0, :cond_1e

    .line 1595
    :cond_1d
    :goto_1d
    return-void

    .line 1587
    :cond_1e
    const/4 v0, 0x0

    :goto_1f
    sget-object v1, Lcom/a/c/d/a;->a:[C

    array-length v1, v1

    if-ge v0, v1, :cond_34

    .line 1588
    iget-object v1, p0, Lcom/a/c/d/a;->E:[C

    iget v2, p0, Lcom/a/c/d/a;->F:I

    add-int/2addr v2, v0

    aget-char v1, v1, v2

    sget-object v2, Lcom/a/c/d/a;->a:[C

    aget-char v2, v2, v0

    if-ne v1, v2, :cond_1d

    .line 1587
    add-int/lit8 v0, v0, 0x1

    goto :goto_1f

    .line 1594
    :cond_34
    iget v0, p0, Lcom/a/c/d/a;->F:I

    sget-object v1, Lcom/a/c/d/a;->a:[C

    array-length v1, v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/a/c/d/a;->F:I

    goto :goto_1d
.end method

.method static synthetic a(Lcom/a/c/d/a;)I
    .registers 2

    .prologue
    .line 190
    iget v0, p0, Lcom/a/c/d/a;->J:I

    return v0
.end method

.method static synthetic a(Lcom/a/c/d/a;I)I
    .registers 2

    .prologue
    .line 190
    iput p1, p0, Lcom/a/c/d/a;->J:I

    return p1
.end method

.method private a(I)V
    .registers 8

    .prologue
    const/4 v5, 0x0

    .line 1267
    iget v0, p0, Lcom/a/c/d/a;->O:I

    iget-object v1, p0, Lcom/a/c/d/a;->N:[I

    array-length v1, v1

    if-ne v0, v1, :cond_35

    .line 1268
    iget v0, p0, Lcom/a/c/d/a;->O:I

    mul-int/lit8 v0, v0, 0x2

    new-array v0, v0, [I

    .line 1269
    iget v1, p0, Lcom/a/c/d/a;->O:I

    mul-int/lit8 v1, v1, 0x2

    new-array v1, v1, [I

    .line 1270
    iget v2, p0, Lcom/a/c/d/a;->O:I

    mul-int/lit8 v2, v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    .line 1271
    iget-object v3, p0, Lcom/a/c/d/a;->N:[I

    iget v4, p0, Lcom/a/c/d/a;->O:I

    invoke-static {v3, v5, v0, v5, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1272
    iget-object v3, p0, Lcom/a/c/d/a;->Q:[I

    iget v4, p0, Lcom/a/c/d/a;->O:I

    invoke-static {v3, v5, v1, v5, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1273
    iget-object v3, p0, Lcom/a/c/d/a;->P:[Ljava/lang/String;

    iget v4, p0, Lcom/a/c/d/a;->O:I

    invoke-static {v3, v5, v2, v5, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1274
    iput-object v0, p0, Lcom/a/c/d/a;->N:[I

    .line 1275
    iput-object v1, p0, Lcom/a/c/d/a;->Q:[I

    .line 1276
    iput-object v2, p0, Lcom/a/c/d/a;->P:[Ljava/lang/String;

    .line 1278
    :cond_35
    iget-object v0, p0, Lcom/a/c/d/a;->N:[I

    iget v1, p0, Lcom/a/c/d/a;->O:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/a/c/d/a;->O:I

    aput p1, v0, v1

    .line 1279
    return-void
.end method

.method private a(Z)V
    .registers 2

    .prologue
    .line 326
    iput-boolean p1, p0, Lcom/a/c/d/a;->b:Z

    .line 327
    return-void
.end method

.method private a(C)Z
    .registers 3

    .prologue
    .line 754
    sparse-switch p1, :sswitch_data_a

    .line 774
    const/4 v0, 0x1

    :goto_4
    return v0

    .line 760
    :sswitch_5
    invoke-direct {p0}, Lcom/a/c/d/a;->x()V

    .line 772
    :sswitch_8
    const/4 v0, 0x0

    goto :goto_4

    .line 754
    :sswitch_data_a
    .sparse-switch
        0x9 -> :sswitch_8
        0xa -> :sswitch_8
        0xc -> :sswitch_8
        0xd -> :sswitch_8
        0x20 -> :sswitch_8
        0x23 -> :sswitch_5
        0x2c -> :sswitch_8
        0x2f -> :sswitch_5
        0x3a -> :sswitch_8
        0x3b -> :sswitch_5
        0x3d -> :sswitch_5
        0x5b -> :sswitch_8
        0x5c -> :sswitch_5
        0x5d -> :sswitch_8
        0x7b -> :sswitch_8
        0x7d -> :sswitch_8
    .end sparse-switch
.end method

.method private a(Ljava/lang/String;)Z
    .registers 6

    .prologue
    const/4 v1, 0x0

    .line 1448
    :goto_1
    iget v0, p0, Lcom/a/c/d/a;->F:I

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v0, v2

    iget v2, p0, Lcom/a/c/d/a;->G:I

    if-le v0, v2, :cond_16

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/a/c/d/a;->b(I)Z

    move-result v0

    if-eqz v0, :cond_4b

    .line 1449
    :cond_16
    iget-object v0, p0, Lcom/a/c/d/a;->E:[C

    iget v2, p0, Lcom/a/c/d/a;->F:I

    aget-char v0, v0, v2

    const/16 v2, 0xa

    if-ne v0, v2, :cond_33

    .line 1450
    iget v0, p0, Lcom/a/c/d/a;->H:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/a/c/d/a;->H:I

    .line 1451
    iget v0, p0, Lcom/a/c/d/a;->F:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/a/c/d/a;->I:I

    .line 1448
    :cond_2c
    iget v0, p0, Lcom/a/c/d/a;->F:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/a/c/d/a;->F:I

    goto :goto_1

    :cond_33
    move v0, v1

    .line 1454
    :goto_34
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v0, v2, :cond_4a

    .line 1455
    iget-object v2, p0, Lcom/a/c/d/a;->E:[C

    iget v3, p0, Lcom/a/c/d/a;->F:I

    add-int/2addr v3, v0

    aget-char v2, v2, v3

    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v3

    if-ne v2, v3, :cond_2c

    .line 1454
    add-int/lit8 v0, v0, 0x1

    goto :goto_34

    .line 1459
    :cond_4a
    const/4 v1, 0x1

    .line 1461
    :cond_4b
    return v1
.end method

.method static synthetic b(Lcom/a/c/d/a;)I
    .registers 2

    .prologue
    .line 190
    invoke-direct {p0}, Lcom/a/c/d/a;->q()I

    move-result v0

    return v0
.end method

.method private b(Z)I
    .registers 10

    .prologue
    const/16 v7, 0xa

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1338
    iget-object v5, p0, Lcom/a/c/d/a;->E:[C

    .line 1339
    iget v1, p0, Lcom/a/c/d/a;->F:I

    .line 1340
    iget v0, p0, Lcom/a/c/d/a;->G:I

    .line 1342
    :goto_a
    if-ne v1, v0, :cond_18

    .line 1343
    iput v1, p0, Lcom/a/c/d/a;->F:I

    .line 1344
    invoke-direct {p0, v3}, Lcom/a/c/d/a;->b(I)Z

    move-result v0

    if-eqz v0, :cond_e6

    .line 1347
    iget v1, p0, Lcom/a/c/d/a;->F:I

    .line 1348
    iget v0, p0, Lcom/a/c/d/a;->G:I

    .line 1351
    :cond_18
    add-int/lit8 v4, v1, 0x1

    aget-char v1, v5, v1

    .line 1352
    if-ne v1, v7, :cond_28

    .line 1353
    iget v1, p0, Lcom/a/c/d/a;->H:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/a/c/d/a;->H:I

    .line 1354
    iput v4, p0, Lcom/a/c/d/a;->I:I

    move v1, v4

    .line 1355
    goto :goto_a

    .line 1356
    :cond_28
    const/16 v6, 0x20

    if-eq v1, v6, :cond_112

    const/16 v6, 0xd

    if-eq v1, v6, :cond_112

    const/16 v6, 0x9

    if-eq v1, v6, :cond_112

    .line 1360
    const/16 v6, 0x2f

    if-ne v1, v6, :cond_cf

    .line 1361
    iput v4, p0, Lcom/a/c/d/a;->F:I

    .line 1362
    if-ne v4, v0, :cond_51

    .line 1363
    iget v0, p0, Lcom/a/c/d/a;->F:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/a/c/d/a;->F:I

    .line 1364
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/a/c/d/a;->b(I)Z

    move-result v0

    .line 1365
    iget v4, p0, Lcom/a/c/d/a;->F:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/a/c/d/a;->F:I

    .line 1366
    if-nez v0, :cond_51

    move v0, v1

    .line 1415
    :goto_50
    return v0

    .line 1371
    :cond_51
    invoke-direct {p0}, Lcom/a/c/d/a;->x()V

    .line 1372
    iget v0, p0, Lcom/a/c/d/a;->F:I

    aget-char v0, v5, v0

    .line 1373
    sparse-switch v0, :sswitch_data_116

    move v0, v1

    .line 1393
    goto :goto_50

    .line 1376
    :sswitch_5d
    iget v0, p0, Lcom/a/c/d/a;->F:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/a/c/d/a;->F:I

    .line 1377
    const-string v1, "*/"

    .line 17448
    :goto_65
    iget v0, p0, Lcom/a/c/d/a;->F:I

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v0, v4

    iget v4, p0, Lcom/a/c/d/a;->G:I

    if-le v0, v4, :cond_7a

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/a/c/d/a;->b(I)Z

    move-result v0

    if-eqz v0, :cond_b6

    .line 17449
    :cond_7a
    iget-object v0, p0, Lcom/a/c/d/a;->E:[C

    iget v4, p0, Lcom/a/c/d/a;->F:I

    aget-char v0, v0, v4

    if-ne v0, v7, :cond_95

    .line 17450
    iget v0, p0, Lcom/a/c/d/a;->H:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/a/c/d/a;->H:I

    .line 17451
    iget v0, p0, Lcom/a/c/d/a;->F:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/a/c/d/a;->I:I

    .line 17448
    :cond_8e
    iget v0, p0, Lcom/a/c/d/a;->F:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/a/c/d/a;->F:I

    goto :goto_65

    :cond_95
    move v0, v2

    .line 17454
    :goto_96
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    if-ge v0, v4, :cond_ac

    .line 17455
    iget-object v4, p0, Lcom/a/c/d/a;->E:[C

    iget v6, p0, Lcom/a/c/d/a;->F:I

    add-int/2addr v6, v0

    aget-char v4, v4, v6

    invoke-virtual {v1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v6

    if-ne v4, v6, :cond_8e

    .line 17454
    add-int/lit8 v0, v0, 0x1

    goto :goto_96

    :cond_ac
    move v0, v3

    .line 1377
    :goto_ad
    if-nez v0, :cond_b8

    .line 1378
    const-string v0, "Unterminated comment"

    invoke-direct {p0, v0}, Lcom/a/c/d/a;->b(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_b6
    move v0, v2

    .line 17461
    goto :goto_ad

    .line 1380
    :cond_b8
    iget v0, p0, Lcom/a/c/d/a;->F:I

    add-int/lit8 v1, v0, 0x2

    .line 1381
    iget v0, p0, Lcom/a/c/d/a;->G:I

    goto/16 :goto_a

    .line 1386
    :sswitch_c0
    iget v0, p0, Lcom/a/c/d/a;->F:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/a/c/d/a;->F:I

    .line 1387
    invoke-direct {p0}, Lcom/a/c/d/a;->y()V

    .line 1388
    iget v1, p0, Lcom/a/c/d/a;->F:I

    .line 1389
    iget v0, p0, Lcom/a/c/d/a;->G:I

    goto/16 :goto_a

    .line 1395
    :cond_cf
    const/16 v0, 0x23

    if-ne v1, v0, :cond_e1

    .line 1396
    iput v4, p0, Lcom/a/c/d/a;->F:I

    .line 1402
    invoke-direct {p0}, Lcom/a/c/d/a;->x()V

    .line 1403
    invoke-direct {p0}, Lcom/a/c/d/a;->y()V

    .line 1404
    iget v1, p0, Lcom/a/c/d/a;->F:I

    .line 1405
    iget v0, p0, Lcom/a/c/d/a;->G:I

    goto/16 :goto_a

    .line 1407
    :cond_e1
    iput v4, p0, Lcom/a/c/d/a;->F:I

    move v0, v1

    .line 1408
    goto/16 :goto_50

    .line 1411
    :cond_e6
    if-eqz p1, :cond_10f

    .line 1412
    new-instance v0, Ljava/io/EOFException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "End of input at line "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 18316
    iget v2, p0, Lcom/a/c/d/a;->H:I

    add-int/lit8 v2, v2, 0x1

    .line 1413
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " column "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lcom/a/c/d/a;->w()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/EOFException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1415
    :cond_10f
    const/4 v0, -0x1

    goto/16 :goto_50

    :cond_112
    move v1, v4

    goto/16 :goto_a

    .line 1373
    nop

    :sswitch_data_116
    .sparse-switch
        0x2a -> :sswitch_5d
        0x2f -> :sswitch_c0
    .end sparse-switch
.end method

.method private b(Ljava/lang/String;)Ljava/io/IOException;
    .registers 5

    .prologue
    .line 1571
    new-instance v0, Lcom/a/c/d/f;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " at line "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 20316
    iget v2, p0, Lcom/a/c/d/a;->H:I

    add-int/lit8 v2, v2, 0x1

    .line 1572
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " column "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lcom/a/c/d/a;->w()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " path "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/a/c/d/a;->p()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/a/c/d/f;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private b(C)Ljava/lang/String;
    .registers 9

    .prologue
    .line 999
    iget-object v3, p0, Lcom/a/c/d/a;->E:[C

    .line 1000
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 1002
    :cond_7
    :goto_7
    iget v1, p0, Lcom/a/c/d/a;->F:I

    .line 1003
    iget v5, p0, Lcom/a/c/d/a;->G:I

    move v0, v1

    .line 1006
    :goto_c
    if-ge v0, v5, :cond_45

    .line 1007
    add-int/lit8 v2, v0, 0x1

    aget-char v0, v3, v0

    .line 1009
    if-ne v0, p1, :cond_22

    .line 1010
    iput v2, p0, Lcom/a/c/d/a;->F:I

    .line 1011
    sub-int v0, v2, v1

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v4, v3, v1, v0}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    .line 1012
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1013
    :cond_22
    const/16 v6, 0x5c

    if-ne v0, v6, :cond_37

    .line 1014
    iput v2, p0, Lcom/a/c/d/a;->F:I

    .line 1015
    sub-int v0, v2, v1

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v4, v3, v1, v0}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    .line 1016
    invoke-direct {p0}, Lcom/a/c/d/a;->z()C

    move-result v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_7

    .line 1020
    :cond_37
    const/16 v6, 0xa

    if-ne v0, v6, :cond_43

    .line 1021
    iget v0, p0, Lcom/a/c/d/a;->H:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/a/c/d/a;->H:I

    .line 1022
    iput v2, p0, Lcom/a/c/d/a;->I:I

    :cond_43
    move v0, v2

    .line 1024
    goto :goto_c

    .line 1026
    :cond_45
    sub-int v2, v0, v1

    invoke-virtual {v4, v3, v1, v2}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    .line 1027
    iput v0, p0, Lcom/a/c/d/a;->F:I

    .line 1028
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/a/c/d/a;->b(I)Z

    move-result v0

    if-nez v0, :cond_7

    .line 1029
    const-string v0, "Unterminated string"

    invoke-direct {p0, v0}, Lcom/a/c/d/a;->b(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v0

    throw v0
.end method

.method private b(I)Z
    .registers 8

    .prologue
    const/4 v0, 0x0

    .line 1287
    iget-object v1, p0, Lcom/a/c/d/a;->E:[C

    .line 1288
    iget v2, p0, Lcom/a/c/d/a;->I:I

    iget v3, p0, Lcom/a/c/d/a;->F:I

    sub-int/2addr v2, v3

    iput v2, p0, Lcom/a/c/d/a;->I:I

    .line 1289
    iget v2, p0, Lcom/a/c/d/a;->G:I

    iget v3, p0, Lcom/a/c/d/a;->F:I

    if-eq v2, v3, :cond_5b

    .line 1290
    iget v2, p0, Lcom/a/c/d/a;->G:I

    iget v3, p0, Lcom/a/c/d/a;->F:I

    sub-int/2addr v2, v3

    iput v2, p0, Lcom/a/c/d/a;->G:I

    .line 1291
    iget v2, p0, Lcom/a/c/d/a;->F:I

    iget v3, p0, Lcom/a/c/d/a;->G:I

    invoke-static {v1, v2, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1296
    :goto_1e
    iput v0, p0, Lcom/a/c/d/a;->F:I

    .line 1298
    :cond_20
    iget-object v2, p0, Lcom/a/c/d/a;->D:Ljava/io/Reader;

    iget v3, p0, Lcom/a/c/d/a;->G:I

    array-length v4, v1

    iget v5, p0, Lcom/a/c/d/a;->G:I

    sub-int/2addr v4, v5

    invoke-virtual {v2, v1, v3, v4}, Ljava/io/Reader;->read([CII)I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_5a

    .line 1299
    iget v3, p0, Lcom/a/c/d/a;->G:I

    add-int/2addr v2, v3

    iput v2, p0, Lcom/a/c/d/a;->G:I

    .line 1302
    iget v2, p0, Lcom/a/c/d/a;->H:I

    if-nez v2, :cond_55

    iget v2, p0, Lcom/a/c/d/a;->I:I

    if-nez v2, :cond_55

    iget v2, p0, Lcom/a/c/d/a;->G:I

    if-lez v2, :cond_55

    aget-char v2, v1, v0

    const v3, 0xfeff

    if-ne v2, v3, :cond_55

    .line 1303
    iget v2, p0, Lcom/a/c/d/a;->F:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/a/c/d/a;->F:I

    .line 1304
    iget v2, p0, Lcom/a/c/d/a;->I:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/a/c/d/a;->I:I

    .line 1305
    add-int/lit8 p1, p1, 0x1

    .line 1308
    :cond_55
    iget v2, p0, Lcom/a/c/d/a;->G:I

    if-lt v2, p1, :cond_20

    .line 1309
    const/4 v0, 0x1

    .line 1312
    :cond_5a
    return v0

    .line 1293
    :cond_5b
    iput v0, p0, Lcom/a/c/d/a;->G:I

    goto :goto_1e
.end method

.method static synthetic c(Lcom/a/c/d/a;)I
    .registers 2

    .prologue
    .line 190
    .line 21316
    iget v0, p0, Lcom/a/c/d/a;->H:I

    add-int/lit8 v0, v0, 0x1

    .line 190
    return v0
.end method

.method private c(C)V
    .registers 7

    .prologue
    .line 1101
    iget-object v2, p0, Lcom/a/c/d/a;->E:[C

    .line 1103
    :cond_2
    :goto_2
    iget v0, p0, Lcom/a/c/d/a;->F:I

    .line 1104
    iget v3, p0, Lcom/a/c/d/a;->G:I

    .line 1106
    :goto_6
    if-ge v0, v3, :cond_29

    .line 1107
    add-int/lit8 v1, v0, 0x1

    aget-char v0, v2, v0

    .line 1108
    if-ne v0, p1, :cond_11

    .line 1109
    iput v1, p0, Lcom/a/c/d/a;->F:I

    .line 1110
    return-void

    .line 1111
    :cond_11
    const/16 v4, 0x5c

    if-ne v0, v4, :cond_1b

    .line 1112
    iput v1, p0, Lcom/a/c/d/a;->F:I

    .line 1113
    invoke-direct {p0}, Lcom/a/c/d/a;->z()C

    goto :goto_2

    .line 1116
    :cond_1b
    const/16 v4, 0xa

    if-ne v0, v4, :cond_27

    .line 1117
    iget v0, p0, Lcom/a/c/d/a;->H:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/a/c/d/a;->H:I

    .line 1118
    iput v1, p0, Lcom/a/c/d/a;->I:I

    :cond_27
    move v0, v1

    .line 1120
    goto :goto_6

    .line 1121
    :cond_29
    iput v0, p0, Lcom/a/c/d/a;->F:I

    .line 1122
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/a/c/d/a;->b(I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1123
    const-string v0, "Unterminated string"

    invoke-direct {p0, v0}, Lcom/a/c/d/a;->b(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v0

    throw v0
.end method

.method static synthetic d(Lcom/a/c/d/a;)I
    .registers 2

    .prologue
    .line 190
    invoke-direct {p0}, Lcom/a/c/d/a;->w()I

    move-result v0

    return v0
.end method

.method private g()Z
    .registers 2

    .prologue
    .line 333
    iget-boolean v0, p0, Lcom/a/c/d/a;->b:Z

    return v0
.end method

.method private q()I
    .registers 9

    .prologue
    const/4 v4, 0x7

    const/4 v7, 0x5

    const/4 v1, 0x4

    const/4 v2, 0x2

    const/4 v3, 0x1

    .line 464
    iget-object v0, p0, Lcom/a/c/d/a;->N:[I

    iget v5, p0, Lcom/a/c/d/a;->O:I

    add-int/lit8 v5, v5, -0x1

    aget v5, v0, v5

    .line 465
    if-ne v5, v3, :cond_32

    .line 466
    iget-object v0, p0, Lcom/a/c/d/a;->N:[I

    iget v6, p0, Lcom/a/c/d/a;->O:I

    add-int/lit8 v6, v6, -0x1

    aput v2, v0, v6

    .line 551
    :cond_17
    :goto_17
    :pswitch_17
    :sswitch_17
    invoke-direct {p0, v3}, Lcom/a/c/d/a;->b(Z)I

    move-result v0

    .line 552
    sparse-switch v0, :sswitch_data_1b8

    .line 581
    iget v0, p0, Lcom/a/c/d/a;->F:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/a/c/d/a;->F:I

    .line 584
    iget v0, p0, Lcom/a/c/d/a;->O:I

    if-ne v0, v3, :cond_2b

    .line 585
    invoke-direct {p0}, Lcom/a/c/d/a;->x()V

    .line 588
    :cond_2b
    invoke-direct {p0}, Lcom/a/c/d/a;->r()I

    move-result v0

    .line 589
    if-eqz v0, :cond_196

    .line 603
    :cond_31
    :goto_31
    return v0

    .line 467
    :cond_32
    if-ne v5, v2, :cond_4a

    .line 469
    invoke-direct {p0, v3}, Lcom/a/c/d/a;->b(Z)I

    move-result v0

    .line 470
    sparse-switch v0, :sswitch_data_1d6

    .line 478
    const-string v0, "Unterminated array"

    invoke-direct {p0, v0}, Lcom/a/c/d/a;->b(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 472
    :sswitch_42
    iput v1, p0, Lcom/a/c/d/a;->J:I

    move v0, v1

    goto :goto_31

    .line 474
    :sswitch_46
    invoke-direct {p0}, Lcom/a/c/d/a;->x()V

    goto :goto_17

    .line 480
    :cond_4a
    const/4 v0, 0x3

    if-eq v5, v0, :cond_4f

    if-ne v5, v7, :cond_ab

    .line 481
    :cond_4f
    iget-object v0, p0, Lcom/a/c/d/a;->N:[I

    iget v4, p0, Lcom/a/c/d/a;->O:I

    add-int/lit8 v4, v4, -0x1

    aput v1, v0, v4

    .line 483
    if-ne v5, v7, :cond_6e

    .line 484
    invoke-direct {p0, v3}, Lcom/a/c/d/a;->b(Z)I

    move-result v0

    .line 485
    sparse-switch v0, :sswitch_data_1e4

    .line 493
    const-string v0, "Unterminated object"

    invoke-direct {p0, v0}, Lcom/a/c/d/a;->b(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 487
    :sswitch_67
    iput v2, p0, Lcom/a/c/d/a;->J:I

    move v0, v2

    goto :goto_31

    .line 489
    :sswitch_6b
    invoke-direct {p0}, Lcom/a/c/d/a;->x()V

    .line 496
    :cond_6e
    :sswitch_6e
    invoke-direct {p0, v3}, Lcom/a/c/d/a;->b(Z)I

    move-result v0

    .line 497
    sparse-switch v0, :sswitch_data_1f2

    .line 510
    invoke-direct {p0}, Lcom/a/c/d/a;->x()V

    .line 511
    iget v1, p0, Lcom/a/c/d/a;->F:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/a/c/d/a;->F:I

    .line 512
    int-to-char v0, v0

    invoke-direct {p0, v0}, Lcom/a/c/d/a;->a(C)Z

    move-result v0

    if-eqz v0, :cond_a4

    .line 513
    const/16 v0, 0xe

    iput v0, p0, Lcom/a/c/d/a;->J:I

    goto :goto_31

    .line 499
    :sswitch_8a
    const/16 v0, 0xd

    iput v0, p0, Lcom/a/c/d/a;->J:I

    goto :goto_31

    .line 501
    :sswitch_8f
    invoke-direct {p0}, Lcom/a/c/d/a;->x()V

    .line 502
    const/16 v0, 0xc

    iput v0, p0, Lcom/a/c/d/a;->J:I

    goto :goto_31

    .line 504
    :sswitch_97
    if-eq v5, v7, :cond_9d

    .line 505
    iput v2, p0, Lcom/a/c/d/a;->J:I

    move v0, v2

    goto :goto_31

    .line 507
    :cond_9d
    const-string v0, "Expected name"

    invoke-direct {p0, v0}, Lcom/a/c/d/a;->b(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 515
    :cond_a4
    const-string v0, "Expected name"

    invoke-direct {p0, v0}, Lcom/a/c/d/a;->b(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 518
    :cond_ab
    if-ne v5, v1, :cond_e4

    .line 519
    iget-object v0, p0, Lcom/a/c/d/a;->N:[I

    iget v6, p0, Lcom/a/c/d/a;->O:I

    add-int/lit8 v6, v6, -0x1

    aput v7, v0, v6

    .line 521
    invoke-direct {p0, v3}, Lcom/a/c/d/a;->b(Z)I

    move-result v0

    .line 522
    packed-switch v0, :pswitch_data_200

    .line 532
    :pswitch_bc
    const-string v0, "Expected \':\'"

    invoke-direct {p0, v0}, Lcom/a/c/d/a;->b(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 526
    :pswitch_c3
    invoke-direct {p0}, Lcom/a/c/d/a;->x()V

    .line 527
    iget v0, p0, Lcom/a/c/d/a;->F:I

    iget v6, p0, Lcom/a/c/d/a;->G:I

    if-lt v0, v6, :cond_d2

    invoke-direct {p0, v3}, Lcom/a/c/d/a;->b(I)Z

    move-result v0

    if-eqz v0, :cond_17

    :cond_d2
    iget-object v0, p0, Lcom/a/c/d/a;->E:[C

    iget v6, p0, Lcom/a/c/d/a;->F:I

    aget-char v0, v0, v6

    const/16 v6, 0x3e

    if-ne v0, v6, :cond_17

    .line 528
    iget v0, p0, Lcom/a/c/d/a;->F:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/a/c/d/a;->F:I

    goto/16 :goto_17

    .line 534
    :cond_e4
    const/4 v0, 0x6

    if-ne v5, v0, :cond_12f

    .line 535
    iget-boolean v0, p0, Lcom/a/c/d/a;->b:Z

    if-eqz v0, :cond_125

    .line 5580
    invoke-direct {p0, v3}, Lcom/a/c/d/a;->b(Z)I

    .line 5581
    iget v0, p0, Lcom/a/c/d/a;->F:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/a/c/d/a;->F:I

    .line 5583
    iget v0, p0, Lcom/a/c/d/a;->F:I

    sget-object v6, Lcom/a/c/d/a;->a:[C

    array-length v6, v6

    add-int/2addr v0, v6

    iget v6, p0, Lcom/a/c/d/a;->G:I

    if-le v0, v6, :cond_107

    sget-object v0, Lcom/a/c/d/a;->a:[C

    array-length v0, v0

    invoke-direct {p0, v0}, Lcom/a/c/d/a;->b(I)Z

    move-result v0

    if-eqz v0, :cond_125

    .line 5587
    :cond_107
    const/4 v0, 0x0

    :goto_108
    sget-object v6, Lcom/a/c/d/a;->a:[C

    array-length v6, v6

    if-ge v0, v6, :cond_11d

    .line 5588
    iget-object v6, p0, Lcom/a/c/d/a;->E:[C

    iget v7, p0, Lcom/a/c/d/a;->F:I

    add-int/2addr v7, v0

    aget-char v6, v6, v7

    sget-object v7, Lcom/a/c/d/a;->a:[C

    aget-char v7, v7, v0

    if-ne v6, v7, :cond_125

    .line 5587
    add-int/lit8 v0, v0, 0x1

    goto :goto_108

    .line 5594
    :cond_11d
    iget v0, p0, Lcom/a/c/d/a;->F:I

    sget-object v6, Lcom/a/c/d/a;->a:[C

    array-length v6, v6

    add-int/2addr v0, v6

    iput v0, p0, Lcom/a/c/d/a;->F:I

    .line 538
    :cond_125
    iget-object v0, p0, Lcom/a/c/d/a;->N:[I

    iget v6, p0, Lcom/a/c/d/a;->O:I

    add-int/lit8 v6, v6, -0x1

    aput v4, v0, v6

    goto/16 :goto_17

    .line 539
    :cond_12f
    if-ne v5, v4, :cond_14a

    .line 540
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/a/c/d/a;->b(Z)I

    move-result v0

    .line 541
    const/4 v6, -0x1

    if-ne v0, v6, :cond_13f

    .line 542
    const/16 v0, 0x11

    iput v0, p0, Lcom/a/c/d/a;->J:I

    goto/16 :goto_31

    .line 544
    :cond_13f
    invoke-direct {p0}, Lcom/a/c/d/a;->x()V

    .line 545
    iget v0, p0, Lcom/a/c/d/a;->F:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/a/c/d/a;->F:I

    goto/16 :goto_17

    .line 547
    :cond_14a
    const/16 v0, 0x8

    if-ne v5, v0, :cond_17

    .line 548
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "JsonReader is closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 554
    :sswitch_156
    if-ne v5, v3, :cond_15d

    .line 555
    iput v1, p0, Lcom/a/c/d/a;->J:I

    move v0, v1

    goto/16 :goto_31

    .line 561
    :cond_15d
    :sswitch_15d
    if-eq v5, v3, :cond_161

    if-ne v5, v2, :cond_16f

    .line 562
    :cond_161
    invoke-direct {p0}, Lcom/a/c/d/a;->x()V

    .line 563
    iget v0, p0, Lcom/a/c/d/a;->F:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/a/c/d/a;->F:I

    .line 564
    iput v4, p0, Lcom/a/c/d/a;->J:I

    move v0, v4

    goto/16 :goto_31

    .line 566
    :cond_16f
    const-string v0, "Unexpected value"

    invoke-direct {p0, v0}, Lcom/a/c/d/a;->b(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 569
    :sswitch_176
    invoke-direct {p0}, Lcom/a/c/d/a;->x()V

    .line 570
    const/16 v0, 0x8

    iput v0, p0, Lcom/a/c/d/a;->J:I

    goto/16 :goto_31

    .line 572
    :sswitch_17f
    iget v0, p0, Lcom/a/c/d/a;->O:I

    if-ne v0, v3, :cond_186

    .line 573
    invoke-direct {p0}, Lcom/a/c/d/a;->x()V

    .line 575
    :cond_186
    const/16 v0, 0x9

    iput v0, p0, Lcom/a/c/d/a;->J:I

    goto/16 :goto_31

    .line 577
    :sswitch_18c
    const/4 v0, 0x3

    iput v0, p0, Lcom/a/c/d/a;->J:I

    goto/16 :goto_31

    .line 579
    :sswitch_191
    iput v3, p0, Lcom/a/c/d/a;->J:I

    move v0, v3

    goto/16 :goto_31

    .line 593
    :cond_196
    invoke-direct {p0}, Lcom/a/c/d/a;->s()I

    move-result v0

    .line 594
    if-nez v0, :cond_31

    .line 598
    iget-object v0, p0, Lcom/a/c/d/a;->E:[C

    iget v1, p0, Lcom/a/c/d/a;->F:I

    aget-char v0, v0, v1

    invoke-direct {p0, v0}, Lcom/a/c/d/a;->a(C)Z

    move-result v0

    if-nez v0, :cond_1af

    .line 599
    const-string v0, "Expected value"

    invoke-direct {p0, v0}, Lcom/a/c/d/a;->b(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 602
    :cond_1af
    invoke-direct {p0}, Lcom/a/c/d/a;->x()V

    .line 603
    const/16 v0, 0xa

    iput v0, p0, Lcom/a/c/d/a;->J:I

    goto/16 :goto_31

    .line 552
    :sswitch_data_1b8
    .sparse-switch
        0x22 -> :sswitch_17f
        0x27 -> :sswitch_176
        0x2c -> :sswitch_15d
        0x3b -> :sswitch_15d
        0x5b -> :sswitch_18c
        0x5d -> :sswitch_156
        0x7b -> :sswitch_191
    .end sparse-switch

    .line 470
    :sswitch_data_1d6
    .sparse-switch
        0x2c -> :sswitch_17
        0x3b -> :sswitch_46
        0x5d -> :sswitch_42
    .end sparse-switch

    .line 485
    :sswitch_data_1e4
    .sparse-switch
        0x2c -> :sswitch_6e
        0x3b -> :sswitch_6b
        0x7d -> :sswitch_67
    .end sparse-switch

    .line 497
    :sswitch_data_1f2
    .sparse-switch
        0x22 -> :sswitch_8a
        0x27 -> :sswitch_8f
        0x7d -> :sswitch_97
    .end sparse-switch

    .line 522
    :pswitch_data_200
    .packed-switch 0x3a
        :pswitch_17
        :pswitch_bc
        :pswitch_bc
        :pswitch_c3
    .end packed-switch
.end method

.method private r()I
    .registers 9

    .prologue
    const/4 v3, 0x0

    .line 608
    iget-object v0, p0, Lcom/a/c/d/a;->E:[C

    iget v1, p0, Lcom/a/c/d/a;->F:I

    aget-char v0, v0, v1

    .line 612
    const/16 v1, 0x74

    if-eq v0, v1, :cond_f

    const/16 v1, 0x54

    if-ne v0, v1, :cond_2c

    .line 613
    :cond_f
    const-string v2, "true"

    .line 614
    const-string v1, "TRUE"

    .line 615
    const/4 v0, 0x5

    .line 629
    :goto_14
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    .line 630
    const/4 v4, 0x1

    :goto_19
    if-ge v4, v5, :cond_62

    .line 631
    iget v6, p0, Lcom/a/c/d/a;->F:I

    add-int/2addr v6, v4

    iget v7, p0, Lcom/a/c/d/a;->G:I

    if-lt v6, v7, :cond_4a

    add-int/lit8 v6, v4, 0x1

    invoke-direct {p0, v6}, Lcom/a/c/d/a;->b(I)Z

    move-result v6

    if-nez v6, :cond_4a

    move v0, v3

    .line 647
    :goto_2b
    return v0

    .line 616
    :cond_2c
    const/16 v1, 0x66

    if-eq v0, v1, :cond_34

    const/16 v1, 0x46

    if-ne v0, v1, :cond_3a

    .line 617
    :cond_34
    const-string v2, "false"

    .line 618
    const-string v1, "FALSE"

    .line 619
    const/4 v0, 0x6

    goto :goto_14

    .line 620
    :cond_3a
    const/16 v1, 0x6e

    if-eq v0, v1, :cond_42

    const/16 v1, 0x4e

    if-ne v0, v1, :cond_48

    .line 621
    :cond_42
    const-string v2, "null"

    .line 622
    const-string v1, "NULL"

    .line 623
    const/4 v0, 0x7

    goto :goto_14

    :cond_48
    move v0, v3

    .line 625
    goto :goto_2b

    .line 634
    :cond_4a
    iget-object v6, p0, Lcom/a/c/d/a;->E:[C

    iget v7, p0, Lcom/a/c/d/a;->F:I

    add-int/2addr v7, v4

    aget-char v6, v6, v7

    .line 635
    invoke-virtual {v2, v4}, Ljava/lang/String;->charAt(I)C

    move-result v7

    if-eq v6, v7, :cond_5f

    invoke-virtual {v1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v7

    if-eq v6, v7, :cond_5f

    move v0, v3

    .line 636
    goto :goto_2b

    .line 630
    :cond_5f
    add-int/lit8 v4, v4, 0x1

    goto :goto_19

    .line 640
    :cond_62
    iget v1, p0, Lcom/a/c/d/a;->F:I

    add-int/2addr v1, v5

    iget v2, p0, Lcom/a/c/d/a;->G:I

    if-lt v1, v2, :cond_71

    add-int/lit8 v1, v5, 0x1

    invoke-direct {p0, v1}, Lcom/a/c/d/a;->b(I)Z

    move-result v1

    if-eqz v1, :cond_80

    :cond_71
    iget-object v1, p0, Lcom/a/c/d/a;->E:[C

    iget v2, p0, Lcom/a/c/d/a;->F:I

    add-int/2addr v2, v5

    aget-char v1, v1, v2

    .line 641
    invoke-direct {p0, v1}, Lcom/a/c/d/a;->a(C)Z

    move-result v1

    if-eqz v1, :cond_80

    move v0, v3

    .line 642
    goto :goto_2b

    .line 646
    :cond_80
    iget v1, p0, Lcom/a/c/d/a;->F:I

    add-int/2addr v1, v5

    iput v1, p0, Lcom/a/c/d/a;->F:I

    .line 647
    iput v0, p0, Lcom/a/c/d/a;->J:I

    goto :goto_2b
.end method

.method private s()I
    .registers 16

    .prologue
    .line 652
    iget-object v11, p0, Lcom/a/c/d/a;->E:[C

    .line 653
    iget v2, p0, Lcom/a/c/d/a;->F:I

    .line 654
    iget v1, p0, Lcom/a/c/d/a;->G:I

    .line 656
    const-wide/16 v6, 0x0

    .line 657
    const/4 v5, 0x0

    .line 658
    const/4 v4, 0x1

    .line 659
    const/4 v3, 0x0

    .line 661
    const/4 v0, 0x0

    move v10, v0

    move v0, v1

    move v1, v2

    .line 665
    :goto_f
    add-int v2, v1, v10

    if-ne v2, v0, :cond_24

    .line 666
    array-length v0, v11

    if-ne v10, v0, :cond_18

    .line 669
    const/4 v0, 0x0

    .line 749
    :goto_17
    return v0

    .line 671
    :cond_18
    add-int/lit8 v0, v10, 0x1

    invoke-direct {p0, v0}, Lcom/a/c/d/a;->b(I)Z

    move-result v0

    if-eqz v0, :cond_c0

    .line 674
    iget v1, p0, Lcom/a/c/d/a;->F:I

    .line 675
    iget v0, p0, Lcom/a/c/d/a;->G:I

    .line 678
    :cond_24
    add-int v2, v1, v10

    aget-char v2, v11, v2

    .line 679
    sparse-switch v2, :sswitch_data_f8

    .line 714
    const/16 v8, 0x30

    if-lt v2, v8, :cond_33

    const/16 v8, 0x39

    if-le v2, v8, :cond_70

    .line 715
    :cond_33
    invoke-direct {p0, v2}, Lcom/a/c/d/a;->a(C)Z

    move-result v0

    if-eqz v0, :cond_c0

    .line 718
    const/4 v0, 0x0

    goto :goto_17

    .line 681
    :sswitch_3b
    if-nez v3, :cond_49

    .line 682
    const/4 v3, 0x1

    .line 683
    const/4 v2, 0x1

    move v14, v4

    move v4, v3

    move v3, v14

    .line 664
    :goto_42
    add-int/lit8 v5, v10, 0x1

    move v10, v5

    move v5, v4

    move v4, v3

    move v3, v2

    goto :goto_f

    .line 685
    :cond_49
    const/4 v2, 0x5

    if-ne v3, v2, :cond_50

    .line 686
    const/4 v2, 0x6

    move v3, v4

    move v4, v5

    .line 687
    goto :goto_42

    .line 689
    :cond_50
    const/4 v0, 0x0

    goto :goto_17

    .line 692
    :sswitch_52
    const/4 v2, 0x5

    if-ne v3, v2, :cond_59

    .line 693
    const/4 v2, 0x6

    move v3, v4

    move v4, v5

    .line 694
    goto :goto_42

    .line 696
    :cond_59
    const/4 v0, 0x0

    goto :goto_17

    .line 700
    :sswitch_5b
    const/4 v2, 0x2

    if-eq v3, v2, :cond_61

    const/4 v2, 0x4

    if-ne v3, v2, :cond_65

    .line 701
    :cond_61
    const/4 v2, 0x5

    move v3, v4

    move v4, v5

    .line 702
    goto :goto_42

    .line 704
    :cond_65
    const/4 v0, 0x0

    goto :goto_17

    .line 707
    :sswitch_67
    const/4 v2, 0x2

    if-ne v3, v2, :cond_6e

    .line 708
    const/4 v2, 0x3

    move v3, v4

    move v4, v5

    .line 709
    goto :goto_42

    .line 711
    :cond_6e
    const/4 v0, 0x0

    goto :goto_17

    .line 720
    :cond_70
    const/4 v8, 0x1

    if-eq v3, v8, :cond_75

    if-nez v3, :cond_7d

    .line 721
    :cond_75
    add-int/lit8 v2, v2, -0x30

    neg-int v2, v2

    int-to-long v6, v2

    .line 722
    const/4 v2, 0x2

    move v3, v4

    move v4, v5

    goto :goto_42

    .line 723
    :cond_7d
    const/4 v8, 0x2

    if-ne v3, v8, :cond_af

    .line 724
    const-wide/16 v8, 0x0

    cmp-long v8, v6, v8

    if-nez v8, :cond_88

    .line 725
    const/4 v0, 0x0

    goto :goto_17

    .line 727
    :cond_88
    const-wide/16 v8, 0xa

    mul-long/2addr v8, v6

    add-int/lit8 v2, v2, -0x30

    int-to-long v12, v2

    sub-long/2addr v8, v12

    .line 728
    const-wide v12, -0xcccccccccccccccL

    cmp-long v2, v6, v12

    if-gtz v2, :cond_a5

    const-wide v12, -0xcccccccccccccccL

    cmp-long v2, v6, v12

    if-nez v2, :cond_ad

    cmp-long v2, v8, v6

    if-gez v2, :cond_ad

    :cond_a5
    const/4 v2, 0x1

    :goto_a6
    and-int/2addr v2, v4

    move v4, v5

    move-wide v6, v8

    move v14, v3

    move v3, v2

    move v2, v14

    .line 731
    goto :goto_42

    .line 728
    :cond_ad
    const/4 v2, 0x0

    goto :goto_a6

    .line 731
    :cond_af
    const/4 v2, 0x3

    if-ne v3, v2, :cond_b6

    .line 732
    const/4 v2, 0x4

    move v3, v4

    move v4, v5

    goto :goto_42

    .line 733
    :cond_b6
    const/4 v2, 0x5

    if-eq v3, v2, :cond_bc

    const/4 v2, 0x6

    if-ne v3, v2, :cond_f2

    .line 734
    :cond_bc
    const/4 v2, 0x7

    move v3, v4

    move v4, v5

    goto :goto_42

    .line 740
    :cond_c0
    const/4 v0, 0x2

    if-ne v3, v0, :cond_de

    if-eqz v4, :cond_de

    const-wide/high16 v0, -0x8000000000000000L

    cmp-long v0, v6, v0

    if-nez v0, :cond_cd

    if-eqz v5, :cond_de

    .line 741
    :cond_cd
    if-eqz v5, :cond_dc

    :goto_cf
    iput-wide v6, p0, Lcom/a/c/d/a;->K:J

    .line 742
    iget v0, p0, Lcom/a/c/d/a;->F:I

    add-int/2addr v0, v10

    iput v0, p0, Lcom/a/c/d/a;->F:I

    .line 743
    const/16 v0, 0xf

    iput v0, p0, Lcom/a/c/d/a;->J:I

    goto/16 :goto_17

    .line 741
    :cond_dc
    neg-long v6, v6

    goto :goto_cf

    .line 744
    :cond_de
    const/4 v0, 0x2

    if-eq v3, v0, :cond_e7

    const/4 v0, 0x4

    if-eq v3, v0, :cond_e7

    const/4 v0, 0x7

    if-ne v3, v0, :cond_ef

    .line 746
    :cond_e7
    iput v10, p0, Lcom/a/c/d/a;->L:I

    .line 747
    const/16 v0, 0x10

    iput v0, p0, Lcom/a/c/d/a;->J:I

    goto/16 :goto_17

    .line 749
    :cond_ef
    const/4 v0, 0x0

    goto/16 :goto_17

    :cond_f2
    move v2, v3

    move v3, v4

    move v4, v5

    goto/16 :goto_42

    .line 679
    nop

    :sswitch_data_f8
    .sparse-switch
        0x2b -> :sswitch_52
        0x2d -> :sswitch_3b
        0x2e -> :sswitch_67
        0x45 -> :sswitch_5b
        0x65 -> :sswitch_5b
    .end sparse-switch
.end method

.method private t()Ljava/lang/String;
    .registers 6

    .prologue
    const/4 v2, 0x0

    .line 1039
    const/4 v0, 0x0

    move v1, v2

    .line 1044
    :goto_3
    iget v3, p0, Lcom/a/c/d/a;->F:I

    add-int/2addr v3, v1

    iget v4, p0, Lcom/a/c/d/a;->G:I

    if-ge v3, v4, :cond_2b

    .line 1045
    iget-object v3, p0, Lcom/a/c/d/a;->E:[C

    iget v4, p0, Lcom/a/c/d/a;->F:I

    add-int/2addr v4, v1

    aget-char v3, v3, v4

    sparse-switch v3, :sswitch_data_64

    .line 1044
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 1051
    :sswitch_17
    invoke-direct {p0}, Lcom/a/c/d/a;->x()V

    .line 1089
    :cond_1a
    :goto_1a
    :sswitch_1a
    if-nez v0, :cond_55

    .line 1090
    new-instance v0, Ljava/lang/String;

    iget-object v2, p0, Lcom/a/c/d/a;->E:[C

    iget v3, p0, Lcom/a/c/d/a;->F:I

    invoke-direct {v0, v2, v3, v1}, Ljava/lang/String;-><init>([CII)V

    .line 1095
    :goto_25
    iget v2, p0, Lcom/a/c/d/a;->F:I

    add-int/2addr v1, v2

    iput v1, p0, Lcom/a/c/d/a;->F:I

    .line 1096
    return-object v0

    .line 1068
    :cond_2b
    iget-object v3, p0, Lcom/a/c/d/a;->E:[C

    array-length v3, v3

    if-ge v1, v3, :cond_39

    .line 1069
    add-int/lit8 v3, v1, 0x1

    invoke-direct {p0, v3}, Lcom/a/c/d/a;->b(I)Z

    move-result v3

    if-eqz v3, :cond_1a

    goto :goto_3

    .line 1077
    :cond_39
    if-nez v0, :cond_40

    .line 1078
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1080
    :cond_40
    iget-object v3, p0, Lcom/a/c/d/a;->E:[C

    iget v4, p0, Lcom/a/c/d/a;->F:I

    invoke-virtual {v0, v3, v4, v1}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    .line 1081
    iget v3, p0, Lcom/a/c/d/a;->F:I

    add-int/2addr v1, v3

    iput v1, p0, Lcom/a/c/d/a;->F:I

    .line 1083
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/a/c/d/a;->b(I)Z

    move-result v1

    if-nez v1, :cond_61

    move v1, v2

    goto :goto_1a

    .line 1092
    :cond_55
    iget-object v2, p0, Lcom/a/c/d/a;->E:[C

    iget v3, p0, Lcom/a/c/d/a;->F:I

    invoke-virtual {v0, v2, v3, v1}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    .line 1093
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_25

    :cond_61
    move v1, v2

    goto :goto_3

    .line 1045
    nop

    :sswitch_data_64
    .sparse-switch
        0x9 -> :sswitch_1a
        0xa -> :sswitch_1a
        0xc -> :sswitch_1a
        0xd -> :sswitch_1a
        0x20 -> :sswitch_1a
        0x23 -> :sswitch_17
        0x2c -> :sswitch_1a
        0x2f -> :sswitch_17
        0x3a -> :sswitch_1a
        0x3b -> :sswitch_17
        0x3d -> :sswitch_17
        0x5b -> :sswitch_1a
        0x5c -> :sswitch_17
        0x5d -> :sswitch_1a
        0x7b -> :sswitch_1a
        0x7d -> :sswitch_1a
    .end sparse-switch
.end method

.method private u()V
    .registers 4

    .prologue
    .line 1128
    :cond_0
    const/4 v0, 0x0

    .line 1129
    :goto_1
    iget v1, p0, Lcom/a/c/d/a;->F:I

    add-int/2addr v1, v0

    iget v2, p0, Lcom/a/c/d/a;->G:I

    if-ge v1, v2, :cond_1e

    .line 1130
    iget-object v1, p0, Lcom/a/c/d/a;->E:[C

    iget v2, p0, Lcom/a/c/d/a;->F:I

    add-int/2addr v2, v0

    aget-char v1, v1, v2

    sparse-switch v1, :sswitch_data_2c

    .line 1129
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1136
    :sswitch_15
    invoke-direct {p0}, Lcom/a/c/d/a;->x()V

    .line 1148
    :sswitch_18
    iget v1, p0, Lcom/a/c/d/a;->F:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/a/c/d/a;->F:I

    .line 1154
    :goto_1d
    return-void

    .line 1152
    :cond_1e
    iget v1, p0, Lcom/a/c/d/a;->F:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/a/c/d/a;->F:I

    .line 1153
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/a/c/d/a;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_1d

    .line 1130
    nop

    :sswitch_data_2c
    .sparse-switch
        0x9 -> :sswitch_18
        0xa -> :sswitch_18
        0xc -> :sswitch_18
        0xd -> :sswitch_18
        0x20 -> :sswitch_18
        0x23 -> :sswitch_15
        0x2c -> :sswitch_18
        0x2f -> :sswitch_15
        0x3a -> :sswitch_18
        0x3b -> :sswitch_15
        0x3d -> :sswitch_15
        0x5b -> :sswitch_18
        0x5c -> :sswitch_15
        0x5d -> :sswitch_18
        0x7b -> :sswitch_18
        0x7d -> :sswitch_18
    .end sparse-switch
.end method

.method private v()I
    .registers 2

    .prologue
    .line 1316
    iget v0, p0, Lcom/a/c/d/a;->H:I

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method private w()I
    .registers 3

    .prologue
    .line 1320
    iget v0, p0, Lcom/a/c/d/a;->F:I

    iget v1, p0, Lcom/a/c/d/a;->I:I

    sub-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method private x()V
    .registers 2

    .prologue
    .line 1420
    iget-boolean v0, p0, Lcom/a/c/d/a;->b:Z

    if-nez v0, :cond_b

    .line 1421
    const-string v0, "Use JsonReader.setLenient(true) to accept malformed JSON"

    invoke-direct {p0, v0}, Lcom/a/c/d/a;->b(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 1423
    :cond_b
    return-void
.end method

.method private y()V
    .registers 4

    .prologue
    .line 1431
    :cond_0
    iget v0, p0, Lcom/a/c/d/a;->F:I

    iget v1, p0, Lcom/a/c/d/a;->G:I

    if-lt v0, v1, :cond_d

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/a/c/d/a;->b(I)Z

    move-result v0

    if-eqz v0, :cond_25

    .line 1432
    :cond_d
    iget-object v0, p0, Lcom/a/c/d/a;->E:[C

    iget v1, p0, Lcom/a/c/d/a;->F:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/a/c/d/a;->F:I

    aget-char v0, v0, v1

    .line 1433
    const/16 v1, 0xa

    if-ne v0, v1, :cond_26

    .line 1434
    iget v0, p0, Lcom/a/c/d/a;->H:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/a/c/d/a;->H:I

    .line 1435
    iget v0, p0, Lcom/a/c/d/a;->F:I

    iput v0, p0, Lcom/a/c/d/a;->I:I

    .line 1441
    :cond_25
    :goto_25
    return-void

    .line 1437
    :cond_26
    const/16 v1, 0xd

    if-ne v0, v1, :cond_0

    goto :goto_25
.end method

.method private z()C
    .registers 8

    .prologue
    const/4 v5, 0x4

    .line 1510
    iget v0, p0, Lcom/a/c/d/a;->F:I

    iget v1, p0, Lcom/a/c/d/a;->G:I

    if-ne v0, v1, :cond_15

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/a/c/d/a;->b(I)Z

    move-result v0

    if-nez v0, :cond_15

    .line 1511
    const-string v0, "Unterminated escape sequence"

    invoke-direct {p0, v0}, Lcom/a/c/d/a;->b(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 1514
    :cond_15
    iget-object v0, p0, Lcom/a/c/d/a;->E:[C

    iget v1, p0, Lcom/a/c/d/a;->F:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/a/c/d/a;->F:I

    aget-char v0, v0, v1

    .line 1515
    sparse-switch v0, :sswitch_data_ba

    .line 1562
    :goto_22
    return v0

    .line 1517
    :sswitch_23
    iget v0, p0, Lcom/a/c/d/a;->F:I

    add-int/lit8 v0, v0, 0x4

    iget v1, p0, Lcom/a/c/d/a;->G:I

    if-le v0, v1, :cond_38

    invoke-direct {p0, v5}, Lcom/a/c/d/a;->b(I)Z

    move-result v0

    if-nez v0, :cond_38

    .line 1518
    const-string v0, "Unterminated escape sequence"

    invoke-direct {p0, v0}, Lcom/a/c/d/a;->b(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 1521
    :cond_38
    const/4 v1, 0x0

    .line 1522
    iget v0, p0, Lcom/a/c/d/a;->F:I

    add-int/lit8 v2, v0, 0x4

    move v6, v0

    move v0, v1

    move v1, v6

    :goto_40
    if-ge v1, v2, :cond_94

    .line 1523
    iget-object v3, p0, Lcom/a/c/d/a;->E:[C

    aget-char v3, v3, v1

    .line 1524
    shl-int/lit8 v0, v0, 0x4

    int-to-char v0, v0

    .line 1525
    const/16 v4, 0x30

    if-lt v3, v4, :cond_58

    const/16 v4, 0x39

    if-gt v3, v4, :cond_58

    .line 1526
    add-int/lit8 v3, v3, -0x30

    add-int/2addr v0, v3

    int-to-char v0, v0

    .line 1522
    :goto_55
    add-int/lit8 v1, v1, 0x1

    goto :goto_40

    .line 1527
    :cond_58
    const/16 v4, 0x61

    if-lt v3, v4, :cond_67

    const/16 v4, 0x66

    if-gt v3, v4, :cond_67

    .line 1528
    add-int/lit8 v3, v3, -0x61

    add-int/lit8 v3, v3, 0xa

    add-int/2addr v0, v3

    int-to-char v0, v0

    goto :goto_55

    .line 1529
    :cond_67
    const/16 v4, 0x41

    if-lt v3, v4, :cond_76

    const/16 v4, 0x46

    if-gt v3, v4, :cond_76

    .line 1530
    add-int/lit8 v3, v3, -0x41

    add-int/lit8 v3, v3, 0xa

    add-int/2addr v0, v3

    int-to-char v0, v0

    goto :goto_55

    .line 1532
    :cond_76
    new-instance v0, Ljava/lang/NumberFormatException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "\\u"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v2, Ljava/lang/String;

    iget-object v3, p0, Lcom/a/c/d/a;->E:[C

    iget v4, p0, Lcom/a/c/d/a;->F:I

    invoke-direct {v2, v3, v4, v5}, Ljava/lang/String;-><init>([CII)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1535
    :cond_94
    iget v1, p0, Lcom/a/c/d/a;->F:I

    add-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/a/c/d/a;->F:I

    goto :goto_22

    .line 1539
    :sswitch_9b
    const/16 v0, 0x9

    goto :goto_22

    .line 1542
    :sswitch_9e
    const/16 v0, 0x8

    goto :goto_22

    .line 1545
    :sswitch_a1
    const/16 v0, 0xa

    goto/16 :goto_22

    .line 1548
    :sswitch_a5
    const/16 v0, 0xd

    goto/16 :goto_22

    .line 1551
    :sswitch_a9
    const/16 v0, 0xc

    goto/16 :goto_22

    .line 1554
    :sswitch_ad
    iget v1, p0, Lcom/a/c/d/a;->H:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/a/c/d/a;->H:I

    .line 1555
    iget v1, p0, Lcom/a/c/d/a;->F:I

    iput v1, p0, Lcom/a/c/d/a;->I:I

    goto/16 :goto_22

    .line 1515
    nop

    :sswitch_data_ba
    .sparse-switch
        0xa -> :sswitch_ad
        0x62 -> :sswitch_9e
        0x66 -> :sswitch_a9
        0x6e -> :sswitch_a1
        0x72 -> :sswitch_a5
        0x74 -> :sswitch_9b
        0x75 -> :sswitch_23
    .end sparse-switch
.end method


# virtual methods
.method public a()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 341
    iget v0, p0, Lcom/a/c/d/a;->J:I

    .line 342
    if-nez v0, :cond_9

    .line 343
    invoke-direct {p0}, Lcom/a/c/d/a;->q()I

    move-result v0

    .line 345
    :cond_9
    const/4 v1, 0x3

    if-ne v0, v1, :cond_1b

    .line 346
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/a/c/d/a;->a(I)V

    .line 347
    iget-object v0, p0, Lcom/a/c/d/a;->Q:[I

    iget v1, p0, Lcom/a/c/d/a;->O:I

    add-int/lit8 v1, v1, -0x1

    aput v2, v0, v1

    .line 348
    iput v2, p0, Lcom/a/c/d/a;->J:I

    return-void

    .line 350
    :cond_1b
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Expected BEGIN_ARRAY but was "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/a/c/d/a;->f()Lcom/a/c/d/d;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " at line "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 2316
    iget v2, p0, Lcom/a/c/d/a;->H:I

    add-int/lit8 v2, v2, 0x1

    .line 351
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " column "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lcom/a/c/d/a;->w()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " path "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/a/c/d/a;->p()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public b()V
    .registers 4

    .prologue
    .line 360
    iget v0, p0, Lcom/a/c/d/a;->J:I

    .line 361
    if-nez v0, :cond_8

    .line 362
    invoke-direct {p0}, Lcom/a/c/d/a;->q()I

    move-result v0

    .line 364
    :cond_8
    const/4 v1, 0x4

    if-ne v0, v1, :cond_15

    .line 365
    iget v0, p0, Lcom/a/c/d/a;->O:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/a/c/d/a;->O:I

    .line 366
    const/4 v0, 0x0

    iput v0, p0, Lcom/a/c/d/a;->J:I

    return-void

    .line 368
    :cond_15
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Expected END_ARRAY but was "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/a/c/d/a;->f()Lcom/a/c/d/d;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " at line "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 3316
    iget v2, p0, Lcom/a/c/d/a;->H:I

    add-int/lit8 v2, v2, 0x1

    .line 369
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " column "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lcom/a/c/d/a;->w()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " path "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/a/c/d/a;->p()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public c()V
    .registers 4

    .prologue
    .line 378
    iget v0, p0, Lcom/a/c/d/a;->J:I

    .line 379
    if-nez v0, :cond_8

    .line 380
    invoke-direct {p0}, Lcom/a/c/d/a;->q()I

    move-result v0

    .line 382
    :cond_8
    const/4 v1, 0x1

    if-ne v0, v1, :cond_13

    .line 383
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/a/c/d/a;->a(I)V

    .line 384
    const/4 v0, 0x0

    iput v0, p0, Lcom/a/c/d/a;->J:I

    return-void

    .line 386
    :cond_13
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Expected BEGIN_OBJECT but was "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/a/c/d/a;->f()Lcom/a/c/d/d;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " at line "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 4316
    iget v2, p0, Lcom/a/c/d/a;->H:I

    add-int/lit8 v2, v2, 0x1

    .line 387
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " column "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lcom/a/c/d/a;->w()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " path "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/a/c/d/a;->p()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public close()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 1219
    iput v2, p0, Lcom/a/c/d/a;->J:I

    .line 1220
    iget-object v0, p0, Lcom/a/c/d/a;->N:[I

    const/16 v1, 0x8

    aput v1, v0, v2

    .line 1221
    const/4 v0, 0x1

    iput v0, p0, Lcom/a/c/d/a;->O:I

    .line 1222
    iget-object v0, p0, Lcom/a/c/d/a;->D:Ljava/io/Reader;

    invoke-virtual {v0}, Ljava/io/Reader;->close()V

    .line 1223
    return-void
.end method

.method public d()V
    .registers 4

    .prologue
    .line 396
    iget v0, p0, Lcom/a/c/d/a;->J:I

    .line 397
    if-nez v0, :cond_8

    .line 398
    invoke-direct {p0}, Lcom/a/c/d/a;->q()I

    move-result v0

    .line 400
    :cond_8
    const/4 v1, 0x2

    if-ne v0, v1, :cond_1c

    .line 401
    iget v0, p0, Lcom/a/c/d/a;->O:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/a/c/d/a;->O:I

    .line 402
    iget-object v0, p0, Lcom/a/c/d/a;->P:[Ljava/lang/String;

    iget v1, p0, Lcom/a/c/d/a;->O:I

    const/4 v2, 0x0

    aput-object v2, v0, v1

    .line 403
    const/4 v0, 0x0

    iput v0, p0, Lcom/a/c/d/a;->J:I

    return-void

    .line 405
    :cond_1c
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Expected END_OBJECT but was "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/a/c/d/a;->f()Lcom/a/c/d/d;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " at line "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 5316
    iget v2, p0, Lcom/a/c/d/a;->H:I

    add-int/lit8 v2, v2, 0x1

    .line 406
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " column "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lcom/a/c/d/a;->w()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " path "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/a/c/d/a;->p()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public e()Z
    .registers 3

    .prologue
    .line 414
    iget v0, p0, Lcom/a/c/d/a;->J:I

    .line 415
    if-nez v0, :cond_8

    .line 416
    invoke-direct {p0}, Lcom/a/c/d/a;->q()I

    move-result v0

    .line 418
    :cond_8
    const/4 v1, 0x2

    if-eq v0, v1, :cond_10

    const/4 v1, 0x4

    if-eq v0, v1, :cond_10

    const/4 v0, 0x1

    :goto_f
    return v0

    :cond_10
    const/4 v0, 0x0

    goto :goto_f
.end method

.method public f()Lcom/a/c/d/d;
    .registers 2

    .prologue
    .line 425
    iget v0, p0, Lcom/a/c/d/a;->J:I

    .line 426
    if-nez v0, :cond_8

    .line 427
    invoke-direct {p0}, Lcom/a/c/d/a;->q()I

    move-result v0

    .line 430
    :cond_8
    packed-switch v0, :pswitch_data_30

    .line 459
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 432
    :pswitch_11
    sget-object v0, Lcom/a/c/d/d;->c:Lcom/a/c/d/d;

    .line 457
    :goto_13
    return-object v0

    .line 434
    :pswitch_14
    sget-object v0, Lcom/a/c/d/d;->d:Lcom/a/c/d/d;

    goto :goto_13

    .line 436
    :pswitch_17
    sget-object v0, Lcom/a/c/d/d;->a:Lcom/a/c/d/d;

    goto :goto_13

    .line 438
    :pswitch_1a
    sget-object v0, Lcom/a/c/d/d;->b:Lcom/a/c/d/d;

    goto :goto_13

    .line 442
    :pswitch_1d
    sget-object v0, Lcom/a/c/d/d;->e:Lcom/a/c/d/d;

    goto :goto_13

    .line 445
    :pswitch_20
    sget-object v0, Lcom/a/c/d/d;->h:Lcom/a/c/d/d;

    goto :goto_13

    .line 447
    :pswitch_23
    sget-object v0, Lcom/a/c/d/d;->i:Lcom/a/c/d/d;

    goto :goto_13

    .line 452
    :pswitch_26
    sget-object v0, Lcom/a/c/d/d;->f:Lcom/a/c/d/d;

    goto :goto_13

    .line 455
    :pswitch_29
    sget-object v0, Lcom/a/c/d/d;->g:Lcom/a/c/d/d;

    goto :goto_13

    .line 457
    :pswitch_2c
    sget-object v0, Lcom/a/c/d/d;->j:Lcom/a/c/d/d;

    goto :goto_13

    .line 430
    nop

    :pswitch_data_30
    .packed-switch 0x1
        :pswitch_11
        :pswitch_14
        :pswitch_17
        :pswitch_1a
        :pswitch_20
        :pswitch_20
        :pswitch_23
        :pswitch_26
        :pswitch_26
        :pswitch_26
        :pswitch_26
        :pswitch_1d
        :pswitch_1d
        :pswitch_1d
        :pswitch_29
        :pswitch_29
        :pswitch_2c
    .end packed-switch
.end method

.method public h()Ljava/lang/String;
    .registers 4

    .prologue
    .line 786
    iget v0, p0, Lcom/a/c/d/a;->J:I

    .line 787
    if-nez v0, :cond_8

    .line 788
    invoke-direct {p0}, Lcom/a/c/d/a;->q()I

    move-result v0

    .line 791
    :cond_8
    const/16 v1, 0xe

    if-ne v0, v1, :cond_1c

    .line 792
    invoke-direct {p0}, Lcom/a/c/d/a;->t()Ljava/lang/String;

    move-result-object v0

    .line 801
    :goto_10
    const/4 v1, 0x0

    iput v1, p0, Lcom/a/c/d/a;->J:I

    .line 802
    iget-object v1, p0, Lcom/a/c/d/a;->P:[Ljava/lang/String;

    iget v2, p0, Lcom/a/c/d/a;->O:I

    add-int/lit8 v2, v2, -0x1

    aput-object v0, v1, v2

    .line 803
    return-object v0

    .line 793
    :cond_1c
    const/16 v1, 0xc

    if-ne v0, v1, :cond_27

    .line 794
    const/16 v0, 0x27

    invoke-direct {p0, v0}, Lcom/a/c/d/a;->b(C)Ljava/lang/String;

    move-result-object v0

    goto :goto_10

    .line 795
    :cond_27
    const/16 v1, 0xd

    if-ne v0, v1, :cond_32

    .line 796
    const/16 v0, 0x22

    invoke-direct {p0, v0}, Lcom/a/c/d/a;->b(C)Ljava/lang/String;

    move-result-object v0

    goto :goto_10

    .line 798
    :cond_32
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Expected a name but was "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/a/c/d/a;->f()Lcom/a/c/d/d;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " at line "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 6316
    iget v2, p0, Lcom/a/c/d/a;->H:I

    add-int/lit8 v2, v2, 0x1

    .line 799
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " column "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lcom/a/c/d/a;->w()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " path "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/a/c/d/a;->p()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public i()Ljava/lang/String;
    .registers 5

    .prologue
    .line 815
    iget v0, p0, Lcom/a/c/d/a;->J:I

    .line 816
    if-nez v0, :cond_8

    .line 817
    invoke-direct {p0}, Lcom/a/c/d/a;->q()I

    move-result v0

    .line 820
    :cond_8
    const/16 v1, 0xa

    if-ne v0, v1, :cond_20

    .line 821
    invoke-direct {p0}, Lcom/a/c/d/a;->t()Ljava/lang/String;

    move-result-object v0

    .line 838
    :goto_10
    const/4 v1, 0x0

    iput v1, p0, Lcom/a/c/d/a;->J:I

    .line 839
    iget-object v1, p0, Lcom/a/c/d/a;->Q:[I

    iget v2, p0, Lcom/a/c/d/a;->O:I

    add-int/lit8 v2, v2, -0x1

    aget v3, v1, v2

    add-int/lit8 v3, v3, 0x1

    aput v3, v1, v2

    .line 840
    return-object v0

    .line 822
    :cond_20
    const/16 v1, 0x8

    if-ne v0, v1, :cond_2b

    .line 823
    const/16 v0, 0x27

    invoke-direct {p0, v0}, Lcom/a/c/d/a;->b(C)Ljava/lang/String;

    move-result-object v0

    goto :goto_10

    .line 824
    :cond_2b
    const/16 v1, 0x9

    if-ne v0, v1, :cond_36

    .line 825
    const/16 v0, 0x22

    invoke-direct {p0, v0}, Lcom/a/c/d/a;->b(C)Ljava/lang/String;

    move-result-object v0

    goto :goto_10

    .line 826
    :cond_36
    const/16 v1, 0xb

    if-ne v0, v1, :cond_40

    .line 827
    iget-object v0, p0, Lcom/a/c/d/a;->M:Ljava/lang/String;

    .line 828
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/a/c/d/a;->M:Ljava/lang/String;

    goto :goto_10

    .line 829
    :cond_40
    const/16 v1, 0xf

    if-ne v0, v1, :cond_4b

    .line 830
    iget-wide v0, p0, Lcom/a/c/d/a;->K:J

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    goto :goto_10

    .line 831
    :cond_4b
    const/16 v1, 0x10

    if-ne v0, v1, :cond_62

    .line 832
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/a/c/d/a;->E:[C

    iget v2, p0, Lcom/a/c/d/a;->F:I

    iget v3, p0, Lcom/a/c/d/a;->L:I

    invoke-direct {v0, v1, v2, v3}, Ljava/lang/String;-><init>([CII)V

    .line 833
    iget v1, p0, Lcom/a/c/d/a;->F:I

    iget v2, p0, Lcom/a/c/d/a;->L:I

    add-int/2addr v1, v2

    iput v1, p0, Lcom/a/c/d/a;->F:I

    goto :goto_10

    .line 835
    :cond_62
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Expected a string but was "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/a/c/d/a;->f()Lcom/a/c/d/d;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " at line "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 7316
    iget v2, p0, Lcom/a/c/d/a;->H:I

    add-int/lit8 v2, v2, 0x1

    .line 836
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " column "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lcom/a/c/d/a;->w()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " path "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/a/c/d/a;->p()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public j()Z
    .registers 5

    .prologue
    const/4 v0, 0x0

    .line 851
    iget v1, p0, Lcom/a/c/d/a;->J:I

    .line 852
    if-nez v1, :cond_9

    .line 853
    invoke-direct {p0}, Lcom/a/c/d/a;->q()I

    move-result v1

    .line 855
    :cond_9
    const/4 v2, 0x5

    if-ne v1, v2, :cond_1c

    .line 856
    iput v0, p0, Lcom/a/c/d/a;->J:I

    .line 857
    iget-object v0, p0, Lcom/a/c/d/a;->Q:[I

    iget v1, p0, Lcom/a/c/d/a;->O:I

    add-int/lit8 v1, v1, -0x1

    aget v2, v0, v1

    add-int/lit8 v2, v2, 0x1

    aput v2, v0, v1

    .line 858
    const/4 v0, 0x1

    .line 862
    :goto_1b
    return v0

    .line 859
    :cond_1c
    const/4 v2, 0x6

    if-ne v1, v2, :cond_2e

    .line 860
    iput v0, p0, Lcom/a/c/d/a;->J:I

    .line 861
    iget-object v1, p0, Lcom/a/c/d/a;->Q:[I

    iget v2, p0, Lcom/a/c/d/a;->O:I

    add-int/lit8 v2, v2, -0x1

    aget v3, v1, v2

    add-int/lit8 v3, v3, 0x1

    aput v3, v1, v2

    goto :goto_1b

    .line 864
    :cond_2e
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Expected a boolean but was "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/a/c/d/a;->f()Lcom/a/c/d/d;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " at line "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 8316
    iget v2, p0, Lcom/a/c/d/a;->H:I

    add-int/lit8 v2, v2, 0x1

    .line 865
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " column "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lcom/a/c/d/a;->w()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " path "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/a/c/d/a;->p()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public k()V
    .registers 4

    .prologue
    .line 876
    iget v0, p0, Lcom/a/c/d/a;->J:I

    .line 877
    if-nez v0, :cond_8

    .line 878
    invoke-direct {p0}, Lcom/a/c/d/a;->q()I

    move-result v0

    .line 880
    :cond_8
    const/4 v1, 0x7

    if-ne v0, v1, :cond_1b

    .line 881
    const/4 v0, 0x0

    iput v0, p0, Lcom/a/c/d/a;->J:I

    .line 882
    iget-object v0, p0, Lcom/a/c/d/a;->Q:[I

    iget v1, p0, Lcom/a/c/d/a;->O:I

    add-int/lit8 v1, v1, -0x1

    aget v2, v0, v1

    add-int/lit8 v2, v2, 0x1

    aput v2, v0, v1

    return-void

    .line 884
    :cond_1b
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Expected null but was "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/a/c/d/a;->f()Lcom/a/c/d/d;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " at line "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 9316
    iget v2, p0, Lcom/a/c/d/a;->H:I

    add-int/lit8 v2, v2, 0x1

    .line 885
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " column "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lcom/a/c/d/a;->w()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " path "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/a/c/d/a;->p()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public l()D
    .registers 7

    .prologue
    const/16 v5, 0xb

    const/16 v2, 0x8

    const/4 v4, 0x0

    .line 899
    iget v0, p0, Lcom/a/c/d/a;->J:I

    .line 900
    if-nez v0, :cond_d

    .line 901
    invoke-direct {p0}, Lcom/a/c/d/a;->q()I

    move-result v0

    .line 904
    :cond_d
    const/16 v1, 0xf

    if-ne v0, v1, :cond_23

    .line 905
    iput v4, p0, Lcom/a/c/d/a;->J:I

    .line 906
    iget-object v0, p0, Lcom/a/c/d/a;->Q:[I

    iget v1, p0, Lcom/a/c/d/a;->O:I

    add-int/lit8 v1, v1, -0x1

    aget v2, v0, v1

    add-int/lit8 v2, v2, 0x1

    aput v2, v0, v1

    .line 907
    iget-wide v0, p0, Lcom/a/c/d/a;->K:J

    long-to-double v0, v0

    .line 931
    :goto_22
    return-wide v0

    .line 910
    :cond_23
    const/16 v1, 0x10

    if-ne v0, v1, :cond_92

    .line 911
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/a/c/d/a;->E:[C

    iget v2, p0, Lcom/a/c/d/a;->F:I

    iget v3, p0, Lcom/a/c/d/a;->L:I

    invoke-direct {v0, v1, v2, v3}, Ljava/lang/String;-><init>([CII)V

    iput-object v0, p0, Lcom/a/c/d/a;->M:Ljava/lang/String;

    .line 912
    iget v0, p0, Lcom/a/c/d/a;->F:I

    iget v1, p0, Lcom/a/c/d/a;->L:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/a/c/d/a;->F:I

    .line 922
    :cond_3b
    :goto_3b
    iput v5, p0, Lcom/a/c/d/a;->J:I

    .line 923
    iget-object v0, p0, Lcom/a/c/d/a;->M:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v0

    .line 924
    iget-boolean v2, p0, Lcom/a/c/d/a;->b:Z

    if-nez v2, :cond_f6

    invoke-static {v0, v1}, Ljava/lang/Double;->isNaN(D)Z

    move-result v2

    if-nez v2, :cond_53

    invoke-static {v0, v1}, Ljava/lang/Double;->isInfinite(D)Z

    move-result v2

    if-eqz v2, :cond_f6

    .line 925
    :cond_53
    new-instance v2, Lcom/a/c/d/f;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "JSON forbids NaN and infinities: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " at line "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 11316
    iget v1, p0, Lcom/a/c/d/a;->H:I

    add-int/lit8 v1, v1, 0x1

    .line 926
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " column "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0}, Lcom/a/c/d/a;->w()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " path "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/a/c/d/a;->p()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/a/c/d/f;-><init>(Ljava/lang/String;)V

    throw v2

    .line 913
    :cond_92
    if-eq v0, v2, :cond_98

    const/16 v1, 0x9

    if-ne v0, v1, :cond_a6

    .line 914
    :cond_98
    if-ne v0, v2, :cond_a3

    const/16 v0, 0x27

    :goto_9c
    invoke-direct {p0, v0}, Lcom/a/c/d/a;->b(C)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/a/c/d/a;->M:Ljava/lang/String;

    goto :goto_3b

    :cond_a3
    const/16 v0, 0x22

    goto :goto_9c

    .line 915
    :cond_a6
    const/16 v1, 0xa

    if-ne v0, v1, :cond_b1

    .line 916
    invoke-direct {p0}, Lcom/a/c/d/a;->t()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/a/c/d/a;->M:Ljava/lang/String;

    goto :goto_3b

    .line 917
    :cond_b1
    if-eq v0, v5, :cond_3b

    .line 918
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Expected a double but was "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/a/c/d/a;->f()Lcom/a/c/d/d;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " at line "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 10316
    iget v2, p0, Lcom/a/c/d/a;->H:I

    add-int/lit8 v2, v2, 0x1

    .line 919
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " column "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lcom/a/c/d/a;->w()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " path "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/a/c/d/a;->p()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 928
    :cond_f6
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/a/c/d/a;->M:Ljava/lang/String;

    .line 929
    iput v4, p0, Lcom/a/c/d/a;->J:I

    .line 930
    iget-object v2, p0, Lcom/a/c/d/a;->Q:[I

    iget v3, p0, Lcom/a/c/d/a;->O:I

    add-int/lit8 v3, v3, -0x1

    aget v4, v2, v3

    add-int/lit8 v4, v4, 0x1

    aput v4, v2, v3

    goto/16 :goto_22
.end method

.method public m()J
    .registers 8

    .prologue
    const/16 v2, 0x8

    const/4 v6, 0x0

    .line 945
    iget v0, p0, Lcom/a/c/d/a;->J:I

    .line 946
    if-nez v0, :cond_b

    .line 947
    invoke-direct {p0}, Lcom/a/c/d/a;->q()I

    move-result v0

    .line 950
    :cond_b
    const/16 v1, 0xf

    if-ne v0, v1, :cond_20

    .line 951
    iput v6, p0, Lcom/a/c/d/a;->J:I

    .line 952
    iget-object v0, p0, Lcom/a/c/d/a;->Q:[I

    iget v1, p0, Lcom/a/c/d/a;->O:I

    add-int/lit8 v1, v1, -0x1

    aget v2, v0, v1

    add-int/lit8 v2, v2, 0x1

    aput v2, v0, v1

    .line 953
    iget-wide v0, p0, Lcom/a/c/d/a;->K:J

    .line 984
    :goto_1f
    return-wide v0

    .line 956
    :cond_20
    const/16 v1, 0x10

    if-ne v0, v1, :cond_89

    .line 957
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/a/c/d/a;->E:[C

    iget v2, p0, Lcom/a/c/d/a;->F:I

    iget v3, p0, Lcom/a/c/d/a;->L:I

    invoke-direct {v0, v1, v2, v3}, Ljava/lang/String;-><init>([CII)V

    iput-object v0, p0, Lcom/a/c/d/a;->M:Ljava/lang/String;

    .line 958
    iget v0, p0, Lcom/a/c/d/a;->F:I

    iget v1, p0, Lcom/a/c/d/a;->L:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/a/c/d/a;->F:I

    .line 974
    :goto_38
    const/16 v0, 0xb

    iput v0, p0, Lcom/a/c/d/a;->J:I

    .line 975
    iget-object v0, p0, Lcom/a/c/d/a;->M:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v2

    .line 976
    double-to-long v0, v2

    .line 977
    long-to-double v4, v0

    cmpl-double v2, v4, v2

    if-eqz v2, :cond_f8

    .line 978
    new-instance v0, Ljava/lang/NumberFormatException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Expected a long but was "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/a/c/d/a;->M:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " at line "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 13316
    iget v2, p0, Lcom/a/c/d/a;->H:I

    add-int/lit8 v2, v2, 0x1

    .line 979
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " column "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lcom/a/c/d/a;->w()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " path "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/a/c/d/a;->p()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 959
    :cond_89
    if-eq v0, v2, :cond_8f

    const/16 v1, 0x9

    if-ne v0, v1, :cond_b5

    .line 960
    :cond_8f
    if-ne v0, v2, :cond_b2

    const/16 v0, 0x27

    :goto_93
    invoke-direct {p0, v0}, Lcom/a/c/d/a;->b(C)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/a/c/d/a;->M:Ljava/lang/String;

    .line 962
    :try_start_99
    iget-object v0, p0, Lcom/a/c/d/a;->M:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 963
    const/4 v2, 0x0

    iput v2, p0, Lcom/a/c/d/a;->J:I

    .line 964
    iget-object v2, p0, Lcom/a/c/d/a;->Q:[I

    iget v3, p0, Lcom/a/c/d/a;->O:I

    add-int/lit8 v3, v3, -0x1

    aget v4, v2, v3

    add-int/lit8 v4, v4, 0x1

    aput v4, v2, v3
    :try_end_ae
    .catch Ljava/lang/NumberFormatException; {:try_start_99 .. :try_end_ae} :catch_b0

    goto/16 :goto_1f

    .line 968
    :catch_b0
    move-exception v0

    goto :goto_38

    .line 960
    :cond_b2
    const/16 v0, 0x22

    goto :goto_93

    .line 970
    :cond_b5
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Expected a long but was "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/a/c/d/a;->f()Lcom/a/c/d/d;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " at line "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 12316
    iget v2, p0, Lcom/a/c/d/a;->H:I

    add-int/lit8 v2, v2, 0x1

    .line 971
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " column "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lcom/a/c/d/a;->w()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " path "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/a/c/d/a;->p()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 981
    :cond_f8
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/a/c/d/a;->M:Ljava/lang/String;

    .line 982
    iput v6, p0, Lcom/a/c/d/a;->J:I

    .line 983
    iget-object v2, p0, Lcom/a/c/d/a;->Q:[I

    iget v3, p0, Lcom/a/c/d/a;->O:I

    add-int/lit8 v3, v3, -0x1

    aget v4, v2, v3

    add-int/lit8 v4, v4, 0x1

    aput v4, v2, v3

    goto/16 :goto_1f
.end method

.method public n()I
    .registers 8

    .prologue
    const/16 v2, 0x8

    const/4 v6, 0x0

    .line 1167
    iget v0, p0, Lcom/a/c/d/a;->J:I

    .line 1168
    if-nez v0, :cond_b

    .line 1169
    invoke-direct {p0}, Lcom/a/c/d/a;->q()I

    move-result v0

    .line 1173
    :cond_b
    const/16 v1, 0xf

    if-ne v0, v1, :cond_69

    .line 1174
    iget-wide v0, p0, Lcom/a/c/d/a;->K:J

    long-to-int v0, v0

    .line 1175
    iget-wide v2, p0, Lcom/a/c/d/a;->K:J

    int-to-long v4, v0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_5a

    .line 1176
    new-instance v0, Ljava/lang/NumberFormatException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Expected an int but was "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, Lcom/a/c/d/a;->K:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " at line "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 14316
    iget v2, p0, Lcom/a/c/d/a;->H:I

    add-int/lit8 v2, v2, 0x1

    .line 1177
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " column "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lcom/a/c/d/a;->w()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " path "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/a/c/d/a;->p()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1179
    :cond_5a
    iput v6, p0, Lcom/a/c/d/a;->J:I

    .line 1180
    iget-object v1, p0, Lcom/a/c/d/a;->Q:[I

    iget v2, p0, Lcom/a/c/d/a;->O:I

    add-int/lit8 v2, v2, -0x1

    aget v3, v1, v2

    add-int/lit8 v3, v3, 0x1

    aput v3, v1, v2

    .line 1212
    :goto_68
    return v0

    .line 1184
    :cond_69
    const/16 v1, 0x10

    if-ne v0, v1, :cond_d2

    .line 1185
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/a/c/d/a;->E:[C

    iget v2, p0, Lcom/a/c/d/a;->F:I

    iget v3, p0, Lcom/a/c/d/a;->L:I

    invoke-direct {v0, v1, v2, v3}, Ljava/lang/String;-><init>([CII)V

    iput-object v0, p0, Lcom/a/c/d/a;->M:Ljava/lang/String;

    .line 1186
    iget v0, p0, Lcom/a/c/d/a;->F:I

    iget v1, p0, Lcom/a/c/d/a;->L:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/a/c/d/a;->F:I

    .line 1202
    :goto_81
    const/16 v0, 0xb

    iput v0, p0, Lcom/a/c/d/a;->J:I

    .line 1203
    iget-object v0, p0, Lcom/a/c/d/a;->M:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v2

    .line 1204
    double-to-int v0, v2

    .line 1205
    int-to-double v4, v0

    cmpl-double v1, v4, v2

    if-eqz v1, :cond_141

    .line 1206
    new-instance v0, Ljava/lang/NumberFormatException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Expected an int but was "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/a/c/d/a;->M:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " at line "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 16316
    iget v2, p0, Lcom/a/c/d/a;->H:I

    add-int/lit8 v2, v2, 0x1

    .line 1207
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " column "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lcom/a/c/d/a;->w()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " path "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/a/c/d/a;->p()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1187
    :cond_d2
    if-eq v0, v2, :cond_d8

    const/16 v1, 0x9

    if-ne v0, v1, :cond_fe

    .line 1188
    :cond_d8
    if-ne v0, v2, :cond_fb

    const/16 v0, 0x27

    :goto_dc
    invoke-direct {p0, v0}, Lcom/a/c/d/a;->b(C)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/a/c/d/a;->M:Ljava/lang/String;

    .line 1190
    :try_start_e2
    iget-object v0, p0, Lcom/a/c/d/a;->M:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 1191
    const/4 v1, 0x0

    iput v1, p0, Lcom/a/c/d/a;->J:I

    .line 1192
    iget-object v1, p0, Lcom/a/c/d/a;->Q:[I

    iget v2, p0, Lcom/a/c/d/a;->O:I

    add-int/lit8 v2, v2, -0x1

    aget v3, v1, v2

    add-int/lit8 v3, v3, 0x1

    aput v3, v1, v2
    :try_end_f7
    .catch Ljava/lang/NumberFormatException; {:try_start_e2 .. :try_end_f7} :catch_f9

    goto/16 :goto_68

    .line 1196
    :catch_f9
    move-exception v0

    goto :goto_81

    .line 1188
    :cond_fb
    const/16 v0, 0x22

    goto :goto_dc

    .line 1198
    :cond_fe
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Expected an int but was "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/a/c/d/a;->f()Lcom/a/c/d/d;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " at line "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 15316
    iget v2, p0, Lcom/a/c/d/a;->H:I

    add-int/lit8 v2, v2, 0x1

    .line 1199
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " column "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lcom/a/c/d/a;->w()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " path "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/a/c/d/a;->p()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1209
    :cond_141
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/a/c/d/a;->M:Ljava/lang/String;

    .line 1210
    iput v6, p0, Lcom/a/c/d/a;->J:I

    .line 1211
    iget-object v1, p0, Lcom/a/c/d/a;->Q:[I

    iget v2, p0, Lcom/a/c/d/a;->O:I

    add-int/lit8 v2, v2, -0x1

    aget v3, v1, v2

    add-int/lit8 v3, v3, 0x1

    aput v3, v1, v2

    goto/16 :goto_68
.end method

.method public o()V
    .registers 8

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 1231
    move v0, v1

    .line 1233
    :cond_4
    iget v2, p0, Lcom/a/c/d/a;->J:I

    .line 1234
    if-nez v2, :cond_c

    .line 1235
    invoke-direct {p0}, Lcom/a/c/d/a;->q()I

    move-result v2

    .line 1238
    :cond_c
    if-ne v2, v6, :cond_2e

    .line 1239
    invoke-direct {p0, v5}, Lcom/a/c/d/a;->a(I)V

    .line 1240
    add-int/lit8 v0, v0, 0x1

    .line 1259
    :cond_13
    :goto_13
    iput v1, p0, Lcom/a/c/d/a;->J:I

    .line 1260
    if-nez v0, :cond_4

    .line 1262
    iget-object v0, p0, Lcom/a/c/d/a;->Q:[I

    iget v1, p0, Lcom/a/c/d/a;->O:I

    add-int/lit8 v1, v1, -0x1

    aget v2, v0, v1

    add-int/lit8 v2, v2, 0x1

    aput v2, v0, v1

    .line 1263
    iget-object v0, p0, Lcom/a/c/d/a;->P:[Ljava/lang/String;

    iget v1, p0, Lcom/a/c/d/a;->O:I

    add-int/lit8 v1, v1, -0x1

    const-string v2, "null"

    aput-object v2, v0, v1

    .line 1264
    return-void

    .line 1241
    :cond_2e
    if-ne v2, v5, :cond_36

    .line 1242
    invoke-direct {p0, v6}, Lcom/a/c/d/a;->a(I)V

    .line 1243
    add-int/lit8 v0, v0, 0x1

    goto :goto_13

    .line 1244
    :cond_36
    const/4 v3, 0x4

    if-ne v2, v3, :cond_42

    .line 1245
    iget v2, p0, Lcom/a/c/d/a;->O:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/a/c/d/a;->O:I

    .line 1246
    add-int/lit8 v0, v0, -0x1

    goto :goto_13

    .line 1247
    :cond_42
    const/4 v3, 0x2

    if-ne v2, v3, :cond_4e

    .line 1248
    iget v2, p0, Lcom/a/c/d/a;->O:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/a/c/d/a;->O:I

    .line 1249
    add-int/lit8 v0, v0, -0x1

    goto :goto_13

    .line 1250
    :cond_4e
    const/16 v3, 0xe

    if-eq v2, v3, :cond_56

    const/16 v3, 0xa

    if-ne v2, v3, :cond_80

    :cond_56
    move v2, v1

    .line 17129
    :goto_57
    iget v3, p0, Lcom/a/c/d/a;->F:I

    add-int/2addr v3, v2

    iget v4, p0, Lcom/a/c/d/a;->G:I

    if-ge v3, v4, :cond_74

    .line 17130
    iget-object v3, p0, Lcom/a/c/d/a;->E:[C

    iget v4, p0, Lcom/a/c/d/a;->F:I

    add-int/2addr v4, v2

    aget-char v3, v3, v4

    sparse-switch v3, :sswitch_data_aa

    .line 17129
    add-int/lit8 v2, v2, 0x1

    goto :goto_57

    .line 17136
    :sswitch_6b
    invoke-direct {p0}, Lcom/a/c/d/a;->x()V

    .line 17148
    :sswitch_6e
    iget v3, p0, Lcom/a/c/d/a;->F:I

    add-int/2addr v2, v3

    iput v2, p0, Lcom/a/c/d/a;->F:I

    goto :goto_13

    .line 17152
    :cond_74
    iget v3, p0, Lcom/a/c/d/a;->F:I

    add-int/2addr v2, v3

    iput v2, p0, Lcom/a/c/d/a;->F:I

    .line 17153
    invoke-direct {p0, v5}, Lcom/a/c/d/a;->b(I)Z

    move-result v2

    if-nez v2, :cond_56

    goto :goto_13

    .line 1252
    :cond_80
    const/16 v3, 0x8

    if-eq v2, v3, :cond_88

    const/16 v3, 0xc

    if-ne v2, v3, :cond_8e

    .line 1253
    :cond_88
    const/16 v2, 0x27

    invoke-direct {p0, v2}, Lcom/a/c/d/a;->c(C)V

    goto :goto_13

    .line 1254
    :cond_8e
    const/16 v3, 0x9

    if-eq v2, v3, :cond_96

    const/16 v3, 0xd

    if-ne v2, v3, :cond_9d

    .line 1255
    :cond_96
    const/16 v2, 0x22

    invoke-direct {p0, v2}, Lcom/a/c/d/a;->c(C)V

    goto/16 :goto_13

    .line 1256
    :cond_9d
    const/16 v3, 0x10

    if-ne v2, v3, :cond_13

    .line 1257
    iget v2, p0, Lcom/a/c/d/a;->F:I

    iget v3, p0, Lcom/a/c/d/a;->L:I

    add-int/2addr v2, v3

    iput v2, p0, Lcom/a/c/d/a;->F:I

    goto/16 :goto_13

    .line 17130
    :sswitch_data_aa
    .sparse-switch
        0x9 -> :sswitch_6e
        0xa -> :sswitch_6e
        0xc -> :sswitch_6e
        0xd -> :sswitch_6e
        0x20 -> :sswitch_6e
        0x23 -> :sswitch_6b
        0x2c -> :sswitch_6e
        0x2f -> :sswitch_6b
        0x3a -> :sswitch_6e
        0x3b -> :sswitch_6b
        0x3d -> :sswitch_6b
        0x5b -> :sswitch_6e
        0x5c -> :sswitch_6b
        0x5d -> :sswitch_6e
        0x7b -> :sswitch_6e
        0x7d -> :sswitch_6e
    .end sparse-switch
.end method

.method public final p()Ljava/lang/String;
    .registers 6

    .prologue
    .line 1474
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v0, "$"

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1475
    const/4 v0, 0x0

    iget v2, p0, Lcom/a/c/d/a;->O:I

    :goto_a
    if-ge v0, v2, :cond_3d

    .line 1476
    iget-object v3, p0, Lcom/a/c/d/a;->N:[I

    aget v3, v3, v0

    packed-switch v3, :pswitch_data_42

    .line 1475
    :cond_13
    :goto_13
    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    .line 1479
    :pswitch_16
    const/16 v3, 0x5b

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/a/c/d/a;->Q:[I

    aget v4, v4, v0

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0x5d

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_13

    .line 1485
    :pswitch_2a
    const/16 v3, 0x2e

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1486
    iget-object v3, p0, Lcom/a/c/d/a;->P:[Ljava/lang/String;

    aget-object v3, v3, v0

    if-eqz v3, :cond_13

    .line 1487
    iget-object v3, p0, Lcom/a/c/d/a;->P:[Ljava/lang/String;

    aget-object v3, v3, v0

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_13

    .line 1497
    :cond_3d
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1476
    :pswitch_data_42
    .packed-switch 0x1
        :pswitch_16
        :pswitch_16
        :pswitch_2a
        :pswitch_2a
        :pswitch_2a
    .end packed-switch
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    .prologue
    .line 1465
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " at line "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 19316
    iget v1, p0, Lcom/a/c/d/a;->H:I

    add-int/lit8 v1, v1, 0x1

    .line 1466
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " column "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0}, Lcom/a/c/d/a;->w()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
