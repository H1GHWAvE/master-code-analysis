.class public abstract Lcom/a/b/n/a/bx;
.super Lcom/a/b/d/hh;
.source "SourceFile"

# interfaces
.implements Ljava/util/concurrent/BlockingQueue;


# direct methods
.method protected constructor <init>()V
    .registers 1

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/a/b/d/hh;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic a()Ljava/util/Queue;
    .registers 2

    .prologue
    .line 36
    invoke-virtual {p0}, Lcom/a/b/n/a/bx;->c()Ljava/util/concurrent/BlockingQueue;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic b()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 36
    invoke-virtual {p0}, Lcom/a/b/n/a/bx;->c()Ljava/util/concurrent/BlockingQueue;

    move-result-object v0

    return-object v0
.end method

.method protected abstract c()Ljava/util/concurrent/BlockingQueue;
.end method

.method public drainTo(Ljava/util/Collection;)I
    .registers 3

    .prologue
    .line 50
    invoke-virtual {p0}, Lcom/a/b/n/a/bx;->c()Ljava/util/concurrent/BlockingQueue;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/concurrent/BlockingQueue;->drainTo(Ljava/util/Collection;)I

    move-result v0

    return v0
.end method

.method public drainTo(Ljava/util/Collection;I)I
    .registers 4

    .prologue
    .line 46
    invoke-virtual {p0}, Lcom/a/b/n/a/bx;->c()Ljava/util/concurrent/BlockingQueue;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Ljava/util/concurrent/BlockingQueue;->drainTo(Ljava/util/Collection;I)I

    move-result v0

    return v0
.end method

.method protected final synthetic k_()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 36
    invoke-virtual {p0}, Lcom/a/b/n/a/bx;->c()Ljava/util/concurrent/BlockingQueue;

    move-result-object v0

    return-object v0
.end method

.method public offer(Ljava/lang/Object;JLjava/util/concurrent/TimeUnit;)Z
    .registers 7

    .prologue
    .line 55
    invoke-virtual {p0}, Lcom/a/b/n/a/bx;->c()Ljava/util/concurrent/BlockingQueue;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3, p4}, Ljava/util/concurrent/BlockingQueue;->offer(Ljava/lang/Object;JLjava/util/concurrent/TimeUnit;)Z

    move-result v0

    return v0
.end method

.method public poll(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
    .registers 5

    .prologue
    .line 60
    invoke-virtual {p0}, Lcom/a/b/n/a/bx;->c()Ljava/util/concurrent/BlockingQueue;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3}, Ljava/util/concurrent/BlockingQueue;->poll(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public put(Ljava/lang/Object;)V
    .registers 3

    .prologue
    .line 64
    invoke-virtual {p0}, Lcom/a/b/n/a/bx;->c()Ljava/util/concurrent/BlockingQueue;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/concurrent/BlockingQueue;->put(Ljava/lang/Object;)V

    .line 65
    return-void
.end method

.method public remainingCapacity()I
    .registers 2

    .prologue
    .line 68
    invoke-virtual {p0}, Lcom/a/b/n/a/bx;->c()Ljava/util/concurrent/BlockingQueue;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/BlockingQueue;->remainingCapacity()I

    move-result v0

    return v0
.end method

.method public take()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 72
    invoke-virtual {p0}, Lcom/a/b/n/a/bx;->c()Ljava/util/concurrent/BlockingQueue;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/BlockingQueue;->take()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
