.class final Lcom/a/b/n/a/h;
.super Ljava/util/concurrent/locks/AbstractQueuedSynchronizer;
.source "SourceFile"


# static fields
.field static final a:I = 0x0

.field static final b:I = 0x1

.field static final c:I = 0x2

.field static final d:I = 0x4

.field static final e:I = 0x8

.field private static final f:J


# instance fields
.field private g:Ljava/lang/Object;

.field private h:Ljava/lang/Throwable;


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    .line 224
    invoke-direct {p0}, Ljava/util/concurrent/locks/AbstractQueuedSynchronizer;-><init>()V

    return-void
.end method

.method private a(J)Ljava/lang/Object;
    .registers 6

    .prologue
    .line 268
    const/4 v0, -0x1

    invoke-virtual {p0, v0, p1, p2}, Lcom/a/b/n/a/h;->tryAcquireSharedNanos(IJ)Z

    move-result v0

    if-nez v0, :cond_f

    .line 269
    new-instance v0, Ljava/util/concurrent/TimeoutException;

    const-string v1, "Timeout waiting for task."

    invoke-direct {v0, v1}, Ljava/util/concurrent/TimeoutException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 272
    :cond_f
    invoke-virtual {p0}, Lcom/a/b/n/a/h;->a()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/Object;)Z
    .registers 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 341
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, p1, v0, v1}, Lcom/a/b/n/a/h;->a(Ljava/lang/Object;Ljava/lang/Throwable;I)Z

    move-result v0

    return v0
.end method

.method private a(Ljava/lang/Throwable;)Z
    .registers 4

    .prologue
    .line 348
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, p1, v1}, Lcom/a/b/n/a/h;->a(Ljava/lang/Object;Ljava/lang/Throwable;I)Z

    move-result v0

    return v0
.end method

.method private a(Z)Z
    .registers 4

    .prologue
    const/4 v1, 0x0

    .line 355
    if-eqz p1, :cond_a

    const/16 v0, 0x8

    :goto_5
    invoke-virtual {p0, v1, v1, v0}, Lcom/a/b/n/a/h;->a(Ljava/lang/Object;Ljava/lang/Throwable;I)Z

    move-result v0

    return v0

    :cond_a
    const/4 v0, 0x4

    goto :goto_5
.end method

.method private e()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 285
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/a/b/n/a/h;->acquireSharedInterruptibly(I)V

    .line 286
    invoke-virtual {p0}, Lcom/a/b/n/a/h;->a()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method final a()Ljava/lang/Object;
    .registers 5

    .prologue
    .line 295
    invoke-virtual {p0}, Lcom/a/b/n/a/h;->getState()I

    move-result v0

    .line 296
    sparse-switch v0, :sswitch_data_3a

    .line 310
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x31

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Error, synchronizer in invalid state: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 298
    :sswitch_22
    iget-object v0, p0, Lcom/a/b/n/a/h;->h:Ljava/lang/Throwable;

    if-eqz v0, :cond_2e

    .line 299
    new-instance v0, Ljava/util/concurrent/ExecutionException;

    iget-object v1, p0, Lcom/a/b/n/a/h;->h:Ljava/lang/Throwable;

    invoke-direct {v0, v1}, Ljava/util/concurrent/ExecutionException;-><init>(Ljava/lang/Throwable;)V

    throw v0

    .line 301
    :cond_2e
    iget-object v0, p0, Lcom/a/b/n/a/h;->g:Ljava/lang/Object;

    return-object v0

    .line 306
    :sswitch_31
    const-string v0, "Task was cancelled."

    iget-object v1, p0, Lcom/a/b/n/a/h;->h:Ljava/lang/Throwable;

    invoke-static {v0, v1}, Lcom/a/b/n/a/g;->a(Ljava/lang/String;Ljava/lang/Throwable;)Ljava/util/concurrent/CancellationException;

    move-result-object v0

    throw v0

    .line 296
    :sswitch_data_3a
    .sparse-switch
        0x2 -> :sswitch_22
        0x4 -> :sswitch_31
        0x8 -> :sswitch_31
    .end sparse-switch
.end method

.method final a(Ljava/lang/Object;Ljava/lang/Throwable;I)Z
    .registers 7
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Throwable;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x1

    .line 372
    const/4 v0, 0x0

    invoke-virtual {p0, v0, v2}, Lcom/a/b/n/a/h;->compareAndSetState(II)Z

    move-result v0

    .line 373
    if-eqz v0, :cond_1b

    .line 376
    iput-object p1, p0, Lcom/a/b/n/a/h;->g:Ljava/lang/Object;

    .line 378
    and-int/lit8 v1, p3, 0xc

    if-eqz v1, :cond_15

    new-instance p2, Ljava/util/concurrent/CancellationException;

    const-string v1, "Future.cancel() was called."

    invoke-direct {p2, v1}, Ljava/util/concurrent/CancellationException;-><init>(Ljava/lang/String;)V

    :cond_15
    iput-object p2, p0, Lcom/a/b/n/a/h;->h:Ljava/lang/Throwable;

    .line 380
    invoke-virtual {p0, p3}, Lcom/a/b/n/a/h;->releaseShared(I)Z

    .line 386
    :cond_1a
    :goto_1a
    return v0

    .line 381
    :cond_1b
    invoke-virtual {p0}, Lcom/a/b/n/a/h;->getState()I

    move-result v1

    if-ne v1, v2, :cond_1a

    .line 384
    const/4 v1, -0x1

    invoke-virtual {p0, v1}, Lcom/a/b/n/a/h;->acquireShared(I)V

    goto :goto_1a
.end method

.method final b()Z
    .registers 2

    .prologue
    .line 320
    invoke-virtual {p0}, Lcom/a/b/n/a/h;->getState()I

    move-result v0

    and-int/lit8 v0, v0, 0xe

    if-eqz v0, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method final c()Z
    .registers 2

    .prologue
    .line 327
    invoke-virtual {p0}, Lcom/a/b/n/a/h;->getState()I

    move-result v0

    and-int/lit8 v0, v0, 0xc

    if-eqz v0, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method final d()Z
    .registers 3

    .prologue
    .line 334
    invoke-virtual {p0}, Lcom/a/b/n/a/h;->getState()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method protected final tryAcquireShared(I)I
    .registers 3

    .prologue
    .line 243
    invoke-virtual {p0}, Lcom/a/b/n/a/h;->b()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 244
    const/4 v0, 0x1

    .line 246
    :goto_7
    return v0

    :cond_8
    const/4 v0, -0x1

    goto :goto_7
.end method

.method protected final tryReleaseShared(I)Z
    .registers 3

    .prologue
    .line 255
    invoke-virtual {p0, p1}, Lcom/a/b/n/a/h;->setState(I)V

    .line 256
    const/4 v0, 0x1

    return v0
.end method
