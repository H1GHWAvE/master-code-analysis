.class Lcom/a/b/n/a/bk;
.super Ljava/lang/IllegalStateException;
.source "SourceFile"


# static fields
.field static final a:[Ljava/lang/StackTraceElement;

.field static b:Ljava/util/Set;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    .line 519
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/StackTraceElement;

    sput-object v0, Lcom/a/b/n/a/bk;->a:[Ljava/lang/StackTraceElement;

    .line 522
    const-class v0, Lcom/a/b/n/a/bd;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-class v1, Lcom/a/b/n/a/bk;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const-class v2, Lcom/a/b/n/a/bl;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/a/b/d/lo;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/lo;

    move-result-object v0

    sput-object v0, Lcom/a/b/n/a/bk;->b:Ljava/util/Set;

    return-void
.end method

.method constructor <init>(Lcom/a/b/n/a/bl;Lcom/a/b/n/a/bl;)V
    .registers 8

    .prologue
    .line 528
    .line 1642
    iget-object v0, p1, Lcom/a/b/n/a/bl;->c:Ljava/lang/String;

    .line 528
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2642
    iget-object v1, p2, Lcom/a/b/n/a/bl;->c:Ljava/lang/String;

    .line 528
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x4

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " -> "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 529
    invoke-virtual {p0}, Lcom/a/b/n/a/bk;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v1

    .line 530
    const/4 v0, 0x0

    array-length v2, v1

    :goto_3f
    if-ge v0, v2, :cond_58

    .line 531
    const-class v3, Lcom/a/b/n/a/bs;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    aget-object v4, v1, v0

    invoke-virtual {v4}, Ljava/lang/StackTraceElement;->getClassName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_59

    .line 534
    sget-object v0, Lcom/a/b/n/a/bk;->a:[Ljava/lang/StackTraceElement;

    invoke-virtual {p0, v0}, Lcom/a/b/n/a/bk;->setStackTrace([Ljava/lang/StackTraceElement;)V

    .line 542
    :cond_58
    :goto_58
    return-void

    .line 537
    :cond_59
    sget-object v3, Lcom/a/b/n/a/bk;->b:Ljava/util/Set;

    aget-object v4, v1, v0

    invoke-virtual {v4}, Ljava/lang/StackTraceElement;->getClassName()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_71

    .line 538
    invoke-static {v1, v0, v2}, Ljava/util/Arrays;->copyOfRange([Ljava/lang/Object;II)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/StackTraceElement;

    invoke-virtual {p0, v0}, Lcom/a/b/n/a/bk;->setStackTrace([Ljava/lang/StackTraceElement;)V

    goto :goto_58

    .line 530
    :cond_71
    add-int/lit8 v0, v0, 0x1

    goto :goto_3f
.end method
