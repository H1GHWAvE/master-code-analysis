.class final Lcom/a/b/n/a/r;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/a/b/n/a/q;


# direct methods
.method constructor <init>(Lcom/a/b/n/a/q;)V
    .registers 2

    .prologue
    .line 170
    iput-object p1, p0, Lcom/a/b/n/a/r;->a:Lcom/a/b/n/a/q;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .registers 6

    .prologue
    .line 172
    iget-object v0, p0, Lcom/a/b/n/a/r;->a:Lcom/a/b/n/a/q;

    invoke-static {v0}, Lcom/a/b/n/a/q;->a(Lcom/a/b/n/a/q;)Ljava/util/concurrent/locks/ReentrantLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 174
    :try_start_9
    iget-object v0, p0, Lcom/a/b/n/a/r;->a:Lcom/a/b/n/a/q;

    iget-object v0, v0, Lcom/a/b/n/a/q;->a:Lcom/a/b/n/a/p;

    invoke-virtual {v0}, Lcom/a/b/n/a/p;->a()V
    :try_end_10
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_10} :catch_1a
    .catchall {:try_start_9 .. :try_end_10} :catchall_2c

    .line 185
    iget-object v0, p0, Lcom/a/b/n/a/r;->a:Lcom/a/b/n/a/q;

    invoke-static {v0}, Lcom/a/b/n/a/q;->a(Lcom/a/b/n/a/q;)Ljava/util/concurrent/locks/ReentrantLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 186
    return-void

    .line 175
    :catch_1a
    move-exception v0

    .line 177
    :try_start_1b
    iget-object v1, p0, Lcom/a/b/n/a/r;->a:Lcom/a/b/n/a/q;

    iget-object v1, v1, Lcom/a/b/n/a/q;->a:Lcom/a/b/n/a/p;

    invoke-static {}, Lcom/a/b/n/a/p;->c()V
    :try_end_22
    .catch Ljava/lang/Exception; {:try_start_1b .. :try_end_22} :catch_37
    .catchall {:try_start_1b .. :try_end_22} :catchall_2c

    .line 182
    :goto_22
    :try_start_22
    iget-object v1, p0, Lcom/a/b/n/a/r;->a:Lcom/a/b/n/a/q;

    invoke-virtual {v1, v0}, Lcom/a/b/n/a/q;->a(Ljava/lang/Throwable;)V

    .line 183
    invoke-static {v0}, Lcom/a/b/b/ei;->a(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
    :try_end_2c
    .catchall {:try_start_22 .. :try_end_2c} :catchall_2c

    .line 185
    :catchall_2c
    move-exception v0

    iget-object v1, p0, Lcom/a/b/n/a/r;->a:Lcom/a/b/n/a/q;

    invoke-static {v1}, Lcom/a/b/n/a/q;->a(Lcom/a/b/n/a/q;)Ljava/util/concurrent/locks/ReentrantLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0

    .line 178
    :catch_37
    move-exception v1

    .line 179
    :try_start_38
    invoke-static {}, Lcom/a/b/n/a/p;->m()Ljava/util/logging/Logger;

    move-result-object v2

    sget-object v3, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string v4, "Error while attempting to shut down the service after failure."

    invoke-virtual {v2, v3, v4, v1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_43
    .catchall {:try_start_38 .. :try_end_43} :catchall_2c

    goto :goto_22
.end method
