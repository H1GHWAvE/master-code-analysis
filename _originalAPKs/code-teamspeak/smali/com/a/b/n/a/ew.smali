.class public abstract enum Lcom/a/b/n/a/ew;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/a;
.end annotation


# static fields
.field public static final enum a:Lcom/a/b/n/a/ew;

.field public static final enum b:Lcom/a/b/n/a/ew;

.field public static final enum c:Lcom/a/b/n/a/ew;

.field public static final enum d:Lcom/a/b/n/a/ew;

.field public static final enum e:Lcom/a/b/n/a/ew;

.field public static final enum f:Lcom/a/b/n/a/ew;

.field private static final synthetic g:[Lcom/a/b/n/a/ew;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    .line 189
    new-instance v0, Lcom/a/b/n/a/ex;

    const-string v1, "NEW"

    invoke-direct {v0, v1}, Lcom/a/b/n/a/ex;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/a/b/n/a/ew;->a:Lcom/a/b/n/a/ew;

    .line 198
    new-instance v0, Lcom/a/b/n/a/ey;

    const-string v1, "STARTING"

    invoke-direct {v0, v1}, Lcom/a/b/n/a/ey;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/a/b/n/a/ew;->b:Lcom/a/b/n/a/ew;

    .line 207
    new-instance v0, Lcom/a/b/n/a/ez;

    const-string v1, "RUNNING"

    invoke-direct {v0, v1}, Lcom/a/b/n/a/ez;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/a/b/n/a/ew;->c:Lcom/a/b/n/a/ew;

    .line 216
    new-instance v0, Lcom/a/b/n/a/fa;

    const-string v1, "STOPPING"

    invoke-direct {v0, v1}, Lcom/a/b/n/a/fa;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/a/b/n/a/ew;->d:Lcom/a/b/n/a/ew;

    .line 226
    new-instance v0, Lcom/a/b/n/a/fb;

    const-string v1, "TERMINATED"

    invoke-direct {v0, v1}, Lcom/a/b/n/a/fb;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/a/b/n/a/ew;->e:Lcom/a/b/n/a/ew;

    .line 236
    new-instance v0, Lcom/a/b/n/a/fc;

    const-string v1, "FAILED"

    invoke-direct {v0, v1}, Lcom/a/b/n/a/fc;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/a/b/n/a/ew;->f:Lcom/a/b/n/a/ew;

    .line 183
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/a/b/n/a/ew;

    const/4 v1, 0x0

    sget-object v2, Lcom/a/b/n/a/ew;->a:Lcom/a/b/n/a/ew;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/a/b/n/a/ew;->b:Lcom/a/b/n/a/ew;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/a/b/n/a/ew;->c:Lcom/a/b/n/a/ew;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Lcom/a/b/n/a/ew;->d:Lcom/a/b/n/a/ew;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, Lcom/a/b/n/a/ew;->e:Lcom/a/b/n/a/ew;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, Lcom/a/b/n/a/ew;->f:Lcom/a/b/n/a/ew;

    aput-object v2, v0, v1

    sput-object v0, Lcom/a/b/n/a/ew;->g:[Lcom/a/b/n/a/ew;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3

    .prologue
    .line 184
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;IB)V
    .registers 4

    .prologue
    .line 184
    invoke-direct {p0, p1, p2}, Lcom/a/b/n/a/ew;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/a/b/n/a/ew;
    .registers 2

    .prologue
    .line 183
    const-class v0, Lcom/a/b/n/a/ew;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/a/b/n/a/ew;

    return-object v0
.end method

.method public static values()[Lcom/a/b/n/a/ew;
    .registers 1

    .prologue
    .line 183
    sget-object v0, Lcom/a/b/n/a/ew;->g:[Lcom/a/b/n/a/ew;

    invoke-virtual {v0}, [Lcom/a/b/n/a/ew;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/a/b/n/a/ew;

    return-object v0
.end method


# virtual methods
.method abstract a()Z
.end method
