.class final Lcom/a/b/n/a/bg;
.super Ljava/util/concurrent/locks/ReentrantLock;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/n/a/bf;


# instance fields
.field final synthetic a:Lcom/a/b/n/a/bd;

.field private final b:Lcom/a/b/n/a/bl;


# direct methods
.method private constructor <init>(Lcom/a/b/n/a/bd;Lcom/a/b/n/a/bl;)V
    .registers 4

    .prologue
    .line 796
    iput-object p1, p0, Lcom/a/b/n/a/bg;->a:Lcom/a/b/n/a/bd;

    .line 797
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>(Z)V

    .line 798
    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/n/a/bl;

    iput-object v0, p0, Lcom/a/b/n/a/bg;->b:Lcom/a/b/n/a/bl;

    .line 799
    return-void
.end method

.method synthetic constructor <init>(Lcom/a/b/n/a/bd;Lcom/a/b/n/a/bl;B)V
    .registers 4

    .prologue
    .line 790
    invoke-direct {p0, p1, p2}, Lcom/a/b/n/a/bg;-><init>(Lcom/a/b/n/a/bd;Lcom/a/b/n/a/bl;)V

    return-void
.end method


# virtual methods
.method public final a()Lcom/a/b/n/a/bl;
    .registers 2

    .prologue
    .line 805
    iget-object v0, p0, Lcom/a/b/n/a/bg;->b:Lcom/a/b/n/a/bl;

    return-object v0
.end method

.method public final b()Z
    .registers 2

    .prologue
    .line 810
    invoke-virtual {p0}, Lcom/a/b/n/a/bg;->isHeldByCurrentThread()Z

    move-result v0

    return v0
.end method

.method public final lock()V
    .registers 2

    .prologue
    .line 817
    iget-object v0, p0, Lcom/a/b/n/a/bg;->a:Lcom/a/b/n/a/bd;

    invoke-static {v0, p0}, Lcom/a/b/n/a/bd;->a(Lcom/a/b/n/a/bd;Lcom/a/b/n/a/bf;)V

    .line 819
    :try_start_5
    invoke-super {p0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V
    :try_end_8
    .catchall {:try_start_5 .. :try_end_8} :catchall_c

    .line 821
    invoke-static {p0}, Lcom/a/b/n/a/bd;->a(Lcom/a/b/n/a/bf;)V

    .line 822
    return-void

    .line 821
    :catchall_c
    move-exception v0

    invoke-static {p0}, Lcom/a/b/n/a/bd;->a(Lcom/a/b/n/a/bf;)V

    throw v0
.end method

.method public final lockInterruptibly()V
    .registers 2

    .prologue
    .line 827
    iget-object v0, p0, Lcom/a/b/n/a/bg;->a:Lcom/a/b/n/a/bd;

    invoke-static {v0, p0}, Lcom/a/b/n/a/bd;->a(Lcom/a/b/n/a/bd;Lcom/a/b/n/a/bf;)V

    .line 829
    :try_start_5
    invoke-super {p0}, Ljava/util/concurrent/locks/ReentrantLock;->lockInterruptibly()V
    :try_end_8
    .catchall {:try_start_5 .. :try_end_8} :catchall_c

    .line 831
    invoke-static {p0}, Lcom/a/b/n/a/bd;->a(Lcom/a/b/n/a/bf;)V

    .line 832
    return-void

    .line 831
    :catchall_c
    move-exception v0

    invoke-static {p0}, Lcom/a/b/n/a/bd;->a(Lcom/a/b/n/a/bf;)V

    throw v0
.end method

.method public final tryLock()Z
    .registers 2

    .prologue
    .line 837
    iget-object v0, p0, Lcom/a/b/n/a/bg;->a:Lcom/a/b/n/a/bd;

    invoke-static {v0, p0}, Lcom/a/b/n/a/bd;->a(Lcom/a/b/n/a/bd;Lcom/a/b/n/a/bf;)V

    .line 839
    :try_start_5
    invoke-super {p0}, Ljava/util/concurrent/locks/ReentrantLock;->tryLock()Z
    :try_end_8
    .catchall {:try_start_5 .. :try_end_8} :catchall_d

    move-result v0

    .line 841
    invoke-static {p0}, Lcom/a/b/n/a/bd;->a(Lcom/a/b/n/a/bf;)V

    return v0

    :catchall_d
    move-exception v0

    invoke-static {p0}, Lcom/a/b/n/a/bd;->a(Lcom/a/b/n/a/bf;)V

    throw v0
.end method

.method public final tryLock(JLjava/util/concurrent/TimeUnit;)Z
    .registers 5

    .prologue
    .line 848
    iget-object v0, p0, Lcom/a/b/n/a/bg;->a:Lcom/a/b/n/a/bd;

    invoke-static {v0, p0}, Lcom/a/b/n/a/bd;->a(Lcom/a/b/n/a/bd;Lcom/a/b/n/a/bf;)V

    .line 850
    :try_start_5
    invoke-super {p0, p1, p2, p3}, Ljava/util/concurrent/locks/ReentrantLock;->tryLock(JLjava/util/concurrent/TimeUnit;)Z
    :try_end_8
    .catchall {:try_start_5 .. :try_end_8} :catchall_d

    move-result v0

    .line 852
    invoke-static {p0}, Lcom/a/b/n/a/bd;->a(Lcom/a/b/n/a/bf;)V

    return v0

    :catchall_d
    move-exception v0

    invoke-static {p0}, Lcom/a/b/n/a/bd;->a(Lcom/a/b/n/a/bf;)V

    throw v0
.end method

.method public final unlock()V
    .registers 2

    .prologue
    .line 859
    :try_start_0
    invoke-super {p0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V
    :try_end_3
    .catchall {:try_start_0 .. :try_end_3} :catchall_7

    .line 861
    invoke-static {p0}, Lcom/a/b/n/a/bd;->a(Lcom/a/b/n/a/bf;)V

    .line 862
    return-void

    .line 861
    :catchall_7
    move-exception v0

    invoke-static {p0}, Lcom/a/b/n/a/bd;->a(Lcom/a/b/n/a/bf;)V

    throw v0
.end method
