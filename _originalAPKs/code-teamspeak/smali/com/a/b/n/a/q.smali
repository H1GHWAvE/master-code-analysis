.class final Lcom/a/b/n/a/q;
.super Lcom/a/b/n/a/ad;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/a/b/n/a/p;

.field private volatile b:Ljava/util/concurrent/Future;

.field private volatile c:Ljava/util/concurrent/ScheduledExecutorService;

.field private final d:Ljava/util/concurrent/locks/ReentrantLock;

.field private final e:Ljava/lang/Runnable;


# direct methods
.method constructor <init>(Lcom/a/b/n/a/p;)V
    .registers 3

    .prologue
    .line 159
    iput-object p1, p0, Lcom/a/b/n/a/q;->a:Lcom/a/b/n/a/p;

    invoke-direct {p0}, Lcom/a/b/n/a/ad;-><init>()V

    .line 168
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, Lcom/a/b/n/a/q;->d:Ljava/util/concurrent/locks/ReentrantLock;

    .line 170
    new-instance v0, Lcom/a/b/n/a/r;

    invoke-direct {v0, p0}, Lcom/a/b/n/a/r;-><init>(Lcom/a/b/n/a/q;)V

    iput-object v0, p0, Lcom/a/b/n/a/q;->e:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic a(Lcom/a/b/n/a/q;Ljava/util/concurrent/Future;)Ljava/util/concurrent/Future;
    .registers 2

    .prologue
    .line 159
    iput-object p1, p0, Lcom/a/b/n/a/q;->b:Ljava/util/concurrent/Future;

    return-object p1
.end method

.method static synthetic a(Lcom/a/b/n/a/q;)Ljava/util/concurrent/locks/ReentrantLock;
    .registers 2

    .prologue
    .line 159
    iget-object v0, p0, Lcom/a/b/n/a/q;->d:Ljava/util/concurrent/locks/ReentrantLock;

    return-object v0
.end method

.method static synthetic b(Lcom/a/b/n/a/q;)Ljava/util/concurrent/ScheduledExecutorService;
    .registers 2

    .prologue
    .line 159
    iget-object v0, p0, Lcom/a/b/n/a/q;->c:Ljava/util/concurrent/ScheduledExecutorService;

    return-object v0
.end method

.method static synthetic c(Lcom/a/b/n/a/q;)Ljava/lang/Runnable;
    .registers 2

    .prologue
    .line 159
    iget-object v0, p0, Lcom/a/b/n/a/q;->e:Ljava/lang/Runnable;

    return-object v0
.end method


# virtual methods
.method protected final a()V
    .registers 4

    .prologue
    .line 191
    iget-object v0, p0, Lcom/a/b/n/a/q;->a:Lcom/a/b/n/a/p;

    invoke-virtual {v0}, Lcom/a/b/n/a/p;->l()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    new-instance v2, Lcom/a/b/n/a/s;

    invoke-direct {v2, p0}, Lcom/a/b/n/a/s;-><init>(Lcom/a/b/n/a/q;)V

    .line 1898
    invoke-static {v0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1899
    invoke-static {v2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1900
    invoke-static {}, Lcom/a/b/n/a/dy;->a()Z

    move-result v1

    if-eqz v1, :cond_24

    .line 191
    :goto_17
    iput-object v0, p0, Lcom/a/b/n/a/q;->c:Ljava/util/concurrent/ScheduledExecutorService;

    .line 196
    iget-object v0, p0, Lcom/a/b/n/a/q;->c:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lcom/a/b/n/a/t;

    invoke-direct {v1, p0}, Lcom/a/b/n/a/t;-><init>(Lcom/a/b/n/a/q;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 211
    return-void

    .line 1904
    :cond_24
    new-instance v1, Lcom/a/b/n/a/ec;

    invoke-direct {v1, v0, v2}, Lcom/a/b/n/a/ec;-><init>(Ljava/util/concurrent/ScheduledExecutorService;Lcom/a/b/b/dz;)V

    move-object v0, v1

    goto :goto_17
.end method

.method protected final b()V
    .registers 3

    .prologue
    .line 214
    iget-object v0, p0, Lcom/a/b/n/a/q;->b:Ljava/util/concurrent/Future;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 215
    iget-object v0, p0, Lcom/a/b/n/a/q;->c:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lcom/a/b/n/a/u;

    invoke-direct {v1, p0}, Lcom/a/b/n/a/u;-><init>(Lcom/a/b/n/a/q;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 238
    return-void
.end method
