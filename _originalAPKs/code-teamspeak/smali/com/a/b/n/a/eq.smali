.class final Lcom/a/b/n/a/eq;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/concurrent/Executor;


# static fields
.field private static final a:Ljava/util/logging/Logger;


# instance fields
.field private final b:Ljava/util/concurrent/Executor;

.field private final c:Ljava/util/Queue;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "internalLock"
    .end annotation
.end field

.field private d:Z
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "internalLock"
    .end annotation
.end field

.field private final e:Lcom/a/b/n/a/es;

.field private final f:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 47
    const-class v0, Lcom/a/b/n/a/eq;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/a/b/n/a/eq;->a:Ljava/util/logging/Logger;

    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/Executor;)V
    .registers 4

    .prologue
    const/4 v1, 0x0

    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    new-instance v0, Ljava/util/ArrayDeque;

    invoke-direct {v0}, Ljava/util/ArrayDeque;-><init>()V

    iput-object v0, p0, Lcom/a/b/n/a/eq;->c:Ljava/util/Queue;

    .line 65
    iput-boolean v1, p0, Lcom/a/b/n/a/eq;->d:Z

    .line 69
    new-instance v0, Lcom/a/b/n/a/es;

    invoke-direct {v0, p0, v1}, Lcom/a/b/n/a/es;-><init>(Lcom/a/b/n/a/eq;B)V

    iput-object v0, p0, Lcom/a/b/n/a/eq;->e:Lcom/a/b/n/a/es;

    .line 81
    new-instance v0, Lcom/a/b/n/a/er;

    invoke-direct {v0, p0}, Lcom/a/b/n/a/er;-><init>(Lcom/a/b/n/a/eq;)V

    iput-object v0, p0, Lcom/a/b/n/a/eq;->f:Ljava/lang/Object;

    .line 77
    const-string v0, "\'executor\' must not be null."

    invoke-static {p1, v0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 78
    iput-object p1, p0, Lcom/a/b/n/a/eq;->b:Ljava/util/concurrent/Executor;

    .line 79
    return-void
.end method

.method static synthetic a()Ljava/util/logging/Logger;
    .registers 1

    .prologue
    .line 46
    sget-object v0, Lcom/a/b/n/a/eq;->a:Ljava/util/logging/Logger;

    return-object v0
.end method

.method static synthetic a(Lcom/a/b/n/a/eq;)Z
    .registers 2

    .prologue
    .line 46
    iget-boolean v0, p0, Lcom/a/b/n/a/eq;->d:Z

    return v0
.end method

.method static synthetic b(Lcom/a/b/n/a/eq;)Ljava/lang/Object;
    .registers 2

    .prologue
    .line 46
    iget-object v0, p0, Lcom/a/b/n/a/eq;->f:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic c(Lcom/a/b/n/a/eq;)Ljava/util/Queue;
    .registers 2

    .prologue
    .line 46
    iget-object v0, p0, Lcom/a/b/n/a/eq;->c:Ljava/util/Queue;

    return-object v0
.end method

.method static synthetic d(Lcom/a/b/n/a/eq;)Z
    .registers 2

    .prologue
    .line 46
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/a/b/n/a/eq;->d:Z

    return v0
.end method


# virtual methods
.method public final execute(Ljava/lang/Runnable;)V
    .registers 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 93
    const-string v2, "\'r\' must not be null."

    invoke-static {p1, v2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 95
    iget-object v2, p0, Lcom/a/b/n/a/eq;->f:Ljava/lang/Object;

    monitor-enter v2

    .line 96
    :try_start_a
    iget-object v3, p0, Lcom/a/b/n/a/eq;->c:Ljava/util/Queue;

    invoke-interface {v3, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 98
    iget-boolean v3, p0, Lcom/a/b/n/a/eq;->d:Z

    if-nez v3, :cond_30

    .line 99
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/a/b/n/a/eq;->d:Z

    .line 102
    :goto_16
    monitor-exit v2
    :try_end_17
    .catchall {:try_start_a .. :try_end_17} :catchall_21

    .line 103
    if-eqz v0, :cond_20

    .line 106
    :try_start_19
    iget-object v0, p0, Lcom/a/b/n/a/eq;->b:Ljava/util/concurrent/Executor;

    iget-object v1, p0, Lcom/a/b/n/a/eq;->e:Lcom/a/b/n/a/es;

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V
    :try_end_20
    .catchall {:try_start_19 .. :try_end_20} :catchall_24

    .line 120
    :cond_20
    return-void

    .line 102
    :catchall_21
    move-exception v0

    :try_start_22
    monitor-exit v2
    :try_end_23
    .catchall {:try_start_22 .. :try_end_23} :catchall_21

    throw v0

    .line 109
    :catchall_24
    move-exception v0

    .line 110
    iget-object v1, p0, Lcom/a/b/n/a/eq;->f:Ljava/lang/Object;

    monitor-enter v1

    .line 115
    const/4 v2, 0x0

    :try_start_29
    iput-boolean v2, p0, Lcom/a/b/n/a/eq;->d:Z

    .line 116
    monitor-exit v1
    :try_end_2c
    .catchall {:try_start_29 .. :try_end_2c} :catchall_2d

    throw v0

    :catchall_2d
    move-exception v0

    :try_start_2e
    monitor-exit v1
    :try_end_2f
    .catchall {:try_start_2e .. :try_end_2f} :catchall_2d

    throw v0

    :cond_30
    move v0, v1

    goto :goto_16
.end method
