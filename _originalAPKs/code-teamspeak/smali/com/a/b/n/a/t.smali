.class final Lcom/a/b/n/a/t;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/a/b/n/a/q;


# direct methods
.method constructor <init>(Lcom/a/b/n/a/q;)V
    .registers 2

    .prologue
    .line 196
    iput-object p1, p0, Lcom/a/b/n/a/t;->a:Lcom/a/b/n/a/q;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .registers 6

    .prologue
    .line 198
    iget-object v0, p0, Lcom/a/b/n/a/t;->a:Lcom/a/b/n/a/q;

    invoke-static {v0}, Lcom/a/b/n/a/q;->a(Lcom/a/b/n/a/q;)Ljava/util/concurrent/locks/ReentrantLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 200
    :try_start_9
    iget-object v0, p0, Lcom/a/b/n/a/t;->a:Lcom/a/b/n/a/q;

    iget-object v0, v0, Lcom/a/b/n/a/q;->a:Lcom/a/b/n/a/p;

    invoke-static {}, Lcom/a/b/n/a/p;->b()V

    .line 201
    iget-object v0, p0, Lcom/a/b/n/a/t;->a:Lcom/a/b/n/a/q;

    iget-object v1, p0, Lcom/a/b/n/a/t;->a:Lcom/a/b/n/a/q;

    iget-object v1, v1, Lcom/a/b/n/a/q;->a:Lcom/a/b/n/a/p;

    invoke-virtual {v1}, Lcom/a/b/n/a/p;->d()Lcom/a/b/n/a/aa;

    move-result-object v1

    iget-object v2, p0, Lcom/a/b/n/a/t;->a:Lcom/a/b/n/a/q;

    iget-object v2, v2, Lcom/a/b/n/a/q;->a:Lcom/a/b/n/a/p;

    invoke-static {v2}, Lcom/a/b/n/a/p;->a(Lcom/a/b/n/a/p;)Lcom/a/b/n/a/ad;

    move-result-object v2

    iget-object v3, p0, Lcom/a/b/n/a/t;->a:Lcom/a/b/n/a/q;

    invoke-static {v3}, Lcom/a/b/n/a/q;->b(Lcom/a/b/n/a/q;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v3

    iget-object v4, p0, Lcom/a/b/n/a/t;->a:Lcom/a/b/n/a/q;

    invoke-static {v4}, Lcom/a/b/n/a/q;->c(Lcom/a/b/n/a/q;)Ljava/lang/Runnable;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, Lcom/a/b/n/a/aa;->a(Lcom/a/b/n/a/ad;Ljava/util/concurrent/ScheduledExecutorService;Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/a/b/n/a/q;->a(Lcom/a/b/n/a/q;Ljava/util/concurrent/Future;)Ljava/util/concurrent/Future;

    .line 202
    iget-object v0, p0, Lcom/a/b/n/a/t;->a:Lcom/a/b/n/a/q;

    invoke-virtual {v0}, Lcom/a/b/n/a/q;->c()V
    :try_end_3a
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_3a} :catch_44
    .catchall {:try_start_9 .. :try_end_3a} :catchall_4f

    .line 207
    iget-object v0, p0, Lcom/a/b/n/a/t;->a:Lcom/a/b/n/a/q;

    invoke-static {v0}, Lcom/a/b/n/a/q;->a(Lcom/a/b/n/a/q;)Ljava/util/concurrent/locks/ReentrantLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 208
    return-void

    .line 203
    :catch_44
    move-exception v0

    .line 204
    :try_start_45
    iget-object v1, p0, Lcom/a/b/n/a/t;->a:Lcom/a/b/n/a/q;

    invoke-virtual {v1, v0}, Lcom/a/b/n/a/q;->a(Ljava/lang/Throwable;)V

    .line 205
    invoke-static {v0}, Lcom/a/b/b/ei;->a(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
    :try_end_4f
    .catchall {:try_start_45 .. :try_end_4f} :catchall_4f

    .line 207
    :catchall_4f
    move-exception v0

    iget-object v1, p0, Lcom/a/b/n/a/t;->a:Lcom/a/b/n/a/q;

    invoke-static {v1}, Lcom/a/b/n/a/q;->a(Lcom/a/b/n/a/q;)Ljava/util/concurrent/locks/ReentrantLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method
