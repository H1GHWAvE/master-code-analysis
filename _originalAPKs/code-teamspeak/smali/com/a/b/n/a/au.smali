.class public final Lcom/a/b/n/a/au;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final a:J


# instance fields
.field private transient b:Ljava/util/concurrent/atomic/AtomicLongArray;


# direct methods
.method private constructor <init>(I)V
    .registers 3

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLongArray;

    invoke-direct {v0, p1}, Ljava/util/concurrent/atomic/AtomicLongArray;-><init>(I)V

    iput-object v0, p0, Lcom/a/b/n/a/au;->b:Ljava/util/concurrent/atomic/AtomicLongArray;

    .line 57
    return-void
.end method

.method private constructor <init>([D)V
    .registers 8

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    array-length v1, p1

    .line 68
    new-array v2, v1, [J

    .line 69
    const/4 v0, 0x0

    :goto_7
    if-ge v0, v1, :cond_14

    .line 70
    aget-wide v4, p1, v0

    invoke-static {v4, v5}, Ljava/lang/Double;->doubleToRawLongBits(D)J

    move-result-wide v4

    aput-wide v4, v2, v0

    .line 69
    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    .line 72
    :cond_14
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLongArray;

    invoke-direct {v0, v2}, Ljava/util/concurrent/atomic/AtomicLongArray;-><init>([J)V

    iput-object v0, p0, Lcom/a/b/n/a/au;->b:Ljava/util/concurrent/atomic/AtomicLongArray;

    .line 73
    return-void
.end method

.method private a(I)D
    .registers 4

    .prologue
    .line 91
    iget-object v0, p0, Lcom/a/b/n/a/au;->b:Ljava/util/concurrent/atomic/AtomicLongArray;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicLongArray;->get(I)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v0

    return-wide v0
.end method

.method private a()I
    .registers 2

    .prologue
    .line 81
    iget-object v0, p0, Lcom/a/b/n/a/au;->b:Ljava/util/concurrent/atomic/AtomicLongArray;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLongArray;->length()I

    move-result v0

    return v0
.end method

.method private a(ID)V
    .registers 8

    .prologue
    .line 101
    invoke-static {p2, p3}, Ljava/lang/Double;->doubleToRawLongBits(D)J

    move-result-wide v0

    .line 102
    iget-object v2, p0, Lcom/a/b/n/a/au;->b:Ljava/util/concurrent/atomic/AtomicLongArray;

    invoke-virtual {v2, p1, v0, v1}, Ljava/util/concurrent/atomic/AtomicLongArray;->set(IJ)V

    .line 103
    return-void
.end method

.method private a(Ljava/io/ObjectInputStream;)V
    .registers 6

    .prologue
    .line 257
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->defaultReadObject()V

    .line 260
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readInt()I

    move-result v1

    .line 261
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLongArray;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicLongArray;-><init>(I)V

    iput-object v0, p0, Lcom/a/b/n/a/au;->b:Ljava/util/concurrent/atomic/AtomicLongArray;

    .line 264
    const/4 v0, 0x0

    :goto_f
    if-ge v0, v1, :cond_1b

    .line 265
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readDouble()D

    move-result-wide v2

    invoke-direct {p0, v0, v2, v3}, Lcom/a/b/n/a/au;->a(ID)V

    .line 264
    add-int/lit8 v0, v0, 0x1

    goto :goto_f

    .line 267
    :cond_1b
    return-void
.end method

.method private a(Ljava/io/ObjectOutputStream;)V
    .registers 6

    .prologue
    .line 240
    invoke-virtual {p1}, Ljava/io/ObjectOutputStream;->defaultWriteObject()V

    .line 2081
    iget-object v0, p0, Lcom/a/b/n/a/au;->b:Ljava/util/concurrent/atomic/AtomicLongArray;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLongArray;->length()I

    move-result v1

    .line 244
    invoke-virtual {p1, v1}, Ljava/io/ObjectOutputStream;->writeInt(I)V

    .line 247
    const/4 v0, 0x0

    :goto_d
    if-ge v0, v1, :cond_1f

    .line 2091
    iget-object v2, p0, Lcom/a/b/n/a/au;->b:Ljava/util/concurrent/atomic/AtomicLongArray;

    invoke-virtual {v2, v0}, Ljava/util/concurrent/atomic/AtomicLongArray;->get(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v2

    .line 248
    invoke-virtual {p1, v2, v3}, Ljava/io/ObjectOutputStream;->writeDouble(D)V

    .line 247
    add-int/lit8 v0, v0, 0x1

    goto :goto_d

    .line 250
    :cond_1f
    return-void
.end method

.method private a(IDD)Z
    .registers 12

    .prologue
    .line 144
    iget-object v0, p0, Lcom/a/b/n/a/au;->b:Ljava/util/concurrent/atomic/AtomicLongArray;

    invoke-static {p2, p3}, Ljava/lang/Double;->doubleToRawLongBits(D)J

    move-result-wide v2

    invoke-static {p4, p5}, Ljava/lang/Double;->doubleToRawLongBits(D)J

    move-result-wide v4

    move v1, p1

    invoke-virtual/range {v0 .. v5}, Ljava/util/concurrent/atomic/AtomicLongArray;->compareAndSet(IJJ)Z

    move-result v0

    return v0
.end method

.method private b(ID)V
    .registers 4

    .prologue
    .line 112
    invoke-direct {p0, p1, p2, p3}, Lcom/a/b/n/a/au;->a(ID)V

    .line 116
    return-void
.end method

.method private b(IDD)Z
    .registers 12

    .prologue
    .line 167
    iget-object v0, p0, Lcom/a/b/n/a/au;->b:Ljava/util/concurrent/atomic/AtomicLongArray;

    invoke-static {p2, p3}, Ljava/lang/Double;->doubleToRawLongBits(D)J

    move-result-wide v2

    invoke-static {p4, p5}, Ljava/lang/Double;->doubleToRawLongBits(D)J

    move-result-wide v4

    move v1, p1

    invoke-virtual/range {v0 .. v5}, Ljava/util/concurrent/atomic/AtomicLongArray;->weakCompareAndSet(IJJ)Z

    move-result v0

    return v0
.end method

.method private c(ID)D
    .registers 8

    .prologue
    .line 127
    invoke-static {p2, p3}, Ljava/lang/Double;->doubleToRawLongBits(D)J

    move-result-wide v0

    .line 128
    iget-object v2, p0, Lcom/a/b/n/a/au;->b:Ljava/util/concurrent/atomic/AtomicLongArray;

    invoke-virtual {v2, p1, v0, v1}, Ljava/util/concurrent/atomic/AtomicLongArray;->getAndSet(IJ)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v0

    return-wide v0
.end method

.method private d(ID)D
    .registers 12

    .prologue
    .line 181
    :cond_0
    iget-object v0, p0, Lcom/a/b/n/a/au;->b:Ljava/util/concurrent/atomic/AtomicLongArray;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicLongArray;->get(I)J

    move-result-wide v2

    .line 182
    invoke-static {v2, v3}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v6

    .line 183
    add-double v0, v6, p2

    .line 184
    invoke-static {v0, v1}, Ljava/lang/Double;->doubleToRawLongBits(D)J

    move-result-wide v4

    .line 185
    iget-object v0, p0, Lcom/a/b/n/a/au;->b:Ljava/util/concurrent/atomic/AtomicLongArray;

    move v1, p1

    invoke-virtual/range {v0 .. v5}, Ljava/util/concurrent/atomic/AtomicLongArray;->compareAndSet(IJJ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 186
    return-wide v6
.end method

.method private e(ID)D
    .registers 12

    .prologue
    .line 200
    :cond_0
    iget-object v0, p0, Lcom/a/b/n/a/au;->b:Ljava/util/concurrent/atomic/AtomicLongArray;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicLongArray;->get(I)J

    move-result-wide v2

    .line 201
    invoke-static {v2, v3}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v0

    .line 202
    add-double v6, v0, p2

    .line 203
    invoke-static {v6, v7}, Ljava/lang/Double;->doubleToRawLongBits(D)J

    move-result-wide v4

    .line 204
    iget-object v0, p0, Lcom/a/b/n/a/au;->b:Ljava/util/concurrent/atomic/AtomicLongArray;

    move v1, p1

    invoke-virtual/range {v0 .. v5}, Ljava/util/concurrent/atomic/AtomicLongArray;->compareAndSet(IJJ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 205
    return-wide v6
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .registers 7

    .prologue
    .line 215
    .line 1081
    iget-object v0, p0, Lcom/a/b/n/a/au;->b:Ljava/util/concurrent/atomic/AtomicLongArray;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLongArray;->length()I

    move-result v0

    .line 215
    add-int/lit8 v1, v0, -0x1

    .line 216
    const/4 v0, -0x1

    if-ne v1, v0, :cond_e

    .line 217
    const-string v0, "[]"

    .line 226
    :goto_d
    return-object v0

    .line 221
    :cond_e
    new-instance v2, Ljava/lang/StringBuilder;

    add-int/lit8 v0, v1, 0x1

    mul-int/lit8 v0, v0, 0x13

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 222
    const/16 v0, 0x5b

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 223
    const/4 v0, 0x0

    .line 224
    :goto_1d
    iget-object v3, p0, Lcom/a/b/n/a/au;->b:Ljava/util/concurrent/atomic/AtomicLongArray;

    invoke-virtual {v3, v0}, Ljava/util/concurrent/atomic/AtomicLongArray;->get(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 225
    if-ne v0, v1, :cond_37

    .line 226
    const/16 v0, 0x5d

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_d

    .line 228
    :cond_37
    const/16 v3, 0x2c

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0x20

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 223
    add-int/lit8 v0, v0, 0x1

    goto :goto_1d
.end method
