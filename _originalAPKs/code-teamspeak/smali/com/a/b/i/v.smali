.class Lcom/a/b/i/v;
.super Lcom/a/b/i/s;
.source "SourceFile"


# instance fields
.field protected final a:[B


# direct methods
.method protected constructor <init>([B)V
    .registers 3

    .prologue
    .line 494
    invoke-direct {p0}, Lcom/a/b/i/s;-><init>()V

    .line 495
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    iput-object v0, p0, Lcom/a/b/i/v;->a:[B

    .line 496
    return-void
.end method


# virtual methods
.method public final a(Ljava/io/OutputStream;)J
    .registers 4

    .prologue
    .line 525
    iget-object v0, p0, Lcom/a/b/i/v;->a:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 526
    iget-object v0, p0, Lcom/a/b/i/v;->a:[B

    array-length v0, v0

    int-to-long v0, v0

    return-wide v0
.end method

.method public final a(Lcom/a/b/g/ak;)Lcom/a/b/g/ag;
    .registers 3

    .prologue
    .line 537
    iget-object v0, p0, Lcom/a/b/i/v;->a:[B

    invoke-interface {p1, v0}, Lcom/a/b/g/ak;->a([B)Lcom/a/b/g/ag;

    move-result-object v0

    return-object v0
.end method

.method public final a()Ljava/io/InputStream;
    .registers 3

    .prologue
    .line 500
    new-instance v0, Ljava/io/ByteArrayInputStream;

    iget-object v1, p0, Lcom/a/b/i/v;->a:[B

    invoke-direct {v0, v1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    return-object v0
.end method

.method public final a(Lcom/a/b/i/o;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 532
    invoke-interface {p1}, Lcom/a/b/i/o;->b()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/io/InputStream;
    .registers 2

    .prologue
    .line 505
    invoke-virtual {p0}, Lcom/a/b/i/v;->a()Ljava/io/InputStream;

    move-result-object v0

    return-object v0
.end method

.method public final c()Z
    .registers 2

    .prologue
    .line 510
    iget-object v0, p0, Lcom/a/b/i/v;->a:[B

    array-length v0, v0

    if-nez v0, :cond_7

    const/4 v0, 0x1

    :goto_6
    return v0

    :cond_7
    const/4 v0, 0x0

    goto :goto_6
.end method

.method public final d()J
    .registers 3

    .prologue
    .line 515
    iget-object v0, p0, Lcom/a/b/i/v;->a:[B

    array-length v0, v0

    int-to-long v0, v0

    return-wide v0
.end method

.method public e()[B
    .registers 2

    .prologue
    .line 520
    iget-object v0, p0, Lcom/a/b/i/v;->a:[B

    invoke-virtual {v0}, [B->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .registers 4

    .prologue
    .line 544
    invoke-static {}, Lcom/a/b/i/b;->e()Lcom/a/b/i/b;

    move-result-object v1

    iget-object v2, p0, Lcom/a/b/i/v;->a:[B

    .line 1158
    invoke-static {v2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    array-length v2, v2

    invoke-virtual {v1, v0, v2}, Lcom/a/b/i/b;->a([BI)Ljava/lang/String;

    move-result-object v0

    .line 544
    const-string v1, "..."

    invoke-static {v0, v1}, Lcom/a/b/b/e;->a(Ljava/lang/CharSequence;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x11

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "ByteSource.wrap("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
