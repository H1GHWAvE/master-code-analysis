.class public final Lcom/a/b/i/ar;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Closeable;


# annotations
.annotation build Lcom/a/b/a/a;
.end annotation


# static fields
.field private static final b:Lcom/a/b/i/au;


# instance fields
.field final a:Lcom/a/b/i/au;
    .annotation build Lcom/a/b/a/d;
    .end annotation
.end field

.field private final c:Ljava/util/Deque;

.field private d:Ljava/lang/Throwable;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 96
    invoke-static {}, Lcom/a/b/i/at;->a()Z

    move-result v0

    if-eqz v0, :cond_b

    sget-object v0, Lcom/a/b/i/at;->a:Lcom/a/b/i/at;

    :goto_8
    sput-object v0, Lcom/a/b/i/ar;->b:Lcom/a/b/i/au;

    return-void

    :cond_b
    sget-object v0, Lcom/a/b/i/as;->a:Lcom/a/b/i/as;

    goto :goto_8
.end method

.method private constructor <init>(Lcom/a/b/i/au;)V
    .registers 4
    .annotation build Lcom/a/b/a/d;
    .end annotation

    .prologue
    .line 113
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 110
    new-instance v0, Ljava/util/ArrayDeque;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Ljava/util/ArrayDeque;-><init>(I)V

    iput-object v0, p0, Lcom/a/b/i/ar;->c:Ljava/util/Deque;

    .line 114
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/i/au;

    iput-object v0, p0, Lcom/a/b/i/ar;->a:Lcom/a/b/i/au;

    .line 115
    return-void
.end method

.method public static a()Lcom/a/b/i/ar;
    .registers 2

    .prologue
    .line 104
    new-instance v0, Lcom/a/b/i/ar;

    sget-object v1, Lcom/a/b/i/ar;->b:Lcom/a/b/i/au;

    invoke-direct {v0, v1}, Lcom/a/b/i/ar;-><init>(Lcom/a/b/i/au;)V

    return-object v0
.end method

.method private a(Ljava/lang/Throwable;Ljava/lang/Class;)Ljava/lang/RuntimeException;
    .registers 4

    .prologue
    .line 168
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 169
    iput-object p1, p0, Lcom/a/b/i/ar;->d:Ljava/lang/Throwable;

    .line 170
    const-class v0, Ljava/io/IOException;

    invoke-static {p1, v0}, Lcom/a/b/b/ei;->b(Ljava/lang/Throwable;Ljava/lang/Class;)V

    .line 171
    invoke-static {p1, p2}, Lcom/a/b/b/ei;->b(Ljava/lang/Throwable;Ljava/lang/Class;)V

    .line 172
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method

.method private a(Ljava/lang/Throwable;Ljava/lang/Class;Ljava/lang/Class;)Ljava/lang/RuntimeException;
    .registers 5

    .prologue
    .line 192
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 193
    iput-object p1, p0, Lcom/a/b/i/ar;->d:Ljava/lang/Throwable;

    .line 194
    const-class v0, Ljava/io/IOException;

    invoke-static {p1, v0}, Lcom/a/b/b/ei;->b(Ljava/lang/Throwable;Ljava/lang/Class;)V

    .line 1129
    invoke-static {p3}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1130
    invoke-static {p1, p2}, Lcom/a/b/b/ei;->a(Ljava/lang/Throwable;Ljava/lang/Class;)V

    .line 1131
    invoke-static {p1, p3}, Lcom/a/b/b/ei;->b(Ljava/lang/Throwable;Ljava/lang/Class;)V

    .line 196
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method


# virtual methods
.method public final a(Ljava/io/Closeable;)Ljava/io/Closeable;
    .registers 3
    .param p1    # Ljava/io/Closeable;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 125
    if-eqz p1, :cond_7

    .line 126
    iget-object v0, p0, Lcom/a/b/i/ar;->c:Ljava/util/Deque;

    invoke-interface {v0, p1}, Ljava/util/Deque;->addFirst(Ljava/lang/Object;)V

    .line 129
    :cond_7
    return-object p1
.end method

.method public final a(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;
    .registers 3

    .prologue
    .line 146
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 147
    iput-object p1, p0, Lcom/a/b/i/ar;->d:Ljava/lang/Throwable;

    .line 148
    const-class v0, Ljava/io/IOException;

    invoke-static {p1, v0}, Lcom/a/b/b/ei;->b(Ljava/lang/Throwable;Ljava/lang/Class;)V

    .line 149
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method

.method public final close()V
    .registers 5

    .prologue
    .line 208
    iget-object v0, p0, Lcom/a/b/i/ar;->d:Ljava/lang/Throwable;

    move-object v1, v0

    .line 211
    :goto_3
    iget-object v0, p0, Lcom/a/b/i/ar;->c:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_22

    .line 212
    iget-object v0, p0, Lcom/a/b/i/ar;->c:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->removeFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/Closeable;

    .line 214
    :try_start_13
    invoke-interface {v0}, Ljava/io/Closeable;->close()V
    :try_end_16
    .catch Ljava/lang/Throwable; {:try_start_13 .. :try_end_16} :catch_17

    goto :goto_3

    .line 215
    :catch_17
    move-exception v2

    .line 216
    if-nez v1, :cond_1c

    move-object v1, v2

    .line 217
    goto :goto_3

    .line 219
    :cond_1c
    iget-object v3, p0, Lcom/a/b/i/ar;->a:Lcom/a/b/i/au;

    invoke-interface {v3, v0, v1, v2}, Lcom/a/b/i/au;->a(Ljava/io/Closeable;Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_3

    .line 224
    :cond_22
    iget-object v0, p0, Lcom/a/b/i/ar;->d:Ljava/lang/Throwable;

    if-nez v0, :cond_33

    if-eqz v1, :cond_33

    .line 225
    const-class v0, Ljava/io/IOException;

    invoke-static {v1, v0}, Lcom/a/b/b/ei;->b(Ljava/lang/Throwable;Ljava/lang/Class;)V

    .line 226
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 228
    :cond_33
    return-void
.end method
