.class final Lcom/a/b/i/l;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/i/bs;


# instance fields
.field a:I

.field b:I

.field c:I

.field d:Z

.field final e:Lcom/a/b/b/m;

.field final synthetic f:Lcom/a/b/i/bu;

.field final synthetic g:Lcom/a/b/i/j;


# direct methods
.method constructor <init>(Lcom/a/b/i/j;Lcom/a/b/i/bu;)V
    .registers 4

    .prologue
    const/4 v0, 0x0

    .line 645
    iput-object p1, p0, Lcom/a/b/i/l;->g:Lcom/a/b/i/j;

    iput-object p2, p0, Lcom/a/b/i/l;->f:Lcom/a/b/i/bu;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 646
    iput v0, p0, Lcom/a/b/i/l;->a:I

    .line 647
    iput v0, p0, Lcom/a/b/i/l;->b:I

    .line 648
    iput v0, p0, Lcom/a/b/i/l;->c:I

    .line 649
    iput-boolean v0, p0, Lcom/a/b/i/l;->d:Z

    .line 650
    iget-object v0, p0, Lcom/a/b/i/l;->g:Lcom/a/b/i/j;

    invoke-virtual {v0}, Lcom/a/b/i/j;->a()Lcom/a/b/b/m;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/i/l;->e:Lcom/a/b/b/m;

    return-void
.end method


# virtual methods
.method public final a()I
    .registers 7

    .prologue
    const/4 v5, 0x1

    const/4 v0, -0x1

    .line 655
    :cond_2
    :goto_2
    iget-object v1, p0, Lcom/a/b/i/l;->f:Lcom/a/b/i/bu;

    invoke-interface {v1}, Lcom/a/b/i/bu;->a()I

    move-result v1

    .line 656
    if-ne v1, v0, :cond_39

    .line 657
    iget-boolean v1, p0, Lcom/a/b/i/l;->d:Z

    if-nez v1, :cond_10c

    iget-object v1, p0, Lcom/a/b/i/l;->g:Lcom/a/b/i/j;

    invoke-static {v1}, Lcom/a/b/i/j;->a(Lcom/a/b/i/j;)Lcom/a/b/i/g;

    move-result-object v1

    iget v2, p0, Lcom/a/b/i/l;->c:I

    invoke-virtual {v1, v2}, Lcom/a/b/i/g;->a(I)Z

    move-result v1

    if-nez v1, :cond_10c

    .line 658
    new-instance v0, Lcom/a/b/i/h;

    iget v1, p0, Lcom/a/b/i/l;->c:I

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x20

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Invalid input length "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/a/b/i/h;-><init>(Ljava/lang/String;)V

    throw v0

    .line 662
    :cond_39
    iget v2, p0, Lcom/a/b/i/l;->c:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/a/b/i/l;->c:I

    .line 663
    int-to-char v1, v1

    .line 664
    iget-object v2, p0, Lcom/a/b/i/l;->e:Lcom/a/b/b/m;

    invoke-virtual {v2, v1}, Lcom/a/b/b/m;->c(C)Z

    move-result v2

    if-eqz v2, :cond_80

    .line 665
    iget-boolean v1, p0, Lcom/a/b/i/l;->d:Z

    if-nez v1, :cond_7d

    iget v1, p0, Lcom/a/b/i/l;->c:I

    if-eq v1, v5, :cond_60

    iget-object v1, p0, Lcom/a/b/i/l;->g:Lcom/a/b/i/j;

    invoke-static {v1}, Lcom/a/b/i/j;->a(Lcom/a/b/i/j;)Lcom/a/b/i/g;

    move-result-object v1

    iget v2, p0, Lcom/a/b/i/l;->c:I

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Lcom/a/b/i/g;->a(I)Z

    move-result v1

    if-nez v1, :cond_7d

    .line 667
    :cond_60
    new-instance v0, Lcom/a/b/i/h;

    iget v1, p0, Lcom/a/b/i/l;->c:I

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x29

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Padding cannot start at index "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/a/b/i/h;-><init>(Ljava/lang/String;)V

    throw v0

    .line 669
    :cond_7d
    iput-boolean v5, p0, Lcom/a/b/i/l;->d:Z

    goto :goto_2

    .line 670
    :cond_80
    iget-boolean v2, p0, Lcom/a/b/i/l;->d:Z

    if-eqz v2, :cond_ab

    .line 671
    new-instance v0, Lcom/a/b/i/h;

    iget v2, p0, Lcom/a/b/i/l;->c:I

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x3d

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Expected padding character but found \'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "\' at index "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/a/b/i/h;-><init>(Ljava/lang/String;)V

    throw v0

    .line 674
    :cond_ab
    iget v2, p0, Lcom/a/b/i/l;->a:I

    iget-object v3, p0, Lcom/a/b/i/l;->g:Lcom/a/b/i/j;

    invoke-static {v3}, Lcom/a/b/i/j;->a(Lcom/a/b/i/j;)Lcom/a/b/i/g;

    move-result-object v3

    iget v3, v3, Lcom/a/b/i/g;->v:I

    shl-int/2addr v2, v3

    iput v2, p0, Lcom/a/b/i/l;->a:I

    .line 675
    iget v2, p0, Lcom/a/b/i/l;->a:I

    iget-object v3, p0, Lcom/a/b/i/l;->g:Lcom/a/b/i/j;

    invoke-static {v3}, Lcom/a/b/i/j;->a(Lcom/a/b/i/j;)Lcom/a/b/i/g;

    move-result-object v3

    .line 1502
    const/16 v4, 0x7f

    if-gt v1, v4, :cond_ca

    iget-object v4, v3, Lcom/a/b/i/g;->y:[B

    aget-byte v4, v4, v1

    if-ne v4, v0, :cond_e5

    .line 1503
    :cond_ca
    new-instance v0, Lcom/a/b/i/h;

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x19

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Unrecognized character: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/a/b/i/h;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1505
    :cond_e5
    iget-object v3, v3, Lcom/a/b/i/g;->y:[B

    aget-byte v1, v3, v1

    .line 675
    or-int/2addr v1, v2

    iput v1, p0, Lcom/a/b/i/l;->a:I

    .line 676
    iget v1, p0, Lcom/a/b/i/l;->b:I

    iget-object v2, p0, Lcom/a/b/i/l;->g:Lcom/a/b/i/j;

    invoke-static {v2}, Lcom/a/b/i/j;->a(Lcom/a/b/i/j;)Lcom/a/b/i/g;

    move-result-object v2

    iget v2, v2, Lcom/a/b/i/g;->v:I

    add-int/2addr v1, v2

    iput v1, p0, Lcom/a/b/i/l;->b:I

    .line 678
    iget v1, p0, Lcom/a/b/i/l;->b:I

    const/16 v2, 0x8

    if-lt v1, v2, :cond_2

    .line 679
    iget v0, p0, Lcom/a/b/i/l;->b:I

    add-int/lit8 v0, v0, -0x8

    iput v0, p0, Lcom/a/b/i/l;->b:I

    .line 680
    iget v0, p0, Lcom/a/b/i/l;->a:I

    iget v1, p0, Lcom/a/b/i/l;->b:I

    shr-int/2addr v0, v1

    and-int/lit16 v0, v0, 0xff

    :cond_10c
    return v0
.end method

.method public final b()V
    .registers 2

    .prologue
    .line 688
    iget-object v0, p0, Lcom/a/b/i/l;->f:Lcom/a/b/i/bu;

    invoke-interface {v0}, Lcom/a/b/i/bu;->b()V

    .line 689
    return-void
.end method
