.class abstract enum Lcom/a/b/i/bh;
.super Ljava/lang/Enum;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/b/co;


# static fields
.field public static final enum a:Lcom/a/b/i/bh;

.field public static final enum b:Lcom/a/b/i/bh;

.field private static final synthetic c:[Lcom/a/b/i/bh;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    .line 846
    new-instance v0, Lcom/a/b/i/bi;

    const-string v1, "IS_DIRECTORY"

    invoke-direct {v0, v1}, Lcom/a/b/i/bi;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/a/b/i/bh;->a:Lcom/a/b/i/bh;

    .line 858
    new-instance v0, Lcom/a/b/i/bj;

    const-string v1, "IS_FILE"

    invoke-direct {v0, v1}, Lcom/a/b/i/bj;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/a/b/i/bh;->b:Lcom/a/b/i/bh;

    .line 845
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/a/b/i/bh;

    const/4 v1, 0x0

    sget-object v2, Lcom/a/b/i/bh;->a:Lcom/a/b/i/bh;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/a/b/i/bh;->b:Lcom/a/b/i/bh;

    aput-object v2, v0, v1

    sput-object v0, Lcom/a/b/i/bh;->c:[Lcom/a/b/i/bh;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3

    .prologue
    .line 845
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;IB)V
    .registers 4

    .prologue
    .line 845
    invoke-direct {p0, p1, p2}, Lcom/a/b/i/bh;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/a/b/i/bh;
    .registers 2

    .prologue
    .line 845
    const-class v0, Lcom/a/b/i/bh;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/a/b/i/bh;

    return-object v0
.end method

.method public static values()[Lcom/a/b/i/bh;
    .registers 1

    .prologue
    .line 845
    sget-object v0, Lcom/a/b/i/bh;->c:[Lcom/a/b/i/bh;

    invoke-virtual {v0}, [Lcom/a/b/i/bh;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/a/b/i/bh;

    return-object v0
.end method
