.class final Lcom/a/b/i/bl;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/b;
    b = true
.end annotation


# direct methods
.method private constructor <init>()V
    .registers 1

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Ljava/io/Reader;)Lcom/a/b/i/bu;
    .registers 2
    .annotation build Lcom/a/b/a/c;
        a = "Reader"
    .end annotation

    .prologue
    .line 53
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    new-instance v0, Lcom/a/b/i/bm;

    invoke-direct {v0, p0}, Lcom/a/b/i/bm;-><init>(Ljava/io/Reader;)V

    return-object v0
.end method

.method private static a(Ljava/lang/CharSequence;)Lcom/a/b/i/bu;
    .registers 2

    .prologue
    .line 71
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 72
    new-instance v0, Lcom/a/b/i/bn;

    invoke-direct {v0, p0}, Lcom/a/b/i/bn;-><init>(Ljava/lang/CharSequence;)V

    return-object v0
.end method

.method private static a(I)Lcom/a/b/i/bv;
    .registers 3

    .prologue
    .line 211
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0, p0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 212
    new-instance v1, Lcom/a/b/i/br;

    invoke-direct {v1, v0}, Lcom/a/b/i/br;-><init>(Ljava/lang/StringBuilder;)V

    return-object v1
.end method

.method private static a(Ljava/io/Writer;)Lcom/a/b/i/bv;
    .registers 2
    .annotation build Lcom/a/b/a/c;
        a = "Writer"
    .end annotation

    .prologue
    .line 187
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 188
    new-instance v0, Lcom/a/b/i/bq;

    invoke-direct {v0, p0}, Lcom/a/b/i/bq;-><init>(Ljava/io/Writer;)V

    return-object v0
.end method

.method private static a(Lcom/a/b/i/bs;)Ljava/io/InputStream;
    .registers 2
    .annotation build Lcom/a/b/a/c;
        a = "InputStream"
    .end annotation

    .prologue
    .line 104
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 105
    new-instance v0, Lcom/a/b/i/bo;

    invoke-direct {v0, p0}, Lcom/a/b/i/bo;-><init>(Lcom/a/b/i/bs;)V

    return-object v0
.end method

.method private static a(Lcom/a/b/i/bt;)Ljava/io/OutputStream;
    .registers 2
    .annotation build Lcom/a/b/a/c;
        a = "OutputStream"
    .end annotation

    .prologue
    .line 154
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 155
    new-instance v0, Lcom/a/b/i/bp;

    invoke-direct {v0, p0}, Lcom/a/b/i/bp;-><init>(Lcom/a/b/i/bt;)V

    return-object v0
.end method
