.class public final Lcom/a/b/b/di;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/b;
    b = true
.end annotation


# instance fields
.field final a:Lcom/a/b/b/m;

.field final b:Z

.field final c:I

.field private final d:Lcom/a/b/b/du;


# direct methods
.method private constructor <init>(Lcom/a/b/b/du;)V
    .registers 5

    .prologue
    .line 110
    const/4 v0, 0x0

    sget-object v1, Lcom/a/b/b/m;->m:Lcom/a/b/b/m;

    const v2, 0x7fffffff

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/a/b/b/di;-><init>(Lcom/a/b/b/du;ZLcom/a/b/b/m;I)V

    .line 111
    return-void
.end method

.method private constructor <init>(Lcom/a/b/b/du;ZLcom/a/b/b/m;I)V
    .registers 5

    .prologue
    .line 114
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 115
    iput-object p1, p0, Lcom/a/b/b/di;->d:Lcom/a/b/b/du;

    .line 116
    iput-boolean p2, p0, Lcom/a/b/b/di;->b:Z

    .line 117
    iput-object p3, p0, Lcom/a/b/b/di;->a:Lcom/a/b/b/m;

    .line 118
    iput p4, p0, Lcom/a/b/b/di;->c:I

    .line 119
    return-void
.end method

.method public static a(C)Lcom/a/b/b/di;
    .registers 4

    .prologue
    .line 130
    invoke-static {p0}, Lcom/a/b/b/m;->a(C)Lcom/a/b/b/m;

    move-result-object v0

    .line 1144
    invoke-static {v0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1146
    new-instance v1, Lcom/a/b/b/di;

    new-instance v2, Lcom/a/b/b/dj;

    invoke-direct {v2, v0}, Lcom/a/b/b/dj;-><init>(Lcom/a/b/b/m;)V

    invoke-direct {v1, v2}, Lcom/a/b/b/di;-><init>(Lcom/a/b/b/du;)V

    .line 130
    return-object v1
.end method

.method private static a(I)Lcom/a/b/b/di;
    .registers 3

    .prologue
    .line 277
    if-lez p0, :cond_13

    const/4 v0, 0x1

    :goto_3
    const-string v1, "The length may not be less than 1"

    invoke-static {v0, v1}, Lcom/a/b/b/cn;->a(ZLjava/lang/Object;)V

    .line 279
    new-instance v0, Lcom/a/b/b/di;

    new-instance v1, Lcom/a/b/b/dp;

    invoke-direct {v1, p0}, Lcom/a/b/b/dp;-><init>(I)V

    invoke-direct {v0, v1}, Lcom/a/b/b/di;-><init>(Lcom/a/b/b/du;)V

    return-object v0

    .line 277
    :cond_13
    const/4 v0, 0x0

    goto :goto_3
.end method

.method private static a(Lcom/a/b/b/m;)Lcom/a/b/b/di;
    .registers 3

    .prologue
    .line 144
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 146
    new-instance v0, Lcom/a/b/b/di;

    new-instance v1, Lcom/a/b/b/dj;

    invoke-direct {v1, p0}, Lcom/a/b/b/dj;-><init>(Lcom/a/b/b/m;)V

    invoke-direct {v0, v1}, Lcom/a/b/b/di;-><init>(Lcom/a/b/b/du;)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;)Lcom/a/b/b/di;
    .registers 3

    .prologue
    .line 171
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_17

    const/4 v0, 0x1

    :goto_7
    const-string v1, "The separator may not be the empty string."

    invoke-static {v0, v1}, Lcom/a/b/b/cn;->a(ZLjava/lang/Object;)V

    .line 174
    new-instance v0, Lcom/a/b/b/di;

    new-instance v1, Lcom/a/b/b/dl;

    invoke-direct {v1, p0}, Lcom/a/b/b/dl;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1}, Lcom/a/b/b/di;-><init>(Lcom/a/b/b/du;)V

    return-object v0

    .line 171
    :cond_17
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public static a(Ljava/util/regex/Pattern;)Lcom/a/b/b/di;
    .registers 5
    .annotation build Lcom/a/b/a/c;
        a = "java.util.regex"
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 216
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 217
    const-string v0, ""

    invoke-virtual {p0, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-nez v0, :cond_26

    move v0, v1

    :goto_12
    const-string v3, "The pattern may not match the empty string: %s"

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p0, v1, v2

    invoke-static {v0, v3, v1}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 220
    new-instance v0, Lcom/a/b/b/di;

    new-instance v1, Lcom/a/b/b/dn;

    invoke-direct {v1, p0}, Lcom/a/b/b/dn;-><init>(Ljava/util/regex/Pattern;)V

    invoke-direct {v0, v1}, Lcom/a/b/b/di;-><init>(Lcom/a/b/b/du;)V

    return-object v0

    :cond_26
    move v0, v2

    .line 217
    goto :goto_12
.end method

.method private a(Lcom/a/b/b/di;)Lcom/a/b/b/ds;
    .registers 4
    .annotation build Lcom/a/b/a/a;
    .end annotation

    .annotation runtime Ljavax/annotation/CheckReturnValue;
    .end annotation

    .prologue
    .line 462
    new-instance v0, Lcom/a/b/b/ds;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lcom/a/b/b/ds;-><init>(Lcom/a/b/b/di;Lcom/a/b/b/di;B)V

    return-object v0
.end method

.method private static synthetic a(Lcom/a/b/b/di;Ljava/lang/CharSequence;)Ljava/util/Iterator;
    .registers 3

    .prologue
    .line 103
    invoke-virtual {p0, p1}, Lcom/a/b/b/di;->b(Ljava/lang/CharSequence;)Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method private b(I)Lcom/a/b/b/di;
    .registers 7
    .annotation runtime Ljavax/annotation/CheckReturnValue;
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 340
    if-lez p1, :cond_1e

    move v0, v1

    :goto_5
    const-string v3, "must be greater than zero: %s"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v2

    invoke-static {v0, v3, v1}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 341
    new-instance v0, Lcom/a/b/b/di;

    iget-object v1, p0, Lcom/a/b/b/di;->d:Lcom/a/b/b/du;

    iget-boolean v2, p0, Lcom/a/b/b/di;->b:Z

    iget-object v3, p0, Lcom/a/b/b/di;->a:Lcom/a/b/b/m;

    invoke-direct {v0, v1, v2, v3, p1}, Lcom/a/b/b/di;-><init>(Lcom/a/b/b/du;ZLcom/a/b/b/m;I)V

    return-object v0

    :cond_1e
    move v0, v2

    .line 340
    goto :goto_5
.end method

.method private b(Lcom/a/b/b/m;)Lcom/a/b/b/di;
    .registers 6
    .annotation runtime Ljavax/annotation/CheckReturnValue;
    .end annotation

    .prologue
    .line 373
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 374
    new-instance v0, Lcom/a/b/b/di;

    iget-object v1, p0, Lcom/a/b/b/di;->d:Lcom/a/b/b/du;

    iget-boolean v2, p0, Lcom/a/b/b/di;->b:Z

    iget v3, p0, Lcom/a/b/b/di;->c:I

    invoke-direct {v0, v1, v2, p1, v3}, Lcom/a/b/b/di;-><init>(Lcom/a/b/b/du;ZLcom/a/b/b/m;I)V

    return-object v0
.end method

.method private static b(Ljava/lang/String;)Lcom/a/b/b/di;
    .registers 2
    .annotation build Lcom/a/b/a/c;
        a = "java.util.regex"
    .end annotation

    .prologue
    .line 254
    invoke-static {p0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/b/di;->a(Ljava/util/regex/Pattern;)Lcom/a/b/b/di;

    move-result-object v0

    return-object v0
.end method

.method private b(C)Lcom/a/b/b/ds;
    .registers 3
    .annotation build Lcom/a/b/a/a;
    .end annotation

    .annotation runtime Ljavax/annotation/CheckReturnValue;
    .end annotation

    .prologue
    .line 449
    invoke-static {p1}, Lcom/a/b/b/di;->a(C)Lcom/a/b/b/di;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/a/b/b/di;->a(Lcom/a/b/b/di;)Lcom/a/b/b/ds;

    move-result-object v0

    return-object v0
.end method

.method private static synthetic b(Lcom/a/b/b/di;)Lcom/a/b/b/m;
    .registers 2

    .prologue
    .line 103
    iget-object v0, p0, Lcom/a/b/b/di;->a:Lcom/a/b/b/m;

    return-object v0
.end method

.method private c(Ljava/lang/String;)Lcom/a/b/b/ds;
    .registers 3
    .annotation build Lcom/a/b/a/a;
    .end annotation

    .annotation runtime Ljavax/annotation/CheckReturnValue;
    .end annotation

    .prologue
    .line 437
    invoke-static {p1}, Lcom/a/b/b/di;->a(Ljava/lang/String;)Lcom/a/b/b/di;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/a/b/b/di;->a(Lcom/a/b/b/di;)Lcom/a/b/b/ds;

    move-result-object v0

    return-object v0
.end method

.method private c(Ljava/lang/CharSequence;)Ljava/util/List;
    .registers 5
    .annotation build Lcom/a/b/a/a;
    .end annotation

    .prologue
    .line 416
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 418
    invoke-virtual {p0, p1}, Lcom/a/b/b/di;->b(Ljava/lang/CharSequence;)Ljava/util/Iterator;

    move-result-object v0

    .line 419
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 421
    :goto_c
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1a

    .line 422
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_c

    .line 425
    :cond_1a
    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private static synthetic c(Lcom/a/b/b/di;)Z
    .registers 2

    .prologue
    .line 103
    iget-boolean v0, p0, Lcom/a/b/b/di;->b:Z

    return v0
.end method

.method private static synthetic d(Lcom/a/b/b/di;)I
    .registers 2

    .prologue
    .line 103
    iget v0, p0, Lcom/a/b/b/di;->c:I

    return v0
.end method


# virtual methods
.method public final a()Lcom/a/b/b/di;
    .registers 6
    .annotation runtime Ljavax/annotation/CheckReturnValue;
    .end annotation

    .prologue
    .line 316
    new-instance v0, Lcom/a/b/b/di;

    iget-object v1, p0, Lcom/a/b/b/di;->d:Lcom/a/b/b/du;

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/a/b/b/di;->a:Lcom/a/b/b/m;

    iget v4, p0, Lcom/a/b/b/di;->c:I

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/a/b/b/di;-><init>(Lcom/a/b/b/du;ZLcom/a/b/b/m;I)V

    return-object v0
.end method

.method public final a(Ljava/lang/CharSequence;)Ljava/lang/Iterable;
    .registers 3

    .prologue
    .line 386
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 388
    new-instance v0, Lcom/a/b/b/dr;

    invoke-direct {v0, p0, p1}, Lcom/a/b/b/dr;-><init>(Lcom/a/b/b/di;Ljava/lang/CharSequence;)V

    return-object v0
.end method

.method public final b()Lcom/a/b/b/di;
    .registers 6
    .annotation runtime Ljavax/annotation/CheckReturnValue;
    .end annotation

    .prologue
    .line 356
    sget-object v0, Lcom/a/b/b/m;->r:Lcom/a/b/b/m;

    .line 1373
    invoke-static {v0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1374
    new-instance v1, Lcom/a/b/b/di;

    iget-object v2, p0, Lcom/a/b/b/di;->d:Lcom/a/b/b/du;

    iget-boolean v3, p0, Lcom/a/b/b/di;->b:Z

    iget v4, p0, Lcom/a/b/b/di;->c:I

    invoke-direct {v1, v2, v3, v0, v4}, Lcom/a/b/b/di;-><init>(Lcom/a/b/b/du;ZLcom/a/b/b/m;I)V

    .line 356
    return-object v1
.end method

.method final b(Ljava/lang/CharSequence;)Ljava/util/Iterator;
    .registers 3

    .prologue
    .line 402
    iget-object v0, p0, Lcom/a/b/b/di;->d:Lcom/a/b/b/du;

    invoke-interface {v0, p0, p1}, Lcom/a/b/b/du;->a(Lcom/a/b/b/di;Ljava/lang/CharSequence;)Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method
