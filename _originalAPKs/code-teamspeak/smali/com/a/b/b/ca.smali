.class public final Lcom/a/b/b/ca;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/b;
.end annotation


# direct methods
.method private constructor <init>()V
    .registers 1

    .prologue
    .line 391
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/Object;)Lcom/a/b/b/cc;
    .registers 4

    .prologue
    .line 95
    new-instance v0, Lcom/a/b/b/cc;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-static {v1}, Lcom/a/b/b/ca;->a(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/a/b/b/cc;-><init>(Ljava/lang/String;B)V

    return-object v0
.end method

.method private static a(Ljava/lang/String;)Lcom/a/b/b/cc;
    .registers 3

    .prologue
    .line 121
    new-instance v0, Lcom/a/b/b/cc;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/a/b/b/cc;-><init>(Ljava/lang/String;B)V

    return-object v0
.end method

.method public static a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .param p0    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 52
    if-eqz p0, :cond_3

    :goto_2
    return-object p0

    :cond_3
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    goto :goto_2
.end method

.method public static a(Ljava/lang/Class;)Ljava/lang/String;
    .registers 4

    .prologue
    .line 130
    invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    .line 134
    const-string v1, "\\$[0-9]+"

    const-string v2, "\\$"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 137
    const/16 v0, 0x24

    invoke-virtual {v1, v0}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    .line 141
    const/4 v2, -0x1

    if-ne v0, v2, :cond_1b

    .line 142
    const/16 v0, 0x2e

    invoke-virtual {v1, v0}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    .line 144
    :cond_1b
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static b(Ljava/lang/Class;)Lcom/a/b/b/cc;
    .registers 4

    .prologue
    .line 109
    new-instance v0, Lcom/a/b/b/cc;

    invoke-static {p0}, Lcom/a/b/b/ca;->a(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/a/b/b/cc;-><init>(Ljava/lang/String;B)V

    return-object v0
.end method
