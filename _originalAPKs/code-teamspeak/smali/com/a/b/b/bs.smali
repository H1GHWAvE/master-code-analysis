.class final Lcom/a/b/b/bs;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/b/bj;
.implements Ljava/io/Serializable;


# static fields
.field private static final b:J


# instance fields
.field private final a:Lcom/a/b/b/co;


# direct methods
.method private constructor <init>(Lcom/a/b/b/co;)V
    .registers 3

    .prologue
    .line 252
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 253
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/b/co;

    iput-object v0, p0, Lcom/a/b/b/bs;->a:Lcom/a/b/b/co;

    .line 254
    return-void
.end method

.method synthetic constructor <init>(Lcom/a/b/b/co;B)V
    .registers 3

    .prologue
    .line 249
    invoke-direct {p0, p1}, Lcom/a/b/b/bs;-><init>(Lcom/a/b/b/co;)V

    return-void
.end method

.method private a(Ljava/lang/Object;)Ljava/lang/Boolean;
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 258
    iget-object v0, p0, Lcom/a/b/b/bs;->a:Lcom/a/b/b/co;

    invoke-interface {v0, p1}, Lcom/a/b/b/co;->a(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final synthetic e(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 249
    .line 1258
    iget-object v0, p0, Lcom/a/b/b/bs;->a:Lcom/a/b/b/co;

    invoke-interface {v0, p1}, Lcom/a/b/b/co;->a(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 249
    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .registers 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 262
    instance-of v0, p1, Lcom/a/b/b/bs;

    if-eqz v0, :cond_f

    .line 263
    check-cast p1, Lcom/a/b/b/bs;

    .line 264
    iget-object v0, p0, Lcom/a/b/b/bs;->a:Lcom/a/b/b/co;

    iget-object v1, p1, Lcom/a/b/b/bs;->a:Lcom/a/b/b/co;

    invoke-interface {v0, v1}, Lcom/a/b/b/co;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 266
    :goto_e
    return v0

    :cond_f
    const/4 v0, 0x0

    goto :goto_e
.end method

.method public final hashCode()I
    .registers 2

    .prologue
    .line 270
    iget-object v0, p0, Lcom/a/b/b/bs;->a:Lcom/a/b/b/co;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .registers 4

    .prologue
    .line 274
    iget-object v0, p0, Lcom/a/b/b/bs;->a:Lcom/a/b/b/co;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0xe

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "forPredicate("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
