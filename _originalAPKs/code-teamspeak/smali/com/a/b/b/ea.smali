.class public final Lcom/a/b/b/ea;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/b;
.end annotation


# direct methods
.method private constructor <init>()V
    .registers 1

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a()Lcom/a/b/b/bj;
    .registers 1
    .annotation build Lcom/a/b/a/a;
    .end annotation

    .prologue
    .line 291
    sget-object v0, Lcom/a/b/b/ef;->a:Lcom/a/b/b/ef;

    .line 292
    return-object v0
.end method

.method private static a(Lcom/a/b/b/bj;Lcom/a/b/b/dz;)Lcom/a/b/b/dz;
    .registers 3

    .prologue
    .line 51
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 52
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    new-instance v0, Lcom/a/b/b/ed;

    invoke-direct {v0, p0, p1}, Lcom/a/b/b/ed;-><init>(Lcom/a/b/b/bj;Lcom/a/b/b/dz;)V

    return-object v0
.end method

.method private static a(Lcom/a/b/b/dz;)Lcom/a/b/b/dz;
    .registers 3

    .prologue
    .line 103
    instance-of v0, p0, Lcom/a/b/b/ec;

    if-eqz v0, :cond_5

    :goto_4
    return-object p0

    :cond_5
    new-instance v1, Lcom/a/b/b/ec;

    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/b/dz;

    invoke-direct {v1, v0}, Lcom/a/b/b/ec;-><init>(Lcom/a/b/b/dz;)V

    move-object p0, v1

    goto :goto_4
.end method

.method private static a(Lcom/a/b/b/dz;JLjava/util/concurrent/TimeUnit;)Lcom/a/b/b/dz;
    .registers 5

    .prologue
    .line 162
    new-instance v0, Lcom/a/b/b/eb;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/a/b/b/eb;-><init>(Lcom/a/b/b/dz;JLjava/util/concurrent/TimeUnit;)V

    return-object v0
.end method

.method private static a(Ljava/lang/Object;)Lcom/a/b/b/dz;
    .registers 2
    .param p0    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 219
    new-instance v0, Lcom/a/b/b/eg;

    invoke-direct {v0, p0}, Lcom/a/b/b/eg;-><init>(Ljava/lang/Object;)V

    return-object v0
.end method

.method private static b(Lcom/a/b/b/dz;)Lcom/a/b/b/dz;
    .registers 3

    .prologue
    .line 258
    new-instance v1, Lcom/a/b/b/eh;

    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/b/dz;

    invoke-direct {v1, v0}, Lcom/a/b/b/eh;-><init>(Lcom/a/b/b/dz;)V

    return-object v1
.end method
