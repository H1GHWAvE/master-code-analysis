.class public final Lcom/a/b/b/cp;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/b;
    b = true
.end annotation


# static fields
.field private static final a:Lcom/a/b/b/bv;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 346
    const/16 v0, 0x2c

    invoke-static {v0}, Lcom/a/b/b/bv;->a(C)Lcom/a/b/b/bv;

    move-result-object v0

    sput-object v0, Lcom/a/b/b/cp;->a:Lcom/a/b/b/bv;

    return-void
.end method

.method private constructor <init>()V
    .registers 1

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()Lcom/a/b/b/co;
    .registers 1
    .annotation build Lcom/a/b/a/b;
        a = true
    .end annotation

    .prologue
    .line 59
    sget-object v0, Lcom/a/b/b/da;->a:Lcom/a/b/b/da;

    return-object v0
.end method

.method public static a(Lcom/a/b/b/co;)Lcom/a/b/b/co;
    .registers 2

    .prologue
    .line 93
    new-instance v0, Lcom/a/b/b/cz;

    invoke-direct {v0, p0}, Lcom/a/b/b/cz;-><init>(Lcom/a/b/b/co;)V

    return-object v0
.end method

.method public static a(Lcom/a/b/b/co;Lcom/a/b/b/bj;)Lcom/a/b/b/co;
    .registers 4

    .prologue
    .line 242
    new-instance v0, Lcom/a/b/b/ct;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lcom/a/b/b/ct;-><init>(Lcom/a/b/b/co;Lcom/a/b/b/bj;B)V

    return-object v0
.end method

.method public static a(Lcom/a/b/b/co;Lcom/a/b/b/co;)Lcom/a/b/b/co;
    .registers 5

    .prologue
    .line 131
    new-instance v2, Lcom/a/b/b/cr;

    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/b/co;

    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/a/b/b/co;

    invoke-static {v0, v1}, Lcom/a/b/b/cp;->c(Lcom/a/b/b/co;Lcom/a/b/b/co;)Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {v2, v0, v1}, Lcom/a/b/b/cr;-><init>(Ljava/util/List;B)V

    return-object v2
.end method

.method public static a(Ljava/lang/Class;)Lcom/a/b/b/co;
    .registers 3
    .annotation build Lcom/a/b/a/c;
        a = "Class.isInstance"
    .end annotation

    .prologue
    .line 201
    new-instance v0, Lcom/a/b/b/cx;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/a/b/b/cx;-><init>(Ljava/lang/Class;B)V

    return-object v0
.end method

.method private static a(Ljava/lang/Iterable;)Lcom/a/b/b/co;
    .registers 4

    .prologue
    .line 107
    new-instance v0, Lcom/a/b/b/cr;

    invoke-static {p0}, Lcom/a/b/b/cp;->c(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/a/b/b/cr;-><init>(Ljava/util/List;B)V

    return-object v0
.end method

.method public static a(Ljava/lang/Object;)Lcom/a/b/b/co;
    .registers 3
    .param p0    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 179
    if-nez p0, :cond_5

    .line 5076
    sget-object v0, Lcom/a/b/b/da;->c:Lcom/a/b/b/da;

    .line 179
    :goto_4
    return-object v0

    :cond_5
    new-instance v0, Lcom/a/b/b/cy;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/a/b/b/cy;-><init>(Ljava/lang/Object;B)V

    goto :goto_4
.end method

.method private static a(Ljava/lang/String;)Lcom/a/b/b/co;
    .registers 2
    .annotation build Lcom/a/b/a/c;
        a = "java.util.regex.Pattern"
    .end annotation

    .prologue
    .line 256
    new-instance v0, Lcom/a/b/b/cu;

    invoke-direct {v0, p0}, Lcom/a/b/b/cu;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static a(Ljava/util/Collection;)Lcom/a/b/b/co;
    .registers 3

    .prologue
    .line 231
    new-instance v0, Lcom/a/b/b/cw;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/a/b/b/cw;-><init>(Ljava/util/Collection;B)V

    return-object v0
.end method

.method private static a(Ljava/util/regex/Pattern;)Lcom/a/b/b/co;
    .registers 2
    .annotation build Lcom/a/b/a/c;
        a = "java.util.regex.Pattern"
    .end annotation

    .prologue
    .line 269
    new-instance v0, Lcom/a/b/b/cv;

    invoke-direct {v0, p0}, Lcom/a/b/b/cv;-><init>(Ljava/util/regex/Pattern;)V

    return-object v0
.end method

.method private static varargs a([Lcom/a/b/b/co;)Lcom/a/b/b/co;
    .registers 4

    .prologue
    .line 120
    new-instance v0, Lcom/a/b/b/cr;

    invoke-static {p0}, Lcom/a/b/b/cp;->a([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/a/b/b/cr;-><init>(Ljava/util/List;B)V

    return-object v0
.end method

.method private static varargs a([Ljava/lang/Object;)Ljava/util/List;
    .registers 2

    .prologue
    .line 642
    invoke-static {p0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/b/cp;->c(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b()Lcom/a/b/b/bv;
    .registers 1

    .prologue
    .line 48
    sget-object v0, Lcom/a/b/b/cp;->a:Lcom/a/b/b/bv;

    return-object v0
.end method

.method private static b(Lcom/a/b/b/co;Lcom/a/b/b/co;)Lcom/a/b/b/co;
    .registers 5

    .prologue
    .line 170
    new-instance v2, Lcom/a/b/b/df;

    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/b/co;

    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/a/b/b/co;

    invoke-static {v0, v1}, Lcom/a/b/b/cp;->c(Lcom/a/b/b/co;Lcom/a/b/b/co;)Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {v2, v0, v1}, Lcom/a/b/b/df;-><init>(Ljava/util/List;B)V

    return-object v2
.end method

.method private static b(Ljava/lang/Class;)Lcom/a/b/b/co;
    .registers 3
    .annotation build Lcom/a/b/a/a;
    .end annotation

    .annotation build Lcom/a/b/a/c;
        a = "Class.isAssignableFrom"
    .end annotation

    .prologue
    .line 214
    new-instance v0, Lcom/a/b/b/cs;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/a/b/b/cs;-><init>(Ljava/lang/Class;B)V

    return-object v0
.end method

.method private static b(Ljava/lang/Iterable;)Lcom/a/b/b/co;
    .registers 4

    .prologue
    .line 146
    new-instance v0, Lcom/a/b/b/df;

    invoke-static {p0}, Lcom/a/b/b/cp;->c(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/a/b/b/df;-><init>(Ljava/util/List;B)V

    return-object v0
.end method

.method private static varargs b([Lcom/a/b/b/co;)Lcom/a/b/b/co;
    .registers 4

    .prologue
    .line 159
    new-instance v0, Lcom/a/b/b/df;

    invoke-static {p0}, Lcom/a/b/b/cp;->a([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/a/b/b/df;-><init>(Ljava/util/List;B)V

    return-object v0
.end method

.method private static c()Lcom/a/b/b/co;
    .registers 1
    .annotation build Lcom/a/b/a/b;
        a = true
    .end annotation

    .prologue
    .line 67
    sget-object v0, Lcom/a/b/b/da;->b:Lcom/a/b/b/da;

    return-object v0
.end method

.method private static c(Lcom/a/b/b/co;Lcom/a/b/b/co;)Ljava/util/List;
    .registers 4

    .prologue
    .line 638
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/a/b/b/co;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private static c(Ljava/lang/Iterable;)Ljava/util/List;
    .registers 4

    .prologue
    .line 646
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 647
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_9
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1b

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 648
    invoke-static {v2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_9

    .line 650
    :cond_1b
    return-object v0
.end method

.method private static d()Lcom/a/b/b/co;
    .registers 1
    .annotation build Lcom/a/b/a/b;
        a = true
    .end annotation

    .prologue
    .line 76
    sget-object v0, Lcom/a/b/b/da;->c:Lcom/a/b/b/da;

    return-object v0
.end method

.method private static e()Lcom/a/b/b/co;
    .registers 1
    .annotation build Lcom/a/b/a/b;
        a = true
    .end annotation

    .prologue
    .line 85
    sget-object v0, Lcom/a/b/b/da;->d:Lcom/a/b/b/da;

    return-object v0
.end method
