.class public final Lcom/a/b/b/cg;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private b:Lcom/a/b/b/ch;

.field private c:Lcom/a/b/b/ch;

.field private d:Z


# direct methods
.method private constructor <init>(Ljava/lang/String;)V
    .registers 4

    .prologue
    const/4 v1, 0x0

    .line 207
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 200
    new-instance v0, Lcom/a/b/b/ch;

    invoke-direct {v0, v1}, Lcom/a/b/b/ch;-><init>(B)V

    iput-object v0, p0, Lcom/a/b/b/cg;->b:Lcom/a/b/b/ch;

    .line 201
    iget-object v0, p0, Lcom/a/b/b/cg;->b:Lcom/a/b/b/ch;

    iput-object v0, p0, Lcom/a/b/b/cg;->c:Lcom/a/b/b/ch;

    .line 202
    iput-boolean v1, p0, Lcom/a/b/b/cg;->d:Z

    .line 208
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/a/b/b/cg;->a:Ljava/lang/String;

    .line 209
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;B)V
    .registers 3

    .prologue
    .line 198
    invoke-direct {p0, p1}, Lcom/a/b/b/cg;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method private a()Lcom/a/b/b/cg;
    .registers 2

    .prologue
    .line 219
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/a/b/b/cg;->d:Z

    .line 220
    return-object p0
.end method

.method private a(C)Lcom/a/b/b/cg;
    .registers 3

    .prologue
    .line 324
    invoke-static {p1}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/a/b/b/cg;->b(Ljava/lang/Object;)Lcom/a/b/b/cg;

    move-result-object v0

    return-object v0
.end method

.method private a(D)Lcom/a/b/b/cg;
    .registers 4

    .prologue
    .line 336
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/a/b/b/cg;->b(Ljava/lang/Object;)Lcom/a/b/b/cg;

    move-result-object v0

    return-object v0
.end method

.method private a(F)Lcom/a/b/b/cg;
    .registers 3

    .prologue
    .line 348
    invoke-static {p1}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/a/b/b/cg;->b(Ljava/lang/Object;)Lcom/a/b/b/cg;

    move-result-object v0

    return-object v0
.end method

.method private a(I)Lcom/a/b/b/cg;
    .registers 3

    .prologue
    .line 360
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/a/b/b/cg;->b(Ljava/lang/Object;)Lcom/a/b/b/cg;

    move-result-object v0

    return-object v0
.end method

.method private a(J)Lcom/a/b/b/cg;
    .registers 4

    .prologue
    .line 372
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/a/b/b/cg;->b(Ljava/lang/Object;)Lcom/a/b/b/cg;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/Object;)Lcom/a/b/b/cg;
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 300
    invoke-direct {p0, p1}, Lcom/a/b/b/cg;->b(Ljava/lang/Object;)Lcom/a/b/b/cg;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;C)Lcom/a/b/b/cg;
    .registers 4

    .prologue
    .line 250
    invoke-static {p2}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/a/b/b/cg;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/a/b/b/cg;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;D)Lcom/a/b/b/cg;
    .registers 6

    .prologue
    .line 260
    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/a/b/b/cg;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/a/b/b/cg;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;F)Lcom/a/b/b/cg;
    .registers 4

    .prologue
    .line 270
    invoke-static {p2}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/a/b/b/cg;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/a/b/b/cg;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;I)Lcom/a/b/b/cg;
    .registers 4

    .prologue
    .line 280
    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/a/b/b/cg;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/a/b/b/cg;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;J)Lcom/a/b/b/cg;
    .registers 6

    .prologue
    .line 290
    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/a/b/b/cg;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/a/b/b/cg;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;Z)Lcom/a/b/b/cg;
    .registers 4

    .prologue
    .line 240
    invoke-static {p2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/a/b/b/cg;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/a/b/b/cg;

    move-result-object v0

    return-object v0
.end method

.method private a(Z)Lcom/a/b/b/cg;
    .registers 3

    .prologue
    .line 312
    invoke-static {p1}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/a/b/b/cg;->b(Ljava/lang/Object;)Lcom/a/b/b/cg;

    move-result-object v0

    return-object v0
.end method

.method private b(Ljava/lang/Object;)Lcom/a/b/b/cg;
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 413
    invoke-direct {p0}, Lcom/a/b/b/cg;->b()Lcom/a/b/b/ch;

    move-result-object v0

    .line 414
    iput-object p1, v0, Lcom/a/b/b/ch;->b:Ljava/lang/Object;

    .line 415
    return-object p0
.end method

.method private b(Ljava/lang/String;Ljava/lang/Object;)Lcom/a/b/b/cg;
    .registers 4
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 230
    invoke-virtual {p0, p1, p2}, Lcom/a/b/b/cg;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/a/b/b/cg;

    move-result-object v0

    return-object v0
.end method

.method private b()Lcom/a/b/b/ch;
    .registers 3

    .prologue
    .line 407
    new-instance v0, Lcom/a/b/b/ch;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/a/b/b/ch;-><init>(B)V

    .line 408
    iget-object v1, p0, Lcom/a/b/b/cg;->c:Lcom/a/b/b/ch;

    iput-object v0, v1, Lcom/a/b/b/ch;->c:Lcom/a/b/b/ch;

    iput-object v0, p0, Lcom/a/b/b/cg;->c:Lcom/a/b/b/ch;

    .line 409
    return-object v0
.end method


# virtual methods
.method final a(Ljava/lang/String;Ljava/lang/Object;)Lcom/a/b/b/cg;
    .registers 5
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 419
    invoke-direct {p0}, Lcom/a/b/b/cg;->b()Lcom/a/b/b/ch;

    move-result-object v1

    .line 420
    iput-object p2, v1, Lcom/a/b/b/ch;->b:Ljava/lang/Object;

    .line 421
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, v1, Lcom/a/b/b/ch;->a:Ljava/lang/String;

    .line 422
    return-object p0
.end method

.method public final toString()Ljava/lang/String;
    .registers 8

    .prologue
    .line 387
    iget-boolean v2, p0, Lcom/a/b/b/cg;->d:Z

    .line 388
    const-string v1, ""

    .line 389
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v3, 0x20

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    iget-object v3, p0, Lcom/a/b/b/cg;->a:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v3, 0x7b

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 391
    iget-object v0, p0, Lcom/a/b/b/cg;->b:Lcom/a/b/b/ch;

    iget-object v0, v0, Lcom/a/b/b/ch;->c:Lcom/a/b/b/ch;

    move-object v6, v0

    move-object v0, v1

    move-object v1, v6

    :goto_1e
    if-eqz v1, :cond_42

    .line 393
    if-eqz v2, :cond_26

    iget-object v4, v1, Lcom/a/b/b/ch;->b:Ljava/lang/Object;

    if-eqz v4, :cond_3f

    .line 394
    :cond_26
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 395
    const-string v0, ", "

    .line 397
    iget-object v4, v1, Lcom/a/b/b/ch;->a:Ljava/lang/String;

    if-eqz v4, :cond_3a

    .line 398
    iget-object v4, v1, Lcom/a/b/b/ch;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/16 v5, 0x3d

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 400
    :cond_3a
    iget-object v4, v1, Lcom/a/b/b/ch;->b:Ljava/lang/Object;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 392
    :cond_3f
    iget-object v1, v1, Lcom/a/b/b/ch;->c:Lcom/a/b/b/ch;

    goto :goto_1e

    .line 403
    :cond_42
    const/16 v0, 0x7d

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
