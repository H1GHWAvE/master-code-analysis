.class final Lcom/a/b/b/ax;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/b/co;
.implements Ljava/io/Serializable;


# static fields
.field private static final c:J


# instance fields
.field private final a:Lcom/a/b/b/au;

.field private final b:Ljava/lang/Object;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/a/b/b/au;Ljava/lang/Object;)V
    .registers 4
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 263
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 264
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/b/au;

    iput-object v0, p0, Lcom/a/b/b/ax;->a:Lcom/a/b/b/au;

    .line 265
    iput-object p2, p0, Lcom/a/b/b/ax;->b:Ljava/lang/Object;

    .line 266
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Z
    .registers 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 269
    iget-object v0, p0, Lcom/a/b/b/ax;->a:Lcom/a/b/b/au;

    iget-object v1, p0, Lcom/a/b/b/ax;->b:Ljava/lang/Object;

    invoke-virtual {v0, p1, v1}, Lcom/a/b/b/au;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .registers 6
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 273
    if-ne p0, p1, :cond_5

    .line 281
    :cond_4
    :goto_4
    return v0

    .line 276
    :cond_5
    instance-of v2, p1, Lcom/a/b/b/ax;

    if-eqz v2, :cond_21

    .line 277
    check-cast p1, Lcom/a/b/b/ax;

    .line 278
    iget-object v2, p0, Lcom/a/b/b/ax;->a:Lcom/a/b/b/au;

    iget-object v3, p1, Lcom/a/b/b/ax;->a:Lcom/a/b/b/au;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1f

    iget-object v2, p0, Lcom/a/b/b/ax;->b:Ljava/lang/Object;

    iget-object v3, p1, Lcom/a/b/b/ax;->b:Ljava/lang/Object;

    invoke-static {v2, v3}, Lcom/a/b/b/ce;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    :cond_1f
    move v0, v1

    goto :goto_4

    :cond_21
    move v0, v1

    .line 281
    goto :goto_4
.end method

.method public final hashCode()I
    .registers 4

    .prologue
    .line 285
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/a/b/b/ax;->a:Lcom/a/b/b/au;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/a/b/b/ax;->b:Ljava/lang/Object;

    aput-object v2, v0, v1

    .line 1084
    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    .line 285
    return v0
.end method

.method public final toString()Ljava/lang/String;
    .registers 6

    .prologue
    .line 289
    iget-object v0, p0, Lcom/a/b/b/ax;->a:Lcom/a/b/b/au;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/b/ax;->b:Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0xf

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ".equivalentTo("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
