.class final Lcom/a/b/b/an;
.super Lcom/a/b/b/ak;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final c:J


# instance fields
.field final a:Lcom/a/b/b/ak;

.field final b:Lcom/a/b/b/ak;


# direct methods
.method constructor <init>(Lcom/a/b/b/ak;Lcom/a/b/b/ak;)V
    .registers 3

    .prologue
    .line 303
    invoke-direct {p0}, Lcom/a/b/b/ak;-><init>()V

    .line 304
    iput-object p1, p0, Lcom/a/b/b/an;->a:Lcom/a/b/b/ak;

    .line 305
    iput-object p2, p0, Lcom/a/b/b/an;->b:Lcom/a/b/b/ak;

    .line 306
    return-void
.end method


# virtual methods
.method protected final a(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 322
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method protected final b(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 317
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method final c(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 328
    iget-object v0, p0, Lcom/a/b/b/an;->b:Lcom/a/b/b/ak;

    iget-object v1, p0, Lcom/a/b/b/an;->a:Lcom/a/b/b/ak;

    invoke-virtual {v1, p1}, Lcom/a/b/b/ak;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/a/b/b/ak;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method final d(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 334
    iget-object v0, p0, Lcom/a/b/b/an;->a:Lcom/a/b/b/ak;

    iget-object v1, p0, Lcom/a/b/b/an;->b:Lcom/a/b/b/ak;

    invoke-virtual {v1, p1}, Lcom/a/b/b/ak;->d(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/a/b/b/ak;->d(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .registers 5
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 339
    instance-of v1, p1, Lcom/a/b/b/an;

    if-eqz v1, :cond_1c

    .line 340
    check-cast p1, Lcom/a/b/b/an;

    .line 341
    iget-object v1, p0, Lcom/a/b/b/an;->a:Lcom/a/b/b/ak;

    iget-object v2, p1, Lcom/a/b/b/an;->a:Lcom/a/b/b/ak;

    invoke-virtual {v1, v2}, Lcom/a/b/b/ak;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1c

    iget-object v1, p0, Lcom/a/b/b/an;->b:Lcom/a/b/b/ak;

    iget-object v2, p1, Lcom/a/b/b/an;->b:Lcom/a/b/b/ak;

    invoke-virtual {v1, v2}, Lcom/a/b/b/ak;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1c

    const/4 v0, 0x1

    .line 344
    :cond_1c
    return v0
.end method

.method public final hashCode()I
    .registers 3

    .prologue
    .line 349
    iget-object v0, p0, Lcom/a/b/b/an;->a:Lcom/a/b/b/ak;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/a/b/b/an;->b:Lcom/a/b/b/ak;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .registers 6

    .prologue
    .line 354
    iget-object v0, p0, Lcom/a/b/b/an;->a:Lcom/a/b/b/ak;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/b/an;->b:Lcom/a/b/b/ak;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0xa

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ".andThen("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
