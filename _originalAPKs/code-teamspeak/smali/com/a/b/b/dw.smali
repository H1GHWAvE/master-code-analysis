.class public final Lcom/a/b/b/dw;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/a;
.end annotation

.annotation build Lcom/a/b/a/b;
    b = true
.end annotation


# instance fields
.field public a:Z

.field private final b:Lcom/a/b/b/ej;

.field private c:J

.field private d:J


# direct methods
.method public constructor <init>()V
    .registers 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 130
    invoke-static {}, Lcom/a/b/b/ej;->b()Lcom/a/b/b/ej;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/a/b/b/dw;-><init>(Lcom/a/b/b/ej;)V

    .line 131
    return-void
.end method

.method private constructor <init>(Lcom/a/b/b/ej;)V
    .registers 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 140
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 141
    const-string v0, "ticker"

    invoke-static {p1, v0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/b/ej;

    iput-object v0, p0, Lcom/a/b/b/dw;->b:Lcom/a/b/b/ej;

    .line 142
    return-void
.end method

.method public static a()Lcom/a/b/b/dw;
    .registers 1

    .prologue
    .line 109
    new-instance v0, Lcom/a/b/b/dw;

    invoke-direct {v0}, Lcom/a/b/b/dw;-><init>()V

    invoke-virtual {v0}, Lcom/a/b/b/dw;->b()Lcom/a/b/b/dw;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/a/b/b/ej;)Lcom/a/b/b/dw;
    .registers 2

    .prologue
    .line 99
    new-instance v0, Lcom/a/b/b/dw;

    invoke-direct {v0, p0}, Lcom/a/b/b/dw;-><init>(Lcom/a/b/b/ej;)V

    return-object v0
.end method

.method private static a(J)Ljava/util/concurrent/TimeUnit;
    .registers 6

    .prologue
    const-wide/16 v2, 0x0

    .line 226
    sget-object v0, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    sget-object v1, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, p0, p1, v1}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    cmp-long v0, v0, v2

    if-lez v0, :cond_11

    .line 227
    sget-object v0, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    .line 244
    :goto_10
    return-object v0

    .line 229
    :cond_11
    sget-object v0, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    sget-object v1, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, p0, p1, v1}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    cmp-long v0, v0, v2

    if-lez v0, :cond_20

    .line 230
    sget-object v0, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    goto :goto_10

    .line 232
    :cond_20
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    sget-object v1, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, p0, p1, v1}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    cmp-long v0, v0, v2

    if-lez v0, :cond_2f

    .line 233
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    goto :goto_10

    .line 235
    :cond_2f
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    sget-object v1, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, p0, p1, v1}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    cmp-long v0, v0, v2

    if-lez v0, :cond_3e

    .line 236
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    goto :goto_10

    .line 238
    :cond_3e
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    sget-object v1, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, p0, p1, v1}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    cmp-long v0, v0, v2

    if-lez v0, :cond_4d

    .line 239
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    goto :goto_10

    .line 241
    :cond_4d
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MICROSECONDS:Ljava/util/concurrent/TimeUnit;

    sget-object v1, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, p0, p1, v1}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    cmp-long v0, v0, v2

    if-lez v0, :cond_5c

    .line 242
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MICROSECONDS:Ljava/util/concurrent/TimeUnit;

    goto :goto_10

    .line 244
    :cond_5c
    sget-object v0, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    goto :goto_10
.end method

.method private static b(Lcom/a/b/b/ej;)Lcom/a/b/b/dw;
    .registers 2

    .prologue
    .line 119
    new-instance v0, Lcom/a/b/b/dw;

    invoke-direct {v0, p0}, Lcom/a/b/b/dw;-><init>(Lcom/a/b/b/ej;)V

    invoke-virtual {v0}, Lcom/a/b/b/dw;->b()Lcom/a/b/b/dw;

    move-result-object v0

    return-object v0
.end method

.method private static b(Ljava/util/concurrent/TimeUnit;)Ljava/lang/String;
    .registers 3

    .prologue
    .line 248
    sget-object v0, Lcom/a/b/b/dx;->a:[I

    invoke-virtual {p0}, Ljava/util/concurrent/TimeUnit;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_26

    .line 264
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 250
    :pswitch_11
    const-string v0, "ns"

    .line 262
    :goto_13
    return-object v0

    .line 252
    :pswitch_14
    const-string v0, "\u03bcs"

    goto :goto_13

    .line 254
    :pswitch_17
    const-string v0, "ms"

    goto :goto_13

    .line 256
    :pswitch_1a
    const-string v0, "s"

    goto :goto_13

    .line 258
    :pswitch_1d
    const-string v0, "min"

    goto :goto_13

    .line 260
    :pswitch_20
    const-string v0, "h"

    goto :goto_13

    .line 262
    :pswitch_23
    const-string v0, "d"

    goto :goto_13

    .line 248
    :pswitch_data_26
    .packed-switch 0x1
        :pswitch_11
        :pswitch_14
        :pswitch_17
        :pswitch_1a
        :pswitch_1d
        :pswitch_20
        :pswitch_23
    .end packed-switch
.end method

.method private static d()Lcom/a/b/b/dw;
    .registers 1

    .prologue
    .line 89
    new-instance v0, Lcom/a/b/b/dw;

    invoke-direct {v0}, Lcom/a/b/b/dw;-><init>()V

    return-object v0
.end method

.method private e()Z
    .registers 2

    .prologue
    .line 150
    iget-boolean v0, p0, Lcom/a/b/b/dw;->a:Z

    return v0
.end method

.method private f()Lcom/a/b/b/dw;
    .registers 3

    .prologue
    .line 188
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/a/b/b/dw;->c:J

    .line 189
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/a/b/b/dw;->a:Z

    .line 190
    return-object p0
.end method

.method private g()J
    .registers 5

    .prologue
    .line 194
    iget-boolean v0, p0, Lcom/a/b/b/dw;->a:Z

    if-eqz v0, :cond_11

    iget-object v0, p0, Lcom/a/b/b/dw;->b:Lcom/a/b/b/ej;

    invoke-virtual {v0}, Lcom/a/b/b/ej;->a()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/a/b/b/dw;->d:J

    sub-long/2addr v0, v2

    iget-wide v2, p0, Lcom/a/b/b/dw;->c:J

    add-long/2addr v0, v2

    :goto_10
    return-wide v0

    :cond_11
    iget-wide v0, p0, Lcom/a/b/b/dw;->c:J

    goto :goto_10
.end method


# virtual methods
.method public final a(Ljava/util/concurrent/TimeUnit;)J
    .registers 5

    .prologue
    .line 208
    invoke-direct {p0}, Lcom/a/b/b/dw;->g()J

    move-result-wide v0

    sget-object v2, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p1, v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    return-wide v0
.end method

.method public final b()Lcom/a/b/b/dw;
    .registers 4

    .prologue
    const/4 v1, 0x1

    .line 160
    iget-boolean v0, p0, Lcom/a/b/b/dw;->a:Z

    if-nez v0, :cond_16

    move v0, v1

    :goto_6
    const-string v2, "This stopwatch is already running."

    invoke-static {v0, v2}, Lcom/a/b/b/cn;->b(ZLjava/lang/Object;)V

    .line 161
    iput-boolean v1, p0, Lcom/a/b/b/dw;->a:Z

    .line 162
    iget-object v0, p0, Lcom/a/b/b/dw;->b:Lcom/a/b/b/ej;

    invoke-virtual {v0}, Lcom/a/b/b/ej;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/a/b/b/dw;->d:J

    .line 163
    return-object p0

    .line 160
    :cond_16
    const/4 v0, 0x0

    goto :goto_6
.end method

.method public final c()Lcom/a/b/b/dw;
    .registers 7

    .prologue
    .line 174
    iget-object v0, p0, Lcom/a/b/b/dw;->b:Lcom/a/b/b/ej;

    invoke-virtual {v0}, Lcom/a/b/b/ej;->a()J

    move-result-wide v0

    .line 175
    iget-boolean v2, p0, Lcom/a/b/b/dw;->a:Z

    const-string v3, "This stopwatch is already stopped."

    invoke-static {v2, v3}, Lcom/a/b/b/cn;->b(ZLjava/lang/Object;)V

    .line 176
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/a/b/b/dw;->a:Z

    .line 177
    iget-wide v2, p0, Lcom/a/b/b/dw;->c:J

    iget-wide v4, p0, Lcom/a/b/b/dw;->d:J

    sub-long/2addr v0, v4

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/a/b/b/dw;->c:J

    .line 178
    return-object p0
.end method

.method public final toString()Ljava/lang/String;
    .registers 7
    .annotation build Lcom/a/b/a/c;
        a = "String.format()"
    .end annotation

    .prologue
    const-wide/16 v4, 0x0

    .line 216
    invoke-direct {p0}, Lcom/a/b/b/dw;->g()J

    move-result-wide v2

    .line 1226
    sget-object v0, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    sget-object v1, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    cmp-long v0, v0, v4

    if-lez v0, :cond_3d

    .line 1227
    sget-object v0, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    .line 219
    :goto_14
    long-to-double v2, v2

    sget-object v1, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v4, 0x1

    invoke-virtual {v1, v4, v5, v0}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v4

    long-to-double v4, v4

    div-double/2addr v2, v4

    .line 222
    const-string v1, "%.4g %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    aput-object v2, v4, v5

    const/4 v2, 0x1

    .line 1248
    sget-object v3, Lcom/a/b/b/dx;->a:[I

    invoke-virtual {v0}, Ljava/util/concurrent/TimeUnit;->ordinal()I

    move-result v0

    aget v0, v3, v0

    packed-switch v0, :pswitch_data_a6

    .line 1264
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1229
    :cond_3d
    sget-object v0, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    sget-object v1, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    cmp-long v0, v0, v4

    if-lez v0, :cond_4c

    .line 1230
    sget-object v0, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    goto :goto_14

    .line 1232
    :cond_4c
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    sget-object v1, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    cmp-long v0, v0, v4

    if-lez v0, :cond_5b

    .line 1233
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    goto :goto_14

    .line 1235
    :cond_5b
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    sget-object v1, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    cmp-long v0, v0, v4

    if-lez v0, :cond_6a

    .line 1236
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    goto :goto_14

    .line 1238
    :cond_6a
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    sget-object v1, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    cmp-long v0, v0, v4

    if-lez v0, :cond_79

    .line 1239
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    goto :goto_14

    .line 1241
    :cond_79
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MICROSECONDS:Ljava/util/concurrent/TimeUnit;

    sget-object v1, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    cmp-long v0, v0, v4

    if-lez v0, :cond_88

    .line 1242
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MICROSECONDS:Ljava/util/concurrent/TimeUnit;

    goto :goto_14

    .line 1244
    :cond_88
    sget-object v0, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    goto :goto_14

    .line 1250
    :pswitch_8b
    const-string v0, "ns"

    .line 222
    :goto_8d
    aput-object v0, v4, v2

    invoke-static {v1, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1252
    :pswitch_94
    const-string v0, "\u03bcs"

    goto :goto_8d

    .line 1254
    :pswitch_97
    const-string v0, "ms"

    goto :goto_8d

    .line 1256
    :pswitch_9a
    const-string v0, "s"

    goto :goto_8d

    .line 1258
    :pswitch_9d
    const-string v0, "min"

    goto :goto_8d

    .line 1260
    :pswitch_a0
    const-string v0, "h"

    goto :goto_8d

    .line 1262
    :pswitch_a3
    const-string v0, "d"

    goto :goto_8d

    .line 1248
    :pswitch_data_a6
    .packed-switch 0x1
        :pswitch_8b
        :pswitch_94
        :pswitch_97
        :pswitch_9a
        :pswitch_9d
        :pswitch_a0
        :pswitch_a3
    .end packed-switch
.end method
