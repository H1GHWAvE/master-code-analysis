.class final Lcom/a/b/b/cl;
.super Lcom/a/b/b/au;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation build Lcom/a/b/a/b;
    a = true
.end annotation


# static fields
.field private static final b:J = 0x1L


# instance fields
.field final a:Lcom/a/b/b/au;


# direct methods
.method constructor <init>(Lcom/a/b/b/au;)V
    .registers 3

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/a/b/b/au;-><init>()V

    .line 33
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/b/au;

    iput-object v0, p0, Lcom/a/b/b/cl;->a:Lcom/a/b/b/au;

    .line 34
    return-void
.end method

.method private a(Ljava/lang/Iterable;)I
    .registers 6

    .prologue
    .line 52
    const v0, 0x13381

    .line 53
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_7
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1b

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 54
    mul-int/lit16 v0, v0, 0x616f

    iget-object v3, p0, Lcom/a/b/b/cl;->a:Lcom/a/b/b/au;

    invoke-virtual {v3, v2}, Lcom/a/b/b/au;->a(Ljava/lang/Object;)I

    move-result v2

    add-int/2addr v0, v2

    .line 55
    goto :goto_7

    .line 56
    :cond_1b
    return v0
.end method

.method private a(Ljava/lang/Iterable;Ljava/lang/Iterable;)Z
    .registers 9

    .prologue
    const/4 v0, 0x0

    .line 38
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 39
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 41
    :cond_9
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_26

    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_26

    .line 42
    iget-object v3, p0, Lcom/a/b/b/cl;->a:Lcom/a/b/b/au;

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/a/b/b/au;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_9

    .line 47
    :cond_25
    :goto_25
    return v0

    :cond_26
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_25

    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_25

    const/4 v0, 0x1

    goto :goto_25
.end method


# virtual methods
.method protected final synthetic b(Ljava/lang/Object;)I
    .registers 6

    .prologue
    .line 26
    check-cast p1, Ljava/lang/Iterable;

    .line 1052
    const v0, 0x13381

    .line 1053
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_9
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1d

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 1054
    mul-int/lit16 v0, v0, 0x616f

    iget-object v3, p0, Lcom/a/b/b/cl;->a:Lcom/a/b/b/au;

    invoke-virtual {v3, v2}, Lcom/a/b/b/au;->a(Ljava/lang/Object;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1055
    goto :goto_9

    .line 26
    :cond_1d
    return v0
.end method

.method protected final synthetic b(Ljava/lang/Object;Ljava/lang/Object;)Z
    .registers 8

    .prologue
    .line 26
    check-cast p1, Ljava/lang/Iterable;

    check-cast p2, Ljava/lang/Iterable;

    .line 2038
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 2039
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 2041
    :cond_c
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2a

    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2a

    .line 2042
    iget-object v2, p0, Lcom/a/b/b/cl;->a:Lcom/a/b/b/au;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/a/b/b/au;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    .line 2047
    :cond_28
    const/4 v0, 0x0

    :goto_29
    return v0

    :cond_2a
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_28

    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_28

    const/4 v0, 0x1

    goto :goto_29
.end method

.method public final equals(Ljava/lang/Object;)Z
    .registers 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 61
    instance-of v0, p1, Lcom/a/b/b/cl;

    if-eqz v0, :cond_f

    .line 62
    check-cast p1, Lcom/a/b/b/cl;

    .line 63
    iget-object v0, p0, Lcom/a/b/b/cl;->a:Lcom/a/b/b/au;

    iget-object v1, p1, Lcom/a/b/b/cl;->a:Lcom/a/b/b/au;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 66
    :goto_e
    return v0

    :cond_f
    const/4 v0, 0x0

    goto :goto_e
.end method

.method public final hashCode()I
    .registers 3

    .prologue
    .line 71
    iget-object v0, p0, Lcom/a/b/b/cl;->a:Lcom/a/b/b/au;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    const v1, 0x46a3eb07

    xor-int/2addr v0, v1

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .registers 4

    .prologue
    .line 76
    iget-object v0, p0, Lcom/a/b/b/cl;->a:Lcom/a/b/b/au;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0xb

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".pairwise()"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
