.class public abstract Lcom/a/b/b/m;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/b/co;


# annotations
.annotation build Lcom/a/b/a/a;
.end annotation

.annotation build Lcom/a/b/a/b;
    b = true
.end annotation


# static fields
.field public static final a:Lcom/a/b/b/m;

.field public static final b:Lcom/a/b/b/m;

.field public static final c:Lcom/a/b/b/m;

.field public static final d:Lcom/a/b/b/m;

.field public static final e:Lcom/a/b/b/m;

.field public static final f:Lcom/a/b/b/m;

.field public static final g:Lcom/a/b/b/m;

.field public static final h:Lcom/a/b/b/m;

.field public static final i:Lcom/a/b/b/m;

.field public static final j:Lcom/a/b/b/m;

.field public static final k:Lcom/a/b/b/m;

.field public static final l:Lcom/a/b/b/m;

.field public static final m:Lcom/a/b/b/m;

.field static final o:Ljava/lang/String; = "\u2002\u3000\r\u0085\u200a\u2005\u2000\u3000\u2029\u000b\u3000\u2008\u2003\u205f\u3000\u1680\t \u2006\u2001\u202f\u00a0\u000c\u2009\u3000\u2004\u3000\u3000\u2028\n\u2007\u3000"

.field static final p:I = 0x6449bf0a

.field static final q:I

.field public static final r:Lcom/a/b/b/m;

.field private static final s:Ljava/lang/String; = "0\u0660\u06f0\u07c0\u0966\u09e6\u0a66\u0ae6\u0b66\u0be6\u0c66\u0ce6\u0d66\u0e50\u0ed0\u0f20\u1040\u1090\u17e0\u1810\u1946\u19d0\u1b50\u1bb0\u1c40\u1c50\ua620\ua8d0\ua900\uaa50\uff10"

.field private static final t:Ljava/lang/String;

.field private static final u:I = 0x10000


# instance fields
.field final n:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 7

    .prologue
    const/16 v6, 0x7f

    const/4 v1, 0x0

    const/16 v5, 0x1f

    .line 67
    new-instance v0, Lcom/a/b/b/n;

    invoke-direct {v0}, Lcom/a/b/b/n;-><init>()V

    sput-object v0, Lcom/a/b/b/m;->a:Lcom/a/b/b/m;

    .line 100
    const-string v0, "CharMatcher.ASCII"

    invoke-static {v1, v6, v0}, Lcom/a/b/b/m;->a(CCLjava/lang/String;)Lcom/a/b/b/m;

    move-result-object v0

    sput-object v0, Lcom/a/b/b/m;->b:Lcom/a/b/b/m;

    .line 138
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    move v0, v1

    .line 139
    :goto_1a
    if-ge v0, v5, :cond_2b

    .line 140
    const-string v3, "0\u0660\u06f0\u07c0\u0966\u09e6\u0a66\u0ae6\u0b66\u0be6\u0c66\u0ce6\u0d66\u0e50\u0ed0\u0f20\u1040\u1090\u17e0\u1810\u1946\u19d0\u1b50\u1bb0\u1c40\u1c50\ua620\ua8d0\ua900\uaa50\uff10"

    invoke-virtual {v3, v0}, Ljava/lang/String;->charAt(I)C

    move-result v3

    add-int/lit8 v3, v3, 0x9

    int-to-char v3, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 139
    add-int/lit8 v0, v0, 0x1

    goto :goto_1a

    .line 142
    :cond_2b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/a/b/b/m;->t:Ljava/lang/String;

    .line 150
    new-instance v0, Lcom/a/b/b/ai;

    const-string v2, "CharMatcher.DIGIT"

    const-string v3, "0\u0660\u06f0\u07c0\u0966\u09e6\u0a66\u0ae6\u0b66\u0be6\u0c66\u0ce6\u0d66\u0e50\u0ed0\u0f20\u1040\u1090\u17e0\u1810\u1946\u19d0\u1b50\u1bb0\u1c40\u1c50\ua620\ua8d0\ua900\uaa50\uff10"

    invoke-virtual {v3}, Ljava/lang/String;->toCharArray()[C

    move-result-object v3

    sget-object v4, Lcom/a/b/b/m;->t:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    invoke-direct {v0, v2, v3, v4}, Lcom/a/b/b/ai;-><init>(Ljava/lang/String;[C[C)V

    sput-object v0, Lcom/a/b/b/m;->c:Lcom/a/b/b/m;

    .line 158
    new-instance v0, Lcom/a/b/b/u;

    const-string v2, "CharMatcher.JAVA_DIGIT"

    invoke-direct {v0, v2}, Lcom/a/b/b/u;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/a/b/b/m;->d:Lcom/a/b/b/m;

    .line 169
    new-instance v0, Lcom/a/b/b/v;

    const-string v2, "CharMatcher.JAVA_LETTER"

    invoke-direct {v0, v2}, Lcom/a/b/b/v;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/a/b/b/m;->e:Lcom/a/b/b/m;

    .line 179
    new-instance v0, Lcom/a/b/b/w;

    const-string v2, "CharMatcher.JAVA_LETTER_OR_DIGIT"

    invoke-direct {v0, v2}, Lcom/a/b/b/w;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/a/b/b/m;->f:Lcom/a/b/b/m;

    .line 190
    new-instance v0, Lcom/a/b/b/x;

    const-string v2, "CharMatcher.JAVA_UPPER_CASE"

    invoke-direct {v0, v2}, Lcom/a/b/b/x;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/a/b/b/m;->g:Lcom/a/b/b/m;

    .line 201
    new-instance v0, Lcom/a/b/b/y;

    const-string v2, "CharMatcher.JAVA_LOWER_CASE"

    invoke-direct {v0, v2}, Lcom/a/b/b/y;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/a/b/b/m;->h:Lcom/a/b/b/m;

    .line 212
    invoke-static {v1, v5}, Lcom/a/b/b/m;->a(CC)Lcom/a/b/b/m;

    move-result-object v0

    const/16 v1, 0x9f

    invoke-static {v6, v1}, Lcom/a/b/b/m;->a(CC)Lcom/a/b/b/m;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/a/b/b/m;->b(Lcom/a/b/b/m;)Lcom/a/b/b/m;

    move-result-object v0

    const-string v1, "CharMatcher.JAVA_ISO_CONTROL"

    invoke-virtual {v0, v1}, Lcom/a/b/b/m;->a(Ljava/lang/String;)Lcom/a/b/b/m;

    move-result-object v0

    sput-object v0, Lcom/a/b/b/m;->i:Lcom/a/b/b/m;

    .line 222
    new-instance v0, Lcom/a/b/b/ai;

    const-string v1, "CharMatcher.INVISIBLE"

    const-string v2, "\u0000\u007f\u00ad\u0600\u061c\u06dd\u070f\u1680\u180e\u2000\u2028\u205f\u2066\u2067\u2068\u2069\u206a\u3000\ud800\ufeff\ufff9\ufffa"

    invoke-virtual {v2}, Ljava/lang/String;->toCharArray()[C

    move-result-object v2

    const-string v3, " \u00a0\u00ad\u0604\u061c\u06dd\u070f\u1680\u180e\u200f\u202f\u2064\u2066\u2067\u2068\u2069\u206f\u3000\uf8ff\ufeff\ufff9\ufffb"

    invoke-virtual {v3}, Ljava/lang/String;->toCharArray()[C

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/a/b/b/ai;-><init>(Ljava/lang/String;[C[C)V

    sput-object v0, Lcom/a/b/b/m;->j:Lcom/a/b/b/m;

    .line 247
    new-instance v0, Lcom/a/b/b/ai;

    const-string v1, "CharMatcher.SINGLE_WIDTH"

    const-string v2, "\u0000\u05be\u05d0\u05f3\u0600\u0750\u0e00\u1e00\u2100\ufb50\ufe70\uff61"

    invoke-virtual {v2}, Ljava/lang/String;->toCharArray()[C

    move-result-object v2

    const-string v3, "\u04f9\u05be\u05ea\u05f4\u06ff\u077f\u0e7f\u20af\u213a\ufdff\ufeff\uffdc"

    invoke-virtual {v3}, Ljava/lang/String;->toCharArray()[C

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/a/b/b/ai;-><init>(Ljava/lang/String;[C[C)V

    sput-object v0, Lcom/a/b/b/m;->k:Lcom/a/b/b/m;

    .line 252
    new-instance v0, Lcom/a/b/b/z;

    const-string v1, "CharMatcher.ANY"

    invoke-direct {v0, v1}, Lcom/a/b/b/z;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/a/b/b/m;->l:Lcom/a/b/b/m;

    .line 328
    new-instance v0, Lcom/a/b/b/aa;

    const-string v1, "CharMatcher.NONE"

    invoke-direct {v0, v1}, Lcom/a/b/b/aa;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/a/b/b/m;->m:Lcom/a/b/b/m;

    .line 1358
    invoke-static {v5}, Ljava/lang/Integer;->numberOfLeadingZeros(I)I

    move-result v0

    sput v0, Lcom/a/b/b/m;->q:I

    .line 1371
    new-instance v0, Lcom/a/b/b/t;

    const-string v1, "WHITESPACE"

    invoke-direct {v0, v1}, Lcom/a/b/b/t;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/a/b/b/m;->r:Lcom/a/b/b/m;

    return-void
.end method

.method public constructor <init>()V
    .registers 2

    .prologue
    .line 608
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 609
    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/b/m;->n:Ljava/lang/String;

    .line 610
    return-void
.end method

.method constructor <init>(Ljava/lang/String;)V
    .registers 2

    .prologue
    .line 600
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 601
    iput-object p1, p0, Lcom/a/b/b/m;->n:Ljava/lang/String;

    .line 602
    return-void
.end method

.method public static a(C)Lcom/a/b/b/m;
    .registers 4

    .prologue
    .line 415
    invoke-static {p0}, Lcom/a/b/b/m;->d(C)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x12

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "CharMatcher.is(\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\')"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 416
    new-instance v1, Lcom/a/b/b/ab;

    invoke-direct {v1, v0, p0}, Lcom/a/b/b/ab;-><init>(Ljava/lang/String;C)V

    return-object v1
.end method

.method public static a(CC)Lcom/a/b/b/m;
    .registers 7

    .prologue
    .line 550
    if-lt p1, p0, :cond_51

    const/4 v0, 0x1

    :goto_3
    invoke-static {v0}, Lcom/a/b/b/cn;->a(Z)V

    .line 551
    invoke-static {p0}, Lcom/a/b/b/m;->d(C)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1}, Lcom/a/b/b/m;->d(C)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1b

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "CharMatcher.inRange(\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\', \'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\')"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 554
    invoke-static {p0, p1, v0}, Lcom/a/b/b/m;->a(CCLjava/lang/String;)Lcom/a/b/b/m;

    move-result-object v0

    return-object v0

    .line 550
    :cond_51
    const/4 v0, 0x0

    goto :goto_3
.end method

.method private static a(CCLjava/lang/String;)Lcom/a/b/b/m;
    .registers 4

    .prologue
    .line 559
    new-instance v0, Lcom/a/b/b/r;

    invoke-direct {v0, p2, p0, p1}, Lcom/a/b/b/r;-><init>(Ljava/lang/String;CC)V

    return-object v0
.end method

.method static a(ILjava/util/BitSet;Ljava/lang/String;)Lcom/a/b/b/m;
    .registers 15
    .annotation build Lcom/a/b/a/c;
        a = "java.util.BitSet"
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v5, 0x0

    .line 861
    packed-switch p0, :pswitch_data_8e

    .line 871
    invoke-virtual {p1}, Ljava/util/BitSet;->length()I

    move-result v0

    .line 2879
    const/16 v2, 0x3ff

    if-gt p0, v2, :cond_66

    mul-int/lit8 v2, p0, 0x4

    mul-int/lit8 v2, v2, 0x10

    if-le v0, v2, :cond_66

    move v0, v1

    .line 871
    :goto_14
    if-eqz v0, :cond_87

    .line 3092
    const-wide/16 v2, 0x0

    .line 3093
    invoke-virtual {p1}, Ljava/util/BitSet;->cardinality()I

    move-result v6

    .line 3094
    invoke-virtual {p1, v5}, Ljava/util/BitSet;->get(I)Z

    move-result v4

    .line 4078
    if-ne v6, v1, :cond_68

    .line 4079
    const/4 v0, 0x2

    .line 3096
    :cond_23
    new-array v1, v0, [C

    .line 3097
    array-length v0, v1

    add-int/lit8 v6, v0, -0x1

    .line 3098
    invoke-virtual {p1, v5}, Ljava/util/BitSet;->nextSetBit(I)I

    move-result v0

    move v5, v0

    :goto_2d
    const/4 v0, -0x1

    if-eq v5, v0, :cond_80

    .line 3100
    const-wide/16 v8, 0x1

    shl-long/2addr v8, v5

    or-long/2addr v2, v8

    .line 3101
    invoke-static {v5}, Lcom/a/b/b/dh;->a(I)I

    move-result v0

    and-int/2addr v0, v6

    .line 3104
    :goto_39
    aget-char v7, v1, v0

    if-nez v7, :cond_7c

    .line 3105
    int-to-char v7, v5

    aput-char v7, v1, v0

    .line 3098
    add-int/lit8 v0, v5, 0x1

    invoke-virtual {p1, v0}, Ljava/util/BitSet;->nextSetBit(I)I

    move-result v0

    move v5, v0

    goto :goto_2d

    .line 863
    :pswitch_48
    sget-object v0, Lcom/a/b/b/m;->m:Lcom/a/b/b/m;

    .line 871
    :goto_4a
    return-object v0

    .line 865
    :pswitch_4b
    invoke-virtual {p1, v5}, Ljava/util/BitSet;->nextSetBit(I)I

    move-result v0

    int-to-char v0, v0

    invoke-static {v0}, Lcom/a/b/b/m;->a(C)Lcom/a/b/b/m;

    move-result-object v0

    goto :goto_4a

    .line 867
    :pswitch_55
    invoke-virtual {p1, v5}, Ljava/util/BitSet;->nextSetBit(I)I

    move-result v0

    int-to-char v0, v0

    .line 868
    add-int/lit8 v1, v0, 0x1

    invoke-virtual {p1, v1}, Ljava/util/BitSet;->nextSetBit(I)I

    move-result v1

    int-to-char v1, v1

    .line 869
    invoke-static {v0, v1}, Lcom/a/b/b/m;->b(CC)Lcom/a/b/b/m;

    move-result-object v0

    goto :goto_4a

    :cond_66
    move v0, v5

    .line 2879
    goto :goto_14

    .line 4083
    :cond_68
    add-int/lit8 v0, v6, -0x1

    invoke-static {v0}, Ljava/lang/Integer;->highestOneBit(I)I

    move-result v0

    shl-int/lit8 v0, v0, 0x1

    .line 4084
    :goto_70
    int-to-double v8, v0

    const-wide/high16 v10, 0x3fe0000000000000L    # 0.5

    mul-double/2addr v8, v10

    int-to-double v10, v6

    cmpg-double v1, v8, v10

    if-gez v1, :cond_23

    .line 4085
    shl-int/lit8 v0, v0, 0x1

    goto :goto_70

    .line 3109
    :cond_7c
    add-int/lit8 v0, v0, 0x1

    and-int/2addr v0, v6

    goto :goto_39

    .line 3112
    :cond_80
    new-instance v0, Lcom/a/b/b/dh;

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/a/b/b/dh;-><init>([CJZLjava/lang/String;)V

    goto :goto_4a

    .line 871
    :cond_87
    new-instance v0, Lcom/a/b/b/ad;

    invoke-direct {v0, p1, p2, v5}, Lcom/a/b/b/ad;-><init>(Ljava/util/BitSet;Ljava/lang/String;B)V

    goto :goto_4a

    .line 861
    nop

    :pswitch_data_8e
    .packed-switch 0x0
        :pswitch_48
        :pswitch_4b
        :pswitch_55
    .end packed-switch
.end method

.method private static a(Lcom/a/b/b/co;)Lcom/a/b/b/m;
    .registers 4

    .prologue
    .line 576
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 577
    instance-of v0, p0, Lcom/a/b/b/m;

    if-eqz v0, :cond_a

    .line 578
    check-cast p0, Lcom/a/b/b/m;

    .line 581
    :goto_9
    return-object p0

    .line 580
    :cond_a
    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x1a

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "CharMatcher.forPredicate("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 581
    new-instance v0, Lcom/a/b/b/s;

    invoke-direct {v0, v1, p0}, Lcom/a/b/b/s;-><init>(Ljava/lang/String;Lcom/a/b/b/co;)V

    move-object p0, v0

    goto :goto_9
.end method

.method public static a(Ljava/lang/CharSequence;)Lcom/a/b/b/m;
    .registers 6

    .prologue
    const/4 v0, 0x0

    .line 483
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v1

    packed-switch v1, :pswitch_data_52

    .line 494
    invoke-interface {p0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v1

    .line 495
    invoke-static {v1}, Ljava/util/Arrays;->sort([C)V

    .line 496
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "CharMatcher.anyOf(\""

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 497
    array-length v3, v1

    :goto_1b
    if-ge v0, v3, :cond_43

    aget-char v4, v1, v0

    .line 498
    invoke-static {v4}, Lcom/a/b/b/m;->d(C)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 497
    add-int/lit8 v0, v0, 0x1

    goto :goto_1b

    .line 485
    :pswitch_29
    sget-object v0, Lcom/a/b/b/m;->m:Lcom/a/b/b/m;

    .line 501
    :goto_2b
    return-object v0

    .line 487
    :pswitch_2c
    invoke-interface {p0, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    invoke-static {v0}, Lcom/a/b/b/m;->a(C)Lcom/a/b/b/m;

    move-result-object v0

    goto :goto_2b

    .line 489
    :pswitch_35
    invoke-interface {p0, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    const/4 v1, 0x1

    invoke-interface {p0, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v1

    invoke-static {v0, v1}, Lcom/a/b/b/m;->b(CC)Lcom/a/b/b/m;

    move-result-object v0

    goto :goto_2b

    .line 500
    :cond_43
    const-string v0, "\")"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 501
    new-instance v0, Lcom/a/b/b/p;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2, v1}, Lcom/a/b/b/p;-><init>(Ljava/lang/String;[C)V

    goto :goto_2b

    .line 483
    :pswitch_data_52
    .packed-switch 0x0
        :pswitch_29
        :pswitch_2c
        :pswitch_35
    .end packed-switch
.end method

.method private a(Ljava/lang/CharSequence;IICLjava/lang/StringBuilder;Z)Ljava/lang/String;
    .registers 10

    .prologue
    .line 1318
    move v0, p6

    :goto_1
    if-ge p2, p3, :cond_1b

    .line 1319
    invoke-interface {p1, p2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v1

    .line 1320
    invoke-virtual {p0, v1}, Lcom/a/b/b/m;->c(C)Z

    move-result v2

    if-eqz v2, :cond_16

    .line 1321
    if-nez v0, :cond_13

    .line 1322
    invoke-virtual {p5, p4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1323
    const/4 v0, 0x1

    .line 1318
    :cond_13
    :goto_13
    add-int/lit8 p2, p2, 0x1

    goto :goto_1

    .line 1326
    :cond_16
    invoke-virtual {p5, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1327
    const/4 v0, 0x0

    goto :goto_13

    .line 1330
    :cond_1b
    invoke-virtual {p5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static a(II)Z
    .registers 3
    .annotation build Lcom/a/b/a/c;
        a = "SmallCharMatcher"
    .end annotation

    .prologue
    .line 879
    const/16 v0, 0x3ff

    if-gt p0, v0, :cond_c

    mul-int/lit8 v0, p0, 0x4

    mul-int/lit8 v0, v0, 0x10

    if-le p1, v0, :cond_c

    const/4 v0, 0x1

    :goto_b
    return v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method

.method public static b(C)Lcom/a/b/b/m;
    .registers 4

    .prologue
    .line 451
    invoke-static {p0}, Lcom/a/b/b/m;->d(C)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x15

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "CharMatcher.isNot(\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\')"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 452
    new-instance v1, Lcom/a/b/b/o;

    invoke-direct {v1, v0, p0}, Lcom/a/b/b/o;-><init>(Ljava/lang/String;C)V

    return-object v1
.end method

.method private static b(CC)Lcom/a/b/b/m;
    .registers 7

    .prologue
    .line 519
    invoke-static {p0}, Lcom/a/b/b/m;->d(C)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1}, Lcom/a/b/b/m;->d(C)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x15

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "CharMatcher.anyOf(\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 521
    new-instance v1, Lcom/a/b/b/q;

    invoke-direct {v1, v0, p0, p1}, Lcom/a/b/b/q;-><init>(Ljava/lang/String;CC)V

    return-object v1
.end method

.method public static b(Ljava/lang/CharSequence;)Lcom/a/b/b/m;
    .registers 2

    .prologue
    .line 539
    invoke-static {p0}, Lcom/a/b/b/m;->a(Ljava/lang/CharSequence;)Lcom/a/b/b/m;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/b/m;->a()Lcom/a/b/b/m;

    move-result-object v0

    return-object v0
.end method

.method private c()Lcom/a/b/b/m;
    .registers 7
    .annotation build Lcom/a/b/a/c;
        a = "java.util.BitSet"
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/high16 v3, 0x10000

    .line 792
    new-instance v2, Ljava/util/BitSet;

    invoke-direct {v2}, Ljava/util/BitSet;-><init>()V

    .line 793
    invoke-virtual {p0, v2}, Lcom/a/b/b/m;->a(Ljava/util/BitSet;)V

    .line 794
    invoke-virtual {v2}, Ljava/util/BitSet;->cardinality()I

    move-result v0

    .line 795
    mul-int/lit8 v1, v0, 0x2

    if-gt v1, v3, :cond_1a

    .line 796
    iget-object v1, p0, Lcom/a/b/b/m;->n:Ljava/lang/String;

    invoke-static {v0, v2, v1}, Lcom/a/b/b/m;->a(ILjava/util/BitSet;Ljava/lang/String;)Lcom/a/b/b/m;

    move-result-object v0

    .line 805
    :goto_19
    return-object v0

    .line 799
    :cond_1a
    invoke-virtual {v2, v5, v3}, Ljava/util/BitSet;->flip(II)V

    .line 800
    sub-int/2addr v3, v0

    .line 801
    const-string v0, ".negate()"

    .line 802
    iget-object v1, p0, Lcom/a/b/b/m;->n:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_49

    iget-object v1, p0, Lcom/a/b/b/m;->n:Ljava/lang/String;

    iget-object v4, p0, Lcom/a/b/b/m;->n:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    sub-int v0, v4, v0

    invoke-virtual {v1, v5, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 805
    :goto_3a
    new-instance v1, Lcom/a/b/b/af;

    invoke-virtual {p0}, Lcom/a/b/b/m;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v2, v0}, Lcom/a/b/b/m;->a(ILjava/util/BitSet;Ljava/lang/String;)Lcom/a/b/b/m;

    move-result-object v0

    invoke-direct {v1, v4, v0}, Lcom/a/b/b/af;-><init>(Ljava/lang/String;Lcom/a/b/b/m;)V

    move-object v0, v1

    goto :goto_19

    .line 802
    :cond_49
    iget-object v1, p0, Lcom/a/b/b/m;->n:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_5e

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_3a

    :cond_5e
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_3a
.end method

.method private c(Ljava/lang/CharSequence;C)Ljava/lang/String;
    .registers 10
    .annotation runtime Ljavax/annotation/CheckReturnValue;
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 1300
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    move v2, v6

    .line 1304
    :goto_6
    if-ge v2, v1, :cond_15

    invoke-interface {p1, v2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    invoke-virtual {p0, v0}, Lcom/a/b/b/m;->c(C)Z

    move-result v0

    if-eqz v0, :cond_15

    add-int/lit8 v2, v2, 0x1

    goto :goto_6

    .line 1305
    :cond_15
    add-int/lit8 v0, v1, -0x1

    :goto_17
    if-le v0, v2, :cond_26

    invoke-interface {p1, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v3

    invoke-virtual {p0, v3}, Lcom/a/b/b/m;->c(C)Z

    move-result v3

    if-eqz v3, :cond_26

    add-int/lit8 v0, v0, -0x1

    goto :goto_17

    .line 1307
    :cond_26
    if-nez v2, :cond_31

    add-int/lit8 v1, v1, -0x1

    if-ne v0, v1, :cond_31

    invoke-virtual {p0, p1, p2}, Lcom/a/b/b/m;->b(Ljava/lang/CharSequence;C)Ljava/lang/String;

    move-result-object v0

    :goto_30
    return-object v0

    :cond_31
    add-int/lit8 v3, v0, 0x1

    new-instance v5, Ljava/lang/StringBuilder;

    add-int/lit8 v0, v0, 0x1

    sub-int/2addr v0, v2

    invoke-direct {v5, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    move-object v0, p0

    move-object v1, p1

    move v4, p2

    invoke-direct/range {v0 .. v6}, Lcom/a/b/b/m;->a(Ljava/lang/CharSequence;IICLjava/lang/StringBuilder;Z)Ljava/lang/String;

    move-result-object v0

    goto :goto_30
.end method

.method private static d(C)Ljava/lang/String;
    .registers 6

    .prologue
    .line 229
    const-string v1, "0123456789ABCDEF"

    .line 230
    const/4 v0, 0x6

    new-array v2, v0, [C

    fill-array-data v2, :array_22

    .line 231
    const/4 v0, 0x0

    :goto_9
    const/4 v3, 0x4

    if-ge v0, v3, :cond_1c

    .line 232
    rsub-int/lit8 v3, v0, 0x5

    and-int/lit8 v4, p0, 0xf

    invoke-virtual {v1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v4

    aput-char v4, v2, v3

    .line 233
    shr-int/lit8 v3, p0, 0x4

    int-to-char p0, v3

    .line 231
    add-int/lit8 v0, v0, 0x1

    goto :goto_9

    .line 235
    :cond_1c
    invoke-static {v2}, Ljava/lang/String;->copyValueOf([C)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 230
    nop

    :array_22
    .array-data 2
        0x5cs
        0x75s
        0x0s
        0x0s
        0x0s
        0x0s
    .end array-data
.end method

.method private l(Ljava/lang/CharSequence;)Z
    .registers 3

    .prologue
    .line 933
    invoke-virtual {p0, p1}, Lcom/a/b/b/m;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method private m(Ljava/lang/CharSequence;)Ljava/lang/String;
    .registers 3
    .annotation runtime Ljavax/annotation/CheckReturnValue;
    .end annotation

    .prologue
    .line 1095
    invoke-virtual {p0}, Lcom/a/b/b/m;->a()Lcom/a/b/b/m;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/a/b/b/m;->h(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/CharSequence;I)I
    .registers 6

    .prologue
    .line 1006
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    .line 1007
    invoke-static {p2, v1}, Lcom/a/b/b/cn;->b(II)I

    move v0, p2

    .line 1008
    :goto_8
    if-ge v0, v1, :cond_18

    .line 1009
    invoke-interface {p1, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    invoke-virtual {p0, v2}, Lcom/a/b/b/m;->c(C)Z

    move-result v2

    if-eqz v2, :cond_15

    .line 1013
    :goto_14
    return v0

    .line 1008
    :cond_15
    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    .line 1013
    :cond_18
    const/4 v0, -0x1

    goto :goto_14
.end method

.method public a()Lcom/a/b/b/m;
    .registers 2

    .prologue
    .line 623
    new-instance v0, Lcom/a/b/b/ag;

    invoke-direct {v0, p0}, Lcom/a/b/b/ag;-><init>(Lcom/a/b/b/m;)V

    return-object v0
.end method

.method public a(Lcom/a/b/b/m;)Lcom/a/b/b/m;
    .registers 4

    .prologue
    .line 677
    new-instance v1, Lcom/a/b/b/ac;

    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/b/m;

    invoke-direct {v1, p0, v0}, Lcom/a/b/b/ac;-><init>(Lcom/a/b/b/m;Lcom/a/b/b/m;)V

    return-object v1
.end method

.method a(Ljava/lang/String;)Lcom/a/b/b/m;
    .registers 3

    .prologue
    .line 775
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public a(Ljava/lang/CharSequence;C)Ljava/lang/String;
    .registers 6
    .annotation runtime Ljavax/annotation/CheckReturnValue;
    .end annotation

    .prologue
    .line 1117
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1118
    invoke-virtual {p0, v0}, Lcom/a/b/b/m;->e(Ljava/lang/CharSequence;)I

    move-result v1

    .line 1119
    const/4 v2, -0x1

    if-ne v1, v2, :cond_c

    .line 1129
    :goto_b
    return-object v0

    .line 1122
    :cond_c
    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v2

    .line 1123
    aput-char p2, v2, v1

    .line 1124
    add-int/lit8 v0, v1, 0x1

    :goto_14
    array-length v1, v2

    if-ge v0, v1, :cond_24

    .line 1125
    aget-char v1, v2, v0

    invoke-virtual {p0, v1}, Lcom/a/b/b/m;->c(C)Z

    move-result v1

    if-eqz v1, :cond_21

    .line 1126
    aput-char p2, v2, v0

    .line 1124
    :cond_21
    add-int/lit8 v0, v0, 0x1

    goto :goto_14

    .line 1129
    :cond_24
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>([C)V

    goto :goto_b
.end method

.method public a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
    .registers 10
    .annotation runtime Ljavax/annotation/CheckReturnValue;
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/4 v6, -0x1

    .line 1150
    invoke-interface {p2}, Ljava/lang/CharSequence;->length()I

    move-result v1

    .line 1151
    if-nez v1, :cond_d

    .line 1152
    invoke-virtual {p0, p1}, Lcom/a/b/b/m;->h(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 1176
    :goto_c
    return-object v0

    .line 1154
    :cond_d
    const/4 v2, 0x1

    if-ne v1, v2, :cond_19

    .line 1155
    invoke-interface {p2, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/a/b/b/m;->a(Ljava/lang/CharSequence;C)Ljava/lang/String;

    move-result-object v0

    goto :goto_c

    .line 1158
    :cond_19
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1159
    invoke-virtual {p0, v2}, Lcom/a/b/b/m;->e(Ljava/lang/CharSequence;)I

    move-result v1

    .line 1160
    if-ne v1, v6, :cond_25

    move-object v0, v2

    .line 1161
    goto :goto_c

    .line 1164
    :cond_25
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    .line 1165
    new-instance v4, Ljava/lang/StringBuilder;

    mul-int/lit8 v5, v3, 0x3

    div-int/lit8 v5, v5, 0x2

    add-int/lit8 v5, v5, 0x10

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 1169
    :cond_34
    invoke-virtual {v4, v2, v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;II)Ljava/lang/StringBuilder;

    .line 1170
    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 1171
    add-int/lit8 v0, v1, 0x1

    .line 1172
    invoke-virtual {p0, v2, v0}, Lcom/a/b/b/m;->a(Ljava/lang/CharSequence;I)I

    move-result v1

    .line 1173
    if-ne v1, v6, :cond_34

    .line 1175
    invoke-virtual {v4, v2, v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;II)Ljava/lang/StringBuilder;

    .line 1176
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_c
.end method

.method a(Ljava/util/BitSet;)V
    .registers 4
    .annotation build Lcom/a/b/a/c;
        a = "java.util.BitSet"
    .end annotation

    .prologue
    .line 912
    const v0, 0xffff

    :goto_3
    if-ltz v0, :cond_12

    .line 913
    int-to-char v1, v0

    invoke-virtual {p0, v1}, Lcom/a/b/b/m;->c(C)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 914
    invoke-virtual {p1, v0}, Ljava/util/BitSet;->set(I)V

    .line 912
    :cond_f
    add-int/lit8 v0, v0, -0x1

    goto :goto_3

    .line 917
    :cond_12
    return-void
.end method

.method public a(Ljava/lang/Character;)Z
    .registers 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1340
    invoke-virtual {p1}, Ljava/lang/Character;->charValue()C

    move-result v0

    invoke-virtual {p0, v0}, Lcom/a/b/b/m;->c(C)Z

    move-result v0

    return v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 55
    check-cast p1, Ljava/lang/Character;

    invoke-virtual {p0, p1}, Lcom/a/b/b/m;->a(Ljava/lang/Character;)Z

    move-result v0

    return v0
.end method

.method public b()Lcom/a/b/b/m;
    .registers 7

    .prologue
    const/4 v5, 0x0

    const/high16 v3, 0x10000

    .line 765
    .line 2792
    new-instance v2, Ljava/util/BitSet;

    invoke-direct {v2}, Ljava/util/BitSet;-><init>()V

    .line 2793
    invoke-virtual {p0, v2}, Lcom/a/b/b/m;->a(Ljava/util/BitSet;)V

    .line 2794
    invoke-virtual {v2}, Ljava/util/BitSet;->cardinality()I

    move-result v0

    .line 2795
    mul-int/lit8 v1, v0, 0x2

    if-gt v1, v3, :cond_1a

    .line 2796
    iget-object v1, p0, Lcom/a/b/b/m;->n:Ljava/lang/String;

    invoke-static {v0, v2, v1}, Lcom/a/b/b/m;->a(ILjava/util/BitSet;Ljava/lang/String;)Lcom/a/b/b/m;

    move-result-object v0

    :goto_19
    return-object v0

    .line 2799
    :cond_1a
    invoke-virtual {v2, v5, v3}, Ljava/util/BitSet;->flip(II)V

    .line 2800
    sub-int/2addr v3, v0

    .line 2801
    const-string v0, ".negate()"

    .line 2802
    iget-object v1, p0, Lcom/a/b/b/m;->n:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_49

    iget-object v1, p0, Lcom/a/b/b/m;->n:Ljava/lang/String;

    iget-object v4, p0, Lcom/a/b/b/m;->n:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    sub-int v0, v4, v0

    invoke-virtual {v1, v5, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 2805
    :goto_3a
    new-instance v1, Lcom/a/b/b/af;

    invoke-virtual {p0}, Lcom/a/b/b/m;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v2, v0}, Lcom/a/b/b/m;->a(ILjava/util/BitSet;Ljava/lang/String;)Lcom/a/b/b/m;

    move-result-object v0

    invoke-direct {v1, v4, v0}, Lcom/a/b/b/af;-><init>(Ljava/lang/String;Lcom/a/b/b/m;)V

    move-object v0, v1

    .line 765
    goto :goto_19

    .line 2802
    :cond_49
    iget-object v1, p0, Lcom/a/b/b/m;->n:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_5e

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_3a

    :cond_5e
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_3a
.end method

.method public b(Lcom/a/b/b/m;)Lcom/a/b/b/m;
    .registers 4

    .prologue
    .line 720
    new-instance v1, Lcom/a/b/b/ah;

    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/b/m;

    invoke-direct {v1, p0, v0}, Lcom/a/b/b/ah;-><init>(Lcom/a/b/b/m;Lcom/a/b/b/m;)V

    return-object v1
.end method

.method public b(Ljava/lang/CharSequence;C)Ljava/lang/String;
    .registers 10
    .annotation runtime Ljavax/annotation/CheckReturnValue;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1272
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v3

    move v0, v1

    .line 1273
    :goto_6
    if-ge v0, v3, :cond_45

    .line 1274
    invoke-interface {p1, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    .line 1275
    invoke-virtual {p0, v2}, Lcom/a/b/b/m;->c(C)Z

    move-result v4

    if-eqz v4, :cond_26

    .line 1276
    if-ne v2, p2, :cond_29

    add-int/lit8 v2, v3, -0x1

    if-eq v0, v2, :cond_24

    add-int/lit8 v2, v0, 0x1

    invoke-interface {p1, v2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    invoke-virtual {p0, v2}, Lcom/a/b/b/m;->c(C)Z

    move-result v2

    if-nez v2, :cond_29

    .line 1279
    :cond_24
    add-int/lit8 v0, v0, 0x1

    .line 1273
    :cond_26
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 1281
    :cond_29
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-interface {p1, v1, v0}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 1284
    add-int/lit8 v2, v0, 0x1

    const/4 v6, 0x1

    move-object v0, p0

    move-object v1, p1

    move v4, p2

    invoke-direct/range {v0 .. v6}, Lcom/a/b/b/m;->a(Ljava/lang/CharSequence;IICLjava/lang/StringBuilder;Z)Ljava/lang/String;

    move-result-object v0

    .line 1289
    :goto_44
    return-object v0

    :cond_45
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_44
.end method

.method public abstract c(C)Z
.end method

.method public c(Ljava/lang/CharSequence;)Z
    .registers 4

    .prologue
    .line 947
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_6
    if-ltz v0, :cond_17

    .line 948
    invoke-interface {p1, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v1

    invoke-virtual {p0, v1}, Lcom/a/b/b/m;->c(C)Z

    move-result v1

    if-nez v1, :cond_14

    .line 949
    const/4 v0, 0x0

    .line 952
    :goto_13
    return v0

    .line 947
    :cond_14
    add-int/lit8 v0, v0, -0x1

    goto :goto_6

    .line 952
    :cond_17
    const/4 v0, 0x1

    goto :goto_13
.end method

.method public d(Ljava/lang/CharSequence;)Z
    .registers 4

    .prologue
    .line 967
    invoke-virtual {p0, p1}, Lcom/a/b/b/m;->e(Ljava/lang/CharSequence;)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_9

    const/4 v0, 0x1

    :goto_8
    return v0

    :cond_9
    const/4 v0, 0x0

    goto :goto_8
.end method

.method public e(Ljava/lang/CharSequence;)I
    .registers 5

    .prologue
    .line 981
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    .line 982
    const/4 v0, 0x0

    :goto_5
    if-ge v0, v1, :cond_15

    .line 983
    invoke-interface {p1, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    invoke-virtual {p0, v2}, Lcom/a/b/b/m;->c(C)Z

    move-result v2

    if-eqz v2, :cond_12

    .line 987
    :goto_11
    return v0

    .line 982
    :cond_12
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 987
    :cond_15
    const/4 v0, -0x1

    goto :goto_11
.end method

.method public f(Ljava/lang/CharSequence;)I
    .registers 4

    .prologue
    .line 1027
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_6
    if-ltz v0, :cond_16

    .line 1028
    invoke-interface {p1, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v1

    invoke-virtual {p0, v1}, Lcom/a/b/b/m;->c(C)Z

    move-result v1

    if-eqz v1, :cond_13

    .line 1032
    :goto_12
    return v0

    .line 1027
    :cond_13
    add-int/lit8 v0, v0, -0x1

    goto :goto_6

    .line 1032
    :cond_16
    const/4 v0, -0x1

    goto :goto_12
.end method

.method public g(Ljava/lang/CharSequence;)I
    .registers 5

    .prologue
    const/4 v0, 0x0

    .line 1039
    move v1, v0

    .line 1040
    :goto_2
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v2

    if-ge v0, v2, :cond_17

    .line 1041
    invoke-interface {p1, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    invoke-virtual {p0, v2}, Lcom/a/b/b/m;->c(C)Z

    move-result v2

    if-eqz v2, :cond_14

    .line 1042
    add-int/lit8 v1, v1, 0x1

    .line 1040
    :cond_14
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1045
    :cond_17
    return v1
.end method

.method public h(Ljava/lang/CharSequence;)Ljava/lang/String;
    .registers 7
    .annotation runtime Ljavax/annotation/CheckReturnValue;
    .end annotation

    .prologue
    .line 1058
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1059
    invoke-virtual {p0, v0}, Lcom/a/b/b/m;->e(Ljava/lang/CharSequence;)I

    move-result v1

    .line 1060
    const/4 v2, -0x1

    if-ne v1, v2, :cond_c

    .line 1082
    :goto_b
    return-object v0

    .line 1064
    :cond_c
    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v3

    .line 1065
    const/4 v0, 0x1

    .line 1069
    :goto_11
    add-int/lit8 v1, v1, 0x1

    .line 1071
    array-length v2, v3

    if-eq v1, v2, :cond_28

    .line 1074
    aget-char v2, v3, v1

    invoke-virtual {p0, v2}, Lcom/a/b/b/m;->c(C)Z

    move-result v2

    if-nez v2, :cond_25

    .line 1077
    sub-int v2, v1, v0

    aget-char v4, v3, v1

    aput-char v4, v3, v2

    goto :goto_11

    .line 1080
    :cond_25
    add-int/lit8 v0, v0, 0x1

    goto :goto_11

    .line 1082
    :cond_28
    new-instance v2, Ljava/lang/String;

    const/4 v4, 0x0

    sub-int v0, v1, v0

    invoke-direct {v2, v3, v4, v0}, Ljava/lang/String;-><init>([CII)V

    move-object v0, v2

    goto :goto_b
.end method

.method public i(Ljava/lang/CharSequence;)Ljava/lang/String;
    .registers 5
    .annotation runtime Ljavax/annotation/CheckReturnValue;
    .end annotation

    .prologue
    .line 1195
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v2

    .line 1199
    const/4 v0, 0x0

    move v1, v0

    :goto_6
    if-ge v1, v2, :cond_16

    .line 1200
    invoke-interface {p1, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    invoke-virtual {p0, v0}, Lcom/a/b/b/m;->c(C)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 1199
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_6

    .line 1204
    :cond_16
    add-int/lit8 v0, v2, -0x1

    :goto_18
    if-le v0, v1, :cond_27

    .line 1205
    invoke-interface {p1, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    invoke-virtual {p0, v2}, Lcom/a/b/b/m;->c(C)Z

    move-result v2

    if-eqz v2, :cond_27

    .line 1204
    add-int/lit8 v0, v0, -0x1

    goto :goto_18

    .line 1210
    :cond_27
    add-int/lit8 v0, v0, 0x1

    invoke-interface {p1, v1, v0}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public j(Ljava/lang/CharSequence;)Ljava/lang/String;
    .registers 5
    .annotation runtime Ljavax/annotation/CheckReturnValue;
    .end annotation

    .prologue
    .line 1223
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    .line 1224
    const/4 v0, 0x0

    :goto_5
    if-ge v0, v1, :cond_1d

    .line 1225
    invoke-interface {p1, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    invoke-virtual {p0, v2}, Lcom/a/b/b/m;->c(C)Z

    move-result v2

    if-nez v2, :cond_1a

    .line 1226
    invoke-interface {p1, v0, v1}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1229
    :goto_19
    return-object v0

    .line 1224
    :cond_1a
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 1229
    :cond_1d
    const-string v0, ""

    goto :goto_19
.end method

.method public k(Ljava/lang/CharSequence;)Ljava/lang/String;
    .registers 4
    .annotation runtime Ljavax/annotation/CheckReturnValue;
    .end annotation

    .prologue
    .line 1242
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    .line 1243
    add-int/lit8 v0, v0, -0x1

    :goto_6
    if-ltz v0, :cond_21

    .line 1244
    invoke-interface {p1, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v1

    invoke-virtual {p0, v1}, Lcom/a/b/b/m;->c(C)Z

    move-result v1

    if-nez v1, :cond_1e

    .line 1245
    const/4 v1, 0x0

    add-int/lit8 v0, v0, 0x1

    invoke-interface {p1, v1, v0}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1248
    :goto_1d
    return-object v0

    .line 1243
    :cond_1e
    add-int/lit8 v0, v0, -0x1

    goto :goto_6

    .line 1248
    :cond_21
    const-string v0, ""

    goto :goto_1d
.end method

.method public toString()Ljava/lang/String;
    .registers 2

    .prologue
    .line 1349
    iget-object v0, p0, Lcom/a/b/b/m;->n:Ljava/lang/String;

    return-object v0
.end method
