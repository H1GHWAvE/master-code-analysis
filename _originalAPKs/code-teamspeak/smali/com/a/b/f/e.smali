.class public final Lcom/a/b/f/e;
.super Lcom/a/b/f/h;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/a;
.end annotation


# instance fields
.field private final a:Ljava/util/concurrent/Executor;

.field private final b:Ljava/util/concurrent/ConcurrentLinkedQueue;


# direct methods
.method private constructor <init>(Ljava/lang/String;Ljava/util/concurrent/Executor;)V
    .registers 4

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lcom/a/b/f/h;-><init>(Ljava/lang/String;)V

    .line 38
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, Lcom/a/b/f/e;->b:Ljava/util/concurrent/ConcurrentLinkedQueue;

    .line 52
    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lcom/a/b/f/e;->a:Ljava/util/concurrent/Executor;

    .line 53
    return-void
.end method

.method private constructor <init>(Ljava/util/concurrent/Executor;)V
    .registers 3

    .prologue
    .line 80
    const-string v0, "default"

    invoke-direct {p0, v0}, Lcom/a/b/f/h;-><init>(Ljava/lang/String;)V

    .line 38
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, Lcom/a/b/f/e;->b:Ljava/util/concurrent/ConcurrentLinkedQueue;

    .line 81
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lcom/a/b/f/e;->a:Ljava/util/concurrent/Executor;

    .line 82
    return-void
.end method

.method private constructor <init>(Ljava/util/concurrent/Executor;Lcom/a/b/f/q;)V
    .registers 4

    .prologue
    .line 67
    invoke-direct {p0, p2}, Lcom/a/b/f/h;-><init>(Lcom/a/b/f/q;)V

    .line 38
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, Lcom/a/b/f/e;->b:Ljava/util/concurrent/ConcurrentLinkedQueue;

    .line 68
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lcom/a/b/f/e;->a:Ljava/util/concurrent/Executor;

    .line 69
    return-void
.end method

.method static synthetic a(Lcom/a/b/f/e;Ljava/lang/Object;Lcom/a/b/f/n;)V
    .registers 3

    .prologue
    .line 34
    invoke-super {p0, p1, p2}, Lcom/a/b/f/h;->b(Ljava/lang/Object;Lcom/a/b/f/n;)V

    return-void
.end method


# virtual methods
.method protected final a()V
    .registers 3

    .prologue
    .line 97
    :goto_0
    iget-object v0, p0, Lcom/a/b/f/e;->b:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/f/l;

    .line 98
    if-eqz v0, :cond_12

    .line 102
    iget-object v1, v0, Lcom/a/b/f/l;->a:Ljava/lang/Object;

    iget-object v0, v0, Lcom/a/b/f/l;->b:Lcom/a/b/f/n;

    invoke-virtual {p0, v1, v0}, Lcom/a/b/f/e;->b(Ljava/lang/Object;Lcom/a/b/f/n;)V

    goto :goto_0

    .line 104
    :cond_12
    return-void
.end method

.method final a(Ljava/lang/Object;Lcom/a/b/f/n;)V
    .registers 5

    .prologue
    .line 86
    iget-object v0, p0, Lcom/a/b/f/e;->b:Ljava/util/concurrent/ConcurrentLinkedQueue;

    new-instance v1, Lcom/a/b/f/l;

    invoke-direct {v1, p1, p2}, Lcom/a/b/f/l;-><init>(Ljava/lang/Object;Lcom/a/b/f/n;)V

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->offer(Ljava/lang/Object;)Z

    .line 87
    return-void
.end method

.method final b(Ljava/lang/Object;Lcom/a/b/f/n;)V
    .registers 5

    .prologue
    .line 111
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 112
    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 113
    iget-object v0, p0, Lcom/a/b/f/e;->a:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/a/b/f/f;

    invoke-direct {v1, p0, p1, p2}, Lcom/a/b/f/f;-><init>(Lcom/a/b/f/e;Ljava/lang/Object;Lcom/a/b/f/n;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 120
    return-void
.end method
