.class final enum Lcom/a/b/g/aa;
.super Ljava/lang/Enum;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/g/w;


# static fields
.field public static final enum a:Lcom/a/b/g/aa;

.field private static final synthetic b:[Lcom/a/b/g/aa;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    .line 205
    new-instance v0, Lcom/a/b/g/aa;

    const-string v1, "INSTANCE"

    invoke-direct {v0, v1}, Lcom/a/b/g/aa;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/a/b/g/aa;->a:Lcom/a/b/g/aa;

    .line 204
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/a/b/g/aa;

    const/4 v1, 0x0

    sget-object v2, Lcom/a/b/g/aa;->a:Lcom/a/b/g/aa;

    aput-object v2, v0, v1

    sput-object v0, Lcom/a/b/g/aa;->b:[Lcom/a/b/g/aa;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .registers 3

    .prologue
    .line 204
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method private static a(Ljava/lang/Long;Lcom/a/b/g/bn;)V
    .registers 4

    .prologue
    .line 208
    invoke-virtual {p0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-interface {p1, v0, v1}, Lcom/a/b/g/bn;->b(J)Lcom/a/b/g/bn;

    .line 209
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/a/b/g/aa;
    .registers 2

    .prologue
    .line 204
    const-class v0, Lcom/a/b/g/aa;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/a/b/g/aa;

    return-object v0
.end method

.method public static values()[Lcom/a/b/g/aa;
    .registers 1

    .prologue
    .line 204
    sget-object v0, Lcom/a/b/g/aa;->b:[Lcom/a/b/g/aa;

    invoke-virtual {v0}, [Lcom/a/b/g/aa;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/a/b/g/aa;

    return-object v0
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Lcom/a/b/g/bn;)V
    .registers 5

    .prologue
    .line 204
    check-cast p1, Ljava/lang/Long;

    .line 1208
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-interface {p2, v0, v1}, Lcom/a/b/g/bn;->b(J)Lcom/a/b/g/bn;

    .line 204
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .registers 2

    .prologue
    .line 212
    const-string v0, "Funnels.longFunnel()"

    return-object v0
.end method
