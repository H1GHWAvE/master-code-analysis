.class final Lcom/a/b/g/bp;
.super Lcom/a/b/g/i;
.source "SourceFile"


# static fields
.field private static final a:I = 0x8


# instance fields
.field private final b:I

.field private final c:I

.field private d:J

.field private e:J

.field private f:J

.field private g:J

.field private h:J

.field private i:J


# direct methods
.method constructor <init>(IIJJ)V
    .registers 12

    .prologue
    const-wide/16 v2, 0x0

    .line 119
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/a/b/g/i;-><init>(I)V

    .line 106
    const-wide v0, 0x736f6d6570736575L    # 1.0986868386607877E248

    iput-wide v0, p0, Lcom/a/b/g/bp;->d:J

    .line 107
    const-wide v0, 0x646f72616e646f6dL    # 6.222199573468475E175

    iput-wide v0, p0, Lcom/a/b/g/bp;->e:J

    .line 108
    const-wide v0, 0x6c7967656e657261L    # 3.4208747916531402E214

    iput-wide v0, p0, Lcom/a/b/g/bp;->f:J

    .line 109
    const-wide v0, 0x7465646279746573L    # 4.901176695720602E252

    iput-wide v0, p0, Lcom/a/b/g/bp;->g:J

    .line 112
    iput-wide v2, p0, Lcom/a/b/g/bp;->h:J

    .line 116
    iput-wide v2, p0, Lcom/a/b/g/bp;->i:J

    .line 120
    iput p1, p0, Lcom/a/b/g/bp;->b:I

    .line 121
    iput p2, p0, Lcom/a/b/g/bp;->c:I

    .line 122
    iget-wide v0, p0, Lcom/a/b/g/bp;->d:J

    xor-long/2addr v0, p3

    iput-wide v0, p0, Lcom/a/b/g/bp;->d:J

    .line 123
    iget-wide v0, p0, Lcom/a/b/g/bp;->e:J

    xor-long/2addr v0, p5

    iput-wide v0, p0, Lcom/a/b/g/bp;->e:J

    .line 124
    iget-wide v0, p0, Lcom/a/b/g/bp;->f:J

    xor-long/2addr v0, p3

    iput-wide v0, p0, Lcom/a/b/g/bp;->f:J

    .line 125
    iget-wide v0, p0, Lcom/a/b/g/bp;->g:J

    xor-long/2addr v0, p5

    iput-wide v0, p0, Lcom/a/b/g/bp;->g:J

    .line 126
    return-void
.end method

.method private c(I)V
    .registers 9

    .prologue
    const/16 v6, 0x20

    .line 158
    const/4 v0, 0x0

    :goto_3
    if-ge v0, p1, :cond_78

    .line 159
    iget-wide v2, p0, Lcom/a/b/g/bp;->d:J

    iget-wide v4, p0, Lcom/a/b/g/bp;->e:J

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/a/b/g/bp;->d:J

    .line 160
    iget-wide v2, p0, Lcom/a/b/g/bp;->f:J

    iget-wide v4, p0, Lcom/a/b/g/bp;->g:J

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/a/b/g/bp;->f:J

    .line 161
    iget-wide v2, p0, Lcom/a/b/g/bp;->e:J

    const/16 v1, 0xd

    invoke-static {v2, v3, v1}, Ljava/lang/Long;->rotateLeft(JI)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/a/b/g/bp;->e:J

    .line 162
    iget-wide v2, p0, Lcom/a/b/g/bp;->g:J

    const/16 v1, 0x10

    invoke-static {v2, v3, v1}, Ljava/lang/Long;->rotateLeft(JI)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/a/b/g/bp;->g:J

    .line 163
    iget-wide v2, p0, Lcom/a/b/g/bp;->e:J

    iget-wide v4, p0, Lcom/a/b/g/bp;->d:J

    xor-long/2addr v2, v4

    iput-wide v2, p0, Lcom/a/b/g/bp;->e:J

    .line 164
    iget-wide v2, p0, Lcom/a/b/g/bp;->g:J

    iget-wide v4, p0, Lcom/a/b/g/bp;->f:J

    xor-long/2addr v2, v4

    iput-wide v2, p0, Lcom/a/b/g/bp;->g:J

    .line 165
    iget-wide v2, p0, Lcom/a/b/g/bp;->d:J

    invoke-static {v2, v3, v6}, Ljava/lang/Long;->rotateLeft(JI)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/a/b/g/bp;->d:J

    .line 166
    iget-wide v2, p0, Lcom/a/b/g/bp;->f:J

    iget-wide v4, p0, Lcom/a/b/g/bp;->e:J

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/a/b/g/bp;->f:J

    .line 167
    iget-wide v2, p0, Lcom/a/b/g/bp;->d:J

    iget-wide v4, p0, Lcom/a/b/g/bp;->g:J

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/a/b/g/bp;->d:J

    .line 168
    iget-wide v2, p0, Lcom/a/b/g/bp;->e:J

    const/16 v1, 0x11

    invoke-static {v2, v3, v1}, Ljava/lang/Long;->rotateLeft(JI)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/a/b/g/bp;->e:J

    .line 169
    iget-wide v2, p0, Lcom/a/b/g/bp;->g:J

    const/16 v1, 0x15

    invoke-static {v2, v3, v1}, Ljava/lang/Long;->rotateLeft(JI)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/a/b/g/bp;->g:J

    .line 170
    iget-wide v2, p0, Lcom/a/b/g/bp;->e:J

    iget-wide v4, p0, Lcom/a/b/g/bp;->f:J

    xor-long/2addr v2, v4

    iput-wide v2, p0, Lcom/a/b/g/bp;->e:J

    .line 171
    iget-wide v2, p0, Lcom/a/b/g/bp;->g:J

    iget-wide v4, p0, Lcom/a/b/g/bp;->d:J

    xor-long/2addr v2, v4

    iput-wide v2, p0, Lcom/a/b/g/bp;->g:J

    .line 172
    iget-wide v2, p0, Lcom/a/b/g/bp;->f:J

    invoke-static {v2, v3, v6}, Ljava/lang/Long;->rotateLeft(JI)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/a/b/g/bp;->f:J

    .line 158
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 174
    :cond_78
    return-void
.end method

.method private c(J)V
    .registers 6

    .prologue
    .line 152
    iget-wide v0, p0, Lcom/a/b/g/bp;->g:J

    xor-long/2addr v0, p1

    iput-wide v0, p0, Lcom/a/b/g/bp;->g:J

    .line 153
    iget v0, p0, Lcom/a/b/g/bp;->b:I

    invoke-direct {p0, v0}, Lcom/a/b/g/bp;->c(I)V

    .line 154
    iget-wide v0, p0, Lcom/a/b/g/bp;->d:J

    xor-long/2addr v0, p1

    iput-wide v0, p0, Lcom/a/b/g/bp;->d:J

    .line 155
    return-void
.end method


# virtual methods
.method protected final a(Ljava/nio/ByteBuffer;)V
    .registers 6

    .prologue
    .line 129
    iget-wide v0, p0, Lcom/a/b/g/bp;->h:J

    const-wide/16 v2, 0x8

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/a/b/g/bp;->h:J

    .line 130
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getLong()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/a/b/g/bp;->c(J)V

    .line 131
    return-void
.end method

.method public final b()Lcom/a/b/g/ag;
    .registers 6

    .prologue
    .line 142
    iget-wide v0, p0, Lcom/a/b/g/bp;->i:J

    iget-wide v2, p0, Lcom/a/b/g/bp;->h:J

    const/16 v4, 0x38

    shl-long/2addr v2, v4

    xor-long/2addr v0, v2

    iput-wide v0, p0, Lcom/a/b/g/bp;->i:J

    .line 143
    iget-wide v0, p0, Lcom/a/b/g/bp;->i:J

    invoke-direct {p0, v0, v1}, Lcom/a/b/g/bp;->c(J)V

    .line 146
    iget-wide v0, p0, Lcom/a/b/g/bp;->f:J

    const-wide/16 v2, 0xff

    xor-long/2addr v0, v2

    iput-wide v0, p0, Lcom/a/b/g/bp;->f:J

    .line 147
    iget v0, p0, Lcom/a/b/g/bp;->c:I

    invoke-direct {p0, v0}, Lcom/a/b/g/bp;->c(I)V

    .line 148
    iget-wide v0, p0, Lcom/a/b/g/bp;->d:J

    iget-wide v2, p0, Lcom/a/b/g/bp;->e:J

    xor-long/2addr v0, v2

    iget-wide v2, p0, Lcom/a/b/g/bp;->f:J

    xor-long/2addr v0, v2

    iget-wide v2, p0, Lcom/a/b/g/bp;->g:J

    xor-long/2addr v0, v2

    invoke-static {v0, v1}, Lcom/a/b/g/ag;->a(J)Lcom/a/b/g/ag;

    move-result-object v0

    return-object v0
.end method

.method protected final b(Ljava/nio/ByteBuffer;)V
    .registers 10

    .prologue
    .line 134
    iget-wide v0, p0, Lcom/a/b/g/bp;->h:J

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v2

    int-to-long v2, v2

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/a/b/g/bp;->h:J

    .line 135
    const/4 v0, 0x0

    :goto_b
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->hasRemaining()Z

    move-result v1

    if-eqz v1, :cond_22

    .line 136
    iget-wide v2, p0, Lcom/a/b/g/bp;->i:J

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->get()B

    move-result v1

    int-to-long v4, v1

    const-wide/16 v6, 0xff

    and-long/2addr v4, v6

    shl-long/2addr v4, v0

    xor-long/2addr v2, v4

    iput-wide v2, p0, Lcom/a/b/g/bp;->i:J

    .line 135
    add-int/lit8 v0, v0, 0x8

    goto :goto_b

    .line 138
    :cond_22
    return-void
.end method
