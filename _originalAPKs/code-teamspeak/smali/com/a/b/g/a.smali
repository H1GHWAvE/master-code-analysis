.class abstract Lcom/a/b/g/a;
.super Lcom/a/b/g/d;
.source "SourceFile"


# instance fields
.field private final a:Ljava/nio/ByteBuffer;


# direct methods
.method constructor <init>()V
    .registers 3

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/a/b/g/d;-><init>()V

    .line 38
    const/16 v0, 0x8

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    sget-object v1, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/g/a;->a:Ljava/nio/ByteBuffer;

    return-void
.end method

.method private c(I)Lcom/a/b/g/al;
    .registers 4

    .prologue
    .line 86
    :try_start_0
    iget-object v0, p0, Lcom/a/b/g/a;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1, p1}, Lcom/a/b/g/a;->a([BII)V
    :try_end_a
    .catchall {:try_start_0 .. :try_end_a} :catchall_10

    .line 88
    iget-object v0, p0, Lcom/a/b/g/a;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 90
    return-object p0

    .line 88
    :catchall_10
    move-exception v0

    iget-object v1, p0, Lcom/a/b/g/a;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    throw v0
.end method


# virtual methods
.method public final a(C)Lcom/a/b/g/al;
    .registers 3

    .prologue
    .line 113
    iget-object v0, p0, Lcom/a/b/g/a;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, p1}, Ljava/nio/ByteBuffer;->putChar(C)Ljava/nio/ByteBuffer;

    .line 114
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/a/b/g/a;->c(I)Lcom/a/b/g/al;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)Lcom/a/b/g/al;
    .registers 3

    .prologue
    .line 101
    iget-object v0, p0, Lcom/a/b/g/a;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, p1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 102
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/a/b/g/a;->c(I)Lcom/a/b/g/al;

    move-result-object v0

    return-object v0
.end method

.method public final a(J)Lcom/a/b/g/al;
    .registers 4

    .prologue
    .line 107
    iget-object v0, p0, Lcom/a/b/g/a;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, p1, p2}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    .line 108
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/a/b/g/a;->c(I)Lcom/a/b/g/al;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;Lcom/a/b/g/w;)Lcom/a/b/g/al;
    .registers 3

    .prologue
    .line 119
    invoke-interface {p2, p1, p0}, Lcom/a/b/g/w;->a(Ljava/lang/Object;Lcom/a/b/g/bn;)V

    .line 120
    return-object p0
.end method

.method public final a(S)Lcom/a/b/g/al;
    .registers 3

    .prologue
    .line 95
    iget-object v0, p0, Lcom/a/b/g/a;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, p1}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 96
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/a/b/g/a;->c(I)Lcom/a/b/g/al;

    move-result-object v0

    return-object v0
.end method

.method protected abstract a(B)V
.end method

.method protected a([B)V
    .registers 4

    .prologue
    .line 49
    const/4 v0, 0x0

    array-length v1, p1

    invoke-virtual {p0, p1, v0, v1}, Lcom/a/b/g/a;->a([BII)V

    .line 50
    return-void
.end method

.method protected a([BII)V
    .registers 6

    .prologue
    .line 56
    move v0, p2

    :goto_1
    add-int v1, p2, p3

    if-ge v0, v1, :cond_d

    .line 57
    aget-byte v1, p1, v0

    invoke-virtual {p0, v1}, Lcom/a/b/g/a;->a(B)V

    .line 56
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 59
    :cond_d
    return-void
.end method

.method public final b(B)Lcom/a/b/g/al;
    .registers 2

    .prologue
    .line 63
    invoke-virtual {p0, p1}, Lcom/a/b/g/a;->a(B)V

    .line 64
    return-object p0
.end method

.method public final b([B)Lcom/a/b/g/al;
    .registers 2

    .prologue
    .line 69
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 70
    invoke-virtual {p0, p1}, Lcom/a/b/g/a;->a([B)V

    .line 71
    return-object p0
.end method

.method public final b([BII)Lcom/a/b/g/al;
    .registers 6

    .prologue
    .line 76
    add-int v0, p2, p3

    array-length v1, p1

    invoke-static {p2, v0, v1}, Lcom/a/b/b/cn;->a(III)V

    .line 77
    invoke-virtual {p0, p1, p2, p3}, Lcom/a/b/g/a;->a([BII)V

    .line 78
    return-object p0
.end method

.method public final synthetic b(C)Lcom/a/b/g/bn;
    .registers 3

    .prologue
    .line 36
    invoke-virtual {p0, p1}, Lcom/a/b/g/a;->a(C)Lcom/a/b/g/al;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(I)Lcom/a/b/g/bn;
    .registers 3

    .prologue
    .line 36
    invoke-virtual {p0, p1}, Lcom/a/b/g/a;->a(I)Lcom/a/b/g/al;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(J)Lcom/a/b/g/bn;
    .registers 4

    .prologue
    .line 36
    invoke-virtual {p0, p1, p2}, Lcom/a/b/g/a;->a(J)Lcom/a/b/g/al;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(S)Lcom/a/b/g/bn;
    .registers 3

    .prologue
    .line 36
    invoke-virtual {p0, p1}, Lcom/a/b/g/a;->a(S)Lcom/a/b/g/al;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c(B)Lcom/a/b/g/bn;
    .registers 2

    .prologue
    .line 36
    .line 1063
    invoke-virtual {p0, p1}, Lcom/a/b/g/a;->a(B)V

    .line 36
    return-object p0
.end method

.method public final synthetic c([B)Lcom/a/b/g/bn;
    .registers 3

    .prologue
    .line 36
    invoke-virtual {p0, p1}, Lcom/a/b/g/a;->b([B)Lcom/a/b/g/al;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c([BII)Lcom/a/b/g/bn;
    .registers 5

    .prologue
    .line 36
    invoke-virtual {p0, p1, p2, p3}, Lcom/a/b/g/a;->b([BII)Lcom/a/b/g/al;

    move-result-object v0

    return-object v0
.end method
