.class final Lcom/a/b/g/c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/g/al;


# instance fields
.field final synthetic a:[Lcom/a/b/g/al;

.field final synthetic b:Lcom/a/b/g/b;


# direct methods
.method constructor <init>(Lcom/a/b/g/b;[Lcom/a/b/g/al;)V
    .registers 3

    .prologue
    .line 54
    iput-object p1, p0, Lcom/a/b/g/c;->b:Lcom/a/b/g/b;

    iput-object p2, p0, Lcom/a/b/g/c;->a:[Lcom/a/b/g/al;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/a/b/g/ag;
    .registers 3

    .prologue
    .line 147
    iget-object v0, p0, Lcom/a/b/g/c;->b:Lcom/a/b/g/b;

    iget-object v1, p0, Lcom/a/b/g/c;->a:[Lcom/a/b/g/al;

    invoke-virtual {v0, v1}, Lcom/a/b/g/b;->a([Lcom/a/b/g/al;)Lcom/a/b/g/ag;

    move-result-object v0

    return-object v0
.end method

.method public final a(C)Lcom/a/b/g/al;
    .registers 6

    .prologue
    .line 119
    iget-object v1, p0, Lcom/a/b/g/c;->a:[Lcom/a/b/g/al;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_4
    if-ge v0, v2, :cond_e

    aget-object v3, v1, v0

    .line 120
    invoke-interface {v3, p1}, Lcom/a/b/g/al;->a(C)Lcom/a/b/g/al;

    .line 119
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 122
    :cond_e
    return-object p0
.end method

.method public final a(D)Lcom/a/b/g/al;
    .registers 8

    .prologue
    .line 105
    iget-object v1, p0, Lcom/a/b/g/c;->a:[Lcom/a/b/g/al;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_4
    if-ge v0, v2, :cond_e

    aget-object v3, v1, v0

    .line 106
    invoke-interface {v3, p1, p2}, Lcom/a/b/g/al;->a(D)Lcom/a/b/g/al;

    .line 105
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 108
    :cond_e
    return-object p0
.end method

.method public final a(F)Lcom/a/b/g/al;
    .registers 6

    .prologue
    .line 98
    iget-object v1, p0, Lcom/a/b/g/c;->a:[Lcom/a/b/g/al;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_4
    if-ge v0, v2, :cond_e

    aget-object v3, v1, v0

    .line 99
    invoke-interface {v3, p1}, Lcom/a/b/g/al;->a(F)Lcom/a/b/g/al;

    .line 98
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 101
    :cond_e
    return-object p0
.end method

.method public final a(I)Lcom/a/b/g/al;
    .registers 6

    .prologue
    .line 84
    iget-object v1, p0, Lcom/a/b/g/c;->a:[Lcom/a/b/g/al;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_4
    if-ge v0, v2, :cond_e

    aget-object v3, v1, v0

    .line 85
    invoke-interface {v3, p1}, Lcom/a/b/g/al;->a(I)Lcom/a/b/g/al;

    .line 84
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 87
    :cond_e
    return-object p0
.end method

.method public final a(J)Lcom/a/b/g/al;
    .registers 8

    .prologue
    .line 91
    iget-object v1, p0, Lcom/a/b/g/c;->a:[Lcom/a/b/g/al;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_4
    if-ge v0, v2, :cond_e

    aget-object v3, v1, v0

    .line 92
    invoke-interface {v3, p1, p2}, Lcom/a/b/g/al;->a(J)Lcom/a/b/g/al;

    .line 91
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 94
    :cond_e
    return-object p0
.end method

.method public final a(Ljava/lang/CharSequence;)Lcom/a/b/g/al;
    .registers 6

    .prologue
    .line 126
    iget-object v1, p0, Lcom/a/b/g/c;->a:[Lcom/a/b/g/al;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_4
    if-ge v0, v2, :cond_e

    aget-object v3, v1, v0

    .line 127
    invoke-interface {v3, p1}, Lcom/a/b/g/al;->a(Ljava/lang/CharSequence;)Lcom/a/b/g/al;

    .line 126
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 129
    :cond_e
    return-object p0
.end method

.method public final a(Ljava/lang/CharSequence;Ljava/nio/charset/Charset;)Lcom/a/b/g/al;
    .registers 7

    .prologue
    .line 133
    iget-object v1, p0, Lcom/a/b/g/c;->a:[Lcom/a/b/g/al;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_4
    if-ge v0, v2, :cond_e

    aget-object v3, v1, v0

    .line 134
    invoke-interface {v3, p1, p2}, Lcom/a/b/g/al;->a(Ljava/lang/CharSequence;Ljava/nio/charset/Charset;)Lcom/a/b/g/al;

    .line 133
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 136
    :cond_e
    return-object p0
.end method

.method public final a(Ljava/lang/Object;Lcom/a/b/g/w;)Lcom/a/b/g/al;
    .registers 7

    .prologue
    .line 140
    iget-object v1, p0, Lcom/a/b/g/c;->a:[Lcom/a/b/g/al;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_4
    if-ge v0, v2, :cond_e

    aget-object v3, v1, v0

    .line 141
    invoke-interface {v3, p1, p2}, Lcom/a/b/g/al;->a(Ljava/lang/Object;Lcom/a/b/g/w;)Lcom/a/b/g/al;

    .line 140
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 143
    :cond_e
    return-object p0
.end method

.method public final a(S)Lcom/a/b/g/al;
    .registers 6

    .prologue
    .line 77
    iget-object v1, p0, Lcom/a/b/g/c;->a:[Lcom/a/b/g/al;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_4
    if-ge v0, v2, :cond_e

    aget-object v3, v1, v0

    .line 78
    invoke-interface {v3, p1}, Lcom/a/b/g/al;->a(S)Lcom/a/b/g/al;

    .line 77
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 80
    :cond_e
    return-object p0
.end method

.method public final a(Z)Lcom/a/b/g/al;
    .registers 6

    .prologue
    .line 112
    iget-object v1, p0, Lcom/a/b/g/c;->a:[Lcom/a/b/g/al;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_4
    if-ge v0, v2, :cond_e

    aget-object v3, v1, v0

    .line 113
    invoke-interface {v3, p1}, Lcom/a/b/g/al;->a(Z)Lcom/a/b/g/al;

    .line 112
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 115
    :cond_e
    return-object p0
.end method

.method public final b(B)Lcom/a/b/g/al;
    .registers 6

    .prologue
    .line 56
    iget-object v1, p0, Lcom/a/b/g/c;->a:[Lcom/a/b/g/al;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_4
    if-ge v0, v2, :cond_e

    aget-object v3, v1, v0

    .line 57
    invoke-interface {v3, p1}, Lcom/a/b/g/al;->b(B)Lcom/a/b/g/al;

    .line 56
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 59
    :cond_e
    return-object p0
.end method

.method public final b([B)Lcom/a/b/g/al;
    .registers 6

    .prologue
    .line 63
    iget-object v1, p0, Lcom/a/b/g/c;->a:[Lcom/a/b/g/al;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_4
    if-ge v0, v2, :cond_e

    aget-object v3, v1, v0

    .line 64
    invoke-interface {v3, p1}, Lcom/a/b/g/al;->b([B)Lcom/a/b/g/al;

    .line 63
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 66
    :cond_e
    return-object p0
.end method

.method public final b([BII)Lcom/a/b/g/al;
    .registers 8

    .prologue
    .line 70
    iget-object v1, p0, Lcom/a/b/g/c;->a:[Lcom/a/b/g/al;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_4
    if-ge v0, v2, :cond_e

    aget-object v3, v1, v0

    .line 71
    invoke-interface {v3, p1, p2, p3}, Lcom/a/b/g/al;->b([BII)Lcom/a/b/g/al;

    .line 70
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 73
    :cond_e
    return-object p0
.end method

.method public final synthetic b(C)Lcom/a/b/g/bn;
    .registers 3

    .prologue
    .line 54
    invoke-virtual {p0, p1}, Lcom/a/b/g/c;->a(C)Lcom/a/b/g/al;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(D)Lcom/a/b/g/bn;
    .registers 4

    .prologue
    .line 54
    invoke-virtual {p0, p1, p2}, Lcom/a/b/g/c;->a(D)Lcom/a/b/g/al;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(F)Lcom/a/b/g/bn;
    .registers 3

    .prologue
    .line 54
    invoke-virtual {p0, p1}, Lcom/a/b/g/c;->a(F)Lcom/a/b/g/al;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(I)Lcom/a/b/g/bn;
    .registers 3

    .prologue
    .line 54
    invoke-virtual {p0, p1}, Lcom/a/b/g/c;->a(I)Lcom/a/b/g/al;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(J)Lcom/a/b/g/bn;
    .registers 4

    .prologue
    .line 54
    invoke-virtual {p0, p1, p2}, Lcom/a/b/g/c;->a(J)Lcom/a/b/g/al;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Ljava/lang/CharSequence;)Lcom/a/b/g/bn;
    .registers 3

    .prologue
    .line 54
    invoke-virtual {p0, p1}, Lcom/a/b/g/c;->a(Ljava/lang/CharSequence;)Lcom/a/b/g/al;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Ljava/lang/CharSequence;Ljava/nio/charset/Charset;)Lcom/a/b/g/bn;
    .registers 4

    .prologue
    .line 54
    invoke-virtual {p0, p1, p2}, Lcom/a/b/g/c;->a(Ljava/lang/CharSequence;Ljava/nio/charset/Charset;)Lcom/a/b/g/al;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(S)Lcom/a/b/g/bn;
    .registers 3

    .prologue
    .line 54
    invoke-virtual {p0, p1}, Lcom/a/b/g/c;->a(S)Lcom/a/b/g/al;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Z)Lcom/a/b/g/bn;
    .registers 3

    .prologue
    .line 54
    invoke-virtual {p0, p1}, Lcom/a/b/g/c;->a(Z)Lcom/a/b/g/al;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c(B)Lcom/a/b/g/bn;
    .registers 3

    .prologue
    .line 54
    invoke-virtual {p0, p1}, Lcom/a/b/g/c;->b(B)Lcom/a/b/g/al;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c([B)Lcom/a/b/g/bn;
    .registers 3

    .prologue
    .line 54
    invoke-virtual {p0, p1}, Lcom/a/b/g/c;->b([B)Lcom/a/b/g/al;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c([BII)Lcom/a/b/g/bn;
    .registers 5

    .prologue
    .line 54
    invoke-virtual {p0, p1, p2, p3}, Lcom/a/b/g/c;->b([BII)Lcom/a/b/g/al;

    move-result-object v0

    return-object v0
.end method
