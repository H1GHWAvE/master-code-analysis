.class abstract Lcom/a/b/g/h;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/g/ak;


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    return-void
.end method


# virtual methods
.method public a(J)Lcom/a/b/g/ag;
    .registers 4

    .prologue
    .line 53
    invoke-virtual {p0}, Lcom/a/b/g/h;->a()Lcom/a/b/g/al;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/a/b/g/al;->a(J)Lcom/a/b/g/al;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/b/g/al;->a()Lcom/a/b/g/ag;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/CharSequence;)Lcom/a/b/g/ag;
    .registers 3

    .prologue
    .line 41
    invoke-virtual {p0}, Lcom/a/b/g/h;->a()Lcom/a/b/g/al;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/a/b/g/al;->a(Ljava/lang/CharSequence;)Lcom/a/b/g/al;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/b/g/al;->a()Lcom/a/b/g/ag;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/CharSequence;Ljava/nio/charset/Charset;)Lcom/a/b/g/ag;
    .registers 4

    .prologue
    .line 45
    invoke-virtual {p0}, Lcom/a/b/g/h;->a()Lcom/a/b/g/al;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/a/b/g/al;->a(Ljava/lang/CharSequence;Ljava/nio/charset/Charset;)Lcom/a/b/g/al;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/b/g/al;->a()Lcom/a/b/g/ag;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;Lcom/a/b/g/w;)Lcom/a/b/g/ag;
    .registers 4

    .prologue
    .line 37
    invoke-virtual {p0}, Lcom/a/b/g/h;->a()Lcom/a/b/g/al;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/a/b/g/al;->a(Ljava/lang/Object;Lcom/a/b/g/w;)Lcom/a/b/g/al;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/b/g/al;->a()Lcom/a/b/g/ag;

    move-result-object v0

    return-object v0
.end method

.method public final a([B)Lcom/a/b/g/ag;
    .registers 3

    .prologue
    .line 57
    invoke-virtual {p0}, Lcom/a/b/g/h;->a()Lcom/a/b/g/al;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/a/b/g/al;->b([B)Lcom/a/b/g/al;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/b/g/al;->a()Lcom/a/b/g/ag;

    move-result-object v0

    return-object v0
.end method

.method public final a([BI)Lcom/a/b/g/ag;
    .registers 5

    .prologue
    .line 61
    invoke-virtual {p0}, Lcom/a/b/g/h;->a()Lcom/a/b/g/al;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1, p2}, Lcom/a/b/g/al;->b([BII)Lcom/a/b/g/al;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/b/g/al;->a()Lcom/a/b/g/ag;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)Lcom/a/b/g/al;
    .registers 3

    .prologue
    .line 65
    if-ltz p1, :cond_b

    const/4 v0, 0x1

    :goto_3
    invoke-static {v0}, Lcom/a/b/b/cn;->a(Z)V

    .line 66
    invoke-virtual {p0}, Lcom/a/b/g/h;->a()Lcom/a/b/g/al;

    move-result-object v0

    return-object v0

    .line 65
    :cond_b
    const/4 v0, 0x0

    goto :goto_3
.end method

.method public b(I)Lcom/a/b/g/ag;
    .registers 3

    .prologue
    .line 49
    invoke-virtual {p0}, Lcom/a/b/g/h;->a()Lcom/a/b/g/al;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/a/b/g/al;->a(I)Lcom/a/b/g/al;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/b/g/al;->a()Lcom/a/b/g/ag;

    move-result-object v0

    return-object v0
.end method
