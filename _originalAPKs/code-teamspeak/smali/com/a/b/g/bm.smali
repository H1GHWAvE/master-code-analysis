.class final Lcom/a/b/g/bm;
.super Lcom/a/b/g/i;
.source "SourceFile"


# static fields
.field private static final a:I = 0x4


# instance fields
.field private b:I

.field private c:I


# direct methods
.method constructor <init>(I)V
    .registers 3

    .prologue
    .line 156
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/a/b/g/i;-><init>(I)V

    .line 157
    iput p1, p0, Lcom/a/b/g/bm;->b:I

    .line 158
    const/4 v0, 0x0

    iput v0, p0, Lcom/a/b/g/bm;->c:I

    .line 159
    return-void
.end method


# virtual methods
.method protected final a(Ljava/nio/ByteBuffer;)V
    .registers 4

    .prologue
    .line 162
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v0

    invoke-static {v0}, Lcom/a/b/g/bl;->c(I)I

    move-result v0

    .line 163
    iget v1, p0, Lcom/a/b/g/bm;->b:I

    invoke-static {v1, v0}, Lcom/a/b/g/bl;->a(II)I

    move-result v0

    iput v0, p0, Lcom/a/b/g/bm;->b:I

    .line 164
    iget v0, p0, Lcom/a/b/g/bm;->c:I

    add-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/a/b/g/bm;->c:I

    .line 165
    return-void
.end method

.method public final b()Lcom/a/b/g/ag;
    .registers 3

    .prologue
    .line 177
    iget v0, p0, Lcom/a/b/g/bm;->b:I

    iget v1, p0, Lcom/a/b/g/bm;->c:I

    invoke-static {v0, v1}, Lcom/a/b/g/bl;->b(II)Lcom/a/b/g/ag;

    move-result-object v0

    return-object v0
.end method

.method protected final b(Ljava/nio/ByteBuffer;)V
    .registers 5

    .prologue
    const/4 v0, 0x0

    .line 168
    iget v1, p0, Lcom/a/b/g/bm;->c:I

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v2

    add-int/2addr v1, v2

    iput v1, p0, Lcom/a/b/g/bm;->c:I

    move v1, v0

    .line 170
    :goto_b
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->hasRemaining()Z

    move-result v2

    if-eqz v2, :cond_1c

    .line 171
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->get()B

    move-result v2

    .line 1075
    and-int/lit16 v2, v2, 0xff

    .line 171
    shl-int/2addr v2, v0

    xor-int/2addr v1, v2

    .line 170
    add-int/lit8 v0, v0, 0x8

    goto :goto_b

    .line 173
    :cond_1c
    iget v0, p0, Lcom/a/b/g/bm;->b:I

    invoke-static {v1}, Lcom/a/b/g/bl;->c(I)I

    move-result v1

    xor-int/2addr v0, v1

    iput v0, p0, Lcom/a/b/g/bm;->b:I

    .line 174
    return-void
.end method
