.class final Lcom/a/b/g/ah;
.super Lcom/a/b/g/ag;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final b:J


# instance fields
.field final a:[B


# direct methods
.method constructor <init>([B)V
    .registers 3

    .prologue
    .line 260
    invoke-direct {p0}, Lcom/a/b/g/ag;-><init>()V

    .line 261
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    iput-object v0, p0, Lcom/a/b/g/ah;->a:[B

    .line 262
    return-void
.end method


# virtual methods
.method public final a()I
    .registers 2

    .prologue
    .line 266
    iget-object v0, p0, Lcom/a/b/g/ah;->a:[B

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x8

    return v0
.end method

.method final a([BII)V
    .registers 6

    .prologue
    .line 302
    iget-object v0, p0, Lcom/a/b/g/ah;->a:[B

    const/4 v1, 0x0

    invoke-static {v0, v1, p1, p2, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 303
    return-void
.end method

.method final a(Lcom/a/b/g/ag;)Z
    .registers 4

    .prologue
    .line 312
    iget-object v0, p0, Lcom/a/b/g/ah;->a:[B

    invoke-virtual {p1}, Lcom/a/b/g/ag;->f()[B

    move-result-object v1

    invoke-static {v0, v1}, Ljava/security/MessageDigest;->isEqual([B[B)Z

    move-result v0

    return v0
.end method

.method public final b()I
    .registers 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 276
    iget-object v0, p0, Lcom/a/b/g/ah;->a:[B

    array-length v0, v0

    const/4 v3, 0x4

    if-lt v0, v3, :cond_3d

    move v0, v1

    :goto_9
    const-string v3, "HashCode#asInt() requires >= 4 bytes (it only has %s bytes)."

    new-array v4, v1, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/a/b/g/ah;->a:[B

    array-length v5, v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v0, v3, v4}, Lcom/a/b/b/cn;->b(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 278
    iget-object v0, p0, Lcom/a/b/g/ah;->a:[B

    aget-byte v0, v0, v2

    and-int/lit16 v0, v0, 0xff

    iget-object v2, p0, Lcom/a/b/g/ah;->a:[B

    aget-byte v1, v2, v1

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x8

    or-int/2addr v0, v1

    iget-object v1, p0, Lcom/a/b/g/ah;->a:[B

    const/4 v2, 0x2

    aget-byte v1, v1, v2

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x10

    or-int/2addr v0, v1

    iget-object v1, p0, Lcom/a/b/g/ah;->a:[B

    const/4 v2, 0x3

    aget-byte v1, v1, v2

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x18

    or-int/2addr v0, v1

    return v0

    :cond_3d
    move v0, v2

    .line 276
    goto :goto_9
.end method

.method public final c()J
    .registers 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 286
    iget-object v0, p0, Lcom/a/b/g/ah;->a:[B

    array-length v0, v0

    const/16 v3, 0x8

    if-lt v0, v3, :cond_1f

    move v0, v1

    :goto_a
    const-string v3, "HashCode#asLong() requires >= 8 bytes (it only has %s bytes)."

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/a/b/g/ah;->a:[B

    array-length v4, v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v2

    invoke-static {v0, v3, v1}, Lcom/a/b/b/cn;->b(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 288
    invoke-virtual {p0}, Lcom/a/b/g/ah;->d()J

    move-result-wide v0

    return-wide v0

    :cond_1f
    move v0, v2

    .line 286
    goto :goto_a
.end method

.method public final d()J
    .registers 9

    .prologue
    .line 293
    iget-object v0, p0, Lcom/a/b/g/ah;->a:[B

    const/4 v1, 0x0

    aget-byte v0, v0, v1

    and-int/lit16 v0, v0, 0xff

    int-to-long v2, v0

    .line 294
    const/4 v0, 0x1

    :goto_9
    iget-object v1, p0, Lcom/a/b/g/ah;->a:[B

    array-length v1, v1

    const/16 v4, 0x8

    invoke-static {v1, v4}, Ljava/lang/Math;->min(II)I

    move-result v1

    if-ge v0, v1, :cond_23

    .line 295
    iget-object v1, p0, Lcom/a/b/g/ah;->a:[B

    aget-byte v1, v1, v0

    int-to-long v4, v1

    const-wide/16 v6, 0xff

    and-long/2addr v4, v6

    mul-int/lit8 v1, v0, 0x8

    shl-long/2addr v4, v1

    or-long/2addr v2, v4

    .line 294
    add-int/lit8 v0, v0, 0x1

    goto :goto_9

    .line 297
    :cond_23
    return-wide v2
.end method

.method public final e()[B
    .registers 2

    .prologue
    .line 271
    iget-object v0, p0, Lcom/a/b/g/ah;->a:[B

    invoke-virtual {v0}, [B->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    return-object v0
.end method

.method final f()[B
    .registers 2

    .prologue
    .line 307
    iget-object v0, p0, Lcom/a/b/g/ah;->a:[B

    return-object v0
.end method
