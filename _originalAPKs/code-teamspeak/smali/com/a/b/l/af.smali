.class final enum Lcom/a/b/l/af;
.super Ljava/lang/Enum;
.source "SourceFile"

# interfaces
.implements Ljava/util/Comparator;


# static fields
.field public static final enum a:Lcom/a/b/l/af;

.field private static final synthetic b:[Lcom/a/b/l/af;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    .line 188
    new-instance v0, Lcom/a/b/l/af;

    const-string v1, "INSTANCE"

    invoke-direct {v0, v1}, Lcom/a/b/l/af;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/a/b/l/af;->a:Lcom/a/b/l/af;

    .line 187
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/a/b/l/af;

    const/4 v1, 0x0

    sget-object v2, Lcom/a/b/l/af;->a:Lcom/a/b/l/af;

    aput-object v2, v0, v1

    sput-object v0, Lcom/a/b/l/af;->b:[Lcom/a/b/l/af;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .registers 3

    .prologue
    .line 187
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method private static a([B[B)I
    .registers 6

    .prologue
    .line 192
    array-length v0, p0

    array-length v1, p1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 193
    const/4 v0, 0x0

    move v1, v0

    :goto_8
    if-ge v1, v2, :cond_16

    .line 194
    aget-byte v0, p0, v1

    aget-byte v3, p1, v1

    .line 1102
    sub-int/2addr v0, v3

    .line 195
    if-eqz v0, :cond_12

    .line 199
    :goto_11
    return v0

    .line 193
    :cond_12
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_8

    .line 199
    :cond_16
    array-length v0, p0

    array-length v1, p1

    sub-int/2addr v0, v1

    goto :goto_11
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/a/b/l/af;
    .registers 2

    .prologue
    .line 187
    const-class v0, Lcom/a/b/l/af;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/a/b/l/af;

    return-object v0
.end method

.method public static values()[Lcom/a/b/l/af;
    .registers 1

    .prologue
    .line 187
    sget-object v0, Lcom/a/b/l/af;->b:[Lcom/a/b/l/af;

    invoke-virtual {v0}, [Lcom/a/b/l/af;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/a/b/l/af;

    return-object v0
.end method


# virtual methods
.method public final synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .registers 7

    .prologue
    .line 187
    check-cast p1, [B

    check-cast p2, [B

    .line 1192
    array-length v0, p1

    array-length v1, p2

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 1193
    const/4 v0, 0x0

    move v1, v0

    :goto_c
    if-ge v1, v2, :cond_1a

    .line 1194
    aget-byte v0, p1, v1

    aget-byte v3, p2, v1

    .line 2102
    sub-int/2addr v0, v3

    .line 1195
    if-eqz v0, :cond_16

    .line 1196
    :goto_15
    return v0

    .line 1193
    :cond_16
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_c

    .line 1199
    :cond_1a
    array-length v0, p1

    array-length v1, p2

    sub-int/2addr v0, v1

    .line 187
    goto :goto_15
.end method
