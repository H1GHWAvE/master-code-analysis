.class public final Lcom/a/b/l/ap;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/a;
.end annotation

.annotation build Lcom/a/b/a/b;
.end annotation


# static fields
.field public static final a:J = -0x1L

.field private static final b:[J

.field private static final c:[I

.field private static final d:[I


# direct methods
.method static constructor <clinit>()V
    .registers 8

    .prologue
    const-wide/16 v6, -0x1

    const/16 v1, 0x25

    .line 384
    new-array v0, v1, [J

    sput-object v0, Lcom/a/b/l/ap;->b:[J

    .line 385
    new-array v0, v1, [I

    sput-object v0, Lcom/a/b/l/ap;->c:[I

    .line 386
    new-array v0, v1, [I

    sput-object v0, Lcom/a/b/l/ap;->d:[I

    .line 388
    new-instance v1, Ljava/math/BigInteger;

    const-string v0, "10000000000000000"

    const/16 v2, 0x10

    invoke-direct {v1, v0, v2}, Ljava/math/BigInteger;-><init>(Ljava/lang/String;I)V

    .line 389
    const/4 v0, 0x2

    :goto_1a
    const/16 v2, 0x24

    if-gt v0, v2, :cond_42

    .line 390
    sget-object v2, Lcom/a/b/l/ap;->b:[J

    int-to-long v4, v0

    invoke-static {v6, v7, v4, v5}, Lcom/a/b/l/ap;->b(JJ)J

    move-result-wide v4

    aput-wide v4, v2, v0

    .line 391
    sget-object v2, Lcom/a/b/l/ap;->c:[I

    int-to-long v4, v0

    invoke-static {v6, v7, v4, v5}, Lcom/a/b/l/ap;->c(JJ)J

    move-result-wide v4

    long-to-int v3, v4

    aput v3, v2, v0

    .line 392
    sget-object v2, Lcom/a/b/l/ap;->d:[I

    invoke-virtual {v1, v0}, Ljava/math/BigInteger;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    aput v3, v2, v0

    .line 389
    add-int/lit8 v0, v0, 0x1

    goto :goto_1a

    .line 394
    :cond_42
    return-void
.end method

.method private constructor <init>()V
    .registers 1

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(JJ)I
    .registers 8

    .prologue
    const-wide/high16 v2, -0x8000000000000000L

    .line 76
    .line 1063
    xor-long v0, p0, v2

    .line 2063
    xor-long/2addr v2, p2

    .line 76
    invoke-static {v0, v1, v2, v3}, Lcom/a/b/l/u;->a(JJ)I

    move-result v0

    return v0
.end method

.method private static a(Ljava/lang/String;)J
    .registers 3

    .prologue
    .line 250
    const/16 v0, 0xa

    invoke-static {p0, v0}, Lcom/a/b/l/ap;->a(Ljava/lang/String;I)J

    move-result-wide v0

    return-wide v0
.end method

.method public static a(Ljava/lang/String;I)J
    .registers 14

    .prologue
    const-wide/16 v4, 0x0

    const/4 v1, 0x0

    .line 294
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 295
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_14

    .line 296
    new-instance v0, Ljava/lang/NumberFormatException;

    const-string v1, "empty string"

    invoke-direct {v0, v1}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 298
    :cond_14
    const/4 v0, 0x2

    if-lt p1, v0, :cond_1b

    const/16 v0, 0x24

    if-le p1, v0, :cond_36

    .line 299
    :cond_1b
    new-instance v0, Ljava/lang/NumberFormatException;

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x1a

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "illegal radix: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 302
    :cond_36
    sget-object v0, Lcom/a/b/l/ap;->d:[I

    aget v0, v0, p1

    add-int/lit8 v7, v0, -0x1

    move v0, v1

    move-wide v2, v4

    .line 304
    :goto_3e
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v6

    if-ge v0, v6, :cond_9b

    .line 305
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v6

    invoke-static {v6, p1}, Ljava/lang/Character;->digit(CI)I

    move-result v8

    .line 306
    const/4 v6, -0x1

    if-ne v8, v6, :cond_55

    .line 307
    new-instance v0, Ljava/lang/NumberFormatException;

    invoke-direct {v0, p0}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 309
    :cond_55
    if-le v0, v7, :cond_94

    .line 8325
    cmp-long v6, v2, v4

    if-ltz v6, :cond_8c

    .line 8326
    sget-object v6, Lcom/a/b/l/ap;->b:[J

    aget-wide v10, v6, p1

    cmp-long v6, v2, v10

    if-gez v6, :cond_7c

    move v6, v1

    .line 309
    :goto_64
    if-eqz v6, :cond_94

    .line 310
    new-instance v1, Ljava/lang/NumberFormatException;

    const-string v2, "Too large for unsigned long: "

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_8e

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_78
    invoke-direct {v1, v0}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 8329
    :cond_7c
    sget-object v6, Lcom/a/b/l/ap;->b:[J

    aget-wide v10, v6, p1

    cmp-long v6, v2, v10

    if-gtz v6, :cond_8c

    .line 8333
    sget-object v6, Lcom/a/b/l/ap;->c:[I

    aget v6, v6, p1

    if-gt v8, v6, :cond_8c

    move v6, v1

    goto :goto_64

    .line 8337
    :cond_8c
    const/4 v6, 0x1

    goto :goto_64

    .line 310
    :cond_8e
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_78

    .line 312
    :cond_94
    int-to-long v10, p1

    mul-long/2addr v2, v10

    int-to-long v8, v8

    add-long/2addr v2, v8

    .line 304
    add-int/lit8 v0, v0, 0x1

    goto :goto_3e

    .line 315
    :cond_9b
    return-wide v2
.end method

.method private static varargs a([J)J
    .registers 9

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const-wide/high16 v6, -0x8000000000000000L

    .line 88
    array-length v0, p0

    if-lez v0, :cond_1c

    move v0, v1

    :goto_8
    invoke-static {v0}, Lcom/a/b/b/cn;->a(Z)V

    .line 89
    aget-wide v2, p0, v2

    .line 3063
    xor-long/2addr v2, v6

    .line 90
    :goto_e
    array-length v0, p0

    if-ge v1, v0, :cond_1e

    .line 91
    aget-wide v4, p0, v1

    .line 4063
    xor-long/2addr v4, v6

    .line 92
    cmp-long v0, v4, v2

    if-gez v0, :cond_19

    move-wide v2, v4

    .line 90
    :cond_19
    add-int/lit8 v1, v1, 0x1

    goto :goto_e

    :cond_1c
    move v0, v2

    .line 88
    goto :goto_8

    .line 5063
    :cond_1e
    xor-long v0, v2, v6

    .line 96
    return-wide v0
.end method

.method public static a(J)Ljava/lang/String;
    .registers 4

    .prologue
    .line 344
    const/16 v0, 0xa

    invoke-static {p0, p1, v0}, Lcom/a/b/l/ap;->a(JI)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(JI)Ljava/lang/String;
    .registers 13

    .prologue
    const/16 v4, 0x40

    const/16 v3, 0x3f

    const/4 v1, 0x1

    const/4 v2, 0x0

    const-wide/16 v8, 0x0

    .line 357
    const/4 v0, 0x2

    if-lt p2, v0, :cond_24

    const/16 v0, 0x24

    if-gt p2, v0, :cond_24

    move v0, v1

    :goto_10
    const-string v5, "radix (%s) must be between Character.MIN_RADIX and Character.MAX_RADIX"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v1, v2

    invoke-static {v0, v5, v1}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 359
    cmp-long v0, p0, v8

    if-nez v0, :cond_26

    .line 361
    const-string v0, "0"

    .line 379
    :goto_23
    return-object v0

    :cond_24
    move v0, v2

    .line 357
    goto :goto_10

    .line 363
    :cond_26
    new-array v2, v4, [C

    .line 365
    cmp-long v0, p0, v8

    if-gez v0, :cond_5a

    .line 368
    int-to-long v0, p2

    invoke-static {p0, p1, v0, v1}, Lcom/a/b/l/ap;->b(JJ)J

    move-result-wide v0

    .line 369
    int-to-long v4, p2

    mul-long/2addr v4, v0

    sub-long v4, p0, v4

    .line 370
    long-to-int v4, v4

    invoke-static {v4, p2}, Ljava/lang/Character;->forDigit(II)C

    move-result v4

    aput-char v4, v2, v3

    move-wide p0, v0

    move v1, v3

    .line 374
    :goto_3e
    cmp-long v0, p0, v8

    if-lez v0, :cond_52

    .line 375
    add-int/lit8 v0, v1, -0x1

    int-to-long v4, p2

    rem-long v4, p0, v4

    long-to-int v1, v4

    invoke-static {v1, p2}, Ljava/lang/Character;->forDigit(II)C

    move-result v1

    aput-char v1, v2, v0

    .line 376
    int-to-long v4, p2

    div-long/2addr p0, v4

    move v1, v0

    goto :goto_3e

    .line 379
    :cond_52
    new-instance v0, Ljava/lang/String;

    rsub-int/lit8 v3, v1, 0x40

    invoke-direct {v0, v2, v1, v3}, Ljava/lang/String;-><init>([CII)V

    goto :goto_23

    :cond_5a
    move v1, v4

    goto :goto_3e
.end method

.method private static varargs a(Ljava/lang/String;[J)Ljava/lang/String;
    .registers 8

    .prologue
    .line 128
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 129
    array-length v0, p1

    if-nez v0, :cond_9

    .line 130
    const-string v0, ""

    .line 139
    :goto_8
    return-object v0

    .line 134
    :cond_9
    new-instance v1, Ljava/lang/StringBuilder;

    array-length v0, p1

    mul-int/lit8 v0, v0, 0x5

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 135
    const/4 v0, 0x0

    aget-wide v2, p1, v0

    invoke-static {v2, v3}, Lcom/a/b/l/ap;->a(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 136
    const/4 v0, 0x1

    :goto_1c
    array-length v2, p1

    if-ge v0, v2, :cond_2f

    .line 137
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-wide v4, p1, v0

    invoke-static {v4, v5}, Lcom/a/b/l/ap;->a(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 136
    add-int/lit8 v0, v0, 0x1

    goto :goto_1c

    .line 139
    :cond_2f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_8
.end method

.method private static a()Ljava/util/Comparator;
    .registers 1

    .prologue
    .line 156
    sget-object v0, Lcom/a/b/l/aq;->a:Lcom/a/b/l/aq;

    return-object v0
.end method

.method private static a(JII)Z
    .registers 8

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 325
    const-wide/16 v2, 0x0

    cmp-long v2, p0, v2

    if-ltz v2, :cond_23

    .line 326
    sget-object v2, Lcom/a/b/l/ap;->b:[J

    aget-wide v2, v2, p3

    cmp-long v2, p0, v2

    if-gez v2, :cond_11

    .line 337
    :cond_10
    :goto_10
    return v0

    .line 329
    :cond_11
    sget-object v2, Lcom/a/b/l/ap;->b:[J

    aget-wide v2, v2, p3

    cmp-long v2, p0, v2

    if-lez v2, :cond_1b

    move v0, v1

    .line 330
    goto :goto_10

    .line 333
    :cond_1b
    sget-object v2, Lcom/a/b/l/ap;->c:[I

    aget v2, v2, p3

    if-le p2, v2, :cond_10

    move v0, v1

    goto :goto_10

    :cond_23
    move v0, v1

    .line 337
    goto :goto_10
.end method

.method private static b(J)J
    .registers 4

    .prologue
    .line 63
    const-wide/high16 v0, -0x8000000000000000L

    xor-long/2addr v0, p0

    return-wide v0
.end method

.method public static b(JJ)J
    .registers 10

    .prologue
    const-wide/16 v2, 0x0

    const/4 v0, 0x1

    .line 183
    cmp-long v1, p2, v2

    if-gez v1, :cond_12

    .line 184
    invoke-static {p0, p1, p2, p3}, Lcom/a/b/l/ap;->a(JJ)I

    move-result v0

    if-gez v0, :cond_f

    move-wide v0, v2

    .line 204
    :goto_e
    return-wide v0

    .line 187
    :cond_f
    const-wide/16 v0, 0x1

    goto :goto_e

    .line 192
    :cond_12
    cmp-long v1, p0, v2

    if-ltz v1, :cond_19

    .line 193
    div-long v0, p0, p2

    goto :goto_e

    .line 202
    :cond_19
    ushr-long v2, p0, v0

    div-long/2addr v2, p2

    shl-long/2addr v2, v0

    .line 203
    mul-long v4, v2, p2

    sub-long v4, p0, v4

    .line 204
    invoke-static {v4, v5, p2, p3}, Lcom/a/b/l/ap;->a(JJ)I

    move-result v1

    if-ltz v1, :cond_2a

    :goto_27
    int-to-long v0, v0

    add-long/2addr v0, v2

    goto :goto_e

    :cond_2a
    const/4 v0, 0x0

    goto :goto_27
.end method

.method private static b(Ljava/lang/String;)J
    .registers 6

    .prologue
    .line 270
    invoke-static {p0}, Lcom/a/b/l/y;->a(Ljava/lang/String;)Lcom/a/b/l/y;

    move-result-object v0

    .line 273
    :try_start_4
    iget-object v1, v0, Lcom/a/b/l/y;->a:Ljava/lang/String;

    iget v0, v0, Lcom/a/b/l/y;->b:I

    invoke-static {v1, v0}, Lcom/a/b/l/ap;->a(Ljava/lang/String;I)J
    :try_end_b
    .catch Ljava/lang/NumberFormatException; {:try_start_4 .. :try_end_b} :catch_d

    move-result-wide v0

    return-wide v0

    .line 274
    :catch_d
    move-exception v0

    move-object v1, v0

    .line 275
    new-instance v2, Ljava/lang/NumberFormatException;

    const-string v3, "Error parsing value: "

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_28

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_21
    invoke-direct {v2, v0}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    .line 277
    invoke-virtual {v2, v1}, Ljava/lang/NumberFormatException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 278
    throw v2

    .line 275
    :cond_28
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_21
.end method

.method private static varargs b([J)J
    .registers 9

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const-wide/high16 v6, -0x8000000000000000L

    .line 108
    array-length v0, p0

    if-lez v0, :cond_1c

    move v0, v1

    :goto_8
    invoke-static {v0}, Lcom/a/b/b/cn;->a(Z)V

    .line 109
    aget-wide v2, p0, v2

    .line 6063
    xor-long/2addr v2, v6

    .line 110
    :goto_e
    array-length v0, p0

    if-ge v1, v0, :cond_1e

    .line 111
    aget-wide v4, p0, v1

    .line 7063
    xor-long/2addr v4, v6

    .line 112
    cmp-long v0, v4, v2

    if-lez v0, :cond_19

    move-wide v2, v4

    .line 110
    :cond_19
    add-int/lit8 v1, v1, 0x1

    goto :goto_e

    :cond_1c
    move v0, v2

    .line 108
    goto :goto_8

    .line 8063
    :cond_1e
    xor-long v0, v2, v6

    .line 116
    return-wide v0
.end method

.method public static c(JJ)J
    .registers 10

    .prologue
    const/4 v4, 0x1

    const-wide/16 v0, 0x0

    .line 217
    cmp-long v2, p2, v0

    if-gez v2, :cond_10

    .line 218
    invoke-static {p0, p1, p2, p3}, Lcom/a/b/l/ap;->a(JJ)I

    move-result v0

    if-gez v0, :cond_e

    .line 238
    :goto_d
    return-wide p0

    .line 221
    :cond_e
    sub-long/2addr p0, p2

    goto :goto_d

    .line 226
    :cond_10
    cmp-long v2, p0, v0

    if-ltz v2, :cond_16

    .line 227
    rem-long/2addr p0, p2

    goto :goto_d

    .line 236
    :cond_16
    ushr-long v2, p0, v4

    div-long/2addr v2, p2

    shl-long/2addr v2, v4

    .line 237
    mul-long/2addr v2, p2

    sub-long v2, p0, v2

    .line 238
    invoke-static {v2, v3, p2, p3}, Lcom/a/b/l/ap;->a(JJ)I

    move-result v4

    if-ltz v4, :cond_26

    :goto_23
    sub-long p0, v2, p2

    goto :goto_d

    :cond_26
    move-wide p2, v0

    goto :goto_23
.end method
