.class final Lcom/a/b/l/j;
.super Ljava/util/AbstractList;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/util/RandomAccess;


# annotations
.annotation build Lcom/a/b/a/b;
.end annotation


# static fields
.field private static final d:J


# instance fields
.field final a:[D

.field final b:I

.field final c:I


# direct methods
.method constructor <init>([D)V
    .registers 4

    .prologue
    .line 466
    const/4 v0, 0x0

    array-length v1, p1

    invoke-direct {p0, p1, v0, v1}, Lcom/a/b/l/j;-><init>([DII)V

    .line 467
    return-void
.end method

.method private constructor <init>([DII)V
    .registers 4

    .prologue
    .line 469
    invoke-direct {p0}, Ljava/util/AbstractList;-><init>()V

    .line 470
    iput-object p1, p0, Lcom/a/b/l/j;->a:[D

    .line 471
    iput p2, p0, Lcom/a/b/l/j;->b:I

    .line 472
    iput p3, p0, Lcom/a/b/l/j;->c:I

    .line 473
    return-void
.end method

.method private a(I)Ljava/lang/Double;
    .registers 4

    .prologue
    .line 484
    invoke-virtual {p0}, Lcom/a/b/l/j;->size()I

    move-result v0

    invoke-static {p1, v0}, Lcom/a/b/b/cn;->a(II)I

    .line 485
    iget-object v0, p0, Lcom/a/b/l/j;->a:[D

    iget v1, p0, Lcom/a/b/l/j;->b:I

    add-int/2addr v1, p1

    aget-wide v0, v0, v1

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    return-object v0
.end method

.method private a(ILjava/lang/Double;)Ljava/lang/Double;
    .registers 11

    .prologue
    .line 517
    invoke-virtual {p0}, Lcom/a/b/l/j;->size()I

    move-result v0

    invoke-static {p1, v0}, Lcom/a/b/b/cn;->a(II)I

    .line 518
    iget-object v0, p0, Lcom/a/b/l/j;->a:[D

    iget v1, p0, Lcom/a/b/l/j;->b:I

    add-int/2addr v1, p1

    aget-wide v2, v0, v1

    .line 520
    iget-object v1, p0, Lcom/a/b/l/j;->a:[D

    iget v0, p0, Lcom/a/b/l/j;->b:I

    add-int v4, v0, p1

    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    aput-wide v6, v1, v4

    .line 521
    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    return-object v0
.end method

.method private a()[D
    .registers 6

    .prologue
    .line 572
    invoke-virtual {p0}, Lcom/a/b/l/j;->size()I

    move-result v0

    .line 573
    new-array v1, v0, [D

    .line 574
    iget-object v2, p0, Lcom/a/b/l/j;->a:[D

    iget v3, p0, Lcom/a/b/l/j;->b:I

    const/4 v4, 0x0

    invoke-static {v2, v3, v1, v4, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 575
    return-object v1
.end method


# virtual methods
.method public final contains(Ljava/lang/Object;)Z
    .registers 7

    .prologue
    .line 490
    instance-of v0, p1, Ljava/lang/Double;

    if-eqz v0, :cond_19

    iget-object v0, p0, Lcom/a/b/l/j;->a:[D

    check-cast p1, Ljava/lang/Double;

    invoke-virtual {p1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    iget v1, p0, Lcom/a/b/l/j;->b:I

    iget v4, p0, Lcom/a/b/l/j;->c:I

    invoke-static {v0, v2, v3, v1, v4}, Lcom/a/b/l/i;->a([DDII)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_19

    const/4 v0, 0x1

    :goto_18
    return v0

    :cond_19
    const/4 v0, 0x0

    goto :goto_18
.end method

.method public final equals(Ljava/lang/Object;)Z
    .registers 10

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 534
    if-ne p1, p0, :cond_5

    .line 550
    :cond_4
    :goto_4
    return v0

    .line 537
    :cond_5
    instance-of v2, p1, Lcom/a/b/l/j;

    if-eqz v2, :cond_31

    .line 538
    check-cast p1, Lcom/a/b/l/j;

    .line 539
    invoke-virtual {p0}, Lcom/a/b/l/j;->size()I

    move-result v3

    .line 540
    invoke-virtual {p1}, Lcom/a/b/l/j;->size()I

    move-result v2

    if-eq v2, v3, :cond_17

    move v0, v1

    .line 541
    goto :goto_4

    :cond_17
    move v2, v1

    .line 543
    :goto_18
    if-ge v2, v3, :cond_4

    .line 544
    iget-object v4, p0, Lcom/a/b/l/j;->a:[D

    iget v5, p0, Lcom/a/b/l/j;->b:I

    add-int/2addr v5, v2

    aget-wide v4, v4, v5

    iget-object v6, p1, Lcom/a/b/l/j;->a:[D

    iget v7, p1, Lcom/a/b/l/j;->b:I

    add-int/2addr v7, v2

    aget-wide v6, v6, v7

    cmpl-double v4, v4, v6

    if-eqz v4, :cond_2e

    move v0, v1

    .line 545
    goto :goto_4

    .line 543
    :cond_2e
    add-int/lit8 v2, v2, 0x1

    goto :goto_18

    .line 550
    :cond_31
    invoke-super {p0, p1}, Ljava/util/AbstractList;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_4
.end method

.method public final synthetic get(I)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 458
    .line 2484
    invoke-virtual {p0}, Lcom/a/b/l/j;->size()I

    move-result v0

    invoke-static {p1, v0}, Lcom/a/b/b/cn;->a(II)I

    .line 2485
    iget-object v0, p0, Lcom/a/b/l/j;->a:[D

    iget v1, p0, Lcom/a/b/l/j;->b:I

    add-int/2addr v1, p1

    aget-wide v0, v0, v1

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    .line 458
    return-object v0
.end method

.method public final hashCode()I
    .registers 5

    .prologue
    .line 554
    const/4 v1, 0x1

    .line 555
    iget v0, p0, Lcom/a/b/l/j;->b:I

    :goto_3
    iget v2, p0, Lcom/a/b/l/j;->c:I

    if-ge v0, v2, :cond_15

    .line 556
    mul-int/lit8 v1, v1, 0x1f

    iget-object v2, p0, Lcom/a/b/l/j;->a:[D

    aget-wide v2, v2, v0

    invoke-static {v2, v3}, Lcom/a/b/l/i;->a(D)I

    move-result v2

    add-int/2addr v1, v2

    .line 555
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 558
    :cond_15
    return v1
.end method

.method public final indexOf(Ljava/lang/Object;)I
    .registers 7

    .prologue
    .line 496
    instance-of v0, p1, Ljava/lang/Double;

    if-eqz v0, :cond_1a

    .line 497
    iget-object v0, p0, Lcom/a/b/l/j;->a:[D

    check-cast p1, Ljava/lang/Double;

    invoke-virtual {p1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    iget v1, p0, Lcom/a/b/l/j;->b:I

    iget v4, p0, Lcom/a/b/l/j;->c:I

    invoke-static {v0, v2, v3, v1, v4}, Lcom/a/b/l/i;->a([DDII)I

    move-result v0

    .line 498
    if-ltz v0, :cond_1a

    .line 499
    iget v1, p0, Lcom/a/b/l/j;->b:I

    sub-int/2addr v0, v1

    .line 502
    :goto_19
    return v0

    :cond_1a
    const/4 v0, -0x1

    goto :goto_19
.end method

.method public final isEmpty()Z
    .registers 2

    .prologue
    .line 480
    const/4 v0, 0x0

    return v0
.end method

.method public final lastIndexOf(Ljava/lang/Object;)I
    .registers 7

    .prologue
    .line 507
    instance-of v0, p1, Ljava/lang/Double;

    if-eqz v0, :cond_1a

    .line 508
    iget-object v0, p0, Lcom/a/b/l/j;->a:[D

    check-cast p1, Ljava/lang/Double;

    invoke-virtual {p1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    iget v1, p0, Lcom/a/b/l/j;->b:I

    iget v4, p0, Lcom/a/b/l/j;->c:I

    invoke-static {v0, v2, v3, v1, v4}, Lcom/a/b/l/i;->b([DDII)I

    move-result v0

    .line 509
    if-ltz v0, :cond_1a

    .line 510
    iget v1, p0, Lcom/a/b/l/j;->b:I

    sub-int/2addr v0, v1

    .line 513
    :goto_19
    return v0

    :cond_1a
    const/4 v0, -0x1

    goto :goto_19
.end method

.method public final synthetic set(ILjava/lang/Object;)Ljava/lang/Object;
    .registers 11

    .prologue
    .line 458
    check-cast p2, Ljava/lang/Double;

    .line 1517
    invoke-virtual {p0}, Lcom/a/b/l/j;->size()I

    move-result v0

    invoke-static {p1, v0}, Lcom/a/b/b/cn;->a(II)I

    .line 1518
    iget-object v0, p0, Lcom/a/b/l/j;->a:[D

    iget v1, p0, Lcom/a/b/l/j;->b:I

    add-int/2addr v1, p1

    aget-wide v2, v0, v1

    .line 1520
    iget-object v1, p0, Lcom/a/b/l/j;->a:[D

    iget v0, p0, Lcom/a/b/l/j;->b:I

    add-int v4, v0, p1

    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    aput-wide v6, v1, v4

    .line 1521
    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    .line 458
    return-object v0
.end method

.method public final size()I
    .registers 3

    .prologue
    .line 476
    iget v0, p0, Lcom/a/b/l/j;->c:I

    iget v1, p0, Lcom/a/b/l/j;->b:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public final subList(II)Ljava/util/List;
    .registers 7

    .prologue
    .line 525
    invoke-virtual {p0}, Lcom/a/b/l/j;->size()I

    move-result v0

    .line 526
    invoke-static {p1, p2, v0}, Lcom/a/b/b/cn;->a(III)V

    .line 527
    if-ne p1, p2, :cond_e

    .line 528
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 530
    :goto_d
    return-object v0

    :cond_e
    new-instance v0, Lcom/a/b/l/j;

    iget-object v1, p0, Lcom/a/b/l/j;->a:[D

    iget v2, p0, Lcom/a/b/l/j;->b:I

    add-int/2addr v2, p1

    iget v3, p0, Lcom/a/b/l/j;->b:I

    add-int/2addr v3, p2

    invoke-direct {v0, v1, v2, v3}, Lcom/a/b/l/j;-><init>([DII)V

    goto :goto_d
.end method

.method public final toString()Ljava/lang/String;
    .registers 7

    .prologue
    .line 562
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/a/b/l/j;->size()I

    move-result v0

    mul-int/lit8 v0, v0, 0xc

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 563
    const/16 v0, 0x5b

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/a/b/l/j;->a:[D

    iget v3, p0, Lcom/a/b/l/j;->b:I

    aget-wide v2, v2, v3

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 564
    iget v0, p0, Lcom/a/b/l/j;->b:I

    add-int/lit8 v0, v0, 0x1

    :goto_1e
    iget v2, p0, Lcom/a/b/l/j;->c:I

    if-ge v0, v2, :cond_32

    .line 565
    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/a/b/l/j;->a:[D

    aget-wide v4, v3, v0

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 564
    add-int/lit8 v0, v0, 0x1

    goto :goto_1e

    .line 567
    :cond_32
    const/16 v0, 0x5d

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
