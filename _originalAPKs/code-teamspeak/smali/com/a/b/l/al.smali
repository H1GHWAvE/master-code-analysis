.class public final Lcom/a/b/l/al;
.super Ljava/lang/Number;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation build Lcom/a/b/a/b;
    b = true
.end annotation


# static fields
.field public static final a:Lcom/a/b/l/al;

.field public static final b:Lcom/a/b/l/al;

.field public static final c:Lcom/a/b/l/al;


# instance fields
.field private final d:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 46
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/a/b/l/al;->a(I)Lcom/a/b/l/al;

    move-result-object v0

    sput-object v0, Lcom/a/b/l/al;->a:Lcom/a/b/l/al;

    .line 47
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/a/b/l/al;->a(I)Lcom/a/b/l/al;

    move-result-object v0

    sput-object v0, Lcom/a/b/l/al;->b:Lcom/a/b/l/al;

    .line 48
    const/4 v0, -0x1

    invoke-static {v0}, Lcom/a/b/l/al;->a(I)Lcom/a/b/l/al;

    move-result-object v0

    sput-object v0, Lcom/a/b/l/al;->c:Lcom/a/b/l/al;

    return-void
.end method

.method private constructor <init>(I)V
    .registers 3

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Number;-><init>()V

    .line 54
    and-int/lit8 v0, p1, -0x1

    iput v0, p0, Lcom/a/b/l/al;->d:I

    .line 55
    return-void
.end method

.method private static a(I)Lcom/a/b/l/al;
    .registers 2

    .prologue
    .line 70
    new-instance v0, Lcom/a/b/l/al;

    invoke-direct {v0, p0}, Lcom/a/b/l/al;-><init>(I)V

    return-object v0
.end method

.method private static a(J)Lcom/a/b/l/al;
    .registers 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 78
    const-wide v4, 0xffffffffL

    and-long/2addr v4, p0

    cmp-long v0, v4, p0

    if-nez v0, :cond_20

    move v0, v1

    :goto_d
    const-string v3, "value (%s) is outside the range for an unsigned integer value"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v1, v2

    invoke-static {v0, v3, v1}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 80
    long-to-int v0, p0

    invoke-static {v0}, Lcom/a/b/l/al;->a(I)Lcom/a/b/l/al;

    move-result-object v0

    return-object v0

    :cond_20
    move v0, v2

    .line 78
    goto :goto_d
.end method

.method private a(Lcom/a/b/l/al;)Lcom/a/b/l/al;
    .registers 4
    .annotation runtime Ljavax/annotation/CheckReturnValue;
    .end annotation

    .prologue
    .line 126
    iget v1, p0, Lcom/a/b/l/al;->d:I

    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/l/al;

    iget v0, v0, Lcom/a/b/l/al;->d:I

    add-int/2addr v0, v1

    invoke-static {v0}, Lcom/a/b/l/al;->a(I)Lcom/a/b/l/al;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/String;)Lcom/a/b/l/al;
    .registers 2

    .prologue
    .line 104
    .line 1115
    const/16 v0, 0xa

    invoke-static {p0, v0}, Lcom/a/b/l/am;->a(Ljava/lang/String;I)I

    move-result v0

    invoke-static {v0}, Lcom/a/b/l/al;->a(I)Lcom/a/b/l/al;

    move-result-object v0

    .line 104
    return-object v0
.end method

.method private static a(Ljava/math/BigInteger;)Lcom/a/b/l/al;
    .registers 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 90
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 91
    invoke-virtual {p0}, Ljava/math/BigInteger;->signum()I

    move-result v0

    if-ltz v0, :cond_26

    invoke-virtual {p0}, Ljava/math/BigInteger;->bitLength()I

    move-result v0

    const/16 v3, 0x20

    if-gt v0, v3, :cond_26

    move v0, v1

    :goto_14
    const-string v3, "value (%s) is outside the range for an unsigned integer value"

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p0, v1, v2

    invoke-static {v0, v3, v1}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 93
    invoke-virtual {p0}, Ljava/math/BigInteger;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/a/b/l/al;->a(I)Lcom/a/b/l/al;

    move-result-object v0

    return-object v0

    :cond_26
    move v0, v2

    .line 91
    goto :goto_14
.end method

.method private a()Ljava/math/BigInteger;
    .registers 3

    .prologue
    .line 217
    invoke-virtual {p0}, Lcom/a/b/l/al;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v0

    return-object v0
.end method

.method private b(Lcom/a/b/l/al;)Lcom/a/b/l/al;
    .registers 4
    .annotation runtime Ljavax/annotation/CheckReturnValue;
    .end annotation

    .prologue
    .line 137
    iget v1, p0, Lcom/a/b/l/al;->d:I

    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/l/al;

    iget v0, v0, Lcom/a/b/l/al;->d:I

    sub-int v0, v1, v0

    invoke-static {v0}, Lcom/a/b/l/al;->a(I)Lcom/a/b/l/al;

    move-result-object v0

    return-object v0
.end method

.method private static b(Ljava/lang/String;)Lcom/a/b/l/al;
    .registers 2

    .prologue
    .line 115
    const/16 v0, 0xa

    invoke-static {p0, v0}, Lcom/a/b/l/am;->a(Ljava/lang/String;I)I

    move-result v0

    invoke-static {v0}, Lcom/a/b/l/al;->a(I)Lcom/a/b/l/al;

    move-result-object v0

    return-object v0
.end method

.method private b()Ljava/lang/String;
    .registers 2

    .prologue
    .line 259
    iget v0, p0, Lcom/a/b/l/al;->d:I

    invoke-static {v0}, Lcom/a/b/l/am;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private c(Lcom/a/b/l/al;)Lcom/a/b/l/al;
    .registers 4
    .annotation build Lcom/a/b/a/c;
        a = "Does not truncate correctly"
    .end annotation

    .annotation runtime Ljavax/annotation/CheckReturnValue;
    .end annotation

    .prologue
    .line 150
    iget v1, p0, Lcom/a/b/l/al;->d:I

    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/l/al;

    iget v0, v0, Lcom/a/b/l/al;->d:I

    mul-int/2addr v0, v1

    invoke-static {v0}, Lcom/a/b/l/al;->a(I)Lcom/a/b/l/al;

    move-result-object v0

    return-object v0
.end method

.method private d(Lcom/a/b/l/al;)Lcom/a/b/l/al;
    .registers 8
    .annotation runtime Ljavax/annotation/CheckReturnValue;
    .end annotation

    .prologue
    const-wide v4, 0xffffffffL

    .line 161
    iget v1, p0, Lcom/a/b/l/al;->d:I

    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/l/al;

    iget v0, v0, Lcom/a/b/l/al;->d:I

    .line 2075
    int-to-long v2, v1

    and-long/2addr v2, v4

    .line 3075
    int-to-long v0, v0

    and-long/2addr v0, v4

    .line 1181
    div-long v0, v2, v0

    long-to-int v0, v0

    .line 161
    invoke-static {v0}, Lcom/a/b/l/al;->a(I)Lcom/a/b/l/al;

    move-result-object v0

    return-object v0
.end method

.method private e(Lcom/a/b/l/al;)Lcom/a/b/l/al;
    .registers 8
    .annotation runtime Ljavax/annotation/CheckReturnValue;
    .end annotation

    .prologue
    const-wide v4, 0xffffffffL

    .line 172
    iget v1, p0, Lcom/a/b/l/al;->d:I

    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/l/al;

    iget v0, v0, Lcom/a/b/l/al;->d:I

    .line 4075
    int-to-long v2, v1

    and-long/2addr v2, v4

    .line 5075
    int-to-long v0, v0

    and-long/2addr v0, v4

    .line 3193
    rem-long v0, v2, v0

    long-to-int v0, v0

    .line 172
    invoke-static {v0}, Lcom/a/b/l/al;->a(I)Lcom/a/b/l/al;

    move-result-object v0

    return-object v0
.end method

.method private f(Lcom/a/b/l/al;)I
    .registers 4

    .prologue
    .line 227
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 228
    iget v0, p0, Lcom/a/b/l/al;->d:I

    iget v1, p1, Lcom/a/b/l/al;->d:I

    invoke-static {v0, v1}, Lcom/a/b/l/am;->a(II)I

    move-result v0

    return v0
.end method


# virtual methods
.method public final synthetic compareTo(Ljava/lang/Object;)I
    .registers 4

    .prologue
    .line 44
    check-cast p1, Lcom/a/b/l/al;

    .line 7227
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 7228
    iget v0, p0, Lcom/a/b/l/al;->d:I

    iget v1, p1, Lcom/a/b/l/al;->d:I

    invoke-static {v0, v1}, Lcom/a/b/l/am;->a(II)I

    move-result v0

    .line 44
    return v0
.end method

.method public final doubleValue()D
    .registers 3

    .prologue
    .line 210
    invoke-virtual {p0}, Lcom/a/b/l/al;->longValue()J

    move-result-wide v0

    long-to-double v0, v0

    return-wide v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .registers 5
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 238
    instance-of v1, p1, Lcom/a/b/l/al;

    if-eqz v1, :cond_e

    .line 239
    check-cast p1, Lcom/a/b/l/al;

    .line 240
    iget v1, p0, Lcom/a/b/l/al;->d:I

    iget v2, p1, Lcom/a/b/l/al;->d:I

    if-ne v1, v2, :cond_e

    const/4 v0, 0x1

    .line 242
    :cond_e
    return v0
.end method

.method public final floatValue()F
    .registers 3

    .prologue
    .line 201
    invoke-virtual {p0}, Lcom/a/b/l/al;->longValue()J

    move-result-wide v0

    long-to-float v0, v0

    return v0
.end method

.method public final hashCode()I
    .registers 2

    .prologue
    .line 233
    iget v0, p0, Lcom/a/b/l/al;->d:I

    return v0
.end method

.method public final intValue()I
    .registers 2

    .prologue
    .line 184
    iget v0, p0, Lcom/a/b/l/al;->d:I

    return v0
.end method

.method public final longValue()J
    .registers 5

    .prologue
    .line 192
    iget v0, p0, Lcom/a/b/l/al;->d:I

    .line 6075
    int-to-long v0, v0

    const-wide v2, 0xffffffffL

    and-long/2addr v0, v2

    .line 192
    return-wide v0
.end method

.method public final toString()Ljava/lang/String;
    .registers 2

    .prologue
    .line 250
    .line 6259
    iget v0, p0, Lcom/a/b/l/al;->d:I

    invoke-static {v0}, Lcom/a/b/l/am;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 250
    return-object v0
.end method
