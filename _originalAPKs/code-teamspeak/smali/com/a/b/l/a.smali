.class public final Lcom/a/b/l/a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/b;
.end annotation


# direct methods
.method private constructor <init>()V
    .registers 1

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Z)I
    .registers 2

    .prologue
    .line 60
    if-eqz p0, :cond_5

    const/16 v0, 0x4cf

    :goto_4
    return v0

    :cond_5
    const/16 v0, 0x4d5

    goto :goto_4
.end method

.method public static a(ZZ)I
    .registers 3

    .prologue
    .line 77
    if-ne p0, p1, :cond_4

    const/4 v0, 0x0

    :goto_3
    return v0

    :cond_4
    if-eqz p0, :cond_8

    const/4 v0, 0x1

    goto :goto_3

    :cond_8
    const/4 v0, -0x1

    goto :goto_3
.end method

.method static a([ZZII)I
    .registers 6

    .prologue
    .line 123
    move v0, p2

    :goto_1
    if-ge v0, p3, :cond_b

    .line 124
    aget-boolean v1, p0, v0

    if-ne v1, p1, :cond_8

    .line 128
    :goto_7
    return v0

    .line 123
    :cond_8
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 128
    :cond_b
    const/4 v0, -0x1

    goto :goto_7
.end method

.method private static a([Z[Z)I
    .registers 7

    .prologue
    const/4 v1, 0x0

    .line 143
    const-string v0, "array"

    invoke-static {p0, v0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 144
    const-string v0, "target"

    invoke-static {p1, v0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 145
    array-length v0, p1

    if-nez v0, :cond_f

    .line 158
    :goto_e
    return v1

    :cond_f
    move v0, v1

    .line 150
    :goto_10
    array-length v2, p0

    array-length v3, p1

    sub-int/2addr v2, v3

    add-int/lit8 v2, v2, 0x1

    if-ge v0, v2, :cond_2b

    move v2, v1

    .line 151
    :goto_18
    array-length v3, p1

    if-ge v2, v3, :cond_26

    .line 152
    add-int v3, v0, v2

    aget-boolean v3, p0, v3

    aget-boolean v4, p1, v2

    if-ne v3, v4, :cond_28

    .line 151
    add-int/lit8 v2, v2, 0x1

    goto :goto_18

    :cond_26
    move v1, v0

    .line 156
    goto :goto_e

    .line 150
    :cond_28
    add-int/lit8 v0, v0, 0x1

    goto :goto_10

    .line 158
    :cond_2b
    const/4 v1, -0x1

    goto :goto_e
.end method

.method private static varargs a(Ljava/lang/String;[Z)Ljava/lang/String;
    .registers 6

    .prologue
    .line 250
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 251
    array-length v0, p1

    if-nez v0, :cond_9

    .line 252
    const-string v0, ""

    .line 261
    :goto_8
    return-object v0

    .line 256
    :cond_9
    new-instance v1, Ljava/lang/StringBuilder;

    array-length v0, p1

    mul-int/lit8 v0, v0, 0x7

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 257
    const/4 v0, 0x0

    aget-boolean v0, p1, v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 258
    const/4 v0, 0x1

    :goto_18
    array-length v2, p1

    if-ge v0, v2, :cond_27

    .line 259
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-boolean v3, p1, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 258
    add-int/lit8 v0, v0, 0x1

    goto :goto_18

    .line 261
    :cond_27
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_8
.end method

.method private static a()Ljava/util/Comparator;
    .registers 1

    .prologue
    .line 281
    sget-object v0, Lcom/a/b/l/c;->a:Lcom/a/b/l/c;

    return-object v0
.end method

.method private static varargs a([Z)Ljava/util/List;
    .registers 2

    .prologue
    .line 347
    array-length v0, p0

    if-nez v0, :cond_8

    .line 348
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 350
    :goto_7
    return-object v0

    :cond_8
    new-instance v0, Lcom/a/b/l/b;

    invoke-direct {v0, p0}, Lcom/a/b/l/b;-><init>([Z)V

    goto :goto_7
.end method

.method private static a([ZZ)Z
    .registers 6

    .prologue
    const/4 v0, 0x0

    .line 95
    array-length v2, p0

    move v1, v0

    :goto_3
    if-ge v1, v2, :cond_a

    aget-boolean v3, p0, v1

    .line 96
    if-ne v3, p1, :cond_b

    .line 97
    const/4 v0, 0x1

    .line 100
    :cond_a
    return v0

    .line 95
    :cond_b
    add-int/lit8 v1, v1, 0x1

    goto :goto_3
.end method

.method private static a(Ljava/util/Collection;)[Z
    .registers 6

    .prologue
    const/4 v2, 0x0

    .line 318
    instance-of v0, p0, Lcom/a/b/l/b;

    if-eqz v0, :cond_15

    .line 319
    check-cast p0, Lcom/a/b/l/b;

    .line 1467
    invoke-virtual {p0}, Lcom/a/b/l/b;->size()I

    move-result v1

    .line 1468
    new-array v0, v1, [Z

    .line 1469
    iget-object v3, p0, Lcom/a/b/l/b;->a:[Z

    iget v4, p0, Lcom/a/b/l/b;->b:I

    invoke-static {v3, v4, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 329
    :goto_14
    return-object v0

    .line 322
    :cond_15
    invoke-interface {p0}, Ljava/util/Collection;->toArray()[Ljava/lang/Object;

    move-result-object v3

    .line 323
    array-length v4, v3

    .line 324
    new-array v1, v4, [Z

    .line 325
    :goto_1c
    if-ge v2, v4, :cond_30

    .line 327
    aget-object v0, v3, v2

    invoke-static {v0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    aput-boolean v0, v1, v2

    .line 325
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1c

    :cond_30
    move-object v0, v1

    .line 329
    goto :goto_14
.end method

.method private static a([ZI)[Z
    .registers 5

    .prologue
    const/4 v2, 0x0

    .line 235
    new-array v0, p1, [Z

    .line 236
    array-length v1, p0

    invoke-static {v1, p1}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {p0, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 237
    return-object v0
.end method

.method private static a([ZII)[Z
    .registers 9

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 226
    if-ltz p1, :cond_33

    move v0, v1

    :goto_5
    const-string v3, "Invalid minLength: %s"

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v0, v3, v4}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 227
    if-ltz p2, :cond_35

    move v0, v1

    :goto_15
    const-string v3, "Invalid padding: %s"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v2

    invoke-static {v0, v3, v1}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 228
    array-length v0, p0

    if-ge v0, p1, :cond_32

    add-int v1, p1, p2

    .line 1235
    new-array v0, v1, [Z

    .line 1236
    array-length v3, p0

    invoke-static {v3, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {p0, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move-object p0, v0

    .line 228
    :cond_32
    return-object p0

    :cond_33
    move v0, v2

    .line 226
    goto :goto_5

    :cond_35
    move v0, v2

    .line 227
    goto :goto_15
.end method

.method private static varargs a([[Z)[Z
    .registers 8

    .prologue
    const/4 v1, 0x0

    .line 195
    .line 196
    array-length v3, p0

    move v0, v1

    move v2, v1

    :goto_4
    if-ge v0, v3, :cond_d

    aget-object v4, p0, v0

    .line 197
    array-length v4, v4

    add-int/2addr v2, v4

    .line 196
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 199
    :cond_d
    new-array v3, v2, [Z

    .line 201
    array-length v4, p0

    move v0, v1

    move v2, v1

    :goto_12
    if-ge v2, v4, :cond_1f

    aget-object v5, p0, v2

    .line 202
    array-length v6, v5

    invoke-static {v5, v1, v3, v0, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 203
    array-length v5, v5

    add-int/2addr v0, v5

    .line 201
    add-int/lit8 v2, v2, 0x1

    goto :goto_12

    .line 205
    :cond_1f
    return-object v3
.end method

.method private static varargs b([Z)I
    .registers 5
    .annotation build Lcom/a/b/a/a;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 483
    .line 484
    array-length v2, p0

    move v1, v0

    :goto_3
    if-ge v1, v2, :cond_e

    aget-boolean v3, p0, v1

    .line 485
    if-eqz v3, :cond_b

    .line 486
    add-int/lit8 v0, v0, 0x1

    .line 484
    :cond_b
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 489
    :cond_e
    return v0
.end method

.method private static b([ZZ)I
    .registers 4

    .prologue
    .line 117
    const/4 v0, 0x0

    array-length v1, p0

    invoke-static {p0, p1, v0, v1}, Lcom/a/b/l/a;->a([ZZII)I

    move-result v0

    return v0
.end method

.method static b([ZZII)I
    .registers 6

    .prologue
    .line 177
    add-int/lit8 v0, p3, -0x1

    :goto_2
    if-lt v0, p2, :cond_c

    .line 178
    aget-boolean v1, p0, v0

    if-ne v1, p1, :cond_9

    .line 182
    :goto_8
    return v0

    .line 177
    :cond_9
    add-int/lit8 v0, v0, -0x1

    goto :goto_2

    .line 182
    :cond_c
    const/4 v0, -0x1

    goto :goto_8
.end method

.method private static c([ZZ)I
    .registers 4

    .prologue
    .line 171
    const/4 v0, 0x0

    array-length v1, p0

    invoke-static {p0, p1, v0, v1}, Lcom/a/b/l/a;->b([ZZII)I

    move-result v0

    return v0
.end method

.method private static synthetic c([ZZII)I
    .registers 5

    .prologue
    .line 49
    invoke-static {p0, p1, p2, p3}, Lcom/a/b/l/a;->a([ZZII)I

    move-result v0

    return v0
.end method

.method private static synthetic d([ZZII)I
    .registers 5

    .prologue
    .line 49
    invoke-static {p0, p1, p2, p3}, Lcom/a/b/l/a;->b([ZZII)I

    move-result v0

    return v0
.end method
