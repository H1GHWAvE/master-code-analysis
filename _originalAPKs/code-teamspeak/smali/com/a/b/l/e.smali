.class final Lcom/a/b/l/e;
.super Ljava/util/AbstractList;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/util/RandomAccess;


# annotations
.annotation build Lcom/a/b/a/b;
.end annotation


# static fields
.field private static final d:J


# instance fields
.field final a:[B

.field final b:I

.field final c:I


# direct methods
.method constructor <init>([B)V
    .registers 4

    .prologue
    .line 275
    const/4 v0, 0x0

    array-length v1, p1

    invoke-direct {p0, p1, v0, v1}, Lcom/a/b/l/e;-><init>([BII)V

    .line 276
    return-void
.end method

.method private constructor <init>([BII)V
    .registers 4

    .prologue
    .line 278
    invoke-direct {p0}, Ljava/util/AbstractList;-><init>()V

    .line 279
    iput-object p1, p0, Lcom/a/b/l/e;->a:[B

    .line 280
    iput p2, p0, Lcom/a/b/l/e;->b:I

    .line 281
    iput p3, p0, Lcom/a/b/l/e;->c:I

    .line 282
    return-void
.end method

.method private a(I)Ljava/lang/Byte;
    .registers 4

    .prologue
    .line 293
    invoke-virtual {p0}, Lcom/a/b/l/e;->size()I

    move-result v0

    invoke-static {p1, v0}, Lcom/a/b/b/cn;->a(II)I

    .line 294
    iget-object v0, p0, Lcom/a/b/l/e;->a:[B

    iget v1, p0, Lcom/a/b/l/e;->b:I

    add-int/2addr v1, p1

    aget-byte v0, v0, v1

    invoke-static {v0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    return-object v0
.end method

.method private a(ILjava/lang/Byte;)Ljava/lang/Byte;
    .registers 7

    .prologue
    .line 326
    invoke-virtual {p0}, Lcom/a/b/l/e;->size()I

    move-result v0

    invoke-static {p1, v0}, Lcom/a/b/b/cn;->a(II)I

    .line 327
    iget-object v0, p0, Lcom/a/b/l/e;->a:[B

    iget v1, p0, Lcom/a/b/l/e;->b:I

    add-int/2addr v1, p1

    aget-byte v1, v0, v1

    .line 329
    iget-object v2, p0, Lcom/a/b/l/e;->a:[B

    iget v0, p0, Lcom/a/b/l/e;->b:I

    add-int v3, v0, p1

    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Byte;

    invoke-virtual {v0}, Ljava/lang/Byte;->byteValue()B

    move-result v0

    aput-byte v0, v2, v3

    .line 330
    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    return-object v0
.end method

.method private a()[B
    .registers 6

    .prologue
    .line 381
    invoke-virtual {p0}, Lcom/a/b/l/e;->size()I

    move-result v0

    .line 382
    new-array v1, v0, [B

    .line 383
    iget-object v2, p0, Lcom/a/b/l/e;->a:[B

    iget v3, p0, Lcom/a/b/l/e;->b:I

    const/4 v4, 0x0

    invoke-static {v2, v3, v1, v4, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 384
    return-object v1
.end method


# virtual methods
.method public final contains(Ljava/lang/Object;)Z
    .registers 6

    .prologue
    .line 299
    instance-of v0, p1, Ljava/lang/Byte;

    if-eqz v0, :cond_19

    iget-object v0, p0, Lcom/a/b/l/e;->a:[B

    check-cast p1, Ljava/lang/Byte;

    invoke-virtual {p1}, Ljava/lang/Byte;->byteValue()B

    move-result v1

    iget v2, p0, Lcom/a/b/l/e;->b:I

    iget v3, p0, Lcom/a/b/l/e;->c:I

    .line 1051
    invoke-static {v0, v1, v2, v3}, Lcom/a/b/l/d;->a([BBII)I

    move-result v0

    .line 299
    const/4 v1, -0x1

    if-eq v0, v1, :cond_19

    const/4 v0, 0x1

    :goto_18
    return v0

    :cond_19
    const/4 v0, 0x0

    goto :goto_18
.end method

.method public final equals(Ljava/lang/Object;)Z
    .registers 9

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 343
    if-ne p1, p0, :cond_5

    .line 359
    :cond_4
    :goto_4
    return v0

    .line 346
    :cond_5
    instance-of v2, p1, Lcom/a/b/l/e;

    if-eqz v2, :cond_2f

    .line 347
    check-cast p1, Lcom/a/b/l/e;

    .line 348
    invoke-virtual {p0}, Lcom/a/b/l/e;->size()I

    move-result v3

    .line 349
    invoke-virtual {p1}, Lcom/a/b/l/e;->size()I

    move-result v2

    if-eq v2, v3, :cond_17

    move v0, v1

    .line 350
    goto :goto_4

    :cond_17
    move v2, v1

    .line 352
    :goto_18
    if-ge v2, v3, :cond_4

    .line 353
    iget-object v4, p0, Lcom/a/b/l/e;->a:[B

    iget v5, p0, Lcom/a/b/l/e;->b:I

    add-int/2addr v5, v2

    aget-byte v4, v4, v5

    iget-object v5, p1, Lcom/a/b/l/e;->a:[B

    iget v6, p1, Lcom/a/b/l/e;->b:I

    add-int/2addr v6, v2

    aget-byte v5, v5, v6

    if-eq v4, v5, :cond_2c

    move v0, v1

    .line 354
    goto :goto_4

    .line 352
    :cond_2c
    add-int/lit8 v2, v2, 0x1

    goto :goto_18

    .line 359
    :cond_2f
    invoke-super {p0, p1}, Ljava/util/AbstractList;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_4
.end method

.method public final synthetic get(I)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 267
    .line 4293
    invoke-virtual {p0}, Lcom/a/b/l/e;->size()I

    move-result v0

    invoke-static {p1, v0}, Lcom/a/b/b/cn;->a(II)I

    .line 4294
    iget-object v0, p0, Lcom/a/b/l/e;->a:[B

    iget v1, p0, Lcom/a/b/l/e;->b:I

    add-int/2addr v1, p1

    aget-byte v0, v0, v1

    invoke-static {v0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    .line 267
    return-object v0
.end method

.method public final hashCode()I
    .registers 4

    .prologue
    .line 363
    const/4 v1, 0x1

    .line 364
    iget v0, p0, Lcom/a/b/l/e;->b:I

    :goto_3
    iget v2, p0, Lcom/a/b/l/e;->c:I

    if-ge v0, v2, :cond_11

    .line 365
    mul-int/lit8 v1, v1, 0x1f

    iget-object v2, p0, Lcom/a/b/l/e;->a:[B

    aget-byte v2, v2, v0

    add-int/2addr v1, v2

    .line 364
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 367
    :cond_11
    return v1
.end method

.method public final indexOf(Ljava/lang/Object;)I
    .registers 6

    .prologue
    .line 305
    instance-of v0, p1, Ljava/lang/Byte;

    if-eqz v0, :cond_1a

    .line 306
    iget-object v0, p0, Lcom/a/b/l/e;->a:[B

    check-cast p1, Ljava/lang/Byte;

    invoke-virtual {p1}, Ljava/lang/Byte;->byteValue()B

    move-result v1

    iget v2, p0, Lcom/a/b/l/e;->b:I

    iget v3, p0, Lcom/a/b/l/e;->c:I

    .line 2051
    invoke-static {v0, v1, v2, v3}, Lcom/a/b/l/d;->a([BBII)I

    move-result v0

    .line 307
    if-ltz v0, :cond_1a

    .line 308
    iget v1, p0, Lcom/a/b/l/e;->b:I

    sub-int/2addr v0, v1

    .line 311
    :goto_19
    return v0

    :cond_1a
    const/4 v0, -0x1

    goto :goto_19
.end method

.method public final isEmpty()Z
    .registers 2

    .prologue
    .line 289
    const/4 v0, 0x0

    return v0
.end method

.method public final lastIndexOf(Ljava/lang/Object;)I
    .registers 6

    .prologue
    .line 316
    instance-of v0, p1, Ljava/lang/Byte;

    if-eqz v0, :cond_1a

    .line 317
    iget-object v0, p0, Lcom/a/b/l/e;->a:[B

    check-cast p1, Ljava/lang/Byte;

    invoke-virtual {p1}, Ljava/lang/Byte;->byteValue()B

    move-result v1

    iget v2, p0, Lcom/a/b/l/e;->b:I

    iget v3, p0, Lcom/a/b/l/e;->c:I

    .line 3051
    invoke-static {v0, v1, v2, v3}, Lcom/a/b/l/d;->b([BBII)I

    move-result v0

    .line 318
    if-ltz v0, :cond_1a

    .line 319
    iget v1, p0, Lcom/a/b/l/e;->b:I

    sub-int/2addr v0, v1

    .line 322
    :goto_19
    return v0

    :cond_1a
    const/4 v0, -0x1

    goto :goto_19
.end method

.method public final synthetic set(ILjava/lang/Object;)Ljava/lang/Object;
    .registers 7

    .prologue
    .line 267
    check-cast p2, Ljava/lang/Byte;

    .line 3326
    invoke-virtual {p0}, Lcom/a/b/l/e;->size()I

    move-result v0

    invoke-static {p1, v0}, Lcom/a/b/b/cn;->a(II)I

    .line 3327
    iget-object v0, p0, Lcom/a/b/l/e;->a:[B

    iget v1, p0, Lcom/a/b/l/e;->b:I

    add-int/2addr v1, p1

    aget-byte v1, v0, v1

    .line 3329
    iget-object v2, p0, Lcom/a/b/l/e;->a:[B

    iget v0, p0, Lcom/a/b/l/e;->b:I

    add-int v3, v0, p1

    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Byte;

    invoke-virtual {v0}, Ljava/lang/Byte;->byteValue()B

    move-result v0

    aput-byte v0, v2, v3

    .line 3330
    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    .line 267
    return-object v0
.end method

.method public final size()I
    .registers 3

    .prologue
    .line 285
    iget v0, p0, Lcom/a/b/l/e;->c:I

    iget v1, p0, Lcom/a/b/l/e;->b:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public final subList(II)Ljava/util/List;
    .registers 7

    .prologue
    .line 334
    invoke-virtual {p0}, Lcom/a/b/l/e;->size()I

    move-result v0

    .line 335
    invoke-static {p1, p2, v0}, Lcom/a/b/b/cn;->a(III)V

    .line 336
    if-ne p1, p2, :cond_e

    .line 337
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 339
    :goto_d
    return-object v0

    :cond_e
    new-instance v0, Lcom/a/b/l/e;

    iget-object v1, p0, Lcom/a/b/l/e;->a:[B

    iget v2, p0, Lcom/a/b/l/e;->b:I

    add-int/2addr v2, p1

    iget v3, p0, Lcom/a/b/l/e;->b:I

    add-int/2addr v3, p2

    invoke-direct {v0, v1, v2, v3}, Lcom/a/b/l/e;-><init>([BII)V

    goto :goto_d
.end method

.method public final toString()Ljava/lang/String;
    .registers 5

    .prologue
    .line 371
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/a/b/l/e;->size()I

    move-result v0

    mul-int/lit8 v0, v0, 0x5

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 372
    const/16 v0, 0x5b

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/a/b/l/e;->a:[B

    iget v3, p0, Lcom/a/b/l/e;->b:I

    aget-byte v2, v2, v3

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 373
    iget v0, p0, Lcom/a/b/l/e;->b:I

    add-int/lit8 v0, v0, 0x1

    :goto_1e
    iget v2, p0, Lcom/a/b/l/e;->c:I

    if-ge v0, v2, :cond_32

    .line 374
    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/a/b/l/e;->a:[B

    aget-byte v3, v3, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 373
    add-int/lit8 v0, v0, 0x1

    goto :goto_1e

    .line 376
    :cond_32
    const/16 v0, 0x5d

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
