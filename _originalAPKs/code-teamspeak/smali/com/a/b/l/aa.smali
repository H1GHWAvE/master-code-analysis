.class public final Lcom/a/b/l/aa;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/b;
    b = true
.end annotation


# static fields
.field public static final a:I = 0x2

.field public static final b:S = 0x4000s


# direct methods
.method private constructor <init>()V
    .registers 1

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(S)I
    .registers 1

    .prologue
    .line 74
    return p0
.end method

.method private static a(SS)I
    .registers 3

    .prologue
    .line 126
    sub-int v0, p0, p1

    return v0
.end method

.method static a([SSII)I
    .registers 6

    .prologue
    .line 163
    move v0, p2

    :goto_1
    if-ge v0, p3, :cond_b

    .line 164
    aget-short v1, p0, v0

    if-ne v1, p1, :cond_8

    .line 168
    :goto_7
    return v0

    .line 163
    :cond_8
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 168
    :cond_b
    const/4 v0, -0x1

    goto :goto_7
.end method

.method private static a([S[S)I
    .registers 7

    .prologue
    const/4 v1, 0x0

    .line 183
    const-string v0, "array"

    invoke-static {p0, v0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 184
    const-string v0, "target"

    invoke-static {p1, v0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 185
    array-length v0, p1

    if-nez v0, :cond_f

    .line 198
    :goto_e
    return v1

    :cond_f
    move v0, v1

    .line 190
    :goto_10
    array-length v2, p0

    array-length v3, p1

    sub-int/2addr v2, v3

    add-int/lit8 v2, v2, 0x1

    if-ge v0, v2, :cond_2b

    move v2, v1

    .line 191
    :goto_18
    array-length v3, p1

    if-ge v2, v3, :cond_26

    .line 192
    add-int v3, v0, v2

    aget-short v3, p0, v3

    aget-short v4, p1, v2

    if-ne v3, v4, :cond_28

    .line 191
    add-int/lit8 v2, v2, 0x1

    goto :goto_18

    :cond_26
    move v1, v0

    .line 196
    goto :goto_e

    .line 190
    :cond_28
    add-int/lit8 v0, v0, 0x1

    goto :goto_10

    .line 198
    :cond_2b
    const/4 v1, -0x1

    goto :goto_e
.end method

.method private static a()Lcom/a/b/b/ak;
    .registers 1
    .annotation build Lcom/a/b/a/a;
    .end annotation

    .prologue
    .line 369
    sget-object v0, Lcom/a/b/l/ad;->a:Lcom/a/b/l/ad;

    return-object v0
.end method

.method private static varargs a(Ljava/lang/String;[S)Ljava/lang/String;
    .registers 6

    .prologue
    .line 414
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 415
    array-length v0, p1

    if-nez v0, :cond_9

    .line 416
    const-string v0, ""

    .line 425
    :goto_8
    return-object v0

    .line 420
    :cond_9
    new-instance v1, Ljava/lang/StringBuilder;

    array-length v0, p1

    mul-int/lit8 v0, v0, 0x6

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 421
    const/4 v0, 0x0

    aget-short v0, p1, v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 422
    const/4 v0, 0x1

    :goto_18
    array-length v2, p1

    if-ge v0, v2, :cond_27

    .line 423
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-short v3, p1, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 422
    add-int/lit8 v0, v0, 0x1

    goto :goto_18

    .line 425
    :cond_27
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_8
.end method

.method private static a(BB)S
    .registers 4
    .annotation build Lcom/a/b/a/c;
        a = "doesn\'t work"
    .end annotation

    .prologue
    .line 333
    shl-int/lit8 v0, p0, 0x8

    and-int/lit16 v1, p1, 0xff

    or-int/2addr v0, v1

    int-to-short v0, v0

    return v0
.end method

.method private static a(J)S
    .registers 6

    .prologue
    .line 87
    long-to-int v0, p0

    int-to-short v0, v0

    .line 88
    int-to-long v2, v0

    cmp-long v1, v2, p0

    if-eqz v1, :cond_22

    .line 90
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x22

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Out of range: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 92
    :cond_22
    return v0
.end method

.method private static a([B)S
    .registers 8
    .annotation build Lcom/a/b/a/c;
        a = "doesn\'t work"
    .end annotation

    .prologue
    const/4 v6, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 319
    array-length v0, p0

    if-lt v0, v6, :cond_26

    move v0, v1

    :goto_7
    const-string v3, "array too small: %s < %s"

    new-array v4, v6, [Ljava/lang/Object;

    array-length v5, p0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-static {v0, v3, v4}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 321
    aget-byte v0, p0, v2

    aget-byte v1, p0, v1

    .line 1333
    shl-int/lit8 v0, v0, 0x8

    and-int/lit16 v1, v1, 0xff

    or-int/2addr v0, v1

    int-to-short v0, v0

    .line 321
    return v0

    :cond_26
    move v0, v2

    .line 319
    goto :goto_7
.end method

.method private static varargs a([S)S
    .registers 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 234
    array-length v0, p0

    if-lez v0, :cond_17

    move v0, v1

    :goto_6
    invoke-static {v0}, Lcom/a/b/b/cn;->a(Z)V

    .line 235
    aget-short v0, p0, v2

    .line 236
    :goto_b
    array-length v2, p0

    if-ge v1, v2, :cond_19

    .line 237
    aget-short v2, p0, v1

    if-ge v2, v0, :cond_14

    .line 238
    aget-short v0, p0, v1

    .line 236
    :cond_14
    add-int/lit8 v1, v1, 0x1

    goto :goto_b

    :cond_17
    move v0, v2

    .line 234
    goto :goto_6

    .line 241
    :cond_19
    return v0
.end method

.method private static a([SS)Z
    .registers 6

    .prologue
    const/4 v0, 0x0

    .line 139
    array-length v2, p0

    move v1, v0

    :goto_3
    if-ge v1, v2, :cond_a

    aget-short v3, p0, v1

    .line 140
    if-ne v3, p1, :cond_b

    .line 141
    const/4 v0, 0x1

    .line 144
    :cond_a
    return v0

    .line 139
    :cond_b
    add-int/lit8 v1, v1, 0x1

    goto :goto_3
.end method

.method private static a(Ljava/util/Collection;)[S
    .registers 6

    .prologue
    const/4 v2, 0x0

    .line 480
    instance-of v0, p0, Lcom/a/b/l/ac;

    if-eqz v0, :cond_15

    .line 481
    check-cast p0, Lcom/a/b/l/ac;

    .line 1629
    invoke-virtual {p0}, Lcom/a/b/l/ac;->size()I

    move-result v1

    .line 1630
    new-array v0, v1, [S

    .line 1631
    iget-object v3, p0, Lcom/a/b/l/ac;->a:[S

    iget v4, p0, Lcom/a/b/l/ac;->b:I

    invoke-static {v3, v4, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 491
    :goto_14
    return-object v0

    .line 484
    :cond_15
    invoke-interface {p0}, Ljava/util/Collection;->toArray()[Ljava/lang/Object;

    move-result-object v3

    .line 485
    array-length v4, v3

    .line 486
    new-array v1, v4, [S

    .line 487
    :goto_1c
    if-ge v2, v4, :cond_30

    .line 489
    aget-object v0, v3, v2

    invoke-static {v0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->shortValue()S

    move-result v0

    aput-short v0, v1, v2

    .line 487
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1c

    :cond_30
    move-object v0, v1

    .line 491
    goto :goto_14
.end method

.method private static a([SI)[S
    .registers 5

    .prologue
    const/4 v2, 0x0

    .line 399
    new-array v0, p1, [S

    .line 400
    array-length v1, p0

    invoke-static {v1, p1}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {p0, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 401
    return-object v0
.end method

.method private static a([SII)[S
    .registers 9

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 390
    if-ltz p1, :cond_33

    move v0, v1

    :goto_5
    const-string v3, "Invalid minLength: %s"

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v0, v3, v4}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 391
    if-ltz p2, :cond_35

    move v0, v1

    :goto_15
    const-string v3, "Invalid padding: %s"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v2

    invoke-static {v0, v3, v1}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 392
    array-length v0, p0

    if-ge v0, p1, :cond_32

    add-int v1, p1, p2

    .line 1399
    new-array v0, v1, [S

    .line 1400
    array-length v3, p0

    invoke-static {v3, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {p0, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move-object p0, v0

    .line 392
    :cond_32
    return-object p0

    :cond_33
    move v0, v2

    .line 390
    goto :goto_5

    :cond_35
    move v0, v2

    .line 391
    goto :goto_15
.end method

.method private static varargs a([[S)[S
    .registers 8

    .prologue
    const/4 v1, 0x0

    .line 273
    .line 274
    array-length v3, p0

    move v0, v1

    move v2, v1

    :goto_4
    if-ge v0, v3, :cond_d

    aget-object v4, p0, v0

    .line 275
    array-length v4, v4

    add-int/2addr v2, v4

    .line 274
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 277
    :cond_d
    new-array v3, v2, [S

    .line 279
    array-length v4, p0

    move v0, v1

    move v2, v1

    :goto_12
    if-ge v2, v4, :cond_1f

    aget-object v5, p0, v2

    .line 280
    array-length v6, v5

    invoke-static {v5, v1, v3, v0, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 281
    array-length v5, v5

    add-int/2addr v0, v5

    .line 279
    add-int/lit8 v2, v2, 0x1

    goto :goto_12

    .line 283
    :cond_1f
    return-object v3
.end method

.method private static b([SS)I
    .registers 4

    .prologue
    .line 157
    const/4 v0, 0x0

    array-length v1, p0

    invoke-static {p0, p1, v0, v1}, Lcom/a/b/l/aa;->a([SSII)I

    move-result v0

    return v0
.end method

.method static b([SSII)I
    .registers 6

    .prologue
    .line 217
    add-int/lit8 v0, p3, -0x1

    :goto_2
    if-lt v0, p2, :cond_c

    .line 218
    aget-short v1, p0, v0

    if-ne v1, p1, :cond_9

    .line 222
    :goto_8
    return v0

    .line 217
    :cond_9
    add-int/lit8 v0, v0, -0x1

    goto :goto_2

    .line 222
    :cond_c
    const/4 v0, -0x1

    goto :goto_8
.end method

.method private static b()Ljava/util/Comparator;
    .registers 1

    .prologue
    .line 445
    sget-object v0, Lcom/a/b/l/ab;->a:Lcom/a/b/l/ab;

    return-object v0
.end method

.method private static b(J)S
    .registers 4

    .prologue
    .line 104
    const-wide/16 v0, 0x7fff

    cmp-long v0, p0, v0

    if-lez v0, :cond_9

    .line 105
    const/16 v0, 0x7fff

    .line 110
    :goto_8
    return v0

    .line 107
    :cond_9
    const-wide/16 v0, -0x8000

    cmp-long v0, p0, v0

    if-gez v0, :cond_12

    .line 108
    const/16 v0, -0x8000

    goto :goto_8

    .line 110
    :cond_12
    long-to-int v0, p0

    int-to-short v0, v0

    goto :goto_8
.end method

.method private static varargs b([S)S
    .registers 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 253
    array-length v0, p0

    if-lez v0, :cond_17

    move v0, v1

    :goto_6
    invoke-static {v0}, Lcom/a/b/b/cn;->a(Z)V

    .line 254
    aget-short v0, p0, v2

    .line 255
    :goto_b
    array-length v2, p0

    if-ge v1, v2, :cond_19

    .line 256
    aget-short v2, p0, v1

    if-le v2, v0, :cond_14

    .line 257
    aget-short v0, p0, v1

    .line 255
    :cond_14
    add-int/lit8 v1, v1, 0x1

    goto :goto_b

    :cond_17
    move v0, v2

    .line 253
    goto :goto_6

    .line 260
    :cond_19
    return v0
.end method

.method private static b(S)[B
    .registers 4
    .annotation build Lcom/a/b/a/c;
        a = "doesn\'t work"
    .end annotation

    .prologue
    .line 300
    const/4 v0, 0x2

    new-array v0, v0, [B

    const/4 v1, 0x0

    shr-int/lit8 v2, p0, 0x8

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    const/4 v1, 0x1

    int-to-byte v2, p0

    aput-byte v2, v0, v1

    return-object v0
.end method

.method private static c([SS)I
    .registers 4

    .prologue
    .line 211
    const/4 v0, 0x0

    array-length v1, p0

    invoke-static {p0, p1, v0, v1}, Lcom/a/b/l/aa;->b([SSII)I

    move-result v0

    return v0
.end method

.method private static synthetic c([SSII)I
    .registers 5

    .prologue
    .line 50
    invoke-static {p0, p1, p2, p3}, Lcom/a/b/l/aa;->a([SSII)I

    move-result v0

    return v0
.end method

.method private static varargs c([S)Ljava/util/List;
    .registers 2

    .prologue
    .line 509
    array-length v0, p0

    if-nez v0, :cond_8

    .line 510
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 512
    :goto_7
    return-object v0

    :cond_8
    new-instance v0, Lcom/a/b/l/ac;

    invoke-direct {v0, p0}, Lcom/a/b/l/ac;-><init>([S)V

    goto :goto_7
.end method

.method private static synthetic d([SSII)I
    .registers 5

    .prologue
    .line 50
    invoke-static {p0, p1, p2, p3}, Lcom/a/b/l/aa;->b([SSII)I

    move-result v0

    return v0
.end method
