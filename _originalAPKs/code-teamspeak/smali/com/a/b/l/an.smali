.class final enum Lcom/a/b/l/an;
.super Ljava/lang/Enum;
.source "SourceFile"

# interfaces
.implements Ljava/util/Comparator;


# static fields
.field public static final enum a:Lcom/a/b/l/an;

.field private static final synthetic b:[Lcom/a/b/l/an;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    .line 158
    new-instance v0, Lcom/a/b/l/an;

    const-string v1, "INSTANCE"

    invoke-direct {v0, v1}, Lcom/a/b/l/an;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/a/b/l/an;->a:Lcom/a/b/l/an;

    .line 157
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/a/b/l/an;

    const/4 v1, 0x0

    sget-object v2, Lcom/a/b/l/an;->a:Lcom/a/b/l/an;

    aput-object v2, v0, v1

    sput-object v0, Lcom/a/b/l/an;->b:[Lcom/a/b/l/an;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .registers 3

    .prologue
    .line 157
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method private static a([I[I)I
    .registers 6

    .prologue
    .line 162
    array-length v0, p0

    array-length v1, p1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 163
    const/4 v0, 0x0

    :goto_7
    if-ge v0, v1, :cond_1b

    .line 164
    aget v2, p0, v0

    aget v3, p1, v0

    if-eq v2, v3, :cond_18

    .line 165
    aget v1, p0, v0

    aget v0, p1, v0

    invoke-static {v1, v0}, Lcom/a/b/l/am;->a(II)I

    move-result v0

    .line 168
    :goto_17
    return v0

    .line 163
    :cond_18
    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    .line 168
    :cond_1b
    array-length v0, p0

    array-length v1, p1

    sub-int/2addr v0, v1

    goto :goto_17
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/a/b/l/an;
    .registers 2

    .prologue
    .line 157
    const-class v0, Lcom/a/b/l/an;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/a/b/l/an;

    return-object v0
.end method

.method public static values()[Lcom/a/b/l/an;
    .registers 1

    .prologue
    .line 157
    sget-object v0, Lcom/a/b/l/an;->b:[Lcom/a/b/l/an;

    invoke-virtual {v0}, [Lcom/a/b/l/an;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/a/b/l/an;

    return-object v0
.end method


# virtual methods
.method public final synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .registers 7

    .prologue
    .line 157
    check-cast p1, [I

    check-cast p2, [I

    .line 1162
    array-length v0, p1

    array-length v1, p2

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 1163
    const/4 v0, 0x0

    :goto_b
    if-ge v0, v1, :cond_1f

    .line 1164
    aget v2, p1, v0

    aget v3, p2, v0

    if-eq v2, v3, :cond_1c

    .line 1165
    aget v1, p1, v0

    aget v0, p2, v0

    invoke-static {v1, v0}, Lcom/a/b/l/am;->a(II)I

    move-result v0

    :goto_1b
    return v0

    .line 1163
    :cond_1c
    add-int/lit8 v0, v0, 0x1

    goto :goto_b

    .line 1168
    :cond_1f
    array-length v0, p1

    array-length v1, p2

    sub-int/2addr v0, v1

    .line 157
    goto :goto_1b
.end method
