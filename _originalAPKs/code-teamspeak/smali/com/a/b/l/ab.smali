.class final enum Lcom/a/b/l/ab;
.super Ljava/lang/Enum;
.source "SourceFile"

# interfaces
.implements Ljava/util/Comparator;


# static fields
.field public static final enum a:Lcom/a/b/l/ab;

.field private static final synthetic b:[Lcom/a/b/l/ab;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    .line 449
    new-instance v0, Lcom/a/b/l/ab;

    const-string v1, "INSTANCE"

    invoke-direct {v0, v1}, Lcom/a/b/l/ab;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/a/b/l/ab;->a:Lcom/a/b/l/ab;

    .line 448
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/a/b/l/ab;

    const/4 v1, 0x0

    sget-object v2, Lcom/a/b/l/ab;->a:Lcom/a/b/l/ab;

    aput-object v2, v0, v1

    sput-object v0, Lcom/a/b/l/ab;->b:[Lcom/a/b/l/ab;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .registers 3

    .prologue
    .line 448
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method private static a([S[S)I
    .registers 6

    .prologue
    .line 453
    array-length v0, p0

    array-length v1, p1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 454
    const/4 v0, 0x0

    move v1, v0

    :goto_8
    if-ge v1, v2, :cond_16

    .line 455
    aget-short v0, p0, v1

    aget-short v3, p1, v1

    .line 1126
    sub-int/2addr v0, v3

    .line 456
    if-eqz v0, :cond_12

    .line 460
    :goto_11
    return v0

    .line 454
    :cond_12
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_8

    .line 460
    :cond_16
    array-length v0, p0

    array-length v1, p1

    sub-int/2addr v0, v1

    goto :goto_11
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/a/b/l/ab;
    .registers 2

    .prologue
    .line 448
    const-class v0, Lcom/a/b/l/ab;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/a/b/l/ab;

    return-object v0
.end method

.method public static values()[Lcom/a/b/l/ab;
    .registers 1

    .prologue
    .line 448
    sget-object v0, Lcom/a/b/l/ab;->b:[Lcom/a/b/l/ab;

    invoke-virtual {v0}, [Lcom/a/b/l/ab;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/a/b/l/ab;

    return-object v0
.end method


# virtual methods
.method public final synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .registers 7

    .prologue
    .line 448
    check-cast p1, [S

    check-cast p2, [S

    .line 1453
    array-length v0, p1

    array-length v1, p2

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 1454
    const/4 v0, 0x0

    move v1, v0

    :goto_c
    if-ge v1, v2, :cond_1a

    .line 1455
    aget-short v0, p1, v1

    aget-short v3, p2, v1

    .line 2126
    sub-int/2addr v0, v3

    .line 1456
    if-eqz v0, :cond_16

    .line 1457
    :goto_15
    return v0

    .line 1454
    :cond_16
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_c

    .line 1460
    :cond_1a
    array-length v0, p1

    array-length v1, p2

    sub-int/2addr v0, v1

    .line 448
    goto :goto_15
.end method
