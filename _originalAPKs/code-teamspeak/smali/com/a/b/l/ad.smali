.class final Lcom/a/b/l/ad;
.super Lcom/a/b/b/ak;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field static final a:Lcom/a/b/l/ad;

.field private static final b:J = 0x1L


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 338
    new-instance v0, Lcom/a/b/l/ad;

    invoke-direct {v0}, Lcom/a/b/l/ad;-><init>()V

    sput-object v0, Lcom/a/b/l/ad;->a:Lcom/a/b/l/ad;

    return-void
.end method

.method private constructor <init>()V
    .registers 1

    .prologue
    .line 336
    invoke-direct {p0}, Lcom/a/b/b/ak;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/String;)Ljava/lang/Short;
    .registers 2

    .prologue
    .line 342
    invoke-static {p0}, Ljava/lang/Short;->decode(Ljava/lang/String;)Ljava/lang/Short;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/Short;)Ljava/lang/String;
    .registers 2

    .prologue
    .line 347
    invoke-virtual {p0}, Ljava/lang/Short;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static b()Ljava/lang/Object;
    .registers 1

    .prologue
    .line 356
    sget-object v0, Lcom/a/b/l/ad;->a:Lcom/a/b/l/ad;

    return-object v0
.end method


# virtual methods
.method protected final synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 336
    check-cast p1, Ljava/lang/Short;

    .line 1347
    invoke-virtual {p1}, Ljava/lang/Short;->toString()Ljava/lang/String;

    move-result-object v0

    .line 336
    return-object v0
.end method

.method protected final synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 336
    check-cast p1, Ljava/lang/String;

    .line 2342
    invoke-static {p1}, Ljava/lang/Short;->decode(Ljava/lang/String;)Ljava/lang/Short;

    move-result-object v0

    .line 336
    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .registers 2

    .prologue
    .line 352
    const-string v0, "Shorts.stringConverter()"

    return-object v0
.end method
