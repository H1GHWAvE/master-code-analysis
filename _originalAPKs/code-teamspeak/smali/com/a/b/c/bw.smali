.class public abstract enum Lcom/a/b/c/bw;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lcom/a/b/c/bw;

.field public static final enum b:Lcom/a/b/c/bw;

.field public static final enum c:Lcom/a/b/c/bw;

.field private static final synthetic d:[Lcom/a/b/c/bw;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    .line 378
    new-instance v0, Lcom/a/b/c/bx;

    const-string v1, "STRONG"

    invoke-direct {v0, v1}, Lcom/a/b/c/bx;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/a/b/c/bw;->a:Lcom/a/b/c/bw;

    .line 393
    new-instance v0, Lcom/a/b/c/by;

    const-string v1, "SOFT"

    invoke-direct {v0, v1}, Lcom/a/b/c/by;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/a/b/c/bw;->b:Lcom/a/b/c/bw;

    .line 409
    new-instance v0, Lcom/a/b/c/bz;

    const-string v1, "WEAK"

    invoke-direct {v0, v1}, Lcom/a/b/c/bz;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/a/b/c/bw;->c:Lcom/a/b/c/bw;

    .line 372
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/a/b/c/bw;

    const/4 v1, 0x0

    sget-object v2, Lcom/a/b/c/bw;->a:Lcom/a/b/c/bw;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/a/b/c/bw;->b:Lcom/a/b/c/bw;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/a/b/c/bw;->c:Lcom/a/b/c/bw;

    aput-object v2, v0, v1

    sput-object v0, Lcom/a/b/c/bw;->d:[Lcom/a/b/c/bw;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3

    .prologue
    .line 372
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;IB)V
    .registers 4

    .prologue
    .line 372
    invoke-direct {p0, p1, p2}, Lcom/a/b/c/bw;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/a/b/c/bw;
    .registers 2

    .prologue
    .line 372
    const-class v0, Lcom/a/b/c/bw;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/a/b/c/bw;

    return-object v0
.end method

.method public static values()[Lcom/a/b/c/bw;
    .registers 1

    .prologue
    .line 372
    sget-object v0, Lcom/a/b/c/bw;->d:[Lcom/a/b/c/bw;

    invoke-virtual {v0}, [Lcom/a/b/c/bw;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/a/b/c/bw;

    return-object v0
.end method


# virtual methods
.method abstract a()Lcom/a/b/b/au;
.end method

.method abstract a(Lcom/a/b/c/bt;Lcom/a/b/c/bs;Ljava/lang/Object;I)Lcom/a/b/c/cg;
.end method
