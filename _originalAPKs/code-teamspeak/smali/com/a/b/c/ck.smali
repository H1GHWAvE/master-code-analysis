.class Lcom/a/b/c/ck;
.super Ljava/lang/ref/WeakReference;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/c/bs;


# instance fields
.field final g:I

.field final h:Lcom/a/b/c/bs;

.field volatile i:Lcom/a/b/c/cg;


# direct methods
.method constructor <init>(Ljava/lang/ref/ReferenceQueue;Ljava/lang/Object;ILcom/a/b/c/bs;)V
    .registers 6
    .param p4    # Lcom/a/b/c/bs;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1288
    invoke-direct {p0, p2, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;Ljava/lang/ref/ReferenceQueue;)V

    .line 1371
    invoke-static {}, Lcom/a/b/c/ao;->j()Lcom/a/b/c/cg;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/c/ck;->i:Lcom/a/b/c/cg;

    .line 1289
    iput p3, p0, Lcom/a/b/c/ck;->g:I

    .line 1290
    iput-object p4, p0, Lcom/a/b/c/ck;->h:Lcom/a/b/c/bs;

    .line 1291
    return-void
.end method


# virtual methods
.method public final a()Lcom/a/b/c/cg;
    .registers 2

    .prologue
    .line 1375
    iget-object v0, p0, Lcom/a/b/c/ck;->i:Lcom/a/b/c/cg;

    return-object v0
.end method

.method public a(J)V
    .registers 4

    .prologue
    .line 1312
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public a(Lcom/a/b/c/bs;)V
    .registers 3

    .prologue
    .line 1322
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final a(Lcom/a/b/c/cg;)V
    .registers 2

    .prologue
    .line 1380
    iput-object p1, p0, Lcom/a/b/c/ck;->i:Lcom/a/b/c/cg;

    .line 1381
    return-void
.end method

.method public final b()Lcom/a/b/c/bs;
    .registers 2

    .prologue
    .line 1390
    iget-object v0, p0, Lcom/a/b/c/ck;->h:Lcom/a/b/c/bs;

    return-object v0
.end method

.method public b(J)V
    .registers 4

    .prologue
    .line 1344
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public b(Lcom/a/b/c/bs;)V
    .registers 3

    .prologue
    .line 1332
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final c()I
    .registers 2

    .prologue
    .line 1385
    iget v0, p0, Lcom/a/b/c/ck;->g:I

    return v0
.end method

.method public c(Lcom/a/b/c/bs;)V
    .registers 3

    .prologue
    .line 1354
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final d()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 1295
    invoke-virtual {p0}, Lcom/a/b/c/ck;->get()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public d(Lcom/a/b/c/bs;)V
    .registers 3

    .prologue
    .line 1364
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public e()J
    .registers 2

    .prologue
    .line 1307
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public f()Lcom/a/b/c/bs;
    .registers 2

    .prologue
    .line 1317
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public g()Lcom/a/b/c/bs;
    .registers 2

    .prologue
    .line 1327
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public h()J
    .registers 2

    .prologue
    .line 1339
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public i()Lcom/a/b/c/bs;
    .registers 2

    .prologue
    .line 1349
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public j()Lcom/a/b/c/bs;
    .registers 2

    .prologue
    .line 1359
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
