.class final Lcom/a/b/c/ch;
.super Ljava/util/AbstractCollection;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/a/b/c/ao;

.field private final b:Ljava/util/concurrent/ConcurrentMap;


# direct methods
.method constructor <init>(Lcom/a/b/c/ao;Ljava/util/concurrent/ConcurrentMap;)V
    .registers 3

    .prologue
    .line 4485
    iput-object p1, p0, Lcom/a/b/c/ch;->a:Lcom/a/b/c/ao;

    invoke-direct {p0}, Ljava/util/AbstractCollection;-><init>()V

    .line 4486
    iput-object p2, p0, Lcom/a/b/c/ch;->b:Ljava/util/concurrent/ConcurrentMap;

    .line 4487
    return-void
.end method


# virtual methods
.method public final clear()V
    .registers 2

    .prologue
    .line 4498
    iget-object v0, p0, Lcom/a/b/c/ch;->b:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0}, Ljava/util/concurrent/ConcurrentMap;->clear()V

    .line 4499
    return-void
.end method

.method public final contains(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 4508
    iget-object v0, p0, Lcom/a/b/c/ch;->b:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p1}, Ljava/util/concurrent/ConcurrentMap;->containsValue(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final isEmpty()Z
    .registers 2

    .prologue
    .line 4494
    iget-object v0, p0, Lcom/a/b/c/ch;->b:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0}, Ljava/util/concurrent/ConcurrentMap;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public final iterator()Ljava/util/Iterator;
    .registers 3

    .prologue
    .line 4503
    new-instance v0, Lcom/a/b/c/cf;

    iget-object v1, p0, Lcom/a/b/c/ch;->a:Lcom/a/b/c/ao;

    invoke-direct {v0, v1}, Lcom/a/b/c/cf;-><init>(Lcom/a/b/c/ao;)V

    return-object v0
.end method

.method public final size()I
    .registers 2

    .prologue
    .line 4490
    iget-object v0, p0, Lcom/a/b/c/ch;->b:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0}, Ljava/util/concurrent/ConcurrentMap;->size()I

    move-result v0

    return v0
.end method
