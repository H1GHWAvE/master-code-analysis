.class public abstract Lcom/a/b/c/d;
.super Lcom/a/b/c/a;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/c/an;


# annotations
.annotation build Lcom/a/b/a/a;
.end annotation


# direct methods
.method protected constructor <init>()V
    .registers 1

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/a/b/c/a;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 53
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/a/b/c/d;->f(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_3
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_3} :catch_5

    move-result-object v0

    return-object v0

    .line 54
    :catch_5
    move-exception v0

    .line 55
    new-instance v1, Lcom/a/b/n/a/gq;

    invoke-virtual {v0}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/a/b/n/a/gq;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final c(Ljava/lang/Iterable;)Lcom/a/b/d/jt;
    .registers 6

    .prologue
    .line 61
    invoke-static {}, Lcom/a/b/d/sz;->d()Ljava/util/LinkedHashMap;

    move-result-object v0

    .line 62
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_8
    :goto_8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_20

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 63
    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_8

    .line 64
    invoke-virtual {p0, v2}, Lcom/a/b/c/d;->f(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_8

    .line 67
    :cond_20
    invoke-static {v0}, Lcom/a/b/d/jt;->a(Ljava/util/Map;)Lcom/a/b/d/jt;

    move-result-object v0

    return-object v0
.end method

.method public final c(Ljava/lang/Object;)V
    .registers 3

    .prologue
    .line 77
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final e(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 72
    invoke-virtual {p0, p1}, Lcom/a/b/c/d;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
