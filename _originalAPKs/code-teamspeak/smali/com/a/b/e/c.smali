.class public abstract Lcom/a/b/e/c;
.super Lcom/a/b/e/p;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/a;
.end annotation

.annotation build Lcom/a/b/a/b;
.end annotation


# instance fields
.field private final a:[[C

.field private final b:I

.field private final c:I

.field private final d:I

.field private final e:C

.field private final f:C


# direct methods
.method private constructor <init>(Lcom/a/b/e/b;II)V
    .registers 5

    .prologue
    .line 105
    invoke-direct {p0}, Lcom/a/b/e/p;-><init>()V

    .line 107
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1069
    iget-object v0, p1, Lcom/a/b/e/b;->a:[[C

    .line 108
    iput-object v0, p0, Lcom/a/b/e/c;->a:[[C

    .line 109
    iget-object v0, p0, Lcom/a/b/e/c;->a:[[C

    array-length v0, v0

    iput v0, p0, Lcom/a/b/e/c;->b:I

    .line 110
    if-ge p3, p2, :cond_15

    .line 113
    const/4 p3, -0x1

    .line 114
    const p2, 0x7fffffff

    .line 116
    :cond_15
    iput p2, p0, Lcom/a/b/e/c;->c:I

    .line 117
    iput p3, p0, Lcom/a/b/e/c;->d:I

    .line 132
    const v0, 0xd800

    if-lt p2, v0, :cond_27

    .line 135
    const v0, 0xffff

    iput-char v0, p0, Lcom/a/b/e/c;->e:C

    .line 136
    const/4 v0, 0x0

    iput-char v0, p0, Lcom/a/b/e/c;->f:C

    .line 144
    :goto_26
    return-void

    .line 140
    :cond_27
    int-to-char v0, p2

    iput-char v0, p0, Lcom/a/b/e/c;->e:C

    .line 141
    const v0, 0xd7ff

    invoke-static {p3, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    int-to-char v0, v0

    iput-char v0, p0, Lcom/a/b/e/c;->f:C

    goto :goto_26
.end method

.method private constructor <init>(Ljava/util/Map;II)V
    .registers 5

    .prologue
    .line 83
    invoke-static {p1}, Lcom/a/b/e/b;->a(Ljava/util/Map;)Lcom/a/b/e/b;

    move-result-object v0

    invoke-direct {p0, v0, p2, p3}, Lcom/a/b/e/c;-><init>(Lcom/a/b/e/b;II)V

    .line 85
    return-void
.end method


# virtual methods
.method protected final a(Ljava/lang/CharSequence;II)I
    .registers 6

    .prologue
    .line 167
    :goto_0
    if-ge p2, p3, :cond_1b

    .line 168
    invoke-interface {p1, p2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    .line 169
    iget v1, p0, Lcom/a/b/e/c;->b:I

    if-ge v0, v1, :cond_10

    iget-object v1, p0, Lcom/a/b/e/c;->a:[[C

    aget-object v1, v1, v0

    if-nez v1, :cond_1b

    :cond_10
    iget-char v1, p0, Lcom/a/b/e/c;->f:C

    if-gt v0, v1, :cond_1b

    iget-char v1, p0, Lcom/a/b/e/c;->e:C

    if-lt v0, v1, :cond_1b

    .line 173
    add-int/lit8 p2, p2, 0x1

    .line 174
    goto :goto_0

    .line 175
    :cond_1b
    return p2
.end method

.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .registers 5

    .prologue
    .line 153
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 154
    const/4 v0, 0x0

    :goto_4
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-ge v0, v1, :cond_24

    .line 155
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v1

    .line 156
    iget v2, p0, Lcom/a/b/e/c;->b:I

    if-ge v1, v2, :cond_18

    iget-object v2, p0, Lcom/a/b/e/c;->a:[[C

    aget-object v2, v2, v1

    if-nez v2, :cond_20

    :cond_18
    iget-char v2, p0, Lcom/a/b/e/c;->f:C

    if-gt v1, v2, :cond_20

    iget-char v2, p0, Lcom/a/b/e/c;->e:C

    if-ge v1, v2, :cond_25

    .line 158
    :cond_20
    invoke-virtual {p0, p1, v0}, Lcom/a/b/e/c;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object p1

    .line 161
    :cond_24
    return-object p1

    .line 154
    :cond_25
    add-int/lit8 v0, v0, 0x1

    goto :goto_4
.end method

.method protected abstract a()[C
.end method

.method protected final a(I)[C
    .registers 3

    .prologue
    .line 185
    iget v0, p0, Lcom/a/b/e/c;->b:I

    if-ge p1, v0, :cond_b

    .line 186
    iget-object v0, p0, Lcom/a/b/e/c;->a:[[C

    aget-object v0, v0, p1

    .line 187
    if-eqz v0, :cond_b

    .line 194
    :goto_a
    return-object v0

    .line 191
    :cond_b
    iget v0, p0, Lcom/a/b/e/c;->c:I

    if-lt p1, v0, :cond_15

    iget v0, p0, Lcom/a/b/e/c;->d:I

    if-gt p1, v0, :cond_15

    .line 192
    const/4 v0, 0x0

    goto :goto_a

    .line 194
    :cond_15
    invoke-virtual {p0}, Lcom/a/b/e/c;->a()[C

    move-result-object v0

    goto :goto_a
.end method
