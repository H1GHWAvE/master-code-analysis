.class final Lcom/a/b/d/acl;
.super Lcom/a/b/d/vb;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/a/b/d/aci;


# direct methods
.method constructor <init>(Lcom/a/b/d/aci;)V
    .registers 2

    .prologue
    .line 853
    iput-object p1, p0, Lcom/a/b/d/acl;->a:Lcom/a/b/d/aci;

    .line 854
    invoke-direct {p0, p1}, Lcom/a/b/d/vb;-><init>(Ljava/util/Map;)V

    .line 855
    return-void
.end method


# virtual methods
.method public final remove(Ljava/lang/Object;)Z
    .registers 5

    .prologue
    .line 858
    iget-object v0, p0, Lcom/a/b/d/acl;->a:Lcom/a/b/d/aci;

    invoke-virtual {v0}, Lcom/a/b/d/aci;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_a
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2f

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 859
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 860
    iget-object v1, p0, Lcom/a/b/d/acl;->a:Lcom/a/b/d/aci;

    iget-object v1, v1, Lcom/a/b/d/aci;->a:Lcom/a/b/d/abx;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/a/b/d/abx;->a(Lcom/a/b/d/abx;Ljava/lang/Object;)Ljava/util/Map;

    .line 861
    const/4 v0, 0x1

    .line 864
    :goto_2e
    return v0

    :cond_2f
    const/4 v0, 0x0

    goto :goto_2e
.end method

.method public final removeAll(Ljava/util/Collection;)Z
    .registers 6

    .prologue
    .line 868
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 869
    const/4 v0, 0x0

    .line 870
    iget-object v1, p0, Lcom/a/b/d/acl;->a:Lcom/a/b/d/aci;

    iget-object v1, v1, Lcom/a/b/d/aci;->a:Lcom/a/b/d/abx;

    invoke-virtual {v1}, Lcom/a/b/d/abx;->b()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    invoke-static {v1}, Lcom/a/b/d/ov;->a(Ljava/util/Iterator;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_18
    :goto_18
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_39

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 871
    iget-object v3, p0, Lcom/a/b/d/acl;->a:Lcom/a/b/d/aci;

    iget-object v3, v3, Lcom/a/b/d/aci;->a:Lcom/a/b/d/abx;

    invoke-virtual {v3, v2}, Lcom/a/b/d/abx;->d(Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v3

    invoke-interface {p1, v3}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_18

    .line 872
    iget-object v0, p0, Lcom/a/b/d/acl;->a:Lcom/a/b/d/aci;

    iget-object v0, v0, Lcom/a/b/d/aci;->a:Lcom/a/b/d/abx;

    invoke-static {v0, v2}, Lcom/a/b/d/abx;->a(Lcom/a/b/d/abx;Ljava/lang/Object;)Ljava/util/Map;

    .line 873
    const/4 v0, 0x1

    goto :goto_18

    .line 876
    :cond_39
    return v0
.end method

.method public final retainAll(Ljava/util/Collection;)Z
    .registers 6

    .prologue
    .line 880
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 881
    const/4 v0, 0x0

    .line 882
    iget-object v1, p0, Lcom/a/b/d/acl;->a:Lcom/a/b/d/aci;

    iget-object v1, v1, Lcom/a/b/d/aci;->a:Lcom/a/b/d/abx;

    invoke-virtual {v1}, Lcom/a/b/d/abx;->b()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    invoke-static {v1}, Lcom/a/b/d/ov;->a(Ljava/util/Iterator;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_18
    :goto_18
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_39

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 883
    iget-object v3, p0, Lcom/a/b/d/acl;->a:Lcom/a/b/d/aci;

    iget-object v3, v3, Lcom/a/b/d/aci;->a:Lcom/a/b/d/abx;

    invoke-virtual {v3, v2}, Lcom/a/b/d/abx;->d(Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v3

    invoke-interface {p1, v3}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_18

    .line 884
    iget-object v0, p0, Lcom/a/b/d/acl;->a:Lcom/a/b/d/aci;

    iget-object v0, v0, Lcom/a/b/d/aci;->a:Lcom/a/b/d/abx;

    invoke-static {v0, v2}, Lcom/a/b/d/abx;->a(Lcom/a/b/d/abx;Ljava/lang/Object;)Ljava/util/Map;

    .line 885
    const/4 v0, 0x1

    goto :goto_18

    .line 888
    :cond_39
    return v0
.end method
