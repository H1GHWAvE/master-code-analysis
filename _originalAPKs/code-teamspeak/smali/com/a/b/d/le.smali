.class public final Lcom/a/b/d/le;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/a/b/d/yr;

.field private final b:Lcom/a/b/d/yq;


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 94
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 95
    invoke-static {}, Lcom/a/b/d/afm;->c()Lcom/a/b/d/afm;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/le;->a:Lcom/a/b/d/yr;

    .line 96
    invoke-static {}, Lcom/a/b/d/afb;->c()Lcom/a/b/d/afb;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/le;->b:Lcom/a/b/d/yq;

    .line 97
    return-void
.end method

.method private a()Lcom/a/b/d/lb;
    .registers 6

    .prologue
    .line 142
    iget-object v0, p0, Lcom/a/b/d/le;->b:Lcom/a/b/d/yq;

    invoke-interface {v0}, Lcom/a/b/d/yq;->d()Ljava/util/Map;

    move-result-object v0

    .line 143
    new-instance v1, Lcom/a/b/d/jn;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v2

    invoke-direct {v1, v2}, Lcom/a/b/d/jn;-><init>(I)V

    .line 145
    new-instance v2, Lcom/a/b/d/jn;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v3

    invoke-direct {v2, v3}, Lcom/a/b/d/jn;-><init>(I)V

    .line 146
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_20
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3b

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 147
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/a/b/d/jn;->c(Ljava/lang/Object;)Lcom/a/b/d/jn;

    .line 148
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/a/b/d/jn;->c(Ljava/lang/Object;)Lcom/a/b/d/jn;

    goto :goto_20

    .line 150
    :cond_3b
    new-instance v0, Lcom/a/b/d/lb;

    invoke-virtual {v1}, Lcom/a/b/d/jn;->b()Lcom/a/b/d/jl;

    move-result-object v1

    invoke-virtual {v2}, Lcom/a/b/d/jn;->b()Lcom/a/b/d/jl;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/a/b/d/lb;-><init>(Lcom/a/b/d/jl;Lcom/a/b/d/jl;)V

    return-object v0
.end method

.method private a(Lcom/a/b/d/yl;Ljava/lang/Object;)Lcom/a/b/d/le;
    .registers 9

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 106
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 107
    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 108
    invoke-virtual {p1}, Lcom/a/b/d/yl;->f()Z

    move-result v0

    if-nez v0, :cond_92

    move v0, v1

    :goto_f
    const-string v3, "Range must not be empty, but was %s"

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p1, v1, v2

    invoke-static {v0, v3, v1}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 109
    iget-object v0, p0, Lcom/a/b/d/le;->a:Lcom/a/b/d/yr;

    invoke-interface {v0}, Lcom/a/b/d/yr;->f()Lcom/a/b/d/yr;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/a/b/d/yr;->c(Lcom/a/b/d/yl;)Z

    move-result v0

    if-nez v0, :cond_95

    .line 111
    iget-object v0, p0, Lcom/a/b/d/le;->b:Lcom/a/b/d/yq;

    invoke-interface {v0}, Lcom/a/b/d/yq;->d()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_32
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_95

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 112
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/a/b/d/yl;

    .line 113
    invoke-virtual {v1, p1}, Lcom/a/b/d/yl;->b(Lcom/a/b/d/yl;)Z

    move-result v3

    if-eqz v3, :cond_32

    invoke-virtual {v1, p1}, Lcom/a/b/d/yl;->c(Lcom/a/b/d/yl;)Lcom/a/b/d/yl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/a/b/d/yl;->f()Z

    move-result v1

    if-nez v1, :cond_32

    .line 114
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x2f

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Overlapping ranges: range "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " overlaps with entry "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_92
    move v0, v2

    .line 108
    goto/16 :goto_f

    .line 119
    :cond_95
    iget-object v0, p0, Lcom/a/b/d/le;->a:Lcom/a/b/d/yr;

    invoke-interface {v0, p1}, Lcom/a/b/d/yr;->a(Lcom/a/b/d/yl;)V

    .line 120
    iget-object v0, p0, Lcom/a/b/d/le;->b:Lcom/a/b/d/yq;

    invoke-interface {v0, p1, p2}, Lcom/a/b/d/yq;->a(Lcom/a/b/d/yl;Ljava/lang/Object;)V

    .line 121
    return-object p0
.end method

.method private a(Lcom/a/b/d/yq;)Lcom/a/b/d/le;
    .registers 11

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 131
    invoke-interface {p1}, Lcom/a/b/d/yq;->d()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_e
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_c3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 132
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/a/b/d/yl;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    .line 1106
    invoke-static {v1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1107
    invoke-static {v6}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1108
    invoke-virtual {v1}, Lcom/a/b/d/yl;->f()Z

    move-result v0

    if-nez v0, :cond_b4

    move v0, v3

    :goto_31
    const-string v2, "Range must not be empty, but was %s"

    new-array v7, v3, [Ljava/lang/Object;

    aput-object v1, v7, v4

    invoke-static {v0, v2, v7}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 1109
    iget-object v0, p0, Lcom/a/b/d/le;->a:Lcom/a/b/d/yr;

    invoke-interface {v0}, Lcom/a/b/d/yr;->f()Lcom/a/b/d/yr;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/a/b/d/yr;->c(Lcom/a/b/d/yl;)Z

    move-result v0

    if-nez v0, :cond_b7

    .line 1111
    iget-object v0, p0, Lcom/a/b/d/le;->b:Lcom/a/b/d/yq;

    invoke-interface {v0}, Lcom/a/b/d/yq;->d()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_54
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b7

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1112
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/a/b/d/yl;

    .line 1113
    invoke-virtual {v2, v1}, Lcom/a/b/d/yl;->b(Lcom/a/b/d/yl;)Z

    move-result v8

    if-eqz v8, :cond_54

    invoke-virtual {v2, v1}, Lcom/a/b/d/yl;->c(Lcom/a/b/d/yl;)Lcom/a/b/d/yl;

    move-result-object v2

    invoke-virtual {v2}, Lcom/a/b/d/yl;->f()Z

    move-result v2

    if-nez v2, :cond_54

    .line 1114
    new-instance v2, Ljava/lang/IllegalArgumentException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x2f

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Overlapping ranges: range "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " overlaps with entry "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_b4
    move v0, v4

    .line 1108
    goto/16 :goto_31

    .line 1119
    :cond_b7
    iget-object v0, p0, Lcom/a/b/d/le;->a:Lcom/a/b/d/yr;

    invoke-interface {v0, v1}, Lcom/a/b/d/yr;->a(Lcom/a/b/d/yl;)V

    .line 1120
    iget-object v0, p0, Lcom/a/b/d/le;->b:Lcom/a/b/d/yq;

    invoke-interface {v0, v1, v6}, Lcom/a/b/d/yq;->a(Lcom/a/b/d/yl;Ljava/lang/Object;)V

    goto/16 :goto_e

    .line 134
    :cond_c3
    return-object p0
.end method
