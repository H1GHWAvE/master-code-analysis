.class final Lcom/a/b/d/aeu;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/Iterator;


# instance fields
.field a:Lcom/a/b/d/aez;

.field b:Lcom/a/b/d/xd;

.field final synthetic c:Lcom/a/b/d/aer;


# direct methods
.method constructor <init>(Lcom/a/b/d/aer;)V
    .registers 3

    .prologue
    .line 444
    iput-object p1, p0, Lcom/a/b/d/aeu;->c:Lcom/a/b/d/aer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 445
    iget-object v0, p0, Lcom/a/b/d/aeu;->c:Lcom/a/b/d/aer;

    invoke-static {v0}, Lcom/a/b/d/aer;->d(Lcom/a/b/d/aer;)Lcom/a/b/d/aez;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/aeu;->a:Lcom/a/b/d/aez;

    .line 446
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/a/b/d/aeu;->b:Lcom/a/b/d/xd;

    return-void
.end method

.method private a()Lcom/a/b/d/xd;
    .registers 4

    .prologue
    .line 462
    invoke-virtual {p0}, Lcom/a/b/d/aeu;->hasNext()Z

    move-result v0

    if-nez v0, :cond_c

    .line 463
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 465
    :cond_c
    iget-object v0, p0, Lcom/a/b/d/aeu;->c:Lcom/a/b/d/aer;

    iget-object v1, p0, Lcom/a/b/d/aeu;->a:Lcom/a/b/d/aez;

    invoke-static {v0, v1}, Lcom/a/b/d/aer;->a(Lcom/a/b/d/aer;Lcom/a/b/d/aez;)Lcom/a/b/d/xd;

    move-result-object v0

    .line 466
    iput-object v0, p0, Lcom/a/b/d/aeu;->b:Lcom/a/b/d/xd;

    .line 467
    iget-object v1, p0, Lcom/a/b/d/aeu;->a:Lcom/a/b/d/aez;

    .line 2519
    iget-object v1, v1, Lcom/a/b/d/aez;->g:Lcom/a/b/d/aez;

    .line 467
    iget-object v2, p0, Lcom/a/b/d/aeu;->c:Lcom/a/b/d/aer;

    invoke-static {v2}, Lcom/a/b/d/aer;->c(Lcom/a/b/d/aer;)Lcom/a/b/d/aez;

    move-result-object v2

    if-ne v1, v2, :cond_26

    .line 468
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/a/b/d/aeu;->a:Lcom/a/b/d/aez;

    .line 472
    :goto_25
    return-object v0

    .line 470
    :cond_26
    iget-object v1, p0, Lcom/a/b/d/aeu;->a:Lcom/a/b/d/aez;

    .line 3519
    iget-object v1, v1, Lcom/a/b/d/aez;->g:Lcom/a/b/d/aez;

    .line 470
    iput-object v1, p0, Lcom/a/b/d/aeu;->a:Lcom/a/b/d/aez;

    goto :goto_25
.end method


# virtual methods
.method public final hasNext()Z
    .registers 4

    .prologue
    const/4 v0, 0x0

    .line 450
    iget-object v1, p0, Lcom/a/b/d/aeu;->a:Lcom/a/b/d/aez;

    if-nez v1, :cond_6

    .line 456
    :goto_5
    return v0

    .line 452
    :cond_6
    iget-object v1, p0, Lcom/a/b/d/aeu;->c:Lcom/a/b/d/aer;

    invoke-static {v1}, Lcom/a/b/d/aer;->b(Lcom/a/b/d/aer;)Lcom/a/b/d/hs;

    move-result-object v1

    iget-object v2, p0, Lcom/a/b/d/aeu;->a:Lcom/a/b/d/aez;

    .line 1923
    iget-object v2, v2, Lcom/a/b/d/aez;->a:Ljava/lang/Object;

    .line 452
    invoke-virtual {v1, v2}, Lcom/a/b/d/hs;->a(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1a

    .line 453
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/a/b/d/aeu;->a:Lcom/a/b/d/aez;

    goto :goto_5

    .line 456
    :cond_1a
    const/4 v0, 0x1

    goto :goto_5
.end method

.method public final synthetic next()Ljava/lang/Object;
    .registers 4

    .prologue
    .line 444
    .line 4462
    invoke-virtual {p0}, Lcom/a/b/d/aeu;->hasNext()Z

    move-result v0

    if-nez v0, :cond_c

    .line 4463
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 4465
    :cond_c
    iget-object v0, p0, Lcom/a/b/d/aeu;->c:Lcom/a/b/d/aer;

    iget-object v1, p0, Lcom/a/b/d/aeu;->a:Lcom/a/b/d/aez;

    invoke-static {v0, v1}, Lcom/a/b/d/aer;->a(Lcom/a/b/d/aer;Lcom/a/b/d/aez;)Lcom/a/b/d/xd;

    move-result-object v0

    .line 4466
    iput-object v0, p0, Lcom/a/b/d/aeu;->b:Lcom/a/b/d/xd;

    .line 4467
    iget-object v1, p0, Lcom/a/b/d/aeu;->a:Lcom/a/b/d/aez;

    .line 4519
    iget-object v1, v1, Lcom/a/b/d/aez;->g:Lcom/a/b/d/aez;

    .line 4467
    iget-object v2, p0, Lcom/a/b/d/aeu;->c:Lcom/a/b/d/aer;

    invoke-static {v2}, Lcom/a/b/d/aer;->c(Lcom/a/b/d/aer;)Lcom/a/b/d/aez;

    move-result-object v2

    if-ne v1, v2, :cond_26

    .line 4468
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/a/b/d/aeu;->a:Lcom/a/b/d/aez;

    .line 444
    :goto_25
    return-object v0

    .line 4470
    :cond_26
    iget-object v1, p0, Lcom/a/b/d/aeu;->a:Lcom/a/b/d/aez;

    .line 5519
    iget-object v1, v1, Lcom/a/b/d/aez;->g:Lcom/a/b/d/aez;

    .line 4470
    iput-object v1, p0, Lcom/a/b/d/aeu;->a:Lcom/a/b/d/aez;

    goto :goto_25
.end method

.method public final remove()V
    .registers 4

    .prologue
    const/4 v1, 0x0

    .line 477
    iget-object v0, p0, Lcom/a/b/d/aeu;->b:Lcom/a/b/d/xd;

    if-eqz v0, :cond_1a

    const/4 v0, 0x1

    .line 4049
    :goto_6
    const-string v2, "no calls to next() since the last call to remove()"

    invoke-static {v0, v2}, Lcom/a/b/b/cn;->b(ZLjava/lang/Object;)V

    .line 478
    iget-object v0, p0, Lcom/a/b/d/aeu;->c:Lcom/a/b/d/aer;

    iget-object v2, p0, Lcom/a/b/d/aeu;->b:Lcom/a/b/d/xd;

    invoke-interface {v2}, Lcom/a/b/d/xd;->a()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Lcom/a/b/d/aer;->c(Ljava/lang/Object;I)I

    .line 479
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/a/b/d/aeu;->b:Lcom/a/b/d/xd;

    .line 480
    return-void

    :cond_1a
    move v0, v1

    .line 477
    goto :goto_6
.end method
