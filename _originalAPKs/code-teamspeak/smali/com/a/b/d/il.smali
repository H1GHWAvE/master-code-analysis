.class abstract Lcom/a/b/d/il;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/Iterator;


# instance fields
.field b:I

.field c:Lcom/a/b/d/ia;

.field d:Lcom/a/b/d/ia;

.field e:I

.field final synthetic f:Lcom/a/b/d/hy;


# direct methods
.method constructor <init>(Lcom/a/b/d/hy;)V
    .registers 4

    .prologue
    const/4 v1, 0x0

    .line 331
    iput-object p1, p0, Lcom/a/b/d/il;->f:Lcom/a/b/d/hy;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 332
    const/4 v0, 0x0

    iput v0, p0, Lcom/a/b/d/il;->b:I

    .line 333
    iput-object v1, p0, Lcom/a/b/d/il;->c:Lcom/a/b/d/ia;

    .line 334
    iput-object v1, p0, Lcom/a/b/d/il;->d:Lcom/a/b/d/ia;

    .line 335
    iget-object v0, p0, Lcom/a/b/d/il;->f:Lcom/a/b/d/hy;

    invoke-static {v0}, Lcom/a/b/d/hy;->a(Lcom/a/b/d/hy;)I

    move-result v0

    iput v0, p0, Lcom/a/b/d/il;->e:I

    return-void
.end method

.method private a()V
    .registers 3

    .prologue
    .line 338
    iget-object v0, p0, Lcom/a/b/d/il;->f:Lcom/a/b/d/hy;

    invoke-static {v0}, Lcom/a/b/d/hy;->a(Lcom/a/b/d/hy;)I

    move-result v0

    iget v1, p0, Lcom/a/b/d/il;->e:I

    if-eq v0, v1, :cond_10

    .line 339
    new-instance v0, Ljava/util/ConcurrentModificationException;

    invoke-direct {v0}, Ljava/util/ConcurrentModificationException;-><init>()V

    throw v0

    .line 341
    :cond_10
    return-void
.end method


# virtual methods
.method abstract a(Lcom/a/b/d/ia;)Ljava/lang/Object;
.end method

.method public hasNext()Z
    .registers 5

    .prologue
    const/4 v0, 0x1

    .line 345
    invoke-direct {p0}, Lcom/a/b/d/il;->a()V

    .line 346
    iget-object v1, p0, Lcom/a/b/d/il;->c:Lcom/a/b/d/ia;

    if-eqz v1, :cond_f

    .line 356
    :goto_8
    return v0

    .line 354
    :cond_9
    iget v1, p0, Lcom/a/b/d/il;->b:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/a/b/d/il;->b:I

    .line 349
    :cond_f
    iget v1, p0, Lcom/a/b/d/il;->b:I

    iget-object v2, p0, Lcom/a/b/d/il;->f:Lcom/a/b/d/hy;

    invoke-static {v2}, Lcom/a/b/d/hy;->b(Lcom/a/b/d/hy;)[Lcom/a/b/d/ia;

    move-result-object v2

    array-length v2, v2

    if-ge v1, v2, :cond_37

    .line 350
    iget-object v1, p0, Lcom/a/b/d/il;->f:Lcom/a/b/d/hy;

    invoke-static {v1}, Lcom/a/b/d/hy;->b(Lcom/a/b/d/hy;)[Lcom/a/b/d/ia;

    move-result-object v1

    iget v2, p0, Lcom/a/b/d/il;->b:I

    aget-object v1, v1, v2

    if-eqz v1, :cond_9

    .line 351
    iget-object v1, p0, Lcom/a/b/d/il;->f:Lcom/a/b/d/hy;

    invoke-static {v1}, Lcom/a/b/d/hy;->b(Lcom/a/b/d/hy;)[Lcom/a/b/d/ia;

    move-result-object v1

    iget v2, p0, Lcom/a/b/d/il;->b:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/a/b/d/il;->b:I

    aget-object v1, v1, v2

    iput-object v1, p0, Lcom/a/b/d/il;->c:Lcom/a/b/d/ia;

    goto :goto_8

    .line 356
    :cond_37
    const/4 v0, 0x0

    goto :goto_8
.end method

.method public next()Ljava/lang/Object;
    .registers 3

    .prologue
    .line 361
    invoke-direct {p0}, Lcom/a/b/d/il;->a()V

    .line 362
    invoke-virtual {p0}, Lcom/a/b/d/il;->hasNext()Z

    move-result v0

    if-nez v0, :cond_f

    .line 363
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 366
    :cond_f
    iget-object v0, p0, Lcom/a/b/d/il;->c:Lcom/a/b/d/ia;

    .line 367
    iget-object v1, v0, Lcom/a/b/d/ia;->c:Lcom/a/b/d/ia;

    iput-object v1, p0, Lcom/a/b/d/il;->c:Lcom/a/b/d/ia;

    .line 368
    iput-object v0, p0, Lcom/a/b/d/il;->d:Lcom/a/b/d/ia;

    .line 369
    invoke-virtual {p0, v0}, Lcom/a/b/d/il;->a(Lcom/a/b/d/ia;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public remove()V
    .registers 3

    .prologue
    .line 374
    invoke-direct {p0}, Lcom/a/b/d/il;->a()V

    .line 375
    iget-object v0, p0, Lcom/a/b/d/il;->d:Lcom/a/b/d/ia;

    if-eqz v0, :cond_20

    const/4 v0, 0x1

    .line 1049
    :goto_8
    const-string v1, "no calls to next() since the last call to remove()"

    invoke-static {v0, v1}, Lcom/a/b/b/cn;->b(ZLjava/lang/Object;)V

    .line 376
    iget-object v0, p0, Lcom/a/b/d/il;->f:Lcom/a/b/d/hy;

    iget-object v1, p0, Lcom/a/b/d/il;->d:Lcom/a/b/d/ia;

    invoke-static {v0, v1}, Lcom/a/b/d/hy;->a(Lcom/a/b/d/hy;Lcom/a/b/d/ia;)V

    .line 377
    iget-object v0, p0, Lcom/a/b/d/il;->f:Lcom/a/b/d/hy;

    invoke-static {v0}, Lcom/a/b/d/hy;->a(Lcom/a/b/d/hy;)I

    move-result v0

    iput v0, p0, Lcom/a/b/d/il;->e:I

    .line 378
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/a/b/d/il;->d:Lcom/a/b/d/ia;

    .line 379
    return-void

    .line 375
    :cond_20
    const/4 v0, 0x0

    goto :goto_8
.end method
