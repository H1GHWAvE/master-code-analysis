.class Lcom/a/b/d/ado;
.super Lcom/a/b/d/add;
.source "SourceFile"

# interfaces
.implements Ljava/util/Queue;


# static fields
.field private static final a:J


# direct methods
.method constructor <init>(Ljava/util/Queue;)V
    .registers 4

    .prologue
    .line 1578
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/a/b/d/add;-><init>(Ljava/util/Collection;Ljava/lang/Object;B)V

    .line 1579
    return-void
.end method


# virtual methods
.method a()Ljava/util/Queue;
    .registers 2

    .prologue
    .line 1582
    invoke-super {p0}, Lcom/a/b/d/add;->b()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/util/Queue;

    return-object v0
.end method

.method synthetic b()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 1574
    invoke-virtual {p0}, Lcom/a/b/d/ado;->a()Ljava/util/Queue;

    move-result-object v0

    return-object v0
.end method

.method synthetic d()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 1574
    invoke-virtual {p0}, Lcom/a/b/d/ado;->a()Ljava/util/Queue;

    move-result-object v0

    return-object v0
.end method

.method public element()Ljava/lang/Object;
    .registers 3

    .prologue
    .line 1587
    iget-object v1, p0, Lcom/a/b/d/ado;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 1588
    :try_start_3
    invoke-virtual {p0}, Lcom/a/b/d/ado;->a()Ljava/util/Queue;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Queue;->element()Ljava/lang/Object;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 1589
    :catchall_d
    move-exception v0

    monitor-exit v1
    :try_end_f
    .catchall {:try_start_3 .. :try_end_f} :catchall_d

    throw v0
.end method

.method public offer(Ljava/lang/Object;)Z
    .registers 4

    .prologue
    .line 1594
    iget-object v1, p0, Lcom/a/b/d/ado;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 1595
    :try_start_3
    invoke-virtual {p0}, Lcom/a/b/d/ado;->a()Ljava/util/Queue;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1

    return v0

    .line 1596
    :catchall_d
    move-exception v0

    monitor-exit v1
    :try_end_f
    .catchall {:try_start_3 .. :try_end_f} :catchall_d

    throw v0
.end method

.method public peek()Ljava/lang/Object;
    .registers 3

    .prologue
    .line 1601
    iget-object v1, p0, Lcom/a/b/d/ado;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 1602
    :try_start_3
    invoke-virtual {p0}, Lcom/a/b/d/ado;->a()Ljava/util/Queue;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 1603
    :catchall_d
    move-exception v0

    monitor-exit v1
    :try_end_f
    .catchall {:try_start_3 .. :try_end_f} :catchall_d

    throw v0
.end method

.method public poll()Ljava/lang/Object;
    .registers 3

    .prologue
    .line 1608
    iget-object v1, p0, Lcom/a/b/d/ado;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 1609
    :try_start_3
    invoke-virtual {p0}, Lcom/a/b/d/ado;->a()Ljava/util/Queue;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 1610
    :catchall_d
    move-exception v0

    monitor-exit v1
    :try_end_f
    .catchall {:try_start_3 .. :try_end_f} :catchall_d

    throw v0
.end method

.method public remove()Ljava/lang/Object;
    .registers 3

    .prologue
    .line 1615
    iget-object v1, p0, Lcom/a/b/d/ado;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 1616
    :try_start_3
    invoke-virtual {p0}, Lcom/a/b/d/ado;->a()Ljava/util/Queue;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 1617
    :catchall_d
    move-exception v0

    monitor-exit v1
    :try_end_f
    .catchall {:try_start_3 .. :try_end_f} :catchall_d

    throw v0
.end method
