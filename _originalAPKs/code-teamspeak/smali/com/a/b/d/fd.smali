.class public final Lcom/a/b/d/fd;
.super Lcom/a/b/d/a;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/b;
    b = true
.end annotation


# static fields
.field private static final d:J
    .annotation build Lcom/a/b/a/c;
        a = "not needed in emulated source."
    .end annotation
.end field


# instance fields
.field private transient b:Ljava/lang/Class;

.field private transient c:Ljava/lang/Class;


# direct methods
.method private constructor <init>(Ljava/lang/Class;Ljava/lang/Class;)V
    .registers 5

    .prologue
    .line 79
    new-instance v0, Ljava/util/EnumMap;

    invoke-direct {v0, p1}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    invoke-static {v0}, Lcom/a/b/d/agm;->a(Ljava/util/Map;)Lcom/a/b/d/agm;

    move-result-object v0

    new-instance v1, Ljava/util/EnumMap;

    invoke-direct {v1, p2}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    invoke-static {v1}, Lcom/a/b/d/agm;->a(Ljava/util/Map;)Lcom/a/b/d/agm;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/a/b/d/a;-><init>(Ljava/util/Map;Ljava/util/Map;)V

    .line 81
    iput-object p1, p0, Lcom/a/b/d/fd;->b:Ljava/lang/Class;

    .line 82
    iput-object p2, p0, Lcom/a/b/d/fd;->c:Ljava/lang/Class;

    .line 83
    return-void
.end method

.method private static a(Ljava/lang/Class;Ljava/lang/Class;)Lcom/a/b/d/fd;
    .registers 3

    .prologue
    .line 58
    new-instance v0, Lcom/a/b/d/fd;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/fd;-><init>(Ljava/lang/Class;Ljava/lang/Class;)V

    return-object v0
.end method

.method static a(Ljava/util/Map;)Ljava/lang/Class;
    .registers 2

    .prologue
    .line 86
    instance-of v0, p0, Lcom/a/b/d/fd;

    if-eqz v0, :cond_9

    .line 87
    check-cast p0, Lcom/a/b/d/fd;

    .line 2106
    iget-object v0, p0, Lcom/a/b/d/fd;->b:Ljava/lang/Class;

    .line 93
    :goto_8
    return-object v0

    .line 89
    :cond_9
    instance-of v0, p0, Lcom/a/b/d/fe;

    if-eqz v0, :cond_12

    .line 90
    check-cast p0, Lcom/a/b/d/fe;

    .line 3103
    iget-object v0, p0, Lcom/a/b/d/fe;->b:Ljava/lang/Class;

    goto :goto_8

    .line 92
    :cond_12
    invoke-interface {p0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2f

    const/4 v0, 0x1

    :goto_19
    invoke-static {v0}, Lcom/a/b/b/cn;->a(Z)V

    .line 93
    invoke-interface {p0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Enum;

    invoke-virtual {v0}, Ljava/lang/Enum;->getDeclaringClass()Ljava/lang/Class;

    move-result-object v0

    goto :goto_8

    .line 92
    :cond_2f
    const/4 v0, 0x0

    goto :goto_19
.end method

.method private static a(Ljava/lang/Enum;)Ljava/lang/Enum;
    .registers 2

    .prologue
    .line 116
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Enum;

    return-object v0
.end method

.method private a(Ljava/io/ObjectInputStream;)V
    .registers 5
    .annotation build Lcom/a/b/a/c;
        a = "java.io.ObjectInputStream"
    .end annotation

    .prologue
    .line 140
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->defaultReadObject()V

    .line 141
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    iput-object v0, p0, Lcom/a/b/d/fd;->b:Ljava/lang/Class;

    .line 142
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    iput-object v0, p0, Lcom/a/b/d/fd;->c:Ljava/lang/Class;

    .line 143
    new-instance v0, Ljava/util/EnumMap;

    iget-object v1, p0, Lcom/a/b/d/fd;->b:Ljava/lang/Class;

    invoke-direct {v0, v1}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    invoke-static {v0}, Lcom/a/b/d/agm;->a(Ljava/util/Map;)Lcom/a/b/d/agm;

    move-result-object v0

    new-instance v1, Ljava/util/EnumMap;

    iget-object v2, p0, Lcom/a/b/d/fd;->c:Ljava/lang/Class;

    invoke-direct {v1, v2}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    invoke-static {v1}, Lcom/a/b/d/agm;->a(Ljava/util/Map;)Lcom/a/b/d/agm;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/a/b/d/fd;->a(Ljava/util/Map;Ljava/util/Map;)V

    .line 146
    invoke-static {p0, p1}, Lcom/a/b/d/zz;->a(Ljava/util/Map;Ljava/io/ObjectInputStream;)V

    .line 147
    return-void
.end method

.method private a(Ljava/io/ObjectOutputStream;)V
    .registers 3
    .annotation build Lcom/a/b/a/c;
        a = "java.io.ObjectOutputStream"
    .end annotation

    .prologue
    .line 130
    invoke-virtual {p1}, Ljava/io/ObjectOutputStream;->defaultWriteObject()V

    .line 131
    iget-object v0, p0, Lcom/a/b/d/fd;->b:Ljava/lang/Class;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 132
    iget-object v0, p0, Lcom/a/b/d/fd;->c:Ljava/lang/Class;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 133
    invoke-static {p0, p1}, Lcom/a/b/d/zz;->a(Ljava/util/Map;Ljava/io/ObjectOutputStream;)V

    .line 134
    return-void
.end method

.method private static b(Ljava/util/Map;)Lcom/a/b/d/fd;
    .registers 4

    .prologue
    .line 73
    invoke-static {p0}, Lcom/a/b/d/fd;->a(Ljava/util/Map;)Ljava/lang/Class;

    move-result-object v1

    .line 1097
    instance-of v0, p0, Lcom/a/b/d/fd;

    if-eqz v0, :cond_16

    move-object v0, p0

    .line 1098
    check-cast v0, Lcom/a/b/d/fd;

    iget-object v0, v0, Lcom/a/b/d/fd;->c:Ljava/lang/Class;

    .line 2058
    :goto_d
    new-instance v2, Lcom/a/b/d/fd;

    invoke-direct {v2, v1, v0}, Lcom/a/b/d/fd;-><init>(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 74
    invoke-virtual {v2, p0}, Lcom/a/b/d/fd;->putAll(Ljava/util/Map;)V

    .line 75
    return-object v2

    .line 1100
    :cond_16
    invoke-interface {p0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_33

    const/4 v0, 0x1

    :goto_1d
    invoke-static {v0}, Lcom/a/b/b/cn;->a(Z)V

    .line 1101
    invoke-interface {p0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Enum;

    invoke-virtual {v0}, Ljava/lang/Enum;->getDeclaringClass()Ljava/lang/Class;

    move-result-object v0

    goto :goto_d

    .line 1100
    :cond_33
    const/4 v0, 0x0

    goto :goto_1d
.end method

.method private static b(Ljava/lang/Enum;)Ljava/lang/Enum;
    .registers 2

    .prologue
    .line 121
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Enum;

    return-object v0
.end method

.method private static c(Ljava/util/Map;)Ljava/lang/Class;
    .registers 2

    .prologue
    .line 97
    instance-of v0, p0, Lcom/a/b/d/fd;

    if-eqz v0, :cond_9

    .line 98
    check-cast p0, Lcom/a/b/d/fd;

    iget-object v0, p0, Lcom/a/b/d/fd;->c:Ljava/lang/Class;

    .line 101
    :goto_8
    return-object v0

    .line 100
    :cond_9
    invoke-interface {p0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_26

    const/4 v0, 0x1

    :goto_10
    invoke-static {v0}, Lcom/a/b/b/cn;->a(Z)V

    .line 101
    invoke-interface {p0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Enum;

    invoke-virtual {v0}, Ljava/lang/Enum;->getDeclaringClass()Ljava/lang/Class;

    move-result-object v0

    goto :goto_8

    .line 100
    :cond_26
    const/4 v0, 0x0

    goto :goto_10
.end method

.method private d()Ljava/lang/Class;
    .registers 2

    .prologue
    .line 106
    iget-object v0, p0, Lcom/a/b/d/fd;->b:Ljava/lang/Class;

    return-object v0
.end method

.method private e()Ljava/lang/Class;
    .registers 2

    .prologue
    .line 111
    iget-object v0, p0, Lcom/a/b/d/fd;->c:Ljava/lang/Class;

    return-object v0
.end method


# virtual methods
.method final bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 43
    check-cast p1, Ljava/lang/Enum;

    .line 4116
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Enum;

    .line 43
    return-object v0
.end method

.method public final bridge synthetic b()Lcom/a/b/d/bw;
    .registers 2

    .prologue
    .line 43
    invoke-super {p0}, Lcom/a/b/d/a;->b()Lcom/a/b/d/bw;

    move-result-object v0

    return-object v0
.end method

.method final synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 43
    check-cast p1, Ljava/lang/Enum;

    .line 3121
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Enum;

    .line 43
    return-object v0
.end method

.method public final bridge synthetic clear()V
    .registers 1

    .prologue
    .line 43
    invoke-super {p0}, Lcom/a/b/d/a;->clear()V

    return-void
.end method

.method public final bridge synthetic containsValue(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 43
    invoke-super {p0, p1}, Lcom/a/b/d/a;->containsValue(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic entrySet()Ljava/util/Set;
    .registers 2

    .prologue
    .line 43
    invoke-super {p0}, Lcom/a/b/d/a;->entrySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic j_()Ljava/util/Set;
    .registers 2

    .prologue
    .line 43
    invoke-super {p0}, Lcom/a/b/d/a;->j_()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic keySet()Ljava/util/Set;
    .registers 2

    .prologue
    .line 43
    invoke-super {p0}, Lcom/a/b/d/a;->keySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic putAll(Ljava/util/Map;)V
    .registers 2

    .prologue
    .line 43
    invoke-super {p0, p1}, Lcom/a/b/d/a;->putAll(Ljava/util/Map;)V

    return-void
.end method
