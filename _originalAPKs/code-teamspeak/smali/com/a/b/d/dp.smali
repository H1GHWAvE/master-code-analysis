.class Lcom/a/b/d/dp;
.super Lcom/a/b/d/gp;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/b;
.end annotation


# instance fields
.field final a:Ljava/util/List;

.field final b:Lcom/a/b/d/dm;


# direct methods
.method constructor <init>(Ljava/util/List;Lcom/a/b/d/dm;)V
    .registers 4

    .prologue
    .line 192
    invoke-direct {p0}, Lcom/a/b/d/gp;-><init>()V

    .line 193
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/a/b/d/dp;->a:Ljava/util/List;

    .line 194
    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/dm;

    iput-object v0, p0, Lcom/a/b/d/dp;->b:Lcom/a/b/d/dm;

    .line 195
    return-void
.end method


# virtual methods
.method protected final a()Ljava/util/List;
    .registers 2

    .prologue
    .line 197
    iget-object v0, p0, Lcom/a/b/d/dp;->a:Ljava/util/List;

    return-object v0
.end method

.method public add(ILjava/lang/Object;)V
    .registers 4

    .prologue
    .line 205
    iget-object v0, p0, Lcom/a/b/d/dp;->b:Lcom/a/b/d/dm;

    invoke-interface {v0, p2}, Lcom/a/b/d/dm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 206
    iget-object v0, p0, Lcom/a/b/d/dp;->a:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 207
    return-void
.end method

.method public add(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 201
    iget-object v0, p0, Lcom/a/b/d/dp;->b:Lcom/a/b/d/dm;

    invoke-interface {v0, p1}, Lcom/a/b/d/dm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 202
    iget-object v0, p0, Lcom/a/b/d/dp;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public addAll(ILjava/util/Collection;)Z
    .registers 5

    .prologue
    .line 213
    iget-object v0, p0, Lcom/a/b/d/dp;->a:Ljava/util/List;

    iget-object v1, p0, Lcom/a/b/d/dp;->b:Lcom/a/b/d/dm;

    invoke-static {p2, v1}, Lcom/a/b/d/dn;->b(Ljava/util/Collection;Lcom/a/b/d/dm;)Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->addAll(ILjava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public addAll(Ljava/util/Collection;)Z
    .registers 4

    .prologue
    .line 209
    iget-object v0, p0, Lcom/a/b/d/dp;->a:Ljava/util/List;

    iget-object v1, p0, Lcom/a/b/d/dp;->b:Lcom/a/b/d/dm;

    invoke-static {p1, v1}, Lcom/a/b/d/dn;->b(Ljava/util/Collection;Lcom/a/b/d/dm;)Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method protected final bridge synthetic b()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 187
    .line 1197
    iget-object v0, p0, Lcom/a/b/d/dp;->a:Ljava/util/List;

    .line 187
    return-object v0
.end method

.method protected final bridge synthetic k_()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 187
    .line 2197
    iget-object v0, p0, Lcom/a/b/d/dp;->a:Ljava/util/List;

    .line 187
    return-object v0
.end method

.method public listIterator()Ljava/util/ListIterator;
    .registers 3

    .prologue
    .line 216
    iget-object v0, p0, Lcom/a/b/d/dp;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->listIterator()Ljava/util/ListIterator;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/d/dp;->b:Lcom/a/b/d/dm;

    invoke-static {v0, v1}, Lcom/a/b/d/dn;->a(Ljava/util/ListIterator;Lcom/a/b/d/dm;)Ljava/util/ListIterator;

    move-result-object v0

    return-object v0
.end method

.method public listIterator(I)Ljava/util/ListIterator;
    .registers 4

    .prologue
    .line 219
    iget-object v0, p0, Lcom/a/b/d/dp;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->listIterator(I)Ljava/util/ListIterator;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/d/dp;->b:Lcom/a/b/d/dm;

    invoke-static {v0, v1}, Lcom/a/b/d/dn;->a(Ljava/util/ListIterator;Lcom/a/b/d/dm;)Ljava/util/ListIterator;

    move-result-object v0

    return-object v0
.end method

.method public set(ILjava/lang/Object;)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 222
    iget-object v0, p0, Lcom/a/b/d/dp;->b:Lcom/a/b/d/dm;

    invoke-interface {v0, p2}, Lcom/a/b/d/dm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 223
    iget-object v0, p0, Lcom/a/b/d/dp;->a:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public subList(II)Ljava/util/List;
    .registers 5

    .prologue
    .line 226
    iget-object v0, p0, Lcom/a/b/d/dp;->a:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/d/dp;->b:Lcom/a/b/d/dm;

    invoke-static {v0, v1}, Lcom/a/b/d/dn;->a(Ljava/util/List;Lcom/a/b/d/dm;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
