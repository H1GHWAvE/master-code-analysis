.class final Lcom/a/b/d/os;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/ListIterator;


# instance fields
.field a:I

.field b:Lcom/a/b/d/or;

.field c:Lcom/a/b/d/or;

.field d:Lcom/a/b/d/or;

.field e:I

.field final synthetic f:Lcom/a/b/d/oj;


# direct methods
.method constructor <init>(Lcom/a/b/d/oj;I)V
    .registers 5

    .prologue
    .line 323
    iput-object p1, p0, Lcom/a/b/d/os;->f:Lcom/a/b/d/oj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 321
    iget-object v0, p0, Lcom/a/b/d/os;->f:Lcom/a/b/d/oj;

    invoke-static {v0}, Lcom/a/b/d/oj;->a(Lcom/a/b/d/oj;)I

    move-result v0

    iput v0, p0, Lcom/a/b/d/os;->e:I

    .line 324
    invoke-virtual {p1}, Lcom/a/b/d/oj;->f()I

    move-result v1

    .line 325
    invoke-static {p2, v1}, Lcom/a/b/b/cn;->b(II)I

    .line 326
    div-int/lit8 v0, v1, 0x2

    if-lt p2, v0, :cond_29

    .line 327
    invoke-static {p1}, Lcom/a/b/d/oj;->b(Lcom/a/b/d/oj;)Lcom/a/b/d/or;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/os;->d:Lcom/a/b/d/or;

    .line 328
    iput v1, p0, Lcom/a/b/d/os;->a:I

    .line 329
    :goto_20
    add-int/lit8 v0, p2, 0x1

    if-ge p2, v1, :cond_38

    .line 330
    invoke-direct {p0}, Lcom/a/b/d/os;->c()Lcom/a/b/d/or;

    move p2, v0

    goto :goto_20

    .line 333
    :cond_29
    invoke-static {p1}, Lcom/a/b/d/oj;->c(Lcom/a/b/d/oj;)Lcom/a/b/d/or;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/os;->b:Lcom/a/b/d/or;

    .line 334
    :goto_2f
    add-int/lit8 v0, p2, -0x1

    if-lez p2, :cond_38

    .line 335
    invoke-direct {p0}, Lcom/a/b/d/os;->b()Lcom/a/b/d/or;

    move p2, v0

    goto :goto_2f

    .line 338
    :cond_38
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/a/b/d/os;->c:Lcom/a/b/d/or;

    .line 339
    return-void
.end method

.method private a()V
    .registers 3

    .prologue
    .line 341
    iget-object v0, p0, Lcom/a/b/d/os;->f:Lcom/a/b/d/oj;

    invoke-static {v0}, Lcom/a/b/d/oj;->a(Lcom/a/b/d/oj;)I

    move-result v0

    iget v1, p0, Lcom/a/b/d/os;->e:I

    if-eq v0, v1, :cond_10

    .line 342
    new-instance v0, Ljava/util/ConcurrentModificationException;

    invoke-direct {v0}, Ljava/util/ConcurrentModificationException;-><init>()V

    throw v0

    .line 344
    :cond_10
    return-void
.end method

.method private a(Ljava/lang/Object;)V
    .registers 3

    .prologue
    .line 404
    iget-object v0, p0, Lcom/a/b/d/os;->c:Lcom/a/b/d/or;

    if-eqz v0, :cond_d

    const/4 v0, 0x1

    :goto_5
    invoke-static {v0}, Lcom/a/b/b/cn;->b(Z)V

    .line 405
    iget-object v0, p0, Lcom/a/b/d/os;->c:Lcom/a/b/d/or;

    iput-object p1, v0, Lcom/a/b/d/or;->b:Ljava/lang/Object;

    .line 406
    return-void

    .line 404
    :cond_d
    const/4 v0, 0x0

    goto :goto_5
.end method

.method private b()Lcom/a/b/d/or;
    .registers 2

    .prologue
    .line 352
    invoke-direct {p0}, Lcom/a/b/d/os;->a()V

    .line 353
    iget-object v0, p0, Lcom/a/b/d/os;->b:Lcom/a/b/d/or;

    invoke-static {v0}, Lcom/a/b/d/oj;->e(Ljava/lang/Object;)V

    .line 354
    iget-object v0, p0, Lcom/a/b/d/os;->b:Lcom/a/b/d/or;

    iput-object v0, p0, Lcom/a/b/d/os;->c:Lcom/a/b/d/or;

    iput-object v0, p0, Lcom/a/b/d/os;->d:Lcom/a/b/d/or;

    .line 355
    iget-object v0, p0, Lcom/a/b/d/os;->b:Lcom/a/b/d/or;

    iget-object v0, v0, Lcom/a/b/d/or;->c:Lcom/a/b/d/or;

    iput-object v0, p0, Lcom/a/b/d/os;->b:Lcom/a/b/d/or;

    .line 356
    iget v0, p0, Lcom/a/b/d/os;->a:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/a/b/d/os;->a:I

    .line 357
    iget-object v0, p0, Lcom/a/b/d/os;->c:Lcom/a/b/d/or;

    return-object v0
.end method

.method private c()Lcom/a/b/d/or;
    .registers 2

    .prologue
    .line 380
    invoke-direct {p0}, Lcom/a/b/d/os;->a()V

    .line 381
    iget-object v0, p0, Lcom/a/b/d/os;->d:Lcom/a/b/d/or;

    invoke-static {v0}, Lcom/a/b/d/oj;->e(Ljava/lang/Object;)V

    .line 382
    iget-object v0, p0, Lcom/a/b/d/os;->d:Lcom/a/b/d/or;

    iput-object v0, p0, Lcom/a/b/d/os;->c:Lcom/a/b/d/or;

    iput-object v0, p0, Lcom/a/b/d/os;->b:Lcom/a/b/d/or;

    .line 383
    iget-object v0, p0, Lcom/a/b/d/os;->d:Lcom/a/b/d/or;

    iget-object v0, v0, Lcom/a/b/d/or;->d:Lcom/a/b/d/or;

    iput-object v0, p0, Lcom/a/b/d/os;->d:Lcom/a/b/d/or;

    .line 384
    iget v0, p0, Lcom/a/b/d/os;->a:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/a/b/d/os;->a:I

    .line 385
    iget-object v0, p0, Lcom/a/b/d/os;->c:Lcom/a/b/d/or;

    return-object v0
.end method

.method private static d()V
    .registers 1

    .prologue
    .line 397
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method private static e()V
    .registers 1

    .prologue
    .line 401
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method


# virtual methods
.method public final synthetic add(Ljava/lang/Object;)V
    .registers 3

    .prologue
    .line 1401
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final hasNext()Z
    .registers 2

    .prologue
    .line 347
    invoke-direct {p0}, Lcom/a/b/d/os;->a()V

    .line 348
    iget-object v0, p0, Lcom/a/b/d/os;->b:Lcom/a/b/d/or;

    if-eqz v0, :cond_9

    const/4 v0, 0x1

    :goto_8
    return v0

    :cond_9
    const/4 v0, 0x0

    goto :goto_8
.end method

.method public final hasPrevious()Z
    .registers 2

    .prologue
    .line 375
    invoke-direct {p0}, Lcom/a/b/d/os;->a()V

    .line 376
    iget-object v0, p0, Lcom/a/b/d/os;->d:Lcom/a/b/d/or;

    if-eqz v0, :cond_9

    const/4 v0, 0x1

    :goto_8
    return v0

    :cond_9
    const/4 v0, 0x0

    goto :goto_8
.end method

.method public final synthetic next()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 316
    invoke-direct {p0}, Lcom/a/b/d/os;->b()Lcom/a/b/d/or;

    move-result-object v0

    return-object v0
.end method

.method public final nextIndex()I
    .registers 2

    .prologue
    .line 389
    iget v0, p0, Lcom/a/b/d/os;->a:I

    return v0
.end method

.method public final synthetic previous()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 316
    invoke-direct {p0}, Lcom/a/b/d/os;->c()Lcom/a/b/d/or;

    move-result-object v0

    return-object v0
.end method

.method public final previousIndex()I
    .registers 2

    .prologue
    .line 393
    iget v0, p0, Lcom/a/b/d/os;->a:I

    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method public final remove()V
    .registers 3

    .prologue
    .line 361
    invoke-direct {p0}, Lcom/a/b/d/os;->a()V

    .line 362
    iget-object v0, p0, Lcom/a/b/d/os;->c:Lcom/a/b/d/or;

    if-eqz v0, :cond_32

    const/4 v0, 0x1

    .line 1049
    :goto_8
    const-string v1, "no calls to next() since the last call to remove()"

    invoke-static {v0, v1}, Lcom/a/b/b/cn;->b(ZLjava/lang/Object;)V

    .line 363
    iget-object v0, p0, Lcom/a/b/d/os;->c:Lcom/a/b/d/or;

    iget-object v1, p0, Lcom/a/b/d/os;->b:Lcom/a/b/d/or;

    if-eq v0, v1, :cond_34

    .line 364
    iget-object v0, p0, Lcom/a/b/d/os;->c:Lcom/a/b/d/or;

    iget-object v0, v0, Lcom/a/b/d/or;->d:Lcom/a/b/d/or;

    iput-object v0, p0, Lcom/a/b/d/os;->d:Lcom/a/b/d/or;

    .line 365
    iget v0, p0, Lcom/a/b/d/os;->a:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/a/b/d/os;->a:I

    .line 369
    :goto_1f
    iget-object v0, p0, Lcom/a/b/d/os;->f:Lcom/a/b/d/oj;

    iget-object v1, p0, Lcom/a/b/d/os;->c:Lcom/a/b/d/or;

    invoke-static {v0, v1}, Lcom/a/b/d/oj;->a(Lcom/a/b/d/oj;Lcom/a/b/d/or;)V

    .line 370
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/a/b/d/os;->c:Lcom/a/b/d/or;

    .line 371
    iget-object v0, p0, Lcom/a/b/d/os;->f:Lcom/a/b/d/oj;

    invoke-static {v0}, Lcom/a/b/d/oj;->a(Lcom/a/b/d/oj;)I

    move-result v0

    iput v0, p0, Lcom/a/b/d/os;->e:I

    .line 372
    return-void

    .line 362
    :cond_32
    const/4 v0, 0x0

    goto :goto_8

    .line 367
    :cond_34
    iget-object v0, p0, Lcom/a/b/d/os;->c:Lcom/a/b/d/or;

    iget-object v0, v0, Lcom/a/b/d/or;->c:Lcom/a/b/d/or;

    iput-object v0, p0, Lcom/a/b/d/os;->b:Lcom/a/b/d/or;

    goto :goto_1f
.end method

.method public final synthetic set(Ljava/lang/Object;)V
    .registers 3

    .prologue
    .line 2397
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
