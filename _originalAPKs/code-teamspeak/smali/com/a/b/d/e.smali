.class final Lcom/a/b/d/e;
.super Lcom/a/b/d/gw;
.source "SourceFile"


# instance fields
.field final synthetic a:Ljava/util/Map$Entry;

.field final synthetic b:Lcom/a/b/d/d;


# direct methods
.method constructor <init>(Lcom/a/b/d/d;Ljava/util/Map$Entry;)V
    .registers 3

    .prologue
    .line 298
    iput-object p1, p0, Lcom/a/b/d/e;->b:Lcom/a/b/d/d;

    iput-object p2, p0, Lcom/a/b/d/e;->a:Ljava/util/Map$Entry;

    invoke-direct {p0}, Lcom/a/b/d/gw;-><init>()V

    return-void
.end method


# virtual methods
.method protected final a()Ljava/util/Map$Entry;
    .registers 2

    .prologue
    .line 300
    iget-object v0, p0, Lcom/a/b/d/e;->a:Ljava/util/Map$Entry;

    return-object v0
.end method

.method protected final bridge synthetic k_()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 298
    .line 1300
    iget-object v0, p0, Lcom/a/b/d/e;->a:Ljava/util/Map$Entry;

    .line 298
    return-object v0
.end method

.method public final setValue(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 305
    iget-object v0, p0, Lcom/a/b/d/e;->b:Lcom/a/b/d/d;

    iget-object v0, v0, Lcom/a/b/d/d;->c:Lcom/a/b/d/c;

    invoke-virtual {v0, p0}, Lcom/a/b/d/c;->contains(Ljava/lang/Object;)Z

    move-result v0

    const-string v3, "entry no longer in map"

    invoke-static {v0, v3}, Lcom/a/b/b/cn;->b(ZLjava/lang/Object;)V

    .line 307
    invoke-virtual {p0}, Lcom/a/b/d/e;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/a/b/b/ce;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 316
    :goto_19
    return-object p1

    .line 310
    :cond_1a
    iget-object v0, p0, Lcom/a/b/d/e;->b:Lcom/a/b/d/d;

    iget-object v0, v0, Lcom/a/b/d/d;->c:Lcom/a/b/d/c;

    iget-object v0, v0, Lcom/a/b/d/c;->b:Lcom/a/b/d/a;

    invoke-virtual {v0, p1}, Lcom/a/b/d/a;->containsValue(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5c

    move v0, v1

    :goto_27
    const-string v3, "value already present: %s"

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p1, v1, v2

    invoke-static {v0, v3, v1}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 312
    iget-object v0, p0, Lcom/a/b/d/e;->a:Ljava/util/Map$Entry;

    invoke-interface {v0, p1}, Ljava/util/Map$Entry;->setValue(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 313
    iget-object v1, p0, Lcom/a/b/d/e;->b:Lcom/a/b/d/d;

    iget-object v1, v1, Lcom/a/b/d/d;->c:Lcom/a/b/d/c;

    iget-object v1, v1, Lcom/a/b/d/c;->b:Lcom/a/b/d/a;

    invoke-virtual {p0}, Lcom/a/b/d/e;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/a/b/d/a;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/a/b/b/ce;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    const-string v2, "entry no longer in map"

    invoke-static {v1, v2}, Lcom/a/b/b/cn;->b(ZLjava/lang/Object;)V

    .line 315
    iget-object v1, p0, Lcom/a/b/d/e;->b:Lcom/a/b/d/d;

    iget-object v1, v1, Lcom/a/b/d/d;->c:Lcom/a/b/d/c;

    iget-object v1, v1, Lcom/a/b/d/c;->b:Lcom/a/b/d/a;

    invoke-virtual {p0}, Lcom/a/b/d/e;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-static {v1, v2, v0, p1}, Lcom/a/b/d/a;->a(Lcom/a/b/d/a;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    move-object p1, v0

    .line 316
    goto :goto_19

    :cond_5c
    move v0, v2

    .line 310
    goto :goto_27
.end method
