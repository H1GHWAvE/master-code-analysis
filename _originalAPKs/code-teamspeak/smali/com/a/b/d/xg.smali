.class final Lcom/a/b/d/xg;
.super Lcom/a/b/d/j;
.source "SourceFile"


# instance fields
.field final synthetic a:Ljava/util/Iterator;

.field final synthetic b:Ljava/util/Iterator;

.field final synthetic c:Lcom/a/b/d/xf;


# direct methods
.method constructor <init>(Lcom/a/b/d/xf;Ljava/util/Iterator;Ljava/util/Iterator;)V
    .registers 4

    .prologue
    .line 413
    iput-object p1, p0, Lcom/a/b/d/xg;->c:Lcom/a/b/d/xf;

    iput-object p2, p0, Lcom/a/b/d/xg;->a:Ljava/util/Iterator;

    iput-object p3, p0, Lcom/a/b/d/xg;->b:Ljava/util/Iterator;

    invoke-direct {p0}, Lcom/a/b/d/j;-><init>()V

    return-void
.end method

.method private c()Lcom/a/b/d/xd;
    .registers 4

    .prologue
    .line 416
    iget-object v0, p0, Lcom/a/b/d/xg;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_29

    .line 417
    iget-object v0, p0, Lcom/a/b/d/xg;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/xd;

    .line 418
    invoke-interface {v0}, Lcom/a/b/d/xd;->a()Ljava/lang/Object;

    move-result-object v1

    .line 419
    invoke-interface {v0}, Lcom/a/b/d/xd;->b()I

    move-result v0

    iget-object v2, p0, Lcom/a/b/d/xg;->c:Lcom/a/b/d/xf;

    iget-object v2, v2, Lcom/a/b/d/xf;->b:Lcom/a/b/d/xc;

    invoke-interface {v2, v1}, Lcom/a/b/d/xc;->a(Ljava/lang/Object;)I

    move-result v2

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 420
    invoke-static {v1, v0}, Lcom/a/b/d/xe;->a(Ljava/lang/Object;I)Lcom/a/b/d/xd;

    move-result-object v0

    .line 429
    :goto_28
    return-object v0

    .line 422
    :cond_29
    iget-object v0, p0, Lcom/a/b/d/xg;->b:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_50

    .line 423
    iget-object v0, p0, Lcom/a/b/d/xg;->b:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/xd;

    .line 424
    invoke-interface {v0}, Lcom/a/b/d/xd;->a()Ljava/lang/Object;

    move-result-object v1

    .line 425
    iget-object v2, p0, Lcom/a/b/d/xg;->c:Lcom/a/b/d/xf;

    iget-object v2, v2, Lcom/a/b/d/xf;->a:Lcom/a/b/d/xc;

    invoke-interface {v2, v1}, Lcom/a/b/d/xc;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_29

    .line 426
    invoke-interface {v0}, Lcom/a/b/d/xd;->b()I

    move-result v0

    invoke-static {v1, v0}, Lcom/a/b/d/xe;->a(Ljava/lang/Object;I)Lcom/a/b/d/xd;

    move-result-object v0

    goto :goto_28

    .line 429
    :cond_50
    invoke-virtual {p0}, Lcom/a/b/d/xg;->b()Ljava/lang/Object;

    const/4 v0, 0x0

    goto :goto_28
.end method


# virtual methods
.method protected final synthetic a()Ljava/lang/Object;
    .registers 4

    .prologue
    .line 413
    .line 1416
    iget-object v0, p0, Lcom/a/b/d/xg;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_29

    .line 1417
    iget-object v0, p0, Lcom/a/b/d/xg;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/xd;

    .line 1418
    invoke-interface {v0}, Lcom/a/b/d/xd;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1419
    invoke-interface {v0}, Lcom/a/b/d/xd;->b()I

    move-result v0

    iget-object v2, p0, Lcom/a/b/d/xg;->c:Lcom/a/b/d/xf;

    iget-object v2, v2, Lcom/a/b/d/xf;->b:Lcom/a/b/d/xc;

    invoke-interface {v2, v1}, Lcom/a/b/d/xc;->a(Ljava/lang/Object;)I

    move-result v2

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 1420
    invoke-static {v1, v0}, Lcom/a/b/d/xe;->a(Ljava/lang/Object;I)Lcom/a/b/d/xd;

    move-result-object v0

    .line 1426
    :goto_28
    return-object v0

    .line 1422
    :cond_29
    iget-object v0, p0, Lcom/a/b/d/xg;->b:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_50

    .line 1423
    iget-object v0, p0, Lcom/a/b/d/xg;->b:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/xd;

    .line 1424
    invoke-interface {v0}, Lcom/a/b/d/xd;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1425
    iget-object v2, p0, Lcom/a/b/d/xg;->c:Lcom/a/b/d/xf;

    iget-object v2, v2, Lcom/a/b/d/xf;->a:Lcom/a/b/d/xc;

    invoke-interface {v2, v1}, Lcom/a/b/d/xc;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_29

    .line 1426
    invoke-interface {v0}, Lcom/a/b/d/xd;->b()I

    move-result v0

    invoke-static {v1, v0}, Lcom/a/b/d/xe;->a(Ljava/lang/Object;I)Lcom/a/b/d/xd;

    move-result-object v0

    goto :goto_28

    .line 1429
    :cond_50
    invoke-virtual {p0}, Lcom/a/b/d/xg;->b()Ljava/lang/Object;

    const/4 v0, 0x0

    .line 413
    goto :goto_28
.end method
