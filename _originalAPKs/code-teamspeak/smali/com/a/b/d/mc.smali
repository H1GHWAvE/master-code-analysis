.class final Lcom/a/b/d/mc;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field a:Ljava/util/Comparator;

.field b:[Ljava/lang/Object;

.field c:[I


# direct methods
.method constructor <init>(Lcom/a/b/d/abn;)V
    .registers 7

    .prologue
    .line 548
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 549
    invoke-interface {p1}, Lcom/a/b/d/abn;->comparator()Ljava/util/Comparator;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/mc;->a:Ljava/util/Comparator;

    .line 550
    invoke-interface {p1}, Lcom/a/b/d/abn;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v1

    .line 551
    new-array v0, v1, [Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    iput-object v0, p0, Lcom/a/b/d/mc;->b:[Ljava/lang/Object;

    .line 552
    new-array v0, v1, [I

    iput-object v0, p0, Lcom/a/b/d/mc;->c:[I

    .line 553
    const/4 v0, 0x0

    .line 554
    invoke-interface {p1}, Lcom/a/b/d/abn;->a()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_25
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_45

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/xd;

    .line 555
    iget-object v3, p0, Lcom/a/b/d/mc;->b:[Ljava/lang/Object;

    invoke-interface {v0}, Lcom/a/b/d/xd;->a()Ljava/lang/Object;

    move-result-object v4

    aput-object v4, v3, v1

    .line 556
    iget-object v3, p0, Lcom/a/b/d/mc;->c:[I

    invoke-interface {v0}, Lcom/a/b/d/xd;->b()I

    move-result v0

    aput v0, v3, v1

    .line 557
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 558
    goto :goto_25

    .line 559
    :cond_45
    return-void
.end method

.method private a()Ljava/lang/Object;
    .registers 6

    .prologue
    .line 562
    iget-object v0, p0, Lcom/a/b/d/mc;->b:[Ljava/lang/Object;

    array-length v1, v0

    .line 563
    new-instance v2, Lcom/a/b/d/mb;

    iget-object v0, p0, Lcom/a/b/d/mc;->a:Ljava/util/Comparator;

    invoke-direct {v2, v0}, Lcom/a/b/d/mb;-><init>(Ljava/util/Comparator;)V

    .line 564
    const/4 v0, 0x0

    :goto_b
    if-ge v0, v1, :cond_1b

    .line 565
    iget-object v3, p0, Lcom/a/b/d/mc;->b:[Ljava/lang/Object;

    aget-object v3, v3, v0

    iget-object v4, p0, Lcom/a/b/d/mc;->c:[I

    aget v4, v4, v0

    invoke-virtual {v2, v3, v4}, Lcom/a/b/d/mb;->c(Ljava/lang/Object;I)Lcom/a/b/d/mb;

    .line 564
    add-int/lit8 v0, v0, 0x1

    goto :goto_b

    .line 567
    :cond_1b
    invoke-virtual {v2}, Lcom/a/b/d/mb;->c()Lcom/a/b/d/ma;

    move-result-object v0

    return-object v0
.end method
