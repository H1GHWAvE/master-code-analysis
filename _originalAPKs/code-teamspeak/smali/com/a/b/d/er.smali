.class final Lcom/a/b/d/er;
.super Lcom/a/b/d/ep;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final a:Lcom/a/b/d/er;

.field private static final b:J


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 59
    new-instance v0, Lcom/a/b/d/er;

    invoke-direct {v0}, Lcom/a/b/d/er;-><init>()V

    sput-object v0, Lcom/a/b/d/er;->a:Lcom/a/b/d/er;

    return-void
.end method

.method private constructor <init>()V
    .registers 1

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/a/b/d/ep;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Integer;Ljava/lang/Integer;)J
    .registers 6

    .prologue
    .line 72
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v0, v0

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    int-to-long v2, v2

    sub-long/2addr v0, v2

    return-wide v0
.end method

.method private static a(Ljava/lang/Integer;)Ljava/lang/Integer;
    .registers 3

    .prologue
    .line 62
    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 63
    const v1, 0x7fffffff

    if-ne v0, v1, :cond_b

    const/4 v0, 0x0

    :goto_a
    return-object v0

    :cond_b
    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_a
.end method

.method private static b(Ljava/lang/Integer;)Ljava/lang/Integer;
    .registers 3

    .prologue
    .line 67
    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 68
    const/high16 v1, -0x80000000

    if-ne v0, v1, :cond_a

    const/4 v0, 0x0

    :goto_9
    return-object v0

    :cond_a
    add-int/lit8 v0, v0, -0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_9
.end method

.method static synthetic c()Lcom/a/b/d/er;
    .registers 1

    .prologue
    .line 57
    sget-object v0, Lcom/a/b/d/er;->a:Lcom/a/b/d/er;

    return-object v0
.end method

.method private static d()Ljava/lang/Integer;
    .registers 1

    .prologue
    .line 76
    const/high16 v0, -0x80000000

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method private static e()Ljava/lang/Integer;
    .registers 1

    .prologue
    .line 80
    const v0, 0x7fffffff

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method private static f()Ljava/lang/Object;
    .registers 1

    .prologue
    .line 84
    sget-object v0, Lcom/a/b/d/er;->a:Lcom/a/b/d/er;

    return-object v0
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Comparable;Ljava/lang/Comparable;)J
    .registers 7

    .prologue
    .line 57
    check-cast p1, Ljava/lang/Integer;

    check-cast p2, Ljava/lang/Integer;

    .line 3072
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v0, v0

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    int-to-long v2, v2

    sub-long/2addr v0, v2

    .line 57
    return-wide v0
.end method

.method public final synthetic a()Ljava/lang/Comparable;
    .registers 2

    .prologue
    .line 2076
    const/high16 v0, -0x80000000

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 57
    return-object v0
.end method

.method public final synthetic a(Ljava/lang/Comparable;)Ljava/lang/Comparable;
    .registers 4

    .prologue
    .line 57
    check-cast p1, Ljava/lang/Integer;

    .line 5062
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 5063
    const v1, 0x7fffffff

    if-ne v0, v1, :cond_d

    const/4 v0, 0x0

    :goto_c
    return-object v0

    :cond_d
    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_c
.end method

.method public final synthetic b()Ljava/lang/Comparable;
    .registers 2

    .prologue
    .line 1080
    const v0, 0x7fffffff

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 57
    return-object v0
.end method

.method public final synthetic b(Ljava/lang/Comparable;)Ljava/lang/Comparable;
    .registers 4

    .prologue
    .line 57
    check-cast p1, Ljava/lang/Integer;

    .line 4067
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 4068
    const/high16 v1, -0x80000000

    if-ne v0, v1, :cond_c

    const/4 v0, 0x0

    :goto_b
    return-object v0

    :cond_c
    add-int/lit8 v0, v0, -0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_b
.end method

.method public final toString()Ljava/lang/String;
    .registers 2

    .prologue
    .line 89
    const-string v0, "DiscreteDomain.integers()"

    return-object v0
.end method
