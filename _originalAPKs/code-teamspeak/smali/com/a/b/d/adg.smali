.class Lcom/a/b/d/adg;
.super Lcom/a/b/d/add;
.source "SourceFile"

# interfaces
.implements Ljava/util/List;


# static fields
.field private static final a:J


# direct methods
.method constructor <init>(Ljava/util/List;Ljava/lang/Object;)V
    .registers 4
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 311
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/a/b/d/add;-><init>(Ljava/util/Collection;Ljava/lang/Object;B)V

    .line 312
    return-void
.end method

.method private a()Ljava/util/List;
    .registers 2

    .prologue
    .line 315
    invoke-super {p0}, Lcom/a/b/d/add;->b()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public add(ILjava/lang/Object;)V
    .registers 5

    .prologue
    .line 320
    iget-object v1, p0, Lcom/a/b/d/adg;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 1315
    :try_start_3
    invoke-super {p0}, Lcom/a/b/d/add;->b()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 321
    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 322
    monitor-exit v1

    return-void

    :catchall_e
    move-exception v0

    monitor-exit v1
    :try_end_10
    .catchall {:try_start_3 .. :try_end_10} :catchall_e

    throw v0
.end method

.method public addAll(ILjava/util/Collection;)Z
    .registers 5

    .prologue
    .line 327
    iget-object v1, p0, Lcom/a/b/d/adg;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 2315
    :try_start_3
    invoke-super {p0}, Lcom/a/b/d/add;->b()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 328
    invoke-interface {v0, p1, p2}, Ljava/util/List;->addAll(ILjava/util/Collection;)Z

    move-result v0

    monitor-exit v1

    return v0

    .line 329
    :catchall_f
    move-exception v0

    monitor-exit v1
    :try_end_11
    .catchall {:try_start_3 .. :try_end_11} :catchall_f

    throw v0
.end method

.method final bridge synthetic b()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 308
    .line 13315
    invoke-super {p0}, Lcom/a/b/d/add;->b()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 308
    return-object v0
.end method

.method final synthetic d()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 308
    .line 14315
    invoke-super {p0}, Lcom/a/b/d/add;->b()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 308
    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 4

    .prologue
    .line 385
    if-ne p1, p0, :cond_4

    .line 386
    const/4 v0, 0x1

    .line 389
    :goto_3
    return v0

    .line 388
    :cond_4
    iget-object v1, p0, Lcom/a/b/d/adg;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 11315
    :try_start_7
    invoke-super {p0}, Lcom/a/b/d/add;->b()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 389
    invoke-interface {v0, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1

    goto :goto_3

    .line 390
    :catchall_13
    move-exception v0

    monitor-exit v1
    :try_end_15
    .catchall {:try_start_7 .. :try_end_15} :catchall_13

    throw v0
.end method

.method public get(I)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 334
    iget-object v1, p0, Lcom/a/b/d/adg;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 3315
    :try_start_3
    invoke-super {p0}, Lcom/a/b/d/add;->b()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 335
    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 336
    :catchall_f
    move-exception v0

    monitor-exit v1
    :try_end_11
    .catchall {:try_start_3 .. :try_end_11} :catchall_f

    throw v0
.end method

.method public hashCode()I
    .registers 3

    .prologue
    .line 394
    iget-object v1, p0, Lcom/a/b/d/adg;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 12315
    :try_start_3
    invoke-super {p0}, Lcom/a/b/d/add;->b()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 395
    invoke-interface {v0}, Ljava/util/List;->hashCode()I

    move-result v0

    monitor-exit v1

    return v0

    .line 396
    :catchall_f
    move-exception v0

    monitor-exit v1
    :try_end_11
    .catchall {:try_start_3 .. :try_end_11} :catchall_f

    throw v0
.end method

.method public indexOf(Ljava/lang/Object;)I
    .registers 4

    .prologue
    .line 341
    iget-object v1, p0, Lcom/a/b/d/adg;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 4315
    :try_start_3
    invoke-super {p0}, Lcom/a/b/d/add;->b()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 342
    invoke-interface {v0, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    monitor-exit v1

    return v0

    .line 343
    :catchall_f
    move-exception v0

    monitor-exit v1
    :try_end_11
    .catchall {:try_start_3 .. :try_end_11} :catchall_f

    throw v0
.end method

.method public lastIndexOf(Ljava/lang/Object;)I
    .registers 4

    .prologue
    .line 348
    iget-object v1, p0, Lcom/a/b/d/adg;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 5315
    :try_start_3
    invoke-super {p0}, Lcom/a/b/d/add;->b()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 349
    invoke-interface {v0, p1}, Ljava/util/List;->lastIndexOf(Ljava/lang/Object;)I

    move-result v0

    monitor-exit v1

    return v0

    .line 350
    :catchall_f
    move-exception v0

    monitor-exit v1
    :try_end_11
    .catchall {:try_start_3 .. :try_end_11} :catchall_f

    throw v0
.end method

.method public listIterator()Ljava/util/ListIterator;
    .registers 2

    .prologue
    .line 355
    .line 6315
    invoke-super {p0}, Lcom/a/b/d/add;->b()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 355
    invoke-interface {v0}, Ljava/util/List;->listIterator()Ljava/util/ListIterator;

    move-result-object v0

    return-object v0
.end method

.method public listIterator(I)Ljava/util/ListIterator;
    .registers 3

    .prologue
    .line 360
    .line 7315
    invoke-super {p0}, Lcom/a/b/d/add;->b()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 360
    invoke-interface {v0, p1}, Ljava/util/List;->listIterator(I)Ljava/util/ListIterator;

    move-result-object v0

    return-object v0
.end method

.method public remove(I)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 365
    iget-object v1, p0, Lcom/a/b/d/adg;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 8315
    :try_start_3
    invoke-super {p0}, Lcom/a/b/d/add;->b()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 366
    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 367
    :catchall_f
    move-exception v0

    monitor-exit v1
    :try_end_11
    .catchall {:try_start_3 .. :try_end_11} :catchall_f

    throw v0
.end method

.method public set(ILjava/lang/Object;)Ljava/lang/Object;
    .registers 5

    .prologue
    .line 372
    iget-object v1, p0, Lcom/a/b/d/adg;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 9315
    :try_start_3
    invoke-super {p0}, Lcom/a/b/d/add;->b()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 373
    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 374
    :catchall_f
    move-exception v0

    monitor-exit v1
    :try_end_11
    .catchall {:try_start_3 .. :try_end_11} :catchall_f

    throw v0
.end method

.method public subList(II)Ljava/util/List;
    .registers 6

    .prologue
    .line 379
    iget-object v1, p0, Lcom/a/b/d/adg;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 10315
    :try_start_3
    invoke-super {p0}, Lcom/a/b/d/add;->b()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 380
    invoke-interface {v0, p1, p2}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    iget-object v2, p0, Lcom/a/b/d/adg;->h:Ljava/lang/Object;

    .line 11060
    invoke-static {v0, v2}, Lcom/a/b/d/acu;->a(Ljava/util/List;Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 380
    monitor-exit v1

    return-object v0

    .line 381
    :catchall_15
    move-exception v0

    monitor-exit v1
    :try_end_17
    .catchall {:try_start_3 .. :try_end_17} :catchall_15

    throw v0
.end method
