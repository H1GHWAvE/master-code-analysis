.class Lcom/a/b/d/uu;
.super Lcom/a/b/d/ur;
.source "SourceFile"

# interfaces
.implements Ljava/util/SortedMap;


# direct methods
.method constructor <init>(Ljava/util/SortedMap;Lcom/a/b/d/tv;)V
    .registers 3

    .prologue
    .line 1945
    invoke-direct {p0, p1, p2}, Lcom/a/b/d/ur;-><init>(Ljava/util/Map;Lcom/a/b/d/tv;)V

    .line 1946
    return-void
.end method


# virtual methods
.method protected c()Ljava/util/SortedMap;
    .registers 2

    .prologue
    .line 1940
    iget-object v0, p0, Lcom/a/b/d/uu;->a:Ljava/util/Map;

    check-cast v0, Ljava/util/SortedMap;

    return-object v0
.end method

.method public comparator()Ljava/util/Comparator;
    .registers 2

    .prologue
    .line 1949
    invoke-virtual {p0}, Lcom/a/b/d/uu;->c()Ljava/util/SortedMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/SortedMap;->comparator()Ljava/util/Comparator;

    move-result-object v0

    return-object v0
.end method

.method public firstKey()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 1953
    invoke-virtual {p0}, Lcom/a/b/d/uu;->c()Ljava/util/SortedMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/SortedMap;->firstKey()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public headMap(Ljava/lang/Object;)Ljava/util/SortedMap;
    .registers 4

    .prologue
    .line 1957
    invoke-virtual {p0}, Lcom/a/b/d/uu;->c()Ljava/util/SortedMap;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/SortedMap;->headMap(Ljava/lang/Object;)Ljava/util/SortedMap;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/d/uu;->b:Lcom/a/b/d/tv;

    invoke-static {v0, v1}, Lcom/a/b/d/sz;->a(Ljava/util/SortedMap;Lcom/a/b/d/tv;)Ljava/util/SortedMap;

    move-result-object v0

    return-object v0
.end method

.method public lastKey()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 1961
    invoke-virtual {p0}, Lcom/a/b/d/uu;->c()Ljava/util/SortedMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/SortedMap;->lastKey()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public subMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedMap;
    .registers 5

    .prologue
    .line 1965
    invoke-virtual {p0}, Lcom/a/b/d/uu;->c()Ljava/util/SortedMap;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Ljava/util/SortedMap;->subMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedMap;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/d/uu;->b:Lcom/a/b/d/tv;

    invoke-static {v0, v1}, Lcom/a/b/d/sz;->a(Ljava/util/SortedMap;Lcom/a/b/d/tv;)Ljava/util/SortedMap;

    move-result-object v0

    return-object v0
.end method

.method public tailMap(Ljava/lang/Object;)Ljava/util/SortedMap;
    .registers 4

    .prologue
    .line 1970
    invoke-virtual {p0}, Lcom/a/b/d/uu;->c()Ljava/util/SortedMap;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/SortedMap;->tailMap(Ljava/lang/Object;)Ljava/util/SortedMap;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/d/uu;->b:Lcom/a/b/d/tv;

    invoke-static {v0, v1}, Lcom/a/b/d/sz;->a(Ljava/util/SortedMap;Lcom/a/b/d/tv;)Ljava/util/SortedMap;

    move-result-object v0

    return-object v0
.end method
