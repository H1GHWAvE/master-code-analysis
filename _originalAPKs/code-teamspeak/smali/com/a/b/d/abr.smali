.class Lcom/a/b/d/abr;
.super Lcom/a/b/d/abq;
.source "SourceFile"

# interfaces
.implements Ljava/util/NavigableSet;


# annotations
.annotation build Lcom/a/b/a/c;
    a = "Navigable"
.end annotation


# direct methods
.method constructor <init>(Lcom/a/b/d/abn;)V
    .registers 2

    .prologue
    .line 91
    invoke-direct {p0, p1}, Lcom/a/b/d/abq;-><init>(Lcom/a/b/d/abn;)V

    .line 92
    return-void
.end method


# virtual methods
.method public ceiling(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 106
    .line 3057
    iget-object v0, p0, Lcom/a/b/d/abq;->b:Lcom/a/b/d/abn;

    .line 106
    sget-object v1, Lcom/a/b/d/ce;->b:Lcom/a/b/d/ce;

    invoke-interface {v0, p1, v1}, Lcom/a/b/d/abn;->c(Ljava/lang/Object;Lcom/a/b/d/ce;)Lcom/a/b/d/abn;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/b/d/abn;->h()Lcom/a/b/d/xd;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/abp;->b(Lcom/a/b/d/xd;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public descendingIterator()Ljava/util/Iterator;
    .registers 2

    .prologue
    .line 121
    invoke-virtual {p0}, Lcom/a/b/d/abr;->descendingSet()Ljava/util/NavigableSet;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/NavigableSet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public descendingSet()Ljava/util/NavigableSet;
    .registers 3

    .prologue
    .line 116
    new-instance v0, Lcom/a/b/d/abr;

    .line 5057
    iget-object v1, p0, Lcom/a/b/d/abq;->b:Lcom/a/b/d/abn;

    .line 116
    invoke-interface {v1}, Lcom/a/b/d/abn;->m()Lcom/a/b/d/abn;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/a/b/d/abr;-><init>(Lcom/a/b/d/abn;)V

    return-object v0
.end method

.method public floor(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 101
    .line 2057
    iget-object v0, p0, Lcom/a/b/d/abq;->b:Lcom/a/b/d/abn;

    .line 101
    sget-object v1, Lcom/a/b/d/ce;->b:Lcom/a/b/d/ce;

    invoke-interface {v0, p1, v1}, Lcom/a/b/d/abn;->d(Ljava/lang/Object;Lcom/a/b/d/ce;)Lcom/a/b/d/abn;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/b/d/abn;->i()Lcom/a/b/d/xd;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/abp;->b(Lcom/a/b/d/xd;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public headSet(Ljava/lang/Object;Z)Ljava/util/NavigableSet;
    .registers 6

    .prologue
    .line 144
    new-instance v0, Lcom/a/b/d/abr;

    .line 9057
    iget-object v1, p0, Lcom/a/b/d/abq;->b:Lcom/a/b/d/abn;

    .line 144
    invoke-static {p2}, Lcom/a/b/d/ce;->a(Z)Lcom/a/b/d/ce;

    move-result-object v2

    invoke-interface {v1, p1, v2}, Lcom/a/b/d/abn;->d(Ljava/lang/Object;Lcom/a/b/d/ce;)Lcom/a/b/d/abn;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/a/b/d/abr;-><init>(Lcom/a/b/d/abn;)V

    return-object v0
.end method

.method public higher(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 111
    .line 4057
    iget-object v0, p0, Lcom/a/b/d/abq;->b:Lcom/a/b/d/abn;

    .line 111
    sget-object v1, Lcom/a/b/d/ce;->a:Lcom/a/b/d/ce;

    invoke-interface {v0, p1, v1}, Lcom/a/b/d/abn;->c(Ljava/lang/Object;Lcom/a/b/d/ce;)Lcom/a/b/d/abn;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/b/d/abn;->h()Lcom/a/b/d/xd;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/abp;->b(Lcom/a/b/d/xd;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public lower(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 96
    .line 1057
    iget-object v0, p0, Lcom/a/b/d/abq;->b:Lcom/a/b/d/abn;

    .line 96
    sget-object v1, Lcom/a/b/d/ce;->a:Lcom/a/b/d/ce;

    invoke-interface {v0, p1, v1}, Lcom/a/b/d/abn;->d(Ljava/lang/Object;Lcom/a/b/d/ce;)Lcom/a/b/d/abn;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/b/d/abn;->i()Lcom/a/b/d/xd;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/abp;->b(Lcom/a/b/d/xd;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public pollFirst()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 126
    .line 6057
    iget-object v0, p0, Lcom/a/b/d/abq;->b:Lcom/a/b/d/abn;

    .line 126
    invoke-interface {v0}, Lcom/a/b/d/abn;->j()Lcom/a/b/d/xd;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/abp;->b(Lcom/a/b/d/xd;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public pollLast()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 131
    .line 7057
    iget-object v0, p0, Lcom/a/b/d/abq;->b:Lcom/a/b/d/abn;

    .line 131
    invoke-interface {v0}, Lcom/a/b/d/abn;->k()Lcom/a/b/d/xd;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/abp;->b(Lcom/a/b/d/xd;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public subSet(Ljava/lang/Object;ZLjava/lang/Object;Z)Ljava/util/NavigableSet;
    .registers 9

    .prologue
    .line 137
    new-instance v0, Lcom/a/b/d/abr;

    .line 8057
    iget-object v1, p0, Lcom/a/b/d/abq;->b:Lcom/a/b/d/abn;

    .line 137
    invoke-static {p2}, Lcom/a/b/d/ce;->a(Z)Lcom/a/b/d/ce;

    move-result-object v2

    invoke-static {p4}, Lcom/a/b/d/ce;->a(Z)Lcom/a/b/d/ce;

    move-result-object v3

    invoke-interface {v1, p1, v2, p3, v3}, Lcom/a/b/d/abn;->a(Ljava/lang/Object;Lcom/a/b/d/ce;Ljava/lang/Object;Lcom/a/b/d/ce;)Lcom/a/b/d/abn;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/a/b/d/abr;-><init>(Lcom/a/b/d/abn;)V

    return-object v0
.end method

.method public tailSet(Ljava/lang/Object;Z)Ljava/util/NavigableSet;
    .registers 6

    .prologue
    .line 150
    new-instance v0, Lcom/a/b/d/abr;

    .line 10057
    iget-object v1, p0, Lcom/a/b/d/abq;->b:Lcom/a/b/d/abn;

    .line 150
    invoke-static {p2}, Lcom/a/b/d/ce;->a(Z)Lcom/a/b/d/ce;

    move-result-object v2

    invoke-interface {v1, p1, v2}, Lcom/a/b/d/abn;->c(Ljava/lang/Object;Lcom/a/b/d/ce;)Lcom/a/b/d/abn;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/a/b/d/abr;-><init>(Lcom/a/b/d/abn;)V

    return-object v0
.end method
