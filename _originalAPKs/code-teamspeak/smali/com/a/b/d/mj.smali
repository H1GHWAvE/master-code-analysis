.class public final Lcom/a/b/d/mj;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Ljava/util/List;

.field private b:Ljava/util/Comparator;

.field private c:Ljava/util/Comparator;


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 166
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1088
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 158
    iput-object v0, p0, Lcom/a/b/d/mj;->a:Ljava/util/List;

    .line 166
    return-void
.end method

.method private a()Lcom/a/b/d/mi;
    .registers 4

    .prologue
    .line 236
    iget-object v0, p0, Lcom/a/b/d/mj;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 237
    packed-switch v0, :pswitch_data_28

    .line 244
    iget-object v0, p0, Lcom/a/b/d/mj;->a:Ljava/util/List;

    iget-object v1, p0, Lcom/a/b/d/mj;->b:Ljava/util/Comparator;

    iget-object v2, p0, Lcom/a/b/d/mj;->c:Ljava/util/Comparator;

    invoke-static {v0, v1, v2}, Lcom/a/b/d/zr;->a(Ljava/util/List;Ljava/util/Comparator;Ljava/util/Comparator;)Lcom/a/b/d/zr;

    move-result-object v0

    :goto_13
    return-object v0

    .line 239
    :pswitch_14
    invoke-static {}, Lcom/a/b/d/mi;->p()Lcom/a/b/d/mi;

    move-result-object v0

    goto :goto_13

    .line 241
    :pswitch_19
    new-instance v1, Lcom/a/b/d/aax;

    iget-object v0, p0, Lcom/a/b/d/mj;->a:Ljava/util/List;

    invoke-static {v0}, Lcom/a/b/d/mq;->b(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/adw;

    invoke-direct {v1, v0}, Lcom/a/b/d/aax;-><init>(Lcom/a/b/d/adw;)V

    move-object v0, v1

    goto :goto_13

    .line 237
    :pswitch_data_28
    .packed-switch 0x0
        :pswitch_14
        :pswitch_19
    .end packed-switch
.end method

.method private a(Lcom/a/b/d/adv;)Lcom/a/b/d/mj;
    .registers 7

    .prologue
    .line 224
    invoke-interface {p1}, Lcom/a/b/d/adv;->e()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_49

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/adw;

    .line 1202
    instance-of v2, v0, Lcom/a/b/d/aea;

    if-eqz v2, :cond_33

    .line 1203
    invoke-interface {v0}, Lcom/a/b/d/adw;->a()Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1204
    invoke-interface {v0}, Lcom/a/b/d/adw;->b()Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1205
    invoke-interface {v0}, Lcom/a/b/d/adw;->c()Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1208
    iget-object v2, p0, Lcom/a/b/d/mj;->a:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_8

    .line 1210
    :cond_33
    invoke-interface {v0}, Lcom/a/b/d/adw;->a()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0}, Lcom/a/b/d/adw;->b()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v0}, Lcom/a/b/d/adw;->c()Ljava/lang/Object;

    move-result-object v0

    .line 2191
    iget-object v4, p0, Lcom/a/b/d/mj;->a:Ljava/util/List;

    invoke-static {v2, v3, v0}, Lcom/a/b/d/mi;->b(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/adw;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_8

    .line 227
    :cond_49
    return-object p0
.end method

.method private a(Lcom/a/b/d/adw;)Lcom/a/b/d/mj;
    .registers 6

    .prologue
    .line 202
    instance-of v0, p1, Lcom/a/b/d/aea;

    if-eqz v0, :cond_1f

    .line 203
    invoke-interface {p1}, Lcom/a/b/d/adw;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 204
    invoke-interface {p1}, Lcom/a/b/d/adw;->b()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 205
    invoke-interface {p1}, Lcom/a/b/d/adw;->c()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 208
    iget-object v0, p0, Lcom/a/b/d/mj;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 212
    :goto_1e
    return-object p0

    .line 210
    :cond_1f
    invoke-interface {p1}, Lcom/a/b/d/adw;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p1}, Lcom/a/b/d/adw;->b()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {p1}, Lcom/a/b/d/adw;->c()Ljava/lang/Object;

    move-result-object v2

    .line 1191
    iget-object v3, p0, Lcom/a/b/d/mj;->a:Ljava/util/List;

    invoke-static {v0, v1, v2}, Lcom/a/b/d/mi;->b(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/adw;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1e
.end method

.method private a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/mj;
    .registers 6

    .prologue
    .line 191
    iget-object v0, p0, Lcom/a/b/d/mj;->a:Ljava/util/List;

    invoke-static {p1, p2, p3}, Lcom/a/b/d/mi;->b(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/adw;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 192
    return-object p0
.end method

.method private a(Ljava/util/Comparator;)Lcom/a/b/d/mj;
    .registers 3

    .prologue
    .line 172
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Comparator;

    iput-object v0, p0, Lcom/a/b/d/mj;->b:Ljava/util/Comparator;

    .line 173
    return-object p0
.end method

.method private b(Ljava/util/Comparator;)Lcom/a/b/d/mj;
    .registers 3

    .prologue
    .line 181
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Comparator;

    iput-object v0, p0, Lcom/a/b/d/mj;->c:Ljava/util/Comparator;

    .line 182
    return-object p0
.end method
