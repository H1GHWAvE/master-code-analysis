.class final Lcom/a/b/d/afj;
.super Lcom/a/b/d/tu;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/a/b/d/afh;


# direct methods
.method constructor <init>(Lcom/a/b/d/afh;)V
    .registers 2

    .prologue
    .line 521
    iput-object p1, p0, Lcom/a/b/d/afj;->a:Lcom/a/b/d/afh;

    invoke-direct {p0}, Lcom/a/b/d/tu;-><init>()V

    return-void
.end method


# virtual methods
.method final a()Ljava/util/Map;
    .registers 2

    .prologue
    .line 524
    iget-object v0, p0, Lcom/a/b/d/afj;->a:Lcom/a/b/d/afh;

    return-object v0
.end method

.method public final isEmpty()Z
    .registers 2

    .prologue
    .line 567
    invoke-virtual {p0}, Lcom/a/b/d/afj;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_c

    const/4 v0, 0x1

    :goto_b
    return v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method

.method public final iterator()Ljava/util/Iterator;
    .registers 4

    .prologue
    .line 529
    iget-object v0, p0, Lcom/a/b/d/afj;->a:Lcom/a/b/d/afh;

    iget-object v0, v0, Lcom/a/b/d/afh;->a:Lcom/a/b/d/afg;

    .line 1316
    iget-object v0, v0, Lcom/a/b/d/afg;->a:Lcom/a/b/d/yl;

    .line 529
    invoke-virtual {v0}, Lcom/a/b/d/yl;->f()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 530
    invoke-static {}, Lcom/a/b/d/nj;->a()Lcom/a/b/d/agi;

    move-result-object v0

    .line 536
    :goto_10
    return-object v0

    .line 532
    :cond_11
    iget-object v0, p0, Lcom/a/b/d/afj;->a:Lcom/a/b/d/afh;

    iget-object v0, v0, Lcom/a/b/d/afh;->a:Lcom/a/b/d/afg;

    iget-object v0, v0, Lcom/a/b/d/afg;->b:Lcom/a/b/d/afb;

    invoke-static {v0}, Lcom/a/b/d/afb;->a(Lcom/a/b/d/afb;)Ljava/util/NavigableMap;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/d/afj;->a:Lcom/a/b/d/afh;

    iget-object v1, v1, Lcom/a/b/d/afh;->a:Lcom/a/b/d/afg;

    .line 2316
    iget-object v1, v1, Lcom/a/b/d/afg;->a:Lcom/a/b/d/yl;

    .line 532
    iget-object v1, v1, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    invoke-interface {v0, v1}, Ljava/util/NavigableMap;->floorKey(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/d/afj;->a:Lcom/a/b/d/afh;

    iget-object v1, v1, Lcom/a/b/d/afh;->a:Lcom/a/b/d/afg;

    .line 3316
    iget-object v1, v1, Lcom/a/b/d/afg;->a:Lcom/a/b/d/yl;

    .line 532
    iget-object v1, v1, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    invoke-static {v0, v1}, Lcom/a/b/b/ca;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/dw;

    .line 534
    iget-object v1, p0, Lcom/a/b/d/afj;->a:Lcom/a/b/d/afh;

    iget-object v1, v1, Lcom/a/b/d/afh;->a:Lcom/a/b/d/afg;

    iget-object v1, v1, Lcom/a/b/d/afg;->b:Lcom/a/b/d/afb;

    invoke-static {v1}, Lcom/a/b/d/afb;->a(Lcom/a/b/d/afb;)Ljava/util/NavigableMap;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v1, v0, v2}, Ljava/util/NavigableMap;->tailMap(Ljava/lang/Object;Z)Ljava/util/NavigableMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/NavigableMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 536
    new-instance v0, Lcom/a/b/d/afk;

    invoke-direct {v0, p0, v1}, Lcom/a/b/d/afk;-><init>(Lcom/a/b/d/afj;Ljava/util/Iterator;)V

    goto :goto_10
.end method

.method public final retainAll(Ljava/util/Collection;)Z
    .registers 4

    .prologue
    .line 557
    iget-object v0, p0, Lcom/a/b/d/afj;->a:Lcom/a/b/d/afh;

    invoke-static {p1}, Lcom/a/b/b/cp;->a(Ljava/util/Collection;)Lcom/a/b/b/co;

    move-result-object v1

    invoke-static {v1}, Lcom/a/b/b/cp;->a(Lcom/a/b/b/co;)Lcom/a/b/b/co;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/a/b/d/afh;->a(Lcom/a/b/d/afh;Lcom/a/b/b/co;)Z

    move-result v0

    return v0
.end method

.method public final size()I
    .registers 2

    .prologue
    .line 562
    invoke-virtual {p0}, Lcom/a/b/d/afj;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/nj;->b(Ljava/util/Iterator;)I

    move-result v0

    return v0
.end method
