.class abstract Lcom/a/b/d/tl;
.super Lcom/a/b/d/uj;
.source "SourceFile"


# instance fields
.field final a:Ljava/util/Map;

.field final b:Lcom/a/b/b/co;


# direct methods
.method constructor <init>(Ljava/util/Map;Lcom/a/b/b/co;)V
    .registers 3

    .prologue
    .line 2565
    invoke-direct {p0}, Lcom/a/b/d/uj;-><init>()V

    .line 2566
    iput-object p1, p0, Lcom/a/b/d/tl;->a:Ljava/util/Map;

    .line 2567
    iput-object p2, p0, Lcom/a/b/d/tl;->b:Lcom/a/b/b/co;

    .line 2568
    return-void
.end method


# virtual methods
.method final b(Ljava/lang/Object;Ljava/lang/Object;)Z
    .registers 5
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2575
    iget-object v0, p0, Lcom/a/b/d/tl;->b:Lcom/a/b/b/co;

    invoke-static {p1, p2}, Lcom/a/b/d/sz;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/a/b/b/co;->a(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method final c_()Ljava/util/Collection;
    .registers 4

    .prologue
    .line 2609
    new-instance v0, Lcom/a/b/d/ui;

    iget-object v1, p0, Lcom/a/b/d/tl;->a:Ljava/util/Map;

    iget-object v2, p0, Lcom/a/b/d/tl;->b:Lcom/a/b/b/co;

    invoke-direct {v0, p0, v1, v2}, Lcom/a/b/d/ui;-><init>(Ljava/util/Map;Ljava/util/Map;Lcom/a/b/b/co;)V

    return-object v0
.end method

.method public containsKey(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 2591
    iget-object v0, p0, Lcom/a/b/d/tl;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_16

    iget-object v0, p0, Lcom/a/b/d/tl;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/a/b/d/tl;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_16

    const/4 v0, 0x1

    :goto_15
    return v0

    :cond_16
    const/4 v0, 0x0

    goto :goto_15
.end method

.method public get(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 2595
    iget-object v0, p0, Lcom/a/b/d/tl;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 2596
    if-eqz v0, :cond_f

    invoke-virtual {p0, p1, v0}, Lcom/a/b/d/tl;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_f

    :goto_e
    return-object v0

    :cond_f
    const/4 v0, 0x0

    goto :goto_e
.end method

.method public isEmpty()Z
    .registers 2

    .prologue
    .line 2600
    invoke-virtual {p0}, Lcom/a/b/d/tl;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 2579
    invoke-virtual {p0, p1, p2}, Lcom/a/b/d/tl;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Lcom/a/b/b/cn;->a(Z)V

    .line 2580
    iget-object v0, p0, Lcom/a/b/d/tl;->a:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public putAll(Ljava/util/Map;)V
    .registers 5

    .prologue
    .line 2584
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_24

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2585
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v2, v0}, Lcom/a/b/d/tl;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Lcom/a/b/b/cn;->a(Z)V

    goto :goto_8

    .line 2587
    :cond_24
    iget-object v0, p0, Lcom/a/b/d/tl;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 2588
    return-void
.end method

.method public remove(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 2604
    invoke-virtual {p0, p1}, Lcom/a/b/d/tl;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/a/b/d/tl;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    :goto_c
    return-object v0

    :cond_d
    const/4 v0, 0x0

    goto :goto_c
.end method
