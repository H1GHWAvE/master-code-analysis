.class final Lcom/a/b/d/aar;
.super Ljava/util/AbstractSet;
.source "SourceFile"


# instance fields
.field private final a:Lcom/a/b/d/jt;

.field private final b:I


# direct methods
.method constructor <init>(Lcom/a/b/d/jt;I)V
    .registers 3

    .prologue
    .line 1236
    invoke-direct {p0}, Ljava/util/AbstractSet;-><init>()V

    .line 1237
    iput-object p1, p0, Lcom/a/b/d/aar;->a:Lcom/a/b/d/jt;

    .line 1238
    iput p2, p0, Lcom/a/b/d/aar;->b:I

    .line 1239
    return-void
.end method

.method static synthetic a(Lcom/a/b/d/aar;)Lcom/a/b/d/jt;
    .registers 2

    .prologue
    .line 1232
    iget-object v0, p0, Lcom/a/b/d/aar;->a:Lcom/a/b/d/jt;

    return-object v0
.end method

.method static synthetic b(Lcom/a/b/d/aar;)I
    .registers 2

    .prologue
    .line 1232
    iget v0, p0, Lcom/a/b/d/aar;->b:I

    return v0
.end method


# virtual methods
.method public final contains(Ljava/lang/Object;)Z
    .registers 5
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    .line 1271
    iget-object v0, p0, Lcom/a/b/d/aar;->a:Lcom/a/b/d/jt;

    invoke-virtual {v0, p1}, Lcom/a/b/d/jt;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 1272
    if-eqz v0, :cond_18

    iget v2, p0, Lcom/a/b/d/aar;->b:I

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    shl-int v0, v1, v0

    and-int/2addr v0, v2

    if-eqz v0, :cond_18

    move v0, v1

    :goto_17
    return v0

    :cond_18
    const/4 v0, 0x0

    goto :goto_17
.end method

.method public final iterator()Ljava/util/Iterator;
    .registers 2

    .prologue
    .line 1243
    new-instance v0, Lcom/a/b/d/aas;

    invoke-direct {v0, p0}, Lcom/a/b/d/aas;-><init>(Lcom/a/b/d/aar;)V

    return-object v0
.end method

.method public final size()I
    .registers 2

    .prologue
    .line 1266
    iget v0, p0, Lcom/a/b/d/aar;->b:I

    invoke-static {v0}, Ljava/lang/Integer;->bitCount(I)I

    move-result v0

    return v0
.end method
