.class final Lcom/a/b/d/afs;
.super Lcom/a/b/d/j;
.source "SourceFile"


# instance fields
.field a:Lcom/a/b/d/dw;

.field final synthetic b:Lcom/a/b/d/dw;

.field final synthetic c:Lcom/a/b/d/yi;

.field final synthetic d:Lcom/a/b/d/afq;


# direct methods
.method constructor <init>(Lcom/a/b/d/afq;Lcom/a/b/d/dw;Lcom/a/b/d/yi;)V
    .registers 5

    .prologue
    .line 538
    iput-object p1, p0, Lcom/a/b/d/afs;->d:Lcom/a/b/d/afq;

    iput-object p2, p0, Lcom/a/b/d/afs;->b:Lcom/a/b/d/dw;

    iput-object p3, p0, Lcom/a/b/d/afs;->c:Lcom/a/b/d/yi;

    invoke-direct {p0}, Lcom/a/b/d/j;-><init>()V

    .line 539
    iget-object v0, p0, Lcom/a/b/d/afs;->b:Lcom/a/b/d/dw;

    iput-object v0, p0, Lcom/a/b/d/afs;->a:Lcom/a/b/d/dw;

    return-void
.end method

.method private c()Ljava/util/Map$Entry;
    .registers 5

    .prologue
    const/4 v1, 0x0

    .line 543
    iget-object v0, p0, Lcom/a/b/d/afs;->a:Lcom/a/b/d/dw;

    invoke-static {}, Lcom/a/b/d/dw;->d()Lcom/a/b/d/dw;

    move-result-object v2

    if-ne v0, v2, :cond_e

    .line 544
    invoke-virtual {p0}, Lcom/a/b/d/afs;->b()Ljava/lang/Object;

    move-object v0, v1

    .line 559
    :goto_d
    return-object v0

    .line 545
    :cond_e
    iget-object v0, p0, Lcom/a/b/d/afs;->c:Lcom/a/b/d/yi;

    invoke-interface {v0}, Lcom/a/b/d/yi;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_41

    .line 546
    iget-object v0, p0, Lcom/a/b/d/afs;->c:Lcom/a/b/d/yi;

    invoke-interface {v0}, Lcom/a/b/d/yi;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/yl;

    .line 547
    iget-object v2, v0, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    iget-object v3, p0, Lcom/a/b/d/afs;->a:Lcom/a/b/d/dw;

    invoke-static {v2, v3}, Lcom/a/b/d/yl;->a(Lcom/a/b/d/dw;Lcom/a/b/d/dw;)Lcom/a/b/d/yl;

    move-result-object v2

    .line 549
    iget-object v0, v0, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    iput-object v0, p0, Lcom/a/b/d/afs;->a:Lcom/a/b/d/dw;

    .line 550
    iget-object v0, p0, Lcom/a/b/d/afs;->d:Lcom/a/b/d/afq;

    invoke-static {v0}, Lcom/a/b/d/afq;->a(Lcom/a/b/d/afq;)Lcom/a/b/d/yl;

    move-result-object v0

    iget-object v0, v0, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    iget-object v3, v2, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    invoke-virtual {v0, v3}, Lcom/a/b/d/dw;->a(Ljava/lang/Comparable;)Z

    move-result v0

    if-eqz v0, :cond_6c

    .line 551
    iget-object v0, v2, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    invoke-static {v0, v2}, Lcom/a/b/d/sz;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v0

    goto :goto_d

    .line 553
    :cond_41
    iget-object v0, p0, Lcom/a/b/d/afs;->d:Lcom/a/b/d/afq;

    invoke-static {v0}, Lcom/a/b/d/afq;->a(Lcom/a/b/d/afq;)Lcom/a/b/d/yl;

    move-result-object v0

    iget-object v0, v0, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    invoke-static {}, Lcom/a/b/d/dw;->d()Lcom/a/b/d/dw;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/a/b/d/dw;->a(Ljava/lang/Comparable;)Z

    move-result v0

    if-eqz v0, :cond_6c

    .line 554
    invoke-static {}, Lcom/a/b/d/dw;->d()Lcom/a/b/d/dw;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/d/afs;->a:Lcom/a/b/d/dw;

    invoke-static {v0, v1}, Lcom/a/b/d/yl;->a(Lcom/a/b/d/dw;Lcom/a/b/d/dw;)Lcom/a/b/d/yl;

    move-result-object v0

    .line 556
    invoke-static {}, Lcom/a/b/d/dw;->d()Lcom/a/b/d/dw;

    move-result-object v1

    iput-object v1, p0, Lcom/a/b/d/afs;->a:Lcom/a/b/d/dw;

    .line 557
    invoke-static {}, Lcom/a/b/d/dw;->d()Lcom/a/b/d/dw;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/a/b/d/sz;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v0

    goto :goto_d

    .line 559
    :cond_6c
    invoke-virtual {p0}, Lcom/a/b/d/afs;->b()Ljava/lang/Object;

    move-object v0, v1

    goto :goto_d
.end method


# virtual methods
.method protected final synthetic a()Ljava/lang/Object;
    .registers 5

    .prologue
    const/4 v1, 0x0

    .line 538
    .line 1543
    iget-object v0, p0, Lcom/a/b/d/afs;->a:Lcom/a/b/d/dw;

    invoke-static {}, Lcom/a/b/d/dw;->d()Lcom/a/b/d/dw;

    move-result-object v2

    if-ne v0, v2, :cond_e

    .line 1544
    invoke-virtual {p0}, Lcom/a/b/d/afs;->b()Ljava/lang/Object;

    move-object v0, v1

    .line 1557
    :goto_d
    return-object v0

    .line 1545
    :cond_e
    iget-object v0, p0, Lcom/a/b/d/afs;->c:Lcom/a/b/d/yi;

    invoke-interface {v0}, Lcom/a/b/d/yi;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_41

    .line 1546
    iget-object v0, p0, Lcom/a/b/d/afs;->c:Lcom/a/b/d/yi;

    invoke-interface {v0}, Lcom/a/b/d/yi;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/yl;

    .line 1547
    iget-object v2, v0, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    iget-object v3, p0, Lcom/a/b/d/afs;->a:Lcom/a/b/d/dw;

    invoke-static {v2, v3}, Lcom/a/b/d/yl;->a(Lcom/a/b/d/dw;Lcom/a/b/d/dw;)Lcom/a/b/d/yl;

    move-result-object v2

    .line 1549
    iget-object v0, v0, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    iput-object v0, p0, Lcom/a/b/d/afs;->a:Lcom/a/b/d/dw;

    .line 1550
    iget-object v0, p0, Lcom/a/b/d/afs;->d:Lcom/a/b/d/afq;

    invoke-static {v0}, Lcom/a/b/d/afq;->a(Lcom/a/b/d/afq;)Lcom/a/b/d/yl;

    move-result-object v0

    iget-object v0, v0, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    iget-object v3, v2, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    invoke-virtual {v0, v3}, Lcom/a/b/d/dw;->a(Ljava/lang/Comparable;)Z

    move-result v0

    if-eqz v0, :cond_6c

    .line 1551
    iget-object v0, v2, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    invoke-static {v0, v2}, Lcom/a/b/d/sz;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v0

    goto :goto_d

    .line 1553
    :cond_41
    iget-object v0, p0, Lcom/a/b/d/afs;->d:Lcom/a/b/d/afq;

    invoke-static {v0}, Lcom/a/b/d/afq;->a(Lcom/a/b/d/afq;)Lcom/a/b/d/yl;

    move-result-object v0

    iget-object v0, v0, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    invoke-static {}, Lcom/a/b/d/dw;->d()Lcom/a/b/d/dw;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/a/b/d/dw;->a(Ljava/lang/Comparable;)Z

    move-result v0

    if-eqz v0, :cond_6c

    .line 1554
    invoke-static {}, Lcom/a/b/d/dw;->d()Lcom/a/b/d/dw;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/d/afs;->a:Lcom/a/b/d/dw;

    invoke-static {v0, v1}, Lcom/a/b/d/yl;->a(Lcom/a/b/d/dw;Lcom/a/b/d/dw;)Lcom/a/b/d/yl;

    move-result-object v0

    .line 1556
    invoke-static {}, Lcom/a/b/d/dw;->d()Lcom/a/b/d/dw;

    move-result-object v1

    iput-object v1, p0, Lcom/a/b/d/afs;->a:Lcom/a/b/d/dw;

    .line 1557
    invoke-static {}, Lcom/a/b/d/dw;->d()Lcom/a/b/d/dw;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/a/b/d/sz;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v0

    goto :goto_d

    .line 1559
    :cond_6c
    invoke-virtual {p0}, Lcom/a/b/d/afs;->b()Ljava/lang/Object;

    move-object v0, v1

    .line 538
    goto :goto_d
.end method
