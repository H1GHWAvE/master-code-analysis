.class final Lcom/a/b/d/cs;
.super Lcom/a/b/d/j;
.source "SourceFile"


# instance fields
.field final a:Ljava/util/List;

.field final b:[I

.field final c:[I

.field d:I


# direct methods
.method constructor <init>(Ljava/util/List;)V
    .registers 4

    .prologue
    .line 602
    invoke-direct {p0}, Lcom/a/b/d/j;-><init>()V

    .line 603
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/a/b/d/cs;->a:Ljava/util/List;

    .line 604
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    .line 605
    new-array v1, v0, [I

    iput-object v1, p0, Lcom/a/b/d/cs;->b:[I

    .line 606
    new-array v0, v0, [I

    iput-object v0, p0, Lcom/a/b/d/cs;->c:[I

    .line 607
    iget-object v0, p0, Lcom/a/b/d/cs;->b:[I

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([II)V

    .line 608
    iget-object v0, p0, Lcom/a/b/d/cs;->c:[I

    const/4 v1, 0x1

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([II)V

    .line 609
    const v0, 0x7fffffff

    iput v0, p0, Lcom/a/b/d/cs;->d:I

    .line 610
    return-void
.end method

.method private c()Ljava/util/List;
    .registers 8

    .prologue
    .line 613
    iget v0, p0, Lcom/a/b/d/cs;->d:I

    if-gtz v0, :cond_9

    .line 614
    invoke-virtual {p0}, Lcom/a/b/d/cs;->b()Ljava/lang/Object;

    const/4 v0, 0x0

    .line 618
    :goto_8
    return-object v0

    .line 616
    :cond_9
    iget-object v0, p0, Lcom/a/b/d/cs;->a:Ljava/util/List;

    invoke-static {v0}, Lcom/a/b/d/jl;->a(Ljava/util/Collection;)Lcom/a/b/d/jl;

    move-result-object v1

    .line 1622
    iget-object v0, p0, Lcom/a/b/d/cs;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/a/b/d/cs;->d:I

    .line 1623
    const/4 v0, 0x0

    .line 1627
    iget v2, p0, Lcom/a/b/d/cs;->d:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_5b

    .line 1632
    :goto_1f
    iget-object v2, p0, Lcom/a/b/d/cs;->b:[I

    iget v3, p0, Lcom/a/b/d/cs;->d:I

    aget v2, v2, v3

    iget-object v3, p0, Lcom/a/b/d/cs;->c:[I

    iget v4, p0, Lcom/a/b/d/cs;->d:I

    aget v3, v3, v4

    add-int/2addr v2, v3

    .line 1633
    if-gez v2, :cond_32

    .line 1634
    invoke-direct {p0}, Lcom/a/b/d/cs;->e()V

    goto :goto_1f

    .line 1637
    :cond_32
    iget v3, p0, Lcom/a/b/d/cs;->d:I

    add-int/lit8 v3, v3, 0x1

    if-ne v2, v3, :cond_42

    .line 1638
    iget v2, p0, Lcom/a/b/d/cs;->d:I

    if-eqz v2, :cond_5b

    .line 1641
    add-int/lit8 v0, v0, 0x1

    .line 1642
    invoke-direct {p0}, Lcom/a/b/d/cs;->e()V

    goto :goto_1f

    .line 1646
    :cond_42
    iget-object v3, p0, Lcom/a/b/d/cs;->a:Ljava/util/List;

    iget v4, p0, Lcom/a/b/d/cs;->d:I

    iget-object v5, p0, Lcom/a/b/d/cs;->b:[I

    iget v6, p0, Lcom/a/b/d/cs;->d:I

    aget v5, v5, v6

    sub-int/2addr v4, v5

    add-int/2addr v4, v0

    iget v5, p0, Lcom/a/b/d/cs;->d:I

    sub-int/2addr v5, v2

    add-int/2addr v0, v5

    invoke-static {v3, v4, v0}, Ljava/util/Collections;->swap(Ljava/util/List;II)V

    .line 1647
    iget-object v0, p0, Lcom/a/b/d/cs;->b:[I

    iget v3, p0, Lcom/a/b/d/cs;->d:I

    aput v2, v0, v3

    :cond_5b
    move-object v0, v1

    .line 618
    goto :goto_8
.end method

.method private d()V
    .registers 7

    .prologue
    .line 622
    iget-object v0, p0, Lcom/a/b/d/cs;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/a/b/d/cs;->d:I

    .line 623
    const/4 v0, 0x0

    .line 627
    iget v1, p0, Lcom/a/b/d/cs;->d:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_11

    .line 650
    :cond_10
    :goto_10
    return-void

    .line 632
    :cond_11
    :goto_11
    iget-object v1, p0, Lcom/a/b/d/cs;->b:[I

    iget v2, p0, Lcom/a/b/d/cs;->d:I

    aget v1, v1, v2

    iget-object v2, p0, Lcom/a/b/d/cs;->c:[I

    iget v3, p0, Lcom/a/b/d/cs;->d:I

    aget v2, v2, v3

    add-int/2addr v1, v2

    .line 633
    if-gez v1, :cond_24

    .line 634
    invoke-direct {p0}, Lcom/a/b/d/cs;->e()V

    goto :goto_11

    .line 637
    :cond_24
    iget v2, p0, Lcom/a/b/d/cs;->d:I

    add-int/lit8 v2, v2, 0x1

    if-ne v1, v2, :cond_34

    .line 638
    iget v1, p0, Lcom/a/b/d/cs;->d:I

    if-eqz v1, :cond_10

    .line 641
    add-int/lit8 v0, v0, 0x1

    .line 642
    invoke-direct {p0}, Lcom/a/b/d/cs;->e()V

    goto :goto_11

    .line 646
    :cond_34
    iget-object v2, p0, Lcom/a/b/d/cs;->a:Ljava/util/List;

    iget v3, p0, Lcom/a/b/d/cs;->d:I

    iget-object v4, p0, Lcom/a/b/d/cs;->b:[I

    iget v5, p0, Lcom/a/b/d/cs;->d:I

    aget v4, v4, v5

    sub-int/2addr v3, v4

    add-int/2addr v3, v0

    iget v4, p0, Lcom/a/b/d/cs;->d:I

    sub-int/2addr v4, v1

    add-int/2addr v0, v4

    invoke-static {v2, v3, v0}, Ljava/util/Collections;->swap(Ljava/util/List;II)V

    .line 647
    iget-object v0, p0, Lcom/a/b/d/cs;->b:[I

    iget v2, p0, Lcom/a/b/d/cs;->d:I

    aput v1, v0, v2

    goto :goto_10
.end method

.method private e()V
    .registers 5

    .prologue
    .line 653
    iget-object v0, p0, Lcom/a/b/d/cs;->c:[I

    iget v1, p0, Lcom/a/b/d/cs;->d:I

    iget-object v2, p0, Lcom/a/b/d/cs;->c:[I

    iget v3, p0, Lcom/a/b/d/cs;->d:I

    aget v2, v2, v3

    neg-int v2, v2

    aput v2, v0, v1

    .line 654
    iget v0, p0, Lcom/a/b/d/cs;->d:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/a/b/d/cs;->d:I

    .line 655
    return-void
.end method


# virtual methods
.method protected final synthetic a()Ljava/lang/Object;
    .registers 8

    .prologue
    .line 595
    .line 2613
    iget v0, p0, Lcom/a/b/d/cs;->d:I

    if-gtz v0, :cond_9

    .line 2614
    invoke-virtual {p0}, Lcom/a/b/d/cs;->b()Ljava/lang/Object;

    const/4 v0, 0x0

    :goto_8
    return-object v0

    .line 2616
    :cond_9
    iget-object v0, p0, Lcom/a/b/d/cs;->a:Ljava/util/List;

    invoke-static {v0}, Lcom/a/b/d/jl;->a(Ljava/util/Collection;)Lcom/a/b/d/jl;

    move-result-object v1

    .line 2622
    iget-object v0, p0, Lcom/a/b/d/cs;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/a/b/d/cs;->d:I

    .line 2623
    const/4 v0, 0x0

    .line 2627
    iget v2, p0, Lcom/a/b/d/cs;->d:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_5b

    .line 2632
    :goto_1f
    iget-object v2, p0, Lcom/a/b/d/cs;->b:[I

    iget v3, p0, Lcom/a/b/d/cs;->d:I

    aget v2, v2, v3

    iget-object v3, p0, Lcom/a/b/d/cs;->c:[I

    iget v4, p0, Lcom/a/b/d/cs;->d:I

    aget v3, v3, v4

    add-int/2addr v2, v3

    .line 2633
    if-gez v2, :cond_32

    .line 2634
    invoke-direct {p0}, Lcom/a/b/d/cs;->e()V

    goto :goto_1f

    .line 2637
    :cond_32
    iget v3, p0, Lcom/a/b/d/cs;->d:I

    add-int/lit8 v3, v3, 0x1

    if-ne v2, v3, :cond_42

    .line 2638
    iget v2, p0, Lcom/a/b/d/cs;->d:I

    if-eqz v2, :cond_5b

    .line 2641
    add-int/lit8 v0, v0, 0x1

    .line 2642
    invoke-direct {p0}, Lcom/a/b/d/cs;->e()V

    goto :goto_1f

    .line 2646
    :cond_42
    iget-object v3, p0, Lcom/a/b/d/cs;->a:Ljava/util/List;

    iget v4, p0, Lcom/a/b/d/cs;->d:I

    iget-object v5, p0, Lcom/a/b/d/cs;->b:[I

    iget v6, p0, Lcom/a/b/d/cs;->d:I

    aget v5, v5, v6

    sub-int/2addr v4, v5

    add-int/2addr v4, v0

    iget v5, p0, Lcom/a/b/d/cs;->d:I

    sub-int/2addr v5, v2

    add-int/2addr v0, v5

    invoke-static {v3, v4, v0}, Ljava/util/Collections;->swap(Ljava/util/List;II)V

    .line 2647
    iget-object v0, p0, Lcom/a/b/d/cs;->b:[I

    iget v3, p0, Lcom/a/b/d/cs;->d:I

    aput v2, v0, v3

    :cond_5b
    move-object v0, v1

    .line 595
    goto :goto_8
.end method
