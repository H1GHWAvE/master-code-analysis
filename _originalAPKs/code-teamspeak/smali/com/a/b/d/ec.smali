.class final Lcom/a/b/d/ec;
.super Lcom/a/b/d/zr;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/b;
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field private final a:Lcom/a/b/d/jt;

.field private final b:Lcom/a/b/d/jt;

.field private final c:Lcom/a/b/d/jt;

.field private final d:Lcom/a/b/d/jt;

.field private final e:[I

.field private final f:[I

.field private final g:[[Ljava/lang/Object;

.field private final h:[I

.field private final i:[I


# direct methods
.method constructor <init>(Lcom/a/b/d/jl;Lcom/a/b/d/lo;Lcom/a/b/d/lo;)V
    .registers 16

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/a/b/d/zr;-><init>()V

    .line 56
    invoke-virtual {p2}, Lcom/a/b/d/lo;->size()I

    move-result v0

    invoke-virtual {p3}, Lcom/a/b/d/lo;->size()I

    move-result v1

    filled-new-array {v0, v1}, [I

    move-result-object v0

    const-class v1, Ljava/lang/Object;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[Ljava/lang/Object;

    check-cast v0, [[Ljava/lang/Object;

    .line 57
    iput-object v0, p0, Lcom/a/b/d/ec;->g:[[Ljava/lang/Object;

    .line 58
    invoke-static {p2}, Lcom/a/b/d/ec;->a(Lcom/a/b/d/lo;)Lcom/a/b/d/jt;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/ec;->a:Lcom/a/b/d/jt;

    .line 59
    invoke-static {p3}, Lcom/a/b/d/ec;->a(Lcom/a/b/d/lo;)Lcom/a/b/d/jt;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/ec;->b:Lcom/a/b/d/jt;

    .line 60
    iget-object v0, p0, Lcom/a/b/d/ec;->a:Lcom/a/b/d/jt;

    invoke-virtual {v0}, Lcom/a/b/d/jt;->size()I

    move-result v0

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/a/b/d/ec;->e:[I

    .line 61
    iget-object v0, p0, Lcom/a/b/d/ec;->b:Lcom/a/b/d/jt;

    invoke-virtual {v0}, Lcom/a/b/d/jt;->size()I

    move-result v0

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/a/b/d/ec;->f:[I

    .line 62
    invoke-virtual {p1}, Lcom/a/b/d/jl;->size()I

    move-result v0

    new-array v3, v0, [I

    .line 63
    invoke-virtual {p1}, Lcom/a/b/d/jl;->size()I

    move-result v0

    new-array v4, v0, [I

    .line 64
    const/4 v0, 0x0

    move v2, v0

    :goto_49
    invoke-virtual {p1}, Lcom/a/b/d/jl;->size()I

    move-result v0

    if-ge v2, v0, :cond_b0

    .line 65
    invoke-virtual {p1, v2}, Lcom/a/b/d/jl;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/adw;

    .line 66
    invoke-interface {v0}, Lcom/a/b/d/adw;->a()Ljava/lang/Object;

    move-result-object v5

    .line 67
    invoke-interface {v0}, Lcom/a/b/d/adw;->b()Ljava/lang/Object;

    move-result-object v6

    .line 68
    iget-object v1, p0, Lcom/a/b/d/ec;->a:Lcom/a/b/d/jt;

    invoke-virtual {v1, v5}, Lcom/a/b/d/jt;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v7

    .line 69
    iget-object v1, p0, Lcom/a/b/d/ec;->b:Lcom/a/b/d/jt;

    invoke-virtual {v1, v6}, Lcom/a/b/d/jt;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v8

    .line 70
    iget-object v1, p0, Lcom/a/b/d/ec;->g:[[Ljava/lang/Object;

    aget-object v1, v1, v7

    aget-object v1, v1, v8

    .line 71
    if-nez v1, :cond_ae

    const/4 v1, 0x1

    :goto_7e
    const-string v9, "duplicate key: (%s, %s)"

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object v5, v10, v11

    const/4 v5, 0x1

    aput-object v6, v10, v5

    invoke-static {v1, v9, v10}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 72
    iget-object v1, p0, Lcom/a/b/d/ec;->g:[[Ljava/lang/Object;

    aget-object v1, v1, v7

    invoke-interface {v0}, Lcom/a/b/d/adw;->c()Ljava/lang/Object;

    move-result-object v0

    aput-object v0, v1, v8

    .line 73
    iget-object v0, p0, Lcom/a/b/d/ec;->e:[I

    aget v1, v0, v7

    add-int/lit8 v1, v1, 0x1

    aput v1, v0, v7

    .line 74
    iget-object v0, p0, Lcom/a/b/d/ec;->f:[I

    aget v1, v0, v8

    add-int/lit8 v1, v1, 0x1

    aput v1, v0, v8

    .line 75
    aput v7, v3, v2

    .line 76
    aput v8, v4, v2

    .line 64
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_49

    .line 71
    :cond_ae
    const/4 v1, 0x0

    goto :goto_7e

    .line 78
    :cond_b0
    iput-object v3, p0, Lcom/a/b/d/ec;->h:[I

    .line 79
    iput-object v4, p0, Lcom/a/b/d/ec;->i:[I

    .line 80
    new-instance v0, Lcom/a/b/d/ek;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/a/b/d/ek;-><init>(Lcom/a/b/d/ec;B)V

    iput-object v0, p0, Lcom/a/b/d/ec;->c:Lcom/a/b/d/jt;

    .line 81
    new-instance v0, Lcom/a/b/d/ef;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/a/b/d/ef;-><init>(Lcom/a/b/d/ec;B)V

    iput-object v0, p0, Lcom/a/b/d/ec;->d:Lcom/a/b/d/jt;

    .line 82
    return-void
.end method

.method private static a(Lcom/a/b/d/lo;)Lcom/a/b/d/jt;
    .registers 6

    .prologue
    .line 44
    invoke-static {}, Lcom/a/b/d/jt;->l()Lcom/a/b/d/ju;

    move-result-object v1

    .line 45
    const/4 v0, 0x0

    .line 46
    invoke-virtual {p0}, Lcom/a/b/d/lo;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_9
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1d

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 47
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lcom/a/b/d/ju;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/ju;

    .line 48
    add-int/lit8 v0, v0, 0x1

    .line 49
    goto :goto_9

    .line 50
    :cond_1d
    invoke-virtual {v1}, Lcom/a/b/d/ju;->a()Lcom/a/b/d/jt;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/a/b/d/ec;)[I
    .registers 2

    .prologue
    .line 31
    iget-object v0, p0, Lcom/a/b/d/ec;->e:[I

    return-object v0
.end method

.method static synthetic b(Lcom/a/b/d/ec;)Lcom/a/b/d/jt;
    .registers 2

    .prologue
    .line 31
    iget-object v0, p0, Lcom/a/b/d/ec;->b:Lcom/a/b/d/jt;

    return-object v0
.end method

.method static synthetic c(Lcom/a/b/d/ec;)[[Ljava/lang/Object;
    .registers 2

    .prologue
    .line 31
    iget-object v0, p0, Lcom/a/b/d/ec;->g:[[Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic d(Lcom/a/b/d/ec;)[I
    .registers 2

    .prologue
    .line 31
    iget-object v0, p0, Lcom/a/b/d/ec;->f:[I

    return-object v0
.end method

.method static synthetic e(Lcom/a/b/d/ec;)Lcom/a/b/d/jt;
    .registers 2

    .prologue
    .line 31
    iget-object v0, p0, Lcom/a/b/d/ec;->a:Lcom/a/b/d/jt;

    return-object v0
.end method


# virtual methods
.method final a(I)Lcom/a/b/d/adw;
    .registers 7

    .prologue
    .line 266
    iget-object v0, p0, Lcom/a/b/d/ec;->h:[I

    aget v0, v0, p1

    .line 267
    iget-object v1, p0, Lcom/a/b/d/ec;->i:[I

    aget v1, v1, p1

    .line 1314
    invoke-virtual {p0}, Lcom/a/b/d/mi;->o()Lcom/a/b/d/jt;

    move-result-object v2

    invoke-virtual {v2}, Lcom/a/b/d/jt;->g()Lcom/a/b/d/lo;

    move-result-object v2

    .line 268
    invoke-virtual {v2}, Lcom/a/b/d/lo;->f()Lcom/a/b/d/jl;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/a/b/d/jl;->get(I)Ljava/lang/Object;

    move-result-object v2

    .line 2290
    invoke-virtual {p0}, Lcom/a/b/d/mi;->n()Lcom/a/b/d/jt;

    move-result-object v3

    invoke-virtual {v3}, Lcom/a/b/d/jt;->g()Lcom/a/b/d/lo;

    move-result-object v3

    .line 269
    invoke-virtual {v3}, Lcom/a/b/d/lo;->f()Lcom/a/b/d/jl;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/a/b/d/jl;->get(I)Ljava/lang/Object;

    move-result-object v3

    .line 270
    iget-object v4, p0, Lcom/a/b/d/ec;->g:[[Ljava/lang/Object;

    aget-object v0, v4, v0

    aget-object v0, v0, v1

    .line 271
    invoke-static {v2, v3, v0}, Lcom/a/b/d/ec;->b(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/adw;

    move-result-object v0

    return-object v0
.end method

.method final b(I)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 276
    iget-object v0, p0, Lcom/a/b/d/ec;->g:[[Ljava/lang/Object;

    iget-object v1, p0, Lcom/a/b/d/ec;->h:[I

    aget v1, v1, p1

    aget-object v0, v0, v1

    iget-object v1, p0, Lcom/a/b/d/ec;->i:[I

    aget v1, v1, p1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 6
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 253
    iget-object v0, p0, Lcom/a/b/d/ec;->a:Lcom/a/b/d/jt;

    invoke-virtual {v0, p1}, Lcom/a/b/d/jt;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 254
    iget-object v1, p0, Lcom/a/b/d/ec;->b:Lcom/a/b/d/jt;

    invoke-virtual {v1, p2}, Lcom/a/b/d/jt;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 255
    if-eqz v0, :cond_14

    if-nez v1, :cond_16

    :cond_14
    const/4 v0, 0x0

    :goto_15
    return-object v0

    :cond_16
    iget-object v2, p0, Lcom/a/b/d/ec;->g:[[Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    aget-object v0, v2, v0

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    aget-object v0, v0, v1

    goto :goto_15
.end method

.method public final k()I
    .registers 2

    .prologue
    .line 261
    iget-object v0, p0, Lcom/a/b/d/ec;->h:[I

    array-length v0, v0

    return v0
.end method

.method public final bridge synthetic l()Ljava/util/Map;
    .registers 2

    .prologue
    .line 29
    .line 3243
    iget-object v0, p0, Lcom/a/b/d/ec;->d:Lcom/a/b/d/jt;

    .line 29
    return-object v0
.end method

.method public final bridge synthetic m()Ljava/util/Map;
    .registers 2

    .prologue
    .line 29
    .line 3248
    iget-object v0, p0, Lcom/a/b/d/ec;->c:Lcom/a/b/d/jt;

    .line 29
    return-object v0
.end method

.method public final n()Lcom/a/b/d/jt;
    .registers 2

    .prologue
    .line 243
    iget-object v0, p0, Lcom/a/b/d/ec;->d:Lcom/a/b/d/jt;

    return-object v0
.end method

.method public final o()Lcom/a/b/d/jt;
    .registers 2

    .prologue
    .line 248
    iget-object v0, p0, Lcom/a/b/d/ec;->c:Lcom/a/b/d/jt;

    return-object v0
.end method
