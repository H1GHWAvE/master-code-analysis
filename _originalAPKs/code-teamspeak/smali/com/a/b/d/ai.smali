.class abstract Lcom/a/b/d/ai;
.super Lcom/a/b/d/as;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation build Lcom/a/b/a/b;
    b = true
.end annotation


# static fields
.field private static final c:J = -0x1f3c5464cd7009c6L
    .annotation build Lcom/a/b/a/c;
        a = "not needed in emulated source."
    .end annotation
.end field


# instance fields
.field transient a:Ljava/util/Map;

.field private transient b:J


# direct methods
.method protected constructor <init>(Ljava/util/Map;)V
    .registers 4

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/a/b/d/as;-><init>()V

    .line 62
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    iput-object v0, p0, Lcom/a/b/d/ai;->a:Ljava/util/Map;

    .line 63
    invoke-super {p0}, Lcom/a/b/d/as;->size()I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/a/b/d/ai;->b:J

    .line 64
    return-void
.end method

.method private static a(Lcom/a/b/d/dv;I)I
    .registers 3

    .prologue
    .line 285
    if-nez p0, :cond_4

    .line 286
    const/4 v0, 0x0

    .line 289
    :goto_3
    return v0

    :cond_4
    invoke-virtual {p0, p1}, Lcom/a/b/d/dv;->b(I)I

    move-result v0

    goto :goto_3
.end method

.method static synthetic a(Lcom/a/b/d/ai;J)J
    .registers 6

    .prologue
    .line 48
    iget-wide v0, p0, Lcom/a/b/d/ai;->b:J

    sub-long/2addr v0, p1

    iput-wide v0, p0, Lcom/a/b/d/ai;->b:J

    return-wide v0
.end method

.method static synthetic a(Lcom/a/b/d/ai;)Ljava/util/Map;
    .registers 2

    .prologue
    .line 48
    iget-object v0, p0, Lcom/a/b/d/ai;->a:Ljava/util/Map;

    return-object v0
.end method

.method private a(Ljava/util/Map;)V
    .registers 2

    .prologue
    .line 68
    iput-object p1, p0, Lcom/a/b/d/ai;->a:Ljava/util/Map;

    .line 69
    return-void
.end method

.method static synthetic b(Lcom/a/b/d/ai;)J
    .registers 5

    .prologue
    .line 48
    iget-wide v0, p0, Lcom/a/b/d/ai;->b:J

    const-wide/16 v2, 0x1

    sub-long v2, v0, v2

    iput-wide v2, p0, Lcom/a/b/d/ai;->b:J

    return-wide v0
.end method

.method private static g()V
    .registers 2
    .annotation build Lcom/a/b/a/c;
        a = "java.io.ObjectStreamException"
    .end annotation

    .prologue
    .line 296
    new-instance v0, Ljava/io/InvalidObjectException;

    const-string v1, "Stream data required"

    invoke-direct {v0, v1}, Ljava/io/InvalidObjectException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public a(Ljava/lang/Object;)I
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 201
    iget-object v0, p0, Lcom/a/b/d/ai;->a:Ljava/util/Map;

    invoke-static {v0, p1}, Lcom/a/b/d/sz;->a(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/dv;

    .line 202
    if-nez v0, :cond_c

    const/4 v0, 0x0

    :goto_b
    return v0

    .line 2037
    :cond_c
    iget v0, v0, Lcom/a/b/d/dv;->a:I

    goto :goto_b
.end method

.method public a(Ljava/lang/Object;I)I
    .registers 13
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 215
    if-nez p2, :cond_9

    .line 216
    invoke-virtual {p0, p1}, Lcom/a/b/d/ai;->a(Ljava/lang/Object;)I

    move-result v2

    .line 233
    :goto_8
    return v2

    .line 218
    :cond_9
    if-lez p2, :cond_34

    move v0, v1

    :goto_c
    const-string v3, "occurrences cannot be negative: %s"

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v0, v3, v4}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 220
    iget-object v0, p0, Lcom/a/b/d/ai;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/dv;

    .line 222
    if-nez v0, :cond_36

    .line 224
    iget-object v0, p0, Lcom/a/b/d/ai;->a:Ljava/util/Map;

    new-instance v1, Lcom/a/b/d/dv;

    invoke-direct {v1, p2}, Lcom/a/b/d/dv;-><init>(I)V

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 232
    :goto_2d
    iget-wide v0, p0, Lcom/a/b/d/ai;->b:J

    int-to-long v4, p2

    add-long/2addr v0, v4

    iput-wide v0, p0, Lcom/a/b/d/ai;->b:J

    goto :goto_8

    :cond_34
    move v0, v2

    .line 218
    goto :goto_c

    .line 3037
    :cond_36
    iget v4, v0, Lcom/a/b/d/dv;->a:I

    .line 227
    int-to-long v6, v4

    int-to-long v8, p2

    add-long/2addr v6, v8

    .line 228
    const-wide/32 v8, 0x7fffffff

    cmp-long v3, v6, v8

    if-gtz v3, :cond_57

    move v3, v1

    :goto_43
    const-string v5, "too many occurrences: %s"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v1, v2

    invoke-static {v3, v5, v1}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 3041
    iget v1, v0, Lcom/a/b/d/dv;->a:I

    .line 3042
    add-int/2addr v1, p2

    iput v1, v0, Lcom/a/b/d/dv;->a:I

    move v2, v4

    goto :goto_2d

    :cond_57
    move v3, v2

    .line 228
    goto :goto_43
.end method

.method public a()Ljava/util/Set;
    .registers 2

    .prologue
    .line 82
    invoke-super {p0}, Lcom/a/b/d/as;->a()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/Object;I)I
    .registers 9
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 237
    if-nez p2, :cond_9

    .line 238
    invoke-virtual {p0, p1}, Lcom/a/b/d/ai;->a(Ljava/lang/Object;)I

    move-result v2

    .line 259
    :cond_8
    :goto_8
    return v2

    .line 240
    :cond_9
    if-lez p2, :cond_39

    move v0, v1

    :goto_c
    const-string v3, "occurrences cannot be negative: %s"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v2

    invoke-static {v0, v3, v1}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 242
    iget-object v0, p0, Lcom/a/b/d/ai;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/dv;

    .line 243
    if-eqz v0, :cond_8

    .line 4037
    iget v1, v0, Lcom/a/b/d/dv;->a:I

    .line 250
    if-gt v1, p2, :cond_2d

    .line 254
    iget-object v2, p0, Lcom/a/b/d/ai;->a:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move p2, v1

    .line 257
    :cond_2d
    neg-int v2, p2

    invoke-virtual {v0, v2}, Lcom/a/b/d/dv;->a(I)I

    .line 258
    iget-wide v2, p0, Lcom/a/b/d/ai;->b:J

    int-to-long v4, p2

    sub-long/2addr v2, v4

    iput-wide v2, p0, Lcom/a/b/d/ai;->b:J

    move v2, v1

    .line 259
    goto :goto_8

    :cond_39
    move v0, v2

    .line 240
    goto :goto_c
.end method

.method final b()Ljava/util/Iterator;
    .registers 3

    .prologue
    .line 87
    iget-object v0, p0, Lcom/a/b/d/ai;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 89
    new-instance v1, Lcom/a/b/d/aj;

    invoke-direct {v1, p0, v0}, Lcom/a/b/d/aj;-><init>(Lcom/a/b/d/ai;Ljava/util/Iterator;)V

    return-object v1
.end method

.method final c()I
    .registers 2

    .prologue
    .line 141
    iget-object v0, p0, Lcom/a/b/d/ai;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    return v0
.end method

.method public c(Ljava/lang/Object;I)I
    .registers 9
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 264
    const-string v0, "count"

    invoke-static {p2, v0}, Lcom/a/b/d/cl;->a(ILjava/lang/String;)I

    .line 268
    if-nez p2, :cond_1c

    .line 269
    iget-object v0, p0, Lcom/a/b/d/ai;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/dv;

    .line 270
    invoke-static {v0, p2}, Lcom/a/b/d/ai;->a(Lcom/a/b/d/dv;I)I

    move-result v0

    .line 280
    :goto_13
    iget-wide v2, p0, Lcom/a/b/d/ai;->b:J

    sub-int v1, p2, v0

    int-to-long v4, v1

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/a/b/d/ai;->b:J

    .line 281
    return v0

    .line 272
    :cond_1c
    iget-object v0, p0, Lcom/a/b/d/ai;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/dv;

    .line 273
    invoke-static {v0, p2}, Lcom/a/b/d/ai;->a(Lcom/a/b/d/dv;I)I

    move-result v1

    .line 275
    if-nez v0, :cond_34

    .line 276
    iget-object v0, p0, Lcom/a/b/d/ai;->a:Ljava/util/Map;

    new-instance v2, Lcom/a/b/d/dv;

    invoke-direct {v2, p2}, Lcom/a/b/d/dv;-><init>(I)V

    invoke-interface {v0, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_34
    move v0, v1

    goto :goto_13
.end method

.method public clear()V
    .registers 4

    .prologue
    .line 132
    iget-object v0, p0, Lcom/a/b/d/ai;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_a
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/dv;

    .line 1051
    const/4 v2, 0x0

    iput v2, v0, Lcom/a/b/d/dv;->a:I

    goto :goto_a

    .line 135
    :cond_1a
    iget-object v0, p0, Lcom/a/b/d/ai;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 136
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/a/b/d/ai;->b:J

    .line 137
    return-void
.end method

.method public iterator()Ljava/util/Iterator;
    .registers 2

    .prologue
    .line 151
    new-instance v0, Lcom/a/b/d/al;

    invoke-direct {v0, p0}, Lcom/a/b/d/al;-><init>(Lcom/a/b/d/ai;)V

    return-object v0
.end method

.method public size()I
    .registers 3

    .prologue
    .line 147
    iget-wide v0, p0, Lcom/a/b/d/ai;->b:J

    invoke-static {v0, v1}, Lcom/a/b/l/q;->b(J)I

    move-result v0

    return v0
.end method
