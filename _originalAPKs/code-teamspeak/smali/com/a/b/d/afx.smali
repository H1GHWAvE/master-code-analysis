.class final Lcom/a/b/d/afx;
.super Lcom/a/b/d/av;
.source "SourceFile"


# instance fields
.field private final a:Lcom/a/b/d/yl;

.field private final b:Lcom/a/b/d/yl;

.field private final c:Ljava/util/NavigableMap;

.field private final d:Ljava/util/NavigableMap;


# direct methods
.method private constructor <init>(Lcom/a/b/d/yl;Lcom/a/b/d/yl;Ljava/util/NavigableMap;)V
    .registers 5

    .prologue
    .line 638
    invoke-direct {p0}, Lcom/a/b/d/av;-><init>()V

    .line 639
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/yl;

    iput-object v0, p0, Lcom/a/b/d/afx;->a:Lcom/a/b/d/yl;

    .line 640
    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/yl;

    iput-object v0, p0, Lcom/a/b/d/afx;->b:Lcom/a/b/d/yl;

    .line 641
    invoke-static {p3}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/NavigableMap;

    iput-object v0, p0, Lcom/a/b/d/afx;->c:Ljava/util/NavigableMap;

    .line 642
    new-instance v0, Lcom/a/b/d/aft;

    invoke-direct {v0, p3}, Lcom/a/b/d/aft;-><init>(Ljava/util/NavigableMap;)V

    iput-object v0, p0, Lcom/a/b/d/afx;->d:Ljava/util/NavigableMap;

    .line 643
    return-void
.end method

.method synthetic constructor <init>(Lcom/a/b/d/yl;Lcom/a/b/d/yl;Ljava/util/NavigableMap;B)V
    .registers 5

    .prologue
    .line 620
    invoke-direct {p0, p1, p2, p3}, Lcom/a/b/d/afx;-><init>(Lcom/a/b/d/yl;Lcom/a/b/d/yl;Ljava/util/NavigableMap;)V

    return-void
.end method

.method static synthetic a(Lcom/a/b/d/afx;)Lcom/a/b/d/yl;
    .registers 2

    .prologue
    .line 620
    iget-object v0, p0, Lcom/a/b/d/afx;->b:Lcom/a/b/d/yl;

    return-object v0
.end method

.method private a(Ljava/lang/Object;)Lcom/a/b/d/yl;
    .registers 6
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 684
    instance-of v0, p1, Lcom/a/b/d/dw;

    if-eqz v0, :cond_64

    .line 687
    :try_start_5
    check-cast p1, Lcom/a/b/d/dw;

    .line 688
    iget-object v0, p0, Lcom/a/b/d/afx;->a:Lcom/a/b/d/yl;

    invoke-virtual {v0, p1}, Lcom/a/b/d/yl;->c(Ljava/lang/Comparable;)Z

    move-result v0

    if-eqz v0, :cond_23

    iget-object v0, p0, Lcom/a/b/d/afx;->b:Lcom/a/b/d/yl;

    iget-object v0, v0, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    invoke-virtual {p1, v0}, Lcom/a/b/d/dw;->a(Lcom/a/b/d/dw;)I

    move-result v0

    if-ltz v0, :cond_23

    iget-object v0, p0, Lcom/a/b/d/afx;->b:Lcom/a/b/d/yl;

    iget-object v0, v0, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    invoke-virtual {p1, v0}, Lcom/a/b/d/dw;->a(Lcom/a/b/d/dw;)I

    move-result v0

    if-ltz v0, :cond_25

    :cond_23
    move-object v0, v1

    .line 707
    :goto_24
    return-object v0

    .line 691
    :cond_25
    iget-object v0, p0, Lcom/a/b/d/afx;->b:Lcom/a/b/d/yl;

    iget-object v0, v0, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    invoke-virtual {p1, v0}, Lcom/a/b/d/dw;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_50

    .line 693
    iget-object v0, p0, Lcom/a/b/d/afx;->c:Ljava/util/NavigableMap;

    invoke-interface {v0, p1}, Ljava/util/NavigableMap;->floorEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/sz;->c(Ljava/util/Map$Entry;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/yl;

    .line 694
    if-eqz v0, :cond_64

    iget-object v2, v0, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    iget-object v3, p0, Lcom/a/b/d/afx;->b:Lcom/a/b/d/yl;

    iget-object v3, v3, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    invoke-virtual {v2, v3}, Lcom/a/b/d/dw;->a(Lcom/a/b/d/dw;)I

    move-result v2

    if-lez v2, :cond_64

    .line 695
    iget-object v2, p0, Lcom/a/b/d/afx;->b:Lcom/a/b/d/yl;

    invoke-virtual {v0, v2}, Lcom/a/b/d/yl;->c(Lcom/a/b/d/yl;)Lcom/a/b/d/yl;

    move-result-object v0

    goto :goto_24

    .line 698
    :cond_50
    iget-object v0, p0, Lcom/a/b/d/afx;->c:Ljava/util/NavigableMap;

    invoke-interface {v0, p1}, Ljava/util/NavigableMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/yl;

    .line 699
    if-eqz v0, :cond_64

    .line 700
    iget-object v2, p0, Lcom/a/b/d/afx;->b:Lcom/a/b/d/yl;

    invoke-virtual {v0, v2}, Lcom/a/b/d/yl;->c(Lcom/a/b/d/yl;)Lcom/a/b/d/yl;
    :try_end_5f
    .catch Ljava/lang/ClassCastException; {:try_start_5 .. :try_end_5f} :catch_61

    move-result-object v0

    goto :goto_24

    .line 704
    :catch_61
    move-exception v0

    move-object v0, v1

    goto :goto_24

    :cond_64
    move-object v0, v1

    .line 707
    goto :goto_24
.end method

.method private a(Lcom/a/b/d/dw;Z)Ljava/util/NavigableMap;
    .registers 4

    .prologue
    .line 663
    invoke-static {p2}, Lcom/a/b/d/ce;->a(Z)Lcom/a/b/d/ce;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/a/b/d/yl;->a(Ljava/lang/Comparable;Lcom/a/b/d/ce;)Lcom/a/b/d/yl;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/a/b/d/afx;->a(Lcom/a/b/d/yl;)Ljava/util/NavigableMap;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/a/b/d/dw;ZLcom/a/b/d/dw;Z)Ljava/util/NavigableMap;
    .registers 7

    .prologue
    .line 657
    invoke-static {p2}, Lcom/a/b/d/ce;->a(Z)Lcom/a/b/d/ce;

    move-result-object v0

    invoke-static {p4}, Lcom/a/b/d/ce;->a(Z)Lcom/a/b/d/ce;

    move-result-object v1

    invoke-static {p1, v0, p3, v1}, Lcom/a/b/d/yl;->a(Ljava/lang/Comparable;Lcom/a/b/d/ce;Ljava/lang/Comparable;Lcom/a/b/d/ce;)Lcom/a/b/d/yl;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/a/b/d/afx;->a(Lcom/a/b/d/yl;)Ljava/util/NavigableMap;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/a/b/d/yl;)Ljava/util/NavigableMap;
    .registers 6

    .prologue
    .line 646
    iget-object v0, p0, Lcom/a/b/d/afx;->a:Lcom/a/b/d/yl;

    invoke-virtual {p1, v0}, Lcom/a/b/d/yl;->b(Lcom/a/b/d/yl;)Z

    move-result v0

    if-nez v0, :cond_d

    .line 647
    invoke-static {}, Lcom/a/b/d/lw;->m()Lcom/a/b/d/lw;

    move-result-object v0

    .line 649
    :goto_c
    return-object v0

    :cond_d
    new-instance v0, Lcom/a/b/d/afx;

    iget-object v1, p0, Lcom/a/b/d/afx;->a:Lcom/a/b/d/yl;

    invoke-virtual {v1, p1}, Lcom/a/b/d/yl;->c(Lcom/a/b/d/yl;)Lcom/a/b/d/yl;

    move-result-object v1

    iget-object v2, p0, Lcom/a/b/d/afx;->b:Lcom/a/b/d/yl;

    iget-object v3, p0, Lcom/a/b/d/afx;->c:Ljava/util/NavigableMap;

    invoke-direct {v0, v1, v2, v3}, Lcom/a/b/d/afx;-><init>(Lcom/a/b/d/yl;Lcom/a/b/d/yl;Ljava/util/NavigableMap;)V

    goto :goto_c
.end method

.method static synthetic b(Lcom/a/b/d/afx;)Lcom/a/b/d/yl;
    .registers 2

    .prologue
    .line 620
    iget-object v0, p0, Lcom/a/b/d/afx;->a:Lcom/a/b/d/yl;

    return-object v0
.end method

.method private b(Lcom/a/b/d/dw;Z)Ljava/util/NavigableMap;
    .registers 4

    .prologue
    .line 668
    invoke-static {p2}, Lcom/a/b/d/ce;->a(Z)Lcom/a/b/d/ce;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/a/b/d/yl;->b(Ljava/lang/Comparable;Lcom/a/b/d/ce;)Lcom/a/b/d/yl;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/a/b/d/afx;->a(Lcom/a/b/d/yl;)Ljava/util/NavigableMap;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method final a()Ljava/util/Iterator;
    .registers 6

    .prologue
    .line 712
    iget-object v0, p0, Lcom/a/b/d/afx;->b:Lcom/a/b/d/yl;

    invoke-virtual {v0}, Lcom/a/b/d/yl;->f()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 713
    invoke-static {}, Lcom/a/b/d/nj;->a()Lcom/a/b/d/agi;

    move-result-object v0

    .line 729
    :goto_c
    return-object v0

    .line 716
    :cond_d
    iget-object v0, p0, Lcom/a/b/d/afx;->a:Lcom/a/b/d/yl;

    iget-object v0, v0, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    iget-object v1, p0, Lcom/a/b/d/afx;->b:Lcom/a/b/d/yl;

    iget-object v1, v1, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    invoke-virtual {v0, v1}, Lcom/a/b/d/dw;->a(Ljava/lang/Comparable;)Z

    move-result v0

    if-eqz v0, :cond_20

    .line 717
    invoke-static {}, Lcom/a/b/d/nj;->a()Lcom/a/b/d/agi;

    move-result-object v0

    goto :goto_c

    .line 718
    :cond_20
    iget-object v0, p0, Lcom/a/b/d/afx;->a:Lcom/a/b/d/yl;

    iget-object v0, v0, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    iget-object v1, p0, Lcom/a/b/d/afx;->b:Lcom/a/b/d/yl;

    iget-object v1, v1, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    invoke-virtual {v0, v1}, Lcom/a/b/d/dw;->a(Ljava/lang/Comparable;)Z

    move-result v0

    if-eqz v0, :cond_62

    .line 720
    iget-object v1, p0, Lcom/a/b/d/afx;->d:Ljava/util/NavigableMap;

    iget-object v0, p0, Lcom/a/b/d/afx;->b:Lcom/a/b/d/yl;

    iget-object v0, v0, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    .line 724
    :cond_34
    const/4 v2, 0x0

    move v4, v2

    move-object v2, v1

    move-object v1, v0

    move v0, v4

    :goto_39
    invoke-interface {v2, v1, v0}, Ljava/util/NavigableMap;->tailMap(Ljava/lang/Object;Z)Ljava/util/NavigableMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/NavigableMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 727
    invoke-static {}, Lcom/a/b/d/yd;->d()Lcom/a/b/d/yd;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/d/afx;->a:Lcom/a/b/d/yl;

    iget-object v1, v1, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    iget-object v3, p0, Lcom/a/b/d/afx;->b:Lcom/a/b/d/yl;

    iget-object v3, v3, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    invoke-static {v3}, Lcom/a/b/d/dw;->b(Ljava/lang/Comparable;)Lcom/a/b/d/dw;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Lcom/a/b/d/yd;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/dw;

    .line 729
    new-instance v1, Lcom/a/b/d/afy;

    invoke-direct {v1, p0, v2, v0}, Lcom/a/b/d/afy;-><init>(Lcom/a/b/d/afx;Ljava/util/Iterator;Lcom/a/b/d/dw;)V

    move-object v0, v1

    goto :goto_c

    .line 724
    :cond_62
    iget-object v1, p0, Lcom/a/b/d/afx;->c:Ljava/util/NavigableMap;

    iget-object v0, p0, Lcom/a/b/d/afx;->a:Lcom/a/b/d/yl;

    iget-object v0, v0, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    invoke-virtual {v0}, Lcom/a/b/d/dw;->c()Ljava/lang/Comparable;

    move-result-object v0

    iget-object v2, p0, Lcom/a/b/d/afx;->a:Lcom/a/b/d/yl;

    .line 1394
    iget-object v2, v2, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    invoke-virtual {v2}, Lcom/a/b/d/dw;->a()Lcom/a/b/d/ce;

    move-result-object v2

    .line 724
    sget-object v3, Lcom/a/b/d/ce;->b:Lcom/a/b/d/ce;

    if-ne v2, v3, :cond_34

    const/4 v2, 0x1

    move v4, v2

    move-object v2, v1

    move-object v1, v0

    move v0, v4

    goto :goto_39
.end method

.method final b()Ljava/util/Iterator;
    .registers 5

    .prologue
    .line 748
    iget-object v0, p0, Lcom/a/b/d/afx;->b:Lcom/a/b/d/yl;

    invoke-virtual {v0}, Lcom/a/b/d/yl;->f()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 749
    invoke-static {}, Lcom/a/b/d/nj;->a()Lcom/a/b/d/agi;

    move-result-object v0

    .line 757
    :goto_c
    return-object v0

    .line 751
    :cond_d
    invoke-static {}, Lcom/a/b/d/yd;->d()Lcom/a/b/d/yd;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/d/afx;->a:Lcom/a/b/d/yl;

    iget-object v1, v1, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    iget-object v2, p0, Lcom/a/b/d/afx;->b:Lcom/a/b/d/yl;

    iget-object v2, v2, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    invoke-static {v2}, Lcom/a/b/d/dw;->b(Ljava/lang/Comparable;)Lcom/a/b/d/dw;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/a/b/d/yd;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/dw;

    .line 753
    iget-object v1, p0, Lcom/a/b/d/afx;->c:Ljava/util/NavigableMap;

    invoke-virtual {v0}, Lcom/a/b/d/dw;->c()Ljava/lang/Comparable;

    move-result-object v2

    invoke-virtual {v0}, Lcom/a/b/d/dw;->b()Lcom/a/b/d/ce;

    move-result-object v0

    sget-object v3, Lcom/a/b/d/ce;->b:Lcom/a/b/d/ce;

    if-ne v0, v3, :cond_48

    const/4 v0, 0x1

    :goto_32
    invoke-interface {v1, v2, v0}, Ljava/util/NavigableMap;->headMap(Ljava/lang/Object;Z)Ljava/util/NavigableMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/NavigableMap;->descendingMap()Ljava/util/NavigableMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/NavigableMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 757
    new-instance v0, Lcom/a/b/d/afz;

    invoke-direct {v0, p0, v1}, Lcom/a/b/d/afz;-><init>(Lcom/a/b/d/afx;Ljava/util/Iterator;)V

    goto :goto_c

    .line 753
    :cond_48
    const/4 v0, 0x0

    goto :goto_32
.end method

.method public final comparator()Ljava/util/Comparator;
    .registers 2

    .prologue
    .line 673
    invoke-static {}, Lcom/a/b/d/yd;->d()Lcom/a/b/d/yd;

    move-result-object v0

    return-object v0
.end method

.method public final containsKey(Ljava/lang/Object;)Z
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 678
    invoke-direct {p0, p1}, Lcom/a/b/d/afx;->a(Ljava/lang/Object;)Lcom/a/b/d/yl;

    move-result-object v0

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public final synthetic get(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 620
    invoke-direct {p0, p1}, Lcom/a/b/d/afx;->a(Ljava/lang/Object;)Lcom/a/b/d/yl;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic headMap(Ljava/lang/Object;Z)Ljava/util/NavigableMap;
    .registers 4

    .prologue
    .line 620
    check-cast p1, Lcom/a/b/d/dw;

    .line 2663
    invoke-static {p2}, Lcom/a/b/d/ce;->a(Z)Lcom/a/b/d/ce;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/a/b/d/yl;->a(Ljava/lang/Comparable;Lcom/a/b/d/ce;)Lcom/a/b/d/yl;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/a/b/d/afx;->a(Lcom/a/b/d/yl;)Ljava/util/NavigableMap;

    move-result-object v0

    .line 620
    return-object v0
.end method

.method public final size()I
    .registers 2

    .prologue
    .line 779
    invoke-virtual {p0}, Lcom/a/b/d/afx;->a()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/nj;->b(Ljava/util/Iterator;)I

    move-result v0

    return v0
.end method

.method public final synthetic subMap(Ljava/lang/Object;ZLjava/lang/Object;Z)Ljava/util/NavigableMap;
    .registers 7

    .prologue
    .line 620
    check-cast p1, Lcom/a/b/d/dw;

    check-cast p3, Lcom/a/b/d/dw;

    .line 3657
    invoke-static {p2}, Lcom/a/b/d/ce;->a(Z)Lcom/a/b/d/ce;

    move-result-object v0

    invoke-static {p4}, Lcom/a/b/d/ce;->a(Z)Lcom/a/b/d/ce;

    move-result-object v1

    invoke-static {p1, v0, p3, v1}, Lcom/a/b/d/yl;->a(Ljava/lang/Comparable;Lcom/a/b/d/ce;Ljava/lang/Comparable;Lcom/a/b/d/ce;)Lcom/a/b/d/yl;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/a/b/d/afx;->a(Lcom/a/b/d/yl;)Ljava/util/NavigableMap;

    move-result-object v0

    .line 620
    return-object v0
.end method

.method public final synthetic tailMap(Ljava/lang/Object;Z)Ljava/util/NavigableMap;
    .registers 4

    .prologue
    .line 620
    check-cast p1, Lcom/a/b/d/dw;

    .line 1668
    invoke-static {p2}, Lcom/a/b/d/ce;->a(Z)Lcom/a/b/d/ce;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/a/b/d/yl;->b(Ljava/lang/Comparable;Lcom/a/b/d/ce;)Lcom/a/b/d/yl;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/a/b/d/afx;->a(Lcom/a/b/d/yl;)Ljava/util/NavigableMap;

    move-result-object v0

    .line 620
    return-object v0
.end method
