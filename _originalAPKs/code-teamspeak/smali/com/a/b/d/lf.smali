.class public final Lcom/a/b/d/lf;
.super Lcom/a/b/d/ay;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation build Lcom/a/b/a/a;
.end annotation


# static fields
.field private static final a:Lcom/a/b/d/lf;

.field private static final b:Lcom/a/b/d/lf;


# instance fields
.field private final transient c:Lcom/a/b/d/jl;

.field private transient d:Lcom/a/b/d/lf;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 46
    new-instance v0, Lcom/a/b/d/lf;

    invoke-static {}, Lcom/a/b/d/jl;->d()Lcom/a/b/d/jl;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/a/b/d/lf;-><init>(Lcom/a/b/d/jl;)V

    sput-object v0, Lcom/a/b/d/lf;->a:Lcom/a/b/d/lf;

    .line 49
    new-instance v0, Lcom/a/b/d/lf;

    invoke-static {}, Lcom/a/b/d/yl;->c()Lcom/a/b/d/yl;

    move-result-object v1

    invoke-static {v1}, Lcom/a/b/d/jl;->a(Ljava/lang/Object;)Lcom/a/b/d/jl;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/a/b/d/lf;-><init>(Lcom/a/b/d/jl;)V

    sput-object v0, Lcom/a/b/d/lf;->b:Lcom/a/b/d/lf;

    return-void
.end method

.method constructor <init>(Lcom/a/b/d/jl;)V
    .registers 2

    .prologue
    .line 103
    invoke-direct {p0}, Lcom/a/b/d/ay;-><init>()V

    .line 104
    iput-object p1, p0, Lcom/a/b/d/lf;->c:Lcom/a/b/d/jl;

    .line 105
    return-void
.end method

.method private constructor <init>(Lcom/a/b/d/jl;Lcom/a/b/d/lf;)V
    .registers 3

    .prologue
    .line 107
    invoke-direct {p0}, Lcom/a/b/d/ay;-><init>()V

    .line 108
    iput-object p1, p0, Lcom/a/b/d/lf;->c:Lcom/a/b/d/jl;

    .line 109
    iput-object p2, p0, Lcom/a/b/d/lf;->d:Lcom/a/b/d/lf;

    .line 110
    return-void
.end method

.method static synthetic a(Lcom/a/b/d/lf;)Lcom/a/b/d/jl;
    .registers 2

    .prologue
    .line 43
    iget-object v0, p0, Lcom/a/b/d/lf;->c:Lcom/a/b/d/jl;

    return-object v0
.end method

.method public static c()Lcom/a/b/d/lf;
    .registers 1

    .prologue
    .line 57
    sget-object v0, Lcom/a/b/d/lf;->a:Lcom/a/b/d/lf;

    return-object v0
.end method

.method static d()Lcom/a/b/d/lf;
    .registers 1

    .prologue
    .line 65
    sget-object v0, Lcom/a/b/d/lf;->b:Lcom/a/b/d/lf;

    return-object v0
.end method

.method public static d(Lcom/a/b/d/yr;)Lcom/a/b/d/lf;
    .registers 3

    .prologue
    .line 87
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 88
    invoke-interface {p0}, Lcom/a/b/d/yr;->a()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 2057
    sget-object v0, Lcom/a/b/d/lf;->a:Lcom/a/b/d/lf;

    .line 100
    :cond_b
    :goto_b
    return-object v0

    .line 90
    :cond_c
    invoke-static {}, Lcom/a/b/d/yl;->c()Lcom/a/b/d/yl;

    move-result-object v0

    invoke-interface {p0, v0}, Lcom/a/b/d/yr;->c(Lcom/a/b/d/yl;)Z

    move-result v0

    if-eqz v0, :cond_19

    .line 2065
    sget-object v0, Lcom/a/b/d/lf;->b:Lcom/a/b/d/lf;

    goto :goto_b

    .line 94
    :cond_19
    instance-of v0, p0, Lcom/a/b/d/lf;

    if-eqz v0, :cond_28

    move-object v0, p0

    .line 95
    check-cast v0, Lcom/a/b/d/lf;

    .line 2532
    iget-object v1, v0, Lcom/a/b/d/lf;->c:Lcom/a/b/d/jl;

    invoke-virtual {v1}, Lcom/a/b/d/jl;->h_()Z

    move-result v1

    .line 96
    if-eqz v1, :cond_b

    .line 100
    :cond_28
    new-instance v0, Lcom/a/b/d/lf;

    invoke-interface {p0}, Lcom/a/b/d/yr;->g()Ljava/util/Set;

    move-result-object v1

    invoke-static {v1}, Lcom/a/b/d/jl;->a(Ljava/util/Collection;)Lcom/a/b/d/jl;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/a/b/d/lf;-><init>(Lcom/a/b/d/jl;)V

    goto :goto_b
.end method

.method private static f(Lcom/a/b/d/yl;)Lcom/a/b/d/lf;
    .registers 3

    .prologue
    .line 73
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    invoke-virtual {p0}, Lcom/a/b/d/yl;->f()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 1057
    sget-object v0, Lcom/a/b/d/lf;->a:Lcom/a/b/d/lf;

    .line 79
    :goto_b
    return-object v0

    .line 76
    :cond_c
    invoke-static {}, Lcom/a/b/d/yl;->c()Lcom/a/b/d/yl;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/a/b/d/yl;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_19

    .line 1065
    sget-object v0, Lcom/a/b/d/lf;->b:Lcom/a/b/d/lf;

    goto :goto_b

    .line 79
    :cond_19
    new-instance v0, Lcom/a/b/d/lf;

    invoke-static {p0}, Lcom/a/b/d/jl;->a(Ljava/lang/Object;)Lcom/a/b/d/jl;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/a/b/d/lf;-><init>(Lcom/a/b/d/jl;)V

    goto :goto_b
.end method

.method private g(Lcom/a/b/d/yl;)Lcom/a/b/d/jl;
    .registers 8

    .prologue
    .line 261
    iget-object v0, p0, Lcom/a/b/d/lf;->c:Lcom/a/b/d/jl;

    invoke-virtual {v0}, Lcom/a/b/d/jl;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_e

    invoke-virtual {p1}, Lcom/a/b/d/yl;->f()Z

    move-result v0

    if-eqz v0, :cond_13

    .line 262
    :cond_e
    invoke-static {}, Lcom/a/b/d/jl;->d()Lcom/a/b/d/jl;

    move-result-object v0

    .line 288
    :goto_12
    return-object v0

    .line 263
    :cond_13
    invoke-virtual {p0}, Lcom/a/b/d/lf;->e()Lcom/a/b/d/yl;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/a/b/d/yl;->a(Lcom/a/b/d/yl;)Z

    move-result v0

    if-eqz v0, :cond_20

    .line 264
    iget-object v0, p0, Lcom/a/b/d/lf;->c:Lcom/a/b/d/jl;

    goto :goto_12

    .line 268
    :cond_20
    invoke-virtual {p1}, Lcom/a/b/d/yl;->d()Z

    move-result v0

    if-eqz v0, :cond_55

    .line 269
    iget-object v0, p0, Lcom/a/b/d/lf;->c:Lcom/a/b/d/jl;

    invoke-static {}, Lcom/a/b/d/yl;->b()Lcom/a/b/b/bj;

    move-result-object v1

    iget-object v2, p1, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    sget-object v3, Lcom/a/b/d/abg;->d:Lcom/a/b/d/abg;

    sget-object v4, Lcom/a/b/d/abc;->b:Lcom/a/b/d/abc;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/a/b/d/aba;->a(Ljava/util/List;Lcom/a/b/b/bj;Ljava/lang/Comparable;Lcom/a/b/d/abg;Lcom/a/b/d/abc;)I

    move-result v0

    .line 277
    :goto_36
    invoke-virtual {p1}, Lcom/a/b/d/yl;->e()Z

    move-result v1

    if-eqz v1, :cond_57

    .line 278
    iget-object v1, p0, Lcom/a/b/d/lf;->c:Lcom/a/b/d/jl;

    invoke-static {}, Lcom/a/b/d/yl;->a()Lcom/a/b/b/bj;

    move-result-object v2

    iget-object v3, p1, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    sget-object v4, Lcom/a/b/d/abg;->c:Lcom/a/b/d/abg;

    sget-object v5, Lcom/a/b/d/abc;->b:Lcom/a/b/d/abc;

    invoke-static {v1, v2, v3, v4, v5}, Lcom/a/b/d/aba;->a(Ljava/util/List;Lcom/a/b/b/bj;Ljava/lang/Comparable;Lcom/a/b/d/abg;Lcom/a/b/d/abc;)I

    move-result v1

    .line 284
    :goto_4c
    sub-int v2, v1, v0

    .line 285
    if-nez v2, :cond_5e

    .line 286
    invoke-static {}, Lcom/a/b/d/jl;->d()Lcom/a/b/d/jl;

    move-result-object v0

    goto :goto_12

    .line 273
    :cond_55
    const/4 v0, 0x0

    goto :goto_36

    .line 282
    :cond_57
    iget-object v1, p0, Lcom/a/b/d/lf;->c:Lcom/a/b/d/jl;

    invoke-virtual {v1}, Lcom/a/b/d/jl;->size()I

    move-result v1

    goto :goto_4c

    .line 288
    :cond_5e
    new-instance v1, Lcom/a/b/d/lg;

    invoke-direct {v1, p0, v2, v0, p1}, Lcom/a/b/d/lg;-><init>(Lcom/a/b/d/lf;IILcom/a/b/d/yl;)V

    move-object v0, v1

    goto :goto_12
.end method

.method private h()Lcom/a/b/d/lo;
    .registers 4

    .prologue
    .line 177
    iget-object v0, p0, Lcom/a/b/d/lf;->c:Lcom/a/b/d/jl;

    invoke-virtual {v0}, Lcom/a/b/d/jl;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 178
    invoke-static {}, Lcom/a/b/d/lo;->h()Lcom/a/b/d/lo;

    move-result-object v0

    .line 180
    :goto_c
    return-object v0

    :cond_d
    new-instance v0, Lcom/a/b/d/zq;

    iget-object v1, p0, Lcom/a/b/d/lf;->c:Lcom/a/b/d/jl;

    sget-object v2, Lcom/a/b/d/yl;->a:Lcom/a/b/d/yd;

    invoke-direct {v0, v1, v2}, Lcom/a/b/d/zq;-><init>(Lcom/a/b/d/jl;Ljava/util/Comparator;)V

    goto :goto_c
.end method

.method private i()Lcom/a/b/d/lf;
    .registers 3

    .prologue
    .line 242
    iget-object v0, p0, Lcom/a/b/d/lf;->d:Lcom/a/b/d/lf;

    .line 243
    if-eqz v0, :cond_5

    .line 253
    :goto_4
    return-object v0

    .line 245
    :cond_5
    iget-object v0, p0, Lcom/a/b/d/lf;->c:Lcom/a/b/d/jl;

    invoke-virtual {v0}, Lcom/a/b/d/jl;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_12

    .line 3065
    sget-object v0, Lcom/a/b/d/lf;->b:Lcom/a/b/d/lf;

    .line 246
    iput-object v0, p0, Lcom/a/b/d/lf;->d:Lcom/a/b/d/lf;

    goto :goto_4

    .line 247
    :cond_12
    iget-object v0, p0, Lcom/a/b/d/lf;->c:Lcom/a/b/d/jl;

    invoke-virtual {v0}, Lcom/a/b/d/jl;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_33

    iget-object v0, p0, Lcom/a/b/d/lf;->c:Lcom/a/b/d/jl;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/a/b/d/jl;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/yl;

    invoke-static {}, Lcom/a/b/d/yl;->c()Lcom/a/b/d/yl;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/a/b/d/yl;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_33

    .line 4057
    sget-object v0, Lcom/a/b/d/lf;->a:Lcom/a/b/d/lf;

    .line 248
    iput-object v0, p0, Lcom/a/b/d/lf;->d:Lcom/a/b/d/lf;

    goto :goto_4

    .line 250
    :cond_33
    new-instance v1, Lcom/a/b/d/lm;

    invoke-direct {v1, p0}, Lcom/a/b/d/lm;-><init>(Lcom/a/b/d/lf;)V

    .line 251
    new-instance v0, Lcom/a/b/d/lf;

    invoke-direct {v0, v1, p0}, Lcom/a/b/d/lf;-><init>(Lcom/a/b/d/jl;Lcom/a/b/d/lf;)V

    iput-object v0, p0, Lcom/a/b/d/lf;->d:Lcom/a/b/d/lf;

    goto :goto_4
.end method

.method private j()Z
    .registers 2

    .prologue
    .line 532
    iget-object v0, p0, Lcom/a/b/d/lf;->c:Lcom/a/b/d/jl;

    invoke-virtual {v0}, Lcom/a/b/d/jl;->h_()Z

    move-result v0

    return v0
.end method

.method private static k()Lcom/a/b/d/ll;
    .registers 1

    .prologue
    .line 539
    new-instance v0, Lcom/a/b/d/ll;

    invoke-direct {v0}, Lcom/a/b/d/ll;-><init>()V

    return-object v0
.end method

.method private l()Ljava/lang/Object;
    .registers 3

    .prologue
    .line 612
    new-instance v0, Lcom/a/b/d/ln;

    iget-object v1, p0, Lcom/a/b/d/lf;->c:Lcom/a/b/d/jl;

    invoke-direct {v0, v1}, Lcom/a/b/d/ln;-><init>(Lcom/a/b/d/jl;)V

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/a/b/d/ep;)Lcom/a/b/d/me;
    .registers 6

    .prologue
    .line 348
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 5152
    iget-object v0, p0, Lcom/a/b/d/lf;->c:Lcom/a/b/d/jl;

    invoke-virtual {v0}, Lcom/a/b/d/jl;->isEmpty()Z

    move-result v0

    .line 349
    if-eqz v0, :cond_10

    .line 350
    invoke-static {}, Lcom/a/b/d/me;->j()Lcom/a/b/d/me;

    move-result-object v0

    .line 367
    :goto_f
    return-object v0

    .line 352
    :cond_10
    invoke-virtual {p0}, Lcom/a/b/d/lf;->e()Lcom/a/b/d/yl;

    move-result-object v0

    .line 5623
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 5624
    iget-object v1, v0, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    invoke-virtual {v1, p1}, Lcom/a/b/d/dw;->c(Lcom/a/b/d/ep;)Lcom/a/b/d/dw;

    move-result-object v1

    .line 5625
    iget-object v2, v0, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    invoke-virtual {v2, p1}, Lcom/a/b/d/dw;->c(Lcom/a/b/d/ep;)Lcom/a/b/d/dw;

    move-result-object v2

    .line 5626
    iget-object v3, v0, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    if-ne v1, v3, :cond_39

    iget-object v3, v0, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    if-ne v2, v3, :cond_39

    .line 353
    :goto_2b
    invoke-virtual {v0}, Lcom/a/b/d/yl;->d()Z

    move-result v1

    if-nez v1, :cond_3e

    .line 356
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Neither the DiscreteDomain nor this range set are bounded below"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 5626
    :cond_39
    invoke-static {v1, v2}, Lcom/a/b/d/yl;->a(Lcom/a/b/d/dw;Lcom/a/b/d/dw;)Lcom/a/b/d/yl;

    move-result-object v0

    goto :goto_2b

    .line 358
    :cond_3e
    invoke-virtual {v0}, Lcom/a/b/d/yl;->e()Z

    move-result v0

    if-nez v0, :cond_47

    .line 360
    :try_start_44
    invoke-virtual {p1}, Lcom/a/b/d/ep;->b()Ljava/lang/Comparable;
    :try_end_47
    .catch Ljava/util/NoSuchElementException; {:try_start_44 .. :try_end_47} :catch_4d

    .line 367
    :cond_47
    new-instance v0, Lcom/a/b/d/lh;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/lh;-><init>(Lcom/a/b/d/lf;Lcom/a/b/d/ep;)V

    goto :goto_f

    .line 362
    :catch_4d
    move-exception v0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Neither the DiscreteDomain nor this range set are bounded above"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Lcom/a/b/d/yl;)V
    .registers 3

    .prologue
    .line 157
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final a()Z
    .registers 2

    .prologue
    .line 152
    iget-object v0, p0, Lcom/a/b/d/lf;->c:Lcom/a/b/d/jl;

    invoke-virtual {v0}, Lcom/a/b/d/jl;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic a(Lcom/a/b/d/yr;)Z
    .registers 3

    .prologue
    .line 42
    invoke-super {p0, p1}, Lcom/a/b/d/ay;->a(Lcom/a/b/d/yr;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic a(Ljava/lang/Comparable;)Z
    .registers 3

    .prologue
    .line 42
    invoke-super {p0, p1}, Lcom/a/b/d/ay;->a(Ljava/lang/Comparable;)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/Comparable;)Lcom/a/b/d/yl;
    .registers 9

    .prologue
    const/4 v6, 0x0

    .line 127
    iget-object v0, p0, Lcom/a/b/d/lf;->c:Lcom/a/b/d/jl;

    invoke-static {}, Lcom/a/b/d/yl;->a()Lcom/a/b/b/bj;

    move-result-object v1

    invoke-static {p1}, Lcom/a/b/d/dw;->b(Ljava/lang/Comparable;)Lcom/a/b/d/dw;

    move-result-object v2

    invoke-static {}, Lcom/a/b/d/yd;->d()Lcom/a/b/d/yd;

    move-result-object v3

    sget-object v4, Lcom/a/b/d/abg;->a:Lcom/a/b/d/abg;

    sget-object v5, Lcom/a/b/d/abc;->a:Lcom/a/b/d/abc;

    invoke-static/range {v0 .. v5}, Lcom/a/b/d/aba;->a(Ljava/util/List;Lcom/a/b/b/bj;Ljava/lang/Object;Ljava/util/Comparator;Lcom/a/b/d/abg;Lcom/a/b/d/abc;)I

    move-result v0

    .line 133
    const/4 v1, -0x1

    if-eq v0, v1, :cond_2b

    .line 134
    iget-object v1, p0, Lcom/a/b/d/lf;->c:Lcom/a/b/d/jl;

    invoke-virtual {v1, v0}, Lcom/a/b/d/jl;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/yl;

    .line 135
    invoke-virtual {v0, p1}, Lcom/a/b/d/yl;->c(Ljava/lang/Comparable;)Z

    move-result v1

    if-eqz v1, :cond_29

    .line 137
    :goto_28
    return-object v0

    :cond_29
    move-object v0, v6

    .line 135
    goto :goto_28

    :cond_2b
    move-object v0, v6

    .line 137
    goto :goto_28
.end method

.method public final bridge synthetic b()V
    .registers 1

    .prologue
    .line 42
    invoke-super {p0}, Lcom/a/b/d/ay;->b()V

    return-void
.end method

.method public final b(Lcom/a/b/d/yl;)V
    .registers 3

    .prologue
    .line 167
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(Lcom/a/b/d/yr;)V
    .registers 3

    .prologue
    .line 162
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final c(Lcom/a/b/d/yr;)V
    .registers 3

    .prologue
    .line 172
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final c(Lcom/a/b/d/yl;)Z
    .registers 8

    .prologue
    .line 116
    iget-object v0, p0, Lcom/a/b/d/lf;->c:Lcom/a/b/d/jl;

    invoke-static {}, Lcom/a/b/d/yl;->a()Lcom/a/b/b/bj;

    move-result-object v1

    iget-object v2, p1, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    invoke-static {}, Lcom/a/b/d/yd;->d()Lcom/a/b/d/yd;

    move-result-object v3

    sget-object v4, Lcom/a/b/d/abg;->a:Lcom/a/b/d/abg;

    sget-object v5, Lcom/a/b/d/abc;->a:Lcom/a/b/d/abc;

    invoke-static/range {v0 .. v5}, Lcom/a/b/d/aba;->a(Ljava/util/List;Lcom/a/b/b/bj;Ljava/lang/Object;Ljava/util/Comparator;Lcom/a/b/d/abg;Lcom/a/b/d/abc;)I

    move-result v0

    .line 122
    const/4 v1, -0x1

    if-eq v0, v1, :cond_27

    iget-object v1, p0, Lcom/a/b/d/lf;->c:Lcom/a/b/d/jl;

    invoke-virtual {v1, v0}, Lcom/a/b/d/jl;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/yl;

    invoke-virtual {v0, p1}, Lcom/a/b/d/yl;->a(Lcom/a/b/d/yl;)Z

    move-result v0

    if-eqz v0, :cond_27

    const/4 v0, 0x1

    :goto_26
    return v0

    :cond_27
    const/4 v0, 0x0

    goto :goto_26
.end method

.method public final d(Lcom/a/b/d/yl;)Lcom/a/b/d/lf;
    .registers 9

    .prologue
    .line 317
    .line 4152
    iget-object v0, p0, Lcom/a/b/d/lf;->c:Lcom/a/b/d/jl;

    invoke-virtual {v0}, Lcom/a/b/d/jl;->isEmpty()Z

    move-result v0

    .line 317
    if-nez v0, :cond_84

    .line 318
    invoke-virtual {p0}, Lcom/a/b/d/lf;->e()Lcom/a/b/d/yl;

    move-result-object v0

    .line 319
    invoke-virtual {p1, v0}, Lcom/a/b/d/yl;->a(Lcom/a/b/d/yl;)Z

    move-result v1

    if-eqz v1, :cond_13

    .line 325
    :goto_12
    return-object p0

    .line 321
    :cond_13
    invoke-virtual {p1, v0}, Lcom/a/b/d/yl;->b(Lcom/a/b/d/yl;)Z

    move-result v0

    if-eqz v0, :cond_84

    .line 322
    new-instance v2, Lcom/a/b/d/lf;

    .line 4261
    iget-object v0, p0, Lcom/a/b/d/lf;->c:Lcom/a/b/d/jl;

    invoke-virtual {v0}, Lcom/a/b/d/jl;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_29

    invoke-virtual {p1}, Lcom/a/b/d/yl;->f()Z

    move-result v0

    if-eqz v0, :cond_32

    .line 4262
    :cond_29
    invoke-static {}, Lcom/a/b/d/jl;->d()Lcom/a/b/d/jl;

    move-result-object v0

    .line 322
    :goto_2d
    invoke-direct {v2, v0}, Lcom/a/b/d/lf;-><init>(Lcom/a/b/d/jl;)V

    move-object p0, v2

    goto :goto_12

    .line 4263
    :cond_32
    invoke-virtual {p0}, Lcom/a/b/d/lf;->e()Lcom/a/b/d/yl;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/a/b/d/yl;->a(Lcom/a/b/d/yl;)Z

    move-result v0

    if-eqz v0, :cond_3f

    .line 4264
    iget-object v0, p0, Lcom/a/b/d/lf;->c:Lcom/a/b/d/jl;

    goto :goto_2d

    .line 4268
    :cond_3f
    invoke-virtual {p1}, Lcom/a/b/d/yl;->d()Z

    move-result v0

    if-eqz v0, :cond_74

    .line 4269
    iget-object v0, p0, Lcom/a/b/d/lf;->c:Lcom/a/b/d/jl;

    invoke-static {}, Lcom/a/b/d/yl;->b()Lcom/a/b/b/bj;

    move-result-object v1

    iget-object v3, p1, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    sget-object v4, Lcom/a/b/d/abg;->d:Lcom/a/b/d/abg;

    sget-object v5, Lcom/a/b/d/abc;->b:Lcom/a/b/d/abc;

    invoke-static {v0, v1, v3, v4, v5}, Lcom/a/b/d/aba;->a(Ljava/util/List;Lcom/a/b/b/bj;Ljava/lang/Comparable;Lcom/a/b/d/abg;Lcom/a/b/d/abc;)I

    move-result v0

    .line 4277
    :goto_55
    invoke-virtual {p1}, Lcom/a/b/d/yl;->e()Z

    move-result v1

    if-eqz v1, :cond_76

    .line 4278
    iget-object v1, p0, Lcom/a/b/d/lf;->c:Lcom/a/b/d/jl;

    invoke-static {}, Lcom/a/b/d/yl;->a()Lcom/a/b/b/bj;

    move-result-object v3

    iget-object v4, p1, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    sget-object v5, Lcom/a/b/d/abg;->c:Lcom/a/b/d/abg;

    sget-object v6, Lcom/a/b/d/abc;->b:Lcom/a/b/d/abc;

    invoke-static {v1, v3, v4, v5, v6}, Lcom/a/b/d/aba;->a(Ljava/util/List;Lcom/a/b/b/bj;Ljava/lang/Comparable;Lcom/a/b/d/abg;Lcom/a/b/d/abc;)I

    move-result v1

    .line 4284
    :goto_6b
    sub-int v3, v1, v0

    .line 4285
    if-nez v3, :cond_7d

    .line 4286
    invoke-static {}, Lcom/a/b/d/jl;->d()Lcom/a/b/d/jl;

    move-result-object v0

    goto :goto_2d

    .line 4273
    :cond_74
    const/4 v0, 0x0

    goto :goto_55

    .line 4282
    :cond_76
    iget-object v1, p0, Lcom/a/b/d/lf;->c:Lcom/a/b/d/jl;

    invoke-virtual {v1}, Lcom/a/b/d/jl;->size()I

    move-result v1

    goto :goto_6b

    .line 4288
    :cond_7d
    new-instance v1, Lcom/a/b/d/lg;

    invoke-direct {v1, p0, v3, v0, p1}, Lcom/a/b/d/lg;-><init>(Lcom/a/b/d/lf;IILcom/a/b/d/yl;)V

    move-object v0, v1

    goto :goto_2d

    .line 5057
    :cond_84
    sget-object p0, Lcom/a/b/d/lf;->a:Lcom/a/b/d/lf;

    goto :goto_12
.end method

.method public final e()Lcom/a/b/d/yl;
    .registers 4

    .prologue
    .line 142
    iget-object v0, p0, Lcom/a/b/d/lf;->c:Lcom/a/b/d/jl;

    invoke-virtual {v0}, Lcom/a/b/d/jl;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 143
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 145
    :cond_e
    iget-object v0, p0, Lcom/a/b/d/lf;->c:Lcom/a/b/d/jl;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/a/b/d/jl;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/yl;

    iget-object v1, v0, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    iget-object v0, p0, Lcom/a/b/d/lf;->c:Lcom/a/b/d/jl;

    iget-object v2, p0, Lcom/a/b/d/lf;->c:Lcom/a/b/d/jl;

    invoke-virtual {v2}, Lcom/a/b/d/jl;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v2}, Lcom/a/b/d/jl;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/yl;

    iget-object v0, v0, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    invoke-static {v1, v0}, Lcom/a/b/d/yl;->a(Lcom/a/b/d/dw;Lcom/a/b/d/dw;)Lcom/a/b/d/yl;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic e(Lcom/a/b/d/yl;)Lcom/a/b/d/yr;
    .registers 3

    .prologue
    .line 42
    invoke-virtual {p0, p1}, Lcom/a/b/d/lf;->d(Lcom/a/b/d/yl;)Lcom/a/b/d/lf;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic equals(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 42
    invoke-super {p0, p1}, Lcom/a/b/d/ay;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final synthetic f()Lcom/a/b/d/yr;
    .registers 3

    .prologue
    .line 42
    .line 6242
    iget-object v0, p0, Lcom/a/b/d/lf;->d:Lcom/a/b/d/lf;

    .line 6243
    if-eqz v0, :cond_5

    .line 6248
    :goto_4
    return-object v0

    .line 6245
    :cond_5
    iget-object v0, p0, Lcom/a/b/d/lf;->c:Lcom/a/b/d/jl;

    invoke-virtual {v0}, Lcom/a/b/d/jl;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_12

    .line 7065
    sget-object v0, Lcom/a/b/d/lf;->b:Lcom/a/b/d/lf;

    .line 6246
    iput-object v0, p0, Lcom/a/b/d/lf;->d:Lcom/a/b/d/lf;

    goto :goto_4

    .line 6247
    :cond_12
    iget-object v0, p0, Lcom/a/b/d/lf;->c:Lcom/a/b/d/jl;

    invoke-virtual {v0}, Lcom/a/b/d/jl;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_33

    iget-object v0, p0, Lcom/a/b/d/lf;->c:Lcom/a/b/d/jl;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/a/b/d/jl;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/yl;

    invoke-static {}, Lcom/a/b/d/yl;->c()Lcom/a/b/d/yl;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/a/b/d/yl;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_33

    .line 8057
    sget-object v0, Lcom/a/b/d/lf;->a:Lcom/a/b/d/lf;

    .line 6248
    iput-object v0, p0, Lcom/a/b/d/lf;->d:Lcom/a/b/d/lf;

    goto :goto_4

    .line 6250
    :cond_33
    new-instance v1, Lcom/a/b/d/lm;

    invoke-direct {v1, p0}, Lcom/a/b/d/lm;-><init>(Lcom/a/b/d/lf;)V

    .line 6251
    new-instance v0, Lcom/a/b/d/lf;

    invoke-direct {v0, v1, p0}, Lcom/a/b/d/lf;-><init>(Lcom/a/b/d/jl;Lcom/a/b/d/lf;)V

    iput-object v0, p0, Lcom/a/b/d/lf;->d:Lcom/a/b/d/lf;

    goto :goto_4
.end method

.method public final synthetic g()Ljava/util/Set;
    .registers 4

    .prologue
    .line 42
    .line 8177
    iget-object v0, p0, Lcom/a/b/d/lf;->c:Lcom/a/b/d/jl;

    invoke-virtual {v0}, Lcom/a/b/d/jl;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 8178
    invoke-static {}, Lcom/a/b/d/lo;->h()Lcom/a/b/d/lo;

    move-result-object v0

    :goto_c
    return-object v0

    .line 8180
    :cond_d
    new-instance v0, Lcom/a/b/d/zq;

    iget-object v1, p0, Lcom/a/b/d/lf;->c:Lcom/a/b/d/jl;

    sget-object v2, Lcom/a/b/d/yl;->a:Lcom/a/b/d/yd;

    invoke-direct {v0, v1, v2}, Lcom/a/b/d/zq;-><init>(Lcom/a/b/d/jl;Ljava/util/Comparator;)V

    goto :goto_c
.end method
