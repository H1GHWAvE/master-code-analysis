.class final Lcom/a/b/d/uz;
.super Lcom/a/b/d/hk;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/util/NavigableMap;


# annotations
.annotation build Lcom/a/b/a/c;
    a = "NavigableMap"
.end annotation


# instance fields
.field private final a:Ljava/util/NavigableMap;

.field private transient b:Lcom/a/b/d/uz;


# direct methods
.method constructor <init>(Ljava/util/NavigableMap;)V
    .registers 2

    .prologue
    .line 3109
    invoke-direct {p0}, Lcom/a/b/d/hk;-><init>()V

    .line 3110
    iput-object p1, p0, Lcom/a/b/d/uz;->a:Ljava/util/NavigableMap;

    .line 3111
    return-void
.end method

.method private constructor <init>(Ljava/util/NavigableMap;Lcom/a/b/d/uz;)V
    .registers 3

    .prologue
    .line 3114
    invoke-direct {p0}, Lcom/a/b/d/hk;-><init>()V

    .line 3115
    iput-object p1, p0, Lcom/a/b/d/uz;->a:Ljava/util/NavigableMap;

    .line 3116
    iput-object p2, p0, Lcom/a/b/d/uz;->b:Lcom/a/b/d/uz;

    .line 3117
    return-void
.end method


# virtual methods
.method protected final synthetic a()Ljava/util/Map;
    .registers 2

    .prologue
    .line 3104
    .line 4121
    iget-object v0, p0, Lcom/a/b/d/uz;->a:Ljava/util/NavigableMap;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSortedMap(Ljava/util/SortedMap;)Ljava/util/SortedMap;

    move-result-object v0

    .line 3104
    return-object v0
.end method

.method protected final c()Ljava/util/SortedMap;
    .registers 2

    .prologue
    .line 3121
    iget-object v0, p0, Lcom/a/b/d/uz;->a:Ljava/util/NavigableMap;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSortedMap(Ljava/util/SortedMap;)Ljava/util/SortedMap;

    move-result-object v0

    return-object v0
.end method

.method public final ceilingEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;
    .registers 3

    .prologue
    .line 3146
    iget-object v0, p0, Lcom/a/b/d/uz;->a:Ljava/util/NavigableMap;

    invoke-interface {v0, p1}, Ljava/util/NavigableMap;->ceilingEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/sz;->d(Ljava/util/Map$Entry;)Ljava/util/Map$Entry;

    move-result-object v0

    return-object v0
.end method

.method public final ceilingKey(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 3151
    iget-object v0, p0, Lcom/a/b/d/uz;->a:Ljava/util/NavigableMap;

    invoke-interface {v0, p1}, Ljava/util/NavigableMap;->ceilingKey(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final descendingKeySet()Ljava/util/NavigableSet;
    .registers 2

    .prologue
    .line 3206
    iget-object v0, p0, Lcom/a/b/d/uz;->a:Ljava/util/NavigableMap;

    invoke-interface {v0}, Ljava/util/NavigableMap;->descendingKeySet()Ljava/util/NavigableSet;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/aad;->a(Ljava/util/NavigableSet;)Ljava/util/NavigableSet;

    move-result-object v0

    return-object v0
.end method

.method public final descendingMap()Ljava/util/NavigableMap;
    .registers 3

    .prologue
    .line 3188
    iget-object v0, p0, Lcom/a/b/d/uz;->b:Lcom/a/b/d/uz;

    .line 3189
    if-nez v0, :cond_11

    new-instance v0, Lcom/a/b/d/uz;

    iget-object v1, p0, Lcom/a/b/d/uz;->a:Ljava/util/NavigableMap;

    invoke-interface {v1}, Ljava/util/NavigableMap;->descendingMap()Ljava/util/NavigableMap;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lcom/a/b/d/uz;-><init>(Ljava/util/NavigableMap;Lcom/a/b/d/uz;)V

    iput-object v0, p0, Lcom/a/b/d/uz;->b:Lcom/a/b/d/uz;

    :cond_11
    return-object v0
.end method

.method public final firstEntry()Ljava/util/Map$Entry;
    .registers 2

    .prologue
    .line 3166
    iget-object v0, p0, Lcom/a/b/d/uz;->a:Ljava/util/NavigableMap;

    invoke-interface {v0}, Ljava/util/NavigableMap;->firstEntry()Ljava/util/Map$Entry;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/sz;->d(Ljava/util/Map$Entry;)Ljava/util/Map$Entry;

    move-result-object v0

    return-object v0
.end method

.method public final floorEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;
    .registers 3

    .prologue
    .line 3136
    iget-object v0, p0, Lcom/a/b/d/uz;->a:Ljava/util/NavigableMap;

    invoke-interface {v0, p1}, Ljava/util/NavigableMap;->floorEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/sz;->d(Ljava/util/Map$Entry;)Ljava/util/Map$Entry;

    move-result-object v0

    return-object v0
.end method

.method public final floorKey(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 3141
    iget-object v0, p0, Lcom/a/b/d/uz;->a:Ljava/util/NavigableMap;

    invoke-interface {v0, p1}, Ljava/util/NavigableMap;->floorKey(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final headMap(Ljava/lang/Object;Z)Ljava/util/NavigableMap;
    .registers 4

    .prologue
    .line 3237
    iget-object v0, p0, Lcom/a/b/d/uz;->a:Ljava/util/NavigableMap;

    invoke-interface {v0, p1, p2}, Ljava/util/NavigableMap;->headMap(Ljava/lang/Object;Z)Ljava/util/NavigableMap;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/sz;->a(Ljava/util/NavigableMap;)Ljava/util/NavigableMap;

    move-result-object v0

    return-object v0
.end method

.method public final headMap(Ljava/lang/Object;)Ljava/util/SortedMap;
    .registers 3

    .prologue
    .line 3216
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/a/b/d/uz;->headMap(Ljava/lang/Object;Z)Ljava/util/NavigableMap;

    move-result-object v0

    return-object v0
.end method

.method public final higherEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;
    .registers 3

    .prologue
    .line 3156
    iget-object v0, p0, Lcom/a/b/d/uz;->a:Ljava/util/NavigableMap;

    invoke-interface {v0, p1}, Ljava/util/NavigableMap;->higherEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/sz;->d(Ljava/util/Map$Entry;)Ljava/util/Map$Entry;

    move-result-object v0

    return-object v0
.end method

.method public final higherKey(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 3161
    iget-object v0, p0, Lcom/a/b/d/uz;->a:Ljava/util/NavigableMap;

    invoke-interface {v0, p1}, Ljava/util/NavigableMap;->higherKey(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic k_()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 3104
    .line 5121
    iget-object v0, p0, Lcom/a/b/d/uz;->a:Ljava/util/NavigableMap;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSortedMap(Ljava/util/SortedMap;)Ljava/util/SortedMap;

    move-result-object v0

    .line 3104
    return-object v0
.end method

.method public final keySet()Ljava/util/Set;
    .registers 2

    .prologue
    .line 3196
    invoke-virtual {p0}, Lcom/a/b/d/uz;->navigableKeySet()Ljava/util/NavigableSet;

    move-result-object v0

    return-object v0
.end method

.method public final lastEntry()Ljava/util/Map$Entry;
    .registers 2

    .prologue
    .line 3171
    iget-object v0, p0, Lcom/a/b/d/uz;->a:Ljava/util/NavigableMap;

    invoke-interface {v0}, Ljava/util/NavigableMap;->lastEntry()Ljava/util/Map$Entry;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/sz;->d(Ljava/util/Map$Entry;)Ljava/util/Map$Entry;

    move-result-object v0

    return-object v0
.end method

.method public final lowerEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;
    .registers 3

    .prologue
    .line 3126
    iget-object v0, p0, Lcom/a/b/d/uz;->a:Ljava/util/NavigableMap;

    invoke-interface {v0, p1}, Ljava/util/NavigableMap;->lowerEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/sz;->d(Ljava/util/Map$Entry;)Ljava/util/Map$Entry;

    move-result-object v0

    return-object v0
.end method

.method public final lowerKey(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 3131
    iget-object v0, p0, Lcom/a/b/d/uz;->a:Ljava/util/NavigableMap;

    invoke-interface {v0, p1}, Ljava/util/NavigableMap;->lowerKey(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final navigableKeySet()Ljava/util/NavigableSet;
    .registers 2

    .prologue
    .line 3201
    iget-object v0, p0, Lcom/a/b/d/uz;->a:Ljava/util/NavigableMap;

    invoke-interface {v0}, Ljava/util/NavigableMap;->navigableKeySet()Ljava/util/NavigableSet;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/aad;->a(Ljava/util/NavigableSet;)Ljava/util/NavigableSet;

    move-result-object v0

    return-object v0
.end method

.method public final pollFirstEntry()Ljava/util/Map$Entry;
    .registers 2

    .prologue
    .line 3176
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final pollLastEntry()Ljava/util/Map$Entry;
    .registers 2

    .prologue
    .line 3181
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final subMap(Ljava/lang/Object;ZLjava/lang/Object;Z)Ljava/util/NavigableMap;
    .registers 6

    .prologue
    .line 3228
    iget-object v0, p0, Lcom/a/b/d/uz;->a:Ljava/util/NavigableMap;

    invoke-interface {v0, p1, p2, p3, p4}, Ljava/util/NavigableMap;->subMap(Ljava/lang/Object;ZLjava/lang/Object;Z)Ljava/util/NavigableMap;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/sz;->a(Ljava/util/NavigableMap;)Ljava/util/NavigableMap;

    move-result-object v0

    return-object v0
.end method

.method public final subMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedMap;
    .registers 5

    .prologue
    .line 3211
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, p2, v1}, Lcom/a/b/d/uz;->subMap(Ljava/lang/Object;ZLjava/lang/Object;Z)Ljava/util/NavigableMap;

    move-result-object v0

    return-object v0
.end method

.method public final tailMap(Ljava/lang/Object;Z)Ljava/util/NavigableMap;
    .registers 4

    .prologue
    .line 3242
    iget-object v0, p0, Lcom/a/b/d/uz;->a:Ljava/util/NavigableMap;

    invoke-interface {v0, p1, p2}, Ljava/util/NavigableMap;->tailMap(Ljava/lang/Object;Z)Ljava/util/NavigableMap;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/sz;->a(Ljava/util/NavigableMap;)Ljava/util/NavigableMap;

    move-result-object v0

    return-object v0
.end method

.method public final tailMap(Ljava/lang/Object;)Ljava/util/SortedMap;
    .registers 3

    .prologue
    .line 3221
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/a/b/d/uz;->tailMap(Ljava/lang/Object;Z)Ljava/util/NavigableMap;

    move-result-object v0

    return-object v0
.end method
