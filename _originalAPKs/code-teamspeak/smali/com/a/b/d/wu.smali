.class public final Lcom/a/b/d/wu;
.super Lcom/a/b/d/wv;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/d/ou;


# direct methods
.method public constructor <init>(Lcom/a/b/d/ou;Lcom/a/b/d/tv;)V
    .registers 3

    .prologue
    .line 1389
    invoke-direct {p0, p1, p2}, Lcom/a/b/d/wv;-><init>(Lcom/a/b/d/vi;Lcom/a/b/d/tv;)V

    .line 1390
    return-void
.end method

.method private b(Ljava/lang/Object;Ljava/util/Collection;)Ljava/util/List;
    .registers 4

    .prologue
    .line 1393
    check-cast p2, Ljava/util/List;

    iget-object v0, p0, Lcom/a/b/d/wu;->b:Lcom/a/b/d/tv;

    invoke-static {v0, p1}, Lcom/a/b/d/sz;->a(Lcom/a/b/d/tv;Ljava/lang/Object;)Lcom/a/b/b/bj;

    move-result-object v0

    invoke-static {p2, v0}, Lcom/a/b/d/ov;->a(Ljava/util/List;Lcom/a/b/b/bj;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method final synthetic a(Ljava/lang/Object;Ljava/util/Collection;)Ljava/util/Collection;
    .registers 4

    .prologue
    .line 1383
    invoke-direct {p0, p1, p2}, Lcom/a/b/d/wu;->b(Ljava/lang/Object;Ljava/util/Collection;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Ljava/util/List;
    .registers 3

    .prologue
    .line 1397
    iget-object v0, p0, Lcom/a/b/d/wu;->a:Lcom/a/b/d/vi;

    invoke-interface {v0, p1}, Lcom/a/b/d/vi;->c(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/a/b/d/wu;->b(Ljava/lang/Object;Ljava/util/Collection;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/List;
    .registers 4

    .prologue
    .line 1407
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final synthetic b(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/Collection;
    .registers 4

    .prologue
    .line 1383
    invoke-virtual {p0, p1, p2}, Lcom/a/b/d/wu;->a(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/Object;)Ljava/util/List;
    .registers 3

    .prologue
    .line 1402
    iget-object v0, p0, Lcom/a/b/d/wu;->a:Lcom/a/b/d/vi;

    invoke-interface {v0, p1}, Lcom/a/b/d/vi;->d(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/a/b/d/wu;->b(Ljava/lang/Object;Ljava/util/Collection;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c(Ljava/lang/Object;)Ljava/util/Collection;
    .registers 3

    .prologue
    .line 1383
    invoke-virtual {p0, p1}, Lcom/a/b/d/wu;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic d(Ljava/lang/Object;)Ljava/util/Collection;
    .registers 3

    .prologue
    .line 1383
    invoke-virtual {p0, p1}, Lcom/a/b/d/wu;->b(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
