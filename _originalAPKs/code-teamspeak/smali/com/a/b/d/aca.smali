.class final Lcom/a/b/d/aca;
.super Lcom/a/b/d/uj;
.source "SourceFile"


# instance fields
.field final a:Ljava/lang/Object;

.field final synthetic b:Lcom/a/b/d/abx;


# direct methods
.method constructor <init>(Lcom/a/b/d/abx;Ljava/lang/Object;)V
    .registers 4

    .prologue
    .line 399
    iput-object p1, p0, Lcom/a/b/d/aca;->b:Lcom/a/b/d/abx;

    invoke-direct {p0}, Lcom/a/b/d/uj;-><init>()V

    .line 400
    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/aca;->a:Ljava/lang/Object;

    .line 401
    return-void
.end method


# virtual methods
.method final a()Ljava/util/Set;
    .registers 3

    .prologue
    .line 444
    new-instance v0, Lcom/a/b/d/acb;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/a/b/d/acb;-><init>(Lcom/a/b/d/aca;B)V

    return-object v0
.end method

.method final a(Lcom/a/b/b/co;)Z
    .registers 7

    .prologue
    .line 424
    const/4 v0, 0x0

    .line 425
    iget-object v1, p0, Lcom/a/b/d/aca;->b:Lcom/a/b/d/abx;

    iget-object v1, v1, Lcom/a/b/d/abx;->a:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v2, v0

    .line 427
    :goto_e
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_48

    .line 428
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 429
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map;

    .line 430
    iget-object v4, p0, Lcom/a/b/d/aca;->a:Ljava/lang/Object;

    invoke-interface {v1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    .line 431
    if-eqz v4, :cond_45

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0, v4}, Lcom/a/b/d/sz;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/a/b/b/co;->a(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_45

    .line 433
    iget-object v0, p0, Lcom/a/b/d/aca;->a:Ljava/lang/Object;

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 434
    const/4 v2, 0x1

    .line 435
    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_45

    .line 436
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    :cond_45
    move v0, v2

    move v2, v0

    .line 439
    goto :goto_e

    .line 440
    :cond_48
    return v2
.end method

.method final c_()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 539
    new-instance v0, Lcom/a/b/d/acf;

    invoke-direct {v0, p0}, Lcom/a/b/d/acf;-><init>(Lcom/a/b/d/aca;)V

    return-object v0
.end method

.method public final containsKey(Ljava/lang/Object;)Z
    .registers 4

    .prologue
    .line 412
    iget-object v0, p0, Lcom/a/b/d/aca;->b:Lcom/a/b/d/abx;

    iget-object v1, p0, Lcom/a/b/d/aca;->a:Ljava/lang/Object;

    invoke-virtual {v0, p1, v1}, Lcom/a/b/d/abx;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method final e()Ljava/util/Set;
    .registers 2

    .prologue
    .line 516
    new-instance v0, Lcom/a/b/d/ace;

    invoke-direct {v0, p0}, Lcom/a/b/d/ace;-><init>(Lcom/a/b/d/aca;)V

    return-object v0
.end method

.method public final get(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 408
    iget-object v0, p0, Lcom/a/b/d/aca;->b:Lcom/a/b/d/abx;

    iget-object v1, p0, Lcom/a/b/d/aca;->a:Ljava/lang/Object;

    invoke-virtual {v0, p1, v1}, Lcom/a/b/d/abx;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 5

    .prologue
    .line 404
    iget-object v0, p0, Lcom/a/b/d/aca;->b:Lcom/a/b/d/abx;

    iget-object v1, p0, Lcom/a/b/d/aca;->a:Ljava/lang/Object;

    invoke-virtual {v0, p1, v1, p2}, Lcom/a/b/d/abx;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final remove(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 416
    iget-object v0, p0, Lcom/a/b/d/aca;->b:Lcom/a/b/d/abx;

    iget-object v1, p0, Lcom/a/b/d/aca;->a:Ljava/lang/Object;

    invoke-virtual {v0, p1, v1}, Lcom/a/b/d/abx;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
