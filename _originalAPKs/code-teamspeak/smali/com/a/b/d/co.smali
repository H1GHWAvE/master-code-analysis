.class Lcom/a/b/d/co;
.super Ljava/util/AbstractCollection;
.source "SourceFile"


# instance fields
.field final a:Ljava/util/Collection;

.field final b:Lcom/a/b/b/co;


# direct methods
.method constructor <init>(Ljava/util/Collection;Lcom/a/b/b/co;)V
    .registers 3

    .prologue
    .line 139
    invoke-direct {p0}, Ljava/util/AbstractCollection;-><init>()V

    .line 140
    iput-object p1, p0, Lcom/a/b/d/co;->a:Ljava/util/Collection;

    .line 141
    iput-object p2, p0, Lcom/a/b/d/co;->b:Lcom/a/b/b/co;

    .line 142
    return-void
.end method

.method private a(Lcom/a/b/b/co;)Lcom/a/b/d/co;
    .registers 5

    .prologue
    .line 145
    new-instance v0, Lcom/a/b/d/co;

    iget-object v1, p0, Lcom/a/b/d/co;->a:Ljava/util/Collection;

    iget-object v2, p0, Lcom/a/b/d/co;->b:Lcom/a/b/b/co;

    invoke-static {v2, p1}, Lcom/a/b/b/cp;->a(Lcom/a/b/b/co;Lcom/a/b/b/co;)Lcom/a/b/b/co;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/a/b/d/co;-><init>(Ljava/util/Collection;Lcom/a/b/b/co;)V

    return-object v0
.end method


# virtual methods
.method public add(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 152
    iget-object v0, p0, Lcom/a/b/d/co;->b:Lcom/a/b/b/co;

    invoke-interface {v0, p1}, Lcom/a/b/b/co;->a(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Lcom/a/b/b/cn;->a(Z)V

    .line 153
    iget-object v0, p0, Lcom/a/b/d/co;->a:Ljava/util/Collection;

    invoke-interface {v0, p1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public addAll(Ljava/util/Collection;)Z
    .registers 5

    .prologue
    .line 158
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_4
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_18

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 159
    iget-object v2, p0, Lcom/a/b/d/co;->b:Lcom/a/b/b/co;

    invoke-interface {v2, v1}, Lcom/a/b/b/co;->a(Ljava/lang/Object;)Z

    move-result v1

    invoke-static {v1}, Lcom/a/b/b/cn;->a(Z)V

    goto :goto_4

    .line 161
    :cond_18
    iget-object v0, p0, Lcom/a/b/d/co;->a:Ljava/util/Collection;

    invoke-interface {v0, p1}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public clear()V
    .registers 3

    .prologue
    .line 166
    iget-object v0, p0, Lcom/a/b/d/co;->a:Ljava/util/Collection;

    iget-object v1, p0, Lcom/a/b/d/co;->b:Lcom/a/b/b/co;

    invoke-static {v0, v1}, Lcom/a/b/d/mq;->a(Ljava/lang/Iterable;Lcom/a/b/b/co;)Z

    .line 167
    return-void
.end method

.method public contains(Ljava/lang/Object;)Z
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 171
    iget-object v0, p0, Lcom/a/b/d/co;->a:Ljava/util/Collection;

    invoke-static {v0, p1}, Lcom/a/b/d/cm;->a(Ljava/util/Collection;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 174
    iget-object v0, p0, Lcom/a/b/d/co;->b:Lcom/a/b/b/co;

    invoke-interface {v0, p1}, Lcom/a/b/b/co;->a(Ljava/lang/Object;)Z

    move-result v0

    .line 176
    :goto_e
    return v0

    :cond_f
    const/4 v0, 0x0

    goto :goto_e
.end method

.method public containsAll(Ljava/util/Collection;)Z
    .registers 3

    .prologue
    .line 181
    invoke-static {p0, p1}, Lcom/a/b/d/cm;->a(Ljava/util/Collection;Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public isEmpty()Z
    .registers 3

    .prologue
    .line 186
    iget-object v0, p0, Lcom/a/b/d/co;->a:Ljava/util/Collection;

    iget-object v1, p0, Lcom/a/b/d/co;->b:Lcom/a/b/b/co;

    invoke-static {v0, v1}, Lcom/a/b/d/mq;->d(Ljava/lang/Iterable;Lcom/a/b/b/co;)Z

    move-result v0

    if-nez v0, :cond_c

    const/4 v0, 0x1

    :goto_b
    return v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method

.method public iterator()Ljava/util/Iterator;
    .registers 3

    .prologue
    .line 191
    iget-object v0, p0, Lcom/a/b/d/co;->a:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/d/co;->b:Lcom/a/b/b/co;

    invoke-static {v0, v1}, Lcom/a/b/d/nj;->b(Ljava/util/Iterator;Lcom/a/b/b/co;)Lcom/a/b/d/agi;

    move-result-object v0

    return-object v0
.end method

.method public remove(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 196
    invoke-virtual {p0, p1}, Lcom/a/b/d/co;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/a/b/d/co;->a:Ljava/util/Collection;

    invoke-interface {v0, p1}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    const/4 v0, 0x1

    :goto_f
    return v0

    :cond_10
    const/4 v0, 0x0

    goto :goto_f
.end method

.method public removeAll(Ljava/util/Collection;)Z
    .registers 5

    .prologue
    .line 201
    iget-object v0, p0, Lcom/a/b/d/co;->a:Ljava/util/Collection;

    iget-object v1, p0, Lcom/a/b/d/co;->b:Lcom/a/b/b/co;

    invoke-static {p1}, Lcom/a/b/b/cp;->a(Ljava/util/Collection;)Lcom/a/b/b/co;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/a/b/b/cp;->a(Lcom/a/b/b/co;Lcom/a/b/b/co;)Lcom/a/b/b/co;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/a/b/d/mq;->a(Ljava/lang/Iterable;Lcom/a/b/b/co;)Z

    move-result v0

    return v0
.end method

.method public retainAll(Ljava/util/Collection;)Z
    .registers 5

    .prologue
    .line 206
    iget-object v0, p0, Lcom/a/b/d/co;->a:Ljava/util/Collection;

    iget-object v1, p0, Lcom/a/b/d/co;->b:Lcom/a/b/b/co;

    invoke-static {p1}, Lcom/a/b/b/cp;->a(Ljava/util/Collection;)Lcom/a/b/b/co;

    move-result-object v2

    invoke-static {v2}, Lcom/a/b/b/cp;->a(Lcom/a/b/b/co;)Lcom/a/b/b/co;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/a/b/b/cp;->a(Lcom/a/b/b/co;Lcom/a/b/b/co;)Lcom/a/b/b/co;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/a/b/d/mq;->a(Ljava/lang/Iterable;Lcom/a/b/b/co;)Z

    move-result v0

    return v0
.end method

.method public size()I
    .registers 2

    .prologue
    .line 211
    invoke-virtual {p0}, Lcom/a/b/d/co;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/nj;->b(Ljava/util/Iterator;)I

    move-result v0

    return v0
.end method

.method public toArray()[Ljava/lang/Object;
    .registers 2

    .prologue
    .line 217
    invoke-virtual {p0}, Lcom/a/b/d/co;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/ov;->a(Ljava/util/Iterator;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public toArray([Ljava/lang/Object;)[Ljava/lang/Object;
    .registers 3

    .prologue
    .line 222
    invoke-virtual {p0}, Lcom/a/b/d/co;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/ov;->a(Ljava/util/Iterator;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
