.class public final Lcom/a/b/d/ve;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/a;
.end annotation


# static fields
.field private static final d:I = -0x1


# instance fields
.field final a:Ljava/util/Comparator;

.field b:I

.field c:I


# direct methods
.method private constructor <init>(Ljava/util/Comparator;)V
    .registers 3

    .prologue
    .line 166
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 163
    const/4 v0, -0x1

    iput v0, p0, Lcom/a/b/d/ve;->b:I

    .line 164
    const v0, 0x7fffffff

    iput v0, p0, Lcom/a/b/d/ve;->c:I

    .line 167
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Comparator;

    iput-object v0, p0, Lcom/a/b/d/ve;->a:Ljava/util/Comparator;

    .line 168
    return-void
.end method

.method synthetic constructor <init>(Ljava/util/Comparator;B)V
    .registers 3

    .prologue
    .line 155
    invoke-direct {p0, p1}, Lcom/a/b/d/ve;-><init>(Ljava/util/Comparator;)V

    return-void
.end method

.method private a()Lcom/a/b/d/vc;
    .registers 2

    .prologue
    .line 197
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/a/b/d/ve;->a(Ljava/lang/Iterable;)Lcom/a/b/d/vc;

    move-result-object v0

    return-object v0
.end method

.method private a(I)Lcom/a/b/d/ve;
    .registers 3

    .prologue
    .line 175
    if-ltz p1, :cond_9

    const/4 v0, 0x1

    :goto_3
    invoke-static {v0}, Lcom/a/b/b/cn;->a(Z)V

    .line 176
    iput p1, p0, Lcom/a/b/d/ve;->b:I

    .line 177
    return-object p0

    .line 175
    :cond_9
    const/4 v0, 0x0

    goto :goto_3
.end method

.method private static synthetic a(Lcom/a/b/d/ve;)Lcom/a/b/d/yd;
    .registers 2

    .prologue
    .line 155
    .line 1216
    iget-object v0, p0, Lcom/a/b/d/ve;->a:Ljava/util/Comparator;

    invoke-static {v0}, Lcom/a/b/d/yd;->a(Ljava/util/Comparator;)Lcom/a/b/d/yd;

    move-result-object v0

    .line 155
    return-object v0
.end method

.method private static synthetic b(Lcom/a/b/d/ve;)I
    .registers 2

    .prologue
    .line 155
    iget v0, p0, Lcom/a/b/d/ve;->c:I

    return v0
.end method

.method private b(I)Lcom/a/b/d/ve;
    .registers 3

    .prologue
    .line 187
    if-lez p1, :cond_9

    const/4 v0, 0x1

    :goto_3
    invoke-static {v0}, Lcom/a/b/b/cn;->a(Z)V

    .line 188
    iput p1, p0, Lcom/a/b/d/ve;->c:I

    .line 189
    return-object p0

    .line 187
    :cond_9
    const/4 v0, 0x0

    goto :goto_3
.end method

.method private b()Lcom/a/b/d/yd;
    .registers 2

    .prologue
    .line 216
    iget-object v0, p0, Lcom/a/b/d/ve;->a:Ljava/util/Comparator;

    invoke-static {v0}, Lcom/a/b/d/yd;->a(Ljava/util/Comparator;)Lcom/a/b/d/yd;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Iterable;)Lcom/a/b/d/vc;
    .registers 5

    .prologue
    .line 206
    new-instance v0, Lcom/a/b/d/vc;

    iget v1, p0, Lcom/a/b/d/ve;->b:I

    iget v2, p0, Lcom/a/b/d/ve;->c:I

    invoke-static {v1, v2, p1}, Lcom/a/b/d/vc;->a(IILjava/lang/Iterable;)I

    move-result v1

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Lcom/a/b/d/vc;-><init>(Lcom/a/b/d/ve;IB)V

    .line 208
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_12
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_20

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 209
    invoke-virtual {v0, v2}, Lcom/a/b/d/vc;->offer(Ljava/lang/Object;)Z

    goto :goto_12

    .line 211
    :cond_20
    return-object v0
.end method
