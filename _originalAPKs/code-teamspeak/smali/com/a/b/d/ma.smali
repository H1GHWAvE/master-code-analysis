.class public abstract Lcom/a/b/d/ma;
.super Lcom/a/b/d/md;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/d/abn;


# annotations
.annotation build Lcom/a/b/a/a;
.end annotation

.annotation build Lcom/a/b/a/c;
    a = "hasn\'t been tested yet"
.end annotation


# static fields
.field private static final b:Ljava/util/Comparator;

.field private static final c:Lcom/a/b/d/ma;


# instance fields
.field transient a:Lcom/a/b/d/ma;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 86
    invoke-static {}, Lcom/a/b/d/yd;->d()Lcom/a/b/d/yd;

    move-result-object v0

    sput-object v0, Lcom/a/b/d/ma;->b:Ljava/util/Comparator;

    .line 88
    new-instance v0, Lcom/a/b/d/fb;

    sget-object v1, Lcom/a/b/d/ma;->b:Ljava/util/Comparator;

    invoke-direct {v0, v1}, Lcom/a/b/d/fb;-><init>(Ljava/util/Comparator;)V

    sput-object v0, Lcom/a/b/d/ma;->c:Lcom/a/b/d/ma;

    return-void
.end method

.method constructor <init>()V
    .registers 1

    .prologue
    .line 318
    invoke-direct {p0}, Lcom/a/b/d/md;-><init>()V

    return-void
.end method

.method public static a(Lcom/a/b/d/abn;)Lcom/a/b/d/ma;
    .registers 3

    .prologue
    .line 286
    invoke-interface {p0}, Lcom/a/b/d/abn;->comparator()Ljava/util/Comparator;

    move-result-object v0

    invoke-interface {p0}, Lcom/a/b/d/abn;->a()Ljava/util/Set;

    move-result-object v1

    invoke-static {v1}, Lcom/a/b/d/ov;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/a/b/d/ma;->a(Ljava/util/Comparator;Ljava/util/Collection;)Lcom/a/b/d/ma;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/Comparable;)Lcom/a/b/d/ma;
    .registers 7

    .prologue
    const/4 v4, 0x0

    const/4 v5, 0x1

    .line 103
    invoke-static {p0}, Lcom/a/b/d/me;->a(Ljava/lang/Comparable;)Lcom/a/b/d/me;

    move-result-object v1

    check-cast v1, Lcom/a/b/d/zq;

    .line 105
    new-array v2, v5, [I

    aput v5, v2, v4

    .line 106
    const/4 v0, 0x2

    new-array v3, v0, [J

    fill-array-data v3, :array_18

    .line 107
    new-instance v0, Lcom/a/b/d/zp;

    invoke-direct/range {v0 .. v5}, Lcom/a/b/d/zp;-><init>(Lcom/a/b/d/zq;[I[JII)V

    return-object v0

    .line 106
    :array_18
    .array-data 8
        0x0
        0x1
    .end array-data
.end method

.method private static a(Ljava/lang/Comparable;Ljava/lang/Comparable;)Lcom/a/b/d/ma;
    .registers 5

    .prologue
    .line 118
    invoke-static {}, Lcom/a/b/d/yd;->d()Lcom/a/b/d/yd;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Comparable;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    const/4 v2, 0x1

    aput-object p1, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/a/b/d/ma;->a(Ljava/util/Comparator;Ljava/lang/Iterable;)Lcom/a/b/d/ma;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/Comparable;Ljava/lang/Comparable;Ljava/lang/Comparable;)Lcom/a/b/d/ma;
    .registers 6

    .prologue
    .line 129
    invoke-static {}, Lcom/a/b/d/yd;->d()Lcom/a/b/d/yd;

    move-result-object v0

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Comparable;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    const/4 v2, 0x1

    aput-object p1, v1, v2

    const/4 v2, 0x2

    aput-object p2, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/a/b/d/ma;->a(Ljava/util/Comparator;Ljava/lang/Iterable;)Lcom/a/b/d/ma;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/Comparable;Ljava/lang/Comparable;Ljava/lang/Comparable;Ljava/lang/Comparable;)Lcom/a/b/d/ma;
    .registers 7

    .prologue
    .line 141
    invoke-static {}, Lcom/a/b/d/yd;->d()Lcom/a/b/d/yd;

    move-result-object v0

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Comparable;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    const/4 v2, 0x1

    aput-object p1, v1, v2

    const/4 v2, 0x2

    aput-object p2, v1, v2

    const/4 v2, 0x3

    aput-object p3, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/a/b/d/ma;->a(Ljava/util/Comparator;Ljava/lang/Iterable;)Lcom/a/b/d/ma;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/Comparable;Ljava/lang/Comparable;Ljava/lang/Comparable;Ljava/lang/Comparable;Ljava/lang/Comparable;)Lcom/a/b/d/ma;
    .registers 8

    .prologue
    .line 153
    invoke-static {}, Lcom/a/b/d/yd;->d()Lcom/a/b/d/yd;

    move-result-object v0

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/Comparable;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    const/4 v2, 0x1

    aput-object p1, v1, v2

    const/4 v2, 0x2

    aput-object p2, v1, v2

    const/4 v2, 0x3

    aput-object p3, v1, v2

    const/4 v2, 0x4

    aput-object p4, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/a/b/d/ma;->a(Ljava/util/Comparator;Ljava/lang/Iterable;)Lcom/a/b/d/ma;

    move-result-object v0

    return-object v0
.end method

.method private static varargs a(Ljava/lang/Comparable;Ljava/lang/Comparable;Ljava/lang/Comparable;Ljava/lang/Comparable;Ljava/lang/Comparable;Ljava/lang/Comparable;[Ljava/lang/Comparable;)Lcom/a/b/d/ma;
    .registers 10

    .prologue
    .line 165
    array-length v0, p6

    add-int/lit8 v0, v0, 0x6

    .line 166
    invoke-static {v0}, Lcom/a/b/d/ov;->a(I)Ljava/util/ArrayList;

    move-result-object v0

    .line 167
    const/4 v1, 0x6

    new-array v1, v1, [Ljava/lang/Comparable;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    const/4 v2, 0x1

    aput-object p1, v1, v2

    const/4 v2, 0x2

    aput-object p2, v1, v2

    const/4 v2, 0x3

    aput-object p3, v1, v2

    const/4 v2, 0x4

    aput-object p4, v1, v2

    const/4 v2, 0x5

    aput-object p5, v1, v2

    invoke-static {v0, v1}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    .line 168
    invoke-static {v0, p6}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    .line 169
    invoke-static {}, Lcom/a/b/d/yd;->d()Lcom/a/b/d/yd;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/a/b/d/ma;->a(Ljava/util/Comparator;Ljava/lang/Iterable;)Lcom/a/b/d/ma;

    move-result-object v0

    return-object v0
.end method

.method static a(Ljava/util/Comparator;)Lcom/a/b/d/ma;
    .registers 2

    .prologue
    .line 312
    sget-object v0, Lcom/a/b/d/ma;->b:Ljava/util/Comparator;

    invoke-interface {v0, p0}, Ljava/util/Comparator;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 313
    sget-object v0, Lcom/a/b/d/ma;->c:Lcom/a/b/d/ma;

    .line 315
    :goto_a
    return-object v0

    :cond_b
    new-instance v0, Lcom/a/b/d/fb;

    invoke-direct {v0, p0}, Lcom/a/b/d/fb;-><init>(Ljava/util/Comparator;)V

    goto :goto_a
.end method

.method private static a(Ljava/util/Comparator;Ljava/lang/Iterable;)Lcom/a/b/d/ma;
    .registers 4

    .prologue
    .line 254
    instance-of v0, p1, Lcom/a/b/d/ma;

    if-eqz v0, :cond_24

    move-object v0, p1

    .line 256
    check-cast v0, Lcom/a/b/d/ma;

    .line 257
    invoke-virtual {v0}, Lcom/a/b/d/ma;->comparator()Ljava/util/Comparator;

    move-result-object v1

    invoke-interface {p0, v1}, Ljava/util/Comparator;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_24

    .line 258
    invoke-virtual {v0}, Lcom/a/b/d/ma;->h_()Z

    move-result v1

    if-eqz v1, :cond_23

    .line 259
    invoke-virtual {v0}, Lcom/a/b/d/ma;->o()Lcom/a/b/d/lo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/lo;->f()Lcom/a/b/d/jl;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/a/b/d/ma;->a(Ljava/util/Comparator;Ljava/util/Collection;)Lcom/a/b/d/ma;

    move-result-object v0

    .line 268
    :cond_23
    :goto_23
    return-object v0

    .line 265
    :cond_24
    invoke-static {p1}, Lcom/a/b/d/ov;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v1

    .line 266
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Comparator;

    invoke-static {v0}, Lcom/a/b/d/aer;->a(Ljava/util/Comparator;)Lcom/a/b/d/aer;

    move-result-object v0

    .line 267
    invoke-static {v0, v1}, Lcom/a/b/d/mq;->a(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    .line 268
    invoke-virtual {v0}, Lcom/a/b/d/aer;->a()Ljava/util/Set;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/a/b/d/ma;->a(Ljava/util/Comparator;Ljava/util/Collection;)Lcom/a/b/d/ma;

    move-result-object v0

    goto :goto_23
.end method

.method private static a(Ljava/util/Comparator;Ljava/util/Collection;)Lcom/a/b/d/ma;
    .registers 14

    .prologue
    const/4 v4, 0x0

    .line 292
    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 293
    invoke-static {p0}, Lcom/a/b/d/ma;->a(Ljava/util/Comparator;)Lcom/a/b/d/ma;

    move-result-object v0

    .line 305
    :goto_b
    return-object v0

    .line 295
    :cond_c
    new-instance v5, Lcom/a/b/d/jn;

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-direct {v5, v0}, Lcom/a/b/d/jn;-><init>(I)V

    .line 296
    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v0

    new-array v2, v0, [I

    .line 297
    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    new-array v3, v0, [J

    .line 299
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v1, v4

    :goto_28
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4f

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/xd;

    .line 300
    invoke-interface {v0}, Lcom/a/b/d/xd;->a()Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v5, v7}, Lcom/a/b/d/jn;->c(Ljava/lang/Object;)Lcom/a/b/d/jn;

    .line 301
    invoke-interface {v0}, Lcom/a/b/d/xd;->b()I

    move-result v0

    aput v0, v2, v1

    .line 302
    add-int/lit8 v0, v1, 0x1

    aget-wide v8, v3, v1

    aget v7, v2, v1

    int-to-long v10, v7

    add-long/2addr v8, v10

    aput-wide v8, v3, v0

    .line 303
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 304
    goto :goto_28

    .line 305
    :cond_4f
    new-instance v0, Lcom/a/b/d/zp;

    new-instance v1, Lcom/a/b/d/zq;

    invoke-virtual {v5}, Lcom/a/b/d/jn;->b()Lcom/a/b/d/jl;

    move-result-object v5

    invoke-direct {v1, v5, p0}, Lcom/a/b/d/zq;-><init>(Lcom/a/b/d/jl;Ljava/util/Comparator;)V

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v5

    invoke-direct/range {v0 .. v5}, Lcom/a/b/d/zp;-><init>(Lcom/a/b/d/zq;[I[JII)V

    goto :goto_b
.end method

.method private static a(Ljava/util/Comparator;Ljava/util/Iterator;)Lcom/a/b/d/ma;
    .registers 3

    .prologue
    .line 238
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 239
    new-instance v0, Lcom/a/b/d/mb;

    invoke-direct {v0, p0}, Lcom/a/b/d/mb;-><init>(Ljava/util/Comparator;)V

    invoke-virtual {v0, p1}, Lcom/a/b/d/mb;->c(Ljava/util/Iterator;)Lcom/a/b/d/mb;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/mb;->c()Lcom/a/b/d/ma;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/util/Iterator;)Lcom/a/b/d/ma;
    .registers 3

    .prologue
    .line 226
    invoke-static {}, Lcom/a/b/d/yd;->d()Lcom/a/b/d/yd;

    move-result-object v0

    .line 1238
    invoke-static {v0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1239
    new-instance v1, Lcom/a/b/d/mb;

    invoke-direct {v1, v0}, Lcom/a/b/d/mb;-><init>(Ljava/util/Comparator;)V

    invoke-virtual {v1, p0}, Lcom/a/b/d/mb;->c(Ljava/util/Iterator;)Lcom/a/b/d/mb;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/mb;->c()Lcom/a/b/d/ma;

    move-result-object v0

    .line 227
    return-object v0
.end method

.method private static a([Ljava/lang/Comparable;)Lcom/a/b/d/ma;
    .registers 3

    .prologue
    .line 179
    invoke-static {}, Lcom/a/b/d/yd;->d()Lcom/a/b/d/yd;

    move-result-object v0

    invoke-static {p0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/a/b/d/ma;->a(Ljava/util/Comparator;Ljava/lang/Iterable;)Lcom/a/b/d/ma;

    move-result-object v0

    return-object v0
.end method

.method private static b(Ljava/lang/Iterable;)Lcom/a/b/d/ma;
    .registers 2

    .prologue
    .line 208
    invoke-static {}, Lcom/a/b/d/yd;->d()Lcom/a/b/d/yd;

    move-result-object v0

    .line 209
    invoke-static {v0, p0}, Lcom/a/b/d/ma;->a(Ljava/util/Comparator;Ljava/lang/Iterable;)Lcom/a/b/d/ma;

    move-result-object v0

    return-object v0
.end method

.method private b(Ljava/lang/Object;Lcom/a/b/d/ce;Ljava/lang/Object;Lcom/a/b/d/ce;)Lcom/a/b/d/ma;
    .registers 10

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 373
    invoke-virtual {p0}, Lcom/a/b/d/ma;->comparator()Ljava/util/Comparator;

    move-result-object v0

    invoke-interface {v0, p1, p3}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    if-gtz v0, :cond_22

    move v0, v1

    :goto_d
    const-string v3, "Expected lowerBound <= upperBound but %s > %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p1, v4, v2

    aput-object p3, v4, v1

    invoke-static {v0, v3, v4}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 375
    invoke-virtual {p0, p1, p2}, Lcom/a/b/d/ma;->b(Ljava/lang/Object;Lcom/a/b/d/ce;)Lcom/a/b/d/ma;

    move-result-object v0

    invoke-virtual {v0, p3, p4}, Lcom/a/b/d/ma;->a(Ljava/lang/Object;Lcom/a/b/d/ce;)Lcom/a/b/d/ma;

    move-result-object v0

    return-object v0

    :cond_22
    move v0, v2

    .line 373
    goto :goto_d
.end method

.method private static b(Ljava/util/Comparator;)Lcom/a/b/d/mb;
    .registers 2

    .prologue
    .line 390
    new-instance v0, Lcom/a/b/d/mb;

    invoke-direct {v0, p0}, Lcom/a/b/d/mb;-><init>(Ljava/util/Comparator;)V

    return-object v0
.end method

.method private static p()Lcom/a/b/d/ma;
    .registers 1

    .prologue
    .line 96
    sget-object v0, Lcom/a/b/d/ma;->c:Lcom/a/b/d/ma;

    return-object v0
.end method

.method private static q()Lcom/a/b/d/mb;
    .registers 2

    .prologue
    .line 402
    new-instance v0, Lcom/a/b/d/mb;

    invoke-static {}, Lcom/a/b/d/yd;->d()Lcom/a/b/d/yd;

    move-result-object v1

    invoke-virtual {v1}, Lcom/a/b/d/yd;->a()Lcom/a/b/d/yd;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/a/b/d/mb;-><init>(Ljava/util/Comparator;)V

    return-object v0
.end method

.method private static r()Lcom/a/b/d/mb;
    .registers 2

    .prologue
    .line 416
    new-instance v0, Lcom/a/b/d/mb;

    invoke-static {}, Lcom/a/b/d/yd;->d()Lcom/a/b/d/yd;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/a/b/d/mb;-><init>(Ljava/util/Comparator;)V

    return-object v0
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Lcom/a/b/d/ce;Ljava/lang/Object;Lcom/a/b/d/ce;)Lcom/a/b/d/abn;
    .registers 10

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 80
    .line 1373
    invoke-virtual {p0}, Lcom/a/b/d/ma;->comparator()Ljava/util/Comparator;

    move-result-object v0

    invoke-interface {v0, p1, p3}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    if-gtz v0, :cond_22

    move v0, v1

    :goto_d
    const-string v3, "Expected lowerBound <= upperBound but %s > %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p1, v4, v2

    aput-object p3, v4, v1

    invoke-static {v0, v3, v4}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 1375
    invoke-virtual {p0, p1, p2}, Lcom/a/b/d/ma;->b(Ljava/lang/Object;Lcom/a/b/d/ce;)Lcom/a/b/d/ma;

    move-result-object v0

    invoke-virtual {v0, p3, p4}, Lcom/a/b/d/ma;->a(Ljava/lang/Object;Lcom/a/b/d/ce;)Lcom/a/b/d/ma;

    move-result-object v0

    .line 80
    return-object v0

    :cond_22
    move v0, v2

    .line 1373
    goto :goto_d
.end method

.method public abstract a(Ljava/lang/Object;Lcom/a/b/d/ce;)Lcom/a/b/d/ma;
.end method

.method public abstract b(Ljava/lang/Object;Lcom/a/b/d/ce;)Lcom/a/b/d/ma;
.end method

.method public abstract b()Lcom/a/b/d/me;
.end method

.method public synthetic c(Ljava/lang/Object;Lcom/a/b/d/ce;)Lcom/a/b/d/abn;
    .registers 4

    .prologue
    .line 80
    invoke-virtual {p0, p1, p2}, Lcom/a/b/d/ma;->b(Ljava/lang/Object;Lcom/a/b/d/ce;)Lcom/a/b/d/ma;

    move-result-object v0

    return-object v0
.end method

.method public final comparator()Ljava/util/Comparator;
    .registers 2

    .prologue
    .line 322
    invoke-virtual {p0}, Lcom/a/b/d/ma;->b()Lcom/a/b/d/me;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/me;->comparator()Ljava/util/Comparator;

    move-result-object v0

    return-object v0
.end method

.method public synthetic d(Ljava/lang/Object;Lcom/a/b/d/ce;)Lcom/a/b/d/abn;
    .registers 4

    .prologue
    .line 80
    invoke-virtual {p0, p1, p2}, Lcom/a/b/d/ma;->a(Ljava/lang/Object;Lcom/a/b/d/ce;)Lcom/a/b/d/ma;

    move-result-object v0

    return-object v0
.end method

.method public e()Lcom/a/b/d/ma;
    .registers 2

    .prologue
    .line 332
    iget-object v0, p0, Lcom/a/b/d/ma;->a:Lcom/a/b/d/ma;

    .line 333
    if-nez v0, :cond_b

    .line 334
    new-instance v0, Lcom/a/b/d/el;

    invoke-direct {v0, p0}, Lcom/a/b/d/el;-><init>(Lcom/a/b/d/ma;)V

    iput-object v0, p0, Lcom/a/b/d/ma;->a:Lcom/a/b/d/ma;

    .line 336
    :cond_b
    return-object v0
.end method

.method public synthetic e_()Ljava/util/NavigableSet;
    .registers 2

    .prologue
    .line 80
    invoke-virtual {p0}, Lcom/a/b/d/ma;->b()Lcom/a/b/d/me;

    move-result-object v0

    return-object v0
.end method

.method final g()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 573
    new-instance v0, Lcom/a/b/d/mc;

    invoke-direct {v0, p0}, Lcom/a/b/d/mc;-><init>(Lcom/a/b/d/abn;)V

    return-object v0
.end method

.method public final j()Lcom/a/b/d/xd;
    .registers 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 350
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final k()Lcom/a/b/d/xd;
    .registers 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 364
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public synthetic m()Lcom/a/b/d/abn;
    .registers 2

    .prologue
    .line 80
    invoke-virtual {p0}, Lcom/a/b/d/ma;->e()Lcom/a/b/d/ma;

    move-result-object v0

    return-object v0
.end method

.method public synthetic n()Ljava/util/SortedSet;
    .registers 2

    .prologue
    .line 80
    invoke-virtual {p0}, Lcom/a/b/d/ma;->b()Lcom/a/b/d/me;

    move-result-object v0

    return-object v0
.end method

.method public synthetic n_()Ljava/util/Set;
    .registers 2

    .prologue
    .line 80
    invoke-virtual {p0}, Lcom/a/b/d/ma;->b()Lcom/a/b/d/me;

    move-result-object v0

    return-object v0
.end method
