.class Lcom/a/b/d/qy;
.super Ljava/util/AbstractMap;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/util/concurrent/ConcurrentMap;


# static fields
.field private static final a:J = 0x5L

.field static final b:I = 0x40000000

.field static final c:I = 0x10000

.field static final d:I = 0x3

.field static final e:I = 0x3f

.field static final f:I = 0x10

.field static final g:J = 0x3cL

.field static final h:Ljava/util/logging/Logger;

.field static final x:Lcom/a/b/d/sr;

.field static final y:Ljava/util/Queue;


# instance fields
.field transient A:Ljava/util/Collection;

.field transient B:Ljava/util/Set;

.field final transient i:I

.field final transient j:I

.field final transient k:[Lcom/a/b/d/sa;

.field final l:I

.field final m:Lcom/a/b/b/au;

.field final n:Lcom/a/b/b/au;

.field final o:Lcom/a/b/d/sh;

.field final p:Lcom/a/b/d/sh;

.field final q:I

.field final r:J

.field final s:J

.field final t:Ljava/util/Queue;

.field final u:Lcom/a/b/d/qw;

.field final transient v:Lcom/a/b/d/re;

.field final w:Lcom/a/b/b/ej;

.field transient z:Ljava/util/Set;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 135
    const-class v0, Lcom/a/b/d/qy;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/a/b/d/qy;->h:Ljava/util/logging/Logger;

    .line 578
    new-instance v0, Lcom/a/b/d/qz;

    invoke-direct {v0}, Lcom/a/b/d/qz;-><init>()V

    sput-object v0, Lcom/a/b/d/qy;->x:Lcom/a/b/d/sr;

    .line 868
    new-instance v0, Lcom/a/b/d/ra;

    invoke-direct {v0}, Lcom/a/b/d/ra;-><init>()V

    sput-object v0, Lcom/a/b/d/qy;->y:Ljava/util/Queue;

    return-void
.end method

.method constructor <init>(Lcom/a/b/d/ql;)V
    .registers 10

    .prologue
    const-wide/16 v2, 0x0

    const-wide/16 v6, -0x1

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 195
    invoke-direct {p0}, Ljava/util/AbstractMap;-><init>()V

    .line 196
    invoke-virtual {p1}, Lcom/a/b/d/ql;->h()I

    move-result v0

    const/high16 v1, 0x10000

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lcom/a/b/d/qy;->l:I

    .line 198
    invoke-virtual {p1}, Lcom/a/b/d/ql;->i()Lcom/a/b/d/sh;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/qy;->o:Lcom/a/b/d/sh;

    .line 4349
    iget-object v0, p1, Lcom/a/b/d/ql;->h:Lcom/a/b/d/sh;

    sget-object v1, Lcom/a/b/d/sh;->a:Lcom/a/b/d/sh;

    invoke-static {v0, v1}, Lcom/a/b/b/ca;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/sh;

    .line 199
    iput-object v0, p0, Lcom/a/b/d/qy;->p:Lcom/a/b/d/sh;

    .line 5155
    iget-object v0, p1, Lcom/a/b/d/ql;->l:Lcom/a/b/b/au;

    invoke-virtual {p1}, Lcom/a/b/d/ql;->i()Lcom/a/b/d/sh;

    move-result-object v1

    invoke-virtual {v1}, Lcom/a/b/d/sh;->a()Lcom/a/b/b/au;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/a/b/b/ca;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/b/au;

    .line 201
    iput-object v0, p0, Lcom/a/b/d/qy;->m:Lcom/a/b/b/au;

    .line 202
    iget-object v0, p0, Lcom/a/b/d/qy;->p:Lcom/a/b/d/sh;

    invoke-virtual {v0}, Lcom/a/b/d/sh;->a()Lcom/a/b/b/au;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/qy;->n:Lcom/a/b/b/au;

    .line 204
    iget v0, p1, Lcom/a/b/d/ql;->f:I

    iput v0, p0, Lcom/a/b/d/qy;->q:I

    .line 5442
    iget-wide v0, p1, Lcom/a/b/d/ql;->j:J

    cmp-long v0, v0, v6

    if-nez v0, :cond_b1

    move-wide v0, v2

    .line 205
    :goto_4c
    iput-wide v0, p0, Lcom/a/b/d/qy;->r:J

    .line 6399
    iget-wide v0, p1, Lcom/a/b/d/ql;->i:J

    cmp-long v0, v0, v6

    if-nez v0, :cond_b4

    .line 206
    :goto_54
    iput-wide v2, p0, Lcom/a/b/d/qy;->s:J

    .line 208
    iget-object v0, p0, Lcom/a/b/d/qy;->o:Lcom/a/b/d/sh;

    invoke-virtual {p0}, Lcom/a/b/d/qy;->c()Z

    move-result v1

    invoke-virtual {p0}, Lcom/a/b/d/qy;->b()Z

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/a/b/d/re;->a(Lcom/a/b/d/sh;ZZ)Lcom/a/b/d/re;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/qy;->v:Lcom/a/b/d/re;

    .line 6447
    iget-object v0, p1, Lcom/a/b/d/ql;->m:Lcom/a/b/b/ej;

    invoke-static {}, Lcom/a/b/b/ej;->b()Lcom/a/b/b/ej;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/a/b/b/ca;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/b/ej;

    .line 209
    iput-object v0, p0, Lcom/a/b/d/qy;->w:Lcom/a/b/b/ej;

    .line 211
    invoke-virtual {p1}, Lcom/a/b/d/ql;->d()Lcom/a/b/d/qw;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/qy;->u:Lcom/a/b/d/qw;

    .line 212
    iget-object v0, p0, Lcom/a/b/d/qy;->u:Lcom/a/b/d/qw;

    sget-object v1, Lcom/a/b/d/hu;->a:Lcom/a/b/d/hu;

    if-ne v0, v1, :cond_b7

    .line 6900
    sget-object v0, Lcom/a/b/d/qy;->y:Ljava/util/Queue;

    .line 212
    :goto_82
    iput-object v0, p0, Lcom/a/b/d/qy;->t:Ljava/util/Queue;

    .line 216
    invoke-virtual {p1}, Lcom/a/b/d/ql;->g()I

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 217
    invoke-virtual {p0}, Lcom/a/b/d/qy;->b()Z

    move-result v1

    if-eqz v1, :cond_9a

    .line 218
    iget v1, p0, Lcom/a/b/d/qy;->q:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    :cond_9a
    move v1, v4

    move v2, v5

    .line 227
    :goto_9c
    iget v3, p0, Lcom/a/b/d/qy;->l:I

    if-ge v1, v3, :cond_bd

    invoke-virtual {p0}, Lcom/a/b/d/qy;->b()Z

    move-result v3

    if-eqz v3, :cond_ac

    mul-int/lit8 v3, v1, 0x2

    iget v6, p0, Lcom/a/b/d/qy;->q:I

    if-gt v3, v6, :cond_bd

    .line 228
    :cond_ac
    add-int/lit8 v2, v2, 0x1

    .line 229
    shl-int/lit8 v1, v1, 0x1

    goto :goto_9c

    .line 5442
    :cond_b1
    iget-wide v0, p1, Lcom/a/b/d/ql;->j:J

    goto :goto_4c

    .line 6399
    :cond_b4
    iget-wide v2, p1, Lcom/a/b/d/ql;->i:J

    goto :goto_54

    .line 212
    :cond_b7
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    goto :goto_82

    .line 231
    :cond_bd
    rsub-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/a/b/d/qy;->j:I

    .line 232
    add-int/lit8 v2, v1, -0x1

    iput v2, p0, Lcom/a/b/d/qy;->i:I

    .line 6974
    new-array v2, v1, [Lcom/a/b/d/sa;

    .line 234
    iput-object v2, p0, Lcom/a/b/d/qy;->k:[Lcom/a/b/d/sa;

    .line 236
    div-int v2, v0, v1

    .line 237
    mul-int v3, v2, v1

    if-ge v3, v0, :cond_10b

    .line 238
    add-int/lit8 v0, v2, 0x1

    .line 242
    :goto_d1
    if-ge v4, v0, :cond_d6

    .line 243
    shl-int/lit8 v4, v4, 0x1

    goto :goto_d1

    .line 246
    :cond_d6
    invoke-virtual {p0}, Lcom/a/b/d/qy;->b()Z

    move-result v0

    if-eqz v0, :cond_f9

    .line 248
    iget v0, p0, Lcom/a/b/d/qy;->q:I

    div-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x1

    .line 249
    iget v2, p0, Lcom/a/b/d/qy;->q:I

    rem-int v1, v2, v1

    .line 250
    :goto_e5
    iget-object v2, p0, Lcom/a/b/d/qy;->k:[Lcom/a/b/d/sa;

    array-length v2, v2

    if-ge v5, v2, :cond_10a

    .line 251
    if-ne v5, v1, :cond_ee

    .line 252
    add-int/lit8 v0, v0, -0x1

    .line 254
    :cond_ee
    iget-object v2, p0, Lcom/a/b/d/qy;->k:[Lcom/a/b/d/sa;

    invoke-virtual {p0, v4, v0}, Lcom/a/b/d/qy;->a(II)Lcom/a/b/d/sa;

    move-result-object v3

    aput-object v3, v2, v5

    .line 250
    add-int/lit8 v5, v5, 0x1

    goto :goto_e5

    .line 258
    :cond_f9
    :goto_f9
    iget-object v0, p0, Lcom/a/b/d/qy;->k:[Lcom/a/b/d/sa;

    array-length v0, v0

    if-ge v5, v0, :cond_10a

    .line 259
    iget-object v0, p0, Lcom/a/b/d/qy;->k:[Lcom/a/b/d/sa;

    const/4 v1, -0x1

    invoke-virtual {p0, v4, v1}, Lcom/a/b/d/qy;->a(II)Lcom/a/b/d/sa;

    move-result-object v1

    aput-object v1, v0, v5

    .line 258
    add-int/lit8 v5, v5, 0x1

    goto :goto_f9

    .line 263
    :cond_10a
    return-void

    :cond_10b
    move v0, v2

    goto :goto_d1
.end method

.method private a(Ljava/lang/Object;)Lcom/a/b/d/rz;
    .registers 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 3442
    if-nez p1, :cond_4

    .line 3443
    const/4 v0, 0x0

    .line 3446
    :goto_3
    return-object v0

    .line 3445
    :cond_4
    invoke-virtual {p0, p1}, Lcom/a/b/d/qy;->b(Ljava/lang/Object;)I

    move-result v0

    .line 3446
    invoke-virtual {p0, v0}, Lcom/a/b/d/qy;->a(I)Lcom/a/b/d/sa;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Lcom/a/b/d/sa;->a(Ljava/lang/Object;I)Lcom/a/b/d/rz;

    move-result-object v0

    goto :goto_3
.end method

.method private a(Ljava/lang/Object;ILcom/a/b/d/rz;)Lcom/a/b/d/rz;
    .registers 5
    .param p3    # Lcom/a/b/d/rz;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/a/b/a/d;
    .end annotation

    .prologue
    .line 1827
    invoke-virtual {p0, p2}, Lcom/a/b/d/qy;->a(I)Lcom/a/b/d/sa;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/a/b/d/sa;->a(Ljava/lang/Object;ILcom/a/b/d/rz;)Lcom/a/b/d/rz;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/a/b/d/rz;Ljava/lang/Object;)Lcom/a/b/d/sr;
    .registers 5
    .annotation build Lcom/a/b/a/d;
    .end annotation

    .prologue
    .line 1846
    invoke-interface {p1}, Lcom/a/b/d/rz;->c()I

    move-result v0

    .line 1847
    iget-object v1, p0, Lcom/a/b/d/qy;->p:Lcom/a/b/d/sh;

    invoke-virtual {p0, v0}, Lcom/a/b/d/qy;->a(I)Lcom/a/b/d/sa;

    move-result-object v0

    invoke-virtual {v1, v0, p1, p2}, Lcom/a/b/d/sh;->a(Lcom/a/b/d/sa;Lcom/a/b/d/rz;Ljava/lang/Object;)Lcom/a/b/d/sr;

    move-result-object v0

    return-object v0
.end method

.method static a(Lcom/a/b/d/rz;Lcom/a/b/d/rz;)V
    .registers 2

    .prologue
    .line 1929
    invoke-interface {p0, p1}, Lcom/a/b/d/rz;->a(Lcom/a/b/d/rz;)V

    .line 1930
    invoke-interface {p1, p0}, Lcom/a/b/d/rz;->b(Lcom/a/b/d/rz;)V

    .line 1931
    return-void
.end method

.method private a(Lcom/a/b/d/sr;)V
    .registers 5

    .prologue
    .line 1856
    invoke-interface {p1}, Lcom/a/b/d/sr;->a()Lcom/a/b/d/rz;

    move-result-object v0

    .line 1857
    invoke-interface {v0}, Lcom/a/b/d/rz;->c()I

    move-result v1

    .line 1858
    invoke-virtual {p0, v1}, Lcom/a/b/d/qy;->a(I)Lcom/a/b/d/sa;

    move-result-object v2

    invoke-interface {v0}, Lcom/a/b/d/rz;->d()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v2, v0, v1, p1}, Lcom/a/b/d/sa;->a(Ljava/lang/Object;ILcom/a/b/d/sr;)Z

    .line 1859
    return-void
.end method

.method static a(Lcom/a/b/d/rz;J)Z
    .registers 8

    .prologue
    .line 1924
    invoke-interface {p0}, Lcom/a/b/d/rz;->e()J

    move-result-wide v0

    sub-long v0, p1, v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method private static b(I)I
    .registers 4

    .prologue
    .line 1813
    shl-int/lit8 v0, p0, 0xf

    xor-int/lit16 v0, v0, -0x3283

    add-int/2addr v0, p0

    .line 1814
    ushr-int/lit8 v1, v0, 0xa

    xor-int/2addr v0, v1

    .line 1815
    shl-int/lit8 v1, v0, 0x3

    add-int/2addr v0, v1

    .line 1816
    ushr-int/lit8 v1, v0, 0x6

    xor-int/2addr v0, v1

    .line 1817
    shl-int/lit8 v1, v0, 0x2

    shl-int/lit8 v2, v0, 0xe

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 1818
    ushr-int/lit8 v1, v0, 0x10

    xor-int/2addr v0, v1

    return v0
.end method

.method static b(Lcom/a/b/d/rz;)V
    .registers 2

    .prologue
    .line 7865
    sget-object v0, Lcom/a/b/d/ry;->a:Lcom/a/b/d/ry;

    .line 1936
    invoke-interface {p0, v0}, Lcom/a/b/d/rz;->a(Lcom/a/b/d/rz;)V

    .line 1937
    invoke-interface {p0, v0}, Lcom/a/b/d/rz;->b(Lcom/a/b/d/rz;)V

    .line 1938
    return-void
.end method

.method static b(Lcom/a/b/d/rz;Lcom/a/b/d/rz;)V
    .registers 2

    .prologue
    .line 1961
    invoke-interface {p0, p1}, Lcom/a/b/d/rz;->c(Lcom/a/b/d/rz;)V

    .line 1962
    invoke-interface {p1, p0}, Lcom/a/b/d/rz;->d(Lcom/a/b/d/rz;)V

    .line 1963
    return-void
.end method

.method private c(Lcom/a/b/d/rz;Lcom/a/b/d/rz;)Lcom/a/b/d/rz;
    .registers 4
    .annotation build Lcom/a/b/a/d;
    .end annotation

    .prologue
    .line 1836
    invoke-interface {p1}, Lcom/a/b/d/rz;->c()I

    move-result v0

    .line 1837
    invoke-virtual {p0, v0}, Lcom/a/b/d/qy;->a(I)Lcom/a/b/d/sa;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/a/b/d/sa;->a(Lcom/a/b/d/rz;Lcom/a/b/d/rz;)Lcom/a/b/d/rz;

    move-result-object v0

    return-object v0
.end method

.method static c(Lcom/a/b/d/rz;)V
    .registers 2

    .prologue
    .line 8865
    sget-object v0, Lcom/a/b/d/ry;->a:Lcom/a/b/d/ry;

    .line 1968
    invoke-interface {p0, v0}, Lcom/a/b/d/rz;->c(Lcom/a/b/d/rz;)V

    .line 1969
    invoke-interface {p0, v0}, Lcom/a/b/d/rz;->d(Lcom/a/b/d/rz;)V

    .line 1970
    return-void
.end method

.method private static c(I)[Lcom/a/b/d/sa;
    .registers 2

    .prologue
    .line 1974
    new-array v0, p0, [Lcom/a/b/d/sa;

    return-object v0
.end method

.method private d(Lcom/a/b/d/rz;)V
    .registers 4

    .prologue
    .line 1862
    invoke-interface {p1}, Lcom/a/b/d/rz;->c()I

    move-result v0

    .line 1863
    invoke-virtual {p0, v0}, Lcom/a/b/d/qy;->a(I)Lcom/a/b/d/sa;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Lcom/a/b/d/sa;->a(Lcom/a/b/d/rz;I)Z

    .line 1864
    return-void
.end method

.method private e(Lcom/a/b/d/rz;)Z
    .registers 3
    .annotation build Lcom/a/b/a/d;
    .end annotation

    .prologue
    .line 1872
    invoke-interface {p1}, Lcom/a/b/d/rz;->c()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/a/b/d/qy;->a(I)Lcom/a/b/d/sa;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/a/b/d/sa;->c(Lcom/a/b/d/rz;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_10

    const/4 v0, 0x1

    :goto_f
    return v0

    :cond_10
    const/4 v0, 0x0

    goto :goto_f
.end method

.method private f(Lcom/a/b/d/rz;)Ljava/lang/Object;
    .registers 5

    .prologue
    const/4 v0, 0x0

    .line 1896
    invoke-interface {p1}, Lcom/a/b/d/rz;->d()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_8

    .line 1907
    :cond_7
    :goto_7
    return-object v0

    .line 1899
    :cond_8
    invoke-interface {p1}, Lcom/a/b/d/rz;->a()Lcom/a/b/d/sr;

    move-result-object v1

    invoke-interface {v1}, Lcom/a/b/d/sr;->get()Ljava/lang/Object;

    move-result-object v1

    .line 1900
    if-eqz v1, :cond_7

    .line 1904
    invoke-virtual {p0}, Lcom/a/b/d/qy;->c()Z

    move-result v2

    if-eqz v2, :cond_1e

    invoke-virtual {p0, p1}, Lcom/a/b/d/qy;->a(Lcom/a/b/d/rz;)Z

    move-result v2

    if-nez v2, :cond_7

    :cond_1e
    move-object v0, v1

    .line 1907
    goto :goto_7
.end method

.method static g()Lcom/a/b/d/sr;
    .registers 1

    .prologue
    .line 614
    sget-object v0, Lcom/a/b/d/qy;->x:Lcom/a/b/d/sr;

    return-object v0
.end method

.method static h()Lcom/a/b/d/rz;
    .registers 1

    .prologue
    .line 865
    sget-object v0, Lcom/a/b/d/ry;->a:Lcom/a/b/d/ry;

    return-object v0
.end method

.method static i()Ljava/util/Queue;
    .registers 1

    .prologue
    .line 900
    sget-object v0, Lcom/a/b/d/qy;->y:Ljava/util/Queue;

    return-object v0
.end method

.method private j()Z
    .registers 5

    .prologue
    .line 274
    iget-wide v0, p0, Lcom/a/b/d/qy;->s:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method private k()V
    .registers 2

    .prologue
    .line 1949
    :cond_0
    iget-object v0, p0, Lcom/a/b/d/qy;->t:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/qx;

    if-nez v0, :cond_0

    .line 1956
    return-void
.end method


# virtual methods
.method a(I)Lcom/a/b/d/sa;
    .registers 5

    .prologue
    .line 1883
    iget-object v0, p0, Lcom/a/b/d/qy;->k:[Lcom/a/b/d/sa;

    iget v1, p0, Lcom/a/b/d/qy;->j:I

    ushr-int v1, p1, v1

    iget v2, p0, Lcom/a/b/d/qy;->i:I

    and-int/2addr v1, v2

    aget-object v0, v0, v1

    return-object v0
.end method

.method a(II)Lcom/a/b/d/sa;
    .registers 4

    .prologue
    .line 1887
    new-instance v0, Lcom/a/b/d/sa;

    invoke-direct {v0, p0, p1, p2}, Lcom/a/b/d/sa;-><init>(Lcom/a/b/d/qy;II)V

    return-object v0
.end method

.method a()Ljava/lang/Object;
    .registers 15

    .prologue
    .line 3887
    new-instance v1, Lcom/a/b/d/sb;

    iget-object v2, p0, Lcom/a/b/d/qy;->o:Lcom/a/b/d/sh;

    iget-object v3, p0, Lcom/a/b/d/qy;->p:Lcom/a/b/d/sh;

    iget-object v4, p0, Lcom/a/b/d/qy;->m:Lcom/a/b/b/au;

    iget-object v5, p0, Lcom/a/b/d/qy;->n:Lcom/a/b/b/au;

    iget-wide v6, p0, Lcom/a/b/d/qy;->s:J

    iget-wide v8, p0, Lcom/a/b/d/qy;->r:J

    iget v10, p0, Lcom/a/b/d/qy;->q:I

    iget v11, p0, Lcom/a/b/d/qy;->l:I

    iget-object v12, p0, Lcom/a/b/d/qy;->u:Lcom/a/b/d/qw;

    move-object v13, p0

    invoke-direct/range {v1 .. v13}, Lcom/a/b/d/sb;-><init>(Lcom/a/b/d/sh;Lcom/a/b/d/sh;Lcom/a/b/b/au;Lcom/a/b/b/au;JJIILcom/a/b/d/qw;Ljava/util/concurrent/ConcurrentMap;)V

    return-object v1
.end method

.method final a(Lcom/a/b/d/rz;)Z
    .registers 4

    .prologue
    .line 1916
    iget-object v0, p0, Lcom/a/b/d/qy;->w:Lcom/a/b/b/ej;

    invoke-virtual {v0}, Lcom/a/b/b/ej;->a()J

    move-result-wide v0

    invoke-static {p1, v0, v1}, Lcom/a/b/d/qy;->a(Lcom/a/b/d/rz;J)Z

    move-result v0

    return v0
.end method

.method final b(Ljava/lang/Object;)I
    .registers 5

    .prologue
    .line 1851
    iget-object v0, p0, Lcom/a/b/d/qy;->m:Lcom/a/b/b/au;

    invoke-virtual {v0, p1}, Lcom/a/b/b/au;->a(Ljava/lang/Object;)I

    move-result v0

    .line 7813
    shl-int/lit8 v1, v0, 0xf

    xor-int/lit16 v1, v1, -0x3283

    add-int/2addr v0, v1

    .line 7814
    ushr-int/lit8 v1, v0, 0xa

    xor-int/2addr v0, v1

    .line 7815
    shl-int/lit8 v1, v0, 0x3

    add-int/2addr v0, v1

    .line 7816
    ushr-int/lit8 v1, v0, 0x6

    xor-int/2addr v0, v1

    .line 7817
    shl-int/lit8 v1, v0, 0x2

    shl-int/lit8 v2, v0, 0xe

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 7818
    ushr-int/lit8 v1, v0, 0x10

    xor-int/2addr v0, v1

    .line 1852
    return v0
.end method

.method final b()Z
    .registers 3

    .prologue
    .line 266
    iget v0, p0, Lcom/a/b/d/qy;->q:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_7

    const/4 v0, 0x1

    :goto_6
    return v0

    :cond_7
    const/4 v0, 0x0

    goto :goto_6
.end method

.method final c()Z
    .registers 7

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 270
    .line 7274
    iget-wide v2, p0, Lcom/a/b/d/qy;->s:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_15

    move v2, v1

    .line 270
    :goto_b
    if-nez v2, :cond_13

    invoke-virtual {p0}, Lcom/a/b/d/qy;->d()Z

    move-result v2

    if-eqz v2, :cond_14

    :cond_13
    move v0, v1

    :cond_14
    return v0

    :cond_15
    move v2, v0

    .line 7274
    goto :goto_b
.end method

.method public clear()V
    .registers 10

    .prologue
    const/4 v1, 0x0

    .line 3559
    iget-object v4, p0, Lcom/a/b/d/qy;->k:[Lcom/a/b/d/sa;

    array-length v5, v4

    move v3, v1

    :goto_5
    if-ge v3, v5, :cond_9b

    aget-object v6, v4, v3

    .line 9819
    iget v0, v6, Lcom/a/b/d/sa;->b:I

    if-eqz v0, :cond_8e

    .line 9820
    invoke-virtual {v6}, Lcom/a/b/d/sa;->lock()V

    .line 9822
    :try_start_10
    iget-object v7, v6, Lcom/a/b/d/sa;->e:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 9823
    iget-object v0, v6, Lcom/a/b/d/sa;->a:Lcom/a/b/d/qy;

    iget-object v0, v0, Lcom/a/b/d/qy;->t:Ljava/util/Queue;

    sget-object v2, Lcom/a/b/d/qy;->y:Ljava/util/Queue;

    if-eq v0, v2, :cond_41

    move v2, v1

    .line 9824
    :goto_1b
    invoke-virtual {v7}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    if-ge v2, v0, :cond_41

    .line 9825
    invoke-virtual {v7, v2}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/rz;

    :goto_27
    if-eqz v0, :cond_3d

    .line 9827
    invoke-interface {v0}, Lcom/a/b/d/rz;->a()Lcom/a/b/d/sr;

    move-result-object v8

    invoke-interface {v8}, Lcom/a/b/d/sr;->b()Z

    move-result v8

    if-nez v8, :cond_38

    .line 9828
    sget-object v8, Lcom/a/b/d/qq;->a:Lcom/a/b/d/qq;

    invoke-virtual {v6, v0, v8}, Lcom/a/b/d/sa;->a(Lcom/a/b/d/rz;Lcom/a/b/d/qq;)V

    .line 9825
    :cond_38
    invoke-interface {v0}, Lcom/a/b/d/rz;->b()Lcom/a/b/d/rz;

    move-result-object v0

    goto :goto_27

    .line 9824
    :cond_3d
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1b

    :cond_41
    move v0, v1

    .line 9833
    :goto_42
    invoke-virtual {v7}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v2

    if-ge v0, v2, :cond_4f

    .line 9834
    const/4 v2, 0x0

    invoke-virtual {v7, v0, v2}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    .line 9833
    add-int/lit8 v0, v0, 0x1

    goto :goto_42

    .line 10226
    :cond_4f
    iget-object v0, v6, Lcom/a/b/d/sa;->a:Lcom/a/b/d/qy;

    invoke-virtual {v0}, Lcom/a/b/d/qy;->e()Z

    move-result v0

    if-eqz v0, :cond_5f

    .line 10235
    :cond_57
    iget-object v0, v6, Lcom/a/b/d/sa;->g:Ljava/lang/ref/ReferenceQueue;

    invoke-virtual {v0}, Ljava/lang/ref/ReferenceQueue;->poll()Ljava/lang/ref/Reference;

    move-result-object v0

    if-nez v0, :cond_57

    .line 10229
    :cond_5f
    iget-object v0, v6, Lcom/a/b/d/sa;->a:Lcom/a/b/d/qy;

    invoke-virtual {v0}, Lcom/a/b/d/qy;->f()Z

    move-result v0

    if-eqz v0, :cond_6f

    .line 10239
    :cond_67
    iget-object v0, v6, Lcom/a/b/d/sa;->h:Ljava/lang/ref/ReferenceQueue;

    invoke-virtual {v0}, Ljava/lang/ref/ReferenceQueue;->poll()Ljava/lang/ref/Reference;

    move-result-object v0

    if-nez v0, :cond_67

    .line 9837
    :cond_6f
    iget-object v0, v6, Lcom/a/b/d/sa;->k:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    .line 9838
    iget-object v0, v6, Lcom/a/b/d/sa;->l:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    .line 9839
    iget-object v0, v6, Lcom/a/b/d/sa;->j:Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 9841
    iget v0, v6, Lcom/a/b/d/sa;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, v6, Lcom/a/b/d/sa;->c:I

    .line 9842
    const/4 v0, 0x0

    iput v0, v6, Lcom/a/b/d/sa;->b:I
    :try_end_88
    .catchall {:try_start_10 .. :try_end_88} :catchall_93

    .line 9844
    invoke-virtual {v6}, Lcom/a/b/d/sa;->unlock()V

    .line 11069
    invoke-virtual {v6}, Lcom/a/b/d/sa;->d()V

    .line 3559
    :cond_8e
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto/16 :goto_5

    .line 9844
    :catchall_93
    move-exception v0

    invoke-virtual {v6}, Lcom/a/b/d/sa;->unlock()V

    .line 12069
    invoke-virtual {v6}, Lcom/a/b/d/sa;->d()V

    .line 9845
    throw v0

    .line 3562
    :cond_9b
    return-void
.end method

.method public containsKey(Ljava/lang/Object;)Z
    .registers 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 3451
    if-nez p1, :cond_4

    .line 3452
    const/4 v0, 0x0

    .line 3455
    :goto_3
    return v0

    .line 3454
    :cond_4
    invoke-virtual {p0, p1}, Lcom/a/b/d/qy;->b(Ljava/lang/Object;)I

    move-result v0

    .line 3455
    invoke-virtual {p0, v0}, Lcom/a/b/d/qy;->a(I)Lcom/a/b/d/sa;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Lcom/a/b/d/sa;->c(Ljava/lang/Object;I)Z

    move-result v0

    goto :goto_3
.end method

.method public containsValue(Ljava/lang/Object;)Z
    .registers 16
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 3460
    if-nez p1, :cond_4

    .line 3461
    const/4 v0, 0x0

    .line 3494
    :goto_3
    return v0

    .line 3469
    :cond_4
    iget-object v7, p0, Lcom/a/b/d/qy;->k:[Lcom/a/b/d/sa;

    .line 3470
    const-wide/16 v4, -0x1

    .line 3471
    const/4 v0, 0x0

    move v6, v0

    move-wide v8, v4

    :goto_b
    const/4 v0, 0x3

    if-ge v6, v0, :cond_56

    .line 3472
    const-wide/16 v2, 0x0

    .line 3473
    array-length v10, v7

    const/4 v0, 0x0

    move-wide v4, v2

    move v2, v0

    :goto_14
    if-ge v2, v10, :cond_4d

    aget-object v3, v7, v2

    .line 3476
    iget v0, v3, Lcom/a/b/d/sa;->b:I

    .line 3478
    iget-object v11, v3, Lcom/a/b/d/sa;->e:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 3479
    const/4 v0, 0x0

    move v1, v0

    :goto_1e
    invoke-virtual {v11}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    if-ge v1, v0, :cond_45

    .line 3480
    invoke-virtual {v11, v1}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/rz;

    :goto_2a
    if-eqz v0, :cond_41

    .line 3481
    invoke-virtual {v3, v0}, Lcom/a/b/d/sa;->c(Lcom/a/b/d/rz;)Ljava/lang/Object;

    move-result-object v12

    .line 3482
    if-eqz v12, :cond_3c

    iget-object v13, p0, Lcom/a/b/d/qy;->n:Lcom/a/b/b/au;

    invoke-virtual {v13, p1, v12}, Lcom/a/b/b/au;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_3c

    .line 3483
    const/4 v0, 0x1

    goto :goto_3

    .line 3480
    :cond_3c
    invoke-interface {v0}, Lcom/a/b/d/rz;->b()Lcom/a/b/d/rz;

    move-result-object v0

    goto :goto_2a

    .line 3479
    :cond_41
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1e

    .line 3487
    :cond_45
    iget v0, v3, Lcom/a/b/d/sa;->c:I

    int-to-long v0, v0

    add-long/2addr v4, v0

    .line 3473
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_14

    .line 3489
    :cond_4d
    cmp-long v0, v4, v8

    if-eqz v0, :cond_56

    .line 3471
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    move-wide v8, v4

    goto :goto_b

    .line 3494
    :cond_56
    const/4 v0, 0x0

    goto :goto_3
.end method

.method final d()Z
    .registers 5

    .prologue
    .line 278
    iget-wide v0, p0, Lcom/a/b/d/qy;->r:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method final e()Z
    .registers 3

    .prologue
    .line 282
    iget-object v0, p0, Lcom/a/b/d/qy;->o:Lcom/a/b/d/sh;

    sget-object v1, Lcom/a/b/d/sh;->a:Lcom/a/b/d/sh;

    if-eq v0, v1, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public entrySet()Ljava/util/Set;
    .registers 2

    .prologue
    .line 3584
    iget-object v0, p0, Lcom/a/b/d/qy;->B:Ljava/util/Set;

    .line 3585
    if-eqz v0, :cond_5

    :goto_4
    return-object v0

    :cond_5
    new-instance v0, Lcom/a/b/d/ro;

    invoke-direct {v0, p0}, Lcom/a/b/d/ro;-><init>(Lcom/a/b/d/qy;)V

    iput-object v0, p0, Lcom/a/b/d/qy;->B:Ljava/util/Set;

    goto :goto_4
.end method

.method final f()Z
    .registers 3

    .prologue
    .line 286
    iget-object v0, p0, Lcom/a/b/d/qy;->p:Lcom/a/b/d/sh;

    sget-object v1, Lcom/a/b/d/sh;->a:Lcom/a/b/d/sh;

    if-eq v0, v1, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public get(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 3430
    if-nez p1, :cond_4

    .line 3431
    const/4 v0, 0x0

    .line 3434
    :goto_3
    return-object v0

    .line 3433
    :cond_4
    invoke-virtual {p0, p1}, Lcom/a/b/d/qy;->b(Ljava/lang/Object;)I

    move-result v0

    .line 3434
    invoke-virtual {p0, v0}, Lcom/a/b/d/qy;->a(I)Lcom/a/b/d/sa;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Lcom/a/b/d/sa;->b(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_3
.end method

.method public isEmpty()Z
    .registers 11

    .prologue
    const-wide/16 v4, 0x0

    const/4 v1, 0x0

    .line 3395
    .line 3396
    iget-object v6, p0, Lcom/a/b/d/qy;->k:[Lcom/a/b/d/sa;

    move v0, v1

    move-wide v2, v4

    .line 3397
    :goto_7
    array-length v7, v6

    if-ge v0, v7, :cond_1a

    .line 3398
    aget-object v7, v6, v0

    iget v7, v7, Lcom/a/b/d/sa;->b:I

    if-eqz v7, :cond_11

    .line 3415
    :cond_10
    :goto_10
    return v1

    .line 3401
    :cond_11
    aget-object v7, v6, v0

    iget v7, v7, Lcom/a/b/d/sa;->c:I

    int-to-long v8, v7

    add-long/2addr v2, v8

    .line 3397
    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    .line 3404
    :cond_1a
    cmp-long v0, v2, v4

    if-eqz v0, :cond_35

    move v0, v1

    .line 3405
    :goto_1f
    array-length v7, v6

    if-ge v0, v7, :cond_31

    .line 3406
    aget-object v7, v6, v0

    iget v7, v7, Lcom/a/b/d/sa;->b:I

    if-nez v7, :cond_10

    .line 3409
    aget-object v7, v6, v0

    iget v7, v7, Lcom/a/b/d/sa;->c:I

    int-to-long v8, v7

    sub-long/2addr v2, v8

    .line 3405
    add-int/lit8 v0, v0, 0x1

    goto :goto_1f

    .line 3411
    :cond_31
    cmp-long v0, v2, v4

    if-nez v0, :cond_10

    .line 3415
    :cond_35
    const/4 v1, 0x1

    goto :goto_10
.end method

.method public keySet()Ljava/util/Set;
    .registers 2

    .prologue
    .line 3568
    iget-object v0, p0, Lcom/a/b/d/qy;->z:Ljava/util/Set;

    .line 3569
    if-eqz v0, :cond_5

    :goto_4
    return-object v0

    :cond_5
    new-instance v0, Lcom/a/b/d/rx;

    invoke-direct {v0, p0}, Lcom/a/b/d/rx;-><init>(Lcom/a/b/d/qy;)V

    iput-object v0, p0, Lcom/a/b/d/qy;->z:Ljava/util/Set;

    goto :goto_4
.end method

.method public put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 6

    .prologue
    .line 3499
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 3500
    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 3501
    invoke-virtual {p0, p1}, Lcom/a/b/d/qy;->b(Ljava/lang/Object;)I

    move-result v0

    .line 3502
    invoke-virtual {p0, v0}, Lcom/a/b/d/qy;->a(I)Lcom/a/b/d/sa;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v0, p2, v2}, Lcom/a/b/d/sa;->a(Ljava/lang/Object;ILjava/lang/Object;Z)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public putAll(Ljava/util/Map;)V
    .registers 5

    .prologue
    .line 3515
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_20

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 3516
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v2, v0}, Lcom/a/b/d/qy;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_8

    .line 3518
    :cond_20
    return-void
.end method

.method public putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 6

    .prologue
    .line 3507
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 3508
    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 3509
    invoke-virtual {p0, p1}, Lcom/a/b/d/qy;->b(Ljava/lang/Object;)I

    move-result v0

    .line 3510
    invoke-virtual {p0, v0}, Lcom/a/b/d/qy;->a(I)Lcom/a/b/d/sa;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, p1, v0, p2, v2}, Lcom/a/b/d/sa;->a(Ljava/lang/Object;ILjava/lang/Object;Z)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public remove(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 3522
    if-nez p1, :cond_4

    .line 3523
    const/4 v0, 0x0

    .line 3526
    :goto_3
    return-object v0

    .line 3525
    :cond_4
    invoke-virtual {p0, p1}, Lcom/a/b/d/qy;->b(Ljava/lang/Object;)I

    move-result v0

    .line 3526
    invoke-virtual {p0, v0}, Lcom/a/b/d/qy;->a(I)Lcom/a/b/d/sa;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Lcom/a/b/d/sa;->d(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_3
.end method

.method public remove(Ljava/lang/Object;Ljava/lang/Object;)Z
    .registers 5
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 3531
    if-eqz p1, :cond_4

    if-nez p2, :cond_6

    .line 3532
    :cond_4
    const/4 v0, 0x0

    .line 3535
    :goto_5
    return v0

    .line 3534
    :cond_6
    invoke-virtual {p0, p1}, Lcom/a/b/d/qy;->b(Ljava/lang/Object;)I

    move-result v0

    .line 3535
    invoke-virtual {p0, v0}, Lcom/a/b/d/qy;->a(I)Lcom/a/b/d/sa;

    move-result-object v1

    invoke-virtual {v1, p1, v0, p2}, Lcom/a/b/d/sa;->b(Ljava/lang/Object;ILjava/lang/Object;)Z

    move-result v0

    goto :goto_5
.end method

.method public replace(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 5

    .prologue
    .line 3551
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 3552
    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 3553
    invoke-virtual {p0, p1}, Lcom/a/b/d/qy;->b(Ljava/lang/Object;)I

    move-result v0

    .line 3554
    invoke-virtual {p0, v0}, Lcom/a/b/d/qy;->a(I)Lcom/a/b/d/sa;

    move-result-object v1

    invoke-virtual {v1, p1, v0, p2}, Lcom/a/b/d/sa;->a(Ljava/lang/Object;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public replace(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z
    .registers 6
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 3540
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 3541
    invoke-static {p3}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 3542
    if-nez p2, :cond_a

    .line 3543
    const/4 v0, 0x0

    .line 3546
    :goto_9
    return v0

    .line 3545
    :cond_a
    invoke-virtual {p0, p1}, Lcom/a/b/d/qy;->b(Ljava/lang/Object;)I

    move-result v0

    .line 3546
    invoke-virtual {p0, v0}, Lcom/a/b/d/qy;->a(I)Lcom/a/b/d/sa;

    move-result-object v1

    invoke-virtual {v1, p1, v0, p2, p3}, Lcom/a/b/d/sa;->a(Ljava/lang/Object;ILjava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_9
.end method

.method public size()I
    .registers 7

    .prologue
    .line 3420
    iget-object v1, p0, Lcom/a/b/d/qy;->k:[Lcom/a/b/d/sa;

    .line 3421
    const-wide/16 v2, 0x0

    .line 3422
    const/4 v0, 0x0

    :goto_5
    array-length v4, v1

    if-ge v0, v4, :cond_11

    .line 3423
    aget-object v4, v1, v0

    iget v4, v4, Lcom/a/b/d/sa;->b:I

    int-to-long v4, v4

    add-long/2addr v2, v4

    .line 3422
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 3425
    :cond_11
    invoke-static {v2, v3}, Lcom/a/b/l/q;->b(J)I

    move-result v0

    return v0
.end method

.method public values()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 3576
    iget-object v0, p0, Lcom/a/b/d/qy;->A:Ljava/util/Collection;

    .line 3577
    if-eqz v0, :cond_5

    :goto_4
    return-object v0

    :cond_5
    new-instance v0, Lcom/a/b/d/ss;

    invoke-direct {v0, p0}, Lcom/a/b/d/ss;-><init>(Lcom/a/b/d/qy;)V

    iput-object v0, p0, Lcom/a/b/d/qy;->A:Ljava/util/Collection;

    goto :goto_4
.end method
