.class final Lcom/a/b/d/afw;
.super Lcom/a/b/d/afm;
.source "SourceFile"


# instance fields
.field final synthetic b:Lcom/a/b/d/afm;

.field private final c:Lcom/a/b/d/yl;


# direct methods
.method constructor <init>(Lcom/a/b/d/afm;Lcom/a/b/d/yl;)V
    .registers 7

    .prologue
    const/4 v3, 0x0

    .line 791
    iput-object p1, p0, Lcom/a/b/d/afw;->b:Lcom/a/b/d/afm;

    .line 792
    new-instance v0, Lcom/a/b/d/afx;

    invoke-static {}, Lcom/a/b/d/yl;->c()Lcom/a/b/d/yl;

    move-result-object v1

    iget-object v2, p1, Lcom/a/b/d/afm;->a:Ljava/util/NavigableMap;

    invoke-direct {v0, v1, p2, v2, v3}, Lcom/a/b/d/afx;-><init>(Lcom/a/b/d/yl;Lcom/a/b/d/yl;Ljava/util/NavigableMap;B)V

    invoke-direct {p0, v0, v3}, Lcom/a/b/d/afm;-><init>(Ljava/util/NavigableMap;B)V

    .line 794
    iput-object p2, p0, Lcom/a/b/d/afw;->c:Lcom/a/b/d/yl;

    .line 795
    return-void
.end method


# virtual methods
.method public final a(Lcom/a/b/d/yl;)V
    .registers 7

    .prologue
    .line 818
    iget-object v0, p0, Lcom/a/b/d/afw;->c:Lcom/a/b/d/yl;

    invoke-virtual {v0, p1}, Lcom/a/b/d/yl;->a(Lcom/a/b/d/yl;)Z

    move-result v0

    const-string v1, "Cannot add range %s to subRangeSet(%s)"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/a/b/d/afw;->c:Lcom/a/b/d/yl;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 820
    invoke-super {p0, p1}, Lcom/a/b/d/afm;->a(Lcom/a/b/d/yl;)V

    .line 821
    return-void
.end method

.method public final a(Ljava/lang/Comparable;)Z
    .registers 3

    .prologue
    .line 832
    iget-object v0, p0, Lcom/a/b/d/afw;->c:Lcom/a/b/d/yl;

    invoke-virtual {v0, p1}, Lcom/a/b/d/yl;->c(Ljava/lang/Comparable;)Z

    move-result v0

    if-eqz v0, :cond_12

    iget-object v0, p0, Lcom/a/b/d/afw;->b:Lcom/a/b/d/afm;

    invoke-virtual {v0, p1}, Lcom/a/b/d/afm;->a(Ljava/lang/Comparable;)Z

    move-result v0

    if-eqz v0, :cond_12

    const/4 v0, 0x1

    :goto_11
    return v0

    :cond_12
    const/4 v0, 0x0

    goto :goto_11
.end method

.method public final b(Ljava/lang/Comparable;)Lcom/a/b/d/yl;
    .registers 4
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 809
    iget-object v1, p0, Lcom/a/b/d/afw;->c:Lcom/a/b/d/yl;

    invoke-virtual {v1, p1}, Lcom/a/b/d/yl;->c(Ljava/lang/Comparable;)Z

    move-result v1

    if-nez v1, :cond_a

    .line 813
    :cond_9
    :goto_9
    return-object v0

    .line 812
    :cond_a
    iget-object v1, p0, Lcom/a/b/d/afw;->b:Lcom/a/b/d/afm;

    invoke-virtual {v1, p1}, Lcom/a/b/d/afm;->b(Ljava/lang/Comparable;)Lcom/a/b/d/yl;

    move-result-object v1

    .line 813
    if-eqz v1, :cond_9

    iget-object v0, p0, Lcom/a/b/d/afw;->c:Lcom/a/b/d/yl;

    invoke-virtual {v1, v0}, Lcom/a/b/d/yl;->c(Lcom/a/b/d/yl;)Lcom/a/b/d/yl;

    move-result-object v0

    goto :goto_9
.end method

.method public final b()V
    .registers 3

    .prologue
    .line 837
    iget-object v0, p0, Lcom/a/b/d/afw;->b:Lcom/a/b/d/afm;

    iget-object v1, p0, Lcom/a/b/d/afw;->c:Lcom/a/b/d/yl;

    invoke-virtual {v0, v1}, Lcom/a/b/d/afm;->b(Lcom/a/b/d/yl;)V

    .line 838
    return-void
.end method

.method public final b(Lcom/a/b/d/yl;)V
    .registers 4

    .prologue
    .line 825
    iget-object v0, p0, Lcom/a/b/d/afw;->c:Lcom/a/b/d/yl;

    invoke-virtual {p1, v0}, Lcom/a/b/d/yl;->b(Lcom/a/b/d/yl;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 826
    iget-object v0, p0, Lcom/a/b/d/afw;->b:Lcom/a/b/d/afm;

    iget-object v1, p0, Lcom/a/b/d/afw;->c:Lcom/a/b/d/yl;

    invoke-virtual {p1, v1}, Lcom/a/b/d/yl;->c(Lcom/a/b/d/yl;)Lcom/a/b/d/yl;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/a/b/d/afm;->b(Lcom/a/b/d/yl;)V

    .line 828
    :cond_13
    return-void
.end method

.method public final c(Lcom/a/b/d/yl;)Z
    .registers 5

    .prologue
    const/4 v1, 0x0

    .line 799
    iget-object v0, p0, Lcom/a/b/d/afw;->c:Lcom/a/b/d/yl;

    invoke-virtual {v0}, Lcom/a/b/d/yl;->f()Z

    move-result v0

    if-nez v0, :cond_46

    iget-object v0, p0, Lcom/a/b/d/afw;->c:Lcom/a/b/d/yl;

    invoke-virtual {v0, p1}, Lcom/a/b/d/yl;->a(Lcom/a/b/d/yl;)Z

    move-result v0

    if-eqz v0, :cond_46

    .line 800
    iget-object v0, p0, Lcom/a/b/d/afw;->b:Lcom/a/b/d/afm;

    .line 1117
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1118
    iget-object v0, v0, Lcom/a/b/d/afm;->a:Ljava/util/NavigableMap;

    iget-object v2, p1, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    invoke-interface {v0, v2}, Ljava/util/NavigableMap;->floorEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v2

    .line 1119
    if-eqz v2, :cond_42

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/yl;

    invoke-virtual {v0, p1}, Lcom/a/b/d/yl;->a(Lcom/a/b/d/yl;)Z

    move-result v0

    if-eqz v0, :cond_42

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/yl;

    .line 801
    :goto_32
    if-eqz v0, :cond_44

    iget-object v2, p0, Lcom/a/b/d/afw;->c:Lcom/a/b/d/yl;

    invoke-virtual {v0, v2}, Lcom/a/b/d/yl;->c(Lcom/a/b/d/yl;)Lcom/a/b/d/yl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/yl;->f()Z

    move-result v0

    if-nez v0, :cond_44

    const/4 v0, 0x1

    .line 803
    :goto_41
    return v0

    .line 1119
    :cond_42
    const/4 v0, 0x0

    goto :goto_32

    :cond_44
    move v0, v1

    .line 801
    goto :goto_41

    :cond_46
    move v0, v1

    .line 803
    goto :goto_41
.end method

.method public final e(Lcom/a/b/d/yl;)Lcom/a/b/d/yr;
    .registers 4

    .prologue
    .line 842
    iget-object v0, p0, Lcom/a/b/d/afw;->c:Lcom/a/b/d/yl;

    invoke-virtual {p1, v0}, Lcom/a/b/d/yl;->a(Lcom/a/b/d/yl;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 847
    :goto_8
    return-object p0

    .line 844
    :cond_9
    iget-object v0, p0, Lcom/a/b/d/afw;->c:Lcom/a/b/d/yl;

    invoke-virtual {p1, v0}, Lcom/a/b/d/yl;->b(Lcom/a/b/d/yl;)Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 845
    new-instance v0, Lcom/a/b/d/afw;

    iget-object v1, p0, Lcom/a/b/d/afw;->c:Lcom/a/b/d/yl;

    invoke-virtual {v1, p1}, Lcom/a/b/d/yl;->c(Lcom/a/b/d/yl;)Lcom/a/b/d/yl;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/a/b/d/afw;-><init>(Lcom/a/b/d/afm;Lcom/a/b/d/yl;)V

    move-object p0, v0

    goto :goto_8

    .line 847
    :cond_1e
    invoke-static {}, Lcom/a/b/d/lf;->c()Lcom/a/b/d/lf;

    move-result-object p0

    goto :goto_8
.end method
