.class final Lcom/a/b/d/adk;
.super Lcom/a/b/d/add;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/d/xc;


# static fields
.field private static final c:J


# instance fields
.field transient a:Ljava/util/Set;

.field transient b:Ljava/util/Set;


# direct methods
.method constructor <init>(Lcom/a/b/d/xc;Ljava/lang/Object;)V
    .registers 4
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 425
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/a/b/d/add;-><init>(Ljava/util/Collection;Ljava/lang/Object;B)V

    .line 426
    return-void
.end method

.method private c()Lcom/a/b/d/xc;
    .registers 2

    .prologue
    .line 429
    invoke-super {p0}, Lcom/a/b/d/add;->b()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/xc;

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)I
    .registers 4

    .prologue
    .line 434
    iget-object v1, p0, Lcom/a/b/d/adk;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 1429
    :try_start_3
    invoke-super {p0}, Lcom/a/b/d/add;->b()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/xc;

    .line 435
    invoke-interface {v0, p1}, Lcom/a/b/d/xc;->a(Ljava/lang/Object;)I

    move-result v0

    monitor-exit v1

    return v0

    .line 436
    :catchall_f
    move-exception v0

    monitor-exit v1
    :try_end_11
    .catchall {:try_start_3 .. :try_end_11} :catchall_f

    throw v0
.end method

.method public final a(Ljava/lang/Object;I)I
    .registers 5

    .prologue
    .line 441
    iget-object v1, p0, Lcom/a/b/d/adk;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 2429
    :try_start_3
    invoke-super {p0}, Lcom/a/b/d/add;->b()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/xc;

    .line 442
    invoke-interface {v0, p1, p2}, Lcom/a/b/d/xc;->a(Ljava/lang/Object;I)I

    move-result v0

    monitor-exit v1

    return v0

    .line 443
    :catchall_f
    move-exception v0

    monitor-exit v1
    :try_end_11
    .catchall {:try_start_3 .. :try_end_11} :catchall_f

    throw v0
.end method

.method public final a()Ljava/util/Set;
    .registers 4

    .prologue
    .line 479
    iget-object v1, p0, Lcom/a/b/d/adk;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 480
    :try_start_3
    iget-object v0, p0, Lcom/a/b/d/adk;->b:Ljava/util/Set;

    if-nez v0, :cond_19

    .line 7429
    invoke-super {p0}, Lcom/a/b/d/add;->b()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/xc;

    .line 481
    invoke-interface {v0}, Lcom/a/b/d/xc;->a()Ljava/util/Set;

    move-result-object v0

    iget-object v2, p0, Lcom/a/b/d/adk;->h:Ljava/lang/Object;

    invoke-static {v0, v2}, Lcom/a/b/d/acu;->b(Ljava/util/Set;Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/adk;->b:Ljava/util/Set;

    .line 483
    :cond_19
    iget-object v0, p0, Lcom/a/b/d/adk;->b:Ljava/util/Set;

    monitor-exit v1

    return-object v0

    .line 484
    :catchall_1d
    move-exception v0

    monitor-exit v1
    :try_end_1f
    .catchall {:try_start_3 .. :try_end_1f} :catchall_1d

    throw v0
.end method

.method public final a(Ljava/lang/Object;II)Z
    .registers 6

    .prologue
    .line 462
    iget-object v1, p0, Lcom/a/b/d/adk;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 5429
    :try_start_3
    invoke-super {p0}, Lcom/a/b/d/add;->b()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/xc;

    .line 463
    invoke-interface {v0, p1, p2, p3}, Lcom/a/b/d/xc;->a(Ljava/lang/Object;II)Z

    move-result v0

    monitor-exit v1

    return v0

    .line 464
    :catchall_f
    move-exception v0

    monitor-exit v1
    :try_end_11
    .catchall {:try_start_3 .. :try_end_11} :catchall_f

    throw v0
.end method

.method public final b(Ljava/lang/Object;I)I
    .registers 5

    .prologue
    .line 448
    iget-object v1, p0, Lcom/a/b/d/adk;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 3429
    :try_start_3
    invoke-super {p0}, Lcom/a/b/d/add;->b()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/xc;

    .line 449
    invoke-interface {v0, p1, p2}, Lcom/a/b/d/xc;->b(Ljava/lang/Object;I)I

    move-result v0

    monitor-exit v1

    return v0

    .line 450
    :catchall_f
    move-exception v0

    monitor-exit v1
    :try_end_11
    .catchall {:try_start_3 .. :try_end_11} :catchall_f

    throw v0
.end method

.method final bridge synthetic b()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 419
    .line 10429
    invoke-super {p0}, Lcom/a/b/d/add;->b()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/xc;

    .line 419
    return-object v0
.end method

.method public final c(Ljava/lang/Object;I)I
    .registers 5

    .prologue
    .line 455
    iget-object v1, p0, Lcom/a/b/d/adk;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 4429
    :try_start_3
    invoke-super {p0}, Lcom/a/b/d/add;->b()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/xc;

    .line 456
    invoke-interface {v0, p1, p2}, Lcom/a/b/d/xc;->c(Ljava/lang/Object;I)I

    move-result v0

    monitor-exit v1

    return v0

    .line 457
    :catchall_f
    move-exception v0

    monitor-exit v1
    :try_end_11
    .catchall {:try_start_3 .. :try_end_11} :catchall_f

    throw v0
.end method

.method final synthetic d()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 419
    .line 11429
    invoke-super {p0}, Lcom/a/b/d/add;->b()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/xc;

    .line 419
    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .registers 4

    .prologue
    .line 488
    if-ne p1, p0, :cond_4

    .line 489
    const/4 v0, 0x1

    .line 492
    :goto_3
    return v0

    .line 491
    :cond_4
    iget-object v1, p0, Lcom/a/b/d/adk;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 8429
    :try_start_7
    invoke-super {p0}, Lcom/a/b/d/add;->b()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/xc;

    .line 492
    invoke-interface {v0, p1}, Lcom/a/b/d/xc;->equals(Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1

    goto :goto_3

    .line 493
    :catchall_13
    move-exception v0

    monitor-exit v1
    :try_end_15
    .catchall {:try_start_7 .. :try_end_15} :catchall_13

    throw v0
.end method

.method public final hashCode()I
    .registers 3

    .prologue
    .line 497
    iget-object v1, p0, Lcom/a/b/d/adk;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 9429
    :try_start_3
    invoke-super {p0}, Lcom/a/b/d/add;->b()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/xc;

    .line 498
    invoke-interface {v0}, Lcom/a/b/d/xc;->hashCode()I

    move-result v0

    monitor-exit v1

    return v0

    .line 499
    :catchall_f
    move-exception v0

    monitor-exit v1
    :try_end_11
    .catchall {:try_start_3 .. :try_end_11} :catchall_f

    throw v0
.end method

.method public final n_()Ljava/util/Set;
    .registers 4

    .prologue
    .line 469
    iget-object v1, p0, Lcom/a/b/d/adk;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 470
    :try_start_3
    iget-object v0, p0, Lcom/a/b/d/adk;->a:Ljava/util/Set;

    if-nez v0, :cond_19

    .line 6429
    invoke-super {p0}, Lcom/a/b/d/add;->b()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/xc;

    .line 471
    invoke-interface {v0}, Lcom/a/b/d/xc;->n_()Ljava/util/Set;

    move-result-object v0

    iget-object v2, p0, Lcom/a/b/d/adk;->h:Ljava/lang/Object;

    invoke-static {v0, v2}, Lcom/a/b/d/acu;->b(Ljava/util/Set;Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/adk;->a:Ljava/util/Set;

    .line 473
    :cond_19
    iget-object v0, p0, Lcom/a/b/d/adk;->a:Ljava/util/Set;

    monitor-exit v1

    return-object v0

    .line 474
    :catchall_1d
    move-exception v0

    monitor-exit v1
    :try_end_1f
    .catchall {:try_start_3 .. :try_end_1f} :catchall_1d

    throw v0
.end method
