.class final Lcom/a/b/d/fv;
.super Lcom/a/b/d/gp;
.source "SourceFile"


# instance fields
.field final a:Ljava/lang/Object;


# direct methods
.method constructor <init>(Ljava/lang/Object;)V
    .registers 2

    .prologue
    .line 138
    invoke-direct {p0}, Lcom/a/b/d/gp;-><init>()V

    .line 139
    iput-object p1, p0, Lcom/a/b/d/fv;->a:Ljava/lang/Object;

    .line 140
    return-void
.end method


# virtual methods
.method protected final a()Ljava/util/List;
    .registers 2

    .prologue
    .line 169
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final add(ILjava/lang/Object;)V
    .registers 7

    .prologue
    .line 156
    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/a/b/b/cn;->b(II)I

    .line 157
    new-instance v0, Ljava/lang/IllegalArgumentException;

    iget-object v1, p0, Lcom/a/b/d/fv;->a:Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x20

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Key does not satisfy predicate: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final add(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 144
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1}, Lcom/a/b/d/fv;->add(ILjava/lang/Object;)V

    .line 145
    const/4 v0, 0x1

    return v0
.end method

.method public final addAll(ILjava/util/Collection;)Z
    .registers 7

    .prologue
    .line 162
    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 163
    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/a/b/b/cn;->b(II)I

    .line 164
    new-instance v0, Ljava/lang/IllegalArgumentException;

    iget-object v1, p0, Lcom/a/b/d/fv;->a:Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x20

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Key does not satisfy predicate: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final addAll(Ljava/util/Collection;)Z
    .registers 3

    .prologue
    .line 150
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1}, Lcom/a/b/d/fv;->addAll(ILjava/util/Collection;)Z

    .line 151
    const/4 v0, 0x1

    return v0
.end method

.method protected final synthetic b()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 1169
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 135
    return-object v0
.end method

.method protected final synthetic k_()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 2169
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 135
    return-object v0
.end method
