.class public abstract enum Lcom/a/b/d/ce;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/b;
.end annotation


# static fields
.field public static final enum a:Lcom/a/b/d/ce;

.field public static final enum b:Lcom/a/b/d/ce;

.field private static final synthetic c:[Lcom/a/b/d/ce;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    .line 31
    new-instance v0, Lcom/a/b/d/cf;

    const-string v1, "OPEN"

    invoke-direct {v0, v1}, Lcom/a/b/d/cf;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/a/b/d/ce;->a:Lcom/a/b/d/ce;

    .line 40
    new-instance v0, Lcom/a/b/d/cg;

    const-string v1, "CLOSED"

    invoke-direct {v0, v1}, Lcom/a/b/d/cg;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/a/b/d/ce;->b:Lcom/a/b/d/ce;

    .line 26
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/a/b/d/ce;

    const/4 v1, 0x0

    sget-object v2, Lcom/a/b/d/ce;->a:Lcom/a/b/d/ce;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/a/b/d/ce;->b:Lcom/a/b/d/ce;

    aput-object v2, v0, v1

    sput-object v0, Lcom/a/b/d/ce;->c:[Lcom/a/b/d/ce;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;IB)V
    .registers 4

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Lcom/a/b/d/ce;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method static a(Z)Lcom/a/b/d/ce;
    .registers 2

    .prologue
    .line 51
    if-eqz p0, :cond_5

    sget-object v0, Lcom/a/b/d/ce;->b:Lcom/a/b/d/ce;

    :goto_4
    return-object v0

    :cond_5
    sget-object v0, Lcom/a/b/d/ce;->a:Lcom/a/b/d/ce;

    goto :goto_4
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/a/b/d/ce;
    .registers 2

    .prologue
    .line 26
    const-class v0, Lcom/a/b/d/ce;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/ce;

    return-object v0
.end method

.method public static values()[Lcom/a/b/d/ce;
    .registers 1

    .prologue
    .line 26
    sget-object v0, Lcom/a/b/d/ce;->c:[Lcom/a/b/d/ce;

    invoke-virtual {v0}, [Lcom/a/b/d/ce;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/a/b/d/ce;

    return-object v0
.end method


# virtual methods
.method abstract a()Lcom/a/b/d/ce;
.end method
