.class abstract Lcom/a/b/d/ir;
.super Lcom/a/b/d/jl;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/b;
    a = true
    b = true
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/a/b/d/jl;-><init>()V

    .line 62
    return-void
.end method

.method private static i()V
    .registers 2
    .annotation build Lcom/a/b/a/c;
        a = "serialization"
    .end annotation

    .prologue
    .line 77
    new-instance v0, Ljava/io/InvalidObjectException;

    const-string v1, "Use SerializedForm"

    invoke-direct {v0, v1}, Ljava/io/InvalidObjectException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method abstract b()Lcom/a/b/d/iz;
.end method

.method public contains(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 41
    invoke-virtual {p0}, Lcom/a/b/d/ir;->b()Lcom/a/b/d/iz;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/a/b/d/iz;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method final g()Ljava/lang/Object;
    .registers 3
    .annotation build Lcom/a/b/a/c;
        a = "serialization"
    .end annotation

    .prologue
    .line 82
    new-instance v0, Lcom/a/b/d/is;

    invoke-virtual {p0}, Lcom/a/b/d/ir;->b()Lcom/a/b/d/iz;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/a/b/d/is;-><init>(Lcom/a/b/d/iz;)V

    return-object v0
.end method

.method final h_()Z
    .registers 2

    .prologue
    .line 56
    invoke-virtual {p0}, Lcom/a/b/d/ir;->b()Lcom/a/b/d/iz;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/iz;->h_()Z

    move-result v0

    return v0
.end method

.method public isEmpty()Z
    .registers 2

    .prologue
    .line 51
    invoke-virtual {p0}, Lcom/a/b/d/ir;->b()Lcom/a/b/d/iz;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/iz;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public size()I
    .registers 2

    .prologue
    .line 46
    invoke-virtual {p0}, Lcom/a/b/d/ir;->b()Lcom/a/b/d/iz;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/iz;->size()I

    move-result v0

    return v0
.end method
