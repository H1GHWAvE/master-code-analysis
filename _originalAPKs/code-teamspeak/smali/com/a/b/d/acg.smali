.class final Lcom/a/b/d/acg;
.super Lcom/a/b/d/j;
.source "SourceFile"


# instance fields
.field final a:Ljava/util/Map;

.field final b:Ljava/util/Iterator;

.field c:Ljava/util/Iterator;

.field final synthetic d:Lcom/a/b/d/abx;


# direct methods
.method private constructor <init>(Lcom/a/b/d/abx;)V
    .registers 3

    .prologue
    .line 656
    iput-object p1, p0, Lcom/a/b/d/acg;->d:Lcom/a/b/d/abx;

    invoke-direct {p0}, Lcom/a/b/d/j;-><init>()V

    .line 659
    iget-object v0, p0, Lcom/a/b/d/acg;->d:Lcom/a/b/d/abx;

    iget-object v0, v0, Lcom/a/b/d/abx;->b:Lcom/a/b/b/dz;

    invoke-interface {v0}, Lcom/a/b/b/dz;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    iput-object v0, p0, Lcom/a/b/d/acg;->a:Ljava/util/Map;

    .line 660
    iget-object v0, p0, Lcom/a/b/d/acg;->d:Lcom/a/b/d/abx;

    iget-object v0, v0, Lcom/a/b/d/abx;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/acg;->b:Ljava/util/Iterator;

    .line 661
    invoke-static {}, Lcom/a/b/d/nj;->a()Lcom/a/b/d/agi;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/acg;->c:Ljava/util/Iterator;

    return-void
.end method

.method synthetic constructor <init>(Lcom/a/b/d/abx;B)V
    .registers 3

    .prologue
    .line 656
    invoke-direct {p0, p1}, Lcom/a/b/d/acg;-><init>(Lcom/a/b/d/abx;)V

    return-void
.end method


# virtual methods
.method protected final a()Ljava/lang/Object;
    .registers 5

    .prologue
    .line 665
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/a/b/d/acg;->c:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2e

    .line 666
    iget-object v0, p0, Lcom/a/b/d/acg;->c:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 667
    iget-object v1, p0, Lcom/a/b/d/acg;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 668
    iget-object v1, p0, Lcom/a/b/d/acg;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 669
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    .line 674
    :goto_2d
    return-object v0

    .line 671
    :cond_2e
    iget-object v0, p0, Lcom/a/b/d/acg;->b:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_49

    .line 672
    iget-object v0, p0, Lcom/a/b/d/acg;->b:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/acg;->c:Ljava/util/Iterator;

    goto :goto_0

    .line 674
    :cond_49
    invoke-virtual {p0}, Lcom/a/b/d/acg;->b()Ljava/lang/Object;

    move-result-object v0

    goto :goto_2d
.end method
