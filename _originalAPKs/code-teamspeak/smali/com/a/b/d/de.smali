.class final Lcom/a/b/d/de;
.super Lcom/a/b/d/rc;
.source "SourceFile"


# static fields
.field private static final l:J = 0x4L


# instance fields
.field final a:Lcom/a/b/b/bj;


# direct methods
.method constructor <init>(Lcom/a/b/d/sh;Lcom/a/b/d/sh;Lcom/a/b/b/au;Lcom/a/b/b/au;JJIILcom/a/b/d/qw;Ljava/util/concurrent/ConcurrentMap;Lcom/a/b/b/bj;)V
    .registers 15

    .prologue
    .line 396
    invoke-direct/range {p0 .. p12}, Lcom/a/b/d/rc;-><init>(Lcom/a/b/d/sh;Lcom/a/b/d/sh;Lcom/a/b/b/au;Lcom/a/b/b/au;JJIILcom/a/b/d/qw;Ljava/util/concurrent/ConcurrentMap;)V

    .line 398
    iput-object p13, p0, Lcom/a/b/d/de;->a:Lcom/a/b/b/bj;

    .line 399
    return-void
.end method

.method private b(Ljava/io/ObjectOutputStream;)V
    .registers 2

    .prologue
    .line 402
    invoke-virtual {p1}, Ljava/io/ObjectOutputStream;->defaultWriteObject()V

    .line 403
    invoke-virtual {p0, p1}, Lcom/a/b/d/de;->a(Ljava/io/ObjectOutputStream;)V

    .line 404
    return-void
.end method

.method private c()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 415
    iget-object v0, p0, Lcom/a/b/d/de;->k:Ljava/util/concurrent/ConcurrentMap;

    return-object v0
.end method

.method private c(Ljava/io/ObjectInputStream;)V
    .registers 4

    .prologue
    .line 408
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->defaultReadObject()V

    .line 409
    invoke-virtual {p0, p1}, Lcom/a/b/d/de;->a(Ljava/io/ObjectInputStream;)Lcom/a/b/d/ql;

    move-result-object v0

    .line 410
    iget-object v1, p0, Lcom/a/b/d/de;->a:Lcom/a/b/b/bj;

    invoke-virtual {v0, v1}, Lcom/a/b/d/ql;->a(Lcom/a/b/b/bj;)Ljava/util/concurrent/ConcurrentMap;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/de;->k:Ljava/util/concurrent/ConcurrentMap;

    .line 411
    invoke-virtual {p0, p1}, Lcom/a/b/d/de;->b(Ljava/io/ObjectInputStream;)V

    .line 412
    return-void
.end method
