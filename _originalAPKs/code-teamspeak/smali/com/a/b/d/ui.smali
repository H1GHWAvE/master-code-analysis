.class final Lcom/a/b/d/ui;
.super Lcom/a/b/d/vb;
.source "SourceFile"


# instance fields
.field a:Ljava/util/Map;

.field b:Lcom/a/b/b/co;


# direct methods
.method constructor <init>(Ljava/util/Map;Ljava/util/Map;Lcom/a/b/b/co;)V
    .registers 4

    .prologue
    .line 2619
    invoke-direct {p0, p1}, Lcom/a/b/d/vb;-><init>(Ljava/util/Map;)V

    .line 2620
    iput-object p2, p0, Lcom/a/b/d/ui;->a:Ljava/util/Map;

    .line 2621
    iput-object p3, p0, Lcom/a/b/d/ui;->b:Lcom/a/b/b/co;

    .line 2622
    return-void
.end method

.method private a(Lcom/a/b/b/co;)Z
    .registers 5

    .prologue
    .line 2631
    iget-object v0, p0, Lcom/a/b/d/ui;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/d/ui;->b:Lcom/a/b/b/co;

    invoke-static {p1}, Lcom/a/b/d/sz;->b(Lcom/a/b/b/co;)Lcom/a/b/b/co;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/a/b/b/cp;->a(Lcom/a/b/b/co;Lcom/a/b/b/co;)Lcom/a/b/b/co;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/a/b/d/mq;->a(Ljava/lang/Iterable;Lcom/a/b/b/co;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final remove(Ljava/lang/Object;)Z
    .registers 5

    .prologue
    .line 2625
    iget-object v0, p0, Lcom/a/b/d/ui;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/d/ui;->b:Lcom/a/b/b/co;

    invoke-static {p1}, Lcom/a/b/b/cp;->a(Ljava/lang/Object;)Lcom/a/b/b/co;

    move-result-object v2

    invoke-static {v2}, Lcom/a/b/d/sz;->b(Lcom/a/b/b/co;)Lcom/a/b/b/co;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/a/b/b/cp;->a(Lcom/a/b/b/co;Lcom/a/b/b/co;)Lcom/a/b/b/co;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/a/b/d/mq;->b(Ljava/lang/Iterable;Lcom/a/b/b/co;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1c

    const/4 v0, 0x1

    :goto_1b
    return v0

    :cond_1c
    const/4 v0, 0x0

    goto :goto_1b
.end method

.method public final removeAll(Ljava/util/Collection;)Z
    .registers 3

    .prologue
    .line 2636
    invoke-static {p1}, Lcom/a/b/b/cp;->a(Ljava/util/Collection;)Lcom/a/b/b/co;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/a/b/d/ui;->a(Lcom/a/b/b/co;)Z

    move-result v0

    return v0
.end method

.method public final retainAll(Ljava/util/Collection;)Z
    .registers 3

    .prologue
    .line 2640
    invoke-static {p1}, Lcom/a/b/b/cp;->a(Ljava/util/Collection;)Lcom/a/b/b/co;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/b/cp;->a(Lcom/a/b/b/co;)Lcom/a/b/b/co;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/a/b/d/ui;->a(Lcom/a/b/b/co;)Z

    move-result v0

    return v0
.end method

.method public final toArray()[Ljava/lang/Object;
    .registers 2

    .prologue
    .line 2645
    invoke-virtual {p0}, Lcom/a/b/d/ui;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/ov;->a(Ljava/util/Iterator;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final toArray([Ljava/lang/Object;)[Ljava/lang/Object;
    .registers 3

    .prologue
    .line 2649
    invoke-virtual {p0}, Lcom/a/b/d/ui;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/ov;->a(Ljava/util/Iterator;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
