.class final Lcom/a/b/d/ob;
.super Lcom/a/b/d/yd;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation build Lcom/a/b/a/b;
    a = true
.end annotation


# static fields
.field private static final b:J


# instance fields
.field final a:Lcom/a/b/d/yd;


# direct methods
.method constructor <init>(Lcom/a/b/d/yd;)V
    .registers 2

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/a/b/d/yd;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/a/b/d/ob;->a:Lcom/a/b/d/yd;

    .line 37
    return-void
.end method

.method private a(Ljava/lang/Iterable;Ljava/lang/Iterable;)I
    .registers 8

    .prologue
    .line 41
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 42
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 43
    :cond_8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_27

    .line 44
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_16

    .line 45
    const/4 v0, 0x1

    .line 55
    :goto_15
    return v0

    .line 47
    :cond_16
    iget-object v0, p0, Lcom/a/b/d/ob;->a:Lcom/a/b/d/yd;

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Lcom/a/b/d/yd;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    .line 48
    if-eqz v0, :cond_8

    goto :goto_15

    .line 52
    :cond_27
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2f

    .line 53
    const/4 v0, -0x1

    goto :goto_15

    .line 55
    :cond_2f
    const/4 v0, 0x0

    goto :goto_15
.end method


# virtual methods
.method public final synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .registers 8

    .prologue
    .line 30
    check-cast p1, Ljava/lang/Iterable;

    check-cast p2, Ljava/lang/Iterable;

    .line 1041
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 1042
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 1043
    :cond_c
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2b

    .line 1044
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1a

    .line 1045
    const/4 v0, 0x1

    .line 1053
    :goto_19
    return v0

    .line 1047
    :cond_1a
    iget-object v0, p0, Lcom/a/b/d/ob;->a:Lcom/a/b/d/yd;

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Lcom/a/b/d/yd;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    .line 1048
    if-eqz v0, :cond_c

    goto :goto_19

    .line 1052
    :cond_2b
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_33

    .line 1053
    const/4 v0, -0x1

    goto :goto_19

    .line 1055
    :cond_33
    const/4 v0, 0x0

    .line 30
    goto :goto_19
.end method

.method public final equals(Ljava/lang/Object;)Z
    .registers 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 59
    if-ne p1, p0, :cond_4

    .line 60
    const/4 v0, 0x1

    .line 66
    :goto_3
    return v0

    .line 62
    :cond_4
    instance-of v0, p1, Lcom/a/b/d/ob;

    if-eqz v0, :cond_13

    .line 63
    check-cast p1, Lcom/a/b/d/ob;

    .line 64
    iget-object v0, p0, Lcom/a/b/d/ob;->a:Lcom/a/b/d/yd;

    iget-object v1, p1, Lcom/a/b/d/ob;->a:Lcom/a/b/d/yd;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_3

    .line 66
    :cond_13
    const/4 v0, 0x0

    goto :goto_3
.end method

.method public final hashCode()I
    .registers 3

    .prologue
    .line 70
    iget-object v0, p0, Lcom/a/b/d/ob;->a:Lcom/a/b/d/yd;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    const v1, 0x7bb78cf5

    xor-int/2addr v0, v1

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .registers 4

    .prologue
    .line 74
    iget-object v0, p0, Lcom/a/b/d/ob;->a:Lcom/a/b/d/yd;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x12

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".lexicographical()"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
