.class final Lcom/a/b/d/pu;
.super Lcom/a/b/d/gh;
.source "SourceFile"


# instance fields
.field final a:Ljava/util/Collection;

.field final b:Ljava/util/Set;


# direct methods
.method constructor <init>(Ljava/util/Collection;Ljava/util/Set;)V
    .registers 3

    .prologue
    .line 518
    invoke-direct {p0}, Lcom/a/b/d/gh;-><init>()V

    .line 519
    iput-object p1, p0, Lcom/a/b/d/pu;->a:Ljava/util/Collection;

    .line 520
    iput-object p2, p0, Lcom/a/b/d/pu;->b:Ljava/util/Set;

    .line 521
    return-void
.end method


# virtual methods
.method protected final b()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 523
    iget-object v0, p0, Lcom/a/b/d/pu;->a:Ljava/util/Collection;

    return-object v0
.end method

.method public final contains(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 551
    invoke-virtual {p0, p1}, Lcom/a/b/d/pu;->b(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final containsAll(Ljava/util/Collection;)Z
    .registers 3

    .prologue
    .line 554
    .line 2141
    invoke-static {p0, p1}, Lcom/a/b/d/cm;->a(Ljava/util/Collection;Ljava/util/Collection;)Z

    move-result v0

    .line 554
    return v0
.end method

.method public final iterator()Ljava/util/Iterator;
    .registers 3

    .prologue
    .line 527
    iget-object v0, p0, Lcom/a/b/d/pu;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 528
    new-instance v1, Lcom/a/b/d/pv;

    invoke-direct {v1, p0, v0}, Lcom/a/b/d/pv;-><init>(Lcom/a/b/d/pu;Ljava/util/Iterator;)V

    return-object v1
.end method

.method protected final bridge synthetic k_()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 508
    .line 2523
    iget-object v0, p0, Lcom/a/b/d/pu;->a:Ljava/util/Collection;

    .line 508
    return-object v0
.end method

.method public final remove(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 557
    invoke-virtual {p0, p1}, Lcom/a/b/d/pu;->c(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final removeAll(Ljava/util/Collection;)Z
    .registers 3

    .prologue
    .line 560
    invoke-virtual {p0, p1}, Lcom/a/b/d/pu;->b(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public final retainAll(Ljava/util/Collection;)Z
    .registers 3

    .prologue
    .line 563
    invoke-virtual {p0, p1}, Lcom/a/b/d/pu;->c(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public final toArray()[Ljava/lang/Object;
    .registers 2

    .prologue
    .line 545
    invoke-virtual {p0}, Lcom/a/b/d/pu;->p()[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final toArray([Ljava/lang/Object;)[Ljava/lang/Object;
    .registers 3

    .prologue
    .line 548
    .line 1253
    invoke-static {p0, p1}, Lcom/a/b/d/yc;->a(Ljava/util/Collection;[Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    .line 548
    return-object v0
.end method
