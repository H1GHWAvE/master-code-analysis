.class public final Lcom/a/b/d/iy;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Lcom/a/b/d/ju;


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    invoke-static {}, Lcom/a/b/d/jt;->l()Lcom/a/b/d/ju;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/iy;->a:Lcom/a/b/d/ju;

    return-void
.end method

.method private a()Lcom/a/b/d/iw;
    .registers 4

    .prologue
    .line 107
    new-instance v0, Lcom/a/b/d/iw;

    iget-object v1, p0, Lcom/a/b/d/iy;->a:Lcom/a/b/d/ju;

    invoke-virtual {v1}, Lcom/a/b/d/ju;->a()Lcom/a/b/d/jt;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/a/b/d/iw;-><init>(Lcom/a/b/d/jt;B)V

    return-object v0
.end method

.method private a(Ljava/lang/Class;Ljava/lang/Object;)Lcom/a/b/d/iy;
    .registers 4

    .prologue
    .line 73
    iget-object v0, p0, Lcom/a/b/d/iy;->a:Lcom/a/b/d/ju;

    invoke-virtual {v0, p1, p2}, Lcom/a/b/d/ju;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/ju;

    .line 74
    return-object p0
.end method

.method private a(Ljava/util/Map;)Lcom/a/b/d/iy;
    .registers 7

    .prologue
    .line 88
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_8
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2c

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 89
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Class;

    .line 90
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    .line 91
    iget-object v3, p0, Lcom/a/b/d/iy;->a:Lcom/a/b/d/ju;

    .line 1097
    invoke-static {v1}, Lcom/a/b/l/z;->a(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 91
    invoke-virtual {v3, v1, v0}, Lcom/a/b/d/ju;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/ju;

    goto :goto_8

    .line 93
    :cond_2c
    return-object p0
.end method

.method private static b(Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 97
    invoke-static {p0}, Lcom/a/b/l/z;->a(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
