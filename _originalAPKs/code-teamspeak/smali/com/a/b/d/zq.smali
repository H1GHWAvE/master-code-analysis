.class final Lcom/a/b/d/zq;
.super Lcom/a/b/d/me;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/b;
    a = true
    b = true
.end annotation


# instance fields
.field private final transient a:Lcom/a/b/d/jl;


# direct methods
.method constructor <init>(Lcom/a/b/d/jl;Ljava/util/Comparator;)V
    .registers 4

    .prologue
    .line 54
    invoke-direct {p0, p2}, Lcom/a/b/d/me;-><init>(Ljava/util/Comparator;)V

    .line 55
    iput-object p1, p0, Lcom/a/b/d/zq;->a:Lcom/a/b/d/jl;

    .line 56
    invoke-virtual {p1}, Lcom/a/b/d/jl;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_10

    const/4 v0, 0x1

    :goto_c
    invoke-static {v0}, Lcom/a/b/b/cn;->a(Z)V

    .line 57
    return-void

    .line 56
    :cond_10
    const/4 v0, 0x0

    goto :goto_c
.end method

.method private e(Ljava/lang/Object;)I
    .registers 4

    .prologue
    .line 137
    iget-object v0, p0, Lcom/a/b/d/zq;->a:Lcom/a/b/d/jl;

    .line 2254
    iget-object v1, p0, Lcom/a/b/d/zq;->d:Ljava/util/Comparator;

    .line 137
    invoke-static {v0, p1, v1}, Ljava/util/Collections;->binarySearch(Ljava/util/List;Ljava/lang/Object;Ljava/util/Comparator;)I

    move-result v0

    return v0
.end method

.method private k()Ljava/util/Comparator;
    .registers 2

    .prologue
    .line 254
    iget-object v0, p0, Lcom/a/b/d/zq;->d:Ljava/util/Comparator;

    return-object v0
.end method


# virtual methods
.method final a([Ljava/lang/Object;I)I
    .registers 4

    .prologue
    .line 146
    iget-object v0, p0, Lcom/a/b/d/zq;->a:Lcom/a/b/d/jl;

    invoke-virtual {v0, p1, p2}, Lcom/a/b/d/jl;->a([Ljava/lang/Object;I)I

    move-result v0

    return v0
.end method

.method final a(II)Lcom/a/b/d/me;
    .registers 6

    .prologue
    .line 258
    if-nez p1, :cond_9

    invoke-virtual {p0}, Lcom/a/b/d/zq;->size()I

    move-result v0

    if-ne p2, v0, :cond_9

    .line 264
    :goto_8
    return-object p0

    .line 260
    :cond_9
    if-ge p1, p2, :cond_1a

    .line 261
    new-instance v0, Lcom/a/b/d/zq;

    iget-object v1, p0, Lcom/a/b/d/zq;->a:Lcom/a/b/d/jl;

    invoke-virtual {v1, p1, p2}, Lcom/a/b/d/jl;->a(II)Lcom/a/b/d/jl;

    move-result-object v1

    iget-object v2, p0, Lcom/a/b/d/zq;->d:Ljava/util/Comparator;

    invoke-direct {v0, v1, v2}, Lcom/a/b/d/zq;-><init>(Lcom/a/b/d/jl;Ljava/util/Comparator;)V

    move-object p0, v0

    goto :goto_8

    .line 264
    :cond_1a
    iget-object v0, p0, Lcom/a/b/d/zq;->d:Ljava/util/Comparator;

    invoke-static {v0}, Lcom/a/b/d/zq;->a(Ljava/util/Comparator;)Lcom/a/b/d/me;

    move-result-object p0

    goto :goto_8
.end method

.method final a(Ljava/lang/Object;Z)Lcom/a/b/d/me;
    .registers 5

    .prologue
    .line 238
    invoke-virtual {p0, p1, p2}, Lcom/a/b/d/zq;->f(Ljava/lang/Object;Z)I

    move-result v0

    invoke-virtual {p0}, Lcom/a/b/d/zq;->size()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/a/b/d/zq;->a(II)Lcom/a/b/d/me;

    move-result-object v0

    return-object v0
.end method

.method final a(Ljava/lang/Object;ZLjava/lang/Object;Z)Lcom/a/b/d/me;
    .registers 6

    .prologue
    .line 232
    invoke-virtual {p0, p1, p2}, Lcom/a/b/d/zq;->a(Ljava/lang/Object;Z)Lcom/a/b/d/me;

    move-result-object v0

    invoke-virtual {v0, p3, p4}, Lcom/a/b/d/me;->b(Ljava/lang/Object;Z)Lcom/a/b/d/me;

    move-result-object v0

    return-object v0
.end method

.method final b(Ljava/lang/Object;Z)Lcom/a/b/d/me;
    .registers 5

    .prologue
    .line 220
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2}, Lcom/a/b/d/zq;->e(Ljava/lang/Object;Z)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/a/b/d/zq;->a(II)Lcom/a/b/d/me;

    move-result-object v0

    return-object v0
.end method

.method final c(Ljava/lang/Object;)I
    .registers 7
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, -0x1

    .line 269
    if-nez p1, :cond_4

    .line 279
    :cond_3
    :goto_3
    return v0

    .line 274
    :cond_4
    :try_start_4
    iget-object v1, p0, Lcom/a/b/d/zq;->a:Lcom/a/b/d/jl;

    .line 3254
    iget-object v2, p0, Lcom/a/b/d/zq;->d:Ljava/util/Comparator;

    .line 274
    sget-object v3, Lcom/a/b/d/abg;->a:Lcom/a/b/d/abg;

    sget-object v4, Lcom/a/b/d/abc;->c:Lcom/a/b/d/abc;

    invoke-static {v1, p1, v2, v3, v4}, Lcom/a/b/d/aba;->a(Ljava/util/List;Ljava/lang/Object;Ljava/util/Comparator;Lcom/a/b/d/abg;Lcom/a/b/d/abc;)I
    :try_end_f
    .catch Ljava/lang/ClassCastException; {:try_start_4 .. :try_end_f} :catch_14

    move-result v1

    .line 279
    if-ltz v1, :cond_3

    move v0, v1

    goto :goto_3

    .line 277
    :catch_14
    move-exception v1

    goto :goto_3
.end method

.method public final c()Lcom/a/b/d/agi;
    .registers 2

    .prologue
    .line 60
    iget-object v0, p0, Lcom/a/b/d/zq;->a:Lcom/a/b/d/jl;

    invoke-virtual {v0}, Lcom/a/b/d/jl;->c()Lcom/a/b/d/agi;

    move-result-object v0

    return-object v0
.end method

.method public final ceiling(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 208
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/a/b/d/zq;->f(Ljava/lang/Object;Z)I

    move-result v0

    .line 209
    invoke-virtual {p0}, Lcom/a/b/d/zq;->size()I

    move-result v1

    if-ne v0, v1, :cond_d

    const/4 v0, 0x0

    :goto_c
    return-object v0

    :cond_d
    iget-object v1, p0, Lcom/a/b/d/zq;->a:Lcom/a/b/d/jl;

    invoke-virtual {v1, v0}, Lcom/a/b/d/jl;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_c
.end method

.method public final contains(Ljava/lang/Object;)Z
    .registers 5

    .prologue
    const/4 v0, 0x0

    .line 79
    if-eqz p1, :cond_e

    .line 1137
    :try_start_3
    iget-object v1, p0, Lcom/a/b/d/zq;->a:Lcom/a/b/d/jl;

    .line 1254
    iget-object v2, p0, Lcom/a/b/d/zq;->d:Ljava/util/Comparator;

    .line 1137
    invoke-static {v1, p1, v2}, Ljava/util/Collections;->binarySearch(Ljava/util/List;Ljava/lang/Object;Ljava/util/Comparator;)I
    :try_end_a
    .catch Ljava/lang/ClassCastException; {:try_start_3 .. :try_end_a} :catch_f

    move-result v1

    .line 79
    if-ltz v1, :cond_e

    const/4 v0, 0x1

    .line 81
    :cond_e
    :goto_e
    return v0

    :catch_f
    move-exception v1

    goto :goto_e
.end method

.method public final containsAll(Ljava/util/Collection;)Z
    .registers 8

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 90
    instance-of v2, p1, Lcom/a/b/d/xc;

    if-eqz v2, :cond_c

    .line 91
    check-cast p1, Lcom/a/b/d/xc;

    invoke-interface {p1}, Lcom/a/b/d/xc;->n_()Ljava/util/Set;

    move-result-object p1

    .line 93
    :cond_c
    invoke-virtual {p0}, Lcom/a/b/d/zq;->comparator()Ljava/util/Comparator;

    move-result-object v2

    invoke-static {v2, p1}, Lcom/a/b/d/aaz;->a(Ljava/util/Comparator;Ljava/lang/Iterable;)Z

    move-result v2

    if-eqz v2, :cond_1c

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v2

    if-gt v2, v0, :cond_21

    .line 95
    :cond_1c
    invoke-super {p0, p1}, Lcom/a/b/d/me;->containsAll(Ljava/util/Collection;)Z

    move-result v0

    .line 133
    :cond_20
    :goto_20
    return v0

    .line 2060
    :cond_21
    iget-object v2, p0, Lcom/a/b/d/zq;->a:Lcom/a/b/d/jl;

    invoke-virtual {v2}, Lcom/a/b/d/jl;->c()Lcom/a/b/d/agi;

    move-result-object v2

    .line 102
    invoke-static {v2}, Lcom/a/b/d/nj;->j(Ljava/util/Iterator;)Lcom/a/b/d/yi;

    move-result-object v3

    .line 103
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 104
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 108
    :cond_33
    :goto_33
    :try_start_33
    invoke-interface {v3}, Lcom/a/b/d/yi;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_5e

    .line 110
    invoke-interface {v3}, Lcom/a/b/d/yi;->a()Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {p0, v5, v2}, Lcom/a/b/d/zq;->c(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v5

    .line 112
    if-gez v5, :cond_4a

    .line 113
    invoke-interface {v3}, Lcom/a/b/d/yi;->next()Ljava/lang/Object;

    goto :goto_33

    .line 128
    :catch_47
    move-exception v0

    move v0, v1

    goto :goto_20

    .line 114
    :cond_4a
    if-nez v5, :cond_57

    .line 116
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_20

    .line 121
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;
    :try_end_55
    .catch Ljava/lang/NullPointerException; {:try_start_33 .. :try_end_55} :catch_47
    .catch Ljava/lang/ClassCastException; {:try_start_33 .. :try_end_55} :catch_5b

    move-result-object v2

    goto :goto_33

    .line 123
    :cond_57
    if-lez v5, :cond_33

    move v0, v1

    .line 124
    goto :goto_20

    .line 130
    :catch_5b
    move-exception v0

    move v0, v1

    goto :goto_20

    :cond_5e
    move v0, v1

    .line 133
    goto :goto_20
.end method

.method public final d()Lcom/a/b/d/agi;
    .registers 2
    .annotation build Lcom/a/b/a/c;
        a = "NavigableSet"
    .end annotation

    .prologue
    .line 65
    iget-object v0, p0, Lcom/a/b/d/zq;->a:Lcom/a/b/d/jl;

    invoke-virtual {v0}, Lcom/a/b/d/jl;->e()Lcom/a/b/d/jl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/jl;->c()Lcom/a/b/d/agi;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic descendingIterator()Ljava/util/Iterator;
    .registers 2

    .prologue
    .line 46
    invoke-virtual {p0}, Lcom/a/b/d/zq;->d()Lcom/a/b/d/agi;

    move-result-object v0

    return-object v0
.end method

.method final e(Ljava/lang/Object;Z)I
    .registers 8

    .prologue
    .line 224
    iget-object v1, p0, Lcom/a/b/d/zq;->a:Lcom/a/b/d/jl;

    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0}, Lcom/a/b/d/zq;->comparator()Ljava/util/Comparator;

    move-result-object v3

    if-eqz p2, :cond_15

    sget-object v0, Lcom/a/b/d/abg;->d:Lcom/a/b/d/abg;

    :goto_e
    sget-object v4, Lcom/a/b/d/abc;->b:Lcom/a/b/d/abc;

    invoke-static {v1, v2, v3, v0, v4}, Lcom/a/b/d/aba;->a(Ljava/util/List;Ljava/lang/Object;Ljava/util/Comparator;Lcom/a/b/d/abg;Lcom/a/b/d/abc;)I

    move-result v0

    return v0

    :cond_15
    sget-object v0, Lcom/a/b/d/abg;->c:Lcom/a/b/d/abg;

    goto :goto_e
.end method

.method final e()Lcom/a/b/d/me;
    .registers 4

    .prologue
    .line 288
    new-instance v0, Lcom/a/b/d/zq;

    iget-object v1, p0, Lcom/a/b/d/zq;->a:Lcom/a/b/d/jl;

    invoke-virtual {v1}, Lcom/a/b/d/jl;->e()Lcom/a/b/d/jl;

    move-result-object v1

    iget-object v2, p0, Lcom/a/b/d/zq;->d:Ljava/util/Comparator;

    invoke-static {v2}, Lcom/a/b/d/yd;->a(Ljava/util/Comparator;)Lcom/a/b/d/yd;

    move-result-object v2

    invoke-virtual {v2}, Lcom/a/b/d/yd;->a()Lcom/a/b/d/yd;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/a/b/d/zq;-><init>(Lcom/a/b/d/jl;Ljava/util/Comparator;)V

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .registers 8
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 150
    if-ne p1, p0, :cond_5

    .line 181
    :cond_4
    :goto_4
    return v0

    .line 153
    :cond_5
    instance-of v2, p1, Ljava/util/Set;

    if-nez v2, :cond_b

    move v0, v1

    .line 154
    goto :goto_4

    .line 157
    :cond_b
    check-cast p1, Ljava/util/Set;

    .line 158
    invoke-virtual {p0}, Lcom/a/b/d/zq;->size()I

    move-result v2

    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result v3

    if-eq v2, v3, :cond_19

    move v0, v1

    .line 159
    goto :goto_4

    .line 162
    :cond_19
    iget-object v2, p0, Lcom/a/b/d/zq;->d:Ljava/util/Comparator;

    invoke-static {v2, p1}, Lcom/a/b/d/aaz;->a(Ljava/util/Comparator;Ljava/lang/Iterable;)Z

    move-result v2

    if-eqz v2, :cond_49

    .line 163
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 3060
    :try_start_25
    iget-object v3, p0, Lcom/a/b/d/zq;->a:Lcom/a/b/d/jl;

    invoke-virtual {v3}, Lcom/a/b/d/jl;->c()Lcom/a/b/d/agi;

    move-result-object v3

    .line 166
    :cond_2b
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 167
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .line 168
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    .line 169
    if-eqz v5, :cond_41

    invoke-virtual {p0, v4, v5}, Lcom/a/b/d/zq;->c(Ljava/lang/Object;Ljava/lang/Object;)I
    :try_end_3e
    .catch Ljava/lang/ClassCastException; {:try_start_25 .. :try_end_3e} :catch_43
    .catch Ljava/util/NoSuchElementException; {:try_start_25 .. :try_end_3e} :catch_46

    move-result v4

    if-eqz v4, :cond_2b

    :cond_41
    move v0, v1

    .line 171
    goto :goto_4

    .line 176
    :catch_43
    move-exception v0

    move v0, v1

    goto :goto_4

    .line 178
    :catch_46
    move-exception v0

    move v0, v1

    goto :goto_4

    .line 181
    :cond_49
    invoke-virtual {p0, p1}, Lcom/a/b/d/zq;->containsAll(Ljava/util/Collection;)Z

    move-result v0

    goto :goto_4
.end method

.method final f(Ljava/lang/Object;Z)I
    .registers 8

    .prologue
    .line 242
    iget-object v1, p0, Lcom/a/b/d/zq;->a:Lcom/a/b/d/jl;

    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0}, Lcom/a/b/d/zq;->comparator()Ljava/util/Comparator;

    move-result-object v3

    if-eqz p2, :cond_15

    sget-object v0, Lcom/a/b/d/abg;->c:Lcom/a/b/d/abg;

    :goto_e
    sget-object v4, Lcom/a/b/d/abc;->b:Lcom/a/b/d/abc;

    invoke-static {v1, v2, v3, v0, v4}, Lcom/a/b/d/aba;->a(Ljava/util/List;Ljava/lang/Object;Ljava/util/Comparator;Lcom/a/b/d/abg;Lcom/a/b/d/abc;)I

    move-result v0

    return v0

    :cond_15
    sget-object v0, Lcom/a/b/d/abg;->d:Lcom/a/b/d/abg;

    goto :goto_e
.end method

.method public final first()Ljava/lang/Object;
    .registers 3

    .prologue
    .line 186
    iget-object v0, p0, Lcom/a/b/d/zq;->a:Lcom/a/b/d/jl;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/a/b/d/jl;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final floor(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 202
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/a/b/d/zq;->e(Ljava/lang/Object;Z)I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 203
    const/4 v1, -0x1

    if-ne v0, v1, :cond_c

    const/4 v0, 0x0

    :goto_b
    return-object v0

    :cond_c
    iget-object v1, p0, Lcom/a/b/d/zq;->a:Lcom/a/b/d/jl;

    invoke-virtual {v1, v0}, Lcom/a/b/d/jl;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_b
.end method

.method final h_()Z
    .registers 2

    .prologue
    .line 141
    iget-object v0, p0, Lcom/a/b/d/zq;->a:Lcom/a/b/d/jl;

    invoke-virtual {v0}, Lcom/a/b/d/jl;->h_()Z

    move-result v0

    return v0
.end method

.method public final higher(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 214
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/a/b/d/zq;->f(Ljava/lang/Object;Z)I

    move-result v0

    .line 215
    invoke-virtual {p0}, Lcom/a/b/d/zq;->size()I

    move-result v1

    if-ne v0, v1, :cond_d

    const/4 v0, 0x0

    :goto_c
    return-object v0

    :cond_d
    iget-object v1, p0, Lcom/a/b/d/zq;->a:Lcom/a/b/d/jl;

    invoke-virtual {v1, v0}, Lcom/a/b/d/jl;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_c
.end method

.method public final isEmpty()Z
    .registers 2

    .prologue
    .line 69
    const/4 v0, 0x0

    return v0
.end method

.method public final synthetic iterator()Ljava/util/Iterator;
    .registers 2

    .prologue
    .line 46
    .line 4060
    iget-object v0, p0, Lcom/a/b/d/zq;->a:Lcom/a/b/d/jl;

    invoke-virtual {v0}, Lcom/a/b/d/jl;->c()Lcom/a/b/d/agi;

    move-result-object v0

    .line 46
    return-object v0
.end method

.method final l()Lcom/a/b/d/jl;
    .registers 3

    .prologue
    .line 283
    new-instance v0, Lcom/a/b/d/lv;

    iget-object v1, p0, Lcom/a/b/d/zq;->a:Lcom/a/b/d/jl;

    invoke-direct {v0, p0, v1}, Lcom/a/b/d/lv;-><init>(Lcom/a/b/d/me;Lcom/a/b/d/jl;)V

    return-object v0
.end method

.method public final last()Ljava/lang/Object;
    .registers 3

    .prologue
    .line 191
    iget-object v0, p0, Lcom/a/b/d/zq;->a:Lcom/a/b/d/jl;

    invoke-virtual {p0}, Lcom/a/b/d/zq;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Lcom/a/b/d/jl;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final lower(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 196
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/a/b/d/zq;->e(Ljava/lang/Object;Z)I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 197
    const/4 v1, -0x1

    if-ne v0, v1, :cond_c

    const/4 v0, 0x0

    :goto_b
    return-object v0

    :cond_c
    iget-object v1, p0, Lcom/a/b/d/zq;->a:Lcom/a/b/d/jl;

    invoke-virtual {v1, v0}, Lcom/a/b/d/jl;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_b
.end method

.method public final size()I
    .registers 2

    .prologue
    .line 74
    iget-object v0, p0, Lcom/a/b/d/zq;->a:Lcom/a/b/d/jl;

    invoke-virtual {v0}, Lcom/a/b/d/jl;->size()I

    move-result v0

    return v0
.end method
