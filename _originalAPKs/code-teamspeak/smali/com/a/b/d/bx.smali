.class public abstract Lcom/a/b/d/bx;
.super Lcom/a/b/d/aga;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/a;
.end annotation

.annotation build Lcom/a/b/a/b;
    b = true
.end annotation


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/a/b/d/aga;-><init>()V

    .line 179
    return-void
.end method

.method static synthetic a(Ljava/util/Deque;Lcom/a/b/b/ci;)V
    .registers 3

    .prologue
    .line 1208
    invoke-virtual {p1}, Lcom/a/b/b/ci;->b()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 1209
    invoke-virtual {p1}, Lcom/a/b/b/ci;->c()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p0, v0}, Ljava/util/Deque;->addLast(Ljava/lang/Object;)V

    .line 39
    :cond_d
    return-void
.end method

.method private static b(Ljava/util/Deque;Lcom/a/b/b/ci;)V
    .registers 3

    .prologue
    .line 208
    invoke-virtual {p1}, Lcom/a/b/b/ci;->b()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 209
    invoke-virtual {p1}, Lcom/a/b/b/ci;->c()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p0, v0}, Ljava/util/Deque;->addLast(Ljava/lang/Object;)V

    .line 211
    :cond_d
    return-void
.end method

.method private d(Ljava/lang/Object;)Lcom/a/b/d/gd;
    .registers 3

    .prologue
    .line 170
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 171
    new-instance v0, Lcom/a/b/d/ca;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/ca;-><init>(Lcom/a/b/d/bx;Ljava/lang/Object;)V

    return-object v0
.end method


# virtual methods
.method public abstract a()Lcom/a/b/b/ci;
.end method

.method public final a(Ljava/lang/Object;)Ljava/lang/Iterable;
    .registers 3

    .prologue
    .line 59
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 60
    new-instance v0, Lcom/a/b/d/by;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/by;-><init>(Lcom/a/b/d/bx;Ljava/lang/Object;)V

    return-object v0
.end method

.method public abstract b()Lcom/a/b/b/ci;
.end method

.method final b(Ljava/lang/Object;)Lcom/a/b/d/agi;
    .registers 3

    .prologue
    .line 92
    new-instance v0, Lcom/a/b/d/cd;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/cd;-><init>(Lcom/a/b/d/bx;Ljava/lang/Object;)V

    return-object v0
.end method

.method final c(Ljava/lang/Object;)Lcom/a/b/d/agi;
    .registers 3

    .prologue
    .line 128
    new-instance v0, Lcom/a/b/d/cc;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/cc;-><init>(Lcom/a/b/d/bx;Ljava/lang/Object;)V

    return-object v0
.end method
