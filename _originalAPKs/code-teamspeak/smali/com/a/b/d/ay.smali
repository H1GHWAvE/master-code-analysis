.class abstract Lcom/a/b/d/ay;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/d/yr;


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/a/b/d/yl;)V
    .registers 3

    .prologue
    .line 42
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public a()Z
    .registers 2

    .prologue
    .line 37
    invoke-virtual {p0}, Lcom/a/b/d/ay;->g()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public a(Lcom/a/b/d/yr;)Z
    .registers 4

    .prologue
    .line 57
    invoke-interface {p1}, Lcom/a/b/d/yr;->g()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1c

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/yl;

    .line 58
    invoke-virtual {p0, v0}, Lcom/a/b/d/ay;->c(Lcom/a/b/d/yl;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 59
    const/4 v0, 0x0

    .line 62
    :goto_1b
    return v0

    :cond_1c
    const/4 v0, 0x1

    goto :goto_1b
.end method

.method public a(Ljava/lang/Comparable;)Z
    .registers 3

    .prologue
    .line 29
    invoke-virtual {p0, p1}, Lcom/a/b/d/ay;->b(Ljava/lang/Comparable;)Lcom/a/b/d/yl;

    move-result-object v0

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public abstract b(Ljava/lang/Comparable;)Lcom/a/b/d/yl;
.end method

.method public b()V
    .registers 2

    .prologue
    .line 52
    invoke-static {}, Lcom/a/b/d/yl;->c()Lcom/a/b/d/yl;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/a/b/d/ay;->b(Lcom/a/b/d/yl;)V

    .line 53
    return-void
.end method

.method public b(Lcom/a/b/d/yl;)V
    .registers 3

    .prologue
    .line 47
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public b(Lcom/a/b/d/yr;)V
    .registers 4

    .prologue
    .line 67
    invoke-interface {p1}, Lcom/a/b/d/yr;->g()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_18

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/yl;

    .line 68
    invoke-virtual {p0, v0}, Lcom/a/b/d/ay;->a(Lcom/a/b/d/yl;)V

    goto :goto_8

    .line 70
    :cond_18
    return-void
.end method

.method public c(Lcom/a/b/d/yr;)V
    .registers 4

    .prologue
    .line 74
    invoke-interface {p1}, Lcom/a/b/d/yr;->g()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_18

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/yl;

    .line 75
    invoke-virtual {p0, v0}, Lcom/a/b/d/ay;->b(Lcom/a/b/d/yl;)V

    goto :goto_8

    .line 77
    :cond_18
    return-void
.end method

.method public abstract c(Lcom/a/b/d/yl;)Z
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 84
    if-ne p1, p0, :cond_4

    .line 85
    const/4 v0, 0x1

    .line 90
    :goto_3
    return v0

    .line 86
    :cond_4
    instance-of v0, p1, Lcom/a/b/d/yr;

    if-eqz v0, :cond_17

    .line 87
    check-cast p1, Lcom/a/b/d/yr;

    .line 88
    invoke-virtual {p0}, Lcom/a/b/d/ay;->g()Ljava/util/Set;

    move-result-object v0

    invoke-interface {p1}, Lcom/a/b/d/yr;->g()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_3

    .line 90
    :cond_17
    const/4 v0, 0x0

    goto :goto_3
.end method

.method public final hashCode()I
    .registers 2

    .prologue
    .line 95
    invoke-virtual {p0}, Lcom/a/b/d/ay;->g()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->hashCode()I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .registers 2

    .prologue
    .line 100
    invoke-virtual {p0}, Lcom/a/b/d/ay;->g()Ljava/util/Set;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
