.class final Lcom/a/b/d/lm;
.super Lcom/a/b/d/jl;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/a/b/d/lf;

.field private final b:Z

.field private final d:Z

.field private final e:I


# direct methods
.method constructor <init>(Lcom/a/b/d/lf;)V
    .registers 4

    .prologue
    .line 194
    iput-object p1, p0, Lcom/a/b/d/lm;->a:Lcom/a/b/d/lf;

    invoke-direct {p0}, Lcom/a/b/d/jl;-><init>()V

    .line 195
    invoke-static {p1}, Lcom/a/b/d/lf;->a(Lcom/a/b/d/lf;)Lcom/a/b/d/jl;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/a/b/d/jl;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/yl;

    invoke-virtual {v0}, Lcom/a/b/d/yl;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/a/b/d/lm;->b:Z

    .line 196
    invoke-static {p1}, Lcom/a/b/d/lf;->a(Lcom/a/b/d/lf;)Lcom/a/b/d/jl;

    move-result-object v0

    .line 1786
    instance-of v1, v0, Ljava/util/List;

    if-eqz v1, :cond_51

    .line 1787
    check-cast v0, Ljava/util/List;

    .line 1788
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2c

    .line 1789
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 1791
    :cond_2c
    invoke-static {v0}, Lcom/a/b/d/mq;->a(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    .line 196
    :goto_30
    check-cast v0, Lcom/a/b/d/yl;

    invoke-virtual {v0}, Lcom/a/b/d/yl;->e()Z

    move-result v0

    iput-boolean v0, p0, Lcom/a/b/d/lm;->d:Z

    .line 198
    invoke-static {p1}, Lcom/a/b/d/lf;->a(Lcom/a/b/d/lf;)Lcom/a/b/d/jl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/jl;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 199
    iget-boolean v1, p0, Lcom/a/b/d/lm;->b:Z

    if-eqz v1, :cond_48

    .line 200
    add-int/lit8 v0, v0, 0x1

    .line 202
    :cond_48
    iget-boolean v1, p0, Lcom/a/b/d/lm;->d:Z

    if-eqz v1, :cond_4e

    .line 203
    add-int/lit8 v0, v0, 0x1

    .line 205
    :cond_4e
    iput v0, p0, Lcom/a/b/d/lm;->e:I

    .line 206
    return-void

    .line 1794
    :cond_51
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/nj;->f(Ljava/util/Iterator;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_30
.end method

.method private b(I)Lcom/a/b/d/yl;
    .registers 5

    .prologue
    .line 215
    iget v0, p0, Lcom/a/b/d/lm;->e:I

    invoke-static {p1, v0}, Lcom/a/b/b/cn;->a(II)I

    .line 218
    iget-boolean v0, p0, Lcom/a/b/d/lm;->b:Z

    if-eqz v0, :cond_34

    .line 219
    if-nez p1, :cond_23

    invoke-static {}, Lcom/a/b/d/dw;->d()Lcom/a/b/d/dw;

    move-result-object v0

    :goto_f
    move-object v1, v0

    .line 225
    :goto_10
    iget-boolean v0, p0, Lcom/a/b/d/lm;->d:Z

    if-eqz v0, :cond_44

    iget v0, p0, Lcom/a/b/d/lm;->e:I

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_44

    .line 226
    invoke-static {}, Lcom/a/b/d/dw;->e()Lcom/a/b/d/dw;

    move-result-object v0

    .line 231
    :goto_1e
    invoke-static {v1, v0}, Lcom/a/b/d/yl;->a(Lcom/a/b/d/dw;Lcom/a/b/d/dw;)Lcom/a/b/d/yl;

    move-result-object v0

    return-object v0

    .line 219
    :cond_23
    iget-object v0, p0, Lcom/a/b/d/lm;->a:Lcom/a/b/d/lf;

    invoke-static {v0}, Lcom/a/b/d/lf;->a(Lcom/a/b/d/lf;)Lcom/a/b/d/jl;

    move-result-object v0

    add-int/lit8 v1, p1, -0x1

    invoke-virtual {v0, v1}, Lcom/a/b/d/jl;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/yl;

    iget-object v0, v0, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    goto :goto_f

    .line 221
    :cond_34
    iget-object v0, p0, Lcom/a/b/d/lm;->a:Lcom/a/b/d/lf;

    invoke-static {v0}, Lcom/a/b/d/lf;->a(Lcom/a/b/d/lf;)Lcom/a/b/d/jl;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/a/b/d/jl;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/yl;

    iget-object v0, v0, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    move-object v1, v0

    goto :goto_10

    .line 228
    :cond_44
    iget-object v0, p0, Lcom/a/b/d/lm;->a:Lcom/a/b/d/lf;

    invoke-static {v0}, Lcom/a/b/d/lf;->a(Lcom/a/b/d/lf;)Lcom/a/b/d/jl;

    move-result-object v2

    iget-boolean v0, p0, Lcom/a/b/d/lm;->b:Z

    if-eqz v0, :cond_59

    const/4 v0, 0x0

    :goto_4f
    add-int/2addr v0, p1

    invoke-virtual {v2, v0}, Lcom/a/b/d/jl;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/yl;

    iget-object v0, v0, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    goto :goto_1e

    :cond_59
    const/4 v0, 0x1

    goto :goto_4f
.end method


# virtual methods
.method public final synthetic get(I)Ljava/lang/Object;
    .registers 5

    .prologue
    .line 185
    .line 2215
    iget v0, p0, Lcom/a/b/d/lm;->e:I

    invoke-static {p1, v0}, Lcom/a/b/b/cn;->a(II)I

    .line 2218
    iget-boolean v0, p0, Lcom/a/b/d/lm;->b:Z

    if-eqz v0, :cond_34

    .line 2219
    if-nez p1, :cond_23

    invoke-static {}, Lcom/a/b/d/dw;->d()Lcom/a/b/d/dw;

    move-result-object v0

    :goto_f
    move-object v1, v0

    .line 2225
    :goto_10
    iget-boolean v0, p0, Lcom/a/b/d/lm;->d:Z

    if-eqz v0, :cond_44

    iget v0, p0, Lcom/a/b/d/lm;->e:I

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_44

    .line 2226
    invoke-static {}, Lcom/a/b/d/dw;->e()Lcom/a/b/d/dw;

    move-result-object v0

    .line 2231
    :goto_1e
    invoke-static {v1, v0}, Lcom/a/b/d/yl;->a(Lcom/a/b/d/dw;Lcom/a/b/d/dw;)Lcom/a/b/d/yl;

    move-result-object v0

    .line 185
    return-object v0

    .line 2219
    :cond_23
    iget-object v0, p0, Lcom/a/b/d/lm;->a:Lcom/a/b/d/lf;

    invoke-static {v0}, Lcom/a/b/d/lf;->a(Lcom/a/b/d/lf;)Lcom/a/b/d/jl;

    move-result-object v0

    add-int/lit8 v1, p1, -0x1

    invoke-virtual {v0, v1}, Lcom/a/b/d/jl;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/yl;

    iget-object v0, v0, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    goto :goto_f

    .line 2221
    :cond_34
    iget-object v0, p0, Lcom/a/b/d/lm;->a:Lcom/a/b/d/lf;

    invoke-static {v0}, Lcom/a/b/d/lf;->a(Lcom/a/b/d/lf;)Lcom/a/b/d/jl;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/a/b/d/jl;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/yl;

    iget-object v0, v0, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    move-object v1, v0

    goto :goto_10

    .line 2228
    :cond_44
    iget-object v0, p0, Lcom/a/b/d/lm;->a:Lcom/a/b/d/lf;

    invoke-static {v0}, Lcom/a/b/d/lf;->a(Lcom/a/b/d/lf;)Lcom/a/b/d/jl;

    move-result-object v2

    iget-boolean v0, p0, Lcom/a/b/d/lm;->b:Z

    if-eqz v0, :cond_59

    const/4 v0, 0x0

    :goto_4f
    add-int/2addr v0, p1

    invoke-virtual {v2, v0}, Lcom/a/b/d/jl;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/yl;

    iget-object v0, v0, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    goto :goto_1e

    :cond_59
    const/4 v0, 0x1

    goto :goto_4f
.end method

.method final h_()Z
    .registers 2

    .prologue
    .line 236
    const/4 v0, 0x1

    return v0
.end method

.method public final size()I
    .registers 2

    .prologue
    .line 210
    iget v0, p0, Lcom/a/b/d/lm;->e:I

    return v0
.end method
