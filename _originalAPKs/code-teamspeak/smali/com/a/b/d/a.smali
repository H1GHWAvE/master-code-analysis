.class abstract Lcom/a/b/d/a;
.super Lcom/a/b/d/gs;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/d/bw;
.implements Ljava/io/Serializable;


# annotations
.annotation build Lcom/a/b/a/b;
    b = true
.end annotation


# static fields
.field private static final f:J
    .annotation build Lcom/a/b/a/c;
        a = "Not needed in emulated source."
    .end annotation
.end field


# instance fields
.field transient a:Lcom/a/b/d/a;

.field private transient b:Ljava/util/Map;

.field private transient c:Ljava/util/Set;

.field private transient d:Ljava/util/Set;

.field private transient e:Ljava/util/Set;


# direct methods
.method private constructor <init>(Ljava/util/Map;Lcom/a/b/d/a;)V
    .registers 3

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/a/b/d/gs;-><init>()V

    .line 62
    iput-object p1, p0, Lcom/a/b/d/a;->b:Ljava/util/Map;

    .line 63
    iput-object p2, p0, Lcom/a/b/d/a;->a:Lcom/a/b/d/a;

    .line 64
    return-void
.end method

.method synthetic constructor <init>(Ljava/util/Map;Lcom/a/b/d/a;B)V
    .registers 4

    .prologue
    .line 49
    invoke-direct {p0, p1, p2}, Lcom/a/b/d/a;-><init>(Ljava/util/Map;Lcom/a/b/d/a;)V

    return-void
.end method

.method constructor <init>(Ljava/util/Map;Ljava/util/Map;)V
    .registers 3

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/a/b/d/gs;-><init>()V

    .line 57
    invoke-virtual {p0, p1, p2}, Lcom/a/b/d/a;->a(Ljava/util/Map;Ljava/util/Map;)V

    .line 58
    return-void
.end method

.method static synthetic a(Lcom/a/b/d/a;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 49
    invoke-direct {p0, p1}, Lcom/a/b/d/a;->d(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/Object;Ljava/lang/Object;Z)Ljava/lang/Object;
    .registers 9
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 120
    invoke-virtual {p0, p1}, Lcom/a/b/d/a;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 121
    invoke-virtual {p0, p2}, Lcom/a/b/d/a;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 122
    invoke-virtual {p0, p1}, Lcom/a/b/d/a;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    .line 123
    if-eqz v3, :cond_19

    invoke-virtual {p0, p1}, Lcom/a/b/d/a;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {p2, v0}, Lcom/a/b/b/ce;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_19

    .line 133
    :goto_18
    return-object p2

    .line 126
    :cond_19
    if-eqz p3, :cond_2d

    .line 127
    invoke-virtual {p0}, Lcom/a/b/d/a;->b()Lcom/a/b/d/bw;

    move-result-object v0

    invoke-interface {v0, p2}, Lcom/a/b/d/bw;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 131
    :goto_22
    iget-object v0, p0, Lcom/a/b/d/a;->b:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 132
    invoke-direct {p0, p1, v3, v0, p2}, Lcom/a/b/d/a;->a(Ljava/lang/Object;ZLjava/lang/Object;Ljava/lang/Object;)V

    move-object p2, v0

    .line 133
    goto :goto_18

    .line 129
    :cond_2d
    invoke-virtual {p0, p2}, Lcom/a/b/d/a;->containsValue(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3e

    move v0, v1

    :goto_34
    const-string v4, "value already present: %s"

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p2, v1, v2

    invoke-static {v0, v4, v1}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    goto :goto_22

    :cond_3e
    move v0, v2

    goto :goto_34
.end method

.method static synthetic a(Lcom/a/b/d/a;)Ljava/util/Map;
    .registers 2

    .prologue
    .line 49
    iget-object v0, p0, Lcom/a/b/d/a;->b:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic a(Lcom/a/b/d/a;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .registers 5

    .prologue
    .line 49
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0, p2, p3}, Lcom/a/b/d/a;->a(Ljava/lang/Object;ZLjava/lang/Object;Ljava/lang/Object;)V

    return-void
.end method

.method private a(Ljava/lang/Object;ZLjava/lang/Object;Ljava/lang/Object;)V
    .registers 6

    .prologue
    .line 138
    if-eqz p2, :cond_5

    .line 139
    invoke-direct {p0, p3}, Lcom/a/b/d/a;->e(Ljava/lang/Object;)V

    .line 141
    :cond_5
    iget-object v0, p0, Lcom/a/b/d/a;->a:Lcom/a/b/d/a;

    iget-object v0, v0, Lcom/a/b/d/a;->b:Ljava/util/Map;

    invoke-interface {v0, p4, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 142
    return-void
.end method

.method private b(Lcom/a/b/d/a;)V
    .registers 2

    .prologue
    .line 99
    iput-object p1, p0, Lcom/a/b/d/a;->a:Lcom/a/b/d/a;

    .line 100
    return-void
.end method

.method static synthetic b(Lcom/a/b/d/a;Ljava/lang/Object;)V
    .registers 2

    .prologue
    .line 49
    invoke-direct {p0, p1}, Lcom/a/b/d/a;->e(Ljava/lang/Object;)V

    return-void
.end method

.method private d(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 149
    iget-object v0, p0, Lcom/a/b/d/a;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 150
    invoke-direct {p0, v0}, Lcom/a/b/d/a;->e(Ljava/lang/Object;)V

    .line 151
    return-object v0
.end method

.method private e(Ljava/lang/Object;)V
    .registers 3

    .prologue
    .line 155
    iget-object v0, p0, Lcom/a/b/d/a;->a:Lcom/a/b/d/a;

    iget-object v0, v0, Lcom/a/b/d/a;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 156
    return-void
.end method


# virtual methods
.method a(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 74
    return-object p1
.end method

.method public a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 116
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Lcom/a/b/d/a;->a(Ljava/lang/Object;Ljava/lang/Object;Z)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected final a()Ljava/util/Map;
    .registers 2

    .prologue
    .line 67
    iget-object v0, p0, Lcom/a/b/d/a;->b:Ljava/util/Map;

    return-object v0
.end method

.method final a(Ljava/util/Map;Ljava/util/Map;)V
    .registers 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 89
    iget-object v0, p0, Lcom/a/b/d/a;->b:Ljava/util/Map;

    if-nez v0, :cond_2f

    move v0, v1

    :goto_7
    invoke-static {v0}, Lcom/a/b/b/cn;->b(Z)V

    .line 90
    iget-object v0, p0, Lcom/a/b/d/a;->a:Lcom/a/b/d/a;

    if-nez v0, :cond_31

    move v0, v1

    :goto_f
    invoke-static {v0}, Lcom/a/b/b/cn;->b(Z)V

    .line 91
    invoke-interface {p1}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    invoke-static {v0}, Lcom/a/b/b/cn;->a(Z)V

    .line 92
    invoke-interface {p2}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    invoke-static {v0}, Lcom/a/b/b/cn;->a(Z)V

    .line 93
    if-eq p1, p2, :cond_33

    :goto_22
    invoke-static {v1}, Lcom/a/b/b/cn;->a(Z)V

    .line 94
    iput-object p1, p0, Lcom/a/b/d/a;->b:Ljava/util/Map;

    .line 95
    new-instance v0, Lcom/a/b/d/f;

    invoke-direct {v0, p2, p0, v2}, Lcom/a/b/d/f;-><init>(Ljava/util/Map;Lcom/a/b/d/a;B)V

    iput-object v0, p0, Lcom/a/b/d/a;->a:Lcom/a/b/d/a;

    .line 96
    return-void

    :cond_2f
    move v0, v2

    .line 89
    goto :goto_7

    :cond_31
    move v0, v2

    .line 90
    goto :goto_f

    :cond_33
    move v1, v2

    .line 93
    goto :goto_22
.end method

.method public b()Lcom/a/b/d/bw;
    .registers 2

    .prologue
    .line 175
    iget-object v0, p0, Lcom/a/b/d/a;->a:Lcom/a/b/d/a;

    return-object v0
.end method

.method b(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 81
    return-object p1
.end method

.method public clear()V
    .registers 2

    .prologue
    .line 167
    iget-object v0, p0, Lcom/a/b/d/a;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 168
    iget-object v0, p0, Lcom/a/b/d/a;->a:Lcom/a/b/d/a;

    iget-object v0, v0, Lcom/a/b/d/a;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 169
    return-void
.end method

.method public containsValue(Ljava/lang/Object;)Z
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 105
    iget-object v0, p0, Lcom/a/b/d/a;->a:Lcom/a/b/d/a;

    invoke-virtual {v0, p1}, Lcom/a/b/d/a;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public entrySet()Ljava/util/Set;
    .registers 3

    .prologue
    .line 253
    iget-object v0, p0, Lcom/a/b/d/a;->e:Ljava/util/Set;

    .line 254
    if-nez v0, :cond_c

    new-instance v0, Lcom/a/b/d/c;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/a/b/d/c;-><init>(Lcom/a/b/d/a;B)V

    iput-object v0, p0, Lcom/a/b/d/a;->e:Ljava/util/Set;

    :cond_c
    return-object v0
.end method

.method public j_()Ljava/util/Set;
    .registers 3

    .prologue
    .line 222
    iget-object v0, p0, Lcom/a/b/d/a;->d:Ljava/util/Set;

    .line 223
    if-nez v0, :cond_c

    new-instance v0, Lcom/a/b/d/h;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/a/b/d/h;-><init>(Lcom/a/b/d/a;B)V

    iput-object v0, p0, Lcom/a/b/d/a;->d:Ljava/util/Set;

    :cond_c
    return-object v0
.end method

.method protected bridge synthetic k_()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 48
    .line 1067
    iget-object v0, p0, Lcom/a/b/d/a;->b:Ljava/util/Map;

    .line 48
    return-object v0
.end method

.method public keySet()Ljava/util/Set;
    .registers 3

    .prologue
    .line 181
    iget-object v0, p0, Lcom/a/b/d/a;->c:Ljava/util/Set;

    .line 182
    if-nez v0, :cond_c

    new-instance v0, Lcom/a/b/d/g;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/a/b/d/g;-><init>(Lcom/a/b/d/a;B)V

    iput-object v0, p0, Lcom/a/b/d/a;->c:Ljava/util/Set;

    :cond_c
    return-object v0
.end method

.method public put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 111
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/a/b/d/a;->a(Ljava/lang/Object;Ljava/lang/Object;Z)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public putAll(Ljava/util/Map;)V
    .registers 5

    .prologue
    .line 161
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_20

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 162
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v2, v0}, Lcom/a/b/d/a;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_8

    .line 164
    :cond_20
    return-void
.end method

.method public remove(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 145
    invoke-virtual {p0, p1}, Lcom/a/b/d/a;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-direct {p0, p1}, Lcom/a/b/d/a;->d(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    :goto_a
    return-object v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public synthetic values()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 48
    invoke-virtual {p0}, Lcom/a/b/d/a;->j_()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method
