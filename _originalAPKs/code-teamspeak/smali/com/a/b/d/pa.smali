.class final Lcom/a/b/d/pa;
.super Ljava/util/AbstractList;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/util/RandomAccess;


# static fields
.field private static final c:J


# instance fields
.field final a:Ljava/lang/Object;

.field final b:[Ljava/lang/Object;


# direct methods
.method constructor <init>(Ljava/lang/Object;[Ljava/lang/Object;)V
    .registers 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 321
    invoke-direct {p0}, Ljava/util/AbstractList;-><init>()V

    .line 322
    iput-object p1, p0, Lcom/a/b/d/pa;->a:Ljava/lang/Object;

    .line 323
    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    iput-object v0, p0, Lcom/a/b/d/pa;->b:[Ljava/lang/Object;

    .line 324
    return-void
.end method


# virtual methods
.method public final get(I)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 330
    invoke-virtual {p0}, Lcom/a/b/d/pa;->size()I

    move-result v0

    invoke-static {p1, v0}, Lcom/a/b/b/cn;->a(II)I

    .line 331
    if-nez p1, :cond_c

    iget-object v0, p0, Lcom/a/b/d/pa;->a:Ljava/lang/Object;

    :goto_b
    return-object v0

    :cond_c
    iget-object v0, p0, Lcom/a/b/d/pa;->b:[Ljava/lang/Object;

    add-int/lit8 v1, p1, -0x1

    aget-object v0, v0, v1

    goto :goto_b
.end method

.method public final size()I
    .registers 2

    .prologue
    .line 326
    iget-object v0, p0, Lcom/a/b/d/pa;->b:[Ljava/lang/Object;

    array-length v0, v0

    add-int/lit8 v0, v0, 0x1

    return v0
.end method
