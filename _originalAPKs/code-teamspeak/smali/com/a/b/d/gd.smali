.class public abstract Lcom/a/b/d/gd;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Iterable;


# annotations
.annotation build Lcom/a/b/a/b;
    b = true
.end annotation


# instance fields
.field public final c:Ljava/lang/Iterable;


# direct methods
.method protected constructor <init>()V
    .registers 1

    .prologue
    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    iput-object p0, p0, Lcom/a/b/d/gd;->c:Ljava/lang/Iterable;

    .line 81
    return-void
.end method

.method constructor <init>(Ljava/lang/Iterable;)V
    .registers 3

    .prologue
    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 84
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    iput-object v0, p0, Lcom/a/b/d/gd;->c:Ljava/lang/Iterable;

    .line 85
    return-void
.end method

.method private a()I
    .registers 3

    .prologue
    .line 137
    iget-object v0, p0, Lcom/a/b/d/gd;->c:Ljava/lang/Iterable;

    .line 2108
    instance-of v1, v0, Ljava/util/Collection;

    if-eqz v1, :cond_d

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v0

    :goto_c
    return v0

    :cond_d
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/nj;->b(Ljava/util/Iterator;)I

    move-result v0

    goto :goto_c
.end method

.method private a(I)Lcom/a/b/d/gd;
    .registers 5
    .annotation runtime Ljavax/annotation/CheckReturnValue;
    .end annotation

    .prologue
    .line 341
    iget-object v0, p0, Lcom/a/b/d/gd;->c:Ljava/lang/Iterable;

    .line 3845
    invoke-static {v0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 3846
    if-ltz p1, :cond_1e

    const/4 v1, 0x1

    :goto_8
    const-string v2, "number to skip cannot be negative"

    invoke-static {v1, v2}, Lcom/a/b/b/cn;->a(ZLjava/lang/Object;)V

    .line 3848
    instance-of v1, v0, Ljava/util/List;

    if-eqz v1, :cond_20

    .line 3849
    check-cast v0, Ljava/util/List;

    .line 3850
    new-instance v1, Lcom/a/b/d/ng;

    invoke-direct {v1, v0, p1}, Lcom/a/b/d/ng;-><init>(Ljava/util/List;I)V

    move-object v0, v1

    .line 341
    :goto_19
    invoke-static {v0}, Lcom/a/b/d/gd;->a(Ljava/lang/Iterable;)Lcom/a/b/d/gd;

    move-result-object v0

    return-object v0

    .line 3846
    :cond_1e
    const/4 v1, 0x0

    goto :goto_8

    .line 3860
    :cond_20
    new-instance v1, Lcom/a/b/d/ms;

    invoke-direct {v1, v0, p1}, Lcom/a/b/d/ms;-><init>(Ljava/lang/Iterable;I)V

    move-object v0, v1

    goto :goto_19
.end method

.method private a(Lcom/a/b/b/bj;)Lcom/a/b/d/gd;
    .registers 3

    .prologue
    .line 248
    iget-object v0, p0, Lcom/a/b/d/gd;->c:Ljava/lang/Iterable;

    invoke-static {v0, p1}, Lcom/a/b/d/mq;->a(Ljava/lang/Iterable;Lcom/a/b/b/bj;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/gd;->a(Ljava/lang/Iterable;)Lcom/a/b/d/gd;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/a/b/d/gd;)Lcom/a/b/d/gd;
    .registers 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 111
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/gd;

    return-object v0
.end method

.method public static a(Ljava/lang/Iterable;)Lcom/a/b/d/gd;
    .registers 2

    .prologue
    .line 92
    instance-of v0, p0, Lcom/a/b/d/gd;

    if-eqz v0, :cond_7

    check-cast p0, Lcom/a/b/d/gd;

    :goto_6
    return-object p0

    :cond_7
    new-instance v0, Lcom/a/b/d/ge;

    invoke-direct {v0, p0, p0}, Lcom/a/b/d/ge;-><init>(Ljava/lang/Iterable;Ljava/lang/Iterable;)V

    move-object p0, v0

    goto :goto_6
.end method

.method private static a([Ljava/lang/Object;)Lcom/a/b/d/gd;
    .registers 2
    .annotation build Lcom/a/b/a/a;
    .end annotation

    .prologue
    .line 121
    invoke-static {p0}, Lcom/a/b/d/ov;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/gd;->a(Ljava/lang/Iterable;)Lcom/a/b/d/gd;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/util/Comparator;)Lcom/a/b/d/jl;
    .registers 4

    .prologue
    .line 386
    invoke-static {p1}, Lcom/a/b/d/yd;->a(Ljava/util/Comparator;)Lcom/a/b/d/yd;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/d/gd;->c:Ljava/lang/Iterable;

    invoke-virtual {v0, v1}, Lcom/a/b/d/yd;->b(Ljava/lang/Iterable;)Lcom/a/b/d/jl;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/a/b/b/bv;)Ljava/lang/String;
    .registers 3
    .annotation build Lcom/a/b/a/a;
    .end annotation

    .prologue
    .line 505
    invoke-virtual {p1, p0}, Lcom/a/b/b/bv;->a(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/util/Collection;)Ljava/util/Collection;
    .registers 4

    .prologue
    .line 486
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 487
    iget-object v0, p0, Lcom/a/b/d/gd;->c:Ljava/lang/Iterable;

    instance-of v0, v0, Ljava/util/Collection;

    if-eqz v0, :cond_13

    .line 488
    iget-object v0, p0, Lcom/a/b/d/gd;->c:Ljava/lang/Iterable;

    invoke-static {v0}, Lcom/a/b/d/cm;->a(Ljava/lang/Iterable;)Ljava/util/Collection;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    .line 494
    :cond_12
    return-object p1

    .line 490
    :cond_13
    iget-object v0, p0, Lcom/a/b/d/gd;->c:Ljava/lang/Iterable;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_19
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_12

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 491
    invoke-interface {p1, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_19
.end method

.method private a(Ljava/lang/Object;)Z
    .registers 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 145
    iget-object v0, p0, Lcom/a/b/d/gd;->c:Ljava/lang/Iterable;

    .line 2118
    instance-of v1, v0, Ljava/util/Collection;

    if-eqz v1, :cond_d

    .line 2119
    check-cast v0, Ljava/util/Collection;

    .line 2120
    invoke-static {v0, p1}, Lcom/a/b/d/cm;->a(Ljava/util/Collection;Ljava/lang/Object;)Z

    move-result v0

    :goto_c
    return v0

    .line 2122
    :cond_d
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/a/b/d/nj;->a(Ljava/util/Iterator;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_c
.end method

.method private b()Lcom/a/b/d/gd;
    .registers 2
    .annotation runtime Ljavax/annotation/CheckReturnValue;
    .end annotation

    .prologue
    .line 163
    iget-object v0, p0, Lcom/a/b/d/gd;->c:Ljava/lang/Iterable;

    invoke-static {v0}, Lcom/a/b/d/mq;->d(Ljava/lang/Iterable;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/gd;->a(Ljava/lang/Iterable;)Lcom/a/b/d/gd;

    move-result-object v0

    return-object v0
.end method

.method private b(I)Lcom/a/b/d/gd;
    .registers 5
    .annotation runtime Ljavax/annotation/CheckReturnValue;
    .end annotation

    .prologue
    .line 356
    iget-object v1, p0, Lcom/a/b/d/gd;->c:Ljava/lang/Iterable;

    .line 3911
    invoke-static {v1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 3912
    if-ltz p1, :cond_17

    const/4 v0, 0x1

    :goto_8
    const-string v2, "limit is negative"

    invoke-static {v0, v2}, Lcom/a/b/b/cn;->a(ZLjava/lang/Object;)V

    .line 3913
    new-instance v0, Lcom/a/b/d/mu;

    invoke-direct {v0, v1, p1}, Lcom/a/b/d/mu;-><init>(Ljava/lang/Iterable;I)V

    .line 356
    invoke-static {v0}, Lcom/a/b/d/gd;->a(Ljava/lang/Iterable;)Lcom/a/b/d/gd;

    move-result-object v0

    return-object v0

    .line 3912
    :cond_17
    const/4 v0, 0x0

    goto :goto_8
.end method

.method private b(Lcom/a/b/b/bj;)Lcom/a/b/d/gd;
    .registers 3

    .prologue
    .line 264
    .line 3248
    iget-object v0, p0, Lcom/a/b/d/gd;->c:Ljava/lang/Iterable;

    invoke-static {v0, p1}, Lcom/a/b/d/mq;->a(Ljava/lang/Iterable;Lcom/a/b/b/bj;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/gd;->a(Ljava/lang/Iterable;)Lcom/a/b/d/gd;

    move-result-object v0

    .line 264
    invoke-static {v0}, Lcom/a/b/d/mq;->e(Ljava/lang/Iterable;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/gd;->a(Ljava/lang/Iterable;)Lcom/a/b/d/gd;

    move-result-object v0

    return-object v0
.end method

.method private b(Ljava/lang/Iterable;)Lcom/a/b/d/gd;
    .registers 3
    .annotation build Lcom/a/b/a/a;
    .end annotation

    .annotation runtime Ljavax/annotation/CheckReturnValue;
    .end annotation

    .prologue
    .line 178
    iget-object v0, p0, Lcom/a/b/d/gd;->c:Ljava/lang/Iterable;

    invoke-static {v0, p1}, Lcom/a/b/d/mq;->a(Ljava/lang/Iterable;Ljava/lang/Iterable;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/gd;->a(Ljava/lang/Iterable;)Lcom/a/b/d/gd;

    move-result-object v0

    return-object v0
.end method

.method private varargs b([Ljava/lang/Object;)Lcom/a/b/d/gd;
    .registers 4
    .annotation build Lcom/a/b/a/a;
    .end annotation

    .annotation runtime Ljavax/annotation/CheckReturnValue;
    .end annotation

    .prologue
    .line 190
    iget-object v0, p0, Lcom/a/b/d/gd;->c:Ljava/lang/Iterable;

    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/a/b/d/mq;->a(Ljava/lang/Iterable;Ljava/lang/Iterable;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/gd;->a(Ljava/lang/Iterable;)Lcom/a/b/d/gd;

    move-result-object v0

    return-object v0
.end method

.method private b(Ljava/util/Comparator;)Lcom/a/b/d/me;
    .registers 3

    .prologue
    .line 410
    iget-object v0, p0, Lcom/a/b/d/gd;->c:Ljava/lang/Iterable;

    invoke-static {p1, v0}, Lcom/a/b/d/me;->a(Ljava/util/Comparator;Ljava/lang/Iterable;)Lcom/a/b/d/me;

    move-result-object v0

    return-object v0
.end method

.method private b(Lcom/a/b/b/co;)Z
    .registers 3

    .prologue
    .line 217
    iget-object v0, p0, Lcom/a/b/d/gd;->c:Ljava/lang/Iterable;

    invoke-static {v0, p1}, Lcom/a/b/d/mq;->d(Ljava/lang/Iterable;Lcom/a/b/b/co;)Z

    move-result v0

    return v0
.end method

.method private b(Ljava/lang/Class;)[Ljava/lang/Object;
    .registers 3
    .annotation build Lcom/a/b/a/c;
        a = "Array.newArray(Class, int)"
    .end annotation

    .prologue
    .line 474
    iget-object v0, p0, Lcom/a/b/d/gd;->c:Ljava/lang/Iterable;

    invoke-static {v0, p1}, Lcom/a/b/d/mq;->a(Ljava/lang/Iterable;Ljava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method private c()Lcom/a/b/b/ci;
    .registers 3

    .prologue
    .line 275
    iget-object v0, p0, Lcom/a/b/d/gd;->c:Ljava/lang/Iterable;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 276
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_15

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/b/ci;->b(Ljava/lang/Object;)Lcom/a/b/b/ci;

    move-result-object v0

    :goto_14
    return-object v0

    :cond_15
    invoke-static {}, Lcom/a/b/b/ci;->f()Lcom/a/b/b/ci;

    move-result-object v0

    goto :goto_14
.end method

.method private c(Lcom/a/b/b/bj;)Lcom/a/b/d/jt;
    .registers 3

    .prologue
    .line 424
    iget-object v0, p0, Lcom/a/b/d/gd;->c:Ljava/lang/Iterable;

    invoke-static {v0, p1}, Lcom/a/b/d/sz;->a(Ljava/lang/Iterable;Lcom/a/b/b/bj;)Lcom/a/b/d/jt;

    move-result-object v0

    return-object v0
.end method

.method private c(I)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 517
    iget-object v0, p0, Lcom/a/b/d/gd;->c:Ljava/lang/Iterable;

    .line 4727
    invoke-static {v0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 4728
    instance-of v1, v0, Ljava/util/List;

    if-eqz v1, :cond_10

    check-cast v0, Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    :goto_f
    return-object v0

    :cond_10
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/a/b/d/nj;->c(Ljava/util/Iterator;I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_f
.end method

.method private c(Lcom/a/b/b/co;)Z
    .registers 3

    .prologue
    .line 225
    iget-object v0, p0, Lcom/a/b/d/gd;->c:Ljava/lang/Iterable;

    invoke-static {v0, p1}, Lcom/a/b/d/mq;->e(Ljava/lang/Iterable;Lcom/a/b/b/co;)Z

    move-result v0

    return v0
.end method

.method private d()Lcom/a/b/b/ci;
    .registers 4

    .prologue
    .line 292
    iget-object v0, p0, Lcom/a/b/d/gd;->c:Ljava/lang/Iterable;

    instance-of v0, v0, Ljava/util/List;

    if-eqz v0, :cond_24

    .line 293
    iget-object v0, p0, Lcom/a/b/d/gd;->c:Ljava/lang/Iterable;

    check-cast v0, Ljava/util/List;

    .line 294
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_15

    .line 295
    invoke-static {}, Lcom/a/b/b/ci;->f()Lcom/a/b/b/ci;

    move-result-object v0

    .line 317
    :goto_14
    return-object v0

    .line 297
    :cond_15
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/b/ci;->b(Ljava/lang/Object;)Lcom/a/b/b/ci;

    move-result-object v0

    goto :goto_14

    .line 299
    :cond_24
    iget-object v0, p0, Lcom/a/b/d/gd;->c:Ljava/lang/Iterable;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 300
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_35

    .line 301
    invoke-static {}, Lcom/a/b/b/ci;->f()Lcom/a/b/b/ci;

    move-result-object v0

    goto :goto_14

    .line 309
    :cond_35
    iget-object v1, p0, Lcom/a/b/d/gd;->c:Ljava/lang/Iterable;

    instance-of v1, v1, Ljava/util/SortedSet;

    if-eqz v1, :cond_48

    .line 310
    iget-object v0, p0, Lcom/a/b/d/gd;->c:Ljava/lang/Iterable;

    check-cast v0, Ljava/util/SortedSet;

    .line 311
    invoke-interface {v0}, Ljava/util/SortedSet;->last()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/b/ci;->b(Ljava/lang/Object;)Lcom/a/b/b/ci;

    move-result-object v0

    goto :goto_14

    .line 315
    :cond_48
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 316
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_48

    .line 317
    invoke-static {v1}, Lcom/a/b/b/ci;->b(Ljava/lang/Object;)Lcom/a/b/b/ci;

    move-result-object v0

    goto :goto_14
.end method

.method private d(Lcom/a/b/b/co;)Lcom/a/b/b/ci;
    .registers 3

    .prologue
    .line 236
    iget-object v0, p0, Lcom/a/b/d/gd;->c:Ljava/lang/Iterable;

    .line 2675
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/a/b/d/nj;->f(Ljava/util/Iterator;Lcom/a/b/b/co;)Lcom/a/b/b/ci;

    move-result-object v0

    .line 236
    return-object v0
.end method

.method private d(Lcom/a/b/b/bj;)Lcom/a/b/d/jr;
    .registers 6

    .prologue
    .line 446
    iget-object v0, p0, Lcom/a/b/d/gd;->c:Ljava/lang/Iterable;

    .line 4455
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 4503
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 4504
    invoke-static {}, Lcom/a/b/d/jr;->c()Lcom/a/b/d/js;

    move-result-object v1

    .line 4506
    :goto_d
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_22

    .line 4507
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 4508
    invoke-static {v2, v0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 4509
    invoke-interface {p1, v2}, Lcom/a/b/b/bj;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v1, v3, v2}, Lcom/a/b/d/js;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/js;

    goto :goto_d

    .line 4511
    :cond_22
    invoke-virtual {v1}, Lcom/a/b/d/js;->a()Lcom/a/b/d/jr;

    move-result-object v0

    .line 446
    return-object v0
.end method

.method private e(Lcom/a/b/b/bj;)Lcom/a/b/d/jt;
    .registers 3

    .prologue
    .line 462
    iget-object v0, p0, Lcom/a/b/d/gd;->c:Ljava/lang/Iterable;

    invoke-static {v0, p1}, Lcom/a/b/d/sz;->b(Ljava/lang/Iterable;Lcom/a/b/b/bj;)Lcom/a/b/d/jt;

    move-result-object v0

    return-object v0
.end method

.method private e()Z
    .registers 2

    .prologue
    .line 363
    iget-object v0, p0, Lcom/a/b/d/gd;->c:Ljava/lang/Iterable;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method private f()Lcom/a/b/d/jl;
    .registers 2

    .prologue
    .line 373
    iget-object v0, p0, Lcom/a/b/d/gd;->c:Ljava/lang/Iterable;

    invoke-static {v0}, Lcom/a/b/d/jl;->a(Ljava/lang/Iterable;)Lcom/a/b/d/jl;

    move-result-object v0

    return-object v0
.end method

.method private g()Lcom/a/b/d/lo;
    .registers 2

    .prologue
    .line 396
    iget-object v0, p0, Lcom/a/b/d/gd;->c:Ljava/lang/Iterable;

    invoke-static {v0}, Lcom/a/b/d/lo;->a(Ljava/lang/Iterable;)Lcom/a/b/d/lo;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/a/b/b/co;)Lcom/a/b/d/gd;
    .registers 3
    .annotation runtime Ljavax/annotation/CheckReturnValue;
    .end annotation

    .prologue
    .line 199
    iget-object v0, p0, Lcom/a/b/d/gd;->c:Ljava/lang/Iterable;

    invoke-static {v0, p1}, Lcom/a/b/d/mq;->c(Ljava/lang/Iterable;Lcom/a/b/b/co;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/gd;->a(Ljava/lang/Iterable;)Lcom/a/b/d/gd;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Class;)Lcom/a/b/d/gd;
    .registers 4
    .annotation build Lcom/a/b/a/c;
        a = "Class.isInstance"
    .end annotation

    .annotation runtime Ljavax/annotation/CheckReturnValue;
    .end annotation

    .prologue
    .line 210
    iget-object v0, p0, Lcom/a/b/d/gd;->c:Ljava/lang/Iterable;

    .line 2608
    invoke-static {v0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2609
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2610
    new-instance v1, Lcom/a/b/d/ne;

    invoke-direct {v1, v0, p1}, Lcom/a/b/d/ne;-><init>(Ljava/lang/Iterable;Ljava/lang/Class;)V

    .line 210
    invoke-static {v1}, Lcom/a/b/d/gd;->a(Ljava/lang/Iterable;)Lcom/a/b/d/gd;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .registers 2

    .prologue
    .line 130
    iget-object v0, p0, Lcom/a/b/d/gd;->c:Ljava/lang/Iterable;

    invoke-static {v0}, Lcom/a/b/d/mq;->a(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
