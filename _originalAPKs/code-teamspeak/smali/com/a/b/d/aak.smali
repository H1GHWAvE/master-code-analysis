.class final Lcom/a/b/d/aak;
.super Lcom/a/b/d/aam;
.source "SourceFile"

# interfaces
.implements Ljava/util/NavigableSet;


# annotations
.annotation build Lcom/a/b/a/c;
    a = "NavigableSet"
.end annotation


# direct methods
.method constructor <init>(Ljava/util/NavigableSet;Lcom/a/b/b/co;)V
    .registers 3

    .prologue
    .line 926
    invoke-direct {p0, p1, p2}, Lcom/a/b/d/aam;-><init>(Ljava/util/SortedSet;Lcom/a/b/b/co;)V

    .line 927
    return-void
.end method

.method private a()Ljava/util/NavigableSet;
    .registers 2

    .prologue
    .line 930
    iget-object v0, p0, Lcom/a/b/d/aak;->a:Ljava/util/Collection;

    check-cast v0, Ljava/util/NavigableSet;

    return-object v0
.end method


# virtual methods
.method public final ceiling(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 947
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/a/b/d/aak;->tailSet(Ljava/lang/Object;Z)Ljava/util/NavigableSet;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/mq;->f(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final descendingIterator()Ljava/util/Iterator;
    .registers 3

    .prologue
    .line 972
    .line 4930
    iget-object v0, p0, Lcom/a/b/d/aak;->a:Ljava/util/Collection;

    check-cast v0, Ljava/util/NavigableSet;

    .line 972
    invoke-interface {v0}, Ljava/util/NavigableSet;->descendingIterator()Ljava/util/Iterator;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/d/aak;->b:Lcom/a/b/b/co;

    invoke-static {v0, v1}, Lcom/a/b/d/nj;->b(Ljava/util/Iterator;Lcom/a/b/b/co;)Lcom/a/b/d/agi;

    move-result-object v0

    return-object v0
.end method

.method public final descendingSet()Ljava/util/NavigableSet;
    .registers 3

    .prologue
    .line 967
    .line 3930
    iget-object v0, p0, Lcom/a/b/d/aak;->a:Ljava/util/Collection;

    check-cast v0, Ljava/util/NavigableSet;

    .line 967
    invoke-interface {v0}, Ljava/util/NavigableSet;->descendingSet()Ljava/util/NavigableSet;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/d/aak;->b:Lcom/a/b/b/co;

    invoke-static {v0, v1}, Lcom/a/b/d/aad;->a(Ljava/util/NavigableSet;Lcom/a/b/b/co;)Ljava/util/NavigableSet;

    move-result-object v0

    return-object v0
.end method

.method public final floor(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 942
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/a/b/d/aak;->headSet(Ljava/lang/Object;Z)Ljava/util/NavigableSet;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/NavigableSet;->descendingIterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/a/b/d/nj;->d(Ljava/util/Iterator;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final headSet(Ljava/lang/Object;Z)Ljava/util/NavigableSet;
    .registers 5

    .prologue
    .line 989
    .line 6930
    iget-object v0, p0, Lcom/a/b/d/aak;->a:Ljava/util/Collection;

    check-cast v0, Ljava/util/NavigableSet;

    .line 989
    invoke-interface {v0, p1, p2}, Ljava/util/NavigableSet;->headSet(Ljava/lang/Object;Z)Ljava/util/NavigableSet;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/d/aak;->b:Lcom/a/b/b/co;

    invoke-static {v0, v1}, Lcom/a/b/d/aad;->a(Ljava/util/NavigableSet;Lcom/a/b/b/co;)Ljava/util/NavigableSet;

    move-result-object v0

    return-object v0
.end method

.method public final higher(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 952
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/a/b/d/aak;->tailSet(Ljava/lang/Object;Z)Ljava/util/NavigableSet;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/mq;->f(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final last()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 977
    invoke-virtual {p0}, Lcom/a/b/d/aak;->descendingIterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final lower(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 936
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/a/b/d/aak;->headSet(Ljava/lang/Object;Z)Ljava/util/NavigableSet;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/NavigableSet;->descendingIterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/a/b/d/nj;->d(Ljava/util/Iterator;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final pollFirst()Ljava/lang/Object;
    .registers 3

    .prologue
    .line 957
    .line 1930
    iget-object v0, p0, Lcom/a/b/d/aak;->a:Ljava/util/Collection;

    check-cast v0, Ljava/util/NavigableSet;

    .line 957
    iget-object v1, p0, Lcom/a/b/d/aak;->b:Lcom/a/b/b/co;

    invoke-static {v0, v1}, Lcom/a/b/d/mq;->b(Ljava/lang/Iterable;Lcom/a/b/b/co;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final pollLast()Ljava/lang/Object;
    .registers 3

    .prologue
    .line 962
    .line 2930
    iget-object v0, p0, Lcom/a/b/d/aak;->a:Ljava/util/Collection;

    check-cast v0, Ljava/util/NavigableSet;

    .line 962
    invoke-interface {v0}, Ljava/util/NavigableSet;->descendingSet()Ljava/util/NavigableSet;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/d/aak;->b:Lcom/a/b/b/co;

    invoke-static {v0, v1}, Lcom/a/b/d/mq;->b(Ljava/lang/Iterable;Lcom/a/b/b/co;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final subSet(Ljava/lang/Object;ZLjava/lang/Object;Z)Ljava/util/NavigableSet;
    .registers 7

    .prologue
    .line 983
    .line 5930
    iget-object v0, p0, Lcom/a/b/d/aak;->a:Ljava/util/Collection;

    check-cast v0, Ljava/util/NavigableSet;

    .line 983
    invoke-interface {v0, p1, p2, p3, p4}, Ljava/util/NavigableSet;->subSet(Ljava/lang/Object;ZLjava/lang/Object;Z)Ljava/util/NavigableSet;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/d/aak;->b:Lcom/a/b/b/co;

    invoke-static {v0, v1}, Lcom/a/b/d/aad;->a(Ljava/util/NavigableSet;Lcom/a/b/b/co;)Ljava/util/NavigableSet;

    move-result-object v0

    return-object v0
.end method

.method public final tailSet(Ljava/lang/Object;Z)Ljava/util/NavigableSet;
    .registers 5

    .prologue
    .line 994
    .line 7930
    iget-object v0, p0, Lcom/a/b/d/aak;->a:Ljava/util/Collection;

    check-cast v0, Ljava/util/NavigableSet;

    .line 994
    invoke-interface {v0, p1, p2}, Ljava/util/NavigableSet;->tailSet(Ljava/lang/Object;Z)Ljava/util/NavigableSet;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/d/aak;->b:Lcom/a/b/b/co;

    invoke-static {v0, v1}, Lcom/a/b/d/aad;->a(Ljava/util/NavigableSet;Lcom/a/b/b/co;)Ljava/util/NavigableSet;

    move-result-object v0

    return-object v0
.end method
