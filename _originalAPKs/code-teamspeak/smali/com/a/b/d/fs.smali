.class final Lcom/a/b/d/fs;
.super Lcom/a/b/d/fi;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/d/gc;


# annotations
.annotation build Lcom/a/b/a/b;
.end annotation


# direct methods
.method constructor <init>(Lcom/a/b/d/aac;Lcom/a/b/b/co;)V
    .registers 3

    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, Lcom/a/b/d/fi;-><init>(Lcom/a/b/d/vi;Lcom/a/b/b/co;)V

    .line 36
    return-void
.end method

.method private e()Ljava/util/Set;
    .registers 3

    .prologue
    .line 60
    .line 1040
    iget-object v0, p0, Lcom/a/b/d/fs;->a:Lcom/a/b/d/vi;

    check-cast v0, Lcom/a/b/d/aac;

    .line 60
    invoke-interface {v0}, Lcom/a/b/d/aac;->u()Ljava/util/Set;

    move-result-object v0

    .line 1062
    iget-object v1, p0, Lcom/a/b/d/fi;->b:Lcom/a/b/b/co;

    .line 60
    invoke-static {v0, v1}, Lcom/a/b/d/aad;->a(Ljava/util/Set;Lcom/a/b/b/co;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic a()Lcom/a/b/d/vi;
    .registers 2

    .prologue
    .line 30
    .line 4040
    iget-object v0, p0, Lcom/a/b/d/fs;->a:Lcom/a/b/d/vi;

    check-cast v0, Lcom/a/b/d/aac;

    .line 30
    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Ljava/util/Set;
    .registers 3

    .prologue
    .line 45
    invoke-super {p0, p1}, Lcom/a/b/d/fi;->c(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    return-object v0
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/Set;
    .registers 4

    .prologue
    .line 55
    invoke-super {p0, p1, p2}, Lcom/a/b/d/fi;->b(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    return-object v0
.end method

.method public final synthetic b(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/Collection;
    .registers 4

    .prologue
    .line 30
    invoke-virtual {p0, p1, p2}, Lcom/a/b/d/fs;->a(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/Object;)Ljava/util/Set;
    .registers 3

    .prologue
    .line 50
    invoke-super {p0, p1}, Lcom/a/b/d/fi;->d(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    return-object v0
.end method

.method public final synthetic c(Ljava/lang/Object;)Ljava/util/Collection;
    .registers 3

    .prologue
    .line 30
    invoke-virtual {p0, p1}, Lcom/a/b/d/fs;->a(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final d()Lcom/a/b/d/aac;
    .registers 2

    .prologue
    .line 40
    iget-object v0, p0, Lcom/a/b/d/fs;->a:Lcom/a/b/d/vi;

    check-cast v0, Lcom/a/b/d/aac;

    return-object v0
.end method

.method public final synthetic d(Ljava/lang/Object;)Ljava/util/Collection;
    .registers 3

    .prologue
    .line 30
    invoke-virtual {p0, p1}, Lcom/a/b/d/fs;->b(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic k()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 30
    invoke-virtual {p0}, Lcom/a/b/d/fs;->u()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method final synthetic o()Ljava/util/Collection;
    .registers 3

    .prologue
    .line 30
    .line 3040
    iget-object v0, p0, Lcom/a/b/d/fs;->a:Lcom/a/b/d/vi;

    check-cast v0, Lcom/a/b/d/aac;

    .line 2060
    invoke-interface {v0}, Lcom/a/b/d/aac;->u()Ljava/util/Set;

    move-result-object v0

    .line 3062
    iget-object v1, p0, Lcom/a/b/d/fi;->b:Lcom/a/b/b/co;

    .line 2060
    invoke-static {v0, v1}, Lcom/a/b/d/aad;->a(Ljava/util/Set;Lcom/a/b/b/co;)Ljava/util/Set;

    move-result-object v0

    .line 30
    return-object v0
.end method

.method public final u()Ljava/util/Set;
    .registers 2

    .prologue
    .line 65
    invoke-super {p0}, Lcom/a/b/d/fi;->k()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    return-object v0
.end method
