.class final Lcom/a/b/d/yj;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/b;
    b = true
.end annotation


# direct methods
.method private constructor <init>()V
    .registers 1

    .prologue
    .line 100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Lcom/a/b/d/ql;)Lcom/a/b/d/ql;
    .registers 2

    .prologue
    .line 68
    .line 1265
    sget-object v0, Lcom/a/b/d/sh;->c:Lcom/a/b/d/sh;

    invoke-virtual {p0, v0}, Lcom/a/b/d/ql;->a(Lcom/a/b/d/sh;)Lcom/a/b/d/ql;

    move-result-object v0

    .line 68
    return-object v0
.end method

.method private static a(Ljava/util/Map;)Ljava/util/Set;
    .registers 2

    .prologue
    .line 58
    invoke-static {p0}, Ljava/util/Collections;->newSetFromMap(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/util/SortedMap;Lcom/a/b/b/co;)Ljava/util/SortedMap;
    .registers 3

    .prologue
    .line 95
    instance-of v0, p0, Ljava/util/NavigableMap;

    if-eqz v0, :cond_b

    check-cast p0, Ljava/util/NavigableMap;

    invoke-static {p0, p1}, Lcom/a/b/d/sz;->a(Ljava/util/NavigableMap;Lcom/a/b/b/co;)Ljava/util/NavigableMap;

    move-result-object v0

    :goto_a
    return-object v0

    :cond_b
    invoke-static {p0, p1}, Lcom/a/b/d/sz;->a(Ljava/util/SortedMap;Lcom/a/b/b/co;)Ljava/util/SortedMap;

    move-result-object v0

    goto :goto_a
.end method

.method private static a(Ljava/util/SortedMap;Lcom/a/b/d/tv;)Ljava/util/SortedMap;
    .registers 3

    .prologue
    .line 74
    instance-of v0, p0, Ljava/util/NavigableMap;

    if-eqz v0, :cond_b

    check-cast p0, Ljava/util/NavigableMap;

    invoke-static {p0, p1}, Lcom/a/b/d/sz;->a(Ljava/util/NavigableMap;Lcom/a/b/d/tv;)Ljava/util/NavigableMap;

    move-result-object v0

    :goto_a
    return-object v0

    :cond_b
    invoke-static {p0, p1}, Lcom/a/b/d/sz;->b(Ljava/util/SortedMap;Lcom/a/b/d/tv;)Ljava/util/SortedMap;

    move-result-object v0

    goto :goto_a
.end method

.method private static a(Ljava/util/SortedSet;Lcom/a/b/b/bj;)Ljava/util/SortedMap;
    .registers 3

    .prologue
    .line 81
    instance-of v0, p0, Ljava/util/NavigableSet;

    if-eqz v0, :cond_b

    check-cast p0, Ljava/util/NavigableSet;

    invoke-static {p0, p1}, Lcom/a/b/d/sz;->a(Ljava/util/NavigableSet;Lcom/a/b/b/bj;)Ljava/util/NavigableMap;

    move-result-object v0

    :goto_a
    return-object v0

    :cond_b
    invoke-static {p0, p1}, Lcom/a/b/d/sz;->b(Ljava/util/SortedSet;Lcom/a/b/b/bj;)Ljava/util/SortedMap;

    move-result-object v0

    goto :goto_a
.end method

.method private static a(Ljava/util/SortedSet;Lcom/a/b/b/co;)Ljava/util/SortedSet;
    .registers 5

    .prologue
    .line 88
    instance-of v0, p0, Ljava/util/NavigableSet;

    if-eqz v0, :cond_b

    check-cast p0, Ljava/util/NavigableSet;

    invoke-static {p0, p1}, Lcom/a/b/d/aad;->a(Ljava/util/NavigableSet;Lcom/a/b/b/co;)Ljava/util/NavigableSet;

    move-result-object v0

    .line 1821
    :goto_a
    return-object v0

    .line 1815
    :cond_b
    instance-of v0, p0, Lcom/a/b/d/aal;

    if-eqz v0, :cond_22

    .line 1818
    check-cast p0, Lcom/a/b/d/aal;

    .line 1819
    iget-object v0, p0, Lcom/a/b/d/aal;->b:Lcom/a/b/b/co;

    invoke-static {v0, p1}, Lcom/a/b/b/cp;->a(Lcom/a/b/b/co;Lcom/a/b/b/co;)Lcom/a/b/b/co;

    move-result-object v2

    .line 1821
    new-instance v1, Lcom/a/b/d/aam;

    iget-object v0, p0, Lcom/a/b/d/aal;->a:Ljava/util/Collection;

    check-cast v0, Ljava/util/SortedSet;

    invoke-direct {v1, v0, v2}, Lcom/a/b/d/aam;-><init>(Ljava/util/SortedSet;Lcom/a/b/b/co;)V

    move-object v0, v1

    goto :goto_a

    .line 1825
    :cond_22
    new-instance v2, Lcom/a/b/d/aam;

    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/SortedSet;

    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/a/b/b/co;

    invoke-direct {v2, v0, v1}, Lcom/a/b/d/aam;-><init>(Ljava/util/SortedSet;Lcom/a/b/b/co;)V

    move-object v0, v2

    .line 88
    goto :goto_a
.end method

.method private static a([Ljava/lang/Object;I)[Ljava/lang/Object;
    .registers 3

    .prologue
    .line 48
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getComponentType()Ljava/lang/Class;

    move-result-object v0

    .line 53
    invoke-static {v0, p1}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    .line 54
    return-object v0
.end method
