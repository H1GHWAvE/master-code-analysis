.class public final Lcom/a/b/d/oi;
.super Lcom/a/b/d/ai;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/b;
    a = true
    b = true
.end annotation


# static fields
.field private static final b:J
    .annotation build Lcom/a/b/a/c;
        a = "not needed in emulated source"
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .registers 2

    .prologue
    .line 83
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    invoke-direct {p0, v0}, Lcom/a/b/d/ai;-><init>(Ljava/util/Map;)V

    .line 84
    return-void
.end method

.method private constructor <init>(I)V
    .registers 4

    .prologue
    .line 88
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-static {p1}, Lcom/a/b/d/sz;->b(I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/LinkedHashMap;-><init>(I)V

    invoke-direct {p0, v0}, Lcom/a/b/d/ai;-><init>(Ljava/util/Map;)V

    .line 89
    return-void
.end method

.method public static a(I)Lcom/a/b/d/oi;
    .registers 2

    .prologue
    .line 63
    new-instance v0, Lcom/a/b/d/oi;

    invoke-direct {v0, p0}, Lcom/a/b/d/oi;-><init>(I)V

    return-object v0
.end method

.method private static a(Ljava/lang/Iterable;)Lcom/a/b/d/oi;
    .registers 2

    .prologue
    .line 76
    invoke-static {p0}, Lcom/a/b/d/xe;->a(Ljava/lang/Iterable;)I

    move-result v0

    invoke-static {v0}, Lcom/a/b/d/oi;->a(I)Lcom/a/b/d/oi;

    move-result-object v0

    .line 78
    invoke-static {v0, p0}, Lcom/a/b/d/mq;->a(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    .line 79
    return-object v0
.end method

.method private a(Ljava/io/ObjectInputStream;)V
    .registers 5
    .annotation build Lcom/a/b/a/c;
        a = "java.io.ObjectInputStream"
    .end annotation

    .prologue
    .line 104
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->defaultReadObject()V

    .line 1050
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readInt()I

    move-result v0

    .line 106
    new-instance v1, Ljava/util/LinkedHashMap;

    invoke-static {v0}, Lcom/a/b/d/sz;->b(I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/LinkedHashMap;-><init>(I)V

    .line 1068
    iput-object v1, p0, Lcom/a/b/d/ai;->a:Ljava/util/Map;

    .line 108
    invoke-static {p0, p1, v0}, Lcom/a/b/d/zz;->a(Lcom/a/b/d/xc;Ljava/io/ObjectInputStream;I)V

    .line 109
    return-void
.end method

.method private a(Ljava/io/ObjectOutputStream;)V
    .registers 2
    .annotation build Lcom/a/b/a/c;
        a = "java.io.ObjectOutputStream"
    .end annotation

    .prologue
    .line 97
    invoke-virtual {p1}, Ljava/io/ObjectOutputStream;->defaultWriteObject()V

    .line 98
    invoke-static {p0, p1}, Lcom/a/b/d/zz;->a(Lcom/a/b/d/xc;Ljava/io/ObjectOutputStream;)V

    .line 99
    return-void
.end method

.method public static g()Lcom/a/b/d/oi;
    .registers 1

    .prologue
    .line 52
    new-instance v0, Lcom/a/b/d/oi;

    invoke-direct {v0}, Lcom/a/b/d/oi;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;)I
    .registers 3

    .prologue
    .line 43
    invoke-super {p0, p1}, Lcom/a/b/d/ai;->a(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;I)I
    .registers 4

    .prologue
    .line 43
    invoke-super {p0, p1, p2}, Lcom/a/b/d/ai;->a(Ljava/lang/Object;I)I

    move-result v0

    return v0
.end method

.method public final bridge synthetic a()Ljava/util/Set;
    .registers 2

    .prologue
    .line 43
    invoke-super {p0}, Lcom/a/b/d/ai;->a()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;II)Z
    .registers 5

    .prologue
    .line 43
    invoke-super {p0, p1, p2, p3}, Lcom/a/b/d/ai;->a(Ljava/lang/Object;II)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic add(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 43
    invoke-super {p0, p1}, Lcom/a/b/d/ai;->add(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic addAll(Ljava/util/Collection;)Z
    .registers 3

    .prologue
    .line 43
    invoke-super {p0, p1}, Lcom/a/b/d/ai;->addAll(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic b(Ljava/lang/Object;I)I
    .registers 4

    .prologue
    .line 43
    invoke-super {p0, p1, p2}, Lcom/a/b/d/ai;->b(Ljava/lang/Object;I)I

    move-result v0

    return v0
.end method

.method public final bridge synthetic c(Ljava/lang/Object;I)I
    .registers 4

    .prologue
    .line 43
    invoke-super {p0, p1, p2}, Lcom/a/b/d/ai;->c(Ljava/lang/Object;I)I

    move-result v0

    return v0
.end method

.method public final bridge synthetic clear()V
    .registers 1

    .prologue
    .line 43
    invoke-super {p0}, Lcom/a/b/d/ai;->clear()V

    return-void
.end method

.method public final bridge synthetic contains(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 43
    invoke-super {p0, p1}, Lcom/a/b/d/ai;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic equals(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 43
    invoke-super {p0, p1}, Lcom/a/b/d/ai;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic hashCode()I
    .registers 2

    .prologue
    .line 43
    invoke-super {p0}, Lcom/a/b/d/ai;->hashCode()I

    move-result v0

    return v0
.end method

.method public final bridge synthetic isEmpty()Z
    .registers 2

    .prologue
    .line 43
    invoke-super {p0}, Lcom/a/b/d/ai;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic iterator()Ljava/util/Iterator;
    .registers 2

    .prologue
    .line 43
    invoke-super {p0}, Lcom/a/b/d/ai;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic n_()Ljava/util/Set;
    .registers 2

    .prologue
    .line 43
    invoke-super {p0}, Lcom/a/b/d/ai;->n_()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic remove(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 43
    invoke-super {p0, p1}, Lcom/a/b/d/ai;->remove(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic removeAll(Ljava/util/Collection;)Z
    .registers 3

    .prologue
    .line 43
    invoke-super {p0, p1}, Lcom/a/b/d/ai;->removeAll(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic retainAll(Ljava/util/Collection;)Z
    .registers 3

    .prologue
    .line 43
    invoke-super {p0, p1}, Lcom/a/b/d/ai;->retainAll(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic size()I
    .registers 2

    .prologue
    .line 43
    invoke-super {p0}, Lcom/a/b/d/ai;->size()I

    move-result v0

    return v0
.end method

.method public final bridge synthetic toString()Ljava/lang/String;
    .registers 2

    .prologue
    .line 43
    invoke-super {p0}, Lcom/a/b/d/ai;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
