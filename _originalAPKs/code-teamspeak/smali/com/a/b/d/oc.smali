.class public final Lcom/a/b/d/oc;
.super Lcom/a/b/d/ba;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/b;
    a = true
    b = true
.end annotation


# static fields
.field static final a:D = 1.0
    .annotation build Lcom/a/b/a/d;
    .end annotation
.end field

.field private static final c:I = 0x10

.field private static final d:I = 0x2

.field private static final f:J = 0x1L
    .annotation build Lcom/a/b/a/c;
        a = "java serialization not supported"
    .end annotation
.end field


# instance fields
.field transient b:I
    .annotation build Lcom/a/b/a/d;
    .end annotation
.end field

.field private transient e:Lcom/a/b/d/oe;


# direct methods
.method private constructor <init>(II)V
    .registers 6

    .prologue
    const/4 v2, 0x0

    .line 226
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0, p1}, Ljava/util/LinkedHashMap;-><init>(I)V

    invoke-direct {p0, v0}, Lcom/a/b/d/ba;-><init>(Ljava/util/Map;)V

    .line 222
    const/4 v0, 0x2

    iput v0, p0, Lcom/a/b/d/oc;->b:I

    .line 227
    const-string v0, "expectedValuesPerKey"

    invoke-static {p2, v0}, Lcom/a/b/d/cl;->a(ILjava/lang/String;)I

    .line 229
    iput p2, p0, Lcom/a/b/d/oc;->b:I

    .line 230
    new-instance v0, Lcom/a/b/d/oe;

    const/4 v1, 0x0

    invoke-direct {v0, v2, v2, v1, v2}, Lcom/a/b/d/oe;-><init>(Ljava/lang/Object;Ljava/lang/Object;ILcom/a/b/d/oe;)V

    iput-object v0, p0, Lcom/a/b/d/oc;->e:Lcom/a/b/d/oe;

    .line 231
    iget-object v0, p0, Lcom/a/b/d/oc;->e:Lcom/a/b/d/oe;

    iget-object v1, p0, Lcom/a/b/d/oc;->e:Lcom/a/b/d/oe;

    invoke-static {v0, v1}, Lcom/a/b/d/oc;->b(Lcom/a/b/d/oe;Lcom/a/b/d/oe;)V

    .line 232
    return-void
.end method

.method private static a(I)Lcom/a/b/d/oc;
    .registers 4

    .prologue
    .line 103
    new-instance v0, Lcom/a/b/d/oc;

    invoke-static {p0}, Lcom/a/b/d/sz;->b(I)I

    move-result v1

    const/4 v2, 0x2

    invoke-static {v2}, Lcom/a/b/d/sz;->b(I)I

    move-result v2

    invoke-direct {v0, v1, v2}, Lcom/a/b/d/oc;-><init>(II)V

    return-object v0
.end method

.method static synthetic a(Lcom/a/b/d/oc;)Lcom/a/b/d/oe;
    .registers 2

    .prologue
    .line 82
    iget-object v0, p0, Lcom/a/b/d/oc;->e:Lcom/a/b/d/oe;

    return-object v0
.end method

.method static synthetic a(Lcom/a/b/d/oe;)V
    .registers 3

    .prologue
    .line 82
    .line 3202
    iget-object v0, p0, Lcom/a/b/d/oe;->g:Lcom/a/b/d/oe;

    .line 3206
    iget-object v1, p0, Lcom/a/b/d/oe;->h:Lcom/a/b/d/oe;

    .line 3148
    invoke-static {v0, v1}, Lcom/a/b/d/oc;->b(Lcom/a/b/d/oe;Lcom/a/b/d/oe;)V

    .line 82
    return-void
.end method

.method static synthetic a(Lcom/a/b/d/oe;Lcom/a/b/d/oe;)V
    .registers 2

    .prologue
    .line 82
    invoke-static {p0, p1}, Lcom/a/b/d/oc;->b(Lcom/a/b/d/oe;Lcom/a/b/d/oe;)V

    return-void
.end method

.method static synthetic a(Lcom/a/b/d/oh;)V
    .registers 3

    .prologue
    .line 82
    .line 3144
    invoke-interface {p0}, Lcom/a/b/d/oh;->a()Lcom/a/b/d/oh;

    move-result-object v0

    invoke-interface {p0}, Lcom/a/b/d/oh;->b()Lcom/a/b/d/oh;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/a/b/d/oc;->b(Lcom/a/b/d/oh;Lcom/a/b/d/oh;)V

    .line 82
    return-void
.end method

.method static synthetic a(Lcom/a/b/d/oh;Lcom/a/b/d/oh;)V
    .registers 2

    .prologue
    .line 82
    invoke-static {p0, p1}, Lcom/a/b/d/oc;->b(Lcom/a/b/d/oh;Lcom/a/b/d/oh;)V

    return-void
.end method

.method private a(Ljava/io/ObjectInputStream;)V
    .registers 8
    .annotation build Lcom/a/b/a/c;
        a = "java.io.ObjectInputStream"
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x0

    .line 556
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->defaultReadObject()V

    .line 557
    new-instance v1, Lcom/a/b/d/oe;

    invoke-direct {v1, v2, v2, v0, v2}, Lcom/a/b/d/oe;-><init>(Ljava/lang/Object;Ljava/lang/Object;ILcom/a/b/d/oe;)V

    iput-object v1, p0, Lcom/a/b/d/oc;->e:Lcom/a/b/d/oe;

    .line 558
    iget-object v1, p0, Lcom/a/b/d/oc;->e:Lcom/a/b/d/oe;

    iget-object v2, p0, Lcom/a/b/d/oc;->e:Lcom/a/b/d/oe;

    invoke-static {v1, v2}, Lcom/a/b/d/oc;->b(Lcom/a/b/d/oe;Lcom/a/b/d/oe;)V

    .line 559
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readInt()I

    move-result v1

    iput v1, p0, Lcom/a/b/d/oc;->b:I

    .line 560
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readInt()I

    move-result v2

    .line 561
    new-instance v3, Ljava/util/LinkedHashMap;

    invoke-static {v2}, Lcom/a/b/d/sz;->b(I)I

    move-result v1

    invoke-direct {v3, v1}, Ljava/util/LinkedHashMap;-><init>(I)V

    move v1, v0

    .line 563
    :goto_27
    if-ge v1, v2, :cond_37

    .line 565
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v4

    .line 566
    invoke-virtual {p0, v4}, Lcom/a/b/d/oc;->e(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 563
    add-int/lit8 v1, v1, 0x1

    goto :goto_27

    .line 568
    :cond_37
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readInt()I

    move-result v2

    move v1, v0

    .line 569
    :goto_3c
    if-ge v1, v2, :cond_53

    .line 571
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    .line 573
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v4

    .line 574
    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 569
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3c

    .line 576
    :cond_53
    invoke-virtual {p0, v3}, Lcom/a/b/d/oc;->a(Ljava/util/Map;)V

    .line 577
    return-void
.end method

.method private a(Ljava/io/ObjectOutputStream;)V
    .registers 5
    .annotation build Lcom/a/b/a/c;
        a = "java.io.ObjectOutputStream"
    .end annotation

    .prologue
    .line 540
    invoke-virtual {p1}, Ljava/io/ObjectOutputStream;->defaultWriteObject()V

    .line 541
    iget v0, p0, Lcom/a/b/d/oc;->b:I

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeInt(I)V

    .line 542
    invoke-virtual {p0}, Lcom/a/b/d/oc;->p()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeInt(I)V

    .line 543
    invoke-virtual {p0}, Lcom/a/b/d/oc;->p()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1b
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_29

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 544
    invoke-virtual {p1, v1}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    goto :goto_1b

    .line 546
    :cond_29
    invoke-virtual {p0}, Lcom/a/b/d/oc;->f()I

    move-result v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeInt(I)V

    .line 547
    invoke-virtual {p0}, Lcom/a/b/d/oc;->u()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_38
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_53

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 548
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 549
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    goto :goto_38

    .line 551
    :cond_53
    return-void
.end method

.method private static b(Lcom/a/b/d/vi;)Lcom/a/b/d/oc;
    .registers 4

    .prologue
    .line 119
    invoke-interface {p0}, Lcom/a/b/d/vi;->p()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    .line 1103
    new-instance v1, Lcom/a/b/d/oc;

    invoke-static {v0}, Lcom/a/b/d/sz;->b(I)I

    move-result v0

    const/4 v2, 0x2

    invoke-static {v2}, Lcom/a/b/d/sz;->b(I)I

    move-result v2

    invoke-direct {v1, v0, v2}, Lcom/a/b/d/oc;-><init>(II)V

    .line 120
    invoke-virtual {v1, p0}, Lcom/a/b/d/oc;->a(Lcom/a/b/d/vi;)Z

    .line 121
    return-object v1
.end method

.method private static b(Lcom/a/b/d/oe;)V
    .registers 3

    .prologue
    .line 148
    .line 2202
    iget-object v0, p0, Lcom/a/b/d/oe;->g:Lcom/a/b/d/oe;

    .line 2206
    iget-object v1, p0, Lcom/a/b/d/oe;->h:Lcom/a/b/d/oe;

    .line 148
    invoke-static {v0, v1}, Lcom/a/b/d/oc;->b(Lcom/a/b/d/oe;Lcom/a/b/d/oe;)V

    .line 149
    return-void
.end method

.method private static b(Lcom/a/b/d/oe;Lcom/a/b/d/oe;)V
    .registers 2

    .prologue
    .line 139
    .line 1210
    iput-object p1, p0, Lcom/a/b/d/oe;->h:Lcom/a/b/d/oe;

    .line 1214
    iput-object p0, p1, Lcom/a/b/d/oe;->g:Lcom/a/b/d/oe;

    .line 141
    return-void
.end method

.method private static b(Lcom/a/b/d/oh;)V
    .registers 3

    .prologue
    .line 144
    invoke-interface {p0}, Lcom/a/b/d/oh;->a()Lcom/a/b/d/oh;

    move-result-object v0

    invoke-interface {p0}, Lcom/a/b/d/oh;->b()Lcom/a/b/d/oh;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/a/b/d/oc;->b(Lcom/a/b/d/oh;Lcom/a/b/d/oh;)V

    .line 145
    return-void
.end method

.method private static b(Lcom/a/b/d/oh;Lcom/a/b/d/oh;)V
    .registers 2

    .prologue
    .line 133
    invoke-interface {p0, p1}, Lcom/a/b/d/oh;->b(Lcom/a/b/d/oh;)V

    .line 134
    invoke-interface {p1, p0}, Lcom/a/b/d/oh;->a(Lcom/a/b/d/oh;)V

    .line 135
    return-void
.end method

.method private static v()Lcom/a/b/d/oc;
    .registers 3

    .prologue
    .line 89
    new-instance v0, Lcom/a/b/d/oc;

    const/16 v1, 0x10

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lcom/a/b/d/oc;-><init>(II)V

    return-object v0
.end method


# virtual methods
.method final a()Ljava/util/Set;
    .registers 3

    .prologue
    .line 245
    new-instance v0, Ljava/util/LinkedHashSet;

    iget v1, p0, Lcom/a/b/d/oc;->b:I

    invoke-direct {v0, v1}, Ljava/util/LinkedHashSet;-><init>(I)V

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Ljava/util/Set;
    .registers 3

    .prologue
    .line 81
    invoke-super {p0, p1}, Lcom/a/b/d/ba;->a(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/Set;
    .registers 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 272
    invoke-super {p0, p1, p2}, Lcom/a/b/d/ba;->a(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/a/b/d/vi;)Z
    .registers 3

    .prologue
    .line 81
    invoke-super {p0, p1}, Lcom/a/b/d/ba;->a(Lcom/a/b/d/vi;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Z
    .registers 4

    .prologue
    .line 81
    invoke-super {p0, p1, p2}, Lcom/a/b/d/ba;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final synthetic b(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/Collection;
    .registers 4

    .prologue
    .line 81
    invoke-virtual {p0, p1, p2}, Lcom/a/b/d/oc;->a(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic b()Ljava/util/Map;
    .registers 2

    .prologue
    .line 81
    invoke-super {p0}, Lcom/a/b/d/ba;->b()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic b(Ljava/lang/Object;)Ljava/util/Set;
    .registers 3

    .prologue
    .line 81
    invoke-super {p0, p1}, Lcom/a/b/d/ba;->b(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic b(Ljava/lang/Object;Ljava/lang/Object;)Z
    .registers 4

    .prologue
    .line 81
    invoke-super {p0, p1, p2}, Lcom/a/b/d/ba;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method final synthetic c()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 81
    invoke-virtual {p0}, Lcom/a/b/d/oc;->a()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic c(Ljava/lang/Object;Ljava/lang/Iterable;)Z
    .registers 4

    .prologue
    .line 81
    invoke-super {p0, p1, p2}, Lcom/a/b/d/ba;->c(Ljava/lang/Object;Ljava/lang/Iterable;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic c(Ljava/lang/Object;Ljava/lang/Object;)Z
    .registers 4

    .prologue
    .line 81
    invoke-super {p0, p1, p2}, Lcom/a/b/d/ba;->c(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method final e(Ljava/lang/Object;)Ljava/util/Collection;
    .registers 4

    .prologue
    .line 259
    new-instance v0, Lcom/a/b/d/of;

    iget v1, p0, Lcom/a/b/d/oc;->b:I

    invoke-direct {v0, p0, p1, v1}, Lcom/a/b/d/of;-><init>(Lcom/a/b/d/oc;Ljava/lang/Object;I)V

    return-object v0
.end method

.method public final bridge synthetic equals(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 81
    invoke-super {p0, p1}, Lcom/a/b/d/ba;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic f()I
    .registers 2

    .prologue
    .line 81
    invoke-super {p0}, Lcom/a/b/d/ba;->f()I

    move-result v0

    return v0
.end method

.method public final bridge synthetic f(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 81
    invoke-super {p0, p1}, Lcom/a/b/d/ba;->f(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final g()V
    .registers 3

    .prologue
    .line 530
    invoke-super {p0}, Lcom/a/b/d/ba;->g()V

    .line 531
    iget-object v0, p0, Lcom/a/b/d/oc;->e:Lcom/a/b/d/oe;

    iget-object v1, p0, Lcom/a/b/d/oc;->e:Lcom/a/b/d/oe;

    invoke-static {v0, v1}, Lcom/a/b/d/oc;->b(Lcom/a/b/d/oe;Lcom/a/b/d/oe;)V

    .line 532
    return-void
.end method

.method public final bridge synthetic g(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 81
    invoke-super {p0, p1}, Lcom/a/b/d/ba;->g(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic hashCode()I
    .registers 2

    .prologue
    .line 81
    invoke-super {p0}, Lcom/a/b/d/ba;->hashCode()I

    move-result v0

    return v0
.end method

.method public final i()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 299
    invoke-super {p0}, Lcom/a/b/d/ba;->i()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method final j()Ljava/util/Iterator;
    .registers 2

    .prologue
    .line 525
    invoke-virtual {p0}, Lcom/a/b/d/oc;->l()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/sz;->b(Ljava/util/Iterator;)Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic k()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 81
    invoke-virtual {p0}, Lcom/a/b/d/oc;->u()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method final l()Ljava/util/Iterator;
    .registers 2

    .prologue
    .line 494
    new-instance v0, Lcom/a/b/d/od;

    invoke-direct {v0, p0}, Lcom/a/b/d/od;-><init>(Lcom/a/b/d/oc;)V

    return-object v0
.end method

.method public final bridge synthetic n()Z
    .registers 2

    .prologue
    .line 81
    invoke-super {p0}, Lcom/a/b/d/ba;->n()Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic p()Ljava/util/Set;
    .registers 2

    .prologue
    .line 81
    invoke-super {p0}, Lcom/a/b/d/ba;->p()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic q()Lcom/a/b/d/xc;
    .registers 2

    .prologue
    .line 81
    invoke-super {p0}, Lcom/a/b/d/ba;->q()Lcom/a/b/d/xc;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic toString()Ljava/lang/String;
    .registers 2

    .prologue
    .line 81
    invoke-super {p0}, Lcom/a/b/d/ba;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final u()Ljava/util/Set;
    .registers 2

    .prologue
    .line 288
    invoke-super {p0}, Lcom/a/b/d/ba;->u()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method
