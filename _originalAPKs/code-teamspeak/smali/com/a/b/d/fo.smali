.class final Lcom/a/b/d/fo;
.super Lcom/a/b/d/wn;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/a/b/d/fi;


# direct methods
.method constructor <init>(Lcom/a/b/d/fi;)V
    .registers 2

    .prologue
    .line 330
    iput-object p1, p0, Lcom/a/b/d/fo;->a:Lcom/a/b/d/fi;

    .line 331
    invoke-direct {p0, p1}, Lcom/a/b/d/wn;-><init>(Lcom/a/b/d/vi;)V

    .line 332
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/Set;
    .registers 2

    .prologue
    .line 362
    new-instance v0, Lcom/a/b/d/fp;

    invoke-direct {v0, p0}, Lcom/a/b/d/fp;-><init>(Lcom/a/b/d/fo;)V

    return-object v0
.end method

.method public final b(Ljava/lang/Object;I)I
    .registers 7
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 336
    const-string v0, "occurrences"

    invoke-static {p2, v0}, Lcom/a/b/d/cl;->a(ILjava/lang/String;)I

    .line 337
    if-nez p2, :cond_d

    .line 338
    invoke-virtual {p0, p1}, Lcom/a/b/d/fo;->a(Ljava/lang/Object;)I

    move-result v1

    .line 357
    :cond_c
    :goto_c
    return v1

    .line 340
    :cond_d
    iget-object v0, p0, Lcom/a/b/d/fo;->a:Lcom/a/b/d/fi;

    iget-object v0, v0, Lcom/a/b/d/fi;->a:Lcom/a/b/d/vi;

    invoke-interface {v0}, Lcom/a/b/d/vi;->b()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 341
    if-eqz v0, :cond_c

    .line 347
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v0, v1

    .line 348
    :cond_22
    :goto_22
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3c

    .line 349
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 350
    iget-object v3, p0, Lcom/a/b/d/fo;->a:Lcom/a/b/d/fi;

    invoke-static {v3, p1, v1}, Lcom/a/b/d/fi;->a(Lcom/a/b/d/fi;Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_22

    .line 351
    add-int/lit8 v0, v0, 0x1

    .line 352
    if-gt v0, p2, :cond_22

    .line 353
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_22

    :cond_3c
    move v1, v0

    .line 357
    goto :goto_c
.end method
