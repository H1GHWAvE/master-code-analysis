.class final Lcom/a/b/d/dq;
.super Lcom/a/b/d/gq;
.source "SourceFile"


# instance fields
.field private final a:Ljava/util/ListIterator;

.field private final b:Lcom/a/b/d/dm;


# direct methods
.method public constructor <init>(Ljava/util/ListIterator;Lcom/a/b/d/dm;)V
    .registers 3

    .prologue
    .line 260
    invoke-direct {p0}, Lcom/a/b/d/gq;-><init>()V

    .line 261
    iput-object p1, p0, Lcom/a/b/d/dq;->a:Ljava/util/ListIterator;

    .line 262
    iput-object p2, p0, Lcom/a/b/d/dq;->b:Lcom/a/b/d/dm;

    .line 263
    return-void
.end method


# virtual methods
.method protected final bridge synthetic a()Ljava/util/Iterator;
    .registers 2

    .prologue
    .line 255
    .line 1265
    iget-object v0, p0, Lcom/a/b/d/dq;->a:Ljava/util/ListIterator;

    .line 255
    return-object v0
.end method

.method public final add(Ljava/lang/Object;)V
    .registers 3

    .prologue
    .line 269
    iget-object v0, p0, Lcom/a/b/d/dq;->b:Lcom/a/b/d/dm;

    invoke-interface {v0, p1}, Lcom/a/b/d/dm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 270
    iget-object v0, p0, Lcom/a/b/d/dq;->a:Ljava/util/ListIterator;

    invoke-interface {v0, p1}, Ljava/util/ListIterator;->add(Ljava/lang/Object;)V

    .line 271
    return-void
.end method

.method protected final b()Ljava/util/ListIterator;
    .registers 2

    .prologue
    .line 265
    iget-object v0, p0, Lcom/a/b/d/dq;->a:Ljava/util/ListIterator;

    return-object v0
.end method

.method protected final bridge synthetic k_()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 255
    .line 2265
    iget-object v0, p0, Lcom/a/b/d/dq;->a:Ljava/util/ListIterator;

    .line 255
    return-object v0
.end method

.method public final set(Ljava/lang/Object;)V
    .registers 3

    .prologue
    .line 273
    iget-object v0, p0, Lcom/a/b/d/dq;->b:Lcom/a/b/d/dm;

    invoke-interface {v0, p1}, Lcom/a/b/d/dm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 274
    iget-object v0, p0, Lcom/a/b/d/dq;->a:Ljava/util/ListIterator;

    invoke-interface {v0, p1}, Ljava/util/ListIterator;->set(Ljava/lang/Object;)V

    .line 275
    return-void
.end method
