.class abstract Lcom/a/b/d/ba;
.super Lcom/a/b/d/n;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/d/aac;


# annotations
.annotation build Lcom/a/b/a/b;
.end annotation


# static fields
.field private static final a:J = 0x67226fd4cd0928d8L


# direct methods
.method protected constructor <init>(Ljava/util/Map;)V
    .registers 2

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/a/b/d/n;-><init>(Ljava/util/Map;)V

    .line 45
    return-void
.end method


# virtual methods
.method abstract a()Ljava/util/Set;
.end method

.method public a(Ljava/lang/Object;)Ljava/util/Set;
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 63
    invoke-super {p0, p1}, Lcom/a/b/d/n;->c(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    return-object v0
.end method

.method public a(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/Set;
    .registers 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 99
    invoke-super {p0, p1, p2}, Lcom/a/b/d/n;->b(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    return-object v0
.end method

.method public a(Ljava/lang/Object;Ljava/lang/Object;)Z
    .registers 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 121
    invoke-super {p0, p1, p2}, Lcom/a/b/d/n;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public synthetic b(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/Collection;
    .registers 4

    .prologue
    .line 34
    invoke-virtual {p0, p1, p2}, Lcom/a/b/d/ba;->a(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public b()Ljava/util/Map;
    .registers 2

    .prologue
    .line 109
    invoke-super {p0}, Lcom/a/b/d/n;->b()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/Object;)Ljava/util/Set;
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 85
    invoke-super {p0, p1}, Lcom/a/b/d/n;->d(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    return-object v0
.end method

.method synthetic c()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 34
    invoke-virtual {p0}, Lcom/a/b/d/ba;->a()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public synthetic c(Ljava/lang/Object;)Ljava/util/Collection;
    .registers 3

    .prologue
    .line 34
    invoke-virtual {p0, p1}, Lcom/a/b/d/ba;->a(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method synthetic d()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 34
    invoke-virtual {p0}, Lcom/a/b/d/ba;->t()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public synthetic d(Ljava/lang/Object;)Ljava/util/Collection;
    .registers 3

    .prologue
    .line 34
    invoke-virtual {p0, p1}, Lcom/a/b/d/ba;->b(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 132
    invoke-super {p0, p1}, Lcom/a/b/d/n;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public synthetic k()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 34
    invoke-virtual {p0}, Lcom/a/b/d/ba;->u()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method t()Ljava/util/Set;
    .registers 2

    .prologue
    .line 50
    invoke-static {}, Lcom/a/b/d/lo;->h()Lcom/a/b/d/lo;

    move-result-object v0

    return-object v0
.end method

.method public u()Ljava/util/Set;
    .registers 2

    .prologue
    .line 74
    invoke-super {p0}, Lcom/a/b/d/n;->k()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    return-object v0
.end method
