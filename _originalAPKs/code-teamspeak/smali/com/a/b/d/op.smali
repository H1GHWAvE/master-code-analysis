.class final Lcom/a/b/d/op;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/Iterator;


# instance fields
.field final a:Ljava/util/Set;

.field b:Lcom/a/b/d/or;

.field c:Lcom/a/b/d/or;

.field d:I

.field final synthetic e:Lcom/a/b/d/oj;


# direct methods
.method private constructor <init>(Lcom/a/b/d/oj;)V
    .registers 3

    .prologue
    .line 410
    iput-object p1, p0, Lcom/a/b/d/op;->e:Lcom/a/b/d/oj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 411
    iget-object v0, p0, Lcom/a/b/d/op;->e:Lcom/a/b/d/oj;

    invoke-virtual {v0}, Lcom/a/b/d/oj;->p()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    invoke-static {v0}, Lcom/a/b/d/aad;->a(I)Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/op;->a:Ljava/util/Set;

    .line 412
    iget-object v0, p0, Lcom/a/b/d/op;->e:Lcom/a/b/d/oj;

    invoke-static {v0}, Lcom/a/b/d/oj;->c(Lcom/a/b/d/oj;)Lcom/a/b/d/or;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/op;->b:Lcom/a/b/d/or;

    .line 414
    iget-object v0, p0, Lcom/a/b/d/op;->e:Lcom/a/b/d/oj;

    invoke-static {v0}, Lcom/a/b/d/oj;->a(Lcom/a/b/d/oj;)I

    move-result v0

    iput v0, p0, Lcom/a/b/d/op;->d:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/a/b/d/oj;B)V
    .registers 3

    .prologue
    .line 410
    invoke-direct {p0, p1}, Lcom/a/b/d/op;-><init>(Lcom/a/b/d/oj;)V

    return-void
.end method

.method private a()V
    .registers 3

    .prologue
    .line 417
    iget-object v0, p0, Lcom/a/b/d/op;->e:Lcom/a/b/d/oj;

    invoke-static {v0}, Lcom/a/b/d/oj;->a(Lcom/a/b/d/oj;)I

    move-result v0

    iget v1, p0, Lcom/a/b/d/op;->d:I

    if-eq v0, v1, :cond_10

    .line 418
    new-instance v0, Ljava/util/ConcurrentModificationException;

    invoke-direct {v0}, Ljava/util/ConcurrentModificationException;-><init>()V

    throw v0

    .line 420
    :cond_10
    return-void
.end method


# virtual methods
.method public final hasNext()Z
    .registers 2

    .prologue
    .line 423
    invoke-direct {p0}, Lcom/a/b/d/op;->a()V

    .line 424
    iget-object v0, p0, Lcom/a/b/d/op;->b:Lcom/a/b/d/or;

    if-eqz v0, :cond_9

    const/4 v0, 0x1

    :goto_8
    return v0

    :cond_9
    const/4 v0, 0x0

    goto :goto_8
.end method

.method public final next()Ljava/lang/Object;
    .registers 3

    .prologue
    .line 428
    invoke-direct {p0}, Lcom/a/b/d/op;->a()V

    .line 429
    iget-object v0, p0, Lcom/a/b/d/op;->b:Lcom/a/b/d/or;

    invoke-static {v0}, Lcom/a/b/d/oj;->e(Ljava/lang/Object;)V

    .line 430
    iget-object v0, p0, Lcom/a/b/d/op;->b:Lcom/a/b/d/or;

    iput-object v0, p0, Lcom/a/b/d/op;->c:Lcom/a/b/d/or;

    .line 431
    iget-object v0, p0, Lcom/a/b/d/op;->a:Ljava/util/Set;

    iget-object v1, p0, Lcom/a/b/d/op;->c:Lcom/a/b/d/or;

    iget-object v1, v1, Lcom/a/b/d/or;->a:Ljava/lang/Object;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 433
    :cond_15
    iget-object v0, p0, Lcom/a/b/d/op;->b:Lcom/a/b/d/or;

    iget-object v0, v0, Lcom/a/b/d/or;->c:Lcom/a/b/d/or;

    iput-object v0, p0, Lcom/a/b/d/op;->b:Lcom/a/b/d/or;

    .line 434
    iget-object v0, p0, Lcom/a/b/d/op;->b:Lcom/a/b/d/or;

    if-eqz v0, :cond_2b

    iget-object v0, p0, Lcom/a/b/d/op;->a:Ljava/util/Set;

    iget-object v1, p0, Lcom/a/b/d/op;->b:Lcom/a/b/d/or;

    iget-object v1, v1, Lcom/a/b/d/or;->a:Ljava/lang/Object;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 435
    :cond_2b
    iget-object v0, p0, Lcom/a/b/d/op;->c:Lcom/a/b/d/or;

    iget-object v0, v0, Lcom/a/b/d/or;->a:Ljava/lang/Object;

    return-object v0
.end method

.method public final remove()V
    .registers 3

    .prologue
    .line 439
    invoke-direct {p0}, Lcom/a/b/d/op;->a()V

    .line 440
    iget-object v0, p0, Lcom/a/b/d/op;->c:Lcom/a/b/d/or;

    if-eqz v0, :cond_22

    const/4 v0, 0x1

    .line 1049
    :goto_8
    const-string v1, "no calls to next() since the last call to remove()"

    invoke-static {v0, v1}, Lcom/a/b/b/cn;->b(ZLjava/lang/Object;)V

    .line 441
    iget-object v0, p0, Lcom/a/b/d/op;->e:Lcom/a/b/d/oj;

    iget-object v1, p0, Lcom/a/b/d/op;->c:Lcom/a/b/d/or;

    iget-object v1, v1, Lcom/a/b/d/or;->a:Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/a/b/d/oj;->a(Lcom/a/b/d/oj;Ljava/lang/Object;)V

    .line 442
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/a/b/d/op;->c:Lcom/a/b/d/or;

    .line 443
    iget-object v0, p0, Lcom/a/b/d/op;->e:Lcom/a/b/d/oj;

    invoke-static {v0}, Lcom/a/b/d/oj;->a(Lcom/a/b/d/oj;)I

    move-result v0

    iput v0, p0, Lcom/a/b/d/op;->d:I

    .line 444
    return-void

    .line 440
    :cond_22
    const/4 v0, 0x0

    goto :goto_8
.end method
