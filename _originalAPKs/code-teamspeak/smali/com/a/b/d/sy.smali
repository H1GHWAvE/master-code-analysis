.class final Lcom/a/b/d/sy;
.super Lcom/a/b/d/am;
.source "SourceFile"


# instance fields
.field final a:Ljava/lang/Object;

.field b:Ljava/lang/Object;

.field final synthetic c:Lcom/a/b/d/qy;


# direct methods
.method constructor <init>(Lcom/a/b/d/qy;Ljava/lang/Object;Ljava/lang/Object;)V
    .registers 4

    .prologue
    .line 3726
    iput-object p1, p0, Lcom/a/b/d/sy;->c:Lcom/a/b/d/qy;

    invoke-direct {p0}, Lcom/a/b/d/am;-><init>()V

    .line 3727
    iput-object p2, p0, Lcom/a/b/d/sy;->a:Ljava/lang/Object;

    .line 3728
    iput-object p3, p0, Lcom/a/b/d/sy;->b:Ljava/lang/Object;

    .line 3729
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .registers 5
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 3744
    instance-of v1, p1, Ljava/util/Map$Entry;

    if-eqz v1, :cond_20

    .line 3745
    check-cast p1, Ljava/util/Map$Entry;

    .line 3746
    iget-object v1, p0, Lcom/a/b/d/sy;->a:Ljava/lang/Object;

    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_20

    iget-object v1, p0, Lcom/a/b/d/sy;->b:Ljava/lang/Object;

    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_20

    const/4 v0, 0x1

    .line 3748
    :cond_20
    return v0
.end method

.method public final getKey()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 3733
    iget-object v0, p0, Lcom/a/b/d/sy;->a:Ljava/lang/Object;

    return-object v0
.end method

.method public final getValue()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 3738
    iget-object v0, p0, Lcom/a/b/d/sy;->b:Ljava/lang/Object;

    return-object v0
.end method

.method public final hashCode()I
    .registers 3

    .prologue
    .line 3754
    iget-object v0, p0, Lcom/a/b/d/sy;->a:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    iget-object v1, p0, Lcom/a/b/d/sy;->b:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    return v0
.end method

.method public final setValue(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 3759
    iget-object v0, p0, Lcom/a/b/d/sy;->c:Lcom/a/b/d/qy;

    iget-object v1, p0, Lcom/a/b/d/sy;->a:Ljava/lang/Object;

    invoke-virtual {v0, v1, p1}, Lcom/a/b/d/qy;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 3760
    iput-object p1, p0, Lcom/a/b/d/sy;->b:Ljava/lang/Object;

    .line 3761
    return-object v0
.end method
