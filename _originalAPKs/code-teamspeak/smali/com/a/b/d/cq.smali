.class final Lcom/a/b/d/cq;
.super Lcom/a/b/d/j;
.source "SourceFile"


# instance fields
.field a:Ljava/util/List;

.field final b:Ljava/util/Comparator;


# direct methods
.method constructor <init>(Ljava/util/List;Ljava/util/Comparator;)V
    .registers 4

    .prologue
    .line 489
    invoke-direct {p0}, Lcom/a/b/d/j;-><init>()V

    .line 490
    invoke-static {p1}, Lcom/a/b/d/ov;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/cq;->a:Ljava/util/List;

    .line 491
    iput-object p2, p0, Lcom/a/b/d/cq;->b:Ljava/util/Comparator;

    .line 492
    return-void
.end method

.method private a(I)I
    .registers 6

    .prologue
    .line 527
    iget-object v0, p0, Lcom/a/b/d/cq;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    .line 528
    iget-object v0, p0, Lcom/a/b/d/cq;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_e
    if-le v0, p1, :cond_22

    .line 529
    iget-object v2, p0, Lcom/a/b/d/cq;->b:Ljava/util/Comparator;

    iget-object v3, p0, Lcom/a/b/d/cq;->a:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2, v1, v3}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v2

    if-gez v2, :cond_1f

    .line 530
    return v0

    .line 528
    :cond_1f
    add-int/lit8 v0, v0, -0x1

    goto :goto_e

    .line 533
    :cond_22
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "this statement should be unreachable"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method private c()Ljava/util/List;
    .registers 9

    .prologue
    const/4 v0, 0x0

    const/4 v3, -0x1

    .line 495
    iget-object v1, p0, Lcom/a/b/d/cq;->a:Ljava/util/List;

    if-nez v1, :cond_a

    .line 496
    invoke-virtual {p0}, Lcom/a/b/d/cq;->b()Ljava/lang/Object;

    .line 500
    :goto_9
    return-object v0

    .line 498
    :cond_a
    iget-object v1, p0, Lcom/a/b/d/cq;->a:Ljava/util/List;

    invoke-static {v1}, Lcom/a/b/d/jl;->a(Ljava/util/Collection;)Lcom/a/b/d/jl;

    move-result-object v1

    .line 1517
    iget-object v2, p0, Lcom/a/b/d/cq;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x2

    :goto_18
    if-ltz v2, :cond_39

    .line 1518
    iget-object v4, p0, Lcom/a/b/d/cq;->b:Ljava/util/Comparator;

    iget-object v5, p0, Lcom/a/b/d/cq;->a:Ljava/util/List;

    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    iget-object v6, p0, Lcom/a/b/d/cq;->a:Ljava/util/List;

    add-int/lit8 v7, v2, 0x1

    invoke-interface {v6, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    invoke-interface {v4, v5, v6}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v4

    if-gez v4, :cond_36

    .line 1505
    :goto_30
    if-ne v2, v3, :cond_3b

    .line 1506
    iput-object v0, p0, Lcom/a/b/d/cq;->a:Ljava/util/List;

    :goto_34
    move-object v0, v1

    .line 500
    goto :goto_9

    .line 1517
    :cond_36
    add-int/lit8 v2, v2, -0x1

    goto :goto_18

    :cond_39
    move v2, v3

    .line 1523
    goto :goto_30

    .line 1527
    :cond_3b
    iget-object v0, p0, Lcom/a/b/d/cq;->a:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    .line 1528
    iget-object v0, p0, Lcom/a/b/d/cq;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_49
    if-le v0, v2, :cond_73

    .line 1529
    iget-object v4, p0, Lcom/a/b/d/cq;->b:Ljava/util/Comparator;

    iget-object v5, p0, Lcom/a/b/d/cq;->a:Ljava/util/List;

    invoke-interface {v5, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-interface {v4, v3, v5}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v4

    if-gez v4, :cond_70

    .line 1511
    iget-object v3, p0, Lcom/a/b/d/cq;->a:Ljava/util/List;

    invoke-static {v3, v2, v0}, Ljava/util/Collections;->swap(Ljava/util/List;II)V

    .line 1512
    iget-object v0, p0, Lcom/a/b/d/cq;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 1513
    iget-object v3, p0, Lcom/a/b/d/cq;->a:Ljava/util/List;

    add-int/lit8 v2, v2, 0x1

    invoke-interface {v3, v2, v0}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    goto :goto_34

    .line 1528
    :cond_70
    add-int/lit8 v0, v0, -0x1

    goto :goto_49

    .line 1533
    :cond_73
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "this statement should be unreachable"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method private d()V
    .registers 7

    .prologue
    const/4 v2, -0x1

    .line 504
    .line 2517
    iget-object v0, p0, Lcom/a/b/d/cq;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x2

    :goto_9
    if-ltz v0, :cond_2b

    .line 2518
    iget-object v1, p0, Lcom/a/b/d/cq;->b:Ljava/util/Comparator;

    iget-object v3, p0, Lcom/a/b/d/cq;->a:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    iget-object v4, p0, Lcom/a/b/d/cq;->a:Ljava/util/List;

    add-int/lit8 v5, v0, 0x1

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v1, v3, v4}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v1

    if-gez v1, :cond_28

    move v1, v0

    .line 505
    :goto_22
    if-ne v1, v2, :cond_2d

    .line 506
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/a/b/d/cq;->a:Ljava/util/List;

    .line 514
    :goto_27
    return-void

    .line 2517
    :cond_28
    add-int/lit8 v0, v0, -0x1

    goto :goto_9

    :cond_2b
    move v1, v2

    .line 2523
    goto :goto_22

    .line 2527
    :cond_2d
    iget-object v0, p0, Lcom/a/b/d/cq;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    .line 2528
    iget-object v0, p0, Lcom/a/b/d/cq;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_3b
    if-le v0, v1, :cond_65

    .line 2529
    iget-object v3, p0, Lcom/a/b/d/cq;->b:Ljava/util/Comparator;

    iget-object v4, p0, Lcom/a/b/d/cq;->a:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v2, v4}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v3

    if-gez v3, :cond_62

    .line 511
    iget-object v2, p0, Lcom/a/b/d/cq;->a:Ljava/util/List;

    invoke-static {v2, v1, v0}, Ljava/util/Collections;->swap(Ljava/util/List;II)V

    .line 512
    iget-object v0, p0, Lcom/a/b/d/cq;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 513
    iget-object v2, p0, Lcom/a/b/d/cq;->a:Ljava/util/List;

    add-int/lit8 v1, v1, 0x1

    invoke-interface {v2, v1, v0}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    goto :goto_27

    .line 2528
    :cond_62
    add-int/lit8 v0, v0, -0x1

    goto :goto_3b

    .line 2533
    :cond_65
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "this statement should be unreachable"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method private e()I
    .registers 6

    .prologue
    .line 517
    iget-object v0, p0, Lcom/a/b/d/cq;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x2

    :goto_8
    if-ltz v0, :cond_24

    .line 518
    iget-object v1, p0, Lcom/a/b/d/cq;->b:Ljava/util/Comparator;

    iget-object v2, p0, Lcom/a/b/d/cq;->a:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p0, Lcom/a/b/d/cq;->a:Ljava/util/List;

    add-int/lit8 v4, v0, 0x1

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v1

    if-gez v1, :cond_21

    .line 523
    :goto_20
    return v0

    .line 517
    :cond_21
    add-int/lit8 v0, v0, -0x1

    goto :goto_8

    .line 523
    :cond_24
    const/4 v0, -0x1

    goto :goto_20
.end method


# virtual methods
.method protected final synthetic a()Ljava/lang/Object;
    .registers 9

    .prologue
    const/4 v0, 0x0

    const/4 v3, -0x1

    .line 482
    .line 3495
    iget-object v1, p0, Lcom/a/b/d/cq;->a:Ljava/util/List;

    if-nez v1, :cond_a

    .line 3496
    invoke-virtual {p0}, Lcom/a/b/d/cq;->b()Ljava/lang/Object;

    :goto_9
    return-object v0

    .line 3498
    :cond_a
    iget-object v1, p0, Lcom/a/b/d/cq;->a:Ljava/util/List;

    invoke-static {v1}, Lcom/a/b/d/jl;->a(Ljava/util/Collection;)Lcom/a/b/d/jl;

    move-result-object v1

    .line 3517
    iget-object v2, p0, Lcom/a/b/d/cq;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x2

    :goto_18
    if-ltz v2, :cond_39

    .line 3518
    iget-object v4, p0, Lcom/a/b/d/cq;->b:Ljava/util/Comparator;

    iget-object v5, p0, Lcom/a/b/d/cq;->a:Ljava/util/List;

    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    iget-object v6, p0, Lcom/a/b/d/cq;->a:Ljava/util/List;

    add-int/lit8 v7, v2, 0x1

    invoke-interface {v6, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    invoke-interface {v4, v5, v6}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v4

    if-gez v4, :cond_36

    .line 3505
    :goto_30
    if-ne v2, v3, :cond_3b

    .line 3506
    iput-object v0, p0, Lcom/a/b/d/cq;->a:Ljava/util/List;

    :goto_34
    move-object v0, v1

    .line 482
    goto :goto_9

    .line 3517
    :cond_36
    add-int/lit8 v2, v2, -0x1

    goto :goto_18

    :cond_39
    move v2, v3

    .line 3523
    goto :goto_30

    .line 3527
    :cond_3b
    iget-object v0, p0, Lcom/a/b/d/cq;->a:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    .line 3528
    iget-object v0, p0, Lcom/a/b/d/cq;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_49
    if-le v0, v2, :cond_73

    .line 3529
    iget-object v4, p0, Lcom/a/b/d/cq;->b:Ljava/util/Comparator;

    iget-object v5, p0, Lcom/a/b/d/cq;->a:Ljava/util/List;

    invoke-interface {v5, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-interface {v4, v3, v5}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v4

    if-gez v4, :cond_70

    .line 3511
    iget-object v3, p0, Lcom/a/b/d/cq;->a:Ljava/util/List;

    invoke-static {v3, v2, v0}, Ljava/util/Collections;->swap(Ljava/util/List;II)V

    .line 3512
    iget-object v0, p0, Lcom/a/b/d/cq;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 3513
    iget-object v3, p0, Lcom/a/b/d/cq;->a:Ljava/util/List;

    add-int/lit8 v2, v2, 0x1

    invoke-interface {v3, v2, v0}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    goto :goto_34

    .line 3528
    :cond_70
    add-int/lit8 v0, v0, -0x1

    goto :goto_49

    .line 3533
    :cond_73
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "this statement should be unreachable"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method
