.class public final Lcom/a/b/d/aeq;
.super Lcom/a/b/d/bb;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/b;
    a = true
    b = true
.end annotation


# static fields
.field private static final c:J
    .annotation build Lcom/a/b/a/c;
        a = "not needed in emulated source"
    .end annotation
.end field


# instance fields
.field private transient a:Ljava/util/Comparator;

.field private transient b:Ljava/util/Comparator;


# direct methods
.method private constructor <init>(Ljava/util/Comparator;Ljava/util/Comparator;)V
    .registers 4

    .prologue
    .line 121
    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0, p1}, Ljava/util/TreeMap;-><init>(Ljava/util/Comparator;)V

    invoke-direct {p0, v0}, Lcom/a/b/d/bb;-><init>(Ljava/util/SortedMap;)V

    .line 122
    iput-object p1, p0, Lcom/a/b/d/aeq;->a:Ljava/util/Comparator;

    .line 123
    iput-object p2, p0, Lcom/a/b/d/aeq;->b:Ljava/util/Comparator;

    .line 124
    return-void
.end method

.method private constructor <init>(Ljava/util/Comparator;Ljava/util/Comparator;Lcom/a/b/d/vi;)V
    .registers 4

    .prologue
    .line 129
    invoke-direct {p0, p1, p2}, Lcom/a/b/d/aeq;-><init>(Ljava/util/Comparator;Ljava/util/Comparator;)V

    .line 130
    invoke-virtual {p0, p3}, Lcom/a/b/d/aeq;->a(Lcom/a/b/d/vi;)Z

    .line 131
    return-void
.end method

.method private A()Ljava/util/Comparator;
    .registers 2

    .prologue
    .line 157
    iget-object v0, p0, Lcom/a/b/d/aeq;->a:Ljava/util/Comparator;

    return-object v0
.end method

.method private B()Ljava/util/NavigableMap;
    .registers 2
    .annotation build Lcom/a/b/a/c;
        a = "NavigableMap"
    .end annotation

    .prologue
    .line 174
    invoke-super {p0}, Lcom/a/b/d/bb;->w()Ljava/util/SortedMap;

    move-result-object v0

    check-cast v0, Ljava/util/NavigableMap;

    return-object v0
.end method

.method private C()Ljava/util/NavigableSet;
    .registers 2
    .annotation build Lcom/a/b/a/c;
        a = "NavigableSet"
    .end annotation

    .prologue
    .line 210
    invoke-super {p0}, Lcom/a/b/d/bb;->x()Ljava/util/SortedSet;

    move-result-object v0

    check-cast v0, Ljava/util/NavigableSet;

    return-object v0
.end method

.method private D()Ljava/util/NavigableSet;
    .registers 3
    .annotation build Lcom/a/b/a/c;
        a = "NavigableSet"
    .end annotation

    .prologue
    .line 216
    new-instance v1, Lcom/a/b/d/x;

    .line 1174
    invoke-super {p0}, Lcom/a/b/d/bb;->w()Ljava/util/SortedMap;

    move-result-object v0

    check-cast v0, Ljava/util/NavigableMap;

    .line 216
    invoke-direct {v1, p0, v0}, Lcom/a/b/d/x;-><init>(Lcom/a/b/d/n;Ljava/util/NavigableMap;)V

    return-object v1
.end method

.method private E()Ljava/util/NavigableMap;
    .registers 2
    .annotation build Lcom/a/b/a/c;
        a = "NavigableMap"
    .end annotation

    .prologue
    .line 231
    invoke-super {p0}, Lcom/a/b/d/bb;->v()Ljava/util/SortedMap;

    move-result-object v0

    check-cast v0, Ljava/util/NavigableMap;

    return-object v0
.end method

.method private F()Ljava/util/NavigableMap;
    .registers 3
    .annotation build Lcom/a/b/a/c;
        a = "NavigableMap"
    .end annotation

    .prologue
    .line 237
    new-instance v1, Lcom/a/b/d/w;

    .line 2174
    invoke-super {p0}, Lcom/a/b/d/bb;->w()Ljava/util/SortedMap;

    move-result-object v0

    check-cast v0, Ljava/util/NavigableMap;

    .line 237
    invoke-direct {v1, p0, v0}, Lcom/a/b/d/w;-><init>(Lcom/a/b/d/n;Ljava/util/NavigableMap;)V

    return-object v1
.end method

.method private static a(Ljava/util/Comparator;Ljava/util/Comparator;)Lcom/a/b/d/aeq;
    .registers 5

    .prologue
    .line 103
    new-instance v2, Lcom/a/b/d/aeq;

    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Comparator;

    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Comparator;

    invoke-direct {v2, v0, v1}, Lcom/a/b/d/aeq;-><init>(Ljava/util/Comparator;Ljava/util/Comparator;)V

    return-object v2
.end method

.method private a(Ljava/io/ObjectInputStream;)V
    .registers 4
    .annotation build Lcom/a/b/a/c;
        a = "java.io.ObjectInputStream"
    .end annotation

    .prologue
    .line 257
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->defaultReadObject()V

    .line 258
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Comparator;

    invoke-static {v0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Comparator;

    iput-object v0, p0, Lcom/a/b/d/aeq;->a:Ljava/util/Comparator;

    .line 259
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Comparator;

    invoke-static {v0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Comparator;

    iput-object v0, p0, Lcom/a/b/d/aeq;->b:Ljava/util/Comparator;

    .line 260
    new-instance v0, Ljava/util/TreeMap;

    iget-object v1, p0, Lcom/a/b/d/aeq;->a:Ljava/util/Comparator;

    invoke-direct {v0, v1}, Ljava/util/TreeMap;-><init>(Ljava/util/Comparator;)V

    invoke-virtual {p0, v0}, Lcom/a/b/d/aeq;->a(Ljava/util/Map;)V

    .line 261
    invoke-static {p0, p1}, Lcom/a/b/d/zz;->a(Lcom/a/b/d/vi;Ljava/io/ObjectInputStream;)V

    .line 262
    return-void
.end method

.method private a(Ljava/io/ObjectOutputStream;)V
    .registers 3
    .annotation build Lcom/a/b/a/c;
        a = "java.io.ObjectOutputStream"
    .end annotation

    .prologue
    .line 247
    invoke-virtual {p1}, Ljava/io/ObjectOutputStream;->defaultWriteObject()V

    .line 3157
    iget-object v0, p0, Lcom/a/b/d/aeq;->a:Ljava/util/Comparator;

    .line 248
    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 249
    invoke-virtual {p0}, Lcom/a/b/d/aeq;->d_()Ljava/util/Comparator;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 250
    invoke-static {p0, p1}, Lcom/a/b/d/zz;->a(Lcom/a/b/d/vi;Ljava/io/ObjectOutputStream;)V

    .line 251
    return-void
.end method

.method private static b(Lcom/a/b/d/vi;)Lcom/a/b/d/aeq;
    .registers 4

    .prologue
    .line 115
    new-instance v0, Lcom/a/b/d/aeq;

    invoke-static {}, Lcom/a/b/d/yd;->d()Lcom/a/b/d/yd;

    move-result-object v1

    invoke-static {}, Lcom/a/b/d/yd;->d()Lcom/a/b/d/yd;

    move-result-object v2

    invoke-direct {v0, v1, v2, p0}, Lcom/a/b/d/aeq;-><init>(Ljava/util/Comparator;Ljava/util/Comparator;Lcom/a/b/d/vi;)V

    return-object v0
.end method

.method private j(Ljava/lang/Object;)Ljava/util/NavigableSet;
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/a/b/a/c;
        a = "NavigableSet"
    .end annotation

    .prologue
    .line 183
    invoke-super {p0, p1}, Lcom/a/b/d/bb;->h(Ljava/lang/Object;)Ljava/util/SortedSet;

    move-result-object v0

    check-cast v0, Ljava/util/NavigableSet;

    return-object v0
.end method

.method private static z()Lcom/a/b/d/aeq;
    .registers 3

    .prologue
    .line 89
    new-instance v0, Lcom/a/b/d/aeq;

    invoke-static {}, Lcom/a/b/d/yd;->d()Lcom/a/b/d/yd;

    move-result-object v1

    invoke-static {}, Lcom/a/b/d/yd;->d()Lcom/a/b/d/yd;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/a/b/d/aeq;-><init>(Ljava/util/Comparator;Ljava/util/Comparator;)V

    return-object v0
.end method


# virtual methods
.method final a(Ljava/lang/Object;Ljava/util/Collection;)Ljava/util/Collection;
    .registers 5
    .annotation build Lcom/a/b/a/c;
        a = "NavigableSet"
    .end annotation

    .prologue
    .line 195
    new-instance v0, Lcom/a/b/d/af;

    check-cast p2, Ljava/util/NavigableSet;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, p2, v1}, Lcom/a/b/d/af;-><init>(Lcom/a/b/d/n;Ljava/lang/Object;Ljava/util/NavigableSet;Lcom/a/b/d/ab;)V

    return-object v0
.end method

.method final a(Ljava/util/Collection;)Ljava/util/Collection;
    .registers 3
    .annotation build Lcom/a/b/a/c;
        a = "NavigableSet"
    .end annotation

    .prologue
    .line 189
    check-cast p1, Ljava/util/NavigableSet;

    invoke-static {p1}, Lcom/a/b/d/aad;->a(Ljava/util/NavigableSet;)Ljava/util/NavigableSet;

    move-result-object v0

    return-object v0
.end method

.method final synthetic a()Ljava/util/Set;
    .registers 2

    .prologue
    .line 78
    invoke-virtual {p0}, Lcom/a/b/d/aeq;->y()Ljava/util/SortedSet;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Ljava/lang/Object;)Ljava/util/Set;
    .registers 3

    .prologue
    .line 78
    invoke-direct {p0, p1}, Lcom/a/b/d/aeq;->j(Ljava/lang/Object;)Ljava/util/NavigableSet;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/a/b/d/vi;)Z
    .registers 3

    .prologue
    .line 78
    invoke-super {p0, p1}, Lcom/a/b/d/bb;->a(Lcom/a/b/d/vi;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Z
    .registers 4

    .prologue
    .line 78
    invoke-super {p0, p1, p2}, Lcom/a/b/d/bb;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final synthetic b()Ljava/util/Map;
    .registers 2

    .prologue
    .line 78
    .line 5231
    invoke-super {p0}, Lcom/a/b/d/bb;->v()Ljava/util/SortedMap;

    move-result-object v0

    check-cast v0, Ljava/util/NavigableMap;

    .line 78
    return-object v0
.end method

.method public final bridge synthetic b(Ljava/lang/Object;Ljava/lang/Object;)Z
    .registers 4

    .prologue
    .line 78
    invoke-super {p0, p1, p2}, Lcom/a/b/d/bb;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method final synthetic c()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 78
    invoke-virtual {p0}, Lcom/a/b/d/aeq;->y()Ljava/util/SortedSet;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c(Ljava/lang/Object;)Ljava/util/Collection;
    .registers 3

    .prologue
    .line 78
    invoke-direct {p0, p1}, Lcom/a/b/d/aeq;->j(Ljava/lang/Object;)Ljava/util/NavigableSet;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic c(Ljava/lang/Object;Ljava/lang/Iterable;)Z
    .registers 4

    .prologue
    .line 78
    invoke-super {p0, p1, p2}, Lcom/a/b/d/bb;->c(Ljava/lang/Object;Ljava/lang/Iterable;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic c(Ljava/lang/Object;Ljava/lang/Object;)Z
    .registers 4

    .prologue
    .line 78
    invoke-super {p0, p1, p2}, Lcom/a/b/d/bb;->c(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic d(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/SortedSet;
    .registers 4

    .prologue
    .line 78
    invoke-super {p0, p1, p2}, Lcom/a/b/d/bb;->d(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/SortedSet;

    move-result-object v0

    return-object v0
.end method

.method public final d_()Ljava/util/Comparator;
    .registers 2

    .prologue
    .line 162
    iget-object v0, p0, Lcom/a/b/d/aeq;->b:Ljava/util/Comparator;

    return-object v0
.end method

.method final e(Ljava/lang/Object;)Ljava/util/Collection;
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 147
    if-nez p1, :cond_7

    .line 1157
    iget-object v0, p0, Lcom/a/b/d/aeq;->a:Ljava/util/Comparator;

    .line 148
    invoke-interface {v0, p1, p1}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    .line 150
    :cond_7
    invoke-super {p0, p1}, Lcom/a/b/d/bb;->e(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method final synthetic e()Ljava/util/Map;
    .registers 2

    .prologue
    .line 78
    .line 9174
    invoke-super {p0}, Lcom/a/b/d/bb;->w()Ljava/util/SortedMap;

    move-result-object v0

    check-cast v0, Ljava/util/NavigableMap;

    .line 78
    return-object v0
.end method

.method public final bridge synthetic equals(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 78
    invoke-super {p0, p1}, Lcom/a/b/d/bb;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic f()I
    .registers 2

    .prologue
    .line 78
    invoke-super {p0}, Lcom/a/b/d/bb;->f()I

    move-result v0

    return v0
.end method

.method public final bridge synthetic f(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 78
    invoke-super {p0, p1}, Lcom/a/b/d/bb;->f(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic g()V
    .registers 1

    .prologue
    .line 78
    invoke-super {p0}, Lcom/a/b/d/bb;->g()V

    return-void
.end method

.method public final bridge synthetic g(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 78
    invoke-super {p0, p1}, Lcom/a/b/d/bb;->g(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method final synthetic h()Ljava/util/Set;
    .registers 3

    .prologue
    .line 78
    .line 7216
    new-instance v1, Lcom/a/b/d/x;

    .line 8174
    invoke-super {p0}, Lcom/a/b/d/bb;->w()Ljava/util/SortedMap;

    move-result-object v0

    check-cast v0, Ljava/util/NavigableMap;

    .line 7216
    invoke-direct {v1, p0, v0}, Lcom/a/b/d/x;-><init>(Lcom/a/b/d/n;Ljava/util/NavigableMap;)V

    .line 78
    return-object v1
.end method

.method public final synthetic h(Ljava/lang/Object;)Ljava/util/SortedSet;
    .registers 3

    .prologue
    .line 78
    invoke-direct {p0, p1}, Lcom/a/b/d/aeq;->j(Ljava/lang/Object;)Ljava/util/NavigableSet;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic hashCode()I
    .registers 2

    .prologue
    .line 78
    invoke-super {p0}, Lcom/a/b/d/bb;->hashCode()I

    move-result v0

    return v0
.end method

.method public final bridge synthetic i()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 78
    invoke-super {p0}, Lcom/a/b/d/bb;->i()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic i(Ljava/lang/Object;)Ljava/util/SortedSet;
    .registers 3

    .prologue
    .line 78
    invoke-super {p0, p1}, Lcom/a/b/d/bb;->i(Ljava/lang/Object;)Ljava/util/SortedSet;

    move-result-object v0

    return-object v0
.end method

.method final synthetic m()Ljava/util/Map;
    .registers 3

    .prologue
    .line 78
    .line 6237
    new-instance v1, Lcom/a/b/d/w;

    .line 7174
    invoke-super {p0}, Lcom/a/b/d/bb;->w()Ljava/util/SortedMap;

    move-result-object v0

    check-cast v0, Ljava/util/NavigableMap;

    .line 6237
    invoke-direct {v1, p0, v0}, Lcom/a/b/d/w;-><init>(Lcom/a/b/d/n;Ljava/util/NavigableMap;)V

    .line 78
    return-object v1
.end method

.method public final bridge synthetic n()Z
    .registers 2

    .prologue
    .line 78
    invoke-super {p0}, Lcom/a/b/d/bb;->n()Z

    move-result v0

    return v0
.end method

.method public final synthetic p()Ljava/util/Set;
    .registers 2

    .prologue
    .line 78
    .line 6210
    invoke-super {p0}, Lcom/a/b/d/bb;->x()Ljava/util/SortedSet;

    move-result-object v0

    check-cast v0, Ljava/util/NavigableSet;

    .line 78
    return-object v0
.end method

.method public final bridge synthetic q()Lcom/a/b/d/xc;
    .registers 2

    .prologue
    .line 78
    invoke-super {p0}, Lcom/a/b/d/bb;->q()Lcom/a/b/d/xc;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic toString()Ljava/lang/String;
    .registers 2

    .prologue
    .line 78
    invoke-super {p0}, Lcom/a/b/d/bb;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic u()Ljava/util/Set;
    .registers 2

    .prologue
    .line 78
    invoke-super {p0}, Lcom/a/b/d/bb;->u()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic v()Ljava/util/SortedMap;
    .registers 2

    .prologue
    .line 78
    .line 4231
    invoke-super {p0}, Lcom/a/b/d/bb;->v()Ljava/util/SortedMap;

    move-result-object v0

    check-cast v0, Ljava/util/NavigableMap;

    .line 78
    return-object v0
.end method

.method final bridge synthetic w()Ljava/util/SortedMap;
    .registers 2

    .prologue
    .line 78
    .line 4174
    invoke-super {p0}, Lcom/a/b/d/bb;->w()Ljava/util/SortedMap;

    move-result-object v0

    check-cast v0, Ljava/util/NavigableMap;

    .line 78
    return-object v0
.end method

.method public final bridge synthetic x()Ljava/util/SortedSet;
    .registers 2

    .prologue
    .line 78
    .line 3210
    invoke-super {p0}, Lcom/a/b/d/bb;->x()Ljava/util/SortedSet;

    move-result-object v0

    check-cast v0, Ljava/util/NavigableSet;

    .line 78
    return-object v0
.end method

.method final y()Ljava/util/SortedSet;
    .registers 3

    .prologue
    .line 142
    new-instance v0, Ljava/util/TreeSet;

    iget-object v1, p0, Lcom/a/b/d/aeq;->b:Ljava/util/Comparator;

    invoke-direct {v0, v1}, Ljava/util/TreeSet;-><init>(Ljava/util/Comparator;)V

    return-object v0
.end method
