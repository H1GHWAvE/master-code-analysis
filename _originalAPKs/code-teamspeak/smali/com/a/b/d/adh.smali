.class final Lcom/a/b/d/adh;
.super Lcom/a/b/d/adj;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/d/ou;


# static fields
.field private static final f:J


# direct methods
.method constructor <init>(Lcom/a/b/d/ou;)V
    .registers 2

    .prologue
    .line 703
    invoke-direct {p0, p1}, Lcom/a/b/d/adj;-><init>(Lcom/a/b/d/vi;)V

    .line 704
    return-void
.end method

.method private c()Lcom/a/b/d/ou;
    .registers 2

    .prologue
    .line 706
    invoke-super {p0}, Lcom/a/b/d/adj;->a()Lcom/a/b/d/vi;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/ou;

    return-object v0
.end method


# virtual methods
.method final bridge synthetic a()Lcom/a/b/d/vi;
    .registers 2

    .prologue
    .line 699
    .line 4706
    invoke-super {p0}, Lcom/a/b/d/adj;->a()Lcom/a/b/d/vi;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/ou;

    .line 699
    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Ljava/util/List;
    .registers 5

    .prologue
    .line 709
    iget-object v1, p0, Lcom/a/b/d/adh;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 1706
    :try_start_3
    invoke-super {p0}, Lcom/a/b/d/adj;->a()Lcom/a/b/d/vi;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/ou;

    .line 710
    invoke-interface {v0, p1}, Lcom/a/b/d/ou;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iget-object v2, p0, Lcom/a/b/d/adh;->h:Ljava/lang/Object;

    .line 2060
    invoke-static {v0, v2}, Lcom/a/b/d/acu;->a(Ljava/util/List;Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 710
    monitor-exit v1

    return-object v0

    .line 711
    :catchall_15
    move-exception v0

    monitor-exit v1
    :try_end_17
    .catchall {:try_start_3 .. :try_end_17} :catchall_15

    throw v0
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/List;
    .registers 5

    .prologue
    .line 720
    iget-object v1, p0, Lcom/a/b/d/adh;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 3706
    :try_start_3
    invoke-super {p0}, Lcom/a/b/d/adj;->a()Lcom/a/b/d/vi;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/ou;

    .line 721
    invoke-interface {v0, p1, p2}, Lcom/a/b/d/ou;->a(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 722
    :catchall_f
    move-exception v0

    monitor-exit v1
    :try_end_11
    .catchall {:try_start_3 .. :try_end_11} :catchall_f

    throw v0
.end method

.method public final synthetic b(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/Collection;
    .registers 4

    .prologue
    .line 699
    invoke-virtual {p0, p1, p2}, Lcom/a/b/d/adh;->a(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/Object;)Ljava/util/List;
    .registers 4

    .prologue
    .line 714
    iget-object v1, p0, Lcom/a/b/d/adh;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 2706
    :try_start_3
    invoke-super {p0}, Lcom/a/b/d/adj;->a()Lcom/a/b/d/vi;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/ou;

    .line 715
    invoke-interface {v0, p1}, Lcom/a/b/d/ou;->b(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 716
    :catchall_f
    move-exception v0

    monitor-exit v1
    :try_end_11
    .catchall {:try_start_3 .. :try_end_11} :catchall_f

    throw v0
.end method

.method public final synthetic c(Ljava/lang/Object;)Ljava/util/Collection;
    .registers 3

    .prologue
    .line 699
    invoke-virtual {p0, p1}, Lcom/a/b/d/adh;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method final synthetic d()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 699
    .line 5706
    invoke-super {p0}, Lcom/a/b/d/adj;->a()Lcom/a/b/d/vi;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/ou;

    .line 699
    return-object v0
.end method

.method public final synthetic d(Ljava/lang/Object;)Ljava/util/Collection;
    .registers 3

    .prologue
    .line 699
    invoke-virtual {p0, p1}, Lcom/a/b/d/adh;->b(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
