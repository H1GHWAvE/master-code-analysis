.class final Lcom/a/b/d/ny;
.super Lcom/a/b/d/agi;
.source "SourceFile"


# instance fields
.field final a:Ljava/util/Queue;


# direct methods
.method public constructor <init>(Ljava/lang/Iterable;Ljava/util/Comparator;)V
    .registers 6

    .prologue
    .line 1276
    invoke-direct {p0}, Lcom/a/b/d/agi;-><init>()V

    .line 1279
    new-instance v0, Lcom/a/b/d/nz;

    invoke-direct {v0, p0, p2}, Lcom/a/b/d/nz;-><init>(Lcom/a/b/d/ny;Ljava/util/Comparator;)V

    .line 1287
    new-instance v1, Ljava/util/PriorityQueue;

    const/4 v2, 0x2

    invoke-direct {v1, v2, v0}, Ljava/util/PriorityQueue;-><init>(ILjava/util/Comparator;)V

    iput-object v1, p0, Lcom/a/b/d/ny;->a:Ljava/util/Queue;

    .line 1289
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_14
    :goto_14
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_30

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Iterator;

    .line 1290
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_14

    .line 1291
    iget-object v2, p0, Lcom/a/b/d/ny;->a:Ljava/util/Queue;

    invoke-static {v0}, Lcom/a/b/d/nj;->j(Ljava/util/Iterator;)Lcom/a/b/d/yi;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    goto :goto_14

    .line 1294
    :cond_30
    return-void
.end method


# virtual methods
.method public final hasNext()Z
    .registers 2

    .prologue
    .line 1298
    iget-object v0, p0, Lcom/a/b/d/ny;->a:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public final next()Ljava/lang/Object;
    .registers 4

    .prologue
    .line 1303
    iget-object v0, p0, Lcom/a/b/d/ny;->a:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/yi;

    .line 1304
    invoke-interface {v0}, Lcom/a/b/d/yi;->next()Ljava/lang/Object;

    move-result-object v1

    .line 1305
    invoke-interface {v0}, Lcom/a/b/d/yi;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_17

    .line 1306
    iget-object v2, p0, Lcom/a/b/d/ny;->a:Ljava/util/Queue;

    invoke-interface {v2, v0}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 1308
    :cond_17
    return-object v1
.end method
