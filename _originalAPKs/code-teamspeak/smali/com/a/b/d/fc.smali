.class final Lcom/a/b/d/fc;
.super Lcom/a/b/d/me;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/b;
    a = true
    b = true
.end annotation


# direct methods
.method constructor <init>(Ljava/util/Comparator;)V
    .registers 2

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/a/b/d/me;-><init>(Ljava/util/Comparator;)V

    .line 39
    return-void
.end method


# virtual methods
.method final a([Ljava/lang/Object;I)I
    .registers 3

    .prologue
    .line 77
    return p2
.end method

.method final a(Ljava/lang/Object;Z)Lcom/a/b/d/me;
    .registers 3

    .prologue
    .line 119
    return-object p0
.end method

.method final a(Ljava/lang/Object;ZLjava/lang/Object;Z)Lcom/a/b/d/me;
    .registers 5

    .prologue
    .line 114
    return-object p0
.end method

.method final b(Ljava/lang/Object;Z)Lcom/a/b/d/me;
    .registers 3

    .prologue
    .line 108
    return-object p0
.end method

.method final c(Ljava/lang/Object;)I
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 123
    const/4 v0, -0x1

    return v0
.end method

.method public final c()Lcom/a/b/d/agi;
    .registers 2

    .prologue
    .line 59
    invoke-static {}, Lcom/a/b/d/nj;->a()Lcom/a/b/d/agi;

    move-result-object v0

    return-object v0
.end method

.method public final contains(Ljava/lang/Object;)Z
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 51
    const/4 v0, 0x0

    return v0
.end method

.method public final containsAll(Ljava/util/Collection;)Z
    .registers 3

    .prologue
    .line 55
    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public final d()Lcom/a/b/d/agi;
    .registers 2
    .annotation build Lcom/a/b/a/c;
        a = "NavigableSet"
    .end annotation

    .prologue
    .line 64
    invoke-static {}, Lcom/a/b/d/nj;->a()Lcom/a/b/d/agi;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic descendingIterator()Ljava/util/Iterator;
    .registers 2

    .prologue
    .line 1064
    invoke-static {}, Lcom/a/b/d/nj;->a()Lcom/a/b/d/agi;

    move-result-object v0

    .line 34
    return-object v0
.end method

.method final e()Lcom/a/b/d/me;
    .registers 3

    .prologue
    .line 128
    new-instance v0, Lcom/a/b/d/fc;

    iget-object v1, p0, Lcom/a/b/d/fc;->d:Ljava/util/Comparator;

    invoke-static {v1}, Lcom/a/b/d/yd;->a(Ljava/util/Comparator;)Lcom/a/b/d/yd;

    move-result-object v1

    invoke-virtual {v1}, Lcom/a/b/d/yd;->a()Lcom/a/b/d/yd;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/a/b/d/fc;-><init>(Ljava/util/Comparator;)V

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 81
    instance-of v0, p1, Ljava/util/Set;

    if-eqz v0, :cond_b

    .line 82
    check-cast p1, Ljava/util/Set;

    .line 83
    invoke-interface {p1}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    .line 85
    :goto_a
    return v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public final f()Lcom/a/b/d/jl;
    .registers 2

    .prologue
    .line 72
    invoke-static {}, Lcom/a/b/d/jl;->d()Lcom/a/b/d/jl;

    move-result-object v0

    return-object v0
.end method

.method public final first()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 98
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0
.end method

.method final h_()Z
    .registers 2

    .prologue
    .line 68
    const/4 v0, 0x0

    return v0
.end method

.method public final hashCode()I
    .registers 2

    .prologue
    .line 89
    const/4 v0, 0x0

    return v0
.end method

.method public final isEmpty()Z
    .registers 2

    .prologue
    .line 47
    const/4 v0, 0x1

    return v0
.end method

.method public final synthetic iterator()Ljava/util/Iterator;
    .registers 2

    .prologue
    .line 2059
    invoke-static {}, Lcom/a/b/d/nj;->a()Lcom/a/b/d/agi;

    move-result-object v0

    .line 34
    return-object v0
.end method

.method public final last()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 103
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0
.end method

.method public final size()I
    .registers 2

    .prologue
    .line 43
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .registers 2

    .prologue
    .line 93
    const-string v0, "[]"

    return-object v0
.end method
