.class public abstract enum Lcom/a/b/d/abg;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lcom/a/b/d/abg;

.field public static final enum b:Lcom/a/b/d/abg;

.field public static final enum c:Lcom/a/b/d/abg;

.field public static final enum d:Lcom/a/b/d/abg;

.field public static final enum e:Lcom/a/b/d/abg;

.field private static final synthetic f:[Lcom/a/b/d/abg;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    .line 53
    new-instance v0, Lcom/a/b/d/abh;

    const-string v1, "ANY_PRESENT"

    invoke-direct {v0, v1}, Lcom/a/b/d/abh;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/a/b/d/abg;->a:Lcom/a/b/d/abg;

    .line 63
    new-instance v0, Lcom/a/b/d/abi;

    const-string v1, "LAST_PRESENT"

    invoke-direct {v0, v1}, Lcom/a/b/d/abi;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/a/b/d/abg;->b:Lcom/a/b/d/abg;

    .line 87
    new-instance v0, Lcom/a/b/d/abj;

    const-string v1, "FIRST_PRESENT"

    invoke-direct {v0, v1}, Lcom/a/b/d/abj;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/a/b/d/abg;->c:Lcom/a/b/d/abg;

    .line 113
    new-instance v0, Lcom/a/b/d/abk;

    const-string v1, "FIRST_AFTER"

    invoke-direct {v0, v1}, Lcom/a/b/d/abk;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/a/b/d/abg;->d:Lcom/a/b/d/abg;

    .line 124
    new-instance v0, Lcom/a/b/d/abl;

    const-string v1, "LAST_BEFORE"

    invoke-direct {v0, v1}, Lcom/a/b/d/abl;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/a/b/d/abg;->e:Lcom/a/b/d/abg;

    .line 48
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/a/b/d/abg;

    const/4 v1, 0x0

    sget-object v2, Lcom/a/b/d/abg;->a:Lcom/a/b/d/abg;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/a/b/d/abg;->b:Lcom/a/b/d/abg;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/a/b/d/abg;->c:Lcom/a/b/d/abg;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Lcom/a/b/d/abg;->d:Lcom/a/b/d/abg;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, Lcom/a/b/d/abg;->e:Lcom/a/b/d/abg;

    aput-object v2, v0, v1

    sput-object v0, Lcom/a/b/d/abg;->f:[Lcom/a/b/d/abg;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3

    .prologue
    .line 48
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;IB)V
    .registers 4

    .prologue
    .line 48
    invoke-direct {p0, p1, p2}, Lcom/a/b/d/abg;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/a/b/d/abg;
    .registers 2

    .prologue
    .line 48
    const-class v0, Lcom/a/b/d/abg;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/abg;

    return-object v0
.end method

.method public static values()[Lcom/a/b/d/abg;
    .registers 1

    .prologue
    .line 48
    sget-object v0, Lcom/a/b/d/abg;->f:[Lcom/a/b/d/abg;

    invoke-virtual {v0}, [Lcom/a/b/d/abg;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/a/b/d/abg;

    return-object v0
.end method


# virtual methods
.method abstract a(Ljava/util/Comparator;Ljava/lang/Object;Ljava/util/List;I)I
.end method
