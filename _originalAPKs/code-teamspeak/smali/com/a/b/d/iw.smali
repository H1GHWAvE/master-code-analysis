.class public final Lcom/a/b/d/iw;
.super Lcom/a/b/d/gs;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/d/ck;
.implements Ljava/io/Serializable;


# instance fields
.field private final a:Lcom/a/b/d/jt;


# direct methods
.method private constructor <init>(Lcom/a/b/d/jt;)V
    .registers 2

    .prologue
    .line 138
    invoke-direct {p0}, Lcom/a/b/d/gs;-><init>()V

    .line 139
    iput-object p1, p0, Lcom/a/b/d/iw;->a:Lcom/a/b/d/jt;

    .line 140
    return-void
.end method

.method synthetic constructor <init>(Lcom/a/b/d/jt;B)V
    .registers 3

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lcom/a/b/d/iw;-><init>(Lcom/a/b/d/jt;)V

    return-void
.end method

.method private static a(Ljava/util/Map;)Lcom/a/b/d/iw;
    .registers 7

    .prologue
    .line 126
    instance-of v0, p0, Lcom/a/b/d/iw;

    if-eqz v0, :cond_7

    .line 129
    check-cast p0, Lcom/a/b/d/iw;

    .line 132
    :goto_6
    return-object p0

    :cond_7
    new-instance v2, Lcom/a/b/d/iy;

    invoke-direct {v2}, Lcom/a/b/d/iy;-><init>()V

    .line 1088
    invoke-interface {p0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_14
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_38

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1089
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Class;

    .line 1090
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    .line 1091
    iget-object v4, v2, Lcom/a/b/d/iy;->a:Lcom/a/b/d/ju;

    .line 1097
    invoke-static {v1}, Lcom/a/b/l/z;->a(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 1091
    invoke-virtual {v4, v1, v0}, Lcom/a/b/d/ju;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/ju;

    goto :goto_14

    .line 1107
    :cond_38
    new-instance p0, Lcom/a/b/d/iw;

    iget-object v0, v2, Lcom/a/b/d/iy;->a:Lcom/a/b/d/ju;

    invoke-virtual {v0}, Lcom/a/b/d/ju;->a()Lcom/a/b/d/jt;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/a/b/d/iw;-><init>(Lcom/a/b/d/jt;B)V

    goto :goto_6
.end method

.method private static b()Lcom/a/b/d/iy;
    .registers 1

    .prologue
    .line 44
    new-instance v0, Lcom/a/b/d/iy;

    invoke-direct {v0}, Lcom/a/b/d/iy;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Class;)Ljava/lang/Object;
    .registers 4
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 150
    iget-object v0, p0, Lcom/a/b/d/iw;->a:Lcom/a/b/d/jt;

    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/a/b/d/jt;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 162
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method protected final a()Ljava/util/Map;
    .registers 2

    .prologue
    .line 143
    iget-object v0, p0, Lcom/a/b/d/iw;->a:Lcom/a/b/d/jt;

    return-object v0
.end method

.method protected final bridge synthetic k_()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 35
    .line 1143
    iget-object v0, p0, Lcom/a/b/d/iw;->a:Lcom/a/b/d/jt;

    .line 35
    return-object v0
.end method
