.class Lcom/a/b/d/aaj;
.super Lcom/a/b/d/he;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/c;
    a = "NavigableSet"
.end annotation


# instance fields
.field private final a:Ljava/util/NavigableSet;


# direct methods
.method constructor <init>(Ljava/util/NavigableSet;)V
    .registers 2

    .prologue
    .line 1569
    invoke-direct {p0}, Lcom/a/b/d/he;-><init>()V

    .line 1570
    iput-object p1, p0, Lcom/a/b/d/aaj;->a:Ljava/util/NavigableSet;

    .line 1571
    return-void
.end method

.method private static a(Ljava/util/Comparator;)Lcom/a/b/d/yd;
    .registers 2

    .prologue
    .line 1650
    invoke-static {p0}, Lcom/a/b/d/yd;->a(Ljava/util/Comparator;)Lcom/a/b/d/yd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/yd;->a()Lcom/a/b/d/yd;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected final bridge synthetic a()Ljava/util/Set;
    .registers 2

    .prologue
    .line 1565
    .line 5575
    iget-object v0, p0, Lcom/a/b/d/aaj;->a:Ljava/util/NavigableSet;

    .line 1565
    return-object v0
.end method

.method protected final bridge synthetic b()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 1565
    .line 6575
    iget-object v0, p0, Lcom/a/b/d/aaj;->a:Ljava/util/NavigableSet;

    .line 1565
    return-object v0
.end method

.method protected final bridge synthetic c()Ljava/util/SortedSet;
    .registers 2

    .prologue
    .line 1565
    .line 4575
    iget-object v0, p0, Lcom/a/b/d/aaj;->a:Ljava/util/NavigableSet;

    .line 1565
    return-object v0
.end method

.method public ceiling(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 1590
    iget-object v0, p0, Lcom/a/b/d/aaj;->a:Ljava/util/NavigableSet;

    invoke-interface {v0, p1}, Ljava/util/NavigableSet;->floor(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public comparator()Ljava/util/Comparator;
    .registers 2

    .prologue
    .line 1640
    iget-object v0, p0, Lcom/a/b/d/aaj;->a:Ljava/util/NavigableSet;

    invoke-interface {v0}, Ljava/util/NavigableSet;->comparator()Ljava/util/Comparator;

    move-result-object v0

    .line 1641
    if-nez v0, :cond_11

    .line 1642
    invoke-static {}, Lcom/a/b/d/yd;->d()Lcom/a/b/d/yd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/yd;->a()Lcom/a/b/d/yd;

    move-result-object v0

    .line 1644
    :goto_10
    return-object v0

    .line 2650
    :cond_11
    invoke-static {v0}, Lcom/a/b/d/yd;->a(Ljava/util/Comparator;)Lcom/a/b/d/yd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/yd;->a()Lcom/a/b/d/yd;

    move-result-object v0

    goto :goto_10
.end method

.method protected final d()Ljava/util/NavigableSet;
    .registers 2

    .prologue
    .line 1575
    iget-object v0, p0, Lcom/a/b/d/aaj;->a:Ljava/util/NavigableSet;

    return-object v0
.end method

.method public descendingIterator()Ljava/util/Iterator;
    .registers 2

    .prologue
    .line 1615
    iget-object v0, p0, Lcom/a/b/d/aaj;->a:Ljava/util/NavigableSet;

    invoke-interface {v0}, Ljava/util/NavigableSet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public descendingSet()Ljava/util/NavigableSet;
    .registers 2

    .prologue
    .line 1610
    iget-object v0, p0, Lcom/a/b/d/aaj;->a:Ljava/util/NavigableSet;

    return-object v0
.end method

.method public first()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 1655
    iget-object v0, p0, Lcom/a/b/d/aaj;->a:Ljava/util/NavigableSet;

    invoke-interface {v0}, Ljava/util/NavigableSet;->last()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public floor(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 1585
    iget-object v0, p0, Lcom/a/b/d/aaj;->a:Ljava/util/NavigableSet;

    invoke-interface {v0, p1}, Ljava/util/NavigableSet;->ceiling(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public headSet(Ljava/lang/Object;Z)Ljava/util/NavigableSet;
    .registers 4

    .prologue
    .line 1629
    iget-object v0, p0, Lcom/a/b/d/aaj;->a:Ljava/util/NavigableSet;

    invoke-interface {v0, p1, p2}, Ljava/util/NavigableSet;->tailSet(Ljava/lang/Object;Z)Ljava/util/NavigableSet;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/NavigableSet;->descendingSet()Ljava/util/NavigableSet;

    move-result-object v0

    return-object v0
.end method

.method public headSet(Ljava/lang/Object;)Ljava/util/SortedSet;
    .registers 3

    .prologue
    .line 1660
    .line 3221
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/a/b/d/he;->headSet(Ljava/lang/Object;Z)Ljava/util/NavigableSet;

    move-result-object v0

    .line 1660
    return-object v0
.end method

.method public higher(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 1595
    iget-object v0, p0, Lcom/a/b/d/aaj;->a:Ljava/util/NavigableSet;

    invoke-interface {v0, p1}, Ljava/util/NavigableSet;->lower(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public iterator()Ljava/util/Iterator;
    .registers 2

    .prologue
    .line 1680
    iget-object v0, p0, Lcom/a/b/d/aaj;->a:Ljava/util/NavigableSet;

    invoke-interface {v0}, Ljava/util/NavigableSet;->descendingIterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method protected final bridge synthetic k_()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 1565
    .line 7575
    iget-object v0, p0, Lcom/a/b/d/aaj;->a:Ljava/util/NavigableSet;

    .line 1565
    return-object v0
.end method

.method public last()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 1665
    iget-object v0, p0, Lcom/a/b/d/aaj;->a:Ljava/util/NavigableSet;

    invoke-interface {v0}, Ljava/util/NavigableSet;->first()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public lower(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 1580
    iget-object v0, p0, Lcom/a/b/d/aaj;->a:Ljava/util/NavigableSet;

    invoke-interface {v0, p1}, Ljava/util/NavigableSet;->higher(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public pollFirst()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 1600
    iget-object v0, p0, Lcom/a/b/d/aaj;->a:Ljava/util/NavigableSet;

    invoke-interface {v0}, Ljava/util/NavigableSet;->pollLast()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public pollLast()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 1605
    iget-object v0, p0, Lcom/a/b/d/aaj;->a:Ljava/util/NavigableSet;

    invoke-interface {v0}, Ljava/util/NavigableSet;->pollFirst()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public subSet(Ljava/lang/Object;ZLjava/lang/Object;Z)Ljava/util/NavigableSet;
    .registers 6

    .prologue
    .line 1624
    iget-object v0, p0, Lcom/a/b/d/aaj;->a:Ljava/util/NavigableSet;

    invoke-interface {v0, p3, p4, p1, p2}, Ljava/util/NavigableSet;->subSet(Ljava/lang/Object;ZLjava/lang/Object;Z)Ljava/util/NavigableSet;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/NavigableSet;->descendingSet()Ljava/util/NavigableSet;

    move-result-object v0

    return-object v0
.end method

.method public subSet(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedSet;
    .registers 5

    .prologue
    .line 1670
    .line 4206
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, p2, v1}, Lcom/a/b/d/he;->subSet(Ljava/lang/Object;ZLjava/lang/Object;Z)Ljava/util/NavigableSet;

    move-result-object v0

    .line 1670
    return-object v0
.end method

.method public tailSet(Ljava/lang/Object;Z)Ljava/util/NavigableSet;
    .registers 4

    .prologue
    .line 1634
    iget-object v0, p0, Lcom/a/b/d/aaj;->a:Ljava/util/NavigableSet;

    invoke-interface {v0, p1, p2}, Ljava/util/NavigableSet;->headSet(Ljava/lang/Object;Z)Ljava/util/NavigableSet;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/NavigableSet;->descendingSet()Ljava/util/NavigableSet;

    move-result-object v0

    return-object v0
.end method

.method public tailSet(Ljava/lang/Object;)Ljava/util/SortedSet;
    .registers 3

    .prologue
    .line 1675
    .line 4236
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/a/b/d/he;->tailSet(Ljava/lang/Object;Z)Ljava/util/NavigableSet;

    move-result-object v0

    .line 1675
    return-object v0
.end method

.method public toArray()[Ljava/lang/Object;
    .registers 2

    .prologue
    .line 1685
    invoke-virtual {p0}, Lcom/a/b/d/aaj;->p()[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public toArray([Ljava/lang/Object;)[Ljava/lang/Object;
    .registers 3

    .prologue
    .line 1690
    .line 4253
    invoke-static {p0, p1}, Lcom/a/b/d/yc;->a(Ljava/util/Collection;[Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    .line 1690
    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .registers 2

    .prologue
    .line 1695
    invoke-virtual {p0}, Lcom/a/b/d/aaj;->o()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
