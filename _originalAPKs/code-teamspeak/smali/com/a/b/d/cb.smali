.class final Lcom/a/b/d/cb;
.super Lcom/a/b/d/j;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/a/b/d/bx;

.field private final b:Ljava/util/Deque;

.field private final c:Ljava/util/BitSet;


# direct methods
.method constructor <init>(Lcom/a/b/d/bx;Ljava/lang/Object;)V
    .registers 4

    .prologue
    .line 183
    iput-object p1, p0, Lcom/a/b/d/cb;->a:Lcom/a/b/d/bx;

    invoke-direct {p0}, Lcom/a/b/d/j;-><init>()V

    .line 184
    new-instance v0, Ljava/util/ArrayDeque;

    invoke-direct {v0}, Ljava/util/ArrayDeque;-><init>()V

    iput-object v0, p0, Lcom/a/b/d/cb;->b:Ljava/util/Deque;

    .line 185
    new-instance v0, Ljava/util/BitSet;

    invoke-direct {v0}, Ljava/util/BitSet;-><init>()V

    iput-object v0, p0, Lcom/a/b/d/cb;->c:Ljava/util/BitSet;

    .line 186
    iget-object v0, p0, Lcom/a/b/d/cb;->b:Ljava/util/Deque;

    invoke-interface {v0, p2}, Ljava/util/Deque;->addLast(Ljava/lang/Object;)V

    .line 187
    return-void
.end method


# virtual methods
.method protected final a()Ljava/lang/Object;
    .registers 4

    .prologue
    .line 191
    :goto_0
    iget-object v0, p0, Lcom/a/b/d/cb;->b:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_53

    .line 192
    iget-object v0, p0, Lcom/a/b/d/cb;->b:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->getLast()Ljava/lang/Object;

    move-result-object v0

    .line 193
    iget-object v1, p0, Lcom/a/b/d/cb;->c:Ljava/util/BitSet;

    iget-object v2, p0, Lcom/a/b/d/cb;->b:Ljava/util/Deque;

    invoke-interface {v2}, Ljava/util/Deque;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->get(I)Z

    move-result v1

    if-eqz v1, :cond_3a

    .line 194
    iget-object v1, p0, Lcom/a/b/d/cb;->b:Ljava/util/Deque;

    invoke-interface {v1}, Ljava/util/Deque;->removeLast()Ljava/lang/Object;

    .line 195
    iget-object v1, p0, Lcom/a/b/d/cb;->c:Ljava/util/BitSet;

    iget-object v2, p0, Lcom/a/b/d/cb;->b:Ljava/util/Deque;

    invoke-interface {v2}, Ljava/util/Deque;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->clear(I)V

    .line 196
    iget-object v1, p0, Lcom/a/b/d/cb;->b:Ljava/util/Deque;

    iget-object v2, p0, Lcom/a/b/d/cb;->a:Lcom/a/b/d/bx;

    invoke-virtual {v2}, Lcom/a/b/d/bx;->b()Lcom/a/b/b/ci;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/a/b/d/bx;->a(Ljava/util/Deque;Lcom/a/b/b/ci;)V

    .line 203
    :goto_39
    return-object v0

    .line 199
    :cond_3a
    iget-object v0, p0, Lcom/a/b/d/cb;->c:Ljava/util/BitSet;

    iget-object v1, p0, Lcom/a/b/d/cb;->b:Ljava/util/Deque;

    invoke-interface {v1}, Ljava/util/Deque;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 200
    iget-object v0, p0, Lcom/a/b/d/cb;->b:Ljava/util/Deque;

    iget-object v1, p0, Lcom/a/b/d/cb;->a:Lcom/a/b/d/bx;

    invoke-virtual {v1}, Lcom/a/b/d/bx;->a()Lcom/a/b/b/ci;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/a/b/d/bx;->a(Ljava/util/Deque;Lcom/a/b/b/ci;)V

    goto :goto_0

    .line 203
    :cond_53
    invoke-virtual {p0}, Lcom/a/b/d/cb;->b()Ljava/lang/Object;

    move-result-object v0

    goto :goto_39
.end method
