.class final Lcom/a/b/d/fa;
.super Lcom/a/b/d/lw;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/b;
    b = true
.end annotation


# instance fields
.field private final transient a:Lcom/a/b/d/me;


# direct methods
.method constructor <init>(Ljava/util/Comparator;)V
    .registers 3

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/a/b/d/lw;-><init>()V

    .line 37
    invoke-static {p1}, Lcom/a/b/d/me;->a(Ljava/util/Comparator;)Lcom/a/b/d/me;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/fa;->a:Lcom/a/b/d/me;

    .line 38
    return-void
.end method

.method private constructor <init>(Ljava/util/Comparator;Lcom/a/b/d/lw;)V
    .registers 4

    .prologue
    .line 42
    invoke-direct {p0, p2}, Lcom/a/b/d/lw;-><init>(Lcom/a/b/d/lw;)V

    .line 43
    invoke-static {p1}, Lcom/a/b/d/me;->a(Ljava/util/Comparator;)Lcom/a/b/d/me;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/fa;->a:Lcom/a/b/d/me;

    .line 44
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Z)Lcom/a/b/d/lw;
    .registers 3

    .prologue
    .line 98
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 99
    return-object p0
.end method

.method public final a()Lcom/a/b/d/me;
    .registers 2

    .prologue
    .line 53
    iget-object v0, p0, Lcom/a/b/d/fa;->a:Lcom/a/b/d/me;

    return-object v0
.end method

.method public final b(Ljava/lang/Object;Z)Lcom/a/b/d/lw;
    .registers 3

    .prologue
    .line 104
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 105
    return-object p0
.end method

.method final d()Lcom/a/b/d/lo;
    .registers 3

    .prologue
    .line 88
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "should never be called"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method public final e()Lcom/a/b/d/lo;
    .registers 2

    .prologue
    .line 83
    invoke-static {}, Lcom/a/b/d/lo;->h()Lcom/a/b/d/lo;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic entrySet()Ljava/util/Set;
    .registers 2

    .prologue
    .line 3083
    invoke-static {}, Lcom/a/b/d/lo;->h()Lcom/a/b/d/lo;

    move-result-object v0

    .line 31
    return-object v0
.end method

.method public final f()Lcom/a/b/d/lr;
    .registers 2

    .prologue
    .line 93
    invoke-static {}, Lcom/a/b/d/lr;->a()Lcom/a/b/d/lr;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic g()Lcom/a/b/d/lo;
    .registers 2

    .prologue
    .line 31
    .line 6053
    iget-object v0, p0, Lcom/a/b/d/fa;->a:Lcom/a/b/d/me;

    .line 31
    return-object v0
.end method

.method public final get(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 48
    const/4 v0, 0x0

    return-object v0
.end method

.method public final h()Lcom/a/b/d/iz;
    .registers 2

    .prologue
    .line 68
    invoke-static {}, Lcom/a/b/d/jl;->d()Lcom/a/b/d/jl;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic headMap(Ljava/lang/Object;Z)Ljava/util/NavigableMap;
    .registers 3

    .prologue
    .line 31
    .line 2098
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 31
    return-object p0
.end method

.method final i()Lcom/a/b/d/lw;
    .registers 3

    .prologue
    .line 110
    new-instance v0, Lcom/a/b/d/fa;

    invoke-virtual {p0}, Lcom/a/b/d/fa;->comparator()Ljava/util/Comparator;

    move-result-object v1

    invoke-static {v1}, Lcom/a/b/d/yd;->a(Ljava/util/Comparator;)Lcom/a/b/d/yd;

    move-result-object v1

    invoke-virtual {v1}, Lcom/a/b/d/yd;->a()Lcom/a/b/d/yd;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lcom/a/b/d/fa;-><init>(Ljava/util/Comparator;Lcom/a/b/d/lw;)V

    return-object v0
.end method

.method final i_()Z
    .registers 2

    .prologue
    .line 78
    const/4 v0, 0x0

    return v0
.end method

.method public final isEmpty()Z
    .registers 2

    .prologue
    .line 63
    const/4 v0, 0x1

    return v0
.end method

.method public final bridge synthetic keySet()Ljava/util/Set;
    .registers 2

    .prologue
    .line 31
    .line 5053
    iget-object v0, p0, Lcom/a/b/d/fa;->a:Lcom/a/b/d/me;

    .line 31
    return-object v0
.end method

.method public final size()I
    .registers 2

    .prologue
    .line 58
    const/4 v0, 0x0

    return v0
.end method

.method public final synthetic tailMap(Ljava/lang/Object;Z)Ljava/util/NavigableMap;
    .registers 3

    .prologue
    .line 31
    .line 1104
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 31
    return-object p0
.end method

.method public final toString()Ljava/lang/String;
    .registers 2

    .prologue
    .line 73
    const-string v0, "{}"

    return-object v0
.end method

.method public final synthetic values()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 4068
    invoke-static {}, Lcom/a/b/d/jl;->d()Lcom/a/b/d/jl;

    move-result-object v0

    .line 31
    return-object v0
.end method
