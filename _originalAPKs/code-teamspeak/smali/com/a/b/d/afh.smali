.class final Lcom/a/b/d/afh;
.super Ljava/util/AbstractMap;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/a/b/d/afg;


# direct methods
.method constructor <init>(Lcom/a/b/d/afg;)V
    .registers 2

    .prologue
    .line 435
    iput-object p1, p0, Lcom/a/b/d/afh;->a:Lcom/a/b/d/afg;

    invoke-direct {p0}, Ljava/util/AbstractMap;-><init>()V

    return-void
.end method

.method private a(Lcom/a/b/b/co;)Z
    .registers 6

    .prologue
    .line 5088
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 493
    invoke-virtual {p0}, Lcom/a/b/d/afh;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_d
    :goto_d
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_27

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 494
    invoke-interface {p1, v0}, Lcom/a/b/b/co;->a(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_d

    .line 495
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_d

    .line 498
    :cond_27
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2b
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3f

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/yl;

    .line 499
    iget-object v3, p0, Lcom/a/b/d/afh;->a:Lcom/a/b/d/afg;

    iget-object v3, v3, Lcom/a/b/d/afg;->b:Lcom/a/b/d/afb;

    invoke-virtual {v3, v0}, Lcom/a/b/d/afb;->a(Lcom/a/b/d/yl;)V

    goto :goto_2b

    .line 501
    :cond_3f
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_47

    const/4 v0, 0x1

    :goto_46
    return v0

    :cond_47
    const/4 v0, 0x0

    goto :goto_46
.end method

.method static synthetic a(Lcom/a/b/d/afh;Lcom/a/b/b/co;)Z
    .registers 6

    .prologue
    .line 6088
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 5493
    invoke-virtual {p0}, Lcom/a/b/d/afh;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_d
    :goto_d
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_27

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 5494
    invoke-interface {p1, v0}, Lcom/a/b/b/co;->a(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_d

    .line 5495
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_d

    .line 5498
    :cond_27
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2b
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3f

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/yl;

    .line 5499
    iget-object v3, p0, Lcom/a/b/d/afh;->a:Lcom/a/b/d/afg;

    iget-object v3, v3, Lcom/a/b/d/afg;->b:Lcom/a/b/d/afb;

    invoke-virtual {v3, v0}, Lcom/a/b/d/afb;->a(Lcom/a/b/d/yl;)V

    goto :goto_2b

    .line 5501
    :cond_3f
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_47

    const/4 v0, 0x1

    :goto_46
    return v0

    :cond_47
    const/4 v0, 0x0

    .line 435
    goto :goto_46
.end method


# virtual methods
.method public final clear()V
    .registers 3

    .prologue
    .line 488
    iget-object v0, p0, Lcom/a/b/d/afh;->a:Lcom/a/b/d/afg;

    .line 4392
    iget-object v1, v0, Lcom/a/b/d/afg;->b:Lcom/a/b/d/afb;

    iget-object v0, v0, Lcom/a/b/d/afg;->a:Lcom/a/b/d/yl;

    invoke-virtual {v1, v0}, Lcom/a/b/d/afb;->a(Lcom/a/b/d/yl;)V

    .line 489
    return-void
.end method

.method public final containsKey(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 439
    invoke-virtual {p0, p1}, Lcom/a/b/d/afh;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public final entrySet()Ljava/util/Set;
    .registers 2

    .prologue
    .line 521
    new-instance v0, Lcom/a/b/d/afj;

    invoke-direct {v0, p0}, Lcom/a/b/d/afj;-><init>(Lcom/a/b/d/afh;)V

    return-object v0
.end method

.method public final get(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 6

    .prologue
    const/4 v1, 0x0

    .line 445
    :try_start_1
    instance-of v0, p1, Lcom/a/b/d/yl;

    if-eqz v0, :cond_17

    .line 447
    check-cast p1, Lcom/a/b/d/yl;

    .line 448
    iget-object v0, p0, Lcom/a/b/d/afh;->a:Lcom/a/b/d/afg;

    .line 1316
    iget-object v0, v0, Lcom/a/b/d/afg;->a:Lcom/a/b/d/yl;

    .line 448
    invoke-virtual {v0, p1}, Lcom/a/b/d/yl;->a(Lcom/a/b/d/yl;)Z

    move-result v0

    if-eqz v0, :cond_17

    invoke-virtual {p1}, Lcom/a/b/d/yl;->f()Z

    move-result v0

    if-eqz v0, :cond_18

    .line 471
    :cond_17
    :goto_17
    return-object v1

    .line 452
    :cond_18
    iget-object v0, p1, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    iget-object v2, p0, Lcom/a/b/d/afh;->a:Lcom/a/b/d/afg;

    .line 2316
    iget-object v2, v2, Lcom/a/b/d/afg;->a:Lcom/a/b/d/yl;

    .line 452
    iget-object v2, v2, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    invoke-virtual {v0, v2}, Lcom/a/b/d/dw;->a(Lcom/a/b/d/dw;)I

    move-result v0

    if-nez v0, :cond_5f

    .line 454
    iget-object v0, p0, Lcom/a/b/d/afh;->a:Lcom/a/b/d/afg;

    iget-object v0, v0, Lcom/a/b/d/afg;->b:Lcom/a/b/d/afb;

    invoke-static {v0}, Lcom/a/b/d/afb;->a(Lcom/a/b/d/afb;)Ljava/util/NavigableMap;

    move-result-object v0

    iget-object v2, p1, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    invoke-interface {v0, v2}, Ljava/util/NavigableMap;->floorEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v0

    .line 456
    if-eqz v0, :cond_72

    .line 457
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/aff;

    .line 463
    :goto_3c
    if-eqz v0, :cond_17

    .line 3084
    iget-object v2, v0, Lcom/a/b/d/aff;->a:Lcom/a/b/d/yl;

    .line 463
    iget-object v3, p0, Lcom/a/b/d/afh;->a:Lcom/a/b/d/afg;

    .line 3316
    iget-object v3, v3, Lcom/a/b/d/afg;->a:Lcom/a/b/d/yl;

    .line 463
    invoke-virtual {v2, v3}, Lcom/a/b/d/yl;->b(Lcom/a/b/d/yl;)Z

    move-result v2

    if-eqz v2, :cond_17

    .line 4084
    iget-object v2, v0, Lcom/a/b/d/aff;->a:Lcom/a/b/d/yl;

    .line 463
    iget-object v3, p0, Lcom/a/b/d/afh;->a:Lcom/a/b/d/afg;

    .line 4316
    iget-object v3, v3, Lcom/a/b/d/afg;->a:Lcom/a/b/d/yl;

    .line 463
    invoke-virtual {v2, v3}, Lcom/a/b/d/yl;->c(Lcom/a/b/d/yl;)Lcom/a/b/d/yl;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/a/b/d/yl;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_17

    .line 465
    invoke-virtual {v0}, Lcom/a/b/d/aff;->getValue()Ljava/lang/Object;

    move-result-object v1

    goto :goto_17

    .line 460
    :cond_5f
    iget-object v0, p0, Lcom/a/b/d/afh;->a:Lcom/a/b/d/afg;

    iget-object v0, v0, Lcom/a/b/d/afg;->b:Lcom/a/b/d/afb;

    invoke-static {v0}, Lcom/a/b/d/afb;->a(Lcom/a/b/d/afb;)Ljava/util/NavigableMap;

    move-result-object v0

    iget-object v2, p1, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    invoke-interface {v0, v2}, Ljava/util/NavigableMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/aff;
    :try_end_6f
    .catch Ljava/lang/ClassCastException; {:try_start_1 .. :try_end_6f} :catch_70

    goto :goto_3c

    .line 469
    :catch_70
    move-exception v0

    goto :goto_17

    :cond_72
    move-object v0, v1

    goto :goto_3c
.end method

.method public final keySet()Ljava/util/Set;
    .registers 2

    .prologue
    .line 506
    new-instance v0, Lcom/a/b/d/afi;

    invoke-direct {v0, p0, p0}, Lcom/a/b/d/afi;-><init>(Lcom/a/b/d/afh;Ljava/util/Map;)V

    return-object v0
.end method

.method public final remove(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 476
    invoke-virtual {p0, p1}, Lcom/a/b/d/afh;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 477
    if-eqz v0, :cond_10

    .line 479
    check-cast p1, Lcom/a/b/d/yl;

    .line 480
    iget-object v1, p0, Lcom/a/b/d/afh;->a:Lcom/a/b/d/afg;

    iget-object v1, v1, Lcom/a/b/d/afg;->b:Lcom/a/b/d/afb;

    invoke-virtual {v1, p1}, Lcom/a/b/d/afb;->a(Lcom/a/b/d/yl;)V

    .line 483
    :goto_f
    return-object v0

    :cond_10
    const/4 v0, 0x0

    goto :goto_f
.end method

.method public final values()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 574
    new-instance v0, Lcom/a/b/d/afl;

    invoke-direct {v0, p0, p0}, Lcom/a/b/d/afl;-><init>(Lcom/a/b/d/afh;Ljava/util/Map;)V

    return-object v0
.end method
