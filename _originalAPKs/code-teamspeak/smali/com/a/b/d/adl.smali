.class final Lcom/a/b/d/adl;
.super Lcom/a/b/d/ads;
.source "SourceFile"

# interfaces
.implements Ljava/util/NavigableMap;


# annotations
.annotation build Lcom/a/b/a/c;
    a = "NavigableMap"
.end annotation

.annotation build Lcom/a/b/a/d;
.end annotation


# static fields
.field private static final i:J


# instance fields
.field transient a:Ljava/util/NavigableSet;

.field transient b:Ljava/util/NavigableMap;

.field transient f:Ljava/util/NavigableSet;


# direct methods
.method constructor <init>(Ljava/util/NavigableMap;Ljava/lang/Object;)V
    .registers 3
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1356
    invoke-direct {p0, p1, p2}, Lcom/a/b/d/ads;-><init>(Ljava/util/SortedMap;Ljava/lang/Object;)V

    .line 1357
    return-void
.end method

.method private c()Ljava/util/NavigableMap;
    .registers 2

    .prologue
    .line 1360
    invoke-super {p0}, Lcom/a/b/d/ads;->b()Ljava/util/SortedMap;

    move-result-object v0

    check-cast v0, Ljava/util/NavigableMap;

    return-object v0
.end method


# virtual methods
.method final synthetic a()Ljava/util/Map;
    .registers 2

    .prologue
    .line 1350
    .line 21360
    invoke-super {p0}, Lcom/a/b/d/ads;->b()Ljava/util/SortedMap;

    move-result-object v0

    check-cast v0, Ljava/util/NavigableMap;

    .line 1350
    return-object v0
.end method

.method final bridge synthetic b()Ljava/util/SortedMap;
    .registers 2

    .prologue
    .line 1350
    .line 20360
    invoke-super {p0}, Lcom/a/b/d/ads;->b()Ljava/util/SortedMap;

    move-result-object v0

    check-cast v0, Ljava/util/NavigableMap;

    .line 1350
    return-object v0
.end method

.method public final ceilingEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;
    .registers 5

    .prologue
    .line 1364
    iget-object v1, p0, Lcom/a/b/d/adl;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 2360
    :try_start_3
    invoke-super {p0}, Lcom/a/b/d/ads;->b()Ljava/util/SortedMap;

    move-result-object v0

    check-cast v0, Ljava/util/NavigableMap;

    .line 1365
    invoke-interface {v0, p1}, Ljava/util/NavigableMap;->ceilingEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v0

    iget-object v2, p0, Lcom/a/b/d/adl;->h:Ljava/lang/Object;

    invoke-static {v0, v2}, Lcom/a/b/d/acu;->a(Ljava/util/Map$Entry;Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 1366
    :catchall_15
    move-exception v0

    monitor-exit v1
    :try_end_17
    .catchall {:try_start_3 .. :try_end_17} :catchall_15

    throw v0
.end method

.method public final ceilingKey(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 1370
    iget-object v1, p0, Lcom/a/b/d/adl;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 3360
    :try_start_3
    invoke-super {p0}, Lcom/a/b/d/ads;->b()Ljava/util/SortedMap;

    move-result-object v0

    check-cast v0, Ljava/util/NavigableMap;

    .line 1371
    invoke-interface {v0, p1}, Ljava/util/NavigableMap;->ceilingKey(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 1372
    :catchall_f
    move-exception v0

    monitor-exit v1
    :try_end_11
    .catchall {:try_start_3 .. :try_end_11} :catchall_f

    throw v0
.end method

.method final synthetic d()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 1350
    .line 22360
    invoke-super {p0}, Lcom/a/b/d/ads;->b()Ljava/util/SortedMap;

    move-result-object v0

    check-cast v0, Ljava/util/NavigableMap;

    .line 1350
    return-object v0
.end method

.method public final descendingKeySet()Ljava/util/NavigableSet;
    .registers 4

    .prologue
    .line 1378
    iget-object v1, p0, Lcom/a/b/d/adl;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 1379
    :try_start_3
    iget-object v0, p0, Lcom/a/b/d/adl;->a:Ljava/util/NavigableSet;

    if-nez v0, :cond_1b

    .line 4360
    invoke-super {p0}, Lcom/a/b/d/ads;->b()Ljava/util/SortedMap;

    move-result-object v0

    check-cast v0, Ljava/util/NavigableMap;

    .line 1380
    invoke-interface {v0}, Ljava/util/NavigableMap;->descendingKeySet()Ljava/util/NavigableSet;

    move-result-object v0

    iget-object v2, p0, Lcom/a/b/d/adl;->h:Ljava/lang/Object;

    invoke-static {v0, v2}, Lcom/a/b/d/acu;->a(Ljava/util/NavigableSet;Ljava/lang/Object;)Ljava/util/NavigableSet;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/adl;->a:Ljava/util/NavigableSet;

    monitor-exit v1

    .line 1383
    :goto_1a
    return-object v0

    :cond_1b
    iget-object v0, p0, Lcom/a/b/d/adl;->a:Ljava/util/NavigableSet;

    monitor-exit v1

    goto :goto_1a

    .line 1384
    :catchall_1f
    move-exception v0

    monitor-exit v1
    :try_end_21
    .catchall {:try_start_3 .. :try_end_21} :catchall_1f

    throw v0
.end method

.method public final descendingMap()Ljava/util/NavigableMap;
    .registers 4

    .prologue
    .line 1390
    iget-object v1, p0, Lcom/a/b/d/adl;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 1391
    :try_start_3
    iget-object v0, p0, Lcom/a/b/d/adl;->b:Ljava/util/NavigableMap;

    if-nez v0, :cond_1b

    .line 5360
    invoke-super {p0}, Lcom/a/b/d/ads;->b()Ljava/util/SortedMap;

    move-result-object v0

    check-cast v0, Ljava/util/NavigableMap;

    .line 1392
    invoke-interface {v0}, Ljava/util/NavigableMap;->descendingMap()Ljava/util/NavigableMap;

    move-result-object v0

    iget-object v2, p0, Lcom/a/b/d/adl;->h:Ljava/lang/Object;

    invoke-static {v0, v2}, Lcom/a/b/d/acu;->a(Ljava/util/NavigableMap;Ljava/lang/Object;)Ljava/util/NavigableMap;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/adl;->b:Ljava/util/NavigableMap;

    monitor-exit v1

    .line 1395
    :goto_1a
    return-object v0

    :cond_1b
    iget-object v0, p0, Lcom/a/b/d/adl;->b:Ljava/util/NavigableMap;

    monitor-exit v1

    goto :goto_1a

    .line 1396
    :catchall_1f
    move-exception v0

    monitor-exit v1
    :try_end_21
    .catchall {:try_start_3 .. :try_end_21} :catchall_1f

    throw v0
.end method

.method public final firstEntry()Ljava/util/Map$Entry;
    .registers 4

    .prologue
    .line 1400
    iget-object v1, p0, Lcom/a/b/d/adl;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 6360
    :try_start_3
    invoke-super {p0}, Lcom/a/b/d/ads;->b()Ljava/util/SortedMap;

    move-result-object v0

    check-cast v0, Ljava/util/NavigableMap;

    .line 1401
    invoke-interface {v0}, Ljava/util/NavigableMap;->firstEntry()Ljava/util/Map$Entry;

    move-result-object v0

    iget-object v2, p0, Lcom/a/b/d/adl;->h:Ljava/lang/Object;

    invoke-static {v0, v2}, Lcom/a/b/d/acu;->a(Ljava/util/Map$Entry;Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 1402
    :catchall_15
    move-exception v0

    monitor-exit v1
    :try_end_17
    .catchall {:try_start_3 .. :try_end_17} :catchall_15

    throw v0
.end method

.method public final floorEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;
    .registers 5

    .prologue
    .line 1406
    iget-object v1, p0, Lcom/a/b/d/adl;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 7360
    :try_start_3
    invoke-super {p0}, Lcom/a/b/d/ads;->b()Ljava/util/SortedMap;

    move-result-object v0

    check-cast v0, Ljava/util/NavigableMap;

    .line 1407
    invoke-interface {v0, p1}, Ljava/util/NavigableMap;->floorEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v0

    iget-object v2, p0, Lcom/a/b/d/adl;->h:Ljava/lang/Object;

    invoke-static {v0, v2}, Lcom/a/b/d/acu;->a(Ljava/util/Map$Entry;Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 1408
    :catchall_15
    move-exception v0

    monitor-exit v1
    :try_end_17
    .catchall {:try_start_3 .. :try_end_17} :catchall_15

    throw v0
.end method

.method public final floorKey(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 1412
    iget-object v1, p0, Lcom/a/b/d/adl;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 8360
    :try_start_3
    invoke-super {p0}, Lcom/a/b/d/ads;->b()Ljava/util/SortedMap;

    move-result-object v0

    check-cast v0, Ljava/util/NavigableMap;

    .line 1413
    invoke-interface {v0, p1}, Ljava/util/NavigableMap;->floorKey(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 1414
    :catchall_f
    move-exception v0

    monitor-exit v1
    :try_end_11
    .catchall {:try_start_3 .. :try_end_11} :catchall_f

    throw v0
.end method

.method public final headMap(Ljava/lang/Object;Z)Ljava/util/NavigableMap;
    .registers 6

    .prologue
    .line 1418
    iget-object v1, p0, Lcom/a/b/d/adl;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 9360
    :try_start_3
    invoke-super {p0}, Lcom/a/b/d/ads;->b()Ljava/util/SortedMap;

    move-result-object v0

    check-cast v0, Ljava/util/NavigableMap;

    .line 1419
    invoke-interface {v0, p1, p2}, Ljava/util/NavigableMap;->headMap(Ljava/lang/Object;Z)Ljava/util/NavigableMap;

    move-result-object v0

    iget-object v2, p0, Lcom/a/b/d/adl;->h:Ljava/lang/Object;

    invoke-static {v0, v2}, Lcom/a/b/d/acu;->a(Ljava/util/NavigableMap;Ljava/lang/Object;)Ljava/util/NavigableMap;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 1421
    :catchall_15
    move-exception v0

    monitor-exit v1
    :try_end_17
    .catchall {:try_start_3 .. :try_end_17} :catchall_15

    throw v0
.end method

.method public final headMap(Ljava/lang/Object;)Ljava/util/SortedMap;
    .registers 3

    .prologue
    .line 1499
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/a/b/d/adl;->headMap(Ljava/lang/Object;Z)Ljava/util/NavigableMap;

    move-result-object v0

    return-object v0
.end method

.method public final higherEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;
    .registers 5

    .prologue
    .line 1425
    iget-object v1, p0, Lcom/a/b/d/adl;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 10360
    :try_start_3
    invoke-super {p0}, Lcom/a/b/d/ads;->b()Ljava/util/SortedMap;

    move-result-object v0

    check-cast v0, Ljava/util/NavigableMap;

    .line 1426
    invoke-interface {v0, p1}, Ljava/util/NavigableMap;->higherEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v0

    iget-object v2, p0, Lcom/a/b/d/adl;->h:Ljava/lang/Object;

    invoke-static {v0, v2}, Lcom/a/b/d/acu;->a(Ljava/util/Map$Entry;Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 1427
    :catchall_15
    move-exception v0

    monitor-exit v1
    :try_end_17
    .catchall {:try_start_3 .. :try_end_17} :catchall_15

    throw v0
.end method

.method public final higherKey(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 1431
    iget-object v1, p0, Lcom/a/b/d/adl;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 11360
    :try_start_3
    invoke-super {p0}, Lcom/a/b/d/ads;->b()Ljava/util/SortedMap;

    move-result-object v0

    check-cast v0, Ljava/util/NavigableMap;

    .line 1432
    invoke-interface {v0, p1}, Ljava/util/NavigableMap;->higherKey(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 1433
    :catchall_f
    move-exception v0

    monitor-exit v1
    :try_end_11
    .catchall {:try_start_3 .. :try_end_11} :catchall_f

    throw v0
.end method

.method public final keySet()Ljava/util/Set;
    .registers 2

    .prologue
    .line 1455
    invoke-virtual {p0}, Lcom/a/b/d/adl;->navigableKeySet()Ljava/util/NavigableSet;

    move-result-object v0

    return-object v0
.end method

.method public final lastEntry()Ljava/util/Map$Entry;
    .registers 4

    .prologue
    .line 1437
    iget-object v1, p0, Lcom/a/b/d/adl;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 12360
    :try_start_3
    invoke-super {p0}, Lcom/a/b/d/ads;->b()Ljava/util/SortedMap;

    move-result-object v0

    check-cast v0, Ljava/util/NavigableMap;

    .line 1438
    invoke-interface {v0}, Ljava/util/NavigableMap;->lastEntry()Ljava/util/Map$Entry;

    move-result-object v0

    iget-object v2, p0, Lcom/a/b/d/adl;->h:Ljava/lang/Object;

    invoke-static {v0, v2}, Lcom/a/b/d/acu;->a(Ljava/util/Map$Entry;Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 1439
    :catchall_15
    move-exception v0

    monitor-exit v1
    :try_end_17
    .catchall {:try_start_3 .. :try_end_17} :catchall_15

    throw v0
.end method

.method public final lowerEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;
    .registers 5

    .prologue
    .line 1443
    iget-object v1, p0, Lcom/a/b/d/adl;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 13360
    :try_start_3
    invoke-super {p0}, Lcom/a/b/d/ads;->b()Ljava/util/SortedMap;

    move-result-object v0

    check-cast v0, Ljava/util/NavigableMap;

    .line 1444
    invoke-interface {v0, p1}, Ljava/util/NavigableMap;->lowerEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v0

    iget-object v2, p0, Lcom/a/b/d/adl;->h:Ljava/lang/Object;

    invoke-static {v0, v2}, Lcom/a/b/d/acu;->a(Ljava/util/Map$Entry;Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 1445
    :catchall_15
    move-exception v0

    monitor-exit v1
    :try_end_17
    .catchall {:try_start_3 .. :try_end_17} :catchall_15

    throw v0
.end method

.method public final lowerKey(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 1449
    iget-object v1, p0, Lcom/a/b/d/adl;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 14360
    :try_start_3
    invoke-super {p0}, Lcom/a/b/d/ads;->b()Ljava/util/SortedMap;

    move-result-object v0

    check-cast v0, Ljava/util/NavigableMap;

    .line 1450
    invoke-interface {v0, p1}, Ljava/util/NavigableMap;->lowerKey(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 1451
    :catchall_f
    move-exception v0

    monitor-exit v1
    :try_end_11
    .catchall {:try_start_3 .. :try_end_11} :catchall_f

    throw v0
.end method

.method public final navigableKeySet()Ljava/util/NavigableSet;
    .registers 4

    .prologue
    .line 1461
    iget-object v1, p0, Lcom/a/b/d/adl;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 1462
    :try_start_3
    iget-object v0, p0, Lcom/a/b/d/adl;->f:Ljava/util/NavigableSet;

    if-nez v0, :cond_1b

    .line 15360
    invoke-super {p0}, Lcom/a/b/d/ads;->b()Ljava/util/SortedMap;

    move-result-object v0

    check-cast v0, Ljava/util/NavigableMap;

    .line 1463
    invoke-interface {v0}, Ljava/util/NavigableMap;->navigableKeySet()Ljava/util/NavigableSet;

    move-result-object v0

    iget-object v2, p0, Lcom/a/b/d/adl;->h:Ljava/lang/Object;

    invoke-static {v0, v2}, Lcom/a/b/d/acu;->a(Ljava/util/NavigableSet;Ljava/lang/Object;)Ljava/util/NavigableSet;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/adl;->f:Ljava/util/NavigableSet;

    monitor-exit v1

    .line 1466
    :goto_1a
    return-object v0

    :cond_1b
    iget-object v0, p0, Lcom/a/b/d/adl;->f:Ljava/util/NavigableSet;

    monitor-exit v1

    goto :goto_1a

    .line 1467
    :catchall_1f
    move-exception v0

    monitor-exit v1
    :try_end_21
    .catchall {:try_start_3 .. :try_end_21} :catchall_1f

    throw v0
.end method

.method public final pollFirstEntry()Ljava/util/Map$Entry;
    .registers 4

    .prologue
    .line 1471
    iget-object v1, p0, Lcom/a/b/d/adl;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 16360
    :try_start_3
    invoke-super {p0}, Lcom/a/b/d/ads;->b()Ljava/util/SortedMap;

    move-result-object v0

    check-cast v0, Ljava/util/NavigableMap;

    .line 1472
    invoke-interface {v0}, Ljava/util/NavigableMap;->pollFirstEntry()Ljava/util/Map$Entry;

    move-result-object v0

    iget-object v2, p0, Lcom/a/b/d/adl;->h:Ljava/lang/Object;

    invoke-static {v0, v2}, Lcom/a/b/d/acu;->a(Ljava/util/Map$Entry;Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 1473
    :catchall_15
    move-exception v0

    monitor-exit v1
    :try_end_17
    .catchall {:try_start_3 .. :try_end_17} :catchall_15

    throw v0
.end method

.method public final pollLastEntry()Ljava/util/Map$Entry;
    .registers 4

    .prologue
    .line 1477
    iget-object v1, p0, Lcom/a/b/d/adl;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 17360
    :try_start_3
    invoke-super {p0}, Lcom/a/b/d/ads;->b()Ljava/util/SortedMap;

    move-result-object v0

    check-cast v0, Ljava/util/NavigableMap;

    .line 1478
    invoke-interface {v0}, Ljava/util/NavigableMap;->pollLastEntry()Ljava/util/Map$Entry;

    move-result-object v0

    iget-object v2, p0, Lcom/a/b/d/adl;->h:Ljava/lang/Object;

    invoke-static {v0, v2}, Lcom/a/b/d/acu;->a(Ljava/util/Map$Entry;Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 1479
    :catchall_15
    move-exception v0

    monitor-exit v1
    :try_end_17
    .catchall {:try_start_3 .. :try_end_17} :catchall_15

    throw v0
.end method

.method public final subMap(Ljava/lang/Object;ZLjava/lang/Object;Z)Ljava/util/NavigableMap;
    .registers 8

    .prologue
    .line 1484
    iget-object v1, p0, Lcom/a/b/d/adl;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 18360
    :try_start_3
    invoke-super {p0}, Lcom/a/b/d/ads;->b()Ljava/util/SortedMap;

    move-result-object v0

    check-cast v0, Ljava/util/NavigableMap;

    .line 1485
    invoke-interface {v0, p1, p2, p3, p4}, Ljava/util/NavigableMap;->subMap(Ljava/lang/Object;ZLjava/lang/Object;Z)Ljava/util/NavigableMap;

    move-result-object v0

    iget-object v2, p0, Lcom/a/b/d/adl;->h:Ljava/lang/Object;

    invoke-static {v0, v2}, Lcom/a/b/d/acu;->a(Ljava/util/NavigableMap;Ljava/lang/Object;)Ljava/util/NavigableMap;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 1488
    :catchall_15
    move-exception v0

    monitor-exit v1
    :try_end_17
    .catchall {:try_start_3 .. :try_end_17} :catchall_15

    throw v0
.end method

.method public final subMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedMap;
    .registers 5

    .prologue
    .line 1503
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, p2, v1}, Lcom/a/b/d/adl;->subMap(Ljava/lang/Object;ZLjava/lang/Object;Z)Ljava/util/NavigableMap;

    move-result-object v0

    return-object v0
.end method

.method public final tailMap(Ljava/lang/Object;Z)Ljava/util/NavigableMap;
    .registers 6

    .prologue
    .line 1492
    iget-object v1, p0, Lcom/a/b/d/adl;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 19360
    :try_start_3
    invoke-super {p0}, Lcom/a/b/d/ads;->b()Ljava/util/SortedMap;

    move-result-object v0

    check-cast v0, Ljava/util/NavigableMap;

    .line 1493
    invoke-interface {v0, p1, p2}, Ljava/util/NavigableMap;->tailMap(Ljava/lang/Object;Z)Ljava/util/NavigableMap;

    move-result-object v0

    iget-object v2, p0, Lcom/a/b/d/adl;->h:Ljava/lang/Object;

    invoke-static {v0, v2}, Lcom/a/b/d/acu;->a(Ljava/util/NavigableMap;Ljava/lang/Object;)Ljava/util/NavigableMap;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 1495
    :catchall_15
    move-exception v0

    monitor-exit v1
    :try_end_17
    .catchall {:try_start_3 .. :try_end_17} :catchall_15

    throw v0
.end method

.method public final tailMap(Ljava/lang/Object;)Ljava/util/SortedMap;
    .registers 3

    .prologue
    .line 1507
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/a/b/d/adl;->tailMap(Ljava/lang/Object;Z)Ljava/util/NavigableMap;

    move-result-object v0

    return-object v0
.end method
