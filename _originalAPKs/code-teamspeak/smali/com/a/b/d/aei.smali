.class Lcom/a/b/d/aei;
.super Lcom/a/b/d/hr;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final b:J


# instance fields
.field final a:Lcom/a/b/d/adv;


# direct methods
.method constructor <init>(Lcom/a/b/d/adv;)V
    .registers 3

    .prologue
    .line 462
    invoke-direct {p0}, Lcom/a/b/d/hr;-><init>()V

    .line 463
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/adv;

    iput-object v0, p0, Lcom/a/b/d/aei;->a:Lcom/a/b/d/adv;

    .line 464
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 5
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 500
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public a()Ljava/util/Set;
    .registers 2

    .prologue
    .line 520
    invoke-super {p0}, Lcom/a/b/d/hr;->a()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/a/b/d/adv;)V
    .registers 3

    .prologue
    .line 505
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b()Ljava/util/Set;
    .registers 2

    .prologue
    .line 489
    invoke-super {p0}, Lcom/a/b/d/hr;->b()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 510
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final d(Ljava/lang/Object;)Ljava/util/Map;
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 484
    invoke-super {p0, p1}, Lcom/a/b/d/hr;->d(Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public final d()V
    .registers 2

    .prologue
    .line 479
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final e(Ljava/lang/Object;)Ljava/util/Map;
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 515
    invoke-super {p0, p1}, Lcom/a/b/d/hr;->e(Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public final e()Ljava/util/Set;
    .registers 2

    .prologue
    .line 474
    invoke-super {p0}, Lcom/a/b/d/hr;->e()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method protected f()Lcom/a/b/d/adv;
    .registers 2

    .prologue
    .line 469
    iget-object v0, p0, Lcom/a/b/d/aei;->a:Lcom/a/b/d/adv;

    return-object v0
.end method

.method public final h()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 531
    invoke-super {p0}, Lcom/a/b/d/hr;->h()Ljava/util/Collection;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableCollection(Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic k_()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 458
    invoke-virtual {p0}, Lcom/a/b/d/aei;->f()Lcom/a/b/d/adv;

    move-result-object v0

    return-object v0
.end method

.method public final l()Ljava/util/Map;
    .registers 3

    .prologue
    .line 494
    invoke-static {}, Lcom/a/b/d/adx;->a()Lcom/a/b/b/bj;

    move-result-object v0

    .line 495
    invoke-super {p0}, Lcom/a/b/d/hr;->l()Ljava/util/Map;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/a/b/d/sz;->a(Ljava/util/Map;Lcom/a/b/b/bj;)Ljava/util/Map;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public m()Ljava/util/Map;
    .registers 3

    .prologue
    .line 525
    invoke-static {}, Lcom/a/b/d/adx;->a()Lcom/a/b/b/bj;

    move-result-object v0

    .line 526
    invoke-super {p0}, Lcom/a/b/d/hr;->m()Ljava/util/Map;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/a/b/d/sz;->a(Ljava/util/Map;Lcom/a/b/b/bj;)Ljava/util/Map;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method
