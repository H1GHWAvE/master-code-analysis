.class public final Lcom/a/b/k/g;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/a;
.end annotation

.annotation build Lcom/a/b/a/b;
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final A:Lcom/a/b/k/g;

.field public static final B:Lcom/a/b/k/g;

.field public static final C:Lcom/a/b/k/g;

.field public static final D:Lcom/a/b/k/g;

.field public static final E:Lcom/a/b/k/g;

.field public static final F:Lcom/a/b/k/g;

.field public static final G:Lcom/a/b/k/g;

.field public static final H:Lcom/a/b/k/g;

.field public static final I:Lcom/a/b/k/g;

.field public static final J:Lcom/a/b/k/g;

.field public static final K:Lcom/a/b/k/g;

.field public static final L:Lcom/a/b/k/g;

.field public static final M:Lcom/a/b/k/g;

.field public static final N:Lcom/a/b/k/g;

.field public static final O:Lcom/a/b/k/g;

.field public static final P:Lcom/a/b/k/g;

.field public static final Q:Lcom/a/b/k/g;

.field public static final R:Lcom/a/b/k/g;

.field public static final S:Lcom/a/b/k/g;

.field public static final T:Lcom/a/b/k/g;

.field public static final U:Lcom/a/b/k/g;

.field public static final V:Lcom/a/b/k/g;

.field public static final W:Lcom/a/b/k/g;

.field public static final X:Lcom/a/b/k/g;

.field public static final Y:Lcom/a/b/k/g;

.field public static final Z:Lcom/a/b/k/g;

.field public static final a:Lcom/a/b/k/g;

.field private static final aA:Lcom/a/b/d/jr;

.field private static final aB:Lcom/a/b/b/m;

.field private static final aC:Lcom/a/b/b/m;

.field private static final aD:Lcom/a/b/b/m;

.field private static final aE:Ljava/lang/String; = "application"

.field private static final aF:Ljava/lang/String; = "audio"

.field private static final aG:Ljava/lang/String; = "image"

.field private static final aH:Ljava/lang/String; = "text"

.field private static final aI:Ljava/lang/String; = "video"

.field private static final aJ:Ljava/lang/String; = "*"

.field private static final aK:Ljava/util/Map;

.field private static final aO:Lcom/a/b/b/bz;

.field public static final aa:Lcom/a/b/k/g;

.field public static final ab:Lcom/a/b/k/g;

.field public static final ac:Lcom/a/b/k/g;

.field public static final ad:Lcom/a/b/k/g;

.field public static final ae:Lcom/a/b/k/g;

.field public static final af:Lcom/a/b/k/g;

.field public static final ag:Lcom/a/b/k/g;

.field public static final ah:Lcom/a/b/k/g;

.field public static final ai:Lcom/a/b/k/g;

.field public static final aj:Lcom/a/b/k/g;

.field public static final ak:Lcom/a/b/k/g;

.field public static final al:Lcom/a/b/k/g;

.field public static final am:Lcom/a/b/k/g;

.field public static final an:Lcom/a/b/k/g;

.field public static final ao:Lcom/a/b/k/g;

.field public static final ap:Lcom/a/b/k/g;

.field public static final aq:Lcom/a/b/k/g;

.field public static final ar:Lcom/a/b/k/g;

.field public static final as:Lcom/a/b/k/g;

.field public static final at:Lcom/a/b/k/g;

.field public static final au:Lcom/a/b/k/g;

.field public static final av:Lcom/a/b/k/g;

.field public static final aw:Lcom/a/b/k/g;

.field public static final ax:Lcom/a/b/k/g;

.field public static final ay:Lcom/a/b/k/g;

.field private static final az:Ljava/lang/String; = "charset"

.field public static final b:Lcom/a/b/k/g;

.field public static final c:Lcom/a/b/k/g;

.field public static final d:Lcom/a/b/k/g;

.field public static final e:Lcom/a/b/k/g;

.field public static final f:Lcom/a/b/k/g;

.field public static final g:Lcom/a/b/k/g;

.field public static final h:Lcom/a/b/k/g;

.field public static final i:Lcom/a/b/k/g;

.field public static final j:Lcom/a/b/k/g;

.field public static final k:Lcom/a/b/k/g;

.field public static final l:Lcom/a/b/k/g;

.field public static final m:Lcom/a/b/k/g;

.field public static final n:Lcom/a/b/k/g;

.field public static final o:Lcom/a/b/k/g;

.field public static final p:Lcom/a/b/k/g;

.field public static final q:Lcom/a/b/k/g;

.field public static final r:Lcom/a/b/k/g;

.field public static final s:Lcom/a/b/k/g;

.field public static final t:Lcom/a/b/k/g;

.field public static final u:Lcom/a/b/k/g;

.field public static final v:Lcom/a/b/k/g;

.field public static final w:Lcom/a/b/k/g;

.field public static final x:Lcom/a/b/k/g;

.field public static final y:Lcom/a/b/k/g;

.field public static final z:Lcom/a/b/k/g;


# instance fields
.field private final aL:Ljava/lang/String;

.field private final aM:Ljava/lang/String;

.field private final aN:Lcom/a/b/d/jr;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 85
    const-string v0, "charset"

    sget-object v1, Lcom/a/b/b/aj;->c:Ljava/nio/charset/Charset;

    invoke-virtual {v1}, Ljava/nio/charset/Charset;->name()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/a/b/b/e;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/a/b/d/jr;->d(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/jr;

    move-result-object v0

    sput-object v0, Lcom/a/b/k/g;->aA:Lcom/a/b/d/jr;

    .line 89
    sget-object v0, Lcom/a/b/b/m;->b:Lcom/a/b/b/m;

    sget-object v1, Lcom/a/b/b/m;->i:Lcom/a/b/b/m;

    invoke-virtual {v1}, Lcom/a/b/b/m;->a()Lcom/a/b/b/m;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/a/b/b/m;->a(Lcom/a/b/b/m;)Lcom/a/b/b/m;

    move-result-object v0

    const/16 v1, 0x20

    invoke-static {v1}, Lcom/a/b/b/m;->b(C)Lcom/a/b/b/m;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/a/b/b/m;->a(Lcom/a/b/b/m;)Lcom/a/b/b/m;

    move-result-object v0

    const-string v1, "()<>@,;:\\\"/[]?="

    invoke-static {v1}, Lcom/a/b/b/m;->b(Ljava/lang/CharSequence;)Lcom/a/b/b/m;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/a/b/b/m;->a(Lcom/a/b/b/m;)Lcom/a/b/b/m;

    move-result-object v0

    sput-object v0, Lcom/a/b/k/g;->aB:Lcom/a/b/b/m;

    .line 92
    sget-object v0, Lcom/a/b/b/m;->b:Lcom/a/b/b/m;

    const-string v1, "\"\\\r"

    invoke-static {v1}, Lcom/a/b/b/m;->b(Ljava/lang/CharSequence;)Lcom/a/b/b/m;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/a/b/b/m;->a(Lcom/a/b/b/m;)Lcom/a/b/b/m;

    move-result-object v0

    sput-object v0, Lcom/a/b/k/g;->aC:Lcom/a/b/b/m;

    .line 98
    const-string v0, " \t\r\n"

    invoke-static {v0}, Lcom/a/b/b/m;->a(Ljava/lang/CharSequence;)Lcom/a/b/b/m;

    move-result-object v0

    sput-object v0, Lcom/a/b/k/g;->aD:Lcom/a/b/b/m;

    .line 109
    invoke-static {}, Lcom/a/b/d/sz;->c()Ljava/util/HashMap;

    move-result-object v0

    sput-object v0, Lcom/a/b/k/g;->aK:Ljava/util/Map;

    .line 134
    const-string v0, "*"

    const-string v1, "*"

    invoke-static {v0, v1}, Lcom/a/b/k/g;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/a/b/k/g;

    move-result-object v0

    sput-object v0, Lcom/a/b/k/g;->a:Lcom/a/b/k/g;

    .line 135
    const-string v0, "text"

    const-string v1, "*"

    invoke-static {v0, v1}, Lcom/a/b/k/g;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/a/b/k/g;

    move-result-object v0

    sput-object v0, Lcom/a/b/k/g;->b:Lcom/a/b/k/g;

    .line 136
    const-string v0, "image"

    const-string v1, "*"

    invoke-static {v0, v1}, Lcom/a/b/k/g;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/a/b/k/g;

    move-result-object v0

    sput-object v0, Lcom/a/b/k/g;->c:Lcom/a/b/k/g;

    .line 137
    const-string v0, "audio"

    const-string v1, "*"

    invoke-static {v0, v1}, Lcom/a/b/k/g;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/a/b/k/g;

    move-result-object v0

    sput-object v0, Lcom/a/b/k/g;->d:Lcom/a/b/k/g;

    .line 138
    const-string v0, "video"

    const-string v1, "*"

    invoke-static {v0, v1}, Lcom/a/b/k/g;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/a/b/k/g;

    move-result-object v0

    sput-object v0, Lcom/a/b/k/g;->e:Lcom/a/b/k/g;

    .line 139
    const-string v0, "application"

    const-string v1, "*"

    invoke-static {v0, v1}, Lcom/a/b/k/g;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/a/b/k/g;

    move-result-object v0

    sput-object v0, Lcom/a/b/k/g;->f:Lcom/a/b/k/g;

    .line 142
    const-string v0, "text"

    const-string v1, "cache-manifest"

    invoke-static {v0, v1}, Lcom/a/b/k/g;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/a/b/k/g;

    move-result-object v0

    sput-object v0, Lcom/a/b/k/g;->g:Lcom/a/b/k/g;

    .line 144
    const-string v0, "text"

    const-string v1, "css"

    invoke-static {v0, v1}, Lcom/a/b/k/g;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/a/b/k/g;

    move-result-object v0

    sput-object v0, Lcom/a/b/k/g;->h:Lcom/a/b/k/g;

    .line 145
    const-string v0, "text"

    const-string v1, "csv"

    invoke-static {v0, v1}, Lcom/a/b/k/g;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/a/b/k/g;

    move-result-object v0

    sput-object v0, Lcom/a/b/k/g;->i:Lcom/a/b/k/g;

    .line 146
    const-string v0, "text"

    const-string v1, "html"

    invoke-static {v0, v1}, Lcom/a/b/k/g;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/a/b/k/g;

    move-result-object v0

    sput-object v0, Lcom/a/b/k/g;->j:Lcom/a/b/k/g;

    .line 147
    const-string v0, "text"

    const-string v1, "calendar"

    invoke-static {v0, v1}, Lcom/a/b/k/g;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/a/b/k/g;

    move-result-object v0

    sput-object v0, Lcom/a/b/k/g;->k:Lcom/a/b/k/g;

    .line 148
    const-string v0, "text"

    const-string v1, "plain"

    invoke-static {v0, v1}, Lcom/a/b/k/g;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/a/b/k/g;

    move-result-object v0

    sput-object v0, Lcom/a/b/k/g;->l:Lcom/a/b/k/g;

    .line 154
    const-string v0, "text"

    const-string v1, "javascript"

    invoke-static {v0, v1}, Lcom/a/b/k/g;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/a/b/k/g;

    move-result-object v0

    sput-object v0, Lcom/a/b/k/g;->m:Lcom/a/b/k/g;

    .line 161
    const-string v0, "text"

    const-string v1, "tab-separated-values"

    invoke-static {v0, v1}, Lcom/a/b/k/g;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/a/b/k/g;

    move-result-object v0

    sput-object v0, Lcom/a/b/k/g;->n:Lcom/a/b/k/g;

    .line 162
    const-string v0, "text"

    const-string v1, "vcard"

    invoke-static {v0, v1}, Lcom/a/b/k/g;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/a/b/k/g;

    move-result-object v0

    sput-object v0, Lcom/a/b/k/g;->o:Lcom/a/b/k/g;

    .line 163
    const-string v0, "text"

    const-string v1, "vnd.wap.wml"

    invoke-static {v0, v1}, Lcom/a/b/k/g;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/a/b/k/g;

    move-result-object v0

    sput-object v0, Lcom/a/b/k/g;->p:Lcom/a/b/k/g;

    .line 169
    const-string v0, "text"

    const-string v1, "xml"

    invoke-static {v0, v1}, Lcom/a/b/k/g;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/a/b/k/g;

    move-result-object v0

    sput-object v0, Lcom/a/b/k/g;->q:Lcom/a/b/k/g;

    .line 172
    const-string v0, "image"

    const-string v1, "bmp"

    invoke-static {v0, v1}, Lcom/a/b/k/g;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/a/b/k/g;

    move-result-object v0

    sput-object v0, Lcom/a/b/k/g;->r:Lcom/a/b/k/g;

    .line 182
    const-string v0, "image"

    const-string v1, "x-canon-crw"

    invoke-static {v0, v1}, Lcom/a/b/k/g;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/a/b/k/g;

    move-result-object v0

    sput-object v0, Lcom/a/b/k/g;->s:Lcom/a/b/k/g;

    .line 183
    const-string v0, "image"

    const-string v1, "gif"

    invoke-static {v0, v1}, Lcom/a/b/k/g;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/a/b/k/g;

    move-result-object v0

    sput-object v0, Lcom/a/b/k/g;->t:Lcom/a/b/k/g;

    .line 184
    const-string v0, "image"

    const-string v1, "vnd.microsoft.icon"

    invoke-static {v0, v1}, Lcom/a/b/k/g;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/a/b/k/g;

    move-result-object v0

    sput-object v0, Lcom/a/b/k/g;->u:Lcom/a/b/k/g;

    .line 185
    const-string v0, "image"

    const-string v1, "jpeg"

    invoke-static {v0, v1}, Lcom/a/b/k/g;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/a/b/k/g;

    move-result-object v0

    sput-object v0, Lcom/a/b/k/g;->v:Lcom/a/b/k/g;

    .line 186
    const-string v0, "image"

    const-string v1, "png"

    invoke-static {v0, v1}, Lcom/a/b/k/g;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/a/b/k/g;

    move-result-object v0

    sput-object v0, Lcom/a/b/k/g;->w:Lcom/a/b/k/g;

    .line 203
    const-string v0, "image"

    const-string v1, "vnd.adobe.photoshop"

    invoke-static {v0, v1}, Lcom/a/b/k/g;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/a/b/k/g;

    move-result-object v0

    sput-object v0, Lcom/a/b/k/g;->x:Lcom/a/b/k/g;

    .line 204
    const-string v0, "image"

    const-string v1, "svg+xml"

    invoke-static {v0, v1}, Lcom/a/b/k/g;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/a/b/k/g;

    move-result-object v0

    sput-object v0, Lcom/a/b/k/g;->y:Lcom/a/b/k/g;

    .line 205
    const-string v0, "image"

    const-string v1, "tiff"

    invoke-static {v0, v1}, Lcom/a/b/k/g;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/a/b/k/g;

    move-result-object v0

    sput-object v0, Lcom/a/b/k/g;->z:Lcom/a/b/k/g;

    .line 206
    const-string v0, "image"

    const-string v1, "webp"

    invoke-static {v0, v1}, Lcom/a/b/k/g;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/a/b/k/g;

    move-result-object v0

    sput-object v0, Lcom/a/b/k/g;->A:Lcom/a/b/k/g;

    .line 209
    const-string v0, "audio"

    const-string v1, "mp4"

    invoke-static {v0, v1}, Lcom/a/b/k/g;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/a/b/k/g;

    move-result-object v0

    sput-object v0, Lcom/a/b/k/g;->B:Lcom/a/b/k/g;

    .line 210
    const-string v0, "audio"

    const-string v1, "mpeg"

    invoke-static {v0, v1}, Lcom/a/b/k/g;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/a/b/k/g;

    move-result-object v0

    sput-object v0, Lcom/a/b/k/g;->C:Lcom/a/b/k/g;

    .line 211
    const-string v0, "audio"

    const-string v1, "ogg"

    invoke-static {v0, v1}, Lcom/a/b/k/g;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/a/b/k/g;

    move-result-object v0

    sput-object v0, Lcom/a/b/k/g;->D:Lcom/a/b/k/g;

    .line 212
    const-string v0, "audio"

    const-string v1, "webm"

    invoke-static {v0, v1}, Lcom/a/b/k/g;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/a/b/k/g;

    move-result-object v0

    sput-object v0, Lcom/a/b/k/g;->E:Lcom/a/b/k/g;

    .line 215
    const-string v0, "video"

    const-string v1, "mp4"

    invoke-static {v0, v1}, Lcom/a/b/k/g;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/a/b/k/g;

    move-result-object v0

    sput-object v0, Lcom/a/b/k/g;->F:Lcom/a/b/k/g;

    .line 216
    const-string v0, "video"

    const-string v1, "mpeg"

    invoke-static {v0, v1}, Lcom/a/b/k/g;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/a/b/k/g;

    move-result-object v0

    sput-object v0, Lcom/a/b/k/g;->G:Lcom/a/b/k/g;

    .line 217
    const-string v0, "video"

    const-string v1, "ogg"

    invoke-static {v0, v1}, Lcom/a/b/k/g;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/a/b/k/g;

    move-result-object v0

    sput-object v0, Lcom/a/b/k/g;->H:Lcom/a/b/k/g;

    .line 218
    const-string v0, "video"

    const-string v1, "quicktime"

    invoke-static {v0, v1}, Lcom/a/b/k/g;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/a/b/k/g;

    move-result-object v0

    sput-object v0, Lcom/a/b/k/g;->I:Lcom/a/b/k/g;

    .line 219
    const-string v0, "video"

    const-string v1, "webm"

    invoke-static {v0, v1}, Lcom/a/b/k/g;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/a/b/k/g;

    move-result-object v0

    sput-object v0, Lcom/a/b/k/g;->J:Lcom/a/b/k/g;

    .line 220
    const-string v0, "video"

    const-string v1, "x-ms-wmv"

    invoke-static {v0, v1}, Lcom/a/b/k/g;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/a/b/k/g;

    move-result-object v0

    sput-object v0, Lcom/a/b/k/g;->K:Lcom/a/b/k/g;

    .line 228
    const-string v0, "application"

    const-string v1, "xml"

    invoke-static {v0, v1}, Lcom/a/b/k/g;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/a/b/k/g;

    move-result-object v0

    sput-object v0, Lcom/a/b/k/g;->L:Lcom/a/b/k/g;

    .line 229
    const-string v0, "application"

    const-string v1, "atom+xml"

    invoke-static {v0, v1}, Lcom/a/b/k/g;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/a/b/k/g;

    move-result-object v0

    sput-object v0, Lcom/a/b/k/g;->M:Lcom/a/b/k/g;

    .line 230
    const-string v0, "application"

    const-string v1, "x-bzip2"

    invoke-static {v0, v1}, Lcom/a/b/k/g;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/a/b/k/g;

    move-result-object v0

    sput-object v0, Lcom/a/b/k/g;->N:Lcom/a/b/k/g;

    .line 240
    const-string v0, "application"

    const-string v1, "vnd.ms-fontobject"

    invoke-static {v0, v1}, Lcom/a/b/k/g;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/a/b/k/g;

    move-result-object v0

    sput-object v0, Lcom/a/b/k/g;->O:Lcom/a/b/k/g;

    .line 250
    const-string v0, "application"

    const-string v1, "epub+zip"

    invoke-static {v0, v1}, Lcom/a/b/k/g;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/a/b/k/g;

    move-result-object v0

    sput-object v0, Lcom/a/b/k/g;->P:Lcom/a/b/k/g;

    .line 251
    const-string v0, "application"

    const-string v1, "x-www-form-urlencoded"

    invoke-static {v0, v1}, Lcom/a/b/k/g;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/a/b/k/g;

    move-result-object v0

    sput-object v0, Lcom/a/b/k/g;->Q:Lcom/a/b/k/g;

    .line 260
    const-string v0, "application"

    const-string v1, "pkcs12"

    invoke-static {v0, v1}, Lcom/a/b/k/g;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/a/b/k/g;

    move-result-object v0

    sput-object v0, Lcom/a/b/k/g;->R:Lcom/a/b/k/g;

    .line 272
    const-string v0, "application"

    const-string v1, "binary"

    invoke-static {v0, v1}, Lcom/a/b/k/g;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/a/b/k/g;

    move-result-object v0

    sput-object v0, Lcom/a/b/k/g;->S:Lcom/a/b/k/g;

    .line 273
    const-string v0, "application"

    const-string v1, "x-gzip"

    invoke-static {v0, v1}, Lcom/a/b/k/g;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/a/b/k/g;

    move-result-object v0

    sput-object v0, Lcom/a/b/k/g;->T:Lcom/a/b/k/g;

    .line 279
    const-string v0, "application"

    const-string v1, "javascript"

    invoke-static {v0, v1}, Lcom/a/b/k/g;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/a/b/k/g;

    move-result-object v0

    sput-object v0, Lcom/a/b/k/g;->U:Lcom/a/b/k/g;

    .line 281
    const-string v0, "application"

    const-string v1, "json"

    invoke-static {v0, v1}, Lcom/a/b/k/g;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/a/b/k/g;

    move-result-object v0

    sput-object v0, Lcom/a/b/k/g;->V:Lcom/a/b/k/g;

    .line 282
    const-string v0, "application"

    const-string v1, "vnd.google-earth.kml+xml"

    invoke-static {v0, v1}, Lcom/a/b/k/g;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/a/b/k/g;

    move-result-object v0

    sput-object v0, Lcom/a/b/k/g;->W:Lcom/a/b/k/g;

    .line 283
    const-string v0, "application"

    const-string v1, "vnd.google-earth.kmz"

    invoke-static {v0, v1}, Lcom/a/b/k/g;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/a/b/k/g;

    move-result-object v0

    sput-object v0, Lcom/a/b/k/g;->X:Lcom/a/b/k/g;

    .line 284
    const-string v0, "application"

    const-string v1, "mbox"

    invoke-static {v0, v1}, Lcom/a/b/k/g;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/a/b/k/g;

    move-result-object v0

    sput-object v0, Lcom/a/b/k/g;->Y:Lcom/a/b/k/g;

    .line 292
    const-string v0, "application"

    const-string v1, "x-apple-aspen-config"

    invoke-static {v0, v1}, Lcom/a/b/k/g;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/a/b/k/g;

    move-result-object v0

    sput-object v0, Lcom/a/b/k/g;->Z:Lcom/a/b/k/g;

    .line 294
    const-string v0, "application"

    const-string v1, "vnd.ms-excel"

    invoke-static {v0, v1}, Lcom/a/b/k/g;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/a/b/k/g;

    move-result-object v0

    sput-object v0, Lcom/a/b/k/g;->aa:Lcom/a/b/k/g;

    .line 295
    const-string v0, "application"

    const-string v1, "vnd.ms-powerpoint"

    invoke-static {v0, v1}, Lcom/a/b/k/g;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/a/b/k/g;

    move-result-object v0

    sput-object v0, Lcom/a/b/k/g;->ab:Lcom/a/b/k/g;

    .line 297
    const-string v0, "application"

    const-string v1, "msword"

    invoke-static {v0, v1}, Lcom/a/b/k/g;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/a/b/k/g;

    move-result-object v0

    sput-object v0, Lcom/a/b/k/g;->ac:Lcom/a/b/k/g;

    .line 298
    const-string v0, "application"

    const-string v1, "octet-stream"

    invoke-static {v0, v1}, Lcom/a/b/k/g;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/a/b/k/g;

    move-result-object v0

    sput-object v0, Lcom/a/b/k/g;->ad:Lcom/a/b/k/g;

    .line 299
    const-string v0, "application"

    const-string v1, "ogg"

    invoke-static {v0, v1}, Lcom/a/b/k/g;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/a/b/k/g;

    move-result-object v0

    sput-object v0, Lcom/a/b/k/g;->ae:Lcom/a/b/k/g;

    .line 300
    const-string v0, "application"

    const-string v1, "vnd.openxmlformats-officedocument.wordprocessingml.document"

    invoke-static {v0, v1}, Lcom/a/b/k/g;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/a/b/k/g;

    move-result-object v0

    sput-object v0, Lcom/a/b/k/g;->af:Lcom/a/b/k/g;

    .line 302
    const-string v0, "application"

    const-string v1, "vnd.openxmlformats-officedocument.presentationml.presentation"

    invoke-static {v0, v1}, Lcom/a/b/k/g;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/a/b/k/g;

    move-result-object v0

    sput-object v0, Lcom/a/b/k/g;->ag:Lcom/a/b/k/g;

    .line 304
    const-string v0, "application"

    const-string v1, "vnd.openxmlformats-officedocument.spreadsheetml.sheet"

    invoke-static {v0, v1}, Lcom/a/b/k/g;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/a/b/k/g;

    move-result-object v0

    sput-object v0, Lcom/a/b/k/g;->ah:Lcom/a/b/k/g;

    .line 306
    const-string v0, "application"

    const-string v1, "vnd.oasis.opendocument.graphics"

    invoke-static {v0, v1}, Lcom/a/b/k/g;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/a/b/k/g;

    move-result-object v0

    sput-object v0, Lcom/a/b/k/g;->ai:Lcom/a/b/k/g;

    .line 308
    const-string v0, "application"

    const-string v1, "vnd.oasis.opendocument.presentation"

    invoke-static {v0, v1}, Lcom/a/b/k/g;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/a/b/k/g;

    move-result-object v0

    sput-object v0, Lcom/a/b/k/g;->aj:Lcom/a/b/k/g;

    .line 310
    const-string v0, "application"

    const-string v1, "vnd.oasis.opendocument.spreadsheet"

    invoke-static {v0, v1}, Lcom/a/b/k/g;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/a/b/k/g;

    move-result-object v0

    sput-object v0, Lcom/a/b/k/g;->ak:Lcom/a/b/k/g;

    .line 312
    const-string v0, "application"

    const-string v1, "vnd.oasis.opendocument.text"

    invoke-static {v0, v1}, Lcom/a/b/k/g;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/a/b/k/g;

    move-result-object v0

    sput-object v0, Lcom/a/b/k/g;->al:Lcom/a/b/k/g;

    .line 314
    const-string v0, "application"

    const-string v1, "pdf"

    invoke-static {v0, v1}, Lcom/a/b/k/g;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/a/b/k/g;

    move-result-object v0

    sput-object v0, Lcom/a/b/k/g;->am:Lcom/a/b/k/g;

    .line 315
    const-string v0, "application"

    const-string v1, "postscript"

    invoke-static {v0, v1}, Lcom/a/b/k/g;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/a/b/k/g;

    move-result-object v0

    sput-object v0, Lcom/a/b/k/g;->an:Lcom/a/b/k/g;

    .line 321
    const-string v0, "application"

    const-string v1, "protobuf"

    invoke-static {v0, v1}, Lcom/a/b/k/g;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/a/b/k/g;

    move-result-object v0

    sput-object v0, Lcom/a/b/k/g;->ao:Lcom/a/b/k/g;

    .line 322
    const-string v0, "application"

    const-string v1, "rdf+xml"

    invoke-static {v0, v1}, Lcom/a/b/k/g;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/a/b/k/g;

    move-result-object v0

    sput-object v0, Lcom/a/b/k/g;->ap:Lcom/a/b/k/g;

    .line 323
    const-string v0, "application"

    const-string v1, "rtf"

    invoke-static {v0, v1}, Lcom/a/b/k/g;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/a/b/k/g;

    move-result-object v0

    sput-object v0, Lcom/a/b/k/g;->aq:Lcom/a/b/k/g;

    .line 333
    const-string v0, "application"

    const-string v1, "font-sfnt"

    invoke-static {v0, v1}, Lcom/a/b/k/g;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/a/b/k/g;

    move-result-object v0

    sput-object v0, Lcom/a/b/k/g;->ar:Lcom/a/b/k/g;

    .line 334
    const-string v0, "application"

    const-string v1, "x-shockwave-flash"

    invoke-static {v0, v1}, Lcom/a/b/k/g;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/a/b/k/g;

    move-result-object v0

    sput-object v0, Lcom/a/b/k/g;->as:Lcom/a/b/k/g;

    .line 336
    const-string v0, "application"

    const-string v1, "vnd.sketchup.skp"

    invoke-static {v0, v1}, Lcom/a/b/k/g;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/a/b/k/g;

    move-result-object v0

    sput-object v0, Lcom/a/b/k/g;->at:Lcom/a/b/k/g;

    .line 337
    const-string v0, "application"

    const-string v1, "x-tar"

    invoke-static {v0, v1}, Lcom/a/b/k/g;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/a/b/k/g;

    move-result-object v0

    sput-object v0, Lcom/a/b/k/g;->au:Lcom/a/b/k/g;

    .line 347
    const-string v0, "application"

    const-string v1, "font-woff"

    invoke-static {v0, v1}, Lcom/a/b/k/g;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/a/b/k/g;

    move-result-object v0

    sput-object v0, Lcom/a/b/k/g;->av:Lcom/a/b/k/g;

    .line 348
    const-string v0, "application"

    const-string v1, "xhtml+xml"

    invoke-static {v0, v1}, Lcom/a/b/k/g;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/a/b/k/g;

    move-result-object v0

    sput-object v0, Lcom/a/b/k/g;->aw:Lcom/a/b/k/g;

    .line 356
    const-string v0, "application"

    const-string v1, "xrd+xml"

    invoke-static {v0, v1}, Lcom/a/b/k/g;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/a/b/k/g;

    move-result-object v0

    sput-object v0, Lcom/a/b/k/g;->ax:Lcom/a/b/k/g;

    .line 357
    const-string v0, "application"

    const-string v1, "zip"

    invoke-static {v0, v1}, Lcom/a/b/k/g;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/a/b/k/g;

    move-result-object v0

    sput-object v0, Lcom/a/b/k/g;->ay:Lcom/a/b/k/g;

    .line 698
    const-string v0, "; "

    invoke-static {v0}, Lcom/a/b/b/bv;->a(Ljava/lang/String;)Lcom/a/b/b/bv;

    move-result-object v0

    const-string v1, "="

    invoke-virtual {v0, v1}, Lcom/a/b/b/bv;->c(Ljava/lang/String;)Lcom/a/b/b/bz;

    move-result-object v0

    sput-object v0, Lcom/a/b/k/g;->aO:Lcom/a/b/b/bz;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/a/b/d/jr;)V
    .registers 4

    .prologue
    .line 364
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 365
    iput-object p1, p0, Lcom/a/b/k/g;->aL:Ljava/lang/String;

    .line 366
    iput-object p2, p0, Lcom/a/b/k/g;->aM:Ljava/lang/String;

    .line 367
    iput-object p3, p0, Lcom/a/b/k/g;->aN:Lcom/a/b/d/jr;

    .line 368
    return-void
.end method

.method static synthetic a()Lcom/a/b/b/m;
    .registers 1

    .prologue
    .line 83
    sget-object v0, Lcom/a/b/k/g;->aB:Lcom/a/b/b/m;

    return-object v0
.end method

.method private a(Lcom/a/b/d/vi;)Lcom/a/b/k/g;
    .registers 4

    .prologue
    .line 428
    iget-object v0, p0, Lcom/a/b/k/g;->aL:Ljava/lang/String;

    iget-object v1, p0, Lcom/a/b/k/g;->aM:Ljava/lang/String;

    invoke-static {v0, v1, p1}, Lcom/a/b/k/g;->a(Ljava/lang/String;Ljava/lang/String;Lcom/a/b/d/vi;)Lcom/a/b/k/g;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/a/b/k/g;)Lcom/a/b/k/g;
    .registers 2

    .prologue
    .line 120
    sget-object v0, Lcom/a/b/k/g;->aK:Ljava/util/Map;

    invoke-interface {v0, p0, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 121
    return-object p0
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;)Lcom/a/b/k/g;
    .registers 4

    .prologue
    .line 112
    new-instance v0, Lcom/a/b/k/g;

    invoke-static {}, Lcom/a/b/d/jr;->a()Lcom/a/b/d/jr;

    move-result-object v1

    invoke-direct {v0, p0, p1, v1}, Lcom/a/b/k/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/a/b/d/jr;)V

    invoke-static {v0}, Lcom/a/b/k/g;->a(Lcom/a/b/k/g;)Lcom/a/b/k/g;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;Lcom/a/b/d/vi;)Lcom/a/b/k/g;
    .registers 9

    .prologue
    .line 564
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 565
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 566
    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 567
    invoke-static {p0}, Lcom/a/b/k/g;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 568
    invoke-static {p1}, Lcom/a/b/k/g;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 569
    const-string v0, "*"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_21

    const-string v0, "*"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_57

    :cond_21
    const/4 v0, 0x1

    :goto_22
    const-string v1, "A wildcard type cannot be used with a non-wildcard subtype"

    invoke-static {v0, v1}, Lcom/a/b/b/cn;->a(ZLjava/lang/Object;)V

    .line 571
    invoke-static {}, Lcom/a/b/d/jr;->c()Lcom/a/b/d/js;

    move-result-object v4

    .line 572
    invoke-interface {p2}, Lcom/a/b/d/vi;->k()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_33
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_59

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 573
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Lcom/a/b/k/g;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 574
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/a/b/k/g;->e(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v1, v0}, Lcom/a/b/d/js;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/js;

    goto :goto_33

    .line 569
    :cond_57
    const/4 v0, 0x0

    goto :goto_22

    .line 576
    :cond_59
    new-instance v0, Lcom/a/b/k/g;

    invoke-virtual {v4}, Lcom/a/b/d/js;->a()Lcom/a/b/d/jr;

    move-result-object v1

    invoke-direct {v0, v2, v3, v1}, Lcom/a/b/k/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/a/b/d/jr;)V

    .line 578
    sget-object v1, Lcom/a/b/k/g;->aK:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/a/b/b/ca;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/k/g;

    return-object v0
.end method

.method private a(Ljava/nio/charset/Charset;)Lcom/a/b/k/g;
    .registers 9

    .prologue
    .line 466
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 467
    const-string v0, "charset"

    invoke-virtual {p1}, Ljava/nio/charset/Charset;->name()Ljava/lang/String;

    move-result-object v2

    .line 2440
    invoke-static {v0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2441
    invoke-static {v2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2442
    invoke-static {v0}, Lcom/a/b/k/g;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2443
    invoke-static {}, Lcom/a/b/d/jr;->c()Lcom/a/b/d/js;

    move-result-object v4

    .line 2444
    iget-object v0, p0, Lcom/a/b/k/g;->aN:Lcom/a/b/d/jr;

    invoke-virtual {v0}, Lcom/a/b/d/jr;->v()Lcom/a/b/d/iz;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/iz;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_21
    :goto_21
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_41

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2445
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2446
    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_21

    .line 2447
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v4, v1, v0}, Lcom/a/b/d/js;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/js;

    goto :goto_21

    .line 2450
    :cond_41
    invoke-static {v3, v2}, Lcom/a/b/k/g;->e(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v3, v0}, Lcom/a/b/d/js;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/js;

    .line 2451
    new-instance v0, Lcom/a/b/k/g;

    iget-object v1, p0, Lcom/a/b/k/g;->aL:Ljava/lang/String;

    iget-object v2, p0, Lcom/a/b/k/g;->aM:Ljava/lang/String;

    invoke-virtual {v4}, Lcom/a/b/d/js;->a()Lcom/a/b/d/jr;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/a/b/k/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/a/b/d/jr;)V

    .line 2453
    sget-object v1, Lcom/a/b/k/g;->aK:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/a/b/b/ca;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/k/g;

    .line 467
    return-object v0
.end method

.method static synthetic a(Ljava/lang/String;)Ljava/lang/String;
    .registers 9

    .prologue
    const/16 v7, 0x5c

    const/16 v6, 0x22

    .line 3720
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x10

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 3721
    invoke-virtual {p0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_19
    if-ge v0, v3, :cond_2e

    aget-char v4, v2, v0

    .line 3722
    const/16 v5, 0xd

    if-eq v4, v5, :cond_25

    if-eq v4, v7, :cond_25

    if-ne v4, v6, :cond_28

    .line 3723
    :cond_25
    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 3725
    :cond_28
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 3721
    add-int/lit8 v0, v0, 0x1

    goto :goto_19

    .line 3727
    :cond_2e
    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 83
    return-object v0
.end method

.method private static b(Ljava/lang/String;)Lcom/a/b/k/g;
    .registers 2

    .prologue
    .line 523
    const-string v0, "application"

    invoke-static {v0, p0}, Lcom/a/b/k/g;->d(Ljava/lang/String;Ljava/lang/String;)Lcom/a/b/k/g;

    move-result-object v0

    return-object v0
.end method

.method private static b(Ljava/lang/String;Ljava/lang/String;)Lcom/a/b/k/g;
    .registers 4

    .prologue
    .line 116
    new-instance v0, Lcom/a/b/k/g;

    sget-object v1, Lcom/a/b/k/g;->aA:Lcom/a/b/d/jr;

    invoke-direct {v0, p0, p1, v1}, Lcom/a/b/k/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/a/b/d/jr;)V

    invoke-static {v0}, Lcom/a/b/k/g;->a(Lcom/a/b/k/g;)Lcom/a/b/k/g;

    move-result-object v0

    return-object v0
.end method

.method private b()Ljava/lang/String;
    .registers 2

    .prologue
    .line 372
    iget-object v0, p0, Lcom/a/b/k/g;->aL:Ljava/lang/String;

    return-object v0
.end method

.method private b(Lcom/a/b/k/g;)Z
    .registers 4

    .prologue
    .line 502
    iget-object v0, p1, Lcom/a/b/k/g;->aL:Ljava/lang/String;

    const-string v1, "*"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_14

    iget-object v0, p1, Lcom/a/b/k/g;->aL:Ljava/lang/String;

    iget-object v1, p0, Lcom/a/b/k/g;->aL:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3c

    :cond_14
    iget-object v0, p1, Lcom/a/b/k/g;->aM:Ljava/lang/String;

    const-string v1, "*"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_28

    iget-object v0, p1, Lcom/a/b/k/g;->aM:Ljava/lang/String;

    iget-object v1, p0, Lcom/a/b/k/g;->aM:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3c

    :cond_28
    iget-object v0, p0, Lcom/a/b/k/g;->aN:Lcom/a/b/d/jr;

    invoke-virtual {v0}, Lcom/a/b/d/jr;->v()Lcom/a/b/d/iz;

    move-result-object v0

    iget-object v1, p1, Lcom/a/b/k/g;->aN:Lcom/a/b/d/jr;

    invoke-virtual {v1}, Lcom/a/b/d/jr;->v()Lcom/a/b/d/iz;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/a/b/d/iz;->containsAll(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_3c

    const/4 v0, 0x1

    :goto_3b
    return v0

    :cond_3c
    const/4 v0, 0x0

    goto :goto_3b
.end method

.method private static c(Ljava/lang/String;)Lcom/a/b/k/g;
    .registers 2

    .prologue
    .line 532
    const-string v0, "audio"

    invoke-static {v0, p0}, Lcom/a/b/k/g;->d(Ljava/lang/String;Ljava/lang/String;)Lcom/a/b/k/g;

    move-result-object v0

    return-object v0
.end method

.method private c(Ljava/lang/String;Ljava/lang/String;)Lcom/a/b/k/g;
    .registers 9

    .prologue
    .line 440
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 441
    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 442
    invoke-static {p1}, Lcom/a/b/k/g;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 443
    invoke-static {}, Lcom/a/b/d/jr;->c()Lcom/a/b/d/js;

    move-result-object v3

    .line 444
    iget-object v0, p0, Lcom/a/b/k/g;->aN:Lcom/a/b/d/jr;

    invoke-virtual {v0}, Lcom/a/b/d/jr;->v()Lcom/a/b/d/iz;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/iz;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_18
    :goto_18
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_38

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 445
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 446
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_18

    .line 447
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v3, v1, v0}, Lcom/a/b/d/js;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/js;

    goto :goto_18

    .line 450
    :cond_38
    invoke-static {v2, p2}, Lcom/a/b/k/g;->e(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v2, v0}, Lcom/a/b/d/js;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/js;

    .line 451
    new-instance v0, Lcom/a/b/k/g;

    iget-object v1, p0, Lcom/a/b/k/g;->aL:Ljava/lang/String;

    iget-object v2, p0, Lcom/a/b/k/g;->aM:Ljava/lang/String;

    invoke-virtual {v3}, Lcom/a/b/d/js;->a()Lcom/a/b/d/jr;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/a/b/k/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/a/b/d/jr;)V

    .line 453
    sget-object v1, Lcom/a/b/k/g;->aK:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/a/b/b/ca;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/k/g;

    return-object v0
.end method

.method private c()Ljava/lang/String;
    .registers 2

    .prologue
    .line 377
    iget-object v0, p0, Lcom/a/b/k/g;->aM:Ljava/lang/String;

    return-object v0
.end method

.method private d()Lcom/a/b/d/jr;
    .registers 2

    .prologue
    .line 382
    iget-object v0, p0, Lcom/a/b/k/g;->aN:Lcom/a/b/d/jr;

    return-object v0
.end method

.method private static d(Ljava/lang/String;)Lcom/a/b/k/g;
    .registers 2

    .prologue
    .line 541
    const-string v0, "image"

    invoke-static {v0, p0}, Lcom/a/b/k/g;->d(Ljava/lang/String;Ljava/lang/String;)Lcom/a/b/k/g;

    move-result-object v0

    return-object v0
.end method

.method private static d(Ljava/lang/String;Ljava/lang/String;)Lcom/a/b/k/g;
    .registers 3

    .prologue
    .line 514
    invoke-static {}, Lcom/a/b/d/jr;->a()Lcom/a/b/d/jr;

    move-result-object v0

    invoke-static {p0, p1, v0}, Lcom/a/b/k/g;->a(Ljava/lang/String;Ljava/lang/String;Lcom/a/b/d/vi;)Lcom/a/b/k/g;

    move-result-object v0

    return-object v0
.end method

.method private static e(Ljava/lang/String;)Lcom/a/b/k/g;
    .registers 2

    .prologue
    .line 550
    const-string v0, "text"

    invoke-static {v0, p0}, Lcom/a/b/k/g;->d(Ljava/lang/String;Ljava/lang/String;)Lcom/a/b/k/g;

    move-result-object v0

    return-object v0
.end method

.method private static e(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 3

    .prologue
    .line 587
    const-string v0, "charset"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-static {p1}, Lcom/a/b/b/e;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    :cond_c
    return-object p1
.end method

.method private e()Ljava/util/Map;
    .registers 3

    .prologue
    .line 386
    iget-object v0, p0, Lcom/a/b/k/g;->aN:Lcom/a/b/d/jr;

    .line 1477
    iget-object v0, v0, Lcom/a/b/d/kk;->b:Lcom/a/b/d/jt;

    .line 386
    new-instance v1, Lcom/a/b/k/h;

    invoke-direct {v1, p0}, Lcom/a/b/k/h;-><init>(Lcom/a/b/k/g;)V

    invoke-static {v0, v1}, Lcom/a/b/d/sz;->a(Ljava/util/Map;Lcom/a/b/b/bj;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method private f()Lcom/a/b/b/ci;
    .registers 5

    .prologue
    .line 403
    iget-object v0, p0, Lcom/a/b/k/g;->aN:Lcom/a/b/d/jr;

    const-string v1, "charset"

    invoke-virtual {v0, v1}, Lcom/a/b/d/jr;->e(Ljava/lang/Object;)Lcom/a/b/d/jl;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/lo;->a(Ljava/util/Collection;)Lcom/a/b/d/lo;

    move-result-object v0

    .line 404
    invoke-virtual {v0}, Lcom/a/b/d/lo;->size()I

    move-result v1

    packed-switch v1, :pswitch_data_4e

    .line 410
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x21

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Multiple charset values defined: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 406
    :pswitch_3a
    invoke-static {}, Lcom/a/b/b/ci;->f()Lcom/a/b/b/ci;

    move-result-object v0

    .line 408
    :goto_3e
    return-object v0

    :pswitch_3f
    invoke-static {v0}, Lcom/a/b/d/mq;->b(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/b/ci;->b(Ljava/lang/Object;)Lcom/a/b/b/ci;

    move-result-object v0

    goto :goto_3e

    .line 404
    :pswitch_data_4e
    .packed-switch 0x0
        :pswitch_3a
        :pswitch_3f
    .end packed-switch
.end method

.method private static f(Ljava/lang/String;)Lcom/a/b/k/g;
    .registers 2

    .prologue
    .line 559
    const-string v0, "video"

    invoke-static {v0, p0}, Lcom/a/b/k/g;->d(Ljava/lang/String;Ljava/lang/String;)Lcom/a/b/k/g;

    move-result-object v0

    return-object v0
.end method

.method private g()Lcom/a/b/k/g;
    .registers 3

    .prologue
    .line 419
    iget-object v0, p0, Lcom/a/b/k/g;->aN:Lcom/a/b/d/jr;

    invoke-virtual {v0}, Lcom/a/b/d/jr;->n()Z

    move-result v0

    if-eqz v0, :cond_9

    :goto_8
    return-object p0

    :cond_9
    iget-object v0, p0, Lcom/a/b/k/g;->aL:Ljava/lang/String;

    iget-object v1, p0, Lcom/a/b/k/g;->aM:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/a/b/k/g;->d(Ljava/lang/String;Ljava/lang/String;)Lcom/a/b/k/g;

    move-result-object p0

    goto :goto_8
.end method

.method private static g(Ljava/lang/String;)Ljava/lang/String;
    .registers 2

    .prologue
    .line 582
    sget-object v0, Lcom/a/b/k/g;->aB:Lcom/a/b/b/m;

    invoke-virtual {v0, p0}, Lcom/a/b/b/m;->c(Ljava/lang/CharSequence;)Z

    move-result v0

    invoke-static {v0}, Lcom/a/b/b/cn;->a(Z)V

    .line 583
    invoke-static {p0}, Lcom/a/b/b/e;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static h(Ljava/lang/String;)Lcom/a/b/k/g;
    .registers 11

    .prologue
    const/16 v9, 0x5c

    const/16 v8, 0x22

    .line 596
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 597
    new-instance v1, Lcom/a/b/k/j;

    invoke-direct {v1, p0}, Lcom/a/b/k/j;-><init>(Ljava/lang/String;)V

    .line 599
    :try_start_c
    sget-object v0, Lcom/a/b/k/g;->aB:Lcom/a/b/b/m;

    invoke-virtual {v1, v0}, Lcom/a/b/k/j;->b(Lcom/a/b/b/m;)Ljava/lang/String;

    move-result-object v2

    .line 600
    const/16 v0, 0x2f

    invoke-virtual {v1, v0}, Lcom/a/b/k/j;->a(C)C

    .line 601
    sget-object v0, Lcom/a/b/k/g;->aB:Lcom/a/b/b/m;

    invoke-virtual {v1, v0}, Lcom/a/b/k/j;->b(Lcom/a/b/b/m;)Ljava/lang/String;

    move-result-object v3

    .line 602
    invoke-static {}, Lcom/a/b/d/jr;->c()Lcom/a/b/d/js;

    move-result-object v4

    .line 603
    :goto_21
    invoke-virtual {v1}, Lcom/a/b/k/j;->b()Z

    move-result v0

    if-eqz v0, :cond_c8

    .line 604
    const/16 v0, 0x3b

    invoke-virtual {v1, v0}, Lcom/a/b/k/j;->a(C)C

    .line 605
    sget-object v0, Lcom/a/b/k/g;->aD:Lcom/a/b/b/m;

    invoke-virtual {v1, v0}, Lcom/a/b/k/j;->a(Lcom/a/b/b/m;)Ljava/lang/String;

    .line 606
    sget-object v0, Lcom/a/b/k/g;->aB:Lcom/a/b/b/m;

    invoke-virtual {v1, v0}, Lcom/a/b/k/j;->b(Lcom/a/b/b/m;)Ljava/lang/String;

    move-result-object v5

    .line 607
    const/16 v0, 0x3d

    invoke-virtual {v1, v0}, Lcom/a/b/k/j;->a(C)C

    .line 609
    invoke-virtual {v1}, Lcom/a/b/k/j;->a()C

    move-result v0

    if-ne v8, v0, :cond_c1

    .line 610
    const/16 v0, 0x22

    invoke-virtual {v1, v0}, Lcom/a/b/k/j;->a(C)C

    .line 611
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 612
    :goto_4c
    invoke-virtual {v1}, Lcom/a/b/k/j;->a()C

    move-result v6

    if-eq v8, v6, :cond_b3

    .line 613
    invoke-virtual {v1}, Lcom/a/b/k/j;->a()C

    move-result v6

    if-ne v9, v6, :cond_a9

    .line 614
    const/16 v6, 0x5c

    invoke-virtual {v1, v6}, Lcom/a/b/k/j;->a(C)C

    .line 615
    sget-object v6, Lcom/a/b/b/m;->b:Lcom/a/b/b/m;

    .line 2656
    invoke-virtual {v1}, Lcom/a/b/k/j;->b()Z

    move-result v7

    invoke-static {v7}, Lcom/a/b/b/cn;->b(Z)V

    .line 2657
    invoke-virtual {v1}, Lcom/a/b/k/j;->a()C

    move-result v7

    .line 2658
    invoke-virtual {v6, v7}, Lcom/a/b/b/m;->c(C)Z

    move-result v6

    invoke-static {v6}, Lcom/a/b/b/cn;->b(Z)V

    .line 2659
    iget v6, v1, Lcom/a/b/k/j;->b:I

    add-int/lit8 v6, v6, 0x1

    iput v6, v1, Lcom/a/b/k/j;->b:I

    .line 615
    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
    :try_end_7a
    .catch Ljava/lang/IllegalStateException; {:try_start_c .. :try_end_7a} :catch_7b

    goto :goto_4c

    .line 628
    :catch_7b
    move-exception v0

    .line 629
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x12

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Could not parse \'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 617
    :cond_a9
    :try_start_a9
    sget-object v6, Lcom/a/b/k/g;->aC:Lcom/a/b/b/m;

    invoke-virtual {v1, v6}, Lcom/a/b/k/j;->b(Lcom/a/b/b/m;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4c

    .line 620
    :cond_b3
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 621
    const/16 v6, 0x22

    invoke-virtual {v1, v6}, Lcom/a/b/k/j;->a(C)C

    .line 625
    :goto_bc
    invoke-virtual {v4, v5, v0}, Lcom/a/b/d/js;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/js;

    goto/16 :goto_21

    .line 623
    :cond_c1
    sget-object v0, Lcom/a/b/k/g;->aB:Lcom/a/b/b/m;

    invoke-virtual {v1, v0}, Lcom/a/b/k/j;->b(Lcom/a/b/b/m;)Ljava/lang/String;

    move-result-object v0

    goto :goto_bc

    .line 627
    :cond_c8
    invoke-virtual {v4}, Lcom/a/b/d/js;->a()Lcom/a/b/d/jr;

    move-result-object v0

    invoke-static {v2, v3, v0}, Lcom/a/b/k/g;->a(Ljava/lang/String;Ljava/lang/String;Lcom/a/b/d/vi;)Lcom/a/b/k/g;
    :try_end_cf
    .catch Ljava/lang/IllegalStateException; {:try_start_a9 .. :try_end_cf} :catch_7b

    move-result-object v0

    return-object v0
.end method

.method private h()Z
    .registers 3

    .prologue
    .line 472
    const-string v0, "*"

    iget-object v1, p0, Lcom/a/b/k/g;->aL:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_14

    const-string v0, "*"

    iget-object v1, p0, Lcom/a/b/k/g;->aM:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_16

    :cond_14
    const/4 v0, 0x1

    :goto_15
    return v0

    :cond_16
    const/4 v0, 0x0

    goto :goto_15
.end method

.method private static i(Ljava/lang/String;)Ljava/lang/String;
    .registers 9

    .prologue
    const/16 v7, 0x5c

    const/16 v6, 0x22

    .line 720
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x10

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 721
    invoke-virtual {p0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_19
    if-ge v0, v3, :cond_2e

    aget-char v4, v2, v0

    .line 722
    const/16 v5, 0xd

    if-eq v4, v5, :cond_25

    if-eq v4, v7, :cond_25

    if-ne v4, v6, :cond_28

    .line 723
    :cond_25
    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 725
    :cond_28
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 721
    add-int/lit8 v0, v0, 0x1

    goto :goto_19

    .line 727
    :cond_2e
    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .registers 6
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 681
    if-ne p1, p0, :cond_5

    .line 690
    :cond_4
    :goto_4
    return v0

    .line 683
    :cond_5
    instance-of v2, p1, Lcom/a/b/k/g;

    if-eqz v2, :cond_2f

    .line 684
    check-cast p1, Lcom/a/b/k/g;

    .line 685
    iget-object v2, p0, Lcom/a/b/k/g;->aL:Ljava/lang/String;

    iget-object v3, p1, Lcom/a/b/k/g;->aL:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2d

    iget-object v2, p0, Lcom/a/b/k/g;->aM:Ljava/lang/String;

    iget-object v3, p1, Lcom/a/b/k/g;->aM:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2d

    invoke-direct {p0}, Lcom/a/b/k/g;->e()Ljava/util/Map;

    move-result-object v2

    invoke-direct {p1}, Lcom/a/b/k/g;->e()Ljava/util/Map;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    :cond_2d
    move v0, v1

    goto :goto_4

    :cond_2f
    move v0, v1

    .line 690
    goto :goto_4
.end method

.method public final hashCode()I
    .registers 4

    .prologue
    .line 695
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/a/b/k/g;->aL:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/a/b/k/g;->aM:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    invoke-direct {p0}, Lcom/a/b/k/g;->e()Ljava/util/Map;

    move-result-object v2

    aput-object v2, v0, v1

    .line 3084
    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    .line 695
    return v0
.end method

.method public final toString()Ljava/lang/String;
    .registers 5

    .prologue
    .line 705
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/a/b/k/g;->aL:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x2f

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/k/g;->aM:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 706
    iget-object v1, p0, Lcom/a/b/k/g;->aN:Lcom/a/b/d/jr;

    invoke-virtual {v1}, Lcom/a/b/d/jr;->n()Z

    move-result v1

    if-nez v1, :cond_40

    .line 707
    const-string v1, "; "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 708
    iget-object v1, p0, Lcom/a/b/k/g;->aN:Lcom/a/b/d/jr;

    new-instance v2, Lcom/a/b/k/i;

    invoke-direct {v2, p0}, Lcom/a/b/k/i;-><init>(Lcom/a/b/k/g;)V

    .line 3320
    invoke-static {v2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 3321
    invoke-static {v2}, Lcom/a/b/d/sz;->a(Lcom/a/b/b/bj;)Lcom/a/b/d/tv;

    move-result-object v2

    .line 3380
    new-instance v3, Lcom/a/b/d/wu;

    invoke-direct {v3, v1, v2}, Lcom/a/b/d/wu;-><init>(Lcom/a/b/d/ou;Lcom/a/b/d/tv;)V

    .line 714
    sget-object v1, Lcom/a/b/k/g;->aO:Lcom/a/b/b/bz;

    invoke-interface {v3}, Lcom/a/b/d/vi;->k()Ljava/util/Collection;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/a/b/b/bz;->a(Ljava/lang/StringBuilder;Ljava/lang/Iterable;)Ljava/lang/StringBuilder;

    .line 716
    :cond_40
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
