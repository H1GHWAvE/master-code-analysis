.class public final Lcom/a/b/m/w;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/a;
.end annotation


# instance fields
.field private final a:Lcom/a/b/m/z;


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    new-instance v0, Lcom/a/b/m/z;

    invoke-direct {v0}, Lcom/a/b/m/z;-><init>()V

    iput-object v0, p0, Lcom/a/b/m/w;->a:Lcom/a/b/m/z;

    .line 61
    return-void
.end method

.method private constructor <init>(Lcom/a/b/m/z;)V
    .registers 2

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    iput-object p1, p0, Lcom/a/b/m/w;->a:Lcom/a/b/m/z;

    .line 65
    return-void
.end method

.method synthetic constructor <init>(Lcom/a/b/m/z;B)V
    .registers 3

    .prologue
    .line 55
    invoke-direct {p0, p1}, Lcom/a/b/m/w;-><init>(Lcom/a/b/m/z;)V

    return-void
.end method

.method private a(Ljava/lang/reflect/Type;Ljava/lang/reflect/Type;)Lcom/a/b/m/w;
    .registers 6

    .prologue
    .line 91
    invoke-static {}, Lcom/a/b/d/sz;->c()Ljava/util/HashMap;

    move-result-object v2

    .line 92
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/reflect/Type;

    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/reflect/Type;

    invoke-static {v2, v0, v1}, Lcom/a/b/m/w;->a(Ljava/util/Map;Ljava/lang/reflect/Type;Ljava/lang/reflect/Type;)V

    .line 93
    invoke-virtual {p0, v2}, Lcom/a/b/m/w;->a(Ljava/util/Map;)Lcom/a/b/m/w;

    move-result-object v0

    return-object v0
.end method

.method static a(Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 8

    .prologue
    .line 207
    :try_start_0
    invoke-virtual {p0, p1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_3
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_3} :catch_5

    move-result-object v0

    return-object v0

    .line 209
    :catch_5
    move-exception v0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0xa

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " is not a "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private a(Ljava/lang/reflect/ParameterizedType;)Ljava/lang/reflect/ParameterizedType;
    .registers 5

    .prologue
    .line 195
    invoke-interface {p1}, Ljava/lang/reflect/ParameterizedType;->getOwnerType()Ljava/lang/reflect/Type;

    move-result-object v0

    .line 196
    if-nez v0, :cond_1f

    const/4 v0, 0x0

    move-object v1, v0

    .line 197
    :goto_8
    invoke-interface {p1}, Ljava/lang/reflect/ParameterizedType;->getRawType()Ljava/lang/reflect/Type;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/a/b/m/w;->a(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;

    move-result-object v0

    .line 199
    invoke-interface {p1}, Ljava/lang/reflect/ParameterizedType;->getActualTypeArguments()[Ljava/lang/reflect/Type;

    move-result-object v2

    .line 200
    invoke-virtual {p0, v2}, Lcom/a/b/m/w;->a([Ljava/lang/reflect/Type;)[Ljava/lang/reflect/Type;

    move-result-object v2

    .line 201
    check-cast v0, Ljava/lang/Class;

    invoke-static {v1, v0, v2}, Lcom/a/b/m/ay;->a(Ljava/lang/reflect/Type;Ljava/lang/Class;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    return-object v0

    .line 196
    :cond_1f
    invoke-virtual {p0, v0}, Lcom/a/b/m/w;->a(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;

    move-result-object v0

    move-object v1, v0

    goto :goto_8
.end method

.method private a(Ljava/lang/reflect/GenericArrayType;)Ljava/lang/reflect/Type;
    .registers 3

    .prologue
    .line 189
    invoke-interface {p1}, Ljava/lang/reflect/GenericArrayType;->getGenericComponentType()Ljava/lang/reflect/Type;

    move-result-object v0

    .line 190
    invoke-virtual {p0, v0}, Lcom/a/b/m/w;->a(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;

    move-result-object v0

    .line 191
    invoke-static {v0}, Lcom/a/b/m/ay;->a(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/reflect/WildcardType;)Ljava/lang/reflect/WildcardType;
    .registers 5

    .prologue
    .line 182
    invoke-interface {p1}, Ljava/lang/reflect/WildcardType;->getLowerBounds()[Ljava/lang/reflect/Type;

    move-result-object v0

    .line 183
    invoke-interface {p1}, Ljava/lang/reflect/WildcardType;->getUpperBounds()[Ljava/lang/reflect/Type;

    move-result-object v1

    .line 184
    new-instance v2, Lcom/a/b/m/bp;

    invoke-virtual {p0, v0}, Lcom/a/b/m/w;->a([Ljava/lang/reflect/Type;)[Ljava/lang/reflect/Type;

    move-result-object v0

    invoke-virtual {p0, v1}, Lcom/a/b/m/w;->a([Ljava/lang/reflect/Type;)[Ljava/lang/reflect/Type;

    move-result-object v1

    invoke-direct {v2, v0, v1}, Lcom/a/b/m/bp;-><init>([Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)V

    return-object v2
.end method

.method static a(Ljava/util/Map;Ljava/lang/reflect/Type;Ljava/lang/reflect/Type;)V
    .registers 6

    .prologue
    .line 103
    invoke-virtual {p1, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 151
    :goto_6
    return-void

    .line 106
    :cond_7
    new-instance v0, Lcom/a/b/m/x;

    invoke-direct {v0, p0, p2}, Lcom/a/b/m/x;-><init>(Ljava/util/Map;Ljava/lang/reflect/Type;)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/reflect/Type;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-virtual {v0, v1}, Lcom/a/b/m/x;->a([Ljava/lang/reflect/Type;)V

    goto :goto_6
.end method

.method private static synthetic a(Lcom/a/b/m/w;[Ljava/lang/reflect/Type;)[Ljava/lang/reflect/Type;
    .registers 3

    .prologue
    .line 55
    invoke-virtual {p0, p1}, Lcom/a/b/m/w;->a([Ljava/lang/reflect/Type;)[Ljava/lang/reflect/Type;

    move-result-object v0

    return-object v0
.end method

.method private static b(Ljava/lang/reflect/Type;)Lcom/a/b/m/w;
    .registers 3

    .prologue
    .line 68
    new-instance v0, Lcom/a/b/m/w;

    invoke-direct {v0}, Lcom/a/b/m/w;-><init>()V

    invoke-static {p0}, Lcom/a/b/m/y;->a(Ljava/lang/reflect/Type;)Lcom/a/b/d/jt;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/a/b/m/w;->a(Ljava/util/Map;)Lcom/a/b/m/w;

    move-result-object v0

    return-object v0
.end method

.method private static synthetic b(Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 55
    invoke-static {p0, p1}, Lcom/a/b/m/w;->a(Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method private static synthetic b(Ljava/util/Map;Ljava/lang/reflect/Type;Ljava/lang/reflect/Type;)V
    .registers 3

    .prologue
    .line 55
    invoke-static {p0, p1, p2}, Lcom/a/b/m/w;->a(Ljava/util/Map;Ljava/lang/reflect/Type;Ljava/lang/reflect/Type;)V

    return-void
.end method


# virtual methods
.method final a(Ljava/util/Map;)Lcom/a/b/m/w;
    .registers 4

    .prologue
    .line 98
    new-instance v0, Lcom/a/b/m/w;

    iget-object v1, p0, Lcom/a/b/m/w;->a:Lcom/a/b/m/z;

    invoke-virtual {v1, p1}, Lcom/a/b/m/z;->a(Ljava/util/Map;)Lcom/a/b/m/z;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/a/b/m/w;-><init>(Lcom/a/b/m/z;)V

    return-object v0
.end method

.method public final a(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;
    .registers 5

    .prologue
    .line 158
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 159
    instance-of v0, p1, Ljava/lang/reflect/TypeVariable;

    if-eqz v0, :cond_15

    .line 160
    iget-object v0, p0, Lcom/a/b/m/w;->a:Lcom/a/b/m/z;

    check-cast p1, Ljava/lang/reflect/TypeVariable;

    .line 1240
    new-instance v1, Lcom/a/b/m/aa;

    invoke-direct {v1, v0, p1, v0}, Lcom/a/b/m/aa;-><init>(Lcom/a/b/m/z;Ljava/lang/reflect/TypeVariable;Lcom/a/b/m/z;)V

    .line 1249
    invoke-virtual {v0, p1, v1}, Lcom/a/b/m/z;->a(Ljava/lang/reflect/TypeVariable;Lcom/a/b/m/z;)Ljava/lang/reflect/Type;

    move-result-object p1

    .line 169
    :cond_14
    :goto_14
    return-object p1

    .line 161
    :cond_15
    instance-of v0, p1, Ljava/lang/reflect/ParameterizedType;

    if-eqz v0, :cond_40

    .line 162
    check-cast p1, Ljava/lang/reflect/ParameterizedType;

    .line 2195
    invoke-interface {p1}, Ljava/lang/reflect/ParameterizedType;->getOwnerType()Ljava/lang/reflect/Type;

    move-result-object v0

    .line 2196
    if-nez v0, :cond_3a

    const/4 v0, 0x0

    move-object v1, v0

    .line 2197
    :goto_23
    invoke-interface {p1}, Ljava/lang/reflect/ParameterizedType;->getRawType()Ljava/lang/reflect/Type;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/a/b/m/w;->a(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;

    move-result-object v0

    .line 2199
    invoke-interface {p1}, Ljava/lang/reflect/ParameterizedType;->getActualTypeArguments()[Ljava/lang/reflect/Type;

    move-result-object v2

    .line 2200
    invoke-virtual {p0, v2}, Lcom/a/b/m/w;->a([Ljava/lang/reflect/Type;)[Ljava/lang/reflect/Type;

    move-result-object v2

    .line 2201
    check-cast v0, Ljava/lang/Class;

    invoke-static {v1, v0, v2}, Lcom/a/b/m/ay;->a(Ljava/lang/reflect/Type;Ljava/lang/Class;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object p1

    goto :goto_14

    .line 2196
    :cond_3a
    invoke-virtual {p0, v0}, Lcom/a/b/m/w;->a(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;

    move-result-object v0

    move-object v1, v0

    goto :goto_23

    .line 163
    :cond_40
    instance-of v0, p1, Ljava/lang/reflect/GenericArrayType;

    if-eqz v0, :cond_53

    .line 164
    check-cast p1, Ljava/lang/reflect/GenericArrayType;

    .line 3189
    invoke-interface {p1}, Ljava/lang/reflect/GenericArrayType;->getGenericComponentType()Ljava/lang/reflect/Type;

    move-result-object v0

    .line 3190
    invoke-virtual {p0, v0}, Lcom/a/b/m/w;->a(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;

    move-result-object v0

    .line 3191
    invoke-static {v0}, Lcom/a/b/m/ay;->a(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;

    move-result-object p1

    goto :goto_14

    .line 165
    :cond_53
    instance-of v0, p1, Ljava/lang/reflect/WildcardType;

    if-eqz v0, :cond_14

    .line 166
    check-cast p1, Ljava/lang/reflect/WildcardType;

    .line 4182
    invoke-interface {p1}, Ljava/lang/reflect/WildcardType;->getLowerBounds()[Ljava/lang/reflect/Type;

    move-result-object v0

    .line 4183
    invoke-interface {p1}, Ljava/lang/reflect/WildcardType;->getUpperBounds()[Ljava/lang/reflect/Type;

    move-result-object v1

    .line 4184
    new-instance p1, Lcom/a/b/m/bp;

    invoke-virtual {p0, v0}, Lcom/a/b/m/w;->a([Ljava/lang/reflect/Type;)[Ljava/lang/reflect/Type;

    move-result-object v0

    invoke-virtual {p0, v1}, Lcom/a/b/m/w;->a([Ljava/lang/reflect/Type;)[Ljava/lang/reflect/Type;

    move-result-object v1

    invoke-direct {p1, v0, v1}, Lcom/a/b/m/bp;-><init>([Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)V

    goto :goto_14
.end method

.method final a([Ljava/lang/reflect/Type;)[Ljava/lang/reflect/Type;
    .registers 5

    .prologue
    .line 174
    array-length v0, p1

    new-array v1, v0, [Ljava/lang/reflect/Type;

    .line 175
    const/4 v0, 0x0

    :goto_4
    array-length v2, p1

    if-ge v0, v2, :cond_12

    .line 176
    aget-object v2, p1, v0

    invoke-virtual {p0, v2}, Lcom/a/b/m/w;->a(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;

    move-result-object v2

    aput-object v2, v1, v0

    .line 175
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 178
    :cond_12
    return-object v1
.end method
