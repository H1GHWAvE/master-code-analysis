.class public final Lcom/a/b/m/b;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/a;
.end annotation


# static fields
.field private static final a:Ljava/util/logging/Logger;

.field private static final b:Lcom/a/b/b/co;

.field private static final c:Lcom/a/b/b/di;

.field private static final d:Ljava/lang/String; = ".class"


# instance fields
.field private final e:Lcom/a/b/d/lo;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 60
    const-class v0, Lcom/a/b/m/b;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/a/b/m/b;->a:Ljava/util/logging/Logger;

    .line 62
    new-instance v0, Lcom/a/b/m/c;

    invoke-direct {v0}, Lcom/a/b/m/c;-><init>()V

    sput-object v0, Lcom/a/b/m/b;->b:Lcom/a/b/b/co;

    .line 69
    const-string v0, " "

    invoke-static {v0}, Lcom/a/b/b/di;->a(Ljava/lang/String;)Lcom/a/b/b/di;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/b/di;->a()Lcom/a/b/b/di;

    move-result-object v0

    sput-object v0, Lcom/a/b/m/b;->c:Lcom/a/b/b/di;

    return-void
.end method

.method private constructor <init>(Lcom/a/b/d/lo;)V
    .registers 2

    .prologue
    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    iput-object p1, p0, Lcom/a/b/m/b;->e:Lcom/a/b/d/lo;

    .line 78
    return-void
.end method

.method private static a(Ljava/lang/ClassLoader;)Lcom/a/b/m/b;
    .registers 5

    .prologue
    .line 90
    new-instance v2, Lcom/a/b/m/f;

    invoke-direct {v2}, Lcom/a/b/m/f;-><init>()V

    .line 91
    invoke-static {p0}, Lcom/a/b/m/b;->b(Ljava/lang/ClassLoader;)Lcom/a/b/d/jt;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/jt;->e()Lcom/a/b/d/lo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/lo;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_11
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2d

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 92
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/net/URI;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ClassLoader;

    invoke-virtual {v2, v1, v0}, Lcom/a/b/m/f;->a(Ljava/net/URI;Ljava/lang/ClassLoader;)V

    goto :goto_11

    .line 94
    :cond_2d
    new-instance v0, Lcom/a/b/m/b;

    .line 1310
    iget-object v1, v2, Lcom/a/b/m/f;->a:Lcom/a/b/d/mf;

    invoke-virtual {v1}, Lcom/a/b/d/mf;->c()Lcom/a/b/d/me;

    move-result-object v1

    .line 94
    invoke-direct {v0, v1}, Lcom/a/b/m/b;-><init>(Lcom/a/b/d/lo;)V

    return-object v0
.end method

.method static a(Ljava/lang/String;)Ljava/lang/String;
    .registers 4
    .annotation build Lcom/a/b/a/d;
    .end annotation

    .prologue
    .line 441
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x6

    .line 442
    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x2f

    const/16 v2, 0x2e

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a()Ljava/util/logging/Logger;
    .registers 1

    .prologue
    .line 59
    sget-object v0, Lcom/a/b/m/b;->a:Ljava/util/logging/Logger;

    return-object v0
.end method

.method static synthetic b()Lcom/a/b/b/di;
    .registers 1

    .prologue
    .line 59
    sget-object v0, Lcom/a/b/m/b;->c:Lcom/a/b/b/di;

    return-object v0
.end method

.method private static b(Ljava/lang/ClassLoader;)Lcom/a/b/d/jt;
    .registers 7
    .annotation build Lcom/a/b/a/d;
    .end annotation

    .prologue
    .line 280
    invoke-static {}, Lcom/a/b/d/sz;->d()Ljava/util/LinkedHashMap;

    move-result-object v1

    .line 282
    invoke-virtual {p0}, Ljava/lang/ClassLoader;->getParent()Ljava/lang/ClassLoader;

    move-result-object v0

    .line 283
    if-eqz v0, :cond_11

    .line 284
    invoke-static {v0}, Lcom/a/b/m/b;->b(Ljava/lang/ClassLoader;)Lcom/a/b/d/jt;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/LinkedHashMap;->putAll(Ljava/util/Map;)V

    .line 286
    :cond_11
    instance-of v0, p0, Ljava/net/URLClassLoader;

    if-eqz v0, :cond_39

    move-object v0, p0

    .line 287
    check-cast v0, Ljava/net/URLClassLoader;

    .line 288
    invoke-virtual {v0}, Ljava/net/URLClassLoader;->getURLs()[Ljava/net/URL;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_1e
    if-ge v0, v3, :cond_39

    aget-object v4, v2, v0

    .line 291
    :try_start_22
    invoke-virtual {v4}, Ljava/net/URL;->toURI()Ljava/net/URI;
    :try_end_25
    .catch Ljava/net/URISyntaxException; {:try_start_22 .. :try_end_25} :catch_32

    move-result-object v4

    .line 295
    invoke-virtual {v1, v4}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2f

    .line 296
    invoke-virtual {v1, v4, p0}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 288
    :cond_2f
    add-int/lit8 v0, v0, 0x1

    goto :goto_1e

    .line 292
    :catch_32
    move-exception v0

    .line 293
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 300
    :cond_39
    invoke-static {v1}, Lcom/a/b/d/jt;->a(Ljava/util/Map;)Lcom/a/b/d/jt;

    move-result-object v0

    return-object v0
.end method

.method private b(Ljava/lang/String;)Lcom/a/b/d/lo;
    .registers 6

    .prologue
    .line 121
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 122
    invoke-static {}, Lcom/a/b/d/lo;->i()Lcom/a/b/d/lp;

    move-result-object v1

    .line 123
    invoke-direct {p0}, Lcom/a/b/m/b;->e()Lcom/a/b/d/lo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/lo;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_f
    :goto_f
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2b

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/m/d;

    .line 3222
    iget-object v3, v0, Lcom/a/b/m/d;->a:Ljava/lang/String;

    invoke-static {v3}, Lcom/a/b/m/t;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 124
    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_f

    .line 125
    invoke-virtual {v1, v0}, Lcom/a/b/d/lp;->c(Ljava/lang/Object;)Lcom/a/b/d/lp;

    goto :goto_f

    .line 128
    :cond_2b
    invoke-virtual {v1}, Lcom/a/b/d/lp;->b()Lcom/a/b/d/lo;

    move-result-object v0

    return-object v0
.end method

.method private c()Lcom/a/b/d/lo;
    .registers 2

    .prologue
    .line 102
    iget-object v0, p0, Lcom/a/b/m/b;->e:Lcom/a/b/d/lo;

    return-object v0
.end method

.method private c(Ljava/lang/String;)Lcom/a/b/d/lo;
    .registers 7

    .prologue
    .line 136
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 137
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 138
    invoke-static {}, Lcom/a/b/d/lo;->i()Lcom/a/b/d/lp;

    move-result-object v2

    .line 139
    invoke-direct {p0}, Lcom/a/b/m/b;->e()Lcom/a/b/d/lo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/lo;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_30
    :goto_30
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_48

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/m/d;

    .line 3255
    iget-object v4, v0, Lcom/a/b/m/d;->a:Ljava/lang/String;

    .line 140
    invoke-virtual {v4, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_30

    .line 141
    invoke-virtual {v2, v0}, Lcom/a/b/d/lp;->c(Ljava/lang/Object;)Lcom/a/b/d/lp;

    goto :goto_30

    .line 144
    :cond_48
    invoke-virtual {v2}, Lcom/a/b/d/lp;->b()Lcom/a/b/d/lo;

    move-result-object v0

    return-object v0
.end method

.method private d()Lcom/a/b/d/lo;
    .registers 3

    .prologue
    .line 111
    iget-object v0, p0, Lcom/a/b/m/b;->e:Lcom/a/b/d/lo;

    invoke-static {v0}, Lcom/a/b/d/gd;->a(Ljava/lang/Iterable;)Lcom/a/b/d/gd;

    move-result-object v0

    const-class v1, Lcom/a/b/m/d;

    invoke-virtual {v0, v1}, Lcom/a/b/d/gd;->a(Ljava/lang/Class;)Lcom/a/b/d/gd;

    move-result-object v0

    .line 1396
    iget-object v0, v0, Lcom/a/b/d/gd;->c:Ljava/lang/Iterable;

    invoke-static {v0}, Lcom/a/b/d/lo;->a(Ljava/lang/Iterable;)Lcom/a/b/d/lo;

    move-result-object v0

    .line 111
    return-object v0
.end method

.method private e()Lcom/a/b/d/lo;
    .registers 3

    .prologue
    .line 116
    iget-object v0, p0, Lcom/a/b/m/b;->e:Lcom/a/b/d/lo;

    invoke-static {v0}, Lcom/a/b/d/gd;->a(Ljava/lang/Iterable;)Lcom/a/b/d/gd;

    move-result-object v0

    const-class v1, Lcom/a/b/m/d;

    invoke-virtual {v0, v1}, Lcom/a/b/d/gd;->a(Ljava/lang/Class;)Lcom/a/b/d/gd;

    move-result-object v0

    sget-object v1, Lcom/a/b/m/b;->b:Lcom/a/b/b/co;

    invoke-virtual {v0, v1}, Lcom/a/b/d/gd;->a(Lcom/a/b/b/co;)Lcom/a/b/d/gd;

    move-result-object v0

    .line 2396
    iget-object v0, v0, Lcom/a/b/d/gd;->c:Ljava/lang/Iterable;

    invoke-static {v0}, Lcom/a/b/d/lo;->a(Ljava/lang/Iterable;)Lcom/a/b/d/lo;

    move-result-object v0

    .line 116
    return-object v0
.end method
