.class final Lcom/a/b/m/ah;
.super Lcom/a/b/m/ax;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/a/b/m/ae;


# direct methods
.method constructor <init>(Lcom/a/b/m/ae;)V
    .registers 2

    .prologue
    .line 726
    iput-object p1, p0, Lcom/a/b/m/ah;->a:Lcom/a/b/m/ae;

    invoke-direct {p0}, Lcom/a/b/m/ax;-><init>()V

    return-void
.end method


# virtual methods
.method final a(Ljava/lang/reflect/GenericArrayType;)V
    .registers 5

    .prologue
    .line 740
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/reflect/Type;

    const/4 v1, 0x0

    invoke-interface {p1}, Ljava/lang/reflect/GenericArrayType;->getGenericComponentType()Ljava/lang/reflect/Type;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Lcom/a/b/m/ah;->a([Ljava/lang/reflect/Type;)V

    .line 741
    return-void
.end method

.method final a(Ljava/lang/reflect/ParameterizedType;)V
    .registers 5

    .prologue
    .line 736
    invoke-interface {p1}, Ljava/lang/reflect/ParameterizedType;->getActualTypeArguments()[Ljava/lang/reflect/Type;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/a/b/m/ah;->a([Ljava/lang/reflect/Type;)V

    .line 737
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/reflect/Type;

    const/4 v1, 0x0

    invoke-interface {p1}, Ljava/lang/reflect/ParameterizedType;->getOwnerType()Ljava/lang/reflect/Type;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Lcom/a/b/m/ah;->a([Ljava/lang/reflect/Type;)V

    .line 738
    return-void
.end method

.method final a(Ljava/lang/reflect/TypeVariable;)V
    .registers 6

    .prologue
    .line 728
    new-instance v0, Ljava/lang/IllegalArgumentException;

    iget-object v1, p0, Lcom/a/b/m/ah;->a:Lcom/a/b/m/ae;

    invoke-static {v1}, Lcom/a/b/m/ae;->b(Lcom/a/b/m/ae;)Ljava/lang/reflect/Type;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x3a

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "contains a type variable and is not safe for the operation"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method final a(Ljava/lang/reflect/WildcardType;)V
    .registers 3

    .prologue
    .line 732
    invoke-interface {p1}, Ljava/lang/reflect/WildcardType;->getLowerBounds()[Ljava/lang/reflect/Type;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/a/b/m/ah;->a([Ljava/lang/reflect/Type;)V

    .line 733
    invoke-interface {p1}, Ljava/lang/reflect/WildcardType;->getUpperBounds()[Ljava/lang/reflect/Type;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/a/b/m/ah;->a([Ljava/lang/reflect/Type;)V

    .line 734
    return-void
.end method
