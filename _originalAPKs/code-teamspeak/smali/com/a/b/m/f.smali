.class final Lcom/a/b/m/f;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/d;
.end annotation


# instance fields
.field final a:Lcom/a/b/d/mf;

.field private final b:Ljava/util/Set;


# direct methods
.method constructor <init>()V
    .registers 3

    .prologue
    .line 303
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 305
    new-instance v0, Lcom/a/b/d/mf;

    invoke-static {}, Lcom/a/b/d/yd;->e()Lcom/a/b/d/yd;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/a/b/d/mf;-><init>(Ljava/util/Comparator;)V

    iput-object v0, p0, Lcom/a/b/m/f;->a:Lcom/a/b/d/mf;

    .line 1164
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 307
    iput-object v0, p0, Lcom/a/b/m/f;->b:Ljava/util/Set;

    return-void
.end method

.method private static a(Ljava/io/File;Ljava/util/jar/Manifest;)Lcom/a/b/d/lo;
    .registers 9
    .param p1    # Ljava/util/jar/Manifest;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/a/b/a/d;
    .end annotation

    .prologue
    .line 401
    if-nez p1, :cond_7

    .line 402
    invoke-static {}, Lcom/a/b/d/lo;->h()Lcom/a/b/d/lo;

    move-result-object v0

    .line 420
    :goto_6
    return-object v0

    .line 404
    :cond_7
    invoke-static {}, Lcom/a/b/d/lo;->i()Lcom/a/b/d/lp;

    move-result-object v2

    .line 405
    invoke-virtual {p1}, Ljava/util/jar/Manifest;->getMainAttributes()Ljava/util/jar/Attributes;

    move-result-object v0

    sget-object v1, Ljava/util/jar/Attributes$Name;->CLASS_PATH:Ljava/util/jar/Attributes$Name;

    invoke-virtual {v1}, Ljava/util/jar/Attributes$Name;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/jar/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 407
    if-eqz v0, :cond_78

    .line 408
    invoke-static {}, Lcom/a/b/m/b;->b()Lcom/a/b/b/di;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/a/b/b/di;->a(Ljava/lang/CharSequence;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_27
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_78

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2431
    :try_start_33
    new-instance v1, Ljava/net/URI;

    invoke-direct {v1, v0}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    .line 2432
    invoke-virtual {v1}, Ljava/net/URI;->isAbsolute()Z
    :try_end_3b
    .catch Ljava/net/URISyntaxException; {:try_start_33 .. :try_end_3b} :catch_59

    move-result v4

    if-eqz v4, :cond_43

    move-object v0, v1

    .line 417
    :goto_3f
    invoke-virtual {v2, v0}, Lcom/a/b/d/lp;->c(Ljava/lang/Object;)Lcom/a/b/d/lp;

    goto :goto_27

    .line 2435
    :cond_43
    :try_start_43
    new-instance v1, Ljava/io/File;

    invoke-virtual {p0}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v4

    const/16 v5, 0x2f

    sget-char v6, Ljava/io/File;->separatorChar:C

    invoke-virtual {v0, v5, v6}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v1, v4, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->toURI()Ljava/net/URI;
    :try_end_57
    .catch Ljava/net/URISyntaxException; {:try_start_43 .. :try_end_57} :catch_59

    move-result-object v0

    goto :goto_3f

    .line 414
    :catch_59
    move-exception v1

    invoke-static {}, Lcom/a/b/m/b;->a()Ljava/util/logging/Logger;

    move-result-object v1

    const-string v4, "Invalid Class-Path entry: "

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_72

    invoke-virtual {v4, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_6e
    invoke-virtual {v1, v0}, Ljava/util/logging/Logger;->warning(Ljava/lang/String;)V

    goto :goto_27

    :cond_72
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v4}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_6e

    .line 420
    :cond_78
    invoke-virtual {v2}, Lcom/a/b/d/lp;->b()Lcom/a/b/d/lo;

    move-result-object v0

    goto :goto_6
.end method

.method private a()Lcom/a/b/d/me;
    .registers 2

    .prologue
    .line 310
    iget-object v0, p0, Lcom/a/b/m/f;->a:Lcom/a/b/d/mf;

    invoke-virtual {v0}, Lcom/a/b/d/mf;->c()Lcom/a/b/d/me;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/io/File;Ljava/lang/String;)Ljava/net/URI;
    .registers 6
    .annotation build Lcom/a/b/a/d;
    .end annotation

    .prologue
    .line 431
    new-instance v0, Ljava/net/URI;

    invoke-direct {v0, p1}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    .line 432
    invoke-virtual {v0}, Ljava/net/URI;->isAbsolute()Z

    move-result v1

    if-eqz v1, :cond_c

    .line 435
    :goto_b
    return-object v0

    :cond_c
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v1

    const/16 v2, 0x2f

    sget-char v3, Ljava/io/File;->separatorChar:C

    invoke-virtual {p1, v2, v3}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->toURI()Ljava/net/URI;

    move-result-object v0

    goto :goto_b
.end method

.method private a(Ljava/io/File;Ljava/lang/ClassLoader;)V
    .registers 8
    .annotation build Lcom/a/b/a/d;
    .end annotation

    .prologue
    .line 321
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_7

    .line 2390
    :goto_6
    return-void

    .line 324
    :cond_7
    invoke-virtual {p1}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-eqz v0, :cond_17

    .line 2332
    const-string v0, ""

    invoke-static {}, Lcom/a/b/d/lo;->h()Lcom/a/b/d/lo;

    move-result-object v1

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/a/b/m/f;->a(Ljava/io/File;Ljava/lang/ClassLoader;Ljava/lang/String;Lcom/a/b/d/lo;)V

    goto :goto_6

    .line 2369
    :cond_17
    :try_start_17
    new-instance v1, Ljava/util/jar/JarFile;

    invoke-direct {v1, p1}, Ljava/util/jar/JarFile;-><init>(Ljava/io/File;)V
    :try_end_1c
    .catch Ljava/io/IOException; {:try_start_17 .. :try_end_1c} :catch_75

    .line 2375
    :try_start_1c
    invoke-virtual {v1}, Ljava/util/jar/JarFile;->getManifest()Ljava/util/jar/Manifest;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/a/b/m/f;->a(Ljava/io/File;Ljava/util/jar/Manifest;)Lcom/a/b/d/lo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/lo;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_28
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3d

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/net/URI;

    .line 2376
    invoke-virtual {p0, v0, p2}, Lcom/a/b/m/f;->a(Ljava/net/URI;Ljava/lang/ClassLoader;)V
    :try_end_37
    .catchall {:try_start_1c .. :try_end_37} :catchall_38

    goto :goto_28

    .line 2387
    :catchall_38
    move-exception v0

    .line 2388
    :try_start_39
    invoke-virtual {v1}, Ljava/util/jar/JarFile;->close()V
    :try_end_3c
    .catch Ljava/io/IOException; {:try_start_39 .. :try_end_3c} :catch_73

    .line 2389
    :goto_3c
    throw v0

    .line 2378
    :cond_3d
    :try_start_3d
    invoke-virtual {v1}, Ljava/util/jar/JarFile;->entries()Ljava/util/Enumeration;

    move-result-object v2

    .line 2379
    :cond_41
    :goto_41
    invoke-interface {v2}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_6d

    .line 2380
    invoke-interface {v2}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/jar/JarEntry;

    .line 2381
    invoke-virtual {v0}, Ljava/util/jar/JarEntry;->isDirectory()Z

    move-result v3

    if-nez v3, :cond_41

    invoke-virtual {v0}, Ljava/util/jar/JarEntry;->getName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "META-INF/MANIFEST.MF"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_41

    .line 2384
    iget-object v3, p0, Lcom/a/b/m/f;->a:Lcom/a/b/d/mf;

    invoke-virtual {v0}, Ljava/util/jar/JarEntry;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/a/b/m/e;->a(Ljava/lang/String;Ljava/lang/ClassLoader;)Lcom/a/b/m/e;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/a/b/d/mf;->d(Ljava/lang/Object;)Lcom/a/b/d/mf;
    :try_end_6c
    .catchall {:try_start_3d .. :try_end_6c} :catchall_38

    goto :goto_41

    .line 2388
    :cond_6d
    :try_start_6d
    invoke-virtual {v1}, Ljava/util/jar/JarFile;->close()V
    :try_end_70
    .catch Ljava/io/IOException; {:try_start_6d .. :try_end_70} :catch_71

    goto :goto_6

    .line 2390
    :catch_71
    move-exception v0

    goto :goto_6

    :catch_73
    move-exception v1

    goto :goto_3c

    .line 2372
    :catch_75
    move-exception v0

    goto :goto_6
.end method

.method private a(Ljava/io/File;Ljava/lang/ClassLoader;Ljava/lang/String;Lcom/a/b/d/lo;)V
    .registers 15

    .prologue
    .line 338
    invoke-virtual {p1}, Ljava/io/File;->getCanonicalFile()Ljava/io/File;

    move-result-object v0

    .line 339
    invoke-virtual {p4, v0}, Lcom/a/b/d/lo;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 364
    :cond_a
    :goto_a
    return-void

    .line 343
    :cond_b
    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    .line 344
    if-nez v2, :cond_3a

    .line 345
    invoke-static {}, Lcom/a/b/m/b;->a()Ljava/util/logging/Logger;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x16

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Cannot read directory "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->warning(Ljava/lang/String;)V

    goto :goto_a

    .line 349
    :cond_3a
    invoke-static {}, Lcom/a/b/d/lo;->i()Lcom/a/b/d/lp;

    move-result-object v1

    invoke-virtual {v1, p4}, Lcom/a/b/d/lp;->b(Ljava/lang/Iterable;)Lcom/a/b/d/lp;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/a/b/d/lp;->c(Ljava/lang/Object;)Lcom/a/b/d/lp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/lp;->b()Lcom/a/b/d/lo;

    move-result-object v3

    .line 353
    array-length v4, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_4d
    if-ge v1, v4, :cond_a

    aget-object v0, v2, v1

    .line 354
    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    .line 355
    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v6

    if-eqz v6, :cond_94

    .line 356
    invoke-static {p3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v8, v8, 0x1

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v9

    add-int/2addr v8, v9

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v0, p2, v5, v3}, Lcom/a/b/m/f;->a(Ljava/io/File;Ljava/lang/ClassLoader;Ljava/lang/String;Lcom/a/b/d/lo;)V

    .line 353
    :cond_90
    :goto_90
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4d

    .line 358
    :cond_94
    invoke-static {p3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_b8

    invoke-virtual {v6, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 359
    :goto_a6
    const-string v5, "META-INF/MANIFEST.MF"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_90

    .line 360
    iget-object v5, p0, Lcom/a/b/m/f;->a:Lcom/a/b/d/mf;

    invoke-static {v0, p2}, Lcom/a/b/m/e;->a(Ljava/lang/String;Ljava/lang/ClassLoader;)Lcom/a/b/m/e;

    move-result-object v0

    invoke-virtual {v5, v0}, Lcom/a/b/d/mf;->d(Ljava/lang/Object;)Lcom/a/b/d/mf;

    goto :goto_90

    .line 358
    :cond_b8
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v6}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_a6
.end method

.method private b(Ljava/io/File;Ljava/lang/ClassLoader;)V
    .registers 5

    .prologue
    .line 332
    const-string v0, ""

    invoke-static {}, Lcom/a/b/d/lo;->h()Lcom/a/b/d/lo;

    move-result-object v1

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/a/b/m/f;->a(Ljava/io/File;Ljava/lang/ClassLoader;Ljava/lang/String;Lcom/a/b/d/lo;)V

    .line 333
    return-void
.end method

.method private c(Ljava/io/File;Ljava/lang/ClassLoader;)V
    .registers 8

    .prologue
    .line 369
    :try_start_0
    new-instance v1, Ljava/util/jar/JarFile;

    invoke-direct {v1, p1}, Ljava/util/jar/JarFile;-><init>(Ljava/io/File;)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_5} :catch_5e

    .line 375
    :try_start_5
    invoke-virtual {v1}, Ljava/util/jar/JarFile;->getManifest()Ljava/util/jar/Manifest;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/a/b/m/f;->a(Ljava/io/File;Ljava/util/jar/Manifest;)Lcom/a/b/d/lo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/lo;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_11
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_26

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/net/URI;

    .line 376
    invoke-virtual {p0, v0, p2}, Lcom/a/b/m/f;->a(Ljava/net/URI;Ljava/lang/ClassLoader;)V
    :try_end_20
    .catchall {:try_start_5 .. :try_end_20} :catchall_21

    goto :goto_11

    .line 387
    :catchall_21
    move-exception v0

    .line 388
    :try_start_22
    invoke-virtual {v1}, Ljava/util/jar/JarFile;->close()V
    :try_end_25
    .catch Ljava/io/IOException; {:try_start_22 .. :try_end_25} :catch_5c

    .line 389
    :goto_25
    throw v0

    .line 378
    :cond_26
    :try_start_26
    invoke-virtual {v1}, Ljava/util/jar/JarFile;->entries()Ljava/util/Enumeration;

    move-result-object v2

    .line 379
    :cond_2a
    :goto_2a
    invoke-interface {v2}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_56

    .line 380
    invoke-interface {v2}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/jar/JarEntry;

    .line 381
    invoke-virtual {v0}, Ljava/util/jar/JarEntry;->isDirectory()Z

    move-result v3

    if-nez v3, :cond_2a

    invoke-virtual {v0}, Ljava/util/jar/JarEntry;->getName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "META-INF/MANIFEST.MF"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2a

    .line 384
    iget-object v3, p0, Lcom/a/b/m/f;->a:Lcom/a/b/d/mf;

    invoke-virtual {v0}, Ljava/util/jar/JarEntry;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/a/b/m/e;->a(Ljava/lang/String;Ljava/lang/ClassLoader;)Lcom/a/b/m/e;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/a/b/d/mf;->d(Ljava/lang/Object;)Lcom/a/b/d/mf;
    :try_end_55
    .catchall {:try_start_26 .. :try_end_55} :catchall_21

    goto :goto_2a

    .line 388
    :cond_56
    :try_start_56
    invoke-virtual {v1}, Ljava/util/jar/JarFile;->close()V
    :try_end_59
    .catch Ljava/io/IOException; {:try_start_56 .. :try_end_59} :catch_5a

    .line 390
    :goto_59
    return-void

    :catch_5a
    move-exception v0

    goto :goto_59

    :catch_5c
    move-exception v1

    goto :goto_25

    .line 372
    :catch_5e
    move-exception v0

    goto :goto_59
.end method


# virtual methods
.method final a(Ljava/net/URI;Ljava/lang/ClassLoader;)V
    .registers 6

    .prologue
    .line 314
    invoke-virtual {p1}, Ljava/net/URI;->getScheme()Ljava/lang/String;

    move-result-object v0

    const-string v1, "file"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2e

    iget-object v0, p0, Lcom/a/b/m/f;->b:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2e

    .line 315
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/net/URI;)V

    .line 1321
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_2e

    .line 1324
    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-eqz v1, :cond_2f

    .line 1332
    const-string v1, ""

    invoke-static {}, Lcom/a/b/d/lo;->h()Lcom/a/b/d/lo;

    move-result-object v2

    invoke-direct {p0, v0, p2, v1, v2}, Lcom/a/b/m/f;->a(Ljava/io/File;Ljava/lang/ClassLoader;Ljava/lang/String;Lcom/a/b/d/lo;)V

    .line 1325
    :cond_2e
    :goto_2e
    return-void

    .line 1327
    :cond_2f
    invoke-direct {p0, v0, p2}, Lcom/a/b/m/f;->c(Ljava/io/File;Ljava/lang/ClassLoader;)V

    goto :goto_2e
.end method
