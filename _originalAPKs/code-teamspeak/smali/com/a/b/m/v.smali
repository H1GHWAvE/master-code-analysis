.class public abstract Lcom/a/b/m/v;
.super Lcom/a/b/m/u;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/a;
.end annotation


# instance fields
.field final a:Ljava/lang/reflect/TypeVariable;


# direct methods
.method protected constructor <init>()V
    .registers 6

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/a/b/m/u;-><init>()V

    .line 47
    invoke-virtual {p0}, Lcom/a/b/m/v;->a()Ljava/lang/reflect/Type;

    move-result-object v0

    .line 48
    instance-of v1, v0, Ljava/lang/reflect/TypeVariable;

    const-string v2, "%s should be a type variable."

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 49
    check-cast v0, Ljava/lang/reflect/TypeVariable;

    iput-object v0, p0, Lcom/a/b/m/v;->a:Ljava/lang/reflect/TypeVariable;

    .line 50
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .registers 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 57
    instance-of v0, p1, Lcom/a/b/m/v;

    if-eqz v0, :cond_f

    .line 58
    check-cast p1, Lcom/a/b/m/v;

    .line 59
    iget-object v0, p0, Lcom/a/b/m/v;->a:Ljava/lang/reflect/TypeVariable;

    iget-object v1, p1, Lcom/a/b/m/v;->a:Ljava/lang/reflect/TypeVariable;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 61
    :goto_e
    return v0

    :cond_f
    const/4 v0, 0x0

    goto :goto_e
.end method

.method public final hashCode()I
    .registers 2

    .prologue
    .line 53
    iget-object v0, p0, Lcom/a/b/m/v;->a:Ljava/lang/reflect/TypeVariable;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 2

    .prologue
    .line 65
    iget-object v0, p0, Lcom/a/b/m/v;->a:Ljava/lang/reflect/TypeVariable;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
