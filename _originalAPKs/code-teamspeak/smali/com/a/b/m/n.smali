.class public final Lcom/a/b/m/n;
.super Lcom/a/b/d/gs;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/m/ad;


# annotations
.annotation build Lcom/a/b/a/a;
.end annotation


# instance fields
.field private final a:Ljava/util/Map;


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/a/b/d/gs;-><init>()V

    .line 46
    invoke-static {}, Lcom/a/b/d/sz;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/m/n;->a:Ljava/util/Map;

    .line 102
    return-void
.end method

.method private static b()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 74
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Please use putInstance() instead."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private b(Lcom/a/b/m/ae;)Ljava/lang/Object;
    .registers 3
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 99
    iget-object v0, p0, Lcom/a/b/m/n;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method private b(Lcom/a/b/m/ae;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 93
    iget-object v0, p0, Lcom/a/b/m/n;->a:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/a/b/m/ae;)Ljava/lang/Object;
    .registers 3
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 63
    invoke-virtual {p1}, Lcom/a/b/m/ae;->c()Lcom/a/b/m/ae;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/a/b/m/n;->b(Lcom/a/b/m/ae;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/a/b/m/ae;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 69
    invoke-virtual {p1}, Lcom/a/b/m/ae;->c()Lcom/a/b/m/ae;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lcom/a/b/m/n;->b(Lcom/a/b/m/ae;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Class;)Ljava/lang/Object;
    .registers 3
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 51
    invoke-static {p1}, Lcom/a/b/m/ae;->a(Ljava/lang/Class;)Lcom/a/b/m/ae;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/a/b/m/n;->b(Lcom/a/b/m/ae;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 57
    invoke-static {p1}, Lcom/a/b/m/ae;->a(Ljava/lang/Class;)Lcom/a/b/m/ae;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lcom/a/b/m/n;->b(Lcom/a/b/m/ae;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected final a()Ljava/util/Map;
    .registers 2

    .prologue
    .line 87
    iget-object v0, p0, Lcom/a/b/m/n;->a:Ljava/util/Map;

    return-object v0
.end method

.method public final entrySet()Ljava/util/Set;
    .registers 2

    .prologue
    .line 83
    invoke-super {p0}, Lcom/a/b/d/gs;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/m/p;->a(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method protected final bridge synthetic k_()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 42
    .line 1087
    iget-object v0, p0, Lcom/a/b/m/n;->a:Ljava/util/Map;

    .line 42
    return-object v0
.end method

.method public final synthetic put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 5

    .prologue
    .line 1074
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Please use putInstance() instead."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final putAll(Ljava/util/Map;)V
    .registers 4

    .prologue
    .line 79
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Please use putInstance() instead."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
