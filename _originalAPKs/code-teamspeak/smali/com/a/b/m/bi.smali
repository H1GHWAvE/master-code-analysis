.class final enum Lcom/a/b/m/bi;
.super Lcom/a/b/m/bh;
.source "SourceFile"


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .registers 3

    .prologue
    const/4 v0, 0x0

    .line 453
    invoke-direct {p0, p1, v0, v0}, Lcom/a/b/m/bh;-><init>(Ljava/lang/String;IB)V

    return-void
.end method

.method private static d(Ljava/lang/reflect/Type;)Ljava/lang/reflect/GenericArrayType;
    .registers 2

    .prologue
    .line 455
    new-instance v0, Lcom/a/b/m/bg;

    invoke-direct {v0, p0}, Lcom/a/b/m/bg;-><init>(Ljava/lang/reflect/Type;)V

    return-object v0
.end method


# virtual methods
.method final synthetic a(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;
    .registers 3

    .prologue
    .line 1455
    new-instance v0, Lcom/a/b/m/bg;

    invoke-direct {v0, p1}, Lcom/a/b/m/bg;-><init>(Ljava/lang/reflect/Type;)V

    .line 453
    return-object v0
.end method

.method final b(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;
    .registers 4

    .prologue
    .line 458
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 459
    instance-of v0, p1, Ljava/lang/Class;

    if-eqz v0, :cond_19

    move-object v0, p1

    .line 460
    check-cast v0, Ljava/lang/Class;

    .line 461
    invoke-virtual {v0}, Ljava/lang/Class;->isArray()Z

    move-result v1

    if-eqz v1, :cond_19

    .line 462
    new-instance p1, Lcom/a/b/m/bg;

    invoke-virtual {v0}, Ljava/lang/Class;->getComponentType()Ljava/lang/Class;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/a/b/m/bg;-><init>(Ljava/lang/reflect/Type;)V

    .line 465
    :cond_19
    return-object p1
.end method
