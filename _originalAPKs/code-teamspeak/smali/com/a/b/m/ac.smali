.class final Lcom/a/b/m/ac;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Ljava/util/concurrent/atomic/AtomicInteger;


# direct methods
.method private constructor <init>()V
    .registers 2

    .prologue
    .line 381
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 383
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object v0, p0, Lcom/a/b/m/ac;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void
.end method

.method synthetic constructor <init>(B)V
    .registers 2

    .prologue
    .line 381
    invoke-direct {p0}, Lcom/a/b/m/ac;-><init>()V

    return-void
.end method

.method private a([Ljava/lang/reflect/Type;)[Ljava/lang/reflect/Type;
    .registers 5

    .prologue
    .line 429
    array-length v0, p1

    new-array v1, v0, [Ljava/lang/reflect/Type;

    .line 430
    const/4 v0, 0x0

    :goto_4
    array-length v2, p1

    if-ge v0, v2, :cond_12

    .line 431
    aget-object v2, p1, v0

    invoke-virtual {p0, v2}, Lcom/a/b/m/ac;->a(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;

    move-result-object v2

    aput-object v2, v1, v0

    .line 430
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 433
    :cond_12
    return-object v1
.end method

.method private b(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;
    .registers 3
    .param p1    # Ljava/lang/reflect/Type;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 422
    if-nez p1, :cond_4

    .line 423
    const/4 v0, 0x0

    .line 425
    :goto_3
    return-object v0

    :cond_4
    invoke-virtual {p0, p1}, Lcom/a/b/m/ac;->a(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;

    move-result-object v0

    goto :goto_3
.end method


# virtual methods
.method final a(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;
    .registers 8

    .prologue
    .line 386
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 387
    instance-of v0, p1, Ljava/lang/Class;

    if-eqz v0, :cond_8

    .line 415
    :cond_7
    :goto_7
    return-object p1

    .line 390
    :cond_8
    instance-of v0, p1, Ljava/lang/reflect/TypeVariable;

    if-nez v0, :cond_7

    .line 393
    instance-of v0, p1, Ljava/lang/reflect/GenericArrayType;

    if-eqz v0, :cond_1f

    .line 394
    check-cast p1, Ljava/lang/reflect/GenericArrayType;

    .line 395
    invoke-interface {p1}, Ljava/lang/reflect/GenericArrayType;->getGenericComponentType()Ljava/lang/reflect/Type;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/a/b/m/ac;->a(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/m/ay;->a(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;

    move-result-object p1

    goto :goto_7

    .line 397
    :cond_1f
    instance-of v0, p1, Ljava/lang/reflect/ParameterizedType;

    if-eqz v0, :cond_54

    .line 398
    check-cast p1, Ljava/lang/reflect/ParameterizedType;

    .line 399
    invoke-interface {p1}, Ljava/lang/reflect/ParameterizedType;->getOwnerType()Ljava/lang/reflect/Type;

    move-result-object v0

    .line 1422
    if-nez v0, :cond_49

    .line 1423
    const/4 v0, 0x0

    move-object v1, v0

    .line 399
    :goto_2d
    invoke-interface {p1}, Ljava/lang/reflect/ParameterizedType;->getRawType()Ljava/lang/reflect/Type;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    invoke-interface {p1}, Ljava/lang/reflect/ParameterizedType;->getActualTypeArguments()[Ljava/lang/reflect/Type;

    move-result-object v3

    .line 1429
    array-length v2, v3

    new-array v4, v2, [Ljava/lang/reflect/Type;

    .line 1430
    const/4 v2, 0x0

    :goto_3b
    array-length v5, v3

    if-ge v2, v5, :cond_4f

    .line 1431
    aget-object v5, v3, v2

    invoke-virtual {p0, v5}, Lcom/a/b/m/ac;->a(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;

    move-result-object v5

    aput-object v5, v4, v2

    .line 1430
    add-int/lit8 v2, v2, 0x1

    goto :goto_3b

    .line 1425
    :cond_49
    invoke-virtual {p0, v0}, Lcom/a/b/m/ac;->a(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;

    move-result-object v0

    move-object v1, v0

    goto :goto_2d

    .line 399
    :cond_4f
    invoke-static {v1, v0, v4}, Lcom/a/b/m/ay;->a(Ljava/lang/reflect/Type;Ljava/lang/Class;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object p1

    goto :goto_7

    .line 404
    :cond_54
    instance-of v0, p1, Ljava/lang/reflect/WildcardType;

    if-eqz v0, :cond_ad

    move-object v0, p1

    .line 405
    check-cast v0, Ljava/lang/reflect/WildcardType;

    .line 406
    invoke-interface {v0}, Ljava/lang/reflect/WildcardType;->getLowerBounds()[Ljava/lang/reflect/Type;

    move-result-object v1

    .line 407
    array-length v1, v1

    if-nez v1, :cond_7

    .line 408
    invoke-interface {v0}, Ljava/lang/reflect/WildcardType;->getUpperBounds()[Ljava/lang/reflect/Type;

    move-result-object v1

    .line 409
    iget-object v2, p0, Lcom/a/b/m/ac;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v2

    const/16 v3, 0x26

    invoke-static {v3}, Lcom/a/b/b/bv;->a(C)Lcom/a/b/b/bv;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/a/b/b/bv;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x21

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "capture#"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "-of ? extends "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 411
    const-class v2, Lcom/a/b/m/ac;

    invoke-interface {v0}, Ljava/lang/reflect/WildcardType;->getUpperBounds()[Ljava/lang/reflect/Type;

    move-result-object v0

    invoke-static {v2, v1, v0}, Lcom/a/b/m/ay;->a(Ljava/lang/reflect/GenericDeclaration;Ljava/lang/String;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/TypeVariable;

    move-result-object p1

    goto/16 :goto_7

    .line 418
    :cond_ad
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "must have been one of the known types"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method
