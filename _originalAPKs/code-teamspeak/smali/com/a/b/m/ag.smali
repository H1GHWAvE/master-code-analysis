.class final Lcom/a/b/m/ag;
.super Lcom/a/b/m/l;
.source "SourceFile"


# instance fields
.field final synthetic b:Lcom/a/b/m/ae;


# direct methods
.method constructor <init>(Lcom/a/b/m/ae;Ljava/lang/reflect/Constructor;)V
    .registers 3

    .prologue
    .line 524
    iput-object p1, p0, Lcom/a/b/m/ag;->b:Lcom/a/b/m/ae;

    invoke-direct {p0, p2}, Lcom/a/b/m/l;-><init>(Ljava/lang/reflect/Constructor;)V

    return-void
.end method


# virtual methods
.method public final a()Lcom/a/b/m/ae;
    .registers 2

    .prologue
    .line 535
    iget-object v0, p0, Lcom/a/b/m/ag;->b:Lcom/a/b/m/ae;

    return-object v0
.end method

.method final d()[Ljava/lang/reflect/Type;
    .registers 3

    .prologue
    .line 529
    iget-object v0, p0, Lcom/a/b/m/ag;->b:Lcom/a/b/m/ae;

    invoke-super {p0}, Lcom/a/b/m/l;->d()[Ljava/lang/reflect/Type;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/a/b/m/ae;->a(Lcom/a/b/m/ae;[Ljava/lang/reflect/Type;)[Ljava/lang/reflect/Type;

    move-result-object v0

    return-object v0
.end method

.method final e()[Ljava/lang/reflect/Type;
    .registers 3

    .prologue
    .line 532
    iget-object v0, p0, Lcom/a/b/m/ag;->b:Lcom/a/b/m/ae;

    invoke-super {p0}, Lcom/a/b/m/l;->e()[Ljava/lang/reflect/Type;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/a/b/m/ae;->a(Lcom/a/b/m/ae;[Ljava/lang/reflect/Type;)[Ljava/lang/reflect/Type;

    move-result-object v0

    return-object v0
.end method

.method final g()Ljava/lang/reflect/Type;
    .registers 3

    .prologue
    .line 526
    iget-object v0, p0, Lcom/a/b/m/ag;->b:Lcom/a/b/m/ae;

    invoke-super {p0}, Lcom/a/b/m/l;->g()Ljava/lang/reflect/Type;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/a/b/m/ae;->b(Ljava/lang/reflect/Type;)Lcom/a/b/m/ae;

    move-result-object v0

    .line 1196
    iget-object v0, v0, Lcom/a/b/m/ae;->a:Ljava/lang/reflect/Type;

    .line 526
    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .registers 6

    .prologue
    .line 538
    .line 1535
    iget-object v0, p0, Lcom/a/b/m/ag;->b:Lcom/a/b/m/ae;

    .line 538
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ", "

    invoke-static {v1}, Lcom/a/b/b/bv;->a(Ljava/lang/String;)Lcom/a/b/b/bv;

    move-result-object v1

    invoke-virtual {p0}, Lcom/a/b/m/ag;->d()[Ljava/lang/reflect/Type;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/a/b/b/bv;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x2

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
