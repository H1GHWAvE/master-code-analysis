.class abstract enum Lcom/a/b/m/bh;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lcom/a/b/m/bh;

.field public static final enum b:Lcom/a/b/m/bh;

.field public static final enum c:Lcom/a/b/m/bh;

.field static final d:Lcom/a/b/m/bh;

.field private static final synthetic e:[Lcom/a/b/m/bh;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    .line 453
    new-instance v0, Lcom/a/b/m/bi;

    const-string v1, "JAVA6"

    invoke-direct {v0, v1}, Lcom/a/b/m/bi;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/a/b/m/bh;->a:Lcom/a/b/m/bh;

    .line 468
    new-instance v0, Lcom/a/b/m/bj;

    const-string v1, "JAVA7"

    invoke-direct {v0, v1}, Lcom/a/b/m/bj;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/a/b/m/bh;->b:Lcom/a/b/m/bh;

    .line 480
    new-instance v0, Lcom/a/b/m/bk;

    const-string v1, "JAVA8"

    invoke-direct {v0, v1}, Lcom/a/b/m/bk;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/a/b/m/bh;->c:Lcom/a/b/m/bh;

    .line 451
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/a/b/m/bh;

    const/4 v1, 0x0

    sget-object v2, Lcom/a/b/m/bh;->a:Lcom/a/b/m/bh;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/a/b/m/bh;->b:Lcom/a/b/m/bh;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/a/b/m/bh;->c:Lcom/a/b/m/bh;

    aput-object v2, v0, v1

    sput-object v0, Lcom/a/b/m/bh;->e:[Lcom/a/b/m/bh;

    .line 504
    const-class v0, Ljava/lang/reflect/AnnotatedElement;

    const-class v1, Ljava/lang/reflect/TypeVariable;

    invoke-virtual {v0, v1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_3e

    .line 505
    sget-object v0, Lcom/a/b/m/bh;->c:Lcom/a/b/m/bh;

    sput-object v0, Lcom/a/b/m/bh;->d:Lcom/a/b/m/bh;

    .line 511
    :goto_3d
    return-void

    .line 506
    :cond_3e
    new-instance v0, Lcom/a/b/m/bl;

    invoke-direct {v0}, Lcom/a/b/m/bl;-><init>()V

    invoke-virtual {v0}, Lcom/a/b/m/bl;->a()Ljava/lang/reflect/Type;

    move-result-object v0

    instance-of v0, v0, Ljava/lang/Class;

    if-eqz v0, :cond_50

    .line 507
    sget-object v0, Lcom/a/b/m/bh;->b:Lcom/a/b/m/bh;

    sput-object v0, Lcom/a/b/m/bh;->d:Lcom/a/b/m/bh;

    goto :goto_3d

    .line 509
    :cond_50
    sget-object v0, Lcom/a/b/m/bh;->a:Lcom/a/b/m/bh;

    sput-object v0, Lcom/a/b/m/bh;->d:Lcom/a/b/m/bh;

    goto :goto_3d
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3

    .prologue
    .line 451
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;IB)V
    .registers 4

    .prologue
    .line 451
    invoke-direct {p0, p1, p2}, Lcom/a/b/m/bh;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/a/b/m/bh;
    .registers 2

    .prologue
    .line 451
    const-class v0, Lcom/a/b/m/bh;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/a/b/m/bh;

    return-object v0
.end method

.method public static values()[Lcom/a/b/m/bh;
    .registers 1

    .prologue
    .line 451
    sget-object v0, Lcom/a/b/m/bh;->e:[Lcom/a/b/m/bh;

    invoke-virtual {v0}, [Lcom/a/b/m/bh;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/a/b/m/bh;

    return-object v0
.end method


# virtual methods
.method final a([Ljava/lang/reflect/Type;)Lcom/a/b/d/jl;
    .registers 6

    .prologue
    .line 520
    invoke-static {}, Lcom/a/b/d/jl;->h()Lcom/a/b/d/jn;

    move-result-object v1

    .line 521
    array-length v2, p1

    const/4 v0, 0x0

    :goto_6
    if-ge v0, v2, :cond_14

    aget-object v3, p1, v0

    .line 522
    invoke-virtual {p0, v3}, Lcom/a/b/m/bh;->b(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/a/b/d/jn;->c(Ljava/lang/Object;)Lcom/a/b/d/jn;

    .line 521
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 524
    :cond_14
    invoke-virtual {v1}, Lcom/a/b/d/jn;->b()Lcom/a/b/d/jl;

    move-result-object v0

    return-object v0
.end method

.method abstract a(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;
.end method

.method abstract b(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;
.end method

.method c(Ljava/lang/reflect/Type;)Ljava/lang/String;
    .registers 3

    .prologue
    .line 516
    invoke-static {p1}, Lcom/a/b/m/ay;->b(Ljava/lang/reflect/Type;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
