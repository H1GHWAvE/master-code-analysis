.class final Lcom/teamspeak/ts3client/ah;
.super Landroid/telephony/PhoneStateListener;
.source "SourceFile"


# instance fields
.field a:Z

.field b:I

.field final synthetic c:Lcom/teamspeak/ts3client/t;


# direct methods
.method constructor <init>(Lcom/teamspeak/ts3client/t;)V
    .registers 3

    .prologue
    const/4 v0, 0x0

    .line 110
    iput-object p1, p0, Lcom/teamspeak/ts3client/ah;->c:Lcom/teamspeak/ts3client/t;

    invoke-direct {p0}, Landroid/telephony/PhoneStateListener;-><init>()V

    .line 111
    iput-boolean v0, p0, Lcom/teamspeak/ts3client/ah;->a:Z

    .line 112
    iput v0, p0, Lcom/teamspeak/ts3client/ah;->b:I

    return-void
.end method

.method private a()V
    .registers 8

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 140
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/ah;->a:Z

    if-eqz v0, :cond_162

    .line 141
    iget-object v0, p0, Lcom/teamspeak/ts3client/ah;->c:Lcom/teamspeak/ts3client/t;

    invoke-static {v0}, Lcom/teamspeak/ts3client/t;->b(Lcom/teamspeak/ts3client/t;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 36061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 141
    if-eqz v0, :cond_b5

    iget-object v0, p0, Lcom/teamspeak/ts3client/ah;->c:Lcom/teamspeak/ts3client/t;

    invoke-static {v0}, Lcom/teamspeak/ts3client/t;->b(Lcom/teamspeak/ts3client/t;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 37061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 37206
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->k:Lcom/teamspeak/ts3client/t;

    .line 141
    if-eqz v0, :cond_b5

    .line 142
    iget-object v0, p0, Lcom/teamspeak/ts3client/ah;->c:Lcom/teamspeak/ts3client/t;

    invoke-static {v0}, Lcom/teamspeak/ts3client/t;->b(Lcom/teamspeak/ts3client/t;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 38093
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 142
    const-string v1, "call_setaway"

    invoke-interface {v0, v1, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_b5

    .line 143
    iget-object v0, p0, Lcom/teamspeak/ts3client/ah;->c:Lcom/teamspeak/ts3client/t;

    invoke-static {v0}, Lcom/teamspeak/ts3client/t;->b(Lcom/teamspeak/ts3client/t;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 39061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 39234
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 143
    iget-object v1, p0, Lcom/teamspeak/ts3client/ah;->c:Lcom/teamspeak/ts3client/t;

    invoke-static {v1}, Lcom/teamspeak/ts3client/t;->b(Lcom/teamspeak/ts3client/t;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v1

    .line 40061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 40267
    iget-wide v2, v1, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 143
    sget-object v1, Lcom/teamspeak/ts3client/jni/d;->N:Lcom/teamspeak/ts3client/jni/d;

    const-string v4, ""

    invoke-virtual {v0, v2, v3, v1, v4}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JLcom/teamspeak/ts3client/jni/d;Ljava/lang/String;)I

    .line 144
    iget-object v0, p0, Lcom/teamspeak/ts3client/ah;->c:Lcom/teamspeak/ts3client/t;

    invoke-static {v0}, Lcom/teamspeak/ts3client/t;->b(Lcom/teamspeak/ts3client/t;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 41061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 41234
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 144
    iget-object v1, p0, Lcom/teamspeak/ts3client/ah;->c:Lcom/teamspeak/ts3client/t;

    invoke-static {v1}, Lcom/teamspeak/ts3client/t;->b(Lcom/teamspeak/ts3client/t;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v1

    .line 42061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 42267
    iget-wide v2, v1, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 144
    sget-object v1, Lcom/teamspeak/ts3client/jni/d;->M:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v0, v2, v3, v1, v5}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JLcom/teamspeak/ts3client/jni/d;I)I

    .line 145
    iget-object v0, p0, Lcom/teamspeak/ts3client/ah;->c:Lcom/teamspeak/ts3client/t;

    invoke-static {v0}, Lcom/teamspeak/ts3client/t;->b(Lcom/teamspeak/ts3client/t;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 43061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 43234
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 145
    iget-object v1, p0, Lcom/teamspeak/ts3client/ah;->c:Lcom/teamspeak/ts3client/t;

    invoke-static {v1}, Lcom/teamspeak/ts3client/t;->b(Lcom/teamspeak/ts3client/t;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v1

    .line 44061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 44267
    iget-wide v2, v1, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 145
    const-string v1, ""

    invoke-virtual {v0, v2, v3, v1}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_flushClientSelfUpdates(JLjava/lang/String;)I

    .line 146
    iget-object v0, p0, Lcom/teamspeak/ts3client/ah;->c:Lcom/teamspeak/ts3client/t;

    invoke-static {v0}, Lcom/teamspeak/ts3client/t;->b(Lcom/teamspeak/ts3client/t;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 45061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 45360
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->v:Lcom/teamspeak/ts3client/data/ab;

    .line 46243
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/ab;->s:Ljava/util/BitSet;

    .line 146
    invoke-virtual {v0, v5}, Ljava/util/BitSet;->clear(I)V

    .line 147
    iget-object v0, p0, Lcom/teamspeak/ts3client/ah;->c:Lcom/teamspeak/ts3client/t;

    invoke-static {v0}, Lcom/teamspeak/ts3client/t;->b(Lcom/teamspeak/ts3client/t;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 47061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 47360
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->v:Lcom/teamspeak/ts3client/data/ab;

    .line 48243
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/ab;->s:Ljava/util/BitSet;

    .line 147
    invoke-virtual {v0, v6}, Ljava/util/BitSet;->clear(I)V

    .line 148
    iget-object v0, p0, Lcom/teamspeak/ts3client/ah;->c:Lcom/teamspeak/ts3client/t;

    invoke-static {v0}, Lcom/teamspeak/ts3client/t;->b(Lcom/teamspeak/ts3client/t;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 49061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 49360
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->v:Lcom/teamspeak/ts3client/data/ab;

    .line 50243
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/ab;->s:Ljava/util/BitSet;

    .line 148
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->clear(I)V

    .line 149
    iget-object v0, p0, Lcom/teamspeak/ts3client/ah;->c:Lcom/teamspeak/ts3client/t;

    invoke-static {v0}, Lcom/teamspeak/ts3client/t;->b(Lcom/teamspeak/ts3client/t;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 50244
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50245
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->v:Lcom/teamspeak/ts3client/data/ab;

    .line 149
    const-string v1, ""

    .line 50246
    iput-object v1, v0, Lcom/teamspeak/ts3client/data/ab;->t:Ljava/lang/String;

    .line 152
    :cond_b5
    iput-boolean v5, p0, Lcom/teamspeak/ts3client/ah;->a:Z

    .line 153
    iget-object v0, p0, Lcom/teamspeak/ts3client/ah;->c:Lcom/teamspeak/ts3client/t;

    invoke-static {v0}, Lcom/teamspeak/ts3client/t;->b(Lcom/teamspeak/ts3client/t;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 50248
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50249
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->k:Lcom/teamspeak/ts3client/t;

    .line 50250
    iget-boolean v1, v0, Lcom/teamspeak/ts3client/t;->f:Z

    if-nez v1, :cond_163

    .line 50251
    iput-boolean v5, v0, Lcom/teamspeak/ts3client/t;->c:Z

    .line 50252
    iget-object v1, v0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->g()Lcom/teamspeak/ts3client/a/p;

    move-result-object v1

    .line 50283
    iput-boolean v5, v1, Lcom/teamspeak/ts3client/a/p;->a:Z

    .line 50253
    iget-object v1, v0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50285
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50286
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 50253
    iget-object v2, v0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50287
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50288
    iget-wide v2, v2, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 50253
    sget-object v4, Lcom/teamspeak/ts3client/jni/d;->g:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JLcom/teamspeak/ts3client/jni/d;I)I

    .line 50254
    iget-object v1, v0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50289
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50290
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 50254
    iget-object v2, v0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50291
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50292
    iget-wide v2, v2, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 50254
    const-string v4, ""

    invoke-virtual {v1, v2, v3, v4}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_flushClientSelfUpdates(JLjava/lang/String;)I

    .line 50255
    iget-object v1, v0, Lcom/teamspeak/ts3client/t;->h:Lcom/teamspeak/ts3client/customs/FloatingButton;

    const v2, 0x7f020096

    iget v3, v0, Lcom/teamspeak/ts3client/t;->d:I

    int-to-float v3, v3

    iget v4, v0, Lcom/teamspeak/ts3client/t;->d:I

    int-to-float v4, v4

    invoke-static {v2, v3, v4}, Lcom/teamspeak/ts3client/data/d/t;->a(IFF)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/teamspeak/ts3client/customs/FloatingButton;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 50256
    iget-object v1, v0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50293
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50294
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->v:Lcom/teamspeak/ts3client/data/ab;

    .line 50295
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/ab;->s:Ljava/util/BitSet;

    .line 50256
    invoke-virtual {v1, v6}, Ljava/util/BitSet;->set(I)V

    .line 50261
    :goto_10e
    iget-boolean v1, v0, Lcom/teamspeak/ts3client/t;->e:Z

    if-nez v1, :cond_176

    .line 50262
    iput-boolean v5, v0, Lcom/teamspeak/ts3client/t;->b:Z

    .line 50263
    iget-object v1, v0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50296
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50297
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 50263
    iget-object v2, v0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50298
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50299
    iget-wide v2, v2, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 50263
    sget-object v4, Lcom/teamspeak/ts3client/jni/d;->f:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JLcom/teamspeak/ts3client/jni/d;I)I

    .line 50264
    iget-object v1, v0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50300
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50301
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 50264
    iget-object v2, v0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50302
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50303
    iget-wide v2, v2, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 50264
    const-string v4, ""

    invoke-virtual {v1, v2, v3, v4}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_flushClientSelfUpdates(JLjava/lang/String;)I

    .line 50265
    iget-object v1, v0, Lcom/teamspeak/ts3client/t;->g:Lcom/teamspeak/ts3client/customs/FloatingButton;

    const v2, 0x7f020081

    iget v3, v0, Lcom/teamspeak/ts3client/t;->d:I

    int-to-float v3, v3

    iget v4, v0, Lcom/teamspeak/ts3client/t;->d:I

    int-to-float v4, v4

    invoke-static {v2, v3, v4}, Lcom/teamspeak/ts3client/data/d/t;->a(IFF)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/teamspeak/ts3client/customs/FloatingButton;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 50266
    iget-object v1, v0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50304
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50305
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->v:Lcom/teamspeak/ts3client/data/ab;

    .line 50306
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/ab;->s:Ljava/util/BitSet;

    .line 50266
    invoke-virtual {v1, v5}, Ljava/util/BitSet;->set(I)V

    .line 50271
    :goto_153
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    .line 50272
    new-instance v2, Lcom/teamspeak/ts3client/as;

    invoke-direct {v2, v0}, Lcom/teamspeak/ts3client/as;-><init>(Lcom/teamspeak/ts3client/t;)V

    const-wide/16 v4, 0x3e8

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 156
    :cond_162
    return-void

    .line 50258
    :cond_163
    iget-object v1, v0, Lcom/teamspeak/ts3client/t;->h:Lcom/teamspeak/ts3client/customs/FloatingButton;

    const v2, 0x7f020098

    iget v3, v0, Lcom/teamspeak/ts3client/t;->d:I

    int-to-float v3, v3

    iget v4, v0, Lcom/teamspeak/ts3client/t;->d:I

    int-to-float v4, v4

    invoke-static {v2, v3, v4}, Lcom/teamspeak/ts3client/data/d/t;->a(IFF)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/teamspeak/ts3client/customs/FloatingButton;->setBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_10e

    .line 50268
    :cond_176
    iget-object v1, v0, Lcom/teamspeak/ts3client/t;->g:Lcom/teamspeak/ts3client/customs/FloatingButton;

    const v2, 0x7f020084

    iget v3, v0, Lcom/teamspeak/ts3client/t;->d:I

    int-to-float v3, v3

    iget v4, v0, Lcom/teamspeak/ts3client/t;->d:I

    int-to-float v4, v4

    invoke-static {v2, v3, v4}, Lcom/teamspeak/ts3client/data/d/t;->a(IFF)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/teamspeak/ts3client/customs/FloatingButton;->setBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_153
.end method

.method private b()V
    .registers 10

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 159
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/ah;->a:Z

    if-nez v0, :cond_16d

    .line 160
    iget-object v0, p0, Lcom/teamspeak/ts3client/ah;->c:Lcom/teamspeak/ts3client/t;

    invoke-static {v0}, Lcom/teamspeak/ts3client/t;->b(Lcom/teamspeak/ts3client/t;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 50307
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50308
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->k:Lcom/teamspeak/ts3client/t;

    .line 160
    if-eqz v0, :cond_16d

    .line 161
    iget-object v0, p0, Lcom/teamspeak/ts3client/ah;->c:Lcom/teamspeak/ts3client/t;

    invoke-static {v0}, Lcom/teamspeak/ts3client/t;->b(Lcom/teamspeak/ts3client/t;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 50309
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50310
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->k:Lcom/teamspeak/ts3client/t;

    .line 50311
    iget-boolean v1, v0, Lcom/teamspeak/ts3client/t;->b:Z

    iput-boolean v1, v0, Lcom/teamspeak/ts3client/t;->e:Z

    .line 50312
    iget-boolean v1, v0, Lcom/teamspeak/ts3client/t;->c:Z

    iput-boolean v1, v0, Lcom/teamspeak/ts3client/t;->f:Z

    .line 50313
    iget-object v1, v0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->g()Lcom/teamspeak/ts3client/a/p;

    move-result-object v1

    .line 50327
    iput-boolean v7, v1, Lcom/teamspeak/ts3client/a/p;->a:Z

    .line 50314
    iget-object v1, v0, Lcom/teamspeak/ts3client/t;->g:Lcom/teamspeak/ts3client/customs/FloatingButton;

    const v2, 0x7f020084

    iget v3, v0, Lcom/teamspeak/ts3client/t;->d:I

    int-to-float v3, v3

    iget v4, v0, Lcom/teamspeak/ts3client/t;->d:I

    int-to-float v4, v4

    invoke-static {v2, v3, v4}, Lcom/teamspeak/ts3client/data/d/t;->a(IFF)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/teamspeak/ts3client/customs/FloatingButton;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 50315
    iget-object v1, v0, Lcom/teamspeak/ts3client/t;->h:Lcom/teamspeak/ts3client/customs/FloatingButton;

    const v2, 0x7f020098

    iget v3, v0, Lcom/teamspeak/ts3client/t;->d:I

    int-to-float v3, v3

    iget v4, v0, Lcom/teamspeak/ts3client/t;->d:I

    int-to-float v4, v4

    invoke-static {v2, v3, v4}, Lcom/teamspeak/ts3client/data/d/t;->a(IFF)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/teamspeak/ts3client/customs/FloatingButton;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 50317
    iput-boolean v7, v0, Lcom/teamspeak/ts3client/t;->c:Z

    .line 50319
    iget-object v1, v0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50329
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50330
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 50319
    iget-object v2, v0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50331
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50332
    iget-wide v2, v2, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 50319
    sget-object v4, Lcom/teamspeak/ts3client/jni/d;->g:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v1, v2, v3, v4, v7}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JLcom/teamspeak/ts3client/jni/d;I)I

    .line 50320
    iget-object v1, v0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50333
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50334
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 50320
    iget-object v2, v0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50335
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50336
    iget-wide v2, v2, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 50320
    const-string v4, ""

    invoke-virtual {v1, v2, v3, v4}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_flushClientSelfUpdates(JLjava/lang/String;)I

    .line 50321
    iput-boolean v7, v0, Lcom/teamspeak/ts3client/t;->b:Z

    .line 50324
    iget-object v1, v0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50337
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50338
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 50324
    iget-object v2, v0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50339
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50340
    iget-wide v2, v2, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 50324
    sget-object v4, Lcom/teamspeak/ts3client/jni/d;->f:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v1, v2, v3, v4, v7}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JLcom/teamspeak/ts3client/jni/d;I)I

    .line 50325
    iget-object v1, v0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50341
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50342
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 50325
    iget-object v0, v0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50343
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50344
    iget-wide v2, v0, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 50325
    const-string v0, ""

    invoke-virtual {v1, v2, v3, v0}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_flushClientSelfUpdates(JLjava/lang/String;)I

    .line 162
    iget-object v0, p0, Lcom/teamspeak/ts3client/ah;->c:Lcom/teamspeak/ts3client/t;

    invoke-static {v0}, Lcom/teamspeak/ts3client/t;->b(Lcom/teamspeak/ts3client/t;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 50345
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 162
    const-string v1, "call_setaway"

    invoke-interface {v0, v1, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_14d

    .line 163
    iget-object v0, p0, Lcom/teamspeak/ts3client/ah;->c:Lcom/teamspeak/ts3client/t;

    invoke-static {v0}, Lcom/teamspeak/ts3client/t;->b(Lcom/teamspeak/ts3client/t;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 50346
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50347
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 163
    iget-object v1, p0, Lcom/teamspeak/ts3client/ah;->c:Lcom/teamspeak/ts3client/t;

    invoke-static {v1}, Lcom/teamspeak/ts3client/t;->b(Lcom/teamspeak/ts3client/t;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v1

    .line 50348
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50349
    iget-wide v2, v1, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 163
    sget-object v1, Lcom/teamspeak/ts3client/jni/d;->N:Lcom/teamspeak/ts3client/jni/d;

    iget-object v4, p0, Lcom/teamspeak/ts3client/ah;->c:Lcom/teamspeak/ts3client/t;

    invoke-static {v4}, Lcom/teamspeak/ts3client/t;->b(Lcom/teamspeak/ts3client/t;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v4

    .line 50350
    iget-object v4, v4, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 163
    const-string v5, "call_awaymessage"

    const-string v6, "On the Phone"

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v2, v3, v1, v4}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JLcom/teamspeak/ts3client/jni/d;Ljava/lang/String;)I

    .line 164
    iget-object v0, p0, Lcom/teamspeak/ts3client/ah;->c:Lcom/teamspeak/ts3client/t;

    invoke-static {v0}, Lcom/teamspeak/ts3client/t;->b(Lcom/teamspeak/ts3client/t;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 50351
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50352
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 164
    iget-object v1, p0, Lcom/teamspeak/ts3client/ah;->c:Lcom/teamspeak/ts3client/t;

    invoke-static {v1}, Lcom/teamspeak/ts3client/t;->b(Lcom/teamspeak/ts3client/t;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v1

    .line 50353
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50354
    iget-wide v2, v1, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 164
    sget-object v1, Lcom/teamspeak/ts3client/jni/d;->M:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v0, v2, v3, v1, v7}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JLcom/teamspeak/ts3client/jni/d;I)I

    .line 165
    iget-object v0, p0, Lcom/teamspeak/ts3client/ah;->c:Lcom/teamspeak/ts3client/t;

    invoke-static {v0}, Lcom/teamspeak/ts3client/t;->b(Lcom/teamspeak/ts3client/t;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 50355
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50356
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 165
    iget-object v1, p0, Lcom/teamspeak/ts3client/ah;->c:Lcom/teamspeak/ts3client/t;

    invoke-static {v1}, Lcom/teamspeak/ts3client/t;->b(Lcom/teamspeak/ts3client/t;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v1

    .line 50357
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50358
    iget-wide v2, v1, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 165
    const-string v1, ""

    invoke-virtual {v0, v2, v3, v1}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_flushClientSelfUpdates(JLjava/lang/String;)I

    .line 166
    iget-object v0, p0, Lcom/teamspeak/ts3client/ah;->c:Lcom/teamspeak/ts3client/t;

    invoke-static {v0}, Lcom/teamspeak/ts3client/t;->b(Lcom/teamspeak/ts3client/t;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 50359
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50360
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->v:Lcom/teamspeak/ts3client/data/ab;

    .line 50361
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/ab;->s:Ljava/util/BitSet;

    .line 166
    invoke-virtual {v0, v8}, Ljava/util/BitSet;->set(I)V

    .line 167
    iget-object v0, p0, Lcom/teamspeak/ts3client/ah;->c:Lcom/teamspeak/ts3client/t;

    invoke-static {v0}, Lcom/teamspeak/ts3client/t;->b(Lcom/teamspeak/ts3client/t;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 50362
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50363
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->v:Lcom/teamspeak/ts3client/data/ab;

    .line 50364
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/ab;->s:Ljava/util/BitSet;

    .line 167
    invoke-virtual {v0, v7}, Ljava/util/BitSet;->set(I)V

    .line 168
    iget-object v0, p0, Lcom/teamspeak/ts3client/ah;->c:Lcom/teamspeak/ts3client/t;

    invoke-static {v0}, Lcom/teamspeak/ts3client/t;->b(Lcom/teamspeak/ts3client/t;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 50365
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50366
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->v:Lcom/teamspeak/ts3client/data/ab;

    .line 50367
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/ab;->s:Ljava/util/BitSet;

    .line 168
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 169
    iget-object v0, p0, Lcom/teamspeak/ts3client/ah;->c:Lcom/teamspeak/ts3client/t;

    invoke-static {v0}, Lcom/teamspeak/ts3client/t;->b(Lcom/teamspeak/ts3client/t;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 50368
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50369
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->v:Lcom/teamspeak/ts3client/data/ab;

    .line 169
    iget-object v1, p0, Lcom/teamspeak/ts3client/ah;->c:Lcom/teamspeak/ts3client/t;

    invoke-static {v1}, Lcom/teamspeak/ts3client/t;->b(Lcom/teamspeak/ts3client/t;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v1

    .line 50370
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 169
    const-string v2, "call_awaymessage"

    const-string v3, "On the Phone"

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 50371
    iput-object v1, v0, Lcom/teamspeak/ts3client/data/ab;->t:Ljava/lang/String;

    .line 172
    :cond_14d
    iget-object v0, p0, Lcom/teamspeak/ts3client/ah;->c:Lcom/teamspeak/ts3client/t;

    invoke-static {v0}, Lcom/teamspeak/ts3client/t;->b(Lcom/teamspeak/ts3client/t;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 50373
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50374
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->b:Lcom/teamspeak/ts3client/a/k;

    .line 172
    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/a/k;->a(Ljava/lang/Boolean;)V

    .line 173
    iput-boolean v7, p0, Lcom/teamspeak/ts3client/ah;->a:Z

    .line 174
    iget-object v0, p0, Lcom/teamspeak/ts3client/ah;->c:Lcom/teamspeak/ts3client/t;

    invoke-static {v0}, Lcom/teamspeak/ts3client/t;->b(Lcom/teamspeak/ts3client/t;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 50375
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50376
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->b:Lcom/teamspeak/ts3client/a/k;

    .line 174
    invoke-virtual {v0}, Lcom/teamspeak/ts3client/a/k;->g()V

    .line 177
    :cond_16d
    return-void
.end method


# virtual methods
.method public final onCallStateChanged(ILjava/lang/String;)V
    .registers 10

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 116
    iget-object v0, p0, Lcom/teamspeak/ts3client/ah;->c:Lcom/teamspeak/ts3client/t;

    invoke-static {v0}, Lcom/teamspeak/ts3client/t;->b(Lcom/teamspeak/ts3client/t;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 1061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 1234
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 116
    iget-object v1, p0, Lcom/teamspeak/ts3client/ah;->c:Lcom/teamspeak/ts3client/t;

    invoke-static {v1}, Lcom/teamspeak/ts3client/t;->b(Lcom/teamspeak/ts3client/t;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v1

    .line 2061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 2267
    iget-wide v2, v1, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 116
    sget-object v1, Lcom/teamspeak/ts3client/jni/d;->M:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v0, v2, v3, v1}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JLcom/teamspeak/ts3client/jni/d;)I

    move-result v0

    if-ne v0, v6, :cond_23

    iget-boolean v0, p0, Lcom/teamspeak/ts3client/ah;->a:Z

    if-nez v0, :cond_23

    .line 137
    :cond_22
    :goto_22
    return-void

    .line 119
    :cond_23
    iget v0, p0, Lcom/teamspeak/ts3client/ah;->b:I

    if-eq p1, v0, :cond_22

    .line 121
    iput p1, p0, Lcom/teamspeak/ts3client/ah;->b:I

    .line 122
    iget-object v0, p0, Lcom/teamspeak/ts3client/ah;->c:Lcom/teamspeak/ts3client/t;

    invoke-static {v0}, Lcom/teamspeak/ts3client/t;->b(Lcom/teamspeak/ts3client/t;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 3085
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->d:Ljava/util/logging/Logger;

    .line 122
    sget-object v1, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "CallState: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 123
    packed-switch p1, :pswitch_data_1dc

    goto :goto_22

    .line 3140
    :pswitch_49
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/ah;->a:Z

    if-eqz v0, :cond_22

    .line 3141
    iget-object v0, p0, Lcom/teamspeak/ts3client/ah;->c:Lcom/teamspeak/ts3client/t;

    invoke-static {v0}, Lcom/teamspeak/ts3client/t;->b(Lcom/teamspeak/ts3client/t;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 4061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 3141
    if-eqz v0, :cond_fc

    iget-object v0, p0, Lcom/teamspeak/ts3client/ah;->c:Lcom/teamspeak/ts3client/t;

    invoke-static {v0}, Lcom/teamspeak/ts3client/t;->b(Lcom/teamspeak/ts3client/t;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 5061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 5206
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->k:Lcom/teamspeak/ts3client/t;

    .line 3141
    if-eqz v0, :cond_fc

    .line 3142
    iget-object v0, p0, Lcom/teamspeak/ts3client/ah;->c:Lcom/teamspeak/ts3client/t;

    invoke-static {v0}, Lcom/teamspeak/ts3client/t;->b(Lcom/teamspeak/ts3client/t;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 6093
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 3142
    const-string v1, "call_setaway"

    invoke-interface {v0, v1, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_fc

    .line 3143
    iget-object v0, p0, Lcom/teamspeak/ts3client/ah;->c:Lcom/teamspeak/ts3client/t;

    invoke-static {v0}, Lcom/teamspeak/ts3client/t;->b(Lcom/teamspeak/ts3client/t;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 7061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 7234
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 3143
    iget-object v1, p0, Lcom/teamspeak/ts3client/ah;->c:Lcom/teamspeak/ts3client/t;

    invoke-static {v1}, Lcom/teamspeak/ts3client/t;->b(Lcom/teamspeak/ts3client/t;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v1

    .line 8061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 8267
    iget-wide v2, v1, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 3143
    sget-object v1, Lcom/teamspeak/ts3client/jni/d;->N:Lcom/teamspeak/ts3client/jni/d;

    const-string v4, ""

    invoke-virtual {v0, v2, v3, v1, v4}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JLcom/teamspeak/ts3client/jni/d;Ljava/lang/String;)I

    .line 3144
    iget-object v0, p0, Lcom/teamspeak/ts3client/ah;->c:Lcom/teamspeak/ts3client/t;

    invoke-static {v0}, Lcom/teamspeak/ts3client/t;->b(Lcom/teamspeak/ts3client/t;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 9061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 9234
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 3144
    iget-object v1, p0, Lcom/teamspeak/ts3client/ah;->c:Lcom/teamspeak/ts3client/t;

    invoke-static {v1}, Lcom/teamspeak/ts3client/t;->b(Lcom/teamspeak/ts3client/t;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v1

    .line 10061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 10267
    iget-wide v2, v1, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 3144
    sget-object v1, Lcom/teamspeak/ts3client/jni/d;->M:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v0, v2, v3, v1, v5}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JLcom/teamspeak/ts3client/jni/d;I)I

    .line 3145
    iget-object v0, p0, Lcom/teamspeak/ts3client/ah;->c:Lcom/teamspeak/ts3client/t;

    invoke-static {v0}, Lcom/teamspeak/ts3client/t;->b(Lcom/teamspeak/ts3client/t;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 11061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 11234
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 3145
    iget-object v1, p0, Lcom/teamspeak/ts3client/ah;->c:Lcom/teamspeak/ts3client/t;

    invoke-static {v1}, Lcom/teamspeak/ts3client/t;->b(Lcom/teamspeak/ts3client/t;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v1

    .line 12061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 12267
    iget-wide v2, v1, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 3145
    const-string v1, ""

    invoke-virtual {v0, v2, v3, v1}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_flushClientSelfUpdates(JLjava/lang/String;)I

    .line 3146
    iget-object v0, p0, Lcom/teamspeak/ts3client/ah;->c:Lcom/teamspeak/ts3client/t;

    invoke-static {v0}, Lcom/teamspeak/ts3client/t;->b(Lcom/teamspeak/ts3client/t;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 13061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 13360
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->v:Lcom/teamspeak/ts3client/data/ab;

    .line 14243
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/ab;->s:Ljava/util/BitSet;

    .line 3146
    invoke-virtual {v0, v5}, Ljava/util/BitSet;->clear(I)V

    .line 3147
    iget-object v0, p0, Lcom/teamspeak/ts3client/ah;->c:Lcom/teamspeak/ts3client/t;

    invoke-static {v0}, Lcom/teamspeak/ts3client/t;->b(Lcom/teamspeak/ts3client/t;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 15061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 15360
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->v:Lcom/teamspeak/ts3client/data/ab;

    .line 16243
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/ab;->s:Ljava/util/BitSet;

    .line 3147
    invoke-virtual {v0, v6}, Ljava/util/BitSet;->clear(I)V

    .line 3148
    iget-object v0, p0, Lcom/teamspeak/ts3client/ah;->c:Lcom/teamspeak/ts3client/t;

    invoke-static {v0}, Lcom/teamspeak/ts3client/t;->b(Lcom/teamspeak/ts3client/t;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 17061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 17360
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->v:Lcom/teamspeak/ts3client/data/ab;

    .line 18243
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/ab;->s:Ljava/util/BitSet;

    .line 3148
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->clear(I)V

    .line 3149
    iget-object v0, p0, Lcom/teamspeak/ts3client/ah;->c:Lcom/teamspeak/ts3client/t;

    invoke-static {v0}, Lcom/teamspeak/ts3client/t;->b(Lcom/teamspeak/ts3client/t;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 19061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 19360
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->v:Lcom/teamspeak/ts3client/data/ab;

    .line 3149
    const-string v1, ""

    .line 20251
    iput-object v1, v0, Lcom/teamspeak/ts3client/data/ab;->t:Ljava/lang/String;

    .line 3152
    :cond_fc
    iput-boolean v5, p0, Lcom/teamspeak/ts3client/ah;->a:Z

    .line 3153
    iget-object v0, p0, Lcom/teamspeak/ts3client/ah;->c:Lcom/teamspeak/ts3client/t;

    invoke-static {v0}, Lcom/teamspeak/ts3client/t;->b(Lcom/teamspeak/ts3client/t;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 21061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 21206
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->k:Lcom/teamspeak/ts3client/t;

    .line 22187
    iget-boolean v1, v0, Lcom/teamspeak/ts3client/t;->f:Z

    if-nez v1, :cond_1b5

    .line 22188
    iput-boolean v5, v0, Lcom/teamspeak/ts3client/t;->c:Z

    .line 22189
    iget-object v1, v0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->g()Lcom/teamspeak/ts3client/a/p;

    move-result-object v1

    .line 23117
    iput-boolean v5, v1, Lcom/teamspeak/ts3client/a/p;->a:Z

    .line 22190
    iget-object v1, v0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 24061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 24234
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 22190
    iget-object v2, v0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 25061
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 25267
    iget-wide v2, v2, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 22190
    sget-object v4, Lcom/teamspeak/ts3client/jni/d;->g:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JLcom/teamspeak/ts3client/jni/d;I)I

    .line 22191
    iget-object v1, v0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 26061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 26234
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 22191
    iget-object v2, v0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 27061
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 27267
    iget-wide v2, v2, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 22191
    const-string v4, ""

    invoke-virtual {v1, v2, v3, v4}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_flushClientSelfUpdates(JLjava/lang/String;)I

    .line 22192
    iget-object v1, v0, Lcom/teamspeak/ts3client/t;->h:Lcom/teamspeak/ts3client/customs/FloatingButton;

    const v2, 0x7f020096

    iget v3, v0, Lcom/teamspeak/ts3client/t;->d:I

    int-to-float v3, v3

    iget v4, v0, Lcom/teamspeak/ts3client/t;->d:I

    int-to-float v4, v4

    invoke-static {v2, v3, v4}, Lcom/teamspeak/ts3client/data/d/t;->a(IFF)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/teamspeak/ts3client/customs/FloatingButton;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 22193
    iget-object v1, v0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 28061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 28360
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->v:Lcom/teamspeak/ts3client/data/ab;

    .line 29243
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/ab;->s:Ljava/util/BitSet;

    .line 22193
    invoke-virtual {v1, v6}, Ljava/util/BitSet;->set(I)V

    .line 22198
    :goto_155
    iget-boolean v1, v0, Lcom/teamspeak/ts3client/t;->e:Z

    if-nez v1, :cond_1c8

    .line 22199
    iput-boolean v5, v0, Lcom/teamspeak/ts3client/t;->b:Z

    .line 22200
    iget-object v1, v0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 30061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 30234
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 22200
    iget-object v2, v0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 31061
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 31267
    iget-wide v2, v2, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 22200
    sget-object v4, Lcom/teamspeak/ts3client/jni/d;->f:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JLcom/teamspeak/ts3client/jni/d;I)I

    .line 22201
    iget-object v1, v0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 32061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 32234
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 22201
    iget-object v2, v0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 33061
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 33267
    iget-wide v2, v2, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 22201
    const-string v4, ""

    invoke-virtual {v1, v2, v3, v4}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_flushClientSelfUpdates(JLjava/lang/String;)I

    .line 22202
    iget-object v1, v0, Lcom/teamspeak/ts3client/t;->g:Lcom/teamspeak/ts3client/customs/FloatingButton;

    const v2, 0x7f020081

    iget v3, v0, Lcom/teamspeak/ts3client/t;->d:I

    int-to-float v3, v3

    iget v4, v0, Lcom/teamspeak/ts3client/t;->d:I

    int-to-float v4, v4

    invoke-static {v2, v3, v4}, Lcom/teamspeak/ts3client/data/d/t;->a(IFF)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/teamspeak/ts3client/customs/FloatingButton;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 22203
    iget-object v1, v0, Lcom/teamspeak/ts3client/t;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 34061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 34360
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->v:Lcom/teamspeak/ts3client/data/ab;

    .line 35243
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/ab;->s:Ljava/util/BitSet;

    .line 22203
    invoke-virtual {v1, v5}, Ljava/util/BitSet;->set(I)V

    .line 22208
    :goto_19a
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    .line 22209
    new-instance v2, Lcom/teamspeak/ts3client/as;

    invoke-direct {v2, v0}, Lcom/teamspeak/ts3client/as;-><init>(Lcom/teamspeak/ts3client/t;)V

    const-wide/16 v4, 0x3e8

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_22

    .line 125
    :pswitch_1ab
    invoke-direct {p0}, Lcom/teamspeak/ts3client/ah;->b()V

    goto/16 :goto_22

    .line 129
    :pswitch_1b0
    invoke-direct {p0}, Lcom/teamspeak/ts3client/ah;->b()V

    goto/16 :goto_22

    .line 22195
    :cond_1b5
    iget-object v1, v0, Lcom/teamspeak/ts3client/t;->h:Lcom/teamspeak/ts3client/customs/FloatingButton;

    const v2, 0x7f020098

    iget v3, v0, Lcom/teamspeak/ts3client/t;->d:I

    int-to-float v3, v3

    iget v4, v0, Lcom/teamspeak/ts3client/t;->d:I

    int-to-float v4, v4

    invoke-static {v2, v3, v4}, Lcom/teamspeak/ts3client/data/d/t;->a(IFF)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/teamspeak/ts3client/customs/FloatingButton;->setBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_155

    .line 22205
    :cond_1c8
    iget-object v1, v0, Lcom/teamspeak/ts3client/t;->g:Lcom/teamspeak/ts3client/customs/FloatingButton;

    const v2, 0x7f020084

    iget v3, v0, Lcom/teamspeak/ts3client/t;->d:I

    int-to-float v3, v3

    iget v4, v0, Lcom/teamspeak/ts3client/t;->d:I

    int-to-float v4, v4

    invoke-static {v2, v3, v4}, Lcom/teamspeak/ts3client/data/d/t;->a(IFF)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/teamspeak/ts3client/customs/FloatingButton;->setBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_19a

    .line 123
    nop

    :pswitch_data_1dc
    .packed-switch 0x0
        :pswitch_49
        :pswitch_1ab
        :pswitch_1b0
    .end packed-switch
.end method
