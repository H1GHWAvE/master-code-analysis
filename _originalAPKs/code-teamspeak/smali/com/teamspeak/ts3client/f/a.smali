.class public final Lcom/teamspeak/ts3client/f/a;
.super Landroid/widget/TextView;
.source "SourceFile"


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .registers 6

    .prologue
    const/4 v1, 0x0

    .line 14
    invoke-direct {p0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 15
    const/high16 v0, 0x41b80000    # 23.0f

    invoke-virtual {p0, v0}, Lcom/teamspeak/ts3client/f/a;->setTextSize(F)V

    .line 16
    const/4 v0, 0x5

    invoke-virtual {p0, v1, v1, v1, v0}, Lcom/teamspeak/ts3client/f/a;->setPadding(IIII)V

    .line 17
    const/high16 v0, 0x40a00000    # 5.0f

    const/high16 v1, 0x40000000    # 2.0f

    const/high16 v2, 0x40400000    # 3.0f

    const/high16 v3, -0x1000000

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/teamspeak/ts3client/f/a;->setShadowLayer(FFFI)V

    .line 18
    const v0, 0x7f0200b0

    invoke-virtual {p0, v0}, Lcom/teamspeak/ts3client/f/a;->setBackgroundResource(I)V

    .line 19
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0}, Lcom/teamspeak/ts3client/f/a;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 20
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/teamspeak/ts3client/f/a;->setGravity(I)V

    .line 21
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/f/a;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    const/16 v1, 0x3c

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 22
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .registers 3

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/teamspeak/ts3client/f/a;-><init>(Landroid/content/Context;)V

    .line 26
    invoke-virtual {p0, p2}, Lcom/teamspeak/ts3client/f/a;->setText(Ljava/lang/CharSequence;)V

    .line 27
    return-void
.end method
