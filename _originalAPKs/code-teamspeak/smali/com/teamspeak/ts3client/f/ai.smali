.class final Lcom/teamspeak/ts3client/f/ai;
.super Landroid/support/v4/app/ax;
.source "SourceFile"


# instance fields
.field at:Landroid/widget/ArrayAdapter;

.field au:Ljava/util/HashMap;

.field final synthetic av:Lcom/teamspeak/ts3client/f/ag;

.field private aw:Landroid/widget/Spinner;

.field private ax:Landroid/view/View;


# direct methods
.method private constructor <init>(Lcom/teamspeak/ts3client/f/ag;)V
    .registers 3

    .prologue
    .line 79
    iput-object p1, p0, Lcom/teamspeak/ts3client/f/ai;->av:Lcom/teamspeak/ts3client/f/ag;

    invoke-direct {p0}, Landroid/support/v4/app/ax;-><init>()V

    .line 82
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/teamspeak/ts3client/f/ai;->au:Ljava/util/HashMap;

    return-void
.end method

.method synthetic constructor <init>(Lcom/teamspeak/ts3client/f/ag;B)V
    .registers 3

    .prologue
    .line 79
    invoke-direct {p0, p1}, Lcom/teamspeak/ts3client/f/ai;-><init>(Lcom/teamspeak/ts3client/f/ag;)V

    return-void
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/f/ai;)Landroid/widget/Spinner;
    .registers 2

    .prologue
    .line 79
    iget-object v0, p0, Lcom/teamspeak/ts3client/f/ai;->aw:Landroid/widget/Spinner;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 16

    .prologue
    const/4 v12, 0x3

    const/4 v11, -0x1

    const/4 v10, -0x2

    const/4 v2, 0x0

    .line 89
    invoke-virtual {p1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/Ts3Application;

    .line 91
    new-instance v3, Landroid/widget/RelativeLayout;

    invoke-virtual {p1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v3, v1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 92
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v11, v10}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v3, v1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 94
    const v1, 0x7f030046

    const/4 v4, 0x0

    invoke-virtual {p1, v1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/teamspeak/ts3client/f/ai;->ax:Landroid/view/View;

    .line 95
    iget-object v1, p0, Lcom/teamspeak/ts3client/f/ai;->ax:Landroid/view/View;

    const v4, 0x7f0c0190

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Spinner;

    iput-object v1, p0, Lcom/teamspeak/ts3client/f/ai;->aw:Landroid/widget/Spinner;

    .line 97
    iget-object v1, p0, Lcom/teamspeak/ts3client/f/ai;->av:Lcom/teamspeak/ts3client/f/ag;

    invoke-static {v1}, Lcom/teamspeak/ts3client/f/ag;->a(Lcom/teamspeak/ts3client/f/ag;)I

    move-result v1

    packed-switch v1, :pswitch_data_178

    .line 126
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/f/ai;->h()Landroid/content/Context;

    move-result-object v1

    iget-object v4, p0, Lcom/teamspeak/ts3client/f/ai;->av:Lcom/teamspeak/ts3client/f/ag;

    invoke-static {v4}, Lcom/teamspeak/ts3client/f/ag;->a(Lcom/teamspeak/ts3client/f/ag;)I

    move-result v4

    const v5, 0x1090008

    invoke-static {v1, v4, v5}, Landroid/widget/ArrayAdapter;->createFromResource(Landroid/content/Context;II)Landroid/widget/ArrayAdapter;

    move-result-object v1

    .line 127
    iget-object v4, p0, Lcom/teamspeak/ts3client/f/ai;->aw:Landroid/widget/Spinner;

    invoke-virtual {v4, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 128
    iget-object v1, p0, Lcom/teamspeak/ts3client/f/ai;->aw:Landroid/widget/Spinner;

    .line 3093
    iget-object v4, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 128
    iget-object v5, p0, Lcom/teamspeak/ts3client/f/ai;->av:Lcom/teamspeak/ts3client/f/ag;

    invoke-static {v5}, Lcom/teamspeak/ts3client/f/ag;->b(Lcom/teamspeak/ts3client/f/ag;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/Spinner;->setSelection(I)V

    .line 133
    :goto_66
    iget-object v1, p0, Lcom/teamspeak/ts3client/f/ai;->ax:Landroid/view/View;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/view/View;->setId(I)V

    .line 134
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v11, v10}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 135
    const/16 v2, 0xa

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 136
    iget-object v2, p0, Lcom/teamspeak/ts3client/f/ai;->ax:Landroid/view/View;

    invoke-virtual {v3, v2, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 138
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v11, v10}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 139
    iget-object v2, p0, Lcom/teamspeak/ts3client/f/ai;->ax:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getId()I

    move-result v2

    invoke-virtual {v1, v12, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 140
    new-instance v2, Landroid/widget/Button;

    invoke-virtual {p1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v2, v4}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 141
    const-string v4, "button.save"

    invoke-static {v4}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 142
    new-instance v4, Lcom/teamspeak/ts3client/f/aj;

    invoke-direct {v4, p0, v0, v3}, Lcom/teamspeak/ts3client/f/aj;-><init>(Lcom/teamspeak/ts3client/f/ai;Lcom/teamspeak/ts3client/Ts3Application;Landroid/widget/RelativeLayout;)V

    invoke-virtual {v2, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 158
    invoke-virtual {v3, v2, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 3207
    iget-object v0, p0, Landroid/support/v4/app/ax;->j:Landroid/app/Dialog;

    .line 159
    iget-object v1, p0, Lcom/teamspeak/ts3client/f/ai;->av:Lcom/teamspeak/ts3client/f/ag;

    invoke-static {v1}, Lcom/teamspeak/ts3client/f/ag;->d(Lcom/teamspeak/ts3client/f/ag;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 161
    return-object v3

    .line 99
    :pswitch_b2
    const-string v1, "settings.screenrotation.array"

    invoke-virtual {p0}, Lcom/teamspeak/ts3client/f/ai;->i()Landroid/support/v4/app/bb;

    move-result-object v4

    invoke-static {v1, v4, v12}, Lcom/teamspeak/ts3client/data/e/a;->b(Ljava/lang/String;Landroid/content/Context;I)Lcom/teamspeak/ts3client/customs/a;

    move-result-object v1

    .line 100
    iget-object v4, p0, Lcom/teamspeak/ts3client/f/ai;->aw:Landroid/widget/Spinner;

    invoke-virtual {v4, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 101
    iget-object v1, p0, Lcom/teamspeak/ts3client/f/ai;->aw:Landroid/widget/Spinner;

    .line 1093
    iget-object v4, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 101
    iget-object v5, p0, Lcom/teamspeak/ts3client/f/ai;->av:Lcom/teamspeak/ts3client/f/ag;

    invoke-static {v5}, Lcom/teamspeak/ts3client/f/ag;->b(Lcom/teamspeak/ts3client/f/ag;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/Spinner;->setSelection(I)V

    goto :goto_66

    .line 104
    :pswitch_d3
    const-string v1, "settings.whisper.array"

    invoke-virtual {p0}, Lcom/teamspeak/ts3client/f/ai;->i()Landroid/support/v4/app/bb;

    move-result-object v4

    invoke-static {v1, v4, v12}, Lcom/teamspeak/ts3client/data/e/a;->b(Ljava/lang/String;Landroid/content/Context;I)Lcom/teamspeak/ts3client/customs/a;

    move-result-object v1

    .line 105
    iget-object v4, p0, Lcom/teamspeak/ts3client/f/ai;->aw:Landroid/widget/Spinner;

    invoke-virtual {v4, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 106
    iget-object v1, p0, Lcom/teamspeak/ts3client/f/ai;->aw:Landroid/widget/Spinner;

    .line 2093
    iget-object v4, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 106
    iget-object v5, p0, Lcom/teamspeak/ts3client/f/ai;->av:Lcom/teamspeak/ts3client/f/ag;

    invoke-static {v5}, Lcom/teamspeak/ts3client/f/ag;->b(Lcom/teamspeak/ts3client/f/ag;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/Spinner;->setSelection(I)V

    goto/16 :goto_66

    .line 109
    :pswitch_f5
    new-instance v4, Ljava/util/ArrayList;

    .line 2101
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, "/TS3/content/lang/"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2102
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2104
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_13f

    .line 2105
    new-instance v1, Lcom/teamspeak/ts3client/data/e/b;

    invoke-direct {v1, v5}, Lcom/teamspeak/ts3client/data/e/b;-><init>(Ljava/io/File;)V

    .line 2119
    invoke-virtual {v5, v1}, Ljava/io/File;->list(Ljava/io/FilenameFilter;)[Ljava/lang/String;

    move-result-object v1

    .line 2123
    :goto_122
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    .line 109
    invoke-direct {v4, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 113
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_12d
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_142

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 114
    iget-object v6, p0, Lcom/teamspeak/ts3client/f/ai;->au:Ljava/util/HashMap;

    invoke-virtual {v6, v1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_12d

    .line 2121
    :cond_13f
    new-array v1, v2, [Ljava/lang/String;

    goto :goto_122

    .line 117
    :cond_142
    invoke-static {}, Lcom/teamspeak/ts3client/data/e/c;->values()[Lcom/teamspeak/ts3client/data/e/c;

    move-result-object v5

    array-length v6, v5

    move v1, v2

    :goto_148
    if-ge v1, v6, :cond_161

    aget-object v7, v5, v1

    .line 118
    invoke-virtual {v7}, Lcom/teamspeak/ts3client/data/e/c;->name()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v4, v2, v8}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 119
    iget-object v8, p0, Lcom/teamspeak/ts3client/f/ai;->au:Ljava/util/HashMap;

    invoke-virtual {v7}, Lcom/teamspeak/ts3client/data/e/c;->name()Ljava/lang/String;

    move-result-object v9

    .line 2260
    iget-object v7, v7, Lcom/teamspeak/ts3client/data/e/c;->i:Ljava/lang/String;

    .line 119
    invoke-virtual {v8, v9, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 117
    add-int/lit8 v1, v1, 0x1

    goto :goto_148

    .line 121
    :cond_161
    new-instance v1, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Lcom/teamspeak/ts3client/f/ai;->h()Landroid/content/Context;

    move-result-object v2

    const v5, 0x1090009

    invoke-direct {v1, v2, v5, v4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    iput-object v1, p0, Lcom/teamspeak/ts3client/f/ai;->at:Landroid/widget/ArrayAdapter;

    .line 122
    iget-object v1, p0, Lcom/teamspeak/ts3client/f/ai;->aw:Landroid/widget/Spinner;

    iget-object v2, p0, Lcom/teamspeak/ts3client/f/ai;->at:Landroid/widget/ArrayAdapter;

    invoke-virtual {v1, v2}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    goto/16 :goto_66

    .line 97
    :pswitch_data_178
    .packed-switch -0x3
        :pswitch_b2
        :pswitch_d3
        :pswitch_f5
    .end packed-switch
.end method
