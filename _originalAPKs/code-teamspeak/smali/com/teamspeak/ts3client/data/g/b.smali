.class public final Lcom/teamspeak/ts3client/data/g/b;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Ljava/util/HashMap;


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/g/b;->a:Ljava/util/HashMap;

    .line 13
    return-void
.end method

.method private a()Ljava/util/HashMap;
    .registers 2

    .prologue
    .line 28
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/g/b;->a:Ljava/util/HashMap;

    return-object v0
.end method

.method private b()Ljava/util/ArrayList;
    .registers 3

    .prologue
    .line 36
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 37
    iget-object v1, p0, Lcom/teamspeak/ts3client/data/g/b;->a:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 38
    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 39
    return-object v0
.end method

.method private b(Lcom/teamspeak/ts3client/data/g/a;)V
    .registers 6

    .prologue
    .line 32
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/g/b;->a:Ljava/util/HashMap;

    .line 2066
    iget-wide v2, p1, Lcom/teamspeak/ts3client/data/g/a;->a:J

    .line 32
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 33
    return-void
.end method


# virtual methods
.method public final a(Lcom/teamspeak/ts3client/data/g/a;)V
    .registers 6

    .prologue
    .line 16
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/g/b;->a:Ljava/util/HashMap;

    .line 1066
    iget-wide v2, p1, Lcom/teamspeak/ts3client/data/g/a;->a:J

    .line 16
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 17
    return-void
.end method

.method public final a(J)Z
    .registers 6

    .prologue
    .line 20
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/g/b;->a:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final b(J)Lcom/teamspeak/ts3client/data/g/a;
    .registers 6

    .prologue
    .line 24
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/g/b;->a:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/data/g/a;

    return-object v0
.end method
