.class public final Lcom/teamspeak/ts3client/data/g;
.super Landroid/widget/BaseExpandableListAdapter;
.source "SourceFile"


# instance fields
.field public a:Ljava/util/List;

.field public b:Lcom/teamspeak/ts3client/data/customExpandableListView;

.field public c:Z

.field public d:Z

.field private e:Landroid/content/Context;

.field private f:Lcom/teamspeak/ts3client/Ts3Application;

.field private g:Landroid/view/LayoutInflater;

.field private h:I

.field private i:I

.field private j:Landroid/support/v4/app/bi;

.field private k:I

.field private l:Ljava/util/regex/Pattern;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/support/v4/app/bi;)V
    .registers 7

    .prologue
    const/16 v3, 0x19

    const/4 v2, 0x1

    .line 66
    invoke-direct {p0}, Landroid/widget/BaseExpandableListAdapter;-><init>()V

    .line 56
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/g;->a:Ljava/util/List;

    .line 67
    iput-object p1, p0, Lcom/teamspeak/ts3client/data/g;->e:Landroid/content/Context;

    .line 68
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/Ts3Application;

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/g;->f:Lcom/teamspeak/ts3client/Ts3Application;

    .line 69
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/g;->g:Landroid/view/LayoutInflater;

    .line 70
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/g;->f:Lcom/teamspeak/ts3client/Ts3Application;

    .line 1093
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 70
    const-string v1, "channel_height"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    invoke-static {v2, v0, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/teamspeak/ts3client/data/g;->h:I

    .line 71
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/g;->f:Lcom/teamspeak/ts3client/Ts3Application;

    .line 2093
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 71
    const-string v1, "client_height"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    invoke-static {v2, v0, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/teamspeak/ts3client/data/g;->i:I

    .line 72
    iput-object p2, p0, Lcom/teamspeak/ts3client/data/g;->j:Landroid/support/v4/app/bi;

    .line 73
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 74
    invoke-virtual {v0}, Landroid/widget/TextView;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/ColorStateList;->getDefaultColor()I

    move-result v0

    iput v0, p0, Lcom/teamspeak/ts3client/data/g;->k:I

    .line 76
    const-string v0, "^[\\W]*\\[(.*)spacer.*?\\](.*)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/g;->l:Ljava/util/regex/Pattern;

    .line 77
    return-void
.end method

.method private a(F)I
    .registers 4

    .prologue
    .line 462
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/g;->e:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    .line 463
    mul-float/2addr v0, p1

    const/high16 v1, 0x3f000000    # 0.5f

    add-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/data/g;)Landroid/support/v4/app/bi;
    .registers 2

    .prologue
    .line 51
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/g;->j:Landroid/support/v4/app/bi;

    return-object v0
.end method

.method private static a(Landroid/view/View;)Landroid/view/View;
    .registers 2

    .prologue
    .line 729
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Landroid/view/View;->setVisibility(I)V

    .line 730
    return-object p0
.end method

.method private a(Landroid/view/View;Lcom/teamspeak/ts3client/data/u;)Landroid/view/View;
    .registers 8

    .prologue
    const/high16 v3, 0x41800000    # 16.0f

    const/4 v2, 0x0

    .line 734
    const/high16 v0, -0x3e480000    # -23.0f

    invoke-direct {p0, v0}, Lcom/teamspeak/ts3client/data/g;->a(F)I

    move-result v0

    invoke-virtual {p1, v0, v2, v2, v2}, Landroid/view/View;->setPadding(IIII)V

    .line 735
    invoke-virtual {p1, v2}, Landroid/view/View;->setBackgroundColor(I)V

    .line 736
    iget-object v0, p2, Lcom/teamspeak/ts3client/data/u;->b:Landroid/widget/ImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 737
    iget-object v0, p2, Lcom/teamspeak/ts3client/data/u;->d:Landroid/widget/ImageView;

    const v1, 0x7f0200ae

    invoke-static {v1, v3, v3}, Lcom/teamspeak/ts3client/data/d/t;->a(IFF)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 738
    iget-object v1, p2, Lcom/teamspeak/ts3client/data/u;->c:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/teamspeak/ts3client/data/g;->a:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/data/a;

    .line 50069
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/a;->a:Ljava/lang/String;

    .line 738
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 739
    iget-object v0, p2, Lcom/teamspeak/ts3client/data/u;->c:Landroid/widget/TextView;

    const/16 v1, 0x13

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    .line 741
    :try_start_37
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/g;->f:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50070
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50071
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->v:Lcom/teamspeak/ts3client/data/ab;

    .line 50072
    iget-wide v0, v0, Lcom/teamspeak/ts3client/data/ab;->m:J

    .line 741
    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_7c

    iget-object v0, p0, Lcom/teamspeak/ts3client/data/g;->f:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50073
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50074
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->r:Lcom/teamspeak/ts3client/data/c/d;

    .line 741
    if-eqz v0, :cond_7c

    .line 742
    new-instance v0, Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/teamspeak/ts3client/data/g;->e:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 743
    iget-object v1, p0, Lcom/teamspeak/ts3client/data/g;->f:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50075
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50076
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->r:Lcom/teamspeak/ts3client/data/c/d;

    .line 743
    iget-object v2, p0, Lcom/teamspeak/ts3client/data/g;->f:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50077
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50078
    iget-object v2, v2, Lcom/teamspeak/ts3client/data/e;->v:Lcom/teamspeak/ts3client/data/ab;

    .line 50079
    iget-wide v2, v2, Lcom/teamspeak/ts3client/data/ab;->m:J

    .line 743
    invoke-virtual {v1, v2, v3}, Lcom/teamspeak/ts3client/data/c/d;->a(J)Lcom/teamspeak/ts3client/data/c/c;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/c/c;->a()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 744
    const/16 v1, 0x14

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setMinimumHeight(I)V

    .line 745
    const/16 v1, 0x14

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setMinimumWidth(I)V

    .line 746
    iget-object v1, p2, Lcom/teamspeak/ts3client/data/u;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V
    :try_end_7c
    .catch Ljava/lang/Exception; {:try_start_37 .. :try_end_7c} :catch_85

    .line 751
    :cond_7c
    :goto_7c
    new-instance v0, Lcom/teamspeak/ts3client/data/o;

    invoke-direct {v0, p0}, Lcom/teamspeak/ts3client/data/o;-><init>(Lcom/teamspeak/ts3client/data/g;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 855
    return-object p1

    .line 748
    :catch_85
    move-exception v0

    .line 749
    iget-object v1, p0, Lcom/teamspeak/ts3client/data/g;->f:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50080
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->d:Ljava/util/logging/Logger;

    .line 749
    sget-object v2, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "setServerEntry(): "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    goto :goto_7c
.end method

.method private a()Ljava/util/List;
    .registers 2

    .prologue
    .line 80
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/g;->a:Ljava/util/List;

    return-object v0
.end method

.method private static a(Landroid/widget/TextView;Ljava/lang/String;)V
    .registers 14

    .prologue
    const/4 v2, 0x0

    .line 696
    const-string v0, "."

    const-string v1, "\u00b7"

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    .line 697
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 698
    invoke-virtual {v3}, Ljava/lang/String;->toCharArray()[C

    move-result-object v5

    .line 699
    invoke-virtual {p0}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v0

    .line 700
    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v6

    .line 701
    invoke-virtual {p0}, Landroid/widget/TextView;->getWidth()I

    move-result v0

    int-to-float v7, v0

    .line 703
    const/4 v0, 0x1

    move v1, v2

    :goto_21
    int-to-double v8, v0

    div-float v10, v7, v6

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v11

    int-to-float v11, v11

    mul-float/2addr v10, v11

    const/high16 v11, 0x3f800000    # 1.0f

    add-float/2addr v10, v11

    float-to-double v10, v10

    invoke-static {v10, v11}, Ljava/lang/Math;->floor(D)D

    move-result-wide v10

    cmpg-double v8, v8, v10

    if-gez v8, :cond_44

    .line 704
    aget-char v8, v5, v1

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 705
    add-int/lit8 v1, v1, 0x1

    .line 706
    array-length v8, v5

    if-lt v1, v8, :cond_41

    move v1, v2

    .line 703
    :cond_41
    add-int/lit8 v0, v0, 0x1

    goto :goto_21

    .line 709
    :cond_44
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 710
    return-void
.end method

.method private a(Lcom/teamspeak/ts3client/data/customExpandableListView;)V
    .registers 2

    .prologue
    .line 725
    iput-object p1, p0, Lcom/teamspeak/ts3client/data/g;->b:Lcom/teamspeak/ts3client/data/customExpandableListView;

    .line 726
    return-void
.end method

.method private a([Lcom/teamspeak/ts3client/data/a;)V
    .registers 10

    .prologue
    const-wide/16 v2, 0x0

    .line 859
    array-length v0, p1

    if-gtz v0, :cond_6

    .line 872
    :goto_5
    return-void

    .line 862
    :cond_6
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/g;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 863
    array-length v1, p1

    const/4 v0, 0x0

    :goto_d
    if-ge v0, v1, :cond_1b

    aget-object v4, p1, v0

    .line 864
    if-eqz v4, :cond_18

    .line 865
    iget-object v5, p0, Lcom/teamspeak/ts3client/data/g;->a:Ljava/util/List;

    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 863
    :cond_18
    add-int/lit8 v0, v0, 0x1

    goto :goto_d

    .line 867
    :cond_1b
    new-instance v0, Lcom/teamspeak/ts3client/data/a;

    const-string v1, "empty"

    move-wide v4, v2

    move-wide v6, v2

    invoke-direct/range {v0 .. v7}, Lcom/teamspeak/ts3client/data/a;-><init>(Ljava/lang/String;JJJ)V

    .line 50081
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/teamspeak/ts3client/data/a;->q:Z

    .line 869
    iget-object v1, p0, Lcom/teamspeak/ts3client/data/g;->a:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_5
.end method

.method static synthetic b(Lcom/teamspeak/ts3client/data/g;)Lcom/teamspeak/ts3client/data/customExpandableListView;
    .registers 2

    .prologue
    .line 51
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/g;->b:Lcom/teamspeak/ts3client/data/customExpandableListView;

    return-object v0
.end method

.method private b()Z
    .registers 2

    .prologue
    .line 875
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/data/g;->c:Z

    return v0
.end method

.method static synthetic c(Lcom/teamspeak/ts3client/data/g;)Lcom/teamspeak/ts3client/Ts3Application;
    .registers 2

    .prologue
    .line 51
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/g;->f:Lcom/teamspeak/ts3client/Ts3Application;

    return-object v0
.end method

.method private c()V
    .registers 2

    .prologue
    .line 892
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/teamspeak/ts3client/data/g;->d:Z

    .line 893
    return-void
.end method

.method static synthetic d(Lcom/teamspeak/ts3client/data/g;)Z
    .registers 2

    .prologue
    .line 51
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/data/g;->c:Z

    return v0
.end method

.method static synthetic e(Lcom/teamspeak/ts3client/data/g;)Ljava/util/List;
    .registers 2

    .prologue
    .line 51
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/g;->a:Ljava/util/List;

    return-object v0
.end method

.method static synthetic f(Lcom/teamspeak/ts3client/data/g;)Landroid/content/Context;
    .registers 2

    .prologue
    .line 51
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/g;->e:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public final a(Z)V
    .registers 3

    .prologue
    .line 880
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/data/g;->d:Z

    if-eqz v0, :cond_18

    iget-boolean v0, p0, Lcom/teamspeak/ts3client/data/g;->c:Z

    if-eqz v0, :cond_18

    .line 881
    if-nez p1, :cond_18

    .line 882
    iput-boolean p1, p0, Lcom/teamspeak/ts3client/data/g;->c:Z

    .line 883
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/teamspeak/ts3client/data/g;->d:Z

    .line 884
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/g;->f:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50083
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50084
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->k:Lcom/teamspeak/ts3client/t;

    .line 884
    invoke-virtual {v0}, Lcom/teamspeak/ts3client/t;->y()V

    .line 887
    :cond_18
    iput-boolean p1, p0, Lcom/teamspeak/ts3client/data/g;->c:Z

    .line 888
    return-void
.end method

.method public final getChild(II)Ljava/lang/Object;
    .registers 5

    .prologue
    .line 87
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/g;->f:Lcom/teamspeak/ts3client/Ts3Application;

    .line 3061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 3364
    iget-object v1, v0, Lcom/teamspeak/ts3client/data/e;->d:Lcom/teamspeak/ts3client/data/d;

    .line 87
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/g;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/data/a;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/a;->k()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/teamspeak/ts3client/data/d;->b(I)Lcom/teamspeak/ts3client/data/c;

    move-result-object v0

    return-object v0
.end method

.method public final getChildId(II)J
    .registers 5

    .prologue
    .line 93
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public final getChildView(IIZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 20

    .prologue
    .line 110
    if-nez p4, :cond_5dc

    .line 111
    iget-object v2, p0, Lcom/teamspeak/ts3client/data/g;->g:Landroid/view/LayoutInflater;

    const v3, 0x7f03003e

    const/4 v4, 0x0

    move-object/from16 v0, p5

    invoke-virtual {v2, v3, v0, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    .line 112
    new-instance v3, Lcom/teamspeak/ts3client/data/t;

    invoke-direct {v3}, Lcom/teamspeak/ts3client/data/t;-><init>()V

    .line 113
    const v2, 0x7f0c0180

    invoke-virtual {v4, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, v3, Lcom/teamspeak/ts3client/data/t;->a:Landroid/widget/ImageView;

    .line 114
    const v2, 0x7f0c0182

    invoke-virtual {v4, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v3, Lcom/teamspeak/ts3client/data/t;->b:Landroid/widget/TextView;

    .line 115
    const v2, 0x7f0c0183

    invoke-virtual {v4, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, v3, Lcom/teamspeak/ts3client/data/t;->c:Landroid/widget/LinearLayout;

    .line 116
    invoke-virtual {v4, v3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 120
    :goto_37
    :try_start_37
    invoke-virtual {v4}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/teamspeak/ts3client/data/t;

    .line 121
    iget-object v3, v2, Lcom/teamspeak/ts3client/data/t;->b:Landroid/widget/TextView;

    iget v5, p0, Lcom/teamspeak/ts3client/data/g;->k:I

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 122
    iget-object v3, v2, Lcom/teamspeak/ts3client/data/t;->c:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->removeAllViews()V
    :try_end_49
    .catch Ljava/lang/Exception; {:try_start_37 .. :try_end_49} :catch_58

    move-object v3, v2

    .line 130
    :goto_4a
    iget-object v2, p0, Lcom/teamspeak/ts3client/data/g;->a:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/teamspeak/ts3client/data/a;

    .line 6143
    iget-boolean v2, v2, Lcom/teamspeak/ts3client/data/a;->o:Z

    .line 130
    if-nez v2, :cond_83

    move-object v2, v4

    .line 457
    :goto_57
    return-object v2

    .line 124
    :catch_58
    move-exception v2

    new-instance v3, Lcom/teamspeak/ts3client/data/t;

    invoke-direct {v3}, Lcom/teamspeak/ts3client/data/t;-><init>()V

    .line 125
    const v2, 0x7f0c0180

    invoke-virtual {v4, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, v3, Lcom/teamspeak/ts3client/data/t;->a:Landroid/widget/ImageView;

    .line 126
    const v2, 0x7f0c0182

    invoke-virtual {v4, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v3, Lcom/teamspeak/ts3client/data/t;->b:Landroid/widget/TextView;

    .line 127
    const v2, 0x7f0c0183

    invoke-virtual {v4, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, v3, Lcom/teamspeak/ts3client/data/t;->c:Landroid/widget/LinearLayout;

    .line 128
    invoke-virtual {v4, v3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    goto :goto_4a

    .line 134
    :cond_83
    :try_start_83
    iget-object v2, p0, Lcom/teamspeak/ts3client/data/g;->f:Lcom/teamspeak/ts3client/Ts3Application;

    .line 7061
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 7364
    iget-object v5, v2, Lcom/teamspeak/ts3client/data/e;->d:Lcom/teamspeak/ts3client/data/d;

    .line 134
    iget-object v2, p0, Lcom/teamspeak/ts3client/data/g;->a:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/teamspeak/ts3client/data/a;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/data/a;->k()Ljava/util/ArrayList;

    move-result-object v2

    move/from16 v0, p2

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v5, v2}, Lcom/teamspeak/ts3client/data/d;->b(I)Lcom/teamspeak/ts3client/data/c;

    move-result-object v5

    .line 135
    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/AbsListView$LayoutParams;

    .line 136
    iget v6, p0, Lcom/teamspeak/ts3client/data/g;->i:I

    iput v6, v2, Landroid/widget/AbsListView$LayoutParams;->height:I

    .line 137
    invoke-virtual {v4, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 138
    iget-object v2, p0, Lcom/teamspeak/ts3client/data/g;->a:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/teamspeak/ts3client/data/a;

    .line 8095
    iget v2, v2, Lcom/teamspeak/ts3client/data/a;->f:I

    .line 138
    add-int/lit8 v2, v2, 0x1

    const/high16 v6, 0x41900000    # 18.0f

    invoke-direct {p0, v6}, Lcom/teamspeak/ts3client/data/g;->a(F)I

    move-result v6

    mul-int/2addr v2, v6

    add-int/lit8 v2, v2, 0x2

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual {v4, v2, v6, v7, v8}, Landroid/view/View;->setPadding(IIII)V

    .line 8211
    iget-object v2, v5, Lcom/teamspeak/ts3client/data/c;->w:Lcom/teamspeak/ts3client/c/a;

    .line 139
    if-eqz v2, :cond_30d

    .line 8418
    iget-boolean v2, v5, Lcom/teamspeak/ts3client/data/c;->s:Z

    .line 140
    if-nez v2, :cond_2c1

    .line 9211
    iget-object v2, v5, Lcom/teamspeak/ts3client/data/c;->w:Lcom/teamspeak/ts3client/c/a;

    .line 10046
    iget v2, v2, Lcom/teamspeak/ts3client/c/a;->d:I

    .line 141
    packed-switch v2, :pswitch_data_5e0

    .line 17211
    :goto_dc
    iget-object v2, v5, Lcom/teamspeak/ts3client/data/c;->w:Lcom/teamspeak/ts3client/c/a;

    .line 178
    if-eqz v2, :cond_e7

    .line 18211
    iget-object v2, v5, Lcom/teamspeak/ts3client/data/c;->w:Lcom/teamspeak/ts3client/c/a;

    .line 19054
    iget v2, v2, Lcom/teamspeak/ts3client/c/a;->e:I

    .line 179
    packed-switch v2, :pswitch_data_5ea

    .line 19418
    :cond_e7
    :goto_e7
    iget-boolean v2, v5, Lcom/teamspeak/ts3client/data/c;->s:Z

    .line 193
    if-eqz v2, :cond_f2

    .line 194
    iget-object v2, v3, Lcom/teamspeak/ts3client/data/t;->b:Landroid/widget/TextView;

    const/high16 v6, -0x10000

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setTextColor(I)V

    .line 20235
    :cond_f2
    iget v2, v5, Lcom/teamspeak/ts3client/data/c;->c:I

    .line 196
    iget-object v6, p0, Lcom/teamspeak/ts3client/data/g;->f:Lcom/teamspeak/ts3client/Ts3Application;

    .line 21061
    iget-object v6, v6, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 21242
    iget v6, v6, Lcom/teamspeak/ts3client/data/e;->h:I

    .line 196
    if-eq v2, v6, :cond_100

    .line 21418
    iget-boolean v2, v5, Lcom/teamspeak/ts3client/data/c;->s:Z

    .line 196
    if-eqz v2, :cond_34b

    .line 197
    :cond_100
    iget-object v2, v3, Lcom/teamspeak/ts3client/data/t;->b:Landroid/widget/TextView;

    const/4 v6, 0x0

    const/4 v7, 0x1

    invoke-virtual {v2, v6, v7}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 22374
    :goto_107
    iget-boolean v2, v5, Lcom/teamspeak/ts3client/data/c;->l:Z

    .line 202
    if-eqz v2, :cond_354

    .line 203
    iget-object v2, v3, Lcom/teamspeak/ts3client/data/t;->a:Landroid/widget/ImageView;

    const v6, 0x7f020083

    const/high16 v7, 0x41800000    # 16.0f

    const/high16 v8, 0x41800000    # 16.0f

    invoke-static {v6, v7, v8}, Lcom/teamspeak/ts3client/data/d/t;->a(IFF)Landroid/graphics/Bitmap;

    move-result-object v6

    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 35382
    :cond_11b
    :goto_11b
    iget v2, v5, Lcom/teamspeak/ts3client/data/c;->A:I

    .line 257
    const/4 v6, 0x1

    if-ne v2, v6, :cond_4ec

    .line 258
    iget-object v2, v3, Lcom/teamspeak/ts3client/data/t;->a:Landroid/widget/ImageView;

    const v6, 0x7f02009f

    const/high16 v7, 0x41800000    # 16.0f

    const/high16 v8, 0x41800000    # 16.0f

    invoke-static {v6, v7, v8}, Lcom/teamspeak/ts3client/data/d/t;->a(IFF)Landroid/graphics/Bitmap;

    move-result-object v6

    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 36275
    :cond_130
    :goto_130
    iget-boolean v2, v5, Lcom/teamspeak/ts3client/data/c;->k:Z

    .line 265
    if-eqz v2, :cond_158

    .line 266
    new-instance v2, Landroid/widget/ImageView;

    iget-object v6, p0, Lcom/teamspeak/ts3client/data/g;->e:Landroid/content/Context;

    invoke-direct {v2, v6}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 267
    const/16 v6, 0x14

    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setMinimumHeight(I)V

    .line 268
    const/16 v6, 0x14

    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setMinimumWidth(I)V

    .line 269
    const v6, 0x7f02005f

    const/high16 v7, 0x41800000    # 16.0f

    const/high16 v8, 0x41800000    # 16.0f

    invoke-static {v6, v7, v8}, Lcom/teamspeak/ts3client/data/d/t;->a(IFF)Landroid/graphics/Bitmap;

    move-result-object v6

    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 270
    iget-object v6, v3, Lcom/teamspeak/ts3client/data/t;->c:Landroid/widget/LinearLayout;

    invoke-virtual {v6, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 37156
    :cond_158
    iget v6, v5, Lcom/teamspeak/ts3client/data/c;->o:I

    .line 272
    iget-object v2, p0, Lcom/teamspeak/ts3client/data/g;->a:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/teamspeak/ts3client/data/a;

    .line 38079
    iget v2, v2, Lcom/teamspeak/ts3client/data/a;->h:I

    .line 272
    if-ge v6, v2, :cond_1c0

    .line 38283
    iget-boolean v2, v5, Lcom/teamspeak/ts3client/data/c;->r:Z

    .line 273
    if-eqz v2, :cond_519

    .line 274
    new-instance v2, Landroid/widget/ImageView;

    iget-object v6, p0, Lcom/teamspeak/ts3client/data/g;->e:Landroid/content/Context;

    invoke-direct {v2, v6}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 275
    const/16 v6, 0x14

    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setMinimumHeight(I)V

    .line 276
    const/16 v6, 0x14

    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setMinimumWidth(I)V

    .line 277
    const v6, 0x7f02005e

    const/high16 v7, 0x41800000    # 16.0f

    const/high16 v8, 0x41800000    # 16.0f

    invoke-static {v6, v7, v8}, Lcom/teamspeak/ts3client/data/d/t;->a(IFF)Landroid/graphics/Bitmap;

    move-result-object v6

    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 278
    iget-object v6, v3, Lcom/teamspeak/ts3client/data/t;->c:Landroid/widget/LinearLayout;

    invoke-virtual {v6, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 39164
    :goto_18e
    iget-object v2, v5, Lcom/teamspeak/ts3client/data/c;->p:Ljava/lang/String;

    .line 286
    const-string v6, "0"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1c0

    .line 39283
    iget-boolean v2, v5, Lcom/teamspeak/ts3client/data/c;->r:Z

    .line 286
    if-nez v2, :cond_1c0

    .line 287
    new-instance v2, Landroid/widget/ImageView;

    iget-object v6, p0, Lcom/teamspeak/ts3client/data/g;->e:Landroid/content/Context;

    invoke-direct {v2, v6}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 288
    const/16 v6, 0x14

    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setMinimumHeight(I)V

    .line 289
    const/16 v6, 0x14

    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setMinimumWidth(I)V

    .line 290
    const v6, 0x7f0200a9

    const/high16 v7, 0x41800000    # 16.0f

    const/high16 v8, 0x41800000    # 16.0f

    invoke-static {v6, v7, v8}, Lcom/teamspeak/ts3client/data/d/t;->a(IFF)Landroid/graphics/Bitmap;

    move-result-object v6

    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 291
    iget-object v6, v3, Lcom/teamspeak/ts3client/data/t;->c:Landroid/widget/LinearLayout;

    invoke-virtual {v6, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 40127
    :cond_1c0
    iget-object v2, v5, Lcom/teamspeak/ts3client/data/c;->u:Ljava/lang/String;

    .line 296
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 297
    iget-object v6, p0, Lcom/teamspeak/ts3client/data/g;->f:Lcom/teamspeak/ts3client/Ts3Application;

    .line 41061
    iget-object v6, v6, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 41161
    iget-object v6, v6, Lcom/teamspeak/ts3client/data/e;->p:Lcom/teamspeak/ts3client/data/a/b;

    .line 297
    int-to-long v8, v2

    invoke-virtual {v6, v8, v9}, Lcom/teamspeak/ts3client/data/a/b;->a(J)Z

    move-result v6

    if-eqz v6, :cond_21a

    iget-object v6, p0, Lcom/teamspeak/ts3client/data/g;->f:Lcom/teamspeak/ts3client/Ts3Application;

    .line 42061
    iget-object v6, v6, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 42161
    iget-object v6, v6, Lcom/teamspeak/ts3client/data/e;->p:Lcom/teamspeak/ts3client/data/a/b;

    .line 297
    int-to-long v8, v2

    invoke-virtual {v6, v8, v9}, Lcom/teamspeak/ts3client/data/a/b;->b(J)Lcom/teamspeak/ts3client/data/a/a;

    move-result-object v6

    .line 43041
    iget-wide v6, v6, Lcom/teamspeak/ts3client/data/a/a;->d:J

    .line 297
    const-wide/16 v8, 0x0

    cmp-long v6, v6, v8

    if-eqz v6, :cond_21a

    .line 298
    new-instance v6, Landroid/widget/ImageView;

    iget-object v7, p0, Lcom/teamspeak/ts3client/data/g;->e:Landroid/content/Context;

    invoke-direct {v6, v7}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 299
    iget-object v7, p0, Lcom/teamspeak/ts3client/data/g;->f:Lcom/teamspeak/ts3client/Ts3Application;

    .line 43061
    iget-object v7, v7, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 43222
    iget-object v7, v7, Lcom/teamspeak/ts3client/data/e;->r:Lcom/teamspeak/ts3client/data/c/d;

    .line 299
    iget-object v8, p0, Lcom/teamspeak/ts3client/data/g;->f:Lcom/teamspeak/ts3client/Ts3Application;

    .line 44061
    iget-object v8, v8, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 44161
    iget-object v8, v8, Lcom/teamspeak/ts3client/data/e;->p:Lcom/teamspeak/ts3client/data/a/b;

    .line 299
    int-to-long v10, v2

    invoke-virtual {v8, v10, v11}, Lcom/teamspeak/ts3client/data/a/b;->b(J)Lcom/teamspeak/ts3client/data/a/a;

    move-result-object v2

    .line 45041
    iget-wide v8, v2, Lcom/teamspeak/ts3client/data/a/a;->d:J

    .line 299
    invoke-virtual {v7, v8, v9}, Lcom/teamspeak/ts3client/data/c/d;->a(J)Lcom/teamspeak/ts3client/data/c/c;

    move-result-object v2

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/data/c/c;->a()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v6, v2}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 300
    const/16 v2, 0x14

    invoke-virtual {v6, v2}, Landroid/widget/ImageView;->setMinimumHeight(I)V

    .line 301
    const/16 v2, 0x14

    invoke-virtual {v6, v2}, Landroid/widget/ImageView;->setMinimumWidth(I)V

    .line 302
    iget-object v2, v3, Lcom/teamspeak/ts3client/data/t;->c:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 45227
    :cond_21a
    iget-object v2, v5, Lcom/teamspeak/ts3client/data/c;->t:Ljava/lang/String;

    .line 306
    const-string v6, ","

    invoke-virtual {v2, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    array-length v7, v6

    const/4 v2, 0x0

    :goto_224
    if-ge v2, v7, :cond_53f

    aget-object v8, v6, v2

    .line 307
    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    .line 308
    iget-object v9, p0, Lcom/teamspeak/ts3client/data/g;->f:Lcom/teamspeak/ts3client/Ts3Application;

    .line 46061
    iget-object v9, v9, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 46271
    iget-object v9, v9, Lcom/teamspeak/ts3client/data/e;->o:Lcom/teamspeak/ts3client/data/g/b;

    .line 308
    int-to-long v10, v8

    invoke-virtual {v9, v10, v11}, Lcom/teamspeak/ts3client/data/g/b;->a(J)Z

    move-result v9

    if-eqz v9, :cond_280

    iget-object v9, p0, Lcom/teamspeak/ts3client/data/g;->f:Lcom/teamspeak/ts3client/Ts3Application;

    .line 47061
    iget-object v9, v9, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 47271
    iget-object v9, v9, Lcom/teamspeak/ts3client/data/e;->o:Lcom/teamspeak/ts3client/data/g/b;

    .line 308
    int-to-long v10, v8

    invoke-virtual {v9, v10, v11}, Lcom/teamspeak/ts3client/data/g/b;->b(J)Lcom/teamspeak/ts3client/data/g/a;

    move-result-object v9

    .line 48034
    iget-wide v10, v9, Lcom/teamspeak/ts3client/data/g/a;->d:J

    .line 308
    const-wide/16 v12, 0x0

    cmp-long v9, v10, v12

    if-eqz v9, :cond_280

    .line 309
    new-instance v9, Landroid/widget/ImageView;

    iget-object v10, p0, Lcom/teamspeak/ts3client/data/g;->e:Landroid/content/Context;

    invoke-direct {v9, v10}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 310
    const/16 v10, 0x14

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setMinimumHeight(I)V

    .line 311
    const/16 v10, 0x14

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setMinimumWidth(I)V

    .line 312
    iget-object v10, p0, Lcom/teamspeak/ts3client/data/g;->f:Lcom/teamspeak/ts3client/Ts3Application;

    .line 48061
    iget-object v10, v10, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 48222
    iget-object v10, v10, Lcom/teamspeak/ts3client/data/e;->r:Lcom/teamspeak/ts3client/data/c/d;

    .line 312
    iget-object v11, p0, Lcom/teamspeak/ts3client/data/g;->f:Lcom/teamspeak/ts3client/Ts3Application;

    .line 49061
    iget-object v11, v11, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 49271
    iget-object v11, v11, Lcom/teamspeak/ts3client/data/e;->o:Lcom/teamspeak/ts3client/data/g/b;

    .line 312
    int-to-long v12, v8

    invoke-virtual {v11, v12, v13}, Lcom/teamspeak/ts3client/data/g/b;->b(J)Lcom/teamspeak/ts3client/data/g/a;

    move-result-object v8

    .line 50034
    iget-wide v12, v8, Lcom/teamspeak/ts3client/data/g/a;->d:J

    .line 312
    invoke-virtual {v10, v12, v13}, Lcom/teamspeak/ts3client/data/c/d;->a(J)Lcom/teamspeak/ts3client/data/c/c;

    move-result-object v8

    invoke-virtual {v8}, Lcom/teamspeak/ts3client/data/c/c;->a()Landroid/graphics/drawable/Drawable;

    move-result-object v8

    invoke-virtual {v9, v8}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 313
    iget-object v8, v3, Lcom/teamspeak/ts3client/data/t;->c:Landroid/widget/LinearLayout;

    invoke-virtual {v8, v9}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 306
    :cond_280
    add-int/lit8 v2, v2, 0x1

    goto :goto_224

    .line 143
    :pswitch_283
    iget-object v2, v3, Lcom/teamspeak/ts3client/data/t;->b:Landroid/widget/TextView;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "["

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 10211
    iget-object v7, v5, Lcom/teamspeak/ts3client/data/c;->w:Lcom/teamspeak/ts3client/c/a;

    .line 11030
    iget-object v7, v7, Lcom/teamspeak/ts3client/c/a;->a:Ljava/lang/String;

    .line 143
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "] "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 11203
    iget-object v7, v5, Lcom/teamspeak/ts3client/data/c;->a:Ljava/lang/String;

    .line 143
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_dc

    :catch_2a9
    move-exception v2

    :goto_2aa
    move-object v2, v4

    .line 457
    goto/16 :goto_57

    .line 146
    :pswitch_2ad
    iget-object v2, v3, Lcom/teamspeak/ts3client/data/t;->b:Landroid/widget/TextView;

    .line 11211
    iget-object v6, v5, Lcom/teamspeak/ts3client/data/c;->w:Lcom/teamspeak/ts3client/c/a;

    .line 12030
    iget-object v6, v6, Lcom/teamspeak/ts3client/c/a;->a:Ljava/lang/String;

    .line 146
    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_dc

    .line 149
    :pswitch_2b8
    iget-object v2, v3, Lcom/teamspeak/ts3client/data/t;->b:Landroid/widget/TextView;

    .line 12203
    iget-object v6, v5, Lcom/teamspeak/ts3client/data/c;->a:Ljava/lang/String;

    .line 149
    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_dc

    .line 12211
    :cond_2c1
    iget-object v2, v5, Lcom/teamspeak/ts3client/data/c;->w:Lcom/teamspeak/ts3client/c/a;

    .line 13046
    iget v2, v2, Lcom/teamspeak/ts3client/c/a;->d:I

    .line 156
    packed-switch v2, :pswitch_data_5f4

    goto/16 :goto_dc

    .line 158
    :pswitch_2ca
    iget-object v2, v3, Lcom/teamspeak/ts3client/data/t;->b:Landroid/widget/TextView;

    const-string v6, "event.client.recording.record"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    .line 13211
    iget-object v9, v5, Lcom/teamspeak/ts3client/data/c;->w:Lcom/teamspeak/ts3client/c/a;

    .line 14030
    iget-object v9, v9, Lcom/teamspeak/ts3client/c/a;->a:Ljava/lang/String;

    .line 158
    aput-object v9, v7, v8

    invoke-static {v6, v7}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_dc

    .line 161
    :pswitch_2e1
    iget-object v2, v3, Lcom/teamspeak/ts3client/data/t;->b:Landroid/widget/TextView;

    const-string v6, "event.client.recording.record"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    .line 14211
    iget-object v9, v5, Lcom/teamspeak/ts3client/data/c;->w:Lcom/teamspeak/ts3client/c/a;

    .line 15030
    iget-object v9, v9, Lcom/teamspeak/ts3client/c/a;->a:Ljava/lang/String;

    .line 161
    aput-object v9, v7, v8

    invoke-static {v6, v7}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_dc

    .line 164
    :pswitch_2f8
    iget-object v2, v3, Lcom/teamspeak/ts3client/data/t;->b:Landroid/widget/TextView;

    const-string v6, "event.client.recording.record"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    .line 15203
    iget-object v9, v5, Lcom/teamspeak/ts3client/data/c;->a:Ljava/lang/String;

    .line 164
    aput-object v9, v7, v8

    invoke-static {v6, v7}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_dc

    .line 15418
    :cond_30d
    iget-boolean v2, v5, Lcom/teamspeak/ts3client/data/c;->s:Z

    .line 173
    if-eqz v2, :cond_326

    .line 174
    iget-object v2, v3, Lcom/teamspeak/ts3client/data/t;->b:Landroid/widget/TextView;

    const-string v6, "event.client.recording.record"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    .line 16203
    iget-object v9, v5, Lcom/teamspeak/ts3client/data/c;->a:Ljava/lang/String;

    .line 174
    aput-object v9, v7, v8

    invoke-static {v6, v7}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_dc

    .line 176
    :cond_326
    iget-object v2, v3, Lcom/teamspeak/ts3client/data/t;->b:Landroid/widget/TextView;

    .line 17203
    iget-object v6, v5, Lcom/teamspeak/ts3client/data/c;->a:Ljava/lang/String;

    .line 176
    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_dc

    .line 181
    :pswitch_32f
    iget-object v2, v3, Lcom/teamspeak/ts3client/data/t;->b:Landroid/widget/TextView;

    iget v6, p0, Lcom/teamspeak/ts3client/data/g;->k:I

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_e7

    .line 184
    :pswitch_338
    iget-object v2, v3, Lcom/teamspeak/ts3client/data/t;->b:Landroid/widget/TextView;

    const/high16 v6, -0x10000

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_e7

    .line 187
    :pswitch_341
    iget-object v2, v3, Lcom/teamspeak/ts3client/data/t;->b:Landroid/widget/TextView;

    const v6, -0xff0100

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_e7

    .line 199
    :cond_34b
    iget-object v2, v3, Lcom/teamspeak/ts3client/data/t;->b:Landroid/widget/TextView;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v2, v6, v7}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    goto/16 :goto_107

    .line 23243
    :cond_354
    iget-boolean v2, v5, Lcom/teamspeak/ts3client/data/c;->j:Z

    .line 204
    if-eqz v2, :cond_46c

    .line 205
    iget-object v2, v3, Lcom/teamspeak/ts3client/data/t;->a:Landroid/widget/ImageView;

    const v6, 0x7f02009a

    const/high16 v7, 0x41800000    # 16.0f

    const/high16 v8, 0x41800000    # 16.0f

    invoke-static {v6, v7, v8}, Lcom/teamspeak/ts3client/data/d/t;->a(IFF)Landroid/graphics/Bitmap;

    move-result-object v6

    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 24135
    iget-object v2, v5, Lcom/teamspeak/ts3client/data/c;->m:Ljava/lang/String;

    .line 206
    const-string v6, ""

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_11b

    .line 24211
    iget-object v2, v5, Lcom/teamspeak/ts3client/data/c;->w:Lcom/teamspeak/ts3client/c/a;

    .line 207
    if-eqz v2, :cond_444

    .line 25211
    iget-object v2, v5, Lcom/teamspeak/ts3client/data/c;->w:Lcom/teamspeak/ts3client/c/a;

    .line 26070
    iget-boolean v2, v2, Lcom/teamspeak/ts3client/c/a;->j:Z

    .line 208
    if-eqz v2, :cond_3bf

    .line 26211
    iget-object v2, v5, Lcom/teamspeak/ts3client/data/c;->w:Lcom/teamspeak/ts3client/c/a;

    .line 27046
    iget v2, v2, Lcom/teamspeak/ts3client/c/a;->d:I

    .line 209
    packed-switch v2, :pswitch_data_5fe

    goto/16 :goto_11b

    .line 211
    :pswitch_385
    iget-object v2, v3, Lcom/teamspeak/ts3client/data/t;->b:Landroid/widget/TextView;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "["

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 27211
    iget-object v7, v5, Lcom/teamspeak/ts3client/data/c;->w:Lcom/teamspeak/ts3client/c/a;

    .line 28030
    iget-object v7, v7, Lcom/teamspeak/ts3client/c/a;->a:Ljava/lang/String;

    .line 211
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "] "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 28203
    iget-object v7, v5, Lcom/teamspeak/ts3client/data/c;->a:Ljava/lang/String;

    .line 211
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_11b

    .line 214
    :pswitch_3ab
    iget-object v2, v3, Lcom/teamspeak/ts3client/data/t;->b:Landroid/widget/TextView;

    .line 28211
    iget-object v6, v5, Lcom/teamspeak/ts3client/data/c;->w:Lcom/teamspeak/ts3client/c/a;

    .line 29030
    iget-object v6, v6, Lcom/teamspeak/ts3client/c/a;->a:Ljava/lang/String;

    .line 214
    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_11b

    .line 217
    :pswitch_3b6
    iget-object v2, v3, Lcom/teamspeak/ts3client/data/t;->b:Landroid/widget/TextView;

    .line 29203
    iget-object v6, v5, Lcom/teamspeak/ts3client/data/c;->a:Ljava/lang/String;

    .line 217
    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_11b

    .line 29211
    :cond_3bf
    iget-object v2, v5, Lcom/teamspeak/ts3client/data/c;->w:Lcom/teamspeak/ts3client/c/a;

    .line 30046
    iget v2, v2, Lcom/teamspeak/ts3client/c/a;->d:I

    .line 224
    packed-switch v2, :pswitch_data_608

    goto/16 :goto_11b

    .line 226
    :pswitch_3c8
    iget-object v2, v3, Lcom/teamspeak/ts3client/data/t;->b:Landroid/widget/TextView;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 30211
    iget-object v7, v5, Lcom/teamspeak/ts3client/data/c;->w:Lcom/teamspeak/ts3client/c/a;

    .line 31030
    iget-object v7, v7, Lcom/teamspeak/ts3client/c/a;->a:Ljava/lang/String;

    .line 226
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " ["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 31135
    iget-object v7, v5, Lcom/teamspeak/ts3client/data/c;->m:Ljava/lang/String;

    .line 226
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_11b

    .line 229
    :pswitch_3f2
    iget-object v2, v3, Lcom/teamspeak/ts3client/data/t;->b:Landroid/widget/TextView;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 31211
    iget-object v7, v5, Lcom/teamspeak/ts3client/data/c;->w:Lcom/teamspeak/ts3client/c/a;

    .line 32030
    iget-object v7, v7, Lcom/teamspeak/ts3client/c/a;->a:Ljava/lang/String;

    .line 229
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " ["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 32135
    iget-object v7, v5, Lcom/teamspeak/ts3client/data/c;->m:Ljava/lang/String;

    .line 229
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_11b

    .line 232
    :pswitch_41c
    iget-object v2, v3, Lcom/teamspeak/ts3client/data/t;->b:Landroid/widget/TextView;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 32203
    iget-object v7, v5, Lcom/teamspeak/ts3client/data/c;->a:Ljava/lang/String;

    .line 232
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " ["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 33135
    iget-object v7, v5, Lcom/teamspeak/ts3client/data/c;->m:Ljava/lang/String;

    .line 232
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_11b

    .line 240
    :cond_444
    iget-object v2, v3, Lcom/teamspeak/ts3client/data/t;->b:Landroid/widget/TextView;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 33203
    iget-object v7, v5, Lcom/teamspeak/ts3client/data/c;->a:Ljava/lang/String;

    .line 240
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " ["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 34135
    iget-object v7, v5, Lcom/teamspeak/ts3client/data/c;->m:Ljava/lang/String;

    .line 240
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_11b

    .line 34291
    :cond_46c
    iget-boolean v2, v5, Lcom/teamspeak/ts3client/data/c;->i:Z

    .line 242
    if-eqz v2, :cond_482

    .line 243
    iget-object v2, v3, Lcom/teamspeak/ts3client/data/t;->a:Landroid/widget/ImageView;

    const v6, 0x7f020074

    const/high16 v7, 0x41800000    # 16.0f

    const/high16 v8, 0x41800000    # 16.0f

    invoke-static {v6, v7, v8}, Lcom/teamspeak/ts3client/data/d/t;->a(IFF)Landroid/graphics/Bitmap;

    move-result-object v6

    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto/16 :goto_11b

    .line 34299
    :cond_482
    iget-boolean v2, v5, Lcom/teamspeak/ts3client/data/c;->g:Z

    .line 244
    if-eqz v2, :cond_498

    .line 245
    iget-object v2, v3, Lcom/teamspeak/ts3client/data/t;->a:Landroid/widget/ImageView;

    const v6, 0x7f020097

    const/high16 v7, 0x41800000    # 16.0f

    const/high16 v8, 0x41800000    # 16.0f

    invoke-static {v6, v7, v8}, Lcom/teamspeak/ts3client/data/d/t;->a(IFF)Landroid/graphics/Bitmap;

    move-result-object v6

    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto/16 :goto_11b

    .line 35251
    :cond_498
    iget-boolean v2, v5, Lcom/teamspeak/ts3client/data/c;->h:Z

    .line 246
    if-eqz v2, :cond_4ae

    .line 247
    iget-object v2, v3, Lcom/teamspeak/ts3client/data/t;->a:Landroid/widget/ImageView;

    const v6, 0x7f020073

    const/high16 v7, 0x41800000    # 16.0f

    const/high16 v8, 0x41800000    # 16.0f

    invoke-static {v6, v7, v8}, Lcom/teamspeak/ts3client/data/d/t;->a(IFF)Landroid/graphics/Bitmap;

    move-result-object v6

    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto/16 :goto_11b

    .line 35259
    :cond_4ae
    iget-boolean v2, v5, Lcom/teamspeak/ts3client/data/c;->f:Z

    .line 248
    if-eqz v2, :cond_4c4

    .line 249
    iget-object v2, v3, Lcom/teamspeak/ts3client/data/t;->a:Landroid/widget/ImageView;

    const v6, 0x7f020082

    const/high16 v7, 0x41800000    # 16.0f

    const/high16 v8, 0x41800000    # 16.0f

    invoke-static {v6, v7, v8}, Lcom/teamspeak/ts3client/data/d/t;->a(IFF)Landroid/graphics/Bitmap;

    move-result-object v6

    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto/16 :goto_11b

    .line 35267
    :cond_4c4
    iget-boolean v2, v5, Lcom/teamspeak/ts3client/data/c;->n:Z

    .line 252
    if-nez v2, :cond_4da

    .line 253
    iget-object v2, v3, Lcom/teamspeak/ts3client/data/t;->a:Landroid/widget/ImageView;

    const v6, 0x7f02009d

    const/high16 v7, 0x41800000    # 16.0f

    const/high16 v8, 0x41800000    # 16.0f

    invoke-static {v6, v7, v8}, Lcom/teamspeak/ts3client/data/d/t;->a(IFF)Landroid/graphics/Bitmap;

    move-result-object v6

    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto/16 :goto_11b

    .line 255
    :cond_4da
    iget-object v2, v3, Lcom/teamspeak/ts3client/data/t;->a:Landroid/widget/ImageView;

    const v6, 0x7f02009b

    const/high16 v7, 0x41800000    # 16.0f

    const/high16 v8, 0x41800000    # 16.0f

    invoke-static {v6, v7, v8}, Lcom/teamspeak/ts3client/data/d/t;->a(IFF)Landroid/graphics/Bitmap;

    move-result-object v6

    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto/16 :goto_11b

    .line 36219
    :cond_4ec
    iget v2, v5, Lcom/teamspeak/ts3client/data/c;->d:I

    .line 259
    const/4 v6, 0x1

    if-ne v2, v6, :cond_130

    .line 36267
    iget-boolean v2, v5, Lcom/teamspeak/ts3client/data/c;->n:Z

    .line 260
    if-nez v2, :cond_507

    .line 261
    iget-object v2, v3, Lcom/teamspeak/ts3client/data/t;->a:Landroid/widget/ImageView;

    const v6, 0x7f02009e

    const/high16 v7, 0x41800000    # 16.0f

    const/high16 v8, 0x41800000    # 16.0f

    invoke-static {v6, v7, v8}, Lcom/teamspeak/ts3client/data/d/t;->a(IFF)Landroid/graphics/Bitmap;

    move-result-object v6

    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto/16 :goto_130

    .line 263
    :cond_507
    iget-object v2, v3, Lcom/teamspeak/ts3client/data/t;->a:Landroid/widget/ImageView;

    const v6, 0x7f02009c

    const/high16 v7, 0x41800000    # 16.0f

    const/high16 v8, 0x41800000    # 16.0f

    invoke-static {v6, v7, v8}, Lcom/teamspeak/ts3client/data/d/t;->a(IFF)Landroid/graphics/Bitmap;

    move-result-object v6

    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto/16 :goto_130

    .line 280
    :cond_519
    new-instance v2, Landroid/widget/ImageView;

    iget-object v6, p0, Lcom/teamspeak/ts3client/data/g;->e:Landroid/content/Context;

    invoke-direct {v2, v6}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 281
    const/16 v6, 0x14

    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setMinimumHeight(I)V

    .line 282
    const/16 v6, 0x14

    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setMinimumWidth(I)V

    .line 283
    const v6, 0x7f020082

    const/high16 v7, 0x41800000    # 16.0f

    const/high16 v8, 0x41800000    # 16.0f

    invoke-static {v6, v7, v8}, Lcom/teamspeak/ts3client/data/d/t;->a(IFF)Landroid/graphics/Bitmap;

    move-result-object v6

    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 284
    iget-object v6, v3, Lcom/teamspeak/ts3client/data/t;->c:Landroid/widget/LinearLayout;

    invoke-virtual {v6, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto/16 :goto_18e

    .line 50035
    :cond_53f
    iget-wide v6, v5, Lcom/teamspeak/ts3client/data/c;->z:J

    .line 317
    const-wide/16 v8, 0x0

    cmp-long v2, v6, v8

    if-eqz v2, :cond_570

    .line 318
    new-instance v2, Landroid/widget/ImageView;

    iget-object v6, p0, Lcom/teamspeak/ts3client/data/g;->e:Landroid/content/Context;

    invoke-direct {v2, v6}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 319
    iget-object v6, p0, Lcom/teamspeak/ts3client/data/g;->f:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50036
    iget-object v6, v6, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50037
    iget-object v6, v6, Lcom/teamspeak/ts3client/data/e;->r:Lcom/teamspeak/ts3client/data/c/d;

    .line 50038
    iget-wide v8, v5, Lcom/teamspeak/ts3client/data/c;->z:J

    .line 319
    invoke-virtual {v6, v8, v9}, Lcom/teamspeak/ts3client/data/c/d;->a(J)Lcom/teamspeak/ts3client/data/c/c;

    move-result-object v6

    invoke-virtual {v6}, Lcom/teamspeak/ts3client/data/c/c;->a()Landroid/graphics/drawable/Drawable;

    move-result-object v6

    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 320
    const/16 v6, 0x14

    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setMinimumHeight(I)V

    .line 321
    const/16 v6, 0x14

    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setMinimumWidth(I)V

    .line 322
    iget-object v6, v3, Lcom/teamspeak/ts3client/data/t;->c:Landroid/widget/LinearLayout;

    invoke-virtual {v6, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 325
    :cond_570
    iget-object v2, p0, Lcom/teamspeak/ts3client/data/g;->f:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50039
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 325
    const-string v6, "show_flags"

    const/4 v7, 0x0

    invoke-interface {v2, v6, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_5b3

    .line 326
    iget-object v2, p0, Lcom/teamspeak/ts3client/data/g;->f:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/Ts3Application;->b()Lcom/teamspeak/ts3client/data/d/f;

    move-result-object v2

    .line 50040
    iget-object v6, v5, Lcom/teamspeak/ts3client/data/c;->x:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v6

    .line 326
    invoke-virtual {v2, v6}, Lcom/teamspeak/ts3client/data/d/f;->a(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 327
    if-eqz v2, :cond_5b3

    .line 328
    new-instance v6, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v7, p0, Lcom/teamspeak/ts3client/data/g;->f:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v7}, Lcom/teamspeak/ts3client/Ts3Application;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-direct {v6, v7, v2}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 329
    new-instance v2, Landroid/widget/ImageView;

    iget-object v7, p0, Lcom/teamspeak/ts3client/data/g;->e:Landroid/content/Context;

    invoke-direct {v2, v7}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 330
    const/16 v7, 0x14

    invoke-virtual {v2, v7}, Landroid/widget/ImageView;->setMinimumHeight(I)V

    .line 331
    const/16 v7, 0x14

    invoke-virtual {v2, v7}, Landroid/widget/ImageView;->setMinimumWidth(I)V

    .line 332
    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 333
    iget-object v3, v3, Lcom/teamspeak/ts3client/data/t;->c:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 337
    :cond_5b3
    iget-object v2, p0, Lcom/teamspeak/ts3client/data/g;->f:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50041
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 337
    const-string v3, "longclick_client"

    const/4 v6, 0x1

    invoke-interface {v2, v3, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_5ca

    .line 338
    new-instance v2, Lcom/teamspeak/ts3client/data/h;

    invoke-direct {v2, p0, v5}, Lcom/teamspeak/ts3client/data/h;-><init>(Lcom/teamspeak/ts3client/data/g;Lcom/teamspeak/ts3client/data/c;)V

    .line 391
    invoke-virtual {v4, v2}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    goto/16 :goto_2aa

    .line 393
    :cond_5ca
    new-instance v2, Lcom/teamspeak/ts3client/data/i;

    invoke-direct {v2, p0}, Lcom/teamspeak/ts3client/data/i;-><init>(Lcom/teamspeak/ts3client/data/g;)V

    invoke-virtual {v4, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 399
    new-instance v2, Lcom/teamspeak/ts3client/data/j;

    invoke-direct {v2, p0, v5}, Lcom/teamspeak/ts3client/data/j;-><init>(Lcom/teamspeak/ts3client/data/g;Lcom/teamspeak/ts3client/data/c;)V

    invoke-virtual {v4, v2}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V
    :try_end_5da
    .catch Ljava/lang/Exception; {:try_start_83 .. :try_end_5da} :catch_2a9

    goto/16 :goto_2aa

    :cond_5dc
    move-object/from16 v4, p4

    goto/16 :goto_37

    .line 141
    :pswitch_data_5e0
    .packed-switch 0x0
        :pswitch_283
        :pswitch_2ad
        :pswitch_2b8
    .end packed-switch

    .line 179
    :pswitch_data_5ea
    .packed-switch 0x0
        :pswitch_32f
        :pswitch_338
        :pswitch_341
    .end packed-switch

    .line 156
    :pswitch_data_5f4
    .packed-switch 0x0
        :pswitch_2ca
        :pswitch_2e1
        :pswitch_2f8
    .end packed-switch

    .line 209
    :pswitch_data_5fe
    .packed-switch 0x0
        :pswitch_385
        :pswitch_3ab
        :pswitch_3b6
    .end packed-switch

    .line 224
    :pswitch_data_608
    .packed-switch 0x0
        :pswitch_3c8
        :pswitch_3f2
        :pswitch_41c
    .end packed-switch
.end method

.method public final getChildrenCount(I)I
    .registers 4

    .prologue
    const/4 v1, 0x0

    .line 99
    if-nez p1, :cond_5

    move v0, v1

    .line 105
    :goto_4
    return v0

    .line 101
    :cond_5
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/g;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_f

    move v0, v1

    .line 102
    goto :goto_4

    .line 103
    :cond_f
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/g;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/data/a;

    .line 4151
    iget-boolean v0, v0, Lcom/teamspeak/ts3client/data/a;->p:Z

    .line 103
    if-nez v0, :cond_27

    iget-object v0, p0, Lcom/teamspeak/ts3client/data/g;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/data/a;

    .line 5143
    iget-boolean v0, v0, Lcom/teamspeak/ts3client/data/a;->o:Z

    .line 103
    if-nez v0, :cond_29

    :cond_27
    move v0, v1

    .line 104
    goto :goto_4

    .line 105
    :cond_29
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/g;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/data/a;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/a;->k()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_4
.end method

.method public final getGroup(I)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 469
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/g;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getGroupCount()I
    .registers 2

    .prologue
    .line 474
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/g;->a:Ljava/util/List;

    if-nez v0, :cond_6

    .line 475
    const/4 v0, 0x0

    .line 476
    :goto_5
    return v0

    :cond_6
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/g;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_5
.end method

.method public final getGroupId(I)J
    .registers 4

    .prologue
    .line 482
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getGroupView(IZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 13

    .prologue
    const/4 v7, 0x1

    const/4 v5, 0x0

    const/16 v6, 0x14

    const/4 v4, 0x2

    const/high16 v3, 0x41800000    # 16.0f

    .line 487
    if-nez p3, :cond_46

    .line 488
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/g;->g:Landroid/view/LayoutInflater;

    const v1, 0x7f03003d

    invoke-virtual {v0, v1, p4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p3

    .line 489
    new-instance v1, Lcom/teamspeak/ts3client/data/u;

    invoke-direct {v1}, Lcom/teamspeak/ts3client/data/u;-><init>()V

    .line 490
    const v0, 0x7f0c017f

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, v1, Lcom/teamspeak/ts3client/data/u;->a:Landroid/widget/LinearLayout;

    .line 491
    const v0, 0x7f0c017b

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lcom/teamspeak/ts3client/data/u;->b:Landroid/widget/ImageView;

    .line 492
    const v0, 0x7f0c017e

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/teamspeak/ts3client/data/u;->c:Landroid/widget/TextView;

    .line 493
    const v0, 0x7f0c017c

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lcom/teamspeak/ts3client/data/u;->d:Landroid/widget/ImageView;

    .line 494
    invoke-virtual {p3, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 496
    :cond_46
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/g;->a:Ljava/util/List;

    if-nez v0, :cond_4b

    .line 692
    :cond_4a
    :goto_4a
    return-object p3

    .line 498
    :cond_4b
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/g;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/data/a;

    .line 50042
    iget-boolean v0, v0, Lcom/teamspeak/ts3client/data/a;->p:Z

    .line 498
    if-nez v0, :cond_4a

    .line 501
    invoke-virtual {p3}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/data/u;

    .line 502
    invoke-virtual {p3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/AbsListView$LayoutParams;

    .line 503
    iget v2, p0, Lcom/teamspeak/ts3client/data/g;->h:I

    iput v2, v1, Landroid/widget/AbsListView$LayoutParams;->height:I

    .line 504
    invoke-virtual {p3, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 506
    iget-object v1, v0, Lcom/teamspeak/ts3client/data/u;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 507
    iget-object v1, p0, Lcom/teamspeak/ts3client/data/g;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ne p1, v1, :cond_7e

    .line 50043
    const/4 v0, 0x4

    invoke-virtual {p3, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_4a

    .line 510
    :cond_7e
    invoke-virtual {p3}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-eqz v1, :cond_87

    .line 511
    invoke-virtual {p3, v5}, Landroid/view/View;->setVisibility(I)V

    .line 513
    :cond_87
    if-nez p1, :cond_8e

    .line 514
    invoke-direct {p0, p3, v0}, Lcom/teamspeak/ts3client/data/g;->a(Landroid/view/View;Lcom/teamspeak/ts3client/data/u;)Landroid/view/View;

    move-result-object p3

    goto :goto_4a

    .line 516
    :cond_8e
    iget-object v1, p0, Lcom/teamspeak/ts3client/data/g;->a:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/teamspeak/ts3client/data/a;

    .line 50045
    iget v1, v1, Lcom/teamspeak/ts3client/data/a;->f:I

    .line 516
    const/high16 v2, 0x41900000    # 18.0f

    invoke-direct {p0, v2}, Lcom/teamspeak/ts3client/data/g;->a(F)I

    move-result v2

    mul-int/2addr v1, v2

    invoke-virtual {p3, v1, v5, v5, v5}, Landroid/view/View;->setPadding(IIII)V

    .line 518
    iget-object v1, p0, Lcom/teamspeak/ts3client/data/g;->a:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/teamspeak/ts3client/data/a;

    .line 50046
    iget-boolean v1, v1, Lcom/teamspeak/ts3client/data/a;->o:Z

    .line 518
    if-eqz v1, :cond_230

    .line 519
    iget-object v1, v0, Lcom/teamspeak/ts3client/data/u;->b:Landroid/widget/ImageView;

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 520
    iget-object v1, v0, Lcom/teamspeak/ts3client/data/u;->b:Landroid/widget/ImageView;

    const v2, 0x7f02006b

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 525
    :goto_bb
    iget-object v1, p0, Lcom/teamspeak/ts3client/data/g;->a:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/teamspeak/ts3client/data/a;

    .line 50047
    iget-boolean v1, v1, Lcom/teamspeak/ts3client/data/a;->q:Z

    .line 525
    if-eqz v1, :cond_23f

    .line 526
    iget-object v1, v0, Lcom/teamspeak/ts3client/data/u;->b:Landroid/widget/ImageView;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 531
    :goto_cd
    iget-object v2, p0, Lcom/teamspeak/ts3client/data/g;->l:Ljava/util/regex/Pattern;

    iget-object v1, p0, Lcom/teamspeak/ts3client/data/g;->a:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/teamspeak/ts3client/data/a;

    .line 50048
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/a;->a:Ljava/lang/String;

    .line 531
    invoke-virtual {v2, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    .line 532
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->find()Z

    move-result v1

    .line 533
    if-eqz v1, :cond_ef

    iget-object v1, p0, Lcom/teamspeak/ts3client/data/g;->a:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/teamspeak/ts3client/data/a;

    .line 50049
    iget v1, v1, Lcom/teamspeak/ts3client/data/a;->f:I

    .line 533
    if-eqz v1, :cond_2ff

    .line 534
    :cond_ef
    iget-object v1, v0, Lcom/teamspeak/ts3client/data/u;->d:Landroid/widget/ImageView;

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 535
    iget-object v1, p0, Lcom/teamspeak/ts3client/data/g;->a:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/teamspeak/ts3client/data/a;

    .line 50050
    iget-boolean v1, v1, Lcom/teamspeak/ts3client/data/a;->j:Z

    .line 535
    if-eqz v1, :cond_246

    .line 536
    iget-object v1, v0, Lcom/teamspeak/ts3client/data/u;->d:Landroid/widget/ImageView;

    const v2, 0x7f020056

    invoke-static {v2, v3, v3}, Lcom/teamspeak/ts3client/data/d/t;->a(IFF)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 551
    :goto_10c
    iget-object v2, v0, Lcom/teamspeak/ts3client/data/u;->c:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/teamspeak/ts3client/data/g;->a:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/teamspeak/ts3client/data/a;

    .line 50058
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/a;->a:Ljava/lang/String;

    .line 551
    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 552
    iget-object v1, v0, Lcom/teamspeak/ts3client/data/u;->c:Landroid/widget/TextView;

    const/16 v2, 0x13

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setGravity(I)V

    .line 554
    iget-object v1, p0, Lcom/teamspeak/ts3client/data/g;->a:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/teamspeak/ts3client/data/a;

    .line 50059
    iget-boolean v1, v1, Lcom/teamspeak/ts3client/data/a;->r:Z

    .line 554
    if-eqz v1, :cond_14a

    .line 555
    new-instance v1, Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/teamspeak/ts3client/data/g;->e:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 556
    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setMinimumHeight(I)V

    .line 557
    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setMinimumWidth(I)V

    .line 558
    const v2, 0x7f020053

    invoke-static {v2, v3, v3}, Lcom/teamspeak/ts3client/data/d/t;->a(IFF)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 559
    iget-object v2, v0, Lcom/teamspeak/ts3client/data/u;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 561
    :cond_14a
    iget-object v1, p0, Lcom/teamspeak/ts3client/data/g;->a:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/teamspeak/ts3client/data/a;

    .line 50060
    iget v1, v1, Lcom/teamspeak/ts3client/data/a;->l:I

    .line 561
    const/4 v2, 0x3

    if-eq v1, v2, :cond_164

    iget-object v1, p0, Lcom/teamspeak/ts3client/data/g;->a:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/teamspeak/ts3client/data/a;

    .line 50061
    iget v1, v1, Lcom/teamspeak/ts3client/data/a;->l:I

    .line 561
    const/4 v2, 0x5

    if-ne v1, v2, :cond_180

    .line 562
    :cond_164
    new-instance v1, Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/teamspeak/ts3client/data/g;->e:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 563
    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setMinimumHeight(I)V

    .line 564
    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setMinimumWidth(I)V

    .line 565
    const v2, 0x7f020095

    invoke-static {v2, v3, v3}, Lcom/teamspeak/ts3client/data/d/t;->a(IFF)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 566
    iget-object v2, v0, Lcom/teamspeak/ts3client/data/u;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 568
    :cond_180
    iget-object v1, p0, Lcom/teamspeak/ts3client/data/g;->a:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/teamspeak/ts3client/data/a;

    .line 50062
    iget v1, v1, Lcom/teamspeak/ts3client/data/a;->h:I

    .line 568
    if-eqz v1, :cond_1a8

    .line 569
    new-instance v1, Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/teamspeak/ts3client/data/g;->e:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 570
    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setMinimumHeight(I)V

    .line 571
    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setMinimumWidth(I)V

    .line 572
    const v2, 0x7f020093

    invoke-static {v2, v3, v3}, Lcom/teamspeak/ts3client/data/d/t;->a(IFF)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 573
    iget-object v2, v0, Lcom/teamspeak/ts3client/data/u;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 575
    :cond_1a8
    iget-object v1, p0, Lcom/teamspeak/ts3client/data/g;->a:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/teamspeak/ts3client/data/a;

    .line 50063
    iget-boolean v1, v1, Lcom/teamspeak/ts3client/data/a;->m:Z

    .line 575
    if-eqz v1, :cond_1d0

    .line 576
    new-instance v1, Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/teamspeak/ts3client/data/g;->e:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 577
    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setMinimumHeight(I)V

    .line 578
    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setMinimumWidth(I)V

    .line 579
    const v2, 0x7f0200a6

    invoke-static {v2, v3, v3}, Lcom/teamspeak/ts3client/data/d/t;->a(IFF)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 580
    iget-object v2, v0, Lcom/teamspeak/ts3client/data/u;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 582
    :cond_1d0
    iget-object v1, p0, Lcom/teamspeak/ts3client/data/g;->a:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/teamspeak/ts3client/data/a;

    .line 50064
    iget-wide v2, v1, Lcom/teamspeak/ts3client/data/a;->s:J

    .line 582
    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_20d

    .line 583
    new-instance v2, Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/teamspeak/ts3client/data/g;->e:Landroid/content/Context;

    invoke-direct {v2, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 584
    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setMinimumHeight(I)V

    .line 585
    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setMinimumWidth(I)V

    .line 587
    :try_start_1ed
    iget-object v1, p0, Lcom/teamspeak/ts3client/data/g;->f:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50065
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50066
    iget-object v3, v1, Lcom/teamspeak/ts3client/data/e;->r:Lcom/teamspeak/ts3client/data/c/d;

    .line 587
    iget-object v1, p0, Lcom/teamspeak/ts3client/data/g;->a:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/teamspeak/ts3client/data/a;

    .line 50067
    iget-wide v4, v1, Lcom/teamspeak/ts3client/data/a;->s:J

    .line 587
    invoke-virtual {v3, v4, v5}, Lcom/teamspeak/ts3client/data/c/d;->a(J)Lcom/teamspeak/ts3client/data/c/c;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/c/c;->a()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 588
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/u;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V
    :try_end_20d
    .catch Ljava/lang/Exception; {:try_start_1ed .. :try_end_20d} :catch_3d7

    .line 618
    :cond_20d
    :goto_20d
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/g;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/data/a;

    .line 50068
    iget-boolean v0, v0, Lcom/teamspeak/ts3client/data/a;->o:Z

    .line 618
    if-eqz v0, :cond_3d0

    .line 619
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/g;->b:Lcom/teamspeak/ts3client/data/customExpandableListView;

    invoke-virtual {v0, p1}, Lcom/teamspeak/ts3client/data/customExpandableListView;->expandGroup(I)Z

    .line 623
    :goto_21e
    new-instance v0, Lcom/teamspeak/ts3client/data/l;

    invoke-direct {v0, p0, p1}, Lcom/teamspeak/ts3client/data/l;-><init>(Lcom/teamspeak/ts3client/data/g;I)V

    invoke-virtual {p3, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 635
    new-instance v0, Lcom/teamspeak/ts3client/data/m;

    invoke-direct {v0, p0, p1}, Lcom/teamspeak/ts3client/data/m;-><init>(Lcom/teamspeak/ts3client/data/g;I)V

    invoke-virtual {p3, v0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    goto/16 :goto_4a

    .line 522
    :cond_230
    iget-object v1, v0, Lcom/teamspeak/ts3client/data/u;->b:Landroid/widget/ImageView;

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 523
    iget-object v1, v0, Lcom/teamspeak/ts3client/data/u;->b:Landroid/widget/ImageView;

    const v2, 0x7f02006c

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto/16 :goto_bb

    .line 528
    :cond_23f
    iget-object v1, v0, Lcom/teamspeak/ts3client/data/u;->b:Landroid/widget/ImageView;

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_cd

    .line 537
    :cond_246
    iget-object v1, p0, Lcom/teamspeak/ts3client/data/g;->a:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/teamspeak/ts3client/data/a;

    .line 50051
    iget v1, v1, Lcom/teamspeak/ts3client/data/a;->i:I

    .line 537
    const/4 v2, -0x1

    if-eq v1, v2, :cond_297

    iget-object v1, p0, Lcom/teamspeak/ts3client/data/g;->a:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/teamspeak/ts3client/data/a;

    .line 50052
    iget v2, v1, Lcom/teamspeak/ts3client/data/a;->i:I

    .line 537
    iget-object v1, p0, Lcom/teamspeak/ts3client/data/g;->a:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/teamspeak/ts3client/data/a;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/data/a;->k()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-gt v2, v1, :cond_297

    .line 538
    iget-object v1, p0, Lcom/teamspeak/ts3client/data/g;->a:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/teamspeak/ts3client/data/a;

    .line 50053
    iget-boolean v1, v1, Lcom/teamspeak/ts3client/data/a;->k:Z

    .line 538
    if-eqz v1, :cond_289

    .line 539
    iget-object v1, v0, Lcom/teamspeak/ts3client/data/u;->d:Landroid/widget/ImageView;

    const v2, 0x7f020058

    invoke-static {v2, v3, v3}, Lcom/teamspeak/ts3client/data/d/t;->a(IFF)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto/16 :goto_10c

    .line 541
    :cond_289
    iget-object v1, v0, Lcom/teamspeak/ts3client/data/u;->d:Landroid/widget/ImageView;

    const v2, 0x7f020057

    invoke-static {v2, v3, v3}, Lcom/teamspeak/ts3client/data/d/t;->a(IFF)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto/16 :goto_10c

    .line 542
    :cond_297
    iget-object v1, p0, Lcom/teamspeak/ts3client/data/g;->a:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/teamspeak/ts3client/data/a;

    .line 50054
    iget-boolean v1, v1, Lcom/teamspeak/ts3client/data/a;->m:Z

    .line 542
    if-eqz v1, :cond_2d7

    iget-object v1, p0, Lcom/teamspeak/ts3client/data/g;->a:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/teamspeak/ts3client/data/a;

    .line 50055
    iget-boolean v1, v1, Lcom/teamspeak/ts3client/data/a;->n:Z

    .line 542
    if-nez v1, :cond_2d7

    .line 543
    iget-object v1, p0, Lcom/teamspeak/ts3client/data/g;->a:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/teamspeak/ts3client/data/a;

    .line 50056
    iget-boolean v1, v1, Lcom/teamspeak/ts3client/data/a;->k:Z

    .line 543
    if-eqz v1, :cond_2c9

    .line 544
    iget-object v1, v0, Lcom/teamspeak/ts3client/data/u;->d:Landroid/widget/ImageView;

    const v2, 0x7f02005a

    invoke-static {v2, v3, v3}, Lcom/teamspeak/ts3client/data/d/t;->a(IFF)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto/16 :goto_10c

    .line 546
    :cond_2c9
    iget-object v1, v0, Lcom/teamspeak/ts3client/data/u;->d:Landroid/widget/ImageView;

    const v2, 0x7f020059

    invoke-static {v2, v3, v3}, Lcom/teamspeak/ts3client/data/d/t;->a(IFF)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto/16 :goto_10c

    .line 547
    :cond_2d7
    iget-object v1, p0, Lcom/teamspeak/ts3client/data/g;->a:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/teamspeak/ts3client/data/a;

    .line 50057
    iget-boolean v1, v1, Lcom/teamspeak/ts3client/data/a;->k:Z

    .line 547
    if-eqz v1, :cond_2f1

    .line 548
    iget-object v1, v0, Lcom/teamspeak/ts3client/data/u;->d:Landroid/widget/ImageView;

    const v2, 0x7f020055

    invoke-static {v2, v3, v3}, Lcom/teamspeak/ts3client/data/d/t;->a(IFF)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto/16 :goto_10c

    .line 550
    :cond_2f1
    iget-object v1, v0, Lcom/teamspeak/ts3client/data/u;->d:Landroid/widget/ImageView;

    const v2, 0x7f020054

    invoke-static {v2, v3, v3}, Lcom/teamspeak/ts3client/data/d/t;->a(IFF)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto/16 :goto_10c

    .line 594
    :cond_2ff
    iget-object v1, v0, Lcom/teamspeak/ts3client/data/u;->c:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 595
    invoke-virtual {v2, v7}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_321

    .line 596
    invoke-virtual {v2, v7}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v1

    const-string v3, "r"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_380

    .line 597
    iget-object v1, v0, Lcom/teamspeak/ts3client/data/u;->c:Landroid/widget/TextView;

    const/16 v3, 0x15

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setGravity(I)V

    .line 606
    :cond_321
    :goto_321
    invoke-virtual {v2, v4}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v1

    const-string v3, "---"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_35d

    invoke-virtual {v2, v4}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v1

    const-string v3, "..."

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_35d

    invoke-virtual {v2, v4}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v1

    const-string v3, "-.-"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_35d

    invoke-virtual {v2, v4}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v1

    const-string v3, "___"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_35d

    invoke-virtual {v2, v4}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v1

    const-string v3, "-.."

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_377

    .line 607
    :cond_35d
    invoke-virtual {v2, v4}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v1

    const-string v3, "-.-"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3b2

    .line 608
    iget-object v1, v0, Lcom/teamspeak/ts3client/data/u;->c:Landroid/widget/TextView;

    const-string v2, "-."

    invoke-static {v1, v2}, Lcom/teamspeak/ts3client/data/g;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 613
    :goto_370
    iget-object v1, v0, Lcom/teamspeak/ts3client/data/u;->c:Landroid/widget/TextView;

    const/16 v2, 0x13

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setGravity(I)V

    .line 615
    :cond_377
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/u;->d:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_20d

    .line 598
    :cond_380
    invoke-virtual {v2, v7}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v1

    const-string v3, "c"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_394

    .line 599
    iget-object v1, v0, Lcom/teamspeak/ts3client/data/u;->c:Landroid/widget/TextView;

    const/16 v3, 0x11

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setGravity(I)V

    goto :goto_321

    .line 600
    :cond_394
    invoke-virtual {v2, v7}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v1

    const-string v3, "*"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3a9

    .line 601
    iget-object v1, v0, Lcom/teamspeak/ts3client/data/u;->c:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/teamspeak/ts3client/data/g;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 604
    :cond_3a9
    iget-object v1, v0, Lcom/teamspeak/ts3client/data/u;->c:Landroid/widget/TextView;

    const/16 v3, 0x13

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setGravity(I)V

    goto/16 :goto_321

    .line 609
    :cond_3b2
    invoke-virtual {v2, v4}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v1

    const-string v3, "___"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3c6

    .line 610
    iget-object v1, v0, Lcom/teamspeak/ts3client/data/u;->c:Landroid/widget/TextView;

    const-string v2, "\u2500"

    invoke-static {v1, v2}, Lcom/teamspeak/ts3client/data/g;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    goto :goto_370

    .line 612
    :cond_3c6
    iget-object v1, v0, Lcom/teamspeak/ts3client/data/u;->c:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/teamspeak/ts3client/data/g;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    goto :goto_370

    .line 621
    :cond_3d0
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/g;->b:Lcom/teamspeak/ts3client/data/customExpandableListView;

    invoke-virtual {v0, p1}, Lcom/teamspeak/ts3client/data/customExpandableListView;->collapseGroup(I)Z

    goto/16 :goto_21e

    .line 592
    :catch_3d7
    move-exception v0

    goto/16 :goto_20d
.end method

.method public final hasStableIds()Z
    .registers 2

    .prologue
    .line 715
    const/4 v0, 0x0

    return v0
.end method

.method public final isChildSelectable(II)Z
    .registers 4

    .prologue
    .line 721
    const/4 v0, 0x0

    return v0
.end method
