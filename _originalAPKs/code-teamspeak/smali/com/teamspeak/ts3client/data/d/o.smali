.class final Lcom/teamspeak/ts3client/data/d/o;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Landroid/net/Uri;

.field final synthetic b:Landroid/app/AlertDialog;

.field final synthetic c:Lcom/teamspeak/ts3client/data/d/n;


# direct methods
.method constructor <init>(Lcom/teamspeak/ts3client/data/d/n;Landroid/net/Uri;Landroid/app/AlertDialog;)V
    .registers 4

    .prologue
    .line 151
    iput-object p1, p0, Lcom/teamspeak/ts3client/data/d/o;->c:Lcom/teamspeak/ts3client/data/d/n;

    iput-object p2, p0, Lcom/teamspeak/ts3client/data/d/o;->a:Landroid/net/Uri;

    iput-object p3, p0, Lcom/teamspeak/ts3client/data/d/o;->b:Landroid/app/AlertDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/content/DialogInterface;I)V
    .registers 6

    .prologue
    .line 156
    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/teamspeak/ts3client/StartGUIFragment;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 157
    const/high16 v1, 0x30000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 158
    const-string v1, "bookmark"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 159
    const-string v1, "uri"

    iget-object v2, p0, Lcom/teamspeak/ts3client/data/d/o;->a:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 160
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/teamspeak/ts3client/Ts3Application;->startActivity(Landroid/content/Intent;)V

    .line 161
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/d/o;->b:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 162
    return-void
.end method
