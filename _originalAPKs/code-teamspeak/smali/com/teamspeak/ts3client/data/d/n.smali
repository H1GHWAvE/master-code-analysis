.class public final Lcom/teamspeak/ts3client/data/d/n;
.super Landroid/text/method/LinkMovementMethod;
.source "SourceFile"


# static fields
.field private static a:Lcom/teamspeak/ts3client/data/d/n;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 29
    new-instance v0, Lcom/teamspeak/ts3client/data/d/n;

    invoke-direct {v0}, Lcom/teamspeak/ts3client/data/d/n;-><init>()V

    sput-object v0, Lcom/teamspeak/ts3client/data/d/n;->a:Lcom/teamspeak/ts3client/data/d/n;

    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 27
    invoke-direct {p0}, Landroid/text/method/LinkMovementMethod;-><init>()V

    return-void
.end method

.method public static a()Landroid/text/method/MovementMethod;
    .registers 1

    .prologue
    .line 32
    sget-object v0, Lcom/teamspeak/ts3client/data/d/n;->a:Lcom/teamspeak/ts3client/data/d/n;

    return-object v0
.end method

.method private a(Landroid/text/style/URLSpan;)V
    .registers 12

    .prologue
    const/4 v9, 0x1

    const/4 v1, 0x0

    .line 117
    new-instance v2, Lcom/teamspeak/ts3client/data/ab;

    invoke-direct {v2}, Lcom/teamspeak/ts3client/data/ab;-><init>()V

    .line 118
    invoke-virtual {p1}, Landroid/text/style/URLSpan;->getURL()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 119
    if-eqz v3, :cond_66

    .line 120
    invoke-virtual {v3}, Landroid/net/Uri;->getQuery()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_67

    .line 121
    invoke-virtual {v3}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/teamspeak/ts3client/data/ab;->a(Ljava/lang/String;)V

    .line 148
    :cond_1e
    :goto_1e
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v4

    .line 9077
    iget-object v4, v4, Lcom/teamspeak/ts3client/Ts3Application;->c:Landroid/support/v4/app/Fragment;

    .line 148
    invoke-virtual {v4}, Landroid/support/v4/app/Fragment;->i()Landroid/support/v4/app/bb;

    move-result-object v4

    invoke-direct {v0, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 149
    const-string v4, "TS3 URL"

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 150
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Found the following Host:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 10055
    iget-object v2, v2, Lcom/teamspeak/ts3client/data/ab;->c:Ljava/lang/String;

    .line 150
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 151
    const/4 v2, -0x3

    const-string v4, "Bookmark"

    new-instance v5, Lcom/teamspeak/ts3client/data/d/o;

    invoke-direct {v5, p0, v3, v0}, Lcom/teamspeak/ts3client/data/d/o;-><init>(Lcom/teamspeak/ts3client/data/d/n;Landroid/net/Uri;Landroid/app/AlertDialog;)V

    invoke-virtual {v0, v2, v4, v5}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 164
    const/4 v2, -0x2

    const-string v3, "Cancel"

    new-instance v4, Lcom/teamspeak/ts3client/data/d/p;

    invoke-direct {v4, p0, v0}, Lcom/teamspeak/ts3client/data/d/p;-><init>(Lcom/teamspeak/ts3client/data/d/n;Landroid/app/AlertDialog;)V

    invoke-virtual {v0, v2, v3, v4}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 171
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setCancelable(Z)V

    .line 172
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 174
    :cond_66
    return-void

    .line 123
    :cond_67
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 124
    invoke-virtual {v3}, Landroid/net/Uri;->getQuery()Ljava/lang/String;

    move-result-object v0

    const-string v5, "&"

    invoke-virtual {v0, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    array-length v6, v5

    move v0, v1

    :goto_78
    if-ge v0, v6, :cond_97

    aget-object v7, v5, v0

    .line 125
    const-string v8, "="

    invoke-virtual {v7, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    .line 126
    array-length v8, v7

    if-le v8, v9, :cond_8f

    .line 127
    aget-object v8, v7, v1

    aget-object v7, v7, v9

    invoke-virtual {v4, v8, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 124
    :goto_8c
    add-int/lit8 v0, v0, 0x1

    goto :goto_78

    .line 129
    :cond_8f
    aget-object v7, v7, v1

    const-string v8, ""

    invoke-virtual {v4, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_8c

    .line 132
    :cond_97
    invoke-virtual {v3}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/teamspeak/ts3client/data/ab;->a(Ljava/lang/String;)V

    .line 133
    const-string v0, "port"

    invoke-virtual {v4, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_ca

    .line 134
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 7055
    iget-object v5, v2, Lcom/teamspeak/ts3client/data/ab;->c:Ljava/lang/String;

    .line 134
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, ":"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v0, "port"

    invoke-virtual {v4, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/teamspeak/ts3client/data/ab;->a(Ljava/lang/String;)V

    .line 135
    :cond_ca
    const-string v0, "nickname"

    invoke-virtual {v4, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_dc

    .line 136
    const-string v0, "nickname"

    invoke-virtual {v4, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 7107
    iput-object v0, v2, Lcom/teamspeak/ts3client/data/ab;->f:Ljava/lang/String;

    .line 137
    :cond_dc
    const-string v0, "password"

    invoke-virtual {v4, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_ee

    .line 138
    const-string v0, "password"

    invoke-virtual {v4, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 7115
    iput-object v0, v2, Lcom/teamspeak/ts3client/data/ab;->e:Ljava/lang/String;

    .line 139
    :cond_ee
    const-string v0, "channel"

    invoke-virtual {v4, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_100

    .line 140
    const-string v0, "channel"

    invoke-virtual {v4, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 8075
    iput-object v0, v2, Lcom/teamspeak/ts3client/data/ab;->g:Ljava/lang/String;

    .line 141
    :cond_100
    const-string v0, "channelpassword"

    invoke-virtual {v4, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_112

    .line 142
    const-string v0, "channelpassword"

    invoke-virtual {v4, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 8083
    iput-object v0, v2, Lcom/teamspeak/ts3client/data/ab;->h:Ljava/lang/String;

    .line 143
    :cond_112
    const-string v0, "addbookmark"

    invoke-virtual {v4, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_124

    .line 144
    const-string v0, "addbookmark"

    invoke-virtual {v4, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 8099
    iput-object v0, v2, Lcom/teamspeak/ts3client/data/ab;->a:Ljava/lang/String;

    .line 145
    :cond_124
    const-string v0, "token"

    invoke-virtual {v4, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 146
    const-string v0, "token"

    invoke-virtual {v4, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 8140
    iput-object v0, v2, Lcom/teamspeak/ts3client/data/ab;->i:Ljava/lang/String;

    goto/16 :goto_1e
.end method


# virtual methods
.method public final onTouchEvent(Landroid/widget/TextView;Landroid/text/Spannable;Landroid/view/MotionEvent;)Z
    .registers 14

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 37
    invoke-virtual {p3}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    .line 39
    if-eq v3, v1, :cond_a

    if-nez v3, :cond_254

    .line 40
    :cond_a
    invoke-virtual {p3}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    .line 41
    invoke-virtual {p3}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    float-to-int v4, v4

    .line 43
    invoke-virtual {p1}, Landroid/widget/TextView;->getTotalPaddingLeft()I

    move-result v5

    sub-int/2addr v0, v5

    .line 44
    invoke-virtual {p1}, Landroid/widget/TextView;->getTotalPaddingTop()I

    move-result v5

    sub-int/2addr v4, v5

    .line 46
    invoke-virtual {p1}, Landroid/widget/TextView;->getScrollX()I

    move-result v5

    add-int/2addr v0, v5

    .line 47
    invoke-virtual {p1}, Landroid/widget/TextView;->getScrollY()I

    move-result v5

    add-int/2addr v4, v5

    .line 49
    invoke-virtual {p1}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v5

    .line 50
    invoke-virtual {v5, v4}, Landroid/text/Layout;->getLineForVertical(I)I

    move-result v4

    .line 51
    int-to-float v0, v0

    invoke-virtual {v5, v4, v0}, Landroid/text/Layout;->getOffsetForHorizontal(IF)I

    move-result v0

    .line 53
    const-class v4, Landroid/text/style/ClickableSpan;

    invoke-interface {p2, v0, v0, v4}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/text/style/ClickableSpan;

    .line 55
    array-length v4, v0

    if-eqz v4, :cond_23e

    .line 56
    if-ne v3, v1, :cond_23b

    .line 57
    aget-object v3, v0, v2

    instance-of v3, v3, Landroid/text/style/URLSpan;

    if-eqz v3, :cond_236

    .line 58
    aget-object v0, v0, v2

    check-cast v0, Landroid/text/style/URLSpan;

    .line 59
    invoke-virtual {v0}, Landroid/text/style/URLSpan;->getURL()Ljava/lang/String;

    move-result-object v3

    const-string v4, "client://"

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_d4

    .line 61
    invoke-virtual {v0}, Landroid/text/style/URLSpan;->getURL()Ljava/lang/String;

    move-result-object v3

    const-string v4, "ts3file://"

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_d4

    .line 63
    invoke-virtual {v0}, Landroid/text/style/URLSpan;->getURL()Ljava/lang/String;

    move-result-object v3

    const-string v4, "ts3server://"

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1a7

    .line 1117
    new-instance v3, Lcom/teamspeak/ts3client/data/ab;

    invoke-direct {v3}, Lcom/teamspeak/ts3client/data/ab;-><init>()V

    .line 1118
    invoke-virtual {v0}, Landroid/text/style/URLSpan;->getURL()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 1119
    if-eqz v4, :cond_d4

    .line 1120
    invoke-virtual {v4}, Landroid/net/Uri;->getQuery()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_d6

    .line 1121
    invoke-virtual {v4}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/teamspeak/ts3client/data/ab;->a(Ljava/lang/String;)V

    .line 1148
    :cond_8c
    :goto_8c
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v5

    .line 4077
    iget-object v5, v5, Lcom/teamspeak/ts3client/Ts3Application;->c:Landroid/support/v4/app/Fragment;

    .line 1148
    invoke-virtual {v5}, Landroid/support/v4/app/Fragment;->i()Landroid/support/v4/app/bb;

    move-result-object v5

    invoke-direct {v0, v5}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 1149
    const-string v5, "TS3 URL"

    invoke-virtual {v0, v5}, Landroid/app/AlertDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 1150
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Found the following Host:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 5055
    iget-object v3, v3, Lcom/teamspeak/ts3client/data/ab;->c:Ljava/lang/String;

    .line 1150
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 1151
    const/4 v3, -0x3

    const-string v5, "Bookmark"

    new-instance v6, Lcom/teamspeak/ts3client/data/d/o;

    invoke-direct {v6, p0, v4, v0}, Lcom/teamspeak/ts3client/data/d/o;-><init>(Lcom/teamspeak/ts3client/data/d/n;Landroid/net/Uri;Landroid/app/AlertDialog;)V

    invoke-virtual {v0, v3, v5, v6}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 1164
    const/4 v3, -0x2

    const-string v4, "Cancel"

    new-instance v5, Lcom/teamspeak/ts3client/data/d/p;

    invoke-direct {v5, p0, v0}, Lcom/teamspeak/ts3client/data/d/p;-><init>(Lcom/teamspeak/ts3client/data/d/n;Landroid/app/AlertDialog;)V

    invoke-virtual {v0, v3, v4, v5}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 1171
    invoke-virtual {v0, v2}, Landroid/app/AlertDialog;->setCancelable(Z)V

    .line 1172
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    :cond_d4
    :goto_d4
    move v0, v1

    .line 112
    :goto_d5
    return v0

    .line 1123
    :cond_d6
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 1124
    invoke-virtual {v4}, Landroid/net/Uri;->getQuery()Ljava/lang/String;

    move-result-object v0

    const-string v6, "&"

    invoke-virtual {v0, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    array-length v7, v6

    move v0, v2

    :goto_e7
    if-ge v0, v7, :cond_106

    aget-object v8, v6, v0

    .line 1125
    const-string v9, "="

    invoke-virtual {v8, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    .line 1126
    array-length v9, v8

    if-le v9, v1, :cond_fe

    .line 1127
    aget-object v9, v8, v2

    aget-object v8, v8, v1

    invoke-virtual {v5, v9, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1124
    :goto_fb
    add-int/lit8 v0, v0, 0x1

    goto :goto_e7

    .line 1129
    :cond_fe
    aget-object v8, v8, v2

    const-string v9, ""

    invoke-virtual {v5, v8, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_fb

    .line 1132
    :cond_106
    invoke-virtual {v4}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/teamspeak/ts3client/data/ab;->a(Ljava/lang/String;)V

    .line 1133
    const-string v0, "port"

    invoke-virtual {v5, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_139

    .line 1134
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 2055
    iget-object v6, v3, Lcom/teamspeak/ts3client/data/ab;->c:Ljava/lang/String;

    .line 1134
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v6, ":"

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v0, "port"

    invoke-virtual {v5, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/teamspeak/ts3client/data/ab;->a(Ljava/lang/String;)V

    .line 1135
    :cond_139
    const-string v0, "nickname"

    invoke-virtual {v5, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14b

    .line 1136
    const-string v0, "nickname"

    invoke-virtual {v5, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2107
    iput-object v0, v3, Lcom/teamspeak/ts3client/data/ab;->f:Ljava/lang/String;

    .line 1137
    :cond_14b
    const-string v0, "password"

    invoke-virtual {v5, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_15d

    .line 1138
    const-string v0, "password"

    invoke-virtual {v5, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2115
    iput-object v0, v3, Lcom/teamspeak/ts3client/data/ab;->e:Ljava/lang/String;

    .line 1139
    :cond_15d
    const-string v0, "channel"

    invoke-virtual {v5, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_16f

    .line 1140
    const-string v0, "channel"

    invoke-virtual {v5, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 3075
    iput-object v0, v3, Lcom/teamspeak/ts3client/data/ab;->g:Ljava/lang/String;

    .line 1141
    :cond_16f
    const-string v0, "channelpassword"

    invoke-virtual {v5, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_181

    .line 1142
    const-string v0, "channelpassword"

    invoke-virtual {v5, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 3083
    iput-object v0, v3, Lcom/teamspeak/ts3client/data/ab;->h:Ljava/lang/String;

    .line 1143
    :cond_181
    const-string v0, "addbookmark"

    invoke-virtual {v5, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_193

    .line 1144
    const-string v0, "addbookmark"

    invoke-virtual {v5, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 3099
    iput-object v0, v3, Lcom/teamspeak/ts3client/data/ab;->a:Ljava/lang/String;

    .line 1145
    :cond_193
    const-string v0, "token"

    invoke-virtual {v5, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8c

    .line 1146
    const-string v0, "token"

    invoke-virtual {v5, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 3140
    iput-object v0, v3, Lcom/teamspeak/ts3client/data/ab;->i:Ljava/lang/String;

    goto/16 :goto_8c

    .line 65
    :cond_1a7
    invoke-virtual {v0}, Landroid/text/style/URLSpan;->getURL()Ljava/lang/String;

    move-result-object v2

    const-string v3, "channelid://"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1f5

    .line 67
    :try_start_1b3
    invoke-virtual {v0}, Landroid/text/style/URLSpan;->getURL()Ljava/lang/String;

    move-result-object v0

    const/16 v2, 0xc

    invoke-virtual {v0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 68
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 5061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 69
    if-eqz v0, :cond_1f2

    .line 70
    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/teamspeak/ts3client/data/b;->d(Ljava/lang/Long;)Z

    move-result v4

    if-eqz v4, :cond_1f2

    .line 71
    new-instance v4, Lcom/teamspeak/ts3client/d/a/a;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v5

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v5, v2}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v4, v2, v3}, Lcom/teamspeak/ts3client/d/a/a;-><init>(Lcom/teamspeak/ts3client/data/a;Z)V

    .line 5206
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->k:Lcom/teamspeak/ts3client/t;

    .line 5688
    iget-object v0, v0, Landroid/support/v4/app/Fragment;->M:Landroid/support/v4/app/bl;

    .line 72
    const-string v2, "ChannelActionDialog"

    invoke-virtual {v4, v0, v2}, Lcom/teamspeak/ts3client/d/a/a;->a(Landroid/support/v4/app/bi;Ljava/lang/String;)V
    :try_end_1f2
    .catch Ljava/lang/Exception; {:try_start_1b3 .. :try_end_1f2} :catch_25d

    :cond_1f2
    move v0, v1

    .line 74
    goto/16 :goto_d5

    .line 78
    :cond_1f5
    invoke-virtual {v0}, Landroid/text/style/URLSpan;->getURL()Ljava/lang/String;

    move-result-object v0

    .line 79
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v2

    .line 6061
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 6234
    iget-object v2, v2, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 79
    invoke-virtual {v2, v0}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_bbcode_shouldPrependHTTP(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_216

    .line 81
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "http://"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 85
    :cond_216
    :try_start_216
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 86
    invoke-virtual {p1}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 87
    new-instance v3, Landroid/content/Intent;

    const-string v4, "android.intent.action.VIEW"

    invoke-direct {v3, v4, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 88
    const-string v0, "com.android.browser.application_id"

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 89
    invoke-virtual {v2, v3}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_231
    .catch Landroid/content/ActivityNotFoundException; {:try_start_216 .. :try_end_231} :catch_233

    goto/16 :goto_d4

    :catch_233
    move-exception v0

    goto/16 :goto_d4

    .line 97
    :cond_236
    aget-object v0, v0, v2

    invoke-virtual {v0, p1}, Landroid/text/style/ClickableSpan;->onClick(Landroid/view/View;)V

    :cond_23b
    :goto_23b
    move v0, v1

    .line 107
    goto/16 :goto_d5

    .line 99
    :cond_23e
    if-nez v3, :cond_23b

    .line 101
    const/4 v2, 0x0

    :try_start_241
    aget-object v2, v0, v2

    invoke-interface {p2, v2}, Landroid/text/Spannable;->getSpanStart(Ljava/lang/Object;)I

    move-result v2

    const/4 v3, 0x0

    aget-object v0, v0, v3

    invoke-interface {p2, v0}, Landroid/text/Spannable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v0

    invoke-static {p2, v2, v0}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;II)V
    :try_end_251
    .catch Ljava/lang/Exception; {:try_start_241 .. :try_end_251} :catch_252

    goto :goto_23b

    :catch_252
    move-exception v0

    goto :goto_23b

    .line 109
    :cond_254
    invoke-static {p2}, Landroid/text/Selection;->removeSelection(Landroid/text/Spannable;)V

    .line 112
    invoke-super {p0, p1, p2, p3}, Landroid/text/method/LinkMovementMethod;->onTouchEvent(Landroid/widget/TextView;Landroid/text/Spannable;Landroid/view/MotionEvent;)Z

    move-result v0

    goto/16 :goto_d5

    .line 76
    :catch_25d
    move-exception v0

    goto/16 :goto_d4
.end method
