.class public final Lcom/teamspeak/ts3client/data/d/j;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:Ljava/io/File;

.field b:Ljava/lang/String;

.field c:I

.field d:Ljava/lang/String;

.field e:Lcom/teamspeak/ts3client/Ts3Application;

.field f:Landroid/app/ProgressDialog;

.field g:Ljava/lang/String;

.field h:I

.field private i:Lcom/teamspeak/ts3client/data/d/k;


# direct methods
.method private constructor <init>(Ljava/io/File;Ljava/lang/String;ILjava/lang/String;Lcom/teamspeak/ts3client/Ts3Application;Landroid/content/Context;Lcom/teamspeak/ts3client/data/d/k;Ljava/lang/String;I)V
    .registers 11

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object p1, p0, Lcom/teamspeak/ts3client/data/d/j;->a:Ljava/io/File;

    .line 46
    iput-object p2, p0, Lcom/teamspeak/ts3client/data/d/j;->b:Ljava/lang/String;

    .line 47
    iput p3, p0, Lcom/teamspeak/ts3client/data/d/j;->c:I

    .line 48
    iput-object p4, p0, Lcom/teamspeak/ts3client/data/d/j;->d:Ljava/lang/String;

    .line 49
    iput-object p5, p0, Lcom/teamspeak/ts3client/data/d/j;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50
    iput-object p7, p0, Lcom/teamspeak/ts3client/data/d/j;->i:Lcom/teamspeak/ts3client/data/d/k;

    .line 51
    iput-object p8, p0, Lcom/teamspeak/ts3client/data/d/j;->g:Ljava/lang/String;

    .line 52
    iput p9, p0, Lcom/teamspeak/ts3client/data/d/j;->h:I

    .line 53
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p6}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/d/j;->f:Landroid/app/ProgressDialog;

    .line 55
    return-void
.end method

.method private a()Ljava/lang/String;
    .registers 2

    .prologue
    .line 58
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/d/j;->g:Ljava/lang/String;

    return-object v0
.end method

.method private static synthetic a(Lcom/teamspeak/ts3client/data/d/j;)Ljava/lang/String;
    .registers 2

    .prologue
    .line 31
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/d/j;->b:Ljava/lang/String;

    return-object v0
.end method

.method private static synthetic b(Lcom/teamspeak/ts3client/data/d/j;)I
    .registers 2

    .prologue
    .line 31
    iget v0, p0, Lcom/teamspeak/ts3client/data/d/j;->h:I

    return v0
.end method

.method private b()V
    .registers 6
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 63
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/d/j;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 1085
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->d:Ljava/util/logging/Logger;

    .line 63
    sget-object v1, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Updating Content_"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/teamspeak/ts3client/data/d/j;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 64
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_2e

    .line 65
    new-instance v0, Lcom/teamspeak/ts3client/data/d/l;

    invoke-direct {v0, p0}, Lcom/teamspeak/ts3client/data/d/l;-><init>(Lcom/teamspeak/ts3client/data/d/j;)V

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v2, v4, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Lcom/teamspeak/ts3client/data/d/l;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 68
    :goto_2d
    return-void

    .line 67
    :cond_2e
    new-instance v0, Lcom/teamspeak/ts3client/data/d/l;

    invoke-direct {v0, p0}, Lcom/teamspeak/ts3client/data/d/l;-><init>(Lcom/teamspeak/ts3client/data/d/j;)V

    new-array v1, v4, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/d/l;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_2d
.end method

.method private static synthetic c(Lcom/teamspeak/ts3client/data/d/j;)Ljava/lang/String;
    .registers 2

    .prologue
    .line 31
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/d/j;->d:Ljava/lang/String;

    return-object v0
.end method

.method private static synthetic d(Lcom/teamspeak/ts3client/data/d/j;)I
    .registers 2

    .prologue
    .line 31
    iget v0, p0, Lcom/teamspeak/ts3client/data/d/j;->c:I

    return v0
.end method

.method private static synthetic e(Lcom/teamspeak/ts3client/data/d/j;)Lcom/teamspeak/ts3client/Ts3Application;
    .registers 2

    .prologue
    .line 31
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/d/j;->e:Lcom/teamspeak/ts3client/Ts3Application;

    return-object v0
.end method

.method private static synthetic f(Lcom/teamspeak/ts3client/data/d/j;)Ljava/io/File;
    .registers 2

    .prologue
    .line 31
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/d/j;->a:Ljava/io/File;

    return-object v0
.end method

.method private static synthetic g(Lcom/teamspeak/ts3client/data/d/j;)Landroid/app/ProgressDialog;
    .registers 2

    .prologue
    .line 31
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/d/j;->f:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method private static synthetic h(Lcom/teamspeak/ts3client/data/d/j;)Ljava/lang/String;
    .registers 2

    .prologue
    .line 31
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/d/j;->g:Ljava/lang/String;

    return-object v0
.end method

.method private static synthetic i(Lcom/teamspeak/ts3client/data/d/j;)Lcom/teamspeak/ts3client/data/d/k;
    .registers 2

    .prologue
    .line 31
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/d/j;->i:Lcom/teamspeak/ts3client/data/d/k;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .registers 3

    .prologue
    .line 72
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ContentDownloader [targetPath="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/teamspeak/ts3client/data/d/j;->a:Ljava/io/File;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", NewVersion="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/teamspeak/ts3client/data/d/j;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Setting="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/data/d/j;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", app="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/data/d/j;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", event="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/data/d/j;->i:Lcom/teamspeak/ts3client/data/d/k;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", dialog="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/data/d/j;->f:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", description="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/data/d/j;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", size="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/teamspeak/ts3client/data/d/j;->h:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
