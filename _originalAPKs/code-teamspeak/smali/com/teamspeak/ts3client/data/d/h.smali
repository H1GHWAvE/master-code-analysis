.class public final Lcom/teamspeak/ts3client/data/d/h;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static a:I

.field static b:Ljava/lang/String;


# instance fields
.field private c:Ljava/lang/String;

.field private d:Lcom/teamspeak/ts3client/Ts3Application;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 20
    const/16 v0, 0x43e9

    sput v0, Lcom/teamspeak/ts3client/data/d/h;->a:I

    .line 21
    const-string v0, "blacklist.teamspeak.com"

    sput-object v0, Lcom/teamspeak/ts3client/data/d/h;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/teamspeak/ts3client/Ts3Application;Ljava/lang/String;)V
    .registers 6
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    :try_start_3
    invoke-static {p2}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v0

    .line 29
    invoke-virtual {v0}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/d/h;->c:Ljava/lang/String;

    .line 30
    iput-object p1, p0, Lcom/teamspeak/ts3client/data/d/h;->d:Lcom/teamspeak/ts3client/Ts3Application;

    .line 31
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_23

    .line 32
    new-instance v0, Lcom/teamspeak/ts3client/data/d/i;

    invoke-direct {v0, p0}, Lcom/teamspeak/ts3client/data/d/i;-><init>(Lcom/teamspeak/ts3client/data/d/h;)V

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Lcom/teamspeak/ts3client/data/d/i;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 38
    :goto_22
    return-void

    .line 34
    :cond_23
    new-instance v0, Lcom/teamspeak/ts3client/data/d/i;

    invoke-direct {v0, p0}, Lcom/teamspeak/ts3client/data/d/i;-><init>(Lcom/teamspeak/ts3client/data/d/h;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/d/i;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;
    :try_end_2e
    .catch Ljava/net/UnknownHostException; {:try_start_3 .. :try_end_2e} :catch_2f

    goto :goto_22

    .line 36
    :catch_2f
    move-exception v0

    .line 1085
    iget-object v0, p1, Lcom/teamspeak/ts3client/Ts3Application;->d:Ljava/util/logging/Logger;

    .line 36
    sget-object v1, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    const-string v2, "Could not resolve ip for check"

    invoke-virtual {v0, v1, v2}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    goto :goto_22
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/data/d/h;)Ljava/lang/String;
    .registers 2

    .prologue
    .line 18
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/d/h;->c:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/teamspeak/ts3client/data/d/h;)Lcom/teamspeak/ts3client/Ts3Application;
    .registers 2

    .prologue
    .line 18
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/d/h;->d:Lcom/teamspeak/ts3client/Ts3Application;

    return-object v0
.end method
