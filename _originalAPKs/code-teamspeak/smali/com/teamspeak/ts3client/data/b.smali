.class public final Lcom/teamspeak/ts3client/data/b;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Ljava/util/concurrent/ConcurrentHashMap;

.field public b:Lcom/teamspeak/ts3client/data/a;


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/b;->b:Lcom/teamspeak/ts3client/data/a;

    .line 11
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/b;->a:Ljava/util/concurrent/ConcurrentHashMap;

    .line 12
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
    .registers 3

    .prologue
    .line 15
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/b;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/data/a;

    return-object v0
.end method

.method public final a(Ljava/lang/Long;I)Ljava/lang/Long;
    .registers 9

    .prologue
    .line 31
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/b;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_a
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2b

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/data/a;

    .line 2111
    iget-wide v2, v0, Lcom/teamspeak/ts3client/data/a;->g:J

    .line 32
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_a

    .line 3095
    iget v2, v0, Lcom/teamspeak/ts3client/data/a;->f:I

    .line 32
    if-ne v2, p2, :cond_a

    .line 4087
    iget-wide v0, v0, Lcom/teamspeak/ts3client/data/a;->b:J

    .line 33
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 35
    :goto_2a
    return-object v0

    :cond_2b
    const/4 v0, 0x0

    goto :goto_2a
.end method

.method public final a()Ljava/util/concurrent/ConcurrentHashMap;
    .registers 2

    .prologue
    .line 19
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/b;->a:Ljava/util/concurrent/ConcurrentHashMap;

    return-object v0
.end method

.method public final a(Lcom/teamspeak/ts3client/data/a;)V
    .registers 6

    .prologue
    .line 39
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/b;->a:Ljava/util/concurrent/ConcurrentHashMap;

    .line 5087
    iget-wide v2, p1, Lcom/teamspeak/ts3client/data/a;->b:J

    .line 39
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 40
    return-void
.end method

.method public final b()Lcom/teamspeak/ts3client/data/a;
    .registers 2

    .prologue
    .line 47
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/b;->b:Lcom/teamspeak/ts3client/data/a;

    return-object v0
.end method

.method public final b(Ljava/lang/Long;)Ljava/lang/Long;
    .registers 8

    .prologue
    .line 23
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/b;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_a
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_27

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/data/a;

    .line 1111
    iget-wide v2, v0, Lcom/teamspeak/ts3client/data/a;->g:J

    .line 24
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_a

    .line 2087
    iget-wide v0, v0, Lcom/teamspeak/ts3client/data/a;->b:J

    .line 25
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 27
    :goto_26
    return-object v0

    :cond_27
    const/4 v0, 0x0

    goto :goto_26
.end method

.method public final b(Lcom/teamspeak/ts3client/data/a;)V
    .registers 2

    .prologue
    .line 51
    iput-object p1, p0, Lcom/teamspeak/ts3client/data/b;->b:Lcom/teamspeak/ts3client/data/a;

    .line 52
    return-void
.end method

.method public final c(Ljava/lang/Long;)V
    .registers 3

    .prologue
    .line 43
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/b;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 44
    return-void
.end method

.method public final d(Ljava/lang/Long;)Z
    .registers 3

    .prologue
    .line 55
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/b;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
