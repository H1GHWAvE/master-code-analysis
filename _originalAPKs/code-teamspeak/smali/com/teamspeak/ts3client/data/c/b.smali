.class final Lcom/teamspeak/ts3client/data/c/b;
.super Landroid/os/AsyncTask;
.source "SourceFile"

# interfaces
.implements Lcom/teamspeak/ts3client/data/w;


# instance fields
.field final synthetic a:Lcom/teamspeak/ts3client/data/c/a;

.field private b:I

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Lcom/teamspeak/ts3client/data/c;

.field private g:Ljava/lang/String;

.field private h:Ljava/io/File;


# direct methods
.method constructor <init>(Lcom/teamspeak/ts3client/data/c/a;)V
    .registers 4

    .prologue
    .line 42
    iput-object p1, p0, Lcom/teamspeak/ts3client/data/c/b;->a:Lcom/teamspeak/ts3client/data/c/a;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 45
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/TS3/cache/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/c/b;->c:Ljava/lang/String;

    .line 47
    const-string v0, ""

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/c/b;->e:Ljava/lang/String;

    return-void
.end method

.method private a()Landroid/graphics/Bitmap;
    .registers 11

    .prologue
    const/4 v1, 0x0

    const/16 v8, 0xa0

    const/4 v0, 0x0

    .line 106
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/teamspeak/ts3client/data/c/b;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v3

    .line 15061
    iget-object v3, v3, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 15287
    iget-object v3, v3, Lcom/teamspeak/ts3client/data/e;->q:Ljava/lang/String;

    .line 106
    const-string v4, "/"

    const-string v5, ""

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/avatar/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/teamspeak/ts3client/data/c/b;->g:Ljava/lang/String;

    .line 107
    new-instance v2, Ljava/io/File;

    iget-object v3, p0, Lcom/teamspeak/ts3client/data/c/b;->g:Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 108
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_3f

    .line 109
    invoke-virtual {v2}, Ljava/io/File;->mkdirs()Z

    .line 110
    :cond_3f
    new-instance v2, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/teamspeak/ts3client/data/c/b;->g:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "avatar_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/teamspeak/ts3client/data/c/b;->e:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lcom/teamspeak/ts3client/data/c/b;->h:Ljava/io/File;

    .line 111
    iget-object v2, p0, Lcom/teamspeak/ts3client/data/c/b;->h:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_fb

    .line 112
    const-string v2, ""

    .line 116
    :try_start_6b
    const-string v3, "MD5"

    invoke-static {v3}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v3

    .line 118
    new-instance v4, Ljava/io/FileInputStream;

    iget-object v5, p0, Lcom/teamspeak/ts3client/data/c/b;->h:Ljava/io/File;

    invoke-direct {v4, v5}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 119
    const/16 v5, 0xfa0

    new-array v5, v5, [B

    .line 122
    :goto_7c
    invoke-virtual {v4, v5}, Ljava/io/InputStream;->read([B)I

    move-result v6

    if-lez v6, :cond_b0

    .line 123
    const/4 v7, 0x0

    invoke-virtual {v3, v5, v7, v6}, Ljava/security/MessageDigest;->update([BII)V
    :try_end_86
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_6b .. :try_end_86} :catch_87
    .catch Ljava/io/FileNotFoundException; {:try_start_6b .. :try_end_86} :catch_e1
    .catch Ljava/io/IOException; {:try_start_6b .. :try_end_86} :catch_e9

    goto :goto_7c

    .line 131
    :catch_87
    move-exception v0

    move-object v9, v0

    move-object v0, v2

    move-object v2, v9

    :goto_8b
    invoke-virtual {v2}, Ljava/security/NoSuchAlgorithmException;->printStackTrace()V

    .line 139
    :cond_8e
    :goto_8e
    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_100

    .line 140
    iget-object v2, p0, Lcom/teamspeak/ts3client/data/c/b;->d:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f1

    .line 141
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 142
    iput v8, v0, Landroid/graphics/BitmapFactory$Options;->inTargetDensity:I

    .line 143
    iget-object v1, p0, Lcom/teamspeak/ts3client/data/c/b;->h:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 159
    :goto_af
    return-object v0

    .line 125
    :cond_b0
    :try_start_b0
    invoke-virtual {v3}, Ljava/security/MessageDigest;->digest()[B
    :try_end_b3
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_b0 .. :try_end_b3} :catch_87
    .catch Ljava/io/FileNotFoundException; {:try_start_b0 .. :try_end_b3} :catch_e1
    .catch Ljava/io/IOException; {:try_start_b0 .. :try_end_b3} :catch_e9

    move-result-object v4

    move v9, v0

    move-object v0, v2

    move v2, v9

    .line 126
    :goto_b7
    :try_start_b7
    array-length v3, v4

    if-ge v2, v3, :cond_8e

    .line 127
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-byte v5, v4, v2

    and-int/lit16 v5, v5, 0xff

    add-int/lit16 v5, v5, 0x100

    const/16 v6, 0x10

    invoke-static {v5, v6}, Ljava/lang/Integer;->toString(II)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_db
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_b7 .. :try_end_db} :catch_116
    .catch Ljava/io/FileNotFoundException; {:try_start_b7 .. :try_end_db} :catch_114
    .catch Ljava/io/IOException; {:try_start_b7 .. :try_end_db} :catch_112

    move-result-object v3

    .line 126
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move-object v0, v3

    goto :goto_b7

    .line 134
    :catch_e1
    move-exception v0

    move-object v9, v0

    move-object v0, v2

    move-object v2, v9

    :goto_e5
    invoke-virtual {v2}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_8e

    .line 137
    :catch_e9
    move-exception v0

    move-object v9, v0

    move-object v0, v2

    move-object v2, v9

    :goto_ed
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_8e

    .line 146
    :cond_f1
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/c/b;->h:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 147
    invoke-direct {p0}, Lcom/teamspeak/ts3client/data/c/b;->b()V

    move-object v0, v1

    .line 148
    goto :goto_af

    .line 153
    :cond_fb
    invoke-direct {p0}, Lcom/teamspeak/ts3client/data/c/b;->b()V

    move-object v0, v1

    .line 154
    goto :goto_af

    .line 156
    :cond_100
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 157
    iput v8, v0, Landroid/graphics/BitmapFactory$Options;->inTargetDensity:I

    .line 158
    iget-object v1, p0, Lcom/teamspeak/ts3client/data/c/b;->h:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_af

    .line 137
    :catch_112
    move-exception v2

    goto :goto_ed

    .line 134
    :catch_114
    move-exception v2

    goto :goto_e5

    .line 131
    :catch_116
    move-exception v2

    goto/16 :goto_8b
.end method

.method private varargs a([Lcom/teamspeak/ts3client/data/c;)Ljava/lang/Void;
    .registers 4

    .prologue
    .line 54
    const/4 v0, 0x0

    aget-object v0, p1, v0

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/c/b;->f:Lcom/teamspeak/ts3client/data/c;

    .line 55
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/c/b;->f:Lcom/teamspeak/ts3client/data/c;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/c;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/c/b;->d:Ljava/lang/String;

    .line 56
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 1061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 1234
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 56
    iget-object v1, p0, Lcom/teamspeak/ts3client/data/c/b;->f:Lcom/teamspeak/ts3client/data/c;

    .line 2188
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/c;->b:Ljava/lang/String;

    .line 56
    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_identityStringToFilename(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/c/b;->e:Ljava/lang/String;

    .line 57
    invoke-direct {p0}, Lcom/teamspeak/ts3client/data/c/b;->a()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 58
    if-eqz v0, :cond_2a

    .line 59
    iget-object v1, p0, Lcom/teamspeak/ts3client/data/c/b;->f:Lcom/teamspeak/ts3client/data/c;

    invoke-virtual {v1, v0}, Lcom/teamspeak/ts3client/data/c;->a(Landroid/graphics/Bitmap;)V

    .line 60
    :cond_2a
    const/4 v0, 0x0

    return-object v0
.end method

.method private b()V
    .registers 10

    .prologue
    .line 163
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 16085
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->d:Ljava/util/logging/Logger;

    .line 163
    sget-object v1, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Loading Avatar avatar_"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/teamspeak/ts3client/data/c/b;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " from Server"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 165
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 17061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 17238
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->c:Lcom/teamspeak/ts3client/jni/l;

    .line 165
    invoke-virtual {v0, p0}, Lcom/teamspeak/ts3client/jni/l;->a(Lcom/teamspeak/ts3client/data/w;)V

    .line 166
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 18061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 18234
    iget-object v1, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 166
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 19061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 19267
    iget-wide v2, v0, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 166
    const-wide/16 v4, 0x0

    const-string v6, ""

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v7, "/avatar_"

    invoke-direct {v0, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/teamspeak/ts3client/data/c/b;->e:Ljava/lang/String;

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v8, "Request info: /avatar_"

    invoke-direct {v0, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v8, p0, Lcom/teamspeak/ts3client/data/c/b;->e:Ljava/lang/String;

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual/range {v1 .. v8}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_requestFileInfo(JJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    .line 172
    return-void
.end method


# virtual methods
.method public final a(Lcom/teamspeak/ts3client/jni/k;)V
    .registers 14

    .prologue
    const-wide/16 v4, 0x0

    const/16 v6, 0xa0

    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 65
    instance-of v0, p1, Lcom/teamspeak/ts3client/jni/events/rare/FileTransferStatus;

    if-eqz v0, :cond_3f

    move-object v0, p1

    .line 66
    check-cast v0, Lcom/teamspeak/ts3client/jni/events/rare/FileTransferStatus;

    .line 3041
    iget-wide v0, v0, Lcom/teamspeak/ts3client/jni/events/rare/FileTransferStatus;->b:J

    .line 66
    const-wide/16 v2, 0x811

    cmp-long v0, v0, v2

    if-nez v0, :cond_3f

    move-object v0, p1

    check-cast v0, Lcom/teamspeak/ts3client/jni/events/rare/FileTransferStatus;

    .line 3049
    iget v0, v0, Lcom/teamspeak/ts3client/jni/events/rare/FileTransferStatus;->a:I

    .line 66
    iget v1, p0, Lcom/teamspeak/ts3client/data/c/b;->b:I

    if-ne v0, v1, :cond_3f

    .line 68
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 69
    iput v6, v0, Landroid/graphics/BitmapFactory$Options;->inTargetDensity:I

    .line 70
    iget-object v1, p0, Lcom/teamspeak/ts3client/data/c/b;->h:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 71
    iget-object v1, p0, Lcom/teamspeak/ts3client/data/c/b;->f:Lcom/teamspeak/ts3client/data/c;

    invoke-virtual {v1, v0}, Lcom/teamspeak/ts3client/data/c;->a(Landroid/graphics/Bitmap;)V

    .line 72
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 3061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 3238
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->c:Lcom/teamspeak/ts3client/jni/l;

    .line 72
    invoke-virtual {v0, p0}, Lcom/teamspeak/ts3client/jni/l;->b(Lcom/teamspeak/ts3client/data/w;)V

    .line 75
    :cond_3f
    instance-of v0, p1, Lcom/teamspeak/ts3client/jni/events/rare/FileInfo;

    if-eqz v0, :cond_e4

    move-object v0, p1

    .line 76
    check-cast v0, Lcom/teamspeak/ts3client/jni/events/rare/FileInfo;

    .line 4036
    iget-object v0, v0, Lcom/teamspeak/ts3client/jni/events/rare/FileInfo;->a:Ljava/lang/String;

    .line 76
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "/avatar_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/teamspeak/ts3client/data/c/b;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e4

    .line 78
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 4093
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 78
    const-string v1, "avatarlimit"

    const-string v2, "500"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 79
    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_17e

    .line 80
    const-string v0, "500"

    move-object v1, v0

    :goto_78
    move-object v0, p1

    .line 81
    check-cast v0, Lcom/teamspeak/ts3client/jni/events/rare/FileInfo;

    .line 5040
    iget-wide v2, v0, Lcom/teamspeak/ts3client/jni/events/rare/FileInfo;->b:J

    .line 81
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    mul-int/lit16 v0, v0, 0x3e8

    int-to-long v0, v0

    cmp-long v0, v2, v0

    if-ltz v0, :cond_98

    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 5069
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->h:Landroid/net/ConnectivityManager;

    .line 81
    invoke-virtual {v0, v8}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_17b

    :cond_98
    move v0, v8

    .line 83
    :goto_99
    if-eqz v0, :cond_153

    .line 84
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 6061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 6238
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->c:Lcom/teamspeak/ts3client/jni/l;

    .line 84
    invoke-virtual {v0, p0}, Lcom/teamspeak/ts3client/jni/l;->a(Lcom/teamspeak/ts3client/data/w;)V

    .line 85
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 7061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 7234
    iget-object v1, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 85
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 8061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 8267
    iget-wide v2, v0, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 85
    const-string v6, ""

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v7, "/avatar_"

    invoke-direct {v0, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/teamspeak/ts3client/data/c/b;->e:Ljava/lang/String;

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    iget-object v10, p0, Lcom/teamspeak/ts3client/data/c/b;->g:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v11, "avatar_"

    invoke-direct {v0, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v11, p0, Lcom/teamspeak/ts3client/data/c/b;->f:Lcom/teamspeak/ts3client/data/c;

    .line 9235
    iget v11, v11, Lcom/teamspeak/ts3client/data/c;->c:I

    .line 85
    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual/range {v1 .. v11}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_requestFile(JJLjava/lang/String;Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/teamspeak/ts3client/data/c/b;->b:I

    .line 95
    :cond_e4
    :goto_e4
    instance-of v0, p1, Lcom/teamspeak/ts3client/jni/events/ServerError;

    if-eqz v0, :cond_152

    .line 96
    check-cast p1, Lcom/teamspeak/ts3client/jni/events/ServerError;

    .line 11029
    iget v0, p1, Lcom/teamspeak/ts3client/jni/events/ServerError;->b:I

    .line 98
    const/16 v1, 0x300

    if-ne v0, v1, :cond_152

    .line 11041
    iget-object v0, p1, Lcom/teamspeak/ts3client/jni/events/ServerError;->c:Ljava/lang/String;

    .line 98
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Request info: /avatar_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/teamspeak/ts3client/data/c/b;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_152

    .line 99
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 11061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 11238
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->c:Lcom/teamspeak/ts3client/jni/l;

    .line 99
    invoke-virtual {v0, p0}, Lcom/teamspeak/ts3client/jni/l;->a(Lcom/teamspeak/ts3client/data/w;)V

    .line 100
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 12061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 12234
    iget-object v1, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 100
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 13061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 13267
    iget-wide v2, v0, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 100
    const-string v6, ""

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v7, "/avatar_"

    invoke-direct {v0, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/teamspeak/ts3client/data/c/b;->e:Ljava/lang/String;

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    iget-object v10, p0, Lcom/teamspeak/ts3client/data/c/b;->g:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v11, "avatar_"

    invoke-direct {v0, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v11, p0, Lcom/teamspeak/ts3client/data/c/b;->f:Lcom/teamspeak/ts3client/data/c;

    .line 14235
    iget v11, v11, Lcom/teamspeak/ts3client/data/c;->c:I

    .line 100
    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual/range {v1 .. v11}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_requestFile(JJLjava/lang/String;Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/teamspeak/ts3client/data/c/b;->b:I

    .line 103
    :cond_152
    return-void

    .line 87
    :cond_153
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 88
    iput v6, v0, Landroid/graphics/BitmapFactory$Options;->inTargetDensity:I

    .line 89
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020092

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 90
    iget-object v1, p0, Lcom/teamspeak/ts3client/data/c/b;->f:Lcom/teamspeak/ts3client/data/c;

    invoke-virtual {v1, v0}, Lcom/teamspeak/ts3client/data/c;->a(Landroid/graphics/Bitmap;)V

    .line 91
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 10061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 10238
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->c:Lcom/teamspeak/ts3client/jni/l;

    .line 91
    invoke-virtual {v0, p0}, Lcom/teamspeak/ts3client/jni/l;->b(Lcom/teamspeak/ts3client/data/w;)V

    goto/16 :goto_e4

    :cond_17b
    move v0, v9

    goto/16 :goto_99

    :cond_17e
    move-object v1, v0

    goto/16 :goto_78
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 42
    check-cast p1, [Lcom/teamspeak/ts3client/data/c;

    .line 20054
    const/4 v0, 0x0

    aget-object v0, p1, v0

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/c/b;->f:Lcom/teamspeak/ts3client/data/c;

    .line 20055
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/c/b;->f:Lcom/teamspeak/ts3client/data/c;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/c;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/c/b;->d:Ljava/lang/String;

    .line 20056
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 20061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 20234
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 20056
    iget-object v1, p0, Lcom/teamspeak/ts3client/data/c/b;->f:Lcom/teamspeak/ts3client/data/c;

    .line 21188
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/c;->b:Ljava/lang/String;

    .line 20056
    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_identityStringToFilename(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/c/b;->e:Ljava/lang/String;

    .line 20057
    invoke-direct {p0}, Lcom/teamspeak/ts3client/data/c/b;->a()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 20058
    if-eqz v0, :cond_2c

    .line 20059
    iget-object v1, p0, Lcom/teamspeak/ts3client/data/c/b;->f:Lcom/teamspeak/ts3client/data/c;

    invoke-virtual {v1, v0}, Lcom/teamspeak/ts3client/data/c;->a(Landroid/graphics/Bitmap;)V

    .line 20060
    :cond_2c
    const/4 v0, 0x0

    .line 42
    return-object v0
.end method
