.class public final Lcom/teamspeak/ts3client/data/ah;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/teamspeak/ts3client/data/af;


# direct methods
.method private constructor <init>(Lcom/teamspeak/ts3client/data/af;)V
    .registers 2

    .prologue
    .line 64
    iput-object p1, p0, Lcom/teamspeak/ts3client/data/ah;->a:Lcom/teamspeak/ts3client/data/af;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/teamspeak/ts3client/data/af;B)V
    .registers 3

    .prologue
    .line 64
    invoke-direct {p0, p1}, Lcom/teamspeak/ts3client/data/ah;-><init>(Lcom/teamspeak/ts3client/data/af;)V

    return-void
.end method

.method private static varargs a([Ljava/lang/String;)Ljava/nio/ByteBuffer;
    .registers 7

    .prologue
    .line 70
    :try_start_0
    new-instance v0, Ljava/net/URL;

    const/4 v1, 0x0

    aget-object v1, p0, v1

    invoke-direct {v0, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 71
    invoke-virtual {v0}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljava/net/HttpURLConnection;

    .line 72
    const/16 v1, 0x9c4

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    .line 73
    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v1

    .line 76
    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 77
    const/16 v3, 0x400

    new-array v3, v3, [B

    .line 79
    :goto_20
    const/4 v4, 0x0

    const/16 v5, 0x400

    invoke-virtual {v1, v3, v4, v5}, Ljava/io/InputStream;->read([BII)I

    move-result v4

    const/4 v5, -0x1

    if-eq v4, v5, :cond_32

    .line 80
    const/4 v5, 0x0

    invoke-virtual {v2, v3, v5, v4}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    goto :goto_20

    .line 86
    :catch_2f
    move-exception v0

    const/4 v0, 0x0

    .line 88
    :goto_31
    return-object v0

    .line 82
    :cond_32
    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getContentLength()I

    move-result v1

    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 83
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getContentLength()I

    move-result v4

    invoke-virtual {v1, v2, v3, v4}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;

    .line 84
    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_49
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_49} :catch_2f

    move-object v0, v1

    .line 88
    goto :goto_31
.end method

.method private a(Ljava/nio/ByteBuffer;)V
    .registers 8

    .prologue
    const/4 v3, 0x0

    .line 93
    if-eqz p1, :cond_6c

    .line 94
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/ah;->a:Lcom/teamspeak/ts3client/data/af;

    invoke-static {}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->b()Lcom/teamspeak/ts3client/jni/Ts3Jni;

    move-result-object v1

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->limit()I

    move-result v2

    invoke-virtual {v1, p1, v2}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_android_parseProtobufDataResult(Ljava/nio/ByteBuffer;I)Z

    move-result v1

    .line 1017
    iput-boolean v1, v0, Lcom/teamspeak/ts3client/data/af;->c:Z

    .line 95
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/ah;->a:Lcom/teamspeak/ts3client/data/af;

    .line 2017
    iget-boolean v0, v0, Lcom/teamspeak/ts3client/data/af;->c:Z

    .line 95
    if-eqz v0, :cond_59

    .line 96
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/ah;->a:Lcom/teamspeak/ts3client/data/af;

    invoke-static {}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->b()Lcom/teamspeak/ts3client/jni/Ts3Jni;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_android_getLicenseAgreementVersion()I

    move-result v1

    .line 3017
    iput v1, v0, Lcom/teamspeak/ts3client/data/af;->a:I

    .line 97
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/ah;->a:Lcom/teamspeak/ts3client/data/af;

    .line 4017
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/af;->d:Lcom/teamspeak/ts3client/data/z;

    .line 97
    if-eqz v0, :cond_54

    .line 98
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 99
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v1

    .line 4093
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 99
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "la_lastcheck"

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    invoke-interface {v1, v2, v4, v5}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 100
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/ah;->a:Lcom/teamspeak/ts3client/data/af;

    .line 5017
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/af;->d:Lcom/teamspeak/ts3client/data/z;

    .line 100
    invoke-interface {v0}, Lcom/teamspeak/ts3client/data/z;->m()V

    .line 101
    const-string v0, "UpdateServerData"

    const-string v1, "Downloaded new update info"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    :cond_54
    :goto_54
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/ah;->a:Lcom/teamspeak/ts3client/data/af;

    .line 10017
    iput-boolean v3, v0, Lcom/teamspeak/ts3client/data/af;->e:Z

    .line 114
    return-void

    .line 104
    :cond_59
    const-string v0, "UpdateServerData"

    const-string v1, "Downloaded corrupt data, skipping..."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/ah;->a:Lcom/teamspeak/ts3client/data/af;

    .line 6017
    iput-boolean v3, v0, Lcom/teamspeak/ts3client/data/af;->c:Z

    .line 106
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/ah;->a:Lcom/teamspeak/ts3client/data/af;

    .line 7017
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/af;->d:Lcom/teamspeak/ts3client/data/z;

    .line 106
    invoke-interface {v0}, Lcom/teamspeak/ts3client/data/z;->o()V

    goto :goto_54

    .line 109
    :cond_6c
    const-string v0, "UpdateServerData"

    const-string v1, "Failed to download new update info"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 110
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/ah;->a:Lcom/teamspeak/ts3client/data/af;

    .line 8017
    iput-boolean v3, v0, Lcom/teamspeak/ts3client/data/af;->c:Z

    .line 111
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/ah;->a:Lcom/teamspeak/ts3client/data/af;

    .line 9017
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/af;->d:Lcom/teamspeak/ts3client/data/z;

    .line 111
    invoke-interface {v0}, Lcom/teamspeak/ts3client/data/z;->o()V

    goto :goto_54
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 64
    check-cast p1, [Ljava/lang/String;

    invoke-static {p1}, Lcom/teamspeak/ts3client/data/ah;->a([Ljava/lang/String;)Ljava/nio/ByteBuffer;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .registers 8

    .prologue
    const/4 v3, 0x0

    .line 64
    check-cast p1, Ljava/nio/ByteBuffer;

    .line 10093
    if-eqz p1, :cond_6e

    .line 10094
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/ah;->a:Lcom/teamspeak/ts3client/data/af;

    invoke-static {}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->b()Lcom/teamspeak/ts3client/jni/Ts3Jni;

    move-result-object v1

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->limit()I

    move-result v2

    invoke-virtual {v1, p1, v2}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_android_parseProtobufDataResult(Ljava/nio/ByteBuffer;I)Z

    move-result v1

    .line 11017
    iput-boolean v1, v0, Lcom/teamspeak/ts3client/data/af;->c:Z

    .line 10095
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/ah;->a:Lcom/teamspeak/ts3client/data/af;

    .line 12017
    iget-boolean v0, v0, Lcom/teamspeak/ts3client/data/af;->c:Z

    .line 10095
    if-eqz v0, :cond_5b

    .line 10096
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/ah;->a:Lcom/teamspeak/ts3client/data/af;

    invoke-static {}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->b()Lcom/teamspeak/ts3client/jni/Ts3Jni;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_android_getLicenseAgreementVersion()I

    move-result v1

    .line 13017
    iput v1, v0, Lcom/teamspeak/ts3client/data/af;->a:I

    .line 10097
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/ah;->a:Lcom/teamspeak/ts3client/data/af;

    .line 14017
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/af;->d:Lcom/teamspeak/ts3client/data/z;

    .line 10097
    if-eqz v0, :cond_56

    .line 10098
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 10099
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v1

    .line 14093
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 10099
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "la_lastcheck"

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    invoke-interface {v1, v2, v4, v5}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 10100
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/ah;->a:Lcom/teamspeak/ts3client/data/af;

    .line 15017
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/af;->d:Lcom/teamspeak/ts3client/data/z;

    .line 10100
    invoke-interface {v0}, Lcom/teamspeak/ts3client/data/z;->m()V

    .line 10101
    const-string v0, "UpdateServerData"

    const-string v1, "Downloaded new update info"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 10113
    :cond_56
    :goto_56
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/ah;->a:Lcom/teamspeak/ts3client/data/af;

    .line 20017
    iput-boolean v3, v0, Lcom/teamspeak/ts3client/data/af;->e:Z

    .line 64
    return-void

    .line 10104
    :cond_5b
    const-string v0, "UpdateServerData"

    const-string v1, "Downloaded corrupt data, skipping..."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 10105
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/ah;->a:Lcom/teamspeak/ts3client/data/af;

    .line 16017
    iput-boolean v3, v0, Lcom/teamspeak/ts3client/data/af;->c:Z

    .line 10106
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/ah;->a:Lcom/teamspeak/ts3client/data/af;

    .line 17017
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/af;->d:Lcom/teamspeak/ts3client/data/z;

    .line 10106
    invoke-interface {v0}, Lcom/teamspeak/ts3client/data/z;->o()V

    goto :goto_56

    .line 10109
    :cond_6e
    const-string v0, "UpdateServerData"

    const-string v1, "Failed to download new update info"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 10110
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/ah;->a:Lcom/teamspeak/ts3client/data/af;

    .line 18017
    iput-boolean v3, v0, Lcom/teamspeak/ts3client/data/af;->c:Z

    .line 10111
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/ah;->a:Lcom/teamspeak/ts3client/data/af;

    .line 19017
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/af;->d:Lcom/teamspeak/ts3client/data/z;

    .line 10111
    invoke-interface {v0}, Lcom/teamspeak/ts3client/data/z;->o()V

    goto :goto_56
.end method
