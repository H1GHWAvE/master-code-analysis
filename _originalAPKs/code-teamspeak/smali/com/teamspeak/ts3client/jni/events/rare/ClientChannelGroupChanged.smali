.class public Lcom/teamspeak/ts3client/jni/events/rare/ClientChannelGroupChanged;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/teamspeak/ts3client/jni/k;


# instance fields
.field private a:J

.field private b:J

.field private c:J

.field private d:I

.field private e:I

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    return-void
.end method

.method private constructor <init>(JJJIILjava/lang/String;Ljava/lang/String;)V
    .registers 12

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-wide p1, p0, Lcom/teamspeak/ts3client/jni/events/rare/ClientChannelGroupChanged;->a:J

    .line 23
    iput-wide p3, p0, Lcom/teamspeak/ts3client/jni/events/rare/ClientChannelGroupChanged;->b:J

    .line 24
    iput-wide p5, p0, Lcom/teamspeak/ts3client/jni/events/rare/ClientChannelGroupChanged;->c:J

    .line 25
    iput p7, p0, Lcom/teamspeak/ts3client/jni/events/rare/ClientChannelGroupChanged;->d:I

    .line 26
    iput p8, p0, Lcom/teamspeak/ts3client/jni/events/rare/ClientChannelGroupChanged;->e:I

    .line 27
    iput-object p9, p0, Lcom/teamspeak/ts3client/jni/events/rare/ClientChannelGroupChanged;->f:Ljava/lang/String;

    .line 28
    iput-object p10, p0, Lcom/teamspeak/ts3client/jni/events/rare/ClientChannelGroupChanged;->g:Ljava/lang/String;

    .line 29
    invoke-static {p0}, Lcom/teamspeak/ts3client/jni/l;->a(Lcom/teamspeak/ts3client/jni/k;)V

    .line 30
    return-void
.end method

.method private e()J
    .registers 3

    .prologue
    .line 37
    iget-wide v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/ClientChannelGroupChanged;->c:J

    return-wide v0
.end method

.method private f()Ljava/lang/String;
    .registers 2

    .prologue
    .line 53
    iget-object v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/ClientChannelGroupChanged;->g:Ljava/lang/String;

    return-object v0
.end method

.method private g()J
    .registers 3

    .prologue
    .line 57
    iget-wide v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/ClientChannelGroupChanged;->a:J

    return-wide v0
.end method


# virtual methods
.method public final a()J
    .registers 3

    .prologue
    .line 33
    iget-wide v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/ClientChannelGroupChanged;->b:J

    return-wide v0
.end method

.method public final b()I
    .registers 2

    .prologue
    .line 41
    iget v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/ClientChannelGroupChanged;->d:I

    return v0
.end method

.method public final c()I
    .registers 2

    .prologue
    .line 45
    iget v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/ClientChannelGroupChanged;->e:I

    return v0
.end method

.method public final d()Ljava/lang/String;
    .registers 2

    .prologue
    .line 49
    iget-object v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/ClientChannelGroupChanged;->f:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .registers 5

    .prologue
    .line 62
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ClientChannelGroupChanged [serverConnectionHandlerID="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, Lcom/teamspeak/ts3client/jni/events/rare/ClientChannelGroupChanged;->a:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", channelGroupID="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/teamspeak/ts3client/jni/events/rare/ClientChannelGroupChanged;->b:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", channelID="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/teamspeak/ts3client/jni/events/rare/ClientChannelGroupChanged;->c:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", clientID="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/teamspeak/ts3client/jni/events/rare/ClientChannelGroupChanged;->d:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", invokerClientID="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/teamspeak/ts3client/jni/events/rare/ClientChannelGroupChanged;->e:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", invokerName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/jni/events/rare/ClientChannelGroupChanged;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", invokerUniqueIdentity="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/jni/events/rare/ClientChannelGroupChanged;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
