.class public Lcom/teamspeak/ts3client/jni/events/rare/BanList;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/teamspeak/ts3client/jni/k;


# instance fields
.field public a:J

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:J

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/String;

.field public j:Ljava/lang/String;

.field public k:Ljava/lang/String;

.field private l:J

.field private m:J

.field private n:J

.field private o:Ljava/lang/String;

.field private p:I


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    return-void
.end method

.method private constructor <init>(JJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;JJLjava/lang/String;JLjava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
    .registers 27

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-wide p1, p0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->l:J

    .line 35
    iput-wide p3, p0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->a:J

    .line 36
    iput-object p5, p0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->b:Ljava/lang/String;

    .line 37
    iput-object p6, p0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->c:Ljava/lang/String;

    .line 38
    iput-object p7, p0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->d:Ljava/lang/String;

    .line 39
    move-wide/from16 v0, p8

    iput-wide v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->m:J

    .line 40
    move-wide/from16 v0, p10

    iput-wide v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->n:J

    .line 41
    move-object/from16 v0, p12

    iput-object v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->e:Ljava/lang/String;

    .line 42
    move-wide/from16 v0, p13

    iput-wide v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->f:J

    .line 43
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->o:Ljava/lang/String;

    .line 44
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->g:Ljava/lang/String;

    .line 45
    move/from16 v0, p17

    iput v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->p:I

    .line 46
    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->h:Ljava/lang/String;

    .line 47
    const-wide/16 v2, 0x0

    cmp-long v2, p10, v2

    if-eqz v2, :cond_115

    .line 48
    const/4 v2, 0x3

    const/4 v3, 0x3

    invoke-static {v2, v3}, Ljava/text/DateFormat;->getDateTimeInstance(II)Ljava/text/DateFormat;

    move-result-object v2

    new-instance v3, Ljava/util/Date;

    add-long v4, p10, p8

    const-wide/16 v6, 0x3e8

    mul-long/2addr v4, v6

    invoke-direct {v3, v4, v5}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v2, v3}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->j:Ljava/lang/String;

    .line 51
    :goto_49
    const/4 v2, 0x3

    const/4 v3, 0x3

    invoke-static {v2, v3}, Ljava/text/DateFormat;->getDateTimeInstance(II)Ljava/text/DateFormat;

    move-result-object v2

    new-instance v3, Ljava/util/Date;

    const-wide/16 v4, 0x3e8

    mul-long v4, v4, p8

    invoke-direct {v3, v4, v5}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v2, v3}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->k:Ljava/lang/String;

    .line 1061
    const-string v2, ""

    iput-object v2, p0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->i:Ljava/lang/String;

    .line 1062
    iget-object v2, p0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->b:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_85

    .line 1063
    iget-object v2, p0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->i:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "ip="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->b:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->i:Ljava/lang/String;

    .line 1064
    :cond_85
    iget-object v2, p0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->c:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a8

    .line 1065
    iget-object v2, p0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->i:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "name="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->c:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->i:Ljava/lang/String;

    .line 1066
    :cond_a8
    iget-object v2, p0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->d:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_cb

    .line 1067
    iget-object v2, p0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->i:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "uid"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->d:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->i:Ljava/lang/String;

    .line 1068
    :cond_cb
    iget-object v2, p0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->h:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_df

    .line 1069
    iget-object v2, p0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->i:Ljava/lang/String;

    iget-object v3, p0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->h:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->i:Ljava/lang/String;

    .line 1070
    :cond_df
    iget-object v2, p0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->g:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f3

    .line 1071
    iget-object v2, p0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->i:Ljava/lang/String;

    iget-object v3, p0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->g:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->i:Ljava/lang/String;

    .line 1072
    :cond_f3
    iget-object v2, p0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->i:Ljava/lang/String;

    .line 1130
    iget-object v3, p0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->j:Ljava/lang/String;

    .line 1072
    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->i:Ljava/lang/String;

    .line 1073
    iget-object v2, p0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->i:Ljava/lang/String;

    .line 1134
    iget-object v3, p0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->k:Ljava/lang/String;

    .line 1073
    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->i:Ljava/lang/String;

    .line 1074
    iget-object v2, p0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->i:Ljava/lang/String;

    iget-object v3, p0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->i:Ljava/lang/String;

    .line 53
    invoke-static {p0}, Lcom/teamspeak/ts3client/jni/l;->a(Lcom/teamspeak/ts3client/jni/k;)V

    .line 54
    return-void

    .line 50
    :cond_115
    const-string v2, "---"

    iput-object v2, p0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->j:Ljava/lang/String;

    goto/16 :goto_49
.end method

.method private a()Ljava/lang/String;
    .registers 2

    .prologue
    .line 57
    iget-object v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->i:Ljava/lang/String;

    return-object v0
.end method

.method private b()V
    .registers 4

    .prologue
    .line 61
    const-string v0, ""

    iput-object v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->i:Ljava/lang/String;

    .line 62
    iget-object v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_27

    .line 63
    iget-object v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->i:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ip="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->i:Ljava/lang/String;

    .line 64
    :cond_27
    iget-object v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->c:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4a

    .line 65
    iget-object v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->i:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "name="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->i:Ljava/lang/String;

    .line 66
    :cond_4a
    iget-object v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->d:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6d

    .line 67
    iget-object v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->i:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "uid"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->i:Ljava/lang/String;

    .line 68
    :cond_6d
    iget-object v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->h:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_81

    .line 69
    iget-object v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->i:Ljava/lang/String;

    iget-object v1, p0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->i:Ljava/lang/String;

    .line 70
    :cond_81
    iget-object v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->g:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_95

    .line 71
    iget-object v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->i:Ljava/lang/String;

    iget-object v1, p0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->i:Ljava/lang/String;

    .line 72
    :cond_95
    iget-object v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->i:Ljava/lang/String;

    .line 2130
    iget-object v1, p0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->j:Ljava/lang/String;

    .line 72
    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->i:Ljava/lang/String;

    .line 73
    iget-object v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->i:Ljava/lang/String;

    .line 2134
    iget-object v1, p0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->k:Ljava/lang/String;

    .line 73
    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->i:Ljava/lang/String;

    .line 74
    iget-object v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->i:Ljava/lang/String;

    iget-object v1, p0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->i:Ljava/lang/String;

    .line 75
    return-void
.end method

.method private c()J
    .registers 3

    .prologue
    .line 78
    iget-wide v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->l:J

    return-wide v0
.end method

.method private d()J
    .registers 3

    .prologue
    .line 82
    iget-wide v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->a:J

    return-wide v0
.end method

.method private e()Ljava/lang/String;
    .registers 2

    .prologue
    .line 86
    iget-object v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->b:Ljava/lang/String;

    return-object v0
.end method

.method private f()Ljava/lang/String;
    .registers 2

    .prologue
    .line 90
    iget-object v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->c:Ljava/lang/String;

    return-object v0
.end method

.method private g()Ljava/lang/String;
    .registers 2

    .prologue
    .line 94
    iget-object v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->d:Ljava/lang/String;

    return-object v0
.end method

.method private h()J
    .registers 3

    .prologue
    .line 98
    iget-wide v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->m:J

    return-wide v0
.end method

.method private i()J
    .registers 3

    .prologue
    .line 102
    iget-wide v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->n:J

    return-wide v0
.end method

.method private j()Ljava/lang/String;
    .registers 2

    .prologue
    .line 106
    iget-object v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->e:Ljava/lang/String;

    return-object v0
.end method

.method private k()J
    .registers 3

    .prologue
    .line 110
    iget-wide v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->f:J

    return-wide v0
.end method

.method private l()Ljava/lang/String;
    .registers 2

    .prologue
    .line 114
    iget-object v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->o:Ljava/lang/String;

    return-object v0
.end method

.method private m()Ljava/lang/String;
    .registers 2

    .prologue
    .line 118
    iget-object v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->g:Ljava/lang/String;

    return-object v0
.end method

.method private n()I
    .registers 2

    .prologue
    .line 122
    iget v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->p:I

    return v0
.end method

.method private o()Ljava/lang/String;
    .registers 2

    .prologue
    .line 126
    iget-object v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->h:Ljava/lang/String;

    return-object v0
.end method

.method private p()Ljava/lang/String;
    .registers 2

    .prologue
    .line 130
    iget-object v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->j:Ljava/lang/String;

    return-object v0
.end method

.method private q()Ljava/lang/String;
    .registers 2

    .prologue
    .line 134
    iget-object v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->k:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .registers 5

    .prologue
    .line 139
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "BanList [serverConnectionHandlerID="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->l:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", banid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->a:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", ip="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", uid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", creationTime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->m:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", durationTime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->n:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", invokerName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", invokercldbid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->f:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", invokeruid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->o:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", reason="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", numberOfEnforcements="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->p:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", lastNickName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
