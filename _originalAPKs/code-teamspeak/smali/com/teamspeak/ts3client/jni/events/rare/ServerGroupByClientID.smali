.class public Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupByClientID;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/teamspeak/ts3client/jni/k;


# instance fields
.field private a:J

.field private b:Ljava/lang/String;

.field private c:J

.field private d:J


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    return-void
.end method

.method private constructor <init>(JLjava/lang/String;JJ)V
    .registers 8

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-wide p1, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupByClientID;->a:J

    .line 17
    iput-object p3, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupByClientID;->b:Ljava/lang/String;

    .line 18
    iput-wide p4, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupByClientID;->c:J

    .line 19
    iput-wide p6, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupByClientID;->d:J

    .line 20
    invoke-static {p0}, Lcom/teamspeak/ts3client/jni/l;->a(Lcom/teamspeak/ts3client/jni/k;)V

    .line 21
    return-void
.end method

.method private a()J
    .registers 3

    .prologue
    .line 24
    iget-wide v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupByClientID;->a:J

    return-wide v0
.end method

.method private b()Ljava/lang/String;
    .registers 2

    .prologue
    .line 28
    iget-object v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupByClientID;->b:Ljava/lang/String;

    return-object v0
.end method

.method private c()J
    .registers 3

    .prologue
    .line 32
    iget-wide v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupByClientID;->c:J

    return-wide v0
.end method

.method private d()J
    .registers 3

    .prologue
    .line 36
    iget-wide v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupByClientID;->d:J

    return-wide v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .registers 5

    .prologue
    .line 41
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ServerGroupByClientID [serverConnectionHandlerID="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupByClientID;->a:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupByClientID;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", serverGroupID="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupByClientID;->c:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", clientDatabaseID="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerGroupByClientID;->d:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
