.class public Lcom/teamspeak/ts3client/jni/events/NewChannel;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/teamspeak/ts3client/jni/k;


# instance fields
.field a:J

.field b:J

.field c:J


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    return-void
.end method

.method private constructor <init>(JJJ)V
    .registers 8

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-wide p1, p0, Lcom/teamspeak/ts3client/jni/events/NewChannel;->a:J

    .line 18
    iput-wide p3, p0, Lcom/teamspeak/ts3client/jni/events/NewChannel;->b:J

    .line 19
    iput-wide p5, p0, Lcom/teamspeak/ts3client/jni/events/NewChannel;->c:J

    .line 20
    invoke-static {p0}, Lcom/teamspeak/ts3client/jni/l;->a(Lcom/teamspeak/ts3client/jni/k;)V

    .line 21
    return-void
.end method

.method private a()J
    .registers 3

    .prologue
    .line 24
    iget-wide v0, p0, Lcom/teamspeak/ts3client/jni/events/NewChannel;->b:J

    return-wide v0
.end method

.method private b()J
    .registers 3

    .prologue
    .line 28
    iget-wide v0, p0, Lcom/teamspeak/ts3client/jni/events/NewChannel;->c:J

    return-wide v0
.end method

.method private c()J
    .registers 3

    .prologue
    .line 32
    iget-wide v0, p0, Lcom/teamspeak/ts3client/jni/events/NewChannel;->a:J

    return-wide v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .registers 5

    .prologue
    .line 37
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "NewChannel [serverConnectionHandlerID="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, Lcom/teamspeak/ts3client/jni/events/NewChannel;->a:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", channelID="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/teamspeak/ts3client/jni/events/NewChannel;->b:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", channelParentID="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/teamspeak/ts3client/jni/events/NewChannel;->c:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
