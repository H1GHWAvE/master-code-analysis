.class public Lcom/teamspeak/ts3client/jni/events/rare/ComplainList;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/teamspeak/ts3client/jni/k;


# instance fields
.field private a:J

.field private b:J

.field private c:Ljava/lang/String;

.field private d:J

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:J


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    return-void
.end method

.method private constructor <init>(JJLjava/lang/String;JLjava/lang/String;Ljava/lang/String;J)V
    .registers 12

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-wide p1, p0, Lcom/teamspeak/ts3client/jni/events/rare/ComplainList;->a:J

    .line 24
    iput-wide p3, p0, Lcom/teamspeak/ts3client/jni/events/rare/ComplainList;->b:J

    .line 25
    iput-object p5, p0, Lcom/teamspeak/ts3client/jni/events/rare/ComplainList;->c:Ljava/lang/String;

    .line 26
    iput-wide p6, p0, Lcom/teamspeak/ts3client/jni/events/rare/ComplainList;->d:J

    .line 27
    iput-object p8, p0, Lcom/teamspeak/ts3client/jni/events/rare/ComplainList;->e:Ljava/lang/String;

    .line 28
    iput-object p9, p0, Lcom/teamspeak/ts3client/jni/events/rare/ComplainList;->f:Ljava/lang/String;

    .line 29
    iput-wide p10, p0, Lcom/teamspeak/ts3client/jni/events/rare/ComplainList;->g:J

    .line 30
    invoke-static {p0}, Lcom/teamspeak/ts3client/jni/l;->a(Lcom/teamspeak/ts3client/jni/k;)V

    .line 31
    return-void
.end method

.method private a()Ljava/lang/String;
    .registers 2

    .prologue
    .line 34
    iget-object v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/ComplainList;->f:Ljava/lang/String;

    return-object v0
.end method

.method private b()J
    .registers 3

    .prologue
    .line 38
    iget-wide v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/ComplainList;->d:J

    return-wide v0
.end method

.method private c()Ljava/lang/String;
    .registers 2

    .prologue
    .line 42
    iget-object v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/ComplainList;->e:Ljava/lang/String;

    return-object v0
.end method

.method private d()J
    .registers 3

    .prologue
    .line 46
    iget-wide v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/ComplainList;->a:J

    return-wide v0
.end method

.method private e()J
    .registers 3

    .prologue
    .line 50
    iget-wide v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/ComplainList;->b:J

    return-wide v0
.end method

.method private f()Ljava/lang/String;
    .registers 2

    .prologue
    .line 54
    iget-object v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/ComplainList;->c:Ljava/lang/String;

    return-object v0
.end method

.method private g()J
    .registers 3

    .prologue
    .line 58
    iget-wide v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/ComplainList;->g:J

    return-wide v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .registers 5

    .prologue
    .line 63
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ComplainList [serverConnectionHandlerID="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, Lcom/teamspeak/ts3client/jni/events/rare/ComplainList;->a:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", targetClientDatabaseID="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/teamspeak/ts3client/jni/events/rare/ComplainList;->b:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", targetClientNickName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/jni/events/rare/ComplainList;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", fromClientDatabaseID="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/teamspeak/ts3client/jni/events/rare/ComplainList;->d:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", fromClientNickName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/jni/events/rare/ComplainList;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", complainReason="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/jni/events/rare/ComplainList;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", timestamp="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/teamspeak/ts3client/jni/events/rare/ComplainList;->g:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
