.class public Lcom/teamspeak/ts3client/jni/events/rare/ServerPermissionError;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/teamspeak/ts3client/jni/k;


# instance fields
.field public a:I

.field private b:Ljava/lang/String;

.field private c:I

.field private d:J


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    return-void
.end method

.method private constructor <init>(JLjava/lang/String;II)V
    .registers 7

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object p3, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerPermissionError;->b:Ljava/lang/String;

    .line 18
    iput p4, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerPermissionError;->c:I

    .line 19
    iput p5, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerPermissionError;->a:I

    .line 20
    iput-wide p1, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerPermissionError;->d:J

    .line 21
    invoke-static {p0}, Lcom/teamspeak/ts3client/jni/l;->a(Lcom/teamspeak/ts3client/jni/k;)V

    .line 22
    return-void
.end method

.method private d()J
    .registers 3

    .prologue
    .line 37
    iget-wide v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerPermissionError;->d:J

    return-wide v0
.end method


# virtual methods
.method public final a()I
    .registers 2

    .prologue
    .line 25
    iget v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerPermissionError;->c:I

    return v0
.end method

.method public final b()Ljava/lang/String;
    .registers 2

    .prologue
    .line 29
    iget-object v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerPermissionError;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final c()I
    .registers 2

    .prologue
    .line 33
    iget v0, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerPermissionError;->a:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 5

    .prologue
    .line 42
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ServerPermissionError [errorMessage="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerPermissionError;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", error="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerPermissionError;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", failedPermissionID="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerPermissionError;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", serverConnectionHandlerID="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/teamspeak/ts3client/jni/events/rare/ServerPermissionError;->d:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
