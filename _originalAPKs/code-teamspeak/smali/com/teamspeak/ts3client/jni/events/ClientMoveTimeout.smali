.class public Lcom/teamspeak/ts3client/jni/events/ClientMoveTimeout;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/teamspeak/ts3client/jni/k;


# instance fields
.field private a:J

.field private b:I

.field private c:J

.field private d:J

.field private e:Lcom/teamspeak/ts3client/jni/j;

.field private f:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    return-void
.end method

.method private constructor <init>(JIJJILjava/lang/String;)V
    .registers 12

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-wide p1, p0, Lcom/teamspeak/ts3client/jni/events/ClientMoveTimeout;->a:J

    .line 24
    iput p3, p0, Lcom/teamspeak/ts3client/jni/events/ClientMoveTimeout;->b:I

    .line 25
    iput-wide p4, p0, Lcom/teamspeak/ts3client/jni/events/ClientMoveTimeout;->c:J

    .line 26
    iput-wide p6, p0, Lcom/teamspeak/ts3client/jni/events/ClientMoveTimeout;->d:J

    .line 27
    if-nez p8, :cond_11

    .line 28
    sget-object v0, Lcom/teamspeak/ts3client/jni/j;->a:Lcom/teamspeak/ts3client/jni/j;

    iput-object v0, p0, Lcom/teamspeak/ts3client/jni/events/ClientMoveTimeout;->e:Lcom/teamspeak/ts3client/jni/j;

    .line 29
    :cond_11
    const/4 v0, 0x1

    if-ne p8, v0, :cond_18

    .line 30
    sget-object v0, Lcom/teamspeak/ts3client/jni/j;->b:Lcom/teamspeak/ts3client/jni/j;

    iput-object v0, p0, Lcom/teamspeak/ts3client/jni/events/ClientMoveTimeout;->e:Lcom/teamspeak/ts3client/jni/j;

    .line 31
    :cond_18
    const/4 v0, 0x2

    if-ne p8, v0, :cond_1f

    .line 32
    sget-object v0, Lcom/teamspeak/ts3client/jni/j;->c:Lcom/teamspeak/ts3client/jni/j;

    iput-object v0, p0, Lcom/teamspeak/ts3client/jni/events/ClientMoveTimeout;->e:Lcom/teamspeak/ts3client/jni/j;

    .line 33
    :cond_1f
    iput-object p9, p0, Lcom/teamspeak/ts3client/jni/events/ClientMoveTimeout;->f:Ljava/lang/String;

    .line 34
    invoke-static {p0}, Lcom/teamspeak/ts3client/jni/l;->a(Lcom/teamspeak/ts3client/jni/k;)V

    .line 35
    return-void
.end method

.method private c()J
    .registers 3

    .prologue
    .line 42
    iget-wide v0, p0, Lcom/teamspeak/ts3client/jni/events/ClientMoveTimeout;->d:J

    return-wide v0
.end method

.method private d()J
    .registers 3

    .prologue
    .line 50
    iget-wide v0, p0, Lcom/teamspeak/ts3client/jni/events/ClientMoveTimeout;->a:J

    return-wide v0
.end method

.method private e()Ljava/lang/String;
    .registers 2

    .prologue
    .line 54
    iget-object v0, p0, Lcom/teamspeak/ts3client/jni/events/ClientMoveTimeout;->f:Ljava/lang/String;

    return-object v0
.end method

.method private f()Lcom/teamspeak/ts3client/jni/j;
    .registers 2

    .prologue
    .line 58
    iget-object v0, p0, Lcom/teamspeak/ts3client/jni/events/ClientMoveTimeout;->e:Lcom/teamspeak/ts3client/jni/j;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .registers 2

    .prologue
    .line 38
    iget v0, p0, Lcom/teamspeak/ts3client/jni/events/ClientMoveTimeout;->b:I

    return v0
.end method

.method public final b()J
    .registers 3

    .prologue
    .line 46
    iget-wide v0, p0, Lcom/teamspeak/ts3client/jni/events/ClientMoveTimeout;->c:J

    return-wide v0
.end method

.method public toString()Ljava/lang/String;
    .registers 5

    .prologue
    .line 63
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ClientMoveTimeout [serverConnectionHandlerID="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, Lcom/teamspeak/ts3client/jni/events/ClientMoveTimeout;->a:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", clientID="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/teamspeak/ts3client/jni/events/ClientMoveTimeout;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", oldChannelID="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/teamspeak/ts3client/jni/events/ClientMoveTimeout;->c:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", newChannelID="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/teamspeak/ts3client/jni/events/ClientMoveTimeout;->d:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", visibility="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/jni/events/ClientMoveTimeout;->e:Lcom/teamspeak/ts3client/jni/j;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", timeoutMessage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/jni/events/ClientMoveTimeout;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
