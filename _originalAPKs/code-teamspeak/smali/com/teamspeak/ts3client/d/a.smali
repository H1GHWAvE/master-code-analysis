.class public final Lcom/teamspeak/ts3client/d/a;
.super Landroid/support/v4/app/ax;
.source "SourceFile"

# interfaces
.implements Lcom/teamspeak/ts3client/data/w;


# instance fields
.field private aA:Z

.field private aB:Landroid/widget/TextView;

.field private aC:Landroid/widget/TextView;

.field private aD:Landroid/widget/TextView;

.field private aE:Landroid/widget/TextView;

.field private aF:Landroid/widget/TextView;

.field private aG:Landroid/widget/TextView;

.field private aH:Landroid/widget/TextView;

.field private aI:Landroid/widget/TextView;

.field private aJ:Landroid/widget/TextView;

.field private aK:Landroid/widget/TextView;

.field private aL:Landroid/widget/TextView;

.field private aM:Landroid/widget/TextView;

.field private aN:Landroid/widget/TextView;

.field private aO:Lcom/teamspeak/ts3client/data/c;

.field private aP:Landroid/widget/TableRow;

.field private at:Landroid/widget/TextView;

.field private au:Landroid/widget/TextView;

.field private av:Landroid/widget/TextView;

.field private aw:Landroid/widget/TextView;

.field private ax:Landroid/widget/TextView;

.field private ay:Lcom/teamspeak/ts3client/Ts3Application;

.field private az:Ljava/lang/Thread;


# direct methods
.method public constructor <init>(Lcom/teamspeak/ts3client/data/c;)V
    .registers 3

    .prologue
    .line 53
    invoke-direct {p0}, Landroid/support/v4/app/ax;-><init>()V

    .line 36
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/teamspeak/ts3client/d/a;->aA:Z

    .line 54
    iput-object p1, p0, Lcom/teamspeak/ts3client/d/a;->aO:Lcom/teamspeak/ts3client/data/c;

    .line 55
    return-void
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/d/a;)Z
    .registers 2

    .prologue
    .line 27
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/d/a;->aA:Z

    return v0
.end method

.method static synthetic b(Lcom/teamspeak/ts3client/d/a;)Lcom/teamspeak/ts3client/data/c;
    .registers 2

    .prologue
    .line 27
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a;->aO:Lcom/teamspeak/ts3client/data/c;

    return-object v0
.end method

.method static synthetic c(Lcom/teamspeak/ts3client/d/a;)Lcom/teamspeak/ts3client/Ts3Application;
    .registers 2

    .prologue
    .line 27
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a;->ay:Lcom/teamspeak/ts3client/Ts3Application;

    return-object v0
.end method

.method static synthetic d(Lcom/teamspeak/ts3client/d/a;)V
    .registers 1

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/teamspeak/ts3client/d/a;->y()V

    return-void
.end method

.method static synthetic e(Lcom/teamspeak/ts3client/d/a;)Landroid/widget/TextView;
    .registers 2

    .prologue
    .line 27
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a;->at:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic f(Lcom/teamspeak/ts3client/d/a;)Landroid/widget/TextView;
    .registers 2

    .prologue
    .line 27
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a;->au:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic g(Lcom/teamspeak/ts3client/d/a;)Landroid/widget/TextView;
    .registers 2

    .prologue
    .line 27
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a;->av:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic h(Lcom/teamspeak/ts3client/d/a;)Landroid/widget/TextView;
    .registers 2

    .prologue
    .line 27
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a;->aw:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic i(Lcom/teamspeak/ts3client/d/a;)Landroid/widget/TextView;
    .registers 2

    .prologue
    .line 27
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a;->ax:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic j(Lcom/teamspeak/ts3client/d/a;)Landroid/widget/TextView;
    .registers 2

    .prologue
    .line 27
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a;->aL:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic k(Lcom/teamspeak/ts3client/d/a;)Landroid/widget/TextView;
    .registers 2

    .prologue
    .line 27
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a;->aM:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic l(Lcom/teamspeak/ts3client/d/a;)Landroid/widget/TextView;
    .registers 2

    .prologue
    .line 27
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a;->aN:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic m(Lcom/teamspeak/ts3client/d/a;)Landroid/widget/TextView;
    .registers 2

    .prologue
    .line 27
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a;->aB:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic n(Lcom/teamspeak/ts3client/d/a;)Landroid/widget/TextView;
    .registers 2

    .prologue
    .line 27
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a;->aC:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic o(Lcom/teamspeak/ts3client/d/a;)Landroid/widget/TextView;
    .registers 2

    .prologue
    .line 27
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a;->aE:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic p(Lcom/teamspeak/ts3client/d/a;)Landroid/widget/TextView;
    .registers 2

    .prologue
    .line 27
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a;->aD:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic q(Lcom/teamspeak/ts3client/d/a;)Landroid/widget/TextView;
    .registers 2

    .prologue
    .line 27
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a;->aF:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic r(Lcom/teamspeak/ts3client/d/a;)Landroid/widget/TextView;
    .registers 2

    .prologue
    .line 27
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a;->aG:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic s(Lcom/teamspeak/ts3client/d/a;)Landroid/widget/TextView;
    .registers 2

    .prologue
    .line 27
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a;->aH:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic t(Lcom/teamspeak/ts3client/d/a;)Landroid/widget/TextView;
    .registers 2

    .prologue
    .line 27
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a;->aI:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic u(Lcom/teamspeak/ts3client/d/a;)Landroid/widget/TextView;
    .registers 2

    .prologue
    .line 27
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a;->aJ:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic v(Lcom/teamspeak/ts3client/d/a;)Landroid/widget/TextView;
    .registers 2

    .prologue
    .line 27
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a;->aK:Landroid/widget/TextView;

    return-object v0
.end method

.method private y()V
    .registers 5

    .prologue
    .line 186
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/d/a;->l()Z

    move-result v0

    if-nez v0, :cond_7

    .line 222
    :goto_6
    return-void

    .line 188
    :cond_7
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v1, "0.00%"

    invoke-direct {v0, v1}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    .line 189
    new-instance v1, Ljava/text/DecimalFormat;

    const-string v2, "0.00"

    invoke-direct {v1, v2}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    .line 190
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/d/a;->i()Landroid/support/v4/app/bb;

    move-result-object v2

    new-instance v3, Lcom/teamspeak/ts3client/d/c;

    invoke-direct {v3, p0, v1, v0}, Lcom/teamspeak/ts3client/d/c;-><init>(Lcom/teamspeak/ts3client/d/a;Ljava/text/DecimalFormat;Ljava/text/DecimalFormat;)V

    invoke-virtual {v2, v3}, Landroid/support/v4/app/bb;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_6
.end method


# virtual methods
.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 7

    .prologue
    const/4 v3, 0x0

    .line 73
    const v0, 0x7f030029

    invoke-virtual {p1, v0, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/HorizontalScrollView;

    .line 1207
    iget-object v1, p0, Landroid/support/v4/app/ax;->j:Landroid/app/Dialog;

    .line 74
    const-string v2, "connectioninfoclient.title"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/Dialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 75
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v1

    iput-object v1, p0, Lcom/teamspeak/ts3client/d/a;->ay:Lcom/teamspeak/ts3client/Ts3Application;

    .line 76
    const v1, 0x7f0c0102

    invoke-virtual {v0, v1}, Landroid/widget/HorizontalScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/teamspeak/ts3client/d/a;->at:Landroid/widget/TextView;

    .line 77
    const v1, 0x7f0c0104

    invoke-virtual {v0, v1}, Landroid/widget/HorizontalScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/teamspeak/ts3client/d/a;->au:Landroid/widget/TextView;

    .line 78
    const v1, 0x7f0c0107

    invoke-virtual {v0, v1}, Landroid/widget/HorizontalScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/teamspeak/ts3client/d/a;->av:Landroid/widget/TextView;

    .line 79
    const v1, 0x7f0c0109

    invoke-virtual {v0, v1}, Landroid/widget/HorizontalScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/teamspeak/ts3client/d/a;->aw:Landroid/widget/TextView;

    .line 80
    const v1, 0x7f0c010c

    invoke-virtual {v0, v1}, Landroid/widget/HorizontalScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/teamspeak/ts3client/d/a;->ax:Landroid/widget/TextView;

    .line 81
    const v1, 0x7f0c010f

    invoke-virtual {v0, v1}, Landroid/widget/HorizontalScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/teamspeak/ts3client/d/a;->aL:Landroid/widget/TextView;

    .line 83
    const v1, 0x7f0c011a

    invoke-virtual {v0, v1}, Landroid/widget/HorizontalScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/teamspeak/ts3client/d/a;->aB:Landroid/widget/TextView;

    .line 84
    const v1, 0x7f0c011b

    invoke-virtual {v0, v1}, Landroid/widget/HorizontalScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/teamspeak/ts3client/d/a;->aC:Landroid/widget/TextView;

    .line 86
    const v1, 0x7f0c011e

    invoke-virtual {v0, v1}, Landroid/widget/HorizontalScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/teamspeak/ts3client/d/a;->aE:Landroid/widget/TextView;

    .line 87
    const v1, 0x7f0c011f

    invoke-virtual {v0, v1}, Landroid/widget/HorizontalScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/teamspeak/ts3client/d/a;->aD:Landroid/widget/TextView;

    .line 89
    const v1, 0x7f0c0122

    invoke-virtual {v0, v1}, Landroid/widget/HorizontalScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/teamspeak/ts3client/d/a;->aF:Landroid/widget/TextView;

    .line 90
    const v1, 0x7f0c0123

    invoke-virtual {v0, v1}, Landroid/widget/HorizontalScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/teamspeak/ts3client/d/a;->aG:Landroid/widget/TextView;

    .line 92
    const v1, 0x7f0c0126

    invoke-virtual {v0, v1}, Landroid/widget/HorizontalScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/teamspeak/ts3client/d/a;->aH:Landroid/widget/TextView;

    .line 93
    const v1, 0x7f0c0127

    invoke-virtual {v0, v1}, Landroid/widget/HorizontalScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/teamspeak/ts3client/d/a;->aI:Landroid/widget/TextView;

    .line 95
    const v1, 0x7f0c012a

    invoke-virtual {v0, v1}, Landroid/widget/HorizontalScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/teamspeak/ts3client/d/a;->aJ:Landroid/widget/TextView;

    .line 96
    const v1, 0x7f0c012b

    invoke-virtual {v0, v1}, Landroid/widget/HorizontalScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/teamspeak/ts3client/d/a;->aK:Landroid/widget/TextView;

    .line 98
    const v1, 0x7f0c0116

    invoke-virtual {v0, v1}, Landroid/widget/HorizontalScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/teamspeak/ts3client/d/a;->aM:Landroid/widget/TextView;

    .line 99
    const v1, 0x7f0c0117

    invoke-virtual {v0, v1}, Landroid/widget/HorizontalScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/teamspeak/ts3client/d/a;->aN:Landroid/widget/TextView;

    .line 100
    const v1, 0x7f0c010a

    invoke-virtual {v0, v1}, Landroid/widget/HorizontalScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TableRow;

    iput-object v1, p0, Lcom/teamspeak/ts3client/d/a;->aP:Landroid/widget/TableRow;

    .line 102
    const-string v1, "connectioninfoclient.loading"

    iget-object v2, p0, Lcom/teamspeak/ts3client/d/a;->at:Landroid/widget/TextView;

    invoke-static {v1, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/widget/TextView;)V

    .line 103
    const-string v1, "connectioninfoclient.loading"

    iget-object v2, p0, Lcom/teamspeak/ts3client/d/a;->au:Landroid/widget/TextView;

    invoke-static {v1, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/widget/TextView;)V

    .line 104
    const-string v1, "connectioninfoclient.loading"

    iget-object v2, p0, Lcom/teamspeak/ts3client/d/a;->av:Landroid/widget/TextView;

    invoke-static {v1, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/widget/TextView;)V

    .line 105
    const-string v1, "connectioninfoclient.loading"

    iget-object v2, p0, Lcom/teamspeak/ts3client/d/a;->aw:Landroid/widget/TextView;

    invoke-static {v1, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/widget/TextView;)V

    .line 106
    const-string v1, "connectioninfoclient.loading"

    iget-object v2, p0, Lcom/teamspeak/ts3client/d/a;->ax:Landroid/widget/TextView;

    invoke-static {v1, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/widget/TextView;)V

    .line 107
    const-string v1, "connectioninfoclient.loading"

    iget-object v2, p0, Lcom/teamspeak/ts3client/d/a;->aL:Landroid/widget/TextView;

    invoke-static {v1, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/widget/TextView;)V

    .line 108
    const-string v1, "connectioninfoclient.loading"

    iget-object v2, p0, Lcom/teamspeak/ts3client/d/a;->aB:Landroid/widget/TextView;

    invoke-static {v1, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/widget/TextView;)V

    .line 109
    const-string v1, "connectioninfoclient.loading"

    iget-object v2, p0, Lcom/teamspeak/ts3client/d/a;->aC:Landroid/widget/TextView;

    invoke-static {v1, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/widget/TextView;)V

    .line 110
    const-string v1, "connectioninfoclient.loading"

    iget-object v2, p0, Lcom/teamspeak/ts3client/d/a;->aE:Landroid/widget/TextView;

    invoke-static {v1, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/widget/TextView;)V

    .line 111
    const-string v1, "connectioninfoclient.loading"

    iget-object v2, p0, Lcom/teamspeak/ts3client/d/a;->aD:Landroid/widget/TextView;

    invoke-static {v1, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/widget/TextView;)V

    .line 112
    const-string v1, "connectioninfoclient.loading"

    iget-object v2, p0, Lcom/teamspeak/ts3client/d/a;->aF:Landroid/widget/TextView;

    invoke-static {v1, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/widget/TextView;)V

    .line 113
    const-string v1, "connectioninfoclient.loading"

    iget-object v2, p0, Lcom/teamspeak/ts3client/d/a;->aG:Landroid/widget/TextView;

    invoke-static {v1, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/widget/TextView;)V

    .line 114
    const-string v1, "connectioninfoclient.loading"

    iget-object v2, p0, Lcom/teamspeak/ts3client/d/a;->aH:Landroid/widget/TextView;

    invoke-static {v1, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/widget/TextView;)V

    .line 115
    const-string v1, "connectioninfoclient.loading"

    iget-object v2, p0, Lcom/teamspeak/ts3client/d/a;->aI:Landroid/widget/TextView;

    invoke-static {v1, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/widget/TextView;)V

    .line 116
    const-string v1, "connectioninfoclient.loading"

    iget-object v2, p0, Lcom/teamspeak/ts3client/d/a;->aJ:Landroid/widget/TextView;

    invoke-static {v1, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/widget/TextView;)V

    .line 117
    const-string v1, "connectioninfoclient.loading"

    iget-object v2, p0, Lcom/teamspeak/ts3client/d/a;->aK:Landroid/widget/TextView;

    invoke-static {v1, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/widget/TextView;)V

    .line 118
    const-string v1, "connectioninfoclient.loading"

    iget-object v2, p0, Lcom/teamspeak/ts3client/d/a;->aM:Landroid/widget/TextView;

    invoke-static {v1, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/widget/TextView;)V

    .line 119
    const-string v1, "connectioninfoclient.loading"

    iget-object v2, p0, Lcom/teamspeak/ts3client/d/a;->aN:Landroid/widget/TextView;

    invoke-static {v1, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/widget/TextView;)V

    .line 121
    const-string v1, "connectioninfoclient.name"

    const v2, 0x7f0c0101

    invoke-static {v1, v0, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/ViewGroup;I)V

    .line 122
    const-string v1, "connectioninfoclient.contime"

    const v2, 0x7f0c0103

    invoke-static {v1, v0, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/ViewGroup;I)V

    .line 123
    const-string v1, "connectioninfoclient.idle"

    const v2, 0x7f0c0106

    invoke-static {v1, v0, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/ViewGroup;I)V

    .line 124
    const-string v1, "connectioninfoclient.ping"

    const v2, 0x7f0c0108

    invoke-static {v1, v0, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/ViewGroup;I)V

    .line 125
    const-string v1, "connectioninfoclient.saddress"

    const v2, 0x7f0c010b

    invoke-static {v1, v0, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/ViewGroup;I)V

    .line 126
    const-string v1, "connectioninfoclient.caddress"

    const v2, 0x7f0c010e

    invoke-static {v1, v0, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/ViewGroup;I)V

    .line 127
    const-string v1, "connectioninfoclient.packettrans"

    const v2, 0x7f0c0119

    invoke-static {v1, v0, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/ViewGroup;I)V

    .line 128
    const-string v1, "connectioninfoclient.bytetrans"

    const v2, 0x7f0c011d

    invoke-static {v1, v0, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/ViewGroup;I)V

    .line 129
    const-string v1, "connectioninfoclient.bandwidthsec"

    const v2, 0x7f0c0121

    invoke-static {v1, v0, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/ViewGroup;I)V

    .line 130
    const-string v1, "connectioninfoclient.bandwidthmin"

    const v2, 0x7f0c0125

    invoke-static {v1, v0, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/ViewGroup;I)V

    .line 131
    const-string v1, "connectioninfoclient.filebandwidth"

    const v2, 0x7f0c0129

    invoke-static {v1, v0, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/ViewGroup;I)V

    .line 132
    const-string v1, "connectioninfoclient.packloss"

    const v2, 0x7f0c0115

    invoke-static {v1, v0, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/ViewGroup;I)V

    .line 133
    const-string v1, "connectioninfoclient.tablein"

    const v2, 0x7f0c0112

    invoke-static {v1, v0, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/ViewGroup;I)V

    .line 134
    const-string v1, "connectioninfoclient.tableout"

    const v2, 0x7f0c0113

    invoke-static {v1, v0, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/ViewGroup;I)V

    .line 136
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/a;->aO:Lcom/teamspeak/ts3client/data/c;

    .line 1235
    iget v1, v1, Lcom/teamspeak/ts3client/data/c;->c:I

    .line 136
    iget-object v2, p0, Lcom/teamspeak/ts3client/d/a;->ay:Lcom/teamspeak/ts3client/Ts3Application;

    .line 2061
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 2242
    iget v2, v2, Lcom/teamspeak/ts3client/data/e;->h:I

    .line 136
    if-eq v1, v2, :cond_1ee

    .line 137
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/a;->aP:Landroid/widget/TableRow;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TableRow;->setVisibility(I)V

    .line 140
    :goto_1ed
    return-object v0

    .line 139
    :cond_1ee
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/a;->aP:Landroid/widget/TableRow;

    invoke-virtual {v1, v3}, Landroid/widget/TableRow;->setVisibility(I)V

    goto :goto_1ed
.end method

.method public final a(Landroid/os/Bundle;)V
    .registers 2

    .prologue
    .line 66
    invoke-super {p0, p1}, Landroid/support/v4/app/ax;->a(Landroid/os/Bundle;)V

    .line 67
    return-void
.end method

.method public final a(Lcom/teamspeak/ts3client/jni/k;)V
    .registers 6

    .prologue
    .line 226
    instance-of v0, p1, Lcom/teamspeak/ts3client/jni/events/ConnectionInfo;

    if-eqz v0, :cond_12

    move-object v0, p1

    .line 227
    check-cast v0, Lcom/teamspeak/ts3client/jni/events/ConnectionInfo;

    .line 10027
    iget v0, v0, Lcom/teamspeak/ts3client/jni/events/ConnectionInfo;->a:I

    .line 227
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/a;->aO:Lcom/teamspeak/ts3client/data/c;

    .line 10235
    iget v1, v1, Lcom/teamspeak/ts3client/data/c;->c:I

    .line 227
    if-ne v0, v1, :cond_12

    .line 228
    invoke-direct {p0}, Lcom/teamspeak/ts3client/d/a;->y()V

    .line 230
    :cond_12
    instance-of v0, p1, Lcom/teamspeak/ts3client/jni/events/ServerError;

    if-eqz v0, :cond_78

    move-object v0, p1

    .line 231
    check-cast v0, Lcom/teamspeak/ts3client/jni/events/ServerError;

    .line 11029
    iget v0, v0, Lcom/teamspeak/ts3client/jni/events/ServerError;->b:I

    .line 231
    const/16 v1, 0x200

    if-ne v0, v1, :cond_78

    check-cast p1, Lcom/teamspeak/ts3client/jni/events/ServerError;

    .line 11041
    iget-object v0, p1, Lcom/teamspeak/ts3client/jni/events/ServerError;->c:Ljava/lang/String;

    .line 231
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "connectioninfo_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/teamspeak/ts3client/d/a;->aO:Lcom/teamspeak/ts3client/data/c;

    .line 11235
    iget v2, v2, Lcom/teamspeak/ts3client/data/c;->c:I

    .line 231
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_78

    .line 232
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a;->ay:Lcom/teamspeak/ts3client/Ts3Application;

    .line 12061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 12238
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->c:Lcom/teamspeak/ts3client/jni/l;

    .line 232
    invoke-virtual {v0, p0}, Lcom/teamspeak/ts3client/jni/l;->b(Lcom/teamspeak/ts3client/data/w;)V

    .line 233
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/teamspeak/ts3client/d/a;->aA:Z

    .line 234
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a;->az:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 235
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a;->aO:Lcom/teamspeak/ts3client/data/c;

    .line 13235
    iget v0, v0, Lcom/teamspeak/ts3client/data/c;->c:I

    .line 235
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/a;->ay:Lcom/teamspeak/ts3client/Ts3Application;

    .line 14061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 14242
    iget v1, v1, Lcom/teamspeak/ts3client/data/e;->h:I

    .line 235
    if-eq v0, v1, :cond_6c

    .line 236
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a;->ay:Lcom/teamspeak/ts3client/Ts3Application;

    .line 15061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 15234
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 236
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/a;->ay:Lcom/teamspeak/ts3client/Ts3Application;

    .line 16061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 16267
    iget-wide v2, v1, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 236
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/a;->aO:Lcom/teamspeak/ts3client/data/c;

    .line 17235
    iget v1, v1, Lcom/teamspeak/ts3client/data/c;->c:I

    .line 236
    invoke-virtual {v0, v2, v3, v1}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_cleanUpConnectionInfo(JI)I

    .line 237
    :cond_6c
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/d/a;->i()Landroid/support/v4/app/bb;

    move-result-object v0

    new-instance v1, Lcom/teamspeak/ts3client/d/d;

    invoke-direct {v1, p0}, Lcom/teamspeak/ts3client/d/d;-><init>(Lcom/teamspeak/ts3client/d/a;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/bb;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 245
    :cond_78
    return-void
.end method

.method public final b()V
    .registers 1

    .prologue
    .line 60
    invoke-super {p0}, Landroid/support/v4/app/ax;->b()V

    .line 61
    return-void
.end method

.method public final e()V
    .registers 1

    .prologue
    .line 250
    invoke-super {p0}, Landroid/support/v4/app/ax;->e()V

    .line 251
    invoke-static {p0}, Lcom/teamspeak/ts3client/data/d/q;->a(Landroid/support/v4/app/ax;)V

    .line 252
    return-void
.end method

.method public final f()V
    .registers 1

    .prologue
    .line 257
    invoke-super {p0}, Landroid/support/v4/app/ax;->f()V

    .line 258
    invoke-static {p0}, Lcom/teamspeak/ts3client/data/d/q;->b(Landroid/support/v4/app/ax;)V

    .line 259
    return-void
.end method

.method public final s()V
    .registers 3

    .prologue
    .line 146
    invoke-super {p0}, Landroid/support/v4/app/ax;->s()V

    .line 147
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a;->ay:Lcom/teamspeak/ts3client/Ts3Application;

    .line 3061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 3238
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->c:Lcom/teamspeak/ts3client/jni/l;

    .line 147
    invoke-virtual {v0, p0}, Lcom/teamspeak/ts3client/jni/l;->a(Lcom/teamspeak/ts3client/data/w;)V

    .line 148
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/teamspeak/ts3client/d/a;->aA:Z

    .line 149
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/teamspeak/ts3client/d/b;

    invoke-direct {v1, p0}, Lcom/teamspeak/ts3client/d/b;-><init>(Lcom/teamspeak/ts3client/d/a;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/teamspeak/ts3client/d/a;->az:Ljava/lang/Thread;

    .line 166
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a;->az:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 167
    return-void
.end method

.method public final t()V
    .registers 5

    .prologue
    .line 172
    invoke-super {p0}, Landroid/support/v4/app/ax;->t()V

    .line 174
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/teamspeak/ts3client/d/a;->aA:Z

    .line 175
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a;->az:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 177
    :try_start_b
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a;->ay:Lcom/teamspeak/ts3client/Ts3Application;

    .line 4061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 4238
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->c:Lcom/teamspeak/ts3client/jni/l;

    .line 177
    invoke-virtual {v0, p0}, Lcom/teamspeak/ts3client/jni/l;->b(Lcom/teamspeak/ts3client/data/w;)V

    .line 178
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a;->aO:Lcom/teamspeak/ts3client/data/c;

    .line 5235
    iget v0, v0, Lcom/teamspeak/ts3client/data/c;->c:I

    .line 178
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/a;->ay:Lcom/teamspeak/ts3client/Ts3Application;

    .line 6061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 6242
    iget v1, v1, Lcom/teamspeak/ts3client/data/e;->h:I

    .line 178
    if-eq v0, v1, :cond_33

    .line 179
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a;->ay:Lcom/teamspeak/ts3client/Ts3Application;

    .line 7061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 7234
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 179
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/a;->ay:Lcom/teamspeak/ts3client/Ts3Application;

    .line 8061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 8267
    iget-wide v2, v1, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 179
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/a;->aO:Lcom/teamspeak/ts3client/data/c;

    .line 9235
    iget v1, v1, Lcom/teamspeak/ts3client/data/c;->c:I

    .line 179
    invoke-virtual {v0, v2, v3, v1}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_cleanUpConnectionInfo(JI)I
    :try_end_33
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_33} :catch_34

    .line 183
    :cond_33
    :goto_33
    return-void

    :catch_34
    move-exception v0

    goto :goto_33
.end method
