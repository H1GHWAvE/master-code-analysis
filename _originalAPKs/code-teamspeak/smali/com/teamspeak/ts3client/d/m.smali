.class public final Lcom/teamspeak/ts3client/d/m;
.super Landroid/app/Dialog;
.source "SourceFile"


# instance fields
.field private a:Ljava/io/File;

.field private b:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 5

    .prologue
    .line 36
    invoke-direct {p0, p1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    .line 32
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Landroid/os/Environment;->DIRECTORY_DOWNLOADS:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/teamspeak/ts3client/d/m;->a:Ljava/io/File;

    .line 37
    return-void
.end method

.method private a(F)I
    .registers 4

    .prologue
    .line 79
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/d/m;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    .line 80
    mul-float/2addr v0, p1

    const/high16 v1, 0x3f000000    # 0.5f

    add-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/d/m;Ljava/lang/String;)Lcom/teamspeak/ts3client/e/a;
    .registers 3

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcom/teamspeak/ts3client/d/m;->a(Ljava/lang/String;)Lcom/teamspeak/ts3client/e/a;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;)Lcom/teamspeak/ts3client/e/a;
    .registers 10

    .prologue
    const/4 v7, 0x2

    const/4 v1, 0x0

    const/4 v6, 0x1

    .line 84
    new-instance v0, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/teamspeak/ts3client/d/m;->a:Ljava/io/File;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 85
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_5b

    .line 86
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/teamspeak/ts3client/d/m;->b:Landroid/content/Context;

    invoke-direct {v0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 87
    invoke-static {v0}, Lcom/teamspeak/ts3client/data/d/q;->a(Landroid/app/Dialog;)V

    .line 88
    const-string v2, "ident.file.error.info"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 89
    const-string v2, "ident.file.error.text2"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 90
    const/4 v2, -0x2

    const-string v3, "button.cancel"

    invoke-static {v3}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/teamspeak/ts3client/d/p;

    invoke-direct {v4, p0, v0}, Lcom/teamspeak/ts3client/d/p;-><init>(Lcom/teamspeak/ts3client/d/m;Landroid/app/AlertDialog;)V

    invoke-virtual {v0, v2, v3, v4}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 97
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    move-object v0, v1

    .line 136
    :goto_5a
    return-object v0

    .line 101
    :cond_5b
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 104
    :try_start_60
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 105
    new-instance v0, Ljava/io/BufferedReader;

    new-instance v4, Ljava/io/InputStreamReader;

    invoke-direct {v4, v2}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v0, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 107
    :goto_6f
    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_c6

    .line 108
    invoke-static {v4}, Lcom/teamspeak/ts3client/data/d/x;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_7c
    .catch Ljava/io/FileNotFoundException; {:try_start_60 .. :try_end_7c} :catch_7d
    .catch Ljava/io/IOException; {:try_start_60 .. :try_end_7c} :catch_cd

    goto :goto_6f

    .line 114
    :catch_7d
    move-exception v0

    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V

    .line 120
    :goto_81
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_c1

    const/4 v0, 0x0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v2, "[Identity]"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c1

    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v2, "id"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c1

    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v2, "identity="

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c1

    const/4 v0, 0x3

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v2, "nickname="

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_d2

    .line 121
    :cond_c1
    invoke-direct {p0}, Lcom/teamspeak/ts3client/d/m;->a()V

    move-object v0, v1

    .line 122
    goto :goto_5a

    .line 110
    :cond_c6
    :try_start_c6
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V

    .line 111
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_cc
    .catch Ljava/io/FileNotFoundException; {:try_start_c6 .. :try_end_cc} :catch_7d
    .catch Ljava/io/IOException; {:try_start_c6 .. :try_end_cc} :catch_cd

    goto :goto_81

    .line 117
    :catch_cd
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_81

    .line 124
    :cond_d2
    new-instance v2, Lcom/teamspeak/ts3client/e/a;

    invoke-direct {v2}, Lcom/teamspeak/ts3client/e/a;-><init>()V

    .line 125
    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v4, "id="

    const-string v5, ""

    invoke-virtual {v0, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1045
    iput-object v0, v2, Lcom/teamspeak/ts3client/e/a;->b:Ljava/lang/String;

    .line 126
    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v4, "identity="

    const-string v5, ""

    invoke-virtual {v0, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    .line 127
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v0, v6, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 2037
    iput-object v0, v2, Lcom/teamspeak/ts3client/e/a;->c:Ljava/lang/String;

    .line 3033
    iget-object v0, v2, Lcom/teamspeak/ts3client/e/a;->c:Ljava/lang/String;

    .line 129
    const-string v4, ""

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_11d

    .line 3041
    iget-object v0, v2, Lcom/teamspeak/ts3client/e/a;->b:Ljava/lang/String;

    .line 129
    const-string v4, ""

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_123

    .line 130
    :cond_11d
    invoke-direct {p0}, Lcom/teamspeak/ts3client/d/m;->a()V

    move-object v0, v1

    .line 131
    goto/16 :goto_5a

    .line 133
    :cond_123
    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v1, "nickname="

    const-string v3, ""

    invoke-virtual {v0, v1, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    .line 3053
    iput-object v0, v2, Lcom/teamspeak/ts3client/e/a;->d:Ljava/lang/String;

    move-object v0, v2

    .line 136
    goto/16 :goto_5a
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/d/m;)Ljava/io/File;
    .registers 2

    .prologue
    .line 30
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/m;->a:Ljava/io/File;

    return-object v0
.end method

.method private a()V
    .registers 5

    .prologue
    .line 40
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/teamspeak/ts3client/d/m;->b:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 41
    invoke-static {v0}, Lcom/teamspeak/ts3client/data/d/q;->a(Landroid/app/Dialog;)V

    .line 42
    const-string v1, "ident.file.error.info"

    invoke-static {v1}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 43
    const-string v1, "ident.file.error.text1"

    invoke-static {v1}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 44
    const/4 v1, -0x2

    const-string v2, "button.cancel"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/teamspeak/ts3client/d/n;

    invoke-direct {v3, p0, v0}, Lcom/teamspeak/ts3client/d/n;-><init>(Lcom/teamspeak/ts3client/d/m;Landroid/app/AlertDialog;)V

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 51
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 52
    return-void
.end method

.method private b()[Ljava/lang/String;
    .registers 3

    .prologue
    .line 56
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/m;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_14

    .line 57
    new-instance v0, Lcom/teamspeak/ts3client/d/o;

    invoke-direct {v0, p0}, Lcom/teamspeak/ts3client/d/o;-><init>(Lcom/teamspeak/ts3client/d/m;)V

    .line 71
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/m;->a:Ljava/io/File;

    invoke-virtual {v1, v0}, Ljava/io/File;->list(Ljava/io/FilenameFilter;)[Ljava/lang/String;

    move-result-object v0

    .line 75
    :goto_13
    return-object v0

    .line 73
    :cond_14
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    goto :goto_13
.end method


# virtual methods
.method protected final onCreate(Landroid/os/Bundle;)V
    .registers 9

    .prologue
    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 141
    invoke-super {p0, p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    .line 142
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/d/m;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/teamspeak/ts3client/d/m;->b:Landroid/content/Context;

    .line 143
    invoke-static {p0}, Lcom/teamspeak/ts3client/data/d/q;->a(Landroid/app/Dialog;)V

    .line 144
    new-instance v2, Landroid/widget/ScrollView;

    invoke-virtual {p0}, Lcom/teamspeak/ts3client/d/m;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v2, v0}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;)V

    .line 145
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v0

    .line 146
    const-string v3, "mounted"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_43

    .line 147
    const-string v0, "ident.file.error.info"

    invoke-static {v0}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/teamspeak/ts3client/d/m;->setTitle(Ljava/lang/CharSequence;)V

    .line 148
    new-instance v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/teamspeak/ts3client/d/m;->b:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 149
    const-string v1, "ident.file.error.dialog.text1"

    invoke-static {v1}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 150
    invoke-virtual {v2, v0}, Landroid/widget/ScrollView;->addView(Landroid/view/View;)V

    .line 151
    invoke-virtual {p0, v2}, Lcom/teamspeak/ts3client/d/m;->setContentView(Landroid/view/View;)V

    .line 188
    :goto_42
    return-void

    .line 154
    :cond_43
    const-string v0, "ident.file.error.dialog.info"

    invoke-static {v0}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/teamspeak/ts3client/d/m;->setTitle(Ljava/lang/CharSequence;)V

    .line 3056
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/m;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_88

    .line 3057
    new-instance v0, Lcom/teamspeak/ts3client/d/o;

    invoke-direct {v0, p0}, Lcom/teamspeak/ts3client/d/o;-><init>(Lcom/teamspeak/ts3client/d/m;)V

    .line 3071
    iget-object v3, p0, Lcom/teamspeak/ts3client/d/m;->a:Ljava/io/File;

    invoke-virtual {v3, v0}, Ljava/io/File;->list(Ljava/io/FilenameFilter;)[Ljava/lang/String;

    move-result-object v0

    .line 157
    :goto_5f
    array-length v3, v0

    if-gtz v3, :cond_8b

    .line 158
    const-string v0, "ident.file.error.info"

    invoke-static {v0}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/teamspeak/ts3client/d/m;->setTitle(Ljava/lang/CharSequence;)V

    .line 159
    new-instance v0, Landroid/widget/TextView;

    iget-object v3, p0, Lcom/teamspeak/ts3client/d/m;->b:Landroid/content/Context;

    invoke-direct {v0, v3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 160
    const-string v3, "ident.file.error.dialog.text2"

    new-array v4, v6, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/teamspeak/ts3client/d/m;->a:Ljava/io/File;

    aput-object v5, v4, v1

    invoke-static {v3, v4}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 161
    invoke-virtual {v2, v0}, Landroid/widget/ScrollView;->addView(Landroid/view/View;)V

    .line 162
    invoke-virtual {p0, v2}, Lcom/teamspeak/ts3client/d/m;->setContentView(Landroid/view/View;)V

    goto :goto_42

    .line 3073
    :cond_88
    new-array v0, v1, [Ljava/lang/String;

    goto :goto_5f

    .line 166
    :cond_8b
    new-instance v3, Landroid/widget/RadioGroup;

    invoke-virtual {p0}, Lcom/teamspeak/ts3client/d/m;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/widget/RadioGroup;-><init>(Landroid/content/Context;)V

    .line 167
    :goto_94
    array-length v4, v0

    if-ge v1, v4, :cond_ae

    .line 168
    new-instance v4, Landroid/widget/RadioButton;

    invoke-virtual {p0}, Lcom/teamspeak/ts3client/d/m;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/widget/RadioButton;-><init>(Landroid/content/Context;)V

    .line 169
    aget-object v5, v0, v1

    invoke-virtual {v4, v5}, Landroid/widget/RadioButton;->setText(Ljava/lang/CharSequence;)V

    .line 170
    invoke-virtual {v4, v1}, Landroid/widget/RadioButton;->setId(I)V

    .line 171
    invoke-virtual {v3, v4}, Landroid/widget/RadioGroup;->addView(Landroid/view/View;)V

    .line 167
    add-int/lit8 v1, v1, 0x1

    goto :goto_94

    .line 173
    :cond_ae
    invoke-virtual {v2, v3}, Landroid/widget/ScrollView;->addView(Landroid/view/View;)V

    .line 174
    invoke-virtual {v2, v6}, Landroid/widget/ScrollView;->setId(I)V

    .line 175
    new-instance v1, Lcom/teamspeak/ts3client/d/q;

    invoke-direct {v1, p0, v0}, Lcom/teamspeak/ts3client/d/q;-><init>(Lcom/teamspeak/ts3client/d/m;[Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 187
    invoke-virtual {p0, v2}, Lcom/teamspeak/ts3client/d/m;->setContentView(Landroid/view/View;)V

    goto :goto_42
.end method
