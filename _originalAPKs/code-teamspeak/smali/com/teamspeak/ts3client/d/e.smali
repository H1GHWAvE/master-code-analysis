.class public final Lcom/teamspeak/ts3client/d/e;
.super Landroid/support/v4/app/ax;
.source "SourceFile"


# instance fields
.field private aA:Landroid/widget/CheckBox;

.field private aB:Landroid/widget/CheckBox;

.field private aC:Lcom/teamspeak/ts3client/data/c;

.field private aD:Landroid/widget/RadioGroup;

.field private aE:Landroid/widget/RadioButton;

.field private aF:Landroid/widget/RadioButton;

.field private aG:Landroid/widget/Button;

.field private aH:Lcom/teamspeak/ts3client/c/a;

.field private aI:Z

.field private at:Landroid/widget/EditText;

.field private au:Landroid/widget/Spinner;

.field private av:Landroid/widget/Spinner;

.field private aw:Landroid/widget/CheckBox;

.field private ax:Landroid/widget/CheckBox;

.field private ay:Landroid/widget/CheckBox;

.field private az:Landroid/widget/CheckBox;


# direct methods
.method public constructor <init>(Lcom/teamspeak/ts3client/c/a;)V
    .registers 3

    .prologue
    .line 51
    invoke-direct {p0}, Landroid/support/v4/app/ax;-><init>()V

    .line 52
    new-instance v0, Lcom/teamspeak/ts3client/data/c;

    invoke-direct {v0, p1}, Lcom/teamspeak/ts3client/data/c;-><init>(Lcom/teamspeak/ts3client/c/a;)V

    iput-object v0, p0, Lcom/teamspeak/ts3client/d/e;->aC:Lcom/teamspeak/ts3client/data/c;

    .line 53
    return-void
.end method

.method public constructor <init>(Lcom/teamspeak/ts3client/data/c;)V
    .registers 2

    .prologue
    .line 47
    invoke-direct {p0}, Landroid/support/v4/app/ax;-><init>()V

    .line 48
    iput-object p1, p0, Lcom/teamspeak/ts3client/d/e;->aC:Lcom/teamspeak/ts3client/data/c;

    .line 49
    return-void
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/d/e;)Z
    .registers 2

    .prologue
    .line 27
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/d/e;->aI:Z

    return v0
.end method

.method static synthetic b(Lcom/teamspeak/ts3client/d/e;)Z
    .registers 2

    .prologue
    .line 27
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/teamspeak/ts3client/d/e;->aI:Z

    return v0
.end method

.method static synthetic c(Lcom/teamspeak/ts3client/d/e;)Landroid/widget/Spinner;
    .registers 2

    .prologue
    .line 27
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/e;->av:Landroid/widget/Spinner;

    return-object v0
.end method

.method static synthetic d(Lcom/teamspeak/ts3client/d/e;)Landroid/widget/CheckBox;
    .registers 2

    .prologue
    .line 27
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/e;->aw:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic e(Lcom/teamspeak/ts3client/d/e;)Landroid/widget/CheckBox;
    .registers 2

    .prologue
    .line 27
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/e;->ax:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic f(Lcom/teamspeak/ts3client/d/e;)Landroid/widget/CheckBox;
    .registers 2

    .prologue
    .line 27
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/e;->ay:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic g(Lcom/teamspeak/ts3client/d/e;)Landroid/widget/CheckBox;
    .registers 2

    .prologue
    .line 27
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/e;->az:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic h(Lcom/teamspeak/ts3client/d/e;)Landroid/widget/CheckBox;
    .registers 2

    .prologue
    .line 27
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/e;->aA:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic i(Lcom/teamspeak/ts3client/d/e;)Landroid/widget/CheckBox;
    .registers 2

    .prologue
    .line 27
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/e;->aB:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic j(Lcom/teamspeak/ts3client/d/e;)Landroid/widget/RadioButton;
    .registers 2

    .prologue
    .line 27
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/e;->aE:Landroid/widget/RadioButton;

    return-object v0
.end method

.method static synthetic k(Lcom/teamspeak/ts3client/d/e;)Landroid/widget/RadioButton;
    .registers 2

    .prologue
    .line 27
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/e;->aF:Landroid/widget/RadioButton;

    return-object v0
.end method

.method static synthetic l(Lcom/teamspeak/ts3client/d/e;)Landroid/widget/EditText;
    .registers 2

    .prologue
    .line 27
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/e;->at:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic m(Lcom/teamspeak/ts3client/d/e;)Lcom/teamspeak/ts3client/c/a;
    .registers 2

    .prologue
    .line 27
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/e;->aH:Lcom/teamspeak/ts3client/c/a;

    return-object v0
.end method

.method static synthetic n(Lcom/teamspeak/ts3client/d/e;)Landroid/widget/Spinner;
    .registers 2

    .prologue
    .line 27
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/e;->au:Landroid/widget/Spinner;

    return-object v0
.end method

.method static synthetic o(Lcom/teamspeak/ts3client/d/e;)Lcom/teamspeak/ts3client/data/c;
    .registers 2

    .prologue
    .line 27
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/e;->aC:Lcom/teamspeak/ts3client/data/c;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 14

    .prologue
    const v10, 0x7f0c016b

    const v9, 0x7f0c016a

    const v8, 0x7f0c0169

    const/4 v7, 0x3

    const/4 v6, 0x0

    .line 79
    invoke-virtual {p1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/Ts3Application;

    .line 80
    const v1, 0x7f030031

    invoke-virtual {p1, v1, p2, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ScrollView;

    .line 9207
    iget-object v2, p0, Landroid/support/v4/app/ax;->j:Landroid/app/Dialog;

    .line 81
    const-string v3, "contact.settings.info"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/teamspeak/ts3client/d/e;->aC:Lcom/teamspeak/ts3client/data/c;

    .line 10203
    iget-object v5, v5, Lcom/teamspeak/ts3client/data/c;->a:Ljava/lang/String;

    .line 81
    aput-object v5, v4, v6

    invoke-static {v3, v4}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/Dialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 82
    const v2, 0x7f0c0164

    invoke-virtual {v1, v2}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    iput-object v2, p0, Lcom/teamspeak/ts3client/d/e;->at:Landroid/widget/EditText;

    .line 83
    iget-object v2, p0, Lcom/teamspeak/ts3client/d/e;->at:Landroid/widget/EditText;

    iget-object v3, p0, Lcom/teamspeak/ts3client/d/e;->aC:Lcom/teamspeak/ts3client/data/c;

    .line 11203
    iget-object v3, v3, Lcom/teamspeak/ts3client/data/c;->a:Ljava/lang/String;

    .line 83
    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 84
    const v2, 0x7f0c0165

    invoke-virtual {v1, v2}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Spinner;

    iput-object v2, p0, Lcom/teamspeak/ts3client/d/e;->au:Landroid/widget/Spinner;

    .line 85
    iget-object v2, p0, Lcom/teamspeak/ts3client/d/e;->au:Landroid/widget/Spinner;

    const-string v3, "contact.settings.status.array"

    invoke-virtual {v1}, Landroid/widget/ScrollView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v3, v4, v7}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/content/Context;I)Landroid/widget/SpinnerAdapter;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 86
    iget-object v2, p0, Lcom/teamspeak/ts3client/d/e;->au:Landroid/widget/Spinner;

    new-instance v3, Lcom/teamspeak/ts3client/d/f;

    invoke-direct {v3, p0}, Lcom/teamspeak/ts3client/d/f;-><init>(Lcom/teamspeak/ts3client/d/e;)V

    invoke-virtual {v2, v3}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 134
    const v2, 0x7f0c0167

    invoke-virtual {v1, v2}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Spinner;

    iput-object v2, p0, Lcom/teamspeak/ts3client/d/e;->av:Landroid/widget/Spinner;

    .line 135
    iget-object v2, p0, Lcom/teamspeak/ts3client/d/e;->av:Landroid/widget/Spinner;

    const-string v3, "contact.settings.display.array"

    invoke-virtual {v1}, Landroid/widget/ScrollView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v3, v4, v7}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/content/Context;I)Landroid/widget/SpinnerAdapter;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 136
    iget-object v2, p0, Lcom/teamspeak/ts3client/d/e;->av:Landroid/widget/Spinner;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Landroid/widget/Spinner;->setSelection(I)V

    .line 137
    invoke-virtual {v1, v8}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    iput-object v2, p0, Lcom/teamspeak/ts3client/d/e;->aw:Landroid/widget/CheckBox;

    .line 138
    invoke-virtual {v1, v9}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    iput-object v2, p0, Lcom/teamspeak/ts3client/d/e;->ax:Landroid/widget/CheckBox;

    .line 139
    invoke-virtual {v1, v10}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    iput-object v2, p0, Lcom/teamspeak/ts3client/d/e;->ay:Landroid/widget/CheckBox;

    .line 140
    const v2, 0x7f0c016c

    invoke-virtual {v1, v2}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    iput-object v2, p0, Lcom/teamspeak/ts3client/d/e;->az:Landroid/widget/CheckBox;

    .line 141
    const v2, 0x7f0c016e

    invoke-virtual {v1, v2}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    iput-object v2, p0, Lcom/teamspeak/ts3client/d/e;->aA:Landroid/widget/CheckBox;

    .line 142
    const v2, 0x7f0c016d

    invoke-virtual {v1, v2}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    iput-object v2, p0, Lcom/teamspeak/ts3client/d/e;->aB:Landroid/widget/CheckBox;

    .line 143
    const v2, 0x7f0c0170

    invoke-virtual {v1, v2}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RadioGroup;

    iput-object v2, p0, Lcom/teamspeak/ts3client/d/e;->aD:Landroid/widget/RadioGroup;

    .line 144
    const v2, 0x7f0c0171

    invoke-virtual {v1, v2}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RadioButton;

    iput-object v2, p0, Lcom/teamspeak/ts3client/d/e;->aE:Landroid/widget/RadioButton;

    .line 145
    const v2, 0x7f0c0172

    invoke-virtual {v1, v2}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RadioButton;

    iput-object v2, p0, Lcom/teamspeak/ts3client/d/e;->aF:Landroid/widget/RadioButton;

    .line 146
    const v2, 0x7f0c0173

    invoke-virtual {v1, v2}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/teamspeak/ts3client/d/e;->aG:Landroid/widget/Button;

    .line 147
    iget-object v2, p0, Lcom/teamspeak/ts3client/d/e;->aG:Landroid/widget/Button;

    new-instance v3, Lcom/teamspeak/ts3client/d/g;

    invoke-direct {v3, p0, v0}, Lcom/teamspeak/ts3client/d/g;-><init>(Lcom/teamspeak/ts3client/d/e;Lcom/teamspeak/ts3client/Ts3Application;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 198
    const-string v0, "contact.settings.name"

    const v2, 0x7f0c0163

    invoke-static {v0, v1, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/ViewGroup;I)V

    .line 199
    const-string v0, "contact.settings.display"

    const v2, 0x7f0c0166

    invoke-static {v0, v1, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/ViewGroup;I)V

    .line 200
    const-string v0, "contact.settings.ignore"

    const v2, 0x7f0c0168

    invoke-static {v0, v1, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/ViewGroup;I)V

    .line 201
    const-string v0, "contact.settings.whisper"

    const v2, 0x7f0c016f

    invoke-static {v0, v1, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/ViewGroup;I)V

    .line 202
    const-string v0, "contact.settings.mute"

    invoke-static {v0, v1, v8}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/ViewGroup;I)V

    .line 203
    const-string v0, "contact.settings.ignorepublicchat"

    invoke-static {v0, v1, v9}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/ViewGroup;I)V

    .line 204
    const-string v0, "contact.settings.ignoreprivatechat"

    invoke-static {v0, v1, v10}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/ViewGroup;I)V

    .line 205
    const-string v0, "contact.settings.ignorepokes"

    const v2, 0x7f0c016c

    invoke-static {v0, v1, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/ViewGroup;I)V

    .line 206
    const-string v0, "contact.settings.hideavatar"

    const v2, 0x7f0c016d

    invoke-static {v0, v1, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/ViewGroup;I)V

    .line 207
    const-string v0, "contact.settings.hideaway"

    const v2, 0x7f0c016e

    invoke-static {v0, v1, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/ViewGroup;I)V

    .line 208
    const-string v0, "contact.settings.whisper.allow"

    const v2, 0x7f0c0171

    invoke-static {v0, v1, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/ViewGroup;I)V

    .line 209
    const-string v0, "contact.settings.whisper.deny"

    const v2, 0x7f0c0172

    invoke-static {v0, v1, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/ViewGroup;I)V

    .line 210
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/e;->aG:Landroid/widget/Button;

    const-string v2, "button.save"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 211
    return-object v1
.end method

.method public final c(Landroid/os/Bundle;)V
    .registers 6

    .prologue
    const/4 v0, 0x1

    .line 58
    invoke-super {p0, p1}, Landroid/support/v4/app/ax;->c(Landroid/os/Bundle;)V

    .line 1042
    sget-object v1, Lcom/teamspeak/ts3client/data/b/c;->a:Lcom/teamspeak/ts3client/data/b/c;

    .line 59
    iget-object v2, p0, Lcom/teamspeak/ts3client/d/e;->aC:Lcom/teamspeak/ts3client/data/c;

    .line 1188
    iget-object v2, v2, Lcom/teamspeak/ts3client/data/c;->b:Ljava/lang/String;

    .line 59
    invoke-virtual {v1, v2}, Lcom/teamspeak/ts3client/data/b/c;->a(Ljava/lang/String;)Lcom/teamspeak/ts3client/c/a;

    move-result-object v1

    .line 60
    if-eqz v1, :cond_63

    .line 61
    iput-object v1, p0, Lcom/teamspeak/ts3client/d/e;->aH:Lcom/teamspeak/ts3client/c/a;

    .line 62
    iput-boolean v0, p0, Lcom/teamspeak/ts3client/d/e;->aI:Z

    .line 63
    iget-object v2, p0, Lcom/teamspeak/ts3client/d/e;->at:Landroid/widget/EditText;

    .line 2030
    iget-object v3, v1, Lcom/teamspeak/ts3client/c/a;->a:Ljava/lang/String;

    .line 63
    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 64
    iget-object v2, p0, Lcom/teamspeak/ts3client/d/e;->au:Landroid/widget/Spinner;

    .line 2054
    iget v3, v1, Lcom/teamspeak/ts3client/c/a;->e:I

    .line 64
    invoke-virtual {v2, v3}, Landroid/widget/Spinner;->setSelection(I)V

    .line 65
    iget-object v2, p0, Lcom/teamspeak/ts3client/d/e;->av:Landroid/widget/Spinner;

    .line 3046
    iget v3, v1, Lcom/teamspeak/ts3client/c/a;->d:I

    .line 65
    invoke-virtual {v2, v3}, Landroid/widget/Spinner;->setSelection(I)V

    .line 66
    iget-object v2, p0, Lcom/teamspeak/ts3client/d/e;->aw:Landroid/widget/CheckBox;

    .line 3102
    iget-boolean v3, v1, Lcom/teamspeak/ts3client/c/a;->f:Z

    .line 66
    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 67
    iget-object v2, p0, Lcom/teamspeak/ts3client/d/e;->ax:Landroid/widget/CheckBox;

    .line 4094
    iget-boolean v3, v1, Lcom/teamspeak/ts3client/c/a;->g:Z

    .line 67
    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 68
    iget-object v2, p0, Lcom/teamspeak/ts3client/d/e;->ay:Landroid/widget/CheckBox;

    .line 5086
    iget-boolean v3, v1, Lcom/teamspeak/ts3client/c/a;->h:Z

    .line 68
    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 69
    iget-object v2, p0, Lcom/teamspeak/ts3client/d/e;->az:Landroid/widget/CheckBox;

    .line 6078
    iget-boolean v3, v1, Lcom/teamspeak/ts3client/c/a;->i:Z

    .line 69
    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 70
    iget-object v2, p0, Lcom/teamspeak/ts3client/d/e;->aA:Landroid/widget/CheckBox;

    .line 7070
    iget-boolean v3, v1, Lcom/teamspeak/ts3client/c/a;->j:Z

    .line 70
    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 71
    iget-object v2, p0, Lcom/teamspeak/ts3client/d/e;->aB:Landroid/widget/CheckBox;

    .line 8062
    iget-boolean v3, v1, Lcom/teamspeak/ts3client/c/a;->k:Z

    .line 71
    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 72
    iget-object v2, p0, Lcom/teamspeak/ts3client/d/e;->aE:Landroid/widget/RadioButton;

    .line 8110
    iget-boolean v3, v1, Lcom/teamspeak/ts3client/c/a;->l:Z

    .line 72
    invoke-virtual {v2, v3}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 73
    iget-object v2, p0, Lcom/teamspeak/ts3client/d/e;->aF:Landroid/widget/RadioButton;

    .line 9110
    iget-boolean v1, v1, Lcom/teamspeak/ts3client/c/a;->l:Z

    .line 73
    if-nez v1, :cond_64

    :goto_60
    invoke-virtual {v2, v0}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 75
    :cond_63
    return-void

    .line 73
    :cond_64
    const/4 v0, 0x0

    goto :goto_60
.end method

.method public final e()V
    .registers 1

    .prologue
    .line 216
    invoke-super {p0}, Landroid/support/v4/app/ax;->e()V

    .line 217
    invoke-static {p0}, Lcom/teamspeak/ts3client/data/d/q;->a(Landroid/support/v4/app/ax;)V

    .line 218
    return-void
.end method

.method public final f()V
    .registers 1

    .prologue
    .line 222
    invoke-super {p0}, Landroid/support/v4/app/ax;->f()V

    .line 223
    invoke-static {p0}, Lcom/teamspeak/ts3client/data/d/q;->b(Landroid/support/v4/app/ax;)V

    .line 225
    return-void
.end method
