.class public final Lcom/teamspeak/ts3client/d/b/aj;
.super Landroid/support/v4/app/ax;
.source "SourceFile"


# instance fields
.field private aA:I

.field private at:Lcom/teamspeak/ts3client/data/c;

.field private au:Lcom/teamspeak/ts3client/Ts3Application;

.field private av:Landroid/widget/TableLayout;

.field private aw:Landroid/widget/TableLayout;

.field private ax:Landroid/widget/Button;

.field private ay:J

.field private az:I


# direct methods
.method public constructor <init>(Lcom/teamspeak/ts3client/data/c;)V
    .registers 2

    .prologue
    .line 41
    invoke-direct {p0}, Landroid/support/v4/app/ax;-><init>()V

    .line 42
    iput-object p1, p0, Lcom/teamspeak/ts3client/d/b/aj;->at:Lcom/teamspeak/ts3client/data/c;

    .line 43
    return-void
.end method

.method private a(F)I
    .registers 4

    .prologue
    .line 59
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/b/aj;->au:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    .line 60
    mul-float/2addr v0, p1

    const/high16 v1, 0x3f000000    # 0.5f

    add-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/d/b/aj;)Lcom/teamspeak/ts3client/data/c;
    .registers 2

    .prologue
    .line 32
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/b/aj;->at:Lcom/teamspeak/ts3client/data/c;

    return-object v0
.end method

.method private a(Landroid/widget/TableLayout;Ljava/lang/String;JZZLcom/teamspeak/ts3client/d/b/am;Z)V
    .registers 12

    .prologue
    .line 46
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/d/b/aj;->i()Landroid/support/v4/app/bb;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03002b

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TableRow;

    .line 47
    const v1, 0x7f0c0133

    invoke-virtual {v0, v1}, Landroid/widget/TableRow;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    .line 48
    invoke-virtual {v1, p2}, Landroid/widget/CheckBox;->setText(Ljava/lang/CharSequence;)V

    .line 49
    invoke-virtual {v1, p5}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 50
    invoke-virtual {v1, p6}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 51
    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setTag(Ljava/lang/Object;)V

    .line 1187
    iget-object v2, p7, Lcom/teamspeak/ts3client/d/b/am;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1188
    invoke-virtual {v1, p7}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 53
    if-eqz p8, :cond_37

    .line 1230
    iput-object v1, p7, Lcom/teamspeak/ts3client/d/b/am;->b:Landroid/widget/CheckBox;

    .line 55
    :cond_37
    invoke-virtual {p1, v0}, Landroid/widget/TableLayout;->addView(Landroid/view/View;)V

    .line 56
    return-void
.end method

.method static synthetic b(Lcom/teamspeak/ts3client/d/b/aj;)Lcom/teamspeak/ts3client/Ts3Application;
    .registers 2

    .prologue
    .line 32
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/b/aj;->au:Lcom/teamspeak/ts3client/Ts3Application;

    return-object v0
.end method

.method static synthetic c(Lcom/teamspeak/ts3client/d/b/aj;)J
    .registers 3

    .prologue
    .line 32
    iget-wide v0, p0, Lcom/teamspeak/ts3client/d/b/aj;->ay:J

    return-wide v0
.end method

.method static synthetic d(Lcom/teamspeak/ts3client/d/b/aj;)I
    .registers 2

    .prologue
    .line 32
    iget v0, p0, Lcom/teamspeak/ts3client/d/b/aj;->aA:I

    return v0
.end method


# virtual methods
.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 20

    .prologue
    .line 72
    invoke-virtual/range {p1 .. p1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    check-cast v2, Lcom/teamspeak/ts3client/Ts3Application;

    .line 73
    const v3, 0x7f03002a

    const/4 v4, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v0, v3, v1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    move-object v12, v3

    check-cast v12, Landroid/widget/FrameLayout;

    .line 5207
    move-object/from16 v0, p0

    iget-object v3, v0, Landroid/support/v4/app/ax;->j:Landroid/app/Dialog;

    .line 74
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/teamspeak/ts3client/d/b/aj;->at:Lcom/teamspeak/ts3client/data/c;

    .line 6203
    iget-object v4, v4, Lcom/teamspeak/ts3client/data/c;->a:Ljava/lang/String;

    .line 74
    invoke-virtual {v3, v4}, Landroid/app/Dialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 7061
    iget-object v3, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 7181
    iget-object v3, v3, Lcom/teamspeak/ts3client/data/e;->s:Lcom/teamspeak/ts3client/data/f/a;

    .line 8061
    iget-object v4, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 8250
    iget-object v4, v4, Lcom/teamspeak/ts3client/data/e;->u:Lcom/teamspeak/ts3client/data/d/v;

    .line 75
    sget-object v5, Lcom/teamspeak/ts3client/jni/g;->cg:Lcom/teamspeak/ts3client/jni/g;

    invoke-virtual {v4, v5}, Lcom/teamspeak/ts3client/data/d/v;->a(Lcom/teamspeak/ts3client/jni/g;)I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/teamspeak/ts3client/data/f/a;->b(I)I

    move-result v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/teamspeak/ts3client/d/b/aj;->az:I

    .line 9061
    iget-object v3, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 9181
    iget-object v3, v3, Lcom/teamspeak/ts3client/data/e;->s:Lcom/teamspeak/ts3client/data/f/a;

    .line 10061
    iget-object v4, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 10250
    iget-object v4, v4, Lcom/teamspeak/ts3client/data/e;->u:Lcom/teamspeak/ts3client/data/d/v;

    .line 76
    sget-object v5, Lcom/teamspeak/ts3client/jni/g;->ci:Lcom/teamspeak/ts3client/jni/g;

    invoke-virtual {v4, v5}, Lcom/teamspeak/ts3client/data/d/v;->a(Lcom/teamspeak/ts3client/jni/g;)I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/teamspeak/ts3client/data/f/a;->b(I)I

    move-result v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/teamspeak/ts3client/d/b/aj;->aA:I

    .line 77
    const-string v3, "groupdialog.servertext"

    const v4, 0x7f0c012d

    invoke-static {v3, v12, v4}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/ViewGroup;I)V

    .line 78
    const-string v3, "groupdialog.channeltext"

    const v4, 0x7f0c012f

    invoke-static {v3, v12, v4}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/ViewGroup;I)V

    .line 79
    const v3, 0x7f0c012e

    invoke-virtual {v12, v3}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TableLayout;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/teamspeak/ts3client/d/b/aj;->av:Landroid/widget/TableLayout;

    .line 80
    const v3, 0x7f0c0130

    invoke-virtual {v12, v3}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TableLayout;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/teamspeak/ts3client/d/b/aj;->aw:Landroid/widget/TableLayout;

    .line 81
    const v3, 0x7f0c0131

    invoke-virtual {v12, v3}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/teamspeak/ts3client/d/b/aj;->ax:Landroid/widget/Button;

    .line 82
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/teamspeak/ts3client/d/b/aj;->ax:Landroid/widget/Button;

    const-string v4, "button.close"

    invoke-static {v4}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 83
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/teamspeak/ts3client/d/b/aj;->ax:Landroid/widget/Button;

    new-instance v4, Lcom/teamspeak/ts3client/d/b/ak;

    move-object/from16 v0, p0

    invoke-direct {v4, v0}, Lcom/teamspeak/ts3client/d/b/ak;-><init>(Lcom/teamspeak/ts3client/d/b/aj;)V

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 90
    new-instance v10, Lcom/teamspeak/ts3client/d/b/am;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-direct {v10, v0, v3}, Lcom/teamspeak/ts3client/d/b/am;-><init>(Lcom/teamspeak/ts3client/d/b/aj;Z)V

    .line 11061
    iget-object v3, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 11234
    iget-object v3, v3, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 12061
    iget-object v4, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 12267
    iget-wide v4, v4, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 91
    sget-object v6, Lcom/teamspeak/ts3client/jni/i;->z:Lcom/teamspeak/ts3client/jni/i;

    invoke-virtual {v3, v4, v5, v6}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->c(JLcom/teamspeak/ts3client/jni/i;)J

    move-result-wide v14

    .line 92
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    .line 13061
    iget-object v3, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 13234
    iget-object v3, v3, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 14061
    iget-object v4, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 14267
    iget-wide v4, v4, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 93
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/teamspeak/ts3client/d/b/aj;->at:Lcom/teamspeak/ts3client/data/c;

    .line 15235
    iget v6, v6, Lcom/teamspeak/ts3client/data/c;->c:I

    .line 93
    sget-object v7, Lcom/teamspeak/ts3client/jni/d;->I:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v3, v4, v5, v6, v7}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->b(JILcom/teamspeak/ts3client/jni/d;)Ljava/lang/String;

    move-result-object v3

    .line 94
    if-nez v3, :cond_10f

    .line 95
    new-instance v12, Landroid/widget/LinearLayout;

    invoke-virtual/range {p0 .. p0}, Lcom/teamspeak/ts3client/d/b/aj;->i()Landroid/support/v4/app/bb;

    move-result-object v2

    invoke-direct {v12, v2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 96
    const/4 v2, 0x1

    invoke-virtual {v12, v2}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 97
    new-instance v2, Landroid/widget/TextView;

    invoke-virtual/range {p0 .. p0}, Lcom/teamspeak/ts3client/d/b/aj;->i()Landroid/support/v4/app/bb;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 98
    const-string v3, "The Client already left the Server"

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 99
    invoke-virtual {v12, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 100
    new-instance v2, Landroid/widget/Button;

    invoke-virtual/range {p0 .. p0}, Lcom/teamspeak/ts3client/d/b/aj;->i()Landroid/support/v4/app/bb;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 101
    const-string v3, "Close"

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 102
    new-instance v3, Lcom/teamspeak/ts3client/d/b/al;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/teamspeak/ts3client/d/b/al;-><init>(Lcom/teamspeak/ts3client/d/b/aj;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 109
    invoke-virtual {v12, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 159
    :cond_10e
    return-object v12

    .line 112
    :cond_10f
    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 113
    array-length v5, v4

    const/4 v3, 0x0

    :goto_117
    if-ge v3, v5, :cond_129

    aget-object v6, v4, v3

    .line 114
    invoke-static {v6}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v13, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 113
    add-int/lit8 v3, v3, 0x1

    goto :goto_117

    .line 16061
    :cond_129
    iget-object v3, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 16271
    iget-object v3, v3, Lcom/teamspeak/ts3client/data/e;->o:Lcom/teamspeak/ts3client/data/g/b;

    .line 17036
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 17037
    iget-object v3, v3, Lcom/teamspeak/ts3client/data/g/b;->a:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 17038
    invoke-static {v4}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 116
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v16

    :cond_142
    :goto_142
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_193

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/teamspeak/ts3client/data/g/a;

    .line 17074
    iget v4, v3, Lcom/teamspeak/ts3client/data/g/a;->c:I

    .line 117
    const/4 v5, 0x1

    if-ne v4, v5, :cond_142

    .line 119
    const/4 v4, 0x0

    .line 120
    const/4 v8, 0x0

    .line 121
    const/4 v11, 0x0

    .line 18046
    iget v5, v3, Lcom/teamspeak/ts3client/data/g/a;->e:I

    .line 122
    move-object/from16 v0, p0

    iget v6, v0, Lcom/teamspeak/ts3client/d/b/aj;->az:I

    if-gt v5, v6, :cond_165

    .line 18066
    iget-wide v6, v3, Lcom/teamspeak/ts3client/data/g/a;->a:J

    .line 122
    cmp-long v5, v6, v14

    if-eqz v5, :cond_165

    .line 123
    const/4 v4, 0x1

    .line 19066
    :cond_165
    iget-wide v6, v3, Lcom/teamspeak/ts3client/data/g/a;->a:J

    .line 124
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v13, v5}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_172

    .line 125
    const/4 v8, 0x1

    .line 126
    :cond_172
    if-eqz v8, :cond_242

    .line 20050
    iget v5, v3, Lcom/teamspeak/ts3client/data/g/a;->f:I

    .line 126
    move-object/from16 v0, p0

    iget v6, v0, Lcom/teamspeak/ts3client/d/b/aj;->aA:I

    if-le v5, v6, :cond_242

    .line 127
    const/4 v4, 0x0

    move v9, v4

    .line 20066
    :goto_17e
    iget-wide v4, v3, Lcom/teamspeak/ts3client/data/g/a;->a:J

    .line 128
    cmp-long v4, v4, v14

    if-nez v4, :cond_185

    .line 129
    const/4 v11, 0x1

    .line 130
    :cond_185
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/teamspeak/ts3client/d/b/aj;->av:Landroid/widget/TableLayout;

    .line 21038
    iget-object v5, v3, Lcom/teamspeak/ts3client/data/g/a;->b:Ljava/lang/String;

    .line 21066
    iget-wide v6, v3, Lcom/teamspeak/ts3client/data/g/a;->a:J

    move-object/from16 v3, p0

    .line 130
    invoke-direct/range {v3 .. v11}, Lcom/teamspeak/ts3client/d/b/aj;->a(Landroid/widget/TableLayout;Ljava/lang/String;JZZLcom/teamspeak/ts3client/d/b/am;Z)V

    goto :goto_142

    .line 134
    :cond_193
    new-instance v10, Lcom/teamspeak/ts3client/d/b/am;

    const/4 v3, 0x1

    move-object/from16 v0, p0

    invoke-direct {v10, v0, v3}, Lcom/teamspeak/ts3client/d/b/am;-><init>(Lcom/teamspeak/ts3client/d/b/aj;Z)V

    .line 22061
    iget-object v3, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 22234
    iget-object v3, v3, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 23061
    iget-object v4, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 23267
    iget-wide v4, v4, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 135
    sget-object v6, Lcom/teamspeak/ts3client/jni/i;->A:Lcom/teamspeak/ts3client/jni/i;

    invoke-virtual {v3, v4, v5, v6}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->c(JLcom/teamspeak/ts3client/jni/i;)J

    move-result-wide v14

    .line 136
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    .line 24061
    iget-object v3, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 24234
    iget-object v3, v3, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 25061
    iget-object v4, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 25267
    iget-wide v4, v4, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 137
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/teamspeak/ts3client/d/b/aj;->at:Lcom/teamspeak/ts3client/data/c;

    .line 26235
    iget v6, v6, Lcom/teamspeak/ts3client/data/c;->c:I

    .line 137
    sget-object v7, Lcom/teamspeak/ts3client/jni/d;->H:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v3, v4, v5, v6, v7}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->b(JILcom/teamspeak/ts3client/jni/d;)Ljava/lang/String;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 138
    array-length v5, v4

    const/4 v3, 0x0

    :goto_1ca
    if-ge v3, v5, :cond_1dc

    aget-object v6, v4, v3

    .line 139
    invoke-static {v6}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v13, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 138
    add-int/lit8 v3, v3, 0x1

    goto :goto_1ca

    .line 27061
    :cond_1dc
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 27161
    iget-object v2, v2, Lcom/teamspeak/ts3client/data/e;->p:Lcom/teamspeak/ts3client/data/a/b;

    .line 28036
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 28037
    iget-object v2, v2, Lcom/teamspeak/ts3client/data/a/b;->a:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 28038
    invoke-static {v3}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 142
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v16

    :cond_1f5
    :goto_1f5
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_10e

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/teamspeak/ts3client/data/a/a;

    .line 28077
    iget v3, v2, Lcom/teamspeak/ts3client/data/a/a;->c:I

    .line 143
    const/4 v4, 0x1

    if-ne v3, v4, :cond_1f5

    .line 145
    const/4 v3, 0x0

    .line 146
    const/4 v8, 0x0

    .line 147
    const/4 v11, 0x0

    .line 29053
    iget v4, v2, Lcom/teamspeak/ts3client/data/a/a;->e:I

    .line 148
    move-object/from16 v0, p0

    iget v5, v0, Lcom/teamspeak/ts3client/d/b/aj;->az:I

    if-gt v4, v5, :cond_212

    .line 149
    const/4 v3, 0x1

    .line 30037
    :cond_212
    iget-wide v4, v2, Lcom/teamspeak/ts3client/data/a/a;->a:J

    .line 150
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v13, v4}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_21f

    .line 151
    const/4 v8, 0x1

    .line 152
    :cond_21f
    if-eqz v8, :cond_240

    .line 30057
    iget v4, v2, Lcom/teamspeak/ts3client/data/a/a;->f:I

    .line 152
    move-object/from16 v0, p0

    iget v5, v0, Lcom/teamspeak/ts3client/d/b/aj;->aA:I

    if-le v4, v5, :cond_240

    .line 153
    const/4 v3, 0x0

    move v9, v3

    .line 31037
    :goto_22b
    iget-wide v4, v2, Lcom/teamspeak/ts3client/data/a/a;->a:J

    .line 154
    cmp-long v3, v4, v14

    if-nez v3, :cond_232

    .line 155
    const/4 v11, 0x1

    .line 156
    :cond_232
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/teamspeak/ts3client/d/b/aj;->aw:Landroid/widget/TableLayout;

    .line 31045
    iget-object v5, v2, Lcom/teamspeak/ts3client/data/a/a;->b:Ljava/lang/String;

    .line 32037
    iget-wide v6, v2, Lcom/teamspeak/ts3client/data/a/a;->a:J

    move-object/from16 v3, p0

    .line 156
    invoke-direct/range {v3 .. v11}, Lcom/teamspeak/ts3client/d/b/aj;->a(Landroid/widget/TableLayout;Ljava/lang/String;JZZLcom/teamspeak/ts3client/d/b/am;Z)V

    goto :goto_1f5

    :cond_240
    move v9, v3

    goto :goto_22b

    :cond_242
    move v9, v4

    goto/16 :goto_17e
.end method

.method public final a(Landroid/os/Bundle;)V
    .registers 7

    .prologue
    .line 65
    invoke-super {p0, p1}, Landroid/support/v4/app/ax;->a(Landroid/os/Bundle;)V

    .line 66
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/d/b/aj;->i()Landroid/support/v4/app/bb;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/bb;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/Ts3Application;

    iput-object v0, p0, Lcom/teamspeak/ts3client/d/b/aj;->au:Lcom/teamspeak/ts3client/Ts3Application;

    .line 67
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/b/aj;->au:Lcom/teamspeak/ts3client/Ts3Application;

    .line 2061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 2234
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 67
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/b/aj;->au:Lcom/teamspeak/ts3client/Ts3Application;

    .line 3061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 3267
    iget-wide v2, v1, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 67
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/b/aj;->at:Lcom/teamspeak/ts3client/data/c;

    .line 4235
    iget v1, v1, Lcom/teamspeak/ts3client/data/c;->c:I

    .line 67
    sget-object v4, Lcom/teamspeak/ts3client/jni/d;->G:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v0, v2, v3, v1, v4}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->c(JILcom/teamspeak/ts3client/jni/d;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/teamspeak/ts3client/d/b/aj;->ay:J

    .line 68
    return-void
.end method

.method public final e()V
    .registers 1

    .prologue
    .line 164
    invoke-super {p0}, Landroid/support/v4/app/ax;->e()V

    .line 165
    invoke-static {p0}, Lcom/teamspeak/ts3client/data/d/q;->a(Landroid/support/v4/app/ax;)V

    .line 166
    return-void
.end method

.method public final f()V
    .registers 1

    .prologue
    .line 170
    invoke-super {p0}, Landroid/support/v4/app/ax;->f()V

    .line 171
    invoke-static {p0}, Lcom/teamspeak/ts3client/data/d/q;->b(Landroid/support/v4/app/ax;)V

    .line 173
    return-void
.end method
