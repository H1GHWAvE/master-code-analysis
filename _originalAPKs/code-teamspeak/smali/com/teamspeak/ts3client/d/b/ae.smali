.class final Lcom/teamspeak/ts3client/d/b/ae;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/teamspeak/ts3client/Ts3Application;

.field final synthetic b:Landroid/widget/RelativeLayout;

.field final synthetic c:Lcom/teamspeak/ts3client/d/b/ad;


# direct methods
.method constructor <init>(Lcom/teamspeak/ts3client/d/b/ad;Lcom/teamspeak/ts3client/Ts3Application;Landroid/widget/RelativeLayout;)V
    .registers 4

    .prologue
    .line 62
    iput-object p1, p0, Lcom/teamspeak/ts3client/d/b/ae;->c:Lcom/teamspeak/ts3client/d/b/ad;

    iput-object p2, p0, Lcom/teamspeak/ts3client/d/b/ae;->a:Lcom/teamspeak/ts3client/Ts3Application;

    iput-object p3, p0, Lcom/teamspeak/ts3client/d/b/ae;->b:Landroid/widget/RelativeLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .registers 10

    .prologue
    .line 65
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/view/View;->performHapticFeedback(I)Z

    .line 66
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/b/ae;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 1061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 1234
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 66
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/b/ae;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 2061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 2267
    iget-wide v2, v1, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 66
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/b/ae;->c:Lcom/teamspeak/ts3client/d/b/ad;

    invoke-static {v1}, Lcom/teamspeak/ts3client/d/b/ad;->a(Lcom/teamspeak/ts3client/d/b/ad;)Lcom/teamspeak/ts3client/data/c;

    move-result-object v1

    .line 3235
    iget v1, v1, Lcom/teamspeak/ts3client/data/c;->c:I

    .line 66
    sget-object v4, Lcom/teamspeak/ts3client/jni/d;->G:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v0, v2, v3, v1, v4}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->c(JILcom/teamspeak/ts3client/jni/d;)J

    move-result-wide v4

    .line 67
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/b/ae;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 4061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 4234
    iget-object v1, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 67
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/b/ae;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 5061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 5267
    iget-wide v2, v0, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 67
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/b/ae;->c:Lcom/teamspeak/ts3client/d/b/ad;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/b/ad;->b(Lcom/teamspeak/ts3client/d/b/ad;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v7, "Complain "

    invoke-direct {v0, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual/range {v1 .. v7}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_requestComplainAdd(JJLjava/lang/String;Ljava/lang/String;)I

    .line 68
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/b/ae;->c:Lcom/teamspeak/ts3client/d/b/ad;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/d/b/ad;->b()V

    .line 69
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/b/ae;->b:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->removeAllViews()V

    .line 70
    return-void
.end method
