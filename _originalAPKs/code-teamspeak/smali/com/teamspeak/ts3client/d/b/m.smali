.class final Lcom/teamspeak/ts3client/d/b/m;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/teamspeak/ts3client/Ts3Application;

.field final synthetic b:Lcom/teamspeak/ts3client/d/b/a;


# direct methods
.method constructor <init>(Lcom/teamspeak/ts3client/d/b/a;Lcom/teamspeak/ts3client/Ts3Application;)V
    .registers 3

    .prologue
    .line 424
    iput-object p1, p0, Lcom/teamspeak/ts3client/d/b/m;->b:Lcom/teamspeak/ts3client/d/b/a;

    iput-object p2, p0, Lcom/teamspeak/ts3client/d/b/m;->a:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .registers 10

    .prologue
    .line 428
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/b/m;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 1061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 1177
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->i:Ljava/util/HashMap;

    .line 428
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/b/m;->b:Lcom/teamspeak/ts3client/d/b/a;

    invoke-static {v1}, Lcom/teamspeak/ts3client/d/b/a;->a(Lcom/teamspeak/ts3client/d/b/a;)Lcom/teamspeak/ts3client/data/c;

    move-result-object v1

    .line 1235
    iget v1, v1, Lcom/teamspeak/ts3client/data/c;->c:I

    .line 428
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1e

    .line 429
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/b/m;->b:Lcom/teamspeak/ts3client/d/b/a;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/d/b/a;->b()V

    .line 468
    :goto_1d
    return-void

    .line 432
    :cond_1e
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/b/m;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 2061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 2165
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->f:Lcom/teamspeak/ts3client/data/g;

    .line 3080
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/g;->a:Ljava/util/List;

    .line 433
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 434
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2f
    :goto_2f
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_47

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/data/a;

    .line 3087
    iget-wide v4, v0, Lcom/teamspeak/ts3client/data/a;->b:J

    .line 435
    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-eqz v3, :cond_2f

    .line 437
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2f

    .line 439
    :cond_47
    new-instance v0, Landroid/app/Dialog;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    .line 440
    invoke-static {v0}, Lcom/teamspeak/ts3client/data/d/q;->a(Landroid/app/Dialog;)V

    .line 441
    new-instance v2, Landroid/widget/LinearLayout;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 442
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 443
    new-instance v3, Landroid/widget/Spinner;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;)V

    .line 444
    new-instance v4, Landroid/view/ViewGroup$LayoutParams;

    const/4 v5, -0x1

    const/4 v6, -0x2

    invoke-direct {v4, v5, v6}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v3, v4}, Landroid/widget/Spinner;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 445
    new-instance v4, Landroid/widget/ArrayAdapter;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v5

    const v6, 0x1090009

    invoke-direct {v4, v5, v6, v1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 446
    invoke-virtual {v3, v4}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 447
    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 448
    new-instance v1, Landroid/widget/Button;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v1, v5}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 449
    const-string v5, "dialog.client.move.dialog.info"

    invoke-static {v5}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 450
    new-instance v5, Lcom/teamspeak/ts3client/d/b/n;

    invoke-direct {v5, p0, v4, v3, v0}, Lcom/teamspeak/ts3client/d/b/n;-><init>(Lcom/teamspeak/ts3client/d/b/m;Landroid/widget/ArrayAdapter;Landroid/widget/Spinner;Landroid/app/Dialog;)V

    invoke-virtual {v1, v5}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 463
    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 464
    invoke-virtual {v0, v2}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    .line 465
    const-string v1, "dialog.client.move.dialog.text"

    invoke-static {v1}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 466
    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 467
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/b/m;->b:Lcom/teamspeak/ts3client/d/b/a;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/d/b/a;->b()V

    goto/16 :goto_1d
.end method
