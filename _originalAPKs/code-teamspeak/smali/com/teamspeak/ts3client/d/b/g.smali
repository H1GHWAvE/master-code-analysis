.class final Lcom/teamspeak/ts3client/d/b/g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Landroid/widget/SeekBar;

.field final synthetic b:Landroid/widget/RelativeLayout;

.field final synthetic c:Landroid/app/Dialog;

.field final synthetic d:Lcom/teamspeak/ts3client/d/b/e;


# direct methods
.method constructor <init>(Lcom/teamspeak/ts3client/d/b/e;Landroid/widget/SeekBar;Landroid/widget/RelativeLayout;Landroid/app/Dialog;)V
    .registers 5

    .prologue
    .line 309
    iput-object p1, p0, Lcom/teamspeak/ts3client/d/b/g;->d:Lcom/teamspeak/ts3client/d/b/e;

    iput-object p2, p0, Lcom/teamspeak/ts3client/d/b/g;->a:Landroid/widget/SeekBar;

    iput-object p3, p0, Lcom/teamspeak/ts3client/d/b/g;->b:Landroid/widget/RelativeLayout;

    iput-object p4, p0, Lcom/teamspeak/ts3client/d/b/g;->c:Landroid/app/Dialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .registers 6

    .prologue
    const/high16 v2, 0x3f000000    # 0.5f

    .line 312
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/b/g;->d:Lcom/teamspeak/ts3client/d/b/e;

    iget-object v0, v0, Lcom/teamspeak/ts3client/d/b/e;->b:Lcom/teamspeak/ts3client/d/b/a;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/b/a;->a(Lcom/teamspeak/ts3client/d/b/a;)Lcom/teamspeak/ts3client/data/c;

    move-result-object v0

    .line 1211
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/c;->w:Lcom/teamspeak/ts3client/c/a;

    .line 312
    if-eqz v0, :cond_4b

    .line 313
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/b/g;->d:Lcom/teamspeak/ts3client/d/b/e;

    iget-object v0, v0, Lcom/teamspeak/ts3client/d/b/e;->b:Lcom/teamspeak/ts3client/d/b/a;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/b/a;->a(Lcom/teamspeak/ts3client/d/b/a;)Lcom/teamspeak/ts3client/data/c;

    move-result-object v0

    .line 2211
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/c;->w:Lcom/teamspeak/ts3client/c/a;

    .line 313
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/b/g;->a:Landroid/widget/SeekBar;

    invoke-virtual {v1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v1

    add-int/lit8 v1, v1, -0x3c

    int-to-float v1, v1

    mul-float/2addr v1, v2

    .line 3122
    iput v1, v0, Lcom/teamspeak/ts3client/c/a;->m:F

    .line 4042
    sget-object v0, Lcom/teamspeak/ts3client/data/b/c;->a:Lcom/teamspeak/ts3client/data/b/c;

    .line 314
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/b/g;->d:Lcom/teamspeak/ts3client/d/b/e;

    iget-object v1, v1, Lcom/teamspeak/ts3client/d/b/e;->b:Lcom/teamspeak/ts3client/d/b/a;

    invoke-static {v1}, Lcom/teamspeak/ts3client/d/b/a;->a(Lcom/teamspeak/ts3client/d/b/a;)Lcom/teamspeak/ts3client/data/c;

    move-result-object v1

    .line 4211
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/c;->w:Lcom/teamspeak/ts3client/c/a;

    .line 5038
    iget v1, v1, Lcom/teamspeak/ts3client/c/a;->b:I

    .line 314
    int-to-long v2, v1

    iget-object v1, p0, Lcom/teamspeak/ts3client/d/b/g;->d:Lcom/teamspeak/ts3client/d/b/e;

    iget-object v1, v1, Lcom/teamspeak/ts3client/d/b/e;->b:Lcom/teamspeak/ts3client/d/b/a;

    invoke-static {v1}, Lcom/teamspeak/ts3client/d/b/a;->a(Lcom/teamspeak/ts3client/d/b/a;)Lcom/teamspeak/ts3client/data/c;

    move-result-object v1

    .line 5211
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/c;->w:Lcom/teamspeak/ts3client/c/a;

    .line 314
    invoke-virtual {v0, v2, v3, v1}, Lcom/teamspeak/ts3client/data/b/c;->a(JLcom/teamspeak/ts3client/c/a;)Z

    .line 324
    :goto_40
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/b/g;->b:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->removeAllViews()V

    .line 325
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/b/g;->c:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 326
    return-void

    .line 316
    :cond_4b
    new-instance v0, Lcom/teamspeak/ts3client/c/a;

    invoke-direct {v0}, Lcom/teamspeak/ts3client/c/a;-><init>()V

    .line 317
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/b/g;->d:Lcom/teamspeak/ts3client/d/b/e;

    iget-object v1, v1, Lcom/teamspeak/ts3client/d/b/e;->b:Lcom/teamspeak/ts3client/d/b/a;

    invoke-static {v1}, Lcom/teamspeak/ts3client/d/b/a;->a(Lcom/teamspeak/ts3client/d/b/a;)Lcom/teamspeak/ts3client/data/c;

    move-result-object v1

    .line 6188
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/c;->b:Ljava/lang/String;

    .line 317
    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/c/a;->a(Ljava/lang/String;)V

    .line 318
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/b/g;->d:Lcom/teamspeak/ts3client/d/b/e;

    iget-object v1, v1, Lcom/teamspeak/ts3client/d/b/e;->b:Lcom/teamspeak/ts3client/d/b/a;

    invoke-static {v1}, Lcom/teamspeak/ts3client/d/b/a;->a(Lcom/teamspeak/ts3client/d/b/a;)Lcom/teamspeak/ts3client/data/c;

    move-result-object v1

    .line 6203
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/c;->a:Ljava/lang/String;

    .line 7034
    iput-object v1, v0, Lcom/teamspeak/ts3client/c/a;->a:Ljava/lang/String;

    .line 319
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/b/g;->a:Landroid/widget/SeekBar;

    invoke-virtual {v1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v1

    add-int/lit8 v1, v1, -0x3c

    int-to-float v1, v1

    mul-float/2addr v1, v2

    .line 7122
    iput v1, v0, Lcom/teamspeak/ts3client/c/a;->m:F

    .line 8042
    sget-object v1, Lcom/teamspeak/ts3client/data/b/c;->a:Lcom/teamspeak/ts3client/data/b/c;

    .line 320
    invoke-virtual {v1, v0}, Lcom/teamspeak/ts3client/data/b/c;->a(Lcom/teamspeak/ts3client/c/a;)J

    .line 9042
    sget-object v0, Lcom/teamspeak/ts3client/data/b/c;->a:Lcom/teamspeak/ts3client/data/b/c;

    .line 321
    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/b/c;->c()V

    goto :goto_40
.end method
