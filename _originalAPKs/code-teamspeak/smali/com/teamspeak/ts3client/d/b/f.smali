.class final Lcom/teamspeak/ts3client/d/b/f;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# instance fields
.field final synthetic a:Landroid/widget/TextView;

.field final synthetic b:Lcom/teamspeak/ts3client/d/b/e;


# direct methods
.method constructor <init>(Lcom/teamspeak/ts3client/d/b/e;Landroid/widget/TextView;)V
    .registers 3

    .prologue
    .line 271
    iput-object p1, p0, Lcom/teamspeak/ts3client/d/b/f;->b:Lcom/teamspeak/ts3client/d/b/e;

    iput-object p2, p0, Lcom/teamspeak/ts3client/d/b/f;->a:Landroid/widget/TextView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .registers 10

    .prologue
    const/high16 v5, 0x3f000000    # 0.5f

    .line 275
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/b/f;->a:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    add-int/lit8 v2, p2, -0x3c

    int-to-float v2, v2

    mul-float/2addr v2, v5

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " / 30"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 276
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/b/f;->b:Lcom/teamspeak/ts3client/d/b/e;

    iget-object v0, v0, Lcom/teamspeak/ts3client/d/b/e;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 1061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 276
    if-eqz v0, :cond_43

    .line 277
    invoke-static {}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->b()Lcom/teamspeak/ts3client/jni/Ts3Jni;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/d/b/f;->b:Lcom/teamspeak/ts3client/d/b/e;

    iget-object v1, v1, Lcom/teamspeak/ts3client/d/b/e;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 2061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 2267
    iget-wide v2, v1, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 277
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/b/f;->b:Lcom/teamspeak/ts3client/d/b/e;

    iget-object v1, v1, Lcom/teamspeak/ts3client/d/b/e;->b:Lcom/teamspeak/ts3client/d/b/a;

    invoke-static {v1}, Lcom/teamspeak/ts3client/d/b/a;->a(Lcom/teamspeak/ts3client/d/b/a;)Lcom/teamspeak/ts3client/data/c;

    move-result-object v1

    .line 3235
    iget v1, v1, Lcom/teamspeak/ts3client/data/c;->c:I

    .line 277
    add-int/lit8 v4, p2, -0x3c

    int-to-float v4, v4

    mul-float/2addr v4, v5

    invoke-virtual {v0, v2, v3, v1, v4}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_setClientVolumeModifier(JIF)I

    .line 278
    :cond_43
    return-void
.end method

.method public final onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .registers 2

    .prologue
    .line 284
    return-void
.end method

.method public final onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .registers 2

    .prologue
    .line 290
    return-void
.end method
