.class public Lcom/teamspeak/ts3client/Ts3Application;
.super Landroid/app/Application;
.source "SourceFile"


# static fields
.field private static p:Lcom/teamspeak/ts3client/Ts3Application;


# instance fields
.field public a:Lcom/teamspeak/ts3client/data/e;

.field public b:Landroid/os/PowerManager$WakeLock;

.field public c:Landroid/support/v4/app/Fragment;

.field public d:Ljava/util/logging/Logger;

.field public e:Landroid/content/SharedPreferences;

.field public f:Lcom/teamspeak/ts3client/StartGUIFragment;

.field g:Z

.field public h:Landroid/net/ConnectivityManager;

.field i:Ljava/util/Vector;

.field j:Ljava/util/Vector;

.field public k:Landroid/content/Intent;

.field public l:Landroid/app/NotificationManager;

.field public m:Landroid/os/PowerManager$WakeLock;

.field public n:Landroid/net/wifi/WifiManager$WifiLock;

.field public o:Landroid/support/v7/app/a;

.field private q:Z

.field private r:Lcom/teamspeak/ts3client/data/d/f;

.field private s:Lcom/teamspeak/ts3client/a/p;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 20
    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    return-void
.end method

.method public static a()Lcom/teamspeak/ts3client/Ts3Application;
    .registers 1

    .prologue
    .line 43
    sget-object v0, Lcom/teamspeak/ts3client/Ts3Application;->p:Lcom/teamspeak/ts3client/Ts3Application;

    return-object v0
.end method

.method private a(Landroid/app/NotificationManager;)V
    .registers 2

    .prologue
    .line 200
    iput-object p1, p0, Lcom/teamspeak/ts3client/Ts3Application;->l:Landroid/app/NotificationManager;

    .line 201
    return-void
.end method

.method private a(Landroid/content/Intent;)V
    .registers 2

    .prologue
    .line 192
    iput-object p1, p0, Lcom/teamspeak/ts3client/Ts3Application;->k:Landroid/content/Intent;

    .line 193
    return-void
.end method

.method private a(Landroid/content/SharedPreferences;)V
    .registers 2

    .prologue
    .line 97
    iput-object p1, p0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 98
    return-void
.end method

.method private a(Landroid/net/ConnectivityManager;)V
    .registers 2

    .prologue
    .line 73
    iput-object p1, p0, Lcom/teamspeak/ts3client/Ts3Application;->h:Landroid/net/ConnectivityManager;

    .line 74
    return-void
.end method

.method private a(Landroid/net/wifi/WifiManager$WifiLock;)V
    .registers 2

    .prologue
    .line 135
    iput-object p1, p0, Lcom/teamspeak/ts3client/Ts3Application;->n:Landroid/net/wifi/WifiManager$WifiLock;

    .line 136
    return-void
.end method

.method private a(Landroid/os/PowerManager$WakeLock;)V
    .registers 2

    .prologue
    .line 119
    iput-object p1, p0, Lcom/teamspeak/ts3client/Ts3Application;->b:Landroid/os/PowerManager$WakeLock;

    .line 120
    return-void
.end method

.method private a(Landroid/support/v4/app/Fragment;)V
    .registers 2

    .prologue
    .line 81
    iput-object p1, p0, Lcom/teamspeak/ts3client/Ts3Application;->c:Landroid/support/v4/app/Fragment;

    .line 82
    return-void
.end method

.method private a(Landroid/support/v7/app/a;)V
    .registers 2

    .prologue
    .line 208
    iput-object p1, p0, Lcom/teamspeak/ts3client/Ts3Application;->o:Landroid/support/v7/app/a;

    .line 209
    return-void
.end method

.method private a(Lcom/teamspeak/ts3client/StartGUIFragment;)V
    .registers 2

    .prologue
    .line 111
    iput-object p1, p0, Lcom/teamspeak/ts3client/Ts3Application;->f:Lcom/teamspeak/ts3client/StartGUIFragment;

    .line 112
    return-void
.end method

.method private a(Lcom/teamspeak/ts3client/data/e;)V
    .registers 2

    .prologue
    .line 65
    iput-object p1, p0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 66
    return-void
.end method

.method private a(Ljava/util/Vector;)V
    .registers 2

    .prologue
    .line 168
    iput-object p1, p0, Lcom/teamspeak/ts3client/Ts3Application;->j:Ljava/util/Vector;

    .line 169
    return-void
.end method

.method private a(Ljava/util/logging/Logger;)V
    .registers 2

    .prologue
    .line 89
    iput-object p1, p0, Lcom/teamspeak/ts3client/Ts3Application;->d:Ljava/util/logging/Logger;

    .line 90
    return-void
.end method

.method private a(Z)V
    .registers 2

    .prologue
    .line 147
    iput-boolean p1, p0, Lcom/teamspeak/ts3client/Ts3Application;->q:Z

    .line 148
    return-void
.end method

.method private b(Landroid/os/PowerManager$WakeLock;)V
    .registers 2

    .prologue
    .line 127
    iput-object p1, p0, Lcom/teamspeak/ts3client/Ts3Application;->m:Landroid/os/PowerManager$WakeLock;

    .line 128
    return-void
.end method

.method private b(Ljava/util/Vector;)V
    .registers 2

    .prologue
    .line 180
    iput-object p1, p0, Lcom/teamspeak/ts3client/Ts3Application;->i:Ljava/util/Vector;

    .line 181
    return-void
.end method

.method private k()V
    .registers 2

    .prologue
    .line 47
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/teamspeak/ts3client/Ts3Application;->g:Z

    .line 48
    return-void
.end method

.method private l()V
    .registers 2

    .prologue
    .line 51
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/teamspeak/ts3client/Ts3Application;->g:Z

    .line 52
    return-void
.end method

.method private m()Landroid/net/ConnectivityManager;
    .registers 2

    .prologue
    .line 69
    iget-object v0, p0, Lcom/teamspeak/ts3client/Ts3Application;->h:Landroid/net/ConnectivityManager;

    return-object v0
.end method

.method private n()Lcom/teamspeak/ts3client/StartGUIFragment;
    .registers 2

    .prologue
    .line 107
    iget-object v0, p0, Lcom/teamspeak/ts3client/Ts3Application;->f:Lcom/teamspeak/ts3client/StartGUIFragment;

    return-object v0
.end method

.method private o()Landroid/os/PowerManager$WakeLock;
    .registers 2

    .prologue
    .line 115
    iget-object v0, p0, Lcom/teamspeak/ts3client/Ts3Application;->b:Landroid/os/PowerManager$WakeLock;

    return-object v0
.end method

.method private p()Landroid/os/PowerManager$WakeLock;
    .registers 2

    .prologue
    .line 123
    iget-object v0, p0, Lcom/teamspeak/ts3client/Ts3Application;->m:Landroid/os/PowerManager$WakeLock;

    return-object v0
.end method

.method private q()Landroid/net/wifi/WifiManager$WifiLock;
    .registers 2

    .prologue
    .line 131
    iget-object v0, p0, Lcom/teamspeak/ts3client/Ts3Application;->n:Landroid/net/wifi/WifiManager$WifiLock;

    return-object v0
.end method

.method private r()Z
    .registers 2

    .prologue
    .line 139
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/Ts3Application;->g:Z

    return v0
.end method

.method private s()Z
    .registers 2

    .prologue
    .line 143
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/Ts3Application;->q:Z

    return v0
.end method

.method private static t()Z
    .registers 1

    .prologue
    .line 160
    const/4 v0, 0x0

    return v0
.end method

.method private u()Ljava/util/Vector;
    .registers 2

    .prologue
    .line 172
    iget-object v0, p0, Lcom/teamspeak/ts3client/Ts3Application;->j:Ljava/util/Vector;

    return-object v0
.end method

.method private v()Ljava/util/Vector;
    .registers 2

    .prologue
    .line 184
    iget-object v0, p0, Lcom/teamspeak/ts3client/Ts3Application;->i:Ljava/util/Vector;

    return-object v0
.end method

.method private w()Landroid/content/Intent;
    .registers 2

    .prologue
    .line 188
    iget-object v0, p0, Lcom/teamspeak/ts3client/Ts3Application;->k:Landroid/content/Intent;

    return-object v0
.end method

.method private x()Landroid/support/v7/app/a;
    .registers 2

    .prologue
    .line 204
    iget-object v0, p0, Lcom/teamspeak/ts3client/Ts3Application;->o:Landroid/support/v7/app/a;

    return-object v0
.end method


# virtual methods
.method public final b()Lcom/teamspeak/ts3client/data/d/f;
    .registers 2

    .prologue
    .line 55
    iget-object v0, p0, Lcom/teamspeak/ts3client/Ts3Application;->r:Lcom/teamspeak/ts3client/data/d/f;

    if-nez v0, :cond_b

    .line 56
    new-instance v0, Lcom/teamspeak/ts3client/data/d/f;

    invoke-direct {v0, p0}, Lcom/teamspeak/ts3client/data/d/f;-><init>(Lcom/teamspeak/ts3client/Ts3Application;)V

    iput-object v0, p0, Lcom/teamspeak/ts3client/Ts3Application;->r:Lcom/teamspeak/ts3client/data/d/f;

    .line 57
    :cond_b
    iget-object v0, p0, Lcom/teamspeak/ts3client/Ts3Application;->r:Lcom/teamspeak/ts3client/data/d/f;

    return-object v0
.end method

.method public final c()Lcom/teamspeak/ts3client/data/e;
    .registers 2

    .prologue
    .line 61
    iget-object v0, p0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    return-object v0
.end method

.method public final d()Landroid/support/v4/app/Fragment;
    .registers 2

    .prologue
    .line 77
    iget-object v0, p0, Lcom/teamspeak/ts3client/Ts3Application;->c:Landroid/support/v4/app/Fragment;

    return-object v0
.end method

.method public final e()Ljava/util/logging/Logger;
    .registers 2

    .prologue
    .line 85
    iget-object v0, p0, Lcom/teamspeak/ts3client/Ts3Application;->d:Ljava/util/logging/Logger;

    return-object v0
.end method

.method public final f()Landroid/content/SharedPreferences;
    .registers 2

    .prologue
    .line 93
    iget-object v0, p0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method public final g()Lcom/teamspeak/ts3client/a/p;
    .registers 2

    .prologue
    .line 101
    iget-object v0, p0, Lcom/teamspeak/ts3client/Ts3Application;->s:Lcom/teamspeak/ts3client/a/p;

    if-nez v0, :cond_b

    .line 102
    new-instance v0, Lcom/teamspeak/ts3client/a/p;

    invoke-direct {v0, p0}, Lcom/teamspeak/ts3client/a/p;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/teamspeak/ts3client/Ts3Application;->s:Lcom/teamspeak/ts3client/a/p;

    .line 103
    :cond_b
    iget-object v0, p0, Lcom/teamspeak/ts3client/Ts3Application;->s:Lcom/teamspeak/ts3client/a/p;

    return-object v0
.end method

.method public final h()I
    .registers 3

    .prologue
    .line 164
    iget-object v0, p0, Lcom/teamspeak/ts3client/Ts3Application;->j:Ljava/util/Vector;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public final i()I
    .registers 3

    .prologue
    .line 176
    iget-object v0, p0, Lcom/teamspeak/ts3client/Ts3Application;->i:Ljava/util/Vector;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public final j()Landroid/app/NotificationManager;
    .registers 2

    .prologue
    .line 196
    iget-object v0, p0, Lcom/teamspeak/ts3client/Ts3Application;->l:Landroid/app/NotificationManager;

    return-object v0
.end method

.method public onCreate()V
    .registers 1

    .prologue
    .line 154
    sput-object p0, Lcom/teamspeak/ts3client/Ts3Application;->p:Lcom/teamspeak/ts3client/Ts3Application;

    .line 155
    invoke-super {p0}, Landroid/app/Application;->onCreate()V

    .line 156
    return-void
.end method
