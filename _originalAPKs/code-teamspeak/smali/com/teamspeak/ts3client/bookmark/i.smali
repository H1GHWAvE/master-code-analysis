.class final Lcom/teamspeak/ts3client/bookmark/i;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Lcom/teamspeak/ts3client/bookmark/e;


# direct methods
.method constructor <init>(Lcom/teamspeak/ts3client/bookmark/e;)V
    .registers 2

    .prologue
    .line 134
    iput-object p1, p0, Lcom/teamspeak/ts3client/bookmark/i;->a:Lcom/teamspeak/ts3client/bookmark/e;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .registers 12

    .prologue
    const/4 v5, -0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 138
    iget-object v0, p0, Lcom/teamspeak/ts3client/bookmark/i;->a:Lcom/teamspeak/ts3client/bookmark/e;

    iget-boolean v0, v0, Lcom/teamspeak/ts3client/bookmark/e;->a:Z

    if-nez v0, :cond_65

    .line 139
    iget-object v0, p0, Lcom/teamspeak/ts3client/bookmark/i;->a:Lcom/teamspeak/ts3client/bookmark/e;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/bookmark/e;->i()Landroid/support/v4/app/bb;

    move-result-object v0

    .line 1072
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x11

    if-lt v3, v4, :cond_68

    .line 1073
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v3, "airplane_mode_on"

    invoke-static {v0, v3, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_66

    move v0, v1

    .line 139
    :goto_22
    if-eqz v0, :cond_78

    iget-object v0, p0, Lcom/teamspeak/ts3client/bookmark/i;->a:Lcom/teamspeak/ts3client/bookmark/e;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/bookmark/e;->i()Landroid/support/v4/app/bb;

    move-result-object v0

    invoke-static {v0}, Lcom/teamspeak/ts3client/data/d/u;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_78

    .line 140
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/teamspeak/ts3client/bookmark/i;->a:Lcom/teamspeak/ts3client/bookmark/e;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/bookmark/e;->i()Landroid/support/v4/app/bb;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 141
    const-string v1, "network.airplane"

    invoke-static {v1}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 142
    const-string v1, "network.airplane.text"

    invoke-static {v1}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 143
    const-string v1, "button.cancel"

    invoke-static {v1}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v3, Lcom/teamspeak/ts3client/bookmark/j;

    invoke-direct {v3, p0, v0}, Lcom/teamspeak/ts3client/bookmark/j;-><init>(Lcom/teamspeak/ts3client/bookmark/i;Landroid/app/AlertDialog;)V

    invoke-virtual {v0, v5, v1, v3}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 150
    invoke-virtual {v0, v2}, Landroid/app/AlertDialog;->setCancelable(Z)V

    .line 151
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 172
    :cond_65
    :goto_65
    return-void

    :cond_66
    move v0, v2

    .line 1073
    goto :goto_22

    .line 1075
    :cond_68
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v3, "airplane_mode_on"

    invoke-static {v0, v3, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_76

    move v0, v1

    goto :goto_22

    :cond_76
    move v0, v2

    goto :goto_22

    .line 152
    :cond_78
    iget-object v0, p0, Lcom/teamspeak/ts3client/bookmark/i;->a:Lcom/teamspeak/ts3client/bookmark/e;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/bookmark/e;->i()Landroid/support/v4/app/bb;

    move-result-object v0

    invoke-static {v0}, Lcom/teamspeak/ts3client/data/d/u;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_ba

    .line 153
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/teamspeak/ts3client/bookmark/i;->a:Lcom/teamspeak/ts3client/bookmark/e;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/bookmark/e;->i()Landroid/support/v4/app/bb;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 154
    const-string v1, "network.noconnection"

    invoke-static {v1}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 155
    const-string v1, "network.noconnection.text"

    invoke-static {v1}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 156
    const-string v1, "button.cancel"

    invoke-static {v1}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v3, Lcom/teamspeak/ts3client/bookmark/k;

    invoke-direct {v3, p0, v0}, Lcom/teamspeak/ts3client/bookmark/k;-><init>(Lcom/teamspeak/ts3client/bookmark/i;Landroid/app/AlertDialog;)V

    invoke-virtual {v0, v5, v1, v3}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 163
    invoke-virtual {v0, v2}, Landroid/app/AlertDialog;->setCancelable(Z)V

    .line 164
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto :goto_65

    .line 166
    :cond_ba
    iget-object v0, p0, Lcom/teamspeak/ts3client/bookmark/i;->a:Lcom/teamspeak/ts3client/bookmark/e;

    iput-boolean v1, v0, Lcom/teamspeak/ts3client/bookmark/e;->a:Z

    .line 167
    iget-object v0, p0, Lcom/teamspeak/ts3client/bookmark/i;->a:Lcom/teamspeak/ts3client/bookmark/e;

    invoke-static {v0}, Lcom/teamspeak/ts3client/bookmark/e;->a(Lcom/teamspeak/ts3client/bookmark/e;)Lcom/teamspeak/ts3client/bookmark/c;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/teamspeak/ts3client/bookmark/c;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/data/ab;

    .line 168
    iget-object v1, p0, Lcom/teamspeak/ts3client/bookmark/i;->a:Lcom/teamspeak/ts3client/bookmark/e;

    invoke-virtual {v1, v0}, Lcom/teamspeak/ts3client/bookmark/e;->a(Lcom/teamspeak/ts3client/data/ab;)V

    goto :goto_65
.end method
