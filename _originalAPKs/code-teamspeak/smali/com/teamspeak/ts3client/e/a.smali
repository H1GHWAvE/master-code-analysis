.class public final Lcom/teamspeak/ts3client/e/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Z


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 5
    const/4 v0, 0x1

    iput v0, p0, Lcom/teamspeak/ts3client/e/a;->a:I

    .line 6
    const-string v0, ""

    iput-object v0, p0, Lcom/teamspeak/ts3client/e/a;->b:Ljava/lang/String;

    .line 7
    const-string v0, ""

    iput-object v0, p0, Lcom/teamspeak/ts3client/e/a;->c:Ljava/lang/String;

    .line 8
    const-string v0, ""

    iput-object v0, p0, Lcom/teamspeak/ts3client/e/a;->d:Ljava/lang/String;

    .line 9
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/teamspeak/ts3client/e/a;->e:Z

    .line 13
    return-void
.end method

.method private constructor <init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .registers 7

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 5
    const/4 v0, 0x1

    iput v0, p0, Lcom/teamspeak/ts3client/e/a;->a:I

    .line 6
    const-string v0, ""

    iput-object v0, p0, Lcom/teamspeak/ts3client/e/a;->b:Ljava/lang/String;

    .line 7
    const-string v0, ""

    iput-object v0, p0, Lcom/teamspeak/ts3client/e/a;->c:Ljava/lang/String;

    .line 8
    const-string v0, ""

    iput-object v0, p0, Lcom/teamspeak/ts3client/e/a;->d:Ljava/lang/String;

    .line 9
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/teamspeak/ts3client/e/a;->e:Z

    .line 17
    iput p1, p0, Lcom/teamspeak/ts3client/e/a;->a:I

    .line 18
    iput-object p2, p0, Lcom/teamspeak/ts3client/e/a;->b:Ljava/lang/String;

    .line 19
    iput-object p3, p0, Lcom/teamspeak/ts3client/e/a;->c:Ljava/lang/String;

    .line 20
    iput-object p4, p0, Lcom/teamspeak/ts3client/e/a;->d:Ljava/lang/String;

    .line 21
    iput-boolean p5, p0, Lcom/teamspeak/ts3client/e/a;->e:Z

    .line 22
    return-void
.end method

.method private a()I
    .registers 2

    .prologue
    .line 25
    iget v0, p0, Lcom/teamspeak/ts3client/e/a;->a:I

    return v0
.end method

.method private a(I)V
    .registers 2

    .prologue
    .line 29
    iput p1, p0, Lcom/teamspeak/ts3client/e/a;->a:I

    .line 30
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .registers 2

    .prologue
    .line 37
    iput-object p1, p0, Lcom/teamspeak/ts3client/e/a;->c:Ljava/lang/String;

    .line 38
    return-void
.end method

.method private a(Z)V
    .registers 2

    .prologue
    .line 61
    iput-boolean p1, p0, Lcom/teamspeak/ts3client/e/a;->e:Z

    .line 62
    return-void
.end method

.method private b()Ljava/lang/String;
    .registers 2

    .prologue
    .line 33
    iget-object v0, p0, Lcom/teamspeak/ts3client/e/a;->c:Ljava/lang/String;

    return-object v0
.end method

.method private b(Ljava/lang/String;)V
    .registers 2

    .prologue
    .line 45
    iput-object p1, p0, Lcom/teamspeak/ts3client/e/a;->b:Ljava/lang/String;

    .line 46
    return-void
.end method

.method private c()Ljava/lang/String;
    .registers 2

    .prologue
    .line 41
    iget-object v0, p0, Lcom/teamspeak/ts3client/e/a;->b:Ljava/lang/String;

    return-object v0
.end method

.method private c(Ljava/lang/String;)V
    .registers 2

    .prologue
    .line 53
    iput-object p1, p0, Lcom/teamspeak/ts3client/e/a;->d:Ljava/lang/String;

    .line 54
    return-void
.end method

.method private d()Ljava/lang/String;
    .registers 2

    .prologue
    .line 49
    iget-object v0, p0, Lcom/teamspeak/ts3client/e/a;->d:Ljava/lang/String;

    return-object v0
.end method

.method private e()Z
    .registers 2

    .prologue
    .line 57
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/e/a;->e:Z

    return v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .registers 2

    .prologue
    .line 66
    iget-object v0, p0, Lcom/teamspeak/ts3client/e/a;->b:Ljava/lang/String;

    return-object v0
.end method
