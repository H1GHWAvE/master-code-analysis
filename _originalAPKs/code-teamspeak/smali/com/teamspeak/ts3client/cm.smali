.class final Lcom/teamspeak/ts3client/cm;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/a/a/a/a/o;


# instance fields
.field final synthetic g:Lcom/teamspeak/ts3client/StartGUIFragment;


# direct methods
.method private constructor <init>(Lcom/teamspeak/ts3client/StartGUIFragment;)V
    .registers 2

    .prologue
    .line 1246
    iput-object p1, p0, Lcom/teamspeak/ts3client/cm;->g:Lcom/teamspeak/ts3client/StartGUIFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/teamspeak/ts3client/StartGUIFragment;B)V
    .registers 3

    .prologue
    .line 1246
    invoke-direct {p0, p1}, Lcom/teamspeak/ts3client/cm;-><init>(Lcom/teamspeak/ts3client/StartGUIFragment;)V

    return-void
.end method

.method private d()V
    .registers 3

    .prologue
    .line 1278
    iget-object v0, p0, Lcom/teamspeak/ts3client/cm;->g:Lcom/teamspeak/ts3client/StartGUIFragment;

    invoke-static {v0}, Lcom/teamspeak/ts3client/StartGUIFragment;->k(Lcom/teamspeak/ts3client/StartGUIFragment;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/teamspeak/ts3client/cn;

    invoke-direct {v1, p0}, Lcom/teamspeak/ts3client/cn;-><init>(Lcom/teamspeak/ts3client/cm;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1283
    return-void
.end method


# virtual methods
.method public final a()V
    .registers 2

    .prologue
    .line 1251
    iget-object v0, p0, Lcom/teamspeak/ts3client/cm;->g:Lcom/teamspeak/ts3client/StartGUIFragment;

    invoke-static {v0}, Lcom/teamspeak/ts3client/StartGUIFragment;->i(Lcom/teamspeak/ts3client/StartGUIFragment;)V

    .line 1252
    return-void
.end method

.method public final a(I)V
    .registers 4

    .prologue
    .line 1257
    const/16 v0, 0x123

    if-ne p1, v0, :cond_3f

    iget-object v0, p0, Lcom/teamspeak/ts3client/cm;->g:Lcom/teamspeak/ts3client/StartGUIFragment;

    invoke-static {v0}, Lcom/teamspeak/ts3client/StartGUIFragment;->j(Lcom/teamspeak/ts3client/StartGUIFragment;)I

    move-result v0

    if-ltz v0, :cond_3f

    .line 1258
    iget-object v0, p0, Lcom/teamspeak/ts3client/cm;->g:Lcom/teamspeak/ts3client/StartGUIFragment;

    iget-object v1, p0, Lcom/teamspeak/ts3client/cm;->g:Lcom/teamspeak/ts3client/StartGUIFragment;

    invoke-static {v1}, Lcom/teamspeak/ts3client/StartGUIFragment;->j(Lcom/teamspeak/ts3client/StartGUIFragment;)I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-static {v0, v1}, Lcom/teamspeak/ts3client/StartGUIFragment;->a(Lcom/teamspeak/ts3client/StartGUIFragment;I)I

    .line 1260
    const-wide/16 v0, 0x3e8

    :try_start_1b
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1e
    .catch Ljava/lang/Exception; {:try_start_1b .. :try_end_1e} :catch_2e

    .line 1264
    :goto_1e
    iget-object v0, p0, Lcom/teamspeak/ts3client/cm;->g:Lcom/teamspeak/ts3client/StartGUIFragment;

    invoke-static {v0}, Lcom/teamspeak/ts3client/StartGUIFragment;->c(Lcom/teamspeak/ts3client/StartGUIFragment;)Lcom/a/a/a/a/k;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/cm;->g:Lcom/teamspeak/ts3client/StartGUIFragment;

    invoke-static {v1}, Lcom/teamspeak/ts3client/StartGUIFragment;->b(Lcom/teamspeak/ts3client/StartGUIFragment;)Lcom/teamspeak/ts3client/cm;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/a/a/a/a/k;->a(Lcom/a/a/a/a/o;)V

    .line 1268
    :goto_2d
    return-void

    .line 1262
    :catch_2e
    move-exception v0

    iget-object v0, p0, Lcom/teamspeak/ts3client/cm;->g:Lcom/teamspeak/ts3client/StartGUIFragment;

    invoke-static {v0}, Lcom/teamspeak/ts3client/StartGUIFragment;->c(Lcom/teamspeak/ts3client/StartGUIFragment;)Lcom/a/a/a/a/k;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/cm;->g:Lcom/teamspeak/ts3client/StartGUIFragment;

    invoke-static {v1}, Lcom/teamspeak/ts3client/StartGUIFragment;->b(Lcom/teamspeak/ts3client/StartGUIFragment;)Lcom/teamspeak/ts3client/cm;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/a/a/a/a/k;->a(Lcom/a/a/a/a/o;)V

    goto :goto_1e

    .line 1267
    :cond_3f
    iget-object v0, p0, Lcom/teamspeak/ts3client/cm;->g:Lcom/teamspeak/ts3client/StartGUIFragment;

    invoke-static {v0, p1}, Lcom/teamspeak/ts3client/StartGUIFragment;->b(Lcom/teamspeak/ts3client/StartGUIFragment;I)V

    goto :goto_2d
.end method

.method public final a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V
    .registers 12

    .prologue
    .line 1297
    if-eqz p1, :cond_8

    if-eqz p2, :cond_8

    if-eqz p3, :cond_8

    if-nez p4, :cond_10

    .line 1298
    :cond_8
    iget-object v0, p0, Lcom/teamspeak/ts3client/cm;->g:Lcom/teamspeak/ts3client/StartGUIFragment;

    const/16 v1, 0x123

    invoke-static {v0, v1}, Lcom/teamspeak/ts3client/StartGUIFragment;->b(Lcom/teamspeak/ts3client/StartGUIFragment;I)V

    .line 1303
    :goto_f
    return-void

    .line 1301
    :cond_10
    invoke-static {}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->b()Lcom/teamspeak/ts3client/jni/Ts3Jni;

    move-result-object v0

    iget v3, p2, Lcom/a/a/a/a/u;->a:I

    iget v4, p2, Lcom/a/a/a/a/u;->b:I

    iget-object v5, p2, Lcom/a/a/a/a/u;->d:Ljava/lang/String;

    const/4 v6, 0x1

    move-object v1, p3

    move-object v2, p4

    invoke-virtual/range {v0 .. v6}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_android_checkSignatureData(Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;I)I

    goto :goto_f
.end method

.method public final b()V
    .registers 2

    .prologue
    .line 1287
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v0}, Ljava/io/PrintStream;->println()V

    .line 1288
    return-void
.end method

.method public final b(I)V
    .registers 3

    .prologue
    .line 1273
    iget-object v0, p0, Lcom/teamspeak/ts3client/cm;->g:Lcom/teamspeak/ts3client/StartGUIFragment;

    invoke-static {v0, p1}, Lcom/teamspeak/ts3client/StartGUIFragment;->b(Lcom/teamspeak/ts3client/StartGUIFragment;I)V

    .line 1274
    return-void
.end method

.method public final c()V
    .registers 2

    .prologue
    .line 1292
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v0}, Ljava/io/PrintStream;->println()V

    .line 1293
    return-void
.end method
