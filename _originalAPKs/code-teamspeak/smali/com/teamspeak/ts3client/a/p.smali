.class public final Lcom/teamspeak/ts3client/a/p;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;


# instance fields
.field public a:Z

.field private b:Lcom/teamspeak/ts3client/Ts3Application;

.field private c:Lcom/teamspeak/ts3client/a/n;

.field private d:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 5

    .prologue
    const/4 v2, 0x0

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-boolean v2, p0, Lcom/teamspeak/ts3client/a/p;->a:Z

    .line 21
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/Ts3Application;

    iput-object v0, p0, Lcom/teamspeak/ts3client/a/p;->b:Lcom/teamspeak/ts3client/Ts3Application;

    .line 22
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/p;->b:Lcom/teamspeak/ts3client/Ts3Application;

    .line 1093
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 22
    const-string v1, "sound_pack"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/teamspeak/ts3client/a/p;->d:I

    .line 23
    invoke-direct {p0, v2}, Lcom/teamspeak/ts3client/a/p;->a(Z)V

    .line 24
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/p;->b:Lcom/teamspeak/ts3client/Ts3Application;

    .line 2093
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 24
    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 25
    return-void
.end method

.method private a()V
    .registers 3

    .prologue
    .line 28
    new-instance v0, Lcom/teamspeak/ts3client/a/r;

    invoke-direct {v0}, Lcom/teamspeak/ts3client/a/r;-><init>()V

    iput-object v0, p0, Lcom/teamspeak/ts3client/a/p;->c:Lcom/teamspeak/ts3client/a/n;

    .line 29
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/p;->c:Lcom/teamspeak/ts3client/a/n;

    iget-object v1, p0, Lcom/teamspeak/ts3client/a/p;->b:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/teamspeak/ts3client/a/n;->a(Landroid/content/Context;)V

    .line 30
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .registers 3

    .prologue
    .line 97
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/a/p;->a:Z

    if-eqz v0, :cond_5

    .line 105
    :goto_4
    return-void

    .line 99
    :cond_5
    iget v0, p0, Lcom/teamspeak/ts3client/a/p;->d:I

    packed-switch v0, :pswitch_data_12

    goto :goto_4

    .line 101
    :pswitch_b
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/p;->c:Lcom/teamspeak/ts3client/a/n;

    invoke-interface {v0, p1}, Lcom/teamspeak/ts3client/a/n;->a(Ljava/lang/String;)V

    goto :goto_4

    .line 99
    nop

    :pswitch_data_12
    .packed-switch 0x1
        :pswitch_b
    .end packed-switch
.end method

.method private a(Z)V
    .registers 4

    .prologue
    .line 38
    if-eqz p1, :cond_b

    iget-object v0, p0, Lcom/teamspeak/ts3client/a/p;->c:Lcom/teamspeak/ts3client/a/n;

    if-eqz v0, :cond_b

    .line 39
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/p;->c:Lcom/teamspeak/ts3client/a/n;

    invoke-interface {v0}, Lcom/teamspeak/ts3client/a/n;->b()V

    .line 41
    :cond_b
    iget v0, p0, Lcom/teamspeak/ts3client/a/p;->d:I

    packed-switch v0, :pswitch_data_38

    .line 49
    :goto_10
    return-void

    .line 3028
    :pswitch_11
    new-instance v0, Lcom/teamspeak/ts3client/a/r;

    invoke-direct {v0}, Lcom/teamspeak/ts3client/a/r;-><init>()V

    iput-object v0, p0, Lcom/teamspeak/ts3client/a/p;->c:Lcom/teamspeak/ts3client/a/n;

    .line 3029
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/p;->c:Lcom/teamspeak/ts3client/a/n;

    iget-object v1, p0, Lcom/teamspeak/ts3client/a/p;->b:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/teamspeak/ts3client/a/n;->a(Landroid/content/Context;)V

    goto :goto_10

    .line 3033
    :pswitch_24
    new-instance v0, Lcom/teamspeak/ts3client/a/t;

    invoke-direct {v0}, Lcom/teamspeak/ts3client/a/t;-><init>()V

    iput-object v0, p0, Lcom/teamspeak/ts3client/a/p;->c:Lcom/teamspeak/ts3client/a/n;

    .line 3034
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/p;->c:Lcom/teamspeak/ts3client/a/n;

    iget-object v1, p0, Lcom/teamspeak/ts3client/a/p;->b:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/teamspeak/ts3client/a/n;->a(Landroid/content/Context;)V

    goto :goto_10

    .line 41
    nop

    :pswitch_data_38
    .packed-switch 0x0
        :pswitch_24
        :pswitch_11
    .end packed-switch
.end method

.method private b()V
    .registers 3

    .prologue
    .line 33
    new-instance v0, Lcom/teamspeak/ts3client/a/t;

    invoke-direct {v0}, Lcom/teamspeak/ts3client/a/t;-><init>()V

    iput-object v0, p0, Lcom/teamspeak/ts3client/a/p;->c:Lcom/teamspeak/ts3client/a/n;

    .line 34
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/p;->c:Lcom/teamspeak/ts3client/a/n;

    iget-object v1, p0, Lcom/teamspeak/ts3client/a/p;->b:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/teamspeak/ts3client/a/n;->a(Landroid/content/Context;)V

    .line 35
    return-void
.end method

.method private b(Z)V
    .registers 2

    .prologue
    .line 117
    iput-boolean p1, p0, Lcom/teamspeak/ts3client/a/p;->a:Z

    .line 118
    return-void
.end method

.method private c()V
    .registers 2

    .prologue
    .line 108
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/p;->c:Lcom/teamspeak/ts3client/a/n;

    invoke-interface {v0}, Lcom/teamspeak/ts3client/a/n;->a()V

    .line 109
    return-void
.end method

.method private d()V
    .registers 2

    .prologue
    .line 112
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/p;->c:Lcom/teamspeak/ts3client/a/n;

    if-eqz v0, :cond_9

    .line 113
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/p;->c:Lcom/teamspeak/ts3client/a/n;

    invoke-interface {v0}, Lcom/teamspeak/ts3client/a/n;->b()V

    .line 114
    :cond_9
    return-void
.end method

.method private e()V
    .registers 2

    .prologue
    .line 121
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/teamspeak/ts3client/a/p;->a:Z

    .line 122
    return-void
.end method


# virtual methods
.method public final a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V
    .registers 4

    .prologue
    .line 80
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/a/p;->a:Z

    if-eqz v0, :cond_5

    .line 91
    :goto_4
    return-void

    .line 82
    :cond_5
    iget v0, p0, Lcom/teamspeak/ts3client/a/p;->d:I

    packed-switch v0, :pswitch_data_18

    goto :goto_4

    .line 87
    :pswitch_b
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/p;->c:Lcom/teamspeak/ts3client/a/n;

    invoke-interface {v0, p1, p2}, Lcom/teamspeak/ts3client/a/n;->a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V

    goto :goto_4

    .line 84
    :pswitch_11
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/p;->c:Lcom/teamspeak/ts3client/a/n;

    invoke-interface {v0, p1, p2}, Lcom/teamspeak/ts3client/a/n;->a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V

    goto :goto_4

    .line 82
    nop

    :pswitch_data_18
    .packed-switch 0x0
        :pswitch_b
        :pswitch_11
    .end packed-switch
.end method

.method public final onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .registers 8

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 53
    const-string v0, "sound_pack"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 54
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/p;->b:Lcom/teamspeak/ts3client/Ts3Application;

    .line 3093
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 54
    const-string v1, "sound_pack"

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/teamspeak/ts3client/a/p;->d:I

    .line 56
    const/4 v0, 0x1

    :try_start_17
    invoke-direct {p0, v0}, Lcom/teamspeak/ts3client/a/p;->a(Z)V
    :try_end_1a
    .catch Ljava/lang/Exception; {:try_start_17 .. :try_end_1a} :catch_1b

    .line 77
    :cond_1a
    :goto_1a
    return-void

    .line 58
    :catch_1b
    move-exception v0

    iget-object v0, p0, Lcom/teamspeak/ts3client/a/p;->b:Lcom/teamspeak/ts3client/Ts3Application;

    .line 4093
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 58
    const-string v1, "sound_pack"

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v2, :cond_1a

    .line 59
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/p;->b:Lcom/teamspeak/ts3client/Ts3Application;

    .line 5093
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 59
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "sound_pack"

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 60
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/p;->b:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->g()Lcom/teamspeak/ts3client/a/p;

    .line 61
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/teamspeak/ts3client/a/p;->b:Lcom/teamspeak/ts3client/Ts3Application;

    .line 5107
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->f:Lcom/teamspeak/ts3client/StartGUIFragment;

    .line 61
    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 62
    const-string v1, "critical.tts"

    invoke-static {v1}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 63
    const-string v1, "critical.tts.text"

    invoke-static {v1}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 64
    const/4 v1, -0x3

    const-string v2, "button.ok"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/teamspeak/ts3client/a/q;

    invoke-direct {v3, p0, v0}, Lcom/teamspeak/ts3client/a/q;-><init>(Lcom/teamspeak/ts3client/a/p;Landroid/app/AlertDialog;)V

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 71
    invoke-virtual {v0, v4}, Landroid/app/AlertDialog;->setCancelable(Z)V

    .line 72
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto :goto_1a
.end method
