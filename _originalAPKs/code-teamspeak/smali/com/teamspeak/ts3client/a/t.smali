.class public final Lcom/teamspeak/ts3client/a/t;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/teamspeak/ts3client/a/n;


# instance fields
.field a:Ljava/util/HashMap;

.field b:Ljava/lang/String;

.field c:Landroid/media/SoundPool;


# direct methods
.method public constructor <init>()V
    .registers 3

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/teamspeak/ts3client/a/t;->a:Ljava/util/HashMap;

    .line 28
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/TS3/content/sound/default/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/teamspeak/ts3client/a/t;->b:Ljava/lang/String;

    .line 93
    return-void
.end method

.method private static synthetic a(Lcom/teamspeak/ts3client/a/t;)Landroid/media/SoundPool;
    .registers 2

    .prologue
    .line 25
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/t;->c:Landroid/media/SoundPool;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .registers 4
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 67
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/t;->c:Landroid/media/SoundPool;

    invoke-virtual {v0}, Landroid/media/SoundPool;->release()V

    .line 68
    new-instance v0, Landroid/media/SoundPool;

    const/4 v1, 0x4

    invoke-direct {v0, v1, v2, v2}, Landroid/media/SoundPool;-><init>(III)V

    iput-object v0, p0, Lcom/teamspeak/ts3client/a/t;->c:Landroid/media/SoundPool;

    .line 69
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_21

    .line 70
    new-instance v0, Lcom/teamspeak/ts3client/a/u;

    invoke-direct {v0, p0}, Lcom/teamspeak/ts3client/a/u;-><init>(Lcom/teamspeak/ts3client/a/t;)V

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Lcom/teamspeak/ts3client/a/u;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 73
    :goto_20
    return-void

    .line 72
    :cond_21
    new-instance v0, Lcom/teamspeak/ts3client/a/u;

    invoke-direct {v0, p0}, Lcom/teamspeak/ts3client/a/u;-><init>(Lcom/teamspeak/ts3client/a/t;)V

    new-array v1, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/a/u;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_20
.end method

.method public final a(Landroid/content/Context;)V
    .registers 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 78
    new-instance v0, Landroid/media/SoundPool;

    const/4 v1, 0x4

    invoke-direct {v0, v1, v2, v2}, Landroid/media/SoundPool;-><init>(III)V

    iput-object v0, p0, Lcom/teamspeak/ts3client/a/t;->c:Landroid/media/SoundPool;

    .line 79
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_1c

    .line 80
    new-instance v0, Lcom/teamspeak/ts3client/a/u;

    invoke-direct {v0, p0}, Lcom/teamspeak/ts3client/a/u;-><init>(Lcom/teamspeak/ts3client/a/t;)V

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Lcom/teamspeak/ts3client/a/u;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 83
    :goto_1b
    return-void

    .line 82
    :cond_1c
    new-instance v0, Lcom/teamspeak/ts3client/a/u;

    invoke-direct {v0, p0}, Lcom/teamspeak/ts3client/a/u;-><init>(Lcom/teamspeak/ts3client/a/t;)V

    new-array v1, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/a/u;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_1b
.end method

.method public final a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V
    .registers 11

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    const/high16 v6, 0x3f800000    # 1.0f

    const v2, 0x3e99999a    # 0.3f

    .line 34
    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/h;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "CLIENT_"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_ea

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/h;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "CLIENT_R"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_ea

    .line 1027
    iget v0, p2, Lcom/teamspeak/ts3client/a/o;->b:I

    .line 35
    packed-switch v0, :pswitch_data_10e

    .line 56
    :cond_24
    :goto_24
    return-void

    .line 37
    :pswitch_25
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/t;->a:Ljava/util/HashMap;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "neutral_"

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/h;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_24

    .line 38
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/t;->c:Landroid/media/SoundPool;

    iget-object v1, p0, Lcom/teamspeak/ts3client/a/t;->a:Ljava/util/HashMap;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v7, "neutral_"

    invoke-direct {v3, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/h;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    move v3, v2

    invoke-virtual/range {v0 .. v6}, Landroid/media/SoundPool;->play(IFFIIF)I

    goto :goto_24

    .line 41
    :pswitch_66
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/t;->a:Ljava/util/HashMap;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "blocked_"

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/h;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_24

    .line 42
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/t;->c:Landroid/media/SoundPool;

    iget-object v1, p0, Lcom/teamspeak/ts3client/a/t;->a:Ljava/util/HashMap;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v7, "blocked_"

    invoke-direct {v3, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/h;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    move v3, v2

    invoke-virtual/range {v0 .. v6}, Landroid/media/SoundPool;->play(IFFIIF)I

    goto/16 :goto_24

    .line 45
    :pswitch_a8
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/t;->a:Ljava/util/HashMap;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "friend_"

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/h;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_24

    .line 46
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/t;->c:Landroid/media/SoundPool;

    iget-object v1, p0, Lcom/teamspeak/ts3client/a/t;->a:Ljava/util/HashMap;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v7, "friend_"

    invoke-direct {v3, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/h;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    move v3, v2

    invoke-virtual/range {v0 .. v6}, Landroid/media/SoundPool;->play(IFFIIF)I

    goto/16 :goto_24

    .line 52
    :cond_ea
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/t;->a:Ljava/util/HashMap;

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/h;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_24

    .line 53
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/t;->c:Landroid/media/SoundPool;

    iget-object v1, p0, Lcom/teamspeak/ts3client/a/t;->a:Ljava/util/HashMap;

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/jni/h;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    move v3, v2

    invoke-virtual/range {v0 .. v6}, Landroid/media/SoundPool;->play(IFFIIF)I

    goto/16 :goto_24

    .line 35
    :pswitch_data_10e
    .packed-switch 0x0
        :pswitch_25
        :pswitch_66
        :pswitch_a8
    .end packed-switch
.end method

.method public final a(Ljava/lang/String;)V
    .registers 2

    .prologue
    .line 62
    return-void
.end method

.method public final b()V
    .registers 2

    .prologue
    .line 87
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/t;->c:Landroid/media/SoundPool;

    invoke-virtual {v0}, Landroid/media/SoundPool;->release()V

    .line 88
    return-void
.end method
