.class public final Lcom/teamspeak/ts3client/customs/a;
.super Landroid/widget/ArrayAdapter;
.source "SourceFile"

# interfaces
.implements Landroid/widget/SpinnerAdapter;


# instance fields
.field private a:Ljava/util/Vector;

.field private b:Landroid/content/Context;

.field private c:Landroid/view/LayoutInflater;

.field private d:I

.field private e:I


# direct methods
.method private constructor <init>(Landroid/content/Context;I)V
    .registers 4

    .prologue
    .line 39
    invoke-direct {p0, p1, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 18
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/teamspeak/ts3client/customs/a;->a:Ljava/util/Vector;

    .line 21
    const/4 v0, 0x0

    iput v0, p0, Lcom/teamspeak/ts3client/customs/a;->d:I

    .line 40
    iput-object p1, p0, Lcom/teamspeak/ts3client/customs/a;->b:Landroid/content/Context;

    .line 41
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/teamspeak/ts3client/customs/a;->c:Landroid/view/LayoutInflater;

    .line 42
    iput p2, p0, Lcom/teamspeak/ts3client/customs/a;->e:I

    .line 43
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;ILjava/util/List;)V
    .registers 5

    .prologue
    .line 25
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 18
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/teamspeak/ts3client/customs/a;->a:Ljava/util/Vector;

    .line 21
    const/4 v0, 0x0

    iput v0, p0, Lcom/teamspeak/ts3client/customs/a;->d:I

    .line 26
    iput-object p1, p0, Lcom/teamspeak/ts3client/customs/a;->b:Landroid/content/Context;

    .line 27
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/teamspeak/ts3client/customs/a;->c:Landroid/view/LayoutInflater;

    .line 28
    iput p2, p0, Lcom/teamspeak/ts3client/customs/a;->e:I

    .line 29
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;[Ljava/lang/String;)V
    .registers 5

    .prologue
    const v1, 0x1090009

    .line 32
    invoke-direct {p0, p1, v1, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    .line 18
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/teamspeak/ts3client/customs/a;->a:Ljava/util/Vector;

    .line 21
    const/4 v0, 0x0

    iput v0, p0, Lcom/teamspeak/ts3client/customs/a;->d:I

    .line 33
    iput-object p1, p0, Lcom/teamspeak/ts3client/customs/a;->b:Landroid/content/Context;

    .line 34
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/teamspeak/ts3client/customs/a;->c:Landroid/view/LayoutInflater;

    .line 35
    iput v1, p0, Lcom/teamspeak/ts3client/customs/a;->e:I

    .line 36
    return-void
.end method

.method private a(ILandroid/view/ViewGroup;I)Landroid/view/View;
    .registers 10

    .prologue
    const/4 v5, 0x0

    .line 74
    iget-object v1, p0, Lcom/teamspeak/ts3client/customs/a;->c:Landroid/view/LayoutInflater;

    invoke-virtual {v1, p3, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 77
    :try_start_7
    iget v1, p0, Lcom/teamspeak/ts3client/customs/a;->d:I

    if-nez v1, :cond_2d

    .line 79
    move-object v0, v2

    check-cast v0, Landroid/widget/TextView;

    move-object v1, v0
    :try_end_f
    .catch Ljava/lang/ClassCastException; {:try_start_7 .. :try_end_f} :catch_37

    move-object v3, v1

    .line 89
    :goto_10
    invoke-virtual {p0, p1}, Lcom/teamspeak/ts3client/customs/a;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 90
    instance-of v4, v1, Ljava/lang/CharSequence;

    if-eqz v4, :cond_47

    .line 91
    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 95
    :goto_1d
    invoke-virtual {p0, p1}, Lcom/teamspeak/ts3client/customs/a;->isEnabled(I)Z

    move-result v1

    if-nez v1, :cond_2c

    .line 96
    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 97
    const v1, -0xbbbbbc

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 100
    :cond_2c
    return-object v2

    .line 82
    :cond_2d
    :try_start_2d
    iget v1, p0, Lcom/teamspeak/ts3client/customs/a;->d:I

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;
    :try_end_35
    .catch Ljava/lang/ClassCastException; {:try_start_2d .. :try_end_35} :catch_37

    move-object v3, v1

    .line 87
    goto :goto_10

    .line 84
    :catch_37
    move-exception v1

    .line 85
    const-string v2, "ArrayAdapter"

    const-string v3, "You must supply a resource ID for a TextView"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 86
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "ArrayAdapter requires the resource ID to be a TextView"

    invoke-direct {v2, v3, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2

    .line 93
    :cond_47
    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1d
.end method

.method private b(I)V
    .registers 4

    .prologue
    .line 62
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/a;->a:Ljava/util/Vector;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Vector;->removeElement(Ljava/lang/Object;)Z

    .line 63
    return-void
.end method


# virtual methods
.method public final a(I)V
    .registers 4

    .prologue
    .line 58
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/a;->a:Ljava/util/Vector;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 59
    return-void
.end method

.method public final areAllItemsEnabled()Z
    .registers 2

    .prologue
    .line 47
    const/4 v0, 0x0

    return v0
.end method

.method public final getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 5

    .prologue
    .line 67
    iget v0, p0, Lcom/teamspeak/ts3client/customs/a;->e:I

    invoke-direct {p0, p1, p3, v0}, Lcom/teamspeak/ts3client/customs/a;->a(ILandroid/view/ViewGroup;I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final isEnabled(I)Z
    .registers 4

    .prologue
    .line 52
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/a;->a:Ljava/util/Vector;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Vector;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 53
    const/4 v0, 0x0

    .line 54
    :goto_d
    return v0

    :cond_e
    invoke-super {p0, p1}, Landroid/widget/ArrayAdapter;->isEnabled(I)Z

    move-result v0

    goto :goto_d
.end method
