.class final Lcom/teamspeak/ts3client/customs/d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# instance fields
.field final synthetic a:Lcom/teamspeak/ts3client/customs/CustomCodecSettings;


# direct methods
.method constructor <init>(Lcom/teamspeak/ts3client/customs/CustomCodecSettings;)V
    .registers 2

    .prologue
    .line 167
    iput-object p1, p0, Lcom/teamspeak/ts3client/customs/d;->a:Lcom/teamspeak/ts3client/customs/CustomCodecSettings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .registers 10

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 171
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/d;->a:Lcom/teamspeak/ts3client/customs/CustomCodecSettings;

    invoke-static {v0}, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->c(Lcom/teamspeak/ts3client/customs/CustomCodecSettings;)I

    move-result v0

    if-ge v0, p2, :cond_29

    if-eqz p3, :cond_29

    .line 172
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/d;->a:Lcom/teamspeak/ts3client/customs/CustomCodecSettings;

    invoke-static {v0}, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->i(Lcom/teamspeak/ts3client/customs/CustomCodecSettings;)Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 173
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/d;->a:Lcom/teamspeak/ts3client/customs/CustomCodecSettings;

    invoke-static {v0}, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->c(Lcom/teamspeak/ts3client/customs/CustomCodecSettings;)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 202
    :cond_1e
    :goto_1e
    return-void

    .line 175
    :cond_1f
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/d;->a:Lcom/teamspeak/ts3client/customs/CustomCodecSettings;

    invoke-static {v0}, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->j(Lcom/teamspeak/ts3client/customs/CustomCodecSettings;)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/widget/SeekBar;->setProgress(I)V

    goto :goto_1e

    .line 178
    :cond_29
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/d;->a:Lcom/teamspeak/ts3client/customs/CustomCodecSettings;

    invoke-static {v0}, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->k(Lcom/teamspeak/ts3client/customs/CustomCodecSettings;)Landroid/widget/TextView;

    move-result-object v0

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 179
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/d;->a:Lcom/teamspeak/ts3client/customs/CustomCodecSettings;

    invoke-static {v0}, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->a(Lcom/teamspeak/ts3client/customs/CustomCodecSettings;)Z

    move-result v0

    if-nez v0, :cond_1e

    .line 181
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/d;->a:Lcom/teamspeak/ts3client/customs/CustomCodecSettings;

    invoke-static {v0}, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->e(Lcom/teamspeak/ts3client/customs/CustomCodecSettings;)Landroid/widget/Spinner;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/customs/d;->a:Lcom/teamspeak/ts3client/customs/CustomCodecSettings;

    invoke-static {v1}, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->b(Lcom/teamspeak/ts3client/customs/CustomCodecSettings;)Ljava/util/BitSet;

    move-result-object v1

    iget-object v2, p0, Lcom/teamspeak/ts3client/customs/d;->a:Lcom/teamspeak/ts3client/customs/CustomCodecSettings;

    invoke-static {v2}, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->c(Lcom/teamspeak/ts3client/customs/CustomCodecSettings;)I

    move-result v2

    invoke-static {v0, p2, v1, v2}, Lcom/teamspeak/ts3client/data/d/y;->a(IILjava/util/BitSet;I)Lcom/teamspeak/ts3client/data/d/aa;

    move-result-object v0

    .line 182
    iget-object v1, p0, Lcom/teamspeak/ts3client/customs/d;->a:Lcom/teamspeak/ts3client/customs/CustomCodecSettings;

    invoke-static {v1, v4}, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->a(Lcom/teamspeak/ts3client/customs/CustomCodecSettings;Z)Z

    .line 183
    sget-object v1, Lcom/teamspeak/ts3client/customs/e;->a:[I

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/d/aa;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_ae

    .line 197
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/d;->a:Lcom/teamspeak/ts3client/customs/CustomCodecSettings;

    invoke-static {v0}, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->h(Lcom/teamspeak/ts3client/customs/CustomCodecSettings;)Landroid/widget/Spinner;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/widget/Spinner;->setSelection(I)V

    .line 199
    :goto_71
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/d;->a:Lcom/teamspeak/ts3client/customs/CustomCodecSettings;

    invoke-static {v0, v3}, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->a(Lcom/teamspeak/ts3client/customs/CustomCodecSettings;Z)Z

    .line 200
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/d;->a:Lcom/teamspeak/ts3client/customs/CustomCodecSettings;

    invoke-static {v0}, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->g(Lcom/teamspeak/ts3client/customs/CustomCodecSettings;)V

    .line 201
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/d;->a:Lcom/teamspeak/ts3client/customs/CustomCodecSettings;

    invoke-static {v0}, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->f(Lcom/teamspeak/ts3client/customs/CustomCodecSettings;)Lcom/teamspeak/ts3client/data/d/ab;

    move-result-object v0

    .line 1148
    iput p2, v0, Lcom/teamspeak/ts3client/data/d/ab;->b:I

    goto :goto_1e

    .line 185
    :pswitch_84
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/d;->a:Lcom/teamspeak/ts3client/customs/CustomCodecSettings;

    invoke-static {v0}, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->h(Lcom/teamspeak/ts3client/customs/CustomCodecSettings;)Landroid/widget/Spinner;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/Spinner;->setSelection(I)V

    goto :goto_71

    .line 188
    :pswitch_8e
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/d;->a:Lcom/teamspeak/ts3client/customs/CustomCodecSettings;

    invoke-static {v0}, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->h(Lcom/teamspeak/ts3client/customs/CustomCodecSettings;)Landroid/widget/Spinner;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/Spinner;->setSelection(I)V

    goto :goto_71

    .line 191
    :pswitch_98
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/d;->a:Lcom/teamspeak/ts3client/customs/CustomCodecSettings;

    invoke-static {v0}, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->h(Lcom/teamspeak/ts3client/customs/CustomCodecSettings;)Landroid/widget/Spinner;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    goto :goto_71

    .line 194
    :pswitch_a3
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/d;->a:Lcom/teamspeak/ts3client/customs/CustomCodecSettings;

    invoke-static {v0}, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->h(Lcom/teamspeak/ts3client/customs/CustomCodecSettings;)Landroid/widget/Spinner;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/widget/Spinner;->setSelection(I)V

    goto :goto_71

    .line 183
    nop

    :pswitch_data_ae
    .packed-switch 0x1
        :pswitch_84
        :pswitch_8e
        :pswitch_98
        :pswitch_a3
    .end packed-switch
.end method

.method public final onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .registers 2

    .prologue
    .line 206
    invoke-virtual {p1}, Landroid/widget/SeekBar;->requestFocus()Z

    .line 208
    return-void
.end method

.method public final onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .registers 2

    .prologue
    .line 212
    invoke-virtual {p1}, Landroid/widget/SeekBar;->requestFocus()Z

    .line 214
    return-void
.end method
