.class public final Lcom/teamspeak/ts3client/bn;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final ab_background_textured_teamspeak:I = 0x7f020000

.field public static final ab_bottom_solid_teamspeak:I = 0x7f020001

.field public static final ab_solid_teamspeak:I = 0x7f020002

.field public static final ab_stacked_solid_teamspeak:I = 0x7f020003

.field public static final ab_texture_tile_teamspeak:I = 0x7f020004

.field public static final ab_transparent_teamspeak:I = 0x7f020005

.field public static final abc_ab_share_pack_mtrl_alpha:I = 0x7f020006

.field public static final abc_action_bar_item_background_material:I = 0x7f020007

.field public static final abc_btn_borderless_material:I = 0x7f020008

.field public static final abc_btn_check_material:I = 0x7f020009

.field public static final abc_btn_check_to_on_mtrl_000:I = 0x7f02000a

.field public static final abc_btn_check_to_on_mtrl_015:I = 0x7f02000b

.field public static final abc_btn_colored_material:I = 0x7f02000c

.field public static final abc_btn_default_mtrl_shape:I = 0x7f02000d

.field public static final abc_btn_radio_material:I = 0x7f02000e

.field public static final abc_btn_radio_to_on_mtrl_000:I = 0x7f02000f

.field public static final abc_btn_radio_to_on_mtrl_015:I = 0x7f020010

.field public static final abc_btn_rating_star_off_mtrl_alpha:I = 0x7f020011

.field public static final abc_btn_rating_star_on_mtrl_alpha:I = 0x7f020012

.field public static final abc_btn_switch_to_on_mtrl_00001:I = 0x7f020013

.field public static final abc_btn_switch_to_on_mtrl_00012:I = 0x7f020014

.field public static final abc_cab_background_internal_bg:I = 0x7f020015

.field public static final abc_cab_background_top_material:I = 0x7f020016

.field public static final abc_cab_background_top_mtrl_alpha:I = 0x7f020017

.field public static final abc_control_background_material:I = 0x7f020018

.field public static final abc_dialog_material_background_dark:I = 0x7f020019

.field public static final abc_dialog_material_background_light:I = 0x7f02001a

.field public static final abc_edit_text_material:I = 0x7f02001b

.field public static final abc_ic_ab_back_mtrl_am_alpha:I = 0x7f02001c

.field public static final abc_ic_clear_mtrl_alpha:I = 0x7f02001d

.field public static final abc_ic_commit_search_api_mtrl_alpha:I = 0x7f02001e

.field public static final abc_ic_go_search_api_mtrl_alpha:I = 0x7f02001f

.field public static final abc_ic_menu_copy_mtrl_am_alpha:I = 0x7f020020

.field public static final abc_ic_menu_cut_mtrl_alpha:I = 0x7f020021

.field public static final abc_ic_menu_moreoverflow_mtrl_alpha:I = 0x7f020022

.field public static final abc_ic_menu_paste_mtrl_am_alpha:I = 0x7f020023

.field public static final abc_ic_menu_selectall_mtrl_alpha:I = 0x7f020024

.field public static final abc_ic_menu_share_mtrl_alpha:I = 0x7f020025

.field public static final abc_ic_search_api_mtrl_alpha:I = 0x7f020026

.field public static final abc_ic_voice_search_api_mtrl_alpha:I = 0x7f020027

.field public static final abc_item_background_holo_dark:I = 0x7f020028

.field public static final abc_item_background_holo_light:I = 0x7f020029

.field public static final abc_list_divider_mtrl_alpha:I = 0x7f02002a

.field public static final abc_list_focused_holo:I = 0x7f02002b

.field public static final abc_list_longpressed_holo:I = 0x7f02002c

.field public static final abc_list_pressed_holo_dark:I = 0x7f02002d

.field public static final abc_list_pressed_holo_light:I = 0x7f02002e

.field public static final abc_list_selector_background_transition_holo_dark:I = 0x7f02002f

.field public static final abc_list_selector_background_transition_holo_light:I = 0x7f020030

.field public static final abc_list_selector_disabled_holo_dark:I = 0x7f020031

.field public static final abc_list_selector_disabled_holo_light:I = 0x7f020032

.field public static final abc_list_selector_holo_dark:I = 0x7f020033

.field public static final abc_list_selector_holo_light:I = 0x7f020034

.field public static final abc_menu_hardkey_panel_mtrl_mult:I = 0x7f020035

.field public static final abc_popup_background_mtrl_mult:I = 0x7f020036

.field public static final abc_ratingbar_full_material:I = 0x7f020037

.field public static final abc_spinner_mtrl_am_alpha:I = 0x7f020038

.field public static final abc_spinner_textfield_background_material:I = 0x7f020039

.field public static final abc_switch_thumb_material:I = 0x7f02003a

.field public static final abc_switch_track_mtrl_alpha:I = 0x7f02003b

.field public static final abc_tab_indicator_material:I = 0x7f02003c

.field public static final abc_tab_indicator_mtrl_alpha:I = 0x7f02003d

.field public static final abc_text_cursor_material:I = 0x7f02003e

.field public static final abc_textfield_activated_mtrl_alpha:I = 0x7f02003f

.field public static final abc_textfield_default_mtrl_alpha:I = 0x7f020040

.field public static final abc_textfield_search_activated_mtrl_alpha:I = 0x7f020041

.field public static final abc_textfield_search_default_mtrl_alpha:I = 0x7f020042

.field public static final abc_textfield_search_material:I = 0x7f020043

.field public static final action_betaaccess:I = 0x7f020044

.field public static final action_update_true:I = 0x7f020045

.field public static final ban_client:I = 0x7f020046

.field public static final bg_bot:I = 0x7f020047

.field public static final bg_top:I = 0x7f020048

.field public static final blue_spacer:I = 0x7f020049

.field public static final bookmark_add:I = 0x7f02004a

.field public static final bookmark_manager:I = 0x7f02004b

.field public static final broken_image:I = 0x7f02004c

.field public static final btn_cab_done_default_teamspeak:I = 0x7f02004d

.field public static final btn_cab_done_focused_teamspeak:I = 0x7f02004e

.field public static final btn_cab_done_pressed_teamspeak:I = 0x7f02004f

.field public static final btn_cab_done_teamspeak:I = 0x7f020050

.field public static final cab_background_bottom_teamspeak:I = 0x7f020051

.field public static final cab_background_top_teamspeak:I = 0x7f020052

.field public static final channel_default:I = 0x7f020053

.field public static final channel_green:I = 0x7f020054

.field public static final channel_green_subscribed:I = 0x7f020055

.field public static final channel_private:I = 0x7f020056

.field public static final channel_red:I = 0x7f020057

.field public static final channel_red_subscribed:I = 0x7f020058

.field public static final channel_yellow:I = 0x7f020059

.field public static final channel_yellow_subscribed:I = 0x7f02005a

.field public static final chat_avatar:I = 0x7f02005b

.field public static final chat_close:I = 0x7f02005c

.field public static final chat_send:I = 0x7f02005d

.field public static final client_is_talker:I = 0x7f02005e

.field public static final client_priority_speaker:I = 0x7f02005f

.field public static final contact:I = 0x7f020060

.field public static final contact_bugreport:I = 0x7f020061

.field public static final contact_edit:I = 0x7f020062

.field public static final contact_menu:I = 0x7f020063

.field public static final copyright:I = 0x7f020064

.field public static final default_logo:I = 0x7f020065

.field public static final design_fab_background:I = 0x7f020066

.field public static final design_snackbar_background:I = 0x7f020067

.field public static final dialog_full_holo_dark:I = 0x7f020068

.field public static final dummy:I = 0x7f020069

.field public static final edit_icon:I = 0x7f02006a

.field public static final expander_close_holo_dark:I = 0x7f02006b

.field public static final expander_open_holo_dark:I = 0x7f02006c

.field public static final group_100:I = 0x7f02006d

.field public static final group_200:I = 0x7f02006e

.field public static final group_300:I = 0x7f02006f

.field public static final group_500:I = 0x7f020070

.field public static final group_600:I = 0x7f020071

.field public static final group_querry:I = 0x7f020072

.field public static final hardware_input_muted:I = 0x7f020073

.field public static final hardware_output_muted:I = 0x7f020074

.field public static final ic_action_select_all:I = 0x7f020075

.field public static final ic_launcher:I = 0x7f020076

.field public static final identity_default:I = 0x7f020077

.field public static final identity_edit:I = 0x7f020078

.field public static final identity_remove:I = 0x7f020079

.field public static final image_add:I = 0x7f02007a

.field public static final image_channel_create:I = 0x7f02007b

.field public static final image_channel_create_sub:I = 0x7f02007c

.field public static final image_check_update:I = 0x7f02007d

.field public static final image_identity_import:I = 0x7f02007e

.field public static final image_identity_manager:I = 0x7f02007f

.field public static final image_listview:I = 0x7f020080

.field public static final input_menu:I = 0x7f020081

.field public static final input_muted:I = 0x7f020082

.field public static final input_muted_local:I = 0x7f020083

.field public static final input_muted_menu:I = 0x7f020084

.field public static final list_focused_teamspeak:I = 0x7f020085

.field public static final loading_image:I = 0x7f020086

.field public static final menu_changelog:I = 0x7f020087

.field public static final menu_chat:I = 0x7f020088

.field public static final menu_dropdown_panel_teamspeak:I = 0x7f020089

.field public static final menu_gradient_slider:I = 0x7f02008a

.field public static final menu_item_background:I = 0x7f02008b

.field public static final menu_self_ptt:I = 0x7f02008c

.field public static final menu_subscribe_to_all_channels:I = 0x7f02008d

.field public static final menu_unsubscribe_to_all_channels:I = 0x7f02008e

.field public static final message_l:I = 0x7f02008f

.field public static final message_n:I = 0x7f020090

.field public static final message_r:I = 0x7f020091

.field public static final mobile_limit:I = 0x7f020092

.field public static final moderated:I = 0x7f020093

.field public static final moderated_:I = 0x7f020094

.field public static final music:I = 0x7f020095

.field public static final notification_template_icon_bg:I = 0x7f0200c9

.field public static final output_menu:I = 0x7f020096

.field public static final output_muted:I = 0x7f020097

.field public static final output_muted_menu:I = 0x7f020098

.field public static final playback:I = 0x7f020099

.field public static final player_away:I = 0x7f02009a

.field public static final player_commander_off:I = 0x7f02009b

.field public static final player_commander_on:I = 0x7f02009c

.field public static final player_off:I = 0x7f02009d

.field public static final player_on:I = 0x7f02009e

.field public static final player_whisper:I = 0x7f02009f

.field public static final plus:I = 0x7f0200a0

.field public static final pressed_background_teamspeak:I = 0x7f0200a1

.field public static final progress_bg_teamspeak:I = 0x7f0200a2

.field public static final progress_horizontal_teamspeak:I = 0x7f0200a3

.field public static final progress_primary_teamspeak:I = 0x7f0200a4

.field public static final progress_secondary_teamspeak:I = 0x7f0200a5

.field public static final register:I = 0x7f0200a6

.field public static final remove_foe:I = 0x7f0200a7

.field public static final remove_friend:I = 0x7f0200a8

.field public static final request_talk_power:I = 0x7f0200a9

.field public static final request_talk_power_cancel:I = 0x7f0200aa

.field public static final search:I = 0x7f0200ab

.field public static final seekbar_custom:I = 0x7f0200ac

.field public static final selectable_background_teamspeak:I = 0x7f0200ad

.field public static final server_green:I = 0x7f0200ae

.field public static final settings:I = 0x7f0200af

.field public static final settings_category_gradient:I = 0x7f0200b0

.field public static final spinner_ab_default_teamspeak:I = 0x7f0200b1

.field public static final spinner_ab_disabled_teamspeak:I = 0x7f0200b2

.field public static final spinner_ab_focused_teamspeak:I = 0x7f0200b3

.field public static final spinner_ab_pressed_teamspeak:I = 0x7f0200b4

.field public static final spinner_background_ab_teamspeak:I = 0x7f0200b5

.field public static final state_menu:I = 0x7f0200b6

.field public static final tab_bg:I = 0x7f0200b7

.field public static final tab_bg_inactive:I = 0x7f0200b8

.field public static final tab_indicator_ab_teamspeak:I = 0x7f0200b9

.field public static final tab_selected_focused_teamspeak:I = 0x7f0200ba

.field public static final tab_selected_pressed_teamspeak:I = 0x7f0200bb

.field public static final tab_selected_teamspeak:I = 0x7f0200bc

.field public static final tab_unselected_focused_teamspeak:I = 0x7f0200bd

.field public static final tab_unselected_pressed_teamspeak:I = 0x7f0200be

.field public static final tab_unselected_teamspeak:I = 0x7f0200bf

.field public static final teamspeak:I = 0x7f0200c0

.field public static final teamspeak_full:I = 0x7f0200c1

.field public static final textformat_bold:I = 0x7f0200c2

.field public static final textformat_italic:I = 0x7f0200c3

.field public static final textformat_underline:I = 0x7f0200c4

.field public static final token:I = 0x7f0200c5

.field public static final topmenu_extendedmenu:I = 0x7f0200c6

.field public static final update:I = 0x7f0200c7

.field public static final white_spacer:I = 0x7f0200c8


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 2209
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
