.class final Lcom/teamspeak/ts3client/chat/n;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field a:Ljava/util/Date;

.field final synthetic b:Lcom/teamspeak/ts3client/chat/j;


# direct methods
.method constructor <init>(Lcom/teamspeak/ts3client/chat/j;)V
    .registers 3

    .prologue
    .line 127
    iput-object p1, p0, Lcom/teamspeak/ts3client/chat/n;->b:Lcom/teamspeak/ts3client/chat/j;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 128
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    iput-object v0, p0, Lcom/teamspeak/ts3client/chat/n;->a:Ljava/util/Date;

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .registers 2

    .prologue
    .line 151
    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .registers 5

    .prologue
    .line 145
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .registers 11

    .prologue
    .line 132
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/n;->b:Lcom/teamspeak/ts3client/chat/j;

    invoke-static {v0}, Lcom/teamspeak/ts3client/chat/j;->a(Lcom/teamspeak/ts3client/chat/j;)Lcom/teamspeak/ts3client/chat/a;

    move-result-object v0

    .line 1123
    iget-object v0, v0, Lcom/teamspeak/ts3client/chat/a;->e:Ljava/lang/String;

    .line 132
    const-string v1, "SERVER"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_20

    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/n;->b:Lcom/teamspeak/ts3client/chat/j;

    invoke-static {v0}, Lcom/teamspeak/ts3client/chat/j;->a(Lcom/teamspeak/ts3client/chat/j;)Lcom/teamspeak/ts3client/chat/a;

    move-result-object v0

    .line 2123
    iget-object v0, v0, Lcom/teamspeak/ts3client/chat/a;->e:Ljava/lang/String;

    .line 132
    const-string v1, "CHANNEL"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_21

    .line 139
    :cond_20
    :goto_20
    return-void

    .line 134
    :cond_21
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    .line 135
    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    iget-object v1, p0, Lcom/teamspeak/ts3client/chat/n;->a:Ljava/util/Date;

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    sub-long/2addr v2, v4

    const-wide/16 v4, 0xbb8

    cmp-long v1, v2, v4

    if-lez v1, :cond_20

    .line 136
    iput-object v0, p0, Lcom/teamspeak/ts3client/chat/n;->a:Ljava/util/Date;

    .line 137
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/n;->b:Lcom/teamspeak/ts3client/chat/j;

    invoke-static {v0}, Lcom/teamspeak/ts3client/chat/j;->d(Lcom/teamspeak/ts3client/chat/j;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 3061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 3234
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 137
    iget-object v1, p0, Lcom/teamspeak/ts3client/chat/n;->b:Lcom/teamspeak/ts3client/chat/j;

    invoke-static {v1}, Lcom/teamspeak/ts3client/chat/j;->d(Lcom/teamspeak/ts3client/chat/j;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v1

    .line 4061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 4267
    iget-wide v2, v1, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 137
    iget-object v1, p0, Lcom/teamspeak/ts3client/chat/n;->b:Lcom/teamspeak/ts3client/chat/j;

    invoke-static {v1}, Lcom/teamspeak/ts3client/chat/j;->a(Lcom/teamspeak/ts3client/chat/j;)Lcom/teamspeak/ts3client/chat/a;

    move-result-object v1

    .line 5097
    iget-object v1, v1, Lcom/teamspeak/ts3client/chat/a;->g:Lcom/teamspeak/ts3client/data/c;

    .line 5235
    iget v1, v1, Lcom/teamspeak/ts3client/data/c;->c:I

    .line 137
    const-string v4, "Composing"

    invoke-virtual {v0, v2, v3, v1, v4}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_clientChatComposing(JILjava/lang/String;)I

    goto :goto_20
.end method
