.class final Lcom/teamspeak/ts3client/chat/e;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:I

.field final synthetic b:Lcom/teamspeak/ts3client/chat/d;


# direct methods
.method constructor <init>(Lcom/teamspeak/ts3client/chat/d;I)V
    .registers 3

    .prologue
    .line 141
    iput-object p1, p0, Lcom/teamspeak/ts3client/chat/e;->b:Lcom/teamspeak/ts3client/chat/d;

    iput p2, p0, Lcom/teamspeak/ts3client/chat/e;->a:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .registers 9

    .prologue
    .line 145
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/e;->b:Lcom/teamspeak/ts3client/chat/d;

    invoke-static {v0}, Lcom/teamspeak/ts3client/chat/d;->a(Lcom/teamspeak/ts3client/chat/d;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 1061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 1234
    iget-object v1, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 145
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/e;->b:Lcom/teamspeak/ts3client/chat/d;

    invoke-static {v0}, Lcom/teamspeak/ts3client/chat/d;->a(Lcom/teamspeak/ts3client/chat/d;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 2061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 2267
    iget-wide v2, v0, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 145
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/e;->b:Lcom/teamspeak/ts3client/chat/d;

    invoke-static {v0}, Lcom/teamspeak/ts3client/chat/d;->b(Lcom/teamspeak/ts3client/chat/d;)Ljava/util/ArrayList;

    move-result-object v0

    iget v4, p0, Lcom/teamspeak/ts3client/chat/e;->a:I

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/chat/a;

    .line 3097
    iget-object v0, v0, Lcom/teamspeak/ts3client/chat/a;->g:Lcom/teamspeak/ts3client/data/c;

    .line 3188
    iget-object v4, v0, Lcom/teamspeak/ts3client/data/c;->b:Ljava/lang/String;

    .line 145
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/e;->b:Lcom/teamspeak/ts3client/chat/d;

    invoke-static {v0}, Lcom/teamspeak/ts3client/chat/d;->b(Lcom/teamspeak/ts3client/chat/d;)Ljava/util/ArrayList;

    move-result-object v0

    iget v5, p0, Lcom/teamspeak/ts3client/chat/e;->a:I

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/chat/a;

    .line 4097
    iget-object v0, v0, Lcom/teamspeak/ts3client/chat/a;->g:Lcom/teamspeak/ts3client/data/c;

    .line 4235
    iget v5, v0, Lcom/teamspeak/ts3client/data/c;->c:I

    .line 145
    const-string v6, "Chat closed"

    invoke-virtual/range {v1 .. v6}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_clientChatClosed(JLjava/lang/String;ILjava/lang/String;)I

    .line 146
    iget-object v1, p0, Lcom/teamspeak/ts3client/chat/e;->b:Lcom/teamspeak/ts3client/chat/d;

    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/e;->b:Lcom/teamspeak/ts3client/chat/d;

    invoke-static {v0}, Lcom/teamspeak/ts3client/chat/d;->b(Lcom/teamspeak/ts3client/chat/d;)Ljava/util/ArrayList;

    move-result-object v0

    iget v2, p0, Lcom/teamspeak/ts3client/chat/e;->a:I

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/chat/a;

    .line 5174
    iget-object v2, v1, Lcom/teamspeak/ts3client/chat/d;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_66

    .line 5175
    iget-object v2, v1, Lcom/teamspeak/ts3client/chat/d;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 5176
    iget-object v2, v1, Lcom/teamspeak/ts3client/chat/d;->b:Ljava/util/HashMap;

    .line 6097
    iget-object v0, v0, Lcom/teamspeak/ts3client/chat/a;->g:Lcom/teamspeak/ts3client/data/c;

    .line 6188
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/c;->b:Ljava/lang/String;

    .line 5176
    invoke-virtual {v2, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 5177
    invoke-virtual {v1}, Lcom/teamspeak/ts3client/chat/d;->d()V

    .line 147
    :cond_66
    return-void
.end method
