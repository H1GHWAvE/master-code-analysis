.class public final Lnet/hockeyapp/android/c/g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final o:J = -0x79c0036a37b8b480L


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:I

.field public h:Ljava/lang/String;

.field public i:I

.field public j:Ljava/lang/String;

.field public k:Ljava/lang/String;

.field public l:Ljava/lang/String;

.field public m:Ljava/lang/String;

.field public n:Ljava/util/List;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a()Ljava/lang/String;
    .registers 2

    .prologue
    .line 59
    iget-object v0, p0, Lnet/hockeyapp/android/c/g;->a:Ljava/lang/String;

    return-object v0
.end method

.method private a(I)V
    .registers 2

    .prologue
    .line 111
    iput p1, p0, Lnet/hockeyapp/android/c/g;->g:I

    .line 112
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .registers 2

    .prologue
    .line 63
    iput-object p1, p0, Lnet/hockeyapp/android/c/g;->a:Ljava/lang/String;

    .line 64
    return-void
.end method

.method private a(Ljava/util/List;)V
    .registers 2

    .prologue
    .line 164
    iput-object p1, p0, Lnet/hockeyapp/android/c/g;->n:Ljava/util/List;

    return-void
.end method

.method private b()Ljava/lang/String;
    .registers 2

    .prologue
    .line 67
    iget-object v0, p0, Lnet/hockeyapp/android/c/g;->b:Ljava/lang/String;

    return-object v0
.end method

.method private b(I)V
    .registers 2

    .prologue
    .line 127
    iput p1, p0, Lnet/hockeyapp/android/c/g;->i:I

    .line 128
    return-void
.end method

.method private b(Ljava/lang/String;)V
    .registers 2

    .prologue
    .line 71
    iput-object p1, p0, Lnet/hockeyapp/android/c/g;->b:Ljava/lang/String;

    .line 72
    return-void
.end method

.method private c()Ljava/lang/String;
    .registers 2

    .prologue
    .line 75
    iget-object v0, p0, Lnet/hockeyapp/android/c/g;->c:Ljava/lang/String;

    return-object v0
.end method

.method private c(Ljava/lang/String;)V
    .registers 2

    .prologue
    .line 79
    iput-object p1, p0, Lnet/hockeyapp/android/c/g;->c:Ljava/lang/String;

    .line 80
    return-void
.end method

.method private d()Ljava/lang/String;
    .registers 2

    .prologue
    .line 83
    iget-object v0, p0, Lnet/hockeyapp/android/c/g;->d:Ljava/lang/String;

    return-object v0
.end method

.method private d(Ljava/lang/String;)V
    .registers 2

    .prologue
    .line 87
    iput-object p1, p0, Lnet/hockeyapp/android/c/g;->d:Ljava/lang/String;

    .line 88
    return-void
.end method

.method private e()Ljava/lang/String;
    .registers 2

    .prologue
    .line 91
    iget-object v0, p0, Lnet/hockeyapp/android/c/g;->e:Ljava/lang/String;

    return-object v0
.end method

.method private e(Ljava/lang/String;)V
    .registers 2

    .prologue
    .line 95
    iput-object p1, p0, Lnet/hockeyapp/android/c/g;->e:Ljava/lang/String;

    .line 96
    return-void
.end method

.method private f()Ljava/lang/String;
    .registers 2

    .prologue
    .line 99
    iget-object v0, p0, Lnet/hockeyapp/android/c/g;->f:Ljava/lang/String;

    return-object v0
.end method

.method private f(Ljava/lang/String;)V
    .registers 2

    .prologue
    .line 103
    iput-object p1, p0, Lnet/hockeyapp/android/c/g;->f:Ljava/lang/String;

    .line 104
    return-void
.end method

.method private g()I
    .registers 2

    .prologue
    .line 107
    iget v0, p0, Lnet/hockeyapp/android/c/g;->g:I

    return v0
.end method

.method private g(Ljava/lang/String;)V
    .registers 2

    .prologue
    .line 119
    iput-object p1, p0, Lnet/hockeyapp/android/c/g;->h:Ljava/lang/String;

    .line 120
    return-void
.end method

.method private h()Ljava/lang/String;
    .registers 2

    .prologue
    .line 115
    iget-object v0, p0, Lnet/hockeyapp/android/c/g;->h:Ljava/lang/String;

    return-object v0
.end method

.method private h(Ljava/lang/String;)V
    .registers 2

    .prologue
    .line 135
    iput-object p1, p0, Lnet/hockeyapp/android/c/g;->j:Ljava/lang/String;

    .line 136
    return-void
.end method

.method private i()I
    .registers 2

    .prologue
    .line 123
    iget v0, p0, Lnet/hockeyapp/android/c/g;->i:I

    return v0
.end method

.method private i(Ljava/lang/String;)V
    .registers 2

    .prologue
    .line 143
    iput-object p1, p0, Lnet/hockeyapp/android/c/g;->k:Ljava/lang/String;

    .line 144
    return-void
.end method

.method private j()Ljava/lang/String;
    .registers 2

    .prologue
    .line 131
    iget-object v0, p0, Lnet/hockeyapp/android/c/g;->j:Ljava/lang/String;

    return-object v0
.end method

.method private j(Ljava/lang/String;)V
    .registers 2

    .prologue
    .line 151
    iput-object p1, p0, Lnet/hockeyapp/android/c/g;->l:Ljava/lang/String;

    .line 152
    return-void
.end method

.method private k()Ljava/lang/String;
    .registers 2

    .prologue
    .line 139
    iget-object v0, p0, Lnet/hockeyapp/android/c/g;->k:Ljava/lang/String;

    return-object v0
.end method

.method private k(Ljava/lang/String;)V
    .registers 2

    .prologue
    .line 159
    iput-object p1, p0, Lnet/hockeyapp/android/c/g;->m:Ljava/lang/String;

    .line 160
    return-void
.end method

.method private l()Ljava/lang/String;
    .registers 2

    .prologue
    .line 147
    iget-object v0, p0, Lnet/hockeyapp/android/c/g;->l:Ljava/lang/String;

    return-object v0
.end method

.method private m()Ljava/lang/String;
    .registers 2

    .prologue
    .line 155
    iget-object v0, p0, Lnet/hockeyapp/android/c/g;->m:Ljava/lang/String;

    return-object v0
.end method

.method private n()Ljava/util/List;
    .registers 2

    .prologue
    .line 162
    iget-object v0, p0, Lnet/hockeyapp/android/c/g;->n:Ljava/util/List;

    return-object v0
.end method
