.class public Lnet/hockeyapp/android/aa;
.super Landroid/app/Activity;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:I

.field private d:Lnet/hockeyapp/android/d/p;

.field private e:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 57
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/String;)Ljava/lang/String;
    .registers 8

    .prologue
    .line 167
    :try_start_0
    const-string v0, "MD5"

    invoke-static {v0}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v0

    .line 168
    invoke-virtual {p0}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/security/MessageDigest;->update([B)V

    .line 169
    invoke-virtual {v0}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v2

    .line 172
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 173
    array-length v4, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_19
    if-ge v1, v4, :cond_41

    aget-byte v0, v2, v1

    .line 174
    and-int/lit16 v0, v0, 0xff

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    .line 175
    :goto_23
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    const/4 v6, 0x2

    if-ge v5, v6, :cond_3a

    .line 176
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "0"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_23

    .line 177
    :cond_3a
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 173
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_19

    .line 179
    :cond_41
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_44
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_44} :catch_46

    move-result-object v0

    .line 184
    :goto_45
    return-object v0

    .line 182
    :catch_46
    move-exception v0

    invoke-virtual {v0}, Ljava/security/NoSuchAlgorithmException;->printStackTrace()V

    .line 184
    const-string v0, ""

    goto :goto_45
.end method

.method private a()V
    .registers 3

    .prologue
    .line 107
    iget v0, p0, Lnet/hockeyapp/android/aa;->c:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_11

    .line 108
    const/16 v0, 0x3004

    invoke-virtual {p0, v0}, Lnet/hockeyapp/android/aa;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 109
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setVisibility(I)V

    .line 112
    :cond_11
    const/16 v0, 0x3005

    invoke-virtual {p0, v0}, Lnet/hockeyapp/android/aa;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 113
    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 114
    return-void
.end method

.method private b()V
    .registers 2

    .prologue
    .line 117
    new-instance v0, Lnet/hockeyapp/android/ab;

    invoke-direct {v0, p0}, Lnet/hockeyapp/android/ab;-><init>(Lnet/hockeyapp/android/aa;)V

    iput-object v0, p0, Lnet/hockeyapp/android/aa;->e:Landroid/os/Handler;

    .line 135
    return-void
.end method

.method private c()V
    .registers 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 138
    const/16 v0, 0x3003

    invoke-virtual {p0, v0}, Lnet/hockeyapp/android/aa;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    .line 139
    const/16 v0, 0x3004

    invoke-virtual {p0, v0}, Lnet/hockeyapp/android/aa;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 142
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 144
    iget v4, p0, Lnet/hockeyapp/android/aa;->c:I

    if-ne v4, v1, :cond_6b

    .line 145
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_69

    move v0, v1

    .line 146
    :goto_32
    const-string v1, "email"

    invoke-interface {v5, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 147
    const-string v1, "authcode"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lnet/hockeyapp/android/aa;->b:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lnet/hockeyapp/android/aa;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v5, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 155
    :goto_53
    if-eqz v0, :cond_8a

    .line 156
    new-instance v0, Lnet/hockeyapp/android/d/p;

    iget-object v2, p0, Lnet/hockeyapp/android/aa;->e:Landroid/os/Handler;

    iget-object v3, p0, Lnet/hockeyapp/android/aa;->a:Ljava/lang/String;

    iget v4, p0, Lnet/hockeyapp/android/aa;->c:I

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lnet/hockeyapp/android/d/p;-><init>(Landroid/content/Context;Landroid/os/Handler;Ljava/lang/String;ILjava/util/Map;)V

    iput-object v0, p0, Lnet/hockeyapp/android/aa;->d:Lnet/hockeyapp/android/d/p;

    .line 157
    iget-object v0, p0, Lnet/hockeyapp/android/aa;->d:Lnet/hockeyapp/android/d/p;

    invoke-static {v0}, Lnet/hockeyapp/android/e/a;->a(Landroid/os/AsyncTask;)V

    .line 162
    :goto_68
    return-void

    :cond_69
    move v0, v2

    .line 145
    goto :goto_32

    .line 149
    :cond_6b
    iget v4, p0, Lnet/hockeyapp/android/aa;->c:I

    const/4 v6, 0x2

    if-ne v4, v6, :cond_9a

    .line 150
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_88

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_88

    .line 151
    :goto_7c
    const-string v2, "email"

    invoke-interface {v5, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 152
    const-string v2, "password"

    invoke-interface {v5, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move v0, v1

    goto :goto_53

    :cond_88
    move v1, v2

    .line 150
    goto :goto_7c

    .line 160
    :cond_8a
    const/16 v0, 0x501

    invoke-static {v0}, Lnet/hockeyapp/android/aj;->a(I)Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x3e8

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_68

    :cond_9a
    move v0, v2

    goto :goto_53
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .registers 9

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 189
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_a6

    .line 2157
    :goto_9
    return-void

    .line 2138
    :pswitch_a
    const/16 v0, 0x3003

    invoke-virtual {p0, v0}, Lnet/hockeyapp/android/aa;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2139
    const/16 v0, 0x3004

    invoke-virtual {p0, v0}, Lnet/hockeyapp/android/aa;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2142
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 2144
    iget v4, p0, Lnet/hockeyapp/android/aa;->c:I

    if-ne v4, v1, :cond_73

    .line 2145
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_71

    move v0, v1

    .line 2146
    :goto_3a
    const-string v1, "email"

    invoke-interface {v5, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2147
    const-string v1, "authcode"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lnet/hockeyapp/android/aa;->b:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lnet/hockeyapp/android/aa;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v5, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2155
    :goto_5b
    if-eqz v0, :cond_92

    .line 2156
    new-instance v0, Lnet/hockeyapp/android/d/p;

    iget-object v2, p0, Lnet/hockeyapp/android/aa;->e:Landroid/os/Handler;

    iget-object v3, p0, Lnet/hockeyapp/android/aa;->a:Ljava/lang/String;

    iget v4, p0, Lnet/hockeyapp/android/aa;->c:I

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lnet/hockeyapp/android/d/p;-><init>(Landroid/content/Context;Landroid/os/Handler;Ljava/lang/String;ILjava/util/Map;)V

    iput-object v0, p0, Lnet/hockeyapp/android/aa;->d:Lnet/hockeyapp/android/d/p;

    .line 2157
    iget-object v0, p0, Lnet/hockeyapp/android/aa;->d:Lnet/hockeyapp/android/d/p;

    invoke-static {v0}, Lnet/hockeyapp/android/e/a;->a(Landroid/os/AsyncTask;)V

    goto :goto_9

    :cond_71
    move v0, v2

    .line 2145
    goto :goto_3a

    .line 2149
    :cond_73
    iget v4, p0, Lnet/hockeyapp/android/aa;->c:I

    const/4 v6, 0x2

    if-ne v4, v6, :cond_a3

    .line 2150
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_90

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_90

    .line 2151
    :goto_84
    const-string v2, "email"

    invoke-interface {v5, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2152
    const-string v2, "password"

    invoke-interface {v5, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move v0, v1

    goto :goto_5b

    :cond_90
    move v1, v2

    .line 2150
    goto :goto_84

    .line 2160
    :cond_92
    const/16 v0, 0x501

    invoke-static {v0}, Lnet/hockeyapp/android/aj;->a(I)Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x3e8

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_9

    :cond_a3
    move v0, v2

    goto :goto_5b

    .line 189
    nop

    :pswitch_data_a6
    .packed-switch 0x3005
        :pswitch_a
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 4

    .prologue
    .line 85
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 86
    new-instance v0, Lnet/hockeyapp/android/f/j;

    invoke-direct {v0, p0}, Lnet/hockeyapp/android/f/j;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0, v0}, Lnet/hockeyapp/android/aa;->setContentView(Landroid/view/View;)V

    .line 88
    invoke-virtual {p0}, Lnet/hockeyapp/android/aa;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 89
    if-eqz v0, :cond_2d

    .line 90
    const-string v1, "url"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lnet/hockeyapp/android/aa;->a:Ljava/lang/String;

    .line 91
    const-string v1, "secret"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lnet/hockeyapp/android/aa;->b:Ljava/lang/String;

    .line 92
    const-string v1, "mode"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lnet/hockeyapp/android/aa;->c:I

    .line 1107
    :cond_2d
    iget v0, p0, Lnet/hockeyapp/android/aa;->c:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3e

    .line 1108
    const/16 v0, 0x3004

    invoke-virtual {p0, v0}, Lnet/hockeyapp/android/aa;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 1109
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setVisibility(I)V

    .line 1112
    :cond_3e
    const/16 v0, 0x3005

    invoke-virtual {p0, v0}, Lnet/hockeyapp/android/aa;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 1113
    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1117
    new-instance v0, Lnet/hockeyapp/android/ab;

    invoke-direct {v0, p0}, Lnet/hockeyapp/android/ab;-><init>(Lnet/hockeyapp/android/aa;)V

    iput-object v0, p0, Lnet/hockeyapp/android/aa;->e:Landroid/os/Handler;

    .line 99
    invoke-virtual {p0}, Lnet/hockeyapp/android/aa;->getLastNonConfigurationInstance()Ljava/lang/Object;

    move-result-object v0

    .line 100
    if-eqz v0, :cond_62

    .line 101
    check-cast v0, Lnet/hockeyapp/android/d/p;

    iput-object v0, p0, Lnet/hockeyapp/android/aa;->d:Lnet/hockeyapp/android/d/p;

    .line 102
    iget-object v0, p0, Lnet/hockeyapp/android/aa;->d:Lnet/hockeyapp/android/d/p;

    iget-object v1, p0, Lnet/hockeyapp/android/aa;->e:Landroid/os/Handler;

    .line 2099
    iput-object p0, v0, Lnet/hockeyapp/android/d/p;->a:Landroid/content/Context;

    .line 2100
    iput-object v1, v0, Lnet/hockeyapp/android/d/p;->b:Landroid/os/Handler;

    .line 104
    :cond_62
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .registers 6

    .prologue
    const/4 v0, 0x1

    .line 201
    const/4 v1, 0x4

    if-ne p1, v1, :cond_1d

    .line 202
    sget-object v1, Lnet/hockeyapp/android/ac;->g:Lnet/hockeyapp/android/ae;

    if-nez v1, :cond_1d

    .line 206
    new-instance v1, Landroid/content/Intent;

    sget-object v2, Lnet/hockeyapp/android/ac;->f:Ljava/lang/Class;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 207
    const/high16 v2, 0x4000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 208
    const-string v2, "net.hockeyapp.android.EXIT"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 209
    invoke-virtual {p0, v1}, Lnet/hockeyapp/android/aa;->startActivity(Landroid/content/Intent;)V

    .line 214
    :goto_1c
    return v0

    :cond_1d
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_1c
.end method

.method public onRetainNonConfigurationInstance()Ljava/lang/Object;
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 226
    iget-object v0, p0, Lnet/hockeyapp/android/aa;->d:Lnet/hockeyapp/android/d/p;

    if-eqz v0, :cond_d

    .line 227
    iget-object v0, p0, Lnet/hockeyapp/android/aa;->d:Lnet/hockeyapp/android/d/p;

    .line 3104
    iput-object v1, v0, Lnet/hockeyapp/android/d/p;->a:Landroid/content/Context;

    .line 3105
    iput-object v1, v0, Lnet/hockeyapp/android/d/p;->b:Landroid/os/Handler;

    .line 3106
    iput-object v1, v0, Lnet/hockeyapp/android/d/p;->c:Landroid/app/ProgressDialog;

    .line 230
    :cond_d
    iget-object v0, p0, Lnet/hockeyapp/android/aa;->d:Lnet/hockeyapp/android/d/p;

    return-object v0
.end method
