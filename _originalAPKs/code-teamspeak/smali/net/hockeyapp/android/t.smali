.class public final Lnet/hockeyapp/android/t;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:I = 0x1

.field private static final b:I = 0x1

.field private static final c:Ljava/lang/String; = "net.hockeyapp.android.SCREENSHOT"

.field private static d:Landroid/content/BroadcastReceiver;

.field private static e:Landroid/app/Activity;

.field private static f:Z

.field private static g:Ljava/lang/String;

.field private static h:Ljava/lang/String;

.field private static i:Lnet/hockeyapp/android/c/i;

.field private static j:Lnet/hockeyapp/android/c/i;

.field private static k:Lnet/hockeyapp/android/y;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    const/4 v1, 0x0

    .line 84
    sput-object v1, Lnet/hockeyapp/android/t;->d:Landroid/content/BroadcastReceiver;

    .line 94
    const/4 v0, 0x0

    sput-boolean v0, Lnet/hockeyapp/android/t;->f:Z

    .line 99
    sput-object v1, Lnet/hockeyapp/android/t;->g:Ljava/lang/String;

    .line 104
    sput-object v1, Lnet/hockeyapp/android/t;->h:Ljava/lang/String;

    .line 119
    sput-object v1, Lnet/hockeyapp/android/t;->k:Lnet/hockeyapp/android/y;

    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 405
    return-void
.end method

.method public static a()Lnet/hockeyapp/android/y;
    .registers 1

    .prologue
    .line 242
    sget-object v0, Lnet/hockeyapp/android/t;->k:Lnet/hockeyapp/android/y;

    return-object v0
.end method

.method private static a(Landroid/app/Activity;)V
    .registers 8

    .prologue
    const/4 v6, 0x1

    .line 293
    sput-object p0, Lnet/hockeyapp/android/t;->e:Landroid/app/Activity;

    .line 295
    sget-boolean v0, Lnet/hockeyapp/android/t;->f:Z

    if-nez v0, :cond_5b

    .line 4367
    sput-boolean v6, Lnet/hockeyapp/android/t;->f:Z

    .line 4369
    sget-object v0, Lnet/hockeyapp/android/t;->e:Landroid/app/Activity;

    const-string v1, "notification"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 4371
    sget-object v1, Lnet/hockeyapp/android/t;->e:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const-string v2, "ic_menu_camera"

    const-string v3, "drawable"

    const-string v4, "android"

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 4373
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 4374
    const-string v3, "net.hockeyapp.android.SCREENSHOT"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 4375
    sget-object v3, Lnet/hockeyapp/android/t;->e:Landroid/app/Activity;

    const/high16 v4, 0x40000000    # 2.0f

    invoke-static {v3, v6, v2, v4}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 4377
    sget-object v3, Lnet/hockeyapp/android/t;->e:Landroid/app/Activity;

    const-string v4, "HockeyApp Feedback"

    const-string v5, "Take a screenshot for your feedback."

    invoke-static {v3, v2, v4, v5, v1}, Lnet/hockeyapp/android/e/w;->a(Landroid/content/Context;Landroid/app/PendingIntent;Ljava/lang/String;Ljava/lang/String;I)Landroid/app/Notification;

    move-result-object v1

    .line 4379
    invoke-virtual {v0, v6, v1}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 4381
    sget-object v0, Lnet/hockeyapp/android/t;->d:Landroid/content/BroadcastReceiver;

    if-nez v0, :cond_4d

    .line 4382
    new-instance v0, Lnet/hockeyapp/android/w;

    invoke-direct {v0}, Lnet/hockeyapp/android/w;-><init>()V

    sput-object v0, Lnet/hockeyapp/android/t;->d:Landroid/content/BroadcastReceiver;

    .line 4389
    :cond_4d
    sget-object v0, Lnet/hockeyapp/android/t;->e:Landroid/app/Activity;

    sget-object v1, Lnet/hockeyapp/android/t;->d:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "net.hockeyapp.android.SCREENSHOT"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/Activity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 298
    :cond_5b
    return-void
.end method

.method public static a(Landroid/content/Context;)V
    .registers 10

    .prologue
    const/4 v8, 0x0

    const/4 v1, 0x1

    .line 321
    sget-object v0, Lnet/hockeyapp/android/t;->e:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    .line 322
    invoke-virtual {v0, v1}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    .line 323
    invoke-virtual {v0}, Landroid/view/View;->getDrawingCache()Landroid/graphics/Bitmap;

    move-result-object v3

    .line 325
    sget-object v0, Lnet/hockeyapp/android/t;->e:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getLocalClassName()Ljava/lang/String;

    move-result-object v4

    .line 326
    invoke-static {}, Lnet/hockeyapp/android/a;->a()Ljava/io/File;

    move-result-object v5

    .line 327
    new-instance v0, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, ".jpg"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v5, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    move-object v2, v0

    move v0, v1

    .line 329
    :goto_37
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_62

    .line 330
    new-instance v2, Ljava/io/File;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "_"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ".jpg"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v2, v5, v6}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 331
    add-int/lit8 v0, v0, 0x1

    goto :goto_37

    .line 334
    :cond_62
    new-instance v0, Lnet/hockeyapp/android/v;

    invoke-direct {v0, v3, p0}, Lnet/hockeyapp/android/v;-><init>(Landroid/graphics/Bitmap;Landroid/content/Context;)V

    new-array v1, v1, [Ljava/io/File;

    aput-object v2, v1, v8

    .line 354
    invoke-virtual {v0, v1}, Lnet/hockeyapp/android/v;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 357
    new-instance v0, Lnet/hockeyapp/android/x;

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, v8}, Lnet/hockeyapp/android/x;-><init>(Ljava/lang/String;B)V

    .line 358
    new-instance v1, Landroid/media/MediaScannerConnection;

    sget-object v3, Lnet/hockeyapp/android/t;->e:Landroid/app/Activity;

    invoke-direct {v1, v3, v0}, Landroid/media/MediaScannerConnection;-><init>(Landroid/content/Context;Landroid/media/MediaScannerConnection$MediaScannerConnectionClient;)V

    .line 4417
    iput-object v1, v0, Lnet/hockeyapp/android/x;->a:Landroid/media/MediaScannerConnection;

    .line 360
    invoke-virtual {v1}, Landroid/media/MediaScannerConnection;->connect()V

    .line 362
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Screenshot \'"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\' is available in gallery."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x7d0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 363
    return-void
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;)V
    .registers 4

    .prologue
    .line 128
    .line 1139
    const-string v0, "https://sdk.hockeyapp.net/"

    .line 1151
    if-eqz p0, :cond_12

    .line 1152
    invoke-static {p1}, Lnet/hockeyapp/android/e/w;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lnet/hockeyapp/android/t;->g:Ljava/lang/String;

    .line 1153
    sput-object v0, Lnet/hockeyapp/android/t;->h:Ljava/lang/String;

    .line 1154
    const/4 v0, 0x0

    sput-object v0, Lnet/hockeyapp/android/t;->k:Lnet/hockeyapp/android/y;

    .line 1156
    invoke-static {p0}, Lnet/hockeyapp/android/a;->a(Landroid/content/Context;)V

    .line 129
    :cond_12
    return-void
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lnet/hockeyapp/android/y;)V
    .registers 5

    .prologue
    .line 151
    if-eqz p0, :cond_f

    .line 152
    invoke-static {p2}, Lnet/hockeyapp/android/e/w;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lnet/hockeyapp/android/t;->g:Ljava/lang/String;

    .line 153
    sput-object p1, Lnet/hockeyapp/android/t;->h:Ljava/lang/String;

    .line 154
    sput-object p3, Lnet/hockeyapp/android/t;->k:Lnet/hockeyapp/android/y;

    .line 156
    invoke-static {p0}, Lnet/hockeyapp/android/a;->a(Landroid/content/Context;)V

    .line 158
    :cond_f
    return-void
.end method

.method private static varargs a(Landroid/content/Context;[Landroid/net/Uri;)V
    .registers 5

    .prologue
    .line 173
    .line 2182
    if-eqz p0, :cond_2b

    .line 2183
    const/4 v0, 0x0

    .line 2184
    sget-object v1, Lnet/hockeyapp/android/t;->k:Lnet/hockeyapp/android/y;

    if-eqz v1, :cond_9

    .line 3046
    const-class v0, Lnet/hockeyapp/android/FeedbackActivity;

    .line 2187
    :cond_9
    if-nez v0, :cond_d

    .line 2188
    const-class v0, Lnet/hockeyapp/android/FeedbackActivity;

    .line 2191
    :cond_d
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 2195
    const/high16 v2, 0x10000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 2196
    invoke-virtual {v1, p0, v0}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 2197
    const-string v0, "url"

    invoke-static {}, Lnet/hockeyapp/android/t;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2198
    const-string v0, "initialAttachments"

    invoke-virtual {v1, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2199
    invoke-virtual {p0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 174
    :cond_2b
    return-void
.end method

.method private static a(Lnet/hockeyapp/android/c/i;)V
    .registers 1

    .prologue
    .line 267
    sput-object p0, Lnet/hockeyapp/android/t;->i:Lnet/hockeyapp/android/c/i;

    .line 268
    return-void
.end method

.method public static b()Lnet/hockeyapp/android/c/i;
    .registers 1

    .prologue
    .line 259
    sget-object v0, Lnet/hockeyapp/android/t;->i:Lnet/hockeyapp/android/c/i;

    return-object v0
.end method

.method private static b(Landroid/app/Activity;)V
    .registers 3

    .prologue
    .line 306
    sget-object v0, Lnet/hockeyapp/android/t;->e:Landroid/app/Activity;

    if-eqz v0, :cond_8

    sget-object v0, Lnet/hockeyapp/android/t;->e:Landroid/app/Activity;

    if-eq v0, p0, :cond_9

    .line 312
    :cond_8
    :goto_8
    return-void

    .line 4395
    :cond_9
    const/4 v0, 0x0

    sput-boolean v0, Lnet/hockeyapp/android/t;->f:Z

    .line 4397
    sget-object v0, Lnet/hockeyapp/android/t;->e:Landroid/app/Activity;

    sget-object v1, Lnet/hockeyapp/android/t;->d:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 4398
    sget-object v0, Lnet/hockeyapp/android/t;->e:Landroid/app/Activity;

    const-string v1, "notification"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 4399
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 311
    const/4 v0, 0x0

    sput-object v0, Lnet/hockeyapp/android/t;->e:Landroid/app/Activity;

    goto :goto_8
.end method

.method private static b(Landroid/content/Context;)V
    .registers 14

    .prologue
    const/4 v12, 0x0

    const/4 v3, 0x0

    .line 4065
    sget-object v0, Lnet/hockeyapp/android/e/p;->a:Lnet/hockeyapp/android/e/n;

    .line 210
    invoke-virtual {v0, p0}, Lnet/hockeyapp/android/e/n;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v8

    .line 211
    if-nez v8, :cond_b

    .line 234
    :goto_a
    return-void

    .line 215
    :cond_b
    const-string v0, "net.hockeyapp.android.feedback"

    invoke-virtual {p0, v0, v12}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "idLastMessageSend"

    const/4 v2, -0x1

    .line 216
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v11

    .line 218
    new-instance v0, Lnet/hockeyapp/android/d/r;

    invoke-static {}, Lnet/hockeyapp/android/t;->f()Ljava/lang/String;

    move-result-object v2

    new-instance v9, Lnet/hockeyapp/android/u;

    invoke-direct {v9, p0}, Lnet/hockeyapp/android/u;-><init>(Landroid/content/Context;)V

    const/4 v10, 0x1

    move-object v1, p0

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    move-object v7, v3

    invoke-direct/range {v0 .. v10}, Lnet/hockeyapp/android/d/r;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Landroid/os/Handler;Z)V

    .line 4114
    iput-boolean v12, v0, Lnet/hockeyapp/android/d/r;->a:Z

    .line 4118
    iput v11, v0, Lnet/hockeyapp/android/d/r;->b:I

    .line 233
    invoke-static {v0}, Lnet/hockeyapp/android/e/a;->a(Landroid/os/AsyncTask;)V

    goto :goto_a
.end method

.method private static b(Landroid/content/Context;Ljava/lang/String;)V
    .registers 4

    .prologue
    .line 139
    const-string v0, "https://sdk.hockeyapp.net/"

    .line 2151
    if-eqz p0, :cond_12

    .line 2152
    invoke-static {p1}, Lnet/hockeyapp/android/e/w;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lnet/hockeyapp/android/t;->g:Ljava/lang/String;

    .line 2153
    sput-object v0, Lnet/hockeyapp/android/t;->h:Ljava/lang/String;

    .line 2154
    const/4 v0, 0x0

    sput-object v0, Lnet/hockeyapp/android/t;->k:Lnet/hockeyapp/android/y;

    .line 2156
    invoke-static {p0}, Lnet/hockeyapp/android/a;->a(Landroid/content/Context;)V

    .line 140
    :cond_12
    return-void
.end method

.method private static varargs b(Landroid/content/Context;[Landroid/net/Uri;)V
    .registers 5

    .prologue
    .line 182
    if-eqz p0, :cond_2b

    .line 183
    const/4 v0, 0x0

    .line 184
    sget-object v1, Lnet/hockeyapp/android/t;->k:Lnet/hockeyapp/android/y;

    if-eqz v1, :cond_9

    .line 4046
    const-class v0, Lnet/hockeyapp/android/FeedbackActivity;

    .line 187
    :cond_9
    if-nez v0, :cond_d

    .line 188
    const-class v0, Lnet/hockeyapp/android/FeedbackActivity;

    .line 191
    :cond_d
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 195
    const/high16 v2, 0x10000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 196
    invoke-virtual {v1, p0, v0}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 197
    const-string v0, "url"

    invoke-static {}, Lnet/hockeyapp/android/t;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 198
    const-string v0, "initialAttachments"

    invoke-virtual {v1, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 199
    invoke-virtual {p0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 201
    :cond_2b
    return-void
.end method

.method private static b(Lnet/hockeyapp/android/c/i;)V
    .registers 1

    .prologue
    .line 283
    sput-object p0, Lnet/hockeyapp/android/t;->j:Lnet/hockeyapp/android/c/i;

    .line 284
    return-void
.end method

.method public static c()Lnet/hockeyapp/android/c/i;
    .registers 1

    .prologue
    .line 275
    sget-object v0, Lnet/hockeyapp/android/t;->j:Lnet/hockeyapp/android/c/i;

    return-object v0
.end method

.method static synthetic d()Ljava/lang/String;
    .registers 1

    .prologue
    .line 65
    invoke-static {}, Lnet/hockeyapp/android/t;->f()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static e()V
    .registers 1

    .prologue
    .line 164
    const/4 v0, 0x0

    sput-object v0, Lnet/hockeyapp/android/t;->k:Lnet/hockeyapp/android/y;

    .line 165
    return-void
.end method

.method private static f()Ljava/lang/String;
    .registers 2

    .prologue
    .line 251
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lnet/hockeyapp/android/t;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "api/2/apps/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lnet/hockeyapp/android/t;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/feedback/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static g()V
    .registers 7

    .prologue
    const/4 v6, 0x1

    .line 367
    sput-boolean v6, Lnet/hockeyapp/android/t;->f:Z

    .line 369
    sget-object v0, Lnet/hockeyapp/android/t;->e:Landroid/app/Activity;

    const-string v1, "notification"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 371
    sget-object v1, Lnet/hockeyapp/android/t;->e:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const-string v2, "ic_menu_camera"

    const-string v3, "drawable"

    const-string v4, "android"

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 373
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 374
    const-string v3, "net.hockeyapp.android.SCREENSHOT"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 375
    sget-object v3, Lnet/hockeyapp/android/t;->e:Landroid/app/Activity;

    const/high16 v4, 0x40000000    # 2.0f

    invoke-static {v3, v6, v2, v4}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 377
    sget-object v3, Lnet/hockeyapp/android/t;->e:Landroid/app/Activity;

    const-string v4, "HockeyApp Feedback"

    const-string v5, "Take a screenshot for your feedback."

    invoke-static {v3, v2, v4, v5, v1}, Lnet/hockeyapp/android/e/w;->a(Landroid/content/Context;Landroid/app/PendingIntent;Ljava/lang/String;Ljava/lang/String;I)Landroid/app/Notification;

    move-result-object v1

    .line 379
    invoke-virtual {v0, v6, v1}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 381
    sget-object v0, Lnet/hockeyapp/android/t;->d:Landroid/content/BroadcastReceiver;

    if-nez v0, :cond_47

    .line 382
    new-instance v0, Lnet/hockeyapp/android/w;

    invoke-direct {v0}, Lnet/hockeyapp/android/w;-><init>()V

    sput-object v0, Lnet/hockeyapp/android/t;->d:Landroid/content/BroadcastReceiver;

    .line 389
    :cond_47
    sget-object v0, Lnet/hockeyapp/android/t;->e:Landroid/app/Activity;

    sget-object v1, Lnet/hockeyapp/android/t;->d:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "net.hockeyapp.android.SCREENSHOT"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/Activity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 390
    return-void
.end method

.method private static h()V
    .registers 2

    .prologue
    .line 395
    const/4 v0, 0x0

    sput-boolean v0, Lnet/hockeyapp/android/t;->f:Z

    .line 397
    sget-object v0, Lnet/hockeyapp/android/t;->e:Landroid/app/Activity;

    sget-object v1, Lnet/hockeyapp/android/t;->d:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 398
    sget-object v0, Lnet/hockeyapp/android/t;->e:Landroid/app/Activity;

    const-string v1, "notification"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 399
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 400
    return-void
.end method
