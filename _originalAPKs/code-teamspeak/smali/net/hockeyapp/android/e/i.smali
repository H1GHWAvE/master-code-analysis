.class public final Lnet/hockeyapp/android/e/i;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method private constructor <init>()V
    .registers 1

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    return-void
.end method

.method synthetic constructor <init>(B)V
    .registers 2

    .prologue
    .line 50
    invoke-direct {p0}, Lnet/hockeyapp/android/e/i;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;)Lnet/hockeyapp/android/c/h;
    .registers 34

    .prologue
    .line 74
    const/4 v3, 0x0

    .line 76
    if-eqz p0, :cond_225

    .line 78
    :try_start_3
    new-instance v7, Lorg/json/JSONObject;

    move-object/from16 v0, p0

    invoke-direct {v7, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 80
    const-string v2, "feedback"

    invoke-virtual {v7, v2}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v8

    .line 81
    new-instance v9, Lnet/hockeyapp/android/c/d;

    invoke-direct {v9}, Lnet/hockeyapp/android/c/d;-><init>()V

    .line 84
    const-string v2, "messages"

    invoke-virtual {v8, v2}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v10

    .line 85
    const/4 v2, 0x0

    .line 88
    invoke-virtual {v10}, Lorg/json/JSONArray;->length()I

    move-result v4

    if-lez v4, :cond_1ad

    .line 89
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 91
    const/4 v4, 0x0

    move v6, v4

    :goto_29
    invoke-virtual {v10}, Lorg/json/JSONArray;->length()I

    move-result v4

    if-ge v6, v4, :cond_1ad

    .line 92
    invoke-virtual {v10, v6}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "subject"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v11

    .line 93
    invoke-virtual {v10, v6}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "text"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v12

    .line 94
    invoke-virtual {v10, v6}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "oem"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v13

    .line 95
    invoke-virtual {v10, v6}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "model"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v14

    .line 96
    invoke-virtual {v10, v6}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "os_version"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v15

    .line 97
    invoke-virtual {v10, v6}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "created_at"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v16

    .line 98
    invoke-virtual {v10, v6}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "id"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v17

    .line 99
    invoke-virtual {v10, v6}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "token"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v18

    .line 100
    invoke-virtual {v10, v6}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "via"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v19

    .line 101
    invoke-virtual {v10, v6}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "user_string"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v20

    .line 102
    invoke-virtual {v10, v6}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "clean_text"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v21

    .line 103
    invoke-virtual {v10, v6}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "name"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v22

    .line 104
    invoke-virtual {v10, v6}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "app_id"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v23

    .line 106
    invoke-virtual {v10, v6}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "attachments"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v24

    .line 107
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v4

    .line 108
    if-eqz v24, :cond_174

    .line 109
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 111
    const/4 v5, 0x0

    :goto_f3
    invoke-virtual/range {v24 .. v24}, Lorg/json/JSONArray;->length()I

    move-result v25

    move/from16 v0, v25

    if-ge v5, v0, :cond_174

    .line 112
    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v25

    const-string v26, "id"

    invoke-virtual/range {v25 .. v26}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v25

    .line 113
    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v26

    const-string v27, "feedback_message_id"

    invoke-virtual/range {v26 .. v27}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v26

    .line 114
    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v27

    const-string v28, "file_name"

    invoke-virtual/range {v27 .. v28}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v27

    .line 115
    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v28

    const-string v29, "url"

    invoke-virtual/range {v28 .. v29}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v28

    .line 116
    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v29

    const-string v30, "created_at"

    invoke-virtual/range {v29 .. v30}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v29

    .line 117
    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v30

    const-string v31, "updated_at"

    invoke-virtual/range {v30 .. v31}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v30

    .line 119
    new-instance v31, Lnet/hockeyapp/android/c/e;

    invoke-direct/range {v31 .. v31}, Lnet/hockeyapp/android/c/e;-><init>()V

    .line 1059
    move/from16 v0, v25

    move-object/from16 v1, v31

    iput v0, v1, Lnet/hockeyapp/android/c/e;->a:I

    .line 1064
    move/from16 v0, v26

    move-object/from16 v1, v31

    iput v0, v1, Lnet/hockeyapp/android/c/e;->b:I

    .line 1071
    move-object/from16 v0, v27

    move-object/from16 v1, v31

    iput-object v0, v1, Lnet/hockeyapp/android/c/e;->c:Ljava/lang/String;

    .line 1079
    move-object/from16 v0, v28

    move-object/from16 v1, v31

    iput-object v0, v1, Lnet/hockeyapp/android/c/e;->d:Ljava/lang/String;

    .line 1087
    move-object/from16 v0, v29

    move-object/from16 v1, v31

    iput-object v0, v1, Lnet/hockeyapp/android/c/e;->e:Ljava/lang/String;

    .line 1092
    move-object/from16 v0, v30

    move-object/from16 v1, v31

    iput-object v0, v1, Lnet/hockeyapp/android/c/e;->f:Ljava/lang/String;

    .line 126
    move-object/from16 v0, v31

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 111
    add-int/lit8 v5, v5, 0x1

    goto :goto_f3

    .line 130
    :cond_174
    new-instance v5, Lnet/hockeyapp/android/c/g;

    invoke-direct {v5}, Lnet/hockeyapp/android/c/g;-><init>()V

    .line 1159
    move-object/from16 v0, v23

    iput-object v0, v5, Lnet/hockeyapp/android/c/g;->m:Ljava/lang/String;

    .line 2143
    move-object/from16 v0, v21

    iput-object v0, v5, Lnet/hockeyapp/android/c/g;->k:Ljava/lang/String;

    .line 3103
    move-object/from16 v0, v16

    iput-object v0, v5, Lnet/hockeyapp/android/c/g;->f:Ljava/lang/String;

    .line 3111
    move/from16 v0, v17

    iput v0, v5, Lnet/hockeyapp/android/c/g;->g:I

    .line 4087
    iput-object v14, v5, Lnet/hockeyapp/android/c/g;->d:Ljava/lang/String;

    .line 4151
    move-object/from16 v0, v22

    iput-object v0, v5, Lnet/hockeyapp/android/c/g;->l:Ljava/lang/String;

    .line 5079
    iput-object v13, v5, Lnet/hockeyapp/android/c/g;->c:Ljava/lang/String;

    .line 5095
    iput-object v15, v5, Lnet/hockeyapp/android/c/g;->e:Ljava/lang/String;

    .line 6063
    iput-object v11, v5, Lnet/hockeyapp/android/c/g;->a:Ljava/lang/String;

    .line 6071
    iput-object v12, v5, Lnet/hockeyapp/android/c/g;->b:Ljava/lang/String;

    .line 6119
    move-object/from16 v0, v18

    iput-object v0, v5, Lnet/hockeyapp/android/c/g;->h:Ljava/lang/String;

    .line 6135
    move-object/from16 v0, v20

    iput-object v0, v5, Lnet/hockeyapp/android/c/g;->j:Ljava/lang/String;

    .line 7127
    move/from16 v0, v19

    iput v0, v5, Lnet/hockeyapp/android/c/g;->i:I

    .line 7164
    iput-object v4, v5, Lnet/hockeyapp/android/c/g;->n:Ljava/util/List;

    .line 145
    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 91
    add-int/lit8 v4, v6, 0x1

    move v6, v4

    goto/16 :goto_29

    .line 8086
    :cond_1ad
    iput-object v2, v9, Lnet/hockeyapp/android/c/d;->e:Ljava/util/ArrayList;
    :try_end_1af
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_1af} :catch_200

    .line 152
    :try_start_1af
    const-string v2, "name"

    invoke-virtual {v8, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    .line 9054
    iput-object v2, v9, Lnet/hockeyapp/android/c/d;->a:Ljava/lang/String;
    :try_end_1bb
    .catch Lorg/json/JSONException; {:try_start_1af .. :try_end_1bb} :catch_1fb

    .line 158
    :goto_1bb
    :try_start_1bb
    const-string v2, "email"

    invoke-virtual {v8, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    .line 9062
    iput-object v2, v9, Lnet/hockeyapp/android/c/d;->b:Ljava/lang/String;
    :try_end_1c7
    .catch Lorg/json/JSONException; {:try_start_1bb .. :try_end_1c7} :catch_20a

    .line 164
    :goto_1c7
    :try_start_1c7
    const-string v2, "id"

    invoke-virtual {v8, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 9070
    iput v2, v9, Lnet/hockeyapp/android/c/d;->c:I
    :try_end_1cf
    .catch Lorg/json/JSONException; {:try_start_1c7 .. :try_end_1cf} :catch_20f

    .line 170
    :goto_1cf
    :try_start_1cf
    const-string v2, "created_at"

    invoke-virtual {v8, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    .line 9078
    iput-object v2, v9, Lnet/hockeyapp/android/c/d;->d:Ljava/lang/String;
    :try_end_1db
    .catch Lorg/json/JSONException; {:try_start_1cf .. :try_end_1db} :catch_214

    .line 175
    :goto_1db
    :try_start_1db
    new-instance v2, Lnet/hockeyapp/android/c/h;

    invoke-direct {v2}, Lnet/hockeyapp/android/c/h;-><init>()V
    :try_end_1e0
    .catch Lorg/json/JSONException; {:try_start_1db .. :try_end_1e0} :catch_200

    .line 10059
    :try_start_1e0
    iput-object v9, v2, Lnet/hockeyapp/android/c/h;->b:Lnet/hockeyapp/android/c/d;
    :try_end_1e2
    .catch Lorg/json/JSONException; {:try_start_1e0 .. :try_end_1e2} :catch_21e

    .line 178
    :try_start_1e2
    const-string v3, "status"

    invoke-virtual {v7, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v3

    .line 11051
    iput-object v3, v2, Lnet/hockeyapp/android/c/h;->a:Ljava/lang/String;
    :try_end_1ee
    .catch Lorg/json/JSONException; {:try_start_1e2 .. :try_end_1ee} :catch_219

    .line 185
    :goto_1ee
    :try_start_1ee
    const-string v3, "token"

    invoke-virtual {v7, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v3

    .line 11067
    iput-object v3, v2, Lnet/hockeyapp/android/c/h;->c:Ljava/lang/String;
    :try_end_1fa
    .catch Lorg/json/JSONException; {:try_start_1ee .. :try_end_1fa} :catch_220

    .line 194
    :goto_1fa
    return-object v2

    .line 154
    :catch_1fb
    move-exception v2

    :try_start_1fc
    invoke-virtual {v2}, Lorg/json/JSONException;->printStackTrace()V
    :try_end_1ff
    .catch Lorg/json/JSONException; {:try_start_1fc .. :try_end_1ff} :catch_200

    goto :goto_1bb

    .line 190
    :catch_200
    move-exception v2

    move-object/from16 v32, v2

    move-object v2, v3

    move-object/from16 v3, v32

    :goto_206
    invoke-virtual {v3}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_1fa

    .line 160
    :catch_20a
    move-exception v2

    :try_start_20b
    invoke-virtual {v2}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_1c7

    .line 166
    :catch_20f
    move-exception v2

    invoke-virtual {v2}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_1cf

    .line 172
    :catch_214
    move-exception v2

    invoke-virtual {v2}, Lorg/json/JSONException;->printStackTrace()V
    :try_end_218
    .catch Lorg/json/JSONException; {:try_start_20b .. :try_end_218} :catch_200

    goto :goto_1db

    .line 180
    :catch_219
    move-exception v3

    :try_start_21a
    invoke-virtual {v3}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_1ee

    .line 190
    :catch_21e
    move-exception v3

    goto :goto_206

    .line 187
    :catch_220
    move-exception v3

    invoke-virtual {v3}, Lorg/json/JSONException;->printStackTrace()V
    :try_end_224
    .catch Lorg/json/JSONException; {:try_start_21a .. :try_end_224} :catch_21e

    goto :goto_1fa

    :cond_225
    move-object v2, v3

    goto :goto_1fa
.end method

.method private static a()Lnet/hockeyapp/android/e/i;
    .registers 1

    .prologue
    .line 64
    sget-object v0, Lnet/hockeyapp/android/e/k;->a:Lnet/hockeyapp/android/e/i;

    return-object v0
.end method
