.class public final Lnet/hockeyapp/android/e/y;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Ljava/lang/String; = "99.0"


# instance fields
.field private b:Ljava/util/ArrayList;

.field private c:Lorg/json/JSONObject;

.field private d:Lnet/hockeyapp/android/ax;

.field private e:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lnet/hockeyapp/android/ax;)V
    .registers 6

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    iput-object p3, p0, Lnet/hockeyapp/android/e/y;->d:Lnet/hockeyapp/android/ax;

    .line 62
    invoke-direct {p0, p1, p2}, Lnet/hockeyapp/android/e/y;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 1094
    iget-object v0, p0, Lnet/hockeyapp/android/e/y;->b:Ljava/util/ArrayList;

    new-instance v1, Lnet/hockeyapp/android/e/z;

    invoke-direct {v1, p0}, Lnet/hockeyapp/android/e/z;-><init>(Lnet/hockeyapp/android/e/y;)V

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 64
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)I
    .registers 9

    .prologue
    const/4 v1, 0x1

    const/4 v0, -0x1

    const/4 v2, 0x0

    .line 271
    if-eqz p0, :cond_7

    if-nez p1, :cond_9

    :cond_7
    move v0, v2

    .line 309
    :cond_8
    :goto_8
    return v0

    .line 277
    :cond_9
    :try_start_9
    new-instance v3, Ljava/util/Scanner;

    const-string v4, "\\-.*"

    const-string v5, ""

    invoke-virtual {p0, v4, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/util/Scanner;-><init>(Ljava/lang/String;)V

    .line 278
    new-instance v4, Ljava/util/Scanner;

    const-string v5, "\\-.*"

    const-string v6, ""

    invoke-virtual {p1, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/util/Scanner;-><init>(Ljava/lang/String;)V

    .line 279
    const-string v5, "\\."

    invoke-virtual {v3, v5}, Ljava/util/Scanner;->useDelimiter(Ljava/lang/String;)Ljava/util/Scanner;

    .line 280
    const-string v5, "\\."

    invoke-virtual {v4, v5}, Ljava/util/Scanner;->useDelimiter(Ljava/lang/String;)Ljava/util/Scanner;

    .line 283
    :cond_2d
    invoke-virtual {v3}, Ljava/util/Scanner;->hasNextInt()Z

    move-result v5

    if-eqz v5, :cond_47

    invoke-virtual {v4}, Ljava/util/Scanner;->hasNextInt()Z

    move-result v5

    if-eqz v5, :cond_47

    .line 284
    invoke-virtual {v3}, Ljava/util/Scanner;->nextInt()I

    move-result v5

    .line 285
    invoke-virtual {v4}, Ljava/util/Scanner;->nextInt()I

    move-result v6

    .line 286
    if-lt v5, v6, :cond_8

    .line 289
    if-le v5, v6, :cond_2d

    move v0, v1

    .line 290
    goto :goto_8

    .line 295
    :cond_47
    invoke-virtual {v3}, Ljava/util/Scanner;->hasNextInt()Z

    move-result v3

    if-eqz v3, :cond_4f

    move v0, v1

    .line 296
    goto :goto_8

    .line 299
    :cond_4f
    invoke-virtual {v4}, Ljava/util/Scanner;->hasNextInt()Z
    :try_end_52
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_52} :catch_57

    move-result v1

    if-nez v1, :cond_8

    move v0, v2

    .line 304
    goto :goto_8

    .line 309
    :catch_57
    move-exception v0

    move v0, v2

    goto :goto_8
.end method

.method private static a(Lorg/json/JSONObject;Ljava/lang/String;)J
    .registers 4

    .prologue
    .line 143
    :try_start_0
    invoke-virtual {p0, p1}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_3} :catch_5

    move-result-wide v0

    .line 146
    :goto_4
    return-wide v0

    :catch_5
    move-exception v0

    const-wide/16 v0, 0x0

    goto :goto_4
.end method

.method private a(ILorg/json/JSONObject;)Ljava/lang/String;
    .registers 9

    .prologue
    .line 200
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 202
    iget-object v1, p0, Lnet/hockeyapp/android/e/y;->c:Lorg/json/JSONObject;

    invoke-static {v1}, Lnet/hockeyapp/android/e/y;->c(Lorg/json/JSONObject;)I

    move-result v1

    .line 203
    invoke-static {p2}, Lnet/hockeyapp/android/e/y;->c(Lorg/json/JSONObject;)I

    move-result v2

    .line 204
    invoke-static {p2}, Lnet/hockeyapp/android/e/y;->d(Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object v3

    .line 206
    const-string v4, "<div style=\'padding: 20px 10px 10px;\'><strong>"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 207
    if-nez p1, :cond_29

    .line 208
    const-string v1, "Newest version:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 217
    :cond_1f
    :goto_1f
    const-string v1, "</strong></div>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 219
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 211
    :cond_29
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Version "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "): "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 212
    if-eq v2, v1, :cond_1f

    iget v1, p0, Lnet/hockeyapp/android/e/y;->e:I

    if-ne v2, v1, :cond_1f

    .line 213
    const/4 v1, -0x1

    iput v1, p0, Lnet/hockeyapp/android/e/y;->e:I

    .line 214
    const-string v1, "[INSTALLED]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1f
.end method

.method private static a(Ljava/lang/String;)Ljava/lang/String;
    .registers 2

    .prologue
    .line 353
    if-eqz p0, :cond_a

    const-string v0, "L"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 354
    :cond_a
    const-string p0, "5.0"

    .line 360
    :cond_c
    :goto_c
    return-object p0

    .line 355
    :cond_d
    const-string v0, "M"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 356
    const-string p0, "6.0"

    goto :goto_c

    .line 357
    :cond_18
    const-string v0, "^[a-zA-Z]+"

    invoke-static {v0, p0}, Ljava/util/regex/Pattern;->matches(Ljava/lang/String;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 358
    const-string p0, "99.0"

    goto :goto_c
.end method

.method private static a(Lorg/json/JSONObject;)Ljava/lang/String;
    .registers 5

    .prologue
    .line 179
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 181
    invoke-static {p0}, Lnet/hockeyapp/android/e/y;->b(Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object v1

    .line 182
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_27

    .line 183
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "<a href=\'restore:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'  style=\'background: #c8c8c8; color: #000; display: block; float: right; padding: 7px; margin: 0px 10px 10px; text-decoration: none;\'>Restore</a>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 186
    :cond_27
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 4

    .prologue
    .line 134
    :try_start_0
    invoke-virtual {p0, p1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_3} :catch_5

    move-result-object p2

    .line 137
    :goto_4
    return-object p2

    :catch_5
    move-exception v0

    goto :goto_4
.end method

.method private a(Landroid/content/Context;Ljava/lang/String;)V
    .registers 13

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 67
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    iput-object v0, p0, Lnet/hockeyapp/android/e/y;->c:Lorg/json/JSONObject;

    .line 68
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lnet/hockeyapp/android/e/y;->b:Ljava/util/ArrayList;

    .line 69
    iget-object v0, p0, Lnet/hockeyapp/android/e/y;->d:Lnet/hockeyapp/android/ax;

    invoke-interface {v0}, Lnet/hockeyapp/android/ax;->getCurrentVersionCode()I

    move-result v0

    iput v0, p0, Lnet/hockeyapp/android/e/y;->e:I

    .line 72
    :try_start_18
    new-instance v6, Lorg/json/JSONArray;

    invoke-direct {v6, p2}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 74
    iget-object v0, p0, Lnet/hockeyapp/android/e/y;->d:Lnet/hockeyapp/android/ax;

    invoke-interface {v0}, Lnet/hockeyapp/android/ax;->getCurrentVersionCode()I

    move-result v0

    move v5, v3

    .line 75
    :goto_24
    invoke-virtual {v6}, Lorg/json/JSONArray;->length()I

    move-result v1

    if-ge v5, v1, :cond_66

    .line 76
    invoke-virtual {v6, v5}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v7

    .line 77
    const-string v1, "version"

    invoke-virtual {v7, v1}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v1

    if-le v1, v0, :cond_61

    move v4, v2

    .line 78
    :goto_37
    const-string v1, "version"

    invoke-virtual {v7, v1}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v1

    if-ne v1, v0, :cond_63

    const-string v1, "timestamp"

    invoke-virtual {v7, v1}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v8

    invoke-static {p1, v8, v9}, Lnet/hockeyapp/android/e/y;->a(Landroid/content/Context;J)Z

    move-result v1

    if-eqz v1, :cond_63

    move v1, v2

    .line 80
    :goto_4c
    if-nez v4, :cond_50

    if-eqz v1, :cond_58

    .line 81
    :cond_50
    iput-object v7, p0, Lnet/hockeyapp/android/e/y;->c:Lorg/json/JSONObject;

    .line 82
    const-string v0, "version"

    invoke-virtual {v7, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 84
    :cond_58
    iget-object v1, p0, Lnet/hockeyapp/android/e/y;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_5d
    .catch Lorg/json/JSONException; {:try_start_18 .. :try_end_5d} :catch_67
    .catch Ljava/lang/NullPointerException; {:try_start_18 .. :try_end_5d} :catch_65

    .line 75
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    goto :goto_24

    :cond_61
    move v4, v3

    .line 77
    goto :goto_37

    :cond_63
    move v1, v3

    .line 78
    goto :goto_4c

    .line 91
    :catch_65
    move-exception v0

    :cond_66
    :goto_66
    return-void

    .line 90
    :catch_67
    move-exception v0

    goto :goto_66
.end method

.method public static a(Landroid/content/Context;J)Z
    .registers 10

    .prologue
    const/4 v0, 0x0

    .line 322
    if-nez p0, :cond_4

    .line 339
    :cond_3
    :goto_3
    return v0

    .line 327
    :cond_4
    :try_start_4
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 328
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    .line 329
    iget-object v1, v1, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    .line 333
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->lastModified()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4
    :try_end_1f
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_4 .. :try_end_1f} :catch_28

    const-wide/16 v4, 0x708

    add-long/2addr v2, v4

    .line 335
    cmp-long v1, p1, v2

    if-lez v1, :cond_3

    const/4 v0, 0x1

    goto :goto_3

    .line 338
    :catch_28
    move-exception v1

    invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_3
.end method

.method private static b(Lorg/json/JSONObject;)Ljava/lang/String;
    .registers 3

    .prologue
    .line 190
    const-string v0, ""

    .line 192
    :try_start_2
    const-string v1, "id"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_7
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_7} :catch_9

    move-result-object v0

    .line 196
    :goto_8
    return-object v0

    :catch_9
    move-exception v1

    goto :goto_8
.end method

.method private static c(Lorg/json/JSONObject;)I
    .registers 3

    .prologue
    .line 223
    const/4 v0, 0x0

    .line 225
    :try_start_1
    const-string v1, "version"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
    :try_end_6
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_6} :catch_8

    move-result v0

    .line 229
    :goto_7
    return v0

    :catch_8
    move-exception v1

    goto :goto_7
.end method

.method private static d(Lorg/json/JSONObject;)Ljava/lang/String;
    .registers 3

    .prologue
    .line 233
    const-string v0, ""

    .line 235
    :try_start_2
    const-string v1, "shortversion"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_7
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_7} :catch_9

    move-result-object v0

    .line 239
    :goto_8
    return-object v0

    :catch_9
    move-exception v1

    goto :goto_8
.end method

.method private static e(Lorg/json/JSONObject;)Ljava/lang/String;
    .registers 4

    .prologue
    .line 243
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 245
    const-string v1, "notes"

    const-string v2, ""

    invoke-static {p0, v1, v2}, Lnet/hockeyapp/android/e/y;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 246
    const-string v2, "<div style=\'padding: 0px 10px;\'>"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 247
    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_2b

    .line 248
    const-string v1, "<em>No information.</em>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 253
    :goto_21
    const-string v1, "</div>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 255
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 251
    :cond_2b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_21
.end method

.method private e()V
    .registers 3

    .prologue
    .line 94
    iget-object v0, p0, Lnet/hockeyapp/android/e/y;->b:Ljava/util/ArrayList;

    new-instance v1, Lnet/hockeyapp/android/e/z;

    invoke-direct {v1, p0}, Lnet/hockeyapp/android/e/z;-><init>(Lnet/hockeyapp/android/e/y;)V

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 109
    return-void
.end method

.method private static f()Ljava/lang/Object;
    .registers 1

    .prologue
    .line 175
    const-string v0, "<hr style=\'border-top: 1px solid #c8c8c8; border-bottom: 0px; margin: 40px 10px 0px 10px;\' />"

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .registers 5

    .prologue
    .line 112
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lnet/hockeyapp/android/e/y;->c:Lorg/json/JSONObject;

    const-string v2, "shortversion"

    const-string v3, ""

    invoke-static {v1, v2, v3}, Lnet/hockeyapp/android/e/y;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lnet/hockeyapp/android/e/y;->c:Lorg/json/JSONObject;

    const-string v2, "version"

    const-string v3, ""

    invoke-static {v1, v2, v3}, Lnet/hockeyapp/android/e/y;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .registers 7

    .prologue
    .line 116
    iget-object v0, p0, Lnet/hockeyapp/android/e/y;->c:Lorg/json/JSONObject;

    const-string v1, "timestamp"

    invoke-static {v0, v1}, Lnet/hockeyapp/android/e/y;->a(Lorg/json/JSONObject;Ljava/lang/String;)J

    move-result-wide v0

    .line 117
    new-instance v2, Ljava/util/Date;

    const-wide/16 v4, 0x3e8

    mul-long/2addr v0, v4

    invoke-direct {v2, v0, v1}, Ljava/util/Date;-><init>(J)V

    .line 118
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "dd.MM.yyyy"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 119
    invoke-virtual {v0, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c()J
    .registers 5

    .prologue
    .line 123
    iget-object v0, p0, Lnet/hockeyapp/android/e/y;->c:Lorg/json/JSONObject;

    const-string v1, "external"

    const-string v2, "false"

    invoke-static {v0, v1, v2}, Lnet/hockeyapp/android/e/y;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    .line 124
    iget-object v0, p0, Lnet/hockeyapp/android/e/y;->c:Lorg/json/JSONObject;

    const-string v1, "appsize"

    invoke-static {v0, v1}, Lnet/hockeyapp/android/e/y;->a(Lorg/json/JSONObject;Ljava/lang/String;)J

    move-result-wide v0

    .line 129
    if-eqz v2, :cond_24

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-nez v2, :cond_24

    const-wide/16 v0, -0x1

    :cond_24
    return-wide v0
.end method

.method public final d()Ljava/lang/String;
    .registers 11

    .prologue
    .line 151
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 152
    const-string v0, "<html>"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 153
    const-string v0, "<body style=\'padding: 0px 0px 20px 0px\'>"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 155
    const/4 v0, 0x0

    .line 156
    iget-object v1, p0, Lnet/hockeyapp/android/e/y;->b:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_17
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_bb

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/json/JSONObject;

    .line 157
    if-lez v1, :cond_2a

    .line 1175
    const-string v4, "<hr style=\'border-top: 1px solid #c8c8c8; border-bottom: 0px; margin: 40px 10px 0px 10px;\' />"

    .line 158
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1200
    :cond_2a
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 1202
    iget-object v5, p0, Lnet/hockeyapp/android/e/y;->c:Lorg/json/JSONObject;

    invoke-static {v5}, Lnet/hockeyapp/android/e/y;->c(Lorg/json/JSONObject;)I

    move-result v5

    .line 1203
    invoke-static {v0}, Lnet/hockeyapp/android/e/y;->c(Lorg/json/JSONObject;)I

    move-result v6

    .line 1204
    invoke-static {v0}, Lnet/hockeyapp/android/e/y;->d(Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object v7

    .line 1206
    const-string v8, "<div style=\'padding: 20px 10px 10px;\'><strong>"

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1207
    if-nez v1, :cond_86

    .line 1208
    const-string v5, "Newest version:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1217
    :cond_49
    :goto_49
    const-string v5, "</strong></div>"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1219
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 163
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1243
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 1245
    const-string v5, "notes"

    const-string v6, ""

    invoke-static {v0, v5, v6}, Lnet/hockeyapp/android/e/y;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1246
    const-string v5, "<div style=\'padding: 0px 10px;\'>"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1247
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    if-nez v5, :cond_b7

    .line 1248
    const-string v0, "<em>No information.</em>"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1253
    :goto_76
    const-string v0, "</div>"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1255
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 164
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 165
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 166
    goto :goto_17

    .line 1211
    :cond_86
    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Version "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " ("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "): "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1212
    if-eq v6, v5, :cond_49

    iget v5, p0, Lnet/hockeyapp/android/e/y;->e:I

    if-ne v6, v5, :cond_49

    .line 1213
    const/4 v5, -0x1

    iput v5, p0, Lnet/hockeyapp/android/e/y;->e:I

    .line 1214
    const-string v5, "[INSTALLED]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_49

    .line 1251
    :cond_b7
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_76

    .line 168
    :cond_bb
    const-string v0, "</body>"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 169
    const-string v0, "</html>"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 171
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
