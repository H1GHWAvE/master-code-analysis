.class public final Lnet/hockeyapp/android/e/l;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Ljava/lang/String; = "UTF-8"

.field private static final c:I = 0x1d4c0


# instance fields
.field public b:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Lnet/hockeyapp/android/e/q;

.field private g:I

.field private final h:Ljava/util/Map;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .registers 3

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    const v0, 0x1d4c0

    iput v0, p0, Lnet/hockeyapp/android/e/l;->g:I

    .line 66
    iput-object p1, p0, Lnet/hockeyapp/android/e/l;->d:Ljava/lang/String;

    .line 67
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lnet/hockeyapp/android/e/l;->h:Ljava/util/Map;

    .line 68
    return-void
.end method

.method private static a(Ljava/util/Map;Ljava/lang/String;)Ljava/lang/String;
    .registers 7

    .prologue
    .line 187
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 188
    invoke-interface {p0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_d
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_42

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 189
    invoke-interface {p0, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 190
    invoke-static {v0, p1}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 191
    invoke-static {v1, p1}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 192
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "="

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_d

    .line 194
    :cond_42
    const-string v0, "&"

    invoke-static {v0, v2}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(I)Lnet/hockeyapp/android/e/l;
    .registers 4

    .prologue
    .line 120
    if-gez p1, :cond_a

    .line 121
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Timeout has to be positive."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 123
    :cond_a
    iput p1, p0, Lnet/hockeyapp/android/e/l;->g:I

    .line 124
    return-object p0
.end method

.method private a(Ljava/lang/String;)Lnet/hockeyapp/android/e/l;
    .registers 2

    .prologue
    .line 71
    iput-object p1, p0, Lnet/hockeyapp/android/e/l;->b:Ljava/lang/String;

    .line 72
    return-object p0
.end method

.method private b(Ljava/lang/String;)Lnet/hockeyapp/android/e/l;
    .registers 2

    .prologue
    .line 76
    iput-object p1, p0, Lnet/hockeyapp/android/e/l;->e:Ljava/lang/String;

    .line 77
    return-object p0
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;)Lnet/hockeyapp/android/e/l;
    .registers 6

    .prologue
    .line 133
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Basic "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 134
    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    .line 133
    invoke-static {v1}, Lnet/hockeyapp/android/e/b;->a([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 136
    const-string v1, "Authorization"

    invoke-virtual {p0, v1, v0}, Lnet/hockeyapp/android/e/l;->a(Ljava/lang/String;Ljava/lang/String;)Lnet/hockeyapp/android/e/l;

    .line 137
    return-object p0
.end method


# virtual methods
.method public final a()Ljava/net/HttpURLConnection;
    .registers 6

    .prologue
    .line 143
    :try_start_0
    new-instance v0, Ljava/net/URL;

    iget-object v1, p0, Lnet/hockeyapp/android/e/l;->d:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 144
    invoke-virtual {v0}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljava/net/HttpURLConnection;

    .line 146
    iget v1, p0, Lnet/hockeyapp/android/e/l;->g:I

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 147
    iget v1, p0, Lnet/hockeyapp/android/e/l;->g:I

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    .line 149
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x9

    if-gt v1, v2, :cond_24

    .line 150
    const-string v1, "Connection"

    const-string v2, "close"

    invoke-virtual {v0, v1, v2}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    :cond_24
    iget-object v1, p0, Lnet/hockeyapp/android/e/l;->b:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_51

    .line 154
    iget-object v1, p0, Lnet/hockeyapp/android/e/l;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 155
    iget-object v1, p0, Lnet/hockeyapp/android/e/l;->e:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4d

    iget-object v1, p0, Lnet/hockeyapp/android/e/l;->b:Ljava/lang/String;

    const-string v2, "POST"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_4d

    iget-object v1, p0, Lnet/hockeyapp/android/e/l;->b:Ljava/lang/String;

    const-string v2, "PUT"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_51

    .line 156
    :cond_4d
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    .line 160
    :cond_51
    iget-object v1, p0, Lnet/hockeyapp/android/e/l;->h:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_5b
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_7a

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 161
    iget-object v2, p0, Lnet/hockeyapp/android/e/l;->h:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_72
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_72} :catch_73

    goto :goto_5b

    .line 180
    :catch_73
    move-exception v0

    .line 181
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 164
    :cond_7a
    :try_start_7a
    iget-object v1, p0, Lnet/hockeyapp/android/e/l;->e:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_9d

    .line 165
    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v1

    .line 166
    new-instance v2, Ljava/io/BufferedWriter;

    new-instance v3, Ljava/io/OutputStreamWriter;

    const-string v4, "UTF-8"

    invoke-direct {v3, v1, v4}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;Ljava/lang/String;)V

    invoke-direct {v2, v3}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V

    .line 167
    iget-object v1, p0, Lnet/hockeyapp/android/e/l;->e:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 168
    invoke-virtual {v2}, Ljava/io/BufferedWriter;->flush()V

    .line 169
    invoke-virtual {v2}, Ljava/io/BufferedWriter;->close()V

    .line 172
    :cond_9d
    iget-object v1, p0, Lnet/hockeyapp/android/e/l;->f:Lnet/hockeyapp/android/e/q;

    if-eqz v1, :cond_d4

    .line 173
    const-string v1, "Content-Length"

    iget-object v2, p0, Lnet/hockeyapp/android/e/l;->f:Lnet/hockeyapp/android/e/q;

    .line 3148
    invoke-virtual {v2}, Lnet/hockeyapp/android/e/q;->b()V

    .line 3149
    iget-object v2, v2, Lnet/hockeyapp/android/e/q;->a:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v2

    array-length v2, v2

    int-to-long v2, v2

    .line 173
    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 174
    new-instance v1, Ljava/io/BufferedOutputStream;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 175
    iget-object v2, p0, Lnet/hockeyapp/android/e/l;->f:Lnet/hockeyapp/android/e/q;

    .line 3157
    invoke-virtual {v2}, Lnet/hockeyapp/android/e/q;->b()V

    .line 3158
    iget-object v2, v2, Lnet/hockeyapp/android/e/q;->a:Ljava/io/ByteArrayOutputStream;

    .line 175
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/BufferedOutputStream;->write([B)V

    .line 176
    invoke-virtual {v1}, Ljava/io/BufferedOutputStream;->flush()V

    .line 177
    invoke-virtual {v1}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_d4
    .catch Ljava/io/IOException; {:try_start_7a .. :try_end_d4} :catch_73

    .line 183
    :cond_d4
    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)Lnet/hockeyapp/android/e/l;
    .registers 4

    .prologue
    .line 128
    iget-object v0, p0, Lnet/hockeyapp/android/e/l;->h:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 129
    return-object p0
.end method

.method public final a(Ljava/util/Map;)Lnet/hockeyapp/android/e/l;
    .registers 8

    .prologue
    .line 82
    :try_start_0
    const-string v2, "UTF-8"

    .line 1187
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1188
    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_f
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4b

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1189
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1190
    invoke-static {v0, v2}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1191
    invoke-static {v1, v2}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1192
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, "="

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_43
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_43} :catch_44

    goto :goto_f

    .line 85
    :catch_44
    move-exception v0

    .line 86
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 1194
    :cond_4b
    :try_start_4b
    const-string v0, "&"

    invoke-static {v0, v3}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    .line 83
    const-string v1, "Content-Type"

    const-string v2, "application/x-www-form-urlencoded"

    invoke-virtual {p0, v1, v2}, Lnet/hockeyapp/android/e/l;->a(Ljava/lang/String;Ljava/lang/String;)Lnet/hockeyapp/android/e/l;

    .line 2076
    iput-object v0, p0, Lnet/hockeyapp/android/e/l;->e:Ljava/lang/String;
    :try_end_5a
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_4b .. :try_end_5a} :catch_44

    .line 88
    return-object p0
.end method

.method public final a(Ljava/util/Map;Landroid/content/Context;Ljava/util/List;)Lnet/hockeyapp/android/e/l;
    .registers 12

    .prologue
    const/4 v2, 0x0

    .line 93
    :try_start_1
    new-instance v0, Lnet/hockeyapp/android/e/q;

    invoke-direct {v0}, Lnet/hockeyapp/android/e/q;-><init>()V

    iput-object v0, p0, Lnet/hockeyapp/android/e/l;->f:Lnet/hockeyapp/android/e/q;

    .line 94
    iget-object v0, p0, Lnet/hockeyapp/android/e/l;->f:Lnet/hockeyapp/android/e/q;

    invoke-virtual {v0}, Lnet/hockeyapp/android/e/q;->a()V

    .line 96
    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_15
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_91

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 97
    iget-object v4, p0, Lnet/hockeyapp/android/e/l;->f:Lnet/hockeyapp/android/e/q;

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2097
    invoke-virtual {v4}, Lnet/hockeyapp/android/e/q;->a()V

    .line 2099
    iget-object v5, v4, Lnet/hockeyapp/android/e/q;->a:Ljava/io/ByteArrayOutputStream;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Content-Disposition: form-data; name=\""

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v6, "\"\r\n"

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 2100
    iget-object v0, v4, Lnet/hockeyapp/android/e/q;->a:Ljava/io/ByteArrayOutputStream;

    const-string v5, "Content-Type: text/plain; charset=UTF-8\r\n"

    invoke-virtual {v5}, Ljava/lang/String;->getBytes()[B

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 2101
    iget-object v0, v4, Lnet/hockeyapp/android/e/q;->a:Ljava/io/ByteArrayOutputStream;

    const-string v5, "Content-Transfer-Encoding: 8bit\r\n\r\n"

    invoke-virtual {v5}, Ljava/lang/String;->getBytes()[B

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 2102
    iget-object v0, v4, Lnet/hockeyapp/android/e/q;->a:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 2103
    iget-object v0, v4, Lnet/hockeyapp/android/e/q;->a:Ljava/io/ByteArrayOutputStream;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v5, "\r\n--"

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, v4, Lnet/hockeyapp/android/e/q;->b:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "\r\n"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/ByteArrayOutputStream;->write([B)V
    :try_end_89
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_89} :catch_8a

    goto :goto_15

    .line 112
    :catch_8a
    move-exception v0

    .line 113
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :cond_91
    move v3, v2

    .line 100
    :goto_92
    :try_start_92
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_cd

    .line 101
    invoke-interface {p3, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 102
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ne v3, v1, :cond_cb

    const/4 v1, 0x1

    .line 104
    :goto_a7
    invoke-virtual {p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-virtual {v4, v0}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v4

    .line 105
    invoke-virtual {v0}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v0

    .line 106
    iget-object v5, p0, Lnet/hockeyapp/android/e/l;->f:Lnet/hockeyapp/android/e/q;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "attachment"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6, v0, v4, v1}, Lnet/hockeyapp/android/e/q;->a(Ljava/lang/String;Ljava/lang/String;Ljava/io/InputStream;Z)V

    .line 100
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_92

    :cond_cb
    move v1, v2

    .line 102
    goto :goto_a7

    .line 108
    :cond_cd
    iget-object v0, p0, Lnet/hockeyapp/android/e/l;->f:Lnet/hockeyapp/android/e/q;

    invoke-virtual {v0}, Lnet/hockeyapp/android/e/q;->b()V

    .line 110
    const-string v0, "Content-Type"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "multipart/form-data; boundary="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lnet/hockeyapp/android/e/l;->f:Lnet/hockeyapp/android/e/q;

    .line 3073
    iget-object v2, v2, Lnet/hockeyapp/android/e/q;->b:Ljava/lang/String;

    .line 110
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lnet/hockeyapp/android/e/l;->a(Ljava/lang/String;Ljava/lang/String;)Lnet/hockeyapp/android/e/l;
    :try_end_ea
    .catch Ljava/io/IOException; {:try_start_92 .. :try_end_ea} :catch_8a

    .line 116
    return-object p0
.end method
