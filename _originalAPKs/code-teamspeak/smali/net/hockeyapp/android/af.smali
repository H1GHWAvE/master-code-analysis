.class public Lnet/hockeyapp/android/af;
.super Landroid/app/Activity;
.source "SourceFile"


# static fields
.field private static final a:I = 0x1

.field private static final b:I = 0x2

.field private static final c:I = 0x3


# instance fields
.field private d:Lnet/hockeyapp/android/f/k;

.field private e:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 30
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method private a(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;
    .registers 9

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 197
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "_data"

    aput-object v0, v2, v1

    .line 200
    invoke-virtual {p0}, Lnet/hockeyapp/android/af;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    move-object v1, p1

    move-object v4, v3

    move-object v5, v3

    .line 201
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 203
    if-eqz v1, :cond_28

    .line 205
    :try_start_1a
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_25

    .line 206
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_24
    .catchall {:try_start_1a .. :try_end_24} :catchall_2b

    move-result-object v3

    .line 210
    :cond_25
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 213
    :cond_28
    if-nez v3, :cond_30

    :goto_2a
    return-object p2

    .line 210
    :catchall_2b
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    .line 213
    :cond_30
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object p2

    goto :goto_2a
.end method

.method private a()V
    .registers 8

    .prologue
    const/4 v6, -0x1

    const/4 v1, 0x1

    .line 153
    new-instance v3, Ljava/io/File;

    invoke-virtual {p0}, Lnet/hockeyapp/android/af;->getCacheDir()Ljava/io/File;

    move-result-object v0

    const-string v2, "HockeyApp"

    invoke-direct {v3, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 154
    invoke-virtual {v3}, Ljava/io/File;->mkdir()Z

    .line 156
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lnet/hockeyapp/android/af;->e:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ".jpg"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 157
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v3, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    move-object v2, v0

    move v0, v1

    .line 160
    :goto_2c
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_59

    .line 161
    new-instance v2, Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lnet/hockeyapp/android/af;->e:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ".jpg"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 162
    add-int/lit8 v0, v0, 0x1

    goto :goto_2c

    .line 165
    :cond_59
    iget-object v0, p0, Lnet/hockeyapp/android/af;->d:Lnet/hockeyapp/android/f/k;

    invoke-virtual {v0, v1}, Lnet/hockeyapp/android/f/k;->setDrawingCacheEnabled(Z)V

    .line 166
    iget-object v0, p0, Lnet/hockeyapp/android/af;->d:Lnet/hockeyapp/android/f/k;

    invoke-virtual {v0}, Lnet/hockeyapp/android/f/k;->getDrawingCache()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 167
    new-instance v3, Lnet/hockeyapp/android/ah;

    invoke-direct {v3, p0, v0}, Lnet/hockeyapp/android/ah;-><init>(Lnet/hockeyapp/android/af;Landroid/graphics/Bitmap;)V

    new-array v0, v1, [Ljava/io/File;

    const/4 v1, 0x0

    aput-object v2, v0, v1

    .line 181
    invoke-virtual {v3, v0}, Lnet/hockeyapp/android/ah;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 183
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 184
    invoke-static {v2}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    .line 185
    const-string v2, "imageUri"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 187
    invoke-virtual {p0}, Lnet/hockeyapp/android/af;->getParent()Landroid/app/Activity;

    move-result-object v1

    if-nez v1, :cond_8c

    .line 188
    invoke-virtual {p0, v6, v0}, Lnet/hockeyapp/android/af;->setResult(ILandroid/content/Intent;)V

    .line 193
    :goto_88
    invoke-virtual {p0}, Lnet/hockeyapp/android/af;->finish()V

    .line 194
    return-void

    .line 191
    :cond_8c
    invoke-virtual {p0}, Lnet/hockeyapp/android/af;->getParent()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1, v6, v0}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    goto :goto_88
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .registers 11

    .prologue
    const/16 v8, 0x11

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v7, -0x1

    .line 41
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 44
    invoke-virtual {p0}, Lnet/hockeyapp/android/af;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 45
    const-string v1, "imageUri"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 47
    invoke-virtual {v0}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lnet/hockeyapp/android/af;->a(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lnet/hockeyapp/android/af;->e:Ljava/lang/String;

    .line 49
    invoke-virtual {p0}, Lnet/hockeyapp/android/af;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v4, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 50
    invoke-virtual {p0}, Lnet/hockeyapp/android/af;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v5, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 51
    if-le v4, v5, :cond_4e

    move v1, v2

    .line 54
    :goto_39
    invoke-virtual {p0}, Lnet/hockeyapp/android/af;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    invoke-static {v6, v0}, Lnet/hockeyapp/android/f/k;->a(Landroid/content/ContentResolver;Landroid/net/Uri;)I

    move-result v6

    .line 55
    invoke-virtual {p0, v6}, Lnet/hockeyapp/android/af;->setRequestedOrientation(I)V

    .line 57
    if-eq v1, v6, :cond_50

    .line 59
    const-string v0, "HockeyApp"

    const-string v1, "Image loading skipped because activity will be destroyed for orientation change."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 84
    :goto_4d
    return-void

    :cond_4e
    move v1, v3

    .line 51
    goto :goto_39

    .line 64
    :cond_50
    new-instance v1, Lnet/hockeyapp/android/f/k;

    invoke-direct {v1, p0, v0, v4, v5}, Lnet/hockeyapp/android/f/k;-><init>(Landroid/content/Context;Landroid/net/Uri;II)V

    iput-object v1, p0, Lnet/hockeyapp/android/af;->d:Lnet/hockeyapp/android/f/k;

    .line 66
    new-instance v0, Landroid/widget/LinearLayout;

    invoke-direct {v0, p0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 67
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v1, v7, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 68
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 69
    invoke-virtual {v0, v8}, Landroid/widget/LinearLayout;->setGravity(I)V

    .line 70
    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 72
    new-instance v1, Landroid/widget/LinearLayout;

    invoke-direct {v1, p0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 73
    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v3, v7, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 74
    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 75
    invoke-virtual {v1, v8}, Landroid/widget/LinearLayout;->setGravity(I)V

    .line 76
    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 78
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 79
    iget-object v2, p0, Lnet/hockeyapp/android/af;->d:Lnet/hockeyapp/android/f/k;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 80
    invoke-virtual {p0, v0}, Lnet/hockeyapp/android/af;->setContentView(Landroid/view/View;)V

    .line 82
    const/16 v0, 0x600

    invoke-static {v0}, Lnet/hockeyapp/android/aj;->a(I)Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x3e8

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 83
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_4d
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .registers 6

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 88
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    .line 90
    const/16 v0, 0x601

    invoke-static {v0}, Lnet/hockeyapp/android/aj;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v2, v3, v2, v0}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 91
    const/4 v0, 0x2

    const/16 v1, 0x602

    invoke-static {v1}, Lnet/hockeyapp/android/aj;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 92
    const/4 v0, 0x3

    const/16 v1, 0x603

    invoke-static {v1}, Lnet/hockeyapp/android/aj;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 94
    return v3
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .registers 6

    .prologue
    .line 123
    const/4 v0, 0x4

    if-ne p1, v0, :cond_3a

    .line 124
    iget-object v0, p0, Lnet/hockeyapp/android/af;->d:Lnet/hockeyapp/android/f/k;

    .line 2209
    iget-object v0, v0, Lnet/hockeyapp/android/f/k;->a:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->empty()Z

    move-result v0

    .line 124
    if-nez v0, :cond_3a

    .line 125
    new-instance v0, Lnet/hockeyapp/android/ag;

    invoke-direct {v0, p0}, Lnet/hockeyapp/android/ag;-><init>(Lnet/hockeyapp/android/af;)V

    .line 140
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 141
    const/16 v2, 0x604

    invoke-static {v2}, Lnet/hockeyapp/android/aj;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const/16 v2, 0x606

    .line 142
    invoke-static {v2}, Lnet/hockeyapp/android/aj;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const/16 v2, 0x605

    .line 143
    invoke-static {v2}, Lnet/hockeyapp/android/aj;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 144
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 145
    const/4 v0, 0x1

    .line 149
    :goto_39
    return v0

    :cond_3a
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_39
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .registers 9

    .prologue
    const/4 v6, -0x1

    const/4 v1, 0x1

    .line 105
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_c2

    .line 118
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    :cond_d
    :goto_d
    return v1

    .line 1153
    :pswitch_e
    new-instance v3, Ljava/io/File;

    invoke-virtual {p0}, Lnet/hockeyapp/android/af;->getCacheDir()Ljava/io/File;

    move-result-object v0

    const-string v2, "HockeyApp"

    invoke-direct {v3, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 1154
    invoke-virtual {v3}, Ljava/io/File;->mkdir()Z

    .line 1156
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lnet/hockeyapp/android/af;->e:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ".jpg"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1157
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v3, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    move-object v2, v0

    move v0, v1

    .line 1160
    :goto_38
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_65

    .line 1161
    new-instance v2, Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lnet/hockeyapp/android/af;->e:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ".jpg"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 1162
    add-int/lit8 v0, v0, 0x1

    goto :goto_38

    .line 1165
    :cond_65
    iget-object v0, p0, Lnet/hockeyapp/android/af;->d:Lnet/hockeyapp/android/f/k;

    invoke-virtual {v0, v1}, Lnet/hockeyapp/android/f/k;->setDrawingCacheEnabled(Z)V

    .line 1166
    iget-object v0, p0, Lnet/hockeyapp/android/af;->d:Lnet/hockeyapp/android/f/k;

    invoke-virtual {v0}, Lnet/hockeyapp/android/f/k;->getDrawingCache()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1167
    new-instance v3, Lnet/hockeyapp/android/ah;

    invoke-direct {v3, p0, v0}, Lnet/hockeyapp/android/ah;-><init>(Lnet/hockeyapp/android/af;Landroid/graphics/Bitmap;)V

    new-array v0, v1, [Ljava/io/File;

    const/4 v4, 0x0

    aput-object v2, v0, v4

    .line 1181
    invoke-virtual {v3, v0}, Lnet/hockeyapp/android/ah;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 1183
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1184
    invoke-static {v2}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    .line 1185
    const-string v3, "imageUri"

    invoke-virtual {v0, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1187
    invoke-virtual {p0}, Lnet/hockeyapp/android/af;->getParent()Landroid/app/Activity;

    move-result-object v2

    if-nez v2, :cond_99

    .line 1188
    invoke-virtual {p0, v6, v0}, Lnet/hockeyapp/android/af;->setResult(ILandroid/content/Intent;)V

    .line 1193
    :goto_94
    invoke-virtual {p0}, Lnet/hockeyapp/android/af;->finish()V

    goto/16 :goto_d

    .line 1191
    :cond_99
    invoke-virtual {p0}, Lnet/hockeyapp/android/af;->getParent()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2, v6, v0}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    goto :goto_94

    .line 111
    :pswitch_a1
    iget-object v0, p0, Lnet/hockeyapp/android/af;->d:Lnet/hockeyapp/android/f/k;

    .line 1202
    iget-object v2, v0, Lnet/hockeyapp/android/f/k;->a:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/Stack;->empty()Z

    move-result v2

    if-nez v2, :cond_d

    .line 1203
    iget-object v2, v0, Lnet/hockeyapp/android/f/k;->a:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    .line 1204
    invoke-virtual {v0}, Lnet/hockeyapp/android/f/k;->invalidate()V

    goto/16 :goto_d

    .line 115
    :pswitch_b5
    iget-object v0, p0, Lnet/hockeyapp/android/af;->d:Lnet/hockeyapp/android/f/k;

    .line 2197
    iget-object v2, v0, Lnet/hockeyapp/android/f/k;->a:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/Stack;->clear()V

    .line 2198
    invoke-virtual {v0}, Lnet/hockeyapp/android/f/k;->invalidate()V

    goto/16 :goto_d

    .line 105
    nop

    :pswitch_data_c2
    .packed-switch 0x1
        :pswitch_e
        :pswitch_a1
        :pswitch_b5
    .end packed-switch
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .registers 3

    .prologue
    .line 99
    invoke-super {p0, p1}, Landroid/app/Activity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    .line 100
    const/4 v0, 0x1

    return v0
.end method
