.class public final Lnet/hockeyapp/android/d/o;
.super Lnet/hockeyapp/android/d/l;
.source "SourceFile"


# instance fields
.field public h:J


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lnet/hockeyapp/android/b/a;)V
    .registers 4

    .prologue
    .line 48
    invoke-direct {p0, p1, p2, p3}, Lnet/hockeyapp/android/d/l;-><init>(Landroid/content/Context;Ljava/lang/String;Lnet/hockeyapp/android/b/a;)V

    .line 49
    return-void
.end method

.method private d()J
    .registers 3

    .prologue
    .line 81
    iget-wide v0, p0, Lnet/hockeyapp/android/d/o;->h:J

    return-wide v0
.end method


# virtual methods
.method protected final a(Ljava/lang/Long;)V
    .registers 6

    .prologue
    .line 71
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lnet/hockeyapp/android/d/o;->h:J

    .line 72
    iget-wide v0, p0, Lnet/hockeyapp/android/d/o;->h:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_14

    .line 73
    iget-object v0, p0, Lnet/hockeyapp/android/d/o;->c:Lnet/hockeyapp/android/b/a;

    invoke-virtual {v0, p0}, Lnet/hockeyapp/android/b/a;->a(Lnet/hockeyapp/android/d/l;)V

    .line 78
    :goto_13
    return-void

    .line 76
    :cond_14
    iget-object v0, p0, Lnet/hockeyapp/android/d/o;->c:Lnet/hockeyapp/android/b/a;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lnet/hockeyapp/android/b/a;->a(Ljava/lang/Boolean;)V

    goto :goto_13
.end method

.method protected final varargs a([Ljava/lang/Integer;)V
    .registers 2

    .prologue
    .line 67
    return-void
.end method

.method protected final varargs b()Ljava/lang/Long;
    .registers 3

    .prologue
    .line 54
    :try_start_0
    new-instance v0, Ljava/net/URL;

    invoke-virtual {p0}, Lnet/hockeyapp/android/d/o;->c()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 55
    const/4 v1, 0x6

    invoke-static {v0, v1}, Lnet/hockeyapp/android/d/o;->a(Ljava/net/URL;I)Ljava/net/URLConnection;

    move-result-object v0

    .line 56
    invoke-virtual {v0}, Ljava/net/URLConnection;->getContentLength()I

    move-result v0

    int-to-long v0, v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_16
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_16} :catch_18

    move-result-object v0

    .line 60
    :goto_17
    return-object v0

    .line 59
    :catch_18
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 60
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_17
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 44
    invoke-virtual {p0}, Lnet/hockeyapp/android/d/o;->b()Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .registers 2

    .prologue
    .line 44
    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p0, p1}, Lnet/hockeyapp/android/d/o;->a(Ljava/lang/Long;)V

    return-void
.end method

.method protected final bridge synthetic onProgressUpdate([Ljava/lang/Object;)V
    .registers 2

    .prologue
    .line 44
    return-void
.end method
