.class final Lnet/hockeyapp/android/x;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/media/MediaScannerConnection$MediaScannerConnectionClient;


# instance fields
.field a:Landroid/media/MediaScannerConnection;

.field private b:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;)V
    .registers 3

    .prologue
    .line 411
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 412
    const/4 v0, 0x0

    iput-object v0, p0, Lnet/hockeyapp/android/x;->a:Landroid/media/MediaScannerConnection;

    .line 413
    iput-object p1, p0, Lnet/hockeyapp/android/x;->b:Ljava/lang/String;

    .line 414
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;B)V
    .registers 3

    .prologue
    .line 405
    invoke-direct {p0, p1}, Lnet/hockeyapp/android/x;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method private a(Landroid/media/MediaScannerConnection;)V
    .registers 2

    .prologue
    .line 417
    iput-object p1, p0, Lnet/hockeyapp/android/x;->a:Landroid/media/MediaScannerConnection;

    .line 418
    return-void
.end method


# virtual methods
.method public final onMediaScannerConnected()V
    .registers 4

    .prologue
    .line 422
    iget-object v0, p0, Lnet/hockeyapp/android/x;->a:Landroid/media/MediaScannerConnection;

    if-eqz v0, :cond_c

    .line 423
    iget-object v0, p0, Lnet/hockeyapp/android/x;->a:Landroid/media/MediaScannerConnection;

    iget-object v1, p0, Lnet/hockeyapp/android/x;->b:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaScannerConnection;->scanFile(Ljava/lang/String;Ljava/lang/String;)V

    .line 425
    :cond_c
    return-void
.end method

.method public final onScanCompleted(Ljava/lang/String;Landroid/net/Uri;)V
    .registers 8

    .prologue
    .line 429
    const-string v0, "HockeyApp"

    const-string v1, "Scanned path %s -> URI = %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 430
    iget-object v0, p0, Lnet/hockeyapp/android/x;->a:Landroid/media/MediaScannerConnection;

    invoke-virtual {v0}, Landroid/media/MediaScannerConnection;->disconnect()V

    .line 431
    return-void
.end method
