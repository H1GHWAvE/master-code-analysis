.class public Lnet/hockeyapp/android/FeedbackActivity;
.super Landroid/app/Activity;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lnet/hockeyapp/android/s;


# static fields
.field private static final a:I = 0x3


# instance fields
.field private A:Ljava/util/ArrayList;

.field private B:Z

.field private C:Z

.field private D:Ljava/lang/String;

.field private final b:I

.field private final c:I

.field private final d:I

.field private final e:I

.field private f:Landroid/content/Context;

.field private g:Landroid/widget/TextView;

.field private h:Landroid/widget/EditText;

.field private i:Landroid/widget/EditText;

.field private j:Landroid/widget/EditText;

.field private k:Landroid/widget/EditText;

.field private l:Landroid/widget/Button;

.field private m:Landroid/widget/Button;

.field private n:Landroid/widget/Button;

.field private o:Landroid/widget/Button;

.field private p:Landroid/widget/ScrollView;

.field private q:Landroid/widget/LinearLayout;

.field private r:Landroid/widget/ListView;

.field private s:Lnet/hockeyapp/android/d/r;

.field private t:Landroid/os/Handler;

.field private u:Lnet/hockeyapp/android/d/q;

.field private v:Landroid/os/Handler;

.field private w:Ljava/util/List;

.field private x:Ljava/lang/String;

.field private y:Lnet/hockeyapp/android/c/c;

.field private z:Lnet/hockeyapp/android/a/a;


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 77
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 82
    const/4 v0, 0x0

    iput v0, p0, Lnet/hockeyapp/android/FeedbackActivity;->b:I

    .line 84
    const/4 v0, 0x1

    iput v0, p0, Lnet/hockeyapp/android/FeedbackActivity;->c:I

    .line 85
    const/4 v0, 0x2

    iput v0, p0, Lnet/hockeyapp/android/FeedbackActivity;->d:I

    .line 86
    const/4 v0, 0x3

    iput v0, p0, Lnet/hockeyapp/android/FeedbackActivity;->e:I

    return-void
.end method

.method static synthetic a(Lnet/hockeyapp/android/FeedbackActivity;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .registers 2

    .prologue
    .line 77
    iput-object p1, p0, Lnet/hockeyapp/android/FeedbackActivity;->A:Ljava/util/ArrayList;

    return-object p1
.end method

.method static synthetic a(Lnet/hockeyapp/android/FeedbackActivity;Lnet/hockeyapp/android/a/a;)Lnet/hockeyapp/android/a/a;
    .registers 2

    .prologue
    .line 77
    iput-object p1, p0, Lnet/hockeyapp/android/FeedbackActivity;->z:Lnet/hockeyapp/android/a/a;

    return-object p1
.end method

.method static synthetic a(Lnet/hockeyapp/android/FeedbackActivity;)Lnet/hockeyapp/android/c/c;
    .registers 2

    .prologue
    .line 77
    iget-object v0, p0, Lnet/hockeyapp/android/FeedbackActivity;->y:Lnet/hockeyapp/android/c/c;

    return-object v0
.end method

.method static synthetic a(Lnet/hockeyapp/android/FeedbackActivity;Lnet/hockeyapp/android/c/c;)Lnet/hockeyapp/android/c/c;
    .registers 2

    .prologue
    .line 77
    iput-object p1, p0, Lnet/hockeyapp/android/FeedbackActivity;->y:Lnet/hockeyapp/android/c/c;

    return-object p1
.end method

.method private a(Landroid/widget/EditText;I)V
    .registers 4

    .prologue
    .line 780
    invoke-static {p2}, Lnet/hockeyapp/android/aj;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    .line 781
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lnet/hockeyapp/android/FeedbackActivity;->a(Z)V

    .line 782
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .registers 5

    .prologue
    .line 558
    new-instance v0, Lnet/hockeyapp/android/d/q;

    iget-object v1, p0, Lnet/hockeyapp/android/FeedbackActivity;->v:Landroid/os/Handler;

    invoke-direct {v0, p0, p1, v1, p2}, Lnet/hockeyapp/android/d/q;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/os/Handler;Ljava/lang/String;)V

    iput-object v0, p0, Lnet/hockeyapp/android/FeedbackActivity;->u:Lnet/hockeyapp/android/d/q;

    .line 559
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Landroid/os/Handler;Z)V
    .registers 21

    .prologue
    .line 796
    new-instance v0, Lnet/hockeyapp/android/d/r;

    iget-object v1, p0, Lnet/hockeyapp/android/FeedbackActivity;->f:Landroid/content/Context;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move/from16 v10, p9

    invoke-direct/range {v0 .. v10}, Lnet/hockeyapp/android/d/r;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Landroid/os/Handler;Z)V

    iput-object v0, p0, Lnet/hockeyapp/android/FeedbackActivity;->s:Lnet/hockeyapp/android/d/r;

    .line 797
    iget-object v0, p0, Lnet/hockeyapp/android/FeedbackActivity;->s:Lnet/hockeyapp/android/d/r;

    invoke-static {v0}, Lnet/hockeyapp/android/e/a;->a(Landroid/os/AsyncTask;)V

    .line 798
    return-void
.end method

.method static synthetic a(Lnet/hockeyapp/android/FeedbackActivity;Ljava/lang/String;Ljava/lang/String;)V
    .registers 5

    .prologue
    .line 77
    .line 11558
    new-instance v0, Lnet/hockeyapp/android/d/q;

    iget-object v1, p0, Lnet/hockeyapp/android/FeedbackActivity;->v:Landroid/os/Handler;

    invoke-direct {v0, p0, p1, v1, p2}, Lnet/hockeyapp/android/d/q;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/os/Handler;Ljava/lang/String;)V

    iput-object v0, p0, Lnet/hockeyapp/android/FeedbackActivity;->u:Lnet/hockeyapp/android/d/q;

    .line 10806
    iget-object v0, p0, Lnet/hockeyapp/android/FeedbackActivity;->u:Lnet/hockeyapp/android/d/q;

    invoke-static {v0}, Lnet/hockeyapp/android/e/a;->a(Landroid/os/AsyncTask;)V

    .line 77
    return-void
.end method

.method static synthetic a(Lnet/hockeyapp/android/FeedbackActivity;Lnet/hockeyapp/android/c/h;)V
    .registers 3

    .prologue
    .line 77
    .line 11675
    new-instance v0, Lnet/hockeyapp/android/q;

    invoke-direct {v0, p0, p1}, Lnet/hockeyapp/android/q;-><init>(Lnet/hockeyapp/android/FeedbackActivity;Lnet/hockeyapp/android/c/h;)V

    invoke-virtual {p0, v0}, Lnet/hockeyapp/android/FeedbackActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 77
    return-void
.end method

.method private a(Lnet/hockeyapp/android/c/h;)V
    .registers 3

    .prologue
    .line 675
    new-instance v0, Lnet/hockeyapp/android/q;

    invoke-direct {v0, p0, p1}, Lnet/hockeyapp/android/q;-><init>(Lnet/hockeyapp/android/FeedbackActivity;Lnet/hockeyapp/android/c/h;)V

    invoke-virtual {p0, v0}, Lnet/hockeyapp/android/FeedbackActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 718
    return-void
.end method

.method private a(I)Z
    .registers 6

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    .line 522
    if-ne p1, v3, :cond_21

    .line 523
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 524
    const-string v2, "*/*"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 525
    const-string v2, "android.intent.action.GET_CONTENT"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 526
    const/16 v2, 0x416

    invoke-static {v2}, Lnet/hockeyapp/android/aj;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v1, v3}, Lnet/hockeyapp/android/FeedbackActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 536
    :goto_20
    return v0

    .line 529
    :cond_21
    if-ne p1, v0, :cond_40

    .line 530
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 531
    const-string v2, "image/*"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 532
    const-string v2, "android.intent.action.GET_CONTENT"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 533
    const/16 v2, 0x417

    invoke-static {v2}, Lnet/hockeyapp/android/aj;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v1, v0}, Lnet/hockeyapp/android/FeedbackActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_20

    .line 536
    :cond_40
    const/4 v0, 0x0

    goto :goto_20
.end method

.method private b()Landroid/view/ViewGroup;
    .registers 2

    .prologue
    .line 149
    new-instance v0, Lnet/hockeyapp/android/f/i;

    invoke-direct {v0, p0}, Lnet/hockeyapp/android/f/i;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;)V
    .registers 5

    .prologue
    .line 805
    .line 10558
    new-instance v0, Lnet/hockeyapp/android/d/q;

    iget-object v1, p0, Lnet/hockeyapp/android/FeedbackActivity;->v:Landroid/os/Handler;

    invoke-direct {v0, p0, p1, v1, p2}, Lnet/hockeyapp/android/d/q;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/os/Handler;Ljava/lang/String;)V

    iput-object v0, p0, Lnet/hockeyapp/android/FeedbackActivity;->u:Lnet/hockeyapp/android/d/q;

    .line 806
    iget-object v0, p0, Lnet/hockeyapp/android/FeedbackActivity;->u:Lnet/hockeyapp/android/d/q;

    invoke-static {v0}, Lnet/hockeyapp/android/e/a;->a(Landroid/os/AsyncTask;)V

    .line 807
    return-void
.end method

.method static synthetic b(Lnet/hockeyapp/android/FeedbackActivity;)V
    .registers 2

    .prologue
    .line 77
    .line 10721
    new-instance v0, Lnet/hockeyapp/android/r;

    invoke-direct {v0, p0}, Lnet/hockeyapp/android/r;-><init>(Lnet/hockeyapp/android/FeedbackActivity;)V

    invoke-virtual {p0, v0}, Lnet/hockeyapp/android/FeedbackActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 77
    return-void
.end method

.method static synthetic c(Lnet/hockeyapp/android/FeedbackActivity;)Landroid/content/Context;
    .registers 2

    .prologue
    .line 77
    iget-object v0, p0, Lnet/hockeyapp/android/FeedbackActivity;->f:Landroid/content/Context;

    return-object v0
.end method

.method private static c()V
    .registers 0

    .prologue
    .line 400
    return-void
.end method

.method private d()V
    .registers 11

    .prologue
    const/4 v9, 0x1

    const/4 v2, 0x0

    .line 541
    .line 8065
    sget-object v0, Lnet/hockeyapp/android/e/p;->a:Lnet/hockeyapp/android/e/n;

    .line 541
    invoke-virtual {v0, p0}, Lnet/hockeyapp/android/e/n;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnet/hockeyapp/android/FeedbackActivity;->D:Ljava/lang/String;

    .line 542
    iget-object v0, p0, Lnet/hockeyapp/android/FeedbackActivity;->D:Ljava/lang/String;

    if-eqz v0, :cond_12

    iget-boolean v0, p0, Lnet/hockeyapp/android/FeedbackActivity;->B:Z

    if-eqz v0, :cond_17

    .line 544
    :cond_12
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lnet/hockeyapp/android/FeedbackActivity;->b(Z)V

    .line 551
    :goto_16
    return-void

    .line 548
    :cond_17
    invoke-virtual {p0, v9}, Lnet/hockeyapp/android/FeedbackActivity;->b(Z)V

    .line 549
    iget-object v1, p0, Lnet/hockeyapp/android/FeedbackActivity;->x:Ljava/lang/String;

    iget-object v7, p0, Lnet/hockeyapp/android/FeedbackActivity;->D:Ljava/lang/String;

    iget-object v8, p0, Lnet/hockeyapp/android/FeedbackActivity;->t:Landroid/os/Handler;

    move-object v0, p0

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    invoke-direct/range {v0 .. v9}, Lnet/hockeyapp/android/FeedbackActivity;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Landroid/os/Handler;Z)V

    goto :goto_16
.end method

.method static synthetic d(Lnet/hockeyapp/android/FeedbackActivity;)Z
    .registers 2

    .prologue
    .line 77
    const/4 v0, 0x0

    iput-boolean v0, p0, Lnet/hockeyapp/android/FeedbackActivity;->B:Z

    return v0
.end method

.method static synthetic e(Lnet/hockeyapp/android/FeedbackActivity;)Ljava/util/ArrayList;
    .registers 2

    .prologue
    .line 77
    iget-object v0, p0, Lnet/hockeyapp/android/FeedbackActivity;->A:Ljava/util/ArrayList;

    return-object v0
.end method

.method private e()V
    .registers 4

    .prologue
    .line 562
    iget-object v0, p0, Lnet/hockeyapp/android/FeedbackActivity;->k:Landroid/widget/EditText;

    if-eqz v0, :cond_16

    .line 563
    const-string v0, "input_method"

    invoke-virtual {p0, v0}, Lnet/hockeyapp/android/FeedbackActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 564
    iget-object v1, p0, Lnet/hockeyapp/android/FeedbackActivity;->k:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 566
    :cond_16
    return-void
.end method

.method static synthetic f(Lnet/hockeyapp/android/FeedbackActivity;)Landroid/widget/TextView;
    .registers 2

    .prologue
    .line 77
    iget-object v0, p0, Lnet/hockeyapp/android/FeedbackActivity;->g:Landroid/widget/TextView;

    return-object v0
.end method

.method private f()V
    .registers 2

    .prologue
    .line 572
    new-instance v0, Lnet/hockeyapp/android/m;

    invoke-direct {v0, p0}, Lnet/hockeyapp/android/m;-><init>(Lnet/hockeyapp/android/FeedbackActivity;)V

    iput-object v0, p0, Lnet/hockeyapp/android/FeedbackActivity;->t:Landroid/os/Handler;

    .line 619
    return-void
.end method

.method static synthetic g(Lnet/hockeyapp/android/FeedbackActivity;)Lnet/hockeyapp/android/a/a;
    .registers 2

    .prologue
    .line 77
    iget-object v0, p0, Lnet/hockeyapp/android/FeedbackActivity;->z:Lnet/hockeyapp/android/a/a;

    return-object v0
.end method

.method private g()V
    .registers 2

    .prologue
    .line 625
    new-instance v0, Lnet/hockeyapp/android/o;

    invoke-direct {v0, p0}, Lnet/hockeyapp/android/o;-><init>(Lnet/hockeyapp/android/FeedbackActivity;)V

    iput-object v0, p0, Lnet/hockeyapp/android/FeedbackActivity;->v:Landroid/os/Handler;

    .line 668
    return-void
.end method

.method static synthetic h(Lnet/hockeyapp/android/FeedbackActivity;)Landroid/widget/ListView;
    .registers 2

    .prologue
    .line 77
    iget-object v0, p0, Lnet/hockeyapp/android/FeedbackActivity;->r:Landroid/widget/ListView;

    return-object v0
.end method

.method private h()V
    .registers 2

    .prologue
    .line 721
    new-instance v0, Lnet/hockeyapp/android/r;

    invoke-direct {v0, p0}, Lnet/hockeyapp/android/r;-><init>(Lnet/hockeyapp/android/FeedbackActivity;)V

    invoke-virtual {p0, v0}, Lnet/hockeyapp/android/FeedbackActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 734
    return-void
.end method

.method private i()V
    .registers 13

    .prologue
    const/4 v9, 0x0

    .line 740
    invoke-virtual {p0, v9}, Lnet/hockeyapp/android/FeedbackActivity;->a(Z)V

    .line 8562
    iget-object v0, p0, Lnet/hockeyapp/android/FeedbackActivity;->k:Landroid/widget/EditText;

    if-eqz v0, :cond_19

    .line 8563
    const-string v0, "input_method"

    invoke-virtual {p0, v0}, Lnet/hockeyapp/android/FeedbackActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 8564
    iget-object v1, p0, Lnet/hockeyapp/android/FeedbackActivity;->k:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v0, v1, v9}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 9065
    :cond_19
    sget-object v0, Lnet/hockeyapp/android/e/p;->a:Lnet/hockeyapp/android/e/n;

    .line 743
    iget-object v1, p0, Lnet/hockeyapp/android/FeedbackActivity;->f:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lnet/hockeyapp/android/e/n;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v7

    .line 745
    iget-object v0, p0, Lnet/hockeyapp/android/FeedbackActivity;->h:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    .line 746
    iget-object v0, p0, Lnet/hockeyapp/android/FeedbackActivity;->i:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    .line 747
    iget-object v0, p0, Lnet/hockeyapp/android/FeedbackActivity;->j:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    .line 748
    iget-object v0, p0, Lnet/hockeyapp/android/FeedbackActivity;->k:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    .line 750
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_6c

    .line 751
    iget-object v0, p0, Lnet/hockeyapp/android/FeedbackActivity;->j:Landroid/widget/EditText;

    invoke-virtual {v0, v9}, Landroid/widget/EditText;->setVisibility(I)V

    .line 752
    iget-object v0, p0, Lnet/hockeyapp/android/FeedbackActivity;->j:Landroid/widget/EditText;

    const/16 v1, 0x40e

    invoke-direct {p0, v0, v1}, Lnet/hockeyapp/android/FeedbackActivity;->a(Landroid/widget/EditText;I)V

    .line 777
    :goto_6b
    return-void

    .line 754
    :cond_6c
    invoke-static {}, Lnet/hockeyapp/android/t;->b()Lnet/hockeyapp/android/c/i;

    move-result-object v0

    sget-object v1, Lnet/hockeyapp/android/c/i;->c:Lnet/hockeyapp/android/c/i;

    if-ne v0, v1, :cond_82

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_82

    .line 755
    iget-object v0, p0, Lnet/hockeyapp/android/FeedbackActivity;->h:Landroid/widget/EditText;

    const/16 v1, 0x411

    invoke-direct {p0, v0, v1}, Lnet/hockeyapp/android/FeedbackActivity;->a(Landroid/widget/EditText;I)V

    goto :goto_6b

    .line 757
    :cond_82
    invoke-static {}, Lnet/hockeyapp/android/t;->c()Lnet/hockeyapp/android/c/i;

    move-result-object v0

    sget-object v1, Lnet/hockeyapp/android/c/i;->c:Lnet/hockeyapp/android/c/i;

    if-ne v0, v1, :cond_98

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_98

    .line 758
    iget-object v0, p0, Lnet/hockeyapp/android/FeedbackActivity;->i:Landroid/widget/EditText;

    const/16 v1, 0x412

    invoke-direct {p0, v0, v1}, Lnet/hockeyapp/android/FeedbackActivity;->a(Landroid/widget/EditText;I)V

    goto :goto_6b

    .line 760
    :cond_98
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_a6

    .line 761
    iget-object v0, p0, Lnet/hockeyapp/android/FeedbackActivity;->k:Landroid/widget/EditText;

    const/16 v1, 0x413

    invoke-direct {p0, v0, v1}, Lnet/hockeyapp/android/FeedbackActivity;->a(Landroid/widget/EditText;I)V

    goto :goto_6b

    .line 763
    :cond_a6
    invoke-static {}, Lnet/hockeyapp/android/t;->c()Lnet/hockeyapp/android/c/i;

    move-result-object v0

    sget-object v1, Lnet/hockeyapp/android/c/i;->c:Lnet/hockeyapp/android/c/i;

    if-ne v0, v1, :cond_bc

    invoke-static {v3}, Lnet/hockeyapp/android/e/w;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_bc

    .line 764
    iget-object v0, p0, Lnet/hockeyapp/android/FeedbackActivity;->i:Landroid/widget/EditText;

    const/16 v1, 0x40f

    invoke-direct {p0, v0, v1}, Lnet/hockeyapp/android/FeedbackActivity;->a(Landroid/widget/EditText;I)V

    goto :goto_6b

    .line 10065
    :cond_bc
    sget-object v0, Lnet/hockeyapp/android/e/p;->a:Lnet/hockeyapp/android/e/n;

    .line 768
    iget-object v1, p0, Lnet/hockeyapp/android/FeedbackActivity;->f:Landroid/content/Context;

    .line 10113
    if-eqz v1, :cond_e9

    .line 10114
    const-string v6, "net.hockeyapp.android.prefs_name_email"

    invoke-virtual {v1, v6, v9}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, v0, Lnet/hockeyapp/android/e/n;->a:Landroid/content/SharedPreferences;

    .line 10115
    iget-object v1, v0, Lnet/hockeyapp/android/e/n;->a:Landroid/content/SharedPreferences;

    if-eqz v1, :cond_e9

    .line 10116
    iget-object v1, v0, Lnet/hockeyapp/android/e/n;->a:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    iput-object v1, v0, Lnet/hockeyapp/android/e/n;->b:Landroid/content/SharedPreferences$Editor;

    .line 10117
    if-eqz v2, :cond_dc

    if-eqz v3, :cond_dc

    if-nez v4, :cond_ff

    .line 10118
    :cond_dc
    iget-object v1, v0, Lnet/hockeyapp/android/e/n;->b:Landroid/content/SharedPreferences$Editor;

    const-string v6, "net.hockeyapp.android.prefs_key_name_email"

    const/4 v8, 0x0

    invoke-interface {v1, v6, v8}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 10124
    :goto_e4
    iget-object v0, v0, Lnet/hockeyapp/android/e/n;->b:Landroid/content/SharedPreferences$Editor;

    invoke-static {v0}, Lnet/hockeyapp/android/e/n;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 771
    :cond_e9
    const/16 v0, 0x2011

    invoke-virtual {p0, v0}, Lnet/hockeyapp/android/FeedbackActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lnet/hockeyapp/android/f/a;

    .line 772
    invoke-virtual {v0}, Lnet/hockeyapp/android/f/a;->getAttachments()Ljava/util/ArrayList;

    move-result-object v6

    .line 775
    iget-object v1, p0, Lnet/hockeyapp/android/FeedbackActivity;->x:Ljava/lang/String;

    iget-object v8, p0, Lnet/hockeyapp/android/FeedbackActivity;->t:Landroid/os/Handler;

    move-object v0, p0

    invoke-direct/range {v0 .. v9}, Lnet/hockeyapp/android/FeedbackActivity;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Landroid/os/Handler;Z)V

    goto/16 :goto_6b

    .line 10120
    :cond_ff
    iget-object v1, v0, Lnet/hockeyapp/android/e/n;->b:Landroid/content/SharedPreferences$Editor;

    const-string v6, "net.hockeyapp.android.prefs_key_name_email"

    const-string v8, "%s|%s|%s"

    const/4 v10, 0x3

    new-array v10, v10, [Ljava/lang/Object;

    aput-object v2, v10, v9

    const/4 v11, 0x1

    aput-object v3, v10, v11

    const/4 v11, 0x2

    aput-object v4, v10, v11

    invoke-static {v8, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v1, v6, v8}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    goto :goto_e4
.end method


# virtual methods
.method public final synthetic a()Landroid/view/View;
    .registers 2

    .prologue
    .line 77
    invoke-direct {p0}, Lnet/hockeyapp/android/FeedbackActivity;->b()Landroid/view/ViewGroup;

    move-result-object v0

    return-object v0
.end method

.method public final a(Z)V
    .registers 3

    .prologue
    .line 138
    iget-object v0, p0, Lnet/hockeyapp/android/FeedbackActivity;->l:Landroid/widget/Button;

    if-eqz v0, :cond_9

    .line 139
    iget-object v0, p0, Lnet/hockeyapp/android/FeedbackActivity;->l:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 141
    :cond_9
    return-void
.end method

.method protected final b(Z)V
    .registers 10

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v1, 0x0

    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 305
    const v0, 0x20017

    invoke-virtual {p0, v0}, Lnet/hockeyapp/android/FeedbackActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    iput-object v0, p0, Lnet/hockeyapp/android/FeedbackActivity;->p:Landroid/widget/ScrollView;

    .line 306
    const v0, 0x20015

    invoke-virtual {p0, v0}, Lnet/hockeyapp/android/FeedbackActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lnet/hockeyapp/android/FeedbackActivity;->q:Landroid/widget/LinearLayout;

    .line 307
    const v0, 0x20016

    invoke-virtual {p0, v0}, Lnet/hockeyapp/android/FeedbackActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lnet/hockeyapp/android/FeedbackActivity;->r:Landroid/widget/ListView;

    .line 309
    if-eqz p1, :cond_5e

    .line 311
    iget-object v0, p0, Lnet/hockeyapp/android/FeedbackActivity;->q:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 312
    iget-object v0, p0, Lnet/hockeyapp/android/FeedbackActivity;->p:Landroid/widget/ScrollView;

    invoke-virtual {v0, v5}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 314
    const/16 v0, 0x2000

    invoke-virtual {p0, v0}, Lnet/hockeyapp/android/FeedbackActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lnet/hockeyapp/android/FeedbackActivity;->g:Landroid/widget/TextView;

    .line 316
    const v0, 0x20010

    invoke-virtual {p0, v0}, Lnet/hockeyapp/android/FeedbackActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lnet/hockeyapp/android/FeedbackActivity;->n:Landroid/widget/Button;

    .line 317
    iget-object v0, p0, Lnet/hockeyapp/android/FeedbackActivity;->n:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 319
    const v0, 0x20011

    invoke-virtual {p0, v0}, Lnet/hockeyapp/android/FeedbackActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lnet/hockeyapp/android/FeedbackActivity;->o:Landroid/widget/Button;

    .line 320
    iget-object v0, p0, Lnet/hockeyapp/android/FeedbackActivity;->o:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 393
    :goto_5d
    return-void

    .line 324
    :cond_5e
    iget-object v0, p0, Lnet/hockeyapp/android/FeedbackActivity;->q:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 325
    iget-object v0, p0, Lnet/hockeyapp/android/FeedbackActivity;->p:Landroid/widget/ScrollView;

    invoke-virtual {v0, v4}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 327
    const/16 v0, 0x2002

    invoke-virtual {p0, v0}, Lnet/hockeyapp/android/FeedbackActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lnet/hockeyapp/android/FeedbackActivity;->h:Landroid/widget/EditText;

    .line 328
    const/16 v0, 0x2004

    invoke-virtual {p0, v0}, Lnet/hockeyapp/android/FeedbackActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lnet/hockeyapp/android/FeedbackActivity;->i:Landroid/widget/EditText;

    .line 329
    const/16 v0, 0x2006

    invoke-virtual {p0, v0}, Lnet/hockeyapp/android/FeedbackActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lnet/hockeyapp/android/FeedbackActivity;->j:Landroid/widget/EditText;

    .line 330
    const/16 v0, 0x2008

    invoke-virtual {p0, v0}, Lnet/hockeyapp/android/FeedbackActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lnet/hockeyapp/android/FeedbackActivity;->k:Landroid/widget/EditText;

    .line 333
    iget-boolean v0, p0, Lnet/hockeyapp/android/FeedbackActivity;->C:Z

    if-nez v0, :cond_c8

    .line 6065
    sget-object v0, Lnet/hockeyapp/android/e/p;->a:Lnet/hockeyapp/android/e/n;

    .line 334
    iget-object v2, p0, Lnet/hockeyapp/android/FeedbackActivity;->f:Landroid/content/Context;

    .line 6136
    if-nez v2, :cond_108

    move-object v0, v1

    .line 335
    :goto_9b
    if-eqz v0, :cond_126

    .line 337
    const-string v1, "\\|"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 338
    if-eqz v0, :cond_c6

    array-length v1, v0

    if-lt v1, v7, :cond_c6

    .line 339
    iget-object v1, p0, Lnet/hockeyapp/android/FeedbackActivity;->h:Landroid/widget/EditText;

    aget-object v2, v0, v4

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 340
    iget-object v1, p0, Lnet/hockeyapp/android/FeedbackActivity;->i:Landroid/widget/EditText;

    aget-object v2, v0, v6

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 342
    array-length v1, v0

    const/4 v2, 0x3

    if-lt v1, v2, :cond_120

    .line 343
    iget-object v1, p0, Lnet/hockeyapp/android/FeedbackActivity;->j:Landroid/widget/EditText;

    aget-object v0, v0, v7

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 344
    iget-object v0, p0, Lnet/hockeyapp/android/FeedbackActivity;->k:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 359
    :cond_c6
    :goto_c6
    iput-boolean v6, p0, Lnet/hockeyapp/android/FeedbackActivity;->C:Z

    .line 363
    :cond_c8
    iget-object v0, p0, Lnet/hockeyapp/android/FeedbackActivity;->k:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 7065
    sget-object v0, Lnet/hockeyapp/android/e/p;->a:Lnet/hockeyapp/android/e/n;

    .line 366
    iget-object v1, p0, Lnet/hockeyapp/android/FeedbackActivity;->f:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lnet/hockeyapp/android/e/n;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_141

    .line 368
    iget-object v0, p0, Lnet/hockeyapp/android/FeedbackActivity;->j:Landroid/widget/EditText;

    invoke-virtual {v0, v5}, Landroid/widget/EditText;->setVisibility(I)V

    .line 376
    :goto_de
    const/16 v0, 0x2011

    invoke-virtual {p0, v0}, Lnet/hockeyapp/android/FeedbackActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 377
    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 379
    iget-object v1, p0, Lnet/hockeyapp/android/FeedbackActivity;->w:Ljava/util/List;

    if-eqz v1, :cond_147

    .line 380
    iget-object v1, p0, Lnet/hockeyapp/android/FeedbackActivity;->w:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_f3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_147

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/Uri;

    .line 381
    new-instance v3, Lnet/hockeyapp/android/f/b;

    invoke-direct {v3, p0, v0, v1}, Lnet/hockeyapp/android/f/b;-><init>(Landroid/content/Context;Landroid/view/ViewGroup;Landroid/net/Uri;)V

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_f3

    .line 6140
    :cond_108
    const-string v3, "net.hockeyapp.android.prefs_name_email"

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    iput-object v2, v0, Lnet/hockeyapp/android/e/n;->a:Landroid/content/SharedPreferences;

    .line 6141
    iget-object v2, v0, Lnet/hockeyapp/android/e/n;->a:Landroid/content/SharedPreferences;

    if-nez v2, :cond_116

    move-object v0, v1

    .line 6142
    goto :goto_9b

    .line 6145
    :cond_116
    iget-object v0, v0, Lnet/hockeyapp/android/e/n;->a:Landroid/content/SharedPreferences;

    const-string v2, "net.hockeyapp.android.prefs_key_name_email"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_9b

    .line 347
    :cond_120
    iget-object v0, p0, Lnet/hockeyapp/android/FeedbackActivity;->j:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    goto :goto_c6

    .line 353
    :cond_126
    iget-object v0, p0, Lnet/hockeyapp/android/FeedbackActivity;->h:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 354
    iget-object v0, p0, Lnet/hockeyapp/android/FeedbackActivity;->i:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 355
    iget-object v0, p0, Lnet/hockeyapp/android/FeedbackActivity;->j:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 356
    iget-object v0, p0, Lnet/hockeyapp/android/FeedbackActivity;->h:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    goto :goto_c6

    .line 372
    :cond_141
    iget-object v0, p0, Lnet/hockeyapp/android/FeedbackActivity;->j:Landroid/widget/EditText;

    invoke-virtual {v0, v4}, Landroid/widget/EditText;->setVisibility(I)V

    goto :goto_de

    .line 386
    :cond_147
    const/16 v0, 0x2010

    invoke-virtual {p0, v0}, Lnet/hockeyapp/android/FeedbackActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lnet/hockeyapp/android/FeedbackActivity;->m:Landroid/widget/Button;

    .line 387
    iget-object v0, p0, Lnet/hockeyapp/android/FeedbackActivity;->m:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 388
    iget-object v0, p0, Lnet/hockeyapp/android/FeedbackActivity;->m:Landroid/widget/Button;

    invoke-virtual {p0, v0}, Lnet/hockeyapp/android/FeedbackActivity;->registerForContextMenu(Landroid/view/View;)V

    .line 390
    const/16 v0, 0x2009

    invoke-virtual {p0, v0}, Lnet/hockeyapp/android/FeedbackActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lnet/hockeyapp/android/FeedbackActivity;->l:Landroid/widget/Button;

    .line 391
    iget-object v0, p0, Lnet/hockeyapp/android/FeedbackActivity;->l:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_5d
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .registers 7

    .prologue
    const/16 v2, 0x2011

    const/4 v1, 0x3

    .line 406
    const/4 v0, -0x1

    if-eq p2, v0, :cond_7

    .line 444
    :cond_6
    :goto_6
    return-void

    .line 410
    :cond_7
    const/4 v0, 0x2

    if-ne p1, v0, :cond_1f

    .line 412
    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    .line 414
    if-eqz v1, :cond_6

    .line 415
    invoke-virtual {p0, v2}, Lnet/hockeyapp/android/FeedbackActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 416
    new-instance v2, Lnet/hockeyapp/android/f/b;

    invoke-direct {v2, p0, v0, v1}, Lnet/hockeyapp/android/f/b;-><init>(Landroid/content/Context;Landroid/view/ViewGroup;Landroid/net/Uri;)V

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_6

    .line 419
    :cond_1f
    const/4 v0, 0x1

    if-ne p1, v0, :cond_42

    .line 421
    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    .line 424
    if-eqz v0, :cond_6

    .line 426
    :try_start_28
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lnet/hockeyapp/android/af;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 427
    const-string v2, "imageUri"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 428
    const/4 v0, 0x3

    invoke-virtual {p0, v1, v0}, Lnet/hockeyapp/android/FeedbackActivity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_38
    .catch Landroid/content/ActivityNotFoundException; {:try_start_28 .. :try_end_38} :catch_39

    goto :goto_6

    .line 429
    :catch_39
    move-exception v0

    .line 430
    const-string v1, "HockeyApp"

    const-string v2, "Paint activity not declared!"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_6

    .line 435
    :cond_42
    if-ne p1, v1, :cond_6

    .line 437
    const-string v0, "imageUri"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 439
    if-eqz v0, :cond_6

    .line 440
    invoke-virtual {p0, v2}, Lnet/hockeyapp/android/FeedbackActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 441
    new-instance v2, Lnet/hockeyapp/android/f/b;

    invoke-direct {v2, p0, v1, v0}, Lnet/hockeyapp/android/f/b;-><init>(Landroid/content/Context;Landroid/view/ViewGroup;Landroid/net/Uri;)V

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_6
.end method

.method public onClick(Landroid/view/View;)V
    .registers 16

    .prologue
    const/16 v13, 0x2011

    const/4 v12, 0x3

    const/4 v11, 0x1

    const/4 v10, 0x0

    const/4 v9, 0x0

    .line 158
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sparse-switch v0, :sswitch_data_16e

    .line 1764
    :goto_d
    return-void

    .line 1740
    :sswitch_e
    invoke-virtual {p0, v9}, Lnet/hockeyapp/android/FeedbackActivity;->a(Z)V

    .line 2562
    iget-object v0, p0, Lnet/hockeyapp/android/FeedbackActivity;->k:Landroid/widget/EditText;

    if-eqz v0, :cond_26

    .line 2563
    const-string v0, "input_method"

    invoke-virtual {p0, v0}, Lnet/hockeyapp/android/FeedbackActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 2564
    iget-object v1, p0, Lnet/hockeyapp/android/FeedbackActivity;->k:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v0, v1, v9}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 3065
    :cond_26
    sget-object v0, Lnet/hockeyapp/android/e/p;->a:Lnet/hockeyapp/android/e/n;

    .line 1743
    iget-object v1, p0, Lnet/hockeyapp/android/FeedbackActivity;->f:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lnet/hockeyapp/android/e/n;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v7

    .line 1745
    iget-object v0, p0, Lnet/hockeyapp/android/FeedbackActivity;->h:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    .line 1746
    iget-object v0, p0, Lnet/hockeyapp/android/FeedbackActivity;->i:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    .line 1747
    iget-object v0, p0, Lnet/hockeyapp/android/FeedbackActivity;->j:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    .line 1748
    iget-object v0, p0, Lnet/hockeyapp/android/FeedbackActivity;->k:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    .line 1750
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_79

    .line 1751
    iget-object v0, p0, Lnet/hockeyapp/android/FeedbackActivity;->j:Landroid/widget/EditText;

    invoke-virtual {v0, v9}, Landroid/widget/EditText;->setVisibility(I)V

    .line 1752
    iget-object v0, p0, Lnet/hockeyapp/android/FeedbackActivity;->j:Landroid/widget/EditText;

    const/16 v1, 0x40e

    invoke-direct {p0, v0, v1}, Lnet/hockeyapp/android/FeedbackActivity;->a(Landroid/widget/EditText;I)V

    goto :goto_d

    .line 1754
    :cond_79
    invoke-static {}, Lnet/hockeyapp/android/t;->b()Lnet/hockeyapp/android/c/i;

    move-result-object v0

    sget-object v1, Lnet/hockeyapp/android/c/i;->c:Lnet/hockeyapp/android/c/i;

    if-ne v0, v1, :cond_90

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_90

    .line 1755
    iget-object v0, p0, Lnet/hockeyapp/android/FeedbackActivity;->h:Landroid/widget/EditText;

    const/16 v1, 0x411

    invoke-direct {p0, v0, v1}, Lnet/hockeyapp/android/FeedbackActivity;->a(Landroid/widget/EditText;I)V

    goto/16 :goto_d

    .line 1757
    :cond_90
    invoke-static {}, Lnet/hockeyapp/android/t;->c()Lnet/hockeyapp/android/c/i;

    move-result-object v0

    sget-object v1, Lnet/hockeyapp/android/c/i;->c:Lnet/hockeyapp/android/c/i;

    if-ne v0, v1, :cond_a7

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_a7

    .line 1758
    iget-object v0, p0, Lnet/hockeyapp/android/FeedbackActivity;->i:Landroid/widget/EditText;

    const/16 v1, 0x412

    invoke-direct {p0, v0, v1}, Lnet/hockeyapp/android/FeedbackActivity;->a(Landroid/widget/EditText;I)V

    goto/16 :goto_d

    .line 1760
    :cond_a7
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_b6

    .line 1761
    iget-object v0, p0, Lnet/hockeyapp/android/FeedbackActivity;->k:Landroid/widget/EditText;

    const/16 v1, 0x413

    invoke-direct {p0, v0, v1}, Lnet/hockeyapp/android/FeedbackActivity;->a(Landroid/widget/EditText;I)V

    goto/16 :goto_d

    .line 1763
    :cond_b6
    invoke-static {}, Lnet/hockeyapp/android/t;->c()Lnet/hockeyapp/android/c/i;

    move-result-object v0

    sget-object v1, Lnet/hockeyapp/android/c/i;->c:Lnet/hockeyapp/android/c/i;

    if-ne v0, v1, :cond_cd

    invoke-static {v3}, Lnet/hockeyapp/android/e/w;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_cd

    .line 1764
    iget-object v0, p0, Lnet/hockeyapp/android/FeedbackActivity;->i:Landroid/widget/EditText;

    const/16 v1, 0x40f

    invoke-direct {p0, v0, v1}, Lnet/hockeyapp/android/FeedbackActivity;->a(Landroid/widget/EditText;I)V

    goto/16 :goto_d

    .line 4065
    :cond_cd
    sget-object v0, Lnet/hockeyapp/android/e/p;->a:Lnet/hockeyapp/android/e/n;

    .line 1768
    iget-object v1, p0, Lnet/hockeyapp/android/FeedbackActivity;->f:Landroid/content/Context;

    .line 4113
    if-eqz v1, :cond_f9

    .line 4114
    const-string v6, "net.hockeyapp.android.prefs_name_email"

    invoke-virtual {v1, v6, v9}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, v0, Lnet/hockeyapp/android/e/n;->a:Landroid/content/SharedPreferences;

    .line 4115
    iget-object v1, v0, Lnet/hockeyapp/android/e/n;->a:Landroid/content/SharedPreferences;

    if-eqz v1, :cond_f9

    .line 4116
    iget-object v1, v0, Lnet/hockeyapp/android/e/n;->a:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    iput-object v1, v0, Lnet/hockeyapp/android/e/n;->b:Landroid/content/SharedPreferences$Editor;

    .line 4117
    if-eqz v2, :cond_ed

    if-eqz v3, :cond_ed

    if-nez v4, :cond_10d

    .line 4118
    :cond_ed
    iget-object v1, v0, Lnet/hockeyapp/android/e/n;->b:Landroid/content/SharedPreferences$Editor;

    const-string v6, "net.hockeyapp.android.prefs_key_name_email"

    invoke-interface {v1, v6, v10}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 4124
    :goto_f4
    iget-object v0, v0, Lnet/hockeyapp/android/e/n;->b:Landroid/content/SharedPreferences$Editor;

    invoke-static {v0}, Lnet/hockeyapp/android/e/n;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 1771
    :cond_f9
    invoke-virtual {p0, v13}, Lnet/hockeyapp/android/FeedbackActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lnet/hockeyapp/android/f/a;

    .line 1772
    invoke-virtual {v0}, Lnet/hockeyapp/android/f/a;->getAttachments()Ljava/util/ArrayList;

    move-result-object v6

    .line 1775
    iget-object v1, p0, Lnet/hockeyapp/android/FeedbackActivity;->x:Ljava/lang/String;

    iget-object v8, p0, Lnet/hockeyapp/android/FeedbackActivity;->t:Landroid/os/Handler;

    move-object v0, p0

    invoke-direct/range {v0 .. v9}, Lnet/hockeyapp/android/FeedbackActivity;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Landroid/os/Handler;Z)V

    goto/16 :goto_d

    .line 4120
    :cond_10d
    iget-object v1, v0, Lnet/hockeyapp/android/e/n;->b:Landroid/content/SharedPreferences$Editor;

    const-string v6, "net.hockeyapp.android.prefs_key_name_email"

    const-string v8, "%s|%s|%s"

    new-array v10, v12, [Ljava/lang/Object;

    aput-object v2, v10, v9

    aput-object v3, v10, v11

    const/4 v11, 0x2

    aput-object v4, v10, v11

    invoke-static {v8, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v1, v6, v8}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    goto :goto_f4

    .line 164
    :sswitch_124
    invoke-virtual {p0, v13}, Lnet/hockeyapp/android/FeedbackActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 165
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-lt v0, v12, :cond_149

    .line 166
    const-string v0, ""

    new-array v1, v11, [Ljava/lang/Object;

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v9

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x3e8

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_d

    .line 168
    :cond_149
    invoke-virtual {p0, p1}, Lnet/hockeyapp/android/FeedbackActivity;->openContextMenu(Landroid/view/View;)V

    goto/16 :goto_d

    .line 173
    :sswitch_14e
    invoke-virtual {p0, v9}, Lnet/hockeyapp/android/FeedbackActivity;->b(Z)V

    .line 174
    iput-boolean v11, p0, Lnet/hockeyapp/android/FeedbackActivity;->B:Z

    goto/16 :goto_d

    .line 178
    :sswitch_155
    iget-object v1, p0, Lnet/hockeyapp/android/FeedbackActivity;->x:Ljava/lang/String;

    .line 5065
    sget-object v0, Lnet/hockeyapp/android/e/p;->a:Lnet/hockeyapp/android/e/n;

    .line 178
    iget-object v2, p0, Lnet/hockeyapp/android/FeedbackActivity;->f:Landroid/content/Context;

    invoke-virtual {v0, v2}, Lnet/hockeyapp/android/e/n;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lnet/hockeyapp/android/FeedbackActivity;->t:Landroid/os/Handler;

    move-object v0, p0

    move-object v2, v10

    move-object v3, v10

    move-object v4, v10

    move-object v5, v10

    move-object v6, v10

    move v9, v11

    invoke-direct/range {v0 .. v9}, Lnet/hockeyapp/android/FeedbackActivity;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Landroid/os/Handler;Z)V

    goto/16 :goto_d

    .line 158
    nop

    :sswitch_data_16e
    .sparse-switch
        0x2009 -> :sswitch_e
        0x2010 -> :sswitch_124
        0x20010 -> :sswitch_14e
        0x20011 -> :sswitch_155
    .end sparse-switch
.end method

.method public onContextItemSelected(Landroid/view/MenuItem;)Z
    .registers 6

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    .line 191
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    packed-switch v1, :pswitch_data_52

    .line 197
    invoke-super {p0, p1}, Landroid/app/Activity;->onContextItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    .line 5534
    :goto_d
    return v0

    .line 194
    :pswitch_e
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    .line 5522
    if-ne v1, v3, :cond_31

    .line 5523
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 5524
    const-string v2, "*/*"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 5525
    const-string v2, "android.intent.action.GET_CONTENT"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 5526
    const/16 v2, 0x416

    invoke-static {v2}, Lnet/hockeyapp/android/aj;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v1, v3}, Lnet/hockeyapp/android/FeedbackActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_d

    .line 5529
    :cond_31
    if-ne v1, v0, :cond_50

    .line 5530
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 5531
    const-string v2, "image/*"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 5532
    const-string v2, "android.intent.action.GET_CONTENT"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 5533
    const/16 v2, 0x417

    invoke-static {v2}, Lnet/hockeyapp/android/aj;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v1, v0}, Lnet/hockeyapp/android/FeedbackActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_d

    .line 5536
    :cond_50
    const/4 v0, 0x0

    .line 194
    goto :goto_d

    .line 191
    :pswitch_data_52
    .packed-switch 0x1
        :pswitch_e
        :pswitch_e
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 8

    .prologue
    const/4 v2, 0x0

    .line 208
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 210
    invoke-direct {p0}, Lnet/hockeyapp/android/FeedbackActivity;->b()Landroid/view/ViewGroup;

    move-result-object v0

    invoke-virtual {p0, v0}, Lnet/hockeyapp/android/FeedbackActivity;->setContentView(Landroid/view/View;)V

    .line 212
    const/16 v0, 0x40b

    invoke-static {v0}, Lnet/hockeyapp/android/aj;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lnet/hockeyapp/android/FeedbackActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 213
    iput-object p0, p0, Lnet/hockeyapp/android/FeedbackActivity;->f:Landroid/content/Context;

    .line 215
    invoke-virtual {p0}, Lnet/hockeyapp/android/FeedbackActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 216
    if-eqz v0, :cond_48

    .line 217
    const-string v1, "url"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lnet/hockeyapp/android/FeedbackActivity;->x:Ljava/lang/String;

    .line 219
    const-string v1, "initialAttachments"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelableArray(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v3

    .line 220
    if-eqz v3, :cond_48

    .line 221
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lnet/hockeyapp/android/FeedbackActivity;->w:Ljava/util/List;

    .line 222
    array-length v4, v3

    move v1, v2

    :goto_39
    if-ge v1, v4, :cond_48

    aget-object v0, v3, v1

    .line 223
    iget-object v5, p0, Lnet/hockeyapp/android/FeedbackActivity;->w:Ljava/util/List;

    check-cast v0, Landroid/net/Uri;

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 222
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_39

    .line 228
    :cond_48
    if-eqz p1, :cond_78

    .line 229
    const-string v0, "feedbackViewInitialized"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lnet/hockeyapp/android/FeedbackActivity;->C:Z

    .line 230
    const-string v0, "inSendFeedback"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lnet/hockeyapp/android/FeedbackActivity;->B:Z

    .line 238
    :goto_5a
    const-string v0, "notification"

    invoke-virtual {p0, v0}, Lnet/hockeyapp/android/FeedbackActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 239
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 5572
    new-instance v0, Lnet/hockeyapp/android/m;

    invoke-direct {v0, p0}, Lnet/hockeyapp/android/m;-><init>(Lnet/hockeyapp/android/FeedbackActivity;)V

    iput-object v0, p0, Lnet/hockeyapp/android/FeedbackActivity;->t:Landroid/os/Handler;

    .line 5625
    new-instance v0, Lnet/hockeyapp/android/o;

    invoke-direct {v0, p0}, Lnet/hockeyapp/android/o;-><init>(Lnet/hockeyapp/android/FeedbackActivity;)V

    iput-object v0, p0, Lnet/hockeyapp/android/FeedbackActivity;->v:Landroid/os/Handler;

    .line 243
    invoke-direct {p0}, Lnet/hockeyapp/android/FeedbackActivity;->d()V

    .line 244
    return-void

    .line 233
    :cond_78
    iput-boolean v2, p0, Lnet/hockeyapp/android/FeedbackActivity;->B:Z

    .line 234
    iput-boolean v2, p0, Lnet/hockeyapp/android/FeedbackActivity;->C:Z

    goto :goto_5a
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .registers 7

    .prologue
    const/4 v2, 0x0

    .line 251
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V

    .line 253
    const/4 v0, 0x2

    const/16 v1, 0x414

    invoke-static {v1}, Lnet/hockeyapp/android/aj;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/ContextMenu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 254
    const/4 v0, 0x1

    const/16 v1, 0x415

    invoke-static {v1}, Lnet/hockeyapp/android/aj;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/ContextMenu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 255
    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .registers 5

    .prologue
    .line 449
    packed-switch p1, :pswitch_data_3e

    .line 464
    const/4 v0, 0x0

    :goto_4
    return-object v0

    .line 451
    :pswitch_5
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const/16 v1, 0x803

    .line 452
    invoke-static {v1}, Lnet/hockeyapp/android/aj;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/4 v1, 0x0

    .line 453
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/16 v1, 0x802

    .line 454
    invoke-static {v1}, Lnet/hockeyapp/android/aj;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x1080027

    .line 455
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/16 v1, 0x800

    .line 456
    invoke-static {v1}, Lnet/hockeyapp/android/aj;->a(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lnet/hockeyapp/android/l;

    invoke-direct {v2, p0}, Lnet/hockeyapp/android/l;-><init>(Lnet/hockeyapp/android/FeedbackActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 461
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_4

    .line 449
    :pswitch_data_3e
    .packed-switch 0x0
        :pswitch_5
    .end packed-switch
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .registers 4

    .prologue
    .line 268
    const/4 v0, 0x4

    if-ne p1, v0, :cond_13

    .line 269
    iget-boolean v0, p0, Lnet/hockeyapp/android/FeedbackActivity;->B:Z

    if-eqz v0, :cond_f

    .line 270
    const/4 v0, 0x0

    iput-boolean v0, p0, Lnet/hockeyapp/android/FeedbackActivity;->B:Z

    .line 271
    invoke-direct {p0}, Lnet/hockeyapp/android/FeedbackActivity;->d()V

    .line 276
    :goto_d
    const/4 v0, 0x1

    .line 279
    :goto_e
    return v0

    .line 273
    :cond_f
    invoke-virtual {p0}, Lnet/hockeyapp/android/FeedbackActivity;->finish()V

    goto :goto_d

    .line 279
    :cond_13
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_e
.end method

.method protected onPrepareDialog(ILandroid/app/Dialog;)V
    .registers 4

    .prologue
    .line 469
    packed-switch p1, :pswitch_data_1c

    .line 482
    :goto_3
    return-void

    .line 471
    :pswitch_4
    check-cast p2, Landroid/app/AlertDialog;

    .line 472
    iget-object v0, p0, Lnet/hockeyapp/android/FeedbackActivity;->y:Lnet/hockeyapp/android/c/c;

    if-eqz v0, :cond_12

    .line 474
    iget-object v0, p0, Lnet/hockeyapp/android/FeedbackActivity;->y:Lnet/hockeyapp/android/c/c;

    .line 8054
    iget-object v0, v0, Lnet/hockeyapp/android/c/c;->a:Ljava/lang/String;

    .line 474
    invoke-virtual {p2, v0}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    goto :goto_3

    .line 477
    :cond_12
    const/16 v0, 0x410

    invoke-static {v0}, Lnet/hockeyapp/android/aj;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    goto :goto_3

    .line 469
    :pswitch_data_1c
    .packed-switch 0x0
        :pswitch_4
    .end packed-switch
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .registers 6

    .prologue
    .line 489
    if-eqz p1, :cond_31

    .line 490
    const/16 v0, 0x2011

    invoke-virtual {p0, v0}, Lnet/hockeyapp/android/FeedbackActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 491
    const-string v1, "attachments"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    .line 492
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_14
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_29

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/Uri;

    .line 493
    new-instance v3, Lnet/hockeyapp/android/f/b;

    invoke-direct {v3, p0, v0, v1}, Lnet/hockeyapp/android/f/b;-><init>(Landroid/content/Context;Landroid/view/ViewGroup;Landroid/net/Uri;)V

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_14

    .line 496
    :cond_29
    const-string v0, "feedbackViewInitialized"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lnet/hockeyapp/android/FeedbackActivity;->C:Z

    .line 499
    :cond_31
    invoke-super {p0, p1}, Landroid/app/Activity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 500
    return-void
.end method

.method public onRetainNonConfigurationInstance()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 291
    iget-object v0, p0, Lnet/hockeyapp/android/FeedbackActivity;->s:Lnet/hockeyapp/android/d/r;

    if-eqz v0, :cond_9

    .line 292
    iget-object v0, p0, Lnet/hockeyapp/android/FeedbackActivity;->s:Lnet/hockeyapp/android/d/r;

    invoke-virtual {v0}, Lnet/hockeyapp/android/d/r;->a()V

    .line 295
    :cond_9
    iget-object v0, p0, Lnet/hockeyapp/android/FeedbackActivity;->s:Lnet/hockeyapp/android/d/r;

    return-object v0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 4

    .prologue
    .line 507
    const/16 v0, 0x2011

    invoke-virtual {p0, v0}, Lnet/hockeyapp/android/FeedbackActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lnet/hockeyapp/android/f/a;

    .line 509
    const-string v1, "attachments"

    invoke-virtual {v0}, Lnet/hockeyapp/android/f/a;->getAttachments()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 510
    const-string v0, "feedbackViewInitialized"

    iget-boolean v1, p0, Lnet/hockeyapp/android/FeedbackActivity;->C:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 511
    const-string v0, "inSendFeedback"

    iget-boolean v1, p0, Lnet/hockeyapp/android/FeedbackActivity;->B:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 513
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 514
    return-void
.end method

.method protected onStop()V
    .registers 2

    .prologue
    .line 259
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 261
    iget-object v0, p0, Lnet/hockeyapp/android/FeedbackActivity;->s:Lnet/hockeyapp/android/d/r;

    if-eqz v0, :cond_c

    .line 262
    iget-object v0, p0, Lnet/hockeyapp/android/FeedbackActivity;->s:Lnet/hockeyapp/android/d/r;

    invoke-virtual {v0}, Lnet/hockeyapp/android/d/r;->a()V

    .line 264
    :cond_c
    return-void
.end method
