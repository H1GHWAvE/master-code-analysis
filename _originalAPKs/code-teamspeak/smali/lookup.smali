.class public Llookup;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/String;Lorg/xbill/DNS/Lookup;)V
    .registers 7

    .prologue
    const/4 v1, 0x0

    .line 11
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 12
    invoke-virtual {p1}, Lorg/xbill/DNS/Lookup;->getResult()I

    move-result v0

    .line 13
    if-eqz v0, :cond_37

    .line 14
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuffer;

    const-string v3, " "

    invoke-direct {v2, v3}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lorg/xbill/DNS/Lookup;->getErrorString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 15
    :cond_37
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v0}, Ljava/io/PrintStream;->println()V

    .line 16
    invoke-virtual {p1}, Lorg/xbill/DNS/Lookup;->getAliases()[Lorg/xbill/DNS/Name;

    move-result-object v2

    .line 17
    array-length v0, v2

    if-lez v0, :cond_69

    .line 18
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "# aliases: "

    invoke-virtual {v0, v3}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    move v0, v1

    .line 19
    :goto_4b
    array-length v3, v2

    if-ge v0, v3, :cond_64

    .line 20
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    aget-object v4, v2, v0

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->print(Ljava/lang/Object;)V

    .line 21
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_61

    .line 22
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 19
    :cond_61
    add-int/lit8 v0, v0, 0x1

    goto :goto_4b

    .line 24
    :cond_64
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v0}, Ljava/io/PrintStream;->println()V

    .line 26
    :cond_69
    invoke-virtual {p1}, Lorg/xbill/DNS/Lookup;->getResult()I

    move-result v0

    if-nez v0, :cond_80

    .line 27
    invoke-virtual {p1}, Lorg/xbill/DNS/Lookup;->getAnswers()[Lorg/xbill/DNS/Record;

    move-result-object v0

    .line 28
    :goto_73
    array-length v2, v0

    if-ge v1, v2, :cond_80

    .line 29
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    aget-object v3, v0, v1

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    .line 28
    add-int/lit8 v1, v1, 0x1

    goto :goto_73

    .line 31
    :cond_80
    return-void
.end method

.method public static main([Ljava/lang/String;)V
    .registers 6

    .prologue
    const/4 v0, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 35
    .line 37
    array-length v3, p0

    if-le v3, v0, :cond_36

    aget-object v3, p0, v1

    const-string v4, "-t"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_36

    .line 38
    aget-object v1, p0, v2

    invoke-static {v1}, Lorg/xbill/DNS/Type;->value(Ljava/lang/String;)I

    move-result v1

    .line 39
    if-gez v1, :cond_20

    .line 40
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "invalid type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 43
    :cond_20
    :goto_20
    array-length v2, p0

    if-ge v0, v2, :cond_35

    .line 44
    new-instance v2, Lorg/xbill/DNS/Lookup;

    aget-object v3, p0, v0

    invoke-direct {v2, v3, v1}, Lorg/xbill/DNS/Lookup;-><init>(Ljava/lang/String;I)V

    .line 45
    invoke-virtual {v2}, Lorg/xbill/DNS/Lookup;->run()[Lorg/xbill/DNS/Record;

    .line 46
    aget-object v3, p0, v0

    invoke-static {v3, v2}, Llookup;->a(Ljava/lang/String;Lorg/xbill/DNS/Lookup;)V

    .line 43
    add-int/lit8 v0, v0, 0x1

    goto :goto_20

    .line 48
    :cond_35
    return-void

    :cond_36
    move v0, v1

    move v1, v2

    goto :goto_20
.end method
