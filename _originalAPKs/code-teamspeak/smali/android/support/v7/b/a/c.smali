.class public Landroid/support/v7/b/a/c;
.super Landroid/graphics/drawable/Drawable;
.source "SourceFile"


# static fields
.field public static final a:I = 0x0

.field public static final b:I = 0x1

.field public static final c:I = 0x2

.field public static final d:I = 0x3

.field private static final g:F


# instance fields
.field public e:F

.field private final f:Landroid/graphics/Paint;

.field private h:F

.field private i:F

.field private j:F

.field private k:F

.field private l:Z

.field private final m:Landroid/graphics/Path;

.field private final n:I

.field private o:Z

.field private p:F

.field private q:I


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 95
    const-wide v0, 0x4046800000000000L    # 45.0

    invoke-static {v0, v1}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v0

    double-to-float v0, v0

    sput v0, Landroid/support/v7/b/a/c;->g:F

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 11

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 123
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 92
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Landroid/support/v7/b/a/c;->f:Landroid/graphics/Paint;

    .line 108
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Landroid/support/v7/b/a/c;->m:Landroid/graphics/Path;

    .line 112
    iput-boolean v7, p0, Landroid/support/v7/b/a/c;->o:Z

    .line 118
    const/4 v0, 0x2

    iput v0, p0, Landroid/support/v7/b/a/c;->q:I

    .line 124
    iget-object v0, p0, Landroid/support/v7/b/a/c;->f:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 125
    iget-object v0, p0, Landroid/support/v7/b/a/c;->f:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Join;->MITER:Landroid/graphics/Paint$Join;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V

    .line 126
    iget-object v0, p0, Landroid/support/v7/b/a/c;->f:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Cap;->BUTT:Landroid/graphics/Paint$Cap;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 127
    iget-object v0, p0, Landroid/support/v7/b/a/c;->f:Landroid/graphics/Paint;

    invoke-virtual {v0, v8}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 129
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    const/4 v1, 0x0

    sget-object v2, Landroid/support/v7/a/n;->DrawerArrowToggle:[I

    sget v3, Landroid/support/v7/a/d;->drawerArrowStyle:I

    sget v4, Landroid/support/v7/a/m;->Base_Widget_AppCompat_DrawerArrowToggle:I

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 133
    sget v1, Landroid/support/v7/a/n;->DrawerArrowToggle_color:I

    invoke-virtual {v0, v1, v7}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    .line 1211
    iget-object v2, p0, Landroid/support/v7/b/a/c;->f:Landroid/graphics/Paint;

    invoke-virtual {v2}, Landroid/graphics/Paint;->getColor()I

    move-result v2

    if-eq v1, v2, :cond_58

    .line 1212
    iget-object v2, p0, Landroid/support/v7/b/a/c;->f:Landroid/graphics/Paint;

    invoke-virtual {v2, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1213
    invoke-virtual {p0}, Landroid/support/v7/b/a/c;->invalidateSelf()V

    .line 134
    :cond_58
    sget v1, Landroid/support/v7/a/n;->DrawerArrowToggle_thickness:I

    invoke-virtual {v0, v1, v6}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    .line 1231
    iget-object v2, p0, Landroid/support/v7/b/a/c;->f:Landroid/graphics/Paint;

    invoke-virtual {v2}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v2

    cmpl-float v2, v2, v1

    if-eqz v2, :cond_7f

    .line 1232
    iget-object v2, p0, Landroid/support/v7/b/a/c;->f:Landroid/graphics/Paint;

    invoke-virtual {v2, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1233
    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    float-to-double v2, v1

    sget v1, Landroid/support/v7/b/a/c;->g:F

    float-to-double v4, v1

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    mul-double/2addr v2, v4

    double-to-float v1, v2

    iput v1, p0, Landroid/support/v7/b/a/c;->p:F

    .line 1234
    invoke-virtual {p0}, Landroid/support/v7/b/a/c;->invalidateSelf()V

    .line 135
    :cond_7f
    sget v1, Landroid/support/v7/a/n;->DrawerArrowToggle_spinBars:I

    invoke-virtual {v0, v1, v8}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    .line 1295
    iget-boolean v2, p0, Landroid/support/v7/b/a/c;->l:Z

    if-eq v2, v1, :cond_8e

    .line 1296
    iput-boolean v1, p0, Landroid/support/v7/b/a/c;->l:Z

    .line 1297
    invoke-virtual {p0}, Landroid/support/v7/b/a/c;->invalidateSelf()V

    .line 137
    :cond_8e
    sget v1, Landroid/support/v7/a/n;->DrawerArrowToggle_gapBetweenBars:I

    invoke-virtual {v0, v1, v6}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    int-to-float v1, v1

    .line 2262
    iget v2, p0, Landroid/support/v7/b/a/c;->k:F

    cmpl-float v2, v1, v2

    if-eqz v2, :cond_a4

    .line 2263
    iput v1, p0, Landroid/support/v7/b/a/c;->k:F

    .line 2264
    invoke-virtual {p0}, Landroid/support/v7/b/a/c;->invalidateSelf()V

    .line 139
    :cond_a4
    sget v1, Landroid/support/v7/a/n;->DrawerArrowToggle_drawableSize:I

    invoke-virtual {v0, v1, v7}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Landroid/support/v7/b/a/c;->n:I

    .line 141
    sget v1, Landroid/support/v7/a/n;->DrawerArrowToggle_barLength:I

    invoke-virtual {v0, v1, v6}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    int-to-float v1, v1

    iput v1, p0, Landroid/support/v7/b/a/c;->i:F

    .line 143
    sget v1, Landroid/support/v7/a/n;->DrawerArrowToggle_arrowHeadLength:I

    invoke-virtual {v0, v1, v6}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    int-to-float v1, v1

    iput v1, p0, Landroid/support/v7/b/a/c;->h:F

    .line 145
    sget v1, Landroid/support/v7/a/n;->DrawerArrowToggle_arrowShaftLength:I

    invoke-virtual {v0, v1, v6}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    iput v1, p0, Landroid/support/v7/b/a/c;->j:F

    .line 146
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 147
    return-void
.end method

.method private a()F
    .registers 2

    .prologue
    .line 166
    iget v0, p0, Landroid/support/v7/b/a/c;->h:F

    return v0
.end method

.method private static a(FFF)F
    .registers 4

    .prologue
    .line 453
    sub-float v0, p1, p0

    mul-float/2addr v0, p2

    add-float/2addr v0, p0

    return v0
.end method

.method private a(F)V
    .registers 3

    .prologue
    .line 155
    iget v0, p0, Landroid/support/v7/b/a/c;->h:F

    cmpl-float v0, v0, p1

    if-eqz v0, :cond_b

    .line 156
    iput p1, p0, Landroid/support/v7/b/a/c;->h:F

    .line 157
    invoke-virtual {p0}, Landroid/support/v7/b/a/c;->invalidateSelf()V

    .line 159
    :cond_b
    return-void
.end method

.method private a(I)V
    .registers 3
    .param p1    # I
        .annotation build Landroid/support/a/j;
        .end annotation
    .end param

    .prologue
    .line 211
    iget-object v0, p0, Landroid/support/v7/b/a/c;->f:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getColor()I

    move-result v0

    if-eq p1, v0, :cond_10

    .line 212
    iget-object v0, p0, Landroid/support/v7/b/a/c;->f:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 213
    invoke-virtual {p0}, Landroid/support/v7/b/a/c;->invalidateSelf()V

    .line 215
    :cond_10
    return-void
.end method

.method private b()F
    .registers 2

    .prologue
    .line 185
    iget v0, p0, Landroid/support/v7/b/a/c;->j:F

    return v0
.end method

.method private b(F)V
    .registers 3

    .prologue
    .line 175
    iget v0, p0, Landroid/support/v7/b/a/c;->j:F

    cmpl-float v0, v0, p1

    if-eqz v0, :cond_b

    .line 176
    iput p1, p0, Landroid/support/v7/b/a/c;->j:F

    .line 177
    invoke-virtual {p0}, Landroid/support/v7/b/a/c;->invalidateSelf()V

    .line 179
    :cond_b
    return-void
.end method

.method private b(I)V
    .registers 3

    .prologue
    .line 272
    iget v0, p0, Landroid/support/v7/b/a/c;->q:I

    if-eq p1, v0, :cond_9

    .line 273
    iput p1, p0, Landroid/support/v7/b/a/c;->q:I

    .line 274
    invoke-virtual {p0}, Landroid/support/v7/b/a/c;->invalidateSelf()V

    .line 276
    :cond_9
    return-void
.end method

.method private b(Z)V
    .registers 3

    .prologue
    .line 295
    iget-boolean v0, p0, Landroid/support/v7/b/a/c;->l:Z

    if-eq v0, p1, :cond_9

    .line 296
    iput-boolean p1, p0, Landroid/support/v7/b/a/c;->l:Z

    .line 297
    invoke-virtual {p0}, Landroid/support/v7/b/a/c;->invalidateSelf()V

    .line 299
    :cond_9
    return-void
.end method

.method private c()F
    .registers 2

    .prologue
    .line 192
    iget v0, p0, Landroid/support/v7/b/a/c;->i:F

    return v0
.end method

.method private c(F)V
    .registers 3

    .prologue
    .line 201
    iget v0, p0, Landroid/support/v7/b/a/c;->i:F

    cmpl-float v0, v0, p1

    if-eqz v0, :cond_b

    .line 202
    iput p1, p0, Landroid/support/v7/b/a/c;->i:F

    .line 203
    invoke-virtual {p0}, Landroid/support/v7/b/a/c;->invalidateSelf()V

    .line 205
    :cond_b
    return-void
.end method

.method private d()I
    .registers 2
    .annotation build Landroid/support/a/j;
    .end annotation

    .prologue
    .line 222
    iget-object v0, p0, Landroid/support/v7/b/a/c;->f:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getColor()I

    move-result v0

    return v0
.end method

.method private d(F)V
    .registers 6

    .prologue
    .line 231
    iget-object v0, p0, Landroid/support/v7/b/a/c;->f:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v0

    cmpl-float v0, v0, p1

    if-eqz v0, :cond_22

    .line 232
    iget-object v0, p0, Landroid/support/v7/b/a/c;->f:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 233
    const/high16 v0, 0x40000000    # 2.0f

    div-float v0, p1, v0

    float-to-double v0, v0

    sget v2, Landroid/support/v7/b/a/c;->g:F

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v2

    mul-double/2addr v0, v2

    double-to-float v0, v0

    iput v0, p0, Landroid/support/v7/b/a/c;->p:F

    .line 234
    invoke-virtual {p0}, Landroid/support/v7/b/a/c;->invalidateSelf()V

    .line 236
    :cond_22
    return-void
.end method

.method private e()F
    .registers 2

    .prologue
    .line 242
    iget-object v0, p0, Landroid/support/v7/b/a/c;->f:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v0

    return v0
.end method

.method private e(F)V
    .registers 3

    .prologue
    .line 262
    iget v0, p0, Landroid/support/v7/b/a/c;->k:F

    cmpl-float v0, p1, v0

    if-eqz v0, :cond_b

    .line 263
    iput p1, p0, Landroid/support/v7/b/a/c;->k:F

    .line 264
    invoke-virtual {p0}, Landroid/support/v7/b/a/c;->invalidateSelf()V

    .line 266
    :cond_b
    return-void
.end method

.method private f()F
    .registers 2

    .prologue
    .line 251
    iget v0, p0, Landroid/support/v7/b/a/c;->k:F

    return v0
.end method

.method private f(F)V
    .registers 3
    .param p1    # F
        .annotation build Landroid/support/a/n;
            a = 0.0
            b = 1.0
        .end annotation
    .end param

    .prologue
    .line 443
    iget v0, p0, Landroid/support/v7/b/a/c;->e:F

    cmpl-float v0, v0, p1

    if-eqz v0, :cond_b

    .line 444
    iput p1, p0, Landroid/support/v7/b/a/c;->e:F

    .line 445
    invoke-virtual {p0}, Landroid/support/v7/b/a/c;->invalidateSelf()V

    .line 447
    :cond_b
    return-void
.end method

.method private g()Z
    .registers 2

    .prologue
    .line 284
    iget-boolean v0, p0, Landroid/support/v7/b/a/c;->l:Z

    return v0
.end method

.method private h()I
    .registers 2

    .prologue
    .line 306
    iget v0, p0, Landroid/support/v7/b/a/c;->q:I

    return v0
.end method

.method private i()F
    .registers 2
    .annotation build Landroid/support/a/n;
        a = 0.0
        b = 1.0
    .end annotation

    .prologue
    .line 432
    iget v0, p0, Landroid/support/v7/b/a/c;->e:F

    return v0
.end method


# virtual methods
.method public final a(Z)V
    .registers 3

    .prologue
    .line 313
    iget-boolean v0, p0, Landroid/support/v7/b/a/c;->o:Z

    if-eq v0, p1, :cond_9

    .line 314
    iput-boolean p1, p0, Landroid/support/v7/b/a/c;->o:Z

    .line 315
    invoke-virtual {p0}, Landroid/support/v7/b/a/c;->invalidateSelf()V

    .line 317
    :cond_9
    return-void
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .registers 14

    .prologue
    .line 321
    invoke-virtual {p0}, Landroid/support/v7/b/a/c;->getBounds()Landroid/graphics/Rect;

    move-result-object v3

    .line 324
    iget v0, p0, Landroid/support/v7/b/a/c;->q:I

    packed-switch v0, :pswitch_data_128

    .line 337
    :pswitch_9
    invoke-static {p0}, Landroid/support/v4/e/a/a;->e(Landroid/graphics/drawable/Drawable;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_112

    const/4 v0, 0x1

    .line 344
    :goto_11
    iget v1, p0, Landroid/support/v7/b/a/c;->h:F

    iget v2, p0, Landroid/support/v7/b/a/c;->h:F

    mul-float/2addr v1, v2

    const/high16 v2, 0x40000000    # 2.0f

    mul-float/2addr v1, v2

    float-to-double v4, v1

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    double-to-float v1, v4

    .line 345
    iget v2, p0, Landroid/support/v7/b/a/c;->i:F

    iget v4, p0, Landroid/support/v7/b/a/c;->e:F

    .line 2453
    sub-float/2addr v1, v2

    mul-float/2addr v1, v4

    add-float v4, v2, v1

    .line 346
    iget v1, p0, Landroid/support/v7/b/a/c;->i:F

    iget v2, p0, Landroid/support/v7/b/a/c;->j:F

    iget v5, p0, Landroid/support/v7/b/a/c;->e:F

    .line 3453
    sub-float/2addr v2, v1

    mul-float/2addr v2, v5

    add-float v5, v1, v2

    .line 348
    const/4 v1, 0x0

    iget v2, p0, Landroid/support/v7/b/a/c;->p:F

    iget v6, p0, Landroid/support/v7/b/a/c;->e:F

    .line 4453
    const/4 v7, 0x0

    sub-float/2addr v2, v7

    mul-float/2addr v2, v6

    add-float/2addr v1, v2

    .line 348
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    int-to-float v6, v1

    .line 350
    const/4 v1, 0x0

    sget v2, Landroid/support/v7/b/a/c;->g:F

    iget v7, p0, Landroid/support/v7/b/a/c;->e:F

    .line 5453
    const/4 v8, 0x0

    sub-float/2addr v2, v8

    mul-float/2addr v2, v7

    add-float v7, v1, v2

    .line 353
    if-eqz v0, :cond_115

    const/4 v1, 0x0

    move v2, v1

    :goto_4d
    if-eqz v0, :cond_11a

    const/high16 v1, 0x43340000    # 180.0f

    :goto_51
    iget v8, p0, Landroid/support/v7/b/a/c;->e:F

    .line 6453
    sub-float/2addr v1, v2

    mul-float/2addr v1, v8

    add-float/2addr v1, v2

    .line 356
    float-to-double v8, v4

    float-to-double v10, v7

    invoke-static {v10, v11}, Ljava/lang/Math;->cos(D)D

    move-result-wide v10

    mul-double/2addr v8, v10

    invoke-static {v8, v9}, Ljava/lang/Math;->round(D)J

    move-result-wide v8

    long-to-float v2, v8

    .line 357
    float-to-double v8, v4

    float-to-double v10, v7

    invoke-static {v10, v11}, Ljava/lang/Math;->sin(D)D

    move-result-wide v10

    mul-double/2addr v8, v10

    invoke-static {v8, v9}, Ljava/lang/Math;->round(D)J

    move-result-wide v8

    long-to-float v4, v8

    .line 359
    iget-object v7, p0, Landroid/support/v7/b/a/c;->m:Landroid/graphics/Path;

    invoke-virtual {v7}, Landroid/graphics/Path;->rewind()V

    .line 360
    iget v7, p0, Landroid/support/v7/b/a/c;->k:F

    iget-object v8, p0, Landroid/support/v7/b/a/c;->f:Landroid/graphics/Paint;

    invoke-virtual {v8}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v8

    add-float/2addr v7, v8

    iget v8, p0, Landroid/support/v7/b/a/c;->p:F

    neg-float v8, v8

    iget v9, p0, Landroid/support/v7/b/a/c;->e:F

    .line 7453
    sub-float/2addr v8, v7

    mul-float/2addr v8, v9

    add-float/2addr v7, v8

    .line 363
    neg-float v8, v5

    const/high16 v9, 0x40000000    # 2.0f

    div-float/2addr v8, v9

    .line 365
    iget-object v9, p0, Landroid/support/v7/b/a/c;->m:Landroid/graphics/Path;

    add-float v10, v8, v6

    const/4 v11, 0x0

    invoke-virtual {v9, v10, v11}, Landroid/graphics/Path;->moveTo(FF)V

    .line 366
    iget-object v9, p0, Landroid/support/v7/b/a/c;->m:Landroid/graphics/Path;

    const/high16 v10, 0x40000000    # 2.0f

    mul-float/2addr v6, v10

    sub-float/2addr v5, v6

    const/4 v6, 0x0

    invoke-virtual {v9, v5, v6}, Landroid/graphics/Path;->rLineTo(FF)V

    .line 369
    iget-object v5, p0, Landroid/support/v7/b/a/c;->m:Landroid/graphics/Path;

    invoke-virtual {v5, v8, v7}, Landroid/graphics/Path;->moveTo(FF)V

    .line 370
    iget-object v5, p0, Landroid/support/v7/b/a/c;->m:Landroid/graphics/Path;

    invoke-virtual {v5, v2, v4}, Landroid/graphics/Path;->rLineTo(FF)V

    .line 373
    iget-object v5, p0, Landroid/support/v7/b/a/c;->m:Landroid/graphics/Path;

    neg-float v6, v7

    invoke-virtual {v5, v8, v6}, Landroid/graphics/Path;->moveTo(FF)V

    .line 374
    iget-object v5, p0, Landroid/support/v7/b/a/c;->m:Landroid/graphics/Path;

    neg-float v4, v4

    invoke-virtual {v5, v2, v4}, Landroid/graphics/Path;->rLineTo(FF)V

    .line 376
    iget-object v2, p0, Landroid/support/v7/b/a/c;->m:Landroid/graphics/Path;

    invoke-virtual {v2}, Landroid/graphics/Path;->close()V

    .line 378
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 382
    iget-object v2, p0, Landroid/support/v7/b/a/c;->f:Landroid/graphics/Paint;

    invoke-virtual {v2}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v2

    .line 383
    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v4

    int-to-float v4, v4

    const/high16 v5, 0x40400000    # 3.0f

    mul-float/2addr v5, v2

    sub-float/2addr v4, v5

    iget v5, p0, Landroid/support/v7/b/a/c;->k:F

    const/high16 v6, 0x40000000    # 2.0f

    mul-float/2addr v5, v6

    sub-float/2addr v4, v5

    float-to-int v4, v4

    .line 384
    div-int/lit8 v4, v4, 0x4

    mul-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    .line 385
    float-to-double v4, v4

    float-to-double v6, v2

    const-wide/high16 v8, 0x3ff8000000000000L    # 1.5

    mul-double/2addr v6, v8

    iget v2, p0, Landroid/support/v7/b/a/c;->k:F

    float-to-double v8, v2

    add-double/2addr v6, v8

    add-double/2addr v4, v6

    double-to-float v2, v4

    .line 387
    invoke-virtual {v3}, Landroid/graphics/Rect;->centerX()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {p1, v3, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 388
    iget-boolean v2, p0, Landroid/support/v7/b/a/c;->l:Z

    if-eqz v2, :cond_11f

    .line 389
    iget-boolean v2, p0, Landroid/support/v7/b/a/c;->o:Z

    xor-int/2addr v0, v2

    if-eqz v0, :cond_11d

    const/4 v0, -0x1

    :goto_f0
    int-to-float v0, v0

    mul-float/2addr v0, v1

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->rotate(F)V

    .line 393
    :cond_f5
    :goto_f5
    iget-object v0, p0, Landroid/support/v7/b/a/c;->m:Landroid/graphics/Path;

    iget-object v1, p0, Landroid/support/v7/b/a/c;->f:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 395
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 396
    return-void

    .line 326
    :pswitch_100
    const/4 v0, 0x0

    .line 327
    goto/16 :goto_11

    .line 329
    :pswitch_103
    const/4 v0, 0x1

    .line 330
    goto/16 :goto_11

    .line 332
    :pswitch_106
    invoke-static {p0}, Landroid/support/v4/e/a/a;->e(Landroid/graphics/drawable/Drawable;)I

    move-result v0

    if-nez v0, :cond_10f

    const/4 v0, 0x1

    goto/16 :goto_11

    :cond_10f
    const/4 v0, 0x0

    goto/16 :goto_11

    .line 337
    :cond_112
    const/4 v0, 0x0

    goto/16 :goto_11

    .line 353
    :cond_115
    const/high16 v1, -0x3ccc0000    # -180.0f

    move v2, v1

    goto/16 :goto_4d

    :cond_11a
    const/4 v1, 0x0

    goto/16 :goto_51

    .line 389
    :cond_11d
    const/4 v0, 0x1

    goto :goto_f0

    .line 390
    :cond_11f
    if-eqz v0, :cond_f5

    .line 391
    const/high16 v0, 0x43340000    # 180.0f

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->rotate(F)V

    goto :goto_f5

    .line 324
    nop

    :pswitch_data_128
    .packed-switch 0x0
        :pswitch_100
        :pswitch_103
        :pswitch_9
        :pswitch_106
    .end packed-switch
.end method

.method public getIntrinsicHeight()I
    .registers 2

    .prologue
    .line 414
    iget v0, p0, Landroid/support/v7/b/a/c;->n:I

    return v0
.end method

.method public getIntrinsicWidth()I
    .registers 2

    .prologue
    .line 419
    iget v0, p0, Landroid/support/v7/b/a/c;->n:I

    return v0
.end method

.method public getOpacity()I
    .registers 2

    .prologue
    .line 424
    const/4 v0, -0x3

    return v0
.end method

.method public setAlpha(I)V
    .registers 3

    .prologue
    .line 400
    iget-object v0, p0, Landroid/support/v7/b/a/c;->f:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getAlpha()I

    move-result v0

    if-eq p1, v0, :cond_10

    .line 401
    iget-object v0, p0, Landroid/support/v7/b/a/c;->f:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 402
    invoke-virtual {p0}, Landroid/support/v7/b/a/c;->invalidateSelf()V

    .line 404
    :cond_10
    return-void
.end method

.method public setColorFilter(Landroid/graphics/ColorFilter;)V
    .registers 3

    .prologue
    .line 408
    iget-object v0, p0, Landroid/support/v7/b/a/c;->f:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 409
    invoke-virtual {p0}, Landroid/support/v7/b/a/c;->invalidateSelf()V

    .line 410
    return-void
.end method
