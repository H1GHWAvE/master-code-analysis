.class public final Landroid/support/v7/b/a/a;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(I)Landroid/graphics/PorterDuff$Mode;
    .registers 4

    .prologue
    const/4 v0, 0x0

    .line 28
    packed-switch p0, :pswitch_data_22

    .line 36
    :cond_4
    :goto_4
    :pswitch_4
    return-object v0

    .line 29
    :pswitch_5
    sget-object v0, Landroid/graphics/PorterDuff$Mode;->SRC_OVER:Landroid/graphics/PorterDuff$Mode;

    goto :goto_4

    .line 30
    :pswitch_8
    sget-object v0, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    goto :goto_4

    .line 31
    :pswitch_b
    sget-object v0, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    goto :goto_4

    .line 32
    :pswitch_e
    sget-object v0, Landroid/graphics/PorterDuff$Mode;->MULTIPLY:Landroid/graphics/PorterDuff$Mode;

    goto :goto_4

    .line 33
    :pswitch_11
    sget-object v0, Landroid/graphics/PorterDuff$Mode;->SCREEN:Landroid/graphics/PorterDuff$Mode;

    goto :goto_4

    .line 34
    :pswitch_14
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-lt v1, v2, :cond_4

    const-string v0, "ADD"

    invoke-static {v0}, Landroid/graphics/PorterDuff$Mode;->valueOf(Ljava/lang/String;)Landroid/graphics/PorterDuff$Mode;

    move-result-object v0

    goto :goto_4

    .line 28
    nop

    :pswitch_data_22
    .packed-switch 0x3
        :pswitch_5
        :pswitch_4
        :pswitch_8
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_b
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_e
        :pswitch_11
        :pswitch_14
    .end packed-switch
.end method
