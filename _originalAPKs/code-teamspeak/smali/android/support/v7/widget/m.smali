.class public final Landroid/support/v7/widget/m;
.super Landroid/support/v7/widget/al;
.source "SourceFile"


# instance fields
.field public a:Z
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
    .end annotation
.end field

.field public b:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
    .end annotation
.end field

.field public c:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
    .end annotation
.end field

.field public d:Z
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
    .end annotation
.end field

.field public e:Z
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
    .end annotation
.end field

.field f:Z


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    const/4 v0, -0x2

    .line 823
    invoke-direct {p0, v0, v0}, Landroid/support/v7/widget/al;-><init>(II)V

    .line 824
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v7/widget/m;->a:Z

    .line 825
    return-void
.end method

.method private constructor <init>(IIZ)V
    .registers 4

    .prologue
    .line 828
    invoke-direct {p0, p1, p2}, Landroid/support/v7/widget/al;-><init>(II)V

    .line 829
    iput-boolean p3, p0, Landroid/support/v7/widget/m;->a:Z

    .line 830
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 3

    .prologue
    .line 810
    invoke-direct {p0, p1, p2}, Landroid/support/v7/widget/al;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 811
    return-void
.end method

.method public constructor <init>(Landroid/support/v7/widget/m;)V
    .registers 3

    .prologue
    .line 818
    invoke-direct {p0, p1}, Landroid/support/v7/widget/al;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 819
    iget-boolean v0, p1, Landroid/support/v7/widget/m;->a:Z

    iput-boolean v0, p0, Landroid/support/v7/widget/m;->a:Z

    .line 820
    return-void
.end method

.method public constructor <init>(Landroid/view/ViewGroup$LayoutParams;)V
    .registers 2

    .prologue
    .line 814
    invoke-direct {p0, p1}, Landroid/support/v7/widget/al;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 815
    return-void
.end method
