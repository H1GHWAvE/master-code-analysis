.class public Landroid/support/v7/widget/p;
.super Landroid/widget/AutoCompleteTextView;
.source "SourceFile"

# interfaces
.implements Landroid/support/v4/view/cr;


# static fields
.field private static final a:[I


# instance fields
.field private b:Landroid/support/v7/internal/widget/av;

.field private c:Landroid/support/v7/widget/q;

.field private d:Landroid/support/v7/widget/ah;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    .line 51
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x1010176

    aput v2, v0, v1

    sput-object v0, Landroid/support/v7/widget/p;->a:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 3

    .prologue
    .line 60
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/support/v7/widget/p;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 61
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4

    .prologue
    .line 64
    sget v0, Landroid/support/v7/a/d;->autoCompleteTextViewStyle:I

    invoke-direct {p0, p1, p2, v0}, Landroid/support/v7/widget/p;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 65
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 7

    .prologue
    const/4 v2, 0x0

    .line 68
    invoke-static {p1}, Landroid/support/v7/internal/widget/as;->a(Landroid/content/Context;)Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0, p2, p3}, Landroid/widget/AutoCompleteTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 70
    invoke-virtual {p0}, Landroid/support/v7/widget/p;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Landroid/support/v7/widget/p;->a:[I

    invoke-static {v0, p2, v1, p3}, Landroid/support/v7/internal/widget/ax;->a(Landroid/content/Context;Landroid/util/AttributeSet;[II)Landroid/support/v7/internal/widget/ax;

    move-result-object v0

    .line 72
    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ax;->a()Landroid/support/v7/internal/widget/av;

    move-result-object v1

    iput-object v1, p0, Landroid/support/v7/widget/p;->b:Landroid/support/v7/internal/widget/av;

    .line 73
    invoke-virtual {v0, v2}, Landroid/support/v7/internal/widget/ax;->e(I)Z

    move-result v1

    if-eqz v1, :cond_25

    .line 74
    invoke-virtual {v0, v2}, Landroid/support/v7/internal/widget/ax;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {p0, v1}, Landroid/support/v7/widget/p;->setDropDownBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1183
    :cond_25
    iget-object v0, v0, Landroid/support/v7/internal/widget/ax;->a:Landroid/content/res/TypedArray;

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 78
    new-instance v0, Landroid/support/v7/widget/q;

    iget-object v1, p0, Landroid/support/v7/widget/p;->b:Landroid/support/v7/internal/widget/av;

    invoke-direct {v0, p0, v1}, Landroid/support/v7/widget/q;-><init>(Landroid/view/View;Landroid/support/v7/internal/widget/av;)V

    iput-object v0, p0, Landroid/support/v7/widget/p;->c:Landroid/support/v7/widget/q;

    .line 79
    iget-object v0, p0, Landroid/support/v7/widget/p;->c:Landroid/support/v7/widget/q;

    invoke-virtual {v0, p2, p3}, Landroid/support/v7/widget/q;->a(Landroid/util/AttributeSet;I)V

    .line 81
    new-instance v0, Landroid/support/v7/widget/ah;

    invoke-direct {v0, p0}, Landroid/support/v7/widget/ah;-><init>(Landroid/widget/TextView;)V

    iput-object v0, p0, Landroid/support/v7/widget/p;->d:Landroid/support/v7/widget/ah;

    .line 82
    iget-object v0, p0, Landroid/support/v7/widget/p;->d:Landroid/support/v7/widget/ah;

    invoke-virtual {v0, p2, p3}, Landroid/support/v7/widget/ah;->a(Landroid/util/AttributeSet;I)V

    .line 83
    return-void
.end method


# virtual methods
.method protected drawableStateChanged()V
    .registers 2

    .prologue
    .line 164
    invoke-super {p0}, Landroid/widget/AutoCompleteTextView;->drawableStateChanged()V

    .line 165
    iget-object v0, p0, Landroid/support/v7/widget/p;->c:Landroid/support/v7/widget/q;

    if-eqz v0, :cond_c

    .line 166
    iget-object v0, p0, Landroid/support/v7/widget/p;->c:Landroid/support/v7/widget/q;

    invoke-virtual {v0}, Landroid/support/v7/widget/q;->c()V

    .line 168
    :cond_c
    return-void
.end method

.method public getSupportBackgroundTintList()Landroid/content/res/ColorStateList;
    .registers 2
    .annotation build Landroid/support/a/z;
    .end annotation

    .prologue
    .line 132
    iget-object v0, p0, Landroid/support/v7/widget/p;->c:Landroid/support/v7/widget/q;

    if-eqz v0, :cond_b

    iget-object v0, p0, Landroid/support/v7/widget/p;->c:Landroid/support/v7/widget/q;

    invoke-virtual {v0}, Landroid/support/v7/widget/q;->a()Landroid/content/res/ColorStateList;

    move-result-object v0

    :goto_a
    return-object v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public getSupportBackgroundTintMode()Landroid/graphics/PorterDuff$Mode;
    .registers 2
    .annotation build Landroid/support/a/z;
    .end annotation

    .prologue
    .line 158
    iget-object v0, p0, Landroid/support/v7/widget/p;->c:Landroid/support/v7/widget/q;

    if-eqz v0, :cond_b

    iget-object v0, p0, Landroid/support/v7/widget/p;->c:Landroid/support/v7/widget/q;

    invoke-virtual {v0}, Landroid/support/v7/widget/q;->b()Landroid/graphics/PorterDuff$Mode;

    move-result-object v0

    :goto_a
    return-object v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
    .registers 4

    .prologue
    .line 104
    invoke-super {p0, p1}, Landroid/widget/AutoCompleteTextView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 105
    iget-object v0, p0, Landroid/support/v7/widget/p;->c:Landroid/support/v7/widget/q;

    if-eqz v0, :cond_d

    .line 106
    iget-object v0, p0, Landroid/support/v7/widget/p;->c:Landroid/support/v7/widget/q;

    .line 3077
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/q;->b(Landroid/content/res/ColorStateList;)V

    .line 108
    :cond_d
    return-void
.end method

.method public setBackgroundResource(I)V
    .registers 3
    .param p1    # I
        .annotation build Landroid/support/a/m;
        .end annotation
    .end param

    .prologue
    .line 96
    invoke-super {p0, p1}, Landroid/widget/AutoCompleteTextView;->setBackgroundResource(I)V

    .line 97
    iget-object v0, p0, Landroid/support/v7/widget/p;->c:Landroid/support/v7/widget/q;

    if-eqz v0, :cond_c

    .line 98
    iget-object v0, p0, Landroid/support/v7/widget/p;->c:Landroid/support/v7/widget/q;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/q;->a(I)V

    .line 100
    :cond_c
    return-void
.end method

.method public setDropDownBackgroundResource(I)V
    .registers 4
    .param p1    # I
        .annotation build Landroid/support/a/m;
        .end annotation
    .end param

    .prologue
    .line 87
    iget-object v0, p0, Landroid/support/v7/widget/p;->b:Landroid/support/v7/internal/widget/av;

    if-eqz v0, :cond_f

    .line 88
    iget-object v0, p0, Landroid/support/v7/widget/p;->b:Landroid/support/v7/internal/widget/av;

    .line 2167
    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/support/v7/internal/widget/av;->a(IZ)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 88
    invoke-virtual {p0, v0}, Landroid/support/v7/widget/p;->setDropDownBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 92
    :goto_e
    return-void

    .line 90
    :cond_f
    invoke-super {p0, p1}, Landroid/widget/AutoCompleteTextView;->setDropDownBackgroundResource(I)V

    goto :goto_e
.end method

.method public setSupportBackgroundTintList(Landroid/content/res/ColorStateList;)V
    .registers 3
    .param p1    # Landroid/content/res/ColorStateList;
        .annotation build Landroid/support/a/z;
        .end annotation
    .end param

    .prologue
    .line 118
    iget-object v0, p0, Landroid/support/v7/widget/p;->c:Landroid/support/v7/widget/q;

    if-eqz v0, :cond_9

    .line 119
    iget-object v0, p0, Landroid/support/v7/widget/p;->c:Landroid/support/v7/widget/q;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/q;->a(Landroid/content/res/ColorStateList;)V

    .line 121
    :cond_9
    return-void
.end method

.method public setSupportBackgroundTintMode(Landroid/graphics/PorterDuff$Mode;)V
    .registers 3
    .param p1    # Landroid/graphics/PorterDuff$Mode;
        .annotation build Landroid/support/a/z;
        .end annotation
    .end param

    .prologue
    .line 144
    iget-object v0, p0, Landroid/support/v7/widget/p;->c:Landroid/support/v7/widget/q;

    if-eqz v0, :cond_9

    .line 145
    iget-object v0, p0, Landroid/support/v7/widget/p;->c:Landroid/support/v7/widget/q;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/q;->a(Landroid/graphics/PorterDuff$Mode;)V

    .line 147
    :cond_9
    return-void
.end method

.method public setTextAppearance(Landroid/content/Context;I)V
    .registers 4

    .prologue
    .line 172
    invoke-super {p0, p1, p2}, Landroid/widget/AutoCompleteTextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 173
    iget-object v0, p0, Landroid/support/v7/widget/p;->d:Landroid/support/v7/widget/ah;

    if-eqz v0, :cond_c

    .line 174
    iget-object v0, p0, Landroid/support/v7/widget/p;->d:Landroid/support/v7/widget/ah;

    invoke-virtual {v0, p1, p2}, Landroid/support/v7/widget/ah;->a(Landroid/content/Context;I)V

    .line 176
    :cond_c
    return-void
.end method
