.class public final Landroid/support/v7/widget/w;
.super Landroid/widget/EditText;
.source "SourceFile"

# interfaces
.implements Landroid/support/v4/view/cr;


# instance fields
.field private a:Landroid/support/v7/internal/widget/av;

.field private b:Landroid/support/v7/widget/q;

.field private c:Landroid/support/v7/widget/ah;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .registers 3

    .prologue
    .line 54
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/support/v7/widget/w;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 55
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4

    .prologue
    .line 58
    sget v0, Landroid/support/v7/a/d;->editTextStyle:I

    invoke-direct {p0, p1, p2, v0}, Landroid/support/v7/widget/w;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 59
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 6

    .prologue
    .line 62
    invoke-static {p1}, Landroid/support/v7/internal/widget/as;->a(Landroid/content/Context;)Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0, p2, p3}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 64
    invoke-virtual {p0}, Landroid/support/v7/widget/w;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v7/internal/widget/av;->a(Landroid/content/Context;)Landroid/support/v7/internal/widget/av;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/widget/w;->a:Landroid/support/v7/internal/widget/av;

    .line 65
    new-instance v0, Landroid/support/v7/widget/q;

    iget-object v1, p0, Landroid/support/v7/widget/w;->a:Landroid/support/v7/internal/widget/av;

    invoke-direct {v0, p0, v1}, Landroid/support/v7/widget/q;-><init>(Landroid/view/View;Landroid/support/v7/internal/widget/av;)V

    iput-object v0, p0, Landroid/support/v7/widget/w;->b:Landroid/support/v7/widget/q;

    .line 66
    iget-object v0, p0, Landroid/support/v7/widget/w;->b:Landroid/support/v7/widget/q;

    invoke-virtual {v0, p2, p3}, Landroid/support/v7/widget/q;->a(Landroid/util/AttributeSet;I)V

    .line 68
    new-instance v0, Landroid/support/v7/widget/ah;

    invoke-direct {v0, p0}, Landroid/support/v7/widget/ah;-><init>(Landroid/widget/TextView;)V

    iput-object v0, p0, Landroid/support/v7/widget/w;->c:Landroid/support/v7/widget/ah;

    .line 69
    iget-object v0, p0, Landroid/support/v7/widget/w;->c:Landroid/support/v7/widget/ah;

    invoke-virtual {v0, p2, p3}, Landroid/support/v7/widget/ah;->a(Landroid/util/AttributeSet;I)V

    .line 70
    return-void
.end method


# virtual methods
.method protected final drawableStateChanged()V
    .registers 2

    .prologue
    .line 142
    invoke-super {p0}, Landroid/widget/EditText;->drawableStateChanged()V

    .line 143
    iget-object v0, p0, Landroid/support/v7/widget/w;->b:Landroid/support/v7/widget/q;

    if-eqz v0, :cond_c

    .line 144
    iget-object v0, p0, Landroid/support/v7/widget/w;->b:Landroid/support/v7/widget/q;

    invoke-virtual {v0}, Landroid/support/v7/widget/q;->c()V

    .line 146
    :cond_c
    return-void
.end method

.method public final getSupportBackgroundTintList()Landroid/content/res/ColorStateList;
    .registers 2
    .annotation build Landroid/support/a/z;
    .end annotation

    .prologue
    .line 110
    iget-object v0, p0, Landroid/support/v7/widget/w;->b:Landroid/support/v7/widget/q;

    if-eqz v0, :cond_b

    iget-object v0, p0, Landroid/support/v7/widget/w;->b:Landroid/support/v7/widget/q;

    invoke-virtual {v0}, Landroid/support/v7/widget/q;->a()Landroid/content/res/ColorStateList;

    move-result-object v0

    :goto_a
    return-object v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public final getSupportBackgroundTintMode()Landroid/graphics/PorterDuff$Mode;
    .registers 2
    .annotation build Landroid/support/a/z;
    .end annotation

    .prologue
    .line 136
    iget-object v0, p0, Landroid/support/v7/widget/w;->b:Landroid/support/v7/widget/q;

    if-eqz v0, :cond_b

    iget-object v0, p0, Landroid/support/v7/widget/w;->b:Landroid/support/v7/widget/q;

    invoke-virtual {v0}, Landroid/support/v7/widget/q;->b()Landroid/graphics/PorterDuff$Mode;

    move-result-object v0

    :goto_a
    return-object v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public final setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
    .registers 4

    .prologue
    .line 82
    invoke-super {p0, p1}, Landroid/widget/EditText;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 83
    iget-object v0, p0, Landroid/support/v7/widget/w;->b:Landroid/support/v7/widget/q;

    if-eqz v0, :cond_d

    .line 84
    iget-object v0, p0, Landroid/support/v7/widget/w;->b:Landroid/support/v7/widget/q;

    .line 1077
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/q;->b(Landroid/content/res/ColorStateList;)V

    .line 86
    :cond_d
    return-void
.end method

.method public final setBackgroundResource(I)V
    .registers 3
    .param p1    # I
        .annotation build Landroid/support/a/m;
        .end annotation
    .end param

    .prologue
    .line 74
    invoke-super {p0, p1}, Landroid/widget/EditText;->setBackgroundResource(I)V

    .line 75
    iget-object v0, p0, Landroid/support/v7/widget/w;->b:Landroid/support/v7/widget/q;

    if-eqz v0, :cond_c

    .line 76
    iget-object v0, p0, Landroid/support/v7/widget/w;->b:Landroid/support/v7/widget/q;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/q;->a(I)V

    .line 78
    :cond_c
    return-void
.end method

.method public final setSupportBackgroundTintList(Landroid/content/res/ColorStateList;)V
    .registers 3
    .param p1    # Landroid/content/res/ColorStateList;
        .annotation build Landroid/support/a/z;
        .end annotation
    .end param

    .prologue
    .line 96
    iget-object v0, p0, Landroid/support/v7/widget/w;->b:Landroid/support/v7/widget/q;

    if-eqz v0, :cond_9

    .line 97
    iget-object v0, p0, Landroid/support/v7/widget/w;->b:Landroid/support/v7/widget/q;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/q;->a(Landroid/content/res/ColorStateList;)V

    .line 99
    :cond_9
    return-void
.end method

.method public final setSupportBackgroundTintMode(Landroid/graphics/PorterDuff$Mode;)V
    .registers 3
    .param p1    # Landroid/graphics/PorterDuff$Mode;
        .annotation build Landroid/support/a/z;
        .end annotation
    .end param

    .prologue
    .line 122
    iget-object v0, p0, Landroid/support/v7/widget/w;->b:Landroid/support/v7/widget/q;

    if-eqz v0, :cond_9

    .line 123
    iget-object v0, p0, Landroid/support/v7/widget/w;->b:Landroid/support/v7/widget/q;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/q;->a(Landroid/graphics/PorterDuff$Mode;)V

    .line 125
    :cond_9
    return-void
.end method

.method public final setTextAppearance(Landroid/content/Context;I)V
    .registers 4

    .prologue
    .line 150
    invoke-super {p0, p1, p2}, Landroid/widget/EditText;->setTextAppearance(Landroid/content/Context;I)V

    .line 151
    iget-object v0, p0, Landroid/support/v7/widget/w;->c:Landroid/support/v7/widget/ah;

    if-eqz v0, :cond_c

    .line 152
    iget-object v0, p0, Landroid/support/v7/widget/w;->c:Landroid/support/v7/widget/ah;

    invoke-virtual {v0, p1, p2}, Landroid/support/v7/widget/ah;->a(Landroid/content/Context;I)V

    .line 154
    :cond_c
    return-void
.end method
