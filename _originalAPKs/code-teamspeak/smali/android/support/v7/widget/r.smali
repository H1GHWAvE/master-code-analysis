.class public final Landroid/support/v7/widget/r;
.super Landroid/widget/Button;
.source "SourceFile"

# interfaces
.implements Landroid/support/v4/view/cr;


# instance fields
.field private final a:Landroid/support/v7/internal/widget/av;

.field private final b:Landroid/support/v7/widget/q;

.field private final c:Landroid/support/v7/widget/ah;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .registers 3

    .prologue
    .line 56
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/support/v7/widget/r;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 57
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4

    .prologue
    .line 60
    sget v0, Landroid/support/v7/a/d;->buttonStyle:I

    invoke-direct {p0, p1, p2, v0}, Landroid/support/v7/widget/r;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 61
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 6

    .prologue
    .line 64
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/Button;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 66
    invoke-virtual {p0}, Landroid/support/v7/widget/r;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v7/internal/widget/av;->a(Landroid/content/Context;)Landroid/support/v7/internal/widget/av;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/widget/r;->a:Landroid/support/v7/internal/widget/av;

    .line 67
    new-instance v0, Landroid/support/v7/widget/q;

    iget-object v1, p0, Landroid/support/v7/widget/r;->a:Landroid/support/v7/internal/widget/av;

    invoke-direct {v0, p0, v1}, Landroid/support/v7/widget/q;-><init>(Landroid/view/View;Landroid/support/v7/internal/widget/av;)V

    iput-object v0, p0, Landroid/support/v7/widget/r;->b:Landroid/support/v7/widget/q;

    .line 68
    iget-object v0, p0, Landroid/support/v7/widget/r;->b:Landroid/support/v7/widget/q;

    invoke-virtual {v0, p2, p3}, Landroid/support/v7/widget/q;->a(Landroid/util/AttributeSet;I)V

    .line 70
    new-instance v0, Landroid/support/v7/widget/ah;

    invoke-direct {v0, p0}, Landroid/support/v7/widget/ah;-><init>(Landroid/widget/TextView;)V

    iput-object v0, p0, Landroid/support/v7/widget/r;->c:Landroid/support/v7/widget/ah;

    .line 71
    iget-object v0, p0, Landroid/support/v7/widget/r;->c:Landroid/support/v7/widget/ah;

    invoke-virtual {v0, p2, p3}, Landroid/support/v7/widget/ah;->a(Landroid/util/AttributeSet;I)V

    .line 72
    return-void
.end method


# virtual methods
.method protected final drawableStateChanged()V
    .registers 2

    .prologue
    .line 144
    invoke-super {p0}, Landroid/widget/Button;->drawableStateChanged()V

    .line 145
    iget-object v0, p0, Landroid/support/v7/widget/r;->b:Landroid/support/v7/widget/q;

    if-eqz v0, :cond_c

    .line 146
    iget-object v0, p0, Landroid/support/v7/widget/r;->b:Landroid/support/v7/widget/q;

    invoke-virtual {v0}, Landroid/support/v7/widget/q;->c()V

    .line 148
    :cond_c
    return-void
.end method

.method public final getSupportBackgroundTintList()Landroid/content/res/ColorStateList;
    .registers 2
    .annotation build Landroid/support/a/z;
    .end annotation

    .prologue
    .line 112
    iget-object v0, p0, Landroid/support/v7/widget/r;->b:Landroid/support/v7/widget/q;

    if-eqz v0, :cond_b

    iget-object v0, p0, Landroid/support/v7/widget/r;->b:Landroid/support/v7/widget/q;

    invoke-virtual {v0}, Landroid/support/v7/widget/q;->a()Landroid/content/res/ColorStateList;

    move-result-object v0

    :goto_a
    return-object v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public final getSupportBackgroundTintMode()Landroid/graphics/PorterDuff$Mode;
    .registers 2
    .annotation build Landroid/support/a/z;
    .end annotation

    .prologue
    .line 138
    iget-object v0, p0, Landroid/support/v7/widget/r;->b:Landroid/support/v7/widget/q;

    if-eqz v0, :cond_b

    iget-object v0, p0, Landroid/support/v7/widget/r;->b:Landroid/support/v7/widget/q;

    invoke-virtual {v0}, Landroid/support/v7/widget/q;->b()Landroid/graphics/PorterDuff$Mode;

    move-result-object v0

    :goto_a
    return-object v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public final onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .registers 3

    .prologue
    .line 160
    invoke-super {p0, p1}, Landroid/widget/Button;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 161
    const-class v0, Landroid/widget/Button;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    .line 162
    return-void
.end method

.method public final onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .registers 3

    .prologue
    .line 166
    invoke-super {p0, p1}, Landroid/widget/Button;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    .line 167
    const-class v0, Landroid/widget/Button;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    .line 168
    return-void
.end method

.method public final setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
    .registers 4

    .prologue
    .line 84
    invoke-super {p0, p1}, Landroid/widget/Button;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 85
    iget-object v0, p0, Landroid/support/v7/widget/r;->b:Landroid/support/v7/widget/q;

    if-eqz v0, :cond_d

    .line 86
    iget-object v0, p0, Landroid/support/v7/widget/r;->b:Landroid/support/v7/widget/q;

    .line 1077
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/q;->b(Landroid/content/res/ColorStateList;)V

    .line 88
    :cond_d
    return-void
.end method

.method public final setBackgroundResource(I)V
    .registers 3
    .param p1    # I
        .annotation build Landroid/support/a/m;
        .end annotation
    .end param

    .prologue
    .line 76
    invoke-super {p0, p1}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 77
    iget-object v0, p0, Landroid/support/v7/widget/r;->b:Landroid/support/v7/widget/q;

    if-eqz v0, :cond_c

    .line 78
    iget-object v0, p0, Landroid/support/v7/widget/r;->b:Landroid/support/v7/widget/q;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/q;->a(I)V

    .line 80
    :cond_c
    return-void
.end method

.method public final setSupportAllCaps(Z)V
    .registers 3

    .prologue
    .line 180
    iget-object v0, p0, Landroid/support/v7/widget/r;->c:Landroid/support/v7/widget/ah;

    if-eqz v0, :cond_9

    .line 181
    iget-object v0, p0, Landroid/support/v7/widget/r;->c:Landroid/support/v7/widget/ah;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/ah;->a(Z)V

    .line 183
    :cond_9
    return-void
.end method

.method public final setSupportBackgroundTintList(Landroid/content/res/ColorStateList;)V
    .registers 3
    .param p1    # Landroid/content/res/ColorStateList;
        .annotation build Landroid/support/a/z;
        .end annotation
    .end param

    .prologue
    .line 98
    iget-object v0, p0, Landroid/support/v7/widget/r;->b:Landroid/support/v7/widget/q;

    if-eqz v0, :cond_9

    .line 99
    iget-object v0, p0, Landroid/support/v7/widget/r;->b:Landroid/support/v7/widget/q;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/q;->a(Landroid/content/res/ColorStateList;)V

    .line 101
    :cond_9
    return-void
.end method

.method public final setSupportBackgroundTintMode(Landroid/graphics/PorterDuff$Mode;)V
    .registers 3
    .param p1    # Landroid/graphics/PorterDuff$Mode;
        .annotation build Landroid/support/a/z;
        .end annotation
    .end param

    .prologue
    .line 124
    iget-object v0, p0, Landroid/support/v7/widget/r;->b:Landroid/support/v7/widget/q;

    if-eqz v0, :cond_9

    .line 125
    iget-object v0, p0, Landroid/support/v7/widget/r;->b:Landroid/support/v7/widget/q;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/q;->a(Landroid/graphics/PorterDuff$Mode;)V

    .line 127
    :cond_9
    return-void
.end method

.method public final setTextAppearance(Landroid/content/Context;I)V
    .registers 4

    .prologue
    .line 152
    invoke-super {p0, p1, p2}, Landroid/widget/Button;->setTextAppearance(Landroid/content/Context;I)V

    .line 153
    iget-object v0, p0, Landroid/support/v7/widget/r;->c:Landroid/support/v7/widget/ah;

    if-eqz v0, :cond_c

    .line 154
    iget-object v0, p0, Landroid/support/v7/widget/r;->c:Landroid/support/v7/widget/ah;

    invoke-virtual {v0, p1, p2}, Landroid/support/v7/widget/ah;->a(Landroid/content/Context;I)V

    .line 156
    :cond_c
    return-void
.end method
