.class public Landroid/support/v7/widget/an;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/lang/String; = "ListPopupWindow"

.field private static final b:Z = false

.field public static final n:I = 0x0

.field public static final o:I = 0x1

.field public static final p:I = -0x1

.field public static final q:I = -0x2

.field public static final r:I = 0x0

.field public static final s:I = 0x1

.field public static final t:I = 0x2

.field private static final u:I = 0xfa

.field private static v:Ljava/lang/reflect/Method;


# instance fields
.field private A:Z

.field private B:Landroid/view/View;

.field private C:Landroid/database/DataSetObserver;

.field private D:Landroid/graphics/drawable/Drawable;

.field private E:Landroid/widget/AdapterView$OnItemSelectedListener;

.field private final F:Landroid/support/v7/widget/az;

.field private final G:Landroid/support/v7/widget/ay;

.field private final H:Landroid/support/v7/widget/ax;

.field private final I:Landroid/support/v7/widget/av;

.field private J:Ljava/lang/Runnable;

.field private K:Landroid/os/Handler;

.field private L:Landroid/graphics/Rect;

.field private M:Z

.field private N:I

.field public c:Landroid/widget/PopupWindow;

.field public d:Landroid/support/v7/widget/ar;

.field e:I

.field f:I

.field g:I

.field h:Z

.field public i:I

.field j:I

.field k:I

.field public l:Landroid/view/View;

.field public m:Landroid/widget/AdapterView$OnItemClickListener;

.field private w:Landroid/content/Context;

.field private x:Landroid/widget/ListAdapter;

.field private y:I

.field private z:Z


# direct methods
.method static constructor <clinit>()V
    .registers 5

    .prologue
    .line 80
    :try_start_0
    const-class v0, Landroid/widget/PopupWindow;

    const-string v1, "setClipToScreenEnabled"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    sget-object v4, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Landroid/support/v7/widget/an;->v:Ljava/lang/reflect/Method;
    :try_end_12
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_12} :catch_13

    .line 85
    :goto_12
    return-void

    .line 83
    :catch_13
    move-exception v0

    const-string v0, "ListPopupWindow"

    const-string v1, "Could not find method setClipToScreenEnabled() on PopupWindow. Oh well."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_12
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 4

    .prologue
    .line 192
    const/4 v0, 0x0

    sget v1, Landroid/support/v7/a/d;->listPopupWindowStyle:I

    invoke-direct {p0, p1, v0, v1}, Landroid/support/v7/widget/an;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 193
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4

    .prologue
    .line 203
    sget v0, Landroid/support/v7/a/d;->listPopupWindowStyle:I

    invoke-direct {p0, p1, p2, v0}, Landroid/support/v7/widget/an;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 204
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 5

    .prologue
    .line 215
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Landroid/support/v7/widget/an;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 216
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .registers 9

    .prologue
    const/4 v3, 0x1

    const/4 v0, -0x2

    const/4 v2, 0x0

    .line 227
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 92
    iput v0, p0, Landroid/support/v7/widget/an;->y:I

    .line 93
    iput v0, p0, Landroid/support/v7/widget/an;->e:I

    .line 98
    iput v2, p0, Landroid/support/v7/widget/an;->i:I

    .line 100
    iput-boolean v2, p0, Landroid/support/v7/widget/an;->z:Z

    .line 101
    iput-boolean v2, p0, Landroid/support/v7/widget/an;->A:Z

    .line 102
    const v0, 0x7fffffff

    iput v0, p0, Landroid/support/v7/widget/an;->j:I

    .line 105
    iput v2, p0, Landroid/support/v7/widget/an;->k:I

    .line 116
    new-instance v0, Landroid/support/v7/widget/az;

    invoke-direct {v0, p0, v2}, Landroid/support/v7/widget/az;-><init>(Landroid/support/v7/widget/an;B)V

    iput-object v0, p0, Landroid/support/v7/widget/an;->F:Landroid/support/v7/widget/az;

    .line 117
    new-instance v0, Landroid/support/v7/widget/ay;

    invoke-direct {v0, p0, v2}, Landroid/support/v7/widget/ay;-><init>(Landroid/support/v7/widget/an;B)V

    iput-object v0, p0, Landroid/support/v7/widget/an;->G:Landroid/support/v7/widget/ay;

    .line 118
    new-instance v0, Landroid/support/v7/widget/ax;

    invoke-direct {v0, p0, v2}, Landroid/support/v7/widget/ax;-><init>(Landroid/support/v7/widget/an;B)V

    iput-object v0, p0, Landroid/support/v7/widget/an;->H:Landroid/support/v7/widget/ax;

    .line 119
    new-instance v0, Landroid/support/v7/widget/av;

    invoke-direct {v0, p0, v2}, Landroid/support/v7/widget/av;-><init>(Landroid/support/v7/widget/an;B)V

    iput-object v0, p0, Landroid/support/v7/widget/an;->I:Landroid/support/v7/widget/av;

    .line 122
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/an;->K:Landroid/os/Handler;

    .line 124
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/an;->L:Landroid/graphics/Rect;

    .line 228
    iput-object p1, p0, Landroid/support/v7/widget/an;->w:Landroid/content/Context;

    .line 230
    sget-object v0, Landroid/support/v7/a/n;->ListPopupWindow:[I

    invoke-virtual {p1, p2, v0, p3, p4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 232
    sget v1, Landroid/support/v7/a/n;->ListPopupWindow_android_dropDownHorizontalOffset:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v1

    iput v1, p0, Landroid/support/v7/widget/an;->f:I

    .line 234
    sget v1, Landroid/support/v7/a/n;->ListPopupWindow_android_dropDownVerticalOffset:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v1

    iput v1, p0, Landroid/support/v7/widget/an;->g:I

    .line 236
    iget v1, p0, Landroid/support/v7/widget/an;->g:I

    if-eqz v1, :cond_5f

    .line 237
    iput-boolean v3, p0, Landroid/support/v7/widget/an;->h:Z

    .line 239
    :cond_5f
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 241
    new-instance v0, Landroid/support/v7/internal/widget/aa;

    invoke-direct {v0, p1, p2, p3}, Landroid/support/v7/internal/widget/aa;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Landroid/support/v7/widget/an;->c:Landroid/widget/PopupWindow;

    .line 242
    iget-object v0, p0, Landroid/support/v7/widget/an;->c:Landroid/widget/PopupWindow;

    invoke-virtual {v0, v3}, Landroid/widget/PopupWindow;->setInputMethodMode(I)V

    .line 245
    iget-object v0, p0, Landroid/support/v7/widget/an;->w:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 246
    invoke-static {v0}, Landroid/support/v4/m/u;->a(Ljava/util/Locale;)I

    move-result v0

    iput v0, p0, Landroid/support/v7/widget/an;->N:I

    .line 247
    return-void
.end method

.method private A()Landroid/widget/ListView;
    .registers 2

    .prologue
    .line 845
    iget-object v0, p0, Landroid/support/v7/widget/an;->d:Landroid/support/v7/widget/ar;

    return-object v0
.end method

.method private B()I
    .registers 10

    .prologue
    const/high16 v8, 0x40000000    # 2.0f

    const/4 v7, -0x1

    const/high16 v6, -0x80000000

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1049
    .line 1051
    iget-object v0, p0, Landroid/support/v7/widget/an;->d:Landroid/support/v7/widget/ar;

    if-nez v0, :cond_fa

    .line 1052
    iget-object v4, p0, Landroid/support/v7/widget/an;->w:Landroid/content/Context;

    .line 1060
    new-instance v0, Landroid/support/v7/widget/ap;

    invoke-direct {v0, p0}, Landroid/support/v7/widget/ap;-><init>(Landroid/support/v7/widget/an;)V

    iput-object v0, p0, Landroid/support/v7/widget/an;->J:Ljava/lang/Runnable;

    .line 1070
    new-instance v3, Landroid/support/v7/widget/ar;

    iget-boolean v0, p0, Landroid/support/v7/widget/an;->M:Z

    if-nez v0, :cond_e9

    move v0, v1

    :goto_1b
    invoke-direct {v3, v4, v0}, Landroid/support/v7/widget/ar;-><init>(Landroid/content/Context;Z)V

    iput-object v3, p0, Landroid/support/v7/widget/an;->d:Landroid/support/v7/widget/ar;

    .line 1071
    iget-object v0, p0, Landroid/support/v7/widget/an;->D:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_2b

    .line 1072
    iget-object v0, p0, Landroid/support/v7/widget/an;->d:Landroid/support/v7/widget/ar;

    iget-object v3, p0, Landroid/support/v7/widget/an;->D:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/ar;->setSelector(Landroid/graphics/drawable/Drawable;)V

    .line 1074
    :cond_2b
    iget-object v0, p0, Landroid/support/v7/widget/an;->d:Landroid/support/v7/widget/ar;

    iget-object v3, p0, Landroid/support/v7/widget/an;->x:Landroid/widget/ListAdapter;

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/ar;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1075
    iget-object v0, p0, Landroid/support/v7/widget/an;->d:Landroid/support/v7/widget/ar;

    iget-object v3, p0, Landroid/support/v7/widget/an;->m:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/ar;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 1076
    iget-object v0, p0, Landroid/support/v7/widget/an;->d:Landroid/support/v7/widget/ar;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/ar;->setFocusable(Z)V

    .line 1077
    iget-object v0, p0, Landroid/support/v7/widget/an;->d:Landroid/support/v7/widget/ar;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/ar;->setFocusableInTouchMode(Z)V

    .line 1078
    iget-object v0, p0, Landroid/support/v7/widget/an;->d:Landroid/support/v7/widget/ar;

    new-instance v3, Landroid/support/v7/widget/aq;

    invoke-direct {v3, p0}, Landroid/support/v7/widget/aq;-><init>(Landroid/support/v7/widget/an;)V

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/ar;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 1094
    iget-object v0, p0, Landroid/support/v7/widget/an;->d:Landroid/support/v7/widget/ar;

    iget-object v3, p0, Landroid/support/v7/widget/an;->H:Landroid/support/v7/widget/ax;

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/ar;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 1096
    iget-object v0, p0, Landroid/support/v7/widget/an;->E:Landroid/widget/AdapterView$OnItemSelectedListener;

    if-eqz v0, :cond_5f

    .line 1097
    iget-object v0, p0, Landroid/support/v7/widget/an;->d:Landroid/support/v7/widget/ar;

    iget-object v3, p0, Landroid/support/v7/widget/an;->E:Landroid/widget/AdapterView$OnItemSelectedListener;

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/ar;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 1100
    :cond_5f
    iget-object v0, p0, Landroid/support/v7/widget/an;->d:Landroid/support/v7/widget/ar;

    .line 1102
    iget-object v5, p0, Landroid/support/v7/widget/an;->B:Landroid/view/View;

    .line 1103
    if-eqz v5, :cond_16a

    .line 1106
    new-instance v3, Landroid/widget/LinearLayout;

    invoke-direct {v3, v4}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 1107
    invoke-virtual {v3, v1}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 1109
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-direct {v1, v7, v2, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    .line 1113
    iget v4, p0, Landroid/support/v7/widget/an;->k:I

    packed-switch v4, :pswitch_data_16e

    .line 1125
    const-string v0, "ListPopupWindow"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "Invalid hint position "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, p0, Landroid/support/v7/widget/an;->k:I

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1131
    :goto_8f
    iget v0, p0, Landroid/support/v7/widget/an;->e:I

    invoke-static {v0, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 1133
    invoke-virtual {v5, v0, v2}, Landroid/view/View;->measure(II)V

    .line 1135
    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 1136
    invoke-virtual {v5}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    iget v4, v0, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    add-int/2addr v1, v4

    iget v0, v0, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    add-int/2addr v0, v1

    move-object v1, v3

    .line 1142
    :goto_a9
    iget-object v3, p0, Landroid/support/v7/widget/an;->c:Landroid/widget/PopupWindow;

    invoke-virtual {v3, v1}, Landroid/widget/PopupWindow;->setContentView(Landroid/view/View;)V

    .line 1157
    :goto_ae
    iget-object v1, p0, Landroid/support/v7/widget/an;->c:Landroid/widget/PopupWindow;

    invoke-virtual {v1}, Landroid/widget/PopupWindow;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 1158
    if-eqz v1, :cond_114

    .line 1159
    iget-object v2, p0, Landroid/support/v7/widget/an;->L:Landroid/graphics/Rect;

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    .line 1160
    iget-object v1, p0, Landroid/support/v7/widget/an;->L:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    iget-object v2, p0, Landroid/support/v7/widget/an;->L:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v2, v1

    .line 1164
    iget-boolean v1, p0, Landroid/support/v7/widget/an;->h:Z

    if-nez v1, :cond_cf

    .line 1165
    iget-object v1, p0, Landroid/support/v7/widget/an;->L:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    neg-int v1, v1

    iput v1, p0, Landroid/support/v7/widget/an;->g:I

    .line 1172
    :cond_cf
    :goto_cf
    iget-object v1, p0, Landroid/support/v7/widget/an;->c:Landroid/widget/PopupWindow;

    invoke-virtual {v1}, Landroid/widget/PopupWindow;->getInputMethodMode()I

    .line 1174
    iget-object v1, p0, Landroid/support/v7/widget/an;->c:Landroid/widget/PopupWindow;

    .line 16425
    iget-object v3, p0, Landroid/support/v7/widget/an;->l:Landroid/view/View;

    .line 1174
    iget v4, p0, Landroid/support/v7/widget/an;->g:I

    invoke-virtual {v1, v3, v4}, Landroid/widget/PopupWindow;->getMaxAvailableHeight(Landroid/view/View;I)I

    move-result v3

    .line 1177
    iget-boolean v1, p0, Landroid/support/v7/widget/an;->z:Z

    if-nez v1, :cond_e6

    iget v1, p0, Landroid/support/v7/widget/an;->y:I

    if-ne v1, v7, :cond_11a

    .line 1178
    :cond_e6
    add-int v0, v3, v2

    .line 1206
    :goto_e8
    return v0

    :cond_e9
    move v0, v2

    .line 1070
    goto/16 :goto_1b

    .line 1115
    :pswitch_ec
    invoke-virtual {v3, v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1116
    invoke-virtual {v3, v5}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_8f

    .line 1120
    :pswitch_f3
    invoke-virtual {v3, v5}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1121
    invoke-virtual {v3, v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_8f

    .line 1144
    :cond_fa
    iget-object v0, p0, Landroid/support/v7/widget/an;->c:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->getContentView()Landroid/view/View;

    .line 1145
    iget-object v1, p0, Landroid/support/v7/widget/an;->B:Landroid/view/View;

    .line 1146
    if-eqz v1, :cond_167

    .line 1147
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 1149
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    iget v3, v0, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    add-int/2addr v1, v3

    iget v0, v0, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    add-int/2addr v0, v1

    goto :goto_ae

    .line 1168
    :cond_114
    iget-object v1, p0, Landroid/support/v7/widget/an;->L:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->setEmpty()V

    goto :goto_cf

    .line 1182
    :cond_11a
    iget v1, p0, Landroid/support/v7/widget/an;->e:I

    packed-switch v1, :pswitch_data_176

    .line 1196
    iget v1, p0, Landroid/support/v7/widget/an;->e:I

    invoke-static {v1, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 1200
    :goto_125
    iget-object v4, p0, Landroid/support/v7/widget/an;->d:Landroid/support/v7/widget/ar;

    sub-int/2addr v3, v0

    invoke-virtual {v4, v1, v3}, Landroid/support/v7/widget/ar;->a(II)I

    move-result v1

    .line 1204
    if-lez v1, :cond_12f

    add-int/2addr v0, v2

    .line 1206
    :cond_12f
    add-int/2addr v0, v1

    goto :goto_e8

    .line 1184
    :pswitch_131
    iget-object v1, p0, Landroid/support/v7/widget/an;->w:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    iget-object v4, p0, Landroid/support/v7/widget/an;->L:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    iget-object v5, p0, Landroid/support/v7/widget/an;->L:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->right:I

    add-int/2addr v4, v5

    sub-int/2addr v1, v4

    invoke-static {v1, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    goto :goto_125

    .line 1190
    :pswitch_14c
    iget-object v1, p0, Landroid/support/v7/widget/an;->w:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    iget-object v4, p0, Landroid/support/v7/widget/an;->L:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    iget-object v5, p0, Landroid/support/v7/widget/an;->L:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->right:I

    add-int/2addr v4, v5

    sub-int/2addr v1, v4

    invoke-static {v1, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    goto :goto_125

    :cond_167
    move v0, v2

    goto/16 :goto_ae

    :cond_16a
    move-object v1, v0

    move v0, v2

    goto/16 :goto_a9

    .line 1113
    :pswitch_data_16e
    .packed-switch 0x0
        :pswitch_f3
        :pswitch_ec
    .end packed-switch

    .line 1182
    :pswitch_data_176
    .packed-switch -0x2
        :pswitch_131
        :pswitch_14c
    .end packed-switch
.end method

.method private C()V
    .registers 6

    .prologue
    .line 1771
    sget-object v0, Landroid/support/v7/widget/an;->v:Ljava/lang/reflect/Method;

    if-eqz v0, :cond_16

    .line 1773
    :try_start_4
    sget-object v0, Landroid/support/v7/widget/an;->v:Ljava/lang/reflect/Method;

    iget-object v1, p0, Landroid/support/v7/widget/an;->c:Landroid/widget/PopupWindow;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_16
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_16} :catch_17

    .line 1778
    :cond_16
    :goto_16
    return-void

    .line 1775
    :catch_17
    move-exception v0

    const-string v0, "ListPopupWindow"

    const-string v1, "Could not call setClipToScreenEnabled() on PopupWindow. Oh well."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_16
.end method

.method static synthetic a(Landroid/support/v7/widget/an;)Landroid/support/v7/widget/ar;
    .registers 2

    .prologue
    .line 65
    iget-object v0, p0, Landroid/support/v7/widget/an;->d:Landroid/support/v7/widget/ar;

    return-object v0
.end method

.method private a()V
    .registers 2

    .prologue
    .line 281
    const/4 v0, 0x0

    iput v0, p0, Landroid/support/v7/widget/an;->k:I

    .line 282
    return-void
.end method

.method private a(Landroid/view/View;)V
    .registers 2

    .prologue
    .line 435
    iput-object p1, p0, Landroid/support/v7/widget/an;->l:Landroid/view/View;

    .line 436
    return-void
.end method

.method private a(Landroid/widget/AdapterView$OnItemClickListener;)V
    .registers 2

    .prologue
    .line 541
    iput-object p1, p0, Landroid/support/v7/widget/an;->m:Landroid/widget/AdapterView$OnItemClickListener;

    .line 542
    return-void
.end method

.method private a(Landroid/widget/AdapterView$OnItemSelectedListener;)V
    .registers 2

    .prologue
    .line 552
    iput-object p1, p0, Landroid/support/v7/widget/an;->E:Landroid/widget/AdapterView$OnItemSelectedListener;

    .line 553
    return-void
.end method

.method private a(Z)V
    .registers 2

    .prologue
    .line 325
    iput-boolean p1, p0, Landroid/support/v7/widget/an;->A:Z

    .line 326
    return-void
.end method

.method private a(ILandroid/view/KeyEvent;)Z
    .registers 13

    .prologue
    const/16 v8, 0x14

    const/16 v7, 0x13

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 870
    .line 13760
    iget-object v0, p0, Landroid/support/v7/widget/an;->c:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    .line 870
    if-eqz v0, :cond_96

    .line 876
    const/16 v0, 0x3e

    if-eq p1, v0, :cond_96

    iget-object v0, p0, Landroid/support/v7/widget/an;->d:Landroid/support/v7/widget/ar;

    invoke-virtual {v0}, Landroid/support/v7/widget/ar;->getSelectedItemPosition()I

    move-result v0

    if-gez v0, :cond_20

    invoke-static {p1}, Landroid/support/v7/widget/an;->l(I)Z

    move-result v0

    if-nez v0, :cond_96

    .line 879
    :cond_20
    iget-object v0, p0, Landroid/support/v7/widget/an;->d:Landroid/support/v7/widget/ar;

    invoke-virtual {v0}, Landroid/support/v7/widget/ar;->getSelectedItemPosition()I

    move-result v5

    .line 882
    iget-object v0, p0, Landroid/support/v7/widget/an;->c:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isAboveAnchor()Z

    move-result v0

    if-nez v0, :cond_62

    move v0, v1

    .line 884
    :goto_2f
    iget-object v6, p0, Landroid/support/v7/widget/an;->x:Landroid/widget/ListAdapter;

    .line 887
    const v3, 0x7fffffff

    .line 888
    const/high16 v4, -0x80000000

    .line 890
    if-eqz v6, :cond_4a

    .line 891
    invoke-interface {v6}, Landroid/widget/ListAdapter;->areAllItemsEnabled()Z

    move-result v3

    .line 892
    if-eqz v3, :cond_64

    move v4, v2

    .line 894
    :goto_3f
    if-eqz v3, :cond_6b

    invoke-interface {v6}, Landroid/widget/ListAdapter;->getCount()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    :goto_47
    move v9, v4

    move v4, v3

    move v3, v9

    .line 898
    :cond_4a
    if-eqz v0, :cond_50

    if-ne p1, v7, :cond_50

    if-le v5, v3, :cond_56

    :cond_50
    if-nez v0, :cond_78

    if-ne p1, v8, :cond_78

    if-lt v5, v4, :cond_78

    .line 902
    :cond_56
    invoke-virtual {p0}, Landroid/support/v7/widget/an;->f()V

    .line 903
    iget-object v0, p0, Landroid/support/v7/widget/an;->c:Landroid/widget/PopupWindow;

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setInputMethodMode(I)V

    .line 904
    invoke-virtual {p0}, Landroid/support/v7/widget/an;->b()V

    .line 950
    :goto_61
    :sswitch_61
    return v1

    :cond_62
    move v0, v2

    .line 882
    goto :goto_2f

    .line 892
    :cond_64
    iget-object v4, p0, Landroid/support/v7/widget/an;->d:Landroid/support/v7/widget/ar;

    invoke-virtual {v4, v2, v1}, Landroid/support/v7/widget/ar;->a(IZ)I

    move-result v4

    goto :goto_3f

    .line 894
    :cond_6b
    iget-object v3, p0, Landroid/support/v7/widget/an;->d:Landroid/support/v7/widget/ar;

    invoke-interface {v6}, Landroid/widget/ListAdapter;->getCount()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    invoke-virtual {v3, v6, v2}, Landroid/support/v7/widget/ar;->a(IZ)I

    move-result v3

    goto :goto_47

    .line 909
    :cond_78
    iget-object v6, p0, Landroid/support/v7/widget/an;->d:Landroid/support/v7/widget/ar;

    invoke-static {v6, v2}, Landroid/support/v7/widget/ar;->a(Landroid/support/v7/widget/ar;Z)Z

    .line 912
    iget-object v6, p0, Landroid/support/v7/widget/an;->d:Landroid/support/v7/widget/ar;

    invoke-virtual {v6, p1, p2}, Landroid/support/v7/widget/ar;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v6

    .line 915
    if-eqz v6, :cond_98

    .line 918
    iget-object v0, p0, Landroid/support/v7/widget/an;->c:Landroid/widget/PopupWindow;

    const/4 v3, 0x2

    invoke-virtual {v0, v3}, Landroid/widget/PopupWindow;->setInputMethodMode(I)V

    .line 923
    iget-object v0, p0, Landroid/support/v7/widget/an;->d:Landroid/support/v7/widget/ar;

    invoke-virtual {v0}, Landroid/support/v7/widget/ar;->requestFocusFromTouch()Z

    .line 924
    invoke-virtual {p0}, Landroid/support/v7/widget/an;->b()V

    .line 926
    sparse-switch p1, :sswitch_data_a6

    :cond_96
    move v1, v2

    .line 950
    goto :goto_61

    .line 936
    :cond_98
    if-eqz v0, :cond_9f

    if-ne p1, v8, :cond_9f

    .line 939
    if-ne v5, v4, :cond_96

    goto :goto_61

    .line 942
    :cond_9f
    if-nez v0, :cond_96

    if-ne p1, v7, :cond_96

    if-ne v5, v3, :cond_96

    goto :goto_61

    .line 926
    :sswitch_data_a6
    .sparse-switch
        0x13 -> :sswitch_61
        0x14 -> :sswitch_61
        0x17 -> :sswitch_61
        0x42 -> :sswitch_61
    .end sparse-switch
.end method

.method static synthetic b(Landroid/support/v7/widget/an;)Landroid/widget/PopupWindow;
    .registers 2

    .prologue
    .line 65
    iget-object v0, p0, Landroid/support/v7/widget/an;->c:Landroid/widget/PopupWindow;

    return-object v0
.end method

.method private b(I)V
    .registers 3

    .prologue
    .line 363
    iget-object v0, p0, Landroid/support/v7/widget/an;->c:Landroid/widget/PopupWindow;

    invoke-virtual {v0, p1}, Landroid/widget/PopupWindow;->setSoftInputMode(I)V

    .line 364
    return-void
.end method

.method private b(Landroid/graphics/drawable/Drawable;)V
    .registers 2

    .prologue
    .line 382
    iput-object p1, p0, Landroid/support/v7/widget/an;->D:Landroid/graphics/drawable/Drawable;

    .line 383
    return-void
.end method

.method private b(Landroid/view/View;)V
    .registers 3

    .prologue
    .line 562
    .line 2760
    iget-object v0, p0, Landroid/support/v7/widget/an;->c:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    .line 563
    if-eqz v0, :cond_b

    .line 564
    invoke-direct {p0}, Landroid/support/v7/widget/an;->t()V

    .line 566
    :cond_b
    iput-object p1, p0, Landroid/support/v7/widget/an;->B:Landroid/view/View;

    .line 567
    if-eqz v0, :cond_12

    .line 568
    invoke-virtual {p0}, Landroid/support/v7/widget/an;->b()V

    .line 570
    :cond_12
    return-void
.end method

.method private b(Z)V
    .registers 2

    .prologue
    .line 340
    iput-boolean p1, p0, Landroid/support/v7/widget/an;->z:Z

    .line 341
    return-void
.end method

.method private b(ILandroid/view/KeyEvent;)Z
    .registers 5

    .prologue
    .line 964
    .line 14760
    iget-object v0, p0, Landroid/support/v7/widget/an;->c:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    .line 964
    if-eqz v0, :cond_22

    iget-object v0, p0, Landroid/support/v7/widget/an;->d:Landroid/support/v7/widget/ar;

    invoke-virtual {v0}, Landroid/support/v7/widget/ar;->getSelectedItemPosition()I

    move-result v0

    if-ltz v0, :cond_22

    .line 965
    iget-object v0, p0, Landroid/support/v7/widget/an;->d:Landroid/support/v7/widget/ar;

    invoke-virtual {v0, p1, p2}, Landroid/support/v7/widget/ar;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    .line 966
    if-eqz v0, :cond_21

    invoke-static {p1}, Landroid/support/v7/widget/an;->l(I)Z

    move-result v1

    if-eqz v1, :cond_21

    .line 969
    invoke-virtual {p0}, Landroid/support/v7/widget/an;->d()V

    .line 973
    :cond_21
    :goto_21
    return v0

    :cond_22
    const/4 v0, 0x0

    goto :goto_21
.end method

.method static synthetic c(Landroid/support/v7/widget/an;)Landroid/support/v7/widget/az;
    .registers 2

    .prologue
    .line 65
    iget-object v0, p0, Landroid/support/v7/widget/an;->F:Landroid/support/v7/widget/az;

    return-object v0
.end method

.method private c(Landroid/view/View;)Landroid/view/View$OnTouchListener;
    .registers 3

    .prologue
    .line 1033
    new-instance v0, Landroid/support/v7/widget/ao;

    invoke-direct {v0, p0, p1}, Landroid/support/v7/widget/ao;-><init>(Landroid/support/v7/widget/an;Landroid/view/View;)V

    return-object v0
.end method

.method private c(I)V
    .registers 3

    .prologue
    .line 407
    iget-object v0, p0, Landroid/support/v7/widget/an;->c:Landroid/widget/PopupWindow;

    invoke-virtual {v0, p1}, Landroid/widget/PopupWindow;->setAnimationStyle(I)V

    .line 408
    return-void
.end method

.method private c(ILandroid/view/KeyEvent;)Z
    .registers 6

    .prologue
    const/4 v0, 0x1

    .line 988
    const/4 v1, 0x4

    if-ne p1, v1, :cond_43

    .line 15760
    iget-object v1, p0, Landroid/support/v7/widget/an;->c:Landroid/widget/PopupWindow;

    invoke-virtual {v1}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v1

    .line 988
    if-eqz v1, :cond_43

    .line 991
    iget-object v1, p0, Landroid/support/v7/widget/an;->l:Landroid/view/View;

    .line 992
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-nez v2, :cond_24

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v2

    if-nez v2, :cond_24

    .line 993
    invoke-virtual {v1}, Landroid/view/View;->getKeyDispatcherState()Landroid/view/KeyEvent$DispatcherState;

    move-result-object v1

    .line 994
    if-eqz v1, :cond_23

    .line 995
    invoke-virtual {v1, p2, p0}, Landroid/view/KeyEvent$DispatcherState;->startTracking(Landroid/view/KeyEvent;Ljava/lang/Object;)V

    .line 1009
    :cond_23
    :goto_23
    return v0

    .line 998
    :cond_24
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-ne v2, v0, :cond_43

    .line 999
    invoke-virtual {v1}, Landroid/view/View;->getKeyDispatcherState()Landroid/view/KeyEvent$DispatcherState;

    move-result-object v1

    .line 1000
    if-eqz v1, :cond_33

    .line 1001
    invoke-virtual {v1, p2}, Landroid/view/KeyEvent$DispatcherState;->handleUpEvent(Landroid/view/KeyEvent;)V

    .line 1003
    :cond_33
    invoke-virtual {p2}, Landroid/view/KeyEvent;->isTracking()Z

    move-result v1

    if-eqz v1, :cond_43

    invoke-virtual {p2}, Landroid/view/KeyEvent;->isCanceled()Z

    move-result v1

    if-nez v1, :cond_43

    .line 1004
    invoke-virtual {p0}, Landroid/support/v7/widget/an;->d()V

    goto :goto_23

    .line 1009
    :cond_43
    const/4 v0, 0x0

    goto :goto_23
.end method

.method static synthetic d(Landroid/support/v7/widget/an;)Landroid/os/Handler;
    .registers 2

    .prologue
    .line 65
    iget-object v0, p0, Landroid/support/v7/widget/an;->K:Landroid/os/Handler;

    return-object v0
.end method

.method private d(I)V
    .registers 2

    .prologue
    .line 451
    iput p1, p0, Landroid/support/v7/widget/an;->f:I

    .line 452
    return-void
.end method

.method private e(I)V
    .registers 3

    .prologue
    .line 470
    iput p1, p0, Landroid/support/v7/widget/an;->g:I

    .line 471
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/widget/an;->h:Z

    .line 472
    return-void
.end method

.method private f(I)V
    .registers 2

    .prologue
    .line 481
    iput p1, p0, Landroid/support/v7/widget/an;->i:I

    .line 482
    return-void
.end method

.method private g(I)V
    .registers 2

    .prologue
    .line 498
    iput p1, p0, Landroid/support/v7/widget/an;->e:I

    .line 499
    return-void
.end method

.method private h()I
    .registers 2

    .prologue
    .line 291
    iget v0, p0, Landroid/support/v7/widget/an;->k:I

    return v0
.end method

.method private h(I)V
    .registers 2

    .prologue
    .line 530
    iput p1, p0, Landroid/support/v7/widget/an;->y:I

    .line 531
    return-void
.end method

.method private i(I)V
    .registers 5

    .prologue
    .line 729
    iget-object v0, p0, Landroid/support/v7/widget/an;->d:Landroid/support/v7/widget/ar;

    .line 7760
    iget-object v1, p0, Landroid/support/v7/widget/an;->c:Landroid/widget/PopupWindow;

    invoke-virtual {v1}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v1

    .line 730
    if-eqz v1, :cond_23

    if-eqz v0, :cond_23

    .line 731
    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/support/v7/widget/ar;->a(Landroid/support/v7/widget/ar;Z)Z

    .line 732
    invoke-virtual {v0, p1}, Landroid/support/v7/widget/ar;->setSelection(I)V

    .line 734
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-lt v1, v2, :cond_23

    .line 735
    invoke-virtual {v0}, Landroid/support/v7/widget/ar;->getChoiceMode()I

    move-result v1

    if-eqz v1, :cond_23

    .line 736
    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Landroid/support/v7/widget/ar;->setItemChecked(IZ)V

    .line 740
    :cond_23
    return-void
.end method

.method private i()Z
    .registers 2

    .prologue
    .line 314
    iget-boolean v0, p0, Landroid/support/v7/widget/an;->M:Z

    return v0
.end method

.method private j()Z
    .registers 2

    .prologue
    .line 349
    iget-boolean v0, p0, Landroid/support/v7/widget/an;->z:Z

    return v0
.end method

.method private j(I)Z
    .registers 8

    .prologue
    .line 779
    .line 8760
    iget-object v0, p0, Landroid/support/v7/widget/an;->c:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    .line 779
    if-eqz v0, :cond_28

    .line 780
    iget-object v0, p0, Landroid/support/v7/widget/an;->m:Landroid/widget/AdapterView$OnItemClickListener;

    if-eqz v0, :cond_26

    .line 781
    iget-object v1, p0, Landroid/support/v7/widget/an;->d:Landroid/support/v7/widget/ar;

    .line 782
    invoke-virtual {v1}, Landroid/support/v7/widget/ar;->getFirstVisiblePosition()I

    move-result v0

    sub-int v0, p1, v0

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/ar;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 783
    invoke-virtual {v1}, Landroid/support/v7/widget/ar;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v3

    .line 784
    iget-object v0, p0, Landroid/support/v7/widget/an;->m:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-interface {v3, p1}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v4

    move v3, p1

    invoke-interface/range {v0 .. v5}, Landroid/widget/AdapterView$OnItemClickListener;->onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V

    .line 786
    :cond_26
    const/4 v0, 0x1

    .line 788
    :goto_27
    return v0

    :cond_28
    const/4 v0, 0x0

    goto :goto_27
.end method

.method private k()I
    .registers 2

    .prologue
    .line 373
    iget-object v0, p0, Landroid/support/v7/widget/an;->c:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->getSoftInputMode()I

    move-result v0

    return v0
.end method

.method private k(I)V
    .registers 2

    .prologue
    .line 855
    iput p1, p0, Landroid/support/v7/widget/an;->j:I

    .line 856
    return-void
.end method

.method private l()Landroid/graphics/drawable/Drawable;
    .registers 2

    .prologue
    .line 389
    iget-object v0, p0, Landroid/support/v7/widget/an;->c:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method private static l(I)Z
    .registers 2

    .prologue
    .line 1767
    const/16 v0, 0x42

    if-eq p0, v0, :cond_8

    const/16 v0, 0x17

    if-ne p0, v0, :cond_a

    :cond_8
    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method private m()I
    .registers 2

    .prologue
    .line 416
    iget-object v0, p0, Landroid/support/v7/widget/an;->c:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->getAnimationStyle()I

    move-result v0

    return v0
.end method

.method private n()Landroid/view/View;
    .registers 2

    .prologue
    .line 425
    iget-object v0, p0, Landroid/support/v7/widget/an;->l:Landroid/view/View;

    return-object v0
.end method

.method private o()I
    .registers 2

    .prologue
    .line 442
    iget v0, p0, Landroid/support/v7/widget/an;->f:I

    return v0
.end method

.method private p()I
    .registers 2

    .prologue
    .line 458
    iget-boolean v0, p0, Landroid/support/v7/widget/an;->h:Z

    if-nez v0, :cond_6

    .line 459
    const/4 v0, 0x0

    .line 461
    :goto_5
    return v0

    :cond_6
    iget v0, p0, Landroid/support/v7/widget/an;->g:I

    goto :goto_5
.end method

.method private q()I
    .registers 2

    .prologue
    .line 488
    iget v0, p0, Landroid/support/v7/widget/an;->e:I

    return v0
.end method

.method private r()I
    .registers 2

    .prologue
    .line 521
    iget v0, p0, Landroid/support/v7/widget/an;->y:I

    return v0
.end method

.method private s()V
    .registers 3

    .prologue
    .line 576
    iget-object v0, p0, Landroid/support/v7/widget/an;->K:Landroid/os/Handler;

    iget-object v1, p0, Landroid/support/v7/widget/an;->J:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 577
    return-void
.end method

.method private t()V
    .registers 3

    .prologue
    .line 688
    iget-object v0, p0, Landroid/support/v7/widget/an;->B:Landroid/view/View;

    if-eqz v0, :cond_15

    .line 689
    iget-object v0, p0, Landroid/support/v7/widget/an;->B:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 690
    instance-of v1, v0, Landroid/view/ViewGroup;

    if-eqz v1, :cond_15

    .line 691
    check-cast v0, Landroid/view/ViewGroup;

    .line 692
    iget-object v1, p0, Landroid/support/v7/widget/an;->B:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 695
    :cond_15
    return-void
.end method

.method private u()I
    .registers 2

    .prologue
    .line 719
    iget-object v0, p0, Landroid/support/v7/widget/an;->c:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->getInputMethodMode()I

    move-result v0

    return v0
.end method

.method private v()Z
    .registers 2

    .prologue
    .line 760
    iget-object v0, p0, Landroid/support/v7/widget/an;->c:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    return v0
.end method

.method private w()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 795
    .line 9760
    iget-object v0, p0, Landroid/support/v7/widget/an;->c:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    .line 795
    if-nez v0, :cond_a

    .line 796
    const/4 v0, 0x0

    .line 798
    :goto_9
    return-object v0

    :cond_a
    iget-object v0, p0, Landroid/support/v7/widget/an;->d:Landroid/support/v7/widget/ar;

    invoke-virtual {v0}, Landroid/support/v7/widget/ar;->getSelectedItem()Ljava/lang/Object;

    move-result-object v0

    goto :goto_9
.end method

.method private x()I
    .registers 2

    .prologue
    .line 808
    .line 10760
    iget-object v0, p0, Landroid/support/v7/widget/an;->c:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    .line 808
    if-nez v0, :cond_a

    .line 809
    const/4 v0, -0x1

    .line 811
    :goto_9
    return v0

    :cond_a
    iget-object v0, p0, Landroid/support/v7/widget/an;->d:Landroid/support/v7/widget/ar;

    invoke-virtual {v0}, Landroid/support/v7/widget/ar;->getSelectedItemPosition()I

    move-result v0

    goto :goto_9
.end method

.method private y()J
    .registers 3

    .prologue
    .line 821
    .line 11760
    iget-object v0, p0, Landroid/support/v7/widget/an;->c:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    .line 821
    if-nez v0, :cond_b

    .line 822
    const-wide/high16 v0, -0x8000000000000000L

    .line 824
    :goto_a
    return-wide v0

    :cond_b
    iget-object v0, p0, Landroid/support/v7/widget/an;->d:Landroid/support/v7/widget/ar;

    invoke-virtual {v0}, Landroid/support/v7/widget/ar;->getSelectedItemId()J

    move-result-wide v0

    goto :goto_a
.end method

.method private z()Landroid/view/View;
    .registers 2

    .prologue
    .line 834
    .line 12760
    iget-object v0, p0, Landroid/support/v7/widget/an;->c:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    .line 834
    if-nez v0, :cond_a

    .line 835
    const/4 v0, 0x0

    .line 837
    :goto_9
    return-object v0

    :cond_a
    iget-object v0, p0, Landroid/support/v7/widget/an;->d:Landroid/support/v7/widget/ar;

    invoke-virtual {v0}, Landroid/support/v7/widget/ar;->getSelectedView()Landroid/view/View;

    move-result-object v0

    goto :goto_9
.end method


# virtual methods
.method public final a(I)V
    .registers 4

    .prologue
    .line 508
    iget-object v0, p0, Landroid/support/v7/widget/an;->c:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 509
    if-eqz v0, :cond_1a

    .line 510
    iget-object v1, p0, Landroid/support/v7/widget/an;->L:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    .line 511
    iget-object v0, p0, Landroid/support/v7/widget/an;->L:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iget-object v1, p0, Landroid/support/v7/widget/an;->L:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    add-int/2addr v0, v1

    add-int/2addr v0, p1

    iput v0, p0, Landroid/support/v7/widget/an;->e:I

    .line 515
    :goto_19
    return-void

    .line 2498
    :cond_1a
    iput p1, p0, Landroid/support/v7/widget/an;->e:I

    goto :goto_19
.end method

.method public final a(Landroid/graphics/drawable/Drawable;)V
    .registers 3

    .prologue
    .line 398
    iget-object v0, p0, Landroid/support/v7/widget/an;->c:Landroid/widget/PopupWindow;

    invoke-virtual {v0, p1}, Landroid/widget/PopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 399
    return-void
.end method

.method public a(Landroid/widget/ListAdapter;)V
    .registers 4

    .prologue
    .line 256
    iget-object v0, p0, Landroid/support/v7/widget/an;->C:Landroid/database/DataSetObserver;

    if-nez v0, :cond_23

    .line 257
    new-instance v0, Landroid/support/v7/widget/aw;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Landroid/support/v7/widget/aw;-><init>(Landroid/support/v7/widget/an;B)V

    iput-object v0, p0, Landroid/support/v7/widget/an;->C:Landroid/database/DataSetObserver;

    .line 261
    :cond_c
    :goto_c
    iput-object p1, p0, Landroid/support/v7/widget/an;->x:Landroid/widget/ListAdapter;

    .line 262
    iget-object v0, p0, Landroid/support/v7/widget/an;->x:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_17

    .line 263
    iget-object v0, p0, Landroid/support/v7/widget/an;->C:Landroid/database/DataSetObserver;

    invoke-interface {p1, v0}, Landroid/widget/ListAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 266
    :cond_17
    iget-object v0, p0, Landroid/support/v7/widget/an;->d:Landroid/support/v7/widget/ar;

    if-eqz v0, :cond_22

    .line 267
    iget-object v0, p0, Landroid/support/v7/widget/an;->d:Landroid/support/v7/widget/ar;

    iget-object v1, p0, Landroid/support/v7/widget/an;->x:Landroid/widget/ListAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/ar;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 269
    :cond_22
    return-void

    .line 258
    :cond_23
    iget-object v0, p0, Landroid/support/v7/widget/an;->x:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_c

    .line 259
    iget-object v0, p0, Landroid/support/v7/widget/an;->x:Landroid/widget/ListAdapter;

    iget-object v1, p0, Landroid/support/v7/widget/an;->C:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    goto :goto_c
.end method

.method public final a(Landroid/widget/PopupWindow$OnDismissListener;)V
    .registers 3

    .prologue
    .line 684
    iget-object v0, p0, Landroid/support/v7/widget/an;->c:Landroid/widget/PopupWindow;

    invoke-virtual {v0, p1}, Landroid/widget/PopupWindow;->setOnDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V

    .line 685
    return-void
.end method

.method public b()V
    .registers 12

    .prologue
    const/high16 v10, -0x80000000

    const/4 v9, -0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v5, -0x1

    .line 584
    .line 3051
    iget-object v0, p0, Landroid/support/v7/widget/an;->d:Landroid/support/v7/widget/ar;

    if-nez v0, :cond_136

    .line 3052
    iget-object v4, p0, Landroid/support/v7/widget/an;->w:Landroid/content/Context;

    .line 3060
    new-instance v0, Landroid/support/v7/widget/ap;

    invoke-direct {v0, p0}, Landroid/support/v7/widget/ap;-><init>(Landroid/support/v7/widget/an;)V

    iput-object v0, p0, Landroid/support/v7/widget/an;->J:Ljava/lang/Runnable;

    .line 3070
    new-instance v3, Landroid/support/v7/widget/ar;

    iget-boolean v0, p0, Landroid/support/v7/widget/an;->M:Z

    if-nez v0, :cond_123

    move v0, v1

    :goto_1a
    invoke-direct {v3, v4, v0}, Landroid/support/v7/widget/ar;-><init>(Landroid/content/Context;Z)V

    iput-object v3, p0, Landroid/support/v7/widget/an;->d:Landroid/support/v7/widget/ar;

    .line 3071
    iget-object v0, p0, Landroid/support/v7/widget/an;->D:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_2a

    .line 3072
    iget-object v0, p0, Landroid/support/v7/widget/an;->d:Landroid/support/v7/widget/ar;

    iget-object v3, p0, Landroid/support/v7/widget/an;->D:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/ar;->setSelector(Landroid/graphics/drawable/Drawable;)V

    .line 3074
    :cond_2a
    iget-object v0, p0, Landroid/support/v7/widget/an;->d:Landroid/support/v7/widget/ar;

    iget-object v3, p0, Landroid/support/v7/widget/an;->x:Landroid/widget/ListAdapter;

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/ar;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 3075
    iget-object v0, p0, Landroid/support/v7/widget/an;->d:Landroid/support/v7/widget/ar;

    iget-object v3, p0, Landroid/support/v7/widget/an;->m:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/ar;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 3076
    iget-object v0, p0, Landroid/support/v7/widget/an;->d:Landroid/support/v7/widget/ar;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/ar;->setFocusable(Z)V

    .line 3077
    iget-object v0, p0, Landroid/support/v7/widget/an;->d:Landroid/support/v7/widget/ar;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/ar;->setFocusableInTouchMode(Z)V

    .line 3078
    iget-object v0, p0, Landroid/support/v7/widget/an;->d:Landroid/support/v7/widget/ar;

    new-instance v3, Landroid/support/v7/widget/aq;

    invoke-direct {v3, p0}, Landroid/support/v7/widget/aq;-><init>(Landroid/support/v7/widget/an;)V

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/ar;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 3094
    iget-object v0, p0, Landroid/support/v7/widget/an;->d:Landroid/support/v7/widget/ar;

    iget-object v3, p0, Landroid/support/v7/widget/an;->H:Landroid/support/v7/widget/ax;

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/ar;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 3096
    iget-object v0, p0, Landroid/support/v7/widget/an;->E:Landroid/widget/AdapterView$OnItemSelectedListener;

    if-eqz v0, :cond_5e

    .line 3097
    iget-object v0, p0, Landroid/support/v7/widget/an;->d:Landroid/support/v7/widget/ar;

    iget-object v3, p0, Landroid/support/v7/widget/an;->E:Landroid/widget/AdapterView$OnItemSelectedListener;

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/ar;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 3100
    :cond_5e
    iget-object v0, p0, Landroid/support/v7/widget/an;->d:Landroid/support/v7/widget/ar;

    .line 3102
    iget-object v6, p0, Landroid/support/v7/widget/an;->B:Landroid/view/View;

    .line 3103
    if-eqz v6, :cond_283

    .line 3106
    new-instance v3, Landroid/widget/LinearLayout;

    invoke-direct {v3, v4}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 3107
    invoke-virtual {v3, v1}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 3109
    new-instance v4, Landroid/widget/LinearLayout$LayoutParams;

    const/high16 v7, 0x3f800000    # 1.0f

    invoke-direct {v4, v5, v2, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    .line 3113
    iget v7, p0, Landroid/support/v7/widget/an;->k:I

    packed-switch v7, :pswitch_data_288

    .line 3125
    const-string v0, "ListPopupWindow"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v7, "Invalid hint position "

    invoke-direct {v4, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v7, p0, Landroid/support/v7/widget/an;->k:I

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3131
    :goto_8e
    iget v0, p0, Landroid/support/v7/widget/an;->e:I

    invoke-static {v0, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 3133
    invoke-virtual {v6, v0, v2}, Landroid/view/View;->measure(II)V

    .line 3135
    invoke-virtual {v6}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 3136
    invoke-virtual {v6}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    iget v6, v0, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    add-int/2addr v4, v6

    iget v0, v0, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    add-int/2addr v0, v4

    .line 3142
    :goto_a7
    iget-object v4, p0, Landroid/support/v7/widget/an;->c:Landroid/widget/PopupWindow;

    invoke-virtual {v4, v3}, Landroid/widget/PopupWindow;->setContentView(Landroid/view/View;)V

    .line 3157
    :goto_ac
    iget-object v3, p0, Landroid/support/v7/widget/an;->c:Landroid/widget/PopupWindow;

    invoke-virtual {v3}, Landroid/widget/PopupWindow;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 3158
    if-eqz v3, :cond_151

    .line 3159
    iget-object v4, p0, Landroid/support/v7/widget/an;->L:Landroid/graphics/Rect;

    invoke-virtual {v3, v4}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    .line 3160
    iget-object v3, p0, Landroid/support/v7/widget/an;->L:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    iget-object v4, p0, Landroid/support/v7/widget/an;->L:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v3, v4

    .line 3164
    iget-boolean v4, p0, Landroid/support/v7/widget/an;->h:Z

    if-nez v4, :cond_cd

    .line 3165
    iget-object v4, p0, Landroid/support/v7/widget/an;->L:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    neg-int v4, v4

    iput v4, p0, Landroid/support/v7/widget/an;->g:I

    .line 3172
    :cond_cd
    :goto_cd
    iget-object v4, p0, Landroid/support/v7/widget/an;->c:Landroid/widget/PopupWindow;

    invoke-virtual {v4}, Landroid/widget/PopupWindow;->getInputMethodMode()I

    .line 3174
    iget-object v4, p0, Landroid/support/v7/widget/an;->c:Landroid/widget/PopupWindow;

    .line 3425
    iget-object v6, p0, Landroid/support/v7/widget/an;->l:Landroid/view/View;

    .line 3174
    iget v7, p0, Landroid/support/v7/widget/an;->g:I

    invoke-virtual {v4, v6, v7}, Landroid/widget/PopupWindow;->getMaxAvailableHeight(Landroid/view/View;I)I

    move-result v6

    .line 3177
    iget-boolean v4, p0, Landroid/support/v7/widget/an;->z:Z

    if-nez v4, :cond_e4

    iget v4, p0, Landroid/support/v7/widget/an;->y:I

    if-ne v4, v5, :cond_159

    .line 3178
    :cond_e4
    add-int v0, v6, v3

    .line 589
    :goto_e6
    invoke-virtual {p0}, Landroid/support/v7/widget/an;->g()Z

    move-result v6

    .line 591
    iget-object v3, p0, Landroid/support/v7/widget/an;->c:Landroid/widget/PopupWindow;

    invoke-virtual {v3}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v3

    if-eqz v3, :cond_1de

    .line 592
    iget v3, p0, Landroid/support/v7/widget/an;->e:I

    if-ne v3, v5, :cond_1ab

    move v4, v5

    .line 602
    :goto_f7
    iget v3, p0, Landroid/support/v7/widget/an;->y:I

    if-ne v3, v5, :cond_1d0

    .line 605
    if-eqz v6, :cond_1bb

    move v3, v0

    .line 606
    :goto_fe
    if-eqz v6, :cond_1c1

    .line 607
    iget-object v0, p0, Landroid/support/v7/widget/an;->c:Landroid/widget/PopupWindow;

    iget v6, p0, Landroid/support/v7/widget/an;->e:I

    if-ne v6, v5, :cond_1be

    :goto_106
    invoke-virtual {v0, v5, v2}, Landroid/widget/PopupWindow;->setWindowLayoutMode(II)V

    move v5, v3

    .line 622
    :goto_10a
    iget-object v0, p0, Landroid/support/v7/widget/an;->c:Landroid/widget/PopupWindow;

    iget-boolean v3, p0, Landroid/support/v7/widget/an;->A:Z

    if-nez v3, :cond_1db

    iget-boolean v3, p0, Landroid/support/v7/widget/an;->z:Z

    if-nez v3, :cond_1db

    :goto_114
    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setOutsideTouchable(Z)V

    .line 624
    iget-object v0, p0, Landroid/support/v7/widget/an;->c:Landroid/widget/PopupWindow;

    .line 5425
    iget-object v1, p0, Landroid/support/v7/widget/an;->l:Landroid/view/View;

    .line 624
    iget v2, p0, Landroid/support/v7/widget/an;->f:I

    iget v3, p0, Landroid/support/v7/widget/an;->g:I

    invoke-virtual/range {v0 .. v5}, Landroid/widget/PopupWindow;->update(Landroid/view/View;IIII)V

    .line 665
    :cond_122
    :goto_122
    return-void

    :cond_123
    move v0, v2

    .line 3070
    goto/16 :goto_1a

    .line 3115
    :pswitch_126
    invoke-virtual {v3, v0, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 3116
    invoke-virtual {v3, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto/16 :goto_8e

    .line 3120
    :pswitch_12e
    invoke-virtual {v3, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 3121
    invoke-virtual {v3, v0, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_8e

    .line 3144
    :cond_136
    iget-object v0, p0, Landroid/support/v7/widget/an;->c:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->getContentView()Landroid/view/View;

    .line 3145
    iget-object v3, p0, Landroid/support/v7/widget/an;->B:Landroid/view/View;

    .line 3146
    if-eqz v3, :cond_280

    .line 3147
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 3149
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    iget v4, v0, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    add-int/2addr v3, v4

    iget v0, v0, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    add-int/2addr v0, v3

    goto/16 :goto_ac

    .line 3168
    :cond_151
    iget-object v3, p0, Landroid/support/v7/widget/an;->L:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->setEmpty()V

    move v3, v2

    goto/16 :goto_cd

    .line 3182
    :cond_159
    iget v4, p0, Landroid/support/v7/widget/an;->e:I

    packed-switch v4, :pswitch_data_290

    .line 3196
    iget v4, p0, Landroid/support/v7/widget/an;->e:I

    const/high16 v7, 0x40000000    # 2.0f

    invoke-static {v4, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    .line 3200
    :goto_166
    iget-object v7, p0, Landroid/support/v7/widget/an;->d:Landroid/support/v7/widget/ar;

    sub-int/2addr v6, v0

    invoke-virtual {v7, v4, v6}, Landroid/support/v7/widget/ar;->a(II)I

    move-result v4

    .line 3204
    if-lez v4, :cond_170

    add-int/2addr v0, v3

    .line 3206
    :cond_170
    add-int/2addr v0, v4

    goto/16 :goto_e6

    .line 3184
    :pswitch_173
    iget-object v4, p0, Landroid/support/v7/widget/an;->w:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    iget v4, v4, Landroid/util/DisplayMetrics;->widthPixels:I

    iget-object v7, p0, Landroid/support/v7/widget/an;->L:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->left:I

    iget-object v8, p0, Landroid/support/v7/widget/an;->L:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->right:I

    add-int/2addr v7, v8

    sub-int/2addr v4, v7

    invoke-static {v4, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    goto :goto_166

    .line 3190
    :pswitch_18e
    iget-object v4, p0, Landroid/support/v7/widget/an;->w:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    iget v4, v4, Landroid/util/DisplayMetrics;->widthPixels:I

    iget-object v7, p0, Landroid/support/v7/widget/an;->L:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->left:I

    iget-object v8, p0, Landroid/support/v7/widget/an;->L:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->right:I

    add-int/2addr v7, v8

    sub-int/2addr v4, v7

    const/high16 v7, 0x40000000    # 2.0f

    invoke-static {v4, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    goto :goto_166

    .line 596
    :cond_1ab
    iget v3, p0, Landroid/support/v7/widget/an;->e:I

    if-ne v3, v9, :cond_1b7

    .line 4425
    iget-object v3, p0, Landroid/support/v7/widget/an;->l:Landroid/view/View;

    .line 597
    invoke-virtual {v3}, Landroid/view/View;->getWidth()I

    move-result v4

    goto/16 :goto_f7

    .line 599
    :cond_1b7
    iget v4, p0, Landroid/support/v7/widget/an;->e:I

    goto/16 :goto_f7

    :cond_1bb
    move v3, v5

    .line 605
    goto/16 :goto_fe

    :cond_1be
    move v5, v2

    .line 607
    goto/16 :goto_106

    .line 611
    :cond_1c1
    iget-object v6, p0, Landroid/support/v7/widget/an;->c:Landroid/widget/PopupWindow;

    iget v0, p0, Landroid/support/v7/widget/an;->e:I

    if-ne v0, v5, :cond_1ce

    move v0, v5

    :goto_1c8
    invoke-virtual {v6, v0, v5}, Landroid/widget/PopupWindow;->setWindowLayoutMode(II)V

    move v5, v3

    goto/16 :goto_10a

    :cond_1ce
    move v0, v2

    goto :goto_1c8

    .line 616
    :cond_1d0
    iget v3, p0, Landroid/support/v7/widget/an;->y:I

    if-ne v3, v9, :cond_1d7

    move v5, v0

    .line 617
    goto/16 :goto_10a

    .line 619
    :cond_1d7
    iget v5, p0, Landroid/support/v7/widget/an;->y:I

    goto/16 :goto_10a

    :cond_1db
    move v1, v2

    .line 622
    goto/16 :goto_114

    .line 627
    :cond_1de
    iget v3, p0, Landroid/support/v7/widget/an;->e:I

    if-ne v3, v5, :cond_245

    move v3, v5

    .line 637
    :goto_1e3
    iget v4, p0, Landroid/support/v7/widget/an;->y:I

    if-ne v4, v5, :cond_25f

    move v0, v5

    .line 647
    :goto_1e8
    iget-object v4, p0, Landroid/support/v7/widget/an;->c:Landroid/widget/PopupWindow;

    invoke-virtual {v4, v3, v0}, Landroid/widget/PopupWindow;->setWindowLayoutMode(II)V

    .line 6771
    sget-object v0, Landroid/support/v7/widget/an;->v:Ljava/lang/reflect/Method;

    if-eqz v0, :cond_203

    .line 6773
    :try_start_1f1
    sget-object v0, Landroid/support/v7/widget/an;->v:Ljava/lang/reflect/Method;

    iget-object v3, p0, Landroid/support/v7/widget/an;->c:Landroid/widget/PopupWindow;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v6, 0x0

    const/4 v7, 0x1

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    aput-object v7, v4, v6

    invoke-virtual {v0, v3, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_203
    .catch Ljava/lang/Exception; {:try_start_1f1 .. :try_end_203} :catch_275

    .line 652
    :cond_203
    :goto_203
    iget-object v0, p0, Landroid/support/v7/widget/an;->c:Landroid/widget/PopupWindow;

    iget-boolean v3, p0, Landroid/support/v7/widget/an;->A:Z

    if-nez v3, :cond_27e

    iget-boolean v3, p0, Landroid/support/v7/widget/an;->z:Z

    if-nez v3, :cond_27e

    :goto_20d
    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setOutsideTouchable(Z)V

    .line 653
    iget-object v0, p0, Landroid/support/v7/widget/an;->c:Landroid/widget/PopupWindow;

    iget-object v1, p0, Landroid/support/v7/widget/an;->G:Landroid/support/v7/widget/ay;

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setTouchInterceptor(Landroid/view/View$OnTouchListener;)V

    .line 654
    iget-object v0, p0, Landroid/support/v7/widget/an;->c:Landroid/widget/PopupWindow;

    .line 7425
    iget-object v1, p0, Landroid/support/v7/widget/an;->l:Landroid/view/View;

    .line 654
    iget v2, p0, Landroid/support/v7/widget/an;->f:I

    iget v3, p0, Landroid/support/v7/widget/an;->g:I

    iget v4, p0, Landroid/support/v7/widget/an;->i:I

    invoke-static {v0, v1, v2, v3, v4}, Landroid/support/v4/widget/bo;->a(Landroid/widget/PopupWindow;Landroid/view/View;III)V

    .line 656
    iget-object v0, p0, Landroid/support/v7/widget/an;->d:Landroid/support/v7/widget/ar;

    invoke-virtual {v0, v5}, Landroid/support/v7/widget/ar;->setSelection(I)V

    .line 658
    iget-boolean v0, p0, Landroid/support/v7/widget/an;->M:Z

    if-eqz v0, :cond_235

    iget-object v0, p0, Landroid/support/v7/widget/an;->d:Landroid/support/v7/widget/ar;

    invoke-virtual {v0}, Landroid/support/v7/widget/ar;->isInTouchMode()Z

    move-result v0

    if-eqz v0, :cond_238

    .line 659
    :cond_235
    invoke-virtual {p0}, Landroid/support/v7/widget/an;->f()V

    .line 661
    :cond_238
    iget-boolean v0, p0, Landroid/support/v7/widget/an;->M:Z

    if-nez v0, :cond_122

    .line 662
    iget-object v0, p0, Landroid/support/v7/widget/an;->K:Landroid/os/Handler;

    iget-object v1, p0, Landroid/support/v7/widget/an;->I:Landroid/support/v7/widget/av;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_122

    .line 630
    :cond_245
    iget v3, p0, Landroid/support/v7/widget/an;->e:I

    if-ne v3, v9, :cond_256

    .line 631
    iget-object v3, p0, Landroid/support/v7/widget/an;->c:Landroid/widget/PopupWindow;

    .line 6425
    iget-object v4, p0, Landroid/support/v7/widget/an;->l:Landroid/view/View;

    .line 631
    invoke-virtual {v4}, Landroid/view/View;->getWidth()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/PopupWindow;->setWidth(I)V

    move v3, v2

    goto :goto_1e3

    .line 633
    :cond_256
    iget-object v3, p0, Landroid/support/v7/widget/an;->c:Landroid/widget/PopupWindow;

    iget v4, p0, Landroid/support/v7/widget/an;->e:I

    invoke-virtual {v3, v4}, Landroid/widget/PopupWindow;->setWidth(I)V

    move v3, v2

    goto :goto_1e3

    .line 640
    :cond_25f
    iget v4, p0, Landroid/support/v7/widget/an;->y:I

    if-ne v4, v9, :cond_26b

    .line 641
    iget-object v4, p0, Landroid/support/v7/widget/an;->c:Landroid/widget/PopupWindow;

    invoke-virtual {v4, v0}, Landroid/widget/PopupWindow;->setHeight(I)V

    move v0, v2

    goto/16 :goto_1e8

    .line 643
    :cond_26b
    iget-object v0, p0, Landroid/support/v7/widget/an;->c:Landroid/widget/PopupWindow;

    iget v4, p0, Landroid/support/v7/widget/an;->y:I

    invoke-virtual {v0, v4}, Landroid/widget/PopupWindow;->setHeight(I)V

    move v0, v2

    goto/16 :goto_1e8

    .line 6775
    :catch_275
    move-exception v0

    const-string v0, "ListPopupWindow"

    const-string v3, "Could not call setClipToScreenEnabled() on PopupWindow. Oh well."

    invoke-static {v0, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_203

    :cond_27e
    move v1, v2

    .line 652
    goto :goto_20d

    :cond_280
    move v0, v2

    goto/16 :goto_ac

    :cond_283
    move-object v3, v0

    move v0, v2

    goto/16 :goto_a7

    .line 3113
    nop

    :pswitch_data_288
    .packed-switch 0x0
        :pswitch_12e
        :pswitch_126
    .end packed-switch

    .line 3182
    :pswitch_data_290
    .packed-switch -0x2
        :pswitch_173
        :pswitch_18e
    .end packed-switch
.end method

.method public final c()V
    .registers 3

    .prologue
    const/4 v1, 0x1

    .line 304
    iput-boolean v1, p0, Landroid/support/v7/widget/an;->M:Z

    .line 305
    iget-object v0, p0, Landroid/support/v7/widget/an;->c:Landroid/widget/PopupWindow;

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setFocusable(Z)V

    .line 306
    return-void
.end method

.method public final d()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 671
    iget-object v0, p0, Landroid/support/v7/widget/an;->c:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    .line 672
    invoke-direct {p0}, Landroid/support/v7/widget/an;->t()V

    .line 673
    iget-object v0, p0, Landroid/support/v7/widget/an;->c:Landroid/widget/PopupWindow;

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setContentView(Landroid/view/View;)V

    .line 674
    iput-object v1, p0, Landroid/support/v7/widget/an;->d:Landroid/support/v7/widget/ar;

    .line 675
    iget-object v0, p0, Landroid/support/v7/widget/an;->K:Landroid/os/Handler;

    iget-object v1, p0, Landroid/support/v7/widget/an;->F:Landroid/support/v7/widget/az;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 676
    return-void
.end method

.method public final e()V
    .registers 3

    .prologue
    .line 710
    iget-object v0, p0, Landroid/support/v7/widget/an;->c:Landroid/widget/PopupWindow;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setInputMethodMode(I)V

    .line 711
    return-void
.end method

.method public final f()V
    .registers 3

    .prologue
    .line 747
    iget-object v0, p0, Landroid/support/v7/widget/an;->d:Landroid/support/v7/widget/ar;

    .line 748
    if-eqz v0, :cond_b

    .line 750
    const/4 v1, 0x1

    invoke-static {v0, v1}, Landroid/support/v7/widget/ar;->a(Landroid/support/v7/widget/ar;Z)Z

    .line 752
    invoke-virtual {v0}, Landroid/support/v7/widget/ar;->requestLayout()V

    .line 754
    :cond_b
    return-void
.end method

.method public final g()Z
    .registers 3

    .prologue
    .line 768
    iget-object v0, p0, Landroid/support/v7/widget/an;->c:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->getInputMethodMode()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_b

    const/4 v0, 0x1

    :goto_a
    return v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method
