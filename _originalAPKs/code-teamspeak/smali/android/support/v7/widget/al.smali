.class public Landroid/support/v7/widget/al;
.super Landroid/view/ViewGroup$MarginLayoutParams;
.source "SourceFile"


# instance fields
.field public g:F

.field public h:I


# direct methods
.method public constructor <init>()V
    .registers 3

    .prologue
    const/4 v1, -0x1

    .line 1806
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(II)V

    .line 1771
    iput v1, p0, Landroid/support/v7/widget/al;->h:I

    .line 1807
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Landroid/support/v7/widget/al;->g:F

    .line 1808
    return-void
.end method

.method public constructor <init>(II)V
    .registers 4

    .prologue
    .line 1791
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(II)V

    .line 1771
    const/4 v0, -0x1

    iput v0, p0, Landroid/support/v7/widget/al;->h:I

    .line 1792
    const/4 v0, 0x0

    iput v0, p0, Landroid/support/v7/widget/al;->g:F

    .line 1793
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 7

    .prologue
    const/4 v3, -0x1

    .line 1777
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1771
    iput v3, p0, Landroid/support/v7/widget/al;->h:I

    .line 1778
    sget-object v0, Landroid/support/v7/a/n;->LinearLayoutCompat_Layout:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1781
    sget v1, Landroid/support/v7/a/n;->LinearLayoutCompat_Layout_android_layout_weight:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v1

    iput v1, p0, Landroid/support/v7/widget/al;->g:F

    .line 1782
    sget v1, Landroid/support/v7/a/n;->LinearLayoutCompat_Layout_android_layout_gravity:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Landroid/support/v7/widget/al;->h:I

    .line 1784
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1785
    return-void
.end method

.method private constructor <init>(Landroid/support/v7/widget/al;)V
    .registers 3

    .prologue
    .line 1831
    invoke-direct {p0, p1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    .line 1771
    const/4 v0, -0x1

    iput v0, p0, Landroid/support/v7/widget/al;->h:I

    .line 1833
    iget v0, p1, Landroid/support/v7/widget/al;->g:F

    iput v0, p0, Landroid/support/v7/widget/al;->g:F

    .line 1834
    iget v0, p1, Landroid/support/v7/widget/al;->h:I

    iput v0, p0, Landroid/support/v7/widget/al;->h:I

    .line 1835
    return-void
.end method

.method public constructor <init>(Landroid/view/ViewGroup$LayoutParams;)V
    .registers 3

    .prologue
    .line 1814
    invoke-direct {p0, p1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1771
    const/4 v0, -0x1

    iput v0, p0, Landroid/support/v7/widget/al;->h:I

    .line 1815
    return-void
.end method

.method private constructor <init>(Landroid/view/ViewGroup$MarginLayoutParams;)V
    .registers 3

    .prologue
    .line 1821
    invoke-direct {p0, p1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    .line 1771
    const/4 v0, -0x1

    iput v0, p0, Landroid/support/v7/widget/al;->h:I

    .line 1822
    return-void
.end method
