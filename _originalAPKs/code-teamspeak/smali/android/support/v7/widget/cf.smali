.class public final Landroid/support/v7/widget/cf;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Landroid/view/LayoutInflater;

.field private c:Landroid/view/LayoutInflater;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .registers 3
    .param p1    # Landroid/content/Context;
        .annotation build Landroid/support/a/y;
        .end annotation
    .end param

    .prologue
    .line 110
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 111
    iput-object p1, p0, Landroid/support/v7/widget/cf;->a:Landroid/content/Context;

    .line 112
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/widget/cf;->b:Landroid/view/LayoutInflater;

    .line 113
    return-void
.end method

.method private a()Landroid/content/res/Resources$Theme;
    .registers 2
    .annotation build Landroid/support/a/z;
    .end annotation

    .prologue
    .line 139
    iget-object v0, p0, Landroid/support/v7/widget/cf;->c:Landroid/view/LayoutInflater;

    if-nez v0, :cond_6

    const/4 v0, 0x0

    :goto_5
    return-object v0

    :cond_6
    iget-object v0, p0, Landroid/support/v7/widget/cf;->c:Landroid/view/LayoutInflater;

    invoke-virtual {v0}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    goto :goto_5
.end method

.method private a(Landroid/content/res/Resources$Theme;)V
    .registers 4
    .param p1    # Landroid/content/res/Resources$Theme;
        .annotation build Landroid/support/a/z;
        .end annotation
    .end param

    .prologue
    .line 123
    if-nez p1, :cond_6

    .line 124
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v7/widget/cf;->c:Landroid/view/LayoutInflater;

    .line 131
    :goto_5
    return-void

    .line 125
    :cond_6
    iget-object v0, p0, Landroid/support/v7/widget/cf;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    if-ne p1, v0, :cond_13

    .line 126
    iget-object v0, p0, Landroid/support/v7/widget/cf;->b:Landroid/view/LayoutInflater;

    iput-object v0, p0, Landroid/support/v7/widget/cf;->c:Landroid/view/LayoutInflater;

    goto :goto_5

    .line 128
    :cond_13
    new-instance v0, Landroid/support/v7/internal/view/b;

    iget-object v1, p0, Landroid/support/v7/widget/cf;->a:Landroid/content/Context;

    invoke-direct {v0, v1, p1}, Landroid/support/v7/internal/view/b;-><init>(Landroid/content/Context;Landroid/content/res/Resources$Theme;)V

    .line 129
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/widget/cf;->c:Landroid/view/LayoutInflater;

    goto :goto_5
.end method

.method private b()Landroid/view/LayoutInflater;
    .registers 2
    .annotation build Landroid/support/a/y;
    .end annotation

    .prologue
    .line 151
    iget-object v0, p0, Landroid/support/v7/widget/cf;->c:Landroid/view/LayoutInflater;

    if-eqz v0, :cond_7

    iget-object v0, p0, Landroid/support/v7/widget/cf;->c:Landroid/view/LayoutInflater;

    :goto_6
    return-object v0

    :cond_7
    iget-object v0, p0, Landroid/support/v7/widget/cf;->b:Landroid/view/LayoutInflater;

    goto :goto_6
.end method
