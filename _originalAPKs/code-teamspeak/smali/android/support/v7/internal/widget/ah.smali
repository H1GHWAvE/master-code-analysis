.class public Landroid/support/v7/internal/widget/ah;
.super Landroid/widget/ListView;
.source "SourceFile"


# static fields
.field public static final a:I = -0x1

.field public static final b:I = -0x1

.field private static final h:[I


# instance fields
.field final c:Landroid/graphics/Rect;

.field d:I

.field e:I

.field f:I

.field g:I

.field private i:Ljava/lang/reflect/Field;

.field private j:Landroid/support/v7/internal/widget/ai;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    const/4 v1, 0x0

    .line 45
    const/4 v0, 0x1

    new-array v0, v0, [I

    aput v1, v0, v1

    sput-object v0, Landroid/support/v7/internal/widget/ah;->h:[I

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .registers 3

    .prologue
    .line 58
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/support/v7/internal/widget/ah;-><init>(Landroid/content/Context;B)V

    .line 59
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;B)V
    .registers 4

    .prologue
    .line 62
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/support/v7/internal/widget/ah;-><init>(Landroid/content/Context;I)V

    .line 63
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .registers 5

    .prologue
    const/4 v1, 0x0

    .line 66
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 47
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Landroid/support/v7/internal/widget/ah;->c:Landroid/graphics/Rect;

    .line 48
    iput v1, p0, Landroid/support/v7/internal/widget/ah;->d:I

    .line 49
    iput v1, p0, Landroid/support/v7/internal/widget/ah;->e:I

    .line 50
    iput v1, p0, Landroid/support/v7/internal/widget/ah;->f:I

    .line 51
    iput v1, p0, Landroid/support/v7/internal/widget/ah;->g:I

    .line 69
    :try_start_14
    const-class v0, Landroid/widget/AbsListView;

    const-string v1, "mIsChildViewEnabled"

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/internal/widget/ah;->i:Ljava/lang/reflect/Field;

    .line 70
    iget-object v0, p0, Landroid/support/v7/internal/widget/ah;->i:Ljava/lang/reflect/Field;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->setAccessible(Z)V
    :try_end_24
    .catch Ljava/lang/NoSuchFieldException; {:try_start_14 .. :try_end_24} :catch_25

    .line 74
    :goto_24
    return-void

    .line 72
    :catch_25
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/NoSuchFieldException;->printStackTrace()V

    goto :goto_24
.end method

.method private a(ILandroid/view/View;)V
    .registers 13

    .prologue
    const/4 v9, -0x1

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 187
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ah;->getSelector()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    .line 188
    if-eqz v4, :cond_78

    if-eq p1, v9, :cond_78

    move v3, v0

    .line 189
    :goto_c
    if-eqz v3, :cond_11

    .line 190
    invoke-virtual {v4, v1, v1}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    .line 3205
    :cond_11
    iget-object v2, p0, Landroid/support/v7/internal/widget/ah;->c:Landroid/graphics/Rect;

    .line 3206
    invoke-virtual {p2}, Landroid/view/View;->getLeft()I

    move-result v5

    invoke-virtual {p2}, Landroid/view/View;->getTop()I

    move-result v6

    invoke-virtual {p2}, Landroid/view/View;->getRight()I

    move-result v7

    invoke-virtual {p2}, Landroid/view/View;->getBottom()I

    move-result v8

    invoke-virtual {v2, v5, v6, v7, v8}, Landroid/graphics/Rect;->set(IIII)V

    .line 3209
    iget v5, v2, Landroid/graphics/Rect;->left:I

    iget v6, p0, Landroid/support/v7/internal/widget/ah;->d:I

    sub-int/2addr v5, v6

    iput v5, v2, Landroid/graphics/Rect;->left:I

    .line 3210
    iget v5, v2, Landroid/graphics/Rect;->top:I

    iget v6, p0, Landroid/support/v7/internal/widget/ah;->e:I

    sub-int/2addr v5, v6

    iput v5, v2, Landroid/graphics/Rect;->top:I

    .line 3211
    iget v5, v2, Landroid/graphics/Rect;->right:I

    iget v6, p0, Landroid/support/v7/internal/widget/ah;->f:I

    add-int/2addr v5, v6

    iput v5, v2, Landroid/graphics/Rect;->right:I

    .line 3212
    iget v5, v2, Landroid/graphics/Rect;->bottom:I

    iget v6, p0, Landroid/support/v7/internal/widget/ah;->g:I

    add-int/2addr v5, v6

    iput v5, v2, Landroid/graphics/Rect;->bottom:I

    .line 3217
    :try_start_42
    iget-object v2, p0, Landroid/support/v7/internal/widget/ah;->i:Ljava/lang/reflect/Field;

    invoke-virtual {v2, p0}, Ljava/lang/reflect/Field;->getBoolean(Ljava/lang/Object;)Z

    move-result v2

    .line 3218
    invoke-virtual {p2}, Landroid/view/View;->isEnabled()Z

    move-result v5

    if-eq v5, v2, :cond_5f

    .line 3219
    iget-object v5, p0, Landroid/support/v7/internal/widget/ah;->i:Ljava/lang/reflect/Field;

    if-nez v2, :cond_7a

    move v2, v0

    :goto_53
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v5, p0, v2}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 3220
    if-eq p1, v9, :cond_5f

    .line 3221
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ah;->refreshDrawableState()V
    :try_end_5f
    .catch Ljava/lang/IllegalAccessException; {:try_start_42 .. :try_end_5f} :catch_7c

    .line 195
    :cond_5f
    :goto_5f
    if-eqz v3, :cond_77

    .line 196
    iget-object v2, p0, Landroid/support/v7/internal/widget/ah;->c:Landroid/graphics/Rect;

    .line 197
    invoke-virtual {v2}, Landroid/graphics/Rect;->exactCenterX()F

    move-result v3

    .line 198
    invoke-virtual {v2}, Landroid/graphics/Rect;->exactCenterY()F

    move-result v2

    .line 199
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ah;->getVisibility()I

    move-result v5

    if-nez v5, :cond_81

    :goto_71
    invoke-virtual {v4, v0, v1}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    .line 200
    invoke-static {v4, v3, v2}, Landroid/support/v4/e/a/a;->a(Landroid/graphics/drawable/Drawable;FF)V

    .line 202
    :cond_77
    return-void

    :cond_78
    move v3, v1

    .line 188
    goto :goto_c

    :cond_7a
    move v2, v1

    .line 3219
    goto :goto_53

    .line 3225
    :catch_7c
    move-exception v2

    invoke-virtual {v2}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_5f

    :cond_81
    move v0, v1

    .line 199
    goto :goto_71
.end method

.method private a(Landroid/graphics/Canvas;)V
    .registers 4

    .prologue
    .line 126
    iget-object v0, p0, Landroid/support/v7/internal/widget/ah;->c:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_16

    .line 127
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ah;->getSelector()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 128
    if-eqz v0, :cond_16

    .line 129
    iget-object v1, p0, Landroid/support/v7/internal/widget/ah;->c:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 130
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 133
    :cond_16
    return-void
.end method

.method private b()V
    .registers 3

    .prologue
    .line 111
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ah;->getSelector()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 112
    if-eqz v1, :cond_1c

    .line 2118
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ah;->a()Z

    move-result v0

    if-eqz v0, :cond_1d

    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ah;->isPressed()Z

    move-result v0

    if-eqz v0, :cond_1d

    const/4 v0, 0x1

    .line 112
    :goto_13
    if-eqz v0, :cond_1c

    .line 113
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ah;->getDrawableState()[I

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 115
    :cond_1c
    return-void

    .line 2118
    :cond_1d
    const/4 v0, 0x0

    goto :goto_13
.end method

.method private b(ILandroid/view/View;)V
    .registers 8

    .prologue
    .line 205
    iget-object v0, p0, Landroid/support/v7/internal/widget/ah;->c:Landroid/graphics/Rect;

    .line 206
    invoke-virtual {p2}, Landroid/view/View;->getLeft()I

    move-result v1

    invoke-virtual {p2}, Landroid/view/View;->getTop()I

    move-result v2

    invoke-virtual {p2}, Landroid/view/View;->getRight()I

    move-result v3

    invoke-virtual {p2}, Landroid/view/View;->getBottom()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 209
    iget v1, v0, Landroid/graphics/Rect;->left:I

    iget v2, p0, Landroid/support/v7/internal/widget/ah;->d:I

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->left:I

    .line 210
    iget v1, v0, Landroid/graphics/Rect;->top:I

    iget v2, p0, Landroid/support/v7/internal/widget/ah;->e:I

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->top:I

    .line 211
    iget v1, v0, Landroid/graphics/Rect;->right:I

    iget v2, p0, Landroid/support/v7/internal/widget/ah;->f:I

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->right:I

    .line 212
    iget v1, v0, Landroid/graphics/Rect;->bottom:I

    iget v2, p0, Landroid/support/v7/internal/widget/ah;->g:I

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    .line 217
    :try_start_31
    iget-object v0, p0, Landroid/support/v7/internal/widget/ah;->i:Ljava/lang/reflect/Field;

    invoke-virtual {v0, p0}, Ljava/lang/reflect/Field;->getBoolean(Ljava/lang/Object;)Z

    move-result v0

    .line 218
    invoke-virtual {p2}, Landroid/view/View;->isEnabled()Z

    move-result v1

    if-eq v1, v0, :cond_4f

    .line 219
    iget-object v1, p0, Landroid/support/v7/internal/widget/ah;->i:Ljava/lang/reflect/Field;

    if-nez v0, :cond_50

    const/4 v0, 0x1

    :goto_42
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v1, p0, v0}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 220
    const/4 v0, -0x1

    if-eq p1, v0, :cond_4f

    .line 221
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ah;->refreshDrawableState()V
    :try_end_4f
    .catch Ljava/lang/IllegalAccessException; {:try_start_31 .. :try_end_4f} :catch_52

    .line 227
    :cond_4f
    :goto_4f
    return-void

    .line 219
    :cond_50
    const/4 v0, 0x0

    goto :goto_42

    .line 225
    :catch_52
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_4f
.end method

.method private c()Z
    .registers 2

    .prologue
    .line 118
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ah;->a()Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ah;->isPressed()Z

    move-result v0

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method


# virtual methods
.method public final a(II)I
    .registers 14

    .prologue
    const/4 v5, 0x0

    const/4 v1, 0x0

    .line 259
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ah;->getListPaddingTop()I

    move-result v2

    .line 260
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ah;->getListPaddingBottom()I

    move-result v3

    .line 261
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ah;->getListPaddingLeft()I

    .line 262
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ah;->getListPaddingRight()I

    .line 263
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ah;->getDividerHeight()I

    move-result v0

    .line 264
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ah;->getDivider()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    .line 266
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ah;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v8

    .line 268
    if-nez v8, :cond_21

    .line 269
    add-int p2, v2, v3

    .line 328
    :cond_20
    :goto_20
    return p2

    .line 273
    :cond_21
    add-int/2addr v3, v2

    .line 274
    if-lez v0, :cond_5f

    if-eqz v4, :cond_5f

    .line 283
    :goto_26
    invoke-interface {v8}, Landroid/widget/ListAdapter;->getCount()I

    move-result v9

    move v7, v1

    move v4, v1

    move-object v6, v5

    .line 284
    :goto_2d
    if-ge v7, v9, :cond_66

    .line 285
    invoke-interface {v8, v7}, Landroid/widget/ListAdapter;->getItemViewType(I)I

    move-result v2

    .line 286
    if-eq v2, v4, :cond_68

    move v4, v2

    move-object v2, v5

    .line 290
    :goto_37
    invoke-interface {v8, v7, v2, p0}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    .line 294
    invoke-virtual {v6}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    .line 295
    if-eqz v2, :cond_61

    iget v10, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    if-lez v10, :cond_61

    .line 296
    iget v2, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    const/high16 v10, 0x40000000    # 2.0f

    invoke-static {v2, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 301
    :goto_4d
    invoke-virtual {v6, p1, v2}, Landroid/view/View;->measure(II)V

    .line 303
    if-lez v7, :cond_6a

    .line 305
    add-int v2, v3, v0

    .line 308
    :goto_54
    invoke-virtual {v6}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    add-int/2addr v3, v2

    .line 310
    if-ge v3, p2, :cond_20

    .line 284
    add-int/lit8 v2, v7, 0x1

    move v7, v2

    goto :goto_2d

    :cond_5f
    move v0, v1

    .line 274
    goto :goto_26

    .line 299
    :cond_61
    invoke-static {v1, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    goto :goto_4d

    :cond_66
    move p2, v3

    .line 328
    goto :goto_20

    :cond_68
    move-object v2, v6

    goto :goto_37

    :cond_6a
    move v2, v3

    goto :goto_54
.end method

.method public final a(IZ)I
    .registers 7

    .prologue
    const/4 v0, -0x1

    .line 144
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ah;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v1

    .line 145
    if-eqz v1, :cond_d

    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ah;->isInTouchMode()Z

    move-result v2

    if-eqz v2, :cond_f

    :cond_d
    move p1, v0

    .line 171
    :cond_e
    :goto_e
    return p1

    .line 149
    :cond_f
    invoke-interface {v1}, Landroid/widget/ListAdapter;->getCount()I

    move-result v2

    .line 150
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ah;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v3

    invoke-interface {v3}, Landroid/widget/ListAdapter;->areAllItemsEnabled()Z

    move-result v3

    if-nez v3, :cond_46

    .line 151
    if-eqz p2, :cond_2f

    .line 152
    const/4 v3, 0x0

    invoke-static {v3, p1}, Ljava/lang/Math;->max(II)I

    move-result p1

    .line 153
    :goto_24
    if-ge p1, v2, :cond_40

    invoke-interface {v1, p1}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    move-result v3

    if-nez v3, :cond_40

    .line 154
    add-int/lit8 p1, p1, 0x1

    goto :goto_24

    .line 157
    :cond_2f
    add-int/lit8 v3, v2, -0x1

    invoke-static {p1, v3}, Ljava/lang/Math;->min(II)I

    move-result p1

    .line 158
    :goto_35
    if-ltz p1, :cond_40

    invoke-interface {v1, p1}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    move-result v3

    if-nez v3, :cond_40

    .line 159
    add-int/lit8 p1, p1, -0x1

    goto :goto_35

    .line 163
    :cond_40
    if-ltz p1, :cond_44

    if-lt p1, v2, :cond_e

    :cond_44
    move p1, v0

    .line 164
    goto :goto_e

    .line 168
    :cond_46
    if-ltz p1, :cond_4a

    if-lt p1, v2, :cond_e

    :cond_4a
    move p1, v0

    .line 169
    goto :goto_e
.end method

.method public final a(ILandroid/view/View;FF)V
    .registers 15

    .prologue
    const/4 v0, 0x1

    const/4 v9, -0x1

    const/4 v1, 0x0

    .line 176
    .line 2187
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ah;->getSelector()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    .line 2188
    if-eqz v4, :cond_83

    if-eq p1, v9, :cond_83

    move v3, v0

    .line 2189
    :goto_c
    if-eqz v3, :cond_11

    .line 2190
    invoke-virtual {v4, v1, v1}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    .line 2205
    :cond_11
    iget-object v2, p0, Landroid/support/v7/internal/widget/ah;->c:Landroid/graphics/Rect;

    .line 2206
    invoke-virtual {p2}, Landroid/view/View;->getLeft()I

    move-result v5

    invoke-virtual {p2}, Landroid/view/View;->getTop()I

    move-result v6

    invoke-virtual {p2}, Landroid/view/View;->getRight()I

    move-result v7

    invoke-virtual {p2}, Landroid/view/View;->getBottom()I

    move-result v8

    invoke-virtual {v2, v5, v6, v7, v8}, Landroid/graphics/Rect;->set(IIII)V

    .line 2209
    iget v5, v2, Landroid/graphics/Rect;->left:I

    iget v6, p0, Landroid/support/v7/internal/widget/ah;->d:I

    sub-int/2addr v5, v6

    iput v5, v2, Landroid/graphics/Rect;->left:I

    .line 2210
    iget v5, v2, Landroid/graphics/Rect;->top:I

    iget v6, p0, Landroid/support/v7/internal/widget/ah;->e:I

    sub-int/2addr v5, v6

    iput v5, v2, Landroid/graphics/Rect;->top:I

    .line 2211
    iget v5, v2, Landroid/graphics/Rect;->right:I

    iget v6, p0, Landroid/support/v7/internal/widget/ah;->f:I

    add-int/2addr v5, v6

    iput v5, v2, Landroid/graphics/Rect;->right:I

    .line 2212
    iget v5, v2, Landroid/graphics/Rect;->bottom:I

    iget v6, p0, Landroid/support/v7/internal/widget/ah;->g:I

    add-int/2addr v5, v6

    iput v5, v2, Landroid/graphics/Rect;->bottom:I

    .line 2217
    :try_start_42
    iget-object v2, p0, Landroid/support/v7/internal/widget/ah;->i:Ljava/lang/reflect/Field;

    invoke-virtual {v2, p0}, Ljava/lang/reflect/Field;->getBoolean(Ljava/lang/Object;)Z

    move-result v2

    .line 2218
    invoke-virtual {p2}, Landroid/view/View;->isEnabled()Z

    move-result v5

    if-eq v5, v2, :cond_5f

    .line 2219
    iget-object v5, p0, Landroid/support/v7/internal/widget/ah;->i:Ljava/lang/reflect/Field;

    if-nez v2, :cond_85

    move v2, v0

    :goto_53
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v5, p0, v2}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 2220
    if-eq p1, v9, :cond_5f

    .line 2221
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ah;->refreshDrawableState()V
    :try_end_5f
    .catch Ljava/lang/IllegalAccessException; {:try_start_42 .. :try_end_5f} :catch_87

    .line 2195
    :cond_5f
    :goto_5f
    if-eqz v3, :cond_77

    .line 2196
    iget-object v2, p0, Landroid/support/v7/internal/widget/ah;->c:Landroid/graphics/Rect;

    .line 2197
    invoke-virtual {v2}, Landroid/graphics/Rect;->exactCenterX()F

    move-result v3

    .line 2198
    invoke-virtual {v2}, Landroid/graphics/Rect;->exactCenterY()F

    move-result v2

    .line 2199
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ah;->getVisibility()I

    move-result v5

    if-nez v5, :cond_8c

    :goto_71
    invoke-virtual {v4, v0, v1}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    .line 2200
    invoke-static {v4, v3, v2}, Landroid/support/v4/e/a/a;->a(Landroid/graphics/drawable/Drawable;FF)V

    .line 178
    :cond_77
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ah;->getSelector()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 179
    if-eqz v0, :cond_82

    if-eq p1, v9, :cond_82

    .line 180
    invoke-static {v0, p3, p4}, Landroid/support/v4/e/a/a;->a(Landroid/graphics/drawable/Drawable;FF)V

    .line 182
    :cond_82
    return-void

    :cond_83
    move v3, v1

    .line 2188
    goto :goto_c

    :cond_85
    move v2, v1

    .line 2219
    goto :goto_53

    .line 2225
    :catch_87
    move-exception v2

    invoke-virtual {v2}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_5f

    :cond_8c
    move v0, v1

    .line 2199
    goto :goto_71
.end method

.method public a()Z
    .registers 2

    .prologue
    .line 122
    const/4 v0, 0x0

    return v0
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .registers 4

    .prologue
    .line 104
    .line 1126
    iget-object v0, p0, Landroid/support/v7/internal/widget/ah;->c:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_16

    .line 1127
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ah;->getSelector()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1128
    if-eqz v0, :cond_16

    .line 1129
    iget-object v1, p0, Landroid/support/v7/internal/widget/ah;->c:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 1130
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 107
    :cond_16
    invoke-super {p0, p1}, Landroid/widget/ListView;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 108
    return-void
.end method

.method public drawableStateChanged()V
    .registers 4

    .prologue
    const/4 v0, 0x1

    .line 94
    invoke-super {p0}, Landroid/widget/ListView;->drawableStateChanged()V

    .line 96
    invoke-virtual {p0, v0}, Landroid/support/v7/internal/widget/ah;->setSelectorEnabled(Z)V

    .line 1111
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ah;->getSelector()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 1112
    if-eqz v1, :cond_22

    .line 1118
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ah;->a()Z

    move-result v2

    if-eqz v2, :cond_23

    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ah;->isPressed()Z

    move-result v2

    if-eqz v2, :cond_23

    .line 1112
    :goto_19
    if-eqz v0, :cond_22

    .line 1113
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ah;->getDrawableState()[I

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 98
    :cond_22
    return-void

    .line 1118
    :cond_23
    const/4 v0, 0x0

    goto :goto_19
.end method

.method public setSelector(Landroid/graphics/drawable/Drawable;)V
    .registers 4

    .prologue
    .line 78
    if-eqz p1, :cond_29

    new-instance v0, Landroid/support/v7/internal/widget/ai;

    invoke-direct {v0, p1}, Landroid/support/v7/internal/widget/ai;-><init>(Landroid/graphics/drawable/Drawable;)V

    :goto_7
    iput-object v0, p0, Landroid/support/v7/internal/widget/ah;->j:Landroid/support/v7/internal/widget/ai;

    .line 79
    iget-object v0, p0, Landroid/support/v7/internal/widget/ah;->j:Landroid/support/v7/internal/widget/ai;

    invoke-super {p0, v0}, Landroid/widget/ListView;->setSelector(Landroid/graphics/drawable/Drawable;)V

    .line 81
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 82
    if-eqz p1, :cond_18

    .line 83
    invoke-virtual {p1, v0}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    .line 86
    :cond_18
    iget v1, v0, Landroid/graphics/Rect;->left:I

    iput v1, p0, Landroid/support/v7/internal/widget/ah;->d:I

    .line 87
    iget v1, v0, Landroid/graphics/Rect;->top:I

    iput v1, p0, Landroid/support/v7/internal/widget/ah;->e:I

    .line 88
    iget v1, v0, Landroid/graphics/Rect;->right:I

    iput v1, p0, Landroid/support/v7/internal/widget/ah;->f:I

    .line 89
    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    iput v0, p0, Landroid/support/v7/internal/widget/ah;->g:I

    .line 90
    return-void

    .line 78
    :cond_29
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public setSelectorEnabled(Z)V
    .registers 3

    .prologue
    .line 332
    iget-object v0, p0, Landroid/support/v7/internal/widget/ah;->j:Landroid/support/v7/internal/widget/ai;

    if-eqz v0, :cond_8

    .line 333
    iget-object v0, p0, Landroid/support/v7/internal/widget/ah;->j:Landroid/support/v7/internal/widget/ai;

    .line 3346
    iput-boolean p1, v0, Landroid/support/v7/internal/widget/ai;->a:Z

    .line 335
    :cond_8
    return-void
.end method
