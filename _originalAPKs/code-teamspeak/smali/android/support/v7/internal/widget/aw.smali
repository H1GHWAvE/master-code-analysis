.class final Landroid/support/v7/internal/widget/aw;
.super Landroid/support/v4/n/j;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 547
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Landroid/support/v4/n/j;-><init>(I)V

    .line 548
    return-void
.end method

.method static a(ILandroid/graphics/PorterDuff$Mode;)I
    .registers 4

    .prologue
    .line 560
    add-int/lit8 v0, p0, 0x1f

    .line 561
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p1}, Landroid/graphics/PorterDuff$Mode;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 562
    return v0
.end method

.method private a(ILandroid/graphics/PorterDuff$Mode;Landroid/graphics/PorterDuffColorFilter;)Landroid/graphics/PorterDuffColorFilter;
    .registers 5

    .prologue
    .line 555
    invoke-static {p1, p2}, Landroid/support/v7/internal/widget/aw;->a(ILandroid/graphics/PorterDuff$Mode;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p0, v0, p3}, Landroid/support/v7/internal/widget/aw;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/PorterDuffColorFilter;

    return-object v0
.end method

.method private b(ILandroid/graphics/PorterDuff$Mode;)Landroid/graphics/PorterDuffColorFilter;
    .registers 4

    .prologue
    .line 551
    invoke-static {p1, p2}, Landroid/support/v7/internal/widget/aw;->a(ILandroid/graphics/PorterDuff$Mode;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/support/v7/internal/widget/aw;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/PorterDuffColorFilter;

    return-object v0
.end method
