.class final Landroid/support/v7/internal/widget/t;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# instance fields
.field final synthetic a:Landroid/support/v7/internal/widget/l;


# direct methods
.method private constructor <init>(Landroid/support/v7/internal/widget/l;)V
    .registers 2

    .prologue
    .line 1035
    iput-object p1, p0, Landroid/support/v7/internal/widget/t;->a:Landroid/support/v7/internal/widget/l;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Landroid/support/v7/internal/widget/l;B)V
    .registers 3

    .prologue
    .line 1035
    invoke-direct {p0, p1}, Landroid/support/v7/internal/widget/t;-><init>(Landroid/support/v7/internal/widget/l;)V

    return-void
.end method

.method private varargs a([Ljava/lang/Object;)Ljava/lang/Void;
    .registers 13

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v10, 0x0

    .line 1040
    aget-object v0, p1, v2

    check-cast v0, Ljava/util/List;

    .line 1041
    aget-object v1, p1, v1

    check-cast v1, Ljava/lang/String;

    .line 1046
    :try_start_b
    iget-object v3, p0, Landroid/support/v7/internal/widget/t;->a:Landroid/support/v7/internal/widget/l;

    invoke-static {v3}, Landroid/support/v7/internal/widget/l;->a(Landroid/support/v7/internal/widget/l;)Landroid/content/Context;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v1, v4}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;
    :try_end_15
    .catch Ljava/io/FileNotFoundException; {:try_start_b .. :try_end_15} :catch_6f

    move-result-object v3

    .line 1052
    invoke-static {}, Landroid/util/Xml;->newSerializer()Lorg/xmlpull/v1/XmlSerializer;

    move-result-object v4

    .line 1055
    const/4 v1, 0x0

    :try_start_1b
    invoke-interface {v4, v3, v1}, Lorg/xmlpull/v1/XmlSerializer;->setOutput(Ljava/io/OutputStream;Ljava/lang/String;)V

    .line 1056
    const-string v1, "UTF-8"

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-interface {v4, v1, v5}, Lorg/xmlpull/v1/XmlSerializer;->startDocument(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1057
    const/4 v1, 0x0

    const-string v5, "historical-records"

    invoke-interface {v4, v1, v5}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 1059
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    .line 1060
    :goto_32
    if-ge v2, v5, :cond_87

    .line 1061
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v7/internal/widget/r;

    .line 1062
    const/4 v6, 0x0

    const-string v7, "historical-record"

    invoke-interface {v4, v6, v7}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 1063
    const/4 v6, 0x0

    const-string v7, "activity"

    iget-object v8, v1, Landroid/support/v7/internal/widget/r;->a:Landroid/content/ComponentName;

    invoke-virtual {v8}, Landroid/content/ComponentName;->flattenToString()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v4, v6, v7, v8}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 1065
    const/4 v6, 0x0

    const-string v7, "time"

    iget-wide v8, v1, Landroid/support/v7/internal/widget/r;->b:J

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v4, v6, v7, v8}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 1066
    const/4 v6, 0x0

    const-string v7, "weight"

    iget v1, v1, Landroid/support/v7/internal/widget/r;->c:F

    invoke-static {v1}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v4, v6, v7, v1}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 1067
    const/4 v1, 0x0

    const-string v6, "historical-record"

    invoke-interface {v4, v1, v6}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;
    :try_end_6b
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1b .. :try_end_6b} :catch_9d
    .catch Ljava/lang/IllegalStateException; {:try_start_1b .. :try_end_6b} :catch_c7
    .catch Ljava/io/IOException; {:try_start_1b .. :try_end_6b} :catch_f1
    .catchall {:try_start_1b .. :try_end_6b} :catchall_11d

    .line 1060
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_32

    .line 1047
    :catch_6f
    move-exception v0

    .line 1048
    invoke-static {}, Landroid/support/v7/internal/widget/l;->e()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Error writing historical recrod file: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1095
    :cond_86
    :goto_86
    return-object v10

    .line 1073
    :cond_87
    const/4 v0, 0x0

    :try_start_88
    const-string v1, "historical-records"

    invoke-interface {v4, v0, v1}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 1074
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlSerializer;->endDocument()V
    :try_end_90
    .catch Ljava/lang/IllegalArgumentException; {:try_start_88 .. :try_end_90} :catch_9d
    .catch Ljava/lang/IllegalStateException; {:try_start_88 .. :try_end_90} :catch_c7
    .catch Ljava/io/IOException; {:try_start_88 .. :try_end_90} :catch_f1
    .catchall {:try_start_88 .. :try_end_90} :catchall_11d

    .line 1086
    iget-object v0, p0, Landroid/support/v7/internal/widget/t;->a:Landroid/support/v7/internal/widget/l;

    invoke-static {v0}, Landroid/support/v7/internal/widget/l;->c(Landroid/support/v7/internal/widget/l;)Z

    .line 1087
    if-eqz v3, :cond_86

    .line 1089
    :try_start_97
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_9a
    .catch Ljava/io/IOException; {:try_start_97 .. :try_end_9a} :catch_9b

    goto :goto_86

    .line 1092
    :catch_9b
    move-exception v0

    goto :goto_86

    .line 1079
    :catch_9d
    move-exception v0

    .line 1080
    :try_start_9e
    invoke-static {}, Landroid/support/v7/internal/widget/l;->e()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "Error writing historical recrod file: "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Landroid/support/v7/internal/widget/t;->a:Landroid/support/v7/internal/widget/l;

    invoke-static {v4}, Landroid/support/v7/internal/widget/l;->b(Landroid/support/v7/internal/widget/l;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_ba
    .catchall {:try_start_9e .. :try_end_ba} :catchall_11d

    .line 1086
    iget-object v0, p0, Landroid/support/v7/internal/widget/t;->a:Landroid/support/v7/internal/widget/l;

    invoke-static {v0}, Landroid/support/v7/internal/widget/l;->c(Landroid/support/v7/internal/widget/l;)Z

    .line 1087
    if-eqz v3, :cond_86

    .line 1089
    :try_start_c1
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_c4
    .catch Ljava/io/IOException; {:try_start_c1 .. :try_end_c4} :catch_c5

    goto :goto_86

    .line 1092
    :catch_c5
    move-exception v0

    goto :goto_86

    .line 1081
    :catch_c7
    move-exception v0

    .line 1082
    :try_start_c8
    invoke-static {}, Landroid/support/v7/internal/widget/l;->e()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "Error writing historical recrod file: "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Landroid/support/v7/internal/widget/t;->a:Landroid/support/v7/internal/widget/l;

    invoke-static {v4}, Landroid/support/v7/internal/widget/l;->b(Landroid/support/v7/internal/widget/l;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_e4
    .catchall {:try_start_c8 .. :try_end_e4} :catchall_11d

    .line 1086
    iget-object v0, p0, Landroid/support/v7/internal/widget/t;->a:Landroid/support/v7/internal/widget/l;

    invoke-static {v0}, Landroid/support/v7/internal/widget/l;->c(Landroid/support/v7/internal/widget/l;)Z

    .line 1087
    if-eqz v3, :cond_86

    .line 1089
    :try_start_eb
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_ee
    .catch Ljava/io/IOException; {:try_start_eb .. :try_end_ee} :catch_ef

    goto :goto_86

    .line 1092
    :catch_ef
    move-exception v0

    goto :goto_86

    .line 1083
    :catch_f1
    move-exception v0

    .line 1084
    :try_start_f2
    invoke-static {}, Landroid/support/v7/internal/widget/l;->e()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "Error writing historical recrod file: "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Landroid/support/v7/internal/widget/t;->a:Landroid/support/v7/internal/widget/l;

    invoke-static {v4}, Landroid/support/v7/internal/widget/l;->b(Landroid/support/v7/internal/widget/l;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_10e
    .catchall {:try_start_f2 .. :try_end_10e} :catchall_11d

    .line 1086
    iget-object v0, p0, Landroid/support/v7/internal/widget/t;->a:Landroid/support/v7/internal/widget/l;

    invoke-static {v0}, Landroid/support/v7/internal/widget/l;->c(Landroid/support/v7/internal/widget/l;)Z

    .line 1087
    if-eqz v3, :cond_86

    .line 1089
    :try_start_115
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_118
    .catch Ljava/io/IOException; {:try_start_115 .. :try_end_118} :catch_11a

    goto/16 :goto_86

    .line 1092
    :catch_11a
    move-exception v0

    goto/16 :goto_86

    .line 1086
    :catchall_11d
    move-exception v0

    iget-object v1, p0, Landroid/support/v7/internal/widget/t;->a:Landroid/support/v7/internal/widget/l;

    invoke-static {v1}, Landroid/support/v7/internal/widget/l;->c(Landroid/support/v7/internal/widget/l;)Z

    .line 1087
    if-eqz v3, :cond_128

    .line 1089
    :try_start_125
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_128
    .catch Ljava/io/IOException; {:try_start_125 .. :try_end_128} :catch_129

    .line 1092
    :cond_128
    :goto_128
    throw v0

    :catch_129
    move-exception v1

    goto :goto_128
.end method


# virtual methods
.method public final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 1035
    invoke-direct {p0, p1}, Landroid/support/v7/internal/widget/t;->a([Ljava/lang/Object;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method
