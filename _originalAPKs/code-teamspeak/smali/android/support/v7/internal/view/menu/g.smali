.class public final Landroid/support/v7/internal/view/menu/g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/support/v7/internal/view/menu/x;
.implements Landroid/widget/AdapterView$OnItemClickListener;


# static fields
.field public static final i:Ljava/lang/String; = "android:menu:list"

.field private static final j:Ljava/lang/String; = "ListMenuPresenter"


# instance fields
.field a:Landroid/content/Context;

.field b:Landroid/view/LayoutInflater;

.field c:Landroid/support/v7/internal/view/menu/i;

.field d:Landroid/support/v7/internal/view/menu/ExpandedMenuView;

.field e:I

.field f:I

.field public g:Landroid/support/v7/internal/view/menu/y;

.field h:Landroid/support/v7/internal/view/menu/h;

.field private k:I

.field private l:I


# direct methods
.method private constructor <init>(I)V
    .registers 3

    .prologue
    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    iput p1, p0, Landroid/support/v7/internal/view/menu/g;->f:I

    .line 79
    const/4 v0, 0x0

    iput v0, p0, Landroid/support/v7/internal/view/menu/g;->e:I

    .line 80
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .registers 4

    .prologue
    .line 67
    invoke-direct {p0, p2}, Landroid/support/v7/internal/view/menu/g;-><init>(I)V

    .line 68
    iput-object p1, p0, Landroid/support/v7/internal/view/menu/g;->a:Landroid/content/Context;

    .line 69
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/g;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/internal/view/menu/g;->b:Landroid/view/LayoutInflater;

    .line 70
    return-void
.end method

.method static synthetic a(Landroid/support/v7/internal/view/menu/g;)I
    .registers 2

    .prologue
    .line 40
    iget v0, p0, Landroid/support/v7/internal/view/menu/g;->k:I

    return v0
.end method

.method private a(I)V
    .registers 3

    .prologue
    .line 161
    iput p1, p0, Landroid/support/v7/internal/view/menu/g;->k:I

    .line 162
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/g;->d:Landroid/support/v7/internal/view/menu/ExpandedMenuView;

    if-eqz v0, :cond_a

    .line 163
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/support/v7/internal/view/menu/g;->a(Z)V

    .line 165
    :cond_a
    return-void
.end method

.method private a(Landroid/os/Bundle;)V
    .registers 4

    .prologue
    .line 186
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    .line 187
    iget-object v1, p0, Landroid/support/v7/internal/view/menu/g;->d:Landroid/support/v7/internal/view/menu/ExpandedMenuView;

    if-eqz v1, :cond_e

    .line 188
    iget-object v1, p0, Landroid/support/v7/internal/view/menu/g;->d:Landroid/support/v7/internal/view/menu/ExpandedMenuView;

    invoke-virtual {v1, v0}, Landroid/view/View;->saveHierarchyState(Landroid/util/SparseArray;)V

    .line 190
    :cond_e
    const-string v1, "android:menu:list"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putSparseParcelableArray(Ljava/lang/String;Landroid/util/SparseArray;)V

    .line 191
    return-void
.end method

.method private b(I)V
    .registers 2

    .prologue
    .line 201
    iput p1, p0, Landroid/support/v7/internal/view/menu/g;->l:I

    .line 202
    return-void
.end method

.method private b(Landroid/os/Bundle;)V
    .registers 4

    .prologue
    .line 194
    const-string v0, "android:menu:list"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSparseParcelableArray(Ljava/lang/String;)Landroid/util/SparseArray;

    move-result-object v0

    .line 195
    if-eqz v0, :cond_d

    .line 196
    iget-object v1, p0, Landroid/support/v7/internal/view/menu/g;->d:Landroid/support/v7/internal/view/menu/ExpandedMenuView;

    invoke-virtual {v1, v0}, Landroid/view/View;->restoreHierarchyState(Landroid/util/SparseArray;)V

    .line 198
    :cond_d
    return-void
.end method

.method private e()I
    .registers 2

    .prologue
    .line 157
    iget v0, p0, Landroid/support/v7/internal/view/menu/g;->k:I

    return v0
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;)Landroid/support/v7/internal/view/menu/z;
    .registers 5

    .prologue
    .line 101
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/g;->d:Landroid/support/v7/internal/view/menu/ExpandedMenuView;

    if-nez v0, :cond_28

    .line 102
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/g;->b:Landroid/view/LayoutInflater;

    sget v1, Landroid/support/v7/a/k;->abc_expanded_menu_layout:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/view/menu/ExpandedMenuView;

    iput-object v0, p0, Landroid/support/v7/internal/view/menu/g;->d:Landroid/support/v7/internal/view/menu/ExpandedMenuView;

    .line 104
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/g;->h:Landroid/support/v7/internal/view/menu/h;

    if-nez v0, :cond_1c

    .line 105
    new-instance v0, Landroid/support/v7/internal/view/menu/h;

    invoke-direct {v0, p0}, Landroid/support/v7/internal/view/menu/h;-><init>(Landroid/support/v7/internal/view/menu/g;)V

    iput-object v0, p0, Landroid/support/v7/internal/view/menu/g;->h:Landroid/support/v7/internal/view/menu/h;

    .line 107
    :cond_1c
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/g;->d:Landroid/support/v7/internal/view/menu/ExpandedMenuView;

    iget-object v1, p0, Landroid/support/v7/internal/view/menu/g;->h:Landroid/support/v7/internal/view/menu/h;

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/view/menu/ExpandedMenuView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 108
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/g;->d:Landroid/support/v7/internal/view/menu/ExpandedMenuView;

    invoke-virtual {v0, p0}, Landroid/support/v7/internal/view/menu/ExpandedMenuView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 110
    :cond_28
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/g;->d:Landroid/support/v7/internal/view/menu/ExpandedMenuView;

    return-object v0
.end method

.method public final a(Landroid/content/Context;Landroid/support/v7/internal/view/menu/i;)V
    .registers 5

    .prologue
    .line 84
    iget v0, p0, Landroid/support/v7/internal/view/menu/g;->e:I

    if-eqz v0, :cond_21

    .line 85
    new-instance v0, Landroid/view/ContextThemeWrapper;

    iget v1, p0, Landroid/support/v7/internal/view/menu/g;->e:I

    invoke-direct {v0, p1, v1}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Landroid/support/v7/internal/view/menu/g;->a:Landroid/content/Context;

    .line 86
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/g;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/internal/view/menu/g;->b:Landroid/view/LayoutInflater;

    .line 93
    :cond_15
    :goto_15
    iput-object p2, p0, Landroid/support/v7/internal/view/menu/g;->c:Landroid/support/v7/internal/view/menu/i;

    .line 94
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/g;->h:Landroid/support/v7/internal/view/menu/h;

    if-eqz v0, :cond_20

    .line 95
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/g;->h:Landroid/support/v7/internal/view/menu/h;

    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/h;->notifyDataSetChanged()V

    .line 97
    :cond_20
    return-void

    .line 87
    :cond_21
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/g;->a:Landroid/content/Context;

    if-eqz v0, :cond_15

    .line 88
    iput-object p1, p0, Landroid/support/v7/internal/view/menu/g;->a:Landroid/content/Context;

    .line 89
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/g;->b:Landroid/view/LayoutInflater;

    if-nez v0, :cond_15

    .line 90
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/g;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/internal/view/menu/g;->b:Landroid/view/LayoutInflater;

    goto :goto_15
.end method

.method public final a(Landroid/os/Parcelable;)V
    .registers 4

    .prologue
    .line 222
    check-cast p1, Landroid/os/Bundle;

    .line 8194
    const-string v0, "android:menu:list"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSparseParcelableArray(Ljava/lang/String;)Landroid/util/SparseArray;

    move-result-object v0

    .line 8195
    if-eqz v0, :cond_f

    .line 8196
    iget-object v1, p0, Landroid/support/v7/internal/view/menu/g;->d:Landroid/support/v7/internal/view/menu/ExpandedMenuView;

    invoke-virtual {v1, v0}, Landroid/view/View;->restoreHierarchyState(Landroid/util/SparseArray;)V

    .line 223
    :cond_f
    return-void
.end method

.method public final a(Landroid/support/v7/internal/view/menu/i;Z)V
    .registers 4

    .prologue
    .line 151
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/g;->g:Landroid/support/v7/internal/view/menu/y;

    if-eqz v0, :cond_9

    .line 152
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/g;->g:Landroid/support/v7/internal/view/menu/y;

    invoke-interface {v0, p1, p2}, Landroid/support/v7/internal/view/menu/y;->a(Landroid/support/v7/internal/view/menu/i;Z)V

    .line 154
    :cond_9
    return-void
.end method

.method public final a(Landroid/support/v7/internal/view/menu/y;)V
    .registers 2

    .prologue
    .line 134
    iput-object p1, p0, Landroid/support/v7/internal/view/menu/g;->g:Landroid/support/v7/internal/view/menu/y;

    .line 135
    return-void
.end method

.method public final a(Z)V
    .registers 3

    .prologue
    .line 129
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/g;->h:Landroid/support/v7/internal/view/menu/h;

    if-eqz v0, :cond_9

    iget-object v0, p0, Landroid/support/v7/internal/view/menu/g;->h:Landroid/support/v7/internal/view/menu/h;

    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/h;->notifyDataSetChanged()V

    .line 130
    :cond_9
    return-void
.end method

.method public final a()Z
    .registers 2

    .prologue
    .line 174
    const/4 v0, 0x0

    return v0
.end method

.method public final a(Landroid/support/v7/internal/view/menu/ad;)Z
    .registers 8

    .prologue
    .line 139
    invoke-virtual {p1}, Landroid/support/v7/internal/view/menu/ad;->hasVisibleItems()Z

    move-result v0

    if-nez v0, :cond_8

    const/4 v0, 0x0

    .line 146
    :goto_7
    return v0

    .line 142
    :cond_8
    new-instance v0, Landroid/support/v7/internal/view/menu/l;

    invoke-direct {v0, p1}, Landroid/support/v7/internal/view/menu/l;-><init>(Landroid/support/v7/internal/view/menu/i;)V

    .line 2054
    iget-object v1, v0, Landroid/support/v7/internal/view/menu/l;->a:Landroid/support/v7/internal/view/menu/i;

    .line 2057
    new-instance v2, Landroid/support/v7/app/ag;

    .line 2807
    iget-object v3, v1, Landroid/support/v7/internal/view/menu/i;->e:Landroid/content/Context;

    .line 2057
    invoke-direct {v2, v3}, Landroid/support/v7/app/ag;-><init>(Landroid/content/Context;)V

    .line 2059
    new-instance v3, Landroid/support/v7/internal/view/menu/g;

    .line 3295
    iget-object v4, v2, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    iget-object v4, v4, Landroid/support/v7/app/x;->a:Landroid/content/Context;

    .line 2059
    sget v5, Landroid/support/v7/a/k;->abc_list_menu_item_layout:I

    invoke-direct {v3, v4, v5}, Landroid/support/v7/internal/view/menu/g;-><init>(Landroid/content/Context;I)V

    iput-object v3, v0, Landroid/support/v7/internal/view/menu/l;->c:Landroid/support/v7/internal/view/menu/g;

    .line 2062
    iget-object v3, v0, Landroid/support/v7/internal/view/menu/l;->c:Landroid/support/v7/internal/view/menu/g;

    .line 4134
    iput-object v0, v3, Landroid/support/v7/internal/view/menu/g;->g:Landroid/support/v7/internal/view/menu/y;

    .line 2063
    iget-object v3, v0, Landroid/support/v7/internal/view/menu/l;->a:Landroid/support/v7/internal/view/menu/i;

    iget-object v4, v0, Landroid/support/v7/internal/view/menu/l;->c:Landroid/support/v7/internal/view/menu/g;

    invoke-virtual {v3, v4}, Landroid/support/v7/internal/view/menu/i;->a(Landroid/support/v7/internal/view/menu/x;)V

    .line 2064
    iget-object v3, v0, Landroid/support/v7/internal/view/menu/l;->c:Landroid/support/v7/internal/view/menu/g;

    invoke-virtual {v3}, Landroid/support/v7/internal/view/menu/g;->d()Landroid/widget/ListAdapter;

    move-result-object v3

    .line 4554
    iget-object v4, v2, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    iput-object v3, v4, Landroid/support/v7/app/x;->t:Landroid/widget/ListAdapter;

    .line 4555
    iget-object v3, v2, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    iput-object v0, v3, Landroid/support/v7/app/x;->u:Landroid/content/DialogInterface$OnClickListener;

    .line 5282
    iget-object v3, v1, Landroid/support/v7/internal/view/menu/i;->l:Landroid/view/View;

    .line 2068
    if-eqz v3, :cond_78

    .line 5329
    iget-object v1, v2, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    iput-object v3, v1, Landroid/support/v7/app/x;->g:Landroid/view/View;

    .line 7514
    :goto_44
    iget-object v1, v2, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    iput-object v0, v1, Landroid/support/v7/app/x;->r:Landroid/content/DialogInterface$OnKeyListener;

    .line 2080
    invoke-virtual {v2}, Landroid/support/v7/app/ag;->a()Landroid/support/v7/app/af;

    move-result-object v1

    iput-object v1, v0, Landroid/support/v7/internal/view/menu/l;->b:Landroid/support/v7/app/af;

    .line 2081
    iget-object v1, v0, Landroid/support/v7/internal/view/menu/l;->b:Landroid/support/v7/app/af;

    invoke-virtual {v1, v0}, Landroid/support/v7/app/af;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 2083
    iget-object v1, v0, Landroid/support/v7/internal/view/menu/l;->b:Landroid/support/v7/app/af;

    invoke-virtual {v1}, Landroid/support/v7/app/af;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 2084
    const/16 v2, 0x3eb

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->type:I

    .line 2088
    iget v2, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    const/high16 v3, 0x20000

    or-int/2addr v2, v3

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 2090
    iget-object v0, v0, Landroid/support/v7/internal/view/menu/l;->b:Landroid/support/v7/app/af;

    invoke-virtual {v0}, Landroid/support/v7/app/af;->show()V

    .line 143
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/g;->g:Landroid/support/v7/internal/view/menu/y;

    if-eqz v0, :cond_76

    .line 144
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/g;->g:Landroid/support/v7/internal/view/menu/y;

    invoke-interface {v0, p1}, Landroid/support/v7/internal/view/menu/y;->a_(Landroid/support/v7/internal/view/menu/i;)Z

    .line 146
    :cond_76
    const/4 v0, 0x1

    goto :goto_7

    .line 6278
    :cond_78
    iget-object v3, v1, Landroid/support/v7/internal/view/menu/i;->k:Landroid/graphics/drawable/Drawable;

    .line 6371
    iget-object v4, v2, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    iput-object v3, v4, Landroid/support/v7/app/x;->d:Landroid/graphics/drawable/Drawable;

    .line 7274
    iget-object v1, v1, Landroid/support/v7/internal/view/menu/i;->j:Ljava/lang/CharSequence;

    .line 7314
    iget-object v3, v2, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    iput-object v1, v3, Landroid/support/v7/app/x;->f:Ljava/lang/CharSequence;

    goto :goto_44
.end method

.method public final a(Landroid/support/v7/internal/view/menu/m;)Z
    .registers 3

    .prologue
    .line 178
    const/4 v0, 0x0

    return v0
.end method

.method public final b()I
    .registers 2

    .prologue
    .line 206
    iget v0, p0, Landroid/support/v7/internal/view/menu/g;->l:I

    return v0
.end method

.method public final b(Landroid/support/v7/internal/view/menu/m;)Z
    .registers 3

    .prologue
    .line 182
    const/4 v0, 0x0

    return v0
.end method

.method public final c()Landroid/os/Parcelable;
    .registers 4

    .prologue
    .line 211
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/g;->d:Landroid/support/v7/internal/view/menu/ExpandedMenuView;

    if-nez v0, :cond_6

    .line 212
    const/4 v0, 0x0

    .line 217
    :goto_5
    return-object v0

    .line 215
    :cond_6
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 8186
    new-instance v1, Landroid/util/SparseArray;

    invoke-direct {v1}, Landroid/util/SparseArray;-><init>()V

    .line 8187
    iget-object v2, p0, Landroid/support/v7/internal/view/menu/g;->d:Landroid/support/v7/internal/view/menu/ExpandedMenuView;

    if-eqz v2, :cond_19

    .line 8188
    iget-object v2, p0, Landroid/support/v7/internal/view/menu/g;->d:Landroid/support/v7/internal/view/menu/ExpandedMenuView;

    invoke-virtual {v2, v1}, Landroid/view/View;->saveHierarchyState(Landroid/util/SparseArray;)V

    .line 8190
    :cond_19
    const-string v2, "android:menu:list"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putSparseParcelableArray(Ljava/lang/String;Landroid/util/SparseArray;)V

    goto :goto_5
.end method

.method public final d()Landroid/widget/ListAdapter;
    .registers 2

    .prologue
    .line 121
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/g;->h:Landroid/support/v7/internal/view/menu/h;

    if-nez v0, :cond_b

    .line 122
    new-instance v0, Landroid/support/v7/internal/view/menu/h;

    invoke-direct {v0, p0}, Landroid/support/v7/internal/view/menu/h;-><init>(Landroid/support/v7/internal/view/menu/g;)V

    iput-object v0, p0, Landroid/support/v7/internal/view/menu/g;->h:Landroid/support/v7/internal/view/menu/h;

    .line 124
    :cond_b
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/g;->h:Landroid/support/v7/internal/view/menu/h;

    return-object v0
.end method

.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .registers 9

    .prologue
    .line 169
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/g;->c:Landroid/support/v7/internal/view/menu/i;

    iget-object v1, p0, Landroid/support/v7/internal/view/menu/g;->h:Landroid/support/v7/internal/view/menu/h;

    invoke-virtual {v1, p3}, Landroid/support/v7/internal/view/menu/h;->a(I)Landroid/support/v7/internal/view/menu/m;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p0, v2}, Landroid/support/v7/internal/view/menu/i;->a(Landroid/view/MenuItem;Landroid/support/v7/internal/view/menu/x;I)Z

    .line 170
    return-void
.end method
