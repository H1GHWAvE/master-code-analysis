.class public final Landroid/support/v7/internal/view/menu/l;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Landroid/content/DialogInterface$OnDismissListener;
.implements Landroid/content/DialogInterface$OnKeyListener;
.implements Landroid/support/v7/internal/view/menu/y;


# instance fields
.field a:Landroid/support/v7/internal/view/menu/i;

.field b:Landroid/support/v7/app/af;

.field c:Landroid/support/v7/internal/view/menu/g;

.field private d:Landroid/support/v7/internal/view/menu/y;


# direct methods
.method public constructor <init>(Landroid/support/v7/internal/view/menu/i;)V
    .registers 2

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-object p1, p0, Landroid/support/v7/internal/view/menu/l;->a:Landroid/support/v7/internal/view/menu/i;

    .line 45
    return-void
.end method

.method private a()V
    .registers 6

    .prologue
    .line 54
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/l;->a:Landroid/support/v7/internal/view/menu/i;

    .line 57
    new-instance v1, Landroid/support/v7/app/ag;

    .line 1807
    iget-object v2, v0, Landroid/support/v7/internal/view/menu/i;->e:Landroid/content/Context;

    .line 57
    invoke-direct {v1, v2}, Landroid/support/v7/app/ag;-><init>(Landroid/content/Context;)V

    .line 59
    new-instance v2, Landroid/support/v7/internal/view/menu/g;

    .line 2295
    iget-object v3, v1, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    iget-object v3, v3, Landroid/support/v7/app/x;->a:Landroid/content/Context;

    .line 59
    sget v4, Landroid/support/v7/a/k;->abc_list_menu_item_layout:I

    invoke-direct {v2, v3, v4}, Landroid/support/v7/internal/view/menu/g;-><init>(Landroid/content/Context;I)V

    iput-object v2, p0, Landroid/support/v7/internal/view/menu/l;->c:Landroid/support/v7/internal/view/menu/g;

    .line 62
    iget-object v2, p0, Landroid/support/v7/internal/view/menu/l;->c:Landroid/support/v7/internal/view/menu/g;

    .line 3134
    iput-object p0, v2, Landroid/support/v7/internal/view/menu/g;->g:Landroid/support/v7/internal/view/menu/y;

    .line 63
    iget-object v2, p0, Landroid/support/v7/internal/view/menu/l;->a:Landroid/support/v7/internal/view/menu/i;

    iget-object v3, p0, Landroid/support/v7/internal/view/menu/l;->c:Landroid/support/v7/internal/view/menu/g;

    invoke-virtual {v2, v3}, Landroid/support/v7/internal/view/menu/i;->a(Landroid/support/v7/internal/view/menu/x;)V

    .line 64
    iget-object v2, p0, Landroid/support/v7/internal/view/menu/l;->c:Landroid/support/v7/internal/view/menu/g;

    invoke-virtual {v2}, Landroid/support/v7/internal/view/menu/g;->d()Landroid/widget/ListAdapter;

    move-result-object v2

    .line 3554
    iget-object v3, v1, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    iput-object v2, v3, Landroid/support/v7/app/x;->t:Landroid/widget/ListAdapter;

    .line 3555
    iget-object v2, v1, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    iput-object p0, v2, Landroid/support/v7/app/x;->u:Landroid/content/DialogInterface$OnClickListener;

    .line 4282
    iget-object v2, v0, Landroid/support/v7/internal/view/menu/i;->l:Landroid/view/View;

    .line 68
    if-eqz v2, :cond_61

    .line 4329
    iget-object v0, v1, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    iput-object v2, v0, Landroid/support/v7/app/x;->g:Landroid/view/View;

    .line 6514
    :goto_37
    iget-object v0, v1, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    iput-object p0, v0, Landroid/support/v7/app/x;->r:Landroid/content/DialogInterface$OnKeyListener;

    .line 80
    invoke-virtual {v1}, Landroid/support/v7/app/ag;->a()Landroid/support/v7/app/af;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/internal/view/menu/l;->b:Landroid/support/v7/app/af;

    .line 81
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/l;->b:Landroid/support/v7/app/af;

    invoke-virtual {v0, p0}, Landroid/support/v7/app/af;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 83
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/l;->b:Landroid/support/v7/app/af;

    invoke-virtual {v0}, Landroid/support/v7/app/af;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 84
    const/16 v1, 0x3eb

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->type:I

    .line 88
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    const/high16 v2, 0x20000

    or-int/2addr v1, v2

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 90
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/l;->b:Landroid/support/v7/app/af;

    invoke-virtual {v0}, Landroid/support/v7/app/af;->show()V

    .line 91
    return-void

    .line 5278
    :cond_61
    iget-object v2, v0, Landroid/support/v7/internal/view/menu/i;->k:Landroid/graphics/drawable/Drawable;

    .line 5371
    iget-object v3, v1, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    iput-object v2, v3, Landroid/support/v7/app/x;->d:Landroid/graphics/drawable/Drawable;

    .line 6274
    iget-object v0, v0, Landroid/support/v7/internal/view/menu/i;->j:Ljava/lang/CharSequence;

    .line 6314
    iget-object v2, v1, Landroid/support/v7/app/ag;->a:Landroid/support/v7/app/x;

    iput-object v0, v2, Landroid/support/v7/app/x;->f:Ljava/lang/CharSequence;

    goto :goto_37
.end method

.method private a(Landroid/support/v7/internal/view/menu/y;)V
    .registers 2

    .prologue
    .line 130
    iput-object p1, p0, Landroid/support/v7/internal/view/menu/l;->d:Landroid/support/v7/internal/view/menu/y;

    .line 131
    return-void
.end method

.method private b()V
    .registers 2

    .prologue
    .line 139
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/l;->b:Landroid/support/v7/app/af;

    if-eqz v0, :cond_9

    .line 140
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/l;->b:Landroid/support/v7/app/af;

    invoke-virtual {v0}, Landroid/support/v7/app/af;->dismiss()V

    .line 142
    :cond_9
    return-void
.end method


# virtual methods
.method public final a(Landroid/support/v7/internal/view/menu/i;Z)V
    .registers 4

    .prologue
    .line 151
    if-nez p2, :cond_6

    iget-object v0, p0, Landroid/support/v7/internal/view/menu/l;->a:Landroid/support/v7/internal/view/menu/i;

    if-ne p1, v0, :cond_f

    .line 7139
    :cond_6
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/l;->b:Landroid/support/v7/app/af;

    if-eqz v0, :cond_f

    .line 7140
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/l;->b:Landroid/support/v7/app/af;

    invoke-virtual {v0}, Landroid/support/v7/app/af;->dismiss()V

    .line 154
    :cond_f
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/l;->d:Landroid/support/v7/internal/view/menu/y;

    if-eqz v0, :cond_18

    .line 155
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/l;->d:Landroid/support/v7/internal/view/menu/y;

    invoke-interface {v0, p1, p2}, Landroid/support/v7/internal/view/menu/y;->a(Landroid/support/v7/internal/view/menu/i;Z)V

    .line 157
    :cond_18
    return-void
.end method

.method public final a_(Landroid/support/v7/internal/view/menu/i;)Z
    .registers 3

    .prologue
    .line 161
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/l;->d:Landroid/support/v7/internal/view/menu/y;

    if-eqz v0, :cond_b

    .line 162
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/l;->d:Landroid/support/v7/internal/view/menu/y;

    invoke-interface {v0, p1}, Landroid/support/v7/internal/view/menu/y;->a_(Landroid/support/v7/internal/view/menu/i;)Z

    move-result v0

    .line 164
    :goto_a
    return v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public final onClick(Landroid/content/DialogInterface;I)V
    .registers 7

    .prologue
    .line 168
    iget-object v1, p0, Landroid/support/v7/internal/view/menu/l;->a:Landroid/support/v7/internal/view/menu/i;

    iget-object v0, p0, Landroid/support/v7/internal/view/menu/l;->c:Landroid/support/v7/internal/view/menu/g;

    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/g;->d()Landroid/widget/ListAdapter;

    move-result-object v0

    invoke-interface {v0, p2}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/view/menu/m;

    .line 7948
    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v2, v3}, Landroid/support/v7/internal/view/menu/i;->a(Landroid/view/MenuItem;Landroid/support/v7/internal/view/menu/x;I)Z

    .line 169
    return-void
.end method

.method public final onDismiss(Landroid/content/DialogInterface;)V
    .registers 5

    .prologue
    .line 146
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/l;->c:Landroid/support/v7/internal/view/menu/g;

    iget-object v1, p0, Landroid/support/v7/internal/view/menu/l;->a:Landroid/support/v7/internal/view/menu/i;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/internal/view/menu/g;->a(Landroid/support/v7/internal/view/menu/i;Z)V

    .line 147
    return-void
.end method

.method public final onKey(Landroid/content/DialogInterface;ILandroid/view/KeyEvent;)Z
    .registers 6

    .prologue
    const/4 v0, 0x1

    .line 94
    const/16 v1, 0x52

    if-eq p2, v1, :cond_8

    const/4 v1, 0x4

    if-ne p2, v1, :cond_5b

    .line 95
    :cond_8
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_2c

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v1

    if-nez v1, :cond_2c

    .line 97
    iget-object v1, p0, Landroid/support/v7/internal/view/menu/l;->b:Landroid/support/v7/app/af;

    invoke-virtual {v1}, Landroid/support/v7/app/af;->getWindow()Landroid/view/Window;

    move-result-object v1

    .line 98
    if-eqz v1, :cond_5b

    .line 99
    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    .line 100
    if-eqz v1, :cond_5b

    .line 101
    invoke-virtual {v1}, Landroid/view/View;->getKeyDispatcherState()Landroid/view/KeyEvent$DispatcherState;

    move-result-object v1

    .line 102
    if-eqz v1, :cond_5b

    .line 103
    invoke-virtual {v1, p3, p0}, Landroid/view/KeyEvent$DispatcherState;->startTracking(Landroid/view/KeyEvent;Ljava/lang/Object;)V

    .line 125
    :goto_2b
    return v0

    .line 108
    :cond_2c
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-ne v1, v0, :cond_5b

    invoke-virtual {p3}, Landroid/view/KeyEvent;->isCanceled()Z

    move-result v1

    if-nez v1, :cond_5b

    .line 109
    iget-object v1, p0, Landroid/support/v7/internal/view/menu/l;->b:Landroid/support/v7/app/af;

    invoke-virtual {v1}, Landroid/support/v7/app/af;->getWindow()Landroid/view/Window;

    move-result-object v1

    .line 110
    if-eqz v1, :cond_5b

    .line 111
    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    .line 112
    if-eqz v1, :cond_5b

    .line 113
    invoke-virtual {v1}, Landroid/view/View;->getKeyDispatcherState()Landroid/view/KeyEvent$DispatcherState;

    move-result-object v1

    .line 114
    if-eqz v1, :cond_5b

    invoke-virtual {v1, p3}, Landroid/view/KeyEvent$DispatcherState;->isTracking(Landroid/view/KeyEvent;)Z

    move-result v1

    if-eqz v1, :cond_5b

    .line 115
    iget-object v1, p0, Landroid/support/v7/internal/view/menu/l;->a:Landroid/support/v7/internal/view/menu/i;

    invoke-virtual {v1, v0}, Landroid/support/v7/internal/view/menu/i;->b(Z)V

    .line 116
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    goto :goto_2b

    .line 125
    :cond_5b
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/l;->a:Landroid/support/v7/internal/view/menu/i;

    const/4 v1, 0x0

    invoke-virtual {v0, p2, p3, v1}, Landroid/support/v7/internal/view/menu/i;->performShortcut(ILandroid/view/KeyEvent;I)Z

    move-result v0

    goto :goto_2b
.end method
