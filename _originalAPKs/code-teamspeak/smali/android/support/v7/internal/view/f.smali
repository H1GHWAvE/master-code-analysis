.class public final Landroid/support/v7/internal/view/f;
.super Landroid/view/MenuInflater;
.source "SourceFile"


# static fields
.field private static final a:Ljava/lang/String; = "SupportMenuInflater"

.field private static final b:Ljava/lang/String; = "menu"

.field private static final c:Ljava/lang/String; = "group"

.field private static final d:Ljava/lang/String; = "item"

.field private static final e:I

.field private static final f:[Ljava/lang/Class;

.field private static final g:[Ljava/lang/Class;


# instance fields
.field private final h:[Ljava/lang/Object;

.field private final i:[Ljava/lang/Object;

.field private j:Landroid/content/Context;

.field private k:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    .line 72
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Class;

    const/4 v1, 0x0

    const-class v2, Landroid/content/Context;

    aput-object v2, v0, v1

    .line 74
    sput-object v0, Landroid/support/v7/internal/view/f;->f:[Ljava/lang/Class;

    sput-object v0, Landroid/support/v7/internal/view/f;->g:[Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 4

    .prologue
    .line 90
    invoke-direct {p0, p1}, Landroid/view/MenuInflater;-><init>(Landroid/content/Context;)V

    .line 91
    iput-object p1, p0, Landroid/support/v7/internal/view/f;->j:Landroid/content/Context;

    .line 92
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    iput-object v0, p0, Landroid/support/v7/internal/view/f;->h:[Ljava/lang/Object;

    .line 93
    iget-object v0, p0, Landroid/support/v7/internal/view/f;->h:[Ljava/lang/Object;

    iput-object v0, p0, Landroid/support/v7/internal/view/f;->i:[Ljava/lang/Object;

    .line 94
    return-void
.end method

.method static synthetic a(Landroid/support/v7/internal/view/f;)Landroid/content/Context;
    .registers 2

    .prologue
    .line 58
    iget-object v0, p0, Landroid/support/v7/internal/view/f;->j:Landroid/content/Context;

    return-object v0
.end method

.method private static a(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 220
    move-object v0, p0

    :goto_1
    instance-of v1, v0, Landroid/app/Activity;

    if-eqz v1, :cond_6

    .line 226
    :cond_5
    return-object v0

    .line 223
    :cond_6
    instance-of v1, v0, Landroid/content/ContextWrapper;

    if-eqz v1, :cond_5

    .line 224
    check-cast v0, Landroid/content/ContextWrapper;

    invoke-virtual {v0}, Landroid/content/ContextWrapper;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    goto :goto_1
.end method

.method private a(Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;Landroid/view/Menu;)V
    .registers 14

    .prologue
    .line 134
    new-instance v4, Landroid/support/v7/internal/view/h;

    invoke-direct {v4, p0, p3}, Landroid/support/v7/internal/view/h;-><init>(Landroid/support/v7/internal/view/f;Landroid/view/Menu;)V

    .line 136
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v0

    .line 138
    const/4 v3, 0x0

    .line 139
    const/4 v2, 0x0

    .line 143
    :cond_b
    const/4 v1, 0x2

    if-ne v0, v1, :cond_46

    .line 144
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    .line 145
    const-string v1, "menu"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_31

    .line 147
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v0

    .line 156
    :goto_1e
    const/4 v1, 0x0

    .line 157
    :goto_1f
    if-nez v1, :cond_22f

    .line 158
    packed-switch v0, :pswitch_data_230

    :cond_24
    move v0, v1

    move-object v1, v2

    move v2, v3

    .line 208
    :goto_27
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v3

    move v9, v0

    move v0, v3

    move v3, v2

    move-object v2, v1

    move v1, v9

    goto :goto_1f

    .line 151
    :cond_31
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Expecting menu, got "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 153
    :cond_46
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v0

    .line 154
    const/4 v1, 0x1

    if-ne v0, v1, :cond_b

    goto :goto_1e

    .line 160
    :pswitch_4e
    if-nez v3, :cond_24

    .line 164
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    .line 165
    const-string v5, "group"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_a3

    .line 1348
    iget-object v0, v4, Landroid/support/v7/internal/view/h;->z:Landroid/support/v7/internal/view/f;

    .line 2058
    iget-object v0, v0, Landroid/support/v7/internal/view/f;->j:Landroid/content/Context;

    .line 1348
    sget-object v5, Landroid/support/v7/a/n;->MenuGroup:[I

    invoke-virtual {v0, p2, v5}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1350
    sget v5, Landroid/support/v7/a/n;->MenuGroup_android_id:I

    const/4 v6, 0x0

    invoke-virtual {v0, v5, v6}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v5

    iput v5, v4, Landroid/support/v7/internal/view/h;->b:I

    .line 1351
    sget v5, Landroid/support/v7/a/n;->MenuGroup_android_menuCategory:I

    const/4 v6, 0x0

    invoke-virtual {v0, v5, v6}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v5

    iput v5, v4, Landroid/support/v7/internal/view/h;->c:I

    .line 1353
    sget v5, Landroid/support/v7/a/n;->MenuGroup_android_orderInCategory:I

    const/4 v6, 0x0

    invoke-virtual {v0, v5, v6}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v5

    iput v5, v4, Landroid/support/v7/internal/view/h;->d:I

    .line 1354
    sget v5, Landroid/support/v7/a/n;->MenuGroup_android_checkableBehavior:I

    const/4 v6, 0x0

    invoke-virtual {v0, v5, v6}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v5

    iput v5, v4, Landroid/support/v7/internal/view/h;->e:I

    .line 1356
    sget v5, Landroid/support/v7/a/n;->MenuGroup_android_visible:I

    const/4 v6, 0x1

    invoke-virtual {v0, v5, v6}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v5

    iput-boolean v5, v4, Landroid/support/v7/internal/view/h;->f:Z

    .line 1357
    sget v5, Landroid/support/v7/a/n;->MenuGroup_android_enabled:I

    const/4 v6, 0x1

    invoke-virtual {v0, v5, v6}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v5

    iput-boolean v5, v4, Landroid/support/v7/internal/view/h;->g:Z

    .line 1359
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    move v0, v1

    move-object v1, v2

    move v2, v3

    .line 166
    goto :goto_27

    .line 167
    :cond_a3
    const-string v5, "item"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1a4

    .line 2366
    iget-object v0, v4, Landroid/support/v7/internal/view/h;->z:Landroid/support/v7/internal/view/f;

    .line 3058
    iget-object v0, v0, Landroid/support/v7/internal/view/f;->j:Landroid/content/Context;

    .line 2366
    sget-object v5, Landroid/support/v7/a/n;->MenuItem:[I

    invoke-virtual {v0, p2, v5}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v5

    .line 2369
    sget v0, Landroid/support/v7/a/n;->MenuItem_android_id:I

    const/4 v6, 0x0

    invoke-virtual {v5, v0, v6}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, v4, Landroid/support/v7/internal/view/h;->i:I

    .line 2370
    sget v0, Landroid/support/v7/a/n;->MenuItem_android_menuCategory:I

    iget v6, v4, Landroid/support/v7/internal/view/h;->c:I

    invoke-virtual {v5, v0, v6}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    .line 2371
    sget v6, Landroid/support/v7/a/n;->MenuItem_android_orderInCategory:I

    iget v7, v4, Landroid/support/v7/internal/view/h;->d:I

    invoke-virtual {v5, v6, v7}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v6

    .line 2372
    const/high16 v7, -0x10000

    and-int/2addr v0, v7

    const v7, 0xffff

    and-int/2addr v6, v7

    or-int/2addr v0, v6

    iput v0, v4, Landroid/support/v7/internal/view/h;->j:I

    .line 2374
    sget v0, Landroid/support/v7/a/n;->MenuItem_android_title:I

    invoke-virtual {v5, v0}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, v4, Landroid/support/v7/internal/view/h;->k:Ljava/lang/CharSequence;

    .line 2375
    sget v0, Landroid/support/v7/a/n;->MenuItem_android_titleCondensed:I

    invoke-virtual {v5, v0}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, v4, Landroid/support/v7/internal/view/h;->l:Ljava/lang/CharSequence;

    .line 2376
    sget v0, Landroid/support/v7/a/n;->MenuItem_android_icon:I

    const/4 v6, 0x0

    invoke-virtual {v5, v0, v6}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, v4, Landroid/support/v7/internal/view/h;->m:I

    .line 2377
    sget v0, Landroid/support/v7/a/n;->MenuItem_android_alphabeticShortcut:I

    invoke-virtual {v5, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v7/internal/view/h;->a(Ljava/lang/String;)C

    move-result v0

    iput-char v0, v4, Landroid/support/v7/internal/view/h;->n:C

    .line 2379
    sget v0, Landroid/support/v7/a/n;->MenuItem_android_numericShortcut:I

    invoke-virtual {v5, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v7/internal/view/h;->a(Ljava/lang/String;)C

    move-result v0

    iput-char v0, v4, Landroid/support/v7/internal/view/h;->o:C

    .line 2381
    sget v0, Landroid/support/v7/a/n;->MenuItem_android_checkable:I

    invoke-virtual {v5, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_190

    .line 2383
    sget v0, Landroid/support/v7/a/n;->MenuItem_android_checkable:I

    const/4 v6, 0x0

    invoke-virtual {v5, v0, v6}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    if-eqz v0, :cond_18e

    const/4 v0, 0x1

    :goto_11b
    iput v0, v4, Landroid/support/v7/internal/view/h;->p:I

    .line 2389
    :goto_11d
    sget v0, Landroid/support/v7/a/n;->MenuItem_android_checked:I

    const/4 v6, 0x0

    invoke-virtual {v5, v0, v6}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, v4, Landroid/support/v7/internal/view/h;->q:Z

    .line 2390
    sget v0, Landroid/support/v7/a/n;->MenuItem_android_visible:I

    iget-boolean v6, v4, Landroid/support/v7/internal/view/h;->f:Z

    invoke-virtual {v5, v0, v6}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, v4, Landroid/support/v7/internal/view/h;->r:Z

    .line 2391
    sget v0, Landroid/support/v7/a/n;->MenuItem_android_enabled:I

    iget-boolean v6, v4, Landroid/support/v7/internal/view/h;->g:Z

    invoke-virtual {v5, v0, v6}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, v4, Landroid/support/v7/internal/view/h;->s:Z

    .line 2392
    sget v0, Landroid/support/v7/a/n;->MenuItem_showAsAction:I

    const/4 v6, -0x1

    invoke-virtual {v5, v0, v6}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    iput v0, v4, Landroid/support/v7/internal/view/h;->t:I

    .line 2393
    sget v0, Landroid/support/v7/a/n;->MenuItem_android_onClick:I

    invoke-virtual {v5, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, Landroid/support/v7/internal/view/h;->x:Ljava/lang/String;

    .line 2394
    sget v0, Landroid/support/v7/a/n;->MenuItem_actionLayout:I

    const/4 v6, 0x0

    invoke-virtual {v5, v0, v6}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, v4, Landroid/support/v7/internal/view/h;->u:I

    .line 2395
    sget v0, Landroid/support/v7/a/n;->MenuItem_actionViewClass:I

    invoke-virtual {v5, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, Landroid/support/v7/internal/view/h;->v:Ljava/lang/String;

    .line 2396
    sget v0, Landroid/support/v7/a/n;->MenuItem_actionProviderClass:I

    invoke-virtual {v5, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, Landroid/support/v7/internal/view/h;->w:Ljava/lang/String;

    .line 2398
    iget-object v0, v4, Landroid/support/v7/internal/view/h;->w:Ljava/lang/String;

    if-eqz v0, :cond_195

    const/4 v0, 0x1

    .line 2399
    :goto_169
    if-eqz v0, :cond_197

    iget v6, v4, Landroid/support/v7/internal/view/h;->u:I

    if-nez v6, :cond_197

    iget-object v6, v4, Landroid/support/v7/internal/view/h;->v:Ljava/lang/String;

    if-nez v6, :cond_197

    .line 2400
    iget-object v0, v4, Landroid/support/v7/internal/view/h;->w:Ljava/lang/String;

    .line 4058
    sget-object v6, Landroid/support/v7/internal/view/f;->g:[Ljava/lang/Class;

    .line 2400
    iget-object v7, v4, Landroid/support/v7/internal/view/h;->z:Landroid/support/v7/internal/view/f;

    .line 5058
    iget-object v7, v7, Landroid/support/v7/internal/view/f;->i:[Ljava/lang/Object;

    .line 2400
    invoke-virtual {v4, v0, v6, v7}, Landroid/support/v7/internal/view/h;->a(Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/n;

    iput-object v0, v4, Landroid/support/v7/internal/view/h;->y:Landroid/support/v4/view/n;

    .line 2411
    :goto_183
    invoke-virtual {v5}, Landroid/content/res/TypedArray;->recycle()V

    .line 2413
    const/4 v0, 0x0

    iput-boolean v0, v4, Landroid/support/v7/internal/view/h;->h:Z

    move v0, v1

    move-object v1, v2

    move v2, v3

    .line 168
    goto/16 :goto_27

    .line 2383
    :cond_18e
    const/4 v0, 0x0

    goto :goto_11b

    .line 2387
    :cond_190
    iget v0, v4, Landroid/support/v7/internal/view/h;->e:I

    iput v0, v4, Landroid/support/v7/internal/view/h;->p:I

    goto :goto_11d

    .line 2398
    :cond_195
    const/4 v0, 0x0

    goto :goto_169

    .line 2404
    :cond_197
    if-eqz v0, :cond_1a0

    .line 2405
    const-string v0, "SupportMenuInflater"

    const-string v6, "Ignoring attribute \'actionProviderClass\'. Action view already specified."

    invoke-static {v0, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2408
    :cond_1a0
    const/4 v0, 0x0

    iput-object v0, v4, Landroid/support/v7/internal/view/h;->y:Landroid/support/v4/view/n;

    goto :goto_183

    .line 169
    :cond_1a4
    const-string v5, "menu"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1b8

    .line 171
    invoke-virtual {v4}, Landroid/support/v7/internal/view/h;->b()Landroid/view/SubMenu;

    move-result-object v0

    .line 174
    invoke-direct {p0, p1, p2, v0}, Landroid/support/v7/internal/view/f;->a(Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;Landroid/view/Menu;)V

    move v0, v1

    move-object v1, v2

    move v2, v3

    .line 175
    goto/16 :goto_27

    .line 176
    :cond_1b8
    const/4 v2, 0x1

    move v9, v1

    move-object v1, v0

    move v0, v9

    .line 179
    goto/16 :goto_27

    .line 182
    :pswitch_1be
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    .line 183
    if-eqz v3, :cond_1d1

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1d1

    .line 184
    const/4 v2, 0x0

    .line 185
    const/4 v0, 0x0

    move v9, v1

    move-object v1, v0

    move v0, v9

    goto/16 :goto_27

    .line 186
    :cond_1d1
    const-string v5, "group"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1e1

    .line 187
    invoke-virtual {v4}, Landroid/support/v7/internal/view/h;->a()V

    move v0, v1

    move-object v1, v2

    move v2, v3

    goto/16 :goto_27

    .line 188
    :cond_1e1
    const-string v5, "item"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_21a

    .line 5490
    iget-boolean v0, v4, Landroid/support/v7/internal/view/h;->h:Z

    .line 191
    if-nez v0, :cond_24

    .line 6270
    iget-object v0, v4, Landroid/support/v7/internal/view/h;->y:Landroid/support/v4/view/n;

    .line 192
    if-eqz v0, :cond_201

    .line 7270
    iget-object v0, v4, Landroid/support/v7/internal/view/h;->y:Landroid/support/v4/view/n;

    .line 192
    invoke-virtual {v0}, Landroid/support/v4/view/n;->f()Z

    move-result v0

    if-eqz v0, :cond_201

    .line 194
    invoke-virtual {v4}, Landroid/support/v7/internal/view/h;->b()Landroid/view/SubMenu;

    move v0, v1

    move-object v1, v2

    move v2, v3

    goto/16 :goto_27

    .line 7478
    :cond_201
    const/4 v0, 0x1

    iput-boolean v0, v4, Landroid/support/v7/internal/view/h;->h:Z

    .line 7479
    iget-object v0, v4, Landroid/support/v7/internal/view/h;->a:Landroid/view/Menu;

    iget v5, v4, Landroid/support/v7/internal/view/h;->b:I

    iget v6, v4, Landroid/support/v7/internal/view/h;->i:I

    iget v7, v4, Landroid/support/v7/internal/view/h;->j:I

    iget-object v8, v4, Landroid/support/v7/internal/view/h;->k:Ljava/lang/CharSequence;

    invoke-interface {v0, v5, v6, v7, v8}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/support/v7/internal/view/h;->a(Landroid/view/MenuItem;)V

    move v0, v1

    move-object v1, v2

    move v2, v3

    .line 196
    goto/16 :goto_27

    .line 199
    :cond_21a
    const-string v5, "menu"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_24

    .line 200
    const/4 v0, 0x1

    move-object v1, v2

    move v2, v3

    goto/16 :goto_27

    .line 205
    :pswitch_227
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Unexpected end of document"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 210
    :cond_22f
    return-void

    .line 158
    :pswitch_data_230
    .packed-switch 0x1
        :pswitch_227
        :pswitch_4e
        :pswitch_1be
    .end packed-switch
.end method

.method static synthetic a()[Ljava/lang/Class;
    .registers 1

    .prologue
    .line 58
    sget-object v0, Landroid/support/v7/internal/view/f;->g:[Ljava/lang/Class;

    return-object v0
.end method

.method static synthetic b()[Ljava/lang/Class;
    .registers 1

    .prologue
    .line 58
    sget-object v0, Landroid/support/v7/internal/view/f;->f:[Ljava/lang/Class;

    return-object v0
.end method

.method static synthetic b(Landroid/support/v7/internal/view/f;)[Ljava/lang/Object;
    .registers 2

    .prologue
    .line 58
    iget-object v0, p0, Landroid/support/v7/internal/view/f;->i:[Ljava/lang/Object;

    return-object v0
.end method

.method private c()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 213
    iget-object v0, p0, Landroid/support/v7/internal/view/f;->k:Ljava/lang/Object;

    if-nez v0, :cond_c

    .line 214
    iget-object v0, p0, Landroid/support/v7/internal/view/f;->j:Landroid/content/Context;

    invoke-static {v0}, Landroid/support/v7/internal/view/f;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/internal/view/f;->k:Ljava/lang/Object;

    .line 216
    :cond_c
    iget-object v0, p0, Landroid/support/v7/internal/view/f;->k:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic c(Landroid/support/v7/internal/view/f;)Ljava/lang/Object;
    .registers 2

    .prologue
    .line 58
    .line 8213
    iget-object v0, p0, Landroid/support/v7/internal/view/f;->k:Ljava/lang/Object;

    if-nez v0, :cond_c

    .line 8214
    iget-object v0, p0, Landroid/support/v7/internal/view/f;->j:Landroid/content/Context;

    invoke-static {v0}, Landroid/support/v7/internal/view/f;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/internal/view/f;->k:Ljava/lang/Object;

    .line 8216
    :cond_c
    iget-object v0, p0, Landroid/support/v7/internal/view/f;->k:Ljava/lang/Object;

    .line 58
    return-object v0
.end method

.method static synthetic d(Landroid/support/v7/internal/view/f;)[Ljava/lang/Object;
    .registers 2

    .prologue
    .line 58
    iget-object v0, p0, Landroid/support/v7/internal/view/f;->h:[Ljava/lang/Object;

    return-object v0
.end method


# virtual methods
.method public final inflate(ILandroid/view/Menu;)V
    .registers 7

    .prologue
    .line 108
    instance-of v0, p2, Landroid/support/v4/g/a/a;

    if-nez v0, :cond_8

    .line 109
    invoke-super {p0, p1, p2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 126
    :cond_7
    :goto_7
    return-void

    .line 113
    :cond_8
    const/4 v1, 0x0

    .line 115
    :try_start_9
    iget-object v0, p0, Landroid/support/v7/internal/view/f;->j:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getLayout(I)Landroid/content/res/XmlResourceParser;

    move-result-object v1

    .line 116
    invoke-static {v1}, Landroid/util/Xml;->asAttributeSet(Lorg/xmlpull/v1/XmlPullParser;)Landroid/util/AttributeSet;

    move-result-object v0

    .line 118
    invoke-direct {p0, v1, v0, p2}, Landroid/support/v7/internal/view/f;->a(Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;Landroid/view/Menu;)V
    :try_end_1a
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_9 .. :try_end_1a} :catch_20
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_1a} :catch_30
    .catchall {:try_start_9 .. :try_end_1a} :catchall_29

    .line 124
    if-eqz v1, :cond_7

    invoke-interface {v1}, Landroid/content/res/XmlResourceParser;->close()V

    goto :goto_7

    .line 119
    :catch_20
    move-exception v0

    .line 120
    :try_start_21
    new-instance v2, Landroid/view/InflateException;

    const-string v3, "Error inflating menu XML"

    invoke-direct {v2, v3, v0}, Landroid/view/InflateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
    :try_end_29
    .catchall {:try_start_21 .. :try_end_29} :catchall_29

    .line 124
    :catchall_29
    move-exception v0

    if-eqz v1, :cond_2f

    invoke-interface {v1}, Landroid/content/res/XmlResourceParser;->close()V

    :cond_2f
    throw v0

    .line 121
    :catch_30
    move-exception v0

    .line 122
    :try_start_31
    new-instance v2, Landroid/view/InflateException;

    const-string v3, "Error inflating menu XML"

    invoke-direct {v2, v3, v0}, Landroid/view/InflateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
    :try_end_39
    .catchall {:try_start_31 .. :try_end_39} :catchall_29
.end method
