.class public final Landroid/support/v7/internal/a/p;
.super Landroid/support/v7/c/a;
.source "SourceFile"

# interfaces
.implements Landroid/support/v7/internal/view/menu/j;


# instance fields
.field final synthetic a:Landroid/support/v7/internal/a/l;

.field private final d:Landroid/content/Context;

.field private final e:Landroid/support/v7/internal/view/menu/i;

.field private f:Landroid/support/v7/c/b;

.field private g:Ljava/lang/ref/WeakReference;


# direct methods
.method public constructor <init>(Landroid/support/v7/internal/a/l;Landroid/content/Context;Landroid/support/v7/c/b;)V
    .registers 6

    .prologue
    .line 937
    iput-object p1, p0, Landroid/support/v7/internal/a/p;->a:Landroid/support/v7/internal/a/l;

    invoke-direct {p0}, Landroid/support/v7/c/a;-><init>()V

    .line 938
    iput-object p2, p0, Landroid/support/v7/internal/a/p;->d:Landroid/content/Context;

    .line 939
    iput-object p3, p0, Landroid/support/v7/internal/a/p;->f:Landroid/support/v7/c/b;

    .line 940
    new-instance v0, Landroid/support/v7/internal/view/menu/i;

    invoke-direct {v0, p2}, Landroid/support/v7/internal/view/menu/i;-><init>(Landroid/content/Context;)V

    .line 1231
    const/4 v1, 0x1

    iput v1, v0, Landroid/support/v7/internal/view/menu/i;->i:I

    .line 940
    iput-object v0, p0, Landroid/support/v7/internal/a/p;->e:Landroid/support/v7/internal/view/menu/i;

    .line 942
    iget-object v0, p0, Landroid/support/v7/internal/a/p;->e:Landroid/support/v7/internal/view/menu/i;

    invoke-virtual {v0, p0}, Landroid/support/v7/internal/view/menu/i;->a(Landroid/support/v7/internal/view/menu/j;)V

    .line 943
    return-void
.end method

.method private a(Landroid/support/v7/internal/view/menu/ad;)Z
    .registers 5

    .prologue
    const/4 v0, 0x1

    .line 1076
    iget-object v1, p0, Landroid/support/v7/internal/a/p;->f:Landroid/support/v7/c/b;

    if-nez v1, :cond_7

    .line 1077
    const/4 v0, 0x0

    .line 1085
    :cond_6
    :goto_6
    return v0

    .line 1080
    :cond_7
    invoke-virtual {p1}, Landroid/support/v7/internal/view/menu/ad;->hasVisibleItems()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 1084
    new-instance v1, Landroid/support/v7/internal/view/menu/v;

    iget-object v2, p0, Landroid/support/v7/internal/a/p;->a:Landroid/support/v7/internal/a/l;

    invoke-virtual {v2}, Landroid/support/v7/internal/a/l;->r()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2, p1}, Landroid/support/v7/internal/view/menu/v;-><init>(Landroid/content/Context;Landroid/support/v7/internal/view/menu/i;)V

    invoke-virtual {v1}, Landroid/support/v7/internal/view/menu/v;->d()V

    goto :goto_6
.end method

.method private static j()V
    .registers 0

    .prologue
    .line 1073
    return-void
.end method

.method private static k()V
    .registers 0

    .prologue
    .line 1089
    return-void
.end method


# virtual methods
.method public final a()Landroid/view/MenuInflater;
    .registers 3

    .prologue
    .line 947
    new-instance v0, Landroid/support/v7/internal/view/f;

    iget-object v1, p0, Landroid/support/v7/internal/a/p;->d:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/support/v7/internal/view/f;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public final a(I)V
    .registers 3

    .prologue
    .line 1030
    iget-object v0, p0, Landroid/support/v7/internal/a/p;->a:Landroid/support/v7/internal/a/l;

    invoke-static {v0}, Landroid/support/v7/internal/a/l;->j(Landroid/support/v7/internal/a/l;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/support/v7/internal/a/p;->b(Ljava/lang/CharSequence;)V

    .line 1031
    return-void
.end method

.method public final a(Landroid/support/v7/internal/view/menu/i;)V
    .registers 3

    .prologue
    .line 1092
    iget-object v0, p0, Landroid/support/v7/internal/a/p;->f:Landroid/support/v7/c/b;

    if-nez v0, :cond_5

    .line 1097
    :goto_4
    return-void

    .line 1095
    :cond_5
    invoke-virtual {p0}, Landroid/support/v7/internal/a/p;->d()V

    .line 1096
    iget-object v0, p0, Landroid/support/v7/internal/a/p;->a:Landroid/support/v7/internal/a/l;

    invoke-static {v0}, Landroid/support/v7/internal/a/l;->h(Landroid/support/v7/internal/a/l;)Landroid/support/v7/internal/widget/ActionBarContextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarContextView;->a()Z

    goto :goto_4
.end method

.method public final a(Landroid/view/View;)V
    .registers 3

    .prologue
    .line 1014
    iget-object v0, p0, Landroid/support/v7/internal/a/p;->a:Landroid/support/v7/internal/a/l;

    invoke-static {v0}, Landroid/support/v7/internal/a/l;->h(Landroid/support/v7/internal/a/l;)Landroid/support/v7/internal/widget/ActionBarContextView;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v7/internal/widget/ActionBarContextView;->setCustomView(Landroid/view/View;)V

    .line 1015
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Landroid/support/v7/internal/a/p;->g:Ljava/lang/ref/WeakReference;

    .line 1016
    return-void
.end method

.method public final a(Ljava/lang/CharSequence;)V
    .registers 3

    .prologue
    .line 1020
    iget-object v0, p0, Landroid/support/v7/internal/a/p;->a:Landroid/support/v7/internal/a/l;

    invoke-static {v0}, Landroid/support/v7/internal/a/l;->h(Landroid/support/v7/internal/a/l;)Landroid/support/v7/internal/widget/ActionBarContextView;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v7/internal/widget/ActionBarContextView;->setSubtitle(Ljava/lang/CharSequence;)V

    .line 1021
    return-void
.end method

.method public final a(Z)V
    .registers 3

    .prologue
    .line 1050
    invoke-super {p0, p1}, Landroid/support/v7/c/a;->a(Z)V

    .line 1051
    iget-object v0, p0, Landroid/support/v7/internal/a/p;->a:Landroid/support/v7/internal/a/l;

    invoke-static {v0}, Landroid/support/v7/internal/a/l;->h(Landroid/support/v7/internal/a/l;)Landroid/support/v7/internal/widget/ActionBarContextView;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v7/internal/widget/ActionBarContextView;->setTitleOptional(Z)V

    .line 1052
    return-void
.end method

.method public final a(Landroid/support/v7/internal/view/menu/i;Landroid/view/MenuItem;)Z
    .registers 4

    .prologue
    .line 1065
    iget-object v0, p0, Landroid/support/v7/internal/a/p;->f:Landroid/support/v7/c/b;

    if-eqz v0, :cond_b

    .line 1066
    iget-object v0, p0, Landroid/support/v7/internal/a/p;->f:Landroid/support/v7/c/b;

    invoke-interface {v0, p0, p2}, Landroid/support/v7/c/b;->a(Landroid/support/v7/c/a;Landroid/view/MenuItem;)Z

    move-result v0

    .line 1068
    :goto_a
    return v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public final b()Landroid/view/Menu;
    .registers 2

    .prologue
    .line 952
    iget-object v0, p0, Landroid/support/v7/internal/a/p;->e:Landroid/support/v7/internal/view/menu/i;

    return-object v0
.end method

.method public final b(I)V
    .registers 3

    .prologue
    .line 1035
    iget-object v0, p0, Landroid/support/v7/internal/a/p;->a:Landroid/support/v7/internal/a/l;

    invoke-static {v0}, Landroid/support/v7/internal/a/l;->j(Landroid/support/v7/internal/a/l;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/support/v7/internal/a/p;->a(Ljava/lang/CharSequence;)V

    .line 1036
    return-void
.end method

.method public final b(Ljava/lang/CharSequence;)V
    .registers 3

    .prologue
    .line 1025
    iget-object v0, p0, Landroid/support/v7/internal/a/p;->a:Landroid/support/v7/internal/a/l;

    invoke-static {v0}, Landroid/support/v7/internal/a/l;->h(Landroid/support/v7/internal/a/l;)Landroid/support/v7/internal/widget/ActionBarContextView;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v7/internal/widget/ActionBarContextView;->setTitle(Ljava/lang/CharSequence;)V

    .line 1026
    return-void
.end method

.method public final c()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 957
    iget-object v0, p0, Landroid/support/v7/internal/a/p;->a:Landroid/support/v7/internal/a/l;

    iget-object v0, v0, Landroid/support/v7/internal/a/l;->j:Landroid/support/v7/internal/a/p;

    if-eq v0, p0, :cond_8

    .line 984
    :goto_7
    return-void

    .line 966
    :cond_8
    iget-object v0, p0, Landroid/support/v7/internal/a/p;->a:Landroid/support/v7/internal/a/l;

    invoke-static {v0}, Landroid/support/v7/internal/a/l;->f(Landroid/support/v7/internal/a/l;)Z

    move-result v0

    iget-object v1, p0, Landroid/support/v7/internal/a/p;->a:Landroid/support/v7/internal/a/l;

    invoke-static {v1}, Landroid/support/v7/internal/a/l;->g(Landroid/support/v7/internal/a/l;)Z

    move-result v1

    invoke-static {v0, v1}, Landroid/support/v7/internal/a/l;->a(ZZ)Z

    move-result v0

    if-nez v0, :cond_5a

    .line 969
    iget-object v0, p0, Landroid/support/v7/internal/a/p;->a:Landroid/support/v7/internal/a/l;

    iput-object p0, v0, Landroid/support/v7/internal/a/l;->k:Landroid/support/v7/c/a;

    .line 970
    iget-object v0, p0, Landroid/support/v7/internal/a/p;->a:Landroid/support/v7/internal/a/l;

    iget-object v1, p0, Landroid/support/v7/internal/a/p;->f:Landroid/support/v7/c/b;

    iput-object v1, v0, Landroid/support/v7/internal/a/l;->l:Landroid/support/v7/c/b;

    .line 974
    :goto_24
    iput-object v2, p0, Landroid/support/v7/internal/a/p;->f:Landroid/support/v7/c/b;

    .line 975
    iget-object v0, p0, Landroid/support/v7/internal/a/p;->a:Landroid/support/v7/internal/a/l;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/a/l;->j(Z)V

    .line 978
    iget-object v0, p0, Landroid/support/v7/internal/a/p;->a:Landroid/support/v7/internal/a/l;

    invoke-static {v0}, Landroid/support/v7/internal/a/l;->h(Landroid/support/v7/internal/a/l;)Landroid/support/v7/internal/widget/ActionBarContextView;

    move-result-object v0

    .line 2190
    iget-object v1, v0, Landroid/support/v7/internal/widget/ActionBarContextView;->g:Landroid/view/View;

    if-nez v1, :cond_39

    .line 2191
    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarContextView;->i()V

    .line 979
    :cond_39
    iget-object v0, p0, Landroid/support/v7/internal/a/p;->a:Landroid/support/v7/internal/a/l;

    invoke-static {v0}, Landroid/support/v7/internal/a/l;->i(Landroid/support/v7/internal/a/l;)Landroid/support/v7/internal/widget/ad;

    move-result-object v0

    invoke-interface {v0}, Landroid/support/v7/internal/widget/ad;->a()Landroid/view/ViewGroup;

    move-result-object v0

    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->sendAccessibilityEvent(I)V

    .line 981
    iget-object v0, p0, Landroid/support/v7/internal/a/p;->a:Landroid/support/v7/internal/a/l;

    invoke-static {v0}, Landroid/support/v7/internal/a/l;->e(Landroid/support/v7/internal/a/l;)Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v7/internal/a/p;->a:Landroid/support/v7/internal/a/l;

    iget-boolean v1, v1, Landroid/support/v7/internal/a/l;->m:Z

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/widget/ActionBarOverlayLayout;->setHideOnContentScrollEnabled(Z)V

    .line 983
    iget-object v0, p0, Landroid/support/v7/internal/a/p;->a:Landroid/support/v7/internal/a/l;

    iput-object v2, v0, Landroid/support/v7/internal/a/l;->j:Landroid/support/v7/internal/a/p;

    goto :goto_7

    .line 972
    :cond_5a
    iget-object v0, p0, Landroid/support/v7/internal/a/p;->f:Landroid/support/v7/c/b;

    invoke-interface {v0, p0}, Landroid/support/v7/c/b;->a(Landroid/support/v7/c/a;)V

    goto :goto_24
.end method

.method public final d()V
    .registers 3

    .prologue
    .line 988
    iget-object v0, p0, Landroid/support/v7/internal/a/p;->a:Landroid/support/v7/internal/a/l;

    iget-object v0, v0, Landroid/support/v7/internal/a/l;->j:Landroid/support/v7/internal/a/p;

    if-eq v0, p0, :cond_7

    .line 1000
    :goto_6
    return-void

    .line 995
    :cond_7
    iget-object v0, p0, Landroid/support/v7/internal/a/p;->e:Landroid/support/v7/internal/view/menu/i;

    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/i;->d()V

    .line 997
    :try_start_c
    iget-object v0, p0, Landroid/support/v7/internal/a/p;->f:Landroid/support/v7/c/b;

    iget-object v1, p0, Landroid/support/v7/internal/a/p;->e:Landroid/support/v7/internal/view/menu/i;

    invoke-interface {v0, p0, v1}, Landroid/support/v7/c/b;->b(Landroid/support/v7/c/a;Landroid/view/Menu;)Z
    :try_end_13
    .catchall {:try_start_c .. :try_end_13} :catchall_19

    .line 999
    iget-object v0, p0, Landroid/support/v7/internal/a/p;->e:Landroid/support/v7/internal/view/menu/i;

    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/i;->e()V

    goto :goto_6

    :catchall_19
    move-exception v0

    iget-object v1, p0, Landroid/support/v7/internal/a/p;->e:Landroid/support/v7/internal/view/menu/i;

    invoke-virtual {v1}, Landroid/support/v7/internal/view/menu/i;->e()V

    throw v0
.end method

.method public final e()Z
    .registers 3

    .prologue
    .line 1004
    iget-object v0, p0, Landroid/support/v7/internal/a/p;->e:Landroid/support/v7/internal/view/menu/i;

    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/i;->d()V

    .line 1006
    :try_start_5
    iget-object v0, p0, Landroid/support/v7/internal/a/p;->f:Landroid/support/v7/c/b;

    iget-object v1, p0, Landroid/support/v7/internal/a/p;->e:Landroid/support/v7/internal/view/menu/i;

    invoke-interface {v0, p0, v1}, Landroid/support/v7/c/b;->a(Landroid/support/v7/c/a;Landroid/view/Menu;)Z
    :try_end_c
    .catchall {:try_start_5 .. :try_end_c} :catchall_13

    move-result v0

    .line 1008
    iget-object v1, p0, Landroid/support/v7/internal/a/p;->e:Landroid/support/v7/internal/view/menu/i;

    invoke-virtual {v1}, Landroid/support/v7/internal/view/menu/i;->e()V

    return v0

    :catchall_13
    move-exception v0

    iget-object v1, p0, Landroid/support/v7/internal/a/p;->e:Landroid/support/v7/internal/view/menu/i;

    invoke-virtual {v1}, Landroid/support/v7/internal/view/menu/i;->e()V

    throw v0
.end method

.method public final f()Ljava/lang/CharSequence;
    .registers 2

    .prologue
    .line 1040
    iget-object v0, p0, Landroid/support/v7/internal/a/p;->a:Landroid/support/v7/internal/a/l;

    invoke-static {v0}, Landroid/support/v7/internal/a/l;->h(Landroid/support/v7/internal/a/l;)Landroid/support/v7/internal/widget/ActionBarContextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarContextView;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public final g()Ljava/lang/CharSequence;
    .registers 2

    .prologue
    .line 1045
    iget-object v0, p0, Landroid/support/v7/internal/a/p;->a:Landroid/support/v7/internal/a/l;

    invoke-static {v0}, Landroid/support/v7/internal/a/l;->h(Landroid/support/v7/internal/a/l;)Landroid/support/v7/internal/widget/ActionBarContextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarContextView;->getSubtitle()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public final h()Z
    .registers 2

    .prologue
    .line 1056
    iget-object v0, p0, Landroid/support/v7/internal/a/p;->a:Landroid/support/v7/internal/a/l;

    invoke-static {v0}, Landroid/support/v7/internal/a/l;->h(Landroid/support/v7/internal/a/l;)Landroid/support/v7/internal/widget/ActionBarContextView;

    move-result-object v0

    .line 2377
    iget-boolean v0, v0, Landroid/support/v7/internal/widget/ActionBarContextView;->h:Z

    .line 1056
    return v0
.end method

.method public final i()Landroid/view/View;
    .registers 2

    .prologue
    .line 1061
    iget-object v0, p0, Landroid/support/v7/internal/a/p;->g:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_d

    iget-object v0, p0, Landroid/support/v7/internal/a/p;->g:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    :goto_c
    return-object v0

    :cond_d
    const/4 v0, 0x0

    goto :goto_c
.end method
