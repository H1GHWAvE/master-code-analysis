.class final Landroid/support/v7/internal/a/h;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/support/v7/internal/view/menu/y;


# instance fields
.field final synthetic a:Landroid/support/v7/internal/a/e;

.field private b:Z


# direct methods
.method private constructor <init>(Landroid/support/v7/internal/a/e;)V
    .registers 2

    .prologue
    .line 586
    iput-object p1, p0, Landroid/support/v7/internal/a/h;->a:Landroid/support/v7/internal/a/e;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Landroid/support/v7/internal/a/e;B)V
    .registers 3

    .prologue
    .line 586
    invoke-direct {p0, p1}, Landroid/support/v7/internal/a/h;-><init>(Landroid/support/v7/internal/a/e;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/support/v7/internal/view/menu/i;Z)V
    .registers 5

    .prologue
    .line 600
    iget-boolean v0, p0, Landroid/support/v7/internal/a/h;->b:Z

    if-eqz v0, :cond_5

    .line 610
    :goto_4
    return-void

    .line 604
    :cond_5
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/internal/a/h;->b:Z

    .line 605
    iget-object v0, p0, Landroid/support/v7/internal/a/h;->a:Landroid/support/v7/internal/a/e;

    .line 3051
    iget-object v0, v0, Landroid/support/v7/internal/a/e;->i:Landroid/support/v7/internal/widget/ad;

    .line 605
    invoke-interface {v0}, Landroid/support/v7/internal/widget/ad;->q()V

    .line 606
    iget-object v0, p0, Landroid/support/v7/internal/a/h;->a:Landroid/support/v7/internal/a/e;

    .line 4051
    iget-object v0, v0, Landroid/support/v7/internal/a/e;->k:Landroid/view/Window$Callback;

    .line 606
    if-eqz v0, :cond_1e

    .line 607
    iget-object v0, p0, Landroid/support/v7/internal/a/h;->a:Landroid/support/v7/internal/a/e;

    .line 5051
    iget-object v0, v0, Landroid/support/v7/internal/a/e;->k:Landroid/view/Window$Callback;

    .line 607
    const/16 v1, 0x6c

    invoke-interface {v0, v1, p1}, Landroid/view/Window$Callback;->onPanelClosed(ILandroid/view/Menu;)V

    .line 609
    :cond_1e
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v7/internal/a/h;->b:Z

    goto :goto_4
.end method

.method public final a_(Landroid/support/v7/internal/view/menu/i;)Z
    .registers 4

    .prologue
    .line 591
    iget-object v0, p0, Landroid/support/v7/internal/a/h;->a:Landroid/support/v7/internal/a/e;

    .line 1051
    iget-object v0, v0, Landroid/support/v7/internal/a/e;->k:Landroid/view/Window$Callback;

    .line 591
    if-eqz v0, :cond_11

    .line 592
    iget-object v0, p0, Landroid/support/v7/internal/a/h;->a:Landroid/support/v7/internal/a/e;

    .line 2051
    iget-object v0, v0, Landroid/support/v7/internal/a/e;->k:Landroid/view/Window$Callback;

    .line 592
    const/16 v1, 0x6c

    invoke-interface {v0, v1, p1}, Landroid/view/Window$Callback;->onMenuOpened(ILandroid/view/Menu;)Z

    .line 593
    const/4 v0, 0x1

    .line 595
    :goto_10
    return v0

    :cond_11
    const/4 v0, 0x0

    goto :goto_10
.end method
