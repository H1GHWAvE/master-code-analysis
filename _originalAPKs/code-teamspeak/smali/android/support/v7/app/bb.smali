.class final Landroid/support/v7/app/bb;
.super Landroid/support/v4/view/ge;
.source "SourceFile"


# instance fields
.field final synthetic a:Landroid/support/v7/app/ba;


# direct methods
.method constructor <init>(Landroid/support/v7/app/ba;)V
    .registers 2

    .prologue
    .line 1664
    iput-object p1, p0, Landroid/support/v7/app/bb;->a:Landroid/support/v7/app/ba;

    invoke-direct {p0}, Landroid/support/v4/view/ge;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(Landroid/view/View;)V
    .registers 5

    .prologue
    const/4 v2, 0x0

    .line 1667
    iget-object v0, p0, Landroid/support/v7/app/bb;->a:Landroid/support/v7/app/ba;

    iget-object v0, v0, Landroid/support/v7/app/ba;->a:Landroid/support/v7/app/AppCompatDelegateImplV7;

    iget-object v0, v0, Landroid/support/v7/app/AppCompatDelegateImplV7;->u:Landroid/support/v7/internal/widget/ActionBarContextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/widget/ActionBarContextView;->setVisibility(I)V

    .line 1668
    iget-object v0, p0, Landroid/support/v7/app/bb;->a:Landroid/support/v7/app/ba;

    iget-object v0, v0, Landroid/support/v7/app/ba;->a:Landroid/support/v7/app/AppCompatDelegateImplV7;

    iget-object v0, v0, Landroid/support/v7/app/AppCompatDelegateImplV7;->v:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_36

    .line 1669
    iget-object v0, p0, Landroid/support/v7/app/bb;->a:Landroid/support/v7/app/ba;

    iget-object v0, v0, Landroid/support/v7/app/ba;->a:Landroid/support/v7/app/AppCompatDelegateImplV7;

    iget-object v0, v0, Landroid/support/v7/app/AppCompatDelegateImplV7;->v:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    .line 1673
    :cond_1d
    :goto_1d
    iget-object v0, p0, Landroid/support/v7/app/bb;->a:Landroid/support/v7/app/ba;

    iget-object v0, v0, Landroid/support/v7/app/ba;->a:Landroid/support/v7/app/AppCompatDelegateImplV7;

    iget-object v0, v0, Landroid/support/v7/app/AppCompatDelegateImplV7;->u:Landroid/support/v7/internal/widget/ActionBarContextView;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarContextView;->removeAllViews()V

    .line 1674
    iget-object v0, p0, Landroid/support/v7/app/bb;->a:Landroid/support/v7/app/ba;

    iget-object v0, v0, Landroid/support/v7/app/ba;->a:Landroid/support/v7/app/AppCompatDelegateImplV7;

    iget-object v0, v0, Landroid/support/v7/app/AppCompatDelegateImplV7;->x:Landroid/support/v4/view/fk;

    invoke-virtual {v0, v2}, Landroid/support/v4/view/fk;->a(Landroid/support/v4/view/gd;)Landroid/support/v4/view/fk;

    .line 1675
    iget-object v0, p0, Landroid/support/v7/app/bb;->a:Landroid/support/v7/app/ba;

    iget-object v0, v0, Landroid/support/v7/app/ba;->a:Landroid/support/v7/app/AppCompatDelegateImplV7;

    iput-object v2, v0, Landroid/support/v7/app/AppCompatDelegateImplV7;->x:Landroid/support/v4/view/fk;

    .line 1676
    return-void

    .line 1670
    :cond_36
    iget-object v0, p0, Landroid/support/v7/app/bb;->a:Landroid/support/v7/app/ba;

    iget-object v0, v0, Landroid/support/v7/app/ba;->a:Landroid/support/v7/app/AppCompatDelegateImplV7;

    iget-object v0, v0, Landroid/support/v7/app/AppCompatDelegateImplV7;->u:Landroid/support/v7/internal/widget/ActionBarContextView;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarContextView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    instance-of v0, v0, Landroid/view/View;

    if-eqz v0, :cond_1d

    .line 1671
    iget-object v0, p0, Landroid/support/v7/app/bb;->a:Landroid/support/v7/app/ba;

    iget-object v0, v0, Landroid/support/v7/app/ba;->a:Landroid/support/v7/app/AppCompatDelegateImplV7;

    iget-object v0, v0, Landroid/support/v7/app/AppCompatDelegateImplV7;->u:Landroid/support/v7/internal/widget/ActionBarContextView;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarContextView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Landroid/support/v4/view/cx;->t(Landroid/view/View;)V

    goto :goto_1d
.end method
