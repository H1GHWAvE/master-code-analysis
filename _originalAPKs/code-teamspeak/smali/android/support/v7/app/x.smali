.class public final Landroid/support/v7/app/x;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public A:I

.field public B:Z

.field public C:[Z

.field public D:Z

.field public E:Z

.field public F:I

.field public G:Landroid/content/DialogInterface$OnMultiChoiceClickListener;

.field public H:Landroid/database/Cursor;

.field public I:Ljava/lang/String;

.field public J:Ljava/lang/String;

.field public K:Z

.field public L:Landroid/widget/AdapterView$OnItemSelectedListener;

.field public M:Landroid/support/v7/app/ac;

.field public N:Z

.field public final a:Landroid/content/Context;

.field public final b:Landroid/view/LayoutInflater;

.field public c:I

.field public d:Landroid/graphics/drawable/Drawable;

.field public e:I

.field public f:Ljava/lang/CharSequence;

.field public g:Landroid/view/View;

.field public h:Ljava/lang/CharSequence;

.field public i:Ljava/lang/CharSequence;

.field public j:Landroid/content/DialogInterface$OnClickListener;

.field public k:Ljava/lang/CharSequence;

.field public l:Landroid/content/DialogInterface$OnClickListener;

.field public m:Ljava/lang/CharSequence;

.field public n:Landroid/content/DialogInterface$OnClickListener;

.field public o:Z

.field public p:Landroid/content/DialogInterface$OnCancelListener;

.field public q:Landroid/content/DialogInterface$OnDismissListener;

.field public r:Landroid/content/DialogInterface$OnKeyListener;

.field public s:[Ljava/lang/CharSequence;

.field public t:Landroid/widget/ListAdapter;

.field public u:Landroid/content/DialogInterface$OnClickListener;

.field public v:I

.field public w:Landroid/view/View;

.field public x:I

.field public y:I

.field public z:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 670
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 617
    iput v0, p0, Landroid/support/v7/app/x;->c:I

    .line 619
    iput v0, p0, Landroid/support/v7/app/x;->e:I

    .line 642
    iput-boolean v0, p0, Landroid/support/v7/app/x;->B:Z

    .line 646
    const/4 v0, -0x1

    iput v0, p0, Landroid/support/v7/app/x;->F:I

    .line 654
    iput-boolean v1, p0, Landroid/support/v7/app/x;->N:Z

    .line 671
    iput-object p1, p0, Landroid/support/v7/app/x;->a:Landroid/content/Context;

    .line 672
    iput-boolean v1, p0, Landroid/support/v7/app/x;->o:Z

    .line 673
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Landroid/support/v7/app/x;->b:Landroid/view/LayoutInflater;

    .line 674
    return-void
.end method

.method private a(Landroid/support/v7/app/v;)V
    .registers 14

    .prologue
    .line 677
    iget-object v0, p0, Landroid/support/v7/app/x;->g:Landroid/view/View;

    if-eqz v0, :cond_9f

    .line 678
    iget-object v0, p0, Landroid/support/v7/app/x;->g:Landroid/view/View;

    .line 1241
    iput-object v0, p1, Landroid/support/v7/app/v;->C:Landroid/view/View;

    .line 693
    :cond_8
    :goto_8
    iget-object v0, p0, Landroid/support/v7/app/x;->h:Ljava/lang/CharSequence;

    if-eqz v0, :cond_11

    .line 694
    iget-object v0, p0, Landroid/support/v7/app/x;->h:Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Landroid/support/v7/app/v;->b(Ljava/lang/CharSequence;)V

    .line 696
    :cond_11
    iget-object v0, p0, Landroid/support/v7/app/x;->i:Ljava/lang/CharSequence;

    if-eqz v0, :cond_1e

    .line 697
    const/4 v0, -0x1

    iget-object v1, p0, Landroid/support/v7/app/x;->i:Ljava/lang/CharSequence;

    iget-object v2, p0, Landroid/support/v7/app/x;->j:Landroid/content/DialogInterface$OnClickListener;

    const/4 v3, 0x0

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/support/v7/app/v;->a(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;Landroid/os/Message;)V

    .line 700
    :cond_1e
    iget-object v0, p0, Landroid/support/v7/app/x;->k:Ljava/lang/CharSequence;

    if-eqz v0, :cond_2b

    .line 701
    const/4 v0, -0x2

    iget-object v1, p0, Landroid/support/v7/app/x;->k:Ljava/lang/CharSequence;

    iget-object v2, p0, Landroid/support/v7/app/x;->l:Landroid/content/DialogInterface$OnClickListener;

    const/4 v3, 0x0

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/support/v7/app/v;->a(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;Landroid/os/Message;)V

    .line 704
    :cond_2b
    iget-object v0, p0, Landroid/support/v7/app/x;->m:Ljava/lang/CharSequence;

    if-eqz v0, :cond_38

    .line 705
    const/4 v0, -0x3

    iget-object v1, p0, Landroid/support/v7/app/x;->m:Ljava/lang/CharSequence;

    iget-object v2, p0, Landroid/support/v7/app/x;->n:Landroid/content/DialogInterface$OnClickListener;

    const/4 v3, 0x0

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/support/v7/app/v;->a(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;Landroid/os/Message;)V

    .line 710
    :cond_38
    iget-object v0, p0, Landroid/support/v7/app/x;->s:[Ljava/lang/CharSequence;

    if-nez v0, :cond_44

    iget-object v0, p0, Landroid/support/v7/app/x;->H:Landroid/database/Cursor;

    if-nez v0, :cond_44

    iget-object v0, p0, Landroid/support/v7/app/x;->t:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_88

    .line 1734
    :cond_44
    iget-object v0, p0, Landroid/support/v7/app/x;->b:Landroid/view/LayoutInflater;

    .line 2057
    iget v1, p1, Landroid/support/v7/app/v;->H:I

    .line 1734
    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ListView;

    .line 1737
    iget-boolean v0, p0, Landroid/support/v7/app/x;->D:Z

    if-eqz v0, :cond_e3

    .line 1738
    iget-object v0, p0, Landroid/support/v7/app/x;->H:Landroid/database/Cursor;

    if-nez v0, :cond_d6

    .line 1739
    new-instance v0, Landroid/support/v7/app/y;

    iget-object v2, p0, Landroid/support/v7/app/x;->a:Landroid/content/Context;

    .line 3057
    iget v3, p1, Landroid/support/v7/app/v;->I:I

    .line 1739
    iget-object v4, p0, Landroid/support/v7/app/x;->s:[Ljava/lang/CharSequence;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Landroid/support/v7/app/y;-><init>(Landroid/support/v7/app/x;Landroid/content/Context;I[Ljava/lang/CharSequence;Landroid/widget/ListView;)V

    .line 6057
    :goto_63
    iput-object v0, p1, Landroid/support/v7/app/v;->D:Landroid/widget/ListAdapter;

    .line 1801
    iget v0, p0, Landroid/support/v7/app/x;->F:I

    .line 7057
    iput v0, p1, Landroid/support/v7/app/v;->E:I

    .line 1803
    iget-object v0, p0, Landroid/support/v7/app/x;->u:Landroid/content/DialogInterface$OnClickListener;

    if-eqz v0, :cond_120

    .line 1804
    new-instance v0, Landroid/support/v7/app/aa;

    invoke-direct {v0, p0, p1}, Landroid/support/v7/app/aa;-><init>(Landroid/support/v7/app/x;Landroid/support/v7/app/v;)V

    invoke-virtual {v5, v0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 1827
    :cond_75
    :goto_75
    iget-object v0, p0, Landroid/support/v7/app/x;->L:Landroid/widget/AdapterView$OnItemSelectedListener;

    if-eqz v0, :cond_7e

    .line 1828
    iget-object v0, p0, Landroid/support/v7/app/x;->L:Landroid/widget/AdapterView$OnItemSelectedListener;

    invoke-virtual {v5, v0}, Landroid/widget/ListView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 1831
    :cond_7e
    iget-boolean v0, p0, Landroid/support/v7/app/x;->E:Z

    if-eqz v0, :cond_12e

    .line 1832
    const/4 v0, 0x1

    invoke-virtual {v5, v0}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 8057
    :cond_86
    :goto_86
    iput-object v5, p1, Landroid/support/v7/app/v;->f:Landroid/widget/ListView;

    .line 713
    :cond_88
    iget-object v0, p0, Landroid/support/v7/app/x;->w:Landroid/view/View;

    if-eqz v0, :cond_13f

    .line 714
    iget-boolean v0, p0, Landroid/support/v7/app/x;->B:Z

    if-eqz v0, :cond_138

    .line 715
    iget-object v1, p0, Landroid/support/v7/app/x;->w:Landroid/view/View;

    iget v2, p0, Landroid/support/v7/app/x;->x:I

    iget v3, p0, Landroid/support/v7/app/x;->y:I

    iget v4, p0, Landroid/support/v7/app/x;->z:I

    iget v5, p0, Landroid/support/v7/app/x;->A:I

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/support/v7/app/v;->a(Landroid/view/View;IIII)V

    .line 731
    :cond_9e
    :goto_9e
    return-void

    .line 680
    :cond_9f
    iget-object v0, p0, Landroid/support/v7/app/x;->f:Ljava/lang/CharSequence;

    if-eqz v0, :cond_a8

    .line 681
    iget-object v0, p0, Landroid/support/v7/app/x;->f:Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Landroid/support/v7/app/v;->a(Ljava/lang/CharSequence;)V

    .line 683
    :cond_a8
    iget-object v0, p0, Landroid/support/v7/app/x;->d:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_b1

    .line 684
    iget-object v0, p0, Landroid/support/v7/app/x;->d:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p1, v0}, Landroid/support/v7/app/v;->a(Landroid/graphics/drawable/Drawable;)V

    .line 686
    :cond_b1
    iget v0, p0, Landroid/support/v7/app/x;->c:I

    if-eqz v0, :cond_ba

    .line 687
    iget v0, p0, Landroid/support/v7/app/x;->c:I

    invoke-virtual {p1, v0}, Landroid/support/v7/app/v;->a(I)V

    .line 689
    :cond_ba
    iget v0, p0, Landroid/support/v7/app/x;->e:I

    if-eqz v0, :cond_8

    .line 690
    iget v0, p0, Landroid/support/v7/app/x;->e:I

    .line 1374
    new-instance v1, Landroid/util/TypedValue;

    invoke-direct {v1}, Landroid/util/TypedValue;-><init>()V

    .line 1375
    iget-object v2, p1, Landroid/support/v7/app/v;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v0, v1, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 1376
    iget v0, v1, Landroid/util/TypedValue;->resourceId:I

    .line 690
    invoke-virtual {p1, v0}, Landroid/support/v7/app/v;->a(I)V

    goto/16 :goto_8

    .line 1754
    :cond_d6
    new-instance v1, Landroid/support/v7/app/z;

    iget-object v3, p0, Landroid/support/v7/app/x;->a:Landroid/content/Context;

    iget-object v4, p0, Landroid/support/v7/app/x;->H:Landroid/database/Cursor;

    move-object v2, p0

    move-object v6, p1

    invoke-direct/range {v1 .. v6}, Landroid/support/v7/app/z;-><init>(Landroid/support/v7/app/x;Landroid/content/Context;Landroid/database/Cursor;Landroid/widget/ListView;Landroid/support/v7/app/v;)V

    move-object v0, v1

    goto :goto_63

    .line 1782
    :cond_e3
    iget-boolean v0, p0, Landroid/support/v7/app/x;->E:Z

    if-eqz v0, :cond_f5

    .line 4057
    iget v8, p1, Landroid/support/v7/app/v;->J:I

    .line 1784
    :goto_e9
    iget-object v0, p0, Landroid/support/v7/app/x;->H:Landroid/database/Cursor;

    if-nez v0, :cond_103

    .line 1785
    iget-object v0, p0, Landroid/support/v7/app/x;->t:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_f8

    iget-object v0, p0, Landroid/support/v7/app/x;->t:Landroid/widget/ListAdapter;

    goto/16 :goto_63

    .line 5057
    :cond_f5
    iget v8, p1, Landroid/support/v7/app/v;->K:I

    goto :goto_e9

    .line 1785
    :cond_f8
    new-instance v0, Landroid/support/v7/app/ae;

    iget-object v1, p0, Landroid/support/v7/app/x;->a:Landroid/content/Context;

    iget-object v2, p0, Landroid/support/v7/app/x;->s:[Ljava/lang/CharSequence;

    invoke-direct {v0, v1, v8, v2}, Landroid/support/v7/app/ae;-><init>(Landroid/content/Context;I[Ljava/lang/CharSequence;)V

    goto/16 :goto_63

    .line 1788
    :cond_103
    new-instance v6, Landroid/widget/SimpleCursorAdapter;

    iget-object v7, p0, Landroid/support/v7/app/x;->a:Landroid/content/Context;

    iget-object v9, p0, Landroid/support/v7/app/x;->H:Landroid/database/Cursor;

    const/4 v0, 0x1

    new-array v10, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    iget-object v1, p0, Landroid/support/v7/app/x;->I:Ljava/lang/String;

    aput-object v1, v10, v0

    const/4 v0, 0x1

    new-array v11, v0, [I

    const/4 v0, 0x0

    const v1, 0x1020014

    aput v1, v11, v0

    invoke-direct/range {v6 .. v11}, Landroid/widget/SimpleCursorAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;[Ljava/lang/String;[I)V

    move-object v0, v6

    goto/16 :goto_63

    .line 1813
    :cond_120
    iget-object v0, p0, Landroid/support/v7/app/x;->G:Landroid/content/DialogInterface$OnMultiChoiceClickListener;

    if-eqz v0, :cond_75

    .line 1814
    new-instance v0, Landroid/support/v7/app/ab;

    invoke-direct {v0, p0, v5, p1}, Landroid/support/v7/app/ab;-><init>(Landroid/support/v7/app/x;Landroid/widget/ListView;Landroid/support/v7/app/v;)V

    invoke-virtual {v5, v0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    goto/16 :goto_75

    .line 1833
    :cond_12e
    iget-boolean v0, p0, Landroid/support/v7/app/x;->D:Z

    if-eqz v0, :cond_86

    .line 1834
    const/4 v0, 0x2

    invoke-virtual {v5, v0}, Landroid/widget/ListView;->setChoiceMode(I)V

    goto/16 :goto_86

    .line 718
    :cond_138
    iget-object v0, p0, Landroid/support/v7/app/x;->w:Landroid/view/View;

    invoke-virtual {p1, v0}, Landroid/support/v7/app/v;->b(Landroid/view/View;)V

    goto/16 :goto_9e

    .line 720
    :cond_13f
    iget v0, p0, Landroid/support/v7/app/x;->v:I

    if-eqz v0, :cond_9e

    .line 721
    iget v0, p0, Landroid/support/v7/app/x;->v:I

    .line 8255
    const/4 v1, 0x0

    iput-object v1, p1, Landroid/support/v7/app/v;->g:Landroid/view/View;

    .line 8256
    iput v0, p1, Landroid/support/v7/app/v;->h:I

    .line 8257
    const/4 v0, 0x0

    iput-boolean v0, p1, Landroid/support/v7/app/v;->m:Z

    goto/16 :goto_9e
.end method

.method private b(Landroid/support/v7/app/v;)V
    .registers 15

    .prologue
    const/4 v3, 0x0

    const/4 v12, 0x1

    .line 734
    iget-object v0, p0, Landroid/support/v7/app/x;->b:Landroid/view/LayoutInflater;

    .line 9057
    iget v1, p1, Landroid/support/v7/app/v;->H:I

    .line 734
    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ListView;

    .line 737
    iget-boolean v0, p0, Landroid/support/v7/app/x;->D:Z

    if-eqz v0, :cond_53

    .line 738
    iget-object v0, p0, Landroid/support/v7/app/x;->H:Landroid/database/Cursor;

    if-nez v0, :cond_46

    .line 739
    new-instance v0, Landroid/support/v7/app/y;

    iget-object v2, p0, Landroid/support/v7/app/x;->a:Landroid/content/Context;

    .line 10057
    iget v3, p1, Landroid/support/v7/app/v;->I:I

    .line 739
    iget-object v4, p0, Landroid/support/v7/app/x;->s:[Ljava/lang/CharSequence;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Landroid/support/v7/app/y;-><init>(Landroid/support/v7/app/x;Landroid/content/Context;I[Ljava/lang/CharSequence;Landroid/widget/ListView;)V

    .line 13057
    :goto_21
    iput-object v0, p1, Landroid/support/v7/app/v;->D:Landroid/widget/ListAdapter;

    .line 801
    iget v0, p0, Landroid/support/v7/app/x;->F:I

    .line 14057
    iput v0, p1, Landroid/support/v7/app/v;->E:I

    .line 803
    iget-object v0, p0, Landroid/support/v7/app/x;->u:Landroid/content/DialogInterface$OnClickListener;

    if-eqz v0, :cond_89

    .line 804
    new-instance v0, Landroid/support/v7/app/aa;

    invoke-direct {v0, p0, p1}, Landroid/support/v7/app/aa;-><init>(Landroid/support/v7/app/x;Landroid/support/v7/app/v;)V

    invoke-virtual {v5, v0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 827
    :cond_33
    :goto_33
    iget-object v0, p0, Landroid/support/v7/app/x;->L:Landroid/widget/AdapterView$OnItemSelectedListener;

    if-eqz v0, :cond_3c

    .line 828
    iget-object v0, p0, Landroid/support/v7/app/x;->L:Landroid/widget/AdapterView$OnItemSelectedListener;

    invoke-virtual {v5, v0}, Landroid/widget/ListView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 831
    :cond_3c
    iget-boolean v0, p0, Landroid/support/v7/app/x;->E:Z

    if-eqz v0, :cond_96

    .line 832
    invoke-virtual {v5, v12}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 15057
    :cond_43
    :goto_43
    iput-object v5, p1, Landroid/support/v7/app/v;->f:Landroid/widget/ListView;

    .line 837
    return-void

    .line 754
    :cond_46
    new-instance v1, Landroid/support/v7/app/z;

    iget-object v3, p0, Landroid/support/v7/app/x;->a:Landroid/content/Context;

    iget-object v4, p0, Landroid/support/v7/app/x;->H:Landroid/database/Cursor;

    move-object v2, p0

    move-object v6, p1

    invoke-direct/range {v1 .. v6}, Landroid/support/v7/app/z;-><init>(Landroid/support/v7/app/x;Landroid/content/Context;Landroid/database/Cursor;Landroid/widget/ListView;Landroid/support/v7/app/v;)V

    move-object v0, v1

    goto :goto_21

    .line 782
    :cond_53
    iget-boolean v0, p0, Landroid/support/v7/app/x;->E:Z

    if-eqz v0, :cond_64

    .line 11057
    iget v8, p1, Landroid/support/v7/app/v;->J:I

    .line 784
    :goto_59
    iget-object v0, p0, Landroid/support/v7/app/x;->H:Landroid/database/Cursor;

    if-nez v0, :cond_71

    .line 785
    iget-object v0, p0, Landroid/support/v7/app/x;->t:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_67

    iget-object v0, p0, Landroid/support/v7/app/x;->t:Landroid/widget/ListAdapter;

    goto :goto_21

    .line 12057
    :cond_64
    iget v8, p1, Landroid/support/v7/app/v;->K:I

    goto :goto_59

    .line 785
    :cond_67
    new-instance v0, Landroid/support/v7/app/ae;

    iget-object v1, p0, Landroid/support/v7/app/x;->a:Landroid/content/Context;

    iget-object v2, p0, Landroid/support/v7/app/x;->s:[Ljava/lang/CharSequence;

    invoke-direct {v0, v1, v8, v2}, Landroid/support/v7/app/ae;-><init>(Landroid/content/Context;I[Ljava/lang/CharSequence;)V

    goto :goto_21

    .line 788
    :cond_71
    new-instance v6, Landroid/widget/SimpleCursorAdapter;

    iget-object v7, p0, Landroid/support/v7/app/x;->a:Landroid/content/Context;

    iget-object v9, p0, Landroid/support/v7/app/x;->H:Landroid/database/Cursor;

    new-array v10, v12, [Ljava/lang/String;

    iget-object v0, p0, Landroid/support/v7/app/x;->I:Ljava/lang/String;

    aput-object v0, v10, v3

    new-array v11, v12, [I

    const v0, 0x1020014

    aput v0, v11, v3

    invoke-direct/range {v6 .. v11}, Landroid/widget/SimpleCursorAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;[Ljava/lang/String;[I)V

    move-object v0, v6

    goto :goto_21

    .line 813
    :cond_89
    iget-object v0, p0, Landroid/support/v7/app/x;->G:Landroid/content/DialogInterface$OnMultiChoiceClickListener;

    if-eqz v0, :cond_33

    .line 814
    new-instance v0, Landroid/support/v7/app/ab;

    invoke-direct {v0, p0, v5, p1}, Landroid/support/v7/app/ab;-><init>(Landroid/support/v7/app/x;Landroid/widget/ListView;Landroid/support/v7/app/v;)V

    invoke-virtual {v5, v0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    goto :goto_33

    .line 833
    :cond_96
    iget-boolean v0, p0, Landroid/support/v7/app/x;->D:Z

    if-eqz v0, :cond_43

    .line 834
    const/4 v0, 0x2

    invoke-virtual {v5, v0}, Landroid/widget/ListView;->setChoiceMode(I)V

    goto :goto_43
.end method
