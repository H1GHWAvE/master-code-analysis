.class public Landroid/support/v7/app/ah;
.super Landroid/support/v4/app/bb;
.source "SourceFile"

# interfaces
.implements Landroid/support/v4/app/gm;
.implements Landroid/support/v7/app/ai;
.implements Landroid/support/v7/app/m;


# instance fields
.field private m:Landroid/support/v7/app/aj;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 52
    invoke-direct {p0}, Landroid/support/v4/app/bb;-><init>()V

    return-void
.end method

.method private a(Landroid/support/v7/c/b;)Landroid/support/v7/c/a;
    .registers 3

    .prologue
    .line 237
    invoke-virtual {p0}, Landroid/support/v7/app/ah;->i()Landroid/support/v7/app/aj;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v7/app/aj;->a(Landroid/support/v7/c/b;)Landroid/support/v7/c/a;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/support/v4/app/gl;)V
    .registers 4

    .prologue
    .line 292
    .line 1197
    const/4 v0, 0x0

    .line 1198
    instance-of v1, p0, Landroid/support/v4/app/gm;

    if-eqz v1, :cond_c

    move-object v0, p0

    .line 1199
    check-cast v0, Landroid/support/v4/app/gm;

    invoke-interface {v0}, Landroid/support/v4/app/gm;->a_()Landroid/content/Intent;

    move-result-object v0

    .line 1201
    :cond_c
    if-nez v0, :cond_2c

    .line 1202
    invoke-static {p0}, Landroid/support/v4/app/cv;->a(Landroid/app/Activity;)Landroid/content/Intent;

    move-result-object v0

    move-object v1, v0

    .line 1205
    :goto_13
    if-eqz v1, :cond_2b

    .line 1208
    invoke-virtual {v1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    .line 1209
    if-nez v0, :cond_25

    .line 1210
    iget-object v0, p1, Landroid/support/v4/app/gl;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->resolveActivity(Landroid/content/pm/PackageManager;)Landroid/content/ComponentName;

    move-result-object v0

    .line 1212
    :cond_25
    invoke-virtual {p1, v0}, Landroid/support/v4/app/gl;->a(Landroid/content/ComponentName;)Landroid/support/v4/app/gl;

    .line 1213
    invoke-virtual {p1, v1}, Landroid/support/v4/app/gl;->a(Landroid/content/Intent;)Landroid/support/v4/app/gl;

    .line 293
    :cond_2b
    return-void

    :cond_2c
    move-object v1, v0

    goto :goto_13
.end method

.method private a(Landroid/support/v7/widget/Toolbar;)V
    .registers 3
    .param p1    # Landroid/support/v7/widget/Toolbar;
        .annotation build Landroid/support/a/z;
        .end annotation
    .end param

    .prologue
    .line 99
    invoke-virtual {p0}, Landroid/support/v7/app/ah;->i()Landroid/support/v7/app/aj;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v7/app/aj;->a(Landroid/support/v7/widget/Toolbar;)V

    .line 100
    return-void
.end method

.method private a(Landroid/content/Intent;)Z
    .registers 3

    .prologue
    .line 391
    invoke-static {p0, p1}, Landroid/support/v4/app/cv;->a(Landroid/app/Activity;Landroid/content/Intent;)Z

    move-result v0

    return v0
.end method

.method private b(Landroid/content/Intent;)V
    .registers 2

    .prologue
    .line 407
    invoke-static {p0, p1}, Landroid/support/v4/app/cv;->b(Landroid/app/Activity;Landroid/content/Intent;)V

    .line 408
    return-void
.end method

.method private b(I)Z
    .registers 3

    .prologue
    .line 186
    invoke-virtual {p0}, Landroid/support/v7/app/ah;->i()Landroid/support/v7/app/aj;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v7/app/aj;->b(I)Z

    move-result v0

    return v0
.end method

.method private j()Landroid/support/v7/app/a;
    .registers 2
    .annotation build Landroid/support/a/z;
    .end annotation

    .prologue
    .line 79
    invoke-virtual {p0}, Landroid/support/v7/app/ah;->i()Landroid/support/v7/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/aj;->a()Landroid/support/v7/app/a;

    move-result-object v0

    return-object v0
.end method

.method private static k()V
    .registers 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 245
    return-void
.end method

.method private static l()V
    .registers 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 252
    return-void
.end method

.method private static m()V
    .registers 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 259
    return-void
.end method

.method private static n()V
    .registers 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 266
    return-void
.end method

.method private static o()V
    .registers 0

    .prologue
    .line 311
    return-void
.end method

.method private static p()V
    .registers 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 421
    return-void
.end method


# virtual methods
.method public final a_()Landroid/content/Intent;
    .registers 2
    .annotation build Landroid/support/a/z;
    .end annotation

    .prologue
    .line 374
    invoke-static {p0}, Landroid/support/v4/app/cv;->a(Landroid/app/Activity;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public addContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .registers 4

    .prologue
    .line 124
    invoke-virtual {p0}, Landroid/support/v7/app/ah;->i()Landroid/support/v7/app/aj;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroid/support/v7/app/aj;->b(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 125
    return-void
.end method

.method public final b()V
    .registers 2

    .prologue
    .line 191
    invoke-virtual {p0}, Landroid/support/v7/app/ah;->i()Landroid/support/v7/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/aj;->g()V

    .line 192
    return-void
.end method

.method public final d()Landroid/support/v7/app/l;
    .registers 2
    .annotation build Landroid/support/a/z;
    .end annotation

    .prologue
    .line 426
    invoke-virtual {p0}, Landroid/support/v7/app/ah;->i()Landroid/support/v7/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/aj;->i()Landroid/support/v7/app/l;

    move-result-object v0

    return-object v0
.end method

.method public final e()V
    .registers 1
    .annotation build Landroid/support/a/h;
    .end annotation

    .prologue
    .line 209
    return-void
.end method

.method public final f()V
    .registers 1
    .annotation build Landroid/support/a/h;
    .end annotation

    .prologue
    .line 219
    return-void
.end method

.method public final g()Landroid/support/v7/c/a;
    .registers 2
    .annotation build Landroid/support/a/z;
    .end annotation

    .prologue
    .line 233
    const/4 v0, 0x0

    return-object v0
.end method

.method public getMenuInflater()Landroid/view/MenuInflater;
    .registers 2

    .prologue
    .line 104
    invoke-virtual {p0}, Landroid/support/v7/app/ah;->i()Landroid/support/v7/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/aj;->b()Landroid/view/MenuInflater;

    move-result-object v0

    return-object v0
.end method

.method public h()Z
    .registers 6

    .prologue
    const/4 v1, 0x0

    .line 337
    .line 1374
    invoke-static {p0}, Landroid/support/v4/app/cv;->a(Landroid/app/Activity;)Landroid/content/Intent;

    move-result-object v0

    .line 339
    if-eqz v0, :cond_a0

    .line 1391
    invoke-static {p0, v0}, Landroid/support/v4/app/cv;->a(Landroid/app/Activity;Landroid/content/Intent;)Z

    move-result v2

    .line 340
    if-eqz v2, :cond_9c

    .line 341
    invoke-static {p0}, Landroid/support/v4/app/gl;->a(Landroid/content/Context;)Landroid/support/v4/app/gl;

    move-result-object v3

    .line 3197
    const/4 v0, 0x0

    .line 3198
    instance-of v2, p0, Landroid/support/v4/app/gm;

    if-eqz v2, :cond_1d

    move-object v0, p0

    .line 3199
    check-cast v0, Landroid/support/v4/app/gm;

    invoke-interface {v0}, Landroid/support/v4/app/gm;->a_()Landroid/content/Intent;

    move-result-object v0

    .line 3201
    :cond_1d
    if-nez v0, :cond_a2

    .line 3202
    invoke-static {p0}, Landroid/support/v4/app/cv;->a(Landroid/app/Activity;)Landroid/content/Intent;

    move-result-object v0

    move-object v2, v0

    .line 3205
    :goto_24
    if-eqz v2, :cond_3c

    .line 3208
    invoke-virtual {v2}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    .line 3209
    if-nez v0, :cond_36

    .line 3210
    iget-object v0, v3, Landroid/support/v4/app/gl;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/content/Intent;->resolveActivity(Landroid/content/pm/PackageManager;)Landroid/content/ComponentName;

    move-result-object v0

    .line 3212
    :cond_36
    invoke-virtual {v3, v0}, Landroid/support/v4/app/gl;->a(Landroid/content/ComponentName;)Landroid/support/v4/app/gl;

    .line 3213
    invoke-virtual {v3, v2}, Landroid/support/v4/app/gl;->a(Landroid/content/Intent;)Landroid/support/v4/app/gl;

    .line 3316
    :cond_3c
    iget-object v0, v3, Landroid/support/v4/app/gl;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4c

    .line 3317
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No intents added to TaskStackBuilder; cannot startActivities"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 3321
    :cond_4c
    iget-object v0, v3, Landroid/support/v4/app/gl;->a:Ljava/util/ArrayList;

    iget-object v2, v3, Landroid/support/v4/app/gl;->a:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    new-array v2, v2, [Landroid/content/Intent;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/content/Intent;

    .line 3322
    new-instance v2, Landroid/content/Intent;

    aget-object v4, v0, v1

    invoke-direct {v2, v4}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    const v4, 0x1000c000

    invoke-virtual {v2, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v2

    aput-object v2, v0, v1

    .line 3325
    iget-object v1, v3, Landroid/support/v4/app/gl;->b:Landroid/content/Context;

    invoke-static {v1, v0}, Landroid/support/v4/c/h;->a(Landroid/content/Context;[Landroid/content/Intent;)Z

    move-result v1

    if-nez v1, :cond_88

    .line 3326
    new-instance v1, Landroid/content/Intent;

    array-length v2, v0

    add-int/lit8 v2, v2, -0x1

    aget-object v0, v0, v2

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    .line 3327
    const/high16 v0, 0x10000000

    invoke-virtual {v1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 3328
    iget-object v0, v3, Landroid/support/v4/app/gl;->b:Landroid/content/Context;

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 4176
    :cond_88
    :try_start_88
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_93

    .line 5034
    invoke-virtual {p0}, Landroid/app/Activity;->finishAffinity()V

    .line 358
    :goto_91
    const/4 v0, 0x1

    .line 360
    :goto_92
    return v0

    .line 4179
    :cond_93
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V
    :try_end_96
    .catch Ljava/lang/IllegalStateException; {:try_start_88 .. :try_end_96} :catch_97

    goto :goto_91

    .line 351
    :catch_97
    move-exception v0

    invoke-virtual {p0}, Landroid/support/v7/app/ah;->finish()V

    goto :goto_91

    .line 5407
    :cond_9c
    invoke-static {p0, v0}, Landroid/support/v4/app/cv;->b(Landroid/app/Activity;Landroid/content/Intent;)V

    goto :goto_91

    :cond_a0
    move v0, v1

    .line 360
    goto :goto_92

    :cond_a2
    move-object v2, v0

    goto :goto_24
.end method

.method public final i()Landroid/support/v7/app/aj;
    .registers 2

    .prologue
    .line 455
    iget-object v0, p0, Landroid/support/v7/app/ah;->m:Landroid/support/v7/app/aj;

    if-nez v0, :cond_e

    .line 6117
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-static {p0, v0, p0}, Landroid/support/v7/app/aj;->a(Landroid/content/Context;Landroid/view/Window;Landroid/support/v7/app/ai;)Landroid/support/v7/app/aj;

    move-result-object v0

    .line 456
    iput-object v0, p0, Landroid/support/v7/app/ah;->m:Landroid/support/v7/app/aj;

    .line 458
    :cond_e
    iget-object v0, p0, Landroid/support/v7/app/ah;->m:Landroid/support/v7/app/aj;

    return-object v0
.end method

.method public invalidateOptionsMenu()V
    .registers 2

    .prologue
    .line 198
    invoke-virtual {p0}, Landroid/support/v7/app/ah;->i()Landroid/support/v7/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/aj;->g()V

    .line 199
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .registers 3

    .prologue
    .line 129
    invoke-super {p0, p1}, Landroid/support/v4/app/bb;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 130
    invoke-virtual {p0}, Landroid/support/v7/app/ah;->i()Landroid/support/v7/app/aj;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v7/app/aj;->a(Landroid/content/res/Configuration;)V

    .line 131
    return-void
.end method

.method public onContentChanged()V
    .registers 1

    .prologue
    .line 414
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 3
    .param p1    # Landroid/os/Bundle;
        .annotation build Landroid/support/a/z;
        .end annotation
    .end param

    .prologue
    .line 59
    invoke-virtual {p0}, Landroid/support/v7/app/ah;->i()Landroid/support/v7/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/aj;->j()V

    .line 60
    invoke-virtual {p0}, Landroid/support/v7/app/ah;->i()Landroid/support/v7/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/aj;->c()V

    .line 61
    invoke-super {p0, p1}, Landroid/support/v4/app/bb;->onCreate(Landroid/os/Bundle;)V

    .line 62
    return-void
.end method

.method public onDestroy()V
    .registers 2

    .prologue
    .line 161
    invoke-super {p0}, Landroid/support/v4/app/bb;->onDestroy()V

    .line 162
    invoke-virtual {p0}, Landroid/support/v7/app/ah;->i()Landroid/support/v7/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/aj;->h()V

    .line 163
    return-void
.end method

.method public final onMenuItemSelected(ILandroid/view/MenuItem;)Z
    .registers 6

    .prologue
    .line 147
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/bb;->onMenuItemSelected(ILandroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 148
    const/4 v0, 0x1

    .line 156
    :goto_7
    return v0

    .line 1079
    :cond_8
    invoke-virtual {p0}, Landroid/support/v7/app/ah;->i()Landroid/support/v7/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/aj;->a()Landroid/support/v7/app/a;

    move-result-object v0

    .line 152
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x102002c

    if-ne v1, v2, :cond_28

    if-eqz v0, :cond_28

    invoke-virtual {v0}, Landroid/support/v7/app/a;->h()I

    move-result v0

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_28

    .line 154
    invoke-virtual {p0}, Landroid/support/v7/app/ah;->h()Z

    move-result v0

    goto :goto_7

    .line 156
    :cond_28
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public onMenuOpened(ILandroid/view/Menu;)Z
    .registers 4

    .prologue
    .line 437
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/bb;->onMenuOpened(ILandroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public onPanelClosed(ILandroid/view/Menu;)V
    .registers 3

    .prologue
    .line 448
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/bb;->onPanelClosed(ILandroid/view/Menu;)V

    .line 449
    return-void
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .registers 3
    .param p1    # Landroid/os/Bundle;
        .annotation build Landroid/support/a/z;
        .end annotation
    .end param

    .prologue
    .line 66
    invoke-super {p0, p1}, Landroid/support/v4/app/bb;->onPostCreate(Landroid/os/Bundle;)V

    .line 67
    invoke-virtual {p0}, Landroid/support/v7/app/ah;->i()Landroid/support/v7/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/aj;->d()V

    .line 68
    return-void
.end method

.method protected onPostResume()V
    .registers 2

    .prologue
    .line 141
    invoke-super {p0}, Landroid/support/v4/app/bb;->onPostResume()V

    .line 142
    invoke-virtual {p0}, Landroid/support/v7/app/ah;->i()Landroid/support/v7/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/aj;->f()V

    .line 143
    return-void
.end method

.method protected onStop()V
    .registers 2

    .prologue
    .line 135
    invoke-super {p0}, Landroid/support/v4/app/bb;->onStop()V

    .line 136
    invoke-virtual {p0}, Landroid/support/v7/app/ah;->i()Landroid/support/v7/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/aj;->e()V

    .line 137
    return-void
.end method

.method protected onTitleChanged(Ljava/lang/CharSequence;I)V
    .registers 4

    .prologue
    .line 167
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/bb;->onTitleChanged(Ljava/lang/CharSequence;I)V

    .line 168
    invoke-virtual {p0}, Landroid/support/v7/app/ah;->i()Landroid/support/v7/app/aj;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v7/app/aj;->a(Ljava/lang/CharSequence;)V

    .line 169
    return-void
.end method

.method public setContentView(I)V
    .registers 3
    .param p1    # I
        .annotation build Landroid/support/a/v;
        .end annotation
    .end param

    .prologue
    .line 109
    invoke-virtual {p0}, Landroid/support/v7/app/ah;->i()Landroid/support/v7/app/aj;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v7/app/aj;->a(I)V

    .line 110
    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .registers 3

    .prologue
    .line 114
    invoke-virtual {p0}, Landroid/support/v7/app/ah;->i()Landroid/support/v7/app/aj;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v7/app/aj;->a(Landroid/view/View;)V

    .line 115
    return-void
.end method

.method public setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .registers 4

    .prologue
    .line 119
    invoke-virtual {p0}, Landroid/support/v7/app/ah;->i()Landroid/support/v7/app/aj;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroid/support/v7/app/aj;->a(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 120
    return-void
.end method
