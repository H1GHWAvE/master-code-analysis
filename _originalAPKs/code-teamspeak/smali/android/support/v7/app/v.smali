.class final Landroid/support/v7/app/v;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field A:Landroid/widget/TextView;

.field B:Landroid/widget/TextView;

.field C:Landroid/view/View;

.field D:Landroid/widget/ListAdapter;

.field E:I

.field F:I

.field G:I

.field H:I

.field I:I

.field J:I

.field K:I

.field L:I

.field M:Landroid/os/Handler;

.field final N:Landroid/view/View$OnClickListener;

.field final a:Landroid/content/Context;

.field final b:Landroid/support/v7/app/bf;

.field final c:Landroid/view/Window;

.field d:Ljava/lang/CharSequence;

.field e:Ljava/lang/CharSequence;

.field f:Landroid/widget/ListView;

.field g:Landroid/view/View;

.field h:I

.field i:I

.field j:I

.field k:I

.field l:I

.field m:Z

.field n:Landroid/widget/Button;

.field o:Ljava/lang/CharSequence;

.field p:Landroid/os/Message;

.field q:Landroid/widget/Button;

.field r:Ljava/lang/CharSequence;

.field s:Landroid/os/Message;

.field t:Landroid/widget/Button;

.field u:Ljava/lang/CharSequence;

.field v:Landroid/os/Message;

.field w:Landroid/widget/ScrollView;

.field x:I

.field y:Landroid/graphics/drawable/Drawable;

.field z:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/support/v7/app/bf;Landroid/view/Window;)V
    .registers 8

    .prologue
    const/4 v3, 0x0

    .line 168
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    iput-boolean v3, p0, Landroid/support/v7/app/v;->m:Z

    .line 89
    iput v3, p0, Landroid/support/v7/app/v;->x:I

    .line 99
    const/4 v0, -0x1

    iput v0, p0, Landroid/support/v7/app/v;->E:I

    .line 108
    iput v3, p0, Landroid/support/v7/app/v;->L:I

    .line 112
    new-instance v0, Landroid/support/v7/app/w;

    invoke-direct {v0, p0}, Landroid/support/v7/app/w;-><init>(Landroid/support/v7/app/v;)V

    iput-object v0, p0, Landroid/support/v7/app/v;->N:Landroid/view/View$OnClickListener;

    .line 169
    iput-object p1, p0, Landroid/support/v7/app/v;->a:Landroid/content/Context;

    .line 170
    iput-object p2, p0, Landroid/support/v7/app/v;->b:Landroid/support/v7/app/bf;

    .line 171
    iput-object p3, p0, Landroid/support/v7/app/v;->c:Landroid/view/Window;

    .line 172
    new-instance v0, Landroid/support/v7/app/ad;

    invoke-direct {v0, p2}, Landroid/support/v7/app/ad;-><init>(Landroid/content/DialogInterface;)V

    iput-object v0, p0, Landroid/support/v7/app/v;->M:Landroid/os/Handler;

    .line 174
    const/4 v0, 0x0

    sget-object v1, Landroid/support/v7/a/n;->AlertDialog:[I

    sget v2, Landroid/support/v7/a/d;->alertDialogStyle:I

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 177
    sget v1, Landroid/support/v7/a/n;->AlertDialog_android_layout:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Landroid/support/v7/app/v;->F:I

    .line 178
    sget v1, Landroid/support/v7/a/n;->AlertDialog_buttonPanelSideLayout:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Landroid/support/v7/app/v;->G:I

    .line 180
    sget v1, Landroid/support/v7/a/n;->AlertDialog_listLayout:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Landroid/support/v7/app/v;->H:I

    .line 181
    sget v1, Landroid/support/v7/a/n;->AlertDialog_multiChoiceItemLayout:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Landroid/support/v7/app/v;->I:I

    .line 182
    sget v1, Landroid/support/v7/a/n;->AlertDialog_singleChoiceItemLayout:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Landroid/support/v7/app/v;->J:I

    .line 184
    sget v1, Landroid/support/v7/a/n;->AlertDialog_listItemLayout:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Landroid/support/v7/app/v;->K:I

    .line 186
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 187
    return-void
.end method

.method private static synthetic a(Landroid/support/v7/app/v;I)I
    .registers 2

    .prologue
    .line 57
    iput p1, p0, Landroid/support/v7/app/v;->E:I

    return p1
.end method

.method private static synthetic a(Landroid/support/v7/app/v;)Landroid/widget/Button;
    .registers 2

    .prologue
    .line 57
    iget-object v0, p0, Landroid/support/v7/app/v;->n:Landroid/widget/Button;

    return-object v0
.end method

.method private static synthetic a(Landroid/support/v7/app/v;Landroid/widget/ListAdapter;)Landroid/widget/ListAdapter;
    .registers 2

    .prologue
    .line 57
    iput-object p1, p0, Landroid/support/v7/app/v;->D:Landroid/widget/ListAdapter;

    return-object p1
.end method

.method private static synthetic a(Landroid/support/v7/app/v;Landroid/widget/ListView;)Landroid/widget/ListView;
    .registers 2

    .prologue
    .line 57
    iput-object p1, p0, Landroid/support/v7/app/v;->f:Landroid/widget/ListView;

    return-object p1
.end method

.method private a()V
    .registers 13

    .prologue
    const/high16 v11, 0x20000

    const/4 v10, -0x1

    const/4 v3, 0x1

    const/16 v9, 0x8

    const/4 v2, 0x0

    .line 213
    iget-object v0, p0, Landroid/support/v7/app/v;->b:Landroid/support/v7/app/bf;

    invoke-virtual {v0}, Landroid/support/v7/app/bf;->a()Z

    .line 1221
    iget v0, p0, Landroid/support/v7/app/v;->G:I

    if-eqz v0, :cond_18e

    .line 1224
    iget v0, p0, Landroid/support/v7/app/v;->L:I

    if-ne v0, v3, :cond_18e

    .line 1225
    iget v0, p0, Landroid/support/v7/app/v;->G:I

    .line 216
    :goto_16
    iget-object v1, p0, Landroid/support/v7/app/v;->b:Landroid/support/v7/app/bf;

    invoke-virtual {v1, v0}, Landroid/support/v7/app/bf;->setContentView(I)V

    .line 1407
    iget-object v0, p0, Landroid/support/v7/app/v;->c:Landroid/view/Window;

    sget v1, Landroid/support/v7/a/i;->contentPanel:I

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 1522
    iget-object v1, p0, Landroid/support/v7/app/v;->c:Landroid/view/Window;

    sget v4, Landroid/support/v7/a/i;->scrollView:I

    invoke-virtual {v1, v4}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ScrollView;

    iput-object v1, p0, Landroid/support/v7/app/v;->w:Landroid/widget/ScrollView;

    .line 1523
    iget-object v1, p0, Landroid/support/v7/app/v;->w:Landroid/widget/ScrollView;

    invoke-virtual {v1, v2}, Landroid/widget/ScrollView;->setFocusable(Z)V

    .line 1526
    iget-object v1, p0, Landroid/support/v7/app/v;->c:Landroid/view/Window;

    const v4, 0x102000b

    invoke-virtual {v1, v4}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Landroid/support/v7/app/v;->B:Landroid/widget/TextView;

    .line 1527
    iget-object v1, p0, Landroid/support/v7/app/v;->B:Landroid/widget/TextView;

    if-eqz v1, :cond_52

    .line 1531
    iget-object v1, p0, Landroid/support/v7/app/v;->e:Ljava/lang/CharSequence;

    if-eqz v1, :cond_192

    .line 1532
    iget-object v0, p0, Landroid/support/v7/app/v;->B:Landroid/widget/TextView;

    iget-object v1, p0, Landroid/support/v7/app/v;->e:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1554
    :cond_52
    :goto_52
    iget-object v0, p0, Landroid/support/v7/app/v;->c:Landroid/view/Window;

    const v1, 0x1020019

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Landroid/support/v7/app/v;->n:Landroid/widget/Button;

    .line 1555
    iget-object v0, p0, Landroid/support/v7/app/v;->n:Landroid/widget/Button;

    iget-object v1, p0, Landroid/support/v7/app/v;->N:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1557
    iget-object v0, p0, Landroid/support/v7/app/v;->o:Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1c4

    .line 1558
    iget-object v0, p0, Landroid/support/v7/app/v;->n:Landroid/widget/Button;

    invoke-virtual {v0, v9}, Landroid/widget/Button;->setVisibility(I)V

    move v1, v2

    .line 1565
    :goto_74
    iget-object v0, p0, Landroid/support/v7/app/v;->c:Landroid/view/Window;

    const v4, 0x102001a

    invoke-virtual {v0, v4}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Landroid/support/v7/app/v;->q:Landroid/widget/Button;

    .line 1566
    iget-object v0, p0, Landroid/support/v7/app/v;->q:Landroid/widget/Button;

    iget-object v4, p0, Landroid/support/v7/app/v;->N:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1568
    iget-object v0, p0, Landroid/support/v7/app/v;->r:Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1d3

    .line 1569
    iget-object v0, p0, Landroid/support/v7/app/v;->q:Landroid/widget/Button;

    invoke-virtual {v0, v9}, Landroid/widget/Button;->setVisibility(I)V

    .line 1577
    :goto_95
    iget-object v0, p0, Landroid/support/v7/app/v;->c:Landroid/view/Window;

    const v4, 0x102001b

    invoke-virtual {v0, v4}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Landroid/support/v7/app/v;->t:Landroid/widget/Button;

    .line 1578
    iget-object v0, p0, Landroid/support/v7/app/v;->t:Landroid/widget/Button;

    iget-object v4, p0, Landroid/support/v7/app/v;->N:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1580
    iget-object v0, p0, Landroid/support/v7/app/v;->u:Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1e3

    .line 1581
    iget-object v0, p0, Landroid/support/v7/app/v;->t:Landroid/widget/Button;

    invoke-virtual {v0, v9}, Landroid/widget/Button;->setVisibility(I)V

    .line 1589
    :goto_b6
    iget-object v0, p0, Landroid/support/v7/app/v;->a:Landroid/content/Context;

    .line 2163
    new-instance v4, Landroid/util/TypedValue;

    invoke-direct {v4}, Landroid/util/TypedValue;-><init>()V

    .line 2164
    invoke-virtual {v0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    sget v5, Landroid/support/v7/a/d;->alertDialogCenterButtons:I

    invoke-virtual {v0, v5, v4, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 2165
    iget v0, v4, Landroid/util/TypedValue;->data:I

    if-eqz v0, :cond_1f3

    move v0, v3

    .line 1589
    :goto_cb
    if-eqz v0, :cond_d4

    .line 1594
    if-ne v1, v3, :cond_1f6

    .line 1595
    iget-object v0, p0, Landroid/support/v7/app/v;->n:Landroid/widget/Button;

    invoke-static {v0}, Landroid/support/v7/app/v;->a(Landroid/widget/Button;)V

    .line 1603
    :cond_d4
    :goto_d4
    if-eqz v1, :cond_20a

    move v4, v3

    .line 1411
    :goto_d7
    iget-object v0, p0, Landroid/support/v7/app/v;->c:Landroid/view/Window;

    sget v1, Landroid/support/v7/a/i;->topPanel:I

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 1412
    iget-object v1, p0, Landroid/support/v7/app/v;->a:Landroid/content/Context;

    const/4 v5, 0x0

    sget-object v6, Landroid/support/v7/a/n;->AlertDialog:[I

    sget v7, Landroid/support/v7/a/d;->alertDialogStyle:I

    invoke-static {v1, v5, v6, v7}, Landroid/support/v7/internal/widget/ax;->a(Landroid/content/Context;Landroid/util/AttributeSet;[II)Landroid/support/v7/internal/widget/ax;

    move-result-object v5

    .line 2474
    iget-object v1, p0, Landroid/support/v7/app/v;->C:Landroid/view/View;

    if-eqz v1, :cond_20d

    .line 2476
    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    const/4 v6, -0x2

    invoke-direct {v1, v10, v6}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 2479
    iget-object v6, p0, Landroid/support/v7/app/v;->C:Landroid/view/View;

    invoke-virtual {v0, v6, v2, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 2482
    iget-object v0, p0, Landroid/support/v7/app/v;->c:Landroid/view/Window;

    sget v1, Landroid/support/v7/a/i;->title_template:I

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 2483
    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    .line 1416
    :goto_106
    iget-object v0, p0, Landroid/support/v7/app/v;->c:Landroid/view/Window;

    sget v1, Landroid/support/v7/a/i;->buttonPanel:I

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1417
    if-nez v4, :cond_120

    .line 1418
    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    .line 1419
    iget-object v0, p0, Landroid/support/v7/app/v;->c:Landroid/view/Window;

    sget v1, Landroid/support/v7/a/i;->textSpacerNoButtons:I

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1420
    if-eqz v0, :cond_120

    .line 1421
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1425
    :cond_120
    iget-object v0, p0, Landroid/support/v7/app/v;->c:Landroid/view/Window;

    sget v1, Landroid/support/v7/a/i;->customPanel:I

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 1427
    iget-object v1, p0, Landroid/support/v7/app/v;->g:Landroid/view/View;

    if-eqz v1, :cond_28d

    .line 1428
    iget-object v1, p0, Landroid/support/v7/app/v;->g:Landroid/view/View;

    move-object v4, v1

    .line 1436
    :goto_131
    if-eqz v4, :cond_134

    move v2, v3

    .line 1437
    :cond_134
    if-eqz v2, :cond_13c

    invoke-static {v4}, Landroid/support/v7/app/v;->a(Landroid/view/View;)Z

    move-result v1

    if-nez v1, :cond_141

    .line 1438
    :cond_13c
    iget-object v1, p0, Landroid/support/v7/app/v;->c:Landroid/view/Window;

    invoke-virtual {v1, v11, v11}, Landroid/view/Window;->setFlags(II)V

    .line 1442
    :cond_141
    if-eqz v2, :cond_2a4

    .line 1443
    iget-object v1, p0, Landroid/support/v7/app/v;->c:Landroid/view/Window;

    sget v2, Landroid/support/v7/a/i;->custom:I

    invoke-virtual {v1, v2}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout;

    .line 1444
    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v2, v10, v10}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v4, v2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1446
    iget-boolean v2, p0, Landroid/support/v7/app/v;->m:Z

    if-eqz v2, :cond_164

    .line 1447
    iget v2, p0, Landroid/support/v7/app/v;->i:I

    iget v4, p0, Landroid/support/v7/app/v;->j:I

    iget v6, p0, Landroid/support/v7/app/v;->k:I

    iget v7, p0, Landroid/support/v7/app/v;->l:I

    invoke-virtual {v1, v2, v4, v6, v7}, Landroid/widget/FrameLayout;->setPadding(IIII)V

    .line 1451
    :cond_164
    iget-object v1, p0, Landroid/support/v7/app/v;->f:Landroid/widget/ListView;

    if-eqz v1, :cond_171

    .line 1452
    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, 0x0

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 1458
    :cond_171
    :goto_171
    iget-object v0, p0, Landroid/support/v7/app/v;->f:Landroid/widget/ListView;

    .line 1459
    if-eqz v0, :cond_188

    iget-object v1, p0, Landroid/support/v7/app/v;->D:Landroid/widget/ListAdapter;

    if-eqz v1, :cond_188

    .line 1460
    iget-object v1, p0, Landroid/support/v7/app/v;->D:Landroid/widget/ListAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1461
    iget v1, p0, Landroid/support/v7/app/v;->E:I

    .line 1462
    if-ltz v1, :cond_188

    .line 1463
    invoke-virtual {v0, v1, v3}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 1464
    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setSelection(I)V

    .line 3183
    :cond_188
    iget-object v0, v5, Landroid/support/v7/internal/widget/ax;->a:Landroid/content/res/TypedArray;

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 218
    return-void

    .line 1227
    :cond_18e
    iget v0, p0, Landroid/support/v7/app/v;->F:I

    goto/16 :goto_16

    .line 1534
    :cond_192
    iget-object v1, p0, Landroid/support/v7/app/v;->B:Landroid/widget/TextView;

    invoke-virtual {v1, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1535
    iget-object v1, p0, Landroid/support/v7/app/v;->w:Landroid/widget/ScrollView;

    iget-object v4, p0, Landroid/support/v7/app/v;->B:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/ScrollView;->removeView(Landroid/view/View;)V

    .line 1537
    iget-object v1, p0, Landroid/support/v7/app/v;->f:Landroid/widget/ListView;

    if-eqz v1, :cond_1bf

    .line 1538
    iget-object v0, p0, Landroid/support/v7/app/v;->w:Landroid/widget/ScrollView;

    invoke-virtual {v0}, Landroid/widget/ScrollView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 1539
    iget-object v1, p0, Landroid/support/v7/app/v;->w:Landroid/widget/ScrollView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v1

    .line 1540
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeViewAt(I)V

    .line 1541
    iget-object v4, p0, Landroid/support/v7/app/v;->f:Landroid/widget/ListView;

    new-instance v5, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v5, v10, v10}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v4, v1, v5}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_52

    .line 1544
    :cond_1bf
    invoke-virtual {v0, v9}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto/16 :goto_52

    .line 1560
    :cond_1c4
    iget-object v0, p0, Landroid/support/v7/app/v;->n:Landroid/widget/Button;

    iget-object v1, p0, Landroid/support/v7/app/v;->o:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 1561
    iget-object v0, p0, Landroid/support/v7/app/v;->n:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    move v1, v3

    .line 1562
    goto/16 :goto_74

    .line 1571
    :cond_1d3
    iget-object v0, p0, Landroid/support/v7/app/v;->q:Landroid/widget/Button;

    iget-object v4, p0, Landroid/support/v7/app/v;->r:Ljava/lang/CharSequence;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 1572
    iget-object v0, p0, Landroid/support/v7/app/v;->q:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 1574
    or-int/lit8 v1, v1, 0x2

    goto/16 :goto_95

    .line 1583
    :cond_1e3
    iget-object v0, p0, Landroid/support/v7/app/v;->t:Landroid/widget/Button;

    iget-object v4, p0, Landroid/support/v7/app/v;->u:Ljava/lang/CharSequence;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 1584
    iget-object v0, p0, Landroid/support/v7/app/v;->t:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 1586
    or-int/lit8 v1, v1, 0x4

    goto/16 :goto_b6

    :cond_1f3
    move v0, v2

    .line 2165
    goto/16 :goto_cb

    .line 1596
    :cond_1f6
    const/4 v0, 0x2

    if-ne v1, v0, :cond_200

    .line 1597
    iget-object v0, p0, Landroid/support/v7/app/v;->q:Landroid/widget/Button;

    invoke-static {v0}, Landroid/support/v7/app/v;->a(Landroid/widget/Button;)V

    goto/16 :goto_d4

    .line 1598
    :cond_200
    const/4 v0, 0x4

    if-ne v1, v0, :cond_d4

    .line 1599
    iget-object v0, p0, Landroid/support/v7/app/v;->t:Landroid/widget/Button;

    invoke-static {v0}, Landroid/support/v7/app/v;->a(Landroid/widget/Button;)V

    goto/16 :goto_d4

    :cond_20a
    move v4, v2

    .line 1603
    goto/16 :goto_d7

    .line 2485
    :cond_20d
    iget-object v1, p0, Landroid/support/v7/app/v;->c:Landroid/view/Window;

    const v6, 0x1020006

    invoke-virtual {v1, v6}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Landroid/support/v7/app/v;->z:Landroid/widget/ImageView;

    .line 2487
    iget-object v1, p0, Landroid/support/v7/app/v;->d:Ljava/lang/CharSequence;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_245

    move v1, v3

    .line 2488
    :goto_223
    if-eqz v1, :cond_278

    .line 2490
    iget-object v0, p0, Landroid/support/v7/app/v;->c:Landroid/view/Window;

    sget v1, Landroid/support/v7/a/i;->alertTitle:I

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Landroid/support/v7/app/v;->A:Landroid/widget/TextView;

    .line 2491
    iget-object v0, p0, Landroid/support/v7/app/v;->A:Landroid/widget/TextView;

    iget-object v1, p0, Landroid/support/v7/app/v;->d:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2496
    iget v0, p0, Landroid/support/v7/app/v;->x:I

    if-eqz v0, :cond_247

    .line 2497
    iget-object v0, p0, Landroid/support/v7/app/v;->z:Landroid/widget/ImageView;

    iget v1, p0, Landroid/support/v7/app/v;->x:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_106

    :cond_245
    move v1, v2

    .line 2487
    goto :goto_223

    .line 2498
    :cond_247
    iget-object v0, p0, Landroid/support/v7/app/v;->y:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_254

    .line 2499
    iget-object v0, p0, Landroid/support/v7/app/v;->z:Landroid/widget/ImageView;

    iget-object v1, p0, Landroid/support/v7/app/v;->y:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_106

    .line 2503
    :cond_254
    iget-object v0, p0, Landroid/support/v7/app/v;->A:Landroid/widget/TextView;

    iget-object v1, p0, Landroid/support/v7/app/v;->z:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getPaddingLeft()I

    move-result v1

    iget-object v6, p0, Landroid/support/v7/app/v;->z:Landroid/widget/ImageView;

    invoke-virtual {v6}, Landroid/widget/ImageView;->getPaddingTop()I

    move-result v6

    iget-object v7, p0, Landroid/support/v7/app/v;->z:Landroid/widget/ImageView;

    invoke-virtual {v7}, Landroid/widget/ImageView;->getPaddingRight()I

    move-result v7

    iget-object v8, p0, Landroid/support/v7/app/v;->z:Landroid/widget/ImageView;

    invoke-virtual {v8}, Landroid/widget/ImageView;->getPaddingBottom()I

    move-result v8

    invoke-virtual {v0, v1, v6, v7, v8}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 2507
    iget-object v0, p0, Landroid/support/v7/app/v;->z:Landroid/widget/ImageView;

    invoke-virtual {v0, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_106

    .line 2511
    :cond_278
    iget-object v1, p0, Landroid/support/v7/app/v;->c:Landroid/view/Window;

    sget v6, Landroid/support/v7/a/i;->title_template:I

    invoke-virtual {v1, v6}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 2512
    invoke-virtual {v1, v9}, Landroid/view/View;->setVisibility(I)V

    .line 2513
    iget-object v1, p0, Landroid/support/v7/app/v;->z:Landroid/widget/ImageView;

    invoke-virtual {v1, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2514
    invoke-virtual {v0, v9}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto/16 :goto_106

    .line 1429
    :cond_28d
    iget v1, p0, Landroid/support/v7/app/v;->h:I

    if-eqz v1, :cond_2a0

    .line 1430
    iget-object v1, p0, Landroid/support/v7/app/v;->a:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 1431
    iget v4, p0, Landroid/support/v7/app/v;->h:I

    invoke-virtual {v1, v4, v0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    move-object v4, v1

    .line 1432
    goto/16 :goto_131

    .line 1433
    :cond_2a0
    const/4 v1, 0x0

    move-object v4, v1

    goto/16 :goto_131

    .line 1455
    :cond_2a4
    invoke-virtual {v0, v9}, Landroid/widget/FrameLayout;->setVisibility(I)V

    goto/16 :goto_171
.end method

.method static a(Landroid/widget/Button;)V
    .registers 3

    .prologue
    .line 607
    invoke-virtual {p0}, Landroid/widget/Button;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 608
    const/4 v1, 0x1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 609
    const/high16 v1, 0x3f000000    # 0.5f

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 610
    invoke-virtual {p0, v0}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 611
    return-void
.end method

.method private static a(Landroid/content/Context;)Z
    .registers 5

    .prologue
    const/4 v0, 0x1

    .line 163
    new-instance v1, Landroid/util/TypedValue;

    invoke-direct {v1}, Landroid/util/TypedValue;-><init>()V

    .line 164
    invoke-virtual {p0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v2

    sget v3, Landroid/support/v7/a/d;->alertDialogCenterButtons:I

    invoke-virtual {v2, v3, v1, v0}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 165
    iget v1, v1, Landroid/util/TypedValue;->data:I

    if-eqz v1, :cond_14

    :goto_13
    return v0

    :cond_14
    const/4 v0, 0x0

    goto :goto_13
.end method

.method private a(Landroid/view/KeyEvent;)Z
    .registers 3

    .prologue
    .line 398
    iget-object v0, p0, Landroid/support/v7/app/v;->w:Landroid/widget/ScrollView;

    if-eqz v0, :cond_e

    iget-object v0, p0, Landroid/support/v7/app/v;->w:Landroid/widget/ScrollView;

    invoke-virtual {v0, p1}, Landroid/widget/ScrollView;->executeKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method static a(Landroid/view/View;)Z
    .registers 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 190
    invoke-virtual {p0}, Landroid/view/View;->onCheckIsTextEditor()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 208
    :goto_8
    return v0

    .line 194
    :cond_9
    instance-of v2, p0, Landroid/view/ViewGroup;

    if-nez v2, :cond_f

    move v0, v1

    .line 195
    goto :goto_8

    .line 198
    :cond_f
    check-cast p0, Landroid/view/ViewGroup;

    .line 199
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    .line 200
    :cond_15
    if-lez v2, :cond_24

    .line 201
    add-int/lit8 v2, v2, -0x1

    .line 202
    invoke-virtual {p0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 203
    invoke-static {v3}, Landroid/support/v7/app/v;->a(Landroid/view/View;)Z

    move-result v3

    if-eqz v3, :cond_15

    goto :goto_8

    :cond_24
    move v0, v1

    .line 208
    goto :goto_8
.end method

.method private a(Landroid/view/ViewGroup;)Z
    .registers 9

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/16 v6, 0x8

    .line 472
    .line 474
    iget-object v0, p0, Landroid/support/v7/app/v;->C:Landroid/view/View;

    if-eqz v0, :cond_21

    .line 476
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    const/4 v3, -0x1

    const/4 v4, -0x2

    invoke-direct {v0, v3, v4}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 479
    iget-object v3, p0, Landroid/support/v7/app/v;->C:Landroid/view/View;

    invoke-virtual {p1, v3, v2, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 482
    iget-object v0, p0, Landroid/support/v7/app/v;->c:Landroid/view/Window;

    sget v2, Landroid/support/v7/a/i;->title_template:I

    invoke-virtual {v0, v2}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 483
    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    move v0, v1

    .line 518
    :goto_20
    return v0

    .line 485
    :cond_21
    iget-object v0, p0, Landroid/support/v7/app/v;->c:Landroid/view/Window;

    const v3, 0x1020006

    invoke-virtual {v0, v3}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Landroid/support/v7/app/v;->z:Landroid/widget/ImageView;

    .line 487
    iget-object v0, p0, Landroid/support/v7/app/v;->d:Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_59

    move v0, v1

    .line 488
    :goto_37
    if-eqz v0, :cond_8c

    .line 490
    iget-object v0, p0, Landroid/support/v7/app/v;->c:Landroid/view/Window;

    sget v2, Landroid/support/v7/a/i;->alertTitle:I

    invoke-virtual {v0, v2}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Landroid/support/v7/app/v;->A:Landroid/widget/TextView;

    .line 491
    iget-object v0, p0, Landroid/support/v7/app/v;->A:Landroid/widget/TextView;

    iget-object v2, p0, Landroid/support/v7/app/v;->d:Ljava/lang/CharSequence;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 496
    iget v0, p0, Landroid/support/v7/app/v;->x:I

    if-eqz v0, :cond_5b

    .line 497
    iget-object v0, p0, Landroid/support/v7/app/v;->z:Landroid/widget/ImageView;

    iget v2, p0, Landroid/support/v7/app/v;->x:I

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    move v0, v1

    goto :goto_20

    :cond_59
    move v0, v2

    .line 487
    goto :goto_37

    .line 498
    :cond_5b
    iget-object v0, p0, Landroid/support/v7/app/v;->y:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_68

    .line 499
    iget-object v0, p0, Landroid/support/v7/app/v;->z:Landroid/widget/ImageView;

    iget-object v2, p0, Landroid/support/v7/app/v;->y:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    move v0, v1

    goto :goto_20

    .line 503
    :cond_68
    iget-object v0, p0, Landroid/support/v7/app/v;->A:Landroid/widget/TextView;

    iget-object v2, p0, Landroid/support/v7/app/v;->z:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getPaddingLeft()I

    move-result v2

    iget-object v3, p0, Landroid/support/v7/app/v;->z:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getPaddingTop()I

    move-result v3

    iget-object v4, p0, Landroid/support/v7/app/v;->z:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getPaddingRight()I

    move-result v4

    iget-object v5, p0, Landroid/support/v7/app/v;->z:Landroid/widget/ImageView;

    invoke-virtual {v5}, Landroid/widget/ImageView;->getPaddingBottom()I

    move-result v5

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 507
    iget-object v0, p0, Landroid/support/v7/app/v;->z:Landroid/widget/ImageView;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    move v0, v1

    goto :goto_20

    .line 511
    :cond_8c
    iget-object v0, p0, Landroid/support/v7/app/v;->c:Landroid/view/Window;

    sget v1, Landroid/support/v7/a/i;->title_template:I

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 512
    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 513
    iget-object v0, p0, Landroid/support/v7/app/v;->z:Landroid/widget/ImageView;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 514
    invoke-virtual {p1, v6}, Landroid/view/ViewGroup;->setVisibility(I)V

    move v0, v2

    .line 515
    goto :goto_20
.end method

.method private b()I
    .registers 3

    .prologue
    .line 221
    iget v0, p0, Landroid/support/v7/app/v;->G:I

    if-nez v0, :cond_7

    .line 222
    iget v0, p0, Landroid/support/v7/app/v;->F:I

    .line 227
    :goto_6
    return v0

    .line 224
    :cond_7
    iget v0, p0, Landroid/support/v7/app/v;->L:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_f

    .line 225
    iget v0, p0, Landroid/support/v7/app/v;->G:I

    goto :goto_6

    .line 227
    :cond_f
    iget v0, p0, Landroid/support/v7/app/v;->F:I

    goto :goto_6
.end method

.method private static synthetic b(Landroid/support/v7/app/v;)Landroid/os/Message;
    .registers 2

    .prologue
    .line 57
    iget-object v0, p0, Landroid/support/v7/app/v;->p:Landroid/os/Message;

    return-object v0
.end method

.method private b(I)V
    .registers 3

    .prologue
    .line 255
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v7/app/v;->g:Landroid/view/View;

    .line 256
    iput p1, p0, Landroid/support/v7/app/v;->h:I

    .line 257
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v7/app/v;->m:Z

    .line 258
    return-void
.end method

.method private b(Landroid/view/ViewGroup;)V
    .registers 7

    .prologue
    const/16 v2, 0x8

    const/4 v4, -0x1

    .line 522
    iget-object v0, p0, Landroid/support/v7/app/v;->c:Landroid/view/Window;

    sget v1, Landroid/support/v7/a/i;->scrollView:I

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    iput-object v0, p0, Landroid/support/v7/app/v;->w:Landroid/widget/ScrollView;

    .line 523
    iget-object v0, p0, Landroid/support/v7/app/v;->w:Landroid/widget/ScrollView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->setFocusable(Z)V

    .line 526
    iget-object v0, p0, Landroid/support/v7/app/v;->c:Landroid/view/Window;

    const v1, 0x102000b

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Landroid/support/v7/app/v;->B:Landroid/widget/TextView;

    .line 527
    iget-object v0, p0, Landroid/support/v7/app/v;->B:Landroid/widget/TextView;

    if-nez v0, :cond_27

    .line 547
    :goto_26
    return-void

    .line 531
    :cond_27
    iget-object v0, p0, Landroid/support/v7/app/v;->e:Ljava/lang/CharSequence;

    if-eqz v0, :cond_33

    .line 532
    iget-object v0, p0, Landroid/support/v7/app/v;->B:Landroid/widget/TextView;

    iget-object v1, p0, Landroid/support/v7/app/v;->e:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_26

    .line 534
    :cond_33
    iget-object v0, p0, Landroid/support/v7/app/v;->B:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 535
    iget-object v0, p0, Landroid/support/v7/app/v;->w:Landroid/widget/ScrollView;

    iget-object v1, p0, Landroid/support/v7/app/v;->B:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->removeView(Landroid/view/View;)V

    .line 537
    iget-object v0, p0, Landroid/support/v7/app/v;->f:Landroid/widget/ListView;

    if-eqz v0, :cond_5f

    .line 538
    iget-object v0, p0, Landroid/support/v7/app/v;->w:Landroid/widget/ScrollView;

    invoke-virtual {v0}, Landroid/widget/ScrollView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 539
    iget-object v1, p0, Landroid/support/v7/app/v;->w:Landroid/widget/ScrollView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v1

    .line 540
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeViewAt(I)V

    .line 541
    iget-object v2, p0, Landroid/support/v7/app/v;->f:Landroid/widget/ListView;

    new-instance v3, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v3, v4, v4}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v2, v1, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    goto :goto_26

    .line 544
    :cond_5f
    invoke-virtual {p1, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_26
.end method

.method private b(Landroid/view/KeyEvent;)Z
    .registers 3

    .prologue
    .line 403
    iget-object v0, p0, Landroid/support/v7/app/v;->w:Landroid/widget/ScrollView;

    if-eqz v0, :cond_e

    iget-object v0, p0, Landroid/support/v7/app/v;->w:Landroid/widget/ScrollView;

    invoke-virtual {v0, p1}, Landroid/widget/ScrollView;->executeKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method private static synthetic c(Landroid/support/v7/app/v;)Landroid/widget/Button;
    .registers 2

    .prologue
    .line 57
    iget-object v0, p0, Landroid/support/v7/app/v;->q:Landroid/widget/Button;

    return-object v0
.end method

.method private c()Landroid/widget/ListView;
    .registers 2

    .prologue
    .line 380
    iget-object v0, p0, Landroid/support/v7/app/v;->f:Landroid/widget/ListView;

    return-object v0
.end method

.method private c(I)V
    .registers 2

    .prologue
    .line 287
    iput p1, p0, Landroid/support/v7/app/v;->L:I

    .line 288
    return-void
.end method

.method private c(Landroid/view/View;)V
    .registers 2

    .prologue
    .line 241
    iput-object p1, p0, Landroid/support/v7/app/v;->C:Landroid/view/View;

    .line 242
    return-void
.end method

.method private d(I)I
    .registers 5

    .prologue
    .line 374
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 375
    iget-object v1, p0, Landroid/support/v7/app/v;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, p1, v0, v2}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 376
    iget v0, v0, Landroid/util/TypedValue;->resourceId:I

    return v0
.end method

.method private static synthetic d(Landroid/support/v7/app/v;)Landroid/os/Message;
    .registers 2

    .prologue
    .line 57
    iget-object v0, p0, Landroid/support/v7/app/v;->s:Landroid/os/Message;

    return-object v0
.end method

.method private d()V
    .registers 13

    .prologue
    const/high16 v11, 0x20000

    const/4 v10, -0x1

    const/4 v3, 0x1

    const/16 v9, 0x8

    const/4 v2, 0x0

    .line 407
    iget-object v0, p0, Landroid/support/v7/app/v;->c:Landroid/view/Window;

    sget v1, Landroid/support/v7/a/i;->contentPanel:I

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 3522
    iget-object v1, p0, Landroid/support/v7/app/v;->c:Landroid/view/Window;

    sget v4, Landroid/support/v7/a/i;->scrollView:I

    invoke-virtual {v1, v4}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ScrollView;

    iput-object v1, p0, Landroid/support/v7/app/v;->w:Landroid/widget/ScrollView;

    .line 3523
    iget-object v1, p0, Landroid/support/v7/app/v;->w:Landroid/widget/ScrollView;

    invoke-virtual {v1, v2}, Landroid/widget/ScrollView;->setFocusable(Z)V

    .line 3526
    iget-object v1, p0, Landroid/support/v7/app/v;->c:Landroid/view/Window;

    const v4, 0x102000b

    invoke-virtual {v1, v4}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Landroid/support/v7/app/v;->B:Landroid/widget/TextView;

    .line 3527
    iget-object v1, p0, Landroid/support/v7/app/v;->B:Landroid/widget/TextView;

    if-eqz v1, :cond_3e

    .line 3531
    iget-object v1, p0, Landroid/support/v7/app/v;->e:Ljava/lang/CharSequence;

    if-eqz v1, :cond_17a

    .line 3532
    iget-object v0, p0, Landroid/support/v7/app/v;->B:Landroid/widget/TextView;

    iget-object v1, p0, Landroid/support/v7/app/v;->e:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 3554
    :cond_3e
    :goto_3e
    iget-object v0, p0, Landroid/support/v7/app/v;->c:Landroid/view/Window;

    const v1, 0x1020019

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Landroid/support/v7/app/v;->n:Landroid/widget/Button;

    .line 3555
    iget-object v0, p0, Landroid/support/v7/app/v;->n:Landroid/widget/Button;

    iget-object v1, p0, Landroid/support/v7/app/v;->N:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 3557
    iget-object v0, p0, Landroid/support/v7/app/v;->o:Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1ac

    .line 3558
    iget-object v0, p0, Landroid/support/v7/app/v;->n:Landroid/widget/Button;

    invoke-virtual {v0, v9}, Landroid/widget/Button;->setVisibility(I)V

    move v1, v2

    .line 3565
    :goto_60
    iget-object v0, p0, Landroid/support/v7/app/v;->c:Landroid/view/Window;

    const v4, 0x102001a

    invoke-virtual {v0, v4}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Landroid/support/v7/app/v;->q:Landroid/widget/Button;

    .line 3566
    iget-object v0, p0, Landroid/support/v7/app/v;->q:Landroid/widget/Button;

    iget-object v4, p0, Landroid/support/v7/app/v;->N:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 3568
    iget-object v0, p0, Landroid/support/v7/app/v;->r:Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1bb

    .line 3569
    iget-object v0, p0, Landroid/support/v7/app/v;->q:Landroid/widget/Button;

    invoke-virtual {v0, v9}, Landroid/widget/Button;->setVisibility(I)V

    .line 3577
    :goto_81
    iget-object v0, p0, Landroid/support/v7/app/v;->c:Landroid/view/Window;

    const v4, 0x102001b

    invoke-virtual {v0, v4}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Landroid/support/v7/app/v;->t:Landroid/widget/Button;

    .line 3578
    iget-object v0, p0, Landroid/support/v7/app/v;->t:Landroid/widget/Button;

    iget-object v4, p0, Landroid/support/v7/app/v;->N:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 3580
    iget-object v0, p0, Landroid/support/v7/app/v;->u:Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1cb

    .line 3581
    iget-object v0, p0, Landroid/support/v7/app/v;->t:Landroid/widget/Button;

    invoke-virtual {v0, v9}, Landroid/widget/Button;->setVisibility(I)V

    .line 3589
    :goto_a2
    iget-object v0, p0, Landroid/support/v7/app/v;->a:Landroid/content/Context;

    .line 4163
    new-instance v4, Landroid/util/TypedValue;

    invoke-direct {v4}, Landroid/util/TypedValue;-><init>()V

    .line 4164
    invoke-virtual {v0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    sget v5, Landroid/support/v7/a/d;->alertDialogCenterButtons:I

    invoke-virtual {v0, v5, v4, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 4165
    iget v0, v4, Landroid/util/TypedValue;->data:I

    if-eqz v0, :cond_1db

    move v0, v3

    .line 3589
    :goto_b7
    if-eqz v0, :cond_c0

    .line 3594
    if-ne v1, v3, :cond_1de

    .line 3595
    iget-object v0, p0, Landroid/support/v7/app/v;->n:Landroid/widget/Button;

    invoke-static {v0}, Landroid/support/v7/app/v;->a(Landroid/widget/Button;)V

    .line 3603
    :cond_c0
    :goto_c0
    if-eqz v1, :cond_1f2

    move v4, v3

    .line 411
    :goto_c3
    iget-object v0, p0, Landroid/support/v7/app/v;->c:Landroid/view/Window;

    sget v1, Landroid/support/v7/a/i;->topPanel:I

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 412
    iget-object v1, p0, Landroid/support/v7/app/v;->a:Landroid/content/Context;

    const/4 v5, 0x0

    sget-object v6, Landroid/support/v7/a/n;->AlertDialog:[I

    sget v7, Landroid/support/v7/a/d;->alertDialogStyle:I

    invoke-static {v1, v5, v6, v7}, Landroid/support/v7/internal/widget/ax;->a(Landroid/content/Context;Landroid/util/AttributeSet;[II)Landroid/support/v7/internal/widget/ax;

    move-result-object v5

    .line 4474
    iget-object v1, p0, Landroid/support/v7/app/v;->C:Landroid/view/View;

    if-eqz v1, :cond_1f5

    .line 4476
    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    const/4 v6, -0x2

    invoke-direct {v1, v10, v6}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 4479
    iget-object v6, p0, Landroid/support/v7/app/v;->C:Landroid/view/View;

    invoke-virtual {v0, v6, v2, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 4482
    iget-object v0, p0, Landroid/support/v7/app/v;->c:Landroid/view/Window;

    sget v1, Landroid/support/v7/a/i;->title_template:I

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 4483
    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    .line 416
    :goto_f2
    iget-object v0, p0, Landroid/support/v7/app/v;->c:Landroid/view/Window;

    sget v1, Landroid/support/v7/a/i;->buttonPanel:I

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 417
    if-nez v4, :cond_10c

    .line 418
    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    .line 419
    iget-object v0, p0, Landroid/support/v7/app/v;->c:Landroid/view/Window;

    sget v1, Landroid/support/v7/a/i;->textSpacerNoButtons:I

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 420
    if-eqz v0, :cond_10c

    .line 421
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 425
    :cond_10c
    iget-object v0, p0, Landroid/support/v7/app/v;->c:Landroid/view/Window;

    sget v1, Landroid/support/v7/a/i;->customPanel:I

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 427
    iget-object v1, p0, Landroid/support/v7/app/v;->g:Landroid/view/View;

    if-eqz v1, :cond_275

    .line 428
    iget-object v1, p0, Landroid/support/v7/app/v;->g:Landroid/view/View;

    move-object v4, v1

    .line 436
    :goto_11d
    if-eqz v4, :cond_120

    move v2, v3

    .line 437
    :cond_120
    if-eqz v2, :cond_128

    invoke-static {v4}, Landroid/support/v7/app/v;->a(Landroid/view/View;)Z

    move-result v1

    if-nez v1, :cond_12d

    .line 438
    :cond_128
    iget-object v1, p0, Landroid/support/v7/app/v;->c:Landroid/view/Window;

    invoke-virtual {v1, v11, v11}, Landroid/view/Window;->setFlags(II)V

    .line 442
    :cond_12d
    if-eqz v2, :cond_28c

    .line 443
    iget-object v1, p0, Landroid/support/v7/app/v;->c:Landroid/view/Window;

    sget v2, Landroid/support/v7/a/i;->custom:I

    invoke-virtual {v1, v2}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout;

    .line 444
    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v2, v10, v10}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v4, v2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 446
    iget-boolean v2, p0, Landroid/support/v7/app/v;->m:Z

    if-eqz v2, :cond_150

    .line 447
    iget v2, p0, Landroid/support/v7/app/v;->i:I

    iget v4, p0, Landroid/support/v7/app/v;->j:I

    iget v6, p0, Landroid/support/v7/app/v;->k:I

    iget v7, p0, Landroid/support/v7/app/v;->l:I

    invoke-virtual {v1, v2, v4, v6, v7}, Landroid/widget/FrameLayout;->setPadding(IIII)V

    .line 451
    :cond_150
    iget-object v1, p0, Landroid/support/v7/app/v;->f:Landroid/widget/ListView;

    if-eqz v1, :cond_15d

    .line 452
    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, 0x0

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 458
    :cond_15d
    :goto_15d
    iget-object v0, p0, Landroid/support/v7/app/v;->f:Landroid/widget/ListView;

    .line 459
    if-eqz v0, :cond_174

    iget-object v1, p0, Landroid/support/v7/app/v;->D:Landroid/widget/ListAdapter;

    if-eqz v1, :cond_174

    .line 460
    iget-object v1, p0, Landroid/support/v7/app/v;->D:Landroid/widget/ListAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 461
    iget v1, p0, Landroid/support/v7/app/v;->E:I

    .line 462
    if-ltz v1, :cond_174

    .line 463
    invoke-virtual {v0, v1, v3}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 464
    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setSelection(I)V

    .line 5183
    :cond_174
    iget-object v0, v5, Landroid/support/v7/internal/widget/ax;->a:Landroid/content/res/TypedArray;

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 469
    return-void

    .line 3534
    :cond_17a
    iget-object v1, p0, Landroid/support/v7/app/v;->B:Landroid/widget/TextView;

    invoke-virtual {v1, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 3535
    iget-object v1, p0, Landroid/support/v7/app/v;->w:Landroid/widget/ScrollView;

    iget-object v4, p0, Landroid/support/v7/app/v;->B:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/ScrollView;->removeView(Landroid/view/View;)V

    .line 3537
    iget-object v1, p0, Landroid/support/v7/app/v;->f:Landroid/widget/ListView;

    if-eqz v1, :cond_1a7

    .line 3538
    iget-object v0, p0, Landroid/support/v7/app/v;->w:Landroid/widget/ScrollView;

    invoke-virtual {v0}, Landroid/widget/ScrollView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 3539
    iget-object v1, p0, Landroid/support/v7/app/v;->w:Landroid/widget/ScrollView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v1

    .line 3540
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeViewAt(I)V

    .line 3541
    iget-object v4, p0, Landroid/support/v7/app/v;->f:Landroid/widget/ListView;

    new-instance v5, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v5, v10, v10}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v4, v1, v5}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_3e

    .line 3544
    :cond_1a7
    invoke-virtual {v0, v9}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto/16 :goto_3e

    .line 3560
    :cond_1ac
    iget-object v0, p0, Landroid/support/v7/app/v;->n:Landroid/widget/Button;

    iget-object v1, p0, Landroid/support/v7/app/v;->o:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 3561
    iget-object v0, p0, Landroid/support/v7/app/v;->n:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    move v1, v3

    .line 3562
    goto/16 :goto_60

    .line 3571
    :cond_1bb
    iget-object v0, p0, Landroid/support/v7/app/v;->q:Landroid/widget/Button;

    iget-object v4, p0, Landroid/support/v7/app/v;->r:Ljava/lang/CharSequence;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 3572
    iget-object v0, p0, Landroid/support/v7/app/v;->q:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 3574
    or-int/lit8 v1, v1, 0x2

    goto/16 :goto_81

    .line 3583
    :cond_1cb
    iget-object v0, p0, Landroid/support/v7/app/v;->t:Landroid/widget/Button;

    iget-object v4, p0, Landroid/support/v7/app/v;->u:Ljava/lang/CharSequence;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 3584
    iget-object v0, p0, Landroid/support/v7/app/v;->t:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 3586
    or-int/lit8 v1, v1, 0x4

    goto/16 :goto_a2

    :cond_1db
    move v0, v2

    .line 4165
    goto/16 :goto_b7

    .line 3596
    :cond_1de
    const/4 v0, 0x2

    if-ne v1, v0, :cond_1e8

    .line 3597
    iget-object v0, p0, Landroid/support/v7/app/v;->q:Landroid/widget/Button;

    invoke-static {v0}, Landroid/support/v7/app/v;->a(Landroid/widget/Button;)V

    goto/16 :goto_c0

    .line 3598
    :cond_1e8
    const/4 v0, 0x4

    if-ne v1, v0, :cond_c0

    .line 3599
    iget-object v0, p0, Landroid/support/v7/app/v;->t:Landroid/widget/Button;

    invoke-static {v0}, Landroid/support/v7/app/v;->a(Landroid/widget/Button;)V

    goto/16 :goto_c0

    :cond_1f2
    move v4, v2

    .line 3603
    goto/16 :goto_c3

    .line 4485
    :cond_1f5
    iget-object v1, p0, Landroid/support/v7/app/v;->c:Landroid/view/Window;

    const v6, 0x1020006

    invoke-virtual {v1, v6}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Landroid/support/v7/app/v;->z:Landroid/widget/ImageView;

    .line 4487
    iget-object v1, p0, Landroid/support/v7/app/v;->d:Ljava/lang/CharSequence;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_22d

    move v1, v3

    .line 4488
    :goto_20b
    if-eqz v1, :cond_260

    .line 4490
    iget-object v0, p0, Landroid/support/v7/app/v;->c:Landroid/view/Window;

    sget v1, Landroid/support/v7/a/i;->alertTitle:I

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Landroid/support/v7/app/v;->A:Landroid/widget/TextView;

    .line 4491
    iget-object v0, p0, Landroid/support/v7/app/v;->A:Landroid/widget/TextView;

    iget-object v1, p0, Landroid/support/v7/app/v;->d:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 4496
    iget v0, p0, Landroid/support/v7/app/v;->x:I

    if-eqz v0, :cond_22f

    .line 4497
    iget-object v0, p0, Landroid/support/v7/app/v;->z:Landroid/widget/ImageView;

    iget v1, p0, Landroid/support/v7/app/v;->x:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_f2

    :cond_22d
    move v1, v2

    .line 4487
    goto :goto_20b

    .line 4498
    :cond_22f
    iget-object v0, p0, Landroid/support/v7/app/v;->y:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_23c

    .line 4499
    iget-object v0, p0, Landroid/support/v7/app/v;->z:Landroid/widget/ImageView;

    iget-object v1, p0, Landroid/support/v7/app/v;->y:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_f2

    .line 4503
    :cond_23c
    iget-object v0, p0, Landroid/support/v7/app/v;->A:Landroid/widget/TextView;

    iget-object v1, p0, Landroid/support/v7/app/v;->z:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getPaddingLeft()I

    move-result v1

    iget-object v6, p0, Landroid/support/v7/app/v;->z:Landroid/widget/ImageView;

    invoke-virtual {v6}, Landroid/widget/ImageView;->getPaddingTop()I

    move-result v6

    iget-object v7, p0, Landroid/support/v7/app/v;->z:Landroid/widget/ImageView;

    invoke-virtual {v7}, Landroid/widget/ImageView;->getPaddingRight()I

    move-result v7

    iget-object v8, p0, Landroid/support/v7/app/v;->z:Landroid/widget/ImageView;

    invoke-virtual {v8}, Landroid/widget/ImageView;->getPaddingBottom()I

    move-result v8

    invoke-virtual {v0, v1, v6, v7, v8}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 4507
    iget-object v0, p0, Landroid/support/v7/app/v;->z:Landroid/widget/ImageView;

    invoke-virtual {v0, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_f2

    .line 4511
    :cond_260
    iget-object v1, p0, Landroid/support/v7/app/v;->c:Landroid/view/Window;

    sget v6, Landroid/support/v7/a/i;->title_template:I

    invoke-virtual {v1, v6}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 4512
    invoke-virtual {v1, v9}, Landroid/view/View;->setVisibility(I)V

    .line 4513
    iget-object v1, p0, Landroid/support/v7/app/v;->z:Landroid/widget/ImageView;

    invoke-virtual {v1, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 4514
    invoke-virtual {v0, v9}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto/16 :goto_f2

    .line 429
    :cond_275
    iget v1, p0, Landroid/support/v7/app/v;->h:I

    if-eqz v1, :cond_288

    .line 430
    iget-object v1, p0, Landroid/support/v7/app/v;->a:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 431
    iget v4, p0, Landroid/support/v7/app/v;->h:I

    invoke-virtual {v1, v4, v0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    move-object v4, v1

    .line 432
    goto/16 :goto_11d

    .line 433
    :cond_288
    const/4 v1, 0x0

    move-object v4, v1

    goto/16 :goto_11d

    .line 455
    :cond_28c
    invoke-virtual {v0, v9}, Landroid/widget/FrameLayout;->setVisibility(I)V

    goto/16 :goto_15d
.end method

.method private e(I)Landroid/widget/Button;
    .registers 3

    .prologue
    .line 384
    packed-switch p1, :pswitch_data_e

    .line 392
    const/4 v0, 0x0

    :goto_4
    return-object v0

    .line 386
    :pswitch_5
    iget-object v0, p0, Landroid/support/v7/app/v;->n:Landroid/widget/Button;

    goto :goto_4

    .line 388
    :pswitch_8
    iget-object v0, p0, Landroid/support/v7/app/v;->q:Landroid/widget/Button;

    goto :goto_4

    .line 390
    :pswitch_b
    iget-object v0, p0, Landroid/support/v7/app/v;->t:Landroid/widget/Button;

    goto :goto_4

    .line 384
    :pswitch_data_e
    .packed-switch -0x3
        :pswitch_b
        :pswitch_8
        :pswitch_5
    .end packed-switch
.end method

.method private static synthetic e(Landroid/support/v7/app/v;)Landroid/widget/Button;
    .registers 2

    .prologue
    .line 57
    iget-object v0, p0, Landroid/support/v7/app/v;->t:Landroid/widget/Button;

    return-object v0
.end method

.method private e()Z
    .registers 7

    .prologue
    const/16 v5, 0x8

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 553
    .line 554
    iget-object v0, p0, Landroid/support/v7/app/v;->c:Landroid/view/Window;

    const v1, 0x1020019

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Landroid/support/v7/app/v;->n:Landroid/widget/Button;

    .line 555
    iget-object v0, p0, Landroid/support/v7/app/v;->n:Landroid/widget/Button;

    iget-object v1, p0, Landroid/support/v7/app/v;->N:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 557
    iget-object v0, p0, Landroid/support/v7/app/v;->o:Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_89

    .line 558
    iget-object v0, p0, Landroid/support/v7/app/v;->n:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setVisibility(I)V

    move v1, v2

    .line 565
    :goto_26
    iget-object v0, p0, Landroid/support/v7/app/v;->c:Landroid/view/Window;

    const v4, 0x102001a

    invoke-virtual {v0, v4}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Landroid/support/v7/app/v;->q:Landroid/widget/Button;

    .line 566
    iget-object v0, p0, Landroid/support/v7/app/v;->q:Landroid/widget/Button;

    iget-object v4, p0, Landroid/support/v7/app/v;->N:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 568
    iget-object v0, p0, Landroid/support/v7/app/v;->r:Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_97

    .line 569
    iget-object v0, p0, Landroid/support/v7/app/v;->q:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setVisibility(I)V

    .line 577
    :goto_47
    iget-object v0, p0, Landroid/support/v7/app/v;->c:Landroid/view/Window;

    const v4, 0x102001b

    invoke-virtual {v0, v4}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Landroid/support/v7/app/v;->t:Landroid/widget/Button;

    .line 578
    iget-object v0, p0, Landroid/support/v7/app/v;->t:Landroid/widget/Button;

    iget-object v4, p0, Landroid/support/v7/app/v;->N:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 580
    iget-object v0, p0, Landroid/support/v7/app/v;->u:Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_a6

    .line 581
    iget-object v0, p0, Landroid/support/v7/app/v;->t:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setVisibility(I)V

    .line 589
    :goto_68
    iget-object v0, p0, Landroid/support/v7/app/v;->a:Landroid/content/Context;

    .line 6163
    new-instance v4, Landroid/util/TypedValue;

    invoke-direct {v4}, Landroid/util/TypedValue;-><init>()V

    .line 6164
    invoke-virtual {v0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    sget v5, Landroid/support/v7/a/d;->alertDialogCenterButtons:I

    invoke-virtual {v0, v5, v4, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 6165
    iget v0, v4, Landroid/util/TypedValue;->data:I

    if-eqz v0, :cond_b5

    move v0, v3

    .line 589
    :goto_7d
    if-eqz v0, :cond_86

    .line 594
    if-ne v1, v3, :cond_b7

    .line 595
    iget-object v0, p0, Landroid/support/v7/app/v;->n:Landroid/widget/Button;

    invoke-static {v0}, Landroid/support/v7/app/v;->a(Landroid/widget/Button;)V

    .line 603
    :cond_86
    :goto_86
    if-eqz v1, :cond_c9

    :goto_88
    return v3

    .line 560
    :cond_89
    iget-object v0, p0, Landroid/support/v7/app/v;->n:Landroid/widget/Button;

    iget-object v1, p0, Landroid/support/v7/app/v;->o:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 561
    iget-object v0, p0, Landroid/support/v7/app/v;->n:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    move v1, v3

    .line 562
    goto :goto_26

    .line 571
    :cond_97
    iget-object v0, p0, Landroid/support/v7/app/v;->q:Landroid/widget/Button;

    iget-object v4, p0, Landroid/support/v7/app/v;->r:Ljava/lang/CharSequence;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 572
    iget-object v0, p0, Landroid/support/v7/app/v;->q:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 574
    or-int/lit8 v1, v1, 0x2

    goto :goto_47

    .line 583
    :cond_a6
    iget-object v0, p0, Landroid/support/v7/app/v;->t:Landroid/widget/Button;

    iget-object v4, p0, Landroid/support/v7/app/v;->u:Ljava/lang/CharSequence;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 584
    iget-object v0, p0, Landroid/support/v7/app/v;->t:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 586
    or-int/lit8 v1, v1, 0x4

    goto :goto_68

    :cond_b5
    move v0, v2

    .line 6165
    goto :goto_7d

    .line 596
    :cond_b7
    const/4 v0, 0x2

    if-ne v1, v0, :cond_c0

    .line 597
    iget-object v0, p0, Landroid/support/v7/app/v;->q:Landroid/widget/Button;

    invoke-static {v0}, Landroid/support/v7/app/v;->a(Landroid/widget/Button;)V

    goto :goto_86

    .line 598
    :cond_c0
    const/4 v0, 0x4

    if-ne v1, v0, :cond_86

    .line 599
    iget-object v0, p0, Landroid/support/v7/app/v;->t:Landroid/widget/Button;

    invoke-static {v0}, Landroid/support/v7/app/v;->a(Landroid/widget/Button;)V

    goto :goto_86

    :cond_c9
    move v3, v2

    .line 603
    goto :goto_88
.end method

.method private static synthetic f(Landroid/support/v7/app/v;)Landroid/os/Message;
    .registers 2

    .prologue
    .line 57
    iget-object v0, p0, Landroid/support/v7/app/v;->v:Landroid/os/Message;

    return-object v0
.end method

.method private static synthetic g(Landroid/support/v7/app/v;)Landroid/support/v7/app/bf;
    .registers 2

    .prologue
    .line 57
    iget-object v0, p0, Landroid/support/v7/app/v;->b:Landroid/support/v7/app/bf;

    return-object v0
.end method

.method private static synthetic h(Landroid/support/v7/app/v;)Landroid/os/Handler;
    .registers 2

    .prologue
    .line 57
    iget-object v0, p0, Landroid/support/v7/app/v;->M:Landroid/os/Handler;

    return-object v0
.end method

.method private static synthetic i(Landroid/support/v7/app/v;)I
    .registers 2

    .prologue
    .line 57
    iget v0, p0, Landroid/support/v7/app/v;->H:I

    return v0
.end method

.method private static synthetic j(Landroid/support/v7/app/v;)I
    .registers 2

    .prologue
    .line 57
    iget v0, p0, Landroid/support/v7/app/v;->I:I

    return v0
.end method

.method private static synthetic k(Landroid/support/v7/app/v;)I
    .registers 2

    .prologue
    .line 57
    iget v0, p0, Landroid/support/v7/app/v;->J:I

    return v0
.end method

.method private static synthetic l(Landroid/support/v7/app/v;)I
    .registers 2

    .prologue
    .line 57
    iget v0, p0, Landroid/support/v7/app/v;->K:I

    return v0
.end method


# virtual methods
.method public final a(I)V
    .registers 4

    .prologue
    .line 338
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v7/app/v;->y:Landroid/graphics/drawable/Drawable;

    .line 339
    iput p1, p0, Landroid/support/v7/app/v;->x:I

    .line 341
    iget-object v0, p0, Landroid/support/v7/app/v;->z:Landroid/widget/ImageView;

    if-eqz v0, :cond_12

    .line 342
    if-eqz p1, :cond_13

    .line 343
    iget-object v0, p0, Landroid/support/v7/app/v;->z:Landroid/widget/ImageView;

    iget v1, p0, Landroid/support/v7/app/v;->x:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 348
    :cond_12
    :goto_12
    return-void

    .line 345
    :cond_13
    iget-object v0, p0, Landroid/support/v7/app/v;->z:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_12
.end method

.method public final a(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;Landroid/os/Message;)V
    .registers 7

    .prologue
    .line 305
    if-nez p4, :cond_a

    if-eqz p3, :cond_a

    .line 306
    iget-object v0, p0, Landroid/support/v7/app/v;->M:Landroid/os/Handler;

    invoke-virtual {v0, p1, p3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object p4

    .line 309
    :cond_a
    packed-switch p1, :pswitch_data_24

    .line 327
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Button does not exist"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 312
    :pswitch_15
    iput-object p2, p0, Landroid/support/v7/app/v;->o:Ljava/lang/CharSequence;

    .line 313
    iput-object p4, p0, Landroid/support/v7/app/v;->p:Landroid/os/Message;

    .line 324
    :goto_19
    return-void

    .line 317
    :pswitch_1a
    iput-object p2, p0, Landroid/support/v7/app/v;->r:Ljava/lang/CharSequence;

    .line 318
    iput-object p4, p0, Landroid/support/v7/app/v;->s:Landroid/os/Message;

    goto :goto_19

    .line 322
    :pswitch_1f
    iput-object p2, p0, Landroid/support/v7/app/v;->u:Ljava/lang/CharSequence;

    .line 323
    iput-object p4, p0, Landroid/support/v7/app/v;->v:Landroid/os/Message;

    goto :goto_19

    .line 309
    :pswitch_data_24
    .packed-switch -0x3
        :pswitch_1f
        :pswitch_1a
        :pswitch_15
    .end packed-switch
.end method

.method public final a(Landroid/graphics/drawable/Drawable;)V
    .registers 4

    .prologue
    .line 356
    iput-object p1, p0, Landroid/support/v7/app/v;->y:Landroid/graphics/drawable/Drawable;

    .line 357
    const/4 v0, 0x0

    iput v0, p0, Landroid/support/v7/app/v;->x:I

    .line 359
    iget-object v0, p0, Landroid/support/v7/app/v;->z:Landroid/widget/ImageView;

    if-eqz v0, :cond_10

    .line 360
    if-eqz p1, :cond_11

    .line 361
    iget-object v0, p0, Landroid/support/v7/app/v;->z:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 366
    :cond_10
    :goto_10
    return-void

    .line 363
    :cond_11
    iget-object v0, p0, Landroid/support/v7/app/v;->z:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_10
.end method

.method public final a(Landroid/view/View;IIII)V
    .registers 7

    .prologue
    .line 274
    iput-object p1, p0, Landroid/support/v7/app/v;->g:Landroid/view/View;

    .line 275
    const/4 v0, 0x0

    iput v0, p0, Landroid/support/v7/app/v;->h:I

    .line 276
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/app/v;->m:Z

    .line 277
    iput p2, p0, Landroid/support/v7/app/v;->i:I

    .line 278
    iput p3, p0, Landroid/support/v7/app/v;->j:I

    .line 279
    iput p4, p0, Landroid/support/v7/app/v;->k:I

    .line 280
    iput p5, p0, Landroid/support/v7/app/v;->l:I

    .line 281
    return-void
.end method

.method public final a(Ljava/lang/CharSequence;)V
    .registers 3

    .prologue
    .line 231
    iput-object p1, p0, Landroid/support/v7/app/v;->d:Ljava/lang/CharSequence;

    .line 232
    iget-object v0, p0, Landroid/support/v7/app/v;->A:Landroid/widget/TextView;

    if-eqz v0, :cond_b

    .line 233
    iget-object v0, p0, Landroid/support/v7/app/v;->A:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 235
    :cond_b
    return-void
.end method

.method public final b(Landroid/view/View;)V
    .registers 3

    .prologue
    const/4 v0, 0x0

    .line 264
    iput-object p1, p0, Landroid/support/v7/app/v;->g:Landroid/view/View;

    .line 265
    iput v0, p0, Landroid/support/v7/app/v;->h:I

    .line 266
    iput-boolean v0, p0, Landroid/support/v7/app/v;->m:Z

    .line 267
    return-void
.end method

.method public final b(Ljava/lang/CharSequence;)V
    .registers 3

    .prologue
    .line 245
    iput-object p1, p0, Landroid/support/v7/app/v;->e:Ljava/lang/CharSequence;

    .line 246
    iget-object v0, p0, Landroid/support/v7/app/v;->B:Landroid/widget/TextView;

    if-eqz v0, :cond_b

    .line 247
    iget-object v0, p0, Landroid/support/v7/app/v;->B:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 249
    :cond_b
    return-void
.end method
