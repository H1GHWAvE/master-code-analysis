.class Landroid/support/v7/app/AppCompatDelegateImplV7;
.super Landroid/support/v7/app/ak;
.source "SourceFile"

# interfaces
.implements Landroid/support/v4/view/as;
.implements Landroid/support/v7/internal/view/menu/j;


# instance fields
.field private A:Landroid/support/v7/app/az;

.field private B:Landroid/support/v7/app/be;

.field private C:Z

.field private D:Landroid/view/ViewGroup;

.field private E:Landroid/view/ViewGroup;

.field private F:Landroid/widget/TextView;

.field private G:Landroid/view/View;

.field private H:Z

.field private I:Z

.field private J:Z

.field private K:[Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;

.field private L:Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;

.field private final M:Ljava/lang/Runnable;

.field private N:Z

.field private O:Landroid/graphics/Rect;

.field private P:Landroid/graphics/Rect;

.field private Q:Landroid/support/v7/internal/a/a;

.field private s:Landroid/support/v7/internal/widget/ac;

.field t:Landroid/support/v7/c/a;

.field u:Landroid/support/v7/internal/widget/ActionBarContextView;

.field v:Landroid/widget/PopupWindow;

.field w:Ljava/lang/Runnable;

.field x:Landroid/support/v4/view/fk;

.field y:Z

.field z:I


# direct methods
.method constructor <init>(Landroid/content/Context;Landroid/view/Window;Landroid/support/v7/app/ai;)V
    .registers 5

    .prologue
    .line 143
    invoke-direct {p0, p1, p2, p3}, Landroid/support/v7/app/ak;-><init>(Landroid/content/Context;Landroid/view/Window;Landroid/support/v7/app/ai;)V

    .line 101
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->x:Landroid/support/v4/view/fk;

    .line 121
    new-instance v0, Landroid/support/v7/app/at;

    invoke-direct {v0, p0}, Landroid/support/v7/app/at;-><init>(Landroid/support/v7/app/AppCompatDelegateImplV7;)V

    iput-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->M:Ljava/lang/Runnable;

    .line 144
    return-void
.end method

.method private static synthetic a(Landroid/support/v7/app/AppCompatDelegateImplV7;)I
    .registers 2

    .prologue
    .line 90
    iget v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->z:I

    return v0
.end method

.method private static synthetic a(Landroid/support/v7/app/AppCompatDelegateImplV7;Landroid/view/Menu;)Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;
    .registers 3

    .prologue
    .line 90
    invoke-virtual {p0, p1}, Landroid/support/v7/app/AppCompatDelegateImplV7;->a(Landroid/view/Menu;)Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;Landroid/view/KeyEvent;)V
    .registers 13

    .prologue
    const/4 v1, -0x1

    const/4 v2, -0x2

    const/4 v3, 0x0

    const/4 v9, 0x1

    .line 983
    iget-boolean v0, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->o:Z

    if-nez v0, :cond_c

    .line 19193
    iget-boolean v0, p0, Landroid/support/v7/app/ak;->r:Z

    .line 983
    if-eqz v0, :cond_d

    .line 1079
    :cond_c
    :goto_c
    return-void

    .line 989
    :cond_d
    iget v0, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->a:I

    if-nez v0, :cond_32

    .line 990
    iget-object v4, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->e:Landroid/content/Context;

    .line 991
    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 992
    iget v0, v0, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v0, v0, 0xf

    const/4 v5, 0x4

    if-ne v0, v5, :cond_48

    move v0, v9

    .line 994
    :goto_23
    invoke-virtual {v4}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v4

    iget v4, v4, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    const/16 v5, 0xb

    if-lt v4, v5, :cond_4a

    move v4, v9

    .line 997
    :goto_2e
    if-eqz v0, :cond_32

    if-nez v4, :cond_c

    .line 19197
    :cond_32
    iget-object v0, p0, Landroid/support/v7/app/ak;->f:Landroid/view/Window;

    invoke-virtual {v0}, Landroid/view/Window;->getCallback()Landroid/view/Window$Callback;

    move-result-object v0

    .line 1003
    if-eqz v0, :cond_4c

    iget v4, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->a:I

    iget-object v5, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->j:Landroid/support/v7/internal/view/menu/i;

    invoke-interface {v0, v4, v5}, Landroid/view/Window$Callback;->onMenuOpened(ILandroid/view/Menu;)Z

    move-result v0

    if-nez v0, :cond_4c

    .line 1005
    invoke-virtual {p0, p1, v9}, Landroid/support/v7/app/AppCompatDelegateImplV7;->a(Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;Z)V

    goto :goto_c

    :cond_48
    move v0, v3

    .line 992
    goto :goto_23

    :cond_4a
    move v4, v3

    .line 994
    goto :goto_2e

    .line 1009
    :cond_4c
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->e:Landroid/content/Context;

    const-string v4, "window"

    invoke-virtual {v0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Landroid/view/WindowManager;

    .line 1010
    if-eqz v8, :cond_c

    .line 1015
    invoke-direct {p0, p1, p2}, Landroid/support/v7/app/AppCompatDelegateImplV7;->b(Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;Landroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 1020
    iget-object v0, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->g:Landroid/view/ViewGroup;

    if-eqz v0, :cond_67

    iget-boolean v0, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->q:Z

    if-eqz v0, :cond_1be

    .line 1021
    :cond_67
    iget-object v0, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->g:Landroid/view/ViewGroup;

    if-nez v0, :cond_152

    .line 20082
    invoke-virtual {p0}, Landroid/support/v7/app/AppCompatDelegateImplV7;->m()Landroid/content/Context;

    move-result-object v0

    .line 20821
    new-instance v1, Landroid/util/TypedValue;

    invoke-direct {v1}, Landroid/util/TypedValue;-><init>()V

    .line 20822
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->newTheme()Landroid/content/res/Resources$Theme;

    move-result-object v4

    .line 20823
    invoke-virtual {v0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/content/res/Resources$Theme;->setTo(Landroid/content/res/Resources$Theme;)V

    .line 20826
    sget v5, Landroid/support/v7/a/d;->actionBarPopupTheme:I

    invoke-virtual {v4, v5, v1, v9}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 20827
    iget v5, v1, Landroid/util/TypedValue;->resourceId:I

    if-eqz v5, :cond_91

    .line 20828
    iget v5, v1, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {v4, v5, v9}, Landroid/content/res/Resources$Theme;->applyStyle(IZ)V

    .line 20832
    :cond_91
    sget v5, Landroid/support/v7/a/d;->panelMenuListTheme:I

    invoke-virtual {v4, v5, v1, v9}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 20833
    iget v5, v1, Landroid/util/TypedValue;->resourceId:I

    if-eqz v5, :cond_14b

    .line 20834
    iget v1, v1, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {v4, v1, v9}, Landroid/content/res/Resources$Theme;->applyStyle(IZ)V

    .line 20839
    :goto_9f
    new-instance v1, Landroid/support/v7/internal/view/b;

    invoke-direct {v1, v0, v3}, Landroid/support/v7/internal/view/b;-><init>(Landroid/content/Context;I)V

    .line 20840
    invoke-virtual {v1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/content/res/Resources$Theme;->setTo(Landroid/content/res/Resources$Theme;)V

    .line 20842
    iput-object v1, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->l:Landroid/content/Context;

    .line 20844
    sget-object v0, Landroid/support/v7/a/n;->Theme:[I

    invoke-virtual {v1, v0}, Landroid/content/Context;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 20845
    sget v1, Landroid/support/v7/a/n;->Theme_panelBackground:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->b:I

    .line 20847
    sget v1, Landroid/support/v7/a/n;->Theme_android_windowAnimationStyle:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->f:I

    .line 20849
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 20083
    new-instance v0, Landroid/support/v7/app/bc;

    iget-object v1, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->l:Landroid/content/Context;

    invoke-direct {v0, p0, v1}, Landroid/support/v7/app/bc;-><init>(Landroid/support/v7/app/AppCompatDelegateImplV7;Landroid/content/Context;)V

    iput-object v0, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->g:Landroid/view/ViewGroup;

    .line 20084
    const/16 v0, 0x51

    iput v0, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->c:I

    .line 1023
    iget-object v0, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->g:Landroid/view/ViewGroup;

    if-eqz v0, :cond_c

    .line 21176
    :cond_d7
    :goto_d7
    iget-object v0, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->i:Landroid/view/View;

    if-eqz v0, :cond_165

    .line 21177
    iget-object v0, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->i:Landroid/view/View;

    iput-object v0, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->h:Landroid/view/View;

    move v0, v9

    .line 1031
    :goto_e0
    if-eqz v0, :cond_c

    .line 22804
    iget-object v0, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->h:Landroid/view/View;

    if-eqz v0, :cond_1bb

    .line 22805
    iget-object v0, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->i:Landroid/view/View;

    if-eqz v0, :cond_1ac

    move v0, v9

    .line 1031
    :goto_eb
    if-eqz v0, :cond_c

    .line 1035
    iget-object v0, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->h:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 1036
    if-nez v0, :cond_1d1

    .line 1037
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v0, v2, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    move-object v1, v0

    .line 1040
    :goto_fb
    iget v0, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->b:I

    .line 1041
    iget-object v4, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->g:Landroid/view/ViewGroup;

    invoke-virtual {v4, v0}, Landroid/view/ViewGroup;->setBackgroundResource(I)V

    .line 1043
    iget-object v0, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->h:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 1044
    if-eqz v0, :cond_115

    instance-of v4, v0, Landroid/view/ViewGroup;

    if-eqz v4, :cond_115

    .line 1045
    check-cast v0, Landroid/view/ViewGroup;

    iget-object v4, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->h:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 1047
    :cond_115
    iget-object v0, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->g:Landroid/view/ViewGroup;

    iget-object v4, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->h:Landroid/view/View;

    invoke-virtual {v0, v4, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1053
    iget-object v0, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->h:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->hasFocus()Z

    move-result v0

    if-nez v0, :cond_129

    .line 1054
    iget-object v0, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->h:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    :cond_129
    move v1, v2

    .line 1065
    :cond_12a
    :goto_12a
    iput-boolean v3, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->n:Z

    .line 1067
    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    iget v3, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->d:I

    iget v4, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->e:I

    const/16 v5, 0x3ea

    const/high16 v6, 0x820000

    const/4 v7, -0x3

    invoke-direct/range {v0 .. v7}, Landroid/view/WindowManager$LayoutParams;-><init>(IIIIIII)V

    .line 1074
    iget v1, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->c:I

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 1075
    iget v1, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->f:I

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    .line 1077
    iget-object v1, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->g:Landroid/view/ViewGroup;

    invoke-interface {v8, v1, v0}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1078
    iput-boolean v9, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->o:Z

    goto/16 :goto_c

    .line 20836
    :cond_14b
    sget v1, Landroid/support/v7/a/m;->Theme_AppCompat_CompactMenu:I

    invoke-virtual {v4, v1, v9}, Landroid/content/res/Resources$Theme;->applyStyle(IZ)V

    goto/16 :goto_9f

    .line 1025
    :cond_152
    iget-boolean v0, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->q:Z

    if-eqz v0, :cond_d7

    iget-object v0, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->g:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-lez v0, :cond_d7

    .line 1027
    iget-object v0, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->g:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    goto/16 :goto_d7

    .line 21181
    :cond_165
    iget-object v0, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->j:Landroid/support/v7/internal/view/menu/i;

    if-eqz v0, :cond_1a9

    .line 21185
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->B:Landroid/support/v7/app/be;

    if-nez v0, :cond_174

    .line 21186
    new-instance v0, Landroid/support/v7/app/be;

    invoke-direct {v0, p0, v3}, Landroid/support/v7/app/be;-><init>(Landroid/support/v7/app/AppCompatDelegateImplV7;B)V

    iput-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->B:Landroid/support/v7/app/be;

    .line 21189
    :cond_174
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->B:Landroid/support/v7/app/be;

    .line 21865
    iget-object v1, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->j:Landroid/support/v7/internal/view/menu/i;

    if-nez v1, :cond_186

    const/4 v0, 0x0

    .line 21191
    :goto_17b
    check-cast v0, Landroid/view/View;

    iput-object v0, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->h:Landroid/view/View;

    .line 21193
    iget-object v0, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->h:Landroid/view/View;

    if-eqz v0, :cond_1a9

    move v0, v9

    goto/16 :goto_e0

    .line 21867
    :cond_186
    iget-object v1, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->k:Landroid/support/v7/internal/view/menu/g;

    if-nez v1, :cond_1a0

    .line 21868
    new-instance v1, Landroid/support/v7/internal/view/menu/g;

    iget-object v4, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->l:Landroid/content/Context;

    sget v5, Landroid/support/v7/a/k;->abc_list_menu_item_layout:I

    invoke-direct {v1, v4, v5}, Landroid/support/v7/internal/view/menu/g;-><init>(Landroid/content/Context;I)V

    iput-object v1, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->k:Landroid/support/v7/internal/view/menu/g;

    .line 21870
    iget-object v1, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->k:Landroid/support/v7/internal/view/menu/g;

    .line 22134
    iput-object v0, v1, Landroid/support/v7/internal/view/menu/g;->g:Landroid/support/v7/internal/view/menu/y;

    .line 21871
    iget-object v0, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->j:Landroid/support/v7/internal/view/menu/i;

    iget-object v1, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->k:Landroid/support/v7/internal/view/menu/g;

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/view/menu/i;->a(Landroid/support/v7/internal/view/menu/x;)V

    .line 21874
    :cond_1a0
    iget-object v0, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->k:Landroid/support/v7/internal/view/menu/g;

    iget-object v1, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->g:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/view/menu/g;->a(Landroid/view/ViewGroup;)Landroid/support/v7/internal/view/menu/z;

    move-result-object v0

    goto :goto_17b

    :cond_1a9
    move v0, v3

    .line 21193
    goto/16 :goto_e0

    .line 22807
    :cond_1ac
    iget-object v0, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->k:Landroid/support/v7/internal/view/menu/g;

    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/g;->d()Landroid/widget/ListAdapter;

    move-result-object v0

    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v0

    if-lez v0, :cond_1bb

    move v0, v9

    goto/16 :goto_eb

    :cond_1bb
    move v0, v3

    goto/16 :goto_eb

    .line 1056
    :cond_1be
    iget-object v0, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->i:Landroid/view/View;

    if-eqz v0, :cond_1ce

    .line 1059
    iget-object v0, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->i:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 1060
    if-eqz v0, :cond_1ce

    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    if-eq v0, v1, :cond_12a

    :cond_1ce
    move v1, v2

    goto/16 :goto_12a

    :cond_1d1
    move-object v1, v0

    goto/16 :goto_fb
.end method

.method static synthetic a(Landroid/support/v7/app/AppCompatDelegateImplV7;I)V
    .registers 7

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 90
    .line 30506
    invoke-virtual {p0, p1}, Landroid/support/v7/app/AppCompatDelegateImplV7;->f(I)Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;

    move-result-object v0

    .line 30508
    iget-object v1, v0, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->j:Landroid/support/v7/internal/view/menu/i;

    if-eqz v1, :cond_26

    .line 30509
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 30510
    iget-object v2, v0, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->j:Landroid/support/v7/internal/view/menu/i;

    invoke-virtual {v2, v1}, Landroid/support/v7/internal/view/menu/i;->c(Landroid/os/Bundle;)V

    .line 30511
    invoke-virtual {v1}, Landroid/os/Bundle;->size()I

    move-result v2

    if-lez v2, :cond_1c

    .line 30512
    iput-object v1, v0, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->u:Landroid/os/Bundle;

    .line 30515
    :cond_1c
    iget-object v1, v0, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->j:Landroid/support/v7/internal/view/menu/i;

    invoke-virtual {v1}, Landroid/support/v7/internal/view/menu/i;->d()V

    .line 30516
    iget-object v1, v0, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->j:Landroid/support/v7/internal/view/menu/i;

    invoke-virtual {v1}, Landroid/support/v7/internal/view/menu/i;->clear()V

    .line 30518
    :cond_26
    iput-boolean v4, v0, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->r:Z

    .line 30519
    iput-boolean v4, v0, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->q:Z

    .line 30522
    const/16 v0, 0x6c

    if-eq p1, v0, :cond_30

    if-nez p1, :cond_40

    :cond_30
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->s:Landroid/support/v7/internal/widget/ac;

    if-eqz v0, :cond_40

    .line 30524
    invoke-virtual {p0, v3}, Landroid/support/v7/app/AppCompatDelegateImplV7;->f(I)Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;

    move-result-object v0

    .line 30525
    if-eqz v0, :cond_40

    .line 30526
    iput-boolean v3, v0, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->m:Z

    .line 30527
    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Landroid/support/v7/app/AppCompatDelegateImplV7;->b(Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;Landroid/view/KeyEvent;)Z

    .line 90
    :cond_40
    return-void
.end method

.method private static synthetic a(Landroid/support/v7/app/AppCompatDelegateImplV7;ILandroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;Landroid/view/Menu;)V
    .registers 4

    .prologue
    .line 90
    invoke-virtual {p0, p1, p2, p3}, Landroid/support/v7/app/AppCompatDelegateImplV7;->a(ILandroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;Landroid/view/Menu;)V

    return-void
.end method

.method private static synthetic a(Landroid/support/v7/app/AppCompatDelegateImplV7;Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;Z)V
    .registers 3

    .prologue
    .line 90
    invoke-virtual {p0, p1, p2}, Landroid/support/v7/app/AppCompatDelegateImplV7;->a(Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;Z)V

    return-void
.end method

.method private static synthetic a(Landroid/support/v7/app/AppCompatDelegateImplV7;Landroid/support/v7/internal/view/menu/i;)V
    .registers 2

    .prologue
    .line 90
    invoke-virtual {p0, p1}, Landroid/support/v7/app/AppCompatDelegateImplV7;->b(Landroid/support/v7/internal/view/menu/i;)V

    return-void
.end method

.method private a(Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;)Z
    .registers 8

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 1082
    invoke-virtual {p0}, Landroid/support/v7/app/AppCompatDelegateImplV7;->m()Landroid/content/Context;

    move-result-object v0

    .line 22821
    new-instance v1, Landroid/util/TypedValue;

    invoke-direct {v1}, Landroid/util/TypedValue;-><init>()V

    .line 22822
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->newTheme()Landroid/content/res/Resources$Theme;

    move-result-object v2

    .line 22823
    invoke-virtual {v0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources$Theme;->setTo(Landroid/content/res/Resources$Theme;)V

    .line 22826
    sget v3, Landroid/support/v7/a/d;->actionBarPopupTheme:I

    invoke-virtual {v2, v3, v1, v4}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 22827
    iget v3, v1, Landroid/util/TypedValue;->resourceId:I

    if-eqz v3, :cond_28

    .line 22828
    iget v3, v1, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources$Theme;->applyStyle(IZ)V

    .line 22832
    :cond_28
    sget v3, Landroid/support/v7/a/d;->panelMenuListTheme:I

    invoke-virtual {v2, v3, v1, v4}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 22833
    iget v3, v1, Landroid/util/TypedValue;->resourceId:I

    if-eqz v3, :cond_6b

    .line 22834
    iget v1, v1, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {v2, v1, v4}, Landroid/content/res/Resources$Theme;->applyStyle(IZ)V

    .line 22839
    :goto_36
    new-instance v1, Landroid/support/v7/internal/view/b;

    invoke-direct {v1, v0, v5}, Landroid/support/v7/internal/view/b;-><init>(Landroid/content/Context;I)V

    .line 22840
    invoke-virtual {v1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/content/res/Resources$Theme;->setTo(Landroid/content/res/Resources$Theme;)V

    .line 22842
    iput-object v1, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->l:Landroid/content/Context;

    .line 22844
    sget-object v0, Landroid/support/v7/a/n;->Theme:[I

    invoke-virtual {v1, v0}, Landroid/content/Context;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 22845
    sget v1, Landroid/support/v7/a/n;->Theme_panelBackground:I

    invoke-virtual {v0, v1, v5}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->b:I

    .line 22847
    sget v1, Landroid/support/v7/a/n;->Theme_android_windowAnimationStyle:I

    invoke-virtual {v0, v1, v5}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->f:I

    .line 22849
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1083
    new-instance v0, Landroid/support/v7/app/bc;

    iget-object v1, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->l:Landroid/content/Context;

    invoke-direct {v0, p0, v1}, Landroid/support/v7/app/bc;-><init>(Landroid/support/v7/app/AppCompatDelegateImplV7;Landroid/content/Context;)V

    iput-object v0, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->g:Landroid/view/ViewGroup;

    .line 1084
    const/16 v0, 0x51

    iput v0, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->c:I

    .line 1085
    return v4

    .line 22836
    :cond_6b
    sget v1, Landroid/support/v7/a/m;->Theme_AppCompat_CompactMenu:I

    invoke-virtual {v2, v1, v4}, Landroid/content/res/Resources$Theme;->applyStyle(IZ)V

    goto :goto_36
.end method

.method private a(Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;ILandroid/view/KeyEvent;)Z
    .registers 6

    .prologue
    const/4 v0, 0x0

    .line 1473
    invoke-virtual {p3}, Landroid/view/KeyEvent;->isSystem()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 1493
    :cond_7
    :goto_7
    return v0

    .line 1481
    :cond_8
    iget-boolean v1, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->m:Z

    if-nez v1, :cond_12

    invoke-direct {p0, p1, p3}, Landroid/support/v7/app/AppCompatDelegateImplV7;->b(Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;Landroid/view/KeyEvent;)Z

    move-result v1

    if-eqz v1, :cond_7

    :cond_12
    iget-object v1, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->j:Landroid/support/v7/internal/view/menu/i;

    if-eqz v1, :cond_7

    .line 1483
    iget-object v0, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->j:Landroid/support/v7/internal/view/menu/i;

    const/4 v1, 0x1

    invoke-virtual {v0, p2, p3, v1}, Landroid/support/v7/internal/view/menu/i;->performShortcut(ILandroid/view/KeyEvent;I)Z

    move-result v0

    goto :goto_7
.end method

.method private a(Landroid/view/ViewParent;)Z
    .registers 5

    .prologue
    const/4 v2, 0x0

    .line 919
    if-nez p1, :cond_20

    move v0, v2

    .line 936
    :goto_4
    return v0

    .line 938
    :cond_5
    invoke-interface {v1}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    .line 924
    :goto_9
    if-nez v1, :cond_d

    .line 929
    const/4 v0, 0x1

    goto :goto_4

    .line 930
    :cond_d
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->D:Landroid/view/ViewGroup;

    if-eq v1, v0, :cond_1e

    instance-of v0, v1, Landroid/view/View;

    if-eqz v0, :cond_1e

    move-object v0, v1

    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Landroid/support/v4/view/cx;->D(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_1e
    move v0, v2

    .line 936
    goto :goto_4

    :cond_20
    move-object v1, p1

    goto :goto_9
.end method

.method static synthetic b(Landroid/support/v7/app/AppCompatDelegateImplV7;I)I
    .registers 10

    .prologue
    const/4 v6, -0x1

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 30539
    .line 30541
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->u:Landroid/support/v7/internal/widget/ActionBarContextView;

    if-eqz v0, :cond_be

    .line 30542
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->u:Landroid/support/v7/internal/widget/ActionBarContextView;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarContextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    instance-of v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;

    if-eqz v0, :cond_be

    .line 30543
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->u:Landroid/support/v7/internal/widget/ActionBarContextView;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarContextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 30547
    iget-object v1, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->u:Landroid/support/v7/internal/widget/ActionBarContextView;

    invoke-virtual {v1}, Landroid/support/v7/internal/widget/ActionBarContextView;->isShown()Z

    move-result v1

    if-eqz v1, :cond_ae

    .line 30548
    iget-object v1, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->O:Landroid/graphics/Rect;

    if-nez v1, :cond_33

    .line 30549
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->O:Landroid/graphics/Rect;

    .line 30550
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->P:Landroid/graphics/Rect;

    .line 30552
    :cond_33
    iget-object v1, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->O:Landroid/graphics/Rect;

    .line 30553
    iget-object v4, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->P:Landroid/graphics/Rect;

    .line 30554
    invoke-virtual {v1, v2, p1, v2, v2}, Landroid/graphics/Rect;->set(IIII)V

    .line 30556
    iget-object v5, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->E:Landroid/view/ViewGroup;

    invoke-static {v5, v1, v4}, Landroid/support/v7/internal/widget/bd;->a(Landroid/view/View;Landroid/graphics/Rect;Landroid/graphics/Rect;)V

    .line 30557
    iget v1, v4, Landroid/graphics/Rect;->top:I

    if-nez v1, :cond_97

    move v1, p1

    .line 30558
    :goto_44
    iget v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    if-eq v4, v1, :cond_bc

    .line 30560
    iput p1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 30562
    iget-object v1, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->G:Landroid/view/View;

    if-nez v1, :cond_99

    .line 30563
    new-instance v1, Landroid/view/View;

    iget-object v4, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->e:Landroid/content/Context;

    invoke-direct {v1, v4}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->G:Landroid/view/View;

    .line 30564
    iget-object v1, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->G:Landroid/view/View;

    iget-object v4, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->e:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Landroid/support/v7/a/f;->abc_input_method_navigation_guard:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v1, v4}, Landroid/view/View;->setBackgroundColor(I)V

    .line 30566
    iget-object v1, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->E:Landroid/view/ViewGroup;

    iget-object v4, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->G:Landroid/view/View;

    new-instance v5, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v5, v6, p1}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v4, v6, v5}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    move v1, v3

    .line 30580
    :goto_75
    iget-object v4, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->G:Landroid/view/View;

    if-eqz v4, :cond_ac

    .line 30586
    :goto_79
    iget-boolean v4, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->n:Z

    if-nez v4, :cond_80

    if-eqz v3, :cond_80

    move p1, v2

    :cond_80
    move v7, v1

    move v1, v3

    move v3, v7

    .line 30596
    :goto_83
    if-eqz v3, :cond_8a

    .line 30597
    iget-object v3, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->u:Landroid/support/v7/internal/widget/ActionBarContextView;

    invoke-virtual {v3, v0}, Landroid/support/v7/internal/widget/ActionBarContextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_8a
    move v0, v1

    .line 30601
    :goto_8b
    iget-object v1, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->G:Landroid/view/View;

    if-eqz v1, :cond_96

    .line 30602
    iget-object v1, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->G:Landroid/view/View;

    if-eqz v0, :cond_b6

    :goto_93
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 90
    :cond_96
    return p1

    :cond_97
    move v1, v2

    .line 30557
    goto :goto_44

    .line 30570
    :cond_99
    iget-object v1, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->G:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 30571
    iget v4, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    if-eq v4, p1, :cond_aa

    .line 30572
    iput p1, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 30573
    iget-object v4, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->G:Landroid/view/View;

    invoke-virtual {v4, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_aa
    move v1, v3

    goto :goto_75

    :cond_ac
    move v3, v2

    .line 30580
    goto :goto_79

    .line 30591
    :cond_ae
    iget v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    if-eqz v1, :cond_b9

    .line 30593
    iput v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    move v1, v2

    goto :goto_83

    .line 30602
    :cond_b6
    const/16 v2, 0x8

    goto :goto_93

    :cond_b9
    move v3, v2

    move v1, v2

    goto :goto_83

    :cond_bc
    move v1, v2

    goto :goto_75

    :cond_be
    move v0, v2

    goto :goto_8b
.end method

.method private b(ILandroid/view/KeyEvent;)Z
    .registers 7

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 863
    sparse-switch p1, :sswitch_data_b8

    :cond_5
    move v0, v2

    .line 878
    :goto_6
    return v0

    .line 15362
    :sswitch_7
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->t:Landroid/support/v7/c/a;

    if-nez v0, :cond_50

    .line 15367
    invoke-virtual {p0, v2}, Landroid/support/v7/app/AppCompatDelegateImplV7;->f(I)Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;

    move-result-object v3

    .line 15368
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->s:Landroid/support/v7/internal/widget/ac;

    if-eqz v0, :cond_59

    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->s:Landroid/support/v7/internal/widget/ac;

    invoke-interface {v0}, Landroid/support/v7/internal/widget/ac;->c()Z

    move-result v0

    if-eqz v0, :cond_59

    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->e:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/view/du;->b(Landroid/view/ViewConfiguration;)Z

    move-result v0

    if-nez v0, :cond_59

    .line 15371
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->s:Landroid/support/v7/internal/widget/ac;

    invoke-interface {v0}, Landroid/support/v7/internal/widget/ac;->d()Z

    move-result v0

    if-nez v0, :cond_52

    .line 16193
    iget-boolean v0, p0, Landroid/support/v7/app/ak;->r:Z

    .line 15372
    if-nez v0, :cond_b3

    invoke-direct {p0, v3, p2}, Landroid/support/v7/app/AppCompatDelegateImplV7;->b(Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;Landroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_b3

    .line 15373
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->s:Landroid/support/v7/internal/widget/ac;

    invoke-interface {v0}, Landroid/support/v7/internal/widget/ac;->f()Z

    move-result v0

    .line 15402
    :goto_3f
    if-eqz v0, :cond_50

    .line 15403
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->e:Landroid/content/Context;

    const-string v3, "audio"

    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 15405
    if-eqz v0, :cond_7c

    .line 15406
    invoke-virtual {v0, v2}, Landroid/media/AudioManager;->playSoundEffect(I)V

    :cond_50
    :goto_50
    move v0, v1

    .line 866
    goto :goto_6

    .line 15376
    :cond_52
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->s:Landroid/support/v7/internal/widget/ac;

    invoke-interface {v0}, Landroid/support/v7/internal/widget/ac;->g()Z

    move-result v0

    goto :goto_3f

    .line 15379
    :cond_59
    iget-boolean v0, v3, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->o:Z

    if-nez v0, :cond_61

    iget-boolean v0, v3, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->n:Z

    if-eqz v0, :cond_67

    .line 15382
    :cond_61
    iget-boolean v0, v3, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->o:Z

    .line 15384
    invoke-virtual {p0, v3, v1}, Landroid/support/v7/app/AppCompatDelegateImplV7;->a(Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;Z)V

    goto :goto_3f

    .line 15385
    :cond_67
    iget-boolean v0, v3, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->m:Z

    if-eqz v0, :cond_b3

    .line 15387
    iget-boolean v0, v3, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->r:Z

    if-eqz v0, :cond_b5

    .line 15390
    iput-boolean v2, v3, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->m:Z

    .line 15391
    invoke-direct {p0, v3, p2}, Landroid/support/v7/app/AppCompatDelegateImplV7;->b(Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;Landroid/view/KeyEvent;)Z

    move-result v0

    .line 15394
    :goto_75
    if-eqz v0, :cond_b3

    .line 15396
    invoke-direct {p0, v3, p2}, Landroid/support/v7/app/AppCompatDelegateImplV7;->a(Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;Landroid/view/KeyEvent;)V

    move v0, v1

    .line 15397
    goto :goto_3f

    .line 15408
    :cond_7c
    const-string v0, "AppCompatDelegate"

    const-string v2, "Couldn\'t get audio manager"

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_50

    .line 868
    :sswitch_84
    invoke-virtual {p0, v2}, Landroid/support/v7/app/AppCompatDelegateImplV7;->f(I)Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;

    move-result-object v0

    .line 869
    if-eqz v0, :cond_94

    iget-boolean v3, v0, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->o:Z

    if-eqz v3, :cond_94

    .line 870
    invoke-virtual {p0, v0, v1}, Landroid/support/v7/app/AppCompatDelegateImplV7;->a(Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;Z)V

    move v0, v1

    .line 871
    goto/16 :goto_6

    .line 16794
    :cond_94
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->t:Landroid/support/v7/c/a;

    if-eqz v0, :cond_a3

    .line 16795
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->t:Landroid/support/v7/c/a;

    invoke-virtual {v0}, Landroid/support/v7/c/a;->c()V

    move v0, v1

    .line 873
    :goto_9e
    if-eqz v0, :cond_5

    move v0, v1

    .line 874
    goto/16 :goto_6

    .line 16800
    :cond_a3
    invoke-virtual {p0}, Landroid/support/v7/app/AppCompatDelegateImplV7;->a()Landroid/support/v7/app/a;

    move-result-object v0

    .line 16801
    if-eqz v0, :cond_b1

    invoke-virtual {v0}, Landroid/support/v7/app/a;->z()Z

    move-result v0

    if-eqz v0, :cond_b1

    move v0, v1

    .line 16802
    goto :goto_9e

    :cond_b1
    move v0, v2

    .line 16806
    goto :goto_9e

    :cond_b3
    move v0, v2

    goto :goto_3f

    :cond_b5
    move v0, v1

    goto :goto_75

    .line 863
    nop

    :sswitch_data_b8
    .sparse-switch
        0x4 -> :sswitch_84
        0x52 -> :sswitch_7
    .end sparse-switch
.end method

.method private b(Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;)Z
    .registers 8

    .prologue
    const/4 v5, 0x1

    .line 1133
    iget-object v1, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->e:Landroid/content/Context;

    .line 1136
    iget v0, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->a:I

    if-eqz v0, :cond_d

    iget v0, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->a:I

    const/16 v2, 0x6c

    if-ne v0, v2, :cond_71

    :cond_d
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->s:Landroid/support/v7/internal/widget/ac;

    if-eqz v0, :cond_71

    .line 1138
    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    .line 1139
    invoke-virtual {v1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v3

    .line 1140
    sget v0, Landroid/support/v7/a/d;->actionBarTheme:I

    invoke-virtual {v3, v0, v2, v5}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 1142
    const/4 v0, 0x0

    .line 1143
    iget v4, v2, Landroid/util/TypedValue;->resourceId:I

    if-eqz v4, :cond_6b

    .line 1144
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->newTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    .line 1145
    invoke-virtual {v0, v3}, Landroid/content/res/Resources$Theme;->setTo(Landroid/content/res/Resources$Theme;)V

    .line 1146
    iget v4, v2, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {v0, v4, v5}, Landroid/content/res/Resources$Theme;->applyStyle(IZ)V

    .line 1147
    sget v4, Landroid/support/v7/a/d;->actionBarWidgetTheme:I

    invoke-virtual {v0, v4, v2, v5}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 1154
    :goto_39
    iget v4, v2, Landroid/util/TypedValue;->resourceId:I

    if-eqz v4, :cond_4f

    .line 1155
    if-nez v0, :cond_4a

    .line 1156
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->newTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    .line 1157
    invoke-virtual {v0, v3}, Landroid/content/res/Resources$Theme;->setTo(Landroid/content/res/Resources$Theme;)V

    .line 1159
    :cond_4a
    iget v2, v2, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {v0, v2, v5}, Landroid/content/res/Resources$Theme;->applyStyle(IZ)V

    :cond_4f
    move-object v2, v0

    .line 1162
    if-eqz v2, :cond_71

    .line 1163
    new-instance v0, Landroid/support/v7/internal/view/b;

    const/4 v3, 0x0

    invoke-direct {v0, v1, v3}, Landroid/support/v7/internal/view/b;-><init>(Landroid/content/Context;I)V

    .line 1164
    invoke-virtual {v0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources$Theme;->setTo(Landroid/content/res/Resources$Theme;)V

    .line 1168
    :goto_5f
    new-instance v1, Landroid/support/v7/internal/view/menu/i;

    invoke-direct {v1, v0}, Landroid/support/v7/internal/view/menu/i;-><init>(Landroid/content/Context;)V

    .line 1169
    invoke-virtual {v1, p0}, Landroid/support/v7/internal/view/menu/i;->a(Landroid/support/v7/internal/view/menu/j;)V

    .line 1170
    invoke-virtual {p1, v1}, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->a(Landroid/support/v7/internal/view/menu/i;)V

    .line 1172
    return v5

    .line 1150
    :cond_6b
    sget v4, Landroid/support/v7/a/d;->actionBarWidgetTheme:I

    invoke-virtual {v3, v4, v2, v5}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    goto :goto_39

    :cond_71
    move-object v0, v1

    goto :goto_5f
.end method

.method private b(Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;Landroid/view/KeyEvent;)Z
    .registers 13

    .prologue
    const/16 v5, 0x6c

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 1197
    .line 26193
    iget-boolean v0, p0, Landroid/support/v7/app/ak;->r:Z

    .line 1197
    if-eqz v0, :cond_a

    .line 1297
    :cond_9
    :goto_9
    return v4

    .line 1202
    :cond_a
    iget-boolean v0, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->m:Z

    if-eqz v0, :cond_10

    move v4, v3

    .line 1203
    goto :goto_9

    .line 1206
    :cond_10
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->L:Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;

    if-eqz v0, :cond_1d

    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->L:Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;

    if-eq v0, p1, :cond_1d

    .line 1208
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->L:Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;

    invoke-virtual {p0, v0, v4}, Landroid/support/v7/app/AppCompatDelegateImplV7;->a(Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;Z)V

    .line 26197
    :cond_1d
    iget-object v0, p0, Landroid/support/v7/app/ak;->f:Landroid/view/Window;

    invoke-virtual {v0}, Landroid/view/Window;->getCallback()Landroid/view/Window$Callback;

    move-result-object v7

    .line 1213
    if-eqz v7, :cond_2d

    .line 1214
    iget v0, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->a:I

    invoke-interface {v7, v0}, Landroid/view/Window$Callback;->onCreatePanelView(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->i:Landroid/view/View;

    .line 1217
    :cond_2d
    iget v0, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->a:I

    if-eqz v0, :cond_35

    iget v0, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->a:I

    if-ne v0, v5, :cond_fd

    :cond_35
    move v6, v3

    .line 1220
    :goto_36
    if-eqz v6, :cond_41

    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->s:Landroid/support/v7/internal/widget/ac;

    if-eqz v0, :cond_41

    .line 1223
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->s:Landroid/support/v7/internal/widget/ac;

    invoke-interface {v0}, Landroid/support/v7/internal/widget/ac;->h()V

    .line 1226
    :cond_41
    iget-object v0, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->i:Landroid/view/View;

    if-nez v0, :cond_158

    if-eqz v6, :cond_4d

    .line 27092
    iget-object v0, p0, Landroid/support/v7/app/ak;->j:Landroid/support/v7/app/a;

    .line 1226
    instance-of v0, v0, Landroid/support/v7/internal/a/e;

    if-nez v0, :cond_158

    .line 1230
    :cond_4d
    iget-object v0, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->j:Landroid/support/v7/internal/view/menu/i;

    if-eqz v0, :cond_55

    iget-boolean v0, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->r:Z

    if-eqz v0, :cond_109

    .line 1231
    :cond_55
    iget-object v0, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->j:Landroid/support/v7/internal/view/menu/i;

    if-nez v0, :cond_c2

    .line 27133
    iget-object v2, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->e:Landroid/content/Context;

    .line 27136
    iget v0, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->a:I

    if-eqz v0, :cond_63

    iget v0, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->a:I

    if-ne v0, v5, :cond_165

    :cond_63
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->s:Landroid/support/v7/internal/widget/ac;

    if-eqz v0, :cond_165

    .line 27138
    new-instance v5, Landroid/util/TypedValue;

    invoke-direct {v5}, Landroid/util/TypedValue;-><init>()V

    .line 27139
    invoke-virtual {v2}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v8

    .line 27140
    sget v0, Landroid/support/v7/a/d;->actionBarTheme:I

    invoke-virtual {v8, v0, v5, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 27143
    iget v0, v5, Landroid/util/TypedValue;->resourceId:I

    if-eqz v0, :cond_100

    .line 27144
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->newTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    .line 27145
    invoke-virtual {v0, v8}, Landroid/content/res/Resources$Theme;->setTo(Landroid/content/res/Resources$Theme;)V

    .line 27146
    iget v9, v5, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {v0, v9, v3}, Landroid/content/res/Resources$Theme;->applyStyle(IZ)V

    .line 27147
    sget v9, Landroid/support/v7/a/d;->actionBarWidgetTheme:I

    invoke-virtual {v0, v9, v5, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 27154
    :goto_8e
    iget v9, v5, Landroid/util/TypedValue;->resourceId:I

    if-eqz v9, :cond_a4

    .line 27155
    if-nez v0, :cond_9f

    .line 27156
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->newTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    .line 27157
    invoke-virtual {v0, v8}, Landroid/content/res/Resources$Theme;->setTo(Landroid/content/res/Resources$Theme;)V

    .line 27159
    :cond_9f
    iget v5, v5, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {v0, v5, v3}, Landroid/content/res/Resources$Theme;->applyStyle(IZ)V

    :cond_a4
    move-object v5, v0

    .line 27162
    if-eqz v5, :cond_165

    .line 27163
    new-instance v0, Landroid/support/v7/internal/view/b;

    invoke-direct {v0, v2, v4}, Landroid/support/v7/internal/view/b;-><init>(Landroid/content/Context;I)V

    .line 27164
    invoke-virtual {v0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources$Theme;->setTo(Landroid/content/res/Resources$Theme;)V

    .line 27168
    :goto_b3
    new-instance v2, Landroid/support/v7/internal/view/menu/i;

    invoke-direct {v2, v0}, Landroid/support/v7/internal/view/menu/i;-><init>(Landroid/content/Context;)V

    .line 27169
    invoke-virtual {v2, p0}, Landroid/support/v7/internal/view/menu/i;->a(Landroid/support/v7/internal/view/menu/j;)V

    .line 27170
    invoke-virtual {p1, v2}, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->a(Landroid/support/v7/internal/view/menu/i;)V

    .line 1232
    iget-object v0, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->j:Landroid/support/v7/internal/view/menu/i;

    if-eqz v0, :cond_9

    .line 1237
    :cond_c2
    if-eqz v6, :cond_dc

    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->s:Landroid/support/v7/internal/widget/ac;

    if-eqz v0, :cond_dc

    .line 1238
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->A:Landroid/support/v7/app/az;

    if-nez v0, :cond_d3

    .line 1239
    new-instance v0, Landroid/support/v7/app/az;

    invoke-direct {v0, p0, v4}, Landroid/support/v7/app/az;-><init>(Landroid/support/v7/app/AppCompatDelegateImplV7;B)V

    iput-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->A:Landroid/support/v7/app/az;

    .line 1241
    :cond_d3
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->s:Landroid/support/v7/internal/widget/ac;

    iget-object v2, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->j:Landroid/support/v7/internal/view/menu/i;

    iget-object v5, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->A:Landroid/support/v7/app/az;

    invoke-interface {v0, v2, v5}, Landroid/support/v7/internal/widget/ac;->a(Landroid/view/Menu;Landroid/support/v7/internal/view/menu/y;)V

    .line 1246
    :cond_dc
    iget-object v0, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->j:Landroid/support/v7/internal/view/menu/i;

    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/i;->d()V

    .line 1247
    iget v0, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->a:I

    iget-object v2, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->j:Landroid/support/v7/internal/view/menu/i;

    invoke-interface {v7, v0, v2}, Landroid/view/Window$Callback;->onCreatePanelMenu(ILandroid/view/Menu;)Z

    move-result v0

    if-nez v0, :cond_107

    .line 1249
    invoke-virtual {p1, v1}, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->a(Landroid/support/v7/internal/view/menu/i;)V

    .line 1251
    if-eqz v6, :cond_9

    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->s:Landroid/support/v7/internal/widget/ac;

    if-eqz v0, :cond_9

    .line 1253
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->s:Landroid/support/v7/internal/widget/ac;

    iget-object v2, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->A:Landroid/support/v7/app/az;

    invoke-interface {v0, v1, v2}, Landroid/support/v7/internal/widget/ac;->a(Landroid/view/Menu;Landroid/support/v7/internal/view/menu/y;)V

    goto/16 :goto_9

    :cond_fd
    move v6, v4

    .line 1217
    goto/16 :goto_36

    .line 27150
    :cond_100
    sget v0, Landroid/support/v7/a/d;->actionBarWidgetTheme:I

    invoke-virtual {v8, v0, v5, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    move-object v0, v1

    goto :goto_8e

    .line 1259
    :cond_107
    iput-boolean v4, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->r:Z

    .line 1264
    :cond_109
    iget-object v0, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->j:Landroid/support/v7/internal/view/menu/i;

    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/i;->d()V

    .line 1268
    iget-object v0, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->u:Landroid/os/Bundle;

    if-eqz v0, :cond_11b

    .line 1269
    iget-object v0, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->j:Landroid/support/v7/internal/view/menu/i;

    iget-object v2, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->u:Landroid/os/Bundle;

    invoke-virtual {v0, v2}, Landroid/support/v7/internal/view/menu/i;->d(Landroid/os/Bundle;)V

    .line 1270
    iput-object v1, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->u:Landroid/os/Bundle;

    .line 1274
    :cond_11b
    iget-object v0, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->i:Landroid/view/View;

    iget-object v2, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->j:Landroid/support/v7/internal/view/menu/i;

    invoke-interface {v7, v4, v0, v2}, Landroid/view/Window$Callback;->onPreparePanel(ILandroid/view/View;Landroid/view/Menu;)Z

    move-result v0

    if-nez v0, :cond_139

    .line 1275
    if-eqz v6, :cond_132

    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->s:Landroid/support/v7/internal/widget/ac;

    if-eqz v0, :cond_132

    .line 1278
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->s:Landroid/support/v7/internal/widget/ac;

    iget-object v2, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->A:Landroid/support/v7/app/az;

    invoke-interface {v0, v1, v2}, Landroid/support/v7/internal/widget/ac;->a(Landroid/view/Menu;Landroid/support/v7/internal/view/menu/y;)V

    .line 1280
    :cond_132
    iget-object v0, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->j:Landroid/support/v7/internal/view/menu/i;

    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/i;->e()V

    goto/16 :goto_9

    .line 1285
    :cond_139
    if-eqz p2, :cond_161

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getDeviceId()I

    move-result v0

    :goto_13f
    invoke-static {v0}, Landroid/view/KeyCharacterMap;->load(I)Landroid/view/KeyCharacterMap;

    move-result-object v0

    .line 1287
    invoke-virtual {v0}, Landroid/view/KeyCharacterMap;->getKeyboardType()I

    move-result v0

    if-eq v0, v3, :cond_163

    move v0, v3

    :goto_14a
    iput-boolean v0, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->p:Z

    .line 1288
    iget-object v0, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->j:Landroid/support/v7/internal/view/menu/i;

    iget-boolean v1, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->p:Z

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/view/menu/i;->setQwertyMode(Z)V

    .line 1289
    iget-object v0, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->j:Landroid/support/v7/internal/view/menu/i;

    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/i;->e()V

    .line 1293
    :cond_158
    iput-boolean v3, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->m:Z

    .line 1294
    iput-boolean v4, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->n:Z

    .line 1295
    iput-object p1, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->L:Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;

    move v4, v3

    .line 1297
    goto/16 :goto_9

    .line 1285
    :cond_161
    const/4 v0, -0x1

    goto :goto_13f

    :cond_163
    move v0, v4

    .line 1287
    goto :goto_14a

    :cond_165
    move-object v0, v2

    goto/16 :goto_b3
.end method

.method private static synthetic b(Landroid/support/v7/app/AppCompatDelegateImplV7;)Z
    .registers 2

    .prologue
    .line 90
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->y:Z

    return v0
.end method

.method private b(Landroid/view/KeyEvent;)Z
    .registers 5

    .prologue
    const/4 v0, 0x0

    .line 1351
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v1

    if-nez v1, :cond_13

    .line 1352
    invoke-virtual {p0, v0}, Landroid/support/v7/app/AppCompatDelegateImplV7;->f(I)Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;

    move-result-object v1

    .line 1353
    iget-boolean v2, v1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->o:Z

    if-nez v2, :cond_13

    .line 1354
    invoke-direct {p0, v1, p1}, Landroid/support/v7/app/AppCompatDelegateImplV7;->b(Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;Landroid/view/KeyEvent;)Z

    move-result v0

    .line 1358
    :cond_13
    return v0
.end method

.method private static synthetic c(Landroid/support/v7/app/AppCompatDelegateImplV7;)I
    .registers 2

    .prologue
    .line 90
    const/4 v0, 0x0

    iput v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->z:I

    return v0
.end method

.method private c(ILandroid/view/KeyEvent;)Z
    .registers 6

    .prologue
    const/4 v2, 0x0

    .line 882
    packed-switch p1, :pswitch_data_20

    .line 891
    :cond_4
    :goto_4
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_d

    .line 894
    invoke-virtual {p0, p1, p2}, Landroid/support/v7/app/AppCompatDelegateImplV7;->a(ILandroid/view/KeyEvent;)Z

    .line 896
    :cond_d
    return v2

    .line 17351
    :pswitch_e
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v0

    if-nez v0, :cond_4

    .line 17352
    invoke-virtual {p0, v2}, Landroid/support/v7/app/AppCompatDelegateImplV7;->f(I)Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;

    move-result-object v0

    .line 17353
    iget-boolean v1, v0, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->o:Z

    if-nez v1, :cond_4

    .line 17354
    invoke-direct {p0, v0, p2}, Landroid/support/v7/app/AppCompatDelegateImplV7;->b(Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;Landroid/view/KeyEvent;)Z

    goto :goto_4

    .line 882
    :pswitch_data_20
    .packed-switch 0x52
        :pswitch_e
    .end packed-switch
.end method

.method private c(Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;)Z
    .registers 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1176
    iget-object v0, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->i:Landroid/view/View;

    if-eqz v0, :cond_c

    .line 1177
    iget-object v0, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->i:Landroid/view/View;

    iput-object v0, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->h:Landroid/view/View;

    move v0, v1

    .line 1193
    :goto_b
    return v0

    .line 1181
    :cond_c
    iget-object v0, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->j:Landroid/support/v7/internal/view/menu/i;

    if-nez v0, :cond_12

    move v0, v2

    .line 1182
    goto :goto_b

    .line 1185
    :cond_12
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->B:Landroid/support/v7/app/be;

    if-nez v0, :cond_1d

    .line 1186
    new-instance v0, Landroid/support/v7/app/be;

    invoke-direct {v0, p0, v2}, Landroid/support/v7/app/be;-><init>(Landroid/support/v7/app/AppCompatDelegateImplV7;B)V

    iput-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->B:Landroid/support/v7/app/be;

    .line 1189
    :cond_1d
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->B:Landroid/support/v7/app/be;

    .line 25865
    iget-object v3, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->j:Landroid/support/v7/internal/view/menu/i;

    if-nez v3, :cond_2e

    const/4 v0, 0x0

    .line 1191
    :goto_24
    check-cast v0, Landroid/view/View;

    iput-object v0, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->h:Landroid/view/View;

    .line 1193
    iget-object v0, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->h:Landroid/view/View;

    if-eqz v0, :cond_51

    move v0, v1

    goto :goto_b

    .line 25867
    :cond_2e
    iget-object v3, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->k:Landroid/support/v7/internal/view/menu/g;

    if-nez v3, :cond_48

    .line 25868
    new-instance v3, Landroid/support/v7/internal/view/menu/g;

    iget-object v4, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->l:Landroid/content/Context;

    sget v5, Landroid/support/v7/a/k;->abc_list_menu_item_layout:I

    invoke-direct {v3, v4, v5}, Landroid/support/v7/internal/view/menu/g;-><init>(Landroid/content/Context;I)V

    iput-object v3, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->k:Landroid/support/v7/internal/view/menu/g;

    .line 25870
    iget-object v3, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->k:Landroid/support/v7/internal/view/menu/g;

    .line 26134
    iput-object v0, v3, Landroid/support/v7/internal/view/menu/g;->g:Landroid/support/v7/internal/view/menu/y;

    .line 25871
    iget-object v0, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->j:Landroid/support/v7/internal/view/menu/i;

    iget-object v3, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->k:Landroid/support/v7/internal/view/menu/g;

    invoke-virtual {v0, v3}, Landroid/support/v7/internal/view/menu/i;->a(Landroid/support/v7/internal/view/menu/x;)V

    .line 25874
    :cond_48
    iget-object v0, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->k:Landroid/support/v7/internal/view/menu/g;

    iget-object v3, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->g:Landroid/view/ViewGroup;

    invoke-virtual {v0, v3}, Landroid/support/v7/internal/view/menu/g;->a(Landroid/view/ViewGroup;)Landroid/support/v7/internal/view/menu/z;

    move-result-object v0

    goto :goto_24

    :cond_51
    move v0, v2

    .line 1193
    goto :goto_b
.end method

.method private c(Landroid/view/KeyEvent;)Z
    .registers 6

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1362
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->t:Landroid/support/v7/c/a;

    if-eqz v0, :cond_8

    move v0, v1

    .line 1411
    :goto_7
    return v0

    .line 1367
    :cond_8
    invoke-virtual {p0, v1}, Landroid/support/v7/app/AppCompatDelegateImplV7;->f(I)Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;

    move-result-object v3

    .line 1368
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->s:Landroid/support/v7/internal/widget/ac;

    if-eqz v0, :cond_56

    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->s:Landroid/support/v7/internal/widget/ac;

    invoke-interface {v0}, Landroid/support/v7/internal/widget/ac;->c()Z

    move-result v0

    if-eqz v0, :cond_56

    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->e:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/view/du;->b(Landroid/view/ViewConfiguration;)Z

    move-result v0

    if-nez v0, :cond_56

    .line 1371
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->s:Landroid/support/v7/internal/widget/ac;

    invoke-interface {v0}, Landroid/support/v7/internal/widget/ac;->d()Z

    move-result v0

    if-nez v0, :cond_4f

    .line 29193
    iget-boolean v0, p0, Landroid/support/v7/app/ak;->r:Z

    .line 1372
    if-nez v0, :cond_81

    invoke-direct {p0, v3, p1}, Landroid/support/v7/app/AppCompatDelegateImplV7;->b(Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;Landroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_81

    .line 1373
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->s:Landroid/support/v7/internal/widget/ac;

    invoke-interface {v0}, Landroid/support/v7/internal/widget/ac;->f()Z

    move-result v2

    .line 1402
    :goto_3c
    if-eqz v2, :cond_4d

    .line 1403
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->e:Landroid/content/Context;

    const-string v3, "audio"

    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 1405
    if-eqz v0, :cond_79

    .line 1406
    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->playSoundEffect(I)V

    :cond_4d
    :goto_4d
    move v0, v2

    .line 1411
    goto :goto_7

    .line 1376
    :cond_4f
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->s:Landroid/support/v7/internal/widget/ac;

    invoke-interface {v0}, Landroid/support/v7/internal/widget/ac;->g()Z

    move-result v2

    goto :goto_3c

    .line 1379
    :cond_56
    iget-boolean v0, v3, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->o:Z

    if-nez v0, :cond_5e

    iget-boolean v0, v3, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->n:Z

    if-eqz v0, :cond_65

    .line 1382
    :cond_5e
    iget-boolean v0, v3, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->o:Z

    .line 1384
    invoke-virtual {p0, v3, v2}, Landroid/support/v7/app/AppCompatDelegateImplV7;->a(Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;Z)V

    move v2, v0

    goto :goto_3c

    .line 1385
    :cond_65
    iget-boolean v0, v3, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->m:Z

    if-eqz v0, :cond_81

    .line 1387
    iget-boolean v0, v3, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->r:Z

    if-eqz v0, :cond_83

    .line 1390
    iput-boolean v1, v3, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->m:Z

    .line 1391
    invoke-direct {p0, v3, p1}, Landroid/support/v7/app/AppCompatDelegateImplV7;->b(Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;Landroid/view/KeyEvent;)Z

    move-result v0

    .line 1394
    :goto_73
    if-eqz v0, :cond_81

    .line 1396
    invoke-direct {p0, v3, p1}, Landroid/support/v7/app/AppCompatDelegateImplV7;->a(Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;Landroid/view/KeyEvent;)V

    goto :goto_3c

    .line 1408
    :cond_79
    const-string v0, "AppCompatDelegate"

    const-string v1, "Couldn\'t get audio manager"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4d

    :cond_81
    move v2, v1

    goto :goto_3c

    :cond_83
    move v0, v2

    goto :goto_73
.end method

.method private static synthetic d(Landroid/support/v7/app/AppCompatDelegateImplV7;)V
    .registers 1

    .prologue
    .line 90
    invoke-virtual {p0}, Landroid/support/v7/app/AppCompatDelegateImplV7;->n()V

    return-void
.end method

.method private static synthetic e(Landroid/support/v7/app/AppCompatDelegateImplV7;)V
    .registers 3

    .prologue
    .line 90
    .line 31315
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/support/v7/app/AppCompatDelegateImplV7;->f(I)Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Landroid/support/v7/app/AppCompatDelegateImplV7;->a(Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;Z)V

    .line 90
    return-void
.end method

.method private g(I)V
    .registers 4

    .prologue
    .line 1315
    invoke-virtual {p0, p1}, Landroid/support/v7/app/AppCompatDelegateImplV7;->f(I)Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Landroid/support/v7/app/AppCompatDelegateImplV7;->a(Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;Z)V

    .line 1316
    return-void
.end method

.method private h(I)V
    .registers 5

    .prologue
    const/4 v2, 0x1

    .line 1497
    iget v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->z:I

    shl-int v1, v2, p1

    or-int/2addr v0, v1

    iput v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->z:I

    .line 1499
    iget-boolean v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->y:Z

    if-nez v0, :cond_19

    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->D:Landroid/view/ViewGroup;

    if-eqz v0, :cond_19

    .line 1500
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->D:Landroid/view/ViewGroup;

    iget-object v1, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->M:Ljava/lang/Runnable;

    invoke-static {v0, v1}, Landroid/support/v4/view/cx;->a(Landroid/view/View;Ljava/lang/Runnable;)V

    .line 1501
    iput-boolean v2, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->y:Z

    .line 1503
    :cond_19
    return-void
.end method

.method private i(I)V
    .registers 7

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1506
    invoke-virtual {p0, p1}, Landroid/support/v7/app/AppCompatDelegateImplV7;->f(I)Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;

    move-result-object v0

    .line 1508
    iget-object v1, v0, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->j:Landroid/support/v7/internal/view/menu/i;

    if-eqz v1, :cond_26

    .line 1509
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1510
    iget-object v2, v0, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->j:Landroid/support/v7/internal/view/menu/i;

    invoke-virtual {v2, v1}, Landroid/support/v7/internal/view/menu/i;->c(Landroid/os/Bundle;)V

    .line 1511
    invoke-virtual {v1}, Landroid/os/Bundle;->size()I

    move-result v2

    if-lez v2, :cond_1c

    .line 1512
    iput-object v1, v0, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->u:Landroid/os/Bundle;

    .line 1515
    :cond_1c
    iget-object v1, v0, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->j:Landroid/support/v7/internal/view/menu/i;

    invoke-virtual {v1}, Landroid/support/v7/internal/view/menu/i;->d()V

    .line 1516
    iget-object v1, v0, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->j:Landroid/support/v7/internal/view/menu/i;

    invoke-virtual {v1}, Landroid/support/v7/internal/view/menu/i;->clear()V

    .line 1518
    :cond_26
    iput-boolean v4, v0, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->r:Z

    .line 1519
    iput-boolean v4, v0, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->q:Z

    .line 1522
    const/16 v0, 0x6c

    if-eq p1, v0, :cond_30

    if-nez p1, :cond_40

    :cond_30
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->s:Landroid/support/v7/internal/widget/ac;

    if-eqz v0, :cond_40

    .line 1524
    invoke-virtual {p0, v3}, Landroid/support/v7/app/AppCompatDelegateImplV7;->f(I)Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;

    move-result-object v0

    .line 1525
    if-eqz v0, :cond_40

    .line 1526
    iput-boolean v3, v0, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->m:Z

    .line 1527
    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Landroid/support/v7/app/AppCompatDelegateImplV7;->b(Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;Landroid/view/KeyEvent;)Z

    .line 1530
    :cond_40
    return-void
.end method

.method private j(I)I
    .registers 10

    .prologue
    const/4 v6, -0x1

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1539
    .line 1541
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->u:Landroid/support/v7/internal/widget/ActionBarContextView;

    if-eqz v0, :cond_be

    .line 1542
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->u:Landroid/support/v7/internal/widget/ActionBarContextView;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarContextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    instance-of v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;

    if-eqz v0, :cond_be

    .line 1543
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->u:Landroid/support/v7/internal/widget/ActionBarContextView;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarContextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 1547
    iget-object v1, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->u:Landroid/support/v7/internal/widget/ActionBarContextView;

    invoke-virtual {v1}, Landroid/support/v7/internal/widget/ActionBarContextView;->isShown()Z

    move-result v1

    if-eqz v1, :cond_ae

    .line 1548
    iget-object v1, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->O:Landroid/graphics/Rect;

    if-nez v1, :cond_33

    .line 1549
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->O:Landroid/graphics/Rect;

    .line 1550
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->P:Landroid/graphics/Rect;

    .line 1552
    :cond_33
    iget-object v1, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->O:Landroid/graphics/Rect;

    .line 1553
    iget-object v4, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->P:Landroid/graphics/Rect;

    .line 1554
    invoke-virtual {v1, v2, p1, v2, v2}, Landroid/graphics/Rect;->set(IIII)V

    .line 1556
    iget-object v5, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->E:Landroid/view/ViewGroup;

    invoke-static {v5, v1, v4}, Landroid/support/v7/internal/widget/bd;->a(Landroid/view/View;Landroid/graphics/Rect;Landroid/graphics/Rect;)V

    .line 1557
    iget v1, v4, Landroid/graphics/Rect;->top:I

    if-nez v1, :cond_97

    move v1, p1

    .line 1558
    :goto_44
    iget v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    if-eq v4, v1, :cond_bc

    .line 1560
    iput p1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 1562
    iget-object v1, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->G:Landroid/view/View;

    if-nez v1, :cond_99

    .line 1563
    new-instance v1, Landroid/view/View;

    iget-object v4, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->e:Landroid/content/Context;

    invoke-direct {v1, v4}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->G:Landroid/view/View;

    .line 1564
    iget-object v1, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->G:Landroid/view/View;

    iget-object v4, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->e:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Landroid/support/v7/a/f;->abc_input_method_navigation_guard:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v1, v4}, Landroid/view/View;->setBackgroundColor(I)V

    .line 1566
    iget-object v1, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->E:Landroid/view/ViewGroup;

    iget-object v4, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->G:Landroid/view/View;

    new-instance v5, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v5, v6, p1}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v4, v6, v5}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    move v1, v3

    .line 1580
    :goto_75
    iget-object v4, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->G:Landroid/view/View;

    if-eqz v4, :cond_ac

    .line 1586
    :goto_79
    iget-boolean v4, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->n:Z

    if-nez v4, :cond_80

    if-eqz v3, :cond_80

    move p1, v2

    :cond_80
    move v7, v1

    move v1, v3

    move v3, v7

    .line 1596
    :goto_83
    if-eqz v3, :cond_8a

    .line 1597
    iget-object v3, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->u:Landroid/support/v7/internal/widget/ActionBarContextView;

    invoke-virtual {v3, v0}, Landroid/support/v7/internal/widget/ActionBarContextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_8a
    move v0, v1

    .line 1601
    :goto_8b
    iget-object v1, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->G:Landroid/view/View;

    if-eqz v1, :cond_96

    .line 1602
    iget-object v1, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->G:Landroid/view/View;

    if-eqz v0, :cond_b6

    :goto_93
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1605
    :cond_96
    return p1

    :cond_97
    move v1, v2

    .line 1557
    goto :goto_44

    .line 1570
    :cond_99
    iget-object v1, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->G:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 1571
    iget v4, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    if-eq v4, p1, :cond_aa

    .line 1572
    iput p1, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1573
    iget-object v4, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->G:Landroid/view/View;

    invoke-virtual {v4, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_aa
    move v1, v3

    goto :goto_75

    :cond_ac
    move v3, v2

    .line 1580
    goto :goto_79

    .line 1591
    :cond_ae
    iget v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    if-eqz v1, :cond_b9

    .line 1593
    iput v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    move v1, v2

    goto :goto_83

    .line 1602
    :cond_b6
    const/16 v2, 0x8

    goto :goto_93

    :cond_b9
    move v3, v2

    move v1, v2

    goto :goto_83

    :cond_bc
    move v1, v2

    goto :goto_75

    :cond_be
    move v0, v2

    goto :goto_8b
.end method

.method private static k(I)I
    .registers 3

    .prologue
    .line 1616
    const/16 v0, 0x8

    if-ne p0, v0, :cond_e

    .line 1617
    const-string v0, "AppCompatDelegate"

    const-string v1, "You should now use the AppCompatDelegate.FEATURE_SUPPORT_ACTION_BAR id when requesting this feature."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1619
    const/16 p0, 0x6c

    .line 1626
    :cond_d
    :goto_d
    return p0

    .line 1620
    :cond_e
    const/16 v0, 0x9

    if-ne p0, v0, :cond_d

    .line 1621
    const-string v0, "AppCompatDelegate"

    const-string v1, "You should now use the AppCompatDelegate.FEATURE_SUPPORT_ACTION_BAR_OVERLAY id when requesting this feature."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1623
    const/16 p0, 0x6d

    goto :goto_d
.end method

.method private o()V
    .registers 10

    .prologue
    const/16 v8, 0x6c

    const v5, 0x1020002

    const/4 v7, 0x1

    const/4 v3, 0x0

    const/4 v6, 0x0

    .line 277
    iget-boolean v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->C:Z

    if-nez v0, :cond_270

    .line 3305
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->e:Landroid/content/Context;

    sget-object v1, Landroid/support/v7/a/n;->Theme:[I

    invoke-virtual {v0, v1}, Landroid/content/Context;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 3307
    sget v1, Landroid/support/v7/a/n;->Theme_windowActionBar:I

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v1

    if-nez v1, :cond_27

    .line 3308
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 3309
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "You need to use a Theme.AppCompat theme (or descendant) with this activity."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 3313
    :cond_27
    sget v1, Landroid/support/v7/a/n;->Theme_windowNoTitle:I

    invoke-virtual {v0, v1, v6}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    if-eqz v1, :cond_c1

    .line 3314
    invoke-virtual {p0, v7}, Landroid/support/v7/app/AppCompatDelegateImplV7;->b(I)Z

    .line 3319
    :cond_32
    :goto_32
    sget v1, Landroid/support/v7/a/n;->Theme_windowActionBarOverlay:I

    invoke-virtual {v0, v1, v6}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    if-eqz v1, :cond_3f

    .line 3320
    const/16 v1, 0x6d

    invoke-virtual {p0, v1}, Landroid/support/v7/app/AppCompatDelegateImplV7;->b(I)Z

    .line 3322
    :cond_3f
    sget v1, Landroid/support/v7/a/n;->Theme_windowActionModeOverlay:I

    invoke-virtual {v0, v1, v6}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    if-eqz v1, :cond_4c

    .line 3323
    const/16 v1, 0xa

    invoke-virtual {p0, v1}, Landroid/support/v7/app/AppCompatDelegateImplV7;->b(I)Z

    .line 3325
    :cond_4c
    sget v1, Landroid/support/v7/a/n;->Theme_android_windowIsFloating:I

    invoke-virtual {v0, v1, v6}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->o:Z

    .line 3326
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 3328
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->e:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 3332
    iget-boolean v1, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->p:Z

    if-nez v1, :cond_135

    .line 3333
    iget-boolean v1, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->o:Z

    if-eqz v1, :cond_ce

    .line 3335
    sget v1, Landroid/support/v7/a/k;->abc_dialog_title_material:I

    invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 3339
    iput-boolean v6, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->m:Z

    iput-boolean v6, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->l:Z

    move-object v2, v0

    .line 3420
    :goto_72
    if-nez v2, :cond_16b

    .line 3421
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "AppCompat does not support the current theme features: { windowActionBar: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->l:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", windowActionBarOverlay: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->m:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", android:windowIsFloating: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->o:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", windowActionModeOverlay: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->n:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", windowNoTitle: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->p:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " }"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 3315
    :cond_c1
    sget v1, Landroid/support/v7/a/n;->Theme_windowActionBar:I

    invoke-virtual {v0, v1, v6}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    if-eqz v1, :cond_32

    .line 3317
    invoke-virtual {p0, v8}, Landroid/support/v7/app/AppCompatDelegateImplV7;->b(I)Z

    goto/16 :goto_32

    .line 3340
    :cond_ce
    iget-boolean v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->l:Z

    if-eqz v0, :cond_275

    .line 3346
    new-instance v1, Landroid/util/TypedValue;

    invoke-direct {v1}, Landroid/util/TypedValue;-><init>()V

    .line 3347
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->e:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    sget v2, Landroid/support/v7/a/d;->actionBarTheme:I

    invoke-virtual {v0, v2, v1, v7}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 3350
    iget v0, v1, Landroid/util/TypedValue;->resourceId:I

    if-eqz v0, :cond_132

    .line 3351
    new-instance v0, Landroid/support/v7/internal/view/b;

    iget-object v2, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->e:Landroid/content/Context;

    iget v1, v1, Landroid/util/TypedValue;->resourceId:I

    invoke-direct {v0, v2, v1}, Landroid/support/v7/internal/view/b;-><init>(Landroid/content/Context;I)V

    .line 3357
    :goto_ef
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Landroid/support/v7/a/k;->abc_screen_toolbar:I

    invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 3360
    sget v1, Landroid/support/v7/a/i;->decor_content_parent:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/v7/internal/widget/ac;

    iput-object v1, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->s:Landroid/support/v7/internal/widget/ac;

    .line 3362
    iget-object v1, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->s:Landroid/support/v7/internal/widget/ac;

    .line 4197
    iget-object v2, p0, Landroid/support/v7/app/ak;->f:Landroid/view/Window;

    invoke-virtual {v2}, Landroid/view/Window;->getCallback()Landroid/view/Window$Callback;

    move-result-object v2

    .line 3362
    invoke-interface {v1, v2}, Landroid/support/v7/internal/widget/ac;->setWindowCallback(Landroid/view/Window$Callback;)V

    .line 3367
    iget-boolean v1, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->m:Z

    if-eqz v1, :cond_11b

    .line 3368
    iget-object v1, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->s:Landroid/support/v7/internal/widget/ac;

    const/16 v2, 0x6d

    invoke-interface {v1, v2}, Landroid/support/v7/internal/widget/ac;->a(I)V

    .line 3370
    :cond_11b
    iget-boolean v1, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->H:Z

    if-eqz v1, :cond_125

    .line 3371
    iget-object v1, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->s:Landroid/support/v7/internal/widget/ac;

    const/4 v2, 0x2

    invoke-interface {v1, v2}, Landroid/support/v7/internal/widget/ac;->a(I)V

    .line 3373
    :cond_125
    iget-boolean v1, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->I:Z

    if-eqz v1, :cond_12f

    .line 3374
    iget-object v1, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->s:Landroid/support/v7/internal/widget/ac;

    const/4 v2, 0x5

    invoke-interface {v1, v2}, Landroid/support/v7/internal/widget/ac;->a(I)V

    :cond_12f
    move-object v2, v0

    .line 3376
    goto/16 :goto_72

    .line 3353
    :cond_132
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->e:Landroid/content/Context;

    goto :goto_ef

    .line 3378
    :cond_135
    iget-boolean v1, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->n:Z

    if-eqz v1, :cond_153

    .line 3379
    sget v1, Landroid/support/v7/a/k;->abc_screen_simple_overlay_action_mode:I

    invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    move-object v1, v0

    .line 3385
    :goto_142
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-lt v0, v2, :cond_15d

    .line 3388
    new-instance v0, Landroid/support/v7/app/au;

    invoke-direct {v0, p0}, Landroid/support/v7/app/au;-><init>(Landroid/support/v7/app/AppCompatDelegateImplV7;)V

    invoke-static {v1, v0}, Landroid/support/v4/view/cx;->a(Landroid/view/View;Landroid/support/v4/view/bx;)V

    move-object v2, v1

    goto/16 :goto_72

    .line 3382
    :cond_153
    sget v1, Landroid/support/v7/a/k;->abc_screen_simple:I

    invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    move-object v1, v0

    goto :goto_142

    :cond_15d
    move-object v0, v1

    .line 3410
    check-cast v0, Landroid/support/v7/internal/widget/af;

    new-instance v2, Landroid/support/v7/app/av;

    invoke-direct {v2, p0}, Landroid/support/v7/app/av;-><init>(Landroid/support/v7/app/AppCompatDelegateImplV7;)V

    invoke-interface {v0, v2}, Landroid/support/v7/internal/widget/af;->setOnFitSystemWindowsListener(Landroid/support/v7/internal/widget/ag;)V

    move-object v2, v1

    goto/16 :goto_72

    .line 3431
    :cond_16b
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->s:Landroid/support/v7/internal/widget/ac;

    if-nez v0, :cond_179

    .line 3432
    sget v0, Landroid/support/v7/a/i;->title:I

    invoke-virtual {v2, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->F:Landroid/widget/TextView;

    .line 3436
    :cond_179
    invoke-static {v2}, Landroid/support/v7/internal/widget/bd;->b(Landroid/view/View;)V

    .line 3438
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->f:Landroid/view/Window;

    invoke-virtual {v0, v5}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 3439
    sget v1, Landroid/support/v7/a/i;->action_bar_activity_content:I

    invoke-virtual {v2, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/v7/internal/widget/ContentFrameLayout;

    .line 3444
    :goto_18c
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v4

    if-lez v4, :cond_19d

    .line 3445
    invoke-virtual {v0, v6}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 3446
    invoke-virtual {v0, v6}, Landroid/view/ViewGroup;->removeViewAt(I)V

    .line 3447
    invoke-virtual {v1, v4}, Landroid/support/v7/internal/widget/ContentFrameLayout;->addView(Landroid/view/View;)V

    goto :goto_18c

    .line 3451
    :cond_19d
    iget-object v4, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->f:Landroid/view/Window;

    invoke-virtual {v4, v2}, Landroid/view/Window;->setContentView(Landroid/view/View;)V

    .line 3455
    const/4 v4, -0x1

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->setId(I)V

    .line 3456
    invoke-virtual {v1, v5}, Landroid/support/v7/internal/widget/ContentFrameLayout;->setId(I)V

    .line 3460
    instance-of v1, v0, Landroid/widget/FrameLayout;

    if-eqz v1, :cond_1b2

    .line 3461
    check-cast v0, Landroid/widget/FrameLayout;

    invoke-virtual {v0, v3}, Landroid/widget/FrameLayout;->setForeground(Landroid/graphics/drawable/Drawable;)V

    .line 278
    :cond_1b2
    iput-object v2, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->E:Landroid/view/ViewGroup;

    .line 4210
    iget-object v0, p0, Landroid/support/v7/app/ak;->g:Landroid/view/Window$Callback;

    instance-of v0, v0, Landroid/app/Activity;

    if-eqz v0, :cond_271

    .line 4211
    iget-object v0, p0, Landroid/support/v7/app/ak;->g:Landroid/view/Window$Callback;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    .line 282
    :goto_1c2
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1cb

    .line 283
    invoke-virtual {p0, v0}, Landroid/support/v7/app/AppCompatDelegateImplV7;->b(Ljava/lang/CharSequence;)V

    .line 4470
    :cond_1cb
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->E:Landroid/view/ViewGroup;

    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ContentFrameLayout;

    .line 4476
    iget-object v1, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->D:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getPaddingLeft()I

    move-result v1

    iget-object v2, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->D:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getPaddingTop()I

    move-result v2

    iget-object v3, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->D:Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getPaddingRight()I

    move-result v3

    iget-object v4, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->D:Landroid/view/ViewGroup;

    invoke-virtual {v4}, Landroid/view/ViewGroup;->getPaddingBottom()I

    move-result v4

    .line 5072
    iget-object v5, v0, Landroid/support/v7/internal/widget/ContentFrameLayout;->a:Landroid/graphics/Rect;

    invoke-virtual {v5, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 5073
    invoke-static {v0}, Landroid/support/v4/view/cx;->B(Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_1f9

    .line 5074
    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ContentFrameLayout;->requestLayout()V

    .line 4480
    :cond_1f9
    iget-object v1, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->e:Landroid/content/Context;

    sget-object v2, Landroid/support/v7/a/n;->Theme:[I

    invoke-virtual {v1, v2}, Landroid/content/Context;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 4481
    sget v2, Landroid/support/v7/a/n;->Theme_windowMinWidthMajor:I

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ContentFrameLayout;->getMinWidthMajor()Landroid/util/TypedValue;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getValue(ILandroid/util/TypedValue;)Z

    .line 4482
    sget v2, Landroid/support/v7/a/n;->Theme_windowMinWidthMinor:I

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ContentFrameLayout;->getMinWidthMinor()Landroid/util/TypedValue;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getValue(ILandroid/util/TypedValue;)Z

    .line 4484
    sget v2, Landroid/support/v7/a/n;->Theme_windowFixedWidthMajor:I

    invoke-virtual {v1, v2}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v2

    if-eqz v2, :cond_224

    .line 4485
    sget v2, Landroid/support/v7/a/n;->Theme_windowFixedWidthMajor:I

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ContentFrameLayout;->getFixedWidthMajor()Landroid/util/TypedValue;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getValue(ILandroid/util/TypedValue;)Z

    .line 4487
    :cond_224
    sget v2, Landroid/support/v7/a/n;->Theme_windowFixedWidthMinor:I

    invoke-virtual {v1, v2}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v2

    if-eqz v2, :cond_235

    .line 4488
    sget v2, Landroid/support/v7/a/n;->Theme_windowFixedWidthMinor:I

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ContentFrameLayout;->getFixedWidthMinor()Landroid/util/TypedValue;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getValue(ILandroid/util/TypedValue;)Z

    .line 4490
    :cond_235
    sget v2, Landroid/support/v7/a/n;->Theme_windowFixedHeightMajor:I

    invoke-virtual {v1, v2}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v2

    if-eqz v2, :cond_246

    .line 4491
    sget v2, Landroid/support/v7/a/n;->Theme_windowFixedHeightMajor:I

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ContentFrameLayout;->getFixedHeightMajor()Landroid/util/TypedValue;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getValue(ILandroid/util/TypedValue;)Z

    .line 4493
    :cond_246
    sget v2, Landroid/support/v7/a/n;->Theme_windowFixedHeightMinor:I

    invoke-virtual {v1, v2}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v2

    if-eqz v2, :cond_257

    .line 4494
    sget v2, Landroid/support/v7/a/n;->Theme_windowFixedHeightMinor:I

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ContentFrameLayout;->getFixedHeightMinor()Landroid/util/TypedValue;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getValue(ILandroid/util/TypedValue;)Z

    .line 4496
    :cond_257
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 4498
    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ContentFrameLayout;->requestLayout()V

    .line 290
    iput-boolean v7, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->C:Z

    .line 297
    invoke-virtual {p0, v6}, Landroid/support/v7/app/AppCompatDelegateImplV7;->f(I)Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;

    move-result-object v0

    .line 5193
    iget-boolean v1, p0, Landroid/support/v7/app/ak;->r:Z

    .line 298
    if-nez v1, :cond_270

    if-eqz v0, :cond_26d

    iget-object v0, v0, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->j:Landroid/support/v7/internal/view/menu/i;

    if-nez v0, :cond_270

    .line 299
    :cond_26d
    invoke-direct {p0, v8}, Landroid/support/v7/app/AppCompatDelegateImplV7;->h(I)V

    .line 302
    :cond_270
    return-void

    .line 4214
    :cond_271
    iget-object v0, p0, Landroid/support/v7/app/ak;->q:Ljava/lang/CharSequence;

    goto/16 :goto_1c2

    :cond_275
    move-object v2, v3

    goto/16 :goto_72
.end method

.method private p()Landroid/view/ViewGroup;
    .registers 9

    .prologue
    const v7, 0x1020002

    const/16 v6, 0x6d

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v5, 0x0

    .line 305
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->e:Landroid/content/Context;

    sget-object v1, Landroid/support/v7/a/n;->Theme:[I

    invoke-virtual {v0, v1}, Landroid/content/Context;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 307
    sget v1, Landroid/support/v7/a/n;->Theme_windowActionBar:I

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v1

    if-nez v1, :cond_23

    .line 308
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 309
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "You need to use a Theme.AppCompat theme (or descendant) with this activity."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 313
    :cond_23
    sget v1, Landroid/support/v7/a/n;->Theme_windowNoTitle:I

    invoke-virtual {v0, v1, v5}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    if-eqz v1, :cond_bb

    .line 314
    invoke-virtual {p0, v4}, Landroid/support/v7/app/AppCompatDelegateImplV7;->b(I)Z

    .line 319
    :cond_2e
    :goto_2e
    sget v1, Landroid/support/v7/a/n;->Theme_windowActionBarOverlay:I

    invoke-virtual {v0, v1, v5}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    if-eqz v1, :cond_39

    .line 320
    invoke-virtual {p0, v6}, Landroid/support/v7/app/AppCompatDelegateImplV7;->b(I)Z

    .line 322
    :cond_39
    sget v1, Landroid/support/v7/a/n;->Theme_windowActionModeOverlay:I

    invoke-virtual {v0, v1, v5}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    if-eqz v1, :cond_46

    .line 323
    const/16 v1, 0xa

    invoke-virtual {p0, v1}, Landroid/support/v7/app/AppCompatDelegateImplV7;->b(I)Z

    .line 325
    :cond_46
    sget v1, Landroid/support/v7/a/n;->Theme_android_windowIsFloating:I

    invoke-virtual {v0, v1, v5}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->o:Z

    .line 326
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 328
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->e:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 332
    iget-boolean v1, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->p:Z

    if-nez v1, :cond_12f

    .line 333
    iget-boolean v1, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->o:Z

    if-eqz v1, :cond_ca

    .line 335
    sget v1, Landroid/support/v7/a/k;->abc_dialog_title_material:I

    invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 339
    iput-boolean v5, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->m:Z

    iput-boolean v5, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->l:Z

    move-object v2, v0

    .line 420
    :goto_6c
    if-nez v2, :cond_165

    .line 421
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "AppCompat does not support the current theme features: { windowActionBar: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->l:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", windowActionBarOverlay: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->m:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", android:windowIsFloating: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->o:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", windowActionModeOverlay: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->n:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", windowNoTitle: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->p:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " }"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 315
    :cond_bb
    sget v1, Landroid/support/v7/a/n;->Theme_windowActionBar:I

    invoke-virtual {v0, v1, v5}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    if-eqz v1, :cond_2e

    .line 317
    const/16 v1, 0x6c

    invoke-virtual {p0, v1}, Landroid/support/v7/app/AppCompatDelegateImplV7;->b(I)Z

    goto/16 :goto_2e

    .line 340
    :cond_ca
    iget-boolean v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->l:Z

    if-eqz v0, :cond_1ad

    .line 346
    new-instance v1, Landroid/util/TypedValue;

    invoke-direct {v1}, Landroid/util/TypedValue;-><init>()V

    .line 347
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->e:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    sget v2, Landroid/support/v7/a/d;->actionBarTheme:I

    invoke-virtual {v0, v2, v1, v4}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 350
    iget v0, v1, Landroid/util/TypedValue;->resourceId:I

    if-eqz v0, :cond_12c

    .line 351
    new-instance v0, Landroid/support/v7/internal/view/b;

    iget-object v2, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->e:Landroid/content/Context;

    iget v1, v1, Landroid/util/TypedValue;->resourceId:I

    invoke-direct {v0, v2, v1}, Landroid/support/v7/internal/view/b;-><init>(Landroid/content/Context;I)V

    .line 357
    :goto_eb
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Landroid/support/v7/a/k;->abc_screen_toolbar:I

    invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 360
    sget v1, Landroid/support/v7/a/i;->decor_content_parent:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/v7/internal/widget/ac;

    iput-object v1, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->s:Landroid/support/v7/internal/widget/ac;

    .line 362
    iget-object v1, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->s:Landroid/support/v7/internal/widget/ac;

    .line 5197
    iget-object v2, p0, Landroid/support/v7/app/ak;->f:Landroid/view/Window;

    invoke-virtual {v2}, Landroid/view/Window;->getCallback()Landroid/view/Window$Callback;

    move-result-object v2

    .line 362
    invoke-interface {v1, v2}, Landroid/support/v7/internal/widget/ac;->setWindowCallback(Landroid/view/Window$Callback;)V

    .line 367
    iget-boolean v1, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->m:Z

    if-eqz v1, :cond_115

    .line 368
    iget-object v1, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->s:Landroid/support/v7/internal/widget/ac;

    invoke-interface {v1, v6}, Landroid/support/v7/internal/widget/ac;->a(I)V

    .line 370
    :cond_115
    iget-boolean v1, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->H:Z

    if-eqz v1, :cond_11f

    .line 371
    iget-object v1, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->s:Landroid/support/v7/internal/widget/ac;

    const/4 v2, 0x2

    invoke-interface {v1, v2}, Landroid/support/v7/internal/widget/ac;->a(I)V

    .line 373
    :cond_11f
    iget-boolean v1, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->I:Z

    if-eqz v1, :cond_129

    .line 374
    iget-object v1, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->s:Landroid/support/v7/internal/widget/ac;

    const/4 v2, 0x5

    invoke-interface {v1, v2}, Landroid/support/v7/internal/widget/ac;->a(I)V

    :cond_129
    move-object v2, v0

    .line 376
    goto/16 :goto_6c

    .line 353
    :cond_12c
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->e:Landroid/content/Context;

    goto :goto_eb

    .line 378
    :cond_12f
    iget-boolean v1, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->n:Z

    if-eqz v1, :cond_14d

    .line 379
    sget v1, Landroid/support/v7/a/k;->abc_screen_simple_overlay_action_mode:I

    invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    move-object v1, v0

    .line 385
    :goto_13c
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-lt v0, v2, :cond_157

    .line 388
    new-instance v0, Landroid/support/v7/app/au;

    invoke-direct {v0, p0}, Landroid/support/v7/app/au;-><init>(Landroid/support/v7/app/AppCompatDelegateImplV7;)V

    invoke-static {v1, v0}, Landroid/support/v4/view/cx;->a(Landroid/view/View;Landroid/support/v4/view/bx;)V

    move-object v2, v1

    goto/16 :goto_6c

    .line 382
    :cond_14d
    sget v1, Landroid/support/v7/a/k;->abc_screen_simple:I

    invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    move-object v1, v0

    goto :goto_13c

    :cond_157
    move-object v0, v1

    .line 410
    check-cast v0, Landroid/support/v7/internal/widget/af;

    new-instance v2, Landroid/support/v7/app/av;

    invoke-direct {v2, p0}, Landroid/support/v7/app/av;-><init>(Landroid/support/v7/app/AppCompatDelegateImplV7;)V

    invoke-interface {v0, v2}, Landroid/support/v7/internal/widget/af;->setOnFitSystemWindowsListener(Landroid/support/v7/internal/widget/ag;)V

    move-object v2, v1

    goto/16 :goto_6c

    .line 431
    :cond_165
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->s:Landroid/support/v7/internal/widget/ac;

    if-nez v0, :cond_173

    .line 432
    sget v0, Landroid/support/v7/a/i;->title:I

    invoke-virtual {v2, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->F:Landroid/widget/TextView;

    .line 436
    :cond_173
    invoke-static {v2}, Landroid/support/v7/internal/widget/bd;->b(Landroid/view/View;)V

    .line 438
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->f:Landroid/view/Window;

    invoke-virtual {v0, v7}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 439
    sget v1, Landroid/support/v7/a/i;->action_bar_activity_content:I

    invoke-virtual {v2, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/v7/internal/widget/ContentFrameLayout;

    .line 444
    :goto_186
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v4

    if-lez v4, :cond_197

    .line 445
    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 446
    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->removeViewAt(I)V

    .line 447
    invoke-virtual {v1, v4}, Landroid/support/v7/internal/widget/ContentFrameLayout;->addView(Landroid/view/View;)V

    goto :goto_186

    .line 451
    :cond_197
    iget-object v4, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->f:Landroid/view/Window;

    invoke-virtual {v4, v2}, Landroid/view/Window;->setContentView(Landroid/view/View;)V

    .line 455
    const/4 v4, -0x1

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->setId(I)V

    .line 456
    invoke-virtual {v1, v7}, Landroid/support/v7/internal/widget/ContentFrameLayout;->setId(I)V

    .line 460
    instance-of v1, v0, Landroid/widget/FrameLayout;

    if-eqz v1, :cond_1ac

    .line 461
    check-cast v0, Landroid/widget/FrameLayout;

    invoke-virtual {v0, v3}, Landroid/widget/FrameLayout;->setForeground(Landroid/graphics/drawable/Drawable;)V

    .line 464
    :cond_1ac
    return-object v2

    :cond_1ad
    move-object v2, v3

    goto/16 :goto_6c
.end method

.method private static q()V
    .registers 0

    .prologue
    .line 467
    return-void
.end method

.method private r()V
    .registers 7

    .prologue
    .line 470
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->E:Landroid/view/ViewGroup;

    const v1, 0x1020002

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ContentFrameLayout;

    .line 476
    iget-object v1, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->D:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getPaddingLeft()I

    move-result v1

    iget-object v2, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->D:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getPaddingTop()I

    move-result v2

    iget-object v3, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->D:Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getPaddingRight()I

    move-result v3

    iget-object v4, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->D:Landroid/view/ViewGroup;

    invoke-virtual {v4}, Landroid/view/ViewGroup;->getPaddingBottom()I

    move-result v4

    .line 6072
    iget-object v5, v0, Landroid/support/v7/internal/widget/ContentFrameLayout;->a:Landroid/graphics/Rect;

    invoke-virtual {v5, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 6073
    invoke-static {v0}, Landroid/support/v4/view/cx;->B(Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_31

    .line 6074
    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ContentFrameLayout;->requestLayout()V

    .line 480
    :cond_31
    iget-object v1, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->e:Landroid/content/Context;

    sget-object v2, Landroid/support/v7/a/n;->Theme:[I

    invoke-virtual {v1, v2}, Landroid/content/Context;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 481
    sget v2, Landroid/support/v7/a/n;->Theme_windowMinWidthMajor:I

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ContentFrameLayout;->getMinWidthMajor()Landroid/util/TypedValue;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getValue(ILandroid/util/TypedValue;)Z

    .line 482
    sget v2, Landroid/support/v7/a/n;->Theme_windowMinWidthMinor:I

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ContentFrameLayout;->getMinWidthMinor()Landroid/util/TypedValue;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getValue(ILandroid/util/TypedValue;)Z

    .line 484
    sget v2, Landroid/support/v7/a/n;->Theme_windowFixedWidthMajor:I

    invoke-virtual {v1, v2}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v2

    if-eqz v2, :cond_5c

    .line 485
    sget v2, Landroid/support/v7/a/n;->Theme_windowFixedWidthMajor:I

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ContentFrameLayout;->getFixedWidthMajor()Landroid/util/TypedValue;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getValue(ILandroid/util/TypedValue;)Z

    .line 487
    :cond_5c
    sget v2, Landroid/support/v7/a/n;->Theme_windowFixedWidthMinor:I

    invoke-virtual {v1, v2}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v2

    if-eqz v2, :cond_6d

    .line 488
    sget v2, Landroid/support/v7/a/n;->Theme_windowFixedWidthMinor:I

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ContentFrameLayout;->getFixedWidthMinor()Landroid/util/TypedValue;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getValue(ILandroid/util/TypedValue;)Z

    .line 490
    :cond_6d
    sget v2, Landroid/support/v7/a/n;->Theme_windowFixedHeightMajor:I

    invoke-virtual {v1, v2}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v2

    if-eqz v2, :cond_7e

    .line 491
    sget v2, Landroid/support/v7/a/n;->Theme_windowFixedHeightMajor:I

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ContentFrameLayout;->getFixedHeightMajor()Landroid/util/TypedValue;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getValue(ILandroid/util/TypedValue;)Z

    .line 493
    :cond_7e
    sget v2, Landroid/support/v7/a/n;->Theme_windowFixedHeightMinor:I

    invoke-virtual {v1, v2}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v2

    if-eqz v2, :cond_8f

    .line 494
    sget v2, Landroid/support/v7/a/n;->Theme_windowFixedHeightMinor:I

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ContentFrameLayout;->getFixedHeightMinor()Landroid/util/TypedValue;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getValue(ILandroid/util/TypedValue;)Z

    .line 496
    :cond_8f
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 498
    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ContentFrameLayout;->requestLayout()V

    .line 499
    return-void
.end method

.method private s()Z
    .registers 3

    .prologue
    const/4 v0, 0x1

    .line 794
    iget-object v1, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->t:Landroid/support/v7/c/a;

    if-eqz v1, :cond_b

    .line 795
    iget-object v1, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->t:Landroid/support/v7/c/a;

    invoke-virtual {v1}, Landroid/support/v7/c/a;->c()V

    .line 806
    :cond_a
    :goto_a
    return v0

    .line 800
    :cond_b
    invoke-virtual {p0}, Landroid/support/v7/app/AppCompatDelegateImplV7;->a()Landroid/support/v7/app/a;

    move-result-object v1

    .line 801
    if-eqz v1, :cond_17

    invoke-virtual {v1}, Landroid/support/v7/app/a;->z()Z

    move-result v1

    if-nez v1, :cond_a

    .line 806
    :cond_17
    const/4 v0, 0x0

    goto :goto_a
.end method

.method private t()V
    .registers 7

    .prologue
    const/16 v5, 0x6c

    const/4 v4, 0x0

    .line 1089
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->s:Landroid/support/v7/internal/widget/ac;

    if-eqz v0, :cond_81

    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->s:Landroid/support/v7/internal/widget/ac;

    invoke-interface {v0}, Landroid/support/v7/internal/widget/ac;->c()Z

    move-result v0

    if-eqz v0, :cond_81

    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->e:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/view/du;->b(Landroid/view/ViewConfiguration;)Z

    move-result v0

    if-eqz v0, :cond_23

    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->s:Landroid/support/v7/internal/widget/ac;

    invoke-interface {v0}, Landroid/support/v7/internal/widget/ac;->e()Z

    move-result v0

    if-eqz v0, :cond_81

    .line 23197
    :cond_23
    iget-object v0, p0, Landroid/support/v7/app/ak;->f:Landroid/view/Window;

    invoke-virtual {v0}, Landroid/view/Window;->getCallback()Landroid/view/Window$Callback;

    move-result-object v0

    .line 1095
    iget-object v1, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->s:Landroid/support/v7/internal/widget/ac;

    invoke-interface {v1}, Landroid/support/v7/internal/widget/ac;->d()Z

    move-result v1

    if-nez v1, :cond_6e

    .line 1096
    if-eqz v0, :cond_6d

    .line 24193
    iget-boolean v1, p0, Landroid/support/v7/app/ak;->r:Z

    .line 1096
    if-nez v1, :cond_6d

    .line 1098
    iget-boolean v1, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->y:Z

    if-eqz v1, :cond_4d

    iget v1, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->z:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_4d

    .line 1100
    iget-object v1, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->D:Landroid/view/ViewGroup;

    iget-object v2, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->M:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 1101
    iget-object v1, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->M:Ljava/lang/Runnable;

    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    .line 1104
    :cond_4d
    invoke-virtual {p0, v4}, Landroid/support/v7/app/AppCompatDelegateImplV7;->f(I)Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;

    move-result-object v1

    .line 1108
    iget-object v2, v1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->j:Landroid/support/v7/internal/view/menu/i;

    if-eqz v2, :cond_6d

    iget-boolean v2, v1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->r:Z

    if-nez v2, :cond_6d

    iget-object v2, v1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->i:Landroid/view/View;

    iget-object v3, v1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->j:Landroid/support/v7/internal/view/menu/i;

    invoke-interface {v0, v4, v2, v3}, Landroid/view/Window$Callback;->onPreparePanel(ILandroid/view/View;Landroid/view/Menu;)Z

    move-result v2

    if-eqz v2, :cond_6d

    .line 1110
    iget-object v1, v1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->j:Landroid/support/v7/internal/view/menu/i;

    invoke-interface {v0, v5, v1}, Landroid/view/Window$Callback;->onMenuOpened(ILandroid/view/Menu;)Z

    .line 1111
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->s:Landroid/support/v7/internal/widget/ac;

    invoke-interface {v0}, Landroid/support/v7/internal/widget/ac;->f()Z

    .line 1130
    :cond_6d
    :goto_6d
    return-void

    .line 1115
    :cond_6e
    iget-object v1, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->s:Landroid/support/v7/internal/widget/ac;

    invoke-interface {v1}, Landroid/support/v7/internal/widget/ac;->g()Z

    .line 25193
    iget-boolean v1, p0, Landroid/support/v7/app/ak;->r:Z

    .line 1116
    if-nez v1, :cond_6d

    .line 1117
    invoke-virtual {p0, v4}, Landroid/support/v7/app/AppCompatDelegateImplV7;->f(I)Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;

    move-result-object v1

    .line 1118
    iget-object v1, v1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->j:Landroid/support/v7/internal/view/menu/i;

    invoke-interface {v0, v5, v1}, Landroid/view/Window$Callback;->onPanelClosed(ILandroid/view/Menu;)V

    goto :goto_6d

    .line 1124
    :cond_81
    invoke-virtual {p0, v4}, Landroid/support/v7/app/AppCompatDelegateImplV7;->f(I)Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;

    move-result-object v0

    .line 1126
    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->q:Z

    .line 1127
    invoke-virtual {p0, v0, v4}, Landroid/support/v7/app/AppCompatDelegateImplV7;->a(Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;Z)V

    .line 1129
    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Landroid/support/v7/app/AppCompatDelegateImplV7;->a(Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;Landroid/view/KeyEvent;)V

    goto :goto_6d
.end method

.method private u()V
    .registers 3

    .prologue
    .line 1609
    iget-boolean v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->C:Z

    if-eqz v0, :cond_c

    .line 1610
    new-instance v0, Landroid/util/AndroidRuntimeException;

    const-string v1, "Window feature must be requested before adding content"

    invoke-direct {v0, v1}, Landroid/util/AndroidRuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1613
    :cond_c
    return-void
.end method

.method private v()Landroid/view/ViewGroup;
    .registers 2

    .prologue
    .line 1630
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->E:Landroid/view/ViewGroup;

    return-object v0
.end method


# virtual methods
.method final a(Landroid/view/Menu;)Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;
    .registers 7

    .prologue
    const/4 v1, 0x0

    .line 1443
    iget-object v3, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->K:[Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;

    .line 1444
    if-eqz v3, :cond_13

    array-length v0, v3

    :goto_6
    move v2, v1

    .line 1445
    :goto_7
    if-ge v2, v0, :cond_19

    .line 1446
    aget-object v1, v3, v2

    .line 1447
    if-eqz v1, :cond_15

    iget-object v4, v1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->j:Landroid/support/v7/internal/view/menu/i;

    if-ne v4, p1, :cond_15

    move-object v0, v1

    .line 1451
    :goto_12
    return-object v0

    :cond_13
    move v0, v1

    .line 1444
    goto :goto_6

    .line 1445
    :cond_15
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_7

    .line 1451
    :cond_19
    const/4 v0, 0x0

    goto :goto_12
.end method

.method public final a(Landroid/support/v7/c/b;)Landroid/support/v7/c/a;
    .registers 11

    .prologue
    const/4 v8, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 622
    if-nez p1, :cond_d

    .line 623
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "ActionMode callback can not be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 626
    :cond_d
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->t:Landroid/support/v7/c/a;

    if-eqz v0, :cond_16

    .line 627
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->t:Landroid/support/v7/c/a;

    invoke-virtual {v0}, Landroid/support/v7/c/a;->c()V

    .line 630
    :cond_16
    new-instance v3, Landroid/support/v7/app/ba;

    invoke-direct {v3, p0, p1}, Landroid/support/v7/app/ba;-><init>(Landroid/support/v7/app/AppCompatDelegateImplV7;Landroid/support/v7/c/b;)V

    .line 632
    invoke-virtual {p0}, Landroid/support/v7/app/AppCompatDelegateImplV7;->a()Landroid/support/v7/app/a;

    move-result-object v0

    .line 633
    if-eqz v0, :cond_27

    .line 634
    invoke-virtual {v0, v3}, Landroid/support/v7/app/a;->a(Landroid/support/v7/c/b;)Landroid/support/v7/c/a;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->t:Landroid/support/v7/c/a;

    .line 640
    :cond_27
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->t:Landroid/support/v7/c/a;

    if-nez v0, :cond_126

    .line 11658
    invoke-virtual {p0}, Landroid/support/v7/app/AppCompatDelegateImplV7;->n()V

    .line 11659
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->t:Landroid/support/v7/c/a;

    if-eqz v0, :cond_37

    .line 11660
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->t:Landroid/support/v7/c/a;

    invoke-virtual {v0}, Landroid/support/v7/c/a;->c()V

    .line 11663
    :cond_37
    new-instance v4, Landroid/support/v7/app/ba;

    invoke-direct {v4, p0, v3}, Landroid/support/v7/app/ba;-><init>(Landroid/support/v7/app/AppCompatDelegateImplV7;Landroid/support/v7/c/b;)V

    .line 11676
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->u:Landroid/support/v7/internal/widget/ActionBarContextView;

    if-nez v0, :cond_c3

    .line 11677
    iget-boolean v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->o:Z

    if-eqz v0, :cond_12d

    .line 11679
    new-instance v5, Landroid/util/TypedValue;

    invoke-direct {v5}, Landroid/util/TypedValue;-><init>()V

    .line 11680
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->e:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    .line 11681
    sget v6, Landroid/support/v7/a/d;->actionBarTheme:I

    invoke-virtual {v0, v6, v5, v1}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 11684
    iget v6, v5, Landroid/util/TypedValue;->resourceId:I

    if-eqz v6, :cond_129

    .line 11685
    iget-object v6, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->e:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/res/Resources;->newTheme()Landroid/content/res/Resources$Theme;

    move-result-object v6

    .line 11686
    invoke-virtual {v6, v0}, Landroid/content/res/Resources$Theme;->setTo(Landroid/content/res/Resources$Theme;)V

    .line 11687
    iget v0, v5, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {v6, v0, v1}, Landroid/content/res/Resources$Theme;->applyStyle(IZ)V

    .line 11689
    new-instance v0, Landroid/support/v7/internal/view/b;

    iget-object v7, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->e:Landroid/content/Context;

    invoke-direct {v0, v7, v2}, Landroid/support/v7/internal/view/b;-><init>(Landroid/content/Context;I)V

    .line 11690
    invoke-virtual {v0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v7

    invoke-virtual {v7, v6}, Landroid/content/res/Resources$Theme;->setTo(Landroid/content/res/Resources$Theme;)V

    .line 11695
    :goto_78
    new-instance v6, Landroid/support/v7/internal/widget/ActionBarContextView;

    invoke-direct {v6, v0}, Landroid/support/v7/internal/widget/ActionBarContextView;-><init>(Landroid/content/Context;)V

    iput-object v6, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->u:Landroid/support/v7/internal/widget/ActionBarContextView;

    .line 11696
    new-instance v6, Landroid/widget/PopupWindow;

    sget v7, Landroid/support/v7/a/d;->actionModePopupWindowStyle:I

    invoke-direct {v6, v0, v8, v7}, Landroid/widget/PopupWindow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v6, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->v:Landroid/widget/PopupWindow;

    .line 11698
    iget-object v6, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->v:Landroid/widget/PopupWindow;

    invoke-static {v6}, Landroid/support/v4/widget/bo;->b(Landroid/widget/PopupWindow;)V

    .line 11700
    iget-object v6, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->v:Landroid/widget/PopupWindow;

    iget-object v7, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->u:Landroid/support/v7/internal/widget/ActionBarContextView;

    invoke-virtual {v6, v7}, Landroid/widget/PopupWindow;->setContentView(Landroid/view/View;)V

    .line 11701
    iget-object v6, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->v:Landroid/widget/PopupWindow;

    const/4 v7, -0x1

    invoke-virtual {v6, v7}, Landroid/widget/PopupWindow;->setWidth(I)V

    .line 11703
    invoke-virtual {v0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v6

    sget v7, Landroid/support/v7/a/d;->actionBarSize:I

    invoke-virtual {v6, v7, v5, v1}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 11705
    iget v5, v5, Landroid/util/TypedValue;->data:I

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    invoke-static {v5, v0}, Landroid/util/TypedValue;->complexToDimensionPixelSize(ILandroid/util/DisplayMetrics;)I

    move-result v0

    .line 11707
    iget-object v5, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->u:Landroid/support/v7/internal/widget/ActionBarContextView;

    invoke-virtual {v5, v0}, Landroid/support/v7/internal/widget/ActionBarContextView;->setContentHeight(I)V

    .line 11708
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->v:Landroid/widget/PopupWindow;

    const/4 v5, -0x2

    invoke-virtual {v0, v5}, Landroid/widget/PopupWindow;->setHeight(I)V

    .line 11709
    new-instance v0, Landroid/support/v7/app/aw;

    invoke-direct {v0, p0}, Landroid/support/v7/app/aw;-><init>(Landroid/support/v7/app/AppCompatDelegateImplV7;)V

    iput-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->w:Ljava/lang/Runnable;

    .line 11743
    :cond_c3
    :goto_c3
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->u:Landroid/support/v7/internal/widget/ActionBarContextView;

    if-eqz v0, :cond_122

    .line 11744
    invoke-virtual {p0}, Landroid/support/v7/app/AppCompatDelegateImplV7;->n()V

    .line 11745
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->u:Landroid/support/v7/internal/widget/ActionBarContextView;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarContextView;->i()V

    .line 11746
    new-instance v5, Landroid/support/v7/internal/view/c;

    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->u:Landroid/support/v7/internal/widget/ActionBarContextView;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarContextView;->getContext()Landroid/content/Context;

    move-result-object v6

    iget-object v7, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->u:Landroid/support/v7/internal/widget/ActionBarContextView;

    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->v:Landroid/widget/PopupWindow;

    if-nez v0, :cond_14e

    move v0, v1

    :goto_de
    invoke-direct {v5, v6, v7, v4, v0}, Landroid/support/v7/internal/view/c;-><init>(Landroid/content/Context;Landroid/support/v7/internal/widget/ActionBarContextView;Landroid/support/v7/c/b;Z)V

    .line 11748
    invoke-virtual {v5}, Landroid/support/v7/c/a;->b()Landroid/view/Menu;

    move-result-object v0

    invoke-interface {v3, v5, v0}, Landroid/support/v7/c/b;->a(Landroid/support/v7/c/a;Landroid/view/Menu;)Z

    move-result v0

    if-eqz v0, :cond_150

    .line 11749
    invoke-virtual {v5}, Landroid/support/v7/c/a;->d()V

    .line 11750
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->u:Landroid/support/v7/internal/widget/ActionBarContextView;

    invoke-virtual {v0, v5}, Landroid/support/v7/internal/widget/ActionBarContextView;->a(Landroid/support/v7/c/a;)V

    .line 11751
    iput-object v5, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->t:Landroid/support/v7/c/a;

    .line 11752
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->u:Landroid/support/v7/internal/widget/ActionBarContextView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/support/v4/view/cx;->c(Landroid/view/View;F)V

    .line 11753
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->u:Landroid/support/v7/internal/widget/ActionBarContextView;

    invoke-static {v0}, Landroid/support/v4/view/cx;->p(Landroid/view/View;)Landroid/support/v4/view/fk;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/support/v4/view/fk;->a(F)Landroid/support/v4/view/fk;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->x:Landroid/support/v4/view/fk;

    .line 11754
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->x:Landroid/support/v4/view/fk;

    new-instance v1, Landroid/support/v7/app/ay;

    invoke-direct {v1, p0}, Landroid/support/v7/app/ay;-><init>(Landroid/support/v7/app/AppCompatDelegateImplV7;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/view/fk;->a(Landroid/support/v4/view/gd;)Landroid/support/v4/view/fk;

    .line 11772
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->v:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_122

    .line 11773
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->f:Landroid/view/Window;

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->w:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 11783
    :cond_122
    :goto_122
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->t:Landroid/support/v7/c/a;

    .line 642
    iput-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->t:Landroid/support/v7/c/a;

    .line 645
    :cond_126
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->t:Landroid/support/v7/c/a;

    return-object v0

    .line 11692
    :cond_129
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->e:Landroid/content/Context;

    goto/16 :goto_78

    .line 11733
    :cond_12d
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->E:Landroid/view/ViewGroup;

    sget v5, Landroid/support/v7/a/i;->action_mode_bar_stub:I

    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ViewStubCompat;

    .line 11735
    if-eqz v0, :cond_c3

    .line 11737
    invoke-virtual {p0}, Landroid/support/v7/app/AppCompatDelegateImplV7;->m()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/support/v7/internal/widget/ViewStubCompat;->setLayoutInflater(Landroid/view/LayoutInflater;)V

    .line 11738
    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ViewStubCompat;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ActionBarContextView;

    iput-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->u:Landroid/support/v7/internal/widget/ActionBarContextView;

    goto/16 :goto_c3

    :cond_14e
    move v0, v2

    .line 11746
    goto :goto_de

    .line 11776
    :cond_150
    iput-object v8, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->t:Landroid/support/v7/c/a;

    goto :goto_122
.end method

.method public final a(Landroid/view/View;Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;
    .registers 11

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 960
    invoke-virtual {p0, p2, p3, p4}, Landroid/support/v7/app/AppCompatDelegateImplV7;->a(Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;

    move-result-object v0

    .line 961
    if-eqz v0, :cond_9

    .line 19109
    :goto_8
    return-object v0

    .line 18902
    :cond_9
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-ge v0, v1, :cond_4b

    move v2, v3

    .line 18904
    :goto_10
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->Q:Landroid/support/v7/internal/a/a;

    if-nez v0, :cond_1b

    .line 18905
    new-instance v0, Landroid/support/v7/internal/a/a;

    invoke-direct {v0}, Landroid/support/v7/internal/a/a;-><init>()V

    iput-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->Q:Landroid/support/v7/internal/a/a;

    .line 18909
    :cond_1b
    if-eqz v2, :cond_68

    iget-boolean v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->C:Z

    if-eqz v0, :cond_68

    move-object v0, p1

    check-cast v0, Landroid/view/ViewParent;

    .line 18919
    if-nez v0, :cond_11d

    move v0, v4

    .line 18909
    :goto_27
    if-eqz v0, :cond_68

    move v0, v3

    .line 18912
    :goto_2a
    iget-object v1, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->Q:Landroid/support/v7/internal/a/a;

    .line 19074
    if-eqz v0, :cond_11a

    if-eqz p1, :cond_11a

    .line 19075
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 19079
    :goto_34
    invoke-static {v0, p4, v2}, Landroid/support/v7/internal/a/a;->a(Landroid/content/Context;Landroid/util/AttributeSet;Z)Landroid/content/Context;

    move-result-object v2

    .line 19083
    const/4 v0, -0x1

    invoke-virtual {p2}, Ljava/lang/String;->hashCode()I

    move-result v5

    sparse-switch v5, :sswitch_data_120

    :cond_40
    move v4, v0

    :goto_41
    packed-switch v4, :pswitch_data_14a

    .line 19106
    if-eq p3, v2, :cond_117

    .line 19109
    invoke-virtual {v1, v2, p2, p4}, Landroid/support/v7/internal/a/a;->a(Landroid/content/Context;Ljava/lang/String;Landroid/util/AttributeSet;)Landroid/view/View;

    move-result-object v0

    goto :goto_8

    :cond_4b
    move v2, v4

    .line 18902
    goto :goto_10

    .line 18938
    :cond_4d
    invoke-interface {v1}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    .line 18924
    :goto_51
    if-nez v1, :cond_55

    move v0, v3

    .line 18929
    goto :goto_27

    .line 18930
    :cond_55
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->D:Landroid/view/ViewGroup;

    if-eq v1, v0, :cond_66

    instance-of v0, v1, Landroid/view/View;

    if-eqz v0, :cond_66

    move-object v0, v1

    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Landroid/support/v4/view/cx;->D(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_4d

    :cond_66
    move v0, v4

    .line 18936
    goto :goto_27

    :cond_68
    move v0, v4

    .line 18909
    goto :goto_2a

    .line 19083
    :sswitch_6a
    const-string v3, "EditText"

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_40

    goto :goto_41

    :sswitch_73
    const-string v4, "Spinner"

    invoke-virtual {p2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_40

    move v4, v3

    goto :goto_41

    :sswitch_7d
    const-string v3, "CheckBox"

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_40

    const/4 v4, 0x2

    goto :goto_41

    :sswitch_87
    const-string v3, "RadioButton"

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_40

    const/4 v4, 0x3

    goto :goto_41

    :sswitch_91
    const-string v3, "CheckedTextView"

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_40

    const/4 v4, 0x4

    goto :goto_41

    :sswitch_9b
    const-string v3, "AutoCompleteTextView"

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_40

    const/4 v4, 0x5

    goto :goto_41

    :sswitch_a5
    const-string v3, "MultiAutoCompleteTextView"

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_40

    const/4 v4, 0x6

    goto :goto_41

    :sswitch_af
    const-string v3, "RatingBar"

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_40

    const/4 v4, 0x7

    goto :goto_41

    :sswitch_b9
    const-string v3, "Button"

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_40

    const/16 v4, 0x8

    goto/16 :goto_41

    :sswitch_c5
    const-string v3, "TextView"

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_40

    const/16 v4, 0x9

    goto/16 :goto_41

    .line 19085
    :pswitch_d1
    new-instance v0, Landroid/support/v7/widget/w;

    invoke-direct {v0, v2, p4}, Landroid/support/v7/widget/w;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    goto/16 :goto_8

    .line 19087
    :pswitch_d8
    new-instance v0, Landroid/support/v7/widget/aa;

    invoke-direct {v0, v2, p4}, Landroid/support/v7/widget/aa;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    goto/16 :goto_8

    .line 19089
    :pswitch_df
    new-instance v0, Landroid/support/v7/widget/s;

    invoke-direct {v0, v2, p4}, Landroid/support/v7/widget/s;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    goto/16 :goto_8

    .line 19091
    :pswitch_e6
    new-instance v0, Landroid/support/v7/widget/y;

    invoke-direct {v0, v2, p4}, Landroid/support/v7/widget/y;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    goto/16 :goto_8

    .line 19093
    :pswitch_ed
    new-instance v0, Landroid/support/v7/widget/t;

    invoke-direct {v0, v2, p4}, Landroid/support/v7/widget/t;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    goto/16 :goto_8

    .line 19095
    :pswitch_f4
    new-instance v0, Landroid/support/v7/widget/p;

    invoke-direct {v0, v2, p4}, Landroid/support/v7/widget/p;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    goto/16 :goto_8

    .line 19097
    :pswitch_fb
    new-instance v0, Landroid/support/v7/widget/x;

    invoke-direct {v0, v2, p4}, Landroid/support/v7/widget/x;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    goto/16 :goto_8

    .line 19099
    :pswitch_102
    new-instance v0, Landroid/support/v7/widget/z;

    invoke-direct {v0, v2, p4}, Landroid/support/v7/widget/z;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    goto/16 :goto_8

    .line 19101
    :pswitch_109
    new-instance v0, Landroid/support/v7/widget/r;

    invoke-direct {v0, v2, p4}, Landroid/support/v7/widget/r;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    goto/16 :goto_8

    .line 19103
    :pswitch_110
    new-instance v0, Landroid/support/v7/widget/ai;

    invoke-direct {v0, v2, p4}, Landroid/support/v7/widget/ai;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    goto/16 :goto_8

    .line 19112
    :cond_117
    const/4 v0, 0x0

    .line 966
    goto/16 :goto_8

    :cond_11a
    move-object v0, p3

    goto/16 :goto_34

    :cond_11d
    move-object v1, v0

    goto/16 :goto_51

    .line 19083
    :sswitch_data_120
    .sparse-switch
        -0x7404ceea -> :sswitch_af
        -0x56c015e7 -> :sswitch_91
        -0x503aa7ad -> :sswitch_a5
        -0x37f7066e -> :sswitch_c5
        -0x1440b607 -> :sswitch_73
        0x2e46a6ed -> :sswitch_87
        0x5445f9ba -> :sswitch_9b
        0x5f7507c3 -> :sswitch_7d
        0x63577677 -> :sswitch_6a
        0x77471352 -> :sswitch_b9
    .end sparse-switch

    :pswitch_data_14a
    .packed-switch 0x0
        :pswitch_d1
        :pswitch_d8
        :pswitch_df
        :pswitch_e6
        :pswitch_ed
        :pswitch_f4
        :pswitch_fb
        :pswitch_102
        :pswitch_109
        :pswitch_110
    .end packed-switch
.end method

.method a(Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;
    .registers 5

    .prologue
    .line 971
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->g:Landroid/view/Window$Callback;

    instance-of v0, v0, Landroid/view/LayoutInflater$Factory;

    if-eqz v0, :cond_11

    .line 972
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->g:Landroid/view/Window$Callback;

    check-cast v0, Landroid/view/LayoutInflater$Factory;

    invoke-interface {v0, p1, p2, p3}, Landroid/view/LayoutInflater$Factory;->onCreateView(Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;

    move-result-object v0

    .line 974
    if-eqz v0, :cond_11

    .line 978
    :goto_10
    return-object v0

    :cond_11
    const/4 v0, 0x0

    goto :goto_10
.end method

.method public final a(I)V
    .registers 4

    .prologue
    .line 252
    invoke-direct {p0}, Landroid/support/v7/app/AppCompatDelegateImplV7;->o()V

    .line 253
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->E:Landroid/view/ViewGroup;

    const v1, 0x1020002

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 254
    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 255
    iget-object v1, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->e:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 256
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->g:Landroid/view/Window$Callback;

    invoke-interface {v0}, Landroid/view/Window$Callback;->onContentChanged()V

    .line 257
    return-void
.end method

.method final a(ILandroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;Landroid/view/Menu;)V
    .registers 5

    .prologue
    .line 1416
    if-nez p3, :cond_13

    .line 1418
    if-nez p2, :cond_f

    .line 1419
    if-ltz p1, :cond_f

    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->K:[Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;

    array-length v0, v0

    if-ge p1, v0, :cond_f

    .line 1420
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->K:[Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;

    aget-object p2, v0, p1

    .line 1424
    :cond_f
    if-eqz p2, :cond_13

    .line 1426
    iget-object p3, p2, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->j:Landroid/support/v7/internal/view/menu/i;

    .line 1431
    :cond_13
    if-eqz p2, :cond_1a

    iget-boolean v0, p2, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->o:Z

    if-nez v0, :cond_1a

    .line 1440
    :cond_19
    :goto_19
    return-void

    .line 30193
    :cond_1a
    iget-boolean v0, p0, Landroid/support/v7/app/ak;->r:Z

    .line 1434
    if-nez v0, :cond_19

    .line 1438
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->g:Landroid/view/Window$Callback;

    invoke-interface {v0, p1, p3}, Landroid/view/Window$Callback;->onPanelClosed(ILandroid/view/Menu;)V

    goto :goto_19
.end method

.method public final a(Landroid/content/res/Configuration;)V
    .registers 3

    .prologue
    .line 215
    iget-boolean v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->l:Z

    if-eqz v0, :cond_11

    iget-boolean v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->C:Z

    if-eqz v0, :cond_11

    .line 218
    invoke-virtual {p0}, Landroid/support/v7/app/AppCompatDelegateImplV7;->a()Landroid/support/v7/app/a;

    move-result-object v0

    .line 219
    if-eqz v0, :cond_11

    .line 220
    invoke-virtual {v0, p1}, Landroid/support/v7/app/a;->a(Landroid/content/res/Configuration;)V

    .line 223
    :cond_11
    return-void
.end method

.method final a(Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;Z)V
    .registers 7

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 1319
    if-eqz p2, :cond_1a

    iget v0, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->a:I

    if-nez v0, :cond_1a

    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->s:Landroid/support/v7/internal/widget/ac;

    if-eqz v0, :cond_1a

    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->s:Landroid/support/v7/internal/widget/ac;

    invoke-interface {v0}, Landroid/support/v7/internal/widget/ac;->d()Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 1321
    iget-object v0, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->j:Landroid/support/v7/internal/view/menu/i;

    invoke-virtual {p0, v0}, Landroid/support/v7/app/AppCompatDelegateImplV7;->b(Landroid/support/v7/internal/view/menu/i;)V

    .line 1348
    :cond_19
    :goto_19
    return-void

    .line 1325
    :cond_1a
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->e:Landroid/content/Context;

    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 1326
    if-eqz v0, :cond_3a

    iget-boolean v1, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->o:Z

    if-eqz v1, :cond_3a

    iget-object v1, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->g:Landroid/view/ViewGroup;

    if-eqz v1, :cond_3a

    .line 1327
    iget-object v1, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->g:Landroid/view/ViewGroup;

    invoke-interface {v0, v1}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    .line 1329
    if-eqz p2, :cond_3a

    .line 1330
    iget v0, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->a:I

    invoke-virtual {p0, v0, p1, v3}, Landroid/support/v7/app/AppCompatDelegateImplV7;->a(ILandroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;Landroid/view/Menu;)V

    .line 1334
    :cond_3a
    iput-boolean v2, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->m:Z

    .line 1335
    iput-boolean v2, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->n:Z

    .line 1336
    iput-boolean v2, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->o:Z

    .line 1339
    iput-object v3, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->h:Landroid/view/View;

    .line 1343
    const/4 v0, 0x1

    iput-boolean v0, p1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->q:Z

    .line 1345
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->L:Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;

    if-ne v0, p1, :cond_19

    .line 1346
    iput-object v3, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->L:Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;

    goto :goto_19
.end method

.method public final a(Landroid/support/v7/internal/view/menu/i;)V
    .registers 8

    .prologue
    const/16 v5, 0x6c

    const/4 v4, 0x0

    .line 617
    .line 9089
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->s:Landroid/support/v7/internal/widget/ac;

    if-eqz v0, :cond_81

    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->s:Landroid/support/v7/internal/widget/ac;

    invoke-interface {v0}, Landroid/support/v7/internal/widget/ac;->c()Z

    move-result v0

    if-eqz v0, :cond_81

    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->e:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/view/du;->b(Landroid/view/ViewConfiguration;)Z

    move-result v0

    if-eqz v0, :cond_23

    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->s:Landroid/support/v7/internal/widget/ac;

    invoke-interface {v0}, Landroid/support/v7/internal/widget/ac;->e()Z

    move-result v0

    if-eqz v0, :cond_81

    .line 9197
    :cond_23
    iget-object v0, p0, Landroid/support/v7/app/ak;->f:Landroid/view/Window;

    invoke-virtual {v0}, Landroid/view/Window;->getCallback()Landroid/view/Window$Callback;

    move-result-object v0

    .line 9095
    iget-object v1, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->s:Landroid/support/v7/internal/widget/ac;

    invoke-interface {v1}, Landroid/support/v7/internal/widget/ac;->d()Z

    move-result v1

    if-nez v1, :cond_6e

    .line 9096
    if-eqz v0, :cond_6d

    .line 10193
    iget-boolean v1, p0, Landroid/support/v7/app/ak;->r:Z

    .line 9096
    if-nez v1, :cond_6d

    .line 9098
    iget-boolean v1, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->y:Z

    if-eqz v1, :cond_4d

    iget v1, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->z:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_4d

    .line 9100
    iget-object v1, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->D:Landroid/view/ViewGroup;

    iget-object v2, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->M:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 9101
    iget-object v1, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->M:Ljava/lang/Runnable;

    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    .line 9104
    :cond_4d
    invoke-virtual {p0, v4}, Landroid/support/v7/app/AppCompatDelegateImplV7;->f(I)Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;

    move-result-object v1

    .line 9108
    iget-object v2, v1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->j:Landroid/support/v7/internal/view/menu/i;

    if-eqz v2, :cond_6d

    iget-boolean v2, v1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->r:Z

    if-nez v2, :cond_6d

    iget-object v2, v1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->i:Landroid/view/View;

    iget-object v3, v1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->j:Landroid/support/v7/internal/view/menu/i;

    invoke-interface {v0, v4, v2, v3}, Landroid/view/Window$Callback;->onPreparePanel(ILandroid/view/View;Landroid/view/Menu;)Z

    move-result v2

    if-eqz v2, :cond_6d

    .line 9110
    iget-object v1, v1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->j:Landroid/support/v7/internal/view/menu/i;

    invoke-interface {v0, v5, v1}, Landroid/view/Window$Callback;->onMenuOpened(ILandroid/view/Menu;)Z

    .line 9111
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->s:Landroid/support/v7/internal/widget/ac;

    invoke-interface {v0}, Landroid/support/v7/internal/widget/ac;->f()Z

    .line 9121
    :cond_6d
    :goto_6d
    return-void

    .line 9115
    :cond_6e
    iget-object v1, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->s:Landroid/support/v7/internal/widget/ac;

    invoke-interface {v1}, Landroid/support/v7/internal/widget/ac;->g()Z

    .line 11193
    iget-boolean v1, p0, Landroid/support/v7/app/ak;->r:Z

    .line 9116
    if-nez v1, :cond_6d

    .line 9117
    invoke-virtual {p0, v4}, Landroid/support/v7/app/AppCompatDelegateImplV7;->f(I)Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;

    move-result-object v1

    .line 9118
    iget-object v1, v1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->j:Landroid/support/v7/internal/view/menu/i;

    invoke-interface {v0, v5, v1}, Landroid/view/Window$Callback;->onPanelClosed(ILandroid/view/Menu;)V

    goto :goto_6d

    .line 9124
    :cond_81
    invoke-virtual {p0, v4}, Landroid/support/v7/app/AppCompatDelegateImplV7;->f(I)Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;

    move-result-object v0

    .line 9126
    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->q:Z

    .line 9127
    invoke-virtual {p0, v0, v4}, Landroid/support/v7/app/AppCompatDelegateImplV7;->a(Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;Z)V

    .line 9129
    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Landroid/support/v7/app/AppCompatDelegateImplV7;->a(Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;Landroid/view/KeyEvent;)V

    goto :goto_6d
.end method

.method public final a(Landroid/support/v7/widget/Toolbar;)V
    .registers 5

    .prologue
    .line 190
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->g:Landroid/view/Window$Callback;

    instance-of v0, v0, Landroid/app/Activity;

    if-nez v0, :cond_7

    .line 209
    :goto_6
    return-void

    .line 195
    :cond_7
    invoke-virtual {p0}, Landroid/support/v7/app/AppCompatDelegateImplV7;->a()Landroid/support/v7/app/a;

    move-result-object v0

    .line 196
    instance-of v0, v0, Landroid/support/v7/internal/a/l;

    if-eqz v0, :cond_17

    .line 197
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "This Activity already has an action bar supplied by the window decor. Do not request Window.FEATURE_SUPPORT_ACTION_BAR and set windowActionBar to false in your theme to use a Toolbar instead."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 202
    :cond_17
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->k:Landroid/view/MenuInflater;

    .line 204
    new-instance v1, Landroid/support/v7/internal/a/e;

    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->e:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    iget-object v2, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->h:Landroid/view/Window$Callback;

    invoke-direct {v1, p1, v0, v2}, Landroid/support/v7/internal/a/e;-><init>(Landroid/support/v7/widget/Toolbar;Ljava/lang/CharSequence;Landroid/view/Window$Callback;)V

    .line 206
    iput-object v1, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->j:Landroid/support/v7/app/a;

    .line 207
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->f:Landroid/view/Window;

    .line 3086
    iget-object v2, v1, Landroid/support/v7/internal/a/e;->k:Landroid/view/Window$Callback;

    .line 207
    invoke-virtual {v0, v2}, Landroid/view/Window;->setCallback(Landroid/view/Window$Callback;)V

    .line 208
    invoke-virtual {v1}, Landroid/support/v7/internal/a/e;->y()Z

    goto :goto_6
.end method

.method public final a(Landroid/view/View;)V
    .registers 4

    .prologue
    .line 243
    invoke-direct {p0}, Landroid/support/v7/app/AppCompatDelegateImplV7;->o()V

    .line 244
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->E:Landroid/view/ViewGroup;

    const v1, 0x1020002

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 245
    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 246
    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 247
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->g:Landroid/view/Window$Callback;

    invoke-interface {v0}, Landroid/view/Window$Callback;->onContentChanged()V

    .line 248
    return-void
.end method

.method public final a(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .registers 5

    .prologue
    .line 261
    invoke-direct {p0}, Landroid/support/v7/app/AppCompatDelegateImplV7;->o()V

    .line 262
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->E:Landroid/view/ViewGroup;

    const v1, 0x1020002

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 263
    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 264
    invoke-virtual {v0, p1, p2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 265
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->g:Landroid/view/Window$Callback;

    invoke-interface {v0}, Landroid/view/Window$Callback;->onContentChanged()V

    .line 266
    return-void
.end method

.method final a(ILandroid/view/KeyEvent;)Z
    .registers 7

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 812
    invoke-virtual {p0}, Landroid/support/v7/app/AppCompatDelegateImplV7;->a()Landroid/support/v7/app/a;

    move-result-object v2

    .line 813
    if-eqz v2, :cond_f

    invoke-virtual {v2, p1, p2}, Landroid/support/v7/app/a;->a(ILandroid/view/KeyEvent;)Z

    move-result v2

    if-eqz v2, :cond_f

    .line 843
    :cond_e
    :goto_e
    return v0

    .line 819
    :cond_f
    iget-object v2, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->L:Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;

    if-eqz v2, :cond_28

    .line 820
    iget-object v2, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->L:Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v3

    invoke-direct {p0, v2, v3, p2}, Landroid/support/v7/app/AppCompatDelegateImplV7;->a(Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;ILandroid/view/KeyEvent;)Z

    move-result v2

    .line 822
    if-eqz v2, :cond_28

    .line 823
    iget-object v1, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->L:Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;

    if-eqz v1, :cond_e

    .line 824
    iget-object v1, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->L:Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;

    iput-boolean v0, v1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->n:Z

    goto :goto_e

    .line 834
    :cond_28
    iget-object v2, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->L:Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;

    if-nez v2, :cond_3f

    .line 835
    invoke-virtual {p0, v1}, Landroid/support/v7/app/AppCompatDelegateImplV7;->f(I)Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;

    move-result-object v2

    .line 836
    invoke-direct {p0, v2, p2}, Landroid/support/v7/app/AppCompatDelegateImplV7;->b(Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;Landroid/view/KeyEvent;)Z

    .line 837
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v3

    invoke-direct {p0, v2, v3, p2}, Landroid/support/v7/app/AppCompatDelegateImplV7;->a(Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;ILandroid/view/KeyEvent;)Z

    move-result v3

    .line 838
    iput-boolean v1, v2, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->m:Z

    .line 839
    if-nez v3, :cond_e

    :cond_3f
    move v0, v1

    .line 843
    goto :goto_e
.end method

.method public final a(Landroid/support/v7/internal/view/menu/i;Landroid/view/MenuItem;)Z
    .registers 5

    .prologue
    .line 605
    .line 7197
    iget-object v0, p0, Landroid/support/v7/app/ak;->f:Landroid/view/Window;

    invoke-virtual {v0}, Landroid/view/Window;->getCallback()Landroid/view/Window$Callback;

    move-result-object v0

    .line 606
    if-eqz v0, :cond_1d

    .line 8193
    iget-boolean v1, p0, Landroid/support/v7/app/ak;->r:Z

    .line 606
    if-nez v1, :cond_1d

    .line 607
    invoke-virtual {p1}, Landroid/support/v7/internal/view/menu/i;->k()Landroid/support/v7/internal/view/menu/i;

    move-result-object v1

    invoke-virtual {p0, v1}, Landroid/support/v7/app/AppCompatDelegateImplV7;->a(Landroid/view/Menu;)Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;

    move-result-object v1

    .line 608
    if-eqz v1, :cond_1d

    .line 609
    iget v1, v1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->a:I

    invoke-interface {v0, v1, p2}, Landroid/view/Window$Callback;->onMenuItemSelected(ILandroid/view/MenuItem;)Z

    move-result v0

    .line 612
    :goto_1c
    return v0

    :cond_1d
    const/4 v0, 0x0

    goto :goto_1c
.end method

.method final a(Landroid/view/KeyEvent;)Z
    .registers 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 848
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/16 v3, 0x52

    if-ne v0, v3, :cond_14

    .line 850
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->g:Landroid/view/Window$Callback;

    invoke-interface {v0, p1}, Landroid/view/Window$Callback;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_14

    move v0, v1

    .line 12874
    :goto_13
    return v0

    .line 855
    :cond_14
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v3

    .line 856
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    .line 857
    if-nez v0, :cond_2f

    move v0, v1

    .line 859
    :goto_1f
    if-eqz v0, :cond_43

    .line 11882
    packed-switch v3, :pswitch_data_f8

    .line 11891
    :cond_24
    :goto_24
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_2d

    .line 11894
    invoke-virtual {p0, v3, p1}, Landroid/support/v7/app/AppCompatDelegateImplV7;->a(ILandroid/view/KeyEvent;)Z

    :cond_2d
    move v0, v2

    .line 859
    goto :goto_13

    :cond_2f
    move v0, v2

    .line 857
    goto :goto_1f

    .line 12351
    :pswitch_31
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v0

    if-nez v0, :cond_24

    .line 12352
    invoke-virtual {p0, v2}, Landroid/support/v7/app/AppCompatDelegateImplV7;->f(I)Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;

    move-result-object v0

    .line 12353
    iget-boolean v1, v0, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->o:Z

    if-nez v1, :cond_24

    .line 12354
    invoke-direct {p0, v0, p1}, Landroid/support/v7/app/AppCompatDelegateImplV7;->b(Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;Landroid/view/KeyEvent;)Z

    goto :goto_24

    .line 12863
    :cond_43
    sparse-switch v3, :sswitch_data_fe

    :cond_46
    move v0, v2

    .line 859
    goto :goto_13

    .line 13362
    :sswitch_48
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->t:Landroid/support/v7/c/a;

    if-nez v0, :cond_91

    .line 13367
    invoke-virtual {p0, v2}, Landroid/support/v7/app/AppCompatDelegateImplV7;->f(I)Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;

    move-result-object v3

    .line 13368
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->s:Landroid/support/v7/internal/widget/ac;

    if-eqz v0, :cond_9a

    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->s:Landroid/support/v7/internal/widget/ac;

    invoke-interface {v0}, Landroid/support/v7/internal/widget/ac;->c()Z

    move-result v0

    if-eqz v0, :cond_9a

    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->e:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/view/du;->b(Landroid/view/ViewConfiguration;)Z

    move-result v0

    if-nez v0, :cond_9a

    .line 13371
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->s:Landroid/support/v7/internal/widget/ac;

    invoke-interface {v0}, Landroid/support/v7/internal/widget/ac;->d()Z

    move-result v0

    if-nez v0, :cond_93

    .line 14193
    iget-boolean v0, p0, Landroid/support/v7/app/ak;->r:Z

    .line 13372
    if-nez v0, :cond_f4

    invoke-direct {p0, v3, p1}, Landroid/support/v7/app/AppCompatDelegateImplV7;->b(Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;Landroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_f4

    .line 13373
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->s:Landroid/support/v7/internal/widget/ac;

    invoke-interface {v0}, Landroid/support/v7/internal/widget/ac;->f()Z

    move-result v0

    .line 13402
    :goto_80
    if-eqz v0, :cond_91

    .line 13403
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->e:Landroid/content/Context;

    const-string v3, "audio"

    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 13405
    if-eqz v0, :cond_bd

    .line 13406
    invoke-virtual {v0, v2}, Landroid/media/AudioManager;->playSoundEffect(I)V

    :cond_91
    :goto_91
    move v0, v1

    .line 12866
    goto :goto_13

    .line 13376
    :cond_93
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->s:Landroid/support/v7/internal/widget/ac;

    invoke-interface {v0}, Landroid/support/v7/internal/widget/ac;->g()Z

    move-result v0

    goto :goto_80

    .line 13379
    :cond_9a
    iget-boolean v0, v3, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->o:Z

    if-nez v0, :cond_a2

    iget-boolean v0, v3, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->n:Z

    if-eqz v0, :cond_a8

    .line 13382
    :cond_a2
    iget-boolean v0, v3, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->o:Z

    .line 13384
    invoke-virtual {p0, v3, v1}, Landroid/support/v7/app/AppCompatDelegateImplV7;->a(Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;Z)V

    goto :goto_80

    .line 13385
    :cond_a8
    iget-boolean v0, v3, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->m:Z

    if-eqz v0, :cond_f4

    .line 13387
    iget-boolean v0, v3, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->r:Z

    if-eqz v0, :cond_f6

    .line 13390
    iput-boolean v2, v3, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->m:Z

    .line 13391
    invoke-direct {p0, v3, p1}, Landroid/support/v7/app/AppCompatDelegateImplV7;->b(Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;Landroid/view/KeyEvent;)Z

    move-result v0

    .line 13394
    :goto_b6
    if-eqz v0, :cond_f4

    .line 13396
    invoke-direct {p0, v3, p1}, Landroid/support/v7/app/AppCompatDelegateImplV7;->a(Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;Landroid/view/KeyEvent;)V

    move v0, v1

    .line 13397
    goto :goto_80

    .line 13408
    :cond_bd
    const-string v0, "AppCompatDelegate"

    const-string v2, "Couldn\'t get audio manager"

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_91

    .line 12868
    :sswitch_c5
    invoke-virtual {p0, v2}, Landroid/support/v7/app/AppCompatDelegateImplV7;->f(I)Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;

    move-result-object v0

    .line 12869
    if-eqz v0, :cond_d5

    iget-boolean v3, v0, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->o:Z

    if-eqz v3, :cond_d5

    .line 12870
    invoke-virtual {p0, v0, v1}, Landroid/support/v7/app/AppCompatDelegateImplV7;->a(Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;Z)V

    move v0, v1

    .line 12871
    goto/16 :goto_13

    .line 14794
    :cond_d5
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->t:Landroid/support/v7/c/a;

    if-eqz v0, :cond_e4

    .line 14795
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->t:Landroid/support/v7/c/a;

    invoke-virtual {v0}, Landroid/support/v7/c/a;->c()V

    move v0, v1

    .line 12873
    :goto_df
    if-eqz v0, :cond_46

    move v0, v1

    .line 12874
    goto/16 :goto_13

    .line 14800
    :cond_e4
    invoke-virtual {p0}, Landroid/support/v7/app/AppCompatDelegateImplV7;->a()Landroid/support/v7/app/a;

    move-result-object v0

    .line 14801
    if-eqz v0, :cond_f2

    invoke-virtual {v0}, Landroid/support/v7/app/a;->z()Z

    move-result v0

    if-eqz v0, :cond_f2

    move v0, v1

    .line 14802
    goto :goto_df

    :cond_f2
    move v0, v2

    .line 14806
    goto :goto_df

    :cond_f4
    move v0, v2

    goto :goto_80

    :cond_f6
    move v0, v1

    goto :goto_b6

    .line 11882
    :pswitch_data_f8
    .packed-switch 0x52
        :pswitch_31
    .end packed-switch

    .line 12863
    :sswitch_data_fe
    .sparse-switch
        0x4 -> :sswitch_c5
        0x52 -> :sswitch_48
    .end sparse-switch
.end method

.method final b(Landroid/support/v7/c/b;)Landroid/support/v7/c/a;
    .registers 10

    .prologue
    const/4 v7, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 658
    invoke-virtual {p0}, Landroid/support/v7/app/AppCompatDelegateImplV7;->n()V

    .line 659
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->t:Landroid/support/v7/c/a;

    if-eqz v0, :cond_f

    .line 660
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->t:Landroid/support/v7/c/a;

    invoke-virtual {v0}, Landroid/support/v7/c/a;->c()V

    .line 663
    :cond_f
    new-instance v3, Landroid/support/v7/app/ba;

    invoke-direct {v3, p0, p1}, Landroid/support/v7/app/ba;-><init>(Landroid/support/v7/app/AppCompatDelegateImplV7;Landroid/support/v7/c/b;)V

    .line 676
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->u:Landroid/support/v7/internal/widget/ActionBarContextView;

    if-nez v0, :cond_9b

    .line 677
    iget-boolean v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->o:Z

    if-eqz v0, :cond_101

    .line 679
    new-instance v4, Landroid/util/TypedValue;

    invoke-direct {v4}, Landroid/util/TypedValue;-><init>()V

    .line 680
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->e:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    .line 681
    sget v5, Landroid/support/v7/a/d;->actionBarTheme:I

    invoke-virtual {v0, v5, v4, v1}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 684
    iget v5, v4, Landroid/util/TypedValue;->resourceId:I

    if-eqz v5, :cond_fd

    .line 685
    iget-object v5, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->e:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->newTheme()Landroid/content/res/Resources$Theme;

    move-result-object v5

    .line 686
    invoke-virtual {v5, v0}, Landroid/content/res/Resources$Theme;->setTo(Landroid/content/res/Resources$Theme;)V

    .line 687
    iget v0, v4, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {v5, v0, v1}, Landroid/content/res/Resources$Theme;->applyStyle(IZ)V

    .line 689
    new-instance v0, Landroid/support/v7/internal/view/b;

    iget-object v6, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->e:Landroid/content/Context;

    invoke-direct {v0, v6, v2}, Landroid/support/v7/internal/view/b;-><init>(Landroid/content/Context;I)V

    .line 690
    invoke-virtual {v0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v6

    invoke-virtual {v6, v5}, Landroid/content/res/Resources$Theme;->setTo(Landroid/content/res/Resources$Theme;)V

    .line 695
    :goto_50
    new-instance v5, Landroid/support/v7/internal/widget/ActionBarContextView;

    invoke-direct {v5, v0}, Landroid/support/v7/internal/widget/ActionBarContextView;-><init>(Landroid/content/Context;)V

    iput-object v5, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->u:Landroid/support/v7/internal/widget/ActionBarContextView;

    .line 696
    new-instance v5, Landroid/widget/PopupWindow;

    sget v6, Landroid/support/v7/a/d;->actionModePopupWindowStyle:I

    invoke-direct {v5, v0, v7, v6}, Landroid/widget/PopupWindow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v5, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->v:Landroid/widget/PopupWindow;

    .line 698
    iget-object v5, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->v:Landroid/widget/PopupWindow;

    invoke-static {v5}, Landroid/support/v4/widget/bo;->b(Landroid/widget/PopupWindow;)V

    .line 700
    iget-object v5, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->v:Landroid/widget/PopupWindow;

    iget-object v6, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->u:Landroid/support/v7/internal/widget/ActionBarContextView;

    invoke-virtual {v5, v6}, Landroid/widget/PopupWindow;->setContentView(Landroid/view/View;)V

    .line 701
    iget-object v5, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->v:Landroid/widget/PopupWindow;

    const/4 v6, -0x1

    invoke-virtual {v5, v6}, Landroid/widget/PopupWindow;->setWidth(I)V

    .line 703
    invoke-virtual {v0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v5

    sget v6, Landroid/support/v7/a/d;->actionBarSize:I

    invoke-virtual {v5, v6, v4, v1}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 705
    iget v4, v4, Landroid/util/TypedValue;->data:I

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    invoke-static {v4, v0}, Landroid/util/TypedValue;->complexToDimensionPixelSize(ILandroid/util/DisplayMetrics;)I

    move-result v0

    .line 707
    iget-object v4, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->u:Landroid/support/v7/internal/widget/ActionBarContextView;

    invoke-virtual {v4, v0}, Landroid/support/v7/internal/widget/ActionBarContextView;->setContentHeight(I)V

    .line 708
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->v:Landroid/widget/PopupWindow;

    const/4 v4, -0x2

    invoke-virtual {v0, v4}, Landroid/widget/PopupWindow;->setHeight(I)V

    .line 709
    new-instance v0, Landroid/support/v7/app/aw;

    invoke-direct {v0, p0}, Landroid/support/v7/app/aw;-><init>(Landroid/support/v7/app/AppCompatDelegateImplV7;)V

    iput-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->w:Ljava/lang/Runnable;

    .line 743
    :cond_9b
    :goto_9b
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->u:Landroid/support/v7/internal/widget/ActionBarContextView;

    if-eqz v0, :cond_fa

    .line 744
    invoke-virtual {p0}, Landroid/support/v7/app/AppCompatDelegateImplV7;->n()V

    .line 745
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->u:Landroid/support/v7/internal/widget/ActionBarContextView;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarContextView;->i()V

    .line 746
    new-instance v4, Landroid/support/v7/internal/view/c;

    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->u:Landroid/support/v7/internal/widget/ActionBarContextView;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarContextView;->getContext()Landroid/content/Context;

    move-result-object v5

    iget-object v6, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->u:Landroid/support/v7/internal/widget/ActionBarContextView;

    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->v:Landroid/widget/PopupWindow;

    if-nez v0, :cond_122

    move v0, v1

    :goto_b6
    invoke-direct {v4, v5, v6, v3, v0}, Landroid/support/v7/internal/view/c;-><init>(Landroid/content/Context;Landroid/support/v7/internal/widget/ActionBarContextView;Landroid/support/v7/c/b;Z)V

    .line 748
    invoke-virtual {v4}, Landroid/support/v7/c/a;->b()Landroid/view/Menu;

    move-result-object v0

    invoke-interface {p1, v4, v0}, Landroid/support/v7/c/b;->a(Landroid/support/v7/c/a;Landroid/view/Menu;)Z

    move-result v0

    if-eqz v0, :cond_124

    .line 749
    invoke-virtual {v4}, Landroid/support/v7/c/a;->d()V

    .line 750
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->u:Landroid/support/v7/internal/widget/ActionBarContextView;

    invoke-virtual {v0, v4}, Landroid/support/v7/internal/widget/ActionBarContextView;->a(Landroid/support/v7/c/a;)V

    .line 751
    iput-object v4, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->t:Landroid/support/v7/c/a;

    .line 752
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->u:Landroid/support/v7/internal/widget/ActionBarContextView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/support/v4/view/cx;->c(Landroid/view/View;F)V

    .line 753
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->u:Landroid/support/v7/internal/widget/ActionBarContextView;

    invoke-static {v0}, Landroid/support/v4/view/cx;->p(Landroid/view/View;)Landroid/support/v4/view/fk;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/support/v4/view/fk;->a(F)Landroid/support/v4/view/fk;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->x:Landroid/support/v4/view/fk;

    .line 754
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->x:Landroid/support/v4/view/fk;

    new-instance v1, Landroid/support/v7/app/ay;

    invoke-direct {v1, p0}, Landroid/support/v7/app/ay;-><init>(Landroid/support/v7/app/AppCompatDelegateImplV7;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/view/fk;->a(Landroid/support/v4/view/gd;)Landroid/support/v4/view/fk;

    .line 772
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->v:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_fa

    .line 773
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->f:Landroid/view/Window;

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->w:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 783
    :cond_fa
    :goto_fa
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->t:Landroid/support/v7/c/a;

    return-object v0

    .line 692
    :cond_fd
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->e:Landroid/content/Context;

    goto/16 :goto_50

    .line 733
    :cond_101
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->E:Landroid/view/ViewGroup;

    sget v4, Landroid/support/v7/a/i;->action_mode_bar_stub:I

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ViewStubCompat;

    .line 735
    if-eqz v0, :cond_9b

    .line 737
    invoke-virtual {p0}, Landroid/support/v7/app/AppCompatDelegateImplV7;->m()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/support/v7/internal/widget/ViewStubCompat;->setLayoutInflater(Landroid/view/LayoutInflater;)V

    .line 738
    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ViewStubCompat;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ActionBarContextView;

    iput-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->u:Landroid/support/v7/internal/widget/ActionBarContextView;

    goto/16 :goto_9b

    :cond_122
    move v0, v2

    .line 746
    goto :goto_b6

    .line 776
    :cond_124
    iput-object v7, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->t:Landroid/support/v7/c/a;

    goto :goto_fa
.end method

.method public final b(Landroid/view/View;Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;
    .registers 11
    .param p3    # Landroid/content/Context;
        .annotation build Landroid/support/a/y;
        .end annotation
    .end param
    .param p4    # Landroid/util/AttributeSet;
        .annotation build Landroid/support/a/y;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 902
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-ge v0, v1, :cond_44

    move v2, v3

    .line 904
    :goto_9
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->Q:Landroid/support/v7/internal/a/a;

    if-nez v0, :cond_14

    .line 905
    new-instance v0, Landroid/support/v7/internal/a/a;

    invoke-direct {v0}, Landroid/support/v7/internal/a/a;-><init>()V

    iput-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->Q:Landroid/support/v7/internal/a/a;

    .line 909
    :cond_14
    if-eqz v2, :cond_61

    iget-boolean v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->C:Z

    if-eqz v0, :cond_61

    move-object v0, p1

    check-cast v0, Landroid/view/ViewParent;

    .line 17919
    if-nez v0, :cond_116

    move v0, v4

    .line 909
    :goto_20
    if-eqz v0, :cond_61

    move v0, v3

    .line 912
    :goto_23
    iget-object v1, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->Q:Landroid/support/v7/internal/a/a;

    .line 18074
    if-eqz v0, :cond_113

    if-eqz p1, :cond_113

    .line 18075
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 18079
    :goto_2d
    invoke-static {v0, p4, v2}, Landroid/support/v7/internal/a/a;->a(Landroid/content/Context;Landroid/util/AttributeSet;Z)Landroid/content/Context;

    move-result-object v2

    .line 18083
    const/4 v0, -0x1

    invoke-virtual {p2}, Ljava/lang/String;->hashCode()I

    move-result v5

    sparse-switch v5, :sswitch_data_11a

    :cond_39
    move v4, v0

    :goto_3a
    packed-switch v4, :pswitch_data_144

    .line 18106
    if-eq p3, v2, :cond_110

    .line 18109
    invoke-virtual {v1, v2, p2, p4}, Landroid/support/v7/internal/a/a;->a(Landroid/content/Context;Ljava/lang/String;Landroid/util/AttributeSet;)Landroid/view/View;

    move-result-object v0

    :goto_43
    return-object v0

    :cond_44
    move v2, v4

    .line 902
    goto :goto_9

    .line 17938
    :cond_46
    invoke-interface {v1}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    .line 17924
    :goto_4a
    if-nez v1, :cond_4e

    move v0, v3

    .line 17929
    goto :goto_20

    .line 17930
    :cond_4e
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->D:Landroid/view/ViewGroup;

    if-eq v1, v0, :cond_5f

    instance-of v0, v1, Landroid/view/View;

    if-eqz v0, :cond_5f

    move-object v0, v1

    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Landroid/support/v4/view/cx;->D(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_46

    :cond_5f
    move v0, v4

    .line 17936
    goto :goto_20

    :cond_61
    move v0, v4

    .line 909
    goto :goto_23

    .line 18083
    :sswitch_63
    const-string v3, "EditText"

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_39

    goto :goto_3a

    :sswitch_6c
    const-string v4, "Spinner"

    invoke-virtual {p2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_39

    move v4, v3

    goto :goto_3a

    :sswitch_76
    const-string v3, "CheckBox"

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_39

    const/4 v4, 0x2

    goto :goto_3a

    :sswitch_80
    const-string v3, "RadioButton"

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_39

    const/4 v4, 0x3

    goto :goto_3a

    :sswitch_8a
    const-string v3, "CheckedTextView"

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_39

    const/4 v4, 0x4

    goto :goto_3a

    :sswitch_94
    const-string v3, "AutoCompleteTextView"

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_39

    const/4 v4, 0x5

    goto :goto_3a

    :sswitch_9e
    const-string v3, "MultiAutoCompleteTextView"

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_39

    const/4 v4, 0x6

    goto :goto_3a

    :sswitch_a8
    const-string v3, "RatingBar"

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_39

    const/4 v4, 0x7

    goto :goto_3a

    :sswitch_b2
    const-string v3, "Button"

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_39

    const/16 v4, 0x8

    goto/16 :goto_3a

    :sswitch_be
    const-string v3, "TextView"

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_39

    const/16 v4, 0x9

    goto/16 :goto_3a

    .line 18085
    :pswitch_ca
    new-instance v0, Landroid/support/v7/widget/w;

    invoke-direct {v0, v2, p4}, Landroid/support/v7/widget/w;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    goto/16 :goto_43

    .line 18087
    :pswitch_d1
    new-instance v0, Landroid/support/v7/widget/aa;

    invoke-direct {v0, v2, p4}, Landroid/support/v7/widget/aa;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    goto/16 :goto_43

    .line 18089
    :pswitch_d8
    new-instance v0, Landroid/support/v7/widget/s;

    invoke-direct {v0, v2, p4}, Landroid/support/v7/widget/s;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    goto/16 :goto_43

    .line 18091
    :pswitch_df
    new-instance v0, Landroid/support/v7/widget/y;

    invoke-direct {v0, v2, p4}, Landroid/support/v7/widget/y;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    goto/16 :goto_43

    .line 18093
    :pswitch_e6
    new-instance v0, Landroid/support/v7/widget/t;

    invoke-direct {v0, v2, p4}, Landroid/support/v7/widget/t;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    goto/16 :goto_43

    .line 18095
    :pswitch_ed
    new-instance v0, Landroid/support/v7/widget/p;

    invoke-direct {v0, v2, p4}, Landroid/support/v7/widget/p;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    goto/16 :goto_43

    .line 18097
    :pswitch_f4
    new-instance v0, Landroid/support/v7/widget/x;

    invoke-direct {v0, v2, p4}, Landroid/support/v7/widget/x;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    goto/16 :goto_43

    .line 18099
    :pswitch_fb
    new-instance v0, Landroid/support/v7/widget/z;

    invoke-direct {v0, v2, p4}, Landroid/support/v7/widget/z;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    goto/16 :goto_43

    .line 18101
    :pswitch_102
    new-instance v0, Landroid/support/v7/widget/r;

    invoke-direct {v0, v2, p4}, Landroid/support/v7/widget/r;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    goto/16 :goto_43

    .line 18103
    :pswitch_109
    new-instance v0, Landroid/support/v7/widget/ai;

    invoke-direct {v0, v2, p4}, Landroid/support/v7/widget/ai;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    goto/16 :goto_43

    .line 18112
    :cond_110
    const/4 v0, 0x0

    .line 912
    goto/16 :goto_43

    :cond_113
    move-object v0, p3

    goto/16 :goto_2d

    :cond_116
    move-object v1, v0

    goto/16 :goto_4a

    .line 18083
    nop

    :sswitch_data_11a
    .sparse-switch
        -0x7404ceea -> :sswitch_a8
        -0x56c015e7 -> :sswitch_8a
        -0x503aa7ad -> :sswitch_9e
        -0x37f7066e -> :sswitch_be
        -0x1440b607 -> :sswitch_6c
        0x2e46a6ed -> :sswitch_80
        0x5445f9ba -> :sswitch_94
        0x5f7507c3 -> :sswitch_76
        0x63577677 -> :sswitch_63
        0x77471352 -> :sswitch_b2
    .end sparse-switch

    :pswitch_data_144
    .packed-switch 0x0
        :pswitch_ca
        :pswitch_d1
        :pswitch_d8
        :pswitch_df
        :pswitch_e6
        :pswitch_ed
        :pswitch_f4
        :pswitch_fb
        :pswitch_102
        :pswitch_109
    .end packed-switch
.end method

.method final b(Landroid/support/v7/internal/view/menu/i;)V
    .registers 4

    .prologue
    .line 1301
    iget-boolean v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->J:Z

    if-eqz v0, :cond_5

    .line 1312
    :goto_4
    return-void

    .line 1305
    :cond_5
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->J:Z

    .line 1306
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->s:Landroid/support/v7/internal/widget/ac;

    invoke-interface {v0}, Landroid/support/v7/internal/widget/ac;->i()V

    .line 27197
    iget-object v0, p0, Landroid/support/v7/app/ak;->f:Landroid/view/Window;

    invoke-virtual {v0}, Landroid/view/Window;->getCallback()Landroid/view/Window$Callback;

    move-result-object v0

    .line 1308
    if-eqz v0, :cond_1e

    .line 28193
    iget-boolean v1, p0, Landroid/support/v7/app/ak;->r:Z

    .line 1308
    if-nez v1, :cond_1e

    .line 1309
    const/16 v1, 0x6c

    invoke-interface {v0, v1, p1}, Landroid/view/Window$Callback;->onPanelClosed(ILandroid/view/Menu;)V

    .line 1311
    :cond_1e
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->J:Z

    goto :goto_4
.end method

.method public final b(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .registers 5

    .prologue
    .line 270
    invoke-direct {p0}, Landroid/support/v7/app/AppCompatDelegateImplV7;->o()V

    .line 271
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->E:Landroid/view/ViewGroup;

    const v1, 0x1020002

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 272
    invoke-virtual {v0, p1, p2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 273
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->g:Landroid/view/Window$Callback;

    invoke-interface {v0}, Landroid/view/Window$Callback;->onContentChanged()V

    .line 274
    return-void
.end method

.method final b(Ljava/lang/CharSequence;)V
    .registers 3

    .prologue
    .line 565
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->s:Landroid/support/v7/internal/widget/ac;

    if-eqz v0, :cond_a

    .line 566
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->s:Landroid/support/v7/internal/widget/ac;

    invoke-interface {v0, p1}, Landroid/support/v7/internal/widget/ac;->setWindowTitle(Ljava/lang/CharSequence;)V

    .line 572
    :cond_9
    :goto_9
    return-void

    .line 6092
    :cond_a
    iget-object v0, p0, Landroid/support/v7/app/ak;->j:Landroid/support/v7/app/a;

    .line 567
    if-eqz v0, :cond_14

    .line 7092
    iget-object v0, p0, Landroid/support/v7/app/ak;->j:Landroid/support/v7/app/a;

    .line 568
    invoke-virtual {v0, p1}, Landroid/support/v7/app/a;->d(Ljava/lang/CharSequence;)V

    goto :goto_9

    .line 569
    :cond_14
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->F:Landroid/widget/TextView;

    if-eqz v0, :cond_9

    .line 570
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->F:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_9
.end method

.method public final b(I)Z
    .registers 6

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 503
    invoke-static {p1}, Landroid/support/v7/app/AppCompatDelegateImplV7;->k(I)I

    move-result v2

    .line 505
    iget-boolean v3, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->p:Z

    if-eqz v3, :cond_f

    const/16 v3, 0x6c

    if-ne v2, v3, :cond_f

    .line 540
    :goto_e
    return v0

    .line 508
    :cond_f
    iget-boolean v3, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->l:Z

    if-eqz v3, :cond_17

    if-ne v2, v1, :cond_17

    .line 510
    iput-boolean v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->l:Z

    .line 513
    :cond_17
    sparse-switch v2, :sswitch_data_4c

    .line 540
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->f:Landroid/view/Window;

    invoke-virtual {v0, v2}, Landroid/view/Window;->requestFeature(I)Z

    move-result v0

    goto :goto_e

    .line 515
    :sswitch_21
    invoke-direct {p0}, Landroid/support/v7/app/AppCompatDelegateImplV7;->u()V

    .line 516
    iput-boolean v1, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->l:Z

    move v0, v1

    .line 517
    goto :goto_e

    .line 519
    :sswitch_28
    invoke-direct {p0}, Landroid/support/v7/app/AppCompatDelegateImplV7;->u()V

    .line 520
    iput-boolean v1, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->m:Z

    move v0, v1

    .line 521
    goto :goto_e

    .line 523
    :sswitch_2f
    invoke-direct {p0}, Landroid/support/v7/app/AppCompatDelegateImplV7;->u()V

    .line 524
    iput-boolean v1, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->n:Z

    move v0, v1

    .line 525
    goto :goto_e

    .line 527
    :sswitch_36
    invoke-direct {p0}, Landroid/support/v7/app/AppCompatDelegateImplV7;->u()V

    .line 528
    iput-boolean v1, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->H:Z

    move v0, v1

    .line 529
    goto :goto_e

    .line 531
    :sswitch_3d
    invoke-direct {p0}, Landroid/support/v7/app/AppCompatDelegateImplV7;->u()V

    .line 532
    iput-boolean v1, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->I:Z

    move v0, v1

    .line 533
    goto :goto_e

    .line 535
    :sswitch_44
    invoke-direct {p0}, Landroid/support/v7/app/AppCompatDelegateImplV7;->u()V

    .line 536
    iput-boolean v1, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->p:Z

    move v0, v1

    .line 537
    goto :goto_e

    .line 513
    nop

    :sswitch_data_4c
    .sparse-switch
        0x1 -> :sswitch_44
        0x2 -> :sswitch_36
        0x5 -> :sswitch_3d
        0xa -> :sswitch_2f
        0x6c -> :sswitch_21
        0x6d -> :sswitch_28
    .end sparse-switch
.end method

.method public final c()V
    .registers 3

    .prologue
    const/4 v1, 0x1

    .line 148
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->f:Landroid/view/Window;

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->D:Landroid/view/ViewGroup;

    .line 150
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->g:Landroid/view/Window$Callback;

    instance-of v0, v0, Landroid/app/Activity;

    if-eqz v0, :cond_21

    .line 151
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->g:Landroid/view/Window$Callback;

    check-cast v0, Landroid/app/Activity;

    invoke-static {v0}, Landroid/support/v4/app/cv;->b(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_21

    .line 2092
    iget-object v0, p0, Landroid/support/v7/app/ak;->j:Landroid/support/v7/app/a;

    .line 154
    if-nez v0, :cond_22

    .line 155
    iput-boolean v1, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->N:Z

    .line 161
    :cond_21
    :goto_21
    return-void

    .line 157
    :cond_22
    invoke-virtual {v0, v1}, Landroid/support/v7/app/a;->f(Z)V

    goto :goto_21
.end method

.method public final c(I)Z
    .registers 4

    .prologue
    .line 545
    invoke-static {p1}, Landroid/support/v7/app/AppCompatDelegateImplV7;->k(I)I

    move-result v0

    .line 546
    sparse-switch v0, :sswitch_data_20

    .line 560
    iget-object v1, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->f:Landroid/view/Window;

    invoke-virtual {v1, v0}, Landroid/view/Window;->hasFeature(I)Z

    move-result v0

    :goto_d
    return v0

    .line 548
    :sswitch_e
    iget-boolean v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->l:Z

    goto :goto_d

    .line 550
    :sswitch_11
    iget-boolean v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->m:Z

    goto :goto_d

    .line 552
    :sswitch_14
    iget-boolean v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->n:Z

    goto :goto_d

    .line 554
    :sswitch_17
    iget-boolean v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->H:Z

    goto :goto_d

    .line 556
    :sswitch_1a
    iget-boolean v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->I:Z

    goto :goto_d

    .line 558
    :sswitch_1d
    iget-boolean v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->p:Z

    goto :goto_d

    .line 546
    :sswitch_data_20
    .sparse-switch
        0x1 -> :sswitch_1d
        0x2 -> :sswitch_17
        0x5 -> :sswitch_1a
        0xa -> :sswitch_14
        0x6c -> :sswitch_e
        0x6d -> :sswitch_11
    .end sparse-switch
.end method

.method public final d()V
    .registers 1

    .prologue
    .line 166
    invoke-direct {p0}, Landroid/support/v7/app/AppCompatDelegateImplV7;->o()V

    .line 167
    return-void
.end method

.method final d(I)V
    .registers 5

    .prologue
    const/4 v2, 0x0

    .line 576
    const/16 v0, 0x6c

    if-ne p1, v0, :cond_f

    .line 577
    invoke-virtual {p0}, Landroid/support/v7/app/AppCompatDelegateImplV7;->a()Landroid/support/v7/app/a;

    move-result-object v0

    .line 578
    if-eqz v0, :cond_e

    .line 579
    invoke-virtual {v0, v2}, Landroid/support/v7/app/a;->h(Z)V

    .line 589
    :cond_e
    :goto_e
    return-void

    .line 581
    :cond_f
    if-nez p1, :cond_e

    .line 584
    invoke-virtual {p0, p1}, Landroid/support/v7/app/AppCompatDelegateImplV7;->f(I)Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;

    move-result-object v0

    .line 585
    iget-boolean v1, v0, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;->o:Z

    if-eqz v1, :cond_e

    .line 586
    invoke-virtual {p0, v0, v2}, Landroid/support/v7/app/AppCompatDelegateImplV7;->a(Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;Z)V

    goto :goto_e
.end method

.method public final e()V
    .registers 3

    .prologue
    .line 227
    invoke-virtual {p0}, Landroid/support/v7/app/AppCompatDelegateImplV7;->a()Landroid/support/v7/app/a;

    move-result-object v0

    .line 228
    if-eqz v0, :cond_a

    .line 229
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/app/a;->g(Z)V

    .line 231
    :cond_a
    return-void
.end method

.method final e(I)Z
    .registers 4

    .prologue
    const/4 v0, 0x1

    .line 593
    const/16 v1, 0x6c

    if-ne p1, v1, :cond_f

    .line 594
    invoke-virtual {p0}, Landroid/support/v7/app/AppCompatDelegateImplV7;->a()Landroid/support/v7/app/a;

    move-result-object v1

    .line 595
    if-eqz v1, :cond_e

    .line 596
    invoke-virtual {v1, v0}, Landroid/support/v7/app/a;->h(Z)V

    .line 600
    :cond_e
    :goto_e
    return v0

    :cond_f
    const/4 v0, 0x0

    goto :goto_e
.end method

.method final f(I)Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;
    .registers 6

    .prologue
    const/4 v3, 0x0

    .line 1456
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->K:[Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;

    if-eqz v0, :cond_8

    array-length v1, v0

    if-gt v1, p1, :cond_15

    .line 1457
    :cond_8
    add-int/lit8 v1, p1, 0x1

    new-array v1, v1, [Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;

    .line 1458
    if-eqz v0, :cond_12

    .line 1459
    array-length v2, v0

    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1461
    :cond_12
    iput-object v1, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->K:[Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;

    move-object v0, v1

    .line 1464
    :cond_15
    aget-object v1, v0, p1

    .line 1465
    if-nez v1, :cond_22

    .line 1466
    new-instance v1, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;

    invoke-direct {v1, p1}, Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState;-><init>(I)V

    aput-object v1, v0, p1

    move-object v0, v1

    .line 1468
    :goto_21
    return-object v0

    :cond_22
    move-object v0, v1

    goto :goto_21
.end method

.method public final f()V
    .registers 3

    .prologue
    .line 235
    invoke-virtual {p0}, Landroid/support/v7/app/AppCompatDelegateImplV7;->a()Landroid/support/v7/app/a;

    move-result-object v0

    .line 236
    if-eqz v0, :cond_a

    .line 237
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/a;->g(Z)V

    .line 239
    :cond_a
    return-void
.end method

.method public final g()V
    .registers 2

    .prologue
    .line 650
    invoke-virtual {p0}, Landroid/support/v7/app/AppCompatDelegateImplV7;->a()Landroid/support/v7/app/a;

    move-result-object v0

    .line 651
    if-eqz v0, :cond_d

    invoke-virtual {v0}, Landroid/support/v7/app/a;->y()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 654
    :goto_c
    return-void

    .line 653
    :cond_d
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Landroid/support/v7/app/AppCompatDelegateImplV7;->h(I)V

    goto :goto_c
.end method

.method public final j()V
    .registers 3

    .prologue
    .line 944
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->e:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 945
    invoke-virtual {v0}, Landroid/view/LayoutInflater;->getFactory()Landroid/view/LayoutInflater$Factory;

    move-result-object v1

    if-nez v1, :cond_10

    .line 946
    invoke-static {v0, p0}, Landroid/support/v4/view/ai;->a(Landroid/view/LayoutInflater;Landroid/support/v4/view/as;)V

    .line 951
    :goto_f
    return-void

    .line 948
    :cond_10
    const-string v0, "AppCompatDelegate"

    const-string v1, "The Activity\'s LayoutInflater already has a Factory installed so we can not install AppCompat\'s"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_f
.end method

.method public final l()V
    .registers 4

    .prologue
    .line 171
    invoke-direct {p0}, Landroid/support/v7/app/AppCompatDelegateImplV7;->o()V

    .line 173
    iget-boolean v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->l:Z

    if-eqz v0, :cond_b

    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->j:Landroid/support/v7/app/a;

    if-eqz v0, :cond_c

    .line 186
    :cond_b
    :goto_b
    return-void

    .line 177
    :cond_c
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->g:Landroid/view/Window$Callback;

    instance-of v0, v0, Landroid/app/Activity;

    if-eqz v0, :cond_2b

    .line 178
    new-instance v1, Landroid/support/v7/internal/a/l;

    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->g:Landroid/view/Window$Callback;

    check-cast v0, Landroid/app/Activity;

    iget-boolean v2, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->m:Z

    invoke-direct {v1, v0, v2}, Landroid/support/v7/internal/a/l;-><init>(Landroid/app/Activity;Z)V

    iput-object v1, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->j:Landroid/support/v7/app/a;

    .line 183
    :cond_1f
    :goto_1f
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->j:Landroid/support/v7/app/a;

    if-eqz v0, :cond_b

    .line 184
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->j:Landroid/support/v7/app/a;

    iget-boolean v1, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->N:Z

    invoke-virtual {v0, v1}, Landroid/support/v7/app/a;->f(Z)V

    goto :goto_b

    .line 180
    :cond_2b
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->g:Landroid/view/Window$Callback;

    instance-of v0, v0, Landroid/app/Dialog;

    if-eqz v0, :cond_1f

    .line 181
    new-instance v1, Landroid/support/v7/internal/a/l;

    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->g:Landroid/view/Window$Callback;

    check-cast v0, Landroid/app/Dialog;

    invoke-direct {v1, v0}, Landroid/support/v7/internal/a/l;-><init>(Landroid/app/Dialog;)V

    iput-object v1, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->j:Landroid/support/v7/app/a;

    goto :goto_1f
.end method

.method final n()V
    .registers 2

    .prologue
    .line 787
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->x:Landroid/support/v4/view/fk;

    if-eqz v0, :cond_9

    .line 788
    iget-object v0, p0, Landroid/support/v7/app/AppCompatDelegateImplV7;->x:Landroid/support/v4/view/fk;

    invoke-virtual {v0}, Landroid/support/v4/view/fk;->a()V

    .line 790
    :cond_9
    return-void
.end method
