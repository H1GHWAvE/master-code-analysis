.class final Landroid/support/v7/app/bp;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/lang/String; = "TwilightManager"

.field private static final b:I = 0x6

.field private static final c:I = 0x16

.field private static final d:Landroid/support/v7/app/br;


# instance fields
.field private final e:Landroid/content/Context;

.field private final f:Landroid/location/LocationManager;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 40
    new-instance v0, Landroid/support/v7/app/br;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Landroid/support/v7/app/br;-><init>(B)V

    sput-object v0, Landroid/support/v7/app/bp;->d:Landroid/support/v7/app/br;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .registers 3

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput-object p1, p0, Landroid/support/v7/app/bp;->e:Landroid/content/Context;

    .line 47
    const-string v0, "location"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    iput-object v0, p0, Landroid/support/v7/app/bp;->f:Landroid/location/LocationManager;

    .line 48
    return-void
.end method

.method private a(Ljava/lang/String;)Landroid/location/Location;
    .registers 5

    .prologue
    .line 111
    iget-object v0, p0, Landroid/support/v7/app/bp;->f:Landroid/location/LocationManager;

    if-eqz v0, :cond_1b

    .line 113
    :try_start_4
    iget-object v0, p0, Landroid/support/v7/app/bp;->f:Landroid/location/LocationManager;

    invoke-virtual {v0, p1}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 114
    iget-object v0, p0, Landroid/support/v7/app/bp;->f:Landroid/location/LocationManager;

    invoke-virtual {v0, p1}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;
    :try_end_11
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_11} :catch_13

    move-result-object v0

    .line 120
    :goto_12
    return-object v0

    .line 116
    :catch_13
    move-exception v0

    .line 117
    const-string v1, "TwilightManager"

    const-string v2, "Failed to get last known location"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 120
    :cond_1b
    const/4 v0, 0x0

    goto :goto_12
.end method

.method private static a(Landroid/location/Location;)V
    .registers 21
    .param p0    # Landroid/location/Location;
        .annotation build Landroid/support/a/y;
        .end annotation
    .end param

    .prologue
    .line 128
    sget-object v10, Landroid/support/v7/app/bp;->d:Landroid/support/v7/app/br;

    .line 129
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    .line 4031
    sget-object v2, Landroid/support/v7/app/bo;->a:Landroid/support/v7/app/bo;

    if-nez v2, :cond_11

    .line 4032
    new-instance v2, Landroid/support/v7/app/bo;

    invoke-direct {v2}, Landroid/support/v7/app/bo;-><init>()V

    sput-object v2, Landroid/support/v7/app/bo;->a:Landroid/support/v7/app/bo;

    .line 4034
    :cond_11
    sget-object v3, Landroid/support/v7/app/bo;->a:Landroid/support/v7/app/bo;

    .line 133
    const-wide/32 v4, 0x5265c00

    sub-long v4, v12, v4

    invoke-virtual/range {p0 .. p0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v6

    invoke-virtual/range {p0 .. p0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v8

    invoke-virtual/range {v3 .. v9}, Landroid/support/v7/app/bo;->a(JDD)V

    .line 135
    iget-wide v14, v3, Landroid/support/v7/app/bo;->d:J

    .line 138
    invoke-virtual/range {p0 .. p0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v6

    invoke-virtual/range {p0 .. p0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v8

    move-wide v4, v12

    invoke-virtual/range {v3 .. v9}, Landroid/support/v7/app/bo;->a(JDD)V

    .line 139
    iget v2, v3, Landroid/support/v7/app/bo;->f:I

    const/4 v4, 0x1

    if-ne v2, v4, :cond_71

    const/4 v2, 0x1

    .line 140
    :goto_37
    iget-wide v0, v3, Landroid/support/v7/app/bo;->e:J

    move-wide/from16 v16, v0

    .line 141
    iget-wide v0, v3, Landroid/support/v7/app/bo;->d:J

    move-wide/from16 v18, v0

    .line 144
    const-wide/32 v4, 0x5265c00

    add-long/2addr v4, v12

    invoke-virtual/range {p0 .. p0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v6

    invoke-virtual/range {p0 .. p0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v8

    invoke-virtual/range {v3 .. v9}, Landroid/support/v7/app/bo;->a(JDD)V

    .line 146
    iget-wide v6, v3, Landroid/support/v7/app/bo;->e:J

    .line 150
    const-wide/16 v4, -0x1

    cmp-long v3, v16, v4

    if-eqz v3, :cond_5c

    const-wide/16 v4, -0x1

    cmp-long v3, v18, v4

    if-nez v3, :cond_73

    .line 152
    :cond_5c
    const-wide/32 v4, 0x2932e00

    add-long/2addr v4, v12

    .line 166
    :goto_60
    iput-boolean v2, v10, Landroid/support/v7/app/br;->a:Z

    .line 167
    iput-wide v14, v10, Landroid/support/v7/app/br;->b:J

    .line 168
    move-wide/from16 v0, v16

    iput-wide v0, v10, Landroid/support/v7/app/br;->c:J

    .line 169
    move-wide/from16 v0, v18

    iput-wide v0, v10, Landroid/support/v7/app/br;->d:J

    .line 170
    iput-wide v6, v10, Landroid/support/v7/app/br;->e:J

    .line 171
    iput-wide v4, v10, Landroid/support/v7/app/br;->f:J

    .line 172
    return-void

    .line 139
    :cond_71
    const/4 v2, 0x0

    goto :goto_37

    .line 154
    :cond_73
    cmp-long v3, v12, v18

    if-lez v3, :cond_7f

    .line 155
    const-wide/16 v4, 0x0

    add-long/2addr v4, v6

    .line 162
    :goto_7a
    const-wide/32 v8, 0xea60

    add-long/2addr v4, v8

    goto :goto_60

    .line 156
    :cond_7f
    cmp-long v3, v12, v16

    if-lez v3, :cond_88

    .line 157
    const-wide/16 v4, 0x0

    add-long v4, v4, v18

    goto :goto_7a

    .line 159
    :cond_88
    const-wide/16 v4, 0x0

    add-long v4, v4, v16

    goto :goto_7a
.end method

.method private a()Z
    .registers 23

    .prologue
    .line 56
    sget-object v11, Landroid/support/v7/app/bp;->d:Landroid/support/v7/app/br;

    .line 1124
    if-eqz v11, :cond_14

    iget-wide v2, v11, Landroid/support/v7/app/br;->f:J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-lez v2, :cond_14

    const/4 v2, 0x1

    .line 58
    :goto_f
    if-eqz v2, :cond_16

    .line 60
    iget-boolean v2, v11, Landroid/support/v7/app/br;->a:Z

    .line 78
    :goto_13
    return v2

    .line 1124
    :cond_14
    const/4 v2, 0x0

    goto :goto_f

    .line 2082
    :cond_16
    const/4 v2, 0x0

    .line 2083
    const/4 v3, 0x0

    .line 2085
    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/v7/app/bp;->e:Landroid/content/Context;

    const-string v5, "android.permission.ACCESS_FINE_LOCATION"

    invoke-static {v4, v5}, Landroid/support/v4/c/ar;->a(Landroid/content/Context;Ljava/lang/String;)I

    move-result v4

    .line 2087
    if-nez v4, :cond_2c

    .line 2088
    const-string v2, "network"

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Landroid/support/v7/app/bp;->a(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v2

    .line 2091
    :cond_2c
    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/v7/app/bp;->e:Landroid/content/Context;

    const-string v5, "android.permission.ACCESS_COARSE_LOCATION"

    invoke-static {v4, v5}, Landroid/support/v4/c/ar;->a(Landroid/content/Context;Ljava/lang/String;)I

    move-result v4

    .line 2093
    if-nez v4, :cond_40

    .line 2094
    const-string v3, "gps"

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Landroid/support/v7/app/bp;->a(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v3

    .line 2097
    :cond_40
    if-eqz v2, :cond_cd

    if-eqz v3, :cond_cd

    .line 2099
    invoke-virtual {v3}, Landroid/location/Location;->getTime()J

    move-result-wide v4

    invoke-virtual {v2}, Landroid/location/Location;->getTime()J

    move-result-wide v6

    cmp-long v4, v4, v6

    if-lez v4, :cond_cb

    move-object v10, v3

    .line 65
    :goto_51
    if-eqz v10, :cond_f0

    .line 2128
    sget-object v12, Landroid/support/v7/app/bp;->d:Landroid/support/v7/app/br;

    .line 2129
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v14

    .line 3031
    sget-object v2, Landroid/support/v7/app/bo;->a:Landroid/support/v7/app/bo;

    if-nez v2, :cond_64

    .line 3032
    new-instance v2, Landroid/support/v7/app/bo;

    invoke-direct {v2}, Landroid/support/v7/app/bo;-><init>()V

    sput-object v2, Landroid/support/v7/app/bo;->a:Landroid/support/v7/app/bo;

    .line 3034
    :cond_64
    sget-object v3, Landroid/support/v7/app/bo;->a:Landroid/support/v7/app/bo;

    .line 2133
    const-wide/32 v4, 0x5265c00

    sub-long v4, v14, v4

    invoke-virtual {v10}, Landroid/location/Location;->getLatitude()D

    move-result-wide v6

    invoke-virtual {v10}, Landroid/location/Location;->getLongitude()D

    move-result-wide v8

    invoke-virtual/range {v3 .. v9}, Landroid/support/v7/app/bo;->a(JDD)V

    .line 2135
    iget-wide v0, v3, Landroid/support/v7/app/bo;->d:J

    move-wide/from16 v16, v0

    .line 2138
    invoke-virtual {v10}, Landroid/location/Location;->getLatitude()D

    move-result-wide v6

    invoke-virtual {v10}, Landroid/location/Location;->getLongitude()D

    move-result-wide v8

    move-wide v4, v14

    invoke-virtual/range {v3 .. v9}, Landroid/support/v7/app/bo;->a(JDD)V

    .line 2139
    iget v2, v3, Landroid/support/v7/app/bo;->f:I

    const/4 v4, 0x1

    if-ne v2, v4, :cond_d4

    const/4 v2, 0x1

    .line 2140
    :goto_8c
    iget-wide v0, v3, Landroid/support/v7/app/bo;->e:J

    move-wide/from16 v18, v0

    .line 2141
    iget-wide v0, v3, Landroid/support/v7/app/bo;->d:J

    move-wide/from16 v20, v0

    .line 2144
    const-wide/32 v4, 0x5265c00

    add-long/2addr v4, v14

    invoke-virtual {v10}, Landroid/location/Location;->getLatitude()D

    move-result-wide v6

    invoke-virtual {v10}, Landroid/location/Location;->getLongitude()D

    move-result-wide v8

    invoke-virtual/range {v3 .. v9}, Landroid/support/v7/app/bo;->a(JDD)V

    .line 2146
    iget-wide v6, v3, Landroid/support/v7/app/bo;->e:J

    .line 2150
    const-wide/16 v4, -0x1

    cmp-long v3, v18, v4

    if-eqz v3, :cond_b1

    const-wide/16 v4, -0x1

    cmp-long v3, v20, v4

    if-nez v3, :cond_d6

    .line 2152
    :cond_b1
    const-wide/32 v4, 0x2932e00

    add-long/2addr v4, v14

    .line 2166
    :goto_b5
    iput-boolean v2, v12, Landroid/support/v7/app/br;->a:Z

    .line 2167
    move-wide/from16 v0, v16

    iput-wide v0, v12, Landroid/support/v7/app/br;->b:J

    .line 2168
    move-wide/from16 v0, v18

    iput-wide v0, v12, Landroid/support/v7/app/br;->c:J

    .line 2169
    move-wide/from16 v0, v20

    iput-wide v0, v12, Landroid/support/v7/app/br;->d:J

    .line 2170
    iput-wide v6, v12, Landroid/support/v7/app/br;->e:J

    .line 2171
    iput-wide v4, v12, Landroid/support/v7/app/br;->f:J

    .line 67
    iget-boolean v2, v11, Landroid/support/v7/app/br;->a:Z

    goto/16 :goto_13

    :cond_cb
    move-object v10, v2

    .line 2102
    goto :goto_51

    .line 2106
    :cond_cd
    if-eqz v3, :cond_d1

    move-object v10, v3

    goto :goto_51

    :cond_d1
    move-object v10, v2

    goto/16 :goto_51

    .line 2139
    :cond_d4
    const/4 v2, 0x0

    goto :goto_8c

    .line 2154
    :cond_d6
    cmp-long v3, v14, v20

    if-lez v3, :cond_e2

    .line 2155
    const-wide/16 v4, 0x0

    add-long/2addr v4, v6

    .line 2162
    :goto_dd
    const-wide/32 v8, 0xea60

    add-long/2addr v4, v8

    goto :goto_b5

    .line 2156
    :cond_e2
    cmp-long v3, v14, v18

    if-lez v3, :cond_eb

    .line 2157
    const-wide/16 v4, 0x0

    add-long v4, v4, v20

    goto :goto_dd

    .line 2159
    :cond_eb
    const-wide/16 v4, 0x0

    add-long v4, v4, v18

    goto :goto_dd

    .line 70
    :cond_f0
    const-string v2, "TwilightManager"

    const-string v3, "Could not get last known location. This is probably because the app does not have any location permissions. Falling back to hardcoded sunrise/sunset values."

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 76
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    .line 77
    const/16 v3, 0xb

    invoke-virtual {v2, v3}, Ljava/util/Calendar;->get(I)I

    move-result v2

    .line 78
    const/4 v3, 0x6

    if-lt v2, v3, :cond_108

    const/16 v3, 0x16

    if-lt v2, v3, :cond_10b

    :cond_108
    const/4 v2, 0x1

    goto/16 :goto_13

    :cond_10b
    const/4 v2, 0x0

    goto/16 :goto_13
.end method

.method private static a(Landroid/support/v7/app/br;)Z
    .registers 5

    .prologue
    .line 124
    if-eqz p0, :cond_e

    iget-wide v0, p0, Landroid/support/v7/app/br;->f:J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method private b()Landroid/location/Location;
    .registers 7

    .prologue
    const/4 v1, 0x0

    .line 82
    .line 85
    iget-object v0, p0, Landroid/support/v7/app/bp;->e:Landroid/content/Context;

    const-string v2, "android.permission.ACCESS_FINE_LOCATION"

    invoke-static {v0, v2}, Landroid/support/v4/c/ar;->a(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    .line 87
    if-nez v0, :cond_37

    .line 88
    const-string v0, "network"

    invoke-direct {p0, v0}, Landroid/support/v7/app/bp;->a(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v0

    .line 91
    :goto_11
    iget-object v2, p0, Landroid/support/v7/app/bp;->e:Landroid/content/Context;

    const-string v3, "android.permission.ACCESS_COARSE_LOCATION"

    invoke-static {v2, v3}, Landroid/support/v4/c/ar;->a(Landroid/content/Context;Ljava/lang/String;)I

    move-result v2

    .line 93
    if-nez v2, :cond_21

    .line 94
    const-string v1, "gps"

    invoke-direct {p0, v1}, Landroid/support/v7/app/bp;->a(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v1

    .line 97
    :cond_21
    if-eqz v0, :cond_33

    if-eqz v1, :cond_33

    .line 99
    invoke-virtual {v1}, Landroid/location/Location;->getTime()J

    move-result-wide v2

    invoke-virtual {v0}, Landroid/location/Location;->getTime()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-lez v2, :cond_32

    move-object v0, v1

    .line 106
    :cond_32
    :goto_32
    return-object v0

    :cond_33
    if-eqz v1, :cond_32

    move-object v0, v1

    goto :goto_32

    :cond_37
    move-object v0, v1

    goto :goto_11
.end method
