.class public Landroid/support/v7/app/bf;
.super Landroid/app/Dialog;
.source "SourceFile"

# interfaces
.implements Landroid/support/v7/app/ai;


# instance fields
.field private a:Landroid/support/v7/app/aj;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .registers 3

    .prologue
    .line 38
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/support/v7/app/bf;-><init>(Landroid/content/Context;I)V

    .line 39
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .registers 7

    .prologue
    .line 42
    .line 1148
    if-nez p2, :cond_13

    .line 1150
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 1151
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    sget v2, Landroid/support/v7/a/d;->dialogTheme:I

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 1152
    iget p2, v0, Landroid/util/TypedValue;->resourceId:I

    .line 42
    :cond_13
    invoke-direct {p0, p1, p2}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 48
    invoke-direct {p0}, Landroid/support/v7/app/bf;->c()Landroid/support/v7/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/aj;->c()V

    .line 49
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;ZLandroid/content/DialogInterface$OnCancelListener;)V
    .registers 4

    .prologue
    .line 53
    invoke-direct {p0, p1, p2, p3}, Landroid/app/Dialog;-><init>(Landroid/content/Context;ZLandroid/content/DialogInterface$OnCancelListener;)V

    .line 54
    return-void
.end method

.method private static a(Landroid/content/Context;I)I
    .registers 6

    .prologue
    .line 148
    if-nez p1, :cond_13

    .line 150
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 151
    invoke-virtual {p0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    sget v2, Landroid/support/v7/a/d;->dialogTheme:I

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 152
    iget p1, v0, Landroid/util/TypedValue;->resourceId:I

    .line 154
    :cond_13
    return p1
.end method

.method private b()Landroid/support/v7/app/a;
    .registers 2

    .prologue
    .line 71
    invoke-direct {p0}, Landroid/support/v7/app/bf;->c()Landroid/support/v7/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/aj;->a()Landroid/support/v7/app/a;

    move-result-object v0

    return-object v0
.end method

.method private c()Landroid/support/v7/app/aj;
    .registers 3

    .prologue
    .line 141
    iget-object v0, p0, Landroid/support/v7/app/bf;->a:Landroid/support/v7/app/aj;

    if-nez v0, :cond_12

    .line 2126
    invoke-virtual {p0}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-static {v0, v1, p0}, Landroid/support/v7/app/aj;->a(Landroid/content/Context;Landroid/view/Window;Landroid/support/v7/app/ai;)Landroid/support/v7/app/aj;

    move-result-object v0

    .line 142
    iput-object v0, p0, Landroid/support/v7/app/bf;->a:Landroid/support/v7/app/aj;

    .line 144
    :cond_12
    iget-object v0, p0, Landroid/support/v7/app/bf;->a:Landroid/support/v7/app/aj;

    return-object v0
.end method


# virtual methods
.method public final a()Z
    .registers 3

    .prologue
    .line 127
    invoke-direct {p0}, Landroid/support/v7/app/bf;->c()Landroid/support/v7/app/aj;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/aj;->b(I)Z

    move-result v0

    return v0
.end method

.method public addContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .registers 4

    .prologue
    .line 103
    invoke-direct {p0}, Landroid/support/v7/app/bf;->c()Landroid/support/v7/app/aj;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroid/support/v7/app/aj;->b(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 104
    return-void
.end method

.method public final e()V
    .registers 1

    .prologue
    .line 159
    return-void
.end method

.method public final f()V
    .registers 1

    .prologue
    .line 163
    return-void
.end method

.method public final g()Landroid/support/v7/c/a;
    .registers 2
    .annotation build Landroid/support/a/z;
    .end annotation

    .prologue
    .line 168
    const/4 v0, 0x0

    return-object v0
.end method

.method public invalidateOptionsMenu()V
    .registers 2

    .prologue
    .line 134
    invoke-direct {p0}, Landroid/support/v7/app/bf;->c()Landroid/support/v7/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/aj;->g()V

    .line 135
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 3

    .prologue
    .line 58
    invoke-direct {p0}, Landroid/support/v7/app/bf;->c()Landroid/support/v7/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/aj;->j()V

    .line 59
    invoke-super {p0, p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    .line 60
    invoke-direct {p0}, Landroid/support/v7/app/bf;->c()Landroid/support/v7/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/aj;->c()V

    .line 61
    return-void
.end method

.method protected onStop()V
    .registers 2

    .prologue
    .line 108
    invoke-super {p0}, Landroid/app/Dialog;->onStop()V

    .line 109
    invoke-direct {p0}, Landroid/support/v7/app/bf;->c()Landroid/support/v7/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/aj;->e()V

    .line 110
    return-void
.end method

.method public setContentView(I)V
    .registers 3
    .param p1    # I
        .annotation build Landroid/support/a/v;
        .end annotation
    .end param

    .prologue
    .line 76
    invoke-direct {p0}, Landroid/support/v7/app/bf;->c()Landroid/support/v7/app/aj;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v7/app/aj;->a(I)V

    .line 77
    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .registers 3

    .prologue
    .line 81
    invoke-direct {p0}, Landroid/support/v7/app/bf;->c()Landroid/support/v7/app/aj;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v7/app/aj;->a(Landroid/view/View;)V

    .line 82
    return-void
.end method

.method public setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .registers 4

    .prologue
    .line 86
    invoke-direct {p0}, Landroid/support/v7/app/bf;->c()Landroid/support/v7/app/aj;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroid/support/v7/app/aj;->a(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 87
    return-void
.end method

.method public setTitle(I)V
    .registers 4

    .prologue
    .line 97
    invoke-super {p0, p1}, Landroid/app/Dialog;->setTitle(I)V

    .line 98
    invoke-direct {p0}, Landroid/support/v7/app/bf;->c()Landroid/support/v7/app/aj;

    move-result-object v0

    invoke-virtual {p0}, Landroid/support/v7/app/bf;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/aj;->a(Ljava/lang/CharSequence;)V

    .line 99
    return-void
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .registers 3

    .prologue
    .line 91
    invoke-super {p0, p1}, Landroid/app/Dialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 92
    invoke-direct {p0}, Landroid/support/v7/app/bf;->c()Landroid/support/v7/app/aj;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v7/app/aj;->a(Ljava/lang/CharSequence;)V

    .line 93
    return-void
.end method
