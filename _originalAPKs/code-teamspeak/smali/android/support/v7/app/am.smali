.class final Landroid/support/v7/app/am;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/support/v7/app/l;


# instance fields
.field final synthetic a:Landroid/support/v7/app/ak;


# direct methods
.method private constructor <init>(Landroid/support/v7/app/ak;)V
    .registers 2

    .prologue
    .line 135
    iput-object p1, p0, Landroid/support/v7/app/am;->a:Landroid/support/v7/app/ak;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Landroid/support/v7/app/ak;B)V
    .registers 3

    .prologue
    .line 135
    invoke-direct {p0, p1}, Landroid/support/v7/app/am;-><init>(Landroid/support/v7/app/ak;)V

    return-void
.end method


# virtual methods
.method public final a()Landroid/graphics/drawable/Drawable;
    .registers 6

    .prologue
    const/4 v4, 0x0

    .line 138
    .line 1147
    iget-object v0, p0, Landroid/support/v7/app/am;->a:Landroid/support/v7/app/ak;

    invoke-virtual {v0}, Landroid/support/v7/app/ak;->m()Landroid/content/Context;

    move-result-object v0

    .line 138
    const/4 v1, 0x0

    const/4 v2, 0x1

    new-array v2, v2, [I

    sget v3, Landroid/support/v7/a/d;->homeAsUpIndicator:I

    aput v3, v2, v4

    invoke-static {v0, v1, v2}, Landroid/support/v7/internal/widget/ax;->a(Landroid/content/Context;Landroid/util/AttributeSet;[I)Landroid/support/v7/internal/widget/ax;

    move-result-object v0

    .line 140
    invoke-virtual {v0, v4}, Landroid/support/v7/internal/widget/ax;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 1183
    iget-object v0, v0, Landroid/support/v7/internal/widget/ax;->a:Landroid/content/res/TypedArray;

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 142
    return-object v1
.end method

.method public final a(I)V
    .registers 3

    .prologue
    .line 167
    iget-object v0, p0, Landroid/support/v7/app/am;->a:Landroid/support/v7/app/ak;

    invoke-virtual {v0}, Landroid/support/v7/app/ak;->a()Landroid/support/v7/app/a;

    move-result-object v0

    .line 168
    if-eqz v0, :cond_b

    .line 169
    invoke-virtual {v0, p1}, Landroid/support/v7/app/a;->l(I)V

    .line 171
    :cond_b
    return-void
.end method

.method public final a(Landroid/graphics/drawable/Drawable;I)V
    .registers 4

    .prologue
    .line 158
    iget-object v0, p0, Landroid/support/v7/app/am;->a:Landroid/support/v7/app/ak;

    invoke-virtual {v0}, Landroid/support/v7/app/ak;->a()Landroid/support/v7/app/a;

    move-result-object v0

    .line 159
    if-eqz v0, :cond_e

    .line 160
    invoke-virtual {v0, p1}, Landroid/support/v7/app/a;->e(Landroid/graphics/drawable/Drawable;)V

    .line 161
    invoke-virtual {v0, p2}, Landroid/support/v7/app/a;->l(I)V

    .line 163
    :cond_e
    return-void
.end method

.method public final b()Landroid/content/Context;
    .registers 2

    .prologue
    .line 147
    iget-object v0, p0, Landroid/support/v7/app/am;->a:Landroid/support/v7/app/ak;

    invoke-virtual {v0}, Landroid/support/v7/app/ak;->m()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method public final c()Z
    .registers 2

    .prologue
    .line 152
    iget-object v0, p0, Landroid/support/v7/app/am;->a:Landroid/support/v7/app/ak;

    invoke-virtual {v0}, Landroid/support/v7/app/ak;->a()Landroid/support/v7/app/a;

    move-result-object v0

    .line 153
    if-eqz v0, :cond_12

    invoke-virtual {v0}, Landroid/support/v7/app/a;->h()I

    move-result v0

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_12

    const/4 v0, 0x1

    :goto_11
    return v0

    :cond_12
    const/4 v0, 0x0

    goto :goto_11
.end method
