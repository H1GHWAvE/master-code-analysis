.class public final Landroid/support/v4/h/j;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Landroid/support/v4/h/p;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 124
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_e

    .line 125
    new-instance v0, Landroid/support/v4/h/o;

    invoke-direct {v0}, Landroid/support/v4/h/o;-><init>()V

    sput-object v0, Landroid/support/v4/h/j;->a:Landroid/support/v4/h/p;

    .line 129
    :goto_d
    return-void

    .line 127
    :cond_e
    new-instance v0, Landroid/support/v4/h/l;

    invoke-direct {v0}, Landroid/support/v4/h/l;-><init>()V

    sput-object v0, Landroid/support/v4/h/j;->a:Landroid/support/v4/h/p;

    goto :goto_d
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 84
    return-void
.end method

.method private static a()V
    .registers 1

    .prologue
    .line 136
    sget-object v0, Landroid/support/v4/h/j;->a:Landroid/support/v4/h/p;

    invoke-interface {v0}, Landroid/support/v4/h/p;->a()V

    .line 137
    return-void
.end method

.method private static a(I)V
    .registers 2

    .prologue
    .line 156
    sget-object v0, Landroid/support/v4/h/j;->a:Landroid/support/v4/h/p;

    invoke-interface {v0, p0}, Landroid/support/v4/h/p;->a(I)V

    .line 157
    return-void
.end method

.method private static a(II)V
    .registers 3

    .prologue
    .line 167
    sget-object v0, Landroid/support/v4/h/j;->a:Landroid/support/v4/h/p;

    invoke-interface {v0, p0, p1}, Landroid/support/v4/h/p;->a(II)V

    .line 168
    return-void
.end method

.method private static a(Ljava/net/Socket;)V
    .registers 2

    .prologue
    .line 194
    sget-object v0, Landroid/support/v4/h/j;->a:Landroid/support/v4/h/p;

    invoke-interface {v0, p0}, Landroid/support/v4/h/p;->a(Ljava/net/Socket;)V

    .line 195
    return-void
.end method

.method private static b()I
    .registers 1

    .prologue
    .line 145
    sget-object v0, Landroid/support/v4/h/j;->a:Landroid/support/v4/h/p;

    invoke-interface {v0}, Landroid/support/v4/h/p;->b()I

    move-result v0

    return v0
.end method

.method private static b(I)V
    .registers 2

    .prologue
    .line 182
    sget-object v0, Landroid/support/v4/h/j;->a:Landroid/support/v4/h/p;

    invoke-interface {v0, p0}, Landroid/support/v4/h/p;->b(I)V

    .line 183
    return-void
.end method

.method private static b(Ljava/net/Socket;)V
    .registers 2

    .prologue
    .line 201
    sget-object v0, Landroid/support/v4/h/j;->a:Landroid/support/v4/h/p;

    invoke-interface {v0, p0}, Landroid/support/v4/h/p;->b(Ljava/net/Socket;)V

    .line 202
    return-void
.end method
