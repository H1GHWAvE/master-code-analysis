.class public final Landroid/support/v4/widget/NestedScrollView;
.super Landroid/widget/FrameLayout;
.source "SourceFile"

# interfaces
.implements Landroid/support/v4/view/bt;
.implements Landroid/support/v4/view/bv;


# static fields
.field private static final A:[I

.field static final a:I = 0xfa

.field static final b:F = 0.5f

.field private static final c:Ljava/lang/String; = "NestedScrollView"

.field private static final x:I = -0x1

.field private static final z:Landroid/support/v4/widget/bh;


# instance fields
.field private final B:Landroid/support/v4/view/bw;

.field private final C:Landroid/support/v4/view/bu;

.field private D:F

.field private d:J

.field private final e:Landroid/graphics/Rect;

.field private f:Landroid/support/v4/widget/ca;

.field private g:Landroid/support/v4/widget/al;

.field private h:Landroid/support/v4/widget/al;

.field private i:I

.field private j:Z

.field private k:Z

.field private l:Landroid/view/View;

.field private m:Z

.field private n:Landroid/view/VelocityTracker;

.field private o:Z

.field private p:Z

.field private q:I

.field private r:I

.field private s:I

.field private t:I

.field private final u:[I

.field private final v:[I

.field private w:I

.field private y:Landroid/support/v4/widget/NestedScrollView$SavedState;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    .line 145
    new-instance v0, Landroid/support/v4/widget/bh;

    invoke-direct {v0}, Landroid/support/v4/widget/bh;-><init>()V

    sput-object v0, Landroid/support/v4/widget/NestedScrollView;->z:Landroid/support/v4/widget/bh;

    .line 147
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x101017a

    aput v2, v0, v1

    sput-object v0, Landroid/support/v4/widget/NestedScrollView;->A:[I

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .registers 3

    .prologue
    .line 157
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/support/v4/widget/NestedScrollView;-><init>(Landroid/content/Context;B)V

    .line 158
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;B)V
    .registers 4

    .prologue
    .line 161
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/support/v4/widget/NestedScrollView;-><init>(Landroid/content/Context;C)V

    .line 162
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;C)V
    .registers 8

    .prologue
    const/4 v1, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 165
    invoke-direct {p0, p1, v4, v2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 73
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Landroid/support/v4/widget/NestedScrollView;->e:Landroid/graphics/Rect;

    .line 87
    iput-boolean v3, p0, Landroid/support/v4/widget/NestedScrollView;->j:Z

    .line 88
    iput-boolean v2, p0, Landroid/support/v4/widget/NestedScrollView;->k:Z

    .line 95
    iput-object v4, p0, Landroid/support/v4/widget/NestedScrollView;->l:Landroid/view/View;

    .line 102
    iput-boolean v2, p0, Landroid/support/v4/widget/NestedScrollView;->m:Z

    .line 118
    iput-boolean v3, p0, Landroid/support/v4/widget/NestedScrollView;->p:Z

    .line 128
    const/4 v0, -0x1

    iput v0, p0, Landroid/support/v4/widget/NestedScrollView;->t:I

    .line 133
    new-array v0, v1, [I

    iput-object v0, p0, Landroid/support/v4/widget/NestedScrollView;->u:[I

    .line 134
    new-array v0, v1, [I

    iput-object v0, p0, Landroid/support/v4/widget/NestedScrollView;->v:[I

    .line 2332
    new-instance v0, Landroid/support/v4/widget/ca;

    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, v4}, Landroid/support/v4/widget/ca;-><init>(Landroid/content/Context;Landroid/view/animation/Interpolator;)V

    iput-object v0, p0, Landroid/support/v4/widget/NestedScrollView;->f:Landroid/support/v4/widget/ca;

    .line 2333
    invoke-virtual {p0, v3}, Landroid/support/v4/widget/NestedScrollView;->setFocusable(Z)V

    .line 2334
    const/high16 v0, 0x40000

    invoke-virtual {p0, v0}, Landroid/support/v4/widget/NestedScrollView;->setDescendantFocusability(I)V

    .line 2335
    invoke-virtual {p0, v2}, Landroid/support/v4/widget/NestedScrollView;->setWillNotDraw(Z)V

    .line 2336
    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    .line 2337
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v1

    iput v1, p0, Landroid/support/v4/widget/NestedScrollView;->q:I

    .line 2338
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMinimumFlingVelocity()I

    move-result v1

    iput v1, p0, Landroid/support/v4/widget/NestedScrollView;->r:I

    .line 2339
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    move-result v0

    iput v0, p0, Landroid/support/v4/widget/NestedScrollView;->s:I

    .line 168
    sget-object v0, Landroid/support/v4/widget/NestedScrollView;->A:[I

    invoke-virtual {p1, v4, v0, v2, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 171
    invoke-virtual {v0, v2, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    invoke-virtual {p0, v1}, Landroid/support/v4/widget/NestedScrollView;->setFillViewport(Z)V

    .line 173
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 175
    new-instance v0, Landroid/support/v4/view/bw;

    invoke-direct {v0, p0}, Landroid/support/v4/view/bw;-><init>(Landroid/view/ViewGroup;)V

    iput-object v0, p0, Landroid/support/v4/widget/NestedScrollView;->B:Landroid/support/v4/view/bw;

    .line 176
    new-instance v0, Landroid/support/v4/view/bu;

    invoke-direct {v0, p0}, Landroid/support/v4/view/bu;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Landroid/support/v4/widget/NestedScrollView;->C:Landroid/support/v4/view/bu;

    .line 179
    invoke-virtual {p0, v3}, Landroid/support/v4/widget/NestedScrollView;->setNestedScrollingEnabled(Z)V

    .line 181
    sget-object v0, Landroid/support/v4/widget/NestedScrollView;->z:Landroid/support/v4/widget/bh;

    invoke-static {p0, v0}, Landroid/support/v4/view/cx;->a(Landroid/view/View;Landroid/support/v4/view/a;)V

    .line 182
    return-void
.end method

.method private a(Landroid/graphics/Rect;)I
    .registers 9

    .prologue
    const/4 v2, 0x0

    .line 1415
    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getChildCount()I

    move-result v0

    if-nez v0, :cond_8

    .line 1469
    :goto_7
    return v2

    .line 1417
    :cond_8
    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getHeight()I

    move-result v3

    .line 1418
    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getScrollY()I

    move-result v0

    .line 1419
    add-int v1, v0, v3

    .line 1421
    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getVerticalFadingEdgeLength()I

    move-result v4

    .line 1424
    iget v5, p1, Landroid/graphics/Rect;->top:I

    if-lez v5, :cond_1b

    .line 1425
    add-int/2addr v0, v4

    .line 1429
    :cond_1b
    iget v5, p1, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {p0, v2}, Landroid/support/v4/widget/NestedScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6}, Landroid/view/View;->getHeight()I

    move-result v6

    if-ge v5, v6, :cond_28

    .line 1430
    sub-int/2addr v1, v4

    .line 1435
    :cond_28
    iget v4, p1, Landroid/graphics/Rect;->bottom:I

    if-le v4, v1, :cond_52

    iget v4, p1, Landroid/graphics/Rect;->top:I

    if-le v4, v0, :cond_52

    .line 1440
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v4

    if-le v4, v3, :cond_4c

    .line 1442
    iget v3, p1, Landroid/graphics/Rect;->top:I

    sub-int v0, v3, v0

    add-int/lit8 v0, v0, 0x0

    .line 1449
    :goto_3c
    invoke-virtual {p0, v2}, Landroid/support/v4/widget/NestedScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getBottom()I

    move-result v2

    .line 1450
    sub-int v1, v2, v1

    .line 1451
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    :goto_4a
    move v2, v0

    .line 1469
    goto :goto_7

    .line 1445
    :cond_4c
    iget v0, p1, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    goto :goto_3c

    .line 1453
    :cond_52
    iget v4, p1, Landroid/graphics/Rect;->top:I

    if-ge v4, v0, :cond_76

    iget v4, p1, Landroid/graphics/Rect;->bottom:I

    if-ge v4, v1, :cond_76

    .line 1458
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v2

    if-le v2, v3, :cond_70

    .line 1460
    iget v0, p1, Landroid/graphics/Rect;->bottom:I

    sub-int v0, v1, v0

    rsub-int/lit8 v0, v0, 0x0

    .line 1467
    :goto_66
    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getScrollY()I

    move-result v1

    neg-int v1, v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_4a

    .line 1463
    :cond_70
    iget v1, p1, Landroid/graphics/Rect;->top:I

    sub-int/2addr v0, v1

    rsub-int/lit8 v0, v0, 0x0

    goto :goto_66

    :cond_76
    move v0, v2

    goto :goto_4a
.end method

.method static synthetic a(Landroid/support/v4/widget/NestedScrollView;)I
    .registers 2

    .prologue
    .line 63
    invoke-direct {p0}, Landroid/support/v4/widget/NestedScrollView;->getScrollRange()I

    move-result v0

    return v0
.end method

.method private a(ZII)Landroid/view/View;
    .registers 15

    .prologue
    .line 977
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Landroid/support/v4/widget/NestedScrollView;->getFocusables(I)Ljava/util/ArrayList;

    move-result-object v6

    .line 978
    const/4 v3, 0x0

    .line 987
    const/4 v2, 0x0

    .line 989
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v7

    .line 990
    const/4 v0, 0x0

    move v5, v0

    :goto_d
    if-ge v5, v7, :cond_5b

    .line 991
    invoke-interface {v6, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 992
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v4

    .line 993
    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v8

    .line 995
    if-ge p2, v8, :cond_5c

    if-ge v4, p3, :cond_5c

    .line 1001
    if-ge p2, v4, :cond_31

    if-ge v8, p3, :cond_31

    const/4 v1, 0x1

    .line 1004
    :goto_26
    if-nez v3, :cond_33

    move v10, v1

    move-object v1, v0

    move v0, v10

    .line 990
    :goto_2b
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    move-object v3, v1

    move v2, v0

    goto :goto_d

    .line 1001
    :cond_31
    const/4 v1, 0x0

    goto :goto_26

    .line 1009
    :cond_33
    if-eqz p1, :cond_3b

    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    move-result v9

    if-lt v4, v9, :cond_43

    :cond_3b
    if-nez p1, :cond_4d

    invoke-virtual {v3}, Landroid/view/View;->getBottom()I

    move-result v4

    if-le v8, v4, :cond_4d

    :cond_43
    const/4 v4, 0x1

    .line 1014
    :goto_44
    if-eqz v2, :cond_4f

    .line 1015
    if-eqz v1, :cond_5c

    if-eqz v4, :cond_5c

    move-object v1, v0

    move v0, v2

    .line 1021
    goto :goto_2b

    .line 1009
    :cond_4d
    const/4 v4, 0x0

    goto :goto_44

    .line 1024
    :cond_4f
    if-eqz v1, :cond_56

    .line 1027
    const/4 v1, 0x1

    move v10, v1

    move-object v1, v0

    move v0, v10

    goto :goto_2b

    .line 1028
    :cond_56
    if-eqz v4, :cond_5c

    move-object v1, v0

    move v0, v2

    .line 1033
    goto :goto_2b

    .line 1040
    :cond_5b
    return-object v3

    :cond_5c
    move v0, v2

    move-object v1, v3

    goto :goto_2b
.end method

.method private a()V
    .registers 4

    .prologue
    .line 332
    new-instance v0, Landroid/support/v4/widget/ca;

    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Landroid/support/v4/widget/ca;-><init>(Landroid/content/Context;Landroid/view/animation/Interpolator;)V

    iput-object v0, p0, Landroid/support/v4/widget/NestedScrollView;->f:Landroid/support/v4/widget/ca;

    .line 333
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/support/v4/widget/NestedScrollView;->setFocusable(Z)V

    .line 334
    const/high16 v0, 0x40000

    invoke-virtual {p0, v0}, Landroid/support/v4/widget/NestedScrollView;->setDescendantFocusability(I)V

    .line 335
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/support/v4/widget/NestedScrollView;->setWillNotDraw(Z)V

    .line 336
    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    .line 337
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v1

    iput v1, p0, Landroid/support/v4/widget/NestedScrollView;->q:I

    .line 338
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMinimumFlingVelocity()I

    move-result v1

    iput v1, p0, Landroid/support/v4/widget/NestedScrollView;->r:I

    .line 339
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    move-result v0

    iput v0, p0, Landroid/support/v4/widget/NestedScrollView;->s:I

    .line 340
    return-void
.end method

.method private a(Landroid/view/MotionEvent;)V
    .registers 5

    .prologue
    .line 831
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const v1, 0xff00

    and-int/2addr v0, v1

    shr-int/lit8 v0, v0, 0x8

    .line 833
    invoke-static {p1, v0}, Landroid/support/v4/view/bk;->b(Landroid/view/MotionEvent;I)I

    move-result v1

    .line 834
    iget v2, p0, Landroid/support/v4/widget/NestedScrollView;->t:I

    if-ne v1, v2, :cond_2b

    .line 838
    if-nez v0, :cond_2c

    const/4 v0, 0x1

    .line 839
    :goto_15
    invoke-static {p1, v0}, Landroid/support/v4/view/bk;->d(Landroid/view/MotionEvent;I)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Landroid/support/v4/widget/NestedScrollView;->i:I

    .line 840
    invoke-static {p1, v0}, Landroid/support/v4/view/bk;->b(Landroid/view/MotionEvent;I)I

    move-result v0

    iput v0, p0, Landroid/support/v4/widget/NestedScrollView;->t:I

    .line 841
    iget-object v0, p0, Landroid/support/v4/widget/NestedScrollView;->n:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_2b

    .line 842
    iget-object v0, p0, Landroid/support/v4/widget/NestedScrollView;->n:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->clear()V

    .line 845
    :cond_2b
    return-void

    .line 838
    :cond_2c
    const/4 v0, 0x0

    goto :goto_15
.end method

.method private a(II)Z
    .registers 7

    .prologue
    const/4 v0, 0x0

    .line 520
    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getChildCount()I

    move-result v1

    if-lez v1, :cond_2b

    .line 521
    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getScrollY()I

    move-result v1

    .line 522
    invoke-virtual {p0, v0}, Landroid/support/v4/widget/NestedScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 523
    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v3

    sub-int/2addr v3, v1

    if-lt p2, v3, :cond_2b

    invoke-virtual {v2}, Landroid/view/View;->getBottom()I

    move-result v3

    sub-int v1, v3, v1

    if-ge p2, v1, :cond_2b

    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v1

    if-lt p1, v1, :cond_2b

    invoke-virtual {v2}, Landroid/view/View;->getRight()I

    move-result v1

    if-ge p1, v1, :cond_2b

    const/4 v0, 0x1

    .line 528
    :cond_2b
    return v0
.end method

.method private a(III)Z
    .registers 20

    .prologue
    .line 1123
    const/4 v6, 0x1

    .line 1125
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/widget/NestedScrollView;->getHeight()I

    move-result v1

    .line 1126
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/widget/NestedScrollView;->getScrollY()I

    move-result v9

    .line 1127
    add-int v10, v9, v1

    .line 1128
    const/16 v1, 0x21

    move/from16 v0, p1

    if-ne v0, v1, :cond_4e

    const/4 v1, 0x1

    move v2, v1

    .line 7977
    :goto_13
    const/4 v1, 0x2

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/NestedScrollView;->getFocusables(I)Ljava/util/ArrayList;

    move-result-object v11

    .line 7978
    const/4 v5, 0x0

    .line 7987
    const/4 v4, 0x0

    .line 7989
    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v12

    .line 7990
    const/4 v1, 0x0

    move v8, v1

    :goto_22
    if-ge v8, v12, :cond_7b

    .line 7991
    invoke-interface {v11, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 7992
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v7

    .line 7993
    invoke-virtual {v1}, Landroid/view/View;->getBottom()I

    move-result v13

    .line 7995
    move/from16 v0, p2

    if-ge v0, v13, :cond_a2

    move/from16 v0, p3

    if-ge v7, v0, :cond_a2

    .line 8001
    move/from16 v0, p2

    if-ge v0, v7, :cond_51

    move/from16 v0, p3

    if-ge v13, v0, :cond_51

    const/4 v3, 0x1

    .line 8004
    :goto_43
    if-nez v5, :cond_53

    move v15, v3

    move-object v3, v1

    move v1, v15

    .line 7990
    :goto_48
    add-int/lit8 v4, v8, 0x1

    move v8, v4

    move-object v5, v3

    move v4, v1

    goto :goto_22

    .line 1128
    :cond_4e
    const/4 v1, 0x0

    move v2, v1

    goto :goto_13

    .line 8001
    :cond_51
    const/4 v3, 0x0

    goto :goto_43

    .line 8009
    :cond_53
    if-eqz v2, :cond_5b

    invoke-virtual {v5}, Landroid/view/View;->getTop()I

    move-result v14

    if-lt v7, v14, :cond_63

    :cond_5b
    if-nez v2, :cond_6d

    invoke-virtual {v5}, Landroid/view/View;->getBottom()I

    move-result v7

    if-le v13, v7, :cond_6d

    :cond_63
    const/4 v7, 0x1

    .line 8014
    :goto_64
    if-eqz v4, :cond_6f

    .line 8015
    if-eqz v3, :cond_a2

    if-eqz v7, :cond_a2

    move-object v3, v1

    move v1, v4

    .line 8021
    goto :goto_48

    .line 8009
    :cond_6d
    const/4 v7, 0x0

    goto :goto_64

    .line 8024
    :cond_6f
    if-eqz v3, :cond_76

    .line 8027
    const/4 v3, 0x1

    move v15, v3

    move-object v3, v1

    move v1, v15

    goto :goto_48

    .line 8028
    :cond_76
    if-eqz v7, :cond_a2

    move-object v3, v1

    move v1, v4

    .line 8033
    goto :goto_48

    .line 1131
    :cond_7b
    if-nez v5, :cond_7f

    move-object/from16 v5, p0

    .line 1135
    :cond_7f
    move/from16 v0, p2

    if-lt v0, v9, :cond_94

    move/from16 v0, p3

    if-gt v0, v10, :cond_94

    .line 1136
    const/4 v1, 0x0

    .line 1142
    :goto_88
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/widget/NestedScrollView;->findFocus()Landroid/view/View;

    move-result-object v2

    if-eq v5, v2, :cond_93

    move/from16 v0, p1

    invoke-virtual {v5, v0}, Landroid/view/View;->requestFocus(I)Z

    .line 1144
    :cond_93
    return v1

    .line 1138
    :cond_94
    if-eqz v2, :cond_9f

    sub-int v1, p2, v9

    .line 1139
    :goto_98
    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Landroid/support/v4/widget/NestedScrollView;->e(I)V

    move v1, v6

    goto :goto_88

    .line 1138
    :cond_9f
    sub-int v1, p3, v10

    goto :goto_98

    :cond_a2
    move v1, v4

    move-object v3, v5

    goto :goto_48
.end method

.method private a(IIIII)Z
    .registers 12

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 901
    invoke-static {p0}, Landroid/support/v4/view/cx;->a(Landroid/view/View;)I

    .line 902
    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->computeHorizontalScrollRange()I

    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->computeHorizontalScrollExtent()I

    .line 904
    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->computeVerticalScrollRange()I

    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->computeVerticalScrollExtent()I

    .line 911
    add-int v4, p3, p1

    .line 916
    add-int v3, p4, p2

    .line 925
    add-int/lit8 v0, p5, 0x0

    .line 928
    if-lez v4, :cond_28

    move v4, v1

    move v5, v2

    .line 937
    :goto_1b
    if-le v3, v0, :cond_2d

    move v3, v0

    move v0, v1

    .line 945
    :goto_1f
    invoke-virtual {p0, v5, v3, v4, v0}, Landroid/support/v4/widget/NestedScrollView;->onOverScrolled(IIZZ)V

    .line 947
    if-nez v4, :cond_26

    if-eqz v0, :cond_27

    :cond_26
    move v2, v1

    :cond_27
    return v2

    .line 931
    :cond_28
    if-gez v4, :cond_34

    move v4, v1

    move v5, v2

    .line 933
    goto :goto_1b

    .line 940
    :cond_2d
    if-gez v3, :cond_32

    move v0, v1

    move v3, v2

    .line 942
    goto :goto_1f

    :cond_32
    move v0, v2

    goto :goto_1f

    :cond_34
    move v5, v4

    move v4, v2

    goto :goto_1b
.end method

.method private a(Landroid/graphics/Rect;Z)Z
    .registers 6

    .prologue
    const/4 v1, 0x0

    .line 1394
    invoke-direct {p0, p1}, Landroid/support/v4/widget/NestedScrollView;->a(Landroid/graphics/Rect;)I

    move-result v2

    .line 1395
    if-eqz v2, :cond_10

    const/4 v0, 0x1

    .line 1396
    :goto_8
    if-eqz v0, :cond_f

    .line 1397
    if-eqz p2, :cond_12

    .line 1398
    invoke-virtual {p0, v1, v2}, Landroid/support/v4/widget/NestedScrollView;->scrollBy(II)V

    .line 1403
    :cond_f
    :goto_f
    return v0

    :cond_10
    move v0, v1

    .line 1395
    goto :goto_8

    .line 1400
    :cond_12
    invoke-direct {p0, v1, v2}, Landroid/support/v4/widget/NestedScrollView;->b(II)V

    goto :goto_f
.end method

.method private a(Landroid/view/KeyEvent;)Z
    .registers 9

    .prologue
    const/16 v0, 0x21

    const/4 v1, 0x1

    const/16 v4, 0x82

    const/4 v2, 0x0

    .line 478
    iget-object v3, p0, Landroid/support/v4/widget/NestedScrollView;->e:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->setEmpty()V

    .line 6382
    invoke-virtual {p0, v2}, Landroid/support/v4/widget/NestedScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 6383
    if-eqz v3, :cond_52

    .line 6384
    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v3

    .line 6385
    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getHeight()I

    move-result v5

    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getPaddingTop()I

    move-result v6

    add-int/2addr v3, v6

    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getPaddingBottom()I

    move-result v6

    add-int/2addr v3, v6

    if-ge v5, v3, :cond_50

    move v3, v1

    .line 480
    :goto_26
    if-nez v3, :cond_54

    .line 481
    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_4f

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/4 v3, 0x4

    if-eq v0, v3, :cond_4f

    .line 482
    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->findFocus()Landroid/view/View;

    move-result-object v0

    .line 483
    if-ne v0, p0, :cond_3c

    const/4 v0, 0x0

    .line 484
    :cond_3c
    invoke-static {}, Landroid/view/FocusFinder;->getInstance()Landroid/view/FocusFinder;

    move-result-object v3

    invoke-virtual {v3, p0, v0, v4}, Landroid/view/FocusFinder;->findNextFocus(Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    .line 486
    if-eqz v0, :cond_4f

    if-eq v0, p0, :cond_4f

    invoke-virtual {v0, v4}, Landroid/view/View;->requestFocus(I)Z

    move-result v0

    if-eqz v0, :cond_4f

    move v2, v1

    .line 516
    :cond_4f
    :goto_4f
    return v2

    :cond_50
    move v3, v2

    .line 6385
    goto :goto_26

    :cond_52
    move v3, v2

    .line 6387
    goto :goto_26

    .line 494
    :cond_54
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v3

    if-nez v3, :cond_4f

    .line 495
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v3

    sparse-switch v3, :sswitch_data_e8

    goto :goto_4f

    .line 497
    :sswitch_62
    invoke-virtual {p1}, Landroid/view/KeyEvent;->isAltPressed()Z

    move-result v1

    if-nez v1, :cond_6d

    .line 498
    invoke-direct {p0, v0}, Landroid/support/v4/widget/NestedScrollView;->d(I)Z

    move-result v2

    goto :goto_4f

    .line 500
    :cond_6d
    invoke-direct {p0, v0}, Landroid/support/v4/widget/NestedScrollView;->c(I)Z

    move-result v2

    goto :goto_4f

    .line 504
    :sswitch_72
    invoke-virtual {p1}, Landroid/view/KeyEvent;->isAltPressed()Z

    move-result v0

    if-nez v0, :cond_7d

    .line 505
    invoke-direct {p0, v4}, Landroid/support/v4/widget/NestedScrollView;->d(I)Z

    move-result v2

    goto :goto_4f

    .line 507
    :cond_7d
    invoke-direct {p0, v4}, Landroid/support/v4/widget/NestedScrollView;->c(I)Z

    move-result v2

    goto :goto_4f

    .line 511
    :sswitch_82
    invoke-virtual {p1}, Landroid/view/KeyEvent;->isShiftPressed()Z

    move-result v3

    if-eqz v3, :cond_d0

    move v3, v0

    .line 7056
    :goto_89
    if-ne v3, v4, :cond_d2

    move v0, v1

    .line 7057
    :goto_8c
    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getHeight()I

    move-result v1

    .line 7059
    if-eqz v0, :cond_d4

    .line 7060
    iget-object v0, p0, Landroid/support/v4/widget/NestedScrollView;->e:Landroid/graphics/Rect;

    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getScrollY()I

    move-result v4

    add-int/2addr v4, v1

    iput v4, v0, Landroid/graphics/Rect;->top:I

    .line 7061
    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getChildCount()I

    move-result v0

    .line 7062
    if-lez v0, :cond_bb

    .line 7063
    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Landroid/support/v4/widget/NestedScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 7064
    iget-object v4, p0, Landroid/support/v4/widget/NestedScrollView;->e:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    add-int/2addr v4, v1

    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v5

    if-le v4, v5, :cond_bb

    .line 7065
    iget-object v4, p0, Landroid/support/v4/widget/NestedScrollView;->e:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v0

    sub-int/2addr v0, v1

    iput v0, v4, Landroid/graphics/Rect;->top:I

    .line 7074
    :cond_bb
    :goto_bb
    iget-object v0, p0, Landroid/support/v4/widget/NestedScrollView;->e:Landroid/graphics/Rect;

    iget-object v4, p0, Landroid/support/v4/widget/NestedScrollView;->e:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    add-int/2addr v1, v4

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    .line 7076
    iget-object v0, p0, Landroid/support/v4/widget/NestedScrollView;->e:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    iget-object v1, p0, Landroid/support/v4/widget/NestedScrollView;->e:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    invoke-direct {p0, v3, v0, v1}, Landroid/support/v4/widget/NestedScrollView;->a(III)Z

    goto :goto_4f

    :cond_d0
    move v3, v4

    .line 511
    goto :goto_89

    :cond_d2
    move v0, v2

    .line 7056
    goto :goto_8c

    .line 7069
    :cond_d4
    iget-object v0, p0, Landroid/support/v4/widget/NestedScrollView;->e:Landroid/graphics/Rect;

    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getScrollY()I

    move-result v4

    sub-int/2addr v4, v1

    iput v4, v0, Landroid/graphics/Rect;->top:I

    .line 7070
    iget-object v0, p0, Landroid/support/v4/widget/NestedScrollView;->e:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    if-gez v0, :cond_bb

    .line 7071
    iget-object v0, p0, Landroid/support/v4/widget/NestedScrollView;->e:Landroid/graphics/Rect;

    iput v2, v0, Landroid/graphics/Rect;->top:I

    goto :goto_bb

    .line 495
    :sswitch_data_e8
    .sparse-switch
        0x13 -> :sswitch_62
        0x14 -> :sswitch_72
        0x3e -> :sswitch_82
    .end sparse-switch
.end method

.method private a(Landroid/view/View;)Z
    .registers 4

    .prologue
    const/4 v0, 0x0

    .line 1210
    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getHeight()I

    move-result v1

    invoke-direct {p0, p1, v0, v1}, Landroid/support/v4/widget/NestedScrollView;->a(Landroid/view/View;II)Z

    move-result v1

    if-nez v1, :cond_c

    const/4 v0, 0x1

    :cond_c
    return v0
.end method

.method private a(Landroid/view/View;II)Z
    .registers 6

    .prologue
    .line 1218
    iget-object v0, p0, Landroid/support/v4/widget/NestedScrollView;->e:Landroid/graphics/Rect;

    invoke-virtual {p1, v0}, Landroid/view/View;->getDrawingRect(Landroid/graphics/Rect;)V

    .line 1219
    iget-object v0, p0, Landroid/support/v4/widget/NestedScrollView;->e:Landroid/graphics/Rect;

    invoke-virtual {p0, p1, v0}, Landroid/support/v4/widget/NestedScrollView;->offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 1221
    iget-object v0, p0, Landroid/support/v4/widget/NestedScrollView;->e:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v0, p2

    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getScrollY()I

    move-result v1

    if-lt v0, v1, :cond_23

    iget-object v0, p0, Landroid/support/v4/widget/NestedScrollView;->e:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    sub-int/2addr v0, p2

    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getScrollY()I

    move-result v1

    add-int/2addr v1, p3

    if-gt v0, v1, :cond_23

    const/4 v0, 0x1

    :goto_22
    return v0

    :cond_23
    const/4 v0, 0x0

    goto :goto_22
.end method

.method private static a(Landroid/view/View;Landroid/view/View;)Z
    .registers 5

    .prologue
    const/4 v1, 0x1

    .line 1596
    if-ne p0, p1, :cond_5

    move v0, v1

    .line 1601
    :goto_4
    return v0

    .line 1600
    :cond_5
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 1601
    instance-of v2, v0, Landroid/view/ViewGroup;

    if-eqz v2, :cond_17

    check-cast v0, Landroid/view/View;

    invoke-static {v0, p1}, Landroid/support/v4/widget/NestedScrollView;->a(Landroid/view/View;Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_17

    move v0, v1

    goto :goto_4

    :cond_17
    const/4 v0, 0x0

    goto :goto_4
.end method

.method private static b(III)I
    .registers 4

    .prologue
    .line 1712
    if-ge p1, p2, :cond_4

    if-gez p0, :cond_6

    .line 1728
    :cond_4
    const/4 p0, 0x0

    .line 1738
    :cond_5
    :goto_5
    return p0

    .line 1730
    :cond_6
    add-int v0, p1, p0

    if-le v0, p2, :cond_5

    .line 1736
    sub-int p0, p2, p1

    goto :goto_5
.end method

.method private b(II)V
    .registers 8

    .prologue
    const/4 v4, 0x0

    .line 1247
    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getChildCount()I

    move-result v0

    if-nez v0, :cond_8

    .line 1268
    :goto_7
    return-void

    .line 1251
    :cond_8
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Landroid/support/v4/widget/NestedScrollView;->d:J

    sub-long/2addr v0, v2

    .line 1252
    const-wide/16 v2, 0xfa

    cmp-long v0, v0, v2

    if-lez v0, :cond_57

    .line 1253
    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getHeight()I

    move-result v0

    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getPaddingBottom()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getPaddingTop()I

    move-result v1

    sub-int/2addr v0, v1

    .line 1254
    invoke-virtual {p0, v4}, Landroid/support/v4/widget/NestedScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    .line 1255
    sub-int v0, v1, v0

    invoke-static {v4, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 1256
    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getScrollY()I

    move-result v1

    .line 1257
    add-int v2, v1, p2

    invoke-static {v2, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v4, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    sub-int/2addr v0, v1

    .line 1259
    iget-object v2, p0, Landroid/support/v4/widget/NestedScrollView;->f:Landroid/support/v4/widget/ca;

    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getScrollX()I

    move-result v3

    .line 8352
    iget-object v4, v2, Landroid/support/v4/widget/ca;->b:Landroid/support/v4/widget/cb;

    iget-object v2, v2, Landroid/support/v4/widget/ca;->a:Ljava/lang/Object;

    invoke-interface {v4, v2, v3, v1, v0}, Landroid/support/v4/widget/cb;->a(Ljava/lang/Object;III)V

    .line 1260
    invoke-static {p0}, Landroid/support/v4/view/cx;->b(Landroid/view/View;)V

    .line 1267
    :goto_50
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Landroid/support/v4/widget/NestedScrollView;->d:J

    goto :goto_7

    .line 1262
    :cond_57
    iget-object v0, p0, Landroid/support/v4/widget/NestedScrollView;->f:Landroid/support/v4/widget/ca;

    invoke-virtual {v0}, Landroid/support/v4/widget/ca;->a()Z

    move-result v0

    if-nez v0, :cond_64

    .line 1263
    iget-object v0, p0, Landroid/support/v4/widget/NestedScrollView;->f:Landroid/support/v4/widget/ca;

    invoke-virtual {v0}, Landroid/support/v4/widget/ca;->g()V

    .line 1265
    :cond_64
    invoke-virtual {p0, p1, p2}, Landroid/support/v4/widget/NestedScrollView;->scrollBy(II)V

    goto :goto_50
.end method

.method private b(Landroid/view/View;)V
    .registers 4

    .prologue
    .line 1373
    iget-object v0, p0, Landroid/support/v4/widget/NestedScrollView;->e:Landroid/graphics/Rect;

    invoke-virtual {p1, v0}, Landroid/view/View;->getDrawingRect(Landroid/graphics/Rect;)V

    .line 1376
    iget-object v0, p0, Landroid/support/v4/widget/NestedScrollView;->e:Landroid/graphics/Rect;

    invoke-virtual {p0, p1, v0}, Landroid/support/v4/widget/NestedScrollView;->offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 1378
    iget-object v0, p0, Landroid/support/v4/widget/NestedScrollView;->e:Landroid/graphics/Rect;

    invoke-direct {p0, v0}, Landroid/support/v4/widget/NestedScrollView;->a(Landroid/graphics/Rect;)I

    move-result v0

    .line 1380
    if-eqz v0, :cond_16

    .line 1381
    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Landroid/support/v4/widget/NestedScrollView;->scrollBy(II)V

    .line 1383
    :cond_16
    return-void
.end method

.method private b()Z
    .registers 5

    .prologue
    const/4 v0, 0x0

    .line 382
    invoke-virtual {p0, v0}, Landroid/support/v4/widget/NestedScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 383
    if-eqz v1, :cond_1c

    .line 384
    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    .line 385
    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getHeight()I

    move-result v2

    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getPaddingTop()I

    move-result v3

    add-int/2addr v1, v3

    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getPaddingBottom()I

    move-result v3

    add-int/2addr v1, v3

    if-ge v2, v1, :cond_1c

    const/4 v0, 0x1

    .line 387
    :cond_1c
    return v0
.end method

.method private b(I)Z
    .registers 6

    .prologue
    const/4 v1, 0x0

    .line 1056
    const/16 v0, 0x82

    if-ne p1, v0, :cond_4b

    const/4 v0, 0x1

    .line 1057
    :goto_6
    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getHeight()I

    move-result v2

    .line 1059
    if-eqz v0, :cond_4d

    .line 1060
    iget-object v0, p0, Landroid/support/v4/widget/NestedScrollView;->e:Landroid/graphics/Rect;

    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getScrollY()I

    move-result v1

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->top:I

    .line 1061
    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getChildCount()I

    move-result v0

    .line 1062
    if-lez v0, :cond_35

    .line 1063
    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Landroid/support/v4/widget/NestedScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1064
    iget-object v1, p0, Landroid/support/v4/widget/NestedScrollView;->e:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    add-int/2addr v1, v2

    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v3

    if-le v1, v3, :cond_35

    .line 1065
    iget-object v1, p0, Landroid/support/v4/widget/NestedScrollView;->e:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v0

    sub-int/2addr v0, v2

    iput v0, v1, Landroid/graphics/Rect;->top:I

    .line 1074
    :cond_35
    :goto_35
    iget-object v0, p0, Landroid/support/v4/widget/NestedScrollView;->e:Landroid/graphics/Rect;

    iget-object v1, p0, Landroid/support/v4/widget/NestedScrollView;->e:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    .line 1076
    iget-object v0, p0, Landroid/support/v4/widget/NestedScrollView;->e:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    iget-object v1, p0, Landroid/support/v4/widget/NestedScrollView;->e:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    invoke-direct {p0, p1, v0, v1}, Landroid/support/v4/widget/NestedScrollView;->a(III)Z

    move-result v0

    return v0

    :cond_4b
    move v0, v1

    .line 1056
    goto :goto_6

    .line 1069
    :cond_4d
    iget-object v0, p0, Landroid/support/v4/widget/NestedScrollView;->e:Landroid/graphics/Rect;

    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getScrollY()I

    move-result v3

    sub-int/2addr v3, v2

    iput v3, v0, Landroid/graphics/Rect;->top:I

    .line 1070
    iget-object v0, p0, Landroid/support/v4/widget/NestedScrollView;->e:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    if-gez v0, :cond_35

    .line 1071
    iget-object v0, p0, Landroid/support/v4/widget/NestedScrollView;->e:Landroid/graphics/Rect;

    iput v1, v0, Landroid/graphics/Rect;->top:I

    goto :goto_35
.end method

.method private c()Z
    .registers 2

    .prologue
    .line 398
    iget-boolean v0, p0, Landroid/support/v4/widget/NestedScrollView;->o:Z

    return v0
.end method

.method private c(I)Z
    .registers 6

    .prologue
    const/4 v1, 0x0

    .line 1092
    const/16 v0, 0x82

    if-ne p1, v0, :cond_43

    const/4 v0, 0x1

    .line 1093
    :goto_6
    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getHeight()I

    move-result v2

    .line 1095
    iget-object v3, p0, Landroid/support/v4/widget/NestedScrollView;->e:Landroid/graphics/Rect;

    iput v1, v3, Landroid/graphics/Rect;->top:I

    .line 1096
    iget-object v1, p0, Landroid/support/v4/widget/NestedScrollView;->e:Landroid/graphics/Rect;

    iput v2, v1, Landroid/graphics/Rect;->bottom:I

    .line 1098
    if-eqz v0, :cond_36

    .line 1099
    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getChildCount()I

    move-result v0

    .line 1100
    if-lez v0, :cond_36

    .line 1101
    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Landroid/support/v4/widget/NestedScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1102
    iget-object v1, p0, Landroid/support/v4/widget/NestedScrollView;->e:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v0

    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getPaddingBottom()I

    move-result v3

    add-int/2addr v0, v3

    iput v0, v1, Landroid/graphics/Rect;->bottom:I

    .line 1103
    iget-object v0, p0, Landroid/support/v4/widget/NestedScrollView;->e:Landroid/graphics/Rect;

    iget-object v1, p0, Landroid/support/v4/widget/NestedScrollView;->e:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->top:I

    .line 1107
    :cond_36
    iget-object v0, p0, Landroid/support/v4/widget/NestedScrollView;->e:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    iget-object v1, p0, Landroid/support/v4/widget/NestedScrollView;->e:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    invoke-direct {p0, p1, v0, v1}, Landroid/support/v4/widget/NestedScrollView;->a(III)Z

    move-result v0

    return v0

    :cond_43
    move v0, v1

    .line 1092
    goto :goto_6
.end method

.method private d()Z
    .registers 2

    .prologue
    .line 421
    iget-boolean v0, p0, Landroid/support/v4/widget/NestedScrollView;->p:Z

    return v0
.end method

.method private d(I)Z
    .registers 9

    .prologue
    const/16 v6, 0x82

    const/4 v2, 0x0

    .line 1156
    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->findFocus()Landroid/view/View;

    move-result-object v0

    .line 1157
    if-ne v0, p0, :cond_a

    const/4 v0, 0x0

    .line 1159
    :cond_a
    invoke-static {}, Landroid/view/FocusFinder;->getInstance()Landroid/view/FocusFinder;

    move-result-object v1

    invoke-virtual {v1, p0, v0, p1}, Landroid/view/FocusFinder;->findNextFocus(Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;

    move-result-object v3

    .line 1161
    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getMaxScrollAmount()I

    move-result v1

    .line 1163
    if-eqz v3, :cond_57

    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getHeight()I

    move-result v4

    invoke-direct {p0, v3, v1, v4}, Landroid/support/v4/widget/NestedScrollView;->a(Landroid/view/View;II)Z

    move-result v4

    if-eqz v4, :cond_57

    .line 1164
    iget-object v1, p0, Landroid/support/v4/widget/NestedScrollView;->e:Landroid/graphics/Rect;

    invoke-virtual {v3, v1}, Landroid/view/View;->getDrawingRect(Landroid/graphics/Rect;)V

    .line 1165
    iget-object v1, p0, Landroid/support/v4/widget/NestedScrollView;->e:Landroid/graphics/Rect;

    invoke-virtual {p0, v3, v1}, Landroid/support/v4/widget/NestedScrollView;->offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 1166
    iget-object v1, p0, Landroid/support/v4/widget/NestedScrollView;->e:Landroid/graphics/Rect;

    invoke-direct {p0, v1}, Landroid/support/v4/widget/NestedScrollView;->a(Landroid/graphics/Rect;)I

    move-result v1

    .line 1167
    invoke-direct {p0, v1}, Landroid/support/v4/widget/NestedScrollView;->e(I)V

    .line 1168
    invoke-virtual {v3, p1}, Landroid/view/View;->requestFocus(I)Z

    .line 1190
    :goto_38
    if-eqz v0, :cond_55

    invoke-virtual {v0}, Landroid/view/View;->isFocused()Z

    move-result v1

    if-eqz v1, :cond_55

    invoke-direct {p0, v0}, Landroid/support/v4/widget/NestedScrollView;->a(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_55

    .line 1197
    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getDescendantFocusability()I

    move-result v0

    .line 1198
    const/high16 v1, 0x20000

    invoke-virtual {p0, v1}, Landroid/support/v4/widget/NestedScrollView;->setDescendantFocusability(I)V

    .line 1199
    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->requestFocus()Z

    .line 1200
    invoke-virtual {p0, v0}, Landroid/support/v4/widget/NestedScrollView;->setDescendantFocusability(I)V

    .line 1202
    :cond_55
    const/4 v0, 0x1

    :goto_56
    return v0

    .line 1173
    :cond_57
    const/16 v3, 0x21

    if-ne p1, v3, :cond_69

    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getScrollY()I

    move-result v3

    if-ge v3, v1, :cond_69

    .line 1174
    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getScrollY()I

    move-result v1

    .line 1184
    :cond_65
    :goto_65
    if-nez v1, :cond_8e

    move v0, v2

    .line 1185
    goto :goto_56

    .line 1175
    :cond_69
    if-ne p1, v6, :cond_65

    .line 1176
    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getChildCount()I

    move-result v3

    if-lez v3, :cond_65

    .line 1177
    invoke-virtual {p0, v2}, Landroid/support/v4/widget/NestedScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getBottom()I

    move-result v3

    .line 1178
    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getScrollY()I

    move-result v4

    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getHeight()I

    move-result v5

    add-int/2addr v4, v5

    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getPaddingBottom()I

    move-result v5

    sub-int/2addr v4, v5

    .line 1179
    sub-int v5, v3, v4

    if-ge v5, v1, :cond_65

    .line 1180
    sub-int v1, v3, v4

    goto :goto_65

    .line 1187
    :cond_8e
    if-ne p1, v6, :cond_94

    :goto_90
    invoke-direct {p0, v1}, Landroid/support/v4/widget/NestedScrollView;->e(I)V

    goto :goto_38

    :cond_94
    neg-int v1, v1

    goto :goto_90
.end method

.method private e()V
    .registers 2

    .prologue
    .line 532
    iget-object v0, p0, Landroid/support/v4/widget/NestedScrollView;->n:Landroid/view/VelocityTracker;

    if-nez v0, :cond_b

    .line 533
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/widget/NestedScrollView;->n:Landroid/view/VelocityTracker;

    .line 537
    :goto_a
    return-void

    .line 535
    :cond_b
    iget-object v0, p0, Landroid/support/v4/widget/NestedScrollView;->n:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->clear()V

    goto :goto_a
.end method

.method private e(I)V
    .registers 4

    .prologue
    const/4 v1, 0x0

    .line 1231
    if-eqz p1, :cond_a

    .line 1232
    iget-boolean v0, p0, Landroid/support/v4/widget/NestedScrollView;->p:Z

    if-eqz v0, :cond_b

    .line 1233
    invoke-direct {p0, v1, p1}, Landroid/support/v4/widget/NestedScrollView;->b(II)V

    .line 1238
    :cond_a
    :goto_a
    return-void

    .line 1235
    :cond_b
    invoke-virtual {p0, v1, p1}, Landroid/support/v4/widget/NestedScrollView;->scrollBy(II)V

    goto :goto_a
.end method

.method private f()V
    .registers 2

    .prologue
    .line 540
    iget-object v0, p0, Landroid/support/v4/widget/NestedScrollView;->n:Landroid/view/VelocityTracker;

    if-nez v0, :cond_a

    .line 541
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/widget/NestedScrollView;->n:Landroid/view/VelocityTracker;

    .line 543
    :cond_a
    return-void
.end method

.method private f(I)V
    .registers 9

    .prologue
    const/4 v5, 0x0

    .line 1612
    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getChildCount()I

    move-result v0

    if-lez v0, :cond_39

    .line 1613
    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getHeight()I

    move-result v0

    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getPaddingBottom()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getPaddingTop()I

    move-result v1

    sub-int/2addr v0, v1

    .line 1614
    invoke-virtual {p0, v5}, Landroid/support/v4/widget/NestedScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    .line 1616
    iget-object v4, p0, Landroid/support/v4/widget/NestedScrollView;->f:Landroid/support/v4/widget/ca;

    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getScrollX()I

    move-result v2

    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getScrollY()I

    move-result v3

    sub-int/2addr v1, v0

    invoke-static {v5, v1}, Ljava/lang/Math;->max(II)I

    move-result v5

    div-int/lit8 v6, v0, 0x2

    .line 8421
    iget-object v0, v4, Landroid/support/v4/widget/ca;->b:Landroid/support/v4/widget/cb;

    iget-object v1, v4, Landroid/support/v4/widget/ca;->a:Ljava/lang/Object;

    move v4, p1

    invoke-interface/range {v0 .. v6}, Landroid/support/v4/widget/cb;->b(Ljava/lang/Object;IIIII)V

    .line 1619
    invoke-static {p0}, Landroid/support/v4/view/cx;->b(Landroid/view/View;)V

    .line 1621
    :cond_39
    return-void
.end method

.method private g()V
    .registers 2

    .prologue
    .line 546
    iget-object v0, p0, Landroid/support/v4/widget/NestedScrollView;->n:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_c

    .line 547
    iget-object v0, p0, Landroid/support/v4/widget/NestedScrollView;->n:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    .line 548
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v4/widget/NestedScrollView;->n:Landroid/view/VelocityTracker;

    .line 550
    :cond_c
    return-void
.end method

.method private g(I)V
    .registers 10

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 1624
    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getScrollY()I

    move-result v0

    .line 1625
    if-gtz v0, :cond_a

    if-lez p1, :cond_59

    :cond_a
    invoke-direct {p0}, Landroid/support/v4/widget/NestedScrollView;->getScrollRange()I

    move-result v2

    if-lt v0, v2, :cond_12

    if-gez p1, :cond_59

    :cond_12
    const/4 v0, 0x1

    .line 1627
    :goto_13
    int-to-float v2, p1

    invoke-virtual {p0, v3, v2}, Landroid/support/v4/widget/NestedScrollView;->dispatchNestedPreFling(FF)Z

    move-result v2

    if-nez v2, :cond_58

    .line 1628
    int-to-float v2, p1

    invoke-virtual {p0, v3, v2, v0}, Landroid/support/v4/widget/NestedScrollView;->dispatchNestedFling(FFZ)Z

    .line 1629
    if-eqz v0, :cond_58

    .line 8612
    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getChildCount()I

    move-result v0

    if-lez v0, :cond_58

    .line 8613
    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getHeight()I

    move-result v0

    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getPaddingBottom()I

    move-result v2

    sub-int/2addr v0, v2

    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getPaddingTop()I

    move-result v2

    sub-int/2addr v0, v2

    .line 8614
    invoke-virtual {p0, v1}, Landroid/support/v4/widget/NestedScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v4

    .line 8616
    iget-object v7, p0, Landroid/support/v4/widget/NestedScrollView;->f:Landroid/support/v4/widget/ca;

    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getScrollX()I

    move-result v2

    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getScrollY()I

    move-result v3

    sub-int/2addr v4, v0

    invoke-static {v1, v4}, Ljava/lang/Math;->max(II)I

    move-result v5

    div-int/lit8 v6, v0, 0x2

    .line 9421
    iget-object v0, v7, Landroid/support/v4/widget/ca;->b:Landroid/support/v4/widget/cb;

    iget-object v1, v7, Landroid/support/v4/widget/ca;->a:Ljava/lang/Object;

    move v4, p1

    invoke-interface/range {v0 .. v6}, Landroid/support/v4/widget/cb;->b(Ljava/lang/Object;IIIII)V

    .line 8619
    invoke-static {p0}, Landroid/support/v4/view/cx;->b(Landroid/view/View;)V

    .line 1633
    :cond_58
    return-void

    :cond_59
    move v0, v1

    .line 1625
    goto :goto_13
.end method

.method private getScrollRange()I
    .registers 5

    .prologue
    const/4 v0, 0x0

    .line 951
    .line 952
    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getChildCount()I

    move-result v1

    if-lez v1, :cond_22

    .line 953
    invoke-virtual {p0, v0}, Landroid/support/v4/widget/NestedScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 954
    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getHeight()I

    move-result v2

    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getPaddingBottom()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getPaddingTop()I

    move-result v3

    sub-int/2addr v2, v3

    sub-int/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 957
    :cond_22
    return v0
.end method

.method private getVerticalScrollFactorCompat()F
    .registers 6

    .prologue
    .line 877
    iget v0, p0, Landroid/support/v4/widget/NestedScrollView;->D:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_34

    .line 878
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 879
    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 880
    invoke-virtual {v1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v2

    const v3, 0x101004d

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v0, v4}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    move-result v2

    if-nez v2, :cond_26

    .line 882
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Expected theme to define listPreferredItemHeight."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 885
    :cond_26
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/util/TypedValue;->getDimension(Landroid/util/DisplayMetrics;)F

    move-result v0

    iput v0, p0, Landroid/support/v4/widget/NestedScrollView;->D:F

    .line 888
    :cond_34
    iget v0, p0, Landroid/support/v4/widget/NestedScrollView;->D:F

    return v0
.end method

.method private h()V
    .registers 2

    .prologue
    .line 1636
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v4/widget/NestedScrollView;->m:Z

    .line 1638
    invoke-direct {p0}, Landroid/support/v4/widget/NestedScrollView;->g()V

    .line 1639
    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->stopNestedScroll()V

    .line 1641
    iget-object v0, p0, Landroid/support/v4/widget/NestedScrollView;->g:Landroid/support/v4/widget/al;

    if-eqz v0, :cond_17

    .line 1642
    iget-object v0, p0, Landroid/support/v4/widget/NestedScrollView;->g:Landroid/support/v4/widget/al;

    invoke-virtual {v0}, Landroid/support/v4/widget/al;->c()Z

    .line 1643
    iget-object v0, p0, Landroid/support/v4/widget/NestedScrollView;->h:Landroid/support/v4/widget/al;

    invoke-virtual {v0}, Landroid/support/v4/widget/al;->c()Z

    .line 1645
    :cond_17
    return-void
.end method

.method private i()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 1666
    invoke-static {p0}, Landroid/support/v4/view/cx;->a(Landroid/view/View;)I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1f

    .line 1667
    iget-object v0, p0, Landroid/support/v4/widget/NestedScrollView;->g:Landroid/support/v4/widget/al;

    if-nez v0, :cond_1e

    .line 1668
    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 1669
    new-instance v1, Landroid/support/v4/widget/al;

    invoke-direct {v1, v0}, Landroid/support/v4/widget/al;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Landroid/support/v4/widget/NestedScrollView;->g:Landroid/support/v4/widget/al;

    .line 1670
    new-instance v1, Landroid/support/v4/widget/al;

    invoke-direct {v1, v0}, Landroid/support/v4/widget/al;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Landroid/support/v4/widget/NestedScrollView;->h:Landroid/support/v4/widget/al;

    .line 1676
    :cond_1e
    :goto_1e
    return-void

    .line 1673
    :cond_1f
    iput-object v2, p0, Landroid/support/v4/widget/NestedScrollView;->g:Landroid/support/v4/widget/al;

    .line 1674
    iput-object v2, p0, Landroid/support/v4/widget/NestedScrollView;->h:Landroid/support/v4/widget/al;

    goto :goto_1e
.end method


# virtual methods
.method public final a(I)V
    .registers 4

    .prologue
    .line 1277
    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getScrollX()I

    move-result v0

    rsub-int/lit8 v0, v0, 0x0

    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getScrollY()I

    move-result v1

    sub-int v1, p1, v1

    invoke-direct {p0, v0, v1}, Landroid/support/v4/widget/NestedScrollView;->b(II)V

    .line 1278
    return-void
.end method

.method public final addView(Landroid/view/View;)V
    .registers 4

    .prologue
    .line 344
    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getChildCount()I

    move-result v0

    if-lez v0, :cond_e

    .line 345
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "ScrollView can host only one direct child"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 348
    :cond_e
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 349
    return-void
.end method

.method public final addView(Landroid/view/View;I)V
    .registers 5

    .prologue
    .line 353
    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getChildCount()I

    move-result v0

    if-lez v0, :cond_e

    .line 354
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "ScrollView can host only one direct child"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 357
    :cond_e
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;I)V

    .line 358
    return-void
.end method

.method public final addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
    .registers 6

    .prologue
    .line 371
    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getChildCount()I

    move-result v0

    if-lez v0, :cond_e

    .line 372
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "ScrollView can host only one direct child"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 375
    :cond_e
    invoke-super {p0, p1, p2, p3}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 376
    return-void
.end method

.method public final addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .registers 5

    .prologue
    .line 362
    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getChildCount()I

    move-result v0

    if-lez v0, :cond_e

    .line 363
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "ScrollView can host only one direct child"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 366
    :cond_e
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 367
    return-void
.end method

.method public final computeScroll()V
    .registers 9

    .prologue
    const/4 v0, 0x1

    .line 1340
    iget-object v1, p0, Landroid/support/v4/widget/NestedScrollView;->f:Landroid/support/v4/widget/ca;

    invoke-virtual {v1}, Landroid/support/v4/widget/ca;->f()Z

    move-result v1

    if-eqz v1, :cond_4c

    .line 1341
    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getScrollX()I

    move-result v3

    .line 1342
    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getScrollY()I

    move-result v4

    .line 1343
    iget-object v1, p0, Landroid/support/v4/widget/NestedScrollView;->f:Landroid/support/v4/widget/ca;

    invoke-virtual {v1}, Landroid/support/v4/widget/ca;->b()I

    move-result v1

    .line 1344
    iget-object v2, p0, Landroid/support/v4/widget/NestedScrollView;->f:Landroid/support/v4/widget/ca;

    invoke-virtual {v2}, Landroid/support/v4/widget/ca;->c()I

    move-result v7

    .line 1346
    if-ne v3, v1, :cond_21

    if-eq v4, v7, :cond_4c

    .line 1347
    :cond_21
    invoke-direct {p0}, Landroid/support/v4/widget/NestedScrollView;->getScrollRange()I

    move-result v5

    .line 1348
    invoke-static {p0}, Landroid/support/v4/view/cx;->a(Landroid/view/View;)I

    move-result v2

    .line 1349
    if-eqz v2, :cond_2f

    if-ne v2, v0, :cond_4d

    if-lez v5, :cond_4d

    :cond_2f
    move v6, v0

    .line 1352
    :goto_30
    sub-int/2addr v1, v3

    sub-int v2, v7, v4

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Landroid/support/v4/widget/NestedScrollView;->a(IIIII)Z

    .line 1355
    if-eqz v6, :cond_4c

    .line 1356
    invoke-direct {p0}, Landroid/support/v4/widget/NestedScrollView;->i()V

    .line 1357
    if-gtz v7, :cond_50

    if-lez v4, :cond_50

    .line 1358
    iget-object v0, p0, Landroid/support/v4/widget/NestedScrollView;->g:Landroid/support/v4/widget/al;

    iget-object v1, p0, Landroid/support/v4/widget/NestedScrollView;->f:Landroid/support/v4/widget/ca;

    invoke-virtual {v1}, Landroid/support/v4/widget/ca;->e()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/al;->a(I)Z

    .line 1365
    :cond_4c
    :goto_4c
    return-void

    .line 1349
    :cond_4d
    const/4 v0, 0x0

    move v6, v0

    goto :goto_30

    .line 1359
    :cond_50
    if-lt v7, v5, :cond_4c

    if-ge v4, v5, :cond_4c

    .line 1360
    iget-object v0, p0, Landroid/support/v4/widget/NestedScrollView;->h:Landroid/support/v4/widget/al;

    iget-object v1, p0, Landroid/support/v4/widget/NestedScrollView;->f:Landroid/support/v4/widget/ca;

    invoke-virtual {v1}, Landroid/support/v4/widget/ca;->e()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/al;->a(I)Z

    goto :goto_4c
.end method

.method protected final computeVerticalScrollOffset()I
    .registers 3

    .prologue
    .line 1306
    const/4 v0, 0x0

    invoke-super {p0}, Landroid/widget/FrameLayout;->computeVerticalScrollOffset()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method protected final computeVerticalScrollRange()I
    .registers 5

    .prologue
    const/4 v3, 0x0

    .line 1286
    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getChildCount()I

    move-result v0

    .line 1287
    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getHeight()I

    move-result v1

    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getPaddingBottom()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getPaddingTop()I

    move-result v2

    sub-int/2addr v1, v2

    .line 1288
    if-nez v0, :cond_17

    move v0, v1

    .line 1301
    :cond_16
    :goto_16
    return v0

    .line 1292
    :cond_17
    invoke-virtual {p0, v3}, Landroid/support/v4/widget/NestedScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v0

    .line 1293
    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getScrollY()I

    move-result v2

    .line 1294
    sub-int v1, v0, v1

    invoke-static {v3, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 1295
    if-gez v2, :cond_2d

    .line 1296
    sub-int/2addr v0, v2

    goto :goto_16

    .line 1297
    :cond_2d
    if-le v2, v1, :cond_16

    .line 1298
    sub-int v1, v2, v1

    add-int/2addr v0, v1

    goto :goto_16
.end method

.method public final dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .registers 9

    .prologue
    const/16 v0, 0x21

    const/4 v1, 0x1

    const/16 v4, 0x82

    const/4 v2, 0x0

    .line 466
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v3

    if-nez v3, :cond_57

    .line 4478
    iget-object v3, p0, Landroid/support/v4/widget/NestedScrollView;->e:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->setEmpty()V

    .line 5382
    invoke-virtual {p0, v2}, Landroid/support/v4/widget/NestedScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 5383
    if-eqz v3, :cond_5b

    .line 5384
    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v3

    .line 5385
    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getHeight()I

    move-result v5

    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getPaddingTop()I

    move-result v6

    add-int/2addr v3, v6

    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getPaddingBottom()I

    move-result v6

    add-int/2addr v3, v6

    if-ge v5, v3, :cond_59

    move v3, v1

    .line 4480
    :goto_2c
    if-nez v3, :cond_61

    .line 4481
    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_5f

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/4 v3, 0x4

    if-eq v0, v3, :cond_5f

    .line 4482
    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->findFocus()Landroid/view/View;

    move-result-object v0

    .line 4483
    if-ne v0, p0, :cond_42

    const/4 v0, 0x0

    .line 4484
    :cond_42
    invoke-static {}, Landroid/view/FocusFinder;->getInstance()Landroid/view/FocusFinder;

    move-result-object v3

    invoke-virtual {v3, p0, v0, v4}, Landroid/view/FocusFinder;->findNextFocus(Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    .line 4486
    if-eqz v0, :cond_5d

    if-eq v0, p0, :cond_5d

    invoke-virtual {v0, v4}, Landroid/view/View;->requestFocus(I)Z

    move-result v0

    if-eqz v0, :cond_5d

    move v0, v1

    .line 466
    :goto_55
    if-eqz v0, :cond_58

    :cond_57
    move v2, v1

    :cond_58
    return v2

    :cond_59
    move v3, v2

    .line 5385
    goto :goto_2c

    :cond_5b
    move v3, v2

    .line 5387
    goto :goto_2c

    :cond_5d
    move v0, v2

    .line 4486
    goto :goto_55

    :cond_5f
    move v0, v2

    .line 4490
    goto :goto_55

    .line 4494
    :cond_61
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v3

    if-nez v3, :cond_6e

    .line 4495
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v3

    sparse-switch v3, :sswitch_data_f6

    :cond_6e
    :goto_6e
    move v0, v2

    goto :goto_55

    .line 4497
    :sswitch_70
    invoke-virtual {p1}, Landroid/view/KeyEvent;->isAltPressed()Z

    move-result v3

    if-nez v3, :cond_7b

    .line 4498
    invoke-direct {p0, v0}, Landroid/support/v4/widget/NestedScrollView;->d(I)Z

    move-result v0

    goto :goto_55

    .line 4500
    :cond_7b
    invoke-direct {p0, v0}, Landroid/support/v4/widget/NestedScrollView;->c(I)Z

    move-result v0

    goto :goto_55

    .line 4504
    :sswitch_80
    invoke-virtual {p1}, Landroid/view/KeyEvent;->isAltPressed()Z

    move-result v0

    if-nez v0, :cond_8b

    .line 4505
    invoke-direct {p0, v4}, Landroid/support/v4/widget/NestedScrollView;->d(I)Z

    move-result v0

    goto :goto_55

    .line 4507
    :cond_8b
    invoke-direct {p0, v4}, Landroid/support/v4/widget/NestedScrollView;->c(I)Z

    move-result v0

    goto :goto_55

    .line 4511
    :sswitch_90
    invoke-virtual {p1}, Landroid/view/KeyEvent;->isShiftPressed()Z

    move-result v3

    if-eqz v3, :cond_de

    move v3, v0

    .line 6056
    :goto_97
    if-ne v3, v4, :cond_e0

    move v0, v1

    .line 6057
    :goto_9a
    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getHeight()I

    move-result v4

    .line 6059
    if-eqz v0, :cond_e2

    .line 6060
    iget-object v0, p0, Landroid/support/v4/widget/NestedScrollView;->e:Landroid/graphics/Rect;

    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getScrollY()I

    move-result v5

    add-int/2addr v5, v4

    iput v5, v0, Landroid/graphics/Rect;->top:I

    .line 6061
    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getChildCount()I

    move-result v0

    .line 6062
    if-lez v0, :cond_c9

    .line 6063
    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Landroid/support/v4/widget/NestedScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 6064
    iget-object v5, p0, Landroid/support/v4/widget/NestedScrollView;->e:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->top:I

    add-int/2addr v5, v4

    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v6

    if-le v5, v6, :cond_c9

    .line 6065
    iget-object v5, p0, Landroid/support/v4/widget/NestedScrollView;->e:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v0

    sub-int/2addr v0, v4

    iput v0, v5, Landroid/graphics/Rect;->top:I

    .line 6074
    :cond_c9
    :goto_c9
    iget-object v0, p0, Landroid/support/v4/widget/NestedScrollView;->e:Landroid/graphics/Rect;

    iget-object v5, p0, Landroid/support/v4/widget/NestedScrollView;->e:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->top:I

    add-int/2addr v4, v5

    iput v4, v0, Landroid/graphics/Rect;->bottom:I

    .line 6076
    iget-object v0, p0, Landroid/support/v4/widget/NestedScrollView;->e:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    iget-object v4, p0, Landroid/support/v4/widget/NestedScrollView;->e:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    invoke-direct {p0, v3, v0, v4}, Landroid/support/v4/widget/NestedScrollView;->a(III)Z

    goto :goto_6e

    :cond_de
    move v3, v4

    .line 4511
    goto :goto_97

    :cond_e0
    move v0, v2

    .line 6056
    goto :goto_9a

    .line 6069
    :cond_e2
    iget-object v0, p0, Landroid/support/v4/widget/NestedScrollView;->e:Landroid/graphics/Rect;

    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getScrollY()I

    move-result v5

    sub-int/2addr v5, v4

    iput v5, v0, Landroid/graphics/Rect;->top:I

    .line 6070
    iget-object v0, p0, Landroid/support/v4/widget/NestedScrollView;->e:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    if-gez v0, :cond_c9

    .line 6071
    iget-object v0, p0, Landroid/support/v4/widget/NestedScrollView;->e:Landroid/graphics/Rect;

    iput v2, v0, Landroid/graphics/Rect;->top:I

    goto :goto_c9

    .line 4495
    :sswitch_data_f6
    .sparse-switch
        0x13 -> :sswitch_70
        0x14 -> :sswitch_80
        0x3e -> :sswitch_90
    .end sparse-switch
.end method

.method public final dispatchNestedFling(FFZ)Z
    .registers 5

    .prologue
    .line 225
    iget-object v0, p0, Landroid/support/v4/widget/NestedScrollView;->C:Landroid/support/v4/view/bu;

    invoke-virtual {v0, p1, p2, p3}, Landroid/support/v4/view/bu;->a(FFZ)Z

    move-result v0

    return v0
.end method

.method public final dispatchNestedPreFling(FF)Z
    .registers 4

    .prologue
    .line 230
    iget-object v0, p0, Landroid/support/v4/widget/NestedScrollView;->C:Landroid/support/v4/view/bu;

    invoke-virtual {v0, p1, p2}, Landroid/support/v4/view/bu;->a(FF)Z

    move-result v0

    return v0
.end method

.method public final dispatchNestedPreScroll(II[I[I)Z
    .registers 6

    .prologue
    .line 220
    iget-object v0, p0, Landroid/support/v4/widget/NestedScrollView;->C:Landroid/support/v4/view/bu;

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/support/v4/view/bu;->a(II[I[I)Z

    move-result v0

    return v0
.end method

.method public final dispatchNestedScroll(IIII[I)Z
    .registers 12

    .prologue
    .line 214
    iget-object v0, p0, Landroid/support/v4/widget/NestedScrollView;->C:Landroid/support/v4/view/bu;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Landroid/support/v4/view/bu;->a(IIII[I)Z

    move-result v0

    return v0
.end method

.method public final draw(Landroid/graphics/Canvas;)V
    .registers 8

    .prologue
    .line 1680
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->draw(Landroid/graphics/Canvas;)V

    .line 1681
    iget-object v0, p0, Landroid/support/v4/widget/NestedScrollView;->g:Landroid/support/v4/widget/al;

    if-eqz v0, :cond_96

    .line 1682
    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getScrollY()I

    move-result v0

    .line 1683
    iget-object v1, p0, Landroid/support/v4/widget/NestedScrollView;->g:Landroid/support/v4/widget/al;

    invoke-virtual {v1}, Landroid/support/v4/widget/al;->a()Z

    move-result v1

    if-nez v1, :cond_4a

    .line 1684
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v1

    .line 1685
    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getWidth()I

    move-result v2

    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getPaddingLeft()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getPaddingRight()I

    move-result v3

    sub-int/2addr v2, v3

    .line 1687
    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getPaddingLeft()I

    move-result v3

    int-to-float v3, v3

    const/4 v4, 0x0

    invoke-static {v4, v0}, Ljava/lang/Math;->min(II)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1688
    iget-object v3, p0, Landroid/support/v4/widget/NestedScrollView;->g:Landroid/support/v4/widget/al;

    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getHeight()I

    move-result v4

    invoke-virtual {v3, v2, v4}, Landroid/support/v4/widget/al;->a(II)V

    .line 1689
    iget-object v2, p0, Landroid/support/v4/widget/NestedScrollView;->g:Landroid/support/v4/widget/al;

    invoke-virtual {v2, p1}, Landroid/support/v4/widget/al;->a(Landroid/graphics/Canvas;)Z

    move-result v2

    if-eqz v2, :cond_47

    .line 1690
    invoke-static {p0}, Landroid/support/v4/view/cx;->b(Landroid/view/View;)V

    .line 1692
    :cond_47
    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 1694
    :cond_4a
    iget-object v1, p0, Landroid/support/v4/widget/NestedScrollView;->h:Landroid/support/v4/widget/al;

    invoke-virtual {v1}, Landroid/support/v4/widget/al;->a()Z

    move-result v1

    if-nez v1, :cond_96

    .line 1695
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v1

    .line 1696
    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getWidth()I

    move-result v2

    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getPaddingLeft()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getPaddingRight()I

    move-result v3

    sub-int/2addr v2, v3

    .line 1697
    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getHeight()I

    move-result v3

    .line 1699
    neg-int v4, v2

    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getPaddingLeft()I

    move-result v5

    add-int/2addr v4, v5

    int-to-float v4, v4

    invoke-direct {p0}, Landroid/support/v4/widget/NestedScrollView;->getScrollRange()I

    move-result v5

    invoke-static {v5, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    add-int/2addr v0, v3

    int-to-float v0, v0

    invoke-virtual {p1, v4, v0}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1701
    const/high16 v0, 0x43340000    # 180.0f

    int-to-float v4, v2

    const/4 v5, 0x0

    invoke-virtual {p1, v0, v4, v5}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 1702
    iget-object v0, p0, Landroid/support/v4/widget/NestedScrollView;->h:Landroid/support/v4/widget/al;

    invoke-virtual {v0, v2, v3}, Landroid/support/v4/widget/al;->a(II)V

    .line 1703
    iget-object v0, p0, Landroid/support/v4/widget/NestedScrollView;->h:Landroid/support/v4/widget/al;

    invoke-virtual {v0, p1}, Landroid/support/v4/widget/al;->a(Landroid/graphics/Canvas;)Z

    move-result v0

    if-eqz v0, :cond_93

    .line 1704
    invoke-static {p0}, Landroid/support/v4/view/cx;->b(Landroid/view/View;)V

    .line 1706
    :cond_93
    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 1709
    :cond_96
    return-void
.end method

.method protected final getBottomFadingEdgeStrength()F
    .registers 5

    .prologue
    .line 309
    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getChildCount()I

    move-result v0

    if-nez v0, :cond_8

    .line 310
    const/4 v0, 0x0

    .line 320
    :goto_7
    return v0

    .line 313
    :cond_8
    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getVerticalFadingEdgeLength()I

    move-result v0

    .line 314
    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getHeight()I

    move-result v1

    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getPaddingBottom()I

    move-result v2

    sub-int/2addr v1, v2

    .line 315
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Landroid/support/v4/widget/NestedScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getBottom()I

    move-result v2

    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getScrollY()I

    move-result v3

    sub-int/2addr v2, v3

    sub-int v1, v2, v1

    .line 316
    if-ge v1, v0, :cond_2c

    .line 317
    int-to-float v1, v1

    int-to-float v0, v0

    div-float v0, v1, v0

    goto :goto_7

    .line 320
    :cond_2c
    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_7
.end method

.method public final getMaxScrollAmount()I
    .registers 3

    .prologue
    .line 328
    const/high16 v0, 0x3f000000    # 0.5f

    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getHeight()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method public final getNestedScrollAxes()I
    .registers 2

    .prologue
    .line 283
    iget-object v0, p0, Landroid/support/v4/widget/NestedScrollView;->B:Landroid/support/v4/view/bw;

    .line 4069
    iget v0, v0, Landroid/support/v4/view/bw;->a:I

    .line 283
    return v0
.end method

.method protected final getTopFadingEdgeStrength()F
    .registers 3

    .prologue
    .line 294
    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getChildCount()I

    move-result v0

    if-nez v0, :cond_8

    .line 295
    const/4 v0, 0x0

    .line 304
    :goto_7
    return v0

    .line 298
    :cond_8
    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getVerticalFadingEdgeLength()I

    move-result v0

    .line 299
    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getScrollY()I

    move-result v1

    .line 300
    if-ge v1, v0, :cond_17

    .line 301
    int-to-float v1, v1

    int-to-float v0, v0

    div-float v0, v1, v0

    goto :goto_7

    .line 304
    :cond_17
    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_7
.end method

.method public final hasNestedScrollingParent()Z
    .registers 2

    .prologue
    .line 208
    iget-object v0, p0, Landroid/support/v4/widget/NestedScrollView;->C:Landroid/support/v4/view/bu;

    invoke-virtual {v0}, Landroid/support/v4/view/bu;->a()Z

    move-result v0

    return v0
.end method

.method public final isNestedScrollingEnabled()Z
    .registers 2

    .prologue
    .line 193
    iget-object v0, p0, Landroid/support/v4/widget/NestedScrollView;->C:Landroid/support/v4/view/bu;

    .line 3076
    iget-boolean v0, v0, Landroid/support/v4/view/bu;->a:Z

    .line 193
    return v0
.end method

.method protected final measureChild(Landroid/view/View;II)V
    .registers 8

    .prologue
    const/4 v3, 0x0

    .line 1311
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 1316
    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getPaddingLeft()I

    move-result v1

    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getPaddingRight()I

    move-result v2

    add-int/2addr v1, v2

    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    invoke-static {p2, v1, v0}, Landroid/support/v4/widget/NestedScrollView;->getChildMeasureSpec(III)I

    move-result v0

    .line 1319
    invoke-static {v3, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 1321
    invoke-virtual {p1, v0, v1}, Landroid/view/View;->measure(II)V

    .line 1322
    return-void
.end method

.method protected final measureChildWithMargins(Landroid/view/View;IIII)V
    .registers 9

    .prologue
    .line 1327
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 1329
    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getPaddingLeft()I

    move-result v1

    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getPaddingRight()I

    move-result v2

    add-int/2addr v1, v2

    iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    add-int/2addr v1, v2

    iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    add-int/2addr v1, v2

    add-int/2addr v1, p3

    iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    invoke-static {p2, v1, v2}, Landroid/support/v4/widget/NestedScrollView;->getChildMeasureSpec(III)I

    move-result v1

    .line 1332
    iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int/2addr v0, v2

    const/4 v2, 0x0

    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 1335
    invoke-virtual {p1, v1, v0}, Landroid/view/View;->measure(II)V

    .line 1336
    return-void
.end method

.method public final onAttachedToWindow()V
    .registers 2

    .prologue
    .line 1570
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v4/widget/NestedScrollView;->k:Z

    .line 1571
    return-void
.end method

.method public final onGenericMotionEvent(Landroid/view/MotionEvent;)Z
    .registers 6

    .prologue
    const/4 v0, 0x0

    .line 848
    invoke-static {p1}, Landroid/support/v4/view/bk;->d(Landroid/view/MotionEvent;)I

    move-result v1

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_10

    .line 849
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_40

    .line 873
    :cond_10
    :goto_10
    return v0

    .line 851
    :pswitch_11
    iget-boolean v1, p0, Landroid/support/v4/widget/NestedScrollView;->m:Z

    if-nez v1, :cond_10

    .line 852
    invoke-static {p1}, Landroid/support/v4/view/bk;->e(Landroid/view/MotionEvent;)F

    move-result v1

    .line 854
    const/4 v2, 0x0

    cmpl-float v2, v1, v2

    if-eqz v2, :cond_10

    .line 855
    invoke-direct {p0}, Landroid/support/v4/widget/NestedScrollView;->getVerticalScrollFactorCompat()F

    move-result v2

    mul-float/2addr v1, v2

    float-to-int v2, v1

    .line 856
    invoke-direct {p0}, Landroid/support/v4/widget/NestedScrollView;->getScrollRange()I

    move-result v1

    .line 857
    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getScrollY()I

    move-result v3

    .line 858
    sub-int v2, v3, v2

    .line 859
    if-gez v2, :cond_3c

    move v1, v0

    .line 864
    :cond_31
    :goto_31
    if-eq v1, v3, :cond_10

    .line 865
    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getScrollX()I

    move-result v0

    invoke-super {p0, v0, v1}, Landroid/widget/FrameLayout;->scrollTo(II)V

    .line 866
    const/4 v0, 0x1

    goto :goto_10

    .line 861
    :cond_3c
    if-gt v2, v1, :cond_31

    move v1, v2

    goto :goto_31

    .line 849
    :pswitch_data_40
    .packed-switch 0x8
        :pswitch_11
    .end packed-switch
.end method

.method public final onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 10

    .prologue
    const/4 v7, 0x2

    const/4 v4, -0x1

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 574
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    .line 575
    if-ne v2, v7, :cond_f

    iget-boolean v3, p0, Landroid/support/v4/widget/NestedScrollView;->m:Z

    if-eqz v3, :cond_f

    .line 671
    :goto_e
    return v0

    .line 582
    :cond_f
    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getScrollY()I

    move-result v3

    if-nez v3, :cond_1d

    invoke-static {p0, v0}, Landroid/support/v4/view/cx;->b(Landroid/view/View;I)Z

    move-result v3

    if-nez v3, :cond_1d

    move v0, v1

    .line 583
    goto :goto_e

    .line 586
    :cond_1d
    and-int/lit16 v2, v2, 0xff

    packed-switch v2, :pswitch_data_fc

    .line 671
    :cond_22
    :goto_22
    :pswitch_22
    iget-boolean v0, p0, Landroid/support/v4/widget/NestedScrollView;->m:Z

    goto :goto_e

    .line 597
    :pswitch_25
    iget v2, p0, Landroid/support/v4/widget/NestedScrollView;->t:I

    .line 598
    if-eq v2, v4, :cond_22

    .line 603
    invoke-static {p1, v2}, Landroid/support/v4/view/bk;->a(Landroid/view/MotionEvent;I)I

    move-result v3

    .line 604
    if-ne v3, v4, :cond_4a

    .line 605
    const-string v0, "NestedScrollView"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Invalid pointerId="

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " in onInterceptTouchEvent"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_22

    .line 610
    :cond_4a
    invoke-static {p1, v3}, Landroid/support/v4/view/bk;->d(Landroid/view/MotionEvent;I)F

    move-result v2

    float-to-int v2, v2

    .line 611
    iget v3, p0, Landroid/support/v4/widget/NestedScrollView;->i:I

    sub-int v3, v2, v3

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v3

    .line 612
    iget v4, p0, Landroid/support/v4/widget/NestedScrollView;->q:I

    if-le v3, v4, :cond_22

    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getNestedScrollAxes()I

    move-result v3

    and-int/lit8 v3, v3, 0x2

    if-nez v3, :cond_22

    .line 614
    iput-boolean v0, p0, Landroid/support/v4/widget/NestedScrollView;->m:Z

    .line 615
    iput v2, p0, Landroid/support/v4/widget/NestedScrollView;->i:I

    .line 616
    invoke-direct {p0}, Landroid/support/v4/widget/NestedScrollView;->f()V

    .line 617
    iget-object v2, p0, Landroid/support/v4/widget/NestedScrollView;->n:Landroid/view/VelocityTracker;

    invoke-virtual {v2, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 618
    iput v1, p0, Landroid/support/v4/widget/NestedScrollView;->w:I

    .line 619
    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    .line 620
    if-eqz v1, :cond_22

    .line 621
    invoke-interface {v1, v0}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    goto :goto_22

    .line 628
    :pswitch_7b
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v3, v2

    .line 629
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    .line 7520
    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getChildCount()I

    move-result v4

    if-lez v4, :cond_ba

    .line 7521
    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getScrollY()I

    move-result v4

    .line 7522
    invoke-virtual {p0, v1}, Landroid/support/v4/widget/NestedScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 7523
    invoke-virtual {v5}, Landroid/view/View;->getTop()I

    move-result v6

    sub-int/2addr v6, v4

    if-lt v3, v6, :cond_b8

    invoke-virtual {v5}, Landroid/view/View;->getBottom()I

    move-result v6

    sub-int v4, v6, v4

    if-ge v3, v4, :cond_b8

    invoke-virtual {v5}, Landroid/view/View;->getLeft()I

    move-result v4

    if-lt v2, v4, :cond_b8

    invoke-virtual {v5}, Landroid/view/View;->getRight()I

    move-result v4

    if-ge v2, v4, :cond_b8

    move v2, v0

    .line 629
    :goto_af
    if-nez v2, :cond_bc

    .line 630
    iput-boolean v1, p0, Landroid/support/v4/widget/NestedScrollView;->m:Z

    .line 631
    invoke-direct {p0}, Landroid/support/v4/widget/NestedScrollView;->g()V

    goto/16 :goto_22

    :cond_b8
    move v2, v1

    .line 7523
    goto :goto_af

    :cond_ba
    move v2, v1

    .line 7528
    goto :goto_af

    .line 639
    :cond_bc
    iput v3, p0, Landroid/support/v4/widget/NestedScrollView;->i:I

    .line 640
    invoke-static {p1, v1}, Landroid/support/v4/view/bk;->b(Landroid/view/MotionEvent;I)I

    move-result v2

    iput v2, p0, Landroid/support/v4/widget/NestedScrollView;->t:I

    .line 7532
    iget-object v2, p0, Landroid/support/v4/widget/NestedScrollView;->n:Landroid/view/VelocityTracker;

    if-nez v2, :cond_e2

    .line 7533
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v2

    iput-object v2, p0, Landroid/support/v4/widget/NestedScrollView;->n:Landroid/view/VelocityTracker;

    .line 643
    :goto_ce
    iget-object v2, p0, Landroid/support/v4/widget/NestedScrollView;->n:Landroid/view/VelocityTracker;

    invoke-virtual {v2, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 649
    iget-object v2, p0, Landroid/support/v4/widget/NestedScrollView;->f:Landroid/support/v4/widget/ca;

    invoke-virtual {v2}, Landroid/support/v4/widget/ca;->a()Z

    move-result v2

    if-nez v2, :cond_e8

    :goto_db
    iput-boolean v0, p0, Landroid/support/v4/widget/NestedScrollView;->m:Z

    .line 650
    invoke-virtual {p0, v7}, Landroid/support/v4/widget/NestedScrollView;->startNestedScroll(I)Z

    goto/16 :goto_22

    .line 7535
    :cond_e2
    iget-object v2, p0, Landroid/support/v4/widget/NestedScrollView;->n:Landroid/view/VelocityTracker;

    invoke-virtual {v2}, Landroid/view/VelocityTracker;->clear()V

    goto :goto_ce

    :cond_e8
    move v0, v1

    .line 649
    goto :goto_db

    .line 657
    :pswitch_ea
    iput-boolean v1, p0, Landroid/support/v4/widget/NestedScrollView;->m:Z

    .line 658
    iput v4, p0, Landroid/support/v4/widget/NestedScrollView;->t:I

    .line 659
    invoke-direct {p0}, Landroid/support/v4/widget/NestedScrollView;->g()V

    .line 660
    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->stopNestedScroll()V

    goto/16 :goto_22

    .line 663
    :pswitch_f6
    invoke-direct {p0, p1}, Landroid/support/v4/widget/NestedScrollView;->a(Landroid/view/MotionEvent;)V

    goto/16 :goto_22

    .line 586
    nop

    :pswitch_data_fc
    .packed-switch 0x0
        :pswitch_7b
        :pswitch_ea
        :pswitch_25
        :pswitch_ea
        :pswitch_22
        :pswitch_22
        :pswitch_f6
    .end packed-switch
.end method

.method protected final onLayout(ZIIII)V
    .registers 10

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 1537
    invoke-super/range {p0 .. p5}, Landroid/widget/FrameLayout;->onLayout(ZIIII)V

    .line 1538
    iput-boolean v1, p0, Landroid/support/v4/widget/NestedScrollView;->j:Z

    .line 1540
    iget-object v0, p0, Landroid/support/v4/widget/NestedScrollView;->l:Landroid/view/View;

    if-eqz v0, :cond_18

    iget-object v0, p0, Landroid/support/v4/widget/NestedScrollView;->l:Landroid/view/View;

    invoke-static {v0, p0}, Landroid/support/v4/widget/NestedScrollView;->a(Landroid/view/View;Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 1541
    iget-object v0, p0, Landroid/support/v4/widget/NestedScrollView;->l:Landroid/view/View;

    invoke-direct {p0, v0}, Landroid/support/v4/widget/NestedScrollView;->b(Landroid/view/View;)V

    .line 1543
    :cond_18
    iput-object v3, p0, Landroid/support/v4/widget/NestedScrollView;->l:Landroid/view/View;

    .line 1545
    iget-boolean v0, p0, Landroid/support/v4/widget/NestedScrollView;->k:Z

    if-nez v0, :cond_5b

    .line 1546
    iget-object v0, p0, Landroid/support/v4/widget/NestedScrollView;->y:Landroid/support/v4/widget/NestedScrollView$SavedState;

    if-eqz v0, :cond_2f

    .line 1547
    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getScrollX()I

    move-result v0

    iget-object v2, p0, Landroid/support/v4/widget/NestedScrollView;->y:Landroid/support/v4/widget/NestedScrollView$SavedState;

    iget v2, v2, Landroid/support/v4/widget/NestedScrollView$SavedState;->a:I

    invoke-virtual {p0, v0, v2}, Landroid/support/v4/widget/NestedScrollView;->scrollTo(II)V

    .line 1548
    iput-object v3, p0, Landroid/support/v4/widget/NestedScrollView;->y:Landroid/support/v4/widget/NestedScrollView$SavedState;

    .line 1551
    :cond_2f
    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getChildCount()I

    move-result v0

    if-lez v0, :cond_6a

    invoke-virtual {p0, v1}, Landroid/support/v4/widget/NestedScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    .line 1552
    :goto_3d
    sub-int v2, p5, p3

    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getPaddingBottom()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getPaddingTop()I

    move-result v3

    sub-int/2addr v2, v3

    sub-int/2addr v0, v2

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 1556
    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getScrollY()I

    move-result v2

    if-le v2, v0, :cond_6c

    .line 1557
    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getScrollX()I

    move-result v1

    invoke-virtual {p0, v1, v0}, Landroid/support/v4/widget/NestedScrollView;->scrollTo(II)V

    .line 1564
    :cond_5b
    :goto_5b
    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getScrollX()I

    move-result v0

    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getScrollY()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Landroid/support/v4/widget/NestedScrollView;->scrollTo(II)V

    .line 1565
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/widget/NestedScrollView;->k:Z

    .line 1566
    return-void

    :cond_6a
    move v0, v1

    .line 1551
    goto :goto_3d

    .line 1558
    :cond_6c
    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getScrollY()I

    move-result v0

    if-gez v0, :cond_5b

    .line 1559
    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getScrollX()I

    move-result v0

    invoke-virtual {p0, v0, v1}, Landroid/support/v4/widget/NestedScrollView;->scrollTo(II)V

    goto :goto_5b
.end method

.method protected final onMeasure(II)V
    .registers 8

    .prologue
    .line 434
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    .line 436
    iget-boolean v0, p0, Landroid/support/v4/widget/NestedScrollView;->o:Z

    if-nez v0, :cond_8

    .line 461
    :cond_7
    :goto_7
    return-void

    .line 440
    :cond_8
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    .line 441
    if-eqz v0, :cond_7

    .line 445
    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getChildCount()I

    move-result v0

    if-lez v0, :cond_7

    .line 446
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/support/v4/widget/NestedScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 447
    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getMeasuredHeight()I

    move-result v2

    .line 448
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    if-ge v0, v2, :cond_7

    .line 449
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 451
    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getPaddingLeft()I

    move-result v3

    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getPaddingRight()I

    move-result v4

    add-int/2addr v3, v4

    iget v0, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    invoke-static {p1, v3, v0}, Landroid/support/v4/widget/NestedScrollView;->getChildMeasureSpec(III)I

    move-result v0

    .line 453
    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getPaddingTop()I

    move-result v3

    sub-int/2addr v2, v3

    .line 454
    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getPaddingBottom()I

    move-result v3

    sub-int/2addr v2, v3

    .line 455
    const/high16 v3, 0x40000000    # 2.0f

    invoke-static {v2, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 458
    invoke-virtual {v1, v0, v2}, Landroid/view/View;->measure(II)V

    goto :goto_7
.end method

.method public final onNestedFling(Landroid/view/View;FFZ)Z
    .registers 6

    .prologue
    .line 268
    if-nez p4, :cond_8

    .line 269
    float-to-int v0, p3

    invoke-direct {p0, v0}, Landroid/support/v4/widget/NestedScrollView;->g(I)V

    .line 270
    const/4 v0, 0x1

    .line 272
    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public final onNestedPreFling(Landroid/view/View;FF)Z
    .registers 5

    .prologue
    .line 278
    const/4 v0, 0x0

    return v0
.end method

.method public final onNestedPreScroll(Landroid/view/View;II[I)V
    .registers 5

    .prologue
    .line 264
    return-void
.end method

.method public final onNestedScroll(Landroid/view/View;IIII)V
    .registers 12

    .prologue
    const/4 v1, 0x0

    .line 254
    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getScrollY()I

    move-result v0

    .line 255
    invoke-virtual {p0, v1, p5}, Landroid/support/v4/widget/NestedScrollView;->scrollBy(II)V

    .line 256
    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getScrollY()I

    move-result v2

    sub-int/2addr v2, v0

    .line 257
    sub-int v4, p5, v2

    .line 258
    const/4 v5, 0x0

    move-object v0, p0

    move v3, v1

    invoke-virtual/range {v0 .. v5}, Landroid/support/v4/widget/NestedScrollView;->dispatchNestedScroll(IIII[I)Z

    .line 259
    return-void
.end method

.method public final onNestedScrollAccepted(Landroid/view/View;Landroid/view/View;I)V
    .registers 5

    .prologue
    .line 242
    iget-object v0, p0, Landroid/support/v4/widget/NestedScrollView;->B:Landroid/support/v4/view/bw;

    .line 4058
    iput p3, v0, Landroid/support/v4/view/bw;->a:I

    .line 243
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Landroid/support/v4/widget/NestedScrollView;->startNestedScroll(I)Z

    .line 244
    return-void
.end method

.method protected final onOverScrolled(IIZZ)V
    .registers 5

    .prologue
    .line 893
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->scrollTo(II)V

    .line 894
    return-void
.end method

.method protected final onRequestFocusInDescendants(ILandroid/graphics/Rect;)Z
    .registers 6

    .prologue
    const/4 v0, 0x0

    .line 1497
    const/4 v1, 0x2

    if-ne p1, v1, :cond_14

    .line 1498
    const/16 p1, 0x82

    .line 1503
    :cond_6
    :goto_6
    if-nez p2, :cond_1a

    invoke-static {}, Landroid/view/FocusFinder;->getInstance()Landroid/view/FocusFinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, p0, v2, p1}, Landroid/view/FocusFinder;->findNextFocus(Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    .line 1508
    :goto_11
    if-nez v1, :cond_23

    .line 1516
    :cond_13
    :goto_13
    return v0

    .line 1499
    :cond_14
    const/4 v1, 0x1

    if-ne p1, v1, :cond_6

    .line 1500
    const/16 p1, 0x21

    goto :goto_6

    .line 1503
    :cond_1a
    invoke-static {}, Landroid/view/FocusFinder;->getInstance()Landroid/view/FocusFinder;

    move-result-object v1

    invoke-virtual {v1, p0, p2, p1}, Landroid/view/FocusFinder;->findNextFocusFromRect(Landroid/view/ViewGroup;Landroid/graphics/Rect;I)Landroid/view/View;

    move-result-object v1

    goto :goto_11

    .line 1512
    :cond_23
    invoke-direct {p0, v1}, Landroid/support/v4/widget/NestedScrollView;->a(Landroid/view/View;)Z

    move-result v2

    if-nez v2, :cond_13

    .line 1516
    invoke-virtual {v1, p1, p2}, Landroid/view/View;->requestFocus(ILandroid/graphics/Rect;)Z

    move-result v0

    goto :goto_13
.end method

.method protected final onRestoreInstanceState(Landroid/os/Parcelable;)V
    .registers 3

    .prologue
    .line 1743
    check-cast p1, Landroid/support/v4/widget/NestedScrollView$SavedState;

    .line 1744
    invoke-virtual {p1}, Landroid/support/v4/widget/NestedScrollView$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/widget/FrameLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 1745
    iput-object p1, p0, Landroid/support/v4/widget/NestedScrollView;->y:Landroid/support/v4/widget/NestedScrollView$SavedState;

    .line 1746
    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->requestLayout()V

    .line 1747
    return-void
.end method

.method protected final onSaveInstanceState()Landroid/os/Parcelable;
    .registers 3

    .prologue
    .line 1751
    invoke-super {p0}, Landroid/widget/FrameLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 1752
    new-instance v1, Landroid/support/v4/widget/NestedScrollView$SavedState;

    invoke-direct {v1, v0}, Landroid/support/v4/widget/NestedScrollView$SavedState;-><init>(Landroid/os/Parcelable;)V

    .line 1753
    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getScrollY()I

    move-result v0

    iput v0, v1, Landroid/support/v4/widget/NestedScrollView$SavedState;->a:I

    .line 1754
    return-object v1
.end method

.method protected final onSizeChanged(IIII)V
    .registers 7

    .prologue
    .line 1575
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/FrameLayout;->onSizeChanged(IIII)V

    .line 1577
    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->findFocus()Landroid/view/View;

    move-result-object v0

    .line 1578
    if-eqz v0, :cond_b

    if-ne p0, v0, :cond_c

    .line 1590
    :cond_b
    :goto_b
    return-void

    .line 1584
    :cond_c
    const/4 v1, 0x0

    invoke-direct {p0, v0, v1, p4}, Landroid/support/v4/widget/NestedScrollView;->a(Landroid/view/View;II)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 1585
    iget-object v1, p0, Landroid/support/v4/widget/NestedScrollView;->e:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/view/View;->getDrawingRect(Landroid/graphics/Rect;)V

    .line 1586
    iget-object v1, p0, Landroid/support/v4/widget/NestedScrollView;->e:Landroid/graphics/Rect;

    invoke-virtual {p0, v0, v1}, Landroid/support/v4/widget/NestedScrollView;->offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 1587
    iget-object v0, p0, Landroid/support/v4/widget/NestedScrollView;->e:Landroid/graphics/Rect;

    invoke-direct {p0, v0}, Landroid/support/v4/widget/NestedScrollView;->a(Landroid/graphics/Rect;)I

    move-result v0

    .line 1588
    invoke-direct {p0, v0}, Landroid/support/v4/widget/NestedScrollView;->e(I)V

    goto :goto_b
.end method

.method public final onStartNestedScroll(Landroid/view/View;Landroid/view/View;I)Z
    .registers 5

    .prologue
    .line 237
    and-int/lit8 v0, p3, 0x2

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public final onStopNestedScroll(Landroid/view/View;)V
    .registers 2

    .prologue
    .line 248
    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->stopNestedScroll()V

    .line 249
    return-void
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 19

    .prologue
    .line 676
    invoke-direct/range {p0 .. p0}, Landroid/support/v4/widget/NestedScrollView;->f()V

    .line 678
    invoke-static/range {p1 .. p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v14

    .line 680
    invoke-static/range {p1 .. p1}, Landroid/support/v4/view/bk;->a(Landroid/view/MotionEvent;)I

    move-result v1

    .line 682
    if-nez v1, :cond_12

    .line 683
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Landroid/support/v4/widget/NestedScrollView;->w:I

    .line 685
    :cond_12
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget v3, v0, Landroid/support/v4/widget/NestedScrollView;->w:I

    int-to-float v3, v3

    invoke-virtual {v14, v2, v3}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    .line 687
    packed-switch v1, :pswitch_data_2b2

    .line 823
    :cond_1e
    :goto_1e
    :pswitch_1e
    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/v4/widget/NestedScrollView;->n:Landroid/view/VelocityTracker;

    if-eqz v1, :cond_2b

    .line 824
    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/v4/widget/NestedScrollView;->n:Landroid/view/VelocityTracker;

    invoke-virtual {v1, v14}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 826
    :cond_2b
    invoke-virtual {v14}, Landroid/view/MotionEvent;->recycle()V

    .line 827
    const/4 v1, 0x1

    :goto_2f
    return v1

    .line 689
    :pswitch_30
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/widget/NestedScrollView;->getChildCount()I

    move-result v1

    if-nez v1, :cond_38

    .line 690
    const/4 v1, 0x0

    goto :goto_2f

    .line 692
    :cond_38
    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/v4/widget/NestedScrollView;->f:Landroid/support/v4/widget/ca;

    invoke-virtual {v1}, Landroid/support/v4/widget/ca;->a()Z

    move-result v1

    if-nez v1, :cond_7f

    const/4 v1, 0x1

    :goto_43
    move-object/from16 v0, p0

    iput-boolean v1, v0, Landroid/support/v4/widget/NestedScrollView;->m:Z

    if-eqz v1, :cond_53

    .line 693
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/widget/NestedScrollView;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    .line 694
    if-eqz v1, :cond_53

    .line 695
    const/4 v2, 0x1

    invoke-interface {v1, v2}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 703
    :cond_53
    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/v4/widget/NestedScrollView;->f:Landroid/support/v4/widget/ca;

    invoke-virtual {v1}, Landroid/support/v4/widget/ca;->a()Z

    move-result v1

    if-nez v1, :cond_64

    .line 704
    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/v4/widget/NestedScrollView;->f:Landroid/support/v4/widget/ca;

    invoke-virtual {v1}, Landroid/support/v4/widget/ca;->g()V

    .line 708
    :cond_64
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    float-to-int v1, v1

    move-object/from16 v0, p0

    iput v1, v0, Landroid/support/v4/widget/NestedScrollView;->i:I

    .line 709
    const/4 v1, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v1}, Landroid/support/v4/view/bk;->b(Landroid/view/MotionEvent;I)I

    move-result v1

    move-object/from16 v0, p0

    iput v1, v0, Landroid/support/v4/widget/NestedScrollView;->t:I

    .line 710
    const/4 v1, 0x2

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/NestedScrollView;->startNestedScroll(I)Z

    goto :goto_1e

    .line 692
    :cond_7f
    const/4 v1, 0x0

    goto :goto_43

    .line 714
    :pswitch_81
    move-object/from16 v0, p0

    iget v1, v0, Landroid/support/v4/widget/NestedScrollView;->t:I

    move-object/from16 v0, p1

    invoke-static {v0, v1}, Landroid/support/v4/view/bk;->a(Landroid/view/MotionEvent;I)I

    move-result v15

    .line 716
    const/4 v1, -0x1

    if-ne v15, v1, :cond_ae

    .line 717
    const-string v1, "NestedScrollView"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid pointerId="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget v3, v0, Landroid/support/v4/widget/NestedScrollView;->t:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " in onTouchEvent"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1e

    .line 721
    :cond_ae
    move-object/from16 v0, p1

    invoke-static {v0, v15}, Landroid/support/v4/view/bk;->d(Landroid/view/MotionEvent;I)F

    move-result v1

    float-to-int v2, v1

    .line 722
    move-object/from16 v0, p0

    iget v1, v0, Landroid/support/v4/widget/NestedScrollView;->i:I

    sub-int/2addr v1, v2

    .line 723
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/v4/widget/NestedScrollView;->v:[I

    move-object/from16 v0, p0

    iget-object v5, v0, Landroid/support/v4/widget/NestedScrollView;->u:[I

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v1, v4, v5}, Landroid/support/v4/widget/NestedScrollView;->dispatchNestedPreScroll(II[I[I)Z

    move-result v3

    if-eqz v3, :cond_ef

    .line 724
    move-object/from16 v0, p0

    iget-object v3, v0, Landroid/support/v4/widget/NestedScrollView;->v:[I

    const/4 v4, 0x1

    aget v3, v3, v4

    sub-int/2addr v1, v3

    .line 725
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/v4/widget/NestedScrollView;->u:[I

    const/4 v5, 0x1

    aget v4, v4, v5

    int-to-float v4, v4

    invoke-virtual {v14, v3, v4}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    .line 726
    move-object/from16 v0, p0

    iget v3, v0, Landroid/support/v4/widget/NestedScrollView;->w:I

    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/v4/widget/NestedScrollView;->u:[I

    const/4 v5, 0x1

    aget v4, v4, v5

    add-int/2addr v3, v4

    move-object/from16 v0, p0

    iput v3, v0, Landroid/support/v4/widget/NestedScrollView;->w:I

    .line 728
    :cond_ef
    move-object/from16 v0, p0

    iget-boolean v3, v0, Landroid/support/v4/widget/NestedScrollView;->m:Z

    if-nez v3, :cond_2ae

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v3

    move-object/from16 v0, p0

    iget v4, v0, Landroid/support/v4/widget/NestedScrollView;->q:I

    if-le v3, v4, :cond_2ae

    .line 729
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/widget/NestedScrollView;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    .line 730
    if-eqz v3, :cond_109

    .line 731
    const/4 v4, 0x1

    invoke-interface {v3, v4}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 733
    :cond_109
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Landroid/support/v4/widget/NestedScrollView;->m:Z

    .line 734
    if-lez v1, :cond_19d

    .line 735
    move-object/from16 v0, p0

    iget v3, v0, Landroid/support/v4/widget/NestedScrollView;->q:I

    sub-int/2addr v1, v3

    move v3, v1

    .line 740
    :goto_116
    move-object/from16 v0, p0

    iget-boolean v1, v0, Landroid/support/v4/widget/NestedScrollView;->m:Z

    if-eqz v1, :cond_1e

    .line 742
    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/v4/widget/NestedScrollView;->u:[I

    const/4 v4, 0x1

    aget v1, v1, v4

    sub-int v1, v2, v1

    move-object/from16 v0, p0

    iput v1, v0, Landroid/support/v4/widget/NestedScrollView;->i:I

    .line 744
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/widget/NestedScrollView;->getScrollY()I

    move-result v16

    .line 745
    invoke-direct/range {p0 .. p0}, Landroid/support/v4/widget/NestedScrollView;->getScrollRange()I

    move-result v6

    .line 746
    invoke-static/range {p0 .. p0}, Landroid/support/v4/view/cx;->a(Landroid/view/View;)I

    move-result v1

    .line 747
    if-eqz v1, :cond_13c

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1a5

    if-lez v6, :cond_1a5

    :cond_13c
    const/4 v1, 0x1

    move v13, v1

    .line 753
    :goto_13e
    const/4 v2, 0x0

    const/4 v4, 0x0

    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/widget/NestedScrollView;->getScrollY()I

    move-result v5

    move-object/from16 v1, p0

    invoke-direct/range {v1 .. v6}, Landroid/support/v4/widget/NestedScrollView;->a(IIIII)Z

    move-result v1

    if-eqz v1, :cond_159

    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/widget/NestedScrollView;->hasNestedScrollingParent()Z

    move-result v1

    if-nez v1, :cond_159

    .line 756
    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/v4/widget/NestedScrollView;->n:Landroid/view/VelocityTracker;

    invoke-virtual {v1}, Landroid/view/VelocityTracker;->clear()V

    .line 759
    :cond_159
    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/widget/NestedScrollView;->getScrollY()I

    move-result v1

    sub-int v9, v1, v16

    .line 760
    sub-int v11, v3, v9

    .line 761
    const/4 v8, 0x0

    const/4 v10, 0x0

    move-object/from16 v0, p0

    iget-object v12, v0, Landroid/support/v4/widget/NestedScrollView;->u:[I

    move-object/from16 v7, p0

    invoke-virtual/range {v7 .. v12}, Landroid/support/v4/widget/NestedScrollView;->dispatchNestedScroll(IIII[I)Z

    move-result v1

    if-eqz v1, :cond_1a8

    .line 762
    move-object/from16 v0, p0

    iget v1, v0, Landroid/support/v4/widget/NestedScrollView;->i:I

    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v4/widget/NestedScrollView;->u:[I

    const/4 v3, 0x1

    aget v2, v2, v3

    sub-int/2addr v1, v2

    move-object/from16 v0, p0

    iput v1, v0, Landroid/support/v4/widget/NestedScrollView;->i:I

    .line 763
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v4/widget/NestedScrollView;->u:[I

    const/4 v3, 0x1

    aget v2, v2, v3

    int-to-float v2, v2

    invoke-virtual {v14, v1, v2}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    .line 764
    move-object/from16 v0, p0

    iget v1, v0, Landroid/support/v4/widget/NestedScrollView;->w:I

    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v4/widget/NestedScrollView;->u:[I

    const/4 v3, 0x1

    aget v2, v2, v3

    add-int/2addr v1, v2

    move-object/from16 v0, p0

    iput v1, v0, Landroid/support/v4/widget/NestedScrollView;->w:I

    goto/16 :goto_1e

    .line 737
    :cond_19d
    move-object/from16 v0, p0

    iget v3, v0, Landroid/support/v4/widget/NestedScrollView;->q:I

    add-int/2addr v1, v3

    move v3, v1

    goto/16 :goto_116

    .line 747
    :cond_1a5
    const/4 v1, 0x0

    move v13, v1

    goto :goto_13e

    .line 765
    :cond_1a8
    if-eqz v13, :cond_1e

    .line 766
    invoke-direct/range {p0 .. p0}, Landroid/support/v4/widget/NestedScrollView;->i()V

    .line 767
    add-int v1, v16, v3

    .line 768
    if-gez v1, :cond_1fb

    .line 769
    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/v4/widget/NestedScrollView;->g:Landroid/support/v4/widget/al;

    int-to-float v2, v3

    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/widget/NestedScrollView;->getHeight()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v2, v3

    move-object/from16 v0, p1

    invoke-static {v0, v15}, Landroid/support/v4/view/bk;->c(Landroid/view/MotionEvent;I)F

    move-result v3

    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/widget/NestedScrollView;->getWidth()I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/support/v4/widget/al;->a(FF)Z

    .line 771
    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/v4/widget/NestedScrollView;->h:Landroid/support/v4/widget/al;

    invoke-virtual {v1}, Landroid/support/v4/widget/al;->a()Z

    move-result v1

    if-nez v1, :cond_1dc

    .line 772
    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/v4/widget/NestedScrollView;->h:Landroid/support/v4/widget/al;

    invoke-virtual {v1}, Landroid/support/v4/widget/al;->c()Z

    .line 782
    :cond_1dc
    :goto_1dc
    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/v4/widget/NestedScrollView;->g:Landroid/support/v4/widget/al;

    if-eqz v1, :cond_1e

    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/v4/widget/NestedScrollView;->g:Landroid/support/v4/widget/al;

    invoke-virtual {v1}, Landroid/support/v4/widget/al;->a()Z

    move-result v1

    if-eqz v1, :cond_1f6

    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/v4/widget/NestedScrollView;->h:Landroid/support/v4/widget/al;

    invoke-virtual {v1}, Landroid/support/v4/widget/al;->a()Z

    move-result v1

    if-nez v1, :cond_1e

    .line 784
    :cond_1f6
    invoke-static/range {p0 .. p0}, Landroid/support/v4/view/cx;->b(Landroid/view/View;)V

    goto/16 :goto_1e

    .line 774
    :cond_1fb
    if-le v1, v6, :cond_1dc

    .line 775
    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/v4/widget/NestedScrollView;->h:Landroid/support/v4/widget/al;

    int-to-float v2, v3

    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/widget/NestedScrollView;->getHeight()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v2, v3

    const/high16 v3, 0x3f800000    # 1.0f

    move-object/from16 v0, p1

    invoke-static {v0, v15}, Landroid/support/v4/view/bk;->c(Landroid/view/MotionEvent;I)F

    move-result v4

    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/widget/NestedScrollView;->getWidth()I

    move-result v5

    int-to-float v5, v5

    div-float/2addr v4, v5

    sub-float/2addr v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/support/v4/widget/al;->a(FF)Z

    .line 778
    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/v4/widget/NestedScrollView;->g:Landroid/support/v4/widget/al;

    invoke-virtual {v1}, Landroid/support/v4/widget/al;->a()Z

    move-result v1

    if-nez v1, :cond_1dc

    .line 779
    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/v4/widget/NestedScrollView;->g:Landroid/support/v4/widget/al;

    invoke-virtual {v1}, Landroid/support/v4/widget/al;->c()Z

    goto :goto_1dc

    .line 790
    :pswitch_22c
    move-object/from16 v0, p0

    iget-boolean v1, v0, Landroid/support/v4/widget/NestedScrollView;->m:Z

    if-eqz v1, :cond_1e

    .line 791
    move-object/from16 v0, p0

    iget-object v1, v0, Landroid/support/v4/widget/NestedScrollView;->n:Landroid/view/VelocityTracker;

    .line 792
    const/16 v2, 0x3e8

    move-object/from16 v0, p0

    iget v3, v0, Landroid/support/v4/widget/NestedScrollView;->s:I

    int-to-float v3, v3

    invoke-virtual {v1, v2, v3}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    .line 793
    move-object/from16 v0, p0

    iget v2, v0, Landroid/support/v4/widget/NestedScrollView;->t:I

    invoke-static {v1, v2}, Landroid/support/v4/view/cs;->b(Landroid/view/VelocityTracker;I)F

    move-result v1

    float-to-int v1, v1

    .line 796
    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v2

    move-object/from16 v0, p0

    iget v3, v0, Landroid/support/v4/widget/NestedScrollView;->r:I

    if-le v2, v3, :cond_259

    .line 797
    neg-int v1, v1

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Landroid/support/v4/widget/NestedScrollView;->g(I)V

    .line 800
    :cond_259
    const/4 v1, -0x1

    move-object/from16 v0, p0

    iput v1, v0, Landroid/support/v4/widget/NestedScrollView;->t:I

    .line 801
    invoke-direct/range {p0 .. p0}, Landroid/support/v4/widget/NestedScrollView;->h()V

    goto/16 :goto_1e

    .line 805
    :pswitch_263
    move-object/from16 v0, p0

    iget-boolean v1, v0, Landroid/support/v4/widget/NestedScrollView;->m:Z

    if-eqz v1, :cond_1e

    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/widget/NestedScrollView;->getChildCount()I

    move-result v1

    if-lez v1, :cond_1e

    .line 806
    const/4 v1, -0x1

    move-object/from16 v0, p0

    iput v1, v0, Landroid/support/v4/widget/NestedScrollView;->t:I

    .line 807
    invoke-direct/range {p0 .. p0}, Landroid/support/v4/widget/NestedScrollView;->h()V

    goto/16 :goto_1e

    .line 811
    :pswitch_279
    invoke-static/range {p1 .. p1}, Landroid/support/v4/view/bk;->b(Landroid/view/MotionEvent;)I

    move-result v1

    .line 812
    move-object/from16 v0, p1

    invoke-static {v0, v1}, Landroid/support/v4/view/bk;->d(Landroid/view/MotionEvent;I)F

    move-result v2

    float-to-int v2, v2

    move-object/from16 v0, p0

    iput v2, v0, Landroid/support/v4/widget/NestedScrollView;->i:I

    .line 813
    move-object/from16 v0, p1

    invoke-static {v0, v1}, Landroid/support/v4/view/bk;->b(Landroid/view/MotionEvent;I)I

    move-result v1

    move-object/from16 v0, p0

    iput v1, v0, Landroid/support/v4/widget/NestedScrollView;->t:I

    goto/16 :goto_1e

    .line 817
    :pswitch_294
    invoke-direct/range {p0 .. p1}, Landroid/support/v4/widget/NestedScrollView;->a(Landroid/view/MotionEvent;)V

    .line 818
    move-object/from16 v0, p0

    iget v1, v0, Landroid/support/v4/widget/NestedScrollView;->t:I

    move-object/from16 v0, p1

    invoke-static {v0, v1}, Landroid/support/v4/view/bk;->a(Landroid/view/MotionEvent;I)I

    move-result v1

    move-object/from16 v0, p1

    invoke-static {v0, v1}, Landroid/support/v4/view/bk;->d(Landroid/view/MotionEvent;I)F

    move-result v1

    float-to-int v1, v1

    move-object/from16 v0, p0

    iput v1, v0, Landroid/support/v4/widget/NestedScrollView;->i:I

    goto/16 :goto_1e

    :cond_2ae
    move v3, v1

    goto/16 :goto_116

    .line 687
    nop

    :pswitch_data_2b2
    .packed-switch 0x0
        :pswitch_30
        :pswitch_22c
        :pswitch_81
        :pswitch_263
        :pswitch_1e
        :pswitch_279
        :pswitch_294
    .end packed-switch
.end method

.method public final requestChildFocus(Landroid/view/View;Landroid/view/View;)V
    .registers 4

    .prologue
    .line 1474
    iget-boolean v0, p0, Landroid/support/v4/widget/NestedScrollView;->j:Z

    if-nez v0, :cond_b

    .line 1475
    invoke-direct {p0, p2}, Landroid/support/v4/widget/NestedScrollView;->b(Landroid/view/View;)V

    .line 1480
    :goto_7
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->requestChildFocus(Landroid/view/View;Landroid/view/View;)V

    .line 1481
    return-void

    .line 1478
    :cond_b
    iput-object p2, p0, Landroid/support/v4/widget/NestedScrollView;->l:Landroid/view/View;

    goto :goto_7
.end method

.method public final requestChildRectangleOnScreen(Landroid/view/View;Landroid/graphics/Rect;Z)Z
    .registers 8

    .prologue
    const/4 v1, 0x0

    .line 1523
    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getScrollX()I

    move-result v2

    sub-int/2addr v0, v2

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v2

    invoke-virtual {p1}, Landroid/view/View;->getScrollY()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {p2, v0, v2}, Landroid/graphics/Rect;->offset(II)V

    .line 8394
    invoke-direct {p0, p2}, Landroid/support/v4/widget/NestedScrollView;->a(Landroid/graphics/Rect;)I

    move-result v2

    .line 8395
    if-eqz v2, :cond_25

    const/4 v0, 0x1

    .line 8396
    :goto_1d
    if-eqz v0, :cond_24

    .line 8397
    if-eqz p3, :cond_27

    .line 8398
    invoke-virtual {p0, v1, v2}, Landroid/support/v4/widget/NestedScrollView;->scrollBy(II)V

    .line 1526
    :cond_24
    :goto_24
    return v0

    :cond_25
    move v0, v1

    .line 8395
    goto :goto_1d

    .line 8400
    :cond_27
    invoke-direct {p0, v1, v2}, Landroid/support/v4/widget/NestedScrollView;->b(II)V

    goto :goto_24
.end method

.method public final requestDisallowInterceptTouchEvent(Z)V
    .registers 2

    .prologue
    .line 554
    if-eqz p1, :cond_5

    .line 555
    invoke-direct {p0}, Landroid/support/v4/widget/NestedScrollView;->g()V

    .line 557
    :cond_5
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->requestDisallowInterceptTouchEvent(Z)V

    .line 558
    return-void
.end method

.method public final requestLayout()V
    .registers 2

    .prologue
    .line 1531
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/widget/NestedScrollView;->j:Z

    .line 1532
    invoke-super {p0}, Landroid/widget/FrameLayout;->requestLayout()V

    .line 1533
    return-void
.end method

.method public final scrollTo(II)V
    .registers 7

    .prologue
    .line 1655
    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getChildCount()I

    move-result v0

    if-lez v0, :cond_46

    .line 1656
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/support/v4/widget/NestedScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1657
    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getPaddingRight()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getPaddingLeft()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v2

    invoke-static {p1, v1, v2}, Landroid/support/v4/widget/NestedScrollView;->b(III)I

    move-result v1

    .line 1658
    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getHeight()I

    move-result v2

    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getPaddingBottom()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getPaddingTop()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    invoke-static {p2, v2, v0}, Landroid/support/v4/widget/NestedScrollView;->b(III)I

    move-result v0

    .line 1659
    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getScrollX()I

    move-result v2

    if-ne v1, v2, :cond_43

    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->getScrollY()I

    move-result v2

    if-eq v0, v2, :cond_46

    .line 1660
    :cond_43
    invoke-super {p0, v1, v0}, Landroid/widget/FrameLayout;->scrollTo(II)V

    .line 1663
    :cond_46
    return-void
.end method

.method public final setFillViewport(Z)V
    .registers 3

    .prologue
    .line 411
    iget-boolean v0, p0, Landroid/support/v4/widget/NestedScrollView;->o:Z

    if-eq p1, v0, :cond_9

    .line 412
    iput-boolean p1, p0, Landroid/support/v4/widget/NestedScrollView;->o:Z

    .line 413
    invoke-virtual {p0}, Landroid/support/v4/widget/NestedScrollView;->requestLayout()V

    .line 415
    :cond_9
    return-void
.end method

.method public final setNestedScrollingEnabled(Z)V
    .registers 3

    .prologue
    .line 188
    iget-object v0, p0, Landroid/support/v4/widget/NestedScrollView;->C:Landroid/support/v4/view/bu;

    invoke-virtual {v0, p1}, Landroid/support/v4/view/bu;->a(Z)V

    .line 189
    return-void
.end method

.method public final setSmoothScrollingEnabled(Z)V
    .registers 2

    .prologue
    .line 429
    iput-boolean p1, p0, Landroid/support/v4/widget/NestedScrollView;->p:Z

    .line 430
    return-void
.end method

.method public final shouldDelayChildPressedState()Z
    .registers 2

    .prologue
    .line 289
    const/4 v0, 0x1

    return v0
.end method

.method public final startNestedScroll(I)Z
    .registers 3

    .prologue
    .line 198
    iget-object v0, p0, Landroid/support/v4/widget/NestedScrollView;->C:Landroid/support/v4/view/bu;

    invoke-virtual {v0, p1}, Landroid/support/v4/view/bu;->a(I)Z

    move-result v0

    return v0
.end method

.method public final stopNestedScroll()V
    .registers 2

    .prologue
    .line 203
    iget-object v0, p0, Landroid/support/v4/widget/NestedScrollView;->C:Landroid/support/v4/view/bu;

    invoke-virtual {v0}, Landroid/support/v4/view/bu;->b()V

    .line 204
    return-void
.end method
