.class final Landroid/support/v4/widget/db;
.super Landroid/support/v4/view/a;
.source "SourceFile"


# instance fields
.field final synthetic a:Landroid/support/v4/widget/SlidingPaneLayout;

.field private final c:Landroid/graphics/Rect;


# direct methods
.method constructor <init>(Landroid/support/v4/widget/SlidingPaneLayout;)V
    .registers 3

    .prologue
    .line 1541
    iput-object p1, p0, Landroid/support/v4/widget/db;->a:Landroid/support/v4/widget/SlidingPaneLayout;

    invoke-direct {p0}, Landroid/support/v4/view/a;-><init>()V

    .line 1542
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Landroid/support/v4/widget/db;->c:Landroid/graphics/Rect;

    return-void
.end method

.method private a(Landroid/support/v4/view/a/q;Landroid/support/v4/view/a/q;)V
    .registers 6

    .prologue
    .line 1600
    iget-object v0, p0, Landroid/support/v4/widget/db;->c:Landroid/graphics/Rect;

    .line 1602
    invoke-virtual {p2, v0}, Landroid/support/v4/view/a/q;->a(Landroid/graphics/Rect;)V

    .line 1603
    invoke-virtual {p1, v0}, Landroid/support/v4/view/a/q;->b(Landroid/graphics/Rect;)V

    .line 1605
    invoke-virtual {p2, v0}, Landroid/support/v4/view/a/q;->c(Landroid/graphics/Rect;)V

    .line 1606
    invoke-virtual {p1, v0}, Landroid/support/v4/view/a/q;->d(Landroid/graphics/Rect;)V

    .line 1608
    invoke-virtual {p2}, Landroid/support/v4/view/a/q;->e()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/support/v4/view/a/q;->c(Z)V

    .line 1609
    invoke-virtual {p2}, Landroid/support/v4/view/a/q;->k()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/support/v4/view/a/q;->a(Ljava/lang/CharSequence;)V

    .line 1610
    invoke-virtual {p2}, Landroid/support/v4/view/a/q;->l()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/support/v4/view/a/q;->b(Ljava/lang/CharSequence;)V

    .line 1611
    invoke-virtual {p2}, Landroid/support/v4/view/a/q;->n()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/support/v4/view/a/q;->c(Ljava/lang/CharSequence;)V

    .line 1613
    invoke-virtual {p2}, Landroid/support/v4/view/a/q;->j()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/support/v4/view/a/q;->h(Z)V

    .line 1614
    invoke-virtual {p2}, Landroid/support/v4/view/a/q;->h()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/support/v4/view/a/q;->f(Z)V

    .line 1615
    invoke-virtual {p2}, Landroid/support/v4/view/a/q;->c()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/support/v4/view/a/q;->a(Z)V

    .line 1616
    invoke-virtual {p2}, Landroid/support/v4/view/a/q;->d()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/support/v4/view/a/q;->b(Z)V

    .line 1617
    invoke-virtual {p2}, Landroid/support/v4/view/a/q;->f()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/support/v4/view/a/q;->d(Z)V

    .line 1618
    invoke-virtual {p2}, Landroid/support/v4/view/a/q;->g()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/support/v4/view/a/q;->e(Z)V

    .line 1619
    invoke-virtual {p2}, Landroid/support/v4/view/a/q;->i()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/support/v4/view/a/q;->g(Z)V

    .line 1621
    invoke-virtual {p2}, Landroid/support/v4/view/a/q;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/support/v4/view/a/q;->a(I)V

    .line 4588
    sget-object v0, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v1, p2, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/view/a/w;->D(Ljava/lang/Object;)I

    move-result v0

    .line 5579
    sget-object v1, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v2, p1, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v1, v2, v0}, Landroid/support/v4/view/a/w;->g(Ljava/lang/Object;I)V

    .line 1624
    return-void
.end method

.method private b(Landroid/view/View;)Z
    .registers 3

    .prologue
    .line 1590
    iget-object v0, p0, Landroid/support/v4/widget/db;->a:Landroid/support/v4/widget/SlidingPaneLayout;

    invoke-virtual {v0, p1}, Landroid/support/v4/widget/SlidingPaneLayout;->b(Landroid/view/View;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(Landroid/view/View;Landroid/support/v4/view/a/q;)V
    .registers 7

    .prologue
    .line 1546
    invoke-static {p2}, Landroid/support/v4/view/a/q;->a(Landroid/support/v4/view/a/q;)Landroid/support/v4/view/a/q;

    move-result-object v0

    .line 1547
    invoke-super {p0, p1, v0}, Landroid/support/v4/view/a;->a(Landroid/view/View;Landroid/support/v4/view/a/q;)V

    .line 2600
    iget-object v1, p0, Landroid/support/v4/widget/db;->c:Landroid/graphics/Rect;

    .line 2602
    invoke-virtual {v0, v1}, Landroid/support/v4/view/a/q;->a(Landroid/graphics/Rect;)V

    .line 2603
    invoke-virtual {p2, v1}, Landroid/support/v4/view/a/q;->b(Landroid/graphics/Rect;)V

    .line 2605
    invoke-virtual {v0, v1}, Landroid/support/v4/view/a/q;->c(Landroid/graphics/Rect;)V

    .line 2606
    invoke-virtual {p2, v1}, Landroid/support/v4/view/a/q;->d(Landroid/graphics/Rect;)V

    .line 2608
    invoke-virtual {v0}, Landroid/support/v4/view/a/q;->e()Z

    move-result v1

    invoke-virtual {p2, v1}, Landroid/support/v4/view/a/q;->c(Z)V

    .line 2609
    invoke-virtual {v0}, Landroid/support/v4/view/a/q;->k()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p2, v1}, Landroid/support/v4/view/a/q;->a(Ljava/lang/CharSequence;)V

    .line 2610
    invoke-virtual {v0}, Landroid/support/v4/view/a/q;->l()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p2, v1}, Landroid/support/v4/view/a/q;->b(Ljava/lang/CharSequence;)V

    .line 2611
    invoke-virtual {v0}, Landroid/support/v4/view/a/q;->n()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p2, v1}, Landroid/support/v4/view/a/q;->c(Ljava/lang/CharSequence;)V

    .line 2613
    invoke-virtual {v0}, Landroid/support/v4/view/a/q;->j()Z

    move-result v1

    invoke-virtual {p2, v1}, Landroid/support/v4/view/a/q;->h(Z)V

    .line 2614
    invoke-virtual {v0}, Landroid/support/v4/view/a/q;->h()Z

    move-result v1

    invoke-virtual {p2, v1}, Landroid/support/v4/view/a/q;->f(Z)V

    .line 2615
    invoke-virtual {v0}, Landroid/support/v4/view/a/q;->c()Z

    move-result v1

    invoke-virtual {p2, v1}, Landroid/support/v4/view/a/q;->a(Z)V

    .line 2616
    invoke-virtual {v0}, Landroid/support/v4/view/a/q;->d()Z

    move-result v1

    invoke-virtual {p2, v1}, Landroid/support/v4/view/a/q;->b(Z)V

    .line 2617
    invoke-virtual {v0}, Landroid/support/v4/view/a/q;->f()Z

    move-result v1

    invoke-virtual {p2, v1}, Landroid/support/v4/view/a/q;->d(Z)V

    .line 2618
    invoke-virtual {v0}, Landroid/support/v4/view/a/q;->g()Z

    move-result v1

    invoke-virtual {p2, v1}, Landroid/support/v4/view/a/q;->e(Z)V

    .line 2619
    invoke-virtual {v0}, Landroid/support/v4/view/a/q;->i()Z

    move-result v1

    invoke-virtual {p2, v1}, Landroid/support/v4/view/a/q;->g(Z)V

    .line 2621
    invoke-virtual {v0}, Landroid/support/v4/view/a/q;->b()I

    move-result v1

    invoke-virtual {p2, v1}, Landroid/support/v4/view/a/q;->a(I)V

    .line 3588
    sget-object v1, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v2, v0, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v1, v2}, Landroid/support/v4/view/a/w;->D(Ljava/lang/Object;)I

    move-result v1

    .line 4579
    sget-object v2, Landroid/support/v4/view/a/q;->a:Landroid/support/v4/view/a/w;

    iget-object v3, p2, Landroid/support/v4/view/a/q;->b:Ljava/lang/Object;

    invoke-interface {v2, v3, v1}, Landroid/support/v4/view/a/w;->g(Ljava/lang/Object;I)V

    .line 1549
    invoke-virtual {v0}, Landroid/support/v4/view/a/q;->o()V

    .line 1551
    const-class v0, Landroid/support/v4/widget/SlidingPaneLayout;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/support/v4/view/a/q;->b(Ljava/lang/CharSequence;)V

    .line 1552
    invoke-virtual {p2, p1}, Landroid/support/v4/view/a/q;->b(Landroid/view/View;)V

    .line 1554
    invoke-static {p1}, Landroid/support/v4/view/cx;->g(Landroid/view/View;)Landroid/view/ViewParent;

    move-result-object v0

    .line 1555
    instance-of v1, v0, Landroid/view/View;

    if-eqz v1, :cond_94

    .line 1556
    check-cast v0, Landroid/view/View;

    invoke-virtual {p2, v0}, Landroid/support/v4/view/a/q;->d(Landroid/view/View;)V

    .line 1561
    :cond_94
    iget-object v0, p0, Landroid/support/v4/widget/db;->a:Landroid/support/v4/widget/SlidingPaneLayout;

    invoke-virtual {v0}, Landroid/support/v4/widget/SlidingPaneLayout;->getChildCount()I

    move-result v1

    .line 1562
    const/4 v0, 0x0

    :goto_9b
    if-ge v0, v1, :cond_b9

    .line 1563
    iget-object v2, p0, Landroid/support/v4/widget/db;->a:Landroid/support/v4/widget/SlidingPaneLayout;

    invoke-virtual {v2, v0}, Landroid/support/v4/widget/SlidingPaneLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 1564
    invoke-direct {p0, v2}, Landroid/support/v4/widget/db;->b(Landroid/view/View;)Z

    move-result v3

    if-nez v3, :cond_b6

    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v3

    if-nez v3, :cond_b6

    .line 1566
    const/4 v3, 0x1

    invoke-static {v2, v3}, Landroid/support/v4/view/cx;->c(Landroid/view/View;I)V

    .line 1568
    invoke-virtual {p2, v2}, Landroid/support/v4/view/a/q;->c(Landroid/view/View;)V

    .line 1562
    :cond_b6
    add-int/lit8 v0, v0, 0x1

    goto :goto_9b

    .line 1571
    :cond_b9
    return-void
.end method

.method public final a(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
    .registers 4

    .prologue
    .line 1575
    invoke-super {p0, p1, p2}, Landroid/support/v4/view/a;->a(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V

    .line 1577
    const-class v0, Landroid/support/v4/widget/SlidingPaneLayout;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    .line 1578
    return-void
.end method

.method public final a(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z
    .registers 5

    .prologue
    .line 1583
    invoke-direct {p0, p2}, Landroid/support/v4/widget/db;->b(Landroid/view/View;)Z

    move-result v0

    if-nez v0, :cond_b

    .line 1584
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/view/a;->a(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v0

    .line 1586
    :goto_a
    return v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method
