.class final Landroid/support/v4/widget/du;
.super Landroid/view/animation/Animation;
.source "SourceFile"


# instance fields
.field final synthetic a:Landroid/support/v4/widget/dn;


# direct methods
.method constructor <init>(Landroid/support/v4/widget/dn;)V
    .registers 2

    .prologue
    .line 1069
    iput-object p1, p0, Landroid/support/v4/widget/du;->a:Landroid/support/v4/widget/dn;

    invoke-direct {p0}, Landroid/view/animation/Animation;-><init>()V

    return-void
.end method


# virtual methods
.method public final applyTransformation(FLandroid/view/animation/Transformation;)V
    .registers 6

    .prologue
    .line 1074
    iget-object v0, p0, Landroid/support/v4/widget/du;->a:Landroid/support/v4/widget/dn;

    invoke-static {v0}, Landroid/support/v4/widget/dn;->j(Landroid/support/v4/widget/dn;)Z

    move-result v0

    if-nez v0, :cond_44

    .line 1075
    iget-object v0, p0, Landroid/support/v4/widget/du;->a:Landroid/support/v4/widget/dn;

    invoke-static {v0}, Landroid/support/v4/widget/dn;->k(Landroid/support/v4/widget/dn;)F

    move-result v0

    iget-object v1, p0, Landroid/support/v4/widget/du;->a:Landroid/support/v4/widget/dn;

    iget v1, v1, Landroid/support/v4/widget/dn;->d:I

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    int-to-float v1, v1

    sub-float/2addr v0, v1

    float-to-int v0, v0

    .line 1079
    :goto_19
    iget-object v1, p0, Landroid/support/v4/widget/du;->a:Landroid/support/v4/widget/dn;

    iget v1, v1, Landroid/support/v4/widget/dn;->c:I

    iget-object v2, p0, Landroid/support/v4/widget/du;->a:Landroid/support/v4/widget/dn;

    iget v2, v2, Landroid/support/v4/widget/dn;->c:I

    sub-int/2addr v0, v2

    int-to-float v0, v0

    mul-float/2addr v0, p1

    float-to-int v0, v0

    add-int/2addr v0, v1

    .line 1080
    iget-object v1, p0, Landroid/support/v4/widget/du;->a:Landroid/support/v4/widget/dn;

    invoke-static {v1}, Landroid/support/v4/widget/dn;->e(Landroid/support/v4/widget/dn;)Landroid/support/v4/widget/e;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/widget/e;->getTop()I

    move-result v1

    sub-int/2addr v0, v1

    .line 1081
    iget-object v1, p0, Landroid/support/v4/widget/du;->a:Landroid/support/v4/widget/dn;

    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Landroid/support/v4/widget/dn;->a(Landroid/support/v4/widget/dn;IZ)V

    .line 1082
    iget-object v0, p0, Landroid/support/v4/widget/du;->a:Landroid/support/v4/widget/dn;

    invoke-static {v0}, Landroid/support/v4/widget/dn;->b(Landroid/support/v4/widget/dn;)Landroid/support/v4/widget/bb;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    sub-float/2addr v1, p1

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/bb;->a(F)V

    .line 1083
    return-void

    .line 1077
    :cond_44
    iget-object v0, p0, Landroid/support/v4/widget/du;->a:Landroid/support/v4/widget/dn;

    invoke-static {v0}, Landroid/support/v4/widget/dn;->k(Landroid/support/v4/widget/dn;)F

    move-result v0

    float-to-int v0, v0

    goto :goto_19
.end method
