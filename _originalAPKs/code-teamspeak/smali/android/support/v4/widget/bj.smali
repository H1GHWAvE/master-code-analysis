.class public final Landroid/support/v4/widget/bj;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final a:Landroid/support/v4/widget/bm;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 58
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 59
    const/16 v1, 0x13

    if-lt v0, v1, :cond_e

    .line 60
    new-instance v0, Landroid/support/v4/widget/bl;

    invoke-direct {v0}, Landroid/support/v4/widget/bl;-><init>()V

    sput-object v0, Landroid/support/v4/widget/bj;->a:Landroid/support/v4/widget/bm;

    .line 64
    :goto_d
    return-void

    .line 62
    :cond_e
    new-instance v0, Landroid/support/v4/widget/bk;

    invoke-direct {v0}, Landroid/support/v4/widget/bk;-><init>()V

    sput-object v0, Landroid/support/v4/widget/bj;->a:Landroid/support/v4/widget/bm;

    goto :goto_d
.end method

.method private constructor <init>()V
    .registers 1

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    return-void
.end method

.method private static a(Ljava/lang/Object;)Landroid/view/View$OnTouchListener;
    .registers 2

    .prologue
    .line 90
    sget-object v0, Landroid/support/v4/widget/bj;->a:Landroid/support/v4/widget/bm;

    invoke-interface {v0, p0}, Landroid/support/v4/widget/bm;->a(Ljava/lang/Object;)Landroid/view/View$OnTouchListener;

    move-result-object v0

    return-object v0
.end method
