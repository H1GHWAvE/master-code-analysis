.class public final Landroid/support/v4/widget/bo;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final a:Landroid/support/v4/widget/bu;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 135
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 136
    const/16 v1, 0x17

    if-lt v0, v1, :cond_e

    .line 137
    new-instance v0, Landroid/support/v4/widget/bq;

    invoke-direct {v0}, Landroid/support/v4/widget/bq;-><init>()V

    sput-object v0, Landroid/support/v4/widget/bo;->a:Landroid/support/v4/widget/bu;

    .line 147
    :goto_d
    return-void

    .line 138
    :cond_e
    const/16 v1, 0x15

    if-lt v0, v1, :cond_1a

    .line 139
    new-instance v0, Landroid/support/v4/widget/bp;

    invoke-direct {v0}, Landroid/support/v4/widget/bp;-><init>()V

    sput-object v0, Landroid/support/v4/widget/bo;->a:Landroid/support/v4/widget/bu;

    goto :goto_d

    .line 140
    :cond_1a
    const/16 v1, 0x13

    if-lt v0, v1, :cond_26

    .line 141
    new-instance v0, Landroid/support/v4/widget/bt;

    invoke-direct {v0}, Landroid/support/v4/widget/bt;-><init>()V

    sput-object v0, Landroid/support/v4/widget/bo;->a:Landroid/support/v4/widget/bu;

    goto :goto_d

    .line 142
    :cond_26
    const/16 v1, 0x9

    if-lt v0, v1, :cond_32

    .line 143
    new-instance v0, Landroid/support/v4/widget/bs;

    invoke-direct {v0}, Landroid/support/v4/widget/bs;-><init>()V

    sput-object v0, Landroid/support/v4/widget/bo;->a:Landroid/support/v4/widget/bu;

    goto :goto_d

    .line 145
    :cond_32
    new-instance v0, Landroid/support/v4/widget/br;

    invoke-direct {v0}, Landroid/support/v4/widget/br;-><init>()V

    sput-object v0, Landroid/support/v4/widget/bo;->a:Landroid/support/v4/widget/bu;

    goto :goto_d
.end method

.method private constructor <init>()V
    .registers 1

    .prologue
    .line 149
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 151
    return-void
.end method

.method public static a(Landroid/widget/PopupWindow;Landroid/view/View;III)V
    .registers 11

    .prologue
    .line 171
    sget-object v0, Landroid/support/v4/widget/bo;->a:Landroid/support/v4/widget/bu;

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-interface/range {v0 .. v5}, Landroid/support/v4/widget/bu;->a(Landroid/widget/PopupWindow;Landroid/view/View;III)V

    .line 172
    return-void
.end method

.method public static a(Landroid/widget/PopupWindow;Z)V
    .registers 3

    .prologue
    .line 181
    sget-object v0, Landroid/support/v4/widget/bo;->a:Landroid/support/v4/widget/bu;

    invoke-interface {v0, p0, p1}, Landroid/support/v4/widget/bu;->a(Landroid/widget/PopupWindow;Z)V

    .line 182
    return-void
.end method

.method public static a(Landroid/widget/PopupWindow;)Z
    .registers 2

    .prologue
    .line 191
    sget-object v0, Landroid/support/v4/widget/bo;->a:Landroid/support/v4/widget/bu;

    invoke-interface {v0, p0}, Landroid/support/v4/widget/bu;->a(Landroid/widget/PopupWindow;)Z

    move-result v0

    return v0
.end method

.method public static b(Landroid/widget/PopupWindow;)V
    .registers 2

    .prologue
    .line 204
    sget-object v0, Landroid/support/v4/widget/bo;->a:Landroid/support/v4/widget/bu;

    invoke-interface {v0, p0}, Landroid/support/v4/widget/bu;->b(Landroid/widget/PopupWindow;)V

    .line 205
    return-void
.end method

.method private static c(Landroid/widget/PopupWindow;)I
    .registers 2

    .prologue
    .line 213
    sget-object v0, Landroid/support/v4/widget/bo;->a:Landroid/support/v4/widget/bu;

    invoke-interface {v0, p0}, Landroid/support/v4/widget/bu;->c(Landroid/widget/PopupWindow;)I

    move-result v0

    return v0
.end method
