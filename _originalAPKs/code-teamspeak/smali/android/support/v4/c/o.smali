.class public final Landroid/support/v4/c/o;
.super Landroid/support/v4/c/a;
.source "SourceFile"


# instance fields
.field final h:Landroid/support/v4/c/ab;

.field i:Landroid/net/Uri;

.field j:[Ljava/lang/String;

.field k:Ljava/lang/String;

.field l:[Ljava/lang/String;

.field m:Ljava/lang/String;

.field n:Landroid/database/Cursor;

.field o:Landroid/support/v4/i/c;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .registers 3

    .prologue
    .line 119
    invoke-direct {p0, p1}, Landroid/support/v4/c/a;-><init>(Landroid/content/Context;)V

    .line 120
    new-instance v0, Landroid/support/v4/c/ab;

    invoke-direct {v0, p0}, Landroid/support/v4/c/ab;-><init>(Landroid/support/v4/c/aa;)V

    iput-object v0, p0, Landroid/support/v4/c/o;->h:Landroid/support/v4/c/ab;

    .line 121
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V
    .registers 8

    .prologue
    .line 131
    invoke-direct {p0, p1}, Landroid/support/v4/c/a;-><init>(Landroid/content/Context;)V

    .line 132
    new-instance v0, Landroid/support/v4/c/ab;

    invoke-direct {v0, p0}, Landroid/support/v4/c/ab;-><init>(Landroid/support/v4/c/aa;)V

    iput-object v0, p0, Landroid/support/v4/c/o;->h:Landroid/support/v4/c/ab;

    .line 133
    iput-object p2, p0, Landroid/support/v4/c/o;->i:Landroid/net/Uri;

    .line 134
    iput-object p3, p0, Landroid/support/v4/c/o;->j:[Ljava/lang/String;

    .line 135
    iput-object p4, p0, Landroid/support/v4/c/o;->k:Ljava/lang/String;

    .line 136
    iput-object p5, p0, Landroid/support/v4/c/o;->l:[Ljava/lang/String;

    .line 137
    iput-object p6, p0, Landroid/support/v4/c/o;->m:Ljava/lang/String;

    .line 138
    return-void
.end method

.method private a(Landroid/database/Cursor;)V
    .registers 4

    .prologue
    .line 94
    .line 4244
    iget-boolean v0, p0, Landroid/support/v4/c/aa;->v:Z

    .line 94
    if-eqz v0, :cond_a

    .line 96
    if-eqz p1, :cond_9

    .line 97
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    .line 111
    :cond_9
    :goto_9
    return-void

    .line 101
    :cond_a
    iget-object v0, p0, Landroid/support/v4/c/o;->n:Landroid/database/Cursor;

    .line 102
    iput-object p1, p0, Landroid/support/v4/c/o;->n:Landroid/database/Cursor;

    .line 5226
    iget-boolean v1, p0, Landroid/support/v4/c/aa;->t:Z

    .line 104
    if-eqz v1, :cond_15

    .line 105
    invoke-super {p0, p1}, Landroid/support/v4/c/a;->b(Ljava/lang/Object;)V

    .line 108
    :cond_15
    if-eqz v0, :cond_9

    if-eq v0, p1, :cond_9

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_9

    .line 109
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_9
.end method

.method private a(Landroid/net/Uri;)V
    .registers 2

    .prologue
    .line 191
    iput-object p1, p0, Landroid/support/v4/c/o;->i:Landroid/net/Uri;

    .line 192
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .registers 2

    .prologue
    .line 207
    iput-object p1, p0, Landroid/support/v4/c/o;->k:Ljava/lang/String;

    .line 208
    return-void
.end method

.method private a([Ljava/lang/String;)V
    .registers 2

    .prologue
    .line 199
    iput-object p1, p0, Landroid/support/v4/c/o;->j:[Ljava/lang/String;

    .line 200
    return-void
.end method

.method private static b(Landroid/database/Cursor;)V
    .registers 2

    .prologue
    .line 168
    if-eqz p0, :cond_b

    invoke-interface {p0}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_b

    .line 169
    invoke-interface {p0}, Landroid/database/Cursor;->close()V

    .line 171
    :cond_b
    return-void
.end method

.method private b(Ljava/lang/String;)V
    .registers 2

    .prologue
    .line 223
    iput-object p1, p0, Landroid/support/v4/c/o;->m:Ljava/lang/String;

    .line 224
    return-void
.end method

.method private b([Ljava/lang/String;)V
    .registers 2

    .prologue
    .line 215
    iput-object p1, p0, Landroid/support/v4/c/o;->l:[Ljava/lang/String;

    .line 216
    return-void
.end method

.method private o()Landroid/database/Cursor;
    .registers 8

    .prologue
    .line 52
    monitor-enter p0

    .line 1321
    :try_start_1
    iget-object v0, p0, Landroid/support/v4/c/a;->d:Landroid/support/v4/c/b;

    if-eqz v0, :cond_11

    const/4 v0, 0x1

    .line 53
    :goto_6
    if-eqz v0, :cond_13

    .line 54
    new-instance v0, Landroid/support/v4/i/h;

    invoke-direct {v0}, Landroid/support/v4/i/h;-><init>()V

    throw v0

    .line 57
    :catchall_e
    move-exception v0

    monitor-exit p0
    :try_end_10
    .catchall {:try_start_1 .. :try_end_10} :catchall_e

    throw v0

    .line 1321
    :cond_11
    const/4 v0, 0x0

    goto :goto_6

    .line 56
    :cond_13
    :try_start_13
    new-instance v0, Landroid/support/v4/i/c;

    invoke-direct {v0}, Landroid/support/v4/i/c;-><init>()V

    iput-object v0, p0, Landroid/support/v4/c/o;->o:Landroid/support/v4/i/c;

    .line 57
    monitor-exit p0
    :try_end_1b
    .catchall {:try_start_13 .. :try_end_1b} :catchall_e

    .line 2146
    :try_start_1b
    iget-object v0, p0, Landroid/support/v4/c/aa;->s:Landroid/content/Context;

    .line 59
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v4/c/o;->i:Landroid/net/Uri;

    iget-object v2, p0, Landroid/support/v4/c/o;->j:[Ljava/lang/String;

    iget-object v3, p0, Landroid/support/v4/c/o;->k:Ljava/lang/String;

    iget-object v4, p0, Landroid/support/v4/c/o;->l:[Ljava/lang/String;

    iget-object v5, p0, Landroid/support/v4/c/o;->m:Ljava/lang/String;

    iget-object v6, p0, Landroid/support/v4/c/o;->o:Landroid/support/v4/i/c;

    invoke-static/range {v0 .. v6}, Landroid/support/v4/c/c;->a(Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/support/v4/i/c;)Landroid/database/Cursor;
    :try_end_30
    .catchall {:try_start_1b .. :try_end_30} :catchall_46

    move-result-object v1

    .line 62
    if-eqz v1, :cond_3b

    .line 65
    :try_start_33
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    .line 66
    iget-object v0, p0, Landroid/support/v4/c/o;->h:Landroid/support/v4/c/ab;

    invoke-interface {v1, v0}, Landroid/database/Cursor;->registerContentObserver(Landroid/database/ContentObserver;)V
    :try_end_3b
    .catch Ljava/lang/RuntimeException; {:try_start_33 .. :try_end_3b} :catch_41
    .catchall {:try_start_33 .. :try_end_3b} :catchall_46

    .line 74
    :cond_3b
    monitor-enter p0

    .line 75
    const/4 v0, 0x0

    :try_start_3d
    iput-object v0, p0, Landroid/support/v4/c/o;->o:Landroid/support/v4/i/c;

    .line 76
    monitor-exit p0
    :try_end_40
    .catchall {:try_start_3d .. :try_end_40} :catchall_4d

    return-object v1

    .line 67
    :catch_41
    move-exception v0

    .line 68
    :try_start_42
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 69
    throw v0
    :try_end_46
    .catchall {:try_start_42 .. :try_end_46} :catchall_46

    .line 74
    :catchall_46
    move-exception v0

    monitor-enter p0

    .line 75
    const/4 v1, 0x0

    :try_start_49
    iput-object v1, p0, Landroid/support/v4/c/o;->o:Landroid/support/v4/i/c;

    .line 76
    monitor-exit p0
    :try_end_4c
    .catchall {:try_start_49 .. :try_end_4c} :catchall_50

    throw v0

    :catchall_4d
    move-exception v0

    :try_start_4e
    monitor-exit p0
    :try_end_4f
    .catchall {:try_start_4e .. :try_end_4f} :catchall_4d

    throw v0

    :catchall_50
    move-exception v0

    :try_start_51
    monitor-exit p0
    :try_end_52
    .catchall {:try_start_51 .. :try_end_52} :catchall_50

    throw v0
.end method

.method private p()Landroid/net/Uri;
    .registers 2

    .prologue
    .line 187
    iget-object v0, p0, Landroid/support/v4/c/o;->i:Landroid/net/Uri;

    return-object v0
.end method

.method private q()[Ljava/lang/String;
    .registers 2

    .prologue
    .line 195
    iget-object v0, p0, Landroid/support/v4/c/o;->j:[Ljava/lang/String;

    return-object v0
.end method

.method private r()Ljava/lang/String;
    .registers 2

    .prologue
    .line 203
    iget-object v0, p0, Landroid/support/v4/c/o;->k:Ljava/lang/String;

    return-object v0
.end method

.method private s()[Ljava/lang/String;
    .registers 2

    .prologue
    .line 211
    iget-object v0, p0, Landroid/support/v4/c/o;->l:[Ljava/lang/String;

    return-object v0
.end method

.method private t()Ljava/lang/String;
    .registers 2

    .prologue
    .line 219
    iget-object v0, p0, Landroid/support/v4/c/o;->m:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;)V
    .registers 3

    .prologue
    .line 37
    check-cast p1, Landroid/database/Cursor;

    .line 6168
    if-eqz p1, :cond_d

    invoke-interface {p1}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_d

    .line 6169
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    .line 37
    :cond_d
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 6

    .prologue
    .line 228
    invoke-super {p0, p1, p2, p3, p4}, Landroid/support/v4/c/a;->a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 229
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mUri="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Landroid/support/v4/c/o;->i:Landroid/net/Uri;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 230
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mProjection="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 231
    iget-object v0, p0, Landroid/support/v4/c/o;->j:[Ljava/lang/String;

    invoke-static {v0}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 232
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mSelection="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Landroid/support/v4/c/o;->k:Ljava/lang/String;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 233
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mSelectionArgs="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 234
    iget-object v0, p0, Landroid/support/v4/c/o;->l:[Ljava/lang/String;

    invoke-static {v0}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 235
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mSortOrder="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Landroid/support/v4/c/o;->m:Ljava/lang/String;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 236
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mCursor="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Landroid/support/v4/c/o;->n:Landroid/database/Cursor;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 237
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mContentChanged="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Landroid/support/v4/c/o;->w:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 238
    return-void
.end method

.method public final synthetic b(Ljava/lang/Object;)V
    .registers 2

    .prologue
    .line 37
    check-cast p1, Landroid/database/Cursor;

    invoke-direct {p0, p1}, Landroid/support/v4/c/o;->a(Landroid/database/Cursor;)V

    return-void
.end method

.method public final synthetic d()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 37
    invoke-direct {p0}, Landroid/support/v4/c/o;->o()Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public final e()V
    .registers 4

    .prologue
    .line 82
    invoke-super {p0}, Landroid/support/v4/c/a;->e()V

    .line 84
    monitor-enter p0

    .line 85
    :try_start_4
    iget-object v0, p0, Landroid/support/v4/c/o;->o:Landroid/support/v4/i/c;

    if-eqz v0, :cond_10

    .line 86
    iget-object v1, p0, Landroid/support/v4/c/o;->o:Landroid/support/v4/i/c;

    .line 3067
    monitor-enter v1
    :try_end_b
    .catchall {:try_start_4 .. :try_end_b} :catchall_2e

    .line 3068
    :try_start_b
    iget-boolean v0, v1, Landroid/support/v4/i/c;->a:Z

    if-eqz v0, :cond_12

    .line 3069
    monitor-exit v1
    :try_end_10
    .catchall {:try_start_b .. :try_end_10} :catchall_31

    .line 88
    :cond_10
    :goto_10
    :try_start_10
    monitor-exit p0
    :try_end_11
    .catchall {:try_start_10 .. :try_end_11} :catchall_2e

    return-void

    .line 3071
    :cond_12
    const/4 v0, 0x1

    :try_start_13
    iput-boolean v0, v1, Landroid/support/v4/i/c;->a:Z

    .line 3072
    const/4 v0, 0x1

    iput-boolean v0, v1, Landroid/support/v4/i/c;->c:Z

    .line 3074
    iget-object v0, v1, Landroid/support/v4/i/c;->b:Ljava/lang/Object;

    .line 3075
    monitor-exit v1
    :try_end_1b
    .catchall {:try_start_13 .. :try_end_1b} :catchall_31

    .line 3081
    if-eqz v0, :cond_22

    .line 4025
    :try_start_1d
    check-cast v0, Landroid/os/CancellationSignal;

    invoke-virtual {v0}, Landroid/os/CancellationSignal;->cancel()V
    :try_end_22
    .catchall {:try_start_1d .. :try_end_22} :catchall_34

    .line 3085
    :cond_22
    :try_start_22
    monitor-enter v1
    :try_end_23
    .catchall {:try_start_22 .. :try_end_23} :catchall_2e

    .line 3086
    const/4 v0, 0x0

    :try_start_24
    iput-boolean v0, v1, Landroid/support/v4/i/c;->c:Z

    .line 3087
    invoke-virtual {v1}, Ljava/lang/Object;->notifyAll()V

    .line 3088
    monitor-exit v1

    goto :goto_10

    :catchall_2b
    move-exception v0

    monitor-exit v1
    :try_end_2d
    .catchall {:try_start_24 .. :try_end_2d} :catchall_2b

    :try_start_2d
    throw v0

    .line 88
    :catchall_2e
    move-exception v0

    monitor-exit p0
    :try_end_30
    .catchall {:try_start_2d .. :try_end_30} :catchall_2e

    throw v0

    .line 3075
    :catchall_31
    move-exception v0

    :try_start_32
    monitor-exit v1
    :try_end_33
    .catchall {:try_start_32 .. :try_end_33} :catchall_31

    :try_start_33
    throw v0

    .line 3085
    :catchall_34
    move-exception v0

    monitor-enter v1
    :try_end_36
    .catchall {:try_start_33 .. :try_end_36} :catchall_2e

    .line 3086
    const/4 v2, 0x0

    :try_start_37
    iput-boolean v2, v1, Landroid/support/v4/i/c;->c:Z

    .line 3087
    invoke-virtual {v1}, Ljava/lang/Object;->notifyAll()V

    .line 3088
    monitor-exit v1
    :try_end_3d
    .catchall {:try_start_37 .. :try_end_3d} :catchall_3e

    :try_start_3d
    throw v0
    :try_end_3e
    .catchall {:try_start_3d .. :try_end_3e} :catchall_2e

    :catchall_3e
    move-exception v0

    :try_start_3f
    monitor-exit v1
    :try_end_40
    .catchall {:try_start_3f .. :try_end_40} :catchall_3e

    :try_start_40
    throw v0
    :try_end_41
    .catchall {:try_start_40 .. :try_end_41} :catchall_2e
.end method

.method protected final f()V
    .registers 3

    .prologue
    .line 149
    iget-object v0, p0, Landroid/support/v4/c/o;->n:Landroid/database/Cursor;

    if-eqz v0, :cond_9

    .line 150
    iget-object v0, p0, Landroid/support/v4/c/o;->n:Landroid/database/Cursor;

    invoke-direct {p0, v0}, Landroid/support/v4/c/o;->a(Landroid/database/Cursor;)V

    .line 5443
    :cond_9
    iget-boolean v0, p0, Landroid/support/v4/c/aa;->w:Z

    .line 5444
    const/4 v1, 0x0

    iput-boolean v1, p0, Landroid/support/v4/c/aa;->w:Z

    .line 5445
    iget-boolean v1, p0, Landroid/support/v4/c/aa;->x:Z

    or-int/2addr v1, v0

    iput-boolean v1, p0, Landroid/support/v4/c/aa;->x:Z

    .line 152
    if-nez v0, :cond_19

    iget-object v0, p0, Landroid/support/v4/c/o;->n:Landroid/database/Cursor;

    if-nez v0, :cond_1c

    .line 153
    :cond_19
    invoke-virtual {p0}, Landroid/support/v4/c/o;->k()V

    .line 155
    :cond_1c
    return-void
.end method

.method protected final g()V
    .registers 1

    .prologue
    .line 163
    invoke-virtual {p0}, Landroid/support/v4/c/o;->j()Z

    .line 164
    return-void
.end method

.method protected final h()V
    .registers 2

    .prologue
    .line 175
    invoke-super {p0}, Landroid/support/v4/c/a;->h()V

    .line 6163
    invoke-virtual {p0}, Landroid/support/v4/c/o;->j()Z

    .line 180
    iget-object v0, p0, Landroid/support/v4/c/o;->n:Landroid/database/Cursor;

    if-eqz v0, :cond_17

    iget-object v0, p0, Landroid/support/v4/c/o;->n:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_17

    .line 181
    iget-object v0, p0, Landroid/support/v4/c/o;->n:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 183
    :cond_17
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v4/c/o;->n:Landroid/database/Cursor;

    .line 184
    return-void
.end method
