.class public final Landroid/support/v4/app/co;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"


# static fields
.field static final a:I = 0xff0001

.field static final b:I = 0xff0002

.field static final c:I = 0xff0003


# instance fields
.field private final at:Landroid/widget/AdapterView$OnItemClickListener;

.field d:Landroid/widget/ListAdapter;

.field e:Landroid/widget/ListView;

.field f:Landroid/view/View;

.field g:Landroid/widget/TextView;

.field h:Landroid/view/View;

.field i:Landroid/view/View;

.field j:Ljava/lang/CharSequence;

.field k:Z

.field private final l:Landroid/os/Handler;

.field private final m:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 71
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 47
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Landroid/support/v4/app/co;->l:Landroid/os/Handler;

    .line 49
    new-instance v0, Landroid/support/v4/app/cp;

    invoke-direct {v0, p0}, Landroid/support/v4/app/cp;-><init>(Landroid/support/v4/app/co;)V

    iput-object v0, p0, Landroid/support/v4/app/co;->m:Ljava/lang/Runnable;

    .line 55
    new-instance v0, Landroid/support/v4/app/cq;

    invoke-direct {v0, p0}, Landroid/support/v4/app/cq;-><init>(Landroid/support/v4/app/co;)V

    iput-object v0, p0, Landroid/support/v4/app/co;->at:Landroid/widget/AdapterView$OnItemClickListener;

    .line 72
    return-void
.end method

.method private A()V
    .registers 6

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 323
    iget-object v0, p0, Landroid/support/v4/app/co;->e:Landroid/widget/ListView;

    if-eqz v0, :cond_7

    .line 374
    :goto_6
    return-void

    .line 3237
    :cond_7
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->ac:Landroid/view/View;

    .line 327
    if-nez v0, :cond_13

    .line 328
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Content view not yet created"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 330
    :cond_13
    instance-of v1, v0, Landroid/widget/ListView;

    if-eqz v1, :cond_57

    .line 331
    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Landroid/support/v4/app/co;->e:Landroid/widget/ListView;

    .line 360
    :cond_1b
    :goto_1b
    iput-boolean v2, p0, Landroid/support/v4/app/co;->k:Z

    .line 361
    iget-object v0, p0, Landroid/support/v4/app/co;->e:Landroid/widget/ListView;

    iget-object v1, p0, Landroid/support/v4/app/co;->at:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 362
    iget-object v0, p0, Landroid/support/v4/app/co;->d:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_ce

    .line 363
    iget-object v1, p0, Landroid/support/v4/app/co;->d:Landroid/widget/ListAdapter;

    .line 364
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v4/app/co;->d:Landroid/widget/ListAdapter;

    .line 4179
    iget-object v0, p0, Landroid/support/v4/app/co;->d:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_cb

    move v0, v2

    .line 4180
    :goto_32
    iput-object v1, p0, Landroid/support/v4/app/co;->d:Landroid/widget/ListAdapter;

    .line 4181
    iget-object v4, p0, Landroid/support/v4/app/co;->e:Landroid/widget/ListView;

    if-eqz v4, :cond_4f

    .line 4182
    iget-object v4, p0, Landroid/support/v4/app/co;->e:Landroid/widget/ListView;

    invoke-virtual {v4, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 4183
    iget-boolean v1, p0, Landroid/support/v4/app/co;->k:Z

    if-nez v1, :cond_4f

    if-nez v0, :cond_4f

    .line 4237
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->ac:Landroid/view/View;

    .line 4186
    invoke-virtual {v0}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v0

    if-eqz v0, :cond_4c

    move v3, v2

    :cond_4c
    invoke-direct {p0, v2, v3}, Landroid/support/v4/app/co;->a(ZZ)V

    .line 373
    :cond_4f
    :goto_4f
    iget-object v0, p0, Landroid/support/v4/app/co;->l:Landroid/os/Handler;

    iget-object v1, p0, Landroid/support/v4/app/co;->m:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_6

    .line 333
    :cond_57
    const v1, 0xff0001

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Landroid/support/v4/app/co;->g:Landroid/widget/TextView;

    .line 334
    iget-object v1, p0, Landroid/support/v4/app/co;->g:Landroid/widget/TextView;

    if-nez v1, :cond_96

    .line 335
    const v1, 0x1020004

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Landroid/support/v4/app/co;->f:Landroid/view/View;

    .line 339
    :goto_6f
    const v1, 0xff0002

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Landroid/support/v4/app/co;->h:Landroid/view/View;

    .line 340
    const v1, 0xff0003

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Landroid/support/v4/app/co;->i:Landroid/view/View;

    .line 341
    const v1, 0x102000a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 342
    instance-of v1, v0, Landroid/widget/ListView;

    if-nez v1, :cond_a6

    .line 343
    if-nez v0, :cond_9e

    .line 344
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Your content must have a ListView whose id attribute is \'android.R.id.list\'"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 337
    :cond_96
    iget-object v1, p0, Landroid/support/v4/app/co;->g:Landroid/widget/TextView;

    const/16 v4, 0x8

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_6f

    .line 348
    :cond_9e
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Content has view with id attribute \'android.R.id.list\' that is not a ListView class"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 352
    :cond_a6
    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Landroid/support/v4/app/co;->e:Landroid/widget/ListView;

    .line 353
    iget-object v0, p0, Landroid/support/v4/app/co;->f:Landroid/view/View;

    if-eqz v0, :cond_b7

    .line 354
    iget-object v0, p0, Landroid/support/v4/app/co;->e:Landroid/widget/ListView;

    iget-object v1, p0, Landroid/support/v4/app/co;->f:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    goto/16 :goto_1b

    .line 355
    :cond_b7
    iget-object v0, p0, Landroid/support/v4/app/co;->j:Ljava/lang/CharSequence;

    if-eqz v0, :cond_1b

    .line 356
    iget-object v0, p0, Landroid/support/v4/app/co;->g:Landroid/widget/TextView;

    iget-object v1, p0, Landroid/support/v4/app/co;->j:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 357
    iget-object v0, p0, Landroid/support/v4/app/co;->e:Landroid/widget/ListView;

    iget-object v1, p0, Landroid/support/v4/app/co;->g:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    goto/16 :goto_1b

    :cond_cb
    move v0, v3

    .line 4179
    goto/16 :goto_32

    .line 369
    :cond_ce
    iget-object v0, p0, Landroid/support/v4/app/co;->h:Landroid/view/View;

    if-eqz v0, :cond_4f

    .line 370
    invoke-direct {p0, v3, v3}, Landroid/support/v4/app/co;->a(ZZ)V

    goto/16 :goto_4f
.end method

.method public static a()V
    .registers 0

    .prologue
    .line 173
    return-void
.end method

.method private a(I)V
    .registers 3

    .prologue
    .line 198
    invoke-direct {p0}, Landroid/support/v4/app/co;->A()V

    .line 199
    iget-object v0, p0, Landroid/support/v4/app/co;->e:Landroid/widget/ListView;

    invoke-virtual {v0, p1}, Landroid/widget/ListView;->setSelection(I)V

    .line 200
    return-void
.end method

.method private a(Landroid/widget/ListAdapter;)V
    .registers 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 179
    iget-object v0, p0, Landroid/support/v4/app/co;->d:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_25

    move v0, v1

    .line 180
    :goto_7
    iput-object p1, p0, Landroid/support/v4/app/co;->d:Landroid/widget/ListAdapter;

    .line 181
    iget-object v3, p0, Landroid/support/v4/app/co;->e:Landroid/widget/ListView;

    if-eqz v3, :cond_24

    .line 182
    iget-object v3, p0, Landroid/support/v4/app/co;->e:Landroid/widget/ListView;

    invoke-virtual {v3, p1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 183
    iget-boolean v3, p0, Landroid/support/v4/app/co;->k:Z

    if-nez v3, :cond_24

    if-nez v0, :cond_24

    .line 2237
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->ac:Landroid/view/View;

    .line 186
    invoke-virtual {v0}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v0

    if-eqz v0, :cond_21

    move v2, v1

    :cond_21
    invoke-direct {p0, v1, v2}, Landroid/support/v4/app/co;->a(ZZ)V

    .line 189
    :cond_24
    return-void

    :cond_25
    move v0, v2

    .line 179
    goto :goto_7
.end method

.method private a(Ljava/lang/CharSequence;)V
    .registers 4

    .prologue
    .line 232
    invoke-direct {p0}, Landroid/support/v4/app/co;->A()V

    .line 233
    iget-object v0, p0, Landroid/support/v4/app/co;->g:Landroid/widget/TextView;

    if-nez v0, :cond_f

    .line 234
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t be used with a custom content view"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 236
    :cond_f
    iget-object v0, p0, Landroid/support/v4/app/co;->g:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 237
    iget-object v0, p0, Landroid/support/v4/app/co;->j:Ljava/lang/CharSequence;

    if-nez v0, :cond_1f

    .line 238
    iget-object v0, p0, Landroid/support/v4/app/co;->e:Landroid/widget/ListView;

    iget-object v1, p0, Landroid/support/v4/app/co;->g:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    .line 240
    :cond_1f
    iput-object p1, p0, Landroid/support/v4/app/co;->j:Ljava/lang/CharSequence;

    .line 241
    return-void
.end method

.method private a(Z)V
    .registers 3

    .prologue
    .line 258
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Landroid/support/v4/app/co;->a(ZZ)V

    .line 259
    return-void
.end method

.method private a(ZZ)V
    .registers 9

    .prologue
    const v5, 0x10a0001

    const/high16 v4, 0x10a0000

    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 280
    invoke-direct {p0}, Landroid/support/v4/app/co;->A()V

    .line 281
    iget-object v0, p0, Landroid/support/v4/app/co;->h:Landroid/view/View;

    if-nez v0, :cond_17

    .line 282
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t be used with a custom content view"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 284
    :cond_17
    iget-boolean v0, p0, Landroid/support/v4/app/co;->k:Z

    if-ne v0, p1, :cond_1c

    .line 313
    :goto_1b
    return-void

    .line 287
    :cond_1c
    iput-boolean p1, p0, Landroid/support/v4/app/co;->k:Z

    .line 288
    if-eqz p1, :cond_52

    .line 289
    if-eqz p2, :cond_47

    .line 290
    iget-object v0, p0, Landroid/support/v4/app/co;->h:Landroid/view/View;

    invoke-virtual {p0}, Landroid/support/v4/app/co;->i()Landroid/support/v4/app/bb;

    move-result-object v1

    invoke-static {v1, v5}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 292
    iget-object v0, p0, Landroid/support/v4/app/co;->i:Landroid/view/View;

    invoke-virtual {p0}, Landroid/support/v4/app/co;->i()Landroid/support/v4/app/bb;

    move-result-object v1

    invoke-static {v1, v4}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 298
    :goto_3c
    iget-object v0, p0, Landroid/support/v4/app/co;->h:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 299
    iget-object v0, p0, Landroid/support/v4/app/co;->i:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1b

    .line 295
    :cond_47
    iget-object v0, p0, Landroid/support/v4/app/co;->h:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    .line 296
    iget-object v0, p0, Landroid/support/v4/app/co;->i:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    goto :goto_3c

    .line 301
    :cond_52
    if-eqz p2, :cond_79

    .line 302
    iget-object v0, p0, Landroid/support/v4/app/co;->h:Landroid/view/View;

    invoke-virtual {p0}, Landroid/support/v4/app/co;->i()Landroid/support/v4/app/bb;

    move-result-object v1

    invoke-static {v1, v4}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 304
    iget-object v0, p0, Landroid/support/v4/app/co;->i:Landroid/view/View;

    invoke-virtual {p0}, Landroid/support/v4/app/co;->i()Landroid/support/v4/app/bb;

    move-result-object v1

    invoke-static {v1, v5}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 310
    :goto_6e
    iget-object v0, p0, Landroid/support/v4/app/co;->h:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 311
    iget-object v0, p0, Landroid/support/v4/app/co;->i:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1b

    .line 307
    :cond_79
    iget-object v0, p0, Landroid/support/v4/app/co;->h:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    .line 308
    iget-object v0, p0, Landroid/support/v4/app/co;->i:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    goto :goto_6e
.end method

.method private b()I
    .registers 2

    .prologue
    .line 206
    invoke-direct {p0}, Landroid/support/v4/app/co;->A()V

    .line 207
    iget-object v0, p0, Landroid/support/v4/app/co;->e:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getSelectedItemPosition()I

    move-result v0

    return v0
.end method

.method private d()J
    .registers 3

    .prologue
    .line 214
    invoke-direct {p0}, Landroid/support/v4/app/co;->A()V

    .line 215
    iget-object v0, p0, Landroid/support/v4/app/co;->e:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getSelectedItemId()J

    move-result-wide v0

    return-wide v0
.end method

.method private d(Z)V
    .registers 3

    .prologue
    .line 266
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/support/v4/app/co;->a(ZZ)V

    .line 267
    return-void
.end method

.method private y()Landroid/widget/ListView;
    .registers 2

    .prologue
    .line 222
    invoke-direct {p0}, Landroid/support/v4/app/co;->A()V

    .line 223
    iget-object v0, p0, Landroid/support/v4/app/co;->e:Landroid/widget/ListView;

    return-object v0
.end method

.method private z()Landroid/widget/ListAdapter;
    .registers 2

    .prologue
    .line 319
    iget-object v0, p0, Landroid/support/v4/app/co;->d:Landroid/widget/ListAdapter;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 12

    .prologue
    const/16 v8, 0x11

    const/4 v7, -0x2

    const/4 v6, -0x1

    .line 91
    invoke-virtual {p0}, Landroid/support/v4/app/co;->i()Landroid/support/v4/app/bb;

    move-result-object v0

    .line 93
    new-instance v1, Landroid/widget/FrameLayout;

    invoke-direct {v1, v0}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 97
    new-instance v2, Landroid/widget/LinearLayout;

    invoke-direct {v2, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 98
    const v3, 0xff0002

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setId(I)V

    .line 99
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 100
    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 101
    invoke-virtual {v2, v8}, Landroid/widget/LinearLayout;->setGravity(I)V

    .line 103
    new-instance v3, Landroid/widget/ProgressBar;

    const/4 v4, 0x0

    const v5, 0x101007a

    invoke-direct {v3, v0, v4, v5}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 105
    new-instance v4, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v4, v7, v7}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v3, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 108
    new-instance v3, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v3, v6, v6}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2, v3}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 113
    new-instance v2, Landroid/widget/FrameLayout;

    invoke-direct {v2, v0}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 114
    const v0, 0xff0003

    invoke-virtual {v2, v0}, Landroid/widget/FrameLayout;->setId(I)V

    .line 116
    new-instance v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Landroid/support/v4/app/co;->i()Landroid/support/v4/app/bb;

    move-result-object v3

    invoke-direct {v0, v3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 117
    const v3, 0xff0001

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setId(I)V

    .line 118
    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setGravity(I)V

    .line 119
    new-instance v3, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v3, v6, v6}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v0, v3}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 122
    new-instance v0, Landroid/widget/ListView;

    invoke-virtual {p0}, Landroid/support/v4/app/co;->i()Landroid/support/v4/app/bb;

    move-result-object v3

    invoke-direct {v0, v3}, Landroid/widget/ListView;-><init>(Landroid/content/Context;)V

    .line 123
    const v3, 0x102000a

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setId(I)V

    .line 124
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setDrawSelectorOnTop(Z)V

    .line 125
    new-instance v3, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v3, v6, v6}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v0, v3}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 128
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, v6, v6}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2, v0}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 133
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, v6, v6}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 136
    return-object v1
.end method

.method public final a(Landroid/view/View;Landroid/os/Bundle;)V
    .registers 3

    .prologue
    .line 144
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->a(Landroid/view/View;Landroid/os/Bundle;)V

    .line 145
    invoke-direct {p0}, Landroid/support/v4/app/co;->A()V

    .line 146
    return-void
.end method

.method public final g()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 153
    iget-object v0, p0, Landroid/support/v4/app/co;->l:Landroid/os/Handler;

    iget-object v1, p0, Landroid/support/v4/app/co;->m:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 154
    iput-object v2, p0, Landroid/support/v4/app/co;->e:Landroid/widget/ListView;

    .line 155
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v4/app/co;->k:Z

    .line 156
    iput-object v2, p0, Landroid/support/v4/app/co;->i:Landroid/view/View;

    iput-object v2, p0, Landroid/support/v4/app/co;->h:Landroid/view/View;

    iput-object v2, p0, Landroid/support/v4/app/co;->f:Landroid/view/View;

    .line 157
    iput-object v2, p0, Landroid/support/v4/app/co;->g:Landroid/widget/TextView;

    .line 158
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->g()V

    .line 159
    return-void
.end method
