.class final Landroid/support/v4/app/p;
.super Landroid/support/v4/app/r;
.source "SourceFile"


# instance fields
.field private a:Landroid/support/v4/app/gj;


# direct methods
.method public constructor <init>(Landroid/support/v4/app/gj;)V
    .registers 2

    .prologue
    .line 382
    invoke-direct {p0}, Landroid/support/v4/app/r;-><init>()V

    .line 383
    iput-object p1, p0, Landroid/support/v4/app/p;->a:Landroid/support/v4/app/gj;

    .line 384
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;Landroid/graphics/Matrix;Landroid/graphics/RectF;)Landroid/os/Parcelable;
    .registers 11

    .prologue
    .line 413
    iget-object v1, p0, Landroid/support/v4/app/p;->a:Landroid/support/v4/app/gj;

    .line 1169
    instance-of v0, p1, Landroid/widget/ImageView;

    if-eqz v0, :cond_4c

    move-object v0, p1

    .line 1170
    check-cast v0, Landroid/widget/ImageView;

    .line 1171
    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 1172
    invoke-virtual {v0}, Landroid/widget/ImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 1173
    if-eqz v2, :cond_4c

    if-nez v3, :cond_4c

    .line 1174
    invoke-static {v2}, Landroid/support/v4/app/gj;->a(Landroid/graphics/drawable/Drawable;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 1175
    if-eqz v2, :cond_4c

    .line 1176
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1177
    const-string v3, "sharedElement:snapshot:bitmap"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1178
    const-string v2, "sharedElement:snapshot:imageScaleType"

    invoke-virtual {v0}, Landroid/widget/ImageView;->getScaleType()Landroid/widget/ImageView$ScaleType;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/ImageView$ScaleType;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1180
    invoke-virtual {v0}, Landroid/widget/ImageView;->getScaleType()Landroid/widget/ImageView$ScaleType;

    move-result-object v2

    sget-object v3, Landroid/widget/ImageView$ScaleType;->MATRIX:Landroid/widget/ImageView$ScaleType;

    if-ne v2, v3, :cond_4a

    .line 1181
    invoke-virtual {v0}, Landroid/widget/ImageView;->getImageMatrix()Landroid/graphics/Matrix;

    move-result-object v0

    .line 1182
    const/16 v2, 0x9

    new-array v2, v2, [F

    .line 1183
    invoke-virtual {v0, v2}, Landroid/graphics/Matrix;->getValues([F)V

    .line 1184
    const-string v0, "sharedElement:snapshot:imageMatrix"

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putFloatArray(Ljava/lang/String;[F)V

    :cond_4a
    move-object v0, v1

    .line 1186
    :cond_4b
    :goto_4b
    return-object v0

    .line 1190
    :cond_4c
    invoke-virtual {p3}, Landroid/graphics/RectF;->width()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v2

    .line 1191
    invoke-virtual {p3}, Landroid/graphics/RectF;->height()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v3

    .line 1192
    const/4 v0, 0x0

    .line 1193
    if-lez v2, :cond_4b

    if-lez v3, :cond_4b

    .line 1194
    const/high16 v0, 0x3f800000    # 1.0f

    sget v4, Landroid/support/v4/app/gj;->b:I

    int-to-float v4, v4

    mul-int v5, v2, v3

    int-to-float v5, v5

    div-float/2addr v4, v5

    invoke-static {v0, v4}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 1195
    int-to-float v2, v2

    mul-float/2addr v2, v0

    float-to-int v2, v2

    .line 1196
    int-to-float v3, v3

    mul-float/2addr v3, v0

    float-to-int v3, v3

    .line 1197
    iget-object v4, v1, Landroid/support/v4/app/gj;->a:Landroid/graphics/Matrix;

    if-nez v4, :cond_7f

    .line 1198
    new-instance v4, Landroid/graphics/Matrix;

    invoke-direct {v4}, Landroid/graphics/Matrix;-><init>()V

    iput-object v4, v1, Landroid/support/v4/app/gj;->a:Landroid/graphics/Matrix;

    .line 1200
    :cond_7f
    iget-object v4, v1, Landroid/support/v4/app/gj;->a:Landroid/graphics/Matrix;

    invoke-virtual {v4, p2}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 1201
    iget-object v4, v1, Landroid/support/v4/app/gj;->a:Landroid/graphics/Matrix;

    iget v5, p3, Landroid/graphics/RectF;->left:F

    neg-float v5, v5

    iget v6, p3, Landroid/graphics/RectF;->top:F

    neg-float v6, v6

    invoke-virtual {v4, v5, v6}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 1202
    iget-object v4, v1, Landroid/support/v4/app/gj;->a:Landroid/graphics/Matrix;

    invoke-virtual {v4, v0, v0}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 1203
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v2, v3, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1204
    new-instance v2, Landroid/graphics/Canvas;

    invoke-direct {v2, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1205
    iget-object v1, v1, Landroid/support/v4/app/gj;->a:Landroid/graphics/Matrix;

    invoke-virtual {v2, v1}, Landroid/graphics/Canvas;->concat(Landroid/graphics/Matrix;)V

    .line 1206
    invoke-virtual {p1, v2}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    goto :goto_4b
.end method

.method public final a(Landroid/content/Context;Landroid/os/Parcelable;)Landroid/view/View;
    .registers 4

    .prologue
    .line 419
    invoke-static {p1, p2}, Landroid/support/v4/app/gj;->a(Landroid/content/Context;Landroid/os/Parcelable;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final a()V
    .registers 1

    .prologue
    .line 391
    return-void
.end method

.method public final b()V
    .registers 1

    .prologue
    .line 398
    return-void
.end method

.method public final c()V
    .registers 1

    .prologue
    .line 403
    return-void
.end method

.method public final d()V
    .registers 1

    .prologue
    .line 408
    return-void
.end method
