.class public final Landroid/support/v4/app/fn;
.super Landroid/support/v4/app/fw;
.source "SourceFile"


# static fields
.field public static final a:Ljava/lang/String; = "android.remoteinput.results"

.field public static final b:Ljava/lang/String; = "android.remoteinput.resultsData"

.field public static final c:Landroid/support/v4/app/fx;

.field private static final d:Ljava/lang/String; = "RemoteInput"

.field private static final j:Landroid/support/v4/app/fq;


# instance fields
.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/CharSequence;

.field private final g:[Ljava/lang/CharSequence;

.field private final h:Z

.field private final i:Landroid/os/Bundle;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 253
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x14

    if-lt v0, v1, :cond_15

    .line 254
    new-instance v0, Landroid/support/v4/app/fr;

    invoke-direct {v0}, Landroid/support/v4/app/fr;-><init>()V

    sput-object v0, Landroid/support/v4/app/fn;->j:Landroid/support/v4/app/fq;

    .line 263
    :goto_d
    new-instance v0, Landroid/support/v4/app/fo;

    invoke-direct {v0}, Landroid/support/v4/app/fo;-><init>()V

    sput-object v0, Landroid/support/v4/app/fn;->c:Landroid/support/v4/app/fx;

    return-void

    .line 255
    :cond_15
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_23

    .line 256
    new-instance v0, Landroid/support/v4/app/ft;

    invoke-direct {v0}, Landroid/support/v4/app/ft;-><init>()V

    sput-object v0, Landroid/support/v4/app/fn;->j:Landroid/support/v4/app/fq;

    goto :goto_d

    .line 258
    :cond_23
    new-instance v0, Landroid/support/v4/app/fs;

    invoke-direct {v0}, Landroid/support/v4/app/fs;-><init>()V

    sput-object v0, Landroid/support/v4/app/fn;->j:Landroid/support/v4/app/fq;

    goto :goto_d
.end method

.method constructor <init>(Ljava/lang/String;Ljava/lang/CharSequence;[Ljava/lang/CharSequence;ZLandroid/os/Bundle;)V
    .registers 6

    .prologue
    .line 44
    invoke-direct {p0}, Landroid/support/v4/app/fw;-><init>()V

    .line 45
    iput-object p1, p0, Landroid/support/v4/app/fn;->e:Ljava/lang/String;

    .line 46
    iput-object p2, p0, Landroid/support/v4/app/fn;->f:Ljava/lang/CharSequence;

    .line 47
    iput-object p3, p0, Landroid/support/v4/app/fn;->g:[Ljava/lang/CharSequence;

    .line 48
    iput-boolean p4, p0, Landroid/support/v4/app/fn;->h:Z

    .line 49
    iput-object p5, p0, Landroid/support/v4/app/fn;->i:Landroid/os/Bundle;

    .line 50
    return-void
.end method

.method private static a(Landroid/content/Intent;)Landroid/os/Bundle;
    .registers 2

    .prologue
    .line 188
    sget-object v0, Landroid/support/v4/app/fn;->j:Landroid/support/v4/app/fq;

    invoke-interface {v0, p0}, Landroid/support/v4/app/fq;->a(Landroid/content/Intent;)Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method private static a([Landroid/support/v4/app/fn;Landroid/content/Intent;Landroid/os/Bundle;)V
    .registers 4

    .prologue
    .line 204
    sget-object v0, Landroid/support/v4/app/fn;->j:Landroid/support/v4/app/fq;

    invoke-interface {v0, p0, p1, p2}, Landroid/support/v4/app/fq;->a([Landroid/support/v4/app/fn;Landroid/content/Intent;Landroid/os/Bundle;)V

    .line 205
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .registers 2

    .prologue
    .line 57
    iget-object v0, p0, Landroid/support/v4/app/fn;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Ljava/lang/CharSequence;
    .registers 2

    .prologue
    .line 64
    iget-object v0, p0, Landroid/support/v4/app/fn;->f:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final c()[Ljava/lang/CharSequence;
    .registers 2

    .prologue
    .line 71
    iget-object v0, p0, Landroid/support/v4/app/fn;->g:[Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final d()Z
    .registers 2

    .prologue
    .line 81
    iget-boolean v0, p0, Landroid/support/v4/app/fn;->h:Z

    return v0
.end method

.method public final e()Landroid/os/Bundle;
    .registers 2

    .prologue
    .line 88
    iget-object v0, p0, Landroid/support/v4/app/fn;->i:Landroid/os/Bundle;

    return-object v0
.end method
