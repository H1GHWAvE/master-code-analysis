.class public abstract Landroid/support/v4/app/cd;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final F:I = 0x1000

.field public static final G:I = 0x2000

.field public static final H:I = -0x1

.field public static final I:I = 0x0

.field public static final J:I = 0x1001

.field public static final K:I = 0x2002

.field public static final L:I = 0x1003


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 165
    return-void
.end method


# virtual methods
.method public abstract a(I)Landroid/support/v4/app/cd;
.end method

.method public abstract a(II)Landroid/support/v4/app/cd;
    .param p1    # I
        .annotation build Landroid/support/a/a;
        .end annotation
    .end param
    .param p2    # I
        .annotation build Landroid/support/a/a;
        .end annotation
    .end param
.end method

.method public abstract a(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/cd;
    .param p1    # I
        .annotation build Landroid/support/a/p;
        .end annotation
    .end param
.end method

.method public abstract a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/cd;
    .param p1    # I
        .annotation build Landroid/support/a/p;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Landroid/support/a/z;
        .end annotation
    .end param
.end method

.method public abstract a(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/cd;
.end method

.method public abstract a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/cd;
.end method

.method public abstract a(Landroid/view/View;Ljava/lang/String;)Landroid/support/v4/app/cd;
.end method

.method public abstract a(Ljava/lang/CharSequence;)Landroid/support/v4/app/cd;
.end method

.method public abstract b(I)Landroid/support/v4/app/cd;
    .param p1    # I
        .annotation build Landroid/support/a/ai;
        .end annotation
    .end param
.end method

.method public abstract b(II)Landroid/support/v4/app/cd;
    .param p1    # I
        .annotation build Landroid/support/a/a;
        .end annotation
    .end param
    .param p2    # I
        .annotation build Landroid/support/a/a;
        .end annotation
    .end param
.end method

.method public abstract b(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/cd;
.end method

.method public abstract b(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/cd;
.end method

.method public abstract b(Ljava/lang/CharSequence;)Landroid/support/v4/app/cd;
.end method

.method public abstract c(I)Landroid/support/v4/app/cd;
    .param p1    # I
        .annotation build Landroid/support/a/ah;
        .end annotation
    .end param
.end method

.method public abstract c(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/cd;
.end method

.method public abstract d(I)Landroid/support/v4/app/cd;
    .param p1    # I
        .annotation build Landroid/support/a/ah;
        .end annotation
    .end param
.end method

.method public abstract d(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/cd;
.end method

.method public abstract e(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/cd;
.end method

.method public abstract f()Landroid/support/v4/app/cd;
.end method

.method public abstract f(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/cd;
.end method

.method public abstract g()Z
.end method

.method public abstract h()Landroid/support/v4/app/cd;
.end method

.method public abstract i()I
.end method

.method public abstract j()I
.end method

.method public abstract l()Z
.end method
