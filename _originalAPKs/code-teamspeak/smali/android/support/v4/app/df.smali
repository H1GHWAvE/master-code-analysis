.class public final Landroid/support/v4/app/df;
.super Landroid/support/v4/app/ek;
.source "SourceFile"


# static fields
.field public static final e:Landroid/support/v4/app/el;


# instance fields
.field final a:Landroid/os/Bundle;

.field public b:I

.field public c:Ljava/lang/CharSequence;

.field public d:Landroid/app/PendingIntent;

.field private final f:[Landroid/support/v4/app/fn;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 2171
    new-instance v0, Landroid/support/v4/app/dg;

    invoke-direct {v0}, Landroid/support/v4/app/dg;-><init>()V

    sput-object v0, Landroid/support/v4/app/df;->e:Landroid/support/v4/app/el;

    return-void
.end method

.method public constructor <init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)V
    .registers 10

    .prologue
    .line 1827
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    const/4 v5, 0x0

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, Landroid/support/v4/app/df;-><init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;Landroid/os/Bundle;[Landroid/support/v4/app/fn;)V

    .line 1828
    return-void
.end method

.method private constructor <init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;Landroid/os/Bundle;[Landroid/support/v4/app/fn;)V
    .registers 7

    .prologue
    .line 1831
    invoke-direct {p0}, Landroid/support/v4/app/ek;-><init>()V

    .line 1832
    iput p1, p0, Landroid/support/v4/app/df;->b:I

    .line 1833
    invoke-static {p2}, Landroid/support/v4/app/dm;->d(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/app/df;->c:Ljava/lang/CharSequence;

    .line 1834
    iput-object p3, p0, Landroid/support/v4/app/df;->d:Landroid/app/PendingIntent;

    .line 1835
    if-eqz p4, :cond_14

    :goto_f
    iput-object p4, p0, Landroid/support/v4/app/df;->a:Landroid/os/Bundle;

    .line 1836
    iput-object p5, p0, Landroid/support/v4/app/df;->f:[Landroid/support/v4/app/fn;

    .line 1837
    return-void

    .line 1835
    :cond_14
    new-instance p4, Landroid/os/Bundle;

    invoke-direct {p4}, Landroid/os/Bundle;-><init>()V

    goto :goto_f
.end method

.method synthetic constructor <init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;Landroid/os/Bundle;[Landroid/support/v4/app/fn;B)V
    .registers 7

    .prologue
    .line 1808
    invoke-direct/range {p0 .. p5}, Landroid/support/v4/app/df;-><init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;Landroid/os/Bundle;[Landroid/support/v4/app/fn;)V

    return-void
.end method

.method static synthetic a(Landroid/support/v4/app/df;)Landroid/os/Bundle;
    .registers 2

    .prologue
    .line 1808
    iget-object v0, p0, Landroid/support/v4/app/df;->a:Landroid/os/Bundle;

    return-object v0
.end method

.method private f()[Landroid/support/v4/app/fn;
    .registers 2

    .prologue
    .line 1868
    iget-object v0, p0, Landroid/support/v4/app/df;->f:[Landroid/support/v4/app/fn;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .registers 2

    .prologue
    .line 1841
    iget v0, p0, Landroid/support/v4/app/df;->b:I

    return v0
.end method

.method public final b()Ljava/lang/CharSequence;
    .registers 2

    .prologue
    .line 1846
    iget-object v0, p0, Landroid/support/v4/app/df;->c:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final c()Landroid/app/PendingIntent;
    .registers 2

    .prologue
    .line 1851
    iget-object v0, p0, Landroid/support/v4/app/df;->d:Landroid/app/PendingIntent;

    return-object v0
.end method

.method public final d()Landroid/os/Bundle;
    .registers 2

    .prologue
    .line 1859
    iget-object v0, p0, Landroid/support/v4/app/df;->a:Landroid/os/Bundle;

    return-object v0
.end method

.method public final bridge synthetic e()[Landroid/support/v4/app/fw;
    .registers 2

    .prologue
    .line 1808
    .line 2868
    iget-object v0, p0, Landroid/support/v4/app/df;->f:[Landroid/support/v4/app/fn;

    .line 1808
    return-object v0
.end method
