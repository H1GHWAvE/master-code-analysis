.class public Landroid/support/v4/app/bb;
.super Landroid/support/v4/app/as;
.source "SourceFile"

# interfaces
.implements Landroid/support/v4/app/o;
.implements Landroid/support/v4/app/v;


# static fields
.field static final a:Ljava/lang/String; = "android:support:fragments"

.field static final b:I = 0x1

.field static final c:I = 0x2

.field private static final m:Ljava/lang/String; = "FragmentActivity"

.field private static final n:I = 0xb


# instance fields
.field final d:Landroid/os/Handler;

.field final e:Landroid/support/v4/app/bg;

.field f:Z

.field g:Z

.field h:Z

.field i:Z

.field j:Z

.field k:Z

.field l:Z


# direct methods
.method public constructor <init>()V
    .registers 3

    .prologue
    .line 78
    invoke-direct {p0}, Landroid/support/v4/app/as;-><init>()V

    .line 91
    new-instance v0, Landroid/support/v4/app/bc;

    invoke-direct {v0, p0}, Landroid/support/v4/app/bc;-><init>(Landroid/support/v4/app/bb;)V

    iput-object v0, p0, Landroid/support/v4/app/bb;->d:Landroid/os/Handler;

    .line 110
    new-instance v0, Landroid/support/v4/app/bd;

    invoke-direct {v0, p0}, Landroid/support/v4/app/bd;-><init>(Landroid/support/v4/app/bb;)V

    .line 2047
    new-instance v1, Landroid/support/v4/app/bg;

    invoke-direct {v1, v0}, Landroid/support/v4/app/bg;-><init>(Landroid/support/v4/app/bh;)V

    .line 110
    iput-object v1, p0, Landroid/support/v4/app/bb;->e:Landroid/support/v4/app/bg;

    .line 839
    return-void
.end method

.method private static a(Landroid/view/View;)Ljava/lang/String;
    .registers 8

    .prologue
    const/16 v3, 0x56

    const/16 v1, 0x46

    const/16 v6, 0x2c

    const/16 v5, 0x20

    const/16 v2, 0x2e

    .line 608
    new-instance v4, Ljava/lang/StringBuilder;

    const/16 v0, 0x80

    invoke-direct {v4, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 609
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 610
    const/16 v0, 0x7b

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 611
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 612
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 613
    invoke-virtual {p0}, Landroid/view/View;->getVisibility()I

    move-result v0

    sparse-switch v0, :sswitch_data_156

    .line 617
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 619
    :goto_39
    invoke-virtual {p0}, Landroid/view/View;->isFocusable()Z

    move-result v0

    if-eqz v0, :cond_131

    move v0, v1

    :goto_40
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 620
    invoke-virtual {p0}, Landroid/view/View;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_134

    const/16 v0, 0x45

    :goto_4b
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 621
    invoke-virtual {p0}, Landroid/view/View;->willNotDraw()Z

    move-result v0

    if-eqz v0, :cond_137

    move v0, v2

    :goto_55
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 622
    invoke-virtual {p0}, Landroid/view/View;->isHorizontalScrollBarEnabled()Z

    move-result v0

    if-eqz v0, :cond_13b

    const/16 v0, 0x48

    :goto_60
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 623
    invoke-virtual {p0}, Landroid/view/View;->isVerticalScrollBarEnabled()Z

    move-result v0

    if-eqz v0, :cond_13e

    move v0, v3

    :goto_6a
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 624
    invoke-virtual {p0}, Landroid/view/View;->isClickable()Z

    move-result v0

    if-eqz v0, :cond_141

    const/16 v0, 0x43

    :goto_75
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 625
    invoke-virtual {p0}, Landroid/view/View;->isLongClickable()Z

    move-result v0

    if-eqz v0, :cond_144

    const/16 v0, 0x4c

    :goto_80
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 626
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 627
    invoke-virtual {p0}, Landroid/view/View;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_147

    :goto_8c
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 628
    invoke-virtual {p0}, Landroid/view/View;->isSelected()Z

    move-result v0

    if-eqz v0, :cond_14a

    const/16 v0, 0x53

    :goto_97
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 629
    invoke-virtual {p0}, Landroid/view/View;->isPressed()Z

    move-result v0

    if-eqz v0, :cond_a2

    const/16 v2, 0x50

    :cond_a2
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 630
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 631
    invoke-virtual {p0}, Landroid/view/View;->getLeft()I

    move-result v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 632
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 633
    invoke-virtual {p0}, Landroid/view/View;->getTop()I

    move-result v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 634
    const/16 v0, 0x2d

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 635
    invoke-virtual {p0}, Landroid/view/View;->getRight()I

    move-result v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 636
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 637
    invoke-virtual {p0}, Landroid/view/View;->getBottom()I

    move-result v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 638
    invoke-virtual {p0}, Landroid/view/View;->getId()I

    move-result v1

    .line 639
    const/4 v0, -0x1

    if-eq v1, v0, :cond_114

    .line 640
    const-string v0, " #"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 641
    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 642
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 643
    if-eqz v1, :cond_114

    if-eqz v2, :cond_114

    .line 646
    const/high16 v0, -0x1000000

    and-int/2addr v0, v1

    sparse-switch v0, :sswitch_data_164

    .line 654
    :try_start_f0
    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getResourcePackageName(I)Ljava/lang/String;

    move-result-object v0

    .line 657
    :goto_f4
    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getResourceTypeName(I)Ljava/lang/String;

    move-result-object v3

    .line 658
    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getResourceEntryName(I)Ljava/lang/String;

    move-result-object v1

    .line 659
    const-string v2, " "

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 660
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 661
    const-string v0, ":"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 662
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 663
    const-string v0, "/"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 664
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_114
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_f0 .. :try_end_114} :catch_153

    .line 669
    :cond_114
    :goto_114
    const-string v0, "}"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 670
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 614
    :sswitch_11e
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto/16 :goto_39

    .line 615
    :sswitch_123
    const/16 v0, 0x49

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto/16 :goto_39

    .line 616
    :sswitch_12a
    const/16 v0, 0x47

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto/16 :goto_39

    :cond_131
    move v0, v2

    .line 619
    goto/16 :goto_40

    :cond_134
    move v0, v2

    .line 620
    goto/16 :goto_4b

    .line 621
    :cond_137
    const/16 v0, 0x44

    goto/16 :goto_55

    :cond_13b
    move v0, v2

    .line 622
    goto/16 :goto_60

    :cond_13e
    move v0, v2

    .line 623
    goto/16 :goto_6a

    :cond_141
    move v0, v2

    .line 624
    goto/16 :goto_75

    :cond_144
    move v0, v2

    .line 625
    goto/16 :goto_80

    :cond_147
    move v1, v2

    .line 627
    goto/16 :goto_8c

    :cond_14a
    move v0, v2

    .line 628
    goto/16 :goto_97

    .line 648
    :sswitch_14d
    :try_start_14d
    const-string v0, "app"

    goto :goto_f4

    .line 651
    :sswitch_150
    const-string v0, "android"
    :try_end_152
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_14d .. :try_end_152} :catch_153

    goto :goto_f4

    :catch_153
    move-exception v0

    goto :goto_114

    .line 613
    nop

    :sswitch_data_156
    .sparse-switch
        0x0 -> :sswitch_11e
        0x4 -> :sswitch_123
        0x8 -> :sswitch_12a
    .end sparse-switch

    .line 646
    :sswitch_data_164
    .sparse-switch
        0x1000000 -> :sswitch_150
        0x7f000000 -> :sswitch_14d
    .end sparse-switch
.end method

.method private a(Landroid/support/v4/app/Fragment;[Ljava/lang/String;I)V
    .registers 6

    .prologue
    .line 827
    const/4 v0, -0x1

    if-ne p3, v0, :cond_7

    .line 828
    invoke-static {p0, p2, p3}, Landroid/support/v4/app/m;->a(Landroid/app/Activity;[Ljava/lang/String;I)V

    .line 837
    :goto_6
    return-void

    .line 831
    :cond_7
    and-int/lit16 v0, p3, -0x100

    if-eqz v0, :cond_13

    .line 832
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Can only use lower 8 bits for requestCode"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 834
    :cond_13
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/app/bb;->l:Z

    .line 835
    iget v0, p1, Landroid/support/v4/app/Fragment;->z:I

    add-int/lit8 v0, v0, 0x1

    shl-int/lit8 v0, v0, 0x8

    and-int/lit16 v1, p3, 0xff

    add-int/2addr v0, v1

    invoke-static {p0, p2, v0}, Landroid/support/v4/app/m;->a(Landroid/app/Activity;[Ljava/lang/String;I)V

    goto :goto_6
.end method

.method static synthetic a(Landroid/support/v4/app/bb;Landroid/support/v4/app/Fragment;[Ljava/lang/String;I)V
    .registers 6

    .prologue
    .line 35827
    const/4 v0, -0x1

    if-ne p3, v0, :cond_7

    .line 35828
    invoke-static {p0, p2, p3}, Landroid/support/v4/app/m;->a(Landroid/app/Activity;[Ljava/lang/String;I)V

    .line 35829
    :goto_6
    return-void

    .line 35831
    :cond_7
    and-int/lit16 v0, p3, -0x100

    if-eqz v0, :cond_13

    .line 35832
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Can only use lower 8 bits for requestCode"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 35834
    :cond_13
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/app/bb;->l:Z

    .line 35835
    iget v0, p1, Landroid/support/v4/app/Fragment;->z:I

    add-int/lit8 v0, v0, 0x1

    shl-int/lit8 v0, v0, 0x8

    and-int/lit16 v1, p3, 0xff

    add-int/2addr v0, v1

    invoke-static {p0, p2, v0}, Landroid/support/v4/app/m;->a(Landroid/app/Activity;[Ljava/lang/String;I)V

    goto :goto_6
.end method

.method private a(Landroid/support/v4/app/gj;)V
    .registers 4

    .prologue
    .line 193
    .line 4231
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_11

    .line 4232
    invoke-static {p1}, Landroid/support/v4/app/m;->a(Landroid/support/v4/app/gj;)Landroid/support/v4/app/r;

    move-result-object v0

    .line 5040
    invoke-static {v0}, Landroid/support/v4/app/q;->a(Landroid/support/v4/app/r;)Landroid/app/SharedElementCallback;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/app/Activity;->setEnterSharedElementCallback(Landroid/app/SharedElementCallback;)V

    .line 194
    :cond_11
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/io/PrintWriter;Landroid/view/View;)V
    .registers 8

    .prologue
    .line 674
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 675
    if-nez p3, :cond_b

    .line 676
    const-string v0, "null"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 692
    :cond_a
    return-void

    .line 679
    :cond_b
    invoke-static {p3}, Landroid/support/v4/app/bb;->a(Landroid/view/View;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 680
    instance-of v0, p3, Landroid/view/ViewGroup;

    if-eqz v0, :cond_a

    .line 683
    check-cast p3, Landroid/view/ViewGroup;

    .line 684
    invoke-virtual {p3}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    .line 685
    if-lez v1, :cond_a

    .line 688
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "  "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 689
    const/4 v0, 0x0

    :goto_32
    if-ge v0, v1, :cond_a

    .line 690
    invoke-virtual {p3, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-direct {p0, v2, p2, v3}, Landroid/support/v4/app/bb;->a(Ljava/lang/String;Ljava/io/PrintWriter;Landroid/view/View;)V

    .line 689
    add-int/lit8 v0, v0, 0x1

    goto :goto_32
.end method

.method private a(Landroid/view/View;Landroid/view/Menu;)Z
    .registers 4

    .prologue
    .line 451
    const/4 v0, 0x0

    invoke-super {p0, v0, p1, p2}, Landroid/support/v4/app/as;->onPreparePanel(ILandroid/view/View;Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method private b(Landroid/support/v4/app/gj;)V
    .registers 4

    .prologue
    .line 206
    .line 5247
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_11

    .line 5248
    invoke-static {p1}, Landroid/support/v4/app/m;->a(Landroid/support/v4/app/gj;)Landroid/support/v4/app/r;

    move-result-object v0

    .line 6045
    invoke-static {v0}, Landroid/support/v4/app/q;->a(Landroid/support/v4/app/r;)Landroid/app/SharedElementCallback;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/app/Activity;->setExitSharedElementCallback(Landroid/app/SharedElementCallback;)V

    .line 207
    :cond_11
    return-void
.end method

.method private d()V
    .registers 3

    .prologue
    .line 181
    .line 3193
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_a

    .line 4035
    invoke-virtual {p0}, Landroid/app/Activity;->finishAfterTransition()V

    .line 3194
    :goto_9
    return-void

    .line 3196
    :cond_a
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto :goto_9
.end method

.method private e()V
    .registers 3

    .prologue
    .line 214
    .line 6253
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_9

    .line 7049
    invoke-virtual {p0}, Landroid/app/Activity;->postponeEnterTransition()V

    .line 215
    :cond_9
    return-void
.end method

.method private f()V
    .registers 3

    .prologue
    .line 222
    .line 7259
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_9

    .line 8053
    invoke-virtual {p0}, Landroid/app/Activity;->startPostponedEnterTransition()V

    .line 223
    :cond_9
    return-void
.end method

.method private static g()Ljava/lang/Object;
    .registers 1

    .prologue
    .line 543
    const/4 v0, 0x0

    return-object v0
.end method

.method private h()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 552
    invoke-virtual {p0}, Landroid/support/v4/app/bb;->getLastNonConfigurationInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/be;

    .line 554
    if-eqz v0, :cond_b

    iget-object v0, v0, Landroid/support/v4/app/be;->a:Ljava/lang/Object;

    :goto_a
    return-object v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method private i()V
    .registers 4

    .prologue
    .line 711
    iget-object v0, p0, Landroid/support/v4/app/bb;->e:Landroid/support/v4/app/bg;

    iget-boolean v1, p0, Landroid/support/v4/app/bb;->j:Z

    .line 31348
    iget-object v0, v0, Landroid/support/v4/app/bg;->a:Landroid/support/v4/app/bh;

    .line 32220
    iget-object v2, v0, Landroid/support/v4/app/bh;->h:Landroid/support/v4/app/ct;

    if-eqz v2, :cond_18

    .line 32224
    iget-boolean v2, v0, Landroid/support/v4/app/bh;->j:Z

    if-eqz v2, :cond_18

    .line 32227
    const/4 v2, 0x0

    iput-boolean v2, v0, Landroid/support/v4/app/bh;->j:Z

    .line 32229
    if-eqz v1, :cond_23

    .line 32230
    iget-object v0, v0, Landroid/support/v4/app/bh;->h:Landroid/support/v4/app/ct;

    invoke-virtual {v0}, Landroid/support/v4/app/ct;->d()V

    .line 713
    :cond_18
    :goto_18
    iget-object v0, p0, Landroid/support/v4/app/bb;->e:Landroid/support/v4/app/bg;

    .line 33213
    iget-object v0, v0, Landroid/support/v4/app/bg;->a:Landroid/support/v4/app/bh;

    iget-object v0, v0, Landroid/support/v4/app/bh;->f:Landroid/support/v4/app/bl;

    .line 34028
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/support/v4/app/bl;->c(I)V

    .line 714
    return-void

    .line 32232
    :cond_23
    iget-object v0, v0, Landroid/support/v4/app/bh;->h:Landroid/support/v4/app/ct;

    invoke-virtual {v0}, Landroid/support/v4/app/ct;->c()V

    goto :goto_18
.end method

.method private static j()V
    .registers 0

    .prologue
    .line 725
    return-void
.end method

.method private k()Landroid/support/v4/app/cr;
    .registers 5

    .prologue
    const/4 v3, 0x1

    .line 736
    iget-object v0, p0, Landroid/support/v4/app/bb;->e:Landroid/support/v4/app/bg;

    .line 35065
    iget-object v0, v0, Landroid/support/v4/app/bg;->a:Landroid/support/v4/app/bh;

    .line 35178
    iget-object v1, v0, Landroid/support/v4/app/bh;->h:Landroid/support/v4/app/ct;

    if-eqz v1, :cond_c

    .line 35179
    iget-object v0, v0, Landroid/support/v4/app/bh;->h:Landroid/support/v4/app/ct;

    :goto_b
    return-object v0

    .line 35181
    :cond_c
    iput-boolean v3, v0, Landroid/support/v4/app/bh;->i:Z

    .line 35182
    const-string v1, "(root)"

    iget-boolean v2, v0, Landroid/support/v4/app/bh;->j:Z

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/app/bh;->a(Ljava/lang/String;ZZ)Landroid/support/v4/app/ct;

    move-result-object v1

    iput-object v1, v0, Landroid/support/v4/app/bh;->h:Landroid/support/v4/app/ct;

    .line 35183
    iget-object v0, v0, Landroid/support/v4/app/bh;->h:Landroid/support/v4/app/ct;

    goto :goto_b
.end method


# virtual methods
.method final a(Landroid/view/View;Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;
    .registers 6

    .prologue
    .line 278
    iget-object v0, p0, Landroid/support/v4/app/bb;->e:Landroid/support/v4/app/bg;

    .line 12111
    iget-object v0, v0, Landroid/support/v4/app/bg;->a:Landroid/support/v4/app/bh;

    iget-object v0, v0, Landroid/support/v4/app/bh;->f:Landroid/support/v4/app/bl;

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/support/v4/app/bl;->a(Landroid/view/View;Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;

    move-result-object v0

    .line 278
    return-object v0
.end method

.method public final a(I)V
    .registers 4

    .prologue
    .line 760
    iget-boolean v0, p0, Landroid/support/v4/app/bb;->l:Z

    if-eqz v0, :cond_8

    .line 761
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v4/app/bb;->l:Z

    .line 765
    :cond_7
    return-void

    .line 762
    :cond_8
    and-int/lit16 v0, p1, -0x100

    if-eqz v0, :cond_7

    .line 763
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Can only use lower 8 bits for requestCode"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Landroid/support/v4/app/Fragment;Landroid/content/Intent;I)V
    .registers 6

    .prologue
    const/4 v0, -0x1

    .line 812
    if-ne p3, v0, :cond_7

    .line 813
    invoke-super {p0, p2, v0}, Landroid/support/v4/app/as;->startActivityForResult(Landroid/content/Intent;I)V

    .line 820
    :goto_6
    return-void

    .line 816
    :cond_7
    const/high16 v0, -0x10000

    and-int/2addr v0, p3

    if-eqz v0, :cond_14

    .line 817
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Can only use lower 16 bits for requestCode"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 819
    :cond_14
    iget v0, p1, Landroid/support/v4/app/Fragment;->z:I

    add-int/lit8 v0, v0, 0x1

    shl-int/lit8 v0, v0, 0x10

    const v1, 0xffff

    and-int/2addr v1, p3

    add-int/2addr v0, v1

    invoke-super {p0, p2, v0}, Landroid/support/v4/app/as;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_6
.end method

.method final a(Z)V
    .registers 5

    .prologue
    const/4 v1, 0x1

    .line 695
    iget-boolean v0, p0, Landroid/support/v4/app/bb;->i:Z

    if-nez v0, :cond_30

    .line 696
    iput-boolean v1, p0, Landroid/support/v4/app/bb;->i:Z

    .line 697
    iput-boolean p1, p0, Landroid/support/v4/app/bb;->j:Z

    .line 698
    iget-object v0, p0, Landroid/support/v4/app/bb;->d:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 27711
    iget-object v0, p0, Landroid/support/v4/app/bb;->e:Landroid/support/v4/app/bg;

    iget-boolean v1, p0, Landroid/support/v4/app/bb;->j:Z

    .line 28348
    iget-object v0, v0, Landroid/support/v4/app/bg;->a:Landroid/support/v4/app/bh;

    .line 29220
    iget-object v2, v0, Landroid/support/v4/app/bh;->h:Landroid/support/v4/app/ct;

    if-eqz v2, :cond_26

    .line 29224
    iget-boolean v2, v0, Landroid/support/v4/app/bh;->j:Z

    if-eqz v2, :cond_26

    .line 29227
    const/4 v2, 0x0

    iput-boolean v2, v0, Landroid/support/v4/app/bh;->j:Z

    .line 29229
    if-eqz v1, :cond_31

    .line 29230
    iget-object v0, v0, Landroid/support/v4/app/bh;->h:Landroid/support/v4/app/ct;

    invoke-virtual {v0}, Landroid/support/v4/app/ct;->d()V

    .line 27713
    :cond_26
    :goto_26
    iget-object v0, p0, Landroid/support/v4/app/bb;->e:Landroid/support/v4/app/bg;

    .line 30213
    iget-object v0, v0, Landroid/support/v4/app/bg;->a:Landroid/support/v4/app/bh;

    iget-object v0, v0, Landroid/support/v4/app/bh;->f:Landroid/support/v4/app/bl;

    .line 31028
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/support/v4/app/bl;->c(I)V

    .line 701
    :cond_30
    return-void

    .line 29232
    :cond_31
    iget-object v0, v0, Landroid/support/v4/app/bh;->h:Landroid/support/v4/app/ct;

    invoke-virtual {v0}, Landroid/support/v4/app/ct;->c()V

    goto :goto_26
.end method

.method public b()V
    .registers 3

    .prologue
    .line 565
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_a

    .line 25029
    invoke-virtual {p0}, Landroid/app/Activity;->invalidateOptionsMenu()V

    .line 575
    :goto_9
    return-void

    .line 574
    :cond_a
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/app/bb;->k:Z

    goto :goto_9
.end method

.method public final c()Landroid/support/v4/app/bi;
    .registers 2

    .prologue
    .line 732
    iget-object v0, p0, Landroid/support/v4/app/bb;->e:Landroid/support/v4/app/bg;

    .line 34058
    iget-object v0, v0, Landroid/support/v4/app/bg;->a:Landroid/support/v4/app/bh;

    .line 34174
    iget-object v0, v0, Landroid/support/v4/app/bh;->f:Landroid/support/v4/app/bl;

    .line 732
    return-object v0
.end method

.method public dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 8

    .prologue
    .line 588
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 592
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "Local FragmentActivity "

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 593
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 594
    const-string v0, " State:"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 595
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 596
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v1, "mCreated="

    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 597
    iget-boolean v1, p0, Landroid/support/v4/app/bb;->f:Z

    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(Z)V

    const-string v1, "mResumed="

    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 598
    iget-boolean v1, p0, Landroid/support/v4/app/bb;->g:Z

    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(Z)V

    const-string v1, " mStopped="

    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 599
    iget-boolean v1, p0, Landroid/support/v4/app/bb;->h:Z

    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(Z)V

    const-string v1, " mReallyStopped="

    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 600
    iget-boolean v1, p0, Landroid/support/v4/app/bb;->i:Z

    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->println(Z)V

    .line 601
    iget-object v1, p0, Landroid/support/v4/app/bb;->e:Landroid/support/v4/app/bg;

    .line 25394
    iget-object v1, v1, Landroid/support/v4/app/bg;->a:Landroid/support/v4/app/bh;

    .line 26313
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v2, "mLoadersStarted="

    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 26314
    iget-boolean v2, v1, Landroid/support/v4/app/bh;->j:Z

    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->println(Z)V

    .line 26315
    iget-object v2, v1, Landroid/support/v4/app/bh;->h:Landroid/support/v4/app/ct;

    if-eqz v2, :cond_9f

    .line 26316
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v2, "Loader Manager "

    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 26317
    iget-object v2, v1, Landroid/support/v4/app/bh;->h:Landroid/support/v4/app/ct;

    invoke-static {v2}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 26318
    const-string v2, ":"

    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 26319
    iget-object v1, v1, Landroid/support/v4/app/bh;->h:Landroid/support/v4/app/ct;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "  "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0, p2, p3, p4}, Landroid/support/v4/app/ct;->a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 602
    :cond_9f
    iget-object v0, p0, Landroid/support/v4/app/bb;->e:Landroid/support/v4/app/bg;

    .line 27058
    iget-object v0, v0, Landroid/support/v4/app/bg;->a:Landroid/support/v4/app/bh;

    .line 27174
    iget-object v0, v0, Landroid/support/v4/app/bh;->f:Landroid/support/v4/app/bl;

    .line 602
    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/support/v4/app/bi;->a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 603
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "View Hierarchy:"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 604
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Landroid/support/v4/app/bb;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-direct {p0, v0, p3, v1}, Landroid/support/v4/app/bb;->a(Ljava/lang/String;Ljava/io/PrintWriter;Landroid/view/View;)V

    .line 605
    return-void
.end method

.method protected final o_()V
    .registers 2

    .prologue
    .line 426
    iget-object v0, p0, Landroid/support/v4/app/bb;->e:Landroid/support/v4/app/bg;

    .line 15187
    iget-object v0, v0, Landroid/support/v4/app/bg;->a:Landroid/support/v4/app/bh;

    iget-object v0, v0, Landroid/support/v4/app/bh;->f:Landroid/support/v4/app/bl;

    invoke-virtual {v0}, Landroid/support/v4/app/bl;->q()V

    .line 427
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .registers 8

    .prologue
    .line 136
    iget-object v0, p0, Landroid/support/v4/app/bb;->e:Landroid/support/v4/app/bg;

    invoke-virtual {v0}, Landroid/support/v4/app/bg;->b()V

    .line 137
    shr-int/lit8 v0, p1, 0x10

    .line 138
    if-eqz v0, :cond_60

    .line 139
    add-int/lit8 v0, v0, -0x1

    .line 140
    iget-object v1, p0, Landroid/support/v4/app/bb;->e:Landroid/support/v4/app/bg;

    invoke-virtual {v1}, Landroid/support/v4/app/bg;->a()I

    move-result v1

    .line 141
    if-eqz v1, :cond_17

    if-ltz v0, :cond_17

    if-lt v0, v1, :cond_30

    .line 142
    :cond_17
    const-string v0, "FragmentActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Activity result fragment index out of range: 0x"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 159
    :goto_2f
    return-void

    .line 146
    :cond_30
    iget-object v2, p0, Landroid/support/v4/app/bb;->e:Landroid/support/v4/app/bg;

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v1}, Ljava/util/ArrayList;-><init>(I)V

    invoke-virtual {v2, v3}, Landroid/support/v4/app/bg;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    .line 148
    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 149
    if-nez v0, :cond_5c

    .line 150
    const-string v0, "FragmentActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Activity result no fragment exists for index: 0x"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2f

    .line 153
    :cond_5c
    invoke-static {}, Landroid/support/v4/app/Fragment;->o()V

    goto :goto_2f

    .line 158
    :cond_60
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/as;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_2f
.end method

.method public onBackPressed()V
    .registers 3

    .prologue
    .line 166
    iget-object v0, p0, Landroid/support/v4/app/bb;->e:Landroid/support/v4/app/bg;

    .line 2058
    iget-object v0, v0, Landroid/support/v4/app/bg;->a:Landroid/support/v4/app/bh;

    .line 2174
    iget-object v0, v0, Landroid/support/v4/app/bh;->f:Landroid/support/v4/app/bl;

    .line 166
    invoke-virtual {v0}, Landroid/support/v4/app/bi;->d()Z

    move-result v0

    if-nez v0, :cond_15

    .line 2193
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_16

    .line 3035
    invoke-virtual {p0}, Landroid/app/Activity;->finishAfterTransition()V

    .line 2194
    :cond_15
    :goto_15
    return-void

    .line 2196
    :cond_16
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto :goto_15
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .registers 3

    .prologue
    .line 230
    invoke-super {p0, p1}, Landroid/support/v4/app/as;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 231
    iget-object v0, p0, Landroid/support/v4/app/bb;->e:Landroid/support/v4/app/bg;

    .line 8246
    iget-object v0, v0, Landroid/support/v4/app/bg;->a:Landroid/support/v4/app/bh;

    iget-object v0, v0, Landroid/support/v4/app/bh;->f:Landroid/support/v4/app/bl;

    invoke-virtual {v0, p1}, Landroid/support/v4/app/bl;->a(Landroid/content/res/Configuration;)V

    .line 232
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 6
    .param p1    # Landroid/os/Bundle;
        .annotation build Landroid/support/a/z;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 240
    iget-object v0, p0, Landroid/support/v4/app/bb;->e:Landroid/support/v4/app/bg;

    .line 9095
    iget-object v2, v0, Landroid/support/v4/app/bg;->a:Landroid/support/v4/app/bh;

    iget-object v2, v2, Landroid/support/v4/app/bh;->f:Landroid/support/v4/app/bl;

    iget-object v3, v0, Landroid/support/v4/app/bg;->a:Landroid/support/v4/app/bh;

    iget-object v0, v0, Landroid/support/v4/app/bg;->a:Landroid/support/v4/app/bh;

    invoke-virtual {v2, v3, v0, v1}, Landroid/support/v4/app/bl;->a(Landroid/support/v4/app/bh;Landroid/support/v4/app/bf;Landroid/support/v4/app/Fragment;)V

    .line 242
    invoke-super {p0, p1}, Landroid/support/v4/app/as;->onCreate(Landroid/os/Bundle;)V

    .line 244
    invoke-virtual {p0}, Landroid/support/v4/app/bb;->getLastNonConfigurationInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/be;

    .line 246
    if-eqz v0, :cond_21

    .line 247
    iget-object v2, p0, Landroid/support/v4/app/bb;->e:Landroid/support/v4/app/bg;

    iget-object v3, v0, Landroid/support/v4/app/be;->c:Landroid/support/v4/n/v;

    .line 9387
    iget-object v2, v2, Landroid/support/v4/app/bg;->a:Landroid/support/v4/app/bh;

    .line 10309
    iput-object v3, v2, Landroid/support/v4/app/bh;->g:Landroid/support/v4/n/v;

    .line 249
    :cond_21
    if-eqz p1, :cond_36

    .line 250
    const-string v2, "android:support:fragments"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    .line 251
    iget-object v3, p0, Landroid/support/v4/app/bb;->e:Landroid/support/v4/app/bg;

    if-eqz v0, :cond_40

    iget-object v0, v0, Landroid/support/v4/app/be;->b:Ljava/util/List;

    .line 11135
    :goto_2f
    iget-object v1, v3, Landroid/support/v4/app/bg;->a:Landroid/support/v4/app/bh;

    iget-object v1, v1, Landroid/support/v4/app/bh;->f:Landroid/support/v4/app/bl;

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/app/bl;->a(Landroid/os/Parcelable;Ljava/util/List;)V

    .line 253
    :cond_36
    iget-object v0, p0, Landroid/support/v4/app/bb;->e:Landroid/support/v4/app/bg;

    .line 11154
    iget-object v0, v0, Landroid/support/v4/app/bg;->a:Landroid/support/v4/app/bh;

    iget-object v0, v0, Landroid/support/v4/app/bh;->f:Landroid/support/v4/app/bl;

    invoke-virtual {v0}, Landroid/support/v4/app/bl;->n()V

    .line 254
    return-void

    :cond_40
    move-object v0, v1

    .line 251
    goto :goto_2f
.end method

.method public onCreatePanelMenu(ILandroid/view/Menu;)Z
    .registers 6

    .prologue
    .line 261
    if-nez p1, :cond_1e

    .line 262
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/as;->onCreatePanelMenu(ILandroid/view/Menu;)Z

    move-result v0

    .line 263
    iget-object v1, p0, Landroid/support/v4/app/bb;->e:Landroid/support/v4/app/bg;

    invoke-virtual {p0}, Landroid/support/v4/app/bb;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v2

    .line 11270
    iget-object v1, v1, Landroid/support/v4/app/bg;->a:Landroid/support/v4/app/bh;

    iget-object v1, v1, Landroid/support/v4/app/bh;->f:Landroid/support/v4/app/bl;

    invoke-virtual {v1, p2, v2}, Landroid/support/v4/app/bl;->a(Landroid/view/Menu;Landroid/view/MenuInflater;)Z

    move-result v1

    .line 263
    or-int/2addr v0, v1

    .line 264
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-lt v1, v2, :cond_1c

    .line 272
    :goto_1b
    return v0

    .line 270
    :cond_1c
    const/4 v0, 0x1

    goto :goto_1b

    .line 272
    :cond_1e
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/as;->onCreatePanelMenu(ILandroid/view/Menu;)Z

    move-result v0

    goto :goto_1b
.end method

.method public bridge synthetic onCreateView(Landroid/view/View;Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;
    .registers 6

    .prologue
    .line 78
    invoke-super {p0, p1, p2, p3, p4}, Landroid/support/v4/app/as;->onCreateView(Landroid/view/View;Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic onCreateView(Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;
    .registers 5

    .prologue
    .line 78
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/as;->onCreateView(Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onDestroy()V
    .registers 3

    .prologue
    .line 286
    invoke-super {p0}, Landroid/support/v4/app/as;->onDestroy()V

    .line 288
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/support/v4/app/bb;->a(Z)V

    .line 290
    iget-object v0, p0, Landroid/support/v4/app/bb;->e:Landroid/support/v4/app/bg;

    .line 12235
    iget-object v0, v0, Landroid/support/v4/app/bg;->a:Landroid/support/v4/app/bh;

    iget-object v0, v0, Landroid/support/v4/app/bh;->f:Landroid/support/v4/app/bl;

    invoke-virtual {v0}, Landroid/support/v4/app/bl;->s()V

    .line 291
    iget-object v0, p0, Landroid/support/v4/app/bb;->e:Landroid/support/v4/app/bg;

    .line 12362
    iget-object v0, v0, Landroid/support/v4/app/bg;->a:Landroid/support/v4/app/bh;

    .line 13244
    iget-object v1, v0, Landroid/support/v4/app/bh;->h:Landroid/support/v4/app/ct;

    if-eqz v1, :cond_1d

    .line 13247
    iget-object v0, v0, Landroid/support/v4/app/bh;->h:Landroid/support/v4/app/ct;

    invoke-virtual {v0}, Landroid/support/v4/app/ct;->g()V

    .line 292
    :cond_1d
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .registers 5

    .prologue
    .line 299
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/4 v1, 0x5

    if-ge v0, v1, :cond_13

    const/4 v0, 0x4

    if-ne p1, v0, :cond_13

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v0

    if-nez v0, :cond_13

    .line 304
    invoke-virtual {p0}, Landroid/support/v4/app/bb;->onBackPressed()V

    .line 305
    const/4 v0, 0x1

    .line 308
    :goto_12
    return v0

    :cond_13
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/as;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_12
.end method

.method public onLowMemory()V
    .registers 2

    .prologue
    .line 316
    invoke-super {p0}, Landroid/support/v4/app/as;->onLowMemory()V

    .line 317
    iget-object v0, p0, Landroid/support/v4/app/bb;->e:Landroid/support/v4/app/bg;

    .line 13258
    iget-object v0, v0, Landroid/support/v4/app/bg;->a:Landroid/support/v4/app/bh;

    iget-object v0, v0, Landroid/support/v4/app/bh;->f:Landroid/support/v4/app/bl;

    invoke-virtual {v0}, Landroid/support/v4/app/bl;->t()V

    .line 318
    return-void
.end method

.method public onMenuItemSelected(ILandroid/view/MenuItem;)Z
    .registers 4

    .prologue
    .line 325
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/as;->onMenuItemSelected(ILandroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 326
    const/4 v0, 0x1

    .line 337
    :goto_7
    return v0

    .line 329
    :cond_8
    sparse-switch p1, :sswitch_data_24

    .line 337
    const/4 v0, 0x0

    goto :goto_7

    .line 331
    :sswitch_d
    iget-object v0, p0, Landroid/support/v4/app/bb;->e:Landroid/support/v4/app/bg;

    .line 13295
    iget-object v0, v0, Landroid/support/v4/app/bg;->a:Landroid/support/v4/app/bh;

    iget-object v0, v0, Landroid/support/v4/app/bh;->f:Landroid/support/v4/app/bl;

    invoke-virtual {v0, p2}, Landroid/support/v4/app/bl;->a(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_7

    .line 334
    :sswitch_18
    iget-object v0, p0, Landroid/support/v4/app/bb;->e:Landroid/support/v4/app/bg;

    .line 13308
    iget-object v0, v0, Landroid/support/v4/app/bg;->a:Landroid/support/v4/app/bh;

    iget-object v0, v0, Landroid/support/v4/app/bh;->f:Landroid/support/v4/app/bl;

    invoke-virtual {v0, p2}, Landroid/support/v4/app/bl;->b(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_7

    .line 329
    nop

    :sswitch_data_24
    .sparse-switch
        0x0 -> :sswitch_d
        0x6 -> :sswitch_18
    .end sparse-switch
.end method

.method public onNewIntent(Landroid/content/Intent;)V
    .registers 3

    .prologue
    .line 380
    invoke-super {p0, p1}, Landroid/support/v4/app/as;->onNewIntent(Landroid/content/Intent;)V

    .line 381
    iget-object v0, p0, Landroid/support/v4/app/bb;->e:Landroid/support/v4/app/bg;

    invoke-virtual {v0}, Landroid/support/v4/app/bg;->b()V

    .line 382
    return-void
.end method

.method public onPanelClosed(ILandroid/view/Menu;)V
    .registers 4

    .prologue
    .line 346
    packed-switch p1, :pswitch_data_12

    .line 351
    :goto_3
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/as;->onPanelClosed(ILandroid/view/Menu;)V

    .line 352
    return-void

    .line 348
    :pswitch_7
    iget-object v0, p0, Landroid/support/v4/app/bb;->e:Landroid/support/v4/app/bg;

    .line 13319
    iget-object v0, v0, Landroid/support/v4/app/bg;->a:Landroid/support/v4/app/bh;

    iget-object v0, v0, Landroid/support/v4/app/bh;->f:Landroid/support/v4/app/bl;

    invoke-virtual {v0, p2}, Landroid/support/v4/app/bl;->b(Landroid/view/Menu;)V

    goto :goto_3

    .line 346
    nop

    :pswitch_data_12
    .packed-switch 0x0
        :pswitch_7
    .end packed-switch
.end method

.method public onPause()V
    .registers 3

    .prologue
    const/4 v1, 0x2

    .line 359
    invoke-super {p0}, Landroid/support/v4/app/as;->onPause()V

    .line 360
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v4/app/bb;->g:Z

    .line 361
    iget-object v0, p0, Landroid/support/v4/app/bb;->d:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 362
    iget-object v0, p0, Landroid/support/v4/app/bb;->d:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 363
    invoke-virtual {p0}, Landroid/support/v4/app/bb;->o_()V

    .line 365
    :cond_17
    iget-object v0, p0, Landroid/support/v4/app/bb;->e:Landroid/support/v4/app/bg;

    .line 14198
    iget-object v0, v0, Landroid/support/v4/app/bg;->a:Landroid/support/v4/app/bh;

    iget-object v0, v0, Landroid/support/v4/app/bh;->f:Landroid/support/v4/app/bl;

    .line 15015
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/support/v4/app/bl;->c(I)V

    .line 366
    return-void
.end method

.method public onPostResume()V
    .registers 3

    .prologue
    .line 413
    invoke-super {p0}, Landroid/support/v4/app/as;->onPostResume()V

    .line 414
    iget-object v0, p0, Landroid/support/v4/app/bb;->d:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 415
    invoke-virtual {p0}, Landroid/support/v4/app/bb;->o_()V

    .line 416
    iget-object v0, p0, Landroid/support/v4/app/bb;->e:Landroid/support/v4/app/bg;

    invoke-virtual {v0}, Landroid/support/v4/app/bg;->c()Z

    .line 417
    return-void
.end method

.method public onPreparePanel(ILandroid/view/View;Landroid/view/Menu;)Z
    .registers 6

    .prologue
    const/4 v1, 0x0

    .line 434
    if-nez p1, :cond_21

    if-eqz p3, :cond_21

    .line 435
    iget-boolean v0, p0, Landroid/support/v4/app/bb;->k:Z

    if-eqz v0, :cond_11

    .line 436
    iput-boolean v1, p0, Landroid/support/v4/app/bb;->k:Z

    .line 437
    invoke-interface {p3}, Landroid/view/Menu;->clear()V

    .line 438
    invoke-virtual {p0, p1, p3}, Landroid/support/v4/app/bb;->onCreatePanelMenu(ILandroid/view/Menu;)Z

    .line 15451
    :cond_11
    invoke-super {p0, v1, p2, p3}, Landroid/support/v4/app/as;->onPreparePanel(ILandroid/view/View;Landroid/view/Menu;)Z

    move-result v0

    .line 441
    iget-object v1, p0, Landroid/support/v4/app/bb;->e:Landroid/support/v4/app/bg;

    .line 16282
    iget-object v1, v1, Landroid/support/v4/app/bg;->a:Landroid/support/v4/app/bh;

    iget-object v1, v1, Landroid/support/v4/app/bh;->f:Landroid/support/v4/app/bl;

    invoke-virtual {v1, p3}, Landroid/support/v4/app/bl;->a(Landroid/view/Menu;)Z

    move-result v1

    .line 441
    or-int/2addr v0, v1

    .line 444
    :goto_20
    return v0

    :cond_21
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/as;->onPreparePanel(ILandroid/view/View;Landroid/view/Menu;)Z

    move-result v0

    goto :goto_20
.end method

.method public onRequestPermissionsResult(I[Ljava/lang/String;[I)V
    .registers 8
    .param p2    # [Ljava/lang/String;
        .annotation build Landroid/support/a/y;
        .end annotation
    .end param
    .param p3    # [I
        .annotation build Landroid/support/a/y;
        .end annotation
    .end param

    .prologue
    .line 786
    shr-int/lit8 v0, p1, 0x8

    and-int/lit16 v0, v0, 0xff

    .line 787
    if-eqz v0, :cond_2c

    .line 788
    add-int/lit8 v0, v0, -0x1

    .line 789
    iget-object v1, p0, Landroid/support/v4/app/bb;->e:Landroid/support/v4/app/bg;

    invoke-virtual {v1}, Landroid/support/v4/app/bg;->a()I

    move-result v1

    .line 790
    if-eqz v1, :cond_14

    if-ltz v0, :cond_14

    if-lt v0, v1, :cond_2d

    .line 791
    :cond_14
    const-string v0, "FragmentActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Activity result fragment index out of range: 0x"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 805
    :cond_2c
    :goto_2c
    return-void

    .line 795
    :cond_2d
    iget-object v2, p0, Landroid/support/v4/app/bb;->e:Landroid/support/v4/app/bg;

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v1}, Ljava/util/ArrayList;-><init>(I)V

    invoke-virtual {v2, v3}, Landroid/support/v4/app/bg;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    .line 797
    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 798
    if-nez v0, :cond_59

    .line 799
    const-string v0, "FragmentActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Activity result no fragment exists for index: 0x"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2c

    .line 802
    :cond_59
    invoke-static {}, Landroid/support/v4/app/Fragment;->p()V

    goto :goto_2c
.end method

.method public onResume()V
    .registers 3

    .prologue
    .line 402
    invoke-super {p0}, Landroid/support/v4/app/as;->onResume()V

    .line 403
    iget-object v0, p0, Landroid/support/v4/app/bb;->d:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 404
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/app/bb;->g:Z

    .line 405
    iget-object v0, p0, Landroid/support/v4/app/bb;->e:Landroid/support/v4/app/bg;

    invoke-virtual {v0}, Landroid/support/v4/app/bg;->c()Z

    .line 406
    return-void
.end method

.method public final onRetainNonConfigurationInstance()Ljava/lang/Object;
    .registers 10

    .prologue
    const/4 v8, 0x1

    const/4 v4, 0x0

    .line 461
    iget-boolean v0, p0, Landroid/support/v4/app/bb;->h:Z

    if-eqz v0, :cond_9

    .line 462
    invoke-virtual {p0, v8}, Landroid/support/v4/app/bb;->a(Z)V

    .line 467
    :cond_9
    iget-object v0, p0, Landroid/support/v4/app/bb;->e:Landroid/support/v4/app/bg;

    .line 17143
    iget-object v0, v0, Landroid/support/v4/app/bg;->a:Landroid/support/v4/app/bh;

    iget-object v5, v0, Landroid/support/v4/app/bh;->f:Landroid/support/v4/app/bl;

    .line 17687
    iget-object v0, v5, Landroid/support/v4/app/bl;->l:Ljava/util/ArrayList;

    if-eqz v0, :cond_60

    .line 17688
    const/4 v0, 0x0

    move v3, v0

    move-object v1, v4

    :goto_16
    iget-object v0, v5, Landroid/support/v4/app/bl;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v3, v0, :cond_61

    .line 17689
    iget-object v0, v5, Landroid/support/v4/app/bl;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 17690
    if-eqz v0, :cond_5a

    iget-boolean v2, v0, Landroid/support/v4/app/Fragment;->V:Z

    if-eqz v2, :cond_5a

    .line 17691
    if-nez v1, :cond_33

    .line 17692
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 17694
    :cond_33
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 17695
    iput-boolean v8, v0, Landroid/support/v4/app/Fragment;->W:Z

    .line 17696
    iget-object v2, v0, Landroid/support/v4/app/Fragment;->C:Landroid/support/v4/app/Fragment;

    if-eqz v2, :cond_5e

    iget-object v2, v0, Landroid/support/v4/app/Fragment;->C:Landroid/support/v4/app/Fragment;

    iget v2, v2, Landroid/support/v4/app/Fragment;->z:I

    :goto_40
    iput v2, v0, Landroid/support/v4/app/Fragment;->D:I

    .line 17697
    sget-boolean v2, Landroid/support/v4/app/bl;->b:Z

    if-eqz v2, :cond_5a

    const-string v2, "FragmentManager"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "retainNonConfig: keeping retained "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 17688
    :cond_5a
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_16

    .line 17696
    :cond_5e
    const/4 v2, -0x1

    goto :goto_40

    :cond_60
    move-object v1, v4

    .line 468
    :cond_61
    iget-object v0, p0, Landroid/support/v4/app/bb;->e:Landroid/support/v4/app/bg;

    .line 18377
    iget-object v0, v0, Landroid/support/v4/app/bg;->a:Landroid/support/v4/app/bh;

    invoke-virtual {v0}, Landroid/support/v4/app/bh;->i()Landroid/support/v4/n/v;

    move-result-object v2

    .line 470
    if-nez v1, :cond_6e

    if-nez v2, :cond_6e

    .line 478
    :goto_6d
    return-object v4

    .line 474
    :cond_6e
    new-instance v0, Landroid/support/v4/app/be;

    invoke-direct {v0}, Landroid/support/v4/app/be;-><init>()V

    .line 475
    iput-object v4, v0, Landroid/support/v4/app/be;->a:Ljava/lang/Object;

    .line 476
    iput-object v1, v0, Landroid/support/v4/app/be;->b:Ljava/util/List;

    .line 477
    iput-object v2, v0, Landroid/support/v4/app/be;->c:Landroid/support/v4/n/v;

    move-object v4, v0

    .line 478
    goto :goto_6d
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 4

    .prologue
    .line 486
    invoke-super {p0, p1}, Landroid/support/v4/app/as;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 487
    iget-object v0, p0, Landroid/support/v4/app/bb;->e:Landroid/support/v4/app/bg;

    .line 19125
    iget-object v0, v0, Landroid/support/v4/app/bg;->a:Landroid/support/v4/app/bh;

    iget-object v0, v0, Landroid/support/v4/app/bh;->f:Landroid/support/v4/app/bl;

    invoke-virtual {v0}, Landroid/support/v4/app/bl;->m()Landroid/os/Parcelable;

    move-result-object v0

    .line 488
    if-eqz v0, :cond_14

    .line 489
    const-string v1, "android:support:fragments"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 491
    :cond_14
    return-void
.end method

.method protected onStart()V
    .registers 11

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 499
    invoke-super {p0}, Landroid/support/v4/app/as;->onStart()V

    .line 501
    iput-boolean v2, p0, Landroid/support/v4/app/bb;->h:Z

    .line 502
    iput-boolean v2, p0, Landroid/support/v4/app/bb;->i:Z

    .line 503
    iget-object v0, p0, Landroid/support/v4/app/bb;->d:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 505
    iget-boolean v0, p0, Landroid/support/v4/app/bb;->f:Z

    if-nez v0, :cond_1d

    .line 506
    iput-boolean v4, p0, Landroid/support/v4/app/bb;->f:Z

    .line 507
    iget-object v0, p0, Landroid/support/v4/app/bb;->e:Landroid/support/v4/app/bg;

    .line 19165
    iget-object v0, v0, Landroid/support/v4/app/bg;->a:Landroid/support/v4/app/bh;

    iget-object v0, v0, Landroid/support/v4/app/bh;->f:Landroid/support/v4/app/bl;

    invoke-virtual {v0}, Landroid/support/v4/app/bl;->o()V

    .line 510
    :cond_1d
    iget-object v0, p0, Landroid/support/v4/app/bb;->e:Landroid/support/v4/app/bg;

    invoke-virtual {v0}, Landroid/support/v4/app/bg;->b()V

    .line 511
    iget-object v0, p0, Landroid/support/v4/app/bb;->e:Landroid/support/v4/app/bg;

    invoke-virtual {v0}, Landroid/support/v4/app/bg;->c()Z

    .line 513
    iget-object v0, p0, Landroid/support/v4/app/bb;->e:Landroid/support/v4/app/bg;

    .line 19337
    iget-object v0, v0, Landroid/support/v4/app/bg;->a:Landroid/support/v4/app/bh;

    .line 20201
    iget-boolean v1, v0, Landroid/support/v4/app/bh;->j:Z

    if-nez v1, :cond_3c

    .line 20204
    iput-boolean v4, v0, Landroid/support/v4/app/bh;->j:Z

    .line 20206
    iget-object v1, v0, Landroid/support/v4/app/bh;->h:Landroid/support/v4/app/ct;

    if-eqz v1, :cond_68

    .line 20207
    iget-object v1, v0, Landroid/support/v4/app/bh;->h:Landroid/support/v4/app/ct;

    invoke-virtual {v1}, Landroid/support/v4/app/ct;->b()V

    .line 20215
    :cond_3a
    :goto_3a
    iput-boolean v4, v0, Landroid/support/v4/app/bh;->i:Z

    .line 517
    :cond_3c
    iget-object v0, p0, Landroid/support/v4/app/bb;->e:Landroid/support/v4/app/bg;

    .line 21176
    iget-object v0, v0, Landroid/support/v4/app/bg;->a:Landroid/support/v4/app/bh;

    iget-object v0, v0, Landroid/support/v4/app/bh;->f:Landroid/support/v4/app/bl;

    invoke-virtual {v0}, Landroid/support/v4/app/bl;->p()V

    .line 518
    iget-object v0, p0, Landroid/support/v4/app/bb;->e:Landroid/support/v4/app/bg;

    .line 21369
    iget-object v3, v0, Landroid/support/v4/app/bg;->a:Landroid/support/v4/app/bh;

    .line 22251
    iget-object v0, v3, Landroid/support/v4/app/bh;->g:Landroid/support/v4/n/v;

    if-eqz v0, :cond_105

    .line 22252
    iget-object v0, v3, Landroid/support/v4/app/bh;->g:Landroid/support/v4/n/v;

    invoke-virtual {v0}, Landroid/support/v4/n/v;->size()I

    move-result v4

    .line 22253
    new-array v5, v4, [Landroid/support/v4/app/ct;

    .line 22254
    add-int/lit8 v0, v4, -0x1

    move v1, v0

    :goto_58
    if-ltz v1, :cond_86

    .line 22255
    iget-object v0, v3, Landroid/support/v4/app/bh;->g:Landroid/support/v4/n/v;

    invoke-virtual {v0, v1}, Landroid/support/v4/n/v;->c(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/ct;

    aput-object v0, v5, v1

    .line 22254
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_58

    .line 20208
    :cond_68
    iget-boolean v1, v0, Landroid/support/v4/app/bh;->i:Z

    if-nez v1, :cond_3a

    .line 20209
    const-string v1, "(root)"

    iget-boolean v3, v0, Landroid/support/v4/app/bh;->j:Z

    invoke-virtual {v0, v1, v3, v2}, Landroid/support/v4/app/bh;->a(Ljava/lang/String;ZZ)Landroid/support/v4/app/ct;

    move-result-object v1

    iput-object v1, v0, Landroid/support/v4/app/bh;->h:Landroid/support/v4/app/ct;

    .line 20211
    iget-object v1, v0, Landroid/support/v4/app/bh;->h:Landroid/support/v4/app/ct;

    if-eqz v1, :cond_3a

    iget-object v1, v0, Landroid/support/v4/app/bh;->h:Landroid/support/v4/app/ct;

    iget-boolean v1, v1, Landroid/support/v4/app/ct;->f:Z

    if-nez v1, :cond_3a

    .line 20212
    iget-object v1, v0, Landroid/support/v4/app/bh;->h:Landroid/support/v4/app/ct;

    invoke-virtual {v1}, Landroid/support/v4/app/ct;->b()V

    goto :goto_3a

    :cond_86
    move v1, v2

    .line 22257
    :goto_87
    if-ge v1, v4, :cond_105

    .line 22258
    aget-object v6, v5, v1

    .line 22801
    iget-boolean v0, v6, Landroid/support/v4/app/ct;->g:Z

    if-eqz v0, :cond_fe

    .line 22802
    sget-boolean v0, Landroid/support/v4/app/ct;->b:Z

    if-eqz v0, :cond_a7

    const-string v0, "LoaderManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v7, "Finished Retaining in "

    invoke-direct {v3, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 22804
    :cond_a7
    iput-boolean v2, v6, Landroid/support/v4/app/ct;->g:Z

    .line 22805
    iget-object v0, v6, Landroid/support/v4/app/ct;->c:Landroid/support/v4/n/w;

    invoke-virtual {v0}, Landroid/support/v4/n/w;->a()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v3, v0

    :goto_b2
    if-ltz v3, :cond_fe

    .line 22806
    iget-object v0, v6, Landroid/support/v4/app/ct;->c:Landroid/support/v4/n/w;

    invoke-virtual {v0, v3}, Landroid/support/v4/n/w;->d(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/cu;

    .line 23286
    iget-boolean v7, v0, Landroid/support/v4/app/cu;->i:Z

    if-eqz v7, :cond_e7

    .line 23287
    sget-boolean v7, Landroid/support/v4/app/ct;->b:Z

    if-eqz v7, :cond_d8

    const-string v7, "LoaderManager"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "  Finished Retaining: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 23288
    :cond_d8
    iput-boolean v2, v0, Landroid/support/v4/app/cu;->i:Z

    .line 23289
    iget-boolean v7, v0, Landroid/support/v4/app/cu;->h:Z

    iget-boolean v8, v0, Landroid/support/v4/app/cu;->j:Z

    if-eq v7, v8, :cond_e7

    .line 23290
    iget-boolean v7, v0, Landroid/support/v4/app/cu;->h:Z

    if-nez v7, :cond_e7

    .line 23294
    invoke-virtual {v0}, Landroid/support/v4/app/cu;->b()V

    .line 23299
    :cond_e7
    iget-boolean v7, v0, Landroid/support/v4/app/cu;->h:Z

    if-eqz v7, :cond_fa

    iget-boolean v7, v0, Landroid/support/v4/app/cu;->e:Z

    if-eqz v7, :cond_fa

    iget-boolean v7, v0, Landroid/support/v4/app/cu;->k:Z

    if-nez v7, :cond_fa

    .line 23306
    iget-object v7, v0, Landroid/support/v4/app/cu;->d:Landroid/support/v4/c/aa;

    iget-object v8, v0, Landroid/support/v4/app/cu;->g:Ljava/lang/Object;

    invoke-virtual {v0, v7, v8}, Landroid/support/v4/app/cu;->b(Landroid/support/v4/c/aa;Ljava/lang/Object;)V

    .line 22805
    :cond_fa
    add-int/lit8 v0, v3, -0x1

    move v3, v0

    goto :goto_b2

    .line 22260
    :cond_fe
    invoke-virtual {v6}, Landroid/support/v4/app/ct;->f()V

    .line 22257
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_87

    .line 519
    :cond_105
    return-void
.end method

.method public onStateNotSaved()V
    .registers 2

    .prologue
    .line 388
    iget-object v0, p0, Landroid/support/v4/app/bb;->e:Landroid/support/v4/app/bg;

    invoke-virtual {v0}, Landroid/support/v4/app/bg;->b()V

    .line 389
    return-void
.end method

.method public onStop()V
    .registers 3

    .prologue
    const/4 v1, 0x1

    .line 526
    invoke-super {p0}, Landroid/support/v4/app/as;->onStop()V

    .line 528
    iput-boolean v1, p0, Landroid/support/v4/app/bb;->h:Z

    .line 529
    iget-object v0, p0, Landroid/support/v4/app/bb;->d:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 531
    iget-object v0, p0, Landroid/support/v4/app/bb;->e:Landroid/support/v4/app/bg;

    .line 24209
    iget-object v0, v0, Landroid/support/v4/app/bg;->a:Landroid/support/v4/app/bh;

    iget-object v0, v0, Landroid/support/v4/app/bh;->f:Landroid/support/v4/app/bl;

    invoke-virtual {v0}, Landroid/support/v4/app/bl;->r()V

    .line 532
    return-void
.end method

.method public startActivityForResult(Landroid/content/Intent;I)V
    .registers 5

    .prologue
    .line 745
    const/4 v0, -0x1

    if-eq p2, v0, :cond_10

    const/high16 v0, -0x10000

    and-int/2addr v0, p2

    if-eqz v0, :cond_10

    .line 746
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Can only use lower 16 bits for requestCode"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 748
    :cond_10
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/as;->startActivityForResult(Landroid/content/Intent;I)V

    .line 749
    return-void
.end method
