.class final Landroid/support/v4/j/j;
.super Landroid/print/PrintDocumentAdapter;
.source "SourceFile"


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Landroid/graphics/Bitmap;

.field final synthetic c:I

.field final synthetic d:Landroid/support/v4/j/n;

.field final synthetic e:Landroid/support/v4/j/i;

.field private f:Landroid/print/PrintAttributes;


# direct methods
.method constructor <init>(Landroid/support/v4/j/i;Ljava/lang/String;Landroid/graphics/Bitmap;ILandroid/support/v4/j/n;)V
    .registers 6

    .prologue
    .line 190
    iput-object p1, p0, Landroid/support/v4/j/j;->e:Landroid/support/v4/j/i;

    iput-object p2, p0, Landroid/support/v4/j/j;->a:Ljava/lang/String;

    iput-object p3, p0, Landroid/support/v4/j/j;->b:Landroid/graphics/Bitmap;

    iput p4, p0, Landroid/support/v4/j/j;->c:I

    iput-object p5, p0, Landroid/support/v4/j/j;->d:Landroid/support/v4/j/n;

    invoke-direct {p0}, Landroid/print/PrintDocumentAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFinish()V
    .registers 1

    .prologue
    .line 269
    return-void
.end method

.method public final onLayout(Landroid/print/PrintAttributes;Landroid/print/PrintAttributes;Landroid/os/CancellationSignal;Landroid/print/PrintDocumentAdapter$LayoutResultCallback;Landroid/os/Bundle;)V
    .registers 9

    .prologue
    const/4 v0, 0x1

    .line 200
    iput-object p2, p0, Landroid/support/v4/j/j;->f:Landroid/print/PrintAttributes;

    .line 202
    new-instance v1, Landroid/print/PrintDocumentInfo$Builder;

    iget-object v2, p0, Landroid/support/v4/j/j;->a:Ljava/lang/String;

    invoke-direct {v1, v2}, Landroid/print/PrintDocumentInfo$Builder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Landroid/print/PrintDocumentInfo$Builder;->setContentType(I)Landroid/print/PrintDocumentInfo$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/print/PrintDocumentInfo$Builder;->setPageCount(I)Landroid/print/PrintDocumentInfo$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/print/PrintDocumentInfo$Builder;->build()Landroid/print/PrintDocumentInfo;

    move-result-object v1

    .line 206
    invoke-virtual {p2, p1}, Landroid/print/PrintAttributes;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_20

    .line 207
    :goto_1c
    invoke-virtual {p4, v1, v0}, Landroid/print/PrintDocumentAdapter$LayoutResultCallback;->onLayoutFinished(Landroid/print/PrintDocumentInfo;Z)V

    .line 208
    return-void

    .line 206
    :cond_20
    const/4 v0, 0x0

    goto :goto_1c
.end method

.method public final onWrite([Landroid/print/PageRange;Landroid/os/ParcelFileDescriptor;Landroid/os/CancellationSignal;Landroid/print/PrintDocumentAdapter$WriteResultCallback;)V
    .registers 12

    .prologue
    .line 214
    new-instance v1, Landroid/print/pdf/PrintedPdfDocument;

    iget-object v0, p0, Landroid/support/v4/j/j;->e:Landroid/support/v4/j/i;

    iget-object v0, v0, Landroid/support/v4/j/i;->a:Landroid/content/Context;

    iget-object v2, p0, Landroid/support/v4/j/j;->f:Landroid/print/PrintAttributes;

    invoke-direct {v1, v0, v2}, Landroid/print/pdf/PrintedPdfDocument;-><init>(Landroid/content/Context;Landroid/print/PrintAttributes;)V

    .line 217
    iget-object v0, p0, Landroid/support/v4/j/j;->b:Landroid/graphics/Bitmap;

    iget-object v2, p0, Landroid/support/v4/j/j;->f:Landroid/print/PrintAttributes;

    invoke-virtual {v2}, Landroid/print/PrintAttributes;->getColorMode()I

    move-result v2

    invoke-static {v0, v2}, Landroid/support/v4/j/i;->a(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 220
    const/4 v0, 0x1

    :try_start_18
    invoke-virtual {v1, v0}, Landroid/print/pdf/PrintedPdfDocument;->startPage(I)Landroid/graphics/pdf/PdfDocument$Page;

    move-result-object v0

    .line 222
    new-instance v3, Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/pdf/PdfDocument$Page;->getInfo()Landroid/graphics/pdf/PdfDocument$PageInfo;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/pdf/PdfDocument$PageInfo;->getContentRect()Landroid/graphics/Rect;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/graphics/RectF;-><init>(Landroid/graphics/Rect;)V

    .line 224
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    iget v6, p0, Landroid/support/v4/j/j;->c:I

    invoke-static {v4, v5, v3, v6}, Landroid/support/v4/j/i;->a(IILandroid/graphics/RectF;I)Landroid/graphics/Matrix;

    move-result-object v3

    .line 229
    invoke-virtual {v0}, Landroid/graphics/pdf/PdfDocument$Page;->getCanvas()Landroid/graphics/Canvas;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v2, v3, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    .line 232
    invoke-virtual {v1, v0}, Landroid/print/pdf/PrintedPdfDocument;->finishPage(Landroid/graphics/pdf/PdfDocument$Page;)V
    :try_end_42
    .catchall {:try_start_18 .. :try_end_42} :catchall_76

    .line 236
    :try_start_42
    new-instance v0, Ljava/io/FileOutputStream;

    invoke-virtual {p2}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/FileDescriptor;)V

    invoke-virtual {v1, v0}, Landroid/print/pdf/PrintedPdfDocument;->writeTo(Ljava/io/OutputStream;)V

    .line 239
    const/4 v0, 0x1

    new-array v0, v0, [Landroid/print/PageRange;

    const/4 v3, 0x0

    sget-object v4, Landroid/print/PageRange;->ALL_PAGES:Landroid/print/PageRange;

    aput-object v4, v0, v3

    invoke-virtual {p4, v0}, Landroid/print/PrintDocumentAdapter$WriteResultCallback;->onWriteFinished([Landroid/print/PageRange;)V
    :try_end_59
    .catch Ljava/io/IOException; {:try_start_42 .. :try_end_59} :catch_69
    .catchall {:try_start_42 .. :try_end_59} :catchall_76

    .line 248
    :goto_59
    invoke-virtual {v1}, Landroid/print/pdf/PrintedPdfDocument;->close()V

    .line 250
    if-eqz p2, :cond_61

    .line 252
    :try_start_5e
    invoke-virtual {p2}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_61
    .catch Ljava/io/IOException; {:try_start_5e .. :try_end_61} :catch_87

    .line 258
    :cond_61
    :goto_61
    iget-object v0, p0, Landroid/support/v4/j/j;->b:Landroid/graphics/Bitmap;

    if-eq v2, v0, :cond_68

    .line 259
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    .line 262
    :cond_68
    return-void

    .line 241
    :catch_69
    move-exception v0

    .line 243
    :try_start_6a
    const-string v3, "PrintHelperKitkat"

    const-string v4, "Error writing printed content"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 244
    const/4 v0, 0x0

    invoke-virtual {p4, v0}, Landroid/print/PrintDocumentAdapter$WriteResultCallback;->onWriteFailed(Ljava/lang/CharSequence;)V
    :try_end_75
    .catchall {:try_start_6a .. :try_end_75} :catchall_76

    goto :goto_59

    .line 247
    :catchall_76
    move-exception v0

    .line 248
    invoke-virtual {v1}, Landroid/print/pdf/PrintedPdfDocument;->close()V

    .line 250
    if-eqz p2, :cond_7f

    .line 252
    :try_start_7c
    invoke-virtual {p2}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_7f
    .catch Ljava/io/IOException; {:try_start_7c .. :try_end_7f} :catch_89

    .line 258
    :cond_7f
    :goto_7f
    iget-object v1, p0, Landroid/support/v4/j/j;->b:Landroid/graphics/Bitmap;

    if-eq v2, v1, :cond_86

    .line 259
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    :cond_86
    throw v0

    :catch_87
    move-exception v0

    goto :goto_61

    :catch_89
    move-exception v1

    goto :goto_7f
.end method
