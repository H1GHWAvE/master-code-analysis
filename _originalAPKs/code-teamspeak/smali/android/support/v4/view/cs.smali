.class public final Landroid/support/v4/view/cs;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final a:Landroid/support/v4/view/cv;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 67
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_e

    .line 68
    new-instance v0, Landroid/support/v4/view/cu;

    invoke-direct {v0}, Landroid/support/v4/view/cu;-><init>()V

    sput-object v0, Landroid/support/v4/view/cs;->a:Landroid/support/v4/view/cv;

    .line 72
    :goto_d
    return-void

    .line 70
    :cond_e
    new-instance v0, Landroid/support/v4/view/ct;

    invoke-direct {v0}, Landroid/support/v4/view/ct;-><init>()V

    sput-object v0, Landroid/support/v4/view/cs;->a:Landroid/support/v4/view/cv;

    goto :goto_d
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    return-void
.end method

.method public static a(Landroid/view/VelocityTracker;I)F
    .registers 3

    .prologue
    .line 82
    sget-object v0, Landroid/support/v4/view/cs;->a:Landroid/support/v4/view/cv;

    invoke-interface {v0, p0, p1}, Landroid/support/v4/view/cv;->a(Landroid/view/VelocityTracker;I)F

    move-result v0

    return v0
.end method

.method public static b(Landroid/view/VelocityTracker;I)F
    .registers 3

    .prologue
    .line 91
    sget-object v0, Landroid/support/v4/view/cs;->a:Landroid/support/v4/view/cv;

    invoke-interface {v0, p0, p1}, Landroid/support/v4/view/cv;->b(Landroid/view/VelocityTracker;I)F

    move-result v0

    return v0
.end method
