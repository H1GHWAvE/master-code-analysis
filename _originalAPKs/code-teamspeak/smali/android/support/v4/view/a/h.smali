.class public final Landroid/support/v4/view/a/h;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Landroid/support/v4/view/a/l;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 125
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_e

    .line 126
    new-instance v0, Landroid/support/v4/view/a/i;

    invoke-direct {v0}, Landroid/support/v4/view/a/i;-><init>()V

    sput-object v0, Landroid/support/v4/view/a/h;->a:Landroid/support/v4/view/a/l;

    .line 130
    :goto_d
    return-void

    .line 128
    :cond_e
    new-instance v0, Landroid/support/v4/view/a/k;

    invoke-direct {v0}, Landroid/support/v4/view/a/k;-><init>()V

    sput-object v0, Landroid/support/v4/view/a/h;->a:Landroid/support/v4/view/a/l;

    goto :goto_d
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 202
    return-void
.end method

.method static synthetic a()Landroid/support/v4/view/a/l;
    .registers 1

    .prologue
    .line 31
    sget-object v0, Landroid/support/v4/view/a/h;->a:Landroid/support/v4/view/a/l;

    return-object v0
.end method

.method private static a(Landroid/view/accessibility/AccessibilityManager;I)Ljava/util/List;
    .registers 3

    .prologue
    .line 186
    sget-object v0, Landroid/support/v4/view/a/h;->a:Landroid/support/v4/view/a/l;

    invoke-interface {v0, p0, p1}, Landroid/support/v4/view/a/l;->a(Landroid/view/accessibility/AccessibilityManager;I)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/view/accessibility/AccessibilityManager;)Z
    .registers 2

    .prologue
    .line 196
    sget-object v0, Landroid/support/v4/view/a/h;->a:Landroid/support/v4/view/a/l;

    invoke-interface {v0, p0}, Landroid/support/v4/view/a/l;->b(Landroid/view/accessibility/AccessibilityManager;)Z

    move-result v0

    return v0
.end method

.method private static a(Landroid/view/accessibility/AccessibilityManager;Landroid/support/v4/view/a/m;)Z
    .registers 3

    .prologue
    .line 144
    sget-object v0, Landroid/support/v4/view/a/h;->a:Landroid/support/v4/view/a/l;

    invoke-interface {v0, p0, p1}, Landroid/support/v4/view/a/l;->a(Landroid/view/accessibility/AccessibilityManager;Landroid/support/v4/view/a/m;)Z

    move-result v0

    return v0
.end method

.method private static b(Landroid/view/accessibility/AccessibilityManager;)Ljava/util/List;
    .registers 2

    .prologue
    .line 167
    sget-object v0, Landroid/support/v4/view/a/h;->a:Landroid/support/v4/view/a/l;

    invoke-interface {v0, p0}, Landroid/support/v4/view/a/l;->a(Landroid/view/accessibility/AccessibilityManager;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private static b(Landroid/view/accessibility/AccessibilityManager;Landroid/support/v4/view/a/m;)Z
    .registers 3

    .prologue
    .line 156
    sget-object v0, Landroid/support/v4/view/a/h;->a:Landroid/support/v4/view/a/l;

    invoke-interface {v0, p0, p1}, Landroid/support/v4/view/a/l;->b(Landroid/view/accessibility/AccessibilityManager;Landroid/support/v4/view/a/m;)Z

    move-result v0

    return v0
.end method
