.class Landroid/support/v4/view/a/z;
.super Landroid/support/v4/view/a/y;
.source "SourceFile"


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    .line 1557
    invoke-direct {p0}, Landroid/support/v4/view/a/y;-><init>()V

    return-void
.end method


# virtual methods
.method public final G(Ljava/lang/Object;)Ljava/lang/String;
    .registers 3

    .prologue
    .line 1562
    .line 2030
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityNodeInfo;->getViewIdResourceName()Ljava/lang/String;

    move-result-object v0

    .line 1562
    return-object v0
.end method

.method public final a(Ljava/lang/Object;II)V
    .registers 4

    .prologue
    .line 1578
    .line 3040
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-virtual {p1, p2, p3}, Landroid/view/accessibility/AccessibilityNodeInfo;->setTextSelection(II)V

    .line 1579
    return-void
.end method

.method public final aa(Ljava/lang/Object;)I
    .registers 3

    .prologue
    .line 1583
    .line 3044
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityNodeInfo;->getTextSelectionStart()I

    move-result v0

    .line 1583
    return v0
.end method

.method public final ab(Ljava/lang/Object;)I
    .registers 3

    .prologue
    .line 1588
    .line 3048
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityNodeInfo;->getTextSelectionEnd()I

    move-result v0

    .line 1588
    return v0
.end method

.method public final ad(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 1593
    .line 3052
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityNodeInfo;->isEditable()Z

    move-result v0

    .line 1593
    return v0
.end method

.method public final af(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 1603
    .line 3060
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityNodeInfo;->refresh()Z

    move-result v0

    .line 1603
    return v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/String;)V
    .registers 3

    .prologue
    .line 1567
    .line 3026
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-virtual {p1, p2}, Landroid/view/accessibility/AccessibilityNodeInfo;->setViewIdResourceName(Ljava/lang/String;)V

    .line 1568
    return-void
.end method

.method public final c(Ljava/lang/Object;Ljava/lang/String;)Ljava/util/List;
    .registers 4

    .prologue
    .line 1572
    .line 3035
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-virtual {p1, p2}, Landroid/view/accessibility/AccessibilityNodeInfo;->findAccessibilityNodeInfosByViewId(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 3036
    check-cast v0, Ljava/util/List;

    .line 1572
    return-object v0
.end method

.method public final o(Ljava/lang/Object;Z)V
    .registers 3

    .prologue
    .line 1598
    .line 3056
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-virtual {p1, p2}, Landroid/view/accessibility/AccessibilityNodeInfo;->setEditable(Z)V

    .line 1599
    return-void
.end method
