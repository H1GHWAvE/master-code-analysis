.class Landroid/support/v4/view/a/v;
.super Landroid/support/v4/view/a/ab;
.source "SourceFile"


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    .line 1220
    invoke-direct {p0}, Landroid/support/v4/view/a/ab;-><init>()V

    return-void
.end method


# virtual methods
.method public final A(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 1348
    .line 2133
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityNodeInfo;->isScrollable()Z

    move-result v0

    .line 1348
    return v0
.end method

.method public final B(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 1353
    .line 2137
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityNodeInfo;->isSelected()Z

    move-result v0

    .line 1353
    return v0
.end method

.method public final C(Ljava/lang/Object;)V
    .registers 2

    .prologue
    .line 1453
    .line 2217
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityNodeInfo;->recycle()V

    .line 1454
    return-void
.end method

.method public final a()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 2031
    invoke-static {}, Landroid/view/accessibility/AccessibilityNodeInfo;->obtain()Landroid/view/accessibility/AccessibilityNodeInfo;

    move-result-object v0

    .line 1223
    return-object v0
.end method

.method public final a(Landroid/view/View;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 1228
    .line 2035
    invoke-static {p1}, Landroid/view/accessibility/AccessibilityNodeInfo;->obtain(Landroid/view/View;)Landroid/view/accessibility/AccessibilityNodeInfo;

    move-result-object v0

    .line 1228
    return-object v0
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/String;)Ljava/util/List;
    .registers 4

    .prologue
    .line 1248
    .line 2052
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-virtual {p1, p2}, Landroid/view/accessibility/AccessibilityNodeInfo;->findAccessibilityNodeInfosByText(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 2053
    check-cast v0, Ljava/util/List;

    .line 1248
    return-object v0
.end method

.method public final a(Ljava/lang/Object;Landroid/graphics/Rect;)V
    .registers 3

    .prologue
    .line 1258
    .line 2061
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-virtual {p1, p2}, Landroid/view/accessibility/AccessibilityNodeInfo;->getBoundsInParent(Landroid/graphics/Rect;)V

    .line 1259
    return-void
.end method

.method public final a(Ljava/lang/Object;Z)V
    .registers 3

    .prologue
    .line 1373
    .line 2153
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-virtual {p1, p2}, Landroid/view/accessibility/AccessibilityNodeInfo;->setCheckable(Z)V

    .line 1374
    return-void
.end method

.method public final b(Ljava/lang/Object;I)V
    .registers 3

    .prologue
    .line 1238
    .line 2043
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-virtual {p1, p2}, Landroid/view/accessibility/AccessibilityNodeInfo;->addAction(I)V

    .line 1239
    return-void
.end method

.method public final b(Ljava/lang/Object;Landroid/graphics/Rect;)V
    .registers 3

    .prologue
    .line 1263
    .line 2065
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-virtual {p1, p2}, Landroid/view/accessibility/AccessibilityNodeInfo;->getBoundsInScreen(Landroid/graphics/Rect;)V

    .line 1264
    return-void
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/CharSequence;)V
    .registers 3

    .prologue
    .line 1383
    .line 2161
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-virtual {p1, p2}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    .line 1384
    return-void
.end method

.method public final b(Ljava/lang/Object;Z)V
    .registers 3

    .prologue
    .line 1378
    .line 2157
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-virtual {p1, p2}, Landroid/view/accessibility/AccessibilityNodeInfo;->setChecked(Z)V

    .line 1379
    return-void
.end method

.method public final c(Ljava/lang/Object;I)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 1268
    .line 2069
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-virtual {p1, p2}, Landroid/view/accessibility/AccessibilityNodeInfo;->getChild(I)Landroid/view/accessibility/AccessibilityNodeInfo;

    move-result-object v0

    .line 1268
    return-object v0
.end method

.method public final c(Ljava/lang/Object;Landroid/graphics/Rect;)V
    .registers 3

    .prologue
    .line 1363
    .line 2145
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-virtual {p1, p2}, Landroid/view/accessibility/AccessibilityNodeInfo;->setBoundsInParent(Landroid/graphics/Rect;)V

    .line 1364
    return-void
.end method

.method public final c(Ljava/lang/Object;Ljava/lang/CharSequence;)V
    .registers 3

    .prologue
    .line 1393
    .line 2169
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-virtual {p1, p2}, Landroid/view/accessibility/AccessibilityNodeInfo;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1394
    return-void
.end method

.method public final c(Ljava/lang/Object;Z)V
    .registers 3

    .prologue
    .line 1388
    .line 2165
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-virtual {p1, p2}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClickable(Z)V

    .line 1389
    return-void
.end method

.method public final d(Ljava/lang/Object;Landroid/graphics/Rect;)V
    .registers 3

    .prologue
    .line 1368
    .line 2149
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-virtual {p1, p2}, Landroid/view/accessibility/AccessibilityNodeInfo;->setBoundsInScreen(Landroid/graphics/Rect;)V

    .line 1369
    return-void
.end method

.method public final d(Ljava/lang/Object;Landroid/view/View;)V
    .registers 3

    .prologue
    .line 1243
    .line 2047
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-virtual {p1, p2}, Landroid/view/accessibility/AccessibilityNodeInfo;->addChild(Landroid/view/View;)V

    .line 1244
    return-void
.end method

.method public final d(Ljava/lang/Object;Ljava/lang/CharSequence;)V
    .registers 3

    .prologue
    .line 1418
    .line 2189
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-virtual {p1, p2}, Landroid/view/accessibility/AccessibilityNodeInfo;->setPackageName(Ljava/lang/CharSequence;)V

    .line 1419
    return-void
.end method

.method public final d(Ljava/lang/Object;Z)V
    .registers 3

    .prologue
    .line 1398
    .line 2173
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-virtual {p1, p2}, Landroid/view/accessibility/AccessibilityNodeInfo;->setEnabled(Z)V

    .line 1399
    return-void
.end method

.method public final d(Ljava/lang/Object;I)Z
    .registers 4

    .prologue
    .line 1358
    .line 2141
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-virtual {p1, p2}, Landroid/view/accessibility/AccessibilityNodeInfo;->performAction(I)Z

    move-result v0

    .line 1358
    return v0
.end method

.method public final e(Ljava/lang/Object;Landroid/view/View;)V
    .registers 3

    .prologue
    .line 1423
    .line 2193
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-virtual {p1, p2}, Landroid/view/accessibility/AccessibilityNodeInfo;->setParent(Landroid/view/View;)V

    .line 1424
    return-void
.end method

.method public final e(Ljava/lang/Object;Ljava/lang/CharSequence;)V
    .registers 3

    .prologue
    .line 1448
    .line 2213
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-virtual {p1, p2}, Landroid/view/accessibility/AccessibilityNodeInfo;->setText(Ljava/lang/CharSequence;)V

    .line 1449
    return-void
.end method

.method public final e(Ljava/lang/Object;Z)V
    .registers 3

    .prologue
    .line 1403
    .line 2177
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-virtual {p1, p2}, Landroid/view/accessibility/AccessibilityNodeInfo;->setFocusable(Z)V

    .line 1404
    return-void
.end method

.method public final f(Ljava/lang/Object;Landroid/view/View;)V
    .registers 3

    .prologue
    .line 1443
    .line 2209
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-virtual {p1, p2}, Landroid/view/accessibility/AccessibilityNodeInfo;->setSource(Landroid/view/View;)V

    .line 1444
    return-void
.end method

.method public final f(Ljava/lang/Object;Z)V
    .registers 3

    .prologue
    .line 1408
    .line 2181
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-virtual {p1, p2}, Landroid/view/accessibility/AccessibilityNodeInfo;->setFocused(Z)V

    .line 1409
    return-void
.end method

.method public final g(Ljava/lang/Object;Z)V
    .registers 3

    .prologue
    .line 1413
    .line 2185
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-virtual {p1, p2}, Landroid/view/accessibility/AccessibilityNodeInfo;->setLongClickable(Z)V

    .line 1414
    return-void
.end method

.method public final h(Ljava/lang/Object;Z)V
    .registers 3

    .prologue
    .line 1428
    .line 2197
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-virtual {p1, p2}, Landroid/view/accessibility/AccessibilityNodeInfo;->setPassword(Z)V

    .line 1429
    return-void
.end method

.method public final i(Ljava/lang/Object;Z)V
    .registers 3

    .prologue
    .line 1433
    .line 2201
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-virtual {p1, p2}, Landroid/view/accessibility/AccessibilityNodeInfo;->setScrollable(Z)V

    .line 1434
    return-void
.end method

.method public final j(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 1233
    .line 2039
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-static {p1}, Landroid/view/accessibility/AccessibilityNodeInfo;->obtain(Landroid/view/accessibility/AccessibilityNodeInfo;)Landroid/view/accessibility/AccessibilityNodeInfo;

    move-result-object v0

    .line 1233
    return-object v0
.end method

.method public final j(Ljava/lang/Object;Z)V
    .registers 3

    .prologue
    .line 1438
    .line 2205
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-virtual {p1, p2}, Landroid/view/accessibility/AccessibilityNodeInfo;->setSelected(Z)V

    .line 1439
    return-void
.end method

.method public final k(Ljava/lang/Object;)I
    .registers 3

    .prologue
    .line 1253
    .line 2057
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityNodeInfo;->getActions()I

    move-result v0

    .line 1253
    return v0
.end method

.method public final l(Ljava/lang/Object;)I
    .registers 3

    .prologue
    .line 1273
    .line 2073
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityNodeInfo;->getChildCount()I

    move-result v0

    .line 1273
    return v0
.end method

.method public final m(Ljava/lang/Object;)Ljava/lang/CharSequence;
    .registers 3

    .prologue
    .line 1278
    .line 2077
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityNodeInfo;->getClassName()Ljava/lang/CharSequence;

    move-result-object v0

    .line 1278
    return-object v0
.end method

.method public final n(Ljava/lang/Object;)Ljava/lang/CharSequence;
    .registers 3

    .prologue
    .line 1283
    .line 2081
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityNodeInfo;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v0

    .line 1283
    return-object v0
.end method

.method public final o(Ljava/lang/Object;)Ljava/lang/CharSequence;
    .registers 3

    .prologue
    .line 1288
    .line 2085
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityNodeInfo;->getPackageName()Ljava/lang/CharSequence;

    move-result-object v0

    .line 1288
    return-object v0
.end method

.method public final p(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 1293
    .line 2089
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityNodeInfo;->getParent()Landroid/view/accessibility/AccessibilityNodeInfo;

    move-result-object v0

    .line 1293
    return-object v0
.end method

.method public final q(Ljava/lang/Object;)Ljava/lang/CharSequence;
    .registers 3

    .prologue
    .line 1298
    .line 2093
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityNodeInfo;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    .line 1298
    return-object v0
.end method

.method public final r(Ljava/lang/Object;)I
    .registers 3

    .prologue
    .line 1303
    .line 2097
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityNodeInfo;->getWindowId()I

    move-result v0

    .line 1303
    return v0
.end method

.method public final s(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 1308
    .line 2101
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityNodeInfo;->isCheckable()Z

    move-result v0

    .line 1308
    return v0
.end method

.method public final t(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 1313
    .line 2105
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityNodeInfo;->isChecked()Z

    move-result v0

    .line 1313
    return v0
.end method

.method public final u(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 1318
    .line 2109
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityNodeInfo;->isClickable()Z

    move-result v0

    .line 1318
    return v0
.end method

.method public final v(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 1323
    .line 2113
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityNodeInfo;->isEnabled()Z

    move-result v0

    .line 1323
    return v0
.end method

.method public final w(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 1328
    .line 2117
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityNodeInfo;->isFocusable()Z

    move-result v0

    .line 1328
    return v0
.end method

.method public final x(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 1333
    .line 2121
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityNodeInfo;->isFocused()Z

    move-result v0

    .line 1333
    return v0
.end method

.method public final y(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 1338
    .line 2125
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityNodeInfo;->isLongClickable()Z

    move-result v0

    .line 1338
    return v0
.end method

.method public final z(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 1343
    .line 2129
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityNodeInfo;->isPassword()Z

    move-result v0

    .line 1343
    return v0
.end method
