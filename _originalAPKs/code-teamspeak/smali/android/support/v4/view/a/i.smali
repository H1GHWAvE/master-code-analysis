.class final Landroid/support/v4/view/a/i;
.super Landroid/support/v4/view/a/k;
.source "SourceFile"


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    .line 78
    invoke-direct {p0}, Landroid/support/v4/view/a/k;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/support/v4/view/a/m;)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 83
    new-instance v0, Landroid/support/v4/view/a/j;

    invoke-direct {v0, p0, p1}, Landroid/support/v4/view/a/j;-><init>(Landroid/support/v4/view/a/i;Landroid/support/v4/view/a/m;)V

    .line 1036
    new-instance v1, Landroid/support/v4/view/a/o;

    invoke-direct {v1, v0}, Landroid/support/v4/view/a/o;-><init>(Landroid/support/v4/view/a/p;)V

    .line 83
    return-object v1
.end method

.method public final a(Landroid/view/accessibility/AccessibilityManager;)Ljava/util/List;
    .registers 3

    .prologue
    .line 115
    .line 1063
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityManager;->getInstalledAccessibilityServiceList()Ljava/util/List;

    move-result-object v0

    .line 115
    return-object v0
.end method

.method public final a(Landroid/view/accessibility/AccessibilityManager;I)Ljava/util/List;
    .registers 4

    .prologue
    .line 108
    .line 1058
    invoke-virtual {p1, p2}, Landroid/view/accessibility/AccessibilityManager;->getEnabledAccessibilityServiceList(I)Ljava/util/List;

    move-result-object v0

    .line 108
    return-object v0
.end method

.method public final a(Landroid/view/accessibility/AccessibilityManager;Landroid/support/v4/view/a/m;)Z
    .registers 4

    .prologue
    .line 94
    iget-object v0, p2, Landroid/support/v4/view/a/m;->a:Ljava/lang/Object;

    .line 1046
    check-cast v0, Landroid/view/accessibility/AccessibilityManager$AccessibilityStateChangeListener;

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityManager;->addAccessibilityStateChangeListener(Landroid/view/accessibility/AccessibilityManager$AccessibilityStateChangeListener;)Z

    move-result v0

    .line 94
    return v0
.end method

.method public final b(Landroid/view/accessibility/AccessibilityManager;)Z
    .registers 3

    .prologue
    .line 120
    .line 1067
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityManager;->isTouchExplorationEnabled()Z

    move-result v0

    .line 120
    return v0
.end method

.method public final b(Landroid/view/accessibility/AccessibilityManager;Landroid/support/v4/view/a/m;)Z
    .registers 4

    .prologue
    .line 101
    iget-object v0, p2, Landroid/support/v4/view/a/m;->a:Ljava/lang/Object;

    .line 1052
    check-cast v0, Landroid/view/accessibility/AccessibilityManager$AccessibilityStateChangeListener;

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityManager;->removeAccessibilityStateChangeListener(Landroid/view/accessibility/AccessibilityManager$AccessibilityStateChangeListener;)Z

    move-result v0

    .line 101
    return v0
.end method
