.class public final Landroid/support/v4/view/bz;
.super Landroid/support/v4/view/cc;
.source "SourceFile"


# static fields
.field private static final f:Ljava/lang/String; = "PagerTabStrip"

.field private static final g:I = 0x3

.field private static final h:I = 0x6

.field private static final i:I = 0x10

.field private static final j:I = 0x20

.field private static final k:I = 0x40

.field private static final l:I = 0x1

.field private static final m:I = 0x20


# instance fields
.field private A:F

.field private B:F

.field private C:I

.field private n:I

.field private o:I

.field private p:I

.field private q:I

.field private r:I

.field private s:I

.field private final t:Landroid/graphics/Paint;

.field private final u:Landroid/graphics/Rect;

.field private v:I

.field private w:Z

.field private x:Z

.field private y:I

.field private z:Z


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .registers 3

    .prologue
    .line 79
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/support/v4/view/bz;-><init>(Landroid/content/Context;B)V

    .line 80
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;B)V
    .registers 9

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const/high16 v2, 0x3f000000    # 0.5f

    .line 83
    invoke-direct {p0, p1, v4}, Landroid/support/v4/view/cc;-><init>(Landroid/content/Context;B)V

    .line 64
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Landroid/support/v4/view/bz;->t:Landroid/graphics/Paint;

    .line 65
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Landroid/support/v4/view/bz;->u:Landroid/graphics/Rect;

    .line 67
    const/16 v0, 0xff

    iput v0, p0, Landroid/support/v4/view/bz;->v:I

    .line 69
    iput-boolean v4, p0, Landroid/support/v4/view/bz;->w:Z

    .line 70
    iput-boolean v4, p0, Landroid/support/v4/view/bz;->x:Z

    .line 85
    iget v0, p0, Landroid/support/v4/view/bz;->e:I

    iput v0, p0, Landroid/support/v4/view/bz;->n:I

    .line 86
    iget-object v0, p0, Landroid/support/v4/view/bz;->t:Landroid/graphics/Paint;

    iget v1, p0, Landroid/support/v4/view/bz;->n:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 90
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    .line 91
    const/high16 v1, 0x40400000    # 3.0f

    mul-float/2addr v1, v0

    add-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, p0, Landroid/support/v4/view/bz;->o:I

    .line 92
    const/high16 v1, 0x40c00000    # 6.0f

    mul-float/2addr v1, v0

    add-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, p0, Landroid/support/v4/view/bz;->p:I

    .line 93
    const/high16 v1, 0x42800000    # 64.0f

    mul-float/2addr v1, v0

    float-to-int v1, v1

    iput v1, p0, Landroid/support/v4/view/bz;->q:I

    .line 94
    const/high16 v1, 0x41800000    # 16.0f

    mul-float/2addr v1, v0

    add-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, p0, Landroid/support/v4/view/bz;->s:I

    .line 95
    const/high16 v1, 0x3f800000    # 1.0f

    mul-float/2addr v1, v0

    add-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, p0, Landroid/support/v4/view/bz;->y:I

    .line 96
    const/high16 v1, 0x42000000    # 32.0f

    mul-float/2addr v0, v1

    add-float/2addr v0, v2

    float-to-int v0, v0

    iput v0, p0, Landroid/support/v4/view/bz;->r:I

    .line 97
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    iput v0, p0, Landroid/support/v4/view/bz;->C:I

    .line 100
    invoke-virtual {p0}, Landroid/support/v4/view/bz;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p0}, Landroid/support/v4/view/bz;->getPaddingTop()I

    move-result v1

    invoke-virtual {p0}, Landroid/support/v4/view/bz;->getPaddingRight()I

    move-result v2

    invoke-virtual {p0}, Landroid/support/v4/view/bz;->getPaddingBottom()I

    move-result v3

    invoke-virtual {p0, v0, v1, v2, v3}, Landroid/support/v4/view/bz;->setPadding(IIII)V

    .line 101
    invoke-virtual {p0}, Landroid/support/v4/view/bz;->getTextSpacing()I

    move-result v0

    invoke-virtual {p0, v0}, Landroid/support/v4/view/bz;->setTextSpacing(I)V

    .line 103
    invoke-virtual {p0, v4}, Landroid/support/v4/view/bz;->setWillNotDraw(Z)V

    .line 105
    iget-object v0, p0, Landroid/support/v4/view/bz;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setFocusable(Z)V

    .line 106
    iget-object v0, p0, Landroid/support/v4/view/bz;->b:Landroid/widget/TextView;

    new-instance v1, Landroid/support/v4/view/ca;

    invoke-direct {v1, p0}, Landroid/support/v4/view/ca;-><init>(Landroid/support/v4/view/bz;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 113
    iget-object v0, p0, Landroid/support/v4/view/bz;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setFocusable(Z)V

    .line 114
    iget-object v0, p0, Landroid/support/v4/view/bz;->d:Landroid/widget/TextView;

    new-instance v1, Landroid/support/v4/view/cb;

    invoke-direct {v1, p0}, Landroid/support/v4/view/cb;-><init>(Landroid/support/v4/view/bz;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 121
    invoke-virtual {p0}, Landroid/support/v4/view/bz;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-nez v0, :cond_a8

    .line 122
    iput-boolean v5, p0, Landroid/support/v4/view/bz;->w:Z

    .line 124
    :cond_a8
    return-void
.end method


# virtual methods
.method final a(IFZ)V
    .registers 10

    .prologue
    .line 281
    iget-object v0, p0, Landroid/support/v4/view/bz;->u:Landroid/graphics/Rect;

    .line 282
    invoke-virtual {p0}, Landroid/support/v4/view/bz;->getHeight()I

    move-result v1

    .line 283
    iget-object v2, p0, Landroid/support/v4/view/bz;->c:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getLeft()I

    move-result v2

    iget v3, p0, Landroid/support/v4/view/bz;->s:I

    sub-int/2addr v2, v3

    .line 284
    iget-object v3, p0, Landroid/support/v4/view/bz;->c:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getRight()I

    move-result v3

    iget v4, p0, Landroid/support/v4/view/bz;->s:I

    add-int/2addr v3, v4

    .line 285
    iget v4, p0, Landroid/support/v4/view/bz;->o:I

    sub-int v4, v1, v4

    .line 287
    invoke-virtual {v0, v2, v4, v3, v1}, Landroid/graphics/Rect;->set(IIII)V

    .line 289
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/view/cc;->a(IFZ)V

    .line 290
    const/high16 v2, 0x3f000000    # 0.5f

    sub-float v2, p2, v2

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    const/high16 v3, 0x40000000    # 2.0f

    mul-float/2addr v2, v3

    const/high16 v3, 0x437f0000    # 255.0f

    mul-float/2addr v2, v3

    float-to-int v2, v2

    iput v2, p0, Landroid/support/v4/view/bz;->v:I

    .line 292
    iget-object v2, p0, Landroid/support/v4/view/bz;->c:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getLeft()I

    move-result v2

    iget v3, p0, Landroid/support/v4/view/bz;->s:I

    sub-int/2addr v2, v3

    .line 293
    iget-object v3, p0, Landroid/support/v4/view/bz;->c:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getRight()I

    move-result v3

    iget v5, p0, Landroid/support/v4/view/bz;->s:I

    add-int/2addr v3, v5

    .line 294
    invoke-virtual {v0, v2, v4, v3, v1}, Landroid/graphics/Rect;->union(IIII)V

    .line 296
    invoke-virtual {p0, v0}, Landroid/support/v4/view/bz;->invalidate(Landroid/graphics/Rect;)V

    .line 297
    return-void
.end method

.method public final getDrawFullUnderline()Z
    .registers 2

    .prologue
    .line 214
    iget-boolean v0, p0, Landroid/support/v4/view/bz;->w:Z

    return v0
.end method

.method final getMinHeight()I
    .registers 3

    .prologue
    .line 219
    invoke-super {p0}, Landroid/support/v4/view/cc;->getMinHeight()I

    move-result v0

    iget v1, p0, Landroid/support/v4/view/bz;->r:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method public final getTabIndicatorColor()I
    .registers 2
    .annotation build Landroid/support/a/j;
    .end annotation

    .prologue
    .line 151
    iget v0, p0, Landroid/support/v4/view/bz;->n:I

    return v0
.end method

.method protected final onDraw(Landroid/graphics/Canvas;)V
    .registers 10

    .prologue
    const v7, 0xffffff

    .line 261
    invoke-super {p0, p1}, Landroid/support/v4/view/cc;->onDraw(Landroid/graphics/Canvas;)V

    .line 263
    invoke-virtual {p0}, Landroid/support/v4/view/bz;->getHeight()I

    move-result v6

    .line 265
    iget-object v0, p0, Landroid/support/v4/view/bz;->c:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getLeft()I

    move-result v0

    iget v1, p0, Landroid/support/v4/view/bz;->s:I

    sub-int/2addr v0, v1

    .line 266
    iget-object v1, p0, Landroid/support/v4/view/bz;->c:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getRight()I

    move-result v1

    iget v2, p0, Landroid/support/v4/view/bz;->s:I

    add-int v3, v1, v2

    .line 267
    iget v1, p0, Landroid/support/v4/view/bz;->o:I

    sub-int v2, v6, v1

    .line 269
    iget-object v1, p0, Landroid/support/v4/view/bz;->t:Landroid/graphics/Paint;

    iget v4, p0, Landroid/support/v4/view/bz;->v:I

    shl-int/lit8 v4, v4, 0x18

    iget v5, p0, Landroid/support/v4/view/bz;->n:I

    and-int/2addr v5, v7

    or-int/2addr v4, v5

    invoke-virtual {v1, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 270
    int-to-float v1, v0

    int-to-float v2, v2

    int-to-float v3, v3

    int-to-float v4, v6

    iget-object v5, p0, Landroid/support/v4/view/bz;->t:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 272
    iget-boolean v0, p0, Landroid/support/v4/view/bz;->w:Z

    if-eqz v0, :cond_62

    .line 273
    iget-object v0, p0, Landroid/support/v4/view/bz;->t:Landroid/graphics/Paint;

    const/high16 v1, -0x1000000

    iget v2, p0, Landroid/support/v4/view/bz;->n:I

    and-int/2addr v2, v7

    or-int/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 274
    invoke-virtual {p0}, Landroid/support/v4/view/bz;->getPaddingLeft()I

    move-result v0

    int-to-float v1, v0

    iget v0, p0, Landroid/support/v4/view/bz;->y:I

    sub-int v0, v6, v0

    int-to-float v2, v0

    invoke-virtual {p0}, Landroid/support/v4/view/bz;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Landroid/support/v4/view/bz;->getPaddingRight()I

    move-result v3

    sub-int/2addr v0, v3

    int-to-float v3, v0

    int-to-float v4, v6

    iget-object v5, p0, Landroid/support/v4/view/bz;->t:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 277
    :cond_62
    return-void
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 7

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 224
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    .line 225
    if-eqz v2, :cond_d

    iget-boolean v3, p0, Landroid/support/v4/view/bz;->z:Z

    if-eqz v3, :cond_d

    .line 256
    :goto_c
    return v0

    .line 231
    :cond_d
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    .line 232
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    .line 233
    packed-switch v2, :pswitch_data_7a

    :cond_18
    :goto_18
    move v0, v1

    .line 256
    goto :goto_c

    .line 235
    :pswitch_1a
    iput v3, p0, Landroid/support/v4/view/bz;->A:F

    .line 236
    iput v4, p0, Landroid/support/v4/view/bz;->B:F

    .line 237
    iput-boolean v0, p0, Landroid/support/v4/view/bz;->z:Z

    goto :goto_18

    .line 241
    :pswitch_21
    iget v0, p0, Landroid/support/v4/view/bz;->A:F

    sub-float v0, v3, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v2, p0, Landroid/support/v4/view/bz;->C:I

    int-to-float v2, v2

    cmpl-float v0, v0, v2

    if-gtz v0, :cond_3f

    iget v0, p0, Landroid/support/v4/view/bz;->B:F

    sub-float v0, v4, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v2, p0, Landroid/support/v4/view/bz;->C:I

    int-to-float v2, v2

    cmpl-float v0, v0, v2

    if-lez v0, :cond_18

    .line 243
    :cond_3f
    iput-boolean v1, p0, Landroid/support/v4/view/bz;->z:Z

    goto :goto_18

    .line 248
    :pswitch_42
    iget-object v0, p0, Landroid/support/v4/view/bz;->c:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getLeft()I

    move-result v0

    iget v2, p0, Landroid/support/v4/view/bz;->s:I

    sub-int/2addr v0, v2

    int-to-float v0, v0

    cmpg-float v0, v3, v0

    if-gez v0, :cond_5e

    .line 249
    iget-object v0, p0, Landroid/support/v4/view/bz;->a:Landroid/support/v4/view/ViewPager;

    iget-object v2, p0, Landroid/support/v4/view/bz;->a:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v2}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v2}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    goto :goto_18

    .line 250
    :cond_5e
    iget-object v0, p0, Landroid/support/v4/view/bz;->c:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getRight()I

    move-result v0

    iget v2, p0, Landroid/support/v4/view/bz;->s:I

    add-int/2addr v0, v2

    int-to-float v0, v0

    cmpl-float v0, v3, v0

    if-lez v0, :cond_18

    .line 251
    iget-object v0, p0, Landroid/support/v4/view/bz;->a:Landroid/support/v4/view/ViewPager;

    iget-object v2, p0, Landroid/support/v4/view/bz;->a:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v2}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v0, v2}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    goto :goto_18

    .line 233
    :pswitch_data_7a
    .packed-switch 0x0
        :pswitch_1a
        :pswitch_42
        :pswitch_21
    .end packed-switch
.end method

.method public final setBackgroundColor(I)V
    .registers 3
    .param p1    # I
        .annotation build Landroid/support/a/j;
        .end annotation
    .end param

    .prologue
    .line 180
    invoke-super {p0, p1}, Landroid/support/v4/view/cc;->setBackgroundColor(I)V

    .line 181
    iget-boolean v0, p0, Landroid/support/v4/view/bz;->x:Z

    if-nez v0, :cond_f

    .line 182
    const/high16 v0, -0x1000000

    and-int/2addr v0, p1

    if-nez v0, :cond_10

    const/4 v0, 0x1

    :goto_d
    iput-boolean v0, p0, Landroid/support/v4/view/bz;->w:Z

    .line 184
    :cond_f
    return-void

    .line 182
    :cond_10
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public final setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
    .registers 3

    .prologue
    .line 172
    invoke-super {p0, p1}, Landroid/support/v4/view/cc;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 173
    iget-boolean v0, p0, Landroid/support/v4/view/bz;->x:Z

    if-nez v0, :cond_c

    .line 174
    if-nez p1, :cond_d

    const/4 v0, 0x1

    :goto_a
    iput-boolean v0, p0, Landroid/support/v4/view/bz;->w:Z

    .line 176
    :cond_c
    return-void

    .line 174
    :cond_d
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public final setBackgroundResource(I)V
    .registers 3
    .param p1    # I
        .annotation build Landroid/support/a/m;
        .end annotation
    .end param

    .prologue
    .line 188
    invoke-super {p0, p1}, Landroid/support/v4/view/cc;->setBackgroundResource(I)V

    .line 189
    iget-boolean v0, p0, Landroid/support/v4/view/bz;->x:Z

    if-nez v0, :cond_c

    .line 190
    if-nez p1, :cond_d

    const/4 v0, 0x1

    :goto_a
    iput-boolean v0, p0, Landroid/support/v4/view/bz;->w:Z

    .line 192
    :cond_c
    return-void

    .line 190
    :cond_d
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public final setDrawFullUnderline(Z)V
    .registers 3

    .prologue
    .line 201
    iput-boolean p1, p0, Landroid/support/v4/view/bz;->w:Z

    .line 202
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/view/bz;->x:Z

    .line 203
    invoke-virtual {p0}, Landroid/support/v4/view/bz;->invalidate()V

    .line 204
    return-void
.end method

.method public final setPadding(IIII)V
    .registers 6

    .prologue
    .line 156
    iget v0, p0, Landroid/support/v4/view/bz;->p:I

    if-ge p4, v0, :cond_6

    .line 157
    iget p4, p0, Landroid/support/v4/view/bz;->p:I

    .line 159
    :cond_6
    invoke-super {p0, p1, p2, p3, p4}, Landroid/support/v4/view/cc;->setPadding(IIII)V

    .line 160
    return-void
.end method

.method public final setTabIndicatorColor(I)V
    .registers 4
    .param p1    # I
        .annotation build Landroid/support/a/j;
        .end annotation
    .end param

    .prologue
    .line 132
    iput p1, p0, Landroid/support/v4/view/bz;->n:I

    .line 133
    iget-object v0, p0, Landroid/support/v4/view/bz;->t:Landroid/graphics/Paint;

    iget v1, p0, Landroid/support/v4/view/bz;->n:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 134
    invoke-virtual {p0}, Landroid/support/v4/view/bz;->invalidate()V

    .line 135
    return-void
.end method

.method public final setTabIndicatorColorResource(I)V
    .registers 3
    .param p1    # I
        .annotation build Landroid/support/a/k;
        .end annotation
    .end param

    .prologue
    .line 143
    invoke-virtual {p0}, Landroid/support/v4/view/bz;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v0}, Landroid/support/v4/view/bz;->setTabIndicatorColor(I)V

    .line 144
    return-void
.end method

.method public final setTextSpacing(I)V
    .registers 3

    .prologue
    .line 164
    iget v0, p0, Landroid/support/v4/view/bz;->q:I

    if-ge p1, v0, :cond_6

    .line 165
    iget p1, p0, Landroid/support/v4/view/bz;->q:I

    .line 167
    :cond_6
    invoke-super {p0, p1}, Landroid/support/v4/view/cc;->setTextSpacing(I)V

    .line 168
    return-void
.end method
