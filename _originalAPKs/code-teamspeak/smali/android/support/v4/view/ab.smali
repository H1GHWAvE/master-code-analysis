.class public final Landroid/support/v4/view/ab;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final a:Landroid/support/v4/view/af;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 166
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_e

    .line 167
    new-instance v0, Landroid/support/v4/view/ae;

    invoke-direct {v0}, Landroid/support/v4/view/ae;-><init>()V

    sput-object v0, Landroid/support/v4/view/ab;->a:Landroid/support/v4/view/af;

    .line 171
    :goto_d
    return-void

    .line 169
    :cond_e
    new-instance v0, Landroid/support/v4/view/ac;

    invoke-direct {v0}, Landroid/support/v4/view/ac;-><init>()V

    sput-object v0, Landroid/support/v4/view/ab;->a:Landroid/support/v4/view/af;

    goto :goto_d
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 144
    return-void
.end method

.method private static a(I)I
    .registers 2

    .prologue
    .line 176
    sget-object v0, Landroid/support/v4/view/ab;->a:Landroid/support/v4/view/af;

    invoke-interface {v0, p0}, Landroid/support/v4/view/af;->a(I)I

    move-result v0

    return v0
.end method

.method public static a(Landroid/view/View;)Ljava/lang/Object;
    .registers 2

    .prologue
    .line 204
    sget-object v0, Landroid/support/v4/view/ab;->a:Landroid/support/v4/view/af;

    invoke-interface {v0, p0}, Landroid/support/v4/view/af;->a(Landroid/view/View;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method private static a(II)Z
    .registers 3

    .prologue
    .line 180
    sget-object v0, Landroid/support/v4/view/ab;->a:Landroid/support/v4/view/af;

    invoke-interface {v0, p0, p1}, Landroid/support/v4/view/af;->a(II)Z

    move-result v0

    return v0
.end method

.method public static a(Landroid/view/KeyEvent;)Z
    .registers 4

    .prologue
    .line 188
    sget-object v0, Landroid/support/v4/view/ab;->a:Landroid/support/v4/view/af;

    invoke-virtual {p0}, Landroid/view/KeyEvent;->getMetaState()I

    move-result v1

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/support/v4/view/af;->a(II)Z

    move-result v0

    return v0
.end method

.method public static a(Landroid/view/KeyEvent;Landroid/view/KeyEvent$Callback;Ljava/lang/Object;Ljava/lang/Object;)Z
    .registers 5

    .prologue
    .line 209
    sget-object v0, Landroid/support/v4/view/ab;->a:Landroid/support/v4/view/af;

    invoke-interface {v0, p0, p1, p2, p3}, Landroid/support/v4/view/af;->a(Landroid/view/KeyEvent;Landroid/view/KeyEvent$Callback;Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private static b(I)Z
    .registers 2

    .prologue
    .line 184
    sget-object v0, Landroid/support/v4/view/ab;->a:Landroid/support/v4/view/af;

    invoke-interface {v0, p0}, Landroid/support/v4/view/af;->b(I)Z

    move-result v0

    return v0
.end method

.method public static b(Landroid/view/KeyEvent;)Z
    .registers 3

    .prologue
    .line 192
    sget-object v0, Landroid/support/v4/view/ab;->a:Landroid/support/v4/view/af;

    invoke-virtual {p0}, Landroid/view/KeyEvent;->getMetaState()I

    move-result v1

    invoke-interface {v0, v1}, Landroid/support/v4/view/af;->b(I)Z

    move-result v0

    return v0
.end method

.method public static c(Landroid/view/KeyEvent;)V
    .registers 2

    .prologue
    .line 196
    sget-object v0, Landroid/support/v4/view/ab;->a:Landroid/support/v4/view/af;

    invoke-interface {v0, p0}, Landroid/support/v4/view/af;->a(Landroid/view/KeyEvent;)V

    .line 197
    return-void
.end method

.method private static d(Landroid/view/KeyEvent;)Z
    .registers 2

    .prologue
    .line 200
    sget-object v0, Landroid/support/v4/view/ab;->a:Landroid/support/v4/view/af;

    invoke-interface {v0, p0}, Landroid/support/v4/view/af;->b(Landroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method
