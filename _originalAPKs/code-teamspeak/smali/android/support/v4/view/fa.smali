.class final Landroid/support/v4/view/fa;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/Comparator;


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    .line 2967
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Landroid/view/View;Landroid/view/View;)I
    .registers 6

    .prologue
    .line 2970
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/es;

    .line 2971
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/support/v4/view/es;

    .line 2972
    iget-boolean v2, v0, Landroid/support/v4/view/es;->a:Z

    iget-boolean v3, v1, Landroid/support/v4/view/es;->a:Z

    if-eq v2, v3, :cond_1a

    .line 2973
    iget-boolean v0, v0, Landroid/support/v4/view/es;->a:Z

    if-eqz v0, :cond_18

    const/4 v0, 0x1

    .line 2975
    :goto_17
    return v0

    .line 2973
    :cond_18
    const/4 v0, -0x1

    goto :goto_17

    .line 2975
    :cond_1a
    iget v0, v0, Landroid/support/v4/view/es;->e:I

    iget v1, v1, Landroid/support/v4/view/es;->e:I

    sub-int/2addr v0, v1

    goto :goto_17
.end method


# virtual methods
.method public final synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .registers 7

    .prologue
    .line 2967
    check-cast p1, Landroid/view/View;

    check-cast p2, Landroid/view/View;

    .line 3970
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/es;

    .line 3971
    invoke-virtual {p2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/support/v4/view/es;

    .line 3972
    iget-boolean v2, v0, Landroid/support/v4/view/es;->a:Z

    iget-boolean v3, v1, Landroid/support/v4/view/es;->a:Z

    if-eq v2, v3, :cond_1e

    .line 3973
    iget-boolean v0, v0, Landroid/support/v4/view/es;->a:Z

    if-eqz v0, :cond_1c

    const/4 v0, 0x1

    :goto_1b
    return v0

    :cond_1c
    const/4 v0, -0x1

    goto :goto_1b

    .line 3975
    :cond_1e
    iget v0, v0, Landroid/support/v4/view/es;->e:I

    iget v1, v1, Landroid/support/v4/view/es;->e:I

    sub-int/2addr v0, v1

    .line 2967
    goto :goto_1b
.end method
