.class Landroid/support/v4/view/fo;
.super Landroid/support/v4/view/fm;
.source "SourceFile"


# instance fields
.field b:Ljava/util/WeakHashMap;


# direct methods
.method constructor <init>()V
    .registers 2

    .prologue
    .line 362
    invoke-direct {p0}, Landroid/support/v4/view/fm;-><init>()V

    .line 363
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v4/view/fo;->b:Ljava/util/WeakHashMap;

    .line 524
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;)J
    .registers 4

    .prologue
    .line 387
    .line 1042
    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->getDuration()J

    move-result-wide v0

    .line 387
    return-wide v0
.end method

.method public final a(Landroid/support/v4/view/fk;Landroid/view/View;)V
    .registers 5

    .prologue
    .line 442
    .line 1086
    invoke-virtual {p2}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    .line 443
    return-void
.end method

.method public final a(Landroid/support/v4/view/fk;Landroid/view/View;F)V
    .registers 5

    .prologue
    .line 372
    .line 1030
    invoke-virtual {p2}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, p3}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    .line 373
    return-void
.end method

.method public a(Landroid/support/v4/view/fk;Landroid/view/View;Landroid/support/v4/view/gd;)V
    .registers 5

    .prologue
    .line 502
    const/high16 v0, 0x7e000000

    invoke-virtual {p2, v0, p3}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 503
    new-instance v0, Landroid/support/v4/view/fp;

    invoke-direct {v0, p1}, Landroid/support/v4/view/fp;-><init>(Landroid/support/v4/view/fk;)V

    invoke-static {p2, v0}, Landroid/support/v4/view/fv;->a(Landroid/view/View;Landroid/support/v4/view/gd;)V

    .line 504
    return-void
.end method

.method public a(Landroid/support/v4/view/fk;Landroid/view/View;Ljava/lang/Runnable;)V
    .registers 5

    .prologue
    .line 508
    new-instance v0, Landroid/support/v4/view/fp;

    invoke-direct {v0, p1}, Landroid/support/v4/view/fp;-><init>(Landroid/support/v4/view/fk;)V

    invoke-static {p2, v0}, Landroid/support/v4/view/fv;->a(Landroid/view/View;Landroid/support/v4/view/gd;)V

    .line 509
    invoke-static {p1, p3}, Landroid/support/v4/view/fk;->a(Landroid/support/v4/view/fk;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 510
    return-void
.end method

.method public final a(Landroid/view/View;J)V
    .registers 6

    .prologue
    .line 367
    .line 1026
    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    .line 368
    return-void
.end method

.method public final a(Landroid/view/View;Landroid/view/animation/Interpolator;)V
    .registers 4

    .prologue
    .line 392
    .line 1046
    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    .line 393
    return-void
.end method

.method public final b(Landroid/support/v4/view/fk;Landroid/view/View;)V
    .registers 5

    .prologue
    .line 452
    .line 1094
    invoke-virtual {p2}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    .line 453
    return-void
.end method

.method public final b(Landroid/support/v4/view/fk;Landroid/view/View;F)V
    .registers 5

    .prologue
    .line 377
    .line 1034
    invoke-virtual {p2}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, p3}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    .line 378
    return-void
.end method

.method public b(Landroid/support/v4/view/fk;Landroid/view/View;Ljava/lang/Runnable;)V
    .registers 5

    .prologue
    .line 514
    new-instance v0, Landroid/support/v4/view/fp;

    invoke-direct {v0, p1}, Landroid/support/v4/view/fp;-><init>(Landroid/support/v4/view/fk;)V

    invoke-static {p2, v0}, Landroid/support/v4/view/fv;->a(Landroid/view/View;Landroid/support/v4/view/gd;)V

    .line 515
    invoke-static {p1, p3}, Landroid/support/v4/view/fk;->b(Landroid/support/v4/view/fk;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 516
    return-void
.end method

.method public final b(Landroid/view/View;J)V
    .registers 6

    .prologue
    .line 397
    .line 1050
    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    .line 398
    return-void
.end method

.method public final c(Landroid/view/View;)J
    .registers 4

    .prologue
    .line 402
    .line 1054
    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->getStartDelay()J

    move-result-wide v0

    .line 402
    return-wide v0
.end method

.method public final c(Landroid/support/v4/view/fk;Landroid/view/View;)V
    .registers 4

    .prologue
    .line 462
    .line 1102
    invoke-virtual {p2}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 463
    return-void
.end method

.method public final c(Landroid/support/v4/view/fk;Landroid/view/View;F)V
    .registers 5

    .prologue
    .line 382
    .line 1038
    invoke-virtual {p2}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, p3}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    .line 383
    return-void
.end method

.method public final d(Landroid/support/v4/view/fk;Landroid/view/View;)V
    .registers 4

    .prologue
    .line 497
    .line 1130
    invoke-virtual {p2}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 498
    return-void
.end method

.method public final d(Landroid/support/v4/view/fk;Landroid/view/View;F)V
    .registers 5

    .prologue
    .line 407
    .line 1058
    invoke-virtual {p2}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, p3}, Landroid/view/ViewPropertyAnimator;->alphaBy(F)Landroid/view/ViewPropertyAnimator;

    .line 408
    return-void
.end method

.method public e(Landroid/support/v4/view/fk;Landroid/view/View;)V
    .registers 4

    .prologue
    .line 520
    invoke-static {p2}, Landroid/support/v4/view/cx;->e(Landroid/view/View;)I

    move-result v0

    invoke-static {p1, v0}, Landroid/support/v4/view/fk;->a(Landroid/support/v4/view/fk;I)I

    .line 521
    new-instance v0, Landroid/support/v4/view/fp;

    invoke-direct {v0, p1}, Landroid/support/v4/view/fp;-><init>(Landroid/support/v4/view/fk;)V

    invoke-static {p2, v0}, Landroid/support/v4/view/fv;->a(Landroid/view/View;Landroid/support/v4/view/gd;)V

    .line 522
    return-void
.end method

.method public final e(Landroid/support/v4/view/fk;Landroid/view/View;F)V
    .registers 5

    .prologue
    .line 412
    .line 1062
    invoke-virtual {p2}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, p3}, Landroid/view/ViewPropertyAnimator;->rotation(F)Landroid/view/ViewPropertyAnimator;

    .line 413
    return-void
.end method

.method public final f(Landroid/support/v4/view/fk;Landroid/view/View;F)V
    .registers 5

    .prologue
    .line 417
    .line 1066
    invoke-virtual {p2}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, p3}, Landroid/view/ViewPropertyAnimator;->rotationBy(F)Landroid/view/ViewPropertyAnimator;

    .line 418
    return-void
.end method

.method public final g(Landroid/support/v4/view/fk;Landroid/view/View;F)V
    .registers 5

    .prologue
    .line 422
    .line 1070
    invoke-virtual {p2}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, p3}, Landroid/view/ViewPropertyAnimator;->rotationX(F)Landroid/view/ViewPropertyAnimator;

    .line 423
    return-void
.end method

.method public final h(Landroid/support/v4/view/fk;Landroid/view/View;F)V
    .registers 5

    .prologue
    .line 427
    .line 1074
    invoke-virtual {p2}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, p3}, Landroid/view/ViewPropertyAnimator;->rotationXBy(F)Landroid/view/ViewPropertyAnimator;

    .line 428
    return-void
.end method

.method public final i(Landroid/support/v4/view/fk;Landroid/view/View;F)V
    .registers 5

    .prologue
    .line 432
    .line 1078
    invoke-virtual {p2}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, p3}, Landroid/view/ViewPropertyAnimator;->rotationY(F)Landroid/view/ViewPropertyAnimator;

    .line 433
    return-void
.end method

.method public final j(Landroid/support/v4/view/fk;Landroid/view/View;F)V
    .registers 5

    .prologue
    .line 437
    .line 1082
    invoke-virtual {p2}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, p3}, Landroid/view/ViewPropertyAnimator;->rotationYBy(F)Landroid/view/ViewPropertyAnimator;

    .line 438
    return-void
.end method

.method public final k(Landroid/support/v4/view/fk;Landroid/view/View;F)V
    .registers 5

    .prologue
    .line 447
    .line 1090
    invoke-virtual {p2}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, p3}, Landroid/view/ViewPropertyAnimator;->scaleXBy(F)Landroid/view/ViewPropertyAnimator;

    .line 448
    return-void
.end method

.method public final l(Landroid/support/v4/view/fk;Landroid/view/View;F)V
    .registers 5

    .prologue
    .line 457
    .line 1098
    invoke-virtual {p2}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, p3}, Landroid/view/ViewPropertyAnimator;->scaleYBy(F)Landroid/view/ViewPropertyAnimator;

    .line 458
    return-void
.end method

.method public final m(Landroid/support/v4/view/fk;Landroid/view/View;F)V
    .registers 5

    .prologue
    .line 467
    .line 1106
    invoke-virtual {p2}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, p3}, Landroid/view/ViewPropertyAnimator;->x(F)Landroid/view/ViewPropertyAnimator;

    .line 468
    return-void
.end method

.method public final n(Landroid/support/v4/view/fk;Landroid/view/View;F)V
    .registers 5

    .prologue
    .line 472
    .line 1110
    invoke-virtual {p2}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, p3}, Landroid/view/ViewPropertyAnimator;->xBy(F)Landroid/view/ViewPropertyAnimator;

    .line 473
    return-void
.end method

.method public final o(Landroid/support/v4/view/fk;Landroid/view/View;F)V
    .registers 5

    .prologue
    .line 477
    .line 1114
    invoke-virtual {p2}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, p3}, Landroid/view/ViewPropertyAnimator;->y(F)Landroid/view/ViewPropertyAnimator;

    .line 478
    return-void
.end method

.method public final p(Landroid/support/v4/view/fk;Landroid/view/View;F)V
    .registers 5

    .prologue
    .line 482
    .line 1118
    invoke-virtual {p2}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, p3}, Landroid/view/ViewPropertyAnimator;->yBy(F)Landroid/view/ViewPropertyAnimator;

    .line 483
    return-void
.end method

.method public final q(Landroid/support/v4/view/fk;Landroid/view/View;F)V
    .registers 5

    .prologue
    .line 487
    .line 1122
    invoke-virtual {p2}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, p3}, Landroid/view/ViewPropertyAnimator;->translationXBy(F)Landroid/view/ViewPropertyAnimator;

    .line 488
    return-void
.end method

.method public final r(Landroid/support/v4/view/fk;Landroid/view/View;F)V
    .registers 5

    .prologue
    .line 492
    .line 1126
    invoke-virtual {p2}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, p3}, Landroid/view/ViewPropertyAnimator;->translationYBy(F)Landroid/view/ViewPropertyAnimator;

    .line 493
    return-void
.end method
