.class public abstract Landroid/support/v4/view/by;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:I = -0x1

.field public static final b:I = -0x2


# instance fields
.field private c:Landroid/database/DataSetObservable;


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    new-instance v0, Landroid/database/DataSetObservable;

    invoke-direct {v0}, Landroid/database/DataSetObservable;-><init>()V

    iput-object v0, p0, Landroid/support/v4/view/by;->c:Landroid/database/DataSetObservable;

    return-void
.end method

.method private static a()V
    .registers 0

    .prologue
    .line 159
    return-void
.end method

.method private static f()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 175
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Required method instantiateItem was not overridden"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static g()V
    .registers 2

    .prologue
    .line 192
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Required method destroyItem was not overridden"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static h()V
    .registers 0

    .prologue
    .line 207
    return-void
.end method

.method private static i()V
    .registers 0

    .prologue
    .line 219
    return-void
.end method

.method private static j()I
    .registers 1

    .prologue
    .line 268
    const/4 v0, -0x1

    return v0
.end method

.method private k()V
    .registers 2

    .prologue
    .line 276
    iget-object v0, p0, Landroid/support/v4/view/by;->c:Landroid/database/DataSetObservable;

    invoke-virtual {v0}, Landroid/database/DataSetObservable;->notifyChanged()V

    .line 277
    return-void
.end method

.method private static l()Ljava/lang/CharSequence;
    .registers 1

    .prologue
    .line 307
    const/4 v0, 0x0

    return-object v0
.end method

.method private static m()F
    .registers 1

    .prologue
    .line 318
    const/high16 v0, 0x3f800000    # 1.0f

    return v0
.end method


# virtual methods
.method public a(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .registers 5

    .prologue
    .line 1175
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Required method instantiateItem was not overridden"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a(ILjava/lang/Object;)V
    .registers 5

    .prologue
    .line 1192
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Required method destroyItem was not overridden"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Landroid/database/DataSetObserver;)V
    .registers 3

    .prologue
    .line 285
    iget-object v0, p0, Landroid/support/v4/view/by;->c:Landroid/database/DataSetObservable;

    invoke-virtual {v0, p1}, Landroid/database/DataSetObservable;->registerObserver(Ljava/lang/Object;)V

    .line 286
    return-void
.end method

.method public a(Landroid/os/Parcelable;Ljava/lang/ClassLoader;)V
    .registers 3

    .prologue
    .line 250
    return-void
.end method

.method public a(Ljava/lang/Object;)V
    .registers 2

    .prologue
    .line 138
    return-void
.end method

.method public abstract a(Landroid/view/View;Ljava/lang/Object;)Z
.end method

.method public b()V
    .registers 1

    .prologue
    .line 96
    return-void
.end method

.method public final b(Landroid/database/DataSetObserver;)V
    .registers 3

    .prologue
    .line 294
    iget-object v0, p0, Landroid/support/v4/view/by;->c:Landroid/database/DataSetObservable;

    invoke-virtual {v0, p1}, Landroid/database/DataSetObservable;->unregisterObserver(Ljava/lang/Object;)V

    .line 295
    return-void
.end method

.method public c()V
    .registers 1

    .prologue
    .line 149
    return-void
.end method

.method public d()Landroid/os/Parcelable;
    .registers 2

    .prologue
    .line 239
    const/4 v0, 0x0

    return-object v0
.end method

.method public abstract e()I
.end method
