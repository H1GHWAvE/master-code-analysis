.class public Landroid/support/v4/n/j;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Ljava/util/LinkedHashMap;

.field private b:I

.field private c:I

.field private d:I

.field private e:I

.field private f:I

.field private g:I

.field private h:I


# direct methods
.method public constructor <init>(I)V
    .registers 6

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    if-gtz p1, :cond_d

    .line 49
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "maxSize <= 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 51
    :cond_d
    iput p1, p0, Landroid/support/v4/n/j;->c:I

    .line 52
    new-instance v0, Ljava/util/LinkedHashMap;

    const/4 v1, 0x0

    const/high16 v2, 0x3f400000    # 0.75f

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Ljava/util/LinkedHashMap;-><init>(IFZ)V

    iput-object v0, p0, Landroid/support/v4/n/j;->a:Ljava/util/LinkedHashMap;

    .line 53
    return-void
.end method

.method private static a()V
    .registers 0

    .prologue
    .line 227
    return-void
.end method

.method private a(I)V
    .registers 4

    .prologue
    .line 61
    if-gtz p1, :cond_a

    .line 62
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "maxSize <= 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 65
    :cond_a
    monitor-enter p0

    .line 66
    :try_start_b
    iput p1, p0, Landroid/support/v4/n/j;->c:I

    .line 67
    monitor-exit p0
    :try_end_e
    .catchall {:try_start_b .. :try_end_e} :catchall_12

    .line 68
    invoke-direct {p0, p1}, Landroid/support/v4/n/j;->b(I)V

    .line 69
    return-void

    .line 67
    :catchall_12
    move-exception v0

    :try_start_13
    monitor-exit p0
    :try_end_14
    .catchall {:try_start_13 .. :try_end_14} :catchall_12

    throw v0
.end method

.method private b(Ljava/lang/Object;Ljava/lang/Object;)I
    .registers 6

    .prologue
    .line 249
    invoke-virtual {p0, p2}, Landroid/support/v4/n/j;->b(Ljava/lang/Object;)I

    move-result v0

    .line 250
    if-gez v0, :cond_25

    .line 251
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Negative size: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 253
    :cond_25
    return v0
.end method

.method private static b()Ljava/lang/Object;
    .registers 1

    .prologue
    .line 245
    const/4 v0, 0x0

    return-object v0
.end method

.method private b(I)V
    .registers 5

    .prologue
    .line 165
    :goto_0
    monitor-enter p0

    .line 166
    :try_start_1
    iget v0, p0, Landroid/support/v4/n/j;->b:I

    if-ltz v0, :cond_11

    iget-object v0, p0, Landroid/support/v4/n/j;->a:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_35

    iget v0, p0, Landroid/support/v4/n/j;->b:I

    if-eqz v0, :cond_35

    .line 167
    :cond_11
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".sizeOf() is reporting inconsistent results!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 181
    :catchall_32
    move-exception v0

    monitor-exit p0
    :try_end_34
    .catchall {:try_start_1 .. :try_end_34} :catchall_32

    throw v0

    .line 171
    :cond_35
    :try_start_35
    iget v0, p0, Landroid/support/v4/n/j;->b:I

    if-le v0, p1, :cond_41

    iget-object v0, p0, Landroid/support/v4/n/j;->a:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_43

    .line 172
    :cond_41
    monitor-exit p0

    return-void

    .line 175
    :cond_43
    iget-object v0, p0, Landroid/support/v4/n/j;->a:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 176
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    .line 177
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    .line 178
    iget-object v2, p0, Landroid/support/v4/n/j;->a:Ljava/util/LinkedHashMap;

    invoke-virtual {v2, v1}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 179
    iget v2, p0, Landroid/support/v4/n/j;->b:I

    invoke-direct {p0, v1, v0}, Landroid/support/v4/n/j;->b(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    sub-int v0, v2, v0

    iput v0, p0, Landroid/support/v4/n/j;->b:I

    .line 180
    iget v0, p0, Landroid/support/v4/n/j;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Landroid/support/v4/n/j;->f:I

    .line 181
    monitor-exit p0
    :try_end_71
    .catchall {:try_start_35 .. :try_end_71} :catchall_32

    goto :goto_0
.end method

.method private c(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 5

    .prologue
    .line 193
    if-nez p1, :cond_a

    .line 194
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "key == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 198
    :cond_a
    monitor-enter p0

    .line 199
    :try_start_b
    iget-object v0, p0, Landroid/support/v4/n/j;->a:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 200
    if-eqz v0, :cond_1c

    .line 201
    iget v1, p0, Landroid/support/v4/n/j;->b:I

    invoke-direct {p0, p1, v0}, Landroid/support/v4/n/j;->b(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v2

    sub-int/2addr v1, v2

    iput v1, p0, Landroid/support/v4/n/j;->b:I

    .line 203
    :cond_1c
    monitor-exit p0

    .line 209
    return-object v0

    .line 203
    :catchall_1e
    move-exception v0

    monitor-exit p0
    :try_end_20
    .catchall {:try_start_b .. :try_end_20} :catchall_1e

    throw v0
.end method

.method private c()V
    .registers 2

    .prologue
    .line 271
    const/4 v0, -0x1

    invoke-direct {p0, v0}, Landroid/support/v4/n/j;->b(I)V

    .line 272
    return-void
.end method

.method private declared-synchronized d()I
    .registers 2

    .prologue
    .line 280
    monitor-enter p0

    :try_start_1
    iget v0, p0, Landroid/support/v4/n/j;->b:I
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    monitor-exit p0

    return v0

    :catchall_5
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized e()I
    .registers 2

    .prologue
    .line 289
    monitor-enter p0

    :try_start_1
    iget v0, p0, Landroid/support/v4/n/j;->c:I
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    monitor-exit p0

    return v0

    :catchall_5
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized f()I
    .registers 2

    .prologue
    .line 297
    monitor-enter p0

    :try_start_1
    iget v0, p0, Landroid/support/v4/n/j;->g:I
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    monitor-exit p0

    return v0

    :catchall_5
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized g()I
    .registers 2

    .prologue
    .line 305
    monitor-enter p0

    :try_start_1
    iget v0, p0, Landroid/support/v4/n/j;->h:I
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    monitor-exit p0

    return v0

    :catchall_5
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized h()I
    .registers 2

    .prologue
    .line 312
    monitor-enter p0

    :try_start_1
    iget v0, p0, Landroid/support/v4/n/j;->e:I
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    monitor-exit p0

    return v0

    :catchall_5
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized i()I
    .registers 2

    .prologue
    .line 319
    monitor-enter p0

    :try_start_1
    iget v0, p0, Landroid/support/v4/n/j;->d:I
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    monitor-exit p0

    return v0

    :catchall_5
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized j()I
    .registers 2

    .prologue
    .line 326
    monitor-enter p0

    :try_start_1
    iget v0, p0, Landroid/support/v4/n/j;->f:I
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    monitor-exit p0

    return v0

    :catchall_5
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized k()Ljava/util/Map;
    .registers 3

    .prologue
    .line 334
    monitor-enter p0

    :try_start_1
    new-instance v0, Ljava/util/LinkedHashMap;

    iget-object v1, p0, Landroid/support/v4/n/j;->a:Ljava/util/LinkedHashMap;

    invoke-direct {v0, v1}, Ljava/util/LinkedHashMap;-><init>(Ljava/util/Map;)V
    :try_end_8
    .catchall {:try_start_1 .. :try_end_8} :catchall_a

    monitor-exit p0

    return-object v0

    :catchall_a
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 78
    if-nez p1, :cond_a

    .line 79
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "key == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 83
    :cond_a
    monitor-enter p0

    .line 84
    :try_start_b
    iget-object v0, p0, Landroid/support/v4/n/j;->a:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 85
    if-eqz v0, :cond_1b

    .line 86
    iget v1, p0, Landroid/support/v4/n/j;->g:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Landroid/support/v4/n/j;->g:I

    .line 87
    monitor-exit p0

    .line 101
    :goto_1a
    return-object v0

    .line 89
    :cond_1b
    iget v0, p0, Landroid/support/v4/n/j;->h:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Landroid/support/v4/n/j;->h:I

    .line 90
    monitor-exit p0

    .line 101
    const/4 v0, 0x0

    goto :goto_1a

    .line 90
    :catchall_24
    move-exception v0

    monitor-exit p0
    :try_end_26
    .catchall {:try_start_b .. :try_end_26} :catchall_24

    throw v0
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 6

    .prologue
    .line 132
    if-eqz p1, :cond_4

    if-nez p2, :cond_c

    .line 133
    :cond_4
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "key == null || value == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 137
    :cond_c
    monitor-enter p0

    .line 138
    :try_start_d
    iget v0, p0, Landroid/support/v4/n/j;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Landroid/support/v4/n/j;->d:I

    .line 139
    iget v0, p0, Landroid/support/v4/n/j;->b:I

    invoke-direct {p0, p1, p2}, Landroid/support/v4/n/j;->b(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Landroid/support/v4/n/j;->b:I

    .line 140
    iget-object v0, p0, Landroid/support/v4/n/j;->a:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 141
    if-eqz v0, :cond_2d

    .line 142
    iget v1, p0, Landroid/support/v4/n/j;->b:I

    invoke-direct {p0, p1, v0}, Landroid/support/v4/n/j;->b(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v2

    sub-int/2addr v1, v2

    iput v1, p0, Landroid/support/v4/n/j;->b:I

    .line 144
    :cond_2d
    monitor-exit p0
    :try_end_2e
    .catchall {:try_start_d .. :try_end_2e} :catchall_34

    .line 150
    iget v1, p0, Landroid/support/v4/n/j;->c:I

    invoke-direct {p0, v1}, Landroid/support/v4/n/j;->b(I)V

    .line 151
    return-object v0

    .line 144
    :catchall_34
    move-exception v0

    :try_start_35
    monitor-exit p0
    :try_end_36
    .catchall {:try_start_35 .. :try_end_36} :catchall_34

    throw v0
.end method

.method public b(Ljava/lang/Object;)I
    .registers 3

    .prologue
    .line 264
    const/4 v0, 0x1

    return v0
.end method

.method public final declared-synchronized toString()Ljava/lang/String;
    .registers 6

    .prologue
    const/4 v0, 0x0

    .line 338
    monitor-enter p0

    :try_start_2
    iget v1, p0, Landroid/support/v4/n/j;->g:I

    iget v2, p0, Landroid/support/v4/n/j;->h:I

    add-int/2addr v1, v2

    .line 339
    if-eqz v1, :cond_e

    iget v0, p0, Landroid/support/v4/n/j;->g:I

    mul-int/lit8 v0, v0, 0x64

    div-int/2addr v0, v1

    .line 340
    :cond_e
    const-string v1, "LruCache[maxSize=%d,hits=%d,misses=%d,hitRate=%d%%]"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Landroid/support/v4/n/j;->c:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget v4, p0, Landroid/support/v4/n/j;->g:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget v4, p0, Landroid/support/v4/n/j;->h:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_38
    .catchall {:try_start_2 .. :try_end_38} :catchall_3b

    move-result-object v0

    monitor-exit p0

    return-object v0

    .line 338
    :catchall_3b
    move-exception v0

    monitor-exit p0

    throw v0
.end method
