.class public final Landroid/support/v4/media/t;
.super Landroid/support/v4/media/s;
.source "SourceFile"


# static fields
.field public static final i:I = 0x7e

.field public static final j:I = 0x7f

.field public static final k:I = 0x82

.field public static final l:I = 0x1

.field public static final m:I = 0x2

.field public static final n:I = 0x4

.field public static final o:I = 0x8

.field public static final p:I = 0x10

.field public static final q:I = 0x20

.field public static final r:I = 0x40

.field public static final s:I = 0x80


# instance fields
.field final a:Landroid/content/Context;

.field final b:Landroid/support/v4/media/ac;

.field final c:Landroid/media/AudioManager;

.field final d:Landroid/view/View;

.field final e:Ljava/lang/Object;

.field final f:Landroid/support/v4/media/x;

.field final g:Ljava/util/ArrayList;

.field final h:Landroid/support/v4/media/w;

.field final t:Landroid/view/KeyEvent$Callback;


# direct methods
.method private constructor <init>(Landroid/app/Activity;Landroid/support/v4/media/ac;)V
    .registers 4

    .prologue
    .line 152
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Landroid/support/v4/media/t;-><init>(Landroid/app/Activity;Landroid/view/View;Landroid/support/v4/media/ac;)V

    .line 153
    return-void
.end method

.method private constructor <init>(Landroid/app/Activity;Landroid/view/View;Landroid/support/v4/media/ac;)V
    .registers 9

    .prologue
    .line 159
    invoke-direct {p0}, Landroid/support/v4/media/s;-><init>()V

    .line 55
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v4/media/t;->g:Ljava/util/ArrayList;

    .line 57
    new-instance v0, Landroid/support/v4/media/u;

    invoke-direct {v0, p0}, Landroid/support/v4/media/u;-><init>(Landroid/support/v4/media/t;)V

    iput-object v0, p0, Landroid/support/v4/media/t;->h:Landroid/support/v4/media/w;

    .line 130
    new-instance v0, Landroid/support/v4/media/v;

    invoke-direct {v0, p0}, Landroid/support/v4/media/v;-><init>(Landroid/support/v4/media/t;)V

    iput-object v0, p0, Landroid/support/v4/media/t;->t:Landroid/view/KeyEvent$Callback;

    .line 160
    if-eqz p1, :cond_55

    move-object v0, p1

    :goto_1b
    iput-object v0, p0, Landroid/support/v4/media/t;->a:Landroid/content/Context;

    .line 161
    iput-object p3, p0, Landroid/support/v4/media/t;->b:Landroid/support/v4/media/ac;

    .line 162
    iget-object v0, p0, Landroid/support/v4/media/t;->a:Landroid/content/Context;

    const-string v1, "audio"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Landroid/support/v4/media/t;->c:Landroid/media/AudioManager;

    .line 163
    if-eqz p1, :cond_35

    invoke-virtual {p1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object p2

    :cond_35
    iput-object p2, p0, Landroid/support/v4/media/t;->d:Landroid/view/View;

    .line 164
    iget-object v0, p0, Landroid/support/v4/media/t;->d:Landroid/view/View;

    invoke-static {v0}, Landroid/support/v4/view/ab;->a(Landroid/view/View;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/media/t;->e:Ljava/lang/Object;

    .line 165
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-lt v0, v1, :cond_5a

    .line 166
    new-instance v0, Landroid/support/v4/media/x;

    iget-object v1, p0, Landroid/support/v4/media/t;->a:Landroid/content/Context;

    iget-object v2, p0, Landroid/support/v4/media/t;->c:Landroid/media/AudioManager;

    iget-object v3, p0, Landroid/support/v4/media/t;->d:Landroid/view/View;

    iget-object v4, p0, Landroid/support/v4/media/t;->h:Landroid/support/v4/media/w;

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/support/v4/media/x;-><init>(Landroid/content/Context;Landroid/media/AudioManager;Landroid/view/View;Landroid/support/v4/media/w;)V

    iput-object v0, p0, Landroid/support/v4/media/t;->f:Landroid/support/v4/media/x;

    .line 171
    :goto_54
    return-void

    .line 160
    :cond_55
    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    goto :goto_1b

    .line 169
    :cond_5a
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v4/media/t;->f:Landroid/support/v4/media/x;

    goto :goto_54
.end method

.method private constructor <init>(Landroid/view/View;Landroid/support/v4/media/ac;)V
    .registers 4

    .prologue
    .line 156
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1, p2}, Landroid/support/v4/media/t;-><init>(Landroid/app/Activity;Landroid/view/View;Landroid/support/v4/media/ac;)V

    .line 157
    return-void
.end method

.method static a(I)Z
    .registers 2

    .prologue
    .line 112
    sparse-switch p0, :sswitch_data_8

    .line 127
    const/4 v0, 0x0

    :goto_4
    return v0

    .line 124
    :sswitch_5
    const/4 v0, 0x1

    goto :goto_4

    .line 112
    nop

    :sswitch_data_8
    .sparse-switch
        0x4f -> :sswitch_5
        0x55 -> :sswitch_5
        0x56 -> :sswitch_5
        0x57 -> :sswitch_5
        0x58 -> :sswitch_5
        0x59 -> :sswitch_5
        0x5a -> :sswitch_5
        0x5b -> :sswitch_5
        0x7e -> :sswitch_5
        0x7f -> :sswitch_5
        0x82 -> :sswitch_5
    .end sparse-switch
.end method

.method private a(Landroid/view/KeyEvent;)Z
    .registers 4

    .prologue
    .line 200
    iget-object v0, p0, Landroid/support/v4/media/t;->t:Landroid/view/KeyEvent$Callback;

    iget-object v1, p0, Landroid/support/v4/media/t;->e:Ljava/lang/Object;

    invoke-static {p1, v0, v1, p0}, Landroid/support/v4/view/ab;->a(Landroid/view/KeyEvent;Landroid/view/KeyEvent$Callback;Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private j()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 190
    iget-object v0, p0, Landroid/support/v4/media/t;->f:Landroid/support/v4/media/x;

    if-eqz v0, :cond_9

    iget-object v0, p0, Landroid/support/v4/media/t;->f:Landroid/support/v4/media/x;

    .line 1101
    iget-object v0, v0, Landroid/support/v4/media/x;->m:Landroid/media/RemoteControlClient;

    .line 190
    :goto_8
    return-object v0

    :cond_9
    const/4 v0, 0x0

    goto :goto_8
.end method

.method private k()[Landroid/support/v4/media/ad;
    .registers 3

    .prologue
    .line 212
    iget-object v0, p0, Landroid/support/v4/media/t;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gtz v0, :cond_a

    .line 213
    const/4 v0, 0x0

    .line 217
    :goto_9
    return-object v0

    .line 215
    :cond_a
    iget-object v0, p0, Landroid/support/v4/media/t;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Landroid/support/v4/media/ad;

    .line 216
    iget-object v1, p0, Landroid/support/v4/media/t;->g:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    goto :goto_9
.end method

.method private l()V
    .registers 1

    .prologue
    .line 221
    invoke-direct {p0}, Landroid/support/v4/media/t;->k()[Landroid/support/v4/media/ad;

    .line 227
    return-void
.end method

.method private m()V
    .registers 1

    .prologue
    .line 230
    invoke-direct {p0}, Landroid/support/v4/media/t;->k()[Landroid/support/v4/media/ad;

    .line 236
    return-void
.end method

.method private n()V
    .registers 8

    .prologue
    .line 239
    iget-object v0, p0, Landroid/support/v4/media/t;->f:Landroid/support/v4/media/x;

    if-eqz v0, :cond_2a

    .line 240
    iget-object v2, p0, Landroid/support/v4/media/t;->f:Landroid/support/v4/media/x;

    iget-object v0, p0, Landroid/support/v4/media/t;->b:Landroid/support/v4/media/ac;

    invoke-virtual {v0}, Landroid/support/v4/media/ac;->g()Z

    move-result v3

    iget-object v0, p0, Landroid/support/v4/media/t;->b:Landroid/support/v4/media/ac;

    invoke-virtual {v0}, Landroid/support/v4/media/ac;->e()J

    move-result-wide v4

    .line 1159
    iget-object v0, v2, Landroid/support/v4/media/x;->m:Landroid/media/RemoteControlClient;

    if-eqz v0, :cond_2a

    .line 1160
    iget-object v6, v2, Landroid/support/v4/media/x;->m:Landroid/media/RemoteControlClient;

    if-eqz v3, :cond_2b

    const/4 v0, 0x3

    move v1, v0

    :goto_1c
    if-eqz v3, :cond_2e

    const/high16 v0, 0x3f800000    # 1.0f

    :goto_20
    invoke-virtual {v6, v1, v4, v5, v0}, Landroid/media/RemoteControlClient;->setPlaybackState(IJF)V

    .line 1162
    iget-object v0, v2, Landroid/support/v4/media/x;->m:Landroid/media/RemoteControlClient;

    const/16 v1, 0x3c

    invoke-virtual {v0, v1}, Landroid/media/RemoteControlClient;->setTransportControlFlags(I)V

    .line 244
    :cond_2a
    return-void

    .line 1160
    :cond_2b
    const/4 v0, 0x1

    move v1, v0

    goto :goto_1c

    :cond_2e
    const/4 v0, 0x0

    goto :goto_20
.end method

.method private o()V
    .registers 1

    .prologue
    .line 247
    invoke-direct {p0}, Landroid/support/v4/media/t;->n()V

    .line 1221
    invoke-direct {p0}, Landroid/support/v4/media/t;->k()[Landroid/support/v4/media/ad;

    .line 1230
    invoke-direct {p0}, Landroid/support/v4/media/t;->k()[Landroid/support/v4/media/ad;

    .line 250
    return-void
.end method

.method private p()V
    .registers 4

    .prologue
    .line 341
    iget-object v0, p0, Landroid/support/v4/media/t;->f:Landroid/support/v4/media/x;

    .line 5105
    invoke-virtual {v0}, Landroid/support/v4/media/x;->d()V

    .line 5106
    iget-object v1, v0, Landroid/support/v4/media/x;->c:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    iget-object v2, v0, Landroid/support/v4/media/x;->h:Landroid/view/ViewTreeObserver$OnWindowAttachListener;

    invoke-virtual {v1, v2}, Landroid/view/ViewTreeObserver;->removeOnWindowAttachListener(Landroid/view/ViewTreeObserver$OnWindowAttachListener;)V

    .line 5107
    iget-object v1, v0, Landroid/support/v4/media/x;->c:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    iget-object v0, v0, Landroid/support/v4/media/x;->i:Landroid/view/ViewTreeObserver$OnWindowFocusChangeListener;

    invoke-virtual {v1, v0}, Landroid/view/ViewTreeObserver;->removeOnWindowFocusChangeListener(Landroid/view/ViewTreeObserver$OnWindowFocusChangeListener;)V

    .line 342
    return-void
.end method


# virtual methods
.method public final a()V
    .registers 4

    .prologue
    const/4 v2, 0x3

    .line 258
    iget-object v0, p0, Landroid/support/v4/media/t;->f:Landroid/support/v4/media/x;

    if-eqz v0, :cond_19

    .line 259
    iget-object v0, p0, Landroid/support/v4/media/t;->f:Landroid/support/v4/media/x;

    .line 2139
    iget v1, v0, Landroid/support/v4/media/x;->o:I

    if-eq v1, v2, :cond_12

    .line 2140
    iput v2, v0, Landroid/support/v4/media/x;->o:I

    .line 2141
    iget-object v1, v0, Landroid/support/v4/media/x;->m:Landroid/media/RemoteControlClient;

    invoke-virtual {v1, v2}, Landroid/media/RemoteControlClient;->setPlaybackState(I)V

    .line 2143
    :cond_12
    iget-boolean v1, v0, Landroid/support/v4/media/x;->n:Z

    if-eqz v1, :cond_19

    .line 2144
    invoke-virtual {v0}, Landroid/support/v4/media/x;->a()V

    .line 262
    :cond_19
    invoke-direct {p0}, Landroid/support/v4/media/t;->n()V

    .line 2221
    invoke-direct {p0}, Landroid/support/v4/media/t;->k()[Landroid/support/v4/media/ad;

    .line 264
    return-void
.end method

.method public final a(Landroid/support/v4/media/ad;)V
    .registers 3

    .prologue
    .line 204
    iget-object v0, p0, Landroid/support/v4/media/t;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 205
    return-void
.end method

.method public final b()V
    .registers 5

    .prologue
    const/4 v3, 0x2

    .line 272
    iget-object v0, p0, Landroid/support/v4/media/t;->f:Landroid/support/v4/media/x;

    if-eqz v0, :cond_16

    .line 273
    iget-object v0, p0, Landroid/support/v4/media/t;->f:Landroid/support/v4/media/x;

    .line 3167
    iget v1, v0, Landroid/support/v4/media/x;->o:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_13

    .line 3168
    iput v3, v0, Landroid/support/v4/media/x;->o:I

    .line 3169
    iget-object v1, v0, Landroid/support/v4/media/x;->m:Landroid/media/RemoteControlClient;

    invoke-virtual {v1, v3}, Landroid/media/RemoteControlClient;->setPlaybackState(I)V

    .line 3171
    :cond_13
    invoke-virtual {v0}, Landroid/support/v4/media/x;->b()V

    .line 276
    :cond_16
    invoke-direct {p0}, Landroid/support/v4/media/t;->n()V

    .line 3221
    invoke-direct {p0}, Landroid/support/v4/media/t;->k()[Landroid/support/v4/media/ad;

    .line 278
    return-void
.end method

.method public final b(Landroid/support/v4/media/ad;)V
    .registers 3

    .prologue
    .line 208
    iget-object v0, p0, Landroid/support/v4/media/t;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 209
    return-void
.end method

.method public final c()V
    .registers 4

    .prologue
    const/4 v2, 0x1

    .line 286
    iget-object v0, p0, Landroid/support/v4/media/t;->f:Landroid/support/v4/media/x;

    if-eqz v0, :cond_15

    .line 287
    iget-object v0, p0, Landroid/support/v4/media/t;->f:Landroid/support/v4/media/x;

    .line 4175
    iget v1, v0, Landroid/support/v4/media/x;->o:I

    if-eq v1, v2, :cond_12

    .line 4176
    iput v2, v0, Landroid/support/v4/media/x;->o:I

    .line 4177
    iget-object v1, v0, Landroid/support/v4/media/x;->m:Landroid/media/RemoteControlClient;

    invoke-virtual {v1, v2}, Landroid/media/RemoteControlClient;->setPlaybackState(I)V

    .line 4179
    :cond_12
    invoke-virtual {v0}, Landroid/support/v4/media/x;->b()V

    .line 290
    :cond_15
    invoke-direct {p0}, Landroid/support/v4/media/t;->n()V

    .line 4221
    invoke-direct {p0}, Landroid/support/v4/media/t;->k()[Landroid/support/v4/media/ad;

    .line 292
    return-void
.end method

.method public final d()J
    .registers 3

    .prologue
    .line 296
    iget-object v0, p0, Landroid/support/v4/media/t;->b:Landroid/support/v4/media/ac;

    invoke-virtual {v0}, Landroid/support/v4/media/ac;->d()J

    move-result-wide v0

    return-wide v0
.end method

.method public final e()J
    .registers 3

    .prologue
    .line 301
    iget-object v0, p0, Landroid/support/v4/media/t;->b:Landroid/support/v4/media/ac;

    invoke-virtual {v0}, Landroid/support/v4/media/ac;->e()J

    move-result-wide v0

    return-wide v0
.end method

.method public final f()V
    .registers 1

    .prologue
    .line 307
    return-void
.end method

.method public final g()Z
    .registers 2

    .prologue
    .line 311
    iget-object v0, p0, Landroid/support/v4/media/t;->b:Landroid/support/v4/media/ac;

    invoke-virtual {v0}, Landroid/support/v4/media/ac;->g()Z

    move-result v0

    return v0
.end method

.method public final h()I
    .registers 2

    .prologue
    .line 316
    const/16 v0, 0x64

    return v0
.end method

.method public final i()I
    .registers 2

    .prologue
    .line 332
    const/16 v0, 0x3c

    return v0
.end method
