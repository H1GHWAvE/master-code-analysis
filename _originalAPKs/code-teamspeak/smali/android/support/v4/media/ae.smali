.class public abstract Landroid/support/v4/media/ae;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:I = 0x0

.field public static final b:I = 0x1

.field public static final c:I = 0x2


# instance fields
.field public final d:I

.field public final e:I

.field public f:I

.field public g:Landroid/support/v4/media/ag;

.field private h:Ljava/lang/Object;


# direct methods
.method private constructor <init>(III)V
    .registers 4

    .prologue
    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    iput p1, p0, Landroid/support/v4/media/ae;->d:I

    .line 79
    iput p2, p0, Landroid/support/v4/media/ae;->e:I

    .line 80
    iput p3, p0, Landroid/support/v4/media/ae;->f:I

    .line 81
    return-void
.end method

.method private a(I)V
    .registers 3

    .prologue
    .line 118
    iput p1, p0, Landroid/support/v4/media/ae;->f:I

    .line 119
    invoke-virtual {p0}, Landroid/support/v4/media/ae;->a()Ljava/lang/Object;

    move-result-object v0

    .line 120
    if-eqz v0, :cond_d

    .line 1038
    check-cast v0, Landroid/media/VolumeProvider;

    invoke-virtual {v0, p1}, Landroid/media/VolumeProvider;->setCurrentVolume(I)V

    .line 123
    :cond_d
    iget-object v0, p0, Landroid/support/v4/media/ae;->g:Landroid/support/v4/media/ag;

    if-eqz v0, :cond_16

    .line 124
    iget-object v0, p0, Landroid/support/v4/media/ae;->g:Landroid/support/v4/media/ag;

    invoke-virtual {v0, p0}, Landroid/support/v4/media/ag;->a(Landroid/support/v4/media/ae;)V

    .line 126
    :cond_16
    return-void
.end method

.method private a(Landroid/support/v4/media/ag;)V
    .registers 2

    .prologue
    .line 151
    iput-object p1, p0, Landroid/support/v4/media/ae;->g:Landroid/support/v4/media/ag;

    .line 152
    return-void
.end method

.method private b()I
    .registers 2

    .prologue
    .line 89
    iget v0, p0, Landroid/support/v4/media/ae;->f:I

    return v0
.end method

.method private c()I
    .registers 2

    .prologue
    .line 99
    iget v0, p0, Landroid/support/v4/media/ae;->d:I

    return v0
.end method

.method private d()I
    .registers 2

    .prologue
    .line 108
    iget v0, p0, Landroid/support/v4/media/ae;->e:I

    return v0
.end method

.method private static e()V
    .registers 0

    .prologue
    .line 134
    return-void
.end method

.method private static f()V
    .registers 0

    .prologue
    .line 142
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Object;
    .registers 6

    .prologue
    .line 163
    iget-object v0, p0, Landroid/support/v4/media/ae;->h:Ljava/lang/Object;

    if-nez v0, :cond_a

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-ge v0, v1, :cond_d

    .line 164
    :cond_a
    iget-object v0, p0, Landroid/support/v4/media/ae;->h:Ljava/lang/Object;

    .line 180
    :goto_c
    return-object v0

    .line 167
    :cond_d
    iget v0, p0, Landroid/support/v4/media/ae;->d:I

    iget v1, p0, Landroid/support/v4/media/ae;->e:I

    iget v2, p0, Landroid/support/v4/media/ae;->f:I

    new-instance v3, Landroid/support/v4/media/af;

    invoke-direct {v3, p0}, Landroid/support/v4/media/af;-><init>(Landroid/support/v4/media/ae;)V

    .line 2024
    new-instance v4, Landroid/support/v4/media/aj;

    invoke-direct {v4, v0, v1, v2, v3}, Landroid/support/v4/media/aj;-><init>(IIILandroid/support/v4/media/ak;)V

    .line 167
    iput-object v4, p0, Landroid/support/v4/media/ae;->h:Ljava/lang/Object;

    .line 180
    iget-object v0, p0, Landroid/support/v4/media/ae;->h:Ljava/lang/Object;

    goto :goto_c
.end method
