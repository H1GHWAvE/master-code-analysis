.class public final Landroid/support/v4/media/b;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:Ljava/lang/String;

.field b:Ljava/lang/CharSequence;

.field c:Ljava/lang/CharSequence;

.field d:Ljava/lang/CharSequence;

.field e:Landroid/graphics/Bitmap;

.field f:Landroid/net/Uri;

.field g:Landroid/os/Bundle;

.field h:Landroid/net/Uri;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 296
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 297
    return-void
.end method

.method private a(Landroid/graphics/Bitmap;)Landroid/support/v4/media/b;
    .registers 2
    .param p1    # Landroid/graphics/Bitmap;
        .annotation build Landroid/support/a/z;
        .end annotation
    .end param

    .prologue
    .line 352
    iput-object p1, p0, Landroid/support/v4/media/b;->e:Landroid/graphics/Bitmap;

    .line 353
    return-object p0
.end method

.method private a(Landroid/net/Uri;)Landroid/support/v4/media/b;
    .registers 2
    .param p1    # Landroid/net/Uri;
        .annotation build Landroid/support/a/z;
        .end annotation
    .end param

    .prologue
    .line 364
    iput-object p1, p0, Landroid/support/v4/media/b;->f:Landroid/net/Uri;

    .line 365
    return-object p0
.end method

.method private a(Landroid/os/Bundle;)Landroid/support/v4/media/b;
    .registers 2
    .param p1    # Landroid/os/Bundle;
        .annotation build Landroid/support/a/z;
        .end annotation
    .end param

    .prologue
    .line 375
    iput-object p1, p0, Landroid/support/v4/media/b;->g:Landroid/os/Bundle;

    .line 376
    return-object p0
.end method

.method private a(Ljava/lang/CharSequence;)Landroid/support/v4/media/b;
    .registers 2
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Landroid/support/a/z;
        .end annotation
    .end param

    .prologue
    .line 317
    iput-object p1, p0, Landroid/support/v4/media/b;->b:Ljava/lang/CharSequence;

    .line 318
    return-object p0
.end method

.method private a(Ljava/lang/String;)Landroid/support/v4/media/b;
    .registers 2
    .param p1    # Ljava/lang/String;
        .annotation build Landroid/support/a/z;
        .end annotation
    .end param

    .prologue
    .line 306
    iput-object p1, p0, Landroid/support/v4/media/b;->a:Ljava/lang/String;

    .line 307
    return-object p0
.end method

.method private b(Landroid/net/Uri;)Landroid/support/v4/media/b;
    .registers 2
    .param p1    # Landroid/net/Uri;
        .annotation build Landroid/support/a/z;
        .end annotation
    .end param

    .prologue
    .line 386
    iput-object p1, p0, Landroid/support/v4/media/b;->h:Landroid/net/Uri;

    .line 387
    return-object p0
.end method

.method private b(Ljava/lang/CharSequence;)Landroid/support/v4/media/b;
    .registers 2
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Landroid/support/a/z;
        .end annotation
    .end param

    .prologue
    .line 328
    iput-object p1, p0, Landroid/support/v4/media/b;->c:Ljava/lang/CharSequence;

    .line 329
    return-object p0
.end method

.method private c(Ljava/lang/CharSequence;)Landroid/support/v4/media/b;
    .registers 2
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Landroid/support/a/z;
        .end annotation
    .end param

    .prologue
    .line 340
    iput-object p1, p0, Landroid/support/v4/media/b;->d:Ljava/lang/CharSequence;

    .line 341
    return-object p0
.end method


# virtual methods
.method public final a()Landroid/support/v4/media/MediaDescriptionCompat;
    .registers 11

    .prologue
    .line 397
    new-instance v0, Landroid/support/v4/media/MediaDescriptionCompat;

    iget-object v1, p0, Landroid/support/v4/media/b;->a:Ljava/lang/String;

    iget-object v2, p0, Landroid/support/v4/media/b;->b:Ljava/lang/CharSequence;

    iget-object v3, p0, Landroid/support/v4/media/b;->c:Ljava/lang/CharSequence;

    iget-object v4, p0, Landroid/support/v4/media/b;->d:Ljava/lang/CharSequence;

    iget-object v5, p0, Landroid/support/v4/media/b;->e:Landroid/graphics/Bitmap;

    iget-object v6, p0, Landroid/support/v4/media/b;->f:Landroid/net/Uri;

    iget-object v7, p0, Landroid/support/v4/media/b;->g:Landroid/os/Bundle;

    iget-object v8, p0, Landroid/support/v4/media/b;->h:Landroid/net/Uri;

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Landroid/support/v4/media/MediaDescriptionCompat;-><init>(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/graphics/Bitmap;Landroid/net/Uri;Landroid/os/Bundle;Landroid/net/Uri;B)V

    return-object v0
.end method
