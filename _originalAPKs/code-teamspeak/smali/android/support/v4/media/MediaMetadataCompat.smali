.class public final Landroid/support/v4/media/MediaMetadataCompat;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final A:Ljava/lang/String; = "android.media.metadata.MEDIA_ID"

.field public static final B:Landroid/support/v4/n/a;

.field public static final CREATOR:Landroid/os/Parcelable$Creator;

.field private static final E:Ljava/lang/String; = "MediaMetadata"

.field private static final F:I = 0x0

.field private static final G:I = 0x1

.field private static final H:I = 0x2

.field private static final I:I = 0x3

.field private static final J:[Ljava/lang/String;

.field private static final K:[Ljava/lang/String;

.field private static final L:[Ljava/lang/String;

.field public static final a:Ljava/lang/String; = "android.media.metadata.TITLE"

.field public static final b:Ljava/lang/String; = "android.media.metadata.ARTIST"

.field public static final c:Ljava/lang/String; = "android.media.metadata.DURATION"

.field public static final d:Ljava/lang/String; = "android.media.metadata.ALBUM"

.field public static final e:Ljava/lang/String; = "android.media.metadata.AUTHOR"

.field public static final f:Ljava/lang/String; = "android.media.metadata.WRITER"

.field public static final g:Ljava/lang/String; = "android.media.metadata.COMPOSER"

.field public static final h:Ljava/lang/String; = "android.media.metadata.COMPILATION"

.field public static final i:Ljava/lang/String; = "android.media.metadata.DATE"

.field public static final j:Ljava/lang/String; = "android.media.metadata.YEAR"

.field public static final k:Ljava/lang/String; = "android.media.metadata.GENRE"

.field public static final l:Ljava/lang/String; = "android.media.metadata.TRACK_NUMBER"

.field public static final m:Ljava/lang/String; = "android.media.metadata.NUM_TRACKS"

.field public static final n:Ljava/lang/String; = "android.media.metadata.DISC_NUMBER"

.field public static final o:Ljava/lang/String; = "android.media.metadata.ALBUM_ARTIST"

.field public static final p:Ljava/lang/String; = "android.media.metadata.ART"

.field public static final q:Ljava/lang/String; = "android.media.metadata.ART_URI"

.field public static final r:Ljava/lang/String; = "android.media.metadata.ALBUM_ART"

.field public static final s:Ljava/lang/String; = "android.media.metadata.ALBUM_ART_URI"

.field public static final t:Ljava/lang/String; = "android.media.metadata.USER_RATING"

.field public static final u:Ljava/lang/String; = "android.media.metadata.RATING"

.field public static final v:Ljava/lang/String; = "android.media.metadata.DISPLAY_TITLE"

.field public static final w:Ljava/lang/String; = "android.media.metadata.DISPLAY_SUBTITLE"

.field public static final x:Ljava/lang/String; = "android.media.metadata.DISPLAY_DESCRIPTION"

.field public static final y:Ljava/lang/String; = "android.media.metadata.DISPLAY_ICON"

.field public static final z:Ljava/lang/String; = "android.media.metadata.DISPLAY_ICON_URI"


# instance fields
.field public final C:Landroid/os/Bundle;

.field public D:Ljava/lang/Object;

.field private M:Landroid/support/v4/media/MediaDescriptionCompat;


# direct methods
.method static constructor <clinit>()V
    .registers 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 240
    new-instance v0, Landroid/support/v4/n/a;

    invoke-direct {v0}, Landroid/support/v4/n/a;-><init>()V

    .line 241
    sput-object v0, Landroid/support/v4/media/MediaMetadataCompat;->B:Landroid/support/v4/n/a;

    const-string v1, "android.media.metadata.TITLE"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/n/a;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 242
    sget-object v0, Landroid/support/v4/media/MediaMetadataCompat;->B:Landroid/support/v4/n/a;

    const-string v1, "android.media.metadata.ARTIST"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/n/a;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 243
    sget-object v0, Landroid/support/v4/media/MediaMetadataCompat;->B:Landroid/support/v4/n/a;

    const-string v1, "android.media.metadata.DURATION"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/n/a;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 244
    sget-object v0, Landroid/support/v4/media/MediaMetadataCompat;->B:Landroid/support/v4/n/a;

    const-string v1, "android.media.metadata.ALBUM"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/n/a;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 245
    sget-object v0, Landroid/support/v4/media/MediaMetadataCompat;->B:Landroid/support/v4/n/a;

    const-string v1, "android.media.metadata.AUTHOR"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/n/a;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 246
    sget-object v0, Landroid/support/v4/media/MediaMetadataCompat;->B:Landroid/support/v4/n/a;

    const-string v1, "android.media.metadata.WRITER"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/n/a;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 247
    sget-object v0, Landroid/support/v4/media/MediaMetadataCompat;->B:Landroid/support/v4/n/a;

    const-string v1, "android.media.metadata.COMPOSER"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/n/a;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 248
    sget-object v0, Landroid/support/v4/media/MediaMetadataCompat;->B:Landroid/support/v4/n/a;

    const-string v1, "android.media.metadata.COMPILATION"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/n/a;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 249
    sget-object v0, Landroid/support/v4/media/MediaMetadataCompat;->B:Landroid/support/v4/n/a;

    const-string v1, "android.media.metadata.DATE"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/n/a;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 250
    sget-object v0, Landroid/support/v4/media/MediaMetadataCompat;->B:Landroid/support/v4/n/a;

    const-string v1, "android.media.metadata.YEAR"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/n/a;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 251
    sget-object v0, Landroid/support/v4/media/MediaMetadataCompat;->B:Landroid/support/v4/n/a;

    const-string v1, "android.media.metadata.GENRE"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/n/a;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 252
    sget-object v0, Landroid/support/v4/media/MediaMetadataCompat;->B:Landroid/support/v4/n/a;

    const-string v1, "android.media.metadata.TRACK_NUMBER"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/n/a;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 253
    sget-object v0, Landroid/support/v4/media/MediaMetadataCompat;->B:Landroid/support/v4/n/a;

    const-string v1, "android.media.metadata.NUM_TRACKS"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/n/a;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 254
    sget-object v0, Landroid/support/v4/media/MediaMetadataCompat;->B:Landroid/support/v4/n/a;

    const-string v1, "android.media.metadata.DISC_NUMBER"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/n/a;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 255
    sget-object v0, Landroid/support/v4/media/MediaMetadataCompat;->B:Landroid/support/v4/n/a;

    const-string v1, "android.media.metadata.ALBUM_ARTIST"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/n/a;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 256
    sget-object v0, Landroid/support/v4/media/MediaMetadataCompat;->B:Landroid/support/v4/n/a;

    const-string v1, "android.media.metadata.ART"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/n/a;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 257
    sget-object v0, Landroid/support/v4/media/MediaMetadataCompat;->B:Landroid/support/v4/n/a;

    const-string v1, "android.media.metadata.ART_URI"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/n/a;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 258
    sget-object v0, Landroid/support/v4/media/MediaMetadataCompat;->B:Landroid/support/v4/n/a;

    const-string v1, "android.media.metadata.ALBUM_ART"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/n/a;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 259
    sget-object v0, Landroid/support/v4/media/MediaMetadataCompat;->B:Landroid/support/v4/n/a;

    const-string v1, "android.media.metadata.ALBUM_ART_URI"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/n/a;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 260
    sget-object v0, Landroid/support/v4/media/MediaMetadataCompat;->B:Landroid/support/v4/n/a;

    const-string v1, "android.media.metadata.USER_RATING"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/n/a;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 261
    sget-object v0, Landroid/support/v4/media/MediaMetadataCompat;->B:Landroid/support/v4/n/a;

    const-string v1, "android.media.metadata.RATING"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/n/a;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 262
    sget-object v0, Landroid/support/v4/media/MediaMetadataCompat;->B:Landroid/support/v4/n/a;

    const-string v1, "android.media.metadata.DISPLAY_TITLE"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/n/a;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 263
    sget-object v0, Landroid/support/v4/media/MediaMetadataCompat;->B:Landroid/support/v4/n/a;

    const-string v1, "android.media.metadata.DISPLAY_SUBTITLE"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/n/a;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 264
    sget-object v0, Landroid/support/v4/media/MediaMetadataCompat;->B:Landroid/support/v4/n/a;

    const-string v1, "android.media.metadata.DISPLAY_DESCRIPTION"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/n/a;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 265
    sget-object v0, Landroid/support/v4/media/MediaMetadataCompat;->B:Landroid/support/v4/n/a;

    const-string v1, "android.media.metadata.DISPLAY_ICON"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/n/a;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 266
    sget-object v0, Landroid/support/v4/media/MediaMetadataCompat;->B:Landroid/support/v4/n/a;

    const-string v1, "android.media.metadata.DISPLAY_ICON_URI"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/n/a;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 267
    sget-object v0, Landroid/support/v4/media/MediaMetadataCompat;->B:Landroid/support/v4/n/a;

    const-string v1, "android.media.metadata.MEDIA_ID"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/n/a;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 270
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "android.media.metadata.TITLE"

    aput-object v1, v0, v4

    const-string v1, "android.media.metadata.ARTIST"

    aput-object v1, v0, v3

    const-string v1, "android.media.metadata.ALBUM"

    aput-object v1, v0, v5

    const-string v1, "android.media.metadata.ALBUM_ARTIST"

    aput-object v1, v0, v6

    const/4 v1, 0x4

    const-string v2, "android.media.metadata.WRITER"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "android.media.metadata.AUTHOR"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "android.media.metadata.COMPOSER"

    aput-object v2, v0, v1

    sput-object v0, Landroid/support/v4/media/MediaMetadataCompat;->J:[Ljava/lang/String;

    .line 280
    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "android.media.metadata.DISPLAY_ICON"

    aput-object v1, v0, v4

    const-string v1, "android.media.metadata.ART"

    aput-object v1, v0, v3

    const-string v1, "android.media.metadata.ALBUM_ART"

    aput-object v1, v0, v5

    sput-object v0, Landroid/support/v4/media/MediaMetadataCompat;->K:[Ljava/lang/String;

    .line 286
    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "android.media.metadata.DISPLAY_ICON_URI"

    aput-object v1, v0, v4

    const-string v1, "android.media.metadata.ART_URI"

    aput-object v1, v0, v3

    const-string v1, "android.media.metadata.ALBUM_ART_URI"

    aput-object v1, v0, v5

    sput-object v0, Landroid/support/v4/media/MediaMetadataCompat;->L:[Ljava/lang/String;

    .line 584
    new-instance v0, Landroid/support/v4/media/g;

    invoke-direct {v0}, Landroid/support/v4/media/g;-><init>()V

    sput-object v0, Landroid/support/v4/media/MediaMetadataCompat;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Bundle;)V
    .registers 3

    .prologue
    .line 296
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 297
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0, p1}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    iput-object v0, p0, Landroid/support/v4/media/MediaMetadataCompat;->C:Landroid/os/Bundle;

    .line 298
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Bundle;B)V
    .registers 3

    .prologue
    .line 36
    invoke-direct {p0, p1}, Landroid/support/v4/media/MediaMetadataCompat;-><init>(Landroid/os/Bundle;)V

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .registers 3

    .prologue
    .line 300
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 301
    invoke-virtual {p1}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/media/MediaMetadataCompat;->C:Landroid/os/Bundle;

    .line 302
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;B)V
    .registers 3

    .prologue
    .line 36
    invoke-direct {p0, p1}, Landroid/support/v4/media/MediaMetadataCompat;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method static synthetic a(Landroid/support/v4/media/MediaMetadataCompat;)Landroid/os/Bundle;
    .registers 2

    .prologue
    .line 36
    iget-object v0, p0, Landroid/support/v4/media/MediaMetadataCompat;->C:Landroid/os/Bundle;

    return-object v0
.end method

.method public static a(Ljava/lang/Object;)Landroid/support/v4/media/MediaMetadataCompat;
    .registers 7

    .prologue
    .line 508
    if-eqz p0, :cond_8

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-ge v0, v1, :cond_a

    .line 509
    :cond_8
    const/4 v0, 0x0

    .line 538
    :goto_9
    return-object v0

    .line 512
    :cond_a
    new-instance v2, Landroid/support/v4/media/i;

    invoke-direct {v2}, Landroid/support/v4/media/i;-><init>()V

    move-object v0, p0

    .line 2027
    check-cast v0, Landroid/media/MediaMetadata;

    invoke-virtual {v0}, Landroid/media/MediaMetadata;->keySet()Ljava/util/Set;

    move-result-object v0

    .line 513
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1a
    :goto_1a
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_13b

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 514
    sget-object v1, Landroid/support/v4/media/MediaMetadataCompat;->B:Landroid/support/v4/n/a;

    invoke-virtual {v1, v0}, Landroid/support/v4/n/a;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 515
    if-eqz v1, :cond_1a

    .line 516
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    packed-switch v1, :pswitch_data_148

    goto :goto_1a

    :pswitch_38
    move-object v1, p0

    .line 5035
    check-cast v1, Landroid/media/MediaMetadata;

    invoke-virtual {v1, v0}, Landroid/media/MediaMetadata;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 6036
    sget-object v1, Landroid/support/v4/media/MediaMetadataCompat;->B:Landroid/support/v4/n/a;

    .line 5714
    invoke-virtual {v1, v0}, Landroid/support/v4/n/a;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b0

    .line 7036
    sget-object v1, Landroid/support/v4/media/MediaMetadataCompat;->B:Landroid/support/v4/n/a;

    .line 5715
    invoke-virtual {v1, v0}, Landroid/support/v4/n/a;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-eqz v1, :cond_b0

    .line 5716
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " key cannot be used to put a long"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :pswitch_70
    move-object v1, p0

    .line 2031
    check-cast v1, Landroid/media/MediaMetadata;

    invoke-virtual {v1, v0}, Landroid/media/MediaMetadata;->getBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 3036
    sget-object v1, Landroid/support/v4/media/MediaMetadataCompat;->B:Landroid/support/v4/n/a;

    .line 2763
    invoke-virtual {v1, v0}, Landroid/support/v4/n/a;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a9

    .line 4036
    sget-object v1, Landroid/support/v4/media/MediaMetadataCompat;->B:Landroid/support/v4/n/a;

    .line 2764
    invoke-virtual {v1, v0}, Landroid/support/v4/n/a;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/4 v5, 0x2

    if-eq v1, v5, :cond_a9

    .line 2765
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " key cannot be used to put a Bitmap"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2769
    :cond_a9
    iget-object v1, v2, Landroid/support/v4/media/i;->a:Landroid/os/Bundle;

    invoke-virtual {v1, v0, v4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    goto/16 :goto_1a

    .line 5720
    :cond_b0
    iget-object v1, v2, Landroid/support/v4/media/i;->a:Landroid/os/Bundle;

    invoke-virtual {v1, v0, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    goto/16 :goto_1a

    :pswitch_b7
    move-object v1, p0

    .line 7039
    check-cast v1, Landroid/media/MediaMetadata;

    invoke-virtual {v1, v0}, Landroid/media/MediaMetadata;->getRating(Ljava/lang/String;)Landroid/media/Rating;

    move-result-object v1

    .line 526
    invoke-static {v1}, Landroid/support/v4/media/RatingCompat;->a(Ljava/lang/Object;)Landroid/support/v4/media/RatingCompat;

    move-result-object v4

    .line 8036
    sget-object v1, Landroid/support/v4/media/MediaMetadataCompat;->B:Landroid/support/v4/n/a;

    .line 7738
    invoke-virtual {v1, v0}, Landroid/support/v4/n/a;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_f4

    .line 9036
    sget-object v1, Landroid/support/v4/media/MediaMetadataCompat;->B:Landroid/support/v4/n/a;

    .line 7739
    invoke-virtual {v1, v0}, Landroid/support/v4/n/a;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/4 v5, 0x3

    if-eq v1, v5, :cond_f4

    .line 7740
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " key cannot be used to put a Rating"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 7744
    :cond_f4
    iget-object v1, v2, Landroid/support/v4/media/i;->a:Landroid/os/Bundle;

    invoke-virtual {v1, v0, v4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    goto/16 :goto_1a

    :pswitch_fb
    move-object v1, p0

    .line 9043
    check-cast v1, Landroid/media/MediaMetadata;

    invoke-virtual {v1, v0}, Landroid/media/MediaMetadata;->getText(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v4

    .line 10036
    sget-object v1, Landroid/support/v4/media/MediaMetadataCompat;->B:Landroid/support/v4/n/a;

    .line 9650
    invoke-virtual {v1, v0}, Landroid/support/v4/n/a;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_134

    .line 11036
    sget-object v1, Landroid/support/v4/media/MediaMetadataCompat;->B:Landroid/support/v4/n/a;

    .line 9651
    invoke-virtual {v1, v0}, Landroid/support/v4/n/a;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/4 v5, 0x1

    if-eq v1, v5, :cond_134

    .line 9652
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " key cannot be used to put a CharSequence"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 9656
    :cond_134
    iget-object v1, v2, Landroid/support/v4/media/i;->a:Landroid/os/Bundle;

    invoke-virtual {v1, v0, v4}, Landroid/os/Bundle;->putCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)V

    goto/16 :goto_1a

    .line 11779
    :cond_13b
    new-instance v0, Landroid/support/v4/media/MediaMetadataCompat;

    iget-object v1, v2, Landroid/support/v4/media/i;->a:Landroid/os/Bundle;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Landroid/support/v4/media/MediaMetadataCompat;-><init>(Landroid/os/Bundle;B)V

    .line 537
    iput-object p0, v0, Landroid/support/v4/media/MediaMetadataCompat;->D:Ljava/lang/Object;

    goto/16 :goto_9

    .line 516
    nop

    :pswitch_data_148
    .packed-switch 0x0
        :pswitch_38
        :pswitch_fb
        :pswitch_70
        :pswitch_b7
    .end packed-switch
.end method

.method static synthetic a()Landroid/support/v4/n/a;
    .registers 1

    .prologue
    .line 36
    sget-object v0, Landroid/support/v4/media/MediaMetadataCompat;->B:Landroid/support/v4/n/a;

    return-object v0
.end method

.method private b()Landroid/support/v4/media/MediaDescriptionCompat;
    .registers 12

    .prologue
    const/4 v3, 0x0

    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v2, 0x0

    .line 395
    iget-object v0, p0, Landroid/support/v4/media/MediaMetadataCompat;->M:Landroid/support/v4/media/MediaDescriptionCompat;

    if-eqz v0, :cond_c

    .line 396
    iget-object v0, p0, Landroid/support/v4/media/MediaMetadataCompat;->M:Landroid/support/v4/media/MediaDescriptionCompat;

    .line 453
    :goto_b
    return-object v0

    .line 399
    :cond_c
    const-string v0, "android.media.metadata.MEDIA_ID"

    invoke-direct {p0, v0}, Landroid/support/v4/media/MediaMetadataCompat;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 401
    new-array v6, v10, [Ljava/lang/CharSequence;

    .line 406
    const-string v0, "android.media.metadata.DISPLAY_TITLE"

    invoke-virtual {p0, v0}, Landroid/support/v4/media/MediaMetadataCompat;->a(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 407
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_7b

    .line 410
    aput-object v0, v6, v2

    .line 411
    const-string v0, "android.media.metadata.DISPLAY_SUBTITLE"

    invoke-virtual {p0, v0}, Landroid/support/v4/media/MediaMetadataCompat;->a(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    aput-object v0, v6, v8

    .line 412
    const-string v0, "android.media.metadata.DISPLAY_DESCRIPTION"

    invoke-virtual {p0, v0}, Landroid/support/v4/media/MediaMetadataCompat;->a(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    aput-object v0, v6, v9

    :cond_32
    move v0, v2

    .line 427
    :goto_33
    sget-object v1, Landroid/support/v4/media/MediaMetadataCompat;->K:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_a1

    .line 428
    sget-object v1, Landroid/support/v4/media/MediaMetadataCompat;->K:[Ljava/lang/String;

    aget-object v1, v1, v0

    invoke-virtual {p0, v1}, Landroid/support/v4/media/MediaMetadataCompat;->d(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 429
    if-eqz v1, :cond_9b

    move-object v0, v1

    :goto_43
    move v1, v2

    .line 436
    :goto_44
    sget-object v4, Landroid/support/v4/media/MediaMetadataCompat;->L:[Ljava/lang/String;

    array-length v4, v4

    if-ge v1, v4, :cond_5b

    .line 437
    sget-object v4, Landroid/support/v4/media/MediaMetadataCompat;->L:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-direct {p0, v4}, Landroid/support/v4/media/MediaMetadataCompat;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 438
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_9e

    .line 439
    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 444
    :cond_5b
    new-instance v1, Landroid/support/v4/media/b;

    invoke-direct {v1}, Landroid/support/v4/media/b;-><init>()V

    .line 1306
    iput-object v5, v1, Landroid/support/v4/media/b;->a:Ljava/lang/String;

    .line 446
    aget-object v2, v6, v2

    .line 1317
    iput-object v2, v1, Landroid/support/v4/media/b;->b:Ljava/lang/CharSequence;

    .line 447
    aget-object v2, v6, v8

    .line 1328
    iput-object v2, v1, Landroid/support/v4/media/b;->c:Ljava/lang/CharSequence;

    .line 448
    aget-object v2, v6, v9

    .line 1340
    iput-object v2, v1, Landroid/support/v4/media/b;->d:Ljava/lang/CharSequence;

    .line 1352
    iput-object v0, v1, Landroid/support/v4/media/b;->e:Landroid/graphics/Bitmap;

    .line 1364
    iput-object v3, v1, Landroid/support/v4/media/b;->f:Landroid/net/Uri;

    .line 451
    invoke-virtual {v1}, Landroid/support/v4/media/b;->a()Landroid/support/v4/media/MediaDescriptionCompat;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/media/MediaMetadataCompat;->M:Landroid/support/v4/media/MediaDescriptionCompat;

    .line 453
    iget-object v0, p0, Landroid/support/v4/media/MediaMetadataCompat;->M:Landroid/support/v4/media/MediaDescriptionCompat;

    goto :goto_b

    :cond_7b
    move v0, v2

    move v1, v2

    .line 417
    :goto_7d
    if-ge v1, v10, :cond_32

    sget-object v4, Landroid/support/v4/media/MediaMetadataCompat;->J:[Ljava/lang/String;

    array-length v4, v4

    if-ge v0, v4, :cond_32

    .line 418
    sget-object v7, Landroid/support/v4/media/MediaMetadataCompat;->J:[Ljava/lang/String;

    add-int/lit8 v4, v0, 0x1

    aget-object v0, v7, v0

    invoke-virtual {p0, v0}, Landroid/support/v4/media/MediaMetadataCompat;->a(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v7

    .line 419
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_a3

    .line 421
    add-int/lit8 v0, v1, 0x1

    aput-object v7, v6, v1

    :goto_98
    move v1, v0

    move v0, v4

    .line 423
    goto :goto_7d

    .line 427
    :cond_9b
    add-int/lit8 v0, v0, 0x1

    goto :goto_33

    .line 436
    :cond_9e
    add-int/lit8 v1, v1, 0x1

    goto :goto_44

    :cond_a1
    move-object v0, v3

    goto :goto_43

    :cond_a3
    move v0, v1

    goto :goto_98
.end method

.method private c()I
    .registers 2

    .prologue
    .line 472
    iget-object v0, p0, Landroid/support/v4/media/MediaMetadataCompat;->C:Landroid/os/Bundle;

    invoke-virtual {v0}, Landroid/os/Bundle;->size()I

    move-result v0

    return v0
.end method

.method private d()Ljava/util/Set;
    .registers 2

    .prologue
    .line 481
    iget-object v0, p0, Landroid/support/v4/media/MediaMetadataCompat;->C:Landroid/os/Bundle;

    invoke-virtual {v0}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method private e()Landroid/os/Bundle;
    .registers 2

    .prologue
    .line 491
    iget-object v0, p0, Landroid/support/v4/media/MediaMetadataCompat;->C:Landroid/os/Bundle;

    return-object v0
.end method

.method private e(Ljava/lang/String;)Z
    .registers 3

    .prologue
    .line 311
    iget-object v0, p0, Landroid/support/v4/media/MediaMetadataCompat;->C:Landroid/os/Bundle;

    invoke-virtual {v0, p1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private f()Ljava/lang/Object;
    .registers 9

    .prologue
    .line 552
    iget-object v0, p0, Landroid/support/v4/media/MediaMetadataCompat;->D:Ljava/lang/Object;

    if-nez v0, :cond_a

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-ge v0, v1, :cond_d

    .line 553
    :cond_a
    iget-object v0, p0, Landroid/support/v4/media/MediaMetadataCompat;->D:Ljava/lang/Object;

    .line 581
    :goto_c
    return-object v0

    .line 12048
    :cond_d
    new-instance v2, Landroid/media/MediaMetadata$Builder;

    invoke-direct {v2}, Landroid/media/MediaMetadata$Builder;-><init>()V

    .line 12481
    iget-object v0, p0, Landroid/support/v4/media/MediaMetadataCompat;->C:Landroid/os/Bundle;

    invoke-virtual {v0}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v0

    .line 557
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1c
    :goto_1c
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6c

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 558
    sget-object v1, Landroid/support/v4/media/MediaMetadataCompat;->B:Landroid/support/v4/n/a;

    invoke-virtual {v1, v0}, Landroid/support/v4/n/a;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 559
    if-eqz v1, :cond_1c

    .line 560
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    packed-switch v1, :pswitch_data_78

    goto :goto_1c

    .line 566
    :pswitch_3a
    invoke-virtual {p0, v0}, Landroid/support/v4/media/MediaMetadataCompat;->b(Ljava/lang/String;)J

    move-result-wide v6

    move-object v1, v2

    .line 13056
    check-cast v1, Landroid/media/MediaMetadata$Builder;

    invoke-virtual {v1, v0, v6, v7}, Landroid/media/MediaMetadata$Builder;->putLong(Ljava/lang/String;J)Landroid/media/MediaMetadata$Builder;

    goto :goto_1c

    .line 562
    :pswitch_45
    invoke-virtual {p0, v0}, Landroid/support/v4/media/MediaMetadataCompat;->d(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v3

    move-object v1, v2

    .line 13052
    check-cast v1, Landroid/media/MediaMetadata$Builder;

    invoke-virtual {v1, v0, v3}, Landroid/media/MediaMetadata$Builder;->putBitmap(Ljava/lang/String;Landroid/graphics/Bitmap;)Landroid/media/MediaMetadata$Builder;

    goto :goto_1c

    .line 570
    :pswitch_50
    invoke-virtual {p0, v0}, Landroid/support/v4/media/MediaMetadataCompat;->c(Ljava/lang/String;)Landroid/support/v4/media/RatingCompat;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/media/RatingCompat;->a()Ljava/lang/Object;

    move-result-object v3

    move-object v1, v2

    .line 13060
    check-cast v1, Landroid/media/MediaMetadata$Builder;

    check-cast v3, Landroid/media/Rating;

    invoke-virtual {v1, v0, v3}, Landroid/media/MediaMetadata$Builder;->putRating(Ljava/lang/String;Landroid/media/Rating;)Landroid/media/MediaMetadata$Builder;

    goto :goto_1c

    .line 574
    :pswitch_61
    invoke-virtual {p0, v0}, Landroid/support/v4/media/MediaMetadataCompat;->a(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v3

    move-object v1, v2

    .line 13064
    check-cast v1, Landroid/media/MediaMetadata$Builder;

    invoke-virtual {v1, v0, v3}, Landroid/media/MediaMetadata$Builder;->putText(Ljava/lang/String;Ljava/lang/CharSequence;)Landroid/media/MediaMetadata$Builder;

    goto :goto_1c

    .line 13072
    :cond_6c
    check-cast v2, Landroid/media/MediaMetadata$Builder;

    invoke-virtual {v2}, Landroid/media/MediaMetadata$Builder;->build()Landroid/media/MediaMetadata;

    move-result-object v0

    .line 580
    iput-object v0, p0, Landroid/support/v4/media/MediaMetadataCompat;->D:Ljava/lang/Object;

    .line 581
    iget-object v0, p0, Landroid/support/v4/media/MediaMetadataCompat;->D:Ljava/lang/Object;

    goto :goto_c

    .line 560
    nop

    :pswitch_data_78
    .packed-switch 0x0
        :pswitch_3a
        :pswitch_61
        :pswitch_45
        :pswitch_50
    .end packed-switch
.end method

.method private f(Ljava/lang/String;)Ljava/lang/String;
    .registers 3

    .prologue
    .line 335
    iget-object v0, p0, Landroid/support/v4/media/MediaMetadataCompat;->C:Landroid/os/Bundle;

    invoke-virtual {v0, p1}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 336
    if-eqz v0, :cond_d

    .line 337
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 339
    :goto_c
    return-object v0

    :cond_d
    const/4 v0, 0x0

    goto :goto_c
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/CharSequence;
    .registers 3

    .prologue
    .line 323
    iget-object v0, p0, Landroid/support/v4/media/MediaMetadataCompat;->C:Landroid/os/Bundle;

    invoke-virtual {v0, p1}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/String;)J
    .registers 6

    .prologue
    .line 350
    iget-object v0, p0, Landroid/support/v4/media/MediaMetadataCompat;->C:Landroid/os/Bundle;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, p1, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public final c(Ljava/lang/String;)Landroid/support/v4/media/RatingCompat;
    .registers 6

    .prologue
    .line 361
    const/4 v1, 0x0

    .line 363
    :try_start_1
    iget-object v0, p0, Landroid/support/v4/media/MediaMetadataCompat;->C:Landroid/os/Bundle;

    invoke-virtual {v0, p1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/support/v4/media/RatingCompat;
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_9} :catch_a

    .line 368
    :goto_9
    return-object v0

    .line 364
    :catch_a
    move-exception v0

    .line 366
    const-string v2, "MediaMetadata"

    const-string v3, "Failed to retrieve a key as Rating."

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v0, v1

    goto :goto_9
.end method

.method public final d(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .registers 6

    .prologue
    .line 379
    const/4 v1, 0x0

    .line 381
    :try_start_1
    iget-object v0, p0, Landroid/support/v4/media/MediaMetadataCompat;->C:Landroid/os/Bundle;

    invoke-virtual {v0, p1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_9} :catch_a

    .line 386
    :goto_9
    return-object v0

    .line 382
    :catch_a
    move-exception v0

    .line 384
    const-string v2, "MediaMetadata"

    const-string v3, "Failed to retrieve a key as Bitmap."

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v0, v1

    goto :goto_9
.end method

.method public final describeContents()I
    .registers 2

    .prologue
    .line 458
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .registers 4

    .prologue
    .line 463
    iget-object v0, p0, Landroid/support/v4/media/MediaMetadataCompat;->C:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    .line 464
    return-void
.end method
