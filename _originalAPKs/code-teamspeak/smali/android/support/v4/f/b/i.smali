.class public final Landroid/support/v4/f/b/i;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 146
    return-void
.end method

.method private static a(Landroid/support/v4/f/b/k;)Landroid/hardware/fingerprint/FingerprintManager$AuthenticationCallback;
    .registers 2

    .prologue
    .line 83
    new-instance v0, Landroid/support/v4/f/b/j;

    invoke-direct {v0, p0}, Landroid/support/v4/f/b/j;-><init>(Landroid/support/v4/f/b/k;)V

    return-object v0
.end method

.method private static a(Landroid/support/v4/f/b/m;)Landroid/hardware/fingerprint/FingerprintManager$CryptoObject;
    .registers 3

    .prologue
    const/4 v0, 0x0

    .line 54
    if-nez p0, :cond_4

    .line 63
    :cond_3
    :goto_3
    return-object v0

    .line 6132
    :cond_4
    iget-object v1, p0, Landroid/support/v4/f/b/m;->b:Ljavax/crypto/Cipher;

    .line 56
    if-eqz v1, :cond_10

    .line 57
    new-instance v0, Landroid/hardware/fingerprint/FingerprintManager$CryptoObject;

    .line 7132
    iget-object v1, p0, Landroid/support/v4/f/b/m;->b:Ljavax/crypto/Cipher;

    .line 57
    invoke-direct {v0, v1}, Landroid/hardware/fingerprint/FingerprintManager$CryptoObject;-><init>(Ljavax/crypto/Cipher;)V

    goto :goto_3

    .line 8131
    :cond_10
    iget-object v1, p0, Landroid/support/v4/f/b/m;->a:Ljava/security/Signature;

    .line 58
    if-eqz v1, :cond_1c

    .line 59
    new-instance v0, Landroid/hardware/fingerprint/FingerprintManager$CryptoObject;

    .line 9131
    iget-object v1, p0, Landroid/support/v4/f/b/m;->a:Ljava/security/Signature;

    .line 59
    invoke-direct {v0, v1}, Landroid/hardware/fingerprint/FingerprintManager$CryptoObject;-><init>(Ljava/security/Signature;)V

    goto :goto_3

    .line 9133
    :cond_1c
    iget-object v1, p0, Landroid/support/v4/f/b/m;->c:Ljavax/crypto/Mac;

    .line 60
    if-eqz v1, :cond_3

    .line 61
    new-instance v0, Landroid/hardware/fingerprint/FingerprintManager$CryptoObject;

    .line 10133
    iget-object v1, p0, Landroid/support/v4/f/b/m;->c:Ljavax/crypto/Mac;

    .line 61
    invoke-direct {v0, v1}, Landroid/hardware/fingerprint/FingerprintManager$CryptoObject;-><init>(Ljavax/crypto/Mac;)V

    goto :goto_3
.end method

.method static a(Landroid/content/Context;)Landroid/hardware/fingerprint/FingerprintManager;
    .registers 2

    .prologue
    .line 35
    const-class v0, Landroid/hardware/fingerprint/FingerprintManager;

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/fingerprint/FingerprintManager;

    return-object v0
.end method

.method private static a(Landroid/hardware/fingerprint/FingerprintManager$CryptoObject;)Landroid/support/v4/f/b/m;
    .registers 3

    .prologue
    const/4 v0, 0x0

    .line 68
    if-nez p0, :cond_4

    .line 77
    :cond_3
    :goto_3
    return-object v0

    .line 70
    :cond_4
    invoke-virtual {p0}, Landroid/hardware/fingerprint/FingerprintManager$CryptoObject;->getCipher()Ljavax/crypto/Cipher;

    move-result-object v1

    if-eqz v1, :cond_14

    .line 71
    new-instance v0, Landroid/support/v4/f/b/m;

    invoke-virtual {p0}, Landroid/hardware/fingerprint/FingerprintManager$CryptoObject;->getCipher()Ljavax/crypto/Cipher;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v4/f/b/m;-><init>(Ljavax/crypto/Cipher;)V

    goto :goto_3

    .line 72
    :cond_14
    invoke-virtual {p0}, Landroid/hardware/fingerprint/FingerprintManager$CryptoObject;->getSignature()Ljava/security/Signature;

    move-result-object v1

    if-eqz v1, :cond_24

    .line 73
    new-instance v0, Landroid/support/v4/f/b/m;

    invoke-virtual {p0}, Landroid/hardware/fingerprint/FingerprintManager$CryptoObject;->getSignature()Ljava/security/Signature;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v4/f/b/m;-><init>(Ljava/security/Signature;)V

    goto :goto_3

    .line 74
    :cond_24
    invoke-virtual {p0}, Landroid/hardware/fingerprint/FingerprintManager$CryptoObject;->getMac()Ljavax/crypto/Mac;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 75
    new-instance v0, Landroid/support/v4/f/b/m;

    invoke-virtual {p0}, Landroid/hardware/fingerprint/FingerprintManager$CryptoObject;->getMac()Ljavax/crypto/Mac;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v4/f/b/m;-><init>(Ljavax/crypto/Mac;)V

    goto :goto_3
.end method

.method private static a(Landroid/content/Context;Landroid/support/v4/f/b/m;ILjava/lang/Object;Landroid/support/v4/f/b/k;Landroid/os/Handler;)V
    .registers 12

    .prologue
    .line 48
    invoke-static {p0}, Landroid/support/v4/f/b/i;->a(Landroid/content/Context;)Landroid/hardware/fingerprint/FingerprintManager;

    move-result-object v0

    .line 1054
    if-eqz p1, :cond_37

    .line 1132
    iget-object v1, p1, Landroid/support/v4/f/b/m;->b:Ljavax/crypto/Cipher;

    .line 1056
    if-eqz v1, :cond_1f

    .line 1057
    new-instance v1, Landroid/hardware/fingerprint/FingerprintManager$CryptoObject;

    .line 2132
    iget-object v2, p1, Landroid/support/v4/f/b/m;->b:Ljavax/crypto/Cipher;

    .line 1057
    invoke-direct {v1, v2}, Landroid/hardware/fingerprint/FingerprintManager$CryptoObject;-><init>(Ljavax/crypto/Cipher;)V

    :goto_11
    move-object v2, p3

    .line 48
    check-cast v2, Landroid/os/CancellationSignal;

    .line 6083
    new-instance v4, Landroid/support/v4/f/b/j;

    invoke-direct {v4, p4}, Landroid/support/v4/f/b/j;-><init>(Landroid/support/v4/f/b/k;)V

    move v3, p2

    move-object v5, p5

    .line 48
    invoke-virtual/range {v0 .. v5}, Landroid/hardware/fingerprint/FingerprintManager;->authenticate(Landroid/hardware/fingerprint/FingerprintManager$CryptoObject;Landroid/os/CancellationSignal;ILandroid/hardware/fingerprint/FingerprintManager$AuthenticationCallback;Landroid/os/Handler;)V

    .line 51
    return-void

    .line 3131
    :cond_1f
    iget-object v1, p1, Landroid/support/v4/f/b/m;->a:Ljava/security/Signature;

    .line 1058
    if-eqz v1, :cond_2b

    .line 1059
    new-instance v1, Landroid/hardware/fingerprint/FingerprintManager$CryptoObject;

    .line 4131
    iget-object v2, p1, Landroid/support/v4/f/b/m;->a:Ljava/security/Signature;

    .line 1059
    invoke-direct {v1, v2}, Landroid/hardware/fingerprint/FingerprintManager$CryptoObject;-><init>(Ljava/security/Signature;)V

    goto :goto_11

    .line 4133
    :cond_2b
    iget-object v1, p1, Landroid/support/v4/f/b/m;->c:Ljavax/crypto/Mac;

    .line 1060
    if-eqz v1, :cond_37

    .line 1061
    new-instance v1, Landroid/hardware/fingerprint/FingerprintManager$CryptoObject;

    .line 5133
    iget-object v2, p1, Landroid/support/v4/f/b/m;->c:Ljavax/crypto/Mac;

    .line 1061
    invoke-direct {v1, v2}, Landroid/hardware/fingerprint/FingerprintManager$CryptoObject;-><init>(Ljavax/crypto/Mac;)V

    goto :goto_11

    .line 1063
    :cond_37
    const/4 v1, 0x0

    goto :goto_11
.end method

.method private static synthetic b(Landroid/hardware/fingerprint/FingerprintManager$CryptoObject;)Landroid/support/v4/f/b/m;
    .registers 3

    .prologue
    .line 32
    .line 11068
    if-eqz p0, :cond_32

    .line 11070
    invoke-virtual {p0}, Landroid/hardware/fingerprint/FingerprintManager$CryptoObject;->getCipher()Ljavax/crypto/Cipher;

    move-result-object v0

    if-eqz v0, :cond_12

    .line 11071
    new-instance v0, Landroid/support/v4/f/b/m;

    invoke-virtual {p0}, Landroid/hardware/fingerprint/FingerprintManager$CryptoObject;->getCipher()Ljavax/crypto/Cipher;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v4/f/b/m;-><init>(Ljavax/crypto/Cipher;)V

    .line 11075
    :goto_11
    return-object v0

    .line 11072
    :cond_12
    invoke-virtual {p0}, Landroid/hardware/fingerprint/FingerprintManager$CryptoObject;->getSignature()Ljava/security/Signature;

    move-result-object v0

    if-eqz v0, :cond_22

    .line 11073
    new-instance v0, Landroid/support/v4/f/b/m;

    invoke-virtual {p0}, Landroid/hardware/fingerprint/FingerprintManager$CryptoObject;->getSignature()Ljava/security/Signature;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v4/f/b/m;-><init>(Ljava/security/Signature;)V

    goto :goto_11

    .line 11074
    :cond_22
    invoke-virtual {p0}, Landroid/hardware/fingerprint/FingerprintManager$CryptoObject;->getMac()Ljavax/crypto/Mac;

    move-result-object v0

    if-eqz v0, :cond_32

    .line 11075
    new-instance v0, Landroid/support/v4/f/b/m;

    invoke-virtual {p0}, Landroid/hardware/fingerprint/FingerprintManager$CryptoObject;->getMac()Ljavax/crypto/Mac;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v4/f/b/m;-><init>(Ljavax/crypto/Mac;)V

    goto :goto_11

    .line 11077
    :cond_32
    const/4 v0, 0x0

    .line 32
    goto :goto_11
.end method

.method private static b(Landroid/content/Context;)Z
    .registers 2

    .prologue
    .line 39
    invoke-static {p0}, Landroid/support/v4/f/b/i;->a(Landroid/content/Context;)Landroid/hardware/fingerprint/FingerprintManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/hardware/fingerprint/FingerprintManager;->hasEnrolledFingerprints()Z

    move-result v0

    return v0
.end method

.method private static c(Landroid/content/Context;)Z
    .registers 2

    .prologue
    .line 43
    invoke-static {p0}, Landroid/support/v4/f/b/i;->a(Landroid/content/Context;)Landroid/hardware/fingerprint/FingerprintManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/hardware/fingerprint/FingerprintManager;->isHardwareDetected()Z

    move-result v0

    return v0
.end method
