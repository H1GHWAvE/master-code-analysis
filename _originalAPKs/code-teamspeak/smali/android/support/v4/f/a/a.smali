.class public abstract Landroid/support/v4/f/a/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Ljava/lang/String; = "android.hardware.display.category.PRESENTATION"

.field private static final b:Ljava/util/WeakHashMap;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 30
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    sput-object v0, Landroid/support/v4/f/a/a;->b:Ljava/util/WeakHashMap;

    return-void
.end method

.method constructor <init>()V
    .registers 1

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    return-void
.end method

.method private static a(Landroid/content/Context;)Landroid/support/v4/f/a/a;
    .registers 4

    .prologue
    .line 54
    sget-object v1, Landroid/support/v4/f/a/a;->b:Ljava/util/WeakHashMap;

    monitor-enter v1

    .line 55
    :try_start_3
    sget-object v0, Landroid/support/v4/f/a/a;->b:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p0}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/f/a/a;

    .line 56
    if-nez v0, :cond_1d

    .line 57
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 58
    const/16 v2, 0x11

    if-lt v0, v2, :cond_1f

    .line 59
    new-instance v0, Landroid/support/v4/f/a/b;

    invoke-direct {v0, p0}, Landroid/support/v4/f/a/b;-><init>(Landroid/content/Context;)V

    .line 63
    :goto_18
    sget-object v2, Landroid/support/v4/f/a/a;->b:Ljava/util/WeakHashMap;

    invoke-virtual {v2, p0, v0}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 65
    :cond_1d
    monitor-exit v1

    return-object v0

    .line 61
    :cond_1f
    new-instance v0, Landroid/support/v4/f/a/c;

    invoke-direct {v0, p0}, Landroid/support/v4/f/a/c;-><init>(Landroid/content/Context;)V

    goto :goto_18

    .line 66
    :catchall_25
    move-exception v0

    monitor-exit v1
    :try_end_27
    .catchall {:try_start_3 .. :try_end_27} :catchall_25

    throw v0
.end method


# virtual methods
.method public abstract a(I)Landroid/view/Display;
.end method

.method public abstract a()[Landroid/view/Display;
.end method

.method public abstract a(Ljava/lang/String;)[Landroid/view/Display;
.end method
