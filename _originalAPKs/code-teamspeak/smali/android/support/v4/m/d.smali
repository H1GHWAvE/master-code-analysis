.class final Landroid/support/v4/m/d;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final f:I = 0x700

.field private static final g:[B


# instance fields
.field final a:Ljava/lang/String;

.field final b:Z

.field final c:I

.field d:I

.field e:C


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .prologue
    const/16 v3, 0x700

    .line 508
    new-array v0, v3, [B

    sput-object v0, Landroid/support/v4/m/d;->g:[B

    .line 509
    const/4 v0, 0x0

    :goto_7
    if-ge v0, v3, :cond_14

    .line 510
    sget-object v1, Landroid/support/v4/m/d;->g:[B

    invoke-static {v0}, Ljava/lang/Character;->getDirectionality(I)B

    move-result v2

    aput-byte v2, v1, v0

    .line 509
    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    .line 512
    :cond_14
    return-void
.end method

.method constructor <init>(Ljava/lang/String;)V
    .registers 3

    .prologue
    .line 552
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 553
    iput-object p1, p0, Landroid/support/v4/m/d;->a:Ljava/lang/String;

    .line 554
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v4/m/d;->b:Z

    .line 555
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    iput v0, p0, Landroid/support/v4/m/d;->c:I

    .line 556
    return-void
.end method

.method static a(C)B
    .registers 2

    .prologue
    .line 727
    const/16 v0, 0x700

    if-ge p0, v0, :cond_9

    sget-object v0, Landroid/support/v4/m/d;->g:[B

    aget-byte v0, v0, p0

    :goto_8
    return v0

    :cond_9
    invoke-static {p0}, Ljava/lang/Character;->getDirectionality(C)B

    move-result v0

    goto :goto_8
.end method

.method private b()I
    .registers 14

    .prologue
    const/16 v12, 0x3c

    const/16 v7, 0xc

    const/4 v5, 0x1

    const/4 v4, -0x1

    const/4 v1, 0x0

    .line 570
    iput v1, p0, Landroid/support/v4/m/d;->d:I

    move v0, v1

    move v3, v1

    move v2, v1

    .line 574
    :goto_c
    :pswitch_c
    iget v6, p0, Landroid/support/v4/m/d;->d:I

    iget v8, p0, Landroid/support/v4/m/d;->c:I

    if-ge v6, v8, :cond_db

    if-nez v0, :cond_db

    .line 1740
    iget-object v6, p0, Landroid/support/v4/m/d;->a:Ljava/lang/String;

    iget v8, p0, Landroid/support/v4/m/d;->d:I

    invoke-virtual {v6, v8}, Ljava/lang/String;->charAt(I)C

    move-result v6

    iput-char v6, p0, Landroid/support/v4/m/d;->e:C

    .line 1741
    iget-char v6, p0, Landroid/support/v4/m/d;->e:C

    invoke-static {v6}, Ljava/lang/Character;->isHighSurrogate(C)Z

    move-result v6

    if-eqz v6, :cond_40

    .line 1742
    iget-object v6, p0, Landroid/support/v4/m/d;->a:Ljava/lang/String;

    iget v8, p0, Landroid/support/v4/m/d;->d:I

    invoke-static {v6, v8}, Ljava/lang/Character;->codePointAt(Ljava/lang/CharSequence;I)I

    move-result v6

    .line 1743
    iget v8, p0, Landroid/support/v4/m/d;->d:I

    invoke-static {v6}, Ljava/lang/Character;->charCount(I)I

    move-result v9

    add-int/2addr v8, v9

    iput v8, p0, Landroid/support/v4/m/d;->d:I

    .line 1744
    invoke-static {v6}, Ljava/lang/Character;->getDirectionality(I)B

    move-result v6

    .line 575
    :cond_3b
    :goto_3b
    packed-switch v6, :pswitch_data_100

    :pswitch_3e
    move v0, v2

    .line 610
    goto :goto_c

    .line 1746
    :cond_40
    iget v6, p0, Landroid/support/v4/m/d;->d:I

    add-int/lit8 v6, v6, 0x1

    iput v6, p0, Landroid/support/v4/m/d;->d:I

    .line 1747
    iget-char v6, p0, Landroid/support/v4/m/d;->e:C

    invoke-static {v6}, Landroid/support/v4/m/d;->a(C)B

    move-result v6

    .line 1748
    iget-boolean v8, p0, Landroid/support/v4/m/d;->b:Z

    if-eqz v8, :cond_3b

    .line 1750
    iget-char v8, p0, Landroid/support/v4/m/d;->e:C

    if-ne v8, v12, :cond_9e

    .line 1796
    iget v6, p0, Landroid/support/v4/m/d;->d:I

    .line 1797
    :cond_56
    :goto_56
    iget v8, p0, Landroid/support/v4/m/d;->d:I

    iget v9, p0, Landroid/support/v4/m/d;->c:I

    if-ge v8, v9, :cond_97

    .line 1798
    iget-object v8, p0, Landroid/support/v4/m/d;->a:Ljava/lang/String;

    iget v9, p0, Landroid/support/v4/m/d;->d:I

    add-int/lit8 v10, v9, 0x1

    iput v10, p0, Landroid/support/v4/m/d;->d:I

    invoke-virtual {v8, v9}, Ljava/lang/String;->charAt(I)C

    move-result v8

    iput-char v8, p0, Landroid/support/v4/m/d;->e:C

    .line 1799
    iget-char v8, p0, Landroid/support/v4/m/d;->e:C

    const/16 v9, 0x3e

    if-ne v8, v9, :cond_72

    move v6, v7

    .line 1801
    goto :goto_3b

    .line 1803
    :cond_72
    iget-char v8, p0, Landroid/support/v4/m/d;->e:C

    const/16 v9, 0x22

    if-eq v8, v9, :cond_7e

    iget-char v8, p0, Landroid/support/v4/m/d;->e:C

    const/16 v9, 0x27

    if-ne v8, v9, :cond_56

    .line 1805
    :cond_7e
    iget-char v8, p0, Landroid/support/v4/m/d;->e:C

    .line 1806
    :cond_80
    iget v9, p0, Landroid/support/v4/m/d;->d:I

    iget v10, p0, Landroid/support/v4/m/d;->c:I

    if-ge v9, v10, :cond_56

    iget-object v9, p0, Landroid/support/v4/m/d;->a:Ljava/lang/String;

    iget v10, p0, Landroid/support/v4/m/d;->d:I

    add-int/lit8 v11, v10, 0x1

    iput v11, p0, Landroid/support/v4/m/d;->d:I

    invoke-virtual {v9, v10}, Ljava/lang/String;->charAt(I)C

    move-result v9

    iput-char v9, p0, Landroid/support/v4/m/d;->e:C

    if-ne v9, v8, :cond_80

    goto :goto_56

    .line 1810
    :cond_97
    iput v6, p0, Landroid/support/v4/m/d;->d:I

    .line 1811
    iput-char v12, p0, Landroid/support/v4/m/d;->e:C

    .line 1812
    const/16 v6, 0xd

    goto :goto_3b

    .line 1752
    :cond_9e
    iget-char v8, p0, Landroid/support/v4/m/d;->e:C

    const/16 v9, 0x26

    if-ne v8, v9, :cond_3b

    .line 1853
    :cond_a4
    iget v6, p0, Landroid/support/v4/m/d;->d:I

    iget v8, p0, Landroid/support/v4/m/d;->c:I

    if-ge v6, v8, :cond_bc

    iget-object v6, p0, Landroid/support/v4/m/d;->a:Ljava/lang/String;

    iget v8, p0, Landroid/support/v4/m/d;->d:I

    add-int/lit8 v9, v8, 0x1

    iput v9, p0, Landroid/support/v4/m/d;->d:I

    invoke-virtual {v6, v8}, Ljava/lang/String;->charAt(I)C

    move-result v6

    iput-char v6, p0, Landroid/support/v4/m/d;->e:C

    const/16 v8, 0x3b

    if-ne v6, v8, :cond_a4

    :cond_bc
    move v6, v7

    .line 1753
    goto/16 :goto_3b

    .line 578
    :pswitch_bf
    add-int/lit8 v2, v2, 0x1

    move v3, v4

    .line 580
    goto/16 :goto_c

    .line 583
    :pswitch_c4
    add-int/lit8 v2, v2, 0x1

    move v3, v5

    .line 585
    goto/16 :goto_c

    .line 587
    :pswitch_c9
    add-int/lit8 v2, v2, -0x1

    move v3, v1

    .line 592
    goto/16 :goto_c

    .line 596
    :pswitch_ce
    if-nez v2, :cond_d1

    .line 652
    :cond_d0
    :goto_d0
    return v4

    :cond_d1
    move v0, v2

    .line 600
    goto/16 :goto_c

    .line 603
    :pswitch_d4
    if-nez v2, :cond_d8

    move v4, v5

    .line 604
    goto :goto_d0

    :cond_d8
    move v0, v2

    .line 607
    goto/16 :goto_c

    .line 616
    :cond_db
    if-nez v0, :cond_df

    move v4, v1

    .line 619
    goto :goto_d0

    .line 623
    :cond_df
    if-eqz v3, :cond_e3

    move v4, v3

    .line 625
    goto :goto_d0

    .line 630
    :cond_e3
    :goto_e3
    iget v3, p0, Landroid/support/v4/m/d;->d:I

    if-lez v3, :cond_fe

    .line 631
    invoke-virtual {p0}, Landroid/support/v4/m/d;->a()B

    move-result v3

    packed-switch v3, :pswitch_data_12a

    goto :goto_e3

    .line 634
    :pswitch_ef
    if-eq v0, v2, :cond_d0

    .line 637
    add-int/lit8 v2, v2, -0x1

    .line 638
    goto :goto_e3

    .line 641
    :pswitch_f4
    if-ne v0, v2, :cond_f8

    move v4, v5

    .line 642
    goto :goto_d0

    .line 644
    :cond_f8
    add-int/lit8 v2, v2, -0x1

    .line 645
    goto :goto_e3

    .line 647
    :pswitch_fb
    add-int/lit8 v2, v2, 0x1

    goto :goto_e3

    :cond_fe
    move v4, v1

    .line 652
    goto :goto_d0

    .line 575
    :pswitch_data_100
    .packed-switch 0x0
        :pswitch_ce
        :pswitch_d4
        :pswitch_d4
        :pswitch_3e
        :pswitch_3e
        :pswitch_3e
        :pswitch_3e
        :pswitch_3e
        :pswitch_3e
        :pswitch_c
        :pswitch_3e
        :pswitch_3e
        :pswitch_3e
        :pswitch_3e
        :pswitch_bf
        :pswitch_bf
        :pswitch_c4
        :pswitch_c4
        :pswitch_c9
    .end packed-switch

    .line 631
    :pswitch_data_12a
    .packed-switch 0xe
        :pswitch_ef
        :pswitch_ef
        :pswitch_f4
        :pswitch_f4
        :pswitch_fb
    .end packed-switch
.end method

.method private c()I
    .registers 7

    .prologue
    const/4 v4, 0x1

    const/4 v3, -0x1

    const/4 v1, 0x0

    .line 668
    iget v0, p0, Landroid/support/v4/m/d;->c:I

    iput v0, p0, Landroid/support/v4/m/d;->d:I

    move v0, v1

    move v2, v1

    .line 671
    :cond_9
    :goto_9
    :pswitch_9
    iget v5, p0, Landroid/support/v4/m/d;->d:I

    if-lez v5, :cond_1b

    .line 672
    invoke-virtual {p0}, Landroid/support/v4/m/d;->a()B

    move-result v5

    packed-switch v5, :pswitch_data_3a

    .line 710
    :pswitch_14
    if-nez v0, :cond_9

    move v0, v2

    .line 711
    goto :goto_9

    .line 674
    :pswitch_18
    if-nez v2, :cond_1c

    move v1, v3

    .line 716
    :cond_1b
    :goto_1b
    return v1

    .line 677
    :cond_1c
    if-nez v0, :cond_9

    move v0, v2

    .line 678
    goto :goto_9

    .line 683
    :pswitch_20
    if-ne v0, v2, :cond_24

    move v1, v3

    .line 684
    goto :goto_1b

    .line 686
    :cond_24
    add-int/lit8 v2, v2, -0x1

    .line 687
    goto :goto_9

    .line 690
    :pswitch_27
    if-nez v2, :cond_2b

    move v1, v4

    .line 691
    goto :goto_1b

    .line 693
    :cond_2b
    if-nez v0, :cond_9

    move v0, v2

    .line 694
    goto :goto_9

    .line 699
    :pswitch_2f
    if-ne v0, v2, :cond_33

    move v1, v4

    .line 700
    goto :goto_1b

    .line 702
    :cond_33
    add-int/lit8 v2, v2, -0x1

    .line 703
    goto :goto_9

    .line 705
    :pswitch_36
    add-int/lit8 v2, v2, 0x1

    .line 706
    goto :goto_9

    .line 672
    nop

    :pswitch_data_3a
    .packed-switch 0x0
        :pswitch_18
        :pswitch_27
        :pswitch_27
        :pswitch_14
        :pswitch_14
        :pswitch_14
        :pswitch_14
        :pswitch_14
        :pswitch_14
        :pswitch_9
        :pswitch_14
        :pswitch_14
        :pswitch_14
        :pswitch_14
        :pswitch_20
        :pswitch_20
        :pswitch_2f
        :pswitch_2f
        :pswitch_36
    .end packed-switch
.end method

.method private d()B
    .registers 8

    .prologue
    const/16 v6, 0x3c

    const/16 v0, 0xc

    .line 740
    iget-object v1, p0, Landroid/support/v4/m/d;->a:Ljava/lang/String;

    iget v2, p0, Landroid/support/v4/m/d;->d:I

    invoke-virtual {v1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v1

    iput-char v1, p0, Landroid/support/v4/m/d;->e:C

    .line 741
    iget-char v1, p0, Landroid/support/v4/m/d;->e:C

    invoke-static {v1}, Ljava/lang/Character;->isHighSurrogate(C)Z

    move-result v1

    if-eqz v1, :cond_2c

    .line 742
    iget-object v0, p0, Landroid/support/v4/m/d;->a:Ljava/lang/String;

    iget v1, p0, Landroid/support/v4/m/d;->d:I

    invoke-static {v0, v1}, Ljava/lang/Character;->codePointAt(Ljava/lang/CharSequence;I)I

    move-result v0

    .line 743
    iget v1, p0, Landroid/support/v4/m/d;->d:I

    invoke-static {v0}, Ljava/lang/Character;->charCount(I)I

    move-result v2

    add-int/2addr v1, v2

    iput v1, p0, Landroid/support/v4/m/d;->d:I

    .line 744
    invoke-static {v0}, Ljava/lang/Character;->getDirectionality(I)B

    move-result v0

    .line 756
    :cond_2b
    :goto_2b
    return v0

    .line 746
    :cond_2c
    iget v1, p0, Landroid/support/v4/m/d;->d:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Landroid/support/v4/m/d;->d:I

    .line 747
    iget-char v1, p0, Landroid/support/v4/m/d;->e:C

    invoke-static {v1}, Landroid/support/v4/m/d;->a(C)B

    move-result v1

    .line 748
    iget-boolean v2, p0, Landroid/support/v4/m/d;->b:Z

    if-eqz v2, :cond_a7

    .line 750
    iget-char v2, p0, Landroid/support/v4/m/d;->e:C

    if-ne v2, v6, :cond_88

    .line 2796
    iget v1, p0, Landroid/support/v4/m/d;->d:I

    .line 2797
    :cond_42
    :goto_42
    iget v2, p0, Landroid/support/v4/m/d;->d:I

    iget v3, p0, Landroid/support/v4/m/d;->c:I

    if-ge v2, v3, :cond_81

    .line 2798
    iget-object v2, p0, Landroid/support/v4/m/d;->a:Ljava/lang/String;

    iget v3, p0, Landroid/support/v4/m/d;->d:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Landroid/support/v4/m/d;->d:I

    invoke-virtual {v2, v3}, Ljava/lang/String;->charAt(I)C

    move-result v2

    iput-char v2, p0, Landroid/support/v4/m/d;->e:C

    .line 2799
    iget-char v2, p0, Landroid/support/v4/m/d;->e:C

    const/16 v3, 0x3e

    if-eq v2, v3, :cond_2b

    .line 2803
    iget-char v2, p0, Landroid/support/v4/m/d;->e:C

    const/16 v3, 0x22

    if-eq v2, v3, :cond_68

    iget-char v2, p0, Landroid/support/v4/m/d;->e:C

    const/16 v3, 0x27

    if-ne v2, v3, :cond_42

    .line 2805
    :cond_68
    iget-char v2, p0, Landroid/support/v4/m/d;->e:C

    .line 2806
    :cond_6a
    iget v3, p0, Landroid/support/v4/m/d;->d:I

    iget v4, p0, Landroid/support/v4/m/d;->c:I

    if-ge v3, v4, :cond_42

    iget-object v3, p0, Landroid/support/v4/m/d;->a:Ljava/lang/String;

    iget v4, p0, Landroid/support/v4/m/d;->d:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Landroid/support/v4/m/d;->d:I

    invoke-virtual {v3, v4}, Ljava/lang/String;->charAt(I)C

    move-result v3

    iput-char v3, p0, Landroid/support/v4/m/d;->e:C

    if-ne v3, v2, :cond_6a

    goto :goto_42

    .line 2810
    :cond_81
    iput v1, p0, Landroid/support/v4/m/d;->d:I

    .line 2811
    iput-char v6, p0, Landroid/support/v4/m/d;->e:C

    .line 2812
    const/16 v0, 0xd

    goto :goto_2b

    .line 752
    :cond_88
    iget-char v2, p0, Landroid/support/v4/m/d;->e:C

    const/16 v3, 0x26

    if-ne v2, v3, :cond_a7

    .line 2853
    :cond_8e
    iget v1, p0, Landroid/support/v4/m/d;->d:I

    iget v2, p0, Landroid/support/v4/m/d;->c:I

    if-ge v1, v2, :cond_2b

    iget-object v1, p0, Landroid/support/v4/m/d;->a:Ljava/lang/String;

    iget v2, p0, Landroid/support/v4/m/d;->d:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Landroid/support/v4/m/d;->d:I

    invoke-virtual {v1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v1

    iput-char v1, p0, Landroid/support/v4/m/d;->e:C

    const/16 v2, 0x3b

    if-ne v1, v2, :cond_8e

    goto :goto_2b

    :cond_a7
    move v0, v1

    goto :goto_2b
.end method

.method private e()B
    .registers 6

    .prologue
    .line 796
    iget v0, p0, Landroid/support/v4/m/d;->d:I

    .line 797
    :cond_2
    :goto_2
    iget v1, p0, Landroid/support/v4/m/d;->d:I

    iget v2, p0, Landroid/support/v4/m/d;->c:I

    if-ge v1, v2, :cond_44

    .line 798
    iget-object v1, p0, Landroid/support/v4/m/d;->a:Ljava/lang/String;

    iget v2, p0, Landroid/support/v4/m/d;->d:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Landroid/support/v4/m/d;->d:I

    invoke-virtual {v1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v1

    iput-char v1, p0, Landroid/support/v4/m/d;->e:C

    .line 799
    iget-char v1, p0, Landroid/support/v4/m/d;->e:C

    const/16 v2, 0x3e

    if-ne v1, v2, :cond_1f

    .line 801
    const/16 v0, 0xc

    .line 812
    :goto_1e
    return v0

    .line 803
    :cond_1f
    iget-char v1, p0, Landroid/support/v4/m/d;->e:C

    const/16 v2, 0x22

    if-eq v1, v2, :cond_2b

    iget-char v1, p0, Landroid/support/v4/m/d;->e:C

    const/16 v2, 0x27

    if-ne v1, v2, :cond_2

    .line 805
    :cond_2b
    iget-char v1, p0, Landroid/support/v4/m/d;->e:C

    .line 806
    :cond_2d
    iget v2, p0, Landroid/support/v4/m/d;->d:I

    iget v3, p0, Landroid/support/v4/m/d;->c:I

    if-ge v2, v3, :cond_2

    iget-object v2, p0, Landroid/support/v4/m/d;->a:Ljava/lang/String;

    iget v3, p0, Landroid/support/v4/m/d;->d:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Landroid/support/v4/m/d;->d:I

    invoke-virtual {v2, v3}, Ljava/lang/String;->charAt(I)C

    move-result v2

    iput-char v2, p0, Landroid/support/v4/m/d;->e:C

    if-ne v2, v1, :cond_2d

    goto :goto_2

    .line 810
    :cond_44
    iput v0, p0, Landroid/support/v4/m/d;->d:I

    .line 811
    const/16 v0, 0x3c

    iput-char v0, p0, Landroid/support/v4/m/d;->e:C

    .line 812
    const/16 v0, 0xd

    goto :goto_1e
.end method

.method private f()B
    .registers 6

    .prologue
    const/16 v4, 0x3e

    .line 825
    iget v0, p0, Landroid/support/v4/m/d;->d:I

    .line 826
    :cond_4
    :goto_4
    iget v1, p0, Landroid/support/v4/m/d;->d:I

    if-lez v1, :cond_46

    .line 827
    iget-object v1, p0, Landroid/support/v4/m/d;->a:Ljava/lang/String;

    iget v2, p0, Landroid/support/v4/m/d;->d:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Landroid/support/v4/m/d;->d:I

    invoke-virtual {v1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v1

    iput-char v1, p0, Landroid/support/v4/m/d;->e:C

    .line 828
    iget-char v1, p0, Landroid/support/v4/m/d;->e:C

    const/16 v2, 0x3c

    if-ne v1, v2, :cond_1f

    .line 830
    const/16 v0, 0xc

    .line 844
    :goto_1e
    return v0

    .line 832
    :cond_1f
    iget-char v1, p0, Landroid/support/v4/m/d;->e:C

    if-eq v1, v4, :cond_46

    .line 835
    iget-char v1, p0, Landroid/support/v4/m/d;->e:C

    const/16 v2, 0x22

    if-eq v1, v2, :cond_2f

    iget-char v1, p0, Landroid/support/v4/m/d;->e:C

    const/16 v2, 0x27

    if-ne v1, v2, :cond_4

    .line 837
    :cond_2f
    iget-char v1, p0, Landroid/support/v4/m/d;->e:C

    .line 838
    :cond_31
    iget v2, p0, Landroid/support/v4/m/d;->d:I

    if-lez v2, :cond_4

    iget-object v2, p0, Landroid/support/v4/m/d;->a:Ljava/lang/String;

    iget v3, p0, Landroid/support/v4/m/d;->d:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Landroid/support/v4/m/d;->d:I

    invoke-virtual {v2, v3}, Ljava/lang/String;->charAt(I)C

    move-result v2

    iput-char v2, p0, Landroid/support/v4/m/d;->e:C

    if-ne v2, v1, :cond_31

    goto :goto_4

    .line 842
    :cond_46
    iput v0, p0, Landroid/support/v4/m/d;->d:I

    .line 843
    iput-char v4, p0, Landroid/support/v4/m/d;->e:C

    .line 844
    const/16 v0, 0xd

    goto :goto_1e
.end method

.method private g()B
    .registers 4

    .prologue
    .line 853
    :cond_0
    iget v0, p0, Landroid/support/v4/m/d;->d:I

    iget v1, p0, Landroid/support/v4/m/d;->c:I

    if-ge v0, v1, :cond_18

    iget-object v0, p0, Landroid/support/v4/m/d;->a:Ljava/lang/String;

    iget v1, p0, Landroid/support/v4/m/d;->d:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Landroid/support/v4/m/d;->d:I

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    iput-char v0, p0, Landroid/support/v4/m/d;->e:C

    const/16 v1, 0x3b

    if-ne v0, v1, :cond_0

    .line 854
    :cond_18
    const/16 v0, 0xc

    return v0
.end method

.method private h()B
    .registers 5

    .prologue
    const/16 v3, 0x3b

    .line 868
    iget v0, p0, Landroid/support/v4/m/d;->d:I

    .line 869
    :cond_4
    iget v1, p0, Landroid/support/v4/m/d;->d:I

    if-lez v1, :cond_23

    .line 870
    iget-object v1, p0, Landroid/support/v4/m/d;->a:Ljava/lang/String;

    iget v2, p0, Landroid/support/v4/m/d;->d:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Landroid/support/v4/m/d;->d:I

    invoke-virtual {v1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v1

    iput-char v1, p0, Landroid/support/v4/m/d;->e:C

    .line 871
    iget-char v1, p0, Landroid/support/v4/m/d;->e:C

    const/16 v2, 0x26

    if-ne v1, v2, :cond_1f

    .line 872
    const/16 v0, 0xc

    .line 880
    :goto_1e
    return v0

    .line 874
    :cond_1f
    iget-char v1, p0, Landroid/support/v4/m/d;->e:C

    if-ne v1, v3, :cond_4

    .line 878
    :cond_23
    iput v0, p0, Landroid/support/v4/m/d;->d:I

    .line 879
    iput-char v3, p0, Landroid/support/v4/m/d;->e:C

    .line 880
    const/16 v0, 0xd

    goto :goto_1e
.end method


# virtual methods
.method final a()B
    .registers 8

    .prologue
    const/16 v1, 0xd

    const/16 v0, 0xc

    const/16 v6, 0x3e

    const/16 v5, 0x3b

    .line 770
    iget-object v2, p0, Landroid/support/v4/m/d;->a:Ljava/lang/String;

    iget v3, p0, Landroid/support/v4/m/d;->d:I

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v2, v3}, Ljava/lang/String;->charAt(I)C

    move-result v2

    iput-char v2, p0, Landroid/support/v4/m/d;->e:C

    .line 771
    iget-char v2, p0, Landroid/support/v4/m/d;->e:C

    invoke-static {v2}, Ljava/lang/Character;->isLowSurrogate(C)Z

    move-result v2

    if-eqz v2, :cond_32

    .line 772
    iget-object v0, p0, Landroid/support/v4/m/d;->a:Ljava/lang/String;

    iget v1, p0, Landroid/support/v4/m/d;->d:I

    invoke-static {v0, v1}, Ljava/lang/Character;->codePointBefore(Ljava/lang/CharSequence;I)I

    move-result v0

    .line 773
    iget v1, p0, Landroid/support/v4/m/d;->d:I

    invoke-static {v0}, Ljava/lang/Character;->charCount(I)I

    move-result v2

    sub-int/2addr v1, v2

    iput v1, p0, Landroid/support/v4/m/d;->d:I

    .line 774
    invoke-static {v0}, Ljava/lang/Character;->getDirectionality(I)B

    move-result v0

    .line 786
    :cond_31
    :goto_31
    return v0

    .line 776
    :cond_32
    iget v2, p0, Landroid/support/v4/m/d;->d:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Landroid/support/v4/m/d;->d:I

    .line 777
    iget-char v2, p0, Landroid/support/v4/m/d;->e:C

    invoke-static {v2}, Landroid/support/v4/m/d;->a(C)B

    move-result v2

    .line 778
    iget-boolean v3, p0, Landroid/support/v4/m/d;->b:Z

    if-eqz v3, :cond_b6

    .line 780
    iget-char v3, p0, Landroid/support/v4/m/d;->e:C

    if-ne v3, v6, :cond_8d

    .line 3825
    iget v2, p0, Landroid/support/v4/m/d;->d:I

    .line 3826
    :cond_48
    :goto_48
    iget v3, p0, Landroid/support/v4/m/d;->d:I

    if-lez v3, :cond_87

    .line 3827
    iget-object v3, p0, Landroid/support/v4/m/d;->a:Ljava/lang/String;

    iget v4, p0, Landroid/support/v4/m/d;->d:I

    add-int/lit8 v4, v4, -0x1

    iput v4, p0, Landroid/support/v4/m/d;->d:I

    invoke-virtual {v3, v4}, Ljava/lang/String;->charAt(I)C

    move-result v3

    iput-char v3, p0, Landroid/support/v4/m/d;->e:C

    .line 3828
    iget-char v3, p0, Landroid/support/v4/m/d;->e:C

    const/16 v4, 0x3c

    if-eq v3, v4, :cond_31

    .line 3832
    iget-char v3, p0, Landroid/support/v4/m/d;->e:C

    if-eq v3, v6, :cond_87

    .line 3835
    iget-char v3, p0, Landroid/support/v4/m/d;->e:C

    const/16 v4, 0x22

    if-eq v3, v4, :cond_70

    iget-char v3, p0, Landroid/support/v4/m/d;->e:C

    const/16 v4, 0x27

    if-ne v3, v4, :cond_48

    .line 3837
    :cond_70
    iget-char v3, p0, Landroid/support/v4/m/d;->e:C

    .line 3838
    :cond_72
    iget v4, p0, Landroid/support/v4/m/d;->d:I

    if-lez v4, :cond_48

    iget-object v4, p0, Landroid/support/v4/m/d;->a:Ljava/lang/String;

    iget v5, p0, Landroid/support/v4/m/d;->d:I

    add-int/lit8 v5, v5, -0x1

    iput v5, p0, Landroid/support/v4/m/d;->d:I

    invoke-virtual {v4, v5}, Ljava/lang/String;->charAt(I)C

    move-result v4

    iput-char v4, p0, Landroid/support/v4/m/d;->e:C

    if-ne v4, v3, :cond_72

    goto :goto_48

    .line 3842
    :cond_87
    iput v2, p0, Landroid/support/v4/m/d;->d:I

    .line 3843
    iput-char v6, p0, Landroid/support/v4/m/d;->e:C

    move v0, v1

    .line 3844
    goto :goto_31

    .line 782
    :cond_8d
    iget-char v3, p0, Landroid/support/v4/m/d;->e:C

    if-ne v3, v5, :cond_b6

    .line 3868
    iget v2, p0, Landroid/support/v4/m/d;->d:I

    .line 3869
    :cond_93
    iget v3, p0, Landroid/support/v4/m/d;->d:I

    if-lez v3, :cond_af

    .line 3870
    iget-object v3, p0, Landroid/support/v4/m/d;->a:Ljava/lang/String;

    iget v4, p0, Landroid/support/v4/m/d;->d:I

    add-int/lit8 v4, v4, -0x1

    iput v4, p0, Landroid/support/v4/m/d;->d:I

    invoke-virtual {v3, v4}, Ljava/lang/String;->charAt(I)C

    move-result v3

    iput-char v3, p0, Landroid/support/v4/m/d;->e:C

    .line 3871
    iget-char v3, p0, Landroid/support/v4/m/d;->e:C

    const/16 v4, 0x26

    if-eq v3, v4, :cond_31

    .line 3874
    iget-char v3, p0, Landroid/support/v4/m/d;->e:C

    if-ne v3, v5, :cond_93

    .line 3878
    :cond_af
    iput v2, p0, Landroid/support/v4/m/d;->d:I

    .line 3879
    iput-char v5, p0, Landroid/support/v4/m/d;->e:C

    move v0, v1

    .line 3880
    goto/16 :goto_31

    :cond_b6
    move v0, v2

    goto/16 :goto_31
.end method
