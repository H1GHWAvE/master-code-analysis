.class Landroid/support/design/widget/df;
.super Landroid/support/design/widget/t;
.source "SourceFile"


# instance fields
.field private a:Landroid/support/design/widget/dg;

.field private b:I

.field private c:I


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    const/4 v0, 0x0

    .line 33
    invoke-direct {p0}, Landroid/support/design/widget/t;-><init>()V

    .line 30
    iput v0, p0, Landroid/support/design/widget/df;->b:I

    .line 31
    iput v0, p0, Landroid/support/design/widget/df;->c:I

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4

    .prologue
    const/4 v0, 0x0

    .line 36
    invoke-direct {p0, p1, p2}, Landroid/support/design/widget/t;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 30
    iput v0, p0, Landroid/support/design/widget/df;->b:I

    .line 31
    iput v0, p0, Landroid/support/design/widget/df;->c:I

    .line 37
    return-void
.end method


# virtual methods
.method public a(I)Z
    .registers 3

    .prologue
    .line 71
    iget-object v0, p0, Landroid/support/design/widget/df;->a:Landroid/support/design/widget/dg;

    if-eqz v0, :cond_b

    .line 72
    iget-object v0, p0, Landroid/support/design/widget/df;->a:Landroid/support/design/widget/dg;

    invoke-virtual {v0, p1}, Landroid/support/design/widget/dg;->b(I)Z

    move-result v0

    .line 76
    :goto_a
    return v0

    .line 74
    :cond_b
    iput p1, p0, Landroid/support/design/widget/df;->c:I

    .line 76
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;I)Z
    .registers 7

    .prologue
    const/4 v2, 0x0

    .line 42
    invoke-virtual {p1, p2, p3}, Landroid/support/design/widget/CoordinatorLayout;->a(Landroid/view/View;I)V

    .line 44
    iget-object v0, p0, Landroid/support/design/widget/df;->a:Landroid/support/design/widget/dg;

    if-nez v0, :cond_f

    .line 45
    new-instance v0, Landroid/support/design/widget/dg;

    invoke-direct {v0, p2}, Landroid/support/design/widget/dg;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Landroid/support/design/widget/df;->a:Landroid/support/design/widget/dg;

    .line 47
    :cond_f
    iget-object v0, p0, Landroid/support/design/widget/df;->a:Landroid/support/design/widget/dg;

    invoke-virtual {v0}, Landroid/support/design/widget/dg;->a()V

    .line 49
    iget v0, p0, Landroid/support/design/widget/df;->b:I

    if-eqz v0, :cond_21

    .line 50
    iget-object v0, p0, Landroid/support/design/widget/df;->a:Landroid/support/design/widget/dg;

    iget v1, p0, Landroid/support/design/widget/df;->b:I

    invoke-virtual {v0, v1}, Landroid/support/design/widget/dg;->a(I)Z

    .line 51
    iput v2, p0, Landroid/support/design/widget/df;->b:I

    .line 53
    :cond_21
    iget v0, p0, Landroid/support/design/widget/df;->c:I

    if-eqz v0, :cond_2e

    .line 54
    iget-object v0, p0, Landroid/support/design/widget/df;->a:Landroid/support/design/widget/dg;

    iget v1, p0, Landroid/support/design/widget/df;->c:I

    invoke-virtual {v0, v1}, Landroid/support/design/widget/dg;->b(I)Z

    .line 55
    iput v2, p0, Landroid/support/design/widget/df;->c:I

    .line 58
    :cond_2e
    const/4 v0, 0x1

    return v0
.end method

.method public b()I
    .registers 2

    .prologue
    .line 84
    iget-object v0, p0, Landroid/support/design/widget/df;->a:Landroid/support/design/widget/dg;

    if-eqz v0, :cond_9

    iget-object v0, p0, Landroid/support/design/widget/df;->a:Landroid/support/design/widget/dg;

    .line 1109
    iget v0, v0, Landroid/support/design/widget/dg;->b:I

    .line 84
    :goto_8
    return v0

    :cond_9
    const/4 v0, 0x0

    goto :goto_8
.end method

.method public b(I)Z
    .registers 3

    .prologue
    .line 62
    iget-object v0, p0, Landroid/support/design/widget/df;->a:Landroid/support/design/widget/dg;

    if-eqz v0, :cond_b

    .line 63
    iget-object v0, p0, Landroid/support/design/widget/df;->a:Landroid/support/design/widget/dg;

    invoke-virtual {v0, p1}, Landroid/support/design/widget/dg;->a(I)Z

    move-result v0

    .line 67
    :goto_a
    return v0

    .line 65
    :cond_b
    iput p1, p0, Landroid/support/design/widget/df;->b:I

    .line 67
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public c()I
    .registers 2

    .prologue
    .line 80
    iget-object v0, p0, Landroid/support/design/widget/df;->a:Landroid/support/design/widget/dg;

    if-eqz v0, :cond_9

    iget-object v0, p0, Landroid/support/design/widget/df;->a:Landroid/support/design/widget/dg;

    .line 1105
    iget v0, v0, Landroid/support/design/widget/dg;->a:I

    .line 80
    :goto_8
    return v0

    :cond_9
    const/4 v0, 0x0

    goto :goto_8
.end method
