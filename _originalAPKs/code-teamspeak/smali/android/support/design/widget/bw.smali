.class final Landroid/support/design/widget/bw;
.super Landroid/widget/LinearLayout;
.source "SourceFile"


# instance fields
.field a:I

.field b:F

.field c:I

.field d:I

.field final synthetic e:Landroid/support/design/widget/br;

.field private f:I

.field private final g:Landroid/graphics/Paint;


# direct methods
.method constructor <init>(Landroid/support/design/widget/br;Landroid/content/Context;)V
    .registers 4

    .prologue
    const/4 v0, -0x1

    .line 1330
    iput-object p1, p0, Landroid/support/design/widget/bw;->e:Landroid/support/design/widget/br;

    .line 1331
    invoke-direct {p0, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 1324
    iput v0, p0, Landroid/support/design/widget/bw;->a:I

    .line 1327
    iput v0, p0, Landroid/support/design/widget/bw;->c:I

    .line 1328
    iput v0, p0, Landroid/support/design/widget/bw;->d:I

    .line 1332
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/support/design/widget/bw;->setWillNotDraw(Z)V

    .line 1333
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Landroid/support/design/widget/bw;->g:Landroid/graphics/Paint;

    .line 1334
    return-void
.end method

.method static synthetic a(Landroid/support/design/widget/bw;)F
    .registers 2

    .prologue
    .line 1320
    const/4 v0, 0x0

    iput v0, p0, Landroid/support/design/widget/bw;->b:F

    return v0
.end method

.method static synthetic a(Landroid/support/design/widget/bw;I)I
    .registers 2

    .prologue
    .line 1320
    iput p1, p0, Landroid/support/design/widget/bw;->a:I

    return p1
.end method

.method private a(IF)V
    .registers 3

    .prologue
    .line 1361
    iput p1, p0, Landroid/support/design/widget/bw;->a:I

    .line 1362
    iput p2, p0, Landroid/support/design/widget/bw;->b:F

    .line 1363
    invoke-virtual {p0}, Landroid/support/design/widget/bw;->a()V

    .line 1364
    return-void
.end method

.method private a(II)V
    .registers 4

    .prologue
    .line 1451
    iget v0, p0, Landroid/support/design/widget/bw;->c:I

    if-ne p1, v0, :cond_8

    iget v0, p0, Landroid/support/design/widget/bw;->d:I

    if-eq p2, v0, :cond_f

    .line 1453
    :cond_8
    iput p1, p0, Landroid/support/design/widget/bw;->c:I

    .line 1454
    iput p2, p0, Landroid/support/design/widget/bw;->d:I

    .line 1455
    invoke-static {p0}, Landroid/support/v4/view/cx;->b(Landroid/view/View;)V

    .line 1457
    :cond_f
    return-void
.end method

.method static synthetic a(Landroid/support/design/widget/bw;II)V
    .registers 3

    .prologue
    .line 1320
    invoke-direct {p0, p1, p2}, Landroid/support/design/widget/bw;->a(II)V

    return-void
.end method

.method private b()Z
    .registers 5

    .prologue
    const/4 v0, 0x0

    .line 1351
    invoke-virtual {p0}, Landroid/support/design/widget/bw;->getChildCount()I

    move-result v2

    move v1, v0

    :goto_6
    if-ge v1, v2, :cond_13

    .line 1352
    invoke-virtual {p0, v1}, Landroid/support/design/widget/bw;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 1353
    invoke-virtual {v3}, Landroid/view/View;->getWidth()I

    move-result v3

    if-gtz v3, :cond_14

    .line 1354
    const/4 v0, 0x1

    .line 1357
    :cond_13
    return v0

    .line 1351
    :cond_14
    add-int/lit8 v1, v1, 0x1

    goto :goto_6
.end method

.method private c()F
    .registers 3

    .prologue
    .line 1367
    iget v0, p0, Landroid/support/design/widget/bw;->a:I

    int-to-float v0, v0

    iget v1, p0, Landroid/support/design/widget/bw;->b:F

    add-float/2addr v0, v1

    return v0
.end method

.method private c(I)V
    .registers 9

    .prologue
    const/4 v1, 0x1

    .line 1460
    invoke-static {p0}, Landroid/support/v4/view/cx;->f(Landroid/view/View;)I

    move-result v0

    if-ne v0, v1, :cond_5e

    move v0, v1

    .line 1463
    :goto_8
    invoke-virtual {p0, p1}, Landroid/support/design/widget/bw;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 1464
    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v3

    .line 1465
    invoke-virtual {v2}, Landroid/view/View;->getRight()I

    move-result v5

    .line 1469
    iget v2, p0, Landroid/support/design/widget/bw;->a:I

    sub-int v2, p1, v2

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    if-gt v2, v1, :cond_60

    .line 1471
    iget v2, p0, Landroid/support/design/widget/bw;->c:I

    .line 1472
    iget v4, p0, Landroid/support/design/widget/bw;->d:I

    .line 1493
    :goto_22
    if-ne v2, v3, :cond_26

    if-eq v4, v5, :cond_5d

    .line 1494
    :cond_26
    iget-object v0, p0, Landroid/support/design/widget/bw;->e:Landroid/support/design/widget/br;

    invoke-static {}, Landroid/support/design/widget/dh;->a()Landroid/support/design/widget/ck;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/support/design/widget/br;->a(Landroid/support/design/widget/br;Landroid/support/design/widget/ck;)Landroid/support/design/widget/ck;

    move-result-object v6

    .line 1495
    sget-object v0, Landroid/support/design/widget/a;->b:Landroid/view/animation/Interpolator;

    invoke-virtual {v6, v0}, Landroid/support/design/widget/ck;->a(Landroid/view/animation/Interpolator;)V

    .line 1496
    const/16 v0, 0x12c

    invoke-virtual {v6, v0}, Landroid/support/design/widget/ck;->a(I)V

    .line 1497
    const/4 v0, 0x0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v6, v0, v1}, Landroid/support/design/widget/ck;->a(FF)V

    .line 1498
    new-instance v0, Landroid/support/design/widget/bx;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Landroid/support/design/widget/bx;-><init>(Landroid/support/design/widget/bw;IIII)V

    invoke-virtual {v6, v0}, Landroid/support/design/widget/ck;->a(Landroid/support/design/widget/cp;)V

    .line 1507
    new-instance v0, Landroid/support/design/widget/by;

    invoke-direct {v0, p0, p1}, Landroid/support/design/widget/by;-><init>(Landroid/support/design/widget/bw;I)V

    .line 2142
    iget-object v1, v6, Landroid/support/design/widget/ck;->a:Landroid/support/design/widget/cr;

    new-instance v2, Landroid/support/design/widget/cm;

    invoke-direct {v2, v6, v0}, Landroid/support/design/widget/cm;-><init>(Landroid/support/design/widget/ck;Landroid/support/design/widget/cn;)V

    invoke-virtual {v1, v2}, Landroid/support/design/widget/cr;->a(Landroid/support/design/widget/cs;)V

    .line 3116
    iget-object v0, v6, Landroid/support/design/widget/ck;->a:Landroid/support/design/widget/cr;

    invoke-virtual {v0}, Landroid/support/design/widget/cr;->a()V

    .line 1522
    :cond_5d
    return-void

    .line 1460
    :cond_5e
    const/4 v0, 0x0

    goto :goto_8

    .line 1475
    :cond_60
    iget-object v1, p0, Landroid/support/design/widget/bw;->e:Landroid/support/design/widget/br;

    const/16 v2, 0x18

    invoke-static {v1, v2}, Landroid/support/design/widget/br;->b(Landroid/support/design/widget/br;I)I

    move-result v1

    .line 1476
    iget v2, p0, Landroid/support/design/widget/bw;->a:I

    if-ge p1, v2, :cond_72

    .line 1478
    if-nez v0, :cond_78

    .line 1481
    add-int v4, v5, v1

    move v2, v4

    goto :goto_22

    .line 1485
    :cond_72
    if-eqz v0, :cond_78

    .line 1486
    add-int v4, v5, v1

    move v2, v4

    goto :goto_22

    .line 1488
    :cond_78
    sub-int v4, v3, v1

    move v2, v4

    goto :goto_22
.end method


# virtual methods
.method final a()V
    .registers 7

    .prologue
    const/high16 v5, 0x3f800000    # 1.0f

    .line 1428
    iget v0, p0, Landroid/support/design/widget/bw;->a:I

    invoke-virtual {p0, v0}, Landroid/support/design/widget/bw;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 1431
    if-eqz v1, :cond_55

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v0

    if-lez v0, :cond_55

    .line 1432
    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v0

    .line 1433
    invoke-virtual {v1}, Landroid/view/View;->getRight()I

    move-result v1

    .line 1435
    iget v2, p0, Landroid/support/design/widget/bw;->b:F

    const/4 v3, 0x0

    cmpl-float v2, v2, v3

    if-lez v2, :cond_51

    iget v2, p0, Landroid/support/design/widget/bw;->a:I

    invoke-virtual {p0}, Landroid/support/design/widget/bw;->getChildCount()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ge v2, v3, :cond_51

    .line 1437
    iget v2, p0, Landroid/support/design/widget/bw;->a:I

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {p0, v2}, Landroid/support/design/widget/bw;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 1438
    iget v3, p0, Landroid/support/design/widget/bw;->b:F

    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v3, v4

    iget v4, p0, Landroid/support/design/widget/bw;->b:F

    sub-float v4, v5, v4

    int-to-float v0, v0

    mul-float/2addr v0, v4

    add-float/2addr v0, v3

    float-to-int v0, v0

    .line 1440
    iget v3, p0, Landroid/support/design/widget/bw;->b:F

    invoke-virtual {v2}, Landroid/view/View;->getRight()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v2, v3

    iget v3, p0, Landroid/support/design/widget/bw;->b:F

    sub-float v3, v5, v3

    int-to-float v1, v1

    mul-float/2addr v1, v3

    add-float/2addr v1, v2

    float-to-int v1, v1

    .line 1447
    :cond_51
    :goto_51
    invoke-direct {p0, v0, v1}, Landroid/support/design/widget/bw;->a(II)V

    .line 1448
    return-void

    .line 1444
    :cond_55
    const/4 v0, -0x1

    move v1, v0

    goto :goto_51
.end method

.method final a(I)V
    .registers 3

    .prologue
    .line 1337
    iget-object v0, p0, Landroid/support/design/widget/bw;->g:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getColor()I

    move-result v0

    if-eq v0, p1, :cond_10

    .line 1338
    iget-object v0, p0, Landroid/support/design/widget/bw;->g:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1339
    invoke-static {p0}, Landroid/support/v4/view/cx;->b(Landroid/view/View;)V

    .line 1341
    :cond_10
    return-void
.end method

.method final b(I)V
    .registers 3

    .prologue
    .line 1344
    iget v0, p0, Landroid/support/design/widget/bw;->f:I

    if-eq v0, p1, :cond_9

    .line 1345
    iput p1, p0, Landroid/support/design/widget/bw;->f:I

    .line 1346
    invoke-static {p0}, Landroid/support/v4/view/cx;->b(Landroid/view/View;)V

    .line 1348
    :cond_9
    return-void
.end method

.method public final draw(Landroid/graphics/Canvas;)V
    .registers 8

    .prologue
    .line 1526
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->draw(Landroid/graphics/Canvas;)V

    .line 1529
    iget v0, p0, Landroid/support/design/widget/bw;->c:I

    if-ltz v0, :cond_26

    iget v0, p0, Landroid/support/design/widget/bw;->d:I

    iget v1, p0, Landroid/support/design/widget/bw;->c:I

    if-le v0, v1, :cond_26

    .line 1530
    iget v0, p0, Landroid/support/design/widget/bw;->c:I

    int-to-float v1, v0

    invoke-virtual {p0}, Landroid/support/design/widget/bw;->getHeight()I

    move-result v0

    iget v2, p0, Landroid/support/design/widget/bw;->f:I

    sub-int/2addr v0, v2

    int-to-float v2, v0

    iget v0, p0, Landroid/support/design/widget/bw;->d:I

    int-to-float v3, v0

    invoke-virtual {p0}, Landroid/support/design/widget/bw;->getHeight()I

    move-result v0

    int-to-float v4, v0

    iget-object v5, p0, Landroid/support/design/widget/bw;->g:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 1533
    :cond_26
    return-void
.end method

.method protected final onLayout(ZIIII)V
    .registers 6

    .prologue
    .line 1422
    invoke-super/range {p0 .. p5}, Landroid/widget/LinearLayout;->onLayout(ZIIII)V

    .line 1424
    invoke-virtual {p0}, Landroid/support/design/widget/bw;->a()V

    .line 1425
    return-void
.end method

.method protected final onMeasure(II)V
    .registers 9

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 1372
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    .line 1374
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    const/high16 v2, 0x40000000    # 2.0f

    if-eq v1, v2, :cond_e

    .line 1418
    :cond_d
    :goto_d
    return-void

    .line 1380
    :cond_e
    iget-object v1, p0, Landroid/support/design/widget/bw;->e:Landroid/support/design/widget/br;

    invoke-static {v1}, Landroid/support/design/widget/br;->j(Landroid/support/design/widget/br;)I

    move-result v1

    if-ne v1, v3, :cond_d

    iget-object v1, p0, Landroid/support/design/widget/bw;->e:Landroid/support/design/widget/br;

    invoke-static {v1}, Landroid/support/design/widget/br;->k(Landroid/support/design/widget/br;)I

    move-result v1

    if-ne v1, v3, :cond_d

    .line 1381
    invoke-virtual {p0}, Landroid/support/design/widget/bw;->getChildCount()I

    move-result v3

    .line 1383
    invoke-static {v0, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    move v1, v0

    move v2, v0

    .line 1387
    :goto_28
    if-ge v1, v3, :cond_3c

    .line 1388
    invoke-virtual {p0, v1}, Landroid/support/design/widget/bw;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 1389
    invoke-virtual {v5, v4, p2}, Landroid/view/View;->measure(II)V

    .line 1390
    invoke-virtual {v5}, Landroid/view/View;->getMeasuredWidth()I

    move-result v5

    invoke-static {v2, v5}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 1387
    add-int/lit8 v1, v1, 0x1

    goto :goto_28

    .line 1393
    :cond_3c
    if-lez v2, :cond_d

    .line 1398
    iget-object v1, p0, Landroid/support/design/widget/bw;->e:Landroid/support/design/widget/br;

    const/16 v4, 0x10

    invoke-static {v1, v4}, Landroid/support/design/widget/br;->b(Landroid/support/design/widget/br;I)I

    move-result v1

    .line 1399
    mul-int v4, v2, v3

    invoke-virtual {p0}, Landroid/support/design/widget/bw;->getMeasuredWidth()I

    move-result v5

    mul-int/lit8 v1, v1, 0x2

    sub-int v1, v5, v1

    if-gt v4, v1, :cond_68

    move v1, v0

    .line 1402
    :goto_53
    if-ge v1, v3, :cond_72

    .line 1403
    invoke-virtual {p0, v1}, Landroid/support/design/widget/bw;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1404
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 1405
    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 1406
    const/4 v4, 0x0

    iput v4, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 1402
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_53

    .line 1411
    :cond_68
    iget-object v0, p0, Landroid/support/design/widget/bw;->e:Landroid/support/design/widget/br;

    invoke-static {v0}, Landroid/support/design/widget/br;->l(Landroid/support/design/widget/br;)I

    .line 1412
    iget-object v0, p0, Landroid/support/design/widget/bw;->e:Landroid/support/design/widget/br;

    invoke-static {v0}, Landroid/support/design/widget/br;->m(Landroid/support/design/widget/br;)V

    .line 1416
    :cond_72
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    goto :goto_d
.end method
