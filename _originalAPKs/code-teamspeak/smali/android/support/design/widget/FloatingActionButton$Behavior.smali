.class public Landroid/support/design/widget/FloatingActionButton$Behavior;
.super Landroid/support/design/widget/t;
.source "SourceFile"


# static fields
.field private static final a:Z


# instance fields
.field private b:Landroid/graphics/Rect;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 325
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_a

    const/4 v0, 0x1

    :goto_7
    sput-boolean v0, Landroid/support/design/widget/FloatingActionButton$Behavior;->a:Z

    return-void

    :cond_a
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 322
    invoke-direct {p0}, Landroid/support/design/widget/t;-><init>()V

    return-void
.end method

.method private static a(Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/FloatingActionButton;)V
    .registers 13

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 397
    invoke-virtual {p1}, Landroid/support/design/widget/FloatingActionButton;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_9

    .line 403
    :goto_8
    return-void

    .line 6407
    :cond_9
    const/4 v4, 0x0

    .line 6408
    invoke-virtual {p0, p1}, Landroid/support/design/widget/CoordinatorLayout;->a(Landroid/view/View;)Ljava/util/List;

    move-result-object v6

    .line 6409
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v7

    move v5, v3

    :goto_13
    if-ge v5, v7, :cond_7a

    .line 6410
    invoke-interface {v6, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 6411
    instance-of v1, v0, Landroid/support/design/widget/Snackbar$SnackbarLayout;

    if-eqz v1, :cond_7e

    .line 7360
    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_78

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_78

    .line 7361
    iget-object v8, p0, Landroid/support/design/widget/CoordinatorLayout;->i:Landroid/graphics/Rect;

    .line 7362
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-eq v1, p0, :cond_72

    move v1, v2

    :goto_34
    invoke-virtual {p0, p1, v1, v8}, Landroid/support/design/widget/CoordinatorLayout;->a(Landroid/view/View;ZLandroid/graphics/Rect;)V

    .line 7363
    iget-object v9, p0, Landroid/support/design/widget/CoordinatorLayout;->j:Landroid/graphics/Rect;

    .line 7364
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-eq v1, p0, :cond_74

    move v1, v2

    :goto_40
    invoke-virtual {p0, v0, v1, v9}, Landroid/support/design/widget/CoordinatorLayout;->a(Landroid/view/View;ZLandroid/graphics/Rect;)V

    .line 7366
    iget v1, v8, Landroid/graphics/Rect;->left:I

    iget v10, v9, Landroid/graphics/Rect;->right:I

    if-gt v1, v10, :cond_76

    iget v1, v8, Landroid/graphics/Rect;->top:I

    iget v10, v9, Landroid/graphics/Rect;->bottom:I

    if-gt v1, v10, :cond_76

    iget v1, v8, Landroid/graphics/Rect;->right:I

    iget v10, v9, Landroid/graphics/Rect;->left:I

    if-lt v1, v10, :cond_76

    iget v1, v8, Landroid/graphics/Rect;->bottom:I

    iget v8, v9, Landroid/graphics/Rect;->top:I

    if-lt v1, v8, :cond_76

    move v1, v2

    .line 6411
    :goto_5c
    if-eqz v1, :cond_7e

    .line 6412
    invoke-static {v0}, Landroid/support/v4/view/cx;->n(Landroid/view/View;)F

    move-result v1

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    int-to-float v0, v0

    sub-float v0, v1, v0

    invoke-static {v4, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 6409
    :goto_6d
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    move v4, v0

    goto :goto_13

    :cond_72
    move v1, v3

    .line 7362
    goto :goto_34

    :cond_74
    move v1, v3

    .line 7364
    goto :goto_40

    :cond_76
    move v1, v3

    .line 7366
    goto :goto_5c

    :cond_78
    move v1, v3

    .line 7369
    goto :goto_5c

    .line 402
    :cond_7a
    invoke-static {p1, v4}, Landroid/support/v4/view/cx;->b(Landroid/view/View;F)V

    goto :goto_8

    :cond_7e
    move v0, v4

    goto :goto_6d
.end method

.method private static a(Landroid/support/design/widget/FloatingActionButton;Landroid/view/View;)V
    .registers 5

    .prologue
    const/4 v1, 0x0

    .line 352
    instance-of v0, p1, Landroid/support/design/widget/Snackbar$SnackbarLayout;

    if-eqz v0, :cond_43

    .line 355
    invoke-static {p0}, Landroid/support/v4/view/cx;->n(Landroid/view/View;)F

    move-result v0

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_43

    .line 356
    invoke-static {p0}, Landroid/support/v4/view/cx;->p(Landroid/view/View;)Landroid/support/v4/view/fk;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/support/v4/view/fk;->b(F)Landroid/support/v4/view/fk;

    move-result-object v1

    .line 3986
    iget-object v0, v1, Landroid/support/v4/view/fk;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_24

    .line 3987
    sget-object v2, Landroid/support/v4/view/fk;->c:Landroid/support/v4/view/fu;

    invoke-interface {v2, v1, v0}, Landroid/support/v4/view/fu;->a(Landroid/support/v4/view/fk;Landroid/view/View;)V

    .line 4020
    :cond_24
    iget-object v0, v1, Landroid/support/v4/view/fk;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_33

    .line 4021
    sget-object v2, Landroid/support/v4/view/fk;->c:Landroid/support/v4/view/fu;

    invoke-interface {v2, v1, v0}, Landroid/support/v4/view/fu;->b(Landroid/support/v4/view/fk;Landroid/view/View;)V

    .line 356
    :cond_33
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {v1, v0}, Landroid/support/v4/view/fk;->a(F)Landroid/support/v4/view/fk;

    move-result-object v0

    sget-object v1, Landroid/support/design/widget/a;->b:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/fk;->a(Landroid/view/animation/Interpolator;)Landroid/support/v4/view/fk;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/view/fk;->a(Landroid/support/v4/view/gd;)Landroid/support/v4/view/fk;

    .line 365
    :cond_43
    return-void
.end method

.method private a(Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/AppBarLayout;Landroid/support/design/widget/FloatingActionButton;)Z
    .registers 6

    .prologue
    .line 369
    invoke-virtual {p3}, Landroid/support/design/widget/FloatingActionButton;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/w;

    .line 4256
    iget v0, v0, Landroid/support/design/widget/w;->f:I

    .line 371
    invoke-virtual {p2}, Landroid/support/design/widget/AppBarLayout;->getId()I

    move-result v1

    if-eq v0, v1, :cond_10

    .line 374
    const/4 v0, 0x0

    .line 392
    :goto_f
    return v0

    .line 377
    :cond_10
    iget-object v0, p0, Landroid/support/design/widget/FloatingActionButton$Behavior;->b:Landroid/graphics/Rect;

    if-nez v0, :cond_1b

    .line 378
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Landroid/support/design/widget/FloatingActionButton$Behavior;->b:Landroid/graphics/Rect;

    .line 382
    :cond_1b
    iget-object v0, p0, Landroid/support/design/widget/FloatingActionButton$Behavior;->b:Landroid/graphics/Rect;

    .line 383
    invoke-static {p1, p2, v0}, Landroid/support/design/widget/cz;->a(Landroid/view/ViewGroup;Landroid/view/View;Landroid/graphics/Rect;)V

    .line 385
    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {p2}, Landroid/support/design/widget/AppBarLayout;->getMinimumHeightForVisibleOverlappingContent()I

    move-result v1

    if-gt v0, v1, :cond_2f

    .line 5250
    iget-object v0, p3, Landroid/support/design/widget/FloatingActionButton;->a:Landroid/support/design/widget/al;

    invoke-virtual {v0}, Landroid/support/design/widget/al;->b()V

    .line 392
    :goto_2d
    const/4 v0, 0x1

    goto :goto_f

    .line 6242
    :cond_2f
    iget-object v0, p3, Landroid/support/design/widget/FloatingActionButton;->a:Landroid/support/design/widget/al;

    invoke-virtual {v0}, Landroid/support/design/widget/al;->c()V

    goto :goto_2d
.end method

.method private a(Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/FloatingActionButton;I)Z
    .registers 11

    .prologue
    const/4 v2, 0x0

    .line 424
    invoke-virtual {p1, p2}, Landroid/support/design/widget/CoordinatorLayout;->a(Landroid/view/View;)Ljava/util/List;

    move-result-object v3

    .line 425
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    move v1, v2

    :goto_a
    if-ge v1, v4, :cond_22

    .line 426
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 427
    instance-of v5, v0, Landroid/support/design/widget/AppBarLayout;

    if-eqz v5, :cond_1e

    check-cast v0, Landroid/support/design/widget/AppBarLayout;

    invoke-direct {p0, p1, v0, p2}, Landroid/support/design/widget/FloatingActionButton$Behavior;->a(Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/AppBarLayout;Landroid/support/design/widget/FloatingActionButton;)Z

    move-result v0

    if-nez v0, :cond_22

    .line 425
    :cond_1e
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_a

    .line 433
    :cond_22
    invoke-virtual {p1, p2, p3}, Landroid/support/design/widget/CoordinatorLayout;->a(Landroid/view/View;I)V

    .line 8445
    invoke-static {p2}, Landroid/support/design/widget/FloatingActionButton;->a(Landroid/support/design/widget/FloatingActionButton;)Landroid/graphics/Rect;

    move-result-object v3

    .line 8447
    if-eqz v3, :cond_61

    invoke-virtual {v3}, Landroid/graphics/Rect;->centerX()I

    move-result v0

    if-lez v0, :cond_61

    invoke-virtual {v3}, Landroid/graphics/Rect;->centerY()I

    move-result v0

    if-lez v0, :cond_61

    .line 8448
    invoke-virtual {p2}, Landroid/support/design/widget/FloatingActionButton;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/w;

    .line 8453
    invoke-virtual {p2}, Landroid/support/design/widget/FloatingActionButton;->getRight()I

    move-result v1

    invoke-virtual {p1}, Landroid/support/design/widget/CoordinatorLayout;->getWidth()I

    move-result v4

    iget v5, v0, Landroid/support/design/widget/w;->rightMargin:I

    sub-int/2addr v4, v5

    if-lt v1, v4, :cond_63

    .line 8455
    iget v1, v3, Landroid/graphics/Rect;->right:I

    .line 8460
    :goto_4c
    invoke-virtual {p2}, Landroid/support/design/widget/FloatingActionButton;->getBottom()I

    move-result v4

    invoke-virtual {p1}, Landroid/support/design/widget/CoordinatorLayout;->getBottom()I

    move-result v5

    iget v6, v0, Landroid/support/design/widget/w;->bottomMargin:I

    sub-int/2addr v5, v6

    if-lt v4, v5, :cond_6f

    .line 8462
    iget v2, v3, Landroid/graphics/Rect;->bottom:I

    .line 8468
    :cond_5b
    :goto_5b
    invoke-virtual {p2, v2}, Landroid/support/design/widget/FloatingActionButton;->offsetTopAndBottom(I)V

    .line 8469
    invoke-virtual {p2, v1}, Landroid/support/design/widget/FloatingActionButton;->offsetLeftAndRight(I)V

    .line 436
    :cond_61
    const/4 v0, 0x1

    return v0

    .line 8456
    :cond_63
    invoke-virtual {p2}, Landroid/support/design/widget/FloatingActionButton;->getLeft()I

    move-result v1

    iget v4, v0, Landroid/support/design/widget/w;->leftMargin:I

    if-gt v1, v4, :cond_7b

    .line 8458
    iget v1, v3, Landroid/graphics/Rect;->left:I

    neg-int v1, v1

    goto :goto_4c

    .line 8463
    :cond_6f
    invoke-virtual {p2}, Landroid/support/design/widget/FloatingActionButton;->getTop()I

    move-result v4

    iget v0, v0, Landroid/support/design/widget/w;->topMargin:I

    if-gt v4, v0, :cond_5b

    .line 8465
    iget v0, v3, Landroid/graphics/Rect;->top:I

    neg-int v2, v0

    goto :goto_5b

    :cond_7b
    move v1, v2

    goto :goto_4c
.end method

.method private a(Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/FloatingActionButton;Landroid/view/View;)Z
    .registers 15

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 339
    instance-of v0, p3, Landroid/support/design/widget/Snackbar$SnackbarLayout;

    if-eqz v0, :cond_81

    .line 2397
    invoke-virtual {p2}, Landroid/support/design/widget/FloatingActionButton;->getVisibility()I

    move-result v0

    if-nez v0, :cond_80

    .line 2407
    const/4 v4, 0x0

    .line 2408
    invoke-virtual {p1, p2}, Landroid/support/design/widget/CoordinatorLayout;->a(Landroid/view/View;)Ljava/util/List;

    move-result-object v6

    .line 2409
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v7

    move v5, v3

    :goto_16
    if-ge v5, v7, :cond_7d

    .line 2410
    invoke-interface {v6, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 2411
    instance-of v1, v0, Landroid/support/design/widget/Snackbar$SnackbarLayout;

    if-eqz v1, :cond_8b

    .line 3360
    invoke-virtual {p2}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_7b

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_7b

    .line 3361
    iget-object v8, p1, Landroid/support/design/widget/CoordinatorLayout;->i:Landroid/graphics/Rect;

    .line 3362
    invoke-virtual {p2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-eq v1, p1, :cond_75

    move v1, v2

    :goto_37
    invoke-virtual {p1, p2, v1, v8}, Landroid/support/design/widget/CoordinatorLayout;->a(Landroid/view/View;ZLandroid/graphics/Rect;)V

    .line 3363
    iget-object v9, p1, Landroid/support/design/widget/CoordinatorLayout;->j:Landroid/graphics/Rect;

    .line 3364
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-eq v1, p1, :cond_77

    move v1, v2

    :goto_43
    invoke-virtual {p1, v0, v1, v9}, Landroid/support/design/widget/CoordinatorLayout;->a(Landroid/view/View;ZLandroid/graphics/Rect;)V

    .line 3366
    iget v1, v8, Landroid/graphics/Rect;->left:I

    iget v10, v9, Landroid/graphics/Rect;->right:I

    if-gt v1, v10, :cond_79

    iget v1, v8, Landroid/graphics/Rect;->top:I

    iget v10, v9, Landroid/graphics/Rect;->bottom:I

    if-gt v1, v10, :cond_79

    iget v1, v8, Landroid/graphics/Rect;->right:I

    iget v10, v9, Landroid/graphics/Rect;->left:I

    if-lt v1, v10, :cond_79

    iget v1, v8, Landroid/graphics/Rect;->bottom:I

    iget v8, v9, Landroid/graphics/Rect;->top:I

    if-lt v1, v8, :cond_79

    move v1, v2

    .line 2411
    :goto_5f
    if-eqz v1, :cond_8b

    .line 2412
    invoke-static {v0}, Landroid/support/v4/view/cx;->n(Landroid/view/View;)F

    move-result v1

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    int-to-float v0, v0

    sub-float v0, v1, v0

    invoke-static {v4, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 2409
    :goto_70
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    move v4, v0

    goto :goto_16

    :cond_75
    move v1, v3

    .line 3362
    goto :goto_37

    :cond_77
    move v1, v3

    .line 3364
    goto :goto_43

    :cond_79
    move v1, v3

    .line 3366
    goto :goto_5f

    :cond_7b
    move v1, v3

    .line 3369
    goto :goto_5f

    .line 2402
    :cond_7d
    invoke-static {p2, v4}, Landroid/support/v4/view/cx;->b(Landroid/view/View;F)V

    .line 346
    :cond_80
    :goto_80
    return v3

    .line 341
    :cond_81
    instance-of v0, p3, Landroid/support/design/widget/AppBarLayout;

    if-eqz v0, :cond_80

    .line 344
    check-cast p3, Landroid/support/design/widget/AppBarLayout;

    invoke-direct {p0, p1, p3, p2}, Landroid/support/design/widget/FloatingActionButton$Behavior;->a(Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/AppBarLayout;Landroid/support/design/widget/FloatingActionButton;)Z

    goto :goto_80

    :cond_8b
    move v0, v4

    goto :goto_70
.end method

.method private static b(Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/FloatingActionButton;)F
    .registers 13

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 407
    const/4 v4, 0x0

    .line 408
    invoke-virtual {p0, p1}, Landroid/support/design/widget/CoordinatorLayout;->a(Landroid/view/View;)Ljava/util/List;

    move-result-object v6

    .line 409
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v7

    move v5, v3

    :goto_c
    if-ge v5, v7, :cond_73

    .line 410
    invoke-interface {v6, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 411
    instance-of v1, v0, Landroid/support/design/widget/Snackbar$SnackbarLayout;

    if-eqz v1, :cond_74

    .line 8360
    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_71

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_71

    .line 8361
    iget-object v8, p0, Landroid/support/design/widget/CoordinatorLayout;->i:Landroid/graphics/Rect;

    .line 8362
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-eq v1, p0, :cond_6b

    move v1, v2

    :goto_2d
    invoke-virtual {p0, p1, v1, v8}, Landroid/support/design/widget/CoordinatorLayout;->a(Landroid/view/View;ZLandroid/graphics/Rect;)V

    .line 8363
    iget-object v9, p0, Landroid/support/design/widget/CoordinatorLayout;->j:Landroid/graphics/Rect;

    .line 8364
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-eq v1, p0, :cond_6d

    move v1, v2

    :goto_39
    invoke-virtual {p0, v0, v1, v9}, Landroid/support/design/widget/CoordinatorLayout;->a(Landroid/view/View;ZLandroid/graphics/Rect;)V

    .line 8366
    iget v1, v8, Landroid/graphics/Rect;->left:I

    iget v10, v9, Landroid/graphics/Rect;->right:I

    if-gt v1, v10, :cond_6f

    iget v1, v8, Landroid/graphics/Rect;->top:I

    iget v10, v9, Landroid/graphics/Rect;->bottom:I

    if-gt v1, v10, :cond_6f

    iget v1, v8, Landroid/graphics/Rect;->right:I

    iget v10, v9, Landroid/graphics/Rect;->left:I

    if-lt v1, v10, :cond_6f

    iget v1, v8, Landroid/graphics/Rect;->bottom:I

    iget v8, v9, Landroid/graphics/Rect;->top:I

    if-lt v1, v8, :cond_6f

    move v1, v2

    .line 411
    :goto_55
    if-eqz v1, :cond_74

    .line 412
    invoke-static {v0}, Landroid/support/v4/view/cx;->n(Landroid/view/View;)F

    move-result v1

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    int-to-float v0, v0

    sub-float v0, v1, v0

    invoke-static {v4, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 409
    :goto_66
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    move v4, v0

    goto :goto_c

    :cond_6b
    move v1, v3

    .line 8362
    goto :goto_2d

    :cond_6d
    move v1, v3

    .line 8364
    goto :goto_39

    :cond_6f
    move v1, v3

    .line 8366
    goto :goto_55

    :cond_71
    move v1, v3

    .line 8369
    goto :goto_55

    .line 417
    :cond_73
    return v4

    :cond_74
    move v0, v4

    goto :goto_66
.end method

.method private static c(Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/FloatingActionButton;)V
    .registers 9

    .prologue
    const/4 v2, 0x0

    .line 445
    invoke-static {p1}, Landroid/support/design/widget/FloatingActionButton;->a(Landroid/support/design/widget/FloatingActionButton;)Landroid/graphics/Rect;

    move-result-object v3

    .line 447
    if-eqz v3, :cond_3d

    invoke-virtual {v3}, Landroid/graphics/Rect;->centerX()I

    move-result v0

    if-lez v0, :cond_3d

    invoke-virtual {v3}, Landroid/graphics/Rect;->centerY()I

    move-result v0

    if-lez v0, :cond_3d

    .line 448
    invoke-virtual {p1}, Landroid/support/design/widget/FloatingActionButton;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/w;

    .line 453
    invoke-virtual {p1}, Landroid/support/design/widget/FloatingActionButton;->getRight()I

    move-result v1

    invoke-virtual {p0}, Landroid/support/design/widget/CoordinatorLayout;->getWidth()I

    move-result v4

    iget v5, v0, Landroid/support/design/widget/w;->rightMargin:I

    sub-int/2addr v4, v5

    if-lt v1, v4, :cond_3e

    .line 455
    iget v1, v3, Landroid/graphics/Rect;->right:I

    .line 460
    :goto_28
    invoke-virtual {p1}, Landroid/support/design/widget/FloatingActionButton;->getBottom()I

    move-result v4

    invoke-virtual {p0}, Landroid/support/design/widget/CoordinatorLayout;->getBottom()I

    move-result v5

    iget v6, v0, Landroid/support/design/widget/w;->bottomMargin:I

    sub-int/2addr v5, v6

    if-lt v4, v5, :cond_4a

    .line 462
    iget v2, v3, Landroid/graphics/Rect;->bottom:I

    .line 468
    :cond_37
    :goto_37
    invoke-virtual {p1, v2}, Landroid/support/design/widget/FloatingActionButton;->offsetTopAndBottom(I)V

    .line 469
    invoke-virtual {p1, v1}, Landroid/support/design/widget/FloatingActionButton;->offsetLeftAndRight(I)V

    .line 471
    :cond_3d
    return-void

    .line 456
    :cond_3e
    invoke-virtual {p1}, Landroid/support/design/widget/FloatingActionButton;->getLeft()I

    move-result v1

    iget v4, v0, Landroid/support/design/widget/w;->leftMargin:I

    if-gt v1, v4, :cond_56

    .line 458
    iget v1, v3, Landroid/graphics/Rect;->left:I

    neg-int v1, v1

    goto :goto_28

    .line 463
    :cond_4a
    invoke-virtual {p1}, Landroid/support/design/widget/FloatingActionButton;->getTop()I

    move-result v4

    iget v0, v0, Landroid/support/design/widget/w;->topMargin:I

    if-gt v4, v0, :cond_37

    .line 465
    iget v0, v3, Landroid/graphics/Rect;->top:I

    neg-int v2, v0

    goto :goto_37

    :cond_56
    move v1, v2

    goto :goto_28
.end method

.method private static c(Landroid/view/View;)Z
    .registers 2

    .prologue
    .line 333
    sget-boolean v0, Landroid/support/design/widget/FloatingActionButton$Behavior;->a:Z

    if-eqz v0, :cond_a

    instance-of v0, p0, Landroid/support/design/widget/Snackbar$SnackbarLayout;

    if-eqz v0, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method


# virtual methods
.method public final synthetic a(Landroid/view/View;Landroid/view/View;)V
    .registers 6

    .prologue
    const/4 v1, 0x0

    .line 322
    check-cast p1, Landroid/support/design/widget/FloatingActionButton;

    .line 10352
    instance-of v0, p2, Landroid/support/design/widget/Snackbar$SnackbarLayout;

    if-eqz v0, :cond_45

    .line 10355
    invoke-static {p1}, Landroid/support/v4/view/cx;->n(Landroid/view/View;)F

    move-result v0

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_45

    .line 10356
    invoke-static {p1}, Landroid/support/v4/view/cx;->p(Landroid/view/View;)Landroid/support/v4/view/fk;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/support/v4/view/fk;->b(F)Landroid/support/v4/view/fk;

    move-result-object v1

    .line 10986
    iget-object v0, v1, Landroid/support/v4/view/fk;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_26

    .line 10987
    sget-object v2, Landroid/support/v4/view/fk;->c:Landroid/support/v4/view/fu;

    invoke-interface {v2, v1, v0}, Landroid/support/v4/view/fu;->a(Landroid/support/v4/view/fk;Landroid/view/View;)V

    .line 11020
    :cond_26
    iget-object v0, v1, Landroid/support/v4/view/fk;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_35

    .line 11021
    sget-object v2, Landroid/support/v4/view/fk;->c:Landroid/support/v4/view/fu;

    invoke-interface {v2, v1, v0}, Landroid/support/v4/view/fu;->b(Landroid/support/v4/view/fk;Landroid/view/View;)V

    .line 10356
    :cond_35
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {v1, v0}, Landroid/support/v4/view/fk;->a(F)Landroid/support/v4/view/fk;

    move-result-object v0

    sget-object v1, Landroid/support/design/widget/a;->b:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/fk;->a(Landroid/view/animation/Interpolator;)Landroid/support/v4/view/fk;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/view/fk;->a(Landroid/support/v4/view/gd;)Landroid/support/v4/view/fk;

    .line 322
    :cond_45
    return-void
.end method

.method public final synthetic a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;I)Z
    .registers 11

    .prologue
    const/4 v2, 0x0

    .line 322
    check-cast p2, Landroid/support/design/widget/FloatingActionButton;

    .line 9424
    invoke-virtual {p1, p2}, Landroid/support/design/widget/CoordinatorLayout;->a(Landroid/view/View;)Ljava/util/List;

    move-result-object v3

    .line 9425
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    move v1, v2

    :goto_c
    if-ge v1, v4, :cond_24

    .line 9426
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 9427
    instance-of v5, v0, Landroid/support/design/widget/AppBarLayout;

    if-eqz v5, :cond_20

    check-cast v0, Landroid/support/design/widget/AppBarLayout;

    invoke-direct {p0, p1, v0, p2}, Landroid/support/design/widget/FloatingActionButton$Behavior;->a(Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/AppBarLayout;Landroid/support/design/widget/FloatingActionButton;)Z

    move-result v0

    if-nez v0, :cond_24

    .line 9425
    :cond_20
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_c

    .line 9433
    :cond_24
    invoke-virtual {p1, p2, p3}, Landroid/support/design/widget/CoordinatorLayout;->a(Landroid/view/View;I)V

    .line 9445
    invoke-static {p2}, Landroid/support/design/widget/FloatingActionButton;->a(Landroid/support/design/widget/FloatingActionButton;)Landroid/graphics/Rect;

    move-result-object v3

    .line 9447
    if-eqz v3, :cond_63

    invoke-virtual {v3}, Landroid/graphics/Rect;->centerX()I

    move-result v0

    if-lez v0, :cond_63

    invoke-virtual {v3}, Landroid/graphics/Rect;->centerY()I

    move-result v0

    if-lez v0, :cond_63

    .line 9448
    invoke-virtual {p2}, Landroid/support/design/widget/FloatingActionButton;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/w;

    .line 9453
    invoke-virtual {p2}, Landroid/support/design/widget/FloatingActionButton;->getRight()I

    move-result v1

    invoke-virtual {p1}, Landroid/support/design/widget/CoordinatorLayout;->getWidth()I

    move-result v4

    iget v5, v0, Landroid/support/design/widget/w;->rightMargin:I

    sub-int/2addr v4, v5

    if-lt v1, v4, :cond_65

    .line 9455
    iget v1, v3, Landroid/graphics/Rect;->right:I

    .line 9460
    :goto_4e
    invoke-virtual {p2}, Landroid/support/design/widget/FloatingActionButton;->getBottom()I

    move-result v4

    invoke-virtual {p1}, Landroid/support/design/widget/CoordinatorLayout;->getBottom()I

    move-result v5

    iget v6, v0, Landroid/support/design/widget/w;->bottomMargin:I

    sub-int/2addr v5, v6

    if-lt v4, v5, :cond_71

    .line 9462
    iget v2, v3, Landroid/graphics/Rect;->bottom:I

    .line 9468
    :cond_5d
    :goto_5d
    invoke-virtual {p2, v2}, Landroid/support/design/widget/FloatingActionButton;->offsetTopAndBottom(I)V

    .line 9469
    invoke-virtual {p2, v1}, Landroid/support/design/widget/FloatingActionButton;->offsetLeftAndRight(I)V

    .line 9436
    :cond_63
    const/4 v0, 0x1

    .line 322
    return v0

    .line 9456
    :cond_65
    invoke-virtual {p2}, Landroid/support/design/widget/FloatingActionButton;->getLeft()I

    move-result v1

    iget v4, v0, Landroid/support/design/widget/w;->leftMargin:I

    if-gt v1, v4, :cond_7d

    .line 9458
    iget v1, v3, Landroid/graphics/Rect;->left:I

    neg-int v1, v1

    goto :goto_4e

    .line 9463
    :cond_71
    invoke-virtual {p2}, Landroid/support/design/widget/FloatingActionButton;->getTop()I

    move-result v4

    iget v0, v0, Landroid/support/design/widget/w;->topMargin:I

    if-gt v4, v0, :cond_5d

    .line 9465
    iget v0, v3, Landroid/graphics/Rect;->top:I

    neg-int v2, v0

    goto :goto_5d

    :cond_7d
    move v1, v2

    goto :goto_4e
.end method

.method public final synthetic a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/view/View;)Z
    .registers 15

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 322
    check-cast p2, Landroid/support/design/widget/FloatingActionButton;

    .line 11339
    instance-of v0, p3, Landroid/support/design/widget/Snackbar$SnackbarLayout;

    if-eqz v0, :cond_83

    .line 11397
    invoke-virtual {p2}, Landroid/support/design/widget/FloatingActionButton;->getVisibility()I

    move-result v0

    if-nez v0, :cond_82

    .line 11407
    const/4 v4, 0x0

    .line 11408
    invoke-virtual {p1, p2}, Landroid/support/design/widget/CoordinatorLayout;->a(Landroid/view/View;)Ljava/util/List;

    move-result-object v6

    .line 11409
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v7

    move v5, v3

    :goto_18
    if-ge v5, v7, :cond_7f

    .line 11410
    invoke-interface {v6, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 11411
    instance-of v1, v0, Landroid/support/design/widget/Snackbar$SnackbarLayout;

    if-eqz v1, :cond_8d

    .line 12360
    invoke-virtual {p2}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_7d

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_7d

    .line 12361
    iget-object v8, p1, Landroid/support/design/widget/CoordinatorLayout;->i:Landroid/graphics/Rect;

    .line 12362
    invoke-virtual {p2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-eq v1, p1, :cond_77

    move v1, v2

    :goto_39
    invoke-virtual {p1, p2, v1, v8}, Landroid/support/design/widget/CoordinatorLayout;->a(Landroid/view/View;ZLandroid/graphics/Rect;)V

    .line 12363
    iget-object v9, p1, Landroid/support/design/widget/CoordinatorLayout;->j:Landroid/graphics/Rect;

    .line 12364
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-eq v1, p1, :cond_79

    move v1, v2

    :goto_45
    invoke-virtual {p1, v0, v1, v9}, Landroid/support/design/widget/CoordinatorLayout;->a(Landroid/view/View;ZLandroid/graphics/Rect;)V

    .line 12366
    iget v1, v8, Landroid/graphics/Rect;->left:I

    iget v10, v9, Landroid/graphics/Rect;->right:I

    if-gt v1, v10, :cond_7b

    iget v1, v8, Landroid/graphics/Rect;->top:I

    iget v10, v9, Landroid/graphics/Rect;->bottom:I

    if-gt v1, v10, :cond_7b

    iget v1, v8, Landroid/graphics/Rect;->right:I

    iget v10, v9, Landroid/graphics/Rect;->left:I

    if-lt v1, v10, :cond_7b

    iget v1, v8, Landroid/graphics/Rect;->bottom:I

    iget v8, v9, Landroid/graphics/Rect;->top:I

    if-lt v1, v8, :cond_7b

    move v1, v2

    .line 11411
    :goto_61
    if-eqz v1, :cond_8d

    .line 11412
    invoke-static {v0}, Landroid/support/v4/view/cx;->n(Landroid/view/View;)F

    move-result v1

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    int-to-float v0, v0

    sub-float v0, v1, v0

    invoke-static {v4, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 11409
    :goto_72
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    move v4, v0

    goto :goto_18

    :cond_77
    move v1, v3

    .line 12362
    goto :goto_39

    :cond_79
    move v1, v3

    .line 12364
    goto :goto_45

    :cond_7b
    move v1, v3

    .line 12366
    goto :goto_61

    :cond_7d
    move v1, v3

    .line 12369
    goto :goto_61

    .line 11402
    :cond_7f
    invoke-static {p2, v4}, Landroid/support/v4/view/cx;->b(Landroid/view/View;F)V

    .line 322
    :cond_82
    :goto_82
    return v3

    .line 11341
    :cond_83
    instance-of v0, p3, Landroid/support/design/widget/AppBarLayout;

    if-eqz v0, :cond_82

    .line 11344
    check-cast p3, Landroid/support/design/widget/AppBarLayout;

    invoke-direct {p0, p1, p3, p2}, Landroid/support/design/widget/FloatingActionButton$Behavior;->a(Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/AppBarLayout;Landroid/support/design/widget/FloatingActionButton;)Z

    goto :goto_82

    :cond_8d
    move v0, v4

    goto :goto_72
.end method

.method public final bridge synthetic b(Landroid/view/View;)Z
    .registers 3

    .prologue
    .line 13333
    sget-boolean v0, Landroid/support/design/widget/FloatingActionButton$Behavior;->a:Z

    if-eqz v0, :cond_a

    instance-of v0, p1, Landroid/support/design/widget/Snackbar$SnackbarLayout;

    if-eqz v0, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    .line 322
    goto :goto_9
.end method
