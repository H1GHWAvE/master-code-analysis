.class final Landroid/support/design/widget/ay;
.super Landroid/support/v4/view/ge;
.source "SourceFile"


# instance fields
.field final synthetic a:Landroid/support/design/widget/Snackbar;


# direct methods
.method constructor <init>(Landroid/support/design/widget/Snackbar;)V
    .registers 2

    .prologue
    .line 481
    iput-object p1, p0, Landroid/support/design/widget/ay;->a:Landroid/support/design/widget/Snackbar;

    invoke-direct {p0}, Landroid/support/v4/view/ge;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;)V
    .registers 10

    .prologue
    const-wide/16 v6, 0xb4

    const-wide/16 v4, 0x46

    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    .line 484
    iget-object v0, p0, Landroid/support/design/widget/ay;->a:Landroid/support/design/widget/Snackbar;

    invoke-static {v0}, Landroid/support/design/widget/Snackbar;->c(Landroid/support/design/widget/Snackbar;)Landroid/support/design/widget/Snackbar$SnackbarLayout;

    move-result-object v0

    .line 1682
    iget-object v1, v0, Landroid/support/design/widget/Snackbar$SnackbarLayout;->a:Landroid/widget/TextView;

    invoke-static {v1, v2}, Landroid/support/v4/view/cx;->c(Landroid/view/View;F)V

    .line 1683
    iget-object v1, v0, Landroid/support/design/widget/Snackbar$SnackbarLayout;->a:Landroid/widget/TextView;

    invoke-static {v1}, Landroid/support/v4/view/cx;->p(Landroid/view/View;)Landroid/support/v4/view/fk;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/support/v4/view/fk;->a(F)Landroid/support/v4/view/fk;

    move-result-object v1

    invoke-virtual {v1, v6, v7}, Landroid/support/v4/view/fk;->a(J)Landroid/support/v4/view/fk;

    move-result-object v1

    invoke-virtual {v1, v4, v5}, Landroid/support/v4/view/fk;->b(J)Landroid/support/v4/view/fk;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/view/fk;->b()V

    .line 1686
    iget-object v1, v0, Landroid/support/design/widget/Snackbar$SnackbarLayout;->b:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->getVisibility()I

    move-result v1

    if-nez v1, :cond_49

    .line 1687
    iget-object v1, v0, Landroid/support/design/widget/Snackbar$SnackbarLayout;->b:Landroid/widget/Button;

    invoke-static {v1, v2}, Landroid/support/v4/view/cx;->c(Landroid/view/View;F)V

    .line 1688
    iget-object v0, v0, Landroid/support/design/widget/Snackbar$SnackbarLayout;->b:Landroid/widget/Button;

    invoke-static {v0}, Landroid/support/v4/view/cx;->p(Landroid/view/View;)Landroid/support/v4/view/fk;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/support/v4/view/fk;->a(F)Landroid/support/v4/view/fk;

    move-result-object v0

    invoke-virtual {v0, v6, v7}, Landroid/support/v4/view/fk;->a(J)Landroid/support/v4/view/fk;

    move-result-object v0

    invoke-virtual {v0, v4, v5}, Landroid/support/v4/view/fk;->b(J)Landroid/support/v4/view/fk;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/view/fk;->b()V

    .line 486
    :cond_49
    return-void
.end method

.method public final b(Landroid/view/View;)V
    .registers 4

    .prologue
    .line 490
    iget-object v0, p0, Landroid/support/design/widget/ay;->a:Landroid/support/design/widget/Snackbar;

    invoke-static {v0}, Landroid/support/design/widget/Snackbar;->d(Landroid/support/design/widget/Snackbar;)Landroid/support/design/widget/bd;

    move-result-object v0

    if-eqz v0, :cond_d

    .line 491
    iget-object v0, p0, Landroid/support/design/widget/ay;->a:Landroid/support/design/widget/Snackbar;

    invoke-static {v0}, Landroid/support/design/widget/Snackbar;->d(Landroid/support/design/widget/Snackbar;)Landroid/support/design/widget/bd;

    .line 493
    :cond_d
    invoke-static {}, Landroid/support/design/widget/bh;->a()Landroid/support/design/widget/bh;

    move-result-object v0

    iget-object v1, p0, Landroid/support/design/widget/ay;->a:Landroid/support/design/widget/Snackbar;

    invoke-static {v1}, Landroid/support/design/widget/Snackbar;->a(Landroid/support/design/widget/Snackbar;)Landroid/support/design/widget/bj;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/design/widget/bh;->a(Landroid/support/design/widget/bj;)V

    .line 494
    return-void
.end method
