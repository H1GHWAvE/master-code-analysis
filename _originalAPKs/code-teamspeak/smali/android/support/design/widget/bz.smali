.class public final Landroid/support/design/widget/bz;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:I = -0x1


# instance fields
.field b:Landroid/graphics/drawable/Drawable;

.field c:Ljava/lang/CharSequence;

.field d:Ljava/lang/CharSequence;

.field e:I

.field f:Landroid/view/View;

.field final g:Landroid/support/design/widget/br;

.field private h:Ljava/lang/Object;


# direct methods
.method constructor <init>(Landroid/support/design/widget/br;)V
    .registers 3

    .prologue
    .line 908
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 903
    const/4 v0, -0x1

    iput v0, p0, Landroid/support/design/widget/bz;->e:I

    .line 909
    iput-object p1, p0, Landroid/support/design/widget/bz;->g:Landroid/support/design/widget/br;

    .line 910
    return-void
.end method

.method private static synthetic a(Landroid/support/design/widget/bz;)Landroid/support/design/widget/br;
    .registers 2

    .prologue
    .line 890
    iget-object v0, p0, Landroid/support/design/widget/bz;->g:Landroid/support/design/widget/br;

    return-object v0
.end method

.method private a(I)Landroid/support/design/widget/bz;
    .registers 4
    .param p1    # I
        .annotation build Landroid/support/a/v;
        .end annotation
    .end param
    .annotation build Landroid/support/a/y;
    .end annotation

    .prologue
    .line 981
    iget-object v0, p0, Landroid/support/design/widget/bz;->g:Landroid/support/design/widget/br;

    invoke-virtual {v0}, Landroid/support/design/widget/br;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 1959
    iput-object v0, p0, Landroid/support/design/widget/bz;->f:Landroid/view/View;

    .line 1960
    iget v0, p0, Landroid/support/design/widget/bz;->e:I

    if-ltz v0, :cond_1c

    .line 1961
    iget-object v0, p0, Landroid/support/design/widget/bz;->g:Landroid/support/design/widget/br;

    iget v1, p0, Landroid/support/design/widget/bz;->e:I

    invoke-static {v0, v1}, Landroid/support/design/widget/br;->a(Landroid/support/design/widget/br;I)V

    .line 981
    :cond_1c
    return-object p0
.end method

.method private a(Landroid/graphics/drawable/Drawable;)Landroid/support/design/widget/bz;
    .registers 4
    .param p1    # Landroid/graphics/drawable/Drawable;
        .annotation build Landroid/support/a/z;
        .end annotation
    .end param
    .annotation build Landroid/support/a/y;
    .end annotation

    .prologue
    .line 1027
    iput-object p1, p0, Landroid/support/design/widget/bz;->b:Landroid/graphics/drawable/Drawable;

    .line 1028
    iget v0, p0, Landroid/support/design/widget/bz;->e:I

    if-ltz v0, :cond_d

    .line 1029
    iget-object v0, p0, Landroid/support/design/widget/bz;->g:Landroid/support/design/widget/br;

    iget v1, p0, Landroid/support/design/widget/bz;->e:I

    invoke-static {v0, v1}, Landroid/support/design/widget/br;->a(Landroid/support/design/widget/br;I)V

    .line 1031
    :cond_d
    return-object p0
.end method

.method private a(Landroid/view/View;)Landroid/support/design/widget/bz;
    .registers 4
    .param p1    # Landroid/view/View;
        .annotation build Landroid/support/a/z;
        .end annotation
    .end param
    .annotation build Landroid/support/a/y;
    .end annotation

    .prologue
    .line 959
    iput-object p1, p0, Landroid/support/design/widget/bz;->f:Landroid/view/View;

    .line 960
    iget v0, p0, Landroid/support/design/widget/bz;->e:I

    if-ltz v0, :cond_d

    .line 961
    iget-object v0, p0, Landroid/support/design/widget/bz;->g:Landroid/support/design/widget/br;

    iget v1, p0, Landroid/support/design/widget/bz;->e:I

    invoke-static {v0, v1}, Landroid/support/design/widget/br;->a(Landroid/support/design/widget/br;I)V

    .line 963
    :cond_d
    return-object p0
.end method

.method private a(Ljava/lang/Object;)Landroid/support/design/widget/bz;
    .registers 2
    .param p1    # Ljava/lang/Object;
        .annotation build Landroid/support/a/z;
        .end annotation
    .end param
    .annotation build Landroid/support/a/y;
    .end annotation

    .prologue
    .line 928
    iput-object p1, p0, Landroid/support/design/widget/bz;->h:Ljava/lang/Object;

    .line 929
    return-object p0
.end method

.method private b(Ljava/lang/CharSequence;)Landroid/support/design/widget/bz;
    .registers 4
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Landroid/support/a/z;
        .end annotation
    .end param
    .annotation build Landroid/support/a/y;
    .end annotation

    .prologue
    .line 1112
    iput-object p1, p0, Landroid/support/design/widget/bz;->d:Ljava/lang/CharSequence;

    .line 1113
    iget v0, p0, Landroid/support/design/widget/bz;->e:I

    if-ltz v0, :cond_d

    .line 1114
    iget-object v0, p0, Landroid/support/design/widget/bz;->g:Landroid/support/design/widget/br;

    iget v1, p0, Landroid/support/design/widget/bz;->e:I

    invoke-static {v0, v1}, Landroid/support/design/widget/br;->a(Landroid/support/design/widget/br;I)V

    .line 1116
    :cond_d
    return-object p0
.end method

.method private b()Ljava/lang/Object;
    .registers 2
    .annotation build Landroid/support/a/z;
    .end annotation

    .prologue
    .line 917
    iget-object v0, p0, Landroid/support/design/widget/bz;->h:Ljava/lang/Object;

    return-object v0
.end method

.method private b(I)V
    .registers 2

    .prologue
    .line 1006
    iput p1, p0, Landroid/support/design/widget/bz;->e:I

    .line 1007
    return-void
.end method

.method private c(I)Landroid/support/design/widget/bz;
    .registers 4
    .param p1    # I
        .annotation build Landroid/support/a/m;
        .end annotation
    .end param
    .annotation build Landroid/support/a/y;
    .end annotation

    .prologue
    .line 1042
    iget-object v0, p0, Landroid/support/design/widget/bz;->g:Landroid/support/design/widget/br;

    invoke-virtual {v0}, Landroid/support/design/widget/br;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Landroid/support/v7/internal/widget/av;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2027
    iput-object v0, p0, Landroid/support/design/widget/bz;->b:Landroid/graphics/drawable/Drawable;

    .line 2028
    iget v0, p0, Landroid/support/design/widget/bz;->e:I

    if-ltz v0, :cond_17

    .line 2029
    iget-object v0, p0, Landroid/support/design/widget/bz;->g:Landroid/support/design/widget/br;

    iget v1, p0, Landroid/support/design/widget/bz;->e:I

    invoke-static {v0, v1}, Landroid/support/design/widget/br;->a(Landroid/support/design/widget/br;I)V

    .line 1042
    :cond_17
    return-object p0
.end method

.method private c()Landroid/view/View;
    .registers 2
    .annotation build Landroid/support/a/z;
    .end annotation

    .prologue
    .line 941
    iget-object v0, p0, Landroid/support/design/widget/bz;->f:Landroid/view/View;

    return-object v0
.end method

.method private d()Landroid/graphics/drawable/Drawable;
    .registers 2
    .annotation build Landroid/support/a/z;
    .end annotation

    .prologue
    .line 992
    iget-object v0, p0, Landroid/support/design/widget/bz;->b:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method private d(I)Landroid/support/design/widget/bz;
    .registers 3
    .param p1    # I
        .annotation build Landroid/support/a/ah;
        .end annotation
    .end param
    .annotation build Landroid/support/a/y;
    .end annotation

    .prologue
    .line 1070
    iget-object v0, p0, Landroid/support/design/widget/bz;->g:Landroid/support/design/widget/br;

    invoke-virtual {v0}, Landroid/support/design/widget/br;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/support/design/widget/bz;->a(Ljava/lang/CharSequence;)Landroid/support/design/widget/bz;

    move-result-object v0

    return-object v0
.end method

.method private e()I
    .registers 2

    .prologue
    .line 1002
    iget v0, p0, Landroid/support/design/widget/bz;->e:I

    return v0
.end method

.method private e(I)Landroid/support/design/widget/bz;
    .registers 4
    .param p1    # I
        .annotation build Landroid/support/a/ah;
        .end annotation
    .end param
    .annotation build Landroid/support/a/y;
    .end annotation

    .prologue
    .line 1098
    iget-object v0, p0, Landroid/support/design/widget/bz;->g:Landroid/support/design/widget/br;

    invoke-virtual {v0}, Landroid/support/design/widget/br;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    .line 3112
    iput-object v0, p0, Landroid/support/design/widget/bz;->d:Ljava/lang/CharSequence;

    .line 3113
    iget v0, p0, Landroid/support/design/widget/bz;->e:I

    if-ltz v0, :cond_17

    .line 3114
    iget-object v0, p0, Landroid/support/design/widget/bz;->g:Landroid/support/design/widget/br;

    iget v1, p0, Landroid/support/design/widget/bz;->e:I

    invoke-static {v0, v1}, Landroid/support/design/widget/br;->a(Landroid/support/design/widget/br;I)V

    .line 1098
    :cond_17
    return-object p0
.end method

.method private f()Ljava/lang/CharSequence;
    .registers 2
    .annotation build Landroid/support/a/z;
    .end annotation

    .prologue
    .line 1016
    iget-object v0, p0, Landroid/support/design/widget/bz;->c:Ljava/lang/CharSequence;

    return-object v0
.end method

.method private g()Z
    .registers 3

    .prologue
    .line 1084
    iget-object v0, p0, Landroid/support/design/widget/bz;->g:Landroid/support/design/widget/br;

    invoke-virtual {v0}, Landroid/support/design/widget/br;->getSelectedTabPosition()I

    move-result v0

    iget v1, p0, Landroid/support/design/widget/bz;->e:I

    if-ne v0, v1, :cond_c

    const/4 v0, 0x1

    :goto_b
    return v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method

.method private h()Ljava/lang/CharSequence;
    .registers 2
    .annotation build Landroid/support/a/z;
    .end annotation

    .prologue
    .line 1128
    iget-object v0, p0, Landroid/support/design/widget/bz;->d:Ljava/lang/CharSequence;

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/CharSequence;)Landroid/support/design/widget/bz;
    .registers 4
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Landroid/support/a/z;
        .end annotation
    .end param
    .annotation build Landroid/support/a/y;
    .end annotation

    .prologue
    .line 1054
    iput-object p1, p0, Landroid/support/design/widget/bz;->c:Ljava/lang/CharSequence;

    .line 1055
    iget v0, p0, Landroid/support/design/widget/bz;->e:I

    if-ltz v0, :cond_d

    .line 1056
    iget-object v0, p0, Landroid/support/design/widget/bz;->g:Landroid/support/design/widget/br;

    iget v1, p0, Landroid/support/design/widget/bz;->e:I

    invoke-static {v0, v1}, Landroid/support/design/widget/br;->a(Landroid/support/design/widget/br;I)V

    .line 1058
    :cond_d
    return-object p0
.end method

.method public final a()V
    .registers 3

    .prologue
    .line 1077
    iget-object v0, p0, Landroid/support/design/widget/bz;->g:Landroid/support/design/widget/br;

    .line 2809
    const/4 v1, 0x1

    invoke-virtual {v0, p0, v1}, Landroid/support/design/widget/br;->a(Landroid/support/design/widget/bz;Z)V

    .line 1078
    return-void
.end method
