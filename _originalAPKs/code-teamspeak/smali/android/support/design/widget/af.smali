.class abstract Landroid/support/design/widget/af;
.super Landroid/view/animation/Animation;
.source "SourceFile"


# instance fields
.field final synthetic a:Landroid/support/design/widget/ad;

.field private b:F

.field private c:F


# direct methods
.method private constructor <init>(Landroid/support/design/widget/ad;)V
    .registers 2

    .prologue
    .line 212
    iput-object p1, p0, Landroid/support/design/widget/af;->a:Landroid/support/design/widget/ad;

    invoke-direct {p0}, Landroid/view/animation/Animation;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Landroid/support/design/widget/ad;B)V
    .registers 3

    .prologue
    .line 212
    invoke-direct {p0, p1}, Landroid/support/design/widget/af;-><init>(Landroid/support/design/widget/ad;)V

    return-void
.end method


# virtual methods
.method protected abstract a()F
.end method

.method protected applyTransformation(FLandroid/view/animation/Transformation;)V
    .registers 6

    .prologue
    .line 226
    iget-object v0, p0, Landroid/support/design/widget/af;->a:Landroid/support/design/widget/ad;

    iget-object v0, v0, Landroid/support/design/widget/ad;->a:Landroid/support/design/widget/ar;

    iget v1, p0, Landroid/support/design/widget/af;->b:F

    iget v2, p0, Landroid/support/design/widget/af;->c:F

    mul-float/2addr v2, p1

    add-float/2addr v1, v2

    .line 2324
    iget v2, v0, Landroid/support/design/widget/ar;->l:F

    invoke-virtual {v0, v1, v2}, Landroid/support/design/widget/ar;->a(FF)V

    .line 227
    return-void
.end method

.method public reset()V
    .registers 3

    .prologue
    .line 218
    invoke-super {p0}, Landroid/view/animation/Animation;->reset()V

    .line 220
    iget-object v0, p0, Landroid/support/design/widget/af;->a:Landroid/support/design/widget/ad;

    iget-object v0, v0, Landroid/support/design/widget/ad;->a:Landroid/support/design/widget/ar;

    .line 1332
    iget v0, v0, Landroid/support/design/widget/ar;->n:F

    .line 220
    iput v0, p0, Landroid/support/design/widget/af;->b:F

    .line 221
    invoke-virtual {p0}, Landroid/support/design/widget/af;->a()F

    move-result v0

    iget v1, p0, Landroid/support/design/widget/af;->b:F

    sub-float/2addr v0, v1

    iput v0, p0, Landroid/support/design/widget/af;->c:F

    .line 222
    return-void
.end method
