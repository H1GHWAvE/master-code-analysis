.class final Landroid/support/design/widget/v;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/ViewGroup$OnHierarchyChangeListener;


# instance fields
.field final synthetic a:Landroid/support/design/widget/CoordinatorLayout;


# direct methods
.method constructor <init>(Landroid/support/design/widget/CoordinatorLayout;)V
    .registers 2

    .prologue
    .line 2523
    iput-object p1, p0, Landroid/support/design/widget/v;->a:Landroid/support/design/widget/CoordinatorLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onChildViewAdded(Landroid/view/View;Landroid/view/View;)V
    .registers 4

    .prologue
    .line 2526
    iget-object v0, p0, Landroid/support/design/widget/v;->a:Landroid/support/design/widget/CoordinatorLayout;

    invoke-static {v0}, Landroid/support/design/widget/CoordinatorLayout;->a(Landroid/support/design/widget/CoordinatorLayout;)Landroid/view/ViewGroup$OnHierarchyChangeListener;

    move-result-object v0

    if-eqz v0, :cond_11

    .line 2527
    iget-object v0, p0, Landroid/support/design/widget/v;->a:Landroid/support/design/widget/CoordinatorLayout;

    invoke-static {v0}, Landroid/support/design/widget/CoordinatorLayout;->a(Landroid/support/design/widget/CoordinatorLayout;)Landroid/view/ViewGroup$OnHierarchyChangeListener;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Landroid/view/ViewGroup$OnHierarchyChangeListener;->onChildViewAdded(Landroid/view/View;Landroid/view/View;)V

    .line 2529
    :cond_11
    return-void
.end method

.method public final onChildViewRemoved(Landroid/view/View;Landroid/view/View;)V
    .registers 9

    .prologue
    .line 2533
    iget-object v2, p0, Landroid/support/design/widget/v;->a:Landroid/support/design/widget/CoordinatorLayout;

    .line 3147
    invoke-virtual {v2}, Landroid/support/design/widget/CoordinatorLayout;->getChildCount()I

    move-result v3

    .line 3148
    const/4 v0, 0x0

    move v1, v0

    :goto_8
    if-ge v1, v3, :cond_25

    .line 3149
    invoke-virtual {v2, v1}, Landroid/support/design/widget/CoordinatorLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 3150
    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/w;

    .line 3281
    iget-object v0, v0, Landroid/support/design/widget/w;->a:Landroid/support/design/widget/t;

    .line 3153
    if-eqz v0, :cond_21

    invoke-virtual {v0, p2}, Landroid/support/design/widget/t;->b(Landroid/view/View;)Z

    move-result v5

    if-eqz v5, :cond_21

    .line 3154
    invoke-virtual {v0, v4, p2}, Landroid/support/design/widget/t;->a(Landroid/view/View;Landroid/view/View;)V

    .line 3148
    :cond_21
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_8

    .line 2535
    :cond_25
    iget-object v0, p0, Landroid/support/design/widget/v;->a:Landroid/support/design/widget/CoordinatorLayout;

    invoke-static {v0}, Landroid/support/design/widget/CoordinatorLayout;->a(Landroid/support/design/widget/CoordinatorLayout;)Landroid/view/ViewGroup$OnHierarchyChangeListener;

    move-result-object v0

    if-eqz v0, :cond_36

    .line 2536
    iget-object v0, p0, Landroid/support/design/widget/v;->a:Landroid/support/design/widget/CoordinatorLayout;

    invoke-static {v0}, Landroid/support/design/widget/CoordinatorLayout;->a(Landroid/support/design/widget/CoordinatorLayout;)Landroid/view/ViewGroup$OnHierarchyChangeListener;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Landroid/view/ViewGroup$OnHierarchyChangeListener;->onChildViewRemoved(Landroid/view/View;Landroid/view/View;)V

    .line 2538
    :cond_36
    return-void
.end method
