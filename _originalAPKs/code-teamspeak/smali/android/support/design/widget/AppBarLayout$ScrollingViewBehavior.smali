.class public Landroid/support/design/widget/AppBarLayout$ScrollingViewBehavior;
.super Landroid/support/design/widget/df;
.source "SourceFile"


# instance fields
.field private a:I


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 1183
    invoke-direct {p0}, Landroid/support/design/widget/df;-><init>()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 6

    .prologue
    .line 1186
    invoke-direct {p0, p1, p2}, Landroid/support/design/widget/df;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1188
    sget-object v0, Landroid/support/design/n;->ScrollingViewBehavior_Params:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1190
    sget v1, Landroid/support/design/n;->ScrollingViewBehavior_Params_behavior_overlapTop:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Landroid/support/design/widget/AppBarLayout$ScrollingViewBehavior;->a:I

    .line 1192
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1193
    return-void
.end method

.method private a()I
    .registers 2

    .prologue
    .line 1287
    iget v0, p0, Landroid/support/design/widget/AppBarLayout$ScrollingViewBehavior;->a:I

    return v0
.end method

.method private static a(Ljava/util/List;)Landroid/support/design/widget/AppBarLayout;
    .registers 5

    .prologue
    .line 1291
    const/4 v0, 0x0

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v2

    move v1, v0

    :goto_6
    if-ge v1, v2, :cond_19

    .line 1292
    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1293
    instance-of v3, v0, Landroid/support/design/widget/AppBarLayout;

    if-eqz v3, :cond_15

    .line 1294
    check-cast v0, Landroid/support/design/widget/AppBarLayout;

    .line 1297
    :goto_14
    return-object v0

    .line 1291
    :cond_15
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_6

    .line 1297
    :cond_19
    const/4 v0, 0x0

    goto :goto_14
.end method

.method private c(I)V
    .registers 2

    .prologue
    .line 1280
    iput p1, p0, Landroid/support/design/widget/AppBarLayout$ScrollingViewBehavior;->a:I

    .line 1281
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(I)Z
    .registers 3

    .prologue
    .line 1180
    invoke-super {p0, p1}, Landroid/support/design/widget/df;->a(I)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;I)Z
    .registers 5

    .prologue
    .line 1180
    invoke-super {p0, p1, p2, p3}, Landroid/support/design/widget/df;->a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;I)Z

    move-result v0

    return v0
.end method

.method public final a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;III)Z
    .registers 15

    .prologue
    const/4 v2, 0x1

    const/4 v8, -0x1

    const/4 v1, 0x0

    .line 1205
    invoke-virtual {p2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v4, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1206
    if-eq v4, v8, :cond_e

    const/4 v0, -0x2

    if-ne v4, v0, :cond_6a

    .line 1211
    :cond_e
    invoke-virtual {p1, p2}, Landroid/support/design/widget/CoordinatorLayout;->a(Landroid/view/View;)Ljava/util/List;

    move-result-object v5

    .line 1212
    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1a

    move v0, v1

    .line 1244
    :goto_19
    return v0

    .line 2291
    :cond_1a
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v6

    move v3, v1

    :goto_1f
    if-ge v3, v6, :cond_64

    .line 2292
    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 2293
    instance-of v7, v0, Landroid/support/design/widget/AppBarLayout;

    if-eqz v7, :cond_60

    .line 2294
    check-cast v0, Landroid/support/design/widget/AppBarLayout;

    move-object v3, v0

    .line 1218
    :goto_2e
    if-eqz v3, :cond_6a

    invoke-static {v3}, Landroid/support/v4/view/cx;->B(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_6a

    .line 1219
    invoke-static {v3}, Landroid/support/v4/view/cx;->u(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_3f

    .line 1222
    invoke-static {p2, v2}, Landroid/support/v4/view/cx;->a(Landroid/view/View;Z)V

    .line 1225
    :cond_3f
    invoke-static {p5}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 1226
    if-nez v0, :cond_49

    .line 1228
    invoke-virtual {p1}, Landroid/support/design/widget/CoordinatorLayout;->getHeight()I

    move-result v0

    .line 1230
    :cond_49
    invoke-virtual {v3}, Landroid/support/design/widget/AppBarLayout;->getMeasuredHeight()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {v3}, Landroid/support/design/widget/AppBarLayout;->getTotalScrollRange()I

    move-result v1

    add-int/2addr v1, v0

    .line 1232
    if-ne v4, v8, :cond_67

    const/high16 v0, 0x40000000    # 2.0f

    :goto_57
    invoke-static {v1, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 1238
    invoke-virtual {p1, p2, p3, p4, v0}, Landroid/support/design/widget/CoordinatorLayout;->a(Landroid/view/View;III)V

    move v0, v2

    .line 1241
    goto :goto_19

    .line 2291
    :cond_60
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1f

    .line 2297
    :cond_64
    const/4 v0, 0x0

    move-object v3, v0

    goto :goto_2e

    .line 1232
    :cond_67
    const/high16 v0, -0x80000000

    goto :goto_57

    :cond_6a
    move v0, v1

    .line 1244
    goto :goto_19
.end method

.method public final a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/view/View;)Z
    .registers 8

    .prologue
    .line 1250
    invoke-virtual {p3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/w;

    .line 3281
    iget-object v0, v0, Landroid/support/design/widget/w;->a:Landroid/support/design/widget/t;

    .line 1252
    instance-of v1, v0, Landroid/support/design/widget/AppBarLayout$Behavior;

    if-eqz v1, :cond_3e

    .line 1255
    check-cast v0, Landroid/support/design/widget/AppBarLayout$Behavior;

    invoke-virtual {v0}, Landroid/support/design/widget/AppBarLayout$Behavior;->a()I

    move-result v0

    .line 1257
    invoke-virtual {p3}, Landroid/view/View;->getHeight()I

    move-result v1

    iget v2, p0, Landroid/support/design/widget/AppBarLayout$ScrollingViewBehavior;->a:I

    sub-int/2addr v1, v2

    .line 1258
    invoke-virtual {p1}, Landroid/support/design/widget/CoordinatorLayout;->getHeight()I

    move-result v2

    invoke-virtual {p2}, Landroid/view/View;->getHeight()I

    move-result v3

    sub-int/2addr v2, v3

    .line 1260
    iget v3, p0, Landroid/support/design/widget/AppBarLayout$ScrollingViewBehavior;->a:I

    if-eqz v3, :cond_40

    instance-of v3, p3, Landroid/support/design/widget/AppBarLayout;

    if-eqz v3, :cond_40

    .line 1264
    check-cast p3, Landroid/support/design/widget/AppBarLayout;

    invoke-virtual {p3}, Landroid/support/design/widget/AppBarLayout;->getTotalScrollRange()I

    move-result v3

    .line 1265
    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    int-to-float v0, v0

    int-to-float v3, v3

    div-float/2addr v0, v3

    invoke-static {v1, v2, v0}, Landroid/support/design/widget/a;->a(IIF)I

    move-result v0

    .line 4180
    invoke-super {p0, v0}, Landroid/support/design/widget/df;->b(I)Z

    .line 1271
    :cond_3e
    :goto_3e
    const/4 v0, 0x0

    return v0

    .line 1268
    :cond_40
    invoke-virtual {p3}, Landroid/view/View;->getHeight()I

    move-result v1

    iget v2, p0, Landroid/support/design/widget/AppBarLayout$ScrollingViewBehavior;->a:I

    sub-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 5180
    invoke-super {p0, v0}, Landroid/support/design/widget/df;->b(I)Z

    goto :goto_3e
.end method

.method public final bridge synthetic b()I
    .registers 2

    .prologue
    .line 1180
    invoke-super {p0}, Landroid/support/design/widget/df;->b()I

    move-result v0

    return v0
.end method

.method public final bridge synthetic b(I)Z
    .registers 3

    .prologue
    .line 1180
    invoke-super {p0, p1}, Landroid/support/design/widget/df;->b(I)Z

    move-result v0

    return v0
.end method

.method public final b(Landroid/view/View;)Z
    .registers 3

    .prologue
    .line 1198
    instance-of v0, p1, Landroid/support/design/widget/AppBarLayout;

    return v0
.end method

.method public final bridge synthetic c()I
    .registers 2

    .prologue
    .line 1180
    invoke-super {p0}, Landroid/support/design/widget/df;->c()I

    move-result v0

    return v0
.end method
