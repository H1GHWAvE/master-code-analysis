.class public final Landroid/support/design/widget/g;
.super Landroid/widget/LinearLayout$LayoutParams;
.source "SourceFile"


# static fields
.field public static final a:I = 0x1

.field public static final b:I = 0x2

.field public static final c:I = 0x4

.field public static final d:I = 0x8

.field static final e:I = 0x5


# instance fields
.field f:I

.field g:Landroid/view/animation/Interpolator;


# direct methods
.method public constructor <init>()V
    .registers 3

    .prologue
    .line 548
    const/4 v0, -0x1

    const/4 v1, -0x2

    invoke-direct {p0, v0, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 531
    const/4 v0, 0x1

    iput v0, p0, Landroid/support/design/widget/g;->f:I

    .line 549
    return-void
.end method

.method private constructor <init>(IIF)V
    .registers 5

    .prologue
    .line 552
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    .line 531
    const/4 v0, 0x1

    iput v0, p0, Landroid/support/design/widget/g;->f:I

    .line 553
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 6

    .prologue
    const/4 v2, 0x0

    .line 535
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 531
    const/4 v0, 0x1

    iput v0, p0, Landroid/support/design/widget/g;->f:I

    .line 536
    sget-object v0, Landroid/support/design/n;->AppBarLayout_LayoutParams:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 537
    sget v1, Landroid/support/design/n;->AppBarLayout_LayoutParams_layout_scrollFlags:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Landroid/support/design/widget/g;->f:I

    .line 538
    sget v1, Landroid/support/design/n;->AppBarLayout_LayoutParams_layout_scrollInterpolator:I

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v1

    if-eqz v1, :cond_29

    .line 539
    sget v1, Landroid/support/design/n;->AppBarLayout_LayoutParams_layout_scrollInterpolator:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 541
    invoke-static {p1, v1}, Landroid/view/animation/AnimationUtils;->loadInterpolator(Landroid/content/Context;I)Landroid/view/animation/Interpolator;

    move-result-object v1

    iput-object v1, p0, Landroid/support/design/widget/g;->g:Landroid/view/animation/Interpolator;

    .line 544
    :cond_29
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 545
    return-void
.end method

.method private constructor <init>(Landroid/support/design/widget/g;)V
    .registers 3

    .prologue
    .line 568
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(Landroid/widget/LinearLayout$LayoutParams;)V

    .line 531
    const/4 v0, 0x1

    iput v0, p0, Landroid/support/design/widget/g;->f:I

    .line 569
    iget v0, p1, Landroid/support/design/widget/g;->f:I

    iput v0, p0, Landroid/support/design/widget/g;->f:I

    .line 570
    iget-object v0, p1, Landroid/support/design/widget/g;->g:Landroid/view/animation/Interpolator;

    iput-object v0, p0, Landroid/support/design/widget/g;->g:Landroid/view/animation/Interpolator;

    .line 571
    return-void
.end method

.method public constructor <init>(Landroid/view/ViewGroup$LayoutParams;)V
    .registers 3

    .prologue
    .line 556
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 531
    const/4 v0, 0x1

    iput v0, p0, Landroid/support/design/widget/g;->f:I

    .line 557
    return-void
.end method

.method public constructor <init>(Landroid/view/ViewGroup$MarginLayoutParams;)V
    .registers 3

    .prologue
    .line 560
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    .line 531
    const/4 v0, 0x1

    iput v0, p0, Landroid/support/design/widget/g;->f:I

    .line 561
    return-void
.end method

.method public constructor <init>(Landroid/widget/LinearLayout$LayoutParams;)V
    .registers 3

    .prologue
    .line 564
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(Landroid/widget/LinearLayout$LayoutParams;)V

    .line 531
    const/4 v0, 0x1

    iput v0, p0, Landroid/support/design/widget/g;->f:I

    .line 565
    return-void
.end method

.method private a()I
    .registers 2

    .prologue
    .line 597
    iget v0, p0, Landroid/support/design/widget/g;->f:I

    return v0
.end method

.method private a(I)V
    .registers 2

    .prologue
    .line 585
    iput p1, p0, Landroid/support/design/widget/g;->f:I

    .line 586
    return-void
.end method

.method private a(Landroid/view/animation/Interpolator;)V
    .registers 2

    .prologue
    .line 610
    iput-object p1, p0, Landroid/support/design/widget/g;->g:Landroid/view/animation/Interpolator;

    .line 611
    return-void
.end method

.method private b()Landroid/view/animation/Interpolator;
    .registers 2

    .prologue
    .line 621
    iget-object v0, p0, Landroid/support/design/widget/g;->g:Landroid/view/animation/Interpolator;

    return-object v0
.end method
