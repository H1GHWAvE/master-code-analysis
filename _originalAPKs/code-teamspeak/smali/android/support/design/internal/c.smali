.class public final Landroid/support/design/internal/c;
.super Landroid/widget/BaseAdapter;
.source "SourceFile"


# static fields
.field private static final e:Ljava/lang/String; = "android:menu:checked"

.field private static final f:I = 0x0

.field private static final g:I = 0x1

.field private static final h:I = 0x2


# instance fields
.field final a:Ljava/util/ArrayList;

.field b:Landroid/support/v7/internal/view/menu/m;

.field c:Z

.field final synthetic d:Landroid/support/design/internal/b;

.field private i:Landroid/graphics/drawable/ColorDrawable;


# direct methods
.method constructor <init>(Landroid/support/design/internal/b;)V
    .registers 3

    .prologue
    .line 278
    iput-object p1, p0, Landroid/support/design/internal/c;->d:Landroid/support/design/internal/b;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 273
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/design/internal/c;->a:Ljava/util/ArrayList;

    .line 279
    invoke-virtual {p0}, Landroid/support/design/internal/c;->a()V

    .line 280
    return-void
.end method

.method private a(II)V
    .registers 6

    .prologue
    .line 446
    :goto_0
    if-ge p1, p2, :cond_28

    .line 447
    iget-object v0, p0, Landroid/support/design/internal/c;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/design/internal/d;

    .line 5539
    iget-object v0, v0, Landroid/support/design/internal/d;->a:Landroid/support/v7/internal/view/menu/m;

    .line 448
    invoke-interface {v0}, Landroid/view/MenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-nez v1, :cond_25

    .line 449
    iget-object v1, p0, Landroid/support/design/internal/c;->i:Landroid/graphics/drawable/ColorDrawable;

    if-nez v1, :cond_20

    .line 450
    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    const v2, 0x106000d

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    iput-object v1, p0, Landroid/support/design/internal/c;->i:Landroid/graphics/drawable/ColorDrawable;

    .line 452
    :cond_20
    iget-object v1, p0, Landroid/support/design/internal/c;->i:Landroid/graphics/drawable/ColorDrawable;

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    .line 446
    :cond_25
    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    .line 455
    :cond_28
    return-void
.end method

.method private a(Landroid/os/Bundle;)V
    .registers 7

    .prologue
    const/4 v4, 0x0

    .line 477
    const-string v0, "android:menu:checked"

    invoke-virtual {p1, v0, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 478
    if-eqz v1, :cond_30

    .line 479
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/design/internal/c;->c:Z

    .line 480
    iget-object v0, p0, Landroid/support/design/internal/c;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_12
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2b

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/design/internal/d;

    .line 6539
    iget-object v0, v0, Landroid/support/design/internal/d;->a:Landroid/support/v7/internal/view/menu/m;

    .line 482
    if-eqz v0, :cond_12

    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/m;->getItemId()I

    move-result v3

    if-ne v3, v1, :cond_12

    .line 483
    invoke-virtual {p0, v0}, Landroid/support/design/internal/c;->a(Landroid/support/v7/internal/view/menu/m;)V

    .line 487
    :cond_2b
    iput-boolean v4, p0, Landroid/support/design/internal/c;->c:Z

    .line 488
    invoke-virtual {p0}, Landroid/support/design/internal/c;->a()V

    .line 490
    :cond_30
    return-void
.end method

.method private a(Z)V
    .registers 2

    .prologue
    .line 493
    iput-boolean p1, p0, Landroid/support/design/internal/c;->c:Z

    .line 494
    return-void
.end method

.method private b()Landroid/os/Bundle;
    .registers 4

    .prologue
    .line 469
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 470
    iget-object v1, p0, Landroid/support/design/internal/c;->b:Landroid/support/v7/internal/view/menu/m;

    if-eqz v1, :cond_14

    .line 471
    const-string v1, "android:menu:checked"

    iget-object v2, p0, Landroid/support/design/internal/c;->b:Landroid/support/v7/internal/view/menu/m;

    invoke-virtual {v2}, Landroid/support/v7/internal/view/menu/m;->getItemId()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 473
    :cond_14
    return-object v0
.end method


# virtual methods
.method public final a(I)Landroid/support/design/internal/d;
    .registers 3

    .prologue
    .line 289
    iget-object v0, p0, Landroid/support/design/internal/c;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/design/internal/d;

    return-object v0
.end method

.method final a()V
    .registers 16

    .prologue
    const/4 v3, 0x1

    const/4 v8, 0x0

    .line 377
    iget-boolean v0, p0, Landroid/support/design/internal/c;->c:Z

    if-eqz v0, :cond_7

    .line 443
    :goto_6
    return-void

    .line 380
    :cond_7
    iput-boolean v3, p0, Landroid/support/design/internal/c;->c:Z

    .line 381
    iget-object v0, p0, Landroid/support/design/internal/c;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 382
    const/4 v6, -0x1

    .line 385
    iget-object v0, p0, Landroid/support/design/internal/c;->d:Landroid/support/design/internal/b;

    invoke-static {v0}, Landroid/support/design/internal/b;->g(Landroid/support/design/internal/b;)Landroid/support/v7/internal/view/menu/i;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/i;->h()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v10

    move v9, v8

    move v4, v8

    move v5, v8

    :goto_20
    if-ge v9, v10, :cond_125

    .line 386
    iget-object v0, p0, Landroid/support/design/internal/c;->d:Landroid/support/design/internal/b;

    invoke-static {v0}, Landroid/support/design/internal/b;->g(Landroid/support/design/internal/b;)Landroid/support/v7/internal/view/menu/i;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/i;->h()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/view/menu/m;

    .line 387
    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/m;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_3b

    .line 388
    invoke-virtual {p0, v0}, Landroid/support/design/internal/c;->a(Landroid/support/v7/internal/view/menu/m;)V

    .line 390
    :cond_3b
    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/m;->isCheckable()Z

    move-result v1

    if-eqz v1, :cond_44

    .line 391
    invoke-virtual {v0, v8}, Landroid/support/v7/internal/view/menu/m;->a(Z)V

    .line 393
    :cond_44
    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/m;->hasSubMenu()Z

    move-result v1

    if-eqz v1, :cond_c6

    .line 394
    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/m;->getSubMenu()Landroid/view/SubMenu;

    move-result-object v11

    .line 395
    invoke-interface {v11}, Landroid/view/SubMenu;->hasVisibleItems()Z

    move-result v1

    if-eqz v1, :cond_bb

    .line 396
    if-eqz v9, :cond_65

    .line 397
    iget-object v1, p0, Landroid/support/design/internal/c;->a:Ljava/util/ArrayList;

    iget-object v2, p0, Landroid/support/design/internal/c;->d:Landroid/support/design/internal/b;

    invoke-static {v2}, Landroid/support/design/internal/b;->h(Landroid/support/design/internal/b;)I

    move-result v2

    invoke-static {v2, v8}, Landroid/support/design/internal/d;->a(II)Landroid/support/design/internal/d;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 399
    :cond_65
    iget-object v1, p0, Landroid/support/design/internal/c;->a:Ljava/util/ArrayList;

    invoke-static {v0}, Landroid/support/design/internal/d;->a(Landroid/support/v7/internal/view/menu/m;)Landroid/support/design/internal/d;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 401
    iget-object v1, p0, Landroid/support/design/internal/c;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v12

    .line 402
    invoke-interface {v11}, Landroid/view/SubMenu;->size()I

    move-result v13

    move v7, v8

    move v2, v8

    :goto_7a
    if-ge v7, v13, :cond_b0

    .line 403
    invoke-interface {v11, v7}, Landroid/view/SubMenu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v1

    check-cast v1, Landroid/support/v7/internal/view/menu/m;

    .line 404
    invoke-virtual {v1}, Landroid/support/v7/internal/view/menu/m;->isVisible()Z

    move-result v14

    if-eqz v14, :cond_ac

    .line 405
    if-nez v2, :cond_91

    invoke-virtual {v1}, Landroid/support/v7/internal/view/menu/m;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v14

    if-eqz v14, :cond_91

    move v2, v3

    .line 408
    :cond_91
    invoke-virtual {v1}, Landroid/support/v7/internal/view/menu/m;->isCheckable()Z

    move-result v14

    if-eqz v14, :cond_9a

    .line 409
    invoke-virtual {v1, v8}, Landroid/support/v7/internal/view/menu/m;->a(Z)V

    .line 411
    :cond_9a
    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/m;->isChecked()Z

    move-result v14

    if-eqz v14, :cond_a3

    .line 412
    invoke-virtual {p0, v0}, Landroid/support/design/internal/c;->a(Landroid/support/v7/internal/view/menu/m;)V

    .line 414
    :cond_a3
    iget-object v14, p0, Landroid/support/design/internal/c;->a:Ljava/util/ArrayList;

    invoke-static {v1}, Landroid/support/design/internal/d;->a(Landroid/support/v7/internal/view/menu/m;)Landroid/support/design/internal/d;

    move-result-object v1

    invoke-virtual {v14, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 402
    :cond_ac
    add-int/lit8 v1, v7, 0x1

    move v7, v1

    goto :goto_7a

    .line 417
    :cond_b0
    if-eqz v2, :cond_bb

    .line 418
    iget-object v0, p0, Landroid/support/design/internal/c;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-direct {p0, v12, v0}, Landroid/support/design/internal/c;->a(II)V

    :cond_bb
    move v0, v4

    move v1, v5

    move v2, v6

    .line 385
    :goto_be
    add-int/lit8 v4, v9, 0x1

    move v9, v4

    move v5, v1

    move v6, v2

    move v4, v0

    goto/16 :goto_20

    .line 422
    :cond_c6
    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/m;->getGroupId()I

    move-result v7

    .line 423
    if-eq v7, v6, :cond_111

    .line 424
    iget-object v1, p0, Landroid/support/design/internal/c;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v5

    .line 425
    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/m;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-eqz v1, :cond_10f

    move v4, v3

    .line 426
    :goto_d9
    if-eqz v9, :cond_129

    .line 427
    add-int/lit8 v5, v5, 0x1

    .line 428
    iget-object v1, p0, Landroid/support/design/internal/c;->a:Ljava/util/ArrayList;

    iget-object v2, p0, Landroid/support/design/internal/c;->d:Landroid/support/design/internal/b;

    invoke-static {v2}, Landroid/support/design/internal/b;->h(Landroid/support/design/internal/b;)I

    move-result v2

    iget-object v6, p0, Landroid/support/design/internal/c;->d:Landroid/support/design/internal/b;

    invoke-static {v6}, Landroid/support/design/internal/b;->h(Landroid/support/design/internal/b;)I

    move-result v6

    invoke-static {v2, v6}, Landroid/support/design/internal/d;->a(II)Landroid/support/design/internal/d;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v1, v4

    move v2, v5

    .line 435
    :goto_f4
    if-eqz v1, :cond_102

    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/m;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    if-nez v4, :cond_102

    .line 436
    const v4, 0x106000d

    invoke-virtual {v0, v4}, Landroid/support/v7/internal/view/menu/m;->setIcon(I)Landroid/view/MenuItem;

    .line 438
    :cond_102
    iget-object v4, p0, Landroid/support/design/internal/c;->a:Ljava/util/ArrayList;

    invoke-static {v0}, Landroid/support/design/internal/d;->a(Landroid/support/v7/internal/view/menu/m;)Landroid/support/design/internal/d;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v0, v1

    move v1, v2

    move v2, v7

    .line 439
    goto :goto_be

    :cond_10f
    move v4, v8

    .line 425
    goto :goto_d9

    .line 431
    :cond_111
    if-nez v4, :cond_129

    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/m;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-eqz v1, :cond_129

    .line 433
    iget-object v1, p0, Landroid/support/design/internal/c;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-direct {p0, v5, v1}, Landroid/support/design/internal/c;->a(II)V

    move v1, v3

    move v2, v5

    goto :goto_f4

    .line 442
    :cond_125
    iput-boolean v8, p0, Landroid/support/design/internal/c;->c:Z

    goto/16 :goto_6

    :cond_129
    move v1, v4

    move v2, v5

    goto :goto_f4
.end method

.method public final a(Landroid/support/v7/internal/view/menu/m;)V
    .registers 4

    .prologue
    .line 458
    iget-object v0, p0, Landroid/support/design/internal/c;->b:Landroid/support/v7/internal/view/menu/m;

    if-eq v0, p1, :cond_a

    invoke-virtual {p1}, Landroid/support/v7/internal/view/menu/m;->isCheckable()Z

    move-result v0

    if-nez v0, :cond_b

    .line 466
    :cond_a
    :goto_a
    return-void

    .line 461
    :cond_b
    iget-object v0, p0, Landroid/support/design/internal/c;->b:Landroid/support/v7/internal/view/menu/m;

    if-eqz v0, :cond_15

    .line 462
    iget-object v0, p0, Landroid/support/design/internal/c;->b:Landroid/support/v7/internal/view/menu/m;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/view/menu/m;->setChecked(Z)Landroid/view/MenuItem;

    .line 464
    :cond_15
    iput-object p1, p0, Landroid/support/design/internal/c;->b:Landroid/support/v7/internal/view/menu/m;

    .line 465
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/support/v7/internal/view/menu/m;->setChecked(Z)Landroid/view/MenuItem;

    goto :goto_a
.end method

.method public final areAllItemsEnabled()Z
    .registers 2

    .prologue
    .line 358
    const/4 v0, 0x0

    return v0
.end method

.method public final getCount()I
    .registers 2

    .prologue
    .line 284
    iget-object v0, p0, Landroid/support/design/internal/c;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public final synthetic getItem(I)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 265
    invoke-virtual {p0, p1}, Landroid/support/design/internal/c;->a(I)Landroid/support/design/internal/d;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .registers 4

    .prologue
    .line 294
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .registers 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 304
    invoke-virtual {p0, p1}, Landroid/support/design/internal/c;->a(I)Landroid/support/design/internal/d;

    move-result-object v3

    .line 1527
    iget-object v2, v3, Landroid/support/design/internal/d;->a:Landroid/support/v7/internal/view/menu/m;

    if-nez v2, :cond_f

    move v2, v0

    .line 305
    :goto_b
    if-eqz v2, :cond_11

    .line 306
    const/4 v0, 0x2

    .line 310
    :cond_e
    :goto_e
    return v0

    :cond_f
    move v2, v1

    .line 1527
    goto :goto_b

    .line 1539
    :cond_11
    iget-object v2, v3, Landroid/support/design/internal/d;->a:Landroid/support/v7/internal/view/menu/m;

    .line 307
    invoke-virtual {v2}, Landroid/support/v7/internal/view/menu/m;->hasSubMenu()Z

    move-result v2

    if-nez v2, :cond_e

    move v0, v1

    .line 310
    goto :goto_e
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 9

    .prologue
    const/4 v2, 0x0

    .line 316
    invoke-virtual {p0, p1}, Landroid/support/design/internal/c;->a(I)Landroid/support/design/internal/d;

    move-result-object v3

    .line 317
    invoke-virtual {p0, p1}, Landroid/support/design/internal/c;->getItemViewType(I)I

    move-result v0

    .line 318
    packed-switch v0, :pswitch_data_a6

    :goto_c
    move-object v1, p2

    .line 353
    :goto_d
    return-object v1

    .line 320
    :pswitch_e
    if-nez p2, :cond_a3

    .line 321
    iget-object v0, p0, Landroid/support/design/internal/c;->d:Landroid/support/design/internal/b;

    invoke-static {v0}, Landroid/support/design/internal/b;->a(Landroid/support/design/internal/b;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Landroid/support/design/k;->design_navigation_item:I

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    :goto_1c
    move-object v0, v1

    .line 324
    check-cast v0, Landroid/support/design/internal/NavigationMenuItemView;

    .line 325
    iget-object v2, p0, Landroid/support/design/internal/c;->d:Landroid/support/design/internal/b;

    invoke-static {v2}, Landroid/support/design/internal/b;->b(Landroid/support/design/internal/b;)Landroid/content/res/ColorStateList;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/support/design/internal/NavigationMenuItemView;->setIconTintList(Landroid/content/res/ColorStateList;)V

    .line 326
    iget-object v2, p0, Landroid/support/design/internal/c;->d:Landroid/support/design/internal/b;

    invoke-static {v2}, Landroid/support/design/internal/b;->c(Landroid/support/design/internal/b;)Z

    move-result v2

    if-eqz v2, :cond_3d

    .line 327
    invoke-virtual {v0}, Landroid/support/design/internal/NavigationMenuItemView;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v4, p0, Landroid/support/design/internal/c;->d:Landroid/support/design/internal/b;

    invoke-static {v4}, Landroid/support/design/internal/b;->d(Landroid/support/design/internal/b;)I

    move-result v4

    invoke-virtual {v0, v2, v4}, Landroid/support/design/internal/NavigationMenuItemView;->setTextAppearance(Landroid/content/Context;I)V

    .line 329
    :cond_3d
    iget-object v2, p0, Landroid/support/design/internal/c;->d:Landroid/support/design/internal/b;

    invoke-static {v2}, Landroid/support/design/internal/b;->e(Landroid/support/design/internal/b;)Landroid/content/res/ColorStateList;

    move-result-object v2

    if-eqz v2, :cond_4e

    .line 330
    iget-object v2, p0, Landroid/support/design/internal/c;->d:Landroid/support/design/internal/b;

    invoke-static {v2}, Landroid/support/design/internal/b;->e(Landroid/support/design/internal/b;)Landroid/content/res/ColorStateList;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/support/design/internal/NavigationMenuItemView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 332
    :cond_4e
    iget-object v2, p0, Landroid/support/design/internal/c;->d:Landroid/support/design/internal/b;

    invoke-static {v2}, Landroid/support/design/internal/b;->f(Landroid/support/design/internal/b;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    if-eqz v2, :cond_6d

    iget-object v2, p0, Landroid/support/design/internal/c;->d:Landroid/support/design/internal/b;

    invoke-static {v2}, Landroid/support/design/internal/b;->f(Landroid/support/design/internal/b;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    :goto_64
    invoke-virtual {v0, v2}, Landroid/support/design/internal/NavigationMenuItemView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2539
    iget-object v2, v3, Landroid/support/design/internal/d;->a:Landroid/support/v7/internal/view/menu/m;

    .line 334
    invoke-virtual {v0, v2}, Landroid/support/design/internal/NavigationMenuItemView;->a(Landroid/support/v7/internal/view/menu/m;)V

    goto :goto_d

    .line 332
    :cond_6d
    const/4 v2, 0x0

    goto :goto_64

    .line 337
    :pswitch_6f
    if-nez p2, :cond_a1

    .line 338
    iget-object v0, p0, Landroid/support/design/internal/c;->d:Landroid/support/design/internal/b;

    invoke-static {v0}, Landroid/support/design/internal/b;->a(Landroid/support/design/internal/b;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Landroid/support/design/k;->design_navigation_item_subheader:I

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    :goto_7d
    move-object v0, v1

    .line 341
    check-cast v0, Landroid/widget/TextView;

    .line 3539
    iget-object v2, v3, Landroid/support/design/internal/d;->a:Landroid/support/v7/internal/view/menu/m;

    .line 342
    invoke-virtual {v2}, Landroid/support/v7/internal/view/menu/m;->getTitle()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_d

    .line 345
    :pswitch_8a
    if-nez p2, :cond_98

    .line 346
    iget-object v0, p0, Landroid/support/design/internal/c;->d:Landroid/support/design/internal/b;

    invoke-static {v0}, Landroid/support/design/internal/b;->a(Landroid/support/design/internal/b;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Landroid/support/design/k;->design_navigation_item_separator:I

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 4531
    :cond_98
    iget v0, v3, Landroid/support/design/internal/d;->b:I

    .line 4535
    iget v1, v3, Landroid/support/design/internal/d;->c:I

    .line 349
    invoke-virtual {p2, v2, v0, v2, v1}, Landroid/view/View;->setPadding(IIII)V

    goto/16 :goto_c

    :cond_a1
    move-object v1, p2

    goto :goto_7d

    :cond_a3
    move-object v1, p2

    goto/16 :goto_1c

    .line 318
    :pswitch_data_a6
    .packed-switch 0x0
        :pswitch_e
        :pswitch_6f
        :pswitch_8a
    .end packed-switch
.end method

.method public final getViewTypeCount()I
    .registers 2

    .prologue
    .line 299
    const/4 v0, 0x3

    return v0
.end method

.method public final isEnabled(I)Z
    .registers 4

    .prologue
    .line 363
    invoke-virtual {p0, p1}, Landroid/support/design/internal/c;->a(I)Landroid/support/design/internal/d;

    move-result-object v0

    .line 4544
    iget-object v1, v0, Landroid/support/design/internal/d;->a:Landroid/support/v7/internal/view/menu/m;

    if-eqz v1, :cond_1a

    iget-object v1, v0, Landroid/support/design/internal/d;->a:Landroid/support/v7/internal/view/menu/m;

    invoke-virtual {v1}, Landroid/support/v7/internal/view/menu/m;->hasSubMenu()Z

    move-result v1

    if-nez v1, :cond_1a

    iget-object v0, v0, Landroid/support/design/internal/d;->a:Landroid/support/v7/internal/view/menu/m;

    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/m;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_1a

    const/4 v0, 0x1

    :goto_19
    return v0

    :cond_1a
    const/4 v0, 0x0

    .line 363
    goto :goto_19
.end method

.method public final notifyDataSetChanged()V
    .registers 1

    .prologue
    .line 368
    invoke-virtual {p0}, Landroid/support/design/internal/c;->a()V

    .line 369
    invoke-super {p0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    .line 370
    return-void
.end method
