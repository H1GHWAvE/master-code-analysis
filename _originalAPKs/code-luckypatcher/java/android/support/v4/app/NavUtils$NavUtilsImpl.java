package android.support.v4.app;
interface NavUtils$NavUtilsImpl {

    public abstract android.content.Intent getParentActivityIntent();

    public abstract String getParentActivityName();

    public abstract void navigateUpTo();

    public abstract boolean shouldUpRecreateTask();
}
