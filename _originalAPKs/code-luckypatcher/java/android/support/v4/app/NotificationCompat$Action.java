package android.support.v4.app;
public class NotificationCompat$Action {
    public android.app.PendingIntent actionIntent;
    public int icon;
    public CharSequence title;

    public NotificationCompat$Action(int p1, CharSequence p2, android.app.PendingIntent p3)
    {
        this.icon = p1;
        this.title = p2;
        this.actionIntent = p3;
        return;
    }
}
