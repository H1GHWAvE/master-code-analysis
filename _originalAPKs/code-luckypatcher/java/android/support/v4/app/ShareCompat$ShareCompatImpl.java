package android.support.v4.app;
interface ShareCompat$ShareCompatImpl {

    public abstract void configureMenuItem();

    public abstract String escapeHtml();
}
