package android.support.v4.app;
final class FragmentState$1 implements android.os.Parcelable$Creator {

    FragmentState$1()
    {
        return;
    }

    public android.support.v4.app.FragmentState createFromParcel(android.os.Parcel p2)
    {
        return new android.support.v4.app.FragmentState(p2);
    }

    public bridge synthetic Object createFromParcel(android.os.Parcel p2)
    {
        return this.createFromParcel(p2);
    }

    public android.support.v4.app.FragmentState[] newArray(int p2)
    {
        android.support.v4.app.FragmentState[] v0 = new android.support.v4.app.FragmentState[p2];
        return v0;
    }

    public bridge synthetic Object[] newArray(int p2)
    {
        return this.newArray(p2);
    }
}
