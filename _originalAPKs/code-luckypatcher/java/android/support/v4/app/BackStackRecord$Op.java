package android.support.v4.app;
final class BackStackRecord$Op {
    int cmd;
    int enterAnim;
    int exitAnim;
    android.support.v4.app.Fragment fragment;
    android.support.v4.app.BackStackRecord$Op next;
    int popEnterAnim;
    int popExitAnim;
    android.support.v4.app.BackStackRecord$Op prev;
    java.util.ArrayList removed;

    BackStackRecord$Op()
    {
        return;
    }
}
