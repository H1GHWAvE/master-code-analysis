package android.support.v4.app;
final class Fragment$SavedState$1 implements android.os.Parcelable$Creator {

    Fragment$SavedState$1()
    {
        return;
    }

    public android.support.v4.app.Fragment$SavedState createFromParcel(android.os.Parcel p3)
    {
        return new android.support.v4.app.Fragment$SavedState(p3, 0);
    }

    public bridge synthetic Object createFromParcel(android.os.Parcel p2)
    {
        return this.createFromParcel(p2);
    }

    public android.support.v4.app.Fragment$SavedState[] newArray(int p2)
    {
        android.support.v4.app.Fragment$SavedState[] v0 = new android.support.v4.app.Fragment$SavedState[p2];
        return v0;
    }

    public bridge synthetic Object[] newArray(int p2)
    {
        return this.newArray(p2);
    }
}
