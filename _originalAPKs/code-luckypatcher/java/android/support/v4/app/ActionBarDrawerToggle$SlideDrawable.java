package android.support.v4.app;
 class ActionBarDrawerToggle$SlideDrawable extends android.graphics.drawable.Drawable implements android.graphics.drawable.Drawable$Callback {
    private float mOffset;
    private float mOffsetBy;
    private final android.graphics.Rect mTmpRect;
    private android.graphics.drawable.Drawable mWrapped;

    public ActionBarDrawerToggle$SlideDrawable(android.graphics.drawable.Drawable p2)
    {
        this.mTmpRect = new android.graphics.Rect();
        this.mWrapped = p2;
        return;
    }

    public void clearColorFilter()
    {
        this.mWrapped.clearColorFilter();
        return;
    }

    public void draw(android.graphics.Canvas p3)
    {
        this.mWrapped.copyBounds(this.mTmpRect);
        p3.save();
        p3.translate(((this.mOffsetBy * ((float) this.mTmpRect.width())) * (- this.mOffset)), 0);
        this.mWrapped.draw(p3);
        p3.restore();
        return;
    }

    public int getChangingConfigurations()
    {
        return this.mWrapped.getChangingConfigurations();
    }

    public android.graphics.drawable.Drawable$ConstantState getConstantState()
    {
        return super.getConstantState();
    }

    public android.graphics.drawable.Drawable getCurrent()
    {
        return this.mWrapped.getCurrent();
    }

    public int getIntrinsicHeight()
    {
        return this.mWrapped.getIntrinsicHeight();
    }

    public int getIntrinsicWidth()
    {
        return this.mWrapped.getIntrinsicWidth();
    }

    public int getMinimumHeight()
    {
        return this.mWrapped.getMinimumHeight();
    }

    public int getMinimumWidth()
    {
        return this.mWrapped.getMinimumWidth();
    }

    public float getOffset()
    {
        return this.mOffset;
    }

    public int getOpacity()
    {
        return this.mWrapped.getOpacity();
    }

    public boolean getPadding(android.graphics.Rect p2)
    {
        return this.mWrapped.getPadding(p2);
    }

    public int[] getState()
    {
        return this.mWrapped.getState();
    }

    public android.graphics.Region getTransparentRegion()
    {
        return this.mWrapped.getTransparentRegion();
    }

    public void invalidateDrawable(android.graphics.drawable.Drawable p2)
    {
        if (p2 == this.mWrapped) {
            this.invalidateSelf();
        }
        return;
    }

    public boolean isStateful()
    {
        return this.mWrapped.isStateful();
    }

    protected void onBoundsChange(android.graphics.Rect p2)
    {
        super.onBoundsChange(p2);
        this.mWrapped.setBounds(p2);
        return;
    }

    protected boolean onStateChange(int[] p2)
    {
        this.mWrapped.setState(p2);
        return super.onStateChange(p2);
    }

    public void scheduleDrawable(android.graphics.drawable.Drawable p2, Runnable p3, long p4)
    {
        if (p2 == this.mWrapped) {
            this.scheduleSelf(p3, p4);
        }
        return;
    }

    public void setAlpha(int p2)
    {
        this.mWrapped.setAlpha(p2);
        return;
    }

    public void setChangingConfigurations(int p2)
    {
        this.mWrapped.setChangingConfigurations(p2);
        return;
    }

    public void setColorFilter(int p2, android.graphics.PorterDuff$Mode p3)
    {
        this.mWrapped.setColorFilter(p2, p3);
        return;
    }

    public void setColorFilter(android.graphics.ColorFilter p2)
    {
        this.mWrapped.setColorFilter(p2);
        return;
    }

    public void setDither(boolean p2)
    {
        this.mWrapped.setDither(p2);
        return;
    }

    public void setFilterBitmap(boolean p2)
    {
        this.mWrapped.setFilterBitmap(p2);
        return;
    }

    public void setOffset(float p1)
    {
        this.mOffset = p1;
        this.invalidateSelf();
        return;
    }

    public void setOffsetBy(float p1)
    {
        this.mOffsetBy = p1;
        this.invalidateSelf();
        return;
    }

    public boolean setState(int[] p2)
    {
        return this.mWrapped.setState(p2);
    }

    public boolean setVisible(boolean p2, boolean p3)
    {
        return super.setVisible(p2, p3);
    }

    public void unscheduleDrawable(android.graphics.drawable.Drawable p2, Runnable p3)
    {
        if (p2 == this.mWrapped) {
            this.unscheduleSelf(p3);
        }
        return;
    }
}
