package android.support.v4.app;
 class ActionBarDrawerToggle$ActionBarDrawerToggleImplBase implements android.support.v4.app.ActionBarDrawerToggle$ActionBarDrawerToggleImpl {

    private ActionBarDrawerToggle$ActionBarDrawerToggleImplBase()
    {
        return;
    }

    synthetic ActionBarDrawerToggle$ActionBarDrawerToggleImplBase(android.support.v4.app.ActionBarDrawerToggle$1 p1)
    {
        return;
    }

    public android.graphics.drawable.Drawable getThemeUpIndicator(android.app.Activity p2)
    {
        return 0;
    }

    public Object setActionBarDescription(Object p1, android.app.Activity p2, int p3)
    {
        return p1;
    }

    public Object setActionBarUpIndicator(Object p1, android.app.Activity p2, android.graphics.drawable.Drawable p3, int p4)
    {
        return p1;
    }
}
