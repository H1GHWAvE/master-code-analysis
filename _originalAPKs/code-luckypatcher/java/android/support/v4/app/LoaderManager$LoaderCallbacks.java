package android.support.v4.app;
public interface LoaderManager$LoaderCallbacks {

    public abstract android.support.v4.content.Loader onCreateLoader();

    public abstract void onLoadFinished();

    public abstract void onLoaderReset();
}
