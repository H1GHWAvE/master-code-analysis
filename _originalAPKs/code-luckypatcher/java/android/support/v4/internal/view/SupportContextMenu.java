package android.support.v4.internal.view;
public interface SupportContextMenu implements android.support.v4.internal.view.SupportMenu, android.view.ContextMenu {

    public abstract void clearHeader();

    public abstract android.support.v4.internal.view.SupportContextMenu setHeaderIcon();

    public abstract android.support.v4.internal.view.SupportContextMenu setHeaderIcon();

    public abstract android.support.v4.internal.view.SupportContextMenu setHeaderTitle();

    public abstract android.support.v4.internal.view.SupportContextMenu setHeaderTitle();

    public abstract android.support.v4.internal.view.SupportContextMenu setHeaderView();
}
