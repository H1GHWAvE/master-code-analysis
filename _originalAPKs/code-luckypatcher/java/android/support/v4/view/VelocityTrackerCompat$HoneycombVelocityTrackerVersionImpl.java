package android.support.v4.view;
 class VelocityTrackerCompat$HoneycombVelocityTrackerVersionImpl implements android.support.v4.view.VelocityTrackerCompat$VelocityTrackerVersionImpl {

    VelocityTrackerCompat$HoneycombVelocityTrackerVersionImpl()
    {
        return;
    }

    public float getXVelocity(android.view.VelocityTracker p2, int p3)
    {
        return android.support.v4.view.VelocityTrackerCompatHoneycomb.getXVelocity(p2, p3);
    }

    public float getYVelocity(android.view.VelocityTracker p2, int p3)
    {
        return android.support.v4.view.VelocityTrackerCompatHoneycomb.getYVelocity(p2, p3);
    }
}
