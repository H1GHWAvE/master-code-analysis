package android.support.v4.view;
interface GestureDetectorCompat$GestureDetectorCompatImpl {

    public abstract boolean isLongpressEnabled();

    public abstract boolean onTouchEvent();

    public abstract void setIsLongpressEnabled();

    public abstract void setOnDoubleTapListener();
}
