package android.support.v4.view;
 class ViewCompat$BaseViewCompatImpl implements android.support.v4.view.ViewCompat$ViewCompatImpl {

    ViewCompat$BaseViewCompatImpl()
    {
        return;
    }

    public boolean canScrollHorizontally(android.view.View p2, int p3)
    {
        return 0;
    }

    public boolean canScrollVertically(android.view.View p2, int p3)
    {
        return 0;
    }

    public android.support.v4.view.accessibility.AccessibilityNodeProviderCompat getAccessibilityNodeProvider(android.view.View p2)
    {
        return 0;
    }

    public float getAlpha(android.view.View p2)
    {
        return 1065353216;
    }

    long getFrameTime()
    {
        return 10;
    }

    public int getImportantForAccessibility(android.view.View p2)
    {
        return 0;
    }

    public int getLabelFor(android.view.View p2)
    {
        return 0;
    }

    public int getLayerType(android.view.View p2)
    {
        return 0;
    }

    public int getLayoutDirection(android.view.View p2)
    {
        return 0;
    }

    public int getOverScrollMode(android.view.View p2)
    {
        return 2;
    }

    public android.view.ViewParent getParentForAccessibility(android.view.View p2)
    {
        return p2.getParent();
    }

    public boolean hasTransientState(android.view.View p2)
    {
        return 0;
    }

    public boolean isOpaque(android.view.View p5)
    {
        int v1 = 0;
        android.graphics.drawable.Drawable v0 = p5.getBackground();
        if ((v0 != null) && (v0.getOpacity() == -1)) {
            v1 = 1;
        }
        return v1;
    }

    public int[] mergeDrawableStates(int[] p6, int[] p7)
    {
        int v1 = (p6.length - 1);
        while ((v1 >= 0) && (p6[v1] == 0)) {
            v1--;
        }
        System.arraycopy(p7, 0, p6, (v1 + 1), p7.length);
        return p6;
    }

    public void onInitializeAccessibilityEvent(android.view.View p1, android.view.accessibility.AccessibilityEvent p2)
    {
        return;
    }

    public void onInitializeAccessibilityNodeInfo(android.view.View p1, android.support.v4.view.accessibility.AccessibilityNodeInfoCompat p2)
    {
        return;
    }

    public void onPopulateAccessibilityEvent(android.view.View p1, android.view.accessibility.AccessibilityEvent p2)
    {
        return;
    }

    public boolean performAccessibilityAction(android.view.View p2, int p3, android.os.Bundle p4)
    {
        return 0;
    }

    public void postInvalidateOnAnimation(android.view.View p3)
    {
        p3.postInvalidateDelayed(this.getFrameTime());
        return;
    }

    public void postInvalidateOnAnimation(android.view.View p8, int p9, int p10, int p11, int p12)
    {
        p8.postInvalidateDelayed(this.getFrameTime(), p9, p10, p11, p12);
        return;
    }

    public void postOnAnimation(android.view.View p3, Runnable p4)
    {
        p3.postDelayed(p4, this.getFrameTime());
        return;
    }

    public void postOnAnimationDelayed(android.view.View p3, Runnable p4, long p5)
    {
        p3.postDelayed(p4, (this.getFrameTime() + p5));
        return;
    }

    public int resolveSizeAndState(int p5, int p6, int p7)
    {
        int v0 = p5;
        int v1 = android.view.View$MeasureSpec.getMode(p6);
        int v2 = android.view.View$MeasureSpec.getSize(p6);
        switch (v1) {
            case -2147483648:
                if (v2 >= p5) {
                    v0 = p5;
                } else {
                    v0 = (v2 | 16777216);
                }
                break;
            case 0:
                v0 = p5;
                break;
            case 1073741824:
                v0 = v2;
                break;
        }
        return ((-16777216 & p7) | v0);
    }

    public void setAccessibilityDelegate(android.view.View p1, android.support.v4.view.AccessibilityDelegateCompat p2)
    {
        return;
    }

    public void setHasTransientState(android.view.View p1, boolean p2)
    {
        return;
    }

    public void setImportantForAccessibility(android.view.View p1, int p2)
    {
        return;
    }

    public void setLabelFor(android.view.View p1, int p2)
    {
        return;
    }

    public void setLayerPaint(android.view.View p1, android.graphics.Paint p2)
    {
        return;
    }

    public void setLayerType(android.view.View p1, int p2, android.graphics.Paint p3)
    {
        return;
    }

    public void setLayoutDirection(android.view.View p1, int p2)
    {
        return;
    }

    public void setOverScrollMode(android.view.View p1, int p2)
    {
        return;
    }
}
