package android.support.v4.view;
public interface ViewPager$PageTransformer {

    public abstract void transformPage();
}
