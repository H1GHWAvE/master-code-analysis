package android.support.v4.view;
public interface AccessibilityDelegateCompatJellyBean$AccessibilityDelegateBridgeJellyBean {

    public abstract boolean dispatchPopulateAccessibilityEvent();

    public abstract Object getAccessibilityNodeProvider();

    public abstract void onInitializeAccessibilityEvent();

    public abstract void onInitializeAccessibilityNodeInfo();

    public abstract void onPopulateAccessibilityEvent();

    public abstract boolean onRequestSendAccessibilityEvent();

    public abstract boolean performAccessibilityAction();

    public abstract void sendAccessibilityEvent();

    public abstract void sendAccessibilityEventUnchecked();
}
