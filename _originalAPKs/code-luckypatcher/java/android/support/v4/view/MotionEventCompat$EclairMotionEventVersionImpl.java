package android.support.v4.view;
 class MotionEventCompat$EclairMotionEventVersionImpl implements android.support.v4.view.MotionEventCompat$MotionEventVersionImpl {

    MotionEventCompat$EclairMotionEventVersionImpl()
    {
        return;
    }

    public int findPointerIndex(android.view.MotionEvent p2, int p3)
    {
        return android.support.v4.view.MotionEventCompatEclair.findPointerIndex(p2, p3);
    }

    public int getPointerCount(android.view.MotionEvent p2)
    {
        return android.support.v4.view.MotionEventCompatEclair.getPointerCount(p2);
    }

    public int getPointerId(android.view.MotionEvent p2, int p3)
    {
        return android.support.v4.view.MotionEventCompatEclair.getPointerId(p2, p3);
    }

    public float getX(android.view.MotionEvent p2, int p3)
    {
        return android.support.v4.view.MotionEventCompatEclair.getX(p2, p3);
    }

    public float getY(android.view.MotionEvent p2, int p3)
    {
        return android.support.v4.view.MotionEventCompatEclair.getY(p2, p3);
    }
}
