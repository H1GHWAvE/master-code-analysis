package android.support.v4.view;
public interface ActionProvider$SubUiVisibilityListener {

    public abstract void onSubUiVisibilityChanged();
}
