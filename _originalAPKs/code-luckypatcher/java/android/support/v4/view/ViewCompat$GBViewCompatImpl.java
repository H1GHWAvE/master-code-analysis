package android.support.v4.view;
 class ViewCompat$GBViewCompatImpl extends android.support.v4.view.ViewCompat$EclairMr1ViewCompatImpl {

    ViewCompat$GBViewCompatImpl()
    {
        return;
    }

    public int getOverScrollMode(android.view.View p2)
    {
        return android.support.v4.view.ViewCompatGingerbread.getOverScrollMode(p2);
    }

    public void setOverScrollMode(android.view.View p1, int p2)
    {
        android.support.v4.view.ViewCompatGingerbread.setOverScrollMode(p1, p2);
        return;
    }
}
