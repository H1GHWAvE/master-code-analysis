package android.support.v4.view;
 class ViewCompatJellybeanMr1 {

    ViewCompatJellybeanMr1()
    {
        return;
    }

    public static int getLabelFor(android.view.View p1)
    {
        return p1.getLabelFor();
    }

    public static int getLayoutDirection(android.view.View p1)
    {
        return p1.getLayoutDirection();
    }

    public static void setLabelFor(android.view.View p0, int p1)
    {
        p0.setLabelFor(p1);
        return;
    }

    public static void setLayerPaint(android.view.View p0, android.graphics.Paint p1)
    {
        p0.setLayerPaint(p1);
        return;
    }

    public static void setLayoutDirection(android.view.View p0, int p1)
    {
        p0.setLayoutDirection(p1);
        return;
    }
}
