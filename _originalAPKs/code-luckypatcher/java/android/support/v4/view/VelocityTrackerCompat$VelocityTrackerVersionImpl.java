package android.support.v4.view;
interface VelocityTrackerCompat$VelocityTrackerVersionImpl {

    public abstract float getXVelocity();

    public abstract float getYVelocity();
}
