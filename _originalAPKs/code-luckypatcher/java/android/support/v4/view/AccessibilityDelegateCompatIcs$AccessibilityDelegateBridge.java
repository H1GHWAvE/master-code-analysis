package android.support.v4.view;
public interface AccessibilityDelegateCompatIcs$AccessibilityDelegateBridge {

    public abstract boolean dispatchPopulateAccessibilityEvent();

    public abstract void onInitializeAccessibilityEvent();

    public abstract void onInitializeAccessibilityNodeInfo();

    public abstract void onPopulateAccessibilityEvent();

    public abstract boolean onRequestSendAccessibilityEvent();

    public abstract void sendAccessibilityEvent();

    public abstract void sendAccessibilityEventUnchecked();
}
