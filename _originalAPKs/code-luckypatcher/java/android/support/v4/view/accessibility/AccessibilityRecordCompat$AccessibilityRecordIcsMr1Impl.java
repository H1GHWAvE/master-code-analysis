package android.support.v4.view.accessibility;
 class AccessibilityRecordCompat$AccessibilityRecordIcsMr1Impl extends android.support.v4.view.accessibility.AccessibilityRecordCompat$AccessibilityRecordIcsImpl {

    AccessibilityRecordCompat$AccessibilityRecordIcsMr1Impl()
    {
        return;
    }

    public int getMaxScrollX(Object p2)
    {
        return android.support.v4.view.accessibility.AccessibilityRecordCompatIcsMr1.getMaxScrollX(p2);
    }

    public int getMaxScrollY(Object p2)
    {
        return android.support.v4.view.accessibility.AccessibilityRecordCompatIcsMr1.getMaxScrollY(p2);
    }

    public void setMaxScrollX(Object p1, int p2)
    {
        android.support.v4.view.accessibility.AccessibilityRecordCompatIcsMr1.setMaxScrollX(p1, p2);
        return;
    }

    public void setMaxScrollY(Object p1, int p2)
    {
        android.support.v4.view.accessibility.AccessibilityRecordCompatIcsMr1.setMaxScrollY(p1, p2);
        return;
    }
}
