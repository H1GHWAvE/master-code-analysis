package android.support.v4.view.accessibility;
interface AccessibilityNodeInfoCompat$AccessibilityNodeInfoImpl {

    public abstract void addAction();

    public abstract void addChild();

    public abstract void addChild();

    public abstract java.util.List findAccessibilityNodeInfosByText();

    public abstract Object findFocus();

    public abstract Object focusSearch();

    public abstract int getActions();

    public abstract void getBoundsInParent();

    public abstract void getBoundsInScreen();

    public abstract Object getChild();

    public abstract int getChildCount();

    public abstract CharSequence getClassName();

    public abstract CharSequence getContentDescription();

    public abstract int getMovementGranularities();

    public abstract CharSequence getPackageName();

    public abstract Object getParent();

    public abstract CharSequence getText();

    public abstract String getViewIdResourceName();

    public abstract int getWindowId();

    public abstract boolean isAccessibilityFocused();

    public abstract boolean isCheckable();

    public abstract boolean isChecked();

    public abstract boolean isClickable();

    public abstract boolean isEnabled();

    public abstract boolean isFocusable();

    public abstract boolean isFocused();

    public abstract boolean isLongClickable();

    public abstract boolean isPassword();

    public abstract boolean isScrollable();

    public abstract boolean isSelected();

    public abstract boolean isVisibleToUser();

    public abstract Object obtain();

    public abstract Object obtain();

    public abstract Object obtain();

    public abstract Object obtain();

    public abstract boolean performAction();

    public abstract boolean performAction();

    public abstract void recycle();

    public abstract void setAccessibilityFocused();

    public abstract void setBoundsInParent();

    public abstract void setBoundsInScreen();

    public abstract void setCheckable();

    public abstract void setChecked();

    public abstract void setClassName();

    public abstract void setClickable();

    public abstract void setContentDescription();

    public abstract void setEnabled();

    public abstract void setFocusable();

    public abstract void setFocused();

    public abstract void setLongClickable();

    public abstract void setMovementGranularities();

    public abstract void setPackageName();

    public abstract void setParent();

    public abstract void setParent();

    public abstract void setPassword();

    public abstract void setScrollable();

    public abstract void setSelected();

    public abstract void setSource();

    public abstract void setSource();

    public abstract void setText();

    public abstract void setViewIdResourceName();

    public abstract void setVisibleToUser();
}
