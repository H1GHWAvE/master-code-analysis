package android.support.v4.view.accessibility;
 class AccessibilityRecordCompat$AccessibilityRecordStubImpl implements android.support.v4.view.accessibility.AccessibilityRecordCompat$AccessibilityRecordImpl {

    AccessibilityRecordCompat$AccessibilityRecordStubImpl()
    {
        return;
    }

    public int getAddedCount(Object p2)
    {
        return 0;
    }

    public CharSequence getBeforeText(Object p2)
    {
        return 0;
    }

    public CharSequence getClassName(Object p2)
    {
        return 0;
    }

    public CharSequence getContentDescription(Object p2)
    {
        return 0;
    }

    public int getCurrentItemIndex(Object p2)
    {
        return 0;
    }

    public int getFromIndex(Object p2)
    {
        return 0;
    }

    public int getItemCount(Object p2)
    {
        return 0;
    }

    public int getMaxScrollX(Object p2)
    {
        return 0;
    }

    public int getMaxScrollY(Object p2)
    {
        return 0;
    }

    public android.os.Parcelable getParcelableData(Object p2)
    {
        return 0;
    }

    public int getRemovedCount(Object p2)
    {
        return 0;
    }

    public int getScrollX(Object p2)
    {
        return 0;
    }

    public int getScrollY(Object p2)
    {
        return 0;
    }

    public android.support.v4.view.accessibility.AccessibilityNodeInfoCompat getSource(Object p2)
    {
        return 0;
    }

    public java.util.List getText(Object p2)
    {
        return java.util.Collections.emptyList();
    }

    public int getToIndex(Object p2)
    {
        return 0;
    }

    public int getWindowId(Object p2)
    {
        return 0;
    }

    public boolean isChecked(Object p2)
    {
        return 0;
    }

    public boolean isEnabled(Object p2)
    {
        return 0;
    }

    public boolean isFullScreen(Object p2)
    {
        return 0;
    }

    public boolean isPassword(Object p2)
    {
        return 0;
    }

    public boolean isScrollable(Object p2)
    {
        return 0;
    }

    public Object obtain()
    {
        return 0;
    }

    public Object obtain(Object p2)
    {
        return 0;
    }

    public void recycle(Object p1)
    {
        return;
    }

    public void setAddedCount(Object p1, int p2)
    {
        return;
    }

    public void setBeforeText(Object p1, CharSequence p2)
    {
        return;
    }

    public void setChecked(Object p1, boolean p2)
    {
        return;
    }

    public void setClassName(Object p1, CharSequence p2)
    {
        return;
    }

    public void setContentDescription(Object p1, CharSequence p2)
    {
        return;
    }

    public void setCurrentItemIndex(Object p1, int p2)
    {
        return;
    }

    public void setEnabled(Object p1, boolean p2)
    {
        return;
    }

    public void setFromIndex(Object p1, int p2)
    {
        return;
    }

    public void setFullScreen(Object p1, boolean p2)
    {
        return;
    }

    public void setItemCount(Object p1, int p2)
    {
        return;
    }

    public void setMaxScrollX(Object p1, int p2)
    {
        return;
    }

    public void setMaxScrollY(Object p1, int p2)
    {
        return;
    }

    public void setParcelableData(Object p1, android.os.Parcelable p2)
    {
        return;
    }

    public void setPassword(Object p1, boolean p2)
    {
        return;
    }

    public void setRemovedCount(Object p1, int p2)
    {
        return;
    }

    public void setScrollX(Object p1, int p2)
    {
        return;
    }

    public void setScrollY(Object p1, int p2)
    {
        return;
    }

    public void setScrollable(Object p1, boolean p2)
    {
        return;
    }

    public void setSource(Object p1, android.view.View p2)
    {
        return;
    }

    public void setSource(Object p1, android.view.View p2, int p3)
    {
        return;
    }

    public void setToIndex(Object p1, int p2)
    {
        return;
    }
}
