package android.support.v4.view.accessibility;
 class AccessibilityRecordCompatIcs {

    AccessibilityRecordCompatIcs()
    {
        return;
    }

    public static int getAddedCount(Object p1)
    {
        return ((android.view.accessibility.AccessibilityRecord) p1).getAddedCount();
    }

    public static CharSequence getBeforeText(Object p1)
    {
        return ((android.view.accessibility.AccessibilityRecord) p1).getBeforeText();
    }

    public static CharSequence getClassName(Object p1)
    {
        return ((android.view.accessibility.AccessibilityRecord) p1).getClassName();
    }

    public static CharSequence getContentDescription(Object p1)
    {
        return ((android.view.accessibility.AccessibilityRecord) p1).getContentDescription();
    }

    public static int getCurrentItemIndex(Object p1)
    {
        return ((android.view.accessibility.AccessibilityRecord) p1).getCurrentItemIndex();
    }

    public static int getFromIndex(Object p1)
    {
        return ((android.view.accessibility.AccessibilityRecord) p1).getFromIndex();
    }

    public static int getItemCount(Object p1)
    {
        return ((android.view.accessibility.AccessibilityRecord) p1).getItemCount();
    }

    public static android.os.Parcelable getParcelableData(Object p1)
    {
        return ((android.view.accessibility.AccessibilityRecord) p1).getParcelableData();
    }

    public static int getRemovedCount(Object p1)
    {
        return ((android.view.accessibility.AccessibilityRecord) p1).getRemovedCount();
    }

    public static int getScrollX(Object p1)
    {
        return ((android.view.accessibility.AccessibilityRecord) p1).getScrollX();
    }

    public static int getScrollY(Object p1)
    {
        return ((android.view.accessibility.AccessibilityRecord) p1).getScrollY();
    }

    public static Object getSource(Object p1)
    {
        return ((android.view.accessibility.AccessibilityRecord) p1).getSource();
    }

    public static java.util.List getText(Object p1)
    {
        return ((android.view.accessibility.AccessibilityRecord) p1).getText();
    }

    public static int getToIndex(Object p1)
    {
        return ((android.view.accessibility.AccessibilityRecord) p1).getToIndex();
    }

    public static int getWindowId(Object p1)
    {
        return ((android.view.accessibility.AccessibilityRecord) p1).getWindowId();
    }

    public static boolean isChecked(Object p1)
    {
        return ((android.view.accessibility.AccessibilityRecord) p1).isChecked();
    }

    public static boolean isEnabled(Object p1)
    {
        return ((android.view.accessibility.AccessibilityRecord) p1).isEnabled();
    }

    public static boolean isFullScreen(Object p1)
    {
        return ((android.view.accessibility.AccessibilityRecord) p1).isFullScreen();
    }

    public static boolean isPassword(Object p1)
    {
        return ((android.view.accessibility.AccessibilityRecord) p1).isPassword();
    }

    public static boolean isScrollable(Object p1)
    {
        return ((android.view.accessibility.AccessibilityRecord) p1).isScrollable();
    }

    public static Object obtain()
    {
        return android.view.accessibility.AccessibilityRecord.obtain();
    }

    public static Object obtain(Object p1)
    {
        return android.view.accessibility.AccessibilityRecord.obtain(((android.view.accessibility.AccessibilityRecord) p1));
    }

    public static void recycle(Object p0)
    {
        ((android.view.accessibility.AccessibilityRecord) p0).recycle();
        return;
    }

    public static void setAddedCount(Object p0, int p1)
    {
        ((android.view.accessibility.AccessibilityRecord) p0).setAddedCount(p1);
        return;
    }

    public static void setBeforeText(Object p0, CharSequence p1)
    {
        ((android.view.accessibility.AccessibilityRecord) p0).setBeforeText(p1);
        return;
    }

    public static void setChecked(Object p0, boolean p1)
    {
        ((android.view.accessibility.AccessibilityRecord) p0).setChecked(p1);
        return;
    }

    public static void setClassName(Object p0, CharSequence p1)
    {
        ((android.view.accessibility.AccessibilityRecord) p0).setClassName(p1);
        return;
    }

    public static void setContentDescription(Object p0, CharSequence p1)
    {
        ((android.view.accessibility.AccessibilityRecord) p0).setContentDescription(p1);
        return;
    }

    public static void setCurrentItemIndex(Object p0, int p1)
    {
        ((android.view.accessibility.AccessibilityRecord) p0).setCurrentItemIndex(p1);
        return;
    }

    public static void setEnabled(Object p0, boolean p1)
    {
        ((android.view.accessibility.AccessibilityRecord) p0).setEnabled(p1);
        return;
    }

    public static void setFromIndex(Object p0, int p1)
    {
        ((android.view.accessibility.AccessibilityRecord) p0).setFromIndex(p1);
        return;
    }

    public static void setFullScreen(Object p0, boolean p1)
    {
        ((android.view.accessibility.AccessibilityRecord) p0).setFullScreen(p1);
        return;
    }

    public static void setItemCount(Object p0, int p1)
    {
        ((android.view.accessibility.AccessibilityRecord) p0).setItemCount(p1);
        return;
    }

    public static void setParcelableData(Object p0, android.os.Parcelable p1)
    {
        ((android.view.accessibility.AccessibilityRecord) p0).setParcelableData(p1);
        return;
    }

    public static void setPassword(Object p0, boolean p1)
    {
        ((android.view.accessibility.AccessibilityRecord) p0).setPassword(p1);
        return;
    }

    public static void setRemovedCount(Object p0, int p1)
    {
        ((android.view.accessibility.AccessibilityRecord) p0).setRemovedCount(p1);
        return;
    }

    public static void setScrollX(Object p0, int p1)
    {
        ((android.view.accessibility.AccessibilityRecord) p0).setScrollX(p1);
        return;
    }

    public static void setScrollY(Object p0, int p1)
    {
        ((android.view.accessibility.AccessibilityRecord) p0).setScrollY(p1);
        return;
    }

    public static void setScrollable(Object p0, boolean p1)
    {
        ((android.view.accessibility.AccessibilityRecord) p0).setScrollable(p1);
        return;
    }

    public static void setSource(Object p0, android.view.View p1)
    {
        ((android.view.accessibility.AccessibilityRecord) p0).setSource(p1);
        return;
    }

    public static void setToIndex(Object p0, int p1)
    {
        ((android.view.accessibility.AccessibilityRecord) p0).setToIndex(p1);
        return;
    }
}
