package android.support.v4.view.accessibility;
 class AccessibilityNodeInfoCompatJellybeanMr2 {

    AccessibilityNodeInfoCompatJellybeanMr2()
    {
        return;
    }

    public static String getViewIdResourceName(Object p1)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo) p1).getViewIdResourceName();
    }

    public static void setViewIdResourceName(Object p0, String p1)
    {
        ((android.view.accessibility.AccessibilityNodeInfo) p0).setViewIdResourceName(p1);
        return;
    }
}
