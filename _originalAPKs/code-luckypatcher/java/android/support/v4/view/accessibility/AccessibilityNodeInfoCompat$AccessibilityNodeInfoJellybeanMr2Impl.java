package android.support.v4.view.accessibility;
 class AccessibilityNodeInfoCompat$AccessibilityNodeInfoJellybeanMr2Impl extends android.support.v4.view.accessibility.AccessibilityNodeInfoCompat$AccessibilityNodeInfoJellybeanImpl {

    AccessibilityNodeInfoCompat$AccessibilityNodeInfoJellybeanMr2Impl()
    {
        return;
    }

    public String getViewIdResourceName(Object p2)
    {
        return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatJellybeanMr2.getViewIdResourceName(p2);
    }

    public void setViewIdResourceName(Object p1, String p2)
    {
        android.support.v4.view.accessibility.AccessibilityNodeInfoCompatJellybeanMr2.setViewIdResourceName(p1, p2);
        return;
    }
}
