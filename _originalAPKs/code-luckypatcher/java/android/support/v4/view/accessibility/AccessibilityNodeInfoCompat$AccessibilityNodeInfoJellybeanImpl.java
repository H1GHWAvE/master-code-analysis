package android.support.v4.view.accessibility;
 class AccessibilityNodeInfoCompat$AccessibilityNodeInfoJellybeanImpl extends android.support.v4.view.accessibility.AccessibilityNodeInfoCompat$AccessibilityNodeInfoIcsImpl {

    AccessibilityNodeInfoCompat$AccessibilityNodeInfoJellybeanImpl()
    {
        return;
    }

    public void addChild(Object p1, android.view.View p2, int p3)
    {
        android.support.v4.view.accessibility.AccessibilityNodeInfoCompatJellyBean.addChild(p1, p2, p3);
        return;
    }

    public Object findFocus(Object p2, int p3)
    {
        return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatJellyBean.findFocus(p2, p3);
    }

    public Object focusSearch(Object p2, int p3)
    {
        return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatJellyBean.focusSearch(p2, p3);
    }

    public int getMovementGranularities(Object p2)
    {
        return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatJellyBean.getMovementGranularities(p2);
    }

    public boolean isAccessibilityFocused(Object p2)
    {
        return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatJellyBean.isAccessibilityFocused(p2);
    }

    public boolean isVisibleToUser(Object p2)
    {
        return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatJellyBean.isVisibleToUser(p2);
    }

    public Object obtain(android.view.View p2, int p3)
    {
        return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatJellyBean.obtain(p2, p3);
    }

    public boolean performAction(Object p2, int p3, android.os.Bundle p4)
    {
        return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatJellyBean.performAction(p2, p3, p4);
    }

    public void setAccessibilityFocused(Object p1, boolean p2)
    {
        android.support.v4.view.accessibility.AccessibilityNodeInfoCompatJellyBean.setAccesibilityFocused(p1, p2);
        return;
    }

    public void setMovementGranularities(Object p1, int p2)
    {
        android.support.v4.view.accessibility.AccessibilityNodeInfoCompatJellyBean.setMovementGranularities(p1, p2);
        return;
    }

    public void setParent(Object p1, android.view.View p2, int p3)
    {
        android.support.v4.view.accessibility.AccessibilityNodeInfoCompatJellyBean.setParent(p1, p2, p3);
        return;
    }

    public void setSource(Object p1, android.view.View p2, int p3)
    {
        android.support.v4.view.accessibility.AccessibilityNodeInfoCompatJellyBean.setSource(p1, p2, p3);
        return;
    }

    public void setVisibleToUser(Object p1, boolean p2)
    {
        android.support.v4.view.accessibility.AccessibilityNodeInfoCompatJellyBean.setVisibleToUser(p1, p2);
        return;
    }
}
