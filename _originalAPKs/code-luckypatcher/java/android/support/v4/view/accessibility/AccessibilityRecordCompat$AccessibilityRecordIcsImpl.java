package android.support.v4.view.accessibility;
 class AccessibilityRecordCompat$AccessibilityRecordIcsImpl extends android.support.v4.view.accessibility.AccessibilityRecordCompat$AccessibilityRecordStubImpl {

    AccessibilityRecordCompat$AccessibilityRecordIcsImpl()
    {
        return;
    }

    public int getAddedCount(Object p2)
    {
        return android.support.v4.view.accessibility.AccessibilityRecordCompatIcs.getAddedCount(p2);
    }

    public CharSequence getBeforeText(Object p2)
    {
        return android.support.v4.view.accessibility.AccessibilityRecordCompatIcs.getBeforeText(p2);
    }

    public CharSequence getClassName(Object p2)
    {
        return android.support.v4.view.accessibility.AccessibilityRecordCompatIcs.getClassName(p2);
    }

    public CharSequence getContentDescription(Object p2)
    {
        return android.support.v4.view.accessibility.AccessibilityRecordCompatIcs.getContentDescription(p2);
    }

    public int getCurrentItemIndex(Object p2)
    {
        return android.support.v4.view.accessibility.AccessibilityRecordCompatIcs.getCurrentItemIndex(p2);
    }

    public int getFromIndex(Object p2)
    {
        return android.support.v4.view.accessibility.AccessibilityRecordCompatIcs.getFromIndex(p2);
    }

    public int getItemCount(Object p2)
    {
        return android.support.v4.view.accessibility.AccessibilityRecordCompatIcs.getItemCount(p2);
    }

    public android.os.Parcelable getParcelableData(Object p2)
    {
        return android.support.v4.view.accessibility.AccessibilityRecordCompatIcs.getParcelableData(p2);
    }

    public int getRemovedCount(Object p2)
    {
        return android.support.v4.view.accessibility.AccessibilityRecordCompatIcs.getRemovedCount(p2);
    }

    public int getScrollX(Object p2)
    {
        return android.support.v4.view.accessibility.AccessibilityRecordCompatIcs.getScrollX(p2);
    }

    public int getScrollY(Object p2)
    {
        return android.support.v4.view.accessibility.AccessibilityRecordCompatIcs.getScrollY(p2);
    }

    public android.support.v4.view.accessibility.AccessibilityNodeInfoCompat getSource(Object p2)
    {
        return android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.wrapNonNullInstance(android.support.v4.view.accessibility.AccessibilityRecordCompatIcs.getSource(p2));
    }

    public java.util.List getText(Object p2)
    {
        return android.support.v4.view.accessibility.AccessibilityRecordCompatIcs.getText(p2);
    }

    public int getToIndex(Object p2)
    {
        return android.support.v4.view.accessibility.AccessibilityRecordCompatIcs.getToIndex(p2);
    }

    public int getWindowId(Object p2)
    {
        return android.support.v4.view.accessibility.AccessibilityRecordCompatIcs.getWindowId(p2);
    }

    public boolean isChecked(Object p2)
    {
        return android.support.v4.view.accessibility.AccessibilityRecordCompatIcs.isChecked(p2);
    }

    public boolean isEnabled(Object p2)
    {
        return android.support.v4.view.accessibility.AccessibilityRecordCompatIcs.isEnabled(p2);
    }

    public boolean isFullScreen(Object p2)
    {
        return android.support.v4.view.accessibility.AccessibilityRecordCompatIcs.isFullScreen(p2);
    }

    public boolean isPassword(Object p2)
    {
        return android.support.v4.view.accessibility.AccessibilityRecordCompatIcs.isPassword(p2);
    }

    public boolean isScrollable(Object p2)
    {
        return android.support.v4.view.accessibility.AccessibilityRecordCompatIcs.isScrollable(p2);
    }

    public Object obtain()
    {
        return android.support.v4.view.accessibility.AccessibilityRecordCompatIcs.obtain();
    }

    public Object obtain(Object p2)
    {
        return android.support.v4.view.accessibility.AccessibilityRecordCompatIcs.obtain(p2);
    }

    public void recycle(Object p1)
    {
        android.support.v4.view.accessibility.AccessibilityRecordCompatIcs.recycle(p1);
        return;
    }

    public void setAddedCount(Object p1, int p2)
    {
        android.support.v4.view.accessibility.AccessibilityRecordCompatIcs.setAddedCount(p1, p2);
        return;
    }

    public void setBeforeText(Object p1, CharSequence p2)
    {
        android.support.v4.view.accessibility.AccessibilityRecordCompatIcs.setBeforeText(p1, p2);
        return;
    }

    public void setChecked(Object p1, boolean p2)
    {
        android.support.v4.view.accessibility.AccessibilityRecordCompatIcs.setChecked(p1, p2);
        return;
    }

    public void setClassName(Object p1, CharSequence p2)
    {
        android.support.v4.view.accessibility.AccessibilityRecordCompatIcs.setClassName(p1, p2);
        return;
    }

    public void setContentDescription(Object p1, CharSequence p2)
    {
        android.support.v4.view.accessibility.AccessibilityRecordCompatIcs.setContentDescription(p1, p2);
        return;
    }

    public void setCurrentItemIndex(Object p1, int p2)
    {
        android.support.v4.view.accessibility.AccessibilityRecordCompatIcs.setCurrentItemIndex(p1, p2);
        return;
    }

    public void setEnabled(Object p1, boolean p2)
    {
        android.support.v4.view.accessibility.AccessibilityRecordCompatIcs.setEnabled(p1, p2);
        return;
    }

    public void setFromIndex(Object p1, int p2)
    {
        android.support.v4.view.accessibility.AccessibilityRecordCompatIcs.setFromIndex(p1, p2);
        return;
    }

    public void setFullScreen(Object p1, boolean p2)
    {
        android.support.v4.view.accessibility.AccessibilityRecordCompatIcs.setFullScreen(p1, p2);
        return;
    }

    public void setItemCount(Object p1, int p2)
    {
        android.support.v4.view.accessibility.AccessibilityRecordCompatIcs.setItemCount(p1, p2);
        return;
    }

    public void setParcelableData(Object p1, android.os.Parcelable p2)
    {
        android.support.v4.view.accessibility.AccessibilityRecordCompatIcs.setParcelableData(p1, p2);
        return;
    }

    public void setPassword(Object p1, boolean p2)
    {
        android.support.v4.view.accessibility.AccessibilityRecordCompatIcs.setPassword(p1, p2);
        return;
    }

    public void setRemovedCount(Object p1, int p2)
    {
        android.support.v4.view.accessibility.AccessibilityRecordCompatIcs.setRemovedCount(p1, p2);
        return;
    }

    public void setScrollX(Object p1, int p2)
    {
        android.support.v4.view.accessibility.AccessibilityRecordCompatIcs.setScrollX(p1, p2);
        return;
    }

    public void setScrollY(Object p1, int p2)
    {
        android.support.v4.view.accessibility.AccessibilityRecordCompatIcs.setScrollY(p1, p2);
        return;
    }

    public void setScrollable(Object p1, boolean p2)
    {
        android.support.v4.view.accessibility.AccessibilityRecordCompatIcs.setScrollable(p1, p2);
        return;
    }

    public void setSource(Object p1, android.view.View p2)
    {
        android.support.v4.view.accessibility.AccessibilityRecordCompatIcs.setSource(p1, p2);
        return;
    }

    public void setToIndex(Object p1, int p2)
    {
        android.support.v4.view.accessibility.AccessibilityRecordCompatIcs.setToIndex(p1, p2);
        return;
    }
}
