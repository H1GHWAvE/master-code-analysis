package android.support.v4.view.accessibility;
 class AccessibilityNodeInfoCompat$AccessibilityNodeInfoIcsImpl extends android.support.v4.view.accessibility.AccessibilityNodeInfoCompat$AccessibilityNodeInfoStubImpl {

    AccessibilityNodeInfoCompat$AccessibilityNodeInfoIcsImpl()
    {
        return;
    }

    public void addAction(Object p1, int p2)
    {
        android.support.v4.view.accessibility.AccessibilityNodeInfoCompatIcs.addAction(p1, p2);
        return;
    }

    public void addChild(Object p1, android.view.View p2)
    {
        android.support.v4.view.accessibility.AccessibilityNodeInfoCompatIcs.addChild(p1, p2);
        return;
    }

    public java.util.List findAccessibilityNodeInfosByText(Object p2, String p3)
    {
        return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatIcs.findAccessibilityNodeInfosByText(p2, p3);
    }

    public int getActions(Object p2)
    {
        return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatIcs.getActions(p2);
    }

    public void getBoundsInParent(Object p1, android.graphics.Rect p2)
    {
        android.support.v4.view.accessibility.AccessibilityNodeInfoCompatIcs.getBoundsInParent(p1, p2);
        return;
    }

    public void getBoundsInScreen(Object p1, android.graphics.Rect p2)
    {
        android.support.v4.view.accessibility.AccessibilityNodeInfoCompatIcs.getBoundsInScreen(p1, p2);
        return;
    }

    public Object getChild(Object p2, int p3)
    {
        return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatIcs.getChild(p2, p3);
    }

    public int getChildCount(Object p2)
    {
        return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatIcs.getChildCount(p2);
    }

    public CharSequence getClassName(Object p2)
    {
        return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatIcs.getClassName(p2);
    }

    public CharSequence getContentDescription(Object p2)
    {
        return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatIcs.getContentDescription(p2);
    }

    public CharSequence getPackageName(Object p2)
    {
        return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatIcs.getPackageName(p2);
    }

    public Object getParent(Object p2)
    {
        return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatIcs.getParent(p2);
    }

    public CharSequence getText(Object p2)
    {
        return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatIcs.getText(p2);
    }

    public int getWindowId(Object p2)
    {
        return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatIcs.getWindowId(p2);
    }

    public boolean isCheckable(Object p2)
    {
        return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatIcs.isCheckable(p2);
    }

    public boolean isChecked(Object p2)
    {
        return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatIcs.isChecked(p2);
    }

    public boolean isClickable(Object p2)
    {
        return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatIcs.isClickable(p2);
    }

    public boolean isEnabled(Object p2)
    {
        return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatIcs.isEnabled(p2);
    }

    public boolean isFocusable(Object p2)
    {
        return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatIcs.isFocusable(p2);
    }

    public boolean isFocused(Object p2)
    {
        return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatIcs.isFocused(p2);
    }

    public boolean isLongClickable(Object p2)
    {
        return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatIcs.isLongClickable(p2);
    }

    public boolean isPassword(Object p2)
    {
        return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatIcs.isPassword(p2);
    }

    public boolean isScrollable(Object p2)
    {
        return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatIcs.isScrollable(p2);
    }

    public boolean isSelected(Object p2)
    {
        return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatIcs.isSelected(p2);
    }

    public Object obtain()
    {
        return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatIcs.obtain();
    }

    public Object obtain(android.view.View p2)
    {
        return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatIcs.obtain(p2);
    }

    public Object obtain(Object p2)
    {
        return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatIcs.obtain(p2);
    }

    public boolean performAction(Object p2, int p3)
    {
        return android.support.v4.view.accessibility.AccessibilityNodeInfoCompatIcs.performAction(p2, p3);
    }

    public void recycle(Object p1)
    {
        android.support.v4.view.accessibility.AccessibilityNodeInfoCompatIcs.recycle(p1);
        return;
    }

    public void setBoundsInParent(Object p1, android.graphics.Rect p2)
    {
        android.support.v4.view.accessibility.AccessibilityNodeInfoCompatIcs.setBoundsInParent(p1, p2);
        return;
    }

    public void setBoundsInScreen(Object p1, android.graphics.Rect p2)
    {
        android.support.v4.view.accessibility.AccessibilityNodeInfoCompatIcs.setBoundsInScreen(p1, p2);
        return;
    }

    public void setCheckable(Object p1, boolean p2)
    {
        android.support.v4.view.accessibility.AccessibilityNodeInfoCompatIcs.setCheckable(p1, p2);
        return;
    }

    public void setChecked(Object p1, boolean p2)
    {
        android.support.v4.view.accessibility.AccessibilityNodeInfoCompatIcs.setChecked(p1, p2);
        return;
    }

    public void setClassName(Object p1, CharSequence p2)
    {
        android.support.v4.view.accessibility.AccessibilityNodeInfoCompatIcs.setClassName(p1, p2);
        return;
    }

    public void setClickable(Object p1, boolean p2)
    {
        android.support.v4.view.accessibility.AccessibilityNodeInfoCompatIcs.setClickable(p1, p2);
        return;
    }

    public void setContentDescription(Object p1, CharSequence p2)
    {
        android.support.v4.view.accessibility.AccessibilityNodeInfoCompatIcs.setContentDescription(p1, p2);
        return;
    }

    public void setEnabled(Object p1, boolean p2)
    {
        android.support.v4.view.accessibility.AccessibilityNodeInfoCompatIcs.setEnabled(p1, p2);
        return;
    }

    public void setFocusable(Object p1, boolean p2)
    {
        android.support.v4.view.accessibility.AccessibilityNodeInfoCompatIcs.setFocusable(p1, p2);
        return;
    }

    public void setFocused(Object p1, boolean p2)
    {
        android.support.v4.view.accessibility.AccessibilityNodeInfoCompatIcs.setFocused(p1, p2);
        return;
    }

    public void setLongClickable(Object p1, boolean p2)
    {
        android.support.v4.view.accessibility.AccessibilityNodeInfoCompatIcs.setLongClickable(p1, p2);
        return;
    }

    public void setPackageName(Object p1, CharSequence p2)
    {
        android.support.v4.view.accessibility.AccessibilityNodeInfoCompatIcs.setPackageName(p1, p2);
        return;
    }

    public void setParent(Object p1, android.view.View p2)
    {
        android.support.v4.view.accessibility.AccessibilityNodeInfoCompatIcs.setParent(p1, p2);
        return;
    }

    public void setPassword(Object p1, boolean p2)
    {
        android.support.v4.view.accessibility.AccessibilityNodeInfoCompatIcs.setPassword(p1, p2);
        return;
    }

    public void setScrollable(Object p1, boolean p2)
    {
        android.support.v4.view.accessibility.AccessibilityNodeInfoCompatIcs.setScrollable(p1, p2);
        return;
    }

    public void setSelected(Object p1, boolean p2)
    {
        android.support.v4.view.accessibility.AccessibilityNodeInfoCompatIcs.setSelected(p1, p2);
        return;
    }

    public void setSource(Object p1, android.view.View p2)
    {
        android.support.v4.view.accessibility.AccessibilityNodeInfoCompatIcs.setSource(p1, p2);
        return;
    }

    public void setText(Object p1, CharSequence p2)
    {
        android.support.v4.view.accessibility.AccessibilityNodeInfoCompatIcs.setText(p1, p2);
        return;
    }
}
