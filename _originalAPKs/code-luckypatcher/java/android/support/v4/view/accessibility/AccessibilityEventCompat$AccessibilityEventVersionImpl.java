package android.support.v4.view.accessibility;
interface AccessibilityEventCompat$AccessibilityEventVersionImpl {

    public abstract void appendRecord();

    public abstract Object getRecord();

    public abstract int getRecordCount();
}
