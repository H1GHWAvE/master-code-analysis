package android.support.v4.view.accessibility;
 class AccessibilityNodeInfoCompat$AccessibilityNodeInfoStubImpl implements android.support.v4.view.accessibility.AccessibilityNodeInfoCompat$AccessibilityNodeInfoImpl {

    AccessibilityNodeInfoCompat$AccessibilityNodeInfoStubImpl()
    {
        return;
    }

    public void addAction(Object p1, int p2)
    {
        return;
    }

    public void addChild(Object p1, android.view.View p2)
    {
        return;
    }

    public void addChild(Object p1, android.view.View p2, int p3)
    {
        return;
    }

    public java.util.List findAccessibilityNodeInfosByText(Object p2, String p3)
    {
        return java.util.Collections.emptyList();
    }

    public Object findFocus(Object p2, int p3)
    {
        return 0;
    }

    public Object focusSearch(Object p2, int p3)
    {
        return 0;
    }

    public int getActions(Object p2)
    {
        return 0;
    }

    public void getBoundsInParent(Object p1, android.graphics.Rect p2)
    {
        return;
    }

    public void getBoundsInScreen(Object p1, android.graphics.Rect p2)
    {
        return;
    }

    public Object getChild(Object p2, int p3)
    {
        return 0;
    }

    public int getChildCount(Object p2)
    {
        return 0;
    }

    public CharSequence getClassName(Object p2)
    {
        return 0;
    }

    public CharSequence getContentDescription(Object p2)
    {
        return 0;
    }

    public int getMovementGranularities(Object p2)
    {
        return 0;
    }

    public CharSequence getPackageName(Object p2)
    {
        return 0;
    }

    public Object getParent(Object p2)
    {
        return 0;
    }

    public CharSequence getText(Object p2)
    {
        return 0;
    }

    public String getViewIdResourceName(Object p2)
    {
        return 0;
    }

    public int getWindowId(Object p2)
    {
        return 0;
    }

    public boolean isAccessibilityFocused(Object p2)
    {
        return 0;
    }

    public boolean isCheckable(Object p2)
    {
        return 0;
    }

    public boolean isChecked(Object p2)
    {
        return 0;
    }

    public boolean isClickable(Object p2)
    {
        return 0;
    }

    public boolean isEnabled(Object p2)
    {
        return 0;
    }

    public boolean isFocusable(Object p2)
    {
        return 0;
    }

    public boolean isFocused(Object p2)
    {
        return 0;
    }

    public boolean isLongClickable(Object p2)
    {
        return 0;
    }

    public boolean isPassword(Object p2)
    {
        return 0;
    }

    public boolean isScrollable(Object p2)
    {
        return 0;
    }

    public boolean isSelected(Object p2)
    {
        return 0;
    }

    public boolean isVisibleToUser(Object p2)
    {
        return 0;
    }

    public Object obtain()
    {
        return 0;
    }

    public Object obtain(android.view.View p2)
    {
        return 0;
    }

    public Object obtain(android.view.View p2, int p3)
    {
        return 0;
    }

    public Object obtain(Object p2)
    {
        return 0;
    }

    public boolean performAction(Object p2, int p3)
    {
        return 0;
    }

    public boolean performAction(Object p2, int p3, android.os.Bundle p4)
    {
        return 0;
    }

    public void recycle(Object p1)
    {
        return;
    }

    public void setAccessibilityFocused(Object p1, boolean p2)
    {
        return;
    }

    public void setBoundsInParent(Object p1, android.graphics.Rect p2)
    {
        return;
    }

    public void setBoundsInScreen(Object p1, android.graphics.Rect p2)
    {
        return;
    }

    public void setCheckable(Object p1, boolean p2)
    {
        return;
    }

    public void setChecked(Object p1, boolean p2)
    {
        return;
    }

    public void setClassName(Object p1, CharSequence p2)
    {
        return;
    }

    public void setClickable(Object p1, boolean p2)
    {
        return;
    }

    public void setContentDescription(Object p1, CharSequence p2)
    {
        return;
    }

    public void setEnabled(Object p1, boolean p2)
    {
        return;
    }

    public void setFocusable(Object p1, boolean p2)
    {
        return;
    }

    public void setFocused(Object p1, boolean p2)
    {
        return;
    }

    public void setLongClickable(Object p1, boolean p2)
    {
        return;
    }

    public void setMovementGranularities(Object p1, int p2)
    {
        return;
    }

    public void setPackageName(Object p1, CharSequence p2)
    {
        return;
    }

    public void setParent(Object p1, android.view.View p2)
    {
        return;
    }

    public void setParent(Object p1, android.view.View p2, int p3)
    {
        return;
    }

    public void setPassword(Object p1, boolean p2)
    {
        return;
    }

    public void setScrollable(Object p1, boolean p2)
    {
        return;
    }

    public void setSelected(Object p1, boolean p2)
    {
        return;
    }

    public void setSource(Object p1, android.view.View p2)
    {
        return;
    }

    public void setSource(Object p1, android.view.View p2, int p3)
    {
        return;
    }

    public void setText(Object p1, CharSequence p2)
    {
        return;
    }

    public void setViewIdResourceName(Object p1, String p2)
    {
        return;
    }

    public void setVisibleToUser(Object p1, boolean p2)
    {
        return;
    }
}
