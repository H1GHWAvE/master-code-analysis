package android.support.v4.view.accessibility;
interface AccessibilityRecordCompat$AccessibilityRecordImpl {

    public abstract int getAddedCount();

    public abstract CharSequence getBeforeText();

    public abstract CharSequence getClassName();

    public abstract CharSequence getContentDescription();

    public abstract int getCurrentItemIndex();

    public abstract int getFromIndex();

    public abstract int getItemCount();

    public abstract int getMaxScrollX();

    public abstract int getMaxScrollY();

    public abstract android.os.Parcelable getParcelableData();

    public abstract int getRemovedCount();

    public abstract int getScrollX();

    public abstract int getScrollY();

    public abstract android.support.v4.view.accessibility.AccessibilityNodeInfoCompat getSource();

    public abstract java.util.List getText();

    public abstract int getToIndex();

    public abstract int getWindowId();

    public abstract boolean isChecked();

    public abstract boolean isEnabled();

    public abstract boolean isFullScreen();

    public abstract boolean isPassword();

    public abstract boolean isScrollable();

    public abstract Object obtain();

    public abstract Object obtain();

    public abstract void recycle();

    public abstract void setAddedCount();

    public abstract void setBeforeText();

    public abstract void setChecked();

    public abstract void setClassName();

    public abstract void setContentDescription();

    public abstract void setCurrentItemIndex();

    public abstract void setEnabled();

    public abstract void setFromIndex();

    public abstract void setFullScreen();

    public abstract void setItemCount();

    public abstract void setMaxScrollX();

    public abstract void setMaxScrollY();

    public abstract void setParcelableData();

    public abstract void setPassword();

    public abstract void setRemovedCount();

    public abstract void setScrollX();

    public abstract void setScrollY();

    public abstract void setScrollable();

    public abstract void setSource();

    public abstract void setSource();

    public abstract void setToIndex();
}
