package android.support.v4.view.accessibility;
 class AccessibilityManagerCompat$AccessibilityManagerStubImpl implements android.support.v4.view.accessibility.AccessibilityManagerCompat$AccessibilityManagerVersionImpl {

    AccessibilityManagerCompat$AccessibilityManagerStubImpl()
    {
        return;
    }

    public boolean addAccessibilityStateChangeListener(android.view.accessibility.AccessibilityManager p2, android.support.v4.view.accessibility.AccessibilityManagerCompat$AccessibilityStateChangeListenerCompat p3)
    {
        return 0;
    }

    public java.util.List getEnabledAccessibilityServiceList(android.view.accessibility.AccessibilityManager p2, int p3)
    {
        return java.util.Collections.emptyList();
    }

    public java.util.List getInstalledAccessibilityServiceList(android.view.accessibility.AccessibilityManager p2)
    {
        return java.util.Collections.emptyList();
    }

    public boolean isTouchExplorationEnabled(android.view.accessibility.AccessibilityManager p2)
    {
        return 0;
    }

    public Object newAccessiblityStateChangeListener(android.support.v4.view.accessibility.AccessibilityManagerCompat$AccessibilityStateChangeListenerCompat p2)
    {
        return 0;
    }

    public boolean removeAccessibilityStateChangeListener(android.view.accessibility.AccessibilityManager p2, android.support.v4.view.accessibility.AccessibilityManagerCompat$AccessibilityStateChangeListenerCompat p3)
    {
        return 0;
    }
}
