package android.support.v4.view;
 class ViewConfigurationCompat$FroyoViewConfigurationVersionImpl implements android.support.v4.view.ViewConfigurationCompat$ViewConfigurationVersionImpl {

    ViewConfigurationCompat$FroyoViewConfigurationVersionImpl()
    {
        return;
    }

    public int getScaledPagingTouchSlop(android.view.ViewConfiguration p2)
    {
        return android.support.v4.view.ViewConfigurationCompatFroyo.getScaledPagingTouchSlop(p2);
    }
}
