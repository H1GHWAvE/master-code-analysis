package android.support.v4.view;
interface GravityCompat$GravityCompatImpl {

    public abstract void apply();

    public abstract void apply();

    public abstract void applyDisplay();

    public abstract int getAbsoluteGravity();
}
