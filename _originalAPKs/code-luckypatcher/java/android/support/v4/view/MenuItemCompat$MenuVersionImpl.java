package android.support.v4.view;
interface MenuItemCompat$MenuVersionImpl {

    public abstract boolean collapseActionView();

    public abstract boolean expandActionView();

    public abstract android.view.View getActionView();

    public abstract boolean isActionViewExpanded();

    public abstract android.view.MenuItem setActionView();

    public abstract android.view.MenuItem setActionView();

    public abstract android.view.MenuItem setOnActionExpandListener();

    public abstract void setShowAsAction();
}
