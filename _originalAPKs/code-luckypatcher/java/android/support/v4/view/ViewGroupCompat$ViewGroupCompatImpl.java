package android.support.v4.view;
interface ViewGroupCompat$ViewGroupCompatImpl {

    public abstract boolean onRequestSendAccessibilityEvent();

    public abstract void setMotionEventSplittingEnabled();
}
