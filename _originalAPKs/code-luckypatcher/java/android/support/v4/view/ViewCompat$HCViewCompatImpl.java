package android.support.v4.view;
 class ViewCompat$HCViewCompatImpl extends android.support.v4.view.ViewCompat$GBViewCompatImpl {

    ViewCompat$HCViewCompatImpl()
    {
        return;
    }

    public float getAlpha(android.view.View p2)
    {
        return android.support.v4.view.ViewCompatHC.getAlpha(p2);
    }

    long getFrameTime()
    {
        return android.support.v4.view.ViewCompatHC.getFrameTime();
    }

    public int getLayerType(android.view.View p2)
    {
        return android.support.v4.view.ViewCompatHC.getLayerType(p2);
    }

    public int resolveSizeAndState(int p2, int p3, int p4)
    {
        return android.support.v4.view.ViewCompatHC.resolveSizeAndState(p2, p3, p4);
    }

    public void setLayerPaint(android.view.View p2, android.graphics.Paint p3)
    {
        this.setLayerType(p2, this.getLayerType(p2), p3);
        p2.invalidate();
        return;
    }

    public void setLayerType(android.view.View p1, int p2, android.graphics.Paint p3)
    {
        android.support.v4.view.ViewCompatHC.setLayerType(p1, p2, p3);
        return;
    }
}
