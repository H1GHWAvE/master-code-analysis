package android.support.v4.view;
 class MarginLayoutParamsCompat$MarginLayoutParamsCompatImplJbMr1 implements android.support.v4.view.MarginLayoutParamsCompat$MarginLayoutParamsCompatImpl {

    MarginLayoutParamsCompat$MarginLayoutParamsCompatImplJbMr1()
    {
        return;
    }

    public int getLayoutDirection(android.view.ViewGroup$MarginLayoutParams p2)
    {
        return android.support.v4.view.MarginLayoutParamsCompatJellybeanMr1.getLayoutDirection(p2);
    }

    public int getMarginEnd(android.view.ViewGroup$MarginLayoutParams p2)
    {
        return android.support.v4.view.MarginLayoutParamsCompatJellybeanMr1.getMarginEnd(p2);
    }

    public int getMarginStart(android.view.ViewGroup$MarginLayoutParams p2)
    {
        return android.support.v4.view.MarginLayoutParamsCompatJellybeanMr1.getMarginStart(p2);
    }

    public boolean isMarginRelative(android.view.ViewGroup$MarginLayoutParams p2)
    {
        return android.support.v4.view.MarginLayoutParamsCompatJellybeanMr1.isMarginRelative(p2);
    }

    public void resolveLayoutDirection(android.view.ViewGroup$MarginLayoutParams p1, int p2)
    {
        android.support.v4.view.MarginLayoutParamsCompatJellybeanMr1.resolveLayoutDirection(p1, p2);
        return;
    }

    public void setLayoutDirection(android.view.ViewGroup$MarginLayoutParams p1, int p2)
    {
        android.support.v4.view.MarginLayoutParamsCompatJellybeanMr1.setLayoutDirection(p1, p2);
        return;
    }

    public void setMarginEnd(android.view.ViewGroup$MarginLayoutParams p1, int p2)
    {
        android.support.v4.view.MarginLayoutParamsCompatJellybeanMr1.setMarginEnd(p1, p2);
        return;
    }

    public void setMarginStart(android.view.ViewGroup$MarginLayoutParams p1, int p2)
    {
        android.support.v4.view.MarginLayoutParamsCompatJellybeanMr1.setMarginStart(p1, p2);
        return;
    }
}
