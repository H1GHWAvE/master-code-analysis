package android.support.v4.view;
 class ViewGroupCompatHC {

    private ViewGroupCompatHC()
    {
        return;
    }

    public static void setMotionEventSplittingEnabled(android.view.ViewGroup p0, boolean p1)
    {
        p0.setMotionEventSplittingEnabled(p1);
        return;
    }
}
