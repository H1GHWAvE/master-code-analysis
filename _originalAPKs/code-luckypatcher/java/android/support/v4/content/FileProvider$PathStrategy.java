package android.support.v4.content;
interface FileProvider$PathStrategy {

    public abstract java.io.File getFileForUri();

    public abstract android.net.Uri getUriForFile();
}
