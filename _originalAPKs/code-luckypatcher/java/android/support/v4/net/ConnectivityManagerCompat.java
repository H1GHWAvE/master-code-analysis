package android.support.v4.net;
public class ConnectivityManagerCompat {
    private static final android.support.v4.net.ConnectivityManagerCompat$ConnectivityManagerCompatImpl IMPL;

    static ConnectivityManagerCompat()
    {
        if (android.os.Build$VERSION.SDK_INT < 16) {
            if (android.os.Build$VERSION.SDK_INT < 13) {
                if (android.os.Build$VERSION.SDK_INT < 8) {
                    android.support.v4.net.ConnectivityManagerCompat.IMPL = new android.support.v4.net.ConnectivityManagerCompat$BaseConnectivityManagerCompatImpl();
                } else {
                    android.support.v4.net.ConnectivityManagerCompat.IMPL = new android.support.v4.net.ConnectivityManagerCompat$GingerbreadConnectivityManagerCompatImpl();
                }
            } else {
                android.support.v4.net.ConnectivityManagerCompat.IMPL = new android.support.v4.net.ConnectivityManagerCompat$HoneycombMR2ConnectivityManagerCompatImpl();
            }
        } else {
            android.support.v4.net.ConnectivityManagerCompat.IMPL = new android.support.v4.net.ConnectivityManagerCompat$JellyBeanConnectivityManagerCompatImpl();
        }
        return;
    }

    public ConnectivityManagerCompat()
    {
        return;
    }

    public static android.net.NetworkInfo getNetworkInfoFromBroadcast(android.net.ConnectivityManager p2, android.content.Intent p3)
    {
        return p2.getNetworkInfo(((android.net.NetworkInfo) p3.getParcelableExtra("networkInfo")).getType());
    }

    public static boolean isActiveNetworkMetered(android.net.ConnectivityManager p1)
    {
        return android.support.v4.net.ConnectivityManagerCompat.IMPL.isActiveNetworkMetered(p1);
    }
}
