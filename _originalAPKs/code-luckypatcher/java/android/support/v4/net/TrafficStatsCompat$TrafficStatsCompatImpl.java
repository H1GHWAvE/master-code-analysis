package android.support.v4.net;
interface TrafficStatsCompat$TrafficStatsCompatImpl {

    public abstract void clearThreadStatsTag();

    public abstract int getThreadStatsTag();

    public abstract void incrementOperationCount();

    public abstract void incrementOperationCount();

    public abstract void setThreadStatsTag();

    public abstract void tagSocket();

    public abstract void untagSocket();
}
