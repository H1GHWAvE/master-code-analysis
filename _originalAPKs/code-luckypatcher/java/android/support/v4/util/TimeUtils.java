package android.support.v4.util;
public class TimeUtils {
    public static final int HUNDRED_DAY_FIELD_LEN = 19;
    private static final int SECONDS_PER_DAY = 86400;
    private static final int SECONDS_PER_HOUR = 3600;
    private static final int SECONDS_PER_MINUTE = 60;
    private static char[] sFormatStr;
    private static final Object sFormatSync;

    static TimeUtils()
    {
        android.support.v4.util.TimeUtils.sFormatSync = new Object();
        char[] v0_3 = new char[24];
        android.support.v4.util.TimeUtils.sFormatStr = v0_3;
        return;
    }

    public TimeUtils()
    {
        return;
    }

    private static int accumField(int p1, int p2, boolean p3, int p4)
    {
        if ((p1 <= 99) && ((!p3) || (p4 < 3))) {
            if ((p1 <= 9) && ((!p3) || (p4 < 2))) {
                if ((!p3) && (p1 <= 0)) {
                    int v0_4 = 0;
                } else {
                    v0_4 = (p2 + 1);
                }
            } else {
                v0_4 = (p2 + 2);
            }
        } else {
            v0_4 = (p2 + 3);
        }
        return v0_4;
    }

    public static void formatDuration(long p3, long p5, java.io.PrintWriter p7)
    {
        if (p3 != 0) {
            android.support.v4.util.TimeUtils.formatDuration((p3 - p5), p7, 0);
        } else {
            p7.print("--");
        }
        return;
    }

    public static void formatDuration(long p1, java.io.PrintWriter p3)
    {
        android.support.v4.util.TimeUtils.formatDuration(p1, p3, 0);
        return;
    }

    public static void formatDuration(long p5, java.io.PrintWriter p7, int p8)
    {
        try {
            p7.print(new String(android.support.v4.util.TimeUtils.sFormatStr, 0, android.support.v4.util.TimeUtils.formatDurationLocked(p5, p8)));
            return;
        } catch (Throwable v1_2) {
            throw v1_2;
        }
    }

    public static void formatDuration(long p4, StringBuilder p6)
    {
        try {
            p6.append(android.support.v4.util.TimeUtils.sFormatStr, 0, android.support.v4.util.TimeUtils.formatDurationLocked(p4, 0));
            return;
        } catch (Throwable v1_2) {
            throw v1_2;
        }
    }

    private static int formatDurationLocked(long p20, int p22)
    {
        if (android.support.v4.util.TimeUtils.sFormatStr.length < p22) {
            int v4_2 = new char[p22];
            android.support.v4.util.TimeUtils.sFormatStr = v4_2;
        }
        int v4_25;
        char[] v2 = android.support.v4.util.TimeUtils.sFormatStr;
        if (p20 != 0) {
            int v16;
            if (p20 <= 0) {
                v16 = 45;
                p20 = (- p20);
            } else {
                v16 = 43;
            }
            int v13 = ((int) (p20 % 1000));
            int v17 = ((int) Math.floor(((double) (p20 / 1000))));
            int v3 = 0;
            int v12 = 0;
            int v14 = 0;
            if (v17 > 86400) {
                v3 = (v17 / 86400);
                v17 -= (86400 * v3);
            }
            if (v17 > 3600) {
                v12 = (v17 / 3600);
                v17 -= (v12 * 3600);
            }
            if (v17 > 60) {
                v14 = (v17 / 60);
                v17 -= (v14 * 60);
            }
            int v5_0 = 0;
            if (p22 != 0) {
                int v4_14;
                int v15_0 = android.support.v4.util.TimeUtils.accumField(v3, 1, 0, 0);
                if (v15_0 <= 0) {
                    v4_14 = 0;
                } else {
                    v4_14 = 1;
                }
                int v4_16;
                int v15_1 = (v15_0 + android.support.v4.util.TimeUtils.accumField(v12, 1, v4_14, 2));
                if (v15_1 <= 0) {
                    v4_16 = 0;
                } else {
                    v4_16 = 1;
                }
                int v4_18;
                int v15_2 = (v15_1 + android.support.v4.util.TimeUtils.accumField(v14, 1, v4_16, 2));
                if (v15_2 <= 0) {
                    v4_18 = 0;
                } else {
                    v4_18 = 1;
                }
                int v4_20;
                int v15_3 = (v15_2 + android.support.v4.util.TimeUtils.accumField(v17, 1, v4_18, 2));
                if (v15_3 <= 0) {
                    v4_20 = 0;
                } else {
                    v4_20 = 3;
                }
                int v15_4 = (v15_3 + (android.support.v4.util.TimeUtils.accumField(v13, 2, 1, v4_20) + 1));
                while (v15_4 < p22) {
                    v2[v5_0] = 32;
                    v5_0++;
                    v15_4++;
                }
            }
            int v19;
            v2[v5_0] = v16;
            int v5_1 = (v5_0 + 1);
            int v18 = v5_1;
            if (p22 == 0) {
                v19 = 0;
            } else {
                v19 = 1;
            }
            int v10_0;
            int v5_2 = android.support.v4.util.TimeUtils.printField(v2, v3, 100, v5_1, 0, 0);
            if (v5_2 == v18) {
                v10_0 = 0;
            } else {
                v10_0 = 1;
            }
            int v11_0;
            if (v19 == 0) {
                v11_0 = 0;
            } else {
                v11_0 = 2;
            }
            int v10_1;
            int v5_3 = android.support.v4.util.TimeUtils.printField(v2, v12, 104, v5_2, v10_0, v11_0);
            if (v5_3 == v18) {
                v10_1 = 0;
            } else {
                v10_1 = 1;
            }
            int v11_1;
            if (v19 == 0) {
                v11_1 = 0;
            } else {
                v11_1 = 2;
            }
            int v10_2;
            int v5_4 = android.support.v4.util.TimeUtils.printField(v2, v14, 109, v5_3, v10_1, v11_1);
            if (v5_4 == v18) {
                v10_2 = 0;
            } else {
                v10_2 = 1;
            }
            int v11_2;
            if (v19 == 0) {
                v11_2 = 0;
            } else {
                v11_2 = 2;
            }
            int v11_3;
            int v5_5 = android.support.v4.util.TimeUtils.printField(v2, v17, 115, v5_4, v10_2, v11_2);
            if ((v19 == 0) || (v5_5 == v18)) {
                v11_3 = 0;
            } else {
                v11_3 = 3;
            }
            int v5_6 = android.support.v4.util.TimeUtils.printField(v2, v13, 109, v5_5, 1, v11_3);
            v2[v5_6] = 115;
            v4_25 = (v5_6 + 1);
        } else {
            int v22_1 = (p22 - 1);
            while (0 < v22_1) {
                v2[0] = 32;
            }
            v2[0] = 48;
            v4_25 = 1;
        }
        return v4_25;
    }

    private static int printField(char[] p3, int p4, char p5, int p6, boolean p7, int p8)
    {
        if ((p7) || (p4 > 0)) {
            if (((p7) && (p8 >= 3)) || (p4 > 99)) {
                int v0_0 = (p4 / 100);
                p3[p6] = ((char) (v0_0 + 48));
                p6++;
                p4 -= (v0_0 * 100);
            }
            if (((p7) && (p8 >= 2)) || ((p4 > 9) || (p6 != p6))) {
                int v0_1 = (p4 / 10);
                p3[p6] = ((char) (v0_1 + 48));
                p6++;
                p4 -= (v0_1 * 10);
            }
            p3[p6] = ((char) (p4 + 48));
            int v6_1 = (p6 + 1);
            p3[v6_1] = p5;
            p6 = (v6_1 + 1);
        }
        return p6;
    }
}
