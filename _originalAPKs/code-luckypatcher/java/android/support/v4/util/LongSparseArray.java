package android.support.v4.util;
public class LongSparseArray implements java.lang.Cloneable {
    private static final Object DELETED;
    private boolean mGarbage;
    private long[] mKeys;
    private int mSize;
    private Object[] mValues;

    static LongSparseArray()
    {
        android.support.v4.util.LongSparseArray.DELETED = new Object();
        return;
    }

    public LongSparseArray()
    {
        this(10);
        return;
    }

    public LongSparseArray(int p3)
    {
        this.mGarbage = 0;
        int v3_1 = android.support.v4.util.LongSparseArray.idealLongArraySize(p3);
        Object[] v0_0 = new long[v3_1];
        this.mKeys = v0_0;
        Object[] v0_1 = new Object[v3_1];
        this.mValues = v0_1;
        this.mSize = 0;
        return;
    }

    private static int binarySearch(long[] p5, int p6, int p7, long p8)
    {
        int v1 = (p6 + p7);
        int v2 = (p6 - 1);
        while ((v1 - v2) > 1) {
            int v0 = ((v1 + v2) / 2);
            if (p5[v0] >= p8) {
                v1 = v0;
            } else {
                v2 = v0;
            }
        }
        if (v1 != (p6 + p7)) {
            if (p5[v1] != p8) {
                v1 ^= -1;
            }
        } else {
            v1 = ((p6 + p7) ^ -1);
        }
        return v1;
    }

    private void gc()
    {
        int v2 = this.mSize;
        int v3 = 0;
        long[] v1 = this.mKeys;
        Object[] v5 = this.mValues;
        int v0 = 0;
        while (v0 < v2) {
            Object v4 = v5[v0];
            if (v4 != android.support.v4.util.LongSparseArray.DELETED) {
                if (v0 != v3) {
                    v1[v3] = v1[v0];
                    v5[v3] = v4;
                    v5[v0] = 0;
                }
                v3++;
            }
            v0++;
        }
        this.mGarbage = 0;
        this.mSize = v3;
        return;
    }

    public static int idealByteArraySize(int p3)
    {
        int v0 = 4;
        while (v0 < 32) {
            if (p3 > ((1 << v0) - 12)) {
                v0++;
            } else {
                p3 = ((1 << v0) - 12);
                break;
            }
        }
        return p3;
    }

    public static int idealLongArraySize(int p1)
    {
        return (android.support.v4.util.LongSparseArray.idealByteArraySize((p1 * 8)) / 8);
    }

    public void append(long p8, Object p10)
    {
        if ((this.mSize == 0) || (p8 > this.mKeys[(this.mSize - 1)])) {
            if ((this.mGarbage) && (this.mSize >= this.mKeys.length)) {
                this.gc();
            }
            int v3 = this.mSize;
            if (v3 >= this.mKeys.length) {
                int v0 = android.support.v4.util.LongSparseArray.idealLongArraySize((v3 + 1));
                long[] v1 = new long[v0];
                Object[] v2 = new Object[v0];
                System.arraycopy(this.mKeys, 0, v1, 0, this.mKeys.length);
                System.arraycopy(this.mValues, 0, v2, 0, this.mValues.length);
                this.mKeys = v1;
                this.mValues = v2;
            }
            this.mKeys[v3] = p8;
            this.mValues[v3] = p10;
            this.mSize = (v3 + 1);
        } else {
            this.put(p8, p10);
        }
        return;
    }

    public void clear()
    {
        int v1 = this.mSize;
        int v0 = 0;
        while (v0 < v1) {
            this.mValues[v0] = 0;
            v0++;
        }
        this.mSize = 0;
        this.mGarbage = 0;
        return;
    }

    public android.support.v4.util.LongSparseArray clone()
    {
        try {
            android.support.v4.util.LongSparseArray v1 = ((android.support.v4.util.LongSparseArray) super.clone());
            v1.mKeys = ((long[]) this.mKeys.clone());
            v1.mValues = ((Object[]) this.mValues.clone());
        } catch (CloneNotSupportedException v2) {
        }
        return v1;
    }

    public bridge synthetic Object clone()
    {
        return this.clone();
    }

    public void delete(long p5)
    {
        int v0 = android.support.v4.util.LongSparseArray.binarySearch(this.mKeys, 0, this.mSize, p5);
        if ((v0 >= 0) && (this.mValues[v0] != android.support.v4.util.LongSparseArray.DELETED)) {
            this.mValues[v0] = android.support.v4.util.LongSparseArray.DELETED;
            this.mGarbage = 1;
        }
        return;
    }

    public Object get(long p2)
    {
        return this.get(p2, 0);
    }

    public Object get(long p5, Object p7)
    {
        int v0 = android.support.v4.util.LongSparseArray.binarySearch(this.mKeys, 0, this.mSize, p5);
        if ((v0 >= 0) && (this.mValues[v0] != android.support.v4.util.LongSparseArray.DELETED)) {
            p7 = this.mValues[v0];
        }
        return p7;
    }

    public int indexOfKey(long p4)
    {
        if (this.mGarbage) {
            this.gc();
        }
        return android.support.v4.util.LongSparseArray.binarySearch(this.mKeys, 0, this.mSize, p4);
    }

    public int indexOfValue(Object p3)
    {
        if (this.mGarbage) {
            this.gc();
        }
        int v0 = 0;
        while (v0 < this.mSize) {
            if (this.mValues[v0] != p3) {
                v0++;
            }
            return v0;
        }
        v0 = -1;
        return v0;
    }

    public long keyAt(int p3)
    {
        if (this.mGarbage) {
            this.gc();
        }
        return this.mKeys[p3];
    }

    public void put(long p9, Object p11)
    {
        int v0_0 = android.support.v4.util.LongSparseArray.binarySearch(this.mKeys, 0, this.mSize, p9);
        if (v0_0 < 0) {
            int v0_1 = (v0_0 ^ -1);
            if ((v0_1 >= this.mSize) || (this.mValues[v0_1] != android.support.v4.util.LongSparseArray.DELETED)) {
                if ((this.mGarbage) && (this.mSize >= this.mKeys.length)) {
                    this.gc();
                    v0_1 = (android.support.v4.util.LongSparseArray.binarySearch(this.mKeys, 0, this.mSize, p9) ^ -1);
                }
                if (this.mSize >= this.mKeys.length) {
                    int v1 = android.support.v4.util.LongSparseArray.idealLongArraySize((this.mSize + 1));
                    long[] v2 = new long[v1];
                    Object[] v3 = new Object[v1];
                    System.arraycopy(this.mKeys, 0, v2, 0, this.mKeys.length);
                    System.arraycopy(this.mValues, 0, v3, 0, this.mValues.length);
                    this.mKeys = v2;
                    this.mValues = v3;
                }
                if ((this.mSize - v0_1) != 0) {
                    System.arraycopy(this.mKeys, v0_1, this.mKeys, (v0_1 + 1), (this.mSize - v0_1));
                    System.arraycopy(this.mValues, v0_1, this.mValues, (v0_1 + 1), (this.mSize - v0_1));
                }
                this.mKeys[v0_1] = p9;
                this.mValues[v0_1] = p11;
                this.mSize = (this.mSize + 1);
            } else {
                this.mKeys[v0_1] = p9;
                this.mValues[v0_1] = p11;
            }
        } else {
            this.mValues[v0_0] = p11;
        }
        return;
    }

    public void remove(long p1)
    {
        this.delete(p1);
        return;
    }

    public void removeAt(int p3)
    {
        if (this.mValues[p3] != android.support.v4.util.LongSparseArray.DELETED) {
            this.mValues[p3] = android.support.v4.util.LongSparseArray.DELETED;
            this.mGarbage = 1;
        }
        return;
    }

    public void setValueAt(int p2, Object p3)
    {
        if (this.mGarbage) {
            this.gc();
        }
        this.mValues[p2] = p3;
        return;
    }

    public int size()
    {
        if (this.mGarbage) {
            this.gc();
        }
        return this.mSize;
    }

    public Object valueAt(int p2)
    {
        if (this.mGarbage) {
            this.gc();
        }
        return this.mValues[p2];
    }
}
