package android.support.v4.widget;
public abstract class DrawerLayout$SimpleDrawerListener implements android.support.v4.widget.DrawerLayout$DrawerListener {

    public DrawerLayout$SimpleDrawerListener()
    {
        return;
    }

    public void onDrawerClosed(android.view.View p1)
    {
        return;
    }

    public void onDrawerOpened(android.view.View p1)
    {
        return;
    }

    public void onDrawerSlide(android.view.View p1, float p2)
    {
        return;
    }

    public void onDrawerStateChanged(int p1)
    {
        return;
    }
}
