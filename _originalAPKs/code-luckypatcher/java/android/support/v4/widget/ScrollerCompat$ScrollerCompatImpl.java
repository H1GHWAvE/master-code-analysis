package android.support.v4.widget;
interface ScrollerCompat$ScrollerCompatImpl {

    public abstract void abortAnimation();

    public abstract boolean computeScrollOffset();

    public abstract Object createScroller();

    public abstract void fling();

    public abstract void fling();

    public abstract float getCurrVelocity();

    public abstract int getCurrX();

    public abstract int getCurrY();

    public abstract int getFinalX();

    public abstract int getFinalY();

    public abstract boolean isFinished();

    public abstract boolean isOverScrolled();

    public abstract void notifyHorizontalEdgeReached();

    public abstract void notifyVerticalEdgeReached();

    public abstract void startScroll();

    public abstract void startScroll();
}
