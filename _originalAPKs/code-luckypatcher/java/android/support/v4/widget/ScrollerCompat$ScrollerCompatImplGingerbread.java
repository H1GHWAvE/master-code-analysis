package android.support.v4.widget;
 class ScrollerCompat$ScrollerCompatImplGingerbread implements android.support.v4.widget.ScrollerCompat$ScrollerCompatImpl {

    ScrollerCompat$ScrollerCompatImplGingerbread()
    {
        return;
    }

    public void abortAnimation(Object p1)
    {
        android.support.v4.widget.ScrollerCompatGingerbread.abortAnimation(p1);
        return;
    }

    public boolean computeScrollOffset(Object p2)
    {
        return android.support.v4.widget.ScrollerCompatGingerbread.computeScrollOffset(p2);
    }

    public Object createScroller(android.content.Context p2, android.view.animation.Interpolator p3)
    {
        return android.support.v4.widget.ScrollerCompatGingerbread.createScroller(p2, p3);
    }

    public void fling(Object p1, int p2, int p3, int p4, int p5, int p6, int p7, int p8, int p9)
    {
        android.support.v4.widget.ScrollerCompatGingerbread.fling(p1, p2, p3, p4, p5, p6, p7, p8, p9);
        return;
    }

    public void fling(Object p1, int p2, int p3, int p4, int p5, int p6, int p7, int p8, int p9, int p10, int p11)
    {
        android.support.v4.widget.ScrollerCompatGingerbread.fling(p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11);
        return;
    }

    public float getCurrVelocity(Object p2)
    {
        return 0;
    }

    public int getCurrX(Object p2)
    {
        return android.support.v4.widget.ScrollerCompatGingerbread.getCurrX(p2);
    }

    public int getCurrY(Object p2)
    {
        return android.support.v4.widget.ScrollerCompatGingerbread.getCurrY(p2);
    }

    public int getFinalX(Object p2)
    {
        return android.support.v4.widget.ScrollerCompatGingerbread.getFinalX(p2);
    }

    public int getFinalY(Object p2)
    {
        return android.support.v4.widget.ScrollerCompatGingerbread.getFinalY(p2);
    }

    public boolean isFinished(Object p2)
    {
        return android.support.v4.widget.ScrollerCompatGingerbread.isFinished(p2);
    }

    public boolean isOverScrolled(Object p2)
    {
        return android.support.v4.widget.ScrollerCompatGingerbread.isOverScrolled(p2);
    }

    public void notifyHorizontalEdgeReached(Object p1, int p2, int p3, int p4)
    {
        android.support.v4.widget.ScrollerCompatGingerbread.notifyHorizontalEdgeReached(p1, p2, p3, p4);
        return;
    }

    public void notifyVerticalEdgeReached(Object p1, int p2, int p3, int p4)
    {
        android.support.v4.widget.ScrollerCompatGingerbread.notifyVerticalEdgeReached(p1, p2, p3, p4);
        return;
    }

    public void startScroll(Object p1, int p2, int p3, int p4, int p5)
    {
        android.support.v4.widget.ScrollerCompatGingerbread.startScroll(p1, p2, p3, p4, p5);
        return;
    }

    public void startScroll(Object p1, int p2, int p3, int p4, int p5, int p6)
    {
        android.support.v4.widget.ScrollerCompatGingerbread.startScroll(p1, p2, p3, p4, p5, p6);
        return;
    }
}
