package android.support.v4.widget;
interface SearchViewCompatHoneycomb$OnQueryTextListenerCompatBridge {

    public abstract boolean onQueryTextChange();

    public abstract boolean onQueryTextSubmit();
}
