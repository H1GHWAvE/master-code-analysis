package android.support.v4.widget;
interface EdgeEffectCompat$EdgeEffectImpl {

    public abstract boolean draw();

    public abstract void finish();

    public abstract boolean isFinished();

    public abstract Object newEdgeEffect();

    public abstract boolean onAbsorb();

    public abstract boolean onPull();

    public abstract boolean onRelease();

    public abstract void setSize();
}
