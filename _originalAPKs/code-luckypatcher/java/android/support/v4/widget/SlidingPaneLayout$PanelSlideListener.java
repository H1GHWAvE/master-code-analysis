package android.support.v4.widget;
public interface SlidingPaneLayout$PanelSlideListener {

    public abstract void onPanelClosed();

    public abstract void onPanelOpened();

    public abstract void onPanelSlide();
}
