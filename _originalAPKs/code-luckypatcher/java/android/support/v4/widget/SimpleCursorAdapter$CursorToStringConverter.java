package android.support.v4.widget;
public interface SimpleCursorAdapter$CursorToStringConverter {

    public abstract CharSequence convertToString();
}
