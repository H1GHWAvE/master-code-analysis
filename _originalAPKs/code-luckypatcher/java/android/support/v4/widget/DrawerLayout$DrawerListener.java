package android.support.v4.widget;
public interface DrawerLayout$DrawerListener {

    public abstract void onDrawerClosed();

    public abstract void onDrawerOpened();

    public abstract void onDrawerSlide();

    public abstract void onDrawerStateChanged();
}
