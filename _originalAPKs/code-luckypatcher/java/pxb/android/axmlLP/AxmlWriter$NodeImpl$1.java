package pxb.android.axmlLP;
 class AxmlWriter$NodeImpl$1 implements java.util.Comparator {
    final synthetic pxb.android.axmlLP.AxmlWriter$NodeImpl this$0;

    AxmlWriter$NodeImpl$1(pxb.android.axmlLP.AxmlWriter$NodeImpl p1)
    {
        this.this$0 = p1;
        return;
    }

    public bridge synthetic int compare(Object p2, Object p3)
    {
        return this.compare(((pxb.android.axmlLP.AxmlWriter$Attr) p2), ((pxb.android.axmlLP.AxmlWriter$Attr) p3));
    }

    public int compare(pxb.android.axmlLP.AxmlWriter$Attr p4, pxb.android.axmlLP.AxmlWriter$Attr p5)
    {
        int v0;
        if (p4.ns != null) {
            if (p5.ns != null) {
                v0 = p4.ns.data.compareTo(p5.ns.data);
                if (v0 == 0) {
                    v0 = (p4.resourceId - p5.resourceId);
                    if (v0 == 0) {
                        v0 = p4.name.data.compareTo(p5.name.data);
                    }
                }
            } else {
                v0 = -1;
            }
        } else {
            if (p5.ns != null) {
                v0 = 1;
            } else {
                v0 = p5.name.data.compareTo(p4.name.data);
            }
        }
        return v0;
    }
}
