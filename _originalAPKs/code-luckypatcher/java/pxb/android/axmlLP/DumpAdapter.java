package pxb.android.axmlLP;
public class DumpAdapter extends pxb.android.axmlLP.AxmlVisitor {
    private java.util.Map nses;

    public DumpAdapter()
    {
        this.nses = new java.util.HashMap();
        return;
    }

    public DumpAdapter(pxb.android.axmlLP.AxmlVisitor p2)
    {
        this(p2);
        this.nses = new java.util.HashMap();
        return;
    }

    public void end()
    {
        super.end();
        return;
    }

    public pxb.android.axmlLP.AxmlVisitor$NodeVisitor first(String p6, String p7)
    {
        System.out.print("<");
        if (p6 != null) {
            System.out.println(new StringBuilder().append(((String) this.nses.get(p6))).append(":").toString());
        }
        int v1_0;
        System.out.println(p7);
        pxb.android.axmlLP.AxmlVisitor$NodeVisitor v0 = super.first(p6, p7);
        if (v0 == null) {
            v1_0 = 0;
        } else {
            v1_0 = new pxb.android.axmlLP.DumpAdapter$DumpNodeAdapter(v0, 1, this.nses);
        }
        return v1_0;
    }

    public void ns(String p4, String p5, int p6)
    {
        System.out.println(new StringBuilder().append(p4).append("=").append(p5).toString());
        this.nses.put(p5, p4);
        super.ns(p4, p5, p6);
        return;
    }
}
