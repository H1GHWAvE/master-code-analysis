package pxb.android.axmlLP;
public class AxmlReader {
    static final int CHUNK_AXML_FILE = 524291;
    static final int CHUNK_RESOURCEIDS = 524672;
    static final int CHUNK_STRINGS = 1835009;
    static final int CHUNK_XML_END_NAMESPACE = 1048833;
    static final int CHUNK_XML_END_TAG = 1048835;
    static final int CHUNK_XML_START_NAMESPACE = 1048832;
    static final int CHUNK_XML_START_TAG = 1048834;
    static final int CHUNK_XML_TEXT = 1048836;
    public static final pxb.android.axmlLP.AxmlVisitor$NodeVisitor EMPTY_VISITOR = None;
    static final int UTF8_FLAG = 256;
    private com.googlecode.dex2jar.reader.io.DataIn in;
    private java.util.List resourceIds;
    private pxb.android.axmlLP.StringItems stringItems;

    static AxmlReader()
    {
        pxb.android.axmlLP.AxmlReader.EMPTY_VISITOR = new pxb.android.axmlLP.AxmlReader$1();
        return;
    }

    public AxmlReader(com.googlecode.dex2jar.reader.io.DataIn p2)
    {
        this.resourceIds = new java.util.ArrayList();
        this.stringItems = new pxb.android.axmlLP.StringItems();
        this.in = p2;
        return;
    }

    public AxmlReader(byte[] p2)
    {
        this(com.googlecode.dex2jar.reader.io.ArrayDataIn.le(p2));
        return;
    }

    public void accept(pxb.android.axmlLP.AxmlVisitor p29)
    {
        com.googlecode.dex2jar.reader.io.DataIn v16 = this.in;
        if (v16.readIntx() == 524291) {
            pxb.android.axmlLP.AxmlReader$2 v23;
            int v13 = v16.readIntx();
            if (p29 != null) {
                v23 = new pxb.android.axmlLP.AxmlReader$2;
                v23(this, p29);
            } else {
                v23 = pxb.android.axmlLP.AxmlReader.EMPTY_VISITOR;
            }
            pxb.android.axmlLP.AxmlVisitor$NodeVisitor v4_0 = v23;
            java.util.Stack v20_1 = new java.util.Stack();
            v20_1.push(v23);
            int v21 = v16.getCurrentPosition();
            while (v21 < v13) {
                int v25_1 = v16.readIntx();
                int v24 = v16.readIntx();
                switch (v25_1) {
                    case 524672:
                        int v12 = ((v24 / 4) - 2);
                        int v15_1 = 0;
                        while (v15_1 < v12) {
                            this.resourceIds.add(Integer.valueOf(v16.readIntx()));
                            v15_1++;
                        }
                        break;
                    case 1048832:
                        if (p29 != null) {
                            int v17_2 = v16.readIntx();
                            v16.skip(4);
                            p29.ns(((pxb.android.axmlLP.StringItem) this.stringItems.get(v16.readIntx())).data, ((pxb.android.axmlLP.StringItem) this.stringItems.get(v16.readIntx())).data, v17_2);
                        } else {
                            v16.skip(16);
                        }
                        break;
                    case 1048833:
                        v16.skip((v24 - 8));
                        break;
                    case 1048834:
                        int v17_1 = v16.readIntx();
                        v16.skip(4);
                        int v19_0 = v16.readIntx();
                        int v18_1 = v16.readIntx();
                        if (v16.readIntx() == 1310740) {
                            int v5_0;
                            if (v19_0 < 0) {
                                v5_0 = 0;
                            } else {
                                v5_0 = ((pxb.android.axmlLP.StringItem) this.stringItems.get(v19_0)).data;
                            }
                            v4_0 = v4_0.child(v5_0, ((pxb.android.axmlLP.StringItem) this.stringItems.get(v18_1)).data);
                            if (v4_0 == null) {
                                v4_0 = pxb.android.axmlLP.AxmlReader.EMPTY_VISITOR;
                            }
                            v20_1.push(v4_0);
                            v4_0.line(v17_1);
                            int v11 = v16.readUShortx();
                            v16.skip(6);
                            if (v4_0 == pxb.android.axmlLP.AxmlReader.EMPTY_VISITOR) {
                                v16.skip(20);
                            } else {
                                int v15_0 = 0;
                                while (v15_0 < v11) {
                                    int v5_1;
                                    int v19_1 = v16.readIntx();
                                    int v18_2 = v16.readIntx();
                                    v16.skip(4);
                                    int v8 = (v16.readIntx() >> 24);
                                    int v10 = v16.readIntx();
                                    String v6_2 = ((pxb.android.axmlLP.StringItem) this.stringItems.get(v18_2)).data;
                                    if (v19_1 < 0) {
                                        v5_1 = 0;
                                    } else {
                                        v5_1 = ((pxb.android.axmlLP.StringItem) this.stringItems.get(v19_1)).data;
                                    }
                                    Boolean v9;
                                    switch (v8) {
                                        case 3:
                                            v9 = ((pxb.android.axmlLP.StringItem) this.stringItems.get(v10)).data;
                                            break;
                                        case 18:
                                            Integer v26_29;
                                            if (v10 == 0) {
                                                v26_29 = 0;
                                            } else {
                                                v26_29 = 1;
                                            }
                                            v9 = Boolean.valueOf(v26_29);
                                            break;
                                        default:
                                            v9 = Integer.valueOf(v10);
                                    }
                                    int v7;
                                    if (v18_2 >= this.resourceIds.size()) {
                                        v7 = -1;
                                    } else {
                                        v7 = ((Integer) this.resourceIds.get(v18_2)).intValue();
                                    }
                                    v4_0.attr(v5_1, v6_2, v7, v8, v9);
                                    v15_0++;
                                }
                            }
                        } else {
                            throw new RuntimeException();
                        }
                        break;
                    case 1048835:
                        v16.skip((v24 - 8));
                        v4_0.end();
                        v20_1.pop();
                        v4_0 = ((pxb.android.axmlLP.AxmlVisitor$NodeVisitor) v20_1.peek());
                        break;
                    case 1048836:
                        if (v4_0 != pxb.android.axmlLP.AxmlReader.EMPTY_VISITOR) {
                            int v17_0 = v16.readIntx();
                            v16.skip(4);
                            int v18_0 = v16.readIntx();
                            v16.skip(8);
                            v4_0.text(v17_0, ((pxb.android.axmlLP.StringItem) this.stringItems.get(v18_0)).data);
                        } else {
                            v16.skip(20);
                        }
                        break;
                    case 1835009:
                        this.stringItems.read(v16, v24);
                        break;
                    default:
                        throw new RuntimeException();
                }
                v16.move((v21 + v24));
                v21 = v16.getCurrentPosition();
            }
            return;
        } else {
            throw new RuntimeException();
        }
    }
}
