package pxb.android.axmlLP;
public class Axml extends pxb.android.axmlLP.AxmlVisitor {
    public java.util.List firsts;
    public java.util.List nses;

    public Axml()
    {
        this.firsts = new java.util.ArrayList();
        this.nses = new java.util.ArrayList();
        return;
    }

    public void accept(pxb.android.axmlLP.AxmlVisitor p6)
    {
        java.util.Iterator v2_1 = this.nses.iterator();
        while (v2_1.hasNext()) {
            ((pxb.android.axmlLP.Axml$Ns) v2_1.next()).accept(p6);
        }
        java.util.Iterator v2_3 = this.firsts.iterator();
        while (v2_3.hasNext()) {
            ((pxb.android.axmlLP.Axml$Node) v2_3.next()).accept(new pxb.android.axmlLP.Axml$1(this, 0, p6));
        }
        return;
    }

    public pxb.android.axmlLP.AxmlVisitor$NodeVisitor first(String p3, String p4)
    {
        pxb.android.axmlLP.Axml$Node v0_1 = new pxb.android.axmlLP.Axml$Node();
        v0_1.name = p4;
        v0_1.ns = p3;
        this.firsts.add(v0_1);
        return v0_1;
    }

    public void ns(String p3, String p4, int p5)
    {
        pxb.android.axmlLP.Axml$Ns v0_1 = new pxb.android.axmlLP.Axml$Ns();
        v0_1.prefix = p3;
        v0_1.uri = p4;
        v0_1.ln = p5;
        this.nses.add(v0_1);
        return;
    }
}
