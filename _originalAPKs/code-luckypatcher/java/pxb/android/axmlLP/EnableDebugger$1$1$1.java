package pxb.android.axmlLP;
 class EnableDebugger$1$1$1 extends pxb.android.axmlLP.AxmlVisitor$NodeVisitor {
    final synthetic pxb.android.axmlLP.EnableDebugger$1$1 this$2;

    EnableDebugger$1$1$1(pxb.android.axmlLP.EnableDebugger$1$1 p1, pxb.android.axmlLP.AxmlVisitor$NodeVisitor p2)
    {
        this.this$2 = p1;
        this(p2);
        return;
    }

    public void attr(String p2, String p3, int p4, int p5, Object p6)
    {
        if ((!"http://schemas.android.com/apk/res/android".equals(p2)) || (!"debuggable".equals(p3))) {
            super.attr(p2, p3, p4, p5, p6);
        }
        return;
    }

    public void end()
    {
        super.attr("http://schemas.android.com/apk/res/android", "debuggable", 16842767, 18, Integer.valueOf(-1));
        super.end();
        return;
    }
}
