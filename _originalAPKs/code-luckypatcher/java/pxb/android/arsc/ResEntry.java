package pxb.android.arsc;
public class ResEntry {
    public final int flag;
    public final pxb.android.arsc.ResSpec spec;
    public Object value;
    int wOffset;

    public ResEntry(int p1, pxb.android.arsc.ResSpec p2)
    {
        this.flag = p1;
        this.spec = p2;
        return;
    }
}
