package pxb.android.arsc;
public class Value {
    public final int data;
    public String raw;
    public final int type;

    public Value(int p1, int p2, String p3)
    {
        this.type = p1;
        this.data = p2;
        this.raw = p3;
        return;
    }

    public String toString()
    {
        String v0_2;
        if (this.type != 3) {
            Object[] v1_2 = new Object[2];
            v1_2[0] = Integer.valueOf(this.type);
            v1_2[1] = Integer.valueOf(this.data);
            v0_2 = String.format("{t=0x%02x d=0x%08x}", v1_2);
        } else {
            v0_2 = this.raw;
        }
        return v0_2;
    }
}
