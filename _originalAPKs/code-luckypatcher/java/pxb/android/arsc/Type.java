package pxb.android.arsc;
public class Type {
    public java.util.List configs;
    public int id;
    public String name;
    public pxb.android.arsc.ResSpec[] specs;
    int wPosition;

    public Type()
    {
        this.configs = new java.util.ArrayList();
        return;
    }

    public void addConfig(pxb.android.arsc.Config p3)
    {
        if (p3.entryCount == this.specs.length) {
            this.configs.add(p3);
            return;
        } else {
            throw new RuntimeException();
        }
    }

    public pxb.android.arsc.ResSpec getSpec(int p3)
    {
        pxb.android.arsc.ResSpec v0_0 = this.specs[p3];
        if (v0_0 == null) {
            v0_0 = new pxb.android.arsc.ResSpec(p3);
            this.specs[p3] = v0_0;
        }
        return v0_0;
    }
}
