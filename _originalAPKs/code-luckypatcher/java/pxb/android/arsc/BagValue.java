package pxb.android.arsc;
public class BagValue {
    public java.util.List map;
    public final int parent;

    public BagValue(int p2)
    {
        this.map = new java.util.ArrayList();
        this.parent = p2;
        return;
    }

    public boolean equals(Object p6)
    {
        int v1 = 1;
        if (this != p6) {
            if (p6 != null) {
                if ((p6 instanceof pxb.android.arsc.BagValue)) {
                    if (this.map != null) {
                        if (!this.map.equals(((pxb.android.arsc.BagValue) p6).map)) {
                            v1 = 0;
                            return v1;
                        }
                    } else {
                        if (((pxb.android.arsc.BagValue) p6).map != null) {
                            v1 = 0;
                            return v1;
                        }
                    }
                    if (this.parent != ((pxb.android.arsc.BagValue) p6).parent) {
                        v1 = 0;
                    }
                } else {
                    v1 = 0;
                }
            } else {
                v1 = 0;
            }
        }
        return v1;
    }

    public int hashCode()
    {
        int v2_2;
        if (this.map != null) {
            v2_2 = this.map.hashCode();
        } else {
            v2_2 = 0;
        }
        return (((v2_2 + 31) * 31) + this.parent);
    }

    public String toString()
    {
        StringBuilder v2_1 = new StringBuilder();
        String v4_0 = new Object[1];
        v4_0[0] = Integer.valueOf(this.parent);
        v2_1.append(String.format("{bag%08x", v4_0));
        java.util.Iterator v1 = this.map.iterator();
        while (v1.hasNext()) {
            java.util.Map$Entry v0_1 = ((java.util.Map$Entry) v1.next());
            String v3_8 = v2_1.append(",");
            Object[] v5_2 = new Object[1];
            v5_2[0] = v0_1.getKey();
            v3_8.append(String.format("0x%08x", v5_2));
            v2_1.append("=");
            v2_1.append(v0_1.getValue());
        }
        return v2_1.append("}").toString();
    }
}
