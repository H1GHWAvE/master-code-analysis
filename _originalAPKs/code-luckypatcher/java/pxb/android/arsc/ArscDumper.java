package pxb.android.arsc;
public class ArscDumper {

    public ArscDumper()
    {
        return;
    }

    public static void dump(java.util.List p17)
    {
        int v11 = 0;
        while (v11 < p17.size()) {
            pxb.android.arsc.Pkg v7_1 = ((pxb.android.arsc.Pkg) p17.get(v11));
            Object[] v14_1 = new Object[4];
            v14_1[0] = Integer.valueOf(v11);
            v14_1[1] = Integer.valueOf(v7_1.id);
            v14_1[2] = v7_1.name;
            v14_1[3] = Integer.valueOf(v7_1.types.size());
            System.out.println(String.format("  Package %d id=%d name=%s typeCount=%d", v14_1));
            java.util.Iterator v5 = v7_1.types.values().iterator();
            while (v5.hasNext()) {
                pxb.android.arsc.Type v10_1 = ((pxb.android.arsc.Type) v5.next());
                Object[] v14_3 = new Object[2];
                v14_3[0] = Integer.valueOf((v10_1.id - 1));
                v14_3[1] = v10_1.name;
                System.out.println(String.format("    type %d %s", v14_3));
                int v8 = ((v7_1.id << 24) | (v10_1.id << 16));
                int v4_0 = 0;
                while (v4_0 < v10_1.specs.length) {
                    pxb.android.arsc.ResSpec v9 = v10_1.getSpec(v4_0);
                    Object[] v14_7 = new Object[3];
                    v14_7[0] = Integer.valueOf((v9.id | v8));
                    v14_7[1] = Integer.valueOf(v9.flags);
                    v14_7[2] = v9.name;
                    System.out.println(String.format("      spec 0x%08x 0x%08x %s", v14_7));
                    v4_0++;
                }
                int v4_1 = 0;
                while (v4_1 < v10_1.configs.size()) {
                    pxb.android.arsc.Config v1_1 = ((pxb.android.arsc.Config) v10_1.configs.get(v4_1));
                    System.out.println("      config");
                    java.util.ArrayList v2_1 = new java.util.ArrayList(v1_1.resources.values());
                    int v6 = 0;
                    while (v6 < v2_1.size()) {
                        pxb.android.arsc.ResEntry v3_1 = ((pxb.android.arsc.ResEntry) v2_1.get(v6));
                        Object[] v14_5 = new Object[3];
                        v14_5[0] = Integer.valueOf((v3_1.spec.id | v8));
                        v14_5[1] = v3_1.spec.name;
                        v14_5[2] = v3_1.value;
                        System.out.println(String.format("        resource 0x%08x %-20s: %s", v14_5));
                        v6++;
                    }
                    v4_1++;
                }
            }
            v11++;
        }
        return;
    }

    public static varargs void main(String[] p4)
    {
        if (p4.length != 0) {
            pxb.android.arsc.ArscDumper.dump(new pxb.android.arsc.ArscParser(pxb.android.axml.Util.readFile(new java.io.File(p4[0]))).parse());
        } else {
            System.err.println("asrc-dump file.arsc");
        }
        return;
    }
}
