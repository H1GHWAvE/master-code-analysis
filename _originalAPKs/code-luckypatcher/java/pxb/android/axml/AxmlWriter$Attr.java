package pxb.android.axml;
 class AxmlWriter$Attr {
    public int index;
    public pxb.android.StringItem name;
    public pxb.android.StringItem ns;
    public pxb.android.StringItem raw;
    public int resourceId;
    public int type;
    public Object value;

    public AxmlWriter$Attr(pxb.android.StringItem p1, pxb.android.StringItem p2, int p3)
    {
        this.ns = p1;
        this.name = p2;
        this.resourceId = p3;
        return;
    }

    public void prepare(pxb.android.axml.AxmlWriter p3)
    {
        this.ns = p3.updateNs(this.ns);
        if (this.name != null) {
            if (this.resourceId == -1) {
                this.name = p3.update(this.name);
            } else {
                this.name = p3.updateWithResourceId(this.name, this.resourceId);
            }
        }
        if ((this.value instanceof pxb.android.StringItem)) {
            this.value = p3.update(((pxb.android.StringItem) this.value));
        }
        if (this.raw != null) {
            this.raw = p3.update(this.raw);
        }
        return;
    }
}
