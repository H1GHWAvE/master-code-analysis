package pxb.android.axml;
public class AxmlWriter extends pxb.android.axml.AxmlVisitor {
    static final java.util.Comparator ATTR_CMP;
    private java.util.List firsts;
    private java.util.Map nses;
    private java.util.List otherString;
    private java.util.Map resourceId2Str;
    private java.util.List resourceIds;
    private java.util.List resourceString;
    private pxb.android.StringItems stringItems;

    static AxmlWriter()
    {
        pxb.android.axml.AxmlWriter.ATTR_CMP = new pxb.android.axml.AxmlWriter$1();
        return;
    }

    public AxmlWriter()
    {
        this.firsts = new java.util.ArrayList(3);
        this.nses = new java.util.HashMap();
        this.otherString = new java.util.ArrayList();
        this.resourceId2Str = new java.util.HashMap();
        this.resourceIds = new java.util.ArrayList();
        this.resourceString = new java.util.ArrayList();
        this.stringItems = new pxb.android.StringItems();
        return;
    }

    private int prepare()
    {
        int v6_0 = 0;
        java.util.Iterator v4_0 = this.firsts.iterator();
        while (v4_0.hasNext()) {
            v6_0 += ((pxb.android.axml.AxmlWriter$NodeImpl) v4_0.next()).prepare(this);
        }
        int v0 = 0;
        java.util.Iterator v4_1 = this.nses.entrySet().iterator();
        while (v4_1.hasNext()) {
            java.util.Map$Entry v2_1 = ((java.util.Map$Entry) v4_1.next());
            pxb.android.axml.AxmlWriter$Ns v5_1 = ((pxb.android.axml.AxmlWriter$Ns) v2_1.getValue());
            if (v5_1 == null) {
                v5_1 = new pxb.android.axml.AxmlWriter$Ns(0, new pxb.android.StringItem(((String) v2_1.getKey())), 0);
                v2_1.setValue(v5_1);
            }
            if (v5_1.prefix == null) {
                Object[] v10_1 = new Object[1];
                int v1 = (v0 + 1);
                v10_1[0] = Integer.valueOf(v0);
                v5_1.prefix = new pxb.android.StringItem(String.format("axml_auto_%02d", v10_1));
                v0 = v1;
            }
            v5_1.prefix = this.update(v5_1.prefix);
            v5_1.uri = this.update(v5_1.uri);
        }
        int v6_1 = (v6_0 + ((this.nses.size() * 24) * 2));
        this.stringItems.addAll(this.resourceString);
        this.resourceString = 0;
        this.stringItems.addAll(this.otherString);
        this.otherString = 0;
        this.stringItems.prepare();
        int v7 = this.stringItems.getSize();
        if ((v7 % 4) != 0) {
            v7 += (4 - (v7 % 4));
        }
        return ((v6_1 + (v7 + 8)) + ((this.resourceIds.size() * 4) + 8));
    }

    public pxb.android.axml.NodeVisitor child(String p3, String p4)
    {
        pxb.android.axml.AxmlWriter$NodeImpl v0_1 = new pxb.android.axml.AxmlWriter$NodeImpl(p3, p4);
        this.firsts.add(v0_1);
        return v0_1;
    }

    public void end()
    {
        return;
    }

    public void ns(String p5, String p6, int p7)
    {
        pxb.android.StringItem v0_1;
        java.util.Map v1 = this.nses;
        if (p5 != null) {
            v0_1 = new pxb.android.StringItem(p5);
        } else {
            v0_1 = 0;
        }
        v1.put(p6, new pxb.android.axml.AxmlWriter$Ns(v0_1, new pxb.android.StringItem(p6), p7));
        return;
    }

    public byte[] toByteArray()
    {
        int v7 = (this.prepare() + 8);
        java.nio.ByteBuffer v5 = java.nio.ByteBuffer.allocate(v7).order(java.nio.ByteOrder.LITTLE_ENDIAN);
        v5.putInt(524291);
        v5.putInt(v7);
        int v9 = this.stringItems.getSize();
        int v6 = 0;
        if ((v9 % 4) != 0) {
            v6 = (4 - (v9 % 4));
        }
        v5.putInt(1835009);
        v5.putInt(((v9 + v6) + 8));
        this.stringItems.write(v5);
        byte[] v10_10 = new byte[v6];
        v5.put(v10_10);
        v5.putInt(524672);
        v5.putInt(((this.resourceIds.size() * 4) + 8));
        java.util.Iterator v3_0 = this.resourceIds.iterator();
        while (v3_0.hasNext()) {
            v5.putInt(((Integer) v3_0.next()).intValue());
        }
        java.util.Stack v8_1 = new java.util.Stack();
        java.util.Iterator v3_1 = this.nses.entrySet().iterator();
        while (v3_1.hasNext()) {
            pxb.android.axml.AxmlWriter$Ns v4_3 = ((pxb.android.axml.AxmlWriter$Ns) ((java.util.Map$Entry) v3_1.next()).getValue());
            v8_1.push(v4_3);
            v5.putInt(1048832);
            v5.putInt(24);
            v5.putInt(-1);
            v5.putInt(-1);
            v5.putInt(v4_3.prefix.index);
            v5.putInt(v4_3.uri.index);
        }
        java.util.Iterator v3_2 = this.firsts.iterator();
        while (v3_2.hasNext()) {
            ((pxb.android.axml.AxmlWriter$NodeImpl) v3_2.next()).write(v5);
        }
        while (v8_1.size() > 0) {
            pxb.android.axml.AxmlWriter$Ns v4_1 = ((pxb.android.axml.AxmlWriter$Ns) v8_1.pop());
            v5.putInt(1048833);
            v5.putInt(24);
            v5.putInt(v4_1.ln);
            v5.putInt(-1);
            v5.putInt(v4_1.prefix.index);
            v5.putInt(v4_1.uri.index);
        }
        return v5.array();
    }

    pxb.android.StringItem update(pxb.android.StringItem p4)
    {
        pxb.android.StringItem v0_0;
        if (p4 != null) {
            int v1 = this.otherString.indexOf(p4);
            if (v1 >= 0) {
                v0_0 = ((pxb.android.StringItem) this.otherString.get(v1));
            } else {
                v0_0 = new pxb.android.StringItem(p4.data);
                this.otherString.add(v0_0);
            }
        } else {
            v0_0 = 0;
        }
        return v0_0;
    }

    pxb.android.StringItem updateNs(pxb.android.StringItem p4)
    {
        pxb.android.StringItem v1 = 0;
        if (p4 != null) {
            String v0 = p4.data;
            if (!this.nses.containsKey(v0)) {
                this.nses.put(v0, 0);
            }
            v1 = this.update(p4);
        }
        return v1;
    }

    pxb.android.StringItem updateWithResourceId(pxb.android.StringItem p6, int p7)
    {
        String v2 = new StringBuilder().append(p6.data).append(p7).toString();
        pxb.android.StringItem v1_1 = ((pxb.android.StringItem) this.resourceId2Str.get(v2));
        if (v1_1 == null) {
            pxb.android.StringItem v0_1 = new pxb.android.StringItem(p6.data);
            this.resourceIds.add(Integer.valueOf(p7));
            this.resourceString.add(v0_1);
            this.resourceId2Str.put(v2, v0_1);
            v1_1 = v0_1;
        }
        return v1_1;
    }
}
