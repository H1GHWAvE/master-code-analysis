package pxb.android.axml;
public class Util {

    public Util()
    {
        return;
    }

    public static void copy(java.io.InputStream p3, java.io.OutputStream p4)
    {
        byte[] v1 = new byte[10240];
        int v0 = p3.read(v1);
        while (v0 > 0) {
            p4.write(v1, 0, v0);
            v0 = p3.read(v1);
        }
        return;
    }

    public static byte[] readFile(java.io.File p3)
    {
        java.io.FileInputStream v0_1 = new java.io.FileInputStream(p3);
        byte[] v1 = new byte[v0_1.available()];
        v0_1.read(v1);
        v0_1.close();
        return v1;
    }

    public static byte[] readIs(java.io.InputStream p2)
    {
        java.io.ByteArrayOutputStream v0_1 = new java.io.ByteArrayOutputStream();
        pxb.android.axml.Util.copy(p2, v0_1);
        return v0_1.toByteArray();
    }

    public static java.util.Map readProguardConfig(java.io.File p7)
    {
        java.util.HashMap v0_1 = new java.util.HashMap();
        java.io.BufferedReader v3_1 = new java.io.BufferedReader(new java.io.InputStreamReader(new java.io.FileInputStream(p7), "utf8"));
        try {
            String v2 = v3_1.readLine();
        } catch (String v4_10) {
            v3_1.close();
            throw v4_10;
        }
        while (v2 != null) {
            if ((!v2.startsWith("#")) && (!v2.startsWith(" "))) {
                int v1 = v2.indexOf("->");
                if (v1 > 0) {
                    v0_1.put(v2.substring(0, v1).trim(), v2.substring((v1 + 2), (v2.length() - 1)).trim());
                }
            }
            v2 = v3_1.readLine();
        }
        v3_1.close();
        return v0_1;
    }

    public static void writeFile(byte[] p1, java.io.File p2)
    {
        java.io.FileOutputStream v0_1 = new java.io.FileOutputStream(p2);
        v0_1.write(p1);
        v0_1.close();
        return;
    }
}
