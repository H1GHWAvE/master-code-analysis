package pxb.android.axml;
public class Axml$Node$Attr {
    public String name;
    public String ns;
    public int resourceId;
    public int type;
    public Object value;

    public Axml$Node$Attr()
    {
        return;
    }

    public void accept(pxb.android.axml.NodeVisitor p7)
    {
        p7.attr(this.ns, this.name, this.resourceId, this.type, this.value);
        return;
    }
}
