package pxb.android.axml;
final class AxmlWriter$1 implements java.util.Comparator {

    AxmlWriter$1()
    {
        return;
    }

    public bridge synthetic int compare(Object p2, Object p3)
    {
        return this.compare(((pxb.android.axml.AxmlWriter$Attr) p2), ((pxb.android.axml.AxmlWriter$Attr) p3));
    }

    public int compare(pxb.android.axml.AxmlWriter$Attr p8, pxb.android.axml.AxmlWriter$Attr p9)
    {
        int v2 = (p8.resourceId - p9.resourceId);
        if (v2 == 0) {
            v2 = p8.name.data.compareTo(p9.name.data);
            if (v2 == 0) {
                int v0;
                if (p8.ns != null) {
                    v0 = 0;
                } else {
                    v0 = 1;
                }
                int v1;
                if (p9.ns != null) {
                    v1 = 0;
                } else {
                    v1 = 1;
                }
                if (v0 == 0) {
                    if (v1 == 0) {
                        v2 = p8.ns.data.compareTo(p9.ns.data);
                    } else {
                        v2 = 1;
                    }
                } else {
                    if (v1 == 0) {
                        v2 = -1;
                    } else {
                        v2 = 0;
                    }
                }
            }
        }
        return v2;
    }
}
