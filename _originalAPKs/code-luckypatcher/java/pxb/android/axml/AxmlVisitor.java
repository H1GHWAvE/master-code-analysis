package pxb.android.axml;
public class AxmlVisitor extends pxb.android.axml.NodeVisitor {

    public AxmlVisitor()
    {
        return;
    }

    public AxmlVisitor(pxb.android.axml.NodeVisitor p1)
    {
        this(p1);
        return;
    }

    public void ns(String p2, String p3, int p4)
    {
        if ((this.nv != null) && ((this.nv instanceof pxb.android.axml.AxmlVisitor))) {
            ((pxb.android.axml.AxmlVisitor) this.nv).ns(p2, p3, p4);
        }
        return;
    }
}
