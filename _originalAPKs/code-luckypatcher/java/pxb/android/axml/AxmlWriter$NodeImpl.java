package pxb.android.axml;
 class AxmlWriter$NodeImpl extends pxb.android.axml.NodeVisitor {
    private java.util.Set attrs;
    private java.util.List children;
    pxb.android.axml.AxmlWriter$Attr clz;
    pxb.android.axml.AxmlWriter$Attr id;
    private int line;
    private pxb.android.StringItem name;
    private pxb.android.StringItem ns;
    pxb.android.axml.AxmlWriter$Attr style;
    private pxb.android.StringItem text;
    private int textLineNumber;

    public AxmlWriter$NodeImpl(String p4, String p5)
    {
        pxb.android.StringItem v0_5;
        pxb.android.StringItem v1_0 = 0;
        this(0);
        this.attrs = new java.util.TreeSet(pxb.android.axml.AxmlWriter.ATTR_CMP);
        this.children = new java.util.ArrayList();
        if (p4 != null) {
            v0_5 = new pxb.android.StringItem(p4);
        } else {
            v0_5 = 0;
        }
        this.ns = v0_5;
        if (p5 != null) {
            v1_0 = new pxb.android.StringItem(p5);
        }
        this.name = v1_0;
        return;
    }

    public void attr(String p7, String p8, int p9, int p10, Object p11)
    {
        if (p8 != null) {
            java.util.Set v3_1;
            if (p7 != null) {
                v3_1 = new pxb.android.StringItem(p7);
            } else {
                v3_1 = 0;
            }
            pxb.android.axml.AxmlWriter$Attr v0_1 = new pxb.android.axml.AxmlWriter$Attr(v3_1, new pxb.android.StringItem(p8), p9);
            v0_1.type = p10;
            if (!(p11 instanceof pxb.android.axml.ValueWrapper)) {
                if (p10 != 3) {
                    v0_1.raw = 0;
                    v0_1.value = p11;
                } else {
                    pxb.android.StringItem v1_1 = new pxb.android.StringItem(((String) p11));
                    v0_1.raw = v1_1;
                    v0_1.value = v1_1;
                }
            } else {
                if (((pxb.android.axml.ValueWrapper) p11).raw != null) {
                    v0_1.raw = new pxb.android.StringItem(((pxb.android.axml.ValueWrapper) p11).raw);
                }
                v0_1.value = Integer.valueOf(((pxb.android.axml.ValueWrapper) p11).ref);
                switch (((pxb.android.axml.ValueWrapper) p11).type) {
                    case 1:
                        this.id = v0_1;
                        break;
                    case 2:
                        this.style = v0_1;
                        break;
                    case 3:
                        this.clz = v0_1;
                        break;
                }
            }
            this.attrs.add(v0_1);
            return;
        } else {
            throw new RuntimeException("name can\'t be null");
        }
    }

    public pxb.android.axml.NodeVisitor child(String p3, String p4)
    {
        pxb.android.axml.AxmlWriter$NodeImpl v0_1 = new pxb.android.axml.AxmlWriter$NodeImpl(p3, p4);
        this.children.add(v0_1);
        return v0_1;
    }

    public void end()
    {
        return;
    }

    public void line(int p1)
    {
        this.line = p1;
        return;
    }

    public int prepare(pxb.android.axml.AxmlWriter p8)
    {
        this.ns = p8.updateNs(this.ns);
        this.name = p8.update(this.name);
        int v1 = 0;
        java.util.Iterator v4_0 = this.attrs.iterator();
        while (v4_0.hasNext()) {
            pxb.android.axml.AxmlWriter$Attr v0_1 = ((pxb.android.axml.AxmlWriter$Attr) v4_0.next());
            int v2 = (v1 + 1);
            v0_1.index = v1;
            v0_1.prepare(p8);
            v1 = v2;
        }
        this.text = p8.update(this.text);
        int v5 = ((this.attrs.size() * 20) + 60);
        java.util.Iterator v4_1 = this.children.iterator();
        while (v4_1.hasNext()) {
            v5 += ((pxb.android.axml.AxmlWriter$NodeImpl) v4_1.next()).prepare(p8);
        }
        if (this.text != null) {
            v5 += 28;
        }
        return v5;
    }

    public void text(int p2, String p3)
    {
        this.text = new pxb.android.StringItem(p3);
        this.textLineNumber = p2;
        return;
    }

    void write(java.nio.ByteBuffer p8)
    {
        int v4_7;
        int v5 = -1;
        p8.putInt(1048834);
        p8.putInt(((this.attrs.size() * 20) + 36));
        p8.putInt(this.line);
        p8.putInt(-1);
        if (this.ns == null) {
            v4_7 = -1;
        } else {
            v4_7 = this.ns.index;
        }
        int v4_18;
        p8.putInt(v4_7);
        p8.putInt(this.name.index);
        p8.putInt(1310740);
        p8.putShort(((short) this.attrs.size()));
        if (this.id != null) {
            v4_18 = (this.id.index + 1);
        } else {
            v4_18 = 0;
        }
        int v4_23;
        p8.putShort(((short) v4_18));
        if (this.clz != null) {
            v4_23 = (this.clz.index + 1);
        } else {
            v4_23 = 0;
        }
        int v4_28;
        p8.putShort(((short) v4_23));
        if (this.style != null) {
            v4_28 = (this.style.index + 1);
        } else {
            v4_28 = 0;
        }
        p8.putShort(((short) v4_28));
        java.util.Iterator v2_0 = this.attrs.iterator();
        while (v2_0.hasNext()) {
            int v4_49;
            pxb.android.axml.AxmlWriter$Attr v0_1 = ((pxb.android.axml.AxmlWriter$Attr) v2_0.next());
            if (v0_1.ns != null) {
                v4_49 = v0_1.ns.index;
            } else {
                v4_49 = -1;
            }
            int v4_53;
            p8.putInt(v4_49);
            p8.putInt(v0_1.name.index);
            if (v0_1.raw == null) {
                v4_53 = -1;
            } else {
                v4_53 = v0_1.raw.index;
            }
            p8.putInt(v4_53);
            p8.putInt(((v0_1.type << 24) | 8));
            Object v3 = v0_1.value;
            if (!(v3 instanceof pxb.android.StringItem)) {
                if (!(v3 instanceof Boolean)) {
                    p8.putInt(((Integer) v0_1.value).intValue());
                } else {
                    int v4_65;
                    if (!Boolean.TRUE.equals(v3)) {
                        v4_65 = 0;
                    } else {
                        v4_65 = -1;
                    }
                    p8.putInt(v4_65);
                }
            } else {
                p8.putInt(((pxb.android.StringItem) v0_1.value).index);
            }
        }
        if (this.text != null) {
            p8.putInt(1048836);
            p8.putInt(28);
            p8.putInt(this.textLineNumber);
            p8.putInt(-1);
            p8.putInt(this.text.index);
            p8.putInt(8);
            p8.putInt(0);
        }
        java.util.Iterator v2_1 = this.children.iterator();
        while (v2_1.hasNext()) {
            ((pxb.android.axml.AxmlWriter$NodeImpl) v2_1.next()).write(p8);
        }
        p8.putInt(1048835);
        p8.putInt(24);
        p8.putInt(-1);
        p8.putInt(-1);
        if (this.ns != null) {
            v5 = this.ns.index;
        }
        p8.putInt(v5);
        p8.putInt(this.name.index);
        return;
    }
}
