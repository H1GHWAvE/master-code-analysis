package pxb.android;
public class StringItem {
    public String data;
    public int dataOffset;
    public int index;

    public StringItem()
    {
        return;
    }

    public StringItem(String p1)
    {
        this.data = p1;
        return;
    }

    public boolean equals(Object p6)
    {
        int v1 = 1;
        if (this != p6) {
            if (p6 != null) {
                if (this.getClass() == p6.getClass()) {
                    if (this.data != null) {
                        if (!this.data.equals(((pxb.android.StringItem) p6).data)) {
                            v1 = 0;
                        }
                    } else {
                        if (((pxb.android.StringItem) p6).data != null) {
                            v1 = 0;
                        }
                    }
                } else {
                    v1 = 0;
                }
            } else {
                v1 = 0;
            }
        }
        return v1;
    }

    public int hashCode()
    {
        int v2_2;
        if (this.data != null) {
            v2_2 = this.data.hashCode();
        } else {
            v2_2 = 0;
        }
        return (v2_2 + 31);
    }

    public String toString()
    {
        Object[] v1_1 = new Object[2];
        v1_1[0] = Integer.valueOf(this.index);
        v1_1[1] = this.data;
        return String.format("S%04d %s", v1_1);
    }
}
