package org.tukaani.xz.common;
public class StreamFlags {
    public long backwardSize;
    public int checkType;

    public StreamFlags()
    {
        this.checkType = -1;
        this.backwardSize = -1;
        return;
    }
}
