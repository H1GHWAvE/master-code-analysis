package org.tukaani.xz;
public class XZFormatException extends org.tukaani.xz.XZIOException {
    private static final long serialVersionUID = 3;

    public XZFormatException()
    {
        this("Input is not in the XZ format");
        return;
    }
}
