package org.tukaani.xz;
 class DeltaEncoder extends org.tukaani.xz.DeltaCoder implements org.tukaani.xz.FilterEncoder {
    private final org.tukaani.xz.DeltaOptions options;
    private final byte[] props;

    DeltaEncoder(org.tukaani.xz.DeltaOptions p4)
    {
        org.tukaani.xz.DeltaOptions v0_1 = new byte[1];
        this.props = v0_1;
        this.props[0] = ((byte) (p4.getDistance() - 1));
        this.options = ((org.tukaani.xz.DeltaOptions) p4.clone());
        return;
    }

    public long getFilterID()
    {
        return 3;
    }

    public byte[] getFilterProps()
    {
        return this.props;
    }

    public org.tukaani.xz.FinishableOutputStream getOutputStream(org.tukaani.xz.FinishableOutputStream p2)
    {
        return this.options.getOutputStream(p2);
    }

    public boolean supportsFlushing()
    {
        return 1;
    }
}
