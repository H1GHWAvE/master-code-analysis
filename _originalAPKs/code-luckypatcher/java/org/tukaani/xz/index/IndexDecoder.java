package org.tukaani.xz.index;
public class IndexDecoder extends org.tukaani.xz.index.IndexBase {
    static final synthetic boolean $assertionsDisabled;
    private long compressedOffset;
    private long largestBlockSize;
    private final int memoryUsage;
    private int recordOffset;
    private final org.tukaani.xz.common.StreamFlags streamFlags;
    private final long streamPadding;
    private final long[] uncompressed;
    private long uncompressedOffset;
    private final long[] unpadded;

    static IndexDecoder()
    {
        int v0_2;
        if (org.tukaani.xz.index.IndexDecoder.desiredAssertionStatus()) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        org.tukaani.xz.index.IndexDecoder.$assertionsDisabled = v0_2;
        return;
    }

    public IndexDecoder(org.tukaani.xz.SeekableInputStream p25, org.tukaani.xz.common.StreamFlags p26, long p27, int p29)
    {
        this(new org.tukaani.xz.CorruptedInputException("XZ Index is corrupt"));
        this.largestBlockSize = 0;
        this.recordOffset = 0;
        this.compressedOffset = 0;
        this.uncompressedOffset = 0;
        this.streamFlags = p26;
        this.streamPadding = p27;
        long v7 = ((p25.position() + p26.backwardSize) - 4);
        java.util.zip.CRC32 v5_1 = new java.util.zip.CRC32();
        java.util.zip.CheckedInputStream v10_1 = new java.util.zip.CheckedInputStream(p25, v5_1);
        if (v10_1.read() == 0) {
            try {
                long v3 = org.tukaani.xz.common.DecoderUtil.decodeVLI(v10_1);
            } catch (java.io.EOFException v6) {
                throw new org.tukaani.xz.CorruptedInputException("XZ Index is corrupt");
            }
            if (v3 < (p26.backwardSize / 2)) {
                if (v3 <= 2147483647) {
                    this.memoryUsage = (((int) (((16 * v3) + 1023) / 1024)) + 1);
                    if ((p29 < 0) || (this.memoryUsage <= p29)) {
                        long v0_20 = new long[((int) v3)];
                        this.unpadded = v0_20;
                        long v0_24 = new long[((int) v3)];
                        this.uncompressed = v0_24;
                        int v13 = 0;
                        int v9_0 = ((int) v3);
                        while (v9_0 > 0) {
                            long v16 = org.tukaani.xz.common.DecoderUtil.decodeVLI(v10_1);
                            long v14 = org.tukaani.xz.common.DecoderUtil.decodeVLI(v10_1);
                            if (p25.position() <= v7) {
                                this.unpadded[v13] = (this.blocksSum + v16);
                                this.uncompressed[v13] = (this.uncompressedSum + v14);
                                v13++;
                                super.add(v16, v14);
                                if ((org.tukaani.xz.index.IndexDecoder.$assertionsDisabled) || (((long) v13) == this.recordCount)) {
                                    if (this.largestBlockSize < v14) {
                                        this.largestBlockSize = v14;
                                    }
                                    v9_0--;
                                } else {
                                    throw new AssertionError();
                                }
                            } else {
                                throw new org.tukaani.xz.CorruptedInputException("XZ Index is corrupt");
                            }
                        }
                        int v11 = this.getIndexPaddingSize();
                        if ((p25.position() + ((long) v11)) != v7) {
                            throw new org.tukaani.xz.CorruptedInputException("XZ Index is corrupt");
                        }
                        do {
                            int v12 = v11;
                            v11 = (v12 - 1);
                            if (v12 <= 0) {
                                long v18 = v5_1.getValue();
                                int v9_1 = 0;
                                while (v9_1 < 4) {
                                    if (((v18 >> (v9_1 * 8)) & 255) == ((long) p25.read())) {
                                        v9_1++;
                                    } else {
                                        throw new org.tukaani.xz.CorruptedInputException("XZ Index is corrupt");
                                    }
                                }
                                return;
                            } else {
                            }
                        } while(v10_1.read() == 0);
                        throw new org.tukaani.xz.CorruptedInputException("XZ Index is corrupt");
                    } else {
                        org.tukaani.xz.CorruptedInputException v20_53 = new org.tukaani.xz.MemoryLimitException;
                        v20_53(this.memoryUsage, p29);
                        throw v20_53;
                    }
                } else {
                    throw new org.tukaani.xz.UnsupportedOptionsException("XZ Index has over 2147483647 Records");
                }
            } else {
                throw new org.tukaani.xz.CorruptedInputException("XZ Index is corrupt");
            }
        } else {
            throw new org.tukaani.xz.CorruptedInputException("XZ Index is corrupt");
        }
    }

    public bridge synthetic long getIndexSize()
    {
        return super.getIndexSize();
    }

    public long getLargestBlockSize()
    {
        return this.largestBlockSize;
    }

    public int getMemoryUsage()
    {
        return this.memoryUsage;
    }

    public int getRecordCount()
    {
        return ((int) this.recordCount);
    }

    public org.tukaani.xz.common.StreamFlags getStreamFlags()
    {
        return this.streamFlags;
    }

    public bridge synthetic long getStreamSize()
    {
        return super.getStreamSize();
    }

    public long getUncompressedSize()
    {
        return this.uncompressedSum;
    }

    public boolean hasRecord(int p7)
    {
        if ((p7 < this.recordOffset) || (((long) p7) >= (((long) this.recordOffset) + this.recordCount))) {
            int v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        return v0_3;
    }

    public boolean hasUncompressedOffset(long p5)
    {
        if ((p5 < this.uncompressedOffset) || (p5 >= (this.uncompressedOffset + this.uncompressedSum))) {
            int v0_5 = 0;
        } else {
            v0_5 = 1;
        }
        return v0_5;
    }

    public void locateBlock(org.tukaani.xz.index.BlockInfo p6, long p7)
    {
        if ((org.tukaani.xz.index.IndexDecoder.$assertionsDisabled) || (p7 >= this.uncompressedOffset)) {
            long v7_1 = (p7 - this.uncompressedOffset);
            if ((org.tukaani.xz.index.IndexDecoder.$assertionsDisabled) || (v7_1 < this.uncompressedSum)) {
                int v1 = 0;
                int v2 = (this.unpadded.length - 1);
                while (v1 < v2) {
                    int v0 = (v1 + ((v2 - v1) / 2));
                    if (this.uncompressed[v0] > v7_1) {
                        v2 = v0;
                    } else {
                        v1 = (v0 + 1);
                    }
                }
                this.setBlockInfo(p6, (this.recordOffset + v1));
                return;
            } else {
                throw new AssertionError();
            }
        } else {
            throw new AssertionError();
        }
    }

    public void setBlockInfo(org.tukaani.xz.index.BlockInfo p8, int p9)
    {
        if ((org.tukaani.xz.index.IndexDecoder.$assertionsDisabled) || (p9 >= this.recordOffset)) {
            if ((org.tukaani.xz.index.IndexDecoder.$assertionsDisabled) || (((long) (p9 - this.recordOffset)) < this.recordCount)) {
                p8.index = this;
                p8.blockNumber = p9;
                int v0 = (p9 - this.recordOffset);
                if (v0 != 0) {
                    p8.compressedOffset = ((this.unpadded[(v0 - 1)] + 3) & -4);
                    p8.uncompressedOffset = this.uncompressed[(v0 - 1)];
                } else {
                    p8.compressedOffset = 0;
                    p8.uncompressedOffset = 0;
                }
                p8.unpaddedSize = (this.unpadded[v0] - p8.compressedOffset);
                p8.uncompressedSize = (this.uncompressed[v0] - p8.uncompressedOffset);
                p8.compressedOffset = (p8.compressedOffset + (this.compressedOffset + 12));
                p8.uncompressedOffset = (p8.uncompressedOffset + this.uncompressedOffset);
                return;
            } else {
                throw new AssertionError();
            }
        } else {
            throw new AssertionError();
        }
    }

    public void setOffsets(org.tukaani.xz.index.IndexDecoder p5)
    {
        this.recordOffset = (p5.recordOffset + ((int) p5.recordCount));
        this.compressedOffset = ((p5.compressedOffset + p5.getStreamSize()) + p5.streamPadding);
        if ((org.tukaani.xz.index.IndexDecoder.$assertionsDisabled) || ((this.compressedOffset & 3) == 0)) {
            this.uncompressedOffset = (p5.uncompressedOffset + p5.uncompressedSum);
            return;
        } else {
            throw new AssertionError();
        }
    }
}
