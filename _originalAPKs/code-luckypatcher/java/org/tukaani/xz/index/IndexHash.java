package org.tukaani.xz.index;
public class IndexHash extends org.tukaani.xz.index.IndexBase {
    private org.tukaani.xz.check.Check hash;

    public IndexHash()
    {
        this(new org.tukaani.xz.CorruptedInputException());
        try {
            this.hash = new org.tukaani.xz.check.SHA256();
        } catch (java.security.NoSuchAlgorithmException v0) {
            this.hash = new org.tukaani.xz.check.CRC32();
        }
        return;
    }

    public void add(long p4, long p6)
    {
        super.add(p4, p6);
        java.nio.ByteBuffer v0 = java.nio.ByteBuffer.allocate(16);
        v0.putLong(p4);
        v0.putLong(p6);
        this.hash.update(v0.array());
        return;
    }

    public bridge synthetic long getIndexSize()
    {
        return super.getIndexSize();
    }

    public bridge synthetic long getStreamSize()
    {
        return super.getStreamSize();
    }

    public void validate(java.io.InputStream p22)
    {
        java.util.zip.CRC32 v2_1 = new java.util.zip.CRC32();
        v2_1.update(0);
        java.util.zip.CheckedInputStream v6_1 = new java.util.zip.CheckedInputStream(p22, v2_1);
        if (org.tukaani.xz.common.DecoderUtil.decodeVLI(v6_1) == this.recordCount) {
            org.tukaani.xz.index.IndexHash v8_1 = new org.tukaani.xz.index.IndexHash();
            int v4_0 = 0;
            while (v4_0 < this.recordCount) {
                try {
                    v8_1.add(org.tukaani.xz.common.DecoderUtil.decodeVLI(v6_1), org.tukaani.xz.common.DecoderUtil.decodeVLI(v6_1));
                } catch (org.tukaani.xz.XZIOException v3) {
                    throw new org.tukaani.xz.CorruptedInputException("XZ Index is corrupt");
                }
                if ((v8_1.blocksSum <= this.blocksSum) && ((v8_1.uncompressedSum <= this.uncompressedSum) && (v8_1.indexListSize <= this.indexListSize))) {
                    v4_0++;
                } else {
                    throw new org.tukaani.xz.CorruptedInputException("XZ Index is corrupt");
                }
            }
            if ((v8_1.blocksSum == this.blocksSum) && ((v8_1.uncompressedSum == this.uncompressedSum) && ((v8_1.indexListSize == this.indexListSize) && (java.util.Arrays.equals(v8_1.hash.finish(), this.hash.finish()))))) {
                java.io.DataInputStream v7_1 = new java.io.DataInputStream(v6_1);
                int v4_1 = this.getIndexPaddingSize();
                while (v4_1 > 0) {
                    if (v7_1.readUnsignedByte() == 0) {
                        v4_1--;
                    } else {
                        throw new org.tukaani.xz.CorruptedInputException("XZ Index is corrupt");
                    }
                }
                long v15 = v2_1.getValue();
                int v4_2 = 0;
                while (v4_2 < 4) {
                    if (((v15 >> (v4_2 * 8)) & 255) == ((long) v7_1.readUnsignedByte())) {
                        v4_2++;
                    } else {
                        throw new org.tukaani.xz.CorruptedInputException("XZ Index is corrupt");
                    }
                }
                return;
            } else {
                throw new org.tukaani.xz.CorruptedInputException("XZ Index is corrupt");
            }
        } else {
            throw new org.tukaani.xz.CorruptedInputException("XZ Index is corrupt");
        }
    }
}
