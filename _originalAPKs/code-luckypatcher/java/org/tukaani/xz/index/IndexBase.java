package org.tukaani.xz.index;
abstract class IndexBase {
    long blocksSum;
    long indexListSize;
    private final org.tukaani.xz.XZIOException invalidIndexException;
    long recordCount;
    long uncompressedSum;

    IndexBase(org.tukaani.xz.XZIOException p3)
    {
        this.blocksSum = 0;
        this.uncompressedSum = 0;
        this.indexListSize = 0;
        this.recordCount = 0;
        this.invalidIndexException = p3;
        return;
    }

    private long getUnpaddedIndexSize()
    {
        return ((((long) (org.tukaani.xz.common.Util.getVLISize(this.recordCount) + 1)) + this.indexListSize) + 4);
    }

    void add(long p9, long p11)
    {
        this.blocksSum = (this.blocksSum + ((3 + p9) & -4));
        this.uncompressedSum = (this.uncompressedSum + p11);
        this.indexListSize = (this.indexListSize + ((long) (org.tukaani.xz.common.Util.getVLISize(p9) + org.tukaani.xz.common.Util.getVLISize(p11))));
        this.recordCount = (this.recordCount + 1);
        if ((this.blocksSum >= 0) && ((this.uncompressedSum >= 0) && ((this.getIndexSize() <= 8.487983164e-314) && (this.getStreamSize() >= 0)))) {
            return;
        } else {
            throw this.invalidIndexException;
        }
    }

    int getIndexPaddingSize()
    {
        return ((int) ((4 - this.getUnpaddedIndexSize()) & 3));
    }

    public long getIndexSize()
    {
        return ((this.getUnpaddedIndexSize() + 3) & -4);
    }

    public long getStreamSize()
    {
        return (((this.blocksSum + 12) + this.getIndexSize()) + 12);
    }
}
