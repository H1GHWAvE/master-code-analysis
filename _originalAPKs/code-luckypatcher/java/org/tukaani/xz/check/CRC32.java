package org.tukaani.xz.check;
public class CRC32 extends org.tukaani.xz.check.Check {
    private final java.util.zip.CRC32 state;

    public CRC32()
    {
        this.state = new java.util.zip.CRC32();
        this.size = 4;
        this.name = "CRC32";
        return;
    }

    public byte[] finish()
    {
        long v1 = this.state.getValue();
        byte[] v0 = new byte[4];
        v0[0] = ((byte) ((int) v1));
        v0[1] = ((byte) ((int) (v1 >> 8)));
        v0[2] = ((byte) ((int) (v1 >> 16)));
        v0[3] = ((byte) ((int) (v1 >> 24)));
        this.state.reset();
        return v0;
    }

    public void update(byte[] p2, int p3, int p4)
    {
        this.state.update(p2, p3, p4);
        return;
    }
}
