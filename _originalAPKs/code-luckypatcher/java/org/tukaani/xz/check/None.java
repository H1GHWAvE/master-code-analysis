package org.tukaani.xz.check;
public class None extends org.tukaani.xz.check.Check {

    public None()
    {
        this.size = 0;
        this.name = "None";
        return;
    }

    public byte[] finish()
    {
        byte[] v0 = new byte[0];
        return v0;
    }

    public void update(byte[] p1, int p2, int p3)
    {
        return;
    }
}
