package org.tukaani.xz.check;
public abstract class Check {
    String name;
    int size;

    public Check()
    {
        return;
    }

    public static org.tukaani.xz.check.Check getInstance(int p3)
    {
        java.security.NoSuchAlgorithmException v0_1;
        switch (p3) {
            case 0:
                v0_1 = new org.tukaani.xz.check.None();
                return v0_1;
            case 1:
                v0_1 = new org.tukaani.xz.check.CRC32();
                return v0_1;
            case 4:
                v0_1 = new org.tukaani.xz.check.CRC64();
                return v0_1;
            case 10:
                try {
                    v0_1 = new org.tukaani.xz.check.SHA256();
                } catch (java.security.NoSuchAlgorithmException v0) {
                }
                return v0_1;
        }
        throw new org.tukaani.xz.UnsupportedOptionsException(new StringBuilder().append("Unsupported Check ID ").append(p3).toString());
    }

    public abstract byte[] finish();

    public String getName()
    {
        return this.name;
    }

    public int getSize()
    {
        return this.size;
    }

    public void update(byte[] p3)
    {
        this.update(p3, 0, p3.length);
        return;
    }

    public abstract void update();
}
