package org.tukaani.xz;
 class BCJEncoder extends org.tukaani.xz.BCJCoder implements org.tukaani.xz.FilterEncoder {
    static final synthetic boolean $assertionsDisabled;
    private final long filterID;
    private final org.tukaani.xz.BCJOptions options;
    private final byte[] props;

    static BCJEncoder()
    {
        int v0_2;
        if (org.tukaani.xz.BCJEncoder.desiredAssertionStatus()) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        org.tukaani.xz.BCJEncoder.$assertionsDisabled = v0_2;
        return;
    }

    BCJEncoder(org.tukaani.xz.BCJOptions p6, long p7)
    {
        if ((org.tukaani.xz.BCJEncoder.$assertionsDisabled) || (org.tukaani.xz.BCJEncoder.isBCJFilterID(p7))) {
            int v1 = p6.getStartOffset();
            if (v1 != 0) {
                byte[] v2_2 = new byte[4];
                this.props = v2_2;
                int v0 = 0;
                while (v0 < 4) {
                    this.props[v0] = ((byte) (v1 >> (v0 * 8)));
                    v0++;
                }
            } else {
                byte[] v2_5 = new byte[0];
                this.props = v2_5;
            }
            this.filterID = p7;
            this.options = ((org.tukaani.xz.BCJOptions) p6.clone());
            return;
        } else {
            throw new AssertionError();
        }
    }

    public long getFilterID()
    {
        return this.filterID;
    }

    public byte[] getFilterProps()
    {
        return this.props;
    }

    public org.tukaani.xz.FinishableOutputStream getOutputStream(org.tukaani.xz.FinishableOutputStream p2)
    {
        return this.options.getOutputStream(p2);
    }

    public boolean supportsFlushing()
    {
        return 0;
    }
}
