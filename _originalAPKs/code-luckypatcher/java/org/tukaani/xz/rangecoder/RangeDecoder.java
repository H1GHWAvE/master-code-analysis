package org.tukaani.xz.rangecoder;
public abstract class RangeDecoder extends org.tukaani.xz.rangecoder.RangeCoder {
    int code;
    int range;

    public RangeDecoder()
    {
        this.range = 0;
        this.code = 0;
        return;
    }

    public int decodeBit(short[] p6, int p7)
    {
        int v0;
        this.normalize();
        short v2 = p6[p7];
        int v1 = ((this.range >> 11) * v2);
        if ((this.code ^ -2147483648) >= (-2147483648 ^ v1)) {
            this.range = (this.range - v1);
            this.code = (this.code - v1);
            p6[p7] = ((short) (v2 - (v2 >> 5)));
            v0 = 1;
        } else {
            this.range = v1;
            p6[p7] = ((short) (((2048 - v2) >> 5) + v2));
            v0 = 0;
        }
        return v0;
    }

    public int decodeBitTree(short[] p4)
    {
        int v0 = 1;
        do {
            v0 = ((v0 << 1) | this.decodeBit(p4, v0));
        } while(v0 < p4.length);
        return (v0 - p4.length);
    }

    public int decodeDirectBits(int p6)
    {
        int v0 = 0;
        do {
            this.normalize();
            this.range = (this.range >> 1);
            int v1 = ((this.code - this.range) >> 31);
            this.code = (this.code - (this.range & (v1 - 1)));
            v0 = ((v0 << 1) | (1 - v1));
            p6--;
        } while(p6 != 0);
        return v0;
    }

    public int decodeReverseBitTree(short[] p7)
    {
        int v4 = 1;
        int v1 = 0;
        int v3 = 0;
        while(true) {
            int v0 = this.decodeBit(p7, v4);
            v4 = ((v4 << 1) | v0);
            v3 |= (v0 << v1);
            if (v4 >= p7.length) {
                break;
            }
            v1++;
        }
        return v3;
    }

    public abstract void normalize();
}
