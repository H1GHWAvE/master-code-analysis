package org.tukaani.xz.rangecoder;
public final class RangeDecoderFromBuffer extends org.tukaani.xz.rangecoder.RangeDecoder {
    private static final int INIT_SIZE = 5;
    private final byte[] buf;
    private int end;
    private int pos;

    public RangeDecoderFromBuffer(int p2)
    {
        this.pos = 0;
        this.end = 0;
        byte[] v0_2 = new byte[(p2 - 5)];
        this.buf = v0_2;
        return;
    }

    public boolean isFinished()
    {
        if ((this.pos != this.end) || (this.code != 0)) {
            int v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    public boolean isInBufferOK()
    {
        int v0_1;
        if (this.pos > this.end) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    public void normalize()
    {
        if ((this.range & -16777216) == 0) {
            try {
                org.tukaani.xz.CorruptedInputException v1_3 = (this.code << 8);
                int v2_1 = this.buf;
                int v3 = this.pos;
                this.pos = (v3 + 1);
                this.code = (v1_3 | (v2_1[v3] & 255));
                this.range = (this.range << 8);
            } catch (ArrayIndexOutOfBoundsException v0) {
                throw new org.tukaani.xz.CorruptedInputException();
            }
        }
        return;
    }

    public void prepareInputBuffer(java.io.DataInputStream p4, int p5)
    {
        if (p5 >= 5) {
            if (p4.readUnsignedByte() == 0) {
                this.code = p4.readInt();
                this.range = -1;
                this.pos = 0;
                this.end = (p5 - 5);
                p4.readFully(this.buf, 0, this.end);
                return;
            } else {
                throw new org.tukaani.xz.CorruptedInputException();
            }
        } else {
            throw new org.tukaani.xz.CorruptedInputException();
        }
    }
}
