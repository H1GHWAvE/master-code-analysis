package org.tukaani.xz;
 class LZMA2OutputStream extends org.tukaani.xz.FinishableOutputStream {
    static final synthetic boolean $assertionsDisabled = False;
    static final int COMPRESSED_SIZE_MAX = 65536;
    private boolean dictResetNeeded;
    private java.io.IOException exception;
    private boolean finished;
    private final org.tukaani.xz.lz.LZEncoder lz;
    private final org.tukaani.xz.lzma.LZMAEncoder lzma;
    private org.tukaani.xz.FinishableOutputStream out;
    private final java.io.DataOutputStream outData;
    private int pendingSize;
    private final int props;
    private boolean propsNeeded;
    private final org.tukaani.xz.rangecoder.RangeEncoder rc;
    private boolean stateResetNeeded;
    private final byte[] tempBuf;

    static LZMA2OutputStream()
    {
        int v0_2;
        if (org.tukaani.xz.LZMA2OutputStream.desiredAssertionStatus()) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        org.tukaani.xz.LZMA2OutputStream.$assertionsDisabled = v0_2;
        return;
    }

    LZMA2OutputStream(org.tukaani.xz.FinishableOutputStream p13, org.tukaani.xz.LZMA2Options p14)
    {
        this.dictResetNeeded = 1;
        this.stateResetNeeded = 1;
        this.propsNeeded = 1;
        this.pendingSize = 0;
        this.finished = 0;
        this.exception = 0;
        org.tukaani.xz.lz.LZEncoder v0_1 = new byte[1];
        this.tempBuf = v0_1;
        if (p13 != null) {
            this.out = p13;
            this.outData = new java.io.DataOutputStream(p13);
            this.rc = new org.tukaani.xz.rangecoder.RangeEncoder(65536);
            int v5 = p14.getDictSize();
            this.lzma = org.tukaani.xz.lzma.LZMAEncoder.getInstance(this.rc, p14.getLc(), p14.getLp(), p14.getPb(), p14.getMode(), v5, org.tukaani.xz.LZMA2OutputStream.getExtraSizeBefore(v5), p14.getNiceLen(), p14.getMatchFinder(), p14.getDepthLimit());
            this.lz = this.lzma.getLZEncoder();
            byte[] v10 = p14.getPresetDict();
            if ((v10 != null) && (v10.length > 0)) {
                this.lz.setPresetDict(v5, v10);
                this.dictResetNeeded = 0;
            }
            this.props = ((((p14.getPb() * 5) + p14.getLp()) * 9) + p14.getLc());
            return;
        } else {
            throw new NullPointerException();
        }
    }

    private static int getExtraSizeBefore(int p1)
    {
        int v0_1;
        if (65536 <= p1) {
            v0_1 = 0;
        } else {
            v0_1 = (65536 - p1);
        }
        return v0_1;
    }

    static int getMemoryUsage(org.tukaani.xz.LZMA2Options p4)
    {
        int v0 = p4.getDictSize();
        return (org.tukaani.xz.lzma.LZMAEncoder.getMemoryUsage(p4.getMode(), v0, org.tukaani.xz.LZMA2OutputStream.getExtraSizeBefore(v0), p4.getMatchFinder()) + 70);
    }

    private void writeChunk()
    {
        int v0 = this.rc.finish();
        int v1 = this.lzma.getUncompressedSize();
        if ((org.tukaani.xz.LZMA2OutputStream.$assertionsDisabled) || (v0 > 0)) {
            if ((org.tukaani.xz.LZMA2OutputStream.$assertionsDisabled) || (v1 > 0)) {
                if ((v0 + 2) >= v1) {
                    this.lzma.reset();
                    v1 = this.lzma.getUncompressedSize();
                    if ((org.tukaani.xz.LZMA2OutputStream.$assertionsDisabled) || (v1 > 0)) {
                        this.writeUncompressed(v1);
                    } else {
                        throw new AssertionError(v1);
                    }
                } else {
                    this.writeLZMA(v1, v0);
                }
                this.pendingSize = (this.pendingSize - v1);
                this.lzma.resetUncompressedSize();
                this.rc.reset();
                return;
            } else {
                throw new AssertionError(v1);
            }
        } else {
            throw new AssertionError(v0);
        }
    }

    private void writeEndMarker()
    {
        if ((org.tukaani.xz.LZMA2OutputStream.$assertionsDisabled) || (!this.finished)) {
            if (this.exception == null) {
                this.lz.setFinishing();
                try {
                    while (this.pendingSize > 0) {
                        this.lzma.encodeForLZMA2();
                        this.writeChunk();
                    }
                } catch (java.io.IOException v0) {
                    this.exception = v0;
                    throw v0;
                }
                this.out.write(0);
                this.finished = 1;
                return;
            } else {
                throw this.exception;
            }
        } else {
            throw new AssertionError();
        }
    }

    private void writeLZMA(int p5, int p6)
    {
        int v0_0;
        if (!this.propsNeeded) {
            if (!this.stateResetNeeded) {
                v0_0 = 128;
            } else {
                v0_0 = 160;
            }
        } else {
            if (!this.dictResetNeeded) {
                v0_0 = 192;
            } else {
                v0_0 = 224;
            }
        }
        this.outData.writeByte((v0_0 | ((p5 - 1) >> 16)));
        this.outData.writeShort((p5 - 1));
        this.outData.writeShort((p6 - 1));
        if (this.propsNeeded) {
            this.outData.writeByte(this.props);
        }
        this.rc.write(this.out);
        this.propsNeeded = 0;
        this.stateResetNeeded = 0;
        this.dictResetNeeded = 0;
        return;
    }

    private void writeUncompressed(int p5)
    {
        while (p5 > 0) {
            int v1_2;
            int v0 = Math.min(p5, 65536);
            if (!this.dictResetNeeded) {
                v1_2 = 2;
            } else {
                v1_2 = 1;
            }
            this.outData.writeByte(v1_2);
            this.outData.writeShort((v0 - 1));
            this.lz.copyUncompressed(this.out, p5, v0);
            p5 -= v0;
            this.dictResetNeeded = 0;
        }
        this.stateResetNeeded = 1;
        return;
    }

    public void close()
    {
        if (this.out != null) {
            if (!this.finished) {
                try {
                    this.writeEndMarker();
                } catch (int v1) {
                }
                this.out.close();
                this.out = 0;
                if (this.exception == null) {
                    return;
                } else {
                    throw this.exception;
                }
            }
            try {
            } catch (java.io.IOException v0) {
                if (this.exception != null) {
                } else {
                    this.exception = v0;
                }
            }
        }
    }

    public void finish()
    {
        if (!this.finished) {
            this.writeEndMarker();
            try {
                this.out.finish();
                this.finished = 1;
            } catch (java.io.IOException v0) {
                this.exception = v0;
                throw v0;
            }
        }
        return;
    }

    public void flush()
    {
        if (this.exception == null) {
            if (!this.finished) {
                try {
                    this.lz.setFlushing();
                } catch (java.io.IOException v0) {
                    this.exception = v0;
                    throw v0;
                }
                while (this.pendingSize > 0) {
                    this.lzma.encodeForLZMA2();
                    this.writeChunk();
                }
                this.out.flush();
                return;
            } else {
                throw new org.tukaani.xz.XZIOException("Stream finished or closed");
            }
        } else {
            throw this.exception;
        }
    }

    public void write(int p4)
    {
        this.tempBuf[0] = ((byte) p4);
        this.write(this.tempBuf, 0, 1);
        return;
    }

    public void write(byte[] p5, int p6, int p7)
    {
        if ((p6 >= 0) && ((p7 >= 0) && (((p6 + p7) >= 0) && ((p6 + p7) <= p5.length)))) {
            if (this.exception == null) {
                if (this.finished) {
                    throw new org.tukaani.xz.XZIOException("Stream finished or closed");
                }
                while (p7 > 0) {
                    try {
                        int v1 = this.lz.fillWindow(p5, p6, p7);
                        p6 += v1;
                        p7 -= v1;
                        this.pendingSize = (this.pendingSize + v1);
                    } catch (java.io.IOException v0) {
                        this.exception = v0;
                        throw v0;
                    }
                    if (this.lzma.encodeForLZMA2()) {
                        this.writeChunk();
                    }
                }
                return;
            } else {
                throw this.exception;
            }
        } else {
            throw new IndexOutOfBoundsException();
        }
    }
}
