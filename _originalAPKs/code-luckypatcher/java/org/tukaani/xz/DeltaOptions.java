package org.tukaani.xz;
public class DeltaOptions extends org.tukaani.xz.FilterOptions {
    static final synthetic boolean $assertionsDisabled = False;
    public static final int DISTANCE_MAX = 256;
    public static final int DISTANCE_MIN = 1;
    private int distance;

    static DeltaOptions()
    {
        int v0_2;
        if (org.tukaani.xz.DeltaOptions.desiredAssertionStatus()) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        org.tukaani.xz.DeltaOptions.$assertionsDisabled = v0_2;
        return;
    }

    public DeltaOptions()
    {
        this.distance = 1;
        return;
    }

    public DeltaOptions(int p2)
    {
        this.distance = 1;
        this.setDistance(p2);
        return;
    }

    public Object clone()
    {
        try {
            return super.clone();
        } catch (CloneNotSupportedException v0) {
            if (org.tukaani.xz.DeltaOptions.$assertionsDisabled) {
                throw new RuntimeException();
            } else {
                throw new AssertionError();
            }
        }
    }

    public int getDecoderMemoryUsage()
    {
        return 1;
    }

    public int getDistance()
    {
        return this.distance;
    }

    public int getEncoderMemoryUsage()
    {
        return org.tukaani.xz.DeltaOutputStream.getMemoryUsage();
    }

    org.tukaani.xz.FilterEncoder getFilterEncoder()
    {
        return new org.tukaani.xz.DeltaEncoder(this);
    }

    public java.io.InputStream getInputStream(java.io.InputStream p3)
    {
        return new org.tukaani.xz.DeltaInputStream(p3, this.distance);
    }

    public org.tukaani.xz.FinishableOutputStream getOutputStream(org.tukaani.xz.FinishableOutputStream p2)
    {
        return new org.tukaani.xz.DeltaOutputStream(p2, this);
    }

    public void setDistance(int p4)
    {
        if ((p4 >= 1) && (p4 <= 256)) {
            this.distance = p4;
            return;
        } else {
            throw new org.tukaani.xz.UnsupportedOptionsException(new StringBuilder().append("Delta distance must be in the range [1, 256]: ").append(p4).toString());
        }
    }
}
