package org.tukaani.xz;
 class CountingOutputStream extends org.tukaani.xz.FinishableOutputStream {
    private final java.io.OutputStream out;
    private long size;

    public CountingOutputStream(java.io.OutputStream p3)
    {
        this.size = 0;
        this.out = p3;
        return;
    }

    public void close()
    {
        this.out.close();
        return;
    }

    public void flush()
    {
        this.out.flush();
        return;
    }

    public long getSize()
    {
        return this.size;
    }

    public void write(int p5)
    {
        this.out.write(p5);
        if (this.size >= 0) {
            this.size = (this.size + 1);
        }
        return;
    }

    public void write(byte[] p5, int p6, int p7)
    {
        this.out.write(p5, p6, p7);
        if (this.size >= 0) {
            this.size = (this.size + ((long) p7));
        }
        return;
    }
}
