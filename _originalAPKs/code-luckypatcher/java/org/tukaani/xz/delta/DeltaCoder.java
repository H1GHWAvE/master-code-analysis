package org.tukaani.xz.delta;
abstract class DeltaCoder {
    static final int DISTANCE_MASK = 255;
    static final int DISTANCE_MAX = 256;
    static final int DISTANCE_MIN = 1;
    final int distance;
    final byte[] history;
    int pos;

    DeltaCoder(int p3)
    {
        IllegalArgumentException v0_0 = new byte[256];
        this.history = v0_0;
        this.pos = 0;
        if ((p3 >= 1) && (p3 <= 256)) {
            this.distance = p3;
            return;
        } else {
            throw new IllegalArgumentException();
        }
    }
}
