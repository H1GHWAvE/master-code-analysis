package org.tukaani.xz.delta;
public class DeltaDecoder extends org.tukaani.xz.delta.DeltaCoder {

    public DeltaDecoder(int p1)
    {
        this(p1);
        return;
    }

    public void decode(byte[] p7, int p8, int p9)
    {
        int v0 = (p8 + p9);
        int v1 = p8;
        while (v1 < v0) {
            p7[v1] = ((byte) (p7[v1] + this.history[((this.distance + this.pos) & 255)]));
            byte[] v2_3 = this.history;
            int v3_2 = this.pos;
            this.pos = (v3_2 - 1);
            v2_3[(v3_2 & 255)] = p7[v1];
            v1++;
        }
        return;
    }
}
