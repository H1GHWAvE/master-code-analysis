package org.tukaani.xz;
abstract class BCJOptions extends org.tukaani.xz.FilterOptions {
    static final synthetic boolean $assertionsDisabled;
    private final int alignment;
    int startOffset;

    static BCJOptions()
    {
        int v0_2;
        if (org.tukaani.xz.BCJOptions.desiredAssertionStatus()) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        org.tukaani.xz.BCJOptions.$assertionsDisabled = v0_2;
        return;
    }

    BCJOptions(int p2)
    {
        this.startOffset = 0;
        this.alignment = p2;
        return;
    }

    public Object clone()
    {
        try {
            return super.clone();
        } catch (CloneNotSupportedException v0) {
            if (org.tukaani.xz.BCJOptions.$assertionsDisabled) {
                throw new RuntimeException();
            } else {
                throw new AssertionError();
            }
        }
    }

    public int getDecoderMemoryUsage()
    {
        return org.tukaani.xz.SimpleInputStream.getMemoryUsage();
    }

    public int getEncoderMemoryUsage()
    {
        return org.tukaani.xz.SimpleOutputStream.getMemoryUsage();
    }

    public int getStartOffset()
    {
        return this.startOffset;
    }

    public void setStartOffset(int p4)
    {
        if (((this.alignment - 1) & p4) == 0) {
            this.startOffset = p4;
            return;
        } else {
            throw new org.tukaani.xz.UnsupportedOptionsException(new StringBuilder().append("Start offset must be a multiple of ").append(this.alignment).toString());
        }
    }
}
