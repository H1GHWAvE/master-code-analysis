package org.tukaani.xz;
public class FinishableWrapperOutputStream extends org.tukaani.xz.FinishableOutputStream {
    protected java.io.OutputStream out;

    public FinishableWrapperOutputStream(java.io.OutputStream p1)
    {
        this.out = p1;
        return;
    }

    public void close()
    {
        this.out.close();
        return;
    }

    public void flush()
    {
        this.out.flush();
        return;
    }

    public void write(int p2)
    {
        this.out.write(p2);
        return;
    }

    public void write(byte[] p2)
    {
        this.out.write(p2);
        return;
    }

    public void write(byte[] p2, int p3, int p4)
    {
        this.out.write(p2, p3, p4);
        return;
    }
}
