package org.tukaani.xz;
 class DeltaOutputStream extends org.tukaani.xz.FinishableOutputStream {
    private static final int FILTER_BUF_SIZE = 4096;
    private final org.tukaani.xz.delta.DeltaEncoder delta;
    private java.io.IOException exception;
    private final byte[] filterBuf;
    private boolean finished;
    private org.tukaani.xz.FinishableOutputStream out;
    private final byte[] tempBuf;

    DeltaOutputStream(org.tukaani.xz.FinishableOutputStream p3, org.tukaani.xz.DeltaOptions p4)
    {
        org.tukaani.xz.delta.DeltaEncoder v0_1 = new byte[4096];
        this.filterBuf = v0_1;
        this.finished = 0;
        this.exception = 0;
        org.tukaani.xz.delta.DeltaEncoder v0_5 = new byte[1];
        this.tempBuf = v0_5;
        this.out = p3;
        this.delta = new org.tukaani.xz.delta.DeltaEncoder(p4.getDistance());
        return;
    }

    static int getMemoryUsage()
    {
        return 5;
    }

    public void close()
    {
        if (this.out != null) {
            try {
                this.out.close();
            } catch (java.io.IOException v0) {
                if (this.exception != null) {
                } else {
                    this.exception = v0;
                }
            }
            this.out = 0;
        }
        if (this.exception == null) {
            return;
        } else {
            throw this.exception;
        }
    }

    public void finish()
    {
        if (!this.finished) {
            if (this.exception == null) {
                try {
                    this.out.finish();
                    this.finished = 1;
                } catch (java.io.IOException v0) {
                    this.exception = v0;
                    throw v0;
                }
            } else {
                throw this.exception;
            }
        }
        return;
    }

    public void flush()
    {
        if (this.exception == null) {
            if (!this.finished) {
                try {
                    this.out.flush();
                    return;
                } catch (java.io.IOException v0) {
                    this.exception = v0;
                    throw v0;
                }
            } else {
                throw new org.tukaani.xz.XZIOException("Stream finished or closed");
            }
        } else {
            throw this.exception;
        }
    }

    public void write(int p4)
    {
        this.tempBuf[0] = ((byte) p4);
        this.write(this.tempBuf, 0, 1);
        return;
    }

    public void write(byte[] p6, int p7, int p8)
    {
        if ((p7 >= 0) && ((p8 >= 0) && (((p7 + p8) >= 0) && ((p7 + p8) <= p6.length)))) {
            if (this.exception == null) {
                if (this.finished) {
                    throw new org.tukaani.xz.XZIOException("Stream finished");
                }
                while (p8 > 4096) {
                    try {
                        this.delta.encode(p6, p7, 4096, this.filterBuf);
                        this.out.write(this.filterBuf);
                        p7 += 4096;
                        p8 += -4096;
                    } catch (java.io.IOException v0) {
                        this.exception = v0;
                        throw v0;
                    }
                }
                this.delta.encode(p6, p7, p8, this.filterBuf);
                this.out.write(this.filterBuf, 0, p8);
                return;
            } else {
                throw this.exception;
            }
        } else {
            throw new IndexOutOfBoundsException();
        }
    }
}
