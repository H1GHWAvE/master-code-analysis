package org.tukaani.xz.lz;
public final class LZDecoder {
    private final byte[] buf;
    private int full;
    private int limit;
    private int pendingDist;
    private int pendingLen;
    private int pos;
    private int start;

    public LZDecoder(int p5, byte[] p6)
    {
        this.start = 0;
        this.pos = 0;
        this.full = 0;
        this.limit = 0;
        this.pendingLen = 0;
        this.pendingDist = 0;
        int v0_0 = new byte[p5];
        this.buf = v0_0;
        if (p6 != null) {
            this.pos = Math.min(p6.length, p5);
            this.full = this.pos;
            this.start = this.pos;
            System.arraycopy(p6, (p6.length - this.pos), this.buf, 0, this.pos);
        }
        return;
    }

    public void copyUncompressed(java.io.DataInputStream p4, int p5)
    {
        int v0 = Math.min((this.buf.length - this.pos), p5);
        p4.readFully(this.buf, this.pos, v0);
        this.pos = (this.pos + v0);
        if (this.full < this.pos) {
            this.full = this.pos;
        }
        return;
    }

    public int flush(byte[] p4, int p5)
    {
        int v0 = (this.pos - this.start);
        if (this.pos == this.buf.length) {
            this.pos = 0;
        }
        System.arraycopy(this.buf, this.start, p4, p5, v0);
        this.start = this.pos;
        return v0;
    }

    public int getByte(int p3)
    {
        int v0 = ((this.pos - p3) - 1);
        if (p3 >= this.pos) {
            v0 += this.buf.length;
        }
        return (this.buf[v0] & 255);
    }

    public int getPos()
    {
        return this.pos;
    }

    public boolean hasPending()
    {
        int v0_1;
        if (this.pendingLen <= 0) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    public boolean hasSpace()
    {
        int v0_1;
        if (this.pos >= this.limit) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    public void putByte(byte p4)
    {
        int v0_0 = this.buf;
        int v1_0 = this.pos;
        this.pos = (v1_0 + 1);
        v0_0[v1_0] = p4;
        if (this.full < this.pos) {
            this.full = this.pos;
        }
        return;
    }

    public void repeat(int p7, int p8)
    {
        if ((p7 >= 0) && (p7 < this.full)) {
            int v2 = Math.min((this.limit - this.pos), p8);
            this.pendingLen = (p8 - v2);
            this.pendingDist = p7;
            int v0 = ((this.pos - p7) - 1);
            if (p7 >= this.pos) {
                v0 += this.buf.length;
            }
            do {
                int v3_9 = this.buf;
                int v4_1 = this.pos;
                this.pos = (v4_1 + 1);
                int v1 = (v0 + 1);
                v3_9[v4_1] = this.buf[v0];
                if (v1 != this.buf.length) {
                    v0 = v1;
                } else {
                    v0 = 0;
                }
                v2--;
            } while(v2 > 0);
            if (this.full < this.pos) {
                this.full = this.pos;
            }
            return;
        } else {
            throw new org.tukaani.xz.CorruptedInputException();
        }
    }

    public void repeatPending()
    {
        if (this.pendingLen > 0) {
            this.repeat(this.pendingDist, this.pendingLen);
        }
        return;
    }

    public void reset()
    {
        this.start = 0;
        this.pos = 0;
        this.full = 0;
        this.limit = 0;
        this.buf[(this.buf.length - 1)] = 0;
        return;
    }

    public void setLimit(int p3)
    {
        if ((this.buf.length - this.pos) > p3) {
            this.limit = (this.pos + p3);
        } else {
            this.limit = this.buf.length;
        }
        return;
    }
}
