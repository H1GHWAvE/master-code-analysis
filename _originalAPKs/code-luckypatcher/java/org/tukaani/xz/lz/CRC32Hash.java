package org.tukaani.xz.lz;
 class CRC32Hash {
    private static final int CRC32_POLY = 3988292384;
    static final int[] crcTable;

    static CRC32Hash()
    {
        int v3_0 = new int[256];
        org.tukaani.xz.lz.CRC32Hash.crcTable = v3_0;
        int v0 = 0;
        while (v0 < 256) {
            int v2 = v0;
            int v1 = 0;
            while (v1 < 8) {
                if ((v2 & 1) == 0) {
                    v2 >>= 1;
                } else {
                    v2 = ((v2 >> 1) ^ -306674912);
                }
                v1++;
            }
            org.tukaani.xz.lz.CRC32Hash.crcTable[v0] = v2;
            v0++;
        }
        return;
    }

    CRC32Hash()
    {
        return;
    }
}
