package org.tukaani.xz.lz;
final class Hash234 extends org.tukaani.xz.lz.CRC32Hash {
    private static final int HASH_2_MASK = 1023;
    private static final int HASH_2_SIZE = 1024;
    private static final int HASH_3_MASK = 65535;
    private static final int HASH_3_SIZE = 65536;
    private final int[] hash2Table;
    private int hash2Value;
    private final int[] hash3Table;
    private int hash3Value;
    private final int hash4Mask;
    private final int[] hash4Table;
    private int hash4Value;

    Hash234(int p3)
    {
        int v0_1 = new int[1024];
        this.hash2Table = v0_1;
        int v0_3 = new int[65536];
        this.hash3Table = v0_3;
        this.hash2Value = 0;
        this.hash3Value = 0;
        this.hash4Value = 0;
        int v0_5 = new int[org.tukaani.xz.lz.Hash234.getHash4Size(p3)];
        this.hash4Table = v0_5;
        this.hash4Mask = (this.hash4Table.length - 1);
        return;
    }

    static int getHash4Size(int p2)
    {
        int v0_0 = (p2 - 1);
        int v0_1 = (v0_0 | (v0_0 >> 1));
        int v0_2 = (v0_1 | (v0_1 >> 2));
        int v0_3 = (v0_2 | (v0_2 >> 4));
        int v0_6 = (((v0_3 | (v0_3 >> 8)) >> 1) | 65535);
        if (v0_6 > 16777216) {
            v0_6 >>= 1;
        }
        return (v0_6 + 1);
    }

    static int getMemoryUsage(int p2)
    {
        return (((66560 + org.tukaani.xz.lz.Hash234.getHash4Size(p2)) / 256) + 4);
    }

    void calcHashes(byte[] p4, int p5)
    {
        int v0_0 = (org.tukaani.xz.lz.Hash234.crcTable[(p4[p5] & 255)] ^ (p4[(p5 + 1)] & 255));
        this.hash2Value = (v0_0 & 1023);
        int v0_1 = (v0_0 ^ ((p4[(p5 + 2)] & 255) << 8));
        this.hash3Value = (65535 & v0_1);
        this.hash4Value = (this.hash4Mask & (v0_1 ^ (org.tukaani.xz.lz.Hash234.crcTable[(p4[(p5 + 3)] & 255)] << 5)));
        return;
    }

    int getHash2Pos()
    {
        return this.hash2Table[this.hash2Value];
    }

    int getHash3Pos()
    {
        return this.hash3Table[this.hash3Value];
    }

    int getHash4Pos()
    {
        return this.hash4Table[this.hash4Value];
    }

    void normalize(int p2)
    {
        org.tukaani.xz.lz.LZEncoder.normalize(this.hash2Table, p2);
        org.tukaani.xz.lz.LZEncoder.normalize(this.hash3Table, p2);
        org.tukaani.xz.lz.LZEncoder.normalize(this.hash4Table, p2);
        return;
    }

    void updateTables(int p3)
    {
        this.hash2Table[this.hash2Value] = p3;
        this.hash3Table[this.hash3Value] = p3;
        this.hash4Table[this.hash4Value] = p3;
        return;
    }
}
