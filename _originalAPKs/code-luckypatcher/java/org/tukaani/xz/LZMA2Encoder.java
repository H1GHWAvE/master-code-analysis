package org.tukaani.xz;
 class LZMA2Encoder extends org.tukaani.xz.LZMA2Coder implements org.tukaani.xz.FilterEncoder {
    private final org.tukaani.xz.LZMA2Options options;
    private final byte[] props;

    LZMA2Encoder(org.tukaani.xz.LZMA2Options p5)
    {
        org.tukaani.xz.LZMA2Options v1_1 = new byte[1];
        this.props = v1_1;
        if (p5.getPresetDict() == null) {
            if (p5.getMode() != 0) {
                this.props[0] = ((byte) (org.tukaani.xz.lzma.LZMAEncoder.getDistSlot((Math.max(p5.getDictSize(), 4096) - 1)) - 23));
            } else {
                this.props[0] = 0;
            }
            this.options = ((org.tukaani.xz.LZMA2Options) p5.clone());
            return;
        } else {
            throw new IllegalArgumentException("XZ doesn\'t support a preset dictionary for now");
        }
    }

    public long getFilterID()
    {
        return 33;
    }

    public byte[] getFilterProps()
    {
        return this.props;
    }

    public org.tukaani.xz.FinishableOutputStream getOutputStream(org.tukaani.xz.FinishableOutputStream p2)
    {
        return this.options.getOutputStream(p2);
    }

    public boolean supportsFlushing()
    {
        return 1;
    }
}
