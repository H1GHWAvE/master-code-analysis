package org.tukaani.xz;
 class BCJDecoder extends org.tukaani.xz.BCJCoder implements org.tukaani.xz.FilterDecoder {
    static final synthetic boolean $assertionsDisabled;
    private final long filterID;
    private final int startOffset;

    static BCJDecoder()
    {
        int v0_2;
        if (org.tukaani.xz.BCJDecoder.desiredAssertionStatus()) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        org.tukaani.xz.BCJDecoder.$assertionsDisabled = v0_2;
        return;
    }

    BCJDecoder(long p6, byte[] p8)
    {
        if ((org.tukaani.xz.BCJDecoder.$assertionsDisabled) || (org.tukaani.xz.BCJDecoder.isBCJFilterID(p6))) {
            this.filterID = p6;
            if (p8.length != 0) {
                if (p8.length != 4) {
                    throw new org.tukaani.xz.UnsupportedOptionsException("Unsupported BCJ filter properties");
                } else {
                    int v1 = 0;
                    int v0 = 0;
                    while (v0 < 4) {
                        v1 |= ((p8[v0] & 255) << (v0 * 8));
                        v0++;
                    }
                    this.startOffset = v1;
                }
            } else {
                this.startOffset = 0;
            }
            return;
        } else {
            throw new AssertionError();
        }
    }

    public java.io.InputStream getInputStream(java.io.InputStream p7)
    {
        org.tukaani.xz.simple.SPARC v0_0 = 0;
        if (this.filterID != 4) {
            if (this.filterID != 5) {
                if (this.filterID != 6) {
                    if (this.filterID != 7) {
                        if (this.filterID != 8) {
                            if (this.filterID != 9) {
                                if (!org.tukaani.xz.BCJDecoder.$assertionsDisabled) {
                                    throw new AssertionError();
                                }
                            } else {
                                v0_0 = new org.tukaani.xz.simple.SPARC(0, this.startOffset);
                            }
                        } else {
                            v0_0 = new org.tukaani.xz.simple.ARMThumb(0, this.startOffset);
                        }
                    } else {
                        v0_0 = new org.tukaani.xz.simple.ARM(0, this.startOffset);
                    }
                } else {
                    v0_0 = new org.tukaani.xz.simple.IA64(0, this.startOffset);
                }
            } else {
                v0_0 = new org.tukaani.xz.simple.PowerPC(0, this.startOffset);
            }
        } else {
            v0_0 = new org.tukaani.xz.simple.X86(0, this.startOffset);
        }
        return new org.tukaani.xz.SimpleInputStream(p7, v0_0);
    }

    public int getMemoryUsage()
    {
        return org.tukaani.xz.SimpleInputStream.getMemoryUsage();
    }
}
