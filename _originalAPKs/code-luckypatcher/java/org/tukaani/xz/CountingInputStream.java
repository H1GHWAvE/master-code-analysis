package org.tukaani.xz;
 class CountingInputStream extends java.io.FilterInputStream {
    private long size;

    public CountingInputStream(java.io.InputStream p3)
    {
        this(p3);
        this.size = 0;
        return;
    }

    public long getSize()
    {
        return this.size;
    }

    public int read()
    {
        int v0 = this.in.read();
        if ((v0 != -1) && (this.size >= 0)) {
            this.size = (this.size + 1);
        }
        return v0;
    }

    public int read(byte[] p6, int p7, int p8)
    {
        int v0 = this.in.read(p6, p7, p8);
        if ((v0 > 0) && (this.size >= 0)) {
            this.size = (this.size + ((long) v0));
        }
        return v0;
    }
}
