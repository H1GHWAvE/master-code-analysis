package org.tukaani.xz.lzma;
final class LZMAEncoderFast extends org.tukaani.xz.lzma.LZMAEncoder {
    private static int EXTRA_SIZE_AFTER;
    private static int EXTRA_SIZE_BEFORE;
    private org.tukaani.xz.lz.Matches matches;

    static LZMAEncoderFast()
    {
        org.tukaani.xz.lzma.LZMAEncoderFast.EXTRA_SIZE_BEFORE = 1;
        org.tukaani.xz.lzma.LZMAEncoderFast.EXTRA_SIZE_AFTER = 272;
        return;
    }

    LZMAEncoderFast(org.tukaani.xz.rangecoder.RangeEncoder p9, int p10, int p11, int p12, int p13, int p14, int p15, int p16, int p17)
    {
        this(p9, org.tukaani.xz.lz.LZEncoder.getInstance(p13, Math.max(p14, org.tukaani.xz.lzma.LZMAEncoderFast.EXTRA_SIZE_BEFORE), org.tukaani.xz.lzma.LZMAEncoderFast.EXTRA_SIZE_AFTER, p15, 273, p16, p17), p10, p11, p12, p13, p15);
        this.matches = 0;
        return;
    }

    private boolean changePair(int p2, int p3)
    {
        int v0_1;
        if (p2 >= (p3 >> 7)) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    static int getMemoryUsage(int p3, int p4, int p5)
    {
        return org.tukaani.xz.lz.LZEncoder.getMemoryUsage(p3, Math.max(p4, org.tukaani.xz.lzma.LZMAEncoderFast.EXTRA_SIZE_BEFORE), org.tukaani.xz.lzma.LZMAEncoderFast.EXTRA_SIZE_AFTER, 273, p5);
    }

    int getNextSymbol()
    {
        if (this.readAhead == -1) {
            this.matches = this.getMatches();
        }
        int v6;
        this.back = -1;
        int v0 = Math.min(this.lz.getAvail(), 273);
        if (v0 >= 2) {
            int v2 = 0;
            int v1 = 0;
            int v9_0 = 0;
            while (v9_0 < 4) {
                int v3 = this.lz.getMatchLen(this.reps[v9_0], v0);
                if (v3 >= 2) {
                    if (v3 < this.niceLen) {
                        if (v3 > v2) {
                            v1 = v9_0;
                            v2 = v3;
                        }
                    } else {
                        this.back = v9_0;
                        this.skip((v3 - 1));
                        v6 = v3;
                        return v6;
                    }
                }
                v9_0++;
            }
            v6 = 0;
            int v5 = 0;
            if (this.matches.count > 0) {
                v6 = this.matches.len[(this.matches.count - 1)];
                v5 = this.matches.dist[(this.matches.count - 1)];
                if (v6 >= this.niceLen) {
                    this.back = (v5 + 4);
                    this.skip((v6 - 1));
                    return v6;
                }
                while ((this.matches.count > 1) && ((v6 == (this.matches.len[(this.matches.count - 2)] + 1)) && (this.changePair(this.matches.dist[(this.matches.count - 2)], v5)))) {
                    boolean v11_21 = this.matches;
                    v11_21.count = (v11_21.count - 1);
                    v6 = this.matches.len[(this.matches.count - 1)];
                    v5 = this.matches.dist[(this.matches.count - 1)];
                }
                if ((v6 == 2) && (v5 >= 128)) {
                    v6 = 1;
                }
            }
            if ((v2 < 2) || ((((v2 + 1) < v6) && (((v2 + 2) < v6) || (v5 < 512))) && (((v2 + 3) < v6) || (v5 < 32768)))) {
                if ((v6 >= 2) && (v0 > 2)) {
                    this.matches = this.getMatches();
                    if (this.matches.count > 0) {
                        int v8 = this.matches.len[(this.matches.count - 1)];
                        int v7 = this.matches.dist[(this.matches.count - 1)];
                        if (((v8 >= v6) && (v7 < v5)) || (((v8 == (v6 + 1)) && (!this.changePair(v5, v7))) || ((v8 > (v6 + 1)) || (((v8 + 1) >= v6) && ((v6 >= 3) && (this.changePair(v7, v5))))))) {
                            v6 = 1;
                            return v6;
                        }
                    }
                    int v4 = Math.max((v6 - 1), 2);
                    int v9_1 = 0;
                    while (v9_1 < 4) {
                        if (this.lz.getMatchLen(this.reps[v9_1], v4) != v4) {
                            v9_1++;
                        } else {
                            v6 = 1;
                        }
                    }
                    this.back = (v5 + 4);
                    this.skip((v6 - 2));
                } else {
                    v6 = 1;
                }
            } else {
                this.back = v1;
                this.skip((v2 - 1));
                v6 = v2;
            }
        } else {
            v6 = 1;
        }
        return v6;
    }
}
