package org.tukaani.xz.lzma;
 class LZMAEncoder$LengthEncoder extends org.tukaani.xz.lzma.LZMACoder$LengthCoder {
    private static final int PRICE_UPDATE_INTERVAL = 32;
    private final int[] counters;
    private final int[][] prices;
    final synthetic org.tukaani.xz.lzma.LZMAEncoder this$0;

    LZMAEncoder$LengthEncoder(org.tukaani.xz.lzma.LZMAEncoder p5, int p6, int p7)
    {
        this.this$0 = p5;
        this(p5);
        int v1 = (1 << p6);
        int[][] v2_1 = new int[v1];
        this.counters = v2_1;
        this.prices = ((int[][]) reflect.Array.newInstance(Integer.TYPE, new int[] {v1, Math.max(((p7 - 2) + 1), 16)})));
        return;
    }

    private void updatePrices(int p9)
    {
        int v0_0 = org.tukaani.xz.rangecoder.RangeEncoder.getBitPrice(this.choice[0], 0);
        int v2 = 0;
        while (v2 < 8) {
            this.prices[p9][v2] = (org.tukaani.xz.rangecoder.RangeEncoder.getBitTreePrice(this.low[p9], v2) + v0_0);
            v2++;
        }
        int v0_1 = org.tukaani.xz.rangecoder.RangeEncoder.getBitPrice(this.choice[0], 1);
        int v1_0 = org.tukaani.xz.rangecoder.RangeEncoder.getBitPrice(this.choice[1], 0);
        while (v2 < 16) {
            this.prices[p9][v2] = ((v0_1 + v1_0) + org.tukaani.xz.rangecoder.RangeEncoder.getBitTreePrice(this.mid[p9], (v2 - 8)));
            v2++;
        }
        int v1_1 = org.tukaani.xz.rangecoder.RangeEncoder.getBitPrice(this.choice[1], 1);
        while (v2 < this.prices[p9].length) {
            this.prices[p9][v2] = ((v0_1 + v1_1) + org.tukaani.xz.rangecoder.RangeEncoder.getBitTreePrice(this.high, ((v2 - 8) - 8)));
            v2++;
        }
        return;
    }

    void encode(int p6, int p7)
    {
        int v6_1 = (p6 - 2);
        if (v6_1 >= 8) {
            org.tukaani.xz.lzma.LZMAEncoder.access$100(this.this$0).encodeBit(this.choice, 0, 1);
            int v6_2 = (v6_1 - 8);
            if (v6_2 >= 8) {
                org.tukaani.xz.lzma.LZMAEncoder.access$100(this.this$0).encodeBit(this.choice, 1, 1);
                org.tukaani.xz.lzma.LZMAEncoder.access$100(this.this$0).encodeBitTree(this.high, (v6_2 - 8));
            } else {
                org.tukaani.xz.lzma.LZMAEncoder.access$100(this.this$0).encodeBit(this.choice, 1, 0);
                org.tukaani.xz.lzma.LZMAEncoder.access$100(this.this$0).encodeBitTree(this.mid[p7], v6_2);
            }
        } else {
            org.tukaani.xz.lzma.LZMAEncoder.access$100(this.this$0).encodeBit(this.choice, 0, 0);
            org.tukaani.xz.lzma.LZMAEncoder.access$100(this.this$0).encodeBitTree(this.low[p7], v6_1);
        }
        org.tukaani.xz.rangecoder.RangeEncoder v0_14 = this.counters;
        v0_14[p7] = (v0_14[p7] - 1);
        return;
    }

    int getPrice(int p3, int p4)
    {
        return this.prices[p4][(p3 - 2)];
    }

    void reset()
    {
        super.reset();
        int v0 = 0;
        while (v0 < this.counters.length) {
            this.counters[v0] = 0;
            v0++;
        }
        return;
    }

    void updatePrices()
    {
        int v0 = 0;
        while (v0 < this.counters.length) {
            if (this.counters[v0] <= 0) {
                this.counters[v0] = 32;
                this.updatePrices(v0);
            }
            v0++;
        }
        return;
    }
}
