package org.tukaani.xz.lzma;
 class LZMAEncoder$LiteralEncoder$LiteralSubencoder extends org.tukaani.xz.lzma.LZMACoder$LiteralCoder$LiteralSubcoder {
    final synthetic org.tukaani.xz.lzma.LZMAEncoder$LiteralEncoder this$1;

    private LZMAEncoder$LiteralEncoder$LiteralSubencoder(org.tukaani.xz.lzma.LZMAEncoder$LiteralEncoder p1)
    {
        this.this$1 = p1;
        this(p1);
        return;
    }

    synthetic LZMAEncoder$LiteralEncoder$LiteralSubencoder(org.tukaani.xz.lzma.LZMAEncoder$LiteralEncoder p1, org.tukaani.xz.lzma.LZMAEncoder$1 p2)
    {
        this(p1);
        return;
    }

    void encode()
    {
        int v5 = (this.this$1.this$0.lz.getByte(this.this$1.this$0.readAhead) | 256);
        if (!this.this$1.this$0.state.isLiteral()) {
            int v2 = this.this$1.this$0.lz.getByte(((this.this$1.this$0.reps[0] + 1) + this.this$1.this$0.readAhead));
            int v3 = 256;
            do {
                v2 <<= 1;
                org.tukaani.xz.lzma.LZMAEncoder.access$100(this.this$1.this$0).encodeBit(this.probs, ((v3 + (v2 & v3)) + (v5 >> 8)), ((v5 >> 7) & 1));
                v5 <<= 1;
                v3 &= ((v2 ^ v5) ^ -1);
            } while(v5 < 65536);
        } else {
            do {
                org.tukaani.xz.lzma.LZMAEncoder.access$100(this.this$1.this$0).encodeBit(this.probs, (v5 >> 8), ((v5 >> 7) & 1));
                v5 <<= 1;
            } while(v5 < 65536);
        }
        this.this$1.this$0.state.updateLiteral();
        return;
    }

    int getMatchedPrice(int p8, int p9)
    {
        int v3 = 0;
        int v2 = 256;
        int v8_1 = (p8 | 256);
        do {
            p9 <<= 1;
            v3 += org.tukaani.xz.rangecoder.RangeEncoder.getBitPrice(this.probs[((v2 + (p9 & v2)) + (v8_1 >> 8))], ((v8_1 >> 7) & 1));
            v8_1 <<= 1;
            v2 &= ((p9 ^ v8_1) ^ -1);
        } while(v8_1 < 65536);
        return v3;
    }

    int getNormalPrice(int p5)
    {
        int v1 = 0;
        int v5_1 = (p5 | 256);
        do {
            v1 += org.tukaani.xz.rangecoder.RangeEncoder.getBitPrice(this.probs[(v5_1 >> 8)], ((v5_1 >> 7) & 1));
            v5_1 <<= 1;
        } while(v5_1 < 65536);
        return v1;
    }
}
