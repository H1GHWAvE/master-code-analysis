package org.tukaani.xz.lzma;
abstract class LZMACoder {
    static final int ALIGN_BITS = 4;
    static final int ALIGN_MASK = 15;
    static final int ALIGN_SIZE = 16;
    static final int DIST_MODEL_END = 14;
    static final int DIST_MODEL_START = 4;
    static final int DIST_SLOTS = 64;
    static final int DIST_STATES = 4;
    static final int FULL_DISTANCES = 128;
    static final int MATCH_LEN_MAX = 273;
    static final int MATCH_LEN_MIN = 2;
    static final int POS_STATES_MAX = 16;
    static final int REPS = 4;
    final short[] distAlign;
    final short[][] distSlots;
    final short[][] distSpecial;
    final short[][] isMatch;
    final short[] isRep;
    final short[] isRep0;
    final short[][] isRep0Long;
    final short[] isRep1;
    final short[] isRep2;
    final int posMask;
    final int[] reps;
    final org.tukaani.xz.lzma.State state;

    LZMACoder(int p8)
    {
        int v0_0 = new int[4];
        this.reps = v0_0;
        this.state = new org.tukaani.xz.lzma.State();
        this.isMatch = ((short[][]) reflect.Array.newInstance(Short.TYPE, new int[] {12, 16})));
        int v0_6 = new short[12];
        this.isRep = v0_6;
        int v0_7 = new short[12];
        this.isRep0 = v0_7;
        int v0_8 = new short[12];
        this.isRep1 = v0_8;
        int v0_9 = new short[12];
        this.isRep2 = v0_9;
        this.isRep0Long = ((short[][]) reflect.Array.newInstance(Short.TYPE, new int[] {12, 16})));
        this.distSlots = ((short[][]) reflect.Array.newInstance(Short.TYPE, new int[] {4, 64})));
        int v0_18 = new short[][10];
        short[] v2_1 = new short[2];
        v0_18[0] = v2_1;
        short[] v2_2 = new short[2];
        v0_18[1] = v2_2;
        int v1_5 = new short[4];
        v0_18[2] = v1_5;
        short[] v2_3 = new short[4];
        v0_18[3] = v2_3;
        int v1_7 = new short[8];
        v0_18[4] = v1_7;
        short[] v2_4 = new short[8];
        v0_18[5] = v2_4;
        short[] v2_5 = new short[16];
        v0_18[6] = v2_5;
        short[] v2_6 = new short[16];
        v0_18[7] = v2_6;
        int v1_12 = new short[32];
        v0_18[8] = v1_12;
        short[] v2_8 = new short[32];
        v0_18[9] = v2_8;
        this.distSpecial = v0_18;
        int v0_19 = new short[16];
        this.distAlign = v0_19;
        this.posMask = ((1 << p8) - 1);
        return;
    }

    static final int getDistState(int p1)
    {
        int v0_1;
        if (p1 >= 6) {
            v0_1 = 3;
        } else {
            v0_1 = (p1 - 2);
        }
        return v0_1;
    }

    void reset()
    {
        this.reps[0] = 0;
        this.reps[1] = 0;
        this.reps[2] = 0;
        this.reps[3] = 0;
        this.state.reset();
        int v0_0 = 0;
        while (v0_0 < this.isMatch.length) {
            org.tukaani.xz.rangecoder.RangeCoder.initProbs(this.isMatch[v0_0]);
            v0_0++;
        }
        org.tukaani.xz.rangecoder.RangeCoder.initProbs(this.isRep);
        org.tukaani.xz.rangecoder.RangeCoder.initProbs(this.isRep0);
        org.tukaani.xz.rangecoder.RangeCoder.initProbs(this.isRep1);
        org.tukaani.xz.rangecoder.RangeCoder.initProbs(this.isRep2);
        int v0_1 = 0;
        while (v0_1 < this.isRep0Long.length) {
            org.tukaani.xz.rangecoder.RangeCoder.initProbs(this.isRep0Long[v0_1]);
            v0_1++;
        }
        int v0_2 = 0;
        while (v0_2 < this.distSlots.length) {
            org.tukaani.xz.rangecoder.RangeCoder.initProbs(this.distSlots[v0_2]);
            v0_2++;
        }
        int v0_3 = 0;
        while (v0_3 < this.distSpecial.length) {
            org.tukaani.xz.rangecoder.RangeCoder.initProbs(this.distSpecial[v0_3]);
            v0_3++;
        }
        org.tukaani.xz.rangecoder.RangeCoder.initProbs(this.distAlign);
        return;
    }
}
