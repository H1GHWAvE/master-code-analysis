package org.tukaani.xz.lzma;
 class LZMADecoder$LengthDecoder extends org.tukaani.xz.lzma.LZMACoder$LengthCoder {
    final synthetic org.tukaani.xz.lzma.LZMADecoder this$0;

    private LZMADecoder$LengthDecoder(org.tukaani.xz.lzma.LZMADecoder p1)
    {
        this.this$0 = p1;
        this(p1);
        return;
    }

    synthetic LZMADecoder$LengthDecoder(org.tukaani.xz.lzma.LZMADecoder p1, org.tukaani.xz.lzma.LZMADecoder$1 p2)
    {
        this(p1);
        return;
    }

    int decode(int p4)
    {
        int v0_11;
        if (org.tukaani.xz.lzma.LZMADecoder.access$300(this.this$0).decodeBit(this.choice, 0) != 0) {
            if (org.tukaani.xz.lzma.LZMADecoder.access$300(this.this$0).decodeBit(this.choice, 1) != 0) {
                v0_11 = (((org.tukaani.xz.lzma.LZMADecoder.access$300(this.this$0).decodeBitTree(this.high) + 2) + 8) + 8);
            } else {
                v0_11 = ((org.tukaani.xz.lzma.LZMADecoder.access$300(this.this$0).decodeBitTree(this.mid[p4]) + 2) + 8);
            }
        } else {
            v0_11 = (org.tukaani.xz.lzma.LZMADecoder.access$300(this.this$0).decodeBitTree(this.low[p4]) + 2);
        }
        return v0_11;
    }
}
