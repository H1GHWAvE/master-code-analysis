package org.tukaani.xz.lzma;
abstract class LZMACoder$LengthCoder {
    static final int HIGH_SYMBOLS = 256;
    static final int LOW_SYMBOLS = 8;
    static final int MID_SYMBOLS = 8;
    final short[] choice;
    final short[] high;
    final short[][] low;
    final short[][] mid;
    final synthetic org.tukaani.xz.lzma.LZMACoder this$0;

    LZMACoder$LengthCoder(org.tukaani.xz.lzma.LZMACoder p5)
    {
        this.this$0 = p5;
        short[] v0_1 = new short[2];
        this.choice = v0_1;
        this.low = ((short[][]) reflect.Array.newInstance(Short.TYPE, new int[] {16, 8})));
        this.mid = ((short[][]) reflect.Array.newInstance(Short.TYPE, new int[] {16, 8})));
        short[] v0_9 = new short[256];
        this.high = v0_9;
        return;
    }

    void reset()
    {
        org.tukaani.xz.rangecoder.RangeCoder.initProbs(this.choice);
        int v0_0 = 0;
        while (v0_0 < this.low.length) {
            org.tukaani.xz.rangecoder.RangeCoder.initProbs(this.low[v0_0]);
            v0_0++;
        }
        int v0_1 = 0;
        while (v0_1 < this.low.length) {
            org.tukaani.xz.rangecoder.RangeCoder.initProbs(this.mid[v0_1]);
            v0_1++;
        }
        org.tukaani.xz.rangecoder.RangeCoder.initProbs(this.high);
        return;
    }
}
