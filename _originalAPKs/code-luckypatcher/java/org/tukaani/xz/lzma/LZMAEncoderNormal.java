package org.tukaani.xz.lzma;
final class LZMAEncoderNormal extends org.tukaani.xz.lzma.LZMAEncoder {
    static final synthetic boolean $assertionsDisabled = False;
    private static int EXTRA_SIZE_AFTER = 0;
    private static int EXTRA_SIZE_BEFORE = 0;
    private static final int OPTS = 4096;
    private org.tukaani.xz.lz.Matches matches;
    private final org.tukaani.xz.lzma.State nextState;
    private int optCur;
    private int optEnd;
    private final org.tukaani.xz.lzma.Optimum[] opts;
    private final int[] repLens;

    static LZMAEncoderNormal()
    {
        int v0_2;
        if (org.tukaani.xz.lzma.LZMAEncoderNormal.desiredAssertionStatus()) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        org.tukaani.xz.lzma.LZMAEncoderNormal.$assertionsDisabled = v0_2;
        org.tukaani.xz.lzma.LZMAEncoderNormal.EXTRA_SIZE_BEFORE = 4096;
        org.tukaani.xz.lzma.LZMAEncoderNormal.EXTRA_SIZE_AFTER = 4096;
        return;
    }

    LZMAEncoderNormal(org.tukaani.xz.rangecoder.RangeEncoder p10, int p11, int p12, int p13, int p14, int p15, int p16, int p17, int p18)
    {
        this(p10, org.tukaani.xz.lz.LZEncoder.getInstance(p14, Math.max(p15, org.tukaani.xz.lzma.LZMAEncoderNormal.EXTRA_SIZE_BEFORE), org.tukaani.xz.lzma.LZMAEncoderNormal.EXTRA_SIZE_AFTER, p16, 273, p17, p18), p11, p12, p13, p14, p16);
        org.tukaani.xz.lzma.Optimum[] v0_4 = new org.tukaani.xz.lzma.Optimum[4096];
        this.opts = v0_4;
        this.optCur = 0;
        this.optEnd = 0;
        org.tukaani.xz.lzma.Optimum[] v0_8 = new int[4];
        this.repLens = v0_8;
        this.nextState = new org.tukaani.xz.lzma.State();
        int v8 = 0;
        while (v8 < 4096) {
            this.opts[v8] = new org.tukaani.xz.lzma.Optimum();
            v8++;
        }
        return;
    }

    private void calc1BytePrices(int p19, int p20, int p21, int p22)
    {
        int v13 = 0;
        int v4 = this.lz.getByte(0);
        int v5 = this.lz.getByte((this.opts[this.optCur].reps[0] + 1));
        int v12 = (this.opts[this.optCur].price + this.literalEncoder.getPrice(v4, v5, this.lz.getByte(1), p19, this.opts[this.optCur].state));
        if (v12 < this.opts[(this.optCur + 1)].price) {
            this.opts[(this.optCur + 1)].set1(v12, this.optCur, -1);
            v13 = 1;
        }
        if ((v5 == v4) && ((this.opts[(this.optCur + 1)].optPrev == this.optCur) || (this.opts[(this.optCur + 1)].backPrev != 0))) {
            int v16 = this.getShortRepPrice(p22, this.opts[this.optCur].state, p20);
            if (v16 <= this.opts[(this.optCur + 1)].price) {
                this.opts[(this.optCur + 1)].set1(v16, this.optCur, 0);
                v13 = 1;
            }
        }
        if ((v13 == 0) && ((v5 != v4) && (p21 > 2))) {
            int v10 = this.lz.getMatchLen(1, this.opts[this.optCur].reps[0], Math.min(this.niceLen, (p21 - 1)));
            if (v10 >= 2) {
                this.nextState.set(this.opts[this.optCur].state);
                this.nextState.updateLiteral();
                int v15 = (v12 + this.getLongRepAndLenPrice(0, v10, this.nextState, ((p19 + 1) & this.posMask)));
                int v9 = ((this.optCur + 1) + v10);
                while (this.optEnd < v9) {
                    org.tukaani.xz.lzma.Optimum v3_42 = this.opts;
                    int v6_34 = (this.optEnd + 1);
                    this.optEnd = v6_34;
                    v3_42[v6_34].reset();
                }
                if (v15 < this.opts[v9].price) {
                    this.opts[v9].set2(v15, this.optCur, 0);
                }
            }
        }
        return;
    }

    private int calcLongRepPrices(int p22, int p23, int p24, int p25)
    {
        int v20 = 2;
        int v16 = Math.min(p24, this.niceLen);
        int v10 = 0;
        while (v10 < 4) {
            int v11 = this.lz.getMatchLen(this.opts[this.optCur].reps[v10], v16);
            if (v11 >= 2) {
                while (this.optEnd < (this.optCur + v11)) {
                    org.tukaani.xz.lzma.Optimum[] v3_43 = this.opts;
                    org.tukaani.xz.lzma.Optimum v7_28 = (this.optEnd + 1);
                    this.optEnd = v7_28;
                    v3_43[v7_28].reset();
                }
                int v17 = this.getLongRepPrice(p25, v10, this.opts[this.optCur].state, p23);
                int v13_0 = v11;
                while (v13_0 >= 2) {
                    int v8_3 = (v17 + this.repLenEncoder.getPrice(v13_0, p23));
                    if (v8_3 < this.opts[(this.optCur + v13_0)].price) {
                        this.opts[(this.optCur + v13_0)].set1(v8_3, this.optCur, v10);
                    }
                    v13_0--;
                }
                if (v10 == 0) {
                    v20 = (v11 + 1);
                }
                int v14 = this.lz.getMatchLen((v11 + 1), this.opts[this.optCur].reps[v10], Math.min(this.niceLen, ((p24 - v11) - 1)));
                if (v14 >= 2) {
                    int v19 = (v17 + this.repLenEncoder.getPrice(v11, p23));
                    this.nextState.set(this.opts[this.optCur].state);
                    this.nextState.updateLongRep();
                    int v8_1 = (v19 + this.literalEncoder.getPrice(this.lz.getByte(v11, 0), this.lz.getByte(0), this.lz.getByte(v11, 1), (p22 + v11), this.nextState));
                    this.nextState.updateLiteral();
                    int v8_2 = (v8_1 + this.getLongRepAndLenPrice(0, v14, this.nextState, (((p22 + v11) + 1) & this.posMask)));
                    int v13_1 = (((this.optCur + v11) + 1) + v14);
                    while (this.optEnd < v13_1) {
                        org.tukaani.xz.lzma.Optimum[] v3_34 = this.opts;
                        org.tukaani.xz.lzma.Optimum v7_21 = (this.optEnd + 1);
                        this.optEnd = v7_21;
                        v3_34[v7_21].reset();
                    }
                    if (v8_2 < this.opts[v13_1].price) {
                        this.opts[v13_1].set3(v8_2, this.optCur, v10, v11, 0);
                    }
                }
            }
            v10++;
        }
        return v20;
    }

    private void calcNormalMatchPrices(int p22, int p23, int p24, int p25, int p26)
    {
        if (this.matches.len[(this.matches.count - 1)] > p24) {
            this.matches.count = 0;
            while (this.matches.len[this.matches.count] < p24) {
                org.tukaani.xz.lzma.Optimum[] v3_58 = this.matches;
                v3_58.count = (v3_58.count + 1);
            }
            org.tukaani.xz.lzma.Optimum[] v3_8 = this.matches.len;
            org.tukaani.xz.lzma.Optimum v7_6 = this.matches;
            int v9_0 = v7_6.count;
            v7_6.count = (v9_0 + 1);
            v3_8[v9_0] = p24;
        }
        if (this.matches.len[(this.matches.count - 1)] >= p26) {
            while (this.optEnd < (this.optCur + this.matches.len[(this.matches.count - 1)])) {
                org.tukaani.xz.lzma.Optimum[] v3_56 = this.opts;
                org.tukaani.xz.lzma.Optimum v7_34 = (this.optEnd + 1);
                this.optEnd = v7_34;
                v3_56[v7_34].reset();
            }
            int v20 = this.getNormalMatchPrice(p25, this.opts[this.optCur].state);
            int v17 = 0;
            while (p26 > this.matches.len[v17]) {
                v17++;
            }
            int v11 = p26;
            while(true) {
                int v13 = this.matches.dist[v17];
                int v18 = this.getMatchAndLenPrice(v20, v13, v11, p23);
                if (v18 < this.opts[(this.optCur + v11)].price) {
                    this.opts[(this.optCur + v11)].set1(v18, this.optCur, (v13 + 4));
                }
                if (v11 == this.matches.len[v17]) {
                    int v15 = this.lz.getMatchLen((v11 + 1), v13, Math.min(this.niceLen, ((p24 - v11) - 1)));
                    if (v15 >= 2) {
                        this.nextState.set(this.opts[this.optCur].state);
                        this.nextState.updateMatch();
                        int v8_1 = (v18 + this.literalEncoder.getPrice(this.lz.getByte(v11, 0), this.lz.getByte(0), this.lz.getByte(v11, 1), (p22 + v11), this.nextState));
                        this.nextState.updateLiteral();
                        int v8_2 = (v8_1 + this.getLongRepAndLenPrice(0, v15, this.nextState, (((p22 + v11) + 1) & this.posMask)));
                        int v14 = (((this.optCur + v11) + 1) + v15);
                        while (this.optEnd < v14) {
                            org.tukaani.xz.lzma.Optimum[] v3_54 = this.opts;
                            org.tukaani.xz.lzma.Optimum v7_32 = (this.optEnd + 1);
                            this.optEnd = v7_32;
                            v3_54[v7_32].reset();
                        }
                        if (v8_2 < this.opts[v14].price) {
                            this.opts[v14].set3(v8_2, this.optCur, (v13 + 4), v11, 0);
                        }
                    }
                    v17++;
                    if (v17 == this.matches.count) {
                        break;
                    }
                }
                v11++;
            }
        }
        return;
    }

    private int convertOpts()
    {
        this.optEnd = this.optCur;
        int v1 = this.opts[this.optCur].optPrev;
        do {
            org.tukaani.xz.lzma.Optimum v0 = this.opts[this.optCur];
            if (v0.prev1IsLiteral) {
                this.opts[v1].optPrev = this.optCur;
                this.opts[v1].backPrev = -1;
                int v2 = (v1 - 1);
                this.optCur = v1;
                if (!v0.hasPrev2) {
                    v1 = v2;
                } else {
                    this.opts[v2].optPrev = (v2 + 1);
                    this.opts[v2].backPrev = v0.backPrev2;
                    this.optCur = v2;
                    v1 = v0.optPrev2;
                }
            }
            int v3 = this.opts[v1].optPrev;
            this.opts[v1].optPrev = this.optCur;
            this.optCur = v1;
            v1 = v3;
        } while(this.optCur > 0);
        this.optCur = this.opts[0].optPrev;
        this.back = this.opts[this.optCur].backPrev;
        return this.optCur;
    }

    static int getMemoryUsage(int p3, int p4, int p5)
    {
        return (org.tukaani.xz.lz.LZEncoder.getMemoryUsage(p3, Math.max(p4, org.tukaani.xz.lzma.LZMAEncoderNormal.EXTRA_SIZE_BEFORE), org.tukaani.xz.lzma.LZMAEncoderNormal.EXTRA_SIZE_AFTER, 273, p5) + 256);
    }

    private void updateOptStateAndReps()
    {
        int v1 = this.opts[this.optCur].optPrev;
        if ((org.tukaani.xz.lzma.LZMAEncoderNormal.$assertionsDisabled) || (v1 < this.optCur)) {
            if (!this.opts[this.optCur].prev1IsLiteral) {
                this.opts[this.optCur].state.set(this.opts[v1].state);
            } else {
                v1--;
                if (!this.opts[this.optCur].hasPrev2) {
                    this.opts[this.optCur].state.set(this.opts[v1].state);
                } else {
                    this.opts[this.optCur].state.set(this.opts[this.opts[this.optCur].optPrev2].state);
                    if (this.opts[this.optCur].backPrev2 >= 4) {
                        this.opts[this.optCur].state.updateMatch();
                    } else {
                        this.opts[this.optCur].state.updateLongRep();
                    }
                }
                this.opts[this.optCur].state.updateLiteral();
            }
            if (v1 != (this.optCur - 1)) {
                if ((!this.opts[this.optCur].prev1IsLiteral) || (!this.opts[this.optCur].hasPrev2)) {
                    int v0 = this.opts[this.optCur].backPrev;
                    if (v0 >= 4) {
                        this.opts[this.optCur].state.updateMatch();
                    } else {
                        this.opts[this.optCur].state.updateLongRep();
                    }
                } else {
                    v1 = this.opts[this.optCur].optPrev2;
                    v0 = this.opts[this.optCur].backPrev2;
                    this.opts[this.optCur].state.updateLongRep();
                }
                if (v0 >= 4) {
                    this.opts[this.optCur].reps[0] = (v0 - 4);
                    System.arraycopy(this.opts[v1].reps, 0, this.opts[this.optCur].reps, 1, 3);
                } else {
                    this.opts[this.optCur].reps[0] = this.opts[v1].reps[v0];
                    int v2 = 1;
                    while (v2 <= v0) {
                        this.opts[this.optCur].reps[v2] = this.opts[v1].reps[(v2 - 1)];
                        v2++;
                    }
                    while (v2 < 4) {
                        this.opts[this.optCur].reps[v2] = this.opts[v1].reps[v2];
                        v2++;
                    }
                }
            } else {
                if ((org.tukaani.xz.lzma.LZMAEncoderNormal.$assertionsDisabled) || ((this.opts[this.optCur].backPrev == 0) || (this.opts[this.optCur].backPrev == -1))) {
                    if (this.opts[this.optCur].backPrev != 0) {
                        this.opts[this.optCur].state.updateLiteral();
                    } else {
                        this.opts[this.optCur].state.updateShortRep();
                    }
                    System.arraycopy(this.opts[v1].reps, 0, this.opts[this.optCur].reps, 0, 4);
                } else {
                    throw new AssertionError();
                }
            }
            return;
        } else {
            throw new AssertionError();
        }
    }

    int getNextSymbol()
    {
        int v17_1;
        if (this.optCur >= this.optEnd) {
            if ((org.tukaani.xz.lzma.LZMAEncoderNormal.$assertionsDisabled) || (this.optCur == this.optEnd)) {
                this.optCur = 0;
                this.optEnd = 0;
                this.back = -1;
                if (this.readAhead == -1) {
                    this.matches = this.getMatches();
                }
                int v11_0 = Math.min(this.lz.getAvail(), 273);
                if (v11_0 >= 2) {
                    int v25 = 0;
                    int v24_0 = 0;
                    while (v24_0 < 4) {
                        this.repLens[v24_0] = this.lz.getMatchLen(this.reps[v24_0], v11_0);
                        if (this.repLens[v24_0] >= 2) {
                            if (this.repLens[v24_0] > this.repLens[v25]) {
                                v25 = v24_0;
                            }
                        } else {
                            this.repLens[v24_0] = 0;
                        }
                        v24_0++;
                    }
                    if (this.repLens[v25] < this.niceLen) {
                        int v21 = 0;
                        if (this.matches.count > 0) {
                            v21 = this.matches.len[(this.matches.count - 1)];
                            if (v21 >= this.niceLen) {
                                this.back = (this.matches.dist[(this.matches.count - 1)] + 4);
                                this.skip((v21 - 1));
                                v17_1 = v21;
                                return v17_1;
                            }
                        }
                        int v4 = this.lz.getByte(0);
                        int v5 = this.lz.getByte((this.reps[0] + 1));
                        if ((v21 >= 2) || ((v4 == v5) || (this.repLens[v25] >= 2))) {
                            int v7 = this.lz.getPos();
                            int v10_0 = (v7 & this.posMask);
                            this.opts[1].set1(this.literalEncoder.getPrice(v4, v5, this.lz.getByte(1), v7, this.state), 0, -1);
                            int v12_0 = this.getAnyMatchPrice(this.state, v10_0);
                            int v14_0 = this.getAnyRepPrice(v12_0, this.state);
                            if (v5 == v4) {
                                int v27 = this.getShortRepPrice(v14_0, this.state, v10_0);
                                if (v27 < this.opts[1].price) {
                                    this.opts[1].set1(v27, 0, 0);
                                }
                            }
                            this.optEnd = Math.max(v21, this.repLens[v25]);
                            if (this.optEnd >= 2) {
                                this.updatePrices();
                                this.opts[0].state.set(this.state);
                                System.arraycopy(this.reps, 0, this.opts[0].reps, 0, 4);
                                int v16_0 = this.optEnd;
                                while (v16_0 >= 2) {
                                    this.opts[v16_0].reset();
                                    v16_0--;
                                }
                                int v24_1 = 0;
                                while (v24_1 < 4) {
                                    int v26 = this.repLens[v24_1];
                                    if (v26 >= 2) {
                                        int v19 = this.getLongRepPrice(v14_0, v24_1, this.state, v10_0);
                                        do {
                                            int v23_1 = (v19 + this.repLenEncoder.getPrice(v26, v10_0));
                                            if (v23_1 < this.opts[v26].price) {
                                                this.opts[v26].set1(v23_1, 0, v24_1);
                                            }
                                            v26--;
                                        } while(v26 >= 2);
                                    }
                                    v24_1++;
                                }
                                int v17_0 = Math.max((this.repLens[0] + 1), 2);
                                if (v17_0 <= v21) {
                                    int v22 = this.getNormalMatchPrice(v12_0, this.state);
                                    int v16_1 = 0;
                                    while (v17_0 > this.matches.len[v16_1]) {
                                        v16_1++;
                                    }
                                    while(true) {
                                        int v15 = this.matches.dist[v16_1];
                                        int v23_0 = this.getMatchAndLenPrice(v22, v15, v17_0, v10_0);
                                        if (v23_0 < this.opts[v17_0].price) {
                                            this.opts[v17_0].set1(v23_0, 0, (v15 + 4));
                                        }
                                        if (v17_0 == this.matches.len[v16_1]) {
                                            v16_1++;
                                            if (v16_1 == this.matches.count) {
                                                break;
                                            }
                                        }
                                        v17_0++;
                                    }
                                }
                                int v11_1 = Math.min(this.lz.getAvail(), 4095);
                                do {
                                    int v3_72 = (this.optCur + 1);
                                    this.optCur = v3_72;
                                    if (v3_72 < this.optEnd) {
                                        this.matches = this.getMatches();
                                        if ((this.matches.count <= 0) || (this.matches.len[(this.matches.count - 1)] < this.niceLen)) {
                                            v11_1--;
                                            v7++;
                                            int v10_1 = (v7 & this.posMask);
                                            this = this.updateOptStateAndReps();
                                            int v12_1 = (this.opts[this.optCur].price + this.getAnyMatchPrice(this.opts[this.optCur].state, v10_1));
                                            int v14_1 = this.getAnyRepPrice(v12_1, this.opts[this.optCur].state);
                                            this.calc1BytePrices(v7, v10_1, v11_1, v14_1);
                                            if (v11_1 >= 2) {
                                                int v13 = this.calcLongRepPrices(v7, v10_1, v11_1, v14_1);
                                            }
                                        }
                                    }
                                    v17_1 = this.convertOpts();
                                } while(this.matches.count <= 0);
                                this.calcNormalMatchPrices(v7, v10_1, v11_1, v12_1, v13);
                            } else {
                                if ((org.tukaani.xz.lzma.LZMAEncoderNormal.$assertionsDisabled) || (this.optEnd == 0)) {
                                    this.back = this.opts[1].backPrev;
                                    v17_1 = 1;
                                } else {
                                    throw new AssertionError(this.optEnd);
                                }
                            }
                        } else {
                            v17_1 = 1;
                        }
                    } else {
                        this.back = v25;
                        this.skip((this.repLens[v25] - 1));
                        v17_1 = this.repLens[v25];
                    }
                } else {
                    v17_1 = 1;
                }
            } else {
                throw new AssertionError();
            }
        } else {
            v17_1 = (this.opts[this.optCur].optPrev - this.optCur);
            this.optCur = this.opts[this.optCur].optPrev;
            this.back = this.opts[this.optCur].backPrev;
        }
        return v17_1;
    }

    public void reset()
    {
        this.optCur = 0;
        this.optEnd = 0;
        super.reset();
        return;
    }
}
