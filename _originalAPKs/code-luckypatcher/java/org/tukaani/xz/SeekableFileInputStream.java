package org.tukaani.xz;
public class SeekableFileInputStream extends org.tukaani.xz.SeekableInputStream {
    protected java.io.RandomAccessFile randomAccessFile;

    public SeekableFileInputStream(java.io.File p3)
    {
        this.randomAccessFile = new java.io.RandomAccessFile(p3, "r");
        return;
    }

    public SeekableFileInputStream(java.io.RandomAccessFile p1)
    {
        this.randomAccessFile = p1;
        return;
    }

    public SeekableFileInputStream(String p3)
    {
        this.randomAccessFile = new java.io.RandomAccessFile(p3, "r");
        return;
    }

    public void close()
    {
        this.randomAccessFile.close();
        return;
    }

    public long length()
    {
        return this.randomAccessFile.length();
    }

    public long position()
    {
        return this.randomAccessFile.getFilePointer();
    }

    public int read()
    {
        return this.randomAccessFile.read();
    }

    public int read(byte[] p2)
    {
        return this.randomAccessFile.read(p2);
    }

    public int read(byte[] p2, int p3, int p4)
    {
        return this.randomAccessFile.read(p2, p3, p4);
    }

    public void seek(long p2)
    {
        this.randomAccessFile.seek(p2);
        return;
    }
}
