package org.tukaani.xz;
interface FilterEncoder implements org.tukaani.xz.FilterCoder {

    public abstract long getFilterID();

    public abstract byte[] getFilterProps();

    public abstract org.tukaani.xz.FinishableOutputStream getOutputStream();

    public abstract boolean supportsFlushing();
}
