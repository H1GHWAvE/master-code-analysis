package org.tukaani.xz;
abstract class BCJCoder implements org.tukaani.xz.FilterCoder {
    public static final long ARMTHUMB_FILTER_ID = 8;
    public static final long ARM_FILTER_ID = 7;
    public static final long IA64_FILTER_ID = 6;
    public static final long POWERPC_FILTER_ID = 5;
    public static final long SPARC_FILTER_ID = 9;
    public static final long X86_FILTER_ID = 4;

    BCJCoder()
    {
        return;
    }

    public static boolean isBCJFilterID(long p2)
    {
        if ((p2 < 4) || (p2 > 9)) {
            int v0_4 = 0;
        } else {
            v0_4 = 1;
        }
        return v0_4;
    }

    public boolean changesSize()
    {
        return 0;
    }

    public boolean lastOK()
    {
        return 0;
    }

    public boolean nonLastOK()
    {
        return 1;
    }
}
