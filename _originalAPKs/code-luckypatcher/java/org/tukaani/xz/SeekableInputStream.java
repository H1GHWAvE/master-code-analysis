package org.tukaani.xz;
public abstract class SeekableInputStream extends java.io.InputStream {

    public SeekableInputStream()
    {
        return;
    }

    public abstract long length();

    public abstract long position();

    public abstract void seek();

    public long skip(long p8)
    {
        long v4_0 = 0;
        if (p8 > 0) {
            long v2 = this.length();
            long v0 = this.position();
            if (v0 < v2) {
                if ((v2 - v0) < p8) {
                    p8 = (v2 - v0);
                }
                this.seek((v0 + p8));
                v4_0 = p8;
            }
        }
        return v4_0;
    }
}
