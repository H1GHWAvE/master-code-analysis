package org.tukaani.xz;
 class SimpleInputStream extends java.io.InputStream {
    static final synthetic boolean $assertionsDisabled = False;
    private static final int FILTER_BUF_SIZE = 4096;
    private boolean endReached;
    private java.io.IOException exception;
    private final byte[] filterBuf;
    private int filtered;
    private java.io.InputStream in;
    private int pos;
    private final org.tukaani.xz.simple.SimpleFilter simpleFilter;
    private final byte[] tempBuf;
    private int unfiltered;

    static SimpleInputStream()
    {
        int v0_2;
        if (org.tukaani.xz.SimpleInputStream.desiredAssertionStatus()) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        org.tukaani.xz.SimpleInputStream.$assertionsDisabled = v0_2;
        return;
    }

    SimpleInputStream(java.io.InputStream p3, org.tukaani.xz.simple.SimpleFilter p4)
    {
        AssertionError v0_1 = new byte[4096];
        this.filterBuf = v0_1;
        this.pos = 0;
        this.filtered = 0;
        this.unfiltered = 0;
        this.endReached = 0;
        this.exception = 0;
        AssertionError v0_4 = new byte[1];
        this.tempBuf = v0_4;
        if (p3 != null) {
            if ((org.tukaani.xz.SimpleInputStream.$assertionsDisabled) || (p4 != null)) {
                this.in = p3;
                this.simpleFilter = p4;
                return;
            } else {
                throw new AssertionError();
            }
        } else {
            throw new NullPointerException();
        }
    }

    static int getMemoryUsage()
    {
        return 5;
    }

    public int available()
    {
        if (this.in != null) {
            if (this.exception == null) {
                return this.filtered;
            } else {
                throw this.exception;
            }
        } else {
            throw new org.tukaani.xz.XZIOException("Stream closed");
        }
    }

    public void close()
    {
        if (this.in != null) {
            try {
                this.in.close();
                this.in = 0;
            } catch (Throwable v0_2) {
                this.in = 0;
                throw v0_2;
            }
        }
        return;
    }

    public int read()
    {
        int v0_0 = -1;
        if (this.read(this.tempBuf, 0, 1) != -1) {
            v0_0 = (this.tempBuf[0] & 255);
        }
        return v0_0;
    }

    public int read(byte[] p12, int p13, int p14)
    {
        java.io.IOException v3 = 0;
        if ((p13 >= 0) && ((p14 >= 0) && (((p13 + p14) >= 0) && ((p13 + p14) <= p12.length)))) {
            if (p14 != 0) {
                if (this.in != null) {
                    if (this.exception == null) {
                        v3 = 0;
                        try {
                            while(true) {
                                int v0 = Math.min(this.filtered, p14);
                                System.arraycopy(this.filterBuf, this.pos, p12, p13, v0);
                                this.pos = (this.pos + v0);
                                this.filtered = (this.filtered - v0);
                                p13 += v0;
                                p14 -= v0;
                                v3 += v0;
                                this.unfiltered = (this.unfiltered - this.filtered);
                            }
                            if (v3 <= null) {
                                v3 = -1;
                            }
                        } catch (java.io.IOException v1) {
                            this.exception = v1;
                            throw v1;
                        }
                        if (((this.pos + this.filtered) + this.unfiltered) == 4096) {
                            System.arraycopy(this.filterBuf, this.pos, this.filterBuf, 0, (this.filtered + this.unfiltered));
                            this.pos = 0;
                        }
                        if ((p14 != 0) && (!this.endReached)) {
                            if ((org.tukaani.xz.SimpleInputStream.$assertionsDisabled) || (this.filtered == 0)) {
                                int v2_1 = this.in.read(this.filterBuf, ((this.pos + this.filtered) + this.unfiltered), (4096 - ((this.pos + this.filtered) + this.unfiltered)));
                                if (v2_1 != -1) {
                                    this.unfiltered = (this.unfiltered + v2_1);
                                    this.filtered = this.simpleFilter.code(this.filterBuf, this.pos, this.unfiltered);
                                    if ((org.tukaani.xz.SimpleInputStream.$assertionsDisabled) || (this.filtered <= this.unfiltered)) {
                                    } else {
                                        throw new AssertionError();
                                    }
                                } else {
                                    this.endReached = 1;
                                    this.filtered = this.unfiltered;
                                    this.unfiltered = 0;
                                }
                            } else {
                                throw new AssertionError();
                            }
                        }
                    } else {
                        throw this.exception;
                    }
                } else {
                    throw new org.tukaani.xz.XZIOException("Stream closed");
                }
            }
            return v3;
        } else {
            throw new IndexOutOfBoundsException();
        }
    }
}
