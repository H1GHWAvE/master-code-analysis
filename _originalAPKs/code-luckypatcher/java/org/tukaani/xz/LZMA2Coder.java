package org.tukaani.xz;
abstract class LZMA2Coder implements org.tukaani.xz.FilterCoder {
    public static final long FILTER_ID = 33;

    LZMA2Coder()
    {
        return;
    }

    public boolean changesSize()
    {
        return 1;
    }

    public boolean lastOK()
    {
        return 1;
    }

    public boolean nonLastOK()
    {
        return 0;
    }
}
