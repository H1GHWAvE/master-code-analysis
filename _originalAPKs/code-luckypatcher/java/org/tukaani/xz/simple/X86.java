package org.tukaani.xz.simple;
public final class X86 implements org.tukaani.xz.simple.SimpleFilter {
    private static final boolean[] MASK_TO_ALLOWED_STATUS;
    private static final int[] MASK_TO_BIT_NUMBER;
    private final boolean isEncoder;
    private int pos;
    private int prevMask;

    static X86()
    {
        int[] v0_0 = new boolean[8];
        v0_0 = {1, 1, 1, 0, 1, 0, 0, 0};
        org.tukaani.xz.simple.X86.MASK_TO_ALLOWED_STATUS = v0_0;
        int[] v0_1 = new int[8];
        v0_1 = {0, 1, 2, 2, 3, 3, 3, 3};
        org.tukaani.xz.simple.X86.MASK_TO_BIT_NUMBER = v0_1;
        return;
    }

    public X86(boolean p2, int p3)
    {
        this.prevMask = 0;
        this.isEncoder = p2;
        this.pos = (p3 + 5);
        return;
    }

    private static boolean test86MSByte(byte p2)
    {
        int v1_1;
        int v0 = (p2 & 255);
        if ((v0 != 0) && (v0 != 255)) {
            v1_1 = 0;
        } else {
            v1_1 = 1;
        }
        return v1_1;
    }

    public int code(byte[] p11, int p12, int p13)
    {
        int v6_0 = 0;
        int v4_0 = (p12 - 1);
        int v1 = ((p12 + p13) - 5);
        int v2_0 = p12;
        while (v2_0 <= v1) {
            if ((p11[v2_0] & 254) == 232) {
                int v4_2 = (v2_0 - v4_0);
                if ((v4_2 & -4) == 0) {
                    this.prevMask = ((this.prevMask << (v4_2 - 1)) & 7);
                    if ((this.prevMask != 0) && ((!org.tukaani.xz.simple.X86.MASK_TO_ALLOWED_STATUS[this.prevMask]) || (org.tukaani.xz.simple.X86.test86MSByte(p11[((v2_0 + 4) - org.tukaani.xz.simple.X86.MASK_TO_BIT_NUMBER[this.prevMask])])))) {
                        v4_0 = v2_0;
                        this.prevMask = ((this.prevMask << 1) | 1);
                        v2_0++;
                    }
                } else {
                    this.prevMask = 0;
                }
                v4_0 = v2_0;
                if (!org.tukaani.xz.simple.X86.test86MSByte(p11[(v2_0 + 4)])) {
                    this.prevMask = ((this.prevMask << 1) | 1);
                } else {
                    int v5 = ((((p11[(v2_0 + 1)] & 255) | ((p11[(v2_0 + 2)] & 255) << 8)) | ((p11[(v2_0 + 3)] & 255) << 16)) | ((p11[(v2_0 + 4)] & 255) << 24));
                    while(true) {
                        int v0;
                        if (!this.isEncoder) {
                            v0 = (v5 - ((this.pos + v2_0) - p12));
                        } else {
                            v0 = (v5 + ((this.pos + v2_0) - p12));
                        }
                        if (this.prevMask == 0) {
                            break;
                        }
                        int v3 = (org.tukaani.xz.simple.X86.MASK_TO_BIT_NUMBER[this.prevMask] * 8);
                        if (!org.tukaani.xz.simple.X86.test86MSByte(((byte) (v0 >> (24 - v3))))) {
                            break;
                        }
                        v5 = (v0 ^ ((1 << (32 - v3)) - 1));
                    }
                    p11[(v2_0 + 1)] = ((byte) v0);
                    p11[(v2_0 + 2)] = ((byte) (v0 >> 8));
                    p11[(v2_0 + 3)] = ((byte) (v0 >> 16));
                    p11[(v2_0 + 4)] = ((byte) ((((v0 >> 24) & 1) - 1) ^ -1));
                    v2_0 += 4;
                }
            }
        }
        int v4_1 = (v2_0 - v4_0);
        if ((v4_1 & -4) == 0) {
            v6_0 = (this.prevMask << (v4_1 - 1));
        }
        this.prevMask = v6_0;
        int v2_1 = (v2_0 - p12);
        this.pos = (this.pos + v2_1);
        return v2_1;
    }
}
