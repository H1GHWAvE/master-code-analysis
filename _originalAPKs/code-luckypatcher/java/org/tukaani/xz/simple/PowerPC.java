package org.tukaani.xz.simple;
public final class PowerPC implements org.tukaani.xz.simple.SimpleFilter {
    private final boolean isEncoder;
    private int pos;

    public PowerPC(boolean p1, int p2)
    {
        this.isEncoder = p1;
        this.pos = p2;
        return;
    }

    public int code(byte[] p7, int p8, int p9)
    {
        int v1 = ((p8 + p9) - 4);
        int v2_0 = p8;
        while (v2_0 <= v1) {
            if (((p7[v2_0] & 252) == 72) && ((p7[(v2_0 + 3)] & 3) == 1)) {
                int v0;
                int v3 = (((((p7[v2_0] & 3) << 24) | ((p7[(v2_0 + 1)] & 255) << 16)) | ((p7[(v2_0 + 2)] & 255) << 8)) | (p7[(v2_0 + 3)] & 252));
                if (!this.isEncoder) {
                    v0 = (v3 - ((this.pos + v2_0) - p8));
                } else {
                    v0 = (v3 + ((this.pos + v2_0) - p8));
                }
                p7[v2_0] = ((byte) (((v0 >> 24) & 3) | 72));
                p7[(v2_0 + 1)] = ((byte) (v0 >> 16));
                p7[(v2_0 + 2)] = ((byte) (v0 >> 8));
                p7[(v2_0 + 3)] = ((byte) ((p7[(v2_0 + 3)] & 3) | v0));
            }
            v2_0 += 4;
        }
        int v2_1 = (v2_0 - p8);
        this.pos = (this.pos + v2_1);
        return v2_1;
    }
}
