package org.tukaani.xz;
public class CorruptedInputException extends org.tukaani.xz.XZIOException {
    private static final long serialVersionUID = 3;

    public CorruptedInputException()
    {
        this("Compressed data is corrupt");
        return;
    }

    public CorruptedInputException(String p1)
    {
        this(p1);
        return;
    }
}
