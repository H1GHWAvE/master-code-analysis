package org.tukaani.xz;
public class SeekableXZInputStream extends org.tukaani.xz.SeekableInputStream {
    static final synthetic boolean $assertionsDisabled;
    private int blockCount;
    private org.tukaani.xz.BlockInputStream blockDecoder;
    private org.tukaani.xz.check.Check check;
    private int checkTypes;
    private final org.tukaani.xz.index.BlockInfo curBlockInfo;
    private long curPos;
    private boolean endReached;
    private java.io.IOException exception;
    private org.tukaani.xz.SeekableInputStream in;
    private int indexMemoryUsage;
    private long largestBlockSize;
    private final int memoryLimit;
    private final org.tukaani.xz.index.BlockInfo queriedBlockInfo;
    private boolean seekNeeded;
    private long seekPos;
    private final java.util.ArrayList streams;
    private final byte[] tempBuf;
    private long uncompressedSize;

    static SeekableXZInputStream()
    {
        int v0_2;
        if (org.tukaani.xz.SeekableXZInputStream.desiredAssertionStatus()) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        org.tukaani.xz.SeekableXZInputStream.$assertionsDisabled = v0_2;
        return;
    }

    public SeekableXZInputStream(org.tukaani.xz.SeekableInputStream p2)
    {
        this(p2, -1);
        return;
    }

    public SeekableXZInputStream(org.tukaani.xz.SeekableInputStream p26, int p27)
    {
        org.tukaani.xz.SeekableXZInputStream v25_1 = ;
v25_1.indexMemoryUsage = 0;
        v25_1.streams = new java.util.ArrayList();
        v25_1.checkTypes = 0;
        v25_1.uncompressedSize = 0;
        v25_1.largestBlockSize = 0;
        v25_1.blockCount = 0;
        v25_1.blockDecoder = 0;
        v25_1.curPos = 0;
        v25_1.seekNeeded = 0;
        v25_1.endReached = 0;
        v25_1.exception = 0;
        java.util.ArrayList v4_10 = new byte[1];
        v25_1.tempBuf = v4_10;
        v25_1.in = p26;
        java.io.DataInputStream v14_1 = new java.io.DataInputStream(p26);
        p26.seek(0);
        byte[] v9_0 = new byte[org.tukaani.xz.XZ.HEADER_MAGIC.length];
        v14_1.readFully(v9_0);
        if (java.util.Arrays.equals(v9_0, org.tukaani.xz.XZ.HEADER_MAGIC)) {
            long v17_0 = p26.length();
            if ((3 & v17_0) == 0) {
                byte[] v9_1 = new byte[12];
                long v6 = 0;
                while (v17_0 > 0) {
                    if (v17_0 >= 12) {
                        p26.seek((v17_0 - 12));
                        v14_1.readFully(v9_1);
                        if ((v9_1[8] != 0) || ((v9_1[9] != 0) || ((v9_1[10] != 0) || (v9_1[11] != 0)))) {
                            long v17_1 = (v17_0 - 12);
                            org.tukaani.xz.common.StreamFlags v5 = org.tukaani.xz.common.DecoderUtil.decodeStreamFooter(v9_1);
                            if (v5.backwardSize < v17_1) {
                                v25_1.check = org.tukaani.xz.check.Check.getInstance(v5.checkType);
                                v25_1.checkTypes = (v25_1.checkTypes | (1 << v5.checkType));
                                p26.seek((v17_1 - v5.backwardSize));
                                try {
                                    org.tukaani.xz.index.IndexDecoder v3_1 = new org.tukaani.xz.index.IndexDecoder(p26, v5, v6, p27);
                                    v25_1.indexMemoryUsage = (v25_1.indexMemoryUsage + v3_1.getMemoryUsage());
                                } catch (org.tukaani.xz.MemoryLimitException v11) {
                                    if (!org.tukaani.xz.SeekableXZInputStream.$assertionsDisabled) {
                                        if (p27 < 0) {
                                            throw new AssertionError();
                                        }
                                    }
                                    throw new org.tukaani.xz.MemoryLimitException((v11.getMemoryNeeded() + v25_1.indexMemoryUsage), (v25_1.indexMemoryUsage + p27));
                                }
                                if (p27 >= 0) {
                                    p27 -= v3_1.getMemoryUsage();
                                    if ((!org.tukaani.xz.SeekableXZInputStream.$assertionsDisabled) && (p27 < 0)) {
                                        throw new AssertionError();
                                    }
                                }
                                if (v25_1.largestBlockSize < v3_1.getLargestBlockSize()) {
                                    v25_1.largestBlockSize = v3_1.getLargestBlockSize();
                                }
                                long v15 = (v3_1.getStreamSize() - 12);
                                if (v17_1 >= v15) {
                                    v17_0 = (v17_1 - v15);
                                    p26.seek(v17_0);
                                    v14_1.readFully(v9_1);
                                    if (org.tukaani.xz.common.DecoderUtil.areStreamFlagsEqual(org.tukaani.xz.common.DecoderUtil.decodeStreamHeader(v9_1), v5)) {
                                        v25_1.uncompressedSize = (v25_1.uncompressedSize + v3_1.getUncompressedSize());
                                        if (v25_1.uncompressedSize >= 0) {
                                            v25_1.blockCount = (v25_1.blockCount + v3_1.getRecordCount());
                                            if (v25_1.blockCount >= 0) {
                                                v25_1.streams.add(v3_1);
                                                v6 = 0;
                                            } else {
                                                throw new org.tukaani.xz.UnsupportedOptionsException("XZ file has over 2147483647 Blocks");
                                            }
                                        } else {
                                            throw new org.tukaani.xz.UnsupportedOptionsException("XZ file is too big");
                                        }
                                    } else {
                                        throw new org.tukaani.xz.CorruptedInputException("XZ Stream Footer does not match Stream Header");
                                    }
                                } else {
                                    throw new org.tukaani.xz.CorruptedInputException("XZ Index indicates too big compressed size for the XZ Stream");
                                }
                            } else {
                                throw new org.tukaani.xz.CorruptedInputException("Backward Size in XZ Stream Footer is too big");
                            }
                        } else {
                            v6 += 4;
                            v17_0 -= 4;
                        }
                    } else {
                        throw new org.tukaani.xz.CorruptedInputException();
                    }
                }
                if ((org.tukaani.xz.SeekableXZInputStream.$assertionsDisabled) || (v17_0 == 0)) {
                    v25_1.memoryLimit = p27;
                    org.tukaani.xz.index.IndexDecoder v19_1 = ((org.tukaani.xz.index.IndexDecoder) v25_1.streams.get((v25_1.streams.size() - 1)));
                    int v13 = (v25_1.streams.size() - 2);
                    while (v13 >= 0) {
                        org.tukaani.xz.index.IndexDecoder v10_1 = ((org.tukaani.xz.index.IndexDecoder) v25_1.streams.get(v13));
                        v10_1.setOffsets(v19_1);
                        v19_1 = v10_1;
                        v13--;
                    }
                    org.tukaani.xz.index.IndexDecoder v12_1 = ((org.tukaani.xz.index.IndexDecoder) v25_1.streams.get((v25_1.streams.size() - 1)));
                    v25_1.curBlockInfo = new org.tukaani.xz.index.BlockInfo(v12_1);
                    v25_1.queriedBlockInfo = new org.tukaani.xz.index.BlockInfo(v12_1);
                    return;
                } else {
                    throw new AssertionError();
                }
            } else {
                throw new org.tukaani.xz.CorruptedInputException("XZ file size is not a multiple of 4 bytes");
            }
        } else {
            throw new org.tukaani.xz.XZFormatException();
        }
    }

    private void initBlockDecoder()
    {
        try {
            this.blockDecoder = 0;
            this.blockDecoder = new org.tukaani.xz.BlockInputStream(this.in, this.check, this.memoryLimit, this.curBlockInfo.unpaddedSize, this.curBlockInfo.uncompressedSize);
            return;
        } catch (org.tukaani.xz.IndexIndicatorException v8) {
            throw new org.tukaani.xz.CorruptedInputException();
        } catch (org.tukaani.xz.IndexIndicatorException v8) {
            if (!org.tukaani.xz.SeekableXZInputStream.$assertionsDisabled) {
                if (this.memoryLimit < 0) {
                    throw new AssertionError();
                }
            }
            throw new org.tukaani.xz.MemoryLimitException((v8.getMemoryNeeded() + this.indexMemoryUsage), (this.memoryLimit + this.indexMemoryUsage));
        }
    }

    private void locateBlockByNumber(org.tukaani.xz.index.BlockInfo p6, int p7)
    {
        if ((p7 >= 0) && (p7 < this.blockCount)) {
            if (p6.blockNumber != p7) {
                int v0 = 0;
                while(true) {
                    org.tukaani.xz.index.IndexDecoder v1_1 = ((org.tukaani.xz.index.IndexDecoder) this.streams.get(v0));
                    if (v1_1.hasRecord(p7)) {
                        break;
                    }
                    v0++;
                }
                v1_1.setBlockInfo(p6, p7);
            }
            return;
        } else {
            throw new IndexOutOfBoundsException(new StringBuilder().append("Invalid XZ Block number: ").append(p7).toString());
        }
    }

    private void locateBlockByPos(org.tukaani.xz.index.BlockInfo p9, long p10)
    {
        if ((p10 >= 0) && (p10 < this.uncompressedSize)) {
            int v0 = 0;
            while(true) {
                org.tukaani.xz.index.IndexDecoder v1_1 = ((org.tukaani.xz.index.IndexDecoder) this.streams.get(v0));
                if (v1_1.hasUncompressedOffset(p10)) {
                    break;
                }
                v0++;
            }
            v1_1.locateBlock(p9, p10);
            if ((org.tukaani.xz.SeekableXZInputStream.$assertionsDisabled) || ((p9.compressedOffset & 3) == 0)) {
                if ((org.tukaani.xz.SeekableXZInputStream.$assertionsDisabled) || (p9.uncompressedSize > 0)) {
                    if ((org.tukaani.xz.SeekableXZInputStream.$assertionsDisabled) || (p10 >= p9.uncompressedOffset)) {
                        if ((org.tukaani.xz.SeekableXZInputStream.$assertionsDisabled) || (p10 < (p9.uncompressedOffset + p9.uncompressedSize))) {
                            return;
                        } else {
                            throw new AssertionError();
                        }
                    } else {
                        throw new AssertionError();
                    }
                } else {
                    throw new AssertionError();
                }
            } else {
                throw new AssertionError();
            }
        } else {
            throw new IndexOutOfBoundsException(new StringBuilder().append("Invalid uncompressed position: ").append(p10).toString());
        }
    }

    private void seek()
    {
        if (this.seekNeeded) {
            this.seekNeeded = 0;
            if (this.seekPos < this.uncompressedSize) {
                this.endReached = 0;
                this.locateBlockByPos(this.curBlockInfo, this.seekPos);
                if ((this.curPos <= this.curBlockInfo.uncompressedOffset) || (this.curPos > this.seekPos)) {
                    this.in.seek(this.curBlockInfo.compressedOffset);
                    this.check = org.tukaani.xz.check.Check.getInstance(this.curBlockInfo.getCheckType());
                    this.initBlockDecoder();
                    this.curPos = this.curBlockInfo.uncompressedOffset;
                }
                if (this.seekPos > this.curPos) {
                    long v0 = (this.seekPos - this.curPos);
                    if (this.blockDecoder.skip(v0) == v0) {
                        this.curPos = this.seekPos;
                    } else {
                        throw new org.tukaani.xz.CorruptedInputException();
                    }
                }
            } else {
                this.curPos = this.seekPos;
                this.blockDecoder = 0;
                this.endReached = 1;
            }
        } else {
            if (!this.curBlockInfo.hasNext()) {
                this.seekPos = this.curPos;
            } else {
                this.curBlockInfo.setNext();
                this.initBlockDecoder();
            }
        }
        return;
    }

    public int available()
    {
        if (this.in != null) {
            if (this.exception == null) {
                if ((!this.endReached) && ((!this.seekNeeded) && (this.blockDecoder != null))) {
                    int v0_6 = this.blockDecoder.available();
                } else {
                    v0_6 = 0;
                }
                return v0_6;
            } else {
                throw this.exception;
            }
        } else {
            throw new org.tukaani.xz.XZIOException("Stream closed");
        }
    }

    public void close()
    {
        if (this.in != null) {
            try {
                this.in.close();
                this.in = 0;
            } catch (Throwable v0_2) {
                this.in = 0;
                throw v0_2;
            }
        }
        return;
    }

    public int getBlockCheckType(int p2)
    {
        this.locateBlockByNumber(this.queriedBlockInfo, p2);
        return this.queriedBlockInfo.getCheckType();
    }

    public long getBlockCompPos(int p3)
    {
        this.locateBlockByNumber(this.queriedBlockInfo, p3);
        return this.queriedBlockInfo.compressedOffset;
    }

    public long getBlockCompSize(int p5)
    {
        this.locateBlockByNumber(this.queriedBlockInfo, p5);
        return ((this.queriedBlockInfo.unpaddedSize + 3) & -4);
    }

    public int getBlockCount()
    {
        return this.blockCount;
    }

    public int getBlockNumber(long p2)
    {
        this.locateBlockByPos(this.queriedBlockInfo, p2);
        return this.queriedBlockInfo.blockNumber;
    }

    public long getBlockPos(int p3)
    {
        this.locateBlockByNumber(this.queriedBlockInfo, p3);
        return this.queriedBlockInfo.uncompressedOffset;
    }

    public long getBlockSize(int p3)
    {
        this.locateBlockByNumber(this.queriedBlockInfo, p3);
        return this.queriedBlockInfo.uncompressedSize;
    }

    public int getCheckTypes()
    {
        return this.checkTypes;
    }

    public int getIndexMemoryUsage()
    {
        return this.indexMemoryUsage;
    }

    public long getLargestBlockSize()
    {
        return this.largestBlockSize;
    }

    public int getStreamCount()
    {
        return this.streams.size();
    }

    public long length()
    {
        return this.uncompressedSize;
    }

    public long position()
    {
        if (this.in != null) {
            long v0_2;
            if (!this.seekNeeded) {
                v0_2 = this.curPos;
            } else {
                v0_2 = this.seekPos;
            }
            return v0_2;
        } else {
            throw new org.tukaani.xz.XZIOException("Stream closed");
        }
    }

    public int read()
    {
        int v0_0 = -1;
        if (this.read(this.tempBuf, 0, 1) != -1) {
            v0_0 = (this.tempBuf[0] & 255);
        }
        return v0_0;
    }

    public int read(byte[] p9, int p10, int p11)
    {
        if ((p10 >= 0) && ((p11 >= 0) && (((p10 + p11) >= 0) && ((p10 + p11) <= p9.length)))) {
            int v2;
            if (p11 != 0) {
                if (this.in != null) {
                    if (this.exception == null) {
                        v2 = 0;
                        try {
                            if (this.seekNeeded) {
                                this.seek();
                            }
                        } catch (org.tukaani.xz.CorruptedInputException v0_0) {
                            if ((v0_0 instanceof java.io.EOFException)) {
                                v0_0 = new org.tukaani.xz.CorruptedInputException();
                            }
                            this.exception = v0_0;
                            if (v2 == 0) {
                                throw v0_0;
                            }
                        }
                        if (this.endReached) {
                            v2 = -1;
                            return v2;
                        }
                        while (p11 > 0) {
                            if (this.blockDecoder == null) {
                                this.seek();
                                if (this.endReached) {
                                    break;
                                }
                            }
                            int v1 = this.blockDecoder.read(p9, p10, p11);
                            if (v1 <= 0) {
                                if (v1 == -1) {
                                    this.blockDecoder = 0;
                                }
                            } else {
                                this.curPos = (this.curPos + ((long) v1));
                                v2 += v1;
                                p10 += v1;
                                p11 -= v1;
                            }
                        }
                    } else {
                        throw this.exception;
                    }
                } else {
                    throw new org.tukaani.xz.XZIOException("Stream closed");
                }
            } else {
                v2 = 0;
            }
            return v2;
        } else {
            throw new IndexOutOfBoundsException();
        }
    }

    public void seek(long p4)
    {
        if (this.in != null) {
            if (p4 >= 0) {
                this.seekPos = p4;
                this.seekNeeded = 1;
                return;
            } else {
                throw new org.tukaani.xz.XZIOException(new StringBuilder().append("Negative seek position: ").append(p4).toString());
            }
        } else {
            throw new org.tukaani.xz.XZIOException("Stream closed");
        }
    }

    public void seekToBlock(int p4)
    {
        if (this.in != null) {
            if ((p4 >= 0) && (p4 < this.blockCount)) {
                this.seekPos = this.getBlockPos(p4);
                this.seekNeeded = 1;
                return;
            } else {
                throw new org.tukaani.xz.XZIOException(new StringBuilder().append("Invalid XZ Block number: ").append(p4).toString());
            }
        } else {
            throw new org.tukaani.xz.XZIOException("Stream closed");
        }
    }
}
