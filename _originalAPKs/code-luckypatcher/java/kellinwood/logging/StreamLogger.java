package kellinwood.logging;
public class StreamLogger extends kellinwood.logging.AbstractLogger {
    java.io.PrintStream out;

    public StreamLogger(String p1, java.io.PrintStream p2)
    {
        this(p1);
        this.out = p2;
        return;
    }

    protected void write(String p3, String p4, Throwable p5)
    {
        this.out.print(this.format(p3, p4));
        if (p5 != null) {
            p5.printStackTrace(this.out);
        }
        return;
    }
}
