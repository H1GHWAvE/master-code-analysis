package kellinwood.logging;
public abstract class AbstractLogger implements kellinwood.logging.LoggerInterface {
    protected String category;
    java.text.SimpleDateFormat dateFormat;

    public AbstractLogger(String p3)
    {
        this.dateFormat = new java.text.SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        this.category = p3;
        return;
    }

    public void debug(String p3)
    {
        this.writeFixNullMessage("DEBUG", p3, 0);
        return;
    }

    public void debug(String p2, Throwable p3)
    {
        this.writeFixNullMessage("DEBUG", p2, p3);
        return;
    }

    public void error(String p3)
    {
        this.writeFixNullMessage("ERROR", p3, 0);
        return;
    }

    public void error(String p2, Throwable p3)
    {
        this.writeFixNullMessage("ERROR", p2, p3);
        return;
    }

    protected String format(String p6, String p7)
    {
        Object[] v1_1 = new Object[4];
        v1_1[0] = this.dateFormat.format(new java.util.Date());
        v1_1[1] = p6;
        v1_1[2] = this.category;
        v1_1[3] = p7;
        return String.format("%s %s %s: %s\n", v1_1);
    }

    public void info(String p3)
    {
        this.writeFixNullMessage("INFO", p3, 0);
        return;
    }

    public void info(String p2, Throwable p3)
    {
        this.writeFixNullMessage("INFO", p2, p3);
        return;
    }

    public boolean isDebugEnabled()
    {
        return 1;
    }

    public boolean isErrorEnabled()
    {
        return 1;
    }

    public boolean isInfoEnabled()
    {
        return 1;
    }

    public boolean isWarningEnabled()
    {
        return 1;
    }

    public void warning(String p3)
    {
        this.writeFixNullMessage("WARNING", p3, 0);
        return;
    }

    public void warning(String p2, Throwable p3)
    {
        this.writeFixNullMessage("WARNING", p2, p3);
        return;
    }

    protected abstract void write();

    protected void writeFixNullMessage(String p2, String p3, Throwable p4)
    {
        if (p3 == null) {
            if (p4 == null) {
                p3 = "null";
            } else {
                p3 = p4.getClass().getName();
            }
        }
        this.write(p2, p3, p4);
        return;
    }
}
