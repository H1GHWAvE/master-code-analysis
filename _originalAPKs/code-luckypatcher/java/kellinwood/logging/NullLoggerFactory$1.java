package kellinwood.logging;
final class NullLoggerFactory$1 implements kellinwood.logging.LoggerInterface {

    NullLoggerFactory$1()
    {
        return;
    }

    public void debug(String p1)
    {
        return;
    }

    public void debug(String p1, Throwable p2)
    {
        return;
    }

    public void error(String p1)
    {
        return;
    }

    public void error(String p1, Throwable p2)
    {
        return;
    }

    public void info(String p1)
    {
        return;
    }

    public void info(String p1, Throwable p2)
    {
        return;
    }

    public boolean isDebugEnabled()
    {
        return 0;
    }

    public boolean isErrorEnabled()
    {
        return 0;
    }

    public boolean isInfoEnabled()
    {
        return 0;
    }

    public boolean isWarningEnabled()
    {
        return 0;
    }

    public void warning(String p1)
    {
        return;
    }

    public void warning(String p1, Throwable p2)
    {
        return;
    }
}
