package kellinwood.logging;
public class ConsoleLoggerFactory implements kellinwood.logging.LoggerFactory {

    public ConsoleLoggerFactory()
    {
        return;
    }

    public kellinwood.logging.LoggerInterface getLogger(String p3)
    {
        return new kellinwood.logging.StreamLogger(p3, System.out);
    }
}
