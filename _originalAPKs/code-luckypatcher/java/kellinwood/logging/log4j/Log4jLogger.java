package kellinwood.logging.log4j;
public class Log4jLogger implements kellinwood.logging.LoggerInterface {
    org.apache.log4j.Logger log;

    public Log4jLogger(String p2)
    {
        this.log = org.apache.log4j.Logger.getLogger(p2);
        return;
    }

    public void debug(String p2)
    {
        this.log.debug(p2);
        return;
    }

    public void debug(String p2, Throwable p3)
    {
        this.log.debug(p2, p3);
        return;
    }

    public void error(String p2)
    {
        this.log.error(p2);
        return;
    }

    public void error(String p2, Throwable p3)
    {
        this.log.error(p2, p3);
        return;
    }

    public void info(String p2)
    {
        this.log.info(p2);
        return;
    }

    public void info(String p2, Throwable p3)
    {
        this.log.info(p2, p3);
        return;
    }

    public boolean isDebugEnabled()
    {
        return this.log.isDebugEnabled();
    }

    public boolean isErrorEnabled()
    {
        return 1;
    }

    public boolean isInfoEnabled()
    {
        return this.log.isInfoEnabled();
    }

    public boolean isWarningEnabled()
    {
        return 1;
    }

    public void warning(String p2)
    {
        this.log.warn(p2);
        return;
    }

    public void warning(String p2, Throwable p3)
    {
        this.log.warn(p2, p3);
        return;
    }
}
