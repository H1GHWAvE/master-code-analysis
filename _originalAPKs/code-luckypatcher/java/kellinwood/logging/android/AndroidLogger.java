package kellinwood.logging.android;
public class AndroidLogger extends kellinwood.logging.AbstractLogger {
    boolean isDebugToastEnabled;
    boolean isErrorToastEnabled;
    boolean isInfoToastEnabled;
    boolean isWarningToastEnabled;
    android.content.Context toastContext;

    public AndroidLogger(String p4)
    {
        this(p4);
        this.isErrorToastEnabled = 1;
        this.isWarningToastEnabled = 1;
        this.isInfoToastEnabled = 0;
        this.isDebugToastEnabled = 0;
        int v0 = this.category.lastIndexOf(46);
        if (v0 > 0) {
            this.category = this.category.substring((v0 + 1));
        }
        return;
    }

    public void debugLO(String p3, Throwable p4)
    {
        boolean v0 = this.isDebugToastEnabled;
        this.isDebugToastEnabled = 0;
        this.writeFixNullMessage("DEBUG", p3, p4);
        this.isDebugToastEnabled = v0;
        return;
    }

    public void errorLO(String p3, Throwable p4)
    {
        boolean v0 = this.isErrorToastEnabled;
        this.isErrorToastEnabled = 0;
        this.writeFixNullMessage("ERROR", p3, p4);
        this.isErrorToastEnabled = v0;
        return;
    }

    public android.content.Context getToastContext()
    {
        return this.toastContext;
    }

    public void infoLO(String p3, Throwable p4)
    {
        boolean v0 = this.isInfoToastEnabled;
        this.isInfoToastEnabled = 0;
        this.writeFixNullMessage("INFO", p3, p4);
        this.isInfoToastEnabled = v0;
        return;
    }

    public boolean isDebugEnabled()
    {
        return android.util.Log.isLoggable(this.category, 3);
    }

    public boolean isDebugToastEnabled()
    {
        return this.isDebugToastEnabled;
    }

    public boolean isErrorEnabled()
    {
        return android.util.Log.isLoggable(this.category, 6);
    }

    public boolean isErrorToastEnabled()
    {
        return this.isErrorToastEnabled;
    }

    public boolean isInfoEnabled()
    {
        return android.util.Log.isLoggable(this.category, 4);
    }

    public boolean isInfoToastEnabled()
    {
        return this.isInfoToastEnabled;
    }

    public boolean isWarningEnabled()
    {
        return android.util.Log.isLoggable(this.category, 5);
    }

    public boolean isWarningToastEnabled()
    {
        return this.isWarningToastEnabled;
    }

    public void setDebugToastEnabled(boolean p1)
    {
        this.isDebugToastEnabled = p1;
        return;
    }

    public void setErrorToastEnabled(boolean p1)
    {
        this.isErrorToastEnabled = p1;
        return;
    }

    public void setInfoToastEnabled(boolean p1)
    {
        this.isInfoToastEnabled = p1;
        return;
    }

    public void setToastContext(android.content.Context p1)
    {
        this.toastContext = p1;
        return;
    }

    public void setWarningToastEnabled(boolean p1)
    {
        this.isWarningToastEnabled = p1;
        return;
    }

    protected void toast(String p4)
    {
        try {
            if (this.toastContext != null) {
                android.widget.Toast.makeText(this.toastContext, p4, 1).show();
            }
        } catch (Throwable v0) {
            android.util.Log.e(this.category, p4, v0);
        }
        return;
    }

    public void warningLO(String p3, Throwable p4)
    {
        boolean v0 = this.isWarningToastEnabled;
        this.isWarningToastEnabled = 0;
        this.writeFixNullMessage("WARNING", p3, p4);
        this.isWarningToastEnabled = v0;
        return;
    }

    public void write(String p2, String p3, Throwable p4)
    {
        if (!"ERROR".equals(p2)) {
            if (!"DEBUG".equals(p2)) {
                if (!"WARNING".equals(p2)) {
                    if ("INFO".equals(p2)) {
                        if (p4 == null) {
                            android.util.Log.i(this.category, p3);
                        } else {
                            android.util.Log.i(this.category, p3, p4);
                        }
                        if (this.isInfoToastEnabled) {
                            this.toast(p3);
                        }
                    }
                } else {
                    if (p4 == null) {
                        android.util.Log.w(this.category, p3);
                    } else {
                        android.util.Log.w(this.category, p3, p4);
                    }
                    if (this.isWarningToastEnabled) {
                        this.toast(p3);
                    }
                }
            } else {
                if (p4 == null) {
                    android.util.Log.d(this.category, p3);
                } else {
                    android.util.Log.d(this.category, p3, p4);
                }
                if (this.isDebugToastEnabled) {
                    this.toast(p3);
                }
            }
        } else {
            if (p4 == null) {
                android.util.Log.e(this.category, p3);
            } else {
                android.util.Log.e(this.category, p3, p4);
            }
            if (this.isErrorToastEnabled) {
                this.toast(p3);
            }
        }
        return;
    }
}
