package kellinwood.logging;
public class LoggerManager {
    static kellinwood.logging.LoggerFactory factory;
    static java.util.Map loggers;

    static LoggerManager()
    {
        kellinwood.logging.LoggerManager.factory = new kellinwood.logging.NullLoggerFactory();
        kellinwood.logging.LoggerManager.loggers = new java.util.TreeMap();
        return;
    }

    public LoggerManager()
    {
        return;
    }

    public static kellinwood.logging.LoggerInterface getLogger(String p2)
    {
        kellinwood.logging.LoggerInterface v0_1 = ((kellinwood.logging.LoggerInterface) kellinwood.logging.LoggerManager.loggers.get(p2));
        if (v0_1 == null) {
            v0_1 = kellinwood.logging.LoggerManager.factory.getLogger(p2);
            kellinwood.logging.LoggerManager.loggers.put(p2, v0_1);
        }
        return v0_1;
    }

    public static void setLoggerFactory(kellinwood.logging.LoggerFactory p0)
    {
        kellinwood.logging.LoggerManager.factory = p0;
        return;
    }
}
