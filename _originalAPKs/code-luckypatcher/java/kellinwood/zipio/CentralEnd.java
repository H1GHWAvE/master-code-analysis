package kellinwood.zipio;
public class CentralEnd {
    private static kellinwood.logging.LoggerInterface log;
    public int centralDirectorySize;
    public short centralStartDisk;
    public int centralStartOffset;
    public String fileComment;
    public short numCentralEntries;
    public short numberThisDisk;
    public int signature;
    public short totalCentralEntries;

    public CentralEnd()
    {
        this.signature = 101010256;
        this.numberThisDisk = 0;
        this.centralStartDisk = 0;
        return;
    }

    private void doRead(kellinwood.zipio.ZipInput p9)
    {
        boolean v0 = kellinwood.zipio.CentralEnd.getLogger().isDebugEnabled();
        this.numberThisDisk = p9.readShort();
        if (v0) {
            String v4_0 = new Object[1];
            v4_0[0] = Short.valueOf(this.numberThisDisk);
            kellinwood.zipio.CentralEnd.log.debug(String.format("This disk number: 0x%04x", v4_0));
        }
        this.centralStartDisk = p9.readShort();
        if (v0) {
            String v4_1 = new Object[1];
            v4_1[0] = Short.valueOf(this.centralStartDisk);
            kellinwood.zipio.CentralEnd.log.debug(String.format("Central dir start disk number: 0x%04x", v4_1));
        }
        this.numCentralEntries = p9.readShort();
        if (v0) {
            String v4_2 = new Object[1];
            v4_2[0] = Short.valueOf(this.numCentralEntries);
            kellinwood.zipio.CentralEnd.log.debug(String.format("Central entries on this disk: 0x%04x", v4_2));
        }
        this.totalCentralEntries = p9.readShort();
        if (v0) {
            String v4_3 = new Object[1];
            v4_3[0] = Short.valueOf(this.totalCentralEntries);
            kellinwood.zipio.CentralEnd.log.debug(String.format("Total number of central entries: 0x%04x", v4_3));
        }
        this.centralDirectorySize = p9.readInt();
        if (v0) {
            String v4_4 = new Object[1];
            v4_4[0] = Integer.valueOf(this.centralDirectorySize);
            kellinwood.zipio.CentralEnd.log.debug(String.format("Central directory size: 0x%08x", v4_4));
        }
        this.centralStartOffset = p9.readInt();
        if (v0) {
            String v4_5 = new Object[1];
            v4_5[0] = Integer.valueOf(this.centralStartOffset);
            kellinwood.zipio.CentralEnd.log.debug(String.format("Central directory offset: 0x%08x", v4_5));
        }
        this.fileComment = p9.readString(p9.readShort());
        if (v0) {
            kellinwood.zipio.CentralEnd.log.debug(new StringBuilder().append(".ZIP file comment: ").append(this.fileComment).toString());
        }
        return;
    }

    public static kellinwood.logging.LoggerInterface getLogger()
    {
        if (kellinwood.zipio.CentralEnd.log == null) {
            kellinwood.zipio.CentralEnd.log = kellinwood.logging.LoggerManager.getLogger(kellinwood.zipio.CentralEnd.getName());
        }
        return kellinwood.zipio.CentralEnd.log;
    }

    public static kellinwood.zipio.CentralEnd read(kellinwood.zipio.ZipInput p6)
    {
        void v0_2;
        if (p6.readInt() == 101010256) {
            v0_2 = new kellinwood.zipio.CentralEnd().doRead(p6);
        } else {
            p6.seek((p6.getFilePointer() - 4));
            v0_2 = 0;
        }
        return v0_2;
    }

    public void write(kellinwood.zipio.ZipOutput p3)
    {
        kellinwood.zipio.CentralEnd.getLogger().isDebugEnabled();
        p3.writeInt(this.signature);
        p3.writeShort(this.numberThisDisk);
        p3.writeShort(this.centralStartDisk);
        p3.writeShort(this.numCentralEntries);
        p3.writeShort(this.totalCentralEntries);
        p3.writeInt(this.centralDirectorySize);
        p3.writeInt(this.centralStartOffset);
        p3.writeShort(((short) this.fileComment.length()));
        p3.writeString(this.fileComment);
        return;
    }
}
