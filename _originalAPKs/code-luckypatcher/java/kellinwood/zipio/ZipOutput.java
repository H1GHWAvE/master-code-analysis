package kellinwood.zipio;
public class ZipOutput {
    static kellinwood.logging.LoggerInterface log;
    java.util.List entriesWritten;
    int filePointer;
    java.util.Set namesWritten;
    java.io.OutputStream out;
    String outputFilename;

    public ZipOutput(java.io.File p3)
    {
        this.out = 0;
        this.filePointer = 0;
        this.entriesWritten = new java.util.LinkedList();
        this.namesWritten = new java.util.HashSet();
        this.outputFilename = p3.getAbsolutePath();
        this.init(p3);
        return;
    }

    public ZipOutput(java.io.OutputStream p2)
    {
        this.out = 0;
        this.filePointer = 0;
        this.entriesWritten = new java.util.LinkedList();
        this.namesWritten = new java.util.HashSet();
        this.out = p2;
        return;
    }

    public ZipOutput(String p3)
    {
        this.out = 0;
        this.filePointer = 0;
        this.entriesWritten = new java.util.LinkedList();
        this.namesWritten = new java.util.HashSet();
        this.outputFilename = p3;
        this.init(new java.io.File(this.outputFilename));
        return;
    }

    private static kellinwood.logging.LoggerInterface getLogger()
    {
        if (kellinwood.zipio.ZipOutput.log == null) {
            kellinwood.zipio.ZipOutput.log = kellinwood.logging.LoggerManager.getLogger(kellinwood.zipio.ZipOutput.getName());
        }
        return kellinwood.zipio.ZipOutput.log;
    }

    private void init(java.io.File p2)
    {
        if (p2.exists()) {
            p2.delete();
        }
        this.out = new java.io.FileOutputStream(p2);
        if (kellinwood.zipio.ZipOutput.getLogger().isDebugEnabled()) {
            kellinwood.zipio.ZipListingHelper.listHeader(kellinwood.zipio.ZipOutput.getLogger());
        }
        return;
    }

    public void close()
    {
        kellinwood.zipio.CentralEnd v0_1 = new kellinwood.zipio.CentralEnd();
        v0_1.centralStartOffset = this.getFilePointer();
        Throwable v3_3 = ((short) this.entriesWritten.size());
        v0_1.totalCentralEntries = v3_3;
        v0_1.numCentralEntries = v3_3;
        java.util.Iterator v2 = this.entriesWritten.iterator();
        while (v2.hasNext()) {
            ((kellinwood.zipio.ZioEntry) v2.next()).write(this);
        }
        v0_1.centralDirectorySize = (this.getFilePointer() - v0_1.centralStartOffset);
        v0_1.fileComment = "";
        v0_1.write(this);
        if (this.out != null) {
            try {
                this.out.close();
            } catch (Throwable v3) {
            }
        }
        return;
    }

    public int getFilePointer()
    {
        return this.filePointer;
    }

    public void write(kellinwood.zipio.ZioEntry p5)
    {
        String v0 = p5.getName();
        if (!this.namesWritten.contains(v0)) {
            p5.writeLocalEntry(this);
            this.entriesWritten.add(p5);
            this.namesWritten.add(v0);
            if (kellinwood.zipio.ZipOutput.getLogger().isDebugEnabled()) {
                kellinwood.zipio.ZipListingHelper.listEntry(kellinwood.zipio.ZipOutput.getLogger(), p5);
            }
        } else {
            kellinwood.zipio.ZipOutput.getLogger().warning(new StringBuilder().append("Skipping duplicate file in output: ").append(v0).toString());
        }
        return;
    }

    public void writeBytes(byte[] p3)
    {
        this.out.write(p3);
        this.filePointer = (this.filePointer + p3.length);
        return;
    }

    public void writeBytes(byte[] p2, int p3, int p4)
    {
        this.out.write(p2, p3, p4);
        this.filePointer = (this.filePointer + p4);
        return;
    }

    public void writeInt(int p5)
    {
        byte[] v0 = new byte[4];
        int v1 = 0;
        while (v1 < 4) {
            v0[v1] = ((byte) (p5 & 255));
            p5 >>= 8;
            v1++;
        }
        this.out.write(v0);
        this.filePointer = (this.filePointer + 4);
        return;
    }

    public void writeShort(short p5)
    {
        byte[] v0 = new byte[2];
        int v1 = 0;
        while (v1 < 2) {
            v0[v1] = ((byte) (p5 & 255));
            p5 = ((short) (p5 >> 8));
            v1++;
        }
        this.out.write(v0);
        this.filePointer = (this.filePointer + 2);
        return;
    }

    public void writeString(String p4)
    {
        byte[] v0 = p4.getBytes();
        this.out.write(v0);
        this.filePointer = (this.filePointer + v0.length);
        return;
    }
}
