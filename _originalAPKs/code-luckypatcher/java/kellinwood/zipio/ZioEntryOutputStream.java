package kellinwood.zipio;
public class ZioEntryOutputStream extends java.io.OutputStream {
    java.util.zip.CRC32 crc;
    int crcValue;
    java.io.OutputStream downstream;
    int size;
    java.io.OutputStream wrapped;

    public ZioEntryOutputStream(int p5, java.io.OutputStream p6)
    {
        this.size = 0;
        this.crc = new java.util.zip.CRC32();
        this.crcValue = 0;
        this.wrapped = p6;
        if (p5 == 0) {
            this.downstream = p6;
        } else {
            this.downstream = new java.util.zip.DeflaterOutputStream(p6, new java.util.zip.Deflater(9, 1));
        }
        return;
    }

    public void close()
    {
        this.downstream.flush();
        this.downstream.close();
        this.crcValue = ((int) this.crc.getValue());
        return;
    }

    public void flush()
    {
        this.downstream.flush();
        return;
    }

    public int getCRC()
    {
        return this.crcValue;
    }

    public int getSize()
    {
        return this.size;
    }

    public java.io.OutputStream getWrappedStream()
    {
        return this.wrapped;
    }

    public void write(int p2)
    {
        this.downstream.write(p2);
        this.crc.update(p2);
        this.size = (this.size + 1);
        return;
    }

    public void write(byte[] p3)
    {
        this.downstream.write(p3);
        this.crc.update(p3);
        this.size = (this.size + p3.length);
        return;
    }

    public void write(byte[] p2, int p3, int p4)
    {
        this.downstream.write(p2, p3, p4);
        this.crc.update(p2, p3, p4);
        this.size = (this.size + p4);
        return;
    }
}
