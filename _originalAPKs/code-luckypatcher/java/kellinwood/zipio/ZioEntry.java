package kellinwood.zipio;
public class ZioEntry implements java.lang.Cloneable {
    private static byte[] alignBytes;
    private static kellinwood.logging.LoggerInterface log;
    private int compressedSize;
    private short compression;
    private int crc32;
    private byte[] data;
    private long dataPosition;
    private short diskNumberStart;
    private kellinwood.zipio.ZioEntryOutputStream entryOut;
    private int externalAttributes;
    private byte[] extraData;
    private String fileComment;
    private String filename;
    private short generalPurposeBits;
    private short internalAttributes;
    private int localHeaderOffset;
    private short modificationDate;
    private short modificationTime;
    private short numAlignBytes;
    private int size;
    private short versionMadeBy;
    private short versionRequired;
    private kellinwood.zipio.ZipInput zipInput;

    static ZioEntry()
    {
        byte[] v0_1 = new byte[4];
        kellinwood.zipio.ZioEntry.alignBytes = v0_1;
        return;
    }

    public ZioEntry(String p5)
    {
        this.numAlignBytes = 0;
        this.dataPosition = -1;
        this.data = 0;
        this.entryOut = 0;
        this.filename = p5;
        this.fileComment = "";
        this.compression = 8;
        long v0_3 = new byte[0];
        this.extraData = v0_3;
        this.setTime(System.currentTimeMillis());
        return;
    }

    public ZioEntry(String p13, String p14)
    {
        this.numAlignBytes = 0;
        this.dataPosition = -1;
        this.data = 0;
        this.entryOut = 0;
        this.zipInput = new kellinwood.zipio.ZipInput(p14);
        this.filename = p13;
        this.fileComment = "";
        this.compression = 0;
        this.size = ((int) this.zipInput.getFileLength());
        this.compressedSize = this.size;
        if (kellinwood.zipio.ZioEntry.getLogger().isDebugEnabled()) {
            long v4_10 = kellinwood.zipio.ZioEntry.getLogger();
            int v6_2 = new Object[2];
            v6_2[0] = p14;
            v6_2[1] = Integer.valueOf(this.size);
            v4_10.debug(String.format("Computing CRC for %s, size=%d", v6_2));
        }
        java.util.zip.CRC32 v2_1 = new java.util.zip.CRC32();
        byte[] v0 = new byte[8096];
        int v3 = 0;
        while (v3 != this.size) {
            int v1 = this.zipInput.read(v0, 0, Math.min(v0.length, (this.size - v3)));
            if (v1 > 0) {
                v2_1.update(v0, 0, v1);
                v3 += v1;
            }
        }
        this.crc32 = ((int) v2_1.getValue());
        this.zipInput.seek(0);
        this.dataPosition = 0;
        long v4_16 = new byte[0];
        this.extraData = v4_16;
        this.setTime(new java.io.File(p14).lastModified());
        return;
    }

    public ZioEntry(String p5, String p6, short p7, int p8, int p9, int p10)
    {
        this.numAlignBytes = 0;
        this.dataPosition = -1;
        this.data = 0;
        this.entryOut = 0;
        this.zipInput = new kellinwood.zipio.ZipInput(p6);
        this.filename = p5;
        this.fileComment = "";
        this.compression = p7;
        this.crc32 = p8;
        this.compressedSize = p9;
        this.size = p10;
        this.dataPosition = 0;
        long v0_5 = new byte[0];
        this.extraData = v0_5;
        this.setTime(new java.io.File(p6).lastModified());
        return;
    }

    public ZioEntry(kellinwood.zipio.ZipInput p4)
    {
        this.numAlignBytes = 0;
        this.dataPosition = -1;
        this.data = 0;
        this.entryOut = 0;
        this.zipInput = p4;
        return;
    }

    private void doRead(kellinwood.zipio.ZipInput p11)
    {
        boolean v0 = kellinwood.zipio.ZioEntry.getLogger().isDebugEnabled();
        this.versionMadeBy = p11.readShort();
        if (v0) {
            String v6_0 = new Object[1];
            v6_0[0] = Short.valueOf(this.versionMadeBy);
            kellinwood.zipio.ZioEntry.log.debug(String.format("Version made by: 0x%04x", v6_0));
        }
        this.versionRequired = p11.readShort();
        if (v0) {
            String v6_1 = new Object[1];
            v6_1[0] = Short.valueOf(this.versionRequired);
            kellinwood.zipio.ZioEntry.log.debug(String.format("Version required: 0x%04x", v6_1));
        }
        this.generalPurposeBits = p11.readShort();
        if (v0) {
            String v6_2 = new Object[1];
            v6_2[0] = Short.valueOf(this.generalPurposeBits);
            kellinwood.zipio.ZioEntry.log.debug(String.format("General purpose bits: 0x%04x", v6_2));
        }
        if ((this.generalPurposeBits & 63473) == 0) {
            this.compression = p11.readShort();
            if (v0) {
                String v6_3 = new Object[1];
                v6_3[0] = Short.valueOf(this.compression);
                kellinwood.zipio.ZioEntry.log.debug(String.format("Compression: 0x%04x", v6_3));
            }
            this.modificationTime = p11.readShort();
            if (v0) {
                String v6_4 = new Object[1];
                v6_4[0] = Short.valueOf(this.modificationTime);
                kellinwood.zipio.ZioEntry.log.debug(String.format("Modification time: 0x%04x", v6_4));
            }
            this.modificationDate = p11.readShort();
            if (v0) {
                String v6_5 = new Object[1];
                v6_5[0] = Short.valueOf(this.modificationDate);
                kellinwood.zipio.ZioEntry.log.debug(String.format("Modification date: 0x%04x", v6_5));
            }
            this.crc32 = p11.readInt();
            if (v0) {
                String v6_6 = new Object[1];
                v6_6[0] = Integer.valueOf(this.crc32);
                kellinwood.zipio.ZioEntry.log.debug(String.format("CRC-32: 0x%04x", v6_6));
            }
            this.compressedSize = p11.readInt();
            if (v0) {
                String v6_7 = new Object[1];
                v6_7[0] = Integer.valueOf(this.compressedSize);
                kellinwood.zipio.ZioEntry.log.debug(String.format("Compressed size: 0x%04x", v6_7));
            }
            this.size = p11.readInt();
            if (v0) {
                String v6_8 = new Object[1];
                v6_8[0] = Integer.valueOf(this.size);
                kellinwood.zipio.ZioEntry.log.debug(String.format("Size: 0x%04x", v6_8));
            }
            short v3 = p11.readShort();
            if (v0) {
                String v6_9 = new Object[1];
                v6_9[0] = Short.valueOf(v3);
                kellinwood.zipio.ZioEntry.log.debug(String.format("File name length: 0x%04x", v6_9));
            }
            short v1 = p11.readShort();
            if (v0) {
                String v6_10 = new Object[1];
                v6_10[0] = Short.valueOf(v1);
                kellinwood.zipio.ZioEntry.log.debug(String.format("Extra length: 0x%04x", v6_10));
            }
            short v2 = p11.readShort();
            if (v0) {
                String v6_11 = new Object[1];
                v6_11[0] = Short.valueOf(v2);
                kellinwood.zipio.ZioEntry.log.debug(String.format("File comment length: 0x%04x", v6_11));
            }
            this.diskNumberStart = p11.readShort();
            if (v0) {
                String v6_12 = new Object[1];
                v6_12[0] = Short.valueOf(this.diskNumberStart);
                kellinwood.zipio.ZioEntry.log.debug(String.format("Disk number start: 0x%04x", v6_12));
            }
            this.internalAttributes = p11.readShort();
            if (v0) {
                String v6_13 = new Object[1];
                v6_13[0] = Short.valueOf(this.internalAttributes);
                kellinwood.zipio.ZioEntry.log.debug(String.format("Internal attributes: 0x%04x", v6_13));
            }
            this.externalAttributes = p11.readInt();
            if (v0) {
                String v6_14 = new Object[1];
                v6_14[0] = Integer.valueOf(this.externalAttributes);
                kellinwood.zipio.ZioEntry.log.debug(String.format("External attributes: 0x%08x", v6_14));
            }
            this.localHeaderOffset = p11.readInt();
            if (v0) {
                String v6_15 = new Object[1];
                v6_15[0] = Integer.valueOf(this.localHeaderOffset);
                kellinwood.zipio.ZioEntry.log.debug(String.format("Local header offset: 0x%08x", v6_15));
            }
            this.filename = p11.readString(v3);
            if (v0) {
                kellinwood.zipio.ZioEntry.log.debug(new StringBuilder().append("Filename: ").append(this.filename).toString());
            }
            this.extraData = p11.readBytes(v1);
            this.fileComment = p11.readString(v2);
            if (v0) {
                kellinwood.zipio.ZioEntry.log.debug(new StringBuilder().append("File comment: ").append(this.fileComment).toString());
            }
            this.generalPurposeBits = ((short) (this.generalPurposeBits & 2048));
            if (this.size == 0) {
                this.compressedSize = 0;
                this.compression = 0;
                this.crc32 = 0;
            }
            return;
        } else {
            String v5_45 = new StringBuilder().append("Can\'t handle general purpose bits == ");
            Integer v7_29 = new Object[1];
            v7_29[0] = Short.valueOf(this.generalPurposeBits);
            throw new IllegalStateException(v5_45.append(String.format("0x%04x", v7_29)).toString());
        }
    }

    public static kellinwood.logging.LoggerInterface getLogger()
    {
        if (kellinwood.zipio.ZioEntry.log == null) {
            kellinwood.zipio.ZioEntry.log = kellinwood.logging.LoggerManager.getLogger(kellinwood.zipio.ZioEntry.getName());
        }
        return kellinwood.zipio.ZioEntry.log;
    }

    public static kellinwood.zipio.ZioEntry read(kellinwood.zipio.ZipInput p6)
    {
        void v0_2;
        if (p6.readInt() == 33639248) {
            v0_2 = new kellinwood.zipio.ZioEntry(p6).doRead(p6);
        } else {
            p6.seek((p6.getFilePointer() - 4));
            v0_2 = 0;
        }
        return v0_2;
    }

    public kellinwood.zipio.ZioEntry getClonedEntry(String p5)
    {
        try {
            kellinwood.zipio.ZioEntry v0_1 = ((kellinwood.zipio.ZioEntry) this.clone());
            v0_1.setName(p5);
            return v0_1;
        } catch (CloneNotSupportedException v1) {
            throw new IllegalStateException("clone() failed!");
        }
    }

    public int getCompressedSize()
    {
        return this.compressedSize;
    }

    public short getCompression()
    {
        return this.compression;
    }

    public int getCrc32()
    {
        return this.crc32;
    }

    public byte[] getData()
    {
        byte[] v3;
        if (this.data == null) {
            v3 = new byte[this.size];
            java.io.InputStream v1 = this.getInputStream();
            int v0 = 0;
            while (v0 != this.size) {
                int v2 = v1.read(v3, v0, (this.size - v0));
                if (v2 >= 0) {
                    v0 += v2;
                } else {
                    Object[] v6_1 = new Object[2];
                    v6_1[0] = Integer.valueOf(this.size);
                    v6_1[1] = Integer.valueOf(v0);
                    throw new IllegalStateException(String.format("Read failed, expecting %d bytes, got %d instead", v6_1));
                }
            }
        } else {
            v3 = this.data;
        }
        return v3;
    }

    public long getDataPosition()
    {
        return this.dataPosition;
    }

    public short getDiskNumberStart()
    {
        return this.diskNumberStart;
    }

    public kellinwood.zipio.ZioEntryOutputStream getEntryOut()
    {
        return this.entryOut;
    }

    public int getExternalAttributes()
    {
        return this.externalAttributes;
    }

    public byte[] getExtraData()
    {
        return this.extraData;
    }

    public String getFileComment()
    {
        return this.fileComment;
    }

    public short getGeneralPurposeBits()
    {
        return this.generalPurposeBits;
    }

    public java.io.InputStream getInputStream()
    {
        return this.getInputStream(0);
    }

    public java.io.InputStream getInputStream(java.io.OutputStream p7)
    {
        java.util.zip.InflaterInputStream v1_0;
        if (this.entryOut == null) {
            java.util.zip.InflaterInputStream v0_1 = new kellinwood.zipio.ZioEntryInputStream(this);
            if (p7 != null) {
                v0_1.setMonitorStream(p7);
            }
            if (this.compression == 0) {
                v1_0 = v0_1;
            } else {
                v0_1.setReturnDummyByte(1);
                v1_0 = new java.util.zip.InflaterInputStream(v0_1, new java.util.zip.Inflater(1));
            }
        } else {
            this.entryOut.close();
            this.size = this.entryOut.getSize();
            this.data = ((java.io.ByteArrayOutputStream) this.entryOut.getWrappedStream()).toByteArray();
            this.compressedSize = this.data.length;
            this.crc32 = this.entryOut.getCRC();
            this.entryOut = 0;
            v1_0 = new java.io.ByteArrayInputStream(this.data);
            if (this.compression != 0) {
                byte[] v4 = new byte[1];
                v1_0 = new java.util.zip.InflaterInputStream(new java.io.SequenceInputStream(v1_0, new java.io.ByteArrayInputStream(v4)), new java.util.zip.Inflater(1));
            }
        }
        return v1_0;
    }

    public short getInternalAttributes()
    {
        return this.internalAttributes;
    }

    public int getLocalHeaderOffset()
    {
        return this.localHeaderOffset;
    }

    public String getName()
    {
        return this.filename;
    }

    public java.io.OutputStream getOutputStream()
    {
        this.entryOut = new kellinwood.zipio.ZioEntryOutputStream(this.compression, new java.io.ByteArrayOutputStream());
        return this.entryOut;
    }

    public int getSize()
    {
        return this.size;
    }

    public long getTime()
    {
        return new java.util.Date((((this.modificationDate >> 9) & 127) + 80), (((this.modificationDate >> 5) & 15) - 1), (this.modificationDate & 31), ((this.modificationTime >> 11) & 31), ((this.modificationTime >> 5) & 63), ((this.modificationTime << 1) & 62)).getTime();
    }

    public short getVersionMadeBy()
    {
        return this.versionMadeBy;
    }

    public short getVersionRequired()
    {
        return this.versionRequired;
    }

    public kellinwood.zipio.ZipInput getZipInput()
    {
        return this.zipInput;
    }

    public boolean isDirectory()
    {
        return this.filename.endsWith("/");
    }

    public void readLocalHeader()
    {
        kellinwood.zipio.ZipInput v5 = this.zipInput;
        boolean v0 = kellinwood.zipio.ZioEntry.getLogger().isDebugEnabled();
        v5.seek(((long) this.localHeaderOffset));
        if (v0) {
            kellinwood.logging.LoggerInterface v9_3 = kellinwood.zipio.ZioEntry.getLogger();
            Object[] v11_1 = new Object[1];
            v11_1[0] = Long.valueOf(v5.getFilePointer());
            v9_3.debug(String.format("FILE POSITION: 0x%08x", v11_1));
        }
        if (v5.readInt() == 67324752) {
            short v8_0 = v5.readShort();
            if (v0) {
                Object[] v11_3 = new Object[1];
                v11_3[0] = Short.valueOf(v8_0);
                kellinwood.zipio.ZioEntry.log.debug(String.format("Version required: 0x%04x", v11_3));
            }
            short v8_1 = v5.readShort();
            if (v0) {
                Object[] v11_5 = new Object[1];
                v11_5[0] = Short.valueOf(v8_1);
                kellinwood.zipio.ZioEntry.log.debug(String.format("General purpose bits: 0x%04x", v11_5));
            }
            short v8_2 = v5.readShort();
            if (v0) {
                Object[] v11_7 = new Object[1];
                v11_7[0] = Short.valueOf(v8_2);
                kellinwood.zipio.ZioEntry.log.debug(String.format("Compression: 0x%04x", v11_7));
            }
            short v8_3 = v5.readShort();
            if (v0) {
                Object[] v11_9 = new Object[1];
                v11_9[0] = Short.valueOf(v8_3);
                kellinwood.zipio.ZioEntry.log.debug(String.format("Modification time: 0x%04x", v11_9));
            }
            short v8_4 = v5.readShort();
            if (v0) {
                Object[] v11_11 = new Object[1];
                v11_11[0] = Short.valueOf(v8_4);
                kellinwood.zipio.ZioEntry.log.debug(String.format("Modification date: 0x%04x", v11_11));
            }
            int v7_0 = v5.readInt();
            if (v0) {
                Object[] v11_13 = new Object[1];
                v11_13[0] = Integer.valueOf(v7_0);
                kellinwood.zipio.ZioEntry.log.debug(String.format("CRC-32: 0x%04x", v11_13));
            }
            int v7_1 = v5.readInt();
            if (v0) {
                Object[] v11_15 = new Object[1];
                v11_15[0] = Integer.valueOf(v7_1);
                kellinwood.zipio.ZioEntry.log.debug(String.format("Compressed size: 0x%04x", v11_15));
            }
            int v7_2 = v5.readInt();
            if (v0) {
                Object[] v11_17 = new Object[1];
                v11_17[0] = Integer.valueOf(v7_2);
                kellinwood.zipio.ZioEntry.log.debug(String.format("Size: 0x%04x", v11_17));
            }
            short v3 = v5.readShort();
            if (v0) {
                Object[] v11_19 = new Object[1];
                v11_19[0] = Short.valueOf(v3);
                kellinwood.zipio.ZioEntry.log.debug(String.format("File name length: 0x%04x", v11_19));
            }
            short v2 = v5.readShort();
            if (v0) {
                Object[] v11_21 = new Object[1];
                v11_21[0] = Short.valueOf(v2);
                kellinwood.zipio.ZioEntry.log.debug(String.format("Extra length: 0x%04x", v11_21));
            }
            String v4 = v5.readString(v3);
            if (v0) {
                kellinwood.zipio.ZioEntry.log.debug(new StringBuilder().append("Filename: ").append(v4).toString());
            }
            v5.readBytes(v2);
            this.dataPosition = v5.getFilePointer();
            if (v0) {
                Object[] v11_24 = new Object[1];
                v11_24[0] = Long.valueOf(this.dataPosition);
                kellinwood.zipio.ZioEntry.log.debug(String.format("Data position: 0x%08x", v11_24));
            }
            return;
        } else {
            Object[] v11_26 = new Object[2];
            v11_26[0] = Long.valueOf(v5.getFilePointer());
            v11_26[1] = this.filename;
            throw new IllegalStateException(String.format("Local header not found at pos=0x%08x, file=%s", v11_26));
        }
    }

    public void setCompression(int p2)
    {
        this.compression = ((short) p2);
        return;
    }

    public void setName(String p1)
    {
        this.filename = p1;
        return;
    }

    public void setTime(long p7)
    {
        long v1;
        java.util.Date v0_1 = new java.util.Date(p7);
        int v3 = (v0_1.getYear() + 1900);
        if (v3 >= 1980) {
            v1 = ((long) (((((((v3 + -1980) << 25) | ((v0_1.getMonth() + 1) << 21)) | (v0_1.getDate() << 16)) | (v0_1.getHours() << 11)) | (v0_1.getMinutes() << 5)) | (v0_1.getSeconds() >> 1)));
        } else {
            v1 = 2162688;
        }
        this.modificationDate = ((short) ((int) (v1 >> 16)));
        this.modificationTime = ((short) ((int) (65535 & v1)));
        return;
    }

    public void write(kellinwood.zipio.ZipOutput p5)
    {
        kellinwood.zipio.ZioEntry.getLogger().isDebugEnabled();
        p5.writeInt(33639248);
        p5.writeShort(this.versionMadeBy);
        p5.writeShort(this.versionRequired);
        p5.writeShort(this.generalPurposeBits);
        p5.writeShort(this.compression);
        p5.writeShort(this.modificationTime);
        p5.writeShort(this.modificationDate);
        p5.writeInt(this.crc32);
        p5.writeInt(this.compressedSize);
        p5.writeInt(this.size);
        p5.writeShort(((short) this.filename.length()));
        p5.writeShort(((short) (this.extraData.length + this.numAlignBytes)));
        p5.writeShort(((short) this.fileComment.length()));
        p5.writeShort(this.diskNumberStart);
        p5.writeShort(this.internalAttributes);
        p5.writeInt(this.externalAttributes);
        p5.writeInt(this.localHeaderOffset);
        p5.writeString(this.filename);
        p5.writeBytes(this.extraData);
        if (this.numAlignBytes > 0) {
            p5.writeBytes(kellinwood.zipio.ZioEntry.alignBytes, 0, this.numAlignBytes);
        }
        p5.writeString(this.fileComment);
        return;
    }

    public void writeLocalEntry(kellinwood.zipio.ZipOutput p17)
    {
        if ((this.data == null) && ((this.dataPosition < 0) && (this.zipInput != null))) {
            this.readLocalHeader();
        }
        this.localHeaderOffset = p17.getFilePointer();
        boolean v6 = kellinwood.zipio.ZioEntry.getLogger().isDebugEnabled();
        if (v6) {
            long v10_6 = kellinwood.zipio.ZioEntry.getLogger();
            Object[] v12_2 = new Object[2];
            v12_2[0] = Integer.valueOf(this.localHeaderOffset);
            v12_2[1] = this.filename;
            v10_6.debug(String.format("Writing local header at 0x%08x - %s", v12_2));
        }
        if (this.entryOut != null) {
            this.entryOut.close();
            this.size = this.entryOut.getSize();
            this.data = ((java.io.ByteArrayOutputStream) this.entryOut.getWrappedStream()).toByteArray();
            this.compressedSize = this.data.length;
            this.crc32 = this.entryOut.getCRC();
        }
        p17.writeInt(67324752);
        p17.writeShort(this.versionRequired);
        p17.writeShort(this.generalPurposeBits);
        p17.writeShort(this.compression);
        p17.writeShort(this.modificationTime);
        p17.writeShort(this.modificationDate);
        p17.writeInt(this.crc32);
        p17.writeInt(this.compressedSize);
        p17.writeInt(this.size);
        p17.writeShort(((short) this.filename.length()));
        this.numAlignBytes = 0;
        if (this.compression == 0) {
            short v5 = ((short) ((int) (((long) (((p17.getFilePointer() + 2) + this.filename.length()) + this.extraData.length)) % 4)));
            if (v5 > 0) {
                this.numAlignBytes = ((short) (4 - v5));
            }
        }
        p17.writeShort(((short) (this.extraData.length + this.numAlignBytes)));
        p17.writeString(this.filename);
        p17.writeBytes(this.extraData);
        if (this.numAlignBytes > 0) {
            p17.writeBytes(kellinwood.zipio.ZioEntry.alignBytes, 0, this.numAlignBytes);
        }
        if (v6) {
            long v10_50 = kellinwood.zipio.ZioEntry.getLogger();
            Object[] v12_5 = new Object[1];
            v12_5[0] = Integer.valueOf(p17.getFilePointer());
            v10_50.debug(String.format("Data position 0x%08x", v12_5));
        }
        if (this.data == null) {
            if (v6) {
                long v10_52 = kellinwood.zipio.ZioEntry.getLogger();
                Object[] v12_7 = new Object[1];
                v12_7[0] = Long.valueOf(this.dataPosition);
                v10_52.debug(String.format("Seeking to position 0x%08x", v12_7));
            }
            this.zipInput.seek(this.dataPosition);
            int v2 = Math.min(this.compressedSize, 8096);
            byte[] v1 = new byte[v2];
            long v8 = 0;
            while (v8 != ((long) this.compressedSize)) {
                int v7 = this.zipInput.in.read(v1, 0, ((int) Math.min((((long) this.compressedSize) - v8), ((long) v2))));
                if (v7 <= 0) {
                    Object[] v12_14 = new Object[2];
                    v12_14[0] = this.filename;
                    v12_14[1] = Long.valueOf((((long) this.compressedSize) - v8));
                    throw new IllegalStateException(String.format("EOF reached while copying %s with %d bytes left to go", v12_14));
                } else {
                    p17.writeBytes(v1, 0, v7);
                    if (v6) {
                        long v10_63 = kellinwood.zipio.ZioEntry.getLogger();
                        Object[] v12_16 = new Object[1];
                        v12_16[0] = Integer.valueOf(v7);
                        v10_63.debug(String.format("Wrote %d bytes", v12_16));
                    }
                    v8 += ((long) v7);
                }
            }
        } else {
            p17.writeBytes(this.data);
            if (v6) {
                long v10_66 = kellinwood.zipio.ZioEntry.getLogger();
                Object[] v12_18 = new Object[1];
                v12_18[0] = Integer.valueOf(this.data.length);
                v10_66.debug(String.format("Wrote %d bytes", v12_18));
            }
        }
        return;
    }
}
