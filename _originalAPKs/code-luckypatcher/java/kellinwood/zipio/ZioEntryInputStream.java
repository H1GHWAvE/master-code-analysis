package kellinwood.zipio;
public class ZioEntryInputStream extends java.io.InputStream {
    boolean debug;
    kellinwood.logging.LoggerInterface log;
    java.io.OutputStream monitor;
    int offset;
    java.io.RandomAccessFile raf;
    boolean returnDummyByte;
    int size;

    public ZioEntryInputStream(kellinwood.zipio.ZioEntry p9)
    {
        this.returnDummyByte = 0;
        this.monitor = 0;
        this.log = kellinwood.logging.LoggerManager.getLogger(this.getClass().getName());
        this.debug = this.log.isDebugEnabled();
        this.offset = 0;
        this.size = p9.getCompressedSize();
        this.raf = p9.getZipInput().in;
        if (p9.getDataPosition() < 0) {
            p9.readLocalHeader();
        } else {
            if (this.debug) {
                java.io.RandomAccessFile v2_12 = this.log;
                Object[] v4_1 = new Object[1];
                v4_1[0] = Long.valueOf(p9.getDataPosition());
                v2_12.debug(String.format("Seeking to %d", v4_1));
            }
            this.raf.seek(p9.getDataPosition());
        }
        return;
    }

    private int readBytes(byte[] p9, int p10, int p11)
    {
        int v0;
        if ((this.size - this.offset) != 0) {
            v0 = this.raf.read(p9, p10, Math.min(p11, this.available()));
            if (v0 > 0) {
                if (this.monitor != null) {
                    this.monitor.write(p9, p10, v0);
                }
                this.offset = (this.offset + v0);
            }
            if (this.debug) {
                kellinwood.logging.LoggerInterface v3_9 = this.log;
                Object[] v5_1 = new Object[3];
                v5_1[0] = Integer.valueOf(v0);
                v5_1[1] = Integer.valueOf(p10);
                v5_1[2] = Integer.valueOf(p11);
                v3_9.debug(String.format("Read %d bytes for read(b,%d,%d)", v5_1));
            }
        } else {
            if (!this.returnDummyByte) {
                v0 = -1;
            } else {
                this.returnDummyByte = 0;
                p9[p10] = 0;
                v0 = 1;
            }
        }
        return v0;
    }

    public int available()
    {
        int v0 = (this.size - this.offset);
        if (this.debug) {
            boolean v2_2 = this.log;
            Object[] v4 = new Object[1];
            v4[0] = Integer.valueOf(v0);
            v2_2.debug(String.format("Available = %d", v4));
        }
        if ((v0 == 0) && (this.returnDummyByte)) {
            v0 = 1;
        }
        return v0;
    }

    public void close()
    {
        return;
    }

    public boolean markSupported()
    {
        return 0;
    }

    public int read()
    {
        int v0 = 0;
        if ((this.size - this.offset) != 0) {
            v0 = this.raf.read();
            if (v0 < 0) {
                if (this.debug) {
                    this.log.debug("Read 0 bytes");
                }
            } else {
                if (this.monitor != null) {
                    this.monitor.write(v0);
                }
                if (this.debug) {
                    this.log.debug("Read 1 byte");
                }
                this.offset = (this.offset + 1);
            }
        } else {
            if (!this.returnDummyByte) {
                v0 = -1;
            } else {
                this.returnDummyByte = 0;
            }
        }
        return v0;
    }

    public int read(byte[] p3)
    {
        return this.readBytes(p3, 0, p3.length);
    }

    public int read(byte[] p2, int p3, int p4)
    {
        return this.readBytes(p2, p3, p4);
    }

    public void setMonitorStream(java.io.OutputStream p1)
    {
        this.monitor = p1;
        return;
    }

    public void setReturnDummyByte(boolean p1)
    {
        this.returnDummyByte = p1;
        return;
    }

    public long skip(long p8)
    {
        long v0 = Math.min(p8, ((long) this.available()));
        this.raf.seek((this.raf.getFilePointer() + v0));
        if (this.debug) {
            kellinwood.logging.LoggerInterface v2_4 = this.log;
            Object[] v4_1 = new Object[1];
            v4_1[0] = Long.valueOf(v0);
            v2_4.debug(String.format("Skipped %d bytes", v4_1));
        }
        return v0;
    }
}
