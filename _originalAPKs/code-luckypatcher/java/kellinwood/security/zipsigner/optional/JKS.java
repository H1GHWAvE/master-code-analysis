package kellinwood.security.zipsigner.optional;
public class JKS extends java.security.KeyStoreSpi {
    private static final int MAGIC = 4277010157;
    private static final int PRIVATE_KEY = 1;
    private static final int TRUSTED_CERT = 2;
    private final java.util.Vector aliases;
    private final java.util.HashMap certChains;
    private final java.util.HashMap dates;
    private final java.util.HashMap privateKeys;
    private final java.util.HashMap trustedCerts;

    public JKS()
    {
        this.aliases = new java.util.Vector();
        this.trustedCerts = new java.util.HashMap();
        this.privateKeys = new java.util.HashMap();
        this.certChains = new java.util.HashMap();
        this.dates = new java.util.HashMap();
        return;
    }

    private static byte[] charsToBytes(char[] p5)
    {
        byte[] v0 = new byte[(p5.length * 2)];
        int v1 = 0;
        int v2 = 0;
        while (v1 < p5.length) {
            int v3 = (v2 + 1);
            v0[v2] = ((byte) (p5[v1] >> 8));
            v2 = (v3 + 1);
            v0[v3] = ((byte) p5[v1]);
            v1++;
        }
        return v0;
    }

    private static byte[] decryptKey(byte[] p12, byte[] p13)
    {
        try {
            byte[] v2 = new javax.crypto.EncryptedPrivateKeyInfo(p12).getEncryptedData();
            byte[] v6 = new byte[20];
            System.arraycopy(v2, 0, v6, 0, 20);
            byte[] v0 = new byte[20];
            System.arraycopy(v2, (v2.length - 20), v0, 0, 20);
            byte[] v5 = new byte[(v2.length - 40)];
            java.security.MessageDigest v7 = java.security.MessageDigest.getInstance("SHA1");
            int v1 = 0;
        } catch (Exception v8) {
            throw new java.security.UnrecoverableKeyException(v8.getMessage());
        }
        while (v1 < v5.length) {
            v7.reset();
            v7.update(p13);
            v7.update(v6);
            v7.digest(v6, 0, v6.length);
            int v4 = 0;
            while ((v4 < v6.length) && (v1 < v5.length)) {
                v5[v1] = ((byte) (v6[v4] ^ v2[(v1 + 20)]));
                v1++;
                v4++;
            }
        }
        v7.reset();
        v7.update(p13);
        v7.update(v5);
        if (java.security.MessageDigest.isEqual(v0, v7.digest())) {
            return v5;
        } else {
            throw new java.security.UnrecoverableKeyException("checksum mismatch");
        }
    }

    private static byte[] encryptKey(java.security.Key p11, byte[] p12)
    {
        try {
            java.security.MessageDigest v6 = java.security.MessageDigest.getInstance("SHA1");
            java.security.SecureRandom.getInstance("SHA1PRNG");
            byte[] v3 = p11.getEncoded();
            byte[] v1 = new byte[(v3.length + 40)];
            byte[] v4 = java.security.SecureRandom.getSeed(20);
            System.arraycopy(v4, 0, v1, 0, 20);
            int v0 = 0;
        } catch (Exception v7) {
            throw new java.security.KeyStoreException(v7.getMessage());
        }
        while (v0 < v3.length) {
            v6.reset();
            v6.update(p12);
            v6.update(v4);
            v6.digest(v4, 0, v4.length);
            int v2 = 0;
            while ((v2 < v4.length) && (v0 < v3.length)) {
                v1[(v0 + 20)] = ((byte) (v4[v2] ^ v3[v0]));
                v0++;
                v2++;
            }
        }
        v6.reset();
        v6.update(p12);
        v6.update(v3);
        v6.digest(v1, (v1.length - 20), 20);
        return new javax.crypto.EncryptedPrivateKeyInfo("1.3.6.1.4.1.42.2.17.1.1", v1).getEncoded();
    }

    private static java.security.cert.Certificate readCert(java.io.DataInputStream p5)
    {
        String v3 = p5.readUTF();
        byte[] v0 = new byte[p5.readInt()];
        p5.read(v0);
        return java.security.cert.CertificateFactory.getInstance(v3).generateCertificate(new java.io.ByteArrayInputStream(v0));
    }

    private static void writeCert(java.io.DataOutputStream p2, java.security.cert.Certificate p3)
    {
        p2.writeUTF(p3.getType());
        byte[] v0 = p3.getEncoded();
        p2.writeInt(v0.length);
        p2.write(v0);
        return;
    }

    public java.util.Enumeration engineAliases()
    {
        return this.aliases.elements();
    }

    public boolean engineContainsAlias(String p2)
    {
        return this.aliases.contains(p2.toLowerCase());
    }

    public void engineDeleteEntry(String p2)
    {
        this.aliases.remove(p2.toLowerCase());
        return;
    }

    public java.security.cert.Certificate engineGetCertificate(String p3)
    {
        java.security.cert.Certificate v1_7;
        String v3_1 = p3.toLowerCase();
        if (!this.engineIsKeyEntry(v3_1)) {
            v1_7 = ((java.security.cert.Certificate) this.trustedCerts.get(v3_1));
        } else {
            java.security.cert.Certificate[] v0_1 = ((java.security.cert.Certificate[]) ((java.security.cert.Certificate[]) this.certChains.get(v3_1)));
            if ((v0_1 == null) || (v0_1.length <= 0)) {
            } else {
                v1_7 = v0_1[0];
            }
        }
        return v1_7;
    }

    public String engineGetCertificateAlias(java.security.cert.Certificate p4)
    {
        java.util.Iterator v1 = this.trustedCerts.keySet().iterator();
        while (v1.hasNext()) {
            int v0_0 = ((String) v1.next());
            if (p4.equals(this.trustedCerts.get(v0_0))) {
            }
            return v0_0;
        }
        v0_0 = 0;
        return v0_0;
    }

    public java.security.cert.Certificate[] engineGetCertificateChain(String p2)
    {
        return ((java.security.cert.Certificate[]) ((java.security.cert.Certificate[]) this.certChains.get(p2.toLowerCase())));
    }

    public java.util.Date engineGetCreationDate(String p2)
    {
        return ((java.util.Date) this.dates.get(p2.toLowerCase()));
    }

    public java.security.Key engineGetKey(String p7, char[] p8)
    {
        java.security.UnrecoverableKeyException v4_8;
        String v7_1 = p7.toLowerCase();
        if (this.privateKeys.containsKey(v7_1)) {
            byte[] v2 = kellinwood.security.zipsigner.optional.JKS.decryptKey(((byte[]) ((byte[]) this.privateKeys.get(v7_1))), kellinwood.security.zipsigner.optional.JKS.charsToBytes(p8));
            java.security.cert.Certificate[] v0 = this.engineGetCertificateChain(v7_1);
            if (v0.length <= 0) {
                v4_8 = new javax.crypto.spec.SecretKeySpec(v2, v7_1);
            } else {
                try {
                    v4_8 = java.security.KeyFactory.getInstance(v0[0].getPublicKey().getAlgorithm()).generatePrivate(new java.security.spec.PKCS8EncodedKeySpec(v2));
                } catch (java.security.spec.InvalidKeySpecException v3) {
                    throw new java.security.UnrecoverableKeyException(v3.getMessage());
                }
            }
        } else {
            v4_8 = 0;
        }
        return v4_8;
    }

    public boolean engineIsCertificateEntry(String p2)
    {
        return this.trustedCerts.containsKey(p2.toLowerCase());
    }

    public boolean engineIsKeyEntry(String p2)
    {
        return this.privateKeys.containsKey(p2.toLowerCase());
    }

    public void engineLoad(java.io.InputStream p18, char[] p19)
    {
        java.security.MessageDigest v10 = java.security.MessageDigest.getInstance("SHA");
        if (p19 != null) {
            v10.update(kellinwood.security.zipsigner.optional.JKS.charsToBytes(p19));
        }
        v10.update("Mighty Aphrodite".getBytes("UTF-8"));
        this.aliases.clear();
        this.trustedCerts.clear();
        this.privateKeys.clear();
        this.certChains.clear();
        this.dates.clear();
        if (p18 != null) {
            java.io.DataInputStream v4_1 = new java.io.DataInputStream(new java.security.DigestInputStream(p18, v10));
            if (v4_1.readInt() == -17957139) {
                v4_1.readInt();
                int v11 = v4_1.readInt();
                this.aliases.ensureCapacity(v11);
                if (v11 >= 0) {
                    int v7 = 0;
                    while (v7 < v11) {
                        int v12 = v4_1.readInt();
                        String v1 = v4_1.readUTF();
                        this.aliases.add(v1);
                        this.dates.put(v1, new java.util.Date(v4_1.readLong()));
                        switch (v12) {
                            case 1:
                                byte[] v5 = new byte[v4_1.readInt()];
                                v4_1.read(v5);
                                this.privateKeys.put(v1, v5);
                                int v3 = v4_1.readInt();
                                java.security.cert.Certificate[] v2 = new java.security.cert.Certificate[v3];
                                int v8 = 0;
                                while (v8 < v3) {
                                    v2[v8] = kellinwood.security.zipsigner.optional.JKS.readCert(v4_1);
                                    v8++;
                                }
                                this.certChains.put(v1, v2);
                                break;
                            case 2:
                                this.trustedCerts.put(v1, kellinwood.security.zipsigner.optional.JKS.readCert(v4_1));
                                break;
                            default:
                                throw new java.io.IOException("malformed key store");
                        }
                        v7++;
                    }
                    byte[] v6 = new byte[20];
                    v4_1.read(v6);
                    if ((p19 != null) && (java.security.MessageDigest.isEqual(v6, v10.digest()))) {
                        throw new java.io.IOException("signature not verified");
                    }
                } else {
                    throw new java.io.IOException("negative entry count");
                }
            } else {
                throw new java.io.IOException("not a JavaKeyStore");
            }
        }
        return;
    }

    public void engineSetCertificateEntry(String p4, java.security.cert.Certificate p5)
    {
        String v4_1 = p4.toLowerCase();
        if (!this.privateKeys.containsKey(v4_1)) {
            if (p5 != null) {
                this.trustedCerts.put(v4_1, p5);
                if (!this.aliases.contains(v4_1)) {
                    this.dates.put(v4_1, new java.util.Date());
                    this.aliases.add(v4_1);
                }
                return;
            } else {
                throw new NullPointerException();
            }
        } else {
            throw new java.security.KeyStoreException(new StringBuilder().append("\"").append(v4_1).append("\" is a private key entry").toString());
        }
    }

    public void engineSetKeyEntry(String p4, java.security.Key p5, char[] p6, java.security.cert.Certificate[] p7)
    {
        String v4_1 = p4.toLowerCase();
        if (!this.trustedCerts.containsKey(v4_1)) {
            this.privateKeys.put(v4_1, kellinwood.security.zipsigner.optional.JKS.encryptKey(p5, kellinwood.security.zipsigner.optional.JKS.charsToBytes(p6)));
            if (p7 == null) {
                java.util.Date v1_3 = new java.security.cert.Certificate[0];
                this.certChains.put(v4_1, v1_3);
            } else {
                this.certChains.put(v4_1, p7);
            }
            if (!this.aliases.contains(v4_1)) {
                this.dates.put(v4_1, new java.util.Date());
                this.aliases.add(v4_1);
            }
            return;
        } else {
            throw new java.security.KeyStoreException(new StringBuilder().append("\"").append(v4_1).append(" is a trusted certificate entry").toString());
        }
    }

    public void engineSetKeyEntry(String p5, byte[] p6, java.security.cert.Certificate[] p7)
    {
        String v5_1 = p5.toLowerCase();
        if (!this.trustedCerts.containsKey(v5_1)) {
            try {
                new javax.crypto.EncryptedPrivateKeyInfo(p6);
                this.privateKeys.put(v5_1, p6);
            } catch (java.io.IOException v0) {
                throw new java.security.KeyStoreException("encoded key is not an EncryptedPrivateKeyInfo");
            }
            if (p7 == null) {
                java.util.Date v2_2 = new java.security.cert.Certificate[0];
                this.certChains.put(v5_1, v2_2);
            } else {
                this.certChains.put(v5_1, p7);
            }
            if (!this.aliases.contains(v5_1)) {
                this.dates.put(v5_1, new java.util.Date());
                this.aliases.add(v5_1);
            }
            return;
        } else {
            throw new java.security.KeyStoreException(new StringBuilder().append("\"").append(v5_1).append("\" is a trusted certificate entry").toString());
        }
    }

    public int engineSize()
    {
        return this.aliases.size();
    }

    public void engineStore(java.io.OutputStream p12, char[] p13)
    {
        java.security.MessageDigest v7 = java.security.MessageDigest.getInstance("SHA1");
        v7.update(kellinwood.security.zipsigner.optional.JKS.charsToBytes(p13));
        v7.update("Mighty Aphrodite".getBytes("UTF-8"));
        java.io.DataOutputStream v3_1 = new java.io.DataOutputStream(new java.security.DigestOutputStream(p12, v7));
        v3_1.writeInt(-17957139);
        v3_1.writeInt(2);
        v3_1.writeInt(this.aliases.size());
        java.util.Enumeration v4 = this.aliases.elements();
        while (v4.hasMoreElements()) {
            String v0_1 = ((String) v4.nextElement());
            if (!this.trustedCerts.containsKey(v0_1)) {
                v3_1.writeInt(1);
                v3_1.writeUTF(v0_1);
                v3_1.writeLong(((java.util.Date) this.dates.get(v0_1)).getTime());
                byte[] v6_1 = ((byte[]) ((byte[]) this.privateKeys.get(v0_1)));
                v3_1.writeInt(v6_1.length);
                v3_1.write(v6_1);
                java.security.cert.Certificate[] v1_1 = ((java.security.cert.Certificate[]) ((java.security.cert.Certificate[]) this.certChains.get(v0_1)));
                v3_1.writeInt(v1_1.length);
                int v5 = 0;
                while (v5 < v1_1.length) {
                    kellinwood.security.zipsigner.optional.JKS.writeCert(v3_1, v1_1[v5]);
                    v5++;
                }
            } else {
                v3_1.writeInt(2);
                v3_1.writeUTF(v0_1);
                v3_1.writeLong(((java.util.Date) this.dates.get(v0_1)).getTime());
                kellinwood.security.zipsigner.optional.JKS.writeCert(v3_1, ((java.security.cert.Certificate) this.trustedCerts.get(v0_1)));
            }
        }
        v3_1.write(v7.digest());
        return;
    }
}
