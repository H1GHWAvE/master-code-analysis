package kellinwood.security.zipsigner.optional;
public class DistinguishedNameValues extends java.util.LinkedHashMap {

    public DistinguishedNameValues()
    {
        this.put(org.spongycastle.asn1.x500.style.BCStyle.C, 0);
        this.put(org.spongycastle.asn1.x500.style.BCStyle.ST, 0);
        this.put(org.spongycastle.asn1.x500.style.BCStyle.L, 0);
        this.put(org.spongycastle.asn1.x500.style.BCStyle.STREET, 0);
        this.put(org.spongycastle.asn1.x500.style.BCStyle.O, 0);
        this.put(org.spongycastle.asn1.x500.style.BCStyle.OU, 0);
        this.put(org.spongycastle.asn1.x500.style.BCStyle.CN, 0);
        return;
    }

    public org.spongycastle.jce.X509Principal getPrincipal()
    {
        java.util.Vector v2_1 = new java.util.Vector();
        java.util.Vector v3_1 = new java.util.Vector();
        java.util.Iterator v1 = this.entrySet().iterator();
        while (v1.hasNext()) {
            java.util.Map$Entry v0_1 = ((java.util.Map$Entry) v1.next());
            if ((v0_1.getValue() != null) && (!((String) v0_1.getValue()).equals(""))) {
                v2_1.add(v0_1.getKey());
                v3_1.add(v0_1.getValue());
            }
        }
        return new org.spongycastle.jce.X509Principal(v2_1, v3_1);
    }

    public bridge synthetic Object put(Object p2, Object p3)
    {
        return this.put(((org.spongycastle.asn1.ASN1ObjectIdentifier) p2), ((String) p3));
    }

    public String put(org.spongycastle.asn1.ASN1ObjectIdentifier p2, String p3)
    {
        if ((p3 != 0) && (p3.equals(""))) {
            p3 = 0;
        }
        if (!this.containsKey(p2)) {
            super.put(p2, p3);
        } else {
            super.put(p2, p3);
        }
        return p3;
    }

    public void setCommonName(String p2)
    {
        this.put(org.spongycastle.asn1.x500.style.BCStyle.CN, p2);
        return;
    }

    public void setCountry(String p2)
    {
        this.put(org.spongycastle.asn1.x500.style.BCStyle.C, p2);
        return;
    }

    public void setLocality(String p2)
    {
        this.put(org.spongycastle.asn1.x500.style.BCStyle.L, p2);
        return;
    }

    public void setOrganization(String p2)
    {
        this.put(org.spongycastle.asn1.x500.style.BCStyle.O, p2);
        return;
    }

    public void setOrganizationalUnit(String p2)
    {
        this.put(org.spongycastle.asn1.x500.style.BCStyle.OU, p2);
        return;
    }

    public void setState(String p2)
    {
        this.put(org.spongycastle.asn1.x500.style.BCStyle.ST, p2);
        return;
    }

    public void setStreet(String p2)
    {
        this.put(org.spongycastle.asn1.x500.style.BCStyle.STREET, p2);
        return;
    }

    public int size()
    {
        int v1 = 0;
        java.util.Iterator v0 = this.values().iterator();
        while (v0.hasNext()) {
            if (((String) v0.next()) != null) {
                v1++;
            }
        }
        return v1;
    }
}
