package kellinwood.security.zipsigner.optional;
public class PasswordObfuscator {
    private static kellinwood.security.zipsigner.optional.PasswordObfuscator instance = None;
    static final String x = "harold-and-maude";
    kellinwood.logging.LoggerInterface logger;
    javax.crypto.spec.SecretKeySpec skeySpec;

    static PasswordObfuscator()
    {
        kellinwood.security.zipsigner.optional.PasswordObfuscator.instance = 0;
        return;
    }

    private PasswordObfuscator()
    {
        this.logger = kellinwood.logging.LoggerManager.getLogger(kellinwood.security.zipsigner.optional.PasswordObfuscator.getName());
        this.skeySpec = new javax.crypto.spec.SecretKeySpec("harold-and-maude".getBytes(), "AES");
        return;
    }

    public static void flush(byte[] p2)
    {
        if (p2 != null) {
            int v0 = 0;
            while (v0 < p2.length) {
                p2[v0] = 0;
                v0++;
            }
        }
        return;
    }

    public static void flush(char[] p2)
    {
        if (p2 != null) {
            int v0 = 0;
            while (v0 < p2.length) {
                p2[v0] = 0;
                v0++;
            }
        }
        return;
    }

    public static kellinwood.security.zipsigner.optional.PasswordObfuscator getInstance()
    {
        if (kellinwood.security.zipsigner.optional.PasswordObfuscator.instance == null) {
            kellinwood.security.zipsigner.optional.PasswordObfuscator.instance = new kellinwood.security.zipsigner.optional.PasswordObfuscator();
        }
        return kellinwood.security.zipsigner.optional.PasswordObfuscator.instance;
    }

    public char[] decode(String p14, String p15)
    {
        char[] v8;
        if (p15 != null) {
            try {
                javax.crypto.Cipher v2 = javax.crypto.Cipher.getInstance("AES/ECB/PKCS5Padding");
                v2.init(2, new javax.crypto.spec.SecretKeySpec("harold-and-maude".getBytes(), "AES"));
                char[] v1 = new char[128];
                int v5 = 0;
            } catch (Exception v10) {
                this.logger.error("Failed to decode password", v10);
                v8 = 0;
            }
            while(true) {
                int v6 = new java.io.BufferedReader(new java.io.InputStreamReader(new java.io.ByteArrayInputStream(v2.doFinal(kellinwood.security.zipsigner.Base64.decode(p15.getBytes()))))).read(v1, v5, (128 - v5));
                if (v6 == -1) {
                    break;
                }
                v5 += v6;
            }
            if (v5 > p14.length()) {
                v8 = new char[(v5 - p14.length())];
                int v4 = 0;
                int v3 = p14.length();
                while (v3 < v5) {
                    v8[v4] = v1[v3];
                    v4++;
                    v3++;
                }
                kellinwood.security.zipsigner.optional.PasswordObfuscator.flush(v1);
            } else {
                v8 = 0;
            }
        } else {
            v8 = 0;
        }
        return v8;
    }

    public char[] decodeAliasPassword(String p2, String p3, String p4)
    {
        return this.decode(new StringBuilder().append(p2).append(p3).toString(), p4);
    }

    public char[] decodeKeystorePassword(String p2, String p3)
    {
        return this.decode(p2, p3);
    }

    public String encode(String p3, String p4)
    {
        String v1;
        if (p4 != null) {
            char[] v0 = p4.toCharArray();
            v1 = this.encode(p3, v0);
            kellinwood.security.zipsigner.optional.PasswordObfuscator.flush(v0);
        } else {
            v1 = 0;
        }
        return v1;
    }

    public String encode(String p9, char[] p10)
    {
        String v5 = 0;
        if (p10 != null) {
            try {
                javax.crypto.Cipher v1 = javax.crypto.Cipher.getInstance("AES/ECB/PKCS5Padding");
                v1.init(1, this.skeySpec);
                java.io.ByteArrayOutputStream v0_1 = new java.io.ByteArrayOutputStream();
                java.io.OutputStreamWriter v3_1 = new java.io.OutputStreamWriter(v0_1);
                v3_1.write(p9);
                v3_1.write(p10);
                v3_1.flush();
                v5 = kellinwood.security.zipsigner.Base64.encode(v1.doFinal(v0_1.toByteArray()));
            } catch (Exception v4) {
                this.logger.error("Failed to obfuscate password", v4);
            }
        }
        return v5;
    }

    public String encodeAliasPassword(String p2, String p3, String p4)
    {
        return this.encode(new StringBuilder().append(p2).append(p3).toString(), p4);
    }

    public String encodeAliasPassword(String p2, String p3, char[] p4)
    {
        return this.encode(new StringBuilder().append(p2).append(p3).toString(), p4);
    }

    public String encodeKeystorePassword(String p2, String p3)
    {
        return this.encode(p2, p3);
    }

    public String encodeKeystorePassword(String p2, char[] p3)
    {
        return this.encode(p2, p3);
    }
}
