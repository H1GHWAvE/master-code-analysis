package kellinwood.security.zipsigner.optional;
public class JksKeyStore extends java.security.KeyStore {

    public JksKeyStore()
    {
        this(new kellinwood.security.zipsigner.optional.JKS(), kellinwood.security.zipsigner.optional.KeyStoreFileManager.getProvider(), "jks");
        return;
    }
}
