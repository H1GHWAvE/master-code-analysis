package kellinwood.security.zipsigner.optional;
public class CertCreator {

    public CertCreator()
    {
        return;
    }

    public static kellinwood.security.zipsigner.KeySet createKey(String p16, int p17, String p18, String p19, int p20, kellinwood.security.zipsigner.optional.DistinguishedNameValues p21)
    {
        try {
            java.security.KeyPairGenerator v3 = java.security.KeyPairGenerator.getInstance(p16);
            v3.initialize(p17);
            java.security.KeyPair v1 = v3.generateKeyPair();
            org.spongycastle.x509.X509V3CertificateGenerator v7_1 = new org.spongycastle.x509.X509V3CertificateGenerator();
            org.spongycastle.jce.X509Principal v5 = p21.getPrincipal();
            java.math.BigInteger v6 = java.math.BigInteger.valueOf(((long) new java.security.SecureRandom().nextInt()));
        } catch (Exception v8) {
            throw new RuntimeException(v8.getMessage(), v8);
        }
        while (v6.compareTo(java.math.BigInteger.ZERO) < 0) {
            v6 = java.math.BigInteger.valueOf(((long) new java.security.SecureRandom().nextInt()));
        }
        v7_1.setSerialNumber(v6);
        v7_1.setIssuerDN(v5);
        v7_1.setNotBefore(new java.util.Date((System.currentTimeMillis() - 1.280618154e-314)));
        v7_1.setNotAfter(new java.util.Date((System.currentTimeMillis() + (1.5623541479e-313 * ((long) p20)))));
        v7_1.setSubjectDN(v5);
        v7_1.setPublicKey(v1.getPublic());
        v7_1.setSignatureAlgorithm(p19);
        java.security.cert.X509Certificate v2 = v7_1.generate(v1.getPrivate(), "BC");
        kellinwood.security.zipsigner.KeySet v4_1 = new kellinwood.security.zipsigner.KeySet();
        v4_1.setName(p18);
        v4_1.setPrivateKey(v1.getPrivate());
        v4_1.setPublicKey(v2);
        return v4_1;
    }

    public static kellinwood.security.zipsigner.KeySet createKey(String p9, char[] p10, String p11, int p12, String p13, char[] p14, String p15, int p16, kellinwood.security.zipsigner.optional.DistinguishedNameValues p17)
    {
        try {
            kellinwood.security.zipsigner.KeySet v6 = kellinwood.security.zipsigner.optional.CertCreator.createKey(p11, p12, p13, p15, p16, p17);
            java.security.KeyStore v7 = kellinwood.security.zipsigner.optional.KeyStoreFileManager.loadKeyStore(p9, p10);
            RuntimeException v0_1 = v6.getPrivateKey();
            String v1_2 = new java.security.cert.Certificate[1];
            v1_2[0] = v6.getPublicKey();
            v7.setKeyEntry(p13, v0_1, p14, v1_2);
            kellinwood.security.zipsigner.optional.KeyStoreFileManager.writeKeyStore(v7, p9, p10);
            return v6;
        } catch (Exception v8_1) {
            throw v8_1;
        } catch (Exception v8_0) {
            throw new RuntimeException(v8_0.getMessage(), v8_0);
        }
    }

    public static kellinwood.security.zipsigner.KeySet createKeystoreAndKey(String p10, char[] p11, String p12, int p13, String p14, char[] p15, String p16, int p17, kellinwood.security.zipsigner.optional.DistinguishedNameValues p18)
    {
        try {
            kellinwood.security.zipsigner.KeySet v6 = kellinwood.security.zipsigner.optional.CertCreator.createKey(p12, p13, p14, p16, p17, p18);
            java.security.KeyStore v7 = kellinwood.security.zipsigner.optional.KeyStoreFileManager.createKeyStore(p10, p11);
            java.io.IOException v0_1 = v6.getPrivateKey();
            String v1_2 = new java.security.cert.Certificate[1];
            v1_2[0] = v6.getPublicKey();
            v7.setKeyEntry(p14, v0_1, p15, v1_2);
        } catch (Exception v9_1) {
            throw v9_1;
        } catch (Exception v9_0) {
            throw new RuntimeException(v9_0.getMessage(), v9_0);
        }
        if (!new java.io.File(p10).exists()) {
            kellinwood.security.zipsigner.optional.KeyStoreFileManager.writeKeyStore(v7, p10, p11);
            return v6;
        } else {
            throw new java.io.IOException(new StringBuilder().append("File already exists: ").append(p10).toString());
        }
    }

    public static void createKeystoreAndKey(String p9, char[] p10, String p11, kellinwood.security.zipsigner.optional.DistinguishedNameValues p12)
    {
        kellinwood.security.zipsigner.optional.CertCreator.createKeystoreAndKey(p9, p10, "RSA", 2048, p11, p10, "SHA1withRSA", 30, p12);
        return;
    }
}
