package kellinwood.security.zipsigner.optional;
public class CustomKeySigner {

    public CustomKeySigner()
    {
        return;
    }

    public static void signZip(kellinwood.security.zipsigner.ZipSigner p11, String p12, char[] p13, String p14, char[] p15, String p16, String p17, String p18)
    {
        p11.issueLoadingCertAndKeysProgressEvent();
        java.security.KeyStore v10 = kellinwood.security.zipsigner.optional.KeyStoreFileManager.loadKeyStore(p12, p13);
        p11.setKeys("custom", ((java.security.cert.X509Certificate) v10.getCertificate(p14)), ((java.security.PrivateKey) v10.getKey(p14, p15)), p16, 0);
        p11.signZip(p17, p18);
        return;
    }
}
