package kellinwood.security.zipsigner;
public class AutoKeyException extends java.lang.RuntimeException {
    private static final long serialVersionUID = 1;

    public AutoKeyException(String p1)
    {
        this(p1);
        return;
    }

    public AutoKeyException(String p1, Throwable p2)
    {
        this(p1, p2);
        return;
    }
}
