package kellinwood.security.zipsigner;
public interface ProgressListener {

    public abstract void onProgress();
}
