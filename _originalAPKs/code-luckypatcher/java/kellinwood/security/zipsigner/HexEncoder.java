package kellinwood.security.zipsigner;
public class HexEncoder {
    protected final byte[] decodingTable;
    protected final byte[] encodingTable;

    public HexEncoder()
    {
        byte[] v0_1 = new byte[16];
        v0_1 = {48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 97, 98, 99, 100, 101, 102};
        this.encodingTable = v0_1;
        byte[] v0_3 = new byte[128];
        this.decodingTable = v0_3;
        this.initialiseDecodingTable();
        return;
    }

    private boolean ignore(char p2)
    {
        if ((p2 != 10) && ((p2 != 13) && ((p2 != 9) && (p2 != 32)))) {
            int v0_4 = 0;
        } else {
            v0_4 = 1;
        }
        return v0_4;
    }

    public int decode(String p9, java.io.OutputStream p10)
    {
        int v5 = 0;
        int v2 = p9.length();
        while ((v2 > 0) && (this.ignore(p9.charAt((v2 - 1))))) {
            v2--;
        }
        int v3_0 = 0;
        while (v3_0 < v2) {
            int v4_0 = v3_0;
            while ((v4_0 < v2) && (this.ignore(p9.charAt(v4_0)))) {
                v4_0++;
            }
            int v4_1 = (v4_0 + 1);
            while ((v4_1 < v2) && (this.ignore(p9.charAt(v4_1)))) {
                v4_1++;
            }
            v3_0 = (v4_1 + 1);
            p10.write(((this.decodingTable[p9.charAt(v4_0)] << 4) | this.decodingTable[p9.charAt(v4_1)]));
            v5++;
        }
        return v5;
    }

    public int decode(byte[] p9, int p10, int p11, java.io.OutputStream p12)
    {
        int v5 = 0;
        int v2 = (p10 + p11);
        while ((v2 > p10) && (this.ignore(((char) p9[(v2 - 1)])))) {
            v2--;
        }
        int v3_0 = p10;
        while (v3_0 < v2) {
            int v4_0 = v3_0;
            while ((v4_0 < v2) && (this.ignore(((char) p9[v4_0])))) {
                v4_0++;
            }
            int v4_1 = (v4_0 + 1);
            while ((v4_1 < v2) && (this.ignore(((char) p9[v4_1])))) {
                v4_1++;
            }
            v3_0 = (v4_1 + 1);
            p12.write(((this.decodingTable[p9[v4_0]] << 4) | this.decodingTable[p9[v4_1]]));
            v5++;
        }
        return v5;
    }

    public int encode(byte[] p5, int p6, int p7, java.io.OutputStream p8)
    {
        int v0 = p6;
        while (v0 < (p6 + p7)) {
            int v1 = (p5[v0] & 255);
            p8.write(this.encodingTable[(v1 >> 4)]);
            p8.write(this.encodingTable[(v1 & 15)]);
            v0++;
        }
        return (p7 * 2);
    }

    protected void initialiseDecodingTable()
    {
        int v0 = 0;
        while (v0 < this.encodingTable.length) {
            this.decodingTable[this.encodingTable[v0]] = ((byte) v0);
            v0++;
        }
        this.decodingTable[65] = this.decodingTable[97];
        this.decodingTable[66] = this.decodingTable[98];
        this.decodingTable[67] = this.decodingTable[99];
        this.decodingTable[68] = this.decodingTable[100];
        this.decodingTable[69] = this.decodingTable[101];
        this.decodingTable[70] = this.decodingTable[102];
        return;
    }
}
