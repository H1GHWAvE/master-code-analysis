package kellinwood.security.zipsigner;
public class DefaultResourceAdapter implements kellinwood.security.zipsigner.ResourceAdapter {

    public DefaultResourceAdapter()
    {
        return;
    }

    public varargs String getString(kellinwood.security.zipsigner.ResourceAdapter$Item p6, Object[] p7)
    {
        String v0_3;
        switch (kellinwood.security.zipsigner.DefaultResourceAdapter$1.$SwitchMap$kellinwood$security$zipsigner$ResourceAdapter$Item[p6.ordinal()]) {
            case 1:
                v0_3 = "Input and output files are the same.  Specify a different name for the output.";
                break;
            case 2:
                v0_3 = new StringBuilder().append("Unable to auto-select key for signing ").append(p7[0]).toString();
                break;
            case 3:
                v0_3 = "Loading certificate and private key";
                break;
            case 4:
                v0_3 = "Parsing the input\'s central directory";
                break;
            case 5:
                v0_3 = "Generating manifest";
                break;
            case 6:
                v0_3 = "Generating signature file";
                break;
            case 7:
                v0_3 = "Generating signature block file";
                break;
            case 8:
                Object[] v1_2 = new Object[2];
                v1_2[0] = p7[0];
                v1_2[1] = p7[1];
                v0_3 = String.format("Copying zip entry %d of %d", v1_2);
                break;
            default:
                throw new IllegalArgumentException(new StringBuilder().append("Unknown item ").append(p6).toString());
        }
        return v0_3;
    }
}
