package kellinwood.security.zipsigner;
public class KeySet {
    String name;
    java.security.PrivateKey privateKey;
    java.security.cert.X509Certificate publicKey;
    byte[] sigBlockTemplate;
    String signatureAlgorithm;

    public KeySet()
    {
        this.publicKey = 0;
        this.privateKey = 0;
        this.sigBlockTemplate = 0;
        this.signatureAlgorithm = "SHA1withRSA";
        return;
    }

    public KeySet(String p2, java.security.cert.X509Certificate p3, java.security.PrivateKey p4, String p5, byte[] p6)
    {
        this.publicKey = 0;
        this.privateKey = 0;
        this.sigBlockTemplate = 0;
        this.signatureAlgorithm = "SHA1withRSA";
        this.name = p2;
        this.publicKey = p3;
        this.privateKey = p4;
        if (p5 != null) {
            this.signatureAlgorithm = p5;
        }
        this.sigBlockTemplate = p6;
        return;
    }

    public KeySet(String p2, java.security.cert.X509Certificate p3, java.security.PrivateKey p4, byte[] p5)
    {
        this.publicKey = 0;
        this.privateKey = 0;
        this.sigBlockTemplate = 0;
        this.signatureAlgorithm = "SHA1withRSA";
        this.name = p2;
        this.publicKey = p3;
        this.privateKey = p4;
        this.sigBlockTemplate = p5;
        return;
    }

    public String getName()
    {
        return this.name;
    }

    public java.security.PrivateKey getPrivateKey()
    {
        return this.privateKey;
    }

    public java.security.cert.X509Certificate getPublicKey()
    {
        return this.publicKey;
    }

    public byte[] getSigBlockTemplate()
    {
        return this.sigBlockTemplate;
    }

    public String getSignatureAlgorithm()
    {
        return this.signatureAlgorithm;
    }

    public void setName(String p1)
    {
        this.name = p1;
        return;
    }

    public void setPrivateKey(java.security.PrivateKey p1)
    {
        this.privateKey = p1;
        return;
    }

    public void setPublicKey(java.security.cert.X509Certificate p1)
    {
        this.publicKey = p1;
        return;
    }

    public void setSigBlockTemplate(byte[] p1)
    {
        this.sigBlockTemplate = p1;
        return;
    }

    public void setSignatureAlgorithm(String p1)
    {
        if (p1 != null) {
            this.signatureAlgorithm = p1;
        }
        return;
    }
}
