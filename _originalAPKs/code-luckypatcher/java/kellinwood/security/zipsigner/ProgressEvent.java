package kellinwood.security.zipsigner;
public class ProgressEvent {
    public static final int PRORITY_IMPORTANT = 1;
    public static final int PRORITY_NORMAL;
    private String message;
    private int percentDone;
    private int priority;

    public ProgressEvent()
    {
        return;
    }

    public String getMessage()
    {
        return this.message;
    }

    public int getPercentDone()
    {
        return this.percentDone;
    }

    public int getPriority()
    {
        return this.priority;
    }

    public void setMessage(String p1)
    {
        this.message = p1;
        return;
    }

    public void setPercentDone(int p1)
    {
        this.percentDone = p1;
        return;
    }

    public void setPriority(int p1)
    {
        this.priority = p1;
        return;
    }
}
