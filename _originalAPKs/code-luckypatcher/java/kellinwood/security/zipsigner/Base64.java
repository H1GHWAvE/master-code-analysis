package kellinwood.security.zipsigner;
public class Base64 {
    static reflect.Method aDecodeMethod;
    static reflect.Method aEncodeMethod;
    static reflect.Method bDecodeMethod;
    static Object bDecoder;
    static reflect.Method bEncodeMethod;
    static Object bEncoder;
    static kellinwood.logging.LoggerInterface logger;

    static Base64()
    {
        kellinwood.security.zipsigner.Base64.aEncodeMethod = 0;
        kellinwood.security.zipsigner.Base64.aDecodeMethod = 0;
        kellinwood.security.zipsigner.Base64.bEncoder = 0;
        kellinwood.security.zipsigner.Base64.bEncodeMethod = 0;
        kellinwood.security.zipsigner.Base64.bDecoder = 0;
        kellinwood.security.zipsigner.Base64.bDecodeMethod = 0;
        kellinwood.security.zipsigner.Base64.logger = 0;
        kellinwood.security.zipsigner.Base64.logger = kellinwood.logging.LoggerManager.getLogger(kellinwood.security.zipsigner.Base64.getName());
        try {
            Class v0_0 = Class.forName("android.util.Base64");
            String v3_1 = new Class[2];
            v3_1[0] = byte[];
            v3_1[1] = Integer.TYPE;
            kellinwood.security.zipsigner.Base64.aEncodeMethod = v0_0.getMethod("encode", v3_1);
            String v3_3 = new Class[2];
            v3_3[0] = byte[];
            v3_3[1] = Integer.TYPE;
            kellinwood.security.zipsigner.Base64.aDecodeMethod = v0_0.getMethod("decode", v3_3);
            kellinwood.security.zipsigner.Base64.logger.info(new StringBuilder().append(v0_0.getName()).append(" is available.").toString());
            try {
                Class v0_1 = Class.forName("org.bouncycastle.util.encoders.Base64Encoder");
                kellinwood.security.zipsigner.Base64.bEncoder = v0_1.newInstance();
                String v3_11 = new Class[4];
                v3_11[0] = byte[];
                v3_11[1] = Integer.TYPE;
                v3_11[2] = Integer.TYPE;
                v3_11[3] = java.io.OutputStream;
                kellinwood.security.zipsigner.Base64.bEncodeMethod = v0_1.getMethod("encode", v3_11);
                kellinwood.security.zipsigner.Base64.logger.info(new StringBuilder().append(v0_1.getName()).append(" is available.").toString());
                String v3_18 = new Class[4];
                v3_18[0] = byte[];
                v3_18[1] = Integer.TYPE;
                v3_18[2] = Integer.TYPE;
                v3_18[3] = java.io.OutputStream;
                kellinwood.security.zipsigner.Base64.bDecodeMethod = v0_1.getMethod("decode", v3_18);
            } catch (IllegalStateException v2) {
            } catch (Exception v1_1) {
                kellinwood.security.zipsigner.Base64.logger.error("Failed to initialize use of org.bouncycastle.util.encoders.Base64Encoder", v1_1);
            }
            if ((kellinwood.security.zipsigner.Base64.aEncodeMethod != null) || (kellinwood.security.zipsigner.Base64.bEncodeMethod != null)) {
                return;
            } else {
                throw new IllegalStateException("No base64 encoder implementation is available.");
            }
        } catch (IllegalStateException v2) {
        } catch (Exception v1_0) {
            kellinwood.security.zipsigner.Base64.logger.error("Failed to initialize use of android.util.Base64", v1_0);
        }
    }

    public Base64()
    {
        return;
    }

    public static byte[] decode(byte[] p9)
    {
        try {
            byte[] v2;
            if (kellinwood.security.zipsigner.Base64.aDecodeMethod == null) {
                if (kellinwood.security.zipsigner.Base64.bDecodeMethod == null) {
                    throw new IllegalStateException("No base64 encoder implementation is available.");
                } else {
                    java.io.ByteArrayOutputStream v1_1 = new java.io.ByteArrayOutputStream();
                    Object[] v6_1 = new Object[4];
                    v6_1[0] = p9;
                    v6_1[1] = Integer.valueOf(0);
                    v6_1[2] = Integer.valueOf(p9.length);
                    v6_1[3] = v1_1;
                    kellinwood.security.zipsigner.Base64.bDecodeMethod.invoke(kellinwood.security.zipsigner.Base64.bEncoder, v6_1);
                    v2 = v1_1.toByteArray();
                }
            } else {
                Object[] v6_3 = new Object[2];
                v6_3[0] = p9;
                v6_3[1] = Integer.valueOf(2);
                v2 = ((byte[]) ((byte[]) kellinwood.security.zipsigner.Base64.aDecodeMethod.invoke(0, v6_3)));
            }
        } catch (Exception v3) {
            throw new IllegalStateException(new StringBuilder().append(v3.getClass().getName()).append(": ").append(v3.getMessage()).toString());
        }
        return v2;
    }

    public static String encode(byte[] p9)
    {
        try {
            IllegalStateException v4_6;
            if (kellinwood.security.zipsigner.Base64.aEncodeMethod == null) {
                if (kellinwood.security.zipsigner.Base64.bEncodeMethod == null) {
                    throw new IllegalStateException("No base64 encoder implementation is available.");
                } else {
                    java.io.ByteArrayOutputStream v1_1 = new java.io.ByteArrayOutputStream();
                    Object[] v6_1 = new Object[4];
                    v6_1[0] = p9;
                    v6_1[1] = Integer.valueOf(0);
                    v6_1[2] = Integer.valueOf(p9.length);
                    v6_1[3] = v1_1;
                    kellinwood.security.zipsigner.Base64.bEncodeMethod.invoke(kellinwood.security.zipsigner.Base64.bEncoder, v6_1);
                    v4_6 = new String(v1_1.toByteArray());
                }
            } else {
                Object[] v6_3 = new Object[2];
                v6_3[0] = p9;
                v6_3[1] = Integer.valueOf(2);
                v4_6 = new String(((byte[]) ((byte[]) kellinwood.security.zipsigner.Base64.aEncodeMethod.invoke(0, v6_3))));
            }
        } catch (Exception v3) {
            throw new IllegalStateException(new StringBuilder().append(v3.getClass().getName()).append(": ").append(v3.getMessage()).toString());
        }
        return v4_6;
    }
}
