package kellinwood.security.zipsigner;
public final enum class ResourceAdapter$Item extends java.lang.Enum {
    private static final synthetic kellinwood.security.zipsigner.ResourceAdapter$Item[] $VALUES;
    public static final enum kellinwood.security.zipsigner.ResourceAdapter$Item AUTO_KEY_SELECTION_ERROR;
    public static final enum kellinwood.security.zipsigner.ResourceAdapter$Item COPYING_ZIP_ENTRY;
    public static final enum kellinwood.security.zipsigner.ResourceAdapter$Item GENERATING_MANIFEST;
    public static final enum kellinwood.security.zipsigner.ResourceAdapter$Item GENERATING_SIGNATURE_BLOCK;
    public static final enum kellinwood.security.zipsigner.ResourceAdapter$Item GENERATING_SIGNATURE_FILE;
    public static final enum kellinwood.security.zipsigner.ResourceAdapter$Item INPUT_SAME_AS_OUTPUT_ERROR;
    public static final enum kellinwood.security.zipsigner.ResourceAdapter$Item LOADING_CERTIFICATE_AND_KEY;
    public static final enum kellinwood.security.zipsigner.ResourceAdapter$Item PARSING_CENTRAL_DIRECTORY;

    static ResourceAdapter$Item()
    {
        kellinwood.security.zipsigner.ResourceAdapter$Item.INPUT_SAME_AS_OUTPUT_ERROR = new kellinwood.security.zipsigner.ResourceAdapter$Item("INPUT_SAME_AS_OUTPUT_ERROR", 0);
        kellinwood.security.zipsigner.ResourceAdapter$Item.AUTO_KEY_SELECTION_ERROR = new kellinwood.security.zipsigner.ResourceAdapter$Item("AUTO_KEY_SELECTION_ERROR", 1);
        kellinwood.security.zipsigner.ResourceAdapter$Item.LOADING_CERTIFICATE_AND_KEY = new kellinwood.security.zipsigner.ResourceAdapter$Item("LOADING_CERTIFICATE_AND_KEY", 2);
        kellinwood.security.zipsigner.ResourceAdapter$Item.PARSING_CENTRAL_DIRECTORY = new kellinwood.security.zipsigner.ResourceAdapter$Item("PARSING_CENTRAL_DIRECTORY", 3);
        kellinwood.security.zipsigner.ResourceAdapter$Item.GENERATING_MANIFEST = new kellinwood.security.zipsigner.ResourceAdapter$Item("GENERATING_MANIFEST", 4);
        kellinwood.security.zipsigner.ResourceAdapter$Item.GENERATING_SIGNATURE_FILE = new kellinwood.security.zipsigner.ResourceAdapter$Item("GENERATING_SIGNATURE_FILE", 5);
        kellinwood.security.zipsigner.ResourceAdapter$Item.GENERATING_SIGNATURE_BLOCK = new kellinwood.security.zipsigner.ResourceAdapter$Item("GENERATING_SIGNATURE_BLOCK", 6);
        kellinwood.security.zipsigner.ResourceAdapter$Item.COPYING_ZIP_ENTRY = new kellinwood.security.zipsigner.ResourceAdapter$Item("COPYING_ZIP_ENTRY", 7);
        kellinwood.security.zipsigner.ResourceAdapter$Item[] v0_17 = new kellinwood.security.zipsigner.ResourceAdapter$Item[8];
        v0_17[0] = kellinwood.security.zipsigner.ResourceAdapter$Item.INPUT_SAME_AS_OUTPUT_ERROR;
        v0_17[1] = kellinwood.security.zipsigner.ResourceAdapter$Item.AUTO_KEY_SELECTION_ERROR;
        v0_17[2] = kellinwood.security.zipsigner.ResourceAdapter$Item.LOADING_CERTIFICATE_AND_KEY;
        v0_17[3] = kellinwood.security.zipsigner.ResourceAdapter$Item.PARSING_CENTRAL_DIRECTORY;
        v0_17[4] = kellinwood.security.zipsigner.ResourceAdapter$Item.GENERATING_MANIFEST;
        v0_17[5] = kellinwood.security.zipsigner.ResourceAdapter$Item.GENERATING_SIGNATURE_FILE;
        v0_17[6] = kellinwood.security.zipsigner.ResourceAdapter$Item.GENERATING_SIGNATURE_BLOCK;
        v0_17[7] = kellinwood.security.zipsigner.ResourceAdapter$Item.COPYING_ZIP_ENTRY;
        kellinwood.security.zipsigner.ResourceAdapter$Item.$VALUES = v0_17;
        return;
    }

    private ResourceAdapter$Item(String p1, int p2)
    {
        this(p1, p2);
        return;
    }

    public static kellinwood.security.zipsigner.ResourceAdapter$Item valueOf(String p1)
    {
        return ((kellinwood.security.zipsigner.ResourceAdapter$Item) Enum.valueOf(kellinwood.security.zipsigner.ResourceAdapter$Item, p1));
    }

    public static kellinwood.security.zipsigner.ResourceAdapter$Item[] values()
    {
        return ((kellinwood.security.zipsigner.ResourceAdapter$Item[]) kellinwood.security.zipsigner.ResourceAdapter$Item.$VALUES.clone());
    }
}
