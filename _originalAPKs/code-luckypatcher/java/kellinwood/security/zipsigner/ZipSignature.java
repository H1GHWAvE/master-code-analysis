package kellinwood.security.zipsigner;
public class ZipSignature {
    byte[] afterAlgorithmIdBytes;
    byte[] algorithmIdBytes;
    byte[] beforeAlgorithmIdBytes;
    javax.crypto.Cipher cipher;
    java.security.MessageDigest md;

    public ZipSignature()
    {
        javax.crypto.Cipher v0_0 = new byte[2];
        v0_0 = {48, 33};
        this.beforeAlgorithmIdBytes = v0_0;
        javax.crypto.Cipher v0_2 = new byte[11];
        v0_2 = {48, 9, 6, 5, 43, 14, 3, 2, 26, 5, 0};
        this.algorithmIdBytes = v0_2;
        javax.crypto.Cipher v0_3 = new byte[2];
        v0_3 = {4, 20};
        this.afterAlgorithmIdBytes = v0_3;
        this.md = java.security.MessageDigest.getInstance("SHA1");
        this.cipher = javax.crypto.Cipher.getInstance("RSA/ECB/PKCS1Padding");
        return;
    }

    public void initSign(java.security.PrivateKey p3)
    {
        this.cipher.init(1, p3);
        return;
    }

    public byte[] sign()
    {
        this.cipher.update(this.beforeAlgorithmIdBytes);
        this.cipher.update(this.algorithmIdBytes);
        this.cipher.update(this.afterAlgorithmIdBytes);
        this.cipher.update(this.md.digest());
        return this.cipher.doFinal();
    }

    public void update(byte[] p2)
    {
        this.md.update(p2);
        return;
    }

    public void update(byte[] p2, int p3, int p4)
    {
        this.md.update(p2, p3, p4);
        return;
    }
}
