package net.lingala.zip4j.model;
public class DataDescriptor {
    private int compressedSize;
    private String crc32;
    private int uncompressedSize;

    public DataDescriptor()
    {
        return;
    }

    public int getCompressedSize()
    {
        return this.compressedSize;
    }

    public String getCrc32()
    {
        return this.crc32;
    }

    public int getUncompressedSize()
    {
        return this.uncompressedSize;
    }

    public void setCompressedSize(int p1)
    {
        this.compressedSize = p1;
        return;
    }

    public void setCrc32(String p1)
    {
        this.crc32 = p1;
        return;
    }

    public void setUncompressedSize(int p1)
    {
        this.uncompressedSize = p1;
        return;
    }
}
