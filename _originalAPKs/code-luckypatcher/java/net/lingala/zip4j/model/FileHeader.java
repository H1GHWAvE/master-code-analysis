package net.lingala.zip4j.model;
public class FileHeader {
    private net.lingala.zip4j.model.AESExtraDataRecord aesExtraDataRecord;
    private long compressedSize;
    private int compressionMethod;
    private long crc32;
    private byte[] crcBuff;
    private boolean dataDescriptorExists;
    private int diskNumberStart;
    private int encryptionMethod;
    private byte[] externalFileAttr;
    private java.util.ArrayList extraDataRecords;
    private int extraFieldLength;
    private String fileComment;
    private int fileCommentLength;
    private String fileName;
    private int fileNameLength;
    private boolean fileNameUTF8Encoded;
    private byte[] generalPurposeFlag;
    private byte[] internalFileAttr;
    private boolean isDirectory;
    private boolean isEncrypted;
    private int lastModFileTime;
    private long offsetLocalHeader;
    private char[] password;
    private int signature;
    private long uncompressedSize;
    private int versionMadeBy;
    private int versionNeededToExtract;
    private net.lingala.zip4j.model.Zip64ExtendedInfo zip64ExtendedInfo;

    public FileHeader()
    {
        this.encryptionMethod = -1;
        this.crc32 = 0;
        this.uncompressedSize = 0;
        return;
    }

    public void extractFile(net.lingala.zip4j.model.ZipModel p8, String p9, net.lingala.zip4j.model.UnzipParameters p10, String p11, net.lingala.zip4j.progress.ProgressMonitor p12, boolean p13)
    {
        if (p8 != null) {
            if (net.lingala.zip4j.util.Zip4jUtil.checkOutputFolder(p9)) {
                if (this != null) {
                    new net.lingala.zip4j.unzip.Unzip(p8).extractFile(this, p9, p10, p11, p12, p13);
                    return;
                } else {
                    throw new net.lingala.zip4j.exception.ZipException("invalid file header");
                }
            } else {
                throw new net.lingala.zip4j.exception.ZipException("Invalid output path");
            }
        } else {
            throw new net.lingala.zip4j.exception.ZipException("input zipModel is null");
        }
    }

    public void extractFile(net.lingala.zip4j.model.ZipModel p8, String p9, net.lingala.zip4j.model.UnzipParameters p10, net.lingala.zip4j.progress.ProgressMonitor p11, boolean p12)
    {
        this.extractFile(p8, p9, p10, 0, p11, p12);
        return;
    }

    public void extractFile(net.lingala.zip4j.model.ZipModel p7, String p8, net.lingala.zip4j.progress.ProgressMonitor p9, boolean p10)
    {
        this.extractFile(p7, p8, 0, p9, p10);
        return;
    }

    public net.lingala.zip4j.model.AESExtraDataRecord getAesExtraDataRecord()
    {
        return this.aesExtraDataRecord;
    }

    public long getCompressedSize()
    {
        return this.compressedSize;
    }

    public int getCompressionMethod()
    {
        return this.compressionMethod;
    }

    public long getCrc32()
    {
        return (this.crc32 & 2.1219957905e-314);
    }

    public byte[] getCrcBuff()
    {
        return this.crcBuff;
    }

    public int getDiskNumberStart()
    {
        return this.diskNumberStart;
    }

    public int getEncryptionMethod()
    {
        return this.encryptionMethod;
    }

    public byte[] getExternalFileAttr()
    {
        return this.externalFileAttr;
    }

    public java.util.ArrayList getExtraDataRecords()
    {
        return this.extraDataRecords;
    }

    public int getExtraFieldLength()
    {
        return this.extraFieldLength;
    }

    public String getFileComment()
    {
        return this.fileComment;
    }

    public int getFileCommentLength()
    {
        return this.fileCommentLength;
    }

    public String getFileName()
    {
        return this.fileName;
    }

    public int getFileNameLength()
    {
        return this.fileNameLength;
    }

    public byte[] getGeneralPurposeFlag()
    {
        return this.generalPurposeFlag;
    }

    public byte[] getInternalFileAttr()
    {
        return this.internalFileAttr;
    }

    public int getLastModFileTime()
    {
        return this.lastModFileTime;
    }

    public long getOffsetLocalHeader()
    {
        return this.offsetLocalHeader;
    }

    public char[] getPassword()
    {
        return this.password;
    }

    public int getSignature()
    {
        return this.signature;
    }

    public long getUncompressedSize()
    {
        return this.uncompressedSize;
    }

    public int getVersionMadeBy()
    {
        return this.versionMadeBy;
    }

    public int getVersionNeededToExtract()
    {
        return this.versionNeededToExtract;
    }

    public net.lingala.zip4j.model.Zip64ExtendedInfo getZip64ExtendedInfo()
    {
        return this.zip64ExtendedInfo;
    }

    public boolean isDataDescriptorExists()
    {
        return this.dataDescriptorExists;
    }

    public boolean isDirectory()
    {
        return this.isDirectory;
    }

    public boolean isEncrypted()
    {
        return this.isEncrypted;
    }

    public boolean isFileNameUTF8Encoded()
    {
        return this.fileNameUTF8Encoded;
    }

    public void setAesExtraDataRecord(net.lingala.zip4j.model.AESExtraDataRecord p1)
    {
        this.aesExtraDataRecord = p1;
        return;
    }

    public void setCompressedSize(long p1)
    {
        this.compressedSize = p1;
        return;
    }

    public void setCompressionMethod(int p1)
    {
        this.compressionMethod = p1;
        return;
    }

    public void setCrc32(long p1)
    {
        this.crc32 = p1;
        return;
    }

    public void setCrcBuff(byte[] p1)
    {
        this.crcBuff = p1;
        return;
    }

    public void setDataDescriptorExists(boolean p1)
    {
        this.dataDescriptorExists = p1;
        return;
    }

    public void setDirectory(boolean p1)
    {
        this.isDirectory = p1;
        return;
    }

    public void setDiskNumberStart(int p1)
    {
        this.diskNumberStart = p1;
        return;
    }

    public void setEncrypted(boolean p1)
    {
        this.isEncrypted = p1;
        return;
    }

    public void setEncryptionMethod(int p1)
    {
        this.encryptionMethod = p1;
        return;
    }

    public void setExternalFileAttr(byte[] p1)
    {
        this.externalFileAttr = p1;
        return;
    }

    public void setExtraDataRecords(java.util.ArrayList p1)
    {
        this.extraDataRecords = p1;
        return;
    }

    public void setExtraFieldLength(int p1)
    {
        this.extraFieldLength = p1;
        return;
    }

    public void setFileComment(String p1)
    {
        this.fileComment = p1;
        return;
    }

    public void setFileCommentLength(int p1)
    {
        this.fileCommentLength = p1;
        return;
    }

    public void setFileName(String p1)
    {
        this.fileName = p1;
        return;
    }

    public void setFileNameLength(int p1)
    {
        this.fileNameLength = p1;
        return;
    }

    public void setFileNameUTF8Encoded(boolean p1)
    {
        this.fileNameUTF8Encoded = p1;
        return;
    }

    public void setGeneralPurposeFlag(byte[] p1)
    {
        this.generalPurposeFlag = p1;
        return;
    }

    public void setInternalFileAttr(byte[] p1)
    {
        this.internalFileAttr = p1;
        return;
    }

    public void setLastModFileTime(int p1)
    {
        this.lastModFileTime = p1;
        return;
    }

    public void setOffsetLocalHeader(long p1)
    {
        this.offsetLocalHeader = p1;
        return;
    }

    public void setPassword(char[] p1)
    {
        this.password = p1;
        return;
    }

    public void setSignature(int p1)
    {
        this.signature = p1;
        return;
    }

    public void setUncompressedSize(long p1)
    {
        this.uncompressedSize = p1;
        return;
    }

    public void setVersionMadeBy(int p1)
    {
        this.versionMadeBy = p1;
        return;
    }

    public void setVersionNeededToExtract(int p1)
    {
        this.versionNeededToExtract = p1;
        return;
    }

    public void setZip64ExtendedInfo(net.lingala.zip4j.model.Zip64ExtendedInfo p1)
    {
        this.zip64ExtendedInfo = p1;
        return;
    }
}
