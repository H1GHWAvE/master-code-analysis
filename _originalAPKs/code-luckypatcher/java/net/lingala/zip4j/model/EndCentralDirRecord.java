package net.lingala.zip4j.model;
public class EndCentralDirRecord {
    private String comment;
    private byte[] commentBytes;
    private int commentLength;
    private int noOfThisDisk;
    private int noOfThisDiskStartOfCentralDir;
    private long offsetOfStartOfCentralDir;
    private long signature;
    private int sizeOfCentralDir;
    private int totNoOfEntriesInCentralDir;
    private int totNoOfEntriesInCentralDirOnThisDisk;

    public EndCentralDirRecord()
    {
        return;
    }

    public String getComment()
    {
        return this.comment;
    }

    public byte[] getCommentBytes()
    {
        return this.commentBytes;
    }

    public int getCommentLength()
    {
        return this.commentLength;
    }

    public int getNoOfThisDisk()
    {
        return this.noOfThisDisk;
    }

    public int getNoOfThisDiskStartOfCentralDir()
    {
        return this.noOfThisDiskStartOfCentralDir;
    }

    public long getOffsetOfStartOfCentralDir()
    {
        return this.offsetOfStartOfCentralDir;
    }

    public long getSignature()
    {
        return this.signature;
    }

    public int getSizeOfCentralDir()
    {
        return this.sizeOfCentralDir;
    }

    public int getTotNoOfEntriesInCentralDir()
    {
        return this.totNoOfEntriesInCentralDir;
    }

    public int getTotNoOfEntriesInCentralDirOnThisDisk()
    {
        return this.totNoOfEntriesInCentralDirOnThisDisk;
    }

    public void setComment(String p1)
    {
        this.comment = p1;
        return;
    }

    public void setCommentBytes(byte[] p1)
    {
        this.commentBytes = p1;
        return;
    }

    public void setCommentLength(int p1)
    {
        this.commentLength = p1;
        return;
    }

    public void setNoOfThisDisk(int p1)
    {
        this.noOfThisDisk = p1;
        return;
    }

    public void setNoOfThisDiskStartOfCentralDir(int p1)
    {
        this.noOfThisDiskStartOfCentralDir = p1;
        return;
    }

    public void setOffsetOfStartOfCentralDir(long p1)
    {
        this.offsetOfStartOfCentralDir = p1;
        return;
    }

    public void setSignature(long p1)
    {
        this.signature = p1;
        return;
    }

    public void setSizeOfCentralDir(int p1)
    {
        this.sizeOfCentralDir = p1;
        return;
    }

    public void setTotNoOfEntriesInCentralDir(int p1)
    {
        this.totNoOfEntriesInCentralDir = p1;
        return;
    }

    public void setTotNoOfEntriesInCentralDirOnThisDisk(int p1)
    {
        this.totNoOfEntriesInCentralDirOnThisDisk = p1;
        return;
    }
}
