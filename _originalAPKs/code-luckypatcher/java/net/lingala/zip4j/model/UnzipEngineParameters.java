package net.lingala.zip4j.model;
public class UnzipEngineParameters {
    private net.lingala.zip4j.model.FileHeader fileHeader;
    private net.lingala.zip4j.crypto.IDecrypter iDecryptor;
    private net.lingala.zip4j.model.LocalFileHeader localFileHeader;
    private java.io.FileOutputStream outputStream;
    private net.lingala.zip4j.unzip.UnzipEngine unzipEngine;
    private net.lingala.zip4j.model.ZipModel zipModel;

    public UnzipEngineParameters()
    {
        return;
    }

    public net.lingala.zip4j.model.FileHeader getFileHeader()
    {
        return this.fileHeader;
    }

    public net.lingala.zip4j.crypto.IDecrypter getIDecryptor()
    {
        return this.iDecryptor;
    }

    public net.lingala.zip4j.model.LocalFileHeader getLocalFileHeader()
    {
        return this.localFileHeader;
    }

    public java.io.FileOutputStream getOutputStream()
    {
        return this.outputStream;
    }

    public net.lingala.zip4j.unzip.UnzipEngine getUnzipEngine()
    {
        return this.unzipEngine;
    }

    public net.lingala.zip4j.model.ZipModel getZipModel()
    {
        return this.zipModel;
    }

    public void setFileHeader(net.lingala.zip4j.model.FileHeader p1)
    {
        this.fileHeader = p1;
        return;
    }

    public void setIDecryptor(net.lingala.zip4j.crypto.IDecrypter p1)
    {
        this.iDecryptor = p1;
        return;
    }

    public void setLocalFileHeader(net.lingala.zip4j.model.LocalFileHeader p1)
    {
        this.localFileHeader = p1;
        return;
    }

    public void setOutputStream(java.io.FileOutputStream p1)
    {
        this.outputStream = p1;
        return;
    }

    public void setUnzipEngine(net.lingala.zip4j.unzip.UnzipEngine p1)
    {
        this.unzipEngine = p1;
        return;
    }

    public void setZipModel(net.lingala.zip4j.model.ZipModel p1)
    {
        this.zipModel = p1;
        return;
    }
}
