package net.lingala.zip4j.model;
public class ZipParameters implements java.lang.Cloneable {
    private int aesKeyStrength;
    private int compressionLevel;
    private int compressionMethod;
    private String defaultFolderPath;
    private boolean encryptFiles;
    private int encryptionMethod;
    private String fileNameInZip;
    private boolean includeRootFolder;
    private boolean isSourceExternalStream;
    private char[] password;
    private boolean readHiddenFiles;
    private String rootFolderInZip;
    private int sourceFileCRC;
    private java.util.TimeZone timeZone;

    public ZipParameters()
    {
        this.compressionMethod = 8;
        this.encryptFiles = 0;
        this.readHiddenFiles = 1;
        this.encryptionMethod = -1;
        this.aesKeyStrength = -1;
        this.includeRootFolder = 1;
        this.timeZone = java.util.TimeZone.getDefault();
        return;
    }

    public Object clone()
    {
        return super.clone();
    }

    public int getAesKeyStrength()
    {
        return this.aesKeyStrength;
    }

    public int getCompressionLevel()
    {
        return this.compressionLevel;
    }

    public int getCompressionMethod()
    {
        return this.compressionMethod;
    }

    public String getDefaultFolderPath()
    {
        return this.defaultFolderPath;
    }

    public int getEncryptionMethod()
    {
        return this.encryptionMethod;
    }

    public String getFileNameInZip()
    {
        return this.fileNameInZip;
    }

    public char[] getPassword()
    {
        return this.password;
    }

    public String getRootFolderInZip()
    {
        return this.rootFolderInZip;
    }

    public int getSourceFileCRC()
    {
        return this.sourceFileCRC;
    }

    public java.util.TimeZone getTimeZone()
    {
        return this.timeZone;
    }

    public boolean isEncryptFiles()
    {
        return this.encryptFiles;
    }

    public boolean isIncludeRootFolder()
    {
        return this.includeRootFolder;
    }

    public boolean isReadHiddenFiles()
    {
        return this.readHiddenFiles;
    }

    public boolean isSourceExternalStream()
    {
        return this.isSourceExternalStream;
    }

    public void setAesKeyStrength(int p1)
    {
        this.aesKeyStrength = p1;
        return;
    }

    public void setCompressionLevel(int p1)
    {
        this.compressionLevel = p1;
        return;
    }

    public void setCompressionMethod(int p1)
    {
        this.compressionMethod = p1;
        return;
    }

    public void setDefaultFolderPath(String p1)
    {
        this.defaultFolderPath = p1;
        return;
    }

    public void setEncryptFiles(boolean p1)
    {
        this.encryptFiles = p1;
        return;
    }

    public void setEncryptionMethod(int p1)
    {
        this.encryptionMethod = p1;
        return;
    }

    public void setFileNameInZip(String p1)
    {
        this.fileNameInZip = p1;
        return;
    }

    public void setIncludeRootFolder(boolean p1)
    {
        this.includeRootFolder = p1;
        return;
    }

    public void setPassword(String p2)
    {
        if (p2 != null) {
            this.setPassword(p2.toCharArray());
        }
        return;
    }

    public void setPassword(char[] p1)
    {
        this.password = p1;
        return;
    }

    public void setReadHiddenFiles(boolean p1)
    {
        this.readHiddenFiles = p1;
        return;
    }

    public void setRootFolderInZip(String p3)
    {
        if (net.lingala.zip4j.util.Zip4jUtil.isStringNotNullAndNotEmpty(p3)) {
            if ((!p3.endsWith("\\")) && (!p3.endsWith("/"))) {
                p3 = new StringBuilder().append(p3).append(net.lingala.zip4j.util.InternalZipConstants.FILE_SEPARATOR).toString();
            }
            p3 = p3.replaceAll("\\\\", "/");
        }
        this.rootFolderInZip = p3;
        return;
    }

    public void setSourceExternalStream(boolean p1)
    {
        this.isSourceExternalStream = p1;
        return;
    }

    public void setSourceFileCRC(int p1)
    {
        this.sourceFileCRC = p1;
        return;
    }

    public void setTimeZone(java.util.TimeZone p1)
    {
        this.timeZone = p1;
        return;
    }
}
