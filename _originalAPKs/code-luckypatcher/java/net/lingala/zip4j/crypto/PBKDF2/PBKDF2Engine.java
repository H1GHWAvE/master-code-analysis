package net.lingala.zip4j.crypto.PBKDF2;
public class PBKDF2Engine {
    protected net.lingala.zip4j.crypto.PBKDF2.PBKDF2Parameters parameters;
    protected net.lingala.zip4j.crypto.PBKDF2.PRF prf;

    public PBKDF2Engine()
    {
        this.parameters = 0;
        this.prf = 0;
        return;
    }

    public PBKDF2Engine(net.lingala.zip4j.crypto.PBKDF2.PBKDF2Parameters p2)
    {
        this.parameters = p2;
        this.prf = 0;
        return;
    }

    public PBKDF2Engine(net.lingala.zip4j.crypto.PBKDF2.PBKDF2Parameters p1, net.lingala.zip4j.crypto.PBKDF2.PRF p2)
    {
        this.parameters = p1;
        this.prf = p2;
        return;
    }

    protected void INT(byte[] p3, int p4, int p5)
    {
        p3[(p4 + 0)] = ((byte) (p5 / 16777216));
        p3[(p4 + 1)] = ((byte) (p5 / 65536));
        p3[(p4 + 2)] = ((byte) (p5 / 256));
        p3[(p4 + 3)] = ((byte) p5);
        return;
    }

    protected byte[] PBKDF2(net.lingala.zip4j.crypto.PBKDF2.PRF p12, byte[] p13, int p14, int p15)
    {
        if (p13 == null) {
            p13 = new byte[0];
        }
        int v8 = p12.getHLen();
        int v9 = this.ceil(p15, v8);
        int v10 = (p15 - ((v9 - 1) * v8));
        byte[] v1 = new byte[(v9 * v8)];
        int v2 = 0;
        int v6 = 1;
        while (v6 <= v9) {
            this._F(v1, v2, p12, p13, p14, v6);
            v2 += v8;
            v6++;
        }
        byte[] v7;
        if (v10 >= v8) {
            v7 = v1;
        } else {
            v7 = new byte[p15];
            System.arraycopy(v1, 0, v7, 0, p15);
        }
        return v7;
    }

    protected void _F(byte[] p7, int p8, net.lingala.zip4j.crypto.PBKDF2.PRF p9, byte[] p10, int p11, int p12)
    {
        int v2 = p9.getHLen();
        byte[] v1 = new byte[v2];
        byte[] v0 = new byte[(p10.length + 4)];
        System.arraycopy(p10, 0, v0, 0, p10.length);
        this.INT(v0, p10.length, p12);
        int v3 = 0;
        while (v3 < p11) {
            v0 = p9.doFinal(v0);
            this.xor(v1, v0);
            v3++;
        }
        System.arraycopy(v1, 0, p7, p8, v2);
        return;
    }

    protected void assertPRF(byte[] p3)
    {
        if (this.prf == null) {
            this.prf = new net.lingala.zip4j.crypto.PBKDF2.MacBasedPRF(this.parameters.getHashAlgorithm());
        }
        this.prf.init(p3);
        return;
    }

    protected int ceil(int p3, int p4)
    {
        int v0 = 0;
        if ((p3 % p4) > 0) {
            v0 = 1;
        }
        return ((p3 / p4) + v0);
    }

    public byte[] deriveKey(char[] p2)
    {
        return this.deriveKey(p2, 0);
    }

    public byte[] deriveKey(char[] p6, int p7)
    {
        if (p6 != null) {
            this.assertPRF(net.lingala.zip4j.util.Raw.convertCharArrayToByteArray(p6));
            if (p7 == 0) {
                p7 = this.prf.getHLen();
            }
            return this.PBKDF2(this.prf, this.parameters.getSalt(), this.parameters.getIterationCount(), p7);
        } else {
            throw new NullPointerException();
        }
    }

    public net.lingala.zip4j.crypto.PBKDF2.PBKDF2Parameters getParameters()
    {
        return this.parameters;
    }

    public net.lingala.zip4j.crypto.PBKDF2.PRF getPseudoRandomFunction()
    {
        return this.prf;
    }

    public void setParameters(net.lingala.zip4j.crypto.PBKDF2.PBKDF2Parameters p1)
    {
        this.parameters = p1;
        return;
    }

    public void setPseudoRandomFunction(net.lingala.zip4j.crypto.PBKDF2.PRF p1)
    {
        this.prf = p1;
        return;
    }

    public boolean verifyKey(char[] p7)
    {
        int v3 = 0;
        byte[] v2 = this.getParameters().getDerivedKey();
        if ((v2 != null) && (v2.length != 0)) {
            byte[] v1 = this.deriveKey(p7, v2.length);
            if ((v1 != null) && (v1.length == v2.length)) {
                int v0 = 0;
                while (v0 < v1.length) {
                    if (v1[v0] == v2[v0]) {
                        v0++;
                    }
                }
                v3 = 1;
            }
        }
        return v3;
    }

    protected void xor(byte[] p4, byte[] p5)
    {
        int v0 = 0;
        while (v0 < p4.length) {
            p4[v0] = ((byte) (p4[v0] ^ p5[v0]));
            v0++;
        }
        return;
    }
}
