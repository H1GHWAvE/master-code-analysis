package net.lingala.zip4j.crypto.PBKDF2;
public class PBKDF2Parameters {
    protected byte[] derivedKey;
    protected String hashAlgorithm;
    protected String hashCharset;
    protected int iterationCount;
    protected byte[] salt;

    public PBKDF2Parameters()
    {
        this.hashAlgorithm = 0;
        this.hashCharset = "UTF-8";
        this.salt = 0;
        this.iterationCount = 1000;
        this.derivedKey = 0;
        return;
    }

    public PBKDF2Parameters(String p2, String p3, byte[] p4, int p5)
    {
        this.hashAlgorithm = p2;
        this.hashCharset = p3;
        this.salt = p4;
        this.iterationCount = p5;
        this.derivedKey = 0;
        return;
    }

    public PBKDF2Parameters(String p1, String p2, byte[] p3, int p4, byte[] p5)
    {
        this.hashAlgorithm = p1;
        this.hashCharset = p2;
        this.salt = p3;
        this.iterationCount = p4;
        this.derivedKey = p5;
        return;
    }

    public byte[] getDerivedKey()
    {
        return this.derivedKey;
    }

    public String getHashAlgorithm()
    {
        return this.hashAlgorithm;
    }

    public String getHashCharset()
    {
        return this.hashCharset;
    }

    public int getIterationCount()
    {
        return this.iterationCount;
    }

    public byte[] getSalt()
    {
        return this.salt;
    }

    public void setDerivedKey(byte[] p1)
    {
        this.derivedKey = p1;
        return;
    }

    public void setHashAlgorithm(String p1)
    {
        this.hashAlgorithm = p1;
        return;
    }

    public void setHashCharset(String p1)
    {
        this.hashCharset = p1;
        return;
    }

    public void setIterationCount(int p1)
    {
        this.iterationCount = p1;
        return;
    }

    public void setSalt(byte[] p1)
    {
        this.salt = p1;
        return;
    }
}
