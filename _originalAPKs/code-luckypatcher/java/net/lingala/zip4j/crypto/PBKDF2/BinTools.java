package net.lingala.zip4j.crypto.PBKDF2;
 class BinTools {
    public static final String hex = "0123456789ABCDEF";

    BinTools()
    {
        return;
    }

    public static String bin2hex(byte[] p7)
    {
        String v3_3;
        if (p7 != null) {
            StringBuffer v1_1 = new StringBuffer((p7.length * 2));
            int v4 = p7.length;
            String v3_2 = 0;
            while (v3_2 < v4) {
                int v2 = ((p7[v3_2] + 256) % 256);
                v1_1.append("0123456789ABCDEF".charAt(((v2 / 16) & 15)));
                v1_1.append("0123456789ABCDEF".charAt(((v2 % 16) & 15)));
                v3_2++;
            }
            v3_3 = v1_1.toString();
        } else {
            v3_3 = "";
        }
        return v3_3;
    }

    public static int hex2bin(char p3)
    {
        if ((p3 < 48) || (p3 > 57)) {
            if ((p3 < 65) || (p3 > 70)) {
                if ((p3 < 97) || (p3 > 102)) {
                    throw new IllegalArgumentException(new StringBuilder().append("Input string may only contain hex digits, but found \'").append(p3).append("\'").toString());
                } else {
                    int v0_9 = ((p3 - 97) + 10);
                }
            } else {
                v0_9 = ((p3 - 65) + 10);
            }
        } else {
            v0_9 = (p3 - 48);
        }
        return v0_9;
    }

    public static byte[] hex2bin(String p9)
    {
        String v4 = p9;
        if (p9 != null) {
            if ((p9.length() % 2) != 0) {
                v4 = new StringBuilder().append("0").append(p9).toString();
            }
        } else {
            v4 = "";
        }
        byte[] v6 = new byte[(v4.length() / 2)];
        int v1 = 0;
        int v5 = 0;
        while (v1 < v4.length()) {
            int v2 = (v1 + 1);
            char v0 = v4.charAt(v1);
            v1 = (v2 + 1);
            v6[v5] = ((byte) ((net.lingala.zip4j.crypto.PBKDF2.BinTools.hex2bin(v0) * 16) + net.lingala.zip4j.crypto.PBKDF2.BinTools.hex2bin(v4.charAt(v2))));
            v5++;
        }
        return v6;
    }
}
