package net.lingala.zip4j.crypto.PBKDF2;
public class MacBasedPRF implements net.lingala.zip4j.crypto.PBKDF2.PRF {
    protected int hLen;
    protected javax.crypto.Mac mac;
    protected String macAlgorithm;

    public MacBasedPRF(String p3)
    {
        this.macAlgorithm = p3;
        try {
            this.mac = javax.crypto.Mac.getInstance(p3);
            this.hLen = this.mac.getMacLength();
            return;
        } catch (java.security.NoSuchAlgorithmException v0) {
            throw new RuntimeException(v0);
        }
    }

    public MacBasedPRF(String p3, String p4)
    {
        this.macAlgorithm = p3;
        try {
            this.mac = javax.crypto.Mac.getInstance(p3, p4);
            this.hLen = this.mac.getMacLength();
            return;
        } catch (java.security.NoSuchProviderException v0_0) {
            throw new RuntimeException(v0_0);
        } catch (java.security.NoSuchProviderException v0_1) {
            throw new RuntimeException(v0_1);
        }
    }

    public byte[] doFinal()
    {
        return this.mac.doFinal();
    }

    public byte[] doFinal(byte[] p3)
    {
        return this.mac.doFinal(p3);
    }

    public int getHLen()
    {
        return this.hLen;
    }

    public void init(byte[] p5)
    {
        try {
            this.mac.init(new javax.crypto.spec.SecretKeySpec(p5, this.macAlgorithm));
            return;
        } catch (java.security.InvalidKeyException v0) {
            throw new RuntimeException(v0);
        }
    }

    public void update(byte[] p3)
    {
        try {
            this.mac.update(p3);
            return;
        } catch (IllegalStateException v0) {
            throw new RuntimeException(v0);
        }
    }

    public void update(byte[] p3, int p4, int p5)
    {
        try {
            this.mac.update(p3, p4, p5);
            return;
        } catch (IllegalStateException v0) {
            throw new RuntimeException(v0);
        }
    }
}
