package net.lingala.zip4j.crypto.engine;
public class AESEngine {
    private static final byte[] S;
    private static final int[] T0;
    private static final int[] rcon;
    private int C0;
    private int C1;
    private int C2;
    private int C3;
    private int rounds;
    private int[][] workingKey;

    static AESEngine()
    {
        int[] v0_0 = new byte[256];
        v0_0 = {99, 124, 119, 123, -14, 107, 111, -59, 48, 1, 103, 43, -2, -41, -85, 118, -54, -126, -55, 125, -6, 89, 71, -16, -83, -44, -94, -81, -100, -92, 114, -64, -73, -3, -109, 38, 54, 63, -9, -52, 52, -91, -27, -15, 113, -40, 49, 21, 4, -57, 35, -61, 24, -106, 5, -102, 7, 18, -128, -30, -21, 39, -78, 117, 9, -125, 44, 26, 27, 110, 90, -96, 82, 59, -42, -77, 41, -29, 47, -124, 83, -47, 0, -19, 32, -4, -79, 91, 106, -53, -66, 57, 74, 76, 88, -49, -48, -17, -86, -5, 67, 77, 51, -123, 69, -7, 2, 127, 80, 60, -97, -88, 81, -93, 64, -113, -110, -99, 56, -11, -68, -74, -38, 33, 16, -1, -13, -46, -51, 12, 19, -20, 95, -105, 68, 23, -60, -89, 126, 61, 100, 93, 25, 115, 96, -127, 79, -36, 34, 42, -112, -120, 70, -18, -72, 20, -34, 94, 11, -37, -32, 50, 58, 10, 73, 6, 36, 92, -62, -45, -84, 98, -111, -107, -28, 121, -25, -56, 55, 109, -115, -43, 78, -87, 108, 86, -12, -22, 101, 122, -82, 8, -70, 120, 37, 46, 28, -90, -76, -58, -24, -35, 116, 31, 75, -67, -117, -118, 112, 62, -75, 102, 72, 3, -10, 14, 97, 53, 87, -71, -122, -63, 29, -98, -31, -8, -104, 17, 105, -39, -114, -108, -101, 30, -121, -23, -50, 85, 40, -33, -116, -95, -119, 13, -65, -26, 66, 104, 65, -103, 45, 15, -80, 84, -69, 22};
        net.lingala.zip4j.crypto.engine.AESEngine.S = v0_0;
        int[] v0_2 = new int[30];
        v0_2 = {1, 2, 4, 8, 16, 32, 64, 128, 27, 54, 108, 216, 171, 77, 154, 47, 94, 188, 99, 198, 151, 53, 106, 212, 179, 125, 250, 239, 197, 145};
        net.lingala.zip4j.crypto.engine.AESEngine.rcon = v0_2;
        int[] v0_3 = new int[256];
        v0_3 = {-1520213050, -2072216328, -1720223762, -1921287178, 234025727, -1117033514, -1318096930, 1422247313, 1345335392, 50397442, -1452841010, 2099981142, 436141799, 1658312629, -424957107, -1703512340, 1170918031, -1652391393, 1086966153, -2021818886, 368769775, -346465870, -918075506, 200339707, -324162239, 1742001331, -39673249, -357585083, -1080255453, -140204973, -1770884380, 1539358875, -1028147339, 486407649, -1366060227, 1780885068, 1513502316, 1094664062, 49805301, 1338821763, 1546925160, -190470831, 887481809, 150073849, -1821281822, 1943591083, 1395732834, 1058346282, 201589768, 1388824469, 1696801606, 1589887901, 672667696, -1583966665, 251987210, -1248159185, 151455502, 907153956, -1686077413, 1038279391, 652995533, 1764173646, -843926913, -1619692054, 453576978, -1635548387, 1949051992, 773462580, 756751158, -1301385508, -296068428, -73359269, -162377052, 1295727478, 1641469623, -827083907, 2066295122, 1055122397, 1898917726, -1752923117, -179088474, 1758581177, 0, 753790401, 1612718144, 536673507, -927878791, -312779850, -1100322092, 1187761037, -641810841, 1262041458, -565556588, -733197160, -396863312, 1255133061, 1808847035, 720367557, -441800113, 385612781, -985447546, -682799718, 1429418854, -1803188975, -817543798, 284817897, 100794884, -2122350594, -263171936, 1144798328, -1163944155, -475486133, -212774494, -22830243, -1069531008, -1970303227, -1382903233, -1130521311, 1211644016, 83228145, -541279133, -1044990345, 1977277103, 1663115586, 806359072, 452984805, 250868733, 1842533055, 1288555905, 336333848, 890442534, 804056259, -513843266, -1567123659, -867941240, 957814574, 1472513171, -223893675, -2105639172, 1195195770, -1402706744, -413311558, 723065138, -1787595802, -1604296512, -1736343271, -783331426, 2145180835, 1713513028, 2116692564, -1416589253, -2088204277, -901364084, 703524551, -742868885, 1007948840, 2044649127, -497131844, 487262998, 1994120109, 1004593371, 1446130276, 1312438900, 503974420, -615954030, 168166924, 1814307912, -463709000, 1573044895, 1859376061, -273896381, -1503501628, -1466855111, -1533700815, 937747667, -1954973198, 854058965, 1137232011, 1496790894, -1217565222, -1936880383, 1691735473, -766620004, -525751991, -1267962664, -95005012, 133494003, 636152527, -1352309302, -1904575756, -374428089, 403179536, -709182865, -2005370640, 1864705354, 1915629148, 605822008, -240736681, -944458637, 1371981463, 602466507, 2094914977, -1670089496, 555687742, -582268010, -591544991, -2037675251, -2054518257, -1871679264, 1111375484, -994724495, -1436129588, -666351472, 84083462, 32962295, 302911004, -1553899070, 1597322602, -111716434, -793134743, -1853454825, 1489093017, 656219450, -1180787161, 954327513, 335083755, -1281845205, 856756514, -1150719534, 1893325225, -1987146233, -1483434957, -1231316179, 572399164, -1836611819, 552200649, 1238290055, -11184726, 2015897680, 2061492133, -1886614525, -123625127, -2138470135, 386731290, -624967835, 837215959, -968736124, -1201116976, -1019133566, -1332111063, 1999449434, 286199582, -877612933, -61582168, -692339859, 974525996};
        net.lingala.zip4j.crypto.engine.AESEngine.T0 = v0_3;
        return;
    }

    public AESEngine(byte[] p2)
    {
        this.workingKey = ((int[][]) 0);
        this.init(p2);
        return;
    }

    private final void encryptBlock(int[][] p15)
    {
        this.C0 = (this.C0 ^ p15[0][0]);
        this.C1 = (this.C1 ^ p15[0][1]);
        this.C2 = (this.C2 ^ p15[0][2]);
        this.C3 = (this.C3 ^ p15[0][3]);
        int v0 = 1;
        while (v0 < (this.rounds - 1)) {
            int v2_1 = ((((net.lingala.zip4j.crypto.engine.AESEngine.T0[(this.C0 & 255)] ^ this.shift(net.lingala.zip4j.crypto.engine.AESEngine.T0[((this.C1 >> 8) & 255)], 24)) ^ this.shift(net.lingala.zip4j.crypto.engine.AESEngine.T0[((this.C2 >> 16) & 255)], 16)) ^ this.shift(net.lingala.zip4j.crypto.engine.AESEngine.T0[((this.C3 >> 24) & 255)], 8)) ^ p15[v0][0]);
            int v3_1 = ((((net.lingala.zip4j.crypto.engine.AESEngine.T0[(this.C1 & 255)] ^ this.shift(net.lingala.zip4j.crypto.engine.AESEngine.T0[((this.C2 >> 8) & 255)], 24)) ^ this.shift(net.lingala.zip4j.crypto.engine.AESEngine.T0[((this.C3 >> 16) & 255)], 16)) ^ this.shift(net.lingala.zip4j.crypto.engine.AESEngine.T0[((this.C0 >> 24) & 255)], 8)) ^ p15[v0][1]);
            int v4_1 = ((((net.lingala.zip4j.crypto.engine.AESEngine.T0[(this.C2 & 255)] ^ this.shift(net.lingala.zip4j.crypto.engine.AESEngine.T0[((this.C3 >> 8) & 255)], 24)) ^ this.shift(net.lingala.zip4j.crypto.engine.AESEngine.T0[((this.C0 >> 16) & 255)], 16)) ^ this.shift(net.lingala.zip4j.crypto.engine.AESEngine.T0[((this.C1 >> 24) & 255)], 8)) ^ p15[v0][2]);
            int v1_1 = (v0 + 1);
            int v5_1 = ((((net.lingala.zip4j.crypto.engine.AESEngine.T0[(this.C3 & 255)] ^ this.shift(net.lingala.zip4j.crypto.engine.AESEngine.T0[((this.C0 >> 8) & 255)], 24)) ^ this.shift(net.lingala.zip4j.crypto.engine.AESEngine.T0[((this.C1 >> 16) & 255)], 16)) ^ this.shift(net.lingala.zip4j.crypto.engine.AESEngine.T0[((this.C2 >> 24) & 255)], 8)) ^ p15[v0][3]);
            this.C0 = ((((net.lingala.zip4j.crypto.engine.AESEngine.T0[(v2_1 & 255)] ^ this.shift(net.lingala.zip4j.crypto.engine.AESEngine.T0[((v3_1 >> 8) & 255)], 24)) ^ this.shift(net.lingala.zip4j.crypto.engine.AESEngine.T0[((v4_1 >> 16) & 255)], 16)) ^ this.shift(net.lingala.zip4j.crypto.engine.AESEngine.T0[((v5_1 >> 24) & 255)], 8)) ^ p15[v1_1][0]);
            this.C1 = ((((net.lingala.zip4j.crypto.engine.AESEngine.T0[(v3_1 & 255)] ^ this.shift(net.lingala.zip4j.crypto.engine.AESEngine.T0[((v4_1 >> 8) & 255)], 24)) ^ this.shift(net.lingala.zip4j.crypto.engine.AESEngine.T0[((v5_1 >> 16) & 255)], 16)) ^ this.shift(net.lingala.zip4j.crypto.engine.AESEngine.T0[((v2_1 >> 24) & 255)], 8)) ^ p15[v1_1][1]);
            this.C2 = ((((net.lingala.zip4j.crypto.engine.AESEngine.T0[(v4_1 & 255)] ^ this.shift(net.lingala.zip4j.crypto.engine.AESEngine.T0[((v5_1 >> 8) & 255)], 24)) ^ this.shift(net.lingala.zip4j.crypto.engine.AESEngine.T0[((v2_1 >> 16) & 255)], 16)) ^ this.shift(net.lingala.zip4j.crypto.engine.AESEngine.T0[((v3_1 >> 24) & 255)], 8)) ^ p15[v1_1][2]);
            v0 = (v1_1 + 1);
            this.C3 = ((((net.lingala.zip4j.crypto.engine.AESEngine.T0[(v5_1 & 255)] ^ this.shift(net.lingala.zip4j.crypto.engine.AESEngine.T0[((v2_1 >> 8) & 255)], 24)) ^ this.shift(net.lingala.zip4j.crypto.engine.AESEngine.T0[((v3_1 >> 16) & 255)], 16)) ^ this.shift(net.lingala.zip4j.crypto.engine.AESEngine.T0[((v4_1 >> 24) & 255)], 8)) ^ p15[v1_1][3]);
        }
        int v2_0 = ((((net.lingala.zip4j.crypto.engine.AESEngine.T0[(this.C0 & 255)] ^ this.shift(net.lingala.zip4j.crypto.engine.AESEngine.T0[((this.C1 >> 8) & 255)], 24)) ^ this.shift(net.lingala.zip4j.crypto.engine.AESEngine.T0[((this.C2 >> 16) & 255)], 16)) ^ this.shift(net.lingala.zip4j.crypto.engine.AESEngine.T0[((this.C3 >> 24) & 255)], 8)) ^ p15[v0][0]);
        int v3_0 = ((((net.lingala.zip4j.crypto.engine.AESEngine.T0[(this.C1 & 255)] ^ this.shift(net.lingala.zip4j.crypto.engine.AESEngine.T0[((this.C2 >> 8) & 255)], 24)) ^ this.shift(net.lingala.zip4j.crypto.engine.AESEngine.T0[((this.C3 >> 16) & 255)], 16)) ^ this.shift(net.lingala.zip4j.crypto.engine.AESEngine.T0[((this.C0 >> 24) & 255)], 8)) ^ p15[v0][1]);
        int v4_0 = ((((net.lingala.zip4j.crypto.engine.AESEngine.T0[(this.C2 & 255)] ^ this.shift(net.lingala.zip4j.crypto.engine.AESEngine.T0[((this.C3 >> 8) & 255)], 24)) ^ this.shift(net.lingala.zip4j.crypto.engine.AESEngine.T0[((this.C0 >> 16) & 255)], 16)) ^ this.shift(net.lingala.zip4j.crypto.engine.AESEngine.T0[((this.C1 >> 24) & 255)], 8)) ^ p15[v0][2]);
        int v1_0 = (v0 + 1);
        int v5_0 = ((((net.lingala.zip4j.crypto.engine.AESEngine.T0[(this.C3 & 255)] ^ this.shift(net.lingala.zip4j.crypto.engine.AESEngine.T0[((this.C0 >> 8) & 255)], 24)) ^ this.shift(net.lingala.zip4j.crypto.engine.AESEngine.T0[((this.C1 >> 16) & 255)], 16)) ^ this.shift(net.lingala.zip4j.crypto.engine.AESEngine.T0[((this.C2 >> 24) & 255)], 8)) ^ p15[v0][3]);
        this.C0 = (((((net.lingala.zip4j.crypto.engine.AESEngine.S[(v2_0 & 255)] & 255) ^ ((net.lingala.zip4j.crypto.engine.AESEngine.S[((v3_0 >> 8) & 255)] & 255) << 8)) ^ ((net.lingala.zip4j.crypto.engine.AESEngine.S[((v4_0 >> 16) & 255)] & 255) << 16)) ^ (net.lingala.zip4j.crypto.engine.AESEngine.S[((v5_0 >> 24) & 255)] << 24)) ^ p15[v1_0][0]);
        this.C1 = (((((net.lingala.zip4j.crypto.engine.AESEngine.S[(v3_0 & 255)] & 255) ^ ((net.lingala.zip4j.crypto.engine.AESEngine.S[((v4_0 >> 8) & 255)] & 255) << 8)) ^ ((net.lingala.zip4j.crypto.engine.AESEngine.S[((v5_0 >> 16) & 255)] & 255) << 16)) ^ (net.lingala.zip4j.crypto.engine.AESEngine.S[((v2_0 >> 24) & 255)] << 24)) ^ p15[v1_0][1]);
        this.C2 = (((((net.lingala.zip4j.crypto.engine.AESEngine.S[(v4_0 & 255)] & 255) ^ ((net.lingala.zip4j.crypto.engine.AESEngine.S[((v5_0 >> 8) & 255)] & 255) << 8)) ^ ((net.lingala.zip4j.crypto.engine.AESEngine.S[((v2_0 >> 16) & 255)] & 255) << 16)) ^ (net.lingala.zip4j.crypto.engine.AESEngine.S[((v3_0 >> 24) & 255)] << 24)) ^ p15[v1_0][2]);
        this.C3 = (((((net.lingala.zip4j.crypto.engine.AESEngine.S[(v5_0 & 255)] & 255) ^ ((net.lingala.zip4j.crypto.engine.AESEngine.S[((v2_0 >> 8) & 255)] & 255) << 8)) ^ ((net.lingala.zip4j.crypto.engine.AESEngine.S[((v3_0 >> 16) & 255)] & 255) << 16)) ^ (net.lingala.zip4j.crypto.engine.AESEngine.S[((v4_0 >> 24) & 255)] << 24)) ^ p15[v1_0][3]);
        return;
    }

    private int[][] generateWorkingKey(byte[] p14)
    {
        int v3 = (p14.length / 4);
        if (((v3 == 4) || ((v3 == 6) || (v3 == 8))) && ((v3 * 4) == p14.length)) {
            this.rounds = (v3 + 6);
            int[][] v0_1 = ((int[][]) reflect.Array.newInstance(Integer.TYPE, new int[] {(this.rounds + 1), 4})));
            int v4 = 0;
            int v1_0 = 0;
            while (v1_0 < p14.length) {
                v0_1[(v4 >> 2)][(v4 & 3)] = ((((p14[v1_0] & 255) | ((p14[(v1_0 + 1)] & 255) << 8)) | ((p14[(v1_0 + 2)] & 255) << 16)) | (p14[(v1_0 + 3)] << 24));
                v1_0 += 4;
                v4++;
            }
            int v2 = ((this.rounds + 1) << 2);
            int v1_1 = v3;
            while (v1_1 < v2) {
                int v5 = v0_1[((v1_1 - 1) >> 2)][((v1_1 - 1) & 3)];
                if ((v1_1 % v3) != 0) {
                    if ((v3 > 6) && ((v1_1 % v3) == 4)) {
                        v5 = this.subWord(v5);
                    }
                } else {
                    v5 = (this.subWord(this.shift(v5, 8)) ^ net.lingala.zip4j.crypto.engine.AESEngine.rcon[((v1_1 / v3) - 1)]);
                }
                v0_1[(v1_1 >> 2)][(v1_1 & 3)] = (v0_1[((v1_1 - v3) >> 2)][((v1_1 - v3) & 3)] ^ v5);
                v1_1++;
            }
            return v0_1;
        } else {
            throw new net.lingala.zip4j.exception.ZipException("invalid key length (not 128/192/256)");
        }
    }

    private int shift(int p3, int p4)
    {
        return ((p3 >> p4) | (p3 << (- p4)));
    }

    private final void stateIn(byte[] p5, int p6)
    {
        int v1_0 = (p6 + 1);
        this.C0 = (p5[p6] & 255);
        int v0_1 = (v1_0 + 1);
        this.C0 = (this.C0 | ((p5[v1_0] & 255) << 8));
        int v1_1 = (v0_1 + 1);
        this.C0 = (this.C0 | ((p5[v0_1] & 255) << 16));
        int v0_2 = (v1_1 + 1);
        this.C0 = (this.C0 | (p5[v1_1] << 24));
        int v1_2 = (v0_2 + 1);
        this.C1 = (p5[v0_2] & 255);
        int v0_3 = (v1_2 + 1);
        this.C1 = (this.C1 | ((p5[v1_2] & 255) << 8));
        int v1_3 = (v0_3 + 1);
        this.C1 = (this.C1 | ((p5[v0_3] & 255) << 16));
        int v0_4 = (v1_3 + 1);
        this.C1 = (this.C1 | (p5[v1_3] << 24));
        int v1_4 = (v0_4 + 1);
        this.C2 = (p5[v0_4] & 255);
        int v0_5 = (v1_4 + 1);
        this.C2 = (this.C2 | ((p5[v1_4] & 255) << 8));
        int v1_5 = (v0_5 + 1);
        this.C2 = (this.C2 | ((p5[v0_5] & 255) << 16));
        int v0_6 = (v1_5 + 1);
        this.C2 = (this.C2 | (p5[v1_5] << 24));
        int v1_6 = (v0_6 + 1);
        this.C3 = (p5[v0_6] & 255);
        int v0_7 = (v1_6 + 1);
        this.C3 = (this.C3 | ((p5[v1_6] & 255) << 8));
        int v1_7 = (v0_7 + 1);
        this.C3 = (this.C3 | ((p5[v0_7] & 255) << 16));
        this.C3 = (this.C3 | (p5[v1_7] << 24));
        return;
    }

    private final void stateOut(byte[] p4, int p5)
    {
        int v1_0 = (p5 + 1);
        p5[int v0_0] = ((byte) this.C0);
        int v0_1 = (v1_0 + 1);
        p4[v1_0] = ((byte) (this.C0 >> 8));
        int v1_1 = (v0_1 + 1);
        p4[v0_1] = ((byte) (this.C0 >> 16));
        int v0_2 = (v1_1 + 1);
        p4[v1_1] = ((byte) (this.C0 >> 24));
        int v1_2 = (v0_2 + 1);
        p4[v0_2] = ((byte) this.C1);
        int v0_3 = (v1_2 + 1);
        p4[v1_2] = ((byte) (this.C1 >> 8));
        int v1_3 = (v0_3 + 1);
        p4[v0_3] = ((byte) (this.C1 >> 16));
        int v0_4 = (v1_3 + 1);
        p4[v1_3] = ((byte) (this.C1 >> 24));
        int v1_4 = (v0_4 + 1);
        p4[v0_4] = ((byte) this.C2);
        int v0_5 = (v1_4 + 1);
        p4[v1_4] = ((byte) (this.C2 >> 8));
        int v1_5 = (v0_5 + 1);
        p4[v0_5] = ((byte) (this.C2 >> 16));
        int v0_6 = (v1_5 + 1);
        p4[v1_5] = ((byte) (this.C2 >> 24));
        int v1_6 = (v0_6 + 1);
        p4[v0_6] = ((byte) this.C3);
        int v0_7 = (v1_6 + 1);
        p4[v1_6] = ((byte) (this.C3 >> 8));
        int v1_7 = (v0_7 + 1);
        p4[v0_7] = ((byte) (this.C3 >> 16));
        p4[v1_7] = ((byte) (this.C3 >> 24));
        return;
    }

    private int subWord(int p4)
    {
        return ((((net.lingala.zip4j.crypto.engine.AESEngine.S[(p4 & 255)] & 255) | ((net.lingala.zip4j.crypto.engine.AESEngine.S[((p4 >> 8) & 255)] & 255) << 8)) | ((net.lingala.zip4j.crypto.engine.AESEngine.S[((p4 >> 16) & 255)] & 255) << 16)) | (net.lingala.zip4j.crypto.engine.AESEngine.S[((p4 >> 24) & 255)] << 24));
    }

    public void init(byte[] p2)
    {
        this.workingKey = this.generateWorkingKey(p2);
        return;
    }

    public int processBlock(byte[] p3, int p4, byte[] p5, int p6)
    {
        if (this.workingKey != null) {
            if ((p4 + 16) <= p3.length) {
                if ((p6 + 16) <= p5.length) {
                    this.stateIn(p3, p4);
                    this.encryptBlock(this.workingKey);
                    this.stateOut(p5, p6);
                    return 16;
                } else {
                    throw new net.lingala.zip4j.exception.ZipException("output buffer too short");
                }
            } else {
                throw new net.lingala.zip4j.exception.ZipException("input buffer too short");
            }
        } else {
            throw new net.lingala.zip4j.exception.ZipException("AES engine not initialised");
        }
    }

    public int processBlock(byte[] p2, byte[] p3)
    {
        return this.processBlock(p2, 0, p3, 0);
    }
}
