package net.lingala.zip4j.crypto.engine;
public class ZipCryptoEngine {
    private static final int[] CRC_TABLE;
    private final int[] keys;

    static ZipCryptoEngine()
    {
        int v3_0 = new int[256];
        net.lingala.zip4j.crypto.engine.ZipCryptoEngine.CRC_TABLE = v3_0;
        int v0 = 0;
        while (v0 < 256) {
            int v2 = v0;
            int v1 = 0;
            while (v1 < 8) {
                if ((v2 & 1) != 1) {
                    v2 >>= 1;
                } else {
                    v2 = ((v2 >> 1) ^ -306674912);
                }
                v1++;
            }
            net.lingala.zip4j.crypto.engine.ZipCryptoEngine.CRC_TABLE[v0] = v2;
            v0++;
        }
        return;
    }

    public ZipCryptoEngine()
    {
        int[] v0_1 = new int[3];
        this.keys = v0_1;
        return;
    }

    private int crc32(int p4, byte p5)
    {
        return ((p4 >> 8) ^ net.lingala.zip4j.crypto.engine.ZipCryptoEngine.CRC_TABLE[((p4 ^ p5) & 255)]);
    }

    public byte decryptByte()
    {
        int v0 = (this.keys[2] | 2);
        return ((byte) (((v0 ^ 1) * v0) >> 8));
    }

    public void initKeys(char[] p5)
    {
        this.keys[0] = 305419896;
        this.keys[1] = 591751049;
        this.keys[2] = 878082192;
        int v0 = 0;
        while (v0 < p5.length) {
            this.updateKeys(((byte) (p5[v0] & 255)));
            v0++;
        }
        return;
    }

    public void updateKeys(byte p7)
    {
        this.keys[0] = this.crc32(this.keys[0], p7);
        int[] v0_1 = this.keys;
        v0_1[1] = (v0_1[1] + (this.keys[0] & 255));
        this.keys[1] = ((this.keys[1] * 134775813) + 1);
        this.keys[2] = this.crc32(this.keys[2], ((byte) (this.keys[1] >> 24)));
        return;
    }
}
