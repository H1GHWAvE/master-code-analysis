package net.lingala.zip4j.crypto;
public class StandardEncrypter implements net.lingala.zip4j.crypto.IEncrypter {
    private byte[] headerBytes;
    private net.lingala.zip4j.crypto.engine.ZipCryptoEngine zipCryptoEngine;

    public StandardEncrypter(char[] p3, int p4)
    {
        if ((p3 != null) && (p3.length > 0)) {
            this.zipCryptoEngine = new net.lingala.zip4j.crypto.engine.ZipCryptoEngine();
            byte[] v0_4 = new byte[12];
            this.headerBytes = v0_4;
            this.init(p3, p4);
            return;
        } else {
            throw new net.lingala.zip4j.exception.ZipException("input password is null or empty in standard encrpyter constructor");
        }
    }

    private void init(char[] p5, int p6)
    {
        if ((p5 != null) && (p5.length > 0)) {
            this.zipCryptoEngine.initKeys(p5);
            this.headerBytes = this.generateRandomBytes(12);
            this.zipCryptoEngine.initKeys(p5);
            this.headerBytes[11] = ((byte) (p6 >> 24));
            this.headerBytes[10] = ((byte) (p6 >> 16));
            if (this.headerBytes.length >= 12) {
                this.encryptData(this.headerBytes);
                return;
            } else {
                throw new net.lingala.zip4j.exception.ZipException("invalid header bytes generated, cannot perform standard encryption");
            }
        } else {
            throw new net.lingala.zip4j.exception.ZipException("input password is null or empty, cannot initialize standard encrypter");
        }
    }

    protected byte encryptByte(byte p3)
    {
        byte v0 = ((byte) ((this.zipCryptoEngine.decryptByte() & 255) ^ p3));
        this.zipCryptoEngine.updateKeys(p3);
        return v0;
    }

    public int encryptData(byte[] p3)
    {
        if (p3 != null) {
            return this.encryptData(p3, 0, p3.length);
        } else {
            throw new NullPointerException();
        }
    }

    public int encryptData(byte[] p5, int p6, int p7)
    {
        if (p7 >= 0) {
            int v1 = p6;
            while (v1 < (p6 + p7)) {
                try {
                    p5[v1] = this.encryptByte(p5[v1]);
                    v1++;
                } catch (Exception v0) {
                    throw new net.lingala.zip4j.exception.ZipException(v0);
                }
            }
            return p7;
        } else {
            throw new net.lingala.zip4j.exception.ZipException("invalid length specified to decrpyt data");
        }
    }

    protected byte[] generateRandomBytes(int p6)
    {
        if (p6 > 0) {
            byte[] v0 = new byte[p6];
            java.util.Random v2_1 = new java.util.Random();
            int v1 = 0;
            while (v1 < v0.length) {
                v0[v1] = this.encryptByte(((byte) v2_1.nextInt(256)));
                v1++;
            }
            return v0;
        } else {
            throw new net.lingala.zip4j.exception.ZipException("size is either 0 or less than 0, cannot generate header for standard encryptor");
        }
    }

    public byte[] getHeaderBytes()
    {
        return this.headerBytes;
    }
}
