package net.lingala.zip4j.zip;
public class ZipEngine {
    private net.lingala.zip4j.model.ZipModel zipModel;

    public ZipEngine(net.lingala.zip4j.model.ZipModel p3)
    {
        if (p3 != null) {
            this.zipModel = p3;
            return;
        } else {
            throw new net.lingala.zip4j.exception.ZipException("zip model is null in ZipEngine constructor");
        }
    }

    static synthetic void access$000(net.lingala.zip4j.zip.ZipEngine p0, java.util.ArrayList p1, net.lingala.zip4j.model.ZipParameters p2, net.lingala.zip4j.progress.ProgressMonitor p3)
    {
        p0.initAddFiles(p1, p2, p3);
        return;
    }

    private long calculateTotalWork(java.util.ArrayList p10, net.lingala.zip4j.model.ZipParameters p11)
    {
        if (p10 != null) {
            long v3 = 0;
            int v1 = 0;
            while (v1 < p10.size()) {
                if (((p10.get(v1) instanceof java.io.File)) && (((java.io.File) p10.get(v1)).exists())) {
                    if ((!p11.isEncryptFiles()) || (p11.getEncryptionMethod() != 0)) {
                        v3 += net.lingala.zip4j.util.Zip4jUtil.getFileLengh(((java.io.File) p10.get(v1)));
                    } else {
                        v3 += (net.lingala.zip4j.util.Zip4jUtil.getFileLengh(((java.io.File) p10.get(v1))) * 2);
                    }
                    if ((this.zipModel.getCentralDirectory() != null) && ((this.zipModel.getCentralDirectory().getFileHeaders() != null) && (this.zipModel.getCentralDirectory().getFileHeaders().size() > 0))) {
                        net.lingala.zip4j.model.FileHeader v0 = net.lingala.zip4j.util.Zip4jUtil.getFileHeader(this.zipModel, net.lingala.zip4j.util.Zip4jUtil.getRelativeFileName(((java.io.File) p10.get(v1)).getAbsolutePath(), p11.getRootFolderInZip(), p11.getDefaultFolderPath()));
                        if (v0 != null) {
                            v3 += (net.lingala.zip4j.util.Zip4jUtil.getFileLengh(new java.io.File(this.zipModel.getZipFile())) - v0.getCompressedSize());
                        }
                    }
                }
                v1++;
            }
            return v3;
        } else {
            throw new net.lingala.zip4j.exception.ZipException("file list is null, cannot calculate total work");
        }
    }

    private void checkParameters(net.lingala.zip4j.model.ZipParameters p4)
    {
        if (p4 != null) {
            if ((p4.getCompressionMethod() == 0) || (p4.getCompressionMethod() == 8)) {
                if ((p4.getCompressionMethod() != 8) || ((p4.getCompressionLevel() >= 0) || (p4.getCompressionLevel() <= 9))) {
                    if (!p4.isEncryptFiles()) {
                        p4.setAesKeyStrength(-1);
                        p4.setEncryptionMethod(-1);
                    } else {
                        if ((p4.getEncryptionMethod() == 0) || (p4.getEncryptionMethod() == 99)) {
                            if ((p4.getPassword() == null) || (p4.getPassword().length <= 0)) {
                                throw new net.lingala.zip4j.exception.ZipException("input password is empty or null");
                            }
                        } else {
                            throw new net.lingala.zip4j.exception.ZipException("unsupported encryption method");
                        }
                    }
                    return;
                } else {
                    throw new net.lingala.zip4j.exception.ZipException("invalid compression level. compression level dor deflate should be in the range of 0-9");
                }
            } else {
                throw new net.lingala.zip4j.exception.ZipException("unsupported compression type");
            }
        } else {
            throw new net.lingala.zip4j.exception.ZipException("cannot validate zip parameters");
        }
    }

    private net.lingala.zip4j.model.EndCentralDirRecord createEndOfCentralDirectoryRecord()
    {
        net.lingala.zip4j.model.EndCentralDirRecord v0_1 = new net.lingala.zip4j.model.EndCentralDirRecord();
        v0_1.setSignature(101010256);
        v0_1.setNoOfThisDisk(0);
        v0_1.setTotNoOfEntriesInCentralDir(0);
        v0_1.setTotNoOfEntriesInCentralDirOnThisDisk(0);
        v0_1.setOffsetOfStartOfCentralDir(0);
        return v0_1;
    }

    private void initAddFiles(java.util.ArrayList p19, net.lingala.zip4j.model.ZipParameters p20, net.lingala.zip4j.progress.ProgressMonitor p21)
    {
        if ((p19 != null) && (p20 != null)) {
            if (p19.size() > 0) {
                if (this.zipModel.getEndCentralDirRecord() == null) {
                    this.zipModel.setEndCentralDirRecord(this.createEndOfCentralDirectoryRecord());
                }
                net.lingala.zip4j.io.ZipOutputStream v9 = 0;
                java.io.FileInputStream v6_0 = 0;
                try {
                    this.checkParameters(p20);
                    void v18_1 = this.removeFilesIfExists(p19, p20, p21);
                    boolean v8 = net.lingala.zip4j.util.Zip4jUtil.checkFileExists(v18_1.zipModel.getZipFile());
                    net.lingala.zip4j.io.SplitOutputStream v13_1 = new net.lingala.zip4j.io.SplitOutputStream(new java.io.File(v18_1.zipModel.getZipFile()), v18_1.zipModel.getSplitLength());
                    net.lingala.zip4j.io.ZipOutputStream v10_1 = new net.lingala.zip4j.io.ZipOutputStream(v13_1, v18_1.zipModel);
                    try {
                        if (v8) {
                            if (v18_1.zipModel.getEndCentralDirRecord() != null) {
                                v13_1.seek(v18_1.zipModel.getEndCentralDirRecord().getOffsetOfStartOfCentralDir());
                            } else {
                                throw new net.lingala.zip4j.exception.ZipException("invalid end of central directory record");
                            }
                        }
                    } catch (java.io.IOException v14_56) {
                        v9 = v10_1;
                        if (v6_0 != null) {
                            try {
                                v6_0.close();
                            } catch (java.io.PrintStream v15) {
                            }
                        }
                        if (v9 != null) {
                            try {
                                v9.close();
                            } catch (java.io.PrintStream v15) {
                            }
                        }
                        throw v14_56;
                    } catch (Exception v2_0) {
                        v9 = v10_1;
                        p21.endProgressMonitorError(v2_0);
                        throw new net.lingala.zip4j.exception.ZipException(v2_0);
                    } catch (Exception v2_1) {
                        v9 = v10_1;
                        p21.endProgressMonitorError(v2_1);
                        throw v2_1;
                    }
                    byte[] v11 = new byte[4096];
                    int v5 = 0;
                    java.io.FileInputStream v7 = 0;
                    try {
                        while (v5 < p19.size()) {
                            if (!p21.isCancelAllTasks()) {
                                net.lingala.zip4j.model.ZipParameters v4_1 = ((net.lingala.zip4j.model.ZipParameters) p20.clone());
                                p21.setFileName(((java.io.File) p19.get(v5)).getAbsolutePath());
                                if (!((java.io.File) p19.get(v5)).isDirectory()) {
                                    if ((v4_1.isEncryptFiles()) && (v4_1.getEncryptionMethod() == 0)) {
                                        p21.setCurrentOperation(3);
                                        v4_1.setSourceFileCRC(((int) net.lingala.zip4j.util.CRCUtil.computeFileCRC(((java.io.File) p19.get(v5)).getAbsolutePath(), p21)));
                                        p21.setCurrentOperation(0);
                                        if (p21.isCancelAllTasks()) {
                                            p21.setResult(3);
                                            p21.setState(0);
                                            if (v7 != null) {
                                                try {
                                                    v7.close();
                                                } catch (java.io.IOException v14) {
                                                }
                                            }
                                            if (v10_1 == null) {
                                                return;
                                            } else {
                                                try {
                                                    v10_1.close();
                                                } catch (java.io.IOException v14) {
                                                }
                                                return;
                                            }
                                        }
                                    }
                                    if (net.lingala.zip4j.util.Zip4jUtil.getFileLengh(((java.io.File) p19.get(v5))) == 0) {
                                        v4_1.setCompressionMethod(0);
                                    }
                                }
                                java.io.File v3_1 = ((java.io.File) p19.get(v5));
                                if ((v3_1.isFile()) || (v3_1.isDirectory())) {
                                    v10_1.putNextEntry(((java.io.File) p19.get(v5)), v4_1);
                                    if (!((java.io.File) p19.get(v5)).isDirectory()) {
                                        System.out.println(((java.io.File) p19.get(v5)).getAbsolutePath());
                                        v6_0 = new java.io.FileInputStream(((java.io.File) p19.get(v5)));
                                        while(true) {
                                            int v12 = v6_0.read(v11);
                                            if (v12 == -1) {
                                                break;
                                            }
                                            if (!p21.isCancelAllTasks()) {
                                                v10_1.write(v11, 0, v12);
                                                p21.updateWorkCompleted(((long) v12));
                                            } else {
                                                p21.setResult(3);
                                                p21.setState(0);
                                                if (v6_0 != null) {
                                                    try {
                                                        v6_0.close();
                                                    } catch (java.io.IOException v14) {
                                                    }
                                                }
                                                if (v10_1 == null) {
                                                    return;
                                                } else {
                                                    try {
                                                        v10_1.close();
                                                    } catch (java.io.IOException v14) {
                                                    }
                                                    return;
                                                }
                                            }
                                        }
                                        v10_1.closeEntry();
                                        if (v6_0 != null) {
                                            v6_0.close();
                                        }
                                    } else {
                                        v10_1.closeEntry();
                                        v6_0 = v7;
                                    }
                                } else {
                                    v6_0 = v7;
                                }
                                v5++;
                                v7 = v6_0;
                            } else {
                                p21.setResult(3);
                                p21.setState(0);
                                if (v7 != null) {
                                    try {
                                        v7.close();
                                    } catch (java.io.IOException v14) {
                                    }
                                }
                                if (v10_1 != null) {
                                    try {
                                        v10_1.close();
                                    } catch (java.io.IOException v14) {
                                    }
                                }
                            }
                            return;
                        }
                    } catch (Exception v2_0) {
                        v6_0 = v7;
                        v9 = v10_1;
                    } catch (java.io.IOException v14_56) {
                        v6_0 = v7;
                        v9 = v10_1;
                    } catch (Exception v2_1) {
                        v6_0 = v7;
                        v9 = v10_1;
                    }
                    v10_1.finish();
                    p21.endProgressMonitorSuccess();
                    if (v7 != null) {
                        try {
                            v7.close();
                        } catch (java.io.IOException v14) {
                        }
                    }
                    if (v10_1 != null) {
                        try {
                            v10_1.close();
                        } catch (java.io.IOException v14) {
                        }
                    }
                    return;
                } catch (java.io.IOException v14_56) {
                } catch (Exception v2_0) {
                } catch (Exception v2_1) {
                }
            } else {
                throw new net.lingala.zip4j.exception.ZipException("no files to add");
            }
        } else {
            throw new net.lingala.zip4j.exception.ZipException("one of the input parameters is null when adding files");
        }
    }

    private java.io.RandomAccessFile prepareFileOutputStream()
    {
        String v2 = this.zipModel.getZipFile();
        if (net.lingala.zip4j.util.Zip4jUtil.isStringNotNullAndNotEmpty(v2)) {
            try {
                java.io.File v1_1 = new java.io.File(v2);
            } catch (java.io.FileNotFoundException v0) {
                throw new net.lingala.zip4j.exception.ZipException(v0);
            }
            if (!v1_1.getParentFile().exists()) {
                v1_1.getParentFile().mkdirs();
            }
            return new java.io.RandomAccessFile(v1_1, "rw");
        } else {
            throw new net.lingala.zip4j.exception.ZipException("invalid output path");
        }
    }

    private void removeFilesIfExists(java.util.ArrayList p15, net.lingala.zip4j.model.ZipParameters p16, net.lingala.zip4j.progress.ProgressMonitor p17)
    {
        if ((this.zipModel != null) && ((this.zipModel.getCentralDirectory() != null) && ((this.zipModel.getCentralDirectory().getFileHeaders() != null) && (this.zipModel.getCentralDirectory().getFileHeaders().size() > 0)))) {
            java.io.RandomAccessFile v9 = 0;
            int v6 = 0;
            try {
                while (v6 < p15.size()) {
                    net.lingala.zip4j.model.FileHeader v4 = net.lingala.zip4j.util.Zip4jUtil.getFileHeader(this.zipModel, net.lingala.zip4j.util.Zip4jUtil.getRelativeFileName(((java.io.File) p15.get(v6)).getAbsolutePath(), p16.getRootFolderInZip(), p16.getDefaultFolderPath()));
                    if (v4 != null) {
                        if (v9 != null) {
                            v9.close();
                            v9 = 0;
                        }
                        net.lingala.zip4j.util.ArchiveMaintainer v1_1 = new net.lingala.zip4j.util.ArchiveMaintainer();
                        p17.setCurrentOperation(2);
                        java.util.HashMap v10 = v1_1.initRemoveZipFile(this.zipModel, v4, p17);
                        if (!p17.isCancelAllTasks()) {
                            p17.setCurrentOperation(0);
                            if (v9 == null) {
                                v9 = this.prepareFileOutputStream();
                                if ((v10 != null) && (v10.get("offsetCentralDir") != null)) {
                                    try {
                                        long v7 = Long.parseLong(((String) v10.get("offsetCentralDir")));
                                    } catch (Exception v2) {
                                        throw new net.lingala.zip4j.exception.ZipException("Error while parsing offset central directory. Cannot update already existing file header");
                                    }
                                    if (v7 >= 0) {
                                        v9.seek(v7);
                                    }
                                }
                            }
                        } else {
                            p17.setResult(3);
                            p17.setState(0);
                            if (v9 != null) {
                                try {
                                    v9.close();
                                } catch (net.lingala.zip4j.exception.ZipException v11) {
                                }
                            }
                            return;
                        }
                    }
                    v6++;
                }
            } catch (Exception v2) {
                throw new net.lingala.zip4j.exception.ZipException(v2);
            } catch (net.lingala.zip4j.exception.ZipException v11_30) {
                if (v9 != null) {
                    try {
                        v9.close();
                    } catch (String v12) {
                    }
                }
                throw v11_30;
            }
            if (v9 != null) {
                try {
                    v9.close();
                } catch (net.lingala.zip4j.exception.ZipException v11) {
                }
            }
        }
        return;
    }

    public void addFiles(java.util.ArrayList p7, net.lingala.zip4j.model.ZipParameters p8, net.lingala.zip4j.progress.ProgressMonitor p9, boolean p10)
    {
        if ((p7 != null) && (p8 != null)) {
            if (p7.size() > 0) {
                p9.setCurrentOperation(0);
                p9.setState(1);
                p9.setResult(1);
                if (!p10) {
                    this.initAddFiles(p7, p8, p9);
                } else {
                    p9.setTotalWork(this.calculateTotalWork(p7, p8));
                    p9.setFileName(((java.io.File) p7.get(0)).getAbsolutePath());
                    new net.lingala.zip4j.zip.ZipEngine$1(this, "Zip4j", p7, p8, p9).start();
                }
                return;
            } else {
                throw new net.lingala.zip4j.exception.ZipException("no files to add");
            }
        } else {
            throw new net.lingala.zip4j.exception.ZipException("one of the input parameters is null when adding files");
        }
    }

    public void addFolderToZip(java.io.File p6, net.lingala.zip4j.model.ZipParameters p7, net.lingala.zip4j.progress.ProgressMonitor p8, boolean p9)
    {
        if ((p6 != null) && (p7 != null)) {
            if (net.lingala.zip4j.util.Zip4jUtil.checkFileExists(p6.getAbsolutePath())) {
                if (p6.isDirectory()) {
                    if (net.lingala.zip4j.util.Zip4jUtil.checkFileReadAccess(p6.getAbsolutePath())) {
                        String v1;
                        if (!p7.isIncludeRootFolder()) {
                            v1 = p6.getAbsolutePath();
                        } else {
                            if (p6.getAbsolutePath() == null) {
                                if (p6.getParentFile() == null) {
                                    v1 = "";
                                } else {
                                    v1 = p6.getParentFile().getAbsolutePath();
                                }
                            } else {
                                if (p6.getAbsoluteFile().getParentFile() == null) {
                                    v1 = "";
                                } else {
                                    v1 = p6.getAbsoluteFile().getParentFile().getAbsolutePath();
                                }
                            }
                        }
                        p7.setDefaultFolderPath(v1);
                        java.util.ArrayList v0_0 = net.lingala.zip4j.util.Zip4jUtil.getFilesInDirectoryRec(p6, p7.isReadHiddenFiles());
                        if (p7.isIncludeRootFolder()) {
                            if (v0_0 == null) {
                                v0_0 = new java.util.ArrayList();
                            }
                            v0_0.add(p6);
                        }
                        this.addFiles(v0_0, p7, p8, p9);
                        return;
                    } else {
                        throw new net.lingala.zip4j.exception.ZipException(new StringBuilder().append("cannot read folder: ").append(p6.getAbsolutePath()).toString());
                    }
                } else {
                    throw new net.lingala.zip4j.exception.ZipException("input file is not a folder, user addFileToZip method to add files");
                }
            } else {
                throw new net.lingala.zip4j.exception.ZipException("input folder does not exist");
            }
        } else {
            throw new net.lingala.zip4j.exception.ZipException("one of the input parameters is null, cannot add folder to zip");
        }
    }

    public void addStreamToZip(java.io.InputStream p11, net.lingala.zip4j.model.ZipParameters p12)
    {
        if ((p11 != null) && (p12 != null)) {
            net.lingala.zip4j.io.ZipOutputStream v2 = 0;
            try {
                this.checkParameters(p12);
                boolean v1 = net.lingala.zip4j.util.Zip4jUtil.checkFileExists(this.zipModel.getZipFile());
                net.lingala.zip4j.io.SplitOutputStream v6_1 = new net.lingala.zip4j.io.SplitOutputStream(new java.io.File(this.zipModel.getZipFile()), this.zipModel.getSplitLength());
                net.lingala.zip4j.io.ZipOutputStream v3_1 = new net.lingala.zip4j.io.ZipOutputStream(v6_1, this.zipModel);
            } catch (Exception v0_0) {
                throw new net.lingala.zip4j.exception.ZipException(v0_0);
            } catch (Exception v0_1) {
                throw v0_1;
            } catch (java.io.IOException v7_22) {
                if (v2 != null) {
                    try {
                        v2.close();
                    } catch (java.io.IOException v8) {
                    }
                }
                throw v7_22;
            }
            try {
                if (v1) {
                    if (this.zipModel.getEndCentralDirRecord() != null) {
                        v6_1.seek(this.zipModel.getEndCentralDirRecord().getOffsetOfStartOfCentralDir());
                    } else {
                        throw new net.lingala.zip4j.exception.ZipException("invalid end of central directory record");
                    }
                }
            } catch (Exception v0_0) {
                v2 = v3_1;
            } catch (java.io.IOException v7_22) {
                v2 = v3_1;
            } catch (Exception v0_1) {
                v2 = v3_1;
            }
            byte[] v4 = new byte[4096];
            v3_1.putNextEntry(0, p12);
            if ((!p12.getFileNameInZip().endsWith("/")) && (!p12.getFileNameInZip().endsWith("\\"))) {
                while(true) {
                    int v5 = p11.read(v4);
                    if (v5 == -1) {
                        break;
                    }
                    v3_1.write(v4, 0, v5);
                }
            }
            v3_1.closeEntry();
            v3_1.finish();
            if (v3_1 != null) {
                try {
                    v3_1.close();
                } catch (java.io.IOException v7) {
                }
            }
            return;
        } else {
            throw new net.lingala.zip4j.exception.ZipException("one of the input parameters is null, cannot add stream to zip");
        }
    }
}
