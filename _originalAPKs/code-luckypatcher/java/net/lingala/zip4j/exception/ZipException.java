package net.lingala.zip4j.exception;
public class ZipException extends java.lang.Exception {
    private static final long serialVersionUID = 1;
    private int code;

    public ZipException()
    {
        this.code = -1;
        return;
    }

    public ZipException(String p2)
    {
        this(p2);
        this.code = -1;
        return;
    }

    public ZipException(String p2, int p3)
    {
        this(p2);
        this.code = -1;
        this.code = p3;
        return;
    }

    public ZipException(String p2, Throwable p3)
    {
        this(p2, p3);
        this.code = -1;
        return;
    }

    public ZipException(String p2, Throwable p3, int p4)
    {
        this(p2, p3);
        this.code = -1;
        this.code = p4;
        return;
    }

    public ZipException(Throwable p2)
    {
        this(p2);
        this.code = -1;
        return;
    }

    public ZipException(Throwable p2, int p3)
    {
        this(p2);
        this.code = -1;
        this.code = p3;
        return;
    }

    public int getCode()
    {
        return this.code;
    }
}
