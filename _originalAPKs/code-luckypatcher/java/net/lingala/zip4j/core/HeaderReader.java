package net.lingala.zip4j.core;
public class HeaderReader {
    private java.io.RandomAccessFile zip4jRaf;
    private net.lingala.zip4j.model.ZipModel zipModel;

    public HeaderReader(java.io.RandomAccessFile p2)
    {
        this.zip4jRaf = 0;
        this.zip4jRaf = p2;
        return;
    }

    private byte[] getLongByteFromIntByte(byte[] p8)
    {
        if (p8 != null) {
            if (p8.length == 4) {
                byte[] v0 = new byte[8];
                v0[0] = p8[0];
                v0[1] = p8[1];
                v0[2] = p8[2];
                v0[3] = p8[3];
                v0[4] = 0;
                v0[5] = 0;
                v0[6] = 0;
                v0[7] = 0;
                return v0;
            } else {
                throw new net.lingala.zip4j.exception.ZipException("invalid byte length, cannot expand to 8 bytes");
            }
        } else {
            throw new net.lingala.zip4j.exception.ZipException("input parameter is null, cannot expand to 8 bytes");
        }
    }

    private net.lingala.zip4j.model.AESExtraDataRecord readAESExtraDataRecord(java.util.ArrayList p12)
    {
        net.lingala.zip4j.model.AESExtraDataRecord v1_0 = 0;
        if (p12 != null) {
            int v3 = 0;
            while (v3 < p12.size()) {
                net.lingala.zip4j.model.ExtraDataRecord v2_1 = ((net.lingala.zip4j.model.ExtraDataRecord) p12.get(v3));
                if ((v2_1 == null) || (v2_1.getHeader() != 39169)) {
                    v3++;
                } else {
                    if (v2_1.getData() != null) {
                        v1_0 = new net.lingala.zip4j.model.AESExtraDataRecord();
                        v1_0.setSignature(39169);
                        v1_0.setDataSize(v2_1.getSizeOfData());
                        byte[] v0 = v2_1.getData();
                        v1_0.setVersionNumber(net.lingala.zip4j.util.Raw.readShortLittleEndian(v0, 0));
                        byte[] v4 = new byte[2];
                        System.arraycopy(v0, 2, v4, 0, 2);
                        v1_0.setVendorID(new String(v4));
                        v1_0.setAesStrength((v0[4] & 255));
                        v1_0.setCompressionMethod(net.lingala.zip4j.util.Raw.readShortLittleEndian(v0, 5));
                        break;
                    } else {
                        throw new net.lingala.zip4j.exception.ZipException("corrput AES extra data records");
                    }
                }
            }
        }
        return v1_0;
    }

    private void readAndSaveAESExtraDataRecord(net.lingala.zip4j.model.FileHeader p4)
    {
        if (p4 != null) {
            if ((p4.getExtraDataRecords() != null) && (p4.getExtraDataRecords().size() > 0)) {
                net.lingala.zip4j.model.AESExtraDataRecord v0 = this.readAESExtraDataRecord(p4.getExtraDataRecords());
                if (v0 != null) {
                    p4.setAesExtraDataRecord(v0);
                    p4.setEncryptionMethod(99);
                }
            }
            return;
        } else {
            throw new net.lingala.zip4j.exception.ZipException("file header is null in reading Zip64 Extended Info");
        }
    }

    private void readAndSaveAESExtraDataRecord(net.lingala.zip4j.model.LocalFileHeader p4)
    {
        if (p4 != null) {
            if ((p4.getExtraDataRecords() != null) && (p4.getExtraDataRecords().size() > 0)) {
                net.lingala.zip4j.model.AESExtraDataRecord v0 = this.readAESExtraDataRecord(p4.getExtraDataRecords());
                if (v0 != null) {
                    p4.setAesExtraDataRecord(v0);
                    p4.setEncryptionMethod(99);
                }
            }
            return;
        } else {
            throw new net.lingala.zip4j.exception.ZipException("file header is null in reading Zip64 Extended Info");
        }
    }

    private void readAndSaveExtraDataRecord(net.lingala.zip4j.model.FileHeader p4)
    {
        if (this.zip4jRaf != null) {
            if (p4 != null) {
                int v0 = p4.getExtraFieldLength();
                if (v0 > 0) {
                    p4.setExtraDataRecords(this.readExtraDataRecords(v0));
                }
                return;
            } else {
                throw new net.lingala.zip4j.exception.ZipException("file header is null");
            }
        } else {
            throw new net.lingala.zip4j.exception.ZipException("invalid file handler when trying to read extra data record");
        }
    }

    private void readAndSaveExtraDataRecord(net.lingala.zip4j.model.LocalFileHeader p4)
    {
        if (this.zip4jRaf != null) {
            if (p4 != null) {
                int v0 = p4.getExtraFieldLength();
                if (v0 > 0) {
                    p4.setExtraDataRecords(this.readExtraDataRecords(v0));
                }
                return;
            } else {
                throw new net.lingala.zip4j.exception.ZipException("file header is null");
            }
        } else {
            throw new net.lingala.zip4j.exception.ZipException("invalid file handler when trying to read extra data record");
        }
    }

    private void readAndSaveZip64ExtendedInfo(net.lingala.zip4j.model.FileHeader p13)
    {
        if (p13 != null) {
            if ((p13.getExtraDataRecords() != null) && (p13.getExtraDataRecords().size() > 0)) {
                net.lingala.zip4j.model.Zip64ExtendedInfo v9 = this.readZip64ExtendedInfo(p13.getExtraDataRecords(), p13.getUncompressedSize(), p13.getCompressedSize(), p13.getOffsetLocalHeader(), p13.getDiskNumberStart());
                if (v9 != null) {
                    p13.setZip64ExtendedInfo(v9);
                    if (v9.getUnCompressedSize() != -1) {
                        p13.setUncompressedSize(v9.getUnCompressedSize());
                    }
                    if (v9.getCompressedSize() != -1) {
                        p13.setCompressedSize(v9.getCompressedSize());
                    }
                    if (v9.getOffsetLocalHeader() != -1) {
                        p13.setOffsetLocalHeader(v9.getOffsetLocalHeader());
                    }
                    if (v9.getDiskNumberStart() != -1) {
                        p13.setDiskNumberStart(v9.getDiskNumberStart());
                    }
                }
            }
            return;
        } else {
            throw new net.lingala.zip4j.exception.ZipException("file header is null in reading Zip64 Extended Info");
        }
    }

    private void readAndSaveZip64ExtendedInfo(net.lingala.zip4j.model.LocalFileHeader p11)
    {
        if (p11 != null) {
            if ((p11.getExtraDataRecords() != null) && (p11.getExtraDataRecords().size() > 0)) {
                net.lingala.zip4j.model.Zip64ExtendedInfo v9 = this.readZip64ExtendedInfo(p11.getExtraDataRecords(), p11.getUncompressedSize(), p11.getCompressedSize(), -1, -1);
                if (v9 != null) {
                    p11.setZip64ExtendedInfo(v9);
                    if (v9.getUnCompressedSize() != -1) {
                        p11.setUncompressedSize(v9.getUnCompressedSize());
                    }
                    if (v9.getCompressedSize() != -1) {
                        p11.setCompressedSize(v9.getCompressedSize());
                    }
                }
            }
            return;
        } else {
            throw new net.lingala.zip4j.exception.ZipException("file header is null in reading Zip64 Extended Info");
        }
    }

    private net.lingala.zip4j.model.CentralDirectory readCentralDirectory()
    {
        if (this.zip4jRaf != null) {
            if (this.zipModel.getEndCentralDirRecord() != null) {
                try {
                    net.lingala.zip4j.model.CentralDirectory v4_1 = new net.lingala.zip4j.model.CentralDirectory();
                    java.util.ArrayList v12_1 = new java.util.ArrayList();
                    net.lingala.zip4j.model.EndCentralDirRecord v7 = this.zipModel.getEndCentralDirRecord();
                    long v20 = v7.getOffsetOfStartOfCentralDir();
                    int v3 = v7.getTotNoOfEntriesInCentralDir();
                } catch (java.io.IOException v6) {
                    int v27_118 = new net.lingala.zip4j.exception.ZipException;
                    v27_118(v6);
                    throw v27_118;
                }
                if (this.zipModel.isZip64Format()) {
                    v20 = this.zipModel.getZip64EndCentralDirRecord().getOffsetStartCenDirWRTStartDiskNo();
                    v3 = ((int) this.zipModel.getZip64EndCentralDirRecord().getTotNoOfEntriesInCentralDir());
                }
                this.zip4jRaf.seek(v20);
                int v0_17 = new byte[4];
                byte[] v18 = v0_17;
                int v0_19 = new byte[2];
                byte[] v23 = v0_19;
                int v17 = 0;
                while (v17 < v3) {
                    net.lingala.zip4j.model.FileHeader v11_1 = new net.lingala.zip4j.model.FileHeader();
                    this.readIntoBuff(this.zip4jRaf, v18);
                    int v25_1 = net.lingala.zip4j.util.Raw.readIntLittleEndian(v18, 0);
                    if (((long) v25_1) == 33639248) {
                        int v27_37;
                        v11_1.setSignature(v25_1);
                        this.readIntoBuff(this.zip4jRaf, v23);
                        v11_1.setVersionMadeBy(net.lingala.zip4j.util.Raw.readShortLittleEndian(v23, 0));
                        this.readIntoBuff(this.zip4jRaf, v23);
                        v11_1.setVersionNeededToExtract(net.lingala.zip4j.util.Raw.readShortLittleEndian(v23, 0));
                        this.readIntoBuff(this.zip4jRaf, v23);
                        if ((net.lingala.zip4j.util.Raw.readShortLittleEndian(v23, 0) & 2048) == 0) {
                            v27_37 = 0;
                        } else {
                            v27_37 = 1;
                        }
                        v11_1.setFileNameUTF8Encoded(v27_37);
                        byte v16 = v23[0];
                        if ((v16 & 1) != 0) {
                            v11_1.setEncrypted(1);
                        }
                        int v27_44;
                        v11_1.setGeneralPurposeFlag(((byte[]) ((byte[]) v23.clone())));
                        if ((v16 >> 3) != 1) {
                            v27_44 = 0;
                        } else {
                            v27_44 = 1;
                        }
                        v11_1.setDataDescriptorExists(v27_44);
                        this.readIntoBuff(this.zip4jRaf, v23);
                        v11_1.setCompressionMethod(net.lingala.zip4j.util.Raw.readShortLittleEndian(v23, 0));
                        this.readIntoBuff(this.zip4jRaf, v18);
                        v11_1.setLastModFileTime(net.lingala.zip4j.util.Raw.readIntLittleEndian(v18, 0));
                        this.readIntoBuff(this.zip4jRaf, v18);
                        v11_1.setCrc32(((long) net.lingala.zip4j.util.Raw.readIntLittleEndian(v18, 0)));
                        v11_1.setCrcBuff(((byte[]) ((byte[]) v18.clone())));
                        this.readIntoBuff(this.zip4jRaf, v18);
                        v11_1.setCompressedSize(net.lingala.zip4j.util.Raw.readLongLittleEndian(this.getLongByteFromIntByte(v18), 0));
                        this.readIntoBuff(this.zip4jRaf, v18);
                        v11_1.setUncompressedSize(net.lingala.zip4j.util.Raw.readLongLittleEndian(this.getLongByteFromIntByte(v18), 0));
                        this.readIntoBuff(this.zip4jRaf, v23);
                        int v15 = net.lingala.zip4j.util.Raw.readShortLittleEndian(v23, 0);
                        v11_1.setFileNameLength(v15);
                        this.readIntoBuff(this.zip4jRaf, v23);
                        v11_1.setExtraFieldLength(net.lingala.zip4j.util.Raw.readShortLittleEndian(v23, 0));
                        this.readIntoBuff(this.zip4jRaf, v23);
                        int v10 = net.lingala.zip4j.util.Raw.readShortLittleEndian(v23, 0);
                        int v27_70 = new String;
                        v27_70(v23);
                        v11_1.setFileComment(v27_70);
                        this.readIntoBuff(this.zip4jRaf, v23);
                        v11_1.setDiskNumberStart(net.lingala.zip4j.util.Raw.readShortLittleEndian(v23, 0));
                        this.readIntoBuff(this.zip4jRaf, v23);
                        v11_1.setInternalFileAttr(((byte[]) ((byte[]) v23.clone())));
                        this.readIntoBuff(this.zip4jRaf, v18);
                        v11_1.setExternalFileAttr(((byte[]) ((byte[]) v18.clone())));
                        this.readIntoBuff(this.zip4jRaf, v18);
                        v11_1.setOffsetLocalHeader((net.lingala.zip4j.util.Raw.readLongLittleEndian(this.getLongByteFromIntByte(v18), 0) & 2.1219957905e-314));
                        if (v15 <= 0) {
                            v11_1.setFileName(0);
                        } else {
                            String v13_0;
                            byte[] v14 = new byte[v15];
                            this.readIntoBuff(this.zip4jRaf, v14);
                            if (!net.lingala.zip4j.util.Zip4jUtil.isStringNotNullAndNotEmpty(this.zipModel.getFileNameCharset())) {
                                v13_0 = net.lingala.zip4j.util.Zip4jUtil.decodeFileName(v14, v11_1.isFileNameUTF8Encoded());
                            } else {
                                v13_0 = new String(v14, this.zipModel.getFileNameCharset());
                            }
                            if (v13_0 != null) {
                                if (v13_0.indexOf(new StringBuilder().append(":").append(System.getProperty("file.separator")).toString()) >= 0) {
                                    v13_0 = v13_0.substring((v13_0.indexOf(new StringBuilder().append(":").append(System.getProperty("file.separator")).toString()) + 2));
                                }
                                int v27_111;
                                v11_1.setFileName(v13_0);
                                if ((!v13_0.endsWith("/")) && (!v13_0.endsWith("\\"))) {
                                    v27_111 = 0;
                                } else {
                                    v27_111 = 1;
                                }
                                v11_1.setDirectory(v27_111);
                            } else {
                                throw new net.lingala.zip4j.exception.ZipException("fileName is null when reading central directory");
                            }
                        }
                        this.readAndSaveExtraDataRecord(v11_1);
                        this.readAndSaveZip64ExtendedInfo(v11_1);
                        this.readAndSaveAESExtraDataRecord(v11_1);
                        if (v10 > 0) {
                            byte[] v9 = new byte[v10];
                            this.readIntoBuff(this.zip4jRaf, v9);
                            int v27_113 = new String;
                            v27_113(v9);
                            v11_1.setFileComment(v27_113);
                        }
                        v12_1.add(v11_1);
                        v17++;
                    } else {
                        throw new net.lingala.zip4j.exception.ZipException(new StringBuilder().append("Expected central directory entry not found (#").append((v17 + 1)).append(")").toString());
                    }
                }
                v4_1.setFileHeaders(v12_1);
                net.lingala.zip4j.model.DigitalSignature v5_1 = new net.lingala.zip4j.model.DigitalSignature();
                this.readIntoBuff(this.zip4jRaf, v18);
                int v25_0 = net.lingala.zip4j.util.Raw.readIntLittleEndian(v18, 0);
                if (((long) v25_0) == 84233040) {
                    v5_1.setHeaderSignature(v25_0);
                    this.readIntoBuff(this.zip4jRaf, v23);
                    int v26 = net.lingala.zip4j.util.Raw.readShortLittleEndian(v23, 0);
                    v5_1.setSizeOfData(v26);
                    if (v26 > 0) {
                        int v0_36 = new byte[v26];
                        byte[] v24 = v0_36;
                        this.readIntoBuff(this.zip4jRaf, v24);
                        int v27_22 = new String;
                        v27_22(v24);
                        v5_1.setSignatureData(v27_22);
                    }
                }
                return v4_1;
            } else {
                throw new net.lingala.zip4j.exception.ZipException("EndCentralRecord was null, maybe a corrupt zip file");
            }
        } else {
            throw new net.lingala.zip4j.exception.ZipException("random access file was null", 3);
        }
    }

    private net.lingala.zip4j.model.EndCentralDirRecord readEndOfCentralDirectoryRecord()
    {
        if (this.zip4jRaf != null) {
            try {
                byte[] v8 = new byte[4];
                long v12_0 = (this.zip4jRaf.length() - 22);
                net.lingala.zip4j.model.EndCentralDirRecord v9_1 = new net.lingala.zip4j.model.EndCentralDirRecord();
                int v5 = 0;
                long v14 = v12_0;
            } catch (java.io.IOException v7) {
                net.lingala.zip4j.model.ZipModel v17_44 = new net.lingala.zip4j.exception.ZipException;
                v17_44("Probably not a zip file or a corrupted zip file", v7, 4);
                throw v17_44;
            }
            while(true) {
                long v12_1 = (v14 - 1);
                this.zip4jRaf.seek(v14);
                v5++;
                if ((((long) net.lingala.zip4j.util.Raw.readLeInt(this.zip4jRaf, v8)) == 101010256) || (v5 > 3000)) {
                    break;
                }
                v14 = v12_1;
            }
            if (((long) net.lingala.zip4j.util.Raw.readIntLittleEndian(v8, 0)) == 101010256) {
                byte[] v10 = new byte[4];
                net.lingala.zip4j.model.ZipModel v0_19 = new byte[2];
                byte[] v16 = v0_19;
                v9_1.setSignature(101010256);
                this.readIntoBuff(this.zip4jRaf, v16);
                v9_1.setNoOfThisDisk(net.lingala.zip4j.util.Raw.readShortLittleEndian(v16, 0));
                this.readIntoBuff(this.zip4jRaf, v16);
                v9_1.setNoOfThisDiskStartOfCentralDir(net.lingala.zip4j.util.Raw.readShortLittleEndian(v16, 0));
                this.readIntoBuff(this.zip4jRaf, v16);
                v9_1.setTotNoOfEntriesInCentralDirOnThisDisk(net.lingala.zip4j.util.Raw.readShortLittleEndian(v16, 0));
                this.readIntoBuff(this.zip4jRaf, v16);
                v9_1.setTotNoOfEntriesInCentralDir(net.lingala.zip4j.util.Raw.readShortLittleEndian(v16, 0));
                this.readIntoBuff(this.zip4jRaf, v10);
                v9_1.setSizeOfCentralDir(net.lingala.zip4j.util.Raw.readIntLittleEndian(v10, 0));
                this.readIntoBuff(this.zip4jRaf, v10);
                v9_1.setOffsetOfStartOfCentralDir(net.lingala.zip4j.util.Raw.readLongLittleEndian(this.getLongByteFromIntByte(v10), 0));
                this.readIntoBuff(this.zip4jRaf, v16);
                int v4 = net.lingala.zip4j.util.Raw.readShortLittleEndian(v16, 0);
                v9_1.setCommentLength(v4);
                if (v4 <= 0) {
                    v9_1.setComment(0);
                } else {
                    byte[] v3 = new byte[v4];
                    this.readIntoBuff(this.zip4jRaf, v3);
                    net.lingala.zip4j.model.ZipModel v17_39 = new String;
                    v17_39(v3);
                    v9_1.setComment(v17_39);
                    v9_1.setCommentBytes(v3);
                }
                if (v9_1.getNoOfThisDisk() <= 0) {
                    this.zipModel.setSplitArchive(0);
                } else {
                    this.zipModel.setSplitArchive(1);
                }
                return v9_1;
            } else {
                throw new net.lingala.zip4j.exception.ZipException("zip headers not found. probably not a zip file");
            }
        } else {
            throw new net.lingala.zip4j.exception.ZipException("random access file was null", 3);
        }
    }

    private java.util.ArrayList readExtraDataRecords(int p12)
    {
        net.lingala.zip4j.exception.ZipException v3_1;
        if (p12 > 0) {
            try {
                byte[] v5 = new byte[p12];
                this.zip4jRaf.read(v5);
                int v0_0 = 0;
                v3_1 = new java.util.ArrayList();
            } catch (java.io.IOException v2) {
                throw new net.lingala.zip4j.exception.ZipException(v2);
            }
            while (v0_0 < p12) {
                net.lingala.zip4j.model.ExtraDataRecord v4_1 = new net.lingala.zip4j.model.ExtraDataRecord();
                v4_1.setHeader(((long) net.lingala.zip4j.util.Raw.readShortLittleEndian(v5, v0_0)));
                int v0_1 = (v0_0 + 2);
                short v7 = net.lingala.zip4j.util.Raw.readShortLittleEndian(v5, v0_1);
                if ((v7 + 2) > p12) {
                    v7 = net.lingala.zip4j.util.Raw.readShortBigEndian(v5, v0_1);
                    if ((v7 + 2) > p12) {
                        break;
                    }
                }
                v4_1.setSizeOfData(v7);
                int v0_2 = (v0_1 + 2);
                if (v7 > 0) {
                    byte[] v1 = new byte[v7];
                    System.arraycopy(v5, v0_2, v1, 0, v7);
                    v4_1.setData(v1);
                }
                v0_0 = (v0_2 + v7);
                v3_1.add(v4_1);
            }
            if (v3_1.size() <= 0) {
                v3_1 = 0;
            }
        } else {
            v3_1 = 0;
        }
        return v3_1;
    }

    private byte[] readIntoBuff(java.io.RandomAccessFile p4, byte[] p5)
    {
        try {
            if (p4.read(p5, 0, p5.length) == -1) {
                throw new net.lingala.zip4j.exception.ZipException("unexpected end of file when reading short buff");
            } else {
                return p5;
            }
        } catch (java.io.IOException v0) {
            throw new net.lingala.zip4j.exception.ZipException("IOException when reading short buff", v0);
        }
    }

    private net.lingala.zip4j.model.Zip64EndCentralDirLocator readZip64EndCentralDirLocator()
    {
        if (this.zip4jRaf != null) {
            try {
                int v4_1 = new net.lingala.zip4j.model.Zip64EndCentralDirLocator();
                this.setFilePointerToReadZip64EndCentralDirLoc();
                byte[] v1 = new byte[4];
                byte[] v2 = new byte[8];
                this.readIntoBuff(this.zip4jRaf, v1);
                int v3 = net.lingala.zip4j.util.Raw.readIntLittleEndian(v1, 0);
            } catch (Exception v0) {
                throw new net.lingala.zip4j.exception.ZipException(v0);
            }
            if (((long) v3) != 117853008) {
                this.zipModel.setZip64Format(0);
                v4_1 = 0;
            } else {
                this.zipModel.setZip64Format(1);
                v4_1.setSignature(((long) v3));
                this.readIntoBuff(this.zip4jRaf, v1);
                v4_1.setNoOfDiskStartOfZip64EndOfCentralDirRec(net.lingala.zip4j.util.Raw.readIntLittleEndian(v1, 0));
                this.readIntoBuff(this.zip4jRaf, v2);
                v4_1.setOffsetZip64EndOfCentralDirRec(net.lingala.zip4j.util.Raw.readLongLittleEndian(v2, 0));
                this.readIntoBuff(this.zip4jRaf, v1);
                v4_1.setTotNumberOfDiscs(net.lingala.zip4j.util.Raw.readIntLittleEndian(v1, 0));
            }
            return v4_1;
        } else {
            throw new net.lingala.zip4j.exception.ZipException("invalid file handler when trying to read Zip64EndCentralDirLocator");
        }
    }

    private net.lingala.zip4j.model.Zip64EndCentralDirRecord readZip64EndCentralDirRec()
    {
        if (this.zipModel.getZip64EndCentralDirLocator() != null) {
            long v6 = this.zipModel.getZip64EndCentralDirLocator().getOffsetZip64EndOfCentralDirRec();
            if (v6 >= 0) {
                try {
                    this.zip4jRaf.seek(v6);
                    net.lingala.zip4j.model.Zip64EndCentralDirRecord v10_1 = new net.lingala.zip4j.model.Zip64EndCentralDirRecord();
                    byte[] v8 = new byte[2];
                    byte[] v4 = new byte[4];
                    byte[] v5 = new byte[8];
                    this.readIntoBuff(this.zip4jRaf, v4);
                    int v9 = net.lingala.zip4j.util.Raw.readIntLittleEndian(v4, 0);
                } catch (java.io.IOException v0) {
                    throw new net.lingala.zip4j.exception.ZipException(v0);
                }
                if (((long) v9) == 101075792) {
                    v10_1.setSignature(((long) v9));
                    this.readIntoBuff(this.zip4jRaf, v5);
                    v10_1.setSizeOfZip64EndCentralDirRec(net.lingala.zip4j.util.Raw.readLongLittleEndian(v5, 0));
                    this.readIntoBuff(this.zip4jRaf, v8);
                    v10_1.setVersionMadeBy(net.lingala.zip4j.util.Raw.readShortLittleEndian(v8, 0));
                    this.readIntoBuff(this.zip4jRaf, v8);
                    v10_1.setVersionNeededToExtract(net.lingala.zip4j.util.Raw.readShortLittleEndian(v8, 0));
                    this.readIntoBuff(this.zip4jRaf, v4);
                    v10_1.setNoOfThisDisk(net.lingala.zip4j.util.Raw.readIntLittleEndian(v4, 0));
                    this.readIntoBuff(this.zip4jRaf, v4);
                    v10_1.setNoOfThisDiskStartOfCentralDir(net.lingala.zip4j.util.Raw.readIntLittleEndian(v4, 0));
                    this.readIntoBuff(this.zip4jRaf, v5);
                    v10_1.setTotNoOfEntriesInCentralDirOnThisDisk(net.lingala.zip4j.util.Raw.readLongLittleEndian(v5, 0));
                    this.readIntoBuff(this.zip4jRaf, v5);
                    v10_1.setTotNoOfEntriesInCentralDir(net.lingala.zip4j.util.Raw.readLongLittleEndian(v5, 0));
                    this.readIntoBuff(this.zip4jRaf, v5);
                    v10_1.setSizeOfCentralDir(net.lingala.zip4j.util.Raw.readLongLittleEndian(v5, 0));
                    this.readIntoBuff(this.zip4jRaf, v5);
                    v10_1.setOffsetStartCenDirWRTStartDiskNo(net.lingala.zip4j.util.Raw.readLongLittleEndian(v5, 0));
                    long v2 = (v10_1.getSizeOfZip64EndCentralDirRec() - 44);
                    if (v2 > 0) {
                        byte[] v1 = new byte[((int) v2)];
                        this.readIntoBuff(this.zip4jRaf, v1);
                        v10_1.setExtensibleDataSector(v1);
                    }
                    return v10_1;
                } else {
                    throw new net.lingala.zip4j.exception.ZipException("invalid signature for zip64 end of central directory record");
                }
            } else {
                throw new net.lingala.zip4j.exception.ZipException("invalid offset for start of end of central directory record");
            }
        } else {
            throw new net.lingala.zip4j.exception.ZipException("invalid zip64 end of central directory locator");
        }
    }

    private net.lingala.zip4j.model.Zip64ExtendedInfo readZip64ExtendedInfo(java.util.ArrayList p15, long p16, long p18, long p20, int p22)
    {
        int v3 = 0;
        while (v3 < p15.size()) {
            net.lingala.zip4j.model.ExtraDataRecord v2_1 = ((net.lingala.zip4j.model.ExtraDataRecord) p15.get(v3));
            if ((v2_1 == null) || (v2_1.getHeader() != 1)) {
                v3++;
            } else {
                net.lingala.zip4j.model.Zip64ExtendedInfo v9_1 = new net.lingala.zip4j.model.Zip64ExtendedInfo();
                byte[] v0 = v2_1.getData();
                if (v2_1.getSizeOfData() <= 0) {
                    break;
                }
                byte[] v5 = new byte[8];
                byte[] v4 = new byte[4];
                int v1 = 0;
                int v8 = 0;
                if (((65535 & p16) == 65535) && (0 < v2_1.getSizeOfData())) {
                    System.arraycopy(v0, 0, v5, 0, 8);
                    v9_1.setUnCompressedSize(net.lingala.zip4j.util.Raw.readLongLittleEndian(v5, 0));
                    v1 = (0 + 8);
                    v8 = 1;
                }
                if (((65535 & p18) == 65535) && (v1 < v2_1.getSizeOfData())) {
                    System.arraycopy(v0, v1, v5, 0, 8);
                    v9_1.setCompressedSize(net.lingala.zip4j.util.Raw.readLongLittleEndian(v5, 0));
                    v1 += 8;
                    v8 = 1;
                }
                if (((65535 & p20) == 65535) && (v1 < v2_1.getSizeOfData())) {
                    System.arraycopy(v0, v1, v5, 0, 8);
                    v9_1.setOffsetLocalHeader(net.lingala.zip4j.util.Raw.readLongLittleEndian(v5, 0));
                    v1 += 8;
                    v8 = 1;
                }
                if (((65535 & p22) == 65535) && (v1 < v2_1.getSizeOfData())) {
                    System.arraycopy(v0, v1, v4, 0, 4);
                    v9_1.setDiskNumberStart(net.lingala.zip4j.util.Raw.readIntLittleEndian(v4, 0));
                    v8 = 1;
                }
                if (v8 == 0) {
                    break;
                }
            }
            return v9_1;
        }
        v9_1 = 0;
        return v9_1;
    }

    private void setFilePointerToReadZip64EndCentralDirLoc()
    {
        try {
            byte[] v1 = new byte[4];
            long v4 = (this.zip4jRaf.length() - 22);
        } catch (java.io.IOException v0) {
            throw new net.lingala.zip4j.exception.ZipException(v0);
        }
        while(true) {
            long v2_1 = (v4 - 1);
            this.zip4jRaf.seek(v4);
            if (((long) net.lingala.zip4j.util.Raw.readLeInt(this.zip4jRaf, v1)) == 101010256) {
                break;
            }
            v4 = v2_1;
        }
        this.zip4jRaf.seek((((((this.zip4jRaf.getFilePointer() - 4) - 4) - 8) - 4) - 4));
        return;
    }

    public net.lingala.zip4j.model.ZipModel readAllHeaders()
    {
        return this.readAllHeaders(0);
    }

    public net.lingala.zip4j.model.ZipModel readAllHeaders(String p3)
    {
        this.zipModel = new net.lingala.zip4j.model.ZipModel();
        this.zipModel.setFileNameCharset(p3);
        this.zipModel.setEndCentralDirRecord(this.readEndOfCentralDirectoryRecord());
        this.zipModel.setZip64EndCentralDirLocator(this.readZip64EndCentralDirLocator());
        if (this.zipModel.isZip64Format()) {
            this.zipModel.setZip64EndCentralDirRecord(this.readZip64EndCentralDirRec());
            if ((this.zipModel.getZip64EndCentralDirRecord() == null) || (this.zipModel.getZip64EndCentralDirRecord().getNoOfThisDisk() <= 0)) {
                this.zipModel.setSplitArchive(0);
            } else {
                this.zipModel.setSplitArchive(1);
            }
        }
        this.zipModel.setCentralDirectory(this.readCentralDirectory());
        return this.zipModel;
    }

    public net.lingala.zip4j.model.LocalFileHeader readLocalFileHeader(net.lingala.zip4j.model.FileHeader p25)
    {
        if ((p25 != null) && (this.zip4jRaf != null)) {
            long v12 = p25.getOffsetLocalHeader();
            if ((p25.getZip64ExtendedInfo() != null) && (p25.getZip64ExtendedInfo().getOffsetLocalHeader() > 0)) {
                v12 = p25.getOffsetLocalHeader();
            }
            if (v12 >= 0) {
                try {
                    this.zip4jRaf.seek(v12);
                    net.lingala.zip4j.model.LocalFileHeader v14_1 = new net.lingala.zip4j.model.LocalFileHeader();
                    long v0_6 = new byte[2];
                    byte[] v17 = v0_6;
                    byte[] v10 = new byte[4];
                    this.readIntoBuff(this.zip4jRaf, v10);
                    int v18 = net.lingala.zip4j.util.Raw.readIntLittleEndian(v10, 0);
                } catch (java.io.IOException v4) {
                    long v20_89 = new net.lingala.zip4j.exception.ZipException;
                    v20_89(v4);
                    throw v20_89;
                }
                if (((long) v18) == 67324752) {
                    long v20_21;
                    v14_1.setSignature(v18);
                    this.readIntoBuff(this.zip4jRaf, v17);
                    v14_1.setVersionNeededToExtract(net.lingala.zip4j.util.Raw.readShortLittleEndian(v17, 0));
                    this.readIntoBuff(this.zip4jRaf, v17);
                    if ((net.lingala.zip4j.util.Raw.readShortLittleEndian(v17, 0) & 2048) == 0) {
                        v20_21 = 0;
                    } else {
                        v20_21 = 1;
                    }
                    v14_1.setFileNameUTF8Encoded(v20_21);
                    byte v9 = v17[0];
                    if ((v9 & 1) != 0) {
                        v14_1.setEncrypted(1);
                    }
                    v14_1.setGeneralPurposeFlag(v17);
                    String v3 = Integer.toBinaryString(v9);
                    if (v3.length() >= 4) {
                        long v20_27;
                        if (v3.charAt(3) != 49) {
                            v20_27 = 0;
                        } else {
                            v20_27 = 1;
                        }
                        v14_1.setDataDescriptorExists(v20_27);
                    }
                    this.readIntoBuff(this.zip4jRaf, v17);
                    v14_1.setCompressionMethod(net.lingala.zip4j.util.Raw.readShortLittleEndian(v17, 0));
                    this.readIntoBuff(this.zip4jRaf, v10);
                    v14_1.setLastModFileTime(net.lingala.zip4j.util.Raw.readIntLittleEndian(v10, 0));
                    this.readIntoBuff(this.zip4jRaf, v10);
                    v14_1.setCrc32(((long) net.lingala.zip4j.util.Raw.readIntLittleEndian(v10, 0)));
                    v14_1.setCrcBuff(((byte[]) ((byte[]) v10.clone())));
                    this.readIntoBuff(this.zip4jRaf, v10);
                    v14_1.setCompressedSize(net.lingala.zip4j.util.Raw.readLongLittleEndian(this.getLongByteFromIntByte(v10), 0));
                    this.readIntoBuff(this.zip4jRaf, v10);
                    v14_1.setUncompressedSize(net.lingala.zip4j.util.Raw.readLongLittleEndian(this.getLongByteFromIntByte(v10), 0));
                    this.readIntoBuff(this.zip4jRaf, v17);
                    int v8 = net.lingala.zip4j.util.Raw.readShortLittleEndian(v17, 0);
                    v14_1.setFileNameLength(v8);
                    this.readIntoBuff(this.zip4jRaf, v17);
                    int v5 = net.lingala.zip4j.util.Raw.readShortLittleEndian(v17, 0);
                    v14_1.setExtraFieldLength(v5);
                    int v11_10 = ((((((((((0 + 4) + 2) + 2) + 2) + 4) + 4) + 4) + 4) + 2) + 2);
                    if (v8 <= 0) {
                        v14_1.setFileName(0);
                    } else {
                        byte[] v7 = new byte[v8];
                        this.readIntoBuff(this.zip4jRaf, v7);
                        String v6 = net.lingala.zip4j.util.Zip4jUtil.decodeFileName(v7, v14_1.isFileNameUTF8Encoded());
                        if (v6 != null) {
                            if (v6.indexOf(new StringBuilder().append(":").append(System.getProperty("file.separator")).toString()) >= 0) {
                                v6 = v6.substring((v6.indexOf(new StringBuilder().append(":").append(System.getProperty("file.separator")).toString()) + 2));
                            }
                            v14_1.setFileName(v6);
                            v11_10 = (v8 + 30);
                        } else {
                            throw new net.lingala.zip4j.exception.ZipException("file name is null, cannot assign file name to local file header");
                        }
                    }
                    this.readAndSaveExtraDataRecord(v14_1);
                    v14_1.setOffsetStartOfData((((long) (v11_10 + v5)) + v12));
                    v14_1.setPassword(p25.getPassword());
                    this.readAndSaveZip64ExtendedInfo(v14_1);
                    this.readAndSaveAESExtraDataRecord(v14_1);
                    if ((v14_1.isEncrypted()) && (v14_1.getEncryptionMethod() != 99)) {
                        if ((v9 & 64) != 64) {
                            v14_1.setEncryptionMethod(0);
                        } else {
                            v14_1.setEncryptionMethod(1);
                        }
                    }
                    if (v14_1.getCrc32() <= 0) {
                        v14_1.setCrc32(p25.getCrc32());
                        v14_1.setCrcBuff(p25.getCrcBuff());
                    }
                    if (v14_1.getCompressedSize() <= 0) {
                        v14_1.setCompressedSize(p25.getCompressedSize());
                    }
                    if (v14_1.getUncompressedSize() <= 0) {
                        v14_1.setUncompressedSize(p25.getUncompressedSize());
                    }
                    return v14_1;
                } else {
                    throw new net.lingala.zip4j.exception.ZipException(new StringBuilder().append("invalid local header signature for file: ").append(p25.getFileName()).toString());
                }
            } else {
                throw new net.lingala.zip4j.exception.ZipException("invalid local header offset");
            }
        } else {
            throw new net.lingala.zip4j.exception.ZipException("invalid read parameters for local header");
        }
    }
}
