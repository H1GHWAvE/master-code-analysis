package net.lingala.zip4j.io;
public abstract class BaseInputStream extends java.io.InputStream {

    public BaseInputStream()
    {
        return;
    }

    public int available()
    {
        return 0;
    }

    public net.lingala.zip4j.unzip.UnzipEngine getUnzipEngine()
    {
        return 0;
    }

    public int read()
    {
        return 0;
    }

    public void seek(long p1)
    {
        return;
    }
}
