package net.lingala.zip4j.io;
public class PartInputStream extends net.lingala.zip4j.io.BaseInputStream {
    private byte[] aesBlockByte;
    private int aesBytesReturned;
    private long bytesRead;
    private int count;
    private net.lingala.zip4j.crypto.IDecrypter decrypter;
    private boolean isAESEncryptedFile;
    private long length;
    private byte[] oneByteBuff;
    private java.io.RandomAccessFile raf;
    private net.lingala.zip4j.unzip.UnzipEngine unzipEngine;

    public PartInputStream(java.io.RandomAccessFile p5, long p6, long p8, net.lingala.zip4j.unzip.UnzipEngine p10)
    {
        int v0 = 1;
        int v2_0 = new byte[1];
        this.oneByteBuff = v2_0;
        int v2_2 = new byte[16];
        this.aesBlockByte = v2_2;
        this.aesBytesReturned = 0;
        this.isAESEncryptedFile = 0;
        this.count = -1;
        this.raf = p5;
        this.unzipEngine = p10;
        this.decrypter = p10.getDecrypter();
        this.bytesRead = 0;
        this.length = p8;
        if ((!p10.getFileHeader().isEncrypted()) || (p10.getFileHeader().getEncryptionMethod() != 99)) {
            v0 = 0;
        }
        this.isAESEncryptedFile = v0;
        return;
    }

    public int available()
    {
        int v2_3;
        long v0 = (this.length - this.bytesRead);
        if (v0 <= 2147483647) {
            v2_3 = ((int) v0);
        } else {
            v2_3 = 2147483647;
        }
        return v2_3;
    }

    protected void checkAndReadAESMacBytes()
    {
        if ((this.isAESEncryptedFile) && ((this.decrypter != null) && (((this.decrypter instanceof net.lingala.zip4j.crypto.AESDecrypter)) && (((net.lingala.zip4j.crypto.AESDecrypter) this.decrypter).getStoredMac() == null)))) {
            byte[] v0 = new byte[10];
            int v2 = this.raf.read(v0);
            if (v2 != 10) {
                if (!this.unzipEngine.getZipModel().isSplitArchive()) {
                    throw new java.io.IOException("Error occured while reading stored AES authentication bytes");
                } else {
                    this.raf.close();
                    this.raf = this.unzipEngine.startNextSplitFile();
                    this.raf.read(v0, v2, (10 - v2));
                }
            }
            ((net.lingala.zip4j.crypto.AESDecrypter) this.unzipEngine.getDecrypter()).setStoredMac(v0);
        }
        return;
    }

    public void close()
    {
        this.raf.close();
        return;
    }

    public net.lingala.zip4j.unzip.UnzipEngine getUnzipEngine()
    {
        return this.unzipEngine;
    }

    public int read()
    {
        int v0_0 = -1;
        if (this.bytesRead < this.length) {
            if (!this.isAESEncryptedFile) {
                if (this.read(this.oneByteBuff, 0, 1) != -1) {
                    v0_0 = (this.oneByteBuff[0] & 255);
                }
            } else {
                if ((this.aesBytesReturned == 0) || (this.aesBytesReturned == 16)) {
                    if (this.read(this.aesBlockByte) == -1) {
                        return v0_0;
                    } else {
                        this.aesBytesReturned = 0;
                    }
                }
                int v0_3 = this.aesBlockByte;
                int v1_9 = this.aesBytesReturned;
                this.aesBytesReturned = (v1_9 + 1);
                v0_0 = (v0_3[v1_9] & 255);
            }
        }
        return v0_0;
    }

    public int read(byte[] p3)
    {
        return this.read(p3, 0, p3.length);
    }

    public int read(byte[] p9, int p10, int p11)
    {
        java.io.IOException v2_36;
        if (((long) p11) <= (this.length - this.bytesRead)) {
            if (((this.unzipEngine.getDecrypter() instanceof net.lingala.zip4j.crypto.AESDecrypter)) && (((this.bytesRead + ((long) p11)) < this.length) && ((p11 % 16) != 0))) {
                p11 -= (p11 % 16);
            }
            this.count = this.raf.read(p9, p10, p11);
            if ((this.count < p11) && (this.unzipEngine.getZipModel().isSplitArchive())) {
                this.raf.close();
                this.raf = this.unzipEngine.startNextSplitFile();
                if (this.count < 0) {
                    this.count = 0;
                }
                int v1 = this.raf.read(p9, this.count, (p11 - this.count));
                if (v1 > 0) {
                    this.count = (this.count + v1);
                }
            }
            if (this.count > 0) {
                if (this.decrypter != null) {
                    try {
                        this.decrypter.decryptData(p9, p10, this.count);
                    } catch (net.lingala.zip4j.exception.ZipException v0) {
                        throw new java.io.IOException(v0.getMessage());
                    }
                }
                this.bytesRead = (this.bytesRead + ((long) this.count));
            }
            if (this.bytesRead >= this.length) {
                this.checkAndReadAESMacBytes();
            }
            v2_36 = this.count;
        } else {
            p11 = ((int) (this.length - this.bytesRead));
            if (p11 != 0) {
            } else {
                this.checkAndReadAESMacBytes();
                v2_36 = -1;
            }
        }
        return v2_36;
    }

    public void seek(long p2)
    {
        this.raf.seek(p2);
        return;
    }

    public long skip(long p5)
    {
        if (p5 >= 0) {
            if (p5 > (this.length - this.bytesRead)) {
                p5 = (this.length - this.bytesRead);
            }
            this.bytesRead = (this.bytesRead + p5);
            return p5;
        } else {
            throw new IllegalArgumentException();
        }
    }
}
