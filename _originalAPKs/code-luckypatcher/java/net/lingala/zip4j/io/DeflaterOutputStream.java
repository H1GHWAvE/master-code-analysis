package net.lingala.zip4j.io;
public class DeflaterOutputStream extends net.lingala.zip4j.io.CipherOutputStream {
    private byte[] buff;
    protected java.util.zip.Deflater deflater;
    private boolean firstBytesRead;

    public DeflaterOutputStream(java.io.OutputStream p2, net.lingala.zip4j.model.ZipModel p3)
    {
        this(p2, p3);
        this.deflater = new java.util.zip.Deflater();
        int v0_3 = new byte[4096];
        this.buff = v0_3;
        this.firstBytesRead = 0;
        return;
    }

    private void deflate()
    {
        int v0 = this.deflater.deflate(this.buff, 0, this.buff.length);
        if (v0 > 0) {
            if (this.deflater.finished()) {
                if (v0 != 4) {
                    if (v0 >= 4) {
                        v0 -= 4;
                    } else {
                        this.decrementCompressedFileSize((4 - v0));
                        return;
                    }
                }
            }
            if (this.firstBytesRead) {
                super.write(this.buff, 0, v0);
            } else {
                super.write(this.buff, 2, (v0 - 2));
                this.firstBytesRead = 1;
            }
        }
        return;
    }

    public void closeEntry()
    {
        if (this.zipParameters.getCompressionMethod() == 8) {
            if (!this.deflater.finished()) {
                this.deflater.finish();
                while (!this.deflater.finished()) {
                    this.deflate();
                }
            }
            this.firstBytesRead = 0;
        }
        super.closeEntry();
        return;
    }

    public void finish()
    {
        super.finish();
        return;
    }

    public void putNextEntry(java.io.File p3, net.lingala.zip4j.model.ZipParameters p4)
    {
        super.putNextEntry(p3, p4);
        if (p4.getCompressionMethod() == 8) {
            this.deflater.reset();
            if (((p4.getCompressionLevel() >= 0) && (p4.getCompressionLevel() <= 9)) || (p4.getCompressionLevel() == -1)) {
                this.deflater.setLevel(p4.getCompressionLevel());
            } else {
                throw new net.lingala.zip4j.exception.ZipException("invalid compression level for deflater. compression level should be in the range of 0-9");
            }
        }
        return;
    }

    public void write(int p5)
    {
        byte[] v0 = new byte[1];
        v0[0] = ((byte) p5);
        this.write(v0, 0, 1);
        return;
    }

    public void write(byte[] p3)
    {
        this.write(p3, 0, p3.length);
        return;
    }

    public void write(byte[] p3, int p4, int p5)
    {
        if (this.zipParameters.getCompressionMethod() == 8) {
            this.deflater.setInput(p3, p4, p5);
            while (!this.deflater.needsInput()) {
                this.deflate();
            }
        } else {
            super.write(p3, p4, p5);
        }
        return;
    }
}
