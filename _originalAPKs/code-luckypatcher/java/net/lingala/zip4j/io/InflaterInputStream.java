package net.lingala.zip4j.io;
public class InflaterInputStream extends net.lingala.zip4j.io.PartInputStream {
    private byte[] buff;
    private long bytesWritten;
    private java.util.zip.Inflater inflater;
    private byte[] oneByteBuff;
    private long uncompressedSize;
    private net.lingala.zip4j.unzip.UnzipEngine unzipEngine;

    public InflaterInputStream(java.io.RandomAccessFile p3, long p4, long p6, net.lingala.zip4j.unzip.UnzipEngine p8)
    {
        net.lingala.zip4j.io.InflaterInputStream v2_1 = this(p3, p4, p6, p8);
        long v0_0 = new byte[1];
        v2_1.oneByteBuff = v0_0;
        v2_1.inflater = new java.util.zip.Inflater(1);
        long v0_4 = new byte[4096];
        v2_1.buff = v0_4;
        v2_1.unzipEngine = p8;
        v2_1.bytesWritten = 0;
        v2_1.uncompressedSize = p8.getFileHeader().getUncompressedSize();
        return;
    }

    private void fill()
    {
        int v0 = super.read(this.buff, 0, this.buff.length);
        if (v0 != -1) {
            this.inflater.setInput(this.buff, 0, v0);
            return;
        } else {
            throw new java.io.EOFException("Unexpected end of ZLIB input stream");
        }
    }

    private void finishInflating()
    {
        byte[] v0 = new byte[1024];
        while (super.read(v0, 0, 1024) != -1) {
        }
        this.checkAndReadAESMacBytes();
        return;
    }

    public int available()
    {
        int v0_2;
        if (!this.inflater.finished()) {
            v0_2 = 1;
        } else {
            v0_2 = 0;
        }
        return v0_2;
    }

    public void close()
    {
        this.inflater.end();
        super.close();
        return;
    }

    public net.lingala.zip4j.unzip.UnzipEngine getUnzipEngine()
    {
        return super.getUnzipEngine();
    }

    public int read()
    {
        int v0_0 = -1;
        if (this.read(this.oneByteBuff, 0, 1) != -1) {
            v0_0 = (this.oneByteBuff[0] & 255);
        }
        return v0_0;
    }

    public int read(byte[] p3)
    {
        if (p3 != null) {
            return this.read(p3, 0, p3.length);
        } else {
            throw new NullPointerException("input buffer is null");
        }
    }

    public int read(byte[] p9, int p10, int p11)
    {
        if (p9 != null) {
            if ((p10 >= 0) && ((p11 >= 0) && (p11 <= (p9.length - p10)))) {
                java.io.IOException v1;
                if (p11 != 0) {
                    try {
                        if (this.bytesWritten >= this.uncompressedSize) {
                            this.finishInflating();
                            v1 = -1;
                            return v1;
                        }
                    } catch (java.util.zip.DataFormatException v0) {
                        String v2 = "Invalid ZLIB data format";
                        if (v0.getMessage() != null) {
                            v2 = v0.getMessage();
                        }
                        if (this.unzipEngine != null) {
                            if (this.unzipEngine.getLocalFileHeader().isEncrypted()) {
                                if (this.unzipEngine.getLocalFileHeader().getEncryptionMethod() == 0) {
                                    v2 = new StringBuilder().append(v2).append(" - Wrong Password?").toString();
                                }
                            }
                        }
                        throw new java.io.IOException(v2);
                    }
                    while(true) {
                        v1 = this.inflater.inflate(p9, p10, p11);
                        if (v1 != null) {
                            this.bytesWritten = (this.bytesWritten + ((long) v1));
                        } else {
                            if ((this.inflater.finished()) || (this.inflater.needsDictionary())) {
                                break;
                            }
                            if (this.inflater.needsInput()) {
                                this.fill();
                            }
                        }
                    }
                    this.finishInflating();
                    v1 = -1;
                } else {
                    v1 = 0;
                }
                return v1;
            } else {
                throw new IndexOutOfBoundsException();
            }
        } else {
            throw new NullPointerException("input buffer is null");
        }
    }

    public void seek(long p1)
    {
        super.seek(p1);
        return;
    }

    public long skip(long p7)
    {
        if (p7 >= 0) {
            int v2 = ((int) Math.min(p7, 2147483647));
            int v3 = 0;
            byte[] v0 = new byte[512];
            while (v3 < v2) {
                int v1_0 = (v2 - v3);
                if (v1_0 > v0.length) {
                    v1_0 = v0.length;
                }
                int v1_1 = this.read(v0, 0, v1_0);
                if (v1_1 == -1) {
                    break;
                }
                v3 += v1_1;
            }
            return ((long) v3);
        } else {
            throw new IllegalArgumentException("negative skip length");
        }
    }
}
