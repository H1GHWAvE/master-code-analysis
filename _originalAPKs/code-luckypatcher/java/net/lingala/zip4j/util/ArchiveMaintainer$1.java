package net.lingala.zip4j.util;
 class ArchiveMaintainer$1 extends java.lang.Thread {
    final synthetic net.lingala.zip4j.util.ArchiveMaintainer this$0;
    final synthetic net.lingala.zip4j.model.FileHeader val$fileHeader;
    final synthetic net.lingala.zip4j.progress.ProgressMonitor val$progressMonitor;
    final synthetic net.lingala.zip4j.model.ZipModel val$zipModel;

    ArchiveMaintainer$1(net.lingala.zip4j.util.ArchiveMaintainer p1, String p2, net.lingala.zip4j.model.ZipModel p3, net.lingala.zip4j.model.FileHeader p4, net.lingala.zip4j.progress.ProgressMonitor p5)
    {
        this.this$0 = p1;
        this.val$zipModel = p3;
        this.val$fileHeader = p4;
        this.val$progressMonitor = p5;
        this(p2);
        return;
    }

    public void run()
    {
        try {
            this.this$0.initRemoveZipFile(this.val$zipModel, this.val$fileHeader, this.val$progressMonitor);
            this.val$progressMonitor.endProgressMonitorSuccess();
        } catch (net.lingala.zip4j.exception.ZipException v0) {
        }
        return;
    }
}
