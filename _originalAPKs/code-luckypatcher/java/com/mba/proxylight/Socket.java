package com.mba.proxylight;
public class Socket {
    public long created;
    public long lastRead;
    public long lastWrite;
    public java.nio.channels.SocketChannel socket;

    public Socket()
    {
        this.socket = 0;
        this.created = System.currentTimeMillis();
        this.lastWrite = this.created;
        this.lastRead = this.created;
        return;
    }
}
