package com.mba.proxylight;
 class ProxyLight$1$1 extends com.mba.proxylight.RequestProcessor {
    final synthetic com.mba.proxylight.ProxyLight$1 this$1;

    ProxyLight$1$1(com.mba.proxylight.ProxyLight$1 p1)
    {
        this.this$1 = p1;
        return;
    }

    public void debug(String p2)
    {
        this.this$1.this$0.debug(p2);
        return;
    }

    public void error(String p2, Throwable p3)
    {
        this.this$1.this$0.error(p2, p3);
        return;
    }

    public String getRemoteProxyHost()
    {
        return this.this$1.this$0.getRemoteProxyHost();
    }

    public int getRemoteProxyPort()
    {
        return this.this$1.this$0.getRemoteProxyPort();
    }

    public java.util.List getRequestFilters()
    {
        return this.this$1.this$0.getRequestFilters();
    }

    public void recycle()
    {
        this.this$1.this$0.recycle(this);
        return;
    }

    public java.net.InetAddress resolve(String p2)
    {
        return this.this$1.this$0.resolve(p2);
    }
}
