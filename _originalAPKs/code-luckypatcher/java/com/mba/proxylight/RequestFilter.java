package com.mba.proxylight;
public interface RequestFilter {

    public abstract boolean filter();
}
