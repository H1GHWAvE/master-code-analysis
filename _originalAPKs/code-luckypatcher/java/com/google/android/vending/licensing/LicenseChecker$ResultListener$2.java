package com.google.android.vending.licensing;
 class LicenseChecker$ResultListener$2 implements java.lang.Runnable {
    final synthetic com.google.android.vending.licensing.LicenseChecker$ResultListener this$1;
    final synthetic int val$responseCode;
    final synthetic String val$signature;
    final synthetic String val$signedData;

    LicenseChecker$ResultListener$2(com.google.android.vending.licensing.LicenseChecker$ResultListener p1, int p2, String p3, String p4)
    {
        this.this$1 = p1;
        this.val$responseCode = p2;
        this.val$signedData = p3;
        this.val$signature = p4;
        return;
    }

    public void run()
    {
        android.util.Log.i("LicenseChecker", "Received response.");
        System.out.println(new StringBuilder().append("Responce Code: ").append(this.val$responseCode).toString());
        if (com.google.android.vending.licensing.LicenseChecker.access$300(this.this$1.this$0).contains(com.google.android.vending.licensing.LicenseChecker$ResultListener.access$000(this.this$1))) {
            com.google.android.vending.licensing.LicenseChecker$ResultListener.access$400(this.this$1);
            com.google.android.vending.licensing.LicenseChecker$ResultListener.access$000(this.this$1).verify(com.google.android.vending.licensing.LicenseChecker.access$500(this.this$1.this$0), this.val$responseCode, this.val$signedData, this.val$signature);
            com.google.android.vending.licensing.LicenseChecker.access$200(this.this$1.this$0, com.google.android.vending.licensing.LicenseChecker$ResultListener.access$000(this.this$1));
        }
        return;
    }
}
