package com.google.android.finsky.billing.iab.google.util;
public class Base64 {
    static final synthetic boolean $assertionsDisabled = False;
    private static final byte[] ALPHABET = None;
    private static final byte[] DECODABET = None;
    public static final boolean DECODE = False;
    public static final boolean ENCODE = True;
    private static final byte EQUALS_SIGN = 0x3d;
    private static final byte EQUALS_SIGN_ENC = 0x-1;
    private static final byte NEW_LINE = 0xa;
    private static final byte[] WEBSAFE_ALPHABET = None;
    private static final byte[] WEBSAFE_DECODABET = None;
    private static final byte WHITE_SPACE_ENC = 0x-5;

    static Base64()
    {
        byte[] v0_2;
        if (com.google.android.finsky.billing.iab.google.util.Base64.desiredAssertionStatus()) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        com.google.android.finsky.billing.iab.google.util.Base64.$assertionsDisabled = v0_2;
        byte[] v0_3 = new byte[64];
        v0_3 = {65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 43, 47};
        com.google.android.finsky.billing.iab.google.util.Base64.ALPHABET = v0_3;
        byte[] v0_4 = new byte[64];
        v0_4 = {65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 45, 95};
        com.google.android.finsky.billing.iab.google.util.Base64.WEBSAFE_ALPHABET = v0_4;
        byte[] v0_5 = new byte[128];
        v0_5 = {-9, -9, -9, -9, -9, -9, -9, -9, -9, -5, -5, -9, -9, -5, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -5, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, 62, -9, -9, -9, 63, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, -9, -9, -9, -1, -9, -9, -9, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, -9, -9, -9, -9, -9, -9, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, -9, -9, -9, -9, -9};
        com.google.android.finsky.billing.iab.google.util.Base64.DECODABET = v0_5;
        byte[] v0_6 = new byte[128];
        v0_6 = {-9, -9, -9, -9, -9, -9, -9, -9, -9, -5, -5, -9, -9, -5, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -5, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, 62, -9, -9, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, -9, -9, -9, -1, -9, -9, -9, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, -9, -9, -9, -9, 63, -9, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, -9, -9, -9, -9, -9};
        com.google.android.finsky.billing.iab.google.util.Base64.WEBSAFE_DECODABET = v0_6;
        return;
    }

    private Base64()
    {
        return;
    }

    public static byte[] decode(String p3)
    {
        byte[] v0 = p3.getBytes();
        return com.google.android.finsky.billing.iab.google.util.Base64.decode(v0, 0, v0.length);
    }

    public static byte[] decode(byte[] p2)
    {
        return com.google.android.finsky.billing.iab.google.util.Base64.decode(p2, 0, p2.length);
    }

    public static byte[] decode(byte[] p1, int p2, int p3)
    {
        return com.google.android.finsky.billing.iab.google.util.Base64.decode(p1, p2, p3, com.google.android.finsky.billing.iab.google.util.Base64.DECODABET);
    }

    public static byte[] decode(byte[] p16, int p17, int p18, byte[] p19)
    {
        byte[] v9 = new byte[(((p18 * 3) / 4) + 2)];
        int v10 = 0;
        byte[] v1 = new byte[4];
        int v5 = 0;
        int v3 = 0;
        while (v5 < p18) {
            byte v11 = ((byte) (p16[(v5 + p17)] & 127));
            byte v12 = p19[v11];
            if (v12 < -5) {
                throw new com.google.android.finsky.billing.iab.google.util.Base64DecoderException(new StringBuilder().append("Bad Base64 input character at ").append(v5).append(": ").append(p16[(v5 + p17)]).append("(decimal)").toString());
            } else {
                int v2_1;
                if (v12 < -1) {
                    v2_1 = v3;
                } else {
                    if (v11 != 61) {
                        v2_1 = (v3 + 1);
                        v1[v3] = v11;
                        if (v2_1 == 4) {
                            v10 += com.google.android.finsky.billing.iab.google.util.Base64.decode4to3(v1, 0, v9, v10, p19);
                            v2_1 = 0;
                        }
                    } else {
                        int v4 = (p18 - v5);
                        byte v6 = ((byte) (p16[((p18 - 1) + p17)] & 127));
                        if ((v3 != 0) && (v3 != 1)) {
                            if (((v3 != 3) || (v4 <= 2)) && ((v3 != 4) || (v4 <= 1))) {
                                if ((v6 == 61) || (v6 == 10)) {
                                    break;
                                }
                                throw new com.google.android.finsky.billing.iab.google.util.Base64DecoderException("encoded value has invalid trailing byte");
                            } else {
                                throw new com.google.android.finsky.billing.iab.google.util.Base64DecoderException(new StringBuilder().append("padding byte \'=\' falsely signals end of encoded value at offset ").append(v5).toString());
                            }
                        } else {
                            throw new com.google.android.finsky.billing.iab.google.util.Base64DecoderException(new StringBuilder().append("invalid padding byte \'=\' at byte offset ").append(v5).toString());
                        }
                    }
                }
                v5++;
                v3 = v2_1;
            }
        }
        if (v3 != 0) {
            if (v3 != 1) {
                v1[v3] = 61;
                v10 += com.google.android.finsky.billing.iab.google.util.Base64.decode4to3(v1, 0, v9, v10, p19);
            } else {
                throw new com.google.android.finsky.billing.iab.google.util.Base64DecoderException(new StringBuilder().append("single trailing character at offset ").append((p18 - 1)).toString());
            }
        }
        byte[] v8 = new byte[v10];
        System.arraycopy(v9, 0, v8, 0, v10);
        return v8;
    }

    private static int decode4to3(byte[] p3, int p4, byte[] p5, int p6, byte[] p7)
    {
        int v1_14;
        if (p3[(p4 + 2)] != 61) {
            if (p3[(p4 + 3)] != 61) {
                int v0_0 = (((((p7[p3[p4]] << 24) >> 6) | ((p7[p3[(p4 + 1)]] << 24) >> 12)) | ((p7[p3[(p4 + 2)]] << 24) >> 18)) | ((p7[p3[(p4 + 3)]] << 24) >> 24));
                p5[p6] = ((byte) (v0_0 >> 16));
                p5[(p6 + 1)] = ((byte) (v0_0 >> 8));
                p5[(p6 + 2)] = ((byte) v0_0);
                v1_14 = 3;
            } else {
                int v0_1 = ((((p7[p3[p4]] << 24) >> 6) | ((p7[p3[(p4 + 1)]] << 24) >> 12)) | ((p7[p3[(p4 + 2)]] << 24) >> 18));
                p5[p6] = ((byte) (v0_1 >> 16));
                p5[(p6 + 1)] = ((byte) (v0_1 >> 8));
                v1_14 = 2;
            }
        } else {
            p5[p6] = ((byte) ((((p7[p3[p4]] << 24) >> 6) | ((p7[p3[(p4 + 1)]] << 24) >> 12)) >> 16));
            v1_14 = 1;
        }
        return v1_14;
    }

    public static byte[] decodeWebSafe(String p3)
    {
        byte[] v0 = p3.getBytes();
        return com.google.android.finsky.billing.iab.google.util.Base64.decodeWebSafe(v0, 0, v0.length);
    }

    public static byte[] decodeWebSafe(byte[] p2)
    {
        return com.google.android.finsky.billing.iab.google.util.Base64.decodeWebSafe(p2, 0, p2.length);
    }

    public static byte[] decodeWebSafe(byte[] p1, int p2, int p3)
    {
        return com.google.android.finsky.billing.iab.google.util.Base64.decode(p1, p2, p3, com.google.android.finsky.billing.iab.google.util.Base64.WEBSAFE_DECODABET);
    }

    public static String encode(byte[] p4)
    {
        return com.google.android.finsky.billing.iab.google.util.Base64.encode(p4, 0, p4.length, com.google.android.finsky.billing.iab.google.util.Base64.ALPHABET, 1);
    }

    public static String encode(byte[] p4, int p5, int p6, byte[] p7, boolean p8)
    {
        byte[] v0 = com.google.android.finsky.billing.iab.google.util.Base64.encode(p4, p5, p6, p7, 2147483647);
        int v1 = v0.length;
        while ((!p8) && ((v1 > 0) && (v0[(v1 - 1)] == 61))) {
            v1--;
        }
        return new String(v0, 0, v1);
    }

    public static byte[] encode(byte[] p13, int p14, int p15, byte[] p16, int p17)
    {
        int v10 = (((p15 + 2) / 3) * 4);
        byte[] v4 = new byte[((v10 / p17) + v10)];
        int v7 = 0;
        int v5 = 0;
        int v9 = (p15 - 2);
        int v12_0 = 0;
        while (v7 < v9) {
            int v8 = ((((p13[(v7 + p14)] << 24) >> 8) | ((p13[((v7 + 1) + p14)] << 24) >> 16)) | ((p13[((v7 + 2) + p14)] << 24) >> 24));
            v4[v5] = p16[(v8 >> 18)];
            v4[(v5 + 1)] = p16[((v8 >> 12) & 63)];
            v4[(v5 + 2)] = p16[((v8 >> 6) & 63)];
            v4[(v5 + 3)] = p16[(v8 & 63)];
            v12_0 += 4;
            if (v12_0 == p17) {
                v4[(v5 + 4)] = 10;
                v5++;
                v12_0 = 0;
            }
            v7 += 3;
            v5 += 4;
        }
        if (v7 < p15) {
            com.google.android.finsky.billing.iab.google.util.Base64.encode3to4(p13, (v7 + p14), (p15 - v7), v4, v5, p16);
            if ((v12_0 + 4) == p17) {
                v4[(v5 + 4)] = 10;
                v5++;
            }
            v5 += 4;
        }
        if ((com.google.android.finsky.billing.iab.google.util.Base64.$assertionsDisabled) || (v5 == v4.length)) {
            return v4;
        } else {
            throw new AssertionError();
        }
    }

    private static byte[] encode3to4(byte[] p5, int p6, int p7, byte[] p8, int p9, byte[] p10)
    {
        int v3_0;
        int v1_0 = 0;
        if (p7 <= 0) {
            v3_0 = 0;
        } else {
            v3_0 = ((p5[p6] << 24) >> 8);
        }
        byte v2_4;
        if (p7 <= 1) {
            v2_4 = 0;
        } else {
            v2_4 = ((p5[(p6 + 1)] << 24) >> 16);
        }
        if (p7 > 2) {
            v1_0 = ((p5[(p6 + 2)] << 24) >> 24);
        }
        int v0 = ((v2_4 | v3_0) | v1_0);
        switch (p7) {
            case 1:
                p8[p9] = p10[(v0 >> 18)];
                p8[(p9 + 1)] = p10[((v0 >> 12) & 63)];
                p8[(p9 + 2)] = 61;
                p8[(p9 + 3)] = 61;
                break;
            case 2:
                p8[p9] = p10[(v0 >> 18)];
                p8[(p9 + 1)] = p10[((v0 >> 12) & 63)];
                p8[(p9 + 2)] = p10[((v0 >> 6) & 63)];
                p8[(p9 + 3)] = 61;
                break;
            case 3:
                p8[p9] = p10[(v0 >> 18)];
                p8[(p9 + 1)] = p10[((v0 >> 12) & 63)];
                p8[(p9 + 2)] = p10[((v0 >> 6) & 63)];
                p8[(p9 + 3)] = p10[(v0 & 63)];
                break;
        }
        return p8;
    }

    public static String encodeWebSafe(byte[] p3, boolean p4)
    {
        return com.google.android.finsky.billing.iab.google.util.Base64.encode(p3, 0, p3.length, com.google.android.finsky.billing.iab.google.util.Base64.WEBSAFE_ALPHABET, p4);
    }
}
