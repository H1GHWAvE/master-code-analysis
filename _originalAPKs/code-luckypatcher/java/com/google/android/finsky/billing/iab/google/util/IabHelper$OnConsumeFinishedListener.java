package com.google.android.finsky.billing.iab.google.util;
public interface IabHelper$OnConsumeFinishedListener {

    public abstract void onConsumeFinished();
}
