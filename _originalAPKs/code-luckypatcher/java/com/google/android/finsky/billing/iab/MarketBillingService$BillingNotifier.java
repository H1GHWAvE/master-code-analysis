package com.google.android.finsky.billing.iab;
public class MarketBillingService$BillingNotifier {
    private com.google.android.finsky.billing.iab.MarketBillingService mService;
    final synthetic com.google.android.finsky.billing.iab.MarketBillingService this$0;

    public MarketBillingService$BillingNotifier(com.google.android.finsky.billing.iab.MarketBillingService p1, com.google.android.finsky.billing.iab.MarketBillingService p2)
    {
        this.this$0 = p1;
        this.mService = p2;
        return;
    }

    protected boolean sendPurchaseStateChanged(String p6, String p7, String p8)
    {
        int v1;
        android.content.Intent v0 = com.google.android.finsky.billing.iab.MarketBillingService.findReceiverName(this.mService, p6, new android.content.Intent("com.android.vending.billing.PURCHASE_STATE_CHANGED"));
        if (v0 != null) {
            v0.putExtra("inapp_signed_data", p7);
            v0.putExtra("inapp_signature", p8);
            this.mService.sendBroadcast(v0);
            v1 = 1;
        } else {
            v1 = 0;
        }
        return v1;
    }

    protected boolean sendResponseCode(String p2, long p3, int p5)
    {
        return com.google.android.finsky.billing.iab.MarketBillingService.sendResponseCode(this.mService, p2, p3, p5);
    }
}
