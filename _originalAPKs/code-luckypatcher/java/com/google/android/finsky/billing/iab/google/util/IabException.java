package com.google.android.finsky.billing.iab.google.util;
public class IabException extends java.lang.Exception {
    com.google.android.finsky.billing.iab.google.util.IabResult mResult;

    public IabException(int p2, String p3)
    {
        this(new com.google.android.finsky.billing.iab.google.util.IabResult(p2, p3));
        return;
    }

    public IabException(int p2, String p3, Exception p4)
    {
        this(new com.google.android.finsky.billing.iab.google.util.IabResult(p2, p3), p4);
        return;
    }

    public IabException(com.google.android.finsky.billing.iab.google.util.IabResult p2)
    {
        this(p2, 0);
        return;
    }

    public IabException(com.google.android.finsky.billing.iab.google.util.IabResult p2, Exception p3)
    {
        this(p2.getMessage(), p3);
        this.mResult = p2;
        return;
    }

    public com.google.android.finsky.billing.iab.google.util.IabResult getResult()
    {
        return this.mResult;
    }
}
