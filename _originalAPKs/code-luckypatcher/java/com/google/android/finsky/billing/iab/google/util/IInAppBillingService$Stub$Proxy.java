package com.google.android.finsky.billing.iab.google.util;
 class IInAppBillingService$Stub$Proxy implements com.google.android.finsky.billing.iab.google.util.IInAppBillingService {
    private android.os.IBinder mRemote;

    IInAppBillingService$Stub$Proxy(android.os.IBinder p1)
    {
        this.mRemote = p1;
        return;
    }

    public android.os.IBinder asBinder()
    {
        return this.mRemote;
    }

    public int consumePurchase(int p7, String p8, String p9)
    {
        android.os.Parcel v0 = android.os.Parcel.obtain();
        android.os.Parcel v1 = android.os.Parcel.obtain();
        try {
            v0.writeInterfaceToken("com.android.vending.billing.IInAppBillingService");
            v0.writeInt(p7);
            v0.writeString(p8);
            v0.writeString(p9);
            this.mRemote.transact(5, v0, v1, 0);
            v1.readException();
            int v2 = v1.readInt();
            v1.recycle();
            v0.recycle();
            return v2;
        } catch (Throwable v3_2) {
            v1.recycle();
            v0.recycle();
            throw v3_2;
        }
    }

    public android.os.Bundle getBuyIntent(int p7, String p8, String p9, String p10, String p11)
    {
        android.os.Parcel v0 = android.os.Parcel.obtain();
        android.os.Parcel v1 = android.os.Parcel.obtain();
        try {
            int v2_0;
            v0.writeInterfaceToken("com.android.vending.billing.IInAppBillingService");
            v0.writeInt(p7);
            v0.writeString(p8);
            v0.writeString(p9);
            v0.writeString(p10);
            v0.writeString(p11);
            this.mRemote.transact(3, v0, v1, 0);
            v1.readException();
        } catch (android.os.Parcelable$Creator v3_4) {
            v1.recycle();
            v0.recycle();
            throw v3_4;
        }
        if (v1.readInt() == 0) {
            v2_0 = 0;
        } else {
            v2_0 = ((android.os.Bundle) android.os.Bundle.CREATOR.createFromParcel(v1));
        }
        v1.recycle();
        v0.recycle();
        return v2_0;
    }

    public android.os.Bundle getBuyIntentToReplaceSkus(int p7, String p8, java.util.List p9, String p10, String p11, String p12)
    {
        android.os.Parcel v0 = android.os.Parcel.obtain();
        android.os.Parcel v1 = android.os.Parcel.obtain();
        try {
            int v2_0;
            v0.writeInterfaceToken("com.android.vending.billing.IInAppBillingService");
            v0.writeInt(p7);
            v0.writeString(p8);
            v0.writeList(p9);
            v0.writeString(p10);
            v0.writeString(p11);
            v0.writeString(p12);
            this.mRemote.transact(6, v0, v1, 0);
            v1.readException();
        } catch (android.os.Parcelable$Creator v3_4) {
            v1.recycle();
            v0.recycle();
            throw v3_4;
        }
        if (v1.readInt() == 0) {
            v2_0 = 0;
        } else {
            v2_0 = ((android.os.Bundle) android.os.Bundle.CREATOR.createFromParcel(v1));
        }
        v1.recycle();
        v0.recycle();
        return v2_0;
    }

    public String getInterfaceDescriptor()
    {
        return "com.android.vending.billing.IInAppBillingService";
    }

    public android.os.Bundle getPurchases(int p7, String p8, String p9, String p10)
    {
        android.os.Parcel v0 = android.os.Parcel.obtain();
        android.os.Parcel v1 = android.os.Parcel.obtain();
        try {
            int v2_0;
            v0.writeInterfaceToken("com.android.vending.billing.IInAppBillingService");
            v0.writeInt(p7);
            v0.writeString(p8);
            v0.writeString(p9);
            v0.writeString(p10);
            this.mRemote.transact(4, v0, v1, 0);
            v1.readException();
        } catch (android.os.Parcelable$Creator v3_4) {
            v1.recycle();
            v0.recycle();
            throw v3_4;
        }
        if (v1.readInt() == 0) {
            v2_0 = 0;
        } else {
            v2_0 = ((android.os.Bundle) android.os.Bundle.CREATOR.createFromParcel(v1));
        }
        v1.recycle();
        v0.recycle();
        return v2_0;
    }

    public android.os.Bundle getSkuDetails(int p7, String p8, String p9, android.os.Bundle p10)
    {
        android.os.Parcel v0 = android.os.Parcel.obtain();
        android.os.Parcel v1 = android.os.Parcel.obtain();
        try {
            v0.writeInterfaceToken("com.android.vending.billing.IInAppBillingService");
            v0.writeInt(p7);
            v0.writeString(p8);
            v0.writeString(p9);
        } catch (android.os.Parcelable$Creator v3_7) {
            v1.recycle();
            v0.recycle();
            throw v3_7;
        }
        if (p10 == null) {
            v0.writeInt(0);
        } else {
            v0.writeInt(1);
            p10.writeToParcel(v0, 0);
        }
        int v2_0;
        this.mRemote.transact(2, v0, v1, 0);
        v1.readException();
        if (v1.readInt() == 0) {
            v2_0 = 0;
        } else {
            v2_0 = ((android.os.Bundle) android.os.Bundle.CREATOR.createFromParcel(v1));
        }
        v1.recycle();
        v0.recycle();
        return v2_0;
    }

    public int isBillingSupported(int p7, String p8, String p9)
    {
        android.os.Parcel v0 = android.os.Parcel.obtain();
        android.os.Parcel v1 = android.os.Parcel.obtain();
        try {
            v0.writeInterfaceToken("com.android.vending.billing.IInAppBillingService");
            v0.writeInt(p7);
            v0.writeString(p8);
            v0.writeString(p9);
            this.mRemote.transact(1, v0, v1, 0);
            v1.readException();
            int v2 = v1.readInt();
            v1.recycle();
            v0.recycle();
            return v2;
        } catch (Throwable v3_2) {
            v1.recycle();
            v0.recycle();
            throw v3_2;
        }
    }
}
