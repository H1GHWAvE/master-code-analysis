package com.google.android.finsky.billing.iab.google.util;
public class IabHelper {
    public static final int BILLING_RESPONSE_RESULT_BILLING_UNAVAILABLE = 3;
    public static final int BILLING_RESPONSE_RESULT_DEVELOPER_ERROR = 5;
    public static final int BILLING_RESPONSE_RESULT_ERROR = 6;
    public static final int BILLING_RESPONSE_RESULT_ITEM_ALREADY_OWNED = 7;
    public static final int BILLING_RESPONSE_RESULT_ITEM_NOT_OWNED = 8;
    public static final int BILLING_RESPONSE_RESULT_ITEM_UNAVAILABLE = 4;
    public static final int BILLING_RESPONSE_RESULT_OK = 0;
    public static final int BILLING_RESPONSE_RESULT_USER_CANCELED = 1;
    public static final String GET_SKU_DETAILS_ITEM_LIST = "ITEM_ID_LIST";
    public static final String GET_SKU_DETAILS_ITEM_TYPE_LIST = "ITEM_TYPE_LIST";
    public static final int IABHELPER_BAD_RESPONSE = 64534;
    public static final int IABHELPER_ERROR_BASE = 64536;
    public static final int IABHELPER_INVALID_CONSUMPTION = 64526;
    public static final int IABHELPER_MISSING_TOKEN = 64529;
    public static final int IABHELPER_REMOTE_EXCEPTION = 64535;
    public static final int IABHELPER_SEND_INTENT_FAILED = 64532;
    public static final int IABHELPER_SUBSCRIPTIONS_NOT_AVAILABLE = 64527;
    public static final int IABHELPER_UNKNOWN_ERROR = 64528;
    public static final int IABHELPER_UNKNOWN_PURCHASE_RESPONSE = 64530;
    public static final int IABHELPER_USER_CANCELLED = 64531;
    public static final int IABHELPER_VERIFICATION_FAILED = 64533;
    public static final String INAPP_CONTINUATION_TOKEN = "INAPP_CONTINUATION_TOKEN";
    public static final String ITEM_TYPE_INAPP = "inapp";
    public static final String ITEM_TYPE_SUBS = "subs";
    public static final String RESPONSE_BUY_INTENT = "BUY_INTENT";
    public static final String RESPONSE_CODE = "RESPONSE_CODE";
    public static final String RESPONSE_GET_SKU_DETAILS_LIST = "DETAILS_LIST";
    public static final String RESPONSE_INAPP_ITEM_LIST = "INAPP_PURCHASE_ITEM_LIST";
    public static final String RESPONSE_INAPP_PURCHASE_DATA = "INAPP_PURCHASE_DATA";
    public static final String RESPONSE_INAPP_PURCHASE_DATA_LIST = "INAPP_PURCHASE_DATA_LIST";
    public static final String RESPONSE_INAPP_SIGNATURE = "INAPP_DATA_SIGNATURE";
    public static final String RESPONSE_INAPP_SIGNATURE_LIST = "INAPP_DATA_SIGNATURE_LIST";
    boolean mAsyncInProgress;
    String mAsyncOperation;
    android.content.Context mContext;
    boolean mDebugLog;
    String mDebugTag;
    boolean mDisposed;
    com.google.android.finsky.billing.iab.google.util.IabHelper$OnIabPurchaseFinishedListener mPurchaseListener;
    String mPurchasingItemType;
    int mRequestCode;
    com.google.android.finsky.billing.iab.google.util.IInAppBillingService mService;
    android.content.ServiceConnection mServiceConn;
    boolean mSetupDone;
    String mSignatureBase64;
    boolean mSubscriptionsSupported;

    public IabHelper(android.content.Context p3, String p4)
    {
        this.mDebugLog = 0;
        this.mDebugTag = "IabHelper";
        this.mSetupDone = 0;
        this.mDisposed = 0;
        this.mSubscriptionsSupported = 0;
        this.mAsyncInProgress = 0;
        this.mAsyncOperation = "";
        this.mSignatureBase64 = 0;
        this.mContext = p3.getApplicationContext();
        this.mSignatureBase64 = p4;
        this.logDebug("IAB helper created.");
        return;
    }

    private void checkNotDisposed()
    {
        if (!this.mDisposed) {
            return;
        } else {
            throw new IllegalStateException("IabHelper was disposed of, so it cannot be used.");
        }
    }

    public static String getResponseDesc(int p5)
    {
        String v3_4;
        String[] v0 = "0:OK/1:User Canceled/2:Unknown/3:Billing Unavailable/4:Item unavailable/5:Developer Error/6:Error/7:Item Already Owned/8:Item not owned".split("/");
        String[] v1 = "0:OK/-1001:Remote exception during initialization/-1002:Bad response received/-1003:Purchase signature verification failed/-1004:Send intent failed/-1005:User cancelled/-1006:Unknown purchase response/-1007:Missing token/-1008:Unknown error/-1009:Subscriptions not available/-1010:Invalid consumption attempt".split("/");
        if (p5 > -1000) {
            if ((p5 >= 0) && (p5 < v0.length)) {
                v3_4 = v0[p5];
            } else {
                v3_4 = new StringBuilder().append(String.valueOf(p5)).append(":Unknown").toString();
            }
        } else {
            int v2 = (-1000 - p5);
            if ((v2 < 0) || (v2 >= v1.length)) {
                v3_4 = new StringBuilder().append(String.valueOf(p5)).append(":Unknown IAB Helper Error").toString();
            } else {
                v3_4 = v1[v2];
            }
        }
        return v3_4;
    }

    void checkSetupDone(String p4)
    {
        if (this.mSetupDone) {
            return;
        } else {
            this.logError(new StringBuilder().append("Illegal state for operation (").append(p4).append("): IAB helper is not set up.").toString());
            throw new IllegalStateException(new StringBuilder().append("IAB helper is not set up. Can\'t perform operation: ").append(p4).toString());
        }
    }

    void consume(com.google.android.finsky.billing.iab.google.util.Purchase p9)
    {
        this.checkNotDisposed();
        this.checkSetupDone("consume");
        if (p9.mItemType.equals("inapp")) {
            try {
                String v3 = p9.getToken();
                String v2 = p9.getSku();
            } catch (android.os.RemoteException v0) {
                throw new com.google.android.finsky.billing.iab.google.util.IabException(-1001, new StringBuilder().append("Remote exception while consuming. PurchaseInfo: ").append(p9).toString(), v0);
            }
            if ((v3 != null) && (!v3.equals(""))) {
                this.logDebug(new StringBuilder().append("Consuming sku: ").append(v2).append(", token: ").append(v3).toString());
                int v1 = this.mService.consumePurchase(3, this.mContext.getPackageName(), v3);
                if (v1 != 0) {
                    this.logDebug(new StringBuilder().append("Error consuming consuming sku ").append(v2).append(". ").append(com.google.android.finsky.billing.iab.google.util.IabHelper.getResponseDesc(v1)).toString());
                    throw new com.google.android.finsky.billing.iab.google.util.IabException(v1, new StringBuilder().append("Error consuming sku ").append(v2).toString());
                } else {
                    this.logDebug(new StringBuilder().append("Successfully consumed sku: ").append(v2).toString());
                    return;
                }
            } else {
                this.logError(new StringBuilder().append("Can\'t consume ").append(v2).append(". No token.").toString());
                throw new com.google.android.finsky.billing.iab.google.util.IabException(-1007, new StringBuilder().append("PurchaseInfo is missing token for sku: ").append(v2).append(" ").append(p9).toString());
            }
        } else {
            throw new com.google.android.finsky.billing.iab.google.util.IabException(-1010, new StringBuilder().append("Items of type \'").append(p9.mItemType).append("\' can\'t be consumed.").toString());
        }
    }

    public void consumeAsync(com.google.android.finsky.billing.iab.google.util.Purchase p3, com.google.android.finsky.billing.iab.google.util.IabHelper$OnConsumeFinishedListener p4)
    {
        this.checkNotDisposed();
        this.checkSetupDone("consume");
        java.util.ArrayList v0_1 = new java.util.ArrayList();
        v0_1.add(p3);
        this.consumeAsyncInternal(v0_1, p4, 0);
        return;
    }

    public void consumeAsync(java.util.List p2, com.google.android.finsky.billing.iab.google.util.IabHelper$OnConsumeMultiFinishedListener p3)
    {
        this.checkNotDisposed();
        this.checkSetupDone("consume");
        this.consumeAsyncInternal(p2, 0, p3);
        return;
    }

    void consumeAsyncInternal(java.util.List p8, com.google.android.finsky.billing.iab.google.util.IabHelper$OnConsumeFinishedListener p9, com.google.android.finsky.billing.iab.google.util.IabHelper$OnConsumeMultiFinishedListener p10)
    {
        android.os.Handler v4_1 = new android.os.Handler();
        this.flagStartAsync("consume");
        new Thread(new com.google.android.finsky.billing.iab.google.util.IabHelper$3(this, p8, p9, v4_1, p10)).start();
        return;
    }

    public void dispose()
    {
        this.logDebug("Disposing.");
        this.mSetupDone = 0;
        if (this.mServiceConn != null) {
            this.logDebug("Unbinding from service.");
            if (this.mContext != null) {
                this.mContext.unbindService(this.mServiceConn);
            }
        }
        this.mDisposed = 1;
        this.mContext = 0;
        this.mServiceConn = 0;
        this.mService = 0;
        this.mPurchaseListener = 0;
        return;
    }

    public void enableDebugLogging(boolean p1)
    {
        this.checkNotDisposed();
        this.mDebugLog = p1;
        return;
    }

    public void enableDebugLogging(boolean p1, String p2)
    {
        this.checkNotDisposed();
        this.mDebugLog = p1;
        this.mDebugTag = p2;
        return;
    }

    void flagEndAsync()
    {
        this.logDebug(new StringBuilder().append("Ending async operation: ").append(this.mAsyncOperation).toString());
        this.mAsyncOperation = "";
        this.mAsyncInProgress = 0;
        return;
    }

    void flagStartAsync(String p4)
    {
        if (!this.mAsyncInProgress) {
            this.mAsyncOperation = p4;
            this.mAsyncInProgress = 1;
            this.logDebug(new StringBuilder().append("Starting async operation: ").append(p4).toString());
            return;
        } else {
            throw new IllegalStateException(new StringBuilder().append("Can\'t start async operation (").append(p4).append(") because another async operation(").append(this.mAsyncOperation).append(") is in progress.").toString());
        }
    }

    int getResponseCodeFromBundle(android.os.Bundle p5)
    {
        RuntimeException v1_9;
        Long v0_0 = p5.get("RESPONSE_CODE");
        if (v0_0 != null) {
            if (!(v0_0 instanceof Integer)) {
                if (!(v0_0 instanceof Long)) {
                    this.logError("Unexpected type for bundle response code.");
                    this.logError(v0_0.getClass().getName());
                    throw new RuntimeException(new StringBuilder().append("Unexpected type for bundle response code: ").append(v0_0.getClass().getName()).toString());
                } else {
                    v1_9 = ((int) ((Long) v0_0).longValue());
                }
            } else {
                v1_9 = ((Integer) v0_0).intValue();
            }
        } else {
            this.logDebug("Bundle with null response code, assuming OK (known issue)");
            v1_9 = 0;
        }
        return v1_9;
    }

    int getResponseCodeFromIntent(android.content.Intent p5)
    {
        RuntimeException v1_9;
        Long v0_0 = p5.getExtras().get("RESPONSE_CODE");
        if (v0_0 != null) {
            if (!(v0_0 instanceof Integer)) {
                if (!(v0_0 instanceof Long)) {
                    this.logError("Unexpected type for intent response code.");
                    this.logError(v0_0.getClass().getName());
                    throw new RuntimeException(new StringBuilder().append("Unexpected type for intent response code: ").append(v0_0.getClass().getName()).toString());
                } else {
                    v1_9 = ((int) ((Long) v0_0).longValue());
                }
            } else {
                v1_9 = ((Integer) v0_0).intValue();
            }
        } else {
            this.logError("Intent with no response code, assuming OK (known issue)");
            v1_9 = 0;
        }
        return v1_9;
    }

    public boolean handleActivityResult(int p13, int p14, android.content.Intent p15)
    {
        com.google.android.finsky.billing.iab.google.util.IabHelper$OnIabPurchaseFinishedListener v8_59;
        if (p13 == this.mRequestCode) {
            this.checkNotDisposed();
            this.checkSetupDone("handleActivityResult");
            this.flagEndAsync();
            if (p15 != null) {
                int v5 = this.getResponseCodeFromIntent(p15);
                String v4 = p15.getStringExtra("INAPP_PURCHASE_DATA");
                String v0 = p15.getStringExtra("INAPP_DATA_SIGNATURE");
                if ((p14 != -1) || (v5 != 0)) {
                    if (p14 != -1) {
                        if (p14 != 0) {
                            this.logError(new StringBuilder().append("Purchase failed. Result code: ").append(Integer.toString(p14)).append(". Response: ").append(com.google.android.finsky.billing.iab.google.util.IabHelper.getResponseDesc(v5)).toString());
                            com.google.android.finsky.billing.iab.google.util.IabResult v6_1 = new com.google.android.finsky.billing.iab.google.util.IabResult(-1006, "Unknown purchase response.");
                            if (this.mPurchaseListener != null) {
                                this.mPurchaseListener.onIabPurchaseFinished(v6_1, 0);
                            }
                        } else {
                            this.logDebug(new StringBuilder().append("Purchase canceled - Response: ").append(com.google.android.finsky.billing.iab.google.util.IabHelper.getResponseDesc(v5)).toString());
                            com.google.android.finsky.billing.iab.google.util.IabResult v6_3 = new com.google.android.finsky.billing.iab.google.util.IabResult(-1005, "User canceled.");
                            if (this.mPurchaseListener != null) {
                                this.mPurchaseListener.onIabPurchaseFinished(v6_3, 0);
                            }
                        }
                    } else {
                        this.logDebug(new StringBuilder().append("Result code was OK but in-app billing response was not OK: ").append(com.google.android.finsky.billing.iab.google.util.IabHelper.getResponseDesc(v5)).toString());
                        if (this.mPurchaseListener != null) {
                            this.mPurchaseListener.onIabPurchaseFinished(new com.google.android.finsky.billing.iab.google.util.IabResult(v5, "Problem purchashing item."), 0);
                        }
                    }
                } else {
                    this.logDebug("Successful resultcode from purchase activity.");
                    this.logDebug(new StringBuilder().append("Purchase data: ").append(v4).toString());
                    this.logDebug(new StringBuilder().append("Data signature: ").append(v0).toString());
                    this.logDebug(new StringBuilder().append("Extras: ").append(p15.getExtras()).toString());
                    this.logDebug(new StringBuilder().append("Expected item type: ").append(this.mPurchasingItemType).toString());
                    if ((v4 != null) && (v0 != null)) {
                        try {
                            com.google.android.finsky.billing.iab.google.util.Purchase v3_1 = new com.google.android.finsky.billing.iab.google.util.Purchase(this.mPurchasingItemType, v4, v0);
                            try {
                                String v7 = v3_1.getSku();
                            } catch (org.json.JSONException v1) {
                                this.logError("Failed to parse purchase data.");
                                v1.printStackTrace();
                                com.google.android.finsky.billing.iab.google.util.IabResult v6_9 = new com.google.android.finsky.billing.iab.google.util.IabResult(-1002, "Failed to parse purchase data.");
                                if (this.mPurchaseListener != null) {
                                    this.mPurchaseListener.onIabPurchaseFinished(v6_9, 0);
                                }
                                v8_59 = 1;
                                return v8_59;
                            }
                            if (com.google.android.finsky.billing.iab.google.util.Security.verifyPurchase(this.mSignatureBase64, v4, v0)) {
                                this.logDebug("Purchase signature successfully verified.");
                                if (this.mPurchaseListener != null) {
                                    this.mPurchaseListener.onIabPurchaseFinished(new com.google.android.finsky.billing.iab.google.util.IabResult(0, "Success"), v3_1);
                                }
                            } else {
                                this.logError(new StringBuilder().append("Purchase signature verification FAILED for sku ").append(v7).toString());
                                com.google.android.finsky.billing.iab.google.util.IabResult v6_7 = new com.google.android.finsky.billing.iab.google.util.IabResult(-1003, new StringBuilder().append("Signature verification failed for sku ").append(v7).toString());
                                if (this.mPurchaseListener != null) {
                                    this.mPurchaseListener.onIabPurchaseFinished(v6_7, v3_1);
                                }
                                v8_59 = 1;
                                return v8_59;
                            }
                        } catch (org.json.JSONException v1) {
                        }
                    } else {
                        this.logError("BUG: either purchaseData or dataSignature is null.");
                        this.logDebug(new StringBuilder().append("Extras: ").append(p15.getExtras().toString()).toString());
                        com.google.android.finsky.billing.iab.google.util.IabResult v6_11 = new com.google.android.finsky.billing.iab.google.util.IabResult(-1008, "IAB returned null purchaseData or dataSignature");
                        if (this.mPurchaseListener != null) {
                            this.mPurchaseListener.onIabPurchaseFinished(v6_11, 0);
                        }
                        v8_59 = 1;
                        return v8_59;
                    }
                }
                v8_59 = 1;
            } else {
                this.logError("Null data in IAB activity result.");
                com.google.android.finsky.billing.iab.google.util.IabResult v6_13 = new com.google.android.finsky.billing.iab.google.util.IabResult(-1002, "Null data in IAB result");
                if (this.mPurchaseListener != null) {
                    this.mPurchaseListener.onIabPurchaseFinished(v6_13, 0);
                }
                v8_59 = 1;
            }
        } else {
            v8_59 = 0;
        }
        return v8_59;
    }

    public void launchPurchaseFlow(android.app.Activity p7, String p8, int p9, com.google.android.finsky.billing.iab.google.util.IabHelper$OnIabPurchaseFinishedListener p10)
    {
        this.launchPurchaseFlow(p7, p8, p9, p10, "");
        return;
    }

    public void launchPurchaseFlow(android.app.Activity p8, String p9, int p10, com.google.android.finsky.billing.iab.google.util.IabHelper$OnIabPurchaseFinishedListener p11, String p12)
    {
        this.launchPurchaseFlow(p8, p9, "inapp", p10, p11, p12);
        return;
    }

    public void launchPurchaseFlow(android.app.Activity p15, String p16, String p17, int p18, com.google.android.finsky.billing.iab.google.util.IabHelper$OnIabPurchaseFinishedListener p19, String p20)
    {
        this.checkNotDisposed();
        this.checkSetupDone("launchPurchaseFlow");
        this.flagStartAsync("launchPurchaseFlow");
        if ((!p17.equals("subs")) || (this.mSubscriptionsSupported)) {
            try {
                this.logDebug(new StringBuilder().append("Constructing buy intent for ").append(p16).append(", item type: ").append(p17).toString());
                android.os.Bundle v8 = this.mService.getBuyIntent(3, this.mContext.getPackageName(), p16, p17, p20);
                int v12 = this.getResponseCodeFromBundle(v8);
            } catch (android.os.RemoteException v9_0) {
                this.logError(new StringBuilder().append("RemoteException while launching purchase flow for sku ").append(p16).toString());
                v9_0.printStackTrace();
                this.flagEndAsync();
                com.google.android.finsky.billing.iab.google.util.IabResult v13_3 = new com.google.android.finsky.billing.iab.google.util.IabResult(-1001, "Remote exception while starting purchase flow");
                if (p19 != null) {
                    p19.onIabPurchaseFinished(v13_3, 0);
                }
            } catch (android.os.RemoteException v9_1) {
                this.logError(new StringBuilder().append("SendIntentException while launching purchase flow for sku ").append(p16).toString());
                v9_1.printStackTrace();
                this.flagEndAsync();
                com.google.android.finsky.billing.iab.google.util.IabResult v13_5 = new com.google.android.finsky.billing.iab.google.util.IabResult(-1004, "Failed to send intent.");
                if (p19 != null) {
                    p19.onIabPurchaseFinished(v13_5, 0);
                }
            }
            if (v12 == 0) {
                android.app.PendingIntent v10_1 = ((android.app.PendingIntent) v8.getParcelable("BUY_INTENT"));
                this.logDebug(new StringBuilder().append("Launching buy intent for ").append(p16).append(". Request code: ").append(p18).toString());
                this.mRequestCode = p18;
                this.mPurchaseListener = p19;
                this.mPurchasingItemType = p17;
                p15.startIntentSenderForResult(v10_1.getIntentSender(), p18, new android.content.Intent(), Integer.valueOf(0).intValue(), Integer.valueOf(0).intValue(), Integer.valueOf(0).intValue());
            } else {
                this.logError(new StringBuilder().append("Unable to buy item, Error response: ").append(com.google.android.finsky.billing.iab.google.util.IabHelper.getResponseDesc(v12)).toString());
                this.flagEndAsync();
                com.google.android.finsky.billing.iab.google.util.IabResult v13_1 = new com.google.android.finsky.billing.iab.google.util.IabResult(v12, "Unable to buy item");
                if (p19 != null) {
                    p19.onIabPurchaseFinished(v13_1, 0);
                }
            }
        } else {
            com.google.android.finsky.billing.iab.google.util.IabResult v11_1 = new com.google.android.finsky.billing.iab.google.util.IabResult(-1009, "Subscriptions are not available.");
            this.flagEndAsync();
            if (p19 != null) {
                p19.onIabPurchaseFinished(v11_1, 0);
            }
        }
        return;
    }

    public void launchSubscriptionPurchaseFlow(android.app.Activity p7, String p8, int p9, com.google.android.finsky.billing.iab.google.util.IabHelper$OnIabPurchaseFinishedListener p10)
    {
        this.launchSubscriptionPurchaseFlow(p7, p8, p9, p10, "");
        return;
    }

    public void launchSubscriptionPurchaseFlow(android.app.Activity p8, String p9, int p10, com.google.android.finsky.billing.iab.google.util.IabHelper$OnIabPurchaseFinishedListener p11, String p12)
    {
        this.launchPurchaseFlow(p8, p9, "subs", p10, p11, p12);
        return;
    }

    void logDebug(String p2)
    {
        if (this.mDebugLog) {
            android.util.Log.d(this.mDebugTag, p2);
        }
        return;
    }

    void logError(String p4)
    {
        android.util.Log.e(this.mDebugTag, new StringBuilder().append("In-app billing error: ").append(p4).toString());
        return;
    }

    void logWarn(String p4)
    {
        android.util.Log.w(this.mDebugTag, new StringBuilder().append("In-app billing warning: ").append(p4).toString());
        return;
    }

    public com.google.android.finsky.billing.iab.google.util.Inventory queryInventory(boolean p2, java.util.List p3)
    {
        return this.queryInventory(p2, p3, 0);
    }

    public com.google.android.finsky.billing.iab.google.util.Inventory queryInventory(boolean p7, java.util.List p8, java.util.List p9)
    {
        this.checkNotDisposed();
        this.checkSetupDone("queryInventory");
        try {
            com.google.android.finsky.billing.iab.google.util.Inventory v1_1 = new com.google.android.finsky.billing.iab.google.util.Inventory();
            int v2_0 = this.queryPurchases(v1_1, "inapp");
        } catch (org.json.JSONException v0_1) {
            throw new com.google.android.finsky.billing.iab.google.util.IabException(-1001, "Remote exception while refreshing inventory.", v0_1);
        } catch (org.json.JSONException v0_0) {
            throw new com.google.android.finsky.billing.iab.google.util.IabException(-1002, "Error parsing JSON response while refreshing inventory.", v0_0);
        }
        if (v2_0 == 0) {
            if (p7) {
                int v2_1 = this.querySkuDetails("inapp", v1_1, p8);
                if (v2_1 != 0) {
                    throw new com.google.android.finsky.billing.iab.google.util.IabException(v2_1, "Error refreshing inventory (querying prices of items).");
                }
            }
            if (this.mSubscriptionsSupported) {
                int v2_2 = this.queryPurchases(v1_1, "subs");
                if (v2_2 == 0) {
                    if (p7) {
                        int v2_3 = this.querySkuDetails("subs", v1_1, p8);
                        if (v2_3 != 0) {
                            throw new com.google.android.finsky.billing.iab.google.util.IabException(v2_3, "Error refreshing inventory (querying prices of subscriptions).");
                        }
                    }
                } else {
                    throw new com.google.android.finsky.billing.iab.google.util.IabException(v2_2, "Error refreshing inventory (querying owned subscriptions).");
                }
            }
            return v1_1;
        } else {
            throw new com.google.android.finsky.billing.iab.google.util.IabException(v2_0, "Error refreshing inventory (querying owned items).");
        }
    }

    public void queryInventoryAsync(com.google.android.finsky.billing.iab.google.util.IabHelper$QueryInventoryFinishedListener p3)
    {
        this.queryInventoryAsync(1, 0, p3);
        return;
    }

    public void queryInventoryAsync(boolean p2, com.google.android.finsky.billing.iab.google.util.IabHelper$QueryInventoryFinishedListener p3)
    {
        this.queryInventoryAsync(p2, 0, p3);
        return;
    }

    public void queryInventoryAsync(boolean p8, java.util.List p9, com.google.android.finsky.billing.iab.google.util.IabHelper$QueryInventoryFinishedListener p10)
    {
        android.os.Handler v5_1 = new android.os.Handler();
        this.checkNotDisposed();
        this.checkSetupDone("queryInventory");
        this.flagStartAsync("refresh inventory");
        new Thread(new com.google.android.finsky.billing.iab.google.util.IabHelper$2(this, p8, p9, p10, v5_1)).start();
        return;
    }

    int queryPurchases(com.google.android.finsky.billing.iab.google.util.Inventory p17, String p18)
    {
        this.logDebug(new StringBuilder().append("Querying owned items, item type: ").append(p18).toString());
        this.logDebug(new StringBuilder().append("Package name: ").append(this.mContext.getPackageName()).toString());
        int v12 = 0;
        String v1 = 0;
        do {
            this.logDebug(new StringBuilder().append("Calling getPurchases with continuation token: ").append(v1).toString());
            android.os.Bundle v3 = this.mService.getPurchases(3, this.mContext.getPackageName(), p18, v1);
            int v8 = this.getResponseCodeFromBundle(v3);
            this.logDebug(new StringBuilder().append("Owned items response: ").append(String.valueOf(v8)).toString());
            if (v8 == 0) {
                if ((v3.containsKey("INAPP_PURCHASE_ITEM_LIST")) && ((v3.containsKey("INAPP_PURCHASE_DATA_LIST")) && (v3.containsKey("INAPP_DATA_SIGNATURE_LIST")))) {
                    java.util.ArrayList v4 = v3.getStringArrayList("INAPP_PURCHASE_ITEM_LIST");
                    java.util.ArrayList v7 = v3.getStringArrayList("INAPP_PURCHASE_DATA_LIST");
                    java.util.ArrayList v10 = v3.getStringArrayList("INAPP_DATA_SIGNATURE_LIST");
                    int v2 = 0;
                    while (v2 < v7.size()) {
                        String v6_1 = ((String) v7.get(v2));
                        String v9_1 = ((String) v10.get(v2));
                        if (!com.google.android.finsky.billing.iab.google.util.Security.verifyPurchase(this.mSignatureBase64, v6_1, v9_1)) {
                            this.logWarn("Purchase signature verification **FAILED**. Not adding item.");
                            this.logDebug(new StringBuilder().append("   Purchase data: ").append(v6_1).toString());
                            this.logDebug(new StringBuilder().append("   Signature: ").append(v9_1).toString());
                            v12 = 1;
                        } else {
                            this.logDebug(new StringBuilder().append("Sku is owned: ").append(((String) v4.get(v2))).toString());
                            com.google.android.finsky.billing.iab.google.util.Purchase v5_1 = new com.google.android.finsky.billing.iab.google.util.Purchase(p18, v6_1, v9_1);
                            if (android.text.TextUtils.isEmpty(v5_1.getToken())) {
                                this.logWarn("BUG: empty/null token!");
                                this.logDebug(new StringBuilder().append("Purchase data: ").append(v6_1).toString());
                            }
                            p17.addPurchase(v5_1);
                        }
                        v2++;
                    }
                    v1 = v3.getString("INAPP_CONTINUATION_TOKEN");
                    this.logDebug(new StringBuilder().append("Continuation token: ").append(v1).toString());
                } else {
                    this.logError("Bundle returned from getPurchases() doesn\'t contain required fields.");
                    v8 = -1002;
                }
            } else {
                this.logDebug(new StringBuilder().append("getPurchases() failed: ").append(com.google.android.finsky.billing.iab.google.util.IabHelper.getResponseDesc(v8)).toString());
            }
            return v8;
        } while(!android.text.TextUtils.isEmpty(v1));
        int v13_38;
        if (v12 == 0) {
            v13_38 = 0;
        } else {
            v13_38 = -1003;
        }
        v8 = v13_38;
        return v8;
    }

    int querySkuDetails(String p12, com.google.android.finsky.billing.iab.google.util.Inventory p13, java.util.List p14)
    {
        int v2 = 0;
        this.logDebug("Querying SKU details.");
        java.util.ArrayList v6_1 = new java.util.ArrayList();
        v6_1.addAll(p13.getAllOwnedSkus(p12));
        if (p14 != null) {
            String v8_2 = p14.iterator();
            while (v8_2.hasNext()) {
                String v4_1 = ((String) v8_2.next());
                if (!v6_1.contains(v4_1)) {
                    v6_1.add(v4_1);
                }
            }
        }
        if (v6_1.size() != 0) {
            android.os.Bundle v1_1 = new android.os.Bundle();
            v1_1.putStringArrayList("ITEM_ID_LIST", v6_1);
            android.os.Bundle v5 = this.mService.getSkuDetails(3, this.mContext.getPackageName(), p12, v1_1);
            if (v5.containsKey("DETAILS_LIST")) {
                String v8_9 = v5.getStringArrayList("DETAILS_LIST").iterator();
                while (v8_9.hasNext()) {
                    com.google.android.finsky.billing.iab.google.util.SkuDetails v0_1 = new com.google.android.finsky.billing.iab.google.util.SkuDetails(p12, ((String) v8_9.next()));
                    this.logDebug(new StringBuilder().append("Got sku details: ").append(v0_1).toString());
                    p13.addSkuDetails(v0_1);
                }
            } else {
                v2 = this.getResponseCodeFromBundle(v5);
                if (v2 == 0) {
                    this.logError("getSkuDetails() returned a bundle with neither an error nor a detail list.");
                    v2 = -1002;
                } else {
                    this.logDebug(new StringBuilder().append("getSkuDetails() failed: ").append(com.google.android.finsky.billing.iab.google.util.IabHelper.getResponseDesc(v2)).toString());
                }
            }
        } else {
            this.logDebug("queryPrices: nothing to do because there are no SKUs.");
        }
        return v2;
    }

    public void startSetup(com.google.android.finsky.billing.iab.google.util.IabHelper$OnIabSetupFinishedListener p5)
    {
        this.checkNotDisposed();
        if (!this.mSetupDone) {
            this.logDebug("Starting in-app billing setup.");
            this.mServiceConn = new com.google.android.finsky.billing.iab.google.util.IabHelper$1(this, p5);
            android.content.Intent v0_1 = new android.content.Intent("com.android.vending.billing.InAppBillingService.BIND");
            if (this.mContext.getPackageManager().queryIntentServices(v0_1, 0).isEmpty()) {
                if (p5 != null) {
                    p5.onIabSetupFinished(new com.google.android.finsky.billing.iab.google.util.IabResult(3, "Billing service unavailable on device."));
                }
            } else {
                this.mContext.bindService(v0_1, this.mServiceConn, 1);
            }
            return;
        } else {
            throw new IllegalStateException("IAB helper is already set up.");
        }
    }

    public boolean subscriptionsSupported()
    {
        this.checkNotDisposed();
        return this.mSubscriptionsSupported;
    }
}
