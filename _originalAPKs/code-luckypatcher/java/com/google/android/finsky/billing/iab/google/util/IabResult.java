package com.google.android.finsky.billing.iab.google.util;
public class IabResult {
    String mMessage;
    int mResponse;

    public IabResult(int p3, String p4)
    {
        this.mResponse = p3;
        if ((p4 != null) && (p4.trim().length() != 0)) {
            this.mMessage = new StringBuilder().append(p4).append(" (response: ").append(com.google.android.finsky.billing.iab.google.util.IabHelper.getResponseDesc(p3)).append(")").toString();
        } else {
            this.mMessage = com.google.android.finsky.billing.iab.google.util.IabHelper.getResponseDesc(p3);
        }
        return;
    }

    public String getMessage()
    {
        return this.mMessage;
    }

    public int getResponse()
    {
        return this.mResponse;
    }

    public boolean isFailure()
    {
        int v0_1;
        if (this.isSuccess()) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    public boolean isSuccess()
    {
        int v0_1;
        if (this.mResponse != 0) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    public String toString()
    {
        return new StringBuilder().append("IabResult: ").append(this.getMessage()).toString();
    }
}
