package com.google.android.finsky.billing.iab.google.util;
public interface IabHelper$OnIabSetupFinishedListener {

    public abstract void onIabSetupFinished();
}
