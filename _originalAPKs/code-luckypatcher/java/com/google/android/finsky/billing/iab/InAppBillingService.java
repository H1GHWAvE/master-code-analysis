package com.google.android.finsky.billing.iab;
public class InAppBillingService extends android.app.Service {
    public static final int BILLING_RESPONSE_RESULT_BILLING_UNAVAILABLE = 3;
    public static final int BILLING_RESPONSE_RESULT_OK = 0;
    public static final String ITEM_TYPE_INAPP = "inapp";
    public static final String ITEM_TYPE_SUBS = "subs";
    public static final String TAG = "BillingHack";
    static android.content.ServiceConnection mServiceConn;
    boolean googleBillingDisabled;
    android.os.IBinder mB;
    private final com.google.android.finsky.billing.iab.google.util.IInAppBillingService$Stub mBinder;
    android.content.Context mContext;
    com.google.android.finsky.billing.iab.google.util.IInAppBillingService mService;
    boolean mSetupDone;
    boolean skipSetupDone;

    public InAppBillingService()
    {
        this.mSetupDone = 0;
        this.skipSetupDone = 0;
        this.googleBillingDisabled = 0;
        this.mB = 0;
        this.mBinder = new com.google.android.finsky.billing.iab.InAppBillingService$6(this);
        return;
    }

    public void connectToBilling(String p12)
    {
        this.startGoogleBilling();
        if (!this.mSetupDone) {
            com.google.android.finsky.billing.iab.InAppBillingService.mServiceConn = new com.google.android.finsky.billing.iab.InAppBillingService$5(this, p12);
            android.content.Intent v5_1 = new android.content.Intent("com.android.vending.billing.InAppBillingService.BIND");
            v5_1.setPackage("com.android.vending");
            v5_1.putExtra("xexe", "lp");
            if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().queryIntentServices(v5_1, 0).isEmpty()) {
                new com.chelpus.Utils("w").waitLP(2000);
            }
            if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().queryIntentServices(v5_1, 0).isEmpty()) {
                System.out.println("Billing service unavailable on device.");
            } else {
                java.io.PrintStream v7_16 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().queryIntentServices(v5_1, 0).iterator();
                while (v7_16.hasNext()) {
                    android.content.pm.ResolveInfo v4_1 = ((android.content.pm.ResolveInfo) v7_16.next());
                    if ((v4_1.serviceInfo.packageName != null) && (v4_1.serviceInfo.packageName.equals("com.android.vending"))) {
                        android.content.ComponentName v1_1 = new android.content.ComponentName(v4_1.serviceInfo.packageName, v4_1.serviceInfo.name);
                        android.content.Intent v5_3 = new android.content.Intent();
                        v5_3.setComponent(v1_1);
                        v5_3.putExtra("xexe", "lp");
                        if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance().bindService(v5_3, com.google.android.finsky.billing.iab.InAppBillingService.mServiceConn, 1)) {
                            int v2 = 0;
                            while ((!this.mSetupDone) && (!this.skipSetupDone)) {
                                new com.chelpus.Utils("w").waitLP(500);
                                v2++;
                                if (v2 == 30) {
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            return;
        } else {
            throw new IllegalStateException("IAB helper is already set up.");
        }
    }

    public android.os.IBinder onBind(android.content.Intent p3)
    {
        if (p3 != null) {
            if ((p3.getStringExtra("xexe") != null) && ((p3.getStringExtra("xexe").equals("lp")) && (p3.getPackage() == null))) {
                System.out.println("Connect from proxy.");
                this.mSetupDone = 0;
                this.skipSetupDone = 1;
            } else {
                System.out.println("Connect from patch.");
            }
        }
        return this.mBinder;
    }

    public void onCreate()
    {
        super.onCreate();
        System.out.println(new StringBuilder().append("create bill+skip:").append(this.skipSetupDone).toString());
        this.mContext = this;
        if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.su) {
            this.startGoogleBilling();
        }
        return;
    }

    public void onDestroy()
    {
        System.out.println("destroy billing");
        super.onDestroy();
        if ((com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.su) && (this.googleBillingDisabled)) {
            new Thread(new com.google.android.finsky.billing.iab.InAppBillingService$3(this)).start();
        }
        return;
    }

    public int onStartCommand(android.content.Intent p3, int p4, int p5)
    {
        if (p3 != null) {
            if ((p3.getStringExtra("xexe") != null) && (p3.getStringExtra("xexe").equals("lp"))) {
                this.skipSetupDone = 1;
            } else {
                System.out.println("Connect from app.");
            }
        }
        return super.onStartCommand(p3, p4, p5);
    }

    public void onTaskRemoved(android.content.Intent p3)
    {
        super.onTaskRemoved(p3);
        System.out.println("on Task Removed billing");
        if ((com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.su) && (this.googleBillingDisabled)) {
            new Thread(new com.google.android.finsky.billing.iab.InAppBillingService$2(this)).start();
        }
        return;
    }

    public boolean onUnbind(android.content.Intent p3)
    {
        System.out.println("destroy billing");
        if ((com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.su) && (this.googleBillingDisabled)) {
            new Thread(new com.google.android.finsky.billing.iab.InAppBillingService$4(this)).start();
        }
        return super.onUnbind(p3);
    }

    void startGoogleBilling()
    {
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.init();
        if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.su) {
            try {
                android.content.pm.PackageInfo v3 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getPackageInfo("com.android.vending", 4);
            } catch (android.content.pm.PackageManager$NameNotFoundException v2) {
                v2.printStackTrace();
            }
            if ((v3 != null) && ((v3.services != null) && (v3.services.length != 0))) {
                int v0 = 0;
                while (v0 < v3.services.length) {
                    try {
                        if (((v3.services[v0].name.endsWith("InAppBillingService")) || (v3.services[v0].name.endsWith("MarketBillingService"))) && (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getComponentEnabledSetting(new android.content.ComponentName("com.android.vending", v3.services[v0].name)) != 1)) {
                            this.googleBillingDisabled = 1;
                            com.chelpus.Utils.market_billing_services(1);
                            new java.util.Timer("FirstRunTimer").schedule(new com.google.android.finsky.billing.iab.InAppBillingService$1(this), 60000);
                        }
                        v0++;
                    } catch (Exception v1) {
                        v1.printStackTrace();
                    }
                }
            }
        }
        return;
    }
}
