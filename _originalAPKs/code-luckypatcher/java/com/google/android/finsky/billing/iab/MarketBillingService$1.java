package com.google.android.finsky.billing.iab;
 class MarketBillingService$1 extends com.android.vending.billing.IMarketBillingService$Stub {
    long request_id;
    final synthetic com.google.android.finsky.billing.iab.MarketBillingService this$0;

    MarketBillingService$1(com.google.android.finsky.billing.iab.MarketBillingService p3)
    {
        this.this$0 = p3;
        this.request_id = -1;
        return;
    }

    private long getNextInAppRequestId()
    {
        this.request_id = (com.google.android.finsky.billing.iab.MarketBillingService.access$200().nextLong() & nan);
        return this.request_id;
    }

    private int updateWithRequestId(android.os.Bundle p3, long p4)
    {
        p3.putLong("REQUEST_ID", p4);
        return 0;
    }

    public long confirmNotifications(String p3, String[] p4)
    {
        return this.getNextInAppRequestId();
    }

    public long restoreTransactions(String p3, long p4)
    {
        return this.getNextInAppRequestId();
    }

    public android.os.Bundle sendBillingRequest(android.os.Bundle p40)
    {
        String v25 = p40.getString("BILLING_REQUEST");
        p40.getInt("API_VERSION", -1);
        String v22 = p40.getString("PACKAGE_NAME");
        String v10 = p40.getString("DEVELOPER_PAYLOAD");
        String v14 = p40.getString("ITEM_ID");
        long v16 = p40.getLong("NONCE");
        String[] v18 = p40.getStringArray("NOTIFY_IDS");
        if (v14 != null) {
            com.google.android.finsky.billing.iab.MarketBillingService.access$002(v14);
        }
        android.os.Bundle v8_1 = new android.os.Bundle();
        String v20_0 = "";
        if (!"CHECK_BILLING_SUPPORTED".equals(v25)) {
            if (!"REQUEST_PURCHASE".equals(v25)) {
                if (!"RESTORE_TRANSACTIONS".equals(v25)) {
                    if (!"GET_PURCHASE_INFORMATION".equals(v25)) {
                        if (!"CONFIRM_NOTIFICATIONS".equals(v25)) {
                            v8_1.putInt("RESPONSE_CODE", 0);
                        } else {
                            System.out.println("CONFIRM_NOTIFICATIONS");
                            v8_1.putInt("RESPONSE_CODE", this.updateWithRequestId(v8_1, this.confirmNotifications(v22, v18)));
                            this.this$0.mNotifier.sendResponseCode(v22, this.request_id, 0);
                        }
                    } else {
                        System.out.println("GET_PURCHASE_INFORMATION");
                        com.chelpus.Utils.getRandom(0, 2147483647);
                        org.json.JSONObject v7_1 = new org.json.JSONObject();
                        org.json.JSONArray v24_1 = new org.json.JSONArray();
                        org.json.JSONObject v23_1 = new org.json.JSONObject();
                        try {
                            v7_1.put("nonce", v16);
                            v23_1.put("notificationId", v18[0]);
                            v23_1.put("orderId", new StringBuilder().append((com.chelpus.Utils.getRandom(7.832953389245686e-242, nan) + com.chelpus.Utils.getRandom(0, 9))).append(".").append(com.chelpus.Utils.getRandom(4.940656458412465e-309, 5.431165199810527e-308)).toString());
                            v23_1.put("packageName", v22);
                            v23_1.put("productId", com.google.android.finsky.billing.iab.MarketBillingService.access$000());
                            v23_1.put("purchaseTime", new Long(System.currentTimeMillis()));
                            v23_1.put("purchaseState", new Integer(0));
                            v23_1.put("developerPayload", com.google.android.finsky.billing.iab.MarketBillingService.access$100());
                            v23_1.put("purchaseToken", com.chelpus.Utils.getRandomStringLowerCase(24));
                            v24_1.put(v23_1);
                            v7_1.put("orders", v24_1);
                        } catch (org.json.JSONException v11_0) {
                            v11_0.printStackTrace();
                        }
                        String v20_1 = v7_1.toString();
                        String v28_0 = "";
                        if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance().getSharedPreferences("config", 4).getBoolean("UnSign", 0)) {
                            System.out.println("send sign");
                            v28_0 = com.chelpus.Utils.gen_sha1withrsa(v20_1);
                        }
                        String v21 = "";
                        if (!v28_0.equals("")) {
                            v21 = "1";
                        }
                        if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance().getSharedPreferences("config", 4).getBoolean("SavePurchase", 0)) {
                            com.google.android.finsky.billing.iab.DbHelper v9_1 = new com.google.android.finsky.billing.iab.DbHelper(this.this$0, v22);
                            String v31_45 = new com.google.android.finsky.billing.iab.ItemsListItem;
                            v31_45(com.google.android.finsky.billing.iab.MarketBillingService.access$000(), v23_1.toString(), v21);
                            v9_1.saveItem(v31_45);
                        }
                        if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance().getSharedPreferences("config", 4).getBoolean("AutoRepeat", 0)) {
                            com.google.android.finsky.billing.iab.DbHelper v9_3 = new com.google.android.finsky.billing.iab.DbHelper(this.this$0, v22);
                            String v31_50 = new com.google.android.finsky.billing.iab.ItemsListItem;
                            v31_50(com.google.android.finsky.billing.iab.MarketBillingService.access$000(), "auto.repeat.LP", v21);
                            v9_3.saveItem(v31_50);
                        }
                        this.this$0.mNotifier.sendPurchaseStateChanged(v22, v20_1, v28_0);
                        this.this$0.mNotifier.sendResponseCode(v22, this.request_id, 0);
                        this.updateWithRequestId(v8_1, this.restoreTransactions(v22, v16));
                        v8_1.putInt("RESPONSE_CODE", 0);
                    }
                } else {
                    System.out.println("RESTORE_TRANSACTIONS");
                    java.util.ArrayList v15_0 = new com.google.android.finsky.billing.iab.DbHelper(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance(), v22).getItems();
                    if (v15_0.size() != 0) {
                        org.json.JSONObject v7_3 = new org.json.JSONObject();
                        org.json.JSONArray v24_3 = new org.json.JSONArray();
                        String v28_1 = "";
                        int v12 = 0;
                        try {
                            while (v12 < v15_0.size()) {
                                if (!((com.google.android.finsky.billing.iab.ItemsListItem) v15_0.get(v12)).pData.equals("auto.repeat.LP")) {
                                    String v32_36 = new org.json.JSONObject;
                                    v32_36(((com.google.android.finsky.billing.iab.ItemsListItem) v15_0.get(v12)).pData);
                                    v24_3.put(v32_36);
                                    if (!((com.google.android.finsky.billing.iab.ItemsListItem) v15_0.get(v12)).pSignature.equals("1")) {
                                        v28_1 = "";
                                    } else {
                                        v28_1 = "1";
                                    }
                                }
                                v12++;
                            }
                        } catch (org.json.JSONException v11_1) {
                            v11_1.printStackTrace();
                            this.this$0.mNotifier.sendPurchaseStateChanged(v22, v20_0, v28_1);
                        }
                        v7_3.put("nonce", v16);
                        v7_3.put("orders", v24_3);
                        v20_0 = v7_3.toString();
                        if (v28_1.equals("1")) {
                            v28_1 = com.chelpus.Utils.gen_sha1withrsa(v20_0);
                        }
                        System.out.println(v20_0);
                    }
                    if (v22.equals("com.dynamixsoftware.printhand")) {
                        org.json.JSONObject v7_5 = new org.json.JSONObject();
                        org.json.JSONArray v24_5 = new org.json.JSONArray();
                        org.json.JSONObject v23_3 = new org.json.JSONObject();
                        try {
                            v7_5.put("nonce", v16);
                            v23_3.put("notificationId", new StringBuilder().append("").append(com.chelpus.Utils.getRandom(7.832953389245686e-242, nan)).toString());
                            v23_3.put("orderId", new StringBuilder().append("").append(com.chelpus.Utils.getRandom(7.832953389245686e-242, nan)).append(com.chelpus.Utils.getRandom(0, 9)).append(".").append(com.chelpus.Utils.getRandom(4.940656458412465e-309, 5.431165199810527e-308)).toString());
                            v23_3.put("packageName", v22);
                            v23_3.put("productId", "printhand.premium");
                            v23_3.put("purchaseTime", new Long(System.currentTimeMillis()));
                            v23_3.put("purchaseState", new Integer(0));
                            v23_3.put("developerPayload", com.google.android.finsky.billing.iab.MarketBillingService.access$100());
                            v23_3.put("purchaseToken", com.chelpus.Utils.getRandomStringLowerCase(24));
                            v24_5.put(v23_3);
                            v7_5.put("orders", v24_5);
                        } catch (org.json.JSONException v11_2) {
                            v11_2.printStackTrace();
                        }
                        String v20_2 = v7_5.toString();
                        String v28_2 = com.chelpus.Utils.gen_sha1withrsa(v20_2);
                        System.out.println(v20_2);
                        this.this$0.mNotifier.sendPurchaseStateChanged(v22, v20_2, v28_2);
                    }
                    this.this$0.mNotifier.sendResponseCode(v22, this.request_id, 0);
                    v8_1.putInt("RESPONSE_CODE", this.updateWithRequestId(v8_1, this.restoreTransactions(v22, v16)));
                }
            } else {
                System.out.println("REQUEST_PURCHASE");
                try {
                    android.content.pm.PackageInfo v29 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getPackageInfo(v22, 0);
                } catch (org.json.JSONException v11_3) {
                    v11_3.printStackTrace();
                }
                android.content.Intent v13_0 = 0;
                if (v29 != null) {
                    long v26 = this.getNextInAppRequestId();
                    v8_1.putLong("REQUEST_ID", v26);
                    v13_0 = new android.content.Intent("android.intent.action.VIEW");
                    v13_0.setClass(this.this$0, com.google.android.finsky.billing.iab.BuyMarketActivity);
                    v13_0.putExtra("assetid", new StringBuilder().append("inapp:").append(v22).append(":").append(v14).toString());
                    v13_0.putExtra("asset_package", v22);
                    com.google.android.finsky.billing.iab.MarketBillingService.access$002(v14);
                    if (v10 == null) {
                        com.google.android.finsky.billing.iab.MarketBillingService.access$102("");
                    } else {
                        com.google.android.finsky.billing.iab.MarketBillingService.access$102(v10);
                    }
                    v13_0.putExtra("asset_version_code", v29.versionCode);
                    v13_0.putExtra("request_id", v26);
                    v13_0.putExtra("packageName", v22);
                    v13_0.putExtra("product", v14);
                    v13_0.putExtra("payload", v10);
                    if (v10 != null) {
                        v13_0.putExtra("developer_payload", v10);
                    }
                    java.util.ArrayList v15_1 = new com.google.android.finsky.billing.iab.DbHelper(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance(), v22).getItems();
                    if (v15_1.size() != 0) {
                        String v31_114 = v15_1.iterator();
                        while (v31_114.hasNext()) {
                            com.google.android.finsky.billing.iab.ItemsListItem v5_1 = ((com.google.android.finsky.billing.iab.ItemsListItem) v31_114.next());
                            if ((v5_1.pData.equals("auto.repeat.LP")) && (v5_1.itemID.equals(v14))) {
                                v13_0.putExtra("autorepeat", v5_1.pSignature);
                            }
                        }
                    }
                }
                v8_1.putParcelable("PURCHASE_INTENT", android.app.PendingIntent.getActivity(this.this$0, 0, v13_0, 1073741824));
                v8_1.putInt("RESPONSE_CODE", 0);
            }
        } else {
            System.out.println("CHECK_BILLING_SUPPORTED");
            v8_1.putInt("RESPONSE_CODE", 0);
        }
        return v8_1;
    }
}
