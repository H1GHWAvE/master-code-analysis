package com.google.android.finsky.billing.iab.google.util;
public class SkuDetails {
    String mDescription;
    String mItemType;
    String mJson;
    String mPrice;
    String mSku;
    String mTitle;
    String mType;

    public SkuDetails(String p2)
    {
        this("inapp", p2);
        return;
    }

    public SkuDetails(String p3, String p4)
    {
        this.mItemType = p3;
        this.mJson = p4;
        org.json.JSONObject v0_1 = new org.json.JSONObject(this.mJson);
        this.mSku = v0_1.optString("productId");
        this.mType = v0_1.optString("type");
        this.mPrice = v0_1.optString("price");
        this.mTitle = v0_1.optString("title");
        this.mDescription = v0_1.optString("description");
        return;
    }

    public String getDescription()
    {
        return this.mDescription;
    }

    public String getPrice()
    {
        return this.mPrice;
    }

    public String getSku()
    {
        return this.mSku;
    }

    public String getTitle()
    {
        return this.mTitle;
    }

    public String getType()
    {
        return this.mType;
    }

    public String toString()
    {
        return new StringBuilder().append("SkuDetails:").append(this.mJson).toString();
    }
}
