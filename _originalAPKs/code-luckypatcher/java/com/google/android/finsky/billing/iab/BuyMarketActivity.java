package com.google.android.finsky.billing.iab;
public class BuyMarketActivity extends android.app.Activity {
    public static final String BUY_INTENT = "org.billinghack.BUY";
    public static final String EXTRA_DEV_PAYLOAD = "payload";
    public static final String EXTRA_PACKAGENAME = "packageName";
    public static final String EXTRA_PRODUCT_ID = "product";
    public static final String TAG = "BillingHack";
    android.widget.CheckBox check;
    android.widget.CheckBox check2;
    android.widget.CheckBox check3;
    public com.google.android.finsky.billing.iab.BuyMarketActivity context;
    String pData;
    public String packageName;

    public BuyMarketActivity()
    {
        this.context = 0;
        this.packageName = "";
        this.check = 0;
        this.check2 = 0;
        this.check3 = 0;
        return;
    }

    public void onConfigurationChanged(android.content.res.Configuration p4)
    {
        int v0 = p4.orientation;
        if (v0 == 2) {
            this.setRequestedOrientation(0);
        }
        if (v0 == 1) {
            this.setRequestedOrientation(1);
        }
        super.onConfigurationChanged(p4);
        return;
    }

    protected void onCreate(android.os.Bundle p15)
    {
        super.onCreate(p15);
        this.context = this;
        android.util.Log.d("BillingHack", "Buy intent!");
        if (p15 == null) {
            this.packageName = this.getIntent().getExtras().getString("packageName");
        } else {
            this.packageName = p15.getString("packageName");
        }
        String v0 = this.getIntent().getExtras().getString("autorepeat");
        if (v0 != null) {
            if (!v0.equals("1")) {
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance().getSharedPreferences("config", 4).edit().putBoolean("UnSign", 1).commit();
            } else {
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance().getSharedPreferences("config", 4).edit().putBoolean("UnSign", 0).commit();
            }
            android.content.Intent v4_1 = new android.content.Intent("com.android.vending.billing.IN_APP_NOTIFY");
            v4_1.setPackage(this.packageName);
            v4_1.putExtra("notification_id", new StringBuilder().append("").append(com.chelpus.Utils.getRandom(7.832953389245686e-242, nan)).toString());
            this.sendBroadcast(v4_1);
            this.finish();
        }
        this.setContentView(2130968594);
        android.widget.Button v2_1 = ((android.widget.Button) this.findViewById(2131558407));
        android.widget.Button v1_1 = ((android.widget.Button) this.findViewById(2131558408));
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance().getSharedPreferences("config", 4).edit().putBoolean("UnSign", 0).commit();
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance().getSharedPreferences("config", 4).edit().putBoolean("SavePurchase", 0).commit();
        this.check = ((android.widget.CheckBox) this.findViewById(2131558483));
        this.check2 = ((android.widget.CheckBox) this.findViewById(2131558484));
        this.check3 = ((android.widget.CheckBox) this.findViewById(2131558485));
        android.widget.TextView v3_1 = ((android.widget.TextView) this.findViewById(2131558471));
        v3_1.setText(com.chelpus.Utils.getText(2131165222));
        v3_1.append(new StringBuilder().append("\n").append(com.chelpus.Utils.getText(2131165223)).toString());
        this.check.setChecked(0);
        this.check2.setChecked(0);
        this.check3.setChecked(0);
        if ((!com.chelpus.Utils.checkCoreJarPatch11()) && (!com.chelpus.Utils.isRebuildedOrOdex(this.packageName, this))) {
            this.check.setChecked(1);
        }
        v2_1.setOnClickListener(new com.google.android.finsky.billing.iab.BuyMarketActivity$1(this));
        v1_1.setOnClickListener(new com.google.android.finsky.billing.iab.BuyMarketActivity$2(this));
        return;
    }

    protected void onRestoreInstanceState(android.os.Bundle p3)
    {
        System.out.println("load instance");
        this.packageName = p3.getString("packageName");
        super.onRestoreInstanceState(p3);
        return;
    }

    protected void onSaveInstanceState(android.os.Bundle p3)
    {
        System.out.println("save instance");
        p3.putString("packageName", this.packageName);
        super.onSaveInstanceState(p3);
        return;
    }
}
