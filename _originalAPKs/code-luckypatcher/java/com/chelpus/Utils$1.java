package com.chelpus;
final class Utils$1 implements java.lang.Runnable {
    final synthetic android.app.Activity val$context;
    final synthetic String val$message;
    final synthetic String val$title;

    Utils$1(android.app.Activity p1, String p2, String p3)
    {
        this.val$context = p1;
        this.val$title = p2;
        this.val$message = p3;
        return;
    }

    public void run()
    {
        com.android.vending.billing.InAppBillingService.LUCK.AlertDlg v0_1 = new com.android.vending.billing.InAppBillingService.LUCK.AlertDlg(this.val$context);
        v0_1.setTitle(this.val$title);
        v0_1.setMessage(this.val$message);
        v0_1.setPositiveButton(17039370, 0);
        try {
            com.chelpus.Utils.showDialog(v0_1.create());
        } catch (Exception v1) {
            v1.printStackTrace();
        }
        return;
    }
}
