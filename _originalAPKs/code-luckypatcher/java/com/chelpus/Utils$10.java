package com.chelpus;
final class Utils$10 implements java.lang.Runnable {

    Utils$10()
    {
        return;
    }

    public void run()
    {
        try {
            if (!com.chelpus.Utils.exists("/system/bin/su")) {
                if (com.chelpus.Utils.exists("/system/xbin/su")) {
                    java.io.PrintStream v2_5 = new com.chelpus.Utils("");
                    String v3_2 = new String[1];
                    v3_2[0] = "chmod 06777 internalBusybox";
                    v2_5.cmdRoot(v3_2);
                    if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.su) {
                        java.io.PrintStream v2_8 = new com.chelpus.Utils("");
                        String v3_5 = new String[3];
                        v3_5[0] = "stat -c %a /system/xbin/su";
                        v3_5[1] = new StringBuilder().append(com.chelpus.Utils.access$400()).append(" stat -c %a /system/xbin/su").toString();
                        v3_5[2] = "busybox stat -c %a /system/xbin/su";
                        String v1_0 = v2_8.cmdRoot(v3_5);
                        System.out.println(new StringBuilder().append("LuckyPatcher (chek root): get permissions ").append(v1_0).append(" /system/xbin/su").toString());
                        if ((v1_0.contains("6755")) || (!v1_0.matches("[0-9]"))) {
                            System.out.println("LuckyPatcher (chek root): Permissions is true.(/system/xbin/su)");
                        } else {
                            System.out.println("LuckyPatcher (chek root): Permissions /system/xbin/su not correct.");
                            com.chelpus.Utils.remount("/system", "rw");
                            java.io.PrintStream v2_18 = new com.chelpus.Utils("");
                            String v3_17 = new String[1];
                            v3_17[0] = "chmod 06755 /system/xbin/su";
                            v2_18.cmdRoot(v3_17);
                            com.chelpus.Utils.remount("/system", "ro");
                            java.io.PrintStream v2_21 = new com.chelpus.Utils("");
                            String v3_21 = new String[1];
                            v3_21[0] = "stat -c %a /system/xbin/su";
                            String v1_1 = v2_21.cmdRoot(v3_21);
                            System.out.println(new StringBuilder().append("LuckyPatcher (chek root): ").append(v1_1).append(" /system/xbin/su").toString());
                            if (v1_1.contains("6755")) {
                                System.out.println("LuckyPatcher (chek root): permission /system/xbin/su set 06755");
                            }
                        }
                    } else {
                        System.out.println("LuckyPatcher: skip root test.");
                    }
                }
            } else {
                if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.su) {
                    java.io.PrintStream v2_29 = new com.chelpus.Utils("");
                    String v3_32 = new String[3];
                    v3_32[0] = "stat -c %a /system/bin/su";
                    v3_32[1] = new StringBuilder().append(com.chelpus.Utils.access$400()).append(" stat -c %a /system/bin/su").toString();
                    v3_32[2] = "busybox stat -c %a /system/bin/su";
                    String v1_2 = v2_29.cmdRoot(v3_32);
                    System.out.println(new StringBuilder().append("LuckyPatcher (chek root): get permissions ").append(v1_2).append(" /system/bin/su").toString());
                    if ((v1_2.contains("6755")) || (!v1_2.matches("[0-9]"))) {
                        System.out.println("LuckyPatcher (chek root): Permissions is true.(/system/bin/su)");
                    } else {
                        System.out.println("LuckyPatcher (chek root): Permissions /system/bin/su not correct.");
                        com.chelpus.Utils.remount("/system", "rw");
                        java.io.PrintStream v2_39 = new com.chelpus.Utils("");
                        String v3_44 = new String[1];
                        v3_44[0] = "chmod 06755 /system/bin/su";
                        v2_39.cmdRoot(v3_44);
                        com.chelpus.Utils.remount("/system", "ro");
                        java.io.PrintStream v2_42 = new com.chelpus.Utils("");
                        String v3_48 = new String[1];
                        v3_48[0] = "stat -c %a /system/bin/su";
                        String v1_3 = v2_42.cmdRoot(v3_48);
                        System.out.println(new StringBuilder().append("LuckyPatcher (chek root): ").append(v1_3).append(" /system/bin/su").toString());
                        if (v1_3.contains("6755")) {
                            System.out.println("LuckyPatcher (chek root): permission /system/bin/su set 06755");
                        }
                    }
                } else {
                    System.out.println("LuckyPatcher: skip root test.");
                }
            }
        } catch (Exception v0) {
            v0.printStackTrace();
        }
        return;
    }
}
