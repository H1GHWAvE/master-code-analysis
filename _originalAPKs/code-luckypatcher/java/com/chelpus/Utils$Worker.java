package com.chelpus;
 class Utils$Worker extends java.lang.Thread {
    private String[] commands;
    private Integer exitCode;
    public java.io.DataInputStream input;
    private String result;
    final synthetic com.chelpus.Utils this$0;

    private Utils$Worker(com.chelpus.Utils p3)
    {
        this.this$0 = p3;
        this.result = "";
        this.commands = 0;
        this.exitCode = 0;
        this.input = 0;
        return;
    }

    synthetic Utils$Worker(com.chelpus.Utils p1, com.chelpus.Utils$1 p2)
    {
        this(p1);
        return;
    }

    static synthetic String[] access$100(com.chelpus.Utils$Worker p1)
    {
        return p1.commands;
    }

    static synthetic String[] access$102(com.chelpus.Utils$Worker p0, String[] p1)
    {
        p0.commands = p1;
        return p1;
    }

    static synthetic Integer access$200(com.chelpus.Utils$Worker p1)
    {
        return p1.exitCode;
    }

    static synthetic String access$300(com.chelpus.Utils$Worker p1)
    {
        return p1.result;
    }

    public void run()
    {
        int v5 = 0;
        int v7 = 0;
        int v2 = 0;
        int v11 = 0;
        int v10 = 0;
        String v16_0 = this.commands;
        String v15_0 = 0;
        while (v15_0 < v16_0.length) {
            String v3_1 = v16_0[v15_0];
            if (v3_1.equals("skipOut")) {
                v11 = 1;
            }
            if (v3_1.contains("-Xbootclasspath:")) {
                v5 = 1;
            }
            if ((v3_1.contains(".runpatchsupport ")) || ((v3_1.contains(".runpatchsupportOld ")) || ((v3_1.contains(".runpatchads ")) || ((v3_1.contains(".odexrunpatch ")) || (v3_1.contains(".custompatch ")))))) {
                v10 = 1;
            }
            // Both branches of the condition point to the same code.
            // if (!v3_1.contains(".custompatch ")) {
                v15_0++;
            // }
        }
        while ((!this.result.contains("SU Java-Code Running!")) && (v7 == 0)) {
            if (this.commands[0].contains("env LD_LIBRARY_PATH=")) {
                System.out.println(new StringBuilder().append("re-run Dalvik on root with environment ").append(this.commands[0]).toString());
            }
            try {
                if (this.commands[0].equals("checkRoot")) {
                    System.out.println("LuckyPatcher: test root.");
                    this.commands[0] = "ps init";
                    v2 = 1;
                }
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.countRoot = (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.countRoot + 1);
                System.out.println(new StringBuilder().append("Block root thread").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.countRoot).toString());
                try {
                    if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.semaphoreRoot.tryAcquire(300, java.util.concurrent.TimeUnit.SECONDS)) {
                        System.out.println("Root command timeout. Bad root.");
                        com.chelpus.Utils.exitRoot();
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.semaphoreRoot.release();
                    }
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.countRoot = (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.countRoot - 1);
                    com.chelpus.Utils.getRoot();
                    System.out.println(new StringBuilder().append("UNBlock root thread N").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.countRoot).toString());
                    String v16_25 = this.commands;
                    String v17_8 = v16_25.length;
                    String v15_22 = 0;
                    while (v15_22 < v17_8) {
                        String v3_0 = v16_25[v15_22];
                        if (v5 != 0) {
                            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.suOutputStream.writeBytes(new String(new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.toolfilesdir).append("/busybox killall dalvikvm\n").toString().getBytes(), "ISO-8859-1"));
                        }
                        if (!v3_0.equals("skipOut")) {
                            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.suOutputStream.writeBytes(new String(new StringBuilder().append(v3_0).append("\n").toString().getBytes(), "ISO-8859-1"));
                        }
                        v15_22++;
                    }
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.suOutputStream.writeBytes("echo \'chelpus done!\'\n");
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.suOutputStream.flush();
                    if (v11 != 0) {
                        break;
                    }
                    this.result = this.this$0.getInput(v10, this);
                    if (v10 != 0) {
                        v10 = 0;
                    }
                    try {
                        if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.suErrorInputStream == null) {
                            break;
                        }
                        byte[] v1 = new byte[com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.suErrorInputStream.available()];
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.suErrorInputStream.read(v1);
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.errorOutput = new String(v1);
                        if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.errorOutput.contains("env: not found")) {
                            com.chelpus.Utils.exitRoot();
                        }
                        if (new String(v1).trim().equals("")) {
                            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.errorOutput = "";
                        } else {
                            System.out.println(new StringBuilder().append("LuckyApcther root errors: ").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.errorOutput).toString());
                            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.errorOutput = new String(v1);
                        }
                        if (v5 == 0) {
                            v7 = 1;
                        } else {
                            int v8 = 0;
                            while (v8 < this.commands.length) {
                                if (this.commands[v8].contains("-Xbootclasspath:")) {
                                    if ((!this.commands[v8].contains("env LD_LIBRARY_PATH=")) && ((!this.commands[v8].contains(".checkDataSize ")) && (!this.commands[v8].contains(".custompatch ")))) {
                                        String v14 = System.getenv("LD_LIBRARY_PATH");
                                        if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.errorOutput.contains("env: not found")) {
                                            v7 = 1;
                                        } else {
                                            this.commands[v8] = new StringBuilder().append("env LD_LIBRARY_PATH=").append(v14).append(" ").append(this.commands[v8]).toString();
                                        }
                                        if (!this.result.contains("SU Java-Code Running!")) {
                                            this.result = "";
                                        } else {
                                            v7 = 1;
                                        }
                                    } else {
                                        v7 = 1;
                                        break;
                                    }
                                }
                                v8++;
                            }
                        }
                        return;
                    } catch (Exception v6_1) {
                        v6_1.printStackTrace();
                    } catch (Exception v6_2) {
                        v6_2.printStackTrace();
                        com.chelpus.Utils.exitRoot();
                    }
                } catch (Exception v6_0) {
                    v6_0.printStackTrace();
                }
            } catch (java.io.IOException v9) {
                v9.printStackTrace();
                System.out.println("LuckyPatcher (result root): root not found.");
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.semaphoreRoot.release();
                return;
            } catch (Exception v6_3) {
                v6_3.printStackTrace();
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.semaphoreRoot.release();
                return;
            }
        }
        if (v2 != 0) {
            System.out.println(new StringBuilder().append("LuckyPatcher (result root): ").append(this.result).toString());
        }
        if ((!this.result.equals("")) || (v2 == 0)) {
            if (this.result.equals("")) {
                this.result = "~";
            }
            if (v5 == 0) {
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.semaphoreRoot.release();
            } else {
                com.chelpus.Utils.exitRoot();
            }
        } else {
            if (v5 == 0) {
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.semaphoreRoot.release();
            } else {
                com.chelpus.Utils.exitRoot();
            }
            this.result = "lucky patcher root not found!";
        }
        return;
    }
}
