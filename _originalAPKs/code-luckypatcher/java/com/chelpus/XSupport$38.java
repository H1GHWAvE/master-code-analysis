package com.chelpus;
 class XSupport$38 extends de.robv.android.xposed.XC_MethodHook {
    final synthetic com.chelpus.XSupport this$0;

    XSupport$38(com.chelpus.XSupport p1)
    {
        this.this$0 = p1;
        return;
    }

    protected void afterHookedMethod(de.robv.android.xposed.XC_MethodHook$MethodHookParam p7)
    {
        this.this$0.loadPrefs();
        if ((this.this$0.forHide != null) && ((this.this$0.forHide.booleanValue()) && ((com.chelpus.XSupport.enable) && (com.chelpus.XSupport.hide)))) {
            // Both branches of the condition point to the same code.
            // if (this.this$0.ctx == null) {
                java.util.List v2_1 = ((java.util.List) p7.getResult());
                android.content.pm.PackageInfo v1 = 0;
                java.util.Iterator v3_10 = v2_1.iterator();
                while (v3_10.hasNext()) {
                    android.content.pm.PackageInfo v0_1 = ((android.content.pm.PackageInfo) v3_10.next());
                    if (v0_1.packageName.equals(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPackage().getName())) {
                        v1 = v0_1;
                    }
                }
                if (v1 != null) {
                    p7.setResult(v2_1);
                }
            // }
        }
        return;
    }
}
