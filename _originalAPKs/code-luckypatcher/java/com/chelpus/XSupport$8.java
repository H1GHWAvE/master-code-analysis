package com.chelpus;
 class XSupport$8 extends de.robv.android.xposed.XC_MethodHook {
    final synthetic com.chelpus.XSupport this$0;

    XSupport$8(com.chelpus.XSupport p1)
    {
        this.this$0 = p1;
        return;
    }

    protected void beforeHookedMethod(de.robv.android.xposed.XC_MethodHook$MethodHookParam p7)
    {
        this.this$0.loadPrefs();
        if ((com.chelpus.XSupport.enable) && (com.chelpus.XSupport.patch4)) {
            android.content.Intent v1_1 = ((android.content.Intent) p7.args[0]);
            if (v1_1 != null) {
                if ((((v1_1.getAction() != null) && (com.chelpus.Utils.isMarketIntent(v1_1.getAction()))) || ((v1_1.getComponent() != null) && ((v1_1.getComponent().toString().contains("com.android.vending")) && (v1_1.getComponent().toString().toLowerCase().contains("inappbillingservice"))))) && (this.this$0.checkIntentRework(p7, v1_1, 0, 0))) {
                    android.content.ComponentName v0_1 = new android.content.ComponentName(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPackage().getName(), com.google.android.finsky.billing.iab.InAppBillingService.getName());
                    android.content.Intent v2_1 = new android.content.Intent();
                    v2_1.setComponent(v0_1);
                    p7.args[0] = v2_1;
                }
                if ((((v1_1.getAction() != null) && (v1_1.getAction().toLowerCase().equals("com.android.vending.billing.marketbillingservice.bind"))) || ((v1_1.getComponent() != null) && ((v1_1.getComponent().toString().contains("com.android.vending")) && (v1_1.getComponent().toString().toLowerCase().contains("marketbillingservice"))))) && (this.this$0.checkIntentRework(p7, v1_1, 0, 0))) {
                    android.content.ComponentName v0_3 = new android.content.ComponentName(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPackage().getName(), com.google.android.finsky.billing.iab.MarketBillingService.getName());
                    android.content.Intent v2_3 = new android.content.Intent();
                    v2_3.setComponent(v0_3);
                    p7.args[0] = v2_3;
                }
            }
            if (((v1_1 != null) && (((v1_1.getAction() != null) && (v1_1.getAction().toLowerCase().equals("com.android.vending.licensing.ilicensingservice"))) || ((v1_1.getComponent() != null) && ((v1_1.getComponent().toString().contains("com.android.vending")) && (v1_1.getComponent().toString().toLowerCase().contains("licensingservice")))))) && (this.this$0.checkIntentRework(p7, v1_1, 1, 0))) {
                android.content.ComponentName v0_5 = new android.content.ComponentName(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPackage().getName(), com.google.android.finsky.services.LicensingService.getName());
                android.content.Intent v2_5 = new android.content.Intent();
                v2_5.setComponent(v0_5);
                p7.args[0] = v2_5;
            }
        }
        return;
    }
}
