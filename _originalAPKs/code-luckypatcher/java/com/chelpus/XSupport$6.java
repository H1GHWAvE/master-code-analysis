package com.chelpus;
 class XSupport$6 extends de.robv.android.xposed.XC_MethodHook {
    final synthetic com.chelpus.XSupport this$0;

    XSupport$6(com.chelpus.XSupport p1)
    {
        this.this$0 = p1;
        return;
    }

    protected void beforeHookedMethod(de.robv.android.xposed.XC_MethodHook$MethodHookParam p4)
    {
        this.this$0.loadPrefs();
        if (((com.chelpus.XSupport.enable) && ((com.chelpus.XSupport.patch1) && ((((java.security.Signature) p4.thisObject).getAlgorithm().toLowerCase().equals("sha1withrsa")) || ((((java.security.Signature) p4.thisObject).getAlgorithm().toLowerCase().equals("rsa-sha1")) || (((java.security.Signature) p4.thisObject).getAlgorithm().toLowerCase().equals("1.3.14.3.2.26with1.2.840.113549.1.1.1")))))) && (Integer.valueOf(de.robv.android.xposed.XposedHelpers.getIntField(p4.thisObject, "state")).intValue() == 3)) {
            p4.setResult(Boolean.valueOf(1));
        }
        return;
    }
}
