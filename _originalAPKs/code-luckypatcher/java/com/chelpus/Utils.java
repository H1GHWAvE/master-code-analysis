package com.chelpus;
public class Utils {
    static final String AB = "abcdefghijklmnopqrstuvwxyz";
    static final String AB2 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    private static final String LIB_ART = "libart.so";
    private static final String LIB_ART_D = "libartd.so";
    private static final String LIB_DALVIK = "libdvm.so";
    public static final String ROOT_NOT_FOUND = "lucky patcher root not found!";
    private static final String SELECT_RUNTIME_PROPERTY = "persist.sys.dalvik.vm.lib";
    protected static final char[] hexArray;
    private static String internalBusybox;
    static com.android.vending.billing.InAppBillingService.LUCK.ITestServiceInterface mService;
    static android.content.ServiceConnection mServiceConn;
    public static String pattern_check;
    static java.util.Random rnd;
    float folder_size;

    static Utils()
    {
        com.chelpus.Utils.pattern_check = "297451286";
        com.chelpus.Utils.internalBusybox = "";
        com.chelpus.Utils.hexArray = "0123456789ABCDEF".toCharArray();
        com.chelpus.Utils.mServiceConn = 0;
        com.chelpus.Utils.mService = 0;
        com.chelpus.Utils.rnd = new java.util.Random();
        return;
    }

    public Utils(String p2)
    {
        this.folder_size = 0;
        return;
    }

    public static boolean XZCompress(java.io.File p10, java.io.File p11)
    {
        int v7 = 0;
        try {
            java.io.FileInputStream v2_1 = new java.io.FileInputStream(p10);
            org.tukaani.xz.XZOutputStream v4_1 = new org.tukaani.xz.XZOutputStream(new java.io.FileOutputStream(p11), new org.tukaani.xz.LZMA2Options());
            byte[] v0 = new byte[2048];
        } catch (Exception v1_3) {
            System.out.println("File not found for xz compress.");
            v1_3.printStackTrace();
            return v7;
        } catch (Exception v1_2) {
            System.out.println("Unsupported options for xz compress.");
            v1_2.printStackTrace();
            return v7;
        } catch (Exception v1_0) {
            v1_0.printStackTrace();
            return v7;
        } catch (Exception v1_1) {
            System.out.println("IO Error for xz compress.");
            v1_1.printStackTrace();
            return v7;
        }
        while(true) {
            int v6 = v2_1.read(v0);
            if (v6 == -1) {
                break;
            }
            v4_1.write(v0, 0, v6);
        }
        v4_1.finish();
        if (!p11.exists()) {
            return v7;
        } else {
            v7 = 1;
            return v7;
        }
    }

    public static boolean XZDecompress(java.io.File p12, String p13)
    {
        int v8 = 0;
        byte[] v0 = new byte[2048];
        java.io.File v2_1 = new java.io.File(p13);
        if ((!v2_1.exists()) || (v2_1.isFile())) {
            if (v2_1.isFile()) {
                v2_1.delete();
            }
            v2_1.mkdirs();
        }
        if (v2_1.exists()) {
            java.io.File v1_1;
            if (!p13.endsWith("/")) {
                v1_1 = new java.io.File(new StringBuilder().append(p13).append("/").append(com.chelpus.Utils.removeExtension(p12.getName())).toString());
            } else {
                v1_1 = new java.io.File(new StringBuilder().append(p13).append(com.chelpus.Utils.removeExtension(p12.getName())).toString());
            }
            try {
                String v5 = p12.getAbsolutePath();
                org.tukaani.xz.XZInputStream v4_1 = new org.tukaani.xz.XZInputStream(new java.io.FileInputStream(p12));
                java.io.FileOutputStream v6_1 = new java.io.FileOutputStream(v1_1);
            } catch (java.io.IOException v3_1) {
                System.err.println(new StringBuilder().append("XZDec: Cannot open ").append(v5).append(": ").append(v3_1.getMessage()).toString());
                v1_1.delete();
            } catch (java.io.IOException v3_0) {
                System.err.println(new StringBuilder().append("XZDec: Error decompressing from ").append(v5).append(": ").append(v3_0.getMessage()).toString());
                v1_1.delete();
            } catch (java.io.IOException v3) {
                System.err.println(new StringBuilder().append("XZDec: Unexpected end of input on ").append(v5).toString());
                v1_1.delete();
            }
            while(true) {
                int v7 = v4_1.read(v0);
                if (v7 == -1) {
                    break;
                }
                v6_1.write(v0, 0, v7);
            }
            if (!v1_1.exists()) {
                v1_1.delete();
            } else {
                v8 = 1;
            }
        } else {
            System.out.println("not found dir for ectract xz.");
        }
        return v8;
    }

    static synthetic String access$400()
    {
        return com.chelpus.Utils.internalBusybox;
    }

    public static final void activityToFront()
    {
        android.content.Intent v0_1 = new android.content.Intent(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.getContext(), com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.getActivity().getClass());
        v0_1.setFlags(131072);
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.getContext().startActivity(v0_1);
        return;
    }

    public static void addFileToList(java.io.File p7, java.util.ArrayList p8)
    {
        if ((p8 == null) || (p8.size() <= 0)) {
            if (p8 != null) {
                p8.add(p7);
            }
        } else {
            int v1 = 0;
            java.util.Iterator v2_1 = p8.iterator();
            while (v2_1.hasNext()) {
                if (((java.io.File) v2_1.next()).length() == p7.length()) {
                    v1 = 1;
                }
            }
            if (v1 == 0) {
                p8.add(p7);
            }
        }
        return;
    }

    public static void addFilesToZip(String p21, String p22, java.util.ArrayList p23)
    {
        kellinwood.zipio.ZipInput v10 = kellinwood.zipio.ZipInput.read(p21);
        java.util.Iterator v16_0 = new java.io.FileOutputStream;
        v16_0(p22);
        kellinwood.zipio.ZipOutput v14_1 = new kellinwood.zipio.ZipOutput(v16_0);
        java.util.Iterator v16_3 = v10.getEntries().values().iterator();
        while (v16_3.hasNext()) {
            kellinwood.zipio.ZioEntry v9_1 = ((kellinwood.zipio.ZioEntry) v16_3.next());
            String v12 = v9_1.getName();
            int v11 = 0;
            java.util.Iterator v17_1 = p23.iterator();
            while (v17_1.hasNext()) {
                com.android.vending.billing.InAppBillingService.LUCK.AddFilesItem v7_1 = ((com.android.vending.billing.InAppBillingService.LUCK.AddFilesItem) v17_1.next());
                if (v12.equals(v7_1.fileName.replace(v7_1.basePath, ""))) {
                    try {
                        java.io.File v8_1 = new java.io.File(v7_1.fileName);
                        byte[] v3 = new byte[2048];
                        java.io.FileInputStream v5_1 = new java.io.FileInputStream(v7_1.fileName);
                        kellinwood.zipio.ZioEntry v2_1 = new kellinwood.zipio.ZioEntry(v12);
                        v2_1.setCompression(v9_1.getCompression());
                        v2_1.setTime(v9_1.getTime());
                        java.io.OutputStream v15 = v2_1.getOutputStream();
                        java.util.zip.CRC32 v4_1 = new java.util.zip.CRC32();
                        v4_1.reset();
                    } catch (Exception v6) {
                        System.out.println(v6);
                    }
                    while(true) {
                        int v13 = v5_1.read(v3);
                        if (v13 <= 0) {
                            break;
                        }
                        v15.write(v3, 0, v13);
                        v4_1.update(v3, 0, v13);
                    }
                    v15.flush();
                    v14_1.write(v2_1);
                    v11 = 1;
                    v8_1.delete();
                    System.out.println(new StringBuilder().append("LuckyPatcher (signer): Additional files added! ").append(v7_1).toString());
                    v5_1.close();
                    v15.close();
                }
            }
            if (v11 == 0) {
                v14_1.write(v9_1);
            }
        }
        v14_1.close();
        return;
    }

    public static void afterPatch(String p10, String p11, String p12, String p13, String p14)
    {
        System.out.println(new StringBuilder().append("uid:").append(p13).toString());
        if ((p10.contains("copyDC")) || (p10.contains("deleteDC"))) {
            java.io.File v0 = com.chelpus.Utils.getFileDalvikCache(p11);
            if (v0 == null) {
                if (p10.contains("copyDC")) {
                    java.io.File v1 = com.chelpus.Utils.getFileDalvikCacheName(p11);
                    com.chelpus.Utils.copyFile(new java.io.File(p12), v1);
                    if ((v1.exists()) && (v1.length() == new java.io.File(p12).length())) {
                        new java.io.File(p12).delete();
                        String[] v2_14 = new String[3];
                        v2_14[0] = "chmod";
                        v2_14[1] = "644";
                        v2_14[2] = v1.getAbsolutePath();
                        com.chelpus.Utils.run_all_no_root(v2_14);
                        String[] v2_15 = new String[3];
                        v2_15[0] = "chown";
                        v2_15[1] = new StringBuilder().append("1000:").append(p13).toString();
                        v2_15[2] = v1.getAbsolutePath();
                        com.chelpus.Utils.run_all_no_root(v2_15);
                        String[] v2_16 = new String[3];
                        v2_16[0] = "chown";
                        v2_16[1] = new StringBuilder().append("1000.").append(p13).toString();
                        v2_16[2] = v1.getAbsolutePath();
                        com.chelpus.Utils.run_all_no_root(v2_16);
                    }
                }
            } else {
                v0.delete();
                if (p10.contains("copyDC")) {
                    com.chelpus.Utils.copyFile(new java.io.File(p12), v0);
                    if ((v0.exists()) && (v0.length() == new java.io.File(p12).length())) {
                        new java.io.File(p12).delete();
                        String[] v2_26 = new String[3];
                        v2_26[0] = "chmod";
                        v2_26[1] = "644";
                        v2_26[2] = v0.getAbsolutePath();
                        com.chelpus.Utils.run_all_no_root(v2_26);
                        String[] v2_27 = new String[3];
                        v2_27[0] = "chown";
                        v2_27[1] = new StringBuilder().append("1000:").append(p13).toString();
                        v2_27[2] = v0.getAbsolutePath();
                        com.chelpus.Utils.run_all_no_root(v2_27);
                        String[] v2_28 = new String[3];
                        v2_28[0] = "chown";
                        v2_28[1] = new StringBuilder().append("1000.").append(p13).toString();
                        v2_28[2] = v0.getAbsolutePath();
                        com.chelpus.Utils.run_all_no_root(v2_28);
                    }
                }
            }
        }
        return;
    }

    public static String apply_TAGS(String p1, String p2)
    {
        return p1.replaceAll("%PACKAGE_NAME%", p2);
    }

    public static String bytesToHex(byte[] p6)
    {
        char[] v0 = new char[(p6.length * 2)];
        int v1 = 0;
        while (v1 < p6.length) {
            int v2 = (p6[v1] & 255);
            v0[(v1 * 2)] = com.chelpus.Utils.hexArray[(v2 >> 4)];
            v0[((v1 * 2) + 1)] = com.chelpus.Utils.hexArray[(v2 & 15)];
            v1++;
        }
        return new String(v0);
    }

    private static final void calcChecksum(java.io.File p10)
    {
        try {
            java.nio.channels.FileChannel v0 = new java.io.RandomAccessFile(p10, "rw").getChannel();
            java.nio.MappedByteBuffer v8 = v0.map(java.nio.channels.FileChannel$MapMode.READ_WRITE, 0, ((long) ((int) v0.size())));
            java.util.zip.Adler32 v9_1 = new java.util.zip.Adler32();
            v8.position(12);
        } catch (java.io.IOException v7_1) {
            v7_1.printStackTrace();
            return;
        } catch (java.io.IOException v7_0) {
            v7_0.printStackTrace();
            return;
        }
        while (v8.hasRemaining()) {
            v9_1.update(v8.get());
        }
        int v6 = ((int) v9_1.getValue());
        v8.position(8);
        v8.put(((byte) v6));
        v8.force();
        v8.position(9);
        v8.put(((byte) (v6 >> 8)));
        v8.force();
        v8.position(10);
        v8.put(((byte) (v6 >> 16)));
        v8.force();
        v8.position(11);
        v8.put(((byte) (v6 >> 24)));
        v8.force();
        v0.close();
        return;
    }

    private static void calcChecksumOdexFly(int p10, int p11, java.io.File p12)
    {
        try {
            java.nio.channels.FileChannel v0 = new java.io.RandomAccessFile(p12, "rw").getChannel();
            java.nio.MappedByteBuffer v8 = v0.map(java.nio.channels.FileChannel$MapMode.READ_WRITE, 0, ((long) ((int) v0.size())));
            java.util.zip.Adler32 v9_1 = new java.util.zip.Adler32();
            v8.position((p10 + 12));
        } catch (java.io.IOException v7_1) {
            v7_1.printStackTrace();
            return;
        } catch (java.io.IOException v7_0) {
            v7_0.printStackTrace();
            return;
        }
        while (p11 > 0) {
            v9_1.update(v8.get());
            p11--;
        }
        int v6 = ((int) v9_1.getValue());
        v8.position((p10 + 8));
        v8.put(((byte) v6));
        v8.force();
        v8.position((p10 + 9));
        v8.put(((byte) (v6 >> 8)));
        v8.force();
        v8.position((p10 + 10));
        v8.put(((byte) (v6 >> 16)));
        v8.force();
        v8.position((p10 + 11));
        v8.put(((byte) (v6 >> 24)));
        v8.force();
        v0.close();
        return;
    }

    private static final void calcSignature(java.io.File p14)
    {
        try {
            java.nio.channels.FileChannel v0 = new java.io.RandomAccessFile(p14, "rw").getChannel();
            java.nio.MappedByteBuffer v7 = v0.map(java.nio.channels.FileChannel$MapMode.READ_WRITE, 0, ((long) ((int) v0.size())));
            try {
                java.security.MessageDigest v10 = java.security.MessageDigest.getInstance("SHA-1");
            } catch (java.security.NoSuchAlgorithmException v11) {
                throw new RuntimeException(v11);
            }
            byte[] v12 = new byte[20];
            v7.position(32);
            while (v7.hasRemaining()) {
                v10.update(v7.get());
            }
            try {
                int v8 = v10.digest(v12, 0, 20);
            } catch (java.security.DigestException v9) {
                throw new RuntimeException(v9);
            }
            if (v8 == 20) {
                v7.position(12);
                v7.put(v12);
                v7.force();
                v0.close();
                return;
            } else {
                throw new RuntimeException(new StringBuilder().append("unexpected digest write:").append(v8).append("bytes").toString());
            }
        } catch (java.io.IOException v6_0) {
            v6_0.printStackTrace();
            return;
        } catch (java.io.IOException v6_1) {
            v6_1.printStackTrace();
            return;
        }
    }

    public static String changeExtension(String p5, String p6)
    {
        String v1 = "";
        if (p5 != null) {
            String[] v2 = p5.split("\\.");
            int v0 = 0;
            while (v0 < v2.length) {
                if (v0 >= (v2.length - 1)) {
                    v1 = new StringBuilder().append(v1).append(p6).toString();
                } else {
                    v1 = new StringBuilder().append(v1).append(v2[v0]).append(".").toString();
                }
                v0++;
            }
        }
        return v1;
    }

    public static int changePackageNameIds(String p27, String p28, String p29)
    {
        String v15 = new StringBuilder().append("L").append(p28.replaceAll("\\.", "/")).toString();
        String v16 = new StringBuilder().append("L").append(p29.replaceAll("\\.", "/")).toString();
        System.out.println(new StringBuilder().append(v15).append(" ").append(v16).toString());
        System.out.println(new StringBuilder().append("scan: ").append(p27).toString());
        int v19 = 0;
        int v3_11 = new String[3];
        v3_11[0] = "chmod";
        v3_11[1] = "777";
        v3_11[2] = p27;
        com.chelpus.Utils.run_all_no_root(v3_11);
        new java.util.ArrayList();
        try {
            if ((p27 != null) && ((new java.io.File(p27).exists()) && ((new java.io.File(p27).length() != 0) && (new java.io.File(p27).exists())))) {
                try {
                    java.nio.channels.FileChannel v2 = new java.io.RandomAccessFile(p27, "rw").getChannel();
                    java.nio.MappedByteBuffer v10 = v2.map(java.nio.channels.FileChannel$MapMode.READ_WRITE, 0, ((long) ((int) v2.size())));
                    v10.position(56);
                    int v23 = com.chelpus.Utils.convertFourBytesToInt(v10.get(), v10.get(), v10.get(), v10.get());
                    v10.position(com.chelpus.Utils.convertFourBytesToInt(v10.get(), v10.get(), v10.get(), v10.get()));
                    int v24 = v23;
                    int[] v14 = new int[v24];
                    int v11 = 0;
                } catch (Exception v9_0) {
                    v9_0.printStackTrace();
                }
                while (v11 < v24) {
                    v14[v11] = com.chelpus.Utils.convertFourBytesToInt(v10.get(), v10.get(), v10.get(), v10.get());
                    v11++;
                }
                int v4_24 = v14.length;
                int v3_29 = 0;
                while (v3_29 < v4_24) {
                    int v12 = v14[v3_29];
                    int v22 = com.chelpus.Utils.convertByteToInt(v10.get(v12));
                    byte[] v8 = new byte[v22];
                    int v17 = (v12 + 1);
                    v10.position(v17);
                    int v18 = 0;
                    while (v18 < v8.length) {
                        v8[v18] = v10.get();
                        v18++;
                    }
                    if ((v22 >= p29.length()) && (new String(v8).contains(p28))) {
                        byte[] v25_0 = new String(v8).replaceAll(p28, p29).getBytes();
                        if (v25_0.length <= v22) {
                            v10.position(v12);
                            v10.put(((byte) v25_0.length));
                            v10.position(v17);
                            v10.put(v25_0);
                            v10.put(0);
                            v10.force();
                            System.out.println(new StringBuilder().append("Replace string:").append(new String(v8)).toString());
                            v19++;
                        }
                    }
                    if ((v22 >= v15.length()) && (new String(v8).contains(v15))) {
                        byte[] v25_1 = new String(v8).replaceAll(v15, v16).getBytes();
                        if (v25_1.length <= v22) {
                            v10.position(v12);
                            v10.put(((byte) v25_1.length));
                            v10.position(v17);
                            v10.put(v25_1);
                            v10.put(0);
                            v10.force();
                            System.out.println(new StringBuilder().append("Replace string:").append(new String(v8)).toString());
                            v19++;
                        }
                    }
                    v3_29++;
                }
                v2.close();
            }
        } catch (Exception v9_2) {
            System.out.println(v9_2);
        } catch (Exception v9_1) {
            v9_1.printStackTrace();
        }
        return v19;
    }

    public static boolean checkBind(com.android.vending.billing.InAppBillingService.LUCK.BindItem p9)
    {
        int v5_0 = 0;
        if (!p9.TargetDir.trim().startsWith("~chelpus_disabled~")) {
            String v1;
            String v0 = p9.SourceDir.trim();
            String v3 = p9.TargetDir.trim();
            if (v0.endsWith("/")) {
                v1 = v0;
            } else {
                v1 = new StringBuilder().append(v0.trim()).append("/").toString();
            }
            String v4;
            if (v3.endsWith("/")) {
                v4 = v3;
            } else {
                v4 = new StringBuilder().append(v3.trim()).append("/").toString();
            }
            new java.io.File(v3).mkdirs();
            new java.io.File(v0).mkdirs();
            if (!new java.io.File(v3).exists()) {
                com.chelpus.Utils.verify_and_run("mkdir", new StringBuilder().append("-p \'").append(v3).append("\'").toString());
            }
            if (!new java.io.File(v0).exists()) {
                com.chelpus.Utils.verify_and_run("mkdir", new StringBuilder().append("-p \'").append(v0).append("\'").toString());
            }
            try {
                new java.io.File(new StringBuilder().append(v1).append("test.txt").toString()).createNewFile();
            } catch (java.io.IOException v2) {
                v2.printStackTrace();
            }
            com.chelpus.Utils.run_all(new StringBuilder().append("echo \'\' >\'").append(v1).append("test.txt\'").toString());
            if (com.chelpus.Utils.exists(new StringBuilder().append(v4).append("test.txt").toString())) {
                new java.io.File(new StringBuilder().append(v1).append("test.txt").toString()).delete();
                if (com.chelpus.Utils.exists(new StringBuilder().append(v1).append("test.txt").toString())) {
                    com.chelpus.Utils.run_all(new StringBuilder().append("rm \'").append(v1).append("test.txt\'").toString());
                }
                v5_0 = 1;
            } else {
                new java.io.File(new StringBuilder().append(v1).append("test.txt").toString()).delete();
                if (com.chelpus.Utils.exists(new StringBuilder().append(v1).append("test.txt").toString())) {
                    com.chelpus.Utils.run_all(new StringBuilder().append("rm \'").append(v1).append("test.txt\'").toString());
                }
            }
        }
        return v5_0;
    }

    public static boolean checkCoreJarPatch11()
    {
        int v5 = 0;
        try {
            java.security.Signature v4 = java.security.Signature.getInstance("SHA1withRSA");
            v4.initVerify(java.security.KeyFactory.getInstance("RSA").generatePublic(new java.security.spec.RSAPublicKeySpec(new java.math.BigInteger("12345678", 16), new java.math.BigInteger("11", 16))));
            v4.update("367".getBytes());
        } catch (java.security.spec.InvalidKeySpecException v0) {
            return v5;
        } catch (java.security.spec.InvalidKeySpecException v0) {
            v0.printStackTrace();
            return v5;
        } catch (java.security.spec.InvalidKeySpecException v0) {
            return v5;
        } catch (java.security.spec.InvalidKeySpecException v0) {
            return v5;
        }
        if (v4.verify("123098".getBytes())) {
            v5 = 1;
            return v5;
        } else {
            return v5;
        }
    }

    public static boolean checkCoreJarPatch12()
    {
        int v5 = 0;
        try {
            java.security.Signature v4 = java.security.Signature.getInstance("SHA1withRSA");
            v4.initVerify(java.security.KeyFactory.getInstance("RSA").generatePublic(new java.security.spec.RSAPublicKeySpec(new java.math.BigInteger("12345678", 16), new java.math.BigInteger("11", 16))));
            v4.update("367".getBytes());
        } catch (java.security.spec.InvalidKeySpecException v0) {
            v0.printStackTrace();
            return v5;
        } catch (java.security.spec.InvalidKeySpecException v0) {
            return v5;
        } catch (java.security.spec.InvalidKeySpecException v0) {
            return v5;
        } catch (java.security.spec.InvalidKeySpecException v0) {
            return v5;
        }
        if (v4.verify("123098".getBytes(), 1, 5)) {
            v5 = 1;
            return v5;
        } else {
            return v5;
        }
    }

    public static boolean checkCoreJarPatch20()
    {
        return java.security.MessageDigest.isEqual("12".getBytes(), "45".getBytes());
    }

    public static boolean checkCoreJarPatch30(android.content.pm.PackageManager p14)
    {
        try {
            String v5_1 = com.google.android.finsky.billing.iab.google.util.Base64.encode(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getPackageInfo("android", 64).signatures[0].toByteArray()).replaceAll("\n", "");
            try {
                com.google.android.finsky.billing.iab.google.util.Base64.encode(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getPackageInfo(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPackage().getName(), 64).signatures[0].toByteArray());
                String v2 = v5_1.replaceAll("\n", "");
            } catch (android.content.pm.PackageManager$NameNotFoundException v1) {
                v2 = "chelpa";
            }
            int v8_14;
            String[] v4 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPackages();
            if ((v4 == null) || (v4.length <= 0)) {
                v8_14 = 0;
            } else {
                int v10_2 = v4.length;
                int v9_8 = 0;
                while (v9_8 < v10_2) {
                    String v3 = v4[v9_8];
                    if ((!v3.equals("android")) && (!v3.equals(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPackage().getName()))) {
                        try {
                            android.content.pm.Signature[] v7 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getPackageInfo(v3, 64).signatures;
                        } catch (android.content.pm.PackageManager$NameNotFoundException v1) {
                            v1.printStackTrace();
                            System.out.println("dont get Android signature");
                        }
                        if ((v7 != null) && (v7.length == 1)) {
                            int v11_2 = v7.length;
                            int v8_24 = 0;
                            while (v8_24 < v11_2) {
                                String v0_1 = com.google.android.finsky.billing.iab.google.util.Base64.encode(v7[v8_24].toByteArray()).replaceAll("\n", "");
                                if ((v0_1.equals(v5_1)) || (v0_1.equals(v2))) {
                                    v8_24++;
                                } else {
                                    if (p14.checkSignatures(v3, com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance().getPackageName()) != 0) {
                                        v8_14 = 0;
                                        return v8_14;
                                    } else {
                                        v8_14 = 1;
                                        return v8_14;
                                    }
                                }
                            }
                        }
                    }
                    v9_8++;
                }
            }
            return v8_14;
        } catch (android.content.pm.PackageManager$NameNotFoundException v1) {
            v5_1 = "chelpa";
        }
    }

    public static boolean checkCoreJarPatch40()
    {
        int v5 = 0;
        com.chelpus.Utils.mServiceConn = new com.chelpus.Utils$12();
        if (com.chelpus.Utils.mService == null) {
            try {
                android.content.Intent v2_1 = new android.content.Intent("com.android.vending.billing.InAppBillingService.LUCK.ITestServiceInterface.BIND");
                v2_1.setPackage("com.android.vending");
            } catch (Exception v1_1) {
                v1_1.printStackTrace();
            } catch (Exception v1_0) {
                v1_0.printStackTrace();
            }
            if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().queryIntentServices(v2_1, 0).isEmpty()) {
                java.util.Iterator v6_9 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().queryIntentServices(v2_1, 0).iterator();
                while (v6_9.hasNext()) {
                    android.content.pm.ResolveInfo v3_1 = ((android.content.pm.ResolveInfo) v6_9.next());
                    if ((v3_1.serviceInfo.packageName != null) && (v3_1.serviceInfo.packageName.equals(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance().getPackageName()))) {
                        v5 = 1;
                        break;
                    }
                }
            }
        }
        return v5;
    }

    public static boolean checkRoot(Boolean p2, String p3)
    {
        com.chelpus.Utils.internalBusybox = p3;
        new Thread(new com.chelpus.Utils$10()).run();
        return 1;
    }

    public static String checkRuntimeFromCache(String p13)
    {
        try {
            String v10_1;
            if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.api < 19) {
                v10_1 = "DALVIK";
                return v10_1;
            } else {
                java.io.FileInputStream v4_1 = new java.io.FileInputStream(com.chelpus.Utils.getFileDalvikCache(p13));
                byte[] v7 = new byte[7];
                v4_1.read(v7);
                v4_1.close();
                byte[] v5 = new byte[3];
                v5 = {100, 101, 120};
                byte[] v6 = new byte[3];
                v6 = {100, 101, 121};
                int v3 = 0;
                while (v3 < 3) {
                    if ((v7[v3] == v5[v3]) || (v7[v3] == v6[v3])) {
                        v3++;
                    } else {
                        System.out.println(new StringBuilder().append("The magic value is not the expected value ").append(new String(v7)).toString());
                        v10_1 = "ART";
                        return v10_1;
                    }
                }
                v10_1 = "DALVIK";
                return v10_1;
            }
        } catch (Exception v1_1) {
            v1_1.printStackTrace();
        } catch (Exception v1_2) {
            v1_2.printStackTrace();
        } catch (Exception v1_0) {
            v1_0.printStackTrace();
            try {
                System.out.println("Althernative runtime check with java.vm.version");
            } catch (Exception v2) {
                v2.printStackTrace();
                if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.api < 21) {
                    v10_1 = "DALVIK";
                    return v10_1;
                } else {
                    v10_1 = "ART";
                    return v10_1;
                }
                v10_1 = "UNKNOWN";
                return v10_1;
            }
            if (Integer.parseInt(new StringBuilder().append("").append(System.getProperty("java.vm.version").charAt(0)).toString()) <= 1) {
                v10_1 = "DALVIK";
                return v10_1;
            } else {
                v10_1 = "ART";
                return v10_1;
            }
        }
    }

    public static int chmod(java.io.File p10, int p11)
    {
        Class v0 = Class.forName("android.os.FileUtils");
        Object[] v3_1 = new Class[4];
        v3_1[0] = String;
        v3_1[1] = Integer.TYPE;
        v3_1[2] = Integer.TYPE;
        v3_1[3] = Integer.TYPE;
        reflect.Method v1 = v0.getMethod("setPermissions", v3_1);
        Object[] v3_3 = new Object[4];
        v3_3[0] = p10.getAbsolutePath();
        v3_3[1] = Integer.valueOf(p11);
        v3_3[2] = Integer.valueOf(-1);
        v3_3[3] = Integer.valueOf(-1);
        return ((Integer) v1.invoke(0, v3_3)).intValue();
    }

    public static boolean classes_test(java.io.File p12)
    {
        int v8 = 1;
        try {
            java.io.FileInputStream v2_1 = new java.io.FileInputStream(p12);
            java.util.zip.ZipInputStream v7_1 = new java.util.zip.ZipInputStream(v2_1);
            java.util.zip.ZipEntry v6 = v7_1.getNextEntry();
        } catch (Exception v0) {
            try {
                boolean v9_5 = kellinwood.zipio.ZipInput.read(p12.getAbsolutePath()).getEntries().values().iterator();
            } catch (java.io.IOException v1) {
                v1.printStackTrace();
                v8 = 0;
                return v8;
            }
        }
        while (v6 != null) {
            if (!v6.getName().toLowerCase().equals("classes.dex")) {
                v6 = v7_1.getNextEntry();
            } else {
                v7_1.closeEntry();
                return v8;
            }
        }
        v7_1.close();
        v2_1.close();
    }

    public static void clear_dalvik_cache()
    {
        if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.startUnderRoot.booleanValue()) {
            com.chelpus.Utils.run_all("rm /data/dalvik-cache/*.dex");
            com.chelpus.Utils.run_all("rm /data/dalvik-cache/*.oat");
            com.chelpus.Utils.run_all("rm /data/dalvik-cache/*.art");
            com.chelpus.Utils.run_all("rm /data/dalvik-cache/arm/*.dex");
            com.chelpus.Utils.run_all("rm /data/dalvik-cache/arm/*.art");
            com.chelpus.Utils.run_all("rm /data/dalvik-cache/arm/*.oat");
            com.chelpus.Utils.run_all("rm /data/dalvik-cache/arm64/*.dex");
            com.chelpus.Utils.run_all("rm /data/dalvik-cache/arm64/*.art");
            com.chelpus.Utils.run_all("rm /data/dalvik-cache/arm64/*.oat");
            com.chelpus.Utils.run_all("rm /data/dalvik-cache/x86/*.dex");
            com.chelpus.Utils.run_all("rm /data/dalvik-cache/x86/*.art");
            com.chelpus.Utils.run_all("rm /data/dalvik-cache/x86/*.oat");
        } else {
            try {
                if ((com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.api >= 23) && (new java.io.File("/data/app").exists())) {
                    java.io.File[] v0 = new java.io.File("/data/app").listFiles();
                    int v7 = v0.length;
                    int v6_3 = 0;
                    while (v6_3 < v7) {
                        java.io.File v3 = v0[v6_3];
                        if (v3.isDirectory()) {
                            java.io.File[] v2 = v3.listFiles();
                            if ((v2 != null) && (v2.length > 0)) {
                                int v8 = v2.length;
                                int v4_22 = 0;
                                while (v4_22 < v8) {
                                    java.io.File v1 = v2[v4_22];
                                    if ((v1.isDirectory()) && (v1.getName().equals("oat"))) {
                                        System.out.println(new StringBuilder().append("delete folder:").append(v1.getAbsolutePath()).toString());
                                        new com.chelpus.Utils("").deleteFolder(v1);
                                    }
                                    v4_22++;
                                }
                            }
                        }
                        v6_3++;
                    }
                }
            } catch (int v4) {
            }
            int v4_24 = new String[2];
            v4_24[0] = "rm";
            v4_24[1] = "/data/dalvik-cache/*.dex";
            com.chelpus.Utils.run_all_no_root(v4_24);
            int v4_25 = new String[2];
            v4_25[0] = "rm";
            v4_25[1] = "/data/dalvik-cache/*.oat";
            com.chelpus.Utils.run_all_no_root(v4_25);
            int v4_26 = new String[2];
            v4_26[0] = "rm";
            v4_26[1] = "/data/dalvik-cache/*.art";
            com.chelpus.Utils.run_all_no_root(v4_26);
            int v4_27 = new String[2];
            v4_27[0] = "rm";
            v4_27[1] = "/data/dalvik-cache/arm/*.dex";
            com.chelpus.Utils.run_all_no_root(v4_27);
            int v4_28 = new String[2];
            v4_28[0] = "rm";
            v4_28[1] = "/data/dalvik-cache/arm/*.art";
            com.chelpus.Utils.run_all_no_root(v4_28);
            int v4_29 = new String[2];
            v4_29[0] = "rm";
            v4_29[1] = "/data/dalvik-cache/arm/*.oat";
            com.chelpus.Utils.run_all_no_root(v4_29);
            int v4_30 = new String[2];
            v4_30[0] = "rm";
            v4_30[1] = "/data/dalvik-cache/arm64/*.dex";
            com.chelpus.Utils.run_all_no_root(v4_30);
            int v4_31 = new String[2];
            v4_31[0] = "rm";
            v4_31[1] = "/data/dalvik-cache/arm64/*.art";
            com.chelpus.Utils.run_all_no_root(v4_31);
            int v4_32 = new String[2];
            v4_32[0] = "rm";
            v4_32[1] = "/data/dalvik-cache/arm64/*.oat";
            com.chelpus.Utils.run_all_no_root(v4_32);
            int v4_33 = new String[2];
            v4_33[0] = "rm";
            v4_33[1] = "/data/dalvik-cache/x86/*.dex";
            com.chelpus.Utils.run_all_no_root(v4_33);
            int v4_34 = new String[2];
            v4_34[0] = "rm";
            v4_34[1] = "/data/dalvik-cache/x86/*.art";
            com.chelpus.Utils.run_all_no_root(v4_34);
            int v4_35 = new String[2];
            v4_35[0] = "rm";
            v4_35[1] = "/data/dalvik-cache/x86/*.oat";
            com.chelpus.Utils.run_all_no_root(v4_35);
        }
        return;
    }

    public static varargs String cmd(String[] p13)
    {
        int v7 = 0;
        String v5 = "";
        Process v4 = 0;
        int v6 = 0;
        int v8_0 = 0;
        while (v8_0 < p13.length) {
            if (p13[v8_0].equals("skipOut")) {
                v6 = 1;
            }
            v8_0++;
        }
        int v8_1 = p13.length;
        while (v7 < v8_1) {
            String v1_0 = p13[v7];
            try {
                if (!v1_0.equals("skipOut")) {
                    v4 = Runtime.getRuntime().exec(new String(v1_0.getBytes(), "ISO-8859-1"));
                    java.io.BufferedReader v0_1 = new java.io.BufferedReader(new java.io.InputStreamReader(v4.getInputStream()));
                    if (v6 != 0) {
                        new com.chelpus.Utils("w").waitLP(2000);
                        v7++;
                    }
                    while(true) {
                        String v3 = v0_1.readLine();
                        if (v3 == null) {
                            break;
                        }
                        v5 = new StringBuilder().append(v5).append(v3).append("\n").toString();
                    }
                    v4.waitFor();
                }
            } catch (InterruptedException v2_1) {
                v2_1.printStackTrace();
            } catch (InterruptedException v2_0) {
                v2_0.printStackTrace();
            }
        }
        v4.destroy();
        return v5;
    }

    public static varargs String cmdParam(String[] p9)
    {
        String v6 = "";
        try {
            int v5 = Runtime.getRuntime().exec(p9);
            java.io.BufferedReader v1_1 = new java.io.BufferedReader(new java.io.InputStreamReader(v5.getInputStream()));
        } catch (InterruptedException v3_1) {
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.errorOutput = v3_1.toString();
            System.out.println(v3_1.toString());
            if (v5 != 0) {
                v5.destroy();
            }
            return v6;
        } catch (Exception v2_1) {
            v2_1.printStackTrace();
        } catch (InterruptedException v3_0) {
            v3_0.printStackTrace();
        }
        while(true) {
            String v4 = v1_1.readLine();
            if (v4 == null) {
                break;
            }
            v6 = new StringBuilder().append(v6).append(v4).append("\n").toString();
        }
        v5.waitFor();
        try {
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.suErrorInputStream = new java.io.DataInputStream(v5.getErrorStream());
        } catch (Exception v2_0) {
            v2_0.printStackTrace();
        }
        if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.suErrorInputStream == null) {
        } else {
            byte[] v0 = new byte[com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.suErrorInputStream.available()];
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.suErrorInputStream.read(v0);
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.errorOutput = new String(v0);
            if (new String(v0).trim().equals("")) {
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.errorOutput = "";
            } else {
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.errorOutput = new String(v0);
            }
        }
    }

    public static int convertByteToInt(byte p1)
    {
        return (p1 & 255);
    }

    public static int convertFourBytesToInt(byte p2, byte p3, byte p4, byte p5)
    {
        return ((((p5 << 24) | ((p4 & 255) << 16)) | ((p3 & 255) << 8)) | (p2 & 255));
    }

    public static void convertStringToArraysPatch(String p9, String p10, byte[] p11, byte[] p12, byte[] p13, byte[] p14)
    {
        String[] v2_0 = new String[1];
        v2_0[0] = "";
        int v1 = 0;
        String v9_1 = p9.trim();
        v9_1.split("[ \t]+");
        String[] v2_1 = v9_1.split("[ \t]+");
        int v4_0 = 0;
        try {
            while (v4_0 < v2_1.length) {
                if (!v2_1[v4_0].matches("\\?+")) {
                    p12[v4_0] = 0;
                } else {
                    v2_1[v4_0] = "60";
                    p12[v4_0] = 1;
                }
                if (!v2_1[v4_0].toUpperCase().contains("R")) {
                    p11[v4_0] = Integer.valueOf(v2_1[v4_0], 16).byteValue();
                }
                if (v2_1[v4_0].toUpperCase().contains("R")) {
                    p11[v4_0] = Integer.valueOf(v2_1[v4_0].toUpperCase().replace("R", ""), 16).byteValue();
                    p12[v4_0] = 23;
                }
                v4_0++;
            }
        } catch (Exception v0_0) {
            System.out.println(new StringBuilder().append(" ").append(v0_0).toString());
        }
        String v10_1 = p10.trim();
        v10_1.split("[ \t]+");
        String[] v3 = v10_1.split("[ \t]+");
        int v4_1 = 0;
        try {
            while (v4_1 < v3.length) {
                if (!v3[v4_1].matches("\\?+")) {
                    p14[v4_1] = 1;
                } else {
                    v3[v4_1] = "60";
                    p14[v4_1] = 0;
                }
                if (v3[v4_1].toUpperCase().contains("S1")) {
                    v3[v4_1] = "60";
                    p14[v4_1] = 21;
                }
                if (v3[v4_1].toUpperCase().contains("S0")) {
                    v3[v4_1] = "60";
                    p14[v4_1] = 20;
                }
                if (!v3[v4_1].toUpperCase().contains("W")) {
                    p13[v4_1] = Integer.valueOf(v3[v4_1], 16).byteValue();
                }
                if (v3[v4_1].toUpperCase().contains("W")) {
                    p13[v4_1] = Integer.valueOf(v3[v4_1].toUpperCase().replace("W", ""), 16).byteValue();
                    p14[v4_1] = 23;
                }
                v4_1++;
            }
        } catch (Exception v0_1) {
            System.out.println(new StringBuilder().append(" ").append(v0_1).toString());
        }
        if ((p14.length != p12.length) || ((p11.length != p13.length) || ((p13.length < 4) || (p11.length < 4)))) {
            v1 = 1;
        }
        if (v1 != 0) {
            System.out.println("Error: Patterns original and replaced not valid!\n- Dimensions of the original hex-string and repleced must be >3.\n- Dimensions of the original hex-string and repleced must be equal.\n- Pattern hex must be: AF 11 4B ** AA **\nCheck the template file and try again!");
        }
        return;
    }

    public static void convertToPatchItemAuto(java.util.ArrayList p18, java.util.ArrayList p19, java.util.ArrayList p20, java.util.ArrayList p21, java.util.ArrayList p22, java.util.ArrayList p23, java.util.ArrayList p24, Boolean p25)
    {
        int v11 = 0;
        while (v11 < p19.size()) {
            String v16_1 = ((String) p19.get(v11));
            String v17_1 = ((String) p20.get(v11));
            if ((!((Boolean) p24.get(v11)).booleanValue()) && (p25.booleanValue())) {
                v16_1 = com.chelpus.Utils.rework(((String) p19.get(v11)));
                v17_1 = com.chelpus.Utils.rework(((String) p20.get(v11)));
            }
            Boolean v15_1 = ((Boolean) p21.get(v11));
            String v7_1 = ((String) p22.get(v11));
            String[] v12_0 = new String[1];
            v12_0[0] = "";
            int v10 = 0;
            String v16_2 = v16_1.trim();
            v16_2.split("[ \t]+");
            String[] v12_1 = v16_2.split("[ \t]+");
            int[] v3 = new int[v12_1.length];
            byte[] v2 = new byte[v12_1.length];
            int v14_0 = 0;
            try {
                while (v14_0 < v12_1.length) {
                    if (!v12_1[v14_0].matches("\\?+")) {
                        v3[v14_0] = 0;
                    } else {
                        v12_1[v14_0] = "60";
                        v3[v14_0] = 1;
                    }
                    if (!v12_1[v14_0].toUpperCase().contains("R")) {
                        v2[v14_0] = Integer.valueOf(v12_1[v14_0], 16).byteValue();
                    }
                    if (v12_1[v14_0].toUpperCase().contains("R")) {
                        v2[v14_0] = Integer.valueOf(v12_1[v14_0].toUpperCase().replace("R", ""), 16).byteValue();
                        v3[v14_0] = 23;
                    }
                    v14_0++;
                }
            } catch (Exception v9_0) {
                System.out.println(new StringBuilder().append(" ").append(v9_0).toString());
            }
            String v17_2 = v17_1.trim();
            v17_2.split("[ \t]+");
            String[] v13 = v17_2.split("[ \t]+");
            int[] v5 = new int[v13.length];
            byte[] v4 = new byte[v13.length];
            int v14_1 = 0;
            try {
                while (v14_1 < v13.length) {
                    if (!v13[v14_1].matches("\\?+")) {
                        v5[v14_1] = 1;
                    } else {
                        v13[v14_1] = "60";
                        v5[v14_1] = 0;
                    }
                    if (v13[v14_1].toUpperCase().contains("S1")) {
                        v13[v14_1] = "60";
                        v5[v14_1] = 21;
                    }
                    if (v13[v14_1].toUpperCase().contains("S0")) {
                        v13[v14_1] = "60";
                        v5[v14_1] = 20;
                    }
                    if (!v13[v14_1].toUpperCase().contains("W")) {
                        v4[v14_1] = Integer.valueOf(v13[v14_1], 16).byteValue();
                    }
                    if (v13[v14_1].toUpperCase().contains("W")) {
                        v4[v14_1] = Integer.valueOf(v13[v14_1].toUpperCase().replace("W", ""), 16).byteValue();
                        v5[v14_1] = 23;
                    }
                    v14_1++;
                }
            } catch (Exception v9_1) {
                System.out.println(new StringBuilder().append(" ").append(v9_1).toString());
            }
            if ((v5.length != v3.length) || ((v2.length != v4.length) || ((v4.length < 4) || (v2.length < 4)))) {
                v10 = 1;
            }
            if (v10 != 0) {
                System.out.println("Error: Patterns original and replaced not valid!\n- Dimensions of the original hex-string and repleced must be >3.\n- Dimensions of the original hex-string and repleced must be equal.\n- Pattern hex must be: AF 11 4B ** AA **\nCheck the template file and try again!");
            }
            if (v10 == 0) {
                p18.add(new com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto(v2, v3, v4, v5, v15_1.booleanValue(), v7_1, ((String) p23.get(v11))));
            }
            v11++;
        }
        return;
    }

    public static int convertTwoBytesToInt(byte p2, byte p3)
    {
        return (((p3 & 255) << 8) | (p2 & 255));
    }

    public static void copyArchFiles(java.util.zip.ZipInputStream p21, java.util.jar.JarOutputStream p22, java.util.ArrayList p23)
    {
        int v11 = 0;
        com.android.vending.billing.InAppBillingService.LUCK.AddFilesItem[] v9_0 = new com.android.vending.billing.InAppBillingService.LUCK.AddFilesItem[p23.size()];
        com.android.vending.billing.InAppBillingService.LUCK.AddFilesItem[] v9_2 = ((com.android.vending.billing.InAppBillingService.LUCK.AddFilesItem[]) p23.toArray(v9_0));
        byte[] v2 = new byte[8192];
        java.util.zip.CRC32 v4_1 = new java.util.zip.CRC32();
        v4_1.reset();
        java.util.jar.JarEntry v13_0 = 0;
        while(true) {
            java.util.zip.ZipEntry v10 = p21.getNextEntry();
            if (v10 == null) {
                break;
            }
            int v16_0 = v9_2.length;
            int v15_2 = 0;
            int v14 = v13_0;
            while (v15_2 < v16_0) {
                java.util.jar.JarEntry v13_3;
                com.android.vending.billing.InAppBillingService.LUCK.AddFilesItem v7_1 = v9_2[v15_2];
                if (!v10.getName().equals(v7_1.fileName.replace(v7_1.basePath, ""))) {
                    v13_3 = v14;
                } else {
                    try {
                        v13_3 = new java.util.zip.ZipEntry(v10.getName());
                        try {
                            v13_3.setTime(v10.getTime());
                            java.io.File v8_3 = new java.io.File(v7_1.fileName);
                            java.io.FileInputStream v5_3 = new java.io.FileInputStream(v7_1.fileName);
                            int v3 = new byte[((int) v8_3.length())];
                            v5_3.read(v3);
                            v13_3.setSize(v8_3.length());
                            v4_1.update(v3);
                            v13_3.setCrc(v4_1.getValue());
                            v13_3.setMethod(v10.getMethod());
                            v5_3.close();
                            v11 = 1;
                        } catch (Exception v6_1) {
                            System.out.println(v6_1);
                        }
                    } catch (Exception v6_1) {
                        v13_3 = v14;
                    }
                }
                v15_2++;
                v14 = v13_3;
            }
            if (v10.getMethod() != 0) {
                if (v11 != 0) {
                    v13_0 = v14;
                } else {
                    v13_0 = new java.util.jar.JarEntry(v10.getName());
                    p22.putNextEntry(v13_0);
                    v13_0.setTime(v10.getTime());
                }
            } else {
                if (v11 != 0) {
                } else {
                    v13_0 = new java.util.zip.ZipEntry(v10);
                    v13_0.setMethod(0);
                    v13_0.setTime(v10.getTime());
                    v13_0.setCompressedSize(v10.getSize());
                    v13_0.setSize(v10.getSize());
                    p22.putNextEntry(v13_0);
                    v4_1.reset();
                }
            }
            if (v11 != 0) {
                int v16_1 = v9_2.length;
                int v15_13 = 0;
                while (v15_13 < v16_1) {
                    com.android.vending.billing.InAppBillingService.LUCK.AddFilesItem v7_0 = v9_2[v15_13];
                    if (v10.getName().equals(v7_0.fileName.replace(v7_0.basePath, ""))) {
                        try {
                            java.io.File v8_1 = new java.io.File(v7_0.fileName);
                            java.io.FileInputStream v5_1 = new java.io.FileInputStream(v7_0.fileName);
                            p22.putNextEntry(v13_0);
                        } catch (Exception v6_0) {
                            System.out.println(v6_0);
                        }
                        while(true) {
                            int v12_1 = v5_1.read(v2);
                            if (v12_1 <= 0) {
                                break;
                            }
                            p22.write(v2, 0, v12_1);
                        }
                        p22.flush();
                        v8_1.delete();
                        v5_1.close();
                    }
                    v15_13++;
                }
                v11 = 0;
            }
            while(true) {
                int v12_0 = p21.read(v2);
                if (v12_0 <= 0) {
                    break;
                }
                p22.write(v2, 0, v12_0);
                v4_1.update(v2, 0, v12_0);
            }
            p22.flush();
            v13_0.setCrc(v4_1.getValue());
        }
        p22.finish();
        p22.close();
        return;
    }

    public static void copyFile(java.io.File p16, java.io.File p17)
    {
        int v10 = 0;
        try {
            java.nio.channels.FileChannel v1 = new java.io.FileInputStream(p16).getChannel();
            java.nio.channels.FileChannel v6 = new java.io.FileOutputStream(p17).getChannel();
        } catch (java.io.IOException v8_1) {
            v8_1.printStackTrace();
            return;
        }
        try {
            long v11 = v1.size();
            long v2 = 0;
        } catch (long v4_36) {
            if (v1 != null) {
                v1.close();
            }
            if (v6 != null) {
                v6.close();
            }
            throw v4_36;
        } catch (java.io.IOException v8_0) {
            v8_0.printStackTrace();
            if (v8_0.toString().toLowerCase().contains("no space left")) {
                v10 = 1;
            }
            if (!v8_0.toString().toLowerCase().contains("no space left")) {
                System.out.println("try copy with root");
                if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.su) {
                    if (!com.chelpus.Utils.getRootUid()) {
                        com.chelpus.Utils.copyFile(p16.getAbsoluteFile().toString(), p17.getAbsoluteFile().toString(), 1, 1);
                    } else {
                        String v13_0 = p16.getAbsoluteFile().toString();
                        String v7_0 = p17.getAbsoluteFile().toString();
                        long v4_19 = new String[1];
                        v4_19[0] = new StringBuilder().append("dd if=").append(v13_0).append(" of=").append(v7_0).toString();
                        com.chelpus.Utils.cmd(v4_19);
                        if (!new java.io.File(v7_0).exists()) {
                            long v4_26 = new String[1];
                            v4_26[0] = new StringBuilder().append("toolbox dd if=").append(v13_0).append(" of=").append(v7_0).toString();
                            com.chelpus.Utils.cmd(v4_26);
                        } else {
                            if (p16.length() != p17.length()) {
                            }
                        }
                        if (!new java.io.File(v7_0).exists()) {
                            long v4_33 = new String[1];
                            v4_33[0] = new StringBuilder().append("busybox dd if=").append(v13_0).append(" of=").append(v7_0).toString();
                            com.chelpus.Utils.cmd(v4_33);
                        } else {
                            if (p16.length() != p17.length()) {
                            }
                        }
                        if (p16.length() != p17.length()) {
                            p17.delete();
                        }
                    }
                }
            }
            if (v1 != null) {
                v1.close();
            }
            if (v6 == null) {
                if (p16.length() == p17.length()) {
                    return;
                } else {
                    p17.delete();
                    if ((!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.su) || (v10 != 0)) {
                        return;
                    } else {
                        if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.startUnderRoot.booleanValue()) {
                            com.chelpus.Utils.copyFile(p16.getAbsoluteFile().toString(), p17.getAbsoluteFile().toString(), 1, 1);
                            return;
                        } else {
                            String v13_1 = p16.getAbsoluteFile().toString();
                            String v7_1 = p17.getAbsoluteFile().toString();
                            long v4_47 = new String[3];
                            v4_47[0] = "dd";
                            v4_47[1] = new StringBuilder().append("if=").append(v13_1).toString();
                            v4_47[2] = new StringBuilder().append("of=").append(v7_1).toString();
                            com.chelpus.Utils.cmdParam(v4_47);
                            if ((!new java.io.File(v7_1).exists()) || (p16.length() != p17.length())) {
                                long v4_54 = new String[4];
                                v4_54[0] = "toolbox";
                                v4_54[1] = "dd";
                                v4_54[2] = new StringBuilder().append("if=").append(v13_1).toString();
                                v4_54[3] = new StringBuilder().append("of=").append(v7_1).toString();
                                com.chelpus.Utils.cmdParam(v4_54);
                            }
                            if ((!new java.io.File(v7_1).exists()) || (p16.length() != p17.length())) {
                                long v4_61 = new String[4];
                                v4_61[0] = "busybox";
                                v4_61[1] = "dd";
                                v4_61[2] = new StringBuilder().append("if=").append(v13_1).toString();
                                v4_61[3] = new StringBuilder().append("of=").append(v7_1).toString();
                                com.chelpus.Utils.cmdParam(v4_61);
                            }
                            if (p16.length() != p17.length()) {
                                p17.delete();
                                return;
                            } else {
                                return;
                            }
                        }
                    }
                }
            } else {
                v6.close();
            }
        }
        while (v2 < v11) {
            v2 += v1.transferTo(v2, ((long) 67076096), v6);
        }
        if (v1 != null) {
            v1.close();
        }
        if (v6 == null) {
        } else {
            v6.close();
        }
    }

    public static boolean copyFile(String p11, String p12, boolean p13, boolean p14)
    {
        int v6_36;
        int v4 = 1;
        try {
            if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.startUnderRoot.booleanValue()) {
                java.io.File v5_1 = new java.io.File(p11);
                java.io.File v0_1 = new java.io.File(p12);
                if (p13) {
                    com.chelpus.Utils.remount(p12, "RW");
                }
                String v2_0 = "";
                if (p14) {
                    String v3_0 = com.chelpus.Utils.getPermissions(p11);
                    try {
                        if ((v3_0 != null) && (!v3_0.equals(""))) {
                            v2_0 = v3_0;
                        } else {
                            v2_0 = "777";
                        }
                    } catch (Exception v1) {
                        v2_0 = "777";
                    }
                }
                int v6_6 = new String[3];
                v6_6[0] = "dd";
                v6_6[1] = new StringBuilder().append("if=").append(p11).toString();
                v6_6[2] = new StringBuilder().append("of=").append(p12).toString();
                com.chelpus.Utils.cmdParam(v6_6);
                if ((!new java.io.File(p12).exists()) || (v5_1.length() != v0_1.length())) {
                    int v6_13 = new String[4];
                    v6_13[0] = "toolbox";
                    v6_13[1] = "dd";
                    v6_13[2] = new StringBuilder().append("if=").append(p11).toString();
                    v6_13[3] = new StringBuilder().append("of=").append(p12).toString();
                    com.chelpus.Utils.cmdParam(v6_13);
                }
                if ((!new java.io.File(p12).exists()) || (v5_1.length() != v0_1.length())) {
                    int v6_20 = new String[4];
                    v6_20[0] = "busybox";
                    v6_20[1] = "dd";
                    v6_20[2] = new StringBuilder().append("if=").append(p11).toString();
                    v6_20[3] = new StringBuilder().append("of=").append(p12).toString();
                    com.chelpus.Utils.cmdParam(v6_20);
                }
                if ((!new java.io.File(p12).exists()) || (v5_1.length() != v0_1.length())) {
                    int v6_27 = new String[4];
                    v6_27[0] = new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.toolfilesdir).append("/busybox").toString();
                    v6_27[1] = "dd";
                    v6_27[2] = new StringBuilder().append("if=").append(p11).toString();
                    v6_27[3] = new StringBuilder().append("of=").append(p12).toString();
                    com.chelpus.Utils.cmdParam(v6_27);
                }
                if ((!new java.io.File(p12).exists()) || (v5_1.length() != v0_1.length())) {
                    com.chelpus.Utils.copyFile(v5_1, v0_1);
                }
                if (v5_1.length() != v0_1.length()) {
                    v0_1.delete();
                    System.out.println("LuckyPatcher (cmdCopy): error copy... not enougth space to destination.");
                    v6_36 = 0;
                } else {
                    if (p14) {
                        int v6_38 = new String[3];
                        v6_38[0] = "chmod";
                        v6_38[1] = v2_0;
                        v6_38[2] = p12;
                        com.chelpus.Utils.run_all_no_root(v6_38);
                    }
                    v6_36 = 1;
                }
            } else {
                if (!com.chelpus.Utils.exists(p11)) {
                    v6_36 = v4;
                } else {
                    if (p13) {
                        com.chelpus.Utils.remount(p12, "RW");
                    }
                    String v2_1 = "";
                    if (p14) {
                        String v3_1 = com.chelpus.Utils.getPermissions(p11);
                        try {
                            if ((v3_1 != null) && (!v3_1.equals(""))) {
                                v2_1 = v3_1;
                            } else {
                                v2_1 = "777";
                            }
                        } catch (Exception v1) {
                            v2_1 = "777";
                        }
                    }
                    com.chelpus.Utils.exitRoot();
                    int v6_44 = new com.chelpus.Utils("");
                    int v7_21 = new String[1];
                    v7_21[0] = new StringBuilder().append("dd if=").append(p11).append(" of=").append(p12).toString();
                    v6_44.cmdRoot(v7_21);
                    if (!com.chelpus.Utils.exists(p12)) {
                        int v6_47 = new com.chelpus.Utils("");
                        int v7_24 = new String[1];
                        v7_24[0] = new StringBuilder().append("toolbox dd if=").append(p11).append(" of=").append(p12).toString();
                        v6_47.cmdRoot(v7_24);
                    }
                    if (!com.chelpus.Utils.exists(p12)) {
                        int v6_50 = new com.chelpus.Utils("");
                        int v7_27 = new String[1];
                        v7_27[0] = new StringBuilder().append("busybox dd if=").append(p11).append(" of=").append(p12).toString();
                        v6_50.cmdRoot(v7_27);
                    }
                    if (!com.chelpus.Utils.exists(p12)) {
                        int v6_53 = new com.chelpus.Utils("");
                        int v7_30 = new String[1];
                        v7_30[0] = new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.toolfilesdir).append("/busybox dd if=").append(p11).append(" of=").append(p12).toString();
                        v6_53.cmdRoot(v7_30);
                    }
                    if (!com.chelpus.Utils.exists(p12)) {
                        int v6_56 = new com.chelpus.Utils("");
                        int v7_33 = new String[1];
                        v7_33[0] = new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.toolfilesdir).append("/busybox cp -fp ").append(p11).append(" ").append(p12).toString();
                        v6_56.cmdRoot(v7_33);
                    }
                    if (!p14) {
                    } else {
                        com.chelpus.Utils.run_all(new StringBuilder().append("chmod ").append(v2_1).append(" ").append(p12).toString());
                    }
                }
            }
        } catch (Exception v1) {
            v1.printStackTrace();
            v4 = 0;
        }
        return v6_36;
    }

    public static void copyFolder(java.io.File p7, java.io.File p8)
    {
        if (!p7.isDirectory()) {
            com.chelpus.Utils.copyFile(p7, p8);
        } else {
            if (!p8.exists()) {
                p8.mkdir();
                System.out.println(new StringBuilder().append("Directory copied from ").append(p7).append("  to ").append(p8).toString());
            }
            String[] v2 = p7.list();
            if (v2.length > 0) {
                int v5_7 = v2.length;
                int v4_4 = 0;
                while (v4_4 < v5_7) {
                    String v1 = v2[v4_4];
                    com.chelpus.Utils.copyFolder(new java.io.File(p7, v1), new java.io.File(p8, v1));
                    v4_4++;
                }
            }
        }
        return;
    }

    public static int create_DC_root(String p22, String p23, String p24, String p25)
    {
        int v16 = 0;
        String[] v0_1 = new String[3];
        String[] v17_1 = v0_1;
        v17_1[0] = "chmod";
        v17_1[1] = "777";
        v17_1[2] = p22;
        com.chelpus.Utils.run_all_no_root(v17_1);
        String[] v0_3 = new String[3];
        String[] v17_3 = v0_3;
        v17_3[0] = "chown";
        v17_3[1] = "0:0";
        v17_3[2] = p22;
        com.chelpus.Utils.run_all_no_root(v17_3);
        String[] v0_5 = new String[3];
        String[] v17_5 = v0_5;
        v17_5[0] = "chown";
        v17_5[1] = "0.0";
        v17_5[2] = p22;
        com.chelpus.Utils.run_all_no_root(v17_5);
        try {
            java.io.File v8 = com.chelpus.Utils.getFileDalvikCacheName(p23);
        } catch (Exception v10_0) {
            v10_0.printStackTrace();
        }
        if (p25 == null) {
            p25 = com.chelpus.Utils.getOdexForCreate(p23, p24);
        }
        if (p25 != null) {
            String[] v17_6 = new java.io.File;
            v17_6(p25);
            if (v17_6.exists()) {
                com.chelpus.Utils.remount(p25, "RW");
                String[] v17_9 = new java.io.File;
                v17_9(p25);
                v17_9.delete();
            }
        }
        int v11 = 0;
        String[] v17_10 = new java.io.File;
        v17_10(p23);
        v17_10.getName();
        com.chelpus.Utils.getOdexForCreate(p23, p24);
        com.chelpus.Utils.getFileDalvikCacheName(p23);
        String[] v17_11 = new java.io.File;
        v17_11(p23);
        com.chelpus.Utils.changeExtension(v17_11.getName(), "odex");
        dalvik.system.DexFile v9 = 0;
        int v3 = 0;
        try {
            if (!com.chelpus.Utils.getCurrentRuntimeValue().contains("ART")) {
                v3 = 0;
                if (v8.exists()) {
                    v8.delete();
                }
                if (v3 == 0) {
                    v8.delete();
                    v9 = dalvik.system.DexFile.loadDex(p23, new StringBuilder().append(p22).append("/temp.dex").toString(), 0);
                    if (com.chelpus.Utils.dalvikvm_copyFile(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.toolfilesdir, new StringBuilder().append(p22).append("/temp.dex").toString(), v8.getAbsolutePath())) {
                        System.out.println("Free space for dex enougth.");
                        v11 = 1;
                    }
                    new java.io.File(new StringBuilder().append(p22).append("/temp.dex").toString()).delete();
                    if ((v8.exists()) && (v8.length() == 0)) {
                        v8.delete();
                    }
                } else {
                    System.out.println("dex-opt-art");
                    try {
                        System.out.println("try create oat with DexFile:");
                        String v14 = "";
                    } catch (Exception v10_3) {
                        v10_3.printStackTrace();
                    }
                    if (v8.getAbsolutePath().contains("/arm/")) {
                        v14 = "arm";
                    }
                    if (v8.getAbsolutePath().contains("/arm64/")) {
                        v14 = "arm64";
                    }
                    if (v8.getAbsolutePath().contains("/x86/")) {
                        v14 = "x86";
                    }
                    v8.delete();
                    if (v8.exists()) {
                        String[] v0_17 = new String[2];
                        String[] v17_39 = v0_17;
                        v17_39[0] = "rm";
                        v17_39[1] = v8.getAbsolutePath();
                        com.chelpus.Utils.run_all_no_root(v17_39);
                    }
                    System.out.println("try create oat with dex2oat:");
                    String[] v0_19 = new String[4];
                    int v18_32 = v0_19;
                    v18_32[0] = "dex2oat";
                    v18_32[1] = new StringBuilder().append("--dex-file=").append(p23).toString();
                    v18_32[2] = new StringBuilder().append("--oat-file=").append(v8.getAbsolutePath()).toString();
                    v18_32[3] = new StringBuilder().append("--instruction-set=").append(v14).toString();
                    System.out.println(com.chelpus.Utils.cmdParam(v18_32));
                    System.out.println("end");
                    if ((!v8.exists()) || (v8.length() == 0)) {
                        v8.delete();
                        if (v8.exists()) {
                            String[] v0_23 = new String[2];
                            String[] v17_48 = v0_23;
                            v17_48[0] = "rm";
                            v17_48[1] = v8.getAbsolutePath();
                            com.chelpus.Utils.run_all_no_root(v17_48);
                        }
                    } else {
                        System.out.println(new StringBuilder().append("oat created with dex2oat - length=").append(v8.length()).toString());
                    }
                }
                if (!p23.startsWith("/system")) {
                    String[] v0_26 = new String[3];
                    String[] v17_53 = v0_26;
                    v17_53[0] = "chmod";
                    v17_53[1] = "0644";
                    v17_53[2] = v8.getAbsolutePath();
                    com.chelpus.Utils.run_all_no_root(v17_53);
                    String[] v0_28 = new String[3];
                    String[] v17_55 = v0_28;
                    v17_55[0] = "chown";
                    v17_55[1] = new StringBuilder().append("1000.").append(p24).toString();
                    v17_55[2] = v8.getAbsolutePath();
                    com.chelpus.Utils.run_all_no_root(v17_55);
                    String[] v0_31 = new String[3];
                    String[] v17_57 = v0_31;
                    v17_57[0] = "chown";
                    v17_57[1] = new StringBuilder().append("1000:").append(p24).toString();
                    v17_57[2] = v8.getAbsolutePath();
                    com.chelpus.Utils.run_all_no_root(v17_57);
                } else {
                    String[] v0_34 = new String[3];
                    String[] v17_59 = v0_34;
                    v17_59[0] = "chmod";
                    v17_59[1] = "0644";
                    v17_59[2] = v8.getAbsolutePath();
                    com.chelpus.Utils.run_all_no_root(v17_59);
                    String[] v0_36 = new String[3];
                    String[] v17_61 = v0_36;
                    v17_61[0] = "chown";
                    v17_61[1] = "0.0";
                    v17_61[2] = v8.getAbsolutePath();
                    com.chelpus.Utils.run_all_no_root(v17_61);
                    String[] v0_38 = new String[3];
                    String[] v17_63 = v0_38;
                    v17_63[0] = "chown";
                    v17_63[1] = "0:0";
                    v17_63[2] = v8.getAbsolutePath();
                    com.chelpus.Utils.run_all_no_root(v17_63);
                }
            } else {
                v3 = 1;
            }
        } catch (Exception v10_4) {
            v10_4.printStackTrace();
        }
        if (v9 != null) {
            try {
                v9.close();
            } catch (Exception v10_5) {
                v10_5.printStackTrace();
            }
        }
        if (1 != 0) {
            if ((v3 == 0) || (0 == 0)) {
                if ((v3 == 0) || (v11 != 0)) {
                    if ((v3 == 0) || (v11 == 0)) {
                        if ((v3 != 0) || (v11 != 0)) {
                            if ((v3 == 0) && (v11 != 0)) {
                                v16 = 0;
                            }
                        } else {
                            v16 = 1;
                        }
                    } else {
                        v16 = 0;
                    }
                } else {
                    v16 = 1;
                }
            } else {
                v16 = 2;
            }
        } else {
            v16 = 3;
        }
        return v16;
    }

    public static int create_ODEX_root(String p34, java.util.ArrayList p35, String p36, String p37, String p38)
    {
        int v27 = 0;
        String[] v0_1 = new String[3];
        String[] v29_1 = v0_1;
        v29_1[0] = "chmod";
        v29_1[1] = "777";
        v29_1[2] = p34;
        com.chelpus.Utils.run_all_no_root(v29_1);
        String[] v0_3 = new String[3];
        String[] v29_3 = v0_3;
        v29_3[0] = "chown";
        v29_3[1] = "0:0";
        v29_3[2] = p34;
        com.chelpus.Utils.run_all_no_root(v29_3);
        String[] v0_5 = new String[3];
        String[] v29_5 = v0_5;
        v29_5[0] = "chown";
        v29_5[1] = "0.0";
        v29_5[2] = p34;
        com.chelpus.Utils.run_all_no_root(v29_5);
        if (p38 == null) {
            p38 = com.chelpus.Utils.getOdexForCreate(p36, p37);
        }
        int v19 = 0;
        int v20 = 1;
        int v21 = 0;
        String[] v29_6 = new java.io.File;
        v29_6(p36);
        String v6 = v29_6.getName();
        String v5 = com.chelpus.Utils.getOdexForCreate(p36, p37);
        java.io.File v10 = com.chelpus.Utils.getFileDalvikCacheName(p36);
        String[] v29_7 = new java.io.File;
        v29_7(p36);
        String v7 = v29_7.getName();
        String v24 = com.chelpus.Utils.changeExtension(v7, "odex");
        com.chelpus.Utils.changeExtension(v7, "dex");
        String v23_0 = new StringBuilder().append(p34).append("/").append(v24).toString();
        dalvik.system.DexFile v13 = 0;
        int v4 = 0;
        try {
            if (!com.chelpus.Utils.getCurrentRuntimeValue().contains("ART")) {
                v4 = 0;
                String[] v29_17 = p35.iterator();
                while (v29_17.hasNext()) {
                    java.io.File v9_1 = ((java.io.File) v29_17.next());
                    String[] v0_248 = new String[3];
                    int v30_301 = v0_248;
                    v30_301[0] = "chmod";
                    v30_301[1] = "777";
                    v30_301[2] = new StringBuilder().append(p34).append("/").append(v9_1.getName()).toString();
                    com.chelpus.Utils.run_all_no_root(v30_301);
                    if (v9_1.exists()) {
                        String[] v0_251 = new String[3];
                        int v30_304 = v0_251;
                        v30_304[0] = "chmod";
                        v30_304[1] = "777";
                        v30_304[2] = new StringBuilder().append(p34).append("/").append(v9_1.getName()).toString();
                        com.chelpus.Utils.run_all_no_root(v30_304);
                        String[] v0_254 = new String[3];
                        int v30_306 = v0_254;
                        v30_306[0] = "chown";
                        v30_306[1] = new StringBuilder().append("1000.").append(p37).toString();
                        v30_306[2] = new StringBuilder().append(p34).append("/").append(v9_1.getName()).toString();
                        com.chelpus.Utils.run_all_no_root(v30_306);
                    } else {
                        if (0 != 0) {
                            try {
                                0.close();
                            } catch (Exception v16_4) {
                                v16_4.printStackTrace();
                            }
                        }
                        v27 = 4;
                    }
                }
                String[] v0_13 = new String[3];
                String[] v29_19 = v0_13;
                v29_19[0] = "chmod";
                v29_19[1] = "777";
                v29_19[2] = new StringBuilder().append(p34).append("/AndroidManifest.xml").toString();
                com.chelpus.Utils.run_all_no_root(v29_19);
                String[] v0_16 = new String[3];
                String[] v29_21 = v0_16;
                v29_21[0] = "chown";
                v29_21[1] = new StringBuilder().append("1000.").append(p37).toString();
                v29_21[2] = new StringBuilder().append(p34).append("/AndroidManifest.xml").toString();
                com.chelpus.Utils.run_all_no_root(v29_21);
                new java.io.File(new StringBuilder().append(p34).append("/").append(v6).toString()).delete();
                if (new java.io.File(new StringBuilder().append(p34).append("/").append(v6).toString()).exists()) {
                    String[] v0_24 = new String[2];
                    String[] v29_28 = v0_24;
                    v29_28[0] = "rm";
                    v29_28[1] = new StringBuilder().append(p34).append("/").append(v6).toString();
                    com.chelpus.Utils.run_all_no_root(v29_28);
                }
                java.io.File v11 = com.chelpus.Utils.getFileDalvikCacheName(p36);
                v11.getName();
                if (v4 == 0) {
                    com.chelpus.Utils.zip(v6, p34, p35, new StringBuilder().append(p34).append("/").append(v6).toString());
                    String[] v0_31 = new String[3];
                    String[] v29_36 = v0_31;
                    v29_36[0] = "chmod";
                    v29_36[1] = "777";
                    v29_36[2] = new StringBuilder().append(p34).append("/").append(v6).toString();
                    com.chelpus.Utils.run_all_no_root(v29_36);
                    String[] v0_35 = new String[3];
                    String[] v29_38 = v0_35;
                    v29_38[0] = "chown";
                    v29_38[1] = new StringBuilder().append("1000.").append(p37).toString();
                    v29_38[2] = new StringBuilder().append(p34).append("/").append(v6).toString();
                    com.chelpus.Utils.run_all_no_root(v29_38);
                    String[] v0_40 = new String[3];
                    String[] v29_40 = v0_40;
                    v29_40[0] = "chown";
                    v29_40[1] = new StringBuilder().append("1000:").append(p37).toString();
                    v29_40[2] = new StringBuilder().append(p34).append("/").append(v6).toString();
                    com.chelpus.Utils.run_all_no_root(v29_40);
                    if ((new java.io.File(new StringBuilder().append(p34).append("/").append(v6).toString()).exists()) && (new java.io.File(new StringBuilder().append(p34).append("/").append(v6).toString()).length() != 0)) {
                        System.out.println(new StringBuilder().append("LuckyPatcher (CustomPatch): foundreworked apk ").append(p34).append("/").append(v6).toString());
                    }
                    if ((new java.io.File(new StringBuilder().append(p34).append("/").append(v6).toString()).exists()) && (new java.io.File(new StringBuilder().append(p34).append("/").append(v6).toString()).length() == 0)) {
                        new java.io.File(new StringBuilder().append(p34).append("/").append(v6).toString()).delete();
                    }
                    new java.io.File(new StringBuilder().append(p34).append("/").append(v10.getName()).toString()).delete();
                    v13 = dalvik.system.DexFile.loadDex(new StringBuilder().append(p34).append("/").append(v6).toString(), new StringBuilder().append(p34).append("/").append(v10.getName()).toString(), 0);
                    String v15 = v10.getName();
                    System.out.println(v15);
                    System.out.println(new StringBuilder().append(p34).append("/").append(v15).toString());
                    java.io.File v8_1 = new java.io.File(new StringBuilder().append(p34).append("/").append(v15).toString());
                    if ((v8_1.exists()) && (v8_1.length() == 0)) {
                        v8_1.delete();
                    }
                    if (new java.io.File(v8_1.getAbsolutePath()).exists()) {
                        com.chelpus.Utils.fixadlerOdex(v8_1, p36);
                        if (!com.chelpus.Utils.dalvikvm_copyFile(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.toolfilesdir, new StringBuilder().append(p34).append("/").append(v15).toString(), p38)) {
                            String[] v29_82 = new java.io.File;
                            v29_82(v5);
                            v29_82.delete();
                            String[] v29_83 = new java.io.File;
                            v29_83(v5);
                            if (v29_83.exists()) {
                                String[] v0_73 = new String[2];
                                String[] v29_86 = v0_73;
                                v29_86[0] = "rm";
                                v29_86[1] = v5;
                                com.chelpus.Utils.run_all_no_root(v29_86);
                            }
                            com.chelpus.Utils.fixadlerOdex(new java.io.File(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.toolfilesdir, new StringBuilder().append(p34).append("/").append(v15).toString()), p36);
                            if (!com.chelpus.Utils.dalvikvm_copyFile(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.toolfilesdir, new StringBuilder().append(p34).append("/").append(v15).toString(), v10.getAbsolutePath())) {
                                String[] v29_91 = new java.io.File;
                                v29_91(v5);
                                v29_91.delete();
                                String[] v29_92 = new java.io.File;
                                v29_92(v5);
                                if (v29_92.exists()) {
                                    String[] v0_82 = new String[2];
                                    String[] v29_95 = v0_82;
                                    v29_95[0] = "rm";
                                    v29_95[1] = v5;
                                    com.chelpus.Utils.run_all_no_root(v29_95);
                                }
                                v19 = 0;
                            } else {
                                p38 = v10.getAbsolutePath();
                                v19 = 0;
                            }
                        } else {
                            System.out.println("Free space for odex enougth.");
                            v19 = 1;
                        }
                    }
                    if ((v19 != 0) && (v4 == 0)) {
                        String[] v29_97 = new java.io.File;
                        v29_97(v5);
                        if (!v29_97.exists()) {
                            System.out.println("lackypatch: dexopt-wrapper used!");
                            String[] v0_85 = new String[3];
                            String[] v29_101 = v0_85;
                            v29_101[0] = "chown";
                            v29_101[1] = "0.0";
                            v29_101[2] = new StringBuilder().append(p34).append("/dexopt-wrapper").toString();
                            com.chelpus.Utils.run_all_no_root(v29_101);
                            String[] v0_88 = new String[3];
                            String[] v29_103 = v0_88;
                            v29_103[0] = "chown";
                            v29_103[1] = "0:0";
                            v29_103[2] = new StringBuilder().append(p34).append("/dexopt-wrapper").toString();
                            com.chelpus.Utils.run_all_no_root(v29_103);
                            String[] v0_91 = new String[3];
                            String[] v29_105 = v0_91;
                            v29_105[0] = "chmod";
                            v29_105[1] = "777";
                            v29_105[2] = new StringBuilder().append(p34).append("/dexopt-wrapper").toString();
                            com.chelpus.Utils.run_all_no_root(v29_105);
                            String[] v0_94 = new String[3];
                            String[] v29_107 = v0_94;
                            v29_107[0] = new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.toolfilesdir).append("/dexopt-wrapper").toString();
                            v29_107[1] = new StringBuilder().append(p34).append("/").append(v6).toString();
                            v29_107[2] = v5;
                            String v26 = com.chelpus.Utils.cmdParam(v29_107);
                            System.out.println(v26);
                            if ((!v26.contains("succes")) || (v26.contains("failed"))) {
                                String[] v29_113 = new java.io.File;
                                v29_113(v5);
                                v29_113.delete();
                                String[] v0_102 = new String[2];
                                String[] v29_115 = v0_102;
                                v29_115[0] = "rm";
                                v29_115[1] = v5;
                                com.chelpus.Utils.run_all_no_root(v29_115);
                                p38 = v10.getAbsolutePath();
                                String[] v29_116 = new java.io.File;
                                v29_116(v5);
                                if (v29_116.exists()) {
                                    String[] v0_105 = new String[2];
                                    String[] v29_119 = v0_105;
                                    v29_119[0] = "rm";
                                    v29_119[1] = p38;
                                    com.chelpus.Utils.run_all_no_root(v29_119);
                                }
                                String[] v0_107 = new String[2];
                                String[] v29_121 = v0_107;
                                v29_121[0] = "rm";
                                v29_121[1] = new StringBuilder().append(p34).append("/").append(v15).toString();
                                com.chelpus.Utils.run_all_no_root(v29_121);
                                String[] v0_111 = new String[3];
                                String[] v29_123 = v0_111;
                                v29_123[0] = new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.toolfilesdir).append("/dexopt-wrapper").toString();
                                v29_123[1] = new StringBuilder().append(p34).append("/").append(v6).toString();
                                v29_123[2] = new StringBuilder().append(p34).append("/").append(v15).toString();
                                com.chelpus.Utils.cmdParam(v29_123);
                                String[] v0_117 = new String[3];
                                String[] v29_125 = v0_117;
                                v29_125[0] = "chmod";
                                v29_125[1] = "777";
                                v29_125[2] = new StringBuilder().append(p34).append("/").append(v15).toString();
                                com.chelpus.Utils.run_all_no_root(v29_125);
                                String[] v0_121 = new String[3];
                                String[] v29_127 = v0_121;
                                v29_127[0] = "chown";
                                v29_127[1] = "0.0";
                                v29_127[2] = new StringBuilder().append(p34).append("/").append(v15).toString();
                                com.chelpus.Utils.run_all_no_root(v29_127);
                                String[] v0_125 = new String[3];
                                String[] v29_129 = v0_125;
                                v29_129[0] = "chown";
                                v29_129[1] = "0:0";
                                v29_129[2] = new StringBuilder().append(p34).append("/").append(v15).toString();
                                com.chelpus.Utils.run_all_no_root(v29_129);
                                com.chelpus.Utils.fixadlerOdex(new java.io.File(new StringBuilder().append(p34).append("/").append(v15).toString()), p36);
                                com.chelpus.Utils.copyFile(new StringBuilder().append(p34).append("/").append(v15).toString(), p38, 0, 1);
                            } else {
                                String[] v29_138 = new java.io.File;
                                v29_138(v5);
                                com.chelpus.Utils.fixadlerOdex(v29_138, p36);
                            }
                        }
                    }
                } else {
                    System.out.println("dex-opt-art");
                    String v25 = com.chelpus.Utils.zipART(v6, p34, p35, p36);
                    if (v25.equals("")) {
                        System.out.println("Error: dont create rebuild apk to /data/tmp/");
                    } else {
                        String[] v29_142 = new java.io.File;
                        v29_142(v25);
                        if (v29_142.exists()) {
                            int v30_160 = new StringBuilder().append("apk found and copy created apk size ");
                            String v31_177 = new java.io.File;
                            v31_177(v25);
                            System.out.println(v30_160.append(v31_177.length()).toString());
                        }
                    }
                    try {
                        String v22_0 = "";
                    } catch (Exception v16_2) {
                        v16_2.printStackTrace();
                        java.io.File v8_3 = new java.io.File(v23_0);
                        if ((v8_3.exists()) && (v8_3.length() == 0)) {
                            v8_3.delete();
                        }
                        if (!new java.io.File(v8_3.getAbsolutePath()).exists()) {
                            v21 = 1;
                        } else {
                            System.out.println("oat file found. try copy and permission apply.");
                            String[] v29_179 = new java.io.File;
                            v29_179(v5);
                            v29_179.delete();
                            String[] v29_180 = new java.io.File;
                            v29_180(v5);
                            if (v29_180.exists()) {
                                String[] v0_162 = new String[2];
                                String[] v29_183 = v0_162;
                                v29_183[0] = "rm";
                                v29_183[1] = v5;
                                com.chelpus.Utils.run_all_no_root(v29_183);
                            }
                            if (!com.chelpus.Utils.dalvikvm_copyFile(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.toolfilesdir, v8_3.getAbsolutePath(), p38)) {
                                System.out.println("Error:Free space for odex not enougth.");
                                if (v8_3.exists()) {
                                    v8_3.delete();
                                }
                                if (v8_3.exists()) {
                                    String[] v0_165 = new String[2];
                                    String[] v29_190 = v0_165;
                                    v29_190[0] = "rm";
                                    v29_190[1] = v8_3.getAbsolutePath();
                                    com.chelpus.Utils.run_all_no_root(v29_190);
                                }
                                String[] v29_191 = new java.io.File;
                                v29_191(v5);
                                v29_191.delete();
                                String[] v29_192 = new java.io.File;
                                v29_192(v5);
                                if (v29_192.exists()) {
                                    String[] v0_169 = new String[2];
                                    String[] v29_195 = v0_169;
                                    v29_195[0] = "rm";
                                    v29_195[1] = v5;
                                    com.chelpus.Utils.run_all_no_root(v29_195);
                                }
                                if (new java.io.File(com.chelpus.Utils.getOdexForCreate(p36, p37)).exists()) {
                                    System.out.println("Error:dont delete corrupt odex.");
                                }
                                p38 = v10.getAbsolutePath();
                                if (v25.equals("")) {
                                    System.out.println("Error: dont create rebuild apk to /data/tmp/");
                                } else {
                                    String[] v29_202 = new java.io.File;
                                    v29_202(v25);
                                    if (v29_202.exists()) {
                                        System.out.println("apk found and copy created apk2");
                                    }
                                }
                                System.out.println("try create oat with DexFile2:");
                                String v23_1 = new StringBuilder().append(p34).append("/").append(v10.getName()).toString();
                                String v22_1 = "";
                                if (v11.getAbsolutePath().contains("/arm/")) {
                                    v22_1 = "arm";
                                }
                                if (v11.getAbsolutePath().contains("/arm64/")) {
                                    v22_1 = "arm64";
                                }
                                if (v11.getAbsolutePath().contains("/x86/")) {
                                    v22_1 = "x86";
                                }
                                String[] v29_218 = new java.io.File;
                                v29_218(v23_1);
                                v29_218.delete();
                                String[] v29_219 = new java.io.File;
                                v29_219(v23_1);
                                if (v29_219.exists()) {
                                    String[] v0_176 = new String[2];
                                    String[] v29_222 = v0_176;
                                    v29_222[0] = "rm";
                                    v29_222[1] = v23_1;
                                    com.chelpus.Utils.run_all_no_root(v29_222);
                                }
                                System.out.println("try create oat with dex2oat:");
                                String[] v0_178 = new String[4];
                                int v30_205 = v0_178;
                                v30_205[0] = "dex2oat";
                                v30_205[1] = new StringBuilder().append("--dex-file=").append(v25).toString();
                                v30_205[2] = new StringBuilder().append("--oat-file=").append(v23_1).toString();
                                v30_205[3] = new StringBuilder().append("--instruction-set=").append(v22_1).toString();
                                System.out.println(com.chelpus.Utils.cmdParam(v30_205));
                                System.out.println("end");
                                String[] v29_226 = new java.io.File;
                                v29_226(v23_1);
                                if (v29_226.exists()) {
                                    int v30_210 = new StringBuilder().append("oat2 created with dex2oat - length=");
                                    String v31_200 = new java.io.File;
                                    v31_200(v23_1);
                                    System.out.println(v30_210.append(v31_200.length()).toString());
                                    String[] v29_229 = new java.io.File;
                                    v29_229(v23_1);
                                    com.chelpus.Utils.fixCRCart(v29_229, p35, p36, v25);
                                }
                                java.io.File v8_5 = new java.io.File(v23_1);
                                if ((v8_5.exists()) && (v8_5.length() == 0)) {
                                    v8_5.delete();
                                }
                                if (!new java.io.File(v8_5.getAbsolutePath()).exists()) {
                                    v21 = 1;
                                } else {
                                    System.out.println("oat file found. try copy and permission apply.");
                                    if (!com.chelpus.Utils.dalvikvm_copyFile(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.toolfilesdir, v23_1, v10.getAbsolutePath())) {
                                        String[] v29_239 = new java.io.File;
                                        v29_239(v5);
                                        v29_239.delete();
                                        String[] v29_240 = new java.io.File;
                                        v29_240(v5);
                                        if (v29_240.exists()) {
                                            String[] v0_191 = new String[2];
                                            String[] v29_243 = v0_191;
                                            v29_243[0] = "rm";
                                            v29_243[1] = v5;
                                            com.chelpus.Utils.run_all_no_root(v29_243);
                                        }
                                        v19 = 0;
                                        v20 = 0;
                                    } else {
                                        p38 = v10.getAbsolutePath();
                                        v19 = 0;
                                    }
                                }
                            } else {
                                System.out.println("Free space for odex enougth.");
                                v19 = 1;
                                if ((com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.api < 23) || (v10.exists())) {
                                    if (!com.chelpus.Utils.dalvikvm_copyFile(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.toolfilesdir, v23_0, v10.getAbsolutePath())) {
                                        String[] v29_249 = new java.io.File;
                                        v29_249(v5);
                                        v29_249.delete();
                                        String[] v29_250 = new java.io.File;
                                        v29_250(v5);
                                        if (v29_250.exists()) {
                                            String[] v0_197 = new String[2];
                                            String[] v29_253 = v0_197;
                                            v29_253[0] = "rm";
                                            v29_253[1] = v5;
                                            com.chelpus.Utils.run_all_no_root(v29_253);
                                        }
                                        v19 = 0;
                                        v20 = 0;
                                    } else {
                                        String v38_1 = v10.getAbsolutePath();
                                        if (new java.io.File(new StringBuilder().append(v38_1).append(".art").toString()).exists()) {
                                            new java.io.File(new StringBuilder().append(v38_1).append(".art").toString()).delete();
                                        }
                                        v19 = 1;
                                        if (!p36.startsWith("/system")) {
                                            String[] v0_202 = new String[3];
                                            String[] v29_262 = v0_202;
                                            v29_262[0] = "chmod";
                                            v29_262[1] = "0644";
                                            v29_262[2] = v38_1;
                                            com.chelpus.Utils.run_all_no_root(v29_262);
                                            String[] v0_204 = new String[3];
                                            String[] v29_264 = v0_204;
                                            v29_264[0] = "chown";
                                            v29_264[1] = new StringBuilder().append("1000.").append(p37).toString();
                                            v29_264[2] = v38_1;
                                            com.chelpus.Utils.run_all_no_root(v29_264);
                                            String[] v0_207 = new String[3];
                                            String[] v29_266 = v0_207;
                                            v29_266[0] = "chown";
                                            v29_266[1] = new StringBuilder().append("1000:").append(p37).toString();
                                            v29_266[2] = v38_1;
                                            com.chelpus.Utils.run_all_no_root(v29_266);
                                        } else {
                                            String[] v0_210 = new String[3];
                                            String[] v29_268 = v0_210;
                                            v29_268[0] = "chmod";
                                            v29_268[1] = "0644";
                                            v29_268[2] = v38_1;
                                            com.chelpus.Utils.run_all_no_root(v29_268);
                                            String[] v0_212 = new String[3];
                                            String[] v29_270 = v0_212;
                                            v29_270[0] = "chown";
                                            v29_270[1] = "0.0";
                                            v29_270[2] = v38_1;
                                            com.chelpus.Utils.run_all_no_root(v29_270);
                                            String[] v0_214 = new String[3];
                                            String[] v29_272 = v0_214;
                                            v29_272[0] = "chown";
                                            v29_272[1] = "0:0";
                                            v29_272[2] = v38_1;
                                            com.chelpus.Utils.run_all_no_root(v29_272);
                                        }
                                        p38 = com.chelpus.Utils.getOdexForCreate(p36, p37);
                                    }
                                }
                            }
                        }
                        String[] v0_216 = new String[3];
                        String[] v29_274 = v0_216;
                        v29_274[0] = "chmod";
                        v29_274[1] = "777";
                        v29_274[2] = "/data/tmp";
                        com.chelpus.Utils.run_all_no_root(v29_274);
                        java.io.File[] v18 = new java.io.File("/data/tmp").listFiles();
                        if (v18 != null) {
                            int v30_255 = v18.length;
                            String[] v29_277 = 0;
                            while (v29_277 < v30_255) {
                                java.io.File v17 = v18[v29_277];
                                com.chelpus.Utils v28 = new com.chelpus.Utils;
                                v28("1");
                                v17.delete();
                                if (v17.exists()) {
                                    v28.deleteFolder(v17);
                                }
                                v29_277++;
                            }
                        }
                    }
                    if (v11.getAbsolutePath().contains("/arm/")) {
                        v22_0 = "arm";
                    }
                    if (v11.getAbsolutePath().contains("/arm64/")) {
                        v22_0 = "arm64";
                    }
                    if (v11.getAbsolutePath().contains("/x86/")) {
                        v22_0 = "x86";
                    }
                    String[] v29_152 = new java.io.File;
                    v29_152(v23_0);
                    v29_152.delete();
                    String[] v29_153 = new java.io.File;
                    v29_153(v23_0);
                    if (v29_153.exists()) {
                        String[] v0_143 = new String[2];
                        String[] v29_156 = v0_143;
                        v29_156[0] = "rm";
                        v29_156[1] = v23_0;
                        com.chelpus.Utils.run_all_no_root(v29_156);
                    }
                    System.out.println("try create oat with dex2oat:");
                    String[] v0_145 = new String[4];
                    int v30_171 = v0_145;
                    v30_171[0] = "dex2oat";
                    v30_171[1] = new StringBuilder().append("--dex-file=").append(v25).toString();
                    v30_171[2] = new StringBuilder().append("--oat-file=").append(v23_0).toString();
                    v30_171[3] = new StringBuilder().append("--instruction-set=").append(v22_0).toString();
                    System.out.println(com.chelpus.Utils.cmdParam(v30_171));
                    System.out.println("end");
                    String[] v29_160 = new java.io.File;
                    v29_160(v23_0);
                    if (v29_160.exists()) {
                        String[] v29_162 = new java.io.File;
                        v29_162(v23_0);
                        if (v29_162.length() != 0) {
                            int v30_178 = new StringBuilder().append("oat created with dex2oat - length=");
                            String v31_187 = new java.io.File;
                            v31_187(v23_0);
                            System.out.println(v30_178.append(v31_187.length()).toString());
                            String[] v29_171 = new java.io.File;
                            v29_171(v23_0);
                            com.chelpus.Utils.fixCRCart(v29_171, p35, p36, v25);
                        }
                    }
                    String[] v29_165 = new java.io.File;
                    v29_165(v23_0);
                    v29_165.delete();
                    String[] v29_166 = new java.io.File;
                    v29_166(v23_0);
                    if (!v29_166.exists()) {
                    } else {
                        String[] v0_154 = new String[2];
                        String[] v29_169 = v0_154;
                        v29_169[0] = "rm";
                        v29_169[1] = v23_0;
                        com.chelpus.Utils.run_all_no_root(v29_169);
                    }
                }
                if (!p36.startsWith("/system")) {
                    String[] v0_224 = new String[3];
                    String[] v29_281 = v0_224;
                    v29_281[0] = "chmod";
                    v29_281[1] = "0644";
                    v29_281[2] = p38;
                    com.chelpus.Utils.run_all_no_root(v29_281);
                    String[] v0_226 = new String[3];
                    String[] v29_283 = v0_226;
                    v29_283[0] = "chown";
                    v29_283[1] = new StringBuilder().append("1000.").append(p37).toString();
                    v29_283[2] = p38;
                    com.chelpus.Utils.run_all_no_root(v29_283);
                    String[] v0_229 = new String[3];
                    String[] v29_285 = v0_229;
                    v29_285[0] = "chown";
                    v29_285[1] = new StringBuilder().append("1000:").append(p37).toString();
                    v29_285[2] = p38;
                    com.chelpus.Utils.run_all_no_root(v29_285);
                } else {
                    String[] v0_232 = new String[3];
                    String[] v29_287 = v0_232;
                    v29_287[0] = "chmod";
                    v29_287[1] = "0644";
                    v29_287[2] = p38;
                    com.chelpus.Utils.run_all_no_root(v29_287);
                    String[] v0_234 = new String[3];
                    String[] v29_289 = v0_234;
                    v29_289[0] = "chown";
                    v29_289[1] = "0.0";
                    v29_289[2] = p38;
                    com.chelpus.Utils.run_all_no_root(v29_289);
                    String[] v0_236 = new String[3];
                    String[] v29_291 = v0_236;
                    v29_291[0] = "chown";
                    v29_291[1] = "0:0";
                    v29_291[2] = p38;
                    com.chelpus.Utils.run_all_no_root(v29_291);
                }
                new java.io.File(new StringBuilder().append(p34).append("/").append(v10.getName()).toString()).delete();
                new java.io.File(new StringBuilder().append(p34).append("/").append(v10.getName()).toString()).delete();
                new java.io.File(new StringBuilder().append(p34).append("/").append(v6).toString()).delete();
                if (!new java.io.File(new StringBuilder().append(p34).append("/").append(v6).toString()).exists()) {
                    if (v13 != null) {
                        try {
                            v13.close();
                        } catch (Exception v16_6) {
                            v16_6.printStackTrace();
                        }
                    }
                    if (v20 != 0) {
                        if ((v4 == 0) || (v21 == 0)) {
                            if ((v4 == 0) || (v19 != 0)) {
                                if ((v4 == 0) || (v19 == 0)) {
                                    if ((v4 != 0) || (v19 != 0)) {
                                        if ((v4 != 0) || (v19 == 0)) {
                                        } else {
                                            v27 = 0;
                                        }
                                    } else {
                                        v27 = 1;
                                    }
                                } else {
                                    v27 = 0;
                                }
                            } else {
                                v27 = 1;
                            }
                        } else {
                            v27 = 2;
                        }
                    } else {
                        v27 = 3;
                    }
                } else {
                    String[] v0_244 = new String[2];
                    String[] v29_302 = v0_244;
                    v29_302[0] = "rm";
                    v29_302[1] = new StringBuilder().append(p34).append("/").append(v6).toString();
                    com.chelpus.Utils.run_all_no_root(v29_302);
                }
            } else {
                v4 = 1;
            }
        } catch (Exception v16_5) {
            v16_5.printStackTrace();
        }
        return v27;
    }

    public static boolean dalvikvm_copyFile(String p11, String p12, String p13)
    {
        java.io.PrintStream v2_0 = 0;
        java.io.File v1_1 = new java.io.File(p12);
        java.io.File v0_1 = new java.io.File(p13);
        if (!v1_1.exists()) {
            System.out.println("Source File not Found!");
        } else {
            com.chelpus.Utils.remount(p13, "RW");
            String v4_3 = new String[3];
            v4_3[0] = "dd";
            v4_3[1] = new StringBuilder().append("if=").append(p12).toString();
            v4_3[2] = new StringBuilder().append("of=").append(p13).toString();
            com.chelpus.Utils.cmdParam(v4_3);
            if ((!new java.io.File(p13).exists()) || (v1_1.length() != v0_1.length())) {
                String v4_9 = new String[4];
                v4_9[0] = "busybox";
                v4_9[1] = "dd";
                v4_9[2] = new StringBuilder().append("if=").append(p12).toString();
                v4_9[3] = new StringBuilder().append("of=").append(p13).toString();
                com.chelpus.Utils.cmdParam(v4_9);
            }
            if ((!new java.io.File(p13).exists()) || (v1_1.length() != v0_1.length())) {
                String v4_15 = new String[4];
                v4_15[0] = new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.toolfilesdir).append("/busybox").toString();
                v4_15[1] = "dd";
                v4_15[2] = new StringBuilder().append("if=").append(p12).toString();
                v4_15[3] = new StringBuilder().append("of=").append(p13).toString();
                com.chelpus.Utils.cmdParam(v4_15);
            }
            if ((v1_1.length() == v0_1.length()) || (v1_1.length() == 0)) {
                System.out.println(v0_1.length());
                System.out.println("File copied!");
                v2_0 = 1;
            } else {
                v0_1.delete();
                System.out.println("Length of Files not equals. Destination deleted!");
            }
        }
        return v2_0;
    }

    public static int dexopt(String p22, String p23, String p24, String p25, Boolean p26)
    {
        int v16 = 0;
        System.out.println(p23);
        java.io.PrintStream v17_1 = new java.io.File;
        v17_1(p23);
        v17_1.mkdirs();
        String v0_3 = new String[3];
        java.io.PrintStream v17_3 = v0_3;
        v17_3[0] = "chmod";
        v17_3[1] = "777";
        v17_3[2] = p22;
        com.chelpus.Utils.run_all_no_root(v17_3);
        String v0_5 = new String[3];
        java.io.PrintStream v17_5 = v0_5;
        v17_5[0] = "chmod";
        v17_5[1] = "777";
        String v19_4 = new java.io.File;
        v19_4(p23);
        v17_5[2] = com.chelpus.Utils.getDirs(v19_4).getAbsolutePath();
        com.chelpus.Utils.run_all_no_root(v17_5);
        String v0_8 = new String[3];
        java.io.PrintStream v17_7 = v0_8;
        v17_7[0] = "chown";
        v17_7[1] = "0:0";
        String v19_9 = new java.io.File;
        v19_9(p23);
        v17_7[2] = com.chelpus.Utils.getDirs(v19_9).getAbsolutePath();
        com.chelpus.Utils.run_all_no_root(v17_7);
        String v0_11 = new String[3];
        java.io.PrintStream v17_9 = v0_11;
        v17_9[0] = "chown";
        v17_9[1] = "0.0";
        String v19_14 = new java.io.File;
        v19_14(p23);
        v17_9[2] = com.chelpus.Utils.getDirs(v19_14).getAbsolutePath();
        com.chelpus.Utils.run_all_no_root(v17_9);
        String v0_14 = new String[3];
        java.io.PrintStream v17_11 = v0_14;
        v17_11[0] = "chmod";
        v17_11[1] = "777";
        v17_11[2] = p23;
        com.chelpus.Utils.run_all_no_root(v17_11);
        String v0_16 = new String[3];
        java.io.PrintStream v17_13 = v0_16;
        v17_13[0] = "chown";
        v17_13[1] = "0:0";
        v17_13[2] = p23;
        com.chelpus.Utils.run_all_no_root(v17_13);
        String v0_18 = new String[3];
        java.io.PrintStream v17_15 = v0_18;
        v17_15[0] = "chown";
        v17_15[1] = "0.0";
        v17_15[2] = p23;
        com.chelpus.Utils.run_all_no_root(v17_15);
        if (p24 == null) {
            p24 = com.chelpus.Utils.getOdexForCreate(p22, p25);
        }
        if (p24 != null) {
            com.chelpus.Utils.remount(p24, "rw");
        }
        int v12 = 0;
        String v3 = com.chelpus.Utils.getOdexForCreate(p22, p25);
        java.io.PrintStream v17_17 = new java.io.File;
        v17_17(p22);
        String v4 = v17_17.getName();
        com.chelpus.Utils.changeExtension(v4, "odex");
        String v14 = new StringBuilder().append(p23).append("/").append(com.chelpus.Utils.changeExtension(v4, "dex")).toString();
        try {
            java.io.File v6 = com.chelpus.Utils.getFileDalvikCacheName(p22);
            v6.getName();
            System.out.println("dex-opt-art");
            java.io.PrintStream v17_26 = new java.io.File;
            v17_26(v14);
        } catch (UnsatisfiedLinkError v9_2) {
            v9_2.printStackTrace();
            v16 = 4;
            java.io.PrintStream v17_126 = new java.io.File;
            v17_126(v14);
            if (v17_126.exists()) {
                java.io.PrintStream v17_128 = new java.io.File;
                v17_128(v14);
                v17_128.delete();
            }
            java.io.PrintStream v17_129 = new java.io.File;
            v17_129(p23);
            java.io.File[] v11 = v17_129.listFiles();
            if ((v11 != null) && (v11.length > 0)) {
                String v18_86 = v11.length;
                java.io.PrintStream v17_131 = 0;
                while (v17_131 < v18_86) {
                    v11[v17_131].delete();
                    v17_131++;
                }
            }
            java.io.PrintStream v17_132 = new java.io.File;
            v17_132(p23);
            if (v17_132.exists()) {
                java.io.PrintStream v17_134 = new java.io.File;
                v17_134(p23);
                v17_134.delete();
            }
            String v0_98 = new String[3];
            java.io.PrintStream v17_136 = v0_98;
            v17_136[0] = "chmod";
            v17_136[1] = "644";
            v17_136[2] = p22;
            com.chelpus.Utils.run_all_no_root(v17_136);
            if (v12 == 0) {
                v16 = 1;
            }
            return v16;
        }
        if (v17_26.exists()) {
            java.io.PrintStream v17_28 = new java.io.File;
            v17_28(v14);
            v17_28.delete();
        }
        try {
            if (!p26.booleanValue()) {
                System.out.println("try create oat with DexFile.");
                java.io.PrintStream v17_31 = new java.io.File;
                v17_31(v14);
                v17_31.delete();
                dalvik.system.DexFile.loadDex(p22, v14, 0).close();
                System.out.println("end. check file. ");
                java.io.PrintStream v17_36 = new java.io.File;
                v17_36(v14);
                if (v17_36.exists()) {
                    String v18_30 = new StringBuilder().append("found file ").append(v14).append(" ");
                    String v19_25 = new java.io.File;
                    v19_25(v14);
                    System.out.println(v18_30.append(v19_25.length()).toString());
                }
                java.io.PrintStream v17_39 = new java.io.File;
                v17_39(p24);
                if (v17_39.exists()) {
                    String v18_37 = new StringBuilder().append("found file ").append(p24).append(" ");
                    String v19_29 = new java.io.File;
                    v19_29(p24);
                    System.out.println(v18_37.append(v19_29.length()).toString());
                }
                java.io.PrintStream v17_42 = new java.io.File;
                v17_42(v14);
                if (v17_42.exists()) {
                    java.io.PrintStream v17_44 = new java.io.File;
                    v17_44(v14);
                    if (v17_44.length() != 0) {
                        System.out.println("odex file create to tmp dir.");
                    }
                }
                java.io.PrintStream v17_48 = new java.io.File;
                v17_48(v14);
                v17_48.delete();
                java.io.PrintStream v17_49 = new java.io.File;
                v17_49(v14);
                if (v17_49.exists()) {
                    String v0_42 = new String[2];
                    java.io.PrintStream v17_52 = v0_42;
                    v17_52[0] = "rm";
                    v17_52[1] = v14;
                    com.chelpus.Utils.run_all_no_root(v17_52);
                }
                java.io.PrintStream v17_53 = new java.io.File;
                v17_53(v14);
                if (v17_53.exists()) {
                    java.io.PrintStream v17_55 = new java.io.File;
                    v17_55(v14);
                    if (v17_55.length() != 0) {
                    }
                }
                java.io.PrintStream v17_58 = new java.io.File;
                v17_58(v14);
                v17_58.delete();
                java.io.PrintStream v17_59 = new java.io.File;
                v17_59(v14);
                if (v17_59.exists()) {
                    String v0_48 = new String[2];
                    java.io.PrintStream v17_62 = v0_48;
                    v17_62[0] = "rm";
                    v17_62[1] = v14;
                    com.chelpus.Utils.run_all_no_root(v17_62);
                }
            } else {
                String v13 = "";
                if (v6.getAbsolutePath().contains("/arm/")) {
                    v13 = "arm";
                }
                if (v6.getAbsolutePath().contains("/arm64/")) {
                    v13 = "arm64";
                }
                if (v6.getAbsolutePath().contains("/x86/")) {
                    v13 = "x86";
                }
                System.out.println("try create oat with dex2oat:");
                String v0_50 = new String[4];
                String v18_50 = v0_50;
                v18_50[0] = "dex2oat";
                v18_50[1] = new StringBuilder().append("--dex-file=").append(p22).toString();
                v18_50[2] = new StringBuilder().append("--oat-file=").append(v14).toString();
                v18_50[3] = new StringBuilder().append("--instruction-set=").append(v13).toString();
                System.out.println(com.chelpus.Utils.cmdParam(v18_50));
                System.out.println("end");
                java.io.PrintStream v17_72 = new java.io.File;
                v17_72(v14);
                if (v17_72.exists()) {
                    java.io.PrintStream v17_74 = new java.io.File;
                    v17_74(v14);
                    if (v17_74.length() != 0) {
                        System.out.println("odex file create to tmp dir.");
                    }
                }
                java.io.PrintStream v17_78 = new java.io.File;
                v17_78(v14);
                v17_78.delete();
                java.io.PrintStream v17_79 = new java.io.File;
                v17_79(v14);
                if (v17_79.exists()) {
                    String v0_59 = new String[2];
                    java.io.PrintStream v17_82 = v0_59;
                    v17_82[0] = "rm";
                    v17_82[1] = v14;
                    com.chelpus.Utils.run_all_no_root(v17_82);
                }
            }
        } catch (UnsatisfiedLinkError v9_1) {
            v9_1.printStackTrace();
            v16 = 4;
        }
        java.io.File v5_1 = new java.io.File(v14);
        System.out.println(v14);
        System.out.println(p24);
        if ((v5_1.exists()) && (v5_1.length() == 0)) {
            v5_1.delete();
        }
        if (!v5_1.exists()) {
        } else {
            System.out.println("oat file found. try copy and permission apply.");
            java.io.PrintStream v17_90 = new java.io.File;
            v17_90(v3);
            v17_90.delete();
            java.io.PrintStream v17_91 = new java.io.File;
            v17_91(v3);
            if (v17_91.exists()) {
                String v0_65 = new String[2];
                java.io.PrintStream v17_94 = v0_65;
                v17_94[0] = "rm";
                v17_94[1] = v3;
                com.chelpus.Utils.run_all_no_root(v17_94);
            }
            if (!com.chelpus.Utils.dalvikvm_copyFile(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.toolfilesdir, v5_1.getAbsolutePath(), p24)) {
                v12 = 0;
                System.out.println("Error:Free space for odex not enougth.");
                if (v5_1.exists()) {
                    v5_1.delete();
                }
                if (v5_1.exists()) {
                    String v0_68 = new String[2];
                    java.io.PrintStream v17_101 = v0_68;
                    v17_101[0] = "rm";
                    v17_101[1] = v5_1.getAbsolutePath();
                    com.chelpus.Utils.run_all_no_root(v17_101);
                }
                java.io.PrintStream v17_102 = new java.io.File;
                v17_102(v3);
                v17_102.delete();
                java.io.PrintStream v17_103 = new java.io.File;
                v17_103(v3);
                if (v17_103.exists()) {
                    String v0_72 = new String[2];
                    java.io.PrintStream v17_106 = v0_72;
                    v17_106[0] = "rm";
                    v17_106[1] = v3;
                    com.chelpus.Utils.run_all_no_root(v17_106);
                }
                if (new java.io.File(com.chelpus.Utils.getOdexForCreate(p22, p25)).exists()) {
                    System.out.println("Error:dont delete corrupt odex.");
                }
            } else {
                System.out.println("Free space for odex enougth.");
                v12 = 1;
            }
            if (!p22.startsWith("/system")) {
                String v0_76 = new String[3];
                java.io.PrintStream v17_115 = v0_76;
                v17_115[0] = "chmod";
                v17_115[1] = "0644";
                v17_115[2] = p24;
                com.chelpus.Utils.run_all_no_root(v17_115);
                String v0_78 = new String[3];
                java.io.PrintStream v17_117 = v0_78;
                v17_117[0] = "chown";
                v17_117[1] = new StringBuilder().append("1000.").append(p25).toString();
                v17_117[2] = p24;
                com.chelpus.Utils.run_all_no_root(v17_117);
                String v0_81 = new String[3];
                java.io.PrintStream v17_119 = v0_81;
                v17_119[0] = "chown";
                v17_119[1] = new StringBuilder().append("1000:").append(p25).toString();
                v17_119[2] = p24;
                com.chelpus.Utils.run_all_no_root(v17_119);
            } else {
                String v0_84 = new String[3];
                java.io.PrintStream v17_121 = v0_84;
                v17_121[0] = "chmod";
                v17_121[1] = "0644";
                v17_121[2] = p24;
                com.chelpus.Utils.run_all_no_root(v17_121);
                String v0_86 = new String[3];
                java.io.PrintStream v17_123 = v0_86;
                v17_123[0] = "chown";
                v17_123[1] = "0.0";
                v17_123[2] = p24;
                com.chelpus.Utils.run_all_no_root(v17_123);
                String v0_88 = new String[3];
                java.io.PrintStream v17_125 = v0_88;
                v17_125[0] = "chown";
                v17_125[1] = "0:0";
                v17_125[2] = p24;
                com.chelpus.Utils.run_all_no_root(v17_125);
            }
        }
    }

    public static void dexoptcopy()
    {
        int v1;
        java.io.File v2_1 = new java.io.File(new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.toolfilesdir).append("/").append("dexopt-wrapper").toString());
        if (!android.os.Build.CPU_ABI.toLowerCase().trim().equals("x86")) {
            if (!android.os.Build.CPU_ABI.toUpperCase().trim().equals("MIPS")) {
                v1 = 2131099652;
            } else {
                v1 = 2131099653;
            }
        } else {
            v1 = 2131099654;
        }
        if ((v2_1.exists()) && (v2_1.length() == com.chelpus.Utils.getRawLength(v1))) {
            com.chelpus.Utils.run_all(new StringBuilder().append("chmod 777 ").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.toolfilesdir).append("/").append("dexopt-wrapper").toString());
            com.chelpus.Utils.run_all(new StringBuilder().append("chown 0.0 ").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.toolfilesdir).append("/").append("dexopt-wrapper").toString());
            com.chelpus.Utils.run_all(new StringBuilder().append("chmod 0:0 ").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.toolfilesdir).append("/").append("dexopt-wrapper").toString());
        } else {
            try {
                com.chelpus.Utils.getRawToFile(v1, new java.io.File(new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.toolfilesdir).append("/dexopt-wrapper").toString()));
                try {
                    com.chelpus.Utils.chmod(new java.io.File(new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.toolfilesdir).append("/").append("dexopt-wrapper").toString()), 777);
                } catch (Exception v0) {
                    System.out.println(v0);
                    v0.printStackTrace();
                }
                com.chelpus.Utils.run_all(new StringBuilder().append("chmod 777 ").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.toolfilesdir).append("/").append("dexopt-wrapper").toString());
                com.chelpus.Utils.run_all(new StringBuilder().append("chown 0.0 ").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.toolfilesdir).append("/").append("dexopt-wrapper").toString());
                com.chelpus.Utils.run_all(new StringBuilder().append("chmod 0:0 ").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.toolfilesdir).append("/").append("dexopt-wrapper").toString());
            } catch (String v3) {
            }
        }
        return;
    }

    public static boolean exists(String p7)
    {
        int v1 = 1;
        if (!new java.io.File(p7).exists()) {
            if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.su) {
                if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.startUnderRoot.booleanValue()) {
                    boolean v3_7 = new com.chelpus.Utils("");
                    String[] v4_1 = new String[1];
                    v4_1[0] = new StringBuilder().append("ls ").append(p7).toString();
                    int v0 = v3_7.cmdRoot(v4_1);
                    System.out.println(v0);
                    if (v0.equals(p7)) {
                        return v1;
                    }
                }
                v1 = 0;
            } else {
                v1 = 0;
            }
        }
        return v1;
    }

    public static void exit()
    {
        if ((!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().getBoolean("OnBootService", 0)) && ((!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patchOnBoot) && ((!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.desktop_launch) && ((!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.appDisabler) && (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.binderWidget))))) {
            System.out.println("LP - exit.");
            System.exit(0);
        }
        return;
    }

    public static void exitFromRootJava()
    {
        boolean v0 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.startUnderRoot.booleanValue();
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.startUnderRoot = Boolean.valueOf(0);
        if (v0) {
            System.exit(0);
        }
        return;
    }

    public static void exitRoot()
    {
        if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.startUnderRoot.booleanValue()) {
            System.out.println("LuckyPatcher: exit root.");
            try {
                if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.suOutputStream != null) {
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.suOutputStream.writeBytes("exit\n");
                }
            } catch (Exception v0_1) {
                v0_1.printStackTrace();
            } catch (Exception v0_0) {
                v0_0.printStackTrace();
            }
            try {
                if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.suProcess == null) {
                    if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.suOutputStream != null) {
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.suOutputStream.close();
                    }
                    if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.suInputStream != null) {
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.suInputStream.close();
                    }
                    if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.suErrorInputStream != null) {
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.suErrorInputStream.close();
                    }
                } else {
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.suProcess.destroy();
                }
            } catch (Exception v0_2) {
                v0_2.printStackTrace();
            }
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.suProcess = 0;
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.suOutputStream = 0;
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.suInputStream = 0;
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.suErrorInputStream = 0;
        }
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.semaphoreRoot.release();
        return;
    }

    private static com.android.vending.billing.InAppBillingService.LUCK.Mount findMountPointRecursive(String p11)
    {
        try {
            com.android.vending.billing.InAppBillingService.LUCK.Mount v2_0;
            java.util.ArrayList v3 = com.chelpus.Utils.getMounts();
            java.io.File v4_1 = new java.io.File(p11);
        } catch (Exception v0) {
            v0.printStackTrace();
            v2_0 = 0;
            return v2_0;
        }
        if (v4_1 == null) {
            v2_0 = 0;
            return v2_0;
        } else {
            java.io.PrintStream v8_0 = v3.iterator();
            while (v8_0.hasNext()) {
                v2_0 = ((com.android.vending.billing.InAppBillingService.LUCK.Mount) v8_0.next());
                if (v2_0.getMountPoint().equals(v4_1)) {
                    return v2_0;
                }
            }
            java.util.ArrayList v6_1 = new java.util.ArrayList();
            java.io.PrintStream v8_1 = v3.iterator();
            while (v8_1.hasNext()) {
                com.android.vending.billing.InAppBillingService.LUCK.Mount v2_2 = ((com.android.vending.billing.InAppBillingService.LUCK.Mount) v8_1.next());
                if (p11.startsWith(v2_2.getMountPoint().getAbsolutePath())) {
                    v6_1.add(v2_2);
                }
            }
            com.android.vending.billing.InAppBillingService.LUCK.Mount v5 = 0;
            java.io.PrintStream v8_2 = v6_1.iterator();
            while (v8_2.hasNext()) {
                com.android.vending.billing.InAppBillingService.LUCK.Mount v1_1 = ((com.android.vending.billing.InAppBillingService.LUCK.Mount) v8_2.next());
                if (v5 == null) {
                    v5 = v1_1;
                }
                if (v5.getMountPoint().getAbsolutePath().length() < v1_1.getMountPoint().getAbsolutePath().length()) {
                    v5 = v1_1;
                }
            }
            if (v5 != null) {
                System.out.println(new StringBuilder().append("recursive mount ").append(v5.getMountPoint().getAbsolutePath()).toString());
            }
            v2_0 = v5;
            return v2_0;
        }
    }

    public static void fixCRCart(java.io.File p38, java.util.ArrayList p39, String p40, String p41)
    {
        java.util.ArrayList v13_1 = new java.util.ArrayList(p39.size());
        byte[] v8 = new byte[8];
        v8 = {100, 101, 121, 10, 48, 51, 53, 0};
        byte v3_3 = new String[3];
        v3_3[0] = "chmod";
        v3_3[1] = "777";
        v3_3[2] = p38.getAbsolutePath();
        com.chelpus.Utils.run_all_no_root(v3_3);
        try {
            if ((p40 != null) && ((new java.io.File(p40).exists()) && (new java.io.File(p40).length() != 0))) {
                try {
                    System.out.println(p40);
                    byte v3_13 = new String[3];
                    v3_13[0] = "chmod";
                    v3_13[1] = "644";
                    v3_13[2] = p40;
                    com.chelpus.Utils.run_all_no_root(v3_13);
                    try {
                        java.io.FileInputStream v22 = new java.io.FileInputStream;
                        v22(p40);
                        java.util.zip.ZipInputStream v37 = new java.util.zip.ZipInputStream;
                        v37(v22);
                        java.util.zip.ZipEntry v36 = v37.getNextEntry();
                    } catch (Exception v17) {
                        System.out.println("Start alternative method.");
                        try {
                            System.out.println("start");
                            kellinwood.zipio.ZipInput v27 = kellinwood.zipio.ZipInput.read(p40);
                            int v4_8 = v27.getEntries().values().iterator();
                        } catch (Exception v18_1) {
                            v18_1.printStackTrace();
                            try {
                                java.nio.channels.FileChannel v2 = new java.io.RandomAccessFile(p38, "rw").getChannel();
                                java.nio.MappedByteBuffer v21 = v2.map(java.nio.channels.FileChannel$MapMode.READ_WRITE, 0, ((long) ((int) v2.size())));
                                v21.position(4096);
                                int v15 = -1;
                                byte[] v29 = p41.getBytes();
                                byte[] v30 = new StringBuilder().append(p41).append(":classes").toString().getBytes();
                                byte[] v32 = p40.getBytes();
                            } catch (Exception v17_2) {
                                v17_2.printStackTrace();
                            }
                            try {
                                while (v21.hasRemaining()) {
                                    v21.position((v15 + 1));
                                    v15 = v21.position();
                                    byte v14 = v21.get();
                                    if (v14 == v30[0]) {
                                        int v24_0 = 1;
                                        v21.position((v15 + 1));
                                        byte v31_0 = v21.get();
                                        while (v31_0 == v30[v24_0]) {
                                            v24_0++;
                                            if (v24_0 != v30.length) {
                                                v31_0 = v21.get();
                                            } else {
                                                int v35 = 0;
                                                byte[] v19 = ".dex".getBytes();
                                                String v10 = "classes";
                                                while (v35 < 7) {
                                                    v35++;
                                                    byte v31_2 = v21.get();
                                                    v10 = new StringBuilder().append(v10).append(((char) v31_2)).toString();
                                                    if (v31_2 == v19[0]) {
                                                        int v34 = 1;
                                                        byte v31_3 = v21.get();
                                                        while ((v34 < v19.length) && (v31_3 == v19[v34])) {
                                                            v10 = new StringBuilder().append(v10).append(((char) v31_3)).toString();
                                                            v34++;
                                                            if (v34 == v19.length) {
                                                                int v20_1 = 0;
                                                                while (v20_1 < p39.size()) {
                                                                    if (!((java.io.File) p39.get(v20_1)).getName().equals(v10)) {
                                                                        v20_1++;
                                                                    } else {
                                                                        if (((Integer) v13_1.get(v20_1)).intValue() != 0) {
                                                                            v21.put(((Integer) v13_1.get(v20_1)).byteValue());
                                                                            v21.force();
                                                                            v21.put(((byte) (((Integer) v13_1.get(v20_1)).intValue() >> 8)));
                                                                            v21.force();
                                                                            v21.put(((byte) (((Integer) v13_1.get(v20_1)).intValue() >> 16)));
                                                                            v21.force();
                                                                            v21.put(((byte) (((Integer) v13_1.get(v20_1)).intValue() >> 24)));
                                                                            v21.force();
                                                                        }
                                                                        v21.position(v15);
                                                                        v21.put(v32);
                                                                        v21.force();
                                                                        v15 = v21.position();
                                                                        v14 = v21.get();
                                                                        break;
                                                                    }
                                                                }
                                                            }
                                                            v31_3 = v21.get();
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    if (v14 == v29[0]) {
                                        int v24_1 = 1;
                                        v21.position((v15 + 1));
                                        byte v31_1 = v21.get();
                                        while (v31_1 == v29[v24_1]) {
                                            v24_1++;
                                            if (v24_1 != v29.length) {
                                                v31_1 = v21.get();
                                            } else {
                                                int v26 = 255;
                                                int v20_0 = 0;
                                                while (v20_0 < p39.size()) {
                                                    if (((java.io.File) p39.get(v20_0)).getName().equals("classes.dex")) {
                                                        v26 = v20_0;
                                                    }
                                                    v20_0++;
                                                }
                                                if ((v26 == 255) || ((v21.get(v21.position()) == 32) || ((v21.get((v21.position() + 1)) == 45) || (v21.get((v21.position() + 2)) == 45)))) {
                                                    break;
                                                }
                                                if (((Integer) v13_1.get(v26)).intValue() != 0) {
                                                    v21.put(((Integer) v13_1.get(v26)).byteValue());
                                                    v21.force();
                                                    v21.put(((byte) (((Integer) v13_1.get(v26)).intValue() >> 8)));
                                                    v21.force();
                                                    v21.put(((byte) (((Integer) v13_1.get(v26)).intValue() >> 16)));
                                                    v21.force();
                                                    v21.put(((byte) (((Integer) v13_1.get(v26)).intValue() >> 24)));
                                                    v21.force();
                                                }
                                                v21.position(v15);
                                                v21.put(v32);
                                                v21.force();
                                                break;
                                            }
                                        }
                                    }
                                }
                            } catch (Exception v17_1) {
                                v17_1.printStackTrace();
                            }
                            v2.close();
                        }
                    }
                    while (v36 != null) {
                        String v23 = v36.getName();
                        if ((v23.startsWith("classes")) && (v23.endsWith(".dex"))) {
                            int v33_0 = 0;
                            while (v33_0 < p39.size()) {
                                if (!((java.io.File) p39.get(v33_0)).getName().equals(v23)) {
                                    v33_0++;
                                } else {
                                    try {
                                        int v11_0 = ((int) v36.getCrc());
                                    } catch (Exception v17_0) {
                                        v17_0.printStackTrace();
                                        v11_0 = 0;
                                        v13_1.add(v33_0, Integer.valueOf(v11_0));
                                        v37.closeEntry();
                                        break;
                                    }
                                    if (v11_0 != -1) {
                                    } else {
                                        java.util.zip.CRC32 v12_1 = new java.util.zip.CRC32();
                                        v12_1.reset();
                                        byte[] v9 = new byte[4096];
                                        while(true) {
                                            int v28 = v37.read(v9);
                                            if (v28 == -1) {
                                                break;
                                            }
                                            v12_1.update(v9, 0, v28);
                                        }
                                        v11_0 = ((int) v12_1.getValue());
                                    }
                                }
                            }
                        }
                        v36 = v37.getNextEntry();
                    }
                    v37.close();
                    v22.close();
                } catch (Exception v17_3) {
                    v17_3.printStackTrace();
                }
            }
        } catch (Exception v17_4) {
            System.out.println(v17_4);
        }
        return;
    }

    public static void fixadler(java.io.File p4)
    {
        try {
            String[] v1_1 = new String[3];
            v1_1[0] = "chmod";
            v1_1[1] = "777";
            v1_1[2] = p4.getAbsolutePath();
            com.chelpus.Utils.cmdParam(v1_1);
            com.chelpus.Utils.calcSignature(p4);
            com.chelpus.Utils.calcChecksum(p4);
        } catch (Exception v0) {
            v0.printStackTrace();
        }
        return;
    }

    public static void fixadlerOdex(java.io.File p37, String p38)
    {
        byte[] v9 = new byte[8];
        v9 = {100, 101, 121, 10, 48, 51, 53, 0};
        try {
            java.io.FileInputStream v32 = new java.io.FileInputStream;
            v32(p37);
            java.nio.MappedByteBuffer v0_1 = new byte[40];
            byte[] v33 = v0_1;
            v32.read(v33);
            v32.close();
            int v27 = 0;
        } catch (Exception v31) {
            v31.printStackTrace();
            return;
        }
        while (v27 < 4) {
            if (v33[v27] == v9[v27]) {
                v27++;
            } else {
                System.out.println(new StringBuilder().append("The magic value is not the expected value ").append(new String(v33)).toString());
                return;
            }
        }
        try {
            java.nio.ByteBuffer v12 = java.nio.ByteBuffer.wrap(v33);
            v12.order(java.nio.ByteOrder.LITTLE_ENDIAN);
            v12.position(8);
            int v20 = v12.getInt();
            v12.position(12);
            int v19 = v12.getInt();
            v12.position(16);
            int v18 = v12.getInt();
            v12.position(20);
            v12.getInt();
            v12.position(24);
            v12.getInt();
            v12.position(28);
            v12.getInt();
            v12.position(32);
            v12.getInt();
            com.chelpus.Utils.calcChecksumOdexFly(v20, v19, p37);
        } catch (java.io.IOException v21_5) {
            System.out.println(v21_5);
            return;
        }
        if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.startUnderRoot.booleanValue()) {
            com.chelpus.Utils.run_all(new StringBuilder().append("chmod 644 ").append(p38).toString());
        } else {
            byte v4_19 = new String[3];
            v4_19[0] = "chmod";
            v4_19[1] = "644";
            v4_19[2] = p38;
            com.chelpus.Utils.run_all_no_root(v4_19);
        }
        if ((p38 == null) || ((!new java.io.File(p38).exists()) || (new java.io.File(p38).length() == 0))) {
            return;
        } else {
            try {
                System.out.println(p38);
                java.io.FileInputStream v24 = new java.io.FileInputStream;
                v24(p38);
                java.util.zip.ZipInputStream v36 = new java.util.zip.ZipInputStream;
                v36(v24);
                java.util.zip.ZipEntry v35 = v36.getNextEntry();
            } catch (java.io.IOException v21_4) {
                System.out.println("alternative method");
                v21_4.printStackTrace();
                try {
                    System.out.println("start");
                    kellinwood.zipio.ZipInput v29 = kellinwood.zipio.ZipInput.read(p38);
                    byte v4_65 = v29.getEntries().values().iterator();
                } catch (java.io.IOException v22_2) {
                    v22_2.printStackTrace();
                    return;
                }
            }
            while (v35 != null) {
                if (!v35.getName().equals("classes.dex")) {
                    v35 = v36.getNextEntry();
                } else {
                    try {
                        int v34_0 = ((int) com.chelpus.Utils.javaToDosTime(v35.getTime()));
                        try {
                            int v14_0 = ((int) v35.getCrc());
                        } catch (java.io.IOException v21_1) {
                            v21_1.printStackTrace();
                            v14_0 = 0;
                            try {
                                java.nio.channels.FileChannel v3_0 = new java.io.RandomAccessFile(p37, "rw").getChannel();
                                java.nio.MappedByteBuffer v23_0 = v3_0.map(java.nio.channels.FileChannel$MapMode.READ_WRITE, 0, ((long) ((int) v3_0.size())));
                            } catch (java.io.IOException v21_3) {
                                v21_3.printStackTrace();
                                v36.closeEntry();
                                break;
                            }
                            if (v34_0 != 0) {
                                v23_0.position(v18);
                                v23_0.put(((byte) v34_0));
                                v23_0.force();
                                v23_0.position((v18 + 1));
                                v23_0.put(((byte) (v34_0 >> 8)));
                                v23_0.force();
                                v23_0.position((v18 + 2));
                                v23_0.put(((byte) (v34_0 >> 16)));
                                v23_0.force();
                                v23_0.position((v18 + 3));
                                v23_0.put(((byte) (v34_0 >> 24)));
                                v23_0.force();
                            }
                            if (v14_0 != 0) {
                                v23_0.position((v18 + 4));
                                v23_0.put(((byte) v14_0));
                                v23_0.force();
                                v23_0.position((v18 + 5));
                                v23_0.put(((byte) (v14_0 >> 8)));
                                v23_0.force();
                                v23_0.position((v18 + 6));
                                v23_0.put(((byte) (v14_0 >> 16)));
                                v23_0.force();
                                v23_0.position((v18 + 7));
                                v23_0.put(((byte) (v14_0 >> 24)));
                                v23_0.force();
                            }
                            v3_0.close();
                        }
                        if (v14_0 != -1) {
                        } else {
                            java.util.zip.CRC32 v15_1 = new java.util.zip.CRC32();
                            v15_1.reset();
                            byte[] v13 = new byte[4096];
                            while(true) {
                                int v30 = v36.read(v13);
                                if (v30 == -1) {
                                    break;
                                }
                                v15_1.update(v13, 0, v30);
                            }
                            v14_0 = ((int) v15_1.getValue());
                        }
                    } catch (java.io.IOException v21_0) {
                        v21_0.printStackTrace();
                        v34_0 = 0;
                    }
                }
            }
            v36.close();
            v24.close();
            return;
        }
    }

    public static String gen_sha1withrsa(String p2)
    {
        byte[] v0 = new byte[p2.length()];
        com.chelpus.Utils.rnd.nextBytes(v0);
        return com.google.android.finsky.billing.iab.google.util.Base64.encode(v0);
    }

    public static android.graphics.drawable.Drawable getApkIcon(String p5)
    {
        android.graphics.drawable.Drawable v3_1;
        android.content.pm.PackageManager v2 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng();
        android.content.pm.PackageInfo v1 = v2.getPackageArchiveInfo(p5, 1);
        if (v1 == null) {
            v3_1 = 0;
        } else {
            android.content.pm.ApplicationInfo v0 = v1.applicationInfo;
            if (android.os.Build$VERSION.SDK_INT >= 8) {
                v0.sourceDir = p5;
                v0.publicSourceDir = p5;
            }
            v3_1 = v0.loadIcon(v2);
        }
        return v3_1;
    }

    public static android.content.pm.ApplicationInfo getApkInfo(String p5)
    {
        int v0;
        android.content.pm.PackageInfo v1 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getPackageArchiveInfo(p5, 1);
        if (v1 == null) {
            v0 = 0;
        } else {
            v0 = v1.applicationInfo;
            if (android.os.Build$VERSION.SDK_INT >= 8) {
                v0.sourceDir = p5;
                v0.publicSourceDir = p5;
            }
        }
        return v0;
    }

    public static String getApkLabelName(String p5)
    {
        String v3_1;
        android.content.pm.PackageManager v2 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng();
        android.content.pm.PackageInfo v1 = v2.getPackageArchiveInfo(p5, 1);
        if (v1 == null) {
            v3_1 = 0;
        } else {
            android.content.pm.ApplicationInfo v0 = v1.applicationInfo;
            if (android.os.Build$VERSION.SDK_INT >= 8) {
                v0.sourceDir = p5;
                v0.publicSourceDir = p5;
            }
            v3_1 = v0.loadLabel(v2).toString();
        }
        return v3_1;
    }

    public static android.content.pm.PackageInfo getApkPackageInfo(String p3)
    {
        int v0 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getPackageArchiveInfo(p3, 1);
        if (v0 == 0) {
            v0 = 0;
        }
        return v0;
    }

    public static void getAssets(String p7, String p8)
    {
        new java.io.File(p8).mkdirs();
        int v2 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getRes().getAssets().open(p7);
        int v3_1 = new java.io.FileOutputStream(new StringBuilder().append(p8).append("/").append(p7).toString());
        byte[] v1 = new byte[8192];
        while(true) {
            int v4 = v2.read(v1);
            if (v4 == -1) {
                break;
            }
            v3_1.write(v1, 0, v4);
        }
        v2.close();
        v3_1.flush();
        v3_1.close();
        return;
    }

    public static android.text.SpannableString getColoredText(String p7, int p8, String p9)
    {
        android.text.SpannableString v1_1 = new android.text.SpannableString(p7);
        int v2 = 0;
        try {
            if (!p9.toLowerCase().contains("bold")) {
                if (p9.toLowerCase().contains("bold_italic")) {
                    v2 = 3;
                }
                if (p9.toLowerCase().contains("italic")) {
                    v2 = 2;
                }
                v1_1.setSpan(new android.text.style.ForegroundColorSpan(p8), 0, p7.length(), 0);
                v1_1.setSpan(new android.text.style.StyleSpan(v2), 0, p7.length(), 0);
            } else {
                v2 = 1;
            }
        } catch (Exception v0) {
            v0.printStackTrace();
            if (p7.length() == 0) {
                p7 = " ";
            }
            v1_1 = new android.text.SpannableString(p7);
        }
        return v1_1;
    }

    public static android.text.SpannableString getColoredText(String p7, String p8, String p9)
    {
        android.text.SpannableString v1_1 = new android.text.SpannableString(p7);
        int v2 = 0;
        try {
            if (!p9.toLowerCase().contains("bold")) {
                if (p9.toLowerCase().contains("bold_italic")) {
                    v2 = 3;
                }
                if (p9.toLowerCase().contains("italic")) {
                    v2 = 2;
                }
                if (!p8.equals("")) {
                    v1_1.setSpan(new android.text.style.ForegroundColorSpan(android.graphics.Color.parseColor(p8)), 0, p7.length(), 0);
                }
                v1_1.setSpan(new android.text.style.StyleSpan(v2), 0, p7.length(), 0);
            } else {
                v2 = 1;
            }
        } catch (Exception v0) {
            v0.printStackTrace();
            if (p7.length() == 0) {
                p7 = " ";
            }
            v1_1 = new android.text.SpannableString(p7);
        }
        return v1_1;
    }

    public static String getCurrentRuntimeValue()
    {
        try {
            String v3_0;
            if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.runtime.contains("ART")) {
                if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.runtime.contains("DALVIK")) {
                    v3_0 = "ART";
                } else {
                    v3_0 = "Dalvik";
                }
            } else {
                v3_0 = "ART";
            }
        } catch (reflect.InvocationTargetException v0_0) {
            v0_0.printStackTrace();
            try {
                Class v2 = Class.forName("android.os.SystemProperties");
            } catch (reflect.InvocationTargetException v0_1) {
                v0_1.printStackTrace();
                v3_0 = "Dalvik";
                return v3_0;
            } catch (reflect.InvocationTargetException v0_2) {
                v0_2.printStackTrace();
                v3_0 = "Dalvik";
                return v3_0;
            }
            try {
                int v5_3 = new Class[2];
                v5_3[0] = String;
                v5_3[1] = String;
                reflect.Method v1 = v2.getMethod("get", v5_3);
            } catch (reflect.InvocationTargetException v0) {
                v3_0 = "SystemProperties.get(String key, String def) method is not found";
                return v3_0;
            }
            if (v1 != null) {
                try {
                    boolean v4_7 = new Object[2];
                    v4_7[0] = "persist.sys.dalvik.vm.lib";
                    v4_7[1] = "Dalvik";
                    v3_0 = ((String) v1.invoke(v2, v4_7));
                    System.out.println(v3_0);
                } catch (reflect.InvocationTargetException v0) {
                    v3_0 = "InvocationTargetException";
                    return v3_0;
                } catch (reflect.InvocationTargetException v0) {
                    v3_0 = "IllegalAccessException";
                    return v3_0;
                }
                if (!"libdvm.so".equals(v3_0)) {
                    if (!"libart.so".equals(v3_0)) {
                        if (!"libartd.so".equals(v3_0)) {
                        } else {
                            v3_0 = "ART debug build";
                        }
                    } else {
                        v3_0 = "ART";
                    }
                } else {
                    v3_0 = "Dalvik";
                }
            } else {
                v3_0 = "WTF?!";
            }
        }
        return v3_0;
    }

    public static java.util.ArrayList getCustomPatchesForPkg(String p7)
    {
        java.util.ArrayList v3_1 = new java.util.ArrayList();
        try {
            if ((com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.customlist != null) && (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.customlist.length != 0)) {
                if ((com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.customlist != null) && (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.customlist.length > 0)) {
                    int v2_1 = 0;
                    while (v2_1 < com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.customlist.length) {
                        if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.customlist[v2_1].getName().contains("_%ALL%.txt")) {
                            if (!p7.contains(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.customlist[v2_1].getName().replace("_%ALL%.txt", ""))) {
                                if ((com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.customlist[v2_1].getName().contains("_%ALL%.txt")) && ((com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.customlist[v2_1].getName().contains("%ALL%_")) && (p7.contains(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.customlist[v2_1].getName().split("%ALL%_")[1].replace("_%ALL%.txt", ""))))) {
                                    v3_1.add(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.customlist[v2_1]);
                                }
                            } else {
                                v3_1.add(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.customlist[v2_1]);
                            }
                        }
                        if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.customlist[v2_1].getName().replace(".txt", "").endsWith(p7)) {
                            v3_1.add(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.customlist[v2_1]);
                        }
                        v2_1++;
                    }
                }
            } else {
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.init();
                try {
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.basepath = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().getString("basepath", com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.basepath);
                    java.io.File v4_9 = new java.io.File[new java.io.File(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.basepath).listFiles().length];
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.customlist = v4_9;
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.customlist = new java.io.File(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.basepath).listFiles();
                    java.util.ArrayList v0_1 = new java.util.ArrayList();
                    v0_1.clear();
                    int v2_0 = 0;
                } catch (Exception v1) {
                    System.out.println("Not found dir by Lucky Patcher. Custom patch not found.");
                }
                while (v2_0 < com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.customlist.length) {
                    if ((com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.customlist[v2_0].isFile()) && (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.customlist[v2_0].getName().endsWith(".txt"))) {
                        v0_1.add(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.customlist[v2_0]);
                    }
                    v2_0++;
                }
                if (v0_1.size() <= 0) {
                } else {
                    java.io.File v4_17 = new java.io.File[v0_1.size()];
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.customlist = v4_17;
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.customlist = ((java.io.File[]) ((java.io.File[]) v0_1.toArray(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.customlist)));
                }
            }
        } catch (Exception v1) {
            v1.printStackTrace();
        }
        return v3_1;
    }

    public static java.io.File getDirs(java.io.File p5)
    {
        String[] v2 = p5.toString().split(java.io.File.separator);
        String v0 = "";
        int v1 = 0;
        while (v1 < v2.length) {
            if (v1 != (v2.length - 1)) {
                v0 = new StringBuilder().append(v0).append(v2[v1]).append(java.io.File.separator).toString();
            }
            v1++;
        }
        return new java.io.File(v0);
    }

    public static java.io.File getFileDalvikCache(String p9)
    {
        if ((com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.api < 23) || (p9.startsWith("/system/"))) {
            String v2_0 = p9.replaceAll("/", "@");
            while (v2_0.startsWith("@")) {
                v2_0 = v2_0.replaceFirst("@", "");
            }
            String v2_1 = new StringBuilder().append(v2_0).append("@classes.dex").toString();
            if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.startUnderRoot.booleanValue()) {
                if (!new java.io.File(new StringBuilder().append("/data/dalvik-cache/x86/").append(v2_1).toString()).exists()) {
                    if (!new java.io.File(new StringBuilder().append("/data/dalvik-cache/arm64/").append(v2_1).toString()).exists()) {
                        if (!new java.io.File(new StringBuilder().append("/data/dalvik-cache/arm/").append(v2_1).toString()).exists()) {
                            if (new java.io.File(new StringBuilder().append("/data/dalvik-cache/").append(v2_1).toString()).exists()) {
                                java.io.File v6_47 = new java.io.File(new StringBuilder().append("/data/dalvik-cache/").append(v2_1).toString());
                                return v6_47;
                            }
                        } else {
                            v6_47 = new java.io.File(new StringBuilder().append("/data/dalvik-cache/arm/").append(v2_1).toString());
                            return v6_47;
                        }
                    } else {
                        v6_47 = new java.io.File(new StringBuilder().append("/data/dalvik-cache/arm64/").append(v2_1).toString());
                        return v6_47;
                    }
                } else {
                    v6_47 = new java.io.File(new StringBuilder().append("/data/dalvik-cache/x86/").append(v2_1).toString());
                    return v6_47;
                }
            } else {
                try {
                    String v1 = new com.chelpus.Utils("").findFileContainText(new java.io.File("/data/dalvik-cache"), v2_1);
                } catch (Exception v3_1) {
                    v3_1.printStackTrace();
                    if (!v1.equals("")) {
                        System.out.println(new StringBuilder().append("").append(v1).toString());
                        if (new java.io.File(v1).exists()) {
                            v6_47 = new java.io.File(v1);
                            return v6_47;
                        }
                    }
                } catch (Exception v3_0) {
                    v3_0.printStackTrace();
                }
                if ((!v1.startsWith("\"/data/dalvik-cache/arm/\"")) || (!new java.io.File(new StringBuilder().append("/data/dalvik-cache/arm64/").append(v2_1).toString()).exists())) {
                } else {
                    v1 = new StringBuilder().append("/data/dalvik-cache/arm64/").append(v2_1).toString();
                }
            }
            v6_47 = 0;
        } else {
            String v4 = "";
            java.io.File v0_1 = new java.io.File(p9);
            if ((new java.io.File("/data/dalvik-cache/arm").exists()) && (new java.io.File("/data/dalvik-cache/arm").isDirectory())) {
                v4 = "/arm";
            }
            if ((new java.io.File("/data/dalvik-cache/arm64").exists()) && (new java.io.File("/data/dalvik-cache/arm64").isDirectory())) {
                v4 = "/arm64";
            }
            if ((new java.io.File("/data/dalvik-cache/x86").exists()) && (new java.io.File("/data/dalvik-cache/x86").isDirectory())) {
                v4 = "/x86";
            }
            String v5 = com.chelpus.Utils.changeExtension(v0_1.getName(), "odex");
            if (!new java.io.File(new StringBuilder().append(com.chelpus.Utils.getDirs(v0_1)).append("/oat").append(v4).append("/").append(v5).toString()).exists()) {
            } else {
                v6_47 = new java.io.File(new StringBuilder().append(com.chelpus.Utils.getDirs(v0_1)).append("/oat").append(v4).append("/").append(v5).toString());
            }
        }
        return v6_47;
    }

    public static java.io.File getFileDalvikCacheName(String p14)
    {
        java.io.File v6_0;
        String v9_0 = 0;
        if (!new java.io.File("/data/art-cache/").exists()) {
            String[] v5 = new String[8];
            v5[0] = "/data/dalvik-cache/";
            v5[1] = "/data/dalvik-cache/arm64/";
            v5[2] = "/data/dalvik-cache/arm/";
            v5[3] = "/data/dalvik-cache/x86/";
            v5[4] = "/sd-ext/data/dalvik-cache/";
            v5[5] = "/cache/dalvik-cache/";
            v5[6] = "/sd-ext/data/cache/dalvik-cache/";
            v5[7] = "/data/cache/dalvik-cache/";
            String v3_0 = p14.replaceAll("/", "@");
            while (v3_0.startsWith("@")) {
                v3_0 = v3_0.replaceFirst("@", "");
            }
            String v3_1 = new StringBuilder().append(v3_0).append("@classes.dex").toString();
            System.out.println(new StringBuilder().append("dalvikfile: ").append(v3_1).toString());
            v6_0 = 0;
            String v10_30 = v5.length;
            while (v9_0 < v10_30) {
                String v4 = v5[v9_0];
                if (new java.io.File(new StringBuilder().append(v4).append(v3_1).toString()).exists()) {
                    System.out.println(new StringBuilder().append("\nLuckyPatcher: found dalvik-cache! ").append(v4).append(v3_1).toString());
                    v6_0 = new java.io.File(new StringBuilder().append(v4).append(v3_1).toString());
                }
                v9_0++;
            }
            if (v6_0 == null) {
                if ((!com.chelpus.Utils.exists("/data/dalvik-cache/arm")) && ((!com.chelpus.Utils.exists("/data/dalvik-cache/arm64")) && (!com.chelpus.Utils.exists("/data/dalvik-cache/x86")))) {
                    v6_0 = new java.io.File(new StringBuilder().append("/data/dalvik-cache/").append(v3_1).toString());
                } else {
                    if (com.chelpus.Utils.exists("/data/dalvik-cache/arm")) {
                        v6_0 = new java.io.File(new StringBuilder().append("/data/dalvik-cache/arm/").append(v3_1).toString());
                    }
                    if (com.chelpus.Utils.exists("/data/dalvik-cache/arm64")) {
                        v6_0 = new java.io.File(new StringBuilder().append("/data/dalvik-cache/arm64/").append(v3_1).toString());
                    }
                    if (com.chelpus.Utils.exists("/data/dalvik-cache/x86")) {
                        v6_0 = new java.io.File(new StringBuilder().append("/data/dalvik-cache/x86/").append(v3_1).toString());
                    }
                }
            }
            if ((com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.api >= 23) && (!p14.startsWith("/system/"))) {
                String v7 = "";
                java.io.File v0_1 = new java.io.File(p14);
                if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.startUnderRoot.booleanValue()) {
                    if (com.chelpus.Utils.exists("/data/dalvik-cache/arm")) {
                        v7 = "/arm";
                    }
                    if (com.chelpus.Utils.exists("/data/dalvik-cache/arm64")) {
                        v7 = "/arm64";
                    }
                    if (com.chelpus.Utils.exists("/data/dalvik-cache/x86")) {
                        v7 = "/x86";
                    }
                } else {
                    if ((new java.io.File("/data/dalvik-cache/arm").exists()) && (new java.io.File("/data/dalvik-cache/arm").isDirectory())) {
                        v7 = "/arm";
                    }
                    if ((new java.io.File("/data/dalvik-cache/arm64").exists()) && (new java.io.File("/data/dalvik-cache/arm64").isDirectory())) {
                        v7 = "/arm64";
                    }
                    if ((new java.io.File("/data/dalvik-cache/x86").exists()) && (new java.io.File("/data/dalvik-cache/x86").isDirectory())) {
                        v7 = "/x86";
                    }
                }
                String v8 = com.chelpus.Utils.changeExtension(v0_1.getName(), "odex");
                if (!p14.startsWith("/system")) {
                    v6_0 = new java.io.File(new StringBuilder().append(com.chelpus.Utils.getDirs(v0_1)).append("/oat").append(v7).append("/").append(v8).toString());
                }
            }
        } else {
            String v2_0 = p14.replaceAll("/", "@");
            while (v2_0.startsWith("@")) {
                v2_0 = v2_0.replaceFirst("@", "");
            }
            String v2_1 = new StringBuilder().append(v2_0).append(".oat").toString();
            if (!new java.io.File(new StringBuilder().append("/data/art-cache/").append(v2_1).toString()).exists()) {
            } else {
                System.out.println(new StringBuilder().append("\nLuckyPatcher: found dalvik-cache! ").append("/data/art-cache/").append(v2_1).toString());
                v6_0 = new java.io.File(new StringBuilder().append("/data/art-cache/").append(v2_1).toString());
            }
        }
        return v6_0;
    }

    public static void getFilesWithPkgName(String p13, java.io.File p14, java.util.ArrayList p15)
    {
        com.chelpus.Utils.remount("/system", "rw");
        java.io.File[] v4 = p14.listFiles();
        if ((v4 == null) || (v4.length == 0)) {
            System.out.println(new StringBuilder().append("LuckyPatcher: 0 packages found in ").append(p14.getAbsolutePath()).toString());
        } else {
            int v7_6 = v4.length;
            int v6_3 = 0;
            while (v6_3 < v7_6) {
                java.io.File v0 = v4[v6_3];
                try {
                    if ((v0.getAbsolutePath().endsWith(".apk")) && (p13.equals(new com.android.vending.billing.InAppBillingService.LUCK.FileApkListItem(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance(), v0, 0).pkgName))) {
                        p15.add(v0);
                    }
                    v6_3++;
                } catch (Exception v2_2) {
                    v2_2.printStackTrace();
                }
            }
            boolean v8_2 = v4.length;
            int v7_7 = 0;
            while (v7_7 < v8_2) {
                java.io.File v1 = v4[v7_7];
                if (v1.isDirectory()) {
                    try {
                        java.io.File[] v5 = v1.listFiles();
                    } catch (Exception v2_1) {
                        v2_1.printStackTrace();
                    }
                    if ((v5 != null) && (v5.length != 0)) {
                        int v9_0 = v5.length;
                        int v6_7 = 0;
                        while (v6_7 < v9_0) {
                            java.io.File v3 = v5[v6_7];
                            try {
                                if ((v3.getAbsolutePath().endsWith(".apk")) && (p13.equals(new com.android.vending.billing.InAppBillingService.LUCK.FileApkListItem(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance(), v3, 0).pkgName))) {
                                    p15.add(v3);
                                }
                                v6_7++;
                            } catch (Exception v2_0) {
                                v2_0.printStackTrace();
                            }
                        }
                    }
                }
                v7_7++;
            }
        }
        return;
    }

    public static boolean getMethodsIds(String p19, java.util.ArrayList p20, boolean p21)
    {
        System.out.println(new StringBuilder().append("scan: ").append(p19).toString());
        int v15 = 0;
        java.util.Iterator v2_2 = new String[3];
        v2_2[0] = "chmod";
        v2_2[1] = "777";
        v2_2[2] = p19;
        com.chelpus.Utils.run_all_no_root(v2_2);
        new java.util.ArrayList();
        try {
            if ((!p21) && ((p19 != null) && ((new java.io.File(p19).exists()) && ((new java.io.File(p19).length() != 0) && (new java.io.File(p19).exists()))))) {
                try {
                    java.nio.channels.FileChannel v1 = new java.io.RandomAccessFile(p19, "r").getChannel();
                    java.nio.MappedByteBuffer v9 = v1.map(java.nio.channels.FileChannel$MapMode.READ_ONLY, 0, ((long) ((int) v1.size())));
                    v9.position(64);
                    int v17_0 = com.chelpus.Utils.convertFourBytesToInt(v9.get(), v9.get(), v9.get(), v9.get());
                    int v13_0 = com.chelpus.Utils.convertFourBytesToInt(v9.get(), v9.get(), v9.get(), v9.get());
                    System.out.println(new StringBuilder().append("LuckyPatcher offset_to_data=").append(Integer.toHexString(v13_0)).toString());
                    v9.position(v13_0);
                    int v11_0 = 0;
                    int v10_0 = 0;
                } catch (Exception v8_0) {
                    v8_0.printStackTrace();
                }
                while (v10_0 < v17_0) {
                    byte[] v14_1 = new byte[4];
                    v14_1[0] = v9.get();
                    v14_1[1] = v9.get();
                    v14_1[2] = v9.get();
                    v14_1[3] = v9.get();
                    java.util.Iterator v2_40 = p20.iterator();
                    while (v2_40.hasNext()) {
                        com.android.vending.billing.InAppBillingService.LUCK.CommandItem v12_5 = ((com.android.vending.billing.InAppBillingService.LUCK.CommandItem) v2_40.next());
                        if ((v14_1[0] == v12_5.Object[0]) && ((v14_1[1] == v12_5.Object[1]) && ((v14_1[2] == v12_5.Object[2]) && (v14_1[3] == v12_5.Object[3])))) {
                            v12_5.id_object[0] = ((byte) v11_0);
                            v12_5.id_object[1] = ((byte) (v11_0 >> 8));
                            v12_5.found_id_object = 1;
                        }
                    }
                    v11_0++;
                    v10_0++;
                }
                int v18 = 0;
                java.util.Iterator v2_20 = p20.iterator();
                while (v2_20.hasNext()) {
                    if (((com.android.vending.billing.InAppBillingService.LUCK.CommandItem) v2_20.next()).found_id_object) {
                        v18 = 1;
                    }
                }
                if (v18 != 0) {
                    v9.position(88);
                    int v17_1 = com.chelpus.Utils.convertFourBytesToInt(v9.get(), v9.get(), v9.get(), v9.get());
                    v9.position(com.chelpus.Utils.convertFourBytesToInt(v9.get(), v9.get(), v9.get(), v9.get()));
                    byte[] v7 = new byte[v17_1];
                    int v11_1 = 0;
                    int v10_1 = 0;
                    while (v10_1 < v7.length) {
                        byte[] v14_0 = new byte[8];
                        v14_0[0] = v9.get();
                        v14_0[1] = v9.get();
                        v14_0[2] = v9.get();
                        v14_0[3] = v9.get();
                        v14_0[4] = v9.get();
                        v14_0[5] = v9.get();
                        v14_0[6] = v9.get();
                        v14_0[7] = v9.get();
                        java.util.Iterator v2_34 = p20.iterator();
                        while (v2_34.hasNext()) {
                            com.android.vending.billing.InAppBillingService.LUCK.CommandItem v12_1 = ((com.android.vending.billing.InAppBillingService.LUCK.CommandItem) v2_34.next());
                            if ((v14_0[0] == v12_1.id_object[0]) && ((v14_0[1] == v12_1.id_object[1]) && ((v14_0[4] == v12_1.Method[0]) && ((v14_0[5] == v12_1.Method[1]) && ((v14_0[6] == v12_1.Method[2]) && (v14_0[7] == v12_1.Method[3])))))) {
                                v12_1.index_command[0] = ((byte) v11_1);
                                v12_1.index_command[1] = ((byte) (v11_1 >> 8));
                                v12_1.found_index_command = 1;
                                v15 = 1;
                            }
                        }
                        v11_1++;
                        v10_1++;
                    }
                }
                v1.close();
            }
        } catch (Exception v8_2) {
            System.out.println(v8_2);
        } catch (Exception v8_1) {
            v8_1.printStackTrace();
        }
        return v15;
    }

    public static java.util.ArrayList getMounts()
    {
        String v7;
        if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.startUnderRoot.booleanValue()) {
            com.android.vending.billing.InAppBillingService.LUCK.Mount v11_3 = new String[1];
            v11_3[0] = "mount";
            v7 = com.chelpus.Utils.cmdParam(v11_3);
            if (v7.startsWith("~")) {
                com.android.vending.billing.InAppBillingService.LUCK.Mount v11_7 = new String[2];
                v11_7[0] = new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.toolfilesdir).append("/busybox").toString();
                v11_7[1] = "mount";
                v7 = com.chelpus.Utils.cmdParam(v11_7);
                if (v7.startsWith("~")) {
                    com.android.vending.billing.InAppBillingService.LUCK.Mount v11_11 = new String[2];
                    v11_11[0] = "busybox";
                    v11_11[1] = "mount";
                    v7 = com.chelpus.Utils.cmdParam(v11_11);
                }
                if (v7.startsWith("~")) {
                    com.android.vending.billing.InAppBillingService.LUCK.Mount v11_15 = new String[2];
                    v11_15[0] = "toolbox";
                    v11_15[1] = "mount";
                    v7 = com.chelpus.Utils.cmdParam(v11_15);
                }
            }
        } else {
            com.android.vending.billing.InAppBillingService.LUCK.Mount v11_17 = new com.chelpus.Utils("");
            java.io.File v12_9 = new String[1];
            v12_9[0] = "mount";
            v7 = v11_17.cmdRoot(v12_9);
            if (v7.startsWith("~")) {
                com.chelpus.Utils.exitRoot();
                try {
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getSu();
                } catch (Exception v0_0) {
                    v0_0.printStackTrace();
                }
                com.android.vending.billing.InAppBillingService.LUCK.Mount v11_21 = new com.chelpus.Utils("");
                java.io.File v12_12 = new String[1];
                v12_12[0] = new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.toolfilesdir).append("/busybox mount").toString();
                v7 = v11_21.cmdRoot(v12_12);
                if (v7.startsWith("~")) {
                    com.android.vending.billing.InAppBillingService.LUCK.Mount v11_25 = new com.chelpus.Utils("");
                    java.io.File v12_15 = new String[1];
                    v12_15[0] = "busybox mount";
                    v7 = v11_25.cmdRoot(v12_15);
                }
                if (v7.startsWith("~")) {
                    com.android.vending.billing.InAppBillingService.LUCK.Mount v11_29 = new com.chelpus.Utils("");
                    java.io.File v12_18 = new String[1];
                    v12_18[0] = "toolbox mount";
                    v7 = v11_29.cmdRoot(v12_18);
                }
            }
        }
        String[] v8 = v7.split("\n");
        java.util.ArrayList v5_1 = new java.util.ArrayList();
        int v9 = 0;
        try {
            java.io.File v12_19 = v8.length;
            com.android.vending.billing.InAppBillingService.LUCK.Mount v11_31 = 0;
        } catch (Exception v0_1) {
            v0_1.printStackTrace();
            if (v9 == 0) {
                System.out.println("LuckyPatcher: get mounts from /proc/mounts");
                if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.startUnderRoot.booleanValue()) {
                    com.chelpus.Utils.copyFile("/proc/mounts", "/data/local/RootToolsMounts", 0, 1);
                    com.android.vending.billing.InAppBillingService.LUCK.Mount v11_37 = new String[3];
                    v11_37[0] = "chmod";
                    v11_37[1] = "777";
                    v11_37[2] = "/data/local/RootToolsMounts";
                    com.chelpus.Utils.run_all_no_root(v11_37);
                } else {
                    com.chelpus.Utils.copyFile("/proc/mounts", "/data/local/RootToolsMounts", 0, 1);
                    com.chelpus.Utils.run_all("chmod 777 /data/local/RootToolsMounts");
                }
                v5_1.clear();
                try {
                    java.io.LineNumberReader v4_1 = new java.io.LineNumberReader(new java.io.FileReader("/data/local/RootToolsMounts"));
                    try {
                        java.util.ArrayList v6_1 = new java.util.ArrayList();
                    } catch (Exception v0_2) {
                        v0_2.printStackTrace();
                    }
                    try {
                        while(true) {
                            String v2_1 = v4_1.readLine();
                            String[] v1_1 = v2_1.split(" ");
                            v6_1.add(new com.android.vending.billing.InAppBillingService.LUCK.Mount(new java.io.File(v1_1[0]), new java.io.File(v1_1[1]), v1_1[2], v1_1[3]));
                        }
                        v4_1.close();
                        v5_1 = v6_1;
                    } catch (Exception v0_2) {
                        v5_1 = v6_1;
                    }
                    if (v2_1 == null) {
                    }
                } catch (Exception v0_2) {
                }
            }
            return v5_1;
        }
        while (v11_31 < v12_19) {
            String[] v1_0 = v8[v11_31].replace(" on ", " ").replace(" type ", " ").split("\\s+");
            if (v1_0[1].startsWith("/system")) {
                v9 = 1;
            }
            v5_1.add(new com.android.vending.billing.InAppBillingService.LUCK.Mount(new java.io.File(v1_0[0]), new java.io.File(v1_0[1]), v1_0[2], v1_0[3]));
            v11_31++;
        }
    }

    public static String getOdexForCreate(String p11, String p12)
    {
        if ((com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.startUnderRoot == null) || (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.startUnderRoot.booleanValue())) {
            if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.startUnderRoot == null) {
                System.out.println("uderRoot not defined");
            }
            if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.startUnderRoot.booleanValue()) {
                System.out.println("uderRoot false");
            }
        } else {
            System.out.println("Start under Root");
        }
        String[] v4_141;
        java.io.File v0_1 = new java.io.File(p11);
        int v2 = 0;
        String v1 = "";
        if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.api < 23) {
            if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.startUnderRoot.booleanValue()) {
                if (com.chelpus.Utils.exists("/data/dalvik-cache/arm")) {
                    v1 = "/arm";
                }
                if (com.chelpus.Utils.exists("/data/dalvik-cache/arm64")) {
                    v1 = "/arm64";
                }
                if (com.chelpus.Utils.exists("/data/dalvik-cache/x86")) {
                    v1 = "/x86";
                }
                if ((!v1.equals("")) && (com.chelpus.Utils.exists(new StringBuilder().append("/data/dalvik-cache").append(v1).toString()))) {
                    if (!com.chelpus.Utils.exists(new StringBuilder().append(com.chelpus.Utils.getDirs(v0_1)).append(v1).toString())) {
                        com.chelpus.Utils.run_all(new StringBuilder().append("mkdir -p \'").append(com.chelpus.Utils.getDirs(v0_1)).append(v1).append("\'").toString());
                        if (com.chelpus.Utils.exists(new StringBuilder().append(com.chelpus.Utils.getDirs(v0_1)).append(v1).toString())) {
                            if (p12.equals("0")) {
                                com.chelpus.Utils.run_all(new StringBuilder().append("chmod 755 ").append(com.chelpus.Utils.getDirs(v0_1)).append(v1).toString());
                                com.chelpus.Utils.run_all(new StringBuilder().append("chown 0.").append(p12).append(" ").append(com.chelpus.Utils.getDirs(v0_1)).append(v1).toString());
                                com.chelpus.Utils.run_all(new StringBuilder().append("chown 0:").append(p12).append(" ").append(com.chelpus.Utils.getDirs(v0_1)).append(v1).toString());
                            } else {
                                com.chelpus.Utils.run_all(new StringBuilder().append("chmod 755 ").append(com.chelpus.Utils.getDirs(v0_1)).append(v1).toString());
                                com.chelpus.Utils.run_all(new StringBuilder().append("chown 1000.").append(p12).append(" ").append(com.chelpus.Utils.getDirs(v0_1)).append(v1).toString());
                                com.chelpus.Utils.run_all(new StringBuilder().append("chown 1000:").append(p12).append(" ").append(com.chelpus.Utils.getDirs(v0_1)).append(v1).toString());
                            }
                        }
                    }
                    v2 = 1;
                }
            } else {
                if ((new java.io.File("/data/dalvik-cache/arm").exists()) && (new java.io.File("/data/dalvik-cache/arm").isDirectory())) {
                    v1 = "/arm";
                }
                if ((new java.io.File("/data/dalvik-cache/arm64").exists()) && (new java.io.File("/data/dalvik-cache/arm64").isDirectory())) {
                    v1 = "/arm64";
                }
                if ((new java.io.File("/data/dalvik-cache/x86").exists()) && (new java.io.File("/data/dalvik-cache/x86").isDirectory())) {
                    v1 = "/x86";
                }
                if ((!v1.equals("")) && ((new java.io.File(new StringBuilder().append("/data/dalvik-cache").append(v1).toString()).exists()) && (new java.io.File(new StringBuilder().append("/data/dalvik-cache").append(v1).toString()).isDirectory()))) {
                    System.out.println(new StringBuilder().append(v1).append(" to dalvik cache found").toString());
                    System.out.println(new StringBuilder().append("check ").append(com.chelpus.Utils.getDirs(v0_1)).append(v1).toString());
                    if ((!new java.io.File(new StringBuilder().append(com.chelpus.Utils.getDirs(v0_1)).append(v1).toString()).exists()) || (!new java.io.File(new StringBuilder().append(com.chelpus.Utils.getDirs(v0_1)).append(v1).toString()).isDirectory())) {
                        new java.io.File(new StringBuilder().append(com.chelpus.Utils.getDirs(v0_1)).append(v1).toString()).mkdirs();
                        System.out.println("try make dirs");
                        if (new java.io.File(new StringBuilder().append(com.chelpus.Utils.getDirs(v0_1)).append(v1).toString()).exists()) {
                            System.out.println("dirs created");
                            if (p12.equals("0")) {
                                String[] v4_134 = new String[3];
                                v4_134[0] = "chmod";
                                v4_134[1] = "755";
                                v4_134[2] = new StringBuilder().append(com.chelpus.Utils.getDirs(v0_1)).append(v1).toString();
                                com.chelpus.Utils.run_all_no_root(v4_134);
                                String[] v4_135 = new String[3];
                                v4_135[0] = "chown";
                                v4_135[1] = new StringBuilder().append("0.").append(p12).toString();
                                v4_135[2] = new StringBuilder().append(com.chelpus.Utils.getDirs(v0_1)).append(v1).toString();
                                com.chelpus.Utils.run_all_no_root(v4_135);
                                String[] v4_136 = new String[3];
                                v4_136[0] = "chown";
                                v4_136[1] = new StringBuilder().append("0:").append(p12).toString();
                                v4_136[2] = new StringBuilder().append(com.chelpus.Utils.getDirs(v0_1)).append(v1).toString();
                                com.chelpus.Utils.run_all_no_root(v4_136);
                            } else {
                                String[] v4_137 = new String[3];
                                v4_137[0] = "chmod";
                                v4_137[1] = "755";
                                v4_137[2] = new StringBuilder().append(com.chelpus.Utils.getDirs(v0_1)).append(v1).toString();
                                com.chelpus.Utils.run_all_no_root(v4_137);
                                String[] v4_138 = new String[3];
                                v4_138[0] = "chown";
                                v4_138[1] = new StringBuilder().append("1000.").append(p12).toString();
                                v4_138[2] = new StringBuilder().append(com.chelpus.Utils.getDirs(v0_1)).append(v1).toString();
                                com.chelpus.Utils.run_all_no_root(v4_138);
                                String[] v4_139 = new String[3];
                                v4_139[0] = "chown";
                                v4_139[1] = new StringBuilder().append("1000:").append(p12).toString();
                                v4_139[2] = new StringBuilder().append(com.chelpus.Utils.getDirs(v0_1)).append(v1).toString();
                                com.chelpus.Utils.run_all_no_root(v4_139);
                            }
                        }
                    }
                    v2 = 1;
                }
            }
            if (v2 == 0) {
                v4_141 = com.chelpus.Utils.changeExtension(p11, "odex");
            } else {
                v4_141 = new StringBuilder().append(com.chelpus.Utils.getDirs(v0_1)).append(v1).append("/").append(com.chelpus.Utils.changeExtension(v0_1.getName(), "odex")).toString();
            }
        } else {
            String v3 = com.chelpus.Utils.changeExtension(v0_1.getName(), "odex");
            if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.startUnderRoot.booleanValue()) {
                if (com.chelpus.Utils.exists("/data/dalvik-cache/arm")) {
                    v1 = "/arm";
                }
                if (com.chelpus.Utils.exists("/data/dalvik-cache/arm64")) {
                    v1 = "/arm64";
                }
                if (com.chelpus.Utils.exists("/data/dalvik-cache/x86")) {
                    v1 = "/x86";
                }
                if ((!v1.equals("")) && ((com.chelpus.Utils.exists(new StringBuilder().append("/data/dalvik-cache").append(v1).toString())) && (!com.chelpus.Utils.exists(new StringBuilder().append(com.chelpus.Utils.getDirs(v0_1)).append("/oat").append(v1).toString())))) {
                    com.chelpus.Utils.run_all(new StringBuilder().append("mkdir -p \'").append(com.chelpus.Utils.getDirs(v0_1)).append("/oat").append(v1).append("\'").toString());
                    if (com.chelpus.Utils.exists(new StringBuilder().append(com.chelpus.Utils.getDirs(v0_1)).append("/oat").append(v1).toString())) {
                        if (p12.equals("0")) {
                            com.chelpus.Utils.run_all(new StringBuilder().append("chmod 755 ").append(com.chelpus.Utils.getDirs(v0_1)).append("/oat").toString());
                            com.chelpus.Utils.run_all(new StringBuilder().append("chown 0.").append(p12).append(" ").append(com.chelpus.Utils.getDirs(v0_1)).append("/oat").toString());
                            com.chelpus.Utils.run_all(new StringBuilder().append("chown 0:").append(p12).append(" ").append(com.chelpus.Utils.getDirs(v0_1)).append("/oat").toString());
                            com.chelpus.Utils.run_all(new StringBuilder().append("chmod 755 ").append(com.chelpus.Utils.getDirs(v0_1)).append("/oat").append(v1).toString());
                            com.chelpus.Utils.run_all(new StringBuilder().append("chown 0.").append(p12).append(" ").append(com.chelpus.Utils.getDirs(v0_1)).append("/oat").append(v1).toString());
                            com.chelpus.Utils.run_all(new StringBuilder().append("chown 0:").append(p12).append(" ").append(com.chelpus.Utils.getDirs(v0_1)).append("/oat").append(v1).toString());
                        } else {
                            com.chelpus.Utils.run_all(new StringBuilder().append("chmod 755 ").append(com.chelpus.Utils.getDirs(v0_1)).append("/oat").toString());
                            com.chelpus.Utils.run_all(new StringBuilder().append("chown 1000.").append(p12).append(" ").append(com.chelpus.Utils.getDirs(v0_1)).append("/oat").toString());
                            com.chelpus.Utils.run_all(new StringBuilder().append("chown 1000:").append(p12).append(" ").append(com.chelpus.Utils.getDirs(v0_1)).append("/oat").toString());
                            com.chelpus.Utils.run_all(new StringBuilder().append("chmod 755 ").append(com.chelpus.Utils.getDirs(v0_1)).append("/oat").append(v1).toString());
                            com.chelpus.Utils.run_all(new StringBuilder().append("chown 1000.").append(p12).append(" ").append(com.chelpus.Utils.getDirs(v0_1)).append("/oat").append(v1).toString());
                            com.chelpus.Utils.run_all(new StringBuilder().append("chown 1000:").append(p12).append(" ").append(com.chelpus.Utils.getDirs(v0_1)).append("/oat").append(v1).toString());
                        }
                    }
                }
            } else {
                if ((new java.io.File("/data/dalvik-cache/arm").exists()) && (new java.io.File("/data/dalvik-cache/arm").isDirectory())) {
                    v1 = "/arm";
                }
                if ((new java.io.File("/data/dalvik-cache/arm64").exists()) && (new java.io.File("/data/dalvik-cache/arm64").isDirectory())) {
                    v1 = "/arm64";
                }
                if ((new java.io.File("/data/dalvik-cache/x86").exists()) && (new java.io.File("/data/dalvik-cache/x86").isDirectory())) {
                    v1 = "/x86";
                }
                if ((!v1.equals("")) && ((new java.io.File(new StringBuilder().append("/data/dalvik-cache").append(v1).toString()).exists()) && (new java.io.File(new StringBuilder().append("/data/dalvik-cache").append(v1).toString()).isDirectory()))) {
                    System.out.println(new StringBuilder().append(v1).append(" to dalvik cache found").toString());
                    System.out.println(new StringBuilder().append("check ").append(com.chelpus.Utils.getDirs(v0_1)).append("/oat").append(v1).toString());
                    if ((!new java.io.File(new StringBuilder().append(com.chelpus.Utils.getDirs(v0_1)).append("/oat").append(v1).toString()).exists()) || (!new java.io.File(new StringBuilder().append(com.chelpus.Utils.getDirs(v0_1)).append("/oat").append(v1).toString()).isDirectory())) {
                        new java.io.File(new StringBuilder().append(com.chelpus.Utils.getDirs(v0_1)).append("/oat").append(v1).toString()).mkdirs();
                        System.out.println("try make dirs");
                        if (new java.io.File(new StringBuilder().append(com.chelpus.Utils.getDirs(v0_1)).append("/oat").append(v1).toString()).exists()) {
                            System.out.println("dirs created");
                            if (p12.equals("0")) {
                                String[] v4_326 = new String[3];
                                v4_326[0] = "chmod";
                                v4_326[1] = "755";
                                v4_326[2] = new StringBuilder().append(com.chelpus.Utils.getDirs(v0_1)).append("/oat").toString();
                                com.chelpus.Utils.run_all_no_root(v4_326);
                                String[] v4_327 = new String[3];
                                v4_327[0] = "chown";
                                v4_327[1] = new StringBuilder().append("0.").append(p12).toString();
                                v4_327[2] = new StringBuilder().append(com.chelpus.Utils.getDirs(v0_1)).append("/oat").toString();
                                com.chelpus.Utils.run_all_no_root(v4_327);
                                String[] v4_328 = new String[3];
                                v4_328[0] = "chown";
                                v4_328[1] = new StringBuilder().append("0:").append(p12).toString();
                                v4_328[2] = new StringBuilder().append(com.chelpus.Utils.getDirs(v0_1)).append("/oat").toString();
                                com.chelpus.Utils.run_all_no_root(v4_328);
                                String[] v4_329 = new String[3];
                                v4_329[0] = "chmod";
                                v4_329[1] = "755";
                                v4_329[2] = new StringBuilder().append(com.chelpus.Utils.getDirs(v0_1)).append("/oat").append(v1).toString();
                                com.chelpus.Utils.run_all_no_root(v4_329);
                                String[] v4_330 = new String[3];
                                v4_330[0] = "chown";
                                v4_330[1] = new StringBuilder().append("0.").append(p12).toString();
                                v4_330[2] = new StringBuilder().append(com.chelpus.Utils.getDirs(v0_1)).append("/oat").append(v1).toString();
                                com.chelpus.Utils.run_all_no_root(v4_330);
                                String[] v4_331 = new String[3];
                                v4_331[0] = "chown";
                                v4_331[1] = new StringBuilder().append("0:").append(p12).toString();
                                v4_331[2] = new StringBuilder().append(com.chelpus.Utils.getDirs(v0_1)).append("/oat").append(v1).toString();
                                com.chelpus.Utils.run_all_no_root(v4_331);
                            } else {
                                String[] v4_332 = new String[3];
                                v4_332[0] = "chmod";
                                v4_332[1] = "755";
                                v4_332[2] = new StringBuilder().append(com.chelpus.Utils.getDirs(v0_1)).append("/oat").toString();
                                com.chelpus.Utils.run_all_no_root(v4_332);
                                String[] v4_333 = new String[3];
                                v4_333[0] = "chown";
                                v4_333[1] = new StringBuilder().append("1000.").append(p12).toString();
                                v4_333[2] = new StringBuilder().append(com.chelpus.Utils.getDirs(v0_1)).append("/oat").toString();
                                com.chelpus.Utils.run_all_no_root(v4_333);
                                String[] v4_334 = new String[3];
                                v4_334[0] = "chown";
                                v4_334[1] = new StringBuilder().append("1000:").append(p12).toString();
                                v4_334[2] = new StringBuilder().append(com.chelpus.Utils.getDirs(v0_1)).append("/oat").toString();
                                com.chelpus.Utils.run_all_no_root(v4_334);
                                String[] v4_335 = new String[3];
                                v4_335[0] = "chmod";
                                v4_335[1] = "755";
                                v4_335[2] = new StringBuilder().append(com.chelpus.Utils.getDirs(v0_1)).append("/oat").append(v1).toString();
                                com.chelpus.Utils.run_all_no_root(v4_335);
                                String[] v4_336 = new String[3];
                                v4_336[0] = "chown";
                                v4_336[1] = new StringBuilder().append("1000.").append(p12).toString();
                                v4_336[2] = new StringBuilder().append(com.chelpus.Utils.getDirs(v0_1)).append("/oat").append(v1).toString();
                                com.chelpus.Utils.run_all_no_root(v4_336);
                                String[] v4_337 = new String[3];
                                v4_337[0] = "chown";
                                v4_337[1] = new StringBuilder().append("1000:").append(p12).toString();
                                v4_337[2] = new StringBuilder().append(com.chelpus.Utils.getDirs(v0_1)).append("/oat").append(v1).toString();
                                com.chelpus.Utils.run_all_no_root(v4_337);
                            }
                        }
                    }
                }
            }
            v4_141 = new StringBuilder().append(com.chelpus.Utils.getDirs(v0_1)).append("/oat").append(v1).append("/").append(v3).toString();
        }
        return v4_141;
    }

    public static String getPermissions(String p10)
    {
        String v2_18;
        if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.startUnderRoot.booleanValue()) {
            String v2_3 = new String[5];
            v2_3[0] = new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.toolfilesdir).append("/busybox").toString();
            v2_3[1] = "stat";
            v2_3[2] = "-c";
            v2_3[3] = "%a";
            v2_3[4] = p10;
            String v0_1 = com.chelpus.Utils.cmdParam(v2_3).replaceAll("\n", "").replaceAll("\r", "").trim();
            System.out.println(new StringBuilder().append("\'").append(v0_1).append("\'").toString());
            if (!v0_1.matches("(\\d+)")) {
                System.out.println("try get permission again");
                String v2_12 = new String[5];
                v2_12[0] = "busybox";
                v2_12[1] = "stat";
                v2_12[2] = "-c";
                v2_12[3] = "%a";
                v2_12[4] = p10;
                v0_1 = com.chelpus.Utils.cmdParam(v2_12).replaceAll("\n", "").replaceAll("\r", "").trim();
            }
            if (v0_1.matches("(\\d+)")) {
                v2_18 = v0_1;
            } else {
                v2_18 = "";
            }
        } else {
            String v2_20 = new String[5];
            v2_20[0] = new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.toolfilesdir).append("/busybox").toString();
            v2_20[1] = "stat";
            v2_20[2] = "-c";
            v2_20[3] = "%a";
            v2_20[4] = p10;
            String v0_4 = com.chelpus.Utils.cmdParam(v2_20).replaceAll("\n", "").replaceAll("\r", "").trim();
            System.out.println(v0_4);
            if (!v0_4.matches("(\\d+)")) {
                System.out.println("try get permission again");
                String v2_29 = new String[5];
                v2_29[0] = "busybox";
                v2_29[1] = "stat";
                v2_29[2] = "-c";
                v2_29[3] = "%a";
                v2_29[4] = p10;
                v0_4 = com.chelpus.Utils.cmdParam(v2_29).replaceAll("\n", "").replaceAll("\r", "").trim();
            }
            if (v0_4.matches("(\\d+)")) {
                v2_18 = v0_4;
            } else {
                v2_18 = "";
            }
        }
        return v2_18;
    }

    public static android.content.pm.PackageInfo getPkgInfo(String p3, int p4)
    {
        try {
            android.content.pm.PackageInfo v1 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getPackageInfo(p3, p4);
        } catch (IllegalArgumentException v0_1) {
            v0_1.printStackTrace();
        } catch (IllegalArgumentException v0_0) {
            v0_0.printStackTrace();
        }
        return v1;
    }

    public static String getPlaceForOdex(String p7, boolean p8)
    {
        String v4_42;
        java.io.File v0_1 = new java.io.File(p7);
        int v2 = 0;
        String v1 = "";
        if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.api < 23) {
            if (!p8) {
                if (com.chelpus.Utils.exists("/data/dalvik-cache/arm")) {
                    v1 = "/arm";
                }
                if (com.chelpus.Utils.exists("/data/dalvik-cache/arm64")) {
                    v1 = "/arm64";
                }
                if (com.chelpus.Utils.exists("/data/dalvik-cache/x86")) {
                    v1 = "/x86";
                }
                if ((!v1.equals("")) && (com.chelpus.Utils.exists(new StringBuilder().append("/data/dalvik-cache").append(v1).toString()))) {
                    v2 = 1;
                }
            } else {
                if ((new java.io.File("/data/dalvik-cache/arm").exists()) && (new java.io.File("/data/dalvik-cache/arm").isDirectory())) {
                    v1 = "/arm";
                }
                if ((new java.io.File("/data/dalvik-cache/arm64").exists()) && (new java.io.File("/data/dalvik-cache/arm64").isDirectory())) {
                    v1 = "/arm64";
                }
                if ((new java.io.File("/data/dalvik-cache/x86").exists()) && (new java.io.File("/data/dalvik-cache/x86").isDirectory())) {
                    v1 = "/x86";
                }
                if ((!v1.equals("")) && ((new java.io.File(new StringBuilder().append("/data/dalvik-cache").append(v1).toString()).exists()) && (new java.io.File(new StringBuilder().append("/data/dalvik-cache").append(v1).toString()).isDirectory()))) {
                    v2 = 1;
                }
            }
            if (v2 == 0) {
                v4_42 = com.chelpus.Utils.changeExtension(p7, "odex");
            } else {
                v4_42 = new StringBuilder().append(com.chelpus.Utils.getDirs(v0_1)).append(v1).append("/").append(com.chelpus.Utils.changeExtension(v0_1.getName(), "odex")).toString();
            }
        } else {
            String v3 = com.chelpus.Utils.changeExtension(v0_1.getName(), "odex");
            if (!p8) {
                if (com.chelpus.Utils.exists("/data/dalvik-cache/arm")) {
                    v1 = "/arm";
                }
                if (com.chelpus.Utils.exists("/data/dalvik-cache/arm64")) {
                    v1 = "/arm64";
                }
                if (com.chelpus.Utils.exists("/data/dalvik-cache/x86")) {
                    v1 = "/x86";
                }
                // Both branches of the condition point to the same code.
                // if ((!v1.equals("")) && (!com.chelpus.Utils.exists(new StringBuilder().append("/data/dalvik-cache").append(v1).toString()))) {
                // }
            } else {
                if ((new java.io.File("/data/dalvik-cache/arm").exists()) && (new java.io.File("/data/dalvik-cache/arm").isDirectory())) {
                    v1 = "/arm";
                }
                if ((new java.io.File("/data/dalvik-cache/arm64").exists()) && (new java.io.File("/data/dalvik-cache/arm64").isDirectory())) {
                    v1 = "/arm64";
                }
                if ((new java.io.File("/data/dalvik-cache/x86").exists()) && (new java.io.File("/data/dalvik-cache/x86").isDirectory())) {
                    v1 = "/x86";
                }
                // Both branches of the condition point to the same code.
                // if ((!v1.equals("")) && ((new java.io.File(new StringBuilder().append("/data/dalvik-cache").append(v1).toString()).exists()) && (!new java.io.File(new StringBuilder().append("/data/dalvik-cache").append(v1).toString()).isDirectory()))) {
                // }
            }
            v4_42 = new StringBuilder().append(com.chelpus.Utils.getDirs(v0_1)).append("/oat").append(v1).append("/").append(v3).toString();
        }
        return v4_42;
    }

    public static long getRandom(long p12, long p14)
    {
        do {
            long v0 = ((new java.util.Random().nextLong() << 1) >> 1);
            long v5 = (v0 % ((p14 - p12) + 1));
        } while(((v0 - v5) + (p14 - p12)) < 0);
        return (v5 + p12);
    }

    public static String getRandomStringLowerCase(int p5)
    {
        StringBuilder v1_1 = new StringBuilder(p5);
        int v0 = 0;
        while (v0 < p5) {
            v1_1.append("abcdefghijklmnopqrstuvwxyz".charAt(com.chelpus.Utils.rnd.nextInt("abcdefghijklmnopqrstuvwxyz".length())));
            v0++;
        }
        return v1_1.toString();
    }

    public static String getRandomStringUpperLowerCase(int p5)
    {
        StringBuilder v1_1 = new StringBuilder(p5);
        int v0 = 0;
        while (v0 < p5) {
            v1_1.append("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz".charAt(com.chelpus.Utils.rnd.nextInt("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz".length())));
            v0++;
        }
        return v1_1.toString();
    }

    public static long getRawLength(int p9)
    {
        try {
            long v3 = 0;
            byte[] v5 = new byte[8192];
        } catch (java.io.IOException v1) {
            v3 = 0;
            return v3;
        }
        while(true) {
            int v0 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getRes().openRawResource(p9).read(v5);
            if (v0 == -1) {
                break;
            }
            v3 += ((long) v0);
        }
        System.out.println(new StringBuilder().append("LuckyPatcher (RAW): length = ").append(v3).toString());
        return v3;
    }

    public static boolean getRawToFile(int p8, java.io.File p9)
    {
        int v5 = 0;
        if (!p9.exists()) {
            com.chelpus.Utils.getDirs(p9).mkdirs();
        } else {
            p9.delete();
        }
        System.out.println("try get file from raw");
        try {
            byte[] v4 = new byte[8192];
            java.io.InputStream v0 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getRes().openRawResource(p8);
            java.io.FileOutputStream v1_1 = new java.io.FileOutputStream(p9);
            int v2 = v0.read(v4);
        } catch (java.io.IOException v3) {
            return v5;
        }
        while (v2 > 0) {
            v1_1.write(v4, 0, v2);
            v2 = v0.read(v4);
        }
        v1_1.flush();
        v1_1.close();
        v0.close();
        System.out.println("get file from raw");
        v5 = 1;
        return v5;
    }

    public static String getRawToString(int p8)
    {
        String v4 = "";
        try {
            byte[] v3 = new byte[2048];
            java.io.InputStream v0 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getRes().openRawResource(p8);
            int v1 = 0;
        } catch (java.io.IOException v2) {
            String v5_3 = "";
            return v5_3;
        }
        while (v1 != -1) {
            v1 = v0.read(v3);
            v4 = new StringBuilder().append(v4).append(new String(v3, "UTF-8")).toString();
        }
        v0.close();
        v5_3 = v4;
        return v5_3;
    }

    public static void getRoot()
    {
        if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.startUnderRoot.booleanValue()) {
            try {
                if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.suProcess != null) {
                    try {
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.suOutputStream.writeBytes("echo chelpusstart!\n");
                    } catch (Exception v0) {
                        com.chelpus.Utils.exitRoot();
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.suProcess = Runtime.getRuntime().exec("su");
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.suOutputStream = new java.io.DataOutputStream(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.suProcess.getOutputStream());
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.suInputStream = new java.io.DataInputStream(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.suProcess.getInputStream());
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.suErrorInputStream = new java.io.DataInputStream(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.suProcess.getErrorStream());
                    }
                } else {
                    System.out.println("LuckyPatcher: GET ROOT.");
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.suProcess = Runtime.getRuntime().exec("su");
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.suOutputStream = new java.io.DataOutputStream(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.suProcess.getOutputStream());
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.suInputStream = new java.io.DataInputStream(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.suProcess.getInputStream());
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.suErrorInputStream = new java.io.DataInputStream(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.suProcess.getErrorStream());
                }
            } catch (Exception v0) {
                v0.printStackTrace();
            }
        }
        return;
    }

    public static boolean getRootUid()
    {
        int v1 = 0;
        try {
            if (System.getProperty("user.name").contains("root")) {
                v1 = 1;
            }
        } catch (Exception v0) {
            v0.printStackTrace();
        }
        return v1;
    }

    private static void getSignatureKeys()
    {
        try {
            if ((new java.io.File(new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.toolfilesdir).append("/").append("zipalign").toString()).exists()) && (new java.io.File(new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.toolfilesdir).append("/").append("zipalign").toString()).length() == com.chelpus.Utils.getRawLength(2131099663))) {
                com.chelpus.Utils.chmod(new java.io.File(new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.toolfilesdir).append("/").append("zipalign").toString()), 777);
                com.chelpus.Utils.run_all(new StringBuilder().append("chmod 777 ").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.toolfilesdir).append("/").append("zipalign").toString());
                com.chelpus.Utils.getAssets("testkey.pk8", new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.basepath).append("/Modified/Keys").toString());
                com.chelpus.Utils.getAssets("testkey.sbt", new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.basepath).append("/Modified/Keys").toString());
                com.chelpus.Utils.getAssets("testkey.x509.pem", new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.basepath).append("/Modified/Keys").toString());
            } else {
                com.chelpus.Utils.getRawToFile(2131099663, new java.io.File(new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.toolfilesdir).append("/zipalign").toString()));
                com.chelpus.Utils.chmod(new java.io.File(new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance().getFilesDir()).append("/").append("zipalign").toString()), 777);
                com.chelpus.Utils.run_all(new StringBuilder().append("chmod 777 ").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance().getFilesDir()).append("/").append("zipalign").toString());
            }
        } catch (Exception v0) {
            v0.printStackTrace();
        }
        return;
    }

    public static String getSimulink(String p12)
    {
        String v1;
        if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.startUnderRoot.booleanValue()) {
            java.io.PrintStream v5_3 = new com.chelpus.Utils("");
            String v6_1 = new String[1];
            v6_1[0] = new StringBuilder().append("ls -l ").append(p12).toString();
            java.util.regex.Matcher v2_0 = java.util.regex.Pattern.compile("^.*?\\-\\>\\s+(.*)$").matcher(v5_3.cmdRoot(v6_1));
            if (!v2_0.find()) {
                java.io.PrintStream v5_6 = new com.chelpus.Utils("");
                String v6_3 = new String[1];
                v6_3[0] = new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.toolfilesdir).append("/busybox ls -l ").append(p12).toString();
                java.util.regex.Matcher v2_1 = java.util.regex.Pattern.compile("^.*?\\-\\>\\s+(.*)$").matcher(v5_6.cmdRoot(v6_3));
                if (!v2_1.find()) {
                    System.out.println("No symlink found!");
                    v1 = "";
                } else {
                    v1 = v2_1.group(1);
                    System.out.println(new StringBuilder().append("Symlink found: ").append(v1).toString());
                }
            } else {
                v1 = v2_0.group(1);
                System.out.println(new StringBuilder().append("Symlink found: ").append(v1).toString());
            }
        } else {
            java.io.PrintStream v5_11 = new String[3];
            v5_11[0] = "ls";
            v5_11[1] = "-l";
            v5_11[2] = p12;
            java.util.regex.Matcher v2_2 = java.util.regex.Pattern.compile("^.*?\\-\\>\\s+(.*)$").matcher(com.chelpus.Utils.cmdParam(v5_11));
            if (!v2_2.find()) {
                java.io.PrintStream v5_14 = new String[4];
                v5_14[0] = new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.toolfilesdir).append("/busybox").toString();
                v5_14[1] = "ls";
                v5_14[2] = "-l";
                v5_14[3] = p12;
                String v0_3 = com.chelpus.Utils.cmdParam(v5_14);
                System.out.println(v0_3);
                java.util.regex.Matcher v2_3 = java.util.regex.Pattern.compile("^.*?\\-\\>\\s+(.*)$").matcher(v0_3);
                if (!v2_3.find()) {
                    System.out.println("No symlink found!");
                    v1 = "";
                } else {
                    v1 = v2_3.group(1);
                    System.out.println(new StringBuilder().append("Symlink found: ").append(v1).toString());
                }
            } else {
                v1 = v2_2.group(1);
                System.out.println(new StringBuilder().append("Symlink found: ").append(v1).toString());
            }
        }
        return v1;
    }

    public static java.util.ArrayList getStringIds(String p26, java.util.ArrayList p27, boolean p28)
    {
        System.out.println(new StringBuilder().append("scan: ").append(p26).toString());
        int v2_2 = new String[3];
        v2_2[0] = "chmod";
        v2_2[1] = "777";
        v2_2[2] = p26;
        com.chelpus.Utils.run_all_no_root(v2_2);
        java.util.ArrayList v20_1 = new java.util.ArrayList();
        try {
            if (p28) {
                if (new java.io.File(p26).exists()) {
                    try {
                        java.nio.channels.FileChannel v1_0 = new java.io.RandomAccessFile(p26, "r").getChannel();
                        java.nio.MappedByteBuffer v10_0 = v1_0.map(java.nio.channels.FileChannel$MapMode.READ_ONLY, 0, ((long) ((int) v1_0.size())));
                        v10_0.position(8);
                        int v16 = com.chelpus.Utils.convertFourBytesToInt(v10_0.get(), v10_0.get(), v10_0.get(), v10_0.get());
                        v10_0.position((v16 + 56));
                        int v22_0 = com.chelpus.Utils.convertFourBytesToInt(v10_0.get(), v10_0.get(), v10_0.get(), v10_0.get());
                        v10_0.position((com.chelpus.Utils.convertFourBytesToInt(v10_0.get(), v10_0.get(), v10_0.get(), v10_0.get()) + v16));
                        int v24_0 = v22_0;
                        java.util.ArrayList v0_4 = new int[v24_0];
                        int[] v17_0 = v0_4;
                        int v11_0 = 0;
                    } catch (Exception v9_0) {
                        v9_0.printStackTrace();
                    }
                    while (v11_0 < v24_0) {
                        v17_0[v11_0] = (com.chelpus.Utils.convertFourBytesToInt(v10_0.get(), v10_0.get(), v10_0.get(), v10_0.get()) + v16);
                        v11_0++;
                    }
                    int v12_0 = 0;
                    java.util.ArrayList v0_6 = new int[p27.size()];
                    int[] v23_0 = v0_6;
                    java.util.ArrayList v0_7 = new String[p27.size()];
                    String[] v25_0 = v0_7;
                    int v11_1 = 0;
                    while (v11_1 < v23_0.length) {
                        v23_0[v11_1] = ((String) p27.get(v11_1)).length();
                        v25_0[v11_1] = ((String) p27.get(v11_1));
                        v11_1++;
                    }
                    int v3_13 = v17_0.length;
                    int v2_18 = 0;
                    while (v2_18 < v3_13) {
                        int v14_0 = v17_0[v2_18];
                        int v13_0 = 0;
                        while (v13_0 < v23_0.length) {
                            int v21_0 = com.chelpus.Utils.convertByteToInt(v10_0.get(v14_0));
                            if (v21_0 == v23_0[v13_0]) {
                                byte[] v8_0 = new byte[v21_0];
                                v10_0.position((v14_0 + 1));
                                int v18_0 = 0;
                                while (v18_0 < v8_0.length) {
                                    v8_0[v18_0] = v10_0.get();
                                    v18_0++;
                                }
                                if (v25_0[v13_0].equals(new String(v8_0))) {
                                    java.util.ArrayList v0_14 = new byte[4];
                                    byte[] v19_0 = v0_14;
                                    v19_0[0] = ((byte) v12_0);
                                    v19_0[1] = ((byte) (v12_0 >> 8));
                                    v19_0[2] = ((byte) (v12_0 >> 16));
                                    v19_0[3] = ((byte) (v12_0 >> 24));
                                    int v7_0 = 0;
                                    if (v17_0.length > 65535) {
                                        v7_0 = 1;
                                    }
                                    v20_1.add(new com.android.vending.billing.InAppBillingService.LUCK.StringItem(v25_0[v13_0], v19_0, v7_0));
                                }
                            }
                            v13_0++;
                        }
                        v12_0++;
                        v2_18++;
                    }
                    v1_0.close();
                }
            } else {
                try {
                    if ((p26 != null) && ((new java.io.File(p26).exists()) && ((new java.io.File(p26).length() != 0) && (new java.io.File(p26).exists())))) {
                        try {
                            java.nio.channels.FileChannel v1_1 = new java.io.RandomAccessFile(p26, "r").getChannel();
                            java.nio.MappedByteBuffer v10_1 = v1_1.map(java.nio.channels.FileChannel$MapMode.READ_ONLY, 0, ((long) ((int) v1_1.size())));
                            v10_1.position(56);
                            int v22_1 = com.chelpus.Utils.convertFourBytesToInt(v10_1.get(), v10_1.get(), v10_1.get(), v10_1.get());
                            v10_1.position(com.chelpus.Utils.convertFourBytesToInt(v10_1.get(), v10_1.get(), v10_1.get(), v10_1.get()));
                            int v24_1 = v22_1;
                            java.util.ArrayList v0_25 = new int[v24_1];
                            int[] v17_1 = v0_25;
                            int v11_2 = 0;
                        } catch (Exception v9_1) {
                            v9_1.printStackTrace();
                        }
                        while (v11_2 < v24_1) {
                            v17_1[v11_2] = com.chelpus.Utils.convertFourBytesToInt(v10_1.get(), v10_1.get(), v10_1.get(), v10_1.get());
                            v11_2++;
                        }
                        int v12_1 = 0;
                        java.util.ArrayList v0_27 = new int[p27.size()];
                        int[] v23_1 = v0_27;
                        java.util.ArrayList v0_28 = new String[p27.size()];
                        String[] v25_1 = v0_28;
                        int v11_3 = 0;
                        while (v11_3 < v23_1.length) {
                            v23_1[v11_3] = ((String) p27.get(v11_3)).length();
                            v25_1[v11_3] = ((String) p27.get(v11_3));
                            v11_3++;
                        }
                        int v3_19 = v17_1.length;
                        int v2_46 = 0;
                        while (v2_46 < v3_19) {
                            int v14_1 = v17_1[v2_46];
                            int v13_1 = 0;
                            while (v13_1 < v23_1.length) {
                                int v21_1 = com.chelpus.Utils.convertByteToInt(v10_1.get(v14_1));
                                if (v21_1 == v23_1[v13_1]) {
                                    byte[] v8_1 = new byte[v21_1];
                                    v10_1.position((v14_1 + 1));
                                    int v18_1 = 0;
                                    while (v18_1 < v8_1.length) {
                                        v8_1[v18_1] = v10_1.get();
                                        v18_1++;
                                    }
                                    if (v25_1[v13_1].equals(new String(v8_1))) {
                                        java.util.ArrayList v0_35 = new byte[4];
                                        byte[] v19_1 = v0_35;
                                        v19_1[0] = ((byte) v12_1);
                                        v19_1[1] = ((byte) (v12_1 >> 8));
                                        v19_1[2] = ((byte) (v12_1 >> 16));
                                        v19_1[3] = ((byte) (v12_1 >> 24));
                                        int v7_1 = 0;
                                        if (v17_1.length > 65535) {
                                            v7_1 = 1;
                                        }
                                        v20_1.add(new com.android.vending.billing.InAppBillingService.LUCK.StringItem(v25_1[v13_1], v19_1, v7_1));
                                    }
                                }
                                v13_1++;
                            }
                            v12_1++;
                            v2_46++;
                        }
                        v1_1.close();
                    }
                } catch (Exception v9_2) {
                    v9_2.printStackTrace();
                }
            }
        } catch (Exception v9_3) {
            System.out.println(v9_3);
        }
        return v20_1;
    }

    public static String getText(int p1)
    {
        if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.resources == null) {
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.resources = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getRes();
        }
        return com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.resources.getString(p1);
    }

    public static boolean getTypesIds(String p17, java.util.ArrayList p18, boolean p19)
    {
        System.out.println(new StringBuilder().append("scan: ").append(p17).toString());
        int v14 = 0;
        java.util.Iterator v2_2 = new String[3];
        v2_2[0] = "chmod";
        v2_2[1] = "777";
        v2_2[2] = p17;
        com.chelpus.Utils.run_all_no_root(v2_2);
        new java.util.ArrayList();
        try {
            if ((!p19) && ((p17 != null) && ((new java.io.File(p17).exists()) && ((new java.io.File(p17).length() != 0) && (new java.io.File(p17).exists()))))) {
                try {
                    java.nio.channels.FileChannel v1 = new java.io.RandomAccessFile(p17, "r").getChannel();
                    java.nio.MappedByteBuffer v8 = v1.map(java.nio.channels.FileChannel$MapMode.READ_ONLY, 0, ((long) ((int) v1.size())));
                    v8.position(64);
                    int v16 = com.chelpus.Utils.convertFourBytesToInt(v8.get(), v8.get(), v8.get(), v8.get());
                    int v12 = com.chelpus.Utils.convertFourBytesToInt(v8.get(), v8.get(), v8.get(), v8.get());
                    System.out.println(new StringBuilder().append("LuckyPatcher offset_to_data=").append(Integer.toHexString(v12)).toString());
                    v8.position(v12);
                    int v10 = 0;
                    int v9 = 0;
                } catch (Exception v7_0) {
                    v7_0.printStackTrace();
                }
                while (v9 < v16) {
                    byte[] v13 = new byte[4];
                    v13[0] = v8.get();
                    v13[1] = v8.get();
                    v13[2] = v8.get();
                    v13[3] = v8.get();
                    java.util.Iterator v2_26 = p18.iterator();
                    while (v2_26.hasNext()) {
                        com.android.vending.billing.InAppBillingService.LUCK.TypesItem v11_3 = ((com.android.vending.billing.InAppBillingService.LUCK.TypesItem) v2_26.next());
                        if ((v13[0] == v11_3.Type[0]) && ((v13[1] == v11_3.Type[1]) && ((v13[2] == v11_3.Type[2]) && (v13[3] == v11_3.Type[3])))) {
                            v11_3.id_type[0] = ((byte) v10);
                            v11_3.id_type[1] = ((byte) (v10 >> 8));
                            v11_3.found_id_type = 1;
                        }
                    }
                    v10++;
                    v9++;
                }
                java.util.Iterator v2_20 = p18.iterator();
                while (v2_20.hasNext()) {
                    if (((com.android.vending.billing.InAppBillingService.LUCK.TypesItem) v2_20.next()).found_id_type) {
                        v14 = 1;
                    }
                }
                v1.close();
            }
        } catch (Exception v7_2) {
            System.out.println(v7_2);
        } catch (Exception v7_1) {
            v7_1.printStackTrace();
        }
        return v14;
    }

    public static String getXmlAttribute(String p8, String p9, String p10)
    {
        try {
            String v6_3;
            org.w3c.dom.NodeList v5 = javax.xml.parsers.DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new org.xml.sax.InputSource(new java.io.StringReader(p8))).getElementsByTagName(p9);
        } catch (org.w3c.dom.Element v3_2) {
            v3_2.printStackTrace();
            v6_3 = "";
            return v6_3;
        } catch (org.w3c.dom.Element v3_0) {
            v3_0.printStackTrace();
        } catch (org.w3c.dom.Element v3_1) {
            v3_1.printStackTrace();
        }
        if (0 >= v5.getLength()) {
        } else {
            v6_3 = ((org.w3c.dom.Element) v5.item(0)).getAttribute(p10);
            return v6_3;
        }
    }

    public static long getfirstInstallTime(String p6, boolean p7)
    {
        if ((com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.api <= 8) || (p7)) {
            try {
                long v1 = new java.io.File(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getPackageInfo(p6, 0).applicationInfo.sourceDir).lastModified();
            } catch (android.content.pm.PackageManager$NameNotFoundException v0_0) {
                v0_0.printStackTrace();
            }
        } else {
            try {
                v1 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getPackageInfo(p6, 0).lastUpdateTime;
            } catch (android.content.pm.PackageManager$NameNotFoundException v0_1) {
                v0_1.printStackTrace();
            }
        }
        return v1;
    }

    public static boolean hasXposed()
    {
        if ((System.getenv("CLASSPATH") == null) || (!System.getenv("CLASSPATH").contains("Xposed"))) {
            int v0_5 = 0;
        } else {
            v0_5 = 1;
        }
        return v0_5;
    }

    public static boolean initXposedParam()
    {
        int v5 = 0;
        if (new java.io.File("/data/lp/xposed").exists()) {
            if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.startUnderRoot.booleanValue()) {
                new java.io.File("/data/lp/xposed").setWritable(1, 0);
                com.chelpus.Utils.run_all("chmod 777 /data/lp/xposed");
                try {
                    new org.json.JSONObject(com.chelpus.Utils.read_from_file(new java.io.File("/data/lp/xposed")));
                } catch (org.json.JSONException v0) {
                    if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.startUnderRoot.booleanValue()) {
                        com.chelpus.Utils.run_all("chmod 777 /data/lp/xposed");
                    } else {
                        String v7_47 = new String[3];
                        v7_47[0] = "chmod";
                        v7_47[1] = "777";
                        v7_47[2] = "/data/lp/xposed";
                        com.chelpus.Utils.run_all_no_root(v7_47);
                    }
                }
                try {
                    org.json.JSONObject v3_3 = new org.json.JSONObject(com.chelpus.Utils.read_from_file(new java.io.File("/data/lp/xposed")));
                } catch (org.json.JSONException v0_1) {
                    v0_1.printStackTrace();
                    org.json.JSONObject v3_5 = new org.json.JSONObject();
                    try {
                        v3_5.put("patch1", 1);
                        v3_5.put("patch2", 1);
                        v3_5.put("patch3", 1);
                        v3_5.put("patch4", 1);
                        v3_5.put("hide", 0);
                        v3_5.put("module_on", 1);
                    } catch (org.json.JSONException v1_0) {
                        v1_0.printStackTrace();
                    }
                    if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.startUnderRoot.booleanValue()) {
                        com.chelpus.Utils.save_text_to_file(new java.io.File("/data/lp/xposed"), v3_5.toString());
                        com.chelpus.Utils.run_all("chmod 777 /data/lp/xposed");
                    } else {
                        com.chelpus.Utils.save_text_to_file(new java.io.File("/data/lp/xposed"), v3_5.toString());
                        String v7_64 = new String[3];
                        v7_64[0] = "chmod";
                        v7_64[1] = "777";
                        v7_64[2] = "/data/lp/xposed";
                        com.chelpus.Utils.run_all_no_root(v7_64);
                    }
                    try {
                        v3_3 = new org.json.JSONObject(com.chelpus.Utils.read_from_file(new java.io.File("/data/lp/xposed")));
                        try {
                            v3_3.getBoolean("module_on");
                            v5 = 1;
                        } catch (org.json.JSONException v0_2) {
                            v0_2.printStackTrace();
                        }
                    } catch (org.json.JSONException v1_1) {
                        v1_1.printStackTrace();
                    }
                }
            } else {
                new java.io.File("/data/lp/xposed").setWritable(1, 0);
                String v7_10 = new String[3];
                v7_10[0] = "chmod";
                v7_10[1] = "777";
                v7_10[2] = "/data/lp/xposed";
                com.chelpus.Utils.run_all_no_root(v7_10);
            }
        } else {
            if (!new java.io.File("/data/lp").exists()) {
                if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.startUnderRoot.booleanValue()) {
                    com.chelpus.Utils.run_all("mkdir /data/lp");
                    com.chelpus.Utils.run_all("chmod 777 /data/lp");
                } else {
                    new java.io.File("/data/lp").mkdirs();
                    new java.io.File("/data/lp").setWritable(1, 0);
                    String v7_22 = new String[3];
                    v7_22[0] = "chmod";
                    v7_22[1] = "777";
                    v7_22[2] = "/data/lp";
                    com.chelpus.Utils.run_all_no_root(v7_22);
                }
            }
            org.json.JSONObject v3_1 = new org.json.JSONObject();
            try {
                v3_1.put("patch1", 1);
                v3_1.put("patch2", 1);
                v3_1.put("patch3", 1);
                v3_1.put("patch4", 1);
                v3_1.put("hide", 0);
                v3_1.put("module_on", 1);
            } catch (org.json.JSONException v0_0) {
                v0_0.printStackTrace();
            }
            if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.startUnderRoot.booleanValue()) {
                com.chelpus.Utils.save_text_to_file(new java.io.File("/data/lp/xposed"), v3_1.toString());
                new java.io.File("/data/lp/xposed").setWritable(1, 0);
                com.chelpus.Utils.run_all("chmod 777 /data/lp/xposed");
            } else {
                com.chelpus.Utils.save_text_to_file(new java.io.File("/data/lp/xposed"), v3_1.toString());
                new java.io.File("/data/lp/xposed").setWritable(1, 0);
                String v7_40 = new String[3];
                v7_40[0] = "chmod";
                v7_40[1] = "777";
                v7_40[2] = "/data/lp/xposed";
                com.chelpus.Utils.run_all_no_root(v7_40);
            }
        }
        return v5;
    }

    public static final boolean isAds(String p1)
    {
        if ((!p1.contains(".ads.")) && ((!p1.contains("adwhirl")) && ((!p1.contains("amobee")) && ((!p1.contains("burstly")) && ((!p1.contains("com.adknowledge.")) && ((!p1.contains("cauly.android.ad.")) && ((!p1.contains(".greystripe.")) && ((!p1.contains("inmobi.")) && ((!p1.contains("inneractive.api.ads.")) && ((!p1.contains(".jumptap.adtag.")) && ((!p1.contains(".mdotm.android.ads.")) && ((!p1.contains("medialets.advertising.")) && ((!p1.contains(".millennialmedia.android.")) && ((!p1.contains(".mobclix.android.sdk.")) && ((!p1.contains(".mobfox.sdk.")) && ((!p1.contains(".adserver.adview.")) && ((!p1.contains(".mopub.mobileads.")) && ((!p1.contains("com.oneriot.")) && ((!p1.contains(".papaya.offer.")) && ((!p1.contains("pontiflex.mobile.webview.sdk.activities")) && ((!p1.contains(".qwapi.adclient.android.view.")) && ((!p1.contains(".smaato.SOMA.")) && ((!p1.contains(".vdopia.client.android.")) && ((!p1.contains(".zestadz.android.")) && ((!p1.contains("com.appenda.")) && ((!p1.contains("com.airpush.android.")) && ((!p1.contains("com.Leadbolt.")) && ((!p1.contains("com.moolah.")) && ((!p1.contains("com.tapit.adview.notif.")) && ((!p1.contains("com.urbanairship.push.")) && ((!p1.contains("com.xtify.android.sdk.")) && ((!p1.contains("MediaPlayerWrapper")) && ((!p1.contains(".vungle.")) && ((!p1.contains(".tapjoy.")) && ((!p1.contains(".nbpcorp.")) && ((!p1.contains("com.appenda.")) && ((!p1.contains(".plus1.sdk.")) && ((!p1.contains(".adsdk.")) && ((!p1.contains(".mdotm.")) && ((!p1.contains("AdView")) && (!p1.contains("mad.ad.")))))))))))))))))))))))))))))))))))))))))) {
            int v0_82 = 0;
        } else {
            v0_82 = 1;
        }
        return v0_82;
    }

    public static boolean isCustomPatchesForPkg(String p8)
    {
        try {
            if ((com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.customlist != null) && (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.customlist.length != 0)) {
                if ((com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.customlist == null) || (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.customlist.length <= 0)) {
                    int v3_35 = 0;
                    return v3_35;
                } else {
                    int v2_1 = 0;
                    while (v2_1 < com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.customlist.length) {
                        if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.customlist[v2_1].getName().contains("_%ALL%.txt")) {
                            if (!p8.contains(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.customlist[v2_1].getName().replace("_%ALL%.txt", ""))) {
                                if ((com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.customlist[v2_1].getName().contains("_%ALL%.txt")) && ((com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.customlist[v2_1].getName().contains("%ALL%_")) && (p8.contains(((String[]) com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.customlist[v2_1].getName().split("%ALL%_"))[1].replace("_%ALL%.txt", ""))))) {
                                    v3_35 = 1;
                                    return v3_35;
                                }
                            } else {
                                v3_35 = 1;
                                return v3_35;
                            }
                        }
                        if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.customlist[v2_1].getName().replace(".txt", "").endsWith(p8)) {
                            v2_1++;
                        } else {
                            v3_35 = 1;
                            return v3_35;
                        }
                    }
                }
            } else {
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.init();
                try {
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.basepath = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().getString("basepath", com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.basepath);
                    int v3_9 = new java.io.File[new java.io.File(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.basepath).listFiles().length];
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.customlist = v3_9;
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.customlist = new java.io.File(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.basepath).listFiles();
                    java.util.ArrayList v0_1 = new java.util.ArrayList();
                    v0_1.clear();
                    int v2_0 = 0;
                } catch (Exception v1) {
                    System.out.println("Not found dir by Lucky Patcher. Custom patch not found.");
                }
                while (v2_0 < com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.customlist.length) {
                    if ((com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.customlist[v2_0].isFile()) && (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.customlist[v2_0].getName().endsWith(".txt"))) {
                        v0_1.add(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.customlist[v2_0]);
                    }
                    v2_0++;
                }
                if (v0_1.size() <= 0) {
                } else {
                    int v3_17 = new java.io.File[v0_1.size()];
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.customlist = v3_17;
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.customlist = ((java.io.File[]) ((java.io.File[]) v0_1.toArray(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.customlist)));
                }
            }
        } catch (Exception v1) {
            v1.printStackTrace();
        }
        v3_35 = 0;
        return v3_35;
    }

    public static boolean isELFfiles(java.io.File p9)
    {
        try {
            int v1_9;
            java.nio.channels.FileChannel v0 = new java.io.RandomAccessFile(p9, "r").getChannel();
            java.nio.MappedByteBuffer v7 = v0.map(java.nio.channels.FileChannel$MapMode.READ_ONLY, 0, ((long) ((int) v0.size())));
            v7.position(0);
        } catch (java.io.IOException v6_1) {
            v6_1.printStackTrace();
            System.out.println("Check file: is not ELF.");
            v1_9 = 0;
            return v1_9;
        } catch (java.io.IOException v6_0) {
            v6_0.printStackTrace();
        }
        if ((v7.get() != 127) || ((v7.get() != 69) || ((v7.get() != 76) || (v7.get() != 70)))) {
            v0.close();
        } else {
            v0.close();
            System.out.println("Check file: is ELF.");
            v1_9 = 1;
            return v1_9;
        }
    }

    public static boolean isInstalledOnSdCard(String p9)
    {
        int v4 = 1;
        if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.api <= 7) {
            try {
                String v1 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getPackageInfo(p9, 0).applicationInfo.sourceDir;
            } catch (int v4) {
                v4 = 0;
            }
            if (!v1.startsWith("/data/")) {
                if ((!v1.contains("/mnt/")) && (!v1.contains("/sdcard/"))) {
                }
            } else {
                v4 = 0;
            }
        } else {
            try {
                if ((com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getPackageInfo(p9, 0).applicationInfo.flags & 262144) != 262144) {
                    v4 = 0;
                }
            } catch (boolean v6) {
            }
        }
        return v4;
    }

    public static boolean isMarketIntent(String p2)
    {
        if ((!p2.toLowerCase().equals("com.android.vending.billing.inappbillingservice.bind")) && ((!p2.toLowerCase().equals("ir.cafebazaar.pardakht.inappbillingservice.bind")) && ((!p2.toLowerCase().equals("com.nokia.payment.iapenabler.inappbillingservice.bind")) && ((!p2.toLowerCase().equals("net.jhoobin.jhub.inappbillingservice.bind")) && ((!p2.toLowerCase().equals("net.jhoobin.jhub.billing.iinappbillingservice")) && (!p2.toLowerCase().equals(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPackage().getName().toLowerCase()))))))) {
            int v0_12 = 0;
        } else {
            v0_12 = 1;
        }
        return v0_12;
    }

    public static boolean isModified(String p9, android.content.Context p10)
    {
        int v5 = 1;
        try {
            byte[] v3 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getPackageInfo(p9, 64).signatures[0].toByteArray();
        } catch (android.content.pm.PackageManager$NameNotFoundException v1) {
            v1.printStackTrace();
        }
        if ((com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().getBoolean(p9, 0)) || (com.google.android.finsky.billing.iab.google.util.Base64.encode(v3).replaceAll("\n", "").equals("MIIEqDCCA5CgAwIBAgIJAJNurL4H8gHfMA0GCSqGSIb3DQEBBQUAMIGUMQswCQYDVQQGEwJVUzETMBEGA1UECBMKQ2FsaWZvcm5pYTEWMBQGA1UEBxMNTW91bnRhaW4gVmlldzEQMA4GA1UEChMHQW5kcm9pZDEQMA4GA1UECxMHQW5kcm9pZDEQMA4GA1UEAxMHQW5kcm9pZDEiMCAGCSqGSIb3DQEJARYTYW5kcm9pZEBhbmRyb2lkLmNvbTAeFw0wODAyMjkwMTMzNDZaFw0zNTA3MTcwMTMzNDZaMIGUMQswCQYDVQQGEwJVUzETMBEGA1UECBMKQ2FsaWZvcm5pYTEWMBQGA1UEBxMNTW91bnRhaW4gVmlldzEQMA4GA1UEChMHQW5kcm9pZDEQMA4GA1UECxMHQW5kcm9pZDEQMA4GA1UEAxMHQW5kcm9pZDEiMCAGCSqGSIb3DQEJARYTYW5kcm9pZEBhbmRyb2lkLmNvbTCCASAwDQYJKoZIhvcNAQEBBQADggENADCCAQgCggEBANaTGQTexgskse3HYuDZ2CU+Ps1s6x3i/waMqOi8qM1r03hupwqnbOYOuw+ZNVn/2T53qUPn6D1LZLjk/qLT5lbx4meoG7+yMLV4wgRDvkxyGLhG9SEVhvA4oU6Jwr44f46+z4/Kw9oe4zDJ6pPQp8PcSvNQIg1QCAcy4ICXF+5qBTNZ5qaU7Cyz8oSgpGbIepTYOzEJOmc3Li9kEsBubULxWBjf/gOBzAzURNps3cO4JFgZSAGzJWQTT7/emMkod0jb9WdqVA2BVMi7yge54kdVMxHEa5r3b97szI5p58ii0I54JiCUP5lyfTwE/nKZHZnfm644oLIXf6MdW2r+6R8CAQOjgfwwgfkwHQYDVR0OBBYEFEhZAFY9JyxGrhGGBaR0GawJyowRMIHJBgNVHSMEgcEwgb6AFEhZAFY9JyxGrhGGBaR0GawJyowRoYGapIGXMIGUMQswCQYDVQQGEwJVUzETMBEGA1UECBMKQ2FsaWZvcm5pYTEWMBQGA1UEBxMNTW91bnRhaW4gVmlldzEQMA4GA1UEChMHQW5kcm9pZDEQMA4GA1UECxMHQW5kcm9pZDEQMA4GA1UEAxMHQW5kcm9pZDEiMCAGCSqGSIb3DQEJARYTYW5kcm9pZEBhbmRyb2lkLmNvbYIJAJNurL4H8gHfMAwGA1UdEwQFMAMBAf8wDQYJKoZIhvcNAQEFBQADggEBAHqvlozrUMRBBVEY0NqrrwFbinZaJ6cVosK0TyIUFf/azgMJWr+kLfcHCHJsIGnlw27drgQAvilFLAhLwn62oX6snb4YLCBOsVMR9FXYJLZW2+TcIkCRLXWG/oiVHQGo/rWuWkJgU134NDEFJCJGjDbiLCpe+ZTWHdcwauTJ9pUbo8EvHRkU3cYfGmLaLfgn9gP+pWA7LFQNvXwBnDa6sppCccEX31I828XzgXpJ4O+mDL1/dBd+ek8ZPUP0IgdyZm5MTYPhvVqGCHzzTy3sIeJFymwrsBbmg2OAUNLEMO6nwmocSdN2ClirfxqCzJOLSDE4QyS9BAH6EhY6UFcOaE0="))) {
            return v5;
        } else {
            v5 = 0;
            return v5;
        }
    }

    public static final boolean isNetworkAvailable()
    {
        int v2_2;
        android.net.NetworkInfo v0 = ((android.net.ConnectivityManager) com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance().getSystemService("connectivity")).getActiveNetworkInfo();
        if ((v0 == null) || (!v0.isConnected())) {
            v2_2 = 0;
        } else {
            v2_2 = 1;
        }
        return v2_2;
    }

    public static boolean isOdex(String p8)
    {
        int v3 = 1;
        try {
            java.io.File v0_1 = new java.io.File(p8);
            String v2 = com.chelpus.Utils.changeExtension(v0_1.getName(), "odex");
        } catch (Exception v1) {
            v1.printStackTrace();
            v3 = 0;
            return v3;
        }
        if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.api < 23) {
            if ((!new java.io.File(com.chelpus.Utils.changeExtension(p8, "odex")).exists()) && ((!new java.io.File(new StringBuilder().append(com.chelpus.Utils.getDirs(v0_1)).append("/arm/").append(com.chelpus.Utils.changeExtension(v0_1.getName(), "odex")).toString()).exists()) && ((!new java.io.File(new StringBuilder().append(com.chelpus.Utils.getDirs(v0_1)).append("/arm64/").append(com.chelpus.Utils.changeExtension(v0_1.getName(), "odex")).toString()).exists()) && (!new java.io.File(new StringBuilder().append(com.chelpus.Utils.getDirs(v0_1)).append("/x86/").append(com.chelpus.Utils.changeExtension(v0_1.getName(), "odex")).toString()).exists())))) {
            } else {
                return v3;
            }
        } else {
            if ((new java.io.File(new StringBuilder().append(com.chelpus.Utils.getDirs(v0_1)).append("/oat/arm/").append(v2).toString()).exists()) || ((new java.io.File(new StringBuilder().append(com.chelpus.Utils.getDirs(v0_1)).append("/oat/arm64/").append(v2).toString()).exists()) || (new java.io.File(new StringBuilder().append(com.chelpus.Utils.getDirs(v0_1)).append("/oat/x86/").append(v2).toString()).exists()))) {
                return v3;
            } else {
            }
        }
    }

    public static boolean isRebuildedOrOdex(String p9, android.content.Context p10)
    {
        int v5 = 1;
        try {
            android.content.pm.PackageInfo v2 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getPackageInfo(p9, 64);
            byte[] v3 = v2.signatures[0].toByteArray();
        } catch (android.content.pm.PackageManager$NameNotFoundException v1) {
            v1.printStackTrace();
        }
        if ((com.chelpus.Utils.isOdex(v2.applicationInfo.sourceDir)) || ((com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().getBoolean(p9, 0)) || (com.google.android.finsky.billing.iab.google.util.Base64.encode(v3).replaceAll("\n", "").equals("MIIEqDCCA5CgAwIBAgIJAJNurL4H8gHfMA0GCSqGSIb3DQEBBQUAMIGUMQswCQYDVQQGEwJVUzETMBEGA1UECBMKQ2FsaWZvcm5pYTEWMBQGA1UEBxMNTW91bnRhaW4gVmlldzEQMA4GA1UEChMHQW5kcm9pZDEQMA4GA1UECxMHQW5kcm9pZDEQMA4GA1UEAxMHQW5kcm9pZDEiMCAGCSqGSIb3DQEJARYTYW5kcm9pZEBhbmRyb2lkLmNvbTAeFw0wODAyMjkwMTMzNDZaFw0zNTA3MTcwMTMzNDZaMIGUMQswCQYDVQQGEwJVUzETMBEGA1UECBMKQ2FsaWZvcm5pYTEWMBQGA1UEBxMNTW91bnRhaW4gVmlldzEQMA4GA1UEChMHQW5kcm9pZDEQMA4GA1UECxMHQW5kcm9pZDEQMA4GA1UEAxMHQW5kcm9pZDEiMCAGCSqGSIb3DQEJARYTYW5kcm9pZEBhbmRyb2lkLmNvbTCCASAwDQYJKoZIhvcNAQEBBQADggENADCCAQgCggEBANaTGQTexgskse3HYuDZ2CU+Ps1s6x3i/waMqOi8qM1r03hupwqnbOYOuw+ZNVn/2T53qUPn6D1LZLjk/qLT5lbx4meoG7+yMLV4wgRDvkxyGLhG9SEVhvA4oU6Jwr44f46+z4/Kw9oe4zDJ6pPQp8PcSvNQIg1QCAcy4ICXF+5qBTNZ5qaU7Cyz8oSgpGbIepTYOzEJOmc3Li9kEsBubULxWBjf/gOBzAzURNps3cO4JFgZSAGzJWQTT7/emMkod0jb9WdqVA2BVMi7yge54kdVMxHEa5r3b97szI5p58ii0I54JiCUP5lyfTwE/nKZHZnfm644oLIXf6MdW2r+6R8CAQOjgfwwgfkwHQYDVR0OBBYEFEhZAFY9JyxGrhGGBaR0GawJyowRMIHJBgNVHSMEgcEwgb6AFEhZAFY9JyxGrhGGBaR0GawJyowRoYGapIGXMIGUMQswCQYDVQQGEwJVUzETMBEGA1UECBMKQ2FsaWZvcm5pYTEWMBQGA1UEBxMNTW91bnRhaW4gVmlldzEQMA4GA1UEChMHQW5kcm9pZDEQMA4GA1UECxMHQW5kcm9pZDEQMA4GA1UEAxMHQW5kcm9pZDEiMCAGCSqGSIb3DQEJARYTYW5kcm9pZEBhbmRyb2lkLmNvbYIJAJNurL4H8gHfMAwGA1UdEwQFMAMBAf8wDQYJKoZIhvcNAQEFBQADggEBAHqvlozrUMRBBVEY0NqrrwFbinZaJ6cVosK0TyIUFf/azgMJWr+kLfcHCHJsIGnlw27drgQAvilFLAhLwn62oX6snb4YLCBOsVMR9FXYJLZW2+TcIkCRLXWG/oiVHQGo/rWuWkJgU134NDEFJCJGjDbiLCpe+ZTWHdcwauTJ9pUbo8EvHRkU3cYfGmLaLfgn9gP+pWA7LFQNvXwBnDa6sppCccEX31I828XzgXpJ4O+mDL1/dBd+ek8ZPUP0IgdyZm5MTYPhvVqGCHzzTy3sIeJFymwrsBbmg2OAUNLEMO6nwmocSdN2ClirfxqCzJOLSDE4QyS9BAH6EhY6UFcOaE0=")))) {
            return v5;
        } else {
            v5 = 0;
            return v5;
        }
    }

    public static boolean isServiceRunning(String p5)
    {
        int v3_2 = ((android.app.ActivityManager) com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance().getSystemService("activity")).getRunningServices(2147483647).iterator();
        while (v3_2.hasNext()) {
            if (((android.app.ActivityManager$RunningServiceInfo) v3_2.next()).service.getClassName().equals(p5)) {
                int v3_3 = 1;
            }
            return v3_3;
        }
        v3_3 = 0;
        return v3_3;
    }

    public static boolean isWithFramework()
    {
        int v1_0 = 0;
        if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.api < 23) {
            String v3_2 = new com.chelpus.Utils("");
            String v4_2 = new String[1];
            v4_2[0] = new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.dalvikruncommandWithFramework).append(".checkWithFramework 123").toString();
            String v0 = v3_2.cmdRoot(v4_2);
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.startUnderRoot = Boolean.valueOf(0);
            if ((!v0.contains("withoutFramework")) && (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.errorOutput.contains("java.lang.ClassNotFoundException:"))) {
                System.out.println("WithFramework support");
                v1_0 = 1;
            }
        }
        return v1_0;
    }

    public static boolean isXposedEnabled()
    {
        return 0;
    }

    public static long javaToDosTime(long p4)
    {
        long v2_9;
        java.util.Date v0_1 = new java.util.Date(p4);
        int v1 = (v0_1.getYear() + 1900);
        if (v1 >= 1980) {
            v2_9 = ((long) (((((((v1 + -1980) << 25) | ((v0_1.getMonth() + 1) << 21)) | (v0_1.getDate() << 16)) | (v0_1.getHours() << 11)) | (v0_1.getMinutes() << 5)) | (v0_1.getSeconds() >> 1)));
        } else {
            v2_9 = 2162688;
        }
        return v2_9;
    }

    public static void kill(String p3)
    {
        if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.startUnderRoot.booleanValue()) {
            String[] v0_3 = new String[2];
            v0_3[0] = "killall";
            v0_3[1] = p3;
            com.chelpus.Utils.run_all_no_root(v0_3);
            com.chelpus.Utils.killAll(p3);
        } else {
            com.chelpus.Utils.run_all(new StringBuilder().append("killall ").append(p3).toString());
            com.chelpus.Utils.killAll(p3);
        }
        return;
    }

    public static boolean killAll(String p15)
    {
        int v9_31;
        if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.startUnderRoot.booleanValue()) {
            java.util.regex.Pattern v6_0 = java.util.regex.Pattern.compile(new StringBuilder().append("^\\S+\\s+([0-9]+).*").append(java.util.regex.Pattern.quote(p15)).append("$").toString());
            int v9_8 = new String[2];
            v9_8[0] = "ps";
            v9_8[1] = p15;
            String v8_0 = com.chelpus.Utils.cmdParam(v9_8);
            if (v8_0.startsWith("~")) {
                if (v8_0.startsWith("~")) {
                    int v9_14 = new String[3];
                    v9_14[0] = new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.toolfilesdir).append("/busybox").toString();
                    v9_14[1] = "ps";
                    v9_14[2] = p15;
                    v8_0 = com.chelpus.Utils.cmdParam(v9_14);
                }
                if (v8_0.startsWith("~")) {
                    int v9_18 = new String[3];
                    v9_18[0] = "busybox";
                    v9_18[1] = "ps";
                    v9_18[2] = p15;
                    v8_0 = com.chelpus.Utils.cmdParam(v9_18);
                }
                if (v8_0.startsWith("~")) {
                    int v9_22 = new String[3];
                    v9_22[0] = "toolbox";
                    v9_22[1] = "ps";
                    v9_22[2] = p15;
                    v8_0 = com.chelpus.Utils.cmdParam(v9_22);
                }
            }
            if ((!v8_0.equals("~")) && (!v8_0.equals(""))) {
                String[] v4_0 = v8_0.split("\n");
                java.util.ArrayList v3_1 = new java.util.ArrayList();
                if (v4_0.length > 1) {
                    String[] v10_15 = v4_0.length;
                    int v9_29 = 0;
                    while (v9_29 < v10_15) {
                        String v1_0 = v4_0[v9_29];
                        if (v1_0.contains(p15)) {
                            java.util.regex.Matcher v5_0 = v6_0.matcher(new StringBuilder().append(v1_0).append("\n").toString());
                            try {
                                if (v5_0.find()) {
                                    String v2_2 = v5_0.group(1);
                                    v3_1.add(v2_2);
                                    System.out.println(new StringBuilder().append("Found pid: ").append(v2_2).append(" for ").append(p15).toString());
                                }
                            } catch (Exception v0_0) {
                                System.out.println(new StringBuilder().append("Error with regex! ").append(v0_0).toString());
                            }
                        }
                        v9_29++;
                    }
                    if (v3_1.size() > 0) {
                        int v9_32 = v3_1.iterator();
                        while (v9_32.hasNext()) {
                            String v2_1 = ((String) v9_32.next());
                            System.out.println(new StringBuilder().append("Kill: ").append(v2_1).append(" for ").append(p15).toString());
                            String[] v10_19 = new String[3];
                            v10_19[0] = "kill";
                            v10_19[1] = "-9";
                            v10_19[2] = v2_1;
                            com.chelpus.Utils.cmdParam(v10_19);
                        }
                        v9_31 = 1;
                        return v9_31;
                    }
                }
            }
            v9_31 = 0;
        } else {
            java.util.regex.Pattern v6_1 = java.util.regex.Pattern.compile(new StringBuilder().append("^\\S+\\s+([0-9]+).*").append(java.util.regex.Pattern.quote(p15)).append("$").toString());
            int v9_39 = new com.chelpus.Utils("");
            String[] v10_25 = new String[1];
            v10_25[0] = new StringBuilder().append("ps ").append(p15).toString();
            String v8_1 = v9_39.cmdRoot(v10_25);
            if (v8_1.startsWith("~")) {
                com.chelpus.Utils.exitRoot();
                try {
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getSu();
                } catch (Exception v0_1) {
                    v0_1.printStackTrace();
                }
                int v9_43 = new com.chelpus.Utils("");
                String[] v10_28 = new String[1];
                v10_28[0] = new StringBuilder().append("ps ").append(p15).toString();
                v8_1 = v9_43.cmdRoot(v10_28);
                if (v8_1.startsWith("~")) {
                    int v9_47 = new com.chelpus.Utils("");
                    String[] v10_31 = new String[1];
                    v10_31[0] = new StringBuilder().append("busybox ps ").append(p15).toString();
                    v8_1 = v9_47.cmdRoot(v10_31);
                }
                if (v8_1.startsWith("~")) {
                    int v9_51 = new com.chelpus.Utils("");
                    String[] v10_34 = new String[1];
                    v10_34[0] = new StringBuilder().append("toolbox ps ").append(p15).toString();
                    v8_1 = v9_51.cmdRoot(v10_34);
                }
            }
            if ((!v8_1.equals("~")) && (!v8_1.equals(""))) {
                String[] v4_1 = v8_1.split("\n");
                java.util.ArrayList v3_3 = new java.util.ArrayList();
                if (v4_1.length > 1) {
                    String[] v10_36 = v4_1.length;
                    int v9_58 = 0;
                    while (v9_58 < v10_36) {
                        String v1_2 = v4_1[v9_58];
                        if (v1_2.contains(p15)) {
                            java.util.regex.Matcher v5_1 = v6_1.matcher(new StringBuilder().append(v1_2).append("\n").toString());
                            try {
                                if (v5_1.find()) {
                                    String v2_5 = v5_1.group(1);
                                    v3_3.add(v2_5);
                                    System.out.println(new StringBuilder().append("Found pid: ").append(v2_5).append(" for ").append(p15).toString());
                                }
                            } catch (Exception v0_2) {
                                System.out.println(new StringBuilder().append("Error with regex! ").append(v0_2).toString());
                            }
                        }
                        v9_58++;
                    }
                    if (v3_3.size() > 0) {
                        int v9_60 = v3_3.iterator();
                        while (v9_60.hasNext()) {
                            String v2_4 = ((String) v9_60.next());
                            System.out.println(new StringBuilder().append("Kill: ").append(v2_4).append(" for ").append(p15).toString());
                            String[] v10_40 = new com.chelpus.Utils("");
                            int v11_43 = new String[1];
                            v11_43[0] = new StringBuilder().append("kill -9 ").append(v2_4).toString();
                            v10_40.cmdRoot(v11_43);
                        }
                        v9_31 = 1;
                        return v9_31;
                    }
                }
            }
            v9_31 = 0;
        }
        return v9_31;
    }

    public static void market_billing_services(boolean p2)
    {
        if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.su) {
            new Thread(new com.chelpus.Utils$13(p2)).start();
        }
        return;
    }

    public static void market_billing_services_to_main_stream(boolean p12)
    {
        if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.su) {
            new java.util.ArrayList();
            try {
                android.content.pm.PackageInfo v3 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getPackageInfo("com.android.vending", 4);
            } catch (android.content.pm.PackageManager$NameNotFoundException v2) {
                v2.printStackTrace();
            }
            if ((v3 != null) && ((v3.services != null) && (v3.services.length != 0))) {
                int v0 = 0;
                while (v0 < v3.services.length) {
                    try {
                        if (p12) {
                            if (((v3.services[v0].name.endsWith("InAppBillingService")) || (v3.services[v0].name.endsWith("MarketBillingService"))) && (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getComponentEnabledSetting(new android.content.ComponentName("com.android.vending", v3.services[v0].name)) != 1)) {
                                com.chelpus.Utils v5_18 = new com.chelpus.Utils("");
                                String[] v6_7 = new String[2];
                                v6_7[0] = new StringBuilder().append("pm enable \'com.android.vending/").append(v3.services[v0].name).append("\'").toString();
                                v6_7[1] = "skipOut";
                                v5_18.cmdRoot(v6_7);
                            }
                        } else {
                            if (((v3.services[v0].name.endsWith("InAppBillingService")) || (v3.services[v0].name.endsWith("MarketBillingService"))) && (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getComponentEnabledSetting(new android.content.ComponentName("com.android.vending", v3.services[v0].name)) != 2)) {
                                com.chelpus.Utils v5_30 = new com.chelpus.Utils("");
                                String[] v6_14 = new String[2];
                                v6_14[0] = new StringBuilder().append("pm disable \'com.android.vending/").append(v3.services[v0].name).append("\'").toString();
                                v6_14[1] = "skipOut";
                                v5_30.cmdRoot(v6_14);
                            }
                        }
                        v0++;
                    } catch (Exception v1) {
                        v1.printStackTrace();
                    }
                }
            }
        }
        return;
    }

    public static void market_licensing_services(boolean p2)
    {
        if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.su) {
            new Thread(new com.chelpus.Utils$14(p2)).start();
        }
        return;
    }

    public static void market_licensing_services_to_main_stream(boolean p11)
    {
        if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.su) {
            new java.util.ArrayList();
            android.content.pm.PackageInfo v2 = com.chelpus.Utils.getPkgInfo("com.android.vending", 4);
            if ((v2 != null) && ((v2.services != null) && (v2.services.length != 0))) {
                int v0 = 0;
                while (v0 < v2.services.length) {
                    try {
                        if (p11) {
                            if ((v2.services[v0].name.endsWith("LicensingService")) && (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getComponentEnabledSetting(new android.content.ComponentName("com.android.vending", v2.services[v0].name)) != 1)) {
                                com.chelpus.Utils v4_14 = new com.chelpus.Utils("");
                                String[] v5_6 = new String[2];
                                v5_6[0] = new StringBuilder().append("pm enable \'com.android.vending/").append(v2.services[v0].name).append("\'").toString();
                                v5_6[1] = "skipOut";
                                v4_14.cmdRoot(v5_6);
                            }
                        } else {
                            if ((v2.services[v0].name.endsWith("LicensingService")) && (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getComponentEnabledSetting(new android.content.ComponentName("com.android.vending", v2.services[v0].name)) != 2)) {
                                com.chelpus.Utils v4_22 = new com.chelpus.Utils("");
                                String[] v5_12 = new String[2];
                                v5_12[0] = new StringBuilder().append("pm disable \'com.android.vending/").append(v2.services[v0].name).append("\'").toString();
                                v5_12[1] = "skipOut";
                                v4_22.cmdRoot(v5_12);
                            }
                        }
                        v0++;
                    } catch (Exception v1) {
                        v1.printStackTrace();
                    }
                }
            }
        }
        return;
    }

    public static boolean onMainThread()
    {
        if ((android.os.Looper.myLooper() == null) || (android.os.Looper.myLooper() != android.os.Looper.getMainLooper())) {
            int v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    public static boolean pattern_checker(android.app.Activity p9)
    {
        int v5 = 0;
        System.out.println(p9.getApplicationInfo().sourceDir);
        try {
            byte[] v3 = p9.getPackageManager().getPackageInfo(p9.getPackageName(), 64).signatures[0].toByteArray();
            System.out.println();
        } catch (android.content.pm.PackageManager$NameNotFoundException v2) {
            v2.printStackTrace();
        }
        if (com.google.android.finsky.billing.iab.google.util.Base64.encode(v3).replaceAll("\n", "").equals("MIIDDTCCAfWgAwIBAgIEeR8eUDANBgkqhkiG9w0BAQsFADA3MQswCQYDVQQGEwJVUzEQMA4GA1UEChMHQW5kcm9pZDEWMBQGA1UEAxMNQW5kcm9pZCBEZWJ1ZzAeFw0xMTEyMDgwNjA1MTBaFw00MTExMzAwNjA1MTBaMDcxCzAJBgNVBAYTAlVTMRAwDgYDVQQKEwdBbmRyb2lkMRYwFAYDVQQDEw1BbmRyb2lkIERlYnVnMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAhENqFp22Dq9M/CEU4on3/xGfoWggUk4tahTdC/okxdVO/nB27odddvB+zDiMSq+mGFprWxk31pzu+W31pbdq4tnBF6aqzhLanAjxVoeuqNUDzXfqNhxjQDJjZ9Q9zntEHNllIkfJclYyADf1GHjQs9vpgo58EXQ4Wt8REG9P+8My5ENmVkfTA3L7yryyTnplRn7d+jVtIcJEKY0s/kCFfRMNJnM2vYYWGpXrmEJFMNPtjvPGvnNgMojHLgWqY7z7foplBjGfEItX/huYZqp7+ZaGWyrksXHStEUXUa7TJJiW++R4e4VL6jIDwTHGOAgYaVA/ZarfLquQhXP28vBNhwIDAQABoyEwHzAdBgNVHQ4EFgQUNrZ//EPQx9WdAor2L5dvsy6i9eYwDQYJKoZIhvcNAQELBQADggEBAH09ZytGQmSrbGNjbCMnuZ+UuKOP+nN5j0U0hbMisC+2rcox36S23hVDPEc7rcBMo/Aep4kY/CZCO9UnRVP5NG3YugQU2mwimM2po4pZZbOBCDx4dEjA4ymJpKlS4fEPQ1qp5p9um8wmMVg5Yl5y9dGpxNF/USDW5jq+H8SBhfcrro+m4V+G/jPGWSN/0QwJpb0dmsD2MLgw7/HyJPnymvSEzom6e7Oe4aJDzOKuRM5hrfvsNyH+WTq+f+IElEVMg1zwo0JHhFTppxEFROPHTYO2FjMdrA26KdPcLTS07pzpP00/0n+4R7SPoAHzMBptlvNZws9KvaQEiOc0ObXhjL0=")) {
            v5 = 1;
        }
        return v5;
    }

    public static String readLine(java.io.InputStream p5)
    {
        java.io.ByteArrayOutputStream v1_1 = new java.io.ByteArrayOutputStream();
        while(true) {
            String v2_2;
            int v0 = p5.read();
            if (v0 >= 0) {
                if (v0 == 10) {
                    break;
                }
                v1_1.write(v0);
            } else {
                v2_2 = 0;
            }
            return v2_2;
        }
        v2_2 = new String(v1_1.toByteArray(), "UTF-8");
        return v2_2;
    }

    public static org.json.JSONObject readXposedParamBoolean()
    {
        return new org.json.JSONObject(com.chelpus.Utils.read_from_file(new java.io.File("/data/lp/xposed")));
    }

    public static String read_from_file(java.io.File p6)
    {
        byte[] v3 = new byte[((int) p6.length())];
        try {
            java.io.RandomAccessFile v2_1 = new java.io.RandomAccessFile(p6, "r");
            try {
                v2_1.seek(0);
                v2_1.read(v3);
                v2_1.close();
            } catch (Exception v0) {
                v0.printStackTrace();
            }
            return new String(v3);
        } catch (Exception v0) {
        }
    }

    public static void reboot()
    {
        if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.startUnderRoot.booleanValue()) {
            try {
                String[] v1_3 = new String[1];
                v1_3[0] = "reboot";
                com.chelpus.Utils.cmdParam(v1_3);
                try {
                    String[] v1_5 = new String[1];
                    v1_5[0] = "/system/bin/reboot";
                    com.chelpus.Utils.cmdParam(v1_5);
                } catch (Exception v0_1) {
                    v0_1.printStackTrace();
                }
                try {
                    String[] v1_7 = new String[1];
                    v1_7[0] = "/system/xbin/reboot";
                    com.chelpus.Utils.cmdParam(v1_7);
                    try {
                        String[] v1_9 = new String[1];
                        v1_9[0] = "busybox reboot";
                        com.chelpus.Utils.cmdParam(v1_9);
                    } catch (Exception v0_3) {
                        v0_3.printStackTrace();
                    }
                    try {
                        String[] v1_11 = new String[1];
                        v1_11[0] = "reboot";
                        com.chelpus.Utils.cmd(v1_11);
                        try {
                            String[] v1_13 = new String[1];
                            v1_13[0] = "reboot";
                            com.chelpus.Utils.run_all_no_root(v1_13);
                        } catch (Exception v0_5) {
                            v0_5.printStackTrace();
                        }
                        try {
                            String[] v1_15 = new String[1];
                            v1_15[0] = new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.toolfilesdir).append("/reboot").toString();
                            com.chelpus.Utils.run_all_no_root(v1_15);
                        } catch (Exception v0_6) {
                            v0_6.printStackTrace();
                        }
                    } catch (Exception v0_4) {
                        v0_4.printStackTrace();
                    }
                } catch (Exception v0_2) {
                    v0_2.printStackTrace();
                }
            } catch (Exception v0_0) {
                v0_0.printStackTrace();
            }
        } else {
            try {
                String[] v1_17 = new com.chelpus.Utils("");
                int v2_9 = new String[1];
                v2_9[0] = "reboot";
                v1_17.cmdRoot(v2_9);
            } catch (Exception v0_7) {
                v0_7.printStackTrace();
            }
            try {
                String[] v1_19 = new com.chelpus.Utils("");
                int v2_12 = new String[1];
                v2_12[0] = "/system/bin/reboot";
                v1_19.cmdRoot(v2_12);
                try {
                    String[] v1_21 = new com.chelpus.Utils("");
                    int v2_15 = new String[1];
                    v2_15[0] = "/system/xbin/reboot";
                    v1_21.cmdRoot(v2_15);
                } catch (Exception v0_9) {
                    v0_9.printStackTrace();
                }
                try {
                    String[] v1_23 = new com.chelpus.Utils("");
                    int v2_18 = new String[1];
                    v2_18[0] = "busybox reboot";
                    v1_23.cmdRoot(v2_18);
                    try {
                        String[] v1_25 = new String[1];
                        v1_25[0] = "reboot";
                        com.chelpus.Utils.cmd(v1_25);
                    } catch (Exception v0_11) {
                        v0_11.printStackTrace();
                    }
                    try {
                        com.chelpus.Utils.run_all("reboot");
                        try {
                            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patchAct.runOnUiThread(new com.chelpus.Utils$15());
                        } catch (Exception v0_13) {
                            v0_13.printStackTrace();
                        }
                        new com.chelpus.Utils("w").waitLP(5000);
                        try {
                            com.chelpus.Utils.run_all(new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance().getFilesDir()).append("/reboot").toString());
                        } catch (Exception v0_14) {
                            v0_14.printStackTrace();
                        }
                    } catch (Exception v0_12) {
                        v0_12.printStackTrace();
                    }
                } catch (Exception v0_10) {
                    v0_10.printStackTrace();
                }
            } catch (Exception v0_8) {
                v0_8.printStackTrace();
            }
        }
        return;
    }

    public static boolean remount(String p9, String p10)
    {
        int v3_0 = 1;
        if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.startUnderRoot.booleanValue()) {
            if (p9.toLowerCase().startsWith("/system")) {
                String v5_4 = new String[2];
                v5_4[0] = "stop";
                v5_4[1] = "ric";
                com.chelpus.Utils.cmdParam(v5_4);
                if ((new java.io.File("/sbin/ric").exists()) || (new java.io.File("/system/bin/ric").exists())) {
                    String v5_11 = new String[2];
                    v5_11[0] = "stop";
                    v5_11[1] = "ric";
                    com.chelpus.Utils.cmdParam(v5_11);
                    String v5_12 = new String[3];
                    v5_12[0] = "pkill";
                    v5_12[1] = "-f";
                    v5_12[2] = "/sbin/ric";
                    com.chelpus.Utils.run_all_no_root(v5_12);
                    String v5_13 = new String[3];
                    v5_13[0] = "pkill";
                    v5_13[1] = "-f";
                    v5_13[2] = "/system/bin/ric";
                    com.chelpus.Utils.run_all_no_root(v5_13);
                }
            }
            if ((p9.endsWith("/")) && (!p9.equals("/"))) {
                p9 = p9.substring(0, p9.lastIndexOf("/"));
            }
            com.android.vending.billing.InAppBillingService.LUCK.Mount v2_0 = com.chelpus.Utils.findMountPointRecursive(p9);
            if (v2_0 != null) {
                if (!v2_0.getFlags().contains(p10.toLowerCase(java.util.Locale.US))) {
                    try {
                        String v5_22 = new String[5];
                        v5_22[0] = "mount";
                        v5_22[1] = "-o";
                        v5_22[2] = new StringBuilder().append("remount,").append(p10.toLowerCase(java.util.Locale.US)).toString();
                        v5_22[3] = v2_0.getDevice().getAbsolutePath();
                        v5_22[4] = v2_0.getMountPoint().getAbsolutePath();
                        com.chelpus.Utils.run_all_no_root(v5_22);
                        com.android.vending.billing.InAppBillingService.LUCK.Mount v2_1 = com.chelpus.Utils.findMountPointRecursive(p9);
                    } catch (String v5) {
                        v2_0 = com.chelpus.Utils.findMountPointRecursive(p9);
                    }
                    if (v2_1.getFlags().contains(p10.toLowerCase(java.util.Locale.US))) {
                    } else {
                        String v5_25 = new String[6];
                        v5_25[0] = "mount";
                        v5_25[1] = "-o";
                        v5_25[2] = "remount";
                        v5_25[3] = p10.toLowerCase(java.util.Locale.US);
                        v5_25[4] = v2_1.getDevice().getAbsolutePath();
                        v5_25[5] = v2_1.getMountPoint().getAbsolutePath();
                        com.chelpus.Utils.run_all_no_root(v5_25);
                    }
                }
                if (v2_0 != null) {
                    System.out.println(new StringBuilder().append(v2_0.getFlags()).append(" AND ").append(p10.toLowerCase(java.util.Locale.US)).toString());
                    if (v2_0.getFlags().contains(p10.toLowerCase(java.util.Locale.US))) {
                        System.out.println(v2_0.getFlags().toString());
                        return v3_0;
                    }
                }
            }
            System.out.println(new StringBuilder().append("LuckyPatcher:not remount ").append(p9).append(" to ").append(p10).toString());
            v3_0 = 0;
        } else {
            if (p9.toLowerCase().startsWith("/system")) {
                String v5_41 = new com.chelpus.Utils("");
                int v6_40 = new String[1];
                v6_40[0] = "stop ric";
                v5_41.cmdRoot(v6_40);
                if ((com.chelpus.Utils.exists("/sbin/ric")) || (com.chelpus.Utils.exists("/system/bin/ric"))) {
                    String v5_47 = new com.chelpus.Utils("");
                    int v6_42 = new String[1];
                    v6_42[0] = "stop ric";
                    v5_47.cmdRoot(v6_42);
                    com.chelpus.Utils.run_all("pkill -f /sbin/ric");
                    com.chelpus.Utils.run_all("pkill -f /system/bin/ric");
                }
            }
            if ((p9.endsWith("/")) && (!p9.equals("/"))) {
                p9 = p9.substring(0, p9.lastIndexOf("/"));
            }
            com.android.vending.billing.InAppBillingService.LUCK.Mount v2_2 = com.chelpus.Utils.findMountPointRecursive(p9);
            if (v2_2 != null) {
                if (!v2_2.getFlags().contains(p10.toLowerCase(java.util.Locale.US))) {
                    try {
                        com.chelpus.Utils.run_all(new StringBuilder().append("mount -o remount,").append(p10.toLowerCase(java.util.Locale.US)).append(" ").append(v2_2.getDevice().getAbsolutePath()).append(" ").append(v2_2.getMountPoint().getAbsolutePath()).toString());
                        com.android.vending.billing.InAppBillingService.LUCK.Mount v2_3 = com.chelpus.Utils.findMountPointRecursive(p9);
                    } catch (String v5) {
                        v2_2 = com.chelpus.Utils.findMountPointRecursive(p9);
                    }
                    if (v2_3.getFlags().contains(p10.toLowerCase(java.util.Locale.US))) {
                    } else {
                        com.chelpus.Utils.run_all(new StringBuilder().append("mount -o remount ").append(p10.toLowerCase(java.util.Locale.US)).append(" ").append(v2_3.getDevice().getAbsolutePath()).append(" ").append(v2_3.getMountPoint().getAbsolutePath()).toString());
                    }
                }
                if (v2_2 != null) {
                    System.out.println(new StringBuilder().append(v2_2.getFlags()).append(" AND ").append(p10.toLowerCase(java.util.Locale.US)).toString());
                    if (v2_2.getFlags().contains(p10.toLowerCase(java.util.Locale.US))) {
                        System.out.println(v2_2.getFlags().toString());
                        return v3_0;
                    }
                }
            }
            System.out.println(new StringBuilder().append("LuckyPatcher:not remount ").append(p9).append(" to ").append(p10).toString());
            v3_0 = 0;
        }
        return v3_0;
    }

    public static String removeExtension(String p5)
    {
        String v1 = "";
        if (p5 != null) {
            String[] v2 = p5.split("\\.");
            int v0 = 0;
            while (v0 < v2.length) {
                if (v0 >= (v2.length - 2)) {
                    v1 = new StringBuilder().append(v1).append(v2[v0]).toString();
                    break;
                } else {
                    v1 = new StringBuilder().append(v1).append(v2[v0]).append(".").toString();
                    v0++;
                }
            }
        }
        return v1;
    }

    public static void removePkgFromSystem(String p17)
    {
        com.chelpus.Utils.remount("/system", "rw");
        java.util.ArrayList v7_1 = new java.util.ArrayList();
        v7_1.add(new java.io.File("/system/app"));
        v7_1.add(new java.io.File("/system/priv-app"));
        java.util.Iterator v11 = v7_1.iterator();
        while (v11.hasNext()) {
            java.io.File v8_1 = ((java.io.File) v11.next());
            java.io.File[] v5 = v8_1.listFiles();
            if ((v5 == null) || (v5.length == 0)) {
                System.out.println(new StringBuilder().append("LuckyPatcher: 0 packages found in ").append(v8_1.getAbsolutePath()).toString());
            } else {
                int v10_8 = v5.length;
                int v9_8 = 0;
                while (v9_8 < v10_8) {
                    java.io.File v1 = v5[v9_8];
                    try {
                        if ((v1.getAbsolutePath().endsWith(".apk")) && (p17.equals(new com.android.vending.billing.InAppBillingService.LUCK.FileApkListItem(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance(), v1, 0).pkgName))) {
                            com.chelpus.Utils.run_all(new StringBuilder().append("chmod 777 \'").append(v1.getAbsolutePath()).append("\'").toString());
                            com.chelpus.Utils.run_all(new StringBuilder().append("rm \'").append(v1.getAbsolutePath()).append("\'").toString());
                            com.chelpus.Utils.run_all(new StringBuilder().append("rm \'").append(com.chelpus.Utils.getPlaceForOdex(v1.getAbsolutePath(), 0)).append("\'").toString());
                        }
                        v9_8++;
                    } catch (Exception v3_2) {
                        v3_2.printStackTrace();
                    }
                }
                String v12_2 = v5.length;
                int v10_9 = 0;
                while (v10_9 < v12_2) {
                    java.io.File v2 = v5[v10_9];
                    if (v2.isDirectory()) {
                        try {
                            java.io.File[] v6 = v2.listFiles();
                        } catch (Exception v3_1) {
                            v3_1.printStackTrace();
                        }
                        if ((v6 != null) && (v6.length != 0)) {
                            int v13_0 = v6.length;
                            int v9_12 = 0;
                            while (v9_12 < v13_0) {
                                java.io.File v4 = v6[v9_12];
                                try {
                                    if ((v4.getAbsolutePath().endsWith(".apk")) && (p17.equals(new com.android.vending.billing.InAppBillingService.LUCK.FileApkListItem(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance(), v4, 0).pkgName))) {
                                        com.chelpus.Utils.run_all(new StringBuilder().append("chmod 777 \'").append(v4.getAbsolutePath()).append("\'").toString());
                                        com.chelpus.Utils.run_all(new StringBuilder().append("rm \'").append(v4.getAbsolutePath()).append("\'").toString());
                                        com.chelpus.Utils.run_all(new StringBuilder().append("rm \'").append(com.chelpus.Utils.getPlaceForOdex(v4.getAbsolutePath(), 0)).append("\'").toString());
                                        com.chelpus.Utils.run_all(new StringBuilder().append("rm -rf \'").append(v2.getAbsolutePath()).append("\'").toString());
                                    }
                                    v9_12++;
                                } catch (Exception v3_0) {
                                    v3_0.printStackTrace();
                                }
                            }
                        }
                    }
                    v10_9++;
                }
            }
        }
        return;
    }

    public static int replaceStringIds(String p26, String[] p27, boolean p28, String[] p29)
    {
        System.out.println(new StringBuilder().append("scan: ").append(p26).toString());
        int v18 = 0;
        int v2_2 = new String[3];
        v2_2[0] = "chmod";
        v2_2[1] = "777";
        v2_2[2] = p26;
        com.chelpus.Utils.run_all_no_root(v2_2);
        new java.util.ArrayList();
        try {
            if (p28) {
                if (new java.io.File(p26).exists()) {
                    try {
                        java.nio.channels.FileChannel v1_0 = new java.io.RandomAccessFile(p26, "rw").getChannel();
                        java.nio.MappedByteBuffer v10_0 = v1_0.map(java.nio.channels.FileChannel$MapMode.READ_WRITE, 0, ((long) ((int) v1_0.size())));
                        v10_0.position(8);
                        int v14 = com.chelpus.Utils.convertFourBytesToInt(v10_0.get(), v10_0.get(), v10_0.get(), v10_0.get());
                        v10_0.position((v14 + 56));
                        int v22_0 = com.chelpus.Utils.convertFourBytesToInt(v10_0.get(), v10_0.get(), v10_0.get(), v10_0.get());
                        int v13_0 = (com.chelpus.Utils.convertFourBytesToInt(v10_0.get(), v10_0.get(), v10_0.get(), v10_0.get()) + v14);
                        System.out.println(new StringBuilder().append("LuckyPatcher offset_to_data=").append(Integer.toHexString(v13_0)).toString());
                        v10_0.position(v13_0);
                        int v23_0 = v22_0;
                        int[] v15_0 = new int[v23_0];
                        int v11_0 = 0;
                    } catch (Exception v9_0) {
                        v9_0.printStackTrace();
                    }
                    while (v11_0 < v23_0) {
                        v15_0[v11_0] = (com.chelpus.Utils.convertFourBytesToInt(v10_0.get(), v10_0.get(), v10_0.get(), v10_0.get()) + v14);
                        v11_0++;
                    }
                    int v3_18 = v15_0.length;
                    int v2_16 = 0;
                    while (v2_16 < v3_18) {
                        int v12_0 = v15_0[v2_16];
                        int v21_0 = com.chelpus.Utils.convertByteToInt(v10_0.get(v12_0));
                        byte[] v8_0 = new byte[v21_0];
                        int v16_0 = (v12_0 + 1);
                        v10_0.position(v16_0);
                        int v17_0 = 0;
                        while (v17_0 < v8_0.length) {
                            v8_0[v17_0] = v10_0.get();
                            v17_0++;
                        }
                        int v7_0 = 0;
                        while (v7_0 < p27.length) {
                            if (new String(v8_0).equals(p27[v7_0])) {
                                byte[] v24_0 = p29[v7_0].getBytes();
                                if (v24_0.length <= v21_0) {
                                    v10_0.position(v12_0);
                                    v10_0.put(((byte) v24_0.length));
                                    v10_0.position(v16_0);
                                    v10_0.put(v24_0);
                                    v10_0.put(0);
                                    v10_0.force();
                                    System.out.println(new StringBuilder().append("Replace string:").append(p27[v7_0]).toString());
                                    v18++;
                                }
                            }
                            v7_0++;
                        }
                        v2_16++;
                    }
                    v1_0.close();
                }
            } else {
                try {
                    if ((p26 != null) && ((new java.io.File(p26).exists()) && ((new java.io.File(p26).length() != 0) && (new java.io.File(p26).exists())))) {
                        try {
                            java.nio.channels.FileChannel v1_1 = new java.io.RandomAccessFile(p26, "rw").getChannel();
                            java.nio.MappedByteBuffer v10_1 = v1_1.map(java.nio.channels.FileChannel$MapMode.READ_WRITE, 0, ((long) ((int) v1_1.size())));
                            v10_1.position(56);
                            int v22_1 = com.chelpus.Utils.convertFourBytesToInt(v10_1.get(), v10_1.get(), v10_1.get(), v10_1.get());
                            v10_1.position(com.chelpus.Utils.convertFourBytesToInt(v10_1.get(), v10_1.get(), v10_1.get(), v10_1.get()));
                            int v23_1 = v22_1;
                            int[] v15_1 = new int[v23_1];
                            int v11_1 = 0;
                        } catch (Exception v9_1) {
                            v9_1.printStackTrace();
                        }
                        while (v11_1 < v23_1) {
                            v15_1[v11_1] = com.chelpus.Utils.convertFourBytesToInt(v10_1.get(), v10_1.get(), v10_1.get(), v10_1.get());
                            v11_1++;
                        }
                        int v3_24 = v15_1.length;
                        int v2_37 = 0;
                        while (v2_37 < v3_24) {
                            int v12_1 = v15_1[v2_37];
                            int v21_1 = com.chelpus.Utils.convertByteToInt(v10_1.get(v12_1));
                            byte[] v8_1 = new byte[v21_1];
                            int v16_1 = (v12_1 + 1);
                            v10_1.position(v16_1);
                            int v17_1 = 0;
                            while (v17_1 < v8_1.length) {
                                v8_1[v17_1] = v10_1.get();
                                v17_1++;
                            }
                            int v7_1 = 0;
                            while (v7_1 < p27.length) {
                                if (new String(v8_1).equals(p27[v7_1])) {
                                    byte[] v24_1 = p29[v7_1].getBytes();
                                    if (v24_1.length <= v21_1) {
                                        v10_1.position(v12_1);
                                        v10_1.put(((byte) v24_1.length));
                                        v10_1.position(v16_1);
                                        v10_1.put(v24_1);
                                        v10_1.put(0);
                                        v10_1.force();
                                        System.out.println(new StringBuilder().append("Replace string:").append(p27[v7_1]).toString());
                                        v18++;
                                    }
                                }
                                v7_1++;
                            }
                            v2_37++;
                        }
                        v1_1.close();
                    }
                } catch (Exception v9_2) {
                    v9_2.printStackTrace();
                }
            }
        } catch (Exception v9_3) {
            System.out.println(v9_3);
        }
        return v18;
    }

    public static String rework(String p3)
    {
        if (p3.contains("52")) {
            p3 = p3.replaceAll("52", "f2");
        }
        if (p3.contains("53")) {
            p3 = p3.replaceAll("53", "f3");
        }
        if (p3.contains("54")) {
            p3 = p3.replaceAll("54", "f4");
        }
        if (p3.contains("55")) {
            p3 = p3.replaceAll("55", "f2");
        }
        if (p3.contains("59")) {
            p3 = p3.replaceAll("59", "f5");
        }
        if (p3.toUpperCase().contains("5A")) {
            p3 = p3.toUpperCase().replaceAll("5A", "F6");
        }
        if (p3.toUpperCase().contains("5B")) {
            p3 = p3.toUpperCase().replaceAll("5B", "F7");
        }
        if (p3.toUpperCase().contains("5C")) {
            p3 = p3.toUpperCase().replaceAll("5C", "F5");
        }
        if (p3.toUpperCase().contains("5D")) {
            p3 = p3.toUpperCase().replaceAll("5D", "F5");
        }
        if (p3.contains("74")) {
            p3 = p3.replaceAll("74", "f9");
        }
        if (p3.toUpperCase().contains("6E")) {
            p3 = p3.toUpperCase().replaceAll("6E", "F8");
        }
        return p3;
    }

    public static void run_all(String p11)
    {
        try {
            if ((!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.su) || (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.checktools)) {
                if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.su) {
                    String[] v0 = new String[(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.tools.length + 1)];
                    int v2 = 1;
                    v0[0] = p11;
                    String v7_11 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.tools;
                    int v8_2 = v7_11.length;
                    int v5_35 = 0;
                    while (v5_35 < v8_2) {
                        v0[v2] = new StringBuilder().append(v7_11[v5_35]).append(" ").append(p11).toString();
                        v2++;
                        v5_35++;
                    }
                    new com.chelpus.Utils("").cmdRoot(v0);
                }
            } else {
                java.util.ArrayList v4_1 = new java.util.ArrayList();
                v4_1.clear();
                v4_1.add(new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.toolfilesdir).append("/busybox").toString());
                if (com.chelpus.Utils.exists("/system/bin/failsafe/toolbox")) {
                    v4_1.add("/system/bin/failsafe/toolbox");
                }
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.errorOutput = "";
                int v5_12 = new com.chelpus.Utils("");
                String v7_4 = new String[1];
                v7_4[0] = new StringBuilder().append("busybox chmod 777 ").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.toolfilesdir).append("/busybox").toString();
                v5_12.cmdRoot(v7_4);
                if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.errorOutput.equals("")) {
                    v4_1.add("busybox");
                }
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.errorOutput = "";
                int v5_18 = new com.chelpus.Utils("");
                String v7_8 = new String[1];
                v7_8[0] = new StringBuilder().append("toolbox chmod 777 ").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.toolfilesdir).append("/busybox").toString();
                v5_18.cmdRoot(v7_8);
                if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.errorOutput.equals("")) {
                    System.out.println("skip toolbox in tools");
                } else {
                    v4_1.add("toolbox");
                }
                int v5_24 = new String[v4_1.size()];
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.tools = v5_24;
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.tools = ((String[]) ((String[]) v4_1.toArray(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.tools)));
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.checktools = 1;
            }
        } catch (Exception v1) {
            v1.printStackTrace();
        }
        return;
    }

    public static varargs void run_all_no_root(String[] p11)
    {
        try {
            if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.checktools) {
                com.chelpus.Utils.cmdParam(p11);
                if ((com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.tools != null) && (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.tools.length > 0)) {
                    String[] v4 = new String[(p11.length + 1)];
                    int v1 = 1;
                    String[] v8_14 = p11.length;
                    int v6_35 = 0;
                    while (v6_35 < v8_14) {
                        v4[v1] = p11[v6_35];
                        v1++;
                        v6_35++;
                    }
                    String[] v8_15 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.tools;
                    int v9_16 = v8_15.length;
                    int v6_36 = 0;
                    while (v6_36 < v9_16) {
                        v4[0] = v8_15[v6_36];
                        com.chelpus.Utils.cmdParam(v4);
                        v6_36++;
                    }
                }
            } else {
                java.util.ArrayList v5_1 = new java.util.ArrayList();
                v5_1.clear();
                v5_1.add(new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.toolfilesdir).append("/busybox").toString());
                if (com.chelpus.Utils.exists("/system/bin/failsafe/toolbox")) {
                    v5_1.add("/system/bin/failsafe/toolbox");
                }
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.errorOutput = "";
                int v6_11 = new String[4];
                v6_11[0] = "busybox";
                v6_11[1] = "chmod";
                v6_11[2] = "777";
                v6_11[3] = new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.toolfilesdir).append("/busybox").toString();
                com.chelpus.Utils.cmdParam(v6_11);
                if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.errorOutput.equals("")) {
                    System.out.println("skip busybox in tools");
                } else {
                    v5_1.add("busybox");
                }
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.errorOutput = "";
                int v6_18 = new String[4];
                v6_18[0] = "toolbox";
                v6_18[1] = "chmod";
                v6_18[2] = "777";
                v6_18[3] = new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.toolfilesdir).append("/busybox").toString();
                com.chelpus.Utils.cmdParam(v6_18);
                if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.errorOutput.equals("")) {
                    System.out.println("skip toolbox in tools");
                } else {
                    v5_1.add("toolbox");
                }
                int v6_24 = new String[v5_1.size()];
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.tools = v6_24;
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.tools = ((String[]) ((String[]) v5_1.toArray(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.tools)));
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.checktools = 1;
            }
        } catch (Exception v0) {
            v0.printStackTrace();
        }
        return;
    }

    public static boolean saveXposedParamBoolean(String p7, boolean p8, boolean p9)
    {
        int v3 = 1;
        try {
            org.json.JSONObject v2_1 = new org.json.JSONObject(com.chelpus.Utils.read_from_file(new java.io.File("/data/lp/xposed")));
        } catch (java.io.IOException v0_0) {
            v0_0.printStackTrace();
            v3 = 0;
            return v3;
        }
        try {
            v2_1.put(p7, p8);
        } catch (java.io.IOException v0_1) {
            v0_1.printStackTrace();
        }
        System.out.println(v2_1.toString());
        com.chelpus.Utils.save_text_to_file(new java.io.File("/data/lp/xposed"), v2_1.toString());
        if (!p9) {
            return v3;
        } else {
            if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.startUnderRoot.booleanValue()) {
                com.chelpus.Utils.run_all("chmod 777 /data/lp/xposed");
                try {
                    new java.io.File("/data/lp/settings_android_changed").createNewFile();
                    new java.io.File("/data/lp/settings_android_changed").setWritable(1, 0);
                    new java.io.File("/data/lp/settings_changed").createNewFile();
                    new java.io.File("/data/lp/settings_changed").setWritable(1, 0);
                } catch (java.io.IOException v0_2) {
                    v0_2.printStackTrace();
                }
                return v3;
            } else {
                int v5_15 = new String[3];
                v5_15[0] = "chmod";
                v5_15[1] = "777";
                v5_15[2] = "/data/lp/xposed";
                com.chelpus.Utils.run_all_no_root(v5_15);
                try {
                    new java.io.File("/data/lp/settings_android_changed").createNewFile();
                    new java.io.File("/data/lp/settings_android_changed").setWritable(1, 0);
                    new java.io.File("/data/lp/settings_changed").createNewFile();
                    new java.io.File("/data/lp/settings_changed").setWritable(1, 0);
                } catch (java.io.IOException v0_3) {
                    v0_3.printStackTrace();
                }
                return v3;
            }
        }
    }

    public static boolean save_text_to_end_file(java.io.File p10, String p11)
    {
        int v5 = 0;
        try {
            if (p10.exists()) {
                System.out.println("...rrunning my app...");
                java.io.RandomAccessFile v4_1 = new java.io.RandomAccessFile(p10, "rw");
                try {
                    long v1 = p10.length();
                    v4_1.seek(p10.length());
                    v4_1.write(p11.getBytes());
                    System.out.println(new StringBuilder().append("...file length...").append(v4_1.length()).toString());
                    v4_1.close();
                } catch (Exception v0) {
                    v0.printStackTrace();
                }
                if (p10.length() == (((long) p11.length()) + v1)) {
                    v5 = 1;
                }
            } else {
                com.chelpus.Utils.getDirs(p10).mkdirs();
                p10.createNewFile();
            }
        } catch (Exception v0) {
        }
        return v5;
    }

    public static boolean save_text_to_end_file_from_file(java.io.File p14, java.io.File p15)
    {
        try {
            System.out.println("...rrunning my app...");
            java.io.RandomAccessFile v7_1 = new java.io.RandomAccessFile(p14, "rw");
            try {
                java.io.RandomAccessFile v9_1 = new java.io.RandomAccessFile(p15, "r");
            } catch (Exception v0) {
                v0.printStackTrace();
                int v12_9 = 0;
                return v12_9;
            }
            try {
                p14.length();
                v7_1.seek(p14.length());
                try {
                    v7_1.write("#Lucky Patcher block Ads start#\n".getBytes());
                    byte[] v10 = new byte[4096];
                } catch (java.io.IOException v2) {
                    v2.printStackTrace();
                    v12_9 = 0;
                    return v12_9;
                }
                while(true) {
                    int v5 = v9_1.read(v10);
                    if (v5 <= 0) {
                        break;
                    }
                    v7_1.write(v10, 0, v5);
                }
                v7_1.write("#Lucky Patcher block Ads finish#\n\n\n\r\n".getBytes());
                v9_1.close();
                v7_1.close();
                v12_9 = 1;
                return v12_9;
            } catch (Exception v0) {
            }
        } catch (Exception v0) {
        }
    }

    public static boolean save_text_to_file(java.io.File p8, String p9)
    {
        int v3 = 0;
        try {
            if (p8.exists()) {
                System.out.println("...rrunning my app...");
                java.io.RandomAccessFile v2_1 = new java.io.RandomAccessFile(p8, "rw");
                try {
                    v2_1.setLength(0);
                    v2_1.seek(0);
                    v2_1.write(p9.getBytes());
                    v2_1.close();
                } catch (Exception v0) {
                    v0.printStackTrace();
                }
                if (p8.length() == ((long) p9.length())) {
                    v3 = 1;
                }
            } else {
                com.chelpus.Utils.getDirs(p8).mkdirs();
                p8.createNewFile();
            }
        } catch (Exception v0) {
        }
        return v3;
    }

    public static boolean sendFromRoot(String p1)
    {
        System.out.println(p1);
        return 0;
    }

    public static boolean sendFromRootCP(String p1)
    {
        System.out.println(p1);
        return 0;
    }

    public static final void setIcon(int p7)
    {
        switch (p7) {
            case 0:
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().setComponentEnabledSetting(new android.content.ComponentName(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance(), "com.android.vending.billing.InAppBillingService.LUCK.MainActivity-Original"), 1, 1);
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().setComponentEnabledSetting(new android.content.ComponentName(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance(), "com.android.vending.billing.InAppBillingService.LUCK.MainActivity-Flint"), 2, 1);
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().setComponentEnabledSetting(new android.content.ComponentName(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance(), "com.android.vending.billing.InAppBillingService.LUCK.MainActivity-2"), 2, 1);
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().setComponentEnabledSetting(new android.content.ComponentName(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance(), "com.android.vending.billing.InAppBillingService.LUCK.MainActivity-3"), 2, 1);
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().setComponentEnabledSetting(new android.content.ComponentName(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance(), "com.android.vending.billing.InAppBillingService.LUCK.MainActivity-4"), 2, 1);
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().setComponentEnabledSetting(new android.content.ComponentName(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance(), "com.android.vending.billing.InAppBillingService.LUCK.MainActivity-5"), 2, 1);
                try {
                    if ((com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag == null) || (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.api <= 10)) {
                    } else {
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.getActivity().getActionBar().setIcon(2130837551);
                    }
                } catch (android.app.ActionBar v0) {
                }
                break;
            case 1:
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().setComponentEnabledSetting(new android.content.ComponentName(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance(), "com.android.vending.billing.InAppBillingService.LUCK.MainActivity-Original"), 2, 1);
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().setComponentEnabledSetting(new android.content.ComponentName(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance(), "com.android.vending.billing.InAppBillingService.LUCK.MainActivity-Flint"), 1, 1);
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().setComponentEnabledSetting(new android.content.ComponentName(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance(), "com.android.vending.billing.InAppBillingService.LUCK.MainActivity-2"), 2, 1);
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().setComponentEnabledSetting(new android.content.ComponentName(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance(), "com.android.vending.billing.InAppBillingService.LUCK.MainActivity-3"), 2, 1);
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().setComponentEnabledSetting(new android.content.ComponentName(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance(), "com.android.vending.billing.InAppBillingService.LUCK.MainActivity-4"), 2, 1);
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().setComponentEnabledSetting(new android.content.ComponentName(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance(), "com.android.vending.billing.InAppBillingService.LUCK.MainActivity-5"), 2, 1);
                try {
                    if ((com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag == null) || (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.api <= 10)) {
                    } else {
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.getActivity().getActionBar().setIcon(2130903046);
                    }
                } catch (android.app.ActionBar v0) {
                }
                break;
            case 2:
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().setComponentEnabledSetting(new android.content.ComponentName(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance(), "com.android.vending.billing.InAppBillingService.LUCK.MainActivity-Original"), 2, 1);
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().setComponentEnabledSetting(new android.content.ComponentName(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance(), "com.android.vending.billing.InAppBillingService.LUCK.MainActivity-Flint"), 2, 1);
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().setComponentEnabledSetting(new android.content.ComponentName(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance(), "com.android.vending.billing.InAppBillingService.LUCK.MainActivity-2"), 1, 1);
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().setComponentEnabledSetting(new android.content.ComponentName(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance(), "com.android.vending.billing.InAppBillingService.LUCK.MainActivity-3"), 2, 1);
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().setComponentEnabledSetting(new android.content.ComponentName(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance(), "com.android.vending.billing.InAppBillingService.LUCK.MainActivity-4"), 2, 1);
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().setComponentEnabledSetting(new android.content.ComponentName(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance(), "com.android.vending.billing.InAppBillingService.LUCK.MainActivity-5"), 2, 1);
                try {
                    if ((com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag == null) || (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.api <= 10)) {
                    } else {
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.getActivity().getActionBar().setIcon(2130903047);
                    }
                } catch (android.app.ActionBar v0) {
                }
                break;
            case 3:
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().setComponentEnabledSetting(new android.content.ComponentName(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance(), "com.android.vending.billing.InAppBillingService.LUCK.MainActivity-Original"), 2, 1);
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().setComponentEnabledSetting(new android.content.ComponentName(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance(), "com.android.vending.billing.InAppBillingService.LUCK.MainActivity-Flint"), 2, 1);
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().setComponentEnabledSetting(new android.content.ComponentName(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance(), "com.android.vending.billing.InAppBillingService.LUCK.MainActivity-2"), 2, 1);
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().setComponentEnabledSetting(new android.content.ComponentName(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance(), "com.android.vending.billing.InAppBillingService.LUCK.MainActivity-3"), 1, 1);
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().setComponentEnabledSetting(new android.content.ComponentName(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance(), "com.android.vending.billing.InAppBillingService.LUCK.MainActivity-4"), 2, 1);
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().setComponentEnabledSetting(new android.content.ComponentName(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance(), "com.android.vending.billing.InAppBillingService.LUCK.MainActivity-5"), 2, 1);
                try {
                    if ((com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag == null) || (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.api <= 10)) {
                    } else {
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.getActivity().getActionBar().setIcon(2130903048);
                    }
                } catch (android.app.ActionBar v0) {
                }
                break;
            case 4:
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().setComponentEnabledSetting(new android.content.ComponentName(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance(), "com.android.vending.billing.InAppBillingService.LUCK.MainActivity-Original"), 2, 1);
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().setComponentEnabledSetting(new android.content.ComponentName(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance(), "com.android.vending.billing.InAppBillingService.LUCK.MainActivity-Flint"), 2, 1);
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().setComponentEnabledSetting(new android.content.ComponentName(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance(), "com.android.vending.billing.InAppBillingService.LUCK.MainActivity-2"), 2, 1);
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().setComponentEnabledSetting(new android.content.ComponentName(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance(), "com.android.vending.billing.InAppBillingService.LUCK.MainActivity-3"), 2, 1);
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().setComponentEnabledSetting(new android.content.ComponentName(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance(), "com.android.vending.billing.InAppBillingService.LUCK.MainActivity-4"), 1, 1);
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().setComponentEnabledSetting(new android.content.ComponentName(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance(), "com.android.vending.billing.InAppBillingService.LUCK.MainActivity-5"), 2, 1);
                try {
                    if ((com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag == null) || (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.api <= 10)) {
                    } else {
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.getActivity().getActionBar().setIcon(2130903049);
                    }
                } catch (android.app.ActionBar v0) {
                }
                break;
            case 5:
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().setComponentEnabledSetting(new android.content.ComponentName(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance(), "com.android.vending.billing.InAppBillingService.LUCK.MainActivity-Original"), 2, 1);
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().setComponentEnabledSetting(new android.content.ComponentName(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance(), "com.android.vending.billing.InAppBillingService.LUCK.MainActivity-Flint"), 2, 1);
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().setComponentEnabledSetting(new android.content.ComponentName(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance(), "com.android.vending.billing.InAppBillingService.LUCK.MainActivity-2"), 2, 1);
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().setComponentEnabledSetting(new android.content.ComponentName(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance(), "com.android.vending.billing.InAppBillingService.LUCK.MainActivity-3"), 2, 1);
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().setComponentEnabledSetting(new android.content.ComponentName(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance(), "com.android.vending.billing.InAppBillingService.LUCK.MainActivity-4"), 2, 1);
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().setComponentEnabledSetting(new android.content.ComponentName(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance(), "com.android.vending.billing.InAppBillingService.LUCK.MainActivity-5"), 1, 1);
                try {
                    if ((com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag == null) || (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.api <= 10)) {
                    } else {
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.getActivity().getActionBar().setIcon(2130903050);
                    }
                } catch (android.app.ActionBar v0) {
                }
                break;
        }
        return;
    }

    public static void setPermissionDir(String p6, String p7, String p8)
    {
        if (new java.io.File(p7).exists()) {
            String[] v1 = p7.split(java.io.File.separator);
            String v0 = "/";
            int v2 = 0;
            while (v2 < v1.length) {
                if (!v1[v2].equals("")) {
                    v0 = new StringBuilder().append(v0).append(v1[v2]).toString();
                }
                if ((v0.startsWith(p6)) || (new StringBuilder().append(v0).append("/").toString().startsWith(p6))) {
                    StringBuilder v3_19 = new String[3];
                    v3_19[0] = "chmod";
                    v3_19[1] = p8;
                    v3_19[2] = v0;
                    com.chelpus.Utils.cmdParam(v3_19);
                }
                if (!v1[v2].equals("")) {
                    v0 = new StringBuilder().append(v0).append("/").toString();
                }
                v2++;
            }
        }
        return;
    }

    public static int setStringIds(String p28, byte[][] p29, boolean p30, byte p31)
    {
        System.out.println(new StringBuilder().append("scan: ").append(p28).toString());
        int v20 = 0;
        int v2_2 = new String[3];
        v2_2[0] = "chmod";
        v2_2[1] = "777";
        v2_2[2] = p28;
        com.chelpus.Utils.run_all_no_root(v2_2);
        new java.util.ArrayList();
        try {
            if (p30) {
                if (new java.io.File(p28).exists()) {
                    try {
                        java.nio.channels.FileChannel v1_0 = new java.io.RandomAccessFile(p28, "rw").getChannel();
                        java.nio.MappedByteBuffer v10_0 = v1_0.map(java.nio.channels.FileChannel$MapMode.READ_WRITE, 0, ((long) ((int) v1_0.size())));
                        v10_0.position(8);
                        int v15 = com.chelpus.Utils.convertFourBytesToInt(v10_0.get(), v10_0.get(), v10_0.get(), v10_0.get());
                        v10_0.position((v15 + 56));
                        int v24_0 = com.chelpus.Utils.convertFourBytesToInt(v10_0.get(), v10_0.get(), v10_0.get(), v10_0.get());
                        int v14_0 = (com.chelpus.Utils.convertFourBytesToInt(v10_0.get(), v10_0.get(), v10_0.get(), v10_0.get()) + v15);
                        System.out.println(new StringBuilder().append("LuckyPatcher offset_to_data=").append(Integer.toHexString(v14_0)).toString());
                        v10_0.position(v14_0);
                        int v25_0 = v24_0;
                        byte v0_4 = new int[v25_0];
                        int[] v16_0 = v0_4;
                        int v11_0 = 0;
                    } catch (Exception v9_0) {
                        v9_0.printStackTrace();
                    }
                    while (v11_0 < v25_0) {
                        v16_0[v11_0] = (com.chelpus.Utils.convertFourBytesToInt(v10_0.get(), v10_0.get(), v10_0.get(), v10_0.get()) + v15);
                        v11_0++;
                    }
                    int v4_8 = v16_0.length;
                    int v3_18 = 0;
                    while (v3_18 < v4_8) {
                        int v13_0 = v16_0[v3_18];
                        int v23_0 = com.chelpus.Utils.convertByteToInt(v10_0.get(v13_0));
                        byte[] v8_0 = new byte[v23_0];
                        int v18_0 = (v13_0 + 1);
                        v10_0.position(v18_0);
                        int v19_0 = 0;
                        while (v19_0 < v8_0.length) {
                            v8_0[v19_0] = v10_0.get();
                            v19_0++;
                        }
                        int v7_0 = 0;
                        while (v7_0 < p29.length) {
                            byte[] v17_0 = ((byte[]) ((byte[]) reflect.Array.get(p29, v7_0)));
                            if ((v17_0.length < v23_0) && (v17_0.length > 2)) {
                                int v27_0 = 0;
                                while (v27_0 < v8_0.length) {
                                    if (v8_0[v27_0] == v17_0[0]) {
                                        int v11_1 = 1;
                                        int v12_0 = v27_0;
                                        while ((v11_1 < v17_0.length) && (((v27_0 + v11_1) < v8_0.length) && (v8_0[(v27_0 + v11_1)] == v17_0[v11_1]))) {
                                            v11_1++;
                                            if (v11_1 == v17_0.length) {
                                                if (v17_0.length > 2) {
                                                    v10_0.position(((v18_0 + v12_0) + (v17_0.length - 1)));
                                                    v10_0.put(p31);
                                                }
                                                v10_0.force();
                                                v20++;
                                            }
                                        }
                                    }
                                    v27_0++;
                                }
                            }
                            v7_0++;
                        }
                        v3_18++;
                    }
                    v1_0.close();
                }
            } else {
                try {
                    if ((p28 != null) && ((new java.io.File(p28).exists()) && ((new java.io.File(p28).length() != 0) && (new java.io.File(p28).exists())))) {
                        try {
                            java.nio.channels.FileChannel v1_1 = new java.io.RandomAccessFile(p28, "rw").getChannel();
                            java.nio.MappedByteBuffer v10_1 = v1_1.map(java.nio.channels.FileChannel$MapMode.READ_WRITE, 0, ((long) ((int) v1_1.size())));
                            v10_1.position(56);
                            int v24_1 = com.chelpus.Utils.convertFourBytesToInt(v10_1.get(), v10_1.get(), v10_1.get(), v10_1.get());
                            v10_1.position(com.chelpus.Utils.convertFourBytesToInt(v10_1.get(), v10_1.get(), v10_1.get(), v10_1.get()));
                            int v25_1 = v24_1;
                            byte v0_29 = new int[v25_1];
                            int[] v16_1 = v0_29;
                            int v11_2 = 0;
                        } catch (Exception v9_1) {
                            v9_1.printStackTrace();
                        }
                        while (v11_2 < v25_1) {
                            v16_1[v11_2] = com.chelpus.Utils.convertFourBytesToInt(v10_1.get(), v10_1.get(), v10_1.get(), v10_1.get());
                            v11_2++;
                        }
                        int v4_13 = v16_1.length;
                        int v3_24 = 0;
                        while (v3_24 < v4_13) {
                            int v13_1 = v16_1[v3_24];
                            int v23_1 = com.chelpus.Utils.convertByteToInt(v10_1.get(v13_1));
                            byte[] v8_1 = new byte[v23_1];
                            int v18_1 = (v13_1 + 1);
                            v10_1.position(v18_1);
                            int v19_1 = 0;
                            while (v19_1 < v8_1.length) {
                                v8_1[v19_1] = v10_1.get();
                                v19_1++;
                            }
                            int v7_1 = 0;
                            while (v7_1 < p29.length) {
                                byte[] v17_1 = ((byte[]) ((byte[]) reflect.Array.get(p29, v7_1)));
                                if ((v17_1.length <= v23_1) && (v17_1.length > 2)) {
                                    int v27_1 = 0;
                                    while (v27_1 < v8_1.length) {
                                        if (v8_1[v27_1] == v17_1[0]) {
                                            int v11_3 = 1;
                                            int v12_1 = v27_1;
                                            while ((v11_3 < v17_1.length) && (((v27_1 + v11_3) < v8_1.length) && (v8_1[(v27_1 + v11_3)] == v17_1[v11_3]))) {
                                                v11_3++;
                                                if (v11_3 == v17_1.length) {
                                                    if (v17_1.length > 2) {
                                                        v10_1.position(((v18_1 + v12_1) + (v17_1.length - 1)));
                                                        v10_1.put(p31);
                                                    }
                                                    v10_1.force();
                                                    v20++;
                                                }
                                            }
                                        }
                                        v27_1++;
                                    }
                                }
                                v7_1++;
                            }
                            v3_24++;
                        }
                        v1_1.close();
                    }
                } catch (Exception v9_2) {
                    v9_2.printStackTrace();
                }
            }
        } catch (Exception v9_3) {
            System.out.println(v9_3);
        }
        return v20;
    }

    public static void setTitle(android.app.AlertDialog$Builder p6, int p7, String p8)
    {
        android.widget.LinearLayout v0_1 = ((android.widget.LinearLayout) android.view.View.inflate(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.getContext(), 2130968610, 0));
        android.widget.TextView v2_1 = ((android.widget.TextView) v0_1.findViewById(2131558434));
        ((android.widget.ImageView) v0_1.findViewById(2131558433)).setImageDrawable(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getRes().getDrawable(p7));
        v2_1.setText(p8);
        p6.setCustomTitle(v0_1);
        return;
    }

    public static String sha1withrsa_sign(String p9)
    {
        try {
            java.security.PrivateKey v3 = java.security.KeyPairGenerator.getInstance("RSA").generateKeyPair().getPrivate();
            java.security.Signature v1 = java.security.Signature.getInstance("SHA1withRSA");
            v1.initSign(v3);
            v1.update(p9.getBytes());
            byte[] v5 = v1.sign();
            String v4 = com.google.android.finsky.billing.iab.google.util.Base64.encode(v5);
            System.out.println(new StringBuilder().append("b64: ").append(com.google.android.finsky.billing.iab.google.util.Base64.encode(v5)).toString());
            System.out.println(new StringBuilder().append("Signature: ").append(new String(v5)).toString());
        } catch (java.security.InvalidKeyException v0_2) {
            v0_2.printStackTrace();
        } catch (java.security.InvalidKeyException v0_0) {
            v0_0.printStackTrace();
        } catch (java.security.InvalidKeyException v0_1) {
            v0_1.printStackTrace();
        }
        return v4;
    }

    public static void showDialog(android.app.Dialog p2)
    {
        try {
            if ((com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patchAct != null) && (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patchAct.isFinishing())) {
                p2.show();
            }
        } catch (Exception v0) {
            v0.printStackTrace();
        }
        return;
    }

    public static void showDialogCustomYes(String p5, String p6, String p7, android.content.DialogInterface$OnClickListener p8, android.content.DialogInterface$OnClickListener p9, android.content.DialogInterface$OnCancelListener p10)
    {
        try {
            if ((com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patchAct != null) && (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patchAct.isFinishing())) {
                android.app.Dialog v1 = new com.android.vending.billing.InAppBillingService.LUCK.AlertDlg(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.getContext()).setTitle(p5).setMessage(p6).setIcon(2130837554).setPositiveButton(p7, p8).setNegativeButton(com.chelpus.Utils.getText(2131165563), p9).setOnCancelListener(p10).create();
                com.chelpus.Utils.showDialog(v1);
                v1.findViewById(16908299);
            }
        } catch (Exception v2) {
            v2.printStackTrace();
        }
        return;
    }

    public static void showDialogCustomYesNo(String p5, String p6, String p7, android.content.DialogInterface$OnClickListener p8, String p9, android.content.DialogInterface$OnClickListener p10, android.content.DialogInterface$OnCancelListener p11)
    {
        try {
            if ((com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patchAct != null) && (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patchAct.isFinishing())) {
                android.app.Dialog v1 = new com.android.vending.billing.InAppBillingService.LUCK.AlertDlg(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.getContext()).setTitle(p5).setMessage(p6).setIcon(2130837554).setPositiveButton(p7, p8).setNegativeButton(p9, p10).setOnCancelListener(p11).create();
                com.chelpus.Utils.showDialog(v1);
                v1.findViewById(16908299);
            }
        } catch (Exception v2) {
            v2.printStackTrace();
        }
        return;
    }

    public static void showDialogYesNo(String p5, String p6, android.content.DialogInterface$OnClickListener p7, android.content.DialogInterface$OnClickListener p8, android.content.DialogInterface$OnCancelListener p9)
    {
        try {
            if ((com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patchAct != null) && (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patchAct.isFinishing())) {
                android.app.Dialog v1 = new com.android.vending.billing.InAppBillingService.LUCK.AlertDlg(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.getContext()).setTitle(p5).setMessage(p6).setIcon(2130837554).setPositiveButton(com.chelpus.Utils.getText(2131165187), p7).setNegativeButton(com.chelpus.Utils.getText(2131165563), p8).setOnCancelListener(p9).create();
                com.chelpus.Utils.showDialog(v1);
                v1.findViewById(16908299);
            }
        } catch (Exception v2) {
            v2.printStackTrace();
        }
        return;
    }

    public static void showMessage(android.app.Activity p1, String p2, String p3)
    {
        p1.runOnUiThread(new com.chelpus.Utils$1(p1, p2, p3));
        return;
    }

    public static void showSystemWindow(String p11, String p12, android.view.View$OnClickListener p13, android.view.View$OnClickListener p14)
    {
        android.view.WindowManager v1_1 = ((android.view.WindowManager) com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance().getSystemService("window"));
        android.view.WindowManager$LayoutParams v0_1 = new android.view.WindowManager$LayoutParams();
        v0_1.gravity = 17;
        v0_1.type = 2003;
        v0_1.width = -2;
        v0_1.height = -2;
        v0_1.alpha = 1065353216;
        v0_1.packageName = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance().getPackageName();
        v0_1.buttonBrightness = 1065353216;
        v0_1.windowAnimations = 16973826;
        android.view.View v5 = android.view.View.inflate(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance(), 2130968634, 0);
        android.widget.Button v6_1 = ((android.widget.Button) v5.findViewById(2131558407));
        android.widget.Button v3_1 = ((android.widget.Button) v5.findViewById(2131558408));
        android.widget.TextView v2_1 = ((android.widget.TextView) v5.findViewById(2131558624));
        ((android.widget.TextView) v5.findViewById(2131558481)).setText(p11);
        v2_1.setText(p12);
        v6_1.setOnClickListener(p13);
        v3_1.setOnClickListener(p14);
        v6_1.setOnKeyListener(new com.chelpus.Utils$11(v1_1));
        v1_1.addView(v5, v0_1);
        return;
    }

    public static void startRootJava()
    {
        System.out.println("SU Java-Code Running!");
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.startUnderRoot = Boolean.valueOf(1);
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.toolfilesdir = "";
        new java.io.File("/data/lp");
        java.io.File v4_1 = new java.io.File("/data/lp/lp_utils");
        if (v4_1.exists()) {
            String[] v2 = com.chelpus.Utils.read_from_file(v4_1).split("%chelpus%");
            if ((v2 != null) && (v2.length > 0)) {
                int v1 = 0;
                while (v1 < v2.length) {
                    switch (v1) {
                        case 0:
                            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.toolfilesdir = v2[v1];
                            break;
                        case 1:
                            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.api = Integer.parseInt(v2[v1]);
                        case 2:
                            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.runtime = v2[v1];
                            break;
                    }
                    v1++;
                }
            }
        }
        return;
    }

    public static void startRootJava(Object p8)
    {
        if (p8 != null) {
            System.out.println(new StringBuilder().append("SU Java-Code Running! ").append(p8.getClass().getEnclosingClass().getName()).toString());
        }
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.startUnderRoot = Boolean.valueOf(1);
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.toolfilesdir = "";
        new java.io.File("/data/lp");
        java.io.File v4_1 = new java.io.File("/data/lp/lp_utils");
        if (v4_1.exists()) {
            String[] v2 = com.chelpus.Utils.read_from_file(v4_1).split("%chelpus%");
            if ((v2 != null) && (v2.length > 0)) {
                int v1 = 0;
                while (v1 < v2.length) {
                    switch (v1) {
                        case 0:
                            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.toolfilesdir = v2[v1];
                            break;
                        case 1:
                            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.api = Integer.parseInt(v2[v1]);
                        case 2:
                            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.runtime = v2[v1];
                            break;
                    }
                    v1++;
                }
            }
            if (p8 != null) {
                System.out.println(new StringBuilder().append("tools read:").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.toolfilesdir).append("/busybox").toString());
            }
            if (new java.io.File(new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.toolfilesdir).append("/busybox").toString()).exists()) {
                if (p8 == null) {
                    if (p8 != null) {
                        System.out.println("Lucky AppManager not found busybox util.");
                    }
                } else {
                    System.out.println("Lucky AppManager found utils.");
                }
            }
        } else {
            if (p8 != null) {
                System.out.println("Lucky Patcher not found utils.");
            }
        }
        return;
    }

    public static final void turn_off_patch_on_boot(String p5)
    {
        String v0_0 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().getString("patch_dalvik_on_boot_patterns", "");
        if (p5.contains("patch1")) {
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().edit().putString("patch_dalvik_on_boot_patterns", v0_0.replaceAll("patch1", "")).commit();
        }
        String v0_1 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().getString("patch_dalvik_on_boot_patterns", "");
        if (p5.contains("patch2")) {
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().edit().putString("patch_dalvik_on_boot_patterns", v0_1.replaceAll("patch2", "")).commit();
        }
        String v0_2 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().getString("patch_dalvik_on_boot_patterns", "");
        if (p5.contains("patch3")) {
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().edit().putString("patch_dalvik_on_boot_patterns", v0_2.replaceAll("patch3", "")).commit();
        }
        return;
    }

    public static final void turn_off_patch_on_boot_all()
    {
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().edit().putString("patch_dalvik_on_boot_patterns", "").commit();
        return;
    }

    public static final void turn_on_patch_on_boot(String p6)
    {
        String v0_0 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().getString("patch_dalvik_on_boot_patterns", "");
        if (p6.contains("patch1")) {
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().edit().putString("patch_dalvik_on_boot_patterns", new StringBuilder().append(v0_0.replaceAll("patch1", "")).append("patch1").toString()).commit();
        }
        String v0_1 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().getString("patch_dalvik_on_boot_patterns", "");
        if (p6.contains("patch2")) {
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().edit().putString("patch_dalvik_on_boot_patterns", new StringBuilder().append(v0_1.replaceAll("patch2", "")).append("patch2").toString()).commit();
        }
        String v0_2 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().getString("patch_dalvik_on_boot_patterns", "");
        if (p6.contains("patch3")) {
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().edit().putString("patch_dalvik_on_boot_patterns", new StringBuilder().append(v0_2.replaceAll("patch3", "")).append("patch3").toString()).commit();
        }
        return;
    }

    public static void verify_and_run(String p6, String p7)
    {
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.errorOutput = "";
        com.chelpus.Utils v0_2 = new com.chelpus.Utils("");
        String[] v1_1 = new String[1];
        v1_1[0] = new StringBuilder().append(p6).append(" ").append(p7).toString();
        v0_2.cmdRoot(v1_1);
        if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.errorOutput.equals("")) {
            com.chelpus.Utils v0_6 = new com.chelpus.Utils("");
            String[] v1_4 = new String[1];
            v1_4[0] = new StringBuilder().append("busybox ").append(p6).append(" ").append(p7).toString();
            v0_6.cmdRoot(v1_4);
        }
        if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.errorOutput.equals("")) {
            com.chelpus.Utils v0_10 = new com.chelpus.Utils("");
            String[] v1_7 = new String[1];
            v1_7[0] = new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.toolfilesdir).append("/busybox ").append(p6).append(" ").append(p7).toString();
            v0_10.cmdRoot(v1_7);
        }
        return;
    }

    public static void verify_bind_and_run(String p9, String p10, String p11, String p12)
    {
        if (!p12.trim().startsWith("~chelpus_disabled~")) {
            String v0;
            String v11_1 = p11.trim();
            String v12_1 = p12.trim();
            if (v11_1.endsWith("/")) {
                v0 = v11_1;
            } else {
                v0 = new StringBuilder().append(v11_1.trim()).append("/").toString();
            }
            String v2;
            if (v12_1.endsWith("/")) {
                v2 = v12_1;
            } else {
                v2 = new StringBuilder().append(v12_1.trim()).append("/").toString();
            }
            new java.io.File(v12_1).mkdirs();
            new java.io.File(v11_1).mkdirs();
            if (!new java.io.File(v12_1).exists()) {
                com.chelpus.Utils.verify_and_run("mkdir", new StringBuilder().append("-p \'").append(v12_1).append("\'").toString());
            }
            if (!new java.io.File(v11_1).exists()) {
                com.chelpus.Utils.verify_and_run("mkdir", new StringBuilder().append("-p \'").append(v11_1).append("\'").toString());
            }
            try {
                new java.io.File(new StringBuilder().append(v0).append("test.txt").toString()).createNewFile();
            } catch (java.io.IOException v1_0) {
                v1_0.printStackTrace();
            }
            com.chelpus.Utils.run_all(new StringBuilder().append("echo \'\' >\'").append(v0).append("test.txt\'").toString());
            if (com.chelpus.Utils.exists(new StringBuilder().append(v2).append("test.txt").toString())) {
                System.out.println(new StringBuilder().append("LuckyPatcher(Binder): ").append(v12_1).append(" exists!").toString());
            } else {
                com.chelpus.Utils.run_all(new StringBuilder().append("umount \'").append(v12_1).append("\'").toString());
                System.out.println(new StringBuilder().append("data: ").append(v0).append("test.txt").toString());
                System.out.println(new StringBuilder().append("target: ").append(v2).append("test.txt").toString());
                try {
                    new java.io.File(new StringBuilder().append(v0).append("test.txt").toString()).createNewFile();
                } catch (java.io.IOException v1_1) {
                    v1_1.printStackTrace();
                }
                com.chelpus.Utils.run_all(new StringBuilder().append("echo \'\' >\'").append(v0).append("test.txt\'").toString());
                if (!com.chelpus.Utils.exists(new StringBuilder().append(v2).append("test.txt").toString())) {
                    java.io.PrintStream v3_64 = new com.chelpus.Utils("");
                    String v4_54 = new String[1];
                    v4_54[0] = new StringBuilder().append("busybox ").append(p9).append(" ").append(p10).toString();
                    v3_64.cmdRoot(v4_54);
                    try {
                        new java.io.File(new StringBuilder().append(v0).append("test.txt").toString()).createNewFile();
                    } catch (java.io.IOException v1_2) {
                        v1_2.printStackTrace();
                    }
                    com.chelpus.Utils.run_all(new StringBuilder().append("echo \'\' >\'").append(v0).append("test.txt\'").toString());
                    if (!com.chelpus.Utils.exists(new StringBuilder().append(v2).append("test.txt").toString())) {
                        java.io.PrintStream v3_80 = new com.chelpus.Utils("");
                        String v4_64 = new String[1];
                        v4_64[0] = new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.toolfilesdir).append("/busybox ").append(p9).append(" ").append(p10).toString();
                        v3_80.cmdRoot(v4_64);
                        java.io.PrintStream v3_82 = new com.chelpus.Utils("");
                        String v4_66 = new String[1];
                        v4_66[0] = new StringBuilder().append("busybox ").append(p9).append(" ").append(p10).toString();
                        v3_82.cmdRoot(v4_66);
                        try {
                            new java.io.File(new StringBuilder().append(v0).append("test.txt").toString()).createNewFile();
                        } catch (java.io.IOException v1_3) {
                            v1_3.printStackTrace();
                        }
                        com.chelpus.Utils.run_all(new StringBuilder().append("echo \'\' >\'").append(v0).append("test.txt\'").toString());
                        if (!com.chelpus.Utils.exists(new StringBuilder().append(v2).append("test.txt").toString())) {
                            System.out.println("LuckyPatcher(Binder error): bind not created!");
                        }
                    }
                }
            }
            if (com.chelpus.Utils.exists(new StringBuilder().append(v0).append("test.txt").toString())) {
                if (!com.chelpus.Utils.exists(new StringBuilder().append(v2).append("test.txt").toString())) {
                    System.out.println(new StringBuilder().append("LuckyPatcher(Binder error): ").append(v12_1).append(" not binded!").toString());
                } else {
                    System.out.println(new StringBuilder().append("LuckyPatcher(Binder): ").append(v12_1).append(" binded!").toString());
                }
                new java.io.File(new StringBuilder().append(v0).append("test.txt").toString()).delete();
                if (com.chelpus.Utils.exists(new StringBuilder().append(v0).append("test.txt").toString())) {
                    com.chelpus.Utils.run_all(new StringBuilder().append("rm \'").append(v0).append("test.txt\'").toString());
                }
            }
        }
        return;
    }

    public static void zip(String p16, String p17, java.util.ArrayList p18, String p19)
    {
        try {
            java.util.zip.ZipOutputStream v11_1 = new java.util.zip.ZipOutputStream(new java.io.BufferedOutputStream(new java.io.FileOutputStream(p19, 0)));
            byte[] v4 = new byte[4096];
            int v12_3 = p18.iterator();
        } catch (Exception v6) {
            v6.printStackTrace();
            new java.io.File(new StringBuilder().append(p17).append("/AndroidManifest.xml").toString()).delete();
            int v12_15 = p18.iterator();
            while (v12_15.hasNext()) {
                new java.io.File(new StringBuilder().append(p17).append("/").append(((java.io.File) v12_15.next()).getName()).toString()).delete();
            }
            return;
        }
        try {
            while (v12_3.hasNext()) {
                java.io.File v2_1 = ((java.io.File) v12_3.next());
                java.io.FileInputStream v8_3 = new java.io.FileInputStream(new StringBuilder().append(p17).append("/").append(v2_1.getName()).toString());
                java.io.BufferedInputStream v9_4 = new java.io.BufferedInputStream(v8_3, 4096);
                v11_1.putNextEntry(new java.util.zip.ZipEntry(v2_1.getName()));
                while(true) {
                    int v3_1 = v9_4.read(v4, 0, 4096);
                    if (v3_1 == -1) {
                        break;
                    }
                    v11_1.write(v4, 0, v3_1);
                }
                v9_4.close();
                v11_1.closeEntry();
                v8_3.close();
            }
        } catch (Exception v6) {
        }
        java.io.FileInputStream v8_1 = new java.io.FileInputStream(new StringBuilder().append(p17).append("/AndroidManifest.xml").toString());
        java.io.BufferedInputStream v9_2 = new java.io.BufferedInputStream(v8_1, 4096);
        v11_1.putNextEntry(new java.util.zip.ZipEntry("AndroidManifest.xml"));
        while(true) {
            int v3_0 = v9_2.read(v4, 0, 4096);
            if (v3_0 == -1) {
                break;
            }
            v11_1.write(v4, 0, v3_0);
        }
        v9_2.close();
        v11_1.closeEntry();
        v11_1.close();
        v8_1.close();
    }

    public static String zipART(String p24, String p25, java.util.ArrayList p26, String p27)
    {
        String v5 = "";
        String v6 = "";
        java.util.zip.ZipOutputStream v0_1 = new String[3];
        java.io.PrintStream v20_1 = v0_1;
        v20_1[0] = "chmod";
        v20_1[1] = "777";
        v20_1[2] = "/data/tmp";
        com.chelpus.Utils.run_all_no_root(v20_1);
        try {
            System.out.println("Try create tmp.");
            new java.io.File("/data/tmp").mkdir();
            java.io.PrintStream v20_5 = new java.io.File;
            v20_5("/data/tmp");
        } catch (Exception v11_1) {
            v11_1.printStackTrace();
            java.io.PrintStream v20_60 = new java.io.File;
            v20_60(v5);
            if (!v20_60.exists()) {
                new java.io.File(new StringBuilder().append(p25).append("/AndroidManifest.xml").toString()).delete();
                java.io.PrintStream v20_65 = p26.iterator();
                while (v20_65.hasNext()) {
                    new java.io.File(new StringBuilder().append(p25).append("/").append(((java.io.File) v20_65.next()).getName()).toString()).delete();
                }
                java.io.PrintStream v20_66 = new java.io.File;
                v20_66(v5);
                if (!v20_66.exists()) {
                    java.io.PrintStream v20_71 = new java.io.File;
                    v20_71(v5);
                    if (v20_71.exists()) {
                        java.io.PrintStream v20_73 = new java.io.File;
                        v20_73(v5);
                        v20_73.delete();
                    }
                    java.io.PrintStream v20_74 = new java.io.File;
                    v20_74(v6);
                    if ((v20_74.exists()) && (v6.contains("1"))) {
                        java.io.PrintStream v20_78 = new java.io.File;
                        v20_78(v6);
                        v20_78.delete();
                    }
                    v5 = "";
                } else {
                    java.io.PrintStream v20_68 = new java.io.File;
                    v20_68(v5);
                    if (v20_68.length() == 0) {
                    }
                }
                return v5;
            } else {
                java.io.PrintStream v20_62 = new java.io.File;
                v20_62(v5);
                v20_62.delete();
            }
        }
        if (v20_5.exists()) {
            java.util.zip.ZipOutputStream v0_4 = new String[3];
            java.io.PrintStream v20_8 = v0_4;
            v20_8[0] = "chmod";
            v20_8[1] = "777";
            v20_8[2] = "/data/tmp";
            com.chelpus.Utils.run_all_no_root(v20_8);
            try {
                java.io.File[] v15 = new java.io.File("/data/tmp").listFiles();
            } catch (Exception v11_0) {
                v11_0.printStackTrace();
            }
            if (v15 != null) {
                int v21_9 = v15.length;
                java.io.PrintStream v20_11 = 0;
                while (v20_11 < v21_9) {
                    java.io.File v13 = v15[v20_11];
                    com.chelpus.Utils v19 = new com.chelpus.Utils;
                    v19("1");
                    v13.delete();
                    if (v13.exists()) {
                        v19.deleteFolder(v13);
                    }
                    v20_11++;
                }
            }
        } else {
            System.out.println("tmp dir not found. Try create with root.");
            java.io.PrintStream v20_13 = new java.io.File;
            v20_13("/data/tmp");
            v20_13.mkdirs();
            java.io.PrintStream v20_14 = new java.io.File;
            v20_14("/data/tmp");
            if (!v20_14.exists()) {
                java.util.zip.ZipOutputStream v0_12 = new String[2];
                java.io.PrintStream v20_17 = v0_12;
                v20_17[0] = "mkdir";
                v20_17[1] = "/data/tmp";
                com.chelpus.Utils.run_all_no_root(v20_17);
            }
            java.io.PrintStream v20_18 = new java.io.File;
            v20_18("/data/tmp");
            if (v20_18.exists()) {
                java.util.zip.ZipOutputStream v0_15 = new String[3];
                java.io.PrintStream v20_21 = v0_15;
                v20_21[0] = "chmod";
                v20_21[1] = "777";
                v20_21[2] = "/data/tmp";
                com.chelpus.Utils.run_all_no_root(v20_21);
            } else {
                System.out.println("tmp dir not created.");
            }
        }
        if ((p27.length() > new StringBuilder().append("/data/tmp/").append(p24).toString().length()) && ((p27.length() - new StringBuilder().append("/data/tmp/").append(p24).toString().length()) > 1)) {
            char[] v10 = new char[((p27.length() - new StringBuilder().append("/data/tmp/").append(p24).toString().length()) - 1)];
            int v16 = 0;
            while (v16 < v10.length) {
                v10[v16] = 49;
                v16++;
            }
            java.io.PrintStream v20_32 = new StringBuilder().append("/data/tmp/");
            int v21_37 = new String;
            v21_37(v10);
            v5 = v20_32.append(v21_37).append("/").append(p24).toString();
            java.io.PrintStream v20_38 = new StringBuilder().append("/data/tmp/");
            int v21_40 = new String;
            v21_40(v10);
            v6 = v20_38.append(v21_40).toString();
            int v21_43 = new StringBuilder().append("/data/tmp/");
            String v22_16 = new String;
            v22_16(v10);
            new java.io.File(v21_43.append(v22_16).toString()).mkdirs();
            int v21_48 = new StringBuilder().append("/data/tmp/");
            String v22_18 = new String;
            v22_18(v10);
            if (new java.io.File(v21_48.append(v22_18).toString()).exists()) {
                System.out.println("Dir delta created.");
            }
            System.out.println(new StringBuilder().append("Path to create zip: ").append(v5).toString());
            System.out.println(new StringBuilder().append("Path to origin zip: ").append(p27).toString());
        }
        if (p27.length() == new StringBuilder().append("/data/tmp/").append(p24).toString().length()) {
            v5 = new StringBuilder().append("/data/tmp/").append(p24).toString();
            v6 = "";
        }
        java.io.PrintStream v20_53 = new java.io.File;
        v20_53(v5);
        if (v20_53.exists()) {
            java.io.PrintStream v20_55 = new java.io.File;
            v20_55(v5);
            v20_55.delete();
        }
        java.util.zip.ZipOutputStream v18 = new java.util.zip.ZipOutputStream;
        java.io.PrintStream v20_57 = new java.io.BufferedOutputStream;
        v20_57(new java.io.FileOutputStream(v5, 0));
        v18(v20_57);
        byte[] v7 = new byte[4096];
        java.io.PrintStream v20_58 = p26.iterator();
        while (v20_58.hasNext()) {
            java.io.File v3_1 = ((java.io.File) v20_58.next());
            java.io.FileInputStream v14_1 = new java.io.FileInputStream(new StringBuilder().append(p25).append("/").append(v3_1.getName()).toString());
            java.io.BufferedInputStream v17 = new java.io.BufferedInputStream;
            v17(v14_1, 4096);
            v18.putNextEntry(new java.util.zip.ZipEntry(v3_1.getName()));
            while(true) {
                int v4 = v17.read(v7, 0, 4096);
                if (v4 == -1) {
                    break;
                }
                v18.write(v7, 0, v4);
            }
            v17.close();
            v18.closeEntry();
            v14_1.close();
        }
        v18.close();
    }

    public varargs String cmdRoot(String[] p9)
    {
        int v5_0 = 0;
        try {
            if (com.chelpus.Utils.onMainThread()) {
                throw new com.android.vending.billing.InAppBillingService.LUCK.ShellOnMainThreadException("Application attempted to run a shell command from the main thread");
            }
            try {
                if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.startUnderRoot.booleanValue()) {
                    if ((com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.su) || (p9[0].equals("checkRoot"))) {
                        com.chelpus.Utils$Worker v4_1 = new com.chelpus.Utils$Worker(this, 0);
                        com.chelpus.Utils$Worker.access$102(v4_1, p9);
                        v4_1.start();
                        int v3 = 0;
                        try {
                        } catch (int v5_4) {
                            throw v5_4;
                        } catch (InterruptedException v2) {
                            v2.printStackTrace();
                            v4_1.interrupt();
                            Thread.currentThread().interrupt();
                            com.chelpus.Utils.exitRoot();
                            int v5_3 = com.chelpus.Utils$Worker.access$300(v4_1);
                        }
                        while (v5_0 < p9.length) {
                            String v0 = p9[v5_0];
                            if ((v0.contains("-Xbootclasspath:")) || ((v0.contains("dd ")) || ((v0.contains("cp ")) || ((v0.contains("cat ")) || (v0.contains("pm ")))))) {
                                v3 = 1;
                            }
                            v5_0++;
                        }
                        if (v3 == 0) {
                            v4_1.join();
                            // Both branches of the condition point to the same code.
                            // if (com.chelpus.Utils$Worker.access$200(v4_1) != null) {
                            // }
                        } else {
                            v4_1.join();
                        }
                    } else {
                        v5_3 = "lucky patcher root not found!";
                    }
                    return v5_3;
                } else {
                    throw new com.android.vending.billing.InAppBillingService.LUCK.ShellOnMainThreadException("Application attempted to run a shell command from the main thread");
                }
            } catch (com.android.vending.billing.InAppBillingService.LUCK.ShellOnMainThreadException v1_1) {
                v1_1.printStackTrace();
            }
        } catch (com.android.vending.billing.InAppBillingService.LUCK.ShellOnMainThreadException v1_0) {
            v1_0.printStackTrace();
        }
    }

    public void deleteFolder(java.io.File p5)
    {
        if (p5.exists()) {
            if (p5.isDirectory()) {
                String v2_0 = p5.listFiles();
                int v3 = v2_0.length;
                int v1_2 = 0;
                while (v1_2 < v3) {
                    this.deleteFolder(v2_0[v1_2]);
                    v1_2++;
                }
            }
            new java.io.File(p5.toString()).delete();
        }
        return;
    }

    public String findFile(java.io.File p7, String p8)
    {
        String v1;
        if (p7.exists()) {
            if (p7.isDirectory()) {
                java.io.File[] v3 = p7.listFiles();
                int v4 = v3.length;
                int v2_2 = 0;
                while (v2_2 < v4) {
                    v1 = this.findFile(v3[v2_2], p8);
                    if (!v1.equals("")) {
                        return v1;
                    } else {
                        v2_2++;
                    }
                }
            }
            if (!p7.getName().equals(p8)) {
                v1 = "";
            } else {
                v1 = p7.getAbsolutePath();
            }
        } else {
            v1 = "";
        }
        return v1;
    }

    public String findFileContainText(java.io.File p7, String p8)
    {
        String v1;
        if (!p7.isDirectory()) {
            if (!p7.getName().contains(p8)) {
                v1 = "";
            } else {
                v1 = p7.getAbsolutePath();
            }
        } else {
            java.io.File[] v3 = p7.listFiles();
            int v4 = v3.length;
            int v2_1 = 0;
            while (v2_1 < v4) {
                v1 = this.findFileContainText(v3[v2_1], p8);
                if (v1.equals("")) {
                    v2_1++;
                }
            }
        }
        return v1;
    }

    public String findFileEndText(java.io.File p7, String p8, java.util.ArrayList p9)
    {
        int v2_5;
        if (p7.exists()) {
            if (p7.isDirectory()) {
                java.io.File[] v3 = p7.listFiles();
                int v4 = v3.length;
                int v2_2 = 0;
                while (v2_2 < v4) {
                    String v1 = this.findFileEndText(v3[v2_2], p8, p9);
                    if (!v1.equals("")) {
                        com.chelpus.Utils.addFileToList(new java.io.File(v1), p9);
                    }
                    v2_2++;
                }
            }
            if (!p7.getName().endsWith(p8)) {
                v2_5 = "";
            } else {
                com.chelpus.Utils.addFileToList(p7, p9);
                v2_5 = p7.getAbsolutePath();
            }
        } else {
            v2_5 = "";
        }
        return v2_5;
    }

    public String getInput(boolean p10, com.chelpus.Utils$Worker p11)
    {
        java.util.Timer v5_1 = new java.util.Timer();
        com.chelpus.Utils$RootTimerTask v4_1 = new com.chelpus.Utils$RootTimerTask(this, p11);
        if (!com.chelpus.Utils$Worker.access$100(p11)[0].contains(".corepatch ")) {
            v5_1.schedule(v4_1, 600000);
            System.out.println("Input a string within 3 minuten: ");
        }
        String v3 = "";
        try {
            p11.input = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.suInputStream;
        } catch (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment v6_41) {
            throw v6_41;
        } catch (Exception v0) {
            v0.printStackTrace();
            v5_1.cancel();
            System.out.println(new StringBuilder().append("you have entered: ").append(v3).toString());
            return v3;
        }
        while(true) {
            String v1 = com.chelpus.Utils.readLine(p11.input);
            if (v1 == null) {
                break;
            }
            if ((p10) && ((com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.progress_loading != null) && (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.progress_loading.isShowing()))) {
                if (v1.contains("Get classes.dex.")) {
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.runToMain(new com.chelpus.Utils$2(this));
                }
                if (v1.equals("String analysis.")) {
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.runToMain(new com.chelpus.Utils$3(this));
                }
                if (v1.equals("Parse data for patch.")) {
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.runToMain(new com.chelpus.Utils$4(this));
                }
                if (v1.startsWith("Progress size:")) {
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.runToMain(new com.chelpus.Utils$5(this, v1));
                }
                if (v1.startsWith("Size file:")) {
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.runToMain(new com.chelpus.Utils$6(this, v1));
                }
                if (v1.startsWith("Analise Results:")) {
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.runToMain(new com.chelpus.Utils$7(this));
                }
                if (v1.startsWith("Create ODEX:")) {
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.runToMain(new com.chelpus.Utils$8(this));
                }
                if (v1.startsWith("Optional Steps After Patch:")) {
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.runToMain(new com.chelpus.Utils$9(this));
                }
            }
            if (v1.contains("com.chelpus.root.utils.custompatch")) {
                System.out.println(v3);
                v3 = "";
            }
            if (v1.contains("chelpus done!")) {
                break;
            }
            if (!v1.contains("chelpusstart!")) {
                v3 = new StringBuilder().append(v3).append(v1).append("\n").toString();
            }
        }
        v5_1.cancel();
        System.out.println(new StringBuilder().append("you have entered: ").append(v3).toString());
        return v3;
    }

    public float sizeFolder(java.io.File p6, boolean p7)
    {
        if (p7) {
            this.folder_size = 0;
        }
        if (p6.isDirectory()) {
            String v3_0 = p6.listFiles();
            int v4 = v3_0.length;
            int v1_2 = 0;
            while (v1_2 < v4) {
                this.sizeFolder(v3_0[v1_2], 0);
                v1_2++;
            }
        }
        this.folder_size = (this.folder_size + ((float) new java.io.File(p6.toString()).length()));
        return (this.folder_size / 1233125376);
    }

    public void waitLP(long p5)
    {
        long v0 = (System.currentTimeMillis() + p5);
        while (System.currentTimeMillis() < v0) {
            try {
                this.wait((v0 - System.currentTimeMillis()));
            } catch (Exception v2_5) {
                throw v2_5;
            } catch (Exception v2) {
            }
        }
        return;
    }
}
