package com.chelpus.root.utils;
public class corepatch {
    public static final byte[] MAGIC;
    public static int adler;
    public static java.nio.MappedByteBuffer fileBytes;
    public static byte[] lastByteReplace;
    public static int lastPatchPosition;
    public static boolean not_found_bytes_for_patch;
    public static boolean onlyDalvik;
    public static String toolfilesdir;

    static corepatch()
    {
        String v0_1 = new byte[8];
        v0_1 = {100, 101, 121, 10, 48, 51, 53, 0};
        com.chelpus.root.utils.corepatch.MAGIC = v0_1;
        com.chelpus.root.utils.corepatch.toolfilesdir = "";
        com.chelpus.root.utils.corepatch.onlyDalvik = 0;
        com.chelpus.root.utils.corepatch.not_found_bytes_for_patch = 0;
        com.chelpus.root.utils.corepatch.fileBytes = 0;
        com.chelpus.root.utils.corepatch.lastPatchPosition = 0;
        com.chelpus.root.utils.corepatch.lastByteReplace = 0;
        return;
    }

    public corepatch()
    {
        return;
    }

    private static boolean applyPatch(int p6, byte p7, byte[] p8, byte[] p9, byte[] p10, byte[] p11, boolean p12)
    {
        java.nio.MappedByteBuffer v2_0 = 1;
        if ((p8 == null) || ((p7 != p8[0]) || (!p12))) {
            v2_0 = 0;
        } else {
            if (p11[0] == 0) {
                p10[0] = p7;
            }
            int v0 = 1;
            com.chelpus.root.utils.corepatch.fileBytes.position((p6 + 1));
            byte v1 = com.chelpus.root.utils.corepatch.fileBytes.get();
            while ((v1 == p8[v0]) || (p9[v0] == 1)) {
                if (p11[v0] == 0) {
                    p10[v0] = v1;
                }
                if (p11[v0] == 3) {
                    p10[v0] = ((byte) (v1 & 15));
                }
                if (p11[v0] == 2) {
                    p10[v0] = ((byte) ((v1 & 15) + ((v1 & 15) * 16)));
                }
                v0++;
                if (v0 != p8.length) {
                    v1 = com.chelpus.root.utils.corepatch.fileBytes.get();
                } else {
                    com.chelpus.root.utils.corepatch.fileBytes.position(p6);
                    com.chelpus.root.utils.corepatch.fileBytes.put(p10);
                    com.chelpus.root.utils.corepatch.fileBytes.force();
                }
            }
            com.chelpus.root.utils.corepatch.fileBytes.position((p6 + 1));
        }
        return v2_0;
    }

    private static boolean applyPatchCounter(int p6, byte p7, byte[] p8, byte[] p9, byte[] p10, byte[] p11, int p12, int p13, boolean p14)
    {
        java.nio.MappedByteBuffer v2_0 = 1;
        if ((p8 == null) || ((p7 != p8[0]) || (!p14))) {
            v2_0 = 0;
        } else {
            if (p11[0] == 0) {
                p10[0] = p7;
            }
            int v0 = 1;
            com.chelpus.root.utils.corepatch.fileBytes.position((p6 + 1));
            byte v1 = com.chelpus.root.utils.corepatch.fileBytes.get();
            while ((v1 == p8[v0]) || (p9[v0] == 1)) {
                if (p11[v0] == 0) {
                    p10[v0] = v1;
                }
                if (p11[v0] == 3) {
                    p10[v0] = ((byte) (v1 & 15));
                }
                if (p11[v0] == 2) {
                    p10[v0] = ((byte) ((v1 & 15) + ((v1 & 15) * 16)));
                }
                v0++;
                if (v0 != p8.length) {
                    v1 = com.chelpus.root.utils.corepatch.fileBytes.get();
                } else {
                    if (p12 >= p13) {
                        com.chelpus.root.utils.corepatch.lastPatchPosition = p6;
                        byte[] v4_18 = new byte[p10.length];
                        com.chelpus.root.utils.corepatch.lastByteReplace = v4_18;
                        System.arraycopy(p10, 0, com.chelpus.root.utils.corepatch.lastByteReplace, 0, p10.length);
                    }
                }
            }
            com.chelpus.root.utils.corepatch.fileBytes.position((p6 + 1));
        }
        return v2_0;
    }

    public static void main(String[] p362)
    {
        com.chelpus.Utils.startRootJava(new com.chelpus.root.utils.corepatch$1());
        int v184 = 0;
        int v33_0 = 0;
        int v81_0 = 0;
        int v105_0 = 0;
        byte[] v212 = 0;
        byte[] v294 = 0;
        byte[] v242 = 0;
        byte[] v340 = 0;
        byte[] v5 = 0;
        byte[] v6 = 0;
        byte[] v7 = 0;
        byte[] v8 = 0;
        byte[] v11 = 0;
        byte[] v12 = 0;
        byte[] v13 = 0;
        byte[] v14 = 0;
        byte[] v17 = 0;
        byte[] v18 = 0;
        byte[] v19 = 0;
        byte[] v20 = 0;
        byte[] v23 = 0;
        byte[] v24 = 0;
        byte[] v25 = 0;
        byte[] v26 = 0;
        byte[] v29 = 0;
        byte[] v30 = 0;
        byte[] v31 = 0;
        byte[] v32 = 0;
        byte[] v35 = 0;
        byte[] v36 = 0;
        byte[] v37 = 0;
        byte[] v38 = 0;
        byte[] v41 = 0;
        byte[] v42 = 0;
        byte[] v43 = 0;
        byte[] v44 = 0;
        byte[] v65 = 0;
        byte[] v66 = 0;
        byte[] v67 = 0;
        byte[] v68 = 0;
        byte[] v47 = 0;
        byte[] v48 = 0;
        byte[] v49 = 0;
        byte[] v50 = 0;
        byte[] v53 = 0;
        byte[] v54 = 0;
        byte[] v55 = 0;
        byte[] v56 = 0;
        byte[] v59 = 0;
        byte[] v60 = 0;
        byte[] v61 = 0;
        byte[] v62 = 0;
        byte[] v71 = 0;
        byte[] v72 = 0;
        byte[] v73 = 0;
        byte[] v74 = 0;
        byte[] v77 = 0;
        byte[] v78 = 0;
        byte[] v79 = 0;
        byte[] v80 = 0;
        byte[] v83 = 0;
        byte[] v84 = 0;
        byte[] v85 = 0;
        byte[] v86 = 0;
        byte[] v89 = 0;
        byte[] v90 = 0;
        byte[] v91 = 0;
        byte[] v92 = 0;
        byte[] v125 = 0;
        byte[] v126 = 0;
        byte[] v127 = 0;
        byte[] v128 = 0;
        byte[] v95 = 0;
        byte[] v96 = 0;
        byte[] v97 = 0;
        byte[] v98 = 0;
        byte[] v101 = 0;
        byte[] v102 = 0;
        byte[] v103 = 0;
        byte[] v104 = 0;
        byte[] v107 = 0;
        byte[] v108 = 0;
        byte[] v109 = 0;
        byte[] v110 = 0;
        byte[] v113 = 0;
        byte[] v114 = 0;
        byte[] v115 = 0;
        byte[] v116 = 0;
        byte[] v119 = 0;
        byte[] v120 = 0;
        byte[] v121 = 0;
        byte[] v122 = 0;
        byte[] v131 = 0;
        byte[] v132 = 0;
        byte[] v133 = 0;
        byte[] v134 = 0;
        byte[] v137 = 0;
        byte[] v138 = 0;
        byte[] v139 = 0;
        byte[] v140 = 0;
        byte[] v143 = 0;
        byte[] v144 = 0;
        byte[] v145 = 0;
        byte[] v146 = 0;
        byte[] v149 = 0;
        byte[] v150 = 0;
        byte[] v151 = 0;
        byte[] v152 = 0;
        byte[] v155 = 0;
        byte[] v156 = 0;
        byte[] v157 = 0;
        byte[] v158 = 0;
        byte[] v161 = 0;
        byte[] v162 = 0;
        byte[] v163 = 0;
        byte[] v164 = 0;
        byte[] v225 = 0;
        byte[] v306 = 0;
        byte[] v255 = 0;
        byte[] v352 = 0;
        byte[] v226 = 0;
        byte[] v307 = 0;
        byte[] v256 = 0;
        byte[] v353 = 0;
        byte[] v227 = 0;
        byte[] v308 = 0;
        byte[] v257 = 0;
        byte[] v354 = 0;
        byte[] v228 = 0;
        byte[] v309 = 0;
        byte[] v258 = 0;
        byte[] v355 = 0;
        byte[] v229 = 0;
        byte[] v310 = 0;
        byte[] v259 = 0;
        byte[] v356 = 0;
        byte[] v230 = 0;
        byte[] v311 = 0;
        byte[] v260 = 0;
        byte[] v357 = 0;
        byte[] v231 = 0;
        byte[] v312 = 0;
        byte[] v261 = 0;
        byte[] v358 = 0;
        int v192 = 0;
        byte[] v195 = 0;
        byte[] v196 = 0;
        byte[] v197 = 0;
        byte[] v198 = 0;
        int v200 = 0;
        byte[] v222 = 0;
        byte[] v303 = 0;
        byte[] v252 = 0;
        byte[] v349 = 0;
        byte[] v223 = 0;
        byte[] v304 = 0;
        byte[] v253 = 0;
        byte[] v350 = 0;
        byte[] v224 = 0;
        byte[] v305 = 0;
        byte[] v254 = 0;
        byte[] v351 = 0;
        byte[] v167 = 0;
        byte[] v168 = 0;
        byte[] v169 = 0;
        byte[] v170 = 0;
        byte[] v173 = 0;
        byte[] v174 = 0;
        byte[] v175 = 0;
        byte[] v176 = 0;
        byte[] v215 = 0;
        byte[] v245 = 0;
        byte[] v216 = 0;
        byte[] v297 = 0;
        byte[] v246 = 0;
        byte[] v343 = 0;
        byte[] v217 = 0;
        byte[] v298 = 0;
        byte[] v247 = 0;
        byte[] v344 = 0;
        byte[] v218 = 0;
        byte[] v299 = 0;
        byte[] v248 = 0;
        byte[] v345 = 0;
        byte[] v219 = 0;
        byte[] v300 = 0;
        byte[] v249 = 0;
        byte[] v346 = 0;
        byte[] v220 = 0;
        byte[] v301 = 0;
        byte[] v250 = 0;
        byte[] v347 = 0;
        byte[] v221 = 0;
        byte[] v302 = 0;
        byte[] v251 = 0;
        byte[] v348 = 0;
        byte[] v202 = 0;
        byte[] v284 = 0;
        byte[] v232 = 0;
        byte[] v330 = 0;
        byte[] v203 = 0;
        byte[] v285 = 0;
        byte[] v233 = 0;
        byte[] v331 = 0;
        byte[] v204 = 0;
        byte[] v286 = 0;
        byte[] v234 = 0;
        byte[] v332 = 0;
        byte[] v205 = 0;
        byte[] v287 = 0;
        byte[] v235 = 0;
        byte[] v333 = 0;
        byte[] v206 = 0;
        byte[] v288 = 0;
        byte[] v236 = 0;
        byte[] v334 = 0;
        byte[] v207 = 0;
        byte[] v289 = 0;
        byte[] v237 = 0;
        byte[] v335 = 0;
        byte[] v208 = 0;
        byte[] v290 = 0;
        byte[] v238 = 0;
        byte[] v336 = 0;
        byte[] v209 = 0;
        byte[] v291 = 0;
        byte[] v239 = 0;
        byte[] v337 = 0;
        byte[] v210 = 0;
        byte[] v292 = 0;
        byte[] v240 = 0;
        byte[] v338 = 0;
        byte[] v211 = 0;
        byte[] v293 = 0;
        byte[] v241 = 0;
        byte[] v339 = 0;
        byte[] v213 = 0;
        byte[] v295 = 0;
        byte[] v243 = 0;
        byte[] v341 = 0;
        byte[] v214 = 0;
        byte[] v296 = 0;
        byte[] v244 = 0;
        byte[] v342 = 0;
        try {
            System.out.println(p362[0]);
            System.out.println(p362[1]);
            System.out.println(p362[2]);
            System.out.println(p362[3]);
            System.out.println(p362[4]);
        } catch (int v0_71) {
            v0_71.printStackTrace();
            if (p362[3] != null) {
                com.chelpus.root.utils.corepatch.toolfilesdir = p362[3];
            }
            if ((!p362[4].equals("framework")) && (!p362[4].equals("OnlyDalvik"))) {
                com.chelpus.Utils.remount("/system", "rw");
            }
            if (((p362[1].contains("core.odex")) || ((p362[1].contains("core.jar")) || ((p362[1].contains("core-libart.jar")) || (p362[1].contains("boot.oat"))))) && (p362[4].equals("framework"))) {
                v184 = 1;
                v33_0 = 1;
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.startUnderRoot = Boolean.valueOf(0);
                p362[0] = "patch";
            }
            if (((p362[1].contains("services.jar")) || (p362[1].contains("services.odex"))) && (p362[4].equals("framework"))) {
                v81_0 = 1;
                v105_0 = 0;
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.startUnderRoot = Boolean.valueOf(0);
                p362[0] = "patch";
            }
            if (p362[0].contains("patch")) {
                if (p362[0].contains("_patch1")) {
                    v184 = 1;
                }
                int v326_0;
                if (!p362[0].contains("_patch2")) {
                    v326_0 = v33_0;
                } else {
                    v326_0 = 1;
                }
                int v327_0;
                if (!p362[0].contains("_patch3")) {
                    v327_0 = v81_0;
                } else {
                    v327_0 = 1;
                }
                int v328_0;
                if (!p362[0].contains("_patch4")) {
                    v328_0 = v105_0;
                } else {
                    v328_0 = 1;
                }
                v5 = new byte["11 90 11 99 01 29 0F D1 01 26 28 1C C0 68 39 1C".split("[ \t]+").length];
                v6 = new byte["11 90 11 99 01 29 0F D1 01 26 28 1C C0 68 39 1C".split("[ \t]+").length];
                v7 = new byte["?? ?? 01 21 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v8 = new byte["?? ?? 01 21 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                com.chelpus.Utils.convertStringToArraysPatch("11 90 11 99 01 29 0F D1 01 26 28 1C C0 68 39 1C", "?? ?? 01 21 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??", v5, v6, v7, v8);
                v11 = new byte["09 90 B0 42 0C D1 D5 F8 DC 0B 39 1C D0 F8 24 E0 F0 47".split("[ \t]+").length];
                v12 = new byte["09 90 B0 42 0C D1 D5 F8 DC 0B 39 1C D0 F8 24 E0 F0 47".split("[ \t]+").length];
                v13 = new byte["?? ?? 80 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v14 = new byte["?? ?? 80 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                com.chelpus.Utils.convertStringToArraysPatch("09 90 B0 42 0C D1 D5 F8 DC 0B 39 1C D0 F8 24 E0 F0 47", "?? ?? 80 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??", v11, v12, v13, v14);
                int v0_91 = new byte["39 1C 08 68 52 46 D0 F8 CC 01 D0 F8 ?? E0 F0 47 05 1C 01 3C 00 F0 04 80 28 1C 06 B0 BD E8 E0 85 D9 F8 ?? E2 F0 47 F7 E7".split("[ \t]+").length];
                v17 = v0_91;
                int v0_92 = new byte["39 1C 08 68 52 46 D0 F8 CC 01 D0 F8 ?? E0 F0 47 05 1C 01 3C 00 F0 04 80 28 1C 06 B0 BD E8 E0 85 D9 F8 ?? E2 F0 47 F7 E7".split("[ \t]+").length];
                v18 = v0_92;
                int v0_93 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 01 20 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v19 = v0_93;
                int v0_94 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 01 20 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v20 = v0_94;
                com.chelpus.Utils.convertStringToArraysPatch("39 1C 08 68 52 46 D0 F8 CC 01 D0 F8 ?? E0 F0 47 05 1C 01 3C 00 F0 04 80 28 1C 06 B0 BD E8 E0 85 D9 F8 ?? E2 F0 47 F7 E7", "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 01 20 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??", v17, v18, v19, v20);
                int v0_95 = new byte["08 68 3A 1C D0 F8 D0 01 43 46 D0 F8 ?? E0 F0 47 05 1C 01 3C 00 F0 04 80 28 1C 09 B0 BD E8 E0 8D D9 F8 ?? E2 F0 47 F7 E7".split("[ \t]+").length];
                v23 = v0_95;
                int v0_96 = new byte["08 68 3A 1C D0 F8 D0 01 43 46 D0 F8 ?? E0 F0 47 05 1C 01 3C 00 F0 04 80 28 1C 09 B0 BD E8 E0 8D D9 F8 ?? E2 F0 47 F7 E7".split("[ \t]+").length];
                v24 = v0_96;
                int v0_97 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 01 20 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v25 = v0_97;
                int v0_98 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 01 20 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v26 = v0_98;
                com.chelpus.Utils.convertStringToArraysPatch("08 68 3A 1C D0 F8 D0 01 43 46 D0 F8 ?? E0 F0 47 05 1C 01 3C 00 F0 04 80 28 1C 09 B0 BD E8 E0 8D D9 F8 ?? E2 F0 47 F7 E7", "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 01 20 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??", v23, v24, v25, v26);
                int v0_99 = new byte["56 45 00 F0 07 80 01 3C 00 F0 31 80 05 98".split("[ \t]+").length];
                v29 = v0_99;
                int v0_100 = new byte["56 45 00 F0 07 80 01 3C 00 F0 31 80 05 98".split("[ \t]+").length];
                v30 = v0_100;
                int v0_101 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 01 20".split("[ \t]+").length];
                v31 = v0_101;
                int v0_102 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 01 20".split("[ \t]+").length];
                v32 = v0_102;
                com.chelpus.Utils.convertStringToArraysPatch("56 45 00 F0 07 80 01 3C 00 F0 31 80 05 98", "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 01 20", v29, v30, v31, v32);
                int v0_103 = new byte["56 45 03 d0 00 20 09 b0 bd e8 e0 8d 00 27 00 25".split("[ \t]+").length];
                v35 = v0_103;
                int v0_104 = new byte["56 45 03 d0 00 20 09 b0 bd e8 e0 8d 00 27 00 25".split("[ \t]+").length];
                v36 = v0_104;
                int v0_105 = new byte["?? ?? 00 00 01 20 ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v37 = v0_105;
                int v0_106 = new byte["?? ?? 00 00 01 20 ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v38 = v0_106;
                com.chelpus.Utils.convertStringToArraysPatch("56 45 03 d0 00 20 09 b0 bd e8 e0 8d 00 27 00 25", "?? ?? 00 00 01 20 ?? ?? ?? ?? ?? ?? ?? ?? ?? ??", v35, v36, v37, v38);
                int v0_107 = new byte["89 44 24 40 8B 5C 24 40 83 FB 01 75 32 BE 01 00 00 00 8B C5 8B 40 0C 8B CF".split("[ \t]+").length];
                v41 = v0_107;
                int v0_108 = new byte["89 44 24 40 8B 5C 24 40 83 FB 01 75 32 BE 01 00 00 00 8B C5 8B 40 0C 8B CF".split("[ \t]+").length];
                v42 = v0_108;
                int v0_109 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 90 90 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v43 = v0_109;
                int v0_110 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 90 90 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v44 = v0_110;
                com.chelpus.Utils.convertStringToArraysPatch("89 44 24 40 8B 5C 24 40 83 FB 01 75 32 BE 01 00 00 00 8B C5 8B 40 0C 8B CF", "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 90 90 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??", v41, v42, v43, v44);
                int v0_111 = new byte["8B 54 24 38 8B CF 8B 01 8B 80 CC 01 00 00 FF 50 28 64 66 83 3D 00 00 00 00 00 8B E8 75 12 8B C5".split("[ \t]+").length];
                v47 = v0_111;
                int v0_112 = new byte["8B 54 24 38 8B CF 8B 01 8B 80 CC 01 00 00 FF 50 28 64 66 83 3D 00 00 00 00 00 8B E8 75 12 8B C5".split("[ \t]+").length];
                v48 = v0_112;
                int v0_113 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? B0 01".split("[ \t]+").length];
                v49 = v0_113;
                int v0_114 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? B0 01".split("[ \t]+").length];
                v50 = v0_114;
                com.chelpus.Utils.convertStringToArraysPatch("8B 54 24 38 8B CF 8B 01 8B 80 CC 01 00 00 FF 50 28 64 66 83 3D 00 00 00 00 00 8B E8 75 12 8B C5", "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? B0 01", v47, v48, v49, v50);
                int v0_115 = new byte["8B 80 D0 01 00 00 8B D7 FF 50 28 64 66 83 3D 00 00 00 00 00 8B E8 75 12 8B C5 8B 6C 24 20".split("[ \t]+").length];
                v53 = v0_115;
                int v0_116 = new byte["8B 80 D0 01 00 00 8B D7 FF 50 28 64 66 83 3D 00 00 00 00 00 8B E8 75 12 8B C5 8B 6C 24 20".split("[ \t]+").length];
                v54 = v0_116;
                int v0_117 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? B0 01 ?? ?? ?? ??".split("[ \t]+").length];
                v55 = v0_117;
                int v0_118 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? B0 01 ?? ?? ?? ??".split("[ \t]+").length];
                v56 = v0_118;
                com.chelpus.Utils.convertStringToArraysPatch("8B 80 D0 01 00 00 8B D7 FF 50 28 64 66 83 3D 00 00 00 00 00 8B E8 75 12 8B C5 8B 6C 24 20", "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? B0 01 ?? ?? ?? ??", v53, v54, v55, v56);
                int v0_119 = new byte["33 D2 89 54 24 10 8B 73 08 8B 44 24 38 8B 48 08 89 4C 24 18 3B F1 74 23 64 66 83 3D 00 00 00 00 00 0F 85 80 00 00 00".split("[ \t]+").length];
                v59 = v0_119;
                int v0_120 = new byte["33 D2 89 54 24 10 8B 73 08 8B 44 24 38 8B 48 08 89 4C 24 18 3B F1 74 23 64 66 83 3D 00 00 00 00 00 0F 85 80 00 00 00".split("[ \t]+").length];
                v60 = v0_120;
                int v0_121 = new byte["B3 01 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v61 = v0_121;
                int v0_122 = new byte["B3 01 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v62 = v0_122;
                com.chelpus.Utils.convertStringToArraysPatch("33 D2 89 54 24 10 8B 73 08 8B 44 24 38 8B 48 08 89 4C 24 18 3B F1 74 23 64 66 83 3D 00 00 00 00 00 0F 85 80 00 00 00", "B3 01 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??", v59, v60, v61, v62);
                int v0_123 = new byte["E5 03 1A AA C0 03 3F D6 FB 03 00 2A 7F 07 00 71 61 02 00 54 35 00 80 52 E1 03 16 AA 2A 00 40 B9 E0 03 14 AA".split("[ \t]+").length];
                v65 = v0_123;
                int v0_124 = new byte["E5 03 1A AA C0 03 3F D6 FB 03 00 2A 7F 07 00 71 61 02 00 54 35 00 80 52 E1 03 16 AA 2A 00 40 B9 E0 03 14 AA".split("[ \t]+").length];
                v66 = v0_124;
                int v0_125 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 35 00 80 52 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v67 = v0_125;
                int v0_126 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 35 00 80 52 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v68 = v0_126;
                com.chelpus.Utils.convertStringToArraysPatch("E5 03 1A AA C0 03 3F D6 FB 03 00 2A 7F 07 00 71 61 02 00 54 35 00 80 52 E1 03 16 AA 2A 00 40 B9 E0 03 14 AA", "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 35 00 80 52 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??", v65, v66, v67, v68);
                int v0_127 = new byte["F7 03 01 AA F9 03 02 AA FA 03 1F 2A F5 0A 40 B9 38 0B 40 B9 BF 02 18 6B".split("[ \t]+").length];
                v71 = v0_127;
                int v0_128 = new byte["F7 03 01 AA F9 03 02 AA FA 03 1F 2A F5 0A 40 B9 38 0B 40 B9 BF 02 18 6B".split("[ \t]+").length];
                v72 = v0_128;
                int v0_129 = new byte["?? ?? ?? ?? ?? ?? ?? ?? 3A 00 80 52 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v73 = v0_129;
                int v0_130 = new byte["?? ?? ?? ?? ?? ?? ?? ?? 3A 00 80 52 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v74 = v0_130;
                com.chelpus.Utils.convertStringToArraysPatch("F7 03 01 AA F9 03 02 AA FA 03 1F 2A F5 0A 40 B9 38 0B 40 B9 BF 02 18 6B", "?? ?? ?? ?? ?? ?? ?? ?? 3A 00 80 52 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??", v71, v72, v73, v74);
                int v0_131 = new byte["CD F8 30 C0 78 B9 1A 99 31 B9 01 3C 00 F0 ?? 80 0B 98".split("[ \t]+").length];
                v77 = v0_131;
                int v0_132 = new byte["CD F8 30 C0 78 B9 1A 99 31 B9 01 3C 00 F0 ?? 80 0B 98".split("[ \t]+").length];
                v78 = v0_132;
                int v0_133 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 00 20".split("[ \t]+").length];
                v79 = v0_133;
                int v0_134 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 00 20".split("[ \t]+").length];
                v80 = v0_134;
                com.chelpus.Utils.convertStringToArraysPatch("CD F8 30 C0 78 B9 1A 99 31 B9 01 3C 00 F0 ?? 80 0B 98", "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 00 20", v77, v78, v79, v80);
                int v0_135 = new byte["00 29 40 F0 ?? 80 00 2A 40 F0 ?? 80 01 20 00 F0 02 B8".split("[ \t]+").length];
                v83 = v0_135;
                int v0_136 = new byte["00 29 40 F0 ?? 80 00 2A 40 F0 ?? 80 01 20 00 F0 02 B8".split("[ \t]+").length];
                v84 = v0_136;
                int v0_137 = new byte["00 45 ?? ?? ?? ?? 00 45 ?? ?? ?? ?? 00 ?? ?? ?? ?? ??".split("[ \t]+").length];
                v85 = v0_137;
                int v0_138 = new byte["00 45 ?? ?? ?? ?? 00 45 ?? ?? ?? ?? 00 ?? ?? ?? ?? ??".split("[ \t]+").length];
                v86 = v0_138;
                com.chelpus.Utils.convertStringToArraysPatch("00 29 40 F0 ?? 80 00 2A 40 F0 ?? 80 01 20 00 F0 02 B8", "00 45 ?? ?? ?? ?? 00 45 ?? ?? ?? ?? 00 ?? ?? ?? ?? ??", v83, v84, v85, v86);
                int v0_139 = new byte["00 2D 40 F0 ?? 80 16 99 CD 69 DA F8 E0 20 0B 92 0B 9B 9D 42".split("[ \t]+").length];
                v89 = v0_139;
                int v0_140 = new byte["00 2D 40 F0 ?? 80 16 99 CD 69 DA F8 E0 20 0B 92 0B 9B 9D 42".split("[ \t]+").length];
                v90 = v0_140;
                int v0_141 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 9B ??".split("[ \t]+").length];
                v91 = v0_141;
                int v0_142 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 9B ??".split("[ \t]+").length];
                v92 = v0_142;
                com.chelpus.Utils.convertStringToArraysPatch("00 2D 40 F0 ?? 80 16 99 CD 69 DA F8 E0 20 0B 92 0B 9B 9D 42", "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 9B ??", v89, v90, v91, v92);
                int v0_143 = new byte["2D E9 E0 4D 8D B0 07 1C 00 90 15 91 16 92 17 93 16 9A 56 6A 31 1C 08 68 D0 F8 BC 01 D0 F8 28 E0 F0 47".split("[ \t]+").length];
                v95 = v0_143;
                int v0_144 = new byte["2D E9 E0 4D 8D B0 07 1C 00 90 15 91 16 92 17 93 16 9A 56 6A 31 1C 08 68 D0 F8 BC 01 D0 F8 28 E0 F0 47".split("[ \t]+").length];
                v96 = v0_144;
                int v0_145 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 01 20 0D B0 BD E8 E0 8D ?? ??".split("[ \t]+").length];
                v97 = v0_145;
                int v0_146 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 01 20 0D B0 BD E8 E0 8D ?? ??".split("[ \t]+").length];
                v98 = v0_146;
                com.chelpus.Utils.convertStringToArraysPatch("2D E9 E0 4D 8D B0 07 1C 00 90 15 91 16 92 17 93 16 9A 56 6A 31 1C 08 68 D0 F8 BC 01 D0 F8 28 E0 F0 47", "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 01 20 0D B0 BD E8 E0 8D ?? ??", v95, v96, v97, v98);
                int v0_147 = new byte["1C BF D9 F8 B0 E1 F0 47 BF F3 5B 8F 31 1C 08 68 D0 F8 ?? 02 D0 F8 28 E0 F0 47 14 90 14 ?? 00 ?? ?? D1".split("[ \t]+").length];
                v101 = v0_147;
                int v0_148 = new byte["1C BF D9 F8 B0 E1 F0 47 BF F3 5B 8F 31 1C 08 68 D0 F8 ?? 02 D0 F8 28 E0 F0 47 14 90 14 ?? 00 ?? ?? D1".split("[ \t]+").length];
                v102 = v0_148;
                int v0_149 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 00 20 ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v103 = v0_149;
                int v0_150 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 00 20 ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v104 = v0_150;
                com.chelpus.Utils.convertStringToArraysPatch("1C BF D9 F8 B0 E1 F0 47 BF F3 5B 8F 31 1C 08 68 D0 F8 ?? 02 D0 F8 28 E0 F0 47 14 90 14 ?? 00 ?? ?? D1", "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 00 20 ?? ?? ?? ?? ?? ?? ?? ??", v101, v102, v103, v104);
                int v0_151 = new byte["F0 47 82 46 BA F1 00 0F ?? D1 01 3C ?? F0 ?? ?? 28 ?? 00 2B".split("[ \t]+").length];
                v107 = v0_151;
                int v0_152 = new byte["F0 47 82 46 BA F1 00 0F ?? D1 01 3C ?? F0 ?? ?? 28 ?? 00 2B".split("[ \t]+").length];
                v108 = v0_152;
                int v0_153 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? E0 ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v109 = v0_153;
                int v0_154 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? E0 ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v110 = v0_154;
                com.chelpus.Utils.convertStringToArraysPatch("F0 47 82 46 BA F1 00 0F ?? D1 01 3C ?? F0 ?? ?? 28 ?? 00 2B", "?? ?? ?? ?? ?? ?? ?? ?? ?? E0 ?? ?? ?? ?? ?? ?? ?? ?? ?? ??", v107, v108, v109, v110);
                int v0_155 = new byte["F8 0C 00 D0 F8 ?? E0 F0 47 80 46 B8 F1 00 0F ?? D1 D9 F8 ?? ?? 39 1C".split("[ \t]+").length];
                v113 = v0_155;
                int v0_156 = new byte["F8 0C 00 D0 F8 ?? E0 F0 47 80 46 B8 F1 00 0F ?? D1 D9 F8 ?? ?? 39 1C".split("[ \t]+").length];
                v114 = v0_156;
                int v0_157 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? E0 ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v115 = v0_157;
                int v0_158 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? E0 ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v116 = v0_158;
                com.chelpus.Utils.convertStringToArraysPatch("F8 0C 00 D0 F8 ?? E0 F0 47 80 46 B8 F1 00 0F ?? D1 D9 F8 ?? ?? 39 1C", "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? E0 ?? ?? ?? ?? ?? ??", v113, v114, v115, v116);
                int v0_159 = new byte["50 F8 0C 00 D0 F8 ?? E0 F0 47 06 1C ?? BB D9 F8 24 E1 29 1C ?? ?? ?? ?? F0 47".split("[ \t]+").length];
                v119 = v0_159;
                int v0_160 = new byte["50 F8 0C 00 D0 F8 ?? E0 F0 47 06 1C ?? BB D9 F8 24 E1 29 1C ?? ?? ?? ?? F0 47".split("[ \t]+").length];
                v120 = v0_160;
                int v0_161 = new byte["?? ?? ?? ?? ?? ?? ?? ?? 01 20 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v121 = v0_161;
                int v0_162 = new byte["?? ?? ?? ?? ?? ?? ?? ?? 01 20 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v122 = v0_162;
                com.chelpus.Utils.convertStringToArraysPatch("50 F8 0C 00 D0 F8 ?? E0 F0 47 06 1C ?? BB D9 F8 24 E1 29 1C ?? ?? ?? ?? F0 47", "?? ?? ?? ?? ?? ?? ?? ?? 01 20 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??", v119, v120, v121, v122);
                int v0_163 = new byte["05 F0 80 05 ?? B9 16 9A 51 46 30 1C 40 68".split("[ \t]+").length];
                v125 = v0_163;
                int v0_164 = new byte["05 F0 80 05 ?? B9 16 9A 51 46 30 1C 40 68".split("[ \t]+").length];
                v126 = v0_164;
                int v0_165 = new byte["45 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v127 = v0_165;
                int v0_166 = new byte["45 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v128 = v0_166;
                com.chelpus.Utils.convertStringToArraysPatch("05 F0 80 05 ?? B9 16 9A 51 46 30 1C 40 68", "45 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??", v125, v126, v127, v128);
                int v0_167 = new byte["BA 01 00 00 00 89 54 24 28 BB FD FF FF FF 89 5C 24 30 8B 4C 24 54 33 C0 89 44 24 2C 85 C9 75 44 8B 54 24 58 85 D2 75 23".split("[ \t]+").length];
                v131 = v0_167;
                int v0_168 = new byte["BA 01 00 00 00 89 54 24 28 BB FD FF FF FF 89 5C 24 30 8B 4C 24 54 33 C0 89 44 24 2C 85 C9 75 44 8B 54 24 58 85 D2 75 23".split("[ \t]+").length];
                v132 = v0_168;
                int v0_169 = new byte["?? 00 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 90 90 ?? ?? ?? ?? ?? ?? 90 90".split("[ \t]+").length];
                v133 = v0_169;
                int v0_170 = new byte["?? 00 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 90 90 ?? ?? ?? ?? ?? ?? 90 90".split("[ \t]+").length];
                v134 = v0_170;
                com.chelpus.Utils.convertStringToArraysPatch("BA 01 00 00 00 89 54 24 28 BB FD FF FF FF 89 5C 24 30 8B 4C 24 54 33 C0 89 44 24 2C 85 C9 75 44 8B 54 24 58 85 D2 75 23", "?? 00 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 90 90 ?? ?? ?? ?? ?? ?? 90 90", v131, v132, v133, v134);
                int v0_171 = new byte["85 ED 0F 85 ?? ?? 00 00 8B 54 24 58 8B 6A 1C 8B 5C 24 24 8B 83 ?? 00 00 00 89 44 24 38 8B 4C 24 38 3B E9".split("[ \t]+").length];
                v137 = v0_171;
                int v0_172 = new byte["85 ED 0F 85 ?? ?? 00 00 8B 54 24 58 8B 6A 1C 8B 5C 24 24 8B 83 ?? 00 00 00 89 44 24 38 8B 4C 24 38 3B E9".split("[ \t]+").length];
                v138 = v0_172;
                int v0_173 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ED".split("[ \t]+").length];
                v139 = v0_173;
                int v0_174 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ED".split("[ \t]+").length];
                v140 = v0_174;
                com.chelpus.Utils.convertStringToArraysPatch("85 ED 0F 85 ?? ?? 00 00 8B 54 24 58 8B 6A 1C 8B 5C 24 24 8B 83 ?? 00 00 00 89 44 24 38 8B 4C 24 38 3B E9", "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ED", v137, v138, v139, v140);
                int v0_175 = new byte["3D 00 80 52 42 00 80 12 E2 3B 00 B9 E3 03 1F 2A E3 37 00 B9 3B 02 00 B5 9C 01 00 B5".split("[ \t]+").length];
                v143 = v0_175;
                int v0_176 = new byte["3D 00 80 52 42 00 80 12 E2 3B 00 B9 E3 03 1F 2A E3 37 00 B9 3B 02 00 B5 9C 01 00 B5".split("[ \t]+").length];
                v144 = v0_176;
                int v0_177 = new byte["FD 03 1F 2A ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? FD 03 1F 2A FD 03 1F 2A".split("[ \t]+").length];
                v145 = v0_177;
                int v0_178 = new byte["FD 03 1F 2A ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? FD 03 1F 2A FD 03 1F 2A".split("[ \t]+").length];
                v146 = v0_178;
                com.chelpus.Utils.convertStringToArraysPatch("3D 00 80 52 42 00 80 12 E2 3B 00 B9 E3 03 1F 2A E3 37 00 B9 3B 02 00 B5 9C 01 00 B5", "FD 03 1F 2A ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? FD 03 1F 2A FD 03 1F 2A", v143, v144, v145, v146);
                int v0_179 = new byte["3C 00 80 52 42 00 80 12 E2 3B 00 B9 E3 03 1F 2A E3 37 00 B9 3A 02 00 B5 9B 01 00 B5".split("[ \t]+").length];
                v149 = v0_179;
                int v0_180 = new byte["3C 00 80 52 42 00 80 12 E2 3B 00 B9 E3 03 1F 2A E3 37 00 B9 3A 02 00 B5 9B 01 00 B5".split("[ \t]+").length];
                v150 = v0_180;
                int v0_181 = new byte["FC 03 1F 2A ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? FC 03 1F 2A FC 03 1F 2A".split("[ \t]+").length];
                v151 = v0_181;
                int v0_182 = new byte["FC 03 1F 2A ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? FC 03 1F 2A FC 03 1F 2A".split("[ \t]+").length];
                v152 = v0_182;
                com.chelpus.Utils.convertStringToArraysPatch("3C 00 80 52 42 00 80 12 E2 3B 00 B9 E3 03 1F 2A E3 37 00 B9 3A 02 00 B5 9B 01 00 B5", "FC 03 1F 2A ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? FC 03 1F 2A FC 03 1F 2A", v149, v150, v151, v152);
                int v0_183 = new byte["E2 33 00 B9 E3 33 40 B9 9F 02 03 6B 6A 13 00 54 A0 16 40 B9".split("[ \t]+").length];
                v155 = v0_183;
                int v0_184 = new byte["E2 33 00 B9 E3 33 40 B9 9F 02 03 6B 6A 13 00 54 A0 16 40 B9".split("[ \t]+").length];
                v156 = v0_184;
                int v0_185 = new byte["?? ?? ?? ?? E3 03 14 2A ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v157 = v0_185;
                int v0_186 = new byte["?? ?? ?? ?? E3 03 14 2A ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v158 = v0_186;
                com.chelpus.Utils.convertStringToArraysPatch("E2 33 00 B9 E3 33 40 B9 9F 02 03 6B 6A 13 00 54 A0 16 40 B9", "?? ?? ?? ?? E3 03 14 2A ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??", v155, v156, v157, v158);
                int v0_187 = new byte["D6 02 19 12 56 01 00 35 E1 03 18 AA E0 03 15 AA 00 0C 40 B9 E2 03 1B AA".split("[ \t]+").length];
                v161 = v0_187;
                int v0_188 = new byte["D6 02 19 12 56 01 00 35 E1 03 18 AA E0 03 15 AA 00 0C 40 B9 E2 03 1B AA".split("[ \t]+").length];
                v162 = v0_188;
                int v0_189 = new byte["D6 02 19 32 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v163 = v0_189;
                int v0_190 = new byte["D6 02 19 32 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v164 = v0_190;
                com.chelpus.Utils.convertStringToArraysPatch("D6 02 19 12 56 01 00 35 E1 03 18 AA E0 03 15 AA 00 0C 40 B9 E2 03 1B AA", "D6 02 19 32 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??", v161, v162, v163, v164);
                int v0_191 = new byte[16];
                v225 = v0_191;
                v225 = {57, 102, 8, 0, 57, 102, 4, 0, 18, 22, 15, 6, 18, -10, 40, -2};
                int v0_192 = new byte[16];
                v306 = v0_192;
                v306 = {0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
                int v0_194 = new byte[16];
                v255 = v0_194;
                v255 = {57, 102, 8, 0, 57, 102, 4, 0, 18, 22, 18, 6, 18, 6, 15, 6};
                int v0_195 = new byte[16];
                v352 = v0_195;
                v352 = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1};
                int v0_197 = new byte[24];
                v226 = v0_197;
                v226 = {10, 102, 57, 102, 102, 102, 26, 102, 102, 102, 26, 102, 102, 102, 113, 102, 102, 102, 102, 102, 19, 102, -19, -1};
                int v0_198 = new byte[24];
                v307 = v0_198;
                v307 = {0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 0, 0};
                int v0_200 = new byte[24];
                v256 = v0_200;
                v256 = {18, 102, 56, 102, 102, 102, 26, 102, 102, 102, 26, 102, 102, 102, 113, 102, 102, 102, 102, 102, 19, 102, -19, -1};
                int v0_202 = new byte[24];
                v353 = v0_202;
                v353 = {1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
                int v0_204 = new byte[19];
                v227 = v0_204;
                v227 = {-128, 0, 57, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 53, -121, 102, 102, 26};
                int v0_205 = new byte[19];
                v308 = v0_205;
                v308 = {0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 0};
                int v0_207 = new byte[19];
                v257 = v0_207;
                v257 = {-128, 0, 57, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 53, -120, 102, 102, 26};
                int v0_209 = new byte[19];
                v354 = v0_209;
                v354 = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0};
                int v0_211 = new byte[14];
                v228 = v0_211;
                v228 = {57, 102, 7, 0, 57, 102, 3, 0, 15, 6, 18, -10, 40, -2};
                int v0_212 = new byte[14];
                v309 = v0_212;
                v309 = {0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0};
                int v0_214 = new byte[14];
                v258 = v0_214;
                v258 = {57, 102, 7, 0, 57, 102, 3, 0, 18, 6, 18, 6, 15, 6};
                int v0_216 = new byte[14];
                v355 = v0_216;
                v355 = {0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1};
                int v0_218 = new byte[20];
                v229 = v0_218;
                v229 = {84, 102, 102, 102, 110, 32, 102, 102, 102, 102, 10, 102, 56, 4, 4, 0, 18, 20, 15, 4};
                int v0_219 = new byte[20];
                v310 = v0_219;
                v310 = {0, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0};
                int v0_221 = new byte[20];
                v259 = v0_221;
                v259 = {84, 102, 102, 102, 110, 32, 102, 102, 102, 102, 18, 102, 57, 4, 4, 0, 18, 20, 15, 4};
                int v0_223 = new byte[20];
                v356 = v0_223;
                v356 = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0};
                int v0_225 = new byte[14];
                v230 = v0_225;
                v230 = {10, 102, 57, 102, 102, 102, 34, 102, 102, 102, 19, 102, -19, -1};
                int v0_226 = new byte[14];
                v311 = v0_226;
                v311 = {0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 0, 0};
                int v0_228 = new byte[14];
                v260 = v0_228;
                v260 = {18, 102, 56, 102, 102, 102, 34, 102, 102, 102, 19, 102, -19, -1};
                int v0_230 = new byte[14];
                v357 = v0_230;
                v357 = {1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
                int v0_232 = new byte[28];
                v231 = v0_232;
                v231 = {29, 102, 102, 102, 102, 102, 102, 102, 12, 102, 57, 102, 102, 0, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 12, 102, 30, 102};
                int v0_233 = new byte[28];
                v312 = v0_233;
                v312 = {0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1};
                int v0_235 = new byte[28];
                v261 = v0_235;
                v261 = {29, 102, 102, 102, 102, 102, 102, 102, 12, 102, 51, 102, 102, 0, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 12, 102, 30, 102};
                int v0_237 = new byte[28];
                v358 = v0_237;
                v358 = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
                v192 = 2;
                int v0_239 = new byte[38];
                v195 = v0_239;
                v195 = {29, 102, 102, 102, 102, 102, 102, 102, 12, 102, 57, 102, 102, 0, 8, 102, 102, 102, 102, 102, 102, 102, 8, 102, 102, 102, 8, 102, 102, 102, 2, 102, 102, 102, 2, 102, 102, 102};
                int v0_240 = new byte[38];
                v196 = v0_240;
                v196 = {0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1};
                int v0_241 = new byte[38];
                v197 = v0_241;
                v197 = {29, 102, 102, 102, 102, 102, 102, 102, 12, 102, 51, 102, 102, 0, 8, 102, 102, 102, 102, 102, 102, 102, 8, 102, 102, 102, 8, 102, 102, 102, 2, 102, 102, 102, 2, 102, 102, 102};
                int v0_242 = new byte[38];
                v198 = v0_242;
                v198 = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
                v200 = 2;
                int v0_243 = new byte[29];
                v222 = v0_243;
                v222 = {56, 102, 102, 102, 8, 102, 102, 102, 8, 102, 102, 102, 102, 102, 102, 102, 102, 102, 10, 102, 57, 102, 102, 102, 56, 102, 102, 102, 26};
                int v0_244 = new byte[29];
                v303 = v0_244;
                v303 = {0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0};
                int v0_246 = new byte[29];
                v252 = v0_246;
                v252 = {56, 102, 102, 102, 8, 102, 102, 102, 8, 102, 102, 102, 102, 102, 102, 102, 102, 102, 18, 102, 56, 102, 102, 102, 56, 102, 102, 102, 26};
                int v0_247 = new byte[29];
                v349 = v0_247;
                v349 = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0};
                int v0_249 = new byte[29];
                v223 = v0_249;
                v223 = {56, 102, 102, 0, 8, 102, 102, 102, 102, 102, 102, 102, 102, 102, 12, 102, 102, 102, 102, 102, 102, 102, 10, 102, 57, 102, 102, 102, 56};
                int v0_250 = new byte[29];
                v304 = v0_250;
                v304 = {0, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0};
                int v0_252 = new byte[29];
                v253 = v0_252;
                v253 = {56, 102, 102, 0, 8, 102, 102, 102, 102, 102, 102, 102, 102, 102, 12, 102, 102, 102, 102, 102, 102, 102, 18, 102, 56, 102, 102, 102, 56};
                int v0_253 = new byte[29];
                v350 = v0_253;
                v350 = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0};
                int v0_255 = new byte[25];
                v224 = v0_255;
                v224 = {56, 102, 102, 102, 8, 102, 102, 102, 102, 102, 102, 102, 102, 102, 10, 102, 57, 102, 102, 102, 56, 102, 102, 102, 26};
                int v0_256 = new byte[25];
                v305 = v0_256;
                v305 = {0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0};
                int v0_258 = new byte[25];
                v254 = v0_258;
                v254 = {56, 102, 102, 102, 8, 102, 102, 102, 102, 102, 102, 102, 102, 102, 18, 102, 56, 102, 102, 102, 56, 102, 102, 102, 26};
                int v0_259 = new byte[25];
                v351 = v0_259;
                v351 = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0};
                int v0_261 = new byte["D5 ?? 80 00 39 ?? ?? ?? 71 20 ?? ?? ?? ?? 54 ?? ?? ?? 52 ?? ?? ?? DD ?? ?? 01 38 ?? ?? 00".split("[ \t]+").length];
                v167 = v0_261;
                int v0_262 = new byte["D5 ?? 80 00 39 ?? ?? ?? 71 20 ?? ?? ?? ?? 54 ?? ?? ?? 52 ?? ?? ?? DD ?? ?? 01 38 ?? ?? 00".split("[ \t]+").length];
                v168 = v0_262;
                int v0_263 = new byte["D6 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v169 = v0_263;
                int v0_264 = new byte["D6 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v170 = v0_264;
                com.chelpus.Utils.convertStringToArraysPatch("D5 ?? 80 00 39 ?? ?? ?? 71 20 ?? ?? ?? ?? 54 ?? ?? ?? 52 ?? ?? ?? DD ?? ?? 01 38 ?? ?? 00", "D6 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??", v167, v168, v169, v170);
                int v0_265 = new byte["39 ?? 07 00 39 ?? 03 00 0F 03 12 F3 28 FE".split("[ \t]+").length];
                v173 = v0_265;
                int v0_266 = new byte["39 ?? 07 00 39 ?? 03 00 0F 03 12 F3 28 FE".split("[ \t]+").length];
                v174 = v0_266;
                int v0_267 = new byte["?? ?? ?? ?? ?? ?? ?? ?? 12 03 12 03 0F 03".split("[ \t]+").length];
                v175 = v0_267;
                int v0_268 = new byte["?? ?? ?? ?? ?? ?? ?? ?? 12 03 12 03 0F 03".split("[ \t]+").length];
                v176 = v0_268;
                com.chelpus.Utils.convertStringToArraysPatch("39 ?? 07 00 39 ?? 03 00 0F 03 12 F3 28 FE", "?? ?? ?? ?? ?? ?? ?? ?? 12 03 12 03 0F 03", v173, v174, v175, v176);
                int v0_269 = new byte[35];
                v202 = v0_269;
                v202 = {32, 102, 102, 18, 49, 50, 16, 10, 0, 34, 0, 102, 102, 26, 1, 102, 102, 112, 32, 102, 102, 102, 102, 39, 0, 102, 32, 102, 102, 102, 102, 10, 0, 15, 0};
                int v0_270 = new byte[35];
                v284 = v0_270;
                v284 = {0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 1, 1, 0, 0, 1, 0, 1, 1, 1, 1, 0, 0, 0, 0};
                int v0_272 = new byte[35];
                v232 = v0_272;
                v232 = {32, 102, 102, 18, 49, 50, 16, 10, 0, 34, 0, 102, 102, 26, 1, 102, 102, 112, 32, 102, 102, 102, 102, 39, 0, 102, 32, 102, 102, 102, 102, 18, 16, 15, 0};
                int v0_273 = new byte[35];
                v330 = v0_273;
                v330 = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0};
                int v0_275 = new byte[36];
                v203 = v0_275;
                v203 = {82, 32, 102, 102, 18, 49, 50, 16, 10, 0, 34, 0, 102, 102, 26, 1, 102, 102, 112, 32, 102, 102, 102, 102, 39, 0, 110, 32, 102, 102, 102, 102, 10, 0, 15, 0};
                int v0_276 = new byte[36];
                v285 = v0_276;
                v285 = {0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0};
                int v0_278 = new byte[36];
                v233 = v0_278;
                v233 = {82, 32, 102, 102, 18, 49, 50, 16, 10, 0, 34, 0, 102, 102, 26, 1, 102, 102, 112, 32, 102, 102, 102, 102, 39, 0, 110, 32, 102, 102, 102, 102, 18, 16, 15, 0};
                int v0_279 = new byte[36];
                v331 = v0_279;
                v331 = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0};
                int v0_281 = new byte[30];
                v204 = v0_281;
                v204 = {18, 3, 33, 65, 33, 102, 50, 102, 102, 0, 102, 102, 15, 1, 18, 0, 33, 102, 53, 102, 102, 0, 72, 102, 4, 0, 72, 102, 5, 0};
                int v0_282 = new byte[30];
                v286 = v0_282;
                v286 = {0, 0, 0, 0, 0, 1, 0, 1, 1, 0, 1, 1, 0, 0, 0, 0, 0, 1, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0};
                int v0_284 = new byte[30];
                v234 = v0_284;
                v234 = {18, 19, 33, 65, 33, 102, 50, 102, 102, 0, 102, 102, 15, 1, 18, 0, 33, 102, 53, 102, 102, 0, 72, 102, 4, 0, 72, 102, 5, 0};
                int v0_285 = new byte[30];
                v332 = v0_285;
                v332 = {1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
                int v0_287 = new byte[28];
                v205 = v0_287;
                v205 = {18, 1, 33, 66, 33, 102, 50, 102, 102, 0, 15, 1, 18, 0, 33, 102, 53, 102, 102, 0, 72, 102, 4, 0, 72, 102, 5, 0};
                int v0_288 = new byte[28];
                v287 = v0_288;
                v287 = {0, 0, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0};
                int v0_290 = new byte[28];
                v235 = v0_290;
                v235 = {18, 17, 33, 66, 33, 102, 50, 102, 102, 0, 15, 1, 18, 0, 33, 102, 53, 102, 102, 0, 72, 102, 4, 0, 72, 102, 5, 0};
                int v0_291 = new byte[28];
                v333 = v0_291;
                v333 = {1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
                int v0_293 = new byte[43];
                v206 = v0_293;
                v206 = {32, 102, 102, 18, 49, 50, 16, 14, 0, 34, 0, 102, 102, 26, 1, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 112, 32, 102, 102, 102, 102, 39, 0, 102, 32, 102, 102, 102, 102, 10, 0, 15, 0};
                int v0_294 = new byte[43];
                v288 = v0_294;
                v288 = {0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 0, 1, 0, 1, 1, 1, 1, 0, 0, 0, 0};
                int v0_296 = new byte[43];
                v236 = v0_296;
                v236 = {32, 102, 102, 18, 49, 50, 16, 14, 0, 34, 0, 102, 102, 26, 1, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 112, 32, 102, 102, 102, 102, 39, 0, 102, 32, 102, 102, 102, 102, 18, 16, 15, 0};
                int v0_297 = new byte[43];
                v334 = v0_297;
                v334 = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0};
                int v0_299 = new byte[44];
                v207 = v0_299;
                v207 = {56, 102, 102, 102, 58, 102, 102, 102, 58, 102, 102, 102, -112, 102, 102, 102, 33, 102, 55, 102, 102, 102, 34, 102, 102, 102, 112, 102, 102, 102, 102, 102, 39, 102, 102, 102, 102, 102, 102, 102, 10, 0, 15, 0};
                int v0_300 = new byte[44];
                v289 = v0_300;
                v289 = {0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0};
                int v0_302 = new byte[44];
                v237 = v0_302;
                v237 = {56, 102, 102, 102, 58, 102, 102, 102, 58, 102, 102, 102, -112, 102, 102, 102, 33, 102, 55, 102, 102, 102, 34, 102, 102, 102, 112, 102, 102, 102, 102, 102, 39, 102, 102, 102, 102, 102, 102, 102, 18, 16, 15, 0};
                int v0_303 = new byte[44];
                v335 = v0_303;
                v335 = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0};
                int v0_305 = new byte[70];
                v208 = v0_305;
                v208 = {56, 102, 102, 102, 58, 102, 102, 102, 58, 102, 102, 102, -112, 102, 102, 102, 33, 102, 55, 102, 102, 102, 34, 102, 102, 102, 112, 102, 102, 102, 102, 102, 39, 102, 33, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 10, 0, 15, 0};
                int v0_306 = new byte[70];
                v290 = v0_306;
                v290 = {0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0};
                int v0_308 = new byte[70];
                v238 = v0_308;
                v238 = {56, 102, 102, 102, 58, 102, 102, 102, 58, 102, 102, 102, -112, 102, 102, 102, 33, 102, 55, 102, 102, 102, 34, 102, 102, 102, 112, 102, 102, 102, 102, 102, 39, 102, 33, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 18, 16, 15, 0};
                int v0_309 = new byte[70];
                v336 = v0_309;
                v336 = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0};
                int v0_311 = new byte[56];
                v209 = v0_311;
                v209 = {56, 102, 102, 102, 58, 102, 102, 102, 58, 102, 102, 102, -112, 102, 102, 102, 33, 102, 55, 102, 102, 102, 34, 102, 102, 102, 26, 102, 102, 102, 113, 102, 102, 102, 102, 102, 12, 102, 112, 102, 102, 102, 102, 102, 39, 102, 102, 102, 102, 102, 102, 102, 10, 0, 15, 0};
                int v0_312 = new byte[56];
                v291 = v0_312;
                v291 = {0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0};
                int v0_314 = new byte[56];
                v239 = v0_314;
                v239 = {56, 102, 102, 102, 58, 102, 102, 102, 58, 102, 102, 102, -112, 102, 102, 102, 33, 102, 55, 102, 102, 102, 34, 102, 102, 102, 26, 102, 102, 102, 113, 102, 102, 102, 102, 102, 12, 102, 112, 102, 102, 102, 102, 102, 39, 102, 102, 102, 102, 102, 102, 102, 18, 16, 15, 0};
                int v0_315 = new byte[56];
                v337 = v0_315;
                v337 = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1};
                int v0_317 = new byte[41];
                v210 = v0_317;
                v210 = {32, 102, 102, 18, 49, 50, 16, 10, 0, 34, 0, 102, 102, 26, 1, 102, 102, 112, 32, 102, 102, 102, 102, 39, 0, 102, 102, 102, 102, 102, 102, 102, 32, 102, 102, 102, 102, 10, 0, 15, 0};
                int v0_318 = new byte[41];
                v292 = v0_318;
                v292 = {0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 0, 0, 0};
                int v0_320 = new byte[41];
                v240 = v0_320;
                v240 = {32, 102, 102, 18, 49, 50, 16, 10, 0, 34, 0, 102, 102, 26, 1, 102, 102, 112, 32, 102, 102, 102, 102, 39, 0, 102, 102, 102, 102, 102, 102, 102, 32, 102, 102, 102, 102, 18, 16, 15, 0};
                int v0_321 = new byte[41];
                v338 = v0_321;
                v338 = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0};
                int v0_323 = new byte[50];
                v211 = v0_323;
                v211 = {56, 102, 102, 102, 58, 102, 102, 102, 58, 102, 102, 102, -112, 102, 102, 102, 33, 102, 55, 102, 102, 102, 34, 102, 102, 102, 112, 102, 102, 102, 102, 102, 39, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 10, 0, 15, 0};
                int v0_324 = new byte[50];
                v293 = v0_324;
                v293 = {0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0};
                int v0_326 = new byte[50];
                v241 = v0_326;
                v241 = {56, 102, 102, 102, 58, 102, 102, 102, 58, 102, 102, 102, -112, 102, 102, 102, 33, 102, 55, 102, 102, 102, 34, 102, 102, 102, 112, 102, 102, 102, 102, 102, 39, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 18, 16, 15, 0};
                int v0_327 = new byte[50];
                v339 = v0_327;
                v339 = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0};
                int v0_329 = new byte[26];
                v213 = v0_329;
                v213 = {18, 2, 33, 83, 33, 102, 50, 102, 102, 0, 15, 2, 18, 1, 18, 0, 33, 102, 53, 102, 102, 0, 72, 102, 5, 0};
                int v0_330 = new byte[26];
                v295 = v0_330;
                v295 = {0, 0, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 1, 0, 0, 1, 0, 0};
                int v0_332 = new byte[26];
                v243 = v0_332;
                v243 = {18, 18, 33, 83, 33, 102, 50, 102, 102, 0, 15, 2, 18, 1, 18, 0, 33, 102, 53, 102, 102, 0, 72, 102, 5, 0};
                int v0_333 = new byte[26];
                v341 = v0_333;
                v341 = {1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
                int v0_335 = new byte[36];
                v214 = v0_335;
                v214 = {-14, 32, 102, 102, 18, 49, 50, 16, 10, 0, 34, 0, 102, 102, 26, 1, 102, 102, 112, 32, 102, 102, 102, 102, 39, 0, -8, 32, 102, 102, 102, 102, 10, 0, 15, 0};
                int v0_336 = new byte[36];
                v296 = v0_336;
                v296 = {0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0};
                int v0_338 = new byte[36];
                v244 = v0_338;
                v244 = {-14, 32, 102, 102, 18, 49, 50, 16, 10, 0, 34, 0, 102, 102, 26, 1, 102, 102, 112, 32, 102, 102, 102, 102, 39, 0, -8, 32, 102, 102, 102, 102, 18, 16, 15, 0};
                int v0_339 = new byte[36];
                v342 = v0_339;
                v342 = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0};
                v105_0 = v328_0;
                v81_0 = v327_0;
                v33_0 = v326_0;
            }
            if (p362[0].contains("restore")) {
                int v326_1;
                if (!p362[0].contains("restoreCore")) {
                    v326_1 = v33_0;
                } else {
                    v184 = 1;
                    v326_1 = 1;
                }
                int v328_1;
                int v327_1;
                if (!p362[0].contains("restoreServices")) {
                    v328_1 = v105_0;
                    v327_1 = v81_0;
                } else {
                    v328_1 = 1;
                    v327_1 = 1;
                }
                v5 = new byte["11 90 01 21 01 29 0F D1 01 26 28 1C C0 68 39 1C".split("[ \t]+").length];
                v6 = new byte["11 90 01 21 01 29 0F D1 01 26 28 1C C0 68 39 1C".split("[ \t]+").length];
                v7 = new byte["?? ?? 11 99 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v8 = new byte["?? ?? 11 99 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                com.chelpus.Utils.convertStringToArraysPatch("11 90 01 21 01 29 0F D1 01 26 28 1C C0 68 39 1C", "?? ?? 11 99 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??", v5, v6, v7, v8);
                v11 = new byte["09 90 80 42 0C D1 D5 F8 DC 0B 39 1C D0 F8 24 E0 F0 47".split("[ \t]+").length];
                v12 = new byte["09 90 80 42 0C D1 D5 F8 DC 0B 39 1C D0 F8 24 E0 F0 47".split("[ \t]+").length];
                v13 = new byte["?? ?? B0 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v14 = new byte["?? ?? B0 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                com.chelpus.Utils.convertStringToArraysPatch("09 90 80 42 0C D1 D5 F8 DC 0B 39 1C D0 F8 24 E0 F0 47", "?? ?? B0 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??", v11, v12, v13, v14);
                int v0_344 = new byte["39 1C 08 68 52 46 D0 F8 CC 01 D0 F8 ?? E0 F0 47 05 1C 01 3C 00 F0 04 80 01 20 06 B0 BD E8 E0 85 D9 F8 ?? E2 F0 47 F7 E7".split("[ \t]+").length];
                v17 = v0_344;
                int v0_345 = new byte["39 1C 08 68 52 46 D0 F8 CC 01 D0 F8 ?? E0 F0 47 05 1C 01 3C 00 F0 04 80 01 20 06 B0 BD E8 E0 85 D9 F8 ?? E2 F0 47 F7 E7".split("[ \t]+").length];
                v18 = v0_345;
                int v0_346 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 28 1C ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v19 = v0_346;
                int v0_347 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 28 1C ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v20 = v0_347;
                com.chelpus.Utils.convertStringToArraysPatch("39 1C 08 68 52 46 D0 F8 CC 01 D0 F8 ?? E0 F0 47 05 1C 01 3C 00 F0 04 80 01 20 06 B0 BD E8 E0 85 D9 F8 ?? E2 F0 47 F7 E7", "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 28 1C ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??", v17, v18, v19, v20);
                int v0_348 = new byte["08 68 3A 1C D0 F8 D0 01 43 46 D0 F8 ?? E0 F0 47 05 1C 01 3C 00 F0 04 80 01 20 09 B0 BD E8 E0 8D D9 F8 ?? E2 F0 47 F7 E7".split("[ \t]+").length];
                v23 = v0_348;
                int v0_349 = new byte["08 68 3A 1C D0 F8 D0 01 43 46 D0 F8 ?? E0 F0 47 05 1C 01 3C 00 F0 04 80 01 20 09 B0 BD E8 E0 8D D9 F8 ?? E2 F0 47 F7 E7".split("[ \t]+").length];
                v24 = v0_349;
                int v0_350 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 28 1C ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v25 = v0_350;
                int v0_351 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 28 1C ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v26 = v0_351;
                com.chelpus.Utils.convertStringToArraysPatch("08 68 3A 1C D0 F8 D0 01 43 46 D0 F8 ?? E0 F0 47 05 1C 01 3C 00 F0 04 80 01 20 09 B0 BD E8 E0 8D D9 F8 ?? E2 F0 47 F7 E7", "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 28 1C ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??", v23, v24, v25, v26);
                int v0_352 = new byte["56 45 00 F0 07 80 01 3C 00 F0 31 80 01 20".split("[ \t]+").length];
                v29 = v0_352;
                int v0_353 = new byte["56 45 00 F0 07 80 01 3C 00 F0 31 80 01 20".split("[ \t]+").length];
                v30 = v0_353;
                int v0_354 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 05 98".split("[ \t]+").length];
                v31 = v0_354;
                int v0_355 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 05 98".split("[ \t]+").length];
                v32 = v0_355;
                com.chelpus.Utils.convertStringToArraysPatch("56 45 00 F0 07 80 01 3C 00 F0 31 80 01 20", "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 05 98", v29, v30, v31, v32);
                int v0_356 = new byte["56 45 00 00 01 20 09 b0 bd e8 e0 8d 00 27 00 25".split("[ \t]+").length];
                v35 = v0_356;
                int v0_357 = new byte["56 45 00 00 01 20 09 b0 bd e8 e0 8d 00 27 00 25".split("[ \t]+").length];
                v36 = v0_357;
                int v0_358 = new byte["?? ?? 03 d0 00 20 ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v37 = v0_358;
                int v0_359 = new byte["?? ?? 03 d0 00 20 ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v38 = v0_359;
                com.chelpus.Utils.convertStringToArraysPatch("56 45 00 00 01 20 09 b0 bd e8 e0 8d 00 27 00 25", "?? ?? 03 d0 00 20 ?? ?? ?? ?? ?? ?? ?? ?? ?? ??", v35, v36, v37, v38);
                int v0_360 = new byte["89 44 24 40 8B 5C 24 40 83 FB 01 90 90 BE 01 00 00 00 8B C5 8B 40 0C 8B CF".split("[ \t]+").length];
                v41 = v0_360;
                int v0_361 = new byte["89 44 24 40 8B 5C 24 40 83 FB 01 90 90 BE 01 00 00 00 8B C5 8B 40 0C 8B CF".split("[ \t]+").length];
                v42 = v0_361;
                int v0_362 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 75 32 BE ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v43 = v0_362;
                int v0_363 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 75 32 BE ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v44 = v0_363;
                com.chelpus.Utils.convertStringToArraysPatch("89 44 24 40 8B 5C 24 40 83 FB 01 90 90 BE 01 00 00 00 8B C5 8B 40 0C 8B CF", "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 75 32 BE ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??", v41, v42, v43, v44);
                int v0_364 = new byte["8B 54 24 38 8B CF 8B 01 8B 80 CC 01 00 00 FF 50 28 64 66 83 3D 00 00 00 00 00 8B E8 75 12 B0 01".split("[ \t]+").length];
                v47 = v0_364;
                int v0_365 = new byte["8B 54 24 38 8B CF 8B 01 8B 80 CC 01 00 00 FF 50 28 64 66 83 3D 00 00 00 00 00 8B E8 75 12 B0 01".split("[ \t]+").length];
                v48 = v0_365;
                int v0_366 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 8B C5".split("[ \t]+").length];
                v49 = v0_366;
                int v0_367 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 8B C5".split("[ \t]+").length];
                v50 = v0_367;
                com.chelpus.Utils.convertStringToArraysPatch("8B 54 24 38 8B CF 8B 01 8B 80 CC 01 00 00 FF 50 28 64 66 83 3D 00 00 00 00 00 8B E8 75 12 B0 01", "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 8B C5", v47, v48, v49, v50);
                int v0_368 = new byte["8B 80 D0 01 00 00 8B D7 FF 50 28 64 66 83 3D 00 00 00 00 00 8B E8 75 12 B0 01 8B 6C 24 20".split("[ \t]+").length];
                v53 = v0_368;
                int v0_369 = new byte["8B 80 D0 01 00 00 8B D7 FF 50 28 64 66 83 3D 00 00 00 00 00 8B E8 75 12 B0 01 8B 6C 24 20".split("[ \t]+").length];
                v54 = v0_369;
                int v0_370 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 8B C5 ?? ?? ?? ??".split("[ \t]+").length];
                v55 = v0_370;
                int v0_371 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 8B C5 ?? ?? ?? ??".split("[ \t]+").length];
                v56 = v0_371;
                com.chelpus.Utils.convertStringToArraysPatch("8B 80 D0 01 00 00 8B D7 FF 50 28 64 66 83 3D 00 00 00 00 00 8B E8 75 12 B0 01 8B 6C 24 20", "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 8B C5 ?? ?? ?? ??", v53, v54, v55, v56);
                int v0_372 = new byte["B3 01 89 54 24 10 8B 73 08 8B 44 24 38 8B 48 08 89 4C 24 18 3B F1 74 23 64 66 83 3D 00 00 00 00 00 0F 85 80 00 00 00".split("[ \t]+").length];
                v59 = v0_372;
                int v0_373 = new byte["B3 01 89 54 24 10 8B 73 08 8B 44 24 38 8B 48 08 89 4C 24 18 3B F1 74 23 64 66 83 3D 00 00 00 00 00 0F 85 80 00 00 00".split("[ \t]+").length];
                v60 = v0_373;
                int v0_374 = new byte["33 D2 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v61 = v0_374;
                int v0_375 = new byte["33 D2 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v62 = v0_375;
                com.chelpus.Utils.convertStringToArraysPatch("B3 01 89 54 24 10 8B 73 08 8B 44 24 38 8B 48 08 89 4C 24 18 3B F1 74 23 64 66 83 3D 00 00 00 00 00 0F 85 80 00 00 00", "33 D2 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??", v59, v60, v61, v62);
                int v0_376 = new byte["E5 03 1A AA C0 03 3F D6 FB 03 00 2A 7F 07 00 71 35 00 80 52 35 00 80 52 E1 03 16 AA 2A 00 40 B9 E0 03 14 AA".split("[ \t]+").length];
                v65 = v0_376;
                int v0_377 = new byte["E5 03 1A AA C0 03 3F D6 FB 03 00 2A 7F 07 00 71 35 00 80 52 35 00 80 52 E1 03 16 AA 2A 00 40 B9 E0 03 14 AA".split("[ \t]+").length];
                v66 = v0_377;
                int v0_378 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 61 02 00 54 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v67 = v0_378;
                int v0_379 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 61 02 00 54 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v68 = v0_379;
                com.chelpus.Utils.convertStringToArraysPatch("E5 03 1A AA C0 03 3F D6 FB 03 00 2A 7F 07 00 71 35 00 80 52 35 00 80 52 E1 03 16 AA 2A 00 40 B9 E0 03 14 AA", "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 61 02 00 54 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??", v65, v66, v67, v68);
                int v0_380 = new byte["F7 03 01 AA F9 03 02 AA 3A 00 80 52 F5 0A 40 B9 38 0B 40 B9 BF 02 18 6B".split("[ \t]+").length];
                v71 = v0_380;
                int v0_381 = new byte["F7 03 01 AA F9 03 02 AA 3A 00 80 52 F5 0A 40 B9 38 0B 40 B9 BF 02 18 6B".split("[ \t]+").length];
                v72 = v0_381;
                int v0_382 = new byte["?? ?? ?? ?? ?? ?? ?? ?? FA 03 1F 2A ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v73 = v0_382;
                int v0_383 = new byte["?? ?? ?? ?? ?? ?? ?? ?? FA 03 1F 2A ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v74 = v0_383;
                com.chelpus.Utils.convertStringToArraysPatch("F7 03 01 AA F9 03 02 AA 3A 00 80 52 F5 0A 40 B9 38 0B 40 B9 BF 02 18 6B", "?? ?? ?? ?? ?? ?? ?? ?? FA 03 1F 2A ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??", v71, v72, v73, v74);
                int v0_384 = new byte["CD F8 30 C0 78 B9 1A 99 31 B9 01 3C 00 F0 ?? 80 00 20".split("[ \t]+").length];
                v77 = v0_384;
                int v0_385 = new byte["CD F8 30 C0 78 B9 1A 99 31 B9 01 3C 00 F0 ?? 80 00 20".split("[ \t]+").length];
                v78 = v0_385;
                int v0_386 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 0B 98".split("[ \t]+").length];
                v79 = v0_386;
                int v0_387 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 0B 98".split("[ \t]+").length];
                v80 = v0_387;
                com.chelpus.Utils.convertStringToArraysPatch("CD F8 30 C0 78 B9 1A 99 31 B9 01 3C 00 F0 ?? 80 00 20", "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 0B 98", v77, v78, v79, v80);
                int v0_388 = new byte["00 45 40 F0 ?? 80 00 45 40 F0 ?? 80 00 20 00 F0 02 B8".split("[ \t]+").length];
                v83 = v0_388;
                int v0_389 = new byte["00 45 40 F0 ?? 80 00 45 40 F0 ?? 80 00 20 00 F0 02 B8".split("[ \t]+").length];
                v84 = v0_389;
                int v0_390 = new byte["00 29 ?? ?? ?? ?? 00 2A ?? ?? ?? ?? 01 ?? ?? ?? ?? ??".split("[ \t]+").length];
                v85 = v0_390;
                int v0_391 = new byte["00 29 ?? ?? ?? ?? 00 2A ?? ?? ?? ?? 01 ?? ?? ?? ?? ??".split("[ \t]+").length];
                v86 = v0_391;
                com.chelpus.Utils.convertStringToArraysPatch("00 45 40 F0 ?? 80 00 45 40 F0 ?? 80 00 20 00 F0 02 B8", "00 29 ?? ?? ?? ?? 00 2A ?? ?? ?? ?? 01 ?? ?? ?? ?? ??", v83, v84, v85, v86);
                int v0_392 = new byte["00 2D 40 F0 ?? 80 16 99 CD 69 DA F8 E0 20 0B 92 0B 9B 9B 42".split("[ \t]+").length];
                v89 = v0_392;
                int v0_393 = new byte["00 2D 40 F0 ?? 80 16 99 CD 69 DA F8 E0 20 0B 92 0B 9B 9B 42".split("[ \t]+").length];
                v90 = v0_393;
                int v0_394 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 9D ??".split("[ \t]+").length];
                v91 = v0_394;
                int v0_395 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 9D ??".split("[ \t]+").length];
                v92 = v0_395;
                com.chelpus.Utils.convertStringToArraysPatch("00 2D 40 F0 ?? 80 16 99 CD 69 DA F8 E0 20 0B 92 0B 9B 9B 42", "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 9D ??", v89, v90, v91, v92);
                int v0_396 = new byte["45 F0 80 05 ?? B9 16 9A 51 46 30 1C 40 68".split("[ \t]+").length];
                v125 = v0_396;
                int v0_397 = new byte["45 F0 80 05 ?? B9 16 9A 51 46 30 1C 40 68".split("[ \t]+").length];
                v126 = v0_397;
                int v0_398 = new byte["05 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v127 = v0_398;
                int v0_399 = new byte["05 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v128 = v0_399;
                com.chelpus.Utils.convertStringToArraysPatch("45 F0 80 05 ?? B9 16 9A 51 46 30 1C 40 68", "05 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??", v125, v126, v127, v128);
                int v0_400 = new byte["2D E9 E0 4D 8D B0 07 1C 00 90 15 91 16 92 17 93 16 9A 56 6A 31 1C 08 68 01 20 0D B0 BD E8 E0 8D F0 47".split("[ \t]+").length];
                v95 = v0_400;
                int v0_401 = new byte["2D E9 E0 4D 8D B0 07 1C 00 90 15 91 16 92 17 93 16 9A 56 6A 31 1C 08 68 01 20 0D B0 BD E8 E0 8D F0 47".split("[ \t]+").length];
                v96 = v0_401;
                int v0_402 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? D0 F8 BC 01 D0 F8 28 E0 ?? ??".split("[ \t]+").length];
                v97 = v0_402;
                int v0_403 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? D0 F8 BC 01 D0 F8 28 E0 ?? ??".split("[ \t]+").length];
                v98 = v0_403;
                com.chelpus.Utils.convertStringToArraysPatch("2D E9 E0 4D 8D B0 07 1C 00 90 15 91 16 92 17 93 16 9A 56 6A 31 1C 08 68 01 20 0D B0 BD E8 E0 8D F0 47", "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? D0 F8 BC 01 D0 F8 28 E0 ?? ??", v95, v96, v97, v98);
                int v0_404 = new byte["1C BF D9 F8 B0 E1 F0 47 BF F3 5B 8F 31 1C 08 68 D0 F8 ?? 02 D0 F8 28 E0 00 20 14 90 14 ?? 00 ?? ?? D1".split("[ \t]+").length];
                v101 = v0_404;
                int v0_405 = new byte["1C BF D9 F8 B0 E1 F0 47 BF F3 5B 8F 31 1C 08 68 D0 F8 ?? 02 D0 F8 28 E0 00 20 14 90 14 ?? 00 ?? ?? D1".split("[ \t]+").length];
                v102 = v0_405;
                int v0_406 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? F0 47 ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v103 = v0_406;
                int v0_407 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? F0 47 ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v104 = v0_407;
                com.chelpus.Utils.convertStringToArraysPatch("1C BF D9 F8 B0 E1 F0 47 BF F3 5B 8F 31 1C 08 68 D0 F8 ?? 02 D0 F8 28 E0 00 20 14 90 14 ?? 00 ?? ?? D1", "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? F0 47 ?? ?? ?? ?? ?? ?? ?? ??", v101, v102, v103, v104);
                int v0_408 = new byte["F0 47 82 46 BA F1 00 0F ?? E0 01 3C ?? F0 ?? ?? 28 ?? 00 2B".split("[ \t]+").length];
                v107 = v0_408;
                int v0_409 = new byte["F0 47 82 46 BA F1 00 0F ?? E0 01 3C ?? F0 ?? ?? 28 ?? 00 2B".split("[ \t]+").length];
                v108 = v0_409;
                int v0_410 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? D1 ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v109 = v0_410;
                int v0_411 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? D1 ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v110 = v0_411;
                com.chelpus.Utils.convertStringToArraysPatch("F0 47 82 46 BA F1 00 0F ?? E0 01 3C ?? F0 ?? ?? 28 ?? 00 2B", "?? ?? ?? ?? ?? ?? ?? ?? ?? D1 ?? ?? ?? ?? ?? ?? ?? ?? ?? ??", v107, v108, v109, v110);
                int v0_412 = new byte["F8 0C 00 D0 F8 ?? E0 F0 47 80 46 B8 F1 00 0F ?? E0 D9 F8 ?? ?? 39 1C".split("[ \t]+").length];
                v113 = v0_412;
                int v0_413 = new byte["F8 0C 00 D0 F8 ?? E0 F0 47 80 46 B8 F1 00 0F ?? E0 D9 F8 ?? ?? 39 1C".split("[ \t]+").length];
                v114 = v0_413;
                int v0_414 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? D1 ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v115 = v0_414;
                int v0_415 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? D1 ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v116 = v0_415;
                com.chelpus.Utils.convertStringToArraysPatch("F8 0C 00 D0 F8 ?? E0 F0 47 80 46 B8 F1 00 0F ?? E0 D9 F8 ?? ?? 39 1C", "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? D1 ?? ?? ?? ?? ?? ??", v113, v114, v115, v116);
                int v0_416 = new byte["50 F8 0C 00 D0 F8 ?? E0 01 20 06 1C ?? BB D9 F8 24 E1 29 1C ?? ?? ?? ?? F0 47".split("[ \t]+").length];
                v119 = v0_416;
                int v0_417 = new byte["50 F8 0C 00 D0 F8 ?? E0 01 20 06 1C ?? BB D9 F8 24 E1 29 1C ?? ?? ?? ?? F0 47".split("[ \t]+").length];
                v120 = v0_417;
                int v0_418 = new byte["?? ?? ?? ?? ?? ?? ?? ?? F0 47 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v121 = v0_418;
                int v0_419 = new byte["?? ?? ?? ?? ?? ?? ?? ?? F0 47 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v122 = v0_419;
                com.chelpus.Utils.convertStringToArraysPatch("50 F8 0C 00 D0 F8 ?? E0 01 20 06 1C ?? BB D9 F8 24 E1 29 1C ?? ?? ?? ?? F0 47", "?? ?? ?? ?? ?? ?? ?? ?? F0 47 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??", v119, v120, v121, v122);
                int v0_420 = new byte["BA 00 00 00 00 89 54 24 28 BB FD FF FF FF 89 5C 24 30 8B 4C 24 54 33 C0 89 44 24 2C 85 C9 90 90 8B 54 24 58 85 D2 90 90".split("[ \t]+").length];
                v131 = v0_420;
                int v0_421 = new byte["BA 00 00 00 00 89 54 24 28 BB FD FF FF FF 89 5C 24 30 8B 4C 24 54 33 C0 89 44 24 2C 85 C9 90 90 8B 54 24 58 85 D2 90 90".split("[ \t]+").length];
                v132 = v0_421;
                int v0_422 = new byte["?? 01 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 75 44 ?? ?? ?? ?? ?? ?? 75 23".split("[ \t]+").length];
                v133 = v0_422;
                int v0_423 = new byte["?? 01 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 75 44 ?? ?? ?? ?? ?? ?? 75 23".split("[ \t]+").length];
                v134 = v0_423;
                com.chelpus.Utils.convertStringToArraysPatch("BA 00 00 00 00 89 54 24 28 BB FD FF FF FF 89 5C 24 30 8B 4C 24 54 33 C0 89 44 24 2C 85 C9 90 90 8B 54 24 58 85 D2 90 90", "?? 01 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 75 44 ?? ?? ?? ?? ?? ?? 75 23", v131, v132, v133, v134);
                int v0_424 = new byte["85 ED 0F 85 ?? ?? 00 00 8B 54 24 58 8B 6A 1C 8B 5C 24 24 8B 83 ?? 00 00 00 89 44 24 38 8B 4C 24 38 3B ED".split("[ \t]+").length];
                v137 = v0_424;
                int v0_425 = new byte["85 ED 0F 85 ?? ?? 00 00 8B 54 24 58 8B 6A 1C 8B 5C 24 24 8B 83 ?? 00 00 00 89 44 24 38 8B 4C 24 38 3B ED".split("[ \t]+").length];
                v138 = v0_425;
                int v0_426 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? E9".split("[ \t]+").length];
                v139 = v0_426;
                int v0_427 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? E9".split("[ \t]+").length];
                v140 = v0_427;
                com.chelpus.Utils.convertStringToArraysPatch("85 ED 0F 85 ?? ?? 00 00 8B 54 24 58 8B 6A 1C 8B 5C 24 24 8B 83 ?? 00 00 00 89 44 24 38 8B 4C 24 38 3B ED", "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? E9", v137, v138, v139, v140);
                int v0_428 = new byte["FD 03 1F 2A 42 00 80 12 E2 3B 00 B9 E3 03 1F 2A E3 37 00 B9 FD 03 1F 2A FD 03 1F 2A".split("[ \t]+").length];
                v143 = v0_428;
                int v0_429 = new byte["FD 03 1F 2A 42 00 80 12 E2 3B 00 B9 E3 03 1F 2A E3 37 00 B9 FD 03 1F 2A FD 03 1F 2A".split("[ \t]+").length];
                v144 = v0_429;
                int v0_430 = new byte["3D 00 80 52 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 3B 02 00 B5 9C 01 00 B5".split("[ \t]+").length];
                v145 = v0_430;
                int v0_431 = new byte["3D 00 80 52 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 3B 02 00 B5 9C 01 00 B5".split("[ \t]+").length];
                v146 = v0_431;
                com.chelpus.Utils.convertStringToArraysPatch("FD 03 1F 2A 42 00 80 12 E2 3B 00 B9 E3 03 1F 2A E3 37 00 B9 FD 03 1F 2A FD 03 1F 2A", "3D 00 80 52 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 3B 02 00 B5 9C 01 00 B5", v143, v144, v145, v146);
                int v0_432 = new byte["FC 03 1F 2A 42 00 80 12 E2 3B 00 B9 E3 03 1F 2A E3 37 00 B9 FC 03 1F 2A FC 03 1F 2A".split("[ \t]+").length];
                v149 = v0_432;
                int v0_433 = new byte["FC 03 1F 2A 42 00 80 12 E2 3B 00 B9 E3 03 1F 2A E3 37 00 B9 FC 03 1F 2A FC 03 1F 2A".split("[ \t]+").length];
                v150 = v0_433;
                int v0_434 = new byte["3C 00 80 52 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 3A 02 00 B5 9B 01 00 B5".split("[ \t]+").length];
                v151 = v0_434;
                int v0_435 = new byte["3C 00 80 52 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 3A 02 00 B5 9B 01 00 B5".split("[ \t]+").length];
                v152 = v0_435;
                com.chelpus.Utils.convertStringToArraysPatch("FC 03 1F 2A 42 00 80 12 E2 3B 00 B9 E3 03 1F 2A E3 37 00 B9 FC 03 1F 2A FC 03 1F 2A", "3C 00 80 52 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 3A 02 00 B5 9B 01 00 B5", v149, v150, v151, v152);
                int v0_436 = new byte["E2 33 00 B9 E3 03 14 2A 9F 02 03 6B 6A 13 00 54 A0 16 40 B9".split("[ \t]+").length];
                v155 = v0_436;
                int v0_437 = new byte["E2 33 00 B9 E3 03 14 2A 9F 02 03 6B 6A 13 00 54 A0 16 40 B9".split("[ \t]+").length];
                v156 = v0_437;
                int v0_438 = new byte["?? ?? ?? ?? E3 33 40 B9 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v157 = v0_438;
                int v0_439 = new byte["?? ?? ?? ?? E3 33 40 B9 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v158 = v0_439;
                com.chelpus.Utils.convertStringToArraysPatch("E2 33 00 B9 E3 03 14 2A 9F 02 03 6B 6A 13 00 54 A0 16 40 B9", "?? ?? ?? ?? E3 33 40 B9 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??", v155, v156, v157, v158);
                int v0_440 = new byte["D6 02 19 32 56 01 00 35 E1 03 18 AA E0 03 15 AA 00 0C 40 B9 E2 03 1B AA".split("[ \t]+").length];
                v161 = v0_440;
                int v0_441 = new byte["D6 02 19 32 56 01 00 35 E1 03 18 AA E0 03 15 AA 00 0C 40 B9 E2 03 1B AA".split("[ \t]+").length];
                v162 = v0_441;
                int v0_442 = new byte["D6 02 19 12 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v163 = v0_442;
                int v0_443 = new byte["D6 02 19 12 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v164 = v0_443;
                com.chelpus.Utils.convertStringToArraysPatch("D6 02 19 32 56 01 00 35 E1 03 18 AA E0 03 15 AA 00 0C 40 B9 E2 03 1B AA", "D6 02 19 12 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??", v161, v162, v163, v164);
                int v0_444 = new byte[35];
                v212 = v0_444;
                v212 = {32, 102, 102, 18, 16, 15, 0, 0, 0, 34, 0, 102, 102, 26, 1, 102, 102, 112, 32, 102, 102, 102, 102, 39, 0, 102, 32, 102, 102, 102, 102, 10, 0, 15, 0};
                int v0_445 = new byte[35];
                v294 = v0_445;
                v294 = {0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 1, 1, 0, 0, 1, 0, 1, 1, 1, 1, 0, 0, 0, 0};
                int v0_447 = new byte[35];
                v242 = v0_447;
                v242 = {32, 102, 102, 18, 49, 50, 16, 10, 0, 34, 0, 102, 102, 26, 1, 102, 102, 112, 32, 102, 102, 102, 102, 39, 0, 102, 32, 102, 102, 102, 102, 10, 0, 15, 0};
                int v0_448 = new byte[35];
                v340 = v0_448;
                v340 = {0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
                int v0_450 = new byte[36];
                v215 = v0_450;
                v215 = {82, 32, 102, 102, 18, 16, 15, 0, 0, 0, 34, 0, 102, 102, 26, 1, 102, 102, 112, 32, 102, 102, 102, 102, 39, 0, 110, 32, 102, 102, 102, 102, 10, 0, 15, 0};
                int v0_451 = new byte[36];
                v245 = v0_451;
                v245 = {82, 32, 102, 102, 18, 49, 50, 16, 10, 0, 34, 0, 102, 102, 26, 1, 102, 102, 112, 32, 102, 102, 102, 102, 39, 0, 110, 32, 102, 102, 102, 102, 10, 0, 15, 0};
                int v0_452 = new byte[30];
                v216 = v0_452;
                v216 = {18, 16, 15, 0, 33, 102, 50, 102, 102, 0, 102, 102, 15, 1, 18, 0, 33, 102, 53, 102, 102, 0, 72, 102, 4, 0, 72, 102, 5, 0};
                int v0_453 = new byte[30];
                v297 = v0_453;
                v297 = {0, 0, 0, 0, 0, 1, 0, 1, 1, 0, 1, 1, 0, 0, 0, 0, 0, 1, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0};
                int v0_455 = new byte[30];
                v246 = v0_455;
                v246 = {18, 3, 33, 65, 33, 102, 50, 102, 102, 0, 102, 102, 15, 1, 18, 0, 33, 102, 53, 102, 102, 0, 72, 102, 4, 0, 72, 102, 5, 0};
                int v0_456 = new byte[30];
                v343 = v0_456;
                v343 = {1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
                int v0_458 = new byte[28];
                v217 = v0_458;
                v217 = {18, 16, 15, 0, 33, 102, 50, 102, 102, 0, 15, 1, 18, 0, 33, 102, 53, 102, 102, 0, 72, 102, 4, 0, 72, 102, 5, 0};
                int v0_459 = new byte[28];
                v298 = v0_459;
                v298 = {0, 0, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0};
                int v0_461 = new byte[28];
                v247 = v0_461;
                v247 = {18, 1, 33, 66, 33, 102, 50, 102, 102, 0, 15, 1, 18, 0, 33, 102, 53, 102, 102, 0, 72, 102, 4, 0, 72, 102, 5, 0};
                int v0_462 = new byte[28];
                v344 = v0_462;
                v344 = {1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
                int v0_464 = new byte[43];
                v218 = v0_464;
                v218 = {32, 102, 102, 18, 16, 15, 0, 0, 0, 34, 0, 102, 102, 26, 1, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 112, 32, 102, 102, 102, 102, 39, 0, 102, 32, 102, 102, 102, 102, 10, 0, 15, 0};
                int v0_465 = new byte[43];
                v299 = v0_465;
                v299 = {0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 0, 1, 0, 1, 1, 1, 1, 0, 0, 0, 0};
                int v0_467 = new byte[43];
                v248 = v0_467;
                v248 = {32, 102, 102, 18, 49, 50, 16, 14, 0, 34, 0, 102, 102, 26, 1, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 112, 32, 102, 102, 102, 102, 39, 0, 102, 32, 102, 102, 102, 102, 10, 0, 15, 0};
                int v0_468 = new byte[43];
                v345 = v0_468;
                v345 = {0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
                int v0_470 = new byte[30];
                v219 = v0_470;
                v219 = {82, 32, 102, 102, 18, 16, 15, 0, 0, 0, 34, 0, 102, 102, 26, 1, 102, 102, 112, 32, 102, 102, 102, 102, 39, 0, 56, 3, 11, 0};
                int v0_471 = new byte[30];
                v300 = v0_471;
                v300 = {0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0};
                int v0_473 = new byte[30];
                v249 = v0_473;
                v249 = {82, 32, 102, 102, 18, 49, 50, 16, 10, 0, 34, 0, 102, 102, 26, 1, 102, 102, 112, 32, 102, 102, 102, 102, 39, 0, 56, 3, 11, 0};
                int v0_474 = new byte[30];
                v346 = v0_474;
                v346 = {0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
                int v0_476 = new byte[30];
                v220 = v0_476;
                v220 = {-14, 32, 102, 102, 18, 16, 15, 0, 0, 0, 34, 0, 102, 102, 26, 1, 102, 102, 112, 32, 102, 102, 102, 102, 39, 0, 56, 3, 11, 0};
                int v0_477 = new byte[30];
                v301 = v0_477;
                v301 = {0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0};
                int v0_479 = new byte[30];
                v250 = v0_479;
                v250 = {-14, 32, 102, 102, 18, 49, 50, 16, 10, 0, 34, 0, 102, 102, 26, 1, 102, 102, 112, 32, 102, 102, 102, 102, 39, 0, 56, 3, 11, 0};
                int v0_480 = new byte[30];
                v347 = v0_480;
                v347 = {0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
                int v0_482 = new byte[38];
                v221 = v0_482;
                v221 = {-14, 32, 102, 102, 18, 16, 15, 0, 0, 0, 34, 0, 102, 102, 26, 1, 102, 102, 113, 16, 102, 102, 102, 102, 12, 1, 112, 32, 102, 102, 102, 102, 39, 0, 56, 3, 11, 0};
                int v0_483 = new byte[38];
                v302 = v0_483;
                v302 = {0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0};
                int v0_485 = new byte[38];
                v251 = v0_485;
                v251 = {-14, 32, 102, 102, 18, 49, 50, 16, 14, 0, 34, 0, 102, 102, 26, 1, 102, 102, 113, 16, 102, 102, 102, 102, 12, 1, 112, 32, 102, 102, 102, 102, 39, 0, 56, 3, 11, 0};
                int v0_486 = new byte[38];
                v348 = v0_486;
                v348 = {0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
                int v0_488 = new byte[16];
                v225 = v0_488;
                v225 = {57, 102, 8, 0, 57, 102, 4, 0, 18, 22, 18, 6, 18, 6, 15, 6};
                int v0_489 = new byte[16];
                v306 = v0_489;
                v306 = {0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
                int v0_491 = new byte[16];
                v255 = v0_491;
                v255 = {57, 102, 8, 0, 57, 102, 4, 0, 18, 22, 15, 6, 18, -10, 40, -2};
                int v0_492 = new byte[16];
                v352 = v0_492;
                v352 = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1};
                int v0_494 = new byte[24];
                v226 = v0_494;
                v226 = {18, 102, 56, 102, 102, 102, 26, 102, 102, 102, 26, 102, 102, 102, 113, 102, 102, 102, 102, 102, 19, 102, -19, -1};
                int v0_495 = new byte[24];
                v307 = v0_495;
                v307 = {0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 0, 0};
                int v0_497 = new byte[24];
                v256 = v0_497;
                v256 = {10, 102, 57, 102, 102, 102, 26, 102, 102, 102, 26, 102, 102, 102, 113, 102, 102, 102, 102, 102, 19, 102, -19, -1};
                int v0_499 = new byte[24];
                v353 = v0_499;
                v353 = {1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
                int v0_501 = new byte[19];
                v227 = v0_501;
                v227 = {-128, 0, 57, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 53, -120, 102, 102, 26};
                int v0_502 = new byte[19];
                v308 = v0_502;
                v308 = {0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 0};
                int v0_504 = new byte[19];
                v257 = v0_504;
                v257 = {-128, 0, 57, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 53, -121, 102, 102, 26};
                int v0_506 = new byte[19];
                v354 = v0_506;
                v354 = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0};
                int v0_508 = new byte[14];
                v228 = v0_508;
                v228 = {57, 102, 7, 0, 57, 102, 3, 0, 18, 6, 18, 6, 15, 6};
                int v0_509 = new byte[14];
                v309 = v0_509;
                v309 = {0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0};
                int v0_511 = new byte[14];
                v258 = v0_511;
                v258 = {57, 102, 7, 0, 57, 102, 3, 0, 15, 6, 18, -10, 40, -2};
                int v0_513 = new byte[14];
                v355 = v0_513;
                v355 = {0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1};
                int v0_515 = new byte[20];
                v229 = v0_515;
                v229 = {84, 102, 102, 102, 110, 32, 102, 102, 102, 102, 18, 102, 57, 4, 4, 0, 18, 20, 15, 4};
                int v0_516 = new byte[20];
                v310 = v0_516;
                v310 = {0, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0};
                int v0_518 = new byte[20];
                v259 = v0_518;
                v259 = {84, 102, 102, 102, 110, 32, 102, 102, 102, 102, 10, 102, 56, 4, 4, 0, 18, 20, 15, 4};
                int v0_520 = new byte[20];
                v356 = v0_520;
                v356 = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0};
                int v0_522 = new byte[14];
                v230 = v0_522;
                v230 = {18, 102, 56, 102, 102, 102, 34, 102, 102, 102, 19, 102, -19, -1};
                int v0_523 = new byte[14];
                v311 = v0_523;
                v311 = {0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 0, 0};
                int v0_525 = new byte[14];
                v260 = v0_525;
                v260 = {10, 102, 57, 102, 102, 102, 34, 102, 102, 102, 19, 102, -19, -1};
                int v0_527 = new byte[14];
                v357 = v0_527;
                v357 = {1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
                int v0_529 = new byte[28];
                v231 = v0_529;
                v231 = {29, 102, 102, 102, 102, 102, 102, 102, 12, 102, 51, 102, 102, 0, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 12, 102, 30, 102};
                int v0_530 = new byte[28];
                v312 = v0_530;
                v312 = {0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1};
                int v0_532 = new byte[28];
                v261 = v0_532;
                v261 = {29, 102, 102, 102, 102, 102, 102, 102, 12, 102, 57, 102, 102, 0, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 12, 102, 30, 102};
                int v0_534 = new byte[28];
                v358 = v0_534;
                v358 = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
                v192 = 0;
                int v0_536 = new byte[38];
                v195 = v0_536;
                v195 = {29, 102, 102, 102, 102, 102, 102, 102, 12, 102, 51, 102, 102, 0, 8, 102, 102, 102, 102, 102, 102, 102, 8, 102, 102, 102, 8, 102, 102, 102, 2, 102, 102, 102, 2, 102, 102, 102};
                int v0_537 = new byte[38];
                v196 = v0_537;
                v196 = {0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1};
                int v0_538 = new byte[38];
                v197 = v0_538;
                v197 = {29, 102, 102, 102, 102, 102, 102, 102, 12, 102, 57, 102, 102, 0, 8, 102, 102, 102, 102, 102, 102, 102, 8, 102, 102, 102, 8, 102, 102, 102, 2, 102, 102, 102, 2, 102, 102, 102};
                int v0_539 = new byte[38];
                v198 = v0_539;
                v198 = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
                v200 = 0;
                int v0_540 = new byte[29];
                v222 = v0_540;
                v222 = {56, 102, 102, 102, 8, 102, 102, 102, 8, 102, 102, 102, 102, 102, 102, 102, 102, 102, 18, 102, 56, 102, 102, 102, 56, 102, 102, 102, 26};
                int v0_541 = new byte[29];
                v303 = v0_541;
                v303 = {0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0};
                int v0_543 = new byte[29];
                v252 = v0_543;
                v252 = {56, 102, 102, 102, 8, 102, 102, 102, 8, 102, 102, 102, 102, 102, 102, 102, 102, 102, 10, 102, 57, 102, 102, 102, 56, 102, 102, 102, 26};
                int v0_544 = new byte[29];
                v349 = v0_544;
                v349 = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0};
                int v0_546 = new byte[29];
                v223 = v0_546;
                v223 = {56, 102, 102, 0, 8, 102, 102, 102, 102, 102, 102, 102, 102, 102, 12, 102, 102, 102, 102, 102, 102, 102, 18, 102, 56, 102, 102, 102, 56};
                int v0_547 = new byte[29];
                v304 = v0_547;
                v304 = {0, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0};
                int v0_549 = new byte[29];
                v253 = v0_549;
                v253 = {56, 102, 102, 0, 8, 102, 102, 102, 102, 102, 102, 102, 102, 102, 12, 102, 102, 102, 102, 102, 102, 102, 10, 102, 57, 102, 102, 102, 56};
                int v0_550 = new byte[29];
                v350 = v0_550;
                v350 = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0};
                int v0_552 = new byte[25];
                v224 = v0_552;
                v224 = {56, 102, 102, 102, 8, 102, 102, 102, 102, 102, 102, 102, 102, 102, 18, 102, 56, 102, 102, 102, 56, 102, 102, 102, 26};
                int v0_553 = new byte[25];
                v305 = v0_553;
                v305 = {0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0};
                int v0_555 = new byte[25];
                v254 = v0_555;
                v254 = {56, 102, 102, 102, 8, 102, 102, 102, 102, 102, 102, 102, 102, 102, 10, 102, 57, 102, 102, 102, 56, 102, 102, 102, 26};
                int v0_556 = new byte[25];
                v351 = v0_556;
                v351 = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0};
                int v0_558 = new byte["D6 ?? 80 00 39 ?? ?? ?? 71 20 ?? ?? ?? ?? 54 ?? ?? ?? 52 ?? ?? ?? DD ?? ?? 01 38 ?? ?? 00".split("[ \t]+").length];
                v167 = v0_558;
                int v0_559 = new byte["D6 ?? 80 00 39 ?? ?? ?? 71 20 ?? ?? ?? ?? 54 ?? ?? ?? 52 ?? ?? ?? DD ?? ?? 01 38 ?? ?? 00".split("[ \t]+").length];
                v168 = v0_559;
                int v0_560 = new byte["D5 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v169 = v0_560;
                int v0_561 = new byte["D5 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v170 = v0_561;
                com.chelpus.Utils.convertStringToArraysPatch("D6 ?? 80 00 39 ?? ?? ?? 71 20 ?? ?? ?? ?? 54 ?? ?? ?? 52 ?? ?? ?? DD ?? ?? 01 38 ?? ?? 00", "D5 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??", v167, v168, v169, v170);
                int v0_562 = new byte["39 ?? 07 00 39 ?? 03 00 12 03 12 03 0F 03".split("[ \t]+").length];
                v173 = v0_562;
                int v0_563 = new byte["39 ?? 07 00 39 ?? 03 00 12 03 12 03 0F 03".split("[ \t]+").length];
                v174 = v0_563;
                int v0_564 = new byte["?? ?? ?? ?? ?? ?? ?? ?? 0F 03 12 F3 28 FE".split("[ \t]+").length];
                v175 = v0_564;
                int v0_565 = new byte["?? ?? ?? ?? ?? ?? ?? ?? 0F 03 12 F3 28 FE".split("[ \t]+").length];
                v176 = v0_565;
                com.chelpus.Utils.convertStringToArraysPatch("39 ?? 07 00 39 ?? 03 00 12 03 12 03 0F 03", "?? ?? ?? ?? ?? ?? ?? ?? 0F 03 12 F3 28 FE", v173, v174, v175, v176);
                int v0_566 = new byte[35];
                v202 = v0_566;
                v202 = {32, 102, 102, 18, 49, 50, 16, 10, 0, 34, 0, 102, 102, 26, 1, 102, 102, 112, 32, 102, 102, 102, 102, 39, 0, 102, 32, 102, 102, 102, 102, 18, 16, 15, 0};
                int v0_567 = new byte[35];
                v284 = v0_567;
                v284 = {0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 1, 1, 0, 0, 1, 0, 1, 1, 1, 1, 0, 0, 0, 0};
                int v0_569 = new byte[35];
                v232 = v0_569;
                v232 = {32, 102, 102, 18, 49, 50, 16, 10, 0, 34, 0, 102, 102, 26, 1, 102, 102, 112, 32, 102, 102, 102, 102, 39, 0, 102, 32, 102, 102, 102, 102, 10, 0, 15, 0};
                int v0_570 = new byte[35];
                v330 = v0_570;
                v330 = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0};
                int v0_572 = new byte[36];
                v203 = v0_572;
                v203 = {82, 32, 102, 102, 18, 49, 50, 16, 10, 0, 34, 0, 102, 102, 26, 1, 102, 102, 112, 32, 102, 102, 102, 102, 39, 0, 110, 32, 102, 102, 102, 102, 18, 16, 15, 0};
                int v0_573 = new byte[36];
                v285 = v0_573;
                v285 = {0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0};
                int v0_575 = new byte[36];
                v233 = v0_575;
                v233 = {82, 32, 102, 102, 18, 49, 50, 16, 10, 0, 34, 0, 102, 102, 26, 1, 102, 102, 112, 32, 102, 102, 102, 102, 39, 0, 110, 32, 102, 102, 102, 102, 10, 0, 15, 0};
                int v0_576 = new byte[36];
                v331 = v0_576;
                v331 = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0};
                int v0_578 = new byte[30];
                v204 = v0_578;
                v204 = {18, 19, 33, 65, 33, 102, 50, 102, 102, 0, 102, 102, 15, 1, 18, 0, 33, 102, 53, 102, 102, 0, 72, 102, 4, 0, 72, 102, 5, 0};
                int v0_579 = new byte[30];
                v286 = v0_579;
                v286 = {0, 0, 0, 0, 0, 1, 0, 1, 1, 0, 1, 1, 0, 0, 0, 0, 0, 1, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0};
                int v0_581 = new byte[30];
                v234 = v0_581;
                v234 = {18, 3, 33, 65, 33, 102, 50, 102, 102, 0, 102, 102, 15, 1, 18, 0, 33, 102, 53, 102, 102, 0, 72, 102, 4, 0, 72, 102, 5, 0};
                int v0_582 = new byte[30];
                v332 = v0_582;
                v332 = {1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
                int v0_584 = new byte[28];
                v205 = v0_584;
                v205 = {18, 17, 33, 66, 33, 102, 50, 102, 102, 0, 15, 1, 18, 0, 33, 102, 53, 102, 102, 0, 72, 102, 4, 0, 72, 102, 5, 0};
                int v0_585 = new byte[28];
                v287 = v0_585;
                v287 = {0, 0, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0};
                int v0_587 = new byte[28];
                v235 = v0_587;
                v235 = {18, 1, 33, 66, 33, 102, 50, 102, 102, 0, 15, 1, 18, 0, 33, 102, 53, 102, 102, 0, 72, 102, 4, 0, 72, 102, 5, 0};
                int v0_588 = new byte[28];
                v333 = v0_588;
                v333 = {1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
                int v0_590 = new byte[43];
                v206 = v0_590;
                v206 = {32, 102, 102, 18, 49, 50, 16, 14, 0, 34, 0, 102, 102, 26, 1, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 112, 32, 102, 102, 102, 102, 39, 0, 102, 32, 102, 102, 102, 102, 18, 16, 15, 0};
                int v0_591 = new byte[43];
                v288 = v0_591;
                v288 = {0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 0, 1, 0, 1, 1, 1, 1, 0, 0, 0, 0};
                int v0_593 = new byte[43];
                v236 = v0_593;
                v236 = {32, 102, 102, 18, 49, 50, 16, 14, 0, 34, 0, 102, 102, 26, 1, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 112, 32, 102, 102, 102, 102, 39, 0, 102, 32, 102, 102, 102, 102, 10, 0, 15, 0};
                int v0_594 = new byte[43];
                v334 = v0_594;
                v334 = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0};
                int v0_596 = new byte[44];
                v207 = v0_596;
                v207 = {56, 102, 102, 102, 58, 102, 102, 102, 58, 102, 102, 102, -112, 102, 102, 102, 33, 102, 55, 102, 102, 102, 34, 102, 102, 102, 112, 102, 102, 102, 102, 102, 39, 102, 102, 102, 102, 102, 102, 102, 18, 16, 15, 0};
                int v0_597 = new byte[44];
                v289 = v0_597;
                v289 = {0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0};
                int v0_599 = new byte[44];
                v237 = v0_599;
                v237 = {56, 102, 102, 102, 58, 102, 102, 102, 58, 102, 102, 102, -112, 102, 102, 102, 33, 102, 55, 102, 102, 102, 34, 102, 102, 102, 112, 102, 102, 102, 102, 102, 39, 102, 102, 102, 102, 102, 102, 102, 10, 0, 15, 0};
                int v0_600 = new byte[44];
                v335 = v0_600;
                v335 = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0};
                int v0_602 = new byte[70];
                v208 = v0_602;
                v208 = {56, 102, 102, 102, 58, 102, 102, 102, 58, 102, 102, 102, -112, 102, 102, 102, 33, 102, 55, 102, 102, 102, 34, 102, 102, 102, 112, 102, 102, 102, 102, 102, 39, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 18, 16, 15, 0};
                int v0_603 = new byte[70];
                v290 = v0_603;
                v290 = {0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0};
                int v0_605 = new byte[70];
                v238 = v0_605;
                v238 = {56, 102, 102, 102, 58, 102, 102, 102, 58, 102, 102, 102, -112, 102, 102, 102, 33, 102, 55, 102, 102, 102, 34, 102, 102, 102, 112, 102, 102, 102, 102, 102, 39, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 10, 0, 15, 0};
                int v0_606 = new byte[70];
                v336 = v0_606;
                v336 = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0};
                int v0_608 = new byte[56];
                v209 = v0_608;
                v209 = {56, 102, 102, 102, 58, 102, 102, 102, 58, 102, 102, 102, -112, 102, 102, 102, 33, 102, 55, 102, 102, 102, 34, 102, 102, 102, 26, 102, 102, 102, 113, 102, 102, 102, 102, 102, 12, 102, 112, 102, 102, 102, 102, 102, 39, 102, 102, 102, 102, 102, 102, 102, 18, 16, 15, 0};
                int v0_609 = new byte[56];
                v291 = v0_609;
                v291 = {0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0};
                int v0_611 = new byte[56];
                v239 = v0_611;
                v239 = {56, 102, 102, 102, 58, 102, 102, 102, 58, 102, 102, 102, -112, 102, 102, 102, 33, 102, 55, 102, 102, 102, 34, 102, 102, 102, 26, 102, 102, 102, 113, 102, 102, 102, 102, 102, 12, 102, 112, 102, 102, 102, 102, 102, 39, 102, 102, 102, 102, 102, 102, 102, 10, 0, 15, 0};
                int v0_612 = new byte[56];
                v337 = v0_612;
                v337 = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1};
                int v0_614 = new byte[41];
                v210 = v0_614;
                v210 = {32, 102, 102, 18, 49, 50, 16, 10, 0, 34, 0, 102, 102, 26, 1, 102, 102, 112, 32, 102, 102, 102, 102, 39, 0, 102, 102, 102, 102, 102, 102, 102, 32, 102, 102, 102, 102, 18, 16, 15, 0};
                int v0_615 = new byte[41];
                v292 = v0_615;
                v292 = {0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 0, 0, 0};
                int v0_617 = new byte[41];
                v240 = v0_617;
                v240 = {32, 102, 102, 18, 49, 50, 16, 10, 0, 34, 0, 102, 102, 26, 1, 102, 102, 112, 32, 102, 102, 102, 102, 39, 0, 102, 102, 102, 102, 102, 102, 102, 32, 102, 102, 102, 102, 10, 0, 15, 0};
                int v0_618 = new byte[41];
                v338 = v0_618;
                v338 = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0};
                int v0_620 = new byte[50];
                v211 = v0_620;
                v211 = {56, 102, 102, 102, 58, 102, 102, 102, 58, 102, 102, 102, -112, 102, 102, 102, 33, 102, 55, 102, 102, 102, 34, 102, 102, 102, 112, 102, 102, 102, 102, 102, 39, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 18, 16, 15, 0};
                int v0_621 = new byte[50];
                v293 = v0_621;
                v293 = {0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0};
                int v0_623 = new byte[50];
                v241 = v0_623;
                v241 = {56, 102, 102, 102, 58, 102, 102, 102, 58, 102, 102, 102, -112, 102, 102, 102, 33, 102, 55, 102, 102, 102, 34, 102, 102, 102, 112, 102, 102, 102, 102, 102, 39, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 10, 0, 15, 0};
                int v0_624 = new byte[50];
                v339 = v0_624;
                v339 = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0};
                int v0_626 = new byte[26];
                v213 = v0_626;
                v213 = {18, 18, 33, 83, 33, 102, 50, 102, 102, 0, 15, 2, 18, 1, 18, 0, 33, 102, 53, 102, 102, 0, 72, 102, 5, 0};
                int v0_627 = new byte[26];
                v295 = v0_627;
                v295 = {0, 0, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 1, 0, 0, 1, 0, 0};
                int v0_629 = new byte[26];
                v243 = v0_629;
                v243 = {18, 2, 33, 83, 33, 102, 50, 102, 102, 0, 15, 2, 18, 1, 18, 0, 33, 102, 53, 102, 102, 0, 72, 102, 5, 0};
                int v0_630 = new byte[26];
                v341 = v0_630;
                v341 = {1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
                int v0_632 = new byte[36];
                v214 = v0_632;
                v214 = {-14, 32, 102, 102, 18, 49, 50, 16, 10, 0, 34, 0, 102, 102, 26, 1, 102, 102, 112, 32, 102, 102, 102, 102, 39, 0, -8, 32, 102, 102, 102, 102, 18, 16, 15, 0};
                int v0_633 = new byte[36];
                v296 = v0_633;
                v296 = {0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0};
                int v0_635 = new byte[36];
                v244 = v0_635;
                v244 = {-14, 32, 102, 102, 18, 49, 50, 16, 10, 0, 34, 0, 102, 102, 26, 1, 102, 102, 112, 32, 102, 102, 102, 102, 39, 0, -8, 32, 102, 102, 102, 102, 10, 0, 15, 0};
                int v0_636 = new byte[36];
                v342 = v0_636;
                v342 = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0};
                v105_0 = v328_1;
                v81_0 = v327_1;
                v33_0 = v326_1;
            }
            if ((p362[0].contains("restore")) || ((p362[0].contains("patch")) || (p362[0].contains("all")))) {
                java.util.ArrayList v273_1 = new java.util.ArrayList();
                v273_1.clear();
                if ((v184 != 0) || (v33_0 != 0)) {
                    java.util.ArrayList v313_1 = new java.util.ArrayList();
                    if (new java.io.File("/system/framework/x86/boot.oat").exists()) {
                        v313_1.add(new java.io.File("/system/framework/x86/boot.oat"));
                    }
                    if (new java.io.File("/system/framework/arm64/boot.oat").exists()) {
                        v313_1.add(new java.io.File("/system/framework/arm64/boot.oat"));
                    }
                    if (new java.io.File("/system/framework/arm/boot.oat").exists()) {
                        v313_1.add(new java.io.File("/system/framework/arm/boot.oat"));
                    }
                    if (new java.io.File("/system/framework/oat/x86/boot.oat").exists()) {
                        v313_1.add(new java.io.File("/system/framework/oat/x86/boot.oat"));
                    }
                    if (new java.io.File("/system/framework/oat/arm64/boot.oat").exists()) {
                        v313_1.add(new java.io.File("/system/framework/oat/arm64/boot.oat"));
                    }
                    if (new java.io.File("/system/framework/oat/arm/boot.oat").exists()) {
                        v313_1.add(new java.io.File("/system/framework/oat/arm/boot.oat"));
                    }
                    if ((v313_1.size() > 0) && (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.api >= 21)) {
                        int v325_0 = 0;
                        int v319_0 = 0;
                        int v320_0 = 0;
                        int v321_0 = 0;
                        java.util.Iterator v34_2 = v313_1.iterator();
                        while (v34_2.hasNext()) {
                            java.io.File v271_1 = ((java.io.File) v34_2.next());
                            System.out.println(new StringBuilder().append("oat file for patch:").append(v271_1.getAbsolutePath()).toString());
                            int v361 = 0;
                            int v314 = 0;
                            int v315 = 0;
                            int v316 = 0;
                            try {
                                java.nio.channels.FileChannel v177_4 = new java.io.RandomAccessFile(v271_1, "rw").getChannel();
                                com.chelpus.root.utils.corepatch.fileBytes = v177_4.map(java.nio.channels.FileChannel$MapMode.READ_WRITE, 0, ((long) ((int) v177_4.size())));
                                com.chelpus.root.utils.corepatch.fileBytes.position(4120);
                                int v329_3 = com.chelpus.Utils.convertFourBytesToInt(com.chelpus.root.utils.corepatch.fileBytes.get(), com.chelpus.root.utils.corepatch.fileBytes.get(), com.chelpus.root.utils.corepatch.fileBytes.get(), com.chelpus.root.utils.corepatch.fileBytes.get());
                                System.out.println(new StringBuilder().append("Start position:").append(v329_3).toString());
                                com.chelpus.root.utils.corepatch.fileBytes.position(v329_3);
                            } catch (int v0_1366) {
                                v0_1366.printStackTrace();
                            }
                            try {
                                while (com.chelpus.root.utils.corepatch.fileBytes.hasRemaining()) {
                                    int v178_7 = com.chelpus.root.utils.corepatch.fileBytes.position();
                                    byte v179_7 = com.chelpus.root.utils.corepatch.fileBytes.get();
                                    if (com.chelpus.root.utils.corepatch.applyPatch(v178_7, v179_7, v5, v6, v7, v8, v184)) {
                                        if (p362[0].contains("patch")) {
                                            System.out.println(new StringBuilder().append("position:").append(v178_7).toString());
                                            System.out.println("Oat Core1uni patched!\n");
                                        }
                                        if (p362[0].contains("restore")) {
                                            System.out.println("Oat Core1uni restored!\n");
                                        }
                                        v325_0 = 1;
                                        v361 = 1;
                                        v314 = 1;
                                        v315 = 1;
                                    }
                                    if (com.chelpus.root.utils.corepatch.applyPatch(v178_7, v179_7, v11, v12, v13, v14, v184)) {
                                        if (p362[0].contains("patch")) {
                                            System.out.println(new StringBuilder().append("position:").append(v178_7).toString());
                                            System.out.println("Oat Core1uni patched!\n");
                                        }
                                        if (p362[0].contains("restore")) {
                                            System.out.println("Oat Core1uni restored!\n");
                                        }
                                        v325_0 = 1;
                                        v361 = 1;
                                        v314 = 1;
                                        v315 = 1;
                                    }
                                    if (com.chelpus.root.utils.corepatch.applyPatch(v178_7, v179_7, v17, v18, v19, v20, v184)) {
                                        if (p362[0].contains("patch")) {
                                            System.out.println(new StringBuilder().append("position:").append(v178_7).toString());
                                            System.out.println("Oat Core11 patched!\n");
                                        }
                                        if (p362[0].contains("restore")) {
                                            System.out.println("Oat Core11 restored!\n");
                                        }
                                        v319_0 = 1;
                                        v314 = 1;
                                    }
                                    if (com.chelpus.root.utils.corepatch.applyPatch(v178_7, v179_7, v41, v42, v43, v44, v184)) {
                                        if (p362[0].contains("patch")) {
                                            System.out.println(new StringBuilder().append("position:").append(v178_7).toString());
                                            System.out.println("Oat Core1uni patched!\n");
                                        }
                                        if (p362[0].contains("restore")) {
                                            System.out.println("Oat Core1uni restored!\n");
                                        }
                                        v319_0 = 1;
                                        v314 = 1;
                                        v315 = 1;
                                        v361 = 1;
                                    }
                                    if (com.chelpus.root.utils.corepatch.applyPatch(v178_7, v179_7, v65, v66, v67, v68, v184)) {
                                        if (p362[0].contains("patch")) {
                                            System.out.println(new StringBuilder().append("position:").append(v178_7).toString());
                                            System.out.println("Oat Core1uni patched!\n");
                                        }
                                        if (p362[0].contains("restore")) {
                                            System.out.println("Oat Core1uni restored!\n");
                                        }
                                        v319_0 = 1;
                                        v361 = 1;
                                        v314 = 1;
                                        v315 = 1;
                                    }
                                    if (com.chelpus.root.utils.corepatch.applyPatch(v178_7, v179_7, v47, v48, v49, v50, v184)) {
                                        if (p362[0].contains("patch")) {
                                            System.out.println(new StringBuilder().append("position:").append(v178_7).toString());
                                            System.out.println("Oat Core11 patched!\n");
                                        }
                                        if (p362[0].contains("restore")) {
                                            System.out.println("Oat Core11 restored!\n");
                                        }
                                        v319_0 = 1;
                                        v314 = 1;
                                    }
                                    if (com.chelpus.root.utils.corepatch.applyPatch(v178_7, v179_7, v23, v24, v25, v26, v184)) {
                                        if (p362[0].contains("patch")) {
                                            System.out.println("Oat Core12 patched!\n");
                                            System.out.println(new StringBuilder().append("position:").append(v178_7).toString());
                                        }
                                        if (p362[0].contains("restore")) {
                                            System.out.println("Oat Core12 restored!\n");
                                        }
                                        v320_0 = 1;
                                        v315 = 1;
                                    }
                                    if (com.chelpus.root.utils.corepatch.applyPatch(v178_7, v179_7, v53, v54, v55, v56, v184)) {
                                        if (p362[0].contains("patch")) {
                                            System.out.println("Oat Core12 patched!\n");
                                            System.out.println(new StringBuilder().append("position:").append(v178_7).toString());
                                        }
                                        if (p362[0].contains("restore")) {
                                            System.out.println("Oat Core12 restored!\n");
                                        }
                                        v320_0 = 1;
                                        v315 = 1;
                                    }
                                    if (com.chelpus.root.utils.corepatch.applyPatch(v178_7, v179_7, v29, v30, v31, v32, v33_0)) {
                                        if (p362[0].contains("patch")) {
                                            System.out.println("Oat Core2 patched!\n");
                                            System.out.println(new StringBuilder().append("position:").append(v178_7).toString());
                                        }
                                        if (p362[0].contains("restore")) {
                                            System.out.println("Oat Core2 restored!\n");
                                        }
                                        v321_0 = 1;
                                        v316 = 1;
                                    }
                                    if (com.chelpus.root.utils.corepatch.applyPatch(v178_7, v179_7, v35, v36, v37, v38, v33_0)) {
                                        if (p362[0].contains("patch")) {
                                            System.out.println("Oat Core2 patched!\n");
                                            System.out.println(new StringBuilder().append("position:").append(v178_7).toString());
                                        }
                                        if (p362[0].contains("restore")) {
                                            System.out.println("Oat Core2 restored!\n");
                                        }
                                        v321_0 = 1;
                                        v316 = 1;
                                    }
                                    if (com.chelpus.root.utils.corepatch.applyPatch(v178_7, v179_7, v59, v60, v61, v62, v33_0)) {
                                        if (p362[0].contains("patch")) {
                                            System.out.println("Oat Core2 patched!\n");
                                            System.out.println(new StringBuilder().append("position:").append(v178_7).toString());
                                        }
                                        if (p362[0].contains("restore")) {
                                            System.out.println("Oat Core2 restored!\n");
                                        }
                                        v321_0 = 1;
                                        v316 = 1;
                                    }
                                    if (com.chelpus.root.utils.corepatch.applyPatch(v178_7, v179_7, v71, v72, v73, v74, v33_0)) {
                                        if (p362[0].contains("patch")) {
                                            System.out.println("Oat Core2 patched!\n");
                                            System.out.println(new StringBuilder().append("position:").append(v178_7).toString());
                                        }
                                        if (p362[0].contains("restore")) {
                                            System.out.println("Oat Core2 restored!\n");
                                        }
                                        v321_0 = 1;
                                        v316 = 1;
                                    }
                                    if (((v184 != 0) && ((v33_0 != 0) && ((v361 != 0) && (v316 != 0)))) || (((v184 != 0) && ((v33_0 != 0) && ((v361 != 0) && ((v314 != 0) && ((v315 != 0) && (v316 != 0)))))) || (((v184 != 0) && ((v33_0 == 0) && (v361 != 0))) || (((v184 != 0) && ((v33_0 == 0) && ((v361 != 0) && ((v314 != 0) && (v315 != 0))))) || ((v184 == 0) && ((v33_0 != 0) && (v316 != 0))))))) {
                                        break;
                                    }
                                    com.chelpus.root.utils.corepatch.fileBytes.position((v178_7 + 1));
                                }
                            } catch (int v0_1364) {
                                System.out.println(new StringBuilder().append("").append(v0_1364).toString());
                            }
                            v177_4.close();
                        }
                        if ((v325_0 != 0) || ((v319_0 != 0) || ((v320_0 != 0) || (v321_0 != 0)))) {
                            new java.io.File("/data/dalvik-cache/arm/system@framework@boot.oat").delete();
                            new java.io.File("/data/dalvik-cache/arm/system@framework@boot.art").delete();
                            new java.io.File("/data/dalvik-cache/arm64/system@framework@boot.oat").delete();
                            new java.io.File("/data/dalvik-cache/arm64/system@framework@boot.art").delete();
                            new java.io.File("/data/dalvik-cache/x86/system@framework@boot.oat").delete();
                            new java.io.File("/data/dalvik-cache/x86/system@framework@boot.art").delete();
                            String[] v9_1045 = new String[1];
                            v9_1045[0] = new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.toolfilesdir).append("/reboot").toString();
                            com.chelpus.Utils.cmdParam(v9_1045);
                        }
                    }
                    try {
                        if (!p362[4].contains("framework")) {
                            if (!p362[4].contains("ART")) {
                                try {
                                    if (!com.chelpus.root.utils.corepatch.onlyDalvik) {
                                        java.io.File v282_0 = new java.io.File;
                                        v282_0(p362[1]);
                                        if (v282_0.exists()) {
                                            if (v282_0.toString().contains("system@framework@core.jar@classes.dex")) {
                                                java.io.File v264_0 = new java.io.File;
                                                v264_0("/system/framework/core.odex");
                                                if ((v264_0.exists()) && (v264_0.length() == 0)) {
                                                    v264_0.delete();
                                                }
                                            }
                                            v273_1.add(v282_0);
                                            java.util.Iterator v34_3 = v273_1.iterator();
                                            while (v34_3.hasNext()) {
                                                java.io.File v281_0 = ((java.io.File) v34_3.next());
                                                System.out.println(new StringBuilder().append("file for patch: ").append(v281_0.getAbsolutePath()).append(" size:").append(v281_0.length()).toString());
                                                java.nio.channels.FileChannel v177_0 = new java.io.RandomAccessFile(v281_0, "rw").getChannel();
                                                com.chelpus.root.utils.corepatch.fileBytes = v177_0.map(java.nio.channels.FileChannel$MapMode.READ_WRITE, 0, ((long) ((int) v177_0.size())));
                                                int v317 = 0;
                                                if (v281_0.getName().contains("boot.oat")) {
                                                    int v319_1 = 0;
                                                    int v320_1 = 0;
                                                    int v321_1 = 0;
                                                    com.chelpus.root.utils.corepatch.fileBytes.position(4120);
                                                    int v329_0 = com.chelpus.Utils.convertFourBytesToInt(com.chelpus.root.utils.corepatch.fileBytes.get(), com.chelpus.root.utils.corepatch.fileBytes.get(), com.chelpus.root.utils.corepatch.fileBytes.get(), com.chelpus.root.utils.corepatch.fileBytes.get());
                                                    System.out.println(new StringBuilder().append("Start position:").append(v329_0).toString());
                                                    com.chelpus.root.utils.corepatch.fileBytes.position(v329_0);
                                                    try {
                                                        while (com.chelpus.root.utils.corepatch.fileBytes.hasRemaining()) {
                                                            int v178_0 = com.chelpus.root.utils.corepatch.fileBytes.position();
                                                            byte v179_0 = com.chelpus.root.utils.corepatch.fileBytes.get();
                                                            if (com.chelpus.root.utils.corepatch.applyPatch(v178_0, v179_0, v5, v6, v7, v8, v184)) {
                                                                if (p362[0].contains("patch")) {
                                                                    System.out.println(new StringBuilder().append("position:").append(v178_0).toString());
                                                                    System.out.println("Oat Core1uni patched!\n");
                                                                }
                                                                if (p362[0].contains("restore")) {
                                                                    System.out.println("Oat Core1uni restored!\n");
                                                                }
                                                                v319_1 = 1;
                                                                v317 = 1;
                                                            }
                                                            if (com.chelpus.root.utils.corepatch.applyPatch(v178_0, v179_0, v11, v12, v13, v14, v184)) {
                                                                if (p362[0].contains("patch")) {
                                                                    System.out.println(new StringBuilder().append("position:").append(v178_0).toString());
                                                                    System.out.println("Oat Core1uni patched!\n");
                                                                }
                                                                if (p362[0].contains("restore")) {
                                                                    System.out.println("Oat Core1uni restored!\n");
                                                                }
                                                                v319_1 = 1;
                                                                v317 = 1;
                                                            }
                                                            if (com.chelpus.root.utils.corepatch.applyPatch(v178_0, v179_0, v17, v18, v19, v20, v184)) {
                                                                if (p362[0].contains("patch")) {
                                                                    System.out.println(new StringBuilder().append("position:").append(v178_0).toString());
                                                                    System.out.println("Oat Core11 patched!\n");
                                                                }
                                                                if (p362[0].contains("restore")) {
                                                                    System.out.println("Oat Core11 restored!\n");
                                                                }
                                                                v319_1 = 1;
                                                                v317 = 1;
                                                            }
                                                            if (com.chelpus.root.utils.corepatch.applyPatch(v178_0, v179_0, v41, v42, v43, v44, v184)) {
                                                                if (p362[0].contains("patch")) {
                                                                    System.out.println(new StringBuilder().append("position:").append(v178_0).toString());
                                                                    System.out.println("Oat Core1uni patched!\n");
                                                                }
                                                                if (p362[0].contains("restore")) {
                                                                    System.out.println("Oat Core1uni restored!\n");
                                                                }
                                                                v319_1 = 1;
                                                                v317 = 1;
                                                            }
                                                            if (com.chelpus.root.utils.corepatch.applyPatch(v178_0, v179_0, v65, v66, v67, v68, v184)) {
                                                                if (p362[0].contains("patch")) {
                                                                    System.out.println(new StringBuilder().append("position:").append(v178_0).toString());
                                                                    System.out.println("Oat Core1uni patched!\n");
                                                                }
                                                                if (p362[0].contains("restore")) {
                                                                    System.out.println("Oat Core1uni restored!\n");
                                                                }
                                                                v319_1 = 1;
                                                                v317 = 1;
                                                            }
                                                            if (com.chelpus.root.utils.corepatch.applyPatch(v178_0, v179_0, v47, v48, v49, v50, v184)) {
                                                                if (p362[0].contains("patch")) {
                                                                    System.out.println(new StringBuilder().append("position:").append(v178_0).toString());
                                                                    System.out.println("Oat Core11 patched!\n");
                                                                }
                                                                if (p362[0].contains("restore")) {
                                                                    System.out.println("Oat Core11 restored!\n");
                                                                }
                                                                v319_1 = 1;
                                                                v317 = 1;
                                                            }
                                                            if (com.chelpus.root.utils.corepatch.applyPatch(v178_0, v179_0, v23, v24, v25, v26, v184)) {
                                                                if (p362[0].contains("patch")) {
                                                                    System.out.println("Oat Core12 patched!\n");
                                                                    System.out.println(new StringBuilder().append("position:").append(v178_0).toString());
                                                                }
                                                                if (p362[0].contains("restore")) {
                                                                    System.out.println("Oat Core12 restored!\n");
                                                                }
                                                                v320_1 = 1;
                                                                v317 = 1;
                                                            }
                                                            if (com.chelpus.root.utils.corepatch.applyPatch(v178_0, v179_0, v53, v54, v55, v56, v184)) {
                                                                if (p362[0].contains("patch")) {
                                                                    System.out.println("Oat Core12 patched!\n");
                                                                    System.out.println(new StringBuilder().append("position:").append(v178_0).toString());
                                                                }
                                                                if (p362[0].contains("restore")) {
                                                                    System.out.println("Oat Core12 restored!\n");
                                                                }
                                                                v320_1 = 1;
                                                                v317 = 1;
                                                            }
                                                            if (com.chelpus.root.utils.corepatch.applyPatch(v178_0, v179_0, v29, v30, v31, v32, v33_0)) {
                                                                if (p362[0].contains("patch")) {
                                                                    System.out.println("Oat Core2 patched!\n");
                                                                    System.out.println(new StringBuilder().append("position:").append(v178_0).toString());
                                                                }
                                                                if (p362[0].contains("restore")) {
                                                                    System.out.println("Oat Core2 restored!\n");
                                                                }
                                                                v321_1 = 1;
                                                                v317 = 1;
                                                            }
                                                            if (com.chelpus.root.utils.corepatch.applyPatch(v178_0, v179_0, v35, v36, v37, v38, v33_0)) {
                                                                if (p362[0].contains("patch")) {
                                                                    System.out.println("Oat Core2 patched!\n");
                                                                    System.out.println(new StringBuilder().append("position:").append(v178_0).toString());
                                                                }
                                                                if (p362[0].contains("restore")) {
                                                                    System.out.println("Oat Core2 restored!\n");
                                                                }
                                                                v321_1 = 1;
                                                                v317 = 1;
                                                            }
                                                            if (com.chelpus.root.utils.corepatch.applyPatch(v178_0, v179_0, v59, v60, v61, v62, v33_0)) {
                                                                if (p362[0].contains("patch")) {
                                                                    System.out.println("Oat Core2 patched!\n");
                                                                    System.out.println(new StringBuilder().append("position:").append(v178_0).toString());
                                                                }
                                                                if (p362[0].contains("restore")) {
                                                                    System.out.println("Oat Core2 restored!\n");
                                                                }
                                                                v321_1 = 1;
                                                                v317 = 1;
                                                            }
                                                            if (com.chelpus.root.utils.corepatch.applyPatch(v178_0, v179_0, v71, v72, v73, v74, v33_0)) {
                                                                if (p362[0].contains("patch")) {
                                                                    System.out.println("Oat Core2 patched!\n");
                                                                    System.out.println(new StringBuilder().append("position:").append(v178_0).toString());
                                                                }
                                                                if (p362[0].contains("restore")) {
                                                                    System.out.println("Oat Core2 restored!\n");
                                                                }
                                                                v321_1 = 1;
                                                                v317 = 1;
                                                            }
                                                            if (((v184 == 0) || (((v33_0 & 0) == 0) || ((v319_1 == 0) || ((v320_1 == 0) || (v321_1 == 0))))) && (((v184 == 0) || ((v33_0 != 0) || ((0 == 0) || ((v319_1 == 0) || (v320_1 == 0))))) && ((v184 != 0) || ((v33_0 == 0) || (v321_1 == 0))))) {
                                                                com.chelpus.root.utils.corepatch.fileBytes.position((v178_0 + 1));
                                                            } else {
                                                                v317 = 1;
                                                                break;
                                                            }
                                                        }
                                                    } catch (int v0_781) {
                                                        System.out.println(new StringBuilder().append("").append(v0_781).toString());
                                                    }
                                                } else {
                                                    long v279_0 = 0;
                                                    try {
                                                        while (com.chelpus.root.utils.corepatch.fileBytes.hasRemaining()) {
                                                            int v178_1 = com.chelpus.root.utils.corepatch.fileBytes.position();
                                                            byte v179_1 = com.chelpus.root.utils.corepatch.fileBytes.get();
                                                            if (com.chelpus.root.utils.corepatch.applyPatch(v178_1, v179_1, v212, v294, v242, v340, v184)) {
                                                                if (p362[0].contains("patch")) {
                                                                    System.out.println("Core patched!\n");
                                                                }
                                                                if (p362[0].contains("restore")) {
                                                                    System.out.println("Core restored!\n");
                                                                }
                                                                v317 = 1;
                                                            }
                                                            if (com.chelpus.root.utils.corepatch.applyPatch(v178_1, v179_1, v215, v294, v245, v340, v184)) {
                                                                if (p362[0].contains("patch")) {
                                                                    System.out.println("Core patched!\n");
                                                                }
                                                                if (p362[0].contains("restore")) {
                                                                    System.out.println("Core restored!\n");
                                                                }
                                                                v317 = 1;
                                                            }
                                                            if (com.chelpus.root.utils.corepatch.applyPatch(v178_1, v179_1, v216, v297, v246, v343, v33_0)) {
                                                                if (p362[0].contains("patch")) {
                                                                    System.out.println("Core unsigned install patched!\n");
                                                                }
                                                                if (p362[0].contains("restore")) {
                                                                    System.out.println("Core unsigned install restored!\n");
                                                                }
                                                                v317 = 1;
                                                            }
                                                            if (com.chelpus.root.utils.corepatch.applyPatch(v178_1, v179_1, v217, v298, v247, v344, v33_0)) {
                                                                if (p362[0].contains("patch")) {
                                                                    System.out.println("Core unsigned install patched!\n");
                                                                }
                                                                if (p362[0].contains("restore")) {
                                                                    System.out.println("Core unsigned install restored!\n");
                                                                }
                                                                v317 = 1;
                                                            }
                                                            if (com.chelpus.root.utils.corepatch.applyPatch(v178_1, v179_1, v218, v299, v248, v345, v184)) {
                                                                if (p362[0].contains("patch")) {
                                                                    System.out.println("Core patched!\n");
                                                                }
                                                                if (p362[0].contains("restore")) {
                                                                    System.out.println("Core restored!\n");
                                                                }
                                                                v317 = 1;
                                                            }
                                                            if (com.chelpus.root.utils.corepatch.applyPatch(v178_1, v179_1, v219, v300, v249, v346, v184)) {
                                                                if (p362[0].contains("patch")) {
                                                                    System.out.println("Core 2 patched!\n");
                                                                }
                                                                if (p362[0].contains("restore")) {
                                                                    System.out.println("Core 2 restored!\n");
                                                                }
                                                                v317 = 1;
                                                            }
                                                            if (com.chelpus.root.utils.corepatch.applyPatch(v178_1, v179_1, v220, v301, v250, v347, v184)) {
                                                                if (p362[0].contains("patch")) {
                                                                    System.out.println("Core 2 patched!\n");
                                                                }
                                                                if (p362[0].contains("restore")) {
                                                                    System.out.println("Core 2 restored!\n");
                                                                }
                                                                v317 = 1;
                                                            }
                                                            if (com.chelpus.root.utils.corepatch.applyPatch(v178_1, v179_1, v221, v302, v251, v348, v184)) {
                                                                if (p362[0].contains("patch")) {
                                                                    System.out.println("Core 2 patched!\n");
                                                                }
                                                                if (p362[0].contains("restore")) {
                                                                    System.out.println("Core 2 restored!\n");
                                                                }
                                                                v317 = 1;
                                                            }
                                                            if (com.chelpus.root.utils.corepatch.applyPatch(v178_1, v179_1, v202, v284, v232, v330, v184)) {
                                                                if (p362[0].contains("patch")) {
                                                                    System.out.println("Core patched!\n");
                                                                }
                                                                if (p362[0].contains("restore")) {
                                                                    System.out.println("Core restored!\n");
                                                                }
                                                                v317 = 1;
                                                            }
                                                            if (com.chelpus.root.utils.corepatch.applyPatch(v178_1, v179_1, v203, v285, v233, v331, v184)) {
                                                                if (p362[0].contains("patch")) {
                                                                    System.out.println("Core patched!\n");
                                                                }
                                                                if (p362[0].contains("restore")) {
                                                                    System.out.println("Core restored!\n");
                                                                }
                                                                v317 = 1;
                                                            }
                                                            if (com.chelpus.root.utils.corepatch.applyPatch(v178_1, v179_1, v204, v286, v234, v332, v33_0)) {
                                                                if (p362[0].contains("patch")) {
                                                                    System.out.println("Core unsigned install patched!\n");
                                                                }
                                                                if (p362[0].contains("restore")) {
                                                                    System.out.println("Core unsigned install restored!\n");
                                                                }
                                                                v317 = 1;
                                                            }
                                                            if (com.chelpus.root.utils.corepatch.applyPatch(v178_1, v179_1, v205, v287, v235, v333, v33_0)) {
                                                                if (p362[0].contains("patch")) {
                                                                    System.out.println("Core unsigned install patched!\n");
                                                                }
                                                                if (p362[0].contains("restore")) {
                                                                    System.out.println("Core unsigned install restored!\n");
                                                                }
                                                                v317 = 1;
                                                            }
                                                            if (com.chelpus.root.utils.corepatch.applyPatch(v178_1, v179_1, v213, v295, v243, v341, v33_0)) {
                                                                if (p362[0].contains("patch")) {
                                                                    System.out.println("Core unsigned install patched!\n");
                                                                }
                                                                if (p362[0].contains("restore")) {
                                                                    System.out.println("Core unsigned install restored!\n");
                                                                }
                                                                v317 = 1;
                                                            }
                                                            if (com.chelpus.root.utils.corepatch.applyPatch(v178_1, v179_1, v214, v296, v244, v342, v184)) {
                                                                if (p362[0].contains("patch")) {
                                                                    System.out.println("Core patched!\n");
                                                                }
                                                                if (p362[0].contains("restore")) {
                                                                    System.out.println("Core restored!\n");
                                                                }
                                                                v317 = 1;
                                                            }
                                                            if (com.chelpus.root.utils.corepatch.applyPatch(v178_1, v179_1, v206, v288, v236, v334, v184)) {
                                                                if (p362[0].contains("patch")) {
                                                                    System.out.println("Core patched!\n");
                                                                }
                                                                if (p362[0].contains("restore")) {
                                                                    System.out.println("Core restored!\n");
                                                                }
                                                                v317 = 1;
                                                            }
                                                            if (com.chelpus.root.utils.corepatch.applyPatch(v178_1, v179_1, v207, v289, v237, v335, v184)) {
                                                                if (p362[0].contains("patch")) {
                                                                    System.out.println("Core 2 patched!\n");
                                                                }
                                                                if (p362[0].contains("restore")) {
                                                                    System.out.println("Core 2 restored!\n");
                                                                }
                                                                v317 = 1;
                                                            }
                                                            if (com.chelpus.root.utils.corepatch.applyPatch(v178_1, v179_1, v208, v290, v238, v336, v184)) {
                                                                if (p362[0].contains("patch")) {
                                                                    System.out.println("Core 2 patched!\n");
                                                                }
                                                                if (p362[0].contains("restore")) {
                                                                    System.out.println("Core 2 restored!\n");
                                                                }
                                                                v317 = 1;
                                                            }
                                                            if (com.chelpus.root.utils.corepatch.applyPatch(v178_1, v179_1, v209, v291, v239, v337, v184)) {
                                                                if (p362[0].contains("patch")) {
                                                                    System.out.println("Core 2 patched!\n");
                                                                }
                                                                if (p362[0].contains("restore")) {
                                                                    System.out.println("Core 2 restored!\n");
                                                                }
                                                                v317 = 1;
                                                            }
                                                            if (com.chelpus.root.utils.corepatch.applyPatch(v178_1, v179_1, v210, v292, v240, v338, v184)) {
                                                                if (p362[0].contains("patch")) {
                                                                    System.out.println("Core 2 patched!\n");
                                                                }
                                                                if (p362[0].contains("restore")) {
                                                                    System.out.println("Core 2 restored!\n");
                                                                }
                                                                v317 = 1;
                                                            }
                                                            if (com.chelpus.root.utils.corepatch.applyPatch(v178_1, v179_1, v211, v293, v241, v339, v184)) {
                                                                if (p362[0].contains("patch")) {
                                                                    System.out.println("Core 2 patched!\n");
                                                                }
                                                                if (p362[0].contains("restore")) {
                                                                    System.out.println("Core 2 restored!\n");
                                                                }
                                                                v317 = 1;
                                                            }
                                                            com.chelpus.root.utils.corepatch.fileBytes.position((v178_1 + 1));
                                                            v279_0++;
                                                        }
                                                    } catch (int v0_844) {
                                                        System.out.println(new StringBuilder().append("").append(v0_844).toString());
                                                    }
                                                }
                                                v177_0.close();
                                                if (!p362[4].contains("framework")) {
                                                    if (!v281_0.toString().endsWith("/classes.dex")) {
                                                        com.chelpus.Utils.fixadlerOdex(v281_0, "/system/framework/core.jar");
                                                    } else {
                                                        com.chelpus.Utils.fixadler(v281_0);
                                                    }
                                                    if (!p362[4].contains("ART")) {
                                                        if (!com.chelpus.root.utils.corepatch.onlyDalvik) {
                                                            if ((v281_0.toString().contains("system@framework@core.jar@classes.dex")) && (v317 != 0)) {
                                                                System.out.println("LuckyPatcher: dalvik-cache patched! ");
                                                                java.io.File v265 = new java.io.File;
                                                                v265("/system/framework/core.patched");
                                                                if (!com.chelpus.Utils.copyFile(v281_0.getAbsolutePath(), "/system/framework/core.patched", 1, 0)) {
                                                                    v265.delete();
                                                                    System.out.println("LuckyPatcher: not space to system for odex core.jar! ");
                                                                    System.out.println("LuckyPatcher: odex not equal lenght patched! Not enougth space in /system/!");
                                                                } else {
                                                                    String[] v9_1459 = new String[3];
                                                                    v9_1459[0] = "chmod";
                                                                    v9_1459[1] = "0644";
                                                                    v9_1459[2] = "/system/framework/core.patched";
                                                                    com.chelpus.Utils.run_all_no_root(v9_1459);
                                                                    String[] v9_1461 = new String[3];
                                                                    v9_1461[0] = "chown";
                                                                    v9_1461[1] = "0.0";
                                                                    v9_1461[2] = "/system/framework/core.patched";
                                                                    com.chelpus.Utils.run_all_no_root(v9_1461);
                                                                    String[] v9_1463 = new String[3];
                                                                    v9_1463[0] = "chown";
                                                                    v9_1463[1] = "0:0";
                                                                    v9_1463[2] = "/system/framework/core.patched";
                                                                    com.chelpus.Utils.run_all_no_root(v9_1463);
                                                                }
                                                            }
                                                            java.io.File v263_0 = new java.io.File;
                                                            v263_0("/system/framework/core.patched");
                                                            if (v263_0.exists()) {
                                                                System.out.println("LuckyPatcher: root found core.patched! ");
                                                            }
                                                            java.io.File v263_1 = new java.io.File;
                                                            v263_1("/system/framework/core.odex");
                                                            if (v263_1.exists()) {
                                                                System.out.println("LuckyPatcher: root found core.odex! ");
                                                            }
                                                        }
                                                    } else {
                                                        if ((v281_0.toString().contains("/classes.dex")) && (v317 != 0)) {
                                                            System.out.println("start");
                                                            if (!com.chelpus.Utils.copyFile("/system/framework/core-libart.jar", "/system/framework/core-libart.backup", 1, 0)) {
                                                                new java.io.File("/system/framework/core-libart.backup").delete();
                                                                System.out.println("LuckyPatcher: odex not equal lenght patched! Not enougth space in /system/!");
                                                            } else {
                                                                System.out.println("good space");
                                                                new java.io.File("/system/framework/core-libart.backup").delete();
                                                                java.util.ArrayList v272_1 = new java.util.ArrayList();
                                                                System.out.println("add files");
                                                                v272_1.add(new com.android.vending.billing.InAppBillingService.LUCK.AddFilesItem("/data/app/classes.dex", "/data/app/"));
                                                                com.chelpus.Utils.addFilesToZip("/system/framework/core-libart.jar", "/system/framework/core-libart.backup", v272_1);
                                                                System.out.println("add files finish");
                                                                String[] v9_1487 = new String[3];
                                                                v9_1487[0] = "chmod";
                                                                v9_1487[1] = "0644";
                                                                v9_1487[2] = "/system/framework/core-libart.backup";
                                                                com.chelpus.Utils.run_all_no_root(v9_1487);
                                                                String[] v9_1489 = new String[3];
                                                                v9_1489[0] = "chown";
                                                                v9_1489[1] = "0:0";
                                                                v9_1489[2] = "/system/framework/core-libart.backup";
                                                                com.chelpus.Utils.run_all_no_root(v9_1489);
                                                                String[] v9_1491 = new String[3];
                                                                v9_1491[0] = "chmod";
                                                                v9_1491[1] = "0.0";
                                                                v9_1491[2] = "/system/framework/core-libart.backup";
                                                                com.chelpus.Utils.run_all_no_root(v9_1491);
                                                                String[] v9_1493 = new String[2];
                                                                v9_1493[0] = "rm";
                                                                v9_1493[1] = "/system/framework/core-libart.jar";
                                                                com.chelpus.Utils.run_all_no_root(v9_1493);
                                                                if (new java.io.File("/system/framework/core-libart.jar").exists()) {
                                                                    new java.io.File("/system/framework/core-libart.jar").delete();
                                                                }
                                                                String[] v9_1500 = new String[3];
                                                                v9_1500[0] = "mv";
                                                                v9_1500[1] = "/system/framework/core.backup";
                                                                v9_1500[2] = "/system/framework/core-libart.jar";
                                                                com.chelpus.Utils.run_all_no_root(v9_1500);
                                                                if (!new java.io.File("/system/framework/core-libart.jar").exists()) {
                                                                    new java.io.File("/system/framework/core-libart.backup").renameTo(new java.io.File("/system/framework/core-libart.jar"));
                                                                }
                                                                String[] v9_1507 = new String[3];
                                                                v9_1507[0] = "chmod";
                                                                v9_1507[1] = "0644";
                                                                v9_1507[2] = "/system/framework/core-libart.jar";
                                                                com.chelpus.Utils.run_all_no_root(v9_1507);
                                                                String[] v9_1509 = new String[3];
                                                                v9_1509[0] = "chown";
                                                                v9_1509[1] = "0:0";
                                                                v9_1509[2] = "/system/framework/core-libart.jar";
                                                                com.chelpus.Utils.run_all_no_root(v9_1509);
                                                                String[] v9_1511 = new String[3];
                                                                v9_1511[0] = "chmod";
                                                                v9_1511[1] = "0.0";
                                                                v9_1511[2] = "/system/framework/core-libart.jar";
                                                                com.chelpus.Utils.run_all_no_root(v9_1511);
                                                                java.io.File v268 = com.chelpus.Utils.getFileDalvikCache("/system/framework/core-libart.jar");
                                                                if (v268 != null) {
                                                                    String[] v9_1514 = new String[2];
                                                                    v9_1514[0] = "rm";
                                                                    v9_1514[1] = v268.getAbsolutePath();
                                                                    com.chelpus.Utils.run_all_no_root(v9_1514);
                                                                    if (v268.exists()) {
                                                                        v268.delete();
                                                                    }
                                                                }
                                                                new java.io.File("/data/dalvik-cache/arm/system@framework@arm@boot.oat").delete();
                                                                new java.io.File("/data/dalvik-cache/arm/system@framework@arm@boot.art").delete();
                                                                new java.io.File("/data/dalvik-cache/arm64/system@framework@arm@boot.oat").delete();
                                                                new java.io.File("/data/dalvik-cache/arm64/system@framework@arm@boot.art").delete();
                                                                new java.io.File("/data/dalvik-cache/x86/system@framework@arm@boot.oat").delete();
                                                                new java.io.File("/data/dalvik-cache/x86/system@framework@arm@boot.art").delete();
                                                                System.out.println("finish");
                                                            }
                                                        }
                                                    }
                                                } else {
                                                    if (v281_0.toString().endsWith("/classes.dex")) {
                                                        com.chelpus.root.utils.corepatch.not_found_bytes_for_patch = 0;
                                                        com.chelpus.Utils.fixadler(v281_0);
                                                        if (v317 == 0) {
                                                            com.chelpus.root.utils.corepatch.not_found_bytes_for_patch = 1;
                                                        } else {
                                                            String v269_2 = p362[1].replace(".jar", "-patched.jar");
                                                            String v360_2 = p362[1];
                                                            new java.io.File(v269_2).delete();
                                                            java.util.ArrayList v272_3 = new java.util.ArrayList();
                                                            System.out.println("add files");
                                                            v272_3.add(new com.android.vending.billing.InAppBillingService.LUCK.AddFilesItem(v281_0.getAbsolutePath(), new StringBuilder().append(com.chelpus.Utils.getDirs(v281_0).getAbsolutePath()).append("/").toString()));
                                                            com.chelpus.Utils.addFilesToZip(v360_2, v269_2, v272_3);
                                                            System.out.println("add files finish");
                                                            new java.io.File(v360_2).delete();
                                                        }
                                                    }
                                                    if (v281_0.toString().endsWith("/core.odex")) {
                                                        com.chelpus.Utils.fixadlerOdex(v281_0, 0);
                                                        if (v317 != 0) {
                                                            v281_0.renameTo(new java.io.File(v281_0.getAbsolutePath().replace("/core.odex", "/core-patched.odex")));
                                                        }
                                                    }
                                                    if ((v281_0.toString().endsWith("/boot.oat")) && (v317 != 0)) {
                                                        v281_0.renameTo(new java.io.File(v281_0.getAbsolutePath().replace("/boot.oat", "/boot-patched.oat")));
                                                    }
                                                }
                                            }
                                            if ((v81_0 != 0) || (v105_0 != 0)) {
                                                System.out.println("Start patch for services.jar");
                                                String v277 = "";
                                                if (new java.io.File("/system/framework/arm/services.odex").exists()) {
                                                    v277 = "/arm";
                                                }
                                                if (new java.io.File("/system/framework/arm64/services.odex").exists()) {
                                                    v277 = "/arm64";
                                                }
                                                if (new java.io.File("/system/framework/x86/services.odex").exists()) {
                                                    v277 = "/x86";
                                                }
                                                if (new java.io.File("/system/framework/arm/services.odex.xz").exists()) {
                                                    v277 = "/arm";
                                                }
                                                if (new java.io.File("/system/framework/arm64/services.odex.xz").exists()) {
                                                    v277 = "/arm64";
                                                }
                                                if (new java.io.File("/system/framework/x86/services.odex.xz").exists()) {
                                                    v277 = "/x86";
                                                }
                                                if (new java.io.File("/system/framework/oat/arm/services.odex").exists()) {
                                                    v277 = "/oat/arm";
                                                }
                                                if (new java.io.File("/system/framework/oat/arm64/services.odex").exists()) {
                                                    v277 = "/oat/arm64";
                                                }
                                                if (new java.io.File("/system/framework/oat/x86/services.odex").exists()) {
                                                    v277 = "/oat/x86";
                                                }
                                                if (new java.io.File("/system/framework/oat/arm/services.odex.xz").exists()) {
                                                    v277 = "/oat/arm";
                                                }
                                                if (new java.io.File("/system/framework/oat/arm64/services.odex.xz").exists()) {
                                                    v277 = "/oat/arm64";
                                                }
                                                if (new java.io.File("/system/framework/oat/x86/services.odex.xz").exists()) {
                                                    v277 = "/oat/x86";
                                                }
                                                if ((!v277.equals("")) && (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.api >= 21)) {
                                                    int v319_2 = 0;
                                                    int v320_2 = 0;
                                                    int v321_2 = 0;
                                                    int v322_0 = 0;
                                                    int v323_0 = 0;
                                                    int v324_0 = 0;
                                                    java.io.File v274 = new java.io.File;
                                                    v274(new StringBuilder().append("/system/framework").append(v277).append("/services.odex").toString());
                                                    int v276 = 1;
                                                    if (new java.io.File(new StringBuilder().append("/system/framework").append(v277).append("/services.odex.xz").toString()).exists()) {
                                                        System.out.println("try unpack services.odex.xz");
                                                        if (com.chelpus.Utils.XZDecompress(new java.io.File(new StringBuilder().append("/system/framework").append(v277).append("/services.odex.xz").toString()), new StringBuilder().append("/system/framework").append(v277).toString())) {
                                                            String[] v9_1614 = new String[3];
                                                            v9_1614[0] = "chmod";
                                                            v9_1614[1] = "644";
                                                            v9_1614[2] = new StringBuilder().append("/system/framework").append(v277).append("/services.odex").toString();
                                                            com.chelpus.Utils.run_all_no_root(v9_1614);
                                                            String[] v9_1616 = new String[3];
                                                            v9_1616[0] = "chown";
                                                            v9_1616[1] = "0:0";
                                                            v9_1616[2] = new StringBuilder().append("/system/framework").append(v277).append("/services.odex").toString();
                                                            com.chelpus.Utils.run_all_no_root(v9_1616);
                                                            String[] v9_1618 = new String[3];
                                                            v9_1618[0] = "chown";
                                                            v9_1618[1] = "0.0";
                                                            v9_1618[2] = new StringBuilder().append("/system/framework").append(v277).append("/services.odex").toString();
                                                            com.chelpus.Utils.run_all_no_root(v9_1618);
                                                        } else {
                                                            v276 = 0;
                                                            new java.io.File(new StringBuilder().append("/system/framework").append(v277).append("/services.odex").toString()).delete();
                                                            System.out.println("not enought space for unpack services.odex.xz");
                                                        }
                                                    }
                                                    if (v276 == 0) {
                                                        System.out.println("LuckyPatcher: odex not equal lenght patched! Not enougth space in /system/!");
                                                    } else {
                                                        try {
                                                            java.nio.channels.FileChannel v177_1 = new java.io.RandomAccessFile(v274, "rw").getChannel();
                                                            com.chelpus.root.utils.corepatch.fileBytes = v177_1.map(java.nio.channels.FileChannel$MapMode.READ_WRITE, 0, ((long) ((int) v177_1.size())));
                                                            com.chelpus.root.utils.corepatch.fileBytes.position(4120);
                                                            int v329_1 = com.chelpus.Utils.convertFourBytesToInt(com.chelpus.root.utils.corepatch.fileBytes.get(), com.chelpus.root.utils.corepatch.fileBytes.get(), com.chelpus.root.utils.corepatch.fileBytes.get(), com.chelpus.root.utils.corepatch.fileBytes.get());
                                                            System.out.println(new StringBuilder().append("Start position:").append(v329_1).toString());
                                                            com.chelpus.root.utils.corepatch.fileBytes.position(v329_1);
                                                        } catch (int v0_987) {
                                                            v0_987.printStackTrace();
                                                        }
                                                        try {
                                                            while (com.chelpus.root.utils.corepatch.fileBytes.hasRemaining()) {
                                                                int v178_2 = com.chelpus.root.utils.corepatch.fileBytes.position();
                                                                byte v179_2 = com.chelpus.root.utils.corepatch.fileBytes.get();
                                                                if (com.chelpus.root.utils.corepatch.applyPatch(v178_2, v179_2, v77, v78, v79, v80, v81_0)) {
                                                                    if (p362[0].contains("patch")) {
                                                                        System.out.println("Core4 patched!\n");
                                                                    }
                                                                    if (p362[0].contains("restore")) {
                                                                        System.out.println("Core4 restored!\n");
                                                                    }
                                                                    v319_2 = 1;
                                                                }
                                                                if (com.chelpus.root.utils.corepatch.applyPatch(v178_2, v179_2, v83, v84, v85, v86, v81_0)) {
                                                                    if (p362[0].contains("patch")) {
                                                                        System.out.println("Core4 patched!\n");
                                                                    }
                                                                    if (p362[0].contains("restore")) {
                                                                        System.out.println("Core4 restored!\n");
                                                                    }
                                                                    v319_2 = 1;
                                                                }
                                                                if (com.chelpus.root.utils.corepatch.applyPatch(v178_2, v179_2, v89, v90, v91, v92, v81_0)) {
                                                                    if (p362[0].contains("patch")) {
                                                                        System.out.println("Core4 patched!InstallLocationPolice\n");
                                                                    }
                                                                    if (p362[0].contains("restore")) {
                                                                        System.out.println("Core4 restored!InstallLocationPolice\n");
                                                                    }
                                                                    v320_2 = 1;
                                                                }
                                                                if (com.chelpus.root.utils.corepatch.applyPatch(v178_2, v179_2, v125, v126, v127, v128, v81_0)) {
                                                                    if (p362[0].contains("patch")) {
                                                                        System.out.println("Core4 patched!InstallLocationPolice\n");
                                                                    }
                                                                    if (p362[0].contains("restore")) {
                                                                        System.out.println("Core4 restored!InstallLocationPolice\n");
                                                                    }
                                                                    v320_2 = 1;
                                                                }
                                                                if (com.chelpus.root.utils.corepatch.applyPatch(v178_2, v179_2, v95, v96, v97, v98, v81_0)) {
                                                                    if (p362[0].contains("patch")) {
                                                                        System.out.println("Core4 patched!checkUpgradeKeySetLP\n");
                                                                    }
                                                                    if (p362[0].contains("restore")) {
                                                                        System.out.println("Core4 restored!checkUpgradeKeySetLP\n");
                                                                    }
                                                                    v321_2 = 1;
                                                                }
                                                                if (com.chelpus.root.utils.corepatch.applyPatch(v178_2, v179_2, v101, v102, v103, v104, v105_0)) {
                                                                    if (p362[0].contains("patch")) {
                                                                        System.out.println("Core4 patched!queryIntentServices\n");
                                                                    }
                                                                    if (p362[0].contains("restore")) {
                                                                        System.out.println("Core4 restored!queryIntentServices\n");
                                                                    }
                                                                    v322_0 = 1;
                                                                }
                                                                if (com.chelpus.root.utils.corepatch.applyPatch(v178_2, v179_2, v107, v108, v109, v110, v105_0)) {
                                                                    if (p362[0].contains("patch")) {
                                                                        System.out.println("Core4 patched!buildResolveList\n");
                                                                    }
                                                                    if (p362[0].contains("restore")) {
                                                                        System.out.println("Core4 restored!buildResolveList\n");
                                                                    }
                                                                    v323_0 = 1;
                                                                }
                                                                if (com.chelpus.root.utils.corepatch.applyPatch(v178_2, v179_2, v113, v114, v115, v116, v81_0)) {
                                                                    if (p362[0].contains("patch")) {
                                                                        System.out.println("Core4 patched!FixForCM\n");
                                                                    }
                                                                    if (p362[0].contains("restore")) {
                                                                        System.out.println("Core4 restored!FixForCM\n");
                                                                    }
                                                                    v324_0 = 1;
                                                                }
                                                                if (com.chelpus.root.utils.corepatch.applyPatch(v178_2, v179_2, v119, v120, v121, v122, v81_0)) {
                                                                    if (p362[0].contains("patch")) {
                                                                        System.out.println("Core4 patched!FixForCM\n");
                                                                    }
                                                                    if (p362[0].contains("restore")) {
                                                                        System.out.println("Core4 restored!FixForCM\n");
                                                                    }
                                                                    v324_0 = 1;
                                                                }
                                                                if (com.chelpus.root.utils.corepatch.applyPatch(v178_2, v179_2, v131, v132, v133, v134, v81_0)) {
                                                                    if (p362[0].contains("patch")) {
                                                                        System.out.println("Core4 patched!\n");
                                                                    }
                                                                    if (p362[0].contains("restore")) {
                                                                        System.out.println("Core4 restored!\n");
                                                                    }
                                                                    v319_2 = 1;
                                                                }
                                                                if (com.chelpus.root.utils.corepatch.applyPatch(v178_2, v179_2, v137, v138, v139, v140, v81_0)) {
                                                                    if (p362[0].contains("patch")) {
                                                                        System.out.println("Core4 patched!InstallLocationPolice\n");
                                                                    }
                                                                    if (p362[0].contains("restore")) {
                                                                        System.out.println("Core4 restored!InstallLocationPolice\n");
                                                                    }
                                                                    v320_2 = 1;
                                                                }
                                                                if (com.chelpus.root.utils.corepatch.applyPatch(v178_2, v179_2, v155, v156, v157, v158, v81_0)) {
                                                                    if (p362[0].contains("patch")) {
                                                                        System.out.println("Core4 patched!InstallLocationPolice\n");
                                                                    }
                                                                    if (p362[0].contains("restore")) {
                                                                        System.out.println("Core4 restored!InstallLocationPolice\n");
                                                                    }
                                                                    v320_2 = 1;
                                                                }
                                                                if (com.chelpus.root.utils.corepatch.applyPatch(v178_2, v179_2, v161, v162, v163, v164, v81_0)) {
                                                                    if (p362[0].contains("patch")) {
                                                                        System.out.println("Core4 patched!InstallLocationPolice\n");
                                                                    }
                                                                    if (p362[0].contains("restore")) {
                                                                        System.out.println("Core4 restored!InstallLocationPolice\n");
                                                                    }
                                                                    v320_2 = 1;
                                                                }
                                                                if (com.chelpus.root.utils.corepatch.applyPatch(v178_2, v179_2, v143, v144, v145, v146, v81_0)) {
                                                                    if (p362[0].contains("patch")) {
                                                                        System.out.println("Core4 patched!\n");
                                                                    }
                                                                    if (p362[0].contains("restore")) {
                                                                        System.out.println("Core4 restored!\n");
                                                                    }
                                                                    v319_2 = 1;
                                                                }
                                                                if (com.chelpus.root.utils.corepatch.applyPatch(v178_2, v179_2, v149, v150, v151, v152, v81_0)) {
                                                                    if (p362[0].contains("patch")) {
                                                                        System.out.println("Core4 patched!\n");
                                                                    }
                                                                    if (p362[0].contains("restore")) {
                                                                        System.out.println("Core4 restored!\n");
                                                                    }
                                                                    v319_2 = 1;
                                                                }
                                                                if ((v81_0 == 0) || ((v319_2 == 0) || ((v320_2 == 0) || ((v321_2 == 0) || ((v322_0 == 0) || ((v323_0 == 0) || (v324_0 == 0))))))) {
                                                                    com.chelpus.root.utils.corepatch.fileBytes.position((v178_2 + 1));
                                                                } else {
                                                                    new java.io.File("/data/dalvik-cache/arm/system@framework@services.jar@classes.dex").delete();
                                                                    new java.io.File("/data/dalvik-cache/arm/system@framework@services.jar@classes.art").delete();
                                                                    new java.io.File("/data/dalvik-cache/arm64/system@framework@services.jar@classes.dex").delete();
                                                                    new java.io.File("/data/dalvik-cache/arm64/system@framework@services.jar@classes.art").delete();
                                                                    new java.io.File("/data/dalvik-cache/x86/system@framework@services.jar@classes.dex").delete();
                                                                    new java.io.File("/data/dalvik-cache/x86/system@framework@services.jar@classes.art").delete();
                                                                    String[] v9_1786 = new String[1];
                                                                    v9_1786[0] = new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.toolfilesdir).append("/reboot").toString();
                                                                    com.chelpus.Utils.cmdParam(v9_1786);
                                                                    break;
                                                                }
                                                            }
                                                        } catch (int v0_985) {
                                                            System.out.println(new StringBuilder().append("").append(v0_985).toString());
                                                        }
                                                        v177_1.close();
                                                    }
                                                    if ((v319_2 != 0) || ((v320_2 != 0) || ((v321_2 != 0) || ((v322_0 != 0) || ((v323_0 != 0) || (v324_0 != 0)))))) {
                                                        new java.io.File("/data/dalvik-cache/arm/system@framework@services.jar@classes.dex").delete();
                                                        new java.io.File("/data/dalvik-cache/arm/system@framework@services.jar@classes.art").delete();
                                                        new java.io.File("/data/dalvik-cache/arm64/system@framework@services.jar@classes.dex").delete();
                                                        new java.io.File("/data/dalvik-cache/arm64/system@framework@services.jar@classes.art").delete();
                                                        new java.io.File("/data/dalvik-cache/x86/system@framework@services.jar@classes.dex").delete();
                                                        new java.io.File("/data/dalvik-cache/x86/system@framework@services.jar@classes.art").delete();
                                                        String[] v9_1801 = new String[1];
                                                        v9_1801[0] = new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.toolfilesdir).append("/reboot").toString();
                                                        com.chelpus.Utils.cmdParam(v9_1801);
                                                    }
                                                }
                                                try {
                                                    if (!p362[4].contains("framework")) {
                                                        if (!p362[4].contains("ART")) {
                                                            if (!com.chelpus.root.utils.corepatch.onlyDalvik) {
                                                                System.out.println(new StringBuilder().append("Vhodjashij file ").append(p362[2]).toString());
                                                                java.io.File v282_1 = new java.io.File;
                                                                v282_1(p362[2]);
                                                                if (v282_1.exists()) {
                                                                    java.io.File v281_1;
                                                                    if (!v282_1.toString().contains("system@framework@services.jar@classes.dex")) {
                                                                        v281_1 = v282_1;
                                                                    } else {
                                                                        System.out.println(new StringBuilder().append("Vhodjashij file byl dalvick-cache ").append(p362[2]).toString());
                                                                        java.io.File v359_2 = new java.io.File;
                                                                        v359_2("/system/framework/services.jar");
                                                                        com.chelpus.root.utils.corepatch.unzip(v359_2, "/data/app");
                                                                        java.io.File v262_2 = new java.io.File;
                                                                        v262_2("/data/app/classes.dex");
                                                                        if (!v262_2.exists()) {
                                                                            v281_1 = v282_1;
                                                                        } else {
                                                                            v281_1 = v262_2;
                                                                        }
                                                                        java.io.File v264_1 = new java.io.File;
                                                                        v264_1("/system/framework/services.odex");
                                                                        if ((v264_1.exists()) && (v264_1.length() == 0)) {
                                                                            v264_1.delete();
                                                                        }
                                                                    }
                                                                    System.out.println(new StringBuilder().append("Add file for patch ").append(v281_1.toString()).toString());
                                                                    v273_1.add(v281_1);
                                                                    String[] v9_1872 = v273_1.iterator();
                                                                    while (v9_1872.hasNext()) {
                                                                        java.io.File v275_3 = ((java.io.File) v9_1872.next());
                                                                        System.out.println(new StringBuilder().append("Start patch for ").append(v275_3.toString()).toString());
                                                                        java.io.File v281_2 = v275_3;
                                                                        int v278 = 0;
                                                                        if (com.chelpus.Utils.isELFfiles(v281_2)) {
                                                                            v278 = 1;
                                                                        }
                                                                        java.nio.channels.FileChannel v177_2 = new java.io.RandomAccessFile(v281_2, "rw").getChannel();
                                                                        com.chelpus.root.utils.corepatch.fileBytes = v177_2.map(java.nio.channels.FileChannel$MapMode.READ_WRITE, 0, ((long) ((int) v177_2.size())));
                                                                        int v318 = 0;
                                                                        int v266_0 = 0;
                                                                        int v199_0 = 0;
                                                                        if (v278 != 0) {
                                                                            int v319_3 = 0;
                                                                            int v320_3 = 0;
                                                                            int v321_3 = 0;
                                                                            int v322_1 = 0;
                                                                            int v323_1 = 0;
                                                                            int v324_1 = 0;
                                                                            com.chelpus.root.utils.corepatch.fileBytes.position(4120);
                                                                            int v329_2 = com.chelpus.Utils.convertFourBytesToInt(com.chelpus.root.utils.corepatch.fileBytes.get(), com.chelpus.root.utils.corepatch.fileBytes.get(), com.chelpus.root.utils.corepatch.fileBytes.get(), com.chelpus.root.utils.corepatch.fileBytes.get());
                                                                            System.out.println(new StringBuilder().append("Start position:").append(v329_2).toString());
                                                                            com.chelpus.root.utils.corepatch.fileBytes.position(v329_2);
                                                                            try {
                                                                                while (com.chelpus.root.utils.corepatch.fileBytes.hasRemaining()) {
                                                                                    int v178_3 = com.chelpus.root.utils.corepatch.fileBytes.position();
                                                                                    byte v179_3 = com.chelpus.root.utils.corepatch.fileBytes.get();
                                                                                    if (com.chelpus.root.utils.corepatch.applyPatch(v178_3, v179_3, v77, v78, v79, v80, v81_0)) {
                                                                                        if (p362[0].contains("patch")) {
                                                                                            System.out.println("Core4 patched!\n");
                                                                                        }
                                                                                        if (p362[0].contains("restore")) {
                                                                                            System.out.println("Core4 restored!\n");
                                                                                        }
                                                                                        v319_3 = 1;
                                                                                        v318 = 1;
                                                                                    }
                                                                                    if (com.chelpus.root.utils.corepatch.applyPatch(v178_3, v179_3, v83, v84, v85, v86, v81_0)) {
                                                                                        if (p362[0].contains("patch")) {
                                                                                            System.out.println("Core4 patched!\n");
                                                                                        }
                                                                                        if (p362[0].contains("restore")) {
                                                                                            System.out.println("Core4 restored!\n");
                                                                                        }
                                                                                        v319_3 = 1;
                                                                                        v318 = 1;
                                                                                    }
                                                                                    if (com.chelpus.root.utils.corepatch.applyPatch(v178_3, v179_3, v89, v90, v91, v92, v81_0)) {
                                                                                        if (p362[0].contains("patch")) {
                                                                                            System.out.println("Core4 patched!InstallLocationPolice\n");
                                                                                        }
                                                                                        if (p362[0].contains("restore")) {
                                                                                            System.out.println("Core4 restored!InstallLocationPolice\n");
                                                                                        }
                                                                                        v320_3 = 1;
                                                                                        v318 = 1;
                                                                                    }
                                                                                    if (com.chelpus.root.utils.corepatch.applyPatch(v178_3, v179_3, v125, v126, v127, v128, v81_0)) {
                                                                                        if (p362[0].contains("patch")) {
                                                                                            System.out.println("Core4 patched!InstallLocationPolice\n");
                                                                                        }
                                                                                        if (p362[0].contains("restore")) {
                                                                                            System.out.println("Core4 restored!InstallLocationPolice\n");
                                                                                        }
                                                                                        v320_3 = 1;
                                                                                        v318 = 1;
                                                                                    }
                                                                                    if (com.chelpus.root.utils.corepatch.applyPatch(v178_3, v179_3, v95, v96, v97, v98, v81_0)) {
                                                                                        if (p362[0].contains("patch")) {
                                                                                            System.out.println("Core4 patched!checkUpgradeKeySetLP\n");
                                                                                        }
                                                                                        if (p362[0].contains("restore")) {
                                                                                            System.out.println("Core4 restored!checkUpgradeKeySetLP\n");
                                                                                        }
                                                                                        v321_3 = 1;
                                                                                        v318 = 1;
                                                                                    }
                                                                                    if (com.chelpus.root.utils.corepatch.applyPatch(v178_3, v179_3, v101, v102, v103, v104, v105_0)) {
                                                                                        if (p362[0].contains("patch")) {
                                                                                            System.out.println("Core4 patched!queryIntentServices\n");
                                                                                        }
                                                                                        if (p362[0].contains("restore")) {
                                                                                            System.out.println("Core4 restored!queryIntentServices\n");
                                                                                        }
                                                                                        v322_1 = 1;
                                                                                        v318 = 1;
                                                                                    }
                                                                                    if (com.chelpus.root.utils.corepatch.applyPatch(v178_3, v179_3, v107, v108, v109, v110, v105_0)) {
                                                                                        if (p362[0].contains("patch")) {
                                                                                            System.out.println("Core4 patched!buildResolveList\n");
                                                                                        }
                                                                                        if (p362[0].contains("restore")) {
                                                                                            System.out.println("Core4 restored!buildResolveList\n");
                                                                                        }
                                                                                        v323_1 = 1;
                                                                                        v318 = 1;
                                                                                    }
                                                                                    if (com.chelpus.root.utils.corepatch.applyPatch(v178_3, v179_3, v113, v114, v115, v116, v81_0)) {
                                                                                        if (p362[0].contains("patch")) {
                                                                                            System.out.println("Core4 patched!FixForCM\n");
                                                                                        }
                                                                                        if (p362[0].contains("restore")) {
                                                                                            System.out.println("Core4 restored!FixForCM\n");
                                                                                        }
                                                                                        v324_1 = 1;
                                                                                        v318 = 1;
                                                                                    }
                                                                                    if (com.chelpus.root.utils.corepatch.applyPatch(v178_3, v179_3, v119, v120, v121, v122, v81_0)) {
                                                                                        if (p362[0].contains("patch")) {
                                                                                            System.out.println("Core4 patched!FixForCM\n");
                                                                                        }
                                                                                        if (p362[0].contains("restore")) {
                                                                                            System.out.println("Core4 restored!FixForCM\n");
                                                                                        }
                                                                                        v324_1 = 1;
                                                                                        v318 = 1;
                                                                                    }
                                                                                    if (com.chelpus.root.utils.corepatch.applyPatch(v178_3, v179_3, v131, v132, v133, v134, v81_0)) {
                                                                                        if (p362[0].contains("patch")) {
                                                                                            System.out.println("Core4 patched!\n");
                                                                                        }
                                                                                        if (p362[0].contains("restore")) {
                                                                                            System.out.println("Core4 restored!\n");
                                                                                        }
                                                                                        v319_3 = 1;
                                                                                        v318 = 1;
                                                                                    }
                                                                                    if (com.chelpus.root.utils.corepatch.applyPatch(v178_3, v179_3, v143, v144, v145, v146, v81_0)) {
                                                                                        if (p362[0].contains("patch")) {
                                                                                            System.out.println("Core4 patched!\n");
                                                                                        }
                                                                                        if (p362[0].contains("restore")) {
                                                                                            System.out.println("Core4 restored!\n");
                                                                                        }
                                                                                        v319_3 = 1;
                                                                                        v318 = 1;
                                                                                    }
                                                                                    if (com.chelpus.root.utils.corepatch.applyPatch(v178_3, v179_3, v149, v150, v151, v152, v81_0)) {
                                                                                        if (p362[0].contains("patch")) {
                                                                                            System.out.println("Core4 patched!\n");
                                                                                        }
                                                                                        if (p362[0].contains("restore")) {
                                                                                            System.out.println("Core4 restored!\n");
                                                                                        }
                                                                                        v319_3 = 1;
                                                                                        v318 = 1;
                                                                                    }
                                                                                    if (com.chelpus.root.utils.corepatch.applyPatch(v178_3, v179_3, v137, v138, v139, v140, v81_0)) {
                                                                                        if (p362[0].contains("patch")) {
                                                                                            System.out.println("Core4 patched!InstallLocationPolice\n");
                                                                                        }
                                                                                        if (p362[0].contains("restore")) {
                                                                                            System.out.println("Core4 restored!InstallLocationPolice\n");
                                                                                        }
                                                                                        v320_3 = 1;
                                                                                        v318 = 1;
                                                                                    }
                                                                                    if (com.chelpus.root.utils.corepatch.applyPatch(v178_3, v179_3, v155, v156, v157, v158, v81_0)) {
                                                                                        if (p362[0].contains("patch")) {
                                                                                            System.out.println("Core4 patched!InstallLocationPolice\n");
                                                                                        }
                                                                                        if (p362[0].contains("restore")) {
                                                                                            System.out.println("Core4 restored!InstallLocationPolice\n");
                                                                                        }
                                                                                        v320_3 = 1;
                                                                                        v318 = 1;
                                                                                    }
                                                                                    if (com.chelpus.root.utils.corepatch.applyPatch(v178_3, v179_3, v161, v162, v163, v164, v81_0)) {
                                                                                        if (p362[0].contains("patch")) {
                                                                                            System.out.println("Core4 patched!InstallLocationPolice\n");
                                                                                        }
                                                                                        if (p362[0].contains("restore")) {
                                                                                            System.out.println("Core4 restored!InstallLocationPolice\n");
                                                                                        }
                                                                                        v320_3 = 1;
                                                                                        v318 = 1;
                                                                                    }
                                                                                    if ((v81_0 != 0) && ((v319_3 != 0) && ((v320_3 != 0) && ((v321_3 != 0) && ((v322_1 != 0) && ((v323_1 != 0) && (v324_1 != 0))))))) {
                                                                                        break;
                                                                                    }
                                                                                    com.chelpus.root.utils.corepatch.fileBytes.position((v178_3 + 1));
                                                                                }
                                                                            } catch (int v0_1119) {
                                                                                System.out.println(new StringBuilder().append("").append(v0_1119).toString());
                                                                            }
                                                                        } else {
                                                                            com.chelpus.root.utils.corepatch.lastByteReplace = 0;
                                                                            com.chelpus.root.utils.corepatch.lastPatchPosition = 0;
                                                                            long v279_1 = 0;
                                                                            while (com.chelpus.root.utils.corepatch.fileBytes.hasRemaining()) {
                                                                                int v178_4 = com.chelpus.root.utils.corepatch.fileBytes.position();
                                                                                byte v179_4 = com.chelpus.root.utils.corepatch.fileBytes.get();
                                                                                if (com.chelpus.root.utils.corepatch.applyPatch(v178_4, v179_4, v225, v306, v255, v352, v81_0)) {
                                                                                    if (p362[0].contains("patch")) {
                                                                                        System.out.println("Core4 patched!\n");
                                                                                    }
                                                                                    if (p362[0].contains("restore")) {
                                                                                        System.out.println("Core4 restored!\n");
                                                                                    }
                                                                                    v318 = 1;
                                                                                }
                                                                                if (com.chelpus.root.utils.corepatch.applyPatch(v178_4, v179_4, v228, v309, v258, v355, v81_0)) {
                                                                                    if (p362[0].contains("patch")) {
                                                                                        System.out.println("Core4 patched!\n");
                                                                                    }
                                                                                    if (p362[0].contains("restore")) {
                                                                                        System.out.println("Core4 restored!\n");
                                                                                    }
                                                                                    v318 = 1;
                                                                                }
                                                                                if (com.chelpus.root.utils.corepatch.applyPatch(v178_4, v179_4, v173, v174, v175, v176, v81_0)) {
                                                                                    if (p362[0].contains("patch")) {
                                                                                        System.out.println("Core4 patched!\n");
                                                                                    }
                                                                                    if (p362[0].contains("restore")) {
                                                                                        System.out.println("Core4 restored!\n");
                                                                                    }
                                                                                    v318 = 1;
                                                                                }
                                                                                if (com.chelpus.root.utils.corepatch.applyPatch(v178_4, v179_4, v229, v310, v259, v356, v81_0)) {
                                                                                    if (p362[0].contains("patch")) {
                                                                                        System.out.println("Core4 patched!checkUpgradeKeySetLP\n");
                                                                                    }
                                                                                    if (p362[0].contains("restore")) {
                                                                                        System.out.println("Core4 restored!checkUpgradeKeySetLP\n");
                                                                                    }
                                                                                    v318 = 1;
                                                                                }
                                                                                if (com.chelpus.root.utils.corepatch.applyPatch(v178_4, v179_4, v230, v311, v260, v357, v81_0)) {
                                                                                    if (p362[0].contains("patch")) {
                                                                                        System.out.println("Core4 patched!\nCM12");
                                                                                    }
                                                                                    if (p362[0].contains("restore")) {
                                                                                        System.out.println("Core4 restored!\nCM12");
                                                                                    }
                                                                                    v318 = 1;
                                                                                }
                                                                                if (com.chelpus.root.utils.corepatch.applyPatchCounter(v178_4, v179_4, v231, v312, v261, v358, v266_0, v192, v105_0)) {
                                                                                    v266_0++;
                                                                                    if (p362[0].contains("patch")) {
                                                                                        System.out.println("Core4 patched!\nRemove Package from intent");
                                                                                    }
                                                                                    if (p362[0].contains("restore")) {
                                                                                        System.out.println("Core4 restored!\nRemove Package from intent");
                                                                                    }
                                                                                    v318 = 1;
                                                                                }
                                                                                if (com.chelpus.root.utils.corepatch.applyPatchCounter(v178_4, v179_4, v195, v196, v197, v198, v199_0, v200, v105_0)) {
                                                                                    v199_0++;
                                                                                    if (p362[0].contains("patch")) {
                                                                                        System.out.println("Core4 patched!\nRemove Package from intent");
                                                                                    }
                                                                                    if (p362[0].contains("restore")) {
                                                                                        System.out.println("Core4 restored!\nRemove Package from intent");
                                                                                    }
                                                                                    v318 = 1;
                                                                                }
                                                                                if (com.chelpus.root.utils.corepatch.applyPatch(v178_4, v179_4, v222, v303, v252, v349, v105_0)) {
                                                                                    if (p362[0].contains("patch")) {
                                                                                        System.out.println("Core4 patched!\nbuildResolveList");
                                                                                    }
                                                                                    if (p362[0].contains("restore")) {
                                                                                        System.out.println("Core4 restored!\nbuildResolveList");
                                                                                    }
                                                                                    v318 = 1;
                                                                                }
                                                                                if (com.chelpus.root.utils.corepatch.applyPatch(v178_4, v179_4, v223, v304, v253, v350, v105_0)) {
                                                                                    if (p362[0].contains("patch")) {
                                                                                        System.out.println("Core4 patched!\nbuildResolveList");
                                                                                    }
                                                                                    if (p362[0].contains("restore")) {
                                                                                        System.out.println("Core4 restored!\nbuildResolveList");
                                                                                    }
                                                                                    v318 = 1;
                                                                                }
                                                                                if (com.chelpus.root.utils.corepatch.applyPatch(v178_4, v179_4, v224, v305, v254, v351, v105_0)) {
                                                                                    if (p362[0].contains("patch")) {
                                                                                        System.out.println("Core4 patched!\nbuildResolveList");
                                                                                    }
                                                                                    if (p362[0].contains("restore")) {
                                                                                        System.out.println("Core4 restored!\nbuildResolveList");
                                                                                    }
                                                                                    v318 = 1;
                                                                                }
                                                                                if (com.chelpus.root.utils.corepatch.applyPatch(v178_4, v179_4, v226, v307, v256, v353, v81_0)) {
                                                                                    if (p362[0].contains("patch")) {
                                                                                        System.out.println("Core4 patched!\nCM11");
                                                                                    }
                                                                                    if (p362[0].contains("restore")) {
                                                                                        System.out.println("Core4 restored!\nCM11");
                                                                                    }
                                                                                    v318 = 1;
                                                                                }
                                                                                if (com.chelpus.root.utils.corepatch.applyPatch(v178_4, v179_4, v227, v308, v257, v354, v81_0)) {
                                                                                    if (p362[0].contains("patch")) {
                                                                                        System.out.println("Core4 policy patched!\n");
                                                                                    }
                                                                                    if (p362[0].contains("restore")) {
                                                                                        System.out.println("Core4 policy restored!\n");
                                                                                    }
                                                                                    v318 = 1;
                                                                                }
                                                                                if (com.chelpus.root.utils.corepatch.applyPatch(v178_4, v179_4, v167, v168, v169, v170, v81_0)) {
                                                                                    if (p362[0].contains("patch")) {
                                                                                        System.out.println("Core4 policy patched!\n");
                                                                                    }
                                                                                    if (p362[0].contains("restore")) {
                                                                                        System.out.println("Core4 policy restored!\n");
                                                                                    }
                                                                                    v318 = 1;
                                                                                }
                                                                                com.chelpus.root.utils.corepatch.fileBytes.position((v178_4 + 1));
                                                                                v279_1++;
                                                                            }
                                                                            if ((com.chelpus.root.utils.corepatch.lastPatchPosition > 0) && (com.chelpus.root.utils.corepatch.lastByteReplace != null)) {
                                                                                com.chelpus.root.utils.corepatch.fileBytes.position(com.chelpus.root.utils.corepatch.lastPatchPosition);
                                                                                com.chelpus.root.utils.corepatch.fileBytes.put(com.chelpus.root.utils.corepatch.lastByteReplace);
                                                                                com.chelpus.root.utils.corepatch.fileBytes.force();
                                                                            }
                                                                        }
                                                                        v177_2.close();
                                                                        if (!p362[4].contains("framework")) {
                                                                            if (!v281_2.toString().endsWith("/classes.dex")) {
                                                                                com.chelpus.Utils.fixadlerOdex(v281_2, "/system/framework/services.jar");
                                                                            } else {
                                                                                com.chelpus.Utils.fixadler(v281_2);
                                                                            }
                                                                            if ((v281_2.toString().contains("system@framework@services.jar@classes.dex")) && (v318 != 0)) {
                                                                                System.out.println("LuckyPatcher: dalvik-cache patched! ");
                                                                            }
                                                                            if ((v281_2.toString().contains("/classes.dex")) && (v318 != 0)) {
                                                                                System.out.println("start");
                                                                                if (!com.chelpus.Utils.copyFile("/system/framework/services.jar", "/system/framework/services.backup", 1, 0)) {
                                                                                    new java.io.File("/system/framework/services.backup").delete();
                                                                                    v282_1 = new java.io.File;
                                                                                    v282_1(p362[2]);
                                                                                    java.nio.channels.FileChannel v177_3 = new java.io.RandomAccessFile(v282_1, "rw").getChannel();
                                                                                    com.chelpus.root.utils.corepatch.fileBytes = v177_3.map(java.nio.channels.FileChannel$MapMode.READ_WRITE, 0, ((long) ((int) v177_3.size())));
                                                                                    int v266_1 = 0;
                                                                                    int v199_1 = 0;
                                                                                    com.chelpus.root.utils.corepatch.lastByteReplace = 0;
                                                                                    com.chelpus.root.utils.corepatch.lastPatchPosition = 0;
                                                                                    long v279_2 = 0;
                                                                                    try {
                                                                                        while (com.chelpus.root.utils.corepatch.fileBytes.hasRemaining()) {
                                                                                            int v178_5 = com.chelpus.root.utils.corepatch.fileBytes.position();
                                                                                            byte v179_5 = com.chelpus.root.utils.corepatch.fileBytes.get();
                                                                                            if (com.chelpus.root.utils.corepatch.applyPatch(v178_5, v179_5, v225, v306, v255, v352, v81_0)) {
                                                                                                if (p362[0].contains("patch")) {
                                                                                                    System.out.println("Core4 patched!\n");
                                                                                                }
                                                                                                if (p362[0].contains("restore")) {
                                                                                                    System.out.println("Core4 restored!\n");
                                                                                                }
                                                                                            }
                                                                                            if (com.chelpus.root.utils.corepatch.applyPatch(v178_5, v179_5, v228, v309, v258, v355, v81_0)) {
                                                                                                if (p362[0].contains("patch")) {
                                                                                                    System.out.println("Core4 patched!\n");
                                                                                                }
                                                                                                if (p362[0].contains("restore")) {
                                                                                                    System.out.println("Core4 restored!\n");
                                                                                                }
                                                                                            }
                                                                                            if (com.chelpus.root.utils.corepatch.applyPatch(v178_5, v179_5, v173, v174, v175, v176, v81_0)) {
                                                                                                if (p362[0].contains("patch")) {
                                                                                                    System.out.println("Core4 patched!\n");
                                                                                                }
                                                                                                if (p362[0].contains("restore")) {
                                                                                                    System.out.println("Core4 restored!\n");
                                                                                                }
                                                                                            }
                                                                                            if (com.chelpus.root.utils.corepatch.applyPatch(v178_5, v179_5, v229, v310, v259, v356, v81_0)) {
                                                                                                if (p362[0].contains("patch")) {
                                                                                                    System.out.println("Core4 patched!checkUpgradeKeySetLP\n");
                                                                                                }
                                                                                                if (p362[0].contains("restore")) {
                                                                                                    System.out.println("Core4 restored!checkUpgradeKeySetLP\n");
                                                                                                }
                                                                                            }
                                                                                            if (com.chelpus.root.utils.corepatch.applyPatch(v178_5, v179_5, v230, v311, v260, v357, v81_0)) {
                                                                                                if (p362[0].contains("patch")) {
                                                                                                    System.out.println("Core4 patched!\nCM12");
                                                                                                }
                                                                                                if (p362[0].contains("restore")) {
                                                                                                    System.out.println("Core4 restored!\nCM12");
                                                                                                }
                                                                                            }
                                                                                            if (com.chelpus.root.utils.corepatch.applyPatchCounter(v178_5, v179_5, v231, v312, v261, v358, v266_1, v192, v105_0)) {
                                                                                                v266_1++;
                                                                                                if (p362[0].contains("patch")) {
                                                                                                    System.out.println("Core4 patched!\nRemove Package from intent");
                                                                                                }
                                                                                                if (p362[0].contains("restore")) {
                                                                                                    System.out.println("Core4 restored!\nRemove Package from intent");
                                                                                                }
                                                                                            }
                                                                                            if (com.chelpus.root.utils.corepatch.applyPatchCounter(v178_5, v179_5, v195, v196, v197, v198, v199_1, v200, v105_0)) {
                                                                                                v199_1++;
                                                                                                if (p362[0].contains("patch")) {
                                                                                                    System.out.println("Core4 patched!\nRemove Package from intent");
                                                                                                }
                                                                                                if (p362[0].contains("restore")) {
                                                                                                    System.out.println("Core4 restored!\nRemove Package from intent");
                                                                                                }
                                                                                            }
                                                                                            if (com.chelpus.root.utils.corepatch.applyPatch(v178_5, v179_5, v222, v303, v252, v349, v105_0)) {
                                                                                                if (p362[0].contains("patch")) {
                                                                                                    System.out.println("Core4 patched!\nbuildResolveList");
                                                                                                }
                                                                                                if (p362[0].contains("restore")) {
                                                                                                    System.out.println("Core4 restored!\nbuildResolveList");
                                                                                                }
                                                                                            }
                                                                                            if (com.chelpus.root.utils.corepatch.applyPatch(v178_5, v179_5, v223, v304, v253, v350, v105_0)) {
                                                                                                if (p362[0].contains("patch")) {
                                                                                                    System.out.println("Core4 patched!\nbuildResolveList");
                                                                                                }
                                                                                                if (p362[0].contains("restore")) {
                                                                                                    System.out.println("Core4 restored!\nbuildResolveList");
                                                                                                }
                                                                                            }
                                                                                            if (com.chelpus.root.utils.corepatch.applyPatch(v178_5, v179_5, v224, v305, v254, v351, v105_0)) {
                                                                                                if (p362[0].contains("patch")) {
                                                                                                    System.out.println("Core4 patched!\nbuildResolveList");
                                                                                                }
                                                                                                if (p362[0].contains("restore")) {
                                                                                                    System.out.println("Core4 restored!\nbuildResolveList");
                                                                                                }
                                                                                            }
                                                                                            if (com.chelpus.root.utils.corepatch.applyPatch(v178_5, v179_5, v226, v307, v256, v353, v81_0)) {
                                                                                                if (p362[0].contains("patch")) {
                                                                                                    System.out.println("Core4 patched!\nCM11");
                                                                                                }
                                                                                                if (p362[0].contains("restore")) {
                                                                                                    System.out.println("Core4 restored!\nCM11");
                                                                                                }
                                                                                            }
                                                                                            if (com.chelpus.root.utils.corepatch.applyPatch(v178_5, v179_5, v227, v308, v257, v354, v81_0)) {
                                                                                                if (p362[0].contains("patch")) {
                                                                                                    System.out.println("Core4 policy patched!\n");
                                                                                                }
                                                                                                if (p362[0].contains("restore")) {
                                                                                                    System.out.println("Core4 policy restored!\n");
                                                                                                }
                                                                                            }
                                                                                            if (com.chelpus.root.utils.corepatch.applyPatch(v178_5, v179_5, v167, v168, v169, v170, v81_0)) {
                                                                                                if (p362[0].contains("patch")) {
                                                                                                    System.out.println("Core4 policy patched!\n");
                                                                                                }
                                                                                                if (p362[0].contains("restore")) {
                                                                                                    System.out.println("Core4 policy restored!\n");
                                                                                                }
                                                                                            }
                                                                                            com.chelpus.root.utils.corepatch.fileBytes.position((v178_5 + 1));
                                                                                            v279_2++;
                                                                                        }
                                                                                    } catch (int v0_1222) {
                                                                                        System.out.println(new StringBuilder().append("").append(v0_1222).toString());
                                                                                    }
                                                                                    if ((com.chelpus.root.utils.corepatch.lastPatchPosition > 0) && (com.chelpus.root.utils.corepatch.lastByteReplace != null)) {
                                                                                        com.chelpus.root.utils.corepatch.fileBytes.position(com.chelpus.root.utils.corepatch.lastPatchPosition);
                                                                                        com.chelpus.root.utils.corepatch.fileBytes.put(com.chelpus.root.utils.corepatch.lastByteReplace);
                                                                                        com.chelpus.root.utils.corepatch.fileBytes.force();
                                                                                    }
                                                                                    v177_3.close();
                                                                                    if (!v282_1.toString().endsWith("/classes.dex")) {
                                                                                        com.chelpus.Utils.fixadlerOdex(v282_1, "/system/framework/services.jar");
                                                                                    } else {
                                                                                        com.chelpus.Utils.fixadler(v282_1);
                                                                                    }
                                                                                    System.out.println("LuckyPatcher: odex not equal lenght patched! Not enougth space in /system/!");
                                                                                } else {
                                                                                    System.out.println("good space");
                                                                                    new java.io.File("/system/framework/services.backup").delete();
                                                                                    java.util.ArrayList v272_5 = new java.util.ArrayList();
                                                                                    System.out.println("add files");
                                                                                    v272_5.add(new com.android.vending.billing.InAppBillingService.LUCK.AddFilesItem("/data/app/classes.dex", "/data/app/"));
                                                                                    try {
                                                                                        com.chelpus.Utils.addFilesToZip("/system/framework/services.jar", "/system/framework/services.backup", v272_5);
                                                                                        System.out.println("add files finish");
                                                                                        java.io.PrintStream v10_1003 = new String[3];
                                                                                        v10_1003[0] = "chmod";
                                                                                        v10_1003[1] = "0644";
                                                                                        v10_1003[2] = "/system/framework/services.backup";
                                                                                        com.chelpus.Utils.run_all_no_root(v10_1003);
                                                                                        java.io.PrintStream v10_1005 = new String[3];
                                                                                        v10_1005[0] = "chown";
                                                                                        v10_1005[1] = "0:0";
                                                                                        v10_1005[2] = "/system/framework/services.backup";
                                                                                        com.chelpus.Utils.run_all_no_root(v10_1005);
                                                                                        java.io.PrintStream v10_1007 = new String[3];
                                                                                        v10_1007[0] = "chmod";
                                                                                        v10_1007[1] = "0.0";
                                                                                        v10_1007[2] = "/system/framework/services.backup";
                                                                                        com.chelpus.Utils.run_all_no_root(v10_1007);
                                                                                        java.io.File v264_2 = new java.io.File;
                                                                                        v264_2("/system/framework/services.odex");
                                                                                        java.io.File v267 = com.chelpus.Utils.getFileDalvikCache("/system/framework/services.jar");
                                                                                    } catch (int v0_1240) {
                                                                                        v0_1240.printStackTrace();
                                                                                        System.out.println("LuckyPatcher: odex not equal lenght patched! Not enougth space in /system/!");
                                                                                        new java.io.File("/system/framework/services.backup").delete();
                                                                                        System.out.println("finish");
                                                                                    }
                                                                                    if (v264_2.exists()) {
                                                                                        System.out.println("fix odex na osnove rebuild services");
                                                                                        com.chelpus.Utils.fixadlerOdex(v264_2, "/system/framework/services.backup");
                                                                                    }
                                                                                    new java.io.File(new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.toolfilesdir).append("/ClearDalvik.on").toString()).createNewFile();
                                                                                    new java.io.File("/system/framework/services.jar").delete();
                                                                                    java.io.PrintStream v10_1018 = new String[2];
                                                                                    v10_1018[0] = "rm";
                                                                                    v10_1018[1] = "/system/framework/services.jar";
                                                                                    com.chelpus.Utils.run_all_no_root(v10_1018);
                                                                                    new java.io.File("/system/framework/services.backup").renameTo(new java.io.File("/system/framework/services.jar"));
                                                                                    if (!new java.io.File("/system/framework/services.jar").exists()) {
                                                                                        java.io.PrintStream v10_1025 = new String[3];
                                                                                        v10_1025[0] = "mv";
                                                                                        v10_1025[1] = "/system/framework/services.backup";
                                                                                        v10_1025[2] = "/system/framework/services.jar";
                                                                                        com.chelpus.Utils.run_all_no_root(v10_1025);
                                                                                    }
                                                                                    try {
                                                                                        if (v267 == null) {
                                                                                            new java.io.File("/data/dalvik-cache/arm/system@framework@arm@boot.oat").delete();
                                                                                            new java.io.File("/data/dalvik-cache/arm/system@framework@arm@boot.art").delete();
                                                                                            new java.io.File("/data/dalvik-cache/arm64/system@framework@arm@boot.oat").delete();
                                                                                            new java.io.File("/data/dalvik-cache/arm64/system@framework@arm@boot.art").delete();
                                                                                            new java.io.File("/data/dalvik-cache/x86/system@framework@arm@boot.oat").delete();
                                                                                            new java.io.File("/data/dalvik-cache/x86/system@framework@arm@boot.art").delete();
                                                                                        } else {
                                                                                            java.io.PrintStream v10_1027 = new String[2];
                                                                                            v10_1027[0] = "rm";
                                                                                            v10_1027[1] = v267.getAbsolutePath();
                                                                                            com.chelpus.Utils.run_all_no_root(v10_1027);
                                                                                            if (!v267.exists()) {
                                                                                            } else {
                                                                                                v267.delete();
                                                                                            }
                                                                                        }
                                                                                        com.chelpus.Utils.clear_dalvik_cache();
                                                                                    } catch (int v0_1239) {
                                                                                        v0_1239.printStackTrace();
                                                                                    }
                                                                                }
                                                                            }
                                                                            if (!com.chelpus.root.utils.corepatch.onlyDalvik) {
                                                                                java.io.File v263_2 = new java.io.File;
                                                                                v263_2("/system/framework/services.patched");
                                                                                if (v263_2.exists()) {
                                                                                    System.out.println("LuckyPatcher: root found services.patched! ");
                                                                                }
                                                                                java.io.File v263_3 = new java.io.File;
                                                                                v263_3("/system/framework/services.odex");
                                                                                if (v263_3.exists()) {
                                                                                    System.out.println("LuckyPatcher: root found services.odex! ");
                                                                                }
                                                                                if (p362[0].contains("restore")) {
                                                                                    new java.io.File("/system/framework/patch3.done").delete();
                                                                                }
                                                                            }
                                                                        } else {
                                                                            System.out.println("Rebuild file!");
                                                                            if (v281_2.toString().endsWith("/classes.dex")) {
                                                                                com.chelpus.Utils.fixadler(v281_2);
                                                                                if (v318 == 0) {
                                                                                    com.chelpus.root.utils.corepatch.not_found_bytes_for_patch = 1;
                                                                                } else {
                                                                                    com.chelpus.root.utils.corepatch.not_found_bytes_for_patch = 0;
                                                                                    String v269_4 = p362[1].replace("/services.jar", "/services-patched.jar");
                                                                                    String v360_4 = p362[1];
                                                                                    new java.io.File(v269_4).delete();
                                                                                    java.util.ArrayList v272_7 = new java.util.ArrayList();
                                                                                    System.out.println("add files");
                                                                                    v272_7.add(new com.android.vending.billing.InAppBillingService.LUCK.AddFilesItem(v281_2.getAbsolutePath(), new StringBuilder().append(com.chelpus.Utils.getDirs(v281_2).getAbsolutePath()).append("/").toString()));
                                                                                    com.chelpus.Utils.addFilesToZip(v360_4, v269_4, v272_7);
                                                                                    System.out.println("add files finish");
                                                                                    new java.io.File(v360_4).delete();
                                                                                }
                                                                            }
                                                                            if (v281_2.toString().endsWith("/services.odex")) {
                                                                                if (!com.chelpus.Utils.isELFfiles(v281_2)) {
                                                                                    com.chelpus.Utils.fixadlerOdex(v281_2, 0);
                                                                                }
                                                                                if (v318 != 0) {
                                                                                    v281_2.renameTo(new java.io.File(v281_2.getAbsolutePath().replace("/services.odex", "/services-patched.odex")));
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                    com.chelpus.Utils.exitFromRootJava();
                                                                    return;
                                                                } else {
                                                                    throw new java.io.FileNotFoundException();
                                                                }
                                                            } else {
                                                                v282_1 = new java.io.File;
                                                                v282_1(p362[2]);
                                                                if (v282_1.exists()) {
                                                                    v273_1.add(v282_1);
                                                                } else {
                                                                    throw new java.io.FileNotFoundException();
                                                                }
                                                            }
                                                        } else {
                                                            if ((new java.io.File("/system/framework/services.odex").exists()) && (new java.io.File("/system/framework/services.odex").length() != 0)) {
                                                                System.out.println("Add services.odex for patch");
                                                                v273_1.add(new java.io.File("/system/framework/services.odex"));
                                                            }
                                                            if (!com.chelpus.Utils.classes_test(new java.io.File("/system/framework/services.jar"))) {
                                                            } else {
                                                                System.out.println("services.jar contain classes,dex");
                                                                java.io.File v359_3 = new java.io.File;
                                                                v359_3("/system/framework/services.jar");
                                                                com.chelpus.root.utils.corepatch.unzip(v359_3, "/data/app");
                                                                java.io.File v262_3 = new java.io.File;
                                                                v262_3("/data/app/classes.dex");
                                                                if (!v262_3.exists()) {
                                                                } else {
                                                                    System.out.println("Add classes.dex for patch");
                                                                    v273_1.add(v262_3);
                                                                }
                                                            }
                                                        }
                                                    } else {
                                                        if ((p362[1].contains("services.jar")) && (com.chelpus.Utils.classes_test(new java.io.File(p362[1])))) {
                                                            java.io.File v359_4 = new java.io.File;
                                                            v359_4(p362[1]);
                                                            com.chelpus.root.utils.corepatch.unzip(v359_4, com.chelpus.Utils.getDirs(v359_4).getAbsolutePath());
                                                            java.io.File v262_4 = new java.io.File;
                                                            v262_4(new StringBuilder().append(com.chelpus.Utils.getDirs(v359_4).getAbsolutePath()).append("/classes.dex").toString());
                                                            if (v262_4.exists()) {
                                                                v273_1.add(v262_4);
                                                            }
                                                        }
                                                        if (!p362[1].contains("services.odex")) {
                                                        } else {
                                                            v273_1.add(new java.io.File(p362[1]));
                                                        }
                                                    }
                                                } catch (int v0_1226) {
                                                    Exception v270_11 = v0_1226;
                                                    System.out.println(new StringBuilder().append("Exception e").append(v270_11.toString()).toString());
                                                } catch (int v0_1267) {
                                                }
                                                System.out.println("Error: services.odex not found!\n\nPlease Odex services.jar and try again!");
                                            }
                                            com.chelpus.Utils.exitFromRootJava();
                                            return;
                                        } else {
                                            throw new java.io.FileNotFoundException();
                                        }
                                    } else {
                                        System.out.println(new StringBuilder().append("OnlyDalvik: add for patch ").append(p362[1]).toString());
                                        v282_0 = new java.io.File;
                                        v282_0(p362[1]);
                                        if (v282_0.exists()) {
                                            v273_1.add(v282_0);
                                        } else {
                                            throw new java.io.FileNotFoundException();
                                        }
                                    }
                                } catch (int v0_673) {
                                    Exception v270_1 = v0_673;
                                    System.out.println(new StringBuilder().append("Exception e").append(v270_1.toString()).toString());
                                }
                            } else {
                                if ((new java.io.File("/system/framework/core-libart.odex").exists()) && (new java.io.File("/system/framework/core-libart.odex").length() != 0)) {
                                    v273_1.add(new java.io.File("/system/framework/core-libart.odex"));
                                }
                                if (!com.chelpus.Utils.classes_test(new java.io.File("/system/framework/core-libart.jar"))) {
                                } else {
                                    java.io.File v359_0 = new java.io.File;
                                    v359_0("/system/framework/core-libart.jar");
                                    com.chelpus.root.utils.corepatch.unzip(v359_0, "/data/app");
                                    java.io.File v262_0 = new java.io.File;
                                    v262_0("/data/app/classes.dex");
                                    if (!v262_0.exists()) {
                                    } else {
                                        v273_1.add(v262_0);
                                    }
                                }
                            }
                        } else {
                            if (((p362[1].contains("core.jar")) || (p362[1].contains("core-libart.jar"))) && (com.chelpus.Utils.classes_test(new java.io.File(p362[1])))) {
                                java.io.File v359_1 = new java.io.File;
                                v359_1(p362[1]);
                                com.chelpus.root.utils.corepatch.unzip(v359_1, com.chelpus.Utils.getDirs(v359_1).getAbsolutePath());
                                java.io.File v262_1 = new java.io.File;
                                v262_1(new StringBuilder().append(com.chelpus.Utils.getDirs(v359_1).getAbsolutePath()).append("/classes.dex").toString());
                                if (v262_1.exists()) {
                                    v273_1.add(v262_1);
                                }
                            }
                            if ((!p362[1].contains("core.odex")) && (!p362[1].contains("boot.oat"))) {
                            } else {
                                v273_1.add(new java.io.File(p362[1]));
                            }
                        }
                    } catch (int v0_894) {
                    }
                    System.out.println("Error: core.odex not found!\n\nPlease Odex core.jar and try again!");
                }
            }
            com.chelpus.Utils.exitFromRootJava();
            return;
        }
        if ((p362[4] == null) || (!p362[4].equals("OnlyDalvik"))) {
        } else {
            com.chelpus.root.utils.corepatch.onlyDalvik = 1;
        }
    }

    public static void unzip(java.io.File p18, String p19)
    {
        int v6 = 0;
        try {
            java.io.FileInputStream v5_1 = new java.io.FileInputStream(p18);
            java.util.zip.ZipInputStream v12_1 = new java.util.zip.ZipInputStream(v5_1);
            java.util.zip.ZipEntry v11 = v12_1.getNextEntry();
        } catch (Exception v3) {
            try {
                new net.lingala.zip4j.core.ZipFile(p18).extractFile("classes.dex", p19);
            } catch (Exception v4_1) {
                System.out.println(new StringBuilder().append("Error classes.dex decompress! ").append(v4_1).toString());
                System.out.println(new StringBuilder().append("Exception e1").append(v3.toString()).toString());
            } catch (Exception v4_0) {
                System.out.println(new StringBuilder().append("Error classes.dex decompress! ").append(v4_0).toString());
                System.out.println(new StringBuilder().append("Exception e1").append(v3.toString()).toString());
            }
            System.out.println(new StringBuilder().append("Exception e").append(v3.toString()).toString());
            return;
        }
        while ((v11 != null) && (1 != 0)) {
            if (v11.getName().equals("classes.dex")) {
                java.io.FileOutputStream v7_1 = new java.io.FileOutputStream(new StringBuilder().append(p19).append("/").append("classes.dex").toString());
                byte[] v1 = new byte[2048];
                while(true) {
                    int v9 = v12_1.read(v1);
                    if (v9 == -1) {
                        break;
                    }
                    v7_1.write(v1, 0, v9);
                }
                String[] v14_11 = new String[3];
                v14_11[0] = "chmod";
                v14_11[1] = "777";
                v14_11[2] = new StringBuilder().append(p19).append("/").append("classes.dex").toString();
                com.chelpus.Utils.run_all_no_root(v14_11);
                String[] v14_13 = new String[3];
                v14_13[0] = "chown";
                v14_13[1] = "0.0";
                v14_13[2] = new StringBuilder().append(p19).append("/").append("classes.dex").toString();
                com.chelpus.Utils.run_all_no_root(v14_13);
                String[] v14_15 = new String[3];
                v14_15[0] = "chown";
                v14_15[1] = "0:0";
                v14_15[2] = new StringBuilder().append(p19).append("/").append("classes.dex").toString();
                com.chelpus.Utils.run_all_no_root(v14_15);
                v12_1.closeEntry();
                v7_1.close();
                v6 = 1;
            }
            if (v6 == 0) {
                v11 = v12_1.getNextEntry();
            } else {
                break;
            }
        }
        v12_1.close();
        v5_1.close();
        return;
    }
}
