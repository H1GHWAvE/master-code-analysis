package com.chelpus.root.utils;
public class live_restorelib {

    public live_restorelib()
    {
        return;
    }

    public static void main(String[] p10)
    {
        com.chelpus.Utils.startRootJava(new com.chelpus.root.utils.live_restorelib$1());
        String v2 = p10[0];
        try {
            java.io.File v4_1 = new java.io.File(v2);
            java.io.File v5_1 = new java.io.File(v2.replace("/data/data/", "/mnt/asec/"));
        } catch (java.io.FileNotFoundException v6) {
            System.out.println("Error: Backup files are not found!");
            com.chelpus.Utils.exitFromRootJava();
            return;
        } catch (Exception v3) {
            System.out.println(new StringBuilder().append("Exception e").append(v3.toString()).toString());
            com.chelpus.Utils.exitFromRootJava();
            return;
        }
        if (v4_1.exists()) {
            v5_1 = v4_1;
        }
        if (!v5_1.exists()) {
            v5_1 = new java.io.File(new StringBuilder().append(v2).append("-1").toString());
        }
        if (!v5_1.exists()) {
            v5_1 = new java.io.File(v2.replace("-1", "-2"));
        }
        if (v5_1.exists()) {
            java.io.File v0_1 = new java.io.File(new StringBuilder().append(v5_1.getAbsolutePath()).append(".backup").toString());
            if (v0_1.exists()) {
                com.chelpus.Utils.copyFile(v0_1, v5_1);
                System.out.println("Restore - done!");
                com.chelpus.Utils.exitFromRootJava();
                return;
            } else {
                throw new java.io.FileNotFoundException();
            }
        } else {
            throw new java.io.FileNotFoundException();
        }
    }
}
