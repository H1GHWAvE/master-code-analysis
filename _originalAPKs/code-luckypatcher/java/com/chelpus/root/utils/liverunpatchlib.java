package com.chelpus.root.utils;
public class liverunpatchlib {

    public liverunpatchlib()
    {
        return;
    }

    public static void main(String[] p28)
    {
        com.chelpus.Utils.startRootJava(new com.chelpus.root.utils.liverunpatchlib$1());
        String[] v22 = p28[1].split(" ");
        String[] v26 = p28[2].split(" ");
        byte[] v7 = new byte[v22.length];
        byte[] v8 = new byte[v26.length];
        byte v0_3 = new byte[v22.length];
        byte[] v21 = v0_3;
        byte v0_5 = new byte[v26.length];
        byte[] v25 = v0_5;
        int v13 = 0;
        if ((v25.length != v21.length) || ((v7.length != v8.length) || ((v8.length <= 3) || (v7.length <= 3)))) {
            System.out.println("Error: Original & Replace hex-string not valid!\n\nOriginal.hex.length != Replacmant.hex.length.\nOR\nLength of hex-string < 4");
        } else {
            int v27 = 0;
            while (v27 < v22.length) {
                if ((v22[v27].contains("*")) && (!v22[v27].contains("**"))) {
                    v13 = 1;
                    v22[v27] = "00";
                }
                if ((v26[v27].contains("*")) && (!v26[v27].contains("**"))) {
                    v13 = 1;
                    v26[v27] = "00";
                }
                if (!v22[v27].contains("**")) {
                    v21[v27] = 0;
                } else {
                    v22[v27] = "00";
                    v21[v27] = 1;
                }
                v7[v27] = Integer.valueOf(v22[v27], 16).byteValue();
                if (!v26[v27].contains("**")) {
                    v25[v27] = 1;
                } else {
                    v26[v27] = "00";
                    v25[v27] = 0;
                }
                v8[v27] = Integer.valueOf(v26[v27], 16).byteValue();
                v27++;
            }
            if (v13 != 0) {
                System.out.println("Error: Pattern not valid!\n\nPattern can not be \"*4\" or \"A*\", etc.\n\n It can only be ** ");
            } else {
                String v11 = p28[0];
                try {
                    java.io.File v18 = new java.io.File;
                    v18(v11);
                    java.io.File v19 = new java.io.File;
                    v19(v11.replace("/data/data/", "/mnt/asec/"));
                } catch (java.io.FileNotFoundException v20) {
                    System.out.println("Error: Program files are not found!\n\nMove Program to internal storage.");
                } catch (java.io.PrintStream v2) {
                }
                if (v18.exists()) {
                    v19 = v18;
                }
                if (!v19.exists()) {
                    v19 = new java.io.File;
                    v19(new StringBuilder().append(v11).append("-1").toString());
                }
                if (!v19.exists()) {
                    v19 = new java.io.File;
                    v19(v11.replace("-1", "-2"));
                }
                if (v19.exists()) {
                    java.nio.channels.FileChannel v1 = new java.io.RandomAccessFile(v19, "rw").getChannel();
                    java.nio.MappedByteBuffer v14 = v1.map(java.nio.channels.FileChannel$MapMode.READ_WRITE, 0, ((long) ((int) v1.size())));
                    int v23 = 0;
                    long v16 = 0;
                    try {
                        while (v14.hasRemaining()) {
                            int v10 = v14.position();
                            byte v9 = v14.get();
                            if (v9 == v7[0]) {
                                if (v25[0] == 0) {
                                    v8[0] = v9;
                                }
                                int v15 = 1;
                                v14.position((v10 + 1));
                                byte v24 = v14.get();
                                while ((v24 == v7[v15]) || (v21[v15] == 1)) {
                                    if (v25[v15] == 0) {
                                        v8[v15] = v24;
                                    }
                                    v15++;
                                    if (v15 != v7.length) {
                                        v24 = v14.get();
                                    } else {
                                        v14.position(v10);
                                        v14.put(v8);
                                        v14.force();
                                        System.out.println(new StringBuilder().append("Offset in file:").append(Integer.toHexString(v10)).append(" - Patch done!\n").toString());
                                        v23 = 1;
                                        break;
                                    }
                                }
                            }
                            v14.position((v10 + 1));
                            v16++;
                        }
                    } catch (Exception v12) {
                        System.out.println(new StringBuilder().append("").append(v12).toString());
                    }
                    v1.close();
                    if (v23 == 0) {
                        System.out.println("Error: Pattern not found!\nor patch is already applied?!\n");
                    }
                } else {
                    throw new java.io.FileNotFoundException();
                }
            }
        }
        com.chelpus.Utils.exitFromRootJava();
        return;
    }
}
