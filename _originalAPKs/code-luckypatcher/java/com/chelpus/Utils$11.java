package com.chelpus;
final class Utils$11 implements android.view.View$OnKeyListener {
    final synthetic android.view.WindowManager val$manager;

    Utils$11(android.view.WindowManager p1)
    {
        this.val$manager = p1;
        return;
    }

    public boolean onKey(android.view.View p4, int p5, android.view.KeyEvent p6)
    {
        System.out.println(new StringBuilder().append("keyCode ").append(p5).toString());
        if (p5 == 4) {
            this.val$manager.removeView(p4.getRootView());
        }
        return 0;
    }
}
