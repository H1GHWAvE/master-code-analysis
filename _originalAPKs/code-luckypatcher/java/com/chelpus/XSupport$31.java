package com.chelpus;
 class XSupport$31 extends de.robv.android.xposed.XC_MethodHook {
    final synthetic com.chelpus.XSupport this$0;

    XSupport$31(com.chelpus.XSupport p1)
    {
        this.this$0 = p1;
        return;
    }

    protected void afterHookedMethod(de.robv.android.xposed.XC_MethodHook$MethodHookParam p3)
    {
        this.this$0.skipGB = 0;
        return;
    }

    protected void beforeHookedMethod(de.robv.android.xposed.XC_MethodHook$MethodHookParam p3)
    {
        this.this$0.loadPrefs();
        if ((com.chelpus.XSupport.enable) && ((com.chelpus.XSupport.hide) && (this.this$0.checkForHideApp(p3)))) {
            this.this$0.skipGB = 1;
        }
        return;
    }
}
