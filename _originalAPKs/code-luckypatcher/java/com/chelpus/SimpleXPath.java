package com.chelpus;
public class SimpleXPath {
    public static final String XPATH_SEPARATOR = "/";
    org.w3c.dom.Document mDocument;

    public SimpleXPath(java.io.File p4)
    {
        this.mDocument = javax.xml.parsers.DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new java.io.FileInputStream(p4));
        return;
    }

    private void getNodes(org.w3c.dom.Node p9, String p10, java.util.List p11)
    {
        int v1;
        String[] v3 = p10.split("/", 2);
        if (v3.length != 1) {
            v1 = 0;
        } else {
            v1 = 1;
        }
        int v2 = p9.getFirstChild();
        while (v2 != 0) {
            if (v3[0].equals(v2.getNodeName())) {
                if (v1 == 0) {
                    this.getNodes(v2, v3[1], p11);
                } else {
                    p11.add(new com.chelpus.SimpleXPath$XNode(this, v2));
                }
            }
            try {
                v2 = v2.getNextSibling();
            } catch (Exception v0) {
                v2 = 0;
            }
        }
        return;
    }

    public org.w3c.dom.Node getRoot()
    {
        return this.mDocument;
    }

    public com.chelpus.SimpleXPath$XNode getXPathNode(org.w3c.dom.Node p10, String p11)
    {
        int v1;
        com.chelpus.SimpleXPath$XNode v4_0 = 0;
        String[] v3 = p11.split("/", 2);
        if (v3.length != 1) {
            v1 = 0;
        } else {
            v1 = 1;
        }
        int v2 = p10.getFirstChild();
        while (v2 != 0) {
            if (!v3[0].equals(v2.getNodeName())) {
                try {
                    v2 = v2.getNextSibling();
                } catch (Exception v0) {
                    v2 = 0;
                }
            } else {
                if (v1 == 0) {
                    v4_0 = this.getXPathNode(v2, v3[1]);
                    break;
                } else {
                    v4_0 = new com.chelpus.SimpleXPath$XNode(this, v2);
                    break;
                }
            }
        }
        return v4_0;
    }

    public java.util.List getXPathNodes(org.w3c.dom.Node p2, String p3)
    {
        java.util.ArrayList v0_1 = new java.util.ArrayList();
        this.getNodes(p2, p3, v0_1);
        return v0_1;
    }
}
