package com.chelpus;
public class GradientTextView extends android.widget.TextView {

    public GradientTextView(android.content.Context p3)
    {
        this(p3, 0, -1);
        return;
    }

    public GradientTextView(android.content.Context p2, android.util.AttributeSet p3)
    {
        this(p2, p3, -1);
        return;
    }

    public GradientTextView(android.content.Context p1, android.util.AttributeSet p2, int p3)
    {
        this(p1, p2, p3);
        return;
    }

    protected void onLayout(boolean p10, int p11, int p12, int p13, int p14)
    {
        this = super.onLayout(p10, p11, p12, p13, p14);
        if (p10) {
            this.getPaint().setShader(new android.graphics.LinearGradient(0, 0, ((float) this.getWidth()), 0, android.graphics.Color.parseColor("#FF6699cc"), android.graphics.Color.parseColor("#906699cc"), android.graphics.Shader$TileMode.CLAMP));
        }
        return;
    }
}
