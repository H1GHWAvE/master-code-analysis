package com.chelpus;
public class SimpleXPath$XNode {
    org.w3c.dom.Node mNode;
    final synthetic com.chelpus.SimpleXPath this$0;

    public SimpleXPath$XNode(com.chelpus.SimpleXPath p1, org.w3c.dom.Node p2)
    {
        this.this$0 = p1;
        this.mNode = p2;
        return;
    }

    String getAttribute(String p8)
    {
        String v0 = ((org.w3c.dom.Element) this.mNode).getAttribute(p8);
        if (v0.length() != 0) {
            return v0;
        } else {
            Object[] v4_1 = new Object[2];
            v4_1[0] = this.getName();
            v4_1[1] = p8;
            throw new com.chelpus.XNodeException(String.format("Node [%s] has no [%s] attribute!", v4_1));
        }
    }

    com.chelpus.SimpleXPath$XNode getChild(String p7)
    {
        java.util.List v1 = this.this$0.getXPathNodes(this.mNode, p7);
        if (v1.size() != 0) {
            return ((com.chelpus.SimpleXPath$XNode) v1.get(0));
        } else {
            Object[] v3_2 = new Object[2];
            v3_2[0] = this.mNode.getNodeName();
            v3_2[1] = p7;
            throw new com.chelpus.XNodeException(String.format("Node [%s] has no child at path [%s]!", v3_2));
        }
    }

    java.util.List getChildren(String p7)
    {
        java.util.List v1 = this.this$0.getXPathNodes(this.mNode, p7);
        if (!v1.isEmpty()) {
            return v1;
        } else {
            Object[] v3_2 = new Object[2];
            v3_2[0] = this.mNode.getNodeName();
            v3_2[1] = p7;
            throw new com.chelpus.XNodeException(String.format("Node [%s] has no children at path [%s]!", v3_2));
        }
    }

    float getFloatAttribute(String p2)
    {
        return Float.parseFloat(this.getAttribute(p2));
    }

    public int getIntAttribute(String p2)
    {
        return Integer.parseInt(this.getAttribute(p2));
    }

    public String getName()
    {
        return this.mNode.getNodeName();
    }

    public String getStringAttribute(String p2)
    {
        return this.getAttribute(p2);
    }

    public String getValue()
    {
        return this.mNode.getChildNodes().item(0).getNodeValue();
    }
}
