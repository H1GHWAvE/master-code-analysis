package com.googlecode.dex2jar.reader.io;
public class ArrayDataIn extends java.io.ByteArrayInputStream implements com.googlecode.dex2jar.reader.io.DataIn {
    private boolean isLE;
    private java.util.Stack stack;

    public ArrayDataIn(byte[] p2, int p3, int p4, boolean p5)
    {
        this(p2, p3, p4);
        this.stack = new java.util.Stack();
        this.isLE = p5;
        return;
    }

    public ArrayDataIn(byte[] p2, boolean p3)
    {
        this(p2);
        this.stack = new java.util.Stack();
        this.isLE = p3;
        return;
    }

    public static com.googlecode.dex2jar.reader.io.ArrayDataIn be(byte[] p2)
    {
        return new com.googlecode.dex2jar.reader.io.ArrayDataIn(p2, 0);
    }

    public static com.googlecode.dex2jar.reader.io.ArrayDataIn be(byte[] p2, int p3, int p4)
    {
        return new com.googlecode.dex2jar.reader.io.ArrayDataIn(p2, p3, p4, 0);
    }

    public static com.googlecode.dex2jar.reader.io.ArrayDataIn le(byte[] p2)
    {
        return new com.googlecode.dex2jar.reader.io.ArrayDataIn(p2, 1);
    }

    public static com.googlecode.dex2jar.reader.io.ArrayDataIn le(byte[] p2, int p3, int p4)
    {
        return new com.googlecode.dex2jar.reader.io.ArrayDataIn(p2, p3, p4, 1);
    }

    public int getCurrentPosition()
    {
        return (this.pos - this.mark);
    }

    public void move(int p2)
    {
        this.pos = (this.mark + p2);
        return;
    }

    public void pop()
    {
        this.pos = ((Integer) this.stack.pop()).intValue();
        return;
    }

    public void push()
    {
        this.stack.push(Integer.valueOf(this.pos));
        return;
    }

    public void pushMove(int p1)
    {
        this.push();
        this.move(p1);
        return;
    }

    public int readByte()
    {
        return ((byte) this.readUByte());
    }

    public byte[] readBytes(int p4)
    {
        byte[] v0 = new byte[p4];
        try {
            super.read(v0);
            return v0;
        } catch (java.io.IOException v1) {
            throw new RuntimeException(v1);
        }
    }

    public int readIntx()
    {
        return this.readUIntx();
    }

    public long readLeb128()
    {
        int v0 = 0;
        long v2 = 0;
        do {
            int v1 = this.readUByte();
            v2 |= (((long) (v1 & 127)) << v0);
            v0 += 7;
        } while((v1 & 128) != 0);
        if (((1 << (v0 - 1)) & v2) != 0) {
            v2 -= (1 << v0);
        }
        return v2;
    }

    public int readShortx()
    {
        return ((short) this.readUShortx());
    }

    public int readUByte()
    {
        if (this.pos < this.count) {
            return super.read();
        } else {
            throw new RuntimeException("EOF");
        }
    }

    public int readUIntx()
    {
        int v0_5;
        if (!this.isLE) {
            v0_5 = ((((this.readUByte() << 24) | (this.readUByte() << 16)) | (this.readUByte() << 8)) | this.readUByte());
        } else {
            v0_5 = (((this.readUByte() | (this.readUByte() << 8)) | (this.readUByte() << 16)) | (this.readUByte() << 24));
        }
        return v0_5;
    }

    public long readULeb128()
    {
        long v2_0 = 0;
        int v1 = 0;
        int v0 = this.readUByte();
        while ((v0 & 128) != 0) {
            v2_0 |= ((long) ((v0 & 127) << v1));
            v1 += 7;
            v0 = this.readUByte();
        }
        return (v2_0 | ((long) ((v0 & 127) << v1)));
    }

    public int readUShortx()
    {
        int v0_3;
        if (!this.isLE) {
            v0_3 = ((this.readUByte() << 8) | this.readUByte());
        } else {
            v0_3 = (this.readUByte() | (this.readUByte() << 8));
        }
        return v0_3;
    }

    public void skip(int p3)
    {
        super.skip(((long) p3));
        return;
    }
}
