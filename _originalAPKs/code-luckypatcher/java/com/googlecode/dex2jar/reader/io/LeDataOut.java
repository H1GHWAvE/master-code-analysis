package com.googlecode.dex2jar.reader.io;
public class LeDataOut implements com.googlecode.dex2jar.reader.io.DataOut {
    private java.io.OutputStream os;

    public LeDataOut(java.io.OutputStream p1)
    {
        this.os = p1;
        return;
    }

    public void writeByte(int p2)
    {
        this.os.write(p2);
        return;
    }

    public void writeBytes(byte[] p2)
    {
        this.os.write(p2);
        return;
    }

    public void writeInt(int p3)
    {
        this.os.write(p3);
        this.os.write((p3 >> 8));
        this.os.write((p3 >> 16));
        this.os.write((p3 >> 24));
        return;
    }

    public void writeShort(int p3)
    {
        this.os.write(p3);
        this.os.write((p3 >> 8));
        return;
    }
}
