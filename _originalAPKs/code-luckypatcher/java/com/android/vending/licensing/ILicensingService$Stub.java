package com.android.vending.licensing;
public abstract class ILicensingService$Stub extends android.os.Binder implements com.android.vending.licensing.ILicensingService {
    private static final String DESCRIPTOR = "com.android.vending.licensing.ILicensingService";
    static final int TRANSACTION_checkLicense = 1;

    public ILicensingService$Stub()
    {
        this.attachInterface(this, "com.android.vending.licensing.ILicensingService");
        return;
    }

    public static com.android.vending.licensing.ILicensingService asInterface(android.os.IBinder p2)
    {
        com.android.vending.licensing.ILicensingService v0_2;
        if (p2 != null) {
            com.android.vending.licensing.ILicensingService v0_0 = p2.queryLocalInterface("com.android.vending.licensing.ILicensingService");
            if ((v0_0 == null) || (!(v0_0 instanceof com.android.vending.licensing.ILicensingService))) {
                v0_2 = new com.android.vending.licensing.ILicensingService$Stub$Proxy(p2);
            } else {
                v0_2 = ((com.android.vending.licensing.ILicensingService) v0_0);
            }
        } else {
            v0_2 = 0;
        }
        return v0_2;
    }

    public android.os.IBinder asBinder()
    {
        return this;
    }

    public boolean onTransact(int p7, android.os.Parcel p8, android.os.Parcel p9, int p10)
    {
        boolean v4 = 1;
        switch (p7) {
            case 1:
                p8.enforceInterface("com.android.vending.licensing.ILicensingService");
                this.checkLicense(p8.readLong(), p8.readString(), com.android.vending.licensing.ILicenseResultListener$Stub.asInterface(p8.readStrongBinder()));
                break;
            case 1598968902:
                p9.writeString("com.android.vending.licensing.ILicensingService");
                break;
            default:
                v4 = super.onTransact(p7, p8, p9, p10);
        }
        return v4;
    }
}
