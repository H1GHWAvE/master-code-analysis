package com.android.vending.licensing;
 class ILicenseResultListener$Stub$Proxy implements com.android.vending.licensing.ILicenseResultListener {
    private android.os.IBinder mRemote;

    ILicenseResultListener$Stub$Proxy(android.os.IBinder p1)
    {
        this.mRemote = p1;
        return;
    }

    public android.os.IBinder asBinder()
    {
        return this.mRemote;
    }

    public String getInterfaceDescriptor()
    {
        return "com.android.vending.licensing.ILicenseResultListener";
    }

    public void verifyLicense(int p6, String p7, String p8)
    {
        android.os.Parcel v0 = android.os.Parcel.obtain();
        try {
            v0.writeInterfaceToken("com.android.vending.licensing.ILicenseResultListener");
            v0.writeInt(p6);
            v0.writeString(p7);
            v0.writeString(p8);
            this.mRemote.transact(1, v0, 0, 1);
            v0.recycle();
            return;
        } catch (Throwable v1_2) {
            v0.recycle();
            throw v1_2;
        }
    }
}
