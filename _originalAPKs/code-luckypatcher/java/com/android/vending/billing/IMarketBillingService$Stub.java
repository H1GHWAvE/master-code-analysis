package com.android.vending.billing;
public abstract class IMarketBillingService$Stub extends android.os.Binder implements com.android.vending.billing.IMarketBillingService {
    private static final String DESCRIPTOR = "com.android.vending.billing.IMarketBillingService";
    static final int TRANSACTION_sendBillingRequest = 1;

    public IMarketBillingService$Stub()
    {
        this.attachInterface(this, "com.android.vending.billing.IMarketBillingService");
        return;
    }

    public static com.android.vending.billing.IMarketBillingService asInterface(android.os.IBinder p2)
    {
        com.android.vending.billing.IMarketBillingService v0_2;
        if (p2 != null) {
            com.android.vending.billing.IMarketBillingService v0_0 = p2.queryLocalInterface("com.android.vending.billing.IMarketBillingService");
            if ((v0_0 == null) || (!(v0_0 instanceof com.android.vending.billing.IMarketBillingService))) {
                v0_2 = new com.android.vending.billing.IMarketBillingService$Stub$Proxy(p2);
            } else {
                v0_2 = ((com.android.vending.billing.IMarketBillingService) v0_0);
            }
        } else {
            v0_2 = 0;
        }
        return v0_2;
    }

    public android.os.IBinder asBinder()
    {
        return this;
    }

    public boolean onTransact(int p5, android.os.Parcel p6, android.os.Parcel p7, int p8)
    {
        boolean v2 = 1;
        switch (p5) {
            case 1:
                int v0_0;
                p6.enforceInterface("com.android.vending.billing.IMarketBillingService");
                if (p6.readInt() == 0) {
                    v0_0 = 0;
                } else {
                    v0_0 = ((android.os.Bundle) android.os.Bundle.CREATOR.createFromParcel(p6));
                }
                android.os.Bundle v1 = this.sendBillingRequest(v0_0);
                p7.writeNoException();
                if (v1 == null) {
                    p7.writeInt(0);
                } else {
                    p7.writeInt(1);
                    v1.writeToParcel(p7, 1);
                }
                break;
            case 1598968902:
                p7.writeString("com.android.vending.billing.IMarketBillingService");
                break;
            default:
                v2 = super.onTransact(p5, p6, p7, p8);
        }
        return v2;
    }
}
