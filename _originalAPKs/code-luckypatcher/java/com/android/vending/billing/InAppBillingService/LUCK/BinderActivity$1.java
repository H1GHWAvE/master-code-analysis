package com.android.vending.billing.InAppBillingService.LUCK;
 class BinderActivity$1 extends android.widget.ArrayAdapter {
    final synthetic com.android.vending.billing.InAppBillingService.LUCK.BinderActivity this$0;

    BinderActivity$1(com.android.vending.billing.InAppBillingService.LUCK.BinderActivity p1, android.content.Context p2, int p3, java.util.List p4)
    {
        this.this$0 = p1;
        this(p2, p3, p4);
        return;
    }

    public android.view.View getView(int p16, android.view.View p17, android.view.ViewGroup p18)
    {
        android.view.View v7 = p17;
        if (p17 == null) {
            v7 = ((android.app.Activity) this.this$0.context).getLayoutInflater().inflate(2130968592, p18, 0);
        }
        android.widget.TextView v9_1 = ((android.widget.TextView) v7.findViewById(2131558475));
        android.widget.TextView v10_1 = ((android.widget.TextView) v7.findViewById(2131558476));
        v9_1.setTextAppearance(this.getContext(), com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getSizeText());
        v10_1.setTextAppearance(this.getContext(), com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getSizeText());
        android.widget.Button v1_1 = ((android.widget.Button) v7.findViewById(2131558477));
        android.widget.ToggleButton v11_1 = ((android.widget.ToggleButton) v7.findViewById(2131558443));
        v11_1.setOnClickListener(new com.android.vending.billing.InAppBillingService.LUCK.BinderActivity$1$1(this, v11_1, ((android.widget.ProgressBar) v7.findViewById(2131558451)), p16));
        v11_1.setChecked(com.chelpus.Utils.checkBind(((com.android.vending.billing.InAppBillingService.LUCK.BindItem) this.getItem(p16))));
        v1_1.setOnClickListener(new com.android.vending.billing.InAppBillingService.LUCK.BinderActivity$1$2(this, p16));
        v9_1.setTextAppearance(this.this$0.context, this.this$0.sizeText);
        v10_1.setTextAppearance(this.this$0.context, this.this$0.sizeText);
        v9_1.setText(com.chelpus.Utils.getColoredText(new StringBuilder().append(com.chelpus.Utils.getText(2131165236)).append("\n").toString(), "#ff00ff00", "bold"));
        v9_1.append(com.chelpus.Utils.getColoredText(((com.android.vending.billing.InAppBillingService.LUCK.BindItem) this.getItem(p16)).SourceDir, "#ffffffff", "italic"));
        v10_1.setText(com.chelpus.Utils.getColoredText(new StringBuilder().append(com.chelpus.Utils.getText(2131165237)).append("\n").toString(), "#ffffff00", "bold"));
        v10_1.append(com.chelpus.Utils.getColoredText(((com.android.vending.billing.InAppBillingService.LUCK.BindItem) this.getItem(p16)).TargetDir.replace("~chelpus_disabled~", ""), "#ffffffff", "italic"));
        return v7;
    }
}
