package com.android.vending.billing.InAppBillingService.LUCK.dialogs;
public class Progress_Dialog {
    public static com.android.vending.billing.InAppBillingService.LUCK.ProgressDlg dialog;
    android.app.Dialog dialog2;
    android.support.v4.app.FragmentManager fm;
    String message;
    String title;

    public Progress_Dialog()
    {
        this.message = "";
        this.title = "";
        this.fm = 0;
        this.dialog2 = 0;
        return;
    }

    public static com.android.vending.billing.InAppBillingService.LUCK.dialogs.Progress_Dialog newInstance()
    {
        return new com.android.vending.billing.InAppBillingService.LUCK.dialogs.Progress_Dialog();
    }

    public void dismiss()
    {
        if (this.dialog2 != null) {
            this.dialog2.dismiss();
            this.dialog2 = 0;
        }
        return;
    }

    public boolean isShowing()
    {
        if ((this.dialog2 == null) || (!this.dialog2.isShowing())) {
            int v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        return v0_3;
    }

    public android.app.Dialog onCreateDialog()
    {
        com.android.vending.billing.InAppBillingService.LUCK.dialogs.Progress_Dialog.dialog = new com.android.vending.billing.InAppBillingService.LUCK.ProgressDlg(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.getContext());
        if (this.title.equals("")) {
            this.title = com.chelpus.Utils.getText(2131165747);
        }
        com.android.vending.billing.InAppBillingService.LUCK.dialogs.Progress_Dialog.dialog.setTitle(this.title);
        if (this.message.equals("")) {
            this.message = com.chelpus.Utils.getText(2131165515);
        }
        com.android.vending.billing.InAppBillingService.LUCK.dialogs.Progress_Dialog.dialog.setMessage(this.message);
        com.android.vending.billing.InAppBillingService.LUCK.dialogs.Progress_Dialog.dialog.setCancelable(1);
        com.android.vending.billing.InAppBillingService.LUCK.dialogs.Progress_Dialog.dialog.setOnCancelListener(new com.android.vending.billing.InAppBillingService.LUCK.dialogs.Progress_Dialog$1(this));
        return com.android.vending.billing.InAppBillingService.LUCK.dialogs.Progress_Dialog.dialog.create();
    }

    public void setCancelable(boolean p2)
    {
        if (this.dialog2 != null) {
            this.dialog2.setCancelable(p2);
        }
        return;
    }

    public void setIndeterminate(boolean p3)
    {
        if (com.android.vending.billing.InAppBillingService.LUCK.dialogs.Progress_Dialog.dialog == null) {
            this.onCreateDialog();
        }
        if (p3) {
            com.android.vending.billing.InAppBillingService.LUCK.dialogs.Progress_Dialog.dialog.setDefaultStyle();
        } else {
            com.android.vending.billing.InAppBillingService.LUCK.dialogs.Progress_Dialog.dialog.setIncrementStyle();
        }
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.getContext().runOnUiThread(new com.android.vending.billing.InAppBillingService.LUCK.dialogs.Progress_Dialog$3(this));
        return;
    }

    public void setMax(int p3)
    {
        if (com.android.vending.billing.InAppBillingService.LUCK.dialogs.Progress_Dialog.dialog == null) {
            this.onCreateDialog();
        }
        com.android.vending.billing.InAppBillingService.LUCK.dialogs.Progress_Dialog.dialog.setMax(p3);
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.getContext().runOnUiThread(new com.android.vending.billing.InAppBillingService.LUCK.dialogs.Progress_Dialog$4(this));
        return;
    }

    public void setMessage(String p3)
    {
        if (com.android.vending.billing.InAppBillingService.LUCK.dialogs.Progress_Dialog.dialog == null) {
            this.onCreateDialog();
        }
        this.message = p3;
        com.android.vending.billing.InAppBillingService.LUCK.dialogs.Progress_Dialog.dialog.setMessage(this.message);
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.getContext().runOnUiThread(new com.android.vending.billing.InAppBillingService.LUCK.dialogs.Progress_Dialog$2(this));
        return;
    }

    public void setProgress(int p3)
    {
        if (com.android.vending.billing.InAppBillingService.LUCK.dialogs.Progress_Dialog.dialog == null) {
            this.onCreateDialog();
        }
        com.android.vending.billing.InAppBillingService.LUCK.dialogs.Progress_Dialog.dialog.setProgress(p3);
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.getContext().runOnUiThread(new com.android.vending.billing.InAppBillingService.LUCK.dialogs.Progress_Dialog$5(this));
        return;
    }

    public void setTitle(String p3)
    {
        if (com.android.vending.billing.InAppBillingService.LUCK.dialogs.Progress_Dialog.dialog == null) {
            this.onCreateDialog();
        }
        this.title = p3;
        com.android.vending.billing.InAppBillingService.LUCK.dialogs.Progress_Dialog.dialog.setTitle(this.title);
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.getContext().runOnUiThread(new com.android.vending.billing.InAppBillingService.LUCK.dialogs.Progress_Dialog$6(this));
        return;
    }

    public void showDialog()
    {
        if (this.dialog2 == null) {
            this.dialog2 = this.onCreateDialog();
        }
        if (this.dialog2 != null) {
            this.dialog2.show();
        }
        return;
    }
}
