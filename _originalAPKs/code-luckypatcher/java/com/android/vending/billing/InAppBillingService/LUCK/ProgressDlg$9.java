package com.android.vending.billing.InAppBillingService.LUCK;
 class ProgressDlg$9 implements java.lang.Runnable {
    final synthetic com.android.vending.billing.InAppBillingService.LUCK.ProgressDlg this$0;
    final synthetic int val$progress;

    ProgressDlg$9(com.android.vending.billing.InAppBillingService.LUCK.ProgressDlg p1, int p2)
    {
        this.this$0 = p1;
        this.val$progress = p2;
        return;
    }

    public void run()
    {
        android.widget.ProgressBar v1_1 = ((android.widget.ProgressBar) this.this$0.dialog.findViewById(2131558621));
        v1_1.setVisibility(0);
        v1_1.setProgress(this.val$progress);
        android.widget.TextView v0_1 = ((android.widget.TextView) this.this$0.dialog.findViewById(2131558623));
        String v2_6 = this.this$0.format;
        Object[] v3_3 = new Object[2];
        v3_3[0] = Integer.valueOf(this.val$progress);
        v3_3[1] = Integer.valueOf(com.android.vending.billing.InAppBillingService.LUCK.ProgressDlg.max);
        v0_1.setText(String.format(v2_6, v3_3));
        return;
    }
}
