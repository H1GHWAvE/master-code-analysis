package com.android.vending.billing.InAppBillingService.LUCK.widgets;
public class BinderWidgetConfigureActivity extends android.app.Activity {
    private static final String PREFS_NAME = "com.android.vending.billing.InAppBillingService.LUCK.widgets.BinderWidget";
    private static final String PREF_PREFIX_KEY = "appwidget_";
    public java.util.ArrayList bindes;
    public android.content.Context context;
    public android.widget.ListView lv;
    int mAppWidgetId;
    android.widget.EditText mAppWidgetText;
    public int sizeText;

    public BinderWidgetConfigureActivity()
    {
        this.mAppWidgetId = 0;
        this.bindes = 0;
        this.lv = 0;
        this.sizeText = 0;
        return;
    }

    static void deleteTitlePref(android.content.Context p3, int p4)
    {
        android.content.SharedPreferences$Editor v0 = p3.getSharedPreferences("com.android.vending.billing.InAppBillingService.LUCK.widgets.BinderWidget", 4).edit();
        v0.remove(new StringBuilder().append("appwidget_").append(p4).toString());
        v0.commit();
        return;
    }

    static String loadTitlePref(android.content.Context p4, int p5)
    {
        String v1 = p4.getSharedPreferences("com.android.vending.billing.InAppBillingService.LUCK.widgets.BinderWidget", 4).getString(new StringBuilder().append("appwidget_").append(p5).toString(), 0);
        if (v1 == null) {
            v1 = "NOT_SAVED_BIND";
        }
        return v1;
    }

    static void saveTitlePref(android.content.Context p3, int p4, String p5)
    {
        android.content.SharedPreferences$Editor v0 = p3.getSharedPreferences("com.android.vending.billing.InAppBillingService.LUCK.widgets.BinderWidget", 4).edit();
        v0.putString(new StringBuilder().append("appwidget_").append(p4).toString(), p5);
        v0.commit();
        return;
    }

    public void onCreate(android.os.Bundle p10)
    {
        super.onCreate(p10);
        this.setResult(0);
        android.os.Bundle v2 = this.getIntent().getExtras();
        if (v2 != null) {
            this.mAppWidgetId = v2.getInt("appWidgetId", 0);
        }
        if (this.mAppWidgetId != 0) {
            this.context = this;
            java.io.File v0_1 = new java.io.File(new StringBuilder().append(this.getDir("binder", 0)).append("/bind.txt").toString());
            if (!v0_1.exists()) {
                try {
                    v0_1.createNewFile();
                } catch (java.io.IOException v1) {
                    v1.printStackTrace();
                }
            }
            this.bindes = com.android.vending.billing.InAppBillingService.LUCK.BinderActivity.getBindes(this.context);
            this.lv = new android.widget.ListView(this.context);
            if (this.bindes.size() == 0) {
                this.bindes.add(new com.android.vending.billing.InAppBillingService.LUCK.BindItem("empty", "empty"));
            }
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.adaptBind = new com.android.vending.billing.InAppBillingService.LUCK.widgets.BinderWidgetConfigureActivity$1(this, this, 2130968605, this.bindes);
            this.lv.setAdapter(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.adaptBind);
            this.lv.invalidateViews();
            this.lv.setBackgroundColor(-16777216);
            if ((this.bindes.size() != 1) || ((!((com.android.vending.billing.InAppBillingService.LUCK.BindItem) this.bindes.get(0)).TargetDir.equals("empty")) || (!((com.android.vending.billing.InAppBillingService.LUCK.BindItem) this.bindes.get(0)).SourceDir.equals("empty")))) {
                this.lv.setOnItemClickListener(new com.android.vending.billing.InAppBillingService.LUCK.widgets.BinderWidgetConfigureActivity$2(this));
            }
            this.setContentView(this.lv);
        } else {
            this.finish();
        }
        return;
    }
}
