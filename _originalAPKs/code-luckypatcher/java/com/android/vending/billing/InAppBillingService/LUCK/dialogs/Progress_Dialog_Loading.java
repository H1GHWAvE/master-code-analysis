package com.android.vending.billing.InAppBillingService.LUCK.dialogs;
public class Progress_Dialog_Loading {
    public static com.android.vending.billing.InAppBillingService.LUCK.ProgressDlg dialog;
    android.support.v4.app.FragmentActivity context;
    android.app.Dialog dialog2;
    android.support.v4.app.FragmentManager fm;
    String message;
    String title;

    static Progress_Dialog_Loading()
    {
        com.android.vending.billing.InAppBillingService.LUCK.dialogs.Progress_Dialog_Loading.dialog = 0;
        return;
    }

    public Progress_Dialog_Loading()
    {
        this.message = "";
        this.title = "";
        this.fm = 0;
        this.context = 0;
        this.dialog2 = 0;
        return;
    }

    public static com.android.vending.billing.InAppBillingService.LUCK.dialogs.Progress_Dialog_Loading newInstance()
    {
        return new com.android.vending.billing.InAppBillingService.LUCK.dialogs.Progress_Dialog_Loading();
    }

    public void dismiss()
    {
        if (this.dialog2 != null) {
            this.dialog2.dismiss();
            this.dialog2 = 0;
        }
        return;
    }

    public boolean isShowing()
    {
        if ((com.android.vending.billing.InAppBillingService.LUCK.dialogs.Progress_Dialog_Loading.dialog == null) || (!com.android.vending.billing.InAppBillingService.LUCK.dialogs.Progress_Dialog_Loading.dialog.isShowing())) {
            int v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        return v0_3;
    }

    public android.app.Dialog onCreateDialog()
    {
        com.android.vending.billing.InAppBillingService.LUCK.dialogs.Progress_Dialog_Loading.dialog = new com.android.vending.billing.InAppBillingService.LUCK.ProgressDlg(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.getContext());
        com.android.vending.billing.InAppBillingService.LUCK.dialogs.Progress_Dialog_Loading.dialog.setIncrementStyle();
        if (this.title.equals("")) {
            this.title = com.chelpus.Utils.getText(2131165423);
        }
        com.android.vending.billing.InAppBillingService.LUCK.dialogs.Progress_Dialog_Loading.dialog.setTitle(this.title);
        if (this.message.equals("")) {
            this.message = com.chelpus.Utils.getText(2131165747);
        }
        com.android.vending.billing.InAppBillingService.LUCK.dialogs.Progress_Dialog_Loading.dialog.setMessage(this.message);
        com.android.vending.billing.InAppBillingService.LUCK.dialogs.Progress_Dialog_Loading.dialog.setIcon(2130837555);
        com.android.vending.billing.InAppBillingService.LUCK.dialogs.Progress_Dialog_Loading.dialog.setCancelable(0);
        com.android.vending.billing.InAppBillingService.LUCK.dialogs.Progress_Dialog_Loading.dialog.setOnCancelListener(new com.android.vending.billing.InAppBillingService.LUCK.dialogs.Progress_Dialog_Loading$1(this));
        return com.android.vending.billing.InAppBillingService.LUCK.dialogs.Progress_Dialog_Loading.dialog.create();
    }

    public void setCancelable(boolean p2)
    {
        if (this.dialog2 != null) {
            this.dialog2.setCancelable(p2);
        }
        return;
    }

    public void setIndeterminate(boolean p4, android.app.Activity p5)
    {
        if (com.android.vending.billing.InAppBillingService.LUCK.dialogs.Progress_Dialog_Loading.dialog == null) {
            this.onCreateDialog();
        }
        if (!p4) {
            com.android.vending.billing.InAppBillingService.LUCK.dialogs.Progress_Dialog_Loading.dialog.setProgressNumberFormat("%1d/%2d");
        } else {
            com.android.vending.billing.InAppBillingService.LUCK.dialogs.Progress_Dialog_Loading.dialog.setProgressNumberFormat("");
        }
        if (p4) {
            com.android.vending.billing.InAppBillingService.LUCK.dialogs.Progress_Dialog_Loading.dialog.setDefaultStyle();
        } else {
            com.android.vending.billing.InAppBillingService.LUCK.dialogs.Progress_Dialog_Loading.dialog.setIncrementStyle();
        }
        try {
            if (!com.chelpus.Utils.onMainThread()) {
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.runToMain(new com.android.vending.billing.InAppBillingService.LUCK.dialogs.Progress_Dialog_Loading$3(this));
            } else {
                if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag != null) {
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.getChildFragmentManager().executePendingTransactions();
                }
            }
        } catch (Exception v0) {
            v0.printStackTrace();
        }
        return;
    }

    public void setMax(int p4)
    {
        if (com.android.vending.billing.InAppBillingService.LUCK.dialogs.Progress_Dialog_Loading.dialog == null) {
            this.onCreateDialog();
        }
        com.android.vending.billing.InAppBillingService.LUCK.dialogs.Progress_Dialog_Loading.dialog.setMax(p4);
        try {
            if (!com.chelpus.Utils.onMainThread()) {
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.runToMain(new com.android.vending.billing.InAppBillingService.LUCK.dialogs.Progress_Dialog_Loading$4(this));
            } else {
                if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag != null) {
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.getChildFragmentManager().executePendingTransactions();
                }
            }
        } catch (Exception v0) {
            v0.printStackTrace();
        }
        return;
    }

    public void setMessage(String p4)
    {
        this.message = p4;
        if (com.android.vending.billing.InAppBillingService.LUCK.dialogs.Progress_Dialog_Loading.dialog == null) {
            this.onCreateDialog();
        }
        com.android.vending.billing.InAppBillingService.LUCK.dialogs.Progress_Dialog_Loading.dialog.setMessage(this.message);
        try {
            if (!com.chelpus.Utils.onMainThread()) {
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.runToMain(new com.android.vending.billing.InAppBillingService.LUCK.dialogs.Progress_Dialog_Loading$2(this));
            } else {
                if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag != null) {
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.getChildFragmentManager().executePendingTransactions();
                }
            }
        } catch (Exception v0) {
            v0.printStackTrace();
        }
        return;
    }

    public void setProgress(int p4)
    {
        if (com.android.vending.billing.InAppBillingService.LUCK.dialogs.Progress_Dialog_Loading.dialog == null) {
            this.onCreateDialog();
        }
        com.android.vending.billing.InAppBillingService.LUCK.dialogs.Progress_Dialog_Loading.dialog.setProgress(p4);
        try {
            if (!com.chelpus.Utils.onMainThread()) {
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.runToMain(new com.android.vending.billing.InAppBillingService.LUCK.dialogs.Progress_Dialog_Loading$6(this));
            } else {
                if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag != null) {
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.getChildFragmentManager().executePendingTransactions();
                }
            }
        } catch (Exception v0) {
            v0.printStackTrace();
        }
        return;
    }

    public void setProgressNumberFormat(String p4)
    {
        if (com.android.vending.billing.InAppBillingService.LUCK.dialogs.Progress_Dialog_Loading.dialog == null) {
            this.onCreateDialog();
        }
        com.android.vending.billing.InAppBillingService.LUCK.dialogs.Progress_Dialog_Loading.dialog.setProgressNumberFormat(p4);
        try {
            if (!com.chelpus.Utils.onMainThread()) {
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.runToMain(new com.android.vending.billing.InAppBillingService.LUCK.dialogs.Progress_Dialog_Loading$5(this));
            } else {
                if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag != null) {
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.getChildFragmentManager().executePendingTransactions();
                }
            }
        } catch (Exception v0) {
            v0.printStackTrace();
        }
        return;
    }

    public void setTitle(String p4)
    {
        this.title = p4;
        if (com.android.vending.billing.InAppBillingService.LUCK.dialogs.Progress_Dialog_Loading.dialog == null) {
            this.onCreateDialog();
        }
        try {
            if (com.android.vending.billing.InAppBillingService.LUCK.dialogs.Progress_Dialog_Loading.dialog != null) {
                com.android.vending.billing.InAppBillingService.LUCK.dialogs.Progress_Dialog_Loading.dialog.setTitle(this.title);
                if (!com.chelpus.Utils.onMainThread()) {
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.runToMain(new com.android.vending.billing.InAppBillingService.LUCK.dialogs.Progress_Dialog_Loading$7(this));
                } else {
                    if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag != null) {
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.getChildFragmentManager().executePendingTransactions();
                    }
                }
            }
        } catch (Exception v0) {
            v0.printStackTrace();
        }
        return;
    }

    public void showDialog()
    {
        if (this.dialog2 == null) {
            this.dialog2 = this.onCreateDialog();
        }
        if (this.dialog2 != null) {
            this.dialog2.show();
        }
        return;
    }
}
