package com.android.vending.billing.InAppBillingService.LUCK.widgets;
 class BinderWidget$2 implements java.lang.Runnable {
    final synthetic com.android.vending.billing.InAppBillingService.LUCK.widgets.BinderWidget this$0;
    final synthetic android.content.Context val$context;
    final synthetic android.os.Handler val$handler;
    final synthetic android.content.Intent val$intent;

    BinderWidget$2(com.android.vending.billing.InAppBillingService.LUCK.widgets.BinderWidget p1, android.content.Intent p2, android.content.Context p3, android.os.Handler p4)
    {
        this.this$0 = p1;
        this.val$intent = p2;
        this.val$context = p3;
        this.val$handler = p4;
        return;
    }

    public void run()
    {
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.binder_process = 1;
        com.chelpus.Utils.exitRoot();
        int v5 = this.val$intent.getIntExtra("appWidgetId", -1);
        if ((v5 != -1) && (!com.android.vending.billing.InAppBillingService.LUCK.widgets.BinderWidgetConfigureActivity.loadTitlePref(this.val$context, v5).equals("NOT_SAVED_BIND"))) {
            com.android.vending.billing.InAppBillingService.LUCK.BindItem v8_1 = new com.android.vending.billing.InAppBillingService.LUCK.BindItem(com.android.vending.billing.InAppBillingService.LUCK.widgets.BinderWidgetConfigureActivity.loadTitlePref(this.val$context, v5));
            android.widget.RemoteViews v11_1 = new android.widget.RemoteViews(this.val$context.getPackageName(), 2130968589);
            v8_1.TargetDir = v8_1.TargetDir.replaceAll("~chelpus_disabled~", "");
            v8_1.SourceDir = v8_1.SourceDir.replaceAll("~chelpus_disabled~", "");
            if (com.chelpus.Utils.checkBind(v8_1)) {
                v8_1.TargetDir = v8_1.TargetDir.replaceAll("~chelpus_disabled~", "");
                v8_1.SourceDir = v8_1.SourceDir.replaceAll("~chelpus_disabled~", "");
                java.util.ArrayList v1_0 = com.android.vending.billing.InAppBillingService.LUCK.BinderActivity.getBindes(this.val$context);
                int v3_0 = 0;
                int v12_20 = v1_0.iterator();
                while (v12_20.hasNext()) {
                    com.android.vending.billing.InAppBillingService.LUCK.BindItem v7_1 = ((com.android.vending.billing.InAppBillingService.LUCK.BindItem) v12_20.next());
                    if ((v7_1.SourceDir.replaceAll("~chelpus_disabled~", "").equals(v8_1.SourceDir.replaceAll("~chelpus_disabled~", ""))) && (v7_1.TargetDir.replaceAll("~chelpus_disabled~", "").equals(v8_1.TargetDir.replaceAll("~chelpus_disabled~", "")))) {
                        v7_1.TargetDir = new StringBuilder().append("~chelpus_disabled~").append(v7_1.TargetDir.replaceAll("~chelpus_disabled~", "")).toString();
                        v7_1.SourceDir = v7_1.SourceDir;
                        v3_0 = 1;
                    }
                }
                if (v3_0 != 0) {
                    com.android.vending.billing.InAppBillingService.LUCK.BinderActivity.savetoFile(v1_0, this.val$context);
                    if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.su) {
                        int v12_24 = new String[1];
                        v12_24[0] = new StringBuilder().append("umount \'").append(v8_1.TargetDir.replaceAll("~chelpus_disabled~", "")).append("\'").toString();
                        com.chelpus.Utils.cmd(v12_24);
                    } else {
                        com.chelpus.Utils.run_all(new StringBuilder().append("umount -f \'").append(v8_1.TargetDir.replaceAll("~chelpus_disabled~", "")).append("\'").toString());
                        com.chelpus.Utils.run_all(new StringBuilder().append("umount -l \'").append(v8_1.TargetDir.replaceAll("~chelpus_disabled~", "")).append("\'").toString());
                    }
                } else {
                    if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.su) {
                        int v12_39 = new String[1];
                        v12_39[0] = new StringBuilder().append("umount \'").append(v8_1.TargetDir.replaceAll("~chelpus_disabled~", "")).append("\'").toString();
                        com.chelpus.Utils.cmd(v12_39);
                    } else {
                        com.chelpus.Utils.run_all(new StringBuilder().append("umount -f \'").append(v8_1.TargetDir.replaceAll("~chelpus_disabled~", "")).append("\'").toString());
                        com.chelpus.Utils.run_all(new StringBuilder().append("umount -l \'").append(v8_1.TargetDir.replaceAll("~chelpus_disabled~", "")).append("\'").toString());
                    }
                    v11_1.setInt(2131558443, "setBackgroundResource", 2130837584);
                    v11_1.setTextColor(2131558442, android.graphics.Color.parseColor("#AAAAAA"));
                }
            } else {
                java.util.ArrayList v1_1 = com.android.vending.billing.InAppBillingService.LUCK.BinderActivity.getBindes(this.val$context);
                int v3_1 = 0;
                int v12_55 = v1_1.iterator();
                while (v12_55.hasNext()) {
                    com.android.vending.billing.InAppBillingService.LUCK.BindItem v7_3 = ((com.android.vending.billing.InAppBillingService.LUCK.BindItem) v12_55.next());
                    if ((v7_3.SourceDir.replaceAll("~chelpus_disabled~", "").equals(v8_1.SourceDir.replaceAll("~chelpus_disabled~", ""))) && (v7_3.TargetDir.replaceAll("~chelpus_disabled~", "").equals(v8_1.TargetDir.replaceAll("~chelpus_disabled~", "")))) {
                        v7_3.TargetDir = v7_3.TargetDir.replaceAll("~chelpus_disabled~", "");
                        v7_3.SourceDir = v7_3.SourceDir.replaceAll("~chelpus_disabled~", "");
                        com.android.vending.billing.InAppBillingService.LUCK.widgets.BinderWidgetConfigureActivity.saveTitlePref(this.val$context, v5, v7_3.toString());
                        v3_1 = 1;
                    }
                }
                if (v3_1 == 0) {
                    if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.su) {
                        int v12_58 = new String[1];
                        v12_58[0] = new StringBuilder().append("umount \'").append(v8_1.TargetDir.replaceAll("~chelpus_disabled~", "")).append("\'").toString();
                        com.chelpus.Utils.cmd(v12_58);
                    } else {
                        com.chelpus.Utils.run_all(new StringBuilder().append("umount -f \'").append(v8_1.TargetDir.replaceAll("~chelpus_disabled~", "")).append("\'").toString());
                        com.chelpus.Utils.run_all(new StringBuilder().append("umount -l \'").append(v8_1.TargetDir.replaceAll("~chelpus_disabled~", "")).append("\'").toString());
                    }
                    v11_1.setInt(2131558443, "setBackgroundResource", 2130837584);
                    v11_1.setTextColor(2131558442, android.graphics.Color.parseColor("#AAAAAA"));
                }
                com.android.vending.billing.InAppBillingService.LUCK.BinderActivity.savetoFile(v1_1, this.val$context);
                com.chelpus.Utils.verify_bind_and_run("mount", new StringBuilder().append("-o bind \'").append(v8_1.SourceDir.replaceAll("~chelpus_disabled~", "")).append("\' \'").append(v8_1.TargetDir.replaceAll("~chelpus_disabled~", "")).append("\'").toString(), v8_1.SourceDir.replaceAll("~chelpus_disabled~", ""), v8_1.TargetDir.replaceAll("~chelpus_disabled~", ""));
            }
            if (!com.chelpus.Utils.checkBind(v8_1)) {
                v11_1.setTextColor(2131558442, android.graphics.Color.parseColor("#FF0000"));
                v11_1.setInt(2131558443, "setBackgroundResource", 2130837584);
                this.val$handler.post(new com.android.vending.billing.InAppBillingService.LUCK.widgets.BinderWidget$2$2(this, v8_1));
            } else {
                v11_1.setTextColor(2131558442, android.graphics.Color.parseColor("#00FF00"));
                v11_1.setInt(2131558443, "setBackgroundResource", 2130837585);
                this.val$handler.post(new com.android.vending.billing.InAppBillingService.LUCK.widgets.BinderWidget$2$1(this, v8_1));
            }
            new android.content.ComponentName(this.val$context, com.android.vending.billing.InAppBillingService.LUCK.widgets.BinderWidget);
            android.appwidget.AppWidgetManager.getInstance(this.val$context).updateAppWidget(v5, v11_1);
            android.appwidget.AppWidgetManager v4 = android.appwidget.AppWidgetManager.getInstance(this.val$context);
            com.android.vending.billing.InAppBillingService.LUCK.widgets.BinderWidget.widget.onUpdate(this.val$context, v4, v4.getAppWidgetIds(new android.content.ComponentName(this.val$context, com.android.vending.billing.InAppBillingService.LUCK.widgets.BinderWidget)));
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.binder_process = 0;
        }
        return;
    }
}
