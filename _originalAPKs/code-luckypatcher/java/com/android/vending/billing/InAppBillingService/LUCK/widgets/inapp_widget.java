package com.android.vending.billing.InAppBillingService.LUCK.widgets;
public class inapp_widget extends android.appwidget.AppWidgetProvider {
    public static String ACTION_WIDGET_RECEIVER;
    public static String ACTION_WIDGET_RECEIVER_Updater;

    static inapp_widget()
    {
        com.android.vending.billing.InAppBillingService.LUCK.widgets.inapp_widget.ACTION_WIDGET_RECEIVER = "ActionReceiverInAppWidget";
        com.android.vending.billing.InAppBillingService.LUCK.widgets.inapp_widget.ACTION_WIDGET_RECEIVER_Updater = "ActionReceiverWidgetInAppUpdate";
        return;
    }

    public inapp_widget()
    {
        return;
    }

    public static void pushWidgetUpdate(android.content.Context p3, android.widget.RemoteViews p4)
    {
        android.appwidget.AppWidgetManager.getInstance(p3).updateAppWidget(new android.content.ComponentName(p3, com.android.vending.billing.InAppBillingService.LUCK.widgets.inapp_widget), p4);
        return;
    }

    public void onReceive(android.content.Context p14, android.content.Intent p15)
    {
        super.onReceive(p14, p15);
        String v0 = p15.getAction();
        if (com.android.vending.billing.InAppBillingService.LUCK.widgets.inapp_widget.ACTION_WIDGET_RECEIVER.equals(v0)) {
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.init();
            android.widget.RemoteViews v5_1 = new android.widget.RemoteViews(p14.getPackageName(), 2130968637);
            v5_1.setTextViewText(2131558636, "");
            v5_1.setViewVisibility(2131558637, 0);
            android.appwidget.AppWidgetManager v2_0 = android.appwidget.AppWidgetManager.getInstance(p14);
            v2_0.updateAppWidget(v2_0.getAppWidgetIds(new android.content.ComponentName(p14, com.android.vending.billing.InAppBillingService.LUCK.widgets.inapp_widget)), v5_1);
            if ((p14.getPackageManager().getComponentEnabledSetting(new android.content.ComponentName(p14, com.google.android.finsky.billing.iab.MarketBillingService)) != 2) && (p14.getPackageManager().getComponentEnabledSetting(new android.content.ComponentName(p14, com.google.android.finsky.billing.iab.InAppBillingService)) != 2)) {
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().setComponentEnabledSetting(new android.content.ComponentName(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance(), com.google.android.finsky.billing.iab.MarketBillingService), 2, 1);
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().setComponentEnabledSetting(new android.content.ComponentName(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance(), com.google.android.finsky.billing.iab.InAppBillingService), 2, 1);
                android.content.Intent v4_1 = new android.content.Intent(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance(), com.android.vending.billing.InAppBillingService.LUCK.widgets.inapp_widget);
                v4_1.setPackage(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance().getPackageName());
                v4_1.setAction(com.android.vending.billing.InAppBillingService.LUCK.widgets.inapp_widget.ACTION_WIDGET_RECEIVER_Updater);
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance().sendBroadcast(v4_1);
                android.widget.Toast.makeText(p14, "InApp-OFF", 0).show();
            } else {
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().setComponentEnabledSetting(new android.content.ComponentName(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance(), com.google.android.finsky.billing.iab.MarketBillingService), 1, 1);
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().setComponentEnabledSetting(new android.content.ComponentName(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance(), com.google.android.finsky.billing.iab.InAppBillingService), 1, 1);
                android.widget.Toast.makeText(p14, "InApp-ON", 0).show();
            }
        }
        if (com.android.vending.billing.InAppBillingService.LUCK.widgets.inapp_widget.ACTION_WIDGET_RECEIVER_Updater.equals(v0)) {
            try {
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.appDisabler = 1;
                android.appwidget.AppWidgetManager v2_1 = android.appwidget.AppWidgetManager.getInstance(p14);
                this.onUpdate(p14, v2_1, v2_1.getAppWidgetIds(new android.content.ComponentName(p14, com.android.vending.billing.InAppBillingService.LUCK.widgets.inapp_widget)));
            } catch (Exception v1) {
                v1.printStackTrace();
            }
        }
        return;
    }

    public void onUpdate(android.content.Context p9, android.appwidget.AppWidgetManager p10, int[] p11)
    {
        android.widget.RemoteViews v2_1 = new android.widget.RemoteViews(p9.getPackageName(), 2130968637);
        android.content.Intent v1_1 = new android.content.Intent(p9, com.android.vending.billing.InAppBillingService.LUCK.widgets.inapp_widget);
        v1_1.setAction(com.android.vending.billing.InAppBillingService.LUCK.widgets.inapp_widget.ACTION_WIDGET_RECEIVER);
        v1_1.putExtra("msg", "Hello Habrahabr");
        v2_1.setOnClickPendingIntent(2131558636, android.app.PendingIntent.getBroadcast(p9, 0, v1_1, 0));
        v2_1.setTextViewText(2131558636, "InApp");
        v2_1.setViewVisibility(2131558637, 8);
        if ((p9.getPackageManager().getComponentEnabledSetting(new android.content.ComponentName(p9, com.google.android.finsky.billing.iab.MarketBillingService)) != 2) && (p9.getPackageManager().getComponentEnabledSetting(new android.content.ComponentName(p9, com.google.android.finsky.billing.iab.InAppBillingService)) != 2)) {
            v2_1.setTextColor(2131558636, android.graphics.Color.parseColor("#00FF00"));
        } else {
            v2_1.setTextColor(2131558636, android.graphics.Color.parseColor("#FF0000"));
        }
        p10.updateAppWidget(p11, v2_1);
        return;
    }
}
