package com.android.vending.billing.InAppBillingService.LUCK.widgets;
public class AppDisablerWidgetConfigureActivity extends android.app.Activity {
    private static final String PREFS_NAME = "com.android.vending.billing.InAppBillingService.LUCK.widgets.AppDisablerWidget";
    private static final String PREF_PREFIX_KEY = "appwidget_";
    public android.content.Context context;
    public android.widget.ListView lv;
    int mAppWidgetId;
    android.widget.EditText mAppWidgetText;
    public String[] packages;
    public int sizeText;

    public AppDisablerWidgetConfigureActivity()
    {
        this.mAppWidgetId = 0;
        this.packages = 0;
        this.lv = 0;
        this.sizeText = 0;
        return;
    }

    static void deleteTitlePref(android.content.Context p3, int p4)
    {
        android.content.SharedPreferences$Editor v0 = p3.getSharedPreferences("com.android.vending.billing.InAppBillingService.LUCK.widgets.AppDisablerWidget", 4).edit();
        v0.remove(new StringBuilder().append("appwidget_").append(p4).toString());
        v0.commit();
        return;
    }

    static String loadTitlePref(android.content.Context p4, int p5)
    {
        String v1 = p4.getSharedPreferences("com.android.vending.billing.InAppBillingService.LUCK.widgets.AppDisablerWidget", 4).getString(new StringBuilder().append("appwidget_").append(p5).toString(), 0);
        if (v1 == null) {
            v1 = "NOT_SAVED_APP_DISABLER";
        }
        return v1;
    }

    static void saveTitlePref(android.content.Context p3, int p4, String p5)
    {
        android.content.SharedPreferences$Editor v0 = p3.getSharedPreferences("com.android.vending.billing.InAppBillingService.LUCK.widgets.AppDisablerWidget", 4).edit();
        v0.putString(new StringBuilder().append("appwidget_").append(p4).toString(), p5);
        v0.commit();
        return;
    }

    public void onCreate(android.os.Bundle p13)
    {
        android.widget.ListView v6_0 = 0;
        super.onCreate(p13);
        this.setResult(0);
        android.os.Bundle v1 = this.getIntent().getExtras();
        if (v1 != null) {
            this.mAppWidgetId = v1.getInt("appWidgetId", 0);
        }
        if (this.mAppWidgetId != 0) {
            this.context = this;
            this.packages = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPackages();
            java.util.ArrayList v5_1 = new java.util.ArrayList();
            com.android.vending.billing.InAppBillingService.LUCK.widgets.AppDisablerWidgetConfigureActivity$2 v7_4 = this.packages;
            int v8 = v7_4.length;
            while (v6_0 < v8) {
                String v3 = v7_4[v6_0];
                if ((!v3.equals("android")) || (!v3.equals(this.context.getPackageName()))) {
                    try {
                        v5_1.add(new com.android.vending.billing.InAppBillingService.LUCK.widgets.PkgItem(v3, com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getPackageInfo(v3, 0).applicationInfo.loadLabel(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng()).toString()));
                    } catch (Exception v0_1) {
                        v0_1.printStackTrace();
                    }
                }
                v6_0++;
            }
            com.android.vending.billing.InAppBillingService.LUCK.widgets.PkgItem[] v4 = new com.android.vending.billing.InAppBillingService.LUCK.widgets.PkgItem[v5_1.size()];
            v5_1.toArray(v4);
            try {
                java.util.Arrays.sort(v4, new com.android.vending.billing.InAppBillingService.LUCK.widgets.AppDisablerWidgetConfigureActivity$byPkgName(this));
            } catch (Exception v0_0) {
                v0_0.printStackTrace();
            }
            this.lv = new android.widget.ListView(this.context);
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.adapt = new com.android.vending.billing.InAppBillingService.LUCK.widgets.AppDisablerWidgetConfigureActivity$1(this, this, 2130968622, v4);
            this.lv.setAdapter(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.adapt);
            this.lv.invalidateViews();
            this.lv.setBackgroundColor(-16777216);
            this.lv.setOnItemClickListener(new com.android.vending.billing.InAppBillingService.LUCK.widgets.AppDisablerWidgetConfigureActivity$2(this));
            this.setContentView(this.lv);
        } else {
            this.finish();
        }
        return;
    }
}
