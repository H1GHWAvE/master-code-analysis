package com.android.vending.billing.InAppBillingService.LUCK;
 class OnBootLuckyPatcher$3 implements java.lang.Runnable {
    final synthetic com.android.vending.billing.InAppBillingService.LUCK.OnBootLuckyPatcher this$0;
    final synthetic android.os.Handler val$handler;

    OnBootLuckyPatcher$3(com.android.vending.billing.InAppBillingService.LUCK.OnBootLuckyPatcher p1, android.os.Handler p2)
    {
        this.this$0 = p1;
        this.val$handler = p2;
        return;
    }

    public void run()
    {
        if (com.chelpus.Utils.exists(new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.toolfilesdir).append("/ClearDalvik.on").toString())) {
            com.chelpus.Utils.remount("/system", "rw");
            try {
                String v6_8 = new com.chelpus.Utils("");
                int v7_5 = new String[1];
                v7_5[0] = new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.dalvikruncommand).append(".clearDalvikCache ").append(com.android.vending.billing.InAppBillingService.LUCK.OnBootLuckyPatcher.contextB.getApplicationContext().getFilesDir().getAbsolutePath()).toString();
                System.out.println(v6_8.cmdRoot(v7_5));
            } catch (Exception v2) {
                v2.printStackTrace();
            }
            this.val$handler.post(new com.android.vending.billing.InAppBillingService.LUCK.OnBootLuckyPatcher$3$1(this));
            System.out.println(new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.OnBootLuckyPatcher.contextB.getApplicationContext().getFilesDir()).append("/reboot").toString());
            com.chelpus.Utils.reboot();
        }
        if (!com.chelpus.Utils.getCurrentRuntimeValue().equals("ART")) {
            if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().getBoolean("trigger_for_good_android_patch_on_boot", 0)) {
                com.chelpus.Utils.turn_off_patch_on_boot_all();
            }
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().edit().putBoolean("trigger_for_good_android_patch_on_boot", 1).commit();
            String v3 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().getString("patch_dalvik_on_boot_patterns", "");
            String v4_0 = "";
            if (!v3.equals("")) {
                if ((v3.contains("patch1")) && ((!com.chelpus.Utils.checkCoreJarPatch11()) && (!com.chelpus.Utils.checkCoreJarPatch12()))) {
                    v4_0 = new StringBuilder().append("").append("_patch1_").toString();
                }
                if ((v3.contains("patch2")) && (!com.chelpus.Utils.checkCoreJarPatch20())) {
                    v4_0 = new StringBuilder().append(v4_0).append("_patch2_").toString();
                }
                if (!v4_0.equals("")) {
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patchOnlyDalvikCore = 1;
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.install_market_to_system = 0;
                    System.out.println("patch only dalvik cache mode on boot");
                    String v0_0 = com.chelpus.Utils.getFileDalvikCache("/system/framework/core.jar").getAbsolutePath();
                    if (v0_0 == null) {
                        System.out.println("dalvik cache for core.jar not found");
                    }
                    String v1_0 = com.chelpus.Utils.getFileDalvikCache("/system/framework/services.jar").getAbsolutePath();
                    if (v1_0 == null) {
                        System.out.println("dalvik cache for services.jar not found");
                    }
                    if ((v0_0 == null) || ((v1_0 == null) || ((v0_0.equals("")) || (v1_0.equals(""))))) {
                        System.out.println("dalvik cache patch on boot skip");
                    } else {
                        String v6_54 = new com.chelpus.Utils("");
                        int v7_25 = new String[1];
                        v7_25[0] = new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.dalvikruncommand).append(".corepatch ").append(v4_0).append(" ").append(v0_0).append(" ").append(v1_0).append(" ").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance().getFilesDir()).append(" OnlyDalvik").toString();
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str = v6_54.cmdRoot(v7_25);
                        System.out.println(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str);
                        new com.chelpus.Utils("w").waitLP(4000);
                        if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str.contains("SU Java-Code Running!")) {
                            if ((v3.contains("patch1")) && ((!com.chelpus.Utils.checkCoreJarPatch11()) && (!com.chelpus.Utils.checkCoreJarPatch12()))) {
                                com.chelpus.Utils.turn_off_patch_on_boot("patch1");
                            }
                            if ((v3.contains("patch2")) && (!com.chelpus.Utils.checkCoreJarPatch20())) {
                                com.chelpus.Utils.turn_off_patch_on_boot("patch2");
                            }
                            if ((com.chelpus.Utils.checkCoreJarPatch11()) || ((com.chelpus.Utils.checkCoreJarPatch12()) || (com.chelpus.Utils.checkCoreJarPatch20()))) {
                                com.android.vending.billing.InAppBillingService.LUCK.OnBootLuckyPatcher.access$000(this.this$0, 254, new StringBuilder().append("Lucky Patcher - ").append(com.chelpus.Utils.getText(2131165578)).toString(), com.chelpus.Utils.getText(2131165578), com.chelpus.Utils.getText(2131165779));
                            }
                        }
                    }
                }
                String v4_1 = "";
                if ((v3.contains("patch3")) && (!com.chelpus.Utils.checkCoreJarPatch30(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng()))) {
                    v4_1 = "_patch3_";
                }
                if (!v4_1.equals("")) {
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patchOnlyDalvikCore = 1;
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.install_market_to_system = 0;
                    System.out.println("patch only dalvik cache mode on boot");
                    String v0_1 = com.chelpus.Utils.getFileDalvikCache("/system/framework/core.jar").getAbsolutePath();
                    if (v0_1 == null) {
                        System.out.println("dalvik cache for core.jar not found");
                    }
                    String v1_1 = com.chelpus.Utils.getFileDalvikCache("/system/framework/services.jar").getAbsolutePath();
                    if (v1_1 == null) {
                        System.out.println("dalvik cache for services.jar not found");
                    }
                    if ((v0_1 == null) || ((v1_1 == null) || ((v0_1.equals("")) || (v1_1.equals(""))))) {
                        System.out.println("dalvik cache patch on boot skip");
                    } else {
                        String v6_95 = new com.chelpus.Utils("");
                        int v7_37 = new String[1];
                        v7_37[0] = new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.dalvikruncommand).append(".corepatch ").append(v4_1).append(" ").append(v0_1).append(" ").append(v1_1).append(" ").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance().getFilesDir()).append(" OnlyDalvik").toString();
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str = v6_95.cmdRoot(v7_37);
                        System.out.println(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str);
                        if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str.contains("SU Java-Code Running!")) {
                            new com.chelpus.Utils("w").waitLP(4000);
                            if ((!v3.contains("patch3")) || (com.chelpus.Utils.checkCoreJarPatch30(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng()))) {
                                com.android.vending.billing.InAppBillingService.LUCK.OnBootLuckyPatcher.access$000(this.this$0, 255, new StringBuilder().append("Lucky Patcher - ").append(com.chelpus.Utils.getText(2131165578)).toString(), com.chelpus.Utils.getText(2131165578), com.chelpus.Utils.getText(2131165780));
                            } else {
                                com.chelpus.Utils.turn_off_patch_on_boot("patch3");
                            }
                        }
                    }
                }
            }
        }
        return;
    }
}
