package com.android.vending.billing.InAppBillingService.LUCK;
public class OnBootLuckyPatcher extends android.content.BroadcastReceiver {
    public static String[] bootlist;
    public static android.content.Context contextB;
    public static int notifyIndex;

    static OnBootLuckyPatcher()
    {
        int v0_1 = new String[1];
        v0_1[0] = "empty";
        com.android.vending.billing.InAppBillingService.LUCK.OnBootLuckyPatcher.bootlist = v0_1;
        com.android.vending.billing.InAppBillingService.LUCK.OnBootLuckyPatcher.contextB = 0;
        com.android.vending.billing.InAppBillingService.LUCK.OnBootLuckyPatcher.notifyIndex = 0;
        return;
    }

    public OnBootLuckyPatcher()
    {
        return;
    }

    static synthetic void access$000(com.android.vending.billing.InAppBillingService.LUCK.OnBootLuckyPatcher p0, int p1, String p2, String p3, String p4)
    {
        p0.showNotify(p1, p2, p3, p4);
        return;
    }

    private void showNotify(int p15, String p16, String p17, String p18)
    {
        if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().getBoolean("hide_notify", 0)) {
            long v9 = System.currentTimeMillis();
            android.content.Context v4 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance();
            android.app.PendingIntent v1 = android.app.PendingIntent.getActivity(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance(), 0, new android.content.Intent(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance(), com.android.vending.billing.InAppBillingService.LUCK.patchActivity), 0);
            android.app.Notification v6_1 = new android.app.Notification(2130837552, p17, v9);
            v6_1.setLatestEventInfo(v4, p16, p18, v1);
            ((android.app.NotificationManager) com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance().getSystemService("notification")).notify(p15, v6_1);
        }
        return;
    }

    public void onReceive(android.content.Context p10, android.content.Intent p11)
    {
        System.out.println("load LP");
        com.android.vending.billing.InAppBillingService.LUCK.OnBootLuckyPatcher.contextB = p10;
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.init();
        new Thread(new com.android.vending.billing.InAppBillingService.LUCK.OnBootLuckyPatcher$1(this)).start();
        if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.su) {
            if ((p11.getAction().equals("android.intent.action.UMS_DISCONNECTED")) || (p11.getAction().equals("android.intent.action.ACTION_POWER_DISCONNECTED"))) {
                System.out.println("load LP");
                new Thread(new com.android.vending.billing.InAppBillingService.LUCK.OnBootLuckyPatcher$2(this, p10)).start();
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patchOnBoot = 1;
            }
            if (p11.getAction().equals("android.intent.action.EXTERNAL_APPLICATIONS_AVAILABLE")) {
                System.out.println("LuckyPatcher: ACTION_EXTERNAL_APPLICATIONS_AVAILABLE");
                if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().getBoolean("OnBootService", 0)) {
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().edit().putBoolean("OnBootService", 0).commit();
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patchOnBoot = 1;
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance().startService(new android.content.Intent(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance(), com.android.vending.billing.InAppBillingService.LUCK.PatchService));
                }
            }
            android.os.Handler v0_1 = new android.os.Handler();
            if (p11.getAction().equals("android.intent.action.BOOT_COMPLETED")) {
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().edit().putBoolean("OnBootService", 1).commit();
                android.app.AlarmManager v2_1 = ((android.app.AlarmManager) p10.getSystemService("alarm"));
                android.content.Intent v1_1 = new android.content.Intent(p10, com.android.vending.billing.InAppBillingService.LUCK.OnAlarmReceiver);
                v1_1.setAction(com.android.vending.billing.InAppBillingService.LUCK.OnAlarmReceiver.ACTION_WIDGET_RECEIVER);
                v2_1.set(2, 300000, android.app.PendingIntent.getBroadcast(p10, 0, v1_1, 0));
                new Thread(new com.android.vending.billing.InAppBillingService.LUCK.OnBootLuckyPatcher$3(this, v0_1)).start();
            }
        }
        if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().getBoolean("OnBootService", 0)) {
            com.chelpus.Utils.exit();
        }
        return;
    }
}
