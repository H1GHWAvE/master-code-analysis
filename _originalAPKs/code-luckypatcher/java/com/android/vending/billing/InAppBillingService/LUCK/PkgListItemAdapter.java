package com.android.vending.billing.InAppBillingService.LUCK;
public class PkgListItemAdapter extends android.widget.BaseExpandableListAdapter {
    public static final int TEXT_DEFAULT = 0;
    public static final int TEXT_LARGE = 2;
    public static final int TEXT_MEDIUM = 1;
    public static final int TEXT_SMALL;
    boolean Ready;
    java.util.ArrayList add;
    public int[] childMenu;
    public int[] childMenuNoRoot;
    public int[] childMenuSystem;
    private android.content.Context context;
    public com.android.vending.billing.InAppBillingService.LUCK.PkgListItem[] data;
    java.util.ArrayList del;
    private android.graphics.drawable.Drawable disabled;
    private android.widget.ImageView imgIcon;
    private int layoutResourceId;
    private android.widget.TextView on_top_txt;
    public com.android.vending.billing.InAppBillingService.LUCK.PkgListItem[] orig;
    private int size;
    public java.util.Comparator sorter;
    private android.widget.TextView txtStatus;
    private android.widget.TextView txtTitle;

    public PkgListItemAdapter(android.content.Context p5, int p6, int p7, java.util.List p8)
    {
        this.Ready = 0;
        this.disabled = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getRes().getDrawable(2130837536);
        this.childMenu = 0;
        this.childMenuNoRoot = 0;
        this.childMenuSystem = 0;
        this.context = p5;
        this.layoutResourceId = p6;
        this.size = p7;
        int v1_4 = new com.android.vending.billing.InAppBillingService.LUCK.PkgListItem[p8.size()];
        this.data = ((com.android.vending.billing.InAppBillingService.LUCK.PkgListItem[]) ((com.android.vending.billing.InAppBillingService.LUCK.PkgListItem[]) p8.toArray(v1_4)));
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.plia = this;
        android.graphics.drawable.Drawable v0 = this.disabled;
        v0.setColorFilter(android.graphics.Color.parseColor("#fe6c00"), android.graphics.PorterDuff$Mode.MULTIPLY);
        this.disabled = v0;
        return;
    }

    public PkgListItemAdapter(android.content.Context p6, int p7, java.util.List p8)
    {
        this.Ready = 0;
        this.disabled = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getRes().getDrawable(2130837536);
        this.childMenu = 0;
        this.childMenuNoRoot = 0;
        this.childMenuSystem = 0;
        this.context = p6;
        this.layoutResourceId = p7;
        this.size = 0;
        int v1_3 = new com.android.vending.billing.InAppBillingService.LUCK.PkgListItem[p8.size()];
        this.data = ((com.android.vending.billing.InAppBillingService.LUCK.PkgListItem[]) ((com.android.vending.billing.InAppBillingService.LUCK.PkgListItem[]) p8.toArray(v1_3)));
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.plia = this;
        android.graphics.drawable.Drawable v0 = this.disabled;
        v0.setColorFilter(android.graphics.Color.parseColor("#fe6c00"), android.graphics.PorterDuff$Mode.MULTIPLY);
        this.disabled = v0;
        return;
    }

    static synthetic android.content.Context access$000(com.android.vending.billing.InAppBillingService.LUCK.PkgListItemAdapter p1)
    {
        return p1.context;
    }

    public void add(com.android.vending.billing.InAppBillingService.LUCK.PkgListItem p5)
    {
        System.out.println(new StringBuilder().append("add ").append(p5.pkgName).toString());
        java.util.ArrayList v0_1 = new java.util.ArrayList(java.util.Arrays.asList(this.data));
        v0_1.add(p5);
        com.android.vending.billing.InAppBillingService.LUCK.PkgListItem[] v1_4 = new com.android.vending.billing.InAppBillingService.LUCK.PkgListItem[v0_1.size()];
        this.data = v1_4;
        v0_1.toArray(this.data);
        this.sort();
        this.notifyDataSetChanged();
        return;
    }

    public boolean checkItem(String p8)
    {
        int v2 = 0;
        try {
            com.android.vending.billing.InAppBillingService.LUCK.PkgListItem[] v4 = this.data;
            int v3 = 0;
        } catch (Exception v1) {
            v1.printStackTrace();
            return v2;
        }
        while (v3 < v4.length) {
            if (!v4[v3].pkgName.contentEquals(p8)) {
                v3++;
            } else {
                v2 = 1;
                break;
            }
        }
        return v2;
    }

    public Integer getChild(int p2, int p3)
    {
        return Integer.valueOf(this.childMenu[p3]);
    }

    public bridge synthetic Object getChild(int p2, int p3)
    {
        return this.getChild(p2, p3);
    }

    public long getChildId(int p3, int p4)
    {
        return ((long) ((p3 * 10) + p4));
    }

    public android.view.View getChildView(int p8, int p9, boolean p10, android.view.View p11, android.view.ViewGroup p12)
    {
        if (p11 == null) {
            p11 = ((android.view.LayoutInflater) this.context.getSystemService("layout_inflater")).inflate(2130968604, 0);
        }
        android.widget.TextView v2_1 = ((android.widget.TextView) p11.findViewById(2131558526));
        v2_1.setTextAppearance(this.context, com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getSizeText());
        v2_1.setTextAppearance(this.context, com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getSizeText());
        v2_1.setText(com.chelpus.Utils.getText(this.getChild(p8, p9).intValue()));
        android.widget.ImageView v0_1 = ((android.widget.ImageView) p11.findViewById(2131558525));
        switch (this.getChild(p8, p9).intValue()) {
            case 2131165197:
                v0_1.setImageDrawable(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getRes().getDrawable(2130837567));
                break;
            case 2131165206:
                v0_1.setImageDrawable(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getRes().getDrawable(2130837569));
                break;
            case 2131165207:
                v0_1.setImageDrawable(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getRes().getDrawable(2130837557));
                break;
            case 2131165253:
                v0_1.setImageDrawable(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getRes().getDrawable(2130837529));
                break;
            case 2131165288:
                v0_1.setImageDrawable(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getRes().getDrawable(2130837567));
                break;
            case 2131165322:
                v0_1.setImageDrawable(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getRes().getDrawable(2130837580));
                break;
            case 2131165554:
                v0_1.setImageDrawable(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getRes().getDrawable(2130837569));
                break;
            case 2131165556:
                v0_1.setImageDrawable(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getRes().getDrawable(2130837569));
                break;
            case 2131165731:
                v0_1.setImageDrawable(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getRes().getDrawable(2130837578));
                break;
        }
        return p11;
    }

    public int getChildrenCount(int p11)
    {
        int v2_0 = 0;
        if (this.getGroup(p11) != null) {
            try {
                String v0 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getPackageInfo(this.getGroup(p11).pkgName, 0).applicationInfo.sourceDir;
            } catch (Exception v1_0) {
                v1_0.printStackTrace();
            } catch (Exception v1_1) {
                v1_1.printStackTrace();
            }
            if ((com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.su) && ((v0 != null) && (!v0.startsWith("/system/")))) {
                int[] v3_8 = new int[8];
                this.childMenu = v3_8;
                this.childMenu[0] = 2131165207;
                this.childMenu[1] = 2131165322;
                this.childMenu[2] = 2131165197;
                this.childMenu[3] = 2131165288;
                this.childMenu[4] = 2131165731;
                this.childMenu[5] = 2131165253;
                if (v0.startsWith("/mnt/asec/")) {
                    this.childMenu[6] = 2131165554;
                } else {
                    this.childMenu[6] = 2131165556;
                }
                this.childMenu[7] = 2131165206;
            }
            if ((com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.su) && ((v0 != null) && (v0.startsWith("/system/")))) {
                int[] v3_24 = new int[7];
                this.childMenu = v3_24;
                this.childMenu[0] = 2131165207;
                this.childMenu[1] = 2131165322;
                this.childMenu[2] = 2131165197;
                this.childMenu[3] = 2131165288;
                this.childMenu[4] = 2131165731;
                this.childMenu[5] = 2131165253;
                this.childMenu[6] = 2131165206;
            }
            if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.su) {
                int[] v3_33 = new int[6];
                this.childMenu = v3_33;
                this.childMenu[0] = 2131165207;
                this.childMenu[1] = 2131165322;
                this.childMenu[2] = 2131165197;
                this.childMenu[3] = 2131165288;
                this.childMenu[4] = 2131165731;
                this.childMenu[5] = 2131165206;
            }
            if (v0 == null) {
                int[] v3_40 = new int[1];
                this.childMenu = v3_40;
                this.childMenu[0] = 2131165757;
            }
        }
        if (this.childMenu != null) {
            v2_0 = this.childMenu.length;
        }
        return v2_0;
    }

    public android.widget.Filter getFilter()
    {
        return new com.android.vending.billing.InAppBillingService.LUCK.PkgListItemAdapter$2(this);
    }

    public com.android.vending.billing.InAppBillingService.LUCK.PkgListItem getGroup(int p2)
    {
        return this.data[p2];
    }

    public bridge synthetic Object getGroup(int p2)
    {
        return this.getGroup(p2);
    }

    public int getGroupCount()
    {
        return this.data.length;
    }

    public long getGroupId(int p3)
    {
        return ((long) p3);
    }

    public android.view.View getGroupView(int p43, boolean p44, android.view.View p45, android.view.ViewGroup p46)
    {
        try {
            android.view.View v29;
            com.android.vending.billing.InAppBillingService.LUCK.PkgListItem v27 = this.getGroup(p43);
        } catch (Exception v20) {
            v29 = new android.view.View;
            v29(this.context);
            return v29;
        }
        if ((v27 != null) && (!v27.hidden)) {
            v29 = p45;
            if (p45 == null) {
                v29 = ((android.view.LayoutInflater) this.context.getSystemService("layout_inflater")).inflate(this.layoutResourceId, p46, 0);
            }
            this.txtTitle = ((android.widget.TextView) v29.findViewById(2131558478));
            this.txtStatus = ((android.widget.TextView) v29.findViewById(2131558479));
            this.imgIcon = ((android.widget.ImageView) v29.findViewById(2131558456));
            this.on_top_txt = ((android.widget.TextView) v29.findViewById(2131558604));
            android.widget.CheckBox v16_1 = ((android.widget.CheckBox) v29.findViewById(2131558528));
            android.widget.CheckBox v17_1 = ((android.widget.CheckBox) v29.findViewById(2131558603));
            android.widget.TextView v18_1 = ((android.widget.TextView) v29.findViewById(2131558605));
            android.widget.CheckBox v34_1 = ((android.widget.CheckBox) v29.findViewById(2131558602));
            if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.adapterSelect) {
                if (v34_1.getVisibility() == 0) {
                    v34_1.setVisibility(8);
                    v16_1.setVisibility(0);
                    v17_1.setVisibility(0);
                    v18_1.setVisibility(0);
                    this.on_top_txt.setVisibility(0);
                }
            } else {
                v34_1.setVisibility(0);
                v16_1.setVisibility(8);
                v17_1.setVisibility(8);
                v18_1.setVisibility(8);
                this.on_top_txt.setVisibility(8);
                v34_1.setTag(Integer.valueOf(p43));
                v34_1.setOnClickListener(new com.android.vending.billing.InAppBillingService.LUCK.PkgListItemAdapter$1(this));
            }
            if (!v27.selected) {
                v34_1.setChecked(0);
            } else {
                v34_1.setChecked(1);
            }
            v17_1.setChecked(v27.modified);
            v17_1.setClickable(0);
            if (v27.system) {
                if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getPackageInfo(v27.pkgName, 0).applicationInfo.sourceDir.startsWith("/system")) {
                    v18_1.setText("INT");
                    v18_1.setTextColor(-16711936);
                } else {
                    v18_1.setText("SYS");
                    v18_1.setTextColor(-65281);
                }
            } else {
                if (!v27.on_sd) {
                    v18_1.setText("INT");
                    v18_1.setTextColor(-16711936);
                } else {
                    v18_1.setText("SD");
                    v18_1.setTextColor(-256);
                }
            }
            v18_1.setClickable(0);
            v16_1.setChecked(v27.odex);
            v16_1.setClickable(0);
            this.txtTitle.setText(v27.name);
            try {
                if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().getBoolean("no_icon", 0)) {
                    if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.goodMemory) {
                        android.widget.Toast.makeText(this.context, "Out of memory! Icon not loaded!", 0).show();
                    } else {
                        this.imgIcon.setImageDrawable(0);
                        if (v27.icon == null) {
                            int v22 = ((int) ((1108082688 * com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getRes().getDisplayMetrics().density) + 1056964608));
                            try {
                                int v2 = ((android.graphics.drawable.BitmapDrawable) com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getApplicationIcon(v27.pkgName)).getBitmap();
                            } catch (Exception v20_0) {
                                v20_0.printStackTrace();
                            }
                            int v5 = v2.getWidth();
                            int v6 = v2.getHeight();
                            float v33 = (((float) v22) / ((float) v5));
                            float v31 = (((float) v22) / ((float) v6));
                            int v7_1 = new android.graphics.Matrix();
                            v7_1.postScale(v33, v31);
                            try {
                                v27.icon = new android.graphics.drawable.BitmapDrawable(android.graphics.Bitmap.createBitmap(v2, 0, 0, v5, v6, v7_1, 1));
                            } catch (Exception v20_1) {
                                v20_1.printStackTrace();
                                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.goodMemory = 0;
                            }
                            v27.saveItem();
                        }
                        if (v27.enable) {
                            this.imgIcon.setImageDrawable(v27.icon);
                        } else {
                            this.imgIcon.setImageDrawable(this.disabled);
                        }
                    }
                }
            } catch (OutOfMemoryError v9) {
                v9.printStackTrace();
                System.gc();
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().edit().putBoolean("no_icon", 1).commit();
            } catch (Exception v20_2) {
                v20_2.printStackTrace();
                if (v27.name == null) {
                    this.remove(v27.pkgName);
                    this.notifyDataSetChanged();
                } else {
                    if (v27.name.equals("")) {
                    }
                }
                System.out.println(new StringBuilder().append("LuckyPatcher(PackageListItemAdapter): ").append(v20_2).toString());
            }
            this.txtTitle.setTextAppearance(this.context, com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getSizeText());
            this.txtStatus.setTextAppearance(this.context, com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getSizeText());
            this.txtStatus.setTextAppearance(this.context, 16973894);
            String v19 = "#ffcc7943";
            switch (v27.stored) {
                case 1:
                    v19 = "#fff0e442";
                case 2:
                    v19 = "#ff00ff73";
                case 3:
                case 5:
                    break;
                case 4:
                    v19 = "#ff00ffff";
                case 3:
                case 5:
                    break;
                case 6:
                    v19 = "#ffff0055";
                    break;
                default:
                    if (v27.stored != 0) {
                        if ((v27.stored == 0) && (v27.ads)) {
                            v19 = "#ff00ffff";
                        }
                        if ((v27.stored == 0) && (v27.lvl)) {
                            v19 = "#ff00ff73";
                        }
                        this.txtTitle.setTextColor(android.graphics.Color.parseColor(v19));
                        this.txtStatus.setTextColor(-7829368);
                        String v35 = "";
                        String v24 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().getString("force_language", "default");
                        if (!v24.equals("default")) {
                            java.util.Locale v13_0 = 0;
                            String[] v36 = v24.split("_");
                            if (v36.length == 1) {
                                v13_0 = new java.util.Locale(v36[0]);
                            }
                            if (v36.length == 2) {
                                v13_0 = new java.util.Locale(v36[0], v36[1], "");
                            }
                            if (v36.length == 3) {
                                v13_0 = new java.util.Locale(v36[0], v36[1], v36[2]);
                            }
                            java.util.Locale.setDefault(v13_0);
                            android.content.res.Configuration v11_1 = new android.content.res.Configuration();
                            v11_1.locale = v13_0;
                            try {
                                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getRes().updateConfiguration(v11_1, com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getRes().getDisplayMetrics());
                            } catch (Exception v20_3) {
                                v20_3.printStackTrace();
                            }
                        } else {
                            java.util.Locale.setDefault(java.util.Locale.getDefault());
                            android.content.res.Configuration v12_1 = new android.content.res.Configuration();
                            v12_1.locale = android.content.res.Resources.getSystem().getConfiguration().locale;
                            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getRes().updateConfiguration(v12_1, com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getRes().getDisplayMetrics());
                        }
                        if (v27.ads) {
                            v35 = com.chelpus.Utils.getText(2131165682);
                        }
                        if (v27.lvl) {
                            if (v27.ads) {
                                v35 = new StringBuilder().append(v35).append("\n").append(com.chelpus.Utils.getText(2131165689)).toString();
                            } else {
                                v35 = com.chelpus.Utils.getText(2131165689);
                            }
                        } else {
                            if ((!v27.ads) && (!v27.billing)) {
                                v35 = com.chelpus.Utils.getText(2131165692);
                            }
                        }
                        if (v27.custom) {
                            v35 = com.chelpus.Utils.getText(2131165686);
                        }
                        if (v27.custom) {
                            this.txtTitle.setTextColor(-256);
                        }
                        int v21 = 0;
                        while (v21 < com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.bootlist.length) {
                            if ((v27.boot_ads) || ((v27.boot_custom) || ((v27.boot_lvl) || (v27.boot_manual)))) {
                                this.txtTitle.setTextColor(-65281);
                            }
                            v21++;
                        }
                        if (v27.billing) {
                            if (!v35.equals("")) {
                                v35 = new StringBuilder().append(v35).append("\n").append(com.chelpus.Utils.getText(2131165684)).toString();
                            } else {
                                v35 = com.chelpus.Utils.getText(2131165684);
                            }
                            int v4_47;
                            if (v27.lvl) {
                                v4_47 = 0;
                            } else {
                                v4_47 = 1;
                            }
                            android.widget.TextView v3_151;
                            if (v27.custom) {
                                v3_151 = 0;
                            } else {
                                v3_151 = 1;
                            }
                            if ((v3_151 & v4_47) != 0) {
                                this.txtTitle.setTextColor(android.graphics.Color.parseColor("#c5b5ff"));
                            }
                        }
                        this.txtStatus.setText(v35);
                        if (v27.system) {
                            this.txtTitle.setTextColor(-162281);
                        }
                        this.txtTitle.setTypeface(this.txtTitle.getTypeface(), 0);
                        if (!v27.enable) {
                            this.txtTitle.setTextColor(-7829368);
                            this.txtStatus.setTextColor(-7829368);
                        }
                        if (v27.stored != 0) {
                            this.on_top_txt.setVisibility(4);
                        } else {
                            this.on_top_txt.setVisibility(0);
                            this.on_top_txt.setText(com.chelpus.Utils.getText(2131165588));
                        }
                        if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.adapterSelect) {
                            return v29;
                        } else {
                            this.on_top_txt.setVisibility(8);
                            if ((com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.adapterSelectType != 2131165247) && (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.adapterSelectType != 2131165763)) {
                                return v29;
                            } else {
                                this.on_top_txt.setVisibility(8);
                                java.io.File v10_2 = new java.io.File(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getPackageInfo(v27.pkgName, 0).applicationInfo.sourceDir);
                                android.widget.TextView v3_175 = this.txtStatus;
                                int v4_66 = new StringBuilder().append(com.chelpus.Utils.getText(2131165202)).append(" ");
                                com.android.vending.billing.InAppBillingService.LUCK.PkgListItemAdapter v0_129 = new Object[1];
                                Object[] v38_1 = v0_129;
                                v38_1[0] = Float.valueOf((((float) v10_2.length()) / 1233125376));
                                v3_175.setText(v4_66.append(String.format("%.3f", v38_1)).append(" Mb").toString());
                                return v29;
                            }
                        }
                    } else {
                        if ((v27.ads) || (v27.lvl)) {
                        } else {
                            v19 = "#ffff0055";
                        }
                    }
            }
            if ((v27.stored == 0) && ((!v27.ads) && (!v27.lvl))) {
            }
        } else {
            v29 = new android.view.View;
            v29(this.context);
            return v29;
        }
    }

    public com.android.vending.billing.InAppBillingService.LUCK.PkgListItem getItem(int p2)
    {
        return this.data[p2];
    }

    public com.android.vending.billing.InAppBillingService.LUCK.PkgListItem getItem(String p6)
    {
        com.android.vending.billing.InAppBillingService.LUCK.PkgListItem[] v2 = this.data;
        int v1 = 0;
        while (v1 < v2.length) {
            int v0 = v2[v1];
            if (!v0.pkgName.contentEquals(p6)) {
                v1++;
            }
            return v0;
        }
        v0 = 0;
        return v0;
    }

    public boolean hasStableIds()
    {
        return 1;
    }

    public boolean isChildSelectable(int p2, int p3)
    {
        return 1;
    }

    public void notifyDataSetChanged()
    {
        super.notifyDataSetChanged();
        return;
    }

    public void notifyDataSetChanged(com.android.vending.billing.InAppBillingService.LUCK.PkgListItem p3)
    {
        super.notifyDataSetChanged();
        if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.database == null) {
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.database = new com.android.vending.billing.InAppBillingService.LUCK.DatabaseHelper(this.context);
        }
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.database.savePackage(p3);
        return;
    }

    public void onGroupCollapsed(int p1)
    {
        super.onGroupCollapsed(p1);
        return;
    }

    public void onGroupCollapsedAll()
    {
        if (this.data.length > 0) {
            int v0 = 0;
            while (v0 < this.data.length) {
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli = 0;
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.lv.collapseGroup(v0);
                v0++;
            }
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli = 0;
        }
        return;
    }

    public void onGroupExpanded(int p3)
    {
        if (p3 != com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.lastExpandedGroupPosition) {
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.lv.collapseGroup(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.lastExpandedGroupPosition);
        }
        super.onGroupExpanded(p3);
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli = this.getGroup(p3);
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.lastExpandedGroupPosition = p3;
        return;
    }

    public void refreshPkgs(boolean p5)
    {
        if ((com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.addapps) || (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.refresh)) {
            System.out.println("LuckyPatcher: finalize refreshPackages.");
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.refresh = 0;
        } else {
            System.out.println("LuckyPatcher: start refreshPackages.");
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.addapps = 1;
            Thread v0_1 = new Thread(new com.android.vending.billing.InAppBillingService.LUCK.PkgListItemAdapter$Refresh_Packages(this));
            v0_1.setPriority(1);
            v0_1.start();
        }
        return;
    }

    public void remove(String p5)
    {
        if (this.checkItem(p5)) {
            com.android.vending.billing.InAppBillingService.LUCK.PkgListItem[] v2 = new com.android.vending.billing.InAppBillingService.LUCK.PkgListItem[(this.data.length - 1)];
            int v1 = 0;
            int v0 = 0;
            while (v0 < this.data.length) {
                if (this.data[v0].pkgName.equals(p5)) {
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.lv.collapseGroup(v0);
                } else {
                    v2[v1] = this.data[v0];
                    v1++;
                }
                v0++;
            }
            this.data = v2;
            this.notifyDataSetChanged();
        }
        return;
    }

    public void setTextSize(int p1)
    {
        this.size = p1;
        this.notifyDataSetChanged();
        return;
    }

    public int size()
    {
        return this.data.length;
    }

    public void sort()
    {
        try {
            java.util.Arrays.sort(this.data, this.sorter);
        } catch (Exception v0) {
            v0.printStackTrace();
        }
        return;
    }

    public void updateItem(String p9)
    {
        boolean v4_0 = 0;
        try {
            int v5_0 = this.data;
        } catch (Exception v1) {
            System.out.println(new StringBuilder().append("LuckyPatcher (updateItem PkgListItemAdapter):").append(v1).toString());
            v1.printStackTrace();
            return;
        }
        while (v4_0 < v5_0.length) {
            String v0 = v5_0[v4_0];
            if (!v0.pkgName.contentEquals(p9)) {
                v4_0++;
            } else {
                com.android.vending.billing.InAppBillingService.LUCK.PkgListItem v2_1 = new com.android.vending.billing.InAppBillingService.LUCK.PkgListItem(this.context, p9, com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.days, 0);
                if (v2_1.equalsPli(v0)) {
                    break;
                }
                v2_1.saveItem();
                v0.pkgName = v2_1.pkgName;
                v0.name = v2_1.name;
                v0.storepref = v2_1.storepref;
                v0.stored = v2_1.stored;
                v0.hidden = v2_1.hidden;
                v0.boot_ads = v2_1.boot_ads;
                v0.boot_lvl = v2_1.boot_lvl;
                v0.boot_custom = v2_1.boot_custom;
                v0.boot_manual = v2_1.boot_manual;
                v0.custom = v2_1.custom;
                v0.on_sd = com.chelpus.Utils.isInstalledOnSdCard(v2_1.pkgName);
                v0.lvl = v2_1.lvl;
                v0.ads = v2_1.ads;
                v0.billing = v2_1.billing;
                if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().getBoolean(p9, 0)) {
                    v0.modified = 1;
                }
                v0.system = v2_1.system;
                v0.updatetime = v2_1.updatetime;
                if (!com.chelpus.Utils.isOdex(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getApplicationInfo(p9, 0).sourceDir)) {
                    v0.odex = 0;
                } else {
                    v0.odex = 1;
                }
                v0.enable = v2_1.enable;
                break;
            }
        }
        return;
    }
}
