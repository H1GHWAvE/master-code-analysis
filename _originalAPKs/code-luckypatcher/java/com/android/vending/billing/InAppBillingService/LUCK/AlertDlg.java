package com.android.vending.billing.InAppBillingService.LUCK;
public class AlertDlg {
    public android.widget.ArrayAdapter adapter;
    public android.content.Context context;
    public android.app.Dialog dialog;
    public boolean not_close;
    public android.view.View root;

    public AlertDlg(android.content.Context p5)
    {
        this.dialog = 0;
        this.root = 0;
        this.context = 0;
        this.not_close = 0;
        this.adapter = 0;
        this.context = p5;
        this.dialog = new android.app.Dialog(p5);
        this.dialog.requestWindowFeature(1);
        this.dialog.getWindow().setBackgroundDrawable(new android.graphics.drawable.ColorDrawable(0));
        this.dialog.getWindow().getDecorView().getViewTreeObserver().addOnGlobalLayoutListener(new com.android.vending.billing.InAppBillingService.LUCK.AlertDlg$1(this));
        this.dialog.setContentView(2130968580);
        this.dialog.setCancelable(1);
        return;
    }

    public AlertDlg(android.content.Context p3, int p4)
    {
        this.dialog = 0;
        this.root = 0;
        this.context = 0;
        this.not_close = 0;
        this.adapter = 0;
        this.context = p3;
        this.dialog = new android.app.Dialog(p3, p4);
        this.dialog.setContentView(2130968580);
        this.dialog.setCancelable(1);
        return;
    }

    public AlertDlg(android.content.Context p3, boolean p4)
    {
        this.dialog = 0;
        this.root = 0;
        this.context = 0;
        this.not_close = 0;
        this.adapter = 0;
        this.context = p3;
        this.dialog = new com.android.vending.billing.InAppBillingService.LUCK.AlertDlg$2(this, p3, 2131230725);
        this.dialog.setContentView(2130968580);
        this.dialog.setCancelable(1);
        return;
    }

    public android.app.Dialog create()
    {
        return this.dialog;
    }

    public com.android.vending.billing.InAppBillingService.LUCK.AlertDlg setAdapter(android.widget.ArrayAdapter p7, android.widget.AdapterView$OnItemClickListener p8)
    {
        this.adapter = p7;
        android.widget.ListView v2_1 = ((android.widget.ListView) this.dialog.findViewById(2131558439));
        this.dialog.findViewById(2131558440);
        v2_1.setVisibility(0);
        v2_1.setAdapter(p7);
        v2_1.setOnItemClickListener(new com.android.vending.billing.InAppBillingService.LUCK.AlertDlg$9(this, p8, p7));
        int[] v1 = new int[3];
        v1 = {868199252, 868199252, 868199252};
        v2_1.setDivider(new android.graphics.drawable.GradientDrawable(android.graphics.drawable.GradientDrawable$Orientation.RIGHT_LEFT, v1));
        v2_1.setDividerHeight(3);
        return this;
    }

    public void setAdapterNotClose(boolean p1)
    {
        this.not_close = p1;
        return;
    }

    public com.android.vending.billing.InAppBillingService.LUCK.AlertDlg setCancelable(boolean p2)
    {
        this.dialog.setCancelable(p2);
        return this;
    }

    public com.android.vending.billing.InAppBillingService.LUCK.AlertDlg setIcon(int p7)
    {
        ((android.widget.LinearLayout) this.dialog.findViewById(2131558432)).setVisibility(0);
        android.widget.ImageView v0_1 = ((android.widget.ImageView) this.dialog.findViewById(2131558433));
        v0_1.setVisibility(0);
        v0_1.setImageDrawable(this.context.getResources().getDrawable(p7));
        ((android.widget.TextView) this.dialog.findViewById(2131558435)).setVisibility(0);
        return this;
    }

    public com.android.vending.billing.InAppBillingService.LUCK.AlertDlg setMessage(android.text.SpannableString p5)
    {
        android.widget.TextView v1_1 = ((android.widget.TextView) this.dialog.findViewById(2131558438));
        ((android.widget.ScrollView) this.dialog.findViewById(2131558437)).setVisibility(0);
        v1_1.setMovementMethod(new android.text.method.ScrollingMovementMethod());
        v1_1.setText(p5);
        return this;
    }

    public com.android.vending.billing.InAppBillingService.LUCK.AlertDlg setMessage(String p5)
    {
        android.widget.TextView v1_1 = ((android.widget.TextView) this.dialog.findViewById(2131558438));
        ((android.widget.ScrollView) this.dialog.findViewById(2131558437)).setVisibility(0);
        v1_1.setMovementMethod(new android.text.method.ScrollingMovementMethod());
        v1_1.setText(p5);
        return this;
    }

    public com.android.vending.billing.InAppBillingService.LUCK.AlertDlg setNegativeButton(int p4, android.content.DialogInterface$OnClickListener p5)
    {
        android.widget.Button v0_1 = ((android.widget.Button) this.dialog.findViewById(2131558408));
        v0_1.setVisibility(0);
        v0_1.setText(p4);
        v0_1.setOnClickListener(new com.android.vending.billing.InAppBillingService.LUCK.AlertDlg$8(this, p5));
        return this;
    }

    public com.android.vending.billing.InAppBillingService.LUCK.AlertDlg setNegativeButton(String p4, android.content.DialogInterface$OnClickListener p5)
    {
        android.widget.Button v0_1 = ((android.widget.Button) this.dialog.findViewById(2131558408));
        v0_1.setVisibility(0);
        v0_1.setText(p4);
        v0_1.setOnClickListener(new com.android.vending.billing.InAppBillingService.LUCK.AlertDlg$5(this, p5));
        return this;
    }

    public com.android.vending.billing.InAppBillingService.LUCK.AlertDlg setNeutralButton(int p4, android.content.DialogInterface$OnClickListener p5)
    {
        android.widget.Button v0_1 = ((android.widget.Button) this.dialog.findViewById(2131558441));
        v0_1.setVisibility(0);
        v0_1.setText(p4);
        v0_1.setOnClickListener(new com.android.vending.billing.InAppBillingService.LUCK.AlertDlg$7(this, p5));
        return this;
    }

    public com.android.vending.billing.InAppBillingService.LUCK.AlertDlg setNeutralButton(String p4, android.content.DialogInterface$OnClickListener p5)
    {
        android.widget.Button v0_1 = ((android.widget.Button) this.dialog.findViewById(2131558441));
        v0_1.setVisibility(0);
        v0_1.setText(p4);
        v0_1.setOnClickListener(new com.android.vending.billing.InAppBillingService.LUCK.AlertDlg$4(this, p5));
        return this;
    }

    public com.android.vending.billing.InAppBillingService.LUCK.AlertDlg setOnCancelListener(android.content.DialogInterface$OnCancelListener p2)
    {
        this.dialog.setOnCancelListener(p2);
        return this;
    }

    public com.android.vending.billing.InAppBillingService.LUCK.AlertDlg setPositiveButton(int p4, android.content.DialogInterface$OnClickListener p5)
    {
        android.widget.Button v0_1 = ((android.widget.Button) this.dialog.findViewById(2131558407));
        v0_1.setVisibility(0);
        v0_1.setText(p4);
        v0_1.setOnClickListener(new com.android.vending.billing.InAppBillingService.LUCK.AlertDlg$6(this, p5));
        return this;
    }

    public com.android.vending.billing.InAppBillingService.LUCK.AlertDlg setPositiveButton(String p4, android.content.DialogInterface$OnClickListener p5)
    {
        android.widget.Button v0_1 = ((android.widget.Button) this.dialog.findViewById(2131558407));
        v0_1.setVisibility(0);
        v0_1.setText(p4);
        v0_1.setOnClickListener(new com.android.vending.billing.InAppBillingService.LUCK.AlertDlg$3(this, p5));
        return this;
    }

    public com.android.vending.billing.InAppBillingService.LUCK.AlertDlg setTitle(int p5)
    {
        ((android.widget.LinearLayout) this.dialog.findViewById(2131558432)).setVisibility(0);
        ((android.widget.TextView) this.dialog.findViewById(2131558434)).setText(p5);
        return this;
    }

    public com.android.vending.billing.InAppBillingService.LUCK.AlertDlg setTitle(String p7)
    {
        ((android.widget.LinearLayout) this.dialog.findViewById(2131558432)).setVisibility(0);
        ((android.widget.TextView) this.dialog.findViewById(2131558434)).setText(p7);
        ((android.widget.TextView) this.dialog.findViewById(2131558435)).setVisibility(0);
        return this;
    }

    public com.android.vending.billing.InAppBillingService.LUCK.AlertDlg setView(android.view.View p5)
    {
        android.widget.LinearLayout v1_1 = ((android.widget.LinearLayout) this.dialog.findViewById(2131558436));
        this.dialog.findViewById(2131558440);
        v1_1.addView(p5);
        return this;
    }
}
