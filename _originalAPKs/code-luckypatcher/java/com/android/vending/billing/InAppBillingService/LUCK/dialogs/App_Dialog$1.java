package com.android.vending.billing.InAppBillingService.LUCK.dialogs;
 class App_Dialog$1 implements java.lang.Runnable {
    final synthetic com.android.vending.billing.InAppBillingService.LUCK.dialogs.App_Dialog this$0;
    final synthetic android.widget.ProgressBar val$bar;
    final synthetic android.text.SpannableStringBuilder val$builder;
    final synthetic android.text.SpannableStringBuilder val$builder2;
    final synthetic android.text.SpannableStringBuilder val$builder3;
    final synthetic android.widget.TextView val$txt;
    final synthetic android.widget.TextView val$txt2;
    final synthetic android.widget.TextView val$txt3;
    final synthetic android.widget.TextView val$txt4;

    App_Dialog$1(com.android.vending.billing.InAppBillingService.LUCK.dialogs.App_Dialog p1, android.widget.TextView p2, android.widget.TextView p3, android.text.SpannableStringBuilder p4, android.text.SpannableStringBuilder p5, android.widget.TextView p6, android.text.SpannableStringBuilder p7, android.widget.TextView p8, android.widget.ProgressBar p9)
    {
        this.this$0 = p1;
        this.val$txt = p2;
        this.val$txt2 = p3;
        this.val$builder = p4;
        this.val$builder3 = p5;
        this.val$txt4 = p6;
        this.val$builder2 = p7;
        this.val$txt3 = p8;
        this.val$bar = p9;
        return;
    }

    public void run()
    {
        try {
            android.text.SpannableString v16_1 = new com.android.vending.billing.InAppBillingService.LUCK.dialogs.App_Dialog$1$1;
            v16_1(this, com.chelpus.Utils.getColoredText(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli.name, "#be6e17", "bold"));
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.handler.post(v16_1);
        } catch (android.content.pm.PackageManager$NameNotFoundException v3_1) {
            v3_1.printStackTrace();
            return;
        }
        if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli.enable) {
            this.val$builder.append(com.chelpus.Utils.getColoredText(new StringBuilder().append(com.chelpus.Utils.getText(2131165421)).append("\n").toString(), "", ""));
        } else {
            this.val$builder.append(com.chelpus.Utils.getColoredText(new StringBuilder().append(com.chelpus.Utils.getText(2131165426)).append("\n").toString(), "", ""));
        }
        if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli.custom) {
            this.val$builder.append(com.chelpus.Utils.getColoredText(new StringBuilder().append(com.chelpus.Utils.getText(2131165686)).append("\n").toString(), -990142, ""));
        }
        if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli.lvl) {
            this.val$builder.append(com.chelpus.Utils.getColoredText(new StringBuilder().append(com.chelpus.Utils.getText(2131165689)).append("\n").toString(), -16711821, ""));
        }
        if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli.ads) {
            this.val$builder.append(com.chelpus.Utils.getColoredText(new StringBuilder().append(com.chelpus.Utils.getText(2131165682)).append("\n").toString(), -990142, ""));
        }
        if ((!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli.ads) && ((!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli.lvl) && (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli.custom))) {
            this.val$builder.append(com.chelpus.Utils.getColoredText(new StringBuilder().append(com.chelpus.Utils.getText(2131165695)).append("\n").toString(), -65451, ""));
        }
        if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli.modified) {
            this.val$builder.append(com.chelpus.Utils.getColoredText(new StringBuilder().append(com.chelpus.Utils.getText(2131165693)).append("\n").toString(), "", ""));
        } else {
            this.val$builder.append(com.chelpus.Utils.getColoredText(new StringBuilder().append(com.chelpus.Utils.getText(2131165690)).append("\n").toString(), -16711821, ""));
        }
        if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli.odex) {
            this.val$builder.append(com.chelpus.Utils.getColoredText(new StringBuilder().append(com.chelpus.Utils.getText(2131165691)).append("\n").toString(), "", ""));
        } else {
            this.val$builder.append(com.chelpus.Utils.getColoredText(new StringBuilder().append(com.chelpus.Utils.getText(2131165688)).append("\n").toString(), -990142, ""));
        }
        if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli.system) {
            this.val$builder.append(com.chelpus.Utils.getColoredText(com.chelpus.Utils.getText(2131165694), -16711821, ""));
        } else {
            this.val$builder.append(com.chelpus.Utils.getColoredText(com.chelpus.Utils.getText(2131165696), -162281, ""));
        }
        android.text.SpannableString v16_56 = new com.android.vending.billing.InAppBillingService.LUCK.dialogs.App_Dialog$1$2;
        v16_56(this);
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.handler.post(v16_56);
        android.content.pm.PackageManager v8 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng();
        this.val$builder3.clear();
        try {
            String[] v9 = v8.getPackageInfo(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli.pkgName, 4096).requestedPermissions;
        } catch (android.content.pm.PackageManager$NameNotFoundException v3_0) {
            v3_0.printStackTrace();
            android.text.SpannableString v16_67 = new com.android.vending.billing.InAppBillingService.LUCK.dialogs.App_Dialog$1$3;
            v16_67(this);
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.handler.post(v16_67);
            this.val$builder2.clear();
            this.val$builder2.append(com.chelpus.Utils.getColoredText("Package name:\n", "#6699cc", "bold"));
            this.val$builder2.append(com.chelpus.Utils.getColoredText(new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli.pkgName).append("\n").toString(), "", ""));
            this.val$builder2.append(com.chelpus.Utils.getColoredText(new StringBuilder().append(com.chelpus.Utils.getText(2131165681)).append("\n").toString(), "#6699cc", "bold"));
            this.val$builder2.append(com.chelpus.Utils.getColoredText(new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getLaunchIntentForPackage(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli.pkgName).getComponent().getClassName()).append("\n").toString(), "", ""));
            this.val$builder2.append(com.chelpus.Utils.getColoredText(new StringBuilder().append(com.chelpus.Utils.getText(2131165683)).append("\n").toString(), "#6699cc", "bold"));
            try {
                this.val$builder2.append(com.chelpus.Utils.getColoredText(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getPackageInfo(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli.pkgName, 0).applicationInfo.sourceDir, "", ""));
                this.val$builder2.append(com.chelpus.Utils.getColoredText(new StringBuilder().append("\n").append(com.chelpus.Utils.getText(2131165687)).append("\n").toString(), "#6699cc", "bold"));
                this.val$builder2.append(com.chelpus.Utils.getColoredText(new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getPackageInfo(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli.pkgName, 0).applicationInfo.dataDir).append("/").toString(), "", ""));
                this.val$builder2.append(com.chelpus.Utils.getColoredText(new StringBuilder().append("\n").append(com.chelpus.Utils.getText(2131165698)).append(" ").toString(), "#6699cc", "bold"));
                this.val$builder2.append(com.chelpus.Utils.getColoredText(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getPackageInfo(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli.pkgName, 0).versionName, "", ""));
                this.val$builder2.append(com.chelpus.Utils.getColoredText(new StringBuilder().append("\n").append(com.chelpus.Utils.getText(2131165685)).append(" ").toString(), "#6699cc", "bold"));
                this.val$builder2.append(com.chelpus.Utils.getColoredText(new StringBuilder().append("").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getPackageInfo(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli.pkgName, 0).versionCode).toString(), "", ""));
                this.val$builder2.append(com.chelpus.Utils.getColoredText(new StringBuilder().append("\n").append(com.chelpus.Utils.getText(2131165697)).append(" ").toString(), "#6699cc", "bold"));
                this.val$builder2.append(com.chelpus.Utils.getColoredText(new StringBuilder().append("").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getPackageInfo(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli.pkgName, 0).applicationInfo.uid).toString(), "", ""));
                this.val$builder2.append(com.chelpus.Utils.getColoredText(new StringBuilder().append("\n").append(com.chelpus.Utils.getText(2131165500)).append(" ").toString(), "#6699cc", "bold"));
                String v14_30 = new StringBuilder().append("").append(new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm").format(new java.util.Date((((long) com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli.updatetime) * 1000)))).toString();
                System.out.println(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli.updatetime);
                System.out.println((((long) com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli.updatetime) * 1000));
                this.val$builder2.append(com.chelpus.Utils.getColoredText(v14_30, "", ""));
                this.val$builder2.append(com.chelpus.Utils.getColoredText(new StringBuilder().append("\n\n").append(com.chelpus.Utils.getText(2131165202)).append(" ").toString(), "#6699cc", "bold"));
                android.text.SpannableStringBuilder v15_193 = new StringBuilder();
                String v0_103 = new Object[1];
                String v17_45 = v0_103;
                v17_45[0] = Float.valueOf((((float) new java.io.File(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getPackageInfo(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli.pkgName, 0).applicationInfo.sourceDir).length()) / 1233125376));
                this.val$builder2.append(com.chelpus.Utils.getColoredText(v15_193.append(String.format("%.3f", v17_45)).append(" Mb").toString(), "", ""));
            } catch (android.content.pm.PackageManager$NameNotFoundException v4) {
                v4.printStackTrace();
            }
            try {
                this.val$builder2.append(com.chelpus.Utils.getColoredText(new StringBuilder().append("\n").append(com.chelpus.Utils.getText(2131165402)).append(" ").toString(), "#6699cc", "bold"));
                try {
                    String v12_0 = com.chelpus.Utils.getFileDalvikCache(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getPackageInfo(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli.pkgName, 0).applicationInfo.sourceDir).length();
                } catch (android.text.SpannableStringBuilder v15) {
                }
                android.text.SpannableStringBuilder v15_209 = new StringBuilder();
                String v0_119 = new Object[1];
                String v17_50 = v0_119;
                v17_50[0] = Float.valueOf((((float) v12_0) / 1233125376));
                this.val$builder2.append(com.chelpus.Utils.getColoredText(v15_209.append(String.format("%.3f", v17_50)).append(" Mb").toString(), "", ""));
                if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.su) {
                    try {
                        this.val$builder2.append(com.chelpus.Utils.getColoredText(new StringBuilder().append("\n").append(com.chelpus.Utils.getText(2131165403)).append(" ").toString(), "#6699cc", "bold"));
                    } catch (android.text.SpannableStringBuilder v15) {
                    }
                    try {
                        android.text.SpannableStringBuilder v15_221 = new com.chelpus.Utils("");
                        String v0_126 = new String[1];
                        android.text.SpannableString v16_194 = v0_126;
                        v16_194[0] = new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.dalvikruncommand).append(".checkDataSize ").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getPackageInfo(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli.pkgName, 0).applicationInfo.dataDir).toString();
                        String v12_2 = v15_221.cmdRoot(v16_194).replace("SU Java-Code Running!\n", "");
                    } catch (android.text.SpannableStringBuilder v15) {
                    }
                    this.val$builder2.append(com.chelpus.Utils.getColoredText(new StringBuilder().append(v12_2.replace("\n", "").replace("\r", "")).append(" Mb").toString(), "", ""));
                }
                android.text.SpannableString v16_202 = new com.android.vending.billing.InAppBillingService.LUCK.dialogs.App_Dialog$1$4;
                v16_202(this);
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.handler.post(v16_202);
                return;
            } catch (android.text.SpannableStringBuilder v15) {
            }
        }
        if ((v9 == null) || (v9.length <= 0)) {
        } else {
            int v6 = 0;
            while (v6 < v9.length) {
                this.val$builder3.append(com.chelpus.Utils.getColoredText(new StringBuilder().append(v9[v6]).append("\n").toString(), "#6699cc", "bold"));
                try {
                    int v10 = v8.getPermissionInfo(v9[v6], 0).loadDescription(v8).toString();
                } catch (android.text.SpannableStringBuilder v15) {
                } catch (android.content.pm.PackageManager$NameNotFoundException v3) {
                    v10 = 0;
                }
                String v14_14;
                if (v10 != 0) {
                    v14_14 = new StringBuilder().append(v10).append("\n\n").toString();
                } else {
                    v14_14 = new StringBuilder().append(com.chelpus.Utils.getText(2131165625)).append("\n\n").toString();
                }
                this.val$builder3.append(com.chelpus.Utils.getColoredText(v14_14, "", ""));
                v6++;
            }
        }
    }
}
