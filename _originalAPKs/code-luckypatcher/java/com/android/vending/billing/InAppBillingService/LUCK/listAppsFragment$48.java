package com.android.vending.billing.InAppBillingService.LUCK;
 class listAppsFragment$48 implements android.content.DialogInterface$OnClickListener {
    final synthetic com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment this$0;
    final synthetic android.widget.EditText val$ed;

    listAppsFragment$48(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment p1, android.widget.EditText p2)
    {
        this.this$0 = p1;
        this.val$ed = p2;
        return;
    }

    public void onClick(android.content.DialogInterface p12, int p13)
    {
        android.text.Editable v4 = this.val$ed.getText();
        if ((!v4.toString().contains("/")) || (v4.toString().equals("/"))) {
            this.this$0.showMessage(com.chelpus.Utils.getText(2131165748), com.chelpus.Utils.getText(2131165444));
        } else {
            String[] v6 = v4.toString().trim().replaceAll("\\s+", ".").split("\\/+");
            String v2 = "";
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$48$1 v8_6 = v6.length;
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment v7_9 = 0;
            while (v7_9 < v8_6) {
                String v0 = v6[v7_9];
                if (!v0.equals("")) {
                    v2 = new StringBuilder().append(v2).append("/").append(v0).toString();
                }
                v7_9++;
            }
            String v1 = v2;
            if (new java.io.File(v1).exists()) {
                this.this$0.showMessage(com.chelpus.Utils.getText(2131165748), com.chelpus.Utils.getText(2131165445));
            } else {
                if ((this.this$0.testPath(1, v1)) && (!v1.startsWith(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance().getDir("sdcard", 0).getAbsolutePath()))) {
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().edit().putString("path", v1).commit();
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().edit().putBoolean("manual_path", 1).commit();
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().edit().putBoolean("path_changed", 1).commit();
                    java.io.File v5_1 = new java.io.File(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().getString("basepath", "Noting"));
                    java.io.File v3_1 = new java.io.File(v1);
                    if (v5_1.exists()) {
                        this.this$0.runWithWait(new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$48$1(this, v5_1, v3_1, v1));
                    } else {
                        System.out.println("Directory does not exist.");
                    }
                }
            }
        }
        return;
    }
}
