package com.android.vending.billing.InAppBillingService.LUCK.widgets;
 class AppDisablerWidget$2 implements java.lang.Runnable {
    final synthetic com.android.vending.billing.InAppBillingService.LUCK.widgets.AppDisablerWidget this$0;
    final synthetic android.content.Context val$context;
    final synthetic android.os.Handler val$handler;
    final synthetic android.content.Intent val$intent;

    AppDisablerWidget$2(com.android.vending.billing.InAppBillingService.LUCK.widgets.AppDisablerWidget p1, android.content.Context p2, android.content.Intent p3, android.os.Handler p4)
    {
        this.this$0 = p1;
        this.val$context = p2;
        this.val$intent = p3;
        this.val$handler = p4;
        return;
    }

    public void run()
    {
        try {
            android.content.pm.PackageManager v4 = this.val$context.getPackageManager();
            int v2 = this.val$intent.getIntExtra("appWidgetId", -1);
        } catch (Exception v0) {
            v0.printStackTrace();
            android.widget.RemoteViews v7_1 = new android.widget.RemoteViews(this.val$context.getPackageName(), 2130968582);
            v7_1.setBoolean(2131558443, "setEnabled", 0);
            v7_1.setInt(2131558443, "setBackgroundResource", 2130837584);
            v7_1.setTextColor(2131558442, android.graphics.Color.parseColor("#AAAAAA"));
            v7_1.setTextViewText(2131558442, com.chelpus.Utils.getText(2131165756));
            this.val$handler.post(new com.android.vending.billing.InAppBillingService.LUCK.widgets.AppDisablerWidget$2$3(this));
            return;
        }
        if ((v2 == -1) || (com.android.vending.billing.InAppBillingService.LUCK.widgets.AppDisablerWidgetConfigureActivity.loadTitlePref(this.val$context, v2).equals("NOT_SAVED_APP_DISABLER"))) {
            return;
        } else {
            String v3 = com.android.vending.billing.InAppBillingService.LUCK.widgets.AppDisablerWidgetConfigureActivity.loadTitlePref(this.val$context, v2);
            android.widget.RemoteViews v5_1 = new android.widget.RemoteViews(this.val$context.getPackageName(), 2130968582);
            if (!v4.getPackageInfo(v3, 0).applicationInfo.enabled) {
                android.os.Handler v8_14 = new com.chelpus.Utils("");
                com.android.vending.billing.InAppBillingService.LUCK.widgets.AppDisablerWidget$2$2 v9_5 = new String[1];
                v9_5[0] = new StringBuilder().append("pm enable ").append(v3).toString();
                v8_14.cmdRoot(v9_5);
                v5_1.setTextColor(2131558442, android.graphics.Color.parseColor("#00FF00"));
                v5_1.setInt(2131558443, "setBackgroundResource", 2130837585);
                this.val$handler.post(new com.android.vending.billing.InAppBillingService.LUCK.widgets.AppDisablerWidget$2$2(this, v3));
                return;
            } else {
                android.os.Handler v8_19 = new com.chelpus.Utils("");
                com.android.vending.billing.InAppBillingService.LUCK.widgets.AppDisablerWidget$2$2 v9_13 = new String[1];
                v9_13[0] = new StringBuilder().append("pm disable ").append(v3).toString();
                v8_19.cmdRoot(v9_13);
                v5_1.setInt(2131558443, "setBackgroundResource", 2130837584);
                v5_1.setTextColor(2131558442, android.graphics.Color.parseColor("#FF0000"));
                this.val$handler.post(new com.android.vending.billing.InAppBillingService.LUCK.widgets.AppDisablerWidget$2$1(this, v3));
                return;
            }
        }
    }
}
