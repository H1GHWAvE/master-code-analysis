package com.android.vending.billing.InAppBillingService.LUCK;
 class listAppsFragment$121 extends android.widget.ArrayAdapter {
    final synthetic com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment this$0;

    listAppsFragment$121(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment p1, android.content.Context p2, int p3, java.util.List p4)
    {
        this.this$0 = p1;
        this(p2, p3, p4);
        return;
    }

    public android.view.View getView(int p12, android.view.View p13, android.view.ViewGroup p14)
    {
        android.view.View v6 = ((android.view.LayoutInflater) com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance().getSystemService("layout_inflater")).inflate(2130968605, p14, 0);
        android.widget.TextView v5_1 = ((android.widget.TextView) v6.findViewById(2131558457));
        v5_1.setTextAppearance(this.getContext(), com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getSizeText());
        v5_1.setCompoundDrawablePadding(((int) ((1084227584 * com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getRes().getDisplayMetrics().density) + 1056964608)));
        if (!((com.android.vending.billing.InAppBillingService.LUCK.Components) this.getItem(p12)).header) {
            if (((com.android.vending.billing.InAppBillingService.LUCK.Components) this.getItem(p12)).activity) {
                String v4_0 = new StringBuilder().append(((com.android.vending.billing.InAppBillingService.LUCK.Components) this.getItem(p12)).Name).append("\n").toString();
                if (!((com.android.vending.billing.InAppBillingService.LUCK.Components) this.getItem(p12)).Status) {
                    v5_1.setText(com.chelpus.Utils.getColoredText(v4_0, "#ffff0000", "bold"));
                } else {
                    v5_1.setText(com.chelpus.Utils.getColoredText(v4_0, "#ff00ffff", "bold"));
                    if (com.chelpus.Utils.isAds(((com.android.vending.billing.InAppBillingService.LUCK.Components) this.getItem(p12)).Name)) {
                        v5_1.setText(com.chelpus.Utils.getColoredText(v4_0, "#ffffff00", "bold"));
                    }
                }
                String v4_1;
                if (!com.chelpus.Utils.isAds(((com.android.vending.billing.InAppBillingService.LUCK.Components) this.getItem(p12)).Name)) {
                    v4_1 = com.chelpus.Utils.getText(2131165193);
                } else {
                    v4_1 = com.chelpus.Utils.getText(2131165192);
                }
                v5_1.append(com.chelpus.Utils.getColoredText(v4_1, "#ff888888", "italic"));
            }
            if (((com.android.vending.billing.InAppBillingService.LUCK.Components) this.getItem(p12)).permission) {
                String v4_2 = new StringBuilder().append(((com.android.vending.billing.InAppBillingService.LUCK.Components) this.getItem(p12)).Name).append("\n").toString();
                if (!((com.android.vending.billing.InAppBillingService.LUCK.Components) this.getItem(p12)).Status) {
                    v5_1.setText(com.chelpus.Utils.getColoredText(v4_2, "#ffff0000", "bold"));
                } else {
                    v5_1.setText(com.chelpus.Utils.getColoredText(v4_2, "#ff00ff00", "bold"));
                }
                android.content.pm.PackageManager v3 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng();
                try {
                    String v4_3 = v3.getPermissionInfo(((com.android.vending.billing.InAppBillingService.LUCK.Components) this.getItem(p12)).Name.replace("chelpa_per_", "").replace("chelpus_", ""), 0).loadDescription(v3).toString();
                } catch (NullPointerException v1) {
                    v5_1.append(com.chelpus.Utils.getColoredText(com.chelpus.Utils.getText(2131165625), "#ff888888", "italic"));
                } catch (NullPointerException v1) {
                    v5_1.append(com.chelpus.Utils.getColoredText(com.chelpus.Utils.getText(2131165625), "#ff888888", "italic"));
                }
                if (v4_3 != null) {
                    v5_1.append(com.chelpus.Utils.getColoredText(v4_3, "#ff888888", "italic"));
                } else {
                    v5_1.append(com.chelpus.Utils.getColoredText(com.chelpus.Utils.getText(2131165625), "#ff888888", "italic"));
                }
            }
            if (((com.android.vending.billing.InAppBillingService.LUCK.Components) this.getItem(p12)).service) {
                String v4_7 = new StringBuilder().append(((com.android.vending.billing.InAppBillingService.LUCK.Components) this.getItem(p12)).Name).append("\n").toString();
                if (!((com.android.vending.billing.InAppBillingService.LUCK.Components) this.getItem(p12)).Status) {
                    v5_1.setText(com.chelpus.Utils.getColoredText(v4_7, "#ffff0000", "bold"));
                } else {
                    v5_1.setText(com.chelpus.Utils.getColoredText(v4_7, "#ff00ff00", "bold"));
                }
            }
            if (((com.android.vending.billing.InAppBillingService.LUCK.Components) this.getItem(p12)).reciver) {
                String v4_8 = new StringBuilder().append(((com.android.vending.billing.InAppBillingService.LUCK.Components) this.getItem(p12)).Name).append("\n").toString();
                if (!((com.android.vending.billing.InAppBillingService.LUCK.Components) this.getItem(p12)).Status) {
                    v5_1.setText(com.chelpus.Utils.getColoredText(v4_8, "#ffff0000", "bold"));
                } else {
                    v5_1.setText(com.chelpus.Utils.getColoredText(v4_8, "#ff00ffff", "bold"));
                }
            }
            if (((com.android.vending.billing.InAppBillingService.LUCK.Components) this.getItem(p12)).provider) {
                String v4_9 = new StringBuilder().append(((com.android.vending.billing.InAppBillingService.LUCK.Components) this.getItem(p12)).Name).append("\n").toString();
                if (!((com.android.vending.billing.InAppBillingService.LUCK.Components) this.getItem(p12)).Status) {
                    v5_1.setText(com.chelpus.Utils.getColoredText(v4_9, "#ffff0000", "bold"));
                } else {
                    v5_1.setText(com.chelpus.Utils.getColoredText(v4_9, "#ff00ff00", "bold"));
                }
            }
        } else {
            v5_1.setText(com.chelpus.Utils.getColoredText(((com.android.vending.billing.InAppBillingService.LUCK.Components) this.getItem(p12)).Name, "#ffffffff", "bold"));
            v6.setBackgroundColor(-12303292);
        }
        return v6;
    }
}
