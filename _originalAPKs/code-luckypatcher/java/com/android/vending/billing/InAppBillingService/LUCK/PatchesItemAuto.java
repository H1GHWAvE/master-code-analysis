package com.android.vending.billing.InAppBillingService.LUCK;
public class PatchesItemAuto {
    public String marker;
    public boolean markerTrig;
    public byte[] origByte;
    public int[] origMask;
    public boolean pattern;
    public byte[] repByte;
    public int[] repMask;
    public boolean result;
    public String resultText;

    public PatchesItemAuto(byte[] p3, int[] p4, byte[] p5, int[] p6, boolean p7, String p8, String p9)
    {
        this.result = 0;
        this.pattern = 0;
        this.resultText = "";
        this.marker = "";
        this.markerTrig = 0;
        int[] v0_3 = new byte[p3.length];
        this.origByte = v0_3;
        this.origByte = p3;
        int[] v0_5 = new int[p4.length];
        this.origMask = v0_5;
        this.origMask = p4;
        int[] v0_7 = new byte[p5.length];
        this.repByte = v0_7;
        this.repByte = p5;
        int[] v0_9 = new int[p6.length];
        this.repMask = v0_9;
        this.repMask = p6;
        this.pattern = p7;
        this.resultText = p8;
        this.marker = p9;
        return;
    }
}
