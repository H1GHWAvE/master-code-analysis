package com.android.vending.billing.InAppBillingService.LUCK.widgets;
public class AndroidPatchWidget extends android.appwidget.AppWidgetProvider {

    public AndroidPatchWidget()
    {
        return;
    }

    static void updateAppWidget(android.content.Context p16, android.appwidget.AppWidgetManager p17, int p18)
    {
        p16.getString(2131165790);
        android.widget.RemoteViews v11_1 = new android.widget.RemoteViews(p16.getPackageName(), 2130968581);
        int v3 = 0;
        int v7 = 0;
        int v8 = 0;
        int v9 = 0;
        if (com.chelpus.Utils.checkCoreJarPatch11()) {
            v3 = (0 + 1);
            v7 = 1;
        }
        if (com.chelpus.Utils.checkCoreJarPatch12()) {
            v3++;
            v7 = 1;
        }
        String v10_0;
        if (v3 <= 0) {
            v10_0 = new StringBuilder().append(com.chelpus.Utils.getText(2131165300)).append("\n(not patched)").toString();
        } else {
            v10_0 = new StringBuilder().append(com.chelpus.Utils.getText(2131165300)).append("\n(").append(v3).append("/2 patched)").toString();
        }
        String v10_1;
        if (!com.chelpus.Utils.checkCoreJarPatch20()) {
            v10_1 = new StringBuilder().append(v10_0).append("\n").append(com.chelpus.Utils.getText(2131165302)).append("\n(not patched)").toString();
        } else {
            v10_1 = new StringBuilder().append(v10_0).append("\n").append(com.chelpus.Utils.getText(2131165302)).append("\n(patched)").toString();
            v8 = 1;
        }
        String v10_2;
        if (!com.chelpus.Utils.checkCoreJarPatch30(p16.getPackageManager())) {
            v10_2 = new StringBuilder().append(v10_1).append("\n").append(com.chelpus.Utils.getText(2131165304)).append("\n(not patched)").toString();
        } else {
            v10_2 = new StringBuilder().append(v10_1).append("\n").append(com.chelpus.Utils.getText(2131165304)).append("\n(patched)").toString();
            v9 = 1;
        }
        int v4;
        android.text.SpannableString v2_1 = new android.text.SpannableString(v10_2);
        if (v7 == 0) {
            v4 = (com.chelpus.Utils.getText(2131165300).length() + " (not patched)".length());
            v2_1.setSpan(new android.text.style.ForegroundColorSpan(-65536), com.chelpus.Utils.getText(2131165300).length(), v4, 0);
        } else {
            v4 = (com.chelpus.Utils.getText(2131165300).length() + new StringBuilder().append(" (").append(v3).append("/2 patched)").toString().length());
            v2_1.setSpan(new android.text.style.ForegroundColorSpan(-16711936), com.chelpus.Utils.getText(2131165300).length(), v4, 0);
        }
        int v5;
        if (v8 == 0) {
            v5 = (((com.chelpus.Utils.getText(2131165302).length() + 1) + v4) + " (not patched)".length());
            v2_1.setSpan(new android.text.style.ForegroundColorSpan(-65536), ((com.chelpus.Utils.getText(2131165302).length() + 1) + v4), v5, 0);
        } else {
            v5 = (((com.chelpus.Utils.getText(2131165302).length() + v4) + 1) + " (patched)".length());
            v2_1.setSpan(new android.text.style.ForegroundColorSpan(-16711936), ((com.chelpus.Utils.getText(2131165302).length() + v4) + 1), v5, 0);
        }
        if (v9 == 0) {
            int v6_0 = (((com.chelpus.Utils.getText(2131165304).length() + v5) + 1) + " (not patched)".length());
            System.out.println(v4);
            System.out.println(v5);
            System.out.println(v10_2.length());
            v2_1.setSpan(new android.text.style.ForegroundColorSpan(-65536), ((com.chelpus.Utils.getText(2131165304).length() + v5) + 1), v6_0, 0);
        } else {
            v2_1.setSpan(new android.text.style.ForegroundColorSpan(-16711936), ((com.chelpus.Utils.getText(2131165304).length() + v5) + 1), (((com.chelpus.Utils.getText(2131165304).length() + v5) + 1) + " (patched)".length()), 0);
        }
        v11_1.setTextViewText(2131558442, v2_1);
        p17.updateAppWidget(p18, v11_1);
        return;
    }

    public void onDisabled(android.content.Context p1)
    {
        return;
    }

    public void onEnabled(android.content.Context p1)
    {
        return;
    }

    public void onUpdate(android.content.Context p6, android.appwidget.AppWidgetManager p7, int[] p8)
    {
        new android.widget.RemoteViews(p6.getPackageName(), 2130968581);
        int v4_1 = p8.length;
        int v3_1 = 0;
        while (v3_1 < v4_1) {
            com.android.vending.billing.InAppBillingService.LUCK.widgets.AndroidPatchWidget.updateAppWidget(p6, p7, p8[v3_1]);
            v3_1++;
        }
        return;
    }
}
