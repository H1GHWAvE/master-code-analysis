package com.android.vending.billing.InAppBillingService.LUCK.widgets;
final class AppDisablerWidget$1 implements java.lang.Runnable {
    final synthetic int val$appWidgetId;
    final synthetic android.appwidget.AppWidgetManager val$appWidgetManager;
    final synthetic android.content.Context val$context;

    AppDisablerWidget$1(android.content.Context p1, int p2, android.appwidget.AppWidgetManager p3)
    {
        this.val$context = p1;
        this.val$appWidgetId = p2;
        this.val$appWidgetManager = p3;
        return;
    }

    public void run()
    {
        if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.su) {
            android.widget.RemoteViews v6_1 = new android.widget.RemoteViews(this.val$context.getPackageName(), 2130968582);
            v6_1.setOnClickPendingIntent(2131558444, android.app.PendingIntent.getBroadcast(this.val$context, this.val$appWidgetId, new android.content.Intent(this.val$context, com.android.vending.billing.InAppBillingService.LUCK.widgets.AppDisablerWidget), 0));
            v6_1.setInt(2131558443, "setBackgroundResource", 2130837584);
            v6_1.setTextColor(2131558442, android.graphics.Color.parseColor("#AAAAAA"));
            v6_1.setTextViewText(2131558442, "you need root access");
            try {
                android.appwidget.AppWidgetManager.getInstance(this.val$context).updateAppWidget(this.val$appWidgetId, v6_1);
            } catch (Exception v2_0) {
                v2_0.printStackTrace();
            }
        } else {
            String v4 = com.android.vending.billing.InAppBillingService.LUCK.widgets.AppDisablerWidgetConfigureActivity.loadTitlePref(this.val$context, this.val$appWidgetId);
            try {
                android.content.pm.PackageManager v5 = this.val$context.getPackageManager();
                android.widget.RemoteViews v6_3 = new android.widget.RemoteViews(this.val$context.getPackageName(), 2130968582);
                v6_3.setTextViewText(2131558442, v5.getApplicationLabel(v5.getApplicationInfo(v4, 0)));
            } catch (Exception v2) {
                android.widget.RemoteViews v6_5 = new android.widget.RemoteViews(this.val$context.getPackageName(), 2130968582);
                v6_5.setOnClickPendingIntent(2131558444, android.app.PendingIntent.getBroadcast(this.val$context, this.val$appWidgetId, new android.content.Intent(this.val$context, com.android.vending.billing.InAppBillingService.LUCK.widgets.AppDisablerWidget), 0));
                v6_5.setInt(2131558443, "setBackgroundResource", 2130837584);
                v6_5.setTextColor(2131558442, android.graphics.Color.parseColor("#AAAAAA"));
                v6_5.setTextViewText(2131558442, com.chelpus.Utils.getText(2131165756));
                try {
                    this.val$appWidgetManager.updateAppWidget(this.val$appWidgetId, v6_5);
                } catch (Exception v3) {
                    v3.printStackTrace();
                }
            }
            if (!v5.getPackageInfo(v4, 0).applicationInfo.enabled) {
                v6_3.setTextColor(2131558442, android.graphics.Color.parseColor("#FF0000"));
                v6_3.setInt(2131558443, "setBackgroundResource", 2130837584);
            } else {
                v6_3.setTextColor(2131558442, android.graphics.Color.parseColor("#00FF00"));
                v6_3.setInt(2131558443, "setBackgroundResource", 2130837585);
            }
            android.content.Intent v1_3 = new android.content.Intent(this.val$context, com.android.vending.billing.InAppBillingService.LUCK.widgets.AppDisablerWidget);
            v1_3.setAction(com.android.vending.billing.InAppBillingService.LUCK.widgets.AppDisablerWidget.ACTION_WIDGET_RECEIVER);
            v1_3.putExtra("appWidgetId", this.val$appWidgetId);
            v1_3.putExtra("msg", v4);
            v6_3.setOnClickPendingIntent(2131558444, android.app.PendingIntent.getBroadcast(this.val$context, this.val$appWidgetId, v1_3, 0));
            try {
                this.val$appWidgetManager.updateAppWidget(this.val$appWidgetId, v6_3);
            } catch (Exception v2_1) {
                v2_1.printStackTrace();
            }
        }
        return;
    }
}
