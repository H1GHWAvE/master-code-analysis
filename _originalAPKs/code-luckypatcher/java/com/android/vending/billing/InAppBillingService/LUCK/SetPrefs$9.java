package com.android.vending.billing.InAppBillingService.LUCK;
 class SetPrefs$9 implements android.preference.Preference$OnPreferenceChangeListener {
    final synthetic com.android.vending.billing.InAppBillingService.LUCK.SetPrefs this$0;

    SetPrefs$9(com.android.vending.billing.InAppBillingService.LUCK.SetPrefs p1)
    {
        this.this$0 = p1;
        return;
    }

    public boolean onPreferenceChange(android.preference.Preference p6, Object p7)
    {
        int v1_9;
        android.content.SharedPreferences v0 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance().getSharedPreferences("config", 0);
        if ((p6 != this.this$0.findPreference("lvlapp")) || (!((Boolean) p7).booleanValue())) {
            if ((p6 != this.this$0.findPreference("lvlapp")) || (((Boolean) p7).booleanValue())) {
                v1_9 = 0;
            } else {
                v0.edit().putBoolean("lvlapp", 0).commit();
                p6.getEditor().putBoolean("lvlapp", 0);
                v0.edit().putBoolean("settings_change", 1).commit();
                v0.edit().putBoolean("lang_change", 1).commit();
                v1_9 = 1;
            }
        } else {
            v0.edit().putBoolean("lvlapp", 1).commit();
            p6.getEditor().putBoolean("lvlapp", 1);
            v0.edit().putBoolean("settings_change", 1).commit();
            v0.edit().putBoolean("lang_change", 1).commit();
            v1_9 = 1;
        }
        return v1_9;
    }
}
