package com.android.vending.billing.InAppBillingService.LUCK.dialogs;
public class Menu_Dialog_Static {
    android.app.Dialog dialog;

    public Menu_Dialog_Static()
    {
        this.dialog = 0;
        return;
    }

    public void dismiss()
    {
        if (this.dialog != null) {
            this.dialog.dismiss();
            this.dialog = 0;
        }
        return;
    }

    public android.app.Dialog onCreateDialog()
    {
        try {
            System.out.println("Menu Dialog create.");
        } catch (Exception v1) {
            v1.printStackTrace();
            android.app.Dialog v2_11 = 0;
            return v2_11;
        }
        if ((com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag == null) || (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.getContext() == null)) {
            this.dismiss();
        }
        com.android.vending.billing.InAppBillingService.LUCK.AlertDlg v0_1 = new com.android.vending.billing.InAppBillingService.LUCK.AlertDlg(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.getContext());
        v0_1.setPositiveButton(2131165662, new com.android.vending.billing.InAppBillingService.LUCK.dialogs.Menu_Dialog_Static$1(this));
        if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.adapt != null) {
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.adapt.setNotifyOnChange(1);
            v0_1.setAdapterNotClose(1);
            v0_1.setAdapter(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.adapt, new com.android.vending.billing.InAppBillingService.LUCK.dialogs.Menu_Dialog_Static$2(this));
        }
        v0_1.setOnCancelListener(new com.android.vending.billing.InAppBillingService.LUCK.dialogs.Menu_Dialog_Static$3(this));
        v2_11 = v0_1.create();
        return v2_11;
    }

    public void showDialog()
    {
        if (this.dialog == null) {
            this.dialog = this.onCreateDialog();
        }
        if (this.dialog != null) {
            this.dialog.show();
        }
        return;
    }
}
