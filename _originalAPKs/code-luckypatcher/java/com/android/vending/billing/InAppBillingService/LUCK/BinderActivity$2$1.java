package com.android.vending.billing.InAppBillingService.LUCK;
 class BinderActivity$2$1 implements android.view.View$OnClickListener {
    final synthetic com.android.vending.billing.InAppBillingService.LUCK.BinderActivity$2 this$1;
    final synthetic android.widget.EditText val$ed1;
    final synthetic android.widget.EditText val$ed2;

    BinderActivity$2$1(com.android.vending.billing.InAppBillingService.LUCK.BinderActivity$2 p1, android.widget.EditText p2, android.widget.EditText p3)
    {
        this.this$1 = p1;
        this.val$ed1 = p2;
        this.val$ed2 = p3;
        return;
    }

    public void onClick(android.view.View p12)
    {
        String v0_1;
        String v0_0 = this.val$ed1.getText().toString();
        String v5_0 = this.val$ed2.getText().toString();
        if (v0_0.endsWith("/")) {
            v0_1 = v0_0.trim();
        } else {
            v0_1 = new StringBuilder().append(v0_0.trim()).append("/").toString();
        }
        String v5_1;
        if (v5_0.endsWith("/")) {
            v5_1 = v5_0.trim();
        } else {
            v5_1 = new StringBuilder().append(v5_0.trim()).append("/").toString();
        }
        if ((new java.io.File(v0_1.trim()).exists()) && ((!v5_1.equals("/")) && (!v0_1.equals("/")))) {
            if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.su) {
                android.app.Dialog v6_24 = new String[1];
                v6_24[0] = new StringBuilder().append("umount \'").append(v5_1).append("\'").toString();
                com.chelpus.Utils.cmd(v6_24);
                android.app.Dialog v6_25 = new String[1];
                v6_25[0] = new StringBuilder().append("mount -o bind \'").append(v0_1).append("\' \'").append(v5_1).append("\'").toString();
                com.chelpus.Utils.cmd(v6_25);
            } else {
                new Thread(new com.android.vending.billing.InAppBillingService.LUCK.BinderActivity$2$1$1(this, v0_1, v5_1)).start();
            }
            int v1 = 0;
            android.app.Dialog v6_31 = this.this$1.this$0.bindes.iterator();
            while (v6_31.hasNext()) {
                com.android.vending.billing.InAppBillingService.LUCK.BindItem v2_1 = ((com.android.vending.billing.InAppBillingService.LUCK.BindItem) v6_31.next());
                if (v2_1.TargetDir.equals(v5_1)) {
                    v2_1.SourceDir = v0_1;
                    v2_1.TargetDir = v5_1;
                    v1 = 1;
                }
            }
            if (v1 == 0) {
                this.this$1.this$0.bindes.add(new com.android.vending.billing.InAppBillingService.LUCK.BindItem(v0_1, v5_1));
            }
            com.android.vending.billing.InAppBillingService.LUCK.BinderActivity.savetoFile(this.this$1.this$0.bindes, this.this$1.this$0.context);
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.adaptBind.notifyDataSetChanged();
            this.this$1.this$0.lv.setAdapter(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.adaptBind);
            if (this.this$1.this$0.pp4.isShowing()) {
                this.this$1.this$0.pp4.dismiss();
            }
        } else {
            android.widget.Toast.makeText(this.this$1.this$0.getApplication().getApplicationContext(), com.chelpus.Utils.getText(2131165235), 1).show();
        }
        return;
    }
}
