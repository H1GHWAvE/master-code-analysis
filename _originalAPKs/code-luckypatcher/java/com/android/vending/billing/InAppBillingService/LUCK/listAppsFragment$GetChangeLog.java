package com.android.vending.billing.InAppBillingService.LUCK;
 class listAppsFragment$GetChangeLog extends android.os.AsyncTask {
    final synthetic com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment this$0;

    listAppsFragment$GetChangeLog(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment p1)
    {
        this.this$0 = p1;
        return;
    }

    protected varargs Boolean doInBackground(String[] p18)
    {
        System.out.println("LuckyPatcher: download changelog.");
        this.this$0.Text = "New";
        this.this$0.FalseHttp = 0;
        this.this$0.VersionCode = 0;
        this.this$0.ApkName = "LuckyPatcher.apk";
        this.this$0.AppName = "Lucky Patcher";
        this.this$0.Mirror1 = "http://content.wuala.com/contents/chelpus/Luckypatcher/";
        this.this$0.BuildVersionPath = "http://chelpus.defcon5.biz/Version.txt";
        this.this$0.BuildVersionChanges = "http://chelpus.defcon5.biz/Changelogs.txt";
        this.this$0.PackageName = new StringBuilder().append("package:").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.luckyPackage).toString();
        this.this$0.urlpath = new StringBuilder().append("http://chelpus.defcon5.biz/").append(this.this$0.ApkName).toString();
        java.io.File v10_1 = new java.io.File(new java.io.File(new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.basepath).append("/Download/").toString()), this.this$0.ApkName);
        if (v10_1.exists()) {
            v10_1.delete();
        }
        int v9 = 1;
        while (v9 != 0) {
            try {
                java.net.HttpURLConnection v4_3 = ((java.net.HttpURLConnection) new java.net.URL(this.this$0.BuildVersionPath).openConnection());
                v4_3.setRequestMethod("GET");
                v4_3.setConnectTimeout(1000000);
                v4_3.setUseCaches(0);
                v4_3.setRequestProperty("Cache-Control", "no-cache");
                v4_3.connect();
                java.io.InputStream v7_1 = v4_3.getInputStream();
                java.io.ByteArrayOutputStream v2_3 = new java.io.ByteArrayOutputStream();
                byte[] v3_1 = new byte[8192];
            } catch (Exception v5_5) {
                v5_5.printStackTrace();
                v9 = 0;
            } catch (Exception v5) {
                java.io.File v14_47 = this.this$0;
                v14_47.FalseHttp = (v14_47.FalseHttp + 1);
            } catch (Exception v5_3) {
                v5_3.printStackTrace();
            } catch (Exception v5_4) {
                v5_4.printStackTrace();
                java.io.File v14_48 = this.this$0;
                v14_48.FalseHttp = (v14_48.FalseHttp + 1);
            }
            while(true) {
                int v8_1 = v7_1.read(v3_1);
                if (v8_1 == -1) {
                    break;
                }
                v2_3.write(v3_1, 0, v8_1);
            }
            this.this$0.VersionCode = Integer.parseInt(v2_3.toString());
            v2_3.close();
        }
        if ((this.this$0.VersionCode > com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.versionCodeLocal) && ((this.this$0.VersionCode != 999) || (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().getBoolean("999", 0)))) {
            try {
                java.net.HttpURLConnection v4_1 = ((java.net.HttpURLConnection) new java.net.URL(this.this$0.BuildVersionChanges).openConnection());
                v4_1.setRequestMethod("GET");
                v4_1.setConnectTimeout(1000000);
                v4_1.setUseCaches(0);
                v4_1.setRequestProperty("Cache-Control", "no-cache");
                v4_1.connect();
                java.io.InputStream v7_0 = v4_1.getInputStream();
                java.io.ByteArrayOutputStream v2_1 = new java.io.ByteArrayOutputStream();
                byte[] v3_0 = new byte[8192];
            } catch (Exception v5_1) {
                v5_1.printStackTrace();
            } catch (Exception v5_2) {
                v5_2.printStackTrace();
            } catch (java.io.File v14) {
            } catch (Exception v5_0) {
                v5_0.printStackTrace();
            }
            while(true) {
                int v8_0 = v7_0.read(v3_0);
                if (v8_0 == -1) {
                    break;
                }
                v2_1.write(v3_0, 0, v8_0);
            }
            this.this$0.Changes = v2_1.toString();
            com.chelpus.Utils.save_text_to_file(new java.io.File(new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.basepath).append("/Changes/changelog.txt").toString()), this.this$0.Changes);
            v2_1.close();
        }
        return Boolean.valueOf(1);
    }

    protected bridge synthetic Object doInBackground(Object[] p2)
    {
        return this.doInBackground(((String[]) p2));
    }

    protected void onPostExecute(Boolean p7)
    {
        super.onPostExecute(p7);
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$GetChangeLog$1 v3_1 = new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$GetChangeLog$1(this);
        com.chelpus.Utils.showDialogCustomYes("", new StringBuilder().append(com.chelpus.Utils.getText(2131165562)).append("\n\n").append(com.chelpus.Utils.getText(2131165219)).append(" ").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.version).append("\n\nChangeLog:\n\n").append(this.this$0.Changes).toString(), com.chelpus.Utils.getText(2131165734), v3_1, v3_1, 0);
        return;
    }

    protected bridge synthetic void onPostExecute(Object p1)
    {
        this.onPostExecute(((Boolean) p1));
        return;
    }
}
