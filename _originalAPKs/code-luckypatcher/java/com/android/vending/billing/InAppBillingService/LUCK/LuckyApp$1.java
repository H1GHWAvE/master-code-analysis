package com.android.vending.billing.InAppBillingService.LUCK;
 class LuckyApp$1 implements java.lang.Thread$UncaughtExceptionHandler {
    final synthetic com.android.vending.billing.InAppBillingService.LUCK.LuckyApp this$0;

    LuckyApp$1(com.android.vending.billing.InAppBillingService.LUCK.LuckyApp p1)
    {
        this.this$0 = p1;
        return;
    }

    public void uncaughtException(Thread p13, Throwable p14)
    {
        System.out.println(new StringBuilder().append("FATAL Exception LP ").append(p14.toString()).toString());
        try {
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.init();
            String v5 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getPackageInfo(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.getContext().getPackageName(), 0).versionName;
            com.android.vending.billing.InAppBillingService.LUCK.LogCollector v3_1 = new java.io.File(new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.basepath).append("/Log/Exception.").append(v5).append(".txt").toString());
            java.io.StringWriter v1_1 = new java.io.StringWriter();
            p14.printStackTrace(new java.io.PrintWriter(v1_1));
            com.chelpus.Utils.save_text_to_file(v3_1, new StringBuilder().append("Lucky Pacther ver. ").append(v5).append("\n\n ").append(v1_1.toString()).toString());
        } catch (Exception v0_0) {
            v0_0.printStackTrace();
        }
        p14.printStackTrace();
        int v4 = 0;
        if (this.this$0.getSharedPreferences("config", 4).getBoolean("force_close", 0)) {
            v4 = 1;
        }
        this.this$0.getSharedPreferences("config", 4).edit().putBoolean("force_close", 1).commit();
        try {
            new com.android.vending.billing.InAppBillingService.LUCK.LogCollector().collect(com.android.vending.billing.InAppBillingService.LUCK.LuckyApp.instance, 1);
        } catch (Exception v0_2) {
            v0_2.printStackTrace();
            System.exit(0);
            return;
        } catch (Exception v0_1) {
            System.exit(0);
            v0_1.printStackTrace();
            System.exit(0);
            return;
        }
        if (v4 != 0) {
            System.exit(0);
            return;
        } else {
            this.this$0.startActivity(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getLaunchIntentForPackage(this.this$0.getPackageName()));
            System.exit(0);
            return;
        }
    }
}
