package com.android.vending.billing.InAppBillingService.LUCK;
 class ITestServiceInterface$Stub$Proxy implements com.android.vending.billing.InAppBillingService.LUCK.ITestServiceInterface {
    private android.os.IBinder mRemote;

    ITestServiceInterface$Stub$Proxy(android.os.IBinder p1)
    {
        this.mRemote = p1;
        return;
    }

    public android.os.IBinder asBinder()
    {
        return this.mRemote;
    }

    public boolean checkService()
    {
        Throwable v2 = 1;
        android.os.Parcel v0 = android.os.Parcel.obtain();
        android.os.Parcel v1 = android.os.Parcel.obtain();
        try {
            v0.writeInterfaceToken("com.android.vending.billing.InAppBillingService.LUCK.ITestServiceInterface");
            this.mRemote.transact(1, v0, v1, 0);
            v1.readException();
        } catch (Throwable v3_1) {
            v1.recycle();
            v0.recycle();
            throw v3_1;
        }
        if (v1.readInt() == 0) {
            v2 = 0;
        }
        v1.recycle();
        v0.recycle();
        return v2;
    }

    public String getInterfaceDescriptor()
    {
        return "com.android.vending.billing.InAppBillingService.LUCK.ITestServiceInterface";
    }
}
