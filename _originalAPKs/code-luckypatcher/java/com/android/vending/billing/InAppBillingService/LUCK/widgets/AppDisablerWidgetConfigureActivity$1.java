package com.android.vending.billing.InAppBillingService.LUCK.widgets;
 class AppDisablerWidgetConfigureActivity$1 extends android.widget.ArrayAdapter {
    final synthetic com.android.vending.billing.InAppBillingService.LUCK.widgets.AppDisablerWidgetConfigureActivity this$0;

    AppDisablerWidgetConfigureActivity$1(com.android.vending.billing.InAppBillingService.LUCK.widgets.AppDisablerWidgetConfigureActivity p1, android.content.Context p2, int p3, com.android.vending.billing.InAppBillingService.LUCK.widgets.PkgItem[] p4)
    {
        this.this$0 = p1;
        this(p2, p3, p4);
        return;
    }

    public android.view.View getView(int p29, android.view.View p30, android.view.ViewGroup p31)
    {
        android.view.View v21 = p30;
        if (p30 == null) {
            v21 = ((android.view.LayoutInflater) com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance().getSystemService("layout_inflater")).inflate(2130968622, p31, 0);
        }
        android.widget.TextView v26_1 = ((android.widget.TextView) v21.findViewById(2131558457));
        android.widget.ImageView v13_1 = ((android.widget.ImageView) v21.findViewById(2131558456));
        v13_1.setImageDrawable(0);
        v26_1.setTextAppearance(this.getContext(), com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getSizeText());
        v26_1.setTextColor(-1);
        com.android.vending.billing.InAppBillingService.LUCK.widgets.PkgItem v16_1 = ((com.android.vending.billing.InAppBillingService.LUCK.widgets.PkgItem) this.getItem(p29));
        try {
            int v14 = ((int) ((1103626240 * com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getRes().getDisplayMetrics().density) + 1056964608));
        } catch (OutOfMemoryError v27) {
            v27.printStackTrace();
            System.gc();
            v26_1.setCompoundDrawablePadding(((int) ((1084227584 * com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getRes().getDisplayMetrics().density) + 1056964608)));
            v26_1.setText(new StringBuilder().append(v16_1.label).append("\n").append(v16_1.package_name).toString());
            v26_1.setTypeface(0, 1);
            return v21;
        } catch (OutOfMemoryError v12_2) {
            v12_2.printStackTrace();
            v26_1.setCompoundDrawablePadding(((int) ((1084227584 * com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getRes().getDisplayMetrics().density) + 1056964608)));
            v26_1.setText(new StringBuilder().append(v16_1.label).append("\n").append(v16_1.package_name).toString());
            v26_1.setTypeface(0, 1);
            return v21;
        }
        try {
            int v2 = ((android.graphics.drawable.BitmapDrawable) com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getApplicationIcon(v16_1.package_name)).getBitmap();
            int v5 = v2.getWidth();
            int v6 = v2.getHeight();
            float v25 = (((float) v14) / ((float) v5));
            float v23 = (((float) v14) / ((float) v6));
            int v7_1 = new android.graphics.Matrix();
            v7_1.postScale(v25, v23);
            v13_1.setImageDrawable(new android.graphics.drawable.BitmapDrawable(android.graphics.Bitmap.createBitmap(v2, 0, 0, v5, v6, v7_1, 1)));
        } catch (OutOfMemoryError v12_0) {
            v12_0.printStackTrace();
        } catch (OutOfMemoryError v12_1) {
            v12_1.printStackTrace();
        } catch (OutOfMemoryError v12) {
        }
        v26_1.setCompoundDrawablePadding(((int) ((1084227584 * com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getRes().getDisplayMetrics().density) + 1056964608)));
        v26_1.setText(new StringBuilder().append(v16_1.label).append("\n").append(v16_1.package_name).toString());
        v26_1.setTypeface(0, 1);
        return v21;
    }
}
