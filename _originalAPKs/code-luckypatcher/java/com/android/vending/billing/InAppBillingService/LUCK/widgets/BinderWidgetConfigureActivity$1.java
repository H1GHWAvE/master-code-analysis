package com.android.vending.billing.InAppBillingService.LUCK.widgets;
 class BinderWidgetConfigureActivity$1 extends android.widget.ArrayAdapter {
    final synthetic com.android.vending.billing.InAppBillingService.LUCK.widgets.BinderWidgetConfigureActivity this$0;

    BinderWidgetConfigureActivity$1(com.android.vending.billing.InAppBillingService.LUCK.widgets.BinderWidgetConfigureActivity p1, android.content.Context p2, int p3, java.util.List p4)
    {
        this.this$0 = p1;
        this(p2, p3, p4);
        return;
    }

    public android.view.View getView(int p8, android.view.View p9, android.view.ViewGroup p10)
    {
        android.view.View v3 = super.getView(p8, p9, p10);
        android.widget.TextView v2_1 = ((android.widget.TextView) v3.findViewById(2131558457));
        v2_1.setTextAppearance(this.getContext(), com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getSizeText());
        v2_1.setTextAppearance(this.this$0.context, this.this$0.sizeText);
        com.android.vending.billing.InAppBillingService.LUCK.BindItem v0_1 = ((com.android.vending.billing.InAppBillingService.LUCK.BindItem) this.getItem(p8));
        if ((!v0_1.SourceDir.equals("empty")) || (!v0_1.TargetDir.equals("empty"))) {
            v2_1.setText(com.chelpus.Utils.getColoredText(new StringBuilder().append(com.chelpus.Utils.getText(2131165236)).append("\n").toString(), "#ff00ff00", "bold"));
            v2_1.append(com.chelpus.Utils.getColoredText(v0_1.SourceDir, "#ffffffff", "italic"));
            v2_1.append(com.chelpus.Utils.getColoredText(new StringBuilder().append("\n").append(com.chelpus.Utils.getText(2131165237)).append("\n").toString(), "#ffffff00", "bold"));
            v2_1.append(com.chelpus.Utils.getColoredText(v0_1.TargetDir.replace("~chelpus_disabled~", ""), "#ffffffff", "italic"));
        } else {
            v2_1.setText(new StringBuilder().append(com.chelpus.Utils.getText(2131165424)).append("\n").append(com.chelpus.Utils.getText(2131165425)).toString());
            v3.setEnabled(0);
            v3.setClickable(0);
        }
        return v3;
    }
}
