package com.android.vending.billing.InAppBillingService.LUCK;
final class listAppsFragment$byPkgStatus implements java.util.Comparator {
    final synthetic com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment this$0;

    listAppsFragment$byPkgStatus(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment p1)
    {
        this.this$0 = p1;
        return;
    }

    public int compare(com.android.vending.billing.InAppBillingService.LUCK.PkgListItem p7, com.android.vending.billing.InAppBillingService.LUCK.PkgListItem p8)
    {
        if ((p7 != null) && (p8 != null)) {
            int v1 = Integer.valueOf(p7.stored).compareTo(Integer.valueOf(p8.stored));
            if (v1 == 0) {
                if (p7.stored != 0) {
                    try {
                        v1 = p7.toString().compareToIgnoreCase(p8.toString());
                    } catch (NullPointerException v0) {
                        v0.printStackTrace();
                        v1 = 0;
                    }
                } else {
                    int v2 = 255;
                    int v3 = 255;
                    if ((!p7.ads) && (!p7.lvl)) {
                        v2 = 4;
                    }
                    if ((!p8.ads) && (!p8.lvl)) {
                        v3 = 4;
                    }
                    if (p7.ads) {
                        v2 = 3;
                    }
                    if (p8.ads) {
                        v3 = 3;
                    }
                    if (p7.lvl) {
                        v2 = 2;
                    }
                    if (p8.lvl) {
                        v3 = 2;
                    }
                    if (p7.custom) {
                        v2 = 1;
                    }
                    if (p8.custom) {
                        v3 = 1;
                    }
                    v1 = Integer.valueOf(v2).compareTo(Integer.valueOf(v3));
                }
            }
            return v1;
        } else {
            throw new ClassCastException();
        }
    }

    public bridge synthetic int compare(Object p2, Object p3)
    {
        return this.compare(((com.android.vending.billing.InAppBillingService.LUCK.PkgListItem) p2), ((com.android.vending.billing.InAppBillingService.LUCK.PkgListItem) p3));
    }
}
