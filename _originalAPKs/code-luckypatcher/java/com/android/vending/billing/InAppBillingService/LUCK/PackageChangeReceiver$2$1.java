package com.android.vending.billing.InAppBillingService.LUCK;
 class PackageChangeReceiver$2$1 implements android.view.View$OnClickListener {
    final synthetic com.android.vending.billing.InAppBillingService.LUCK.PackageChangeReceiver$2 this$1;

    PackageChangeReceiver$2$1(com.android.vending.billing.InAppBillingService.LUCK.PackageChangeReceiver$2 p1)
    {
        this.this$1 = p1;
        return;
    }

    public void onClick(android.view.View p8)
    {
        if (android.os.Build$VERSION.SDK_INT < 9) {
            android.content.Intent v0_1 = new android.content.Intent("android.intent.action.VIEW");
            v0_1.setClassName("com.android.settings", "com.android.settings.InstalledAppDetails");
            v0_1.putExtra("com.android.settings.ApplicationPkgName", "com.android.vending");
            v0_1.putExtra("pkg", "com.android.vending");
            v0_1.setFlags(131072);
            v0_1.setFlags(268435456);
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance().startActivity(v0_1);
        } else {
            android.content.Intent v0_3 = new android.content.Intent("android.settings.APPLICATION_DETAILS_SETTINGS", android.net.Uri.parse("package:com.android.vending"));
            v0_3.setFlags(131072);
            v0_3.setFlags(268435456);
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance().startActivity(v0_3);
        }
        ((android.view.WindowManager) com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance().getSystemService("window")).removeView(p8.getRootView());
        return;
    }
}
