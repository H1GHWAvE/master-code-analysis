package com.android.vending.billing.InAppBillingService.LUCK;
public class MenuItem {
    public String booleanPref;
    public boolean booleanPrefDef;
    public java.util.List childs;
    public boolean icon;
    public int punkt_menu;
    public int punkt_menu_descr;
    public Runnable runCode;
    public int type;
    public int typeChild;

    public MenuItem(int p3, int p4, java.util.List p5, int p6, int p7, boolean p8)
    {
        this.punkt_menu_descr = 0;
        this.icon = 0;
        this.type = 0;
        this.typeChild = 0;
        this.runCode = 0;
        this.booleanPref = 0;
        this.booleanPrefDef = 0;
        this.punkt_menu = p3;
        this.childs = p5;
        this.icon = p8;
        this.type = p6;
        this.typeChild = p7;
        this.punkt_menu_descr = p4;
        return;
    }

    public MenuItem(int p3, int p4, java.util.List p5, int p6, boolean p7)
    {
        this.punkt_menu_descr = 0;
        this.icon = 0;
        this.type = 0;
        this.typeChild = 0;
        this.runCode = 0;
        this.booleanPref = 0;
        this.booleanPrefDef = 0;
        this.punkt_menu = p3;
        this.childs = p5;
        this.icon = p7;
        this.type = p6;
        this.punkt_menu_descr = p4;
        return;
    }

    public MenuItem(int p3, int p4, java.util.List p5, int p6, boolean p7, String p8, boolean p9, Runnable p10)
    {
        this.punkt_menu_descr = 0;
        this.icon = 0;
        this.type = 0;
        this.typeChild = 0;
        this.runCode = 0;
        this.booleanPref = 0;
        this.booleanPrefDef = 0;
        this.punkt_menu = p3;
        this.childs = p5;
        this.icon = p7;
        this.type = p6;
        this.punkt_menu_descr = p4;
        this.runCode = p10;
        this.booleanPref = p8;
        this.booleanPrefDef = p9;
        return;
    }

    public MenuItem(int p3, java.util.List p4, int p5, int p6, boolean p7)
    {
        this.punkt_menu_descr = 0;
        this.icon = 0;
        this.type = 0;
        this.typeChild = 0;
        this.runCode = 0;
        this.booleanPref = 0;
        this.booleanPrefDef = 0;
        this.punkt_menu = p3;
        this.childs = p4;
        this.icon = p7;
        this.type = p5;
        this.typeChild = p6;
        return;
    }

    public MenuItem(int p3, java.util.List p4, int p5, boolean p6)
    {
        this.punkt_menu_descr = 0;
        this.icon = 0;
        this.type = 0;
        this.typeChild = 0;
        this.runCode = 0;
        this.booleanPref = 0;
        this.booleanPrefDef = 0;
        this.punkt_menu = p3;
        this.childs = p4;
        this.icon = p6;
        this.type = p5;
        return;
    }

    public MenuItem(int p3, java.util.List p4, boolean p5)
    {
        this.punkt_menu_descr = 0;
        this.icon = 0;
        this.type = 0;
        this.typeChild = 0;
        this.runCode = 0;
        this.booleanPref = 0;
        this.booleanPrefDef = 0;
        this.punkt_menu = p3;
        this.childs = p4;
        this.icon = p5;
        return;
    }
}
