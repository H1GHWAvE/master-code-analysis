package com.android.vending.billing.InAppBillingService.LUCK.dialogs;
public class Menu_Dialog {
    android.app.Dialog dialog;

    public Menu_Dialog()
    {
        this.dialog = 0;
        return;
    }

    public void dismiss()
    {
        if (this.dialog != null) {
            this.dialog.dismiss();
            this.dialog = 0;
        }
        return;
    }

    public android.app.Dialog onCreateDialog()
    {
        try {
            System.out.println("Menu Dialog create.");
        } catch (Exception v1) {
            v1.printStackTrace();
            android.app.Dialog v2_9 = 0;
            return v2_9;
        }
        if ((com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag == null) || (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.getContext() == null)) {
            this.dismiss();
        }
        com.android.vending.billing.InAppBillingService.LUCK.AlertDlg v0_1 = new com.android.vending.billing.InAppBillingService.LUCK.AlertDlg(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.getContext());
        if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.adapt != null) {
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.adapt.setNotifyOnChange(1);
            v0_1.setAdapter(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.adapt, new com.android.vending.billing.InAppBillingService.LUCK.dialogs.Menu_Dialog$1(this));
        }
        v0_1.setOnCancelListener(new com.android.vending.billing.InAppBillingService.LUCK.dialogs.Menu_Dialog$2(this));
        v2_9 = v0_1.create();
        return v2_9;
    }

    public void showDialog()
    {
        if (this.dialog == null) {
            this.dialog = this.onCreateDialog();
        }
        if (this.dialog != null) {
            this.dialog.show();
        }
        return;
    }
}
