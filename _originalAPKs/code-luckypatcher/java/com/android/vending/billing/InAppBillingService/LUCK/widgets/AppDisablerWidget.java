package com.android.vending.billing.InAppBillingService.LUCK.widgets;
public class AppDisablerWidget extends android.appwidget.AppWidgetProvider {
    public static String ACTION_WIDGET_RECEIVER;
    public static String ACTION_WIDGET_RECEIVER_Updater;

    static AppDisablerWidget()
    {
        com.android.vending.billing.InAppBillingService.LUCK.widgets.AppDisablerWidget.ACTION_WIDGET_RECEIVER = "ActionReceiverWidgetAppDisabler";
        com.android.vending.billing.InAppBillingService.LUCK.widgets.AppDisablerWidget.ACTION_WIDGET_RECEIVER_Updater = "ActionReceiverWidgetAppDisablerUpdate";
        return;
    }

    public AppDisablerWidget()
    {
        return;
    }

    public static void pushWidgetUpdate(android.content.Context p3, android.widget.RemoteViews p4)
    {
        android.appwidget.AppWidgetManager.getInstance(p3).updateAppWidget(new android.content.ComponentName(p3, com.android.vending.billing.InAppBillingService.LUCK.widgets.AppDisablerWidget), p4);
        return;
    }

    static void updateAppWidget(android.content.Context p2, android.appwidget.AppWidgetManager p3, int p4)
    {
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.init();
        Thread v0_1 = new Thread(new com.android.vending.billing.InAppBillingService.LUCK.widgets.AppDisablerWidget$1(p2, p4, p3));
        v0_1.setPriority(10);
        v0_1.start();
        return;
    }

    public void onDeleted(android.content.Context p4, int[] p5)
    {
        int v0 = p5.length;
        int v1 = 0;
        while (v1 < v0) {
            com.android.vending.billing.InAppBillingService.LUCK.widgets.AppDisablerWidgetConfigureActivity.deleteTitlePref(p4, p5[v1]);
            v1++;
        }
        return;
    }

    public void onDisabled(android.content.Context p1)
    {
        return;
    }

    public void onEnabled(android.content.Context p1)
    {
        return;
    }

    public void onReceive(android.content.Context p9, android.content.Intent p10)
    {
        String v0 = p10.getAction();
        if (com.android.vending.billing.InAppBillingService.LUCK.widgets.AppDisablerWidget.ACTION_WIDGET_RECEIVER.equals(v0)) {
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.init();
            Thread v5_1 = new Thread(new com.android.vending.billing.InAppBillingService.LUCK.widgets.AppDisablerWidget$2(this, p9, p10, new android.os.Handler()));
            v5_1.setPriority(10);
            v5_1.start();
        }
        if (com.android.vending.billing.InAppBillingService.LUCK.widgets.AppDisablerWidget.ACTION_WIDGET_RECEIVER_Updater.equals(v0)) {
            try {
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.appDisabler = 1;
                android.appwidget.AppWidgetManager v2 = android.appwidget.AppWidgetManager.getInstance(p9);
                this.onUpdate(p9, v2, v2.getAppWidgetIds(new android.content.ComponentName(p9, com.android.vending.billing.InAppBillingService.LUCK.widgets.AppDisablerWidget)));
            } catch (Exception v1) {
                v1.printStackTrace();
            }
        }
        super.onReceive(p9, p10);
        return;
    }

    public void onUpdate(android.content.Context p4, android.appwidget.AppWidgetManager p5, int[] p6)
    {
        int v0 = p6.length;
        int v1 = 0;
        while (v1 < v0) {
            com.android.vending.billing.InAppBillingService.LUCK.widgets.AppDisablerWidget.updateAppWidget(p4, p5, p6[v1]);
            v1++;
        }
        return;
    }
}
