package com.android.vending.billing.InAppBillingService.LUCK;
 class PkgListItemAdapter$2 extends android.widget.Filter {
    final synthetic com.android.vending.billing.InAppBillingService.LUCK.PkgListItemAdapter this$0;

    PkgListItemAdapter$2(com.android.vending.billing.InAppBillingService.LUCK.PkgListItemAdapter p1)
    {
        this.this$0 = p1;
        return;
    }

    protected android.widget.Filter$FilterResults performFiltering(CharSequence p10)
    {
        android.widget.Filter$FilterResults v1_1 = new android.widget.Filter$FilterResults();
        java.util.ArrayList v2_1 = new java.util.ArrayList();
        if (this.this$0.orig == null) {
            this.this$0.orig = this.this$0.data;
        }
        if (p10 != null) {
            if ((this.this$0.orig != null) && (this.this$0.orig.length > 0)) {
                com.android.vending.billing.InAppBillingService.LUCK.PkgListItem[] v5_2 = this.this$0.orig;
                int v6 = v5_2.length;
                int v4_9 = 0;
                while (v4_9 < v6) {
                    com.android.vending.billing.InAppBillingService.LUCK.PkgListItem v0 = v5_2[v4_9];
                    if ((v0.name.toLowerCase().contains(p10.toString().toLowerCase())) || (v0.pkgName.toLowerCase().contains(p10.toString().toLowerCase()))) {
                        v2_1.add(v0);
                    }
                    v4_9++;
                }
            }
            com.android.vending.billing.InAppBillingService.LUCK.PkgListItem[] v3 = new com.android.vending.billing.InAppBillingService.LUCK.PkgListItem[v2_1.size()];
            v2_1.toArray(v3);
            v1_1.values = v3;
        }
        return v1_1;
    }

    protected void publishResults(CharSequence p3, android.widget.Filter$FilterResults p4)
    {
        this.this$0.data = ((com.android.vending.billing.InAppBillingService.LUCK.PkgListItem[]) ((com.android.vending.billing.InAppBillingService.LUCK.PkgListItem[]) p4.values));
        this.this$0.notifyDataSetChanged();
        return;
    }
}
