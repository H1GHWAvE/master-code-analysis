package com.android.vending.billing.InAppBillingService.LUCK.widgets;
public class BinderWidget extends android.appwidget.AppWidgetProvider {
    public static String ACTION_WIDGET_RECEIVER;
    public static String ACTION_WIDGET_RECEIVER_Updater;
    public static com.android.vending.billing.InAppBillingService.LUCK.widgets.BinderWidget widget;

    static BinderWidget()
    {
        com.android.vending.billing.InAppBillingService.LUCK.widgets.BinderWidget.ACTION_WIDGET_RECEIVER = "ActionReceiverWidgetBinder";
        com.android.vending.billing.InAppBillingService.LUCK.widgets.BinderWidget.ACTION_WIDGET_RECEIVER_Updater = "ActionReceiverWidgetBinderUpdate";
        com.android.vending.billing.InAppBillingService.LUCK.widgets.BinderWidget.widget = 0;
        return;
    }

    public BinderWidget()
    {
        return;
    }

    public static void pushWidgetUpdate(android.content.Context p3, android.widget.RemoteViews p4)
    {
        android.appwidget.AppWidgetManager.getInstance(p3).updateAppWidget(new android.content.ComponentName(p3, com.android.vending.billing.InAppBillingService.LUCK.widgets.BinderWidget), p4);
        return;
    }

    static void updateAppWidget(android.content.Context p2, android.appwidget.AppWidgetManager p3, int p4)
    {
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.init();
        Thread v0_1 = new Thread(new com.android.vending.billing.InAppBillingService.LUCK.widgets.BinderWidget$1(p2, p4));
        v0_1.setPriority(10);
        v0_1.start();
        return;
    }

    public void onDeleted(android.content.Context p4, int[] p5)
    {
        int v0 = p5.length;
        int v1 = 0;
        while (v1 < v0) {
            com.android.vending.billing.InAppBillingService.LUCK.widgets.BinderWidgetConfigureActivity.deleteTitlePref(p4, p5[v1]);
            v1++;
        }
        return;
    }

    public void onDisabled(android.content.Context p1)
    {
        return;
    }

    public void onEnabled(android.content.Context p1)
    {
        return;
    }

    public void onReceive(android.content.Context p10, android.content.Intent p11)
    {
        super.onReceive(p10, p11);
        String v0 = p11.getAction();
        if (com.android.vending.billing.InAppBillingService.LUCK.widgets.BinderWidget.ACTION_WIDGET_RECEIVER.equals(v0)) {
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.init();
            if (!com.android.vending.billing.InAppBillingService.LUCK.widgets.BinderWidgetConfigureActivity.loadTitlePref(p10, p11.getIntExtra("appWidgetId", -1)).equals("NOT_SAVED_BIND")) {
                com.android.vending.billing.InAppBillingService.LUCK.widgets.BinderWidget.widget = this;
            }
            try {
                Thread v6_1 = new Thread(new com.android.vending.billing.InAppBillingService.LUCK.widgets.BinderWidget$2(this, p11, p10, new android.os.Handler()));
                v6_1.setPriority(10);
            } catch (NullPointerException v1) {
                v1.printStackTrace();
            }
            if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.binder_process) {
                v6_1.start();
            }
        }
        if (com.android.vending.billing.InAppBillingService.LUCK.widgets.BinderWidget.ACTION_WIDGET_RECEIVER_Updater.equals(v0)) {
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.binderWidget = 1;
            android.appwidget.AppWidgetManager v2 = android.appwidget.AppWidgetManager.getInstance(p10);
            this.onUpdate(p10, v2, v2.getAppWidgetIds(new android.content.ComponentName(p10, com.android.vending.billing.InAppBillingService.LUCK.widgets.BinderWidget)));
        }
        return;
    }

    public void onUpdate(android.content.Context p4, android.appwidget.AppWidgetManager p5, int[] p6)
    {
        int v0 = p6.length;
        int v1 = 0;
        while (v1 < v0) {
            com.android.vending.billing.InAppBillingService.LUCK.widgets.BinderWidget.updateAppWidget(p4, p5, p6[v1]);
            v1++;
        }
        return;
    }
}
