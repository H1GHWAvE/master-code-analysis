package com.android.vending.billing.InAppBillingService.LUCK;
public class BindItem {
    public String SourceDir;
    public String TargetDir;

    public BindItem(String p4)
    {
        this.SourceDir = "";
        this.TargetDir = "";
        String[] v0 = p4.split(";");
        if (v0.length == 2) {
            this.SourceDir = v0[0];
            this.TargetDir = v0[1];
        }
        return;
    }

    public BindItem(String p2, String p3)
    {
        this.SourceDir = "";
        this.TargetDir = "";
        this.SourceDir = p2;
        this.TargetDir = p3;
        return;
    }

    public String toString()
    {
        return new StringBuilder().append(this.SourceDir).append(";").append(this.TargetDir).toString();
    }
}
