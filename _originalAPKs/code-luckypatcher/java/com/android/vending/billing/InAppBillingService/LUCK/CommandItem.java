package com.android.vending.billing.InAppBillingService.LUCK;
public class CommandItem {
    public byte[] Method;
    public byte[] Object;
    public boolean bits32;
    public boolean found_id_object;
    public boolean found_index_command;
    public byte[] id_object;
    public byte[] index_command;
    public String method;
    public String object;

    public CommandItem(String p5, String p6)
    {
        this.object = 0;
        this.method = 0;
        this.Object = 0;
        this.Method = 0;
        this.id_object = 0;
        this.index_command = 0;
        this.bits32 = 0;
        this.found_index_command = 0;
        this.found_id_object = 0;
        this.object = p5;
        this.method = p6;
        byte[] v0_1 = new byte[4];
        this.Object = v0_1;
        byte[] v0_2 = new byte[4];
        this.Method = v0_2;
        this.bits32 = 0;
        byte[] v0_3 = new byte[2];
        this.id_object = v0_3;
        byte[] v0_4 = new byte[2];
        this.index_command = v0_4;
        return;
    }
}
