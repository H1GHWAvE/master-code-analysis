package com.android.vending.billing.InAppBillingService.LUCK.dialogs;
public class Ext_Patch_Dialog {
    android.app.Dialog dialog;

    public Ext_Patch_Dialog()
    {
        this.dialog = 0;
        return;
    }

    public void dismiss()
    {
        if (this.dialog != null) {
            this.dialog.dismiss();
            this.dialog = 0;
        }
        return;
    }

    public android.app.Dialog onCreateDialog()
    {
        System.out.println("Ext Patch Dialog create.");
        if ((com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag == null) || (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.getContext() == null)) {
            this.dismiss();
        }
        android.widget.LinearLayout v6_1 = ((android.widget.LinearLayout) android.view.View.inflate(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.getContext(), 2130968613, 0));
        android.widget.LinearLayout v5_1 = ((android.widget.LinearLayout) v6_1.findViewById(2131558543).findViewById(2131558544));
        int v11 = 0;
        int v8 = 0;
        if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str == null) {
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str = " ";
        }
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.CurentSelect = 0;
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.contextext = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.getContext();
        String v16 = 0;
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str.split("\n");
        String[] v12 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str.split("\n");
        int v9 = 1;
        while ((v9 != 0) && (v8 < v12.length)) {
            try {
                v16 = new org.json.JSONObject(v12[v8]).getString("objects");
                v9 = 0;
                v8 = 0;
            } catch (org.json.JSONException v7) {
                v9 = 1;
                v8 = (0 + 1);
                v11 = 0;
            }
        }
        if (v16 != null) {
            v11 = Integer.parseInt(v16);
        }
        String[] v4 = 0;
        if (v11 != 0) {
            v4 = new String[(v11 + 1)];
            while (v8 < v4.length) {
                if (v8 != 0) {
                    v4[v8] = new StringBuilder().append("Object N").append(v8).toString();
                } else {
                    v4[0] = "Please Select";
                }
                v8++;
            }
        }
        String v14_0 = new StringBuilder().append(com.chelpus.Utils.getText(2131165448)).append(" ").append(v11).append(" ").append(com.chelpus.Utils.getText(2131165449)).toString();
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.tvt = ((android.widget.TextView) v5_1.findViewById(2131558545));
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.tvt.append(com.chelpus.Utils.getColoredText(v14_0, -16711821, "bold"));
        android.widget.Spinner v13_1 = ((android.widget.Spinner) v5_1.findViewById(2131558546));
        if (v4 != null) {
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.CurentSelect = 0;
            v13_1.setAdapter(new android.widget.ArrayAdapter(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.getContext(), 17367048, v4));
            com.android.vending.billing.InAppBillingService.LUCK.dialogs.Ext_Patch_Dialog$2 v17_35 = new com.android.vending.billing.InAppBillingService.LUCK.dialogs.Ext_Patch_Dialog$1;
            v17_35(this);
            v13_1.setOnItemSelectedListener(v17_35);
        }
        if (v11 == 0) {
            v13_1.setEnabled(0);
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.CurentSelect = 0;
        }
        String v14_1 = com.chelpus.Utils.getText(2131165450);
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.tvt = ((android.widget.TextView) v5_1.findViewById(2131558551));
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.tvt.append(com.chelpus.Utils.getColoredText(v14_1, -990142, "bold"));
        if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str.contains("SU Java-Code Running!")) {
            String v14_2 = new StringBuilder().append("\n").append(com.chelpus.Utils.getText(2131165567)).append("\n").toString();
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.tvt = ((android.widget.TextView) v5_1.findViewById(2131558551));
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.tvt.append(com.chelpus.Utils.getColoredText(v14_2, -16711681, "bold"));
        }
        android.app.Dialog v15 = new com.android.vending.billing.InAppBillingService.LUCK.AlertDlg(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.getContext(), 1).setView(v6_1).create();
        v15.setTitle(com.chelpus.Utils.getText(2131165186));
        v15.setCancelable(1);
        com.android.vending.billing.InAppBillingService.LUCK.dialogs.Ext_Patch_Dialog$2 v17_61 = new com.android.vending.billing.InAppBillingService.LUCK.dialogs.Ext_Patch_Dialog$2;
        v17_61(this);
        v15.setOnCancelListener(v17_61);
        return v15;
    }

    public void showDialog()
    {
        if (this.dialog == null) {
            this.dialog = this.onCreateDialog();
        }
        if (this.dialog != null) {
            this.dialog.show();
        }
        return;
    }
}
