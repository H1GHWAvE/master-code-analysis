package com.android.vending.billing;
 class IMarketBillingService$Stub$Proxy implements com.android.vending.billing.IMarketBillingService {
    private android.os.IBinder mRemote;

    IMarketBillingService$Stub$Proxy(android.os.IBinder p1)
    {
        this.mRemote = p1;
        return;
    }

    public android.os.IBinder asBinder()
    {
        return this.mRemote;
    }

    public String getInterfaceDescriptor()
    {
        return "com.android.vending.billing.IMarketBillingService";
    }

    public android.os.Bundle sendBillingRequest(android.os.Bundle p7)
    {
        android.os.Parcel v0 = android.os.Parcel.obtain();
        android.os.Parcel v1 = android.os.Parcel.obtain();
        try {
            v0.writeInterfaceToken("com.android.vending.billing.IMarketBillingService");
        } catch (android.os.Parcelable$Creator v3_7) {
            v1.recycle();
            v0.recycle();
            throw v3_7;
        }
        if (p7 == null) {
            v0.writeInt(0);
        } else {
            v0.writeInt(1);
            p7.writeToParcel(v0, 0);
        }
        int v2_0;
        this.mRemote.transact(1, v0, v1, 0);
        v1.readException();
        if (v1.readInt() == 0) {
            v2_0 = 0;
        } else {
            v2_0 = ((android.os.Bundle) android.os.Bundle.CREATOR.createFromParcel(v1));
        }
        v1.recycle();
        v0.recycle();
        return v2_0;
    }
}
