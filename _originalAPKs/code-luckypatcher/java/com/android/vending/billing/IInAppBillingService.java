package com.android.vending.billing;
public interface IInAppBillingService implements android.os.IInterface {

    public abstract int consumePurchase();

    public abstract android.os.Bundle getBuyIntent();

    public abstract android.os.Bundle getBuyIntentToReplaceSkus();

    public abstract android.os.Bundle getPurchases();

    public abstract android.os.Bundle getSkuDetails();

    public abstract int isBillingSupported();
}
