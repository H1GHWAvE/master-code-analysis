package com.proxoid;
public class ProxoidService extends android.app.Service {
    private static int ID = 0;
    private static final String TAG = "ProxoidService";
    private com.mba.proxylight.ProxyLight proxy;
    private String randomUserAgent;
    private String useragent;

    static ProxoidService()
    {
        com.proxoid.ProxoidService.ID = 2130837551;
        return;
    }

    public ProxoidService()
    {
        this.proxy = 0;
        this.useragent = 0;
        this.randomUserAgent = Long.toString(System.currentTimeMillis(), 20);
        return;
    }

    static synthetic android.content.SharedPreferences access$000(com.proxoid.ProxoidService p1)
    {
        return p1.getSharedPreferences();
    }

    static synthetic String access$102(com.proxoid.ProxoidService p0, String p1)
    {
        p0.useragent = p1;
        return p1;
    }

    static synthetic void access$200(com.proxoid.ProxoidService p0)
    {
        p0.doStop();
        return;
    }

    static synthetic com.mba.proxylight.ProxyLight access$300(com.proxoid.ProxoidService p1)
    {
        return p1.proxy;
    }

    static synthetic com.mba.proxylight.ProxyLight access$302(com.proxoid.ProxoidService p0, com.mba.proxylight.ProxyLight p1)
    {
        p0.proxy = p1;
        return p1;
    }

    static synthetic int access$500()
    {
        return com.proxoid.ProxoidService.ID;
    }

    private void doStop()
    {
        if ((this.proxy != null) && (this.proxy.isRunning())) {
            android.util.Log.d("ProxoidService", "stopping");
            this.proxy.stop();
            ((android.app.NotificationManager) this.getSystemService("notification")).cancel(com.proxoid.ProxoidService.ID);
            android.widget.Toast.makeText(this, "Proxy stopped.", 0).show();
            android.content.SharedPreferences$Editor v0 = this.getSharedPreferences().edit();
            v0.putBoolean("onoff", 0);
            v0.commit();
        }
        return;
    }

    private android.content.SharedPreferences getSharedPreferences()
    {
        return super.getSharedPreferences("proxoidv6", 4);
    }

    public android.os.IBinder onBind(android.content.Intent p2)
    {
        return new com.proxoid.ProxoidService$1(this);
    }

    public void onCreate()
    {
        super.onCreate();
        return;
    }

    public void onDestroy()
    {
        this.doStop();
        super.onDestroy();
        return;
    }
}
