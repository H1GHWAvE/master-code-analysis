package com.proxoid;
 class ProxoidService$1 extends com.proxoid.IProxoidControl$Stub {
    final synthetic com.proxoid.ProxoidService this$0;

    ProxoidService$1(com.proxoid.ProxoidService p1)
    {
        this.this$0 = p1;
        return;
    }

    public boolean update()
    {
        boolean v12 = com.proxoid.ProxoidService.access$000(this.this$0).getBoolean("onoff", 0);
        int v10 = Integer.parseInt("8080");
        com.proxoid.ProxoidService.access$102(this.this$0, "Don\\\'t change");
        if (v12) {
            if (com.proxoid.ProxoidService.access$300(this.this$0) == null) {
                com.proxoid.ProxoidService.access$302(this.this$0, new com.proxoid.ProxoidService$1$1(this));
                com.proxoid.ProxoidService.access$300(this.this$0).getRequestFilters().add(new com.proxoid.ProxoidService$UserAgentRequestFilter(this.this$0, 0));
                com.proxoid.ProxoidService.access$300(this.this$0).setPort(v10);
            }
            if (com.proxoid.ProxoidService.access$300(this.this$0).getPort() != v10) {
                com.proxoid.ProxoidService.access$300(this.this$0).setPort(v10);
                com.proxoid.ProxoidService.access$300(this.this$0).stop();
                try {
                    com.proxoid.ProxoidService.access$300(this.this$0).start();
                    android.widget.Toast.makeText(this.this$0, "Service proxy restarted", 0).show();
                } catch (Exception v5_0) {
                    android.util.Log.e("ProxoidService", "", v5_0);
                    com.proxoid.ProxoidService.access$300(this.this$0).stop();
                    com.proxoid.ProxoidService.access$302(this.this$0, 0);
                    int v13_27 = 0;
                    return v13_27;
                }
            }
            if (com.proxoid.ProxoidService.access$300(this.this$0).isRunning()) {
                v13_27 = 1;
            } else {
                try {
                    com.proxoid.ProxoidService.access$300(this.this$0).start();
                    android.app.NotificationManager v7_1 = ((android.app.NotificationManager) this.this$0.getSystemService("notification"));
                    android.app.Notification v8_1 = new android.app.Notification(2130837552, "Lucky Patcher proxy running.", System.currentTimeMillis());
                    v8_1.setLatestEventInfo(this.this$0.getApplicationContext(), "Lucky Proxoid", "proxy running", android.app.PendingIntent.getActivity(this.this$0, 0, new android.content.Intent(this.this$0, com.proxoid.Proxoid), 0));
                    v8_1.flags = (v8_1.flags | 2);
                    v7_1.notify(com.proxoid.ProxoidService.access$500(), v8_1);
                    android.widget.Toast.makeText(this.this$0, "Proxy running.", 0).show();
                } catch (Exception v5_1) {
                    android.util.Log.e("ProxoidService", "", v5_1);
                    com.proxoid.ProxoidService.access$300(this.this$0).stop();
                    com.proxoid.ProxoidService.access$302(this.this$0, 0);
                    v13_27 = 0;
                }
            }
        } else {
            com.proxoid.ProxoidService.access$200(this.this$0);
        }
        return v13_27;
    }
}
