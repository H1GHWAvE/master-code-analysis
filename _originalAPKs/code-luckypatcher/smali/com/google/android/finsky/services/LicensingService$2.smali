.class Lcom/google/android/finsky/services/LicensingService$2;
.super Lcom/android/vending/licensing/ILicensingService$Stub;
.source "LicensingService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/services/LicensingService;-><init>()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/services/LicensingService;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/services/LicensingService;)V
    .registers 2
    .param p1, "this$0"    # Lcom/google/android/finsky/services/LicensingService;

    .prologue
    .line 149
    iput-object p1, p0, Lcom/google/android/finsky/services/LicensingService$2;->this$0:Lcom/google/android/finsky/services/LicensingService;

    invoke-direct {p0}, Lcom/android/vending/licensing/ILicensingService$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public checkLicense(JLjava/lang/String;Lcom/android/vending/licensing/ILicenseResultListener;)V
    .registers 15
    .param p1, "nonce"    # J
    .param p3, "packageName"    # Ljava/lang/String;
    .param p4, "listener"    # Lcom/android/vending/licensing/ILicenseResultListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 155
    :try_start_0
    iget-object v5, p0, Lcom/google/android/finsky/services/LicensingService$2;->this$0:Lcom/google/android/finsky/services/LicensingService;

    invoke-virtual {v5}, Lcom/google/android/finsky/services/LicensingService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, p3, v6}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v3

    .line 162
    .local v3, "v1":Landroid/content/pm/PackageInfo;
    iget-object v5, p0, Lcom/google/android/finsky/services/LicensingService$2;->this$0:Lcom/google/android/finsky/services/LicensingService;

    invoke-virtual {v5, p1, p2, p3}, Lcom/google/android/finsky/services/LicensingService;->connectToLicensing(JLjava/lang/String;)V

    .line 163
    iget-object v5, p0, Lcom/google/android/finsky/services/LicensingService$2;->this$0:Lcom/google/android/finsky/services/LicensingService;

    iget v5, v5, Lcom/google/android/finsky/services/LicensingService;->responseCode:I

    const/16 v6, 0xff

    if-eq v5, v6, :cond_35

    iget-object v5, p0, Lcom/google/android/finsky/services/LicensingService$2;->this$0:Lcom/google/android/finsky/services/LicensingService;

    iget v5, v5, Lcom/google/android/finsky/services/LicensingService;->responseCode:I

    if-nez v5, :cond_35

    .line 164
    sget-object v5, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v6, "Transfer license from Google Play"

    invoke-virtual {v5, v6}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 165
    iget-object v5, p0, Lcom/google/android/finsky/services/LicensingService$2;->this$0:Lcom/google/android/finsky/services/LicensingService;

    iget v5, v5, Lcom/google/android/finsky/services/LicensingService;->responseCode:I

    iget-object v6, p0, Lcom/google/android/finsky/services/LicensingService$2;->this$0:Lcom/google/android/finsky/services/LicensingService;

    iget-object v6, v6, Lcom/google/android/finsky/services/LicensingService;->signedData:Ljava/lang/String;

    iget-object v7, p0, Lcom/google/android/finsky/services/LicensingService$2;->this$0:Lcom/google/android/finsky/services/LicensingService;

    iget-object v7, v7, Lcom/google/android/finsky/services/LicensingService;->signature:Ljava/lang/String;

    invoke-interface {p4, v5, v6, v7}, Lcom/android/vending/licensing/ILicenseResultListener;->verifyLicense(ILjava/lang/String;Ljava/lang/String;)V

    .line 196
    .end local v3    # "v1":Landroid/content/pm/PackageInfo;
    :goto_34
    return-void

    .line 179
    .restart local v3    # "v1":Landroid/content/pm/PackageInfo;
    :cond_35
    const-wide v0, 0x757b12c00L

    .line 180
    .local v0, "god":J
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "0|"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "|"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "|"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v3, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "|ANlOHQOShF3uJUwv3Ql+fbsgEG9FD35Hag==|"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    add-long/2addr v6, v0

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ":GR=10&VT="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    const-string v8, "31622400000"

    invoke-static {v8}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v8

    .line 181
    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    add-long/2addr v6, v8

    .line 180
    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "&GT="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 181
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    add-long/2addr v6, v0

    add-long/2addr v6, v0

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 184
    .local v2, "signedData":Ljava/lang/String;
    new-instance v5, Lcom/chelpus/Utils;

    const-string v6, "w"

    invoke-direct {v5, v6}, Lcom/chelpus/Utils;-><init>(Ljava/lang/String;)V

    const-wide/16 v6, 0xfa0

    invoke-virtual {v5, v6, v7}, Lcom/chelpus/Utils;->waitLP(J)V

    .line 185
    const/4 v5, 0x0

    const-string v6, "hL9GqWwZL35OoLxZQN1EYmyylu3zmf8umnXW4P0EPqGjV0QcRYjD+NtiqoDEmxnnocvrqA7Z/0v+i0O4cwgOsD7/Tg3B1QI/ukA7ZUcibvFQUNoq7KjUWSg1Qn5MauaFFhAhZbuP840wnCuntxVDUkVJ6GDymDXLqhFG1LbZmNoPl6QjkschEBLVth1YtBxE4GnbVVI8Cq5LY7/F0N8d6EGLIISD6ekoD4lkhxq3nORsibX7kjmotyhLpO7THNMXvOeXeKhVp6dNSblOHp9tcL6l/NJY7sHPw/DBSxteW2hZ9y7yyaMxMAz+nTIN/V8gJXzeaRlmIXntJpQDEMz5pQ=="

    invoke-interface {p4, v5, v2, v6}, Lcom/android/vending/licensing/ILicenseResultListener;->verifyLicense(ILjava/lang/String;Ljava/lang/String;)V
    :try_end_b5
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_b5} :catch_b7

    goto/16 :goto_34

    .line 188
    .end local v0    # "god":J
    .end local v2    # "signedData":Ljava/lang/String;
    .end local v3    # "v1":Landroid/content/pm/PackageInfo;
    :catch_b7
    move-exception v4

    .line 190
    .local v4, "v5":Ljava/lang/Exception;
    const/16 v5, 0x102

    const/4 v6, 0x0

    const/4 v7, 0x0

    :try_start_bc
    invoke-interface {p4, v5, v6, v7}, Lcom/android/vending/licensing/ILicenseResultListener;->verifyLicense(ILjava/lang/String;Ljava/lang/String;)V
    :try_end_bf
    .catch Landroid/os/RemoteException; {:try_start_bc .. :try_end_bf} :catch_c1

    goto/16 :goto_34

    .line 192
    :catch_c1
    move-exception v5

    goto/16 :goto_34
.end method
