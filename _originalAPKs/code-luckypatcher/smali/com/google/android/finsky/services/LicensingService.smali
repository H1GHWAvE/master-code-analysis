.class public Lcom/google/android/finsky/services/LicensingService;
.super Landroid/app/Service;
.source "LicensingService.java"


# static fields
.field static mServiceConn:Landroid/content/ServiceConnection;


# instance fields
.field private final mBinder:Lcom/android/vending/licensing/ILicensingService$Stub;

.field public mChecker:Lcom/google/android/vending/licensing/LicenseChecker;

.field private mListener:Lcom/android/vending/licensing/ILicenseResultListener;

.field mService:Lcom/android/vending/licensing/ILicensingService;

.field mSetupDone:Z

.field public responseCode:I

.field public signature:Ljava/lang/String;

.field public signedData:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 148
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 30
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/finsky/services/LicensingService;->mSetupDone:Z

    .line 31
    const/16 v0, 0xff

    iput v0, p0, Lcom/google/android/finsky/services/LicensingService;->responseCode:I

    .line 32
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/services/LicensingService;->signature:Ljava/lang/String;

    .line 33
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/services/LicensingService;->signedData:Ljava/lang/String;

    .line 149
    new-instance v0, Lcom/google/android/finsky/services/LicensingService$2;

    invoke-direct {v0, p0}, Lcom/google/android/finsky/services/LicensingService$2;-><init>(Lcom/google/android/finsky/services/LicensingService;)V

    iput-object v0, p0, Lcom/google/android/finsky/services/LicensingService;->mBinder:Lcom/android/vending/licensing/ILicensingService$Stub;

    .line 198
    return-void
.end method

.method private cleanupService()V
    .registers 3

    .prologue
    .line 136
    iget-object v0, p0, Lcom/google/android/finsky/services/LicensingService;->mService:Lcom/android/vending/licensing/ILicensingService;

    if-eqz v0, :cond_10

    .line 138
    :try_start_4
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/google/android/finsky/services/LicensingService;->mServiceConn:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V
    :try_end_d
    .catch Ljava/lang/IllegalArgumentException; {:try_start_4 .. :try_end_d} :catch_11

    .line 144
    :goto_d
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/services/LicensingService;->mService:Lcom/android/vending/licensing/ILicensingService;

    .line 146
    :cond_10
    return-void

    .line 139
    :catch_11
    move-exception v0

    goto :goto_d
.end method


# virtual methods
.method public connectToLicensing(JLjava/lang/String;)V
    .registers 21
    .param p1, "nonce"    # J
    .param p3, "pkgName"    # Ljava/lang/String;

    .prologue
    .line 37
    new-instance v13, Lcom/google/android/finsky/services/LicensingService$1;

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move-wide/from16 v2, p1

    invoke-direct {v13, v0, v1, v2, v3}, Lcom/google/android/finsky/services/LicensingService$1;-><init>(Lcom/google/android/finsky/services/LicensingService;Ljava/lang/String;J)V

    sput-object v13, Lcom/google/android/finsky/services/LicensingService;->mServiceConn:Landroid/content/ServiceConnection;

    .line 81
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/services/LicensingService;->mService:Lcom/android/vending/licensing/ILicensingService;

    if-nez v13, :cond_d3

    .line 83
    :try_start_13
    new-instance v9, Landroid/content/Intent;

    new-instance v13, Ljava/lang/String;

    const-string v14, "Y29tLmFuZHJvaWQudmVuZGluZy5saWNlbnNpbmcuSUxpY2Vuc2luZ1NlcnZpY2U="

    .line 85
    invoke-static {v14}, Lcom/google/android/vending/licensing/util/Base64;->decode(Ljava/lang/String;)[B

    move-result-object v14

    invoke-direct {v13, v14}, Ljava/lang/String;-><init>([B)V

    invoke-direct {v9, v13}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 86
    .local v9, "intent":Landroid/content/Intent;
    const-string v13, "com.android.vending"

    invoke-virtual {v9, v13}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 87
    const-string v13, "xexe"

    const-string v14, "lp"

    invoke-virtual {v9, v13, v14}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 88
    const/4 v4, 0x0

    .line 90
    .local v4, "bindResult":Z
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getPkgMng()Landroid/content/pm/PackageManager;

    move-result-object v13

    const/4 v14, 0x0

    invoke-virtual {v13, v9, v14}, Landroid/content/pm/PackageManager;->queryIntentServices(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v13

    invoke-interface {v13}, Ljava/util/List;->isEmpty()Z

    move-result v13

    if-eqz v13, :cond_4b

    new-instance v13, Lcom/chelpus/Utils;

    const-string v14, "w"

    invoke-direct {v13, v14}, Lcom/chelpus/Utils;-><init>(Ljava/lang/String;)V

    const-wide/16 v14, 0x1388

    invoke-virtual {v13, v14, v15}, Lcom/chelpus/Utils;->waitLP(J)V

    .line 92
    :cond_4b
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getPkgMng()Landroid/content/pm/PackageManager;

    move-result-object v13

    const/4 v14, 0x0

    invoke-virtual {v13, v9, v14}, Landroid/content/pm/PackageManager;->queryIntentServices(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v13

    invoke-interface {v13}, Ljava/util/List;->isEmpty()Z

    move-result v13

    if-nez v13, :cond_b0

    .line 94
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getPkgMng()Landroid/content/pm/PackageManager;

    move-result-object v13

    const/4 v14, 0x0

    invoke-virtual {v13, v9, v14}, Landroid/content/pm/PackageManager;->queryIntentServices(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v12

    .line 95
    .local v12, "services":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v12}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :cond_67
    :goto_67
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_b0

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/content/pm/ResolveInfo;

    .line 96
    .local v11, "se":Landroid/content/pm/ResolveInfo;
    iget-object v14, v11, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    iget-object v14, v14, Landroid/content/pm/ServiceInfo;->packageName:Ljava/lang/String;

    if-eqz v14, :cond_67

    iget-object v14, v11, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    iget-object v14, v14, Landroid/content/pm/ServiceInfo;->packageName:Ljava/lang/String;

    const-string v15, "com.android.vending"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_67

    .line 97
    iget-object v14, v11, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    iget-object v10, v14, Landroid/content/pm/ServiceInfo;->packageName:Ljava/lang/String;

    .line 98
    .local v10, "packageName":Ljava/lang/String;
    iget-object v14, v11, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    iget-object v5, v14, Landroid/content/pm/ServiceInfo;->name:Ljava/lang/String;

    .line 99
    .local v5, "className":Ljava/lang/String;
    new-instance v6, Landroid/content/ComponentName;

    invoke-direct {v6, v10, v5}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    .local v6, "component":Landroid/content/ComponentName;
    new-instance v9, Landroid/content/Intent;

    .end local v9    # "intent":Landroid/content/Intent;
    invoke-direct {v9}, Landroid/content/Intent;-><init>()V

    .line 101
    .restart local v9    # "intent":Landroid/content/Intent;
    invoke-virtual {v9, v6}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 102
    const-string v14, "xexe"

    const-string v15, "lp"

    invoke-virtual {v9, v14, v15}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 103
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v14

    sget-object v15, Lcom/google/android/finsky/services/LicensingService;->mServiceConn:Landroid/content/ServiceConnection;

    const/16 v16, 0x1

    .line 104
    move/from16 v0, v16

    invoke-virtual {v14, v9, v15, v0}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v4

    goto :goto_67

    .line 110
    .end local v5    # "className":Ljava/lang/String;
    .end local v6    # "component":Landroid/content/ComponentName;
    .end local v10    # "packageName":Ljava/lang/String;
    .end local v11    # "se":Landroid/content/pm/ResolveInfo;
    .end local v12    # "services":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    :cond_b0
    if-eqz v4, :cond_d7

    .line 112
    const/4 v8, 0x0

    .line 113
    .local v8, "i":I
    :goto_b3
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/google/android/finsky/services/LicensingService;->mSetupDone:Z

    if-nez v13, :cond_c9

    .line 114
    new-instance v13, Lcom/chelpus/Utils;

    const-string v14, "w"

    invoke-direct {v13, v14}, Lcom/chelpus/Utils;-><init>(Ljava/lang/String;)V

    const-wide/16 v14, 0x7d0

    invoke-virtual {v13, v14, v15}, Lcom/chelpus/Utils;->waitLP(J)V

    .line 115
    const/16 v13, 0xa

    if-le v8, v13, :cond_d4

    .line 118
    :cond_c9
    sget-object v13, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v14, "Stop licensing"

    invoke-virtual {v13, v14}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 119
    invoke-direct/range {p0 .. p0}, Lcom/google/android/finsky/services/LicensingService;->cleanupService()V

    .line 134
    .end local v4    # "bindResult":Z
    .end local v8    # "i":I
    .end local v9    # "intent":Landroid/content/Intent;
    :cond_d3
    :goto_d3
    return-void

    .line 116
    .restart local v4    # "bindResult":Z
    .restart local v8    # "i":I
    .restart local v9    # "intent":Landroid/content/Intent;
    :cond_d4
    add-int/lit8 v8, v8, 0x1

    goto :goto_b3

    .line 121
    .end local v8    # "i":I
    :cond_d7
    invoke-direct/range {p0 .. p0}, Lcom/google/android/finsky/services/LicensingService;->cleanupService()V
    :try_end_da
    .catch Ljava/lang/SecurityException; {:try_start_13 .. :try_end_da} :catch_db
    .catch Lcom/google/android/vending/licensing/util/Base64DecoderException; {:try_start_13 .. :try_end_da} :catch_e0

    goto :goto_d3

    .line 125
    .end local v4    # "bindResult":Z
    .end local v9    # "intent":Landroid/content/Intent;
    :catch_db
    move-exception v7

    .line 127
    .local v7, "e":Ljava/lang/SecurityException;
    invoke-direct/range {p0 .. p0}, Lcom/google/android/finsky/services/LicensingService;->cleanupService()V

    goto :goto_d3

    .line 128
    .end local v7    # "e":Ljava/lang/SecurityException;
    :catch_e0
    move-exception v7

    .line 129
    .local v7, "e":Lcom/google/android/vending/licensing/util/Base64DecoderException;
    invoke-virtual {v7}, Lcom/google/android/vending/licensing/util/Base64DecoderException;->printStackTrace()V

    .line 130
    invoke-direct/range {p0 .. p0}, Lcom/google/android/finsky/services/LicensingService;->cleanupService()V

    goto :goto_d3
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .registers 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 201
    iget-object v0, p0, Lcom/google/android/finsky/services/LicensingService;->mBinder:Lcom/android/vending/licensing/ILicensingService$Stub;

    return-object v0
.end method
