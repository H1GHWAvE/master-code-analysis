.class Lcom/google/android/finsky/billing/iab/InAppBillingService$6;
.super Lcom/google/android/finsky/billing/iab/google/util/IInAppBillingService$Stub;
.source "InAppBillingService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/billing/iab/InAppBillingService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final productIDinapp:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field final productIDsubs:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/google/android/finsky/billing/iab/InAppBillingService;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/billing/iab/InAppBillingService;)V
    .registers 3
    .param p1, "this$0"    # Lcom/google/android/finsky/billing/iab/InAppBillingService;

    .prologue
    .line 322
    iput-object p1, p0, Lcom/google/android/finsky/billing/iab/InAppBillingService$6;->this$0:Lcom/google/android/finsky/billing/iab/InAppBillingService;

    invoke-direct {p0}, Lcom/google/android/finsky/billing/iab/google/util/IInAppBillingService$Stub;-><init>()V

    .line 323
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/finsky/billing/iab/InAppBillingService$6;->productIDinapp:Ljava/util/ArrayList;

    .line 324
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/finsky/billing/iab/InAppBillingService$6;->productIDsubs:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method public consumePurchase(ILjava/lang/String;Ljava/lang/String;)I
    .registers 6
    .param p1, "apiVersion"    # I
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "purchaseToken"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 606
    const-string v0, "BillingHack"

    const-string v1, "consumePurchase"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 611
    const/4 v0, 0x0

    return v0
.end method

.method public getBuyIntent(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;
    .registers 16
    .param p1, "apiVersion"    # I
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "sku"    # Ljava/lang/String;
    .param p4, "type"    # Ljava/lang/String;
    .param p5, "developerPayload"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v9, 0x0

    .line 520
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 521
    .local v1, "bundle":Landroid/os/Bundle;
    const-string v6, "RESPONSE_CODE"

    invoke-virtual {v1, v6, v9}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 524
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 525
    .local v3, "intent":Landroid/content/Intent;
    iget-object v6, p0, Lcom/google/android/finsky/billing/iab/InAppBillingService$6;->this$0:Lcom/google/android/finsky/billing/iab/InAppBillingService;

    invoke-virtual {v6}, Lcom/google/android/finsky/billing/iab/InAppBillingService;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    const-class v7, Lcom/google/android/finsky/billing/iab/BuyActivity;

    invoke-virtual {v3, v6, v7}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 526
    const-string v6, "com.google.android.finsky.billing.iab.BUY"

    invoke-virtual {v3, v6}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 527
    const/4 v6, 0x3

    invoke-virtual {v3, v6}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 529
    const-string v6, "packageName"

    invoke-virtual {v3, v6, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 530
    const-string v6, "product"

    invoke-virtual {v3, v6, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 531
    const-string v6, "payload"

    invoke-virtual {v3, v6, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 532
    const-string v6, "Type"

    invoke-virtual {v3, v6, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 533
    new-instance v2, Lcom/google/android/finsky/billing/iab/DbHelper;

    iget-object v6, p0, Lcom/google/android/finsky/billing/iab/InAppBillingService$6;->this$0:Lcom/google/android/finsky/billing/iab/InAppBillingService;

    iget-object v6, v6, Lcom/google/android/finsky/billing/iab/InAppBillingService;->mContext:Landroid/content/Context;

    invoke-direct {v2, v6, p2}, Lcom/google/android/finsky/billing/iab/DbHelper;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 534
    .local v2, "db":Lcom/google/android/finsky/billing/iab/DbHelper;
    invoke-virtual {v2}, Lcom/google/android/finsky/billing/iab/DbHelper;->getItems()Ljava/util/ArrayList;

    move-result-object v4

    .line 535
    .local v4, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/finsky/billing/iab/ItemsListItem;>;"
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-eqz v6, :cond_75

    .line 536
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_4f
    :goto_4f
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_75

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/billing/iab/ItemsListItem;

    .line 537
    .local v0, "aList":Lcom/google/android/finsky/billing/iab/ItemsListItem;
    iget-object v7, v0, Lcom/google/android/finsky/billing/iab/ItemsListItem;->pData:Ljava/lang/String;

    const-string v8, "auto.repeat.LP"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4f

    iget-object v7, v0, Lcom/google/android/finsky/billing/iab/ItemsListItem;->itemID:Ljava/lang/String;

    .line 538
    invoke-virtual {v7, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4f

    .line 539
    const-string v7, "autorepeat"

    iget-object v8, v0, Lcom/google/android/finsky/billing/iab/ItemsListItem;->pSignature:Ljava/lang/String;

    invoke-virtual {v3, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_4f

    .line 543
    .end local v0    # "aList":Lcom/google/android/finsky/billing/iab/ItemsListItem;
    :cond_75
    iget-object v6, p0, Lcom/google/android/finsky/billing/iab/InAppBillingService$6;->this$0:Lcom/google/android/finsky/billing/iab/InAppBillingService;

    invoke-virtual {v6}, Lcom/google/android/finsky/billing/iab/InAppBillingService;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    const/high16 v7, 0x8000000

    invoke-static {v6, v9, v3, v7}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v5

    .line 546
    .local v5, "pendingIntent":Landroid/app/PendingIntent;
    const-string v6, "BUY_INTENT"

    invoke-virtual {v1, v6, v5}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 548
    return-object v1
.end method

.method public getBuyIntentToReplaceSkus(ILjava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;
    .registers 13
    .param p1, "apiVersion"    # I
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "skus"    # Ljava/util/List;
    .param p4, "newSku"    # Ljava/lang/String;
    .param p5, "type"    # Ljava/lang/String;
    .param p6, "developerPayload"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 617
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v4, "LuckyPatcher: use api 5 getBuyIntentToReplaceSkus"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 618
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 619
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v3, "RESPONSE_CODE"

    invoke-virtual {v0, v3, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 622
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 623
    .local v1, "intent":Landroid/content/Intent;
    iget-object v3, p0, Lcom/google/android/finsky/billing/iab/InAppBillingService$6;->this$0:Lcom/google/android/finsky/billing/iab/InAppBillingService;

    invoke-virtual {v3}, Lcom/google/android/finsky/billing/iab/InAppBillingService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-class v4, Lcom/google/android/finsky/billing/iab/BuyActivity;

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 624
    const-string v3, "com.google.android.finsky.billing.iab.BUY"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 625
    const/4 v3, 0x3

    invoke-virtual {v1, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 627
    const-string v3, "packageName"

    invoke-virtual {v1, v3, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 628
    const-string v3, "product"

    invoke-virtual {v1, v3, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 629
    const-string v3, "payload"

    invoke-virtual {v1, v3, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 630
    const-string v3, "Type"

    invoke-virtual {v1, v3, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 631
    iget-object v3, p0, Lcom/google/android/finsky/billing/iab/InAppBillingService$6;->this$0:Lcom/google/android/finsky/billing/iab/InAppBillingService;

    invoke-virtual {v3}, Lcom/google/android/finsky/billing/iab/InAppBillingService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const/high16 v4, 0x8000000

    invoke-static {v3, v5, v1, v4}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 634
    .local v2, "pendingIntent":Landroid/app/PendingIntent;
    const-string v3, "BUY_INTENT"

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 636
    return-object v0
.end method

.method public getPurchases(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;
    .registers 15
    .param p1, "apiVersion"    # I
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "type"    # Ljava/lang/String;
    .param p4, "continuationToken"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 554
    const-string v7, "BillingHack"

    const-string v8, "getPurchases"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 574
    new-instance v3, Lcom/google/android/finsky/billing/iab/DbHelper;

    iget-object v7, p0, Lcom/google/android/finsky/billing/iab/InAppBillingService$6;->this$0:Lcom/google/android/finsky/billing/iab/InAppBillingService;

    iget-object v7, v7, Lcom/google/android/finsky/billing/iab/InAppBillingService;->mContext:Landroid/content/Context;

    invoke-direct {v3, v7, p2}, Lcom/google/android/finsky/billing/iab/DbHelper;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 575
    .local v3, "db":Lcom/google/android/finsky/billing/iab/DbHelper;
    invoke-virtual {v3}, Lcom/google/android/finsky/billing/iab/DbHelper;->getItems()Ljava/util/ArrayList;

    move-result-object v5

    .line 576
    .local v5, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/finsky/billing/iab/ItemsListItem;>;"
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 577
    .local v4, "items_id":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 578
    .local v2, "datas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 579
    .local v6, "signatures":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-eqz v7, :cond_67

    .line 580
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_2d
    :goto_2d
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_67

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/billing/iab/ItemsListItem;

    .line 581
    .local v0, "aList":Lcom/google/android/finsky/billing/iab/ItemsListItem;
    iget-object v8, v0, Lcom/google/android/finsky/billing/iab/ItemsListItem;->pData:Ljava/lang/String;

    const-string v9, "auto.repeat.LP"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_2d

    .line 582
    iget-object v8, v0, Lcom/google/android/finsky/billing/iab/ItemsListItem;->itemID:Ljava/lang/String;

    invoke-virtual {v4, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 587
    iget-object v8, v0, Lcom/google/android/finsky/billing/iab/ItemsListItem;->pData:Ljava/lang/String;

    invoke-virtual {v2, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 588
    iget-object v8, v0, Lcom/google/android/finsky/billing/iab/ItemsListItem;->pSignature:Ljava/lang/String;

    const-string v9, "1"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_61

    .line 589
    iget-object v8, v0, Lcom/google/android/finsky/billing/iab/ItemsListItem;->pData:Ljava/lang/String;

    invoke-static {v8}, Lcom/chelpus/Utils;->gen_sha1withrsa(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2d

    .line 590
    :cond_61
    const-string v8, ""

    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2d

    .line 594
    .end local v0    # "aList":Lcom/google/android/finsky/billing/iab/ItemsListItem;
    :cond_67
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 595
    .local v1, "bundle":Landroid/os/Bundle;
    const-string v7, "RESPONSE_CODE"

    const/4 v8, 0x0

    invoke-virtual {v1, v7, v8}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 596
    const-string v7, "INAPP_PURCHASE_ITEM_LIST"

    invoke-virtual {v1, v7, v4}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 597
    const-string v7, "INAPP_PURCHASE_DATA_LIST"

    invoke-virtual {v1, v7, v2}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 598
    const-string v7, "INAPP_DATA_SIGNATURE_LIST"

    invoke-virtual {v1, v7, v6}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 600
    return-object v1
.end method

.method public getSkuDetails(ILjava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    .registers 35
    .param p1, "apiVersion"    # I
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "type"    # Ljava/lang/String;
    .param p4, "skusBundle"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 341
    const-string v26, "BillingHack"

    const-string v27, "getSkuDetails"

    invoke-static/range {v26 .. v27}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 349
    :try_start_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/billing/iab/InAppBillingService$6;->this$0:Lcom/google/android/finsky/billing/iab/InAppBillingService;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget-boolean v0, v0, Lcom/google/android/finsky/billing/iab/InAppBillingService;->mSetupDone:Z

    move/from16 v26, v0

    if-nez v26, :cond_30

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/billing/iab/InAppBillingService$6;->this$0:Lcom/google/android/finsky/billing/iab/InAppBillingService;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget-boolean v0, v0, Lcom/google/android/finsky/billing/iab/InAppBillingService;->skipSetupDone:Z

    move/from16 v26, v0

    if-nez v26, :cond_30

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/billing/iab/InAppBillingService$6;->this$0:Lcom/google/android/finsky/billing/iab/InAppBillingService;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/billing/iab/InAppBillingService;->connectToBilling(Ljava/lang/String;)V

    .line 350
    :cond_30
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 351
    .local v6, "bundle":Landroid/os/Bundle;
    const-string v26, "RESPONSE_CODE"

    const/16 v27, 0x0

    move-object/from16 v0, v26

    move/from16 v1, v27

    invoke-virtual {v6, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 353
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/billing/iab/InAppBillingService$6;->this$0:Lcom/google/android/finsky/billing/iab/InAppBillingService;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget-boolean v0, v0, Lcom/google/android/finsky/billing/iab/InAppBillingService;->mSetupDone:Z

    move/from16 v26, v0

    if-eqz v26, :cond_be

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/billing/iab/InAppBillingService$6;->this$0:Lcom/google/android/finsky/billing/iab/InAppBillingService;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/google/android/finsky/billing/iab/InAppBillingService;->mService:Lcom/google/android/finsky/billing/iab/google/util/IInAppBillingService;

    move-object/from16 v26, v0

    if-eqz v26, :cond_be

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/billing/iab/InAppBillingService$6;->this$0:Lcom/google/android/finsky/billing/iab/InAppBillingService;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget-boolean v0, v0, Lcom/google/android/finsky/billing/iab/InAppBillingService;->skipSetupDone:Z

    move/from16 v26, v0
    :try_end_68
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_68} :catch_1ad

    if-nez v26, :cond_be

    .line 355
    :try_start_6a
    sget-object v26, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v27, "Connect to google billing"

    invoke-virtual/range {v26 .. v27}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 356
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/billing/iab/InAppBillingService$6;->this$0:Lcom/google/android/finsky/billing/iab/InAppBillingService;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/google/android/finsky/billing/iab/InAppBillingService;->mService:Lcom/google/android/finsky/billing/iab/google/util/IInAppBillingService;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    move/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/google/android/finsky/billing/iab/google/util/IInAppBillingService;->getSkuDetails(ILjava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v14

    .line 358
    .local v14, "itemsList":Landroid/os/Bundle;
    const-string v26, "DETAILS_LIST"

    move-object/from16 v0, v26

    invoke-virtual {v14, v0}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v21

    .line 359
    .local v21, "res_skus":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v26, "ITEM_ID_LIST"

    move-object/from16 v0, p4

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v24

    .line 360
    .local v24, "skus":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v26, "RESPONSE_CODE"

    move-object/from16 v0, v26

    invoke-virtual {v14, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v22

    .line 361
    .local v22, "response":I
    sget-object v26, Ljava/lang/System;->out:Ljava/io/PrintStream;

    move-object/from16 v0, v26

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(I)V

    .line 362
    if-eqz v22, :cond_13b

    .line 363
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/billing/iab/InAppBillingService$6;->this$0:Lcom/google/android/finsky/billing/iab/InAppBillingService;

    move-object/from16 v26, v0

    const/16 v27, 0x1

    move/from16 v0, v27

    move-object/from16 v1, v26

    iput-boolean v0, v1, Lcom/google/android/finsky/billing/iab/InAppBillingService;->skipSetupDone:Z
    :try_end_be
    .catch Ljava/lang/Exception; {:try_start_6a .. :try_end_be} :catch_1a7

    .line 391
    .end local v6    # "bundle":Landroid/os/Bundle;
    .end local v14    # "itemsList":Landroid/os/Bundle;
    .end local v21    # "res_skus":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v22    # "response":I
    .end local v24    # "skus":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_be
    :goto_be
    const-string v26, "ITEM_ID_LIST"

    move-object/from16 v0, p4

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v24

    .line 392
    .restart local v24    # "skus":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    .line 393
    .local v15, "itemsList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/billing/iab/InAppBillingService$6;->this$0:Lcom/google/android/finsky/billing/iab/InAppBillingService;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget-boolean v0, v0, Lcom/google/android/finsky/billing/iab/InAppBillingService;->mSetupDone:Z

    move/from16 v26, v0

    if-eqz v26, :cond_f7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/billing/iab/InAppBillingService$6;->this$0:Lcom/google/android/finsky/billing/iab/InAppBillingService;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/google/android/finsky/billing/iab/InAppBillingService;->mService:Lcom/google/android/finsky/billing/iab/google/util/IInAppBillingService;

    move-object/from16 v26, v0

    if-eqz v26, :cond_f7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/billing/iab/InAppBillingService$6;->this$0:Lcom/google/android/finsky/billing/iab/InAppBillingService;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget-boolean v0, v0, Lcom/google/android/finsky/billing/iab/InAppBillingService;->skipSetupDone:Z

    move/from16 v26, v0

    if-eqz v26, :cond_3fb

    .line 394
    :cond_f7
    sget-object v26, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v27, "Dont Connect to google billing"

    invoke-virtual/range {v26 .. v27}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 395
    invoke-virtual/range {v24 .. v24}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v26

    :cond_102
    :goto_102
    invoke-interface/range {v26 .. v26}, Ljava/util/Iterator;->hasNext()Z

    move-result v27

    if-eqz v27, :cond_200

    invoke-interface/range {v26 .. v26}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Ljava/lang/String;

    .line 396
    .local v23, "sku":Ljava/lang/String;
    const-string v27, "subs"

    move-object/from16 v0, p3

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_1c2

    .line 397
    const/4 v9, 0x0

    .line 398
    .local v9, "found":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/billing/iab/InAppBillingService$6;->productIDsubs:Ljava/util/ArrayList;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v27

    :cond_125
    :goto_125
    invoke-interface/range {v27 .. v27}, Ljava/util/Iterator;->hasNext()Z

    move-result v28

    if-eqz v28, :cond_1b3

    invoke-interface/range {v27 .. v27}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    .line 399
    .local v10, "inapp":Ljava/lang/String;
    move-object/from16 v0, v23

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_125

    const/4 v9, 0x1

    goto :goto_125

    .line 365
    .end local v9    # "found":Z
    .end local v10    # "inapp":Ljava/lang/String;
    .end local v15    # "itemsList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v23    # "sku":Ljava/lang/String;
    .restart local v6    # "bundle":Landroid/os/Bundle;
    .restart local v14    # "itemsList":Landroid/os/Bundle;
    .restart local v21    # "res_skus":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v22    # "response":I
    :cond_13b
    if-eqz v21, :cond_413

    :try_start_13d
    invoke-virtual/range {v21 .. v21}, Ljava/util/ArrayList;->size()I

    move-result v26

    if-eqz v26, :cond_413

    .line 366
    invoke-virtual/range {v21 .. v21}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v26

    :cond_147
    :goto_147
    invoke-interface/range {v26 .. v26}, Ljava/util/Iterator;->hasNext()Z

    move-result v27

    if-eqz v27, :cond_413

    invoke-interface/range {v26 .. v26}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Ljava/lang/String;

    .line 367
    .local v20, "res_sku":Ljava/lang/String;
    new-instance v12, Lorg/json/JSONObject;

    move-object/from16 v0, v20

    invoke-direct {v12, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 368
    .local v12, "item":Lorg/json/JSONObject;
    const-string v27, "type"

    move-object/from16 v0, v27

    invoke-virtual {v12, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v27

    const-string v28, "inapp"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_147

    .line 369
    const/4 v9, 0x0

    .line 370
    .restart local v9    # "found":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/billing/iab/InAppBillingService$6;->productIDinapp:Ljava/util/ArrayList;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v27

    :cond_175
    :goto_175
    invoke-interface/range {v27 .. v27}, Ljava/util/Iterator;->hasNext()Z

    move-result v28

    if-eqz v28, :cond_193

    invoke-interface/range {v27 .. v27}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    .line 371
    .restart local v10    # "inapp":Ljava/lang/String;
    const-string v28, "productId"

    move-object/from16 v0, v28

    invoke-virtual {v12, v0}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v28

    move-object/from16 v0, v28

    invoke-virtual {v0, v10}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_175

    const/4 v9, 0x1

    goto :goto_175

    .line 373
    .end local v10    # "inapp":Ljava/lang/String;
    :cond_193
    if-nez v9, :cond_147

    .line 376
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/billing/iab/InAppBillingService$6;->productIDinapp:Ljava/util/ArrayList;

    move-object/from16 v27, v0

    const-string v28, "productId"

    move-object/from16 v0, v28

    invoke-virtual {v12, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1a6
    .catch Ljava/lang/Exception; {:try_start_13d .. :try_end_1a6} :catch_1a7

    goto :goto_147

    .line 383
    .end local v9    # "found":Z
    .end local v12    # "item":Lorg/json/JSONObject;
    .end local v14    # "itemsList":Landroid/os/Bundle;
    .end local v20    # "res_sku":Ljava/lang/String;
    .end local v21    # "res_skus":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v22    # "response":I
    .end local v24    # "skus":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :catch_1a7
    move-exception v8

    .local v8, "e":Ljava/lang/Exception;
    :try_start_1a8
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1ab
    .catch Ljava/lang/Exception; {:try_start_1a8 .. :try_end_1ab} :catch_1ad

    goto/16 :goto_be

    .line 385
    .end local v6    # "bundle":Landroid/os/Bundle;
    .end local v8    # "e":Ljava/lang/Exception;
    :catch_1ad
    move-exception v8

    .line 386
    .restart local v8    # "e":Ljava/lang/Exception;
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_be

    .line 401
    .end local v8    # "e":Ljava/lang/Exception;
    .restart local v9    # "found":Z
    .restart local v15    # "itemsList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v23    # "sku":Ljava/lang/String;
    .restart local v24    # "skus":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_1b3
    if-nez v9, :cond_1c2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/billing/iab/InAppBillingService$6;->productIDsubs:Ljava/util/ArrayList;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 403
    .end local v9    # "found":Z
    :cond_1c2
    const-string v27, "inapp"

    move-object/from16 v0, p3

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_102

    .line 404
    const/4 v9, 0x0

    .line 405
    .restart local v9    # "found":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/billing/iab/InAppBillingService$6;->productIDinapp:Ljava/util/ArrayList;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v27

    :cond_1d9
    :goto_1d9
    invoke-interface/range {v27 .. v27}, Ljava/util/Iterator;->hasNext()Z

    move-result v28

    if-eqz v28, :cond_1ef

    invoke-interface/range {v27 .. v27}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    .line 406
    .restart local v10    # "inapp":Ljava/lang/String;
    move-object/from16 v0, v23

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_1d9

    const/4 v9, 0x1

    goto :goto_1d9

    .line 408
    .end local v10    # "inapp":Ljava/lang/String;
    :cond_1ef
    if-nez v9, :cond_102

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/billing/iab/InAppBillingService$6;->productIDinapp:Ljava/util/ArrayList;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_102

    .line 411
    .end local v9    # "found":Z
    .end local v23    # "sku":Ljava/lang/String;
    :cond_200
    invoke-virtual/range {v24 .. v24}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v26

    :goto_204
    invoke-interface/range {v26 .. v26}, Ljava/util/Iterator;->hasNext()Z

    move-result v27

    if-eqz v27, :cond_3fb

    invoke-interface/range {v26 .. v26}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Ljava/lang/String;

    .line 413
    .restart local v23    # "sku":Ljava/lang/String;
    const-string v27, "\\."

    move-object/from16 v0, v23

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v25

    .line 414
    .local v25, "w":[Ljava/lang/String;
    const-string v13, ""

    .line 415
    .local v13, "item_name":Ljava/lang/String;
    if-eqz v25, :cond_2c7

    move-object/from16 v0, v25

    array-length v0, v0

    move/from16 v27, v0

    if-eqz v27, :cond_2c7

    .line 416
    move-object/from16 v0, v25

    array-length v0, v0

    move/from16 v27, v0

    const/16 v28, 0x2

    move/from16 v0, v27

    move/from16 v1, v28

    if-lt v0, v1, :cond_25b

    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v25

    array-length v0, v0

    move/from16 v28, v0

    add-int/lit8 v28, v28, -0x2

    aget-object v28, v25, v28

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, " "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v25

    array-length v0, v0

    move/from16 v28, v0

    add-int/lit8 v28, v28, -0x1

    aget-object v28, v25, v28

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    .line 417
    :cond_25b
    move-object/from16 v0, v25

    array-length v0, v0

    move/from16 v27, v0

    const/16 v28, 0x1

    move/from16 v0, v27

    move/from16 v1, v28

    if-ne v0, v1, :cond_274

    .line 418
    const/16 v27, 0x0

    aget-object v27, v25, v27

    const-string v28, "_"

    const-string v29, " "

    invoke-virtual/range {v27 .. v29}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 424
    :cond_274
    :goto_274
    const-wide/16 v16, 0x0

    .line 425
    .local v16, "micro_price":J
    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string v28, "$0."

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    move-wide/from16 v1, v16

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    .line 426
    .local v18, "price":Ljava/lang/String;
    new-instance v7, Lcom/google/android/finsky/billing/iab/DbHelper;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/billing/iab/InAppBillingService$6;->this$0:Lcom/google/android/finsky/billing/iab/InAppBillingService;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    iget-object v0, v0, Lcom/google/android/finsky/billing/iab/InAppBillingService;->mContext:Landroid/content/Context;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    move-object/from16 v1, p2

    invoke-direct {v7, v0, v1}, Lcom/google/android/finsky/billing/iab/DbHelper;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 427
    .local v7, "db":Lcom/google/android/finsky/billing/iab/DbHelper;
    invoke-virtual {v7}, Lcom/google/android/finsky/billing/iab/DbHelper;->getItems()Ljava/util/ArrayList;

    move-result-object v5

    .line 428
    .local v5, "all":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/finsky/billing/iab/ItemsListItem;>;"
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v27

    :cond_2aa
    :goto_2aa
    invoke-interface/range {v27 .. v27}, Ljava/util/Iterator;->hasNext()Z

    move-result v28

    if-eqz v28, :cond_2d6

    invoke-interface/range {v27 .. v27}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/google/android/finsky/billing/iab/ItemsListItem;

    .line 430
    .local v11, "it":Lcom/google/android/finsky/billing/iab/ItemsListItem;
    iget-object v0, v11, Lcom/google/android/finsky/billing/iab/ItemsListItem;->itemID:Ljava/lang/String;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_2aa

    const-string v18, "Purchased"

    goto :goto_2aa

    .line 422
    .end local v5    # "all":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/finsky/billing/iab/ItemsListItem;>;"
    .end local v7    # "db":Lcom/google/android/finsky/billing/iab/DbHelper;
    .end local v11    # "it":Lcom/google/android/finsky/billing/iab/ItemsListItem;
    .end local v16    # "micro_price":J
    .end local v18    # "price":Ljava/lang/String;
    :cond_2c7
    const-string v27, "_"

    const-string v28, " "

    move-object/from16 v0, v23

    move-object/from16 v1, v27

    move-object/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    goto :goto_274

    .line 435
    .restart local v5    # "all":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/finsky/billing/iab/ItemsListItem;>;"
    .restart local v7    # "db":Lcom/google/android/finsky/billing/iab/DbHelper;
    .restart local v16    # "micro_price":J
    .restart local v18    # "price":Ljava/lang/String;
    :cond_2d6
    const-string v27, "subs"

    move-object/from16 v0, p3

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_396

    .line 437
    const/4 v9, 0x0

    .line 438
    .restart local v9    # "found":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/billing/iab/InAppBillingService$6;->productIDinapp:Ljava/util/ArrayList;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v27

    :cond_2ed
    :goto_2ed
    invoke-interface/range {v27 .. v27}, Ljava/util/Iterator;->hasNext()Z

    move-result v28

    if-eqz v28, :cond_303

    invoke-interface/range {v27 .. v27}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    .line 440
    .restart local v10    # "inapp":Ljava/lang/String;
    move-object/from16 v0, v23

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_2ed

    const/4 v9, 0x1

    goto :goto_2ed

    .line 442
    .end local v10    # "inapp":Ljava/lang/String;
    :cond_303
    if-nez v9, :cond_36a

    .line 443
    new-instance v19, Lorg/json/JSONObject;

    invoke-direct/range {v19 .. v19}, Lorg/json/JSONObject;-><init>()V

    .line 445
    .local v19, "purch":Lorg/json/JSONObject;
    :try_start_30a
    const-string v27, "productId"

    move-object/from16 v0, v19

    move-object/from16 v1, v27

    move-object/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 446
    const-string v27, "type"

    move-object/from16 v0, v19

    move-object/from16 v1, v27

    move-object/from16 v2, p3

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 447
    const-string v27, "price"

    move-object/from16 v0, v19

    move-object/from16 v1, v27

    move-object/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 448
    const-string v27, "title"

    move-object/from16 v0, v19

    move-object/from16 v1, v27

    invoke-virtual {v0, v1, v13}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 449
    const-string v27, "description"

    move-object/from16 v0, v19

    move-object/from16 v1, v27

    invoke-virtual {v0, v1, v13}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 450
    const-string v27, "price_amount_micros"

    const-wide/32 v28, 0xf4240

    mul-long v28, v28, v16

    move-object/from16 v0, v19

    move-object/from16 v1, v27

    move-wide/from16 v2, v28

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 451
    const-string v27, "price_currency_code"

    const-string v28, "USD"

    move-object/from16 v0, v19

    move-object/from16 v1, v27

    move-object/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_35a
    .catch Lorg/json/JSONException; {:try_start_30a .. :try_end_35a} :catch_365

    .line 458
    :goto_35a
    invoke-virtual/range {v19 .. v19}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, v27

    invoke-virtual {v15, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_204

    .line 452
    :catch_365
    move-exception v8

    .line 453
    .local v8, "e":Lorg/json/JSONException;
    invoke-virtual {v8}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_35a

    .line 460
    .end local v8    # "e":Lorg/json/JSONException;
    .end local v19    # "purch":Lorg/json/JSONObject;
    :cond_36a
    sget-object v27, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    const-string v29, "skip "

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    const-string v29, " "

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_204

    .line 464
    .end local v9    # "found":Z
    :cond_396
    new-instance v19, Lorg/json/JSONObject;

    invoke-direct/range {v19 .. v19}, Lorg/json/JSONObject;-><init>()V

    .line 466
    .restart local v19    # "purch":Lorg/json/JSONObject;
    :try_start_39b
    const-string v27, "productId"

    move-object/from16 v0, v19

    move-object/from16 v1, v27

    move-object/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 467
    const-string v27, "type"

    move-object/from16 v0, v19

    move-object/from16 v1, v27

    move-object/from16 v2, p3

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 468
    const-string v27, "price"

    move-object/from16 v0, v19

    move-object/from16 v1, v27

    move-object/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 469
    const-string v27, "title"

    move-object/from16 v0, v19

    move-object/from16 v1, v27

    invoke-virtual {v0, v1, v13}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 470
    const-string v27, "description"

    move-object/from16 v0, v19

    move-object/from16 v1, v27

    invoke-virtual {v0, v1, v13}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 471
    const-string v27, "price_amount_micros"

    const-wide/32 v28, 0xf4240

    mul-long v28, v28, v16

    move-object/from16 v0, v19

    move-object/from16 v1, v27

    move-wide/from16 v2, v28

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 472
    const-string v27, "price_currency_code"

    const-string v28, "USD"

    move-object/from16 v0, v19

    move-object/from16 v1, v27

    move-object/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_3eb
    .catch Lorg/json/JSONException; {:try_start_39b .. :try_end_3eb} :catch_3f6

    .line 479
    :goto_3eb
    invoke-virtual/range {v19 .. v19}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, v27

    invoke-virtual {v15, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_204

    .line 473
    :catch_3f6
    move-exception v8

    .line 474
    .restart local v8    # "e":Lorg/json/JSONException;
    invoke-virtual {v8}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_3eb

    .line 485
    .end local v5    # "all":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/finsky/billing/iab/ItemsListItem;>;"
    .end local v7    # "db":Lcom/google/android/finsky/billing/iab/DbHelper;
    .end local v8    # "e":Lorg/json/JSONException;
    .end local v13    # "item_name":Ljava/lang/String;
    .end local v16    # "micro_price":J
    .end local v18    # "price":Ljava/lang/String;
    .end local v19    # "purch":Lorg/json/JSONObject;
    .end local v23    # "sku":Ljava/lang/String;
    .end local v25    # "w":[Ljava/lang/String;
    :cond_3fb
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 486
    .restart local v6    # "bundle":Landroid/os/Bundle;
    const-string v26, "RESPONSE_CODE"

    const/16 v27, 0x0

    move-object/from16 v0, v26

    move/from16 v1, v27

    invoke-virtual {v6, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 487
    const-string v26, "DETAILS_LIST"

    move-object/from16 v0, v26

    invoke-virtual {v6, v0, v15}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    move-object v14, v6

    .line 489
    .end local v15    # "itemsList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_413
    return-object v14
.end method

.method public isBillingSupported(ILjava/lang/String;Ljava/lang/String;)I
    .registers 6
    .param p1, "apiVersion"    # I
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "type"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 330
    const-string v0, "BillingHack"

    const-string v1, "isBillingSupported"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 333
    iget-object v0, p0, Lcom/google/android/finsky/billing/iab/InAppBillingService$6;->this$0:Lcom/google/android/finsky/billing/iab/InAppBillingService;

    invoke-virtual {v0}, Lcom/google/android/finsky/billing/iab/InAppBillingService;->startGoogleBilling()V

    .line 335
    const/4 v0, 0x0

    return v0
.end method
