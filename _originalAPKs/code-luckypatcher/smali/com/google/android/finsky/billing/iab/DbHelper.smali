.class public Lcom/google/android/finsky/billing/iab/DbHelper;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "DbHelper.java"


# static fields
.field public static contextdb:Landroid/content/Context; = null

.field static final dbName:Ljava/lang/String; = "BillingRestoreTransactions"

.field public static getPackage:Z = false

.field static final itemID:Ljava/lang/String; = "itemID"

.field static final pData:Ljava/lang/String; = "Data"

.field static final pSignature:Ljava/lang/String; = "Signature"

.field static packageTable:Ljava/lang/String;

.field public static savePackage:Z


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    const/4 v1, 0x0

    .line 20
    const-string v0, "Packages"

    sput-object v0, Lcom/google/android/finsky/billing/iab/DbHelper;->packageTable:Ljava/lang/String;

    .line 25
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/finsky/billing/iab/DbHelper;->contextdb:Landroid/content/Context;

    .line 27
    sput-boolean v1, Lcom/google/android/finsky/billing/iab/DbHelper;->getPackage:Z

    .line 28
    sput-boolean v1, Lcom/google/android/finsky/billing/iab/DbHelper;->savePackage:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 7
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/16 v4, 0x29

    .line 60
    const-string v1, "BillingRestoreTransactions"

    const/4 v2, 0x0

    invoke-direct {p0, p1, v1, v2, v4}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 61
    sput-object p1, Lcom/google/android/finsky/billing/iab/DbHelper;->contextdb:Landroid/content/Context;

    .line 62
    sget-object v1, Lcom/google/android/finsky/billing/iab/DbHelper;->packageTable:Ljava/lang/String;

    sput-object v1, Lcom/google/android/finsky/billing/iab/DbHelper;->packageTable:Ljava/lang/String;

    .line 64
    :try_start_e
    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->billing_db:Landroid/database/sqlite/SQLiteDatabase;

    if-nez v1, :cond_6a

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/iab/DbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    sput-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->billing_db:Landroid/database/sqlite/SQLiteDatabase;

    .line 69
    :goto_18
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SQLite base version is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->billing_db:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->getVersion()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 70
    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->billing_db:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->getVersion()I

    move-result v1

    if-eq v1, v4, :cond_64

    .line 71
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, "SQL delete and recreate."

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 72
    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->billing_db:Landroid/database/sqlite/SQLiteDatabase;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "DROP TABLE IF EXISTS "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/google/android/finsky/billing/iab/DbHelper;->packageTable:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 73
    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->billing_db:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/billing/iab/DbHelper;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 75
    :cond_64
    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->billing_db:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/billing/iab/DbHelper;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 84
    :goto_69
    return-void

    .line 66
    :cond_6a
    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->billing_db:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 67
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/iab/DbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    sput-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->billing_db:Landroid/database/sqlite/SQLiteDatabase;
    :try_end_75
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_e .. :try_end_75} :catch_76

    goto :goto_18

    .line 76
    :catch_76
    move-exception v0

    .line 77
    .local v0, "e":Landroid/database/sqlite/SQLiteException;
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    goto :goto_69
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .registers 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "packageTable"    # Ljava/lang/String;

    .prologue
    const/16 v3, 0x29

    .line 32
    const-string v1, "BillingRestoreTransactions"

    const/4 v2, 0x0

    invoke-direct {p0, p1, v1, v2, v3}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 33
    sput-object p1, Lcom/google/android/finsky/billing/iab/DbHelper;->contextdb:Landroid/content/Context;

    .line 35
    sput-object p2, Lcom/google/android/finsky/billing/iab/DbHelper;->packageTable:Ljava/lang/String;

    .line 37
    :try_start_c
    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->billing_db:Landroid/database/sqlite/SQLiteDatabase;

    if-nez v1, :cond_4c

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/iab/DbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    sput-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->billing_db:Landroid/database/sqlite/SQLiteDatabase;

    .line 42
    :goto_16
    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->billing_db:Landroid/database/sqlite/SQLiteDatabase;

    sput-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->billing_db:Landroid/database/sqlite/SQLiteDatabase;

    .line 44
    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->billing_db:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->getVersion()I

    move-result v1

    if-eq v1, v3, :cond_46

    .line 45
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, "SQL delete and recreate."

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 46
    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->billing_db:Landroid/database/sqlite/SQLiteDatabase;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "DROP TABLE IF EXISTS "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 47
    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->billing_db:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/billing/iab/DbHelper;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 49
    :cond_46
    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->billing_db:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/billing/iab/DbHelper;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 58
    :goto_4b
    return-void

    .line 39
    :cond_4c
    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->billing_db:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 40
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/iab/DbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    sput-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->billing_db:Landroid/database/sqlite/SQLiteDatabase;
    :try_end_57
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_c .. :try_end_57} :catch_58

    goto :goto_16

    .line 50
    :catch_58
    move-exception v0

    .line 51
    .local v0, "e":Landroid/database/sqlite/SQLiteException;
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    goto :goto_4b
.end method


# virtual methods
.method public deleteAll()V
    .registers 1

    .prologue
    .line 231
    return-void
.end method

.method public deleteItem(Lcom/google/android/finsky/billing/iab/ItemsListItem;)V
    .registers 7
    .param p1, "deleteObject"    # Lcom/google/android/finsky/billing/iab/ItemsListItem;

    .prologue
    .line 203
    :try_start_0
    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->billing_db:Landroid/database/sqlite/SQLiteDatabase;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/google/android/finsky/billing/iab/DbHelper;->packageTable:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "itemID = \'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p1, Lcom/google/android/finsky/billing/iab/ItemsListItem;->itemID:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_3c
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_3c} :catch_3d

    .line 210
    :goto_3c
    return-void

    .line 205
    :catch_3d
    move-exception v0

    .line 207
    .local v0, "e":Ljava/lang/Exception;
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "LuckyPatcher-Error: deletePackage "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_3c
.end method

.method public deleteItem(Ljava/lang/String;)V
    .registers 7
    .param p1, "itemId"    # Ljava/lang/String;

    .prologue
    .line 215
    :try_start_0
    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->billing_db:Landroid/database/sqlite/SQLiteDatabase;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/google/android/finsky/billing/iab/DbHelper;->packageTable:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "itemID = \'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_3a
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_3a} :catch_3b

    .line 222
    :goto_3a
    return-void

    .line 217
    :catch_3b
    move-exception v0

    .line 219
    .local v0, "e":Ljava/lang/Exception;
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "LuckyPatcher-Error: deletePackage "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_3a
.end method

.method public getItems()Ljava/util/ArrayList;
    .registers 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/finsky/billing/iab/ItemsListItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 104
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 105
    .local v14, "pat":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/finsky/billing/iab/ItemsListItem;>;"
    const/4 v11, 0x0

    .line 106
    .local v11, "enable":Z
    invoke-virtual {v14}, Ljava/util/ArrayList;->clear()V

    .line 122
    :try_start_9
    sget-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->billing_db:Landroid/database/sqlite/SQLiteDatabase;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/google/android/finsky/billing/iab/DbHelper;->packageTable:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "itemID"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "Data"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "Signature"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 123
    .local v8, "c":Landroid/database/Cursor;
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_44
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_44} :catch_7c

    .line 127
    :cond_44
    :try_start_44
    const-string v0, "itemID"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_4d
    .catch Ljava/lang/Exception; {:try_start_44 .. :try_end_4d} :catch_99

    move-result-object v13

    .line 130
    .local v13, "itemId":Ljava/lang/String;
    :try_start_4e
    const-string v0, "Data"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 131
    .local v9, "data":Ljava/lang/String;
    const-string v0, "Signature"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 135
    .local v15, "signature":Ljava/lang/String;
    new-instance v12, Lcom/google/android/finsky/billing/iab/ItemsListItem;

    invoke-direct {v12, v13, v9, v15}, Lcom/google/android/finsky/billing/iab/ItemsListItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    .local v12, "item":Lcom/google/android/finsky/billing/iab/ItemsListItem;
    invoke-virtual {v14, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_6a
    .catch Ljava/lang/IllegalArgumentException; {:try_start_4e .. :try_end_6a} :catch_9b
    .catch Ljava/lang/Exception; {:try_start_4e .. :try_end_6a} :catch_99

    .line 143
    .end local v9    # "data":Ljava/lang/String;
    .end local v12    # "item":Lcom/google/android/finsky/billing/iab/ItemsListItem;
    .end local v13    # "itemId":Ljava/lang/String;
    .end local v15    # "signature":Ljava/lang/String;
    :goto_6a
    :try_start_6a
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_44

    .line 144
    invoke-interface {v8}, Landroid/database/Cursor;->close()V
    :try_end_73
    .catch Ljava/lang/Exception; {:try_start_6a .. :try_end_73} :catch_77

    .line 154
    :goto_73
    const/4 v0, 0x0

    :try_start_74
    sput-boolean v0, Lcom/google/android/finsky/billing/iab/DbHelper;->getPackage:Z

    .line 160
    .end local v8    # "c":Landroid/database/Cursor;
    :goto_76
    return-object v14

    .line 147
    .restart local v8    # "c":Landroid/database/Cursor;
    :catch_77
    move-exception v10

    .line 148
    .local v10, "e":Ljava/lang/Exception;
    invoke-interface {v8}, Landroid/database/Cursor;->close()V
    :try_end_7b
    .catch Ljava/lang/Exception; {:try_start_74 .. :try_end_7b} :catch_7c

    goto :goto_73

    .line 155
    .end local v8    # "c":Landroid/database/Cursor;
    .end local v10    # "e":Ljava/lang/Exception;
    :catch_7c
    move-exception v10

    .line 156
    .restart local v10    # "e":Ljava/lang/Exception;
    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/finsky/billing/iab/DbHelper;->getPackage:Z

    .line 158
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "LuckyPatcher-Error: getPackage "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_76

    .line 140
    .end local v10    # "e":Ljava/lang/Exception;
    .restart local v8    # "c":Landroid/database/Cursor;
    :catch_99
    move-exception v0

    goto :goto_6a

    .line 138
    .restart local v13    # "itemId":Ljava/lang/String;
    :catch_9b
    move-exception v0

    goto :goto_6a
.end method

.method public isOpen()Z
    .registers 2

    .prologue
    .line 238
    sget-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->billing_db:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->isOpen()Z

    move-result v0

    if-eqz v0, :cond_a

    const/4 v0, 0x1

    .line 239
    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .registers 4
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 88
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CREATE TABLE IF NOT EXISTS \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/google/android/finsky/billing/iab/DbHelper;->packageTable:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\' ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "itemID"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " TEXT PRIMARY KEY, "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "Data"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " TEXT, "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "Signature"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " TEXT"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ");"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 92
    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .registers 6
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "oldVersion"    # I
    .param p3, "newVersion"    # I

    .prologue
    .line 98
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DROP TABLE IF EXISTS \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/google/android/finsky/billing/iab/DbHelper;->packageTable:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 100
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/billing/iab/DbHelper;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 101
    return-void
.end method

.method public saveItem(Lcom/google/android/finsky/billing/iab/ItemsListItem;)V
    .registers 8
    .param p1, "savedObject"    # Lcom/google/android/finsky/billing/iab/ItemsListItem;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/database/sqlite/SQLiteException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 175
    const/4 v2, 0x1

    :try_start_2
    sput-boolean v2, Lcom/google/android/finsky/billing/iab/DbHelper;->savePackage:Z

    .line 177
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 179
    .local v0, "cv":Landroid/content/ContentValues;
    const-string v2, "itemID"

    iget-object v3, p1, Lcom/google/android/finsky/billing/iab/ItemsListItem;->itemID:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    const-string v2, "Data"

    iget-object v3, p1, Lcom/google/android/finsky/billing/iab/ItemsListItem;->pData:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 181
    const-string v2, "Signature"

    iget-object v3, p1, Lcom/google/android/finsky/billing/iab/ItemsListItem;->pSignature:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1e
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_1e} :catch_6a

    .line 185
    :try_start_1e
    sget-object v2, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->billing_db:Landroid/database/sqlite/SQLiteDatabase;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/google/android/finsky/billing/iab/DbHelper;->packageTable:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "itemID"

    invoke-virtual {v2, v3, v4, v0}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_40
    .catch Ljava/lang/Exception; {:try_start_1e .. :try_end_40} :catch_47

    .line 190
    :goto_40
    const/4 v2, 0x0

    :try_start_41
    sput-boolean v2, Lcom/google/android/finsky/billing/iab/DbHelper;->savePackage:Z

    .line 192
    const/4 v2, 0x0

    sput-boolean v2, Lcom/google/android/finsky/billing/iab/DbHelper;->savePackage:Z

    .line 198
    .end local v0    # "cv":Landroid/content/ContentValues;
    :goto_46
    return-void

    .line 186
    .restart local v0    # "cv":Landroid/content/ContentValues;
    :catch_47
    move-exception v1

    .line 188
    .local v1, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->billing_db:Landroid/database/sqlite/SQLiteDatabase;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/google/android/finsky/billing/iab/DbHelper;->packageTable:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4, v0}, Landroid/database/sqlite/SQLiteDatabase;->replace(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_69
    .catch Ljava/lang/Exception; {:try_start_41 .. :try_end_69} :catch_6a

    goto :goto_40

    .line 193
    .end local v0    # "cv":Landroid/content/ContentValues;
    .end local v1    # "e":Ljava/lang/Exception;
    :catch_6a
    move-exception v1

    .line 194
    .restart local v1    # "e":Ljava/lang/Exception;
    sput-boolean v5, Lcom/google/android/finsky/billing/iab/DbHelper;->savePackage:Z

    .line 196
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "LuckyPatcher-Error: savePackage "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_46
.end method

.method public updatePackage(Ljava/util/List;)V
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 235
    .local p1, "pli":Ljava/util/List;, "Ljava/util/List<Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;>;"
    return-void
.end method
