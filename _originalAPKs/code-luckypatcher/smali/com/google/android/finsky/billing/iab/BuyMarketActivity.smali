.class public Lcom/google/android/finsky/billing/iab/BuyMarketActivity;
.super Landroid/app/Activity;
.source "BuyMarketActivity.java"


# static fields
.field public static final BUY_INTENT:Ljava/lang/String; = "org.billinghack.BUY"

.field public static final EXTRA_DEV_PAYLOAD:Ljava/lang/String; = "payload"

.field public static final EXTRA_PACKAGENAME:Ljava/lang/String; = "packageName"

.field public static final EXTRA_PRODUCT_ID:Ljava/lang/String; = "product"

.field public static final TAG:Ljava/lang/String; = "BillingHack"


# instance fields
.field check:Landroid/widget/CheckBox;

.field check2:Landroid/widget/CheckBox;

.field check3:Landroid/widget/CheckBox;

.field public context:Lcom/google/android/finsky/billing/iab/BuyMarketActivity;

.field pData:Ljava/lang/String;

.field public packageName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 20
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 28
    iput-object v1, p0, Lcom/google/android/finsky/billing/iab/BuyMarketActivity;->context:Lcom/google/android/finsky/billing/iab/BuyMarketActivity;

    .line 29
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/billing/iab/BuyMarketActivity;->packageName:Ljava/lang/String;

    .line 31
    iput-object v1, p0, Lcom/google/android/finsky/billing/iab/BuyMarketActivity;->check:Landroid/widget/CheckBox;

    .line 32
    iput-object v1, p0, Lcom/google/android/finsky/billing/iab/BuyMarketActivity;->check2:Landroid/widget/CheckBox;

    .line 33
    iput-object v1, p0, Lcom/google/android/finsky/billing/iab/BuyMarketActivity;->check3:Landroid/widget/CheckBox;

    return-void
.end method


# virtual methods
.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .registers 5
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    const/4 v2, 0x1

    .line 218
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    .line 220
    .local v0, "currentOrientation":I
    const/4 v1, 0x2

    if-ne v0, v1, :cond_a

    .line 221
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/billing/iab/BuyMarketActivity;->setRequestedOrientation(I)V

    .line 223
    :cond_a
    if-ne v0, v2, :cond_f

    .line 224
    invoke-virtual {p0, v2}, Lcom/google/android/finsky/billing/iab/BuyMarketActivity;->setRequestedOrientation(I)V

    .line 226
    :cond_f
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 228
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 16
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v13, 0x1

    const/4 v12, 0x4

    const/4 v11, 0x0

    .line 36
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 37
    iput-object p0, p0, Lcom/google/android/finsky/billing/iab/BuyMarketActivity;->context:Lcom/google/android/finsky/billing/iab/BuyMarketActivity;

    .line 39
    const-string v5, "BillingHack"

    const-string v6, "Buy intent!"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 40
    if-eqz p1, :cond_14a

    .line 41
    const-string v5, "packageName"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/billing/iab/BuyMarketActivity;->packageName:Ljava/lang/String;

    .line 45
    :goto_19
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/iab/BuyMarketActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v5

    const-string v6, "autorepeat"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 46
    .local v0, "autorepeat":Ljava/lang/String;
    if-eqz v0, :cond_80

    .line 47
    const-string v5, "1"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_15c

    .line 48
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v5

    const-string v6, "config"

    invoke-virtual {v5, v6, v12}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v5

    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    const-string v6, "UnSign"

    invoke-interface {v5, v6, v11}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    invoke-interface {v5}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 52
    :goto_48
    new-instance v4, Landroid/content/Intent;

    const-string v5, "com.android.vending.billing.IN_APP_NOTIFY"

    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 53
    .local v4, "v2":Landroid/content/Intent;
    iget-object v5, p0, Lcom/google/android/finsky/billing/iab/BuyMarketActivity;->packageName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 55
    const-string v5, "notification_id"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, ""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-wide v7, 0xde0b6b3a7640000L

    const-wide v9, 0x7fffffffffffffffL

    invoke-static {v7, v8, v9, v10}, Lcom/chelpus/Utils;->getRandom(JJ)J

    move-result-wide v7

    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 56
    invoke-virtual {p0, v4}, Lcom/google/android/finsky/billing/iab/BuyMarketActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 57
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/iab/BuyMarketActivity;->finish()V

    .line 60
    .end local v4    # "v2":Landroid/content/Intent;
    :cond_80
    const v5, 0x7f040012

    invoke-virtual {p0, v5}, Lcom/google/android/finsky/billing/iab/BuyMarketActivity;->setContentView(I)V

    .line 78
    const v5, 0x7f0d0007

    invoke-virtual {p0, v5}, Lcom/google/android/finsky/billing/iab/BuyMarketActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    .line 79
    .local v2, "btnYes":Landroid/widget/Button;
    const v5, 0x7f0d0008

    invoke-virtual {p0, v5}, Lcom/google/android/finsky/billing/iab/BuyMarketActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 80
    .local v1, "btnNo":Landroid/widget/Button;
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v5

    const-string v6, "config"

    invoke-virtual {v5, v6, v12}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v5

    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    const-string v6, "UnSign"

    invoke-interface {v5, v6, v11}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    invoke-interface {v5}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 81
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v5

    const-string v6, "config"

    invoke-virtual {v5, v6, v12}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v5

    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    const-string v6, "SavePurchase"

    invoke-interface {v5, v6, v11}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    invoke-interface {v5}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 82
    const v5, 0x7f0d0053

    invoke-virtual {p0, v5}, Lcom/google/android/finsky/billing/iab/BuyMarketActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/CheckBox;

    iput-object v5, p0, Lcom/google/android/finsky/billing/iab/BuyMarketActivity;->check:Landroid/widget/CheckBox;

    .line 83
    const v5, 0x7f0d0054

    invoke-virtual {p0, v5}, Lcom/google/android/finsky/billing/iab/BuyMarketActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/CheckBox;

    iput-object v5, p0, Lcom/google/android/finsky/billing/iab/BuyMarketActivity;->check2:Landroid/widget/CheckBox;

    .line 84
    const v5, 0x7f0d0055

    invoke-virtual {p0, v5}, Lcom/google/android/finsky/billing/iab/BuyMarketActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/CheckBox;

    iput-object v5, p0, Lcom/google/android/finsky/billing/iab/BuyMarketActivity;->check3:Landroid/widget/CheckBox;

    .line 85
    const v5, 0x7f0d0047

    invoke-virtual {p0, v5}, Lcom/google/android/finsky/billing/iab/BuyMarketActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 86
    .local v3, "text2":Landroid/widget/TextView;
    const v5, 0x7f070026

    invoke-static {v5}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 87
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f070027

    invoke-static {v6}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    .line 88
    iget-object v5, p0, Lcom/google/android/finsky/billing/iab/BuyMarketActivity;->check:Landroid/widget/CheckBox;

    invoke-virtual {v5, v11}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 89
    iget-object v5, p0, Lcom/google/android/finsky/billing/iab/BuyMarketActivity;->check2:Landroid/widget/CheckBox;

    invoke-virtual {v5, v11}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 90
    iget-object v5, p0, Lcom/google/android/finsky/billing/iab/BuyMarketActivity;->check3:Landroid/widget/CheckBox;

    invoke-virtual {v5, v11}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 91
    invoke-static {}, Lcom/chelpus/Utils;->checkCoreJarPatch11()Z

    move-result v5

    if-nez v5, :cond_139

    .line 92
    iget-object v5, p0, Lcom/google/android/finsky/billing/iab/BuyMarketActivity;->packageName:Ljava/lang/String;

    invoke-static {v5, p0}, Lcom/chelpus/Utils;->isRebuildedOrOdex(Ljava/lang/String;Landroid/content/Context;)Z

    move-result v5

    if-nez v5, :cond_139

    .line 93
    iget-object v5, p0, Lcom/google/android/finsky/billing/iab/BuyMarketActivity;->check:Landroid/widget/CheckBox;

    invoke-virtual {v5, v13}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 148
    :cond_139
    new-instance v5, Lcom/google/android/finsky/billing/iab/BuyMarketActivity$1;

    invoke-direct {v5, p0}, Lcom/google/android/finsky/billing/iab/BuyMarketActivity$1;-><init>(Lcom/google/android/finsky/billing/iab/BuyMarketActivity;)V

    invoke-virtual {v2, v5}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 186
    new-instance v5, Lcom/google/android/finsky/billing/iab/BuyMarketActivity$2;

    invoke-direct {v5, p0}, Lcom/google/android/finsky/billing/iab/BuyMarketActivity$2;-><init>(Lcom/google/android/finsky/billing/iab/BuyMarketActivity;)V

    invoke-virtual {v1, v5}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 200
    return-void

    .line 43
    .end local v0    # "autorepeat":Ljava/lang/String;
    .end local v1    # "btnNo":Landroid/widget/Button;
    .end local v2    # "btnYes":Landroid/widget/Button;
    .end local v3    # "text2":Landroid/widget/TextView;
    :cond_14a
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/iab/BuyMarketActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v5

    const-string v6, "packageName"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/billing/iab/BuyMarketActivity;->packageName:Ljava/lang/String;

    goto/16 :goto_19

    .line 50
    .restart local v0    # "autorepeat":Ljava/lang/String;
    :cond_15c
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v5

    const-string v6, "config"

    invoke-virtual {v5, v6, v12}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v5

    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    const-string v6, "UnSign"

    invoke-interface {v5, v6, v13}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    invoke-interface {v5}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto/16 :goto_48
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .registers 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 211
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "load instance"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 212
    const-string v0, "packageName"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/iab/BuyMarketActivity;->packageName:Ljava/lang/String;

    .line 213
    invoke-super {p0, p1}, Landroid/app/Activity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 214
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 4
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 204
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "save instance"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 205
    const-string v0, "packageName"

    iget-object v1, p0, Lcom/google/android/finsky/billing/iab/BuyMarketActivity;->packageName:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 207
    return-void
.end method
