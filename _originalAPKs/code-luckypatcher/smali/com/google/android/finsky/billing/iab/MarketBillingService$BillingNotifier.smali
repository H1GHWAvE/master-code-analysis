.class public Lcom/google/android/finsky/billing/iab/MarketBillingService$BillingNotifier;
.super Ljava/lang/Object;
.source "MarketBillingService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/billing/iab/MarketBillingService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "BillingNotifier"
.end annotation


# instance fields
.field private mService:Lcom/google/android/finsky/billing/iab/MarketBillingService;

.field final synthetic this$0:Lcom/google/android/finsky/billing/iab/MarketBillingService;


# direct methods
.method public constructor <init>(Lcom/google/android/finsky/billing/iab/MarketBillingService;Lcom/google/android/finsky/billing/iab/MarketBillingService;)V
    .registers 3
    .param p1, "this$0"    # Lcom/google/android/finsky/billing/iab/MarketBillingService;
    .param p2, "service"    # Lcom/google/android/finsky/billing/iab/MarketBillingService;

    .prologue
    .line 59
    iput-object p1, p0, Lcom/google/android/finsky/billing/iab/MarketBillingService$BillingNotifier;->this$0:Lcom/google/android/finsky/billing/iab/MarketBillingService;

    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    iput-object p2, p0, Lcom/google/android/finsky/billing/iab/MarketBillingService$BillingNotifier;->mService:Lcom/google/android/finsky/billing/iab/MarketBillingService;

    .line 62
    return-void
.end method


# virtual methods
.method protected sendPurchaseStateChanged(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .registers 9
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "data"    # Ljava/lang/String;
    .param p3, "signature"    # Ljava/lang/String;

    .prologue
    .line 68
    iget-object v2, p0, Lcom/google/android/finsky/billing/iab/MarketBillingService$BillingNotifier;->mService:Lcom/google/android/finsky/billing/iab/MarketBillingService;

    new-instance v3, Landroid/content/Intent;

    const-string v4, "com.android.vending.billing.PURCHASE_STATE_CHANGED"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {v2, p1, v3}, Lcom/google/android/finsky/billing/iab/MarketBillingService;->findReceiverName(Landroid/content/Context;Ljava/lang/String;Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v0

    .line 70
    .local v0, "v0":Landroid/content/Intent;
    if-nez v0, :cond_11

    .line 71
    const/4 v1, 0x0

    .line 79
    .local v1, "v1":Z
    :goto_10
    return v1

    .line 73
    .end local v1    # "v1":Z
    :cond_11
    const-string v2, "inapp_signed_data"

    invoke-virtual {v0, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 74
    const-string v2, "inapp_signature"

    invoke-virtual {v0, v2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 75
    iget-object v2, p0, Lcom/google/android/finsky/billing/iab/MarketBillingService$BillingNotifier;->mService:Lcom/google/android/finsky/billing/iab/MarketBillingService;

    invoke-virtual {v2, v0}, Lcom/google/android/finsky/billing/iab/MarketBillingService;->sendBroadcast(Landroid/content/Intent;)V

    .line 76
    const/4 v1, 0x1

    .restart local v1    # "v1":Z
    goto :goto_10
.end method

.method protected sendResponseCode(Ljava/lang/String;JI)Z
    .registers 6
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "requestId"    # J
    .param p4, "responseCode"    # I

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/android/finsky/billing/iab/MarketBillingService$BillingNotifier;->mService:Lcom/google/android/finsky/billing/iab/MarketBillingService;

    invoke-static {v0, p1, p2, p3, p4}, Lcom/google/android/finsky/billing/iab/MarketBillingService;->sendResponseCode(Landroid/content/Context;Ljava/lang/String;JI)Z

    move-result v0

    return v0
.end method
