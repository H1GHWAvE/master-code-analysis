.class public Lcom/google/android/finsky/billing/iab/google/util/IabHelper;
.super Ljava/lang/Object;
.source "IabHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnConsumeMultiFinishedListener;,
        Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnConsumeFinishedListener;,
        Lcom/google/android/finsky/billing/iab/google/util/IabHelper$QueryInventoryFinishedListener;,
        Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnIabPurchaseFinishedListener;,
        Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnIabSetupFinishedListener;
    }
.end annotation


# static fields
.field public static final BILLING_RESPONSE_RESULT_BILLING_UNAVAILABLE:I = 0x3

.field public static final BILLING_RESPONSE_RESULT_DEVELOPER_ERROR:I = 0x5

.field public static final BILLING_RESPONSE_RESULT_ERROR:I = 0x6

.field public static final BILLING_RESPONSE_RESULT_ITEM_ALREADY_OWNED:I = 0x7

.field public static final BILLING_RESPONSE_RESULT_ITEM_NOT_OWNED:I = 0x8

.field public static final BILLING_RESPONSE_RESULT_ITEM_UNAVAILABLE:I = 0x4

.field public static final BILLING_RESPONSE_RESULT_OK:I = 0x0

.field public static final BILLING_RESPONSE_RESULT_USER_CANCELED:I = 0x1

.field public static final GET_SKU_DETAILS_ITEM_LIST:Ljava/lang/String; = "ITEM_ID_LIST"

.field public static final GET_SKU_DETAILS_ITEM_TYPE_LIST:Ljava/lang/String; = "ITEM_TYPE_LIST"

.field public static final IABHELPER_BAD_RESPONSE:I = -0x3ea

.field public static final IABHELPER_ERROR_BASE:I = -0x3e8

.field public static final IABHELPER_INVALID_CONSUMPTION:I = -0x3f2

.field public static final IABHELPER_MISSING_TOKEN:I = -0x3ef

.field public static final IABHELPER_REMOTE_EXCEPTION:I = -0x3e9

.field public static final IABHELPER_SEND_INTENT_FAILED:I = -0x3ec

.field public static final IABHELPER_SUBSCRIPTIONS_NOT_AVAILABLE:I = -0x3f1

.field public static final IABHELPER_UNKNOWN_ERROR:I = -0x3f0

.field public static final IABHELPER_UNKNOWN_PURCHASE_RESPONSE:I = -0x3ee

.field public static final IABHELPER_USER_CANCELLED:I = -0x3ed

.field public static final IABHELPER_VERIFICATION_FAILED:I = -0x3eb

.field public static final INAPP_CONTINUATION_TOKEN:Ljava/lang/String; = "INAPP_CONTINUATION_TOKEN"

.field public static final ITEM_TYPE_INAPP:Ljava/lang/String; = "inapp"

.field public static final ITEM_TYPE_SUBS:Ljava/lang/String; = "subs"

.field public static final RESPONSE_BUY_INTENT:Ljava/lang/String; = "BUY_INTENT"

.field public static final RESPONSE_CODE:Ljava/lang/String; = "RESPONSE_CODE"

.field public static final RESPONSE_GET_SKU_DETAILS_LIST:Ljava/lang/String; = "DETAILS_LIST"

.field public static final RESPONSE_INAPP_ITEM_LIST:Ljava/lang/String; = "INAPP_PURCHASE_ITEM_LIST"

.field public static final RESPONSE_INAPP_PURCHASE_DATA:Ljava/lang/String; = "INAPP_PURCHASE_DATA"

.field public static final RESPONSE_INAPP_PURCHASE_DATA_LIST:Ljava/lang/String; = "INAPP_PURCHASE_DATA_LIST"

.field public static final RESPONSE_INAPP_SIGNATURE:Ljava/lang/String; = "INAPP_DATA_SIGNATURE"

.field public static final RESPONSE_INAPP_SIGNATURE_LIST:Ljava/lang/String; = "INAPP_DATA_SIGNATURE_LIST"


# instance fields
.field mAsyncInProgress:Z

.field mAsyncOperation:Ljava/lang/String;

.field mContext:Landroid/content/Context;

.field mDebugLog:Z

.field mDebugTag:Ljava/lang/String;

.field mDisposed:Z

.field mPurchaseListener:Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnIabPurchaseFinishedListener;

.field mPurchasingItemType:Ljava/lang/String;

.field mRequestCode:I

.field mService:Lcom/google/android/finsky/billing/iab/google/util/IInAppBillingService;

.field mServiceConn:Landroid/content/ServiceConnection;

.field mSetupDone:Z

.field mSignatureBase64:Ljava/lang/String;

.field mSubscriptionsSupported:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .registers 5
    .param p1, "ctx"    # Landroid/content/Context;
    .param p2, "base64PublicKey"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 161
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    iput-boolean v1, p0, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->mDebugLog:Z

    .line 73
    const-string v0, "IabHelper"

    iput-object v0, p0, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->mDebugTag:Ljava/lang/String;

    .line 76
    iput-boolean v1, p0, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->mSetupDone:Z

    .line 79
    iput-boolean v1, p0, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->mDisposed:Z

    .line 82
    iput-boolean v1, p0, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->mSubscriptionsSupported:Z

    .line 86
    iput-boolean v1, p0, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->mAsyncInProgress:Z

    .line 90
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->mAsyncOperation:Ljava/lang/String;

    .line 106
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->mSignatureBase64:Ljava/lang/String;

    .line 162
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->mContext:Landroid/content/Context;

    .line 163
    iput-object p2, p0, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->mSignatureBase64:Ljava/lang/String;

    .line 164
    const-string v0, "IAB helper created."

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->logDebug(Ljava/lang/String;)V

    .line 165
    return-void
.end method

.method private checkNotDisposed()V
    .registers 3

    .prologue
    .line 299
    iget-boolean v0, p0, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->mDisposed:Z

    if-eqz v0, :cond_c

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "IabHelper was disposed of, so it cannot be used."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 300
    :cond_c
    return-void
.end method

.method public static getResponseDesc(I)Ljava/lang/String;
    .registers 6
    .param p0, "code"    # I

    .prologue
    .line 748
    const-string v3, "0:OK/1:User Canceled/2:Unknown/3:Billing Unavailable/4:Item unavailable/5:Developer Error/6:Error/7:Item Already Owned/8:Item not owned"

    const-string v4, "/"

    .line 751
    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 752
    .local v0, "iab_msgs":[Ljava/lang/String;
    const-string v3, "0:OK/-1001:Remote exception during initialization/-1002:Bad response received/-1003:Purchase signature verification failed/-1004:Send intent failed/-1005:User cancelled/-1006:Unknown purchase response/-1007:Missing token/-1008:Unknown error/-1009:Subscriptions not available/-1010:Invalid consumption attempt"

    const-string v4, "/"

    .line 761
    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 763
    .local v1, "iabhelper_msgs":[Ljava/lang/String;
    const/16 v3, -0x3e8

    if-gt p0, v3, :cond_36

    .line 764
    rsub-int v2, p0, -0x3e8

    .line 765
    .local v2, "index":I
    if-ltz v2, :cond_1e

    array-length v3, v1

    if-ge v2, v3, :cond_1e

    aget-object v3, v1, v2

    .line 771
    .end local v2    # "index":I
    :goto_1d
    return-object v3

    .line 766
    .restart local v2    # "index":I
    :cond_1e
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":Unknown IAB Helper Error"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_1d

    .line 768
    .end local v2    # "index":I
    :cond_36
    if-ltz p0, :cond_3b

    array-length v3, v0

    if-lt p0, v3, :cond_53

    .line 769
    :cond_3b
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":Unknown"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_1d

    .line 771
    :cond_53
    aget-object v3, v0, p0

    goto :goto_1d
.end method


# virtual methods
.method checkSetupDone(Ljava/lang/String;)V
    .registers 5
    .param p1, "operation"    # Ljava/lang/String;

    .prologue
    .line 777
    iget-boolean v0, p0, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->mSetupDone:Z

    if-nez v0, :cond_39

    .line 778
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Illegal state for operation ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "): IAB helper is not set up."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->logError(Ljava/lang/String;)V

    .line 779
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "IAB helper is not set up. Can\'t perform operation: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 781
    :cond_39
    return-void
.end method

.method consume(Lcom/google/android/finsky/billing/iab/google/util/Purchase;)V
    .registers 10
    .param p1, "itemInfo"    # Lcom/google/android/finsky/billing/iab/google/util/Purchase;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/finsky/billing/iab/google/util/IabException;
        }
    .end annotation

    .prologue
    .line 654
    invoke-direct {p0}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->checkNotDisposed()V

    .line 655
    const-string v4, "consume"

    invoke-virtual {p0, v4}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->checkSetupDone(Ljava/lang/String;)V

    .line 657
    iget-object v4, p1, Lcom/google/android/finsky/billing/iab/google/util/Purchase;->mItemType:Ljava/lang/String;

    const-string v5, "inapp"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_35

    .line 658
    new-instance v4, Lcom/google/android/finsky/billing/iab/google/util/IabException;

    const/16 v5, -0x3f2

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Items of type \'"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p1, Lcom/google/android/finsky/billing/iab/google/util/Purchase;->mItemType:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\' can\'t be consumed."

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/google/android/finsky/billing/iab/google/util/IabException;-><init>(ILjava/lang/String;)V

    throw v4

    .line 663
    :cond_35
    :try_start_35
    invoke-virtual {p1}, Lcom/google/android/finsky/billing/iab/google/util/Purchase;->getToken()Ljava/lang/String;

    move-result-object v3

    .line 664
    .local v3, "token":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/google/android/finsky/billing/iab/google/util/Purchase;->getSku()Ljava/lang/String;

    move-result-object v2

    .line 665
    .local v2, "sku":Ljava/lang/String;
    if-eqz v3, :cond_47

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_a4

    .line 666
    :cond_47
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Can\'t consume "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ". No token."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->logError(Ljava/lang/String;)V

    .line 667
    new-instance v4, Lcom/google/android/finsky/billing/iab/google/util/IabException;

    const/16 v5, -0x3ef

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "PurchaseInfo is missing token for sku: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/google/android/finsky/billing/iab/google/util/IabException;-><init>(ILjava/lang/String;)V

    throw v4
    :try_end_88
    .catch Landroid/os/RemoteException; {:try_start_35 .. :try_end_88} :catch_88

    .line 681
    .end local v2    # "sku":Ljava/lang/String;
    .end local v3    # "token":Ljava/lang/String;
    :catch_88
    move-exception v0

    .line 682
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v4, Lcom/google/android/finsky/billing/iab/google/util/IabException;

    const/16 v5, -0x3e9

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Remote exception while consuming. PurchaseInfo: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6, v0}, Lcom/google/android/finsky/billing/iab/google/util/IabException;-><init>(ILjava/lang/String;Ljava/lang/Exception;)V

    throw v4

    .line 671
    .end local v0    # "e":Landroid/os/RemoteException;
    .restart local v2    # "sku":Ljava/lang/String;
    .restart local v3    # "token":Ljava/lang/String;
    :cond_a4
    :try_start_a4
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Consuming sku: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", token: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->logDebug(Ljava/lang/String;)V

    .line 672
    iget-object v4, p0, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->mService:Lcom/google/android/finsky/billing/iab/google/util/IInAppBillingService;

    const/4 v5, 0x3

    iget-object v6, p0, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v4, v5, v6, v3}, Lcom/google/android/finsky/billing/iab/google/util/IInAppBillingService;->consumePurchase(ILjava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 673
    .local v1, "response":I
    if-nez v1, :cond_ea

    .line 674
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Successfully consumed sku: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->logDebug(Ljava/lang/String;)V

    .line 684
    return-void

    .line 677
    :cond_ea
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Error consuming consuming sku "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ". "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v1}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->getResponseDesc(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->logDebug(Ljava/lang/String;)V

    .line 678
    new-instance v4, Lcom/google/android/finsky/billing/iab/google/util/IabException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Error consuming sku "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v1, v5}, Lcom/google/android/finsky/billing/iab/google/util/IabException;-><init>(ILjava/lang/String;)V

    throw v4
    :try_end_127
    .catch Landroid/os/RemoteException; {:try_start_a4 .. :try_end_127} :catch_88
.end method

.method public consumeAsync(Lcom/google/android/finsky/billing/iab/google/util/Purchase;Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnConsumeFinishedListener;)V
    .registers 5
    .param p1, "purchase"    # Lcom/google/android/finsky/billing/iab/google/util/Purchase;
    .param p2, "listener"    # Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnConsumeFinishedListener;

    .prologue
    .line 722
    invoke-direct {p0}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->checkNotDisposed()V

    .line 723
    const-string v1, "consume"

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->checkSetupDone(Ljava/lang/String;)V

    .line 724
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 725
    .local v0, "purchases":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/billing/iab/google/util/Purchase;>;"
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 726
    const/4 v1, 0x0

    invoke-virtual {p0, v0, p2, v1}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->consumeAsyncInternal(Ljava/util/List;Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnConsumeFinishedListener;Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnConsumeMultiFinishedListener;)V

    .line 727
    return-void
.end method

.method public consumeAsync(Ljava/util/List;Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnConsumeMultiFinishedListener;)V
    .registers 4
    .param p2, "listener"    # Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnConsumeMultiFinishedListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/billing/iab/google/util/Purchase;",
            ">;",
            "Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnConsumeMultiFinishedListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 735
    .local p1, "purchases":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/billing/iab/google/util/Purchase;>;"
    invoke-direct {p0}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->checkNotDisposed()V

    .line 736
    const-string v0, "consume"

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->checkSetupDone(Ljava/lang/String;)V

    .line 737
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, p2}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->consumeAsyncInternal(Ljava/util/List;Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnConsumeFinishedListener;Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnConsumeMultiFinishedListener;)V

    .line 738
    return-void
.end method

.method consumeAsyncInternal(Ljava/util/List;Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnConsumeFinishedListener;Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnConsumeMultiFinishedListener;)V
    .registers 11
    .param p2, "singleListener"    # Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnConsumeFinishedListener;
    .param p3, "multiListener"    # Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnConsumeMultiFinishedListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/billing/iab/google/util/Purchase;",
            ">;",
            "Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnConsumeFinishedListener;",
            "Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnConsumeMultiFinishedListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 943
    .local p1, "purchases":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/billing/iab/google/util/Purchase;>;"
    new-instance v4, Landroid/os/Handler;

    invoke-direct {v4}, Landroid/os/Handler;-><init>()V

    .line 944
    .local v4, "handler":Landroid/os/Handler;
    const-string v0, "consume"

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->flagStartAsync(Ljava/lang/String;)V

    .line 945
    new-instance v6, Ljava/lang/Thread;

    new-instance v0, Lcom/google/android/finsky/billing/iab/google/util/IabHelper$3;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper$3;-><init>(Lcom/google/android/finsky/billing/iab/google/util/IabHelper;Ljava/util/List;Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnConsumeFinishedListener;Landroid/os/Handler;Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnConsumeMultiFinishedListener;)V

    invoke-direct {v6, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 974
    invoke-virtual {v6}, Ljava/lang/Thread;->start()V

    .line 975
    return-void
.end method

.method public dispose()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 285
    const-string v0, "Disposing."

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->logDebug(Ljava/lang/String;)V

    .line 286
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->mSetupDone:Z

    .line 287
    iget-object v0, p0, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->mServiceConn:Landroid/content/ServiceConnection;

    if-eqz v0, :cond_1d

    .line 288
    const-string v0, "Unbinding from service."

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->logDebug(Ljava/lang/String;)V

    .line 289
    iget-object v0, p0, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_1d

    iget-object v0, p0, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->mServiceConn:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 291
    :cond_1d
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->mDisposed:Z

    .line 292
    iput-object v2, p0, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->mContext:Landroid/content/Context;

    .line 293
    iput-object v2, p0, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->mServiceConn:Landroid/content/ServiceConnection;

    .line 294
    iput-object v2, p0, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->mService:Lcom/google/android/finsky/billing/iab/google/util/IInAppBillingService;

    .line 295
    iput-object v2, p0, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->mPurchaseListener:Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnIabPurchaseFinishedListener;

    .line 296
    return-void
.end method

.method public enableDebugLogging(Z)V
    .registers 2
    .param p1, "enable"    # Z

    .prologue
    .line 177
    invoke-direct {p0}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->checkNotDisposed()V

    .line 178
    iput-boolean p1, p0, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->mDebugLog:Z

    .line 179
    return-void
.end method

.method public enableDebugLogging(ZLjava/lang/String;)V
    .registers 3
    .param p1, "enable"    # Z
    .param p2, "tag"    # Ljava/lang/String;

    .prologue
    .line 171
    invoke-direct {p0}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->checkNotDisposed()V

    .line 172
    iput-boolean p1, p0, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->mDebugLog:Z

    .line 173
    iput-object p2, p0, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->mDebugTag:Ljava/lang/String;

    .line 174
    return-void
.end method

.method flagEndAsync()V
    .registers 3

    .prologue
    .line 824
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Ending async operation: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->mAsyncOperation:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->logDebug(Ljava/lang/String;)V

    .line 825
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->mAsyncOperation:Ljava/lang/String;

    .line 826
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->mAsyncInProgress:Z

    .line 827
    return-void
.end method

.method flagStartAsync(Ljava/lang/String;)V
    .registers 5
    .param p1, "operation"    # Ljava/lang/String;

    .prologue
    .line 816
    iget-boolean v0, p0, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->mAsyncInProgress:Z

    if-eqz v0, :cond_2f

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Can\'t start async operation ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") because another async operation("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->mAsyncOperation:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") is in progress."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 818
    :cond_2f
    iput-object p1, p0, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->mAsyncOperation:Ljava/lang/String;

    .line 819
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->mAsyncInProgress:Z

    .line 820
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Starting async operation: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->logDebug(Ljava/lang/String;)V

    .line 821
    return-void
.end method

.method getResponseCodeFromBundle(Landroid/os/Bundle;)I
    .registers 6
    .param p1, "b"    # Landroid/os/Bundle;

    .prologue
    .line 785
    const-string v1, "RESPONSE_CODE"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 786
    .local v0, "o":Ljava/lang/Object;
    if-nez v0, :cond_f

    .line 787
    const-string v1, "Bundle with null response code, assuming OK (known issue)"

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->logDebug(Ljava/lang/String;)V

    .line 788
    const/4 v1, 0x0

    .line 791
    .end local v0    # "o":Ljava/lang/Object;
    :goto_e
    return v1

    .line 790
    .restart local v0    # "o":Ljava/lang/Object;
    :cond_f
    instance-of v1, v0, Ljava/lang/Integer;

    if-eqz v1, :cond_1a

    check-cast v0, Ljava/lang/Integer;

    .end local v0    # "o":Ljava/lang/Object;
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto :goto_e

    .line 791
    .restart local v0    # "o":Ljava/lang/Object;
    :cond_1a
    instance-of v1, v0, Ljava/lang/Long;

    if-eqz v1, :cond_26

    check-cast v0, Ljava/lang/Long;

    .end local v0    # "o":Ljava/lang/Object;
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    long-to-int v1, v1

    goto :goto_e

    .line 793
    .restart local v0    # "o":Ljava/lang/Object;
    :cond_26
    const-string v1, "Unexpected type for bundle response code."

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->logError(Ljava/lang/String;)V

    .line 794
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->logError(Ljava/lang/String;)V

    .line 795
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unexpected type for bundle response code: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method getResponseCodeFromIntent(Landroid/content/Intent;)I
    .registers 6
    .param p1, "i"    # Landroid/content/Intent;

    .prologue
    .line 801
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "RESPONSE_CODE"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 802
    .local v0, "o":Ljava/lang/Object;
    if-nez v0, :cond_13

    .line 803
    const-string v1, "Intent with no response code, assuming OK (known issue)"

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->logError(Ljava/lang/String;)V

    .line 804
    const/4 v1, 0x0

    .line 807
    .end local v0    # "o":Ljava/lang/Object;
    :goto_12
    return v1

    .line 806
    .restart local v0    # "o":Ljava/lang/Object;
    :cond_13
    instance-of v1, v0, Ljava/lang/Integer;

    if-eqz v1, :cond_1e

    check-cast v0, Ljava/lang/Integer;

    .end local v0    # "o":Ljava/lang/Object;
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto :goto_12

    .line 807
    .restart local v0    # "o":Ljava/lang/Object;
    :cond_1e
    instance-of v1, v0, Ljava/lang/Long;

    if-eqz v1, :cond_2a

    check-cast v0, Ljava/lang/Long;

    .end local v0    # "o":Ljava/lang/Object;
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    long-to-int v1, v1

    goto :goto_12

    .line 809
    .restart local v0    # "o":Ljava/lang/Object;
    :cond_2a
    const-string v1, "Unexpected type for intent response code."

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->logError(Ljava/lang/String;)V

    .line 810
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->logError(Ljava/lang/String;)V

    .line 811
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unexpected type for intent response code: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public handleActivityResult(IILandroid/content/Intent;)Z
    .registers 16
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 436
    iget v8, p0, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->mRequestCode:I

    if-eq p1, v8, :cond_6

    const/4 v8, 0x0

    .line 515
    :goto_5
    return v8

    .line 438
    :cond_6
    invoke-direct {p0}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->checkNotDisposed()V

    .line 439
    const-string v8, "handleActivityResult"

    invoke-virtual {p0, v8}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->checkSetupDone(Ljava/lang/String;)V

    .line 442
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->flagEndAsync()V

    .line 444
    if-nez p3, :cond_2d

    .line 445
    const-string v8, "Null data in IAB activity result."

    invoke-virtual {p0, v8}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->logError(Ljava/lang/String;)V

    .line 446
    new-instance v6, Lcom/google/android/finsky/billing/iab/google/util/IabResult;

    const/16 v8, -0x3ea

    const-string v9, "Null data in IAB result"

    invoke-direct {v6, v8, v9}, Lcom/google/android/finsky/billing/iab/google/util/IabResult;-><init>(ILjava/lang/String;)V

    .line 447
    .local v6, "result":Lcom/google/android/finsky/billing/iab/google/util/IabResult;
    iget-object v8, p0, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->mPurchaseListener:Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnIabPurchaseFinishedListener;

    if-eqz v8, :cond_2b

    iget-object v8, p0, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->mPurchaseListener:Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnIabPurchaseFinishedListener;

    const/4 v9, 0x0

    invoke-interface {v8, v6, v9}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnIabPurchaseFinishedListener;->onIabPurchaseFinished(Lcom/google/android/finsky/billing/iab/google/util/IabResult;Lcom/google/android/finsky/billing/iab/google/util/Purchase;)V

    .line 448
    :cond_2b
    const/4 v8, 0x1

    goto :goto_5

    .line 451
    .end local v6    # "result":Lcom/google/android/finsky/billing/iab/google/util/IabResult;
    :cond_2d
    invoke-virtual {p0, p3}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->getResponseCodeFromIntent(Landroid/content/Intent;)I

    move-result v5

    .line 452
    .local v5, "responseCode":I
    const-string v8, "INAPP_PURCHASE_DATA"

    invoke-virtual {p3, v8}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 453
    .local v4, "purchaseData":Ljava/lang/String;
    const-string v8, "INAPP_DATA_SIGNATURE"

    invoke-virtual {p3, v8}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 455
    .local v0, "dataSignature":Ljava/lang/String;
    const/4 v8, -0x1

    if-ne p2, v8, :cond_16a

    if-nez v5, :cond_16a

    .line 456
    const-string v8, "Successful resultcode from purchase activity."

    invoke-virtual {p0, v8}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->logDebug(Ljava/lang/String;)V

    .line 457
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Purchase data: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->logDebug(Ljava/lang/String;)V

    .line 458
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Data signature: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->logDebug(Ljava/lang/String;)V

    .line 459
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Extras: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->logDebug(Ljava/lang/String;)V

    .line 460
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Expected item type: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->mPurchasingItemType:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->logDebug(Ljava/lang/String;)V

    .line 462
    if-eqz v4, :cond_a9

    if-nez v0, :cond_e2

    .line 463
    :cond_a9
    const-string v8, "BUG: either purchaseData or dataSignature is null."

    invoke-virtual {p0, v8}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->logError(Ljava/lang/String;)V

    .line 464
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Extras: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v9

    invoke-virtual {v9}, Landroid/os/Bundle;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->logDebug(Ljava/lang/String;)V

    .line 465
    new-instance v6, Lcom/google/android/finsky/billing/iab/google/util/IabResult;

    const/16 v8, -0x3f0

    const-string v9, "IAB returned null purchaseData or dataSignature"

    invoke-direct {v6, v8, v9}, Lcom/google/android/finsky/billing/iab/google/util/IabResult;-><init>(ILjava/lang/String;)V

    .line 466
    .restart local v6    # "result":Lcom/google/android/finsky/billing/iab/google/util/IabResult;
    iget-object v8, p0, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->mPurchaseListener:Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnIabPurchaseFinishedListener;

    if-eqz v8, :cond_df

    iget-object v8, p0, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->mPurchaseListener:Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnIabPurchaseFinishedListener;

    const/4 v9, 0x0

    invoke-interface {v8, v6, v9}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnIabPurchaseFinishedListener;->onIabPurchaseFinished(Lcom/google/android/finsky/billing/iab/google/util/IabResult;Lcom/google/android/finsky/billing/iab/google/util/Purchase;)V

    .line 467
    :cond_df
    const/4 v8, 0x1

    goto/16 :goto_5

    .line 470
    .end local v6    # "result":Lcom/google/android/finsky/billing/iab/google/util/IabResult;
    :cond_e2
    const/4 v2, 0x0

    .line 472
    .local v2, "purchase":Lcom/google/android/finsky/billing/iab/google/util/Purchase;
    :try_start_e3
    new-instance v3, Lcom/google/android/finsky/billing/iab/google/util/Purchase;

    iget-object v8, p0, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->mPurchasingItemType:Ljava/lang/String;

    invoke-direct {v3, v8, v4, v0}, Lcom/google/android/finsky/billing/iab/google/util/Purchase;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_ea
    .catch Lorg/json/JSONException; {:try_start_e3 .. :try_end_ea} :catch_14b

    .line 473
    .end local v2    # "purchase":Lcom/google/android/finsky/billing/iab/google/util/Purchase;
    .local v3, "purchase":Lcom/google/android/finsky/billing/iab/google/util/Purchase;
    :try_start_ea
    invoke-virtual {v3}, Lcom/google/android/finsky/billing/iab/google/util/Purchase;->getSku()Ljava/lang/String;

    move-result-object v7

    .line 476
    .local v7, "sku":Ljava/lang/String;
    iget-object v8, p0, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->mSignatureBase64:Ljava/lang/String;

    invoke-static {v8, v4, v0}, Lcom/google/android/finsky/billing/iab/google/util/Security;->verifyPurchase(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_132

    .line 477
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Purchase signature verification FAILED for sku "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->logError(Ljava/lang/String;)V

    .line 478
    new-instance v6, Lcom/google/android/finsky/billing/iab/google/util/IabResult;

    const/16 v8, -0x3eb

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Signature verification failed for sku "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v6, v8, v9}, Lcom/google/android/finsky/billing/iab/google/util/IabResult;-><init>(ILjava/lang/String;)V

    .line 479
    .restart local v6    # "result":Lcom/google/android/finsky/billing/iab/google/util/IabResult;
    iget-object v8, p0, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->mPurchaseListener:Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnIabPurchaseFinishedListener;

    if-eqz v8, :cond_12f

    iget-object v8, p0, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->mPurchaseListener:Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnIabPurchaseFinishedListener;

    invoke-interface {v8, v6, v3}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnIabPurchaseFinishedListener;->onIabPurchaseFinished(Lcom/google/android/finsky/billing/iab/google/util/IabResult;Lcom/google/android/finsky/billing/iab/google/util/Purchase;)V

    .line 480
    :cond_12f
    const/4 v8, 0x1

    goto/16 :goto_5

    .line 482
    .end local v6    # "result":Lcom/google/android/finsky/billing/iab/google/util/IabResult;
    :cond_132
    const-string v8, "Purchase signature successfully verified."

    invoke-virtual {p0, v8}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->logDebug(Ljava/lang/String;)V
    :try_end_137
    .catch Lorg/json/JSONException; {:try_start_ea .. :try_end_137} :catch_206

    .line 492
    iget-object v8, p0, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->mPurchaseListener:Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnIabPurchaseFinishedListener;

    if-eqz v8, :cond_148

    .line 493
    iget-object v8, p0, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->mPurchaseListener:Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnIabPurchaseFinishedListener;

    new-instance v9, Lcom/google/android/finsky/billing/iab/google/util/IabResult;

    const/4 v10, 0x0

    const-string v11, "Success"

    invoke-direct {v9, v10, v11}, Lcom/google/android/finsky/billing/iab/google/util/IabResult;-><init>(ILjava/lang/String;)V

    invoke-interface {v8, v9, v3}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnIabPurchaseFinishedListener;->onIabPurchaseFinished(Lcom/google/android/finsky/billing/iab/google/util/IabResult;Lcom/google/android/finsky/billing/iab/google/util/Purchase;)V

    .line 515
    .end local v3    # "purchase":Lcom/google/android/finsky/billing/iab/google/util/Purchase;
    .end local v7    # "sku":Ljava/lang/String;
    :cond_148
    :goto_148
    const/4 v8, 0x1

    goto/16 :goto_5

    .line 484
    .restart local v2    # "purchase":Lcom/google/android/finsky/billing/iab/google/util/Purchase;
    :catch_14b
    move-exception v1

    .line 485
    .local v1, "e":Lorg/json/JSONException;
    :goto_14c
    const-string v8, "Failed to parse purchase data."

    invoke-virtual {p0, v8}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->logError(Ljava/lang/String;)V

    .line 486
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    .line 487
    new-instance v6, Lcom/google/android/finsky/billing/iab/google/util/IabResult;

    const/16 v8, -0x3ea

    const-string v9, "Failed to parse purchase data."

    invoke-direct {v6, v8, v9}, Lcom/google/android/finsky/billing/iab/google/util/IabResult;-><init>(ILjava/lang/String;)V

    .line 488
    .restart local v6    # "result":Lcom/google/android/finsky/billing/iab/google/util/IabResult;
    iget-object v8, p0, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->mPurchaseListener:Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnIabPurchaseFinishedListener;

    if-eqz v8, :cond_167

    iget-object v8, p0, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->mPurchaseListener:Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnIabPurchaseFinishedListener;

    const/4 v9, 0x0

    invoke-interface {v8, v6, v9}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnIabPurchaseFinishedListener;->onIabPurchaseFinished(Lcom/google/android/finsky/billing/iab/google/util/IabResult;Lcom/google/android/finsky/billing/iab/google/util/Purchase;)V

    .line 489
    :cond_167
    const/4 v8, 0x1

    goto/16 :goto_5

    .line 496
    .end local v1    # "e":Lorg/json/JSONException;
    .end local v2    # "purchase":Lcom/google/android/finsky/billing/iab/google/util/Purchase;
    .end local v6    # "result":Lcom/google/android/finsky/billing/iab/google/util/IabResult;
    :cond_16a
    const/4 v8, -0x1

    if-ne p2, v8, :cond_199

    .line 498
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Result code was OK but in-app billing response was not OK: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {v5}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->getResponseDesc(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->logDebug(Ljava/lang/String;)V

    .line 499
    iget-object v8, p0, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->mPurchaseListener:Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnIabPurchaseFinishedListener;

    if-eqz v8, :cond_148

    .line 500
    new-instance v6, Lcom/google/android/finsky/billing/iab/google/util/IabResult;

    const-string v8, "Problem purchashing item."

    invoke-direct {v6, v5, v8}, Lcom/google/android/finsky/billing/iab/google/util/IabResult;-><init>(ILjava/lang/String;)V

    .line 501
    .restart local v6    # "result":Lcom/google/android/finsky/billing/iab/google/util/IabResult;
    iget-object v8, p0, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->mPurchaseListener:Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnIabPurchaseFinishedListener;

    const/4 v9, 0x0

    invoke-interface {v8, v6, v9}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnIabPurchaseFinishedListener;->onIabPurchaseFinished(Lcom/google/android/finsky/billing/iab/google/util/IabResult;Lcom/google/android/finsky/billing/iab/google/util/Purchase;)V

    goto :goto_148

    .line 504
    .end local v6    # "result":Lcom/google/android/finsky/billing/iab/google/util/IabResult;
    :cond_199
    if-nez p2, :cond_1c9

    .line 505
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Purchase canceled - Response: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {v5}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->getResponseDesc(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->logDebug(Ljava/lang/String;)V

    .line 506
    new-instance v6, Lcom/google/android/finsky/billing/iab/google/util/IabResult;

    const/16 v8, -0x3ed

    const-string v9, "User canceled."

    invoke-direct {v6, v8, v9}, Lcom/google/android/finsky/billing/iab/google/util/IabResult;-><init>(ILjava/lang/String;)V

    .line 507
    .restart local v6    # "result":Lcom/google/android/finsky/billing/iab/google/util/IabResult;
    iget-object v8, p0, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->mPurchaseListener:Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnIabPurchaseFinishedListener;

    if-eqz v8, :cond_148

    iget-object v8, p0, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->mPurchaseListener:Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnIabPurchaseFinishedListener;

    const/4 v9, 0x0

    invoke-interface {v8, v6, v9}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnIabPurchaseFinishedListener;->onIabPurchaseFinished(Lcom/google/android/finsky/billing/iab/google/util/IabResult;Lcom/google/android/finsky/billing/iab/google/util/Purchase;)V

    goto :goto_148

    .line 510
    .end local v6    # "result":Lcom/google/android/finsky/billing/iab/google/util/IabResult;
    :cond_1c9
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Purchase failed. Result code: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ". Response: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 511
    invoke-static {v5}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->getResponseDesc(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 510
    invoke-virtual {p0, v8}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->logError(Ljava/lang/String;)V

    .line 512
    new-instance v6, Lcom/google/android/finsky/billing/iab/google/util/IabResult;

    const/16 v8, -0x3ee

    const-string v9, "Unknown purchase response."

    invoke-direct {v6, v8, v9}, Lcom/google/android/finsky/billing/iab/google/util/IabResult;-><init>(ILjava/lang/String;)V

    .line 513
    .restart local v6    # "result":Lcom/google/android/finsky/billing/iab/google/util/IabResult;
    iget-object v8, p0, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->mPurchaseListener:Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnIabPurchaseFinishedListener;

    if-eqz v8, :cond_148

    iget-object v8, p0, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->mPurchaseListener:Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnIabPurchaseFinishedListener;

    const/4 v9, 0x0

    invoke-interface {v8, v6, v9}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnIabPurchaseFinishedListener;->onIabPurchaseFinished(Lcom/google/android/finsky/billing/iab/google/util/IabResult;Lcom/google/android/finsky/billing/iab/google/util/Purchase;)V

    goto/16 :goto_148

    .line 484
    .end local v6    # "result":Lcom/google/android/finsky/billing/iab/google/util/IabResult;
    .restart local v3    # "purchase":Lcom/google/android/finsky/billing/iab/google/util/Purchase;
    :catch_206
    move-exception v1

    move-object v2, v3

    .end local v3    # "purchase":Lcom/google/android/finsky/billing/iab/google/util/Purchase;
    .restart local v2    # "purchase":Lcom/google/android/finsky/billing/iab/google/util/Purchase;
    goto/16 :goto_14c
.end method

.method public launchPurchaseFlow(Landroid/app/Activity;Ljava/lang/String;ILcom/google/android/finsky/billing/iab/google/util/IabHelper$OnIabPurchaseFinishedListener;)V
    .registers 11
    .param p1, "act"    # Landroid/app/Activity;
    .param p2, "sku"    # Ljava/lang/String;
    .param p3, "requestCode"    # I
    .param p4, "listener"    # Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnIabPurchaseFinishedListener;

    .prologue
    .line 330
    const-string v5, ""

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->launchPurchaseFlow(Landroid/app/Activity;Ljava/lang/String;ILcom/google/android/finsky/billing/iab/google/util/IabHelper$OnIabPurchaseFinishedListener;Ljava/lang/String;)V

    .line 331
    return-void
.end method

.method public launchPurchaseFlow(Landroid/app/Activity;Ljava/lang/String;ILcom/google/android/finsky/billing/iab/google/util/IabHelper$OnIabPurchaseFinishedListener;Ljava/lang/String;)V
    .registers 13
    .param p1, "act"    # Landroid/app/Activity;
    .param p2, "sku"    # Ljava/lang/String;
    .param p3, "requestCode"    # I
    .param p4, "listener"    # Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnIabPurchaseFinishedListener;
    .param p5, "extraData"    # Ljava/lang/String;

    .prologue
    .line 335
    const-string v3, "inapp"

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->launchPurchaseFlow(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;ILcom/google/android/finsky/billing/iab/google/util/IabHelper$OnIabPurchaseFinishedListener;Ljava/lang/String;)V

    .line 336
    return-void
.end method

.method public launchPurchaseFlow(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;ILcom/google/android/finsky/billing/iab/google/util/IabHelper$OnIabPurchaseFinishedListener;Ljava/lang/String;)V
    .registers 21
    .param p1, "act"    # Landroid/app/Activity;
    .param p2, "sku"    # Ljava/lang/String;
    .param p3, "itemType"    # Ljava/lang/String;
    .param p4, "requestCode"    # I
    .param p5, "listener"    # Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnIabPurchaseFinishedListener;
    .param p6, "extraData"    # Ljava/lang/String;

    .prologue
    .line 368
    invoke-direct {p0}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->checkNotDisposed()V

    .line 369
    const-string v1, "launchPurchaseFlow"

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->checkSetupDone(Ljava/lang/String;)V

    .line 370
    const-string v1, "launchPurchaseFlow"

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->flagStartAsync(Ljava/lang/String;)V

    .line 373
    const-string v1, "subs"

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_30

    iget-boolean v1, p0, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->mSubscriptionsSupported:Z

    if-nez v1, :cond_30

    .line 374
    new-instance v11, Lcom/google/android/finsky/billing/iab/google/util/IabResult;

    const/16 v1, -0x3f1

    const-string v2, "Subscriptions are not available."

    invoke-direct {v11, v1, v2}, Lcom/google/android/finsky/billing/iab/google/util/IabResult;-><init>(ILjava/lang/String;)V

    .line 376
    .local v11, "r":Lcom/google/android/finsky/billing/iab/google/util/IabResult;
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->flagEndAsync()V

    .line 377
    if-eqz p5, :cond_2f

    const/4 v1, 0x0

    move-object/from16 v0, p5

    invoke-interface {v0, v11, v1}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnIabPurchaseFinishedListener;->onIabPurchaseFinished(Lcom/google/android/finsky/billing/iab/google/util/IabResult;Lcom/google/android/finsky/billing/iab/google/util/Purchase;)V

    .line 419
    .end local v11    # "r":Lcom/google/android/finsky/billing/iab/google/util/IabResult;
    :cond_2f
    :goto_2f
    return-void

    .line 382
    :cond_30
    :try_start_30
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Constructing buy intent for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p2

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", item type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p3

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->logDebug(Ljava/lang/String;)V

    .line 383
    iget-object v1, p0, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->mService:Lcom/google/android/finsky/billing/iab/google/util/IInAppBillingService;

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    move-object/from16 v6, p6

    invoke-interface/range {v1 .. v6}, Lcom/google/android/finsky/billing/iab/google/util/IInAppBillingService;->getBuyIntent(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v8

    .line 384
    .local v8, "buyIntentBundle":Landroid/os/Bundle;
    invoke-virtual {p0, v8}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->getResponseCodeFromBundle(Landroid/os/Bundle;)I

    move-result v12

    .line 385
    .local v12, "response":I
    if-eqz v12, :cond_cc

    .line 386
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unable to buy item, Error response: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v12}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->getResponseDesc(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->logError(Ljava/lang/String;)V

    .line 387
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->flagEndAsync()V

    .line 388
    new-instance v13, Lcom/google/android/finsky/billing/iab/google/util/IabResult;

    const-string v1, "Unable to buy item"

    invoke-direct {v13, v12, v1}, Lcom/google/android/finsky/billing/iab/google/util/IabResult;-><init>(ILjava/lang/String;)V

    .line 389
    .local v13, "result":Lcom/google/android/finsky/billing/iab/google/util/IabResult;
    if-eqz p5, :cond_2f

    const/4 v1, 0x0

    move-object/from16 v0, p5

    invoke-interface {v0, v13, v1}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnIabPurchaseFinishedListener;->onIabPurchaseFinished(Lcom/google/android/finsky/billing/iab/google/util/IabResult;Lcom/google/android/finsky/billing/iab/google/util/Purchase;)V
    :try_end_99
    .catch Landroid/content/IntentSender$SendIntentException; {:try_start_30 .. :try_end_99} :catch_9a
    .catch Landroid/os/RemoteException; {:try_start_30 .. :try_end_99} :catch_130

    goto :goto_2f

    .line 403
    .end local v8    # "buyIntentBundle":Landroid/os/Bundle;
    .end local v12    # "response":I
    .end local v13    # "result":Lcom/google/android/finsky/billing/iab/google/util/IabResult;
    :catch_9a
    move-exception v9

    .line 404
    .local v9, "e":Landroid/content/IntentSender$SendIntentException;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SendIntentException while launching purchase flow for sku "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p2

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->logError(Ljava/lang/String;)V

    .line 405
    invoke-virtual {v9}, Landroid/content/IntentSender$SendIntentException;->printStackTrace()V

    .line 406
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->flagEndAsync()V

    .line 408
    new-instance v13, Lcom/google/android/finsky/billing/iab/google/util/IabResult;

    const/16 v1, -0x3ec

    const-string v2, "Failed to send intent."

    invoke-direct {v13, v1, v2}, Lcom/google/android/finsky/billing/iab/google/util/IabResult;-><init>(ILjava/lang/String;)V

    .line 409
    .restart local v13    # "result":Lcom/google/android/finsky/billing/iab/google/util/IabResult;
    if-eqz p5, :cond_2f

    const/4 v1, 0x0

    move-object/from16 v0, p5

    invoke-interface {v0, v13, v1}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnIabPurchaseFinishedListener;->onIabPurchaseFinished(Lcom/google/android/finsky/billing/iab/google/util/IabResult;Lcom/google/android/finsky/billing/iab/google/util/Purchase;)V

    goto/16 :goto_2f

    .line 393
    .end local v9    # "e":Landroid/content/IntentSender$SendIntentException;
    .end local v13    # "result":Lcom/google/android/finsky/billing/iab/google/util/IabResult;
    .restart local v8    # "buyIntentBundle":Landroid/os/Bundle;
    .restart local v12    # "response":I
    :cond_cc
    :try_start_cc
    const-string v1, "BUY_INTENT"

    invoke-virtual {v8, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v10

    check-cast v10, Landroid/app/PendingIntent;

    .line 394
    .local v10, "pendingIntent":Landroid/app/PendingIntent;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Launching buy intent for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p2

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". Request code: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move/from16 v0, p4

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->logDebug(Ljava/lang/String;)V

    .line 395
    move/from16 v0, p4

    iput v0, p0, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->mRequestCode:I

    .line 396
    move-object/from16 v0, p5

    iput-object v0, p0, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->mPurchaseListener:Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnIabPurchaseFinishedListener;

    .line 397
    move-object/from16 v0, p3

    iput-object v0, p0, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->mPurchasingItemType:Ljava/lang/String;

    .line 398
    invoke-virtual {v10}, Landroid/app/PendingIntent;->getIntentSender()Landroid/content/IntentSender;

    move-result-object v2

    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    const/4 v1, 0x0

    .line 400
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v5

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v6

    const/4 v1, 0x0

    .line 401
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v7

    move-object v1, p1

    move/from16 v3, p4

    .line 398
    invoke-virtual/range {v1 .. v7}, Landroid/app/Activity;->startIntentSenderForResult(Landroid/content/IntentSender;ILandroid/content/Intent;III)V
    :try_end_12e
    .catch Landroid/content/IntentSender$SendIntentException; {:try_start_cc .. :try_end_12e} :catch_9a
    .catch Landroid/os/RemoteException; {:try_start_cc .. :try_end_12e} :catch_130

    goto/16 :goto_2f

    .line 411
    .end local v8    # "buyIntentBundle":Landroid/os/Bundle;
    .end local v10    # "pendingIntent":Landroid/app/PendingIntent;
    .end local v12    # "response":I
    :catch_130
    move-exception v9

    .line 412
    .local v9, "e":Landroid/os/RemoteException;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "RemoteException while launching purchase flow for sku "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p2

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->logError(Ljava/lang/String;)V

    .line 413
    invoke-virtual {v9}, Landroid/os/RemoteException;->printStackTrace()V

    .line 414
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->flagEndAsync()V

    .line 416
    new-instance v13, Lcom/google/android/finsky/billing/iab/google/util/IabResult;

    const/16 v1, -0x3e9

    const-string v2, "Remote exception while starting purchase flow"

    invoke-direct {v13, v1, v2}, Lcom/google/android/finsky/billing/iab/google/util/IabResult;-><init>(ILjava/lang/String;)V

    .line 417
    .restart local v13    # "result":Lcom/google/android/finsky/billing/iab/google/util/IabResult;
    if-eqz p5, :cond_2f

    const/4 v1, 0x0

    move-object/from16 v0, p5

    invoke-interface {v0, v13, v1}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnIabPurchaseFinishedListener;->onIabPurchaseFinished(Lcom/google/android/finsky/billing/iab/google/util/IabResult;Lcom/google/android/finsky/billing/iab/google/util/Purchase;)V

    goto/16 :goto_2f
.end method

.method public launchSubscriptionPurchaseFlow(Landroid/app/Activity;Ljava/lang/String;ILcom/google/android/finsky/billing/iab/google/util/IabHelper$OnIabPurchaseFinishedListener;)V
    .registers 11
    .param p1, "act"    # Landroid/app/Activity;
    .param p2, "sku"    # Ljava/lang/String;
    .param p3, "requestCode"    # I
    .param p4, "listener"    # Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnIabPurchaseFinishedListener;

    .prologue
    .line 340
    const-string v5, ""

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->launchSubscriptionPurchaseFlow(Landroid/app/Activity;Ljava/lang/String;ILcom/google/android/finsky/billing/iab/google/util/IabHelper$OnIabPurchaseFinishedListener;Ljava/lang/String;)V

    .line 341
    return-void
.end method

.method public launchSubscriptionPurchaseFlow(Landroid/app/Activity;Ljava/lang/String;ILcom/google/android/finsky/billing/iab/google/util/IabHelper$OnIabPurchaseFinishedListener;Ljava/lang/String;)V
    .registers 13
    .param p1, "act"    # Landroid/app/Activity;
    .param p2, "sku"    # Ljava/lang/String;
    .param p3, "requestCode"    # I
    .param p4, "listener"    # Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnIabPurchaseFinishedListener;
    .param p5, "extraData"    # Ljava/lang/String;

    .prologue
    .line 345
    const-string v3, "subs"

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->launchPurchaseFlow(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;ILcom/google/android/finsky/billing/iab/google/util/IabHelper$OnIabPurchaseFinishedListener;Ljava/lang/String;)V

    .line 346
    return-void
.end method

.method logDebug(Ljava/lang/String;)V
    .registers 3
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 978
    iget-boolean v0, p0, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->mDebugLog:Z

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->mDebugTag:Ljava/lang/String;

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 979
    :cond_9
    return-void
.end method

.method logError(Ljava/lang/String;)V
    .registers 5
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 982
    iget-object v0, p0, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->mDebugTag:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "In-app billing error: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 983
    return-void
.end method

.method logWarn(Ljava/lang/String;)V
    .registers 5
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 986
    iget-object v0, p0, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->mDebugTag:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "In-app billing warning: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 987
    return-void
.end method

.method public queryInventory(ZLjava/util/List;)Lcom/google/android/finsky/billing/iab/google/util/Inventory;
    .registers 4
    .param p1, "querySkuDetails"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/android/finsky/billing/iab/google/util/Inventory;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/finsky/billing/iab/google/util/IabException;
        }
    .end annotation

    .prologue
    .line 519
    .local p2, "moreSkus":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->queryInventory(ZLjava/util/List;Ljava/util/List;)Lcom/google/android/finsky/billing/iab/google/util/Inventory;

    move-result-object v0

    return-object v0
.end method

.method public queryInventory(ZLjava/util/List;Ljava/util/List;)Lcom/google/android/finsky/billing/iab/google/util/Inventory;
    .registers 10
    .param p1, "querySkuDetails"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/android/finsky/billing/iab/google/util/Inventory;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/finsky/billing/iab/google/util/IabException;
        }
    .end annotation

    .prologue
    .line 537
    .local p2, "moreItemSkus":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p3, "moreSubsSkus":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->checkNotDisposed()V

    .line 538
    const-string v3, "queryInventory"

    invoke-virtual {p0, v3}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->checkSetupDone(Ljava/lang/String;)V

    .line 540
    :try_start_8
    new-instance v1, Lcom/google/android/finsky/billing/iab/google/util/Inventory;

    invoke-direct {v1}, Lcom/google/android/finsky/billing/iab/google/util/Inventory;-><init>()V

    .line 541
    .local v1, "inv":Lcom/google/android/finsky/billing/iab/google/util/Inventory;
    const-string v3, "inapp"

    invoke-virtual {p0, v1, v3}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->queryPurchases(Lcom/google/android/finsky/billing/iab/google/util/Inventory;Ljava/lang/String;)I

    move-result v2

    .line 542
    .local v2, "r":I
    if-eqz v2, :cond_28

    .line 543
    new-instance v3, Lcom/google/android/finsky/billing/iab/google/util/IabException;

    const-string v4, "Error refreshing inventory (querying owned items)."

    invoke-direct {v3, v2, v4}, Lcom/google/android/finsky/billing/iab/google/util/IabException;-><init>(ILjava/lang/String;)V

    throw v3
    :try_end_1d
    .catch Landroid/os/RemoteException; {:try_start_8 .. :try_end_1d} :catch_1d
    .catch Lorg/json/JSONException; {:try_start_8 .. :try_end_1d} :catch_3a

    .line 570
    .end local v1    # "inv":Lcom/google/android/finsky/billing/iab/google/util/Inventory;
    .end local v2    # "r":I
    :catch_1d
    move-exception v0

    .line 571
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v3, Lcom/google/android/finsky/billing/iab/google/util/IabException;

    const/16 v4, -0x3e9

    const-string v5, "Remote exception while refreshing inventory."

    invoke-direct {v3, v4, v5, v0}, Lcom/google/android/finsky/billing/iab/google/util/IabException;-><init>(ILjava/lang/String;Ljava/lang/Exception;)V

    throw v3

    .line 546
    .end local v0    # "e":Landroid/os/RemoteException;
    .restart local v1    # "inv":Lcom/google/android/finsky/billing/iab/google/util/Inventory;
    .restart local v2    # "r":I
    :cond_28
    if-eqz p1, :cond_45

    .line 547
    :try_start_2a
    const-string v3, "inapp"

    invoke-virtual {p0, v3, v1, p2}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->querySkuDetails(Ljava/lang/String;Lcom/google/android/finsky/billing/iab/google/util/Inventory;Ljava/util/List;)I

    move-result v2

    .line 548
    if-eqz v2, :cond_45

    .line 549
    new-instance v3, Lcom/google/android/finsky/billing/iab/google/util/IabException;

    const-string v4, "Error refreshing inventory (querying prices of items)."

    invoke-direct {v3, v2, v4}, Lcom/google/android/finsky/billing/iab/google/util/IabException;-><init>(ILjava/lang/String;)V

    throw v3
    :try_end_3a
    .catch Landroid/os/RemoteException; {:try_start_2a .. :try_end_3a} :catch_1d
    .catch Lorg/json/JSONException; {:try_start_2a .. :try_end_3a} :catch_3a

    .line 573
    .end local v1    # "inv":Lcom/google/android/finsky/billing/iab/google/util/Inventory;
    .end local v2    # "r":I
    :catch_3a
    move-exception v0

    .line 574
    .local v0, "e":Lorg/json/JSONException;
    new-instance v3, Lcom/google/android/finsky/billing/iab/google/util/IabException;

    const/16 v4, -0x3ea

    const-string v5, "Error parsing JSON response while refreshing inventory."

    invoke-direct {v3, v4, v5, v0}, Lcom/google/android/finsky/billing/iab/google/util/IabException;-><init>(ILjava/lang/String;Ljava/lang/Exception;)V

    throw v3

    .line 554
    .end local v0    # "e":Lorg/json/JSONException;
    .restart local v1    # "inv":Lcom/google/android/finsky/billing/iab/google/util/Inventory;
    .restart local v2    # "r":I
    :cond_45
    :try_start_45
    iget-boolean v3, p0, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->mSubscriptionsSupported:Z

    if-eqz v3, :cond_6b

    .line 555
    const-string v3, "subs"

    invoke-virtual {p0, v1, v3}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->queryPurchases(Lcom/google/android/finsky/billing/iab/google/util/Inventory;Ljava/lang/String;)I

    move-result v2

    .line 556
    if-eqz v2, :cond_59

    .line 557
    new-instance v3, Lcom/google/android/finsky/billing/iab/google/util/IabException;

    const-string v4, "Error refreshing inventory (querying owned subscriptions)."

    invoke-direct {v3, v2, v4}, Lcom/google/android/finsky/billing/iab/google/util/IabException;-><init>(ILjava/lang/String;)V

    throw v3

    .line 560
    :cond_59
    if-eqz p1, :cond_6b

    .line 561
    const-string v3, "subs"

    invoke-virtual {p0, v3, v1, p2}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->querySkuDetails(Ljava/lang/String;Lcom/google/android/finsky/billing/iab/google/util/Inventory;Ljava/util/List;)I

    move-result v2

    .line 562
    if-eqz v2, :cond_6b

    .line 563
    new-instance v3, Lcom/google/android/finsky/billing/iab/google/util/IabException;

    const-string v4, "Error refreshing inventory (querying prices of subscriptions)."

    invoke-direct {v3, v2, v4}, Lcom/google/android/finsky/billing/iab/google/util/IabException;-><init>(ILjava/lang/String;)V

    throw v3
    :try_end_6b
    .catch Landroid/os/RemoteException; {:try_start_45 .. :try_end_6b} :catch_1d
    .catch Lorg/json/JSONException; {:try_start_45 .. :try_end_6b} :catch_3a

    .line 568
    :cond_6b
    return-object v1
.end method

.method public queryInventoryAsync(Lcom/google/android/finsky/billing/iab/google/util/IabHelper$QueryInventoryFinishedListener;)V
    .registers 4
    .param p1, "listener"    # Lcom/google/android/finsky/billing/iab/google/util/IabHelper$QueryInventoryFinishedListener;

    .prologue
    .line 636
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1, p1}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->queryInventoryAsync(ZLjava/util/List;Lcom/google/android/finsky/billing/iab/google/util/IabHelper$QueryInventoryFinishedListener;)V

    .line 637
    return-void
.end method

.method public queryInventoryAsync(ZLcom/google/android/finsky/billing/iab/google/util/IabHelper$QueryInventoryFinishedListener;)V
    .registers 4
    .param p1, "querySkuDetails"    # Z
    .param p2, "listener"    # Lcom/google/android/finsky/billing/iab/google/util/IabHelper$QueryInventoryFinishedListener;

    .prologue
    .line 640
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, p2}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->queryInventoryAsync(ZLjava/util/List;Lcom/google/android/finsky/billing/iab/google/util/IabHelper$QueryInventoryFinishedListener;)V

    .line 641
    return-void
.end method

.method public queryInventoryAsync(ZLjava/util/List;Lcom/google/android/finsky/billing/iab/google/util/IabHelper$QueryInventoryFinishedListener;)V
    .registers 11
    .param p1, "querySkuDetails"    # Z
    .param p3, "listener"    # Lcom/google/android/finsky/billing/iab/google/util/IabHelper$QueryInventoryFinishedListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/google/android/finsky/billing/iab/google/util/IabHelper$QueryInventoryFinishedListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 605
    .local p2, "moreSkus":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v5, Landroid/os/Handler;

    invoke-direct {v5}, Landroid/os/Handler;-><init>()V

    .line 606
    .local v5, "handler":Landroid/os/Handler;
    invoke-direct {p0}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->checkNotDisposed()V

    .line 607
    const-string v0, "queryInventory"

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->checkSetupDone(Ljava/lang/String;)V

    .line 608
    const-string v0, "refresh inventory"

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->flagStartAsync(Ljava/lang/String;)V

    .line 609
    new-instance v6, Ljava/lang/Thread;

    new-instance v0, Lcom/google/android/finsky/billing/iab/google/util/IabHelper$2;

    move-object v1, p0

    move v2, p1

    move-object v3, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper$2;-><init>(Lcom/google/android/finsky/billing/iab/google/util/IabHelper;ZLjava/util/List;Lcom/google/android/finsky/billing/iab/google/util/IabHelper$QueryInventoryFinishedListener;Landroid/os/Handler;)V

    invoke-direct {v6, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 632
    invoke-virtual {v6}, Ljava/lang/Thread;->start()V

    .line 633
    return-void
.end method

.method queryPurchases(Lcom/google/android/finsky/billing/iab/google/util/Inventory;Ljava/lang/String;)I
    .registers 19
    .param p1, "inv"    # Lcom/google/android/finsky/billing/iab/google/util/Inventory;
    .param p2, "itemType"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;,
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 832
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Querying owned items, item type: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p2

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->logDebug(Ljava/lang/String;)V

    .line 833
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Package name: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v14}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->logDebug(Ljava/lang/String;)V

    .line 834
    const/4 v12, 0x0

    .line 835
    .local v12, "verificationFailed":Z
    const/4 v1, 0x0

    .line 838
    .local v1, "continueToken":Ljava/lang/String;
    :cond_3c
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Calling getPurchases with continuation token: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->logDebug(Ljava/lang/String;)V

    .line 839
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->mService:Lcom/google/android/finsky/billing/iab/google/util/IInAppBillingService;

    const/4 v14, 0x3

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v15}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p2

    invoke-interface {v13, v14, v15, v0, v1}, Lcom/google/android/finsky/billing/iab/google/util/IInAppBillingService;->getPurchases(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v3

    .line 842
    .local v3, "ownedItems":Landroid/os/Bundle;
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->getResponseCodeFromBundle(Landroid/os/Bundle;)I

    move-result v8

    .line 843
    .local v8, "response":I
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Owned items response: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->logDebug(Ljava/lang/String;)V

    .line 844
    if-eqz v8, :cond_a8

    .line 845
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "getPurchases() failed: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-static {v8}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->getResponseDesc(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->logDebug(Ljava/lang/String;)V

    .line 890
    .end local v8    # "response":I
    :goto_a7
    return v8

    .line 848
    .restart local v8    # "response":I
    :cond_a8
    const-string v13, "INAPP_PURCHASE_ITEM_LIST"

    invoke-virtual {v3, v13}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_c0

    const-string v13, "INAPP_PURCHASE_DATA_LIST"

    .line 849
    invoke-virtual {v3, v13}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_c0

    const-string v13, "INAPP_DATA_SIGNATURE_LIST"

    .line 850
    invoke-virtual {v3, v13}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v13

    if-nez v13, :cond_ca

    .line 851
    :cond_c0
    const-string v13, "Bundle returned from getPurchases() doesn\'t contain required fields."

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->logError(Ljava/lang/String;)V

    .line 852
    const/16 v8, -0x3ea

    goto :goto_a7

    .line 855
    :cond_ca
    const-string v13, "INAPP_PURCHASE_ITEM_LIST"

    invoke-virtual {v3, v13}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v4

    .line 857
    .local v4, "ownedSkus":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v13, "INAPP_PURCHASE_DATA_LIST"

    invoke-virtual {v3, v13}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v7

    .line 859
    .local v7, "purchaseDataList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v13, "INAPP_DATA_SIGNATURE_LIST"

    invoke-virtual {v3, v13}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v10

    .line 862
    .local v10, "signatureList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_dd
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v13

    if-ge v2, v13, :cond_188

    .line 863
    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 864
    .local v6, "purchaseData":Ljava/lang/String;
    invoke-virtual {v10, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    .line 865
    .local v9, "signature":Ljava/lang/String;
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    .line 866
    .local v11, "sku":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->mSignatureBase64:Ljava/lang/String;

    invoke-static {v13, v6, v9}, Lcom/google/android/finsky/billing/iab/google/util/Security;->verifyPurchase(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_14f

    .line 867
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Sku is owned: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->logDebug(Ljava/lang/String;)V

    .line 868
    new-instance v5, Lcom/google/android/finsky/billing/iab/google/util/Purchase;

    move-object/from16 v0, p2

    invoke-direct {v5, v0, v6, v9}, Lcom/google/android/finsky/billing/iab/google/util/Purchase;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 870
    .local v5, "purchase":Lcom/google/android/finsky/billing/iab/google/util/Purchase;
    invoke-virtual {v5}, Lcom/google/android/finsky/billing/iab/google/util/Purchase;->getToken()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v13

    if-eqz v13, :cond_147

    .line 871
    const-string v13, "BUG: empty/null token!"

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->logWarn(Ljava/lang/String;)V

    .line 872
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Purchase data: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->logDebug(Ljava/lang/String;)V

    .line 876
    :cond_147
    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/google/android/finsky/billing/iab/google/util/Inventory;->addPurchase(Lcom/google/android/finsky/billing/iab/google/util/Purchase;)V

    .line 862
    .end local v5    # "purchase":Lcom/google/android/finsky/billing/iab/google/util/Purchase;
    :goto_14c
    add-int/lit8 v2, v2, 0x1

    goto :goto_dd

    .line 879
    :cond_14f
    const-string v13, "Purchase signature verification **FAILED**. Not adding item."

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->logWarn(Ljava/lang/String;)V

    .line 880
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "   Purchase data: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->logDebug(Ljava/lang/String;)V

    .line 881
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "   Signature: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->logDebug(Ljava/lang/String;)V

    .line 882
    const/4 v12, 0x1

    goto :goto_14c

    .line 886
    .end local v6    # "purchaseData":Ljava/lang/String;
    .end local v9    # "signature":Ljava/lang/String;
    .end local v11    # "sku":Ljava/lang/String;
    :cond_188
    const-string v13, "INAPP_CONTINUATION_TOKEN"

    invoke-virtual {v3, v13}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 887
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Continuation token: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->logDebug(Ljava/lang/String;)V

    .line 888
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v13

    if-eqz v13, :cond_3c

    .line 890
    if-eqz v12, :cond_1b3

    const/16 v13, -0x3eb

    :goto_1b0
    move v8, v13

    goto/16 :goto_a7

    :cond_1b3
    const/4 v13, 0x0

    goto :goto_1b0
.end method

.method querySkuDetails(Ljava/lang/String;Lcom/google/android/finsky/billing/iab/google/util/Inventory;Ljava/util/List;)I
    .registers 15
    .param p1, "itemType"    # Ljava/lang/String;
    .param p2, "inv"    # Lcom/google/android/finsky/billing/iab/google/util/Inventory;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/android/finsky/billing/iab/google/util/Inventory;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)I"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .local p3, "moreSkus":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v2, 0x0

    .line 895
    const-string v8, "Querying SKU details."

    invoke-virtual {p0, v8}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->logDebug(Ljava/lang/String;)V

    .line 896
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 897
    .local v6, "skuList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {p2, p1}, Lcom/google/android/finsky/billing/iab/google/util/Inventory;->getAllOwnedSkus(Ljava/lang/String;)Ljava/util/List;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 898
    if-eqz p3, :cond_2e

    .line 899
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_18
    :goto_18
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_2e

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 900
    .local v4, "sku":Ljava/lang/String;
    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_18

    .line 901
    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_18

    .line 906
    .end local v4    # "sku":Ljava/lang/String;
    :cond_2e
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-nez v8, :cond_3a

    .line 907
    const-string v8, "queryPrices: nothing to do because there are no SKUs."

    invoke-virtual {p0, v8}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->logDebug(Ljava/lang/String;)V

    .line 936
    :cond_39
    :goto_39
    return v2

    .line 911
    :cond_3a
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 912
    .local v1, "querySkus":Landroid/os/Bundle;
    const-string v8, "ITEM_ID_LIST"

    invoke-virtual {v1, v8, v6}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 913
    iget-object v8, p0, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->mService:Lcom/google/android/finsky/billing/iab/google/util/IInAppBillingService;

    const/4 v9, 0x3

    iget-object v10, p0, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v8, v9, v10, p1, v1}, Lcom/google/android/finsky/billing/iab/google/util/IInAppBillingService;->getSkuDetails(ILjava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v5

    .line 916
    .local v5, "skuDetails":Landroid/os/Bundle;
    const-string v8, "DETAILS_LIST"

    invoke-virtual {v5, v8}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_82

    .line 917
    invoke-virtual {p0, v5}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->getResponseCodeFromBundle(Landroid/os/Bundle;)I

    move-result v2

    .line 918
    .local v2, "response":I
    if-eqz v2, :cond_7a

    .line 919
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "getSkuDetails() failed: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {v2}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->getResponseDesc(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->logDebug(Ljava/lang/String;)V

    goto :goto_39

    .line 923
    :cond_7a
    const-string v8, "getSkuDetails() returned a bundle with neither an error nor a detail list."

    invoke-virtual {p0, v8}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->logError(Ljava/lang/String;)V

    .line 924
    const/16 v2, -0x3ea

    goto :goto_39

    .line 928
    .end local v2    # "response":I
    :cond_82
    const-string v8, "DETAILS_LIST"

    invoke-virtual {v5, v8}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    .line 931
    .local v3, "responseList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_8c
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_39

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 932
    .local v7, "thisResponse":Ljava/lang/String;
    new-instance v0, Lcom/google/android/finsky/billing/iab/google/util/SkuDetails;

    invoke-direct {v0, p1, v7}, Lcom/google/android/finsky/billing/iab/google/util/SkuDetails;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 933
    .local v0, "d":Lcom/google/android/finsky/billing/iab/google/util/SkuDetails;
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Got sku details: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v9}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->logDebug(Ljava/lang/String;)V

    .line 934
    invoke-virtual {p2, v0}, Lcom/google/android/finsky/billing/iab/google/util/Inventory;->addSkuDetails(Lcom/google/android/finsky/billing/iab/google/util/SkuDetails;)V

    goto :goto_8c
.end method

.method public startSetup(Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnIabSetupFinishedListener;)V
    .registers 6
    .param p1, "listener"    # Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnIabSetupFinishedListener;

    .prologue
    .line 203
    invoke-direct {p0}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->checkNotDisposed()V

    .line 204
    iget-boolean v1, p0, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->mSetupDone:Z

    if-eqz v1, :cond_f

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "IAB helper is already set up."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 207
    :cond_f
    const-string v1, "Starting in-app billing setup."

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->logDebug(Ljava/lang/String;)V

    .line 208
    new-instance v1, Lcom/google/android/finsky/billing/iab/google/util/IabHelper$1;

    invoke-direct {v1, p0, p1}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper$1;-><init>(Lcom/google/android/finsky/billing/iab/google/util/IabHelper;Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnIabSetupFinishedListener;)V

    iput-object v1, p0, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->mServiceConn:Landroid/content/ServiceConnection;

    .line 263
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.android.vending.billing.InAppBillingService.BIND"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 264
    .local v0, "serviceIntent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/pm/PackageManager;->queryIntentServices(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3c

    .line 266
    iget-object v1, p0, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->mServiceConn:Landroid/content/ServiceConnection;

    const/4 v3, 0x1

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 276
    :cond_3b
    :goto_3b
    return-void

    .line 270
    :cond_3c
    if-eqz p1, :cond_3b

    .line 271
    new-instance v1, Lcom/google/android/finsky/billing/iab/google/util/IabResult;

    const/4 v2, 0x3

    const-string v3, "Billing service unavailable on device."

    invoke-direct {v1, v2, v3}, Lcom/google/android/finsky/billing/iab/google/util/IabResult;-><init>(ILjava/lang/String;)V

    invoke-interface {p1, v1}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnIabSetupFinishedListener;->onIabSetupFinished(Lcom/google/android/finsky/billing/iab/google/util/IabResult;)V

    goto :goto_3b
.end method

.method public subscriptionsSupported()Z
    .registers 2

    .prologue
    .line 304
    invoke-direct {p0}, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->checkNotDisposed()V

    .line 305
    iget-boolean v0, p0, Lcom/google/android/finsky/billing/iab/google/util/IabHelper;->mSubscriptionsSupported:Z

    return v0
.end method
