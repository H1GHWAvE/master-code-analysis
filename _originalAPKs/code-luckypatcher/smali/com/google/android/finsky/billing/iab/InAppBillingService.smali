.class public Lcom/google/android/finsky/billing/iab/InAppBillingService;
.super Landroid/app/Service;
.source "InAppBillingService.java"


# static fields
.field public static final BILLING_RESPONSE_RESULT_BILLING_UNAVAILABLE:I = 0x3

.field public static final BILLING_RESPONSE_RESULT_OK:I = 0x0

.field public static final ITEM_TYPE_INAPP:Ljava/lang/String; = "inapp"

.field public static final ITEM_TYPE_SUBS:Ljava/lang/String; = "subs"

.field public static final TAG:Ljava/lang/String; = "BillingHack"

.field static mServiceConn:Landroid/content/ServiceConnection;


# instance fields
.field googleBillingDisabled:Z

.field mB:Landroid/os/IBinder;

.field private final mBinder:Lcom/google/android/finsky/billing/iab/google/util/IInAppBillingService$Stub;

.field mContext:Landroid/content/Context;

.field mService:Lcom/google/android/finsky/billing/iab/google/util/IInAppBillingService;

.field mSetupDone:Z

.field skipSetupDone:Z


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    const/4 v0, 0x0

    .line 30
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 39
    iput-boolean v0, p0, Lcom/google/android/finsky/billing/iab/InAppBillingService;->mSetupDone:Z

    .line 40
    iput-boolean v0, p0, Lcom/google/android/finsky/billing/iab/InAppBillingService;->skipSetupDone:Z

    .line 41
    iput-boolean v0, p0, Lcom/google/android/finsky/billing/iab/InAppBillingService;->googleBillingDisabled:Z

    .line 243
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/billing/iab/InAppBillingService;->mB:Landroid/os/IBinder;

    .line 322
    new-instance v0, Lcom/google/android/finsky/billing/iab/InAppBillingService$6;

    invoke-direct {v0, p0}, Lcom/google/android/finsky/billing/iab/InAppBillingService$6;-><init>(Lcom/google/android/finsky/billing/iab/InAppBillingService;)V

    iput-object v0, p0, Lcom/google/android/finsky/billing/iab/InAppBillingService;->mBinder:Lcom/google/android/finsky/billing/iab/google/util/IInAppBillingService$Stub;

    return-void
.end method


# virtual methods
.method public connectToBilling(Ljava/lang/String;)V
    .registers 13
    .param p1, "pkgName"    # Ljava/lang/String;

    .prologue
    const/4 v10, 0x0

    .line 154
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/iab/InAppBillingService;->startGoogleBilling()V

    .line 155
    iget-boolean v7, p0, Lcom/google/android/finsky/billing/iab/InAppBillingService;->mSetupDone:Z

    if-eqz v7, :cond_10

    new-instance v7, Ljava/lang/IllegalStateException;

    const-string v8, "IAB helper is already set up."

    invoke-direct {v7, v8}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 156
    :cond_10
    new-instance v7, Lcom/google/android/finsky/billing/iab/InAppBillingService$5;

    invoke-direct {v7, p0, p1}, Lcom/google/android/finsky/billing/iab/InAppBillingService$5;-><init>(Lcom/google/android/finsky/billing/iab/InAppBillingService;Ljava/lang/String;)V

    sput-object v7, Lcom/google/android/finsky/billing/iab/InAppBillingService;->mServiceConn:Landroid/content/ServiceConnection;

    .line 207
    new-instance v5, Landroid/content/Intent;

    const-string v7, "com.android.vending.billing.InAppBillingService.BIND"

    invoke-direct {v5, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 208
    .local v5, "serviceIntent":Landroid/content/Intent;
    const-string v7, "com.android.vending"

    invoke-virtual {v5, v7}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 209
    const-string v7, "xexe"

    const-string v8, "lp"

    invoke-virtual {v5, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 211
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getPkgMng()Landroid/content/pm/PackageManager;

    move-result-object v7

    invoke-virtual {v7, v5, v10}, Landroid/content/pm/PackageManager;->queryIntentServices(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_44

    new-instance v7, Lcom/chelpus/Utils;

    const-string v8, "w"

    invoke-direct {v7, v8}, Lcom/chelpus/Utils;-><init>(Ljava/lang/String;)V

    const-wide/16 v8, 0x7d0

    invoke-virtual {v7, v8, v9}, Lcom/chelpus/Utils;->waitLP(J)V

    .line 213
    :cond_44
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getPkgMng()Landroid/content/pm/PackageManager;

    move-result-object v7

    invoke-virtual {v7, v5, v10}, Landroid/content/pm/PackageManager;->queryIntentServices(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_c1

    .line 215
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getPkgMng()Landroid/content/pm/PackageManager;

    move-result-object v7

    invoke-virtual {v7, v5, v10}, Landroid/content/pm/PackageManager;->queryIntentServices(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v6

    .line 216
    .local v6, "services":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_5e
    :goto_5e
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_c8

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/pm/ResolveInfo;

    .line 217
    .local v4, "se":Landroid/content/pm/ResolveInfo;
    iget-object v8, v4, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    iget-object v8, v8, Landroid/content/pm/ServiceInfo;->packageName:Ljava/lang/String;

    if-eqz v8, :cond_5e

    iget-object v8, v4, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    iget-object v8, v8, Landroid/content/pm/ServiceInfo;->packageName:Ljava/lang/String;

    const-string v9, "com.android.vending"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5e

    .line 218
    iget-object v8, v4, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    iget-object v3, v8, Landroid/content/pm/ServiceInfo;->packageName:Ljava/lang/String;

    .line 219
    .local v3, "packageName":Ljava/lang/String;
    iget-object v8, v4, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    iget-object v0, v8, Landroid/content/pm/ServiceInfo;->name:Ljava/lang/String;

    .line 220
    .local v0, "className":Ljava/lang/String;
    new-instance v1, Landroid/content/ComponentName;

    invoke-direct {v1, v3, v0}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 221
    .local v1, "component":Landroid/content/ComponentName;
    new-instance v5, Landroid/content/Intent;

    .end local v5    # "serviceIntent":Landroid/content/Intent;
    invoke-direct {v5}, Landroid/content/Intent;-><init>()V

    .line 222
    .restart local v5    # "serviceIntent":Landroid/content/Intent;
    invoke-virtual {v5, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 223
    const-string v8, "xexe"

    const-string v9, "lp"

    invoke-virtual {v5, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 225
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v8

    sget-object v9, Lcom/google/android/finsky/billing/iab/InAppBillingService;->mServiceConn:Landroid/content/ServiceConnection;

    const/4 v10, 0x1

    invoke-virtual {v8, v5, v9, v10}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v8

    if-eqz v8, :cond_5e

    .line 226
    const/4 v2, 0x0

    .line 227
    .local v2, "i":I
    :cond_a6
    iget-boolean v8, p0, Lcom/google/android/finsky/billing/iab/InAppBillingService;->mSetupDone:Z

    if-nez v8, :cond_5e

    iget-boolean v8, p0, Lcom/google/android/finsky/billing/iab/InAppBillingService;->skipSetupDone:Z

    if-nez v8, :cond_5e

    .line 228
    new-instance v8, Lcom/chelpus/Utils;

    const-string v9, "w"

    invoke-direct {v8, v9}, Lcom/chelpus/Utils;-><init>(Ljava/lang/String;)V

    const-wide/16 v9, 0x1f4

    invoke-virtual {v8, v9, v10}, Lcom/chelpus/Utils;->waitLP(J)V

    .line 229
    add-int/lit8 v2, v2, 0x1

    .line 230
    const/16 v8, 0x1e

    if-ne v2, v8, :cond_a6

    goto :goto_5e

    .line 239
    .end local v0    # "className":Ljava/lang/String;
    .end local v1    # "component":Landroid/content/ComponentName;
    .end local v2    # "i":I
    .end local v3    # "packageName":Ljava/lang/String;
    .end local v4    # "se":Landroid/content/pm/ResolveInfo;
    .end local v6    # "services":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    :cond_c1
    sget-object v7, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v8, "Billing service unavailable on device."

    invoke-virtual {v7, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 242
    :cond_c8
    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .registers 4
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 307
    if-eqz p1, :cond_25

    .line 308
    const-string v0, "xexe"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1e

    const-string v0, "xexe"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "lp"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1e

    invoke-virtual {p1}, Landroid/content/Intent;->getPackage()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_28

    .line 309
    :cond_1e
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "Connect from patch."

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 318
    :cond_25
    :goto_25
    iget-object v0, p0, Lcom/google/android/finsky/billing/iab/InAppBillingService;->mBinder:Lcom/google/android/finsky/billing/iab/google/util/IInAppBillingService$Stub;

    return-object v0

    .line 311
    :cond_28
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "Connect from proxy."

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 312
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/finsky/billing/iab/InAppBillingService;->mSetupDone:Z

    .line 313
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/billing/iab/InAppBillingService;->skipSetupDone:Z

    goto :goto_25
.end method

.method public onCreate()V
    .registers 4

    .prologue
    .line 103
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 104
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "create bill+skip:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/android/finsky/billing/iab/InAppBillingService;->skipSetupDone:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 105
    iput-object p0, p0, Lcom/google/android/finsky/billing/iab/InAppBillingService;->mContext:Landroid/content/Context;

    .line 106
    sget-boolean v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->su:Z

    if-eqz v0, :cond_26

    .line 107
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/iab/InAppBillingService;->startGoogleBilling()V

    .line 110
    :cond_26
    return-void
.end method

.method public onDestroy()V
    .registers 3

    .prologue
    .line 126
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "destroy billing"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 127
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 128
    sget-boolean v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->su:Z

    if-eqz v0, :cond_1f

    iget-boolean v0, p0, Lcom/google/android/finsky/billing/iab/InAppBillingService;->googleBillingDisabled:Z

    if-eqz v0, :cond_1f

    .line 129
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/google/android/finsky/billing/iab/InAppBillingService$3;

    invoke-direct {v1, p0}, Lcom/google/android/finsky/billing/iab/InAppBillingService$3;-><init>(Lcom/google/android/finsky/billing/iab/InAppBillingService;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 134
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 136
    :cond_1f
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .registers 6
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 91
    if-eqz p1, :cond_1f

    .line 92
    const-string v0, "xexe"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_18

    const-string v0, "xexe"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "lp"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_24

    .line 93
    :cond_18
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "Connect from app."

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 99
    :cond_1f
    :goto_1f
    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    move-result v0

    return v0

    .line 95
    :cond_24
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/billing/iab/InAppBillingService;->skipSetupDone:Z

    goto :goto_1f
.end method

.method public onTaskRemoved(Landroid/content/Intent;)V
    .registers 4
    .param p1, "rootInent"    # Landroid/content/Intent;

    .prologue
    .line 113
    invoke-super {p0, p1}, Landroid/app/Service;->onTaskRemoved(Landroid/content/Intent;)V

    .line 114
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "on Task Removed billing"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 115
    sget-boolean v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->su:Z

    if-eqz v0, :cond_1f

    iget-boolean v0, p0, Lcom/google/android/finsky/billing/iab/InAppBillingService;->googleBillingDisabled:Z

    if-eqz v0, :cond_1f

    .line 116
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/google/android/finsky/billing/iab/InAppBillingService$2;

    invoke-direct {v1, p0}, Lcom/google/android/finsky/billing/iab/InAppBillingService$2;-><init>(Lcom/google/android/finsky/billing/iab/InAppBillingService;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 121
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 123
    :cond_1f
    return-void
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .registers 4
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 139
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "destroy billing"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 140
    sget-boolean v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->su:Z

    if-eqz v0, :cond_1c

    iget-boolean v0, p0, Lcom/google/android/finsky/billing/iab/InAppBillingService;->googleBillingDisabled:Z

    if-eqz v0, :cond_1c

    .line 141
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/google/android/finsky/billing/iab/InAppBillingService$4;

    invoke-direct {v1, p0}, Lcom/google/android/finsky/billing/iab/InAppBillingService$4;-><init>(Lcom/google/android/finsky/billing/iab/InAppBillingService;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 146
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 148
    :cond_1c
    invoke-super {p0, p1}, Landroid/app/Service;->onUnbind(Landroid/content/Intent;)Z

    move-result v0

    return v0
.end method

.method startGoogleBilling()V
    .registers 11

    .prologue
    const/4 v9, 0x1

    .line 43
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->init()V

    .line 44
    sget-boolean v5, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->su:Z

    if-eqz v5, :cond_7e

    .line 45
    const/4 v3, 0x0

    .line 47
    .local v3, "info":Landroid/content/pm/PackageInfo;
    :try_start_9
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getPkgMng()Landroid/content/pm/PackageManager;

    move-result-object v5

    const-string v6, "com.android.vending"

    const/4 v7, 0x4

    invoke-virtual {v5, v6, v7}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_13
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_9 .. :try_end_13} :catch_74

    move-result-object v3

    .line 53
    :goto_14
    if-eqz v3, :cond_7e

    iget-object v5, v3, Landroid/content/pm/PackageInfo;->services:[Landroid/content/pm/ServiceInfo;

    if-eqz v5, :cond_7e

    iget-object v5, v3, Landroid/content/pm/PackageInfo;->services:[Landroid/content/pm/ServiceInfo;

    array-length v5, v5

    if-eqz v5, :cond_7e

    .line 54
    const/4 v0, 0x0

    .local v0, "d":I
    :goto_20
    iget-object v5, v3, Landroid/content/pm/PackageInfo;->services:[Landroid/content/pm/ServiceInfo;

    array-length v5, v5

    if-ge v0, v5, :cond_7e

    .line 57
    :try_start_25
    iget-object v5, v3, Landroid/content/pm/PackageInfo;->services:[Landroid/content/pm/ServiceInfo;

    aget-object v5, v5, v0

    iget-object v5, v5, Landroid/content/pm/ServiceInfo;->name:Ljava/lang/String;

    const-string v6, "InAppBillingService"

    invoke-virtual {v5, v6}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_41

    iget-object v5, v3, Landroid/content/pm/PackageInfo;->services:[Landroid/content/pm/ServiceInfo;

    aget-object v5, v5, v0

    iget-object v5, v5, Landroid/content/pm/ServiceInfo;->name:Ljava/lang/String;

    const-string v6, "MarketBillingService"

    .line 58
    invoke-virtual {v5, v6}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_71

    :cond_41
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getPkgMng()Landroid/content/pm/PackageManager;

    move-result-object v5

    new-instance v6, Landroid/content/ComponentName;

    const-string v7, "com.android.vending"

    iget-object v8, v3, Landroid/content/pm/PackageInfo;->services:[Landroid/content/pm/ServiceInfo;

    aget-object v8, v8, v0

    iget-object v8, v8, Landroid/content/pm/ServiceInfo;->name:Ljava/lang/String;

    invoke-direct {v6, v7, v8}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6}, Landroid/content/pm/PackageManager;->getComponentEnabledSetting(Landroid/content/ComponentName;)I

    move-result v5

    if-eq v5, v9, :cond_71

    .line 59
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/google/android/finsky/billing/iab/InAppBillingService;->googleBillingDisabled:Z

    .line 61
    const/4 v5, 0x1

    invoke-static {v5}, Lcom/chelpus/Utils;->market_billing_services(Z)V

    .line 62
    new-instance v4, Ljava/util/Timer;

    const-string v5, "FirstRunTimer"

    invoke-direct {v4, v5}, Ljava/util/Timer;-><init>(Ljava/lang/String;)V

    .line 63
    .local v4, "timer2":Ljava/util/Timer;
    new-instance v5, Lcom/google/android/finsky/billing/iab/InAppBillingService$1;

    invoke-direct {v5, p0}, Lcom/google/android/finsky/billing/iab/InAppBillingService$1;-><init>(Lcom/google/android/finsky/billing/iab/InAppBillingService;)V

    const-wide/32 v6, 0xea60

    invoke-virtual {v4, v5, v6, v7}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V
    :try_end_71
    .catch Ljava/lang/Exception; {:try_start_25 .. :try_end_71} :catch_79

    .line 54
    .end local v4    # "timer2":Ljava/util/Timer;
    :cond_71
    :goto_71
    add-int/lit8 v0, v0, 0x1

    goto :goto_20

    .line 49
    .end local v0    # "d":I
    :catch_74
    move-exception v2

    .line 51
    .local v2, "e1":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v2}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_14

    .line 80
    .end local v2    # "e1":Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v0    # "d":I
    :catch_79
    move-exception v1

    .line 81
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_71

    .line 88
    .end local v0    # "d":I
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v3    # "info":Landroid/content/pm/PackageInfo;
    :cond_7e
    return-void
.end method
