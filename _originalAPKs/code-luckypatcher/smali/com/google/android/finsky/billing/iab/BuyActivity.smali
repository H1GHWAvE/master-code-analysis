.class public Lcom/google/android/finsky/billing/iab/BuyActivity;
.super Landroid/app/Activity;
.source "BuyActivity.java"


# static fields
.field public static final BUY_INTENT:Ljava/lang/String; = "com.google.android.finsky.billing.iab.BUY"

.field public static final EXTRA_DEV_PAYLOAD:Ljava/lang/String; = "payload"

.field public static final EXTRA_PACKAGENAME:Ljava/lang/String; = "packageName"

.field public static final EXTRA_PRODUCT_ID:Ljava/lang/String; = "product"

.field public static final TAG:Ljava/lang/String; = "BillingHack"


# instance fields
.field bundle:Landroid/os/Bundle;

.field public context:Lcom/google/android/finsky/billing/iab/BuyActivity;

.field devPayload:Ljava/lang/String;

.field packageName:Ljava/lang/String;

.field productId:Ljava/lang/String;

.field type:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 22
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 30
    iput-object v1, p0, Lcom/google/android/finsky/billing/iab/BuyActivity;->context:Lcom/google/android/finsky/billing/iab/BuyActivity;

    .line 31
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/billing/iab/BuyActivity;->packageName:Ljava/lang/String;

    .line 32
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/billing/iab/BuyActivity;->productId:Ljava/lang/String;

    .line 33
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/billing/iab/BuyActivity;->devPayload:Ljava/lang/String;

    .line 34
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/billing/iab/BuyActivity;->type:Ljava/lang/String;

    .line 35
    iput-object v1, p0, Lcom/google/android/finsky/billing/iab/BuyActivity;->bundle:Landroid/os/Bundle;

    return-void
.end method


# virtual methods
.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .registers 4
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 184
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    .line 186
    .local v0, "currentOrientation":I
    const/4 v1, 0x2

    if-ne v0, v1, :cond_d

    .line 187
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/billing/iab/BuyActivity;->setRequestedOrientation(I)V

    .line 192
    :goto_9
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 194
    return-void

    .line 190
    :cond_d
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/billing/iab/BuyActivity;->setRequestedOrientation(I)V

    goto :goto_9
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 25
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 40
    invoke-super/range {p0 .. p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 41
    move-object/from16 v0, p0

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/finsky/billing/iab/BuyActivity;->context:Lcom/google/android/finsky/billing/iab/BuyActivity;

    .line 42
    const-string v2, "com.google.android.finsky.billing.iab.BUY"

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/billing/iab/BuyActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1b

    if-eqz p1, :cond_23b

    .line 43
    :cond_1b
    const-string v2, "BillingHack"

    const-string v3, "Buy intent!"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 44
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/billing/iab/BuyActivity;->getRequestedOrientation()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(I)V

    .line 45
    if-eqz p1, :cond_1e7

    .line 46
    const-string v2, "packageName"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/finsky/billing/iab/BuyActivity;->packageName:Ljava/lang/String;

    .line 47
    const-string v2, "product"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/finsky/billing/iab/BuyActivity;->productId:Ljava/lang/String;

    .line 48
    const-string v2, "payload"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/finsky/billing/iab/BuyActivity;->devPayload:Ljava/lang/String;

    .line 49
    const-string v2, "Type"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/finsky/billing/iab/BuyActivity;->type:Ljava/lang/String;

    .line 56
    :goto_5d
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/billing/iab/BuyActivity;->type:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 57
    new-instance v14, Lorg/json/JSONObject;

    invoke-direct {v14}, Lorg/json/JSONObject;-><init>()V

    .line 59
    .local v14, "purch":Lorg/json/JSONObject;
    :try_start_6b
    const-string v2, "orderId"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-wide v17, 0xde0b6b3a7640000L

    const-wide v19, 0x7fffffffffffffffL

    invoke-static/range {v17 .. v20}, Lcom/chelpus/Utils;->getRandom(JJ)J

    move-result-wide v17

    const-wide/16 v19, 0x0

    const-wide/16 v21, 0x9

    invoke-static/range {v19 .. v22}, Lcom/chelpus/Utils;->getRandom(JJ)J

    move-result-wide v19

    add-long v17, v17, v19

    move-wide/from16 v0, v17

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v17, "."

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-wide v17, 0x38d7ea4c68000L

    const-wide v19, 0x2386f26fc0ffffL

    invoke-static/range {v17 .. v20}, Lcom/chelpus/Utils;->getRandom(JJ)J

    move-result-wide v17

    move-wide/from16 v0, v17

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v14, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 60
    const-string v2, "packageName"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/billing/iab/BuyActivity;->packageName:Ljava/lang/String;

    invoke-virtual {v14, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 61
    const-string v2, "productId"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/billing/iab/BuyActivity;->productId:Ljava/lang/String;

    invoke-virtual {v14, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 62
    const-string v2, "purchaseTime"

    new-instance v3, Ljava/lang/Long;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v17

    move-wide/from16 v0, v17

    invoke-direct {v3, v0, v1}, Ljava/lang/Long;-><init>(J)V

    invoke-virtual {v14, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 63
    const-string v2, "purchaseState"

    new-instance v3, Ljava/lang/Integer;

    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-direct {v3, v0}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v14, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 64
    const-string v2, "developerPayload"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/billing/iab/BuyActivity;->devPayload:Ljava/lang/String;

    invoke-virtual {v14, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 65
    const-string v2, "purchaseToken"

    const/16 v3, 0x18

    invoke-static {v3}, Lcom/chelpus/Utils;->getRandomStringLowerCase(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v14, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_f7
    .catch Lorg/json/JSONException; {:try_start_6b .. :try_end_f7} :catch_231

    .line 70
    :goto_f7
    invoke-virtual {v14}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v5

    .line 71
    .local v5, "pData":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/billing/iab/BuyActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "autorepeat"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 72
    .local v8, "autorepeat":Ljava/lang/String;
    if-eqz v8, :cond_13f

    .line 73
    new-instance v11, Landroid/content/Intent;

    invoke-direct {v11}, Landroid/content/Intent;-><init>()V

    .line 74
    .local v11, "data":Landroid/content/Intent;
    new-instance v13, Landroid/os/Bundle;

    invoke-direct {v13}, Landroid/os/Bundle;-><init>()V

    .line 75
    .local v13, "extras":Landroid/os/Bundle;
    const-string v15, ""

    .line 77
    .local v15, "signature":Ljava/lang/String;
    const-string v2, "1"

    invoke-virtual {v8, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_237

    .line 78
    invoke-static {v5}, Lcom/chelpus/Utils;->gen_sha1withrsa(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 83
    :goto_123
    const-string v2, "RESPONSE_CODE"

    const/4 v3, 0x0

    invoke-virtual {v13, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 84
    const-string v2, "INAPP_PURCHASE_DATA"

    invoke-virtual {v13, v2, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    const-string v2, "INAPP_DATA_SIGNATURE"

    invoke-virtual {v13, v2, v15}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    invoke-virtual {v11, v13}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 87
    const/4 v2, -0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v11}, Lcom/google/android/finsky/billing/iab/BuyActivity;->setResult(ILandroid/content/Intent;)V

    .line 89
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/billing/iab/BuyActivity;->finish()V

    .line 91
    .end local v11    # "data":Landroid/content/Intent;
    .end local v13    # "extras":Landroid/os/Bundle;
    .end local v15    # "signature":Ljava/lang/String;
    :cond_13f
    const v2, 0x7f040012

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/finsky/billing/iab/BuyActivity;->setContentView(I)V

    .line 98
    const v2, 0x7f0d0007

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/finsky/billing/iab/BuyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/Button;

    .line 99
    .local v10, "btnYes":Landroid/widget/Button;
    const v2, 0x7f0d0008

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/finsky/billing/iab/BuyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/Button;

    .line 100
    .local v9, "btnNo":Landroid/widget/Button;
    const v2, 0x7f0d0053

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/finsky/billing/iab/BuyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/CheckBox;

    .line 101
    .local v4, "check":Landroid/widget/CheckBox;
    invoke-static {}, Lcom/chelpus/Utils;->checkCoreJarPatch11()Z

    move-result v2

    if-nez v2, :cond_17e

    .line 102
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/billing/iab/BuyActivity;->packageName:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-static {v2, v0}, Lcom/chelpus/Utils;->isRebuildedOrOdex(Ljava/lang/String;Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_17e

    .line 103
    const/4 v2, 0x1

    invoke-virtual {v4, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 105
    :cond_17e
    const v2, 0x7f0d0054

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/finsky/billing/iab/BuyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/CheckBox;

    .line 106
    .local v6, "check2":Landroid/widget/CheckBox;
    const v2, 0x7f0d0055

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/finsky/billing/iab/BuyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/CheckBox;

    .line 107
    .local v7, "check3":Landroid/widget/CheckBox;
    const v2, 0x7f0d0047

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/finsky/billing/iab/BuyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Landroid/widget/TextView;

    .line 108
    .local v16, "text2":Landroid/widget/TextView;
    const v2, 0x7f070026

    invoke-static {v2}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 109
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const v3, 0x7f070027

    invoke-static {v3}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    .line 110
    const/4 v2, 0x0

    invoke-virtual {v6, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 111
    const/4 v2, 0x0

    invoke-virtual {v7, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 112
    new-instance v2, Lcom/google/android/finsky/billing/iab/BuyActivity$1;

    move-object/from16 v3, p0

    invoke-direct/range {v2 .. v7}, Lcom/google/android/finsky/billing/iab/BuyActivity$1;-><init>(Lcom/google/android/finsky/billing/iab/BuyActivity;Landroid/widget/CheckBox;Ljava/lang/String;Landroid/widget/CheckBox;Landroid/widget/CheckBox;)V

    invoke-virtual {v10, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 146
    new-instance v2, Lcom/google/android/finsky/billing/iab/BuyActivity$2;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/google/android/finsky/billing/iab/BuyActivity$2;-><init>(Lcom/google/android/finsky/billing/iab/BuyActivity;)V

    invoke-virtual {v9, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 165
    .end local v4    # "check":Landroid/widget/CheckBox;
    .end local v5    # "pData":Ljava/lang/String;
    .end local v6    # "check2":Landroid/widget/CheckBox;
    .end local v7    # "check3":Landroid/widget/CheckBox;
    .end local v8    # "autorepeat":Ljava/lang/String;
    .end local v9    # "btnNo":Landroid/widget/Button;
    .end local v10    # "btnYes":Landroid/widget/Button;
    .end local v14    # "purch":Lorg/json/JSONObject;
    .end local v16    # "text2":Landroid/widget/TextView;
    :goto_1e6
    return-void

    .line 51
    :cond_1e7
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/billing/iab/BuyActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "packageName"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/finsky/billing/iab/BuyActivity;->packageName:Ljava/lang/String;

    .line 52
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/billing/iab/BuyActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "product"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/finsky/billing/iab/BuyActivity;->productId:Ljava/lang/String;

    .line 53
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/billing/iab/BuyActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "payload"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/finsky/billing/iab/BuyActivity;->devPayload:Ljava/lang/String;

    .line 54
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/billing/iab/BuyActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "Type"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/finsky/billing/iab/BuyActivity;->type:Ljava/lang/String;

    goto/16 :goto_5d

    .line 67
    .restart local v14    # "purch":Lorg/json/JSONObject;
    :catch_231
    move-exception v12

    .line 68
    .local v12, "e":Lorg/json/JSONException;
    invoke-virtual {v12}, Lorg/json/JSONException;->printStackTrace()V

    goto/16 :goto_f7

    .line 82
    .end local v12    # "e":Lorg/json/JSONException;
    .restart local v5    # "pData":Ljava/lang/String;
    .restart local v8    # "autorepeat":Ljava/lang/String;
    .restart local v11    # "data":Landroid/content/Intent;
    .restart local v13    # "extras":Landroid/os/Bundle;
    .restart local v15    # "signature":Ljava/lang/String;
    :cond_237
    const-string v15, ""

    goto/16 :goto_123

    .line 163
    .end local v5    # "pData":Ljava/lang/String;
    .end local v8    # "autorepeat":Ljava/lang/String;
    .end local v11    # "data":Landroid/content/Intent;
    .end local v13    # "extras":Landroid/os/Bundle;
    .end local v14    # "purch":Lorg/json/JSONObject;
    .end local v15    # "signature":Ljava/lang/String;
    :cond_23b
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/billing/iab/BuyActivity;->finish()V

    goto :goto_1e6
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .registers 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 178
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "load instance"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 179
    const-string v0, "packageName"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/iab/BuyActivity;->packageName:Ljava/lang/String;

    .line 180
    invoke-super {p0, p1}, Landroid/app/Activity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 181
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 4
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 168
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "save instance"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 169
    const-string v0, "packageName"

    iget-object v1, p0, Lcom/google/android/finsky/billing/iab/BuyActivity;->packageName:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    const-string v0, "product"

    iget-object v1, p0, Lcom/google/android/finsky/billing/iab/BuyActivity;->productId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 171
    const-string v0, "payload"

    iget-object v1, p0, Lcom/google/android/finsky/billing/iab/BuyActivity;->devPayload:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 172
    const-string v0, "Type"

    iget-object v1, p0, Lcom/google/android/finsky/billing/iab/BuyActivity;->type:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 174
    return-void
.end method
