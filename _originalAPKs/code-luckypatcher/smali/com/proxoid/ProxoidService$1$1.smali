.class Lcom/proxoid/ProxoidService$1$1;
.super Lcom/mba/proxylight/ProxyLight;
.source "ProxoidService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/proxoid/ProxoidService$1;->update()Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/proxoid/ProxoidService$1;


# direct methods
.method constructor <init>(Lcom/proxoid/ProxoidService$1;)V
    .registers 2
    .param p1, "this$1"    # Lcom/proxoid/ProxoidService$1;

    .prologue
    .line 67
    iput-object p1, p0, Lcom/proxoid/ProxoidService$1$1;->this$1:Lcom/proxoid/ProxoidService$1;

    invoke-direct {p0}, Lcom/mba/proxylight/ProxyLight;-><init>()V

    return-void
.end method


# virtual methods
.method public debug(Ljava/lang/String;)V
    .registers 4
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 70
    const-string v0, "ProxoidService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 71
    const-string v0, "ProxoidService"

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 73
    :cond_e
    const-string v0, "ProxoidService"

    invoke-static {v0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 74
    return-void
.end method

.method public error(Ljava/lang/String;Ljava/lang/Throwable;)V
    .registers 4
    .param p1, "message"    # Ljava/lang/String;
    .param p2, "t"    # Ljava/lang/Throwable;

    .prologue
    .line 77
    const-string v0, "ProxoidService"

    invoke-static {v0, p1, p2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 78
    return-void
.end method
