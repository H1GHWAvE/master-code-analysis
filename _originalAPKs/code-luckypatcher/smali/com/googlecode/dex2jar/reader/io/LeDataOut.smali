.class public Lcom/googlecode/dex2jar/reader/io/LeDataOut;
.super Ljava/lang/Object;
.source "LeDataOut.java"

# interfaces
.implements Lcom/googlecode/dex2jar/reader/io/DataOut;


# instance fields
.field private os:Ljava/io/OutputStream;


# direct methods
.method public constructor <init>(Ljava/io/OutputStream;)V
    .registers 2
    .param p1, "os"    # Ljava/io/OutputStream;

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput-object p1, p0, Lcom/googlecode/dex2jar/reader/io/LeDataOut;->os:Ljava/io/OutputStream;

    .line 13
    return-void
.end method


# virtual methods
.method public writeByte(I)V
    .registers 3
    .param p1, "v"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 17
    iget-object v0, p0, Lcom/googlecode/dex2jar/reader/io/LeDataOut;->os:Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write(I)V

    .line 18
    return-void
.end method

.method public writeBytes([B)V
    .registers 3
    .param p1, "bs"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 22
    iget-object v0, p0, Lcom/googlecode/dex2jar/reader/io/LeDataOut;->os:Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write([B)V

    .line 23
    return-void
.end method

.method public writeInt(I)V
    .registers 4
    .param p1, "v"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 27
    iget-object v0, p0, Lcom/googlecode/dex2jar/reader/io/LeDataOut;->os:Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write(I)V

    .line 28
    iget-object v0, p0, Lcom/googlecode/dex2jar/reader/io/LeDataOut;->os:Ljava/io/OutputStream;

    shr-int/lit8 v1, p1, 0x8

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write(I)V

    .line 29
    iget-object v0, p0, Lcom/googlecode/dex2jar/reader/io/LeDataOut;->os:Ljava/io/OutputStream;

    shr-int/lit8 v1, p1, 0x10

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write(I)V

    .line 30
    iget-object v0, p0, Lcom/googlecode/dex2jar/reader/io/LeDataOut;->os:Ljava/io/OutputStream;

    ushr-int/lit8 v1, p1, 0x18

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write(I)V

    .line 31
    return-void
.end method

.method public writeShort(I)V
    .registers 4
    .param p1, "v"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 35
    iget-object v0, p0, Lcom/googlecode/dex2jar/reader/io/LeDataOut;->os:Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write(I)V

    .line 36
    iget-object v0, p0, Lcom/googlecode/dex2jar/reader/io/LeDataOut;->os:Ljava/io/OutputStream;

    shr-int/lit8 v1, p1, 0x8

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write(I)V

    .line 37
    return-void
.end method
