.class Lcom/mba/proxylight/ProxyLight$1;
.super Ljava/lang/Object;
.source "ProxyLight.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mba/proxylight/ProxyLight;->start()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mba/proxylight/ProxyLight;


# direct methods
.method constructor <init>(Lcom/mba/proxylight/ProxyLight;)V
    .registers 2
    .param p1, "this$0"    # Lcom/mba/proxylight/ProxyLight;

    .prologue
    .line 62
    iput-object p1, p0, Lcom/mba/proxylight/ProxyLight$1;->this$0:Lcom/mba/proxylight/ProxyLight;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .registers 11

    .prologue
    const/4 v9, 0x0

    .line 69
    :try_start_1
    iget-object v6, p0, Lcom/mba/proxylight/ProxyLight$1;->this$0:Lcom/mba/proxylight/ProxyLight;

    invoke-static {}, Ljava/nio/channels/ServerSocketChannel;->open()Ljava/nio/channels/ServerSocketChannel;

    move-result-object v7

    iput-object v7, v6, Lcom/mba/proxylight/ProxyLight;->server:Ljava/nio/channels/ServerSocketChannel;

    .line 70
    iget-object v6, p0, Lcom/mba/proxylight/ProxyLight$1;->this$0:Lcom/mba/proxylight/ProxyLight;

    iget-object v6, v6, Lcom/mba/proxylight/ProxyLight;->server:Ljava/nio/channels/ServerSocketChannel;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Ljava/nio/channels/ServerSocketChannel;->configureBlocking(Z)Ljava/nio/channels/SelectableChannel;

    .line 71
    iget-object v6, p0, Lcom/mba/proxylight/ProxyLight$1;->this$0:Lcom/mba/proxylight/ProxyLight;

    iget-object v6, v6, Lcom/mba/proxylight/ProxyLight;->server:Ljava/nio/channels/ServerSocketChannel;

    invoke-virtual {v6}, Ljava/nio/channels/ServerSocketChannel;->socket()Ljava/net/ServerSocket;

    move-result-object v6

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Ljava/net/ServerSocket;->setReuseAddress(Z)V

    .line 72
    iget-object v6, p0, Lcom/mba/proxylight/ProxyLight$1;->this$0:Lcom/mba/proxylight/ProxyLight;

    iget-object v6, v6, Lcom/mba/proxylight/ProxyLight;->server:Ljava/nio/channels/ServerSocketChannel;

    invoke-virtual {v6}, Ljava/nio/channels/ServerSocketChannel;->socket()Ljava/net/ServerSocket;

    move-result-object v6

    new-instance v7, Ljava/net/InetSocketAddress;

    const/16 v8, 0x1f90

    invoke-direct {v7, v8}, Ljava/net/InetSocketAddress;-><init>(I)V

    invoke-virtual {v6, v7}, Ljava/net/ServerSocket;->bind(Ljava/net/SocketAddress;)V

    .line 74
    iget-object v6, p0, Lcom/mba/proxylight/ProxyLight$1;->this$0:Lcom/mba/proxylight/ProxyLight;

    invoke-static {}, Ljava/nio/channels/Selector;->open()Ljava/nio/channels/Selector;

    move-result-object v7

    # setter for: Lcom/mba/proxylight/ProxyLight;->selector:Ljava/nio/channels/Selector;
    invoke-static {v6, v7}, Lcom/mba/proxylight/ProxyLight;->access$002(Lcom/mba/proxylight/ProxyLight;Ljava/nio/channels/Selector;)Ljava/nio/channels/Selector;

    .line 75
    iget-object v6, p0, Lcom/mba/proxylight/ProxyLight$1;->this$0:Lcom/mba/proxylight/ProxyLight;

    iget-object v6, v6, Lcom/mba/proxylight/ProxyLight;->server:Ljava/nio/channels/ServerSocketChannel;

    iget-object v7, p0, Lcom/mba/proxylight/ProxyLight$1;->this$0:Lcom/mba/proxylight/ProxyLight;

    # getter for: Lcom/mba/proxylight/ProxyLight;->selector:Ljava/nio/channels/Selector;
    invoke-static {v7}, Lcom/mba/proxylight/ProxyLight;->access$000(Lcom/mba/proxylight/ProxyLight;)Ljava/nio/channels/Selector;

    move-result-object v7

    const/16 v8, 0x10

    invoke-virtual {v6, v7, v8}, Ljava/nio/channels/ServerSocketChannel;->register(Ljava/nio/channels/Selector;I)Ljava/nio/channels/SelectionKey;

    .line 78
    :cond_47
    iget-object v6, p0, Lcom/mba/proxylight/ProxyLight$1;->this$0:Lcom/mba/proxylight/ProxyLight;

    # getter for: Lcom/mba/proxylight/ProxyLight;->selector:Ljava/nio/channels/Selector;
    invoke-static {v6}, Lcom/mba/proxylight/ProxyLight;->access$000(Lcom/mba/proxylight/ProxyLight;)Ljava/nio/channels/Selector;

    move-result-object v6

    invoke-virtual {v6}, Ljava/nio/channels/Selector;->select()I

    .line 79
    iget-object v6, p0, Lcom/mba/proxylight/ProxyLight$1;->this$0:Lcom/mba/proxylight/ProxyLight;

    iget-object v6, v6, Lcom/mba/proxylight/ProxyLight;->server:Ljava/nio/channels/ServerSocketChannel;
    :try_end_54
    .catch Ljava/nio/channels/ClosedSelectorException; {:try_start_1 .. :try_end_54} :catch_ce
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_54} :catch_db
    .catchall {:try_start_1 .. :try_end_54} :catchall_e8

    if-nez v6, :cond_5b

    .line 150
    iget-object v6, p0, Lcom/mba/proxylight/ProxyLight$1;->this$0:Lcom/mba/proxylight/ProxyLight;

    iput-boolean v9, v6, Lcom/mba/proxylight/ProxyLight;->running:Z

    .line 152
    :goto_5a
    return-void

    .line 82
    :cond_5b
    :try_start_5b
    iget-object v6, p0, Lcom/mba/proxylight/ProxyLight$1;->this$0:Lcom/mba/proxylight/ProxyLight;

    # getter for: Lcom/mba/proxylight/ProxyLight;->selector:Ljava/nio/channels/Selector;
    invoke-static {v6}, Lcom/mba/proxylight/ProxyLight;->access$000(Lcom/mba/proxylight/ProxyLight;)Ljava/nio/channels/Selector;

    move-result-object v6

    invoke-virtual {v6}, Ljava/nio/channels/Selector;->selectedKeys()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 84
    .local v1, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/nio/channels/SelectionKey;>;"
    :cond_69
    :goto_69
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_47

    .line 85
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/nio/channels/SelectionKey;

    .line 86
    .local v2, "key":Ljava/nio/channels/SelectionKey;
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    .line 88
    invoke-virtual {v2}, Ljava/nio/channels/SelectionKey;->isValid()Z

    move-result v6

    if-eqz v6, :cond_69

    .line 89
    invoke-virtual {v2}, Ljava/nio/channels/SelectionKey;->isAcceptable()Z
    :try_end_81
    .catch Ljava/nio/channels/ClosedSelectorException; {:try_start_5b .. :try_end_81} :catch_ce
    .catch Ljava/lang/Throwable; {:try_start_5b .. :try_end_81} :catch_db
    .catchall {:try_start_5b .. :try_end_81} :catchall_e8

    move-result v6

    if-eqz v6, :cond_69

    .line 91
    const/4 v3, 0x0

    .line 92
    .local v3, "p":Lcom/mba/proxylight/RequestProcessor;
    :try_start_85
    iget-object v6, p0, Lcom/mba/proxylight/ProxyLight$1;->this$0:Lcom/mba/proxylight/ProxyLight;

    # getter for: Lcom/mba/proxylight/ProxyLight;->processors:Ljava/util/Stack;
    invoke-static {v6}, Lcom/mba/proxylight/ProxyLight;->access$100(Lcom/mba/proxylight/ProxyLight;)Ljava/util/Stack;

    move-result-object v7

    monitor-enter v7
    :try_end_8c
    .catch Ljava/lang/Throwable; {:try_start_85 .. :try_end_8c} :catch_bf
    .catch Ljava/nio/channels/ClosedSelectorException; {:try_start_85 .. :try_end_8c} :catch_ce
    .catchall {:try_start_85 .. :try_end_8c} :catchall_e8

    .line 93
    :cond_8c
    :try_start_8c
    iget-object v6, p0, Lcom/mba/proxylight/ProxyLight$1;->this$0:Lcom/mba/proxylight/ProxyLight;

    # getter for: Lcom/mba/proxylight/ProxyLight;->processors:Ljava/util/Stack;
    invoke-static {v6}, Lcom/mba/proxylight/ProxyLight;->access$100(Lcom/mba/proxylight/ProxyLight;)Ljava/util/Stack;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/Stack;->size()I

    move-result v6

    if-lez v6, :cond_ac

    iget-object v6, p0, Lcom/mba/proxylight/ProxyLight$1;->this$0:Lcom/mba/proxylight/ProxyLight;

    # getter for: Lcom/mba/proxylight/ProxyLight;->processors:Ljava/util/Stack;
    invoke-static {v6}, Lcom/mba/proxylight/ProxyLight;->access$100(Lcom/mba/proxylight/ProxyLight;)Ljava/util/Stack;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v6

    move-object v0, v6

    check-cast v0, Lcom/mba/proxylight/RequestProcessor;

    move-object v3, v0

    invoke-virtual {v3}, Lcom/mba/proxylight/RequestProcessor;->isAlive()Z
    :try_end_a9
    .catchall {:try_start_8c .. :try_end_a9} :catchall_cb

    move-result v6

    if-eqz v6, :cond_8c

    :cond_ac
    move-object v4, v3

    .line 94
    .end local v3    # "p":Lcom/mba/proxylight/RequestProcessor;
    .local v4, "p":Lcom/mba/proxylight/RequestProcessor;
    :try_start_ad
    monitor-exit v7
    :try_end_ae
    .catchall {:try_start_ad .. :try_end_ae} :catchall_f1

    .line 95
    if-eqz v4, :cond_b6

    :try_start_b0
    invoke-virtual {v4}, Lcom/mba/proxylight/RequestProcessor;->isAlive()Z

    move-result v6

    if-nez v6, :cond_f4

    .line 96
    :cond_b6
    new-instance v3, Lcom/mba/proxylight/ProxyLight$1$1;

    invoke-direct {v3, p0}, Lcom/mba/proxylight/ProxyLight$1$1;-><init>(Lcom/mba/proxylight/ProxyLight$1;)V
    :try_end_bb
    .catch Ljava/lang/Throwable; {:try_start_b0 .. :try_end_bb} :catch_ee
    .catch Ljava/nio/channels/ClosedSelectorException; {:try_start_b0 .. :try_end_bb} :catch_ce
    .catchall {:try_start_b0 .. :try_end_bb} :catchall_e8

    .line 132
    .end local v4    # "p":Lcom/mba/proxylight/RequestProcessor;
    .restart local v3    # "p":Lcom/mba/proxylight/RequestProcessor;
    :goto_bb
    :try_start_bb
    invoke-virtual {v3, v2}, Lcom/mba/proxylight/RequestProcessor;->process(Ljava/nio/channels/SelectionKey;)V
    :try_end_be
    .catch Ljava/lang/Throwable; {:try_start_bb .. :try_end_be} :catch_bf
    .catch Ljava/nio/channels/ClosedSelectorException; {:try_start_bb .. :try_end_be} :catch_ce
    .catchall {:try_start_bb .. :try_end_be} :catchall_e8

    goto :goto_69

    .line 133
    :catch_bf
    move-exception v5

    .line 134
    .local v5, "t":Ljava/lang/Throwable;
    :goto_c0
    :try_start_c0
    iget-object v6, p0, Lcom/mba/proxylight/ProxyLight$1;->this$0:Lcom/mba/proxylight/ProxyLight;

    iget-object v6, v6, Lcom/mba/proxylight/ProxyLight;->server:Ljava/nio/channels/ServerSocketChannel;
    :try_end_c4
    .catch Ljava/nio/channels/ClosedSelectorException; {:try_start_c0 .. :try_end_c4} :catch_ce
    .catch Ljava/lang/Throwable; {:try_start_c0 .. :try_end_c4} :catch_db
    .catchall {:try_start_c0 .. :try_end_c4} :catchall_e8

    if-nez v6, :cond_d4

    .line 150
    iget-object v6, p0, Lcom/mba/proxylight/ProxyLight$1;->this$0:Lcom/mba/proxylight/ProxyLight;

    iput-boolean v9, v6, Lcom/mba/proxylight/ProxyLight;->running:Z

    goto :goto_5a

    .line 94
    .end local v5    # "t":Ljava/lang/Throwable;
    :catchall_cb
    move-exception v6

    :goto_cc
    :try_start_cc
    monitor-exit v7
    :try_end_cd
    .catchall {:try_start_cc .. :try_end_cd} :catchall_cb

    :try_start_cd
    throw v6
    :try_end_ce
    .catch Ljava/lang/Throwable; {:try_start_cd .. :try_end_ce} :catch_bf
    .catch Ljava/nio/channels/ClosedSelectorException; {:try_start_cd .. :try_end_ce} :catch_ce
    .catchall {:try_start_cd .. :try_end_ce} :catchall_e8

    .line 145
    .end local v1    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/nio/channels/SelectionKey;>;"
    .end local v2    # "key":Ljava/nio/channels/SelectionKey;
    .end local v3    # "p":Lcom/mba/proxylight/RequestProcessor;
    :catch_ce
    move-exception v6

    .line 150
    iget-object v6, p0, Lcom/mba/proxylight/ProxyLight$1;->this$0:Lcom/mba/proxylight/ProxyLight;

    iput-boolean v9, v6, Lcom/mba/proxylight/ProxyLight;->running:Z

    goto :goto_5a

    .line 138
    .restart local v1    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/nio/channels/SelectionKey;>;"
    .restart local v2    # "key":Ljava/nio/channels/SelectionKey;
    .restart local v3    # "p":Lcom/mba/proxylight/RequestProcessor;
    .restart local v5    # "t":Ljava/lang/Throwable;
    :cond_d4
    :try_start_d4
    iget-object v6, p0, Lcom/mba/proxylight/ProxyLight$1;->this$0:Lcom/mba/proxylight/ProxyLight;

    const/4 v7, 0x0

    invoke-virtual {v6, v7, v5}, Lcom/mba/proxylight/ProxyLight;->error(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_da
    .catch Ljava/nio/channels/ClosedSelectorException; {:try_start_d4 .. :try_end_da} :catch_ce
    .catch Ljava/lang/Throwable; {:try_start_d4 .. :try_end_da} :catch_db
    .catchall {:try_start_d4 .. :try_end_da} :catchall_e8

    goto :goto_69

    .line 147
    .end local v1    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/nio/channels/SelectionKey;>;"
    .end local v2    # "key":Ljava/nio/channels/SelectionKey;
    .end local v3    # "p":Lcom/mba/proxylight/RequestProcessor;
    .end local v5    # "t":Ljava/lang/Throwable;
    :catch_db
    move-exception v5

    .line 148
    .restart local v5    # "t":Ljava/lang/Throwable;
    :try_start_dc
    iget-object v6, p0, Lcom/mba/proxylight/ProxyLight$1;->this$0:Lcom/mba/proxylight/ProxyLight;

    const/4 v7, 0x0

    invoke-virtual {v6, v7, v5}, Lcom/mba/proxylight/ProxyLight;->error(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_e2
    .catchall {:try_start_dc .. :try_end_e2} :catchall_e8

    .line 150
    iget-object v6, p0, Lcom/mba/proxylight/ProxyLight$1;->this$0:Lcom/mba/proxylight/ProxyLight;

    iput-boolean v9, v6, Lcom/mba/proxylight/ProxyLight;->running:Z

    goto/16 :goto_5a

    .end local v5    # "t":Ljava/lang/Throwable;
    :catchall_e8
    move-exception v6

    iget-object v7, p0, Lcom/mba/proxylight/ProxyLight$1;->this$0:Lcom/mba/proxylight/ProxyLight;

    iput-boolean v9, v7, Lcom/mba/proxylight/ProxyLight;->running:Z

    throw v6

    .line 133
    .restart local v1    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/nio/channels/SelectionKey;>;"
    .restart local v2    # "key":Ljava/nio/channels/SelectionKey;
    .restart local v4    # "p":Lcom/mba/proxylight/RequestProcessor;
    :catch_ee
    move-exception v5

    move-object v3, v4

    .end local v4    # "p":Lcom/mba/proxylight/RequestProcessor;
    .restart local v3    # "p":Lcom/mba/proxylight/RequestProcessor;
    goto :goto_c0

    .line 94
    .end local v3    # "p":Lcom/mba/proxylight/RequestProcessor;
    .restart local v4    # "p":Lcom/mba/proxylight/RequestProcessor;
    :catchall_f1
    move-exception v6

    move-object v3, v4

    .end local v4    # "p":Lcom/mba/proxylight/RequestProcessor;
    .restart local v3    # "p":Lcom/mba/proxylight/RequestProcessor;
    goto :goto_cc

    .end local v3    # "p":Lcom/mba/proxylight/RequestProcessor;
    .restart local v4    # "p":Lcom/mba/proxylight/RequestProcessor;
    :cond_f4
    move-object v3, v4

    .end local v4    # "p":Lcom/mba/proxylight/RequestProcessor;
    .restart local v3    # "p":Lcom/mba/proxylight/RequestProcessor;
    goto :goto_bb
.end method
