.class public abstract Lcom/mba/proxylight/RequestProcessor;
.super Ljava/lang/Object;
.source "RequestProcessor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mba/proxylight/RequestProcessor$REQUEST_STEP;
    }
.end annotation


# static fields
.field private static final CONNECT_OK:[B

.field private static final CRLF:Ljava/lang/String; = "\r\n"

.field private static SOCKET_TIMEOUT:J

.field private static processorsCount:I

.field private static processorsCpt:I


# instance fields
.field private alive:Z

.field private currentOutSocket:Lcom/mba/proxylight/Socket;

.field private inData:Ljava/lang/String;

.field private inSocket:Lcom/mba/proxylight/Socket;

.field private outData:Ljava/lang/String;

.field private outSockets:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/mba/proxylight/Socket;",
            ">;"
        }
    .end annotation
.end field

.field private processorIdx:I

.field private readBuffer:Ljava/nio/ByteBuffer;

.field private read_buf:[C

.field read_offset:I

.field private selector:Ljava/nio/channels/Selector;

.field private shutdown:Z

.field private t:Ljava/lang/Thread;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 25
    const/4 v0, 0x1

    sput v0, Lcom/mba/proxylight/RequestProcessor;->processorsCpt:I

    .line 26
    const/4 v0, 0x0

    sput v0, Lcom/mba/proxylight/RequestProcessor;->processorsCount:I

    .line 28
    const-wide/16 v0, 0x3a98

    sput-wide v0, Lcom/mba/proxylight/RequestProcessor;->SOCKET_TIMEOUT:J

    .line 41
    const-string v0, "HTTP/1.0 200 Connection established\r\nProxy-agent: ProxyLight\r\n\r\n"

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    sput-object v0, Lcom/mba/proxylight/RequestProcessor;->CONNECT_OK:[B

    return-void
.end method

.method public constructor <init>()V
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v3, 0x80

    const/4 v4, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object v1, p0, Lcom/mba/proxylight/RequestProcessor;->t:Ljava/lang/Thread;

    .line 22
    iput-boolean v2, p0, Lcom/mba/proxylight/RequestProcessor;->alive:Z

    .line 23
    iput-boolean v2, p0, Lcom/mba/proxylight/RequestProcessor;->shutdown:Z

    .line 27
    iput v4, p0, Lcom/mba/proxylight/RequestProcessor;->processorIdx:I

    .line 31
    iput-object v1, p0, Lcom/mba/proxylight/RequestProcessor;->selector:Ljava/nio/channels/Selector;

    .line 32
    invoke-static {v3}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/mba/proxylight/RequestProcessor;->readBuffer:Ljava/nio/ByteBuffer;

    .line 33
    iput-object v1, p0, Lcom/mba/proxylight/RequestProcessor;->inSocket:Lcom/mba/proxylight/Socket;

    .line 34
    const-string v0, ""

    iput-object v0, p0, Lcom/mba/proxylight/RequestProcessor;->inData:Ljava/lang/String;

    .line 35
    const-string v0, ""

    iput-object v0, p0, Lcom/mba/proxylight/RequestProcessor;->outData:Ljava/lang/String;

    .line 37
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/mba/proxylight/RequestProcessor;->outSockets:Ljava/util/Map;

    .line 38
    iput-object v1, p0, Lcom/mba/proxylight/RequestProcessor;->currentOutSocket:Lcom/mba/proxylight/Socket;

    .line 374
    new-array v0, v3, [C

    iput-object v0, p0, Lcom/mba/proxylight/RequestProcessor;->read_buf:[C

    .line 375
    iput v2, p0, Lcom/mba/proxylight/RequestProcessor;->read_offset:I

    .line 49
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/mba/proxylight/RequestProcessor$1;

    invoke-direct {v1, p0}, Lcom/mba/proxylight/RequestProcessor$1;-><init>(Lcom/mba/proxylight/RequestProcessor;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/mba/proxylight/RequestProcessor;->t:Ljava/lang/Thread;

    .line 267
    iget-object v0, p0, Lcom/mba/proxylight/RequestProcessor;->t:Ljava/lang/Thread;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ProxyLight processor - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/mba/proxylight/RequestProcessor;->processorsCpt:I

    add-int/lit8 v3, v2, 0x1

    sput v3, Lcom/mba/proxylight/RequestProcessor;->processorsCpt:I

    iput v2, p0, Lcom/mba/proxylight/RequestProcessor;->processorIdx:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    .line 268
    iget-object v0, p0, Lcom/mba/proxylight/RequestProcessor;->t:Ljava/lang/Thread;

    invoke-virtual {v0, v4}, Ljava/lang/Thread;->setDaemon(Z)V

    .line 270
    iget-object v0, p0, Lcom/mba/proxylight/RequestProcessor;->t:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 272
    :goto_67
    invoke-virtual {p0}, Lcom/mba/proxylight/RequestProcessor;->isAlive()Z

    move-result v0

    if-nez v0, :cond_7a

    .line 273
    new-instance v0, Lcom/chelpus/Utils;

    const-string v1, "w"

    invoke-direct {v0, v1}, Lcom/chelpus/Utils;-><init>(Ljava/lang/String;)V

    const-wide/16 v1, 0x32

    invoke-virtual {v0, v1, v2}, Lcom/chelpus/Utils;->waitLP(J)V

    goto :goto_67

    .line 276
    :cond_7a
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Processeur "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/mba/proxylight/RequestProcessor;->processorIdx:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " demarre."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/mba/proxylight/RequestProcessor;->debug(Ljava/lang/String;)V

    .line 277
    return-void
.end method

.method static synthetic access$008()I
    .registers 2

    .prologue
    .line 20
    sget v0, Lcom/mba/proxylight/RequestProcessor;->processorsCount:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/mba/proxylight/RequestProcessor;->processorsCount:I

    return v0
.end method

.method static synthetic access$010()I
    .registers 2

    .prologue
    .line 20
    sget v0, Lcom/mba/proxylight/RequestProcessor;->processorsCount:I

    add-int/lit8 v1, v0, -0x1

    sput v1, Lcom/mba/proxylight/RequestProcessor;->processorsCount:I

    return v0
.end method

.method static synthetic access$1000(Lcom/mba/proxylight/RequestProcessor;)Ljava/nio/ByteBuffer;
    .registers 2
    .param p0, "x0"    # Lcom/mba/proxylight/RequestProcessor;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/mba/proxylight/RequestProcessor;->readBuffer:Ljava/nio/ByteBuffer;

    return-object v0
.end method

.method static synthetic access$102(Lcom/mba/proxylight/RequestProcessor;Z)Z
    .registers 2
    .param p0, "x0"    # Lcom/mba/proxylight/RequestProcessor;
    .param p1, "x1"    # Z

    .prologue
    .line 20
    iput-boolean p1, p0, Lcom/mba/proxylight/RequestProcessor;->alive:Z

    return p1
.end method

.method static synthetic access$1100(Lcom/mba/proxylight/RequestProcessor;Lcom/mba/proxylight/Socket;Ljava/nio/ByteBuffer;J)I
    .registers 6
    .param p0, "x0"    # Lcom/mba/proxylight/RequestProcessor;
    .param p1, "x1"    # Lcom/mba/proxylight/Socket;
    .param p2, "x2"    # Ljava/nio/ByteBuffer;
    .param p3, "x3"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 20
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/mba/proxylight/RequestProcessor;->read(Lcom/mba/proxylight/Socket;Ljava/nio/ByteBuffer;J)I

    move-result v0

    return v0
.end method

.method static synthetic access$1200(Lcom/mba/proxylight/RequestProcessor;)Ljava/lang/String;
    .registers 2
    .param p0, "x0"    # Lcom/mba/proxylight/RequestProcessor;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/mba/proxylight/RequestProcessor;->inData:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1202(Lcom/mba/proxylight/RequestProcessor;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .param p0, "x0"    # Lcom/mba/proxylight/RequestProcessor;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 20
    iput-object p1, p0, Lcom/mba/proxylight/RequestProcessor;->inData:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1300(Lcom/mba/proxylight/RequestProcessor;Ljava/nio/ByteBuffer;)Ljava/lang/String;
    .registers 3
    .param p0, "x0"    # Lcom/mba/proxylight/RequestProcessor;
    .param p1, "x1"    # Ljava/nio/ByteBuffer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 20
    invoke-direct {p0, p1}, Lcom/mba/proxylight/RequestProcessor;->readNext(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1400(Lcom/mba/proxylight/RequestProcessor;Lcom/mba/proxylight/Request;)Z
    .registers 3
    .param p0, "x0"    # Lcom/mba/proxylight/RequestProcessor;
    .param p1, "x1"    # Lcom/mba/proxylight/Request;

    .prologue
    .line 20
    invoke-direct {p0, p1}, Lcom/mba/proxylight/RequestProcessor;->filterRequest(Lcom/mba/proxylight/Request;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1500()[B
    .registers 1

    .prologue
    .line 20
    sget-object v0, Lcom/mba/proxylight/RequestProcessor;->CONNECT_OK:[B

    return-object v0
.end method

.method static synthetic access$1600(Lcom/mba/proxylight/RequestProcessor;Lcom/mba/proxylight/Socket;Ljava/nio/ByteBuffer;J)V
    .registers 5
    .param p0, "x0"    # Lcom/mba/proxylight/RequestProcessor;
    .param p1, "x1"    # Lcom/mba/proxylight/Socket;
    .param p2, "x2"    # Ljava/nio/ByteBuffer;
    .param p3, "x3"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 20
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/mba/proxylight/RequestProcessor;->write(Lcom/mba/proxylight/Socket;Ljava/nio/ByteBuffer;J)V

    return-void
.end method

.method static synthetic access$1700(Lcom/mba/proxylight/RequestProcessor;Lcom/mba/proxylight/Socket;)V
    .registers 2
    .param p0, "x0"    # Lcom/mba/proxylight/RequestProcessor;
    .param p1, "x1"    # Lcom/mba/proxylight/Socket;

    .prologue
    .line 20
    invoke-direct {p0, p1}, Lcom/mba/proxylight/RequestProcessor;->closeOutSocket(Lcom/mba/proxylight/Socket;)V

    return-void
.end method

.method static synthetic access$1800(Lcom/mba/proxylight/RequestProcessor;Lcom/mba/proxylight/Socket;Lcom/mba/proxylight/Socket;J)Z
    .registers 6
    .param p0, "x0"    # Lcom/mba/proxylight/RequestProcessor;
    .param p1, "x1"    # Lcom/mba/proxylight/Socket;
    .param p2, "x2"    # Lcom/mba/proxylight/Socket;
    .param p3, "x3"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 20
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/mba/proxylight/RequestProcessor;->transfer(Lcom/mba/proxylight/Socket;Lcom/mba/proxylight/Socket;J)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1900(Lcom/mba/proxylight/RequestProcessor;)Ljava/lang/String;
    .registers 2
    .param p0, "x0"    # Lcom/mba/proxylight/RequestProcessor;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/mba/proxylight/RequestProcessor;->outData:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1902(Lcom/mba/proxylight/RequestProcessor;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .param p0, "x0"    # Lcom/mba/proxylight/RequestProcessor;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 20
    iput-object p1, p0, Lcom/mba/proxylight/RequestProcessor;->outData:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$200(Lcom/mba/proxylight/RequestProcessor;)Ljava/nio/channels/Selector;
    .registers 2
    .param p0, "x0"    # Lcom/mba/proxylight/RequestProcessor;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/mba/proxylight/RequestProcessor;->selector:Ljava/nio/channels/Selector;

    return-object v0
.end method

.method static synthetic access$300(Lcom/mba/proxylight/RequestProcessor;)Z
    .registers 2
    .param p0, "x0"    # Lcom/mba/proxylight/RequestProcessor;

    .prologue
    .line 20
    iget-boolean v0, p0, Lcom/mba/proxylight/RequestProcessor;->shutdown:Z

    return v0
.end method

.method static synthetic access$400(Lcom/mba/proxylight/RequestProcessor;)Lcom/mba/proxylight/Socket;
    .registers 2
    .param p0, "x0"    # Lcom/mba/proxylight/RequestProcessor;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/mba/proxylight/RequestProcessor;->inSocket:Lcom/mba/proxylight/Socket;

    return-object v0
.end method

.method static synthetic access$500()J
    .registers 2

    .prologue
    .line 20
    sget-wide v0, Lcom/mba/proxylight/RequestProcessor;->SOCKET_TIMEOUT:J

    return-wide v0
.end method

.method static synthetic access$600(Lcom/mba/proxylight/RequestProcessor;)Ljava/util/Map;
    .registers 2
    .param p0, "x0"    # Lcom/mba/proxylight/RequestProcessor;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/mba/proxylight/RequestProcessor;->outSockets:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$700(Lcom/mba/proxylight/RequestProcessor;)I
    .registers 2
    .param p0, "x0"    # Lcom/mba/proxylight/RequestProcessor;

    .prologue
    .line 20
    iget v0, p0, Lcom/mba/proxylight/RequestProcessor;->processorIdx:I

    return v0
.end method

.method static synthetic access$800(Lcom/mba/proxylight/RequestProcessor;)V
    .registers 1
    .param p0, "x0"    # Lcom/mba/proxylight/RequestProcessor;

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/mba/proxylight/RequestProcessor;->closeAll()V

    return-void
.end method

.method static synthetic access$900(Lcom/mba/proxylight/RequestProcessor;)Lcom/mba/proxylight/Socket;
    .registers 2
    .param p0, "x0"    # Lcom/mba/proxylight/RequestProcessor;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/mba/proxylight/RequestProcessor;->currentOutSocket:Lcom/mba/proxylight/Socket;

    return-object v0
.end method

.method static synthetic access$902(Lcom/mba/proxylight/RequestProcessor;Lcom/mba/proxylight/Socket;)Lcom/mba/proxylight/Socket;
    .registers 2
    .param p0, "x0"    # Lcom/mba/proxylight/RequestProcessor;
    .param p1, "x1"    # Lcom/mba/proxylight/Socket;

    .prologue
    .line 20
    iput-object p1, p0, Lcom/mba/proxylight/RequestProcessor;->currentOutSocket:Lcom/mba/proxylight/Socket;

    return-object p1
.end method

.method private closeAll()V
    .registers 6

    .prologue
    const/4 v4, 0x0

    .line 310
    iget-object v2, p0, Lcom/mba/proxylight/RequestProcessor;->inSocket:Lcom/mba/proxylight/Socket;

    if-eqz v2, :cond_e

    .line 312
    :try_start_5
    iget-object v2, p0, Lcom/mba/proxylight/RequestProcessor;->inSocket:Lcom/mba/proxylight/Socket;

    iget-object v2, v2, Lcom/mba/proxylight/Socket;->socket:Ljava/nio/channels/SocketChannel;

    invoke-virtual {v2}, Ljava/nio/channels/SocketChannel;->close()V
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_c} :catch_2f

    .line 316
    :goto_c
    iput-object v4, p0, Lcom/mba/proxylight/RequestProcessor;->inSocket:Lcom/mba/proxylight/Socket;

    .line 318
    :cond_e
    iget-object v2, p0, Lcom/mba/proxylight/RequestProcessor;->outSockets:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_18
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_34

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mba/proxylight/Socket;

    .line 320
    .local v1, "outSocket":Lcom/mba/proxylight/Socket;
    :try_start_24
    iget-object v3, v1, Lcom/mba/proxylight/Socket;->socket:Ljava/nio/channels/SocketChannel;

    invoke-virtual {v3}, Ljava/nio/channels/SocketChannel;->close()V
    :try_end_29
    .catch Ljava/lang/Exception; {:try_start_24 .. :try_end_29} :catch_2a

    goto :goto_18

    .line 321
    :catch_2a
    move-exception v0

    .line 322
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {p0, v4, v0}, Lcom/mba/proxylight/RequestProcessor;->error(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_18

    .line 313
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v1    # "outSocket":Lcom/mba/proxylight/Socket;
    :catch_2f
    move-exception v0

    .line 314
    .restart local v0    # "e":Ljava/lang/Exception;
    invoke-virtual {p0, v4, v0}, Lcom/mba/proxylight/RequestProcessor;->error(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_c

    .line 326
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_34
    iget-object v2, p0, Lcom/mba/proxylight/RequestProcessor;->outSockets:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->clear()V

    .line 327
    iput-object v4, p0, Lcom/mba/proxylight/RequestProcessor;->currentOutSocket:Lcom/mba/proxylight/Socket;

    .line 328
    iget-object v2, p0, Lcom/mba/proxylight/RequestProcessor;->selector:Ljava/nio/channels/Selector;

    if-eqz v2, :cond_46

    .line 330
    :try_start_3f
    iget-object v2, p0, Lcom/mba/proxylight/RequestProcessor;->selector:Ljava/nio/channels/Selector;

    invoke-virtual {v2}, Ljava/nio/channels/Selector;->wakeup()Ljava/nio/channels/Selector;
    :try_end_44
    .catch Ljava/lang/Exception; {:try_start_3f .. :try_end_44} :catch_47

    .line 334
    :goto_44
    iput-object v4, p0, Lcom/mba/proxylight/RequestProcessor;->selector:Ljava/nio/channels/Selector;

    .line 336
    :cond_46
    return-void

    .line 331
    :catch_47
    move-exception v0

    .line 332
    .restart local v0    # "e":Ljava/lang/Exception;
    invoke-virtual {p0, v4, v0}, Lcom/mba/proxylight/RequestProcessor;->error(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_44
.end method

.method private closeOutSocket(Lcom/mba/proxylight/Socket;)V
    .registers 6
    .param p1, "out"    # Lcom/mba/proxylight/Socket;

    .prologue
    .line 294
    :try_start_0
    iget-object v2, p0, Lcom/mba/proxylight/RequestProcessor;->outSockets:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_a
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_41

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 295
    .local v1, "e":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/mba/proxylight/Socket;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    if-ne v3, p1, :cond_a

    .line 296
    iget-object v2, p0, Lcom/mba/proxylight/RequestProcessor;->outSockets:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 297
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Fermeture de la socket vers "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/mba/proxylight/RequestProcessor;->debug(Ljava/lang/String;)V

    .line 301
    .end local v1    # "e":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/mba/proxylight/Socket;>;"
    :cond_41
    iget-object v2, p1, Lcom/mba/proxylight/Socket;->socket:Ljava/nio/channels/SocketChannel;

    invoke-virtual {v2}, Ljava/nio/channels/SocketChannel;->isOpen()Z

    move-result v2

    if-eqz v2, :cond_4e

    .line 302
    iget-object v2, p1, Lcom/mba/proxylight/Socket;->socket:Ljava/nio/channels/SocketChannel;

    invoke-virtual {v2}, Ljava/nio/channels/SocketChannel;->close()V
    :try_end_4e
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_4e} :catch_4f

    .line 307
    :cond_4e
    :goto_4e
    return-void

    .line 304
    :catch_4f
    move-exception v0

    .line 305
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, ""

    invoke-virtual {p0, v2, v0}, Lcom/mba/proxylight/RequestProcessor;->error(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_4e
.end method

.method private filterRequest(Lcom/mba/proxylight/Request;)Z
    .registers 6
    .param p1, "request"    # Lcom/mba/proxylight/Request;

    .prologue
    .line 417
    invoke-virtual {p0}, Lcom/mba/proxylight/RequestProcessor;->getRequestFilters()Ljava/util/List;

    move-result-object v1

    .line 418
    .local v1, "filters":Ljava/util/List;, "Ljava/util/List<Lcom/mba/proxylight/RequestFilter;>;"
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_22

    .line 419
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_b
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_22

    .line 420
    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mba/proxylight/RequestFilter;

    .line 421
    .local v0, "filter":Lcom/mba/proxylight/RequestFilter;
    invoke-interface {v0, p1}, Lcom/mba/proxylight/RequestFilter;->filter(Lcom/mba/proxylight/Request;)Z

    move-result v3

    if-eqz v3, :cond_1f

    .line 422
    const/4 v3, 0x1

    .line 426
    .end local v0    # "filter":Lcom/mba/proxylight/RequestFilter;
    .end local v2    # "i":I
    :goto_1e
    return v3

    .line 419
    .restart local v0    # "filter":Lcom/mba/proxylight/RequestFilter;
    .restart local v2    # "i":I
    :cond_1f
    add-int/lit8 v2, v2, 0x1

    goto :goto_b

    .line 426
    .end local v0    # "filter":Lcom/mba/proxylight/RequestFilter;
    .end local v2    # "i":I
    :cond_22
    const/4 v3, 0x0

    goto :goto_1e
.end method

.method private read(Lcom/mba/proxylight/Socket;Ljava/nio/ByteBuffer;J)I
    .registers 7
    .param p1, "socket"    # Lcom/mba/proxylight/Socket;
    .param p2, "b"    # Ljava/nio/ByteBuffer;
    .param p3, "when"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 285
    iget-object v1, p1, Lcom/mba/proxylight/Socket;->socket:Ljava/nio/channels/SocketChannel;

    invoke-virtual {v1, p2}, Ljava/nio/channels/SocketChannel;->read(Ljava/nio/ByteBuffer;)I

    move-result v0

    .line 286
    .local v0, "retour":I
    if-lez v0, :cond_a

    .line 287
    iput-wide p3, p1, Lcom/mba/proxylight/Socket;->lastWrite:J

    .line 289
    :cond_a
    return v0
.end method

.method private readNext(Ljava/nio/ByteBuffer;)Ljava/lang/String;
    .registers 10
    .param p1, "buffer"    # Ljava/nio/ByteBuffer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 379
    const/4 v0, 0x0

    .line 380
    .local v0, "atCR":Z
    :cond_2
    :goto_2
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v4

    if-lez v4, :cond_14

    .line 381
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->get()B

    move-result v1

    .line 382
    .local v1, "ch":I
    const/4 v4, -0x1

    if-eq v1, v4, :cond_13

    const/16 v4, 0xa

    if-ne v1, v4, :cond_18

    .line 383
    :cond_13
    const/4 v0, 0x1

    .line 396
    .end local v1    # "ch":I
    :cond_14
    if-nez v0, :cond_3f

    .line 397
    const/4 v2, 0x0

    .line 401
    :goto_17
    return-object v2

    .line 387
    .restart local v1    # "ch":I
    :cond_18
    const/16 v4, 0xd

    if-eq v1, v4, :cond_2

    .line 388
    iget v4, p0, Lcom/mba/proxylight/RequestProcessor;->read_offset:I

    iget-object v5, p0, Lcom/mba/proxylight/RequestProcessor;->read_buf:[C

    array-length v5, v5

    if-ne v4, v5, :cond_33

    .line 389
    iget-object v3, p0, Lcom/mba/proxylight/RequestProcessor;->read_buf:[C

    .line 390
    .local v3, "tmpbuf":[C
    array-length v4, v3

    mul-int/lit8 v4, v4, 0x2

    new-array v4, v4, [C

    iput-object v4, p0, Lcom/mba/proxylight/RequestProcessor;->read_buf:[C

    .line 391
    iget-object v4, p0, Lcom/mba/proxylight/RequestProcessor;->read_buf:[C

    iget v5, p0, Lcom/mba/proxylight/RequestProcessor;->read_offset:I

    invoke-static {v3, v7, v4, v7, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 393
    .end local v3    # "tmpbuf":[C
    :cond_33
    iget-object v4, p0, Lcom/mba/proxylight/RequestProcessor;->read_buf:[C

    iget v5, p0, Lcom/mba/proxylight/RequestProcessor;->read_offset:I

    add-int/lit8 v6, v5, 0x1

    iput v6, p0, Lcom/mba/proxylight/RequestProcessor;->read_offset:I

    int-to-char v6, v1

    aput-char v6, v4, v5

    goto :goto_2

    .line 399
    .end local v1    # "ch":I
    :cond_3f
    iget-object v4, p0, Lcom/mba/proxylight/RequestProcessor;->read_buf:[C

    iget v5, p0, Lcom/mba/proxylight/RequestProcessor;->read_offset:I

    invoke-static {v4, v7, v5}, Ljava/lang/String;->copyValueOf([CII)Ljava/lang/String;

    move-result-object v2

    .line 400
    .local v2, "s":Ljava/lang/String;
    iput v7, p0, Lcom/mba/proxylight/RequestProcessor;->read_offset:I

    goto :goto_17
.end method

.method private transfer(Lcom/mba/proxylight/Socket;Lcom/mba/proxylight/Socket;J)Z
    .registers 7
    .param p1, "inSocket"    # Lcom/mba/proxylight/Socket;
    .param p2, "outSocket"    # Lcom/mba/proxylight/Socket;
    .param p3, "when"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 339
    iget-object v1, p0, Lcom/mba/proxylight/RequestProcessor;->readBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 340
    iget-object v1, p0, Lcom/mba/proxylight/RequestProcessor;->readBuffer:Ljava/nio/ByteBuffer;

    invoke-direct {p0, p1, v1, p3, p4}, Lcom/mba/proxylight/RequestProcessor;->read(Lcom/mba/proxylight/Socket;Ljava/nio/ByteBuffer;J)I

    move-result v0

    .line 341
    .local v0, "numRead":I
    const/4 v1, -0x1

    if-ne v0, v1, :cond_10

    .line 343
    const/4 v1, 0x0

    .line 349
    :goto_f
    return v1

    .line 345
    :cond_10
    if-lez v0, :cond_1c

    .line 346
    iget-object v1, p0, Lcom/mba/proxylight/RequestProcessor;->readBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    .line 347
    iget-object v1, p0, Lcom/mba/proxylight/RequestProcessor;->readBuffer:Ljava/nio/ByteBuffer;

    invoke-direct {p0, p2, v1, p3, p4}, Lcom/mba/proxylight/RequestProcessor;->write(Lcom/mba/proxylight/Socket;Ljava/nio/ByteBuffer;J)V

    .line 349
    :cond_1c
    const/4 v1, 0x1

    goto :goto_f
.end method

.method private write(Lcom/mba/proxylight/Socket;Ljava/nio/ByteBuffer;J)V
    .registers 6
    .param p1, "socket"    # Lcom/mba/proxylight/Socket;
    .param p2, "b"    # Ljava/nio/ByteBuffer;
    .param p3, "when"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 280
    iget-object v0, p1, Lcom/mba/proxylight/Socket;->socket:Ljava/nio/channels/SocketChannel;

    invoke-virtual {v0, p2}, Ljava/nio/channels/SocketChannel;->write(Ljava/nio/ByteBuffer;)I

    .line 281
    iput-wide p3, p1, Lcom/mba/proxylight/Socket;->lastWrite:J

    .line 282
    return-void
.end method


# virtual methods
.method public abstract debug(Ljava/lang/String;)V
.end method

.method public abstract error(Ljava/lang/String;Ljava/lang/Throwable;)V
.end method

.method public abstract getRemoteProxyHost()Ljava/lang/String;
.end method

.method public abstract getRemoteProxyPort()I
.end method

.method public abstract getRequestFilters()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/mba/proxylight/RequestFilter;",
            ">;"
        }
    .end annotation
.end method

.method public isAlive()Z
    .registers 2

    .prologue
    .line 413
    iget-boolean v0, p0, Lcom/mba/proxylight/RequestProcessor;->alive:Z

    return v0
.end method

.method public process(Ljava/nio/channels/SelectionKey;)V
    .registers 7
    .param p1, "key"    # Ljava/nio/channels/SelectionKey;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 353
    monitor-enter p0

    .line 354
    :try_start_1
    invoke-virtual {p1}, Ljava/nio/channels/SelectionKey;->channel()Ljava/nio/channels/SelectableChannel;

    move-result-object v0

    check-cast v0, Ljava/nio/channels/ServerSocketChannel;

    .line 357
    .local v0, "serverSocketChannel":Ljava/nio/channels/ServerSocketChannel;
    new-instance v1, Lcom/mba/proxylight/Socket;

    invoke-direct {v1}, Lcom/mba/proxylight/Socket;-><init>()V

    iput-object v1, p0, Lcom/mba/proxylight/RequestProcessor;->inSocket:Lcom/mba/proxylight/Socket;

    .line 358
    iget-object v1, p0, Lcom/mba/proxylight/RequestProcessor;->inSocket:Lcom/mba/proxylight/Socket;

    invoke-virtual {v0}, Ljava/nio/channels/ServerSocketChannel;->accept()Ljava/nio/channels/SocketChannel;

    move-result-object v2

    iput-object v2, v1, Lcom/mba/proxylight/Socket;->socket:Ljava/nio/channels/SocketChannel;

    .line 359
    iget-object v1, p0, Lcom/mba/proxylight/RequestProcessor;->inSocket:Lcom/mba/proxylight/Socket;

    iget-object v1, v1, Lcom/mba/proxylight/Socket;->socket:Ljava/nio/channels/SocketChannel;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/nio/channels/SocketChannel;->configureBlocking(Z)Ljava/nio/channels/SelectableChannel;

    .line 361
    invoke-static {}, Ljava/nio/channels/spi/SelectorProvider;->provider()Ljava/nio/channels/spi/SelectorProvider;

    move-result-object v1

    invoke-virtual {v1}, Ljava/nio/channels/spi/SelectorProvider;->openSelector()Ljava/nio/channels/spi/AbstractSelector;

    move-result-object v1

    iput-object v1, p0, Lcom/mba/proxylight/RequestProcessor;->selector:Ljava/nio/channels/Selector;

    .line 362
    iget-object v1, p0, Lcom/mba/proxylight/RequestProcessor;->inSocket:Lcom/mba/proxylight/Socket;

    iget-object v1, v1, Lcom/mba/proxylight/Socket;->socket:Ljava/nio/channels/SocketChannel;

    iget-object v2, p0, Lcom/mba/proxylight/RequestProcessor;->selector:Ljava/nio/channels/Selector;

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/mba/proxylight/RequestProcessor;->inSocket:Lcom/mba/proxylight/Socket;

    invoke-virtual {v1, v2, v3, v4}, Ljava/nio/channels/SocketChannel;->register(Ljava/nio/channels/Selector;ILjava/lang/Object;)Ljava/nio/channels/SelectionKey;

    .line 364
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V

    .line 365
    monitor-exit p0

    .line 366
    return-void

    .line 365
    .end local v0    # "serverSocketChannel":Ljava/nio/channels/ServerSocketChannel;
    :catchall_39
    move-exception v1

    monitor-exit p0
    :try_end_3b
    .catchall {:try_start_1 .. :try_end_3b} :catchall_39

    throw v1
.end method

.method public abstract recycle()V
.end method

.method public abstract resolve(Ljava/lang/String;)Ljava/net/InetAddress;
.end method

.method public shutdown()V
    .registers 2

    .prologue
    .line 405
    invoke-direct {p0}, Lcom/mba/proxylight/RequestProcessor;->closeAll()V

    .line 406
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mba/proxylight/RequestProcessor;->shutdown:Z

    .line 407
    monitor-enter p0

    .line 408
    :try_start_7
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V

    .line 409
    monitor-exit p0

    .line 410
    return-void

    .line 409
    :catchall_c
    move-exception v0

    monitor-exit p0
    :try_end_e
    .catchall {:try_start_7 .. :try_end_e} :catchall_c

    throw v0
.end method
