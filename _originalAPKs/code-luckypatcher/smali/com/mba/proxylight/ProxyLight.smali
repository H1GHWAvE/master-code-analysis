.class public Lcom/mba/proxylight/ProxyLight;
.super Ljava/lang/Object;
.source "ProxyLight.java"


# instance fields
.field private filters:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/mba/proxylight/RequestFilter;",
            ">;"
        }
    .end annotation
.end field

.field private ipCache:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/net/InetAddress;",
            ">;"
        }
    .end annotation
.end field

.field private port:I

.field private processors:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "Lcom/mba/proxylight/RequestProcessor;",
            ">;"
        }
    .end annotation
.end field

.field private remoteProxyHost:Ljava/lang/String;

.field private remoteProxyPort:I

.field running:Z

.field private selector:Ljava/nio/channels/Selector;

.field server:Ljava/nio/channels/ServerSocketChannel;


# direct methods
.method public constructor <init>()V
    .registers 4

    .prologue
    const/16 v2, 0x1f90

    const/4 v1, 0x0

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput v2, p0, Lcom/mba/proxylight/ProxyLight;->port:I

    .line 29
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mba/proxylight/ProxyLight;->running:Z

    .line 32
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lcom/mba/proxylight/ProxyLight;->processors:Ljava/util/Stack;

    .line 35
    iput-object v1, p0, Lcom/mba/proxylight/ProxyLight;->selector:Ljava/nio/channels/Selector;

    .line 38
    iput-object v1, p0, Lcom/mba/proxylight/ProxyLight;->remoteProxyHost:Ljava/lang/String;

    .line 39
    iput v2, p0, Lcom/mba/proxylight/ProxyLight;->remoteProxyPort:I

    .line 42
    iput-object v1, p0, Lcom/mba/proxylight/ProxyLight;->server:Ljava/nio/channels/ServerSocketChannel;

    .line 164
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/mba/proxylight/ProxyLight;->ipCache:Ljava/util/Map;

    .line 260
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mba/proxylight/ProxyLight;->filters:Ljava/util/List;

    .line 45
    return-void
.end method

.method static synthetic access$000(Lcom/mba/proxylight/ProxyLight;)Ljava/nio/channels/Selector;
    .registers 2
    .param p0, "x0"    # Lcom/mba/proxylight/ProxyLight;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/mba/proxylight/ProxyLight;->selector:Ljava/nio/channels/Selector;

    return-object v0
.end method

.method static synthetic access$002(Lcom/mba/proxylight/ProxyLight;Ljava/nio/channels/Selector;)Ljava/nio/channels/Selector;
    .registers 2
    .param p0, "x0"    # Lcom/mba/proxylight/ProxyLight;
    .param p1, "x1"    # Ljava/nio/channels/Selector;

    .prologue
    .line 20
    iput-object p1, p0, Lcom/mba/proxylight/ProxyLight;->selector:Ljava/nio/channels/Selector;

    return-object p1
.end method

.method static synthetic access$100(Lcom/mba/proxylight/ProxyLight;)Ljava/util/Stack;
    .registers 2
    .param p0, "x0"    # Lcom/mba/proxylight/ProxyLight;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/mba/proxylight/ProxyLight;->processors:Ljava/util/Stack;

    return-object v0
.end method


# virtual methods
.method public debug(Ljava/lang/String;)V
    .registers 3
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 211
    if-eqz p1, :cond_7

    .line 212
    sget-object v0, Ljava/lang/System;->err:Ljava/io/PrintStream;

    invoke-virtual {v0, p1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 214
    :cond_7
    return-void
.end method

.method public error(Ljava/lang/String;Ljava/lang/Throwable;)V
    .registers 4
    .param p1, "message"    # Ljava/lang/String;
    .param p2, "t"    # Ljava/lang/Throwable;

    .prologue
    .line 202
    if-eqz p1, :cond_7

    .line 203
    sget-object v0, Ljava/lang/System;->err:Ljava/io/PrintStream;

    invoke-virtual {v0, p1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 205
    :cond_7
    if-eqz p2, :cond_9

    .line 208
    :cond_9
    return-void
.end method

.method public getPort()I
    .registers 2

    .prologue
    .line 48
    iget v0, p0, Lcom/mba/proxylight/ProxyLight;->port:I

    return v0
.end method

.method public getRemoteProxyHost()Ljava/lang/String;
    .registers 2

    .prologue
    .line 248
    iget-object v0, p0, Lcom/mba/proxylight/ProxyLight;->remoteProxyHost:Ljava/lang/String;

    return-object v0
.end method

.method public getRemoteProxyPort()I
    .registers 2

    .prologue
    .line 251
    iget v0, p0, Lcom/mba/proxylight/ProxyLight;->remoteProxyPort:I

    return v0
.end method

.method public getRequestFilters()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/mba/proxylight/RequestFilter;",
            ">;"
        }
    .end annotation

    .prologue
    .line 262
    iget-object v0, p0, Lcom/mba/proxylight/ProxyLight;->filters:Ljava/util/List;

    return-object v0
.end method

.method public isRunning()Z
    .registers 2

    .prologue
    .line 217
    iget-boolean v0, p0, Lcom/mba/proxylight/ProxyLight;->running:Z

    return v0
.end method

.method public recycle(Lcom/mba/proxylight/RequestProcessor;)V
    .registers 4
    .param p1, "processor"    # Lcom/mba/proxylight/RequestProcessor;

    .prologue
    .line 255
    iget-object v1, p0, Lcom/mba/proxylight/ProxyLight;->processors:Ljava/util/Stack;

    monitor-enter v1

    .line 256
    :try_start_3
    iget-object v0, p0, Lcom/mba/proxylight/ProxyLight;->processors:Ljava/util/Stack;

    invoke-virtual {v0, p1}, Ljava/util/Stack;->add(Ljava/lang/Object;)Z

    .line 257
    monitor-exit v1

    .line 258
    return-void

    .line 257
    :catchall_a
    move-exception v0

    monitor-exit v1
    :try_end_c
    .catchall {:try_start_3 .. :try_end_c} :catchall_a

    throw v0
.end method

.method protected resolve(Ljava/lang/String;)Ljava/net/InetAddress;
    .registers 6
    .param p1, "host"    # Ljava/lang/String;

    .prologue
    .line 166
    iget-object v3, p0, Lcom/mba/proxylight/ProxyLight;->ipCache:Ljava/util/Map;

    invoke-interface {v3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/net/InetAddress;

    .line 167
    .local v0, "retour":Ljava/net/InetAddress;
    if-nez v0, :cond_13

    .line 169
    :try_start_a
    invoke-static {p1}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v0

    .line 170
    iget-object v3, p0, Lcom/mba/proxylight/ProxyLight;->ipCache:Ljava/util/Map;

    invoke-interface {v3, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_13
    .catch Ljava/net/UnknownHostException; {:try_start_a .. :try_end_13} :catch_15
    .catch Ljava/lang/Throwable; {:try_start_a .. :try_end_13} :catch_18

    :cond_13
    :goto_13
    move-object v3, v0

    .line 178
    :goto_14
    return-object v3

    .line 172
    :catch_15
    move-exception v2

    .line 173
    .local v2, "uhe":Ljava/net/UnknownHostException;
    const/4 v3, 0x0

    goto :goto_14

    .line 174
    .end local v2    # "uhe":Ljava/net/UnknownHostException;
    :catch_18
    move-exception v1

    .line 175
    .local v1, "t":Ljava/lang/Throwable;
    const-string v3, ""

    invoke-virtual {p0, v3, v1}, Lcom/mba/proxylight/ProxyLight;->error(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_13
.end method

.method public setPort(I)V
    .registers 2
    .param p1, "port"    # I

    .prologue
    .line 51
    iput p1, p0, Lcom/mba/proxylight/ProxyLight;->port:I

    .line 52
    return-void
.end method

.method public setRemoteProxy(Ljava/lang/String;I)V
    .registers 3
    .param p1, "host"    # Ljava/lang/String;
    .param p2, "port"    # I

    .prologue
    .line 243
    iput-object p1, p0, Lcom/mba/proxylight/ProxyLight;->remoteProxyHost:Ljava/lang/String;

    .line 244
    iput p2, p0, Lcom/mba/proxylight/ProxyLight;->remoteProxyPort:I

    .line 245
    return-void
.end method

.method public declared-synchronized start()V
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 56
    monitor-enter p0

    :try_start_1
    iget-boolean v1, p0, Lcom/mba/proxylight/ProxyLight;->running:Z
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_21

    if-eqz v1, :cond_7

    .line 158
    :goto_5
    monitor-exit p0

    return-void

    .line 60
    :cond_7
    const/4 v1, 0x1

    :try_start_8
    iput-boolean v1, p0, Lcom/mba/proxylight/ProxyLight;->running:Z

    .line 61
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/mba/proxylight/ProxyLight$1;

    invoke-direct {v1, p0}, Lcom/mba/proxylight/ProxyLight$1;-><init>(Lcom/mba/proxylight/ProxyLight;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 155
    .local v0, "t":Ljava/lang/Thread;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setDaemon(Z)V

    .line 156
    const-string v1, "ProxyLight server"

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    .line 157
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V
    :try_end_20
    .catchall {:try_start_8 .. :try_end_20} :catchall_21

    goto :goto_5

    .line 56
    .end local v0    # "t":Ljava/lang/Thread;
    :catchall_21
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public stop()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 182
    iget-boolean v1, p0, Lcom/mba/proxylight/ProxyLight;->running:Z

    if-nez v1, :cond_6

    .line 199
    :cond_5
    :goto_5
    return-void

    .line 187
    :cond_6
    :try_start_6
    iget-object v1, p0, Lcom/mba/proxylight/ProxyLight;->server:Ljava/nio/channels/ServerSocketChannel;

    invoke-virtual {v1}, Ljava/nio/channels/ServerSocketChannel;->close()V

    .line 188
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/mba/proxylight/ProxyLight;->server:Ljava/nio/channels/ServerSocketChannel;

    .line 190
    iget-object v1, p0, Lcom/mba/proxylight/ProxyLight;->selector:Ljava/nio/channels/Selector;

    invoke-virtual {v1}, Ljava/nio/channels/Selector;->wakeup()Ljava/nio/channels/Selector;

    .line 191
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/mba/proxylight/ProxyLight;->selector:Ljava/nio/channels/Selector;

    .line 193
    :goto_16
    iget-object v1, p0, Lcom/mba/proxylight/ProxyLight;->processors:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->size()I

    move-result v1

    if-lez v1, :cond_5

    .line 194
    iget-object v1, p0, Lcom/mba/proxylight/ProxyLight;->processors:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mba/proxylight/RequestProcessor;

    invoke-virtual {v1}, Lcom/mba/proxylight/RequestProcessor;->shutdown()V
    :try_end_29
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_29} :catch_2a

    goto :goto_16

    .line 196
    :catch_2a
    move-exception v0

    .line 197
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {p0, v2, v0}, Lcom/mba/proxylight/ProxyLight;->error(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_5
.end method
