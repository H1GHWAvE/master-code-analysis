.class final enum Lcom/mba/proxylight/RequestProcessor$REQUEST_STEP;
.super Ljava/lang/Enum;
.source "RequestProcessor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mba/proxylight/RequestProcessor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "REQUEST_STEP"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mba/proxylight/RequestProcessor$REQUEST_STEP;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mba/proxylight/RequestProcessor$REQUEST_STEP;

.field public static final enum REQUEST_CONTENT:Lcom/mba/proxylight/RequestProcessor$REQUEST_STEP;

.field public static final enum REQUEST_HEADERS:Lcom/mba/proxylight/RequestProcessor$REQUEST_STEP;

.field public static final enum STATUS_LINE:Lcom/mba/proxylight/RequestProcessor$REQUEST_STEP;

.field public static final enum TRANSFER:Lcom/mba/proxylight/RequestProcessor$REQUEST_STEP;


# direct methods
.method static constructor <clinit>()V
    .registers 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 44
    new-instance v0, Lcom/mba/proxylight/RequestProcessor$REQUEST_STEP;

    const-string v1, "STATUS_LINE"

    invoke-direct {v0, v1, v2}, Lcom/mba/proxylight/RequestProcessor$REQUEST_STEP;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mba/proxylight/RequestProcessor$REQUEST_STEP;->STATUS_LINE:Lcom/mba/proxylight/RequestProcessor$REQUEST_STEP;

    new-instance v0, Lcom/mba/proxylight/RequestProcessor$REQUEST_STEP;

    const-string v1, "REQUEST_HEADERS"

    invoke-direct {v0, v1, v3}, Lcom/mba/proxylight/RequestProcessor$REQUEST_STEP;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mba/proxylight/RequestProcessor$REQUEST_STEP;->REQUEST_HEADERS:Lcom/mba/proxylight/RequestProcessor$REQUEST_STEP;

    new-instance v0, Lcom/mba/proxylight/RequestProcessor$REQUEST_STEP;

    const-string v1, "REQUEST_CONTENT"

    invoke-direct {v0, v1, v4}, Lcom/mba/proxylight/RequestProcessor$REQUEST_STEP;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mba/proxylight/RequestProcessor$REQUEST_STEP;->REQUEST_CONTENT:Lcom/mba/proxylight/RequestProcessor$REQUEST_STEP;

    new-instance v0, Lcom/mba/proxylight/RequestProcessor$REQUEST_STEP;

    const-string v1, "TRANSFER"

    invoke-direct {v0, v1, v5}, Lcom/mba/proxylight/RequestProcessor$REQUEST_STEP;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mba/proxylight/RequestProcessor$REQUEST_STEP;->TRANSFER:Lcom/mba/proxylight/RequestProcessor$REQUEST_STEP;

    .line 43
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/mba/proxylight/RequestProcessor$REQUEST_STEP;

    sget-object v1, Lcom/mba/proxylight/RequestProcessor$REQUEST_STEP;->STATUS_LINE:Lcom/mba/proxylight/RequestProcessor$REQUEST_STEP;

    aput-object v1, v0, v2

    sget-object v1, Lcom/mba/proxylight/RequestProcessor$REQUEST_STEP;->REQUEST_HEADERS:Lcom/mba/proxylight/RequestProcessor$REQUEST_STEP;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mba/proxylight/RequestProcessor$REQUEST_STEP;->REQUEST_CONTENT:Lcom/mba/proxylight/RequestProcessor$REQUEST_STEP;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mba/proxylight/RequestProcessor$REQUEST_STEP;->TRANSFER:Lcom/mba/proxylight/RequestProcessor$REQUEST_STEP;

    aput-object v1, v0, v5

    sput-object v0, Lcom/mba/proxylight/RequestProcessor$REQUEST_STEP;->$VALUES:[Lcom/mba/proxylight/RequestProcessor$REQUEST_STEP;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 43
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mba/proxylight/RequestProcessor$REQUEST_STEP;
    .registers 2
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 43
    const-class v0, Lcom/mba/proxylight/RequestProcessor$REQUEST_STEP;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mba/proxylight/RequestProcessor$REQUEST_STEP;

    return-object v0
.end method

.method public static values()[Lcom/mba/proxylight/RequestProcessor$REQUEST_STEP;
    .registers 1

    .prologue
    .line 43
    sget-object v0, Lcom/mba/proxylight/RequestProcessor$REQUEST_STEP;->$VALUES:[Lcom/mba/proxylight/RequestProcessor$REQUEST_STEP;

    invoke-virtual {v0}, [Lcom/mba/proxylight/RequestProcessor$REQUEST_STEP;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mba/proxylight/RequestProcessor$REQUEST_STEP;

    return-object v0
.end method
