.class public Lcom/chelpus/XSupport;
.super Ljava/lang/Object;
.source "XSupport.java"

# interfaces
.implements Lde/robv/android/xposed/IXposedHookZygoteInit;
.implements Lde/robv/android/xposed/IXposedHookLoadPackage;


# static fields
.field public static enable:Z

.field public static hide:Z

.field public static patch1:Z

.field public static patch2:Z

.field public static patch3:Z

.field public static patch4:Z


# instance fields
.field PMcontext:Landroid/content/Context;

.field public checkDuplicatedPermissions:Z

.field public checkPermissions:Z

.field public checkSignatures:Z

.field ctx:Landroid/content/Context;

.field public currentPkgInApp:Ljava/lang/String;

.field public disableCheckSignatures:Z

.field forHide:Ljava/lang/Boolean;

.field hideFromThisApp:Ljava/lang/Boolean;

.field public initialize:J

.field public installUnsignedApps:Z

.field mContext:Landroid/content/Context;

.field public prefs:Lde/robv/android/xposed/XSharedPreferences;

.field skip1:Z

.field skip2:Z

.field skip3:Z

.field skipGB:Z

.field public verifyApps:Z


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    const/4 v1, 0x1

    .line 53
    sput-boolean v1, Lcom/chelpus/XSupport;->patch1:Z

    .line 54
    sput-boolean v1, Lcom/chelpus/XSupport;->patch2:Z

    .line 55
    sput-boolean v1, Lcom/chelpus/XSupport;->patch3:Z

    .line 56
    sput-boolean v1, Lcom/chelpus/XSupport;->patch4:Z

    .line 57
    const/4 v0, 0x0

    sput-boolean v0, Lcom/chelpus/XSupport;->hide:Z

    .line 58
    sput-boolean v1, Lcom/chelpus/XSupport;->enable:Z

    return-void
.end method

.method public constructor <init>()V
    .registers 5

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    iput-object v2, p0, Lcom/chelpus/XSupport;->currentPkgInApp:Ljava/lang/String;

    .line 52
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/chelpus/XSupport;->initialize:J

    .line 186
    iput-object v2, p0, Lcom/chelpus/XSupport;->ctx:Landroid/content/Context;

    .line 187
    iput-object v2, p0, Lcom/chelpus/XSupport;->forHide:Ljava/lang/Boolean;

    .line 188
    iput-object v2, p0, Lcom/chelpus/XSupport;->mContext:Landroid/content/Context;

    .line 189
    iput-object v2, p0, Lcom/chelpus/XSupport;->PMcontext:Landroid/content/Context;

    .line 190
    iput-object v2, p0, Lcom/chelpus/XSupport;->hideFromThisApp:Ljava/lang/Boolean;

    .line 191
    iput-boolean v3, p0, Lcom/chelpus/XSupport;->skip1:Z

    iput-boolean v3, p0, Lcom/chelpus/XSupport;->skip2:Z

    iput-boolean v3, p0, Lcom/chelpus/XSupport;->skip3:Z

    .line 192
    iput-boolean v3, p0, Lcom/chelpus/XSupport;->skipGB:Z

    return-void
.end method


# virtual methods
.method public checkForHide(Lde/robv/android/xposed/XC_MethodHook$MethodHookParam;)Z
    .registers 16
    .param p1, "param"    # Lde/robv/android/xposed/XC_MethodHook$MethodHookParam;

    .prologue
    .line 1242
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v8

    .line 1243
    .local v8, "uid":I
    iget-object v9, p1, Lde/robv/android/xposed/XC_MethodHook$MethodHookParam;->thisObject:Ljava/lang/Object;

    const-string v10, "getNameForUid"

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v9, v10, v11}, Lde/robv/android/xposed/XposedHelpers;->callMethod(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 1244
    .local v6, "packageName":Ljava/lang/String;
    const-string v9, ":"

    invoke-virtual {v6, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_50

    const-class v9, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v9}, Ljava/lang/Class;->getPackage()Ljava/lang/Package;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Package;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_50

    const-string v9, "de.robv.android.xposed.installer"

    .line 1245
    invoke-virtual {v6, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_50

    const-string v9, "supersu"

    .line 1246
    invoke-virtual {v6, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_50

    const-string v9, "superuser"

    invoke-virtual {v6, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_50

    const-string v9, "pro.burgerz.wsm.manager"

    .line 1247
    invoke-virtual {v6, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_52

    .line 1248
    :cond_50
    const/4 v9, 0x0

    .line 1287
    :goto_51
    return v9

    .line 1251
    :cond_52
    const/4 v1, 0x1

    .line 1252
    .local v1, "fh":Z
    iget-object v9, p0, Lcom/chelpus/XSupport;->PMcontext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v7

    .line 1253
    .local v7, "pm":Landroid/content/pm/PackageManager;
    const/4 v2, 0x0

    .line 1255
    .local v2, "info":Landroid/content/pm/ApplicationInfo;
    const/4 v9, 0x0

    :try_start_5b
    invoke-virtual {v7, v6, v9}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_5e
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_5b .. :try_end_5e} :catch_69

    move-result-object v2

    .line 1261
    if-eqz v2, :cond_70

    .line 1262
    iget v9, v2, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit8 v9, v9, 0x1

    if-eqz v9, :cond_6f

    .line 1263
    const/4 v9, 0x0

    goto :goto_51

    .line 1256
    :catch_69
    move-exception v0

    .line 1257
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    .line 1258
    const/4 v9, 0x0

    goto :goto_51

    .line 1265
    .end local v0    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_6f
    const/4 v1, 0x1

    .line 1268
    :cond_70
    if-eqz v1, :cond_a8

    .line 1269
    new-instance v3, Landroid/content/Intent;

    const-string v9, "android.intent.action.MAIN"

    invoke-direct {v3, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1270
    .local v3, "intent_home":Landroid/content/Intent;
    const-string v9, "android.intent.category.HOME"

    invoke-virtual {v3, v9}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 1271
    const-string v9, "android.intent.category.DEFAULT"

    invoke-virtual {v3, v9}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 1273
    const/4 v9, 0x0

    invoke-virtual {v7, v3, v9}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v5

    .line 1275
    .local v5, "launcherList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_8c
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_a4

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/pm/ResolveInfo;

    .line 1277
    .local v4, "launcher":Landroid/content/pm/ResolveInfo;
    iget-object v10, v4, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v10, v10, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v10, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_8c

    .line 1279
    const/4 v9, 0x0

    goto :goto_51

    .line 1282
    .end local v4    # "launcher":Landroid/content/pm/ResolveInfo;
    :cond_a4
    if-eqz v1, :cond_a8

    .line 1284
    const/4 v9, 0x1

    goto :goto_51

    .line 1287
    .end local v3    # "intent_home":Landroid/content/Intent;
    .end local v5    # "launcherList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    :cond_a8
    const/4 v9, 0x0

    goto :goto_51
.end method

.method public checkForHideApp(Lde/robv/android/xposed/XC_MethodHook$MethodHookParam;)Z
    .registers 16
    .param p1, "param"    # Lde/robv/android/xposed/XC_MethodHook$MethodHookParam;

    .prologue
    .line 1293
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v8

    .line 1294
    .local v8, "uid":I
    iget-object v9, p1, Lde/robv/android/xposed/XC_MethodHook$MethodHookParam;->thisObject:Ljava/lang/Object;

    const-string v10, "getNameForUid"

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v9, v10, v11}, Lde/robv/android/xposed/XposedHelpers;->callMethod(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 1295
    .local v6, "packageName":Ljava/lang/String;
    const-string v9, ":"

    invoke-virtual {v6, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_50

    const-class v9, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v9}, Ljava/lang/Class;->getPackage()Ljava/lang/Package;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Package;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_50

    const-string v9, "de.robv.android.xposed.installer"

    .line 1296
    invoke-virtual {v6, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_50

    const-string v9, "supersu"

    .line 1297
    invoke-virtual {v6, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_50

    const-string v9, "superuser"

    invoke-virtual {v6, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_50

    const-string v9, "pro.burgerz.wsm.manager"

    .line 1298
    invoke-virtual {v6, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_52

    .line 1299
    :cond_50
    const/4 v9, 0x0

    .line 1338
    :goto_51
    return v9

    .line 1302
    :cond_52
    const/4 v1, 0x1

    .line 1303
    .local v1, "fh":Z
    iget-object v9, p0, Lcom/chelpus/XSupport;->PMcontext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v7

    .line 1304
    .local v7, "pm":Landroid/content/pm/PackageManager;
    const/4 v2, 0x0

    .line 1306
    .local v2, "info":Landroid/content/pm/PackageInfo;
    const/4 v9, 0x0

    :try_start_5b
    invoke-virtual {v7, v6, v9}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_5e
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_5b .. :try_end_5e} :catch_6b

    move-result-object v2

    .line 1312
    if-eqz v2, :cond_72

    .line 1313
    iget-object v9, v2, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v9, v9, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit8 v9, v9, 0x1

    if-eqz v9, :cond_71

    .line 1314
    const/4 v9, 0x0

    goto :goto_51

    .line 1307
    :catch_6b
    move-exception v0

    .line 1308
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    .line 1309
    const/4 v9, 0x0

    goto :goto_51

    .line 1316
    .end local v0    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_71
    const/4 v1, 0x1

    .line 1319
    :cond_72
    if-eqz v1, :cond_aa

    .line 1320
    new-instance v3, Landroid/content/Intent;

    const-string v9, "android.intent.action.MAIN"

    invoke-direct {v3, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1321
    .local v3, "intent_home":Landroid/content/Intent;
    const-string v9, "android.intent.category.HOME"

    invoke-virtual {v3, v9}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 1322
    const-string v9, "android.intent.category.DEFAULT"

    invoke-virtual {v3, v9}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 1324
    const/4 v9, 0x0

    invoke-virtual {v7, v3, v9}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v5

    .line 1326
    .local v5, "launcherList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_8e
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_a6

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/pm/ResolveInfo;

    .line 1328
    .local v4, "launcher":Landroid/content/pm/ResolveInfo;
    iget-object v10, v4, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v10, v10, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v10, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_8e

    .line 1330
    const/4 v9, 0x0

    goto :goto_51

    .line 1333
    .end local v4    # "launcher":Landroid/content/pm/ResolveInfo;
    :cond_a6
    if-eqz v1, :cond_aa

    .line 1335
    const/4 v9, 0x1

    goto :goto_51

    .line 1338
    .end local v3    # "intent_home":Landroid/content/Intent;
    .end local v5    # "launcherList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    :cond_aa
    const/4 v9, 0x0

    goto :goto_51
.end method

.method public checkIntentRework(Lde/robv/android/xposed/XC_MethodHook$MethodHookParam;Landroid/content/Intent;II)Z
    .registers 15
    .param p1, "param"    # Lde/robv/android/xposed/XC_MethodHook$MethodHookParam;
    .param p2, "intent"    # Landroid/content/Intent;
    .param p3, "serviceCheck"    # I
    .param p4, "methodGetContext"    # I

    .prologue
    const/4 v9, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1344
    if-nez p4, :cond_9e

    .line 1345
    const/4 v0, 0x0

    .line 1347
    .local v0, "cont":Landroid/content/Context;
    :try_start_6
    iget-object v3, p1, Lde/robv/android/xposed/XC_MethodHook$MethodHookParam;->thisObject:Ljava/lang/Object;

    const-string v6, "getBaseContext"

    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {v3, v6, v7}, Lde/robv/android/xposed/XposedHelpers;->callMethod(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "cont":Landroid/content/Context;
    check-cast v0, Landroid/content/Context;
    :try_end_13
    .catch Ljava/lang/ClassCastException; {:try_start_6 .. :try_end_13} :catch_5d

    .line 1354
    .restart local v0    # "cont":Landroid/content/Context;
    :goto_13
    if-eqz v0, :cond_9e

    .line 1355
    if-nez p3, :cond_6b

    .line 1356
    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    new-instance v6, Landroid/content/ComponentName;

    sget-object v7, Lcom/chelpus/Common;->PACKAGE_NAME:Ljava/lang/String;

    const-class v8, Lcom/google/android/finsky/billing/iab/InAppBillingService;

    invoke-virtual {v8}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v6, v7, v8}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v6}, Landroid/content/pm/PackageManager;->getComponentEnabledSetting(Landroid/content/ComponentName;)I

    move-result v3

    if-ne v3, v9, :cond_45

    .line 1357
    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    new-instance v6, Landroid/content/ComponentName;

    sget-object v7, Lcom/chelpus/Common;->PACKAGE_NAME:Ljava/lang/String;

    const-class v8, Lcom/google/android/finsky/billing/iab/MarketBillingService;

    invoke-virtual {v8}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v6, v7, v8}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v6}, Landroid/content/pm/PackageManager;->getComponentEnabledSetting(Landroid/content/ComponentName;)I

    move-result v3

    if-eq v3, v9, :cond_6b

    .line 1358
    :cond_45
    const-string v3, "xexe"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_5b

    const-string v3, "xexe"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v6, "lp"

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_69

    :cond_5b
    move v3, v5

    .line 1437
    .end local v0    # "cont":Landroid/content/Context;
    :goto_5c
    return v3

    .line 1348
    :catch_5d
    move-exception v1

    .line 1349
    .local v1, "e":Ljava/lang/ClassCastException;
    iget-object v3, p1, Lde/robv/android/xposed/XC_MethodHook$MethodHookParam;->thisObject:Ljava/lang/Object;

    const-string v6, "mBase"

    invoke-static {v3, v6}, Lde/robv/android/xposed/XposedHelpers;->getObjectField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .restart local v0    # "cont":Landroid/content/Context;
    goto :goto_13

    .end local v1    # "e":Ljava/lang/ClassCastException;
    :cond_69
    move v3, v4

    .line 1361
    goto :goto_5c

    .line 1366
    :cond_6b
    if-ne p3, v5, :cond_9e

    .line 1367
    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    new-instance v6, Landroid/content/ComponentName;

    sget-object v7, Lcom/chelpus/Common;->PACKAGE_NAME:Ljava/lang/String;

    const-class v8, Lcom/google/android/finsky/services/LicensingService;

    invoke-virtual {v8}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v6, v7, v8}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v6}, Landroid/content/pm/PackageManager;->getComponentEnabledSetting(Landroid/content/ComponentName;)I

    move-result v3

    if-eq v3, v9, :cond_9e

    .line 1368
    const-string v3, "xexe"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_9a

    const-string v3, "xexe"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v6, "lp"

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_9c

    :cond_9a
    move v3, v5

    .line 1369
    goto :goto_5c

    :cond_9c
    move v3, v4

    .line 1371
    goto :goto_5c

    .line 1377
    .end local v0    # "cont":Landroid/content/Context;
    :cond_9e
    if-ne p4, v5, :cond_143

    .line 1378
    if-nez p3, :cond_101

    .line 1379
    iget-object v3, p1, Lde/robv/android/xposed/XC_MethodHook$MethodHookParam;->thisObject:Ljava/lang/Object;

    const-string v6, "getPackageManager"

    new-array v7, v4, [Ljava/lang/Object;

    invoke-static {v3, v6, v7}, Lde/robv/android/xposed/XposedHelpers;->callMethod(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/pm/PackageManager;

    new-instance v6, Landroid/content/ComponentName;

    sget-object v7, Lcom/chelpus/Common;->PACKAGE_NAME:Ljava/lang/String;

    const-class v8, Lcom/google/android/finsky/billing/iab/InAppBillingService;

    invoke-virtual {v8}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v6, v7, v8}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v6}, Landroid/content/pm/PackageManager;->getComponentEnabledSetting(Landroid/content/ComponentName;)I

    move-result v3

    if-ne v3, v9, :cond_e0

    iget-object v3, p1, Lde/robv/android/xposed/XC_MethodHook$MethodHookParam;->thisObject:Ljava/lang/Object;

    const-string v6, "getPackageManager"

    new-array v7, v4, [Ljava/lang/Object;

    .line 1380
    invoke-static {v3, v6, v7}, Lde/robv/android/xposed/XposedHelpers;->callMethod(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/pm/PackageManager;

    new-instance v6, Landroid/content/ComponentName;

    sget-object v7, Lcom/chelpus/Common;->PACKAGE_NAME:Ljava/lang/String;

    const-class v8, Lcom/google/android/finsky/billing/iab/MarketBillingService;

    invoke-virtual {v8}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v6, v7, v8}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v6}, Landroid/content/pm/PackageManager;->getComponentEnabledSetting(Landroid/content/ComponentName;)I

    move-result v3

    if-eq v3, v9, :cond_101

    .line 1381
    :cond_e0
    const/4 v2, 0x0

    .line 1383
    .local v2, "xexe":Ljava/lang/String;
    :try_start_e1
    const-string v3, "xexe"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;
    :try_end_e6
    .catch Ljava/lang/Exception; {:try_start_e1 .. :try_end_e6} :catch_f4

    move-result-object v2

    .line 1385
    :goto_e7
    if-eqz v2, :cond_f1

    const-string v3, "lp"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_fe

    :cond_f1
    move v3, v5

    .line 1386
    goto/16 :goto_5c

    .line 1384
    :catch_f4
    move-exception v1

    .local v1, "e":Ljava/lang/Exception;
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v6, "skip inapp xposed queryIntentServices"

    invoke-virtual {v3, v6}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    const/4 v2, 0x0

    goto :goto_e7

    .end local v1    # "e":Ljava/lang/Exception;
    :cond_fe
    move v3, v4

    .line 1388
    goto/16 :goto_5c

    .line 1393
    .end local v2    # "xexe":Ljava/lang/String;
    :cond_101
    if-ne p3, v5, :cond_143

    .line 1394
    iget-object v3, p1, Lde/robv/android/xposed/XC_MethodHook$MethodHookParam;->thisObject:Ljava/lang/Object;

    const-string v6, "getPackageManager"

    new-array v7, v4, [Ljava/lang/Object;

    invoke-static {v3, v6, v7}, Lde/robv/android/xposed/XposedHelpers;->callMethod(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/pm/PackageManager;

    new-instance v6, Landroid/content/ComponentName;

    sget-object v7, Lcom/chelpus/Common;->PACKAGE_NAME:Ljava/lang/String;

    const-class v8, Lcom/google/android/finsky/services/LicensingService;

    invoke-virtual {v8}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v6, v7, v8}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v6}, Landroid/content/pm/PackageManager;->getComponentEnabledSetting(Landroid/content/ComponentName;)I

    move-result v3

    if-eq v3, v9, :cond_143

    .line 1395
    const/4 v2, 0x0

    .line 1397
    .restart local v2    # "xexe":Ljava/lang/String;
    :try_start_123
    const-string v3, "xexe"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;
    :try_end_128
    .catch Ljava/lang/Exception; {:try_start_123 .. :try_end_128} :catch_136

    move-result-object v2

    .line 1399
    :goto_129
    if-eqz v2, :cond_133

    const-string v3, "lp"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_140

    :cond_133
    move v3, v5

    .line 1400
    goto/16 :goto_5c

    .line 1398
    :catch_136
    move-exception v1

    .restart local v1    # "e":Ljava/lang/Exception;
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v6, "skip inapp xposed queryIntentServices"

    invoke-virtual {v3, v6}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    const/4 v2, 0x0

    goto :goto_129

    .end local v1    # "e":Ljava/lang/Exception;
    :cond_140
    move v3, v4

    .line 1402
    goto/16 :goto_5c

    .line 1407
    .end local v2    # "xexe":Ljava/lang/String;
    :cond_143
    if-ne p4, v9, :cond_1d6

    .line 1408
    if-nez p3, :cond_19a

    .line 1409
    iget-object v3, p0, Lcom/chelpus/XSupport;->PMcontext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    new-instance v6, Landroid/content/ComponentName;

    sget-object v7, Lcom/chelpus/Common;->PACKAGE_NAME:Ljava/lang/String;

    const-class v8, Lcom/google/android/finsky/billing/iab/InAppBillingService;

    invoke-virtual {v8}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v6, v7, v8}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v6}, Landroid/content/pm/PackageManager;->getComponentEnabledSetting(Landroid/content/ComponentName;)I

    move-result v3

    if-ne v3, v9, :cond_179

    iget-object v3, p0, Lcom/chelpus/XSupport;->PMcontext:Landroid/content/Context;

    .line 1410
    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    new-instance v6, Landroid/content/ComponentName;

    sget-object v7, Lcom/chelpus/Common;->PACKAGE_NAME:Ljava/lang/String;

    const-class v8, Lcom/google/android/finsky/billing/iab/MarketBillingService;

    invoke-virtual {v8}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v6, v7, v8}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v6}, Landroid/content/pm/PackageManager;->getComponentEnabledSetting(Landroid/content/ComponentName;)I

    move-result v3

    if-eq v3, v9, :cond_19a

    .line 1411
    :cond_179
    const/4 v2, 0x0

    .line 1413
    .restart local v2    # "xexe":Ljava/lang/String;
    :try_start_17a
    const-string v3, "xexe"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;
    :try_end_17f
    .catch Ljava/lang/Exception; {:try_start_17a .. :try_end_17f} :catch_18d

    move-result-object v2

    .line 1415
    :goto_180
    if-eqz v2, :cond_18a

    const-string v3, "lp"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_197

    :cond_18a
    move v3, v5

    .line 1416
    goto/16 :goto_5c

    .line 1414
    :catch_18d
    move-exception v1

    .restart local v1    # "e":Ljava/lang/Exception;
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v6, "skip inapp xposed queryIntentServices"

    invoke-virtual {v3, v6}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    const/4 v2, 0x0

    goto :goto_180

    .end local v1    # "e":Ljava/lang/Exception;
    :cond_197
    move v3, v4

    .line 1418
    goto/16 :goto_5c

    .line 1423
    .end local v2    # "xexe":Ljava/lang/String;
    :cond_19a
    if-ne p3, v5, :cond_1d6

    .line 1424
    iget-object v3, p0, Lcom/chelpus/XSupport;->PMcontext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    new-instance v6, Landroid/content/ComponentName;

    sget-object v7, Lcom/chelpus/Common;->PACKAGE_NAME:Ljava/lang/String;

    const-class v8, Lcom/google/android/finsky/services/LicensingService;

    invoke-virtual {v8}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v6, v7, v8}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v6}, Landroid/content/pm/PackageManager;->getComponentEnabledSetting(Landroid/content/ComponentName;)I

    move-result v3

    if-eq v3, v9, :cond_1d6

    .line 1425
    const/4 v2, 0x0

    .line 1427
    .restart local v2    # "xexe":Ljava/lang/String;
    :try_start_1b6
    const-string v3, "xexe"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;
    :try_end_1bb
    .catch Ljava/lang/Exception; {:try_start_1b6 .. :try_end_1bb} :catch_1c9

    move-result-object v2

    .line 1429
    :goto_1bc
    if-eqz v2, :cond_1c6

    const-string v3, "lp"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1d3

    :cond_1c6
    move v3, v5

    .line 1430
    goto/16 :goto_5c

    .line 1428
    :catch_1c9
    move-exception v1

    .restart local v1    # "e":Ljava/lang/Exception;
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v6, "skip inapp xposed queryIntentServices"

    invoke-virtual {v3, v6}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    const/4 v2, 0x0

    goto :goto_1bc

    .end local v1    # "e":Ljava/lang/Exception;
    :cond_1d3
    move v3, v4

    .line 1432
    goto/16 :goto_5c

    .end local v2    # "xexe":Ljava/lang/String;
    :cond_1d6
    move v3, v4

    .line 1437
    goto/16 :goto_5c
.end method

.method public handleLoadPackage(Lde/robv/android/xposed/callbacks/XC_LoadPackage$LoadPackageParam;)V
    .registers 12
    .param p1, "lpparam"    # Lde/robv/android/xposed/callbacks/XC_LoadPackage$LoadPackageParam;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    const/16 v9, 0xa

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 197
    const-class v2, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v2}, Ljava/lang/Class;->getPackage()Ljava/lang/Package;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Package;->getName()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p1, Lde/robv/android/xposed/callbacks/XC_LoadPackage$LoadPackageParam;->packageName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_28

    .line 198
    const-string v2, "com.chelpus.Utils"

    iget-object v3, p1, Lde/robv/android/xposed/callbacks/XC_LoadPackage$LoadPackageParam;->classLoader:Ljava/lang/ClassLoader;

    const-string v4, "isXposedEnabled"

    new-array v5, v8, [Ljava/lang/Object;

    new-instance v6, Lcom/chelpus/XSupport$7;

    invoke-direct {v6, p0}, Lcom/chelpus/XSupport$7;-><init>(Lcom/chelpus/XSupport;)V

    aput-object v6, v5, v7

    invoke-static {v2, v3, v4, v5}, Lde/robv/android/xposed/XposedHelpers;->findAndHookMethod(Ljava/lang/String;Ljava/lang/ClassLoader;Ljava/lang/String;[Ljava/lang/Object;)Lde/robv/android/xposed/XC_MethodHook$Unhook;

    .line 210
    :cond_28
    const-string v2, "android.content.ContextWrapper"

    const/4 v3, 0x0

    const-string v4, "bindService"

    const/4 v5, 0x4

    new-array v5, v5, [Ljava/lang/Object;

    const-class v6, Landroid/content/Intent;

    aput-object v6, v5, v7

    const-class v6, Landroid/content/ServiceConnection;

    aput-object v6, v5, v8

    const/4 v6, 0x2

    sget-object v7, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v7, v5, v6

    const/4 v6, 0x3

    new-instance v7, Lcom/chelpus/XSupport$8;

    invoke-direct {v7, p0}, Lcom/chelpus/XSupport$8;-><init>(Lcom/chelpus/XSupport;)V

    aput-object v7, v5, v6

    invoke-static {v2, v3, v4, v5}, Lde/robv/android/xposed/XposedHelpers;->findAndHookMethod(Ljava/lang/String;Ljava/lang/ClassLoader;Ljava/lang/String;[Ljava/lang/Object;)Lde/robv/android/xposed/XC_MethodHook$Unhook;

    .line 264
    const-string v2, "android.app.ContextImpl"

    iget-object v3, p1, Lde/robv/android/xposed/callbacks/XC_LoadPackage$LoadPackageParam;->classLoader:Ljava/lang/ClassLoader;

    invoke-static {v2, v3}, Lde/robv/android/xposed/XposedHelpers;->findClass(Ljava/lang/String;Ljava/lang/ClassLoader;)Ljava/lang/Class;

    move-result-object v2

    const-string v3, "queryIntentServices"

    new-instance v4, Lcom/chelpus/XSupport$9;

    invoke-direct {v4, p0}, Lcom/chelpus/XSupport$9;-><init>(Lcom/chelpus/XSupport;)V

    invoke-static {v2, v3, v4}, Lde/robv/android/xposed/XposedBridge;->hookAllMethods(Ljava/lang/Class;Ljava/lang/String;Lde/robv/android/xposed/XC_MethodHook;)Ljava/util/Set;

    .line 334
    const-string v2, "android"

    iget-object v3, p1, Lde/robv/android/xposed/callbacks/XC_LoadPackage$LoadPackageParam;->packageName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_108

    iget-object v2, p1, Lde/robv/android/xposed/callbacks/XC_LoadPackage$LoadPackageParam;->processName:Ljava/lang/String;

    const-string v3, "android"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_108

    .line 337
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    if-le v2, v9, :cond_195

    .line 338
    const-string v2, "com.android.server.pm.PackageManagerService"

    iget-object v3, p1, Lde/robv/android/xposed/callbacks/XC_LoadPackage$LoadPackageParam;->classLoader:Ljava/lang/ClassLoader;

    invoke-static {v2, v3}, Lde/robv/android/xposed/XposedHelpers;->findClass(Ljava/lang/String;Ljava/lang/ClassLoader;)Ljava/lang/Class;

    move-result-object v1

    .line 340
    .local v1, "packageManagerClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-string v2, "com.android.server.pm.PackageManagerService"

    iget-object v3, p1, Lde/robv/android/xposed/callbacks/XC_LoadPackage$LoadPackageParam;->classLoader:Ljava/lang/ClassLoader;

    invoke-static {v2, v3}, Lde/robv/android/xposed/XposedHelpers;->findClass(Ljava/lang/String;Ljava/lang/ClassLoader;)Ljava/lang/Class;

    move-result-object v2

    new-instance v3, Lcom/chelpus/XSupport$10;

    invoke-direct {v3, p0}, Lcom/chelpus/XSupport$10;-><init>(Lcom/chelpus/XSupport;)V

    invoke-static {v2, v3}, Lde/robv/android/xposed/XposedBridge;->hookAllConstructors(Ljava/lang/Class;Lde/robv/android/xposed/XC_MethodHook;)Ljava/util/Set;

    .line 348
    sget-boolean v2, Lcom/chelpus/Common;->LOLLIPOP_NEWER:Z

    if-eqz v2, :cond_179

    .line 350
    const-string v2, "installPackageAsUser"

    new-instance v3, Lcom/chelpus/XSupport$11;

    invoke-direct {v3, p0}, Lcom/chelpus/XSupport$11;-><init>(Lcom/chelpus/XSupport;)V

    invoke-static {v1, v2, v3}, Lde/robv/android/xposed/XposedBridge;->hookAllMethods(Ljava/lang/Class;Ljava/lang/String;Lde/robv/android/xposed/XC_MethodHook;)Ljava/util/Set;

    .line 421
    :goto_98
    const-string v2, "com.android.server.pm.PackageManagerService"

    iget-object v3, p1, Lde/robv/android/xposed/callbacks/XC_LoadPackage$LoadPackageParam;->classLoader:Ljava/lang/ClassLoader;

    invoke-static {v2, v3}, Lde/robv/android/xposed/XposedHelpers;->findClass(Ljava/lang/String;Ljava/lang/ClassLoader;)Ljava/lang/Class;

    move-result-object v2

    const-string v3, "queryIntentServices"

    new-instance v4, Lcom/chelpus/XSupport$14;

    invoke-direct {v4, p0}, Lcom/chelpus/XSupport$14;-><init>(Lcom/chelpus/XSupport;)V

    invoke-static {v2, v3, v4}, Lde/robv/android/xposed/XposedBridge;->hookAllMethods(Ljava/lang/Class;Ljava/lang/String;Lde/robv/android/xposed/XC_MethodHook;)Ljava/util/Set;

    .line 491
    sget-boolean v2, Lcom/chelpus/Common;->LOLLIPOP_NEWER:Z

    if-eqz v2, :cond_b8

    .line 493
    const-string v2, "checkUpgradeKeySetLP"

    new-instance v3, Lcom/chelpus/XSupport$15;

    invoke-direct {v3, p0}, Lcom/chelpus/XSupport$15;-><init>(Lcom/chelpus/XSupport;)V

    invoke-static {v1, v2, v3}, Lde/robv/android/xposed/XposedBridge;->hookAllMethods(Ljava/lang/Class;Ljava/lang/String;Lde/robv/android/xposed/XC_MethodHook;)Ljava/util/Set;

    .line 509
    :cond_b8
    const-string v2, "verifySignaturesLP"

    new-instance v3, Lcom/chelpus/XSupport$16;

    invoke-direct {v3, p0}, Lcom/chelpus/XSupport$16;-><init>(Lcom/chelpus/XSupport;)V

    invoke-static {v1, v2, v3}, Lde/robv/android/xposed/XposedBridge;->hookAllMethods(Ljava/lang/Class;Ljava/lang/String;Lde/robv/android/xposed/XC_MethodHook;)Ljava/util/Set;

    .line 523
    const-string v2, "compareSignatures"

    new-instance v3, Lcom/chelpus/XSupport$17;

    invoke-direct {v3, p0}, Lcom/chelpus/XSupport$17;-><init>(Lcom/chelpus/XSupport;)V

    invoke-static {v1, v2, v3}, Lde/robv/android/xposed/XposedBridge;->hookAllMethods(Ljava/lang/Class;Ljava/lang/String;Lde/robv/android/xposed/XC_MethodHook;)Ljava/util/Set;

    .line 584
    const-string v2, "getPackageInfo"

    new-instance v3, Lcom/chelpus/XSupport$18;

    invoke-direct {v3, p0}, Lcom/chelpus/XSupport$18;-><init>(Lcom/chelpus/XSupport;)V

    invoke-static {v1, v2, v3}, Lde/robv/android/xposed/XposedBridge;->hookAllMethods(Ljava/lang/Class;Ljava/lang/String;Lde/robv/android/xposed/XC_MethodHook;)Ljava/util/Set;

    .line 603
    const-string v2, "getApplicationInfo"

    new-instance v3, Lcom/chelpus/XSupport$19;

    invoke-direct {v3, p0}, Lcom/chelpus/XSupport$19;-><init>(Lcom/chelpus/XSupport;)V

    invoke-static {v1, v2, v3}, Lde/robv/android/xposed/XposedBridge;->hookAllMethods(Ljava/lang/Class;Ljava/lang/String;Lde/robv/android/xposed/XC_MethodHook;)Ljava/util/Set;

    .line 620
    const-string v2, "generatePackageInfo"

    new-instance v3, Lcom/chelpus/XSupport$20;

    invoke-direct {v3, p0}, Lcom/chelpus/XSupport$20;-><init>(Lcom/chelpus/XSupport;)V

    invoke-static {v1, v2, v3}, Lde/robv/android/xposed/XposedBridge;->hookAllMethods(Ljava/lang/Class;Ljava/lang/String;Lde/robv/android/xposed/XC_MethodHook;)Ljava/util/Set;

    .line 634
    const-string v2, "getInstalledApplications"

    new-instance v3, Lcom/chelpus/XSupport$21;

    invoke-direct {v3, p0}, Lcom/chelpus/XSupport$21;-><init>(Lcom/chelpus/XSupport;)V

    invoke-static {v1, v2, v3}, Lde/robv/android/xposed/XposedBridge;->hookAllMethods(Ljava/lang/Class;Ljava/lang/String;Lde/robv/android/xposed/XC_MethodHook;)Ljava/util/Set;

    .line 681
    const-string v2, "getInstalledPackages"

    new-instance v3, Lcom/chelpus/XSupport$22;

    invoke-direct {v3, p0}, Lcom/chelpus/XSupport$22;-><init>(Lcom/chelpus/XSupport;)V

    invoke-static {v1, v2, v3}, Lde/robv/android/xposed/XposedBridge;->hookAllMethods(Ljava/lang/Class;Ljava/lang/String;Lde/robv/android/xposed/XC_MethodHook;)Ljava/util/Set;

    .line 727
    const-string v2, "getPreferredPackages"

    new-instance v3, Lcom/chelpus/XSupport$23;

    invoke-direct {v3, p0}, Lcom/chelpus/XSupport$23;-><init>(Lcom/chelpus/XSupport;)V

    invoke-static {v1, v2, v3}, Lde/robv/android/xposed/XposedBridge;->hookAllMethods(Ljava/lang/Class;Ljava/lang/String;Lde/robv/android/xposed/XC_MethodHook;)Ljava/util/Set;

    .line 1017
    .end local v1    # "packageManagerClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :cond_108
    :goto_108
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    if-le v2, v9, :cond_178

    .line 1018
    const-string v2, "android.app.ApplicationPackageManager"

    iget-object v3, p1, Lde/robv/android/xposed/callbacks/XC_LoadPackage$LoadPackageParam;->classLoader:Ljava/lang/ClassLoader;

    invoke-static {v2, v3}, Lde/robv/android/xposed/XposedHelpers;->findClass(Ljava/lang/String;Ljava/lang/ClassLoader;)Ljava/lang/Class;

    move-result-object v2

    new-instance v3, Lcom/chelpus/XSupport$34;

    invoke-direct {v3, p0}, Lcom/chelpus/XSupport$34;-><init>(Lcom/chelpus/XSupport;)V

    invoke-static {v2, v3}, Lde/robv/android/xposed/XposedBridge;->hookAllConstructors(Ljava/lang/Class;Lde/robv/android/xposed/XC_MethodHook;)Ljava/util/Set;

    .line 1081
    const-string v0, "android.app.ApplicationPackageManager"

    .line 1083
    .local v0, "cClassName":Ljava/lang/String;
    const-string v2, "android.app.ApplicationPackageManager"

    iget-object v3, p1, Lde/robv/android/xposed/callbacks/XC_LoadPackage$LoadPackageParam;->classLoader:Ljava/lang/ClassLoader;

    invoke-static {v2, v3}, Lde/robv/android/xposed/XposedHelpers;->findClass(Ljava/lang/String;Ljava/lang/ClassLoader;)Ljava/lang/Class;

    move-result-object v2

    const-string v3, "getPackageInfo"

    new-instance v4, Lcom/chelpus/XSupport$35;

    invoke-direct {v4, p0}, Lcom/chelpus/XSupport$35;-><init>(Lcom/chelpus/XSupport;)V

    invoke-static {v2, v3, v4}, Lde/robv/android/xposed/XposedBridge;->hookAllMethods(Ljava/lang/Class;Ljava/lang/String;Lde/robv/android/xposed/XC_MethodHook;)Ljava/util/Set;

    .line 1104
    const-string v2, "android.app.ApplicationPackageManager"

    iget-object v3, p1, Lde/robv/android/xposed/callbacks/XC_LoadPackage$LoadPackageParam;->classLoader:Ljava/lang/ClassLoader;

    invoke-static {v2, v3}, Lde/robv/android/xposed/XposedHelpers;->findClass(Ljava/lang/String;Ljava/lang/ClassLoader;)Ljava/lang/Class;

    move-result-object v2

    const-string v3, "getApplicationInfo"

    new-instance v4, Lcom/chelpus/XSupport$36;

    invoke-direct {v4, p0}, Lcom/chelpus/XSupport$36;-><init>(Lcom/chelpus/XSupport;)V

    invoke-static {v2, v3, v4}, Lde/robv/android/xposed/XposedBridge;->hookAllMethods(Ljava/lang/Class;Ljava/lang/String;Lde/robv/android/xposed/XC_MethodHook;)Ljava/util/Set;

    .line 1125
    const-string v2, "android.app.ApplicationPackageManager"

    iget-object v3, p1, Lde/robv/android/xposed/callbacks/XC_LoadPackage$LoadPackageParam;->classLoader:Ljava/lang/ClassLoader;

    invoke-static {v2, v3}, Lde/robv/android/xposed/XposedHelpers;->findClass(Ljava/lang/String;Ljava/lang/ClassLoader;)Ljava/lang/Class;

    move-result-object v2

    const-string v3, "getInstalledApplications"

    new-instance v4, Lcom/chelpus/XSupport$37;

    invoke-direct {v4, p0}, Lcom/chelpus/XSupport$37;-><init>(Lcom/chelpus/XSupport;)V

    invoke-static {v2, v3, v4}, Lde/robv/android/xposed/XposedBridge;->hookAllMethods(Ljava/lang/Class;Ljava/lang/String;Lde/robv/android/xposed/XC_MethodHook;)Ljava/util/Set;

    .line 1154
    const-string v2, "android.app.ApplicationPackageManager"

    iget-object v3, p1, Lde/robv/android/xposed/callbacks/XC_LoadPackage$LoadPackageParam;->classLoader:Ljava/lang/ClassLoader;

    invoke-static {v2, v3}, Lde/robv/android/xposed/XposedHelpers;->findClass(Ljava/lang/String;Ljava/lang/ClassLoader;)Ljava/lang/Class;

    move-result-object v2

    const-string v3, "getInstalledPackages"

    new-instance v4, Lcom/chelpus/XSupport$38;

    invoke-direct {v4, p0}, Lcom/chelpus/XSupport$38;-><init>(Lcom/chelpus/XSupport;)V

    invoke-static {v2, v3, v4}, Lde/robv/android/xposed/XposedBridge;->hookAllMethods(Ljava/lang/Class;Ljava/lang/String;Lde/robv/android/xposed/XC_MethodHook;)Ljava/util/Set;

    .line 1183
    const-string v2, "android.app.ApplicationPackageManager"

    iget-object v3, p1, Lde/robv/android/xposed/callbacks/XC_LoadPackage$LoadPackageParam;->classLoader:Ljava/lang/ClassLoader;

    invoke-static {v2, v3}, Lde/robv/android/xposed/XposedHelpers;->findClass(Ljava/lang/String;Ljava/lang/ClassLoader;)Ljava/lang/Class;

    move-result-object v2

    const-string v3, "getPreferredPackages"

    new-instance v4, Lcom/chelpus/XSupport$39;

    invoke-direct {v4, p0}, Lcom/chelpus/XSupport$39;-><init>(Lcom/chelpus/XSupport;)V

    invoke-static {v2, v3, v4}, Lde/robv/android/xposed/XposedBridge;->hookAllMethods(Ljava/lang/Class;Ljava/lang/String;Lde/robv/android/xposed/XC_MethodHook;)Ljava/util/Set;

    .line 1213
    .end local v0    # "cClassName":Ljava/lang/String;
    :cond_178
    return-void

    .line 370
    .restart local v1    # "packageManagerClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :cond_179
    sget-boolean v2, Lcom/chelpus/Common;->JB_MR1_NEWER:Z

    if-eqz v2, :cond_189

    .line 372
    const-string v2, "installPackageWithVerificationAndEncryption"

    new-instance v3, Lcom/chelpus/XSupport$12;

    invoke-direct {v3, p0}, Lcom/chelpus/XSupport$12;-><init>(Lcom/chelpus/XSupport;)V

    invoke-static {v1, v2, v3}, Lde/robv/android/xposed/XposedBridge;->hookAllMethods(Ljava/lang/Class;Ljava/lang/String;Lde/robv/android/xposed/XC_MethodHook;)Ljava/util/Set;

    goto/16 :goto_98

    .line 394
    :cond_189
    const-string v2, "installPackageWithVerification"

    new-instance v3, Lcom/chelpus/XSupport$13;

    invoke-direct {v3, p0}, Lcom/chelpus/XSupport$13;-><init>(Lcom/chelpus/XSupport;)V

    invoke-static {v1, v2, v3}, Lde/robv/android/xposed/XposedBridge;->hookAllMethods(Ljava/lang/Class;Ljava/lang/String;Lde/robv/android/xposed/XC_MethodHook;)Ljava/util/Set;

    goto/16 :goto_98

    .line 776
    .end local v1    # "packageManagerClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :cond_195
    const-string v2, "com.android.server.PackageManagerService"

    iget-object v3, p1, Lde/robv/android/xposed/callbacks/XC_LoadPackage$LoadPackageParam;->classLoader:Ljava/lang/ClassLoader;

    invoke-static {v2, v3}, Lde/robv/android/xposed/XposedHelpers;->findClass(Ljava/lang/String;Ljava/lang/ClassLoader;)Ljava/lang/Class;

    move-result-object v2

    new-instance v3, Lcom/chelpus/XSupport$24;

    invoke-direct {v3, p0}, Lcom/chelpus/XSupport$24;-><init>(Lcom/chelpus/XSupport;)V

    invoke-static {v2, v3}, Lde/robv/android/xposed/XposedBridge;->hookAllConstructors(Ljava/lang/Class;Lde/robv/android/xposed/XC_MethodHook;)Ljava/util/Set;

    .line 784
    const-string v2, "com.android.server.PackageManagerService"

    iget-object v3, p1, Lde/robv/android/xposed/callbacks/XC_LoadPackage$LoadPackageParam;->classLoader:Ljava/lang/ClassLoader;

    invoke-static {v2, v3}, Lde/robv/android/xposed/XposedHelpers;->findClass(Ljava/lang/String;Ljava/lang/ClassLoader;)Ljava/lang/Class;

    move-result-object v2

    const-string v3, "getPackageInfo"

    new-instance v4, Lcom/chelpus/XSupport$25;

    invoke-direct {v4, p0}, Lcom/chelpus/XSupport$25;-><init>(Lcom/chelpus/XSupport;)V

    invoke-static {v2, v3, v4}, Lde/robv/android/xposed/XposedBridge;->hookAllMethods(Ljava/lang/Class;Ljava/lang/String;Lde/robv/android/xposed/XC_MethodHook;)Ljava/util/Set;

    .line 804
    const-string v2, "com.android.server.PackageManagerService"

    iget-object v3, p1, Lde/robv/android/xposed/callbacks/XC_LoadPackage$LoadPackageParam;->classLoader:Ljava/lang/ClassLoader;

    invoke-static {v2, v3}, Lde/robv/android/xposed/XposedHelpers;->findClass(Ljava/lang/String;Ljava/lang/ClassLoader;)Ljava/lang/Class;

    move-result-object v2

    const-string v3, "getApplicationInfo"

    new-instance v4, Lcom/chelpus/XSupport$26;

    invoke-direct {v4, p0}, Lcom/chelpus/XSupport$26;-><init>(Lcom/chelpus/XSupport;)V

    invoke-static {v2, v3, v4}, Lde/robv/android/xposed/XposedBridge;->hookAllMethods(Ljava/lang/Class;Ljava/lang/String;Lde/robv/android/xposed/XC_MethodHook;)Ljava/util/Set;

    .line 822
    const-string v2, "android.content.pm.PackageParser"

    iget-object v3, p1, Lde/robv/android/xposed/callbacks/XC_LoadPackage$LoadPackageParam;->classLoader:Ljava/lang/ClassLoader;

    invoke-static {v2, v3}, Lde/robv/android/xposed/XposedHelpers;->findClass(Ljava/lang/String;Ljava/lang/ClassLoader;)Ljava/lang/Class;

    move-result-object v2

    const-string v3, "generatePackageInfo"

    new-instance v4, Lcom/chelpus/XSupport$27;

    invoke-direct {v4, p0}, Lcom/chelpus/XSupport$27;-><init>(Lcom/chelpus/XSupport;)V

    invoke-static {v2, v3, v4}, Lde/robv/android/xposed/XposedBridge;->hookAllMethods(Ljava/lang/Class;Ljava/lang/String;Lde/robv/android/xposed/XC_MethodHook;)Ljava/util/Set;

    .line 840
    const-string v2, "android.content.pm.PackageParser"

    iget-object v3, p1, Lde/robv/android/xposed/callbacks/XC_LoadPackage$LoadPackageParam;->classLoader:Ljava/lang/ClassLoader;

    invoke-static {v2, v3}, Lde/robv/android/xposed/XposedHelpers;->findClass(Ljava/lang/String;Ljava/lang/ClassLoader;)Ljava/lang/Class;

    move-result-object v2

    const-string v3, "generateApplicationInfo"

    new-instance v4, Lcom/chelpus/XSupport$28;

    invoke-direct {v4, p0}, Lcom/chelpus/XSupport$28;-><init>(Lcom/chelpus/XSupport;)V

    invoke-static {v2, v3, v4}, Lde/robv/android/xposed/XposedBridge;->hookAllMethods(Ljava/lang/Class;Ljava/lang/String;Lde/robv/android/xposed/XC_MethodHook;)Ljava/util/Set;

    .line 858
    const-string v2, "com.android.server.PackageManagerService"

    iget-object v3, p1, Lde/robv/android/xposed/callbacks/XC_LoadPackage$LoadPackageParam;->classLoader:Ljava/lang/ClassLoader;

    invoke-static {v2, v3}, Lde/robv/android/xposed/XposedHelpers;->findClass(Ljava/lang/String;Ljava/lang/ClassLoader;)Ljava/lang/Class;

    move-result-object v2

    const-string v3, "getInstalledApplications"

    new-instance v4, Lcom/chelpus/XSupport$29;

    invoke-direct {v4, p0}, Lcom/chelpus/XSupport$29;-><init>(Lcom/chelpus/XSupport;)V

    invoke-static {v2, v3, v4}, Lde/robv/android/xposed/XposedBridge;->hookAllMethods(Ljava/lang/Class;Ljava/lang/String;Lde/robv/android/xposed/XC_MethodHook;)Ljava/util/Set;

    .line 882
    const-string v2, "com.android.server.PackageManagerService"

    iget-object v3, p1, Lde/robv/android/xposed/callbacks/XC_LoadPackage$LoadPackageParam;->classLoader:Ljava/lang/ClassLoader;

    invoke-static {v2, v3}, Lde/robv/android/xposed/XposedHelpers;->findClass(Ljava/lang/String;Ljava/lang/ClassLoader;)Ljava/lang/Class;

    move-result-object v2

    const-string v3, "getInstalledPackages"

    new-instance v4, Lcom/chelpus/XSupport$30;

    invoke-direct {v4, p0}, Lcom/chelpus/XSupport$30;-><init>(Lcom/chelpus/XSupport;)V

    invoke-static {v2, v3, v4}, Lde/robv/android/xposed/XposedBridge;->hookAllMethods(Ljava/lang/Class;Ljava/lang/String;Lde/robv/android/xposed/XC_MethodHook;)Ljava/util/Set;

    .line 906
    const-string v2, "com.android.server.PackageManagerService"

    iget-object v3, p1, Lde/robv/android/xposed/callbacks/XC_LoadPackage$LoadPackageParam;->classLoader:Ljava/lang/ClassLoader;

    invoke-static {v2, v3}, Lde/robv/android/xposed/XposedHelpers;->findClass(Ljava/lang/String;Ljava/lang/ClassLoader;)Ljava/lang/Class;

    move-result-object v2

    const-string v3, "getPreferredPackages"

    new-instance v4, Lcom/chelpus/XSupport$31;

    invoke-direct {v4, p0}, Lcom/chelpus/XSupport$31;-><init>(Lcom/chelpus/XSupport;)V

    invoke-static {v2, v3, v4}, Lde/robv/android/xposed/XposedBridge;->hookAllMethods(Ljava/lang/Class;Ljava/lang/String;Lde/robv/android/xposed/XC_MethodHook;)Ljava/util/Set;

    .line 930
    const-string v2, "com.android.server.PackageManagerService"

    iget-object v3, p1, Lde/robv/android/xposed/callbacks/XC_LoadPackage$LoadPackageParam;->classLoader:Ljava/lang/ClassLoader;

    invoke-static {v2, v3}, Lde/robv/android/xposed/XposedHelpers;->findClass(Ljava/lang/String;Ljava/lang/ClassLoader;)Ljava/lang/Class;

    move-result-object v2

    const-string v3, "checkSignaturesLP"

    new-instance v4, Lcom/chelpus/XSupport$32;

    invoke-direct {v4, p0}, Lcom/chelpus/XSupport$32;-><init>(Lcom/chelpus/XSupport;)V

    invoke-static {v2, v3, v4}, Lde/robv/android/xposed/XposedBridge;->hookAllMethods(Ljava/lang/Class;Ljava/lang/String;Lde/robv/android/xposed/XC_MethodHook;)Ljava/util/Set;

    .line 947
    const-string v2, "com.android.server.PackageManagerService"

    iget-object v3, p1, Lde/robv/android/xposed/callbacks/XC_LoadPackage$LoadPackageParam;->classLoader:Ljava/lang/ClassLoader;

    invoke-static {v2, v3}, Lde/robv/android/xposed/XposedHelpers;->findClass(Ljava/lang/String;Ljava/lang/ClassLoader;)Ljava/lang/Class;

    move-result-object v2

    const-string v3, "queryIntentServices"

    new-instance v4, Lcom/chelpus/XSupport$33;

    invoke-direct {v4, p0}, Lcom/chelpus/XSupport$33;-><init>(Lcom/chelpus/XSupport;)V

    invoke-static {v2, v3, v4}, Lde/robv/android/xposed/XposedBridge;->hookAllMethods(Ljava/lang/Class;Ljava/lang/String;Lde/robv/android/xposed/XC_MethodHook;)Ljava/util/Set;

    goto/16 :goto_108
.end method

.method public initZygote(Lde/robv/android/xposed/IXposedHookZygoteInit$StartupParam;)V
    .registers 14
    .param p1, "startupParam"    # Lde/robv/android/xposed/IXposedHookZygoteInit$StartupParam;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x0

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 62
    const-string v3, "android.content.pm.PackageParser"

    invoke-static {v3, v7}, Lde/robv/android/xposed/XposedHelpers;->findClass(Ljava/lang/String;Ljava/lang/ClassLoader;)Ljava/lang/Class;

    move-result-object v2

    .line 64
    .local v2, "packageParserClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-string v3, "java.util.jar.JarVerifier$VerifierEntry"

    invoke-static {v3, v7}, Lde/robv/android/xposed/XposedHelpers;->findClass(Ljava/lang/String;Ljava/lang/ClassLoader;)Ljava/lang/Class;

    move-result-object v0

    .line 67
    .local v0, "jarVerifierClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    iput-boolean v8, p0, Lcom/chelpus/XSupport;->disableCheckSignatures:Z

    .line 69
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x12

    if-le v3, v4, :cond_2a

    .line 72
    :try_start_19
    const-string v3, "com.android.org.conscrypt.OpenSSLSignature"

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lde/robv/android/xposed/XposedHelpers;->findClass(Ljava/lang/String;Ljava/lang/ClassLoader;)Ljava/lang/Class;

    move-result-object v1

    .line 73
    .local v1, "openSSL":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-string v3, "engineVerify"

    new-instance v4, Lcom/chelpus/XSupport$1;

    invoke-direct {v4, p0}, Lcom/chelpus/XSupport$1;-><init>(Lcom/chelpus/XSupport;)V

    invoke-static {v1, v3, v4}, Lde/robv/android/xposed/XposedBridge;->hookAllMethods(Ljava/lang/Class;Ljava/lang/String;Lde/robv/android/xposed/XC_MethodHook;)Ljava/util/Set;
    :try_end_2a
    .catch Ljava/lang/Exception; {:try_start_19 .. :try_end_2a} :catch_ac

    .line 91
    .end local v1    # "openSSL":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :cond_2a
    :goto_2a
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0xe

    if-le v3, v4, :cond_47

    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x13

    if-ge v3, v4, :cond_47

    .line 94
    :try_start_36
    const-string v3, "org.apache.harmony.xnet.provider.jsse.OpenSSLSignature"

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lde/robv/android/xposed/XposedHelpers;->findClass(Ljava/lang/String;Ljava/lang/ClassLoader;)Ljava/lang/Class;

    move-result-object v1

    .line 95
    .restart local v1    # "openSSL":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-string v3, "engineVerify"

    new-instance v4, Lcom/chelpus/XSupport$2;

    invoke-direct {v4, p0}, Lcom/chelpus/XSupport$2;-><init>(Lcom/chelpus/XSupport;)V

    invoke-static {v1, v3, v4}, Lde/robv/android/xposed/XposedBridge;->hookAllMethods(Ljava/lang/Class;Ljava/lang/String;Lde/robv/android/xposed/XC_MethodHook;)Ljava/util/Set;
    :try_end_47
    .catch Ljava/lang/Exception; {:try_start_36 .. :try_end_47} :catch_aa

    .line 113
    .end local v1    # "openSSL":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :cond_47
    :goto_47
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0xa

    if-ne v3, v4, :cond_5e

    .line 116
    :try_start_4d
    const-string v3, "org.bouncycastle.jce.provider.JDKDigestSignature"

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lde/robv/android/xposed/XposedHelpers;->findClass(Ljava/lang/String;Ljava/lang/ClassLoader;)Ljava/lang/Class;

    move-result-object v1

    .line 117
    .restart local v1    # "openSSL":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-string v3, "engineVerify"

    new-instance v4, Lcom/chelpus/XSupport$3;

    invoke-direct {v4, p0}, Lcom/chelpus/XSupport$3;-><init>(Lcom/chelpus/XSupport;)V

    invoke-static {v1, v3, v4}, Lde/robv/android/xposed/XposedBridge;->hookAllMethods(Ljava/lang/Class;Ljava/lang/String;Lde/robv/android/xposed/XC_MethodHook;)Ljava/util/Set;
    :try_end_5e
    .catch Ljava/lang/ClassNotFoundException; {:try_start_4d .. :try_end_5e} :catch_a8

    .line 135
    .end local v1    # "openSSL":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :cond_5e
    :goto_5e
    const-string v3, "java.security.MessageDigest"

    const-string v4, "isEqual"

    new-array v5, v11, [Ljava/lang/Object;

    const-class v6, [B

    aput-object v6, v5, v9

    const-class v6, [B

    aput-object v6, v5, v8

    new-instance v6, Lcom/chelpus/XSupport$4;

    invoke-direct {v6, p0}, Lcom/chelpus/XSupport$4;-><init>(Lcom/chelpus/XSupport;)V

    aput-object v6, v5, v10

    invoke-static {v3, v7, v4, v5}, Lde/robv/android/xposed/XposedHelpers;->findAndHookMethod(Ljava/lang/String;Ljava/lang/ClassLoader;Ljava/lang/String;[Ljava/lang/Object;)Lde/robv/android/xposed/XC_MethodHook$Unhook;

    .line 148
    const-string v3, "java.security.Signature"

    const-string v4, "verify"

    new-array v5, v10, [Ljava/lang/Object;

    const-class v6, [B

    aput-object v6, v5, v9

    new-instance v6, Lcom/chelpus/XSupport$5;

    invoke-direct {v6, p0}, Lcom/chelpus/XSupport$5;-><init>(Lcom/chelpus/XSupport;)V

    aput-object v6, v5, v8

    invoke-static {v3, v7, v4, v5}, Lde/robv/android/xposed/XposedHelpers;->findAndHookMethod(Ljava/lang/String;Ljava/lang/ClassLoader;Ljava/lang/String;[Ljava/lang/Object;)Lde/robv/android/xposed/XC_MethodHook$Unhook;

    .line 167
    const-string v3, "java.security.Signature"

    const-string v4, "verify"

    const/4 v5, 0x4

    new-array v5, v5, [Ljava/lang/Object;

    const-class v6, [B

    aput-object v6, v5, v9

    sget-object v6, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v6, v5, v8

    sget-object v6, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v6, v5, v10

    new-instance v6, Lcom/chelpus/XSupport$6;

    invoke-direct {v6, p0}, Lcom/chelpus/XSupport$6;-><init>(Lcom/chelpus/XSupport;)V

    aput-object v6, v5, v11

    invoke-static {v3, v7, v4, v5}, Lde/robv/android/xposed/XposedHelpers;->findAndHookMethod(Ljava/lang/String;Ljava/lang/ClassLoader;Ljava/lang/String;[Ljava/lang/Object;)Lde/robv/android/xposed/XC_MethodHook$Unhook;

    .line 185
    return-void

    .line 132
    :catch_a8
    move-exception v3

    goto :goto_5e

    .line 110
    :catch_aa
    move-exception v3

    goto :goto_47

    .line 88
    :catch_ac
    move-exception v3

    goto/16 :goto_2a
.end method

.method public loadPrefs()V
    .registers 11

    .prologue
    const-wide/16 v8, 0x0

    const/4 v7, 0x1

    .line 1217
    iget-wide v3, p0, Lcom/chelpus/XSupport;->initialize:J

    cmp-long v3, v3, v8

    if-eqz v3, :cond_1a

    iget-wide v3, p0, Lcom/chelpus/XSupport;->initialize:J

    new-instance v5, Ljava/io/File;

    const-string v6, "/data/lp/xposed"

    invoke-direct {v5, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/io/File;->lastModified()J

    move-result-wide v5

    cmp-long v3, v3, v5

    if-eqz v3, :cond_82

    .line 1218
    :cond_1a
    iget-wide v3, p0, Lcom/chelpus/XSupport;->initialize:J

    cmp-long v3, v3, v8

    if-eqz v3, :cond_38

    iget-wide v3, p0, Lcom/chelpus/XSupport;->initialize:J

    new-instance v5, Ljava/io/File;

    const-string v6, "/data/lp/xposed"

    invoke-direct {v5, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/io/File;->lastModified()J

    move-result-wide v5

    cmp-long v3, v3, v5

    if-eqz v3, :cond_38

    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v4, "Update settings xposed"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1220
    :cond_38
    new-instance v3, Ljava/io/File;

    const-string v4, "/data/lp/xposed"

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->lastModified()J

    move-result-wide v3

    iput-wide v3, p0, Lcom/chelpus/XSupport;->initialize:J

    .line 1221
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1222
    .local v1, "s":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/CoreItem;>;"
    const/4 v2, 0x0

    .line 1224
    .local v2, "settings":Lorg/json/JSONObject;
    :try_start_4b
    invoke-static {}, Lcom/chelpus/Utils;->readXposedParamBoolean()Lorg/json/JSONObject;
    :try_end_4e
    .catch Lorg/json/JSONException; {:try_start_4b .. :try_end_4e} :catch_83

    move-result-object v2

    .line 1229
    :goto_4f
    if-eqz v2, :cond_82

    .line 1230
    const-string v3, "patch1"

    invoke-virtual {v2, v3, v7}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v3

    sput-boolean v3, Lcom/chelpus/XSupport;->patch1:Z

    .line 1231
    const-string v3, "patch2"

    invoke-virtual {v2, v3, v7}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v3

    sput-boolean v3, Lcom/chelpus/XSupport;->patch2:Z

    .line 1232
    const-string v3, "patch3"

    invoke-virtual {v2, v3, v7}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v3

    sput-boolean v3, Lcom/chelpus/XSupport;->patch3:Z

    .line 1233
    const-string v3, "patch4"

    invoke-virtual {v2, v3, v7}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v3

    sput-boolean v3, Lcom/chelpus/XSupport;->patch4:Z

    .line 1234
    const-string v3, "hide"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v3

    sput-boolean v3, Lcom/chelpus/XSupport;->hide:Z

    .line 1235
    const-string v3, "module_on"

    invoke-virtual {v2, v3, v7}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v3

    sput-boolean v3, Lcom/chelpus/XSupport;->enable:Z

    .line 1239
    .end local v1    # "s":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/CoreItem;>;"
    .end local v2    # "settings":Lorg/json/JSONObject;
    :cond_82
    return-void

    .line 1226
    .restart local v1    # "s":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/CoreItem;>;"
    .restart local v2    # "settings":Lorg/json/JSONObject;
    :catch_83
    move-exception v0

    .line 1227
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_4f
.end method
