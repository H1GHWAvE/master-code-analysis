.class Lcom/chelpus/Utils$Worker;
.super Ljava/lang/Thread;
.source "Utils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chelpus/Utils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "Worker"
.end annotation


# instance fields
.field private commands:[Ljava/lang/String;

.field private exitCode:Ljava/lang/Integer;

.field public input:Ljava/io/DataInputStream;

.field private result:Ljava/lang/String;

.field final synthetic this$0:Lcom/chelpus/Utils;


# direct methods
.method private constructor <init>(Lcom/chelpus/Utils;)V
    .registers 4

    .prologue
    const/4 v1, 0x0

    .line 1069
    iput-object p1, p0, Lcom/chelpus/Utils$Worker;->this$0:Lcom/chelpus/Utils;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 1065
    const-string v0, ""

    iput-object v0, p0, Lcom/chelpus/Utils$Worker;->result:Ljava/lang/String;

    .line 1066
    iput-object v1, p0, Lcom/chelpus/Utils$Worker;->commands:[Ljava/lang/String;

    .line 1067
    iput-object v1, p0, Lcom/chelpus/Utils$Worker;->exitCode:Ljava/lang/Integer;

    .line 1068
    iput-object v1, p0, Lcom/chelpus/Utils$Worker;->input:Ljava/io/DataInputStream;

    .line 1071
    return-void
.end method

.method synthetic constructor <init>(Lcom/chelpus/Utils;Lcom/chelpus/Utils$1;)V
    .registers 3
    .param p1, "x0"    # Lcom/chelpus/Utils;
    .param p2, "x1"    # Lcom/chelpus/Utils$1;

    .prologue
    .line 1064
    invoke-direct {p0, p1}, Lcom/chelpus/Utils$Worker;-><init>(Lcom/chelpus/Utils;)V

    return-void
.end method

.method static synthetic access$100(Lcom/chelpus/Utils$Worker;)[Ljava/lang/String;
    .registers 2
    .param p0, "x0"    # Lcom/chelpus/Utils$Worker;

    .prologue
    .line 1064
    iget-object v0, p0, Lcom/chelpus/Utils$Worker;->commands:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$102(Lcom/chelpus/Utils$Worker;[Ljava/lang/String;)[Ljava/lang/String;
    .registers 2
    .param p0, "x0"    # Lcom/chelpus/Utils$Worker;
    .param p1, "x1"    # [Ljava/lang/String;

    .prologue
    .line 1064
    iput-object p1, p0, Lcom/chelpus/Utils$Worker;->commands:[Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$200(Lcom/chelpus/Utils$Worker;)Ljava/lang/Integer;
    .registers 2
    .param p0, "x0"    # Lcom/chelpus/Utils$Worker;

    .prologue
    .line 1064
    iget-object v0, p0, Lcom/chelpus/Utils$Worker;->exitCode:Ljava/lang/Integer;

    return-object v0
.end method

.method static synthetic access$300(Lcom/chelpus/Utils$Worker;)Ljava/lang/String;
    .registers 2
    .param p0, "x0"    # Lcom/chelpus/Utils$Worker;

    .prologue
    .line 1064
    iget-object v0, p0, Lcom/chelpus/Utils$Worker;->result:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public run()V
    .registers 23

    .prologue
    .line 1075
    const/4 v5, 0x0

    .line 1076
    .local v5, "dalvikfound":Z
    const/4 v7, 0x0

    .line 1077
    .local v7, "except":Z
    const/4 v2, 0x0

    .line 1078
    .local v2, "checkRoot":Z
    const/4 v11, 0x0

    .line 1080
    .local v11, "skipOut":Z
    const/4 v10, 0x0

    .line 1081
    .local v10, "send_to_dialog":Z
    const/4 v4, 0x0

    .line 1083
    .local v4, "custom_patch":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/chelpus/Utils$Worker;->commands:[Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    array-length v0, v0

    move/from16 v17, v0

    const/4 v15, 0x0

    :goto_12
    move/from16 v0, v17

    if-ge v15, v0, :cond_129

    aget-object v3, v16, v15

    .line 1084
    .local v3, "command":Ljava/lang/String;
    const-string v18, "skipOut"

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_23

    const/4 v11, 0x1

    .line 1085
    :cond_23
    const-string v18, "-Xbootclasspath:"

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v18

    if-eqz v18, :cond_2e

    .line 1086
    const/4 v5, 0x1

    .line 1087
    :cond_2e
    const-string v18, ".runpatchsupport "

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v18

    if-nez v18, :cond_60

    const-string v18, ".runpatchsupportOld "

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v18

    if-nez v18, :cond_60

    const-string v18, ".runpatchads "

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v18

    if-nez v18, :cond_60

    const-string v18, ".odexrunpatch "

    .line 1088
    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v18

    if-nez v18, :cond_60

    const-string v18, ".custompatch "

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v18

    if-eqz v18, :cond_61

    .line 1089
    :cond_60
    const/4 v10, 0x1

    .line 1090
    :cond_61
    const-string v18, ".custompatch "

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v18

    if-eqz v18, :cond_6c

    const/4 v4, 0x1

    .line 1083
    :cond_6c
    add-int/lit8 v15, v15, 0x1

    goto :goto_12

    .line 1131
    .end local v3    # "command":Ljava/lang/String;
    .local v13, "thread_number":I
    :cond_6f
    :try_start_6f
    sget-object v15, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->suOutputStream:Ljava/io/DataOutputStream;

    const-string v16, "echo \'chelpus done!\'\n"

    invoke-virtual/range {v15 .. v16}, Ljava/io/DataOutputStream;->writeBytes(Ljava/lang/String;)V

    .line 1133
    sget-object v15, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->suOutputStream:Ljava/io/DataOutputStream;

    invoke-virtual {v15}, Ljava/io/DataOutputStream;->flush()V

    .line 1139
    if-nez v11, :cond_2c7

    .line 1140
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/chelpus/Utils$Worker;->this$0:Lcom/chelpus/Utils;

    move-object/from16 v0, p0

    invoke-virtual {v15, v10, v0}, Lcom/chelpus/Utils;->getInput(ZLcom/chelpus/Utils$Worker;)Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/chelpus/Utils$Worker;->result:Ljava/lang/String;
    :try_end_8b
    .catch Ljava/io/IOException; {:try_start_6f .. :try_end_8b} :catch_236
    .catch Ljava/lang/Exception; {:try_start_6f .. :try_end_8b} :catch_26d

    .line 1141
    if-eqz v10, :cond_8f

    .line 1142
    const/4 v10, 0x0

    .line 1143
    const/4 v4, 0x0

    .line 1147
    :cond_8f
    :try_start_8f
    sget-object v15, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->suErrorInputStream:Ljava/io/DataInputStream;

    if-eqz v15, :cond_286

    .line 1149
    sget-object v15, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->suErrorInputStream:Ljava/io/DataInputStream;

    invoke-virtual {v15}, Ljava/io/DataInputStream;->available()I

    move-result v15

    new-array v1, v15, [B

    .line 1150
    .local v1, "arrayErrorOfByte":[B
    sget-object v15, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->suErrorInputStream:Ljava/io/DataInputStream;

    invoke-virtual {v15, v1}, Ljava/io/DataInputStream;->read([B)I

    .line 1152
    new-instance v15, Ljava/lang/String;

    invoke-direct {v15, v1}, Ljava/lang/String;-><init>([B)V

    sput-object v15, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->errorOutput:Ljava/lang/String;

    .line 1153
    sget-object v15, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->errorOutput:Ljava/lang/String;

    const-string v16, "env: not found"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v15

    if-eqz v15, :cond_b4

    .line 1154
    invoke-static {}, Lcom/chelpus/Utils;->exitRoot()V

    .line 1156
    :cond_b4
    new-instance v15, Ljava/lang/String;

    invoke-direct {v15, v1}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v15}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v15

    const-string v16, ""

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-nez v15, :cond_277

    .line 1157
    sget-object v15, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "LuckyApcther root errors: "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    sget-object v17, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->errorOutput:Ljava/lang/String;

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1158
    new-instance v15, Ljava/lang/String;

    invoke-direct {v15, v1}, Ljava/lang/String;-><init>([B)V

    sput-object v15, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->errorOutput:Ljava/lang/String;
    :try_end_e6
    .catch Ljava/io/IOException; {:try_start_8f .. :try_end_e6} :catch_27d
    .catch Ljava/lang/Exception; {:try_start_8f .. :try_end_e6} :catch_2c1

    .line 1209
    .end local v1    # "arrayErrorOfByte":[B
    :goto_e6
    if-eqz v5, :cond_320

    .line 1210
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_e9
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/chelpus/Utils$Worker;->commands:[Ljava/lang/String;

    array-length v15, v15

    if-ge v8, v15, :cond_129

    .line 1211
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/chelpus/Utils$Worker;->commands:[Ljava/lang/String;

    aget-object v15, v15, v8

    const-string v16, "-Xbootclasspath:"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v15

    if-eqz v15, :cond_313

    .line 1212
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/chelpus/Utils$Worker;->commands:[Ljava/lang/String;

    aget-object v15, v15, v8

    const-string v16, "env LD_LIBRARY_PATH="

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v15

    if-nez v15, :cond_128

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/chelpus/Utils$Worker;->commands:[Ljava/lang/String;

    aget-object v15, v15, v8

    const-string v16, ".checkDataSize "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v15

    if-nez v15, :cond_128

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/chelpus/Utils$Worker;->commands:[Ljava/lang/String;

    aget-object v15, v15, v8

    const-string v16, ".custompatch "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v15

    if-eqz v15, :cond_2c9

    .line 1213
    :cond_128
    const/4 v7, 0x1

    .line 1093
    .end local v8    # "i":I
    .end local v13    # "thread_number":I
    :cond_129
    :goto_129
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/chelpus/Utils$Worker;->result:Ljava/lang/String;

    const-string v16, "SU Java-Code Running!"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v15

    if-nez v15, :cond_287

    if-nez v7, :cond_287

    .line 1094
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/chelpus/Utils$Worker;->commands:[Ljava/lang/String;

    const/16 v16, 0x0

    aget-object v15, v15, v16

    const-string v16, "env LD_LIBRARY_PATH="

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v15

    if-eqz v15, :cond_169

    sget-object v15, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "re-run Dalvik on root with environment "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/chelpus/Utils$Worker;->commands:[Ljava/lang/String;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    aget-object v17, v17, v18

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1097
    :cond_169
    :try_start_169
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/chelpus/Utils$Worker;->commands:[Ljava/lang/String;

    const/16 v16, 0x0

    aget-object v15, v15, v16

    const-string v16, "checkRoot"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_18b

    .line 1098
    sget-object v15, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v16, "LuckyPatcher: test root."

    invoke-virtual/range {v15 .. v16}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1099
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/chelpus/Utils$Worker;->commands:[Ljava/lang/String;

    const/16 v16, 0x0

    const-string v17, "ps init"

    aput-object v17, v15, v16

    .line 1100
    const/4 v2, 0x1

    .line 1106
    :cond_18b
    sget v15, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->countRoot:I

    add-int/lit8 v15, v15, 0x1

    sput v15, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->countRoot:I

    .line 1107
    sget v13, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->countRoot:I

    .line 1108
    .restart local v13    # "thread_number":I
    sget-object v15, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "Block root thread"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    sget v17, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->countRoot:I

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_1ad
    .catch Ljava/io/IOException; {:try_start_169 .. :try_end_1ad} :catch_236
    .catch Ljava/lang/Exception; {:try_start_169 .. :try_end_1ad} :catch_26d

    .line 1110
    :try_start_1ad
    sget-object v15, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->semaphoreRoot:Ljava/util/concurrent/Semaphore;

    const-wide/16 v16, 0x12c

    sget-object v18, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual/range {v15 .. v18}, Ljava/util/concurrent/Semaphore;->tryAcquire(JLjava/util/concurrent/TimeUnit;)Z

    move-result v15

    if-nez v15, :cond_1c8

    .line 1111
    sget-object v15, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v16, "Root command timeout. Bad root."

    invoke-virtual/range {v15 .. v16}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1112
    invoke-static {}, Lcom/chelpus/Utils;->exitRoot()V

    .line 1113
    sget-object v15, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->semaphoreRoot:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v15}, Ljava/util/concurrent/Semaphore;->release()V
    :try_end_1c8
    .catch Ljava/lang/InterruptedException; {:try_start_1ad .. :try_end_1c8} :catch_231
    .catch Ljava/io/IOException; {:try_start_1ad .. :try_end_1c8} :catch_236
    .catch Ljava/lang/Exception; {:try_start_1ad .. :try_end_1c8} :catch_26d

    .line 1118
    :cond_1c8
    :goto_1c8
    :try_start_1c8
    sget v15, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->countRoot:I

    add-int/lit8 v15, v15, -0x1

    sput v15, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->countRoot:I

    .line 1119
    invoke-static {}, Lcom/chelpus/Utils;->getRoot()V

    .line 1121
    sget-object v15, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "UNBlock root thread N"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1122
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/chelpus/Utils$Worker;->commands:[Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    array-length v0, v0

    move/from16 v17, v0

    const/4 v15, 0x0

    :goto_1f7
    move/from16 v0, v17

    if-ge v15, v0, :cond_6f

    aget-object v3, v16, v15

    .line 1123
    .restart local v3    # "command":Ljava/lang/String;
    if-eqz v5, :cond_224

    .line 1124
    sget-object v18, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->suOutputStream:Ljava/io/DataOutputStream;

    new-instance v19, Ljava/lang/String;

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v21, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->toolfilesdir:Ljava/lang/String;

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "/busybox killall dalvikvm\n"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/String;->getBytes()[B

    move-result-object v20

    const-string v21, "ISO-8859-1"

    invoke-direct/range {v19 .. v21}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    invoke-virtual/range {v18 .. v19}, Ljava/io/DataOutputStream;->writeBytes(Ljava/lang/String;)V

    .line 1126
    :cond_224
    const-string v18, "skipOut"

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_247

    .line 1122
    :goto_22e
    add-int/lit8 v15, v15, 0x1

    goto :goto_1f7

    .line 1115
    .end local v3    # "command":Ljava/lang/String;
    :catch_231
    move-exception v6

    .line 1116
    .local v6, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v6}, Ljava/lang/InterruptedException;->printStackTrace()V
    :try_end_235
    .catch Ljava/io/IOException; {:try_start_1c8 .. :try_end_235} :catch_236
    .catch Ljava/lang/Exception; {:try_start_1c8 .. :try_end_235} :catch_26d

    goto :goto_1c8

    .line 1194
    .end local v6    # "e":Ljava/lang/InterruptedException;
    .end local v13    # "thread_number":I
    :catch_236
    move-exception v9

    .line 1195
    .local v9, "localIOException":Ljava/io/IOException;
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V

    .line 1196
    sget-object v15, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v16, "LuckyPatcher (result root): root not found."

    invoke-virtual/range {v15 .. v16}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1199
    sget-object v15, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->semaphoreRoot:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v15}, Ljava/util/concurrent/Semaphore;->release()V

    .line 1253
    .end local v9    # "localIOException":Ljava/io/IOException;
    :goto_246
    return-void

    .line 1129
    .restart local v3    # "command":Ljava/lang/String;
    .restart local v13    # "thread_number":I
    :cond_247
    :try_start_247
    sget-object v18, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->suOutputStream:Ljava/io/DataOutputStream;

    new-instance v19, Ljava/lang/String;

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "\n"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/String;->getBytes()[B

    move-result-object v20

    const-string v21, "ISO-8859-1"

    invoke-direct/range {v19 .. v21}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    invoke-virtual/range {v18 .. v19}, Ljava/io/DataOutputStream;->writeBytes(Ljava/lang/String;)V
    :try_end_26c
    .catch Ljava/io/IOException; {:try_start_247 .. :try_end_26c} :catch_236
    .catch Ljava/lang/Exception; {:try_start_247 .. :try_end_26c} :catch_26d

    goto :goto_22e

    .line 1201
    .end local v3    # "command":Ljava/lang/String;
    .end local v13    # "thread_number":I
    :catch_26d
    move-exception v6

    .line 1202
    .local v6, "e":Ljava/lang/Exception;
    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V

    .line 1205
    sget-object v15, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->semaphoreRoot:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v15}, Ljava/util/concurrent/Semaphore;->release()V

    goto :goto_246

    .line 1160
    .end local v6    # "e":Ljava/lang/Exception;
    .restart local v1    # "arrayErrorOfByte":[B
    .restart local v13    # "thread_number":I
    :cond_277
    :try_start_277
    const-string v15, ""

    sput-object v15, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->errorOutput:Ljava/lang/String;
    :try_end_27b
    .catch Ljava/io/IOException; {:try_start_277 .. :try_end_27b} :catch_27d
    .catch Ljava/lang/Exception; {:try_start_277 .. :try_end_27b} :catch_2c1

    goto/16 :goto_e6

    .line 1168
    .end local v1    # "arrayErrorOfByte":[B
    :catch_27d
    move-exception v6

    .line 1169
    .local v6, "e":Ljava/io/IOException;
    :try_start_27e
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    .line 1170
    invoke-static {}, Lcom/chelpus/Utils;->exitRoot()V
    :try_end_284
    .catch Ljava/io/IOException; {:try_start_27e .. :try_end_284} :catch_236
    .catch Ljava/lang/Exception; {:try_start_27e .. :try_end_284} :catch_26d

    goto/16 :goto_e6

    .line 1165
    .end local v6    # "e":Ljava/io/IOException;
    :cond_286
    const/4 v7, 0x1

    .line 1235
    .end local v13    # "thread_number":I
    :cond_287
    :goto_287
    if-eqz v2, :cond_2a7

    .line 1236
    sget-object v15, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "LuckyPatcher (result root): "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/chelpus/Utils$Worker;->result:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1238
    :cond_2a7
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/chelpus/Utils$Worker;->result:Ljava/lang/String;

    const-string v16, ""

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_329

    if-eqz v2, :cond_329

    .line 1239
    if-eqz v5, :cond_323

    invoke-static {}, Lcom/chelpus/Utils;->exitRoot()V

    .line 1242
    :goto_2ba
    const-string v15, "lucky patcher root not found!"

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/chelpus/Utils$Worker;->result:Ljava/lang/String;

    goto :goto_246

    .line 1171
    .restart local v13    # "thread_number":I
    :catch_2c1
    move-exception v6

    .line 1172
    .local v6, "e":Ljava/lang/Exception;
    :try_start_2c2
    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2c5
    .catch Ljava/io/IOException; {:try_start_2c2 .. :try_end_2c5} :catch_236
    .catch Ljava/lang/Exception; {:try_start_2c2 .. :try_end_2c5} :catch_26d

    goto/16 :goto_e6

    .line 1176
    .end local v6    # "e":Ljava/lang/Exception;
    :cond_2c7
    const/4 v7, 0x1

    goto :goto_287

    .line 1216
    .restart local v8    # "i":I
    :cond_2c9
    const-string v15, "LD_LIBRARY_PATH"

    invoke-static {v15}, Ljava/lang/System;->getenv(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 1217
    .local v14, "val":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/chelpus/Utils$Worker;->commands:[Ljava/lang/String;

    aget-object v12, v15, v8

    .line 1218
    .local v12, "temp":Ljava/lang/String;
    sget-object v15, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->errorOutput:Ljava/lang/String;

    const-string v16, "env: not found"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v15

    if-eqz v15, :cond_317

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/chelpus/Utils$Worker;->commands:[Ljava/lang/String;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "env LD_LIBRARY_PATH="

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, " "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    aput-object v16, v15, v8

    .line 1223
    :goto_306
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/chelpus/Utils$Worker;->result:Ljava/lang/String;

    const-string v16, "SU Java-Code Running!"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v15

    if-eqz v15, :cond_319

    .line 1225
    const/4 v7, 0x1

    .line 1210
    .end local v12    # "temp":Ljava/lang/String;
    .end local v14    # "val":Ljava/lang/String;
    :cond_313
    :goto_313
    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_e9

    .line 1221
    .restart local v12    # "temp":Ljava/lang/String;
    .restart local v14    # "val":Ljava/lang/String;
    :cond_317
    const/4 v7, 0x1

    goto :goto_306

    .line 1227
    :cond_319
    const-string v15, ""

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/chelpus/Utils$Worker;->result:Ljava/lang/String;

    goto :goto_313

    .line 1232
    .end local v8    # "i":I
    .end local v12    # "temp":Ljava/lang/String;
    .end local v14    # "val":Ljava/lang/String;
    :cond_320
    const/4 v7, 0x1

    goto/16 :goto_129

    .line 1241
    .end local v13    # "thread_number":I
    :cond_323
    sget-object v15, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->semaphoreRoot:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v15}, Ljava/util/concurrent/Semaphore;->release()V

    goto :goto_2ba

    .line 1245
    :cond_329
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/chelpus/Utils$Worker;->result:Ljava/lang/String;

    const-string v16, ""

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_33b

    .line 1247
    const-string v15, "~"

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/chelpus/Utils$Worker;->result:Ljava/lang/String;

    .line 1250
    :cond_33b
    if-eqz v5, :cond_342

    invoke-static {}, Lcom/chelpus/Utils;->exitRoot()V

    goto/16 :goto_246

    .line 1252
    :cond_342
    sget-object v15, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->semaphoreRoot:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v15}, Ljava/util/concurrent/Semaphore;->release()V

    goto/16 :goto_246
.end method
