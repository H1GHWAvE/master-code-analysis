.class Lcom/chelpus/Utils$RootTimerTask;
.super Ljava/util/TimerTask;
.source "Utils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chelpus/Utils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "RootTimerTask"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/chelpus/Utils;

.field work:Lcom/chelpus/Utils$Worker;


# direct methods
.method public constructor <init>(Lcom/chelpus/Utils;Lcom/chelpus/Utils$Worker;)V
    .registers 4
    .param p1, "this$0"    # Lcom/chelpus/Utils;
    .param p2, "worker"    # Lcom/chelpus/Utils$Worker;

    .prologue
    .line 876
    iput-object p1, p0, Lcom/chelpus/Utils$RootTimerTask;->this$0:Lcom/chelpus/Utils;

    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    .line 875
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/chelpus/Utils$RootTimerTask;->work:Lcom/chelpus/Utils$Worker;

    .line 877
    iput-object p2, p0, Lcom/chelpus/Utils$RootTimerTask;->work:Lcom/chelpus/Utils$Worker;

    .line 878
    return-void
.end method


# virtual methods
.method public run()V
    .registers 10

    .prologue
    const/4 v3, 0x0

    .line 882
    sget-object v4, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->result:Ljava/lang/String;

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_ad

    .line 883
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v5, "timeout for wait root. exit..."

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 885
    :try_start_12
    iget-object v4, p0, Lcom/chelpus/Utils$RootTimerTask;->work:Lcom/chelpus/Utils$Worker;

    invoke-virtual {v4}, Lcom/chelpus/Utils$Worker;->interrupt()V

    .line 886
    iget-object v4, p0, Lcom/chelpus/Utils$RootTimerTask;->work:Lcom/chelpus/Utils$Worker;

    iget-object v4, v4, Lcom/chelpus/Utils$Worker;->input:Ljava/io/DataInputStream;

    invoke-virtual {v4}, Ljava/io/DataInputStream;->close()V
    :try_end_1e
    .catch Ljava/io/IOException; {:try_start_12 .. :try_end_1e} :catch_a9

    .line 888
    :try_start_1e
    sget-object v4, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    if-eqz v4, :cond_29

    .line 889
    sget-object v4, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    const/16 v4, 0x17

    invoke-static {v4}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->removeDialogLP(I)V

    .line 890
    :cond_29
    sget-object v4, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    const/4 v4, 0x1

    invoke-static {v4}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->removeDialogLP(I)V

    .line 891
    sget-object v4, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    const/16 v4, 0xb

    invoke-static {v4}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->removeDialogLP(I)V

    .line 892
    const-string v1, ""
    :try_end_38
    .catch Ljava/lang/Exception; {:try_start_1e .. :try_end_38} :catch_a4
    .catch Ljava/io/IOException; {:try_start_1e .. :try_end_38} :catch_a9

    .line 894
    .local v1, "commands":Ljava/lang/String;
    :try_start_38
    iget-object v4, p0, Lcom/chelpus/Utils$RootTimerTask;->work:Lcom/chelpus/Utils$Worker;

    # getter for: Lcom/chelpus/Utils$Worker;->commands:[Ljava/lang/String;
    invoke-static {v4}, Lcom/chelpus/Utils$Worker;->access$100(Lcom/chelpus/Utils$Worker;)[Ljava/lang/String;

    move-result-object v5

    array-length v6, v5

    move v4, v3

    :goto_40
    if-ge v4, v6, :cond_5e

    aget-object v0, v5, v4

    .line 895
    .local v0, "com":Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "\n"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_56
    .catch Ljava/lang/Exception; {:try_start_38 .. :try_end_56} :catch_5a
    .catch Ljava/io/IOException; {:try_start_38 .. :try_end_56} :catch_a9

    move-result-object v1

    .line 894
    add-int/lit8 v4, v4, 0x1

    goto :goto_40

    .line 897
    .end local v0    # "com":Ljava/lang/String;
    :catch_5a
    move-exception v2

    .local v2, "e":Ljava/lang/Exception;
    :try_start_5b
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    .line 898
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_5e
    sget-object v4, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    const-string v5, "Root error"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Your root hung at commands:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\nTry restart operation or updating your superSu and binary su."

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->showMessage(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_7e
    .catch Ljava/lang/Exception; {:try_start_5b .. :try_end_7e} :catch_a4
    .catch Ljava/io/IOException; {:try_start_5b .. :try_end_7e} :catch_a9

    .line 901
    .end local v1    # "commands":Ljava/lang/String;
    :goto_7e
    :try_start_7e
    iget-object v4, p0, Lcom/chelpus/Utils$RootTimerTask;->work:Lcom/chelpus/Utils$Worker;

    # getter for: Lcom/chelpus/Utils$Worker;->commands:[Ljava/lang/String;
    invoke-static {v4}, Lcom/chelpus/Utils$Worker;->access$100(Lcom/chelpus/Utils$Worker;)[Ljava/lang/String;

    move-result-object v4

    array-length v5, v4

    :goto_85
    if-ge v3, v5, :cond_b2

    aget-object v0, v4, v3

    .line 902
    .restart local v0    # "com":Ljava/lang/String;
    sget-object v6, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Lucky Patcher: freezes root commands:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_a1
    .catch Ljava/lang/Exception; {:try_start_7e .. :try_end_a1} :catch_ae
    .catch Ljava/io/IOException; {:try_start_7e .. :try_end_a1} :catch_a9

    .line 901
    add-int/lit8 v3, v3, 0x1

    goto :goto_85

    .line 899
    .end local v0    # "com":Ljava/lang/String;
    :catch_a4
    move-exception v2

    .restart local v2    # "e":Ljava/lang/Exception;
    :try_start_a5
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_a8
    .catch Ljava/io/IOException; {:try_start_a5 .. :try_end_a8} :catch_a9

    goto :goto_7e

    .line 907
    .end local v2    # "e":Ljava/lang/Exception;
    :catch_a9
    move-exception v2

    .line 908
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    .line 911
    .end local v2    # "e":Ljava/io/IOException;
    :cond_ad
    :goto_ad
    return-void

    .line 904
    :catch_ae
    move-exception v2

    .local v2, "e":Ljava/lang/Exception;
    :try_start_af
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    .line 905
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_b2
    invoke-static {}, Lcom/chelpus/Utils;->exitRoot()V

    .line 906
    sget-object v3, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->semaphoreRoot:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v3}, Ljava/util/concurrent/Semaphore;->release()V
    :try_end_ba
    .catch Ljava/io/IOException; {:try_start_af .. :try_end_ba} :catch_a9

    goto :goto_ad
.end method
