.class public Lcom/chelpus/root/utils/backup;
.super Ljava/lang/Object;
.source "backup.java"


# static fields
.field private static dalvikDexIn:Ljava/lang/String;

.field private static dalvikDexIn2:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 10
    const-string v0, "/cache/dalvik-cache/data@app@zamenitetodelo-1.apk@classes.dex"

    sput-object v0, Lcom/chelpus/root/utils/backup;->dalvikDexIn2:Ljava/lang/String;

    .line 11
    const-string v0, "/data/dalvik-cache/data@app@zamenitetodelo-1.apk@classes.dex"

    sput-object v0, Lcom/chelpus/root/utils/backup;->dalvikDexIn:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static main([Ljava/lang/String;)V
    .registers 12
    .param p0, "paramArrayOfString"    # [Ljava/lang/String;

    .prologue
    const/4 v10, 0x0

    .line 20
    invoke-static {}, Lcom/chelpus/Utils;->startRootJava()V

    .line 22
    sget-object v8, Lcom/chelpus/root/utils/backup;->dalvikDexIn:Ljava/lang/String;

    const-string v9, "zamenitetodelo"

    aget-object v10, p0, v10

    invoke-virtual {v8, v9, v10}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    .line 26
    .local v2, "dalvikDex":Ljava/lang/String;
    :try_start_e
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 27
    .local v5, "localFile1":Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v8

    if-nez v8, :cond_26

    new-instance v5, Ljava/io/File;

    .end local v5    # "localFile1":Ljava/io/File;
    const-string v8, "-1"

    const-string v9, "-2"

    invoke-virtual {v2, v8, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v5, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 28
    .restart local v5    # "localFile1":Ljava/io/File;
    :cond_26
    new-instance v6, Ljava/io/File;

    const-string v8, "-1"

    const-string v9, ""

    invoke-virtual {v2, v8, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v6, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 29
    .local v6, "localFile2":Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_3a

    move-object v6, v5

    .line 30
    :cond_3a
    const-string v8, "data@app"

    const-string v9, "mnt@asec"

    invoke-virtual {v2, v8, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    .line 31
    .local v3, "dalvikDexTemp":Ljava/lang/String;
    const-string v8, ".apk@classes.dex"

    const-string v9, "@pkg.apk@classes.dex"

    invoke-virtual {v3, v8, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    .line 32
    new-instance v5, Ljava/io/File;

    .end local v5    # "localFile1":Ljava/io/File;
    invoke-direct {v5, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 33
    .restart local v5    # "localFile1":Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_56

    move-object v6, v5

    .line 34
    :cond_56
    new-instance v5, Ljava/io/File;

    .end local v5    # "localFile1":Ljava/io/File;
    const-string v8, "-1"

    const-string v9, "-2"

    invoke-virtual {v3, v8, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v5, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 35
    .restart local v5    # "localFile1":Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_6a

    move-object v6, v5

    .line 36
    :cond_6a
    new-instance v5, Ljava/io/File;

    .end local v5    # "localFile1":Ljava/io/File;
    const-string v8, "-1"

    const-string v9, ""

    invoke-virtual {v3, v8, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v5, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 37
    .restart local v5    # "localFile1":Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_7e

    move-object v6, v5

    .line 39
    :cond_7e
    sget-object v8, Lcom/chelpus/root/utils/backup;->dalvikDexIn2:Ljava/lang/String;

    const-string v9, "zamenitetodelo"

    const/4 v10, 0x0

    aget-object v10, p0, v10

    invoke-virtual {v8, v9, v10}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    .line 40
    new-instance v5, Ljava/io/File;

    .end local v5    # "localFile1":Ljava/io/File;
    invoke-direct {v5, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 41
    .restart local v5    # "localFile1":Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v8

    if-nez v8, :cond_a1

    new-instance v5, Ljava/io/File;

    .end local v5    # "localFile1":Ljava/io/File;
    const-string v8, "-1"

    const-string v9, "-2"

    invoke-virtual {v2, v8, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v5, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 43
    .restart local v5    # "localFile1":Ljava/io/File;
    :cond_a1
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v8

    if-nez v8, :cond_b4

    new-instance v5, Ljava/io/File;

    .end local v5    # "localFile1":Ljava/io/File;
    const-string v8, "-1"

    const-string v9, ""

    invoke-virtual {v2, v8, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v5, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 44
    .restart local v5    # "localFile1":Ljava/io/File;
    :cond_b4
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_bb

    move-object v6, v5

    .line 45
    :cond_bb
    const-string v8, "data@app"

    const-string v9, "mnt@asec"

    invoke-virtual {v2, v8, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    .line 46
    const-string v8, ".apk@classes.dex"

    const-string v9, "@pkg.apk@classes.dex"

    invoke-virtual {v3, v8, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    .line 47
    new-instance v5, Ljava/io/File;

    .end local v5    # "localFile1":Ljava/io/File;
    invoke-direct {v5, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 48
    .restart local v5    # "localFile1":Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_d7

    move-object v6, v5

    .line 49
    :cond_d7
    new-instance v5, Ljava/io/File;

    .end local v5    # "localFile1":Ljava/io/File;
    const-string v8, "-1"

    const-string v9, "-2"

    invoke-virtual {v3, v8, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v5, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 50
    .restart local v5    # "localFile1":Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_eb

    move-object v6, v5

    .line 51
    :cond_eb
    new-instance v5, Ljava/io/File;

    .end local v5    # "localFile1":Ljava/io/File;
    const-string v8, "-1"

    const-string v9, ""

    invoke-virtual {v3, v8, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v5, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 52
    .restart local v5    # "localFile1":Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_ff

    move-object v6, v5

    .line 54
    :cond_ff
    sget-object v8, Lcom/chelpus/root/utils/backup;->dalvikDexIn:Ljava/lang/String;

    const-string v9, "zamenitetodelo"

    const/4 v10, 0x0

    aget-object v10, p0, v10

    invoke-virtual {v8, v9, v10}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    .line 55
    const-string v8, "/data/"

    const-string v9, "/sd-ext/data/"

    invoke-virtual {v2, v8, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    .line 56
    new-instance v5, Ljava/io/File;

    .end local v5    # "localFile1":Ljava/io/File;
    invoke-direct {v5, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 57
    .restart local v5    # "localFile1":Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v8

    if-nez v8, :cond_12a

    new-instance v5, Ljava/io/File;

    .end local v5    # "localFile1":Ljava/io/File;
    const-string v8, "-1"

    const-string v9, "-2"

    invoke-virtual {v2, v8, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v5, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 59
    .restart local v5    # "localFile1":Ljava/io/File;
    :cond_12a
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v8

    if-nez v8, :cond_13d

    new-instance v5, Ljava/io/File;

    .end local v5    # "localFile1":Ljava/io/File;
    const-string v8, "-1"

    const-string v9, ""

    invoke-virtual {v2, v8, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v5, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 60
    .restart local v5    # "localFile1":Ljava/io/File;
    :cond_13d
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_144

    move-object v6, v5

    .line 61
    :cond_144
    const-string v8, "data@app"

    const-string v9, "mnt@asec"

    invoke-virtual {v2, v8, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    .line 62
    const-string v8, ".apk@classes.dex"

    const-string v9, "@pkg.apk@classes.dex"

    invoke-virtual {v3, v8, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    .line 63
    new-instance v5, Ljava/io/File;

    .end local v5    # "localFile1":Ljava/io/File;
    invoke-direct {v5, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 64
    .restart local v5    # "localFile1":Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_160

    move-object v6, v5

    .line 65
    :cond_160
    new-instance v5, Ljava/io/File;

    .end local v5    # "localFile1":Ljava/io/File;
    const-string v8, "-1"

    const-string v9, "-2"

    invoke-virtual {v3, v8, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v5, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 66
    .restart local v5    # "localFile1":Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_174

    move-object v6, v5

    .line 67
    :cond_174
    new-instance v5, Ljava/io/File;

    .end local v5    # "localFile1":Ljava/io/File;
    const-string v8, "-1"

    const-string v9, ""

    invoke-virtual {v3, v8, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v5, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 68
    .restart local v5    # "localFile1":Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_188

    move-object v6, v5

    .line 70
    :cond_188
    sget-object v8, Lcom/chelpus/root/utils/backup;->dalvikDexIn2:Ljava/lang/String;

    const-string v9, "zamenitetodelo"

    const/4 v10, 0x0

    aget-object v10, p0, v10

    invoke-virtual {v8, v9, v10}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    .line 71
    const-string v8, "/cache/"

    const-string v9, "/sd-ext/data/cache/"

    invoke-virtual {v2, v8, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    .line 72
    new-instance v5, Ljava/io/File;

    .end local v5    # "localFile1":Ljava/io/File;
    invoke-direct {v5, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 73
    .restart local v5    # "localFile1":Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v8

    if-nez v8, :cond_1b3

    new-instance v5, Ljava/io/File;

    .end local v5    # "localFile1":Ljava/io/File;
    const-string v8, "-1"

    const-string v9, "-2"

    invoke-virtual {v2, v8, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v5, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 75
    .restart local v5    # "localFile1":Ljava/io/File;
    :cond_1b3
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v8

    if-nez v8, :cond_1c6

    new-instance v5, Ljava/io/File;

    .end local v5    # "localFile1":Ljava/io/File;
    const-string v8, "-1"

    const-string v9, ""

    invoke-virtual {v2, v8, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v5, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 76
    .restart local v5    # "localFile1":Ljava/io/File;
    :cond_1c6
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_1cd

    move-object v6, v5

    .line 77
    :cond_1cd
    const-string v8, "data@app"

    const-string v9, "mnt@asec"

    invoke-virtual {v2, v8, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    .line 78
    const-string v8, ".apk@classes.dex"

    const-string v9, "@pkg.apk@classes.dex"

    invoke-virtual {v3, v8, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    .line 79
    new-instance v5, Ljava/io/File;

    .end local v5    # "localFile1":Ljava/io/File;
    invoke-direct {v5, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 80
    .restart local v5    # "localFile1":Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_1e9

    move-object v6, v5

    .line 81
    :cond_1e9
    new-instance v5, Ljava/io/File;

    .end local v5    # "localFile1":Ljava/io/File;
    const-string v8, "-1"

    const-string v9, "-2"

    invoke-virtual {v3, v8, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v5, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 82
    .restart local v5    # "localFile1":Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_1fd

    move-object v6, v5

    .line 83
    :cond_1fd
    new-instance v5, Ljava/io/File;

    .end local v5    # "localFile1":Ljava/io/File;
    const-string v8, "-1"

    const-string v9, ""

    invoke-virtual {v3, v8, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v5, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 84
    .restart local v5    # "localFile1":Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_211

    move-object v6, v5

    .line 85
    :cond_211
    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v8

    if-nez v8, :cond_229

    new-instance v8, Ljava/io/FileNotFoundException;

    invoke-direct {v8}, Ljava/io/FileNotFoundException;-><init>()V

    throw v8
    :try_end_21d
    .catch Ljava/io/FileNotFoundException; {:try_start_e .. :try_end_21d} :catch_21d
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_21d} :catch_245

    .line 95
    .end local v3    # "dalvikDexTemp":Ljava/lang/String;
    .end local v5    # "localFile1":Ljava/io/File;
    .end local v6    # "localFile2":Ljava/io/File;
    :catch_21d
    move-exception v7

    .line 96
    .local v7, "localFileNotFoundException":Ljava/io/FileNotFoundException;
    sget-object v8, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v9, "Error: Backup failed!"

    invoke-virtual {v8, v9}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 101
    .end local v7    # "localFileNotFoundException":Ljava/io/FileNotFoundException;
    :goto_225
    invoke-static {}, Lcom/chelpus/Utils;->exitFromRootJava()V

    .line 102
    return-void

    .line 88
    .restart local v3    # "dalvikDexTemp":Ljava/lang/String;
    .restart local v5    # "localFile1":Ljava/io/File;
    .restart local v6    # "localFile2":Ljava/io/File;
    :cond_229
    :try_start_229
    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    const-string v9, "classes"

    const-string v10, "backup"

    invoke-virtual {v8, v9, v10}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    .line 89
    .local v1, "backTemp":Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 90
    .local v0, "backFile":Ljava/io/File;
    invoke-static {v6, v0}, Lcom/chelpus/Utils;->copyFile(Ljava/io/File;Ljava/io/File;)V

    .line 91
    sget-object v8, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v9, "Backup - done!"

    invoke-virtual {v8, v9}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_244
    .catch Ljava/io/FileNotFoundException; {:try_start_229 .. :try_end_244} :catch_21d
    .catch Ljava/lang/Exception; {:try_start_229 .. :try_end_244} :catch_245

    goto :goto_225

    .line 98
    .end local v0    # "backFile":Ljava/io/File;
    .end local v1    # "backTemp":Ljava/lang/String;
    .end local v3    # "dalvikDexTemp":Ljava/lang/String;
    .end local v5    # "localFile1":Ljava/io/File;
    .end local v6    # "localFile2":Ljava/io/File;
    :catch_245
    move-exception v4

    .line 99
    .local v4, "e":Ljava/lang/Exception;
    sget-object v8, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Exception e"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v4}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_225
.end method
