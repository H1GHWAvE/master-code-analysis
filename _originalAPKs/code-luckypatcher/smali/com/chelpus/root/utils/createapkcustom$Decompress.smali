.class public Lcom/chelpus/root/utils/createapkcustom$Decompress;
.super Ljava/lang/Object;
.source "createapkcustom.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chelpus/root/utils/createapkcustom;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Decompress"
.end annotation


# instance fields
.field private _location:Ljava/lang/String;

.field private _zipFile:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .param p1, "zipFile"    # Ljava/lang/String;
    .param p2, "location"    # Ljava/lang/String;

    .prologue
    .line 1206
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1207
    iput-object p1, p0, Lcom/chelpus/root/utils/createapkcustom$Decompress;->_zipFile:Ljava/lang/String;

    .line 1208
    iput-object p2, p0, Lcom/chelpus/root/utils/createapkcustom$Decompress;->_location:Ljava/lang/String;

    .line 1210
    const-string v0, ""

    invoke-direct {p0, v0}, Lcom/chelpus/root/utils/createapkcustom$Decompress;->_dirChecker(Ljava/lang/String;)V

    .line 1211
    return-void
.end method

.method private _dirChecker(Ljava/lang/String;)V
    .registers 5
    .param p1, "dir"    # Ljava/lang/String;

    .prologue
    .line 1346
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/chelpus/root/utils/createapkcustom$Decompress;->_location:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1347
    .local v0, "f":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->isFile()Z

    move-result v1

    if-eqz v1, :cond_21

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 1348
    :cond_21
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_2a

    .line 1349
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 1351
    :cond_2a
    return-void
.end method


# virtual methods
.method public unzip(Ljava/lang/String;)Ljava/lang/String;
    .registers 22
    .param p1, "file"    # Ljava/lang/String;

    .prologue
    .line 1277
    :try_start_0
    new-instance v9, Ljava/io/FileInputStream;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/chelpus/root/utils/createapkcustom$Decompress;->_zipFile:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-direct {v9, v0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    .line 1278
    .local v9, "fin":Ljava/io/FileInputStream;
    new-instance v15, Ljava/util/zip/ZipInputStream;

    invoke-direct {v15, v9}, Ljava/util/zip/ZipInputStream;-><init>(Ljava/io/InputStream;)V

    .line 1279
    .local v15, "zin":Ljava/util/zip/ZipInputStream;
    const/4 v14, 0x0

    .line 1280
    .local v14, "ze":Ljava/util/zip/ZipEntry;
    :cond_13
    :goto_13
    invoke-virtual {v15}, Ljava/util/zip/ZipInputStream;->getNextEntry()Ljava/util/zip/ZipEntry;

    move-result-object v14

    if-eqz v14, :cond_176

    .line 1283
    invoke-virtual {v14}, Ljava/util/zip/ZipEntry;->isDirectory()Z

    move-result v17

    if-eqz v17, :cond_a8

    .line 1284
    invoke-virtual {v14}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/chelpus/root/utils/createapkcustom$Decompress;->_dirChecker(Ljava/lang/String;)V
    :try_end_2a
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_2a} :catch_2b

    goto :goto_13

    .line 1318
    .end local v9    # "fin":Ljava/io/FileInputStream;
    .end local v14    # "ze":Ljava/util/zip/ZipEntry;
    .end local v15    # "zin":Ljava/util/zip/ZipInputStream;
    :catch_2b
    move-exception v5

    .line 1320
    .local v5, "e":Ljava/lang/Exception;
    # getter for: Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;
    invoke-static {}, Lcom/chelpus/root/utils/createapkcustom;->access$000()Ljava/io/PrintStream;

    move-result-object v17

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Decompressunzip "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1322
    :try_start_48
    new-instance v16, Lnet/lingala/zip4j/core/ZipFile;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/chelpus/root/utils/createapkcustom$Decompress;->_zipFile:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-direct/range {v16 .. v17}, Lnet/lingala/zip4j/core/ZipFile;-><init>(Ljava/lang/String;)V

    .line 1325
    .local v16, "zipFile":Lnet/lingala/zip4j/core/ZipFile;
    invoke-virtual/range {v16 .. v16}, Lnet/lingala/zip4j/core/ZipFile;->getFileHeaders()Ljava/util/List;

    move-result-object v8

    .line 1328
    .local v8, "fileHeaderList":Ljava/util/List;
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_58
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v17

    move/from16 v0, v17

    if-ge v11, v0, :cond_17c

    .line 1329
    invoke-interface {v8, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lnet/lingala/zip4j/model/FileHeader;

    .line 1330
    .local v7, "fileHeader":Lnet/lingala/zip4j/model/FileHeader;
    invoke-virtual {v7}, Lnet/lingala/zip4j/model/FileHeader;->getFileName()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_180

    .line 1331
    # getter for: Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;
    invoke-static {}, Lcom/chelpus/root/utils/createapkcustom;->access$000()Ljava/io/PrintStream;

    move-result-object v17

    invoke-virtual {v7}, Lnet/lingala/zip4j/model/FileHeader;->getFileName()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1332
    invoke-virtual {v7}, Lnet/lingala/zip4j/model/FileHeader;->getFileName()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/chelpus/root/utils/createapkcustom$Decompress;->_location:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v16 .. v18}, Lnet/lingala/zip4j/core/ZipFile;->extractFile(Ljava/lang/String;Ljava/lang/String;)V

    .line 1333
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/chelpus/root/utils/createapkcustom$Decompress;->_location:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual {v7}, Lnet/lingala/zip4j/model/FileHeader;->getFileName()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_a6
    .catch Lnet/lingala/zip4j/exception/ZipException; {:try_start_48 .. :try_end_a6} :catch_184
    .catch Ljava/lang/Exception; {:try_start_48 .. :try_end_a6} :catch_189

    move-result-object v17

    .line 1343
    .end local v5    # "e":Ljava/lang/Exception;
    .end local v7    # "fileHeader":Lnet/lingala/zip4j/model/FileHeader;
    .end local v8    # "fileHeaderList":Ljava/util/List;
    .end local v11    # "i":I
    .end local v16    # "zipFile":Lnet/lingala/zip4j/core/ZipFile;
    :goto_a7
    return-object v17

    .line 1287
    .restart local v9    # "fin":Ljava/io/FileInputStream;
    .restart local v14    # "ze":Ljava/util/zip/ZipEntry;
    .restart local v15    # "zin":Ljava/util/zip/ZipInputStream;
    :cond_a8
    :try_start_a8
    const-string v17, "/"

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_c2

    .line 1288
    const-string v17, "/"

    const-string v18, ""

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    move-object/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 1291
    :cond_c2
    invoke-virtual {v14}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_13

    .line 1292
    invoke-virtual {v14}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v17

    const-string v18, "\\/+"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v13

    .line 1293
    .local v13, "tail":[Ljava/lang/String;
    const-string v4, ""

    .line 1294
    .local v4, "data_dir":Ljava/lang/String;
    const/4 v11, 0x0

    .restart local v11    # "i":I
    :goto_dd
    array-length v0, v13

    move/from16 v17, v0

    add-int/lit8 v17, v17, -0x1

    move/from16 v0, v17

    if-ge v11, v0, :cond_10e

    .line 1295
    aget-object v17, v13, v11

    const-string v18, ""

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-nez v17, :cond_10b

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, "/"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    aget-object v18, v13, v11

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1294
    :cond_10b
    add-int/lit8 v11, v11, 0x1

    goto :goto_dd

    .line 1298
    :cond_10e
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/chelpus/root/utils/createapkcustom$Decompress;->_dirChecker(Ljava/lang/String;)V

    .line 1299
    new-instance v10, Ljava/io/FileOutputStream;

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/chelpus/root/utils/createapkcustom$Decompress;->_location:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual {v14}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-direct {v10, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    .line 1300
    .local v10, "fout":Ljava/io/FileOutputStream;
    const/16 v17, 0x400

    move/from16 v0, v17

    new-array v3, v0, [B

    .line 1302
    .local v3, "buffer":[B
    :goto_13b
    invoke-virtual {v15, v3}, Ljava/util/zip/ZipInputStream;->read([B)I

    move-result v12

    .local v12, "length":I
    const/16 v17, -0x1

    move/from16 v0, v17

    if-eq v12, v0, :cond_14d

    .line 1303
    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-virtual {v10, v3, v0, v12}, Ljava/io/FileOutputStream;->write([BII)V

    goto :goto_13b

    .line 1307
    :cond_14d
    invoke-virtual {v15}, Ljava/util/zip/ZipInputStream;->closeEntry()V

    .line 1308
    invoke-virtual {v10}, Ljava/io/FileOutputStream;->close()V

    .line 1309
    invoke-virtual {v15}, Ljava/util/zip/ZipInputStream;->close()V

    .line 1310
    invoke-virtual {v9}, Ljava/io/FileInputStream;->close()V

    .line 1311
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/chelpus/root/utils/createapkcustom$Decompress;->_location:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual {v14}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    goto/16 :goto_a7

    .line 1316
    .end local v3    # "buffer":[B
    .end local v4    # "data_dir":Ljava/lang/String;
    .end local v10    # "fout":Ljava/io/FileOutputStream;
    .end local v11    # "i":I
    .end local v12    # "length":I
    .end local v13    # "tail":[Ljava/lang/String;
    :cond_176
    invoke-virtual {v15}, Ljava/util/zip/ZipInputStream;->close()V

    .line 1317
    invoke-virtual {v9}, Ljava/io/FileInputStream;->close()V
    :try_end_17c
    .catch Ljava/lang/Exception; {:try_start_a8 .. :try_end_17c} :catch_2b

    .line 1343
    .end local v9    # "fin":Ljava/io/FileInputStream;
    .end local v14    # "ze":Ljava/util/zip/ZipEntry;
    .end local v15    # "zin":Ljava/util/zip/ZipInputStream;
    :cond_17c
    :goto_17c
    const-string v17, ""

    goto/16 :goto_a7

    .line 1328
    .restart local v5    # "e":Ljava/lang/Exception;
    .restart local v7    # "fileHeader":Lnet/lingala/zip4j/model/FileHeader;
    .restart local v8    # "fileHeaderList":Ljava/util/List;
    .restart local v11    # "i":I
    .restart local v16    # "zipFile":Lnet/lingala/zip4j/core/ZipFile;
    :cond_180
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_58

    .line 1337
    .end local v7    # "fileHeader":Lnet/lingala/zip4j/model/FileHeader;
    .end local v8    # "fileHeaderList":Ljava/util/List;
    .end local v11    # "i":I
    .end local v16    # "zipFile":Lnet/lingala/zip4j/core/ZipFile;
    :catch_184
    move-exception v6

    .line 1338
    .local v6, "e1":Lnet/lingala/zip4j/exception/ZipException;
    invoke-virtual {v6}, Lnet/lingala/zip4j/exception/ZipException;->printStackTrace()V

    goto :goto_17c

    .line 1339
    .end local v6    # "e1":Lnet/lingala/zip4j/exception/ZipException;
    :catch_189
    move-exception v6

    .line 1340
    .local v6, "e1":Ljava/lang/Exception;
    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_17c
.end method

.method public unzip()V
    .registers 19

    .prologue
    .line 1215
    :try_start_0
    new-instance v7, Ljava/io/FileInputStream;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/chelpus/root/utils/createapkcustom$Decompress;->_zipFile:Ljava/lang/String;

    invoke-direct {v7, v15}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    .line 1216
    .local v7, "fin":Ljava/io/FileInputStream;
    new-instance v13, Ljava/util/zip/ZipInputStream;

    invoke-direct {v13, v7}, Ljava/util/zip/ZipInputStream;-><init>(Ljava/io/InputStream;)V

    .line 1217
    .local v13, "zin":Ljava/util/zip/ZipInputStream;
    const/4 v12, 0x0

    .line 1218
    .local v12, "ze":Ljava/util/zip/ZipEntry;
    :cond_f
    :goto_f
    invoke-virtual {v13}, Ljava/util/zip/ZipInputStream;->getNextEntry()Ljava/util/zip/ZipEntry;

    move-result-object v12

    if-eqz v12, :cond_104

    .line 1221
    invoke-virtual {v12}, Ljava/util/zip/ZipEntry;->isDirectory()Z

    move-result v15

    if-eqz v15, :cond_83

    .line 1222
    invoke-virtual {v12}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/chelpus/root/utils/createapkcustom$Decompress;->_dirChecker(Ljava/lang/String;)V
    :try_end_24
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_24} :catch_25

    goto :goto_f

    .line 1249
    .end local v7    # "fin":Ljava/io/FileInputStream;
    .end local v12    # "ze":Ljava/util/zip/ZipEntry;
    .end local v13    # "zin":Ljava/util/zip/ZipInputStream;
    :catch_25
    move-exception v3

    .line 1251
    .local v3, "e":Ljava/lang/Exception;
    # getter for: Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;
    invoke-static {}, Lcom/chelpus/root/utils/createapkcustom;->access$000()Ljava/io/PrintStream;

    move-result-object v15

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "Decompressunzip "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1253
    :try_start_42
    new-instance v14, Lnet/lingala/zip4j/core/ZipFile;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/chelpus/root/utils/createapkcustom$Decompress;->_zipFile:Ljava/lang/String;

    invoke-direct {v14, v15}, Lnet/lingala/zip4j/core/ZipFile;-><init>(Ljava/lang/String;)V

    .line 1256
    .local v14, "zipFile":Lnet/lingala/zip4j/core/ZipFile;
    invoke-virtual {v14}, Lnet/lingala/zip4j/core/ZipFile;->getFileHeaders()Ljava/util/List;

    move-result-object v6

    .line 1259
    .local v6, "fileHeaderList":Ljava/util/List;
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_50
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v15

    if-ge v9, v15, :cond_10a

    .line 1260
    invoke-interface {v6, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lnet/lingala/zip4j/model/FileHeader;

    .line 1261
    .local v5, "fileHeader":Lnet/lingala/zip4j/model/FileHeader;
    invoke-virtual {v5}, Lnet/lingala/zip4j/model/FileHeader;->getFileName()Ljava/lang/String;

    move-result-object v15

    const-string v16, ".so"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_80

    .line 1262
    # getter for: Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;
    invoke-static {}, Lcom/chelpus/root/utils/createapkcustom;->access$000()Ljava/io/PrintStream;

    move-result-object v15

    invoke-virtual {v5}, Lnet/lingala/zip4j/model/FileHeader;->getFileName()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1263
    invoke-virtual {v5}, Lnet/lingala/zip4j/model/FileHeader;->getFileName()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/chelpus/root/utils/createapkcustom$Decompress;->_location:Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-virtual/range {v14 .. v16}, Lnet/lingala/zip4j/core/ZipFile;->extractFile(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_80
    .catch Lnet/lingala/zip4j/exception/ZipException; {:try_start_42 .. :try_end_80} :catch_10b
    .catch Ljava/lang/Exception; {:try_start_42 .. :try_end_80} :catch_110

    .line 1259
    :cond_80
    add-int/lit8 v9, v9, 0x1

    goto :goto_50

    .line 1225
    .end local v3    # "e":Ljava/lang/Exception;
    .end local v5    # "fileHeader":Lnet/lingala/zip4j/model/FileHeader;
    .end local v6    # "fileHeaderList":Ljava/util/List;
    .end local v9    # "i":I
    .end local v14    # "zipFile":Lnet/lingala/zip4j/core/ZipFile;
    .restart local v7    # "fin":Ljava/io/FileInputStream;
    .restart local v12    # "ze":Ljava/util/zip/ZipEntry;
    .restart local v13    # "zin":Ljava/util/zip/ZipInputStream;
    :cond_83
    :try_start_83
    invoke-virtual {v12}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v15

    const-string v16, ".so"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_f

    .line 1226
    invoke-virtual {v12}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v15

    const-string v16, "\\/+"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v11

    .line 1227
    .local v11, "tail":[Ljava/lang/String;
    const-string v2, ""

    .line 1228
    .local v2, "data_dir":Ljava/lang/String;
    const/4 v9, 0x0

    .restart local v9    # "i":I
    :goto_9c
    array-length v15, v11

    add-int/lit8 v15, v15, -0x1

    if-ge v9, v15, :cond_c7

    .line 1229
    aget-object v15, v11, v9

    const-string v16, ""

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-nez v15, :cond_c4

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "/"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    aget-object v16, v11, v9

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1228
    :cond_c4
    add-int/lit8 v9, v9, 0x1

    goto :goto_9c

    .line 1232
    :cond_c7
    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/chelpus/root/utils/createapkcustom$Decompress;->_dirChecker(Ljava/lang/String;)V

    .line 1233
    new-instance v8, Ljava/io/FileOutputStream;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/chelpus/root/utils/createapkcustom$Decompress;->_location:Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v12}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-direct {v8, v15}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    .line 1234
    .local v8, "fout":Ljava/io/FileOutputStream;
    const/16 v15, 0x400

    new-array v1, v15, [B

    .line 1236
    .local v1, "buffer":[B
    :goto_f0
    invoke-virtual {v13, v1}, Ljava/util/zip/ZipInputStream;->read([B)I

    move-result v10

    .local v10, "length":I
    const/4 v15, -0x1

    if-eq v10, v15, :cond_fc

    .line 1237
    const/4 v15, 0x0

    invoke-virtual {v8, v1, v15, v10}, Ljava/io/FileOutputStream;->write([BII)V

    goto :goto_f0

    .line 1241
    :cond_fc
    invoke-virtual {v13}, Ljava/util/zip/ZipInputStream;->closeEntry()V

    .line 1242
    invoke-virtual {v8}, Ljava/io/FileOutputStream;->close()V

    goto/16 :goto_f

    .line 1247
    .end local v1    # "buffer":[B
    .end local v2    # "data_dir":Ljava/lang/String;
    .end local v8    # "fout":Ljava/io/FileOutputStream;
    .end local v9    # "i":I
    .end local v10    # "length":I
    .end local v11    # "tail":[Ljava/lang/String;
    :cond_104
    invoke-virtual {v13}, Ljava/util/zip/ZipInputStream;->close()V

    .line 1248
    invoke-virtual {v7}, Ljava/io/FileInputStream;->close()V
    :try_end_10a
    .catch Ljava/lang/Exception; {:try_start_83 .. :try_end_10a} :catch_25

    .line 1274
    .end local v7    # "fin":Ljava/io/FileInputStream;
    .end local v12    # "ze":Ljava/util/zip/ZipEntry;
    .end local v13    # "zin":Ljava/util/zip/ZipInputStream;
    :cond_10a
    :goto_10a
    return-void

    .line 1267
    .restart local v3    # "e":Ljava/lang/Exception;
    :catch_10b
    move-exception v4

    .line 1268
    .local v4, "e1":Lnet/lingala/zip4j/exception/ZipException;
    invoke-virtual {v4}, Lnet/lingala/zip4j/exception/ZipException;->printStackTrace()V

    goto :goto_10a

    .line 1269
    .end local v4    # "e1":Lnet/lingala/zip4j/exception/ZipException;
    :catch_110
    move-exception v4

    .line 1270
    .local v4, "e1":Ljava/lang/Exception;
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_10a
.end method
