.class public Lcom/chelpus/root/utils/createnerorunpatch;
.super Ljava/lang/Object;
.source "createnerorunpatch.java"


# static fields
.field public static classes:Ljava/io/File;

.field public static crkapk:Ljava/io/File;

.field private static dalvikDexIn:Ljava/lang/String;

.field public static sddir:Ljava/lang/String;

.field public static tooldir:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 14
    const-string v0, "/data/dalvik-cache/data@app@zamenitetodelo-1.apk@classes.dex"

    sput-object v0, Lcom/chelpus/root/utils/createnerorunpatch;->dalvikDexIn:Ljava/lang/String;

    .line 15
    const-string v0, "/sdcard/"

    sput-object v0, Lcom/chelpus/root/utils/createnerorunpatch;->sddir:Ljava/lang/String;

    .line 16
    const-string v0, "/sdcard/"

    sput-object v0, Lcom/chelpus/root/utils/createnerorunpatch;->tooldir:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static main([Ljava/lang/String;)V
    .registers 60
    .param p0, "paramArrayOfString"    # [Ljava/lang/String;

    .prologue
    .line 23
    new-instance v3, Lcom/chelpus/root/utils/createnerorunpatch$1;

    invoke-direct {v3}, Lcom/chelpus/root/utils/createnerorunpatch$1;-><init>()V

    invoke-static {v3}, Lcom/chelpus/Utils;->startRootJava(Ljava/lang/Object;)V

    .line 25
    const/4 v3, 0x2

    aget-object v3, p0, v3

    sput-object v3, Lcom/chelpus/root/utils/createnerorunpatch;->sddir:Ljava/lang/String;

    .line 26
    const/4 v3, 0x3

    aget-object v3, p0, v3

    sput-object v3, Lcom/chelpus/root/utils/createnerorunpatch;->tooldir:Ljava/lang/String;

    .line 30
    const/16 v3, 0x18

    new-array v8, v3, [B

    fill-array-data v8, :array_676

    .line 31
    .local v8, "byteOrig":[B
    const/16 v3, 0x18

    new-array v0, v3, [B

    move-object/from16 v32, v0

    fill-array-data v32, :array_686

    .line 33
    .local v32, "mask":[B
    const/16 v3, 0x8

    new-array v10, v3, [B

    fill-array-data v10, :array_696

    .line 34
    .local v10, "byteOrig2":[B
    const/16 v3, 0x8

    new-array v0, v3, [B

    move-object/from16 v34, v0

    fill-array-data v34, :array_69e

    .line 35
    .local v34, "mask2":[B
    const/16 v3, 0x8

    new-array v0, v3, [B

    move-object/from16 v18, v0

    fill-array-data v18, :array_6a6

    .line 36
    .local v18, "byteReplace2":[B
    const/16 v3, 0x8

    new-array v0, v3, [B

    move-object/from16 v51, v0

    fill-array-data v51, :array_6ae

    .line 38
    .local v51, "rep_mask2":[B
    const/16 v3, 0x8

    new-array v11, v3, [B

    fill-array-data v11, :array_6b6

    .line 39
    .local v11, "byteOrig3":[B
    const/16 v3, 0x8

    new-array v0, v3, [B

    move-object/from16 v35, v0

    fill-array-data v35, :array_6be

    .line 40
    .local v35, "mask3":[B
    const/16 v3, 0x8

    new-array v0, v3, [B

    move-object/from16 v19, v0

    fill-array-data v19, :array_6c6

    .line 41
    .local v19, "byteReplace3":[B
    const/16 v3, 0x8

    new-array v0, v3, [B

    move-object/from16 v52, v0

    fill-array-data v52, :array_6ce

    .line 43
    .local v52, "rep_mask3":[B
    const/16 v3, 0x8

    new-array v12, v3, [B

    fill-array-data v12, :array_6d6

    .line 44
    .local v12, "byteOrig4":[B
    const/16 v3, 0x8

    new-array v0, v3, [B

    move-object/from16 v36, v0

    fill-array-data v36, :array_6de

    .line 45
    .local v36, "mask4":[B
    const/16 v3, 0x8

    new-array v0, v3, [B

    move-object/from16 v20, v0

    fill-array-data v20, :array_6e6

    .line 46
    .local v20, "byteReplace4":[B
    const/16 v3, 0x8

    new-array v0, v3, [B

    move-object/from16 v53, v0

    fill-array-data v53, :array_6ee

    .line 48
    .local v53, "rep_mask4":[B
    const/16 v3, 0x8

    new-array v13, v3, [B

    fill-array-data v13, :array_6f6

    .line 49
    .local v13, "byteOrig5":[B
    const/16 v3, 0x8

    new-array v0, v3, [B

    move-object/from16 v37, v0

    fill-array-data v37, :array_6fe

    .line 50
    .local v37, "mask5":[B
    const/16 v3, 0x8

    new-array v0, v3, [B

    move-object/from16 v21, v0

    fill-array-data v21, :array_706

    .line 51
    .local v21, "byteReplace5":[B
    const/16 v3, 0x8

    new-array v0, v3, [B

    move-object/from16 v54, v0

    fill-array-data v54, :array_70e

    .line 53
    .local v54, "rep_mask5":[B
    const/16 v3, 0x8

    new-array v14, v3, [B

    fill-array-data v14, :array_716

    .line 54
    .local v14, "byteOrig6":[B
    const/16 v3, 0x8

    new-array v0, v3, [B

    move-object/from16 v38, v0

    fill-array-data v38, :array_71e

    .line 55
    .local v38, "mask6":[B
    const/16 v3, 0x8

    new-array v0, v3, [B

    move-object/from16 v22, v0

    fill-array-data v22, :array_726

    .line 56
    .local v22, "byteReplace6":[B
    const/16 v3, 0x8

    new-array v0, v3, [B

    move-object/from16 v55, v0

    fill-array-data v55, :array_72e

    .line 58
    .local v55, "rep_mask6":[B
    const/16 v3, 0x8

    new-array v15, v3, [B

    fill-array-data v15, :array_736

    .line 59
    .local v15, "byteOrig8":[B
    const/16 v3, 0x8

    new-array v0, v3, [B

    move-object/from16 v39, v0

    fill-array-data v39, :array_73e

    .line 60
    .local v39, "mask8":[B
    const/16 v3, 0x8

    new-array v0, v3, [B

    move-object/from16 v23, v0

    fill-array-data v23, :array_746

    .line 61
    .local v23, "byteReplace8":[B
    const/16 v3, 0x8

    new-array v0, v3, [B

    move-object/from16 v56, v0

    fill-array-data v56, :array_74e

    .line 63
    .local v56, "rep_mask8":[B
    const/16 v3, 0x10

    new-array v0, v3, [B

    move-object/from16 v16, v0

    fill-array-data v16, :array_756

    .line 64
    .local v16, "byteOrig9":[B
    const/16 v3, 0x10

    new-array v0, v3, [B

    move-object/from16 v40, v0

    fill-array-data v40, :array_762

    .line 65
    .local v40, "mask9":[B
    const/16 v3, 0x10

    new-array v0, v3, [B

    move-object/from16 v24, v0

    fill-array-data v24, :array_76e

    .line 66
    .local v24, "byteReplace9":[B
    const/16 v3, 0x10

    new-array v0, v3, [B

    move-object/from16 v57, v0

    fill-array-data v57, :array_77a

    .line 68
    .local v57, "rep_mask9":[B
    const/16 v3, 0x10

    new-array v9, v3, [B

    fill-array-data v9, :array_786

    .line 69
    .local v9, "byteOrig10":[B
    const/16 v3, 0x10

    new-array v0, v3, [B

    move-object/from16 v33, v0

    fill-array-data v33, :array_792

    .line 70
    .local v33, "mask10":[B
    const/16 v3, 0x10

    new-array v0, v3, [B

    move-object/from16 v17, v0

    fill-array-data v17, :array_79e

    .line 71
    .local v17, "byteReplace10":[B
    const/16 v3, 0x10

    new-array v0, v3, [B

    move-object/from16 v50, v0

    fill-array-data v50, :array_7aa

    .line 75
    .local v50, "rep_mask10":[B
    sget-object v3, Lcom/chelpus/root/utils/createnerorunpatch;->dalvikDexIn:Ljava/lang/String;

    const-string v4, "zamenitetodelo"

    const/4 v5, 0x0

    aget-object v5, p0, v5

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    .line 80
    :try_start_13e
    new-instance v3, Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/chelpus/root/utils/createnerorunpatch;->sddir:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/Modified/classes.dex"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v3, Lcom/chelpus/root/utils/createnerorunpatch;->classes:Ljava/io/File;

    .line 82
    sget-object v3, Lcom/chelpus/root/utils/createnerorunpatch;->classes:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_4d8

    new-instance v3, Ljava/io/FileNotFoundException;

    invoke-direct {v3}, Ljava/io/FileNotFoundException;-><init>()V

    throw v3
    :try_end_168
    .catch Ljava/io/FileNotFoundException; {:try_start_13e .. :try_end_168} :catch_168
    .catch Ljava/io/IOException; {:try_start_13e .. :try_end_168} :catch_602

    .line 165
    :catch_168
    move-exception v31

    .line 166
    .local v31, "localFileNotFoundException":Ljava/io/FileNotFoundException;
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v4, "Error: Program files are not found!\n\nCheck the location dalvik-cache to solve problems!\n\nDefault: /data/dalvik-cache/*"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 171
    .end local v31    # "localFileNotFoundException":Ljava/io/FileNotFoundException;
    :cond_170
    :goto_170
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/4 v5, 0x2

    aget-byte v5, v10, v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 172
    const/4 v3, 0x1

    aget-object v3, p0, v3

    const-string v4, "object"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_4d4

    .line 175
    :try_start_196
    new-instance v3, Ljava/io/RandomAccessFile;

    sget-object v4, Lcom/chelpus/root/utils/createnerorunpatch;->classes:Ljava/io/File;

    const-string v5, "rw"

    invoke-direct {v3, v4, v5}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v2

    .line 178
    .local v2, "ChannelDex":Ljava/nio/channels/FileChannel;
    sget-object v3, Ljava/nio/channels/FileChannel$MapMode;->READ_WRITE:Ljava/nio/channels/FileChannel$MapMode;

    const-wide/16 v4, 0x0

    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v6

    long-to-int v6, v6

    int-to-long v6, v6

    invoke-virtual/range {v2 .. v7}, Ljava/nio/channels/FileChannel;->map(Ljava/nio/channels/FileChannel$MapMode;JJ)Ljava/nio/MappedByteBuffer;
    :try_end_1b0
    .catch Ljava/io/FileNotFoundException; {:try_start_196 .. :try_end_1b0} :catch_64f
    .catch Ljava/io/IOException; {:try_start_196 .. :try_end_1b0} :catch_659

    move-result-object v29

    .line 180
    .local v29, "fileBytes":Ljava/nio/MappedByteBuffer;
    const/16 v43, 0x0

    .line 181
    .local v43, "patch":Z
    const/16 v44, 0x0

    .line 182
    .local v44, "patch1":Z
    const/16 v45, 0x0

    .line 183
    .local v45, "patch2":Z
    const/16 v46, 0x0

    .line 184
    .local v46, "patch3":Z
    const/16 v47, 0x0

    .line 185
    .local v47, "patch4":Z
    const/16 v48, 0x0

    .line 186
    .local v48, "patch5":Z
    const/16 v27, 0x0

    .line 187
    .local v27, "curentPos":I
    const/16 v26, -0x1

    .line 189
    .local v26, "curentByte":B
    :cond_1c1
    :goto_1c1
    :try_start_1c1
    invoke-virtual/range {v29 .. v29}, Ljava/nio/MappedByteBuffer;->hasRemaining()Z

    move-result v3

    if-eqz v3, :cond_49b

    .line 191
    add-int/lit8 v3, v27, 0x1

    move-object/from16 v0, v29

    invoke-virtual {v0, v3}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 192
    invoke-virtual/range {v29 .. v29}, Ljava/nio/MappedByteBuffer;->position()I

    move-result v27

    .line 193
    invoke-virtual/range {v29 .. v29}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v26

    .line 196
    const/4 v3, 0x0

    aget-byte v3, v10, v3

    move/from16 v0, v26

    if-ne v0, v3, :cond_22b

    .line 198
    const/4 v3, 0x0

    aget-byte v3, v51, v3

    if-nez v3, :cond_1e5

    const/4 v3, 0x0

    aput-byte v26, v18, v3

    .line 199
    :cond_1e5
    const/16 v30, 0x1

    .line 200
    .local v30, "i":I
    add-int/lit8 v3, v27, 0x1

    move-object/from16 v0, v29

    invoke-virtual {v0, v3}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 201
    invoke-virtual/range {v29 .. v29}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v49

    .line 203
    .local v49, "prufbyte":B
    :goto_1f2
    aget-byte v3, v10, v30

    move/from16 v0, v49

    if-eq v0, v3, :cond_1fd

    aget-byte v3, v34, v30

    const/4 v4, 0x1

    if-ne v3, v4, :cond_224

    .line 205
    :cond_1fd
    aget-byte v3, v51, v30

    if-nez v3, :cond_203

    aput-byte v49, v18, v30

    .line 206
    :cond_203
    add-int/lit8 v30, v30, 0x1

    .line 208
    array-length v3, v10

    move/from16 v0, v30

    if-ne v0, v3, :cond_61f

    .line 210
    move-object/from16 v0, v29

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 211
    move-object/from16 v0, v29

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/nio/MappedByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 212
    invoke-virtual/range {v29 .. v29}, Ljava/nio/MappedByteBuffer;->force()Ljava/nio/MappedByteBuffer;

    .line 214
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v4, "Done!\n"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 215
    const/16 v44, 0x1

    .line 224
    :cond_224
    add-int/lit8 v3, v27, 0x1

    move-object/from16 v0, v29

    invoke-virtual {v0, v3}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 228
    .end local v30    # "i":I
    .end local v49    # "prufbyte":B
    :cond_22b
    const/4 v3, 0x0

    aget-byte v3, v13, v3

    move/from16 v0, v26

    if-ne v0, v3, :cond_280

    .line 230
    const/4 v3, 0x0

    aget-byte v3, v54, v3

    if-nez v3, :cond_23a

    const/4 v3, 0x0

    aput-byte v26, v21, v3

    .line 231
    :cond_23a
    const/16 v30, 0x1

    .line 232
    .restart local v30    # "i":I
    add-int/lit8 v3, v27, 0x1

    move-object/from16 v0, v29

    invoke-virtual {v0, v3}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 233
    invoke-virtual/range {v29 .. v29}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v49

    .line 235
    .restart local v49    # "prufbyte":B
    :goto_247
    aget-byte v3, v13, v30

    move/from16 v0, v49

    if-eq v0, v3, :cond_252

    aget-byte v3, v37, v30

    const/4 v4, 0x1

    if-ne v3, v4, :cond_279

    .line 237
    :cond_252
    aget-byte v3, v54, v30

    if-nez v3, :cond_258

    aput-byte v49, v21, v30

    .line 238
    :cond_258
    add-int/lit8 v30, v30, 0x1

    .line 239
    array-length v3, v13

    move/from16 v0, v30

    if-ne v0, v3, :cond_625

    .line 241
    move-object/from16 v0, v29

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 242
    move-object/from16 v0, v29

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/nio/MappedByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 243
    invoke-virtual/range {v29 .. v29}, Ljava/nio/MappedByteBuffer;->force()Ljava/nio/MappedByteBuffer;

    .line 245
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v4, "Done!\n"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 247
    const/16 v45, 0x1

    .line 252
    :cond_279
    add-int/lit8 v3, v27, 0x1

    move-object/from16 v0, v29

    invoke-virtual {v0, v3}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 254
    .end local v30    # "i":I
    .end local v49    # "prufbyte":B
    :cond_280
    const/4 v3, 0x0

    aget-byte v3, v11, v3

    move/from16 v0, v26

    if-ne v0, v3, :cond_2d5

    .line 256
    const/4 v3, 0x0

    aget-byte v3, v52, v3

    if-nez v3, :cond_28f

    const/4 v3, 0x0

    aput-byte v26, v19, v3

    .line 257
    :cond_28f
    const/16 v30, 0x1

    .line 258
    .restart local v30    # "i":I
    add-int/lit8 v3, v27, 0x1

    move-object/from16 v0, v29

    invoke-virtual {v0, v3}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 259
    invoke-virtual/range {v29 .. v29}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v49

    .line 261
    .restart local v49    # "prufbyte":B
    :goto_29c
    aget-byte v3, v11, v30

    move/from16 v0, v49

    if-eq v0, v3, :cond_2a7

    aget-byte v3, v35, v30

    const/4 v4, 0x1

    if-ne v3, v4, :cond_2ce

    .line 263
    :cond_2a7
    aget-byte v3, v52, v30

    if-nez v3, :cond_2ad

    aput-byte v49, v19, v30

    .line 264
    :cond_2ad
    add-int/lit8 v30, v30, 0x1

    .line 266
    array-length v3, v11

    move/from16 v0, v30

    if-ne v0, v3, :cond_62b

    .line 268
    move-object/from16 v0, v29

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 269
    move-object/from16 v0, v29

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/nio/MappedByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 270
    invoke-virtual/range {v29 .. v29}, Ljava/nio/MappedByteBuffer;->force()Ljava/nio/MappedByteBuffer;

    .line 272
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v4, "Done!\n"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 273
    const/16 v46, 0x1

    .line 278
    :cond_2ce
    add-int/lit8 v3, v27, 0x1

    move-object/from16 v0, v29

    invoke-virtual {v0, v3}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 280
    .end local v30    # "i":I
    .end local v49    # "prufbyte":B
    :cond_2d5
    const/4 v3, 0x0

    aget-byte v3, v12, v3

    move/from16 v0, v26

    if-ne v0, v3, :cond_32a

    .line 282
    const/4 v3, 0x0

    aget-byte v3, v53, v3

    if-nez v3, :cond_2e4

    const/4 v3, 0x0

    aput-byte v26, v20, v3

    .line 283
    :cond_2e4
    const/16 v30, 0x1

    .line 284
    .restart local v30    # "i":I
    add-int/lit8 v3, v27, 0x1

    move-object/from16 v0, v29

    invoke-virtual {v0, v3}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 285
    invoke-virtual/range {v29 .. v29}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v49

    .line 287
    .restart local v49    # "prufbyte":B
    :goto_2f1
    aget-byte v3, v12, v30

    move/from16 v0, v49

    if-eq v0, v3, :cond_2fc

    aget-byte v3, v36, v30

    const/4 v4, 0x1

    if-ne v3, v4, :cond_323

    .line 289
    :cond_2fc
    aget-byte v3, v53, v30

    if-nez v3, :cond_302

    aput-byte v49, v20, v30

    .line 290
    :cond_302
    add-int/lit8 v30, v30, 0x1

    .line 291
    array-length v3, v12

    move/from16 v0, v30

    if-ne v0, v3, :cond_631

    .line 293
    move-object/from16 v0, v29

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 294
    move-object/from16 v0, v29

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/nio/MappedByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 295
    invoke-virtual/range {v29 .. v29}, Ljava/nio/MappedByteBuffer;->force()Ljava/nio/MappedByteBuffer;

    .line 297
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v4, "Done!\n"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 299
    const/16 v47, 0x1

    .line 304
    :cond_323
    add-int/lit8 v3, v27, 0x1

    move-object/from16 v0, v29

    invoke-virtual {v0, v3}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 306
    .end local v30    # "i":I
    .end local v49    # "prufbyte":B
    :cond_32a
    const/4 v3, 0x0

    aget-byte v3, v14, v3

    move/from16 v0, v26

    if-ne v0, v3, :cond_37f

    .line 308
    const/4 v3, 0x0

    aget-byte v3, v55, v3

    if-nez v3, :cond_339

    const/4 v3, 0x0

    aput-byte v26, v22, v3

    .line 309
    :cond_339
    const/16 v30, 0x1

    .line 310
    .restart local v30    # "i":I
    add-int/lit8 v3, v27, 0x1

    move-object/from16 v0, v29

    invoke-virtual {v0, v3}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 311
    invoke-virtual/range {v29 .. v29}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v49

    .line 313
    .restart local v49    # "prufbyte":B
    :goto_346
    aget-byte v3, v14, v30

    move/from16 v0, v49

    if-eq v0, v3, :cond_351

    aget-byte v3, v38, v30

    const/4 v4, 0x1

    if-ne v3, v4, :cond_378

    .line 315
    :cond_351
    aget-byte v3, v55, v30

    if-nez v3, :cond_357

    aput-byte v49, v22, v30

    .line 316
    :cond_357
    add-int/lit8 v30, v30, 0x1

    .line 318
    array-length v3, v14

    move/from16 v0, v30

    if-ne v0, v3, :cond_637

    .line 320
    move-object/from16 v0, v29

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 321
    move-object/from16 v0, v29

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/nio/MappedByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 322
    invoke-virtual/range {v29 .. v29}, Ljava/nio/MappedByteBuffer;->force()Ljava/nio/MappedByteBuffer;

    .line 324
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v4, "Done!\n"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 325
    const/16 v48, 0x1

    .line 330
    :cond_378
    add-int/lit8 v3, v27, 0x1

    move-object/from16 v0, v29

    invoke-virtual {v0, v3}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 332
    .end local v30    # "i":I
    .end local v49    # "prufbyte":B
    :cond_37f
    const/4 v3, 0x0

    aget-byte v3, v16, v3

    move/from16 v0, v26

    if-ne v0, v3, :cond_3d6

    .line 334
    const/4 v3, 0x0

    aget-byte v3, v57, v3

    if-nez v3, :cond_38e

    const/4 v3, 0x0

    aput-byte v26, v24, v3

    .line 335
    :cond_38e
    const/16 v30, 0x1

    .line 336
    .restart local v30    # "i":I
    add-int/lit8 v3, v27, 0x1

    move-object/from16 v0, v29

    invoke-virtual {v0, v3}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 337
    invoke-virtual/range {v29 .. v29}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v49

    .line 339
    .restart local v49    # "prufbyte":B
    :goto_39b
    aget-byte v3, v16, v30

    move/from16 v0, v49

    if-eq v0, v3, :cond_3a6

    aget-byte v3, v40, v30

    const/4 v4, 0x1

    if-ne v3, v4, :cond_3cf

    .line 341
    :cond_3a6
    aget-byte v3, v57, v30

    if-nez v3, :cond_3ac

    aput-byte v49, v24, v30

    .line 342
    :cond_3ac
    add-int/lit8 v30, v30, 0x1

    .line 344
    move-object/from16 v0, v16

    array-length v3, v0

    move/from16 v0, v30

    if-ne v0, v3, :cond_63d

    .line 346
    move-object/from16 v0, v29

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 347
    move-object/from16 v0, v29

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/nio/MappedByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 348
    invoke-virtual/range {v29 .. v29}, Ljava/nio/MappedByteBuffer;->force()Ljava/nio/MappedByteBuffer;

    .line 350
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v4, "Done!\n"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 351
    const/16 v48, 0x1

    .line 356
    :cond_3cf
    add-int/lit8 v3, v27, 0x1

    move-object/from16 v0, v29

    invoke-virtual {v0, v3}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 358
    .end local v30    # "i":I
    .end local v49    # "prufbyte":B
    :cond_3d6
    const/4 v3, 0x0

    aget-byte v3, v9, v3

    move/from16 v0, v26

    if-ne v0, v3, :cond_42b

    .line 360
    const/4 v3, 0x0

    aget-byte v3, v50, v3

    if-nez v3, :cond_3e5

    const/4 v3, 0x0

    aput-byte v26, v17, v3

    .line 361
    :cond_3e5
    const/16 v30, 0x1

    .line 362
    .restart local v30    # "i":I
    add-int/lit8 v3, v27, 0x1

    move-object/from16 v0, v29

    invoke-virtual {v0, v3}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 363
    invoke-virtual/range {v29 .. v29}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v49

    .line 365
    .restart local v49    # "prufbyte":B
    :goto_3f2
    aget-byte v3, v9, v30

    move/from16 v0, v49

    if-eq v0, v3, :cond_3fd

    aget-byte v3, v33, v30

    const/4 v4, 0x1

    if-ne v3, v4, :cond_424

    .line 367
    :cond_3fd
    aget-byte v3, v50, v30

    if-nez v3, :cond_403

    aput-byte v49, v17, v30

    .line 368
    :cond_403
    add-int/lit8 v30, v30, 0x1

    .line 370
    array-length v3, v9

    move/from16 v0, v30

    if-ne v0, v3, :cond_643

    .line 372
    move-object/from16 v0, v29

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 373
    move-object/from16 v0, v29

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/nio/MappedByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 374
    invoke-virtual/range {v29 .. v29}, Ljava/nio/MappedByteBuffer;->force()Ljava/nio/MappedByteBuffer;

    .line 376
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v4, "Done!\n"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 377
    const/16 v48, 0x1

    .line 382
    :cond_424
    add-int/lit8 v3, v27, 0x1

    move-object/from16 v0, v29

    invoke-virtual {v0, v3}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 384
    .end local v30    # "i":I
    .end local v49    # "prufbyte":B
    :cond_42b
    const/4 v3, 0x0

    aget-byte v3, v15, v3

    move/from16 v0, v26

    if-ne v0, v3, :cond_1c1

    .line 386
    const/4 v3, 0x0

    aget-byte v3, v56, v3

    if-nez v3, :cond_43a

    const/4 v3, 0x0

    aput-byte v26, v23, v3

    .line 387
    :cond_43a
    const/16 v30, 0x1

    .line 388
    .restart local v30    # "i":I
    add-int/lit8 v3, v27, 0x1

    move-object/from16 v0, v29

    invoke-virtual {v0, v3}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 389
    invoke-virtual/range {v29 .. v29}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v49

    .line 391
    .restart local v49    # "prufbyte":B
    :goto_447
    aget-byte v3, v15, v30

    move/from16 v0, v49

    if-eq v0, v3, :cond_452

    aget-byte v3, v39, v30

    const/4 v4, 0x1

    if-ne v3, v4, :cond_477

    .line 393
    :cond_452
    aget-byte v3, v56, v30

    if-nez v3, :cond_458

    aput-byte v49, v23, v30

    .line 394
    :cond_458
    add-int/lit8 v30, v30, 0x1

    .line 396
    array-length v3, v15

    move/from16 v0, v30

    if-ne v0, v3, :cond_649

    .line 398
    move-object/from16 v0, v29

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 399
    move-object/from16 v0, v29

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/nio/MappedByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 400
    invoke-virtual/range {v29 .. v29}, Ljava/nio/MappedByteBuffer;->force()Ljava/nio/MappedByteBuffer;

    .line 402
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v4, "Done!\n"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 407
    :cond_477
    add-int/lit8 v3, v27, 0x1

    move-object/from16 v0, v29

    invoke-virtual {v0, v3}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;
    :try_end_47e
    .catch Ljava/lang/Exception; {:try_start_1c1 .. :try_end_47e} :catch_480
    .catch Ljava/io/FileNotFoundException; {:try_start_1c1 .. :try_end_47e} :catch_64f
    .catch Ljava/io/IOException; {:try_start_1c1 .. :try_end_47e} :catch_659

    goto/16 :goto_1c1

    .line 413
    .end local v30    # "i":I
    .end local v49    # "prufbyte":B
    :catch_480
    move-exception v28

    .line 414
    .local v28, "e":Ljava/lang/Exception;
    :try_start_481
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v28

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 416
    .end local v28    # "e":Ljava/lang/Exception;
    :cond_49b
    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->close()V

    .line 418
    if-nez v44, :cond_4a7

    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v4, "Error: License Key2 patch Failed!\nor patch is already applied?!\n"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 419
    :cond_4a7
    if-nez v45, :cond_4b0

    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v4, "Error: Internet Connection patch Failed!\nor patch is already applied?!\n\n"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 420
    :cond_4b0
    if-nez v46, :cond_4b9

    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v4, "Error: Check License Key patch Failed!\nor patch is already applied?!\n"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 421
    :cond_4b9
    if-nez v47, :cond_4c2

    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v4, "Error: Market Free patch Failed!\nor patch is already applied?!\n"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 422
    :cond_4c2
    if-nez v48, :cond_4cb

    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v4, "Error: Market Free patch Failed!\nor patch is already applied?!\n"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 423
    :cond_4cb
    if-nez v43, :cond_4d4

    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v4, "Error: Cached License Key patch Failed!\nor patch is already applied?!\n"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_4d4
    .catch Ljava/io/FileNotFoundException; {:try_start_481 .. :try_end_4d4} :catch_64f
    .catch Ljava/io/IOException; {:try_start_481 .. :try_end_4d4} :catch_659

    .line 436
    .end local v2    # "ChannelDex":Ljava/nio/channels/FileChannel;
    .end local v26    # "curentByte":B
    .end local v27    # "curentPos":I
    .end local v29    # "fileBytes":Ljava/nio/MappedByteBuffer;
    .end local v43    # "patch":Z
    .end local v44    # "patch1":Z
    .end local v45    # "patch2":Z
    .end local v46    # "patch3":Z
    .end local v47    # "patch4":Z
    .end local v48    # "patch5":Z
    :cond_4d4
    :goto_4d4
    invoke-static {}, Lcom/chelpus/Utils;->exitFromRootJava()V

    .line 437
    return-void

    .line 87
    :cond_4d8
    const/16 v43, 0x0

    .line 88
    .restart local v43    # "patch":Z
    const/16 v58, 0x1

    .line 89
    .local v58, "search":Z
    const/16 v27, 0x0

    .restart local v27    # "curentPos":I
    const/16 v41, 0x0

    .local v41, "o":I
    const/16 v42, 0x3e8

    .line 90
    .local v42, "p":I
    const/4 v3, 0x1

    :try_start_4e3
    aget-object v3, p0, v3

    const-string v4, "object"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_4fc

    const/4 v3, 0x1

    aget-object v3, p0, v3

    const-string v4, "object"

    const-string v5, ""

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v42

    .line 93
    :cond_4fc
    new-instance v3, Ljava/io/RandomAccessFile;

    sget-object v4, Lcom/chelpus/root/utils/createnerorunpatch;->classes:Ljava/io/File;

    const-string v5, "rw"

    invoke-direct {v3, v4, v5}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v2

    .line 96
    .restart local v2    # "ChannelDex":Ljava/nio/channels/FileChannel;
    sget-object v3, Ljava/nio/channels/FileChannel$MapMode;->READ_WRITE:Ljava/nio/channels/FileChannel$MapMode;

    const-wide/16 v4, 0x0

    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v6

    long-to-int v6, v6

    int-to-long v6, v6

    invoke-virtual/range {v2 .. v7}, Ljava/nio/channels/FileChannel;->map(Ljava/nio/channels/FileChannel$MapMode;JJ)Ljava/nio/MappedByteBuffer;
    :try_end_516
    .catch Ljava/io/FileNotFoundException; {:try_start_4e3 .. :try_end_516} :catch_168
    .catch Ljava/io/IOException; {:try_start_4e3 .. :try_end_516} :catch_602

    move-result-object v29

    .line 99
    .restart local v29    # "fileBytes":Ljava/nio/MappedByteBuffer;
    const/16 v26, -0x1

    .line 101
    .restart local v26    # "curentByte":B
    :cond_519
    :try_start_519
    invoke-virtual/range {v29 .. v29}, Ljava/nio/MappedByteBuffer;->hasRemaining()Z

    move-result v3

    if-eqz v3, :cond_5d4

    move/from16 v0, v41

    move/from16 v1, v42

    if-ge v0, v1, :cond_5d4

    .line 103
    const/16 v58, 0x1

    .line 104
    add-int/lit8 v3, v27, 0x1

    move-object/from16 v0, v29

    invoke-virtual {v0, v3}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 105
    invoke-virtual/range {v29 .. v29}, Ljava/nio/MappedByteBuffer;->position()I

    move-result v27

    .line 106
    invoke-virtual/range {v29 .. v29}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v26

    .line 110
    const/4 v3, 0x0

    aget-byte v3, v8, v3

    move/from16 v0, v26

    if-ne v0, v3, :cond_519

    .line 113
    const/16 v30, 0x1

    .restart local v30    # "i":I
    const/16 v25, 0x2

    .line 114
    .local v25, "c":I
    add-int/lit8 v3, v27, 0x1

    move-object/from16 v0, v29

    invoke-virtual {v0, v3}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 115
    invoke-virtual/range {v29 .. v29}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v49

    .line 117
    .restart local v49    # "prufbyte":B
    :goto_54c
    if-eqz v58, :cond_519

    aget-byte v3, v8, v30

    move/from16 v0, v49

    if-eq v0, v3, :cond_55e

    aget-byte v3, v32, v30

    const/4 v4, 0x1

    if-eq v3, v4, :cond_55e

    aget-byte v3, v32, v30

    const/4 v4, 0x5

    if-ne v3, v4, :cond_519

    .line 120
    :cond_55e
    aget-byte v3, v32, v30

    const/4 v4, 0x5

    if-ne v3, v4, :cond_586

    .line 121
    aput-byte v49, v10, v25

    aput-byte v49, v13, v25

    aput-byte v49, v16, v25

    aput-byte v49, v9, v25

    .line 122
    const/4 v3, 0x2

    move/from16 v0, v25

    if-ne v0, v3, :cond_5ca

    .line 124
    add-int/lit8 v3, v49, 0x1

    int-to-byte v3, v3

    aput-byte v3, v11, v25

    add-int/lit8 v3, v49, 0x2

    int-to-byte v3, v3

    aput-byte v3, v12, v25

    .line 125
    add-int/lit8 v3, v49, 0x1

    int-to-byte v3, v3

    aput-byte v3, v14, v25

    add-int/lit8 v3, v49, 0x2

    int-to-byte v3, v3

    aput-byte v3, v15, v25

    .line 128
    :goto_584
    add-int/lit8 v25, v25, 0x1

    .line 130
    :cond_586
    add-int/lit8 v30, v30, 0x1

    .line 132
    array-length v3, v8

    move/from16 v0, v30

    if-ne v0, v3, :cond_5c5

    .line 140
    add-int/lit8 v41, v41, 0x1

    .line 142
    const/16 v3, 0x3e8

    move/from16 v0, v42

    if-ne v0, v3, :cond_5c1

    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Serach "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v41

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " - Done!\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "byteOrig2[2]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 143
    :cond_5c1
    const/16 v58, 0x0

    .line 144
    const/16 v43, 0x1

    .line 148
    :cond_5c5
    invoke-virtual/range {v29 .. v29}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v49

    goto :goto_54c

    .line 127
    :cond_5ca
    aput-byte v49, v11, v25

    aput-byte v49, v12, v25

    aput-byte v49, v14, v25

    aput-byte v49, v15, v25
    :try_end_5d2
    .catch Ljava/lang/Exception; {:try_start_519 .. :try_end_5d2} :catch_5d3
    .catch Ljava/io/FileNotFoundException; {:try_start_519 .. :try_end_5d2} :catch_168
    .catch Ljava/io/IOException; {:try_start_519 .. :try_end_5d2} :catch_602

    goto :goto_584

    .line 157
    .end local v25    # "c":I
    .end local v30    # "i":I
    .end local v49    # "prufbyte":B
    :catch_5d3
    move-exception v3

    .line 160
    :cond_5d4
    :try_start_5d4
    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->close()V

    .line 161
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "{\"objects\":\""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v41

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\"}"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 163
    if-nez v43, :cond_170

    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v4, "Error: Objects not Found!\n"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_600
    .catch Ljava/io/FileNotFoundException; {:try_start_5d4 .. :try_end_600} :catch_168
    .catch Ljava/io/IOException; {:try_start_5d4 .. :try_end_600} :catch_602

    goto/16 :goto_170

    .line 167
    .end local v2    # "ChannelDex":Ljava/nio/channels/FileChannel;
    .end local v26    # "curentByte":B
    .end local v27    # "curentPos":I
    .end local v29    # "fileBytes":Ljava/nio/MappedByteBuffer;
    .end local v41    # "o":I
    .end local v42    # "p":I
    .end local v43    # "patch":Z
    .end local v58    # "search":Z
    :catch_602
    move-exception v31

    .line 168
    .local v31, "localFileNotFoundException":Ljava/io/IOException;
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v31

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_170

    .line 220
    .end local v31    # "localFileNotFoundException":Ljava/io/IOException;
    .restart local v2    # "ChannelDex":Ljava/nio/channels/FileChannel;
    .restart local v26    # "curentByte":B
    .restart local v27    # "curentPos":I
    .restart local v29    # "fileBytes":Ljava/nio/MappedByteBuffer;
    .restart local v30    # "i":I
    .restart local v43    # "patch":Z
    .restart local v44    # "patch1":Z
    .restart local v45    # "patch2":Z
    .restart local v46    # "patch3":Z
    .restart local v47    # "patch4":Z
    .restart local v48    # "patch5":Z
    .restart local v49    # "prufbyte":B
    :cond_61f
    :try_start_61f
    invoke-virtual/range {v29 .. v29}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v49

    goto/16 :goto_1f2

    .line 250
    :cond_625
    invoke-virtual/range {v29 .. v29}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v49

    goto/16 :goto_247

    .line 276
    :cond_62b
    invoke-virtual/range {v29 .. v29}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v49

    goto/16 :goto_29c

    .line 302
    :cond_631
    invoke-virtual/range {v29 .. v29}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v49

    goto/16 :goto_2f1

    .line 328
    :cond_637
    invoke-virtual/range {v29 .. v29}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v49

    goto/16 :goto_346

    .line 354
    :cond_63d
    invoke-virtual/range {v29 .. v29}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v49

    goto/16 :goto_39b

    .line 380
    :cond_643
    invoke-virtual/range {v29 .. v29}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v49

    goto/16 :goto_3f2

    .line 405
    :cond_649
    invoke-virtual/range {v29 .. v29}, Ljava/nio/MappedByteBuffer;->get()B
    :try_end_64c
    .catch Ljava/lang/Exception; {:try_start_61f .. :try_end_64c} :catch_480
    .catch Ljava/io/FileNotFoundException; {:try_start_61f .. :try_end_64c} :catch_64f
    .catch Ljava/io/IOException; {:try_start_61f .. :try_end_64c} :catch_659

    move-result v49

    goto/16 :goto_447

    .line 425
    .end local v2    # "ChannelDex":Ljava/nio/channels/FileChannel;
    .end local v26    # "curentByte":B
    .end local v27    # "curentPos":I
    .end local v29    # "fileBytes":Ljava/nio/MappedByteBuffer;
    .end local v30    # "i":I
    .end local v43    # "patch":Z
    .end local v44    # "patch1":Z
    .end local v45    # "patch2":Z
    .end local v46    # "patch3":Z
    .end local v47    # "patch4":Z
    .end local v48    # "patch5":Z
    .end local v49    # "prufbyte":B
    :catch_64f
    move-exception v31

    .line 426
    .local v31, "localFileNotFoundException":Ljava/io/FileNotFoundException;
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v4, "Error: Program files are not found!\n\nCheck the location dalvik-cache to solve problems!\n\nDefault: /data/dalvik-cache/*"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_4d4

    .line 427
    .end local v31    # "localFileNotFoundException":Ljava/io/FileNotFoundException;
    :catch_659
    move-exception v31

    .line 428
    .local v31, "localFileNotFoundException":Ljava/io/IOException;
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v31

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_4d4

    .line 30
    :array_676
    .array-data 1
        0x12t
        0x24t
        0x12t
        0x13t
        0x12t
        0x2t
        0x22t
        0x0t
        0x6ft
        0x0t
        0x1at
        0x1t
        0x1t
        0x1t
        0x70t
        0x30t
        0x3dt
        0x1t
        0x10t
        0x2t
        0x69t
        0x0t
        0x4ct
        0x0t
    .end array-data

    .line 31
    :array_686
    .array-data 1
        0x0t
        0x1t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x5t
        0x5t
    .end array-data

    .line 33
    :array_696
    .array-data 1
        0x62t
        0x3t
        -0x1t
        -0x1t
        0x33t
        -0x1t
        -0x1t
        -0x1t
    .end array-data

    .line 34
    :array_69e
    .array-data 1
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
    .end array-data

    .line 35
    :array_6a6
    .array-data 1
        0x10t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 36
    :array_6ae
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
    .end array-data

    .line 38
    :array_6b6
    .array-data 1
        0x62t
        0x3t
        -0x1t
        -0x1t
        0x33t
        -0x1t
        -0x1t
        -0x1t
    .end array-data

    .line 39
    :array_6be
    .array-data 1
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
    .end array-data

    .line 40
    :array_6c6
    .array-data 1
        0x10t
        0x0t
        0x0t
        0x0t
        0x29t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 41
    :array_6ce
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
    .end array-data

    .line 43
    :array_6d6
    .array-data 1
        0x62t
        0x3t
        -0x1t
        -0x1t
        0x33t
        -0x1t
        -0x1t
        -0x1t
    .end array-data

    .line 44
    :array_6de
    .array-data 1
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
    .end array-data

    .line 45
    :array_6e6
    .array-data 1
        0x10t
        0x0t
        0x0t
        0x0t
        0x29t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 46
    :array_6ee
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
    .end array-data

    .line 48
    :array_6f6
    .array-data 1
        0x62t
        0x3t
        -0x1t
        -0x1t
        0x32t
        -0x1t
        -0x1t
        -0x1t
    .end array-data

    .line 49
    :array_6fe
    .array-data 1
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
    .end array-data

    .line 50
    :array_706
    .array-data 1
        0x10t
        0x0t
        0x0t
        0x0t
        0x29t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 51
    :array_70e
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
    .end array-data

    .line 53
    :array_716
    .array-data 1
        0x62t
        0x3t
        -0x1t
        -0x1t
        0x32t
        -0x1t
        -0x1t
        -0x1t
    .end array-data

    .line 54
    :array_71e
    .array-data 1
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
    .end array-data

    .line 55
    :array_726
    .array-data 1
        0x10t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 56
    :array_72e
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
    .end array-data

    .line 58
    :array_736
    .array-data 1
        0x62t
        0x3t
        -0x1t
        -0x1t
        0x32t
        -0x1t
        -0x1t
        -0x1t
    .end array-data

    .line 59
    :array_73e
    .array-data 1
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
    .end array-data

    .line 60
    :array_746
    .array-data 1
        0x10t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 61
    :array_74e
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
    .end array-data

    .line 63
    :array_756
    .array-data 1
        0x62t
        0x3t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        0xat
        -0x1t
        0x38t
        -0x1t
        -0x1t
        -0x1t
    .end array-data

    .line 64
    :array_762
    .array-data 1
        0x0t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
    .end array-data

    .line 65
    :array_76e
    .array-data 1
        0x10t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 66
    :array_77a
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
    .end array-data

    .line 68
    :array_786
    .array-data 1
        0x62t
        0x3t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        0xat
        -0x1t
        0x39t
        -0x1t
        -0x1t
        -0x1t
    .end array-data

    .line 69
    :array_792
    .array-data 1
        0x0t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
    .end array-data

    .line 70
    :array_79e
    .array-data 1
        0x10t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x29t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 71
    :array_7aa
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
    .end array-data
.end method
