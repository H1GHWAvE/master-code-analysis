.class public Lcom/chelpus/root/utils/restoredata;
.super Ljava/lang/Object;
.source "restoredata.java"


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static ExtractAllFilesWithInputStreams(Lnet/lingala/zip4j/core/ZipFile;Ljava/lang/String;Ljava/lang/String;)V
    .registers 24
    .param p0, "zipFile"    # Lnet/lingala/zip4j/core/ZipFile;
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "uid"    # Ljava/lang/String;

    .prologue
    .line 106
    const/4 v8, 0x0

    .line 107
    .local v8, "is":Lnet/lingala/zip4j/io/ZipInputStream;
    const/4 v9, 0x0

    .line 112
    .local v9, "os":Ljava/io/OutputStream;
    move-object/from16 v4, p1

    .line 115
    .local v4, "destinationPath":Ljava/lang/String;
    :try_start_4
    invoke-virtual/range {p0 .. p0}, Lnet/lingala/zip4j/core/ZipFile;->isEncrypted()Z

    move-result v16

    if-eqz v16, :cond_13

    .line 116
    const-string v16, "password"

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lnet/lingala/zip4j/core/ZipFile;->setPassword(Ljava/lang/String;)V

    .line 121
    :cond_13
    invoke-virtual/range {p0 .. p0}, Lnet/lingala/zip4j/core/ZipFile;->getFileHeaders()Ljava/util/List;

    move-result-object v7

    .line 124
    .local v7, "fileHeaderList":Ljava/util/List;
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;
    :try_end_1a
    .catch Lnet/lingala/zip4j/exception/ZipException; {:try_start_4 .. :try_end_1a} :catch_1ac
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_1a} :catch_286
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_1a} :catch_2a3
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_1a} :catch_2c0
    .catchall {:try_start_4 .. :try_end_1a} :catchall_2dd

    move-result-object v16

    move-object v10, v9

    .end local v9    # "os":Ljava/io/OutputStream;
    .local v10, "os":Ljava/io/OutputStream;
    :goto_1c
    :try_start_1c
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_265

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 125
    .local v2, "aFileHeaderList":Ljava/lang/Object;
    move-object v0, v2

    check-cast v0, Lnet/lingala/zip4j/model/FileHeader;

    move-object v6, v0

    .line 126
    .local v6, "fileHeader":Lnet/lingala/zip4j/model/FileHeader;
    if-eqz v6, :cond_25c

    .line 129
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, "file.separator"

    invoke-static/range {v18 .. v18}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual {v6}, Lnet/lingala/zip4j/model/FileHeader;->getFileName()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    .line 130
    .local v13, "outFilePath":Ljava/lang/String;
    new-instance v11, Ljava/io/File;

    invoke-direct {v11, v13}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 133
    .local v11, "outFile":Ljava/io/File;
    invoke-virtual {v6}, Lnet/lingala/zip4j/model/FileHeader;->isDirectory()Z

    move-result v17

    if-eqz v17, :cond_f4

    .line 136
    invoke-virtual {v11}, Ljava/io/File;->mkdirs()Z

    .line 137
    const/16 v17, 0x3

    move/from16 v0, v17

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    const-string v19, "chmod"

    aput-object v19, v17, v18

    const/16 v18, 0x1

    const-string v19, "771"

    aput-object v19, v17, v18

    const/16 v18, 0x2

    invoke-virtual {v11}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v19

    aput-object v19, v17, v18

    invoke-static/range {v17 .. v17}, Lcom/chelpus/Utils;->cmdParam([Ljava/lang/String;)Ljava/lang/String;

    .line 138
    const/16 v17, 0x3

    move/from16 v0, v17

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    const-string v19, "chown"

    aput-object v19, v17, v18

    const/16 v18, 0x1

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "0:"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    aput-object v19, v17, v18

    const/16 v18, 0x2

    invoke-virtual {v11}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v19

    aput-object v19, v17, v18

    invoke-static/range {v17 .. v17}, Lcom/chelpus/Utils;->cmdParam([Ljava/lang/String;)Ljava/lang/String;

    .line 139
    const/16 v17, 0x3

    move/from16 v0, v17

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    const-string v19, "chown"

    aput-object v19, v17, v18

    const/16 v18, 0x1

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "0."

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    aput-object v19, v17, v18

    const/16 v18, 0x2

    invoke-virtual {v11}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v19

    aput-object v19, v17, v18

    invoke-static/range {v17 .. v17}, Lcom/chelpus/Utils;->cmdParam([Ljava/lang/String;)Ljava/lang/String;
    :try_end_e2
    .catch Lnet/lingala/zip4j/exception/ZipException; {:try_start_1c .. :try_end_e2} :catch_e4
    .catch Ljava/io/FileNotFoundException; {:try_start_1c .. :try_end_e2} :catch_2f7
    .catch Ljava/io/IOException; {:try_start_1c .. :try_end_e2} :catch_2f4
    .catch Ljava/lang/Exception; {:try_start_1c .. :try_end_e2} :catch_2f1
    .catchall {:try_start_1c .. :try_end_e2} :catchall_2ee

    goto/16 :goto_1c

    .line 184
    .end local v2    # "aFileHeaderList":Ljava/lang/Object;
    .end local v6    # "fileHeader":Lnet/lingala/zip4j/model/FileHeader;
    .end local v11    # "outFile":Ljava/io/File;
    .end local v13    # "outFilePath":Ljava/lang/String;
    :catch_e4
    move-exception v5

    move-object v9, v10

    .line 185
    .end local v7    # "fileHeaderList":Ljava/util/List;
    .end local v10    # "os":Ljava/io/OutputStream;
    .local v5, "e":Lnet/lingala/zip4j/exception/ZipException;
    .restart local v9    # "os":Ljava/io/OutputStream;
    :goto_e6
    :try_start_e6
    invoke-virtual {v5}, Lnet/lingala/zip4j/exception/ZipException;->printStackTrace()V

    .line 186
    sget-object v16, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v17, "error"

    invoke-virtual/range {v16 .. v17}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_f0
    .catchall {:try_start_e6 .. :try_end_f0} :catchall_2dd

    .line 198
    :try_start_f0
    invoke-static {v8, v9}, Lcom/chelpus/root/utils/restoredata;->closeFileHandlers(Lnet/lingala/zip4j/io/ZipInputStream;Ljava/io/OutputStream;)V
    :try_end_f3
    .catch Ljava/io/IOException; {:try_start_f0 .. :try_end_f3} :catch_279

    .line 204
    .end local v5    # "e":Lnet/lingala/zip4j/exception/ZipException;
    :goto_f3
    return-void

    .line 145
    .end local v9    # "os":Ljava/io/OutputStream;
    .restart local v2    # "aFileHeaderList":Ljava/lang/Object;
    .restart local v6    # "fileHeader":Lnet/lingala/zip4j/model/FileHeader;
    .restart local v7    # "fileHeaderList":Ljava/util/List;
    .restart local v10    # "os":Ljava/io/OutputStream;
    .restart local v11    # "outFile":Ljava/io/File;
    .restart local v13    # "outFilePath":Ljava/lang/String;
    :cond_f4
    :try_start_f4
    invoke-virtual {v11}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v14

    .line 146
    .local v14, "parentDir":Ljava/io/File;
    invoke-virtual {v14}, Ljava/io/File;->exists()Z

    move-result v17

    if-nez v17, :cond_188

    .line 147
    invoke-virtual {v14}, Ljava/io/File;->mkdirs()Z

    .line 148
    const/16 v17, 0x3

    move/from16 v0, v17

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    const-string v19, "chmod"

    aput-object v19, v17, v18

    const/16 v18, 0x1

    const-string v19, "771"

    aput-object v19, v17, v18

    const/16 v18, 0x2

    invoke-virtual {v14}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v19

    aput-object v19, v17, v18

    invoke-static/range {v17 .. v17}, Lcom/chelpus/Utils;->cmdParam([Ljava/lang/String;)Ljava/lang/String;

    .line 149
    const/16 v17, 0x3

    move/from16 v0, v17

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    const-string v19, "chown"

    aput-object v19, v17, v18

    const/16 v18, 0x1

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "0:"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    aput-object v19, v17, v18

    const/16 v18, 0x2

    invoke-virtual {v14}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v19

    aput-object v19, v17, v18

    invoke-static/range {v17 .. v17}, Lcom/chelpus/Utils;->cmdParam([Ljava/lang/String;)Ljava/lang/String;

    .line 150
    const/16 v17, 0x3

    move/from16 v0, v17

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    const-string v19, "chown"

    aput-object v19, v17, v18

    const/16 v18, 0x1

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "0."

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    aput-object v19, v17, v18

    const/16 v18, 0x2

    invoke-virtual {v14}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v19

    aput-object v19, v17, v18

    invoke-static/range {v17 .. v17}, Lcom/chelpus/Utils;->cmdParam([Ljava/lang/String;)Ljava/lang/String;

    .line 155
    :cond_188
    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lnet/lingala/zip4j/core/ZipFile;->getInputStream(Lnet/lingala/zip4j/model/FileHeader;)Lnet/lingala/zip4j/io/ZipInputStream;

    move-result-object v8

    .line 157
    new-instance v9, Ljava/io/FileOutputStream;

    invoke-direct {v9, v11}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_193
    .catch Lnet/lingala/zip4j/exception/ZipException; {:try_start_f4 .. :try_end_193} :catch_e4
    .catch Ljava/io/FileNotFoundException; {:try_start_f4 .. :try_end_193} :catch_2f7
    .catch Ljava/io/IOException; {:try_start_f4 .. :try_end_193} :catch_2f4
    .catch Ljava/lang/Exception; {:try_start_f4 .. :try_end_193} :catch_2f1
    .catchall {:try_start_f4 .. :try_end_193} :catchall_2ee

    .line 159
    .end local v10    # "os":Ljava/io/OutputStream;
    .restart local v9    # "os":Ljava/io/OutputStream;
    const/4 v15, -0x1

    .line 160
    .local v15, "readLen":I
    const/16 v17, 0x1000

    :try_start_196
    move/from16 v0, v17

    new-array v3, v0, [B

    .line 163
    .local v3, "buff":[B
    :goto_19a
    invoke-virtual {v8, v3}, Lnet/lingala/zip4j/io/ZipInputStream;->read([B)I

    move-result v15

    const/16 v17, -0x1

    move/from16 v0, v17

    if-eq v15, v0, :cond_1af

    .line 164
    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-virtual {v9, v3, v0, v15}, Ljava/io/OutputStream;->write([BII)V

    goto :goto_19a

    .line 184
    .end local v2    # "aFileHeaderList":Ljava/lang/Object;
    .end local v3    # "buff":[B
    .end local v6    # "fileHeader":Lnet/lingala/zip4j/model/FileHeader;
    .end local v7    # "fileHeaderList":Ljava/util/List;
    .end local v11    # "outFile":Ljava/io/File;
    .end local v13    # "outFilePath":Ljava/lang/String;
    .end local v14    # "parentDir":Ljava/io/File;
    .end local v15    # "readLen":I
    :catch_1ac
    move-exception v5

    goto/16 :goto_e6

    .line 168
    .restart local v2    # "aFileHeaderList":Ljava/lang/Object;
    .restart local v3    # "buff":[B
    .restart local v6    # "fileHeader":Lnet/lingala/zip4j/model/FileHeader;
    .restart local v7    # "fileHeaderList":Ljava/util/List;
    .restart local v11    # "outFile":Ljava/io/File;
    .restart local v13    # "outFilePath":Ljava/lang/String;
    .restart local v14    # "parentDir":Ljava/io/File;
    .restart local v15    # "readLen":I
    :cond_1af
    invoke-static {v8, v9}, Lcom/chelpus/root/utils/restoredata;->closeFileHandlers(Lnet/lingala/zip4j/io/ZipInputStream;Ljava/io/OutputStream;)V

    .line 173
    invoke-static {v6, v11}, Lnet/lingala/zip4j/unzip/UnzipUtil;->applyFileAttributes(Lnet/lingala/zip4j/model/FileHeader;Ljava/io/File;)V

    .line 174
    const/4 v12, 0x0

    .line 176
    .local v12, "outFile2":Ljava/lang/String;
    const/16 v17, 0x3

    move/from16 v0, v17

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    const-string v19, "chmod"

    aput-object v19, v17, v18

    const/16 v18, 0x1

    const-string v19, "771"

    aput-object v19, v17, v18

    const/16 v18, 0x2

    invoke-virtual {v11}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v19

    aput-object v19, v17, v18

    invoke-static/range {v17 .. v17}, Lcom/chelpus/Utils;->cmdParam([Ljava/lang/String;)Ljava/lang/String;

    .line 177
    const/16 v17, 0x3

    move/from16 v0, v17

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    const-string v19, "chown"

    aput-object v19, v17, v18

    const/16 v18, 0x1

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "0:"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    aput-object v19, v17, v18

    const/16 v18, 0x2

    invoke-virtual {v11}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v19

    aput-object v19, v17, v18

    invoke-static/range {v17 .. v17}, Lcom/chelpus/Utils;->cmdParam([Ljava/lang/String;)Ljava/lang/String;

    .line 178
    const/16 v17, 0x3

    move/from16 v0, v17

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    const-string v19, "chown"

    aput-object v19, v17, v18

    const/16 v18, 0x1

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "0."

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    aput-object v19, v17, v18

    const/16 v18, 0x2

    invoke-virtual {v11}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v19

    aput-object v19, v17, v18

    invoke-static/range {v17 .. v17}, Lcom/chelpus/Utils;->cmdParam([Ljava/lang/String;)Ljava/lang/String;

    .line 179
    sget-object v17, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Done extracting: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v6}, Lnet/lingala/zip4j/model/FileHeader;->getFileName()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_259
    .catch Lnet/lingala/zip4j/exception/ZipException; {:try_start_196 .. :try_end_259} :catch_1ac
    .catch Ljava/io/FileNotFoundException; {:try_start_196 .. :try_end_259} :catch_286
    .catch Ljava/io/IOException; {:try_start_196 .. :try_end_259} :catch_2a3
    .catch Ljava/lang/Exception; {:try_start_196 .. :try_end_259} :catch_2c0
    .catchall {:try_start_196 .. :try_end_259} :catchall_2dd

    .end local v3    # "buff":[B
    .end local v11    # "outFile":Ljava/io/File;
    .end local v12    # "outFile2":Ljava/lang/String;
    .end local v13    # "outFilePath":Ljava/lang/String;
    .end local v14    # "parentDir":Ljava/io/File;
    .end local v15    # "readLen":I
    :goto_259
    move-object v10, v9

    .line 183
    .end local v9    # "os":Ljava/io/OutputStream;
    .restart local v10    # "os":Ljava/io/OutputStream;
    goto/16 :goto_1c

    .line 181
    :cond_25c
    :try_start_25c
    sget-object v17, Ljava/lang/System;->err:Ljava/io/PrintStream;

    const-string v18, "fileheader is null. Shouldn\'t be here"

    invoke-virtual/range {v17 .. v18}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_263
    .catch Lnet/lingala/zip4j/exception/ZipException; {:try_start_25c .. :try_end_263} :catch_e4
    .catch Ljava/io/FileNotFoundException; {:try_start_25c .. :try_end_263} :catch_2f7
    .catch Ljava/io/IOException; {:try_start_25c .. :try_end_263} :catch_2f4
    .catch Ljava/lang/Exception; {:try_start_25c .. :try_end_263} :catch_2f1
    .catchall {:try_start_25c .. :try_end_263} :catchall_2ee

    move-object v9, v10

    .end local v10    # "os":Ljava/io/OutputStream;
    .restart local v9    # "os":Ljava/io/OutputStream;
    goto :goto_259

    .line 198
    .end local v2    # "aFileHeaderList":Ljava/lang/Object;
    .end local v6    # "fileHeader":Lnet/lingala/zip4j/model/FileHeader;
    .end local v9    # "os":Ljava/io/OutputStream;
    .restart local v10    # "os":Ljava/io/OutputStream;
    :cond_265
    :try_start_265
    invoke-static {v8, v10}, Lcom/chelpus/root/utils/restoredata;->closeFileHandlers(Lnet/lingala/zip4j/io/ZipInputStream;Ljava/io/OutputStream;)V
    :try_end_268
    .catch Ljava/io/IOException; {:try_start_265 .. :try_end_268} :catch_26b

    move-object v9, v10

    .line 202
    .end local v10    # "os":Ljava/io/OutputStream;
    .restart local v9    # "os":Ljava/io/OutputStream;
    goto/16 :goto_f3

    .line 199
    .end local v9    # "os":Ljava/io/OutputStream;
    .restart local v10    # "os":Ljava/io/OutputStream;
    :catch_26b
    move-exception v5

    .line 200
    .local v5, "e":Ljava/io/IOException;
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    .line 201
    sget-object v16, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v17, "error"

    invoke-virtual/range {v16 .. v17}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    move-object v9, v10

    .line 203
    .end local v10    # "os":Ljava/io/OutputStream;
    .restart local v9    # "os":Ljava/io/OutputStream;
    goto/16 :goto_f3

    .line 199
    .end local v7    # "fileHeaderList":Ljava/util/List;
    .local v5, "e":Lnet/lingala/zip4j/exception/ZipException;
    :catch_279
    move-exception v5

    .line 200
    .local v5, "e":Ljava/io/IOException;
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    .line 201
    sget-object v16, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v17, "error"

    invoke-virtual/range {v16 .. v17}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_f3

    .line 187
    .end local v5    # "e":Ljava/io/IOException;
    :catch_286
    move-exception v5

    .line 188
    .local v5, "e":Ljava/io/FileNotFoundException;
    :goto_287
    :try_start_287
    invoke-virtual {v5}, Ljava/io/FileNotFoundException;->printStackTrace()V

    .line 189
    sget-object v16, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v17, "error"

    invoke-virtual/range {v16 .. v17}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_291
    .catchall {:try_start_287 .. :try_end_291} :catchall_2dd

    .line 198
    :try_start_291
    invoke-static {v8, v9}, Lcom/chelpus/root/utils/restoredata;->closeFileHandlers(Lnet/lingala/zip4j/io/ZipInputStream;Ljava/io/OutputStream;)V
    :try_end_294
    .catch Ljava/io/IOException; {:try_start_291 .. :try_end_294} :catch_296

    goto/16 :goto_f3

    .line 199
    :catch_296
    move-exception v5

    .line 200
    .local v5, "e":Ljava/io/IOException;
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    .line 201
    sget-object v16, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v17, "error"

    invoke-virtual/range {v16 .. v17}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_f3

    .line 190
    .end local v5    # "e":Ljava/io/IOException;
    :catch_2a3
    move-exception v5

    .line 191
    .restart local v5    # "e":Ljava/io/IOException;
    :goto_2a4
    :try_start_2a4
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    .line 192
    sget-object v16, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v17, "error"

    invoke-virtual/range {v16 .. v17}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_2ae
    .catchall {:try_start_2a4 .. :try_end_2ae} :catchall_2dd

    .line 198
    :try_start_2ae
    invoke-static {v8, v9}, Lcom/chelpus/root/utils/restoredata;->closeFileHandlers(Lnet/lingala/zip4j/io/ZipInputStream;Ljava/io/OutputStream;)V
    :try_end_2b1
    .catch Ljava/io/IOException; {:try_start_2ae .. :try_end_2b1} :catch_2b3

    goto/16 :goto_f3

    .line 199
    :catch_2b3
    move-exception v5

    .line 200
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    .line 201
    sget-object v16, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v17, "error"

    invoke-virtual/range {v16 .. v17}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_f3

    .line 193
    .end local v5    # "e":Ljava/io/IOException;
    :catch_2c0
    move-exception v5

    .line 194
    .local v5, "e":Ljava/lang/Exception;
    :goto_2c1
    :try_start_2c1
    invoke-virtual {v5}, Ljava/lang/Exception;->printStackTrace()V

    .line 195
    sget-object v16, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v17, "error"

    invoke-virtual/range {v16 .. v17}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_2cb
    .catchall {:try_start_2c1 .. :try_end_2cb} :catchall_2dd

    .line 198
    :try_start_2cb
    invoke-static {v8, v9}, Lcom/chelpus/root/utils/restoredata;->closeFileHandlers(Lnet/lingala/zip4j/io/ZipInputStream;Ljava/io/OutputStream;)V
    :try_end_2ce
    .catch Ljava/io/IOException; {:try_start_2cb .. :try_end_2ce} :catch_2d0

    goto/16 :goto_f3

    .line 199
    :catch_2d0
    move-exception v5

    .line 200
    .local v5, "e":Ljava/io/IOException;
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    .line 201
    sget-object v16, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v17, "error"

    invoke-virtual/range {v16 .. v17}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_f3

    .line 197
    .end local v5    # "e":Ljava/io/IOException;
    :catchall_2dd
    move-exception v16

    .line 198
    :goto_2de
    :try_start_2de
    invoke-static {v8, v9}, Lcom/chelpus/root/utils/restoredata;->closeFileHandlers(Lnet/lingala/zip4j/io/ZipInputStream;Ljava/io/OutputStream;)V
    :try_end_2e1
    .catch Ljava/io/IOException; {:try_start_2de .. :try_end_2e1} :catch_2e2

    .line 202
    :goto_2e1
    throw v16

    .line 199
    :catch_2e2
    move-exception v5

    .line 200
    .restart local v5    # "e":Ljava/io/IOException;
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    .line 201
    sget-object v17, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v18, "error"

    invoke-virtual/range {v17 .. v18}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_2e1

    .line 197
    .end local v5    # "e":Ljava/io/IOException;
    .end local v9    # "os":Ljava/io/OutputStream;
    .restart local v7    # "fileHeaderList":Ljava/util/List;
    .restart local v10    # "os":Ljava/io/OutputStream;
    :catchall_2ee
    move-exception v16

    move-object v9, v10

    .end local v10    # "os":Ljava/io/OutputStream;
    .restart local v9    # "os":Ljava/io/OutputStream;
    goto :goto_2de

    .line 193
    .end local v9    # "os":Ljava/io/OutputStream;
    .restart local v10    # "os":Ljava/io/OutputStream;
    :catch_2f1
    move-exception v5

    move-object v9, v10

    .end local v10    # "os":Ljava/io/OutputStream;
    .restart local v9    # "os":Ljava/io/OutputStream;
    goto :goto_2c1

    .line 190
    .end local v9    # "os":Ljava/io/OutputStream;
    .restart local v10    # "os":Ljava/io/OutputStream;
    :catch_2f4
    move-exception v5

    move-object v9, v10

    .end local v10    # "os":Ljava/io/OutputStream;
    .restart local v9    # "os":Ljava/io/OutputStream;
    goto :goto_2a4

    .line 187
    .end local v9    # "os":Ljava/io/OutputStream;
    .restart local v10    # "os":Ljava/io/OutputStream;
    :catch_2f7
    move-exception v5

    move-object v9, v10

    .end local v10    # "os":Ljava/io/OutputStream;
    .restart local v9    # "os":Ljava/io/OutputStream;
    goto :goto_287
.end method

.method private static closeFileHandlers(Lnet/lingala/zip4j/io/ZipInputStream;Ljava/io/OutputStream;)V
    .registers 2
    .param p0, "is"    # Lnet/lingala/zip4j/io/ZipInputStream;
    .param p1, "os"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 207
    if-eqz p1, :cond_6

    .line 208
    invoke-virtual {p1}, Ljava/io/OutputStream;->close()V

    .line 209
    const/4 p1, 0x0

    .line 218
    :cond_6
    if-eqz p0, :cond_c

    .line 219
    invoke-virtual {p0}, Lnet/lingala/zip4j/io/ZipInputStream;->close()V

    .line 220
    const/4 p0, 0x0

    .line 222
    :cond_c
    return-void
.end method

.method public static copyFolder(Ljava/io/File;Ljava/io/File;Ljava/lang/String;)V
    .registers 14
    .param p0, "src"    # Ljava/io/File;
    .param p1, "dest"    # Ljava/io/File;
    .param p2, "uid"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v4, 0x0

    .line 226
    invoke-virtual {p0}, Ljava/io/File;->isDirectory()Z

    move-result v5

    if-eqz v5, :cond_ac

    .line 229
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v5

    if-nez v5, :cond_90

    .line 230
    invoke-virtual {p1}, Ljava/io/File;->mkdir()Z

    .line 231
    new-array v5, v10, [Ljava/lang/String;

    const-string v6, "chmod"

    aput-object v6, v5, v4

    const-string v6, "771"

    aput-object v6, v5, v8

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v9

    invoke-static {v5}, Lcom/chelpus/Utils;->cmdParam([Ljava/lang/String;)Ljava/lang/String;

    .line 232
    new-array v5, v10, [Ljava/lang/String;

    const-string v6, "chown"

    aput-object v6, v5, v4

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "0:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v9

    invoke-static {v5}, Lcom/chelpus/Utils;->cmdParam([Ljava/lang/String;)Ljava/lang/String;

    .line 233
    new-array v5, v10, [Ljava/lang/String;

    const-string v6, "chown"

    aput-object v6, v5, v4

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "0."

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v9

    invoke-static {v5}, Lcom/chelpus/Utils;->cmdParam([Ljava/lang/String;)Ljava/lang/String;

    .line 234
    sget-object v5, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Directory copied from "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "  to "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 239
    :cond_90
    invoke-virtual {p0}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v2

    .line 240
    .local v2, "files":[Ljava/lang/String;
    array-length v5, v2

    if-lez v5, :cond_10a

    .line 241
    array-length v5, v2

    :goto_98
    if-ge v4, v5, :cond_10a

    aget-object v1, v2, v4

    .line 243
    .local v1, "file":Ljava/lang/String;
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, p0, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 244
    .local v3, "srcFile":Ljava/io/File;
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 246
    .local v0, "destFile":Ljava/io/File;
    invoke-static {v3, v0, p2}, Lcom/chelpus/root/utils/restoredata;->copyFolder(Ljava/io/File;Ljava/io/File;Ljava/lang/String;)V

    .line 241
    add-int/lit8 v4, v4, 0x1

    goto :goto_98

    .line 253
    .end local v0    # "destFile":Ljava/io/File;
    .end local v1    # "file":Ljava/lang/String;
    .end local v2    # "files":[Ljava/lang/String;
    .end local v3    # "srcFile":Ljava/io/File;
    :cond_ac
    invoke-static {p0, p1}, Lcom/chelpus/Utils;->copyFile(Ljava/io/File;Ljava/io/File;)V

    .line 254
    new-array v5, v10, [Ljava/lang/String;

    const-string v6, "chmod"

    aput-object v6, v5, v4

    const-string v6, "771"

    aput-object v6, v5, v8

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v9

    invoke-static {v5}, Lcom/chelpus/Utils;->cmdParam([Ljava/lang/String;)Ljava/lang/String;

    .line 255
    new-array v5, v10, [Ljava/lang/String;

    const-string v6, "chown"

    aput-object v6, v5, v4

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "0:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v9

    invoke-static {v5}, Lcom/chelpus/Utils;->cmdParam([Ljava/lang/String;)Ljava/lang/String;

    .line 256
    new-array v5, v10, [Ljava/lang/String;

    const-string v6, "chown"

    aput-object v6, v5, v4

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "0."

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v5, v8

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v5, v9

    invoke-static {v5}, Lcom/chelpus/Utils;->cmdParam([Ljava/lang/String;)Ljava/lang/String;

    .line 272
    :cond_10a
    return-void
.end method

.method public static main([Ljava/lang/String;)V
    .registers 18
    .param p0, "paramArrayOfString"    # [Ljava/lang/String;

    .prologue
    .line 23
    new-instance v13, Lcom/chelpus/root/utils/restoredata$1;

    invoke-direct {v13}, Lcom/chelpus/root/utils/restoredata$1;-><init>()V

    invoke-static {v13}, Lcom/chelpus/Utils;->startRootJava(Ljava/lang/Object;)V

    .line 24
    const/4 v13, 0x0

    aget-object v7, p0, v13

    .line 25
    .local v7, "pkg":Ljava/lang/String;
    const/4 v13, 0x1

    aget-object v3, p0, v13

    .line 26
    .local v3, "datadir":Ljava/lang/String;
    const/4 v13, 0x2

    aget-object v0, p0, v13

    .line 27
    .local v0, "backup_data_dir":Ljava/lang/String;
    const/4 v13, 0x3

    aget-object v10, p0, v13

    .line 28
    .local v10, "uid":Ljava/lang/String;
    const/4 v13, 0x4

    aget-object v8, p0, v13

    .line 35
    .local v8, "sddatadir":Ljava/lang/String;
    :try_start_17
    new-instance v1, Ljava/io/File;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "/data.lpbkp"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v1, v13}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 36
    .local v1, "data":Ljava/io/File;
    sget-object v13, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 37
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v13

    if-eqz v13, :cond_58

    .line 38
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_43
    .catch Ljava/lang/Exception; {:try_start_17 .. :try_end_43} :catch_11c

    .line 41
    .local v4, "datadirs":Ljava/io/File;
    :try_start_43
    new-instance v11, Lnet/lingala/zip4j/core/ZipFile;

    invoke-direct {v11, v1}, Lnet/lingala/zip4j/core/ZipFile;-><init>(Ljava/io/File;)V

    .line 42
    .local v11, "zipFile":Lnet/lingala/zip4j/core/ZipFile;
    sget-object v13, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 43
    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v13

    invoke-static {v11, v13, v10}, Lcom/chelpus/root/utils/restoredata;->ExtractAllFilesWithInputStreams(Lnet/lingala/zip4j/core/ZipFile;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_58
    .catch Lnet/lingala/zip4j/exception/ZipException; {:try_start_43 .. :try_end_58} :catch_10f
    .catch Ljava/lang/Exception; {:try_start_43 .. :try_end_58} :catch_11c

    .line 50
    .end local v4    # "datadirs":Ljava/io/File;
    .end local v11    # "zipFile":Lnet/lingala/zip4j/core/ZipFile;
    :cond_58
    :goto_58
    :try_start_58
    new-instance v13, Ljava/io/File;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "/dbdata/databases/"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-direct {v13, v14}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v13}, Ljava/io/File;->exists()Z

    move-result v13

    if-eqz v13, :cond_1aa

    .line 51
    new-instance v5, Ljava/io/File;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "/dbdata.lpbkp"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v5, v13}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 52
    .local v5, "dbdata":Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->exists()Z
    :try_end_91
    .catch Ljava/lang/Exception; {:try_start_58 .. :try_end_91} :catch_11c

    move-result v13

    if-eqz v13, :cond_151

    .line 54
    :try_start_94
    new-instance v12, Lnet/lingala/zip4j/core/ZipFile;

    invoke-direct {v12, v5}, Lnet/lingala/zip4j/core/ZipFile;-><init>(Ljava/io/File;)V

    .line 55
    .local v12, "zipFile2":Lnet/lingala/zip4j/core/ZipFile;
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "/dbdata/databases/"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13, v10}, Lcom/chelpus/root/utils/restoredata;->ExtractAllFilesWithInputStreams(Lnet/lingala/zip4j/core/ZipFile;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_af
    .catch Lnet/lingala/zip4j/exception/ZipException; {:try_start_94 .. :try_end_af} :catch_144
    .catch Ljava/lang/Exception; {:try_start_94 .. :try_end_af} :catch_11c

    .line 79
    .end local v12    # "zipFile2":Lnet/lingala/zip4j/core/ZipFile;
    :cond_af
    :goto_af
    :try_start_af
    new-instance v13, Ljava/io/File;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v14, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "/"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-direct {v13, v14}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v13}, Ljava/io/File;->mkdirs()Z

    .line 80
    new-instance v2, Ljava/io/File;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "/sddata.lpbkp"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v2, v13}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 81
    .local v2, "data3":Ljava/io/File;
    sget-object v13, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 82
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v13

    if-eqz v13, :cond_10b

    .line 83
    new-instance v9, Ljava/io/File;

    invoke-direct {v9, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_f6
    .catch Ljava/lang/Exception; {:try_start_af .. :try_end_f6} :catch_11c

    .line 86
    .local v9, "sddatadirs":Ljava/io/File;
    :try_start_f6
    new-instance v11, Lnet/lingala/zip4j/core/ZipFile;

    invoke-direct {v11, v2}, Lnet/lingala/zip4j/core/ZipFile;-><init>(Ljava/io/File;)V

    .line 87
    .restart local v11    # "zipFile":Lnet/lingala/zip4j/core/ZipFile;
    sget-object v13, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v9}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 88
    invoke-virtual {v9}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v13

    invoke-static {v11, v13, v10}, Lcom/chelpus/root/utils/restoredata;->ExtractAllFilesWithInputStreams(Lnet/lingala/zip4j/core/ZipFile;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_10b
    .catch Lnet/lingala/zip4j/exception/ZipException; {:try_start_f6 .. :try_end_10b} :catch_1df
    .catch Ljava/lang/Exception; {:try_start_f6 .. :try_end_10b} :catch_1ec

    .line 101
    .end local v1    # "data":Ljava/io/File;
    .end local v2    # "data3":Ljava/io/File;
    .end local v5    # "dbdata":Ljava/io/File;
    .end local v9    # "sddatadirs":Ljava/io/File;
    .end local v11    # "zipFile":Lnet/lingala/zip4j/core/ZipFile;
    :cond_10b
    :goto_10b
    invoke-static {}, Lcom/chelpus/Utils;->exitFromRootJava()V

    .line 102
    return-void

    .line 45
    .restart local v1    # "data":Ljava/io/File;
    .restart local v4    # "datadirs":Ljava/io/File;
    :catch_10f
    move-exception v6

    .line 46
    .local v6, "e":Lnet/lingala/zip4j/exception/ZipException;
    :try_start_110
    invoke-virtual {v6}, Lnet/lingala/zip4j/exception/ZipException;->printStackTrace()V

    .line 47
    sget-object v13, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v14, "error"

    invoke-virtual {v13, v14}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_11a
    .catch Ljava/lang/Exception; {:try_start_110 .. :try_end_11a} :catch_11c

    goto/16 :goto_58

    .line 96
    .end local v1    # "data":Ljava/io/File;
    .end local v4    # "datadirs":Ljava/io/File;
    .end local v6    # "e":Lnet/lingala/zip4j/exception/ZipException;
    :catch_11c
    move-exception v6

    .line 97
    .local v6, "e":Ljava/lang/Exception;
    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V

    .line 98
    sget-object v13, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Exception e"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v6}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 99
    sget-object v13, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v14, "error"

    invoke-virtual {v13, v14}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_10b

    .line 57
    .end local v6    # "e":Ljava/lang/Exception;
    .restart local v1    # "data":Ljava/io/File;
    .restart local v5    # "dbdata":Ljava/io/File;
    :catch_144
    move-exception v6

    .line 58
    .local v6, "e":Lnet/lingala/zip4j/exception/ZipException;
    :try_start_145
    invoke-virtual {v6}, Lnet/lingala/zip4j/exception/ZipException;->printStackTrace()V

    .line 59
    sget-object v13, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v14, "error"

    invoke-virtual {v13, v14}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_af

    .line 62
    .end local v6    # "e":Lnet/lingala/zip4j/exception/ZipException;
    :cond_151
    new-instance v13, Ljava/io/File;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v14, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "/shared_prefs"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-direct {v13, v14}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v13}, Ljava/io/File;->exists()Z

    move-result v13

    if-eqz v13, :cond_af

    .line 63
    new-instance v13, Ljava/io/File;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v14, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "/shared_prefs"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-direct {v13, v14}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v14, Ljava/io/File;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "/dbdata/databases/"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "/shared_prefs"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-direct {v14, v15}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v13, v14, v10}, Lcom/chelpus/root/utils/restoredata;->copyFolder(Ljava/io/File;Ljava/io/File;Ljava/lang/String;)V

    goto/16 :goto_af

    .line 67
    .end local v5    # "dbdata":Ljava/io/File;
    :cond_1aa
    new-instance v5, Ljava/io/File;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "/dbdata.lpbkp"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v5, v13}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 68
    .restart local v5    # "dbdata":Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->exists()Z
    :try_end_1c5
    .catch Ljava/lang/Exception; {:try_start_145 .. :try_end_1c5} :catch_11c

    move-result v13

    if-eqz v13, :cond_af

    .line 70
    :try_start_1c8
    new-instance v12, Lnet/lingala/zip4j/core/ZipFile;

    invoke-direct {v12, v5}, Lnet/lingala/zip4j/core/ZipFile;-><init>(Ljava/io/File;)V

    .line 71
    .restart local v12    # "zipFile2":Lnet/lingala/zip4j/core/ZipFile;
    invoke-static {v12, v3, v10}, Lcom/chelpus/root/utils/restoredata;->ExtractAllFilesWithInputStreams(Lnet/lingala/zip4j/core/ZipFile;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1d0
    .catch Lnet/lingala/zip4j/exception/ZipException; {:try_start_1c8 .. :try_end_1d0} :catch_1d2
    .catch Ljava/lang/Exception; {:try_start_1c8 .. :try_end_1d0} :catch_11c

    goto/16 :goto_af

    .line 73
    .end local v12    # "zipFile2":Lnet/lingala/zip4j/core/ZipFile;
    :catch_1d2
    move-exception v6

    .line 74
    .restart local v6    # "e":Lnet/lingala/zip4j/exception/ZipException;
    :try_start_1d3
    invoke-virtual {v6}, Lnet/lingala/zip4j/exception/ZipException;->printStackTrace()V

    .line 75
    sget-object v13, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v14, "error"

    invoke-virtual {v13, v14}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_af

    .line 90
    .end local v6    # "e":Lnet/lingala/zip4j/exception/ZipException;
    .restart local v2    # "data3":Ljava/io/File;
    .restart local v9    # "sddatadirs":Ljava/io/File;
    :catch_1df
    move-exception v6

    .line 91
    .restart local v6    # "e":Lnet/lingala/zip4j/exception/ZipException;
    invoke-virtual {v6}, Lnet/lingala/zip4j/exception/ZipException;->printStackTrace()V

    .line 92
    sget-object v13, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v14, "error"

    invoke-virtual {v13, v14}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_10b

    .line 93
    .end local v6    # "e":Lnet/lingala/zip4j/exception/ZipException;
    :catch_1ec
    move-exception v6

    .local v6, "e":Ljava/lang/Exception;
    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1f0
    .catch Ljava/lang/Exception; {:try_start_1d3 .. :try_end_1f0} :catch_11c

    goto/16 :goto_10b
.end method
